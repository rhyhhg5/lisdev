package com.sinosoft.httpclient.inf;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.httpclient.dto.end010.request.END010Request;
import com.sinosoft.httpclient.dto.end010.request.Head;
import com.sinosoft.httpclient.dto.end010.request.RequestNode;
import com.sinosoft.httpclient.dto.end010.response.Client;
import com.sinosoft.httpclient.dto.end010.response.END010Response;
import com.sinosoft.httpclient.dto.end010.response.TransferOut;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LSTransInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSTransInfoSchema;
import com.sinosoft.lis.schema.PolicyTransformRemainderQuerySchema;
import com.sinosoft.lis.vschema.LSTransInfoSet;
import com.sinosoft.lis.vschema.PolicyTransformRemainderQuerySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranferOut </p>
 * <p>Description: 保单转入余额信息查询End010</p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class PolicyTransformRemainderQuery {
	/**公共的错误类*/
	public CErrors mErrors = new CErrors();
	/**返回结构的容器*/
	private VData mResult = new VData();
	/**存储保存外部传入的值*/
	private VData mInputData = new VData();
	/**携带数据的类型*/
	private TransferData tempTransferData = new TransferData();
	/**存储外部传入的操作类型*/
	private String mOperate;
	/**获取数据的开始日期*/
	private String mQueryStartDate;
	/**获取数据的结束日期*/
	private String mQueryEndDate;
	/**存储交互错误信息*/
	  private String mErrorInfo ;
	
	private MMap mMap = new MMap();
	
	private PolicyTransformRemainderQuerySchema tPolicyTransformRemainderQuerySchema = null;
	private PolicyTransformRemainderQuerySet tPolicyTransformRemainderQuerySet = null;
	
	/**
	 * 公共的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		/**
		 * 获取传入的值
		 */
		if(!getInputData()){
			return false;
		}
		/**
		 * 数据处理方法
		 */
		if(!dealData()){
			return false;
		}
		/**
		 * 准备往后台传输数据
		 */
		if(!preparedData()){
			return false;
		}
		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start PolicyTransformRemainderQuery pubsubmit!!!");
		if(!tPubSubmit.submitData(mInputData, mOperate)){
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContTranferOut";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end PolicyTransformRemainderQuery pubsubmit!!!");
		mInputData = null;
		return true;
	}
	/**
	 * 获取传入的值
	 */
	private boolean getInputData(){
		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mQueryStartDate = (String) tempTransferData.getValueByName("QueryStartDate");
		mQueryEndDate = (String) tempTransferData.getValueByName("QueryEndDate");
		if(mQueryStartDate==null||mQueryStartDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOut";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询起始日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		if(mQueryEndDate==null||mQueryEndDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOut";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询终止日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/**
	 * 数据处理方法
	 */
	private boolean dealData(){
		END010Request tEND010Request = new END010Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setQueryStartDate(mQueryStartDate);
		tRequestNode.setQueryEndDate(mQueryEndDate);
		System.out.println("mQueryStartDate---"+tRequestNode.getQueryStartDate());
		tEND010Request.getRequestNodes().getRequestNode().add(tRequestNode);
		System.out.println("mQueryEndDate---"+tRequestNode.getQueryEndDate());
		/**生成xml的头部信息*/
		Head head = new Head();
		
		try {
			/**
			 * 调用发送请求报文的方法，用responseXml 去接收返回的响应报文
			 */
			String responseXml = CommunicateServiceImpl.post(tEND010Request, mOperate);
			/**
			 * 将接收到的xml字符串解析为bean类
			 */
			END010Response tEND010Response = (END010Response) XmlParseUtil.xmlToDto(responseXml, END010Response.class);
			/**获取返回的状态*/
			String ResponseCode = tEND010Response.getHead().getResponseCode();
			//获取处理结果状态的值
			String ResultStatus = tEND010Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			String ResultInfoDesc = tEND010Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			String CiitcResultCode = tEND010Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultCode();
			String CiitcResultMessage = tEND010Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultMessage();
			
			List<TransferOut> tTransferOut = new ArrayList<TransferOut>();
			List<Client> tClientList = new ArrayList<Client>();
			//根据返回的状态进行判断，0000时代表没有重大的异常
			if(ResponseCode!=null&&!ResponseCode.equals("")&&"0000".equals(ResponseCode)){
				
				System.out.println("tResultStatus--"+ResultStatus);
				System.out.println("tResultInfoDesc--"+ResultInfoDesc);
				System.out.println("tCiitcResultCode--"+CiitcResultCode);
				System.out.println("tCiitcResultMessage--"+CiitcResultMessage);
					
				if("00".equals(ResultStatus)&&"1".equals(CiitcResultCode)){
					tTransferOut = tEND010Response.getResponseNodes().getResponseNode().get(0).getTransferOutList().getTransferOut();
					System.out.println("tTransferOut---"+tTransferOut.size());
					if(tTransferOut.size()>0){
						tPolicyTransformRemainderQuerySet = new PolicyTransformRemainderQuerySet();
						for(int i=0;i<tTransferOut.size();i++){
							tPolicyTransformRemainderQuerySchema = new PolicyTransformRemainderQuerySchema();
							//获取返回的保单号
							String mPolicyNo = tTransferOut.get(i).getPolicyNo();
							tClientList = tTransferOut.get(i).getClientList().getClient();
							for(int j=0;j<tClientList.size();j++){
								String mSequenceNo = tClientList.get(j).getSequenceNo();
								String mContactName = tClientList.get(j).getContactName();
								String mContactTele = tClientList.get(j).getContactTele();
								String mContactEmail = tClientList.get(j).getContactEmail();
								String mTransReceivDate = tClientList.get(j).getTransReceivDate();
								String mTransAmount = tClientList.get(j).getTransAmount();
								tPolicyTransformRemainderQuerySchema.setSerNo(PubFun1.CreateMaxNo("SEQ_NAME_Remainder", 20));
								tPolicyTransformRemainderQuerySchema.setPolicyNo(mPolicyNo);
								tPolicyTransformRemainderQuerySchema.setSequenceNo(mSequenceNo);
								tPolicyTransformRemainderQuerySchema.setContactName(mContactName);
								tPolicyTransformRemainderQuerySchema.setContactTele(mContactTele);
								tPolicyTransformRemainderQuerySchema.setContactEmail(mContactEmail);
								tPolicyTransformRemainderQuerySchema.setTransReceivDate(mTransReceivDate);
								tPolicyTransformRemainderQuerySchema.setTransAmount(mTransAmount);
								tPolicyTransformRemainderQuerySchema.setResponseCode(ResponseCode);
								tPolicyTransformRemainderQuerySchema.setResultStatus(ResultStatus);
								tPolicyTransformRemainderQuerySchema.setResultInfoDesc(ResultInfoDesc);
								tPolicyTransformRemainderQuerySchema.setCiitcResultCode(CiitcResultCode);
								tPolicyTransformRemainderQuerySchema.setCiitcResultMessage(CiitcResultMessage);
								tPolicyTransformRemainderQuerySchema.setMakeDate(PubFun.getCurrentDate());
								tPolicyTransformRemainderQuerySchema.setMakeTime(PubFun.getCurrentTime());
								tPolicyTransformRemainderQuerySchema.setModifyDate(PubFun.getCurrentDate());
								tPolicyTransformRemainderQuerySchema.setModifyTime(PubFun.getCurrentTime());
								tPolicyTransformRemainderQuerySet.add(tPolicyTransformRemainderQuerySchema);
								//将保单转出余额存储到契约 转入申请表lstransinfo表TransAmount字段
								LSTransInfoSchema tLSTransInfoSchema = new LSTransInfoSchema();
								LSTransInfoDB tLSTransInfoDB = new LSTransInfoDB();
								LSTransInfoSet tLSTransInifoSet = tLSTransInfoDB.executeQuery("select * from LSTransInfo where TransGrpContNo = '"+mPolicyNo+"' and TransContNo = '"+mSequenceNo+"' ");
								if(tLSTransInifoSet !=null && tLSTransInifoSet.size()>0){
									tLSTransInfoSchema = tLSTransInifoSet.get(1).getSchema();
									tLSTransInfoSchema.setTransAmount(mTransAmount);
									tLSTransInfoSchema.setSuccFlag("03");
									tLSTransInfoSchema.setModifyDate(PubFun.getCurrentDate());
									tLSTransInfoSchema.setModifyTime(PubFun.getCurrentTime());
									mMap.put(tLSTransInfoSchema, "UPDATE");
								}
							}
						}
					}else{
							System.out.println("系统正常！");
							CError tCError = new CError();
							tCError.moduleName = "GetPlatformCustomerNo";
							tCError.functionName = "getPlatformCustomerNo";
							tCError.errorMessage = "系统正常，未查询到保单转入余额信息";
							mErrorInfo = tCError.errorMessage;
							saveErrorList(tEND010Response,mErrorInfo);
							this.mErrors.addOneError(tCError);
							return false;
					}
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tEND010Response,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+ResultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = CiitcResultMessage;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND010Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+ResultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = CiitcResultMessage;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND010Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND010Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData(){
		mMap.put(tPolicyTransformRemainderQuerySet, "INSERT");
		this.mInputData.add(mMap);
		return true;
	}
	/**
	 * 获取结果集的公共get方法
	 */
	public VData getResult(VData result){
		return mResult;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(END010Response tEND010Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_NAME_BATCHNO", 20);
		String TransType = tEND010Response.getHead().getTransType();
		String TransNo = tEND010Response.getHead().getTransNo();
		String responseCode = tEND010Response.getHead().getResponseCode();
		String resultStatus = tEND010Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+""+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("QueryStartDate", "2015-12-04");
		tTransferData.setNameAndValue("QueryEndDate", "2015-12-09");
		tVData.add(tTransferData);
		PolicyTransformRemainderQuery tLCContTranferInInfo = new PolicyTransformRemainderQuery();
		tLCContTranferInInfo.submitData(tVData, "ZBXPT");
	}
}
