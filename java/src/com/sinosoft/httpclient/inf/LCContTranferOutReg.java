package com.sinosoft.httpclient.inf;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.httpclient.dto.end006.response.END006Response;
import com.sinosoft.httpclient.dto.end007.request.END007Request;
import com.sinosoft.httpclient.dto.end007.request.Head;
import com.sinosoft.httpclient.dto.end007.request.RequestNode;
import com.sinosoft.httpclient.dto.end007.request.RequestNodes;
import com.sinosoft.httpclient.dto.end007.response.END007Response;
import com.sinosoft.httpclient.dto.end007.response.ResponseNode;
import com.sinosoft.httpclient.dto.end007.response.Transfer;
import com.sinosoft.httpclient.dto.end007.response.Transfer;
import com.sinosoft.httpclient.dto.end007.response.TransferList;
import com.sinosoft.httpclient.dto.end007.response.TransferList;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit; //import com.sinosoft.lis.schema.HISLCInsuredListSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LSErrorListSchema;
import com.sinosoft.lis.schema.LSTransOutInfoSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LSTransOutInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>
 * Title: LCContTranferOut
 * </p>
 * <p>
 * Description: 保单转出登记的接口类
 * </p>
 * <p>
 * Date: 2015-10-08
 * </p>
 * <p>
 * Copyright: Copyright (c) 2015
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author : Ligz
 * @version 1.0
 */
public class LCContTranferOutReg {
	/** 公共的错误类 */
	public CErrors mErrors = new CErrors();
	/** 返回结构的容器 */
	private VData mResult = new VData();
	/** 存储保存外部传入的值 */
	private VData mInputData = new VData();
	/** 携带数据的类型 */
	private TransferData tempTransferData = new TransferData();
	/** 存储外部传入的操作类型 */
	private String mOperate;
	/** 保单转出登记信息 */
	private LSTransOutInfoSet mLSTransOutInfoSet = new LSTransOutInfoSet();

	private MMap mMap = new MMap();

	/**
	 * 公共的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		/**
		 * 获取传入的值
		 */
		if (!getInputData()) {
			return false;
		}
		/**
		 * 数据处理方法
		 */
		if (!dealData()) {
			return false;
		}
		/**
		 * 准备往后台传输数据
		 */
		if (!preparedData()) {
			return false;
		}
		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start LCContTranferOut pubsubmit!!!");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContTranferOut";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end LCContTranferOut pubsubmit!!!");
		mInputData = null;
		return true;
	}

	/**
	 * 获取传入的值
	 */
	private boolean getInputData() {
		mLSTransOutInfoSet = (LSTransOutInfoSet) mInputData
				.getObjectByObjectName("LSTransOutInfoSet", 0);
		if (mLSTransOutInfoSet.size() <= 0) {
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOutREG";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "没有获取到保单转出待登记数据";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 数据处理方法
	 */
	private boolean dealData() {
		

		try {
			for (int i = 1; i <= mLSTransOutInfoSet.size(); i++) {
				END007Request tEnd007Request = new END007Request();
				RequestNode tRequestNode = new RequestNode();
				tRequestNode.setContNo(mLSTransOutInfoSet.get(i).getContNo());
				tEnd007Request.getRequestNodes().getRequestNode().add(tRequestNode);
				/** 生成xml的头部信息 */
				Head head = new Head();
				/**
				 * 调用发送请求报文的方法，用responseXml 去接收返回的响应报文
				 */
				String responseXml = CommunicateServiceImpl.post(
						tEnd007Request, mOperate);
				/**
				 * 将接收到的xml字符串解析为bean类
				 */
				END007Response tEND007Response = (END007Response) XmlParseUtil
						.xmlToDto(responseXml, END007Response.class);
				/** 获取返回的状态 */
				String tResponseCode = tEND007Response.getHead().getResponseCode();
				System.out.println("tResponseCode=="+tResponseCode);
				//根据返回的状态进行判断，0000时代表没有重大的异常
				if(null!=tResponseCode && !tResponseCode.equals("")&&"0000".equals(tResponseCode)){
//					//获取处理结果状态的值
					String tResultStatus = tEND007Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
					String tResultInfoDesc = tEND007Response.getResponseNodes().getResponseNode().getResult().getResultInfoDesc();
					String tCiitcResultCode = tEND007Response.getResponseNodes().getResponseNode().getResult().getCiitcResultCode();
					String tCiitcResultMessage = tEND007Response.getResponseNodes().getResponseNode().getResult().getCiitcResultMessage();
					if("00".equals(tResultStatus)&&"1".equals(tCiitcResultCode)){
						List<TransferList> transferList=new ArrayList<TransferList>();
						List<Transfer> transfer=new ArrayList<Transfer>();
						transferList=tEND007Response.getResponseNodes().getResponseNode().getTransferList();
						for(int j=0;j<transferList.size();j++){
							transfer =transferList.get(j).getTransfer();
							
							for(int k=0;k<transfer.size();k++){
								System.out.println(transfer.get(k).getPolicyNo());
								System.out.println(transfer.get(k).getClientList().get(0).getTransferSequenceNo());
								LSTransOutInfoSchema tLSTransOutInfoSchema=mLSTransOutInfoSet.get(i);
								tLSTransOutInfoSchema.setPlatTransNo(transfer.get(k).getClientList().get(0).getTransferSequenceNo());
								tLSTransOutInfoSchema.setRegFlag("1");
								tLSTransOutInfoSchema.setTransOutState("04");//登记待转出状态
								tLSTransOutInfoSchema.setModifyDate(PubFun.getCurrentDate());
								tLSTransOutInfoSchema.setModifyTime(PubFun.getCurrentTime());
								mMap.put(tLSTransOutInfoSchema, SysConst.DELETE_AND_INSERT);
							}
							
						}
					}else if("00".equals(tResultStatus)){
						LSTransOutInfoSchema tLSTransOutInfoSchema=mLSTransOutInfoSet.get(i);
						System.out.println("登记上报失败！原因："+tCiitcResultMessage);
						tLSTransOutInfoSchema.setErrorInfo(tCiitcResultMessage);
						tLSTransOutInfoSchema.setRegFlag("0");
						tLSTransOutInfoSchema.setModifyDate(PubFun.getCurrentDate());
						tLSTransOutInfoSchema.setModifyTime(PubFun.getCurrentTime());
						mMap.put(tLSTransOutInfoSchema, SysConst.DELETE_AND_INSERT);
					}
					
					saveErrorList(tEND007Response,mLSTransOutInfoSet.get(i).getContNo());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(END007Response mEND007Response,String mContNo){
		String ID = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
		String TransType = mEND007Response.getHead().getTransType();
		String TransNo = mEND007Response.getHead().getTransNo();
		String responseCode = mEND007Response.getHead().getResponseCode();
		String resultStatus = mEND007Response.getResponseNodes().getResponseNode().getResult().getResultStatus();
		String mErrorInfo ="";
		if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
			mErrorInfo = "系统正常，业务成功处理!";
		}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
			mErrorInfo = "系统异常!";
		}else{
			mErrorInfo = "处理失败";
		}
		LSErrorListSchema tLSErrorListSchema = new LSErrorListSchema();
		tLSErrorListSchema.setSerNo(ID);
		tLSErrorListSchema.setTransType(TransType);
		tLSErrorListSchema.setTransNo(TransNo);
		tLSErrorListSchema.setBusinessNo(mContNo);
		tLSErrorListSchema.setResponseCode(responseCode);
		tLSErrorListSchema.setResultStatus(resultStatus);
		tLSErrorListSchema.setErrorInfo(mErrorInfo);
		tLSErrorListSchema.setMakeDate(PubFun.getCurrentDate());
		tLSErrorListSchema.setMakeTime(PubFun.getCurrentTime());
		tLSErrorListSchema.setModifyDate(PubFun.getCurrentDate());
		tLSErrorListSchema.setModifyTime(PubFun.getCurrentTime());
		
		mMap.put(tLSErrorListSchema, "DELETE&INSERT");
		
	}

	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData() {
		this.mInputData.add(mMap);
		return true;
	}

	/**
	 * 获取结果集的公共get方法
	 */
	public VData getResult(VData result) {
		return mResult;
	}
}
