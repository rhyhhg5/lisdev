package com.sinosoft.httpclient.inf;

import java.util.List;

import com.sinosoft.httpclient.dto.nbu002.request.Head;
import com.sinosoft.httpclient.dto.nbu002.request.NBU002Request;
import com.sinosoft.httpclient.dto.nbu002.request.RequestNode;
import com.sinosoft.httpclient.dto.nbu002.response.NBU002Response;
import com.sinosoft.httpclient.dto.nbu002.response.ResponseNode;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:  获取客户税优识别码</p>
 * <p>Description:根据保单号获取客户税优识别码</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class GetTaxCodeNBU002 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**执行sql语句查询的类*/
	  private ExeSQL tExeSQL = new ExeSQL();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /**用于存储异步预约码*/
	  private String bookingSequenceNo;
	 
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  /**用于存储客户平台编码*/
	  private String mPTCustomerNo;
	  /**
	   * 提供无参的构造方法
	   */
	  public GetTaxCodeNBU002() {}
	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }
	    
   		if(!dealData()){
			return false;
		}
		return true;
	}
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
   		 */
   		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
   		bookingSequenceNo = (String) tempTransferData.getValueByName("BookingSequenceNo");
   		System.out.println(bookingSequenceNo);
   		if(bookingSequenceNo==null || "".equals(bookingSequenceNo)){
   			CError tCError = new CError();
   			tCError.moduleName = "GetTaxCode";
   			tCError.functionName = "getMessage";
   			tCError.errorMessage = "传入的异步预约码为空！";
   			this.mErrors.addOneError(tCError);
   			return false;
		}
   		return true;
	}
	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData(){
		/**
   		 * 封装请求报文dto
   		 * 上传保单号，从中保信获取税优识别码
   		 */
       	NBU002Request tNBU002Request = new NBU002Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setBookingSequenceNo(bookingSequenceNo);
       	tNBU002Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
			String responseXml = CommunicateServiceImpl.post(tNBU002Request,mOperate);
			/**获取客户校验平台的平台客户编码 */
			NBU002Response tNBU002Response = (NBU002Response)XmlParseUtil.xmlToDto(responseXml, NBU002Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			List<ResponseNode> responseNodeList = tNBU002Response.getResponseNodes().getResponseNode();
			String responseCode = tNBU002Response.getHead().getResponseCode();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tNBU002Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				for(ResponseNode responseNode:responseNodeList){
					String resultStatus = responseNode.getResult().getResultStatus();
//					
					/**
					 * 单号
					 */
					String mPolicyNo = responseNode.getBusinessObject().getPolicyNo();
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
						mErrorInfo = "系统正常，业务成功处理!";
						List<String> taxCodes = null;
						for (int i = 0; i < responseNode.getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().size(); i++) {
							String mTaxCode=responseNode.getBusinessObject().getPolicyOfSingleList().getPolicyOfSingle().get(i).getTaxCode();
							taxCodes.add(mTaxCode);
						}
						setTaxCodes(taxCodes);
					//	zxsql("00",mPolicyNo,null,mTaxCode);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
						System.out.println("系统正常，等待异步返回!");
						mErrorInfo = "系统正常，等待异步返回!";
						CError tCError = new CError();
						tCError.moduleName = "GetTaxCode";
						tCError.functionName = "getTaxCode";
						tCError.errorMessage = "系统正常，等待异步返回!";
						this.mErrors.addOneError(tCError);
					//	zxsql("00",mPolicyNo,null,mTaxCode);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
						System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
						CError tCError = new CError();
						tCError.moduleName = "GetTaxCode";
						tCError.functionName = "getTaxCode";
						tCError.errorMessage = resultInfoDesc;
						mErrorInfo = tCError.errorMessage;
						this.mErrors.addOneError(tCError);
				//		zxsql("99",mPolicyNo,mErrorInfo,mTaxCode);
						return false;
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
						System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
						CError tCError = new CError();
						tCError.moduleName = "GetTaxCode";
						tCError.functionName = "getTaxCode";
						tCError.errorMessage = resultInfoDesc;
						mErrorInfo = tCError.errorMessage;
						this.mErrors.addOneError(tCError);
					//	zxsql("99",mPolicyNo,mErrorInfo,mTaxCode);
						return false;
					}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
						System.out.println("系统异常！");
						CError tCError = new CError();
						tCError.moduleName = "GetTaxCode";
						tCError.functionName = "getTaxCode";
						tCError.errorMessage = "系统异常!";
						mErrorInfo = tCError.errorMessage;
						this.mErrors.addOneError(tCError);
					//	zxsql("99",mPolicyNo,mErrorInfo,mTaxCode);
						return false;
					}
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "GetTaxCode";
			tCError.functionName = "getTaxCode";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/**
	 * 将税优识别码放入到结果集中,通过TaxCode会获得识别码集合
	 */
	public boolean setTaxCodes(List<String> mTaxCode){
		
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("TaxCode", mTaxCode);
		System.out.println(mTaxCode);
		mResult.add(tTransferData);
		return true;
	}
	/**
	 * 获取结果集，包含多个识别码
	 * @param args
	 */
	
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BookingSequenceNo", "13032593126");
		tVData.add(tTransferData);
		GetTaxCodeNBU002 tGetNewResult= new GetTaxCodeNBU002();
		tGetNewResult.submitData(tVData, "ZBXPT");
	}
}
