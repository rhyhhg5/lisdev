package com.sinosoft.httpclient.inf;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.httpclient.dto.chk001.request.CHK001Request;
import com.sinosoft.httpclient.dto.chk001.request.Head;
import com.sinosoft.httpclient.dto.chk001.request.RequestNode;
import com.sinosoft.httpclient.dto.chk001.response.BusinessObject;
import com.sinosoft.httpclient.dto.chk001.response.CHK001Response;
import com.sinosoft.httpclient.dto.chk001.response.Detail;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.BusinessCheckSchema;
import com.sinosoft.lis.vschema.BusinessCheckSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: LCContTranferOut </p>
 * <p>Description: 税优信息查询End009</p>
 * <p>Date: 2016-10-03 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : a
 * @version 1.0
 */
public class NewSYQueryDealBL {
	/**公共的错误类*/
	public CErrors mErrors = new CErrors();
	/**返回结构的容器*/
	private VData mResult = new VData();
	/**存储保存外部传入的值*/
	private VData mInputData = new VData();
	/**携带数据的类型*/
	private TransferData tempTransferData = new TransferData();
	/**存储外部传入的操作类型*/
	private String mOperate;
	/**获取数据的开始日期*/
	private String mCheckDate;
	/**存储交互错误信息*/
	private String mErrorInfo ;
	
	private MMap mMap = new MMap();
	
	private BusinessCheckSchema tBusinessCheckSchema = null;
	private BusinessCheckSet tBusinessCheckSet = null;
	
	/**
	 * 公共的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		/**
		 * 获取传入的值
		 */
		if(!getInputData(mInputData)){
			return false;
		}
		/**
		 * 数据处理方法
		 */
		if(!dealData()){
			return false;
		}
		/**
		 * 准备往后台传输数据
		 */
		if(!preparedData()){
			return false;
		}
		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start BusinessCheck pubsubmit!!!");
		if(!tPubSubmit.submitData(mInputData, mOperate)){
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BusinessCheck";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end BusinessCheck pubsubmit!!!");
		mInputData = null;
		return true;
	}
	/**
	 * 获取传入的值
	 */
	private boolean getInputData(VData tInputData){
		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mCheckDate = (String) tempTransferData.getValueByName("CheckDate");
		if(mCheckDate==null||mCheckDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "BusinessCheck";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询起始日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/**
	 * 数据处理方法
	 */
	private boolean dealData(){
		CHK001Request tCHK001Request = new CHK001Request();
		RequestNode tRequestNode = new RequestNode();
		//查询税优的总个数 
		ExeSQL tExeSQL = new ExeSQL();
		String totalSQL="select count(1) from lserrorlist where  makedate ='"+mCheckDate+"' and transtype in " +
				"('NBU001','NBU003','END001','END002','END003','END005','END007','PRM001','RNW001','RNW003','CLM001','CLM003','ACT001') with ur";
		String tTotalNum=tExeSQL.getOneValue(totalSQL);
		//成功个数
		String successSQL="select count(1) from lserrorlist where  makedate ='"+mCheckDate+"' and resultstatus ='00' and transtype in" +
				"('NBU001','NBU003','END001','END002','END003','END005','END007','PRM001','RNW001','RNW003','CLM001','CLM003','ACT001') with ur";
		String tSuccessNum=tExeSQL.getOneValue(successSQL);
		//失败个数
		String failSQL="select "+tTotalNum+" - "+tSuccessNum+" from dual where 1=1 with ur";
		String tFailNum=tExeSQL.getOneValue(failSQL);
		
		tRequestNode.setCheckDate(mCheckDate);
		tRequestNode.setRequestTotalNum(tTotalNum);
		tRequestNode.setComSuccessNum(tSuccessNum);
		tRequestNode.setComFailNum(tFailNum);
		tCHK001Request.getRequestNodes().getRequestNode().add(tRequestNode);
		/**生成xml的头部信息*/
		Head head = new Head();
		
		try {
			/**
			 * 调用发送请求报文的方法，用responseXml 去接收返回的响应报文
			 */
			String responseXml = CommunicateServiceImpl.post(tCHK001Request, mOperate);
			/**
			 * 将接收到的xml字符串解析为bean类
			 */
			CHK001Response tCHK001Response = (CHK001Response) XmlParseUtil.xmlToDto(responseXml, CHK001Response.class);
			/**获取返回的状态*/
			String ResponseCode = tCHK001Response.getHead().getResponseCode();
			//获取处理结果状态的值
			String ResultStatus = tCHK001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			String ResultInfoDesc = tCHK001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			String CiitcResultCode = tCHK001Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultCode();
			String CiitcResultMessage = tCHK001Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultMessage();
			
			BusinessObject tBusinessObject = null;
			List<Detail> tDetail = new ArrayList<Detail>();
			//根据返回的状态进行判断，0000时代表没有重大的异常
			if(ResponseCode!=null&&!ResponseCode.equals("")&&"0000".equals(ResponseCode)){
				
				System.out.println("tResultStatus--"+ResultStatus);
				System.out.println("tResultInfoDesc--"+ResultInfoDesc);
				System.out.println("tCiitcResultCode--"+CiitcResultCode);
				System.out.println("tCiitcResultMessage--"+CiitcResultMessage);
					
				if("00".equals(ResultStatus)&&"1".equals(CiitcResultCode)){
					
					//将基本信息进行获取并存储
					tBusinessObject = tCHK001Response.getResponseNodes().getResponseNode().get(0).getBusinessObject();
					String mCompanyCode = tBusinessObject.getCompanyCode();
					String mCheckDate = tBusinessObject.getCheckDate();
					String mTotalNum = tBusinessObject.getTotalNum();
					String mSuccessNum = tBusinessObject.getSuccessNum();
					String mFailNum = tBusinessObject.getFailNum();
					
					tDetail = tBusinessObject.getDetail();
					System.out.println("tDetail---"+tDetail.size());
					if(tDetail.size()>0){
						new ExeSQL().execUpdateSQL("delete from BusinessCheck where 1=1 and CheckDate = '"+mCheckDate+"'");
						tBusinessCheckSet = new BusinessCheckSet();
						for(int i=0;i<tDetail.size();i++){
							tBusinessCheckSchema = new BusinessCheckSchema();
							//获取返回的保单号
							String mCheckTransType = tDetail.get(i).getCheckTransType();
							String mTransDate = tDetail.get(i).getTransDate();
							String mPolicyNo = tDetail.get(i).getPolicyNo();
							String mSequenceNo = tDetail.get(i).getSequenceNo();
							String mEndorsementNo = tDetail.get(i).getEndorsementNo();
							String mClaimNo = tDetail.get(i).getClaimNo();
							String mFeeId = tDetail.get(i).getFeeId();
							String mFeeStatus = tDetail.get(i).getFeeStatus();
							String mRenewalEndorsementNo = tDetail.get(i).getRenewalEndorsementNo();
							String mComFeeId = tDetail.get(i).getComFeeId();
							String mReturnCode = tDetail.get(i).getReturnCode();
							String mErrorMessage = tDetail.get(i).getErrorMessage();
							if(mErrorMessage!=null && mErrorMessage != ""){
								mErrorMessage = mErrorMessage.replaceAll("\\|", "-");
								if(mErrorMessage.length()>600){
									mErrorMessage = mErrorMessage.substring(0, 600);
								}
							}
							tBusinessCheckSchema.setSerNo(PubFun1.CreateMaxNo("SEQ_TPH_Busines", 20));
							tBusinessCheckSchema.setCompanyCode(mCompanyCode);
							tBusinessCheckSchema.setCheckDate(mCheckDate);
							tBusinessCheckSchema.setTotalNum(mTotalNum);
							tBusinessCheckSchema.setSuccessNum(mSuccessNum);
							tBusinessCheckSchema.setFailNum(mFailNum);
							tBusinessCheckSchema.setCheckTransType(mCheckTransType);
							tBusinessCheckSchema.setTransDate(mTransDate);
							tBusinessCheckSchema.setPolicyNo(mPolicyNo);
							tBusinessCheckSchema.setSequenceNo(mSequenceNo);
							tBusinessCheckSchema.setEndorsementNo(mEndorsementNo);
							tBusinessCheckSchema.setClaimNo(mClaimNo);
							tBusinessCheckSchema.setFeeId(mFeeId);
							tBusinessCheckSchema.setFeeStatus(mFeeStatus);
							tBusinessCheckSchema.setRenewalEndorsementNo(mRenewalEndorsementNo);
							tBusinessCheckSchema.setComFeeId(mComFeeId);
							tBusinessCheckSchema.setReturnCode(mReturnCode);
							tBusinessCheckSchema.setErrorMessage(mErrorMessage);
							tBusinessCheckSchema.setResultStatus(ResultStatus);
							tBusinessCheckSchema.setResultInfoDesc(ResultInfoDesc);
							tBusinessCheckSchema.setCiitcResultCode(CiitcResultCode);
							tBusinessCheckSchema.setCiitcResultMessage(CiitcResultMessage);
							tBusinessCheckSchema.setMakeDate(PubFun.getCurrentDate());
							tBusinessCheckSchema.setMakeTime(PubFun.getCurrentTime());
							tBusinessCheckSchema.setModifyDate(PubFun.getCurrentDate());
							tBusinessCheckSchema.setModifyTime(PubFun.getCurrentTime());
							tBusinessCheckSet.add(tBusinessCheckSchema);
						}
					}else{
							System.out.println("系统正常！未查询到交易核对信息");
							CError tCError = new CError();
							tCError.moduleName = "BusinessCheck";
							tCError.functionName = "BusinessCheck";
							tCError.errorMessage = "系统正常，当天没有交易信息";
							mErrorInfo = tCError.errorMessage;
							saveErrorList(tCHK001Response,mErrorInfo);
							this.mErrors.addOneError(tCError);
							return false;
					}
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tCHK001Response,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "BusinessCheck";
					tCError.functionName = "BusinessCheck";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+ResultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "BusinessCheck";
					tCError.functionName = "BusinessCheck";
					tCError.errorMessage = CiitcResultMessage;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tCHK001Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+ResultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "BusinessCheck";
					tCError.functionName = "BusinessCheck";
					tCError.errorMessage = CiitcResultMessage;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tCHK001Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "BusinessCheck";
					tCError.functionName = "BusinessCheck";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tCHK001Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData(){
		mMap.put(tBusinessCheckSet, "INSERT");
		this.mInputData.add(mMap);
		return true;
	}
	/**
	 * 获取结果集的公共get方法
	 */
	public VData getResult(VData result){
		return mResult;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(CHK001Response tCHK001Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);  
		String TransType = tCHK001Response.getHead().getTransType();
		String TransNo = tCHK001Response.getHead().getTransNo();
		String responseCode = tCHK001Response.getHead().getResponseCode();
		String resultStatus = tCHK001Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+""+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CheckDate", "2015-12-15");
		tVData.add(tTransferData);
		BusinessCheck tBusinessCheck = new BusinessCheck();
		tBusinessCheck.submitData(tVData, "ZBXPT");
	}

}
