package com.sinosoft.httpclient.inf;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.httpclient.dto.end008.request.END008Request;
import com.sinosoft.httpclient.dto.end008.request.Head;
import com.sinosoft.httpclient.dto.end008.request.RequestNode;
import com.sinosoft.httpclient.dto.end008.response.Client;
import com.sinosoft.httpclient.dto.end008.response.END008Response;
import com.sinosoft.httpclient.dto.end008.response.ResponseNode;
import com.sinosoft.httpclient.dto.end008.response.Result;
import com.sinosoft.httpclient.dto.end008.response.Transfer;
import com.sinosoft.httpclient.dto.end008.response.TransferList;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSTransOutInfoSchema;
import com.sinosoft.lis.vschema.LSTransOutInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetXBInfoAsynEND008 {
	/** 公共的错误类 */
	public CErrors mErrors = new CErrors();
	/** 返回结构的容器 */
	private VData mResult = new VData();
	/** 存储保存外部传入的值 */
	private VData mInputData = new VData();
	/** 携带数据的类型 */
	private TransferData tempTransferData = new TransferData();
	/** 存储外部传入的操作类型 */
	private String mOperate;
	/** 获取数据的开始日期 */
	private String mQueryStartDate;
	/** 获取数据的结束日期 */
	private String mQueryEndDate;
	/** 用于存储异步预约码 */
	private String mBookingSequenceNo;
	/** 存储交互错误信息 */
	private String mErrorInfo;
	/** 执行sql语句查询的类 */
	private ExeSQL tExeSQL = new ExeSQL();

	private MMap mMap = new MMap();

	/**
	 * 公共的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		/**
		 * 获取传入的值
		 */
		if (!getInputData()) {
			return false;
		}
		/**
		 * 数据处理方法
		 */
		if (!dealData()) {
			return false;
		}
		/**
		 * 准备往后台传输数据
		 */
		if (!preparedData()) {
			return false;
		}
		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start GetXBInfoAsynEND008 pubsubmit!!!");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GetXBInfoAsynEND008";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end GetXBInfoAsynEND008 pubsubmit!!!");
		mInputData = null;
		return true;
	}

	/**
	 * 获取传入的值
	 */
	private boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mBookingSequenceNo = (String) tempTransferData
				.getValueByName("BookingSequenceNo");
		System.out.println(mBookingSequenceNo);
		if (mBookingSequenceNo == null || "".equals(mBookingSequenceNo)) {
			CError tCError = new CError();
			tCError.moduleName = "GetXBInfoAsynEND008";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的异步预约码为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 数据处理方法
	 */
	private boolean dealData() {
		try {
			END008Request tEnd008Request = new END008Request();
			RequestNode tRequestNode = new RequestNode();
			tRequestNode.setBookingSequenceNo(mBookingSequenceNo);
			tEnd008Request.getRequestNodes().getRequestNode().add(tRequestNode);
			/** 生成ml的头部信息 */
			Head head = new Head();
			/**
			 * 调用发送请求报文的方法，用responseXml 去接收返回的响应报文
			 */
			String responseXml = CommunicateServiceImpl.post(tEnd008Request,mOperate);
			/******************************** 响应开始返回 *************************************/

			/**
			 * 将接收到的xml字符串解析为bean类
			 */
			END008Response tEND008Response = (END008Response) XmlParseUtil.xmlToDto(responseXml, END008Response.class);
			/** 获取返回的状态 */
			String tResponseCode = tEND008Response.getHead().getResponseCode();
			System.out.println("tResponseCode==" + tResponseCode);

			ResponseNode responseNode = tEND008Response.getResponseNodes().getResponseNode();
			//根据返回的状态进行判断，0000时代表没有重大的异常
			if(tResponseCode!=null && !"".equals(tResponseCode)  && "0000".equals(tResponseCode)){
//				//获取处理结果状态的值
				String tResultStatus = responseNode.getResult().getResultStatus();
				String tResultInfoDesc = responseNode.getResult().getResultInfoDesc();
				String tCiitcResultCode = responseNode.getResult().getCiitcResultCode();
				String tCiitcResultMessage = responseNode.getResult().getCiitcResultMessage();
				boolean bool=false;
				if("00".equals(tResultStatus)&&"1".equals(tCiitcResultCode)){
					List<TransferList> transferList=new ArrayList<TransferList>();
					List<Transfer> transfer=new ArrayList<Transfer>();
					transferList=responseNode.getTransferList();
					try {
					
					for(int j=0;j<transferList.size();j++){
						
						transfer =transferList.get(j).getTransfer();
						
						List<Client> clients=new ArrayList<Client> ();
						for(int k=0;k<transfer.size();k++){
							System.out.println("保单号:"+transfer.get(k).getPolicyNo());
							clients=transfer.get(k).getClientList();
							for (int l = 0; l < clients.size(); l++) {
								System.out.println("保单转出登记编码："+clients.get(l).getTransferSequenceNo());
								LSTransOutInfoSchema tLSTransOutInfoSchema=new LSTransOutInfoSchema();
								//保单转出登记编码  
								tLSTransOutInfoSchema.setPlatTransNo(clients.get(l).getTransferSequenceNo());
								tLSTransOutInfoSchema.setRegFlag("1");
								tLSTransOutInfoSchema.setTransOutState("04");//登记待转出状态
								tLSTransOutInfoSchema.setModifyDate(PubFun.getCurrentDate());
								tLSTransOutInfoSchema.setModifyTime(PubFun.getCurrentTime());
								mMap.put(tLSTransOutInfoSchema, SysConst.DELETE_AND_INSERT);
								bool=true;
							}
						}
					}
					} catch (Exception e) {
						bool=false;
						mErrorInfo =  "处理异常!";
						CError tCError = new CError();
						tCError.moduleName = "GetXBInfoAsynEnd008";
						tCError.functionName = "getXBInfoAsynEnd008";
						tCError.errorMessage = "处理异常!";
						this.mErrors.addOneError(tCError);
						return false;
						// TODO: handle exception
					}
				}else if("00".equals(tResultStatus)&&!"1".equals(tCiitcResultCode)){
					bool=false;
					LSTransOutInfoSchema tLSTransOutInfoSchema= new LSTransOutInfoSchema();
					System.out.println("登记上报失败！原因："+tCiitcResultMessage);
					tLSTransOutInfoSchema.setErrorInfo(tCiitcResultMessage);
					tLSTransOutInfoSchema.setRegFlag("0");
					tLSTransOutInfoSchema.setModifyDate(PubFun.getCurrentDate());
					tLSTransOutInfoSchema.setModifyTime(PubFun.getCurrentTime());
					mMap.put(tLSTransOutInfoSchema, SysConst.DELETE_AND_INSERT);
				//
					mErrorInfo =  tCiitcResultMessage;
					CError tCError = new CError();
					tCError.moduleName = "GetXBInfoAsynEnd008";
					tCError.functionName = "getXBInfoAsynEnd008";
					tCError.errorMessage =  tCiitcResultMessage;
					this.mErrors.addOneError(tCError);
					
				} else if("03".equals(tResultStatus)&&"".equals(tCiitcResultCode)&&!"".equals(tResultInfoDesc)){
				System.out.println("系统正常，业务处理失败!原因为："+tResultInfoDesc);
				CError tCError = new CError();
				tCError.moduleName = "GetXBInfoAsynEnd008";
				tCError.functionName = "getXBInfoAsynEnd008";
				tCError.errorMessage = tResultInfoDesc;
				mErrorInfo = tCError.errorMessage;
				this.mErrors.addOneError(tCError);
				return false;
				}
				if(bool){
					mErrorInfo = "系统正常";
					CError tCError = new CError();
					tCError.moduleName = "GetXBInfoAsynEnd008";
					tCError.functionName = "getXBInfoAsynEnd008";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}
			}else{
				mErrorInfo =  "出现重大异常";
				CError tCError = new CError();
				tCError.moduleName = "GetXBInfoAsynEnd008";
				tCError.functionName = "getXBInfoAsynEnd008";
				tCError.errorMessage =  "出现重大异常";
				this.mErrors.addOneError(tCError);
			}
			} catch (Exception e) {
			System.out.println("抛出异常");
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData() {
		this.mInputData.add(mMap);
		return true;
	}

	/**
	 * 获取结果集的公共get方法
	 */
	public VData getResult(VData result) {
		return mResult;
	}

	/**
	 * 测试用
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BookingSequenceNo", "32019055947");
		tVData.add(tTransferData);
		GetXBInfoAsynEND008 tGetXBInfoAsynEND008 = new GetXBInfoAsynEND008();
		tGetXBInfoAsynEND008.submitData(tVData, "ZBXPT");
	}
}
