package com.sinosoft.httpclient.inf;

import com.sinosoft.httpclient.dto.pty004.request.PTY004Request;
import com.sinosoft.httpclient.dto.pty004.request.RequestNode;
import com.sinosoft.httpclient.dto.pty004.response.PTY004Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 既往理赔信息查询
 * </p>
 * <p>
 * Description:根据保单号获取既往理赔信息查询
 * </p>
 * <p>
 * Copyright: Copyright (c) 2015
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author : Wn
 * @version 1.0
 */
public class AlwaysClaimQuery {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 执行sql语句查询的类 */
	private ExeSQL tExeSQL = new ExeSQL();

	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 用于存储传入的保单号 */
	private String mContNo;

	/** 用于存储既往理赔信息 */
	private String mResult;

	/**
	 * 提供无参的构造方法
	 */
	public AlwaysClaimQuery() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mContNo = (String) tempTransferData.getValueByName("ContNo");
		System.out.println(mContNo);
		if (mContNo == null && mContNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "AlwaysClaimQuery";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的投保单号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData() {
		/**
		 * 封装请求报文dto 上传保单号，从中保信获取税优识别码
		 */
		LCAppntDB mLCAppntDB = new LCAppntDB();
		mLCAppntDB.setContNo(mContNo);
		if (!mLCAppntDB.getInfo()) {
			CError tCError = new CError();
			tCError.moduleName = "AlwaysClaimQuery";
			tCError.functionName = "dealData";
			tCError.errorMessage = "投保人信息查询失败！";
			this.mErrors.addOneError(tCError);
			return false;
		}

		LCContSubDB mLCContSubDB = new LCContSubDB();
		mLCContSubDB.setPrtNo(mLCAppntDB.getPrtNo());
		if (!mLCContSubDB.getInfo()) {
			CError tCError = new CError();
			tCError.moduleName = "AlwaysClaimQuery";
			tCError.functionName = "dealData";
			tCError.errorMessage = "税优信息查询失败！";
			this.mErrors.addOneError(tCError);
			return false;
		}

		// String birthday = tExeSQL.execSQL(
		// "select to_char('" + mLCAppntDB.getAppntBirthday()
		// + "','yyyy-mm-dd') from dual where 1=1 ").GetText(1, 1);

		PTY004Request tPTY004Request = new PTY004Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setBusiNo("1");
		tRequestNode.setName(mLCAppntDB.getAppntName());
		tRequestNode.setGender(mLCAppntDB.getAppntSex());
		tRequestNode.setCertiType(mLCAppntDB.getIDType());
		tRequestNode.setCertiNo(mLCAppntDB.getIDNo());
		tRequestNode.setBirthday(mLCAppntDB.getAppntBirthday());
		tRequestNode.setPolicySource(mLCContSubDB.getTransFlag());
		tRequestNode.setProposalNo(mLCAppntDB.getPrtNo());
		tPTY004Request.getRequestNodes().getRequestNode().add(tRequestNode);
		/** 生成xml的头部信息 */
		try {
			String responseXml = CommunicateServiceImpl.post(tPTY004Request,
					mOperate);
			/** 获取结果 */
			PTY004Response tPTY004Response = (PTY004Response) XmlParseUtil
					.xmlToDto(responseXml, PTY004Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tPTY004Response.getHead().getResponseCode();
			String resultStatus = tPTY004Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tPTY004Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultInfoDesc();
			String ciitcResultMessage = tPTY004Response.getResponseNodes()
					.getResponseNode().get(0).getResult()
					.getCiitcResultMessage();

			if (responseCode != null && !responseCode.equals("")
					&& responseCode.equals("0000")) {
				if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("00")) {
					System.out.println("系统正常，业务成功处理！");

					mResult = tPTY004Response.getResponseNodes()
							.getResponseNode().get(0).getBusinessObject()
							.getSa()
							+ "#"
							+ tPTY004Response.getResponseNodes()
									.getResponseNode().get(0)
									.getBusinessObject().getAnnualClaimSa()
							+ "#"
							+ tPTY004Response.getResponseNodes()
									.getResponseNode().get(0)
									.getBusinessObject().getWholeLifeClaimSa();
					System.out.println("税优既往理赔结果："+mResult);
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("01")) {
					System.out.println("系统正常，等待异步返回!");
					CError tCError = new CError();
					tCError.moduleName = "AlwaysClaimQuery";
					tCError.functionName = "AlwaysClaimQuery";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("02")) {
					System.out
							.println("系统正常，数据校验不通过!原因为：" + ciitcResultMessage);
					CError tCError = new CError();
					tCError.moduleName = "AlwaysClaimQuery";
					tCError.functionName = "AlwaysClaimQuery";
					tCError.errorMessage = "系统正常，数据校验不通过!原因为："
							+ ciitcResultMessage;
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("03")) {
					System.out.println("系统正常，业务处理失败!原因为：" + ciitcResultMessage);
					CError tCError = new CError();
					tCError.moduleName = "AlwaysClaimQuery";
					tCError.functionName = "AlwaysClaimQuery";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："
							+ ciitcResultMessage;
					this.mErrors.addOneError(tCError);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("99")) {
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "AlwaysClaimQuery";
					tCError.functionName = "AlwaysClaimQuery";
					tCError.errorMessage = "系统正常，业务处理失败!原因为："
							+ ciitcResultMessage;
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "AlwaysClaimQuery";
			tCError.functionName = "AlwaysClaimQuery";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取结果集
	 * 
	 * @param args
	 */
	public String getResult() {
		return mResult;
	}

	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "014059968000002");
		tVData.add(tTransferData);
		AlwaysClaimQuery tAlwaysClaimQuery = new AlwaysClaimQuery();
		tAlwaysClaimQuery.submitData(tVData, "ZBXPT");
		System.out.println(tAlwaysClaimQuery.getResult());

	}
}
