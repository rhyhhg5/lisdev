package com.sinosoft.httpclient.inf;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.httpclient.dto.end009.request.END009Request;
import com.sinosoft.httpclient.dto.end009.request.Head;
import com.sinosoft.httpclient.dto.end009.request.RequestNode;
import com.sinosoft.httpclient.dto.end009.response.*;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LSTransInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSTransInfoSchema;
import com.sinosoft.lis.schema.PolicyTransOutRegisterQuerySchema;
import com.sinosoft.lis.vschema.LSTransInfoSet;
import com.sinosoft.lis.vschema.PolicyTransOutRegisterQuerySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 
 * <p>Title: PolicyTransOutRegisterQuery </p>
 * <p>Description: 保单转出登记信息查询End009</p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class PolicyTransOutRegisterQuery {
	/**公共的错误类*/
	public CErrors mErrors = new CErrors();
	/**返回结构的容器*/
	private VData mResult = new VData();
	/**存储保存外部传入的值*/
	private VData mInputData = new VData();
	/**携带数据的类型*/
	private TransferData tempTransferData = new TransferData();
	/**存储外部传入的操作类型*/
	private String mOperate;
	/**获取数据的开始日期*/
	private String mQueryStartDate;
	/**获取数据的结束日期*/
	private String mQueryEndDate;
	/**存储交互错误信息*/
	private String mErrorInfo ;
	
	private MMap mMap = new MMap();
	
	private PolicyTransOutRegisterQuerySchema tPolicyTransOutRegisterQuerySchema = null;
	private PolicyTransOutRegisterQuerySet tPolicyTransOutRegisterQuerySet = null;
	
	/**
	 * 公共的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		/**
		 * 获取传入的值
		 */
		if(!getInputData()){
			return false;
		}
		/**
		 * 数据处理方法
		 */
		if(!dealData()){
			return false;
		}
		/**
		 * 准备往后台传输数据
		 */
		if(!preparedData()){
			return false;
		}
		/**
		 * 插入数据库
		 */
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("start PolicyTransOutRegisterQuery pubsubmit!!!");
		if(!tPubSubmit.submitData(mInputData, mOperate)){
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCContTranferOut";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("end PolicyTransOutRegisterQuery pubsubmit!!!");
		mInputData = null;
		return true;
	}
	/**
	 * 获取传入的值
	 */
	private boolean getInputData(){
		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		mQueryStartDate = (String) tempTransferData.getValueByName("QueryStartDate");
		mQueryEndDate = (String) tempTransferData.getValueByName("QueryEndDate");
		if(mQueryStartDate==null||mQueryStartDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOut";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询起始日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		if(mQueryEndDate==null||mQueryEndDate.equals("")){
			CError tCError = new CError();
			tCError.moduleName = "LCContTranferOut";
			tCError.functionName = "getInputData";
			tCError.errorMessage = "传入的查询终止日期为空！！！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/**
	 * 数据处理方法
	 */
	private boolean dealData(){
		END009Request tEND009Request = new END009Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setQueryStartDate(mQueryStartDate);
		tRequestNode.setQueryEndDate(mQueryEndDate);
		System.out.println("mQueryStartDate---"+tRequestNode.getQueryStartDate());
		tEND009Request.getRequestNodes().getRequestNode().add(tRequestNode);
		System.out.println("mQueryEndDate---"+tRequestNode.getQueryEndDate());
		/**生成xml的头部信息*/
		Head head = new Head();
		
		try {
			/**
			 * 调用发送请求报文的方法，用responseXml 去接收返回的响应报文
			 */
			String responseXml = CommunicateServiceImpl.post(tEND009Request, mOperate);
			/**
			 * 将接收到的xml字符串解析为bean类
			 */
			END009Response tEND009Response = (END009Response) XmlParseUtil.xmlToDto(responseXml, END009Response.class);
			/**获取返回的状态*/
			String ResponseCode = tEND009Response.getHead().getResponseCode();
			//获取处理结果状态的值
			String ResultStatus = tEND009Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
			String ResultInfoDesc = tEND009Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			String CiitcResultCode = tEND009Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultCode();
			String CiitcResultMessage = tEND009Response.getResponseNodes().getResponseNode().get(0).getResult().getCiitcResultMessage();
			
			List<TransferOut> tTransferOut = new ArrayList<TransferOut>();
			List<Client> tClientList = new ArrayList<Client>();
			//根据返回的状态进行判断，0000时代表没有重大的异常
			if(ResponseCode!=null&&!ResponseCode.equals("")&&"0000".equals(ResponseCode)){
				
				System.out.println("tResultStatus--"+ResultStatus);
				System.out.println("tResultInfoDesc--"+ResultInfoDesc);
				System.out.println("tCiitcResultCode--"+CiitcResultCode);
				System.out.println("tCiitcResultMessage--"+CiitcResultMessage);
					
				if("00".equals(ResultStatus)&&"1".equals(CiitcResultCode)){
					tTransferOut = tEND009Response.getResponseNodes().getResponseNode().get(0).getTransferOutList().getTransferOut();
					System.out.println("tTransferOut---"+tTransferOut.size());
					if(tTransferOut.size()>0){
						
						tPolicyTransOutRegisterQuerySet = new PolicyTransOutRegisterQuerySet();
						
						for(int i=0;i<tTransferOut.size();i++){
							tPolicyTransOutRegisterQuerySchema = new PolicyTransOutRegisterQuerySchema();
							
							//获取返回的保单号
							String mPolicyNo = tTransferOut.get(i).getPolicyNo();
							tClientList = tTransferOut.get(i).getClientList().getClient();
							for(int j=0;j<tClientList.size();j++){
								String mSequenceNo = tClientList.get(j).getSequenceNo();
								String mRegisterDate = tClientList.get(j).getRegisterDate();
								String mExpectedTerminiateDate = tClientList.get(j).getExpectedTerminiateDate();
								String mRejectReson = tClientList.get(j).getRejectReson();
								String mContactName = tClientList.get(j).getContactName();
								String mContactTele = tClientList.get(j).getContactTele();
								String mContactEmail = tClientList.get(j).getContactEmail();
								String mTransReceivDate = tClientList.get(j).getTransReceivDate();
								tPolicyTransOutRegisterQuerySchema.setSerNo(PubFun1.CreateMaxNo("SEQ_NAME_Register", 20));
								tPolicyTransOutRegisterQuerySchema.setPolicyNo(mPolicyNo);
								tPolicyTransOutRegisterQuerySchema.setSequenceNo(mSequenceNo);
								tPolicyTransOutRegisterQuerySchema.setRegisterDate(mRegisterDate);
								tPolicyTransOutRegisterQuerySchema.setExpectedTerminiateDate(mExpectedTerminiateDate);
								tPolicyTransOutRegisterQuerySchema.setRejectReson(mRejectReson);
								tPolicyTransOutRegisterQuerySchema.setContactName(mContactName);
								tPolicyTransOutRegisterQuerySchema.setContactTele(mContactTele);
								tPolicyTransOutRegisterQuerySchema.setContactEmail(mContactEmail);
								tPolicyTransOutRegisterQuerySchema.setTransReceivDate(mTransReceivDate);
								tPolicyTransOutRegisterQuerySchema.setResponseCode(ResponseCode);
								tPolicyTransOutRegisterQuerySchema.setResultStatus(ResultStatus);
								tPolicyTransOutRegisterQuerySchema.setResultInfoDesc(ResultInfoDesc);
								tPolicyTransOutRegisterQuerySchema.setCiitcResultCode(CiitcResultCode);
								tPolicyTransOutRegisterQuerySchema.setCiitcResultMessage(CiitcResultMessage);
								tPolicyTransOutRegisterQuerySchema.setMakeDate(PubFun.getCurrentDate());
								tPolicyTransOutRegisterQuerySchema.setMakeTime(PubFun.getCurrentTime());
								tPolicyTransOutRegisterQuerySchema.setModifyDate(PubFun.getCurrentDate());
								tPolicyTransOutRegisterQuerySchema.setModifyTime(PubFun.getCurrentTime());
								tPolicyTransOutRegisterQuerySet.add(tPolicyTransOutRegisterQuerySchema);
								//将预计终止日期、拒绝转出原因存储到契约 转入申请表lstransinfo
								LSTransInfoSchema tLSTransInfoSchema = new LSTransInfoSchema();
								LSTransInfoDB tLSTransInfoDB = new LSTransInfoDB();
//								tLSTransInfoDB.setTransGrpContNo(mPolicyNo);
//								tLSTransInfoDB.setTransContNo(mSequenceNo);
//								LSTransInfoSet tLSTransInifoSet = tLSTransInfoDB.query();
								LSTransInfoSet tLSTransInifoSet = tLSTransInfoDB.executeQuery("select * from LSTransInfo where TransGrpContNo = '"+mPolicyNo+"' and TransContNo = '"+mSequenceNo+"' and receiveddate is not null ");
								if(tLSTransInifoSet !=null && tLSTransInifoSet.size()>0){
									tLSTransInfoSchema = tLSTransInifoSet.get(1).getSchema();
									String receiveddate = tLSTransInfoSchema.getReceivedDate();
									if(PubFun.calInterval(receiveddate, mTransReceivDate, "D")>14){
										tLSTransInfoSchema.setRejectReason("中保信未回复");
										tLSTransInfoSchema.setSuccFlag("02");
										tLSTransInfoSchema.setModifyDate(PubFun.getCurrentDate());
										tLSTransInfoSchema.setModifyTime(PubFun.getCurrentTime());
									}else{
										if(mExpectedTerminiateDate!=null && !mExpectedTerminiateDate.equals("")){
											tLSTransInfoSchema.setExpectedDate(mExpectedTerminiateDate);
											tLSTransInfoSchema.setSuccFlag("01");
											tLSTransInfoSchema.setModifyDate(PubFun.getCurrentDate());
											tLSTransInfoSchema.setModifyTime(PubFun.getCurrentTime());
										}else{
											tLSTransInfoSchema.setRejectReason(mRejectReson);
											tLSTransInfoSchema.setSuccFlag("02");
											tLSTransInfoSchema.setModifyDate(PubFun.getCurrentDate());
											tLSTransInfoSchema.setModifyTime(PubFun.getCurrentTime());
										}
									}
									mMap.put(tLSTransInfoSchema, "UPDATE");
								}
							}
						}
					}else{
							System.out.println("系统正常！");
							CError tCError = new CError();
							tCError.moduleName = "GetPlatformCustomerNo";
							tCError.functionName = "getPlatformCustomerNo";
							tCError.errorMessage = "系统正常，未查询到保单转出登记信息";
							mErrorInfo = tCError.errorMessage;
							saveErrorList(tEND009Response,mErrorInfo);
							this.mErrors.addOneError(tCError);
							return false;
					}
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tEND009Response,mErrorInfo);
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+ResultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = CiitcResultMessage;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND009Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+ResultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = CiitcResultMessage;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND009Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(ResultStatus!=null&&!ResultStatus.equals("")&&ResultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "GetPlatformCustomerNo";
					tCError.functionName = "getPlatformCustomerNo";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tEND009Response,mErrorInfo);
					this.mErrors.addOneError(tCError);
					return false;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * 准备往后台的数据
	 */
	private boolean preparedData(){
		mMap.put(tPolicyTransOutRegisterQuerySet, "INSERT");
		this.mInputData.add(mMap);
		return true;
	}
	/**
	 * 获取结果集的公共get方法
	 */
	public VData getResult(VData result){
		return mResult;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(END009Response tEND009Response,String mErrorInfo){
		String ID = PubFun1.CreateMaxNo("SEQ_NAME_BATCHNO", 20);
		String TransType = tEND009Response.getHead().getTransType();
		String TransNo = tEND009Response.getHead().getTransNo();
		String responseCode = tEND009Response.getHead().getResponseCode();
		String resultStatus = tEND009Response.getResponseNodes().getResponseNode().get(0).getResult().getResultStatus();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+""+"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("QueryStartDate", "2015-12-07");
		tTransferData.setNameAndValue("QueryEndDate", "2015-12-07");
		tVData.add(tTransferData);
		PolicyTransOutRegisterQuery tLCContTranferInInfo = new PolicyTransOutRegisterQuery();
		tLCContTranferInInfo.submitData(tVData, "ZBXPT");
	}
}
