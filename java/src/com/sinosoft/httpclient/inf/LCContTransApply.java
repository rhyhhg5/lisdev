package com.sinosoft.httpclient.inf;

import com.sinosoft.httpclient.dto.end005.request.END005Request;
import com.sinosoft.httpclient.dto.end005.request.RequestNode;
import com.sinosoft.httpclient.dto.end005.response.END005Response;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.db.LSTransInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LSErrorListSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: 保单转入申请
 * </p>
 * <p>
 * Description:保单转入申请
 * </p>
 * <p>
 * Copyright: Copyright (c) 2015
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author : Wn
 * @version 1.0
 */
public class LCContTransApply {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 执行sql语句查询的类 */
	private ExeSQL tExeSQL = new ExeSQL();

	/** 携带数据的类 */
	private TransferData tempTransferData = new TransferData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 用于存储传入的保单号 */
	private String mPrtNo;

	/** 用于存储既往理赔信息 */
	private String mResult;
	
	/**存储错误信息*/
	private String mErrorInfo;

	/**
	 * 提供无参的构造方法
	 */
	public LCContTransApply() {
	}

	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 获取传入的方法
	 */
	public boolean getInputData() {
		/**
		 * 接收传入的数据
		 */
		tempTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mPrtNo = (String) tempTransferData.getValueByName("PrtNo");
		System.out.println(mPrtNo);
		if (mPrtNo == null && mPrtNo.equals("")) {
			CError tCError = new CError();
			tCError.moduleName = "LCContTransApply";
			tCError.functionName = "getMessage";
			tCError.errorMessage = "传入的投保单号为空！";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}

	/**
	 * 获取税优识别码发送报文的方法
	 */
	public boolean dealData() {

		LSTransInfoDB mLSTransInfoDB = new LSTransInfoDB();
		mLSTransInfoDB.setPrtNo(mPrtNo);
		if (!mLSTransInfoDB.getInfo()) {
			CError tCError = new CError();
			tCError.moduleName = "LCContTransApply";
			tCError.functionName = "dealData";
			tCError.errorMessage = "保单转入信息查询失败！";
			this.mErrors.addOneError(tCError);
			return false;
		}

		END005Request tEND005Request = new END005Request();
		RequestNode tRequestNode = new RequestNode();
		tRequestNode.setPrtNo(mPrtNo);
		tEND005Request.getRequestNodes().getRequestNode().add(tRequestNode);
		/** 生成xml的头部信息 */
		try {
			String responseXml = CommunicateServiceImpl.post(tEND005Request,
					mOperate);
			/** 获取结果 */
			END005Response tEND005Response = (END005Response) XmlParseUtil
					.xmlToDto(responseXml, END005Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断 1：如果responseCode不等于"0000",表示处理失败,
			 */
			String responseCode = tEND005Response.getHead().getResponseCode();
			String resultStatus = tEND005Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultStatus();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tEND005Response.getResponseNodes()
					.getResponseNode().get(0).getResult().getResultInfoDesc();

			if (responseCode != null && !responseCode.equals("")
					&& responseCode.equals("0000")) {
				if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("00")) {
					String mTranSequenceNo = tEND005Response.getResponseNodes()
							.getResponseNode().get(0).getBusinessObject()
							.getTransferSequenceNo();
					addSuccFlag("00", mTranSequenceNo);
					this.mErrorInfo = "业务处理成功！";
					saveIntoLSErrorList(tEND005Response,mErrorInfo,responseCode,resultStatus);
					return true;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("01")) {
					addSuccFlag("99", resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "LCContTransApply";
					tCError.functionName = "dealData";
					tCError.errorMessage = resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tEND005Response,mErrorInfo,responseCode,resultStatus);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("02")) {
					addSuccFlag("99", resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "LCContTransApply";
					tCError.functionName = "dealData";
					tCError.errorMessage = resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tEND005Response,mErrorInfo,responseCode,resultStatus);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("03")) {
					addSuccFlag("99", resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "LCContTransApply";
					tCError.functionName = "dealData";
					tCError.errorMessage = resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tEND005Response,mErrorInfo,responseCode,resultStatus);
					return false;
				} else if (resultStatus != null && !resultStatus.equals("")
						&& resultStatus.equals("99")) {
					addSuccFlag("99", resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "LCContTransApply";
					tCError.functionName = "dealData";
					tCError.errorMessage = resultInfoDesc;
					this.mErrors.addOneError(tCError);
					this.mErrorInfo = tCError.errorMessage;
					saveIntoLSErrorList(tEND005Response,mErrorInfo,responseCode,resultStatus);
					return false;
				}
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "LCContTransApply";
			tCError.functionName = "LCContTransApply";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	/***********************************************************
	 * 向LSErrorList表中增加轨迹【无论成功与否都记录一条信息】
	 * *********************************************************
	 */
	private void saveIntoLSErrorList(END005Response tEND005Response, String mErrorInfo, String responseCode, String resultStatus) {
		try {
			//准备参数
			String maxNo = PubFun1.CreateMaxNo("SEQ_TPH_BATCHNO", 20);
			String transType = tEND005Response.getHead().getTransType();
			String transNo = tEND005Response.getHead().getTransNo();
			//封装数据到LSErrorListSchema
			LSErrorListSchema tLSErrorListSchema = new LSErrorListSchema();
			tLSErrorListSchema.setSerNo(maxNo);
			tLSErrorListSchema.setTransType(transType);
			tLSErrorListSchema.setTransNo(transNo);
			tLSErrorListSchema.setBusinessNo(mPrtNo);
			tLSErrorListSchema.setResponseCode(responseCode);
			tLSErrorListSchema.setResultStatus(resultStatus);
			tLSErrorListSchema.setErrorInfo(mErrorInfo);
			tLSErrorListSchema.setMakeDate(PubFun.getCurrentDate());
			tLSErrorListSchema.setMakeTime(PubFun.getCurrentTime());
			tLSErrorListSchema.setModifyDate(PubFun.getCurrentDate());
			tLSErrorListSchema.setModifyTime(PubFun.getCurrentTime());
			//提交数据
			MMap map = new MMap();
			VData mResult = new VData();
			map.put(tLSErrorListSchema, "DELETE&INSERT");//先删再插
			mResult.add(map);
			PubSubmit tSubmit = new PubSubmit();
			if (!tSubmit.submitData(mResult, ""))
			{
				this.mErrors.copyAllErrors(tSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "LCContTransApply";
				tError.functionName = "submitData";
				tError.errorMessage = "向LSErrorList表提交数据失败!";
				this.mErrors.addOneError(tError);
			}
		}catch(Exception e) {
			System.out.println("向LSErrorList表中保存数据时出错！");
			e.printStackTrace();
		}
	}
	private void addSuccFlag(String mSuccFlag, String mErrorInfo) {
		String updateSQL = "";
		if ("00".equals(mSuccFlag)) {
			updateSQL = "update lstransinfo set succflag='"
					+ mSuccFlag
					+ "',TransNo='"
					+ mErrorInfo
					+ "',ReceivedDate=current date,ErrorInfo=null,modifydate=current date,modifytime=current time where prtno='"
					+ mPrtNo + "'";
		} else {
			updateSQL = "update lstransinfo set succflag='"
					+ mSuccFlag
					+ "',ErrorInfo='"
					+ mErrorInfo
					+ "',modifydate=current date,modifytime=current time where prtno='"
					+ mPrtNo + "'";
		}
		if (!tExeSQL.execUpdateSQL(updateSQL)) {
			System.out.println("更新信息失败");
		}
		System.out.println("更新信息成功");
	}

	/**
	 * 获取结果集
	 * 
	 * @param args
	 */
	public String getResult() {
		return mResult;
	}

	/**
	 * 测试用
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", "162015112002");
		tVData.add(tTransferData);
		LCContTransApply tLCContTransApply = new LCContTransApply();
		if(!tLCContTransApply.submitData(tVData, "ZBXPT")){
			System.out.println(tLCContTransApply.mErrors.getError(0).errorMessage);			
		}else{
			System.out.println("成功！");
		}

	}
}
