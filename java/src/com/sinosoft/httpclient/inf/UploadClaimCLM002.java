package com.sinosoft.httpclient.inf;


import java.util.List;

import com.sinosoft.httpclient.dto.clm002.request.CLM002Request;
import com.sinosoft.httpclient.dto.clm002.request.Head;
import com.sinosoft.httpclient.dto.clm002.request.RequestNode;
import com.sinosoft.httpclient.dto.clm002.response.CLM002Response;
import com.sinosoft.httpclient.dto.clm002.response.ResponseNode;
import com.sinosoft.httpclient.util.CommunicateServiceImpl;
import com.sinosoft.httpclient.util.XmlParseUtil;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title:  理赔信息上传</p>
 * <p>Description:根据案件号上传理赔信息</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Liuyc
 * @version 1.0
 */
public class UploadClaimCLM002 {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /**携带数据的类*/
	  private TransferData tempTransferData = new TransferData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /**用于存储传入的预约号*/
	  private String mBookingSequenceNo;

	  /**用于存储核心理赔号*/
	  private List<String> mClaimNo;
	  /**用于存储平台理赔号*/
	  private List<String> mClaimCode;
	  /**用于存储异步理预约码*/
	  private List<String> mSequenceNo;
	  /**存储交互错误信息*/
	  private String mErrorInfo ;
	  /**
	   * 提供无参的构造方法
	   */
	  public UploadClaimCLM002() {}
	/**
	 * 获取税优识别码等的方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		 //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;
	    if(!getInputData()){
	    	return false;
	    }

   		if(!dealData()){
			return false;
		}
   		if(!subMessage()){
   			return false;
   		}
		return true;
	}
	/**
	 * 获取传入的方法
	 */
	public boolean getInputData(){
		/**
    	 * 接收传入的数据
   		 */
   		tempTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
   		mBookingSequenceNo = (String) tempTransferData.getValueByName("BookingSequenceNo");
   		System.out.println(mBookingSequenceNo);
   		if(mBookingSequenceNo==null || "".equals(mBookingSequenceNo)){
   			CError tCError = new CError();
   			tCError.moduleName = "ConnectZBXServiceImpl";
   			tCError.functionName = "getMessage";
   			tCError.errorMessage = "传入的异步预约码为空！";
   			this.mErrors.addOneError(tCError);
   			return false;
		}
   		return true;
	}

	/**
	 * 业务处理
	 */
	public boolean dealData(){
       	CLM002Request tCLM002Request = new CLM002Request();
       	RequestNode tRequestNode = new RequestNode();
       	tRequestNode.setBookingSequenceNo(mBookingSequenceNo);
       	tCLM002Request.getRequestNodes().getRequestNode().add(tRequestNode);
   		/**生成xml的头部信息*/
       	Head head=new Head();
       	try {
			String responseXml = CommunicateServiceImpl.post(tCLM002Request,mOperate);
			CLM002Response tCLM002Response = (CLM002Response)XmlParseUtil.xmlToDto(responseXml, CLM002Response.class);
			/**
			 * 获取返回结果，并对返回的结果进行判断
			 * 1：如果responseCode不等于"0000",表示处理失败,
			 */
			List<ResponseNode> responseNodeList = tCLM002Response.getResponseNodes().getResponseNode();
			String responseCode = tCLM002Response.getHead().getResponseCode();
			/**
			 * 添加上处理失败的情况下的详细信息
			 */
			String resultInfoDesc = tCLM002Response.getResponseNodes().getResponseNode().get(0).getResult().getResultInfoDesc();
			
			if(responseCode!=null&&!responseCode.equals("")&&responseCode.equals("0000")){
				for(ResponseNode responseNode:responseNodeList){
					String resultStatus = responseNode.getResult().getResultStatus();
					
				if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("00")){
					System.out.println("系统正常，业务成功处理！");
					mErrorInfo = "系统正常，业务成功处理!";
					//赔案号
					String claimNo=null;
					String claimCode=null;
					String sequenceNo=null;
					
					try {
						claimNo = responseNode.getBusinessObject().getClaimNo();
						mClaimNo.add(claimNo);
						claimCode = responseNode.getBusinessObject().getClaimCodeP();
						mClaimCode.add(claimCode);
						sequenceNo = responseNode.getBusinessObject().getBookingSequenceNo();
						mSequenceNo.add(sequenceNo);
					} catch (Exception e) {
						// TODO: handle exception
					}
					saveErrorList(tCLM002Response,mErrorInfo,resultStatus,claimNo);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("01")){
					System.out.println("系统正常，等待异步返回!");
					mErrorInfo = "系统正常，等待异步返回!";
					saveErrorList(tCLM002Response,mErrorInfo,resultStatus,null);
					CError tCError = new CError();
					tCError.moduleName = "UploadClaim";
					tCError.functionName = "uploadClaim";
					tCError.errorMessage = "系统正常，等待异步返回!";
					this.mErrors.addOneError(tCError);
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("02")){
					System.out.println("系统正常，数据校验不通过!原因为："+resultInfoDesc);	
					CError tCError = new CError();
					tCError.moduleName = "UploadClaim";
					tCError.functionName = "uploadClaim";
					tCError.errorMessage = resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tCLM002Response,mErrorInfo,resultStatus,null);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("03")){
					System.out.println("系统正常，业务处理失败!原因为："+resultInfoDesc);
					CError tCError = new CError();
					tCError.moduleName = "UploadClaim";
					tCError.functionName = "uploadClaim";
					tCError.errorMessage = resultInfoDesc;
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tCLM002Response,mErrorInfo,resultStatus,null);
					this.mErrors.addOneError(tCError);
					return false;
				}else if(resultStatus!=null&&!resultStatus.equals("")&&resultStatus.equals("99")){
					System.out.println("系统异常！");
					CError tCError = new CError();
					tCError.moduleName = "UploadClaim";
					tCError.functionName = "uploadClaim";
					tCError.errorMessage = "系统异常!";
					mErrorInfo = tCError.errorMessage;
					saveErrorList(tCLM002Response,mErrorInfo,resultStatus,null);
					this.mErrors.addOneError(tCError);
					return false;
				}
				return true;
			}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			CError tCError = new CError();
			tCError.moduleName = "UploadClaim";
			tCError.functionName = "uploadClaim";
			tCError.errorMessage = "处理异常!";
			this.mErrors.addOneError(tCError);
			return false;
		}
		return true;
	}
	
	/**
	 * 将核心与平台交互返回的信息进行存储
	 * @param args
	 */
	public void saveErrorList(CLM002Response tCLM002Response,String mErrorInfo,String resultStatus,String mClaimNo){
		String ID = PubFun1.CreateMaxNo("SEQ_NAME_BATCHNO", 20);
		String TransType = tCLM002Response.getHead().getTransType();
		String TransNo = tCLM002Response.getHead().getTransNo();
		String responseCode = tCLM002Response.getHead().getResponseCode();
		String insertSQL = "insert into lserrorlist values ('"+ID+"','"+TransType+"','"+TransNo+"','"+ mClaimNo +"','"+responseCode+"','"+resultStatus+"','"+mErrorInfo+"',current date,current time,current date,current time)";
		ExeSQL tExeSQL = new ExeSQL();
		if(!tExeSQL.execUpdateSQL(insertSQL)){
			System.out.println("保存返回信息失败--"+TransType+"--"+resultStatus);
		}
		System.out.println("保存返回信息成功--"+TransType+"--"+resultStatus);
	}
	
	/**
	 * 将返回信息集合放入到结果集中
	 */
	public boolean subMessage(){
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ClaimNo", mClaimNo);
		tTransferData.setNameAndValue("ClaimCode", mClaimCode);
		tTransferData.setNameAndValue("SequenceNo", mSequenceNo);
		System.out.println(mClaimNo);
		System.out.println(mClaimCode);
		System.out.println(mSequenceNo);
		mResult.add(tTransferData);
		return true;
	}
	
	/**
	 * 获取结果集
	 * @param args
	 */
	public VData getResult(){
		return mResult;
	}
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BookingSequenceNo", "13032593126");
		tVData.add(tTransferData);
		UploadClaimCLM002 tUploadClaim= new UploadClaimCLM002();
		tUploadClaim.submitData(tVData, "ZBXPT");
		
		
	}
}
