

package com.sinosoft.httpclient.dto.nbu003.request;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestNode"
})
@XmlRootElement(name = "RequestNodes")
public class RequestNodes {

    @XmlElement(name = "RequestNode", required = true)
    protected List<RequestNode> requestNode;
    /**
     * 公共的get方法
     * @return
     */
    public List<RequestNode> getRequestNode() {
        if (requestNode == null) {
            requestNode = new ArrayList<RequestNode>();
        }
        return this.requestNode;
    }

}
