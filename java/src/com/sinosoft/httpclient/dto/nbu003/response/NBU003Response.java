//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.17 at 09:25:07 上午 CST 
//


package com.sinosoft.httpclient.dto.nbu003.response;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Title: NBU001Response</p>
 * <p>Description: 税优健康险，中保信接口    xml响应信息的根目录</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "head",
    "responseNodes"
})
@XmlRootElement(name = "Package")
public class NBU003Response {

    @XmlElement(name = "Head", required = true)
    protected Head head;
    @XmlElement(name = "ResponseNodes", required = true)
    protected ResponseNodes responseNodes;
    
    /**
     *   提供公共的set和get方法  
     */
    public Head getHead() {
        return head;
    }

    public void setHead(Head value) {
        this.head = value;
    }

    public ResponseNodes getResponseNodes() {
        return responseNodes;
    }

    public void setResponseNodes(ResponseNodes value) {
        this.responseNodes = value;
    }

}
