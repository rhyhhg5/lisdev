package com.sinosoft.httpclient.dto.nbu003.response;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Title: BusinessObject</p>
 * <p>Description: 税优健康险和中保信核对客户信息，BusinessObject</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyNo"
})
@XmlRootElement(name = "BusinessObject")
public class BusinessObject {

    @XmlElement(name = "PolicyNo", required = true, nillable = true)
    protected String policyNo;

    /**
     *    公共的get和set方法
     */
    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String value) {
        this.policyNo = value;
    }


}
