package com.sinosoft.httpclient.dto.end001.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: PremiumResult </p>
 * <p>Description: PremiumResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"customerNo",
		"customerNoNew",
		"taxWarning"
})
@XmlRootElement(name = "SinglePolicyHolder")
public class SinglePolicyHolder {
	@XmlElement(name = "CustomerNo",required = true,nillable = true)
	private String customerNo;
	@XmlElement(name = "CustomerNoNew",required = true,nillable = true)
	private String customerNoNew;
	@XmlElement(name = "TaxWarning",required = true,nillable = true)
	private String taxWarning;
	/**
	 * 无参的构造方法
	 */
	public SinglePolicyHolder(){}
	/**
	 * 公共的set和get方法
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerNoNew() {
		return customerNoNew;
	}
	public void setCustomerNoNew(String customerNoNew) {
		this.customerNoNew = customerNoNew;
	}
	public String getTaxWarning() {
		return taxWarning;
	}
	public void setTaxWarning(String taxWarning) {
		this.taxWarning = taxWarning;
	}	
	
}
