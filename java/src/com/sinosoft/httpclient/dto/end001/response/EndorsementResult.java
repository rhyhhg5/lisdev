package com.sinosoft.httpclient.dto.end001.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: PremiumResult </p>
 * <p>Description: PremiumResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"policyNo",
		"endorsementNo",
		"endorsementType",
		"endorsementSequenceNo",
		"singlePolicyHolder",
		"insuredList"
})
@XmlRootElement(name = "EndorsementResult")
public class EndorsementResult {
	@XmlElement(name = "PolicyNo",required = true,nillable = true)
	private String policyNo;
	@XmlElement(name = "EndorsementNo",required = true,nillable = true)
	private String endorsementNo;
	@XmlElement(name = "EndorsementType",required = true,nillable = true)
	private String endorsementType;
	@XmlElement(name = "EndorsementSequenceNo",required = true,nillable = true)
	private String endorsementSequenceNo;
	@XmlElement(name = "SinglePolicyHolder",required = true)
	private List<SinglePolicyHolder> singlePolicyHolder;
	@XmlElement(name = "InsuredList",required = true)
	private List<InsuredList> insuredList;
	/**
	 * 无参的构造方法
	 */
	public EndorsementResult(){}
	/**
	 * 公共的set和get方法
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getEndorsementNo() {
		return endorsementNo;
	}
	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
	public String getEndorsementType() {
		return endorsementType;
	}
	public void setEndorsementType(String endorsementType) {
		this.endorsementType = endorsementType;
	}
	public String getEndorsementSequenceNo() {
		return endorsementSequenceNo;
	}
	public void setEndorsementSequenceNo(String endorsementSequenceNo) {
		this.endorsementSequenceNo = endorsementSequenceNo;
	}
	public List<SinglePolicyHolder> getSinglePolicyHolder() {
		return singlePolicyHolder;
	}
	public void setSinglePolicyHolder(List<SinglePolicyHolder> singlePolicyHolder) {
		this.singlePolicyHolder = singlePolicyHolder;
	}
	public List<InsuredList> getInsuredList() {
		return insuredList;
	}
	public void setInsuredList(List<InsuredList> insuredList) {
		this.insuredList = insuredList;
	}
	
}
