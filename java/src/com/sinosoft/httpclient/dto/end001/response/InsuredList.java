package com.sinosoft.httpclient.dto.end001.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: PremiumResult </p>
 * <p>Description: PremiumResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"insured"
})
@XmlRootElement(name = "InsuredList")
public class InsuredList {
	@XmlElement(name = "Insured",required = true)
	private List<Insured> insured;

	/**
	 * 无参的构造方法
	 */
	public InsuredList(){}
	/**
	 * 公共的set和get方法
	 */

	public List<Insured> getInsured() {
		return insured;
	}

	public void setInsured(List<Insured> insured) {
		this.insured = insured;
	}
	
	
}
