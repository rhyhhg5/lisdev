package com.sinosoft.httpclient.dto.prm002.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  PremiumResultList </p>
 * <p>Description:  PremiumResultList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="",propOrder={
		"premiumResult"
})
@XmlRootElement(name = "PremiumResultList")
public class PremiumResultList {
	@XmlElement(name = "PremiumResult",required = true)
	private List<PremiumResult> premiumResult;
	/**
	 * 无参的构造方法
	 */
	public PremiumResultList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<PremiumResult> getPremiumResult() {
		return premiumResult;
	}
	public void setPremiumResult(List<PremiumResult> premiumResult) {
		this.premiumResult = premiumResult;
	}
	
}
