package com.sinosoft.httpclient.dto.end010.request;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Title:  RequestNodes</p>
 * <p>Description:RequestNodes</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestNode"
})
@XmlRootElement(name = "RequestNodes")
public class RequestNodes {

    @XmlElement(name = "RequestNode", required = true)
    private List<RequestNode> requestNode;
    /**
     * 无参的构造方法
     */
    public RequestNodes(){}
    /**
     * 公共的get方法
     * @return
     */
    public List<RequestNode> getRequestNode() {
        if (requestNode == null) {
            requestNode = new ArrayList<RequestNode>();
        }
        return this.requestNode;
    }
}
