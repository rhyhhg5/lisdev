package com.sinosoft.httpclient.dto.end010.request;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sinosoft.httpclient.util.DateUtil;

/**
 * <p>Title:  Head</p>
 * <p>Description:头部节点</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transTime",
    "transNo",
    "transType"
})
@XmlRootElement(name = "Head")
public class Head {

    @XmlElement(name = "TransTime", required = true, nillable = true)
    private String transTime;
    @XmlElement(name = "TransNo", required = true, nillable = true)
    private String transNo;
    @XmlElement(name = "TransType", required = true, nillable = true)
    private String transType;
    /**
     * 无参的构造方法
     */
    public Head(){
    	this.transTime = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
    	this.transNo = UUID.randomUUID().toString();
    	this.transType="END010";
    }
    /**
     *     公共的set和get方法
     */
    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String value) {
        this.transTime = value;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String value) {
        this.transNo = value;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String value) {
        this.transType = value;
    }
}
