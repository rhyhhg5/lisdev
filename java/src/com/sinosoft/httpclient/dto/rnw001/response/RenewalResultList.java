package com.sinosoft.httpclient.dto.rnw001.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  PremiumResultList </p>
 * <p>Description:  PremiumResultList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="",propOrder={
		"renewalResult"
})
@XmlRootElement(name = "RenewalResultList")
public class RenewalResultList {
	@XmlElement(name = "RenewalResult",required = true)
	private List<RenewalResult> renewalResult;
	/**
	 * 无参的构造方法
	 */
	public RenewalResultList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<RenewalResult> getRenewalResult() {
		return renewalResult;
	}
	public void setRenewalResult(List<RenewalResult> renewalResult) {
		this.renewalResult = renewalResult;
	}
	
}
