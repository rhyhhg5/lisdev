package com.sinosoft.httpclient.dto.rnw002.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: PremiumResult </p>
 * <p>Description: PremiumResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"renewalEndorsementNo",
		"renewalSequenceNo"
})		
@XmlRootElement(name = "RenewalResult")
public class RenewalResult {
	@XmlElement(name = "RenewalEndorsementNo",required = true,nillable = true)
	private String renewalEndorsementNo;
	@XmlElement(name = "RenewalSequenceNo",required = true,nillable = true)
	private String renewalSequenceNo;
	/**
	 * 无参的构造方法
	 */
	public RenewalResult(){}
	/**
	 * 公共的set和get方法
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	public String getRenewalSequenceNo() {
		return renewalSequenceNo;
	}
	public void setRenewalSequenceNo(String renewalSequenceNo) {
		this.renewalSequenceNo = renewalSequenceNo;
	}
	
	
}
