package com.sinosoft.httpclient.dto.rnw002.request;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "head",
    "requestNodes"
})
@XmlRootElement(name = "Package")
public class RNW002Request {

    @XmlElement(name = "Head", required = true)
    protected Head head;
    @XmlElement(name = "RequestNodes", required = true)
    protected RequestNodes requestNodes;
    
    public RNW002Request(){
    	head = new Head();
    	requestNodes = new RequestNodes();
    }
    
    //getter  setter
    public Head getHead() {
        return head;
    }
    public void setHead(Head value) {
        this.head = value;
    }

    public RequestNodes getRequestNodes() {
        return requestNodes;
    }
    public void setRequestNodes(RequestNodes value) {
        this.requestNodes = value;
    }
}
