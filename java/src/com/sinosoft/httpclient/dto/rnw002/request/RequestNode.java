package com.sinosoft.httpclient.dto.rnw002.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bookingSequenceNo"
})
@XmlRootElement(name = "RequestNode")
public class RequestNode {

    @XmlElement(name = "bookingSequenceNo", required = true, nillable = true)
    protected String bookingSequenceNo;
    
    /**
     * 公共的set和get方法    
     */
	public String getBookingSequenceNo() {
		return bookingSequenceNo;
	}
	public void setBookingSequenceNo(String bookingSequenceNo) {
		this.bookingSequenceNo = bookingSequenceNo;
	}

    
}
