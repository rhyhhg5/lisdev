//

package com.sinosoft.httpclient.dto.rnw002.request;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.sinosoft.httpclient.util.DateUtil;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transTime",
    "transNo",
    "transType"
})
@XmlRootElement(name = "Head")
public class Head {

    @XmlElement(name = "TransTime", required = true, nillable = true)
    protected String transTime;
    @XmlElement(name = "TransNo", required = true, nillable = true)
    protected String transNo;
    @XmlElement(name = "TransType", required = true, nillable = true)
    protected String transType;
    public Head(){
    	this.transTime = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
    	this.transNo = UUID.randomUUID().toString();
    	this.transType="RNW002";
    }

    /**
     *     公共的set和get方法
     */
    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String value) {
        this.transTime = value;
    }

    public String getTransNo() {
        return transNo;
    }

    public void setTransNo(String value) {
        this.transNo = value;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String value) {
        this.transType = value;
    }


}
