package com.sinosoft.httpclient.dto.act003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Title:  Head</p>
 * <p>Description:头部节点</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
	 "responseNode"
})
@XmlRootElement(name = "ResponseNodes")
public class ResponseNodes {
	@XmlElement(name = "ResponseNode",required = true)
	private ResponseNode responseNode;
	/**
	 * 无参的构造方法
	 */
	public ResponseNodes(){}
	/**
	 * 公共的set和get方法
	 */
	public ResponseNode getResponseNode() {
		return responseNode;
	}
	public void setResponseNode(ResponseNode responseNode) {
		this.responseNode = responseNode;
	}
}
