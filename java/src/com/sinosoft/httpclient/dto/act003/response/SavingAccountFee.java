package com.sinosoft.httpclient.dto.act003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: PremiumResult </p>
 * <p>Description: PremiumResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"comFeeId",
		"accountFeeSequenceNo",
		"safResult"
})
@XmlRootElement(name = "SavingAccountFee")
public class SavingAccountFee {
	@XmlElement(name = "ComFeeId",required = true,nillable = true)
	private String comFeeId;
	@XmlElement(name = "AccountFeeSequenceNo",required = true,nillable = true)
	private String accountFeeSequenceNo;
	@XmlElement(name = "Result", required = true)
	private SafResult safResult;
	/**
	 * 无参的构造方法
	 */
	public SavingAccountFee(){}
	/**
	 * 公共的set和get方法
	 */
	public String getComFeeId() {
		return comFeeId;
	}
	public void setComFeeId(String comFeeId) {
		this.comFeeId = comFeeId;
	}
	public String getAccountFeeSequenceNo() {
		return accountFeeSequenceNo;
	}
	public void setAccountFeeSequenceNo(String accountFeeSequenceNo) {
		this.accountFeeSequenceNo = accountFeeSequenceNo;
	}
	public SafResult getSafResult() {
		return safResult;
	}
	public void setSafResult(SafResult safResult) {
		this.safResult = safResult;
	}
	
		
}
