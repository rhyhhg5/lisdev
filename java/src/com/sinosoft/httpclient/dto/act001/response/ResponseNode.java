package com.sinosoft.httpclient.dto.act001.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  ResponseNode</p>
 * <p>Description: ResponseNode </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder={
		"result",
		"savingAccountFeeList"
})
@XmlRootElement(name = "ResponseNode")
public class ResponseNode {
	@XmlElement(name = "Result",required = true)
	private Result result;
	@XmlElement(name = "SavingAccountFeeList",required = true)
	private List<SavingAccountFeeList> savingAccountFeeList;
	/**
	 * 无参的构造方法
	 */
	public ResponseNode(){}
	/**
	 * 公共的set和get方法
	 */
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}
	public List<SavingAccountFeeList> getSavingAccountFeeList() {
		return savingAccountFeeList;
	}
	public void setSavingAccountFeeList(List<SavingAccountFeeList> savingAccountFeeList) {
		this.savingAccountFeeList = savingAccountFeeList;
	}
	
}
