package com.sinosoft.httpclient.dto.nbu001.response;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Title: PolicyOfSingle</p>
 * <p>Description: 税优健康险，中保信接口  PolicyOfSingle</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyOfSingle",
    "taxCode",
    "platResultCode",
    "platResultMessage"
})
@XmlRootElement(name = "PolicyOfSingle")
public class PolicyOfSingle {

    @XmlElement(name = "PolicyOfSingle", required = true, nillable = true)
    protected String policyOfSingle;
    @XmlElement(name = "TaxCode", required = true, nillable = true)
    protected String taxCode;
    @XmlElement(name = "PlatResultCode", required = true, nillable = true)
    protected String platResultCode;
    @XmlElement(name = "PlatResultMessage", required = true, nillable = true)
    protected String platResultMessage;

    /**
     * 提供公共的set和get方法    
     */
    public String getPolicyOfSingle() {
        return policyOfSingle;
    }

    public void setPolicyOfSingle(String value) {
        this.policyOfSingle = value;
    }

    
    public String getTaxCode() {
        return taxCode;
    }

   
    public void setTaxCode(String value) {
        this.taxCode = value;
    }

  
    public String getPlatResultCode() {
        return platResultCode;
    }

  
    public void setPlatResultCode(String value) {
        this.platResultCode = value;
    }

   
    public String getPlatResultMessage() {
        return platResultMessage;
    }

    
    public void setPlatResultMessage(String value) {
        this.platResultMessage = value;
    }

}
