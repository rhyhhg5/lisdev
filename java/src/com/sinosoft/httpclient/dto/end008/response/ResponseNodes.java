package com.sinosoft.httpclient.dto.end008.response;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Title: ResponseNodes</p>
 * <p>Description: 税优健康险，中保信接口  ResponseNodes</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseNode"
})
@XmlRootElement(name = "ResponseNodes")
public class ResponseNodes {

    @XmlElement(name = "ResponseNode", required = true)
    protected ResponseNode responseNode;
    /**
     * 
     * 提供set和get的方法
     * 
     */
    public ResponseNode getResponseNode() {
		return responseNode;
	}

	public void setResponseNode(ResponseNode responseNode) {
		this.responseNode = responseNode;
	}
}
