package com.sinosoft.httpclient.dto.end008.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  TransferInList </p>
 * <p>Description:  TransferInList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="",propOrder={
		"Transfer"
})
@XmlRootElement(name = "TransferList")
public class TransferList {
	@XmlElement(name = "Transfer",required = true)
	private List<Transfer> Transfer;
	/**
	 * 无参的构造方法
	 */
	public TransferList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<Transfer> getTransfer() {
		return Transfer;
	}
	public void setTransfer(List<Transfer> Transfer) {
		this.Transfer = Transfer;
	}
	
}
