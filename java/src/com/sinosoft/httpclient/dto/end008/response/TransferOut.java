package com.sinosoft.httpclient.dto.end008.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: TransferOut </p>
 * <p>Description: TransferOut </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
	"policyNo",
	"clientList"
})
@XmlRootElement(name = "TransferOut")
public class TransferOut {
	@XmlElement(name = "PolicyNo",required = true,nillable = true)
	private String policyNo;
	@XmlElement(name = "ClientList",required = true)
	private List<ClientList> clientList;
	/**
	 * 无参的构造方法
	 */
	public TransferOut(){}
	/**
	 * 公共的set和get方法
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public List<ClientList> getClientList() {
		return clientList;
	}
	public void setClientList(List<ClientList> clientList) {
		this.clientList = clientList;
	}
	
}
