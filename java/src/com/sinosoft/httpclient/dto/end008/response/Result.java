package com.sinosoft.httpclient.dto.end008.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  Result </p>
 * <p>Description:  Result </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"resultStatus",
		"resultInfoDesc",
		"ciitcResultCode",
		"ciitcResultMessage"
})
@XmlRootElement(name = "Result")
public class Result {
	@XmlElement(name = "ResultStatus",required = true,nillable = true)
	private String resultStatus;
	@XmlElement(name = "ResultInfoDesc",required = true,nillable = true)
	private String resultInfoDesc;
	@XmlElement(name = "CiitcResultCode",required = true,nillable = true)
	private String ciitcResultCode;
	@XmlElement(name = "CiitcResultMessage",required = true,nillable = true)
	private String ciitcResultMessage;
	/**
	 * 无参的构造方法
	 */
	public Result(){}
	/**
	 * 公共的set和get方法
	 */
	public String getResultStatus() {
		return resultStatus;
	}
	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}
	public String getResultInfoDesc() {
		return resultInfoDesc;
	}
	public void setResultInfoDesc(String resultInfoDesc) {
		this.resultInfoDesc = resultInfoDesc;
	}
	public String getCiitcResultCode() {
		return ciitcResultCode;
	}
	public void setCiitcResultCode(String ciitcResultCode) {
		this.ciitcResultCode = ciitcResultCode;
	}
	public String getCiitcResultMessage() {
		return ciitcResultMessage;
	}
	public void setCiitcResultMessage(String ciitcResultMessage) {
		this.ciitcResultMessage = ciitcResultMessage;
	}
	
}
