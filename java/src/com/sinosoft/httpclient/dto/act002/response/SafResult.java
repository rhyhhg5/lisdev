//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.09.28 at 10:06:42 上午 CST 
//

package com.sinosoft.httpclient.dto.act002.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BankName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountHolder" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactTele" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{}TransferOutList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"resultCode",
		"resultMessage"
})
@XmlRootElement(name = "Result")
public class SafResult {
	@XmlElement(name = "ResultCode",required = true,nillable = true)
	protected String resultCode;
	@XmlElement(name = "ResultMessage", required = true)
	protected List<ResultMessage> resultMessage;
	/**
	 * 无参的构造方法
	 */
	public SafResult(){}
	/**
	 * 公共的set和get方法
	 */
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String value) {
		this.resultCode = value;
	}
	
	public List<ResultMessage> getResultMessage() {
        if (resultMessage == null) {
        	resultMessage = new ArrayList<ResultMessage>();
        }
        return this.resultMessage;
    }
	
	
}
