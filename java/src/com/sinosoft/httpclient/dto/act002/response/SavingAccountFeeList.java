package com.sinosoft.httpclient.dto.act002.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  SavingAccountFeeList </p>
 * <p>Description:  SavingAccountFeeList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="",propOrder={
		"savingAccountFee"
})
@XmlRootElement(name = "SavingAccountFeeList")
public class SavingAccountFeeList {
	@XmlElement(name = "SavingAccountFee",required = true)
	private List<SavingAccountFee> savingAccountFee;
	/**
	 * 无参的构造方法
	 */
	public SavingAccountFeeList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<SavingAccountFee> getSavingAccountFee() {
		return savingAccountFee;
	}
	public void setSavingAccountFeeResult(List<SavingAccountFee> savingAccountFee) {
		this.savingAccountFee = savingAccountFee;
	}
	
}
