package com.sinosoft.httpclient.dto.end004.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  EndorsementResultList </p>
 * <p>Description:  EndorsementResultList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="",propOrder={
		"endorsementResult"
})
@XmlRootElement(name = "EndorsementResultList")
public class EndorsementResultList {
	@XmlElement(name = "EndorsementResult",required = true)
	private List<EndorsementResult> endorsementResult;
	/**
	 * 无参的构造方法
	 */
	public EndorsementResultList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<EndorsementResult> getEndorsementResult() {
		return endorsementResult;
	}
	public void setEndorsementResult(List<EndorsementResult> endorsementResult) {
		this.endorsementResult = endorsementResult;
	}
	
}
