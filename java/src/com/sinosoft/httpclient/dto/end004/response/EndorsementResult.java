package com.sinosoft.httpclient.dto.end004.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: EndorsementResult </p>
 * <p>Description: EndorsementResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"policyNo",
		"endorsementNo",
		"policyStatusUpdateType",
		"endorsementSequenceNo"
})
@XmlRootElement(name = "EndorsementResult")
public class EndorsementResult {
	@XmlElement(name = "PolicyNo",required = true,nillable = true)
	private String policyNo;
	@XmlElement(name = "EndorsementNo",required = true,nillable = true)
	private String endorsementNo;
	@XmlElement(name = "PolicyStatusUpdateType",required = true,nillable = true)
	private String policyStatusUpdateType;
	@XmlElement(name = "EndorsementSequenceNo",required = true,nillable = true)
	private String endorsementSequenceNo;
	/**
	 * 无参的构造方法
	 */
	public EndorsementResult(){}
	/**
	 * 公共的set和get方法
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getEndorsementNo() {
		return endorsementNo;
	}
	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
	public String getPolicyStatusUpdateType() {
		return policyStatusUpdateType;
	}
	public void setPolicyStatusUpdateType(String policyStatusUpdateType) {
		this.policyStatusUpdateType = policyStatusUpdateType;
	}
	public String getEndorsementSequenceNo() {
		return endorsementSequenceNo;
	}
	public void setEndorsementSequenceNo(String endorsementSequenceNo) {
		this.endorsementSequenceNo = endorsementSequenceNo;
	}
	
	

	
}
