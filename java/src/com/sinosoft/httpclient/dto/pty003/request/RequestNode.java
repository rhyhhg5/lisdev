package com.sinosoft.httpclient.dto.pty003.request;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "name",
    "gender",
    "birthday",
    "certiType",
    "certiNo",
    "nationality",
    "proposalNo",
    "policySource",
    "coverageEffectiveDate"
})
@XmlRootElement(name = "RequestNode")
public class RequestNode {
	
	@XmlElement(name = "Name", required = true, nillable = true)
    protected String name;
	@XmlElement(name = "Gender", required = true, nillable = true)
    protected String gender;
	@XmlElement(name = "Birthday", required = true, nillable = true)
    protected String birthday;
	@XmlElement(name = "CertiType", required = true, nillable = true)
    protected String certiType;
	@XmlElement(name = "CertiNo", required = true, nillable = true)
    protected String certiNo;
	@XmlElement(name = "Nationality", required = true, nillable = true)
    protected String nationality;
	@XmlElement(name = "ProposalNo", required = true, nillable = true)
    protected String proposalNo;
	@XmlElement(name = "PolicySource", required = true, nillable = true)
    protected String policySource;
	@XmlElement(name = "CoverageEffectiveDate", required = true, nillable = true)
    protected String coverageEffectiveDate;
	
	/**
     * 提供公共的set和get方法
     */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getProposalNo() {
		return proposalNo;
	}
	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}
	public String getPolicySource() {
		return policySource;
	}
	public void setPolicySource(String policySource) {
		this.policySource = policySource;
	}
	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}
	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}
	
	public String genXml() throws JAXBException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		JAXBContext jaxbContext = JAXBContext.newInstance(RequestNode.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "GBK");
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(this, os);
		
		return os.toString();
	}
	
	public static void main(String[] args) throws JAXBException{
		RequestNode tRequestNode = new RequestNode();
    	tRequestNode.setName("程州");
    	tRequestNode.setGender("0");
    	tRequestNode.setBirthday("1984-07-02");
    	tRequestNode.setCertiType("0");
    	tRequestNode.setCertiNo("522501198407021633");
    	tRequestNode.setNationality("CHN");
    	tRequestNode.setProposalNo("168888888");
    	tRequestNode.setPolicySource("1");
    	tRequestNode.setCoverageEffectiveDate("2015-10-16");
    	System.out.println(tRequestNode.genXml());
	}
}
