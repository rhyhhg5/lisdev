package com.sinosoft.httpclient.dto.pty003.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "head",
    "requestNodes"
})
@XmlRootElement(name = "Package")
public class PTY003Request {
	
	@XmlElement(name = "Head", required = true)
    protected Head head;
    @XmlElement(name = "RequestNodes", required = true)
    protected RequestNodes requestNodes;
    
    public PTY003Request(){
    	head = new Head();
    	requestNodes = new RequestNodes();
    }
    
    /**
     *   提供公共的set和get方法  
     */
	public Head getHead() {
		return head;
	}

	public void setHead(Head head) {
		this.head = head;
	}

	public RequestNodes getRequestNodes() {
		return requestNodes;
	}

	public void setRequestNodes(RequestNodes requestNodes) {
		this.requestNodes = requestNodes;
	}
    
    
}
