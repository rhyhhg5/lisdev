package com.sinosoft.httpclient.dto.pty003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerNo",
    "transferTimes"
})
@XmlRootElement(name = "BusinessObject")
public class BusinessObject {
	
	@XmlElement(name = "CustomerNo", required = true, nillable = true)
    protected String customerNo;
    @XmlElement(name = "TransferTimes",required = true,nillable = true)
    protected String transferTimes;
    
    /**
     *  提供公共的set和get方法  
     */
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getTransferTimes() {
		return transferTimes;
	}
	public void setTransferTimes(String transferTimes) {
		this.transferTimes = transferTimes;
	}
}
