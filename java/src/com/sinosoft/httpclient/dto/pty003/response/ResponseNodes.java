package com.sinosoft.httpclient.dto.pty003.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseNode"
})
@XmlRootElement(name = "ResponseNodes")
public class ResponseNodes {
	
	@XmlElement(name = "ResponseNode", required = true)
    protected List<ResponseNode> responseNode;
	
	public List<ResponseNode> getResponseNode() {
        if (responseNode == null) {
            responseNode = new ArrayList<ResponseNode>();
        }
        return this.responseNode;
    }
}
