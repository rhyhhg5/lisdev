package com.sinosoft.httpclient.dto.pty003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transTime",
    "transNo",
    "transType",
    "responseCode"
})
@XmlRootElement(name = "Head")
public class Head {
	
	@XmlElement(name = "TransTime", required = true, nillable = true)
    protected String transTime;
    @XmlElement(name = "TransNo", required = true, nillable = true)
    protected String transNo;
    @XmlElement(name = "TransType", required = true, nillable = true)
    protected String transType;
    @XmlElement(name = "ResponseCode", required = true, nillable = true)
    protected String responseCode;
    
    /**
     * 提供公共的set和get方法
     */
	public String getTransTime() {
		return transTime;
	}
	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}
	public String getTransNo() {
		return transNo;
	}
	public void setTransNo(String transNo) {
		this.transNo = transNo;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
}
