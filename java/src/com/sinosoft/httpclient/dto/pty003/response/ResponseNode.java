package com.sinosoft.httpclient.dto.pty003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "result",
    "businessObject"
})
@XmlRootElement(name = "ResponseNode")
public class ResponseNode {
	
	@XmlElement(name = "Result", required = true)
    protected Result result;
    @XmlElement(name = "BusinessObject", required = true)
    protected BusinessObject businessObject;
    
    /**
     * 提供公共的set和get方法    
     */
	public Result getResult() {
		return result;
	}
	public void setResult(Result result) {
		this.result = result;
	}
	public BusinessObject getBusinessObject() {
		return businessObject;
	}
	public void setBusinessObject(BusinessObject businessObject) {
		this.businessObject = businessObject;
	}
    
    
}
