package com.sinosoft.httpclient.dto.pty003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "head",
    "responseNodes"
})
@XmlRootElement(name = "Package")
public class PTY003Response {
	
	@XmlElement(name = "Head", required = true)
    protected Head head;
    @XmlElement(name = "ResponseNodes", required = true)
    protected ResponseNodes responseNodes;
    
    /**
     * 提供公共的set和get方法
     */
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public ResponseNodes getResponseNodes() {
		return responseNodes;
	}
	public void setResponseNodes(ResponseNodes responseNodes) {
		this.responseNodes = responseNodes;
	}
    
    
}
