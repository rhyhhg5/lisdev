package com.sinosoft.httpclient.dto.pty003.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultStatus",
    "resultInfoDesc",
    "ciitcResultCode",
    "ciitcResultMessage"
})
@XmlRootElement(name = "Result")
public class Result {
	
	@XmlElement(name = "ResultStatus", required = true, nillable = true)
    protected String resultStatus;
    @XmlElement(name = "ResultInfoDesc", required = true, nillable = true)
    protected String resultInfoDesc;
    @XmlElement(name = "CiitcResultCode", required = true, nillable = true)
    protected String ciitcResultCode;
    @XmlElement(name = "CiitcResultMessage", required = true, nillable = true)
    protected String ciitcResultMessage;
    
    /**
     * 提供公共的set和get方法    
     */
	public String getResultStatus() {
		return resultStatus;
	}
	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}
	public String getResultInfoDesc() {
		return resultInfoDesc;
	}
	public void setResultInfoDesc(String resultInfoDesc) {
		this.resultInfoDesc = resultInfoDesc;
	}
	public String getCiitcResultCode() {
		return ciitcResultCode;
	}
	public void setCiitcResultCode(String ciitcResultCode) {
		this.ciitcResultCode = ciitcResultCode;
	}
	public String getCiitcResultMessage() {
		return ciitcResultMessage;
	}
	public void setCiitcResultMessage(String ciitcResultMessage) {
		this.ciitcResultMessage = ciitcResultMessage;
	}
}
