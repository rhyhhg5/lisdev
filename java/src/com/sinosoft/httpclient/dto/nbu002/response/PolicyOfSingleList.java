package com.sinosoft.httpclient.dto.nbu002.response;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Title: PolicyOfSingleList</p>
 * <p>Description: 税优健康险，中保信接口  PolicyOfSingleList</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyOfSingle"
})
@XmlRootElement(name = "PolicyOfSingleList")
public class PolicyOfSingleList {

    @XmlElement(name = "PolicyOfSingle", required = true)
    protected List<PolicyOfSingle> policyOfSingle;
    /**
     * 提供公共的get方法
     */
    public List<PolicyOfSingle> getPolicyOfSingle() {
        if (policyOfSingle == null) {
            policyOfSingle = new ArrayList<PolicyOfSingle>();
        }
        return this.policyOfSingle;
    }

}
