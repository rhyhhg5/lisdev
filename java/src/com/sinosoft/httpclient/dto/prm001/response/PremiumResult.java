package com.sinosoft.httpclient.dto.prm001.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: PremiumResult </p>
 * <p>Description: PremiumResult </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"policyNo",
		"sequenceNo",
		"feeId",
		"feeSequenceNo"
})
@XmlRootElement(name = "PremiumResult")
public class PremiumResult {
	@XmlElement(name = "PolicyNo",required = true,nillable = true)
	private String policyNo;
	@XmlElement(name = "SequenceNo",required = true,nillable = true)
	private String sequenceNo;
	@XmlElement(name = "FeeId",required = true,nillable = true)
	private String feeId;
	@XmlElement(name = "FeeSequenceNo",required = true,nillable = true)
	private String feeSequenceNo;
	/**
	 * 无参的构造方法
	 */
	public PremiumResult(){}
	/**
	 * 公共的set和get方法
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getFeeId() {
		return feeId;
	}
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}
	public String getFeeSequenceNo() {
		return feeSequenceNo;
	}
	public void setFeeSequenceNo(String feeSequenceNo) {
		this.feeSequenceNo = feeSequenceNo;
	}
	
	

	
}
