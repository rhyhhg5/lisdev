package com.sinosoft.httpclient.dto.end006.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  ClientList </p>
 * <p>Description:  ClientList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
		"client"
})
@XmlRootElement(name = "ClientList")
public class ClientList {
	@XmlElement(name = "Client",required = true)
	private List<Client> client;
	/**
	 * 无参的构造方法
	 */
	public ClientList() {
		super();
	}
	/**
	 * 公共的set和get方法
	 */
	public List<Client> getClient() {
		return client;
	}
	public void setClient(List<Client> client) {
		this.client = client;
	}
}
