package com.sinosoft.httpclient.dto.end006.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * 
 * <p>Title: TransferOutList </p>
 * <p>Description: TransferOutList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"transferOut"
})
@XmlRootElement(name = "TransferOutList")
public class TransferOutList {
	@XmlElement(name = "TransferOut",required = true)
	private List<TransferOut> transferOut;
	/**
	 * 无参的构造方法
	 */
	public TransferOutList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<TransferOut> getTransferOut() {
		return transferOut;
	}
	public void setTransferOut(List<TransferOut> transferOut) {
		this.transferOut = transferOut;
	}
}
