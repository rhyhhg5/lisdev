package com.sinosoft.httpclient.dto.end006.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: TransferIn </p>
 * <p>Description: TransferIn </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder={
		"companyName",
		"bankName",
		"accountHolder",
		"accountNo",
		"contactName",
		"contactTele",
		"contactEmail",
		"transferOutList"
})
@XmlRootElement(name = "TransferIn")
public class TransferIn {
	@XmlElement(name = "CompanyName",required = true,nillable = true)
	private String companyName;
	@XmlElement(name = "BankName",required = true,nillable = true)
	private String bankName;
	@XmlElement(name = "AccountHolder",required = true,nillable = true)
	private String accountHolder;
	@XmlElement(name = "AccountNo",required = true,nillable = true)
	private String accountNo;
	@XmlElement(name = "ContactName",required = true,nillable = true)
	private String contactName;
	@XmlElement(name = "ContactTele",required = true,nillable = true)
	private String contactTele;
	@XmlElement(name = "ContactEmail",required = true,nillable = true)
	private String contactEmail;
	@XmlElement(name = "TransferOutList",required = true)
	private List<TransferOutList> transferOutList;
	/**
	 * 无参的构造方法
	 */
	public TransferIn(){}
	/**
	 * 公共的set和get方法
	 */
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public List<TransferOutList> getTransferOutList() {
		return transferOutList;
	}
	public void setTransferOutList(List<TransferOutList> transferOutList) {
		this.transferOutList = transferOutList;
	}
	
}
