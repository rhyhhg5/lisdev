package com.sinosoft.httpclient.dto.end006.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title:  TransferInList </p>
 * <p>Description:  TransferInList </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="",propOrder={
		"transferIn"
})
@XmlRootElement(name = "TransferInList")
public class TransferInList {
	@XmlElement(name = "TransferIn",required = true)
	private List<TransferIn> transferIn;
	/**
	 * 无参的构造方法
	 */
	public TransferInList(){}
	/**
	 * 公共的set和get方法
	 */
	public List<TransferIn> getTransferIn() {
		return transferIn;
	}
	public void setTransferIn(List<TransferIn> transferIn) {
		this.transferIn = transferIn;
	}
	
}
