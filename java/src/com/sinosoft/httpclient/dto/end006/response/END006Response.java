package com.sinosoft.httpclient.dto.end006.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: END006Response </p>
 * <p>Description: END006Response </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
	"head",
	"responseNodes"
})
@XmlRootElement(name = "Package")
public class END006Response {
	@XmlElement(name = "Head",required = true)
	private Head head;
	@XmlElement(name = "ResponseNodes",required = true)
	private ResponseNodes responseNodes;
	/**
	 * 无参的构造方法
	 */
	public END006Response(){
		head = new Head();
		responseNodes = new ResponseNodes();
	}
	/**
	 * 公共的set和get方法
	 */
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public ResponseNodes getResponseNodes() {
		return responseNodes;
	}
	public void setResponseNodes(ResponseNodes responseNodes) {
		this.responseNodes = responseNodes;
	}
}
