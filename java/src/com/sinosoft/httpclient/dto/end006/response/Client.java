package com.sinosoft.httpclient.dto.end006.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: Client </p>
 * <p>Description: Client </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"sequenceNo",
		"transferSequenceNo",
		"transApplyDate",
		"transReceivDate"
})
@XmlRootElement(name = "Client")
public class Client {
	@XmlElement(name = "SequenceNo",required = true,nillable = true)
	private String sequenceNo;
	@XmlElement(name = "TransferSequenceNo",required = true,nillable = true)
	private String transferSequenceNo;
	@XmlElement(name = "TransApplyDate",required = true,nillable = true)
	private String transApplyDate;
	@XmlElement(name = "TransReceivDate",required = true,nillable = true)
	private String transReceivDate;
	/**
	 * 无参的构造方法
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getTransferSequenceNo() {
		return transferSequenceNo;
	}
	public void setTransferSequenceNo(String transferSequenceNo) {
		this.transferSequenceNo = transferSequenceNo;
	}
	public String getTransApplyDate() {
		return transApplyDate;
	}
	public void setTransApplyDate(String transApplyDate) {
		this.transApplyDate = transApplyDate;
	}
	public String getTransReceivDate() {
		return transReceivDate;
	}
	public void setTransReceivDate(String transReceivDate) {
		this.transReceivDate = transReceivDate;
	}
}
