package com.sinosoft.httpclient.dto.end006.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <p>Title: END006Request </p>
 * <p>Description:  END006Request </p>
 * <p>Date: 2015-10-08 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",propOrder = {
		"head",
		"requestNodes"
})
@XmlRootElement(name = "Package")
public class END006Request {
	@XmlElement(name = "Head",required = true)
	private Head head;
	@XmlElement(name = "RequestNodes",required = true)
	private RequestNodes requestNodes;
	/**
	 * 无参的构造方法
	 */
	public END006Request(){
		head = new Head();
		requestNodes = new RequestNodes();
	}
	/**
	 * 公共的set和get方法
	 */
	public Head getHead() {
		return head;
	}
	public void setHead(Head head) {
		this.head = head;
	}
	public RequestNodes getRequestNodes() {
		return requestNodes;
	}
	public void setRequestNodes(RequestNodes requestNodes) {
		this.requestNodes = requestNodes;
	}
}
