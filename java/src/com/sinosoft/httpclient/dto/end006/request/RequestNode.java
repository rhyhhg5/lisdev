package com.sinosoft.httpclient.dto.end006.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Title:  RequestNode</p>
 * <p>Description:RequestNode</p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queryStartDate",
    "queryEndDate"
})
@XmlRootElement(name = "RequestNode")
public class RequestNode {

    @XmlElement(name = "QueryStartDate", required = true, nillable = true)
    private String queryStartDate;
    @XmlElement(name = "QueryEndDate", required = true, nillable = true)
    private String queryEndDate;
    /**
     * 公共的set和get方法    
     */
	public String getQueryStartDate() {
		return queryStartDate;
	}
	public void setQueryStartDate(String queryStartDate) {
		this.queryStartDate = queryStartDate;
	}
	public String getQueryEndDate() {
		return queryEndDate;
	}
	public void setQueryEndDate(String queryEndDate) {
		this.queryEndDate = queryEndDate;
	}
}
