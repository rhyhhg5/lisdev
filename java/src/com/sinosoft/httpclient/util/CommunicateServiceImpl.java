/**
 * 
 */
package com.sinosoft.httpclient.util;

import java.util.Date;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 * <p>Title:  通讯工具类  </p>
 * <p>Description:中保信     通讯工具类 </p>
 * <p>Copyright: Copyright (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : Ligz
 * @version 1.0
 */
public class CommunicateServiceImpl {
	private static String URL_BASE = "";
	private static String MD5_PRIVATE_KEY = "";
	/**数据操作类型*/
	private static String mOperate;

	/**
	 * http通信客户端，直接发送报文
	 * 
	 * @param requestXml
	 * @return 返回一个字符串
	 * @throws Exception
	 */
	public static String post(String requestXml,String cOperate) throws Exception {
		System.out.println("请求报文：" + requestXml);
		mOperate = cOperate;
		// 时间戳，系统当前时间，服务端校验超过一定时间间隔不处理
		String seq = new Long(new Date().getTime()).toString();
		/**从数据库中根据数据操作类型求出要调用的URL*/
		String uSQL = "select a.code,a.codename from ldcode a where a.codetype = '"+mOperate+"'";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(uSQL);
		if(tSSRS!=null&&tSSRS.getMaxRow()>0){
			MD5_PRIVATE_KEY =tSSRS.GetText(1, 1);
			URL_BASE = tSSRS.GetText(1, 2);
		}
		System.out.println("++++++++++++++++++111111");
		System.out.println(MD5_PRIVATE_KEY);
		System.out.println(URL_BASE);
		System.out.println("++++++++++++++++++111111");
		// 请求报文+时间戳+私钥进行md5加密
		byte[] bytes = (requestXml + MD5_PRIVATE_KEY + seq).getBytes("gbk");
		String sign = Md5.getMD5(bytes);
		System.out.println(URL_BASE);
		StringBuffer urlBuf = new StringBuffer(URL_BASE);
		urlBuf.append("?sign=");
		urlBuf.append(sign);
		urlBuf.append("&seq=");
		urlBuf.append(seq);
		System.out.println("请求连接串：" + urlBuf.toString());
		// 建立连接
		PostMethod post = new PostMethod(urlBuf.toString());

		// 传递参数
		RequestEntity requestEntity = new StringRequestEntity(requestXml, "application/xml", "GBK");
		post.setRequestEntity(requestEntity);
		HttpClient httpClient = new HttpClient();
		int statusCode = httpClient.executeMethod(post);
		if (HttpStatus.SC_OK == statusCode) {
			String responseXml = post.getResponseBodyAsString();
			System.out.println("响应报文：" + responseXml);
			return responseXml;
		} else {
			throw new Exception("通信异常，返回码为：" + statusCode);
		}
	}

	/**
	 * http通信客户端，直接发送报文
	 * 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public static String post(Object obj,String cOperate) throws Exception {
		return post(XmlParseUtil.dtoToXml(obj),cOperate);
	}
}
