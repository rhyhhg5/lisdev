/**
 * 
 */
package com.sinosoft.httpclient.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

/**
 * <p>Title:  </p>
 * <p>Description��</p>
 * <p>Copyright:  (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : 
 * @version 1.0
 */

public class XmlParseUtil {

	private static final String XML_FILE_ENCODE = "GBK";
	private static final String XML_ATTRIBUTE_INVALID = " xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";

	/**
	 * dto ��xml
	 * 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public static String dtoToXml(Object obj, String tEncoding) throws Exception {
		if (tEncoding == null || "".equals(tEncoding)) {
			tEncoding = XML_FILE_ENCODE;
		}
		JAXBContext context1 = JAXBContext.newInstance(obj.getClass());
		Marshaller m = context1.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, tEncoding);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		m.marshal(obj, out);
		String xml = out.toString(tEncoding);
		return xml == null ? null : xml.replaceAll(XML_ATTRIBUTE_INVALID, "");
	}

	public static String dtoToXml(Object obj) throws Exception {
		return dtoToXml(obj, null);
	}

	/**
	 * xml ��dto
	 * 
	 * @param xml
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	public static Object xmlToDto(String xml, Class<?> clazz) throws Exception {
		Unmarshaller um = JAXBContext.newInstance(clazz).createUnmarshaller();
		return um.unmarshal(new StreamSource(new StringReader(xml)));
	}

	/**
	 * xml ��dto
	 * 
	 * @param in
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	public static Object xmlToDto(InputStream in, Class<?> clazz) throws Exception {
		Unmarshaller um = JAXBContext.newInstance(clazz).createUnmarshaller();
		return um.unmarshal(in);
	}

}
