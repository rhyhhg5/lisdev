/**
 * 
 */
package com.sinosoft.httpclient.util;

import java.io.File;

/**
 * <p>Title:  </p>
 * <p>Description：</p>
 * <p>Copyright:  (c) 2015</p>
 * <p>Company: Sinosoft</p>
 * @author : 
 * @version 1.0
 */
public class Xml2JavaBeanUtil {
	private static final String XJC_EXE_PATH = "D:\\Program Files\\Java\\jdk1.6.0_24\\bin\\xjc.exe";
	// 生成的java文件根目录，绝对路径或相对于工程根目录的相对路径
	private static final String JAVA_FILE_ROOT_DIR = "src/";

	/**
	 * 根据xsd文件生成javabean
	 * 
	 * @param xsdFilePath
	 *            xsd文件路径
	 * @param packagePath
	 *            生成的java文件所在的包
	 */
	public static void xsd2JavaBean(String xsdFilePath, String packagePath) throws Exception {
		File javaFile = new File(JAVA_FILE_ROOT_DIR);
		if (!javaFile.exists()) {
			javaFile.mkdirs();
		}

		// 拼接命令行
		StringBuffer cmdBuffer = new StringBuffer("\"");
		cmdBuffer.append(XJC_EXE_PATH);
		cmdBuffer.append("\" -d \"");
		cmdBuffer.append(javaFile.getCanonicalPath());
		cmdBuffer.append("\" -p ");
		cmdBuffer.append(packagePath);
		cmdBuffer.append(" \"");
		cmdBuffer.append(xsdFilePath);
		cmdBuffer.append("\"");
		System.out.println("Exec cmd: " + cmdBuffer.toString());
		Runtime.getRuntime().exec(cmdBuffer.toString());
	}
	
	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception{
		Xml2JavaBeanUtil.xsd2JavaBean("d:/请求报文.xsd", "aaa");
		Xml2JavaBeanUtil.xsd2JavaBean("d:/响应报文.xsd", "aaa");
	}
}
