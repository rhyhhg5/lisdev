package com.sinosoft.easyscan5.core.service.clientupload.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.sinosoft.easyscan5.base.service.impl.BaseServiceImpl;
import com.sinosoft.easyscan5.common.Constants;
import com.sinosoft.easyscan5.common.EsDocMainPToEsQcMain;
import com.sinosoft.easyscan5.common.EsDocPagesPToEsQcPage;
import com.sinosoft.easyscan5.core.service.clientupload.UploadIndexService;
import com.sinosoft.easyscan5.core.vo.EsDocAndPageVO;
import com.sinosoft.easyscan5.entity.EsQcMain;
import com.sinosoft.easyscan5.entity.EsQcPages;
import com.sinosoft.easyscan5.util.FDate;
import com.sinosoft.easyscan5.util.SysMaxNoBasic;
import com.sinosoft.lis.db.ES_BATCHNODB;
import com.sinosoft.lis.db.ES_COM_SERVERDB;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_SERVER_INFODB;
import com.sinosoft.lis.easyscan.CallQCService;
import com.sinosoft.lis.easyscan.CallService;
import com.sinosoft.lis.easyscan.ParameterDataConvert;
import com.sinosoft.lis.easyscan.UploadPrepareUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_BATCHNOSchema;
import com.sinosoft.lis.schema.ES_COM_SERVERSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_PROPERTYSchema;
import com.sinosoft.lis.schema.ES_SERVER_INFOSchema;
import com.sinosoft.lis.vschema.ES_BATCHNOSet;
import com.sinosoft.lis.vschema.ES_COM_SERVERSet;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_SERVER_INFOSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NewUploadIndexServiceImpl extends BaseServiceImpl implements UploadIndexService {
	/** 输入* */
	private List<EsDocAndPageVO> inputEsDocAndPageVOList;
	/** 输出 */
	private List<EsDocAndPageVO> outEsDocAndPageVO = new ArrayList<EsDocAndPageVO>();
	private VData mOutputData;

	MMap map = new MMap();
	/** 服务器信息* */
	private ES_COM_SERVERSchema esComServer;
	private ES_SERVER_INFOSchema esServerInfo;
	private Logger logger = Logger.getLogger(this.getClass());
	private String manageCom = "";
	private String scanNo = "";
	private String channel = "";
	public ExeSQL nExeSQL = new ExeSQL();
	public CErrors mErrors = new CErrors();
	private SysMaxNoBasic sysMaxNoBasic = new SysMaxNoBasic();

	ES_DOC_PAGESSet esdps = new ES_DOC_PAGESSet();
	ES_DOC_MAINSet esdms = new ES_DOC_MAINSet();

	String[] strPage_URL;
	String scanorders;

	List<EsQcPages> savePageList_LIS = null;
	List<EsQcMain> saveMainList_LIS = null;
	String HostName="";
	
	VData mInputData=null;
	public boolean submitData(VData cInputData, String operate, String strOperator, String sServerName)
			throws Exception {
		inputEsDocAndPageVOList = cInputData.get(0) != null ? (List<EsDocAndPageVO>) cInputData.get(0) : null;

		if (!dealService(strOperator, sServerName)) {
			return false;
		}
		return true;
	}

	private boolean dealService(String strOperator, String sServerName) throws Exception {
		// TODO Auto-generated method stub
		if (inputEsDocAndPageVOList != null && inputEsDocAndPageVOList.size() > 0) {
			List<EsQcPages> savePageList = new ArrayList<EsQcPages>();
			List<EsQcMain> saveMainList = new ArrayList<EsQcMain>();
			for (int i = 0; i < inputEsDocAndPageVOList.size(); i++) {
				EsDocAndPageVO esDocAndPageVO = (EsDocAndPageVO) inputEsDocAndPageVOList.get(i);
				// 校验资料是否已经上传成功
				if (i == 0) {
					if (esDocAndPageVO.getEsQcMain().getBatchNo() != null
							&& !"".equals(esDocAndPageVO.getEsQcMain().getBatchNo())) {
						ES_BATCHNODB es_BATCHNODB = new ES_BATCHNODB();
						es_BATCHNODB.setbatchno(esDocAndPageVO.getEsQcMain().getBatchNo());
						ES_BATCHNOSet es_BATCHNOSet = es_BATCHNODB.query();
						if (es_BATCHNOSet != null && es_BATCHNOSet.size() > 0) {
							EsDocAndPageVO esDocVO = new EsDocAndPageVO();
							esDocVO.setReturn_Number(Constants.CLIENT_UPLOAD_SUCCESS);
							esDocVO.setReturn_Message(Constants.CLIENT_UPLOAD_SUCCESS_MESSAGE);
							outEsDocAndPageVO.add(esDocVO);
							return true;
						} else {
							ES_BATCHNOSchema es_BATCHNOSchema = new ES_BATCHNOSchema();
							es_BATCHNOSchema.setbatchno(esDocAndPageVO.getEsQcMain().getBatchNo());
							map.put(es_BATCHNOSchema, "INSERT");
						}
						if (!this.getServer(esDocAndPageVO.getEsQcMain())) {
							return false;
						}

					}

				}
				// 整理索引，涉及到重复上载的逻辑
				if (!updateIndex(esDocAndPageVO, savePageList, saveMainList)) {
					return false;
				}
				if (!bussCheck(sServerName, strOperator)) {
					return false;
				}
				
				//存入vdata   input
				if(!inputVdata(strOperator)){
					return false;
				}
				
				// 转换数据位schema
				if (!this.doData()) {
					return false;
				}
				if (!bussLis()) {
					return false;
				}

				
			}
			// //转换数据位schema
			// if (!this.doData(savePageList, saveMainList)) {
			// return false;
			// }

			// 提交数据库

			VData tVData = new VData();
			tVData.add(map);

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tVData, "INSERT")) {
				EsDocAndPageVO esDocVo = new EsDocAndPageVO();
				esDocVo.setReturn_Number("-500");
				esDocVo.setReturn_Message("提交数据库失败");
				outEsDocAndPageVO.add(esDocVo);
				logger.info("提交数据库失败");
				return false;
			}

			EsDocAndPageVO esDocVO = new EsDocAndPageVO();
			esDocVO.setReturn_Number(Constants.CLIENT_UPLOAD_SUCCESS);
			esDocVO.setReturn_Message(Constants.CLIENT_UPLOAD_SUCCESS_MESSAGE);
			outEsDocAndPageVO.add(esDocVO);
		}
		return true;
	}

	private boolean inputVdata(String strOperator) {
		// TODO Auto-generated method stub
		
		mInputData = new VData();
		mInputData.setSize(10);

		mInputData.setElementAt(esdms, 0);
		mInputData.setElementAt(esdps, 1);
		mInputData.setElementAt(strPage_URL, 2);
		// 设置其它
		mInputData.setElementAt("", 3);
		mInputData.setElementAt("", 4);

		GlobalInput tGI = new GlobalInput();
		tGI.Operator = strOperator;
		mInputData.setElementAt(tGI, 5);
		// 核心耶
		
		return true;
	}

	private boolean bussLis() {

		// ----------********************************************
		CallService tCallService = new CallService();
		CallQCService tCallQCService = new CallQCService();

		// 核心哎。
		// 新单上载
		String m_strServiceType = "1";//

		if ("0".equals(esdms.get(1).getDocFlag())) {
			m_strServiceType = "1";
		} else if ("1".equals(esdms.get(1).getDocFlag())) {
			m_strServiceType = "2";
		}

		if (!tCallQCService.submitData(mInputData, "")) {
			System.out.println("UploadIndexBL 不走质检");
			if (!tCallService.submitData(mInputData, "", m_strServiceType)) {
				EsDocAndPageVO esDocVo = new EsDocAndPageVO();
				esDocVo.setReturn_Number("-500");
				esDocVo.setReturn_Message(tCallService.mErrors.getFirstError());
				outEsDocAndPageVO.add(esDocVo);
				logger.info(tCallService.mErrors.getFirstError());
				return false;
			}

			map.add((MMap) tCallService.getResult().getObjectByObjectName("MMap", 0));
		} else {
			System.out.println("UploadIndexBL 走质检");
			map.add((MMap) tCallQCService.getResult().getObjectByObjectName("MMap", 0));
		}
		return true;
	}

	private boolean bussCheck(String sServerName, String strOperator) throws Exception {

		if (savePageList_LIS.size() > 0) {
			esdps.clear();
			for (EsQcPages page : savePageList_LIS) {
				ES_DOC_PAGESSchema eSchema = new ES_DOC_PAGESSchema();
				EsDocPagesPToEsQcPage.esQcPageToEsDocPagesP(page, eSchema, manageCom, scanNo,HostName);
				esdps.add(eSchema);
//				map.put(eSchema, "INSERT");
			}
		}
		if (saveMainList_LIS.size() > 0) { // 该对象有值,表中无数据

			esdms.clear();
			for (int i = 0; i < saveMainList_LIS.size(); i++) {
//				EsQcMain doc = saveMainList_LIS.get(i);
//				ES_DOC_MAINSchema eSchema = new ES_DOC_MAINSchema();
//				EsDocMainPToEsQcMain.esQcMainToEsDocMainP(doc, eSchema);
//				esdms.add(eSchema);
				

				EsQcMain doc = saveMainList_LIS.get(i);
				ES_DOC_MAINSchema eSchema = new ES_DOC_MAINSchema();
				EsDocMainPToEsQcMain.esQcMainToEsDocMainP(doc, eSchema);
				eSchema.setVersion("1");
				ES_DOC_PROPERTYSchema es_DOC_PROPERTYSchema = getDocPropertySchema(doc);
				map.put(eSchema, "INSERT");
				map.put(es_DOC_PROPERTYSchema, "INSERT");
				esdms.add(eSchema);
			
				
			}
		}

		// 核心哎。

		VData nVData = new VData();
		nVData.clear();
		nVData.setSize(10);
		//
		strPage_URL = new String[esdps.size()];
		nVData.setElementAt(esdms, 0);
		nVData.setElementAt(esdps, 1);
		nVData.setElementAt(strPage_URL, 2);
		// 设置其它
		nVData.setElementAt("", 3);
		nVData.setElementAt("", 4);

		// 核心耶

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ClientURL", sServerName);
		nVData.add(tTransferData);
		UploadPrepareUI tUploadPrepareUI = new UploadPrepareUI();
		if (!tUploadPrepareUI.submitData(nVData, "")) {
			String errcode = tUploadPrepareUI.getResult().get(3).toString();
			String errmess = tUploadPrepareUI.getResult().get(4).toString();

			EsDocAndPageVO esDocVo = new EsDocAndPageVO();
			esDocVo.setReturn_Number("-500");
			esDocVo.setReturn_Message(errmess);
			outEsDocAndPageVO.add(esDocVo);
			logger.info(errmess);

			return false;
		}

		// 核心耶

		return true;
	}

	/**
	 * 更新索引信息 已上传影像追加文件
	 * 
	 * @throws Exception
	 * 
	 */
	private boolean updateIndex(EsDocAndPageVO esDocAndPageVO, List<EsQcPages> savePageList,
			List<EsQcMain> saveMainList) throws Exception {

		savePageList_LIS = new ArrayList<EsQcPages>();
		saveMainList_LIS = new ArrayList<EsQcMain>();

		EsQcMain inputEsQcMain = esDocAndPageVO.getEsQcMain();
		inputEsQcMain.setCreateDate(new Date());
		inputEsQcMain.setUpdateDate(new Date());
		manageCom = inputEsQcMain.getManageCom();
		scanNo = inputEsQcMain.getScanNo();
		channel = inputEsQcMain.getChannel();
		inputEsQcMain.setSystemNo("0");
		String docCode = inputEsQcMain.getDocCode();
		List<EsQcPages> inputEsQcPagesList = esDocAndPageVO.getEsQcPagesList();
		ES_DOC_MAINDB es_DOC_MAINDB = new ES_DOC_MAINDB();
		es_DOC_MAINDB.setDocCode(inputEsQcMain.getDocCode());
		es_DOC_MAINDB.setBussType(inputEsQcMain.getBussType());
		es_DOC_MAINDB.setSubType(inputEsQcMain.getSubType());
		ES_DOC_MAINSet es_DOC_MAINSet = es_DOC_MAINDB.query();
//		if (es_DOC_MAINSet != null && es_DOC_MAINSet.size() > 0) {//新单上载重复上载
//			ES_DOC_MAINSchema nDoc_MAINSchema = es_DOC_MAINSet.get(1);
//			nDoc_MAINSchema.setModifyDate(FDate.getCurrentDate());
//			nDoc_MAINSchema.setModifyTime(FDate.getCurrentTime());
//			nDoc_MAINSchema.setNumPages(inputEsQcPagesList.size() + nDoc_MAINSchema.getNumPages());
//			nDoc_MAINSchema.setVersion(String.valueOf((Integer.valueOf(nDoc_MAINSchema.getVersion()) + 1)));
//			map.put(nDoc_MAINSchema, "UPDATE");
//			esdms.clear();
//			esdms.add(nDoc_MAINSchema);
//			for (int i = 0; i < inputEsQcPagesList.size(); i++) {
//
//				EsQcPages esQcPages = inputEsQcPagesList.get(i);
//				esQcPages.setCreateDate(new Date());
//				esQcPages.setServerNo(esServerInfo.getHostName());
//				esQcPages.setUpdateDate(new Date());
//				esQcPages.setPicPath(Constants.PIC_PATH + esQcPages.getPicPath());
//				esQcPages.setPageNo(Long.valueOf(esQcPages.getPageNo() + (int) nDoc_MAINSchema.getNumPages()));
//				esQcPages.setDocId(nDoc_MAINSchema.getDocID() + "");
//				savePageList.add(esQcPages);
//				savePageList_LIS.add(esQcPages);
//			}
//		} else {//新单上载第一次上载
			inputEsQcMain.setDocFlag("0");
			saveMainList.add(inputEsQcMain);
			saveMainList_LIS.add(inputEsQcMain);
			for (int i = 0; i < inputEsQcPagesList.size(); i++) {
				EsQcPages esQcPages = inputEsQcPagesList.get(i);
				if((inputEsQcMain.getBussType().equals("TB") && esQcPages.getPageSuffix().equals(".gif"))||(inputEsQcMain.getBussType().equals("TB")&&esQcPages.getPageSuffix().equals(".tif"))||!inputEsQcMain.getBussType().equals("TB")){ //zxs
						esQcPages.setCreateDate(new Date());
						esQcPages.setServerNo(esServerInfo.getHostName());
						esQcPages.setUpdateDate(new Date());
						esQcPages.setPicPath(Constants.PIC_PATH + esQcPages.getPicPath());
						savePageList.add(esQcPages);
						savePageList_LIS.add(esQcPages);
				}else{
					EsDocAndPageVO esDocVo = new EsDocAndPageVO();
					esDocVo.setReturn_Number("-500");
					esDocVo.setReturn_Message("上传的图片不符合相关格式，正确的图片格式为GIF或TIF!");
					outEsDocAndPageVO.add(esDocVo);
					return false;	
					
				}
			}
//		}
		return true;
	}

	/**
	 * 获取服务器信息
	 * 
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean getServer(EsQcMain esQcMain) throws Exception {

		ES_COM_SERVERDB tES_COM_SERVERDB = new ES_COM_SERVERDB();
		ParameterDataConvert nConvert = new ParameterDataConvert();
		String strManageCom = nConvert.getManageCom(esQcMain.getManageCom());
		tES_COM_SERVERDB.setManageCom(strManageCom);
		ES_COM_SERVERSet tES_COM_SERVERSet = tES_COM_SERVERDB.query();
		logger.debug(tES_COM_SERVERSet.size());
		if (tES_COM_SERVERSet.size() == 0) {
			EsDocAndPageVO esDocVo = new EsDocAndPageVO();
			esDocVo.setReturn_Number("-500");
			esDocVo.setReturn_Message("管理机构" + strManageCom + "没有设置对应的文件服务器");
			outEsDocAndPageVO.add(esDocVo);
			return false;
		}

		// 查询Es_Server_Info的数据

		ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
		String strHostName = tES_COM_SERVERSet.get(1).getHostName();
		tES_SERVER_INFODB.setHostName(strHostName);

		ES_SERVER_INFOSet tES_SERVER_INFOSet = tES_SERVER_INFODB.query();
		if (tES_SERVER_INFOSet == null || tES_SERVER_INFOSet.size() != 1) {
			EsDocAndPageVO esDocVo = new EsDocAndPageVO();
			esDocVo.setReturn_Number("-500");
			esDocVo.setReturn_Message("获取服务器信息失败，未配置服务器信息");
			outEsDocAndPageVO.add(esDocVo);
			return false;
		}
		esServerInfo = tES_SERVER_INFOSet.get(1);
		HostName=esServerInfo.getHostName();
		return true;
	}

	private boolean doData() throws Exception {

		if (savePageList_LIS.size() > 0) {
			for (EsQcPages page : savePageList_LIS) {
				ES_DOC_PAGESSchema eSchema = new ES_DOC_PAGESSchema();
				EsDocPagesPToEsQcPage.esQcPageToEsDocPagesP(page, eSchema, manageCom, scanNo,HostName);
				eSchema.setDocID(esdps.get(1).getDocID());
				
				map.put(eSchema, "INSERT");

			}
		}
//		if (saveMainList_LIS.size() > 0) {
//			for (int i = 0; i < saveMainList_LIS.size(); i++) {
//				EsQcMain doc = saveMainList_LIS.get(i);
//				ES_DOC_MAINSchema eSchema = new ES_DOC_MAINSchema();
//				EsDocMainPToEsQcMain.esQcMainToEsDocMainP(doc, eSchema);
//				eSchema.setVersion("1");
//				ES_DOC_PROPERTYSchema es_DOC_PROPERTYSchema = getDocPropertySchema(doc);
//				eSchema.setDocID(esdms.get(1).getDocID());
//				es_DOC_PROPERTYSchema.setDocID(esdms.get(1).getDocID() + "");
//				
//				map.put(eSchema, "INSERT");
//				map.put(es_DOC_PROPERTYSchema, "INSERT");
//			}
//		}

		return true;
	}

	/**
	 * 获取返回结果
	 */
	public VData getResult() {
		mOutputData = new VData();
		mOutputData.add(0, outEsDocAndPageVO);
		return mOutputData;
	}

	private ES_DOC_PROPERTYSchema getDocPropertySchema(EsQcMain inputesQcMain) {
		ES_DOC_PROPERTYSchema nES_DOC_PROPERTYSchema = new ES_DOC_PROPERTYSchema();
		nES_DOC_PROPERTYSchema.setP1(inputesQcMain.getP1());
		nES_DOC_PROPERTYSchema.setP2(inputesQcMain.getP2());
		nES_DOC_PROPERTYSchema.setP3(inputesQcMain.getP3());
		nES_DOC_PROPERTYSchema.setP4(inputesQcMain.getP4());
		nES_DOC_PROPERTYSchema.setP5(inputesQcMain.getP5());
		nES_DOC_PROPERTYSchema.setP6(inputesQcMain.getP6());
		nES_DOC_PROPERTYSchema.setP7(inputesQcMain.getP7());
		nES_DOC_PROPERTYSchema.setP8(inputesQcMain.getP8());
		nES_DOC_PROPERTYSchema.setP9(inputesQcMain.getP9());
		nES_DOC_PROPERTYSchema.setP10(inputesQcMain.getP10());
		nES_DOC_PROPERTYSchema.setP11(inputesQcMain.getP11());
		nES_DOC_PROPERTYSchema.setP12(inputesQcMain.getP12());
		nES_DOC_PROPERTYSchema.setP13(inputesQcMain.getP13());
		nES_DOC_PROPERTYSchema.setP14(inputesQcMain.getP14());
		nES_DOC_PROPERTYSchema.setP15(inputesQcMain.getP15());
		nES_DOC_PROPERTYSchema.setP16(inputesQcMain.getP16());
		nES_DOC_PROPERTYSchema.setP17(inputesQcMain.getP17());
		nES_DOC_PROPERTYSchema.setP18(inputesQcMain.getP18());
		nES_DOC_PROPERTYSchema.setP19(inputesQcMain.getP19());
		nES_DOC_PROPERTYSchema.setP20(inputesQcMain.getP20());
		nES_DOC_PROPERTYSchema.setDocID(inputesQcMain.getDocId());
		nES_DOC_PROPERTYSchema.setMakeDate(FDate.getCurrentDate());
		nES_DOC_PROPERTYSchema.setMakeTime(FDate.getCurrentTime());
		nES_DOC_PROPERTYSchema.setDefCode("00000001");
		nES_DOC_PROPERTYSchema.setOperator(inputesQcMain.getScanOperator());
		nES_DOC_PROPERTYSchema.setModifyDate(FDate.getCurrentDate());
		nES_DOC_PROPERTYSchema.setModifyTime(FDate.getCurrentTime());
		return nES_DOC_PROPERTYSchema;
	}
}
