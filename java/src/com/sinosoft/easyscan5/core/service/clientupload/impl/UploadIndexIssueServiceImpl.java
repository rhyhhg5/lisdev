package com.sinosoft.easyscan5.core.service.clientupload.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import com.f1j.ss.es;
import com.sinosoft.easyscan5.base.service.impl.BaseServiceImpl;
import com.sinosoft.easyscan5.common.Constants;
import com.sinosoft.easyscan5.common.EsDocMainPToEsQcMain;
import com.sinosoft.easyscan5.common.EsDocPagesPToEsQcPage;
import com.sinosoft.easyscan5.common.easyscanxml.UpIndexReqXmlConstants;
import com.sinosoft.easyscan5.common.easyscanxml.UpPrepareReqXmlConstants;
import com.sinosoft.easyscan5.core.service.clientupload.UploadIndexIssueService;
import com.sinosoft.easyscan5.core.service.dataconvert.impl.NewDataConvertServiceImpl;
import com.sinosoft.easyscan5.core.vo.EsDocAndPageVO;
import com.sinosoft.easyscan5.core.vo.easyscan.UploadIssueIndexVo;
import com.sinosoft.easyscan5.core.vo.easyscan.UploadIssuePrepareVo;
import com.sinosoft.easyscan5.entity.EsIssueDoc;
import com.sinosoft.easyscan5.entity.EsQcMain;
import com.sinosoft.easyscan5.entity.EsQcPages;
import com.sinosoft.easyscan5.util.FDate;
import com.sinosoft.easyscan5.util.SysMaxNoBasic;
import com.sinosoft.lis.db.ES_COM_SERVERDB;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.ES_DOC_RELATIONDB;
import com.sinosoft.lis.db.ES_SERVER_INFODB;
import com.sinosoft.lis.db.Es_IssueDocDB;
import com.sinosoft.lis.easyscan.CallQCService;
import com.sinosoft.lis.easyscan.CallService;
import com.sinosoft.lis.easyscan.ParameterDataConvert;
import com.sinosoft.lis.easyscan.UploadIndexUI;
import com.sinosoft.lis.easyscan.UploadPrepareUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_COM_SERVERSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_PROPERTYSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.ES_SERVER_INFOSchema;
import com.sinosoft.lis.schema.Es_Doc_LogSchema;
import com.sinosoft.lis.schema.Es_IssueDocSchema;
import com.sinosoft.lis.vschema.ES_COM_SERVERSet;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_PROPERTYSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.lis.vschema.ES_SERVER_INFOSet;
import com.sinosoft.lis.vschema.Es_IssueDocSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class UploadIndexIssueServiceImpl extends BaseServiceImpl implements
		UploadIndexIssueService {
	private boolean isCoreIssue;
	private String returnStr = null;
	private UploadIssueIndexVo tempIndexVo;
	private SysMaxNoBasic sysMaxNoBasic = new SysMaxNoBasic();
	private EsIssueDoc esIssueDoc;
	private ES_SERVER_INFOSchema esServerInfo;
	private Es_IssueDocSchema issueSchema;
	private String issueNo;
	private MMap tMMap = new MMap();//所有待提交数据库的数据 都放到该mmap中
	ExeSQL nExeSQL= new ExeSQL();
    public CErrors mErrors = new CErrors();
    

	ES_DOC_PAGESSet esdps=new ES_DOC_PAGESSet();
	ES_DOC_MAINSet esdms=new ES_DOC_MAINSet();
	
	String[] strPage_URL ;
	String scanorders ;
	
	List<EsQcPages> savePageList_LIS=null;
	List<EsQcMain> saveMainList_LIS=null;
	
	String HostName="";
	public String saveIssueIndex( UploadIssueIndexVo uploadIssueIndexVo ,String strOperator,String sServerName)
			throws Exception {
		tempIndexVo = uploadIssueIndexVo;
		issueNo = tempIndexVo.getIssueNo();
		List esqcmainList = tempIndexVo.getEsQcMainList();
		Map esqcPagesMap = tempIndexVo.getEsQcPagesMap();
		
		for (int i = 0; i < esqcmainList.size(); i++) {
			EsQcMain inputesQcMain = (EsQcMain) esqcmainList.get(i);
//			if (!issueNotHasDeal()) {
//				returnStr = "该问题件已被其他人回销！";
//				return returnStr;
//			}
			String manageCom = inputesQcMain.getManageCom();
			if (i == 0) {
				getServer(manageCom);
			}
			if (!checkInputData(inputesQcMain, esqcPagesMap)) {
				return returnStr;
			}
		}
		for (int i = 0; i < esqcmainList.size(); i++) {
			EsQcMain inputesQcMain = (EsQcMain) esqcmainList.get(i);
			VData nInputData = new VData();
			if (!upIndex(inputesQcMain, esqcPagesMap,nInputData)) {
				return returnStr;
			}
			if(!bussCheck(sServerName, strOperator )){
				return returnStr;
			}
			//核心业务处理
			if(!bussLis( strOperator )){
				return returnStr;
			}
		}

		VData tVData = new VData();
		tVData.add(tMMap);
		PubSubmit puSubmit = new PubSubmit();
		if (puSubmit.submitData(tVData, "")) {
			returnStr = null;
			return returnStr;
		} else {
			returnStr = "提交数据库失败";
			return returnStr;
		}
	}

private boolean bussCheck( String sServerName, String strOperator ) throws Exception{
		
		//核心哎。
		  VData nVData = new VData();
		  nVData.clear();
		  nVData.setSize(10);
		  //
		  strPage_URL= new String[esdps.size()];
		  	nVData.setElementAt(esdms,0);
		  	nVData.setElementAt(esdps,1);
		  	nVData.setElementAt(strPage_URL,2);
		    //设置其它
		    nVData.setElementAt("",3);
		    nVData.setElementAt("",4);
		    
		//核心耶
		  TransferData tTransferData = new TransferData();
		  tTransferData.setNameAndValue("ClientURL", sServerName);
		  nVData.add(tTransferData);
		  nVData.setElementAt("",6);
		  UploadPrepareUI tUploadPrepareUI   = new UploadPrepareUI();
		  if(!tUploadPrepareUI.submitData(nVData,"")){
				 String errcode =tUploadPrepareUI.getResult().get(3).toString();
				 String errmess =tUploadPrepareUI.getResult().get(4).toString();
				  
				 returnStr = errmess;
				
			  return false;
		  }
		  
		//核心耶
		
		return true;
	}
private boolean bussLis( String strOperator ) {
	
	//----------********************************************
	CallService tCallService = new CallService();
	CallQCService tCallQCService = new CallQCService();
	
		//核心哎。
		  VData nVData = new VData();
		  nVData.clear();
		  nVData.setSize(10);
		  
	  	nVData.setElementAt(esdms,0);
	  	nVData.setElementAt(esdps,1);
	  	nVData.setElementAt(strPage_URL,2);
	    //设置其它
	    nVData.setElementAt(0,3);
	    nVData.setElementAt("",4);
	 
		GlobalInput tGI = new GlobalInput();
		tGI.Operator = strOperator;
		nVData.setElementAt(tGI,5);
		
		nVData.setElementAt(issueNo,6);
	//核心耶
		//新单上载
		String m_strServiceType="1";//
		
		if("0".equals(esdms.get(1).getDocFlag())){
			m_strServiceType="1";
		}else if("1".equals(esdms.get(1).getDocFlag())){
			m_strServiceType="2";
		} 
		
    if (!tCallQCService.submitData(nVData, "")){
        System.out.println("UploadIndexBL 不走质检");
        if (!tCallService.submitData(nVData, "", m_strServiceType)){
			returnStr = tCallService.mErrors.getFirstError();
            return false;
        }

        tMMap.add((MMap) tCallService.getResult().getObjectByObjectName("MMap", 0));
    }else{
        System.out.println("UploadIndexBL 走质检");
        tMMap.add((MMap) tCallQCService.getResult().getObjectByObjectName("MMap", 0));
    }
	
	return true;
}
	private boolean upIndex(EsQcMain inputesQcMain, Map esqcPagesMap, VData nInputData)
			throws Exception {

		
		String docId = inputesQcMain.getDocId();
		String soperator = inputesQcMain.getScanOperator();
	
		CallService tCallService = new CallService();
		ES_DOC_MAINSet nES_DOC_MAINSet = new ES_DOC_MAINSet();
		ES_DOC_PAGESSet nES_DOC_PAGESSet = new ES_DOC_PAGESSet();
		ES_DOC_PROPERTYSet nES_DOC_PROPERTYSet = new ES_DOC_PROPERTYSet();

		ES_DOC_PAGESSet oldDocPagePSet = queryOldPages(docId);
		ES_DOC_MAINSchema oldDocMainPSchema = queryOldDoc(docId);
		
		List newEsQcPagesList = (List) esqcPagesMap.get(docId);
		String oldScandate = "";
		String oldScanTime = "";
		for (int j = 1; j <= oldDocPagePSet.size(); j++) {//原来的,表中存在的
			boolean blnDelete = true;
			boolean bbo=false;
			ES_DOC_PAGESSchema oldEsQcPages = oldDocPagePSet.get(j);
			oldScandate = oldEsQcPages.getMakeDate();
			oldScanTime = oldEsQcPages.getMakeTime();
			for (int m = 0; m < newEsQcPagesList.size(); m++) {//变更的一个,和原来的
				EsQcPages esQcPages = (EsQcPages) newEsQcPagesList.get(m);
				String pageId = esQcPages.getPageId();
				if ((pageId != null && !"".equals(pageId))
						&& String.valueOf(oldEsQcPages.getPageID()).equals(
								esQcPages.getPageId())) {
					blnDelete = false;
					if("1".equals(esQcPages.getPageFlag())){
						bbo=true;
					}
					break;
				}
			}
			if (blnDelete ||  bbo ) {
				tMMap.put("delete from es_doc_pages where pageid='"
						+ oldEsQcPages.getPageID() + "'", "DELETE");
				//增加1张表记录被删除影像
//				savePagesLog(oldEsQcPages, "DELETE");
				writeLog(oldEsQcPages,"DELETE",soperator,HostName);
			}
		}
		Date currentDate = FDate.getCurrentDateAndTime();
		inputesQcMain.setUpdateDate(currentDate);
//		inputesQcMain.setQcStatus(Constants.STATUS_TYPE_WAITQC);
		oldDocMainPSchema.setNumPages(inputesQcMain.getNumPages().intValue());
		oldDocMainPSchema.setModifyDate(FDate.getCurrentDate());
		oldDocMainPSchema.setModifyTime(FDate.getCurrentTime());
		oldDocMainPSchema.setDocFlag("1");
		
		nES_DOC_PROPERTYSet.add(this.getDocPropertySchema(inputesQcMain));
		tMMap.put(nES_DOC_PROPERTYSet, "UPDATE");

		nES_DOC_MAINSet.add(oldDocMainPSchema);
		tMMap.put(oldDocMainPSchema, "UPDATE");
		esdms.clear();
		esdms.add(oldDocMainPSchema);
		for (int n = 0; n < newEsQcPagesList.size(); n++) {
			EsQcPages esQcPages = (EsQcPages) newEsQcPagesList.get(n);
			esQcPages.setUpdateDate(currentDate);
			if((oldDocMainPSchema.getBussType().equals("TB")&&esQcPages.getPageSuffix().equals(".gif"))||(oldDocMainPSchema.getBussType().equals("TB")&&esQcPages.getPageSuffix().equals(".tif"))||!oldDocMainPSchema.getBussType().equals("TB")){ //zxs
			String pageFlag = esQcPages.getPageFlag();
			if (Constants.PAGE_FLAG_CHANGED.equals(pageFlag)) {
				esQcPages.setCreateDate(currentDate);
				esQcPages.setDocId(docId);
				esQcPages.setPageId(sysMaxNoBasic.createPageId(inputesQcMain
						.getManageCom()));
				esQcPages.setPageFlag(Constants.PAGE_FLAG_CHANGED);
			
				String strPath = esQcPages.getPicPath();
				esQcPages.setPicPath(Constants.PIC_PATH + strPath);
				esQcPages.setServerNo(esServerInfo.getHostName());
				
				ES_DOC_PAGESSchema newpagesSchema = new ES_DOC_PAGESSchema();
				EsDocPagesPToEsQcPage.esQcPageToEsDocPagesP(esQcPages,
						newpagesSchema, inputesQcMain.getManageCom(),
						inputesQcMain.getScanNo(),HostName);
				newpagesSchema.setMakeDate(FDate.getCurrentDate());
				newpagesSchema.setMakeTime(FDate.getCurrentTime());
				if (".tif".equals(newpagesSchema.getPageSuffix())) {
					newpagesSchema.setPageSuffix(".gif");
				}
				tMMap.put(newpagesSchema, "INSERT");
				writeLog(newpagesSchema,"INSERT",soperator,HostName);
			} else {
				for (int mm = 1; mm <= oldDocPagePSet.size(); mm++) {
					ES_DOC_PAGESSchema oldpage = oldDocPagePSet.get(mm);
					
					if (String.valueOf(oldpage.getPageID()).equals(esQcPages.getPageId())) {
						oldpage.setPageCode(esQcPages.getPageNo().intValue());
						oldpage.setModifyDate(FDate.getCurrentDate());
						oldpage.setModifyTime(FDate.getCurrentTime());
						tMMap.put(oldpage, "UPDATE");
//						savePagesLog(oldpage, "UPDATE");
						writeLog(oldpage,"UPDATE",soperator,HostName);

					}

				}
			}
			}else{//zxs
				returnStr = "上传的图片不符合相关格式，正确的图片格式为GIF或TIF!";
				return false;
			}
		}
		replyIssue(inputesQcMain);
		esdps.clear();
		for (int k = 0; k < newEsQcPagesList.size(); k++) {
			ES_DOC_PAGESSchema aSchema = new ES_DOC_PAGESSchema();
			EsQcPages esQcPages = (EsQcPages) newEsQcPagesList.get(k);
			EsDocPagesPToEsQcPage.esQcPageToEsDocPagesP(esQcPages, aSchema,
					oldDocMainPSchema.getManageCom(),
					oldDocMainPSchema.getScanNo(),HostName);
			nES_DOC_PAGESSet.add(aSchema);
			esdps.add(aSchema);
		}
		nInputData.add(nES_DOC_MAINSet);
		nInputData.add(nES_DOC_PAGESSet);
		nInputData.add(nES_DOC_PROPERTYSet);
		return true;
		
		
	}
	private void replyIssue(EsQcMain esQcMain) {
		// TODO Auto-generated method stub
		Es_IssueDocDB es_IssueDocDB = new Es_IssueDocDB();
		es_IssueDocDB.setIssueDocID(tempIndexVo.getIssueNo());
		Es_IssueDocSet es_IssueDocSet = es_IssueDocDB.query();
		Es_IssueDocSchema es_IssueDocSchema = es_IssueDocSet.get(1);
		es_IssueDocSchema.setStatus("1");
		es_IssueDocSchema.setResult("1");
		es_IssueDocSchema.setReplyOperator(esQcMain.getScanOperator());
		es_IssueDocSchema.setModifyDate(FDate.getCurrentDate());
		es_IssueDocSchema.setModifyTime(FDate.getCurrentTime());
		tMMap.put(es_IssueDocSchema, "UPDATE");
	}

	/**
	 * 查询原影像页信息
	 * 
	 * @param docid
	 * @return
	 */
	private ES_DOC_PAGESSet queryOldPages(String docid) {
		ES_DOC_PAGESDB es_doc_pagesdb = new ES_DOC_PAGESDB();
		es_doc_pagesdb.setDocID(docid);
		ES_DOC_PAGESSet eSet = es_doc_pagesdb.query();
		return eSet;
	}

	/**
	 * 查询原单证信息
	 * 
	 * @param docid
	 * @return
	 */
	private ES_DOC_MAINSchema queryOldDoc(String docid) {
		ES_DOC_MAINDB es_doc_maindb = new ES_DOC_MAINDB();
		es_doc_maindb.setDocID(docid);
		ES_DOC_MAINSet eSet = es_doc_maindb.query();
		ES_DOC_MAINSchema eSchema = eSet.get(1);
		return eSchema;
	}

	/**
	 * 对上传内容校验
	 * 
	 * @return boolean
	 */
	private boolean checkInputData(EsQcMain inputesQcMain, Map esqcPagesMap)
			throws Exception {

		return true;
	}

	/**
	 * 获取服务器信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean getServer(String manageCom) throws Exception {
	

        ES_COM_SERVERDB tES_COM_SERVERDB = new ES_COM_SERVERDB();
        tES_COM_SERVERDB.setManageCom(manageCom);
        ES_COM_SERVERSet tES_COM_SERVERSet = tES_COM_SERVERDB.query();
        if (tES_COM_SERVERSet.size() == 0) {
        	returnStr = "管理机构" + manageCom + "没有设置对应的文件服务器";
			return false;
		}
		ES_COM_SERVERSchema escomSchema = tES_COM_SERVERSet.get(1);
		
        ES_SERVER_INFODB tES_SERVER_INFODB = new ES_SERVER_INFODB();
        String strHostName = tES_COM_SERVERSet.get(1).getHostName();
        tES_SERVER_INFODB.setHostName(strHostName);

        ES_SERVER_INFOSet tES_SERVER_INFOSet = tES_SERVER_INFODB.query();
		if (tES_SERVER_INFOSet == null || tES_SERVER_INFOSet.size() == 0) {
			returnStr = "获取服务器信息失败，未配置服务器信息！";
			return false;
		}
		esServerInfo = tES_SERVER_INFOSet.get(1);
		HostName=esServerInfo.getHostName();
		return true;
	}

	/**
	 * 判断问题件是否回销
	 * 
	 * @param esQcMain
	 * @return
	 */
	public boolean issueNotHasDeal(){
		Es_IssueDocDB esDocDB = new Es_IssueDocDB();
		esDocDB.setIssueDocID(issueNo);
		Es_IssueDocSet eSet = esDocDB.query();
		Es_IssueDocSchema eSchema = eSet.get(1);
		String qcStatus = eSchema.getStatus();
		if(Constants.STATUS_ISSUE_YES.equals(qcStatus)){
			return true;
		}
		return false;
	}

	private boolean checkPersonFileType(EsQcMain inputEsQcMain,
			EsQcPages esQcPages) {
		// TODO Auto-generated method stub
		ExeSQL nExeSQL = new ExeSQL();
		String subtype = inputEsQcMain.getSubType();
		String pageSuffix = esQcPages.getPageSuffix();
		if (".jpg".equals(pageSuffix)) {
			String querySql = "select 1 from ldcode where codeType = 'uploadFileTypeCheck' and codeName = '"
					+ subtype + "'";
			String codeName = nExeSQL.getOneValue(querySql);
			if (codeName == null || "".equals(codeName)) {
				return false;
			} else {
				return true;
			}
		}

		return true;
	}
	private boolean getPageUrl(String xml ,int s) {
//		// TODO Auto-generated method stub
		if(xml!=null && !"".equals(xml)){
			//h获得值
			NewDataConvertServiceImpl ncsi=new NewDataConvertServiceImpl();
			ncsi.setDocument(xml);
			Document doc = ncsi.getReqDocument();
			List listGroup = doc.selectNodes(UpPrepareReqXmlConstants.XML_ROOT + "/" + UpPrepareReqXmlConstants.XML_BODY
					+ "/" + UpPrepareReqXmlConstants.XML_UPLOADBATCH + "/" + UpPrepareReqXmlConstants.XML_GROUP);
			if (listGroup != null && listGroup.size() > 0) {
				
				for (int i = 0; i < listGroup.size(); i++) {
					Element group = (Element) listGroup.get(i);
//					String groupNo = group.attributeValue(UpPrepareReqXmlConstants.XML_GROUP_CASENO);
					List docs = group.selectNodes(UpPrepareReqXmlConstants.XML_DOC);
					if (docs != null && docs.size() > 0) {
						for (int j = 0; j < docs.size(); j++) {
							if(j!=s){
								continue;
							}
							Element doc1 = (Element) docs.get(j);
							String scanorder = doc1.attributeValue(UpPrepareReqXmlConstants.XML_SCANTYPE);
							
							scanorders = scanorder; //事前0，事后1
							
							List pageList = doc1.selectNodes(UpIndexReqXmlConstants.XML_PAGES);
							if (pageList != null && pageList.size() > 0) {
								for (int k = 0; k < pageList.size(); k++) {
									Element pages = (Element) pageList.get(k);
									List pagesList = pages.selectNodes(UpIndexReqXmlConstants.XML_PAGE);
									
									strPage_URL= new String[pagesList.size()];
									             
									for (int h = 0; h < pagesList.size(); h++) {
										Element docPages = (Element) pagesList.get(h);
										String pageurl = docPages.attributeValue(UpIndexReqXmlConstants.XML_PAGE_PAGEURL);
										strPage_URL[h] = pageurl;
									}
								}
							}
						}
					}
				}
				
			}
			
			//如果新增，在doc或者page后 追加
		}
		
		return true;
	}
	private  ES_DOC_PROPERTYSchema  getDocPropertySchema(EsQcMain inputesQcMain){
		ES_DOC_PROPERTYSchema nES_DOC_PROPERTYSchema = new ES_DOC_PROPERTYSchema();
		nES_DOC_PROPERTYSchema.setP1(inputesQcMain.getP1());
		nES_DOC_PROPERTYSchema.setP2(inputesQcMain.getP2());
		nES_DOC_PROPERTYSchema.setP3(inputesQcMain.getP3());
		nES_DOC_PROPERTYSchema.setP4(inputesQcMain.getP4());
		nES_DOC_PROPERTYSchema.setP5(inputesQcMain.getP5());
		nES_DOC_PROPERTYSchema.setP6(inputesQcMain.getP6());
		nES_DOC_PROPERTYSchema.setP7(inputesQcMain.getP7());
		nES_DOC_PROPERTYSchema.setP8(inputesQcMain.getP8());
		nES_DOC_PROPERTYSchema.setP9(inputesQcMain.getP9());
		nES_DOC_PROPERTYSchema.setP10(inputesQcMain.getP10());
		nES_DOC_PROPERTYSchema.setP11(inputesQcMain.getP11());
		nES_DOC_PROPERTYSchema.setP12(inputesQcMain.getP12());
		nES_DOC_PROPERTYSchema.setP13(inputesQcMain.getP13());
		nES_DOC_PROPERTYSchema.setP14(inputesQcMain.getP14());
		nES_DOC_PROPERTYSchema.setP15(inputesQcMain.getP15());
		nES_DOC_PROPERTYSchema.setP16(inputesQcMain.getP16());
		nES_DOC_PROPERTYSchema.setP17(inputesQcMain.getP17());
		nES_DOC_PROPERTYSchema.setP18(inputesQcMain.getP18());
		nES_DOC_PROPERTYSchema.setP19(inputesQcMain.getP19());
		nES_DOC_PROPERTYSchema.setP20(inputesQcMain.getP20());
		nES_DOC_PROPERTYSchema.setDocID(inputesQcMain.getDocId());
		nES_DOC_PROPERTYSchema.setMakeDate(FDate.getCurrentDate());
		nES_DOC_PROPERTYSchema.setMakeTime(FDate.getCurrentTime());
		nES_DOC_PROPERTYSchema.setDefCode("00000001");
		nES_DOC_PROPERTYSchema.setOperator(inputesQcMain.getScanOperator());
		nES_DOC_PROPERTYSchema.setModifyDate(FDate.getCurrentDate());
		nES_DOC_PROPERTYSchema.setModifyTime(FDate.getCurrentTime());
		return nES_DOC_PROPERTYSchema;
	}
	 private boolean writeLog(ES_DOC_PAGESSchema eES_DOC_PAGESSchema,
	            String strModifyDesc, String strModifyOperator,String HostName)
	    {
	        ES_DOC_RELATIONDB nES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
	        nES_DOC_RELATIONDB.setDocID(eES_DOC_PAGESSchema.getDocID());
	        //可能会建立多个关联
	        ES_DOC_RELATIONSet nES_DOC_RELATIONSet = nES_DOC_RELATIONDB.query();
	        int intCount = nES_DOC_RELATIONSet.size();
	        if (intCount > 0)
	        {
	            for (int i = 0; i < intCount; i++)
	            {
	                ES_DOC_RELATIONSchema nES_DOC_RELATIONSchema = nES_DOC_RELATIONSet
	                        .get(i + 1);
	                //填写日志信息
	                Es_Doc_LogSchema nEs_Doc_LogSchema = new Es_Doc_LogSchema();
	                nEs_Doc_LogSchema.setLogID(getMaxNo("ES_LogID"));
	                nEs_Doc_LogSchema.setBussType(nES_DOC_RELATIONSchema
	                        .getBussType());
	                nEs_Doc_LogSchema.setSubType(nES_DOC_RELATIONSchema
	                        .getSubType());
	                nEs_Doc_LogSchema.setBussNo(nES_DOC_RELATIONSchema.getBussNo());
	                nEs_Doc_LogSchema.setBussNoType(nES_DOC_RELATIONSchema
	                        .getBussNoType());
	                nEs_Doc_LogSchema.setPageID(eES_DOC_PAGESSchema.getPageID());
	                nEs_Doc_LogSchema.setDocID(eES_DOC_PAGESSchema.getDocID());
	                nEs_Doc_LogSchema.setManageCom(eES_DOC_PAGESSchema
	                        .getManageCom());
	                
	                
	                nEs_Doc_LogSchema.setScanOperator(eES_DOC_PAGESSchema
	                        .getOperator());
	                nEs_Doc_LogSchema
	                        .setPageCode(eES_DOC_PAGESSchema.getPageCode());
	                nEs_Doc_LogSchema
	                        .setHostName(eES_DOC_PAGESSchema.getHostName());
	                nEs_Doc_LogSchema
	                        .setPageName(eES_DOC_PAGESSchema.getPageName());
	                nEs_Doc_LogSchema.setPageSuffix(eES_DOC_PAGESSchema
	                        .getPageSuffix());
	                if(!"".equals(eES_DOC_PAGESSchema.getPicPathFTP()) && eES_DOC_PAGESSchema.getPicPathFTP()!=null){
	                	nEs_Doc_LogSchema.setPicPathFTP(eES_DOC_PAGESSchema.getPicPathFTP());
	                }else{
	                	String hostname = "select serverport from es_server_info where hostname='" + HostName + "'";
	            		String ftppath = new ExeSQL().getOneValue(hostname);
	            		nEs_Doc_LogSchema.setPicPathFTP(ftppath + "/" + eES_DOC_PAGESSchema.getPicPath().split(Constants.PIC_PATH)[1]);
	                }
	                
//	                nEs_Doc_LogSchema.setPicPathFTP("1");
	                nEs_Doc_LogSchema.setPicPath(eES_DOC_PAGESSchema.getPicPath());
	                nEs_Doc_LogSchema.setScanNo(eES_DOC_PAGESSchema.getScanNo());
	                nEs_Doc_LogSchema
	                        .setMakeDate(eES_DOC_PAGESSchema.getMakeDate());
	                nEs_Doc_LogSchema
	                        .setMakeTime(eES_DOC_PAGESSchema.getMakeTime());
	                nEs_Doc_LogSchema.setModifyDate(eES_DOC_PAGESSchema
	                        .getModifyDate());
	                nEs_Doc_LogSchema.setModifyTime(eES_DOC_PAGESSchema
	                        .getModifyTime());
	                nEs_Doc_LogSchema.setModifyDesc(strModifyDesc);
	                nEs_Doc_LogSchema.setModifyOperator(strModifyOperator);
	                tMMap.put(nEs_Doc_LogSchema, "INSERT");
	            }
	        }
	        return true;
	    }

	    //生成流水号，包含错误处理
	    private String getMaxNo(String cNoType)
	    {
	        String strNo = PubFun1.CreateMaxNo(cNoType, 1);

	        if (strNo.equals("") || strNo.equals("0"))
	        {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "UploadPrepareBL";
	            tError.functionName = "getReturnData";
	            tError.errorNo = "-90";
	            tError.errorMessage = "生成流水号失败!";
	            this.mErrors.addOneError(tError);
	            strNo = "";
	        }

	        return strNo;
	    }
}
