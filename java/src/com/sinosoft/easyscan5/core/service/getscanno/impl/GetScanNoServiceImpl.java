package com.sinosoft.easyscan5.core.service.getscanno.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.sinosoft.easyscan5.base.service.impl.BaseServiceImpl;
import com.sinosoft.easyscan5.common.Constants;
import com.sinosoft.easyscan5.core.service.getscanno.IGetScanNoService;
import com.sinosoft.easyscan5.core.vo.easyscan.GetScanNoVo;
import com.sinosoft.easyscan5.core.vo.easyscan.QueryHisScanNoResVo;

import com.sinosoft.lis.db.ES_DOC_SCANNODB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_SCANNOSchema;
import com.sinosoft.lis.vschema.ES_DOC_SCANNOSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GetScanNoServiceImpl extends BaseServiceImpl implements
		IGetScanNoService {

	private Logger logger = Logger.getLogger(GetScanNoServiceImpl.class);
	private GetScanNoVo tempGetScanNoVo;
	private String newScanNo;
	List scanNoCountList = new ArrayList();
	private ExeSQL exeSQL = new ExeSQL();

	 
	public String getNewScanNo(GetScanNoVo getScanNoVo) {
		tempGetScanNoVo = getScanNoVo;
		String returnStr = null;
		try {

			List list = findUntappedScanNo(getScanNoVo.getManageCom());
			if (list != null && list.size() > 0) {
				returnStr = "存在未使用的箱号：" + (list.get(0).toString())
						+ "，请使用历史箱号查询!";
				return returnStr;
			}
			 
			newScanNo = getNewMaxScanNo(getScanNoVo.getManageCom(),
					getScanNoVo.getChannel());
			if (newScanNo == null || "".equals(newScanNo)) {
				returnStr = "生成新箱号失败!";
				return returnStr;
			}
			if (!saveEsScanNo()) {
				returnStr = "生成箱号失败";
				logger.error("生成箱号失败");
				return returnStr;
			}
		} catch (Exception e) {
			returnStr = "生成箱号失败";
			logger.error("生成箱号失败:" + e.toString());
		}
		return returnStr;
	}

	/**
	 * 查询未使用的箱号
	 * 
	 * @param manageCom
	 * @return
	 */
	public List findUntappedScanNo(String manageCom) {
		StringBuffer strSQL = new StringBuffer(
				"select scanno from ES_DOC_SCANNO where " + " managecom='"
						+ manageCom + "'");
		strSQL.append(" and scanno not in"
				+ " (select distinct scanno from es_doc_main where managecom='"
				+ manageCom + "') order by makedate desc,maketime desc");
		String countScanNo = exeSQL.getOneValue(strSQL.toString());
		List list = new ArrayList();
		if (countScanNo != null && !"".equals(countScanNo)) {
			list.add(countScanNo);
		}
		return list;
	}

	public boolean saveEsScanNo() {

		ES_DOC_SCANNOSchema tES_DOC_SCANNOSchema = new ES_DOC_SCANNOSchema();
		tES_DOC_SCANNOSchema.setscan_no(newScanNo);
//		tES_DOC_SCANNOSchema.setscan_operator(tempGetScanNoVo.getScanUser());
//		tES_DOC_SCANNOSchema.setManageCom(tempGetScanNoVo.getManageCom());
//		tES_DOC_SCANNOSchema.setOperator(tempGetScanNoVo.getScanUser());
		String currentDate = PubFun.getCurrentDate();
		String currentTime = PubFun.getCurrentTime();
//		tES_DOC_SCANNOSchema.setMakeDate(currentDate);
//		tES_DOC_SCANNOSchema.setMakeTime(currentTime);
//		tES_DOC_SCANNOSchema.setModifyDate(currentDate);
//		tES_DOC_SCANNOSchema.setModifyTime(currentTime);
		MMap tMMap = new MMap();
		tMMap.put(tES_DOC_SCANNOSchema, "INSERT");
		VData tVData = new VData();
		tVData.add(tMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param mManageCom
	 * @return
	 */
	public String getNewMaxScanNo(String mManageCom, String channel) {
		String tManageCom = mManageCom;
		String strNo = "";

		strNo = PubFun1.CreateMaxNo(Constants.SEQ_SCANNO, tManageCom);

		return strNo;
	}

	 
	public String getNewScanNoResult() {
		return newScanNo;
	}

	/**
	 * 查询历史箱号
	 * 
	 * @param getScanNoVo
	 * @return
	 */
	public String queryHisScanNo(GetScanNoVo getScanNoVo) {
		tempGetScanNoVo = getScanNoVo;
		String strMessage = null;
		try {
			// List scanNoList = getScanNoDao.queryHisScanNo(getScanNoVo);
			ES_DOC_SCANNOSet eScanNoSet = findHisScanNo(getScanNoVo);
			if (eScanNoSet == null || eScanNoSet.size() == 0) {
				strMessage = "未查询到历史箱号，请确认查询条件！";
				return strMessage;
			}
			// List scanCountList = getScanNoDao.findScanNoCount(getScanNoVo,
			// scanNoList);
			SSRS ssrs = findScanNoDocCount(getScanNoVo, eScanNoSet);
			if (ssrs != null && ssrs.getMaxRow() > 0) {
				String strScanNo = "";
				String strScanNoDocCount = "";
				String strScanNoPageCount = "";

				for (int i = 1; i <= ssrs.getMaxRow(); i++) {
					QueryHisScanNoResVo queryHisScanNoResVo = new QueryHisScanNoResVo();
					strScanNoDocCount = ssrs.GetText(i, 1) == null ? "" : ssrs
							.GetText(i, 1);
					strScanNoPageCount = ssrs.GetText(i, 2) == null ? "" : ssrs
							.GetText(i, 2);
					strScanNo = ssrs.GetText(i, 3) == null ? "" : ssrs.GetText(
							i, 3);
					queryHisScanNoResVo.setScanNo(strScanNo);
					queryHisScanNoResVo.setPageCount(strScanNoPageCount);
					queryHisScanNoResVo.setDocCount(strScanNoDocCount);
					scanNoCountList.add(queryHisScanNoResVo);
				}
			}else{
				for(int i =1;i<=eScanNoSet.size();i++){
					QueryHisScanNoResVo queryHisScanNoResVo = new QueryHisScanNoResVo();
					ES_DOC_SCANNOSchema ES_DOC_SCANNOSchema= eScanNoSet.get(i);
//					queryHisScanNoResVo.setScanNo(ES_DOC_SCANNOSchema.getScanNo());
					queryHisScanNoResVo.setPageCount("0");
					queryHisScanNoResVo.setDocCount("0");
					scanNoCountList.add(queryHisScanNoResVo);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			strMessage = "查询历史箱号失败";
			logger.error("查询历史箱号失败，异常：" + e.toString());
		}
		return strMessage;
	}

	public SSRS findScanNoDocCount(GetScanNoVo getScanNoVo,
			ES_DOC_SCANNOSet eScanNoSet) {
		StringBuffer scanNoParam = new StringBuffer();
		for (int i = 1; i <= eScanNoSet.size(); i++) {
			if (i != 1) {
//				scanNoParam.append(",'" + eScanNoSet.get(i).getScanNo() + "'");
			} else {
//				scanNoParam.append("'" + eScanNoSet.get(i).getScanNo() + "'");
			}
		}
		StringBuffer querySql = new StringBuffer(
				"select count(*) docs,"
						+ " sum(numpages) pages, scanno from es_doc_main where scanno in ("
						+ scanNoParam.toString()
						+ ")"
						+ " group by scanno ");
		SSRS ssrs = exeSQL.execSQL(querySql.toString());
		return ssrs;
	}

	 
	public ES_DOC_SCANNOSet findHisScanNo(GetScanNoVo getScanNoVo) {
		StringBuffer queryScanNoBuffer = new StringBuffer();
		queryScanNoBuffer.append("select * from ES_DOC_SCANNO where 1=1 ");
		if (getScanNoVo.getScanNo() != null
				&& !"".equals(getScanNoVo.getScanNo())) {
			queryScanNoBuffer.append("and scanNo = '" + getScanNoVo.getScanNo()
					+ "'");
		}
		if (getScanNoVo.getScanUser() != null
				&& !"".equals(getScanNoVo.getScanUser())) {
			queryScanNoBuffer.append("and scanoperator = '"
					+ getScanNoVo.getScanUser() + "'");
		}
		queryScanNoBuffer.append("and managecom = '"
				+ getScanNoVo.getManageCom() + "'");
		if (getScanNoVo.getStartDate() != null
				&& !"".equals(getScanNoVo.getStartDate())) {
			if (getScanNoVo.getEndDate() != null) {
				queryScanNoBuffer.append(" and makedate between ");
				queryScanNoBuffer
						.append("'" + getScanNoVo.getStartDate() + "'");
				queryScanNoBuffer.append("and '" + getScanNoVo.getEndDate()
						+ "'");
			} else {
				queryScanNoBuffer.append(" and makedate>=");
				queryScanNoBuffer
						.append("'" + getScanNoVo.getStartDate() + "'");
			}
		} else {
			if (getScanNoVo.getEndDate() != null
					&& !"".equals(getScanNoVo.getEndDate())) {
				queryScanNoBuffer.append(" AND makedate<=");
				queryScanNoBuffer.append("'" + getScanNoVo.getEndDate() + "'");
			}
		}
		queryScanNoBuffer.append("order by makedate desc,maketime desc");
		ES_DOC_SCANNODB eScanNoDB = new ES_DOC_SCANNODB();
		ES_DOC_SCANNOSet eScanNoSet;
		eScanNoSet = eScanNoDB.executeQuery(queryScanNoBuffer.toString());

		return eScanNoSet;
	}

	public List getHisScanNoResult() {
		return scanNoCountList;
	}

	public static void main(String[] args) {
		while (true) {
			String pattern = "HH:mm:ss";
			SimpleDateFormat df = new SimpleDateFormat(pattern);
			Date today = new Date();
			String tString = df.format(today);
			System.out.println(tString);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
