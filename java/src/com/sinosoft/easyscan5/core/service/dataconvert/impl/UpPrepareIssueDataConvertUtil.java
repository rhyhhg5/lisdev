package com.sinosoft.easyscan5.core.service.dataconvert.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;

import com.sinosoft.easyscan5.common.Constants;
import com.sinosoft.easyscan5.common.easyscanxml.UpIndexReqXmlConstants;
import com.sinosoft.easyscan5.common.easyscanxml.UpPrepareIssueReqXmlConstants;
import com.sinosoft.easyscan5.common.easyscanxml.UpPrepareIssueResXmlConstants;
import com.sinosoft.easyscan5.common.easyscanxml.UpPrepareReqXmlConstants;
import com.sinosoft.easyscan5.core.service.dataconvert.INewDataConvertService;
import com.sinosoft.easyscan5.core.vo.EsDocAndPageVO;
import com.sinosoft.easyscan5.core.vo.easyscan.UploadIssuePrepareVo;
import com.sinosoft.easyscan5.entity.EsQcMain;
import com.sinosoft.easyscan5.util.FDate;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.ES_PROPERTY_DEFDB;
import com.sinosoft.lis.db.Es_IssueDocDB;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_PROPERTY_DEFSchema;
import com.sinosoft.lis.schema.Es_IssueDocSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_PROPERTY_DEFSet;
import com.sinosoft.lis.vschema.Es_IssueDocSet;

//import com.sinosoft.utility.SQLwithBindVariables;
public class UpPrepareIssueDataConvertUtil extends NewDataConvertServiceImpl implements INewDataConvertService {
	private Logger logger = Logger.getLogger(this.getClass());

	public String stringToXml(String[] serverUrl, StringBuffer bufXml) {
		String returnStr = null;
		try {
			Element root = this.getHeadXml();
			Element body = root.addElement(UpPrepareIssueResXmlConstants.XML_BODY);
			Element fileurl = body.addElement(UpPrepareIssueResXmlConstants.XML_FILEURL);
			fileurl.addAttribute(UpPrepareIssueResXmlConstants.XML_FILEURL_PAGEURL,
					(serverUrl != null) ? serverUrl[0] : "");
			fileurl.addAttribute(UpPrepareIssueResXmlConstants.XML_FILEURL_FILEPATH,
					(serverUrl != null) ? serverUrl[1] : "");
			addResult(root);
			super.getOutXml(bufXml);
		} catch (RuntimeException e) {
			logger.error("拼接返回报文出错", e);
			returnStr = "拼接返回报文出错" + e.getMessage();
		}
		return returnStr;
	}

	public String xmlToUpIssuePrepareVo(String indexXML, UploadIssuePrepareVo uploadIssuePrepareVo) {
		String returnStr = null;
		String param = "";
		try {
			super.setDocument(indexXML);
			Document doc = super.getReqDocument();
			uploadIssuePrepareVo.setChannel(getChannel(doc));
			List batchList = doc.selectNodes(UpPrepareIssueReqXmlConstants.XML_ROOT + "/"
					+ UpPrepareIssueReqXmlConstants.XML_BODY + "/" + UpPrepareIssueReqXmlConstants.XML_UPLOADBATCH);
			if (batchList == null || batchList.size() == 0) {
				logger.error("报文缺少批次节点");
				returnStr = "报文缺少批次节点";
				return returnStr;
			}
			Element uploadbatchEle = (Element) batchList.get(0);
			String batchKey = uploadbatchEle.attributeValue(UpPrepareIssueReqXmlConstants.XML_UPLOADBATCH_BATCHKEY);
			uploadIssuePrepareVo.setBatchKey(batchKey);
			List issueEleList = uploadbatchEle.selectNodes(UpPrepareIssueReqXmlConstants.XML_ISSUE);
			if (issueEleList == null || issueEleList.size() == 0) {
				logger.error("报文缺少ISSUE节点");
				returnStr = "报文缺少ISSUE节点";
				return returnStr;
			}
			Element issueEle = (Element) issueEleList.get(0);
			String issueNo = issueEle.attributeValue(UpPrepareIssueReqXmlConstants.XML_ISSUE_ISSUENO);
			uploadIssuePrepareVo.setIssueNo(issueNo);

			/*
			 * List groupEleList =
			 * issueEle.selectNodes(UpPrepareReqXmlConstants.XML_GROUP); if
			 * (groupEleList == null || groupEleList.size() == 0) {
			 * logger.error("报文格式不正确,缺少GROUP节点"); returnStr =
			 * "报文格式不正确,缺少GROUP节点"; return returnStr; }
			 * this.parseXmlToEsQcMain(groupEleList, uploadIssuePrepareVo,
			 * batchKey,param);
			 */
			// add by zhangguige 20170607
			this.issueNoToEsQcMain(issueNo, uploadIssuePrepareVo, batchKey);

		} catch (Exception e) {
			logger.error("解析报文出错", e);
			returnStr = "解析报文出错：" + e.getMessage();
		}
		return null;
	}

	/**
	 * add by zhangguige issueNo->-docid->-esdocmain
	 * 
	 * @param issueNo
	 * @param uploadIssuePrepareVo
	 * @param batchKey
	 * @throws Exception
	 *             2017年6月7日下午3:25:29
	 */
	private void issueNoToEsQcMain(String issueNo, UploadIssuePrepareVo uploadIssuePrepareVo, String batchKey)
			throws Exception {

		// StringBuffer sql = new StringBuffer("select * from es_issuedoc where
		// issuedocid = '"+issueNo+"'");
		Es_IssueDocDB es_IssueDocDB = new Es_IssueDocDB();
		es_IssueDocDB.setIssueDocID(issueNo);
		// SQLwithBindVariables sqlbv=new SQLwithBindVariables();
		// sqlbv.sql(sql.toString());
		// sqlbv.put("issueNo", issueNo);
		Es_IssueDocSet es_IssueDocSet = es_IssueDocDB.query();
		int size = es_IssueDocSet.size();
		for (int i = 1; i <= es_IssueDocSet.size(); i++) {
			Es_IssueDocSchema es_IssueDocSchema = es_IssueDocSet.get(i);
			String doccode = es_IssueDocSchema.getBussNo();
			if (doccode != null && !"".equals(doccode)) {
				// StringBuffer sql2 = new StringBuffer("select * from
				// es_doc_main where doccode = '?doccode?'");
				ES_DOC_MAINDB eS_DOC_MAINDB = new ES_DOC_MAINDB();
				eS_DOC_MAINDB.setDocCode(doccode);
				// SQLwithBindVariables sqlbv2=new SQLwithBindVariables();
				// sqlbv2.sql(sql2.toString());
				// sqlbv2.put("doccode", doccode);
				ES_DOC_MAINSet eS_DOC_MAINSet = eS_DOC_MAINDB.query();
				ES_DOC_MAINSchema eS_DOC_MAINSchema = eS_DOC_MAINSet.get(1);
				EsQcMain esQcMain = new EsQcMain();
				this.eS_DOC_MAINSchemaToEsQcMain(eS_DOC_MAINSchema, esQcMain, uploadIssuePrepareVo, batchKey);

			}
		}

	}

	/**
	 * add by zhangguige esdocmain-->--esqcmain
	 * 
	 * @param eS_DOC_MAINSchema
	 * @param esQcMain
	 * @param uploadIssuePrepareVo
	 * @param batchKey
	 * @throws Exception
	 *             2017年6月7日下午3:26:29
	 */
	private void eS_DOC_MAINSchemaToEsQcMain(ES_DOC_MAINSchema eS_DOC_MAINSchema, EsQcMain esQcMain,
			UploadIssuePrepareVo uploadIssuePrepareVo, String batchKey) throws Exception {

		esQcMain.setDocId("" + eS_DOC_MAINSchema.getDocID());
		esQcMain.setDocCode(eS_DOC_MAINSchema.getDocCode());
		esQcMain.setPrintCode(eS_DOC_MAINSchema.getPrintCode());
		esQcMain.setBussType(eS_DOC_MAINSchema.getBussType());
		esQcMain.setSubType(eS_DOC_MAINSchema.getSubType());
		esQcMain.setScanNo(eS_DOC_MAINSchema.getScanNo());
		esQcMain.setNumPages((long) eS_DOC_MAINSchema.getNumPages());
		String scanTime = eS_DOC_MAINSchema.getMakeDate() + " " + eS_DOC_MAINSchema.getMakeTime();
		if (scanTime == null || "".equals(scanTime)) {
			esQcMain.setScanDate(FDate.getCurrentDateAndTime());
		} else {
			esQcMain.setScanDate(FDate.formatYMDHMSToDate(scanTime));
		}
		esQcMain.setScanOperator(eS_DOC_MAINSchema.getScanOperator());
		esQcMain.setManageCom(eS_DOC_MAINSchema.getManageCom());
		esQcMain.setTempFlag("N");
		esQcMain.setDocFlag(Constants.DOC_QC_ISSUE);
		esQcMain.setChannel(uploadIssuePrepareVo.getChannel());
		esQcMain.setBatchNo(batchKey);
		// 扩展属性

		this.xmlToProp(esQcMain);
		uploadIssuePrepareVo.getEsQcMainList().add(esQcMain);
		//

	}

	/*
	 * EsQcMain
	 */
	private void parseXmlToEsQcMain(List groupEleList, UploadIssuePrepareVo uploadIssuePrepareVo, String batchKey,
			String param) throws Exception {
		for (int i = 0; i < groupEleList.size(); i++) {
			Element groupEle = (Element) groupEleList.get(i);

			String groupNo = groupEle.attributeValue(UpPrepareIssueReqXmlConstants.XML_GROUP_CASENO);
			List docEleList = groupEle.selectNodes(UpPrepareIssueReqXmlConstants.XML_DOC);
			if (docEleList != null && docEleList.size() > 0) {
				for (int j = 0; j < docEleList.size(); j++) {
					Element doc = (Element) docEleList.get(j);
					EsDocAndPageVO esDocAndPageVO = new EsDocAndPageVO();
					this.setEsQcMain(doc, uploadIssuePrepareVo, groupNo, batchKey, param);
				}
			}
		}
	}

	private void setEsQcMain(Element doc, UploadIssuePrepareVo uploadIssuePrepareVo, String groupNo, String batchKey,
			String param) throws Exception {
		EsQcMain esQcMain = new EsQcMain();
		esQcMain.setDocId(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_DOCID));
		esQcMain.setDocCode(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_DOCCODE));
		esQcMain.setGroupNo(groupNo);
		esQcMain.setPrintCode(doc.attributeValue(UpIndexReqXmlConstants.XML_DOC_PRINTCODE));
		esQcMain.setBussType(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_BUSSTYPE));
		esQcMain.setSubType(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_SUBTYPE));
		esQcMain.setScanNo(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_BOXNO));
		String scanType = doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_SCANTYPE);
		String numPages = doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_PAGECOUNT);
		if (numPages != null && !"".equals(numPages)) {
			esQcMain.setNumPages(new Long(numPages));
		}
		String scanTime = doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_SCANDATE);
		if (scanTime == null || "".equals(scanTime)) {
			esQcMain.setScanDate(FDate.getCurrentDateAndTime());
		} else {
			esQcMain.setScanDate(FDate.formatYMDHMSToDate(scanTime));
		}

		esQcMain.setScanOperator(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_SCANUSER));
		esQcMain.setScanCom(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_SCANCOM));
		esQcMain.setManageCom(doc.attributeValue(UpPrepareReqXmlConstants.XML_DOC_MANAGECOM));
		esQcMain.setTempFlag("N");
		esQcMain.setDocFlag(Constants.DOC_QC_ISSUE);

		esQcMain.setChannel(uploadIssuePrepareVo.getChannel());
		esQcMain.setBatchNo(batchKey);
		esQcMain.setFu1(doc.attributeValue(UpPrepareReqXmlConstants.XML_SCANTYPE));

		this.xmlToProp(esQcMain, doc);
		uploadIssuePrepareVo.getEsQcMainList().add(esQcMain);
	}

	private void xmlToProp(EsQcMain esQcMain) throws Exception {

		String sql = new String("select * from es_property_def where busstype = '" + esQcMain.getBussType()
				+ "' and subtype='" + esQcMain.getSubType() + "'");
		if ("1".equals(esQcMain.getFu1()) && "BQ".equals(esQcMain.getBussType())) {
			sql = sql + "   and propcode  <>'ishurry'";
		}
		ES_PROPERTY_DEFDB es_PROPERTY_DEFDB = new ES_PROPERTY_DEFDB();
		// es_PROPERTY_DEFDB.setBussType(esQcMain.getBussType());
		// es_PROPERTY_DEFDB.setSubType(esQcMain.getSubType());
		// SQLwithBindVariables sqlbv=new SQLwithBindVariables();
		// sqlbv.sql(sql.toString());
		// sqlbv.put("busstype", esQcMain.getBussType());
		// sqlbv.put("subtype", esQcMain.getSubType());
		ES_PROPERTY_DEFSet es_PROPERTY_DEFSet = es_PROPERTY_DEFDB.executeQuery(sql);
		for (int i = 1; i <= es_PROPERTY_DEFSet.size(); i++) {
			ES_PROPERTY_DEFSchema es_PROPERTY_DEFSchema = es_PROPERTY_DEFSet.get(i);
			if (es_PROPERTY_DEFSchema.getListCodeType().contains("priority")) {
				esQcMain.setV(es_PROPERTY_DEFSchema.getPropField(),
						es_PROPERTY_DEFSchema.getListCodeType().split("_")[1]);
			} else {
				esQcMain.setV(es_PROPERTY_DEFSchema.getPropField(), es_PROPERTY_DEFSchema.getCtrlDefaultValue());
			}
		}
	}

	private void xmlToProp(EsQcMain esQcMain, Element eDocMain) throws Exception {
		List list = eDocMain.selectNodes(UpPrepareReqXmlConstants.XML_PROPS + "/" + UpPrepareReqXmlConstants.XML_PROP);
		ArrayList<String> columnNameArr = new ArrayList<String>();
		boolean boo = false;
		List propertyList = new ArrayList();
		if (list != null && list.size() > 0) {

			for (int j = 0; j < list.size(); j++) {
				Element pro = (Element) list.get(j);
				String columnName = pro.attributeValue(UpPrepareReqXmlConstants.XML_PROP_CODE);
				columnNameArr.add(columnName);
				String fValue = pro.attributeValue(UpPrepareReqXmlConstants.XML_PROP_VALUE);
				propertyList.add(columnName);
				esQcMain.setV(columnName, fValue);
				boo = true;
			}
		} // '123','123',
		StringBuffer sql = new StringBuffer("select * from es_property_def where busstype = '" + esQcMain.getBussType()
				+ "' and subtype='" + esQcMain.getSubType() + "'");
		if (boo) {
			String vals = "";
			for (int i = 0; i < columnNameArr.size(); i++) {
				String val = columnNameArr.get(i);
				System.out.println(val);
				if (i == columnNameArr.size() - 1) {
					vals = vals + "'" + val + "'";
				} else {
					vals = vals + "'" + val + "',";
				}
			}
			sql.append(" and propfield not in (" + vals + ")");
		}
		if ("1".equals(esQcMain.getFu1()) && "BQ".equals(esQcMain.getBussType())) {
			sql.append("   and propcode  <>'ishurry'");
		}
		ES_PROPERTY_DEFDB es_PROPERTY_DEFDB = new ES_PROPERTY_DEFDB();
		// es_PROPERTY_DEFDB.setBussType(esQcMain.getBussType());
		// es_PROPERTY_DEFDB.setSubType(esQcMain.getSubType());
		//// SQLwithBindVariables sqlbv=new SQLwithBindVariables();
		// sqlbv.sql(sql.toString());
		// sqlbv.put("busstype", esQcMain.getBussType());
		// sqlbv.put("subtype", esQcMain.getSubType());
		// sqlbv.put("columnName", columnNameArr);
		ES_PROPERTY_DEFSet es_PROPERTY_DEFSet = es_PROPERTY_DEFDB.executeQuery(sql.toString());
		for (int i = 1; i <= es_PROPERTY_DEFSet.size(); i++) {
			ES_PROPERTY_DEFSchema es_PROPERTY_DEFSchema = es_PROPERTY_DEFSet.get(i);
			if (es_PROPERTY_DEFSchema.getListCodeType().contains("priority")) {
				esQcMain.setV(es_PROPERTY_DEFSchema.getPropField(),
						es_PROPERTY_DEFSchema.getListCodeType().split("_")[1]);
			} else {
				esQcMain.setV(es_PROPERTY_DEFSchema.getPropField(), es_PROPERTY_DEFSchema.getCtrlDefaultValue());
			}
		}
	}

}
