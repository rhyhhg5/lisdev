package com.sinosoft.easyscan5.core.service.dataconvert.impl;

public class ClientHeadXmlBean {

	private String action;
	private String name;
	private String version;
	private String param;
	private String manageCom;
	private String userCode;
	private String sessionId;
	// add by zhangguige 20170606 报文头新增节点

	private String netaccess;
	private String language;
	private String systemid;

	public String getNetaccess() {
		return netaccess;
	}

	public void setNetaccess(String netaccess) {
		this.netaccess = netaccess;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSystemid() {
		return systemid;
	}

	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}

	public String getManageCom() {
		return manageCom;
	}

	public void setManageCom(String manageCom) {
		this.manageCom = manageCom;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

}
