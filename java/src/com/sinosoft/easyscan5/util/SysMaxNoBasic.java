package com.sinosoft.easyscan5.util;

import org.apache.log4j.Logger;

//import com.sinosoft.lis.maxnomanage.CreateMaxNoImp;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;



/**
 * <p>Title: EasyScan影像</p>
 * <p>Description: 系统号码管理 </p>
 * <p> Copyright: Copyright (c) 2013</p>
 * <p> Company: Sinosoft </p> 
 * @version 1.0
 */
public class SysMaxNoBasic {
	private static Logger logger = Logger.getLogger(SysMaxNoBasic.class);
	public CErrors mErrors = new CErrors();
	public String createDocId(String manageCom) {
//		CreateMaxNoImp CreateMaxNo=new CreateMaxNoImp();
		String docid =  getMaxNo("DocID");
		return docid;
	}
	public String createPageId(String manageCom){
//		CreateMaxNoImp CreateMaxNo=new CreateMaxNoImp();
		String pageId = getMaxNo("PageID");
		return pageId;
	}
	private String getMaxNo(String cNoType) {
		String strNo = PubFun1.CreateMaxNo(cNoType, 1);

		if (strNo.equals("") || strNo.equals("0")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UploadPrepareBL";
			tError.functionName = "getReturnData";
			tError.errorNo = "-90";
			tError.errorMessage = "生成流水号失败!";
			this.mErrors.addOneError(tError);
			strNo = "";
		}
		return strNo;
	}
}
