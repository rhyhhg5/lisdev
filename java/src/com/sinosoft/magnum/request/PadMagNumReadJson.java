package com.sinosoft.magnum.request;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PadMagNumReadJson {
	private LCContSchema mLCContSchema = new LCContSchema();
	private static String mMagNumJson = "";
	VData mVData = new VData();
	TransferData resultJsonTransferData = new TransferData();
	public String dealPadReadJson(VData cTransferData) throws IOException{
		getInputData(cTransferData);
		
		//创建objectMapper

        // 将从Magnum Mobile获取的提交数据包，解析为SubmissionDataPackageObject对象
//      //  SubmissionDataPackageObject submissionDataPackageObject = readJson();
//        
//        List<BulkSubmitObject> bulkSubmitObjects = submissionDataPackageObject.getDataPackages();
//        for(int i =0; i < bulkSubmitObjects.size();i++){
//        	BulkSubmitObject bulkSubmitObject = bulkSubmitObjects.get(i);
//        	List<BulkSubmitAttributeObject>attributeObjects =bulkSubmitObject.getAttributes(); 
//        	for(int j = 0 ; j < attributeObjects.size();j++){
//        		BulkSubmitAttributeObject attributeObject = attributeObjects.get(j);
//        		System.out.println("-------   "+ attributeObject.getAttribute() +"   "+attributeObject.getValueAsString());
//        	}
//        }
//
//        // 将数据C创建为BulkSubmitObject对象
//        BulkSubmitObject blockC = new BulkSubmitObject();
//        blockC.setAttributes(new ArrayList<BulkSubmitAttributeObject>());

        // 为数据C创建属性
//        BulkSubmitAttributeObject life1_sumAggr = new BulkSubmitAttributeObject();
//        life1_sumAggr.setAttribute("case.life[lifeId=1234].PIICH.SumAgregated"); //Attribute locator should be created dynamically 应动态地创建属性的定位段
//        life1_sumAggr.setValueAsString("100000");
//
//        BulkSubmitAttributeObject life2_sumAggr = new BulkSubmitAttributeObject();
//        life2_sumAggr.setAttribute("case.life[lifeId=1230004].PIICH.SumAgregated");
//        life2_sumAggr.setValueAsString("100000");
//
//        // 将属性加入到数据C的对象中
//        blockC.getAttributes().add(life1_sumAggr);
//        blockC.getAttributes().add(life2_sumAggr);
//
//        // 将数据C的对象，加入Magnum Mobile的提交数据包
//        submissionDataPackageObject.getDataPackages().add(blockC);

        // 由对象创建Json字符串
        // 即为最终需要传到Magnum Runtime后台服务器的结果
     //   mMagNumJson = createJson(submissionDataPackageObject);
        
		return mMagNumJson;
	}
	// 获得输出数据
	private void getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mMagNumJson = (String) tTransferData.getValueByName("MagNumJson");
		mLCContSchema = (LCContSchema) tTransferData.getValueByName("tLCContSchema");
	}
	
	//调用MagNum
	public  Map<String, String> dealPadMagNumRequestJson(String str,String prtno) throws Exception{
		Map<String, String> resultMap = new HashMap<String, String>();
		String resultJson = "";
		int statusCode;
		ExeSQL exeSql = new ExeSQL();
		String sql = null;
		if(prtno.startsWith("PD1")){
				sql = "select sysvarvalue from ldsysvar where sysvar = 'MagNumAddress'";
		}else if(prtno.startsWith("PD2")){
			    sql = "select sysvarvalue from ldsysvar where sysvar = 'NewMagNumAddress'";
		}
		System.out.println("Prtno="+prtno+"sql="+sql);
    	String magNumUrl = exeSql.getOneValue(sql);
    	System.out.println("Prtno="+prtno+"magNumUrl="+magNumUrl);
		//String url = "http://10.136.4.162:9080/webapp/engine/v1/composite/cases/submit";
		HttpClient httpClient = new HttpClient();
        PostMethod postMethod = new PostMethod(magNumUrl);
        postMethod.setRequestHeader("Content-Type", "application/json");
        //设置编码
        httpClient.getParams().setParameter(
                HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
        postMethod.setRequestBody(str);
        try
        {
            long start = System.currentTimeMillis();
            //执行getMethod
            statusCode = httpClient.executeMethod(postMethod);
            //失败
            if (statusCode != HttpStatus.SC_OK)
            {
                System.out.println("Method failed: " + postMethod.getStatusLine());
                //读取内容 
                byte[] responseBody = postMethod.getResponseBody();
                //处理内容
                resultJson = new String(responseBody, "GBK");
                System.out.println(resultJson);
            }
            else
            {
                //              //读取内容 
                byte[] responseBody = postMethod.getResponseBody();
                //              
                resultJson = new String(responseBody, "UTF-8");
                System.out.println("服务器返回:" + resultJson);
            }
            System.out.println("cost:" + (System.currentTimeMillis() - start));
        }
        finally
        {
            //释放连接
            postMethod.releaseConnection();
        }
        resultMap.put("ResultJson", resultJson);
        resultMap.put("StatusCode", String.valueOf(statusCode));
		return resultMap;
	}
	//解析MagNum返回结论
	public Map<String, String> dealResultMainDecison(String str) throws JSONException {
		Map<String, String> resultMap = new HashMap<String, String>();
//    	String mResultMainDecison = "";
//        //将从Magnum Runtime中获取的返回消息对象，解析成SubmitCaseDataResponseObject
//        SubmitCaseDataResponseObject submitCaseDataResponseObject = readJson(str);
//
//        //Get top level decision InternalCode 获取顶层核保结论的InternalCode
//       
//        String topLevelDecision = submitCaseDataResponseObject.getDecision().getDecision().
//        		getDecision().getInternalCode();
//
//        List<DecisionReferenceDataObject> subLevelDecision = submitCaseDataResponseObject.getDecision().getDecision().getSupportCodes();
//        resultJsonTransferData.setNameAndValue("Size", subLevelDecision.size());
//    	mVData.add(resultJsonTransferData);
//        DecisionNodeObject decision = submitCaseDataResponseObject.getDecision();
//        List<DecisionNodeEntryObject> childNodes = decision.getChildNodes();
//
//        for (DecisionNodeEntryObject entry: childNodes){
//            DecisionNodeObject childDecision = entry.getValue();
//            List<DecisionNodeEntryObject> childChild = childDecision.getChildNodes();
//        }
		JSONObject tJSONObject=null;
		try {
			tJSONObject = new JSONObject(str);
		} catch (JSONException e) {
			System.out.println("========返回Json数据格式错误");
			e.printStackTrace();
		}
		//获取案件的CaseUuid
		String caseUuid = tJSONObject.getString("caseUuid");
		resultMap.put("CaseUuid", caseUuid);
		//获取案件decision
		JSONObject caseJSONObject = tJSONObject.getJSONObject("decision");
		//获取案件结论decision
		JSONObject caseSubJSONObject = caseJSONObject.getJSONObject("decision");
		JSONArray supportCodes = new JSONArray();
		try{
			supportCodes = caseSubJSONObject.getJSONArray("supportCodes");
		}catch(Exception e){
		}
		resultMap.put("SupportCodes", String.valueOf(supportCodes.length()));
		//获取案件主结论
		JSONObject mainJSONObject = caseSubJSONObject.getJSONObject("decision");
		String mainDecision = mainJSONObject.getString("internalCode");
		resultMap.put("MainDecision", mainDecision);
        return resultMap;
    }

//    protected static SubmitCaseDataResponseObject readJson(String strJson) throws IOException {
//        return new ObjectMapper().readValue(strJson, SubmitCaseDataResponseObject.class);
//    }
//     
    public static void main(String[] args) throws Exception {
    	String params="{\n" +
 	            "  \"externalCaseUuid\": \"some id\",\n" +
 	            "  \"rulebaseUuid\": \"1fc8ed5f-444e-4c9a-9d59-c0ff7bc7eb02\",\n" +
 	            "  \"languageCode\": \"en_GB\",\n" +
 	            "  \"dataPackages\": [\n" +
 	            "  {\n" +
 	            "    \"attributes\": [\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"8b35b80a-e9e4-45cf-8864-8f8e68f56407\",\n" +
 	            "        \"attribute\": \"case.countryOfContract\",\n" +
 	            "        \"questionDefinitionUuid\": \"e8b63e25-f541-4511-9909-cba88d6d675d\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"United Kingdom\",\n" +
 	            "              \"uuid\": \"d061d24e-5332-483f-bf68-3eb88d52e978\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"7d3ae56d-7213-4fce-b362-175c35e5cc27\",\n" +
 	            "        \"attribute\": \"case.SalesChannel\",\n" +
 	            "        \"questionDefinitionUuid\": \"4d3db870-5aab-4062-b673-ebed715a2f99\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"Broker\",\n" +
 	            "              \"uuid\": \"0a5f5e0e-a39f-4d49-9b3b-3e39d8ee19f5\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"23bc49f1-f47d-4700-abd0-b21b0623b4f4\",\n" +
 	            "        \"attribute\": \"case.Agency\",\n" +
 	            "        \"questionDefinitionUuid\": \"d89532fb-0cdc-405e-8f52-8c775b0dce49\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"stringValue\": \"211212\"\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"a2526a3c-5213-4f5a-93e8-6cdcf6b7fc72\",\n" +
 	            "        \"attribute\": \"case.ClientPresent\",\n" +
 	            "        \"questionDefinitionUuid\": \"d59dfde9-fa70-411b-965d-ef67d65f5d74\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"YES\",\n" +
 	            "              \"uuid\": \"fc7610f9-7c80-4182-b815-a82bff28524c\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"1b8fab1f-8a55-4002-baa4-db40210cb792\",\n" +
 	            "        \"attribute\": \"case.CurrencyCode\",\n" +
 	            "        \"questionDefinitionUuid\": \"7a4a513b-4bb7-42ec-90de-3cce7770d12a\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"EUR\",\n" +
 	            "              \"uuid\": \"64823feb-0163-45ca-b266-e5e6d1eee44f\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"034d8a33-0a86-4a4e-a626-48743533bb2d\",\n" +
 	            "        \"attribute\": \"case.life[0].personal.name\",\n" +
 	            "        \"questionDefinitionUuid\": \"f2a16d67-45e6-4fc9-8123-1405f0f21f14\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"stringValue\": \"John\"\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"ecb3b333-1640-49ba-a4a8-005b84bde0bb\",\n" +
 	            "        \"attribute\": \"case.life[0].personal.lastname\",\n" +
 	            "        \"questionDefinitionUuid\": \"2ead2119-8b4c-4692-bc9f-e5cd0e9bfc62\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"stringValue\": \"Snow\"\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"89d89095-27cc-410a-9078-d7ea4300ebc0\",\n" +
 	            "        \"attribute\": \"case.life[0].personal.sex\",\n" +
 	            "        \"questionDefinitionUuid\": \"5e2b0bb4-ba0d-4fc8-bec2-520a13d9c1ac\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"MALE\",\n" +
 	            "              \"uuid\": \"2c092228-bc36-4208-874f-9c3082f59bab\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"68442c8d-76ba-4277-a53c-939ad1750fc6\",\n" +
 	            "        \"attribute\": \"case.life[0].personal.dateOfBirth\",\n" +
 	            "        \"questionDefinitionUuid\": \"28f53a8b-9009-47c7-83fc-3231e4f05784\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"dateValue\": \"1998-04-30\"\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"e46a8870-d116-4213-8f1e-61bab527b575\",\n" +
 	            "        \"attribute\": \"case.life[0].personal.smoker\",\n" +
 	            "        \"questionDefinitionUuid\": \"3fa9d730-8037-40fe-bb8a-30c6abc6d010\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"SMOKER\",\n" +
 	            "              \"uuid\": \"3c718d43-4db5-471c-bfc7-9ead40a91d97\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"7ea63e4e-6bba-4dcd-90e4-d26eaa19dc03\",\n" +
 	            "        \"attribute\": \"case.life[0].product[0].type\",\n" +
 	            "        \"questionDefinitionUuid\": \"1a42f5ef-ad41-4e3e-a8b0-3408e954755f\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"MORTGAGE_PROTECTOR\",\n" +
 	            "              \"uuid\": \"778607f5-ccc4-4966-902a-c950ad3faf08\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"value\": {\n" +
 	            "          \"intValue\": 0\n" +
 	            "        },\n" +
 	            "        \"attribute\": \"case.life[0].product[0].ID\"\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"9d3b1f75-f4f0-44d4-8188-2215a66e85b0\",\n" +
 	            "        \"attribute\": \"case.life[0].product[0].LifeRole\",\n" +
 	            "        \"questionDefinitionUuid\": \"d311e3bb-7776-4850-979a-cd5e9fa0baab\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"PROPOSED\",\n" +
 	            "              \"uuid\": \"f3ede9d2-12a2-4b2a-a498-cd3f903dcfb4\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"ec05c46c-2463-4376-bfb1-f8a1a3950d8a\",\n" +
 	            "        \"attribute\": \"case.life[0].product[0].benefit[0].type\",\n" +
 	            "        \"questionDefinitionUuid\": \"57a26ac8-d02e-4351-aa10-f6b189ef1555\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"CI\",\n" +
 	            "              \"uuid\": \"0ca13a68-9c02-4e74-8f9b-f217c164a093\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"8bd879e6-d09a-44ce-a2f3-097317a7f3ed\",\n" +
 	            "        \"attribute\": \"case.life[0].product[0].benefit[0].amount\",\n" +
 	            "        \"questionDefinitionUuid\": \"c82b6979-747b-4520-83db-0d72d4793ae5\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"intValue\": 135000\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"e4ad3fd7-84ed-416e-b8ed-8e14b401c422\",\n" +
 	            "        \"attribute\": \"case.life[0].product[0].benefit[0].termBasis\",\n" +
 	            "        \"questionDefinitionUuid\": \"5fd1b072-63f7-4fd4-808d-750dda6e1480\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"referenceDataValues\": [\n" +
 	            "            {\n" +
 	            "              \"code\": \"YEARS\",\n" +
 	            "              \"uuid\": \"f8b8a32e-b7d5-4d93-8d22-22f199c327e1\"\n" +
 	            "            }\n" +
 	            "          ]\n" +
 	            "        }\n" +
 	            "      },\n" +
 	            "      {\n" +
 	            "        \"attributeDefinitionUuid\": \"0fedeaff-d8e6-45cd-bd5d-cdc7405d5b1c\",\n" +
 	            "        \"attribute\": \"case.life[0].product[0].benefit[0].term\",\n" +
 	            "        \"questionDefinitionUuid\": \"f8dbdea2-4558-48d0-b95a-4e90d09a804f\",\n" +
 	            "        \"value\": {\n" +
 	            "          \"intValue\": 2\n" +
 	            "        }\n" +
 	            "      }\n" +
 	            "    ]\n" +
 	            "  }\n" +
 	            "  ]\n" +
 	            "}";
    	System.out.println("===   "+params);
    	String prtno = "16181210002";
    	Map<String, String> data = new PadMagNumReadJson().dealPadMagNumRequestJson(params,prtno);
    	for(int i = 0; i < data.size();i++){
    	String s = data.get("ResultJson");
    	String code = data.get("StatusCode");
		//String Size = (String) tTransferData.getValueByName("Size");
    	 System.out.println("  ====   "+s);
    	 System.out.println("  ====   "+code);
    	}
	}
}
