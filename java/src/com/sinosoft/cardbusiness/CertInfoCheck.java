package com.sinosoft.cardbusiness;

public final class CertInfoCheck {

	public static boolean verifyMsgHead(StringBuffer tErrInfos,
			String tBatchNo, String tSendDate, String tSendTime,
			String tBranchCode, String tSendOperator, String tMsgType) {
		if (tBatchNo == null || "".equals(tBatchNo) || tSendDate == null
				|| "".equals(tSendDate) || tSendTime == null
				|| "".equals(tSendTime)) {
			String tErrMsg = "请检验文件头中是否有非空项未录";
			errLog(tErrInfos, tErrMsg);
			return false;
		}
		if (!("UnionPay".equals(tBranchCode))
				&& !("UnionPay".equals(tSendOperator))) {
			String tErrMsg = "报送类型BranchCode和报送机构SendOperator为平台系统标识 固定不变 UnionPay";
			errLog(tErrInfos, tErrMsg);
			return false;
		}
		if (!("UP0001".equals(tMsgType))) {
			String tErrMsg = "MsgType平台系统标识 固定不变 UP0001";
			errLog(tErrInfos, tErrMsg);
			return false;
		}
		return true;
	}

	private static void errLog(StringBuffer tErrInfos, String tErrMsg) {
		System.out.println("CertInfoManager: " + tErrMsg);
		if (tErrInfos == null)
			return;
		tErrInfos.append(tErrMsg);

	}

	public static boolean verifySimPolicyInfo(StringBuffer tErrInfos,
			String polApplyDate, String cValiDate, String cInValiDate,
			String premScope, String payIntv) {
		if (polApplyDate == null || "".equals(polApplyDate)
				|| cValiDate == null || "".equals(cValiDate)
				|| cInValiDate == null || "".equals(cInValiDate)
				|| premScope == null || "".equals(premScope) || payIntv == null
				|| "".equals(payIntv)) {
			String tErrMsg = "请检验SimPolicyInfo中是否有非空项未录";
			errLog(tErrInfos, tErrMsg);
			return false;
		}
		return true;
	}

	public static boolean verifyWrapInfo(StringBuffer tErrInfos,
			String wrapCode, String certifyCode, String activeDate,
			String prem, String mult, String copys) {
		{
			if (wrapCode == null || "".equals(wrapCode) || certifyCode == null
					|| "".equals(certifyCode) || activeDate == null
					|| "".equals(activeDate) || prem == null || "".equals(prem)
					|| mult == null || "".equals(mult) || copys == null
					|| "".equals(copys)) {
				String tErrMsg = "请检验产品套餐WrapInfo中是否有非空项未录";
				errLog(tErrInfos, tErrMsg);
				return false;
			}
			return true;
		}
	}

	public static boolean verifySimAppntInfo(StringBuffer tErrInfos,
			String name, String sex, String Birthday, String iDType,
			String idNo, String phone, String mobile) {
		if (("01").equals(iDType)) {
			if (name == null || "".equals(name) || idNo == null
					|| "".equals(idNo) || phone == null || "".equals(phone)
					|| mobile == null || "".equals(mobile)) {
				String tErrMsg = "请检验投保人SimAppntInfo中是否有非空项未录";
				errLog(tErrInfos, tErrMsg);
				return false;
			}

		} else if (("02").equals(iDType) || ("03").equals(iDType)
				|| ("04").equals(iDType) || ("05").equals(iDType)
				|| ("06").equals(iDType) || ("07").equals(iDType)
				|| ("99").equals(iDType)) {
			if (name == null || "".equals(name) || iDType == null
					|| "".equals(iDType) || idNo == null || "".equals(idNo)
					|| phone == null || "".equals(phone) || sex == null
					|| "".equals(sex) || mobile == null || "".equals(mobile)
					|| Birthday == null || "".equals(Birthday)) {
				String tErrMsg = "请检验投保人SimAppntInfo中是否有非空项未录";
				errLog(tErrInfos, tErrMsg);
				return false;
			}
		} else {
			String tErrMsg = "证件类型有误";
			errLog(tErrInfos, tErrMsg);
			return false;
		}
		return true;
	}

	public static boolean verifySimInsuInfo(StringBuffer tErrInfos,
			String bInsuNo, String bToMainInsuRela, String bToAppntRela,
			String bName, String bSex, String bBirthday, String bIDType,
			String bIdNo, String bOccupationType, String bOccupationCode,
			String bPhone, String bMobile) {
		if (("01").equals(bIDType)) {
			if (bName == null || "".equals(bName) || bIdNo == null
					|| "".equals(bIdNo) || bPhone == null || "".equals(bPhone)
					|| bMobile == null || "".equals(bMobile)) {
				String tErrMsg = "请检验投保人SimAppntInfo中是否有非空项未录";
				errLog(tErrInfos, tErrMsg);
				return false;
			}

		} else if (("02").equals(bIDType) || ("03").equals(bIDType)
				|| ("04").equals(bIDType) || ("05").equals(bIDType)
				|| ("06").equals(bIDType) || ("07").equals(bIDType)
				|| ("99").equals(bIDType)) {
			if (bName == null || "".equals(bName) || bIdNo == null
					|| "".equals(bIdNo) || bPhone == null || "".equals(bPhone)
					|| bSex == null || "".equals(bSex) || bMobile == null
					|| "".equals(bMobile) || bBirthday == null
					|| "".equals(bBirthday)) {
				String tErrMsg = "请检验投保人SimAppntInfo中是否有非空项未录";
				errLog(tErrInfos, tErrMsg);
				return false;
			}
		} else {
			String tErrMsg = "证件类型有误";
			errLog(tErrInfos, tErrMsg);
			return false;
		}

		return true;
	}
}