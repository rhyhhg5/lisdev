package com.sinosoft.cardbusiness;
import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.sinosoft.lis.pubfun.PubFun;

public class SimPolicyInfoBL {
	public static void main(String[] args) {
		String cardno ="XYZ00000001";
		UnionPayprintBL ecp = new UnionPayprintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo("UP0001UPY2015111614525844758");
		tMsgHead.setBranchCode("001");
		tMsgHead.setMsgType("INDIGO");
		tMsgHead.setSendDate(PubFun.getCurrentDate());
		tMsgHead.setSendTime(PubFun.getCurrentTime());
		tMsgHead.setSendOperator("EC_WX");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(cardno);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
		ecp.deal(tMsgCollection);
	}}
