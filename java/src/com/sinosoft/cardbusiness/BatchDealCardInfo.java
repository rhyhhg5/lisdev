package com.sinosoft.cardbusiness;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.SimAppntInfo;
import com.cbsws.obj.SimInsuInfo;
import com.cbsws.obj.SimPolicyInfo;
import com.cbsws.obj.WrapInfo;
import com.cbsws.xml.ctrl.blogic.simsale.CardNoManager;
import com.sinosoft.lis.db.LCUnionPayDetailDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCUnionPayDetailSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LCUnionPayDetailSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.midplat.kernel.service.check.IdNoCheck;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class BatchDealCardInfo extends ABusLogic {
	/* 公共错误处理类 */
	public CErrors mErrors = new CErrors();
	/* 与报文头对应的对象 */
	private MsgHead mMsgHead = null;
	/* 与报文节点对应的对象 */
	private SimAppntInfo mMsgSimAppntInfo = null;
	private SimPolicyInfo mMsgSimPolicyInfo = null;
	private WrapInfo mMsgWrapInfo = null;
	// private SimInsuInfo[] mMsgSimInsuInfoList = null;
	private SimInsuInfo mMsgSimInsuInfo = null;

	/* 往后传数据的Schema、Set对象 */
	// private WFInsuListSet tWFInsuInfoSet = new WFInsuListSet();
	private WFInsuListSchema tWFInsuInfo = new WFInsuListSchema();
	private WFAppntListSchema tWFAppntInfo = new WFAppntListSchema();
	private WFContListSchema tWFContInfo = new WFContListSchema();
	// private LICardActiveInfoListSet tLICardActiveInfos = new
	// LICardActiveInfoListSet();
	private LICardActiveInfoListSchema tLICardActiveInfo = new LICardActiveInfoListSchema();
	private LZCardSchema tLZCardInfo = new LZCardSchema();
	private LCUnionPayDetailSchema tLCUnionPayDetailSchema=new LCUnionPayDetailSchema();
	private CardNoManager mCardNoManager = new CardNoManager();

	private String BittchNo = "";
	private String ContNo = "";
	private String InsuYear = "";
	private String calPrem = "";
	private String calAmnt = "";
	String tsex = "t";
	String bsex = "b";
	/* 投保人 */

	boolean messageFlag = true;

	/* 存储Schema、Set向后台提交数据的Map */
	private MMap mMap = new MMap();
	/* 向后面传输数据的集合 */
	VData tVData = new VData();
	/* 返回结果的集合 */
	VData mResult = new VData();
	/* 发送信息时传递的Map集合 */
	private HashMap mAndeMap = new HashMap();

	 Date dt=new Date();
     SimpleDateFormat matter1=new SimpleDateFormat("yyyy-MM-dd");
     SimpleDateFormat matter2=new SimpleDateFormat("HH:mm:ss");
     String date=matter1.format(dt);
     String time=matter2.format(dt);
	
	protected boolean deal(LCUnionPayDetailSchema tLCUnionPayDetailSchema) {
		// 解析报文，获得报文数据
		if (!parseDatas(tLCUnionPayDetailSchema)) {
			return false;
		}
		BittchNo = mMsgHead.getBatchNo();
		// 获取卡号、密码 WR0204/D65005
		if (!getCardInfo()) {
			return false;
		}
		// 校验数据的合法性
		if (!chkCertInfo()) {
			return false;
		}
		// 处理产品算费要素
		if (!dealWrapParams()) {
			return false;
		}
		// 处理业务数据
		if (!tradeService()) {
			return false;
		}

		// 提交数据库
		PubSubmit p = new PubSubmit();
		if (!p.submitData(tVData, "")) {
			System.out.println("提交数据失败");
			errLog("提交数据失败");
			return false;
		}
		
		// 发送短信、邮件
		sendSmsAndEmail();
		// 生成电子保单，上传服务器
		ContNo = mMsgWrapInfo.getCardNo();
		System.out.println(ContNo);
		makeUnionPayPDF(ContNo);

		return true;
	}


	private boolean getCardInfo() {
		String tWrapCode = mMsgWrapInfo.getWrapCode();
		String tCertifyCode = mMsgWrapInfo.getCertifyCode();
		String tMsgType = mMsgHead.getMsgType();
		String[] info = mCardNoManager.getCardNo(tWrapCode, tCertifyCode,
				tMsgType);
		if (info == null) {
			errLog("获取卡号过程出现异常，稍候请尝试重新交易");
			return false;
		} else {
			if (info[2] != null) {
				errLog(info[2]);
				return false;
			} else {
				mMsgWrapInfo.setCardNo(info[0]);
				mMsgWrapInfo.setPassword(info[1]);
				System.out.println("套餐编码为：" + tWrapCode + "试算过程中取得的卡号为："
						+ mMsgWrapInfo.getCardNo() + "取得的密码为："
						+ mMsgWrapInfo.getPassword());
			}

		}
		return true;
	}

	private boolean dealWrapParams() {

		LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

		String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
				+ " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
				+ " where lmcr.CertifyCode = '"
				+ mMsgWrapInfo.getCertifyCode()
				+ "' "
				+ " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
		tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
		System.out.println(tStrSql);
		if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0) {
			String tStrErr = "单证号[" + mMsgWrapInfo.getCardNo()
					+ "]对应套餐要素信息未找到。";
			buildError("dealWrapParams", tStrErr);
			System.out.println(tStrErr);
			return false;
		}

		tStrSql = null;

		double tSysWrapSumPrem = 0;
		double tSysWrapSumAmnt = 0;

		for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);
			String tCalFactorType = tRiskDutyParam.getCalFactorType();
			String tCalFactor = tRiskDutyParam.getCalFactor();
			String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

			if ("Prem".equals(tCalFactor)) {
				String tTmpCalResult = null;
				if ("2".equals(tCalFactorType)) {
					PubCalculator tCal = new PubCalculator();
					tCal.addBasicFactor("Copys",
							String.valueOf(mMsgWrapInfo.getCopys()));
					tCal.addBasicFactor("Mult",
							String.valueOf(mMsgWrapInfo.getMult()));
					tCal.addBasicFactor("InsuYear", String.valueOf(InsuYear));
					// 计算
					tCal.setCalSql(tRiskDutyParam.getCalSql());
					tTmpCalResult = tCal.calculate();
					if (tTmpCalResult == null || tTmpCalResult.equals("")) {
						String tStrErr = "单证号[" + mMsgWrapInfo.getCardNo()
								+ "]保费计算失败。";
						buildError("dealWrapParams", tStrErr);
						System.out.println(tStrErr);
						return false;
					}
				} else {
					tTmpCalResult = tCalFactorValue;
				}

				BigDecimal b = new BigDecimal(tTmpCalResult);
				calPrem = b.setScale(2, BigDecimal.ROUND_HALF_UP)
						.toPlainString();

				try {
					tSysWrapSumPrem = Arith.add(tSysWrapSumPrem,
							new BigDecimal(tTmpCalResult).doubleValue());
				} catch (Exception e) {
					String tStrErr = "套餐要素中保费出现非数值型字符串。";
					buildError("dealWrapParams", tStrErr);
					return false;
				}
			}

			if ("Amnt".equals(tCalFactor)) {
				String tTmpCalResult = null;
				if ("2".equals(tCalFactorType)) {
					PubCalculator tCal = new PubCalculator();
					tCal.addBasicFactor("Copys",
							String.valueOf(mMsgWrapInfo.getCopys()));
					tCal.addBasicFactor("Mult",
							String.valueOf(mMsgWrapInfo.getMult()));
					tCal.addBasicFactor("InsuYear", String.valueOf(InsuYear));
					// 计算
					tCal.setCalSql(tRiskDutyParam.getCalSql());
					tTmpCalResult = tCal.calculate();
					if (tTmpCalResult == null || tTmpCalResult.equals("")) {
						String tStrErr = "单证号[" + mMsgWrapInfo.getCardNo()
								+ "]保额计算失败。";
						buildError("dealWrapParams", tStrErr);
						System.out.println(tStrErr);
						return false;
					}
				} else {
					tTmpCalResult = tCalFactorValue;
				}

				BigDecimal b = new BigDecimal(tTmpCalResult);
				calAmnt = b.setScale(2, BigDecimal.ROUND_HALF_UP)
						.toPlainString();

				try {
					tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt,
							new BigDecimal(tTmpCalResult).doubleValue());
				} catch (Exception e) {
					String tStrErr = "套餐要素中保额出现非数值型字符串。";
					buildError("dealWrapParams", tStrErr);
					return false;
				}
			}
		}

		return true;
	}

	private void makeUnionPayPDF(String ContNo) {
		String cardno = ContNo;
		UnionPayprintBL ecp = new UnionPayprintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo(BittchNo);
		tMsgHead.setBranchCode(mMsgHead.getBranchCode());
		tMsgHead.setMsgType(mMsgHead.getMsgType());
		tMsgHead.setSendDate(mMsgHead.getSendDate());
		tMsgHead.setSendTime(mMsgHead.getSendTime());
		tMsgHead.setSendOperator(mMsgHead.getSendOperator());
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(cardno);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
		ecp.deal(tMsgCollection);
	}

	private void sendSmsAndEmail() {
		if ("".equals(BittchNo) || BittchNo == null) {
			System.out.println("银联获取批次号失败!");
		}
		ExeSQL tExeSql = new ExeSQL();
		String tStrSql2 = "select wfa.name,wfa.mobile ,wfa.email "
				+ "from wfappntlist wfa where wfa.batchno = '" + BittchNo
				+ "' and wfa.cardno = '" + mMsgWrapInfo.getCardNo() + "' ";
		SSRS checkSSRS2 = tExeSql.execSQL(tStrSql2);
		if (checkSSRS2 == null || checkSSRS2.MaxRow == 0) {
			System.out.println("获取发送短信和邮件信息失败!");

		}
		String tcertifyName = checkSSRS2.GetText(1, 1);

		String tmobile = checkSSRS2.GetText(1, 2);

		String tproposerEmail = checkSSRS2.GetText(1, 3);

		String tStrSql = "select distinct lic.cardno,"
				+ " lic.mult,"
				+ "lic.cvalidate,lic.inactivedate "
				+ "from wfcontlist wfc,licardactiveinfolist lic where wfc.batchno = '"
				+ BittchNo + "' " + "and lic.cardno = wfc.cardno "
				+ "and lic.cardno = '" + mMsgWrapInfo.getCardNo() + "' ";
		SSRS checkSSRS = tExeSql.execSQL(tStrSql);
		if (checkSSRS.MaxRow == 0) {
			System.out.println("获取发送短信和邮件信息失败!");

		}
		int length = checkSSRS.MaxRow;
		for (int i = 0; i < length; i++) {
			String tcardNo = "";
			String tCValiDate = "";
			String tCInValiDate = "";
			tcardNo = checkSSRS.GetText(i + 1, 1);
			String tmult = checkSSRS.GetText(1, 2);
			tCValiDate = checkSSRS.GetText(i + 1, 3);
			tCInValiDate = checkSSRS.GetText(i + 1, 4);
			System.out.println("tcardNo=" + tcardNo);
			System.out.println("tCValiDate=" + tCValiDate);
			System.out.println("tCInValiDate=" + tCInValiDate);
			mAndeMap.put("cardNo" + i, tcardNo);
			if (i == length - 1) {
				mAndeMap.put("endindex", "" + i);
			}
			String tWrapName = "";
			if("1".equals(tmult)){
				tWrapName = "银标钻石信用卡航空意外险";
			}else if("2".equals(tmult)){
				tWrapName = "银标白金信用卡航空意外险";
			}else if("3".equals(tmult)){
				tWrapName = "公务卡、商务卡、旅游卡航空意外险";
			}else if("4".equals(tmult)){
				tWrapName = "银标信用卡金卡、普卡航空意外险";
			}else if("5".equals(tmult)){
				tWrapName = "配偶航空意外险";
			}else if("6".equals(tmult)){
				tWrapName = "子女航空意外险";
			}
			mAndeMap.put("WrapName" + i, tWrapName);
			mAndeMap.put("CValiDate" + i, tCValiDate);
			mAndeMap.put("CInValiDate" + i, tCInValiDate);
		}
		mAndeMap.put("certifyName", tcertifyName);
		mAndeMap.put("mobile", tmobile);
		mAndeMap.put("orderID", BittchNo);
		mAndeMap.put("proposerEmail", tproposerEmail);
		mAndeMap.put("ProposerNeedSMS", "T");
		mAndeMap.put("ProposerName", tcertifyName);

		/*
		 * boolean emailFlag = true; String errinfo = "";
		 */
		if (mAndeMap.get("ProposerNeedSMS").equals("T")) {
			System.out.println(((String) mAndeMap.get("mobile")).length()
					+ "----------mobile");
			// 短息通知
			if (mAndeMap.get("mobile") != null
					&& !("").equals(mAndeMap.get("mobile"))) {
				if (new UnionPayEmailAndMessageBL().sendMsg(mAndeMap)) {
					System.out.println("手机号为" + mAndeMap.get("mobile")
							+ "的短息发送成功");

				} else {
					messageFlag = false;
					System.out.println("手机号为" + mAndeMap.get("mobile")
							+ "的短息发送失败");

				}

			}
		}

	}

	private boolean chkCertInfo() {
		String tBranchCode = mMsgHead.getBranchCode();
		String tSendOperator = mMsgHead.getSendOperator();
		String tMsgType = mMsgHead.getMsgType();

		if (!("UnionPay".equals(tBranchCode))
				&& !("UnionPay".equals(tSendOperator))) {			
			errLog( "报送类型BranchCode和报送机构SendOperator为平台系统标识 固定不变 UnionPay");
			return false;
		}
		if (!("UP0001".equals(tMsgType))) {
			errLog( "MsgType平台系统标识 固定不变 UP0001");
			
			return false;
		}
		//增加身份证校验
		String tIDType = mMsgSimAppntInfo.getIDType();
		String tIdNo = mMsgSimAppntInfo.getIdNo();
		String tBirthday = mMsgSimAppntInfo.getBirthday();
		String tSex = mMsgSimAppntInfo.getSex();
		if(tIdNo!=null && !tIdNo.equals("")){
			if(!PubFun.CheckIDNo(tIDType, tIdNo, tBirthday, tSex).equals("")){
				errLog( "投保人身份证号信息输入有误！--->BatchDealCardInfo.java");
				return false;
			}
		}
		
		return true;

	}

	private boolean parseDatas(LCUnionPayDetailSchema tLCUnionPayDetailSchema) {
		// 报文头
		mMsgHead=new MsgHead();
		mMsgHead.setBatchNo(tLCUnionPayDetailSchema.getBatchNo());
		mMsgHead.setSendDate(date);
		mMsgHead.setSendTime(time);
		mMsgHead.setBranchCode("UnionPay");
		mMsgHead.setSendOperator("UnionPay");
		mMsgHead.setMsgType("UP0001");

		// 保单信息
		mMsgSimPolicyInfo=new SimPolicyInfo();
		String tCValiDate=tLCUnionPayDetailSchema.getCValiDate().substring(0, 10);
		String tCValiTime=tLCUnionPayDetailSchema.getCValiDate().substring(11, 19);
		String tCInValiDate=tLCUnionPayDetailSchema.getCInValiDate().substring(0, 10);
		String tCInValitime=tLCUnionPayDetailSchema.getCInValiDate().substring(11, 19);	
		mMsgSimPolicyInfo.setPolApplyDate(date);
		mMsgSimPolicyInfo.setCValiDate(tCValiDate);
		mMsgSimPolicyInfo.setCValiTime(tCValiTime);
		mMsgSimPolicyInfo.setCInValiDate(tCInValiDate);
		mMsgSimPolicyInfo.setCInValiTime(tCInValitime);
		mMsgSimPolicyInfo.setPremScope("0");
		mMsgSimPolicyInfo.setPayIntv("0");
		mMsgSimPolicyInfo.setBankAccNo(tLCUnionPayDetailSchema.getAppntBankAccNo());

		// 投保人信息
		mMsgSimAppntInfo=new SimAppntInfo();
		mMsgSimAppntInfo.setName(tLCUnionPayDetailSchema.getAppntName());
		mMsgSimAppntInfo.setSex(tLCUnionPayDetailSchema.getAppntSex());
		mMsgSimAppntInfo.setBirthday(tLCUnionPayDetailSchema.getAppntBirthday());
		mMsgSimAppntInfo.setIDType(tLCUnionPayDetailSchema.getAppntIDType());
		mMsgSimAppntInfo.setIdNo(tLCUnionPayDetailSchema.getAppntIDNo());
		mMsgSimAppntInfo.setMobile(tLCUnionPayDetailSchema.getAppntPhone());
		System.out.println("姓名："+mMsgSimAppntInfo.getName());

		// 被保人信息（批量投保被保人信息为空）
		mMsgSimInsuInfo=new SimInsuInfo();
		mMsgSimInsuInfo.setInsuNo("1");
		mMsgSimInsuInfo.setToAppntRela("0");
		mMsgSimInsuInfo.setToMainInsuRela("00");
		
//		mMsgSimInsuInfo.setIdNo(tLCUnionPayDetailSchema.getInsuredIDNo());
//		mMsgSimInsuInfo.setBirthday(tLCUnionPayDetailSchema.getInsuredBirthday());
//		mMsgSimInsuInfo.setIDType(tLCUnionPayDetailSchema.getInsuredIDType());
//		mMsgSimInsuInfo.setSex(tLCUnionPayDetailSchema.getInsuredSex());
		//
		mMsgWrapInfo=new WrapInfo();
		mMsgWrapInfo.setWrapCode(tLCUnionPayDetailSchema.getWrapCode());
		mMsgWrapInfo.setCertifyCode(tLCUnionPayDetailSchema.getCertifyCode());
		mMsgWrapInfo.setActiveDate(date);
		mMsgWrapInfo.setPrem("0");
		mMsgWrapInfo.setMult(tLCUnionPayDetailSchema.getCardLevel());
		mMsgWrapInfo.setCopys("1");
		
		
		InsuYear = (new ExeSQL().getOneValue("select days('"
				+ mMsgSimPolicyInfo.getCInValiDate() + "') - days('"
				+ mMsgSimPolicyInfo.getCValiDate() + "') from dual")).trim();

		return true;
	}

	private boolean tradeService() {		
		tWFAppntInfo = getAppntInfo();
		if (tWFAppntInfo == null) {
			String tErrInfo = "查询投保人信息失败。";
			errLog(tErrInfo);
			return false;
		}
		tWFInsuInfo = getInsuListSetInfo();
		if (tWFInsuInfo == null) {
			String tErrInfo = "查询被保人信息失败。";
			errLog(tErrInfo);
			return false;
		}

		tWFContInfo = getSimPolicyInfo();
		if (tWFContInfo == null) {
			String tErrInfo = "查询保单信息失败。";
			errLog(tErrInfo);
			return false;
		}
		tLICardActiveInfo = getwrapInfo();
		if (tLICardActiveInfo == null) {
			String tErrInfo = "中间表信息缺失。";
			errLog(tErrInfo);
			return false;
		}
		tLZCardInfo = getLZCardInfo();
		if (tLZCardInfo == null) {
			String tErrInfo = "单证状态表信息缺失。";
			errLog(tErrInfo);
			return false;
		}

		tLCUnionPayDetailSchema = getLCUnionPayDetailSchema();
		if(tLCUnionPayDetailSchema == null){
			String tErrInfo = "银联表信息缺失。";
			errLog(tErrInfo);
			return false;
		}
		
		tVData.clear();
		mMap=new MMap();
		mMap.put(tWFAppntInfo, SysConst.INSERT);
		mMap.put(tWFInsuInfo, SysConst.INSERT);
		mMap.put(tWFContInfo, SysConst.INSERT);
		mMap.put(tLICardActiveInfo, SysConst.INSERT);
		mMap.put(tLZCardInfo, SysConst.UPDATE);
		mMap.put(tLCUnionPayDetailSchema, SysConst.UPDATE);
		tVData.add(mMap);

		mResult.clear();
		mResult.add(tWFContInfo);
		mResult.add(tWFAppntInfo);
		mResult.add(tWFInsuInfo);
		mResult.add(tLICardActiveInfo);
		mResult.add(tLCUnionPayDetailSchema);

		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		System.out.println(szFunc + ":" + szErrMsg);

		CError cError = new CError();
		cError.moduleName = "CertifyGrpContBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private WFInsuListSchema getInsuListSetInfo() {
		// 被保人信息
		tWFInsuInfo = new WFInsuListSchema();
		tWFInsuInfo.setInsuNo(mMsgSimInsuInfo.getInsuNo());// *
		tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());// *
		tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());// *
		tWFInsuInfo.setRelationToInsured("00");
		tWFInsuInfo.setToAppntRela(mMsgSimInsuInfo.getToAppntRela());
		String ToAppntRela = mMsgSimInsuInfo.getToAppntRela();
		if ("0".equals(ToAppntRela)) {// 若为本人投保则在投保人信息中获取五要素
			tWFInsuInfo.setToAppntRela("00");
			tWFInsuInfo.setIDNo(mMsgSimAppntInfo.getIdNo());
			tWFInsuInfo.setName(mMsgSimAppntInfo.getName());
			if (("01").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("0");// 身份证
				String idno = mMsgSimAppntInfo.getIdNo();
				if (idno.length() == 18) {
					tWFInsuInfo.setBirthday(idno.substring(6, 10) + "-"
							+ idno.substring(10, 12) + "-"
							+ idno.substring(12, 14));
					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女
					} else {
						tWFInsuInfo.setSex("0");
					}
				} else {
					String sSex = idno.substring(14);
					tWFInsuInfo.setBirthday("19" + idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女
					} else {
						tWFInsuInfo.setSex("0");
					}
				}
			} else if (("02").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("2");// 军官证
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

			} else if (("03").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("1");// 护照
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

			} else if (("04").equals(mMsgSimAppntInfo.getIDType())
					|| ("05").equals(mMsgSimAppntInfo.getIDType())
					|| ("99").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("4");// 回乡证/台胞证
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

			}

			else if (("06").equals(mMsgSimAppntInfo.getIDType())
					|| ("07").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("3");// 警官，士兵
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
			}
		} 
		tWFInsuInfo.setOccupationType(mMsgSimInsuInfo.getOccupationType());
		tWFInsuInfo.setOccupationCode(mMsgSimInsuInfo.getOccupationCode());
		tWFInsuInfo.setPostalAddress(mMsgSimInsuInfo.getPostalAddress());
		tWFInsuInfo.setZipCode(mMsgSimInsuInfo.getZipCode());
		tWFInsuInfo.setPhont(mMsgSimInsuInfo.getPhone());
		tWFInsuInfo.setMobile(mMsgSimInsuInfo.getMobile());
		tWFInsuInfo.setEmail(mMsgSimInsuInfo.getEmail());
		tWFInsuInfo.setGrpName(mMsgSimInsuInfo.getGrpName());
		tWFInsuInfo.setCompanyPhone(mMsgSimInsuInfo.getCompayPhone());
		tWFInsuInfo.setCompAddr(mMsgSimInsuInfo.getCompAddr());
		tWFInsuInfo.setCompZipCode(mMsgSimInsuInfo.getCompZipCode());
		return tWFInsuInfo;
	}

	private WFAppntListSchema getAppntInfo() {
		tWFAppntInfo = new WFAppntListSchema();
		tWFAppntInfo.setBatchNo(mMsgHead.getBatchNo());// *
		tWFAppntInfo.setCardNo(mMsgWrapInfo.getCardNo());// *
		tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());// *
		tWFAppntInfo.setName(mMsgSimAppntInfo.getName());
		tsex = mMsgSimAppntInfo.getSex();
		if (("01").equals(mMsgSimAppntInfo.getIDType())) {// 证件类型为身份证判断证件长度取得性别和生日
			tWFAppntInfo.setIDType("0");// 身份证
			String idno = mMsgSimAppntInfo.getIdNo();
			if (idno.length() == 18) {
				tWFAppntInfo
						.setBirthday(idno.substring(6, 10) + "-"
								+ idno.substring(10, 12) + "-"
								+ idno.substring(12, 14));
				String Sex = idno.substring(16, 17);
				if (Integer.parseInt(Sex) % 2 == 0) {
					tWFAppntInfo.setSex("1");// 1代表女
					tsex = "1";
				} else {
					tWFAppntInfo.setSex("0");
					tsex = "0";     
				}
			} else {
				String sSex = idno.substring(14);
				tWFAppntInfo.setBirthday("19" + idno.substring(6, 8) + "-"
						+ idno.substring(8, 10) + "-" + idno.substring(10, 12));
				if (Integer.parseInt(sSex) % 2 == 0) {
					tWFAppntInfo.setSex("1");// 1代表女
					tsex = "1";
				} else {
					tWFAppntInfo.setSex("0");
					tsex = "0";
				}
			}
			// 证件类型不为身份证根据对应关系转换存入数据库
		}
		else if (("02").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("2");// 军官证
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

		} else if (("03").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("1");// 护照
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

		} else if (("04").equals(mMsgSimAppntInfo.getIDType())
				|| ("05").equals(mMsgSimAppntInfo.getIDType())
				|| ("99").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("4");// 回乡证/台胞证
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

		}

		else if (("06").equals(mMsgSimAppntInfo.getIDType())
				|| ("07").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("3");// 警官，士兵
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
		}
		
		tWFAppntInfo.setIDNo(mMsgSimAppntInfo.getIdNo());
		tWFAppntInfo.setOccupationType(mMsgSimAppntInfo.getOccupationType());
		tWFAppntInfo.setOccupationCode(mMsgSimAppntInfo.getOccupationCode());
		tWFAppntInfo.setPostalAddress(mMsgSimAppntInfo.getPostalAddress());
		tWFAppntInfo.setZipCode(mMsgSimAppntInfo.getZipCode());
		tWFAppntInfo.setPhont(mMsgSimAppntInfo.getPhone());
		tWFAppntInfo.setMobile(mMsgSimAppntInfo.getMobile());
		tWFAppntInfo.setEmail(mMsgSimAppntInfo.getEmail());
		tWFAppntInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
		tWFAppntInfo.setCompanyPhone(mMsgSimAppntInfo.getCompayPhone());
		tWFAppntInfo.setCompAddr(mMsgSimAppntInfo.getCompAddr());
		tWFAppntInfo.setCompZipCode(mMsgSimAppntInfo.getCompZipCode());

		return tWFAppntInfo;
	}

	private WFContListSchema getSimPolicyInfo() {
		tWFContInfo = new WFContListSchema();
		// 投保单数据
		// tWFContInfo.setPrtNo(mMsgSimPolicyInfo.getPrtNo());
		tWFContInfo.setSaleChnl(mMsgSimPolicyInfo.getSaleChnl());
		tWFContInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
		tWFContInfo.setBatchNo(mMsgHead.getBatchNo());
		tWFContInfo.setSendDate(mMsgHead.getSendDate());
		tWFContInfo.setSendTime(mMsgHead.getSendTime());
		tWFContInfo.setBranchCode(mMsgHead.getBranchCode());
		tWFContInfo.setSendOperator(mMsgHead.getSendOperator());
		tWFContInfo.setMessageType(mMsgHead.getMsgType());// 报文类型（01-出单；02-撤销；99-其他）
		tWFContInfo.setCardDealType("01");
		tWFContInfo.setCertifyCode(mMsgWrapInfo.getCertifyCode());
		tWFContInfo.setActiveDate(mMsgWrapInfo.getActiveDate());
		tWFContInfo.setAppntNo(mMsgWrapInfo.getCardNo());// *
		tWFContInfo.setCardNo(mMsgWrapInfo.getCardNo());

		tWFContInfo.setAgentCom(mMsgSimPolicyInfo.getAgentCom());
		tWFContInfo.setAgentCode(mMsgSimPolicyInfo.getAgentCode());
		tWFContInfo.setAgentName(mMsgSimPolicyInfo.getAgentName());

		tWFContInfo.setSendDate(mMsgSimPolicyInfo.getPolApplyDate());// 报送的日期
		tWFContInfo.setCvaliDate(mMsgSimPolicyInfo.getCValiDate());// *//保单生效日
		tWFContInfo.setCvaliDateTime(mMsgSimPolicyInfo.getCValiTime());
		// 存放保单终止日期
		tWFContInfo.setAmnt(calAmnt);
		tWFContInfo.setPrem(calPrem);
		// tWFContInfo.setPremScope(mMsgSimPolicyInfo.getPremScope());
		tWFContInfo.setInsuYear(InsuYear);
		tWFContInfo.setPayYearFlag("D");
		tWFContInfo.setPayMode(mMsgSimPolicyInfo.getPayMode());
		tWFContInfo.setPayIntv(mMsgSimPolicyInfo.getPayIntv());// 0趸交
		tWFContInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
		tWFContInfo.setInsuYearFlag("D");
		tWFContInfo.setRemark(mMsgSimPolicyInfo.getRemark());
		tWFContInfo.setCopys(mMsgWrapInfo.getCopys()); // 档次
		tWFContInfo.setMult(mMsgWrapInfo.getMult()); // 份数
		tWFContInfo.setBak2(mMsgWrapInfo.getCardNo());
		tWFContInfo.setBak3(mMsgSimPolicyInfo.getCInValiDate());// 保单终止日期
		 tWFContInfo.setBak4(mMsgSimPolicyInfo.getBankAccNo());
		// tWFContInfo.setBankAccNo(mMsgSimPolicyInfo.getBankCode());
		return tWFContInfo;
	}

	private LICardActiveInfoListSchema getwrapInfo() {

		tLICardActiveInfo = new LICardActiveInfoListSchema();
		tLICardActiveInfo.setSequenceNo("1");
		// }
		tLICardActiveInfo.setBatchNo(BittchNo);
		tLICardActiveInfo.setCardNo(mMsgWrapInfo.getCardNo());
		tLICardActiveInfo.setCardType(mMsgWrapInfo.getWrapCode());
		tLICardActiveInfo.setCValidate(mMsgSimPolicyInfo
				.getCValiDate());
		tLICardActiveInfo.setActiveDate(mMsgWrapInfo.getActiveDate());
		tLICardActiveInfo.setDealFlag("00");// 置为激活导入未处理状态
		// 被保人信息
		if ("0".equals(mMsgSimInsuInfo.getToAppntRela())) {
			// 若为本人投保，则存入投保人信息
			tLICardActiveInfo.setIdNo(mMsgSimAppntInfo.getIdNo());
			tLICardActiveInfo.setName(mMsgSimAppntInfo.getName());
			if (("01").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfo.setIdType("0");// 身份证
				String idno = mMsgSimAppntInfo.getIdNo();
				if (idno.length() == 18) {
					tLICardActiveInfo.setBirthday(idno.substring(6,
							10)
							+ "-"
							+ idno.substring(10, 12)
							+ "-"
							+ idno.substring(12, 14));

					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tLICardActiveInfo.setSex("1");// 1代表女
					} else {
						tLICardActiveInfo.setSex("0");
					}
				} else {
					String sSex = idno.substring(14);
					tLICardActiveInfo.setBirthday("19"
							+ idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tLICardActiveInfo.setSex("1");// 1代表女
					} else {
						tLICardActiveInfo.setSex("0");
					}
				}
			} else if (("02").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfo.setIdType("2");// 军官证
				tLICardActiveInfo.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfo.setBirthday(mMsgSimAppntInfo
						.getBirthday());

			} else if (("03").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfo.setIdType("1");// 护照
				tLICardActiveInfo.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfo.setBirthday(mMsgSimAppntInfo
						.getBirthday());

			} else if (("04").equals(mMsgSimAppntInfo.getIDType())
					|| ("05").equals(mMsgSimAppntInfo.getIDType())
					|| ("99").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfo.setIdType("4");// 回乡证/台胞证
				tLICardActiveInfo.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfo.setBirthday(mMsgSimAppntInfo
						.getBirthday());

			}

			else if (("06").equals(mMsgSimAppntInfo.getIDType())
					|| ("07").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfo.setIdType("3");// 警官，士兵
				tLICardActiveInfo.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfo.setBirthday(mMsgSimAppntInfo
						.getBirthday());
			}
		} 
		tLICardActiveInfo.setOccupationType(mMsgSimInsuInfo
				.getOccupationType());
		tLICardActiveInfo.setOccupationCode(mMsgSimInsuInfo
				.getOccupationCode());
		tLICardActiveInfo.setPostalAddress(mMsgSimInsuInfo
				.getPostalAddress());
		tLICardActiveInfo.setZipCode(mMsgSimInsuInfo.getZipCode());
		tLICardActiveInfo.setPhone(mMsgSimInsuInfo.getPhone());
		tLICardActiveInfo.setMobile(mMsgSimInsuInfo.getMobile());
		tLICardActiveInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
		tLICardActiveInfo.setCompanyPhone(mMsgSimInsuInfo
				.getCompayPhone());
		tLICardActiveInfo.setEMail(mMsgSimInsuInfo.getEmail());
		tLICardActiveInfo.setState("00");
		tLICardActiveInfo.setOperator(mMsgHead.getSendOperator());
		tLICardActiveInfo.setMakeDate(PubFun.getCurrentDate());
		tLICardActiveInfo.setMakeTime(PubFun.getCurrentTime());
		tLICardActiveInfo.setModifyDate(PubFun.getCurrentDate());
		tLICardActiveInfo.setModifyTime(PubFun.getCurrentTime());
		tLICardActiveInfo.setPrem(calPrem);
		tLICardActiveInfo.setAmnt(calAmnt);
		tLICardActiveInfo.setMult(mMsgWrapInfo.getMult());
		tLICardActiveInfo.setCopys(mMsgWrapInfo.getCopys());
		tLICardActiveInfo.setRemark(mMsgWrapInfo.getReMark());
		tLICardActiveInfo.setCardStatus("01");
		tLICardActiveInfo.setInsuYear(InsuYear);
		tLICardActiveInfo.setInsuYearFlag("D");
		tLICardActiveInfo.setInActiveDate(mMsgSimPolicyInfo
				.getCInValiDate());// 保单终止日期
		return tLICardActiveInfo;
	}

	private LZCardSchema getLZCardInfo() {
		String tSQL = "select lzc.* "
				+ " from lzcardnumber lzcn "
				+ " inner join lzcard lzc on lzcn.cardtype = lzc.subcode and lzc.startno = lzcn.cardserno and lzc.endno = lzcn.cardserno "
				+ " where 1 = 1 and lzcn.cardno = '" + mMsgWrapInfo.getCardNo()
				+ "' ";
		LZCardSet tLZCardInfos = new LZCardDB().executeQuery(tSQL);
		if (tLZCardInfos == null || tLZCardInfos.size() == 0) {
			buildError("batchdealCertInfo", "单证信息未找到。");
			return null;
		} else if (tLZCardInfos.size() != 1) {
			buildError("batchdealCertInfo", "单证信息出现多条。");
			return null;
		}
		LZCardSchema tLZCardInfo = tLZCardInfos.get(1);
		String state = tLZCardInfo.getState();
		if (!state.equals("2") && !state.equals("13")) {
			tLZCardInfo.setState("14");
		}
		tLZCardInfo.setActiveFlag("1");
		return tLZCardInfo;
	}

	private LCUnionPayDetailSchema getLCUnionPayDetailSchema(){
		String tSql = " select * from LCUnionPayDetail "
					+ " where batchno = '"
					+ mMsgHead.getBatchNo()
					+ "' ";
		LCUnionPayDetailSet tLCUnionPayDetailSet = new LCUnionPayDetailDB().executeQuery(tSql);
		if (tLCUnionPayDetailSet == null || tLCUnionPayDetailSet.size() == 0) {
			buildError("batchdealCertInfo", "银联信息未找到。");
			return null;
		} else if (tLCUnionPayDetailSet.size() != 1) {
			buildError("batchdealCertInfo", "银联信息出现多条。");
			return null;
		}
		LCUnionPayDetailSchema tLCUnionPayDetailSchema = tLCUnionPayDetailSet.get(1);
		String state = tLCUnionPayDetailSchema.getState();
		tLCUnionPayDetailSchema.setResult("00");
		if(("00").equals(state)){
			tLCUnionPayDetailSchema.setState("01");
		}
		tLCUnionPayDetailSchema.setCardNo(mMsgWrapInfo.getCardNo());
		return tLCUnionPayDetailSchema;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		// TODO Auto-generated method stub
		return false;
	}
}
