package com.sinosoft.cardbusiness;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LCUnionPayDetailDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCUnionPayDetailSchema;
import com.sinosoft.lis.vschema.LCUnionPayDetailSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class HandleTxt {

	private MMap mMap = new MMap();
	private String txtName=new String();

    // 获取一个文件夹下的所有文件 要求：后缀名为txt (可自己修改)     
	private List getFileList(File file) {  
        List result = new ArrayList();  
        if (!file.isDirectory()) {  
            result.add(file.getAbsolutePath());  
        } else {  
            // 内部匿名类，用来过滤文件类型  
            File[] directoryList = file.listFiles(new FileFilter() {  
                public boolean accept(File file) {  
                    if (file.isFile() && file.getName().indexOf("txt") > -1) {  
                        return true;  
                    } else {  
                        return false;  
                    }  
                }  
            });  
            for (int i = 0; i < directoryList.length; i++) {  
                result.add(directoryList[i].getAbsolutePath());  
            }  
        }  
        return result;  
    }  
	
    //根据文件的路径获取文件名称
    private String getTxtName(String interFilePath){
    	if ((interFilePath != null) && (interFilePath.length() > 0)) {  
            int a = interFilePath.lastIndexOf('.');  
            int b = interFilePath.lastIndexOf('\\');
            if ((a > -1) && (a < (interFilePath.length()))) {  
            	txtName =interFilePath.substring((b+1), a);  
            }  
       }  
	System.out.println("文件名:"+txtName);
	return txtName;		
    }
    
    //读取TXT内容，并且根据（,号）读取
	private boolean readTextDate(String interFilePath) throws java.io.FileNotFoundException,
	java.io.IOException {
		ExeSQL tExeSql = new ExeSQL();
		List tResult = new ArrayList();
		LCUnionPayDetailSet tLCUnionPayDetailSet=new LCUnionPayDetailSet();
		FileReader fr = new FileReader(interFilePath);
		txtName=getTxtName(interFilePath);
		String tStrSql="select 1 from LCUnionPayDetail where Fileno='"+txtName+"' ";		
		String tStrResult = tExeSql.getOneValue(tStrSql);
		if ("1".equals(tStrResult)) {
			String tStrErr = "文件【"+txtName+"】，以处理。";
			System.out.println(tStrErr);
			return false;
		}		
		BufferedReader br = new BufferedReader(fr);
		String Line = br.readLine();
		int intFileNum = 0;
		while (Line != null) {
			tResult.add(Line);
			copyToTxt(Line);
			intFileNum = intFileNum + 1;
			if (intFileNum <= 5000){
				tLCUnionPayDetailSet=new LCUnionPayDetailSet();
				for (int i = 0; i < intFileNum; i++) {
					String[] SingleData = ((String) tResult.get(i)).split("\\,");
					LCUnionPayDetailSchema tLCUnionPayDetailSchema=new LCUnionPayDetailSchema();
					tLCUnionPayDetailSchema.setFileNo(txtName);
					tLCUnionPayDetailSchema.setBatchNo(SingleData[0]);
					tLCUnionPayDetailSchema.setContType(SingleData[1]);
					tLCUnionPayDetailSchema.setAppntName(SingleData[2]);
					tLCUnionPayDetailSchema.setAppntIDType(SingleData[3]);
					tLCUnionPayDetailSchema.setAppntIDNo(SingleData[4]);
					tLCUnionPayDetailSchema.setAppntBirthday(SingleData[5]);
					tLCUnionPayDetailSchema.setAppntSex(SingleData[6]);
					tLCUnionPayDetailSchema.setAppntBankAccNo(SingleData[7]);
					tLCUnionPayDetailSchema.setAppntPhone(SingleData[8]);
					tLCUnionPayDetailSchema.setRelationToAppnt(SingleData[9]);
					tLCUnionPayDetailSchema.setInsuredName(SingleData[10]);
					tLCUnionPayDetailSchema.setInsuredIDType(SingleData[11]);
					tLCUnionPayDetailSchema.setInsuredIDNo(SingleData[12]);
					tLCUnionPayDetailSchema.setInsuredBirthday(SingleData[13]);
					tLCUnionPayDetailSchema.setInsuredSex(SingleData[14]);
					tLCUnionPayDetailSchema.setCardLevel(SingleData[15]);
					tLCUnionPayDetailSchema.setAmnt(SingleData[16]);
					tLCUnionPayDetailSchema.setCValiDate(SingleData[17]);
					tLCUnionPayDetailSchema.setCInValiDate(SingleData[18]);
					tLCUnionPayDetailSchema.setWrapCode(SingleData[19]);
					tLCUnionPayDetailSchema.setCertifyCode(SingleData[20]);
					tLCUnionPayDetailSchema.setState("00");
					tLCUnionPayDetailSchema.setMakeDate(PubFun.getCurrentDate());    
					tLCUnionPayDetailSchema.setMakeTime(PubFun.getCurrentTime());    
					tLCUnionPayDetailSchema.setModifyDate(PubFun.getCurrentDate());  
					tLCUnionPayDetailSchema.setModifyTime(PubFun.getCurrentTime());  
					tLCUnionPayDetailSet.add(tLCUnionPayDetailSchema);
				}
				mMap = new MMap();
				mMap.put(tLCUnionPayDetailSet, "INSERT");
				if (!SubmitMap()){
					return false;
				}
				intFileNum = 0;
				tResult = new ArrayList();
			}							
			Line = br.readLine();
		}
			br.close();
			fr.close();
			return true;
		
	}	
	
	/*数据处理*/
	private boolean SubmitMap() {
		PubSubmit tPubSubmit = new PubSubmit();
		VData mResult = new VData();
		mResult.add(mMap);
		if (!tPubSubmit.submitData(mResult, "")) {
			System.out.println("提交数据失败！");
			return false;
		}
		return true;
	}

	private void copyToTxt(String line){
		String filename="E:\\Txt\\txt\\";
		File newFile = new File(filename+txtName+".txt");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(newFile,true)); 
			bw.write(line+"\r\n");
			bw.close();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			newFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
		
	//数据检测
	private boolean checkInfo() {
		String tSQL = "select * from LCUnionPayDetail where state='00' and result is null ";
		LCUnionPayDetailDB tLCUnionPayDetailDB = new LCUnionPayDetailDB();
		LCUnionPayDetailSet tLCUnionPayDetailSet = tLCUnionPayDetailDB.executeQuery(tSQL);
		if(tLCUnionPayDetailSet.size()==0){
			System.out.println("没有要处理的批次！");
			return false;
		}
		for(int i=1;i<=tLCUnionPayDetailSet.size();i++){
			LCUnionPayDetailSchema tLCUnionPayDetailSchema=new LCUnionPayDetailSchema();
			tLCUnionPayDetailSchema = tLCUnionPayDetailSet.get(i);
			if(tLCUnionPayDetailSchema.getContType() == null || "".equals(tLCUnionPayDetailSchema.getContType())
					 || tLCUnionPayDetailSchema.getAppntName() == null || "".equals(tLCUnionPayDetailSchema.getAppntName())
					 || tLCUnionPayDetailSchema.getAppntIDType() == null || "".equals(tLCUnionPayDetailSchema.getAppntIDType())
					 || tLCUnionPayDetailSchema.getAppntIDNo() == null || "".equals(tLCUnionPayDetailSchema.getAppntIDNo())
					 || tLCUnionPayDetailSchema.getAppntBankAccNo() == null || "".equals(tLCUnionPayDetailSchema.getAppntBankAccNo())
					 || tLCUnionPayDetailSchema.getAppntPhone() == null || "".equals(tLCUnionPayDetailSchema.getAppntPhone())
					 || tLCUnionPayDetailSchema.getCardLevel() == null || "".equals(tLCUnionPayDetailSchema.getCardLevel())
					 || tLCUnionPayDetailSchema.getAmnt() == null || "".equals(tLCUnionPayDetailSchema.getAmnt())
					 || tLCUnionPayDetailSchema.getCValiDate() == null || "".equals(tLCUnionPayDetailSchema.getCValiDate())
					 || tLCUnionPayDetailSchema.getCInValiDate() == null || "".equals(tLCUnionPayDetailSchema.getCInValiDate())
					 || tLCUnionPayDetailSchema.getCertifyCode() == null || "".equals(tLCUnionPayDetailSchema.getCertifyCode())
					 || tLCUnionPayDetailSchema.getWrapCode() == null || "".equals(tLCUnionPayDetailSchema.getWrapCode())
					){
							String tErrInfo = "请检验文件中是否有非空项未录";
							tLCUnionPayDetailSchema.setResult("01");
							tLCUnionPayDetailSchema.setState("11");
							tLCUnionPayDetailSchema.setFalseReason(tErrInfo);
							continue;
				
					}
			//校验投保类型是否为1（持卡人投保）
			if(!("1").equals(tLCUnionPayDetailSchema.getContType())){
				String tErrInfo = "投保类型不是持卡人投保！";
				tLCUnionPayDetailSchema.setResult("01");
				tLCUnionPayDetailSchema.setState("11");
				tLCUnionPayDetailSchema.setFalseReason(tErrInfo);
				continue;
			}
			// 证件类型为身份证则只校验身份证号
			if (("01").equals(tLCUnionPayDetailSchema.getAppntIDType())) {
				if ((tLCUnionPayDetailSchema.getAppntIDNo().length() != 18 && tLCUnionPayDetailSchema.getAppntIDNo().length() != 15)) {
					
					String tErrInfo=("投保人身份证证件长度不正确");
					tLCUnionPayDetailSchema.setResult("01");
					tLCUnionPayDetailSchema.setState("11");
					tLCUnionPayDetailSchema.setFalseReason(tErrInfo);
					continue;
				}
				// 否则连同性别和生日一同校验
			} else if (("02").equals(tLCUnionPayDetailSchema.getAppntIDType()) || ("03").equals(tLCUnionPayDetailSchema.getAppntIDType())
					|| ("04").equals(tLCUnionPayDetailSchema.getAppntIDType()) || ("05").equals(tLCUnionPayDetailSchema.getAppntIDType())
					|| ("06").equals(tLCUnionPayDetailSchema.getAppntIDType()) || ("07").equals(tLCUnionPayDetailSchema.getAppntIDType())
					|| ("99").equals(tLCUnionPayDetailSchema.getAppntIDType())) {
				if (tLCUnionPayDetailSchema.getAppntBirthday() == null || "".equals(tLCUnionPayDetailSchema.getAppntBirthday()) 
						|| tLCUnionPayDetailSchema.getAppntIDNo() == null || "".equals(tLCUnionPayDetailSchema.getAppntIDNo()) 
						|| tLCUnionPayDetailSchema.getAppntSex() == null || "".equals(tLCUnionPayDetailSchema.getAppntSex())) {
				
					String tErrInfo=("投保人非身份证件信息时中有非空项未录");
					tLCUnionPayDetailSchema.setResult("01");
					tLCUnionPayDetailSchema.setState("11");
					tLCUnionPayDetailSchema.setFalseReason(tErrInfo);
					continue;				
					}
			} else {
				String tErrInfo=("证件类型有误");
				tLCUnionPayDetailSchema.setResult("01");
				tLCUnionPayDetailSchema.setState("11");
				tLCUnionPayDetailSchema.setFalseReason(tErrInfo);
				continue;			
			}
			ExeSQL tExeSql = new ExeSQL();
			String sql = "select batchno from wfappntlist where  batchno='"+tLCUnionPayDetailSchema.getBatchNo()+"'";
			SSRS SSRS = tExeSql.execSQL(sql);
			if(SSRS.MaxRow!=0){
				String tErrInfo="批次号重复请检查";
				tLCUnionPayDetailSchema.setResult("01");
				tLCUnionPayDetailSchema.setState("11");
				tLCUnionPayDetailSchema.setFalseReason(tErrInfo);
				continue;
			}
		}
		
		mMap = new MMap();
		mMap.put(tLCUnionPayDetailSet, "UPDATE");
		if (!SubmitMap()){
			return false;
		}
		return true;
	}

	//返回TXT文件
	private void writeToTxt(){
		ExeSQL tExeSQL = new ExeSQL();
		String filename="E:\\Txt\\return\\";
		String tSQL=" select * from LCUnionPayDetail where makedate=current date ";
		LCUnionPayDetailDB tLCUnionPayDetailDB = new LCUnionPayDetailDB();
		LCUnionPayDetailSet tLCUnionPayDetailSet = tLCUnionPayDetailDB.executeQuery(tSQL);
		if(tLCUnionPayDetailSet == null){
			System.out.println("没有需要处理的报送银联信息！");
			return;
		}
		String tsql=" select distinct fileno from lcunionpaydetail where makedate=current date ";
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tsql);
        if (tSSRS.getMaxRow() > 0)
        {
            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
            	String tfileno=tSSRS.GetText(i, 1);
        		File file = new File(filename+tfileno+".txt");
            	String tSql=" select * from LCUnionPayDetail where fileno='"
            			   + tfileno
            			   +"' and makedate=current date ";
        		tLCUnionPayDetailDB = new LCUnionPayDetailDB();
        		tLCUnionPayDetailSet = tLCUnionPayDetailDB.executeQuery(tSql);
        		FileWriter fw = null;  
        		try {
        			fw = new FileWriter(file, true);
        			for(int j = 1; j<=tLCUnionPayDetailSet.size(); j++){
        				LCUnionPayDetailSchema tLCUnionPayDetailSchema=new LCUnionPayDetailSchema();
        				tLCUnionPayDetailSchema = tLCUnionPayDetailSet.get(j);
        				String tbatchno=tLCUnionPayDetailSchema.getBatchNo();
        				String tcardno=tLCUnionPayDetailSchema.getCardNo();
        				String tcardlevel=tLCUnionPayDetailSchema.getCardLevel();
        				String tamnt=tLCUnionPayDetailSchema.getAmnt();
        				String tcvalidate=tLCUnionPayDetailSchema.getCValiDate();
        				String tcinvalidate=tLCUnionPayDetailSchema.getCValiDate();
        				String tresult=tLCUnionPayDetailSchema.getResult();
        				String tfalsecode=tLCUnionPayDetailSchema.getFalseCode();
        				
        				if(tbatchno==null||"".equals(tbatchno)){
        					tbatchno="";
        				}
        				if(tcardno==null||"".equals(tcardno)){
        					tcardno="";
        				}
        				if(tcardlevel==null||"".equals(tcardlevel)){
        					tcardlevel="";
        				}
        				if(tamnt==null||"".equals(tamnt)){
        					tamnt="";
        				}
        				if(tcvalidate==null||"".equals(tcvalidate)){
        					tcvalidate="";
        				}
        				if(tcinvalidate==null||"".equals(tcinvalidate)){
        					tcinvalidate="";
        				}
        				if(tresult==null||"".equals(tresult)){
        					tresult="";
        				}
        				if(tfalsecode==null||"".equals(tfalsecode)){
        					tfalsecode="";
        				}
        				fw.append(tbatchno+",").append(tcardno+",")
        					.append(tcardlevel+",").append(tamnt+",")
        					.append(tcvalidate+",").append(tcinvalidate+",")
        					.append(tresult+",").append(tfalsecode);				
        				if(j != tLCUnionPayDetailSet.size()){
        					fw.write("\r\n");
        				}
        				fw.flush();
        			}
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
        		try {
        			fw.close();
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
            }
            
        }		
	}
	
	
	
	private void deal() {
		BatchDealCardInfo tBatchDealCardInfo=new BatchDealCardInfo();
		File fl=new File("E:\\Txt");
		List list=getFileList(fl);
		String a = null;
		for(int i = 0; i < list.size(); i++)  
	        {  
	            a=list.get(i).toString();  
	            System.out.println(list.get(i).toString());  
	            try {
	    			readTextDate(a);
	    		} catch (FileNotFoundException e) {
	    			e.printStackTrace();
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		}

	        } 
		checkInfo();
		String tSQL = "select * from LCUnionPayDetail where state='00' and result is null ";
		LCUnionPayDetailDB tLCUnionPayDetailDB = new LCUnionPayDetailDB();
		LCUnionPayDetailSet tLCUnionPayDetailSet = tLCUnionPayDetailDB.executeQuery(tSQL);
		if(tLCUnionPayDetailSet.size()==0){
			System.out.println("没有要处理的批次！");
		}
		for(int i=1;i<=tLCUnionPayDetailSet.size();i++){
			LCUnionPayDetailSchema tLCUnionPayDetailSchema=new LCUnionPayDetailSchema();
			tLCUnionPayDetailSchema = tLCUnionPayDetailSet.get(i);
			tBatchDealCardInfo.deal(tLCUnionPayDetailSchema);
		}
		writeToTxt();
	}
	public static void main(String args[]) {
		HandleTxt ht = new HandleTxt();
		ht.deal();
	}
}
