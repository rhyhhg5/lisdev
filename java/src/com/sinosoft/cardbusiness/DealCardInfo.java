package com.sinosoft.cardbusiness;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.SimAppntInfo;
import com.cbsws.obj.SimInsuInfo;
import com.cbsws.obj.SimPolicyInfo;
import com.cbsws.obj.WrapInfo;
import com.cbsws.xml.ctrl.blogic.simsale.CardNoManager;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.WFAppntListSchema;
import com.sinosoft.lis.schema.WFContListSchema;
import com.sinosoft.lis.schema.WFInsuListSchema;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class DealCardInfo extends ABusLogic {
	/* 公共错误处理类 */
	public CErrors mErrors = new CErrors();
	/* 与报文头对应的对象 */
	private MsgHead mMsgHead = null;
	/* 与报文节点对应的对象 */
	private SimAppntInfo mMsgSimAppntInfo = null;
	private SimPolicyInfo mMsgSimPolicyInfo = null;
	private WrapInfo mMsgWrapInfo = null;
	// private SimInsuInfo[] mMsgSimInsuInfoList = null;
	private SimInsuInfo mMsgSimInsuInfo = null;

	/* 往后传数据的Schema、Set对象 */
	// private WFInsuListSet tWFInsuInfoSet = new WFInsuListSet();
	private WFInsuListSchema tWFInsuInfo = new WFInsuListSchema();
	private WFAppntListSchema tWFAppntInfo = new WFAppntListSchema();
	private WFContListSchema tWFContInfo = new WFContListSchema();
	// private LICardActiveInfoListSet tLICardActiveInfos = new
	// LICardActiveInfoListSet();
	private LICardActiveInfoListSchema tLICardActiveInfo = new LICardActiveInfoListSchema();
	private LZCardSchema tLZCardInfo = new LZCardSchema();
	private CardNoManager mCardNoManager = new CardNoManager();

	private String BittchNo = "";
	private String ContNo = "";
	private String InsuYear = "";
	private String calPrem = "";
	private String calAmnt = "";
	String tsex = "t";
	String bsex = "b";
	/* 投保人 */

	boolean messageFlag = true;

	/* 存储Schema、Set向后台提交数据的Map */
	private MMap mMap = new MMap();
	/* 向后面传输数据的集合 */
	VData tVData = new VData();
	/* 返回结果的集合 */
	VData mResult = new VData();
	/* 发送信息时传递的Map集合 */
	private HashMap mAndeMap = new HashMap();

	protected boolean deal(MsgCollection cMsgInfos) {
		// 解析报文，获得报文数据
		if (!parseDatas(cMsgInfos)) {
			return false;
		}
		BittchNo = mMsgHead.getBatchNo();
		// 获取卡号、密码 WR0204/D65005
		if (!getCardInfo()) {
			return false;
		}
		// 校验数据的合法性
		if (!chkCertInfo()) {
			return false;
		}
		// 处理产品算费要素
		if (!dealWrapParams()) {
			return false;
		}
		// 处理业务数据
		if (!tradeService()) {
			return false;
		}
		// 检验被保人与投保人关系
		if (!ChkRelation()) {
			return false;
		}
		// 提交数据库
		PubSubmit p = new PubSubmit();
		if (!p.submitData(tVData, "")) {
			System.out.println("提交数据失败");
			 errLog("提交数据失败");
			return false;
		}
		// 封装返回报文
		if (!qryTradeResult(mResult)) {
			buildError("qryTradeResult", "封装返回报文失败");
			return false;
		}
		// 发送短信、邮件
		sendSmsAndEmail();
		// 生成电子保单，上传服务器
		ContNo = mMsgWrapInfo.getCardNo();
		System.out.println(ContNo);
		makeUnionPayPDF(ContNo);

		return true;
	}

	private boolean ChkRelation() {
		if (tsex.equals(bsex)) {
			errLog("投保人与被保人关系为配偶时性别不能相同");
			return false;
		}
		return true;
	}

	private boolean getCardInfo() {
		String tWrapCode = mMsgWrapInfo.getWrapCode();
		String tCertifyCode = mMsgWrapInfo.getCertifyCode();
		String tMsgType = mMsgHead.getMsgType();
		String[] info = mCardNoManager.getCardNo(tWrapCode, tCertifyCode,
				tMsgType);
		if (info == null) {
			errLog("获取卡号过程出现异常，稍候请尝试重新交易");
			return false;
		} else {
			if (info[2] != null) {
				errLog(info[2]);
				return false;
			} else {
				mMsgWrapInfo.setCardNo(info[0]);
				mMsgWrapInfo.setPassword(info[1]);
				System.out.println("套餐编码为：" + tWrapCode + "试算过程中取得的卡号为："
						+ mMsgWrapInfo.getCardNo() + "取得的密码为："
						+ mMsgWrapInfo.getPassword());
			}

		}
		return true;
	}

	private boolean dealWrapParams() {

		LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

		String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
				+ " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
				+ " where lmcr.CertifyCode = '"
				+ mMsgWrapInfo.getCertifyCode()
				+ "' "
				+ " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
		tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
		System.out.println(tStrSql);
		if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0) {
			String tStrErr = "单证号[" + mMsgWrapInfo.getCardNo()
					+ "]对应套餐要素信息未找到。";
			buildError("dealWrapParams", tStrErr);
			System.out.println(tStrErr);
			return false;
		}

		tStrSql = null;

		double tSysWrapSumPrem = 0;
		double tSysWrapSumAmnt = 0;

		for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);
			String tCalFactorType = tRiskDutyParam.getCalFactorType();
			String tCalFactor = tRiskDutyParam.getCalFactor();
			String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

			if ("Prem".equals(tCalFactor)) {
				String tTmpCalResult = null;
				if ("2".equals(tCalFactorType)) {
					PubCalculator tCal = new PubCalculator();
					tCal.addBasicFactor("Copys",
							String.valueOf(mMsgWrapInfo.getCopys()));
					tCal.addBasicFactor("Mult",
							String.valueOf(mMsgWrapInfo.getMult()));
					tCal.addBasicFactor("InsuYear", String.valueOf(InsuYear));
					// 计算
					tCal.setCalSql(tRiskDutyParam.getCalSql());
					tTmpCalResult = tCal.calculate();
					if (tTmpCalResult == null || tTmpCalResult.equals("")) {
						String tStrErr = "单证号[" + mMsgWrapInfo.getCardNo()
								+ "]保费计算失败。";
						buildError("dealWrapParams", tStrErr);
						System.out.println(tStrErr);
						return false;
					}
				} else {
					tTmpCalResult = tCalFactorValue;
				}

				BigDecimal b = new BigDecimal(tTmpCalResult);
				calPrem = b.setScale(2, BigDecimal.ROUND_HALF_UP)
						.toPlainString();

				try {
					tSysWrapSumPrem = Arith.add(tSysWrapSumPrem,
							new BigDecimal(tTmpCalResult).doubleValue());
				} catch (Exception e) {
					String tStrErr = "套餐要素中保费出现非数值型字符串。";
					buildError("dealWrapParams", tStrErr);
					return false;
				}
			}

			if ("Amnt".equals(tCalFactor)) {
				String tTmpCalResult = null;
				if ("2".equals(tCalFactorType)) {
					PubCalculator tCal = new PubCalculator();
					tCal.addBasicFactor("Copys",
							String.valueOf(mMsgWrapInfo.getCopys()));
					tCal.addBasicFactor("Mult",
							String.valueOf(mMsgWrapInfo.getMult()));
					tCal.addBasicFactor("InsuYear", String.valueOf(InsuYear));
					// 计算
					tCal.setCalSql(tRiskDutyParam.getCalSql());
					tTmpCalResult = tCal.calculate();
					if (tTmpCalResult == null || tTmpCalResult.equals("")) {
						String tStrErr = "单证号[" + mMsgWrapInfo.getCardNo()
								+ "]保额计算失败。";
						buildError("dealWrapParams", tStrErr);
						System.out.println(tStrErr);
						return false;
					}
				} else {
					tTmpCalResult = tCalFactorValue;
				}

				BigDecimal b = new BigDecimal(tTmpCalResult);
				calAmnt = b.setScale(2, BigDecimal.ROUND_HALF_UP)
						.toPlainString();

				try {
					tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt,
							new BigDecimal(tTmpCalResult).doubleValue());
				} catch (Exception e) {
					String tStrErr = "套餐要素中保额出现非数值型字符串。";
					buildError("dealWrapParams", tStrErr);
					return false;
				}
			}
		}

		return true;
	}

	private void makeUnionPayPDF(String ContNo) {
		String cardno = ContNo;
		UnionPayprintBL ecp = new UnionPayprintBL();
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo(BittchNo);
		tMsgHead.setBranchCode(mMsgHead.getBranchCode());
		tMsgHead.setMsgType(mMsgHead.getMsgType());
		tMsgHead.setSendDate(mMsgHead.getSendDate());
		tMsgHead.setSendTime(mMsgHead.getSendTime());
		tMsgHead.setSendOperator(mMsgHead.getSendOperator());
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(cardno);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);
		ecp.deal(tMsgCollection);
	}

	private void sendSmsAndEmail() {
		if ("".equals(BittchNo) || BittchNo == null) {
			System.out.println("银联获取批次号失败!");
		}
		ExeSQL tExeSql = new ExeSQL();
		String tStrSql2 = "select wfa.name,wfa.mobile ,wfa.email "
				+ "from wfappntlist wfa where wfa.batchno = '" + BittchNo
				+ "' and wfa.cardno = '" + mMsgWrapInfo.getCardNo() + "' ";
		SSRS checkSSRS2 = tExeSql.execSQL(tStrSql2);
		if (checkSSRS2 == null || checkSSRS2.MaxRow == 0) {
			System.out.println("获取发送短信和邮件信息失败!");

		}
		String tcertifyName = checkSSRS2.GetText(1, 1);

		String tmobile = checkSSRS2.GetText(1, 2);

		String tproposerEmail = checkSSRS2.GetText(1, 3);

		String tStrSql = "select distinct lic.cardno,"
				+ " lic.mult,"
				+ "lic.cvalidate,lic.inactivedate "
				+ "from wfcontlist wfc,licardactiveinfolist lic where wfc.batchno = '"
				+ BittchNo + "' " + "and lic.cardno = wfc.cardno "
				+ "and lic.cardno = '" + mMsgWrapInfo.getCardNo() + "' ";
		//select distinct lic.cardno,(select lica.mult from licardactiveinfolist lica    where sequenceno='1' and lica.cardno = lic.cardno FETCH FIRST 1 ROWS ONLY ),lic.cvalidate,lic.inactivedate from wfcontlist wfc,licardactiveinfolist lic where wfc.batchno = 'UP0001UPY2015122516004300004' and lic.cardno = wfc.cardno and lic.cardno = 'YI000000085' ;
		SSRS checkSSRS = tExeSql.execSQL(tStrSql);
		if (checkSSRS.MaxRow == 0) {
			System.out.println("获取发送短信和邮件信息失败!");

		}
		int length = checkSSRS.MaxRow;
		for (int i = 0; i < length; i++) {
			String tcardNo = "";
			String tCValiDate = "";
			String tCInValiDate = "";
			tcardNo = checkSSRS.GetText(i + 1, 1);
			String tmult = checkSSRS.GetText(1, 2);
			tCValiDate = checkSSRS.GetText(i + 1, 3);
			tCInValiDate = checkSSRS.GetText(i + 1, 4);
			System.out.println("tcardNo=" + tcardNo);
			System.out.println("tCValiDate=" + tCValiDate);
			System.out.println("tCInValiDate=" + tCInValiDate);
			mAndeMap.put("cardNo" + i, tcardNo);
			if (i == length - 1) {
				mAndeMap.put("endindex", "" + i);
			}
			String tWrapName = "";
			if("1".equals(tmult)){
				tWrapName = "银标钻石信用卡航空意外险";
			}else if("2".equals(tmult)){
				tWrapName = "银标白金信用卡航空意外险";
			}else if("3".equals(tmult)){
				tWrapName = "公务卡、商务卡、旅游卡航空意外险";
			}else if("4".equals(tmult)){
				tWrapName = "银标信用卡金卡、普卡航空意外险";
			}else if("5".equals(tmult)){
				tWrapName = "配偶航空意外险";
			}else if("6".equals(tmult)){
				tWrapName = "子女航空意外险";
			}
			mAndeMap.put("WrapName" + i, tWrapName);
			mAndeMap.put("CValiDate" + i, tCValiDate);
			mAndeMap.put("CInValiDate" + i, tCInValiDate);
		}
		mAndeMap.put("certifyName", tcertifyName);
		mAndeMap.put("mobile", tmobile);
		mAndeMap.put("orderID", BittchNo);
		mAndeMap.put("proposerEmail", tproposerEmail);
		mAndeMap.put("ProposerNeedSMS", "T");
		mAndeMap.put("ProposerName", tcertifyName);

		/*
		 * boolean emailFlag = true; String errinfo = "";
		 */
		if (mAndeMap.get("ProposerNeedSMS").equals("T")) {
			System.out.println(((String) mAndeMap.get("mobile")).length()
					+ "----------mobile");
			// 短息通知
			if (mAndeMap.get("mobile") != null
					&& !("").equals(mAndeMap.get("mobile"))) {
				if (new UnionPayEmailAndMessageBL().sendMsg(mAndeMap)) {
					System.out.println("手机号为" + mAndeMap.get("mobile")
							+ "的短息发送成功");

				} else {
					messageFlag = false;
					System.out.println("手机号为" + mAndeMap.get("mobile")
							+ "的短息发送失败");

				}

			}
			/*
			 * //邮件通知 if(mAndeMap.get("proposerEmail")!=null &&
			 * !("").equals(mAndeMap.get("proposerEmail"))){
			 * if(UnionPayEmailAndMessageBL.sendEmail(mAndeMap)){ }else{
			 * emailFlag = false; } if(messageFlag&emailFlag){
			 * System.out.println
			 * ("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送成功"); return true;
			 * }else{
			 * System.out.println("邮箱号为"+mAndeMap.get("proposerEmail")+"的邮件发送失败"
			 * ); return false; }
			 * 
			 * }
			 */
		}

	}

	private boolean chkCertInfo() {
	/*	StringBuffer tErrInfos = new StringBuffer();*/
		// 文件头校验
		String tBatchNo = mMsgHead.getBatchNo();
		String tSendDate = mMsgHead.getSendDate();
		String tSendTime = mMsgHead.getSendTime();
		String tBranchCode = mMsgHead.getBranchCode();
		String tSendOperator = mMsgHead.getSendOperator();
		String tMsgType = mMsgHead.getMsgType();
		/*
		 * if (!CertInfoCheck.verifyMsgHead(tErrInfos, tBatchNo, tSendDate,
		 * tSendTime, tBranchCode, tSendOperator, tMsgType)) { return false; }
		 */
		ExeSQL tExeSql = new ExeSQL();
		String sql = "select batchno from wfappntlist where  batchno='"+tBatchNo+"'";
		SSRS SSRS = tExeSql.execSQL(sql);
		if(SSRS.MaxRow!=0){
			errLog("批次号重复请检查");
			return false;
		}
		if (tBatchNo == null || "".equals(tBatchNo) || tSendDate == null
				|| "".equals(tSendDate) || tSendTime == null
				|| "".equals(tSendTime)) {
			errLog("请检验文件头中是否有非空项未录");
			
			return false;
		}
		if (!("UnionPay".equals(tBranchCode))
				&& !("UnionPay".equals(tSendOperator))) {
			
			errLog( "报送类型BranchCode和报送机构SendOperator为平台系统标识 固定不变 UnionPay");
			return false;
		}
		if (!("UP0001".equals(tMsgType))) {
			errLog( "MsgType平台系统标识 固定不变 UP0001");
			
			return false;
		}

		// SimPolicyInfo
		String PolApplyDate = mMsgSimPolicyInfo.getPolApplyDate();
		String CValiDate = mMsgSimPolicyInfo.getCValiDate();
		String CInValiDate = mMsgSimPolicyInfo.getCValiDate();
		String PremScope = mMsgSimPolicyInfo.getPremScope();
		String PayIntv = mMsgSimPolicyInfo.getPayIntv();
		/*
		 * if (!CertInfoCheck.verifySimPolicyInfo(tErrInfos, PolApplyDate,
		 * CValiDate, CInValiDate, PremScope, PayIntv)) { return false; }
		 */
		if (PolApplyDate == null || "".equals(PolApplyDate)
				|| CValiDate == null || "".equals(CValiDate)
				|| CInValiDate == null || "".equals(CInValiDate)
				|| PremScope == null || "".equals(PremScope) || PayIntv == null
				|| "".equals(PayIntv)) {
		
			errLog("请检验保单信息节点中是否有非空项未录");
			return false;
		}
		// WrapInfo
		String WrapCode = mMsgWrapInfo.getWrapCode();
		String CertifyCode = mMsgWrapInfo.getCertifyCode();
		String ActiveDate = mMsgWrapInfo.getActiveDate();
		String Prem = mMsgWrapInfo.getPrem();
		String Mult = mMsgWrapInfo.getMult();
		String Copys = mMsgWrapInfo.getCopys();
		/*
		 * if (!CertInfoCheck.verifyWrapInfo(tErrInfos, WrapCode, CertifyCode,
		 * ActiveDate, Prem, Mult, Copys)) { return false; }
		 */
		if (WrapCode == null || "".equals(WrapCode) || CertifyCode == null
				|| "".equals(CertifyCode) || ActiveDate == null
				|| "".equals(ActiveDate) || Prem == null || "".equals(Prem)
				|| Mult == null || "".equals(Mult) || Copys == null
				|| "".equals(Copys)) {
			
			errLog("请检验产品信息节点中是否有非空项未录");
			return false;
		}
		// SimAppntInfo
		String tName = mMsgSimAppntInfo.getName();
		String tSex = mMsgSimAppntInfo.getSex();
		String tBirthday = mMsgSimAppntInfo.getBirthday();
		String tIDType = mMsgSimAppntInfo.getIDType();
		String tIdNo = mMsgSimAppntInfo.getIdNo();
		
		//增加投保人身份证的校验
		if(tIdNo!=null && !tIdNo.equals("")){
			String error = PubFun.CheckIDNo(tIDType, tIdNo, tBirthday, tSex);
			if(!"".equals(error)){
				errLog("投保人"+error+"--->DealCardInfo.java");
				return false;
			}
		}
		/*
		 * String tPhone = mMsgSimAppntInfo.getPhone(); String tMobile =
		 * mMsgSimAppntInfo.getMobile();
		 */
		/*
		 * if (!CertInfoCheck.verifySimAppntInfo(tErrInfos, tName, tSex,
		 * tBirthday, tIDType, tIdNo, tPhone, tMobile)) { return false; }
		 */
		// 证件类型为身份证则只校验姓名和身份证号
		if (("01").equals(tIDType)) {
			if (tName == null || "".equals(tName) || tIdNo == null
					|| "".equals(tIdNo)
					|| (tIdNo.length() != 18 && tIdNo.length() != 15)) {
				
				errLog("请检验投保人身份证信息中是否有非空项未录或证件长度是否正确");
				return false;
			}
			// 否则连同性别和生日一同校验
		} else if (("02").equals(tIDType) || ("03").equals(tIDType)
				|| ("04").equals(tIDType) || ("05").equals(tIDType)
				|| ("06").equals(tIDType) || ("07").equals(tIDType)
				|| ("99").equals(tIDType)) {
			if (tName == null || "".equals(tName) || tIDType == null
					|| "".equals(tIDType) || tIdNo == null || "".equals(tIdNo)
					/* || tPhone == null || "".equals(tPhone) */|| tSex == null
					|| "".equals(tSex) /*
										 * || tMobile == null ||
										 * "".equals(tMobile)
										 */
					|| tBirthday == null || "".equals(tBirthday)) {
			
				errLog("请检验投保人非身份证件信息中是否有非空项未录");
				return false;
			}
		} else {
			
			errLog("证件类型有误");
			return false;
		}
		// SimInsuInfo
		// for (int i = 0; i < mMsgSimInsuInfoList.length; i++) {
		// SimInsuInfo mMsgSimInsuInfo = mMsgSimInsuInfo;

		String bInsuNo = mMsgSimInsuInfo.getInsuNo();
		String bToMainInsuRela = mMsgSimInsuInfo.getToMainInsuRela();
		String bToAppntRela = mMsgSimInsuInfo.getToAppntRela();
		String bName = mMsgSimInsuInfo.getName();
		String bSex = mMsgSimInsuInfo.getSex();
		String bBirthday = mMsgSimInsuInfo.getBirthday();
		String bIDType = mMsgSimInsuInfo.getIDType();
		String bIdNo = mMsgSimInsuInfo.getIdNo();
		// String bOccupationType = mMsgSimInsuInfo.getOccupationType();
		// String bOccupationCode = mMsgSimInsuInfo.getOccupationCode();
		/*
		 * String bPhone = mMsgSimInsuInfo.getPhone(); String bMobile =
		 * mMsgSimInsuInfo.getMobile();
		 */
		
		//增加被保人身份证号信息校验
		if(bIdNo!=null && !bIdNo.equals("")){
			String error = PubFun.CheckIDNo(bIDType, bIdNo, bBirthday, bSex);
			if(!"".equals(error)){
				errLog("被保人"+error+"--->DealCardInfo.java");
				return false;
			}
		}
		// 无论是否本人投保，先验证被保人客户号和与主被保人关系
		if (/*
			 * bPhone == null || "".equals(bPhone) || bMobile == null ||
			 * "".equals(bMobile) ||
			 */bInsuNo == null || "".equals(bInsuNo) || bToMainInsuRela == null
				|| "".equals(bToMainInsuRela)
		/*
		 * || bOccupationType == null || "".equals(bOccupationType) ||
		 * bOccupationCode == null || "".equals(bOccupationCode)
		 */) {
		
			errLog("请检验被保人信息中被保人客户号或与主被保人关系是否为空");
			return false;
		}
		// 若本人投保
		if ("0".equals(bToAppntRela)) {
			return true;
		} // 若非本人投保
		else if ("1".equals(bToAppntRela)) {
			if (("01").equals(bIDType)) {
				if (bName == null || "".equals(bName) || bIdNo == null
						|| "".equals(bIdNo)
						|| (bIdNo.length() != 18 && bIdNo.length() != 15)) {
					
					errLog("请检验被保人身份证信息是否有非空项未录或长度是否正确");
					return false;
				}
				return true;
			}

			else if (("02").equals(bIDType) || ("03").equals(bIDType)
					|| ("04").equals(bIDType) || ("05").equals(bIDType)
					|| ("06").equals(bIDType) || ("07").equals(bIDType)
					|| ("99").equals(bIDType)) {
				if (bName == null || "".equals(bName) || bIdNo == null
						|| "".equals(bIdNo) || bSex == null || "".equals(bSex)
						|| bBirthday == null || "".equals(bBirthday)) {
					
					errLog("请检验被保人非身份证件信息中是否有非空项未录");
					return false;
				}
			} else {
			
				errLog("被保人证件类型有误");
				return false;
			}
			if ((!"".equals(mMsgSimAppntInfo.getSex()) && mMsgSimAppntInfo
					.getSex() != null)
					&& (!"".equals(mMsgSimInsuInfo.getSex()))
					|| mMsgSimInsuInfo.getSex() != null) {
				if (mMsgSimAppntInfo.getSex().equals(mMsgSimInsuInfo.getSex())) {
					
					errLog("投保人与被保人关系为配偶时性别不能相同");
					return false;

				}
			}

			return true;
		} else if ("2".equals(bToAppntRela)) {
			if (("01").equals(bIDType)) {
				if (bName == null || "".equals(bName) || bIdNo == null
						|| "".equals(bIdNo)
						|| (bIdNo.length() != 18 && bIdNo.length() != 15)) {
				
					errLog("请检验被保人身份证信息是否有非空项未录或长度是否正确");
					return false;
				}
				return true;
			}

			else if (("02").equals(bIDType) || ("03").equals(bIDType)
					|| ("04").equals(bIDType) || ("05").equals(bIDType)
					|| ("06").equals(bIDType) || ("07").equals(bIDType)
					|| ("99").equals(bIDType)) {
				if (bName == null || "".equals(bName) || bIdNo == null
						|| "".equals(bIdNo) || bSex == null || "".equals(bSex)
						|| bBirthday == null || "".equals(bBirthday)) {
					
					errLog("请检验被保人非身份证件信息中是否有非空项未录");
					return false;
				}
			} else {
			
				errLog("被保人证件类型有误");
				return false;
			}

			return true;
		}

		else {
			
			errLog("与投保人关系录入有误，请核实");
			return false;

		}// return true;
	}

	private boolean parseDatas(MsgCollection cMsgInfos) {
		// 报文头
		mMsgHead = cMsgInfos.getMsgHead();
		if (mMsgHead == null) {
			errLog("报文头信息缺失。");
			return false;
		}
		// 保单信息
		try {
			List tSimPolicyInfoList = cMsgInfos.getBodyByFlag("SimPolicyInfo");
			if (tSimPolicyInfoList == null || tSimPolicyInfoList.size() == 0) {
				errLog("保单信息缺失。");
				return false;
			} else if (tSimPolicyInfoList.size() != 1) {
				errLog("保单信息出现多条。");
				return false;
			}
			mMsgSimPolicyInfo = (SimPolicyInfo) tSimPolicyInfoList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			errLog("报文信息读取异常 - 保单信息");
			return false;
		}

		// 投保人信息
		try {
			List tSimAppntInfoList = cMsgInfos.getBodyByFlag("SimAppntInfo");
			if (tSimAppntInfoList == null || tSimAppntInfoList.size() == 0) {
				errLog("投保人信息缺失。");
				return false;
			} else if (tSimAppntInfoList.size() != 1) {
				errLog("投保人信息出现多条。");
				return false;
			}
			mMsgSimAppntInfo = (SimAppntInfo) tSimAppntInfoList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			errLog("报文信息读取异常 - 投保人信息");
			return false;
		}
		try {
			List tSimInsuInfoList = cMsgInfos.getBodyByFlag("SimInsuInfo");
			if (tSimInsuInfoList == null || tSimInsuInfoList.size() == 0) {
				errLog("被保人信息缺失。");
				return false;
			}
			/*
			 * mMsgSimInsuInfoList = new SimInsuInfo[tSimInsuInfoList.size()];
			 * for (int i = 0; i < tSimInsuInfoList.size(); i++) { SimInsuInfo
			 * tSimInsuInfo = null; tSimInsuInfo = (SimInsuInfo)
			 * tSimInsuInfoList.get(i); mMsgSimInsuInfo = tSimInsuInfo; }
			 */
			mMsgSimInsuInfo = (SimInsuInfo) tSimInsuInfoList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			errLog("报文信息读取异常 - 被保人信息");
			return false;
		}
		try {
			List tWrapInfoList = cMsgInfos.getBodyByFlag("WrapInfo");
			if (tWrapInfoList == null || tWrapInfoList.size() == 0) {
				errLog("产品信息缺失。");
				return false;
			} else if (tWrapInfoList.size() != 1) {
				errLog("产品信息出现多条。");
				return false;
			}
			mMsgWrapInfo = (WrapInfo) tWrapInfoList.get(0);

		} catch (Exception e) {
			e.printStackTrace();
			errLog("报文信息读取异常 - 产品信息");
			return false;
		}

		InsuYear = (new ExeSQL().getOneValue("select days('"
				+ mMsgSimPolicyInfo.getCInValiDate() + "') - days('"
				+ mMsgSimPolicyInfo.getCValiDate() + "') from dual")).trim();

		return true;
	}

	private boolean qryTradeResult(VData cResultDatas) {
		// WFContListSchema tResSimPolicyInfo = (WFContListSchema) cResultDatas
		// .getObjectByObjectName("WFContListSchema", 0);
		// if (tResSimPolicyInfo == null) {
		// String tErrInfo = "查询保单卡产品信息失败。";
		// errLog(tErrInfo);
		// return false;
		// }
		// // for (int i = 0; i < mMsgSimInsuInfoList.length; i++) {
		// LICardActiveInfoListSchema tResWrapInfo =
		// (LICardActiveInfoListSchema) cResultDatas
		// .getObjectByObjectName("LICardActiveInfoListSchema", 0);
		// if (tResWrapInfo == null) {
		// String tErrInfo = "查询保单卡套餐信息失败。";
		// errLog(tErrInfo);
		// return false;
		// }
		// WFAppntListSchema tResWFAppntInfo = (WFAppntListSchema) cResultDatas
		// .getObjectByObjectName("WFAppntListSchema", 0);
		// if (tResWFAppntInfo == null) {
		// String tErrInfo = "查询保单卡投保人信息失败。";
		// errLog(tErrInfo);
		// return false;
		// }
		//
		// // for (int i = 0; i < mMsgSimInsuInfoList.length; i++) {
		// WFInsuListSchema tResWFSimInsuInfo = (WFInsuListSchema) cResultDatas
		// .getObjectByObjectName("WFInsuListSchema", 0);
		//
		// if (tResWFSimInsuInfo == null) {
		// String tErrInfo = "查询保单卡被保人信息失败。";
		// errLog(tErrInfo);
		// return false;
		//
		// }
		// 传什么返回什么
		SimPolicyInfo mSimPolicyInfo = new SimPolicyInfo();
		mSimPolicyInfo.setPrtNo(mMsgWrapInfo.getCardNo());
		mSimPolicyInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
		mSimPolicyInfo.setSaleChnl(mMsgSimPolicyInfo.getSaleChnl());
		mSimPolicyInfo.setAgentCom(mMsgSimPolicyInfo.getAgentCom());
		mSimPolicyInfo.setAgentCode(mMsgSimPolicyInfo.getAgentCode());
		mSimPolicyInfo.setAgentName(mMsgSimPolicyInfo.getAgentName());
		mSimPolicyInfo.setPolApplyDate(mMsgSimPolicyInfo.getPolApplyDate());
		mSimPolicyInfo.setCValiDate(mMsgSimPolicyInfo.getCValiDate());
		mSimPolicyInfo.setCValiTime(mMsgSimPolicyInfo.getCValiTime());
		mSimPolicyInfo.setCInValiDate(mMsgSimPolicyInfo.getCInValiDate());//
		mSimPolicyInfo.setCInValiTime(mMsgSimPolicyInfo.getCInValiTime());
		mSimPolicyInfo.setInsuYear(mMsgSimPolicyInfo.getInsuYear());
		mSimPolicyInfo.setInsuYearFlag(mMsgSimPolicyInfo.getInsuYearFlag());
		mSimPolicyInfo.setPremScope(mMsgSimPolicyInfo.getPremScope());
		mSimPolicyInfo.setPayIntv(mMsgSimPolicyInfo.getPayIntv());
		mSimPolicyInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
		mSimPolicyInfo.setPayYearFlag(mMsgSimPolicyInfo.getPayYearFlag());
		mSimPolicyInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
		mSimPolicyInfo.setPayMode(mMsgSimPolicyInfo.getPayMode());
		mSimPolicyInfo.setBankCode(mMsgSimPolicyInfo.getBankCode());
		mSimPolicyInfo.setBankAccNo(mMsgSimPolicyInfo.getBankAccNo());
		mSimPolicyInfo.setAccName(mMsgSimPolicyInfo.getAccName());
		mSimPolicyInfo.setRemark(mMsgSimPolicyInfo.getRemark());
		putResult("SimPolicyInfo", mSimPolicyInfo);

		WrapInfo tWFWrapInfo = new WrapInfo();
		tWFWrapInfo.setCardNo(mMsgWrapInfo.getCardNo());// 保单号
		tWFWrapInfo.setPassword(mMsgWrapInfo.getPassword());
		tWFWrapInfo.setWrapCode(mMsgWrapInfo.getWrapCode());
		tWFWrapInfo.setWrapName(mMsgWrapInfo.getWrapName());
		tWFWrapInfo.setCertifyCode(mMsgWrapInfo.getCertifyCode());// 单证编码
		tWFWrapInfo.setCertifyName(mMsgWrapInfo.getCertifyName());// 单证名字
		tWFWrapInfo.setActiveDate(mMsgWrapInfo.getActiveDate());// 保单激活日期
		tWFWrapInfo.setPrem(mMsgWrapInfo.getPrem());
		tWFWrapInfo.setAmnt(mMsgWrapInfo.getAmnt());
		tWFWrapInfo.setMult(mMsgWrapInfo.getMult());// 档次
		tWFWrapInfo.setCopys(mMsgWrapInfo.getCopys());// 份数
		tWFWrapInfo.setReMark(mMsgWrapInfo.getReMark());
		/*
		 * ExeSQL tExeSql = new ExeSQL(); String tStrSql =
		 * "select wrapname from LDWrap where riskwrapcode='"
		 * +mMsgWrapInfo.getWrapCode()+"' "; SSRS checkSSRS =
		 * tExeSql.execSQL(tStrSql);
		 * tWFWrapInfo.setWrapName(checkSSRS.GetText(1, 1));
		 */
		putResult("WrapInfo", tWFWrapInfo);

		SimAppntInfo tSimAppntInfo = new SimAppntInfo();
		tSimAppntInfo.setName(mMsgSimAppntInfo.getName());
		tSimAppntInfo.setSex(mMsgSimAppntInfo.getSex());
		tSimAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
		tSimAppntInfo.setIDType(mMsgSimAppntInfo.getIDType());
		tSimAppntInfo.setIdNo(mMsgSimAppntInfo.getIdNo());
		tSimAppntInfo.setOccupationType(mMsgSimAppntInfo.getOccupationType());
		tSimAppntInfo.setOccupationCode(mMsgSimAppntInfo.getOccupationCode());
		tSimAppntInfo.setPostalAddress(mMsgSimAppntInfo.getPostalAddress());
		tSimAppntInfo.setZipCode(mMsgSimAppntInfo.getZipCode());
		tSimAppntInfo.setPhone(mMsgSimAppntInfo.getPhone());
		tSimAppntInfo.setMobile(mMsgSimAppntInfo.getMobile());
		tSimAppntInfo.setEmail(mMsgSimAppntInfo.getEmail());
		tSimAppntInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
		tSimAppntInfo.setCompayPhone(mMsgSimAppntInfo.getCompayPhone());
		tSimAppntInfo.setCompAddr(mMsgSimAppntInfo.getCompAddr());
		tSimAppntInfo.setCompZipCode(mMsgSimAppntInfo.getCompZipCode());
		putResult("SimAppntInfo", tSimAppntInfo);

		// for (int i = 1; i <= tWFInsuInfoSet.size(); i++) {
		SimInsuInfo tSimInsuInfo = new SimInsuInfo();
		tSimInsuInfo.setInsuNo(mMsgSimInsuInfo.getInsuNo());
		tSimInsuInfo.setToMainInsuRela(mMsgSimInsuInfo.getToMainInsuRela());
		tSimInsuInfo.setToAppntRela(mMsgSimInsuInfo.getToAppntRela());
		tSimInsuInfo.setName(mMsgSimInsuInfo.getName());
		tSimInsuInfo.setSex(mMsgSimInsuInfo.getSex());
		tSimInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
		tSimInsuInfo.setIDType(mMsgSimInsuInfo.getIDType());
		tSimInsuInfo.setIdNo(mMsgSimInsuInfo.getIdNo());
		tSimInsuInfo.setOccupationType(mMsgSimInsuInfo.getOccupationType());
		tSimInsuInfo.setOccupationCode(mMsgSimInsuInfo.getOccupationCode());
		tSimInsuInfo.setPostalAddress(mMsgSimInsuInfo.getPostalAddress());
		tSimInsuInfo.setZipCode(mMsgSimInsuInfo.getZipCode());
		tSimInsuInfo.setPhone(mMsgSimInsuInfo.getPhone());
		tSimInsuInfo.setMobile(mMsgSimInsuInfo.getMobile());
		tSimInsuInfo.setEmail(mMsgSimInsuInfo.getEmail());
		tSimInsuInfo.setGrpName(mMsgSimInsuInfo.getGrpName());
		tSimInsuInfo.setCompayPhone(mMsgSimInsuInfo.getCompayPhone());
		tSimInsuInfo.setCompAddr(mMsgSimInsuInfo.getCompAddr());
		tSimInsuInfo.setCompZipCode(mMsgSimInsuInfo.getCompZipCode());
		tSimInsuInfo.setEnglishName(mMsgSimInsuInfo.getEnglishName());

		/*
		 * if ("0".equals(mMsgSimInsuInfo.getToAppntRela())) {
		 * tSimInsuInfo.setIDType(mMsgSimAppntInfo.getIDType()); } else {
		 */

		// }

		putResult("SimInsuInfo", tSimInsuInfo);

		return true;
	}

	private boolean tradeService() {
		tWFAppntInfo = getAppntInfo();
		if (tWFAppntInfo == null) {
			String tErrInfo = "查询投保人信息失败。";
			errLog(tErrInfo);
			return false;
		}
		tWFInsuInfo = getInsuListSetInfo();
		if (tWFInsuInfo == null) {
			String tErrInfo = "查询被保人信息失败。";
			errLog(tErrInfo);
			return false;
		}

		tWFContInfo = getSimPolicyInfo();
		if (tWFContInfo == null) {
			String tErrInfo = "查询保单信息失败。";
			errLog(tErrInfo);
			return false;
		}
		tLICardActiveInfo = getwrapInfo();
		if (tLICardActiveInfo == null) {
			String tErrInfo = "中间表信息缺失。";
			errLog(tErrInfo);
			return false;
		}
		tLZCardInfo = getLZCardInfo();
		if (tLZCardInfo == null) {
			String tErrInfo = "单证状态表信息缺失。";
			errLog(tErrInfo);
			return false;
		}
		/*
		 * String mBatchNo = mMsgHead.getBatchNo(); String mSendDate =
		 * mMsgHead.getSendDate(); String mSendTime = mMsgHead.getSendTime();
		 * String mBranchCode = mMsgHead.getBranchCode(); String mSendOperator =
		 * mMsgHead.getSendOperator(); String mMsgType = mMsgHead.getMsgType();
		 * String mPassword = mMsgWrapInfo.getPassword(); String mCardNo =
		 * mMsgWrapInfo.getCardNo(); String mWrapCode =
		 * mMsgWrapInfo.getWrapCode(); TransferData tTransferData = new
		 * TransferData(); tTransferData.setNameAndValue("BatchNo", mBatchNo);
		 * tTransferData.setNameAndValue("SendDate", mSendDate);
		 * tTransferData.setNameAndValue("SendTime", mSendTime);
		 * tTransferData.setNameAndValue("BranchCode", mBranchCode);
		 * tTransferData.setNameAndValue("SendOperator", mSendOperator);
		 * tTransferData.setNameAndValue("MsgType", mMsgType);
		 * tTransferData.setNameAndValue("Password", mPassword);
		 * tTransferData.setNameAndValue("CardNo", mCardNo);
		 * tTransferData.setNameAndValue("WrapCode", mWrapCode);
		 */

		mMap.put(tWFAppntInfo, SysConst.INSERT);
		mMap.put(tWFInsuInfo, SysConst.INSERT);
		mMap.put(tWFContInfo, SysConst.INSERT);
		mMap.put(tLICardActiveInfo, SysConst.INSERT);
		mMap.put(tLZCardInfo, SysConst.UPDATE);
		tVData.add(mMap);

		mResult.clear();
		mResult.add(tWFContInfo);
		mResult.add(tWFAppntInfo);
		mResult.add(tWFInsuInfo);
		mResult.add(tLICardActiveInfo);

		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		System.out.println(szFunc + ":" + szErrMsg);

		CError cError = new CError();
		cError.moduleName = "CertifyGrpContBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private WFInsuListSchema getInsuListSetInfo() {
		// WFInsuListSet tWFInsuInfos = new WFInsuListSet();
		/*
		 * for (int i = 0; i < mMsgSimInsuInfoList.length; i++) { SimInsuInfo
		 * mMsgSimInsuInfo = mMsgSimInsuInfo;
		 */
		// 被保人信息
		WFInsuListSchema tWFInsuInfo = new WFInsuListSchema();
		tWFInsuInfo.setInsuNo(mMsgSimInsuInfo.getInsuNo());// *
		tWFInsuInfo.setCardNo(mMsgWrapInfo.getCardNo());// *
		tWFInsuInfo.setBatchNo(mMsgHead.getBatchNo());// *
		tWFInsuInfo.setRelationToInsured("00");
		tWFInsuInfo.setToAppntRela(mMsgSimInsuInfo.getToAppntRela());
		String ToAppntRela = mMsgSimInsuInfo.getToAppntRela();
		if ("0".equals(ToAppntRela)) {// 若为本人投保则在投保人信息中获取五要素
			tWFInsuInfo.setToAppntRela("00");
			tWFInsuInfo.setIDNo(mMsgSimAppntInfo.getIdNo());
			tWFInsuInfo.setName(mMsgSimAppntInfo.getName());
			if (("01").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("0");// 身份证
				String idno = mMsgSimAppntInfo.getIdNo();
				if (idno.length() == 18) {
					tWFInsuInfo.setBirthday(idno.substring(6, 10) + "-"
							+ idno.substring(10, 12) + "-"
							+ idno.substring(12, 14));
					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女
					} else {
						tWFInsuInfo.setSex("0");
					}
				} else {
					String sSex = idno.substring(14);
					tWFInsuInfo.setBirthday("19" + idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女
					} else {
						tWFInsuInfo.setSex("0");
					}
				}
			} else if (("02").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("2");// 军官证
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

			} else if (("03").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("1");// 护照
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

			} else if (("04").equals(mMsgSimAppntInfo.getIDType())
					|| ("05").equals(mMsgSimAppntInfo.getIDType())
					|| ("99").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("4");// 回乡证/台胞证
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

			}

			else if (("06").equals(mMsgSimAppntInfo.getIDType())
					|| ("07").equals(mMsgSimAppntInfo.getIDType())) {
				tWFInsuInfo.setIDType("3");// 警官，士兵
				tWFInsuInfo.setSex(mMsgSimAppntInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
			}
			// tWFInsuInfo.setIDNo(mMsgSimAppntInfo.getIdNo());

		} else if ("1".equals(ToAppntRela)) {
			// 若为配偶投保，则根据情况判断其性别存入正确的与投保人关系
			if ("1".equals(mMsgSimInsuInfo.getSex())) {
				tWFInsuInfo.setToAppntRela("01");
				bsex = "1";
			} else {
				tWFInsuInfo.setToAppntRela("02");
				bsex = "0";
			}
			tWFInsuInfo.setName(mMsgSimInsuInfo.getName()); // *
			tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());// *
			if (("01").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("0");// 身份证
				String idno = mMsgSimInsuInfo.getIdNo();
				if (idno.length() == 18) {
					tWFInsuInfo.setBirthday(idno.substring(6, 10) + "-"
							+ idno.substring(10, 12) + "-"
							+ idno.substring(12, 14));
					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女
						bsex = "1";
						tWFInsuInfo.setToAppntRela("01");// 代表妻子
					} else {
						tWFInsuInfo.setSex("0");
						tWFInsuInfo.setToAppntRela("02");
						bsex = "0";
					}
				} else {
					String sSex = idno.substring(14);
					tWFInsuInfo.setBirthday("19" + idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女
						bsex = "1";
						tWFInsuInfo.setToAppntRela("01");
					} else {
						tWFInsuInfo.setSex("0");
						bsex = "0";
						tWFInsuInfo.setToAppntRela("02");
					}
				}

			}// 若不是身份证件，则直接判断性别存储对应关系
		
			if (("02").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("2");// 军官证
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());
			}
			if (("03").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("1");// 护照
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());
			}
			if (("04").equals(mMsgSimInsuInfo.getIDType())
					|| ("05").equals(mMsgSimInsuInfo.getIDType())
					|| ("99").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("4");// 回乡证/台胞证
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());
			}

			if (("06").equals(mMsgSimInsuInfo.getIDType())
					|| ("07").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("3");// 警官，士兵
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());
			}

		} else if ("2".equals(ToAppntRela)) {
			// 投保人子女
			tWFInsuInfo.setName(mMsgSimInsuInfo.getName()); // *
			tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());// *
			tWFInsuInfo.setToAppntRela("03");
			if (("01").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("0");// 身份证
				String idno = mMsgSimInsuInfo.getIdNo();
				if (idno.length() == 18) {
					tWFInsuInfo.setBirthday(idno.substring(6, 10) + "-"
							+ idno.substring(10, 12) + "-"
							+ idno.substring(12, 14));
					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女

					} else {
						tWFInsuInfo.setSex("0");

					}
				} else {
					String sSex = idno.substring(14);
					tWFInsuInfo.setBirthday("19" + idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tWFInsuInfo.setSex("1");// 1代表女

					} else {
						tWFInsuInfo.setSex("0");

					}
				}

			}
			if (("02").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("2");// 军官证
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());

			}
			if (("03").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("1");// 护照
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());

			}
			if (("04").equals(mMsgSimInsuInfo.getIDType())
					|| ("05").equals(mMsgSimInsuInfo.getIDType())
					|| ("99").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("4");// 回乡证/台胞证
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());

			}

			if (("06").equals(mMsgSimInsuInfo.getIDType())
					|| ("07").equals(mMsgSimInsuInfo.getIDType())) {
				tWFInsuInfo.setIDType("3");// 警官，士兵
				tWFInsuInfo.setSex(mMsgSimInsuInfo.getSex());
				tWFInsuInfo.setBirthday(mMsgSimInsuInfo.getBirthday());
				// tWFInsuInfo.setName(mMsgSimInsuInfo.getName());

			}

		} 
		/* tWFInsuInfo.setIDNo(mMsgSimInsuInfo.getIdNo()); */
		tWFInsuInfo.setOccupationType(mMsgSimInsuInfo.getOccupationType());
		tWFInsuInfo.setOccupationCode(mMsgSimInsuInfo.getOccupationCode());
		tWFInsuInfo.setPostalAddress(mMsgSimInsuInfo.getPostalAddress());
		tWFInsuInfo.setZipCode(mMsgSimInsuInfo.getZipCode());
		tWFInsuInfo.setPhont(mMsgSimInsuInfo.getPhone());
		tWFInsuInfo.setMobile(mMsgSimInsuInfo.getMobile());
		tWFInsuInfo.setEmail(mMsgSimInsuInfo.getEmail());
		tWFInsuInfo.setGrpName(mMsgSimInsuInfo.getGrpName());
		tWFInsuInfo.setCompanyPhone(mMsgSimInsuInfo.getCompayPhone());
		tWFInsuInfo.setCompAddr(mMsgSimInsuInfo.getCompAddr());
		tWFInsuInfo.setCompZipCode(mMsgSimInsuInfo.getCompZipCode());
		// tWFInsuInfo.setEnglishName(mMsgSimInsuInfo.getEnglishName());
		// tWFInsuInfos.add(tWFInsuInfo);

		return tWFInsuInfo;
	}

	private WFAppntListSchema getAppntInfo() {
		WFAppntListSchema tWFAppntInfo = new WFAppntListSchema();
		tWFAppntInfo.setBatchNo(mMsgHead.getBatchNo());// *
		tWFAppntInfo.setCardNo(mMsgWrapInfo.getCardNo());// *
		tWFAppntInfo.setAppntNo(mMsgWrapInfo.getCardNo());// *
		tWFAppntInfo.setName(mMsgSimAppntInfo.getName());
		tsex = mMsgSimAppntInfo.getSex();
		if (("01").equals(mMsgSimAppntInfo.getIDType())) {// 证件类型为身份证判断证件长度取得性别和生日
			tWFAppntInfo.setIDType("0");// 身份证
			String idno = mMsgSimAppntInfo.getIdNo();
			if (idno.length() == 18) {
				tWFAppntInfo
						.setBirthday(idno.substring(6, 10) + "-"
								+ idno.substring(10, 12) + "-"
								+ idno.substring(12, 14));
				String Sex = idno.substring(16, 17);
				if (Integer.parseInt(Sex) % 2 == 0) {
					tWFAppntInfo.setSex("1");// 1代表女
					tsex = "1";
				} else {
					tWFAppntInfo.setSex("0");
					tsex = "0";     
				}
			} else {
				String sSex = idno.substring(14);
				tWFAppntInfo.setBirthday("19" + idno.substring(6, 8) + "-"
						+ idno.substring(8, 10) + "-" + idno.substring(10, 12));
				if (Integer.parseInt(sSex) % 2 == 0) {
					tWFAppntInfo.setSex("1");// 1代表女
					tsex = "1";
				} else {
					tWFAppntInfo.setSex("0");
					tsex = "0";
				}
			}
			// 证件类型不为身份证根据对应关系转换存入数据库
		}
		else if (("02").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("2");// 军官证
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

		} else if (("03").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("1");// 护照
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

		} else if (("04").equals(mMsgSimAppntInfo.getIDType())
				|| ("05").equals(mMsgSimAppntInfo.getIDType())
				|| ("99").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("4");// 回乡证/台胞证
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());

		}

		else if (("06").equals(mMsgSimAppntInfo.getIDType())
				|| ("07").equals(mMsgSimAppntInfo.getIDType())) {
			tWFAppntInfo.setIDType("3");// 警官，士兵
			tWFAppntInfo.setSex(mMsgSimAppntInfo.getSex());
			tWFAppntInfo.setBirthday(mMsgSimAppntInfo.getBirthday());
		}
		
		tWFAppntInfo.setIDNo(mMsgSimAppntInfo.getIdNo());
		tWFAppntInfo.setOccupationType(mMsgSimAppntInfo.getOccupationType());
		tWFAppntInfo.setOccupationCode(mMsgSimAppntInfo.getOccupationCode());
		tWFAppntInfo.setPostalAddress(mMsgSimAppntInfo.getPostalAddress());
		tWFAppntInfo.setZipCode(mMsgSimAppntInfo.getZipCode());
		tWFAppntInfo.setPhont(mMsgSimAppntInfo.getPhone());
		tWFAppntInfo.setMobile(mMsgSimAppntInfo.getMobile());
		tWFAppntInfo.setEmail(mMsgSimAppntInfo.getEmail());
		tWFAppntInfo.setGrpName(mMsgSimAppntInfo.getGrpName());
		tWFAppntInfo.setCompanyPhone(mMsgSimAppntInfo.getCompayPhone());
		tWFAppntInfo.setCompAddr(mMsgSimAppntInfo.getCompAddr());
		tWFAppntInfo.setCompZipCode(mMsgSimAppntInfo.getCompZipCode());

		return tWFAppntInfo;
	}

	private WFContListSchema getSimPolicyInfo() {
		WFContListSchema tWFContInfo = new WFContListSchema();
		// 投保单数据
		// tWFContInfo.setPrtNo(mMsgSimPolicyInfo.getPrtNo());
		tWFContInfo.setSaleChnl(mMsgSimPolicyInfo.getSaleChnl());
		tWFContInfo.setManageCom(mMsgSimPolicyInfo.getManageCom());
		tWFContInfo.setBatchNo(mMsgHead.getBatchNo());
		tWFContInfo.setSendDate(mMsgHead.getSendDate());
		tWFContInfo.setSendTime(mMsgHead.getSendTime());
		tWFContInfo.setBranchCode(mMsgHead.getBranchCode());
		tWFContInfo.setSendOperator(mMsgHead.getSendOperator());
		tWFContInfo.setMessageType(mMsgHead.getMsgType());// 报文类型（01-出单；02-撤销；99-其他）
		tWFContInfo.setCardDealType("01");
		tWFContInfo.setCertifyCode(mMsgWrapInfo.getCertifyCode());
		tWFContInfo.setActiveDate(mMsgWrapInfo.getActiveDate());
		tWFContInfo.setAppntNo(mMsgWrapInfo.getCardNo());// *
		tWFContInfo.setCardNo(mMsgWrapInfo.getCardNo());

		tWFContInfo.setAgentCom(mMsgSimPolicyInfo.getAgentCom());
		tWFContInfo.setAgentCode(mMsgSimPolicyInfo.getAgentCode());
		tWFContInfo.setAgentName(mMsgSimPolicyInfo.getAgentName());

		tWFContInfo.setSendDate(mMsgSimPolicyInfo.getPolApplyDate());// 报送的日期
		tWFContInfo.setCvaliDate(mMsgSimPolicyInfo.getCValiDate());// *//保单生效日
		tWFContInfo.setCvaliDateTime(mMsgSimPolicyInfo.getCValiTime());
		// 存放保单终止日期
		tWFContInfo.setAmnt(calAmnt);
		tWFContInfo.setPrem(calPrem);
		// tWFContInfo.setPremScope(mMsgSimPolicyInfo.getPremScope());
		tWFContInfo.setInsuYear(InsuYear);
		tWFContInfo.setPayYearFlag("D");
		tWFContInfo.setPayMode(mMsgSimPolicyInfo.getPayMode());
		tWFContInfo.setPayIntv(mMsgSimPolicyInfo.getPayIntv());// 0趸交
		tWFContInfo.setPayYear(mMsgSimPolicyInfo.getPayYear());
		tWFContInfo.setInsuYearFlag("D");
		tWFContInfo.setRemark(mMsgSimPolicyInfo.getRemark());
		tWFContInfo.setCopys(mMsgWrapInfo.getCopys()); // 档次
		tWFContInfo.setMult(mMsgWrapInfo.getMult()); // 份数
		tWFContInfo.setBak2(mMsgWrapInfo.getCardNo());
		tWFContInfo.setBak3(mMsgSimPolicyInfo.getCInValiDate());// 保单终止日期
		 tWFContInfo.setBak4(mMsgSimPolicyInfo.getBankAccNo());
		// tWFContInfo.setBankAccNo(mMsgSimPolicyInfo.getBankCode());
		return tWFContInfo;
	}

	private LICardActiveInfoListSchema getwrapInfo() {

		/* for (int i = 0; i < mMsgSimInsuInfoList.length; i++) { */
		LICardActiveInfoListSchema tLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
		/*
		 * if("".equals(mMsgSimInsuInfo.getInsuNo())||null==
		 * mMsgSimInsuInfo.getInsuNo()){
		 * tLICardActiveInfoListSchema.setSequenceNo("1"); }else{
		 */
		tLICardActiveInfoListSchema.setSequenceNo("1");
		// }
		tLICardActiveInfoListSchema.setBatchNo(BittchNo);
		tLICardActiveInfoListSchema.setCardNo(mMsgWrapInfo.getCardNo());
		tLICardActiveInfoListSchema.setCardType(mMsgWrapInfo.getWrapCode());
		tLICardActiveInfoListSchema.setCValidate(mMsgSimPolicyInfo
				.getCValiDate());
		tLICardActiveInfoListSchema.setActiveDate(mMsgWrapInfo.getActiveDate());
		tLICardActiveInfoListSchema.setDealFlag("00");// 置为激活导入未处理状态
		// 被保人信息
		if ("0".equals(mMsgSimInsuInfo.getToAppntRela())) {
			// 若为本人投保，则存入投保人信息
			tLICardActiveInfoListSchema.setIdNo(mMsgSimAppntInfo.getIdNo());
			tLICardActiveInfoListSchema.setName(mMsgSimAppntInfo.getName());
			if (("01").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("0");// 身份证
				String idno = mMsgSimAppntInfo.getIdNo();
				if (idno.length() == 18) {
					tLICardActiveInfoListSchema.setBirthday(idno.substring(6,
							10)
							+ "-"
							+ idno.substring(10, 12)
							+ "-"
							+ idno.substring(12, 14));

					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tLICardActiveInfoListSchema.setSex("1");// 1代表女
					} else {
						tLICardActiveInfoListSchema.setSex("0");
					}
				} else {
					String sSex = idno.substring(14);
					tLICardActiveInfoListSchema.setBirthday("19"
							+ idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tLICardActiveInfoListSchema.setSex("1");// 1代表女
					} else {
						tLICardActiveInfoListSchema.setSex("0");
					}
				}
			} else if (("02").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("2");// 军官证
				tLICardActiveInfoListSchema.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimAppntInfo
						.getBirthday());

			} else if (("03").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("1");// 护照
				tLICardActiveInfoListSchema.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimAppntInfo
						.getBirthday());

			} else if (("04").equals(mMsgSimAppntInfo.getIDType())
					|| ("05").equals(mMsgSimAppntInfo.getIDType())
					|| ("99").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("4");// 回乡证/台胞证
				tLICardActiveInfoListSchema.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimAppntInfo
						.getBirthday());

			}

			else if (("06").equals(mMsgSimAppntInfo.getIDType())
					|| ("07").equals(mMsgSimAppntInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("3");// 警官，士兵
				tLICardActiveInfoListSchema.setSex(mMsgSimAppntInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimAppntInfo
						.getBirthday());
			}
			// tLICardActiveInfoListSchema.setIdNo(mMsgSimAppntInfo.getIdNo());

		} else if ("1".equals(mMsgSimInsuInfo.getToAppntRela())
				|| "2".equals(mMsgSimInsuInfo.getToAppntRela())) {
			// 配偶或子女根据情况存入对应信息
			tLICardActiveInfoListSchema.setIdNo(mMsgSimInsuInfo.getIdNo());
			tLICardActiveInfoListSchema.setName(mMsgSimInsuInfo.getName());
			if (("01").equals(mMsgSimInsuInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("0");// 身份证
				String idno = mMsgSimInsuInfo.getIdNo();
				if (idno.length() == 18) {
					tLICardActiveInfoListSchema.setBirthday(idno.substring(6,
							10)
							+ "-"
							+ idno.substring(10, 12)
							+ "-"
							+ idno.substring(12, 14));
					String Sex = idno.substring(16, 17);
					if (Integer.parseInt(Sex) % 2 == 0) {
						tLICardActiveInfoListSchema.setSex("1");// 1代表女
					} else {
						tLICardActiveInfoListSchema.setSex("0");
					}
				} else {
					String sSex = idno.substring(14);
					tLICardActiveInfoListSchema.setBirthday("19"
							+ idno.substring(6, 8) + "-"
							+ idno.substring(8, 10) + "-"
							+ idno.substring(10, 12));
					if (Integer.parseInt(sSex) % 2 == 0) {
						tLICardActiveInfoListSchema.setSex("1");// 1代表女
					} else {
						tLICardActiveInfoListSchema.setSex("0");
					}
				}
			} else if (("02").equals(mMsgSimInsuInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("2");// 军官证
				tLICardActiveInfoListSchema.setSex(mMsgSimInsuInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimInsuInfo
						.getBirthday());

			} else if (("03").equals(mMsgSimInsuInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("1");// 护照
				tLICardActiveInfoListSchema.setSex(mMsgSimInsuInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimInsuInfo
						.getBirthday());

			} else if (("04").equals(mMsgSimInsuInfo.getIDType())
					|| ("05").equals(mMsgSimInsuInfo.getIDType())
					|| ("99").equals(mMsgSimInsuInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("4");// 回乡证/台胞证
				tLICardActiveInfoListSchema.setSex(mMsgSimInsuInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimInsuInfo
						.getBirthday());

			}

			else if (("06").equals(mMsgSimInsuInfo.getIDType())
					|| ("07").equals(mMsgSimInsuInfo.getIDType())) {
				tLICardActiveInfoListSchema.setIdType("3");// 警官，士兵
				tLICardActiveInfoListSchema.setSex(mMsgSimInsuInfo.getSex());
				tLICardActiveInfoListSchema.setBirthday(mMsgSimInsuInfo
						.getBirthday());

			}
		}

		// tLICardActiveInfoListSchema.setIdNo(mMsgSimInsuInfo.getIdNo());
		tLICardActiveInfoListSchema.setOccupationType(mMsgSimInsuInfo
				.getOccupationType());
		tLICardActiveInfoListSchema.setOccupationCode(mMsgSimInsuInfo
				.getOccupationCode());
		tLICardActiveInfoListSchema.setPostalAddress(mMsgSimInsuInfo
				.getPostalAddress());
		tLICardActiveInfoListSchema.setZipCode(mMsgSimInsuInfo.getZipCode());
		tLICardActiveInfoListSchema.setPhone(mMsgSimInsuInfo.getPhone());
		tLICardActiveInfoListSchema.setMobile(mMsgSimInsuInfo.getMobile());
		tLICardActiveInfoListSchema.setGrpName(mMsgSimAppntInfo.getGrpName());
		tLICardActiveInfoListSchema.setCompanyPhone(mMsgSimInsuInfo
				.getCompayPhone());
		tLICardActiveInfoListSchema.setEMail(mMsgSimInsuInfo.getEmail());
		// tLICardActiveInfoListSchema.setPassword(mMsgWrapInfo.getPassword());
		tLICardActiveInfoListSchema.setState("00");
		tLICardActiveInfoListSchema.setOperator(mMsgHead.getSendOperator());
		tLICardActiveInfoListSchema.setMakeDate(PubFun.getCurrentDate());
		tLICardActiveInfoListSchema.setMakeTime(PubFun.getCurrentTime());
		tLICardActiveInfoListSchema.setModifyDate(PubFun.getCurrentDate());
		tLICardActiveInfoListSchema.setModifyTime(PubFun.getCurrentTime());
		tLICardActiveInfoListSchema.setPrem(calPrem);
		tLICardActiveInfoListSchema.setAmnt(calAmnt);
		tLICardActiveInfoListSchema.setMult(mMsgWrapInfo.getMult());
		tLICardActiveInfoListSchema.setCopys(mMsgWrapInfo.getCopys());
		tLICardActiveInfoListSchema.setRemark(mMsgWrapInfo.getReMark());
		tLICardActiveInfoListSchema.setCardStatus("01");
		tLICardActiveInfoListSchema.setInsuYear(InsuYear);
		tLICardActiveInfoListSchema.setInsuYearFlag("D");
		tLICardActiveInfoListSchema.setInActiveDate(mMsgSimPolicyInfo
				.getCInValiDate());// 保单终止日期
		/*
		 * tLICardActiveInfos.add(tLICardActiveInfoListSchema); }
		 */
		return tLICardActiveInfoListSchema;
	}

	private LZCardSchema getLZCardInfo() {
		String tSQL = "select lzc.* "
				+ " from lzcardnumber lzcn "
				+ " inner join lzcard lzc on lzcn.cardtype = lzc.subcode and lzc.startno = lzcn.cardserno and lzc.endno = lzcn.cardserno "
				+ " where 1 = 1 and lzcn.cardno = '" + mMsgWrapInfo.getCardNo()
				+ "' ";
		LZCardSet tLZCardInfos = new LZCardDB().executeQuery(tSQL);
		if (tLZCardInfos == null || tLZCardInfos.size() == 0) {
			buildError("dealCertInfo", "单证信息未找到。");
			return null;
		} else if (tLZCardInfos.size() != 1) {
			buildError("dealCertInfo", "单证信息出现多条。");
			return null;
		}
		LZCardSchema tLZCardInfo = tLZCardInfos.get(1);
		String state = tLZCardInfo.getState();
		if (!state.equals("2") && !state.equals("13")) {
			tLZCardInfo.setState("14");
		}
		tLZCardInfo.setActiveFlag("1");
		return tLZCardInfo;
	}

/*	private static void errLog(StringBuffer tErrInfos, String tErrMsg) {
		System.out.println("CertInfoManager: " + tErrMsg);
		if (tErrInfos == null)
			return;
		tErrInfos.append(tErrMsg);

	}*/
}
