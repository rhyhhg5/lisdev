/**
 * RuleServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.brms.databus.client;

public interface RuleServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getRuleServiceImplPortAddress();

    public com.sinosoft.brms.databus.client.RuleService getRuleServiceImplPort() throws javax.xml.rpc.ServiceException;

    public com.sinosoft.brms.databus.client.RuleService getRuleServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
