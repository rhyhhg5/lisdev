/**
 * RuleService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.brms.databus.client;

public interface RuleService extends java.rmi.Remote {
    public java.lang.String fireRule(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, boolean arg4) throws java.rmi.RemoteException;
}
