/**
 * <p>Title: 民生核心业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: SINOSOFT Copyright (c) 2004</p>
 * <p>Company: 中科软科技</p>
 * @author guoxiang
 * @version 1.0
 */

package com.sinosoft.audit;
import com.sinosoft.utility.*;
public interface CalService {
  public boolean submitData(VData cInputData, String cOperate);
  public VData getResult();
  public CErrors getErrors();
}