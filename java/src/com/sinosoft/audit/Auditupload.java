package com.sinosoft.audit;

import java.util.HashMap;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.lis.vschema.Staff_Info_tempSet;
import com.sinosoft.lis.vschema.Branch_Info_tempSet;
import com.sinosoft.lis.vschema.Plan_info_tempSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author shenq
 * @version 1.0
 */
public class Auditupload {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 业务处理相关变量 */
	private TransferData mTransferData = new TransferData();

	/** 全局变量 */
	private GlobalInput mGlobalInput;

	private String mFilename = "";

	private String mFilePath = "";

	/** 使用默认的导入方式 */
	private AuditSheetImport importer;
    /** Sheet对应table的名字*/
    private static final String[] mTableNames = {"Staff_Info_temp","Branch_Info_temp","Plan_info_temp"};
    /** 读取文件开始行 */
    private int mStartRows = 1;
	/** 节点名 */
	//private String[] sheetName = { "Staff_Info_temp", "Branch_Info_temp", "Plan_info_temp" };
	private String[] sheetName = { "Sheet1","Sheet2","Sheet3"};

	/** 配置文件名 */
	private String configName = "AuditImport.xml";

	public Auditupload() {

	}

	public boolean submitData(VData cInputData,String cOperate) {
        //cOperate区分操作类型
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {//多sheet文件导入处理 如果在前台页面想单sheet导入的话，改造分别对应mTableNames sheetName  configName就可以
			return false;
		}
		return true;
	}

	private boolean dealData() {
		if (!doAdd(mFilePath, mFilename)) {// 路径 文件名
			buildError("dealData","导入文件失败！");
			return false;
		}
		return true;
	}

	/*
	 * 开始导入处理
	 * excel文件路径 xml配置文件路径  sheet名数组
	 */
	private boolean doAdd(String path, String fileName) {
		importer = new AuditSheetImport(path + fileName, path + configName,
				sheetName);
        importer.setTableName(mTableNames);
        importer.setMStartRows(mStartRows);
		if (!importer.doImport()) {
			mErrors.copyAllErrors(importer.mErrors);
			return false;
		}
		Staff_Info_tempSet tStaff_Info_tempSet = (Staff_Info_tempSet) getSchemaSet();//先试验一个表
		Branch_Info_tempSet tBranch_InfoSet = (Branch_Info_tempSet) getBranchSet();//第二张表，考虑循环
		Plan_info_tempSet   tPlan_info_tempSet=(Plan_info_tempSet)getPlanSet();
		//插入数据时有没有其他方法
		MMap map = new MMap();
        for (int i = 1; i <= tStaff_Info_tempSet.size(); i++) {
            map.put(tStaff_Info_tempSet.get(i), "DELETE&INSERT");
        }
        for (int i = 1; i <= tBranch_InfoSet.size(); i++) {
            map.put(tBranch_InfoSet.get(i), "DELETE&INSERT");
        }
        for (int i = 1; i <= tPlan_info_tempSet.size(); i++) {//没有主键  先写成sql删除语句
            String SQL="delete from Plan_info_temp where 1=1 " +
            ReportPubFun.getWherePart("Plan_Code",tPlan_info_tempSet.get(i).getPlan_Code())+
            ReportPubFun.getWherePart("Plan_type",tPlan_info_tempSet.get(i).getPlan_type())+
            ReportPubFun.getWherePart("Design_type",tPlan_info_tempSet.get(i).getDesign_type())+
            ReportPubFun.getWherePart("Regu_Type",tPlan_info_tempSet.get(i).getRegu_Type())+
            ReportPubFun.getWherePart("File_Code",tPlan_info_tempSet.get(i).getFile_Code())+
            ReportPubFun.getWherePart("File_Date",tPlan_info_tempSet.get(i).getFile_Date());
            map.put(SQL, "DELETE");
            map.put(tPlan_info_tempSet.get(i), "INSERT");
        }
        VData data = new VData();
        data.add(map);
		// 提交数据到数据库
	    PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data,"")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
		return true;
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean submitData(MMap map) {
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) {
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	/*
	 * 分别获取数据
	 */
	private Staff_Info_tempSet getSchemaSet() {
		Staff_Info_tempSet tStaff_Info_tempSet=new Staff_Info_tempSet();
		HashMap tHashMap =  importer.getResult();
		tStaff_Info_tempSet.add((Staff_Info_tempSet)tHashMap.get(sheetName[0]));
		return tStaff_Info_tempSet;
	}
	private Branch_Info_tempSet getBranchSet() {
		Branch_Info_tempSet tBranch_Info_tempSet = new Branch_Info_tempSet();
		HashMap tHashMap = importer.getResult();
		tBranch_Info_tempSet.add((Branch_Info_tempSet) tHashMap.get(sheetName[1]));
		return tBranch_Info_tempSet;
	}
	private Plan_info_tempSet getPlanSet() {
		Plan_info_tempSet tPlan_info_tempSet = new Plan_info_tempSet();
		HashMap tHashMap = importer.getResult();
		tPlan_info_tempSet.add((Plan_info_tempSet) tHashMap.get(sheetName[2]));
		return tPlan_info_tempSet;
	}
	/*
	 * 获取传入信息
	 */
	private boolean getInputData(VData cInputData) {
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);
		if (mTransferData == null) {
			buildError("getInputData", "没有得到上传的文件名称和路径！");
			return false;
		}
		mFilename = mTransferData.getValueByName("FileName").toString();
		mFilePath = mTransferData.getValueByName("FilePath").toString();
		if (mFilename == null || mFilename.equals("") || mFilePath == null
				|| mFilePath.equals("")) {
			buildError("getInputData", "没有得到上传的文件名称和路径！");
			return false;
		}
		return true;
	}

	/*
	 * 错误处理
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "Auditupload";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

}