package com.sinosoft.audit;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.math.BigInteger;
import java.sql.Connection;
import java.util.*;


/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:报表一级汇总提数处理接口类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author:sq
 * @version 2.0
 */
public class AuditEngineBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	private MMap mMap = new MMap();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 业务处理相关变量 */
	private TransferData mTransferData = new TransferData();

	private String tOperate;

	private String mNeedItemKey;

	private String mUpPol1 = ""; // 更新pol_main表，若业务来源[BUSI_SRC_TYPE]为直销（1）

	private String mUpPol2 = ""; // 更新pol_main表，若业务来源[BUSI_SRC_TYPE]为个人代理（2）

	private String mUpPol3 = ""; // 更新pol_main表，若业务来源[BUSI_SRC_TYPE]为专业代理、兼业代理、经纪业务（3，4，5）

	private String mUpPrem1 = ""; // 更新Prem_info表，若业务来源[BUSI_SRC_TYPE]为直销（1）

	private String mUpPrem2 = ""; // 更新Prem_info表，若业务来源[BUSI_SRC_TYPE]为个人代理（2）

	private String mUpPrem3 = ""; // 更新Prem_info表，若业务来源[BUSI_SRC_TYPE]为专业代理、兼业代理、经纪业务（3，4，5）

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private Connection mConn = null;
	private VData mBillInfoResult = new VData();
	
	public AuditEngineBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 处理提数逻辑
		if (!dealData()) {
			return false;
		}
		
		//拆分bill_info之批量发放记录为多条记录 Added By Qisl At 2009.04.15
		//只有提取单证信息表时才进行拆分 Modified By Liuyp At 2009.04.30
		if(!dealSplitBill_Info())
		{
			return false;
		}
		
		//为了提取数据的完整性，将保单分配、保单迁移、异地理赔业务所导致的营销员、银保专管员和中介机构信息插入到相关表中
		if(!dealSpec_Buss())
		{
			return false;
		}

		//多线程并发提数改造  Deleted By Qisl At 2009.03.19
//		// 提交到BLS进行插入INSERT 处理
//		AuditEngineBLS tAuditEngineBLS = new AuditEngineBLS();
//
//		if (!tAuditEngineBLS.submitData(mResult, tOperate)) {
//			// @@错误处理
//			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
//			buildError("submitData", "数据提数失败！");
//			mResult.clear();
//
//			return false;
//		} else {
//			mResult = tAuditEngineBLS.getResult();
//			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
//		}
		
		//处理新单期缴保费年期代码 Added by liuyp  2009-6-13
		if (!dealNewBuss()) {
			buildError("submitData：dealNewBuss", "处理新单期缴保费年期代码失败");
			return false;
		}
		
        //处理险种期限类别  Added by liuyp  2009-6-13
		if (!dealPlanPeriod()) {
			buildError("submitData：dealPlanPeriod", "处理险种期限类别失败");
			return false;
		}
		
//		if (!dealPremDate()) {
//			buildError("submitData：dealPremDate", "处理缴别为趸缴的记录失败");
//			return false;
//		}
//		if (!dealEdorType()) {
//			buildError("submitData：dealEdorType", "处理批单费用表为一般保全的记录失败");
//			return false;
//		}

		return true;
	}

	/**
	 * 为了提取数据的完整性，将保单分配、保单迁移、异地理赔业务所导致的营销员、银保专管员和中介机构信息插入到相关表中
	 */
	private boolean dealSpec_Buss()
	{
        //查找计算处理的记录
		String strSQL = "SELECT * FROM LFDesbMode where itemtype like 'U%' with ur";
		System.out.println("处理特殊业务查询描述表的sql:" + strSQL);

		LFDesbModeSet tLFDesbModeSet = new LFDesbModeDB().executeQuery(strSQL);
		System.out.println("完成查询sql描述");

		if ((tLFDesbModeSet == null) || (tLFDesbModeSet.size() == 0)) {//没有描述时的处理
			buildError("dealSpec_Buss", "没有对该表的系统数据导入SQL进行描述！");
			return false;
		}

		mResult.clear();

		for (int i = 1; i <= tLFDesbModeSet.size(); i++) {
			LFDesbModeSchema mLFDesbModeSchema = tLFDesbModeSet.get(i);

			try {
				if (!CheckTransitionCondition(mLFDesbModeSchema, mTransferData)) {
					return false;
				}

			} catch (Exception ex) {
				ex.printStackTrace();

				return false;
			}
		}
		
		// 提交到BLS进行插入INSERT 处理
		AuditEngineBLS tAuditEngineBLS = new AuditEngineBLS();

		if (!tAuditEngineBLS.submitData(mResult, tOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
			buildError("submitData", "数据提数失败！");
			mResult.clear();

			return false;
		} else {
			mResult = tAuditEngineBLS.getResult();
			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
		}
		return true;
	}

	/**
	 * 解决保全取不到保险期间和缴费期间的情况
	 */
	private boolean dealNewBuss() {
	    System.out.print("dealNewBuss");
		mInputData = new VData();
		MMap mMMap = new MMap();
		String tUpPrem1 = "";
		String tUpPrem2 = "";
		String tUpPrem3 = "";
		String tUpPrem4 = "";
		tUpPrem1 = " update prem_info set New_busi_Code='01' where New_busi_Code is null";
		tUpPrem2 = " update prem_info set New_busi_Code='00' where New_busi_Code = '11' and (Period <= 12 or Prem_Term <> 0)";
		tUpPrem3 = " update prem_info set (Period,Prem_Term) = (24,24) where New_busi_Code = '02' and (Period <= 12 or Prem_Term <> 24)";

		mMMap.put(tUpPrem1, "UPDATE");
		mMMap.put(tUpPrem2, "UPDATE");
		mMMap.put(tUpPrem3, "UPDATE");

		mInputData.add(mMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			System.out.println(tPubSubmit.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PubSubmit";
			tError.functionName = "submitData";
			tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/**
	 * 解决核心险种类别RiskPeriod和险种分类5RiskType5不一致的情况
	 */
	private boolean dealPlanPeriod() {
		System.out.print("dealPlanPeriod");
		mInputData = new VData();
		MMap mMMap = new MMap();
		String tUpPlan = "";
		tUpPlan = " update plan_info set Plan_period='2' where plan_code='590206'";

		mMMap.put(tUpPlan, "UPDATE");

		mInputData.add(mMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			System.out.println(tPubSubmit.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PubSubmit";
			tError.functionName = "submitData";
			tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/**
	 * 判断是否提取单证信息表，只有提取才进行数据拆分
	 * @return
	 */
	private boolean dealSplitBill_Info()
	{
		String[] KeyWord = PubFun.split(mOperate, "||");
		System.out.println(KeyWord[2]);
		if(KeyWord[2].indexOf("A17") >= 0)
		{
			if(!dealBill_Info())
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 拆分记录
	 * @return
	 */
	private boolean dealBill_Info()
	{
		String tManageCom = (String)mTransferData.getValueByName("ManageCom");
		if (tManageCom == null ||"".equals(tManageCom))
		{
			CError.buildErr(this, "提数机构获取失败！");
			return false;
		}
		
		String tSql = 
			"select a.BILL_CODE,a.START_PRINT_NO,a.END_PRINT_NO,a.PRINT_NO,a.N_SERIAL_AMOUNT,a.USE_STATUS,a.BRANCH_CODE,a.USE_TIME,a.VERIFIC_TIME,a.INVALID_TIME " 
		  + " from bill_info a "
		  + " where n_serial_amount > 1 " 
		  + " and branch_code like '"+ tManageCom +"%'" 
		  + " with ur ";
		
		RSWrapper tRSWrapper = new RSWrapper();
    	SSRS tSSRS = new SSRS();
    	
        tRSWrapper.prepareData(null,tSql);
        try
        {
			do
			{
				tSSRS = tRSWrapper.getSSRS();
				if (tSSRS != null && tSSRS.MaxRow > 0) {
					for (int j = 1; j <= tSSRS.getMaxRow(); j++)
		            {
						if (!dealOneBillInfo(tSSRS.getRowData(j)))
						{
							return false;
						}
					}
				}
			}
			while (tSSRS != null && tSSRS.MaxRow > 0);
		}
        catch (Exception ex)
        {
            ex.printStackTrace();
            tRSWrapper.close();
        }finally{
        	try{
        		tRSWrapper.close();
        	}catch(Exception ex){}
        }
        
        try
		{
			PubSubmit sPubSubmit = new PubSubmit();
			if (mConn != null)
			{
				sPubSubmit.setConnection(mConn);// 传入连接
				sPubSubmit.setCommitFlag(true);// 传入提交方式
				mBillInfoResult.clear();
				if (sPubSubmit.submitData(mBillInfoResult, "") == false)
				{
					this.mErrors.addOneError("PubSubmit:处理数据库失败!");
					return false;
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			CError.buildErr(this, ex.toString());
			try
			{
				mConn.rollback();
				mConn.close();
			}
			catch (Exception x)
			{}
			return false;
		}
		finally
		{// 释放没有释放的数据库连接
			try
			{
				if (mConn != null && !mConn.isClosed())
				{
					mConn.close();
				}
			}
			catch (Exception x)
			{}
		}
        
		return true;
	}

	/**
	 * 处理一条bill_info记录
	 */
	private boolean dealOneBillInfo(String[] strArr)
	{
		//a.BILL_CODE,a.START_PRINT_NO,a.END_PRINT_NO,a.PRINT_NO,a.N_SERIAL_AMOUNT,a.USE_STATUS,a.BRANCH_CODE,
		//a.USE_TIME,a.VERIFIC_TIME,a.INVALID_TIME
		try
		{
			if (mConn == null)
			{
				mConn = DBConnPool.getConnection();
			}
			if (mConn == null)
			{
				// @@错误处理
				CError.buildErr(this, "拆分单证信息表，申请数据库连接失败");
				return false;
			}
			
			//防止已拆分的记录再被拆分 Modified By Liuyp At 2009.04.30
			String tSql = "select count(*) from bill_info"
				  + " where bill_code = '" + strArr[0]+ "'"
				  + " and start_Print_no = '" + strArr[1]+ "'"
				  + " and end_print_no = '" + strArr[2]+ "'"
				  + " with ur ";
			ExeSQL tExeSQL = new ExeSQL();
			int mNum = Integer.parseInt(tExeSQL.getOneValue(tSql));
            System.out.println("得到的记录数为：　" + mNum);
			if (mNum==1)
			{
				int iLoopCount = Integer.parseInt(strArr[4]);
				for(int i=1; i<iLoopCount; i++)
				{
					String sPrintNo = bigIntegerPlus(strArr[3], String.valueOf(i), strArr[3].length());
					
					mBillInfoResult.clear();
					MMap tMap = new MMap();
					String insertSql = "insert into bill_info values("
						              +"'" + strArr[0]+ "',"
						              +"'" + strArr[1]+ "',"
						              +"'" + strArr[2]+ "',"
						              +"'" + sPrintNo+ "',"
						              +"" + Integer.parseInt(strArr[4])+ ","
						              +"'" + strArr[5]+ "',"
						              +"'" + strArr[6]+ "',";
					if("".equals(strArr[7]))
					{
						insertSql = insertSql +"null,";
					}else
					{
						insertSql = insertSql + "'" + strArr[7]+ "',";
					}
					if("".equals(strArr[8]))
					{
						insertSql = insertSql +"null,";
					}else
					{
						insertSql = insertSql + "'" + strArr[8]+ "',";
					}
					if("".equals(strArr[9]))
					{
						insertSql = insertSql +"null)";
					}else
					{
						insertSql = insertSql +"'" + strArr[9]+ "')";
					}
					
					tMap.put(insertSql, "INSERT");
					mBillInfoResult.add(tMap);

					PubSubmit tPubSubmit = new PubSubmit();
					tPubSubmit.setConnection(mConn);// 传入连接
					tPubSubmit.setCommitFlag(false);// 传入提交方式
					if (tPubSubmit.submitData(mBillInfoResult, "") == false)
					{
						this.mErrors.addOneError("PubSubmit:处理数据库失败!");
						return false;
					}
				}
			}
		}catch(Exception ex)
		{
//			 @@异常或者错误处理
			ex.printStackTrace();
			CError.buildErr(this, ex.toString());
			try
			{
				mConn.rollback();
				mConn.close();
			}
			catch (Exception x)
			{}
			return false;
		}
		return true;
	}
	
	/**
	 * 大数累加
	 * @param strX
	 * @param strY
	 * @param nMinLen
	 * @return
	 */
	private String bigIntegerPlus(String strX, String strY, int nMinLen)
	{
		BigInteger bigX = new BigInteger(strX);
		BigInteger bigY = new BigInteger(strY);

		String str = (bigX.add(bigY)).toString();
		String strTemp = "00000000000000000000";
		strTemp = strTemp.substring(1, (nMinLen - str.length()) + 1);

		return strTemp + str;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		String[] KeyWord = PubFun.split(mOperate, "||");

		// 查找计算处理的记录
		String strSQL = "SELECT * FROM LFDesbMode where 1=1"
				+ ReportPubFun.getWherePart("ItemCode", KeyWord[0])
				+ ReportPubFun.getWherePart("ItemNum", KeyWord[1]) + " "
				+ KeyWord[2];
		System.out.println("通过前台的条件查询描述表:" + strSQL);

		LFDesbModeSet tLFDesbModeSet = new LFDesbModeDB().executeQuery(strSQL);
		System.out.println("完成查询sql描述");

		if ((tLFDesbModeSet == null) || (tLFDesbModeSet.size() == 0)) {//没有描述时的处理
			buildError("dealData", "没有对该表的系统数据导入SQL进行描述！");
			return false;
		}

		mResult.clear();

//		多线程并发提数改造  Deleted By Qisl At 2009.03.19
//		for (int i = 1; i <= tLFDesbModeSet.size(); i++) {
//			LFDesbModeSchema mLFDesbModeSchema = tLFDesbModeSet.get(i);
//
//			try {
//				if (!CheckTransitionCondition(mLFDesbModeSchema, mTransferData)) {
//					return false;
//				}
//
//			} catch (Exception ex) {
//				ex.printStackTrace();
//
//				return false;
//			}
//		}
		
		VData tVData = new VData();
		tVData.add(mGlobalInput);
		tVData.add(mTransferData);
		tVData.add(tLFDesbModeSet);
		
		Thread thread1 = new Thread(new AuditImportThread("Prem_Info",tVData));
		thread1.start();
		System.out.println("Audit        Thread1 start......");
		Thread thread2 = new Thread(new AuditImportThread("Pol_Main||BILL_INFO||Bill_Code_Info||Branch_Info||Salesman_Info||Staff_Info||Banc_Speci_Info||agt_code||Plan_info||Gener_Account_Code||Sub_Account_Code",tVData));
		thread2.start();
		System.out.println("Audit        Thread2 start......");
		Thread thread3 = new Thread(new AuditImportThread("pay_due||Endo_Fee",tVData));
		thread3.start();
		System.out.println("Audit        Thread3 start......");
		Thread thread4 = new Thread(new AuditImportThread("claim_main||claim_report",tVData));
		thread4.start();
		System.out.println("Audit        Thread4 start......");
		Thread thread5 = new Thread(new AuditImportThread("Voucher_Info",tVData));
		thread5.start();
		System.out.println("Audit        Thread5 start......");
		
		while (thread1.isAlive()||thread2.isAlive()||thread3.isAlive()||thread4.isAlive()||thread5.isAlive())
		{
			try
			{
				Thread.sleep(5000);// 主线程挂起5秒
			}
			catch (InterruptedException e)
			{
				return false;
			}
		}
		
		return true;
	}

	/**
	 * 校验转移条件是否满足
	 * 
	 * @param: tLWProcessInstanceSchema 输入的当前描述实例 数据
	 * @param: tInputData 输入的辅助数据
	 * @return boolean:
	 * 
	 */
	private boolean CheckTransitionCondition(
			LFDesbModeSchema tLFDesbModeSchema, TransferData tTransferData)
			throws Exception {
		if (tLFDesbModeSchema == null) {
			// @@错误处理
			buildError("CheckTransitionCondition", "传入的信息为空");

			return false;
		}

		if (tLFDesbModeSchema.getDealType().equals("S")) {
			// S-应用SQL语句进行处理
			String insertSQL = "";
			insertSQL = getInsertSQL(tLFDesbModeSchema, tTransferData);

			MMap map = new MMap();
			map.put(insertSQL, "EXESQL");
			mResult.add(map);

			return true;
		} else if (tLFDesbModeSchema.getDealType().equals("C")) {
			// C -- 应用Class类进行处理
			try {
				Class tClass = Class.forName(tLFDesbModeSchema
						.getInterfaceClassName());
				CalService tCalService = (CalService) tClass.newInstance();

				// 准备数据
				String strOperate = "";
				VData tInputData = new VData();
				tInputData.add(tTransferData);
				tInputData.add(tLFDesbModeSchema);

				if (!tCalService.submitData(tInputData, strOperate)) {
					return false;
				}

				mResult = tCalService.getResult();
			} catch (Exception ex) {
				ex.printStackTrace();

				return false;
			}
		}

		return true;
	}

	private String getInsertSQL(LFDesbModeSchema tLFDesbModeSchema,
			TransferData tTransferData) {
		PubCalculator tPubCalculator = new PubCalculator();

		// 准备计算要素
		Vector tVector = (Vector) tTransferData.getValueNames();
		String tNeedItemKeyName = "NeedItemKey";
		String tNeedItemKeyValue = (String) tTransferData.getValueByName(
				(Object) tNeedItemKeyName).toString();

		// 待修改
		if (tNeedItemKeyName.equals("1")) { // 1-扩充要素
			LFItemRelaDB tLFItemRelaDB = new LFItemRelaDB();
			tLFItemRelaDB.setItemCode(tLFDesbModeSchema.getItemCode());

			if (!tLFItemRelaDB.getInfo()) {
				buildError("getInsertSQL", "查询内外科目编码对应表失败！");

				return "0";
			}

			tPubCalculator.addBasicFactor("UpItemCode", tLFItemRelaDB
					.getUpItemCode());
			tPubCalculator.addBasicFactor("Layer", String.valueOf(tLFItemRelaDB
					.getLayer()));
			tPubCalculator.addBasicFactor("Remark", tLFItemRelaDB.getRemark());
		}

		// 0－普通要素
		for (int i = 0; i < tVector.size(); i++) {
			String tName = (String) tVector.get(i);
			String tValue = (String) tTransferData.getValueByName(
					(Object) tName).toString();
			tPubCalculator.addBasicFactor(tName, tValue);
		}

		System.out.println("准备计算sql！");

		// 准备计算SQL
		if ((tLFDesbModeSchema.getCalSQL1() == null)
				|| (tLFDesbModeSchema.getCalSQL1().length() == 0)) {
			tLFDesbModeSchema.setCalSQL1("");
		}

		if ((tLFDesbModeSchema.getCalSQL2() == null)
				|| (tLFDesbModeSchema.getCalSQL2().length() == 0)) {
			tLFDesbModeSchema.setCalSQL2("");
		}

		if ((tLFDesbModeSchema.getCalSQL3() == null)
				|| (tLFDesbModeSchema.getCalSQL3().length() == 0)) {
			tLFDesbModeSchema.setCalSQL3("");
		}

		String Calsql = tLFDesbModeSchema.getCalSQL()
				+" "+ tLFDesbModeSchema.getCalSQL1()
				+" "+ tLFDesbModeSchema.getCalSQL2()
				+" " +tLFDesbModeSchema.getCalSQL3();
		tPubCalculator.setCalSql(Calsql);

		String strSQL = tPubCalculator.calculateEx();
		System.out.println("从描述表取得的SQL : " + strSQL);

		String insertSQL = "Insert Into ";
		String insertTableName = tLFDesbModeSchema.getDestTableName();
		String stsql = insertSQL + " " + insertTableName + " " + strSQL;
		System.out.println("得到的insert SQL 语句: " + stsql);

		return stsql;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		mNeedItemKey = (String) cInputData.getObjectByObjectName("String", 0);

		if ((mGlobalInput == null) || (mTransferData == null)) {
			buildError("getInputData", "没有得到足够的信息！");

			return false;
		}

		return true;
	}

	/**
	 * 更新销售渠道对应的中介机构代码、业务员代码逻辑； 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealShaleData() {
		mInputData = new VData();
		MMap mMMap = new MMap();
		mUpPol1 = " UPDATE LFCPol_Main set agt_code=null,AGENTNO=null where busi_src_type='1'";
		mUpPol2 = " UPDATE LFCPol_Main set agt_code=null where busi_src_type='2'";
		mUpPol3 = " UPDATE LFCPol_Main set AGENTNO=null where busi_src_type in ('3','4','5')";
		mUpPrem1 = " UPDATE LFCPrem_Info  set agt_code=null,AGENTNO=null,PROC_RATE=NULL,COM_RATE=NULL where busi_src_type='1'";
		mUpPrem2 = " UPDATE LFCPrem_Info  set agt_code=null where busi_src_type='2'";
		mUpPrem3 = " UPDATE LFCPrem_Info  set AGENTNO=null where busi_src_type in ('3','4','5')";

		mMMap.put(mUpPol1, "UPDATE");
		mMMap.put(mUpPol2, "UPDATE");
		mMMap.put(mUpPol3, "UPDATE");
		mMMap.put(mUpPrem1, "UPDATE");
		mMMap.put(mUpPrem2, "UPDATE");
		mMMap.put(mUpPrem3, "UPDATE");
		mInputData.add(mMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			System.out.println(tPubSubmit.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PubSubmit";
			tError.functionName = "submitData";
			tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 更新时间为 1000-01-01的数据，； 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealDate() {
		mInputData = new VData();
		MMap mMMap = new MMap();
		String tUpClmMain = "";
		String tUpClmSet = "";
		String tUpEndo = "";
		tUpEndo = " update lfcendo_fee set gained_date=null where gained_date='1000-01-01'";
		tUpClmMain = " update lfcclaim_main  set end_date=null where end_date='1000-01-01'";
		tUpClmSet = " update lfcclaim_settled set end_date=null where end_date='1000-01-01'";

		mMMap.put(tUpClmMain, "UPDATE");
		mMMap.put(tUpEndo, "UPDATE");
		mMMap.put(tUpClmSet, "UPDATE");

		mInputData.add(mMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			System.out.println(tPubSubmit.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PubSubmit";
			tError.functionName = "submitData";
			tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	/**
	 * 如果为趸缴，则缴费-期间为0； 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealPremDate() {
		mInputData = new VData();
		MMap mMMap = new MMap();
		String tUpEndo = "";
		String tUpClmSet = "";
		String tUpPrem = "";
		tUpPrem = " update lfcprem_info set prem_term=0 where prem_type='1'";
		tUpEndo = " update lfcendo_fee  set prem_term=0 where prem_type='1'";
		tUpClmSet = " update lfcclaim_settled set prem_term=0 where prem_type='1'";

		mMMap.put(tUpEndo, "UPDATE");
		mMMap.put(tUpPrem, "UPDATE");
		mMMap.put(tUpClmSet, "UPDATE");

		mInputData.add(mMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			System.out.println(tPubSubmit.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PubSubmit";
			tError.functionName = "submitData";
			tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	/**
	 * 如果为趸缴，则缴费-期间为0； 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
//	private boolean dealEdorType() {
//		mInputData = new VData();
//		MMap mMMap = new MMap();
//		String tUpEndo = "";
//		tUpEndo = " update lfcendo_fee  set pos_type='01' where pos_type='02' and amt_incured_cnvt=0";
//
//		mMMap.put(tUpEndo, "UPDATE");
//
//		mInputData.add(mMMap);
//		PubSubmit tPubSubmit = new PubSubmit();
//		if (tPubSubmit.submitData(mInputData, "") == false) {
//			System.out.println(tPubSubmit.mErrors.getFirstError());
//			CError tError = new CError();
//			tError.moduleName = "PubSubmit";
//			tError.functionName = "submitData";
//			tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";
//
//			this.mErrors.addOneError(tError);
//			return false;
//		}

//		return true;
//	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */

	public VData getResult() {
		return mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "ReportEngineBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
	}
}
