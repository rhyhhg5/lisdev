package com.sinosoft.audit;

import java.util.Vector;

import com.sinosoft.lis.db.LFItemRelaDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LFDesbModeSchema;
import com.sinosoft.lis.vschema.LFDesbModeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AuditImportThread.java </p>
 * <p>Description:保监会稽核提数多线程并发处理 </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: sinosoft</p>
 * @author Qisl
 * @version 1.0
 * @CreateDate：2009.03.19
 */
public class AuditImportThread extends AuditThread
{
	/**
	 * 类似"Prem_Info||Pol_Main"等用"||"拼接起来的字符串
	 */
	private String mOperateTables = "";
	private String[] mTableArray;
	private TransferData mTransferData = new TransferData();
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private LFDesbModeSet mLFDesbModeSet = new LFDesbModeSet();
	
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	
	/** 传出数据的容器 */
	private VData mResult = new VData();
	
	public AuditImportThread()
    {
    }
	
	public AuditImportThread(String cOperateTables, VData cInputData)
	{
		setOperateTables(cOperateTables);
		setInputData(cInputData);
	}
	
	public void setOperateTables(String tOperateTables)
	{
		mOperateTables = tOperateTables;
	}
	
	public void setInputData(VData tInputData)
	{
		mGlobalInput.setSchema((GlobalInput) tInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mTransferData = (TransferData) tInputData.getObjectByObjectName(
				"TransferData", 0);
		
		mLFDesbModeSet = (LFDesbModeSet) tInputData.getObjectByObjectName(
		          "LFDesbModeSet", 0);
	}
	
	/**
	 * 校验输入数据的合法性
	 * @return
	 */
	private boolean checkInputData()
	{
		if ((mGlobalInput == null) || (mTransferData == null)) {
			buildError("checkInputData", "没有得到足够的信息！");
			return false;
		}
		
		if(mOperateTables == null || mOperateTables.equals(""))
		{
			buildError("checkInputData", "没有得到需要提数的table列表信息！");
			return false;
		}
		
		if(mLFDesbModeSet == null)
		{
			buildError("checkInputData", "没有得到需要提数的table描述信息！");
			return false;
		}
		
		return true;
	}
	
	/**
	 * 获取提数的table列表，放入mTableArray全局字符串数组中
	 * @return
	 */
	private boolean getTableArray()
	{
		try
		{
			mTableArray = PubFun.split(mOperateTables, "||");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			buildError("getTableArray", "出现异常！");
			return false;
		}
		return true;
	}
	
	/**
	 * 线程执行主要方法
	 */
	public void run()
	{
		if (!checkInputData())
		{
			System.out.println("无线程运行所需参数！");
			return;
		}

		if (!getTableArray())
		{
			return;
		}
		mResult.clear();

		// 准备提数的insert语句
		for (int i = 1; i <= mLFDesbModeSet.size(); i++)
		{
			LFDesbModeSchema tLFDesbModeSchema = mLFDesbModeSet.get(i);
			/**
			 * 校验是否该线程执行的提数表
			 */
			if(!checkTableArray(tLFDesbModeSchema))
			{
				continue;
			}
			
			try
			{
				if (!CheckTransitionCondition(tLFDesbModeSchema, mTransferData))
				{
					return;
				}

			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				return;
			}
		}

		if (!pubSubmitData())
		{
			return;
		}
	}
	
	private boolean checkTableArray(LFDesbModeSchema aLFDesbModeSchema)
	{
		String insertTableName = aLFDesbModeSchema.getDestTableName();
		
		if(insertTableName == null || insertTableName.equals(""))
		{
			System.out.println("没有描述目标表信息，出错。");
			return false;
		}
		
		for(int i=0; i<mTableArray.length; i++)
		{
			if(insertTableName.equalsIgnoreCase(mTableArray[i]))
			{
				return true;
			}
		}
		
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 提交数据库
	 * @return
	 */
	private boolean pubSubmitData()
	{
		
//		 提交到BLS进行插入INSERT 处理
		AuditEngineBLS tAuditEngineBLS = new AuditEngineBLS();

		if (!tAuditEngineBLS.submitData(mResult, "")) {
			// @@错误处理
			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
			buildError("submitData", "数据提数失败！");
			mResult.clear();

			return false;
		} else {
			mResult = tAuditEngineBLS.getResult();
			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
		}
		
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * 校验转移条件是否满足
	 * 
	 * @param: tLWProcessInstanceSchema 输入的当前描述实例 数据
	 * @param: tInputData 输入的辅助数据
	 * @return boolean:
	 * 
	 */
	private boolean CheckTransitionCondition(
			LFDesbModeSchema tLFDesbModeSchema, TransferData tTransferData)
			throws Exception {
		if (tLFDesbModeSchema == null) {
			// @@错误处理
			buildError("CheckTransitionCondition", "传入的信息为空");

			return false;
		}

		if (tLFDesbModeSchema.getDealType().equals("S")) {
			// S-应用SQL语句进行处理
			String insertSQL = "";
			insertSQL = getInsertSQL(tLFDesbModeSchema, tTransferData);

			
			
			//Added By Liuyp At 2009.7.15 如果是prem_info表要逐条提交数据库
			if (tLFDesbModeSchema.getItemType().equals("A02") || tLFDesbModeSchema.getItemType().equals("A03")) {				
				if (!dealPremAndEndoInfo(insertSQL)) {
					return false;
				}
				
			}else{
				MMap map = new MMap();
				map.put(insertSQL, "EXESQL");
				//mResult.clear();
				mResult.add(map);
			}
			
			return true;
		} else if (tLFDesbModeSchema.getDealType().equals("C")) {
			// C -- 应用Class类进行处理
			try {
				Class tClass = Class.forName(tLFDesbModeSchema
						.getInterfaceClassName());
				CalService tCalService = (CalService) tClass.newInstance();

				// 准备数据
				String strOperate = "";
				VData tInputData = new VData();
				tInputData.add(tTransferData);
				tInputData.add(tLFDesbModeSchema);

				if (!tCalService.submitData(tInputData, strOperate)) {
					return false;
				}

				mResult = tCalService.getResult();
			} catch (Exception ex) {
				ex.printStackTrace();

				return false;
			}
		}

		return true;
	}
	
	/**
	 * 处理prem_info和endo_fee表时要逐条insert语句提交数据库。
	 * @param tLFDesbModeSchema
	 * @param tTransferData
	 * @return
	 */
	
	private boolean dealPremAndEndoInfo(String tInsertSql) {
		System.out.print("dealPremAndEndoInfo");
		MMap tMap = new MMap();
		VData tResult = new VData();
		tMap.put(tInsertSql, "EXESQL");
		tResult.clear();
		tResult.add(tMap);
		
//		 提交到BLS进行插入INSERT 处理
		AuditEngineBLS tAuditEngineBLS = new AuditEngineBLS();

		if (!tAuditEngineBLS.submitData(tResult, "")) {
			// @@错误处理
			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
			buildError("submitData", "数据提数失败！");
			tResult.clear();

			System.out.print("dealPremAndEndoInfo");
			ExeSQL tExeSQL = new ExeSQL();
			String[] arrDeleteTable = new String[2];
			if (tInsertSql.toUpperCase().indexOf("PREM_INFO") >= 0) {
				arrDeleteTable[0] = "ALTER TABLE prem_info ACTIVATE NOT LOGGED INITIALLY WITH EMPTY TABLE";
			}
			
			if (tInsertSql.toUpperCase().indexOf("ENDO_FEE") >= 0) {
				arrDeleteTable[1] = "ALTER TABLE Endo_Fee ACTIVATE NOT LOGGED INITIALLY WITH EMPTY TABLE";
			}
			
			try{
			    for (int i = 0; i < arrDeleteTable.length; i++){   	
			    	String tSql = arrDeleteTable[i];
			    	if (!tExeSQL.execUpdateSQL(tSql))
						{
							int n = tExeSQL.mErrors.getErrorCount();
							for (int j = 0; j < n; j++){
								System.out.println("Error: " + tExeSQL.mErrors.getError(j).errorMessage);
							}
							break;
						}
			    }
			    }catch (Exception ex)
				{
			    	ex.printStackTrace();
					return false;
				}
			    
//			VData tInputData = new VData();
//			MMap mMMap = new MMap();
//			String tDePrem = "";
//			tDePrem = " delete from prem_info";
//
//			mMMap.put(tDePrem, "DELETE");
//
//			tInputData.add(mMMap);
//			PubSubmit tPubSubmit = new PubSubmit();
//			if (tPubSubmit.submitData(tInputData, "") == false) {
//				System.out.println(tPubSubmit.mErrors.getFirstError());
//				CError tError = new CError();
//				tError.moduleName = "PubSubmit";
//				tError.functionName = "submitData";
//				tError.errorMessage = "删除prem_info的sql语句数据处理时 PubSubmit 失败!";
//
//				this.mErrors.addOneError(tError);
//				return false;
//			}
//			return false;
		} else {
			tResult = tAuditEngineBLS.getResult();
			this.mErrors.copyAllErrors(tAuditEngineBLS.mErrors);
				
		}

		return true;
	}
	
	/**
	 * 获取提数insert语句。
	 * @param tLFDesbModeSchema
	 * @param tTransferData
	 * @return
	 */
	private String getInsertSQL(LFDesbModeSchema tLFDesbModeSchema,
			TransferData tTransferData) {
		PubCalculator tPubCalculator = new PubCalculator();

		// 准备计算要素
		Vector tVector = (Vector) tTransferData.getValueNames();
		String tNeedItemKeyName = "NeedItemKey";
		String tNeedItemKeyValue = (String) tTransferData.getValueByName(
				(Object) tNeedItemKeyName).toString();

		// 待修改
		if (tNeedItemKeyName.equals("1")) { // 1-扩充要素
			LFItemRelaDB tLFItemRelaDB = new LFItemRelaDB();
			tLFItemRelaDB.setItemCode(tLFDesbModeSchema.getItemCode());

			if (!tLFItemRelaDB.getInfo()) {
				buildError("getInsertSQL", "查询内外科目编码对应表失败！");

				return "0";
			}

			tPubCalculator.addBasicFactor("UpItemCode", tLFItemRelaDB
					.getUpItemCode());
			tPubCalculator.addBasicFactor("Layer", String.valueOf(tLFItemRelaDB
					.getLayer()));
			tPubCalculator.addBasicFactor("Remark", tLFItemRelaDB.getRemark());
		}

		// 0－普通要素
		for (int i = 0; i < tVector.size(); i++) {
			String tName = (String) tVector.get(i);
			String tValue = (String) tTransferData.getValueByName(
					(Object) tName).toString();
			tPubCalculator.addBasicFactor(tName, tValue);
		}

		System.out.println("准备计算sql！");

		// 准备计算SQL
		if ((tLFDesbModeSchema.getCalSQL1() == null)
				|| (tLFDesbModeSchema.getCalSQL1().length() == 0)) {
			tLFDesbModeSchema.setCalSQL1("");
		}

		if ((tLFDesbModeSchema.getCalSQL2() == null)
				|| (tLFDesbModeSchema.getCalSQL2().length() == 0)) {
			tLFDesbModeSchema.setCalSQL2("");
		}

		if ((tLFDesbModeSchema.getCalSQL3() == null)
				|| (tLFDesbModeSchema.getCalSQL3().length() == 0)) {
			tLFDesbModeSchema.setCalSQL3("");
		}

		String Calsql = tLFDesbModeSchema.getCalSQL()
				+" "+ tLFDesbModeSchema.getCalSQL1()
				+" "+ tLFDesbModeSchema.getCalSQL2()
				+" " +tLFDesbModeSchema.getCalSQL3();
		tPubCalculator.setCalSql(Calsql);

		String strSQL = tPubCalculator.calculateEx();
		System.out.println("从描述表取得的SQL : " + strSQL);

		String insertSQL = "Insert Into ";
		String insertTableName = tLFDesbModeSchema.getDestTableName();
		String stsql = insertSQL + " " + insertTableName + " " + strSQL;
		System.out.println("得到的insert SQL 语句: " + stsql);

		return stsql;
	}
	
	/**
	 * 构建错误信息，返回调用类
	 * @param szFunc
	 * @param szErrMsg
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "AuditImportThread";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
}
