package com.sinosoft.audit;

import java.util.Vector;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: PICCH业务系统</p>
 * <p>Description: 保监会稽核数据质量检查BL</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: Sinosoft</p>
 * @author Qisl
 * @version 1.0
 * @since 2009-06-18
 */

public class AuditDataCheckBL
{
	private TransferData mTransferData = new TransferData();
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	
	/**
	 * 数据校验
	 * @return
	 */
	private boolean check()
	{
		SSRS mSSRS = new SSRS();
		SSRS rSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();

		String tCalCode = "";
		String tCalSql = "";
		String str = "select * from LFCAuditCheck  where ValiFlag = '1' order by calcode";
		mSSRS = tExeSQL.execSQL(str);
		int trowsize = mSSRS.getMaxRow();
		if (trowsize < 1)
		{
			buildError("check", "LFCAuditCheck表中没有需要执行的检查语句！");
			System.out.println("LFCAuditCheck表中没有需要执行的检查语句！");
			return false;
		}
		tCalCode = "";
		tCalSql = "";
		for (int i = 1; i <= trowsize; i++)
		{
			rSSRS = new SSRS();
			tExeSQL = new ExeSQL();
			try
			{
				tCalCode = mSSRS.GetText(i, 1);
				tCalSql = mSSRS.GetText(i, 6);
				tCalSql = StrTool.replaceEx(tCalSql, "’", "'"); //替换字符

		        if(tCalSql.toUpperCase().indexOf("WITH ")>0)
		        {
		        	buildError("check", "sql语句中不能出现with ur。");
		        	return false;
		        }
				
				//添加计算要素，获取校验sql。
				String tCheckSql = getCheckSQL(tCalSql, mTransferData);
				
				int m = 0;
				if (tCalCode.compareTo("calc") > 0)
				{
					countdata(tCalCode, tCheckSql); //计算数据记录条数的语句
				}
				else
				{
					if(tCheckSql.toLowerCase().indexOf("count(") == -1)
					{
						rSSRS = tExeSQL.execSQL("select count(*) from (" + tCheckSql + ") gq with ur"); 
					}else{
						tCheckSql = addWithUr(tCheckSql);
						rSSRS = tExeSQL.execSQL(tCheckSql);
					}
					if (tExeSQL.mErrors.getErrorCount() == 0)
					{
						if (rSSRS != null)
						{
							m = Integer.parseInt(rSSRS.GetText(1, 1));
							System.out.println("效验的sql数量" + m);
						}
						else{
							m = 0;
						}
						
						str = "update LFCAuditCheck set ErrCount='" + m + "' , ValiFlag = '0' ," + " MakeDate='"
								+ PubFun.getCurrentDate() + "',MakeTime='" + PubFun.getCurrentTime()
								+ "' WHERE CalCode='" + tCalCode + "'";
						if (!tExeSQL.execUpdateSQL(str))
						{
							buildError("check", "更新数据失败！");
							System.out.println("更新数据失败");
							return false;
						}
					}
					else
					{
						str = "update LFCAuditCheck set ErrCount='SQL中的表或字段不存在' , ValiFlag = '0' ," + " MakeDate='"
								+ PubFun.getCurrentDate() + "',MakeTime='" + PubFun.getCurrentTime()
								+ "' WHERE CalCode='" + tCalCode + "'";
						if (!tExeSQL.execUpdateSQL(str))
						{
							buildError("check", "更新数据失败！");
							System.out.println("更新数据失败err");
							return false;
						}
					}
				}
			}
			catch (Exception ex)
			{
				System.err.print(ex);
				return false;
			}
		}
		return true;
	}

	private void countdata(String TcCode, String CalSql)
	{
		String DataCount = "";
		ExeSQL tSQL1 = new ExeSQL();
		DataCount = tSQL1.getOneValue(CalSql);
		if (tSQL1.mErrors.getErrorCount() != 0)
			DataCount = "无表";
		else if (DataCount.equals("") || DataCount == null)
			DataCount = "空值";
		else if (DataCount.length() > 30)
			DataCount = DataCount.substring(30);
		System.out.println("效验的数量" + DataCount);

		String strsql = "update LFCAuditCheck set ErrCount='" + DataCount + "' , ValiFlag = '0' ," + " MakeDate='"
				+ PubFun.getCurrentDate() + "',MakeTime='" + PubFun.getCurrentTime() + "' WHERE CalCode='" + TcCode
				+ "'";
		if (!tSQL1.execUpdateSQL(strsql))
		{
			System.out.println(TcCode + "表更新数据失败");
		}

	}
	
	/**
     * addWithUr
     *add by jixin
     * @param sql String
     * @return String
     */
    private String addWithUr(String sql){
    	String upSql = sql.toUpperCase();
    	int i = upSql.indexOf("WITH ");
        if(i>0)
        {
           return sql;

        }
        else
        {
            sql += " with ur ";

        }
        return sql;
    	}
    

	/**
	 * 获取提数insert语句。
	 * @param tLFDesbModeSchema
	 * @param tTransferData
	 * @return
	 */
	private String getCheckSQL(String tCalSql,TransferData tTransferData) {
		PubCalculator tPubCalculator = new PubCalculator();
		// 准备计算要素
		Vector tVector = (Vector) tTransferData.getValueNames();

		//普通要素
		for (int i = 0; i < tVector.size(); i++) {
			String tName = (String) tVector.get(i);
			String tValue = (String) tTransferData.getValueByName(
					(Object) tName).toString();
			tPubCalculator.addBasicFactor(tName, tValue);
		}

		System.out.println("准备校验sql！");
		tPubCalculator.setCalSql(tCalSql);
		String strSQL = tPubCalculator.calculateEx();

		return strSQL;
	}
	
	private boolean getInputData(VData tInputData)
	{
		mTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);
		if(mTransferData == null)
		{
			buildError("getInputData", "传入参数不正确。");
			return false;
		}
		return true;
	}
	
	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if(!ReCheck())
		{
			return false;
		}
		
		// 处理提数逻辑
		if (!check()) {
			return false;
		}

		return true;
	}
	
	public boolean ReCheck()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String str = "update LFCAuditCheck set ErrCount='0' , ValiFlag = '1' ," + " MakeDate='"
				+ PubFun.getCurrentDate() + "',MakeTime='" + PubFun.getCurrentTime()+"'";
		if (!tExeSQL.execUpdateSQL(str))
		{
			buildError("check", "更新数据失败！");
			return false;
		}
		return true;
	}
	
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "AuditDataCheckBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	public static void main(String args[])
	{
		System.out.println("start");
		AuditDataCheckBL tAuditDataCheckBL = new AuditDataCheckBL();
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		
		tTransferData.setNameAndValue("ManageCom", "8611");
		tTransferData.setNameAndValue("StartDate", "2008-01-01");
		tTransferData.setNameAndValue("EndDate", "2008-06-30");
		
		tVData.add(tTransferData);
		tAuditDataCheckBL.ReCheck();
		tAuditDataCheckBL.submitData(tVData,"");
		System.out.println("end");
	}
}
