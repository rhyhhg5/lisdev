/**
 * Copyright (c) 2005 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.audit;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import java.io.File;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Transformer;
import java.io.PrintStream;
import java.io.BufferedOutputStream;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class PolChkTxtCreateUI {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
//    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;
//    private String mOperate;
    /** 业务数据操作字符串 */
    private String mStatDate;
    private String mEndDate;
    private String mOperate;

    //当前生成的XML 用到的表
    private String mTableName;

    //当前生成的XML 的路径
    private String mFilePath;

    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

//    private Reflections mReflections = new Reflections();
    private SSRS mSSRS;
    private VData mFieldData;

    //转换文件格式

    private String mfileName = "";
    private static final String mPre = "lfc";
    /**生成xml 用到的表名*/
    private static final String mPolMain = mPre + "pol_main";
    private static final String mEndoFee = mPre + "endo_fee";
    private static final String mPremInfo = mPre + "prem_info";
    private static final String mClaimMain = mPre + "claim_main";
    private static final String mClaimSettled = mPre + "claim_settled";
    private static final String mPayDue = mPre + "pay_due";
    private static final String mAgentInfo = mPre + "agent_info";
    private static final String mAgtCode = mPre + "agt_code";
    private static final String mPlanInfo = mPre + "plan_info";
    //王珑加：2006-06-01
    private PrintStream m_ps = null;
    private String m_strEncoding = null;
    private String m_strFileName = null;


    public PolChkTxtCreateUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @return boolean
     */
    public boolean submitData(VData cInputData) {
        System.out.println(" start  submit");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验是否符合要求
        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("------deal data over---:" + PubFun.getCurrentTime());

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("end  Submit");

        //mResult.clear();
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
//        mInputData = cInputData;
        if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if (mOperater == null || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得业务数据
        if (mTransferData == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mStatDate = (String) mTransferData.getValueByName("StatDate");
        if (mStatDate == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中StatYear失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mEndDate = (String) mTransferData.getValueByName("EndDate");
        if (mEndDate == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中StatMon失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mOperate = (String) mTransferData.getValueByName("Operate");
        if (mOperate == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中mOperate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验业务数据
     * @return boolean
     */
    private static boolean checkData() {

        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean prepareOutputData() {
        mResult.clear();
        MMap map = new MMap();

        //添加核保通知书打印管理表数据

        mResult.add(map);
        return true;
    }

    /**
     * 根据前面的输入数据，生成xml文件
     * 如果在生成XML文件过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean makeFile() {
        try {
            //生成XML文件
            //文件名称
            String strFileName = mTableName.substring(3, mTableName.length());
            mfileName = mFilePath + strFileName + ".txt";
            if ("1".equals(mOperate)) {
                createTxtDocument(mfileName, null);
                for (int j = 1; j <= mFieldData.size(); j++) {
                    this.m_ps.print((String) mFieldData.getObject(j - 1));
                    this.m_ps.print('|');
                }
                this.m_ps.print("\n");
                for (int i = 1; i <= mSSRS.getMaxRow(); i++) {

                    for (int j = 1; j <= mSSRS.getMaxCol(); j++) {

                        if (mSSRS.GetText(i, j) == null ||
                            "null".equals(mSSRS.GetText(i, j)) ||
                            "".equals(mSSRS.GetText(i, j))) {

                            this.m_ps.print("");
                        } else {
                            this.m_ps.print(mSSRS.GetText(i, j));
                        }
                        this.m_ps.print('|');
                    }
                    this.m_ps.print("\n");
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "makeFile";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，生成xml文件
     * 如果在生成XML文件过程中出错，则返回false,否则返回true
     * @return boolean
     */
    /*  private boolean makeFile() {
          try {
              //生成XML文件
              //文件名称
     String strFileName = mTableName.substring(3, mTableName.length());
              mfileName = mFilePath + strFileName + ".xml";
              if ("1".equals(mOperate)) {
                  PrintWriter out = new PrintWriter(new FileOutputStream(
                          mfileName));
                  out.println("<?xml version=\"1.0\" encoding=\"gb18030\" ?>");
                  out.flush();
                  out.println("<POLDATA>");
                  out.flush();

                  out.println("  <ROW>");
                  out.flush();
                  for (int j = 1; j <= mFieldData.size(); j++) {
                      out.print("    <" +
                                (String) mFieldData.getObject(j - 1) +
                                ">");
                      out.print((String) mFieldData.getObject(j - 1));
                      out.println("</" +
                                  (String) mFieldData.getObject(j - 1) +
                                  ">");
                  }
                  out.println("  </ROW>");
                  out.println("");
                  out.flush();

                  for (int i = 1; i <= mSSRS.getMaxRow(); i++) {

                      out.println("  <ROW>");
                      out.flush();
                      for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
     out.print("    <" + (String) mFieldData.getObject(j - 1) +
                                    ">");
                          if (mSSRS.GetText(i, j) == null ||
                              "null".equals(mSSRS.GetText(i, j)) ||
                              "".equals(mSSRS.GetText(i, j))) {

                              out.print("");
                          } else {
                              out.print(mSSRS.GetText(i, j));
                          }
     out.println("</" + (String) mFieldData.getObject(j - 1) +
                                      ">");
                      }
                      out.println("  </ROW>");
                      out.println("");
                      out.flush();
                  }
                  out.println("</POLDATA>");
                  out.flush();
                  out.close();
              }
          } catch (Exception ex) {
              ex.printStackTrace();
              CError tError = new CError();
              tError.moduleName = "MakeXMLBL";
              tError.functionName = "makeFile";
              tError.errorMessage = ex.toString();
              this.mErrors.addOneError(tError);
              return false;
          }
          return true;
      }*/


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData() {
        //取得文件生成位置的路径
        if (!getFilePath()) {
            return false;
        }

        //Pol_Main数据处理
        if (!dealPolMain()) {
            return false;
        }

        //Endo_Fee数据处理
        if (!dealEndoFee()) {
            return false;
        }

        //Prem_Info数据处理
        if (!dealPremInfo()) {
            return false;
        }

        //Claim_Main数据处理
        if (!dealClaimMain()) {
            return false;
        }

        //Claim_Settled数据处理
        if (!dealClaimSettled()) {
            return false;
        }

        //Pay_Due数据处理
        if (!dealPayDue()) {
            return false;
        }

        //Agent_Info数据处理
        if (!dealAgentInfo()) {
            return false;
        }

        //Agt_Code数据处理
        if (!dealAgtCode()) {
            return false;
        }

        //Plan_Info数据处理
        if (!dealPlanInfo()) {
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行Pol_Main处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealPolMain() {
        // 生成 pol_main 文件
        String tSQL = "select * from " + mPolMain + " a where a.Eff_date >='" +
                      this.mStatDate
                      + "' and a.Eff_date <='" + this.mEndDate + "'"
                      + " order by a.PolNo";

        mTableName = mPolMain;

        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Endo_Fee处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealEndoFee() {

        // 生成 endo_fee 文件
        String tSQL = "select * from " + mEndoFee + " a where (a.Proc_date >='" +
                      this.mStatDate
                      + "' and a.Proc_date <='" + this.mEndDate + "')"
                      + " or (a.gained_date >='"
                      + this.mStatDate
                      + "' and a.gained_date<='" + this.mEndDate +
                      "' and a.Proc_date < '"
                      + this.mStatDate
                      + "')"
                      + " order by a.PolNo";

        mTableName = mEndoFee;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Prem_Info处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealPremInfo() {

        // 生成 prem_info 文件
        String tSQL = "select * from " + mPremInfo +
                      " a where a.Gained_date >='" + this.mStatDate
                      + "' and a.Gained_date  <='" + this.mEndDate + "'"
                      + " order by a.PolNo";

        mTableName = mPremInfo;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());
        return true;
    }

    /**
     * 根据前面的输入数据，进行Claim_Main处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealClaimMain() {

        // 生成 claim_main文件
        String tSQL = "select distinct a.* from " + mClaimMain + " a ," +
                      mClaimSettled + " b"
                      +
                      " where a.CaseNo =b.CaseNo and a.GP_Type=b.GP_Type and a.polNo=b.polNo"
                      + " and ((a.Docu_date >='" + this.mStatDate
                      + "' and  a.Docu_date <='" + this.mEndDate + "' ) or "
                      + " (  b.Gained_date >='" + this.mStatDate
                      + "' and  b.Gained_date <='" + this.mEndDate + "') or "
                      + " (  a.Docu_date <'" + this.mStatDate
                      + "' and  b.Gained_date >'" + this.mEndDate + "') )"
                      ;

        mTableName = mClaimMain;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Claim_Settled处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealClaimSettled() {

        // 生成 claim_settled 文件
        String tSQL = "select distinct b.* from " + mClaimMain + " a ," +
                      mClaimSettled + " b"
                      + " where a.CaseNo =b.CaseNo and a.GP_Type=b.GP_Type and a.polNo=b.polNo and ((a.Docu_date >='" +
                      this.mStatDate
                      + "' and  a.Docu_date <='" + this.mEndDate + "' ) or "
                      + " (  b.Gained_date >='" + this.mStatDate
                      + "' and  b.Gained_date <='" + this.mEndDate + "') or "
                      + " (  a.Docu_date <'" + this.mStatDate
                      + "' and  b.Gained_date >'" + this.mEndDate + "') )"
                      ;

        mTableName = mClaimSettled;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Pay_Due处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealPayDue() {

        // 生成 pay_due 文件
        String tSQL = "select * from " + mPayDue;
        mTableName = mPayDue;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Agent_Info处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealAgentInfo() {

        // 生成 agent_info 文件
        String tSQL = "select * from " + mAgentInfo
                      + " order by Agentno";
        mTableName = mAgentInfo;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Agt_Code处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealAgtCode() {

        // 生成 agt_code 文件
        String tSQL = "select * from " + mAgtCode
                      + " order by Agt_Code";
        mTableName = mAgtCode;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行Plan_Info处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealPlanInfo() {

        // 生成 plan_code 文件
        String tSQL = "select * from " + mPlanInfo
                      + " order by plan_code";
        mTableName = mPlanInfo;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" +
                           PubFun.getCurrentTime());

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean creatFile(String tSQL) {

        //取得所有要生成文件的数据
        //mOperate=1 只生成XML文件
        if ("1".equals(mOperate)) {
            ExeSQL tExeSQL = new ExeSQL();
            mSSRS = tExeSQL.execSQL(tSQL);
            if (tExeSQL.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "MakeXMLBL";
                tError.functionName = "creatFile";
                tError.errorMessage = "查询XML数据" + mTableName + "出错！";
                this.mErrors.addOneError(tError);
                return false;
            }

            //取得所有要生成文件的表的字段名
            PolChkExecSql tPolChkExecSql = new PolChkExecSql();
            mFieldData = tPolChkExecSql.execSQL(tSQL);
            if (tPolChkExecSql.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "MakeXMLBL";
                tError.functionName = "creatFile";
                tError.errorMessage = "查询XML字段名" + mTableName + "出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //生成XML文件
        if (!makeFile()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败MakeXMLBL-->makeFile!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 取得文件生成路径
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean getFilePath() {
        try {

            //取得文件生成位置的路径
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("ReportXmlPath");
            if (!tLDSysVarDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "audit";
                tError.functionName = "getFilePath";
                tError.errorMessage = "生成路径出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //生成XML文件路径
            mFilePath = tLDSysVarDB.getSysVarValue();
            if (mFilePath.equals("")) {
                return false;
            }
            String tCurrenDate = PubFun.getCurrentDate();
            String tCurrenTime = PubFun.getCurrentTime();
            String arrCurrenDate[] = tCurrenDate.split("-");
            String arrCurrenTime[] = tCurrenTime.split(":");
            String tFilePathSub = arrCurrenDate[0] + arrCurrenDate[1] +
                                  arrCurrenDate[2] + arrCurrenTime[0];
            String tFilePath = mFilePath + tFilePathSub;
            /*if (makeDirectory(tFilePath)) {
                CError tError = new CError();
                tError.moduleName = "MakeXMLBL";
                tError.functionName = "makeDirectory";
                tError.errorMessage = "生成路径出错！";
                this.mErrors.addOneError(tError);
                return false;
                     }
                     mFilePath = mFilePath + tFilePath + "/";*/
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "audit";
            tError.functionName = "getFilePath";
            tError.errorMessage = "获取路径失败！";
            this.mErrors.addOneError(e.getMessage());
            return false;
        }

    }

    /**
     * 创建指定的目录。
     * @param fileName 要创建的目录的目录名
     * @return 完全创建成功时返回true，否则返回false。
     */

    public static boolean makeDirectory(String fileName) {

        File file = new File(fileName);
        return file.mkdir();
    }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return boolean
     */
    private static boolean prepareTransferData() {
        //为工作流中回收体检通知书节点准备属性数据
        return true;
    }


    public VData getResult() {
        mResult = new VData(); //不保证事务一致性
        return mResult;
    }

    public TransferData getReturnTransferData() {
        return mTransferData;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * Simple sample code to show how to run the XSL processor
     * from the API.
     */
    public boolean xmlTransform() {
        // Have the XSLTProcessorFactory obtain a interface to a
        // new XSLTProcessor object.
        String strFileName = "";
        String strfilePath = "";
        try {
            if (mfileName != "") {
                strfilePath = mfileName.substring(0, mfileName.lastIndexOf("/"));
            }
            System.out.println("txtPath:" + strfilePath);
            strFileName = mTableName.substring(3, mTableName.length());
            String xslPath = strfilePath + "/" + "xsl/" + strFileName + ".xsl";
            System.out.println("xslPath:" + xslPath);

            File fSource = new File(mfileName);
            File fStyle = new File(xslPath);
            System.out.println("fStyle");

            Source source = new StreamSource(fSource);
            Result result = new StreamResult(new FileOutputStream(mfileName.
                    substring(0, mfileName.lastIndexOf(".")) + ".txt"));
            Source style = new StreamSource(fStyle);

            //Create the Transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer(style);
            System.out.println(mTableName + PubFun.getCurrentTime());
            try {
                //Transform the Document
                transformer.transform(source, result);
            } catch (Exception e) {
                e.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "WriteToFileBLS";
                tError.functionName = "SimpleTransform";
                this.mErrors.addOneError(e.getMessage());
                return false;
            }
            System.out.println("Transform Success!");
        } catch (Exception e) {
            e.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "WriteToFileBLS";
            tError.functionName = "SimpleTransform";
            tError.errorMessage = "Xml处理失败!!";
            this.mErrors.addOneError(e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();

        /** 全局变量 */
        mGlobalInput.Operator = "circ";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
        /** 传递变量 */
        //生成XML 文件
        mTransferData.setNameAndValue("StatDate", "2005-01-01");
        mTransferData.setNameAndValue("EndDate", "2005-10-20");
        mTransferData.setNameAndValue("Operate", "1");

        /**总变量*/
        tVData.add(mGlobalInput);
        tVData.add(mTransferData);

        PolChkTxtCreateUI tPolChkTxtCreateUI = new PolChkTxtCreateUI();
        try {
            if (tPolChkTxtCreateUI.submitData(tVData)) {
//                VData tResult = new VData();
                //tResult = tActivityOperator.getResult() ;
            } else {
                System.out.println(tPolChkTxtCreateUI.mErrors.getError(0).
                                   errorMessage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createTxtDocument(String strFileName, String strEncoding) {

        if (strEncoding != null && !strEncoding.equals("")) {
            this.m_strEncoding = strEncoding;
        } else {
            this.m_strEncoding = "GBK";
        }

        try {
            this.m_ps = new PrintStream(new BufferedOutputStream(new
                    FileOutputStream(
                            strFileName)), true, this.m_strEncoding);

            //this.m_strFileName = strFileName;

        } catch (Exception ex) {
            ex.printStackTrace();
            this.m_ps = null;
        }
    }

    /**
     * 将列表中的数据写入到TXT文件中，文件的格式为以"|"做为分隔符的文本文件。
     * @param listtable ListTable
     * @param colvalue String[]
     * @return Document
     */
    public void outArray(String[] colvalues) {
        int col = colvalues.length;

        for (int i = 0; i <= col - 1; i++) {
            try {
                m_ps.print(colvalues[i]);
                if (i == col - 1) {
                    m_ps.print("\n");
                } else {
                    m_ps.print('|');
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


}
