package com.sinosoft.audit;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;

public class AuditTransfBLS {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	private boolean getInputData() {

		return true;
	}

	// 该方法实现上传txt文件到指定目录

    public boolean sendFile(String cFilePath)
    {
        //登入前置机
        String ip = "10.252.8.11";
        String user = "jihe";
        String pwd = "jihe123";
        String port = "21";
//        String basePath = "D:/JI-HE/";
        String basePath = ".";
        FTPTool tFTPTool = new FTPTool(ip, user, pwd, Integer.parseInt(port));
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            }
            else
            {
                if (!tFTPTool.upload(basePath,cFilePath))
                {
                    System.out.println("上载文件失败!");
                    CError tError = new CError();
                    tError.moduleName = "AuditTransfBLS";
                    tError.functionName = "sendFile";
                    tError.errorMessage = tFTPTool
                            .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);

                    return false;
                }
  //              deleteFile(cFilePath);
                
                tFTPTool.logoutFTP();
            }
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是网络异常。");
            CError tError = new CError();
            tError.moduleName = "AuditTransfBLS";
            tError.functionName = "sendFile";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是无法写入文件");
            CError tError = new CError();
            tError.moduleName = "AuditTransfBLS";
            tError.functionName = "sendFile";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    
    /**
     * 删除生成的文件
     * @param cFilePath String：完整文件名
     */
    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            CError tError = new CError();
            tError.moduleName = "AuditTransfBLS";
            tError.functionName = "deleteFile";
            tError.errorMessage = "没有找到需要删除的文件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        f.delete();

        return true;
    }
}