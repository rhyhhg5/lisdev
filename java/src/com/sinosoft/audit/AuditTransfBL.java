package com.sinosoft.audit;

import java.io.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import org.apache.commons.fileupload.*;
import com.sinosoft.utility.*;

import java.util.*;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:报表一级数据转换处理接口类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author:sq
 * @version 1.0
 */
public class AuditTransfBL {

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 业务处理相关变量 */
	private TransferData mTransferData = new TransferData();

	/** 本地路径 */
	private String mStrRealPath;
	
	public AuditTransfBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 处理提数逻辑
		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		
		String cFilePath = mStrRealPath + "audit/Audit.txt";
		System.out.println(cFilePath);
		AuditTransfBLS tAuditTransfBLS = new AuditTransfBLS();
		if (!tAuditTransfBLS.sendFile(cFilePath)) {
	    //@@错误处理
		CError tError = new CError();
		tError.moduleName = "AuditTransfBL";
		tError.functionName = "getInputData";
		tError.errorMessage = "稽核数据导入失败!";
		this.mErrors.addOneError(tError);
		return false;
		}
		
		return true;
		
	}
	

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {

//
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		
		// 获得业务数据
		if (mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "AuditTransfBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输业务数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		mStrRealPath = (String) mTransferData.getValueByName("strRealPath").toString();
		
		if (mStrRealPath == null) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "AuditTransfBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输业务数据中StatYear失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "AuditTransfBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
	}
}
