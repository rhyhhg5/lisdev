package com.sinosoft.audit;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.f1j.ss.BookModelImpl;
import com.f1j.util.F1Exception;
import com.sinosoft.lis.pubfun.diskimport.MultiDiskImporter;
import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.lis.pubfun.diskimport.Sheet;
import com.sinosoft.lis.pubfun.diskimport.XlsImporter;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.StrTool;
/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author shenq
 * @version 1.0
 */
public class AuditSheetImport extends MultiDiskImporter {
    /** 一个book对应一个excel文件 */
    private BookModelImpl book = null;
    /** Sheet的名字 */
    private String[] mSheetNames;
    /** 对应Sheet所导入的表名 */
    private String[] mTableNames;
    /** xls文件名*/
    private String fileName = null;
    /** xml文件名*/
    private String configFileName = null;

    /** 对应Set */
    private HashMap mSheetAndSet = new HashMap();
    /** 对应Scheam */
    private HashMap mSheetAndSchema = new HashMap();
    /** 最终数据 */
    private HashMap mData = new HashMap();
    
    private int mStartRows;
	 /**
     * 导入多Sheet
     * @param fileName String
     * @param configFileName String
     * @param sheetNames String[]
     */
    public AuditSheetImport(String fileName, String configFileName,
                              String[] sheetNames) {
        super(fileName, configFileName, sheetNames);
        mSheetNames = sheetNames;
        this.fileName = fileName;
        this.configFileName = configFileName;
    }


    /**
     * 执行磁盘导入
     * @return boolean
     */
    public boolean doImport() {
        XlsImporter importer = new XlsImporter(fileName, configFileName,
        		mSheetNames);
        importer.doImport();
        Sheet[] sheets = importer.getSheets();
        if (!dealSheets(sheets)) {
            return false;
        }
        deleteFile();//删除导入的文件
        return true;
    }
    /**
     * 删除磁盘导入临时文件
     * @return boolean
     */
    private boolean deleteFile() {
        File file = new File(fileName);
        return file.delete();
    }
    /**
     * 处理所有导入的sheet
     * @param sheets Sheet[]
     * @return boolean
     */
    protected boolean dealSheets(Sheet[] sheets)
    {
        for (int i = 0; i < sheets.length; i++)
        {
            if (!dealOneSheet(sheets[i],i))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 处理多个sheet中的一个
     * @param sheet Sheet
     * @return boolean
     */
     protected boolean dealOneSheet(Sheet sheet,int numberOfSheet){
    	 if (!initSchemaSet()) {
             return false;
         }
         String sheetName = sheet.getName();//获取名称
      //   int size=2;
         //获取sheet行数
         int size=sheet.getRowSizeOfVersionSheet(numberOfSheet);
         System.out.println("第"+numberOfSheet+"个sheet的行数"+size);
         SchemaSet schemaSet = null;
         try {
             schemaSet = (SchemaSet) Class.forName((String) mSheetAndSet.get(
            		 mSheetNames[numberOfSheet])).newInstance();
         } catch (Exception ex) {
             String str = "生成Set失败!";
             buildError("dealOneSheet", str);
             System.out.println(
                     "在程序AuditSheetImport.dealOneSheet() : " +
                     sheet.getName() + str);
             return false;
         }
         
         for (int i = 0; i <= size; i++) {//行数循环
             Schema schema = createSchema(sheet, i,numberOfSheet);
             if (schema == null) {
                 return false;
             }
             schemaSet.add(schema);
         }
         mData.put(mSheetNames[numberOfSheet], schemaSet);
    	 return true;
     }
     /**
      * createSchema
      *
      *
      * @param sheet Sheet
      * @param i int 行数
      * @return Schema
      */
     private Schema createSchema(Sheet sheet, int row, int numberOfSheet) {
         try {
             String tSheetName = sheet.getName();//sheet名称  
             //表名称
                 String tSchemaClassName = (String) mSheetAndSchema.get(
                		 mSheetNames[numberOfSheet]);
                 System.out.println("*************"+tSchemaClassName);
                 Class schemaClass = Class.forName(tSchemaClassName);
                 Schema schema = (Schema) schemaClass.newInstance();
                 int col = sheet.getColSize();//总列数
                 for (int i = 0; i < col; i++) {
                     String head = sheet.getHead(i);
                     if ((head == null) || (head.equals(""))) {
                         continue;
                     }
                     String value = sheet.getText(row, i, mStartRows);//从第1行开始按列取数
                     String methodName = getSetMethodName(schemaClass, head);
                     if (methodName == null) {
                         String str = "Schema中没有" + head + "对应方法的描述!";
                         buildError("createSchema", str);
                         System.out.println(
                                 "在程序MultiSheetImporter.createSchema() - 78 : " +
                                 str);
                         return null;
                     }
                     Class[] paramType = new Class[1];
                     paramType[0] = Class.forName("java.lang.String");
                     Method method = null;
                     method = schemaClass.getMethod(methodName, paramType);
                     Object[] args = new Object[1];
                     args[0] = StrTool.cTrim(value);
                     method.invoke(schema, args);
                     //
                 }
                 return schema;
         } catch (ClassNotFoundException ex) {
             String str = "没有到对应Schema!";
             buildError("createSchema", str);          
             return null;
         } catch (IllegalAccessException ex) {
             String str = "类型描述的不是Schema!";
             buildError("createSchema", str);
             return null;
         } catch (InstantiationException ex) {
             String str = "声明类失败!";
             buildError("createSchema", str);
         } catch (InvocationTargetException ex) {
             String str = "执行set方法失败!";
             buildError("createSchema", str);
             return null;
         } catch (IllegalArgumentException ex) {
             String str = "执行set方法失败!";
             buildError("createSchema", str);
             return null;
         } catch (SecurityException ex) {
             String str = "没有找到对应方法!";
             buildError("createSchema", str);
             return null;
         } catch (NoSuchMethodException ex) {
             String str = "没有找到对应方法!";
             buildError("createSchema", str);
             return null;
         }
         return null;
     }
     /**
      * 得到shema中的所有和sheet的head相匹配的set方法
      * @param schemaClass Class
      * @param head String
      * @return String
      */
     private String getSetMethodName(Class schemaClass, String head) {
         Method[] methods = schemaClass.getMethods();
         for (int i = 0; i < methods.length; i++) {
             String methodName = methods[i].getName();
             if ((methodName.length() > 3) &&
                 (methodName.substring(0, 3).equals("set"))) {
                 String subName = methodName.substring(3);
                 if (subName.equalsIgnoreCase(head)) {
                     return methodName;
                 }
             }
         }
         return null;
     }
     /*
      * 
      */
     private boolean initSchemaSet() {
     /** 首先判断sheet与tablename是否对应 */
     if (mSheetNames == null || mSheetNames.length <= 0) {
         String str = "没有定义SheetName!";
         buildError("initSchemaSet", str);
         System.out.println("在程序AuditSheetImport.initSchemaSet(): " +
                            str);
         return false;
     }
     if (mTableNames == null || mTableNames.length <= 0) {
         String str = "没有定义对应Sheet所导入的表名!";
         buildError("initSchemaSet", str);
         System.out.println("在程序AuditSheetImport.initSchemaSet(): " +
                            str);
         return false;
     }
     if (mSheetNames.length != mTableNames.length) {
         String str = "SheetName 与 TableName不对应!";
         buildError("initSchemaSet", str);
         System.out.println("在程序AuditSheetImport.initSchemaSet(): " +
                            str);
         return false;
     }

     for (int i = 0; i < mSheetNames.length; i++) {
             String SetName = "com.sinosoft.lis.vschema." + mTableNames[i] +
                              "Set";
             String SchemaName = "com.sinosoft.lis.schema." + mTableNames[i] +
                                 "Schema";
             mSheetAndSchema.put(mSheetNames[i], SchemaName);
             mSheetAndSet.put(mSheetNames[i], SetName);
     }
     return true;
     }
     protected boolean dealOneSheet(Sheet sheet){
    	 return true;
     }
     /*
      * 开始行数
      */
     public void setMStartRows(int mStartRows) {
         this.mStartRows = mStartRows;
     }
     /**
      * 得到返回结果
      * @return SchemaSet
      */
     public HashMap getResult() {
         return mData;
     }
     /*
      * 
      */
     public void setTableName(String[] tTables) {
         mTableNames = tTables;
     }
     /**
      * 出错处理
      * @param szFunc String
      * @param szErrMsg String
      */
     private void buildError(String szFunc, String szErrMsg) {
         CError cError = new CError();
         cError.moduleName = "AuditSheetImport";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         this.mErrors.addOneError(cError);
     }
}