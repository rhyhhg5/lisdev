package com.sinosoft.f1print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sinosoft.lis.f1print.AccessVtsFile;
//import com.sinosoft.imps.system.security.Base64Coder;
import com.sinosoft.utility.StrTool;

/**
 * <p>Title: IMPS</p>
 *
 * <p>Description: Invoice Management & Printing System</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft Co., Ltd.</p>
 *
 * @author xijiahui
 * @version 1.0
 */
public class F1PrintKernelJ1 extends HttpServlet
{
    private static final long serialVersionUID = -6341572275374750829L;

    public F1PrintKernelJ1()
    {
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws
            ServletException, IOException
    {
        try
        {
            System.out.println("F1PrintKernelJ1_servlet begin.................................");
            // Get a name of VTSFile
            String strVFPathName = req.getParameter("RealPath");
            //加密串中的“+”被替换成“@”在URL里传输
            strVFPathName = StrTool.replace(strVFPathName, "@", "+");
            //加密串中的“/”被替换成“*”在URL里传输
            strVFPathName = StrTool.replace(strVFPathName, "*", "/");
           // strVFPathName = Base64Coder.decode(strVFPathName);
            System.out.println(strVFPathName);
            //strVFPathName = StrTool.replace(strVFPathName,"%20"," ");
            //String strVFPathName = (String)session.getValue("RealPath");
            ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            // Load VTS file to buffer
            AccessVtsFile.loadToBuffer(dataStream, strVFPathName);
            // Put a stream from buffer which contains VTS file to client
            byte[] bArr = dataStream.toByteArray();
            OutputStream ous = res.getOutputStream();
            ous.write(bArr);
            ous.flush();
            ous.close();
            dataStream.close();
        }
        catch(java.net.MalformedURLException urlEx)
        {
            urlEx.printStackTrace();
        }
        catch(java.io.IOException ioEx)
        {
            ioEx.printStackTrace();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
