package com.sinosoft.f1print;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SysConst;

public class PillotConfigBL {

	public CErrors mErrors = new CErrors();

	private String mAction = "";
	private String ManageCom = "";

	MMap map = new MMap();
	private GlobalInput mGlobalInput = new GlobalInput();

	public PillotConfigBL() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "SysSignActivityStatusBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("UPDATE")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("End getInputData");

		// 添加试点
		if (mAction.equals("addToPilot")) {
			addToPilot();
		}

		// 取消试点
		if (mAction.equals("removeFromPilot")) {
			removeFromPilot();
		}

		VData vData = new VData();
		vData.add(map);

		PubSubmit pubsubmit = new PubSubmit();
		if (!pubsubmit.submitData(vData, "")) {
			mErrors.copyAllErrors(pubsubmit.mErrors);
			return false;
		}
		System.out.println("---End pubsubmit---");
		System.out.println("操作成功！");
		return true;
	}

	private boolean getInputData(VData cInputData) {

		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		ManageCom = (String) tTransferData.getValueByName("ManageCom");
		mAction = (String) tTransferData.getValueByName("mAction");

		System.out.println(" ManageCom = " + ManageCom);
		System.out.println(" mAction = " + mAction);
		return true;
	}

	// 添加试点
	public boolean addToPilot() {
		String sql = "update ldcode set othersign ='1'  where codetype='pilotconfig' and code='"
				+ ManageCom + "'";
		System.out.println("Excu SQL : " + sql);
		map.put(sql, SysConst.UPDATE);
		return true;
	}

	// 取消试点
	public boolean removeFromPilot() {
		String sql = "update ldcode set othersign ='0'  where codetype='pilotconfig' and code='"
				+ ManageCom + "'";
		System.out.println("Excu SQL : " + sql);
		map.put(sql, SysConst.UPDATE);
		return true;
	}

}
