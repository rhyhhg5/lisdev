package com.sinosoft.f1print;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.f1j.ss.Constants;
//import com.sinosoft.imps.system.security.Base64Coder;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.f1print.AccessVtsFile;
/**
 * <p>Title: IMPS</p>
 *
 * <p>Description: Invoice Management & Printing System</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft Co., Ltd.</p>
 *
 * @author xijiahui
 * @version 1.0
 */
public class F1DownLoadJ1 extends HttpServlet
{
    private static final long serialVersionUID = -6714557844747838456L;

    public F1DownLoadJ1()
    {
    }

    public void service(HttpServletRequest request,
                        HttpServletResponse response) throws
            ServletException, IOException
    {
        try
        {
            // Get a name of VTSFile
            //现在直接通过参数传递文件路径，不需要从session中读取
            //String strVFPathName = (String)session.getValue("RealPath");
            //session.removeAttribute("RealPath");
            String strVFPathName = request.getParameter("RealPath");
            //加密串中的“+”被替换成“@”在URL里传输
            strVFPathName = StrTool.replace(strVFPathName, "@", "+");
            //加密串中的“/”被替换成“*”在URL里传输
            strVFPathName = StrTool.replace(strVFPathName, "*", "/");
            //strVFPathName = Base64Coder.decode(strVFPathName);
            // Load VTS file to buffer
            ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            AccessVtsFile.loadToBuffer(dataStream, strVFPathName);
            byte[] bArr = dataStream.toByteArray();
            InputStream ins = new ByteArrayInputStream(bArr);
            com.f1j.ss.BookModelImpl bm = new com.f1j.ss.BookModelImpl();
            if(ins != null)
            {
                // Now, reload data file from mem
                bm.read(ins, new com.f1j.ss.ReadParams());
                bm.setShowGridLines(true); // 显示网格
                bm.setShowColHeading(true); // 显示列头
                bm.setShowRowHeading(true); // 显示行头
                bm.setShowTabs(Constants.eTabsBottom); // 显示sheet
                bm.saveViewInfo();
                // Write Excel file to client
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition",
                                   "attachment; filename=data.xls");
                OutputStream ous = response.getOutputStream();
                bm.write(ous,
                         new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.
                        eFileExcel97));
                //输出信息
                ous.flush();
                //关闭输出流
                ous.close();
            }
            else
            {
                System.out.println("There is not any data stream!");
            }
            //关闭不使用的对象
            ins.close();
            dataStream.close();
            bm.destroy();
        }
        catch(java.net.MalformedURLException urlEx)
        {
            urlEx.printStackTrace();
        }
        catch(java.io.IOException ioEx)
        {
            ioEx.printStackTrace();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
