/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;

import org.apache.log4j.Logger;

import com.sinosoft.lis.schema.LDUserSchema;

/*
 * <p>ClassName: </p>
 * <p>Description: 生成 DBTablename.java 文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @author: HST
 * @version: 1.0
 * @date: 2002-06-17
 */
public class SQLString
{
    // @Constructor
    public SQLString(String t)
    {
        TableName = t;
    }

    // @Field
    private String TableName;
    private String WherePart;
    private String PKWherePart;
    private String UpdPart;
    private String InsPart;
    private String SQL;
    private final String mark = "'";
    private Logger m_log = Logger.getLogger(SQLString.class);
    private String Opt = "";
    public CErrors mErrors = new CErrors(); // 错误信息

    /**
     * 获得数据库操作sql
     * @param flag int
     * @param s Schema
     */
    public void setSQL(int flag, Schema s)
    {
        switch (flag)
        {
            case 1:

                // insert
                this.setInsPart(s);
                SQL = "insert into " + TableName + " " + InsPart;
                if (InsPart.equals(""))
                {
                    SQL = "";
                }
                break;
            case 2:

                // update (by Primary Key)
                this.setUpdPart(s);
                this.setPKWherePart(s);
                SQL = "update " + TableName + " " + UpdPart + " " + PKWherePart;
                if (UpdPart.equals(""))
                {
                    SQL = "";
                }
                break;
            case 3:

                // delete
            	Opt = "delete";
            	this.setWherePart(s);
                SQL = "delete from " + TableName + " " + WherePart;
                
                /**公共控件改造，增加where条件校验*/
                if (WherePart.equals("")) {
        	        if (SysConst.IfDeleteError == 1) {
        		        CError tError = new CError();
        		        tError.moduleName = "SQLString";
        		        tError.functionName = "setSQL";
        		        tError.errorMessage = "传入的Schema没有值，请检查程序。";
        		        this.mErrors.addOneError(tError);
        		        SQL = "";
        		        System.err.println("delete SQL = " + SQL);
        	        }
                }
                break;
            case 4:

                // delete (by Primary Key)
                this.setPKWherePart(s);
                SQL = "delete from " + TableName + " " + PKWherePart;
                break;
            case 5:

                // select-
            	Opt = "select";
            	this.setWherePart(s);
                SQL = "select * from " + TableName + " " + WherePart;
                
                /**公共控件改造，增加where条件校验*/
                if (WherePart.equals("")) {
        	        if (SysConst.IfNullQueryError == 1) {
        		        CError tError = new CError();
        		        tError.moduleName = "SQLString";
        		        tError.functionName = "setSQL";
        		        tError.errorMessage = "传入的Schema没有值，请检查程序。";
        		        this.mErrors.addOneError(tError);
        		        SQL = "";
        		        System.err.println("select SQL = " + SQL);
        	        }
                }
                break;
            case 6:

                // select (by Primary Key)
                this.setPKWherePart(s);
                SQL = "select * from " + TableName + " " + PKWherePart;
                break;
            case 7:

                // select Count
            	Opt = "select Count";
            	this.setWherePart(s);
                SQL = "select count(*) from " + TableName + " " + WherePart;
                
                /**公共控件改造，增加where条件校验*/
                if (WherePart.equals("")) {
        	        if (SysConst.IfNullQueryError == 1) {
        		        CError tError = new CError();
        		        tError.moduleName = "SQLString";
        		        tError.functionName = "setSQL";
        		        tError.errorMessage = "传入的Schema没有值，请检查程序。";
        		        this.mErrors.addOneError(tError);
        		        SQL = "";
        		        System.err.println("select count SQL = " + SQL);
        	        }
                }
                break;
        }
    }

    public String getSQL()
    {
        System.out.println("SQL: " + SQL);
        return SQL;
    }

    public String getSQL(int sqlFlag, Schema s)
    {
        if (sqlFlag == 5 || sqlFlag == 6)
        {
            this.setSQL(sqlFlag, s);
        }
        else
        {
            SQL = "";
        }
        System.out.println("SQL: " + SQL);
        return SQL;
    }

    /**
     * 通过 Primary Key 组成 WherePart
     * @param s Schema
     */
    public void setPKWherePart(Schema s)
    {
        PKWherePart = "where";

        String[] pk = s.getPK();
        int n = pk.length;

        for (int i = 0; i < n; i++)
        {
            String strFieldName = pk[i];
            String strFieldValue = s.getV(strFieldName);
            int nFieldType = s.getFieldType(strFieldName);

            String wp = "";

            switch (nFieldType)
            {
                case Schema.TYPE_STRING:
                case Schema.TYPE_DATE:
                    strFieldValue = mark + strFieldValue + mark;
                    break;
                case Schema.TYPE_DOUBLE:
                case Schema.TYPE_FLOAT:
                case Schema.TYPE_INT:
                    break;
                default:
                    break;
            }

            if (i != 0)
            {
                wp += " and";
            }

            wp += " " + strFieldName + "=" + strFieldValue;
            PKWherePart += wp;

        }
    }

    public String getPKWherePart()
    {
        return PKWherePart;
    }

    /**
     * 通过 Schema 对象组成 WherePart
     * @param s Schema
     */
    public void setWherePart(Schema s)
    {
        WherePart = "where";

        int nFieldCount = s.getFieldCount();
        int j = 0;
        for (int i = 0; i < nFieldCount; i++)
        {
            String strFieldName = s.getFieldName(i);
            String strFieldValue = s.getV(i);
            int nFieldType = s.getFieldType(i);
            boolean bFlag = false;

            switch (nFieldType)
            {
                case Schema.TYPE_STRING:
                case Schema.TYPE_DATE:
                    if (!strFieldValue.equals("null"))
                    {
                        strFieldValue = mark + strFieldValue + mark;
                        bFlag = true;
                    }
                    break;

                case Schema.TYPE_DOUBLE:
                    if (!strFieldValue.equals("0.0"))
                    {
                        bFlag = true;
                    }
                    break;

                case Schema.TYPE_FLOAT:
                    if (!strFieldValue.equals("0.0"))
                    {
                        bFlag = true;
                    }
                    break;

                case Schema.TYPE_INT:
                    if (!strFieldValue.equals("0"))
                    {
                        bFlag = true;
                    }
                    break;

                default:
                    bFlag = false;
                    break;
            }

            if (bFlag == true)
            {
                j++;
                String wp = "";
                if (j != 1)
                {
                    wp += " and";
                }
                wp += " " + strFieldName + "=" + strFieldValue;
                WherePart += wp;
            }
        }
        if (j == 0)
        {
        	WherePart = "";
        	
        	/**公共控件改造，增加where条件校验*/
        	/*此处如果增加是否退出的处理，只能使用抛出异常的方式。
        	  考虑到其它类调用该方法时，有可能未作捕获异常处理，所以放弃使用。只把错误信息写入log中。
        	*/
        	//记录堆栈信息
        	Exception ex =new Exception("传入的Schema中没有值 所以没有 where 条件，导致 " + Opt + " 数据量过大，请检查程序");
	        m_log.warn("在类SQLString的方法setWherePart(Schema s)中报错",ex);
        }
    }

    public String getWherePart()
    {
        return WherePart;
    }

    /**
     * 通过 Schema 对象组成 UpdPart
     * @param s Schema
     */
    public void setUpdPart(Schema s)
    {
        UpdPart = "set ";

        int nFieldCount = s.getFieldCount();

        for (int i = 0; i < nFieldCount; i++)
        {
            String strFieldName = s.getFieldName(i);
            String strFieldValue = s.getV(i);
            int nFieldType = s.getFieldType(i);

            switch (nFieldType)
            {
                case Schema.TYPE_STRING:
                case Schema.TYPE_DATE:
                    if (!strFieldValue.equals("null"))
                    {
                        strFieldValue = mark + strFieldValue + mark;
                    }
                    break;
                case Schema.TYPE_DOUBLE:
                case Schema.TYPE_FLOAT:
                case Schema.TYPE_INT:

                    // 不管初始值是0，还是输入0，一律插入数据库。
                    break;
                default:
                    break;
            }

            String wp = "";
            if (i != 0)
            {
                wp += ",";
            }
            wp += strFieldName + "=" + strFieldValue;
            UpdPart += wp;
        }
    }

    public String getUpdPart()
    {
        return UpdPart;
    }

    /**
     * 通过 Schema 对象组成 InsPart
     * @param s Schema
     */
    public void setInsPart(Schema s)
    {
        String ColPart = "( ";
        String ValPart = "values ( ";

        int nFieldCount = s.getFieldCount();
        int j = 0;

        for (int i = 0; i < nFieldCount; i++)
        {
            String strFieldName = s.getFieldName(i);
            String strFieldValue = s.getV(i);
            int nFieldType = s.getFieldType(i);
            boolean bFlag = false;

            switch (nFieldType)
            {
                case Schema.TYPE_STRING:
                case Schema.TYPE_DATE:
                    if (!strFieldValue.equals("null"))
                    {
                        strFieldValue = mark + strFieldValue + mark;
                        bFlag = true;
                    }
                    break;
                case Schema.TYPE_DOUBLE:
                case Schema.TYPE_FLOAT:
                case Schema.TYPE_INT:
                    bFlag = true;
                    break;
                default:
                    bFlag = false; // 不支持的数据类型
                    break;
            }

            if (bFlag == true)
            {
                j++;
                String wp = "";
                if (j != 1)
                {
                    ColPart += ",";
                    ValPart += ",";
                }
                ColPart += strFieldName;
                ValPart += strFieldValue;
            }
        }
        ColPart += " )";
        ValPart += " )";
        InsPart = ColPart + " " + ValPart;
        if (j == 0)
        {
            InsPart = "";
        }
    }

    public String getInsPart()
    {
        return InsPart;
    }
    public static void main(String args[]){
    	SQLString tSQLString = new SQLString("lduser");
    	LDUserSchema tLDUserSchema = new LDUserSchema();
    	tSQLString.setSQL(5, tLDUserSchema);
    	tSQLString.setSQL(7, tLDUserSchema);
    }

}
