package com.sinosoft.utility;

import org.jdom.*;
import org.jdom.output.*;
import org.jdom.input.*;
import java.util.*;
import java.io.*;
import java.util.List;
import com.sinosoft.lis.db.LPEdorAppPrintDB;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Blob;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class XmlUpdate
{
    private Map map = new HashMap();

    private org.jdom.Document document = null;

    public XmlUpdate(InputStream xmlStream)
    {
        try
        {
            SAXBuilder saxBuilder = new SAXBuilder();
            this.document = saxBuilder.build(xmlStream);
        }
        catch (JDOMException ex)
        {
            ex.printStackTrace();
        }
    }

    public void addTextTag(TextTag texttag)
    {
        for (int i = 0; i < texttag.size(); i++)
        {
            String[] temparray = (String[]) texttag.get(i);
            Element element = new Element(temparray[0]).addContent(temparray[1]);
            map.put(element.getName(), element);
        }
    }

    public void addListTable(ListTable listtable, String[] colvalue)
    {
        Element table = new Element(listtable.getName());
        Element head = new Element("HEAD");
        table.addContent(head);
        for (int m = 0; m < colvalue.length; m++)
        {
            int n = m + 1;
            String colnum = "COL" + n;
            head.addContent(new Element(colnum).addContent(colvalue[m]));
        }

        for (int i = 0; i < listtable.size(); i++)
        {
            String[] temparray = new String[colvalue.length];
            temparray = listtable.get(i);
            Element row = new Element("ROW");
            table.addContent(row);
            for (int m = 0; m < temparray.length; m++)
            {
                int n = m + 1;
                String colnum = "COL" + n;
                row.addContent(new Element(colnum).addContent(temparray[m]));
            }
        }
        map.put(listtable.getName(), table);
    }

    /**
     * 若原vts包括map中节点，则修改为map中数据
     * @return boolean
     */
    public boolean update()
    {
        if (document == null)
        {
            return false;
        }
        Element root = document.getRootElement();
        List nodeList = root.getChildren();
        for (int i = 0; i < nodeList.size(); i++)
        {
            Element element = (Element) nodeList.get(i);
            if (map.containsKey(element.getName()))
            {
                nodeList.remove(i);
                nodeList.add(i, map.get(element.getName()));
            }
        }
        return true;
    }

    /**
     * update第二版，若原vts文件不包含map中节点，则插入，包含则修改
     * @return boolean
     */
    public boolean updateV2()
    {
        if (document == null)
        {
            return false;
        }
        Element root = document.getRootElement();
        List nodeList = root.getChildren();
//        for (int i = 0; i < nodeList.size(); i++)
//        {
//            Element element = (Element) nodeList.get(i);
//            if (map.containsKey(element.getName()))
//            {
//                //已存在则修改
//                nodeList.remove(i);
//                nodeList.add(i, map.get(element.getName()));
//            }
//            else
//            {
//                //不存在，将节点放在i+1位置，同时循环指针再往后移一位，
//                //保证下次混换不重复比较nodeList内容
//                nodeList.add(i+1, map.get(element.getName()));
//                i++;
//            }
//        }

        for (int i = 0; i < map.keySet().size(); i++)
        {
            boolean hasFlag = false;
            Element element = null;

            int listSize = nodeList.size();
            for(int j = 0; j < listSize; j++)
            {
                element = (Element) nodeList.get(j);

                if(map.containsKey(element.getName()))
                {
                    //已存在则修改
                    nodeList.remove(i);
                    nodeList.add(i, map.get(element.getName()));
                    break;
                }

                //不存在，再次校验原批单数据
                hasFlag = true;
            }

            if(hasFlag)
            {
                nodeList.add(nodeList.size(), map.get(map.keySet().toArray()[i]));
            }
        }

        return true;
    }

    /**
     * 删除重复节点
     * @return boolean
     */
    public boolean deleteNode()
    {
        if (document == null)
        {
            return false;
        }
        Element root = document.getRootElement();
        List nodeList = root.getChildren();
        for (int i = 0; i < nodeList.size(); i++)
        {
            Element element = (Element) nodeList.get(i);
            if (map.containsKey(element.getName()))
            {
                nodeList.remove(i);
                i--;
            }
        }

        return true;
    }

    public String getXmlString()
    {
        try
        {
            XMLOutputter outputter = new XMLOutputter();
            return outputter.outputString(document);
        }
        catch (IOException ex)
        {
            return null;
        }
    }

    public InputStream getInputStream()
    {
        try
        {
            XMLOutputter outputter = new XMLOutputter();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            outputter.output(document, baos);
            baos.close();
            return new ByteArrayInputStream(baos.toByteArray());
        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws Exception
    {
        String edorNo = "20060117000005";

        InputStream ins = null;
        String sql = "select * from LPEDORAPPPRINT where EdorAcceptNo = '" + edorNo + "' ";
        Connection conn = DBConnPool.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        if(rs.next())
        {
            Blob tBlob = rs.getBlob("EdorInfo");
            ins = tBlob.getBinaryStream();
        }
        rs.close();
        stmt.close();
        conn.close();


        XmlUpdate xmlUpdate = new XmlUpdate(ins);
        TextTag textTag = new TextTag();
        textTag.add("PayMode", "cccc");
        xmlUpdate.addTextTag(textTag);
        xmlUpdate.update();
        String xml = xmlUpdate.getXmlString();
        System.out.println(xml);

        LPEdorAppPrintDB tLPEdorAppPrintDB = new LPEdorAppPrintDB();
        tLPEdorAppPrintDB.setEdorAcceptNo(edorNo);
        tLPEdorAppPrintDB.getInfo();


        ins = xmlUpdate.getInputStream();
        tLPEdorAppPrintDB.setEdorInfo(ins);

        MMap tMMap = new MMap();

        sql = "delete from LPEdorAppPrint " +
                      "where EdorAcceptNo = '" + edorNo + "' ";
                System.out.println(sql);
        tMMap.put(sql, "DELETE");
        tMMap.put(tLPEdorAppPrintDB.getSchema(), "BLOBINSERT");
        VData d = new VData ();
        d.add(tMMap);

        new PubSubmit().submitData(d, "");
    }
}
