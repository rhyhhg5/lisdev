/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;

import java.util.Vector;

/**
 * <p>Title:传递数据 </p>
 * <p>Description:
 *  可以将前台不能通过Schema传递的数据传递到后台处理.
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author hzm
 * @version 1.0
 */

public class TransferData
{
    public TransferData()
    {
    }

    private Vector nameVData = new Vector(); //存放变量名
    private Vector valueVData = new Vector(); //存放对应该变量的值

    public static void main(String[] args)
    {
        TransferData transferData1 = new TransferData();
        /*
             Date t1= new Date("12/22/2000");
             Integer t2= new Integer(23);
             Float t3= new Float(34);
             transferData1.setNameAndValue("first",t2);
             transferData1.setNameAndValue("second",t3);
             Integer value=(Integer)transferData1.getValueByName("first");
             Float value2=(Float)transferData1.getValueByName("second");
         */
        transferData1.setNameAndValue("int", "");
        String value = (String) transferData1.getValueByName("int");
        System.out.println("value:" + value + value.equals("null"));
        Integer value2 = (Integer) transferData1.getValueByName("int");
        System.out.println("value2:" + value2);
    }

    /**设置要传入的变量的名字和对应的值(对象)
     * 将要传入的变量的名字存入 nameVData容器中
     * 将要传入的变量的值存入 valueVData容器中
     * 参数  ：   name:变量的名字，value:变量的值
     * 返回值：   无
     */
    public void setNameAndValue(Object name, Object value)
    {
        nameVData.add(name);
        valueVData.add(value);
    }

    /**设置要传入的变量的名字和对应的值(float)
     * 将要传入的变量的名字存入 nameVData容器中
     * 将要传入的变量的值存入 valueVData容器中
     * 参数  ：   name:变量的名字，value:变量的值float型
     * 返回值：   无
     */
    public void setNameAndValue(Object name, float value)
    {
        nameVData.add(name);
        valueVData.add(new Float(value));
    }

    /**设置要传入的变量的名字和对应的值(double)
     * 将要传入的变量的名字存入 nameVData容器中
     * 将要传入的变量的值存入 valueVData容器中
     * 参数  ：   name:变量的名字，value:变量的值double型
     * 返回值：   无
     */
    public void setNameAndValue(Object name, double value)
    {
        nameVData.add(name);
        valueVData.add(new Double(value));
    }

    /**设置要传入的变量的名字和对应的值(int)
     * 将要传入的变量的名字存入 nameVData容器中
     * 将要传入的变量的值存入 valueVData容器中
     * 参数  ：   name:变量的名字，value:变量的值int型
     * 返回值：   无
     */
    public void setNameAndValue(Object name, int value)
    {
        nameVData.add(name);
        valueVData.add(new Integer(value));
    }

    /**返回和传入的变量的名字对应的值
     * 参数  ：   name:变量的名字
     * 返回值：   变量的值,没有找到返回null
     */
    public Object getValueByName(Object name)
    {
        for (int i = 0; i < nameVData.size(); i++)
        {
            if (nameVData.elementAt(i).equals(name))
            {
                return valueVData.get(i);
            }
        }
        return null;
    }

    /**返回传入的变量名字对应在集合中的位置
     * 参数  ：   name:变量的名字
     * 返回值：   在集合中的位置,从0开始。没有则返回-1
     */
    public int findIndexByName(Object name)
    {
        for (int i = 0; i < nameVData.size(); i++)
        {
            if (nameVData.elementAt(i).equals(name))
            {
                return i;
            }
        }
        return -1;
    }

    /**删除集合中的对应传入变量的值
     * 参数  ：   name:变量的名字
     * 返回值：   返回ture
     */
    public boolean removeByName(Object name)
    {
        for (int i = 0; i < nameVData.size(); i++)
        {
            if (nameVData.elementAt(i).equals(name))
            {
                valueVData.remove(i);
                nameVData.remove(i);
            }
        }
        return true;
    }

    /**返回所有的变量名字值
     * 参数  ：
     * 返回值：   变量的值,没有找到返回null
     */
    public Vector getValueNames()
    {
        return nameVData;
    }
    
    /**
     * 返回所有变量的值 Added by Xx
     * @return
     */
    public Vector getValues(){
    	return valueVData;
    }

    /**返回和传入的变量序号对应的值
     * 参数  ：   i:第几个值 Added By Xx
     * 返回值：   变量的值,没有找到返回null
     */
    public Object getValueByIndex(int i)
    {
    	return valueVData.get(i);
    }

    public Object getNameByIndex(int i)
    {
    	return nameVData.get(i);
    }

    public String[] getValues1(){
    	int n = valueVData.size();
    	String[] tValues = new String[n];
    	for(int i=0;i<n;i++){
    		tValues[i] = valueVData.get(i).toString();
    	}
    	return tValues;
    }
}
