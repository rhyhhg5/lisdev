package com.sinosoft.utility;
import com.f1j.ss.CellFormat;

public class BookModelImpl_Ex extends com.f1j.ss.BookModelImpl {
	
	public static Object lock = new Object();;

	public void BookModelImpl_Ex()
	{
		new com.f1j.ss.BookModelImpl();
	}
	
	public synchronized void insertRange(int arg0, int arg1, int arg2, int arg3, short arg4)
	{
		synchronized (lock) {
			try {
				super.insertRange(arg0, arg1, arg2, arg3, arg4);
			} catch (Exception ex) {
				return;
			}
		}
	}
	
	public synchronized void setCellFormat(CellFormat arg0) {
		// synchronized (lock)
		{
			try {
				super.setCellFormat(arg0);
			} catch (Exception ex) {
				return;
			}
		}
	}
	
	public synchronized void deleteRange(int arg0, int arg1, int arg2,
			int arg3, short arg4) {
//		synchronized (lock) 
		{
			try {
				super.deleteRange(arg0, arg1, arg2, arg3, arg4);
			} catch (Exception ex) {
				return;
			}
		}
	}
	
}
