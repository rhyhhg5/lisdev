package com.sinosoft.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;

public class DBConnSqlServer {
	
	JdbcUrl mJdbcUrl = null;
	Connection conn = null;
	CErrors mErrors = new CErrors();
	
	public DBConnSqlServer(){
		JdbcUrl mJdbcUrl = null;
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		conn=null;
	}
	
	public Connection getConnection(JdbcUrl mJdbcUrl){
		if(conn!=null){
			return conn;
		}else{
			this.createConnection(mJdbcUrl);
			return conn;
		}
	}
	
	public void closeConnection(){
		try {
			if (conn != null) {
				if (!conn.isClosed()) {
					conn.close();
					conn = null;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    private boolean createConnection(JdbcUrl tJdbcUrl) {
    	mJdbcUrl = tJdbcUrl;
    	//连接信息
		String ip = tJdbcUrl.getIP();
		String port = tJdbcUrl.getPort();
		String serverName = tJdbcUrl.getServerName();
		String DBName = tJdbcUrl.getDBName();
		String user = tJdbcUrl.getUserName();
		String pass = tJdbcUrl.getPassWord();
		String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String tUrl="jdbc:sqlserver://"+ip+":"+port+"; DatabaseName="+DBName;
		System.out.println(tUrl);
		//获取连接
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", pass);
        try{
        	if(conn!=null){
        		if(!conn.isClosed()){
        			conn.close();
            		conn=null;
        		}
        	}
        	Class.forName(driver);
        	conn = DriverManager.getConnection(tUrl, props);
        	System.out.println(conn);
        }catch (ClassNotFoundException e) {
        	e.printStackTrace();
            System.out.println("未找到驱动类" + driver);
            CError tError = new CError();
            tError.moduleName = "DBConn";
            tError.functionName = "createConnection";
            tError.errorMessage = "Connect failed!  error code =未找到驱动类 " + driver;
            this.mErrors.addOneError(tError);
		}catch (SQLException e) {
            e.printStackTrace();
            System.out.println("创建连接失败..." + e.getMessage());
            CError tError = new CError();
            tError.moduleName = "DBConn";
            tError.functionName = "createConnection";
            tError.errorMessage = "Connect failed!  error code =" +e.getErrorCode();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    public static void main(String[] args){
    	DBConnSqlServer tDBConnSqlServer = new DBConnSqlServer();
    	JdbcUrl tJdbcUrl = new JdbcUrl();
    	//调用tJdbcUrl的set方法，略
    	tJdbcUrl.setDBType("SQLSERVER");
    	tJdbcUrl.setIP("10.252.11.9");
    	tJdbcUrl.setPort("1433");
    	tJdbcUrl.setDBName("PI4C");
    	tJdbcUrl.setUser("huifang");
    	tJdbcUrl.setPassWord("12345678");
       
    	
    	tDBConnSqlServer.getConnection(tJdbcUrl);
    	//根据SQL语句获取结果集
    	String sql = "select  top 2 *  from AM_11_WorkOrder ";
    	ResultSet tResultSet = tDBConnSqlServer.getResultSet(sql);
    	if(tResultSet==null){
    		System.out.println("获取结果集过程，连接中断或连接为空");
    		//添加错误处理,进行阻断
    	}
    	
//    	/**
//    	 * 方案一开始，拼接SQL的方式
//    	 */
//    	try {
//			while(tResultSet.next()){
//				String col1 = tResultSet.getString(1);
//				//String col2 = String.valueOf(tResultSet.getInt(2));
//				//String insertSql = "insert into table2 values("+col1+","+col2+")";
//				//执行SQL,提交......
//				System.out.println(col1);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//    	/**
//    	 * 方案二开始，拼接SQL的方式
//    	 */
//    	try {
//			while(tResultSet.next()){
//				String col1 = tResultSet.getString(1);
//				String col2 = tResultSet.getString(2);
//				System.out.println(col1);
//				System.out.println(col2);
//				LDCodeSchema tLDCodeSchema = new LDCodeSchema();
//				tLDCodeSchema.setCodeType("yse");
//				tLDCodeSchema.setCode(col2);
//				tLDCodeSchema.setCodeName(col2);
//				MMap map = new MMap();
//				map.put(tLDCodeSchema, "INSERT");	
//				VData mInputData = new VData();
//				mInputData.clear();
//				mInputData.add(map);
//				PubSubmit tPubSubmit = new PubSubmit();
//		        if (!tPubSubmit.submitData(mInputData, "")) {
//		            // @@错误处理
//		        	System.out.println("出錯了，真的");
//		        }
//		        else
//		        	System.out.println("对了，真的");
//				//......pubsubmit;
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
    	//各种操作
    	tDBConnSqlServer.closeConnection();
    }
    
    public ResultSet getResultSet(String sql){
    	ResultSet tResultSet = null;
    	try {
			if(conn==null || conn.isClosed()){
				System.out.println("连接已经关闭，请重新获取");
				return null;
			}
			PreparedStatement tPreparedStatement = conn.prepareStatement(StrTool.GBKToUnicode(sql)
					,ResultSet.TYPE_FORWARD_ONLY
                    , ResultSet.CONCUR_READ_ONLY);
			tResultSet = tPreparedStatement.executeQuery();	
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return tResultSet;
    }
}
