package com.sinosoft.utility;
/*
 *  打印元数据记录工具类
 *  2010-8-3
 *  ZhangXiaolei
 * 
 */

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import weblogic.wtc.jatmi.FML;

import com.sinosoft.utility.SysLog;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.GlobalInput;

import com.sinosoft.lis.schema.LDRInforSchema;
import com.sinosoft.lis.vschema.LDRStatisConSet;
import com.sinosoft.lis.schema.LDRStatisConSchema;  
import com.sinosoft.lis.schema.LDRVDesSchema;
import java.net.URL;
  
   
public class RptMetaDataRecorder {
	
	public final static String rptSeqPrefix = "RPTDWNO";
	
	public final static int rptSeqLength = 10;
	
	private String rptSequence ;
	
	private LDRInforSchema rptInfoSchema ; 
	
	private LDRStatisConSet rptStatCondSet; 
	
	public RptMetaDataRecorder ( HttpServletRequest request)
	{
		//出现报表更新错误的情况，记录文件名非打印文件名 mn 2012-01-19
		rptInfoSchema = new LDRInforSchema();
			this.rptSequence = CreateMaxNo();
			if ("".equals(rptSequence)) {
				System.out.println("报表下载序列号生成错误");
			}
			prepareRptInfo( request );
			String saveFileName = request.getServletPath();
			System.out.println("saveFileName="+saveFileName);
			SysLog.log( RptMetaDataRecorder.class.getName()+ "URI: " + request.getRequestURI() + "  ServletPath: "  + request.getServletPath());
			prepareRptCond( getVariableDesMap ( saveFileName ),request);
			saveReportMetaData();  
	}          
	     
	private void saveReportMetaData()
	{
		if ( rptInfoSchema == null || rptStatCondSet == null || rptStatCondSet.size() == 0 ) return ;
		MMap  map = new MMap();
		VData inputData = new VData();
		map.put(this.rptInfoSchema,"INSERT");		
		map.put(this.rptStatCondSet,"INSERT");		
		inputData.add(map);
		
		PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(inputData, "")) 
        {
        	SysLog.log( RptMetaDataRecorder.class.getName() + " 保存报表打印元数据失败！");
        }
    }
	
	
	private void prepareRptCond( Map  variableNames,HttpServletRequest request )
	{
		if( variableNames == null || variableNames.size() == 0 ) return;
		LDRStatisConSet curSCSet = new LDRStatisConSet();
		Enumeration curVariableNames = request.getParameterNames();
		while( curVariableNames.hasMoreElements() )
		{
			String varnme = (String) curVariableNames.nextElement(); 
			if ( variableNames.containsKey( varnme ) ) 
			{
				LDRStatisConSchema varConditionDes = new LDRStatisConSchema();
				varConditionDes.setSequece(this.rptSequence);
				varConditionDes.setVariableName(varnme);
				varConditionDes.setValue( request.getParameter(varnme) );
				curSCSet.add(varConditionDes);  
			}
		}
		this.rptStatCondSet = curSCSet;
		
	}
	
	private void prepareRptInfo( HttpServletRequest request )
	{
		HttpSession curSession = request.getSession();
		GlobalInput gi =  (GlobalInput)curSession.getAttribute("GI"); // get the user information
		LDRInforSchema curRptinfoSchema = new LDRInforSchema();
		curRptinfoSchema.setNumber(this.rptSequence);  
		curRptinfoSchema.setRptName(request.getServletPath());
		curRptinfoSchema.setTempName("");
		curRptinfoSchema.setUserName(gi.Operator);
		if(request.getParameter("ManageCom")==null)
		{		
		curRptinfoSchema.setManageCom(gi.ComCode);
		}
		else {
		curRptinfoSchema.setManageCom(RptMetaDataRecorder.validatedStr(request.getParameter("ManageCom")));
		}
		curRptinfoSchema.setMakeStatus("0");//正在打印
		curRptinfoSchema.setMakeDate(PubFun.getCurrentDate());
		curRptinfoSchema.setMakeTime(PubFun.getCurrentTime());
		curRptinfoSchema.setModifyDate(PubFun.getCurrentDate());
		curRptinfoSchema.setModifyTime(PubFun.getCurrentTime());  
		curRptinfoSchema.setMagStartDate(RptMetaDataRecorder.validatedStr(request.getParameter("StartDate")));
		curRptinfoSchema.setMagEndDate(RptMetaDataRecorder.validatedStr(request.getParameter("EndDate")));
		this.rptInfoSchema = curRptinfoSchema;
	}  
	
	public static String validatedStr( String str)
	{
		return (str == null ) ? "" : str;		
	}
	
	public  static   Map getVariableDesMap (String saveFileName )
	{
		HashMap hashMap = new HashMap();
		SSRS ssrs = (new ExeSQL()).execSQL(" Select VariableName From LDRVDes Where remark1 ='" + saveFileName + "'" );
		if (ssrs.MaxRow == 0 )
		{
			return null;
		}
		for ( int i =1 ; i<= ssrs.MaxRow ; i++ )
		{
			hashMap.put(ssrs.GetText(i,1), "");
		}
		return hashMap;
	}
	   

	public  void updateReportMetaData(String tempFileName)  
	{
		  
		if ( rptInfoSchema == null ) return;
		rptInfoSchema.setTempName(tempFileName);
		System.out.println("!!!!!!!!!!!");
		rptInfoSchema.setMakeStatus("1");//打印完成
		rptInfoSchema.setModifyDate(PubFun.getCurrentDate());
		rptInfoSchema.setModifyTime(PubFun.getCurrentTime());
		MMap  map = new MMap();
		VData inputData = new VData();
		map.put(this.rptInfoSchema,"UPDATE");
		System.out.println("~~~~~~~~~~~");
		inputData.add(map);  
		System.out.println("**********");
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("<<<<<<<<<<<");
        if (!tPubSubmit.submitData(inputData, "")) 
        {
        	SysLog.log( RptMetaDataRecorder.class.getName() + " 更新报表打印元数据失败！");
        }
	}    
	
	/**
	 * 生成序列号
	 * @return
	 */
	private String CreateMaxNo() {
		String tNumber = "";
		
		String tDate = PubFun.getCurrentDate();
        String tDateCode = tDate.substring(2, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);
        
        Connection tConnection = DBConnPool.getConnection();

        if (tConnection == null)
        {
            System.out.println("CreateMaxNo : fail to get db connection");

            return "";
        }
        
        try
        {
        	tConnection.setAutoCommit(false);
            //            System.out.println(cNoType);
            //            System.out.println(cNoLimit);
            Statement stmt = tConnection.createStatement();
            ResultSet rs = stmt
                    .executeQuery("select Description from final table "
			             + " (update LDRVDes set Description=char(Integer(Description)+1 ) "
			             + " where variablename='MAXNO' and remark1='"+tDateCode+"')");
            while (rs.next())
            {
            	tNumber = String.valueOf(rs.getInt(1));
                System.out.println("nomx = " + tNumber);
            }
            rs.close();
            stmt.close();            
            tConnection.commit();

            //            System.out.println("new maxno function");
        }
        catch (Exception e)
        {
            //      JdbcException jdbcExc = new JdbcException(e, con);
            //      jdbcExc.handle();
            try
            {
            	tConnection.rollback();
            }
            catch (SQLException ex)
            {
            }
            e.printStackTrace();
        }
		
		if (tNumber == null || "".equals(tNumber) ||"null".equals(tNumber)) {
//        	String tInsertSQL = "insert into LDRVDes(variablename,Description,remark1,remark2) values "
//        		              + "('MAXNO','1','ReportDownload','"+tDateCode+"')";
//        	tExeSQL.getOneValue(tInsertSQL);
        	LDRVDesSchema tLDRVDesSchema = new LDRVDesSchema();
        	tLDRVDesSchema.setVariableName("MAXNO");
        	tLDRVDesSchema.setDescription("1");
        	tLDRVDesSchema.setRemark1(tDateCode);
        	MMap tMMap = new MMap();
        	tMMap.put(tLDRVDesSchema, "INSERT");
        	VData inputData = new VData();
        	inputData.add(tMMap);
        	PubSubmit tPubSubmit = new PubSubmit();
        	if (!tPubSubmit.submitData(inputData, "")) {
        		return "";
        	}
        	tNumber = "1";
        }

        
        tNumber = "RD" + tDateCode + PubFun.LCh(tNumber, "0", 12);
		
		return tNumber;
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

	

}
