package com.sinosoft.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;

public class DBConnDB {
	
	JdbcUrl mJdbcUrl = null;
	Connection conn = null;
	CErrors mErrors = new CErrors();
	
	public DBConnDB(){
		JdbcUrl mJdbcUrl = null;
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		conn=null;
	}
	
	public Connection getConnection(JdbcUrl mJdbcUrl){
		if(conn!=null){
			return conn;
		}else{
			this.createConnection(mJdbcUrl);
			return conn;
		}
	}
	
	public void closeConnection(){
		try {
			if (conn != null) {
				if (!conn.isClosed()) {
					conn.close();
					conn = null;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    private boolean createConnection(JdbcUrl tJdbcUrl) {
    	mJdbcUrl = tJdbcUrl;
    	//连接信息
		String ip = tJdbcUrl.getIP();
		String port = tJdbcUrl.getPort();
		String serverName = tJdbcUrl.getServerName();
		String DBName = tJdbcUrl.getDBName();
		String user = tJdbcUrl.getUserName();
		String pass = tJdbcUrl.getPassWord();
		String driver = "com.ibm.db2.jcc.DB2Driver";
		String tUrl="jdbc:db2://"+ip+":"+port+"/"+DBName;
		System.out.println(tUrl);
		//获取连接
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", pass);
        try{
        	if(conn!=null){
        		if(!conn.isClosed()){
        			conn.close();
            		conn=null;
        		}
        	}
        	Class.forName(driver);
        	conn = DriverManager.getConnection(tUrl, props);
        	System.out.println(conn);
        }catch (ClassNotFoundException e) {
        	e.printStackTrace();
            System.out.println("未找到驱动类" + driver);
            CError tError = new CError();
            tError.moduleName = "DBConn";
            tError.functionName = "createConnection";
            tError.errorMessage = "Connect failed!  error code =未找到驱动类 " + driver;
            this.mErrors.addOneError(tError);
		}catch (SQLException e) {
            e.printStackTrace();
            System.out.println("创建连接失败..." + e.getMessage());
            CError tError = new CError();
            tError.moduleName = "DBConn";
            tError.functionName = "createConnection";
            tError.errorMessage = "Connect failed!  error code =" +e.getErrorCode();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    public static void main(String[] args){
    	DBConnDB tDBConnDB = new DBConnDB();
    	JdbcUrl tJdbcUrl = new JdbcUrl();
    	//调用tJdbcUrl的set方法，略
    	tJdbcUrl.setDBType("DB2");
    	tJdbcUrl.setIP("10.252.130.128");
    	tJdbcUrl.setPort("50000");
    	tJdbcUrl.setDBName("FIP");
    	tJdbcUrl.setUser("db2finance");
    	tJdbcUrl.setPassWord("db2finance");
       
    	
    	tDBConnDB.getConnection(tJdbcUrl);
    	//根据SQL语句获取结果集
    	String sql = "select  *  from db2inst1.SIP_YIDI_FINANCESETTLEMENT ";
    	ResultSet tResultSet = tDBConnDB.getResultSet(sql);
    	if(tResultSet==null){
    		System.out.println("获取结果集过程，连接中断或连接为空");
    		//添加错误处理,进行阻断
    	}
    	
    	//各种操作
    	tDBConnDB.closeConnection();
    }
    
    public ResultSet getResultSet(String sql){
    	ResultSet tResultSet = null;
    	try {
			if(conn==null || conn.isClosed()){
				System.out.println("连接已经关闭，请重新获取");
				return null;
			}
			PreparedStatement tPreparedStatement = conn.prepareStatement(StrTool.GBKToUnicode(sql)
					,ResultSet.TYPE_FORWARD_ONLY
                    , ResultSet.CONCUR_READ_ONLY);
			tResultSet = tPreparedStatement.executeQuery();	
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return tResultSet;
    }
}
