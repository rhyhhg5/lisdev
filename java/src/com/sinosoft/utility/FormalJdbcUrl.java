/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;

/**
 * <p>ClassName: FormalJdbcUrl </p>
 * <p>Description: 构建 Jdbc 的 url </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @author: HST
 * @version: 1.0
 * @date: 2002-05-31
 */
public class FormalJdbcUrl {
    // @Field
    private String DBType;
    private String IP;
    private String Port;
    private String DBName;
    private String ServerName;
    private String UserName;
    private String PassWord;
    // @Constructor
    public FormalJdbcUrl() {
        //WebLogic连接池，其中MyPool为连接池的名称
        //DBType = "WEBLOGICPOOL";
        //DBName = "MyPool";

        //apache连接池
//    DBType="COMMONSDBCP";
//    DBName="DB2";

        /*  DBType = "INFORMIX";
             IP = "172.16.120.201";
             Port = "12000";
             DBName = "lis";
             ServerName = "server_intel";
             UserName = "informix";
             PassWord = "informix"; */

        //8.128
        //    DBType = "DB2";  DBName = "lis";
//    UserName = "db2inst1";
//    PassWord = "db2inst1";

        //70.73
//    DBType = "DB2";
//    IP = "10.252.3.8";
//    Port = "50000";
//    DBName = "lis";
//    UserName = "db2inst1";
//    PassWord = "db2inst1";

        //8.182
//    IP = "192.168.8.128";
//    Port = "50000";
//        外测  
//    	  DBType = "DB2";
//        IP = "10.136.10.101";
//        Port = "50000";
//        DBName = "lis";
//        UserName = "db2inst1";
//        PassWord = "db2inst1";
   
//    	  正式
    	DBType = "DB2";
        IP = "10.136.1.5";
        Port = "50000";
        DBName = "lis";
        UserName = "ghc";
        PassWord = "ghc&nzx71";
        
//        DBType = "DB2";
//        IP = "192.168.8.9";
//        Port = "50000";
//        DBName = "lis";
//        UserName = "db2inst1";
//        PassWord = "db2inst1";
//        PassWord = "sino2599";


//    System.out.println("hahahaha");

//        //WebSphere 连接池
//    DBType = "WEBSPHERE";
//    DBName = "PICCLIS";

//    DBType = "DB2";
//    IP = "192.168.71.244";
//    Port = "50000";
//    DBName = "lis";
//    UserName = "db2inst1";
//    PassWord = "test123";

//    DBType = "ORACLE";
//    IP = "192.168.71.244";
//    Port = "1521";
//    DBName = "lis";
//    UserName = "lis";
//    PassWord = "lis";

//5.7
//    DBType = "ORACLE";
//    IP = "10.0.5.7";
//    Port = "1531";
//    DBName = "lisdb";
//    UserName = "lisbak";
//    PassWord = "lisbak";

        //9080
//    DBType = "ORACLE";
//    IP = "10.0.22.136";
//    Port = "1521";
//    DBName = "lisdb";
//    UserName = "lis_test";
//    PassWord = "lis_test";

        //136l
//    DBType = "ORACLE";
//    IP = "10.0.22.136";
//    Port = "1521";
//    DBName = "lisdb";
//    UserName = "lis";
//    PassWord = "lis";

        //New
//    DBType = "ORACLE";
//    IP = "10.0.22.136";
//    Port = "1521";
//    DBName = "lisdb";
//    UserName = "lis_new";
//    PassWord = "lis_new";

        //8990
//    DBType = "ORACLE";
//    IP = "10.0.22.136";
//    Port = "1521";
//    DBName = "lisdb";
//    UserName = "lis_new";
//    PassWord = "lis_new";
    }

    // @Method
    public String getDBType() {
        return DBType;
    }

    public String getIP() {
        return IP;
    }

    public String getPort() {
        return Port;
    }

    public String getDBName() {
        return DBName;
    }

    public String getServerName() {
        return ServerName;
    }

    public String getUserName() {
        return UserName;
    }

    public String getPassWord() {
        return PassWord;
    }

    public void setDBType(String aDBType) {
        DBType = aDBType;
    }

    public void setIP(String aIP) {
        IP = aIP;
    }

    public void setPort(String aPort) {
        Port = aPort;
    }

    public void setDBName(String aDBName) {
        DBName = aDBName;
    }

    public void setServerName(String aServerName) {
        ServerName = aServerName;
    }

    public void setUser(String aUserName) {
        UserName = aUserName;
    }

    public void setPassWord(String aPassWord) {
        PassWord = aPassWord;
    }

    /**
     * 获取连接句柄
     * @return String
     */
    public String getJdbcUrl() {
        String sUrl = "";

        if (DBType.trim().toUpperCase().equals("ORACLE")) {
            sUrl = "jdbc:oracle:thin:@" + IP + ":"
                   + Port + ":"
                   + DBName;
        }

        if (DBType.trim().toUpperCase().equals("INFORMIX")) {
            sUrl = "jdbc:informix-sqli://" + IP + ":"
                   + Port + "/"
                   + DBName + ":"
                   + "informixserver=" + ServerName + ";"
                   + "user=" + UserName + ";"
                   + "password=" + PassWord + ";";
        }

        if (DBType.trim().toUpperCase().equals("SQLSERVER")) {
            sUrl = "jdbc:inetdae:" + IP + ":"
                   + Port + "?sql7=true&"
                   + "database=" + DBName + "&"
                   + "charset=gbk";
        }
        if (DBType.trim().toUpperCase().equals("WEBLOGICPOOL")) {
            sUrl = "jdbc:weblogic:pool:" + DBName;
        }

        if (DBType.trim().toUpperCase().equals("DB2")) {
            sUrl = "jdbc:db2://" + IP + ":"
                   + Port + "/"
                   + DBName;
        }
        return sUrl;
    }

}
