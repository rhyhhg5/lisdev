/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.utility;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 ****************************************************************
 *               Program NAME: 系统常量类
 *                 programmer: Ouyangsheng
 *                Create DATE: 2002.04.17
 *             Create address: Beijing
 *                Modify DATE:
 *             Modify address:
 *****************************************************************
 *
 *                    保存系统中的常量。
 *
 *****************************************************************
 */
public class SysConst {
    /* 系统信息 */
    public static final int FAILURE = -1;
    public static final int SUCCESS = 0;
    public static final int NOTFOUND = 100;

    /* 系统变量 */
    public static final String EMPTY = null;
    public static final boolean CHANGECHARSET = false; // Unicode to GBK

    /* 信息分隔符 */
    public static final String PACKAGESPILTER = "|";
    public static final String RECORDSPLITER = "^";
    public static final String ENDOFPARAMETER = "^";
    public static final String EQUAL = "=";
    public static final String CONTAIN = "*";

    /* 查询显示设置 */
    public static final int MAXSCREENLINES = 10; //每一页最大显示的行数
    public static final int MAXMEMORYPAGES = 20; //内存中存储的最大的页数

    /* 设置信息 */
    public static final String ZERONO = "00000000000000000000"; //对于没有号码的字段的默认值
    public static final String POOLINFO = "poolname";
    public static final String PARAMETERINFO = "parameterbuf";
    public static final String POOLTYPE = "pooltype";
    public static final String MAXSIZE = "maxsize";
    public static final String MINSIZE = "minsize";

    public static final String USERLOGPATH = "userlogpath";
    public static final String SYSLOGPATH = "syslogpath";

    public static final String COMP = "comp";
    public static final String ENCRYPT = "encrypt";
    public static final String MACFLAG = "macflag";
    public static final String SIGNFLAG = "signflag";
    public static final String SRC = "src";
    public static final String SND = "snd";
    public static final String RCV = "rcv";
    public static final String PRIOR = "prior";

    /* 交费间隔 */
    public static final String PayIntvMonth = "月交";
    public static final String PayIntvQuarter = "季交";
    public static final String PayIntvHalfYear = "半年交";
    public static final String PayIntvYear = "年交";

    /*建议书数据同步*/
    public static final int Number = 5000;

    /*报表系统保险公司编码*/
    public static final String CorpCode = "000085";

    /**
     * 一年的天数
     * 在PubFun的AccountManage中计算利息时用到
     */
    public static final String DAYSOFYEAR = "365";

    /*系统号码管理类型：SysMaxNo实现类的后缀，如民生的实现类为SysMaxNoMinSheng*/
    public static final String MAXNOTYPE = "Picch";

    /*数据库类型：DB2、ORACLE等*/
    public static final String DBTYPE = "DB2";
//    public static final String DBTYPE = "ORACLE";
    //大批量数据查询时，使用的缓冲区大小
    public static final int FETCHCOUNT = 5000;
    //给付责任初步筛选：初步筛选出客户所有的给付责任和给付责任给付
    public static String GETDUTYGET = "GetDutyGetImpl";

    //自动责任匹配
    public static String AUTOCHOOSEDUTY = "AutoClaimDutyMapImpl";

    //工作目录
    public final static String WorkPath = "D:\\workspace\\UI\\";

    /**数据库操作方式， 插入*/
    public final static String INSERT = "INSERT";
    /**数据库操作方式， 修改*/
    public final static String UPDATE = "UPDATE";
    /**数据库操作方式， 删除*/
    public final static String DELETE = "DELETE";
    /**数据库操作方式， 删除后插入*/
    public final static String DELETE_AND_INSERT = "DELETE&INSERT";

    public static int MaxRecordSize;

    public static int IfDeleteError;  //无条件删除报错：1
    public static int IfNullQueryError;  //无条件查询报错：1
    public static int IfLargeQueryError;  //大记录查询报错：1
    public static int LoginFailCount;//允许登陆失败的次数 090706刘莉
    /**xml数据块开始标志*/
    public static String TEG_START = "TEG_START";
    /**xml数据块结束标志*/
    public static String TEG_END = "TEG_END";
//  zhangjinquan 2008-08-26 新增保全操作锁类型
    public static String BQ_LOCK_TYPE = "BQ";

    static {
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet rs = null;

    	try {
    		// 设置缺省值
    		MaxRecordSize = 10000;
    		IfDeleteError = 1;
    		IfNullQueryError = 1;
    		IfLargeQueryError = 1;
                LoginFailCount=5;

    		conn = DBConnPool.getConnection();
    		stmt = conn.createStatement();

    		// 取MaxRecordSize
    		rs = stmt.executeQuery("SELECT * FROM LDSYSVAR WHERE SYSVAR = 'MaxRecordSize'");

    		if(rs.next()) {
    			MaxRecordSize = Integer.parseInt(rs.getString("SYSVARVALUE").trim());
    		}

    		rs.close();		rs = null;

    		// 取IfDeleteError
    		rs = stmt.executeQuery("SELECT * FROM LDSYSVAR WHERE SYSVAR = 'IfDeleteError'");

    		if(rs.next()) {
    			IfDeleteError = Integer.parseInt(rs.getString("SYSVARVALUE").trim());
    		}

    		rs.close();		rs = null;

    		// 取IfNullQueryError
    		rs = stmt.executeQuery("SELECT * FROM LDSYSVAR WHERE SYSVAR = 'IfNullQueryError'");

    		if(rs.next()) {
    			IfNullQueryError = Integer.parseInt(rs.getString("SYSVARVALUE").trim());
    		}

    		rs.close();		rs = null;

    		// 取IfLargeQueryError
    		rs = stmt.executeQuery("SELECT * FROM LDSYSVAR WHERE SYSVAR = 'IfLargeQueryError'");

    		if(rs.next()) {
    			IfLargeQueryError = Integer.parseInt(rs.getString("SYSVARVALUE").trim());
    		}

    		rs.close();		rs = null;

                // 取LoginFailCount
                rs = stmt.executeQuery("SELECT * FROM LDSYSVAR WHERE SYSVAR = 'LoginFailCount'");

                if(rs.next()) {
                        LoginFailCount = Integer.parseInt(rs.getString("SYSVARVALUE").trim());
                }

                rs.close();		rs = null;


    		stmt.close();	stmt = null;
    		conn.close();	conn = null;

    	} catch (Exception ex) {
    		ex.printStackTrace();
    		if( rs != null ) {
    			try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
    		}

    		if( stmt != null ) {
    			try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
    		}

    		if( conn != null ) {
    			try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
    		}
    	}
    }
}


