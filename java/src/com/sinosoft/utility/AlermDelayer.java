package com.sinosoft.utility;

import java.util.Date;

public class AlermDelayer {

	/**
	 * @param args
	 */
	private static final double minuteTimeMill = 200;
	private static final double min0_timeMill = 0*minuteTimeMill;
	private static final double min2_timeMill = 2*minuteTimeMill;
	private static final double min4_timeMill = 4*minuteTimeMill;
	private static final double min8_timeMill = 8*minuteTimeMill;
	private static final double min16_timeMill = 16*minuteTimeMill;
	private static final double min32_timeMill = 32*minuteTimeMill;
	private static final double min64_timeMill = 64*minuteTimeMill;
	
	private double firstAlarmTimeMill ;
	private boolean hasAlarmed_0 ;
	private boolean hasAlarmed_2 ;
	private boolean hasAlarmed_4 ;
	private boolean hasAlarmed_8 ;
	private boolean hasAlarmed_16 ;
	private boolean hasAlarmed_32 ;
	private boolean hasAlarmed_64 ;
	
	private String errorMessage;
	private String className;
	private String methodName;
	
	//单例对象
	private static AlermDelayer AlarmDelayer = null;
		
	//获取单例对象的方法
	public static AlermDelayer getAlarmDelayer(){
		return getmAlermDelayer();
	}
	
	static {
		AlarmDelayer = new AlermDelayer("errorMessage","className","methodName",System.currentTimeMillis());
	}
	private AlermDelayer(){
	}
	private AlermDelayer(String errorMessage,String className,String methodName,long currentTimeMill){
		AlarmDelayer = new AlermDelayer();
		AlarmDelayer.hasAlarmed_0 = false;
		AlarmDelayer.hasAlarmed_2 = false;
		AlarmDelayer.hasAlarmed_4 = false;
		AlarmDelayer.hasAlarmed_8 = false;
		AlarmDelayer.hasAlarmed_16 = false;
		AlarmDelayer.hasAlarmed_32 = false;
		AlarmDelayer.hasAlarmed_64 = false;
		AlarmDelayer.errorMessage = errorMessage;
		AlarmDelayer.className = className;
		AlarmDelayer.methodName = methodName;
		AlarmDelayer.firstAlarmTimeMill = System.currentTimeMillis() ;
	}
	
	public static boolean isAllowAlarm(String errorMessage,String className,String methodName,long currentTimeMill) {
		//第一次报警
		if(AlarmDelayer.isHasAlarmed_0()==false){
			System.out.println("alarm!");
			AlarmDelayer.firstAlarmTimeMill = System.currentTimeMillis();
			AlarmDelayer.setHasAlarmed_0(true);
			return true;
		}
		//之前已经报过警，判断在哪个时间范围，该时间范围是否报过警
		double time = currentTimeMill-AlarmDelayer.firstAlarmTimeMill;
		if(time>min0_timeMill && time<min2_timeMill && AlarmDelayer.isHasAlarmed_2()==false){
			System.out.println("alarm!");
			if(AlarmDelayer.isHasAlarmed_0()==false){
				resetDelayer();
			}
			AlarmDelayer.setHasAlarmed_2(true);
			return true;
		}else if(time>min2_timeMill && time<min4_timeMill && AlarmDelayer.isHasAlarmed_4()==false){
			System.out.println("alarm!");
			if(AlarmDelayer.isHasAlarmed_2()==false){
				resetDelayer();
			}
			AlarmDelayer.setHasAlarmed_4(true);
			return true;
		}else if(time>min4_timeMill && time<min8_timeMill && AlarmDelayer.isHasAlarmed_8()==false){
			System.out.println("alarm!");
			if(AlarmDelayer.isHasAlarmed_4()==false){
				resetDelayer();
			}
			AlarmDelayer.setHasAlarmed_8(true);
			return true;
		}else if(time>min8_timeMill && time<min16_timeMill && AlarmDelayer.isHasAlarmed_16()==false){
			System.out.println("alarm!");
			if(AlarmDelayer.isHasAlarmed_8()==false){
				resetDelayer();
			}
			AlarmDelayer.setHasAlarmed_16(true);
			return true;
		}else if(time>min16_timeMill && time<min32_timeMill && AlarmDelayer.isHasAlarmed_32()==false){
			System.out.println("alarm!");
			if(AlarmDelayer.isHasAlarmed_16()==false){
				resetDelayer();
			}
			AlarmDelayer.setHasAlarmed_32(true);
			return true;
		}else if(time>min32_timeMill && time<min64_timeMill && AlarmDelayer.isHasAlarmed_64()==false){
			System.out.println("alarm!");
			if(AlarmDelayer.isHasAlarmed_32()==false){
				resetDelayer();
			}
			AlarmDelayer.setHasAlarmed_64(true);
			return true;
		}else if(time>min64_timeMill){
			System.out.println("alarm!");
			resetDelayer();
			return true;
		}
		return false;
	}
	
	private static void resetDelayer(){
		AlarmDelayer.setFirstAlarmTimeMill(System.currentTimeMillis()); 
		AlarmDelayer.setHasAlarmed_0(false); 
		AlarmDelayer.setHasAlarmed_2(false); 
		AlarmDelayer.setHasAlarmed_4(false); 
		AlarmDelayer.setHasAlarmed_8(false); 
		AlarmDelayer.setHasAlarmed_16(false); 
		AlarmDelayer.setHasAlarmed_32(false); 
		AlarmDelayer.setHasAlarmed_64(false); 
	}
	
	
	public static void main(String[] args){
//		test
		AlermDelayer tAlermDelayer = AlermDelayer.getAlarmDelayer();
		tAlermDelayer.test();
//		使用demo
//		AlermDelayer.getAlarmDelayer().isAllowAlarm("test Alartm "+tDate.toString(),"","",System.currentTimeMillis());
	}

	private void test() {
		for(;;){
			Date tDate = new Date();
			isAllowAlarm("test Alartm "+tDate.toString(),"","",System.currentTimeMillis());
		}
	}


	private void setFirstAlarmTimeMill(double firstAlarmTimeMill) {
		this.firstAlarmTimeMill = firstAlarmTimeMill;
	}

	private boolean isHasAlarmed_0() {
		return hasAlarmed_0;
	}

	private void setHasAlarmed_0(boolean hasAlarmed_0) {
		this.hasAlarmed_0 = hasAlarmed_0;
	}

	private boolean isHasAlarmed_2() {
		return hasAlarmed_2;
	}

	private void setHasAlarmed_2(boolean hasAlarmed_2) {
		this.hasAlarmed_2 = hasAlarmed_2;
	}

	private boolean isHasAlarmed_4() {
		return hasAlarmed_4;
	}

	private void setHasAlarmed_4(boolean hasAlarmed_4) {
		this.hasAlarmed_4 = hasAlarmed_4;
	}

	private boolean isHasAlarmed_8() {
		return hasAlarmed_8;
	}

	private void setHasAlarmed_8(boolean hasAlarmed_8) {
		this.hasAlarmed_8 = hasAlarmed_8;
	}

	private boolean isHasAlarmed_16() {
		return hasAlarmed_16;
	}

	private void setHasAlarmed_16(boolean hasAlarmed_16) {
		this.hasAlarmed_16 = hasAlarmed_16;
	}

	private boolean isHasAlarmed_32() {
		return hasAlarmed_32;
	}

	private void setHasAlarmed_32(boolean hasAlarmed_32) {
		this.hasAlarmed_32 = hasAlarmed_32;
	}

	private boolean isHasAlarmed_64() {
		return hasAlarmed_64;
	}

	private void setHasAlarmed_64(boolean hasAlarmed_64) {
		this.hasAlarmed_64 = hasAlarmed_64;
	}

	private static AlermDelayer getmAlermDelayer() {
		return AlarmDelayer;
	}

}
