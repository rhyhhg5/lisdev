package com.sinosoft.utility;

import com.sinosoft.lis.schema.LMCalModeSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

public class CheckAppAmout {
  public CheckAppAmout() {
  }

  //计算风险保额
  public static double getAmount(String tcontno, String insuredno) {
    ExeSQL tExeSQL = new ExeSQL();
    LMCalModeSchema tLMCalModeSchema;
    String end = "0";
    double fMount = 0;
    double tSumMount = 0;
    String sql = "select count(*) from  lmcalmode"
        + " where type = 'H' and riskcode in "
        + "(select distinct riskcode "
        + " from lcpol where contno = '" + tcontno + "')";
    end = tExeSQL.getOneValue(sql);
    if (Integer.parseInt(end) > 0) {
      LMCalModeDB tLMCalModeDB = new LMCalModeDB();
      LMCalModeSet tLMCalModeSet = new LMCalModeSet();
      tLMCalModeDB.setType("H");
      tLMCalModeSet = tLMCalModeDB.query();
      if (tLMCalModeSet.size() != 0) {
        tLMCalModeSchema = new LMCalModeSchema();
        for (int i = 1; i <= tLMCalModeSet.size(); i++) {
          //取出其中的一条计算公式
          tLMCalModeSchema = tLMCalModeSet.get(i);
          Calculator mCalculator = new Calculator();

          mCalculator.setCalCode(tLMCalModeSchema.getCalCode());
          mCalculator.addBasicFactor("insuredno", insuredno);
          String tStr = "";
          //通过Calculator计算公式结果
          tStr = mCalculator.calculate();
          if (tStr == null || tStr.trim().equals(""))
           fMount = 0;
          else
            fMount = Double.parseDouble(tStr);
          //每次计算结果的累加
          tSumMount = tSumMount + fMount;
        }
      }
      else {
        tSumMount = 0;
      }
    }
    else {
      tSumMount = 0;
    }
    return tSumMount;
  }

  public static void main(String[] args) {
  }
}
