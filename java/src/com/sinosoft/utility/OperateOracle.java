package com.sinosoft.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LYOutPayDetailSchema;
import com.sinosoft.lis.vschema.LYOutPayDetailSet;


public class OperateOracle {

    // 定义连接所需的字符串
    // 192.168.0.X是本机地址(要改成自己的IP地址)，1521端口号，XE是精简版Oracle的默认数据库名
    private static String USERNAMR = "cbspdp";
    private static String PASSWORD = "cbspdp";
    private static String DRVIER = "oracle.jdbc.OracleDriver";
    private static String URL = "jdbc:oracle:thin:@10.136.5.125:1521:orcl";//外侧数据库
    //private static String URL = "jdbc:oracle:thin:@10.136.5.100:1521:orcl";//正式数据库
    private MMap resultMMap = new MMap();

    // 创建一个数据库连接
    Connection connection = null;
    // 创建预编译语句对象，一般都是用这个而不用Statement
    PreparedStatement pstm = null;
    // 创建一个结果集对象
    ResultSet rs = null;

    /**
     * 向数据库中增加数据
     */
    public void AddData(LYOutPayDetailSet mLYOutPayDetailSet,SSRS mSSRS) {
    	  LYOutPayDetailSet resLYOutPayDetailSet=new LYOutPayDetailSet();
        connection = getConnection();
        String sqlStr = "insert into oracle.BIZ_CORE (ID,LSH,DDBH,HTBH,GMF_SFZJLX,GMF_SFZJHM,GMF_XM,GMF_SJH,GMF_DZYX,BMB_BBH,ZSFS,KPLX,XSF_NSRSBH,XSF_MC,XSF_DZDH,XSF_YHZH,GMF_NSRSBH,GMF_MC,GMF_DZDH,GMF_YHZH,KPR,SKR,FHR,FP_DM,FP_HM,KPRQ,JYM,YFP_DM,YFP_HM,JSHJ,HJJE,HJSE,KCE,BZ,FPHXZ,SPBM,ZXBM,YHZCBS,LSLBS,ZZSTSGL,XMMC,DW,GGXH,XMSL,XMDJ,XMJE,SL,SE,CREATE_TIME,UPDATE_TIME,STATE,MSG) values(ORACLE.BIZ_CORE_SEQUENCE.nextVal,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,to_number(?),to_number(?),to_number(?),?,?,?,?,?,to_number(?),to_number(?),?,?,?,?,to_number(?),to_number(?),to_number(?),to_number(?),to_number(?),TO_DATE(?,'yyyy-MM-dd hh24:mi:ss'),TO_DATE(?,'yyyy-MM-dd hh24:mi:ss'),?,?)";
        try {
        	  pstm = connection.prepareStatement(sqlStr);
        	   for(int i=1;i<=mLYOutPayDetailSet.size();i++){
               	LYOutPayDetailSchema detailSchema= mLYOutPayDetailSet.get(i);
               try {
            	   String XMMC = null ;
               	   if(mSSRS!=null&&mSSRS.getMaxRow()>0){
               		   for(int j=1; j<=mSSRS.getMaxRow(); j++){
               		   if(mSSRS.GetText(j, 1).equals(detailSchema.getprocode())&&mSSRS.GetText(j, 2).equals(detailSchema.getbusitype())){
               			XMMC = mSSRS.GetText(j, 3);
               			break;
               		   }
               		   }
               	   }
            	  
                   // 执行插入数据操作
                  // pstm.setString(1, ORACLE.BIZ_CORE_SEQUENCE.nextVal);
                   pstm.setString(1, detailSchema.getBusiNo());
                   pstm.setString(2, null);
                   pstm.setString(3, detailSchema.getvdef2());//保单号
                   pstm.setString(4, detailSchema.getbuyidtype());//投保人证件类型
                   pstm.setString(5, detailSchema.getbuyidnumber());//投保人证件号码
                   pstm.setString(6, detailSchema.getcustname());//投保人姓名
                   pstm.setString(7, null);
                   pstm.setString(8, "myjf@picchealth.com");//投保人电子邮箱
                   pstm.setString(9, "1.0");
                   pstm.setString(10, "0");
                   pstm.setString(11, "0");
                   pstm.setString(12, "91440300781368528U");
                   pstm.setString(13, "中国人民健康保险股份有限公司深圳分公司");
                   pstm.setString(14, "广东省深圳市南山区科技园科苑路11号金融科技大厦14楼C单元 0755-83813999");
                   pstm.setString(15, "招商银行安联支行 814781146310001");
                   pstm.setString(16, null);
                   pstm.setString(17,detailSchema.getcustname());//投保人姓名
                   pstm.setString(18, null);
                   pstm.setString(19, null);
                   pstm.setString(20, "黄晓柳");
                   pstm.setString(21, "韦知");
                   pstm.setString(22, "甘戈");
                   pstm.setString(23, null);
                   pstm.setString(24, null);
                   pstm.setString(25, null);
                   pstm.setString(26, null);
                   pstm.setString(27, null);
                   pstm.setString(28, null);
                   pstm.setString(29, detailSchema.gettranamt());//核心保费
                   pstm.setString(30, detailSchema.getoriamt());//核心不含税金额
                   pstm.setString(31, detailSchema.getoritax());//核心税额
                   pstm.setString(32, null);
                   pstm.setString(33, detailSchema.getvdef1());//保单号；险种名称
                   pstm.setString(34, "0");
                   pstm.setString(35, "3060301010000000000");
                   pstm.setString(36, null);
                   pstm.setString(37, "0");//
                   pstm.setString(38, null);//
                   pstm.setString(39, null);
                   pstm.setString(40, XMMC);//zxs//核心配置
                   pstm.setString(41, null);
                   pstm.setString(42, null);
                   pstm.setString(43, "1");//
                   pstm.setString(44, detailSchema.getoriamt());//核心不含税金额
                   pstm.setString(45, detailSchema.getoriamt());//核心不含税金额
                   pstm.setString(46, detailSchema.gettaxrate());//核心税率
                   pstm.setString(47, detailSchema.getoritax());//核心税额
                   pstm.setString(48, PubFun.getCurrentDate()+" " + PubFun.getCurrentTime());//to_date(to_char(time, 'yyyy-MM-dd'), 'yyyy-mm-dd')
                   pstm.setString(49, PubFun.getCurrentDate()+" " + PubFun.getCurrentTime());
                   pstm.setString(50, "0");//
                   pstm.setString(51, null);
                  System.out.println("pstm=:"+pstm);
                   pstm.executeUpdate();
                   String sql = "insert into oracle.BIZ_CORE values(ORACLE.BIZ_CORE_SEQUENCE.nextVal,'"+detailSchema.getBusiNo()+"',null,'"+detailSchema.getvdef2()+"','"+detailSchema.getbuyidtype()+"','"+detailSchema.getbuyidnumber()+"','"+detailSchema.getcustname()+"',null,'myjf@picchealth.com','1.0','0','0','91440300781368528U','中国人民健康保险股份有限公司深圳分公司','广东省深圳市南山区科技园科苑路11号金融科技大厦14楼C单元 0755-83813999','招商银行安联支行 814781146310001',null,'"+detailSchema.getcustname()+"',null,null,'黄晓','韦知','甘戈',null,null,null,null,null,null,to_number('"+detailSchema.gettranamt()+"'),to_number('"+detailSchema.getoriamt()+"'),to_number('"+detailSchema.getoritax()+"'),null,'"+detailSchema.getvdef1()+"','0','3060301010000000000',null,to_number('0'),null,null,'"+XMMC+"',null,null,to_number('1'),to_number('"+detailSchema.getoriamt()+"'),to_number('"+detailSchema.getoriamt()+"'),to_number('"+detailSchema.gettaxrate()+"'),to_number('"+detailSchema.getoritax()+"'),TO_CHAR('"+PubFun.getCurrentDate()+" " + PubFun.getCurrentTime()+"','YYYY_MM_DD HH:mm:ss'),TO_CHAR('"+PubFun.getCurrentDate()+" " + PubFun.getCurrentTime()+"','YYYY_MM_DD HH:mm:ss'),'0',null)";                   System.out.println("sql=:"+sql);
                   detailSchema.setstate("02");
                   //pstm.execute(sql);
               } catch (SQLException e) {
               	detailSchema.setstate("03");
                   e.printStackTrace();
               } 
               System.out.println("detailSchema.getstate()="+detailSchema.getstate());
               if(!"03".equals(detailSchema.getstate())){
               	detailSchema.setstate("02");
                resLYOutPayDetailSet.add(detailSchema);
               }
             
               }
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
            ReleaseResource();
        }
     
       
		resultMMap.put(resLYOutPayDetailSet,SysConst.INSERT);
    }
    
    //返回数据
    public MMap getResult(){
		return resultMMap;
	}
    /**
     * 获取Connection对象
     * 
     * @return
     */
    public Connection getConnection() {
        try {
            Class.forName(DRVIER);
            connection = DriverManager.getConnection(URL, USERNAMR, PASSWORD);
            System.out.println("成功连接数据库");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("class not find !", e);
        } catch (SQLException e) {
            throw new RuntimeException("get connection error!", e);
        }

        return connection;
    }

    /**
     * 释放资源
     */
    public void ReleaseResource() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (pstm != null) {
            try {
                pstm.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}