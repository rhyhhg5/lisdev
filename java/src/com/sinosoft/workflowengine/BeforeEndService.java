package com.sinosoft.workflowengine;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.utility.*;

  public interface BeforeEndService {
  public boolean submitData(VData cInputData, String cOperate);
  public VData getResult();
  public TransferData getReturnTransferData();
  public CErrors getErrors();
}