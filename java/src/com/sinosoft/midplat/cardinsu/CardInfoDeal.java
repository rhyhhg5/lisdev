package com.sinosoft.midplat.cardinsu;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
//import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CardInfoDeal
{
    private final static Logger cLogger = Logger.getLogger(CardInfoDeal.class);

    private final Document cInXmlDoc;

    private String mDealState = null;

    private StringBuffer mErrInfo = new StringBuffer();

    /** 报文处理成功 */
    public final static String DEAL_SUCC = new String("00");

    /** 报文处理失败 */
    public final static String DEAL_FAIL = new String("01");

    // ----- 卡单中所需数据 -----
    private String mCardNo = null;

    private String mCValidate = null;

    private String mActiveDate = null;

    private String mOperator = null;
    
    private String mBak1 = null;

    private String mBak2 = null;

    private String mBak3 = null;

   

    // -------------------------

    public CardInfoDeal(Document pInXmlDoc)
    {
        cInXmlDoc = pInXmlDoc;
    }

    public Document deal()
    {
        cLogger.info("Into BusinessDeal.deal()...");

        if (!dealCardXml())
        {
            this.mDealState = CardInfoDeal.DEAL_FAIL;
        }
        else
        {
            this.mDealState = CardInfoDeal.DEAL_SUCC;
        }

        // 以报文形式，返回处理结果
        Document tDealResultDoc = createDealResultDoc();
        // --------------------

        cLogger.info("Out BusinessDeal.deal()!");

        return tDealResultDoc;
    }

    /**
     * 处理接收到的报文xml
     * @return
     */
    private boolean dealCardXml()
    {
        // 装载报文中业务数据。
        if (!loadData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkLogicData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkBaseData())
        {
            return false;
        }
        // --------------------

        // 处理业务数据
        if (!dealData())
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean loadData()
    {
        Document tTmpDoc = this.cInXmlDoc;

        if (tTmpDoc == null)
        {
            return false;
        }

        Element tRootData = tTmpDoc.getRootElement();

        Element tContTable = tRootData.getChild("CONTTABLE");
        if (tContTable == null)
        {
            errLog("无保单数据");
            return false;
        }

        this.mCardNo = tContTable.getChildTextTrim("CARDNO");
        this.mCValidate = tContTable.getChildTextTrim("CVALIDATE");
        this.mActiveDate = tContTable.getChildTextTrim("ACTIVEDATE");
        this.mOperator = tContTable.getChildTextTrim("OPERATOR");
        

        this.mBak1 = tContTable.getChildTextTrim("BAK1");
        this.mBak2 = tContTable.getChildTextTrim("BAK2");
        this.mBak3 = tContTable.getChildTextTrim("BAK3");

        return true;
    }

    /**
     * 校验报文数据是否完整。
     * @return
     */
    private boolean checkBaseData()
    {
        if (this.mCardNo == null || this.mCardNo.equals(""))
        {
            errLog("卡号为空");
            return false;
        }

        if (this.mCValidate == null || this.mCValidate.equals(""))
        {
            errLog("单证生效日期为空");
            return false;
        }

        if (this.mActiveDate == null || this.mActiveDate.equals(""))
        {
            errLog("单证激活日期为空");
            return false;
        }

        if (this.mOperator == null || this.mOperator.equals(""))
        {
            errLog("单证操作员为空");
            return false;
        }

        return true;
    }

    /**
     * 校验业务逻辑
     * @return
     */
    private boolean checkLogicData()
    {
        return true;
    }

    private boolean dealData()
    {
        if (!dealCard())
        {
            return false;
        }
        return true;
    }

    /**
     * 保险卡处理类别：01－报文中明确提供卡号密码
     * @return
     */
    private boolean dealCard()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 校验卡号是否在核心进行了录入以及日期格式是否符合格式
        if (!chkCardUsed())
        {
            return false;
        }

        // 处理核心数据
        tTmpMap = null;
        tTmpMap = dealWebCardDatas();
        if (tTmpMap == null)
        {
            return false;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        try
        {
            if (!submit(tMMap))
            {
                errLog("提交数据失败");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            errLog("提交数据失败");
            return false;
        }

        return true;
    }
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap cMMap)
    {
        VData data = new VData();
        data.add(cMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            errLog("提交数据失败");
            return false;
        }

        return true;
    }

    /**
     * 创建返回报文。
     * @return
     */
    private Document createDealResultDoc()
    {
        Element tCardNo = new Element("CARDNO");
        tCardNo.setText(this.mCardNo);

        Element tState = new Element("STATE");
        tState.setText(this.mDealState);

        Element tErrCode = new Element("ERRCODE");
        tErrCode.setText("");

        Element tErrInfo = new Element("ERRINFO");
        tErrInfo.setText(this.mErrInfo.toString());

        Element tContInfo = new Element("CONTINFO");
        tContInfo.addContent(tCardNo);
        tContInfo.addContent(mCValidate);
        tContInfo.addContent(mActiveDate);
        tContInfo.addContent(tState);
        tContInfo.addContent(tErrCode);
        tContInfo.addContent(tErrInfo);

        Element mRootData = new Element("DATASET");
        mRootData.addContent(this.mCardNo);
        mRootData.addContent(tContInfo);

        Document tDocDealResult = new Document(mRootData);
//        JdomUtil.print(tDocDealResult);

        return tDocDealResult;
    }

    /**
     * 校验单证是否在核心系统录入
     * @return
     */
    private boolean chkCardUsed()
    {
        String tCardNo = mCardNo;

        String tStrSql = null;
        String tResult = null;

        // 判断CardNo是否存在或可以使用。
        tStrSql = " select 1 from licertify where Cardno = '" + tCardNo + "' ";
        tResult = new ExeSQL().getOneValue(tStrSql);
        if (!"1".equals(tResult))
        {
            errLog("该单证还没有在核心进行录入");
            return false;
        }
        
        Date actDate = new FDate().getDate(mActiveDate);
        if (actDate == null)
        {
            errLog("系统激活时间不符合YYYY-MM-DD格式。");
            return false;
        }
        
        Date cvaDate = new FDate().getDate(this.mCValidate);
        if (cvaDate == null)
        {
            errLog("系统激活时间不符合YYYY-MM-DD格式。");
            return false;
        }

        // ---------------------------

        return true;
    }

    /**
     * 处理网站保险卡数据
     * <br />1、将保险卡信息（包括：保险卡卡号、密码、相关被保人信息等）保存到 Card_Used 表中。
     * <br />2、删除未使用卡表 Card_New 中该保险卡记录。
     * @return
     */
    private MMap dealWebCardDatas()
    {
        MMap tMMap = new MMap();
        String tStrSql = null;

        // 网站业务表中对应数据。
        HashMap tFieldAndValues = new HashMap();
        tFieldAndValues.put("CardNo", this.mCardNo);
        tFieldAndValues.put("CValidate", this.mCValidate);
        tFieldAndValues.put("ActiveDate", this.mActiveDate);
        tFieldAndValues.put("Operator", this.mOperator);
        
        Date tCValidate = calCValidate(this.mCValidate,this.mActiveDate);
        // 删除未使用表中该卡号数据
        tStrSql = " update LICertify set cvalidate = '"+tCValidate+"',activedate = '"+this.mActiveDate+"' where CardNO = '" + this.mCardNo + "' ";
        tMMap.put(tStrSql, "UPDATE");
        tStrSql = null;
        // ----------------------------------------------

        return tMMap;
    }


    /**
     * 根据激活日期计算生效日期。
     * <br />激活日期次日零时，为保单生效日期。
     * @param cActiveDate 激活日期
     * @return
     */
    private Date calCValidate(String strCValidate,String strActiveDate)
    {
        Date tResCValidate = null;

        FDate tFDate = new FDate();
        Date tCValidate = tFDate.getDate(strCValidate);
        Date tActiveDate = tFDate.getDate(strActiveDate);

        if (tActiveDate == null)
        {
            return null;
        }

        if (tCValidate != null && tCValidate.after(tActiveDate))
        {
            tResCValidate = tCValidate;
        }
        else
        {
            int tIntervalDays = 1;
            Calendar tParamDate = Calendar.getInstance();
            try
            {
                tParamDate.setTime(tActiveDate);
                tParamDate.add(Calendar.DATE, tIntervalDays);
                tResCValidate = tParamDate.getTime();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }

        return tResCValidate;
    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        cLogger.error(cErrInfo);
        this.mErrInfo.append(cErrInfo);
    }
}
