package com.sinosoft.midplat.chinamobile;

import org.apache.log4j.Logger;
import org.jdom.Document;

public class BusinessDeal {
	private final static Logger cLogger = Logger.getLogger(BusinessDeal.class);
	
	private final Document cInXmlDoc;
	
	public BusinessDeal(Document pInXmlDoc) {
		cInXmlDoc = pInXmlDoc;
	}
	
	public Document deal() {
		cLogger.info("Into BusinessDeal.deal()...");
		
		cLogger.info("Out BusinessDeal.deal()!");
		return cInXmlDoc;
	}
}
