package com.sinosoft.midplat.kernel;

import java.io.File;
import java.io.IOException;

import org.jdom.Element;

import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.common.SysInfo;
import com.sinosoft.midplat.common.XmlConf;
import com.sinosoft.midplat.common.cache.FileCacheManage;

public class FeeRateConf extends XmlConf {
	private static FeeRateConf cThisIns = new FeeRateConf();
	
	private String cPath = "conf/BOC_H_FeeRate.xml";
	
	private FeeRateConf() {
		load();
		FileCacheManage.newInstance().register(cPath, this);
	}
	
	public void load() {
		cLogger.info("Into MidplatSufConf.load()...");
		
		String mFilePath = SysInfo.cHome + cPath;
//		String mFilePath = "D:\\picch\\picch_suf\\WebRoot\\WEB-INF\\conf\\BOC_H_FeeRate.xml";
		cLogger.info("Start load " + mFilePath + "...");
		
		cConfFile = new File(mFilePath);
		
		/**
		 * 一定要在加载之前记录文件属性。
		 * 文件的加载到文件属性设置之间存在细微的时间差，
		 * 如果恰巧在此时间差内外部修改了文件，
		 * 那么记录的数据就是新修改后的，导致这次修改不会自动被加载；
		 * 将文件属性设置放在加载之前，就算在时间差内文件发生改变，
		 * 由于记录的是旧的属性，系统会在下一个时间单元重新加载，
		 * 这样顶多会导致同一文件多加载一次，但不会出现修改而不被加载的bug。
		 */
		recordStatus();
		
		cConfDoc = loadXml(cConfFile);
		cLogger.info("End load " + mFilePath + "!");
		
		//是否输出配置文件
		boolean mOut = true;
		if (null != FeeRateConf.newInstance()) {
			Element tConfLoad =
				FeeRateConf.newInstance().getConf().getRootElement().getChild("confLoad");
			if (null != tConfLoad 
				&& "false".equals(tConfLoad.getAttributeValue("out"))) {
				mOut = false;
			}
		}
		if (mOut) {
			try {
				cLogger.info(
						new String(JdomUtil.toBytes(cConfDoc), "GBK"));
			} catch (IOException ex) {
				cLogger.error("输出配置文件异常！", ex);
			}
		}
		
		//重设配置文件加载间隔
		Element mConfLoad =
			cConfDoc.getRootElement().getChild("confLoad");
		if (null != mConfLoad) {
			String tSleepSecond = 
				mConfLoad.getAttributeValue("sleepSecond");
			try {
				FileCacheManage.newInstance().setSleepSecond(
						Integer.parseInt(tSleepSecond));
			} catch (Exception ex) {	//使用加载间隔
				cLogger.warn("配置文件加载间隔，或配置有误！" + ex);
			}
		}
		
		cLogger.info("Out MidplatSufConf.load()!");
	}
	
	public static FeeRateConf newInstance() {
		return cThisIns;
	}
}
