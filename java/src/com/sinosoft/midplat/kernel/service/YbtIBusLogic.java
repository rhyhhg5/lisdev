/**
 * 2010-8-23
 */
package com.sinosoft.midplat.kernel.service;

import com.cbsws.core.xml.ctrl.MsgCollection;

/**
 * @author LY
 *
 */
public interface YbtIBusLogic
{
    public MsgCollection service(MsgCollection cMsgInfos, int mm);
}
