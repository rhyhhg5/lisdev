package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.VData;

public class BatResult extends ServiceImpl {

	public BatResult(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into BatResult.service()...");
		Document mOutXmlDoc = null;

		try {
			updateTransLog(pInXmlDoc);

			// 清除提盘后生成的数据，以便重提
			Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);
			String mSerialNo = mLCCont.getChildText("SerialNo");
			MMap mMMap = new MMap();
			mMMap.put("delete from lysendtobank where serialno = '" + mSerialNo
					+ "' with ur", "DELETE");
			mMMap.put("delete from lybanklog where SerialNo = '" + mSerialNo
					+ "' with ur", "DELETE");
			VData mVData = new VData();
			mVData.add(mMMap);
			PubSubmit mPubSubmit = new PubSubmit();
			if (!mPubSubmit.submitData(mVData, "")) {
				cLogger.error(mPubSubmit.mErrors.getFirstError());
				throw new MidplatException("清除提盘数据失败！");
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");

		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out BatResult.service()!");
		return mOutXmlDoc;
	}

	public void updateTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into BatResult.updateTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mOldLKTransStatusDB = new LKTransStatusDB();
		mOldLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mOldLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mOldLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mOldLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		if (!mOldLKTransStatusDB.getInfo()) {
			throw new MidplatException("查找交易失败！");
		}
		mOldLKTransStatusDB.setRCode("0");
		mOldLKTransStatusDB.setTransStatus("0");
		mOldLKTransStatusDB.setDescr("上传文件失败," + mBaseInfo.getChildText(Desc));
		mOldLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mOldLKTransStatusDB.update()) {
			cLogger.error("更新日志信息失败！"
					+ mOldLKTransStatusDB.mErrors.getFirstError());
		}

		cLogger.info("Out BatResult.updateTransLog()!");
	}
}
