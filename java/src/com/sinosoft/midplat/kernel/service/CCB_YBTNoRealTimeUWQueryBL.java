package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKNoRealTimeUWCheckDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_YBTNoRealTimeUWQueryBL  extends ServiceImpl {

	//得到系统当前时间
	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	
	public CCB_YBTNoRealTimeUWQueryBL(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_YBTNoRealTimeUWQueryBL.service()...");
		LKTransStatusDB mLKTransStatusDB = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			Element mDetails = pInXmlDoc.getRootElement().getChild("Details");
			mOutXmlDoc = getResultDis(mDetails);  //处理数据、返回报文
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_YBTNoRealTimeUWQueryBL.service()...");
		return mOutXmlDoc ;
	}
	
	public Document getResultDis(Element details) throws MidplatException {
		
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		
		Element mDetailList = new Element("DetailList");
		Element mDetailCount = new Element("DetailCount");
		
		
		List detailList = details.getChildren("Detail");
		
		mDetailCount.setText(detailList.size()+"");
		mFlag.setText("0");
		mDesc.setText("交易成功!");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		System.out.println("得到请求报文中Detail节点数: "+detailList.size());
		for (int i = 0; i < detailList.size(); i++) {
			//1.取得所有的保单号
			String mProposalContNo = ((Element) detailList.get(i)).getChildTextTrim("ProposalNo");
			ExeSQL es = new ExeSQL();
			
			Element mDetail = new Element("Detail");
			Element mRiskCode = new Element("RiskCode");
			Element mProposalNo = new Element("ProposalNo");
			Element mStatus = new Element("Status");
			Element insuYear = new Element("insuYear");   				 //保险期限 
			Element insuYearFlag = new Element("insuYearFlag");   			  //保险周期代码 
			Element payYear = new Element("payYear");   //保费缴费期数  
			Element payIntv = new Element("payIntv");     //保费缴费周期代码
			Element prem = new Element("prem");     //保费
			Element mMsg = new Element("Msg");
			LCPolDB lcPol_DB = new LCPolDB();
			//查询此保单号是否存入LKNoRealTimeUWCheck。
			String sql_lk = "select 1 from LKNoRealTimeUWCheck where ProposalContNo = '"+mProposalContNo+"'";
            if(!"1".equals(es.getOneValue(sql_lk))){
            	mMsg.setText(mProposalContNo+"此单未进行过NRT801交易。");
            	
            	cLogger.info(mProposalContNo+"此单未进行过NRT801交易。");
            }else{
			
				LKNoRealTimeUWCheckDB mLKNoRealTimeUWCheckDB = new LKNoRealTimeUWCheckDB();
				mLKNoRealTimeUWCheckDB.setProposalContno(mProposalContNo);
				if(!mLKNoRealTimeUWCheckDB.getInfo()){
					throw new MidplatException("查询非实时信息失败！");
				}
				
				//获取保单状态
//				String mAppFlag = null;
//				String sql_lc = "select stateflag from lccont where contno = '"+mProposalContNo+"'";
//				mAppFlag = es.getOneValue(sql_lc);
//				if("".equals(mAppFlag) || mAppFlag == null){
//					String sql_lb = "select 1 from lbcont where contno = '"+mProposalContNo+"'";
//					mAppFlag = es.getOneValue(sql_lb); //向核心询问，放入b表的单子有哪些状态？
//					if("1".equals(mAppFlag)){
//						mAppFlag = "99";
//					}
//				}
//				mAppFlag = getStateFlag(mAppFlag);
				
				LCContDB tLCContDB = new LCContDB();
				LCContSet tLCContSet = new LCContSet();
				tLCContDB.setPrtNo(mProposalContNo);  //--------
				tLCContSet = tLCContDB.query();
				
				if (tLCContSet.mErrors.needDealError() || tLCContSet == null) {
					System.out.println("查询失败");
					continue;
				} else if(tLCContSet.size() == 0){
					mLKNoRealTimeUWCheckDB.setTbDealStatus("1");
					mLKNoRealTimeUWCheckDB.setPassFlag("0");
				} else {
					String tStateFlag = tLCContSet.get(1).getStateFlag();
					double pre = tLCContSet.get(1).getPrem();
					prem.setText(String.valueOf(pre));
					
					if("1".equals(tStateFlag)){
						mLKNoRealTimeUWCheckDB.setTbDealStatus("2");
						mLKNoRealTimeUWCheckDB.setPassFlag("3");	//已签单
					} else {
						mLKNoRealTimeUWCheckDB.setTbDealStatus("1");  //
						mLKNoRealTimeUWCheckDB.setPassFlag("0");	//已接受未签单
					}  
					lcPol_DB.setContNo(tLCContDB.getContNo());
					if(lcPol_DB.getInfo()){
						LCPolSet lc_Set = lcPol_DB.query();
						LCPolSchema LC_Sc = lc_Set.get(1);
						
						String payMode =  LC_Sc.getPayMode();   //缴费方式
						int pay_Intv = LC_Sc.getPayIntv();      //缴费频率
						int lc_payYear = LC_Sc.getPayYears();  //缴费期数
						
						payIntv.setText(pay_Intv+""); //PAYYEARS
						payYear.setText(lc_payYear+"");
						
						String insuYear_Flag = LC_Sc.getInsuYearFlag();  //保险期间标志
						int insu_Year =  LC_Sc.getInsuYear();
						
						insuYearFlag.setText(insuYear_Flag+"");
						insuYear.setText(insu_Year+"");
						
					}
					else
					{
						throw new MidplatException("查询险种信息失败!");
					}
					
				}
				
//				mLKNoRealTimeUWCheckDB.setTbDealStatus("1"); //1表示此保单号已被查询过。
//				mLKNoRealTimeUWCheckDB.setPassFlag(mAppFlag);  //保单状态
				mLKNoRealTimeUWCheckDB.setModifyDate(mCurrentDate);
				mLKNoRealTimeUWCheckDB.setModifyTime(mCurrentTime);
				if(!mLKNoRealTimeUWCheckDB.update()){
					throw new MidplatException("更新非实时查询数据失败！");
				}
				
				mRiskCode.setText(mLKNoRealTimeUWCheckDB.getRiskCode());
				mProposalNo.setText(mLKNoRealTimeUWCheckDB.getProposalContno());
				if("0".equals(mLKNoRealTimeUWCheckDB.getPassFlag())){
					mStatus.setText("99");
				}else {
					mStatus.setText(mLKNoRealTimeUWCheckDB.getPassFlag());
				}
				if (mLKNoRealTimeUWCheckDB.getPassFlag().equals("0")){
					mLKNoRealTimeUWCheckDB.setTbDealStatus("已接收");
					mMsg.setText("保险公司" + mLKNoRealTimeUWCheckDB.getTbDealStatus() + "非实时核保信息");
				} else {
					mLKNoRealTimeUWCheckDB.setTbDealStatus("已签单");
					mMsg.setText(mLKNoRealTimeUWCheckDB.getTbDealStatus());
				}
				
				mDetail.addContent(mRiskCode);
				mDetail.addContent(mProposalNo);
				
				mDetail.addContent(insuYear);
				mDetail.addContent(insuYearFlag);
				mDetail.addContent(payYear);
				mDetail.addContent(payIntv);
				mDetail.addContent(prem);
				
				mDetail.addContent(mStatus);
            }
            mDetail.addContent(mMsg);
            
			mDetailList.addContent(mDetail);
			//2.将保单号作为条件在LKNoRealTimeUWCheck表中查找对应数据
			
			
			//3.如果数据存在则修改对应的TBDealStatus为‘已查询’，再去lccont表里查找保单状态，修改PassFlag
			//  如果数据不存在，则修改对应的TBDealStatus为‘已查询’,返回‘未发送非实时核保’。
			
			//4.组织返回标准报文
		}
		mDetailList.addContent(new Element("DetailCount").setText(detailList.size()+""));
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mDetailList);

		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransCode(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		return mLKTransStatusDB;
	}
	
	public static String getStateFlag(String state){    //保单状态转换
		//0 - 投保  1 - 承保有效  99 - 退保
		String mState = "";
		if("1".equals(state)){
			mState = "3";
		}else if("99".equals(state)) {
			mState = "99";
		}
		return mState;
	}
	
//	public static void main(String[] args) {
//		LKNoRealTimeUWCheckDB mLKNoRealTimeUWCheckDB = new LKNoRealTimeUWCheckDB();
//		mLKNoRealTimeUWCheckDB.setProposalContno("13000591785");
//		if(mLKNoRealTimeUWCheckDB.getInfo()){
////			throw new MidplatException("查询非实时信息失败！");
//		}
//	}
//	
}
