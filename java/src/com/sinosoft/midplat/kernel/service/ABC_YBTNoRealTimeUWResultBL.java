package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKNoRealTimeUWCheckDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LKNoRealTimeUWCheckSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LKNoRealTimeUWCheckSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ABC_YBTNoRealTimeUWResultBL extends ServiceImpl  {

	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	private LKNoRealTimeUWCheckSet mLKNoRealTimeUWCheckSet ;
	
	public ABC_YBTNoRealTimeUWResultBL(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_YBTNoRealTimeUWResultBL.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		Element mTranData = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
//		Element mBaseInfo = pInXmlDoc.getRootElement().getChild(BaseInfo);
		LKTransStatusDB mLKTransStatusDB = null;	
		
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			//业务处理
			//取投保单受理状态变更文件
			mOutXmlDoc = deal();
			
			mFlag.setText("0");
			mDesc.setText("交易成功！");
			mOutXmlDoc.getRootElement().getChild("RetData").addContent(mFlag);
			mOutXmlDoc.getRootElement().getChild("RetData").addContent(mDesc);
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");//执行任务完毕记
//			mLKTransStatusDB.setTransCode("1");//文件生成
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ABC_YBTNoRealTimeUWResultBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal() throws Exception {
		// 查出未签单的保单变更数据
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mLCCont = new Element("LCCont");
		
		Element mChkDetails = new Element("ChkDetails");
		
		LKNoRealTimeUWCheckDB mLKNoRealTimeUWCheck = new LKNoRealTimeUWCheckDB();
		mLKNoRealTimeUWCheck.setModifyDate(mCurrentDate);
		mLKNoRealTimeUWCheck.setPassFlag("1");
		mLKNoRealTimeUWCheck.setBankCode("04");
		mLKNoRealTimeUWCheckSet = mLKNoRealTimeUWCheck.query();
		ExeSQL es = new ExeSQL();
		SSRS ssrs ;
		for (int i = 1; i <= mLKNoRealTimeUWCheckSet.size(); i++) {
			LKNoRealTimeUWCheckSchema mLKNoRealTimeUWCheckSchema = mLKNoRealTimeUWCheckSet.get(i);
		
			String mPrtNo = mLKNoRealTimeUWCheckSchema.getProposalContno();
			
			Element mChkDetail = new Element("ChkDetail");
			Element mZoneNo = new Element("ZoneNo");
			mZoneNo.setText(mLKNoRealTimeUWCheckSchema.getZoneNo());
			
			Element mInsuredID = new Element("InsuredID");
			mInsuredID.setText("020");
			
			Element mTransNo = new Element("TransNo");
			mTransNo.setText(mLKNoRealTimeUWCheckSchema.getTransNo());
			
			Element mTransDate = new Element("TransDate");
			mTransDate.setText(mLKNoRealTimeUWCheckSchema.getTransDate());
			
			Element mProposalContno = new Element("ProposalContno");
			mProposalContno.setText(mPrtNo);
			//---------------------------------------------------------------------------
			String sql_cont = "select contno,insuredname,insuredidtype,insuredidno,payintv,appntname,appntidtype,appntidno,approvedate,cinvalidate,bankaccno from LCcont where prtno = '"+ mPrtNo +"' with ur";
			ssrs = es.execSQL(sql_cont);
			if(ssrs.MaxRow < 0){
				cLogger.info("此印刷号在LCcont表中未查到对应数据，prtno = "+ mPrtNo);
				continue;
			}
			String xContno = ssrs.GetText(1, 1);
			Element mContNo = new Element("ContNo");
			mContNo.setText(xContno);
			
			/****   核保结论
			 * w:尚未进行人工核保
			 * 1：谢绝承保 2：延期承保 3：条件承保 4：变更承保 5：自核未通过 6：待上级审核 7：问题件 8：延期承保 9：正常承保 a:撤销申请 b:保险计划变更 z:核保订正
			 */
			String sql_hb = "Select Case"
					        +"  When Approveflag Is Null Then"
					        +"   'w'"
					        +"  Else"
					        +"  (Select Code"
					        +"  From Ldcode"
					        +"  Where Codetype = 'uwflag'"
					        +"  And Code = Uwflag)"
					        +"  End"
					        +"  From Lccont"
					        +"  Where Prtno = '"+ mPrtNo +"' and Contno = '"+ xContno +"'";
			String hbResult = es.getOneValue(sql_hb);
			if("w".equals(hbResult)){
				cLogger.info("此印刷号 尚未进行人工核保，prtno = "+ mPrtNo);
				continue;
			}
			Element mHBResult = new Element("HBResult");
			mHBResult.setText(hbResult);//核保结论 
			
			Element mDescr = new Element("Descr");
			mDescr.setText(mLKNoRealTimeUWCheckSchema.getDescr());
			
//			String sql_pol = "select sum(prem) from lcpol where prtno = '"+ mPrtNo +"' with ur";
//			ssrs = es.execSQL(sql_pol);
			LCPolDB polDB = new LCPolDB();
			polDB.setPrtNo(mPrtNo);
			LCPolSet polSet = polDB.query();
			String tMainRiskCode = "";
			double sumPrem = 0;
			if(polSet.size() <= 0){
				cLogger.info("此印刷号在LCPOL表中未查到对应数据，prtno = "+ mPrtNo);
				continue;
			}
			for (int j = 1; j <= polSet.size(); j++) {
				LCPolSchema polBean = polSet.get(j);
				sumPrem += polBean.getPrem();
				
				String sql_app = "select subriskflag from lmriskapp where riskcode = '"+ polBean.getRiskCode() +"' with ur";
				if("M".equals(es.getOneValue(sql_app))){
					tMainRiskCode = polBean.getRiskCode();
				}
			}
			
			Element mSumPrem = new Element("SumPrem");
			mSumPrem.setText(sumPrem+"");
			
			Element mInsuredName = new Element("InsuredName");
			mInsuredName.setText(ssrs.GetText(1, 2));
			
			Element mInsuredIDType = new Element("InsuredIDType");
			mInsuredIDType.setText(ssrs.GetText(1, 3));
			
			Element mInsuredIDNo = new Element("InsuredIDNo");
			mInsuredIDNo.setText(ssrs.GetText(1, 4));
			
			Element mPayintv = new Element("Payintv");
			mPayintv.setText(ssrs.GetText(1, 5));
			
			Element mAppntName = new Element("AppntName");
			mAppntName.setText(ssrs.GetText(1, 6));
			
			Element mAppntIdType = new Element("AppntIdType");
			mAppntIdType.setText(ssrs.GetText(1, 7));
			
			Element mAppntIdNo = new Element("AppntIdNo");
			mAppntIdNo.setText(ssrs.GetText(1, 8));
			
			Element mApproveDate = new Element("ApproveDate");
			mApproveDate.setText(ssrs.GetText(1, 9));
			
			Element mCinvaliDate = new Element("CinvaliDate");
			mCinvaliDate.setText(ssrs.GetText(1, 10));
			
			Element mBankAccNo = new Element("BankAccNo");
			mBankAccNo.setText(ssrs.GetText(1, 11));

			String pSQL = "select relationtoappnt from lcinsured where contno = '" + xContno + "'";
			
			Element mRelationToAppnt = new Element("RelationToAppnt");
			mRelationToAppnt.setText(new ExeSQL().getOneValue(pSQL) );
			
			Element mRiskList = new Element("Risks");
			for (int j = 1; j <= polSet.size(); j++) {
				LCPolSchema mLCPolSchema = polSet.get(j);
				Element mRisk = new Element("Risk");
				
				Element mRiskcode = new Element("Riskcode");
				mRiskcode.setText(mLCPolSchema.getRiskCode());
				
				Element mMainRiskcode = new Element("MainRiskcode");
				mMainRiskcode.setText(tMainRiskCode);
				
				/**  核保结论状态
				 * w:尚未进行人工核保
				 * Ldcode 表中数据
				 * 1：谢绝承保 2：延期承保 3：条件承保 4：变更承保 5：自核未通过 6：待上级审核 7：问题件 8：延期承保 9：正常承保 a:撤销申请 b:保险计划变更 z:核保订正
				 */
				String sql_hbflag = "Select Case"
							        +" When Approveflag Is Null Then"
							        +"  'w'"
							        +" Else"
							        +" (Select Code"
							        +"  From Ldcode"
							        +" Where Codetype = 'uwflag'"
							        +"  And Code = Uwflag)"
							        +" End"
							        +" From Lcpol"
							        +" Where Contno = '"+ xContno +"'"
							        +" And Prtno = '"+ mPrtNo +"'"
							        +" And Riskcode = '"+ mLCPolSchema.getRiskCode() +"'";
				String hbFlag = es.getOneValue(sql_hbflag);
				if("w".equals(hbFlag)){
					cLogger.info("此印刷号 尚未进行人工核保，prtno = "+ mPrtNo);
					continue;
				}
				Element mHBResultFlag = new Element("HBResultFlag");
				mHBResultFlag.setText(hbFlag);//核保结论状态 
				
				Element mUnit = new Element("Unit");
				mUnit.setText(mLCPolSchema.getCopys()+"");
				
				Element mPrem = new Element("Prem");
				mPrem.setText(mLCPolSchema.getPrem()+"");
				
				Element mAmnt = new Element("Amnt");
				mAmnt.setText(mLCPolSchema.getAmnt()+"");
				
				Element mInsuYearFlag = new Element("InsuYearFlag");
				mInsuYearFlag.setText(mLCPolSchema.getInsuYearFlag());
				
				Element mInsuYear = new Element("InsuYear");
				mInsuYear.setText(mLCPolSchema.getInsuYear()+"");
				
				Element mPayEndYearFlag = new Element("PayEndYearFlag");
				mPayEndYearFlag.setText(mLCPolSchema.getPayEndYearFlag());
				
				Element mPayEndYear = new Element("PayEndYear");
				mPayEndYear.setText(mLCPolSchema.getPayEndYear()+"");
				
				mRisk.addContent(mRiskcode);
				mRisk.addContent(mMainRiskcode);
				mRisk.addContent(mHBResultFlag);
				mRisk.addContent(mUnit);
				mRisk.addContent(mPrem);
				mRisk.addContent(mAmnt);
				mRisk.addContent(mInsuYearFlag);
				mRisk.addContent(mInsuYear);
				mRisk.addContent(mPayEndYearFlag);
				mRisk.addContent(mPayEndYear);
				
				mRiskList.addContent(mRisk);
			}
			
			mChkDetail.addContent(mZoneNo);
			mChkDetail.addContent(mInsuredID);
			mChkDetail.addContent(mTransNo);
			mChkDetail.addContent(mTransDate);
			mChkDetail.addContent(mProposalContno);
			mChkDetail.addContent(mContNo);
			mChkDetail.addContent(mHBResult);
			mChkDetail.addContent(mDescr);
			mChkDetail.addContent(mSumPrem);
			mChkDetail.addContent(mAppntName);
			mChkDetail.addContent(mAppntIdType);
			mChkDetail.addContent(mAppntIdNo);
			mChkDetail.addContent(mInsuredName);
			mChkDetail.addContent(mInsuredIDType);
			mChkDetail.addContent(mInsuredIDNo);
			mChkDetail.addContent(mApproveDate);
			mChkDetail.addContent(mCinvaliDate);
			mChkDetail.addContent(mRelationToAppnt);
			mChkDetail.addContent(mPayintv);
			mChkDetail.addContent(mBankAccNo);
			mChkDetail.addContent(mRiskList);
			
			mChkDetails.addContent(mChkDetail);
		}
		
		mLCCont.addContent(mChkDetails);
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		cLogger.info("Into ABC_YBTNoRealTimeUWResultBL.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out ABC_YBTNoRealTimeUWResultBL.insertTransLog()...");
		return mLKTransStatusDB;
	}

}
