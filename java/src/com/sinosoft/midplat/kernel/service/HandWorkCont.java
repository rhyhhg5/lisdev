/**
 * 通用简单交易处理类。
 * 此类本身不做任何业务处理，仅插入一条交易日志。
 * 目前使用此类的交易有：中行冲正、建行绿灯
 */

package com.sinosoft.midplat.kernel.service;

import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class HandWorkCont extends ServiceImpl {
	public HandWorkCont(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into SimpService.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			String mContNo = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("ContNo");
			if(mContNo ==null || "".equals(mContNo)){
				throw new MidplatException("保单号不能为空！");
			}
			mOutXmlDoc = getLCCont(mContNo);
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (MidplatException ex) {
			cLogger.error("通用简单(SimpService)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out SimpService.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into SimpService.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setPolNo(mTranData.getChild("LCCont").getChildText("ContNo"));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out SimpService.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public Document getLCCont(String pContNo) throws MidplatException {
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(pContNo);
		if (!mLCContDB.getInfo()) {
			throw new MidplatException("查询保单合同信息失败！");
		}
		
		Element mContNo = new Element(ContNo);
		mContNo.setText(mLCContDB.getContNo());
		
		Element mPrtNo = new Element(PrtNo);
		mPrtNo.setText(mLCContDB.getPrtNo());
		
		Element mProposalContNo = new Element(ProposalContNo);
		mProposalContNo.setText(mLCContDB.getProposalContNo());
		
//		Element mConsignNo = new Element("ConsignNo");
//		mConsignNo.setText(mLCContDB.getConsignNo());
		
		Element mPayIntv = new Element(PayIntv);
		mPayIntv.setText(
				String.valueOf(mLCContDB.getPayIntv()));
		
		Element mPayMode = new Element(PayMode);
		mPayMode.setText(mLCContDB.getPayMode());
		
		Element mAgentCode = new Element(AgentCode);
		//内部程序使用的是老的工号agentcode，但是显示给客户的都是新的集团工号 20141204
//		mAgentCode.setText(mLCContDB.getAgentCode());
		String mSQL = "select GroupAgentCode from laagent where AgentCode = '" + mLCContDB.getAgentCode() + "' with ur";
		mAgentCode.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentGroupCode = new Element("AgentGroupCode");
		mAgentGroupCode.setText(mLCContDB.getAgentGroup());
		
		Element mAgentGroup = new Element(AgentGroup);
		mSQL = "select name from labranchgroup where agentgroup='" + mLCContDB.getAgentGroup() + "' with ur";
		mAgentGroup.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentCom = new Element(AgentCom);//1.银邮网点的名称
		mSQL = "select name from lacom where agentcom='" + mLCContDB.getAgentCom() + "' with ur";
		mAgentCom.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentName = new Element(AgentName);//4.保险机构银保专管员姓名
		mSQL = "select name from laagent where AgentCode='" + mLCContDB.getAgentCode() + "' with ur";
		mAgentName.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mPrem = new Element(Prem);
		mPrem.setText(
				new DecimalFormat("0.00").format(mLCContDB.getPrem()));
		
		Element mAmnt = new Element(Amnt);
		mAmnt.setText(
				new DecimalFormat("0.00").format(mLCContDB.getAmnt()));
		
//		Element mAnHuiNXS_Mult = new Element("AnHuiNXS_Mult");	//picch小额信贷险为“保额算保费”，无份数概念，安徽农信社非得要份数，借用此字段
//		mAnHuiNXS_Mult.setText(
//				String.valueOf((int)mLCContDB.getCopys()));
		
		Element mManageCom = new Element(ManageCom);
		mManageCom.setText(mLCContDB.getManageCom());
		
		Element mPolApplyDate = new Element(PolApplyDate);
		mPolApplyDate.setText(mLCContDB.getPolApplyDate());
		
		Element mSignDate = new Element(SignDate);
		mSignDate.setText(mLCContDB.getSignDate());
		
		Element mCValiDate = new Element(CValiDate);
		mCValiDate.setText(mLCContDB.getCValiDate());
		
		Element mContEndDate = new Element(ContEndDate);
		mContEndDate.setText(mLCContDB.getCInValiDate());	//picch特有，保单级保单终止日期
		
//		LDComDB mLDComDB = new LDComDB();
//		mLDComDB.setComCode(mLCContDB.getManageCom());
//		if (!mLDComDB.getInfo()) {
//			throw new MidplatException("查询管理机构信息失败！");
//		}
//		Element mComLocation = new Element(ComLocation);
//		mComLocation.setText(mLDComDB.getAddress());
		
//		Element mLetterService = new Element("LetterService");
//		mLetterService.setText(mLDComDB.getLetterServiceName());
//		
//		Element mZipCode = new Element("ZipCode");
//		mZipCode.setText(mLDComDB.getZipCode());
//		
//		Element mComPhone = new Element(ComPhone);
//		mComPhone.setText("95591 / 4006695518");
		
//		Element mSpecContent = new Element(SpecContent);
//		mSpecContent.setText(mLCContDB.getRemark());

		Element mLCAppnt = getLCAppnt(pContNo);
		
		Element mLCInsureds = getLCInsureds(pContNo);
		mAmnt.setText(	//picch这边保额取主险保额。LCCont表中的保额不准确，他会简单累加所有险种(LCPol)保额。
				mLCInsureds.getChild(LCInsured).getChild(Risks).getChild(Risk).getChildText(Amnt));

		//		信用方强烈要求在保单上打印贷款的起止日期。请考虑在中间表中插入此字段。
	    //另，贷款凭证号请一并考虑。
		
		//add by mxk
//		ExeSQL mExeSQL = new ExeSQL(); 
//		String  SQL ="";
//		LKCodeMappingDB LK_Db = new LKCodeMappingDB();
//		LK_Db.setAgentCom(mLCContDB.getAgentCom());
//		LKCodeMappingSet lk_set = LK_Db.query();
//		
//		LKCodeMapping1DB LK_Db1 = new LKCodeMapping1DB();
//		LK_Db1.setAgentCom(mLCContDB.getAgentCom());
//		LKCodeMapping1Set lk_set1 = LK_Db1.query();
//		
//		if(lk_set.size() <1 && lk_set1.size() <1){
//			throw new MidplatException("查询代理人信息失败!");
//		}
//		String bankcode = "";
//		if(lk_set.size()>=1){
//			 bankcode = lk_set.get(1).getBankCode();
//		}
//		else{
//			 bankcode = lk_set1.get(1).getBankCode();
//		}
//		cLogger.info("获取到的银行编码是::"+bankcode);   //针对大连农商行做特殊处理
//		if(bankcode.equals("58")){
//			SQL = "select ServiceStartTime,ServiceEndTime,TempFeeNo ,EdorNo from LkTransStatus where prtno ='"
//				+ mLCContDB.getPrtNo()+"' and makedate = '"+mLCContDB.getMakeDate()
//				+"' and funcflag = '01'  order by maketime desc with ur";
//		}
//		else{
//		 SQL = "select ServiceStartTime,ServiceEndTime,TempFeeNo ,EdorNo from LkTransStatus where prtno ='"
//				+ mLCContDB.getPrtNo()+"' and proposalno = '" +mLCContDB.getPrtNo()+ "' and makedate = '"+mLCContDB.getMakeDate()
//				+"' and funcflag = '01'  order by maketime desc with ur";
//		}
//		SSRS tSSRS = mExeSQL.execSQL(SQL);
//		
//		String mServiceStartTime = "";
//		String mServiceEndTime = "";
//		String mTempFeeNo = "";
//		String mEdorNo = "";
//		
//		if(tSSRS.getMaxRow()>= 1){
//		 mServiceStartTime = tSSRS.GetText(1, 1);
//		 mServiceEndTime = tSSRS.GetText(1, 2);
//		 mTempFeeNo = tSSRS.GetText(1, 3);
//		 mEdorNo = tSSRS.GetText(1, 4);
//		}
		
//		Element mLoanDate = new Element("LoanDate");
//		mLoanDate.setText(mServiceStartTime);
//		
//		Element mLoanEndDate = new Element("LoanEndDate");
//		mLoanEndDate.setText(mServiceEndTime);
//		
//		Element mLoanInvoiceNo = new Element("LoanInvoiceNo");
//		mLoanInvoiceNo.setText(mTempFeeNo);
//		
//		Element mLoanContractAmt = new Element("LoanContractAmt");
//		mLoanContractAmt.setText(mEdorNo);
		
//		Element mBankaccno =  new Element("Bankaccno"); 
//		mBankaccno.setText(mLCContDB.getBankAccNo());
		
		Element mLCCont = new Element(LCCont);
		mLCCont.addContent(mContNo);
		mLCCont.addContent(mPrtNo);
		mLCCont.addContent(mProposalContNo);
//		mLCCont.addContent(mConsignNo);
		mLCCont.addContent(mPayIntv);
		mLCCont.addContent(mPayMode);
		mLCCont.addContent(mAgentCode);
		mLCCont.addContent(mAgentName);//1.银邮网点的名称
		mLCCont.addContent(mAgentGroupCode);
		mLCCont.addContent(mAgentGroup);
		mLCCont.addContent(mAgentCom);//4.保险机构银保专管员姓名
		mLCCont.addContent(mPrem);
		mLCCont.addContent(mAmnt);
//		mLCCont.addContent(mAnHuiNXS_Mult);
		mLCCont.addContent(mManageCom);
		mLCCont.addContent(mPolApplyDate);
		mLCCont.addContent(mSignDate);
		mLCCont.addContent(mCValiDate);
		mLCCont.addContent(mContEndDate);
//		mLCCont.addContent(mComLocation);
//		mLCCont.addContent(mLetterService);
//		mLCCont.addContent(mZipCode);
//		mLCCont.addContent(mComPhone);
//		mLCCont.addContent(mSpecContent);
//		mLCCont.addContent(mLoanDate);
//		mLCCont.addContent(mLoanEndDate);
//		mLCCont.addContent(mLoanInvoiceNo);
//		mLCCont.addContent(mLoanContractAmt);
//		mLCCont.addContent(mBankaccno);
		mLCCont.addContent(mLCAppnt);
		mLCCont.addContent(mLCInsureds);
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		
		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		Element mTranData = new Element(TranData);
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
	
		return new Document(mTranData);
	}
	
	private Element getLCAppnt(String pContNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCAppnt()...");
		
		LCAppntDB mLCAppntDB = new LCAppntDB();
		mLCAppntDB.setContNo(pContNo);	//核心只支持单投保人。ContNo是主键
		if (!mLCAppntDB.getInfo()) {
			throw new MidplatException("查询投保人信息失败！");
		}
		LCAddressDB mAddressDB = new LCAddressDB();
		mAddressDB.setCustomerNo(mLCAppntDB.getAppntNo());
		mAddressDB.setAddressNo(mLCAppntDB.getAddressNo());
		if (!mAddressDB.getInfo()) {
			throw new MidplatException("查询投保人地址信息失败！");
		}
		
		Element mAppntNo = new Element(AppntNo);
		mAppntNo.setText(mLCAppntDB.getAppntNo());
		
		Element mAppntName = new Element(AppntName);
		mAppntName.setText(mLCAppntDB.getAppntName());
		
		Element mAppntSex = new Element(AppntSex);
		mAppntSex.setText(mLCAppntDB.getAppntSex());
		
		Element mAppntBirthday = new Element(AppntBirthday);
		mAppntBirthday.setText(mLCAppntDB.getAppntBirthday());
		
		Element mAppntIDType = new Element(AppntIDType);
		mAppntIDType.setText(mLCAppntDB.getIDType());
		
		Element mAppntIDNo = new Element(AppntIDNo);
		mAppntIDNo.setText(mLCAppntDB.getIDNo());
		
//		Element mOccupationcode = new Element("OccupationCode");
//		mOccupationcode.setText(mLCAppntDB.getOccupationCode());
//		
//		Element mAddress = new Element(Address);
//		mAddress.setText(mAddressDB.getPostalAddress());
//		
//		Element mZipCode = new Element(ZipCode);
//		mZipCode.setText(mAddressDB.getZipCode());
//		
//		Element mMobile = new Element(Mobile);
//		mMobile.setText(mAddressDB.getMobile());
//		
//		Element mPhone = new Element(Phone);
//		mPhone.setText(mAddressDB.getPhone());
//		
//		Element mEmail = new Element(Email);
//		mEmail.setText(mAddressDB.getEMail());
		
		Element mLCAppnt = new Element(LCAppnt);
		mLCAppnt.addContent(mAppntNo);
		mLCAppnt.addContent(mAppntName);
		mLCAppnt.addContent(mAppntSex);
		mLCAppnt.addContent(mAppntBirthday);
		mLCAppnt.addContent(mAppntIDType);
		mLCAppnt.addContent(mAppntIDNo);
//		mLCAppnt.addContent(mOccupationcode);
//		mLCAppnt.addContent(mAddress);
//		mLCAppnt.addContent(mZipCode);
//		mLCAppnt.addContent(mMobile);
//		mLCAppnt.addContent(mPhone);
//		mLCAppnt.addContent(mEmail);
		
		
		
		cLogger.info("Out YbtContQueryBL.getLCAppnt()!");
		return mLCAppnt;
	}
	
	private Element getLCInsureds(String pContNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCInsureds()...");
		
		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setContNo(pContNo);
		LCInsuredSet mLCInsuredSet = mLCInsuredDB.query();
		if ((null==mLCInsuredSet) || (mLCInsuredSet.size()<1)) {
			throw new MidplatException("查询被保人信息失败！");
		}
		
		Element mLCInsureds = new Element("LCInsureds");
		for (int i = 1; i <= mLCInsuredSet.size(); i++) {
			LCInsuredSchema tLCInsuredSchema = mLCInsuredSet.get(i);
			LCAddressDB tAddressDB = new LCAddressDB();
			tAddressDB.setCustomerNo(tLCInsuredSchema.getInsuredNo());
			tAddressDB.setAddressNo(tLCInsuredSchema.getAddressNo());
			if (!tAddressDB.getInfo()) {
				throw new MidplatException("查询被保人地址信息失败！");
			}
			
			Element tInsuredNo = new Element(InsuredNo);
			tInsuredNo.setText(tLCInsuredSchema.getInsuredNo());
			
			Element tAppntName = new Element(Name);
			tAppntName.setText(tLCInsuredSchema.getName());

			Element tSex = new Element(Sex);
			tSex.setText(tLCInsuredSchema.getSex());
			
			Element tBirthday = new Element(Birthday);
			tBirthday.setText(tLCInsuredSchema.getBirthday());
			
			Element tIDType = new Element(IDType);
			tIDType.setText(tLCInsuredSchema.getIDType());
			
			Element tIDNo = new Element(IDNo);
			tIDNo.setText(tLCInsuredSchema.getIDNo());
			
//			Element tOccupationCode = new Element("OccupationCode");
//			tOccupationCode.setText(tLCInsuredSchema.getOccupationCode());
//			
//			Element tAddress = new Element(Address);
//			tAddress.setText(tAddressDB.getPostalAddress());
//			
//			Element tZipCode = new Element(ZipCode);
//			tZipCode.setText(tAddressDB.getZipCode());
//			
//			Element tMobile = new Element(Mobile);
//			tMobile.setText(tAddressDB.getMobile());
//			
//			Element tPhone = new Element(Phone);
//			tPhone.setText(tAddressDB.getPhone());
//			
//			Element tEmail = new Element(Email);
//			tEmail.setText(tAddressDB.getEMail());
//			
//			Element tRelaToMain = new Element(RelaToMain);
//			tRelaToMain.setText(tLCInsuredSchema.getRelationToMainInsured());
//			
//			Element tRelaToAppnt = new Element(RelaToAppnt);
//			tRelaToAppnt.setText(tLCInsuredSchema.getRelationToAppnt());
			
			Element tRisks = getRisks(pContNo, tLCInsuredSchema.getInsuredNo());
			
			Element tLCInsured = new Element("LCInsured");
			tLCInsured.addContent(tAppntName);
			tLCInsured.addContent(tInsuredNo);
			tLCInsured.addContent(tSex);
			tLCInsured.addContent(tBirthday);
			tLCInsured.addContent(tIDType);
			tLCInsured.addContent(tIDNo);
//			tLCInsured.addContent(tOccupationCode);
//			tLCInsured.addContent(tRelaToAppnt);
//			tLCInsured.addContent(tAddress);
//			tLCInsured.addContent(tZipCode);
//			tLCInsured.addContent(tMobile);
//			tLCInsured.addContent(tPhone);
//			tLCInsured.addContent(tEmail);
			
			tLCInsured.addContent(tRisks);
			
			mLCInsureds.addContent(tLCInsured);
		}

		cLogger.info("Out YbtContQueryBL.getLCInsureds()!");
		return mLCInsureds;
	}
	
	private Element getRisks(String pContNo, String pInsuredNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getRisks()...");
		
		LCPolDB mLCPolDB = new LCPolDB();
		mLCPolDB.setContNo(pContNo);
		mLCPolDB.setInsuredNo(pInsuredNo);
		LCPolSet mLCPolSet = mLCPolDB.query();
		if ((null==mLCPolSet) || (mLCPolSet.size()<1)) {
      	cLogger.error(mLCPolDB.mErrors.getFirstError());
      	throw new MidplatException("查询险种信息失败！");
		}
		
		Element mRisks = new Element(Risks);
		double mInitFeeRate = 0.00;
		System.out.println(":::::::"+mLCPolSet.size());
		for (int i = 1; i <= mLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = (LCPolSchema) mLCPolSet.get(i);

			String tRiskCodeStr = tLCPolSchema.getRiskCode();
			
			System.out.println(tRiskCodeStr);
			
//			if (tRiskCodeStr.equals("530701")	//附加健康人生个人意外伤害保险（B款）
//					|| tRiskCodeStr.equals("530801")	//附加健康人生个人意外伤害保险(C款)
//					|| tRiskCodeStr.equals("331401")	//附加健康专家个人护理保险
//					|| tRiskCodeStr.equals("531001")	//附加健康人生个人意外伤害保险（D款）
//					|| tRiskCodeStr.equals("531101")    //附加金利宝个人意外伤害保险
//                    || tRiskCodeStr.equals("1607")
//                    || tRiskCodeStr.equals("260301")
//                    || tRiskCodeStr.equals("531301")) {	//附加康利相伴个人意外伤害保险
//				continue;
//            }
				
//			if(!tLCPolSchema.getAgentCom().startsWith("PY057")){	//江苏银行需要返回附险信息 测试：PY057 生产：PY026
//				if(tRiskCodeStr.equals("340201")){//附加安心宝个人护理保险
//					continue;
//				}
//			}
//			Element mFeeRate = new Element("FeeRate");
//			if (tRiskCodeStr.equals("333001")){
//				mFeeRate.setText(String.valueOf(tLCPolSchema.getInitFeeRate()));
//			}
//            Element tLiaoNFlag = new Element("LiaoNFlag");
//            if (tRiskCodeStr.equals("550806")) {
//                // 辽宁套餐做特殊标志,ABC三款只能根据险种个数不一样去区分.
//                if (mLCPolSet.size() == 3)
//                    tLiaoNFlag.setText("WR0145");
//                else if (mLCPolSet.size() == 2)
//                    tLiaoNFlag.setText("XBT002");
//                else
//                    tLiaoNFlag.setText("XBT003");
//            }
			System.out.println("险种个数标识");
			Element tRiskCode = new Element(RiskCode);
			tRiskCode.setText(tLCPolSchema.getRiskCode());
			
			Element tRiskName = new Element(RiskName);
			String mSQL = "select RiskName from lmrisk where riskcode='" + tLCPolSchema.getRiskCode() + "' with ur";
			tRiskName.setText(new ExeSQL().getOneValue(mSQL));
			
			Element tMainRiskCode = new Element(MainRiskCode);
//			tMainRiskCode.setText(tRiskCode.getText());	//注意：此处暂将主险代码置为与险种代码一致，以后遇到需返回多险种情况时，此处需修改。
//			if("333001".equals(tLCPolSchema.getRiskCode()) || "532201".equals(tLCPolSchema.getRiskCode())){ //万能型H区分主副险
////				String mSQL2 = "select riskcode from lmriskapp where subriskflag = 'M' and riskcode = '"+ tLCPolSchema.getRiskCode() +"' with ur";
////				String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
////				tMainRiskCode.setText(mMainRiskCode);
//				tMainRiskCode.setText("333001");
//			}else if("333501".equals(tLCPolSchema.getRiskCode()) || "532501".equals(tLCPolSchema.getRiskCode()) || "532401".equals(tLCPolSchema.getRiskCode())){ //万能型H区分主副险
//				tMainRiskCode.setText("333501");
//			}else if("730101".equals(tLCPolSchema.getRiskCode()) || "332401".equals(tLCPolSchema.getRiskCode())){ //康利人生两全保险（分红型）
//				//康利人生两全分红型需区分主险附加险
//				String mSQL2 = "select riskcode from lcpol where contno='" + tLCPolSchema.getContNo() + "' and polno = mainpolno with ur";
//				String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
//				tMainRiskCode.setText(mMainRiskCode);
//			}else if("231701".equals(tLCPolSchema.getRiskCode()) || "333801".equals(tLCPolSchema.getRiskCode())){ //康乐人生个人重大疾病保险（A款）
//				tMainRiskCode.setText("231701");
//			}else if("231401".equals(tLCPolSchema.getRiskCode())){ //附加豁免保费个人疾病保险
//				tMainRiskCode.setText("231401");
//			}else{
//				//其余产品区分主附险
//				String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '" + tLCPolSchema.getContNo() + "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
//				String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
//				tMainRiskCode.setText(mMainRiskCode);
//			}
			String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '" + tLCPolSchema.getContNo() + "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
			String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
			tMainRiskCode.setText(mMainRiskCode);
			Element tCValiDate = new Element(CValiDate);
			tCValiDate.setText(tLCPolSchema.getCValiDate());
			
			Element tAmnt = new Element(Amnt);
			tAmnt.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getAmnt()));
			
			Element tPrem = new Element(Prem);
			tPrem.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getPrem()));
			
			Element tSupplementaryPrem = new Element(SupplementaryPrem);
			tSupplementaryPrem.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getSupplementaryPrem()));
			
			Element tMult = new Element(Mult);
			tMult.setText(
					String.valueOf(tLCPolSchema.getMult()));

			Element tPayIntv = new Element(PayIntv);
			tPayIntv.setText(
					String.valueOf(tLCPolSchema.getPayIntv()));
			
			Element tInsuYearFlag = new Element(InsuYearFlag);
			tInsuYearFlag.setText(tLCPolSchema.getInsuYearFlag());

			Element tInsuYear = new Element(InsuYear);
			tInsuYear.setText(
					String.valueOf(tLCPolSchema.getInsuYear()));
			
			Element tYears = new Element(Years);
			tYears.setText(
					String.valueOf(tLCPolSchema.getYears()));
			
			Element tPayEndYearFlag = new Element(PayEndYearFlag);
			tPayEndYearFlag.setText(tLCPolSchema.getPayEndYearFlag());
			
			Element tPayEndYear = new Element(PayEndYear);
			tPayEndYear.setText(
					String.valueOf(tLCPolSchema.getPayEndYear()));
			
			Element tPayEndDate = new Element(PayEndDate);
			tPayEndDate.setText(tLCPolSchema.getPayEndDate());
			
			Element tPayYears = new Element(PayYears);
			tPayYears.setText(
					String.valueOf(tLCPolSchema.getPayYears()));
			
//			Element tGetYearFlag = new Element(GetYearFlag);
//			tGetYearFlag.setText(tLCPolSchema.getGetYearFlag());
//			
//			Element tGetYear = new Element(GetYear);
//			tGetYear.setText(
//					String.valueOf(tLCPolSchema.getGetYear()));

//			Element tLCBnfs = getLCBnfs(tLCPolSchema.getPolNo());
			
//			Element tCashValues = getCashValues(tLCPolSchema.getPolNo());
			
//			Element tBonusValues = getBonusValues(tLCPolSchema.getPolNo());

			Element tRisk = new Element(Risk);
//            tRisk.addContent(tLiaoNFlag);
			tRisk.addContent(tRiskCode);
			tRisk.addContent(tRiskName);
			tRisk.addContent(tMainRiskCode);
			tRisk.addContent(tCValiDate);
			tRisk.addContent(tAmnt);
			tRisk.addContent(tPrem);
			tRisk.addContent(tSupplementaryPrem);
			tRisk.addContent(tMult);
			tRisk.addContent(tPayIntv);
			tRisk.addContent(tInsuYearFlag);
			tRisk.addContent(tInsuYear);
			tRisk.addContent(tYears);
			tRisk.addContent(tPayEndYearFlag);
			tRisk.addContent(tPayEndYear);
			tRisk.addContent(tPayEndDate);
			tRisk.addContent(tPayYears);
//			tRisk.addContent(tGetYearFlag);
//			tRisk.addContent(tGetYear);
//			if (tRiskCodeStr.equals("333001")){
//				tRisk.addContent(mFeeRate);
//			}
//			tRisk.addContent(tLCBnfs);
//			tRisk.addContent(tCashValues);
//			tRisk.addContent(tBonusValues);

			mRisks.addContent(tRisk);
		}

		cLogger.info("Out YbtContQueryBL.getRisks()!");
		return mRisks;
	}
}
