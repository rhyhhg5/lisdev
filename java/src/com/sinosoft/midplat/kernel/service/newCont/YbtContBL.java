/**
 * 保单信息录入！
 */

package com.sinosoft.midplat.kernel.service.newCont;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAccountSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.tb.ContBL;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbtContBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(YbtContBL.class);
	
	private final Document cInXmlDoc;
	private final GlobalInput cGlobalInput;
	
	public YbtContBL(Document pInXmlDoc, GlobalInput pGlobalInput) {
		cInXmlDoc = pInXmlDoc;
		cGlobalInput = pGlobalInput;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into YbtContBL.deal()...");
		
		String mTransrNo = cInXmlDoc.getRootElement()
			.getChild(BaseInfo).getChildText(TransrNo);
		 
		//校验数据
		checkContBL();
		
		//录入保单合同信息
		VData mContBLData = getContBLVData();
		ContBL mContBL = new ContBL();
		cLogger.info("Start call ContBL.submitData()...");
		long mStartMillis = System.currentTimeMillis();
		if (!mContBL.submitData(mContBLData, "INSERT||CONT")) {
			throw new MidplatException(mContBL.mErrors.getFirstError());
		}
		cLogger.info("ContBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s"
				+ "；交易流水号：" + mTransrNo);
		cLogger.info("End call ContBL.submitData()!");
		
		cLogger.info("Out YbtContBL.deal()!");
	}
	
	private void checkContBL() throws Exception {
		cLogger.info("Into YbtContBL.checkContBL()...");
		
		Element mLCCont = cInXmlDoc.getRootElement().getChild(LCCont);
		Element mBaseInfo = cInXmlDoc.getRootElement().getChild(BaseInfo);
		
		//校验投保单号
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(mLCCont.getChildText("PrtNo"));
		
		/**
		 * add by wangxt for checking in 2009-3-18 begin
		 * 投保单号校验
		 */
		String mPrtNo = mLCContDB.getPrtNo();
		if (!"0".equals(mLCCont.getChildTextTrim("SourceType"))) {
			cLogger.info("电子渠道出单不校验保单号!!!");
		} else {
			checkPrtNo(mPrtNo);
		}
		
		/**
		 * add by wangxt for checking in 2009-3-18 end
		 * 投保单印刷号校验（单证号）
		 */
		LCContSet mLCContSet = mLCContDB.query();
		if (mLCContSet.size() > 0) {
			throw new MidplatException("该投保单印刷号已使用，请更换！");
		}
		
		mLCContDB = new LCContDB();
//		mLCContDB.setAppFlag("1"); //未签单不校验保单印刷号 - picch这边使用ProposalContNo作为保单号录单，未签单也得校验，免得报"插入数据库失败的错误"
		mLCContDB.setProposalContNo(mLCCont.getChildText("ProposalContNo"));
		if (mLCContDB.getProposalContNo().equals("")) {
			throw new MidplatException("保单合同印刷号不能为空！");
		}
		mLCContSet = mLCContDB.query();
		if (mLCContSet.size() > 0) {
			throw new MidplatException("该保单合同印刷号已使用，请更换！");
		}
		
		//投保单印刷号校验（单证号）
		if (!"0".equals(mLCCont.getChildTextTrim("SourceType"))) {
			cLogger.info("电子渠道出单不校验单证号!!!"); 
		} else {
			if (!mLCCont.getChildText(ProposalContNo).startsWith("YBT")
					&& !mBaseInfo.getChildText("BankCode").equals("03")) {// 保险公司新规定，所以单证都需校验
				checkProposalContNo(mLCCont);
			}
		}
		cLogger.info("Out YbtContBL.checkContBL()!");
	}
	
	/***
	 * 校验：保单单证号：ProposalContNo 是否符合规则
	 * @throws Exception 
	 * ****/
	private boolean checkProposalContNo(Element mLCCont) throws Exception{
		cLogger.info("Into YbtContBL.checkProposalContNo!");
			//保险公司新规定，所以单证都需校验
		String mSQL = "select stateflag from lzcard where certifyCode in ('HB07/07B','YB12/10B') "
				+ "and startno<='" + mLCCont.getChildText("ProposalContNo") + "' "
				+ "and endno>='" + mLCCont.getChildText("ProposalContNo") + "' "
				+ "and ReceiveCom='E" + cGlobalInput.AgentCom.substring(0, 8) + "' "
				+ "with ur";
		String mStateflag = new ExeSQL().getOneValue(mSQL);
		if (!mStateflag.equals("8")) { 
			mSQL = "select ReceiveCom from lzcard where certifyCode in ('HB07/07B','YB12/10B') "
				+ "and startno<='" + mLCCont.getChildText("ProposalContNo") + "' "
				+ "and endno>='" + mLCCont.getChildText("ProposalContNo") + "' with ur";
			String mReceiveCom = new ExeSQL().getOneValue(mSQL);
			String tReceiveCom = "E" + cGlobalInput.AgentCom.substring(0, 8);
			if(!mReceiveCom.equals("") && !mReceiveCom.equals(tReceiveCom)){
				throw new MidplatException("单证发放错误，请将保单号下发至网点: " + cGlobalInput.AgentCom.substring(0, 8));
			}else {
				throw new MidplatException("保单合同印刷号未下发，或状态无效！");
			}
		}
		cLogger.info("Out YbtContBL.checkProposalContNo!");
		return true;
	}
	/***
	 * 校验：保单印刷号PrtNo 是否符合规则
	 * ****/
	private boolean checkPrtNo(String nPrtNo) throws Exception {
		cLogger.info("Into YbtContBL.checkPrtNo!");
		if (nPrtNo.length() != 13 && nPrtNo.length() != 12) {
			throw new MidplatException("投保单印刷号长度需12位或13位！");
		}
		if (!nPrtNo.matches("[0-9]{13}") && !nPrtNo.matches("[0-9]{12}")) {
			throw new MidplatException("投保单印刷号中不能含字符！");
		}

		int length = nPrtNo.length();
		int sum = 0;
		for (int i = 0; i < length - 1; i++) {
			int num = Integer.parseInt(nPrtNo.substring(i, i + 1));
			sum += num;
		}

		if (nPrtNo.length() == 12) {
			if (sum % 10 != Integer.parseInt(nPrtNo.substring(11))) {
				throw new MidplatException("投保单印刷号不符合规则！");
			}
		}
		if (nPrtNo.length() == 13) {
			if (sum % 10 != Integer.parseInt(nPrtNo.substring(12))) {
				throw new MidplatException("投保单印刷号不符合规则！");
			}
		}
		cLogger.info("Out YbtContBL.checkPrtNo!");
		return true;
	}
	
	private VData getContBLVData() throws MidplatException {
		cLogger.info("Into YbtContBL.getContBLVData()...");

		Element mLCCont = cInXmlDoc.getRootElement().getChild(LCCont);
		Element mLCAppnt = mLCCont.getChild(LCAppnt);	//投保人信息
		//增加缴费账户户名必须与投保人姓名一致校验
		String AccName = mLCCont.getChildTextTrim("AccName");//户名
		String mAppntName = mLCAppnt.getChildTextTrim("AppntName");//投保人名称
		if(null !=AccName && !"".equals(AccName))
		{
			if(!AccName.equals(mAppntName))
				{
					throw new MidplatException("账号持有人必须为投保人本人!");
				}
		}
		
		//保单信息
		LCContSchema mLCContSchema = getLCContSchema(cInXmlDoc);

		//投保人
		LCAppntSchema mLCAppntSchema = new LCAppntSchema();
		mLCAppntSchema.setAppntNo("");	//注意此处必须设置！picch核心判断AppntNo为""时才进行投保人五要素判断，AppntNo为null时会直接生成新客户号
		mLCAppntSchema.setPrtNo(mLCCont.getChildText(PrtNo));
		mLCAppntSchema.setAppntName(mLCAppnt.getChildText(AppntName));
		mLCAppntSchema.setAppntSex(mLCAppnt.getChildText(AppntSex));
		mLCAppntSchema.setAppntBirthday(mLCAppnt.getChildText(AppntBirthday));
		mLCAppntSchema.setIDType(mLCAppnt.getChildText(AppntIDType));
		mLCAppntSchema.setIDNo(mLCAppnt.getChildText(AppntIDNo));
		mLCAppntSchema.setOccupationCode(mLCAppnt.getChildText(JobCode));
		String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + mLCAppnt.getChildText(JobCode) + "' with ur";
		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
		if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
			throw new MidplatException("未查到职业类别！职业代码为：" + mLCAppnt.getChildText(JobCode));
		}
		mLCAppntSchema.setOccupationType(mOccupationTypeStr);	//职业类别;

		//投保人地址
		LCAddressSchema mAppntAddress = new LCAddressSchema();
		mAppntAddress.setCompanyPhone(mLCAppnt.getChildText(AppntOfficePhone));
		mAppntAddress.setMobile(mLCAppnt.getChildText(AppntMobile));
		mAppntAddress.setPhone(mLCAppnt.getChildText(AppntPhone));
		mAppntAddress.setHomePhone(mLCAppnt.getChildText(AppntPhone));
		mAppntAddress.setPostalAddress(mLCAppnt.getChildText(MailAddress));
		mAppntAddress.setZipCode(mLCAppnt.getChildText(MailZipCode));
		mAppntAddress.setHomeAddress(mLCAppnt.getChildText(HomeAddress));
		mAppntAddress.setHomeZipCode(mLCAppnt.getChildText(HomeZipCode));
		mAppntAddress.setEMail(mLCAppnt.getChildText(Email));
		//添加投保人武断地址
		if( cInXmlDoc.getRootElement().getChild(BaseInfo).getChildTextTrim("BankCode").equals("03")){
	         cLogger.info("开始增加建行投被保人,受益人分段信息!");
	       String app_GrpName =  mLCAppnt.getChildText("GrpName");       //国家 
	       String app_PostAlprovince =  mLCAppnt.getChildText("PostalProvince");  //省
	       String app_PostAlcity =  mLCAppnt.getChildText("PostalCity");   //市  
	       String app_PostAlcounty =  mLCAppnt.getChildText("PostalCounty"); //县
	       String app_PostalCommunity = mLCAppnt.getChildText("PostalCommunity"); //通讯地址
	       cLogger.info("获取到的五段信息::"+app_GrpName+"-->"+app_PostAlprovince+"-->"+app_PostAlcity+"-->"+
	    		   app_PostAlcounty+"-->"+app_PostalCommunity);
	       String query_adreaa = "select codename from ldcode where codetype='ybtaddress' and  code = \'";
	       ExeSQL exe = new ExeSQL(); 
	       mAppntAddress.setGrpName(exe.getOneValue(query_adreaa +(app_GrpName.equals("") ? "00000000" : app_GrpName)+"\'"));                         //国家
	       mAppntAddress.setPostalProvince(exe.getOneValue(query_adreaa +(app_PostAlprovince.equals("") ? "00000000" : app_PostAlprovince)+"\'"));    //省
	       mAppntAddress.setPostalCity(exe.getOneValue(query_adreaa +(app_PostAlcity.equals("") ? "00000000" : app_PostAlcity)+"\'"));                //市
	       mAppntAddress.setPostalCounty(exe.getOneValue(query_adreaa +(app_PostAlcounty.equals("") ? "00000000" : app_PostAlcounty)+"\'"));          //县
	       mAppntAddress.setPostalCommunity(exe.getOneValue(query_adreaa +(app_PostalCommunity.equals("") ? "00000000" : app_PostalCommunity)+"\'")); //详细地址
	       
	       cLogger.info("结束增加建行投被保人,受益人分段信息!");
		}
//		if (cGlobalInput.ManageCom.startsWith("8644")) {
//			String mAppntPhone = mAppntAddress.getPhone();
//			String mAppntOfficePhone = mAppntAddress.getCompanyPhone();
//			String mAppntMobile = mAppntAddress.getMobile();
//			if ((mAppntPhone == null || mAppntPhone.equals(""))
//					&& (mAppntOfficePhone == null || mAppntOfficePhone.equals(""))
//					&& (mAppntMobile == null || mAppntMobile.equals(""))) 
//			{
//				throw new MidplatException("投保人联系电话为空！");
//			}
//			Pattern pattern = Pattern.compile("[0-9]*");
//			Matcher tMatches1 = pattern.matcher(mAppntAddress.getPostalAddress());
//			Matcher tMatches2 = pattern.matcher(mAppntAddress.getHomeAddress());
//			boolean tPostalAddress = tMatches1.matches();
//			boolean tHomeAddress = tMatches2.matches();
//			if (tPostalAddress || tHomeAddress
//					|| mAppntAddress.getPostalAddress().getBytes().length < 18
//					|| mAppntAddress.getHomeAddress().getBytes().length < 18) {
//				throw new MidplatException("投保人联系地址少于9个汉字或全为数字，请核对");
//			}
//			if("0".equals(mLCAppntSchema.getIDType())){
//				String tSql = "select appntname from lcappnt where idtype = '0' and idno = '"+mLCAppntSchema.getIDNo()+"' with ur";
//				ExeSQL mExeSQL = new ExeSQL();
//				SSRS mSSRS = mExeSQL.execSQL(tSql);
//				for(int i=1;i<=mSSRS.MaxRow;i++){
//					if(!mSSRS.GetText(i, 1).equals(mLCAppntSchema.getAppntName())){
//						throw new MidplatException("投保人身份证与多人重复");
//					}
//				}
//			}
//		}
		// 投保帐户
		LCAccountSchema mLCAccountSchema = new LCAccountSchema();
		mLCAccountSchema.setBankCode(mLCContSchema.getBankCode());
		mLCAccountSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLCAccountSchema.setAccName(mLCContSchema.getAccName());
		mLCAccountSchema.setAccKind("Y");

		//投保人个人信息
		LDPersonSchema mAppntPerson = new LDPersonSchema();
		mAppntPerson.setName(mLCAppntSchema.getAppntName());
		mAppntPerson.setSex(mLCAppntSchema.getAppntSex());
		mAppntPerson.setIDNo(mLCAppntSchema.getIDNo());
		mAppntPerson.setIDType(mLCAppntSchema.getIDType());
		mAppntPerson.setBirthday(mLCAppntSchema.getAppntBirthday());

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("GrpNo", "");
		mTransferData.setNameAndValue("GrpName", "");

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(mLCContSchema);
		mVData.add(mLCAppntSchema);
		mVData.add(mAppntAddress);
		mVData.add(mLCAccountSchema);
//		mVData.add(mLCCustomerImpartSet);
		mVData.add(mAppntPerson);
		mVData.add(mTransferData);

		cLogger.info("Out YbtContBL.getContBLVData()!");
		return mVData;
	}
	
	private LCContSchema getLCContSchema(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into YbtContBL.getLCContSchema()...");
		
		Element mTranData = cInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		Element mLCAppnt = mLCCont.getChild(LCAppnt);	//投保人信息
		Element mLCInsured = mLCCont.getChild(LCInsureds).getChild(LCInsured);	//被保人信息
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		
		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema.setGrpContNo("00000000000000000000");
		mLCContSchema.setContNo(mLCCont.getChildText(ProposalContNo));	//用保单合同印刷号录单
		mLCContSchema.setProposalContNo(mLCCont.getChildText(ProposalContNo));
		mLCContSchema.setPrtNo(mLCCont.getChildText(PrtNo));
		mLCContSchema.setContType("1");	//保单类别(个单-1，团单-2)
		mLCContSchema.setPolType("0");
		
		//后期工行网银ATM需要需要将下面放开
		String cadflag = mLCCont.getChildTextTrim("SourceType");
		if( cadflag !=null ){
			if(cadflag.equals("1")){
				mLCContSchema.setCardFlag("c");	 //新增网银渠道
			}
			else if(cadflag.equals("8")){
				mLCContSchema.setCardFlag("d");	 //新增ATM渠道
			}
			else if("e".equals(cadflag)){//add by zengzm 2016-11-23 针对手机银行渠道
				mLCContSchema.setCardFlag("e");
			}
			else if("f".equals(cadflag)){
				mLCContSchema.setCardFlag("f");
			}
			else if(!cadflag.equals("8") 
					&& !cadflag.equals("1") 
					&& !cadflag.equals("e") 
					&& !cadflag.equals("f")){//add by zengzm 2016-11-23 针对手机银行渠道
				mLCContSchema.setCardFlag("9");	
			}
		}
		else{
			mLCContSchema.setCardFlag("9");	//渠道"04"，CardFlag"9"，共同识别银保通交易！
		}
		
		
		mLCContSchema.setManageCom(cGlobalInput.ManageCom);
		mLCContSchema.setAgentCom(cGlobalInput.AgentCom);
		LAComToAgentDB mLAComToAgentDB = new LAComToAgentDB();
		mLAComToAgentDB.setAgentCom(cGlobalInput.AgentCom);
		mLAComToAgentDB.setRelaType("1");
		LAComToAgentSet mLAComToAgentSet = mLAComToAgentDB.query();
		if (mLAComToAgentSet.mErrors.needDealError()
				|| (null==mLAComToAgentSet) || (mLAComToAgentSet.size()<1)) {
      	cLogger.error(mLAComToAgentDB.mErrors.getFirstError());
      	throw new MidplatException("未查到相应银行网点的专管员信息！");
		}
		mLCContSchema.setAgentCode(mLAComToAgentSet.get(1).getAgentCode());
		mLCContSchema.setAgentGroup(mLAComToAgentSet.get(1).getAgentGroup());
		//工行增加代理销售业务员编码(AgentSaleCode)数据节点
//		mLCContSchema.setAgentSaleCode(mLCCont.getChildText("AgentSaleCode"));
		mLCContSchema.setSaleChnl("04");	//渠道"04"，CardFlag"9"，共同识别银保通交易！
		//泉州银行将511101的销售渠道从银代渠道切换成团险中介
		if( cGlobalInput.ManageCom.startsWith("8635") && cGlobalInput.AgentCom.startsWith("PY053")){
			List lis_Risks  = mLCInsured.getChild("Risks").getChildren("Risk");
			for(int i=0;i<lis_Risks.size();i++)
			{
				Element lis_Risk = (Element)lis_Risks.get(i);
				if(lis_Risk.getChildText("RiskCode").equals("511101") && lis_Risk.getChildText("MainRiskCode").equals("511101"))
				{
					mLCContSchema.setSaleChnl("03");
					mLCContSchema.setCardFlag("a");	
					break;
				}
			}
		}
		
		
		mLCContSchema.setPassword(mLCCont.getChildText(Password));	//保单密码
		mLCContSchema.setInputOperator(cGlobalInput.Operator);
		mLCContSchema.setInputDate(PubFun.getCurrentDate());
		mLCContSchema.setInputTime(PubFun.getCurrentTime());
		//投保人
		mLCContSchema.setAppntName(mLCAppnt.getChildText(AppntName));
		mLCContSchema.setAppntSex(mLCAppnt.getChildText(AppntSex));
		mLCContSchema.setAppntBirthday(mLCAppnt.getChildText(AppntBirthday));
		mLCContSchema.setAppntIDType(mLCAppnt.getChildText(AppntIDType));
		mLCContSchema.setAppntIDNo(mLCAppnt.getChildText(AppntIDNo));
		//被保人
		mLCContSchema.setInsuredName(mLCInsured.getChildText(Name));
		mLCContSchema.setInsuredSex(mLCInsured.getChildText(Sex));
		mLCContSchema.setInsuredBirthday(mLCInsured.getChildText(Birthday));
		mLCContSchema.setInsuredIDType(mLCInsured.getChildText(IDType));
		mLCContSchema.setInsuredIDNo(mLCInsured.getChildText(IDNo));
		mLCContSchema.setPayIntv(mLCCont.getChildText(PayIntv));	//缴费方式
		mLCContSchema.setPayMode("4");
		mLCContSchema.setPayLocation("0");	//银行转帐
//		String mSQL = "select BankCode from LDBank where 1=1 "
//			+ "and BankCode like '" + mBaseInfo.getChildText(BankCode) + "%' " 
//			+ "and ComCode='"+ cGlobalInput.ComCode + "' "
//			+ "and CansendFlag='1' "
//			+ "with ur";
//		mLCContSchema.setBankCode(new ExeSQL().getOneValue(mSQL));
//		if ((null==mLCContSchema.getBankCode()) || mLCContSchema.getBankCode().equals("")) {
//			mLCContSchema.setBankCode(mBaseInfo.getChildText(BankCode));
//		}
		String mBankcode = mBaseInfo.getChildText(BankCode);
		String mSql = "select * from (Select Bankcode, 2 Ord From Ldbank Where Bankcode Like '"+mBankcode+"%' And" 
				+" Comcode = '"+cGlobalInput.ComCode+"' And Cansendflag = '1' And"
				+ " (Bankuniteflag Is Null Or Bankuniteflag <> '1')"
				+ " Union All Select Bankcode, 3 Ord From Ldbank Where Bankcode Like '"+mBankcode+"%' And" 
				+" Comcode = Substr('"+cGlobalInput.ComCode+"', 1, 4) And Cansendflag = '1' And" 
				+" (Bankuniteflag Is Null Or Bankuniteflag <> '1')"
				+ " Union All Select Bankcode, 4 Ord From Ldbank Where Bankcode Like '"+mBankcode+"%' And " 
				+" Comcode = Substr('"+cGlobalInput.ComCode+"', 1, 4) And Cansendflag = '2' And" 
				+" (Bankuniteflag Is Null Or Bankuniteflag <> '1')"
				+ ") a order by ord With Ur";
		SSRS tResult = new ExeSQL().execSQL(mSql);
		
		if(tResult.MaxRow>0){
			mLCContSchema.setBankCode(tResult.GetText(1, 1));
		}else{
			mLCContSchema.setBankCode(mBaseInfo.getChildText(BankCode));
		}
		mLCContSchema.setBankAccNo(mLCCont.getChildText(BankAccNo));	//投保帐户号
		mLCContSchema.setAccName(mLCCont.getChildText(AccName));	//投保帐户姓名
		mLCContSchema.setPrintCount(0);
		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));
		GregorianCalendar mNowCalendar = new GregorianCalendar();
		mNowCalendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
		mLCContSchema.setCValiDate(mNowCalendar.getTime());	//保单生效日期(当前日期的下一天)
		mLCContSchema.setPolApplyDate(mCurrentDate);	//投保日期
		mLCContSchema.setProposalType("01");	//投保书类型：01-普通个单投保书
		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));	//特别约定
		
		cLogger.info("Out YbtContBL.getLCContSchema()!");
		return mLCContSchema;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "E:\\workplace\\wuzhen_2011\\报文格式\\abc_99001217000000007222_23_103033.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mXmlDoc = new SAXBuilder().build(mIsr);
		
		Element mTranData = mXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
		mLCCont.getChild("PrtNo").setText(mProposalNo);
		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
		
		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				mBaseInfo.getChildText("BankCode"),
				mBaseInfo.getChildText("ZoneNo"),
				mBaseInfo.getChildText("BrNo"));
		
		new YbtContBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
