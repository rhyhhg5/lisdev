/**
 * 保单自核和复核！
 */

package com.sinosoft.midplat.kernel.service.newCont;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.cbcheck.UWAutoChkBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbtAutoCheck implements XmlTag {
	private final Logger cLogger = Logger.getLogger(getClass());
	
	private final GlobalInput cGlobalInput;
	private final LCContSchema cLCContSchema;
	
	public YbtAutoCheck(String pPrtNo, GlobalInput pGlobalInput) throws MidplatException {
		cGlobalInput = pGlobalInput;
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(pPrtNo);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
	}
	
	public void deal() throws Exception {
		cLogger.info("Into YbtAutoCheck.deal()...");

		/**
		 * 清理自核报错记录！(相同保单印刷号反复录单，存储自核报错时，存在主键冲突，在此提前清除可能存在的历史报错记录)
		 * ChenGB(陈贵菠) 2008.08.18
		 */
		String checkSQL="select  contno from lccont where contno='"+ cLCContSchema.getContNo()+"'"
        +" and managecom like '8632%' "
        +" and salechnl in (select  code  from ldcode  where  codetype='JSZJsalechnl')"; 
			ExeSQL tExeSQL= new ExeSQL();
			SSRS tSSRS = tExeSQL.execSQL(checkSQL);
			if(tSSRS.getMaxRow()>0){
			TransferData  tdata=new TransferData();
			tdata.setNameAndValue("ContNo", tSSRS.GetText(1, 1));
			VData vdata=new VData();
			vdata.add(tdata);     
			ContInputAgentcomChkBL cacb=new ContInputAgentcomChkBL();
			if(!cacb.submitData(vdata, "check")){
			System.out.println(cacb.mErrors.getError(0).errorMessage);
			throw new MidplatException(cacb.mErrors.getError(0).errorMessage);
			}
			}
		 
		 
		
		MMap mMMap = new MMap();
		mMMap.put("delete from LCCUWError where ContNo='" + cLCContSchema.getContNo() + "' with ur", "DELETE"); // 保单级别自核报错
		mMMap.put("delete from LCUWError where ContNo='" + cLCContSchema.getContNo() + "' with ur", "DELETE"); // 险种级别自核报错
		VData mSubmitVData = new VData();
		mSubmitVData.add(mMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("清理自核错误记录失败！");
		}
		
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("ContNo", cLCContSchema.getContNo());
		mTransferData.setNameAndValue("BankInsu", "1");
		
		VData mUWAutoChkBLData = new VData();
		mUWAutoChkBLData.add(cGlobalInput);
		mUWAutoChkBLData.add(cLCContSchema);
		
		UWAutoChkBL mUWAutoChkBL = new UWAutoChkBL();
		cLogger.info("Start call UWAutoChkBL.submitData()...");
		long tStartMillism = System.currentTimeMillis();
		if (!mUWAutoChkBL.submitData(mUWAutoChkBLData, "submit")) {
			throw new MidplatException(mUWAutoChkBL.mErrors.getFirstError());
		}
		cLogger.info("UWAutoChkBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s");
		cLogger.info("End call UWAutoChkBL.submitData()!");
		
		/**
		 * 获取自核报错信息！(自核报错描述在数据库中，AutoUWCheckBL并不返回！) ChenGB(陈贵菠) 2008.08.18
		 */
		String mSQL = "select UWFlag from LCCont where ContNo='" + cLCContSchema.getContNo() + "' with ur";
		ExeSQL mExeSQL = new ExeSQL();
		if (!mExeSQL.getOneValue(mSQL).equals("9")) { // 自核标识(UWFlag)：9-通过；5-失败
			//---------- 增加工行自核不过转人工核保返回值   add by 20130725 wz  只用于测试系统 ---------
			cLCContSchema.setRemark("1222");
			LCContDB db = new LCContDB();
			db.setSchema(cLCContSchema);
			db.update();
			//-------------- end ---------------
			mSQL = "select UWError from LCCUWError where ContNo='" + cLCContSchema.getContNo() + "' fetch first 1 rows only with ur";
			String tErrorMsg = mExeSQL.getOneValue(mSQL);
			if (!tErrorMsg.equals("")) {
				throw new MidplatException(tErrorMsg);
			}
			mSQL = "select UWError from LCUWError where ContNo='" + cLCContSchema.getContNo() + "' fetch first 1 rows only with ur";
			tErrorMsg = mExeSQL.getOneValue(mSQL);
			throw new MidplatException(tErrorMsg);
		}
		
		//保单复核
		String mApproveDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mApproveTime = DateUtil.getCurDate("HH:mm:ss");
		mMMap = new MMap();
		mMMap.put("update lccont set ApproveFlag='9', ApproveDate='" + mApproveDate
				+ "', ApproveTime='" + mApproveTime
				+ "', ModifyTime='" + mApproveTime
				+ "' where contno='" + cLCContSchema.getContNo() + "'", "UPDATE");
		mMMap.put("update lcpol set ApproveFlag='9', ApproveDate='" + mApproveDate
				+ "', ApproveTime='" + mApproveTime
				+ "', ModifyTime='" + mApproveTime
				+ "' where contno='" + cLCContSchema.getContNo() + "'", "UPDATE");
		mSubmitVData = new VData();
		mSubmitVData.add(mMMap);
		mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("保单复核失败！");
		}
		
		cLogger.info("Out YbtAutoCheck.deal()!");
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "D:/request/ICBC_std/UW.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mXmlDoc = new SAXBuilder().build(mIsr);
		
		Element mTranData = mXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
		mLCCont.getChild("PrtNo").setText(mProposalNo);
		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
		
		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				mBaseInfo.getChildText("BankCode"),
				mBaseInfo.getChildText("ZoneNo"),
				mBaseInfo.getChildText("BrNo"));
		
		new YbtAutoCheck(mLCCont.getChildTextTrim("PrtNo"), mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
