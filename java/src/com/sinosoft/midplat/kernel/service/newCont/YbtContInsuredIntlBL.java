/**
 * 录入被保人和险种信息！
 */


package com.sinosoft.midplat.kernel.service.newCont;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.intltb.ContInsuredIntlBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LCRiskFeeRateSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.vdb.LCRiskFeeRateDBSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDiseaseResultSet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbtContInsuredIntlBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(YbtContInsuredIntlBL.class);
	
	private final Document cInXmlDoc;
	private final GlobalInput cGlobalInput;
	
	private LCContSchema cLCContSchema = null;
	
	public YbtContInsuredIntlBL(Document pInXmlDoc, GlobalInput pGlobalInput) {
		cInXmlDoc = pInXmlDoc;
		cGlobalInput = pGlobalInput;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into YbtContInsuredIntlBL.deal()...");
		
		Element mTranData = cInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		String mTransrNo = mBaseInfo.getChildText(TransrNo);
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
		
		List mLCInsuredList = cInXmlDoc.getRootElement()
			.getChild(LCCont).getChild(LCInsureds).getChildren(LCInsured);
		for (int i = 0; i < mLCInsuredList.size(); i++) {
			Element tLCInsured = (Element) mLCInsuredList.get(i);
			
			checkContInsuredIntlBL(tLCInsured);
			
			VData tContInsuredIntlBLVData = getContInsuredIntlBLVData(tLCInsured);
			ContInsuredIntlBL tContInsuredIntlBL   = new ContInsuredIntlBL();
			cLogger.info("Start call ContInsuredIntlBL.submitData()...");
			long tStartMillism = System.currentTimeMillis();
			if (!tContInsuredIntlBL.submitData(tContInsuredIntlBLVData, "INSERT||CONTINSURED")) {
				throw new MidplatException(tContInsuredIntlBL.mErrors.getFirstError());
			}
			cLogger.info("ContInsuredIntlBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s"
					+ "；交易流水号：" + mTransrNo);
			cLogger.info("End call ContInsuredIntlBL.submitData()!");
		}
		
		cLogger.info("Out YbtContInsuredIntlBL.deal()!");
	}
	
	private void checkContInsuredIntlBL(Element pLCInsured) throws MidplatException {
		cLogger.info("Into YbtContInsuredIntlBL.checkContInsuredIntlBL()...");
		
		if (!pLCInsured.getChildText(RelaToMain).equals("00")) {
			throw new MidplatException("与主被保人关系必须为本人！");
		}

		//校验险种数据
		List mRiskList = pLCInsured.getChild(Risks).getChildren(Risk);
		for (int i = 0; i < mRiskList.size(); i++) {
			Element tRisk = (Element) mRiskList.get(i);
			
			String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
			if (tMainRiskCodeStr.equals("331201")) {	//健康人生个人护理保险(万能型，B款)
				checkRisk331201(tRisk);
			} else if (tMainRiskCodeStr.equals("331301")) {	//健康人生个人护理保险(万能型，C款)
				checkRisk331301(tRisk);
			}
			
			//校验受益人
			List tLCBnfList = tRisk.getChild(LCBnfs).getChildren(LCBnf);
			for (int j = 0; j < tLCBnfList.size(); j++) {
				Element ttLCBnf = (Element) tLCBnfList.get(j);
				if (ttLCBnf.getChildText(RelationToInsured).equals("00")) {
					throw new MidplatException("受益人和被保人的关系不能为本人!");
				}
			}
		}
		
		cLogger.info("Out YbtContInsuredIntlBL.checkContInsuredIntlBL()!");
	}
	
	/**
	 * 健康人生个人护理保险(万能型，B款)
	 */
	private void checkRisk331201(Element pRisk) throws MidplatException {
		cLogger.info("Into YbtContInsuredIntlBL.checkRisk331201()...");

		if (!pRisk.getChildText(InsuYearFlag).equals("A")) {
			throw new MidplatException("保险年期年龄标识有误！应为年龄缴(A)");
		}

		cLogger.info("Out YbtContInsuredIntlBL.checkRisk331201()!");
	}
	
	/**
	 * 健康人生个人护理保险(万能型，C款)
	 */
	private void checkRisk331301(Element pRisk) throws MidplatException {
		cLogger.info("Into YbtContInsuredIntlBL.checkRisk331301()...");

		

		cLogger.info("Out YbtContInsuredIntlBL.checkRisk331301()!");
	}

	private VData getContInsuredIntlBLVData(Element pLCInsured) throws MidplatException {
		cLogger.info("Into YbtContInsuredIntlBL.getContInsuredIntlBLVData()...");

		//被保人
		LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
		mLCInsuredSchema.setName(pLCInsured.getChildText(Name));
		mLCInsuredSchema.setSex(pLCInsured.getChildText(Sex));
		mLCInsuredSchema.setBirthday(pLCInsured.getChildText(Birthday));
		mLCInsuredSchema.setIDType(pLCInsured.getChildText(IDType));
		mLCInsuredSchema.setIDNo(pLCInsured.getChildText(IDNo));
		
		//add by zengzm 2016-05-27针对百万安行校验被保人年收入特殊添加
		String mIncome = pLCInsured.getChildText("Income");
		String mainRiskCode = pLCInsured.getChild("Risks").getChild("Risk").getChildText("MainRiskCode");
		if("333501".equals(mainRiskCode)||"WX0012".equals(mainRiskCode)||"JZJ001".equals(mainRiskCode)||"333502".equals(mainRiskCode)){
			if(mIncome == null || "".equals(mIncome)){
				throw new MidplatException("被保人年收入必须录入!");
			}else{
				double mSalary = Double.parseDouble(mIncome)/10000.0;//被保人表中Salary是以万元存储的
				mLCInsuredSchema.setSalary(mSalary);
			}
		}
		
		mLCInsuredSchema.setOccupationCode(pLCInsured.getChildText(JobCode));
		String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + pLCInsured.getChildText(JobCode) + "' with ur";
		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
		if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
			throw new MidplatException("未查到职业类别！职业代码为：" + pLCInsured.getChildText(JobCode));
		}
		mLCInsuredSchema.setOccupationType(mOccupationTypeStr);	//职业类别;
		mLCInsuredSchema.setPrtNo(cLCContSchema.getPrtNo());
		mLCInsuredSchema.setContNo(cLCContSchema.getContNo());
		mLCInsuredSchema.setRelationToMainInsured(pLCInsured.getChildText(RelaToMain));
		mLCInsuredSchema.setRelationToAppnt(pLCInsured.getChildText(RelaToAppnt));

		//被保人地址
		LCAddressSchema mInsuredAddress = new LCAddressSchema();
		mInsuredAddress.setHomeAddress(pLCInsured.getChildText(HomeAddress));
		mInsuredAddress.setPostalAddress(pLCInsured.getChildText(MailAddress));
		mInsuredAddress.setZipCode(pLCInsured.getChildText(MailZipCode));
		String mRiskCode = pLCInsured.getChild("Risks").getChild("Risk").getChildText("MainRiskCode");
		if("333001".equals(mRiskCode)){//针对万能H款将移动电话付给联系电话
			mInsuredAddress.setPhone(pLCInsured.getChildText(InsuredMobile));
		}else{
			mInsuredAddress.setPhone(pLCInsured.getChildText(HomePhone));
		}
		
		mInsuredAddress.setHomeZipCode(pLCInsured.getChildText(HomeZipCode));
		mInsuredAddress.setHomePhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setCompanyPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setMobile(pLCInsured.getChildText(InsuredMobile));
		mInsuredAddress.setEMail(pLCInsured.getChildText(Email));
		
		if( cInXmlDoc.getRootElement().getChild(BaseInfo).getChildTextTrim("BankCode").equals("03")){
	         cLogger.info("开始增加建行投被保人,受益人分段信息!");
	       String app_GrpName =  pLCInsured.getChildText("GrpName");       //国家 
	       String app_PostAlprovince =  pLCInsured.getChildText("PostalProvince");  //省
	       String app_PostAlcity =  pLCInsured.getChildText("PostalCity");   //市  
	       String app_PostAlcounty =  pLCInsured.getChildText("PostalCounty"); //县
	       String app_PostalCommunity = pLCInsured.getChildText("PostalCommunity"); //详细地址
	       cLogger.info("获取到的五段信息::"+app_GrpName+"-->"+app_PostAlprovince+"-->"+app_PostAlcity+"-->"+
	    		   app_PostAlcounty+"-->"+app_PostalCommunity);
	       String query_adreaa = "select codename from ldcode where codetype='ybtaddress' and  code = \'";
	       ExeSQL exe = new ExeSQL(); 
	       mInsuredAddress.setGrpName(exe.getOneValue(query_adreaa +(app_GrpName.equals("") ? "00000000" : app_GrpName)+"\'"));                         //国家
	       mInsuredAddress.setPostalProvince(exe.getOneValue(query_adreaa +(app_PostAlprovince.equals("") ? "00000000" : app_PostAlprovince)+"\'"));    //省
	       mInsuredAddress.setPostalCity(exe.getOneValue(query_adreaa +(app_PostAlcity.equals("") ? "00000000" : app_PostAlcity)+"\'"));                //市
	       mInsuredAddress.setPostalCounty(exe.getOneValue(query_adreaa +(app_PostAlcounty.equals("") ? "00000000" : app_PostAlcounty)+"\'"));          //县
	       mInsuredAddress.setPostalCommunity(app_PostalCommunity); //详细地址
	       
	       cLogger.info("结束增加建行投被保人,受益人分段信息!");
		}
		

		//被保人个人信息
		LDPersonSchema mInsuredPerson = new LDPersonSchema();
		mInsuredPerson.setCustomerNo("");	//注意此处必须设置！picch核心判断CustomerNo为""时才进行被保人五要素判断，CustomerNo为null时会直接生成新客户号
		mInsuredPerson.setName(mLCInsuredSchema.getName());
		mInsuredPerson.setSex(mLCInsuredSchema.getSex());
		mInsuredPerson.setIDNo(mLCInsuredSchema.getIDNo());
		mInsuredPerson.setIDType(mLCInsuredSchema.getIDType());
		
		//add by zengzm 2016-05-27针对百万安行校验被保人年收入特殊添加
		if("333501".equals(mainRiskCode)||"WX0012".equals(mainRiskCode)||"JZJ001".equals(mainRiskCode)||"333502".equals(mainRiskCode)){
			mInsuredPerson.setSalary(mLCInsuredSchema.getSalary());
		}
		
		mInsuredPerson.setBirthday(mLCInsuredSchema.getBirthday());
		mInsuredPerson.setOccupationCode(mLCInsuredSchema.getOccupationCode());
		mInsuredPerson.setOccupationType(mLCInsuredSchema.getOccupationType());	//职业类别

		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setSchema(mLCInsuredSchema);
		/*
		 * 
		 
		//被保人告知信息关于《未成年被保险人在其他保险公司累计身故保额》的问题 add by chenjw 2011-5-4 
		LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
		Element mInsCovSumAmt = pLCInsured.getChild("InsCovSumAmt");
		if(mInsCovSumAmt != null){ 
			System.out.println("存在未成年被保险人在其他保险公司累计身故保额节点");
			LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
			tLCCustomerImpartSchema.setGrpContNo("00000000000000000000");
			tLCCustomerImpartSchema.setContNo(cLCContSchema.getContNo());
			tLCCustomerImpartSchema.setProposalContNo(cLCContSchema.getProposalContNo());
			tLCCustomerImpartSchema.setPrtNo(cLCContSchema.getPrtNo());
			tLCCustomerImpartSchema.setImpartCode("250");
			tLCCustomerImpartSchema.setImpartVer("001");
			tLCCustomerImpartSchema.setImpartContent("(18周岁以下未成年人回答)是否已拥有正在生效的以死亡为给付保险金条件的人身保险？如有请告知身故保险金额总额。○是 ○否，保额__万元");
			tLCCustomerImpartSchema.setImpartParamModle("N,N");
			tLCCustomerImpartSchema.setCustomerNo(cLCContSchema.getInsuredNo());
			tLCCustomerImpartSchema.setCustomerNoType("I");
			String tInsCovSumAmt = mInsCovSumAmt.getTextTrim();
			if(!"".equals(tInsCovSumAmt)&&tInsCovSumAmt!=null){
				System.out.println("未成年被保险人在其他保险公司累计身故保额节点值不为空");
			   if(Double.parseDouble(tInsCovSumAmt)>0){
				   System.out.println("未成年被保险人在其他保险公司累计身故保额大于0");
				   tLCCustomerImpartSchema.setImpartParamModle("Y,"+Double.parseDouble(tInsCovSumAmt)/10000);
			   }	
			}
			mLCCustomerImpartSet.add(tLCCustomerImpartSchema);		
		}
		//被保人告知信息关于《未成年被保险人在其他保险公司累计身故保额》的问题 add by chenjw 2011-5-4 
		 * 
		 */

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("FamilyType", "0");
		mTransferData.setNameAndValue("PolTypeFlag", "0");
		mTransferData.setNameAndValue("SequenceNo", "1");
		mTransferData.setNameAndValue("ContType", "1");
		mTransferData.setNameAndValue("SavePolType", "0");
		
		//添加红利领取方式 add by GaoJinfu 2018-02-02
		mTransferData.setNameAndValue("Bonusgetmode", pLCInsured.getChild(Risks).getChild(Risk).getChildTextTrim("BonusGetMode"));

		//套餐信息
		Element mRisk = pLCInsured.getChild(Risks).getChild(Risk);
		
		/***  万能型H款增加比例 20130117 add by wz  **/
		List mRiskList = pLCInsured.getChild(Risks).getChildren(Risk);
		for (int i = 0; i < mRiskList.size(); i++) {
			Element ttRisk = (Element) mRiskList.get(i);
			String mMainRiskCode = ttRisk.getChildText("MainRiskCode");
			if(ttRisk.getChildText("RiskCode").equals(mMainRiskCode) && "333001".equals(mMainRiskCode)){
				LCRiskFeeRateDBSet ttRiskFeeRate =  getRiskFeeRate(ttRisk);
				if(!ttRiskFeeRate.insert()){
					throw new MidplatException("保单比例插入失败！");
				}
			}
		}
		
//		mSQL = "select RiskWrapCode from LDRiskWrap where RiskCode='" + mRisk.getChildText(MainRiskCode) + "' with ur";
		
		LDRiskDutyWrapDB mLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
		//网银和单独套餐有冲突需要解决! 
		if (mRisk.getChildText(MainRiskCode).equals("333502")) {
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0408");
		} else if (mRisk.getChildText(MainRiskCode).equals("232402")) {
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0409");
		} else {
			mLDRiskDutyWrapDB.setRiskWrapCode(mRisk.getChildText(MainRiskCode));
		}
		/*
		if(mRisk.getChildText(MainRiskCode).equals("JZJ001")
				||mRisk.getChildText(MainRiskCode).equals("JZJ002")
				||mRisk.getChildText(MainRiskCode).equals("JZJ003")
				||mRisk.getChildText(MainRiskCode).equals("JZJ004")
				||mRisk.getChildText(MainRiskCode).equals("YBT001")
				||mRisk.getChildText(MainRiskCode).equals("YBT002")
				||mRisk.getChildText(MainRiskCode).equals("YBT003")
				||mRisk.getChildText(MainRiskCode).equals("YBT004")//add by gaojinfu
				||mRisk.getChildText(MainRiskCode).equals("YBT005")
				||mRisk.getChildText(MainRiskCode).equals("YBT006")
				||mRisk.getChildText(MainRiskCode).equals("YBT007")
				||mRisk.getChildText(MainRiskCode).equals("YBT008")
				||mRisk.getChildText(MainRiskCode).equals("WR0347")
				||mRisk.getChildText(MainRiskCode).equals("WR0349")
				||mRisk.getChildText(MainRiskCode).equals("YBT009")//添加福满人生和健康一生套餐  add by GaoJinfu 20180202
				||mRisk.getChildText(MainRiskCode).equals("YBT010")//添加福满人生和健康一生套餐  add by GaoJinfu 20180202
				){	//工行网银渠道传进来的是套餐编码 故在此做判断
			mLDRiskDutyWrapDB.setRiskWrapCode(mRisk.getChildText(MainRiskCode));//注意:网银定义的是套餐编码
		}else if(mRisk.getChildText(MainRiskCode).equals("333502")){	//单独配置百万套餐 区别于网银 注意:其它网银是否也需要区别于套餐
//			mLDRiskDutyWrapDB.setRiskWrapCode("WR0280");
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0408");
		}else if(mRisk.getChildText(MainRiskCode).equals("232101")){//北肿防癌管家个人疾病保险（A款）投保规则（银保渠道专用）
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0299");
		}else if(mRisk.getChildText(MainRiskCode).equals("290201")){//北肿防癌管家个人疾病保险（B款）
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0309");
		}
		else if(mRisk.getChildText(MainRiskCode).equals("511101")){ //福佑相伴借款人（B款）意外伤害保险
			mLDRiskDutyWrapDB.setRiskWrapCode("XBT004");
		}
		else if(mRisk.getChildText(MainRiskCode).equals("232201")){ //add by zengzm 2016-08-15 关爱健康防癌个人疾病保险
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0316");
		}
		else if(mRisk.getChildText(MainRiskCode).equals("232402")){ //add by zengzm 2016-08-24 健康天使个人重大疾病保险
//			mLDRiskDutyWrapDB.setRiskWrapCode("WR0340");
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0409");
		}
		else if(mRisk.getChildText(MainRiskCode).equals("334901")){ //add by zengzm 2016-08-31 福利双全个人保障计划(B款)
			mLDRiskDutyWrapDB.setRiskWrapCode("WR0328");
		}
		else {
//			mLDRiskDutyWrapDB.setRiskWrapCode(new ExeSQL().getOneValue(mSQL));
		}*/
		LDRiskDutyWrapSet mLDRiskDutyWrapSet = mLDRiskDutyWrapDB.query();
		LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet(); 
		for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
			LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
			tLCRiskDutyWrapSchema.setRiskWrapCode(tLDRiskDutyWrapSchema.getRiskWrapCode());
			tLCRiskDutyWrapSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
			tLCRiskDutyWrapSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());
			tLCRiskDutyWrapSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());
			tLCRiskDutyWrapSchema.setCalFactorType(tLDRiskDutyWrapSchema.getCalFactorType());
			tLCRiskDutyWrapSchema.setCalFactorValue(
					mRisk.getChildText(tLDRiskDutyWrapSchema.getCalFactor()));
			
			mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
		}

		//受益人
		LCBnfSet mLCBnfSet = new LCBnfSet();
		List mLCBnfList = mRisk.getChild(LCBnfs).getChildren(LCBnf);
		for (int i = 0; i < mLCBnfList.size(); i++) {
			Element tLCBnf = (Element) mLCBnfList.get(i);
			
			LCBnfSchema tLCBnfSchema = new LCBnfSchema();
			tLCBnfSchema.setContNo(cLCContSchema.getContNo());
			tLCBnfSchema.setBnfType(tLCBnf.getChildText(BnfType));
			tLCBnfSchema.setBnfGrade(tLCBnf.getChildText(BnfGrade));
			tLCBnfSchema.setName(tLCBnf.getChildText(Name));
			tLCBnfSchema.setBirthday(tLCBnf.getChildText(Birthday));
			tLCBnfSchema.setSex(tLCBnf.getChildText(Sex));
			tLCBnfSchema.setIDType(tLCBnf.getChildText(IDType));
			tLCBnfSchema.setIDNo(tLCBnf.getChildText(IDNo));
			tLCBnfSchema.setRelationToInsured(tLCBnf.getChildText(RelationToInsured));
			tLCBnfSchema.setBnfLot(tLCBnf.getChildText(BnfLot));
			/*
			 * 核心要求的受益比例在0.0-1.0之间，而标准报文的受益比例为1-100，所以需要在此做转换
			 */
			tLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot()/100.0);
			mLCBnfSet.add(tLCBnfSchema);
		}

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(cLCContSchema);
		mVData.add(mLCInsuredSchema);
		mVData.add(mLCInsuredDB);
		mVData.add(mInsuredAddress);
		mVData.add(mInsuredPerson);
		mVData.add(mTransferData);
		mVData.add(mLCRiskDutyWrapSet);
		mVData.add(mLCBnfSet);
		mVData.add(new LCDiseaseResultSet());	//告知信息
		mVData.add(new LCNationSet());	//抵达国家

		cLogger.info("Out YbtContInsuredIntlBL.getContInsuredIntlBLVData()!");
		return mVData;
	}
	
	public LCRiskFeeRateDBSet getRiskFeeRate(Element ttRisk){
		cLogger.info("into YbtContInsuredIntlBL.getRiskFeeRate()!");
		
		Element pRisk = ttRisk;
		//万能型H款录入退保比例
		LCRiskFeeRateDBSet mLCRiskFeeList = new LCRiskFeeRateDBSet();
		String mMainRiskCode = pRisk.getChildText("MainRiskCode");
//		String tMainRiskCode = "";
		if(pRisk.getChildText("RiskCode").equals(mMainRiskCode) && "333001".equals(mMainRiskCode)){
//			cLogger.info("333001");
//			tMainRiskCode = mMainRiskCode;
			Date now = new Date();
			String nowDate = new SimpleDateFormat("yyyy-MM-dd").format(now);
			String nowTime = new SimpleDateFormat("HH:mm:ss").format(now);
			Element mWriteOffList = pRisk.getChild("WriteOffList");
			List mWriteOffs = mWriteOffList.getChildren("WriteOff");
			for (int i = 0; i < mWriteOffs.size(); i++) {
				Element mWriteOff = (Element) mWriteOffs.get(i);
				LCRiskFeeRateSchema mLCRiskFeeRate = new LCRiskFeeRateSchema();
				mLCRiskFeeRate.setPrtNo(mWriteOffList.getChildText("PrtNo"));
				mLCRiskFeeRate.setRiskWrapCode("WR0270");
				mLCRiskFeeRate.setRiskCode("WR0270");
				mLCRiskFeeRate.setFeeYear(mWriteOff.getChildText("Year"));
				mLCRiskFeeRate.setDrawRate(mWriteOff.getChildText("TbBnf"));
				mLCRiskFeeRate.setCancelRate(mWriteOff.getChildText("TbBnf"));
				mLCRiskFeeRate.setOperator("ybt");
				mLCRiskFeeRate.setMakeDate(nowDate);
				mLCRiskFeeRate.setMakeTime(nowTime);
				mLCRiskFeeRate.setModifyDate(nowDate); 
				mLCRiskFeeRate.setModifyTime(nowTime);
				
				mLCRiskFeeList.add(mLCRiskFeeRate);
			}
		}
		
		cLogger.info("Out YbtContInsuredIntlBL.getRiskFeeRate()!");
		return mLCRiskFeeList;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "C:\\Users\\zengzm\\Desktop\\68_2017050915510000_01_161243.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mXmlDoc = new SAXBuilder().build(mIsr);
		
		Element mTranData = mXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
		mLCCont.getChild("PrtNo").setText(mPrtNo);
		mLCCont.getChild("ProposalContNo").setText(mProposalNo);
		
		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				mBaseInfo.getChildText("BankCode"),
				mBaseInfo.getChildText("ZoneNo"),
				mBaseInfo.getChildText("BrNo"));
		
		new YbtContInsuredIntlBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
