package com.sinosoft.midplat.kernel.service.newCont;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.finfee.TempFeeBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbtTempFeeBL {
	private final Logger cLogger = Logger.getLogger(getClass());
	
	private final GlobalInput cGlobalInput;
	private final LCContSchema cLCContSchema;
	
	private String cTempFeeNo = null;
	
	public YbtTempFeeBL(LCContSchema pLCContSchema, GlobalInput pGlobalInput) {
		cGlobalInput = pGlobalInput;
		cLCContSchema = pLCContSchema;
	}
	
	/**
	 * 查询保单合同数据失败，将会抛出MidplatException
	 */
	public YbtTempFeeBL(String pPrtNo, GlobalInput pGlobalInput) throws MidplatException {
		cGlobalInput = pGlobalInput;
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(pPrtNo);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
	}
	
	public void deal() throws Exception {
		cLogger.info("Into YbtTempFeeBL.deal()...");
		
		//插入财务暂收费数据
		VData mTempFeeBLData = getTempFeeBLData(cLCContSchema);
		TempFeeBL mTempFeeBL = new TempFeeBL();
		cLogger.info("Start call TempFeeBL.submitData()...");
		long mStartMillis = System.currentTimeMillis();
		if (!mTempFeeBL.submitData(mTempFeeBLData, "INSERT")) {
			throw new MidplatException(mTempFeeBL.mErrors.getFirstError());
		}
		cLogger.info("TempFeeBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("End call TempFeeBL.submitData()!");
		
		//更新LCCont中的TempFeeNo
		String mSQL = "update LCCont set TempFeeNo='" + cTempFeeNo + "' where ContNo='" + cLCContSchema.getContNo() + "' with ur";
		ExeSQL mExeSQL = new ExeSQL();
		if (!mExeSQL.execUpdateSQL(mSQL)) {
			cLogger.error(mExeSQL.mErrors.getFirstError());
			throw new MidplatException("更新暂收费号(TempFeeNo)到LCCont失败！");
		}

		cLogger.info("Out YbtTempFeeBL.deal()!");
	}
	
	private VData getTempFeeBLData(LCContSchema pLCContSchema) throws MidplatException {
		cLogger.info("Into YbtTempFeeBL.getTempFeeBLData()...");
		
		LCPolDB mLCPolDB = new LCPolDB();
		mLCPolDB.setContNo(pLCContSchema.getContNo());
		LCPolSet mLCPolSet = mLCPolDB.query();
		if (mLCPolSet.size() < 1) {
			throw new MidplatException("查询保单险种数据失败！");
		}

		String mSQL = "select max(tempfeeno) from ljtempfee where tempfeeno like '" + pLCContSchema.getPrtNo() + "%' with ur";
		cTempFeeNo = new ExeSQL().getOneValue(mSQL);
		if (cTempFeeNo.equals("")) {
			cTempFeeNo = pLCContSchema.getPrtNo() + "801";
		} else {
			String tSuffix = cTempFeeNo.substring(cTempFeeNo.length() - 3);
			cTempFeeNo = pLCContSchema.getPrtNo() + (Integer.parseInt(tSuffix)+1); // 暂收费号+1
		}
		pLCContSchema.setTempFeeNo(cTempFeeNo);

		ExeSQL mExeSQL = new ExeSQL();
		mSQL = "select ManageCom, BankAccno from ldfinbank where 1=1 "
			+ "and BankCode = '" + pLCContSchema.getBankCode().substring(0, 2) + "' "
			+ "and ManageCom in ('" + pLCContSchema.getManageCom().substring(0, 6) + "00'"
			+ ", '" + pLCContSchema.getManageCom().substring(0, 4) + "0000'"
            + ", '" + pLCContSchema.getManageCom().substring(0, 2) + "000000'"
			+ ", '" + pLCContSchema.getManageCom() + "'"
			+ ") "
			+ "and operater='YBT' "
			+ "and finflag ='S' "
			+ "order by ManageCom desc with ur";
		SSRS tSSRS = mExeSQL.execSQL(mSQL);
		if (tSSRS.getMaxRow() < 1) {
				throw new MidplatException("未查到保险公司在银行的归结帐户！");
		}
		String mManageCom = tSSRS.GetText(1, 1);
		String mBankAccno = tSSRS.GetText(1, 2);
		
		double mSumTempFeePayMoney = 0;
		LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
		for (int i = 1; i <= mLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = mLCPolSet.get(i);

			LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
			tLJTempFeeSchema.setTempFeeNo(cTempFeeNo);
			tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPrtNo()); //投保单号
			tLJTempFeeSchema.setTempFeeType("17");
			tLJTempFeeSchema.setOtherNoType("4");	//zhg 修改7为4--印刷号
			tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
			tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
			tLJTempFeeSchema.setPayMoney(tLCPolSchema.getPrem() + tLCPolSchema.getSupplementaryPrem());
			tLJTempFeeSchema.setPayDate(pLCContSchema.getPolApplyDate());
			tLJTempFeeSchema.setEnterAccDate(pLCContSchema.getPolApplyDate());
			tLJTempFeeSchema.setSaleChnl(pLCContSchema.getSaleChnl());
			tLJTempFeeSchema.setManageCom(mManageCom);	//收费机构
			tLJTempFeeSchema.setPolicyCom(pLCContSchema.getManageCom());	//管理机构
			tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
			tLJTempFeeSchema.setAgentCom(pLCContSchema.getAgentCom());
			tLJTempFeeSchema.setAgentGroup(pLCContSchema.getAgentGroup());
			tLJTempFeeSchema.setAgentCode(pLCContSchema.getAgentCode());
			tLJTempFeeSchema.setConfFlag("0");	//是否核销 0：没有财务核销 1：已有财务核销
			tLJTempFeeSchema.setOperator(cGlobalInput.Operator);
			mLJTempFeeSet.add(tLJTempFeeSchema);
			
			mSumTempFeePayMoney += tLJTempFeeSchema.getPayMoney();
		}

		LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
		mLJTempFeeClassSchema.setTempFeeNo(cTempFeeNo);
		mLJTempFeeClassSchema.setPayMode("4");
		mLJTempFeeClassSchema.setPayMoney(mSumTempFeePayMoney);
		mLJTempFeeClassSchema.setAppntName(pLCContSchema.getAppntName());
		mLJTempFeeClassSchema.setPayDate(pLCContSchema.getPolApplyDate());
		mLJTempFeeClassSchema.setEnterAccDate(pLCContSchema.getPolApplyDate());
		mLJTempFeeClassSchema.setConfFlag("0"); //是否核销 0：没有财务核销 1：已有财务核销
		mLJTempFeeClassSchema.setManageCom(mManageCom);
		mLJTempFeeClassSchema.setPolicyCom(cGlobalInput.ManageCom);
		mLJTempFeeClassSchema.setInsBankCode(pLCContSchema.getBankCode().substring(0, 2));
		mLJTempFeeClassSchema.setInsBankAccNo(mBankAccno);
		mLJTempFeeClassSchema.setBankCode(pLCContSchema.getBankCode());
		mLJTempFeeClassSchema.setBankAccNo(pLCContSchema.getBankAccNo());
		mLJTempFeeClassSchema.setAccName(pLCContSchema.getAccName());
		mLJTempFeeClassSchema.setOperator(cGlobalInput.Operator);
		LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
		mLJTempFeeClassSet.add(mLJTempFeeClassSchema);

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("CertifyFlag", "6");

		VData mVData = new VData();
		mVData.add(mTransferData);
		mVData.add(cGlobalInput);
		mVData.add(mLJTempFeeSet);
		mVData.add(mLJTempFeeClassSet);

		cLogger.info("Out YbtTempFeeBL.getTempFeeBLData()!");
		return mVData;
	}
	
	/**
	 * 返回暂收费号。注意：仅当成功生成暂收数据时，此调用才有效。
	 */
	public String getTempFeeNo() {
		return cTempFeeNo;
	}
}
