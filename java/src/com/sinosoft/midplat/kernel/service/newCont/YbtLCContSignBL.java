/**
 * 签单+单证核销+回执回销
 */

package com.sinosoft.midplat.kernel.service.newCont;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.ContReceiveUI;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.lis.tb.LCContSignBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbtLCContSignBL {
	private final Logger cLogger = Logger.getLogger(getClass());
	
	private final GlobalInput cGlobalInput;
	private LCContSchema cLCContSchema;
	
	public YbtLCContSignBL(LCContSchema pLCContSchema, GlobalInput pGlobalInput) {
		cGlobalInput = pGlobalInput;
		cLCContSchema = pLCContSchema;
	}
	
	/**
	 * 查询保单合同数据失败，将会抛出MidplatException
	 */
	public YbtLCContSignBL(String pPrtNo, GlobalInput pGlobalInput) throws MidplatException {
		cGlobalInput = pGlobalInput;
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(pPrtNo);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
	}
	
	public void deal() throws Exception {
		cLogger.info("Into YbtLCContSignBL.deal()...");
		
		//签单
		LCContSet mLCContSet = new LCContSet();
		mLCContSet.add(cLCContSchema);

		VData mLCContSignBLVData =  new VData();
		mLCContSignBLVData.add(cGlobalInput);
		mLCContSignBLVData.add(mLCContSet);

		LCContSignBL mLCContSignBL = new LCContSignBL();
		cLogger.info("Start call LCContSignBL.submitData()...");
		long mStartMillis = System.currentTimeMillis();
		if (!mLCContSignBL.submitData(mLCContSignBLVData, "")) {
			throw new MidplatException(mLCContSignBL.mErrors.getFirstError());
		}
		if (mLCContSignBL.mErrors.needDealError()) {
			throw new MidplatException(mLCContSignBL.mErrors.getFirstError());
		}
		cLogger.info("LCContSignBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("End call LCContSignBL.submitData()!");

		//提交签单数据入库
		VData mSignResult = mLCContSignBL.getResult();
		cLCContSchema = (LCContSchema) mSignResult.getObjectByObjectName("LCContSchema", 0);
		MMap mSubmitMMap = (MMap) mSignResult.getObjectByObjectName("MMap", 0);
		cLCContSchema.setPrintCount(1);
		cLCContSchema.setGetPolDate(DateUtil.getCurDate("yyyy-MM-dd"));
		cLCContSchema.setGetPolTime(DateUtil.getCurDate("HH:mm:ss"));
		cLCContSchema.setCustomGetPolDate(DateUtil.getCurDate("yyyy-MM-dd"));

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		cLogger.info("开始提交签单数据...");
		PubSubmit mPubSubmit = new PubSubmit();
		mStartMillis = System.currentTimeMillis();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		
		/**
		 * add by wangxt in 2009-4-2 begin
		 * 针对健管产品，增加生成健管工作流节点的处理
		 */
		String mMissionSQL = "select 1 from lmriskapp a where a.risktype2='5' and exists (select 1 from lcpol where contno='"+ cLCContSchema.getContNo() +"' and riskcode=a.riskcode) with ur";
		String mMissionStr = new ExeSQL().getOneValue(mMissionSQL);
		if (mMissionStr.equals("1")) {
			createMission(cLCContSchema);
		}
		/**
		 * add by wangxt in 2009-4-2 end
		 */
		
		cLogger.info("提交签单数据耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("提交签单数据成功！");
		
		//回执回销！
		autoReceive(cLCContSchema.getContNo());
		
		cLogger.info("Out YbtLCContSignBL.deal()!");
	}
	
	/**
	 * 生成工作流节点
	 */
	private void createMission(LCContSchema pLCContSchema) throws MidplatException {
		LWMissionDB mLWMissionDB = new LWMissionDB();
		String mMissionID = PubFun1.CreateMaxNo("MISSIONID", 20);
		mLWMissionDB.setMissionID(mMissionID);
		mLWMissionDB.setSubMissionID("1");
		mLWMissionDB.setProcessID("0000000003");
		mLWMissionDB.setActivityID("0000001190");
		mLWMissionDB.setActivityStatus("1");
		mLWMissionDB.setMissionProp1(pLCContSchema.getContNo());
		mLWMissionDB.setMissionProp2(pLCContSchema.getPrtNo());
		mLWMissionDB.setMissionProp3(pLCContSchema.getAgentCode());
		mLWMissionDB.setMissionProp4(pLCContSchema.getAppntNo());
		mLWMissionDB.setMissionProp5(pLCContSchema.getAppntName());
		mLWMissionDB.setMissionProp6(pLCContSchema.getUWDate());
		mLWMissionDB.setMissionProp7(pLCContSchema.getManageCom());
		mLWMissionDB.setLastOperator(pLCContSchema.getOperator());
		mLWMissionDB.setCreateOperator(pLCContSchema.getOperator());
		mLWMissionDB.setMakeDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLWMissionDB.setMakeTime(DateUtil.getCurDate("HH:mm:ss"));
		mLWMissionDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLWMissionDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
		
		if (!mLWMissionDB.insert()) {
			cLogger.error(mLWMissionDB.mErrors.getFirstError());
			throw new MidplatException("提交生成工作流节点数据失败！");
		}
	}
	
	private void autoReceive(String pContNo) throws MidplatException {
		TransferData mTmpTransferData = new TransferData();
		mTmpTransferData.setNameAndValue("ContNo", pContNo);

		VData mContReceiveVData = new VData();
		mContReceiveVData.add(cGlobalInput);
		mContReceiveVData.add(mTmpTransferData);

		cLogger.info("开始回执…");
		ContReceiveUI mContReceiveUI = new ContReceiveUI();
		if (!mContReceiveUI.submitData(mContReceiveVData, "CreateReceive")) {
			throw new MidplatException(mContReceiveUI.mErrors.getFirstError());
		}		
		//如果成功生成接收轨迹，目前银保险种需要自动进行合同接收。
		if (!mContReceiveUI.submitData(mContReceiveVData, "ReceiveCont")) {
			throw new MidplatException(mContReceiveUI.mErrors.getFirstError());
		}
		cLogger.info("回执成功！");

		cLogger.info("开始回销…");
		LCContGetPolSchema mLCContGetPolSchema = new LCContGetPolSchema();
		mLCContGetPolSchema.setContNo(pContNo);
		mLCContGetPolSchema.setGetpolDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLCContGetPolSchema.setGetpolMakeDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLCContGetPolSchema.setGetpolMan(cGlobalInput.Operator);
		mLCContGetPolSchema.setSendPolMan(cGlobalInput.Operator);
		mLCContGetPolSchema.setGetPolOperator(cGlobalInput.Operator);
		mLCContGetPolSchema.setManageCom(cGlobalInput.ManageCom);

		VData mContGetPolVData = new VData();
		mContGetPolVData.addElement(mLCContGetPolSchema);
		mContGetPolVData.add(cGlobalInput);

		LCContGetPolUI mLCContGetPolUI = new LCContGetPolUI();
		if (!mLCContGetPolUI.submitData(mContGetPolVData, "UPDATE")) {
			throw new MidplatException(mLCContGetPolUI.mErrors.getFirstError());
		}
		cLogger.info("回销成功！");
	}
	
	/**
	 * 返回签单后的保单合同号。注意：仅当签单成功时，此调用才有效。
	 */
	public String getContNo() {
		return cLCContSchema.getContNo();
	}
}
