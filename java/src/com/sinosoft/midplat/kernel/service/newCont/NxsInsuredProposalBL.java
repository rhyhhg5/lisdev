package com.sinosoft.midplat.kernel.service.newCont;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCSpecSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.tb.ContInsuredBL;
import com.sinosoft.lis.tb.ProposalBL;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NxsInsuredProposalBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(NxsInsuredProposalBL.class);
	
	private final Document cInXmlDoc;
	private final GlobalInput cGlobalInput;
	
	private LCContSchema cLCContSchema = null;	//保单
	private LCAppntSchema cLCAppntSchema = null;	//投保人
	
	public NxsInsuredProposalBL(Document pInXmlDoc, GlobalInput pGlobalInput) {
		cInXmlDoc = pInXmlDoc;
		cGlobalInput = pGlobalInput;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into NxsInsuredProposalBL.deal()...");
		
		Element mLCCont = cInXmlDoc.getRootElement().getChild(LCCont);
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
		if ((null==mLCContDB.getPrtNo()) || mLCContDB.getPrtNo().equals("")) {
			throw new MidplatException("投保书印刷号不能为空！");
		}
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
		
		LCAppntDB mLCAppntDB = new LCAppntDB();
		mLCAppntDB.setContNo(cLCContSchema.getContNo());
		mLCAppntDB.setAppntNo(cLCContSchema.getAppntNo());
		if (!mLCAppntDB.getInfo()) {
			throw new MidplatException("查询投保人数据失败！");
		}
		cLCAppntSchema = mLCAppntDB.getSchema();
		
		List mLCInsuredList = mLCCont.getChild(LCInsureds).getChildren(LCInsured);
		for (int i = 0; i < mLCInsuredList.size(); i++) {
			Element tLCInsured = (Element) mLCInsuredList.get(i);
			
			//录入被保人信息
			checkLCInsured(tLCInsured);	//校验被保人数据
			VData tContInsuredBLData = getContInsuredBLData(tLCInsured);
			ContInsuredBL tContInsuredBL = new ContInsuredBL();
			cLogger.info("Start call ContInsuredBL.submitData()...");
			long tStartMillism = System.currentTimeMillis();
			if (!tContInsuredBL.submitData(tContInsuredBLData, "INSERT||CONTINSURED")) {
				throw new MidplatException(tContInsuredBL.mErrors.getFirstError());
			}
			cLogger.info("ContInsuredBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s"
					+ "；投保书号：" + cLCContSchema.getPrtNo());
			cLogger.info("End call ContInsuredBL.submitData()!");
			VData tInsuredResult = tContInsuredBL.getResult();
//			cLCContSchema = 
//				(LCContSchema) tInsuredResult.getObjectByObjectName("LCContSchema", 0);
			LCInsuredSchema tLCInsuredSchema = 
				(LCInsuredSchema) tInsuredResult.getObjectByObjectName("LCInsuredSchema", 0);
			
			//录入险种信息
			List tRiskList = tLCInsured.getChild(Risks).getChildren(Risk);
			for (int j = 0; j < tRiskList.size(); j++) {
				Element ttRisk = (Element) tRiskList.get(j);
				
				checkRisk(ttRisk);	//校验险种数据
				VData ttProposalBLData = getProposalBLData(ttRisk, tLCInsuredSchema);
				ProposalBL ttProposalBL = new ProposalBL();
				cLogger.info("Start call ProposalBL.submitData()...");
				tStartMillism = System.currentTimeMillis();
				if (!ttProposalBL.submitData(ttProposalBLData, "INSERT||PROPOSAL")) {
					throw new MidplatException(ttProposalBL.mErrors.getFirstError());
				}
				cLogger.info("ProposalBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s"
						+ "；投保书号：" + cLCContSchema.getPrtNo());
				cLogger.info("End call ProposalBL.submitData()!");
			}
		}
		
	
		cLogger.info("Out NxsInsuredProposalBL.deal()!");
	}

	private void checkLCInsured(Element pLCInsured) throws MidplatException {
		cLogger.info("Into NxsInsuredProposalBL.checkLCInsured()...");
		
		if (!pLCInsured.getChildText(RelaToMain).equals("00")) {
			throw new MidplatException("与主被保人关系必须为本人！");
		}
		
		cLogger.info("Out NxsInsuredProposalBL.checkLCInsured()()!");
	}

	private VData getContInsuredBLData(Element pLCInsured) throws MidplatException {
		cLogger.info("Into NxsInsuredProposalBL.getContInsuredBLData()...");
		
		//被保人
		LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
		mLCInsuredSchema.setName(pLCInsured.getChildText(Name));
		mLCInsuredSchema.setSex(pLCInsured.getChildText(Sex));
		mLCInsuredSchema.setBirthday(pLCInsured.getChildText(Birthday));
		mLCInsuredSchema.setIDType(pLCInsured.getChildText(IDType));
		mLCInsuredSchema.setIDNo(pLCInsured.getChildText(IDNo));
		mLCInsuredSchema.setOccupationCode(pLCInsured.getChildText(JobCode));
		String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + pLCInsured.getChildText(JobCode) + "' with ur";
		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
		if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
			throw new MidplatException("未查到职业类别！职业代码为：" + pLCInsured.getChildText(JobCode));
		}
		mLCInsuredSchema.setOccupationType(mOccupationTypeStr);	//职业类别;
//		mLCInsuredSchema.setWorkType(pLCInsured.getChildText(""));	//华夏添加
		mLCInsuredSchema.setPrtNo(cLCContSchema.getPrtNo());
		mLCInsuredSchema.setContNo(cLCContSchema.getContNo());
		mLCInsuredSchema.setRelationToMainInsured(pLCInsured.getChildText(RelaToMain));
		mLCInsuredSchema.setRelationToAppnt(pLCInsured.getChildText(RelaToAppnt));

		//被保人地址
		LCAddressSchema mInsuredAddress = new LCAddressSchema();
		mInsuredAddress.setHomeAddress(pLCInsured.getChildText(HomeAddress));
		mInsuredAddress.setPostalAddress(pLCInsured.getChildText(MailAddress));
		mInsuredAddress.setZipCode(pLCInsured.getChildText(MailZipCode));
		mInsuredAddress.setPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setHomeZipCode(pLCInsured.getChildText(HomeZipCode));
		mInsuredAddress.setHomePhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setCompanyPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setMobile(pLCInsured.getChildText(InsuredMobile));
		mInsuredAddress.setEMail(pLCInsured.getChildText(Email));
		
		//被保人个人信息
		LDPersonSchema mInsuredPerson = new LDPersonSchema();
		mInsuredPerson.setCustomerNo("");	//注意此处必须设置！picch核心判断CustomerNo为""时才进行被保人五要素判断，CustomerNo为null时会直接生成新客户号
		mInsuredPerson.setName(mLCInsuredSchema.getName());
		mInsuredPerson.setSex(mLCInsuredSchema.getSex());
		mInsuredPerson.setIDNo(mLCInsuredSchema.getIDNo());
		mInsuredPerson.setIDType(mLCInsuredSchema.getIDType());
		mInsuredPerson.setBirthday(mLCInsuredSchema.getBirthday());
		mInsuredPerson.setOccupationCode(mLCInsuredSchema.getOccupationCode());
		mInsuredPerson.setOccupationType(mLCInsuredSchema.getOccupationType());	//职业类别

		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setSchema(mLCInsuredSchema);
		
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("FamilyType", "0");
		mTransferData.setNameAndValue("PolTypeFlag", "0");
		mTransferData.setNameAndValue("SequenceNo", "1");
		mTransferData.setNameAndValue("ContType", "1");
		mTransferData.setNameAndValue("SavePolType", "0");
		
		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(cLCContSchema);
		mVData.add(mLCInsuredSchema);
		mVData.add(mLCInsuredDB);
		mVData.add(mInsuredAddress);
//		mVData.add(mLCCustomerImpartSet);		//告知信息,保存了被保人的身高和体重
		mVData.add(mInsuredPerson);
		mVData.add(mTransferData);
      
		cLogger.info("Out NxsInsuredProposalBL.getContInsuredBLData()!");
		return mVData;
	}
	
	/**
	 * add by wangxt in 2009-4-3 for checking
	 * @param pRisk
	 * @throws MidplatException
	 */
	private void checkRisk(Element pRisk) throws MidplatException {
		cLogger.info("Into NxsInsuredProposalBL.checkRisk()...");
		
				
		cLogger.info("Out NxsInsuredProposalBL.checkRisk()!");
	}
	
	private VData getProposalBLData(Element pRisk, LCInsuredSchema pLCInsuredSchema) throws MidplatException {
		cLogger.info("Into NxsInsuredProposalBL.getProposalBLData()...");

		//险种信息
		LCPolSchema mLCPolSchema = new LCPolSchema();
		mLCPolSchema.setContNo(cLCContSchema.getContNo());
		mLCPolSchema.setPrtNo(cLCContSchema.getPrtNo());
		mLCPolSchema.setKindCode(pRisk.getChildText("RiskType"));	//险种类型
		mLCPolSchema.setRiskCode(pRisk.getChildText(RiskCode));
		mLCPolSchema.setManageCom(cLCContSchema.getManageCom());
		mLCPolSchema.setAgentType(cLCContSchema.getAgentType());
		mLCPolSchema.setAgentCom(cLCContSchema.getAgentCom());
		mLCPolSchema.setAgentCode(cLCContSchema.getAgentCode());
		mLCPolSchema.setAgentGroup(cLCContSchema.getAgentGroup());
		mLCPolSchema.setSaleChnl(cLCContSchema.getSaleChnl());
		mLCPolSchema.setCValiDate(cLCContSchema.getCValiDate());
		mLCPolSchema.setGetYearFlag(pRisk.getChildText(GetYearFlag));
		mLCPolSchema.setGetYear(pRisk.getChildText(GetYear));
		mLCPolSchema.setPayEndYearFlag(pRisk.getChildText(PayEndYearFlag));
		mLCPolSchema.setPayEndYear(pRisk.getChildText(PayEndYear));
		mLCPolSchema.setInsuYearFlag(pRisk.getChildText(InsuYearFlag));
		mLCPolSchema.setInsuYear(pRisk.getChildText(InsuYear));
//		mLCPolSchema.setSpecifyValiDate("N");
		mLCPolSchema.setPayMode(cLCContSchema.getPayMode());
		mLCPolSchema.setPayIntv(pRisk.getChildText(PayIntv));
		mLCPolSchema.setYears(pRisk.getChildText(Years));
		mLCPolSchema.setFloatRate(pRisk.getChildText("Rate"));
		mLCPolSchema.setMult(pRisk.getChildText(Mult));
		/* picch个人防癌(230701)产品是根据保额自动计算保费，无需录入。
		 * 如果以后遇到需要向核心提供保费的险种，此处需修改。
		 * mLCPolSchema.setPrem(pRisk.getChildText(Prem));
		 */
		mLCPolSchema.setAmnt(pRisk.getChildText(Amnt));
		mLCPolSchema.setAutoPayFlag(pRisk.getChildText("AutoPayFlag"));
		mLCPolSchema.setSubFlag(pRisk.getChildText("SubFlag"));
		mLCPolSchema.setBonusGetMode(pRisk.getChildText("BonusGetMode"));
//		tLCPolSchema.setSaleChnlDetail("13");   //在银保通中增加渠道细节=13，以于核心系统相区别
//		mLCPolSchema.setOccupationType(lCInsured.getChildText(""));
		mLCPolSchema.setPolApplyDate(cLCContSchema.getPolApplyDate());		
		String mSQL = "select mainpolno from lcpol where prtno='" + cLCContSchema.getPrtNo() + "' and mainpolno=polno with ur";
		mLCPolSchema.setMainPolNo(new ExeSQL().getOneValue(mSQL));
		
		//责任信息
		LCDutySchema mLCDutySchema = new LCDutySchema();
		mLCDutySchema.setContNo(cLCContSchema.getContNo());
		mLCDutySchema.setAmnt(mLCPolSchema.getAmnt());
		mLCDutySchema.setFloatRate(mLCPolSchema.getFloatRate());
		mLCDutySchema.setPrem(mLCPolSchema.getPrem());
		mLCDutySchema.setMult(mLCPolSchema.getMult());
		mLCDutySchema.setPayIntv(mLCPolSchema.getPayIntv());
		mLCDutySchema.setYears(mLCPolSchema.getYears());
		mLCDutySchema.setPayEndYearFlag(mLCPolSchema.getPayEndYearFlag());
		mLCDutySchema.setPayEndYear(mLCPolSchema.getPayEndYear());
		mLCDutySchema.setGetYearFlag(mLCPolSchema.getGetYearFlag());
		mLCDutySchema.setGetYear(mLCPolSchema.getGetYear());
		mLCDutySchema.setInsuYearFlag(mLCPolSchema.getInsuYearFlag());
		mLCDutySchema.setInsuYear(mLCPolSchema.getInsuYear());
		mLCDutySchema.setBonusGetMode(mLCPolSchema.getBonusGetMode());

		//特别约定
		LCSpecSet mLCSpecSet = new LCSpecSet();
		LCSpecSchema mLCSpecSchema = new LCSpecSchema();
		mLCSpecSchema.setSpecContent(cLCContSchema.getRemark());
		mLCSpecSet.add(mLCSpecSchema);
      
		//受益人
		LCBnfSet mLCBnfSet = new LCBnfSet();
		List mLCBnfList = pRisk.getChild("LCBnfs").getChildren("LCBnf");
		for (int i = 0; i < mLCBnfList.size(); i++) {
			Element tLCBnf = (Element) mLCBnfList.get(i);

			LCBnfSchema tLCBnfSchema = new LCBnfSchema();
			tLCBnfSchema.setContNo(cLCContSchema.getContNo());
			tLCBnfSchema.setBnfType(tLCBnf.getChildText("BnfType"));
			tLCBnfSchema.setBnfGrade(tLCBnf.getChildText("BnfGrade"));
			tLCBnfSchema.setName(tLCBnf.getChildText("Name"));
			tLCBnfSchema.setBirthday(tLCBnf.getChildText("Birthday"));
			tLCBnfSchema.setSex(tLCBnf.getChildText("Sex"));
			tLCBnfSchema.setIDType(tLCBnf.getChildText("IDType"));
			tLCBnfSchema.setIDNo(tLCBnf.getChildText("IDNo"));
			tLCBnfSchema.setRelationToInsured(tLCBnf.getChildText("RelationToInsured"));
			tLCBnfSchema.setBnfLot(tLCBnf.getChildText("BnfLot"));
			/*
			 * 核心要求的受益比例在0.0-1.0之间，而标准报文的受益比例为1-100，所以需要在此做转换
			 */
			tLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot()/100.0);
			mLCBnfSet.add(tLCBnfSchema);
		}

		TransferData mTransferData = new TransferData();
		//mTransferData.setNameAndValue("samePersonFlag","");
		mTransferData.setNameAndValue("ChangePlanFlag", "1");
		
		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(cLCContSchema);
		mVData.add(pLCInsuredSchema);
		mVData.add(mLCPolSchema);
		mVData.add(cLCAppntSchema);
		mVData.add(mLCDutySchema);
		//mVData.add(mLCCustomerImpartSet2); //被保人的告知信息
		mVData.add(mLCSpecSet);
		mVData.add(mLCBnfSet);
		mVData.add(mTransferData);

		cLogger.info("Out NxsInsuredProposalBL.getProposalBLData()!");
		return mVData;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "D:/request/ICBC_std/UW.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mXmlDoc = new SAXBuilder().build(mIsr);
		
		Element mTranData = mXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
		mLCCont.getChild("PrtNo").setText(mProposalNo);
		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
		
		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				mBaseInfo.getChildText("BankCode"),
				mBaseInfo.getChildText("ZoneNo"),
				mBaseInfo.getChildText("BrNo"));
		
		new NxsInsuredProposalBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
