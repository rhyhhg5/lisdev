/**
 * 保单信息录入！
 */

package com.sinosoft.midplat.kernel.service.newCont;

import java.io.FileInputStream;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAccountSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.tb.ContBL;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NxsContBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(NxsContBL.class);

	private final Document cInXmlDoc;
	private final GlobalInput cGlobalInput;

	public NxsContBL(Document pInXmlDoc, GlobalInput pGlobalInput) {
		cInXmlDoc = pInXmlDoc;
		cGlobalInput = pGlobalInput;
	}

	public void deal() throws Exception {
		cLogger.info("Into NxsContBL.deal()...");

		String mTransrNo = cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(TransrNo);
		// 校验数据
		checkContBL();
		// 录入保单合同信息
		VData mContBLData = getContBLVData();
		ContBL mContBL = new ContBL();
		cLogger.info("Start call ContBL.submitData()...");
		long mStartMillis = System.currentTimeMillis();
		if (!mContBL.submitData(mContBLData, "INSERT||CONT")) {
			throw new MidplatException(mContBL.mErrors.getFirstError());
		}
		cLogger.info("ContBL耗时：" + (System.currentTimeMillis() - mStartMillis)
				/ 1000.0 + "s" + "；交易流水号：" + mTransrNo);
		cLogger.info("End call ContBL.submitData()!");
		cLogger.info("Out NxsContBL.deal()!");
	}

	private void checkContBL() throws MidplatException {
		cLogger.info("Into NxsContBL.checkContBL()...");
		Element mLCCont = cInXmlDoc.getRootElement().getChild(LCCont);
		String mSQL = "select 1 from LCCont where ContNo='"
				+ mLCCont.getChildText(ProposalContNo) + "' with ur";
		if ("1".equals(new ExeSQL().getOneValue(mSQL))) {
			throw new MidplatException("该单证被别的未收费保单挂起！");
		}
		if (cGlobalInput.AgentCom.startsWith("PY098")) {
			if (mLCCont.getChildText(PrtNo).length() != 12
					&& mLCCont.getChildText(PrtNo).length() != 13) {
				throw new MidplatException("投保单印刷号应为12位或13位！"
						+ mLCCont.getChildText(PrtNo));
			}
		} else if (cGlobalInput.AgentCom.startsWith("PC21")
//				|| cGlobalInput.AgentCom.startsWith("PC91")
				|| cGlobalInput.AgentCom.startsWith("PY053")) { // 辽宁农信社13位印刷号
			if (mLCCont.getChildText(PrtNo).length() != 13) {
				throw new MidplatException("投保单印刷号应为13位！"
						+ mLCCont.getChildText(PrtNo));
			}
		}
	//	else if(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode).equals("60")){
			// if (mLCCont.getChildText(PrtNo).length() != 11) {
			// throw new MidplatException("江西测试投保单印刷号应为11位！"+mLCCont.getChildText(PrtNo));
			// }
		//} 
		else if (cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(
				BankCode).equals("04")) {
			// 投保单印刷号不作处理
		} else if (cGlobalInput.AgentCom.startsWith("PC13")) {   //针对河北只转单证号不传prtno所以放行 add by zengzm 2015-12-11
				//此处不需要什么处理
		}
		else if (cGlobalInput.AgentCom.startsWith("PC41")) {  //针对河南只转单证号不传prtno所以放行 add by zengzm 2017-01-20
				//此处不需要什么处理
		}
		//add by zengzm 2016-05-09 针对江门不传投保书号
		else if(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode).equals("65")){
			cLogger.info("江门邮政不校验投保书号!");
		} 
		//add by GaoJinfu 20171202 针对广东邮政不传投保书号
		else if("77".equals(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode))){
			cLogger.info("广东邮政不校验投保书号!");
		}
		//针对临商银行投保单号更新为13位  add by gcy
		else if("80".equals(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode))){
			cLogger.info("临商银行不校验投保书号!");
		}
		//针对湖北农信社投保单号更新为13位  add by gcy 20181008
		else if("81".equals(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode))){
			cLogger.info("湖北农信社不校验投保书号!");
		}
		else if(cGlobalInput.AgentCom.startsWith("PC43")){   //针对湖南只转单证号不传prtno所以放行 add by zengzm 2016-07-26
			//此处不需要什么处理
		} else {
			if (mLCCont.getChildText(PrtNo).length() != 12) {
				throw new MidplatException("投保单印刷号应为12位！" + mLCCont.getChildText(PrtNo));
			}
		}

		if (!mLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
			String mRiskWrapCode = mLCCont.getChild(LCInsureds).getChild(
					LCInsured).getChild(Risks).getChild(Risk).getChildText(
					MainRiskCode);
			String mCertifyCode = null;// 单证编码
			ExeSQL mExeSQL = new ExeSQL();
			
			//针对江门邮政地区管理机构特殊处理 add by zengzm 2016-08-22
			String mManagecom = "";
			if("65".equals(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode))){
				mManagecom = cGlobalInput.ManageCom;
			}else{
				mManagecom = cGlobalInput.ManageCom.substring(0, 4);
			}
			
			String tSQL = "select certifycode from lkcertifymapping where bankcode = '"
				+ cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode)+"' "
				+ "and managecom = '"+ mManagecom+"' "
				+ "and RiskWrapCode = '"
				+ mRiskWrapCode.trim()+"' "
				+ "with ur";
				
			SSRS tSSRS = mExeSQL.execSQL(tSQL);
			
			if (tSSRS.getMaxRow() < 1) {
				throw new MidplatException("未查询到对应的单证编码!");
			}
			mCertifyCode = tSSRS.GetText(1, 1);// 农信社单证编码需要从单证编码定义表中查询
			
			if (cGlobalInput.AgentCom.startsWith("PC41")
					&& cInXmlDoc.getRootElement().getChild(BaseInfo)
							.getChildText(BankCode).equals("45")) {
				if (!new CardManage(mCertifyCode, cGlobalInput)
						.canBeUsed(mLCCont.getChildText(ProposalContNo)
								.substring(3))) {
					throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				}
			} else if (cGlobalInput.AgentCom.startsWith("PC21")) {
				if (!new CardManage(mCertifyCode, cGlobalInput)
						.canBeUsed(mLCCont.getChildText(ProposalContNo)
								.substring(3, 13))) {
					throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				}
			} else if (cGlobalInput.AgentCom.startsWith("PC13")) {//针对河北单证10位 add by zengzm 2015-12-11
				if (!new CardManage(mCertifyCode, cGlobalInput)
						.canBeUsed(mLCCont.getChildText(ProposalContNo)
								.substring(3, 13))) {
					throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				}
			} else if (cGlobalInput.AgentCom.startsWith("PC43")) {//针对湖南单证10位 add by zengzm 2016-07-26
				if (!new CardManage(mCertifyCode, cGlobalInput)
						.canBeUsed(mLCCont.getChildText(ProposalContNo)
								.substring(3, 13))) {
					throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				}
			}else {
				if (cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(
						BankCode).equals("04")) {
					if (!new CardManage(mCertifyCode, cGlobalInput)
							.canBeUsed(mLCCont.getChildText(ProposalContNo))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				//add by zengzm 2016-05-09 江门邮政不传单证号
				else if (cInXmlDoc.getRootElement().getChild(BaseInfo)
						.getChildText(BankCode).equals("65")) {
					cLogger.info("江门邮政不校验单证号!");
				}
				//针对广东邮政 不校验单证号  add by GaoJinfu 20171202
				else if (cInXmlDoc.getRootElement().getChild(BaseInfo)
						.getChildText(BankCode).equals("77")) {
					cLogger.info("广东邮政不校验单证号!");
				}
				//针对陕西农信社 add by GaoJinfu 20180515
				else if (cInXmlDoc.getRootElement().getChild(BaseInfo)
						.getChildText(BankCode).equals("78")) {
					if (!new CardManage(mCertifyCode, cGlobalInput)
							.canBeUsed(mLCCont.getChildText(ProposalContNo)
									.substring(3, 12))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				//江西农信社外测环境单证11位  生产12位没变化   
				else if(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode).equals("60")
				){
					if (!new CardManage(mCertifyCode, cGlobalInput).canBeUsed(
							mLCCont.getChildText(ProposalContNo).substring(3))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				// 针对泉州银行
				else if (cInXmlDoc.getRootElement().getChild(BaseInfo)
						.getChildText(BankCode).equals("53")) {
					if (!new CardManage(mCertifyCode, cGlobalInput)
							.canBeUsed(mLCCont.getChildText(ProposalContNo))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				//针对广州做特殊处理 测试环境11位,正式环境11位
				else if(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode).equals("66"))
				{
					if (!new CardManage(mCertifyCode, cGlobalInput).canBeUsed(
							mLCCont.getChildText(ProposalContNo).substring(2))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				
					}
				}
				//针对农行,将XBT004放行,因为目前农行只有XBT004从银保通切换到信保通中
				else if(cInXmlDoc.getRootElement().getChild(BaseInfo).getChildText(BankCode).equals("04"))
				{   
					if (!new CardManage(mCertifyCode, cGlobalInput).canBeUsed(
							mLCCont.getChildText(ProposalContNo))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				// 针对临商银行
				else if (cInXmlDoc.getRootElement().getChild(BaseInfo)
						.getChildText(BankCode).equals("80")) {
					if (!new CardManage(mCertifyCode, cGlobalInput)
							.canBeUsed(mLCCont.getChildText(ProposalContNo).substring(3))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				// 针对湖北农信社
				else if (cInXmlDoc.getRootElement().getChild(BaseInfo)
						.getChildText(BankCode).equals("81")) {
					if (!new CardManage(mCertifyCode, cGlobalInput)
							.canBeUsed(mLCCont.getChildText(ProposalContNo).substring(3))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
				else {
					if (!new CardManage(mCertifyCode, cGlobalInput)
							.canBeUsed(mLCCont.getChildText(ProposalContNo)
									.substring(2, 12))) {
						throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
					}
				}
			}
		}
		cLogger.info("Out NxsContBL.checkContBL()!");
	}

	private VData getContBLVData() throws MidplatException {
		cLogger.info("Into NxsContBL.getContBLVData()...");

		Element mLCCont = cInXmlDoc.getRootElement().getChild(LCCont);
		Element mLCAppnt = mLCCont.getChild(LCAppnt); // 投保人信息
		
		
		//增加缴费账户户名必须与投保人姓名一致校验
		String AccName = mLCCont.getChildTextTrim("AccName");// 户名
		String AppntName1 = mLCAppnt.getChildTextTrim("AppntName");// 投保人名称
		if (null != AccName && !"".equals(AccName)) {
			if (!AccName.equals(AppntName1)) {
				throw new MidplatException("账号持有人必须为投保人本人!");
			}
		}

		// 保单信息
		LCContSchema mLCContSchema = getLCContSchema(cInXmlDoc);

		// 投保人
		LCAppntSchema mLCAppntSchema = new LCAppntSchema();
		mLCAppntSchema.setAppntNo(""); // 注意此处必须设置！picch核心判断AppntNo为""时才进行投保人五要素判断，AppntNo为null时会直接生成新客户号
		mLCAppntSchema.setPrtNo(mLCCont.getChildText(PrtNo));
		mLCAppntSchema.setAppntName(mLCAppnt.getChildText(AppntName));
		mLCAppntSchema.setAppntSex(mLCAppnt.getChildText(AppntSex));
		mLCAppntSchema.setAppntBirthday(mLCAppnt.getChildText(AppntBirthday));
		mLCAppntSchema.setIDType(mLCAppnt.getChildText(AppntIDType));
		mLCAppntSchema.setIDNo(mLCAppnt.getChildText(AppntIDNo));
		mLCAppntSchema.setOccupationCode(mLCAppnt.getChildText(JobCode));
		String mSQL = "select occupationtype from ldoccupation where OccupationCode='"
				+ mLCAppnt.getChildText(JobCode) + "' with ur";
		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
		if ((null == mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
			throw new MidplatException("未查到职业类别！职业代码为："
					+ mLCAppnt.getChildText(JobCode));
		}
		mLCAppntSchema.setOccupationType(mOccupationTypeStr); // 职业类别;

		// 投保人地址
		LCAddressSchema mAppntAddress = new LCAddressSchema();
		mAppntAddress.setCompanyPhone(mLCAppnt.getChildText(AppntOfficePhone));
		mAppntAddress.setMobile(mLCAppnt.getChildText(AppntMobile));
		mAppntAddress.setPhone(mLCAppnt.getChildText(AppntPhone));
		mAppntAddress.setHomePhone(mLCAppnt.getChildText(AppntPhone));
		mAppntAddress.setPostalAddress(mLCAppnt.getChildText(MailAddress));
		mAppntAddress.setZipCode(mLCAppnt.getChildText(MailZipCode));
		mAppntAddress.setHomeAddress(mLCAppnt.getChildText(HomeAddress));
		mAppntAddress.setHomeZipCode(mLCAppnt.getChildText(HomeZipCode));
		mAppntAddress.setEMail(mLCAppnt.getChildText(Email));

		// 投保帐户
		LCAccountSchema mLCAccountSchema = new LCAccountSchema();
		mLCAccountSchema.setBankCode(mLCContSchema.getBankCode());
		mLCAccountSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLCAccountSchema.setAccName(mLCContSchema.getAccName());
		mLCAccountSchema.setAccKind("Y");

		// 投保人个人信息
		LDPersonSchema mAppntPerson = new LDPersonSchema();
		mAppntPerson.setName(mLCAppntSchema.getAppntName());
		mAppntPerson.setSex(mLCAppntSchema.getAppntSex());
		mAppntPerson.setIDNo(mLCAppntSchema.getIDNo());
		mAppntPerson.setIDType(mLCAppntSchema.getIDType());
		mAppntPerson.setBirthday(mLCAppntSchema.getAppntBirthday());

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("GrpNo", "");
		mTransferData.setNameAndValue("GrpName", "");

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(mLCContSchema);
		mVData.add(mLCAppntSchema);
		mVData.add(mAppntAddress);
		mVData.add(mLCAccountSchema);
		mVData.add(mAppntPerson);
		mVData.add(mTransferData);

		cLogger.info("Out NxsContBL.getContBLVData()!");
		return mVData;
	}

	private LCContSchema getLCContSchema(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into NxsContBL.getLCContSchema()...");

		Element mTranData = cInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		Element mLCAppnt = mLCCont.getChild(LCAppnt); // 投保人信息
		Element mLCInsured = mLCCont.getChild(LCInsureds).getChild(LCInsured); // 被保人信息

		String mCurDate = DateUtil.getCur10Date();

		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema.setGrpContNo("00000000000000000000");
		mLCContSchema.setContNo(mLCCont.getChildText(ProposalContNo)); // 用保单合同印刷号录单
		mLCContSchema.setProposalContNo(mLCCont.getChildText(ProposalContNo));
		mLCContSchema.setPrtNo(mLCCont.getChildText(PrtNo));
		mLCContSchema.setConsignNo(mLCCont.getChildText("ConsignNo")); // 存储贷款合同号
		mLCContSchema.setContType("1"); // 保单类别(个单-1，团单-2)
		mLCContSchema.setPolType("0");
//		mLCContSchema.setCardFlag("a"); // 渠道"03"，CardFlag"a"，共同识别信保通交易！
		String m_Riskcode = cInXmlDoc.getRootElement().getChild(LCCont)
				.getChild(LCInsureds).getChild(LCInsured).getChild(Risks)
				.getChild(Risk).getChildTextTrim(MainRiskCode);

		if (cGlobalInput.ManageCom.startsWith("8661")
				&& mBaseInfo.getChildText("BankCode").equals("04")
				&& m_Riskcode.equals("XBT004")) {
			mLCContSchema.setCardFlag("9"); // 渠道"03"，CardFlag"a"，共同识别信保通交易！ 04 9 银保通
			mLCContSchema.setSaleChnl("04"); // 渠道"03"，CardFlag"a"，共同识别信保通交易！ 04 9 银保通
		} else {
			mLCContSchema.setCardFlag("a"); // 渠道"03"，CardFlag"a"，共同识别信保通交易！ 04 9 银保通
			mLCContSchema.setSaleChnl("03"); // 渠道"03"，CardFlag"a"，共同识别信保通交易！ 04 9 银保通
		}
		mLCContSchema.setManageCom(cGlobalInput.ManageCom);
		mLCContSchema.setAgentCom(cGlobalInput.AgentCom);
		/*LAComToAgentDB mLAComToAgentDB = new LAComToAgentDB();
		mLAComToAgentDB.setAgentCom(cGlobalInput.AgentCom);
		mLAComToAgentDB.setRelaType("1");
		LAComToAgentSet mLAComToAgentSet = mLAComToAgentDB.query();
		if (mLAComToAgentSet.mErrors.needDealError()
				|| (null == mLAComToAgentSet) || (mLAComToAgentSet.size() < 1)) {
			cLogger.error(mLAComToAgentDB.mErrors.getFirstError());
			throw new MidplatException("未查到相应网点的专管员信息！");
		}
		mLCContSchema.setAgentCode(mLAComToAgentSet.get(1).getAgentCode());
		mLCContSchema.setAgentGroup(mLAComToAgentSet.get(1).getAgentGroup());*/
		//代理人查询，之前的方法可能查到已经离职的代理人，将离职的代理人排除掉 modify by GaoJinfu 20180328
		String sSql = "select la.agentcode,la.agentgroup from lacomtoagent la,laagent lg where la.agentcode = lg.agentcode and la.agentcom = '"
				+ cGlobalInput.AgentCom
				+ "' and la.relatype = '1' and lg.agentstate in ('01','02','03','04','05') with ur";
		System.out.println("sSql=="+sSql);
		SSRS ssrs = new ExeSQL().execSQL(sSql);
		if(ssrs.MaxRow <= 0){
			throw new MidplatException("未查到相应网点的专管员信息！");
		}
		mLCContSchema.setAgentCode(ssrs.GetText(1, 1));
		mLCContSchema.setAgentGroup(ssrs.GetText(1, 2));
//		mLCContSchema.setSaleChnl("03"); // 渠道"03"，CardFlag"a"，共同识别银保通交易！
		mLCContSchema.setPassword(mLCCont.getChildText(Password)); // 保单密码
		mLCContSchema.setInputOperator(cGlobalInput.Operator);
		mLCContSchema.setInputDate(PubFun.getCurrentDate());
		mLCContSchema.setInputTime(PubFun.getCurrentTime());
		// 投保人
		mLCContSchema.setAppntName(mLCAppnt.getChildText(AppntName));
		mLCContSchema.setAppntSex(mLCAppnt.getChildText(AppntSex));
		mLCContSchema.setAppntBirthday(mLCAppnt.getChildText(AppntBirthday));
		mLCContSchema.setAppntIDType(mLCAppnt.getChildText(AppntIDType));
		mLCContSchema.setAppntIDNo(mLCAppnt.getChildText(AppntIDNo));
		// 被保人
		mLCContSchema.setInsuredName(mLCInsured.getChildText(Name));
		mLCContSchema.setInsuredSex(mLCInsured.getChildText(Sex));
		mLCContSchema.setInsuredBirthday(mLCInsured.getChildText(Birthday));
		mLCContSchema.setInsuredIDType(mLCInsured.getChildText(IDType));
		mLCContSchema.setInsuredIDNo(mLCInsured.getChildText(IDNo));
		mLCContSchema.setPayIntv(mLCCont.getChildText(PayIntv)); // 缴费方式
		mLCContSchema.setPayMode("4");
		mLCContSchema.setPayLocation("0"); // 银行转帐
		String mSQL = "select BankCode from LDBank where 1=1 "
				+ "and BankCode like '" + mBaseInfo.getChildText(BankCode)
				+ "%' " + "and ComCode='" + cGlobalInput.ComCode + "' "
				+ "and CansendFlag='1' " + "with ur";
		mLCContSchema.setBankCode(new ExeSQL().getOneValue(mSQL));
		if ((null == mLCContSchema.getBankCode())
				|| mLCContSchema.getBankCode().equals("")) {
			mLCContSchema.setBankCode(mBaseInfo.getChildText(BankCode));
		}
		mLCContSchema.setBankAccNo(mLCCont.getChildText(BankAccNo)); // 投保帐户号
		mLCContSchema.setAccName(mLCCont.getChildText(AccName)); // 投保帐户姓名
		mLCContSchema.setPrintCount(0);
		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent));
		// mod by chenjw 2011-5-30 安徽农信社一贷多保 指定生效日 start
		GregorianCalendar mNowCalendar = new GregorianCalendar();
		mNowCalendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
		mLCContSchema.setCValiDate(mNowCalendar.getTime()); // 保单生效日期(当前日期的下一天)

		String tCValiDate = mLCCont.getChildText("CValiDate");
		if (tCValiDate != null && !"".equals(tCValiDate)) {
			if (Integer.parseInt(tCValiDate.replaceAll("-", "")) != 0) {
				mLCContSchema.setCValiDate(tCValiDate);
				System.out.println("指定生效日是：" + tCValiDate);
			}
		}
		// mod by chenjw 2011-5-30 安徽农信社一贷多保 指定生效日 end
		mLCContSchema.setPolApplyDate(mCurDate); // 投保日期
		mLCContSchema.setProposalType("01"); // 投保书类型：01-普通个单投保书
		mLCContSchema.setRemark(mLCCont.getChildText(SpecContent)); // 特别约定
		mLCContSchema.setCopys(mLCInsured.getChild(Risks).getChild(Risk)
				.getChildText(Mult));

		cLogger.info("Out NxsContBL.getLCContSchema()!");
		return mLCContSchema;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "D:/request/ICBC_std/UW.xml";

		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));

		Element mTranData = mInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				mBaseInfo.getChildText("BankCode"),
				mBaseInfo.getChildText("ZoneNo"),
				mBaseInfo.getChildText("BrNo"));

		new YbtContBL(mInXmlDoc, mGlobalInput).deal();

		System.out.println("成功结束！");
	}
}
