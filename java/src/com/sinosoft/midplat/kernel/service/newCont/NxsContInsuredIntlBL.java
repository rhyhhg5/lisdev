/**
 * 录入被保人和险种信息！
 */


package com.sinosoft.midplat.kernel.service.newCont;

import java.io.FileInputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.intltb.ContInsuredIntlBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDiseaseResultSet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NxsContInsuredIntlBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(NxsContInsuredIntlBL.class);
	
	private final Document cInXmlDoc;
	private final GlobalInput cGlobalInput;
	
	private LCContSchema cLCContSchema = null;
	private String cBankCode = null;
	private String tCValiDate = null;
	public NxsContInsuredIntlBL(Document pInXmlDoc, GlobalInput pGlobalInput) {
		cInXmlDoc = pInXmlDoc;
		cGlobalInput = pGlobalInput;
	}
	
	public void deal() throws Exception {
		
		cLogger.info("Into NxsContInsuredIntlBL.deal()...");
		
		Element mTranData = cInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		tCValiDate = mLCCont.getChildText("CValiDate");
		String mTransrNo = mBaseInfo.getChildText(TransrNo);
		cBankCode = mBaseInfo.getChildText(BankCode);
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			throw new MidplatException("查询保单合同数据失败！");
		}
		cLCContSchema = mLCContSet.get(1);
		
		List mLCInsuredList = cInXmlDoc.getRootElement()
			.getChild(LCCont).getChild(LCInsureds).getChildren(LCInsured);
		for (int i = 0; i < mLCInsuredList.size(); i++) {
			Element tLCInsured = (Element) mLCInsuredList.get(i);
			
			checkContInsuredIntlBL(tLCInsured);
			
			VData tContInsuredIntlBLVData = getContInsuredIntlBLVData(tLCInsured);
			ContInsuredIntlBL tContInsuredIntlBL   = new ContInsuredIntlBL();
			cLogger.info("Start call ContInsuredIntlBL.submitData()...");
			long tStartMillism = System.currentTimeMillis();
			if (!tContInsuredIntlBL.submitData(tContInsuredIntlBLVData, "INSERT||CONTINSURED")) {
				throw new MidplatException(tContInsuredIntlBL.mErrors.getFirstError());
			}
			cLogger.info("ContInsuredIntlBL耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s"
					+ "；交易流水号：" + mTransrNo);
			cLogger.info("End call ContInsuredIntlBL.submitData()!");
		}
		
		cLogger.info("Out NxsContInsuredIntlBL.deal()!");
	}
	
	private void checkContInsuredIntlBL(Element pLCInsured) throws MidplatException {
		
		cLogger.info("Into NxsContInsuredIntlBL.checkContInsuredIntlBL()...");
		
		if (!pLCInsured.getChildText(RelaToMain).equals("00")) {
			throw new MidplatException("与主被保人关系必须为本人！");
		}

		//校验险种数据
		List mRiskList = pLCInsured.getChild(Risks).getChildren(Risk);
		for (int i = 0; i < mRiskList.size(); i++) {
			Element tRisk = (Element) mRiskList.get(i);
			
			checkRisk(tRisk);
			
			//校验受益人
			List tLCBnfList = tRisk.getChild(LCBnfs).getChildren(LCBnf);
			for (int j = 0; j < tLCBnfList.size(); j++) {
				Element ttLCBnf = (Element) tLCBnfList.get(j);
				if (ttLCBnf.getChildText(RelationToInsured).equals("00")) {
					throw new MidplatException("受益人和被保人的关系不能为本人!");
				}
			}
		}
		
		cLogger.info("Out NxsContInsuredIntlBL.checkContInsuredIntlBL()!");
	}
	
	private void checkRisk(Element pRisk) throws MidplatException {
		cLogger.info("Into NxsContInsuredIntlBL.checkRisk()...");
		
		String mManageCom = cGlobalInput.ManageCom.substring(0, 4);
		String mRiskWrapCode = pRisk.getChildText(MainRiskCode);
		String mAgentCom = cGlobalInput.AgentCom;
		String mAmntUpLimit = "0";
		String mSQL = "Select AmntUpLimit from lkamntuplimit where "
			+"BankCode = '" + cBankCode + "' and "
			+"ManageCom = '" + mManageCom + "' and "
			+"AgentCom = '" + mAgentCom + "' and "
			+"RiskWrapCode = '" + mRiskWrapCode + "' with ur";
		SSRS mSSRS = new ExeSQL().execSQL(mSQL);
		if (mSSRS.getMaxRow() < 1) {
			//启动费率匹配
			cLogger.info("默认保额上限使用启动: 银行： " + cBankCode
					+ "管理机构： " + mManageCom + "套餐: " + mRiskWrapCode
					+ "代理机构： " + mAgentCom);
			
			//匹配费率
			if (mRiskWrapCode.equals("JZJ001") || mRiskWrapCode.equals("WR0127") || mRiskWrapCode.equals("WR0145")
					|| mRiskWrapCode.equals("XBT002") || mRiskWrapCode.equals("XBT003")) {
				mAmntUpLimit = "1000000";
			} else if (mRiskWrapCode.equals("WR0143")) {
				mAmntUpLimit = "500000";
			} else {
				throw new MidplatException("未配置套餐保额上限!银行：" + cBankCode
						+ "管理机构：" + mManageCom + "套餐：" + mRiskWrapCode
						+ "代理机构：" + mAgentCom + "当前传输保额上限: " + mAmntUpLimit);
			}
			
			//费率匹配完成
			cLogger.info("默认保额上限填入: " + mAmntUpLimit);
		}else{
			mAmntUpLimit = mSSRS.GetText(1, 1);
			cLogger.info("配置的保额上限值为：" + mAmntUpLimit);
		}
		Element mStandbyFlag3 = new Element("StandbyFlag3");
		mStandbyFlag3.setText(mAmntUpLimit);
		pRisk.addContent(mStandbyFlag3);

		cLogger.info("Out NxsContInsuredIntlBL.checkRisk()!");
	}
	
	private VData getContInsuredIntlBLVData(Element pLCInsured) throws MidplatException {
		cLogger.info("Into NxsContInsuredIntlBL.getContInsuredIntlBLVData()...");

		//被保人
		LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
		mLCInsuredSchema.setName(pLCInsured.getChildText(Name));
		mLCInsuredSchema.setSex(pLCInsured.getChildText(Sex));
		mLCInsuredSchema.setBirthday(pLCInsured.getChildText(Birthday));
		mLCInsuredSchema.setIDType(pLCInsured.getChildText(IDType));
		mLCInsuredSchema.setIDNo(pLCInsured.getChildText(IDNo));
		mLCInsuredSchema.setOccupationCode(pLCInsured.getChildText(JobCode));
		String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + pLCInsured.getChildText(JobCode) + "' with ur";
		String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
		if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
			throw new MidplatException("未查到职业类别！职业代码为：" + pLCInsured.getChildText(JobCode));
		}
		mLCInsuredSchema.setOccupationType(mOccupationTypeStr);	//职业类别;
		mLCInsuredSchema.setPrtNo(cLCContSchema.getPrtNo());
		mLCInsuredSchema.setContNo(cLCContSchema.getContNo());
		mLCInsuredSchema.setRelationToMainInsured(pLCInsured.getChildText(RelaToMain));
		mLCInsuredSchema.setRelationToAppnt(pLCInsured.getChildText(RelaToAppnt));

		//被保人地址
		LCAddressSchema mInsuredAddress = new LCAddressSchema();
		mInsuredAddress.setHomeAddress(pLCInsured.getChildText(HomeAddress));
		mInsuredAddress.setPostalAddress(pLCInsured.getChildText(MailAddress));
		mInsuredAddress.setZipCode(pLCInsured.getChildText(MailZipCode));
		mInsuredAddress.setPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setHomeZipCode(pLCInsured.getChildText(HomeZipCode));
		mInsuredAddress.setHomePhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setCompanyPhone(pLCInsured.getChildText(HomePhone));
		mInsuredAddress.setMobile(pLCInsured.getChildText(InsuredMobile));
		mInsuredAddress.setEMail(pLCInsured.getChildText(Email));

		//被保人个人信息
		LDPersonSchema mInsuredPerson = new LDPersonSchema();
		mInsuredPerson.setCustomerNo("");	//注意此处必须设置！picch核心判断CustomerNo为""时才进行被保人五要素判断，CustomerNo为null时会直接生成新客户号
		mInsuredPerson.setName(mLCInsuredSchema.getName());
		mInsuredPerson.setSex(mLCInsuredSchema.getSex());
		mInsuredPerson.setIDNo(mLCInsuredSchema.getIDNo());
		mInsuredPerson.setIDType(mLCInsuredSchema.getIDType());
		mInsuredPerson.setBirthday(mLCInsuredSchema.getBirthday());
		mInsuredPerson.setOccupationCode(mLCInsuredSchema.getOccupationCode());
		mInsuredPerson.setOccupationType(mLCInsuredSchema.getOccupationType());	//职业类别

		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setSchema(mLCInsuredSchema);

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("FamilyType", "0");
		mTransferData.setNameAndValue("PolTypeFlag", "0");
		mTransferData.setNameAndValue("SequenceNo", "1");
		mTransferData.setNameAndValue("ContType", "1");
		mTransferData.setNameAndValue("SavePolType", "0");

		//套餐信息
		Element mRisk = pLCInsured.getChild(Risks).getChild(Risk); 
//		mSQL = "select RiskWrapCode from LDRiskWrap where RiskCode='" + mRisk.getChildText(MainRiskCode) + "' with ur";
		LDRiskDutyWrapDB mLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
		mLDRiskDutyWrapDB.setRiskWrapCode(mRisk.getChildText(MainRiskCode));//注意:农信社传的是险种套餐编码
		LDRiskDutyWrapSet mLDRiskDutyWrapSet = mLDRiskDutyWrapDB.query();
		LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
		for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
			
			LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
			LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
			tLCRiskDutyWrapSchema.setRiskWrapCode(tLDRiskDutyWrapSchema.getRiskWrapCode());
			tLCRiskDutyWrapSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
			tLCRiskDutyWrapSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());
			tLCRiskDutyWrapSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());
			tLCRiskDutyWrapSchema.setCalFactorType(tLDRiskDutyWrapSchema.getCalFactorType());
			
			if("ComCode".equals(tLDRiskDutyWrapSchema.getCalFactor())){
				tLCRiskDutyWrapSchema.setCalFactorValue(cGlobalInput.ManageCom);//信保通套餐信息需要传入管理机构
			}else if("AgentCom".equals(tLDRiskDutyWrapSchema.getCalFactor())){
				tLCRiskDutyWrapSchema.setCalFactorValue(cGlobalInput.AgentCom);//信保通套餐信息需要传入代理机构
			}else if("Copys".equals(tLDRiskDutyWrapSchema.getCalFactor())){
                tLCRiskDutyWrapSchema.setCalFactorValue(mRisk.getChildText(Mult));
            }else{
				tLCRiskDutyWrapSchema.setCalFactorValue(
						
						mRisk.getChildText(tLDRiskDutyWrapSchema.getCalFactor()));
			}

			
			mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
		}
		//add by chnejw借意险一贷多保指定生效日改造，如需指定生效日那么lcpol中specifyvalidate置为Y  start 2011-5-31

			LCRiskDutyWrapSchema mLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
			List mRisks = pLCInsured.getChild(Risks).getChildren(Risk);
			for (int j = 0; j < mRisks.size(); j++) {
			System.out.println("tCValiDate:" + tCValiDate);
			if (tCValiDate != null && !"".equals(tCValiDate)) {
				if (Integer.parseInt(tCValiDate.replaceAll("-", "")) != 0) {
					mLCRiskDutyWrapSchema.setRiskWrapCode(mLDRiskDutyWrapSet
							.get(1).getRiskWrapCode());
					mLCRiskDutyWrapSchema.setRiskCode(mLDRiskDutyWrapSet.get(1)
							.getRiskCode());
					mLCRiskDutyWrapSchema.setDutyCode(mLDRiskDutyWrapSet.get(1)
							.getDutyCode());
					mLCRiskDutyWrapSchema.setCalFactor("SpecifyValiDate");
					mLCRiskDutyWrapSchema.setCalFactorValue("Y");
					mLCRiskDutyWrapSchema.setCalFactorType("1");
					mLCRiskDutyWrapSet.add(mLCRiskDutyWrapSchema);
				}
			}
		}		
//add by chnejw借意险一贷多保指定生效日改造，如需指定生效日那么lcpol中specifyvalidate置为Y  end 2011-5-31
		//受益人
		LCBnfSet mLCBnfSet = new LCBnfSet();
		List mLCBnfList = mRisk.getChild(LCBnfs).getChildren(LCBnf);
		for (int i = 0; i < mLCBnfList.size(); i++) {
			Element tLCBnf = (Element) mLCBnfList.get(i);
			
			LCBnfSchema tLCBnfSchema = new LCBnfSchema();
			tLCBnfSchema.setContNo(cLCContSchema.getContNo());
			tLCBnfSchema.setBnfType(tLCBnf.getChildText(BnfType));
			tLCBnfSchema.setBnfGrade(tLCBnf.getChildText(BnfGrade));
			tLCBnfSchema.setName(tLCBnf.getChildText(Name));
			tLCBnfSchema.setBirthday(tLCBnf.getChildText(Birthday));
			tLCBnfSchema.setSex(tLCBnf.getChildText(Sex));
			tLCBnfSchema.setIDType(tLCBnf.getChildText(IDType));
			tLCBnfSchema.setIDNo(tLCBnf.getChildText(IDNo));
			tLCBnfSchema.setRelationToInsured(tLCBnf.getChildText(RelationToInsured));
			tLCBnfSchema.setBnfLot(tLCBnf.getChildText(BnfLot));
			/*
			 * 核心要求的受益比例在0.0-1.0之间，而标准报文的受益比例为1-100，所以需要在此做转换
			 */
			tLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot()/100.0);
			mLCBnfSet.add(tLCBnfSchema);
		}

		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(cLCContSchema);
		mVData.add(mLCInsuredSchema);
		mVData.add(mLCInsuredDB);
		mVData.add(mInsuredAddress);
		mVData.add(mInsuredPerson);
		mVData.add(mTransferData);
		mVData.add(mLCRiskDutyWrapSet);
		mVData.add(mLCBnfSet);
		mVData.add(new LCDiseaseResultSet());	//告知信息
		mVData.add(new LCNationSet());	//抵达国家

		cLogger.info("Out NxsContInsuredIntlBL.getContInsuredIntlBLVData()!");
		return mVData;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "D:/request/ICBC_std/UW.xml";
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Element mTranData = mInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				mBaseInfo.getChildText(BankCode),
				mBaseInfo.getChildText(ZoneNo),
				mBaseInfo.getChildText(BrNo));
		
		new YbtContInsuredIntlBL(mInXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
