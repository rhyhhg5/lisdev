/**
 * 日终对账服务实现类
 */

package com.sinosoft.midplat.kernel.service;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKBalanceDetailSchema;
import com.sinosoft.lis.vschema.LKBalanceDetailSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.balance.DetailBalanceBL;
import com.sinosoft.midplat.kernel.service.balance.GetherBalance;
import com.sinosoft.midplat.kernel.service.balance.KernelBalanceBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class DailyBalance extends ServiceImpl {
	private String cBalanceNum = null;	//对账次数
	private LKBalanceDetailSet mLKBalanceDetailSet_x = new LKBalanceDetailSet();
	
	public DailyBalance(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into DailyBalance.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		String mManageCom = null;
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		//获取对账次数，设置cBalanceNum
		String mSQL = "select count(1)+1 from LKTransStatus where 1=1 "
			+ "and BankCode='" + mBaseInfo.getChildText(BankCode) + "' "
			+ "and BankBranch='" + mBaseInfo.getChildText(ZoneNo) + "' "
			+ "and BankNode='" + mBaseInfo.getChildText(BrNo) + "' "
			+ "and FuncFlag='" + mBaseInfo.getChildText(FunctionFlag) + "' "
			+ "and TransDate='" + DateUtil.date8to10(mBaseInfo.getChildText(BankDate)) + "'";
		cBalanceNum = new ExeSQL().getOneValue(mSQL);
		cLogger.debug("对账次数：BalanceNum = " + cBalanceNum);
		
		/**
		 * 同一天、同一网点前置机自动生成的流水号相同，
		 * 为防止多次重复对账时交易流水号重复，
		 * 在此重新设置流水号：TransrNo + "." + BalanceNum
		 */
		mBaseInfo.getChild(TransrNo).setText(
				mBaseInfo.getChildText(TransrNo) + "." + cBalanceNum);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mSQL += "and RCode = '1' with ur";
			String mCount = new ExeSQL().getOneValue(mSQL);
			if (mCount.equals("2")) {
				throw new MidplatException("只允许对一次帐！");
			}
			
			mSQL = "select ManageCom from LKCodeMapping where Remark5='1' "
				+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
				+ "and ZoneNo='" + mLKTransStatusDB.getBankBranch() + "' "
				+ "and BankNode='" + mLKTransStatusDB.getBankNode() + "' "
				+ "with ur";
			mManageCom = new ExeSQL().getOneValue(mSQL);
			if ((null==mManageCom) || mManageCom.equals("")) {
				throw new MidplatException("未查到该对帐网点！");
			}

			//处理前置机传过来的报错信息(扫描超时等)
			String tErrorStr = mTranData.getChildText("Error");
			if (null != tErrorStr) {
				throw new MidplatException(tErrorStr);
			}
			
			//保存对账明细
			LKBalanceDetailSet mLKBalanceDetailSet = saveDetails(pInXmlDoc);
			
	      /**
	       * 为防止多次重复对账后日志表信息混乱，每次对账前清理一次对账信息相关字段
	       */
			mSQL = "update lktransstatus set state_code=null, rbankvsmp=null, desbankvsmp=null, "
				+ "rmpvskernel=null, desmpvskernel=null, resultbalance=null, desbalance=null "
				+ "where transdate='" + mLKTransStatusDB.getTransDate() + "' "
				+ "and bankcode='" + mLKTransStatusDB.getBankCode() + "' "
				+ "and ManageCom like '" + mManageCom + "%' "
				+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
				+"'" + mLKTransStatusDB.getBankCode() + "' "
				+"and ManageCom <> '" + mManageCom +"' "
				+"and ManageCom like '" +mManageCom +"%' "
				+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) "  
				+ "with ur";
			new ExeSQL().execUpdateSQL(mSQL);
			
			/*******************************************************************
			 * ***** 在此增加续期缴费对账 ***** add by wz 2012-11-26
			 ******************************************************************/
			
			if (mLKBalanceDetailSet_x != null && mLKBalanceDetailSet_x.size() > 0) {
				cLogger.info("********************="+mLKBalanceDetailSet_x.get(1).getCardNo());
				NXS_RenewalPayBusiblc tNXS_RenewalPayBusiblc = new NXS_RenewalPayBusiblc(
						mManageCom,mLKTransStatusDB, mLKBalanceDetailSet_x);
				tNXS_RenewalPayBusiblc.deal();
			}

			//与核心对账
			KernelBalanceBL mKernelBalanceBL = new KernelBalanceBL(
					mLKTransStatusDB.getBankCode(), 
					mLKTransStatusDB.getTransDate(),
					mManageCom);
			mKernelBalanceBL.deal();
			//清理当日只做试算的保单数据
			mKernelBalanceBL.dealNoSignDate();

			//与银行明细对账
			DetailBalanceBL mDetailBalanceBL = new DetailBalanceBL(
					mLKTransStatusDB.getBankCode(), 
					mLKTransStatusDB.getTransDate(),
					mManageCom,
					mLKBalanceDetailSet);
			mDetailBalanceBL.deal();

			//与银行汇总对账
			GetherBalance mGetherBalance = new GetherBalance(
					mLKTransStatusDB.getBankCode(), 
					mLKTransStatusDB.getTransDate(),
					mManageCom);
			mGetherBalance.deal();
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "对账成功！");
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；2-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error("日终对账(DailyBalance)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；2-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setManageCom(mManageCom);
			mLKTransStatusDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out DailyBalance.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into DailyBalance.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		/**
		 * 如果前置机扫描超时，系统当前时间可能已经是第二天0点，
		 * 此处(TransDate)不取系统当前时间，改用前置机传过来的时间。
		 * 前置机必须保证TranData/BaseInfo/BankDate中的时间的正确性，
		 * 如果是ftp方式对账，BankDate应为本轮扫描的开始时间；
		 * 如果是交易方式，需要银行将此字段传为白天交易的时间。
		 */
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out DailyBalance.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	/**
	 * 保存对账明细，返回保存的明细数据(LKBalanceDetailSet)
	 */
	private LKBalanceDetailSet saveDetails(Document pXmlDoc) throws Exception {
		cLogger.info("Into DailyBalance.saveDetails()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mChkDetails = mTranData.getChild(ChkDetails);
		
		String mCurDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurTime = DateUtil.getCurDate("HH:mm:ss");
		
		List mChkDetailList = mChkDetails.getChildren(ChkDetail);
		LKBalanceDetailSet mLKBalanceDetailSet = new LKBalanceDetailSet();
		LKBalanceDetailSet tLKBalanceDetailSet = new LKBalanceDetailSet();//add by zengzm 2017-01-02 针对对账数据量过大，进行分批提交处理
		int m = mChkDetailList.size();
		int totalNum = m/1000;
		int totalNumY = m%1000;
		if(totalNumY!=0)
		{
			totalNum =totalNum+1;
		}
		int n=1;
		for (int i = 0; i < mChkDetailList.size(); i++) {
			Element tChkDetail = (Element) mChkDetailList.get(i);
				LKBalanceDetailSchema tLKBalanceDetailSchema = new LKBalanceDetailSchema();
				tLKBalanceDetailSchema.setBalanceNo(mBaseInfo.getChildText(TransrNo));
				tLKBalanceDetailSchema.setTransrNo(tChkDetail.getChildText(TransrNo));
				tLKBalanceDetailSchema.setBankCode(mBaseInfo.getChildText(BankCode));
				tLKBalanceDetailSchema.setBankZoneCode(tChkDetail.getChildText(BankZoneCode));
				tLKBalanceDetailSchema.setBrNo(tChkDetail.getChildText(BrNo));
				tLKBalanceDetailSchema.setBalanceNum(cBalanceNum);
				tLKBalanceDetailSchema.setTellerNo(tChkDetail.getChildText(TellerNo));
				tLKBalanceDetailSchema.setFuncFlag(tChkDetail.getChildText(FuncFlag));
				tLKBalanceDetailSchema.setCardNo(tChkDetail.getChildText(CardNo));
	//			tLKBalanceDetailSchema.setAppntName(tChkDetail.getChildText(AppntName));
				tLKBalanceDetailSchema.setTranDate(tChkDetail.getChildText(TranDate));
				tLKBalanceDetailSchema.setTranAmnt(tChkDetail.getChildText(TranAmnt));
				tLKBalanceDetailSchema.setConfirmFlag(tChkDetail.getChildText(ConfirmFlag));
				tLKBalanceDetailSchema.setTemp1(mBaseInfo.getChildText(ZoneNo));
				tLKBalanceDetailSchema.setTemp2(mBaseInfo.getChildText(BrNo));
				tLKBalanceDetailSchema.setMakeDate(mCurDate);
				tLKBalanceDetailSchema.setMakeTime(mCurTime);
				tLKBalanceDetailSchema.setModifyDate(mCurDate);
				tLKBalanceDetailSchema.setModifyTime(mCurTime);
				
				if(mBaseInfo.getChildText(BankCode).equals("03")){ //建行未发送续期标志
					String xq_sql = "select 1 from lktransstatus where bankcode = '03' and polno = '" + tChkDetail.getChildText(CardNo) + "' and makedate = Current Date and funcflag = '52' and descr is null with ur";
					
					if(new ExeSQL().getOneValue(xq_sql).equals("1")){
						mLKBalanceDetailSet_x.add(tLKBalanceDetailSchema);
					}else {
						tLKBalanceDetailSet.add(tLKBalanceDetailSchema);
						mLKBalanceDetailSet.add(tLKBalanceDetailSchema);
					}
				}else {
					if("X".equals(tChkDetail.getChildTextTrim("Type"))){ //从前置机对账文件中判断如果是Type为X则认为是X期
						mLKBalanceDetailSet_x.add(tLKBalanceDetailSchema);
					}else{
						mLKBalanceDetailSet.add(tLKBalanceDetailSchema);
						tLKBalanceDetailSet.add(tLKBalanceDetailSchema);
					}
				}
				
				if((tLKBalanceDetailSet.size()==1000)||(tLKBalanceDetailSet.size()==totalNumY &&n==totalNum))
				{
					MMap mSubmitMMap = new MMap();
					mSubmitMMap.put(tLKBalanceDetailSet, "INSERT");
					VData mSubmitVData = new VData();
					mSubmitVData.add(mSubmitMMap);
					PubSubmit mPubSubmit = new PubSubmit();
					if (!mPubSubmit.submitData(mSubmitVData, "")) {
						cLogger.error(mPubSubmit.mErrors.getFirstError());
						throw new MidplatException("保存对账明细失败！");
					}
					cLogger.info("-----------------提交第"+n+"次循环"+tLKBalanceDetailSet.size()+"条数据成功！！");
					n++;
					tLKBalanceDetailSet.clear();
				}
				
				
				
				
		}
 
		/**
		 * 将银行发过来的对账明细存储到对账明细表(LKBalanceDetail)中
		 */
		cLogger.info("银行提交的对账明细总数(LKBalanceDetailSet)为：" + mLKBalanceDetailSet.size());
//		MMap mSubmitMMap = new MMap();
//		mSubmitMMap.put(mLKBalanceDetailSet, "INSERT");
//		VData mSubmitVData = new VData();
//		mSubmitVData.add(mSubmitMMap);
//		PubSubmit mPubSubmit = new PubSubmit();
//		if (!mPubSubmit.submitData(mSubmitVData, "")) {
//			cLogger.error(mPubSubmit.mErrors.getFirstError());
//			throw new MidplatException("保存对账明细失败！");
//		}

		cLogger.info("Out DailyBalance.saveDetails()!");
		return mLKBalanceDetailSet;
	}
}
