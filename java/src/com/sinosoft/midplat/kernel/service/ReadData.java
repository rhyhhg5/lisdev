package com.sinosoft.midplat.kernel.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.NumberUtil;
import com.sinosoft.midplat.kernel.util.FTPDealBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ReadData extends ServiceImpl {
	
	private static String ip="";
	private static String user="";
	private static String password="";
	private static int port=0;
	private static String separate = "";
	private static Properties props = null;
	
	public ReadData(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ReadFile.service()...");
		Document mOutXmlDoc = null;
		String stateTime = ReadData.getBefTime() ;
		String endTime = ReadData.getAftTime() ;
		
		if(!"".equals(stateTime)){
			ExeSQL mExeSQL = new ExeSQL();
			String mSQL = "select '',a.prtno, a.appntidtype,a.appntidno,b.relationtoappnt,a.InsuredIDType,a.InsuredIDNo," +
					"'1',c.riskseqno,c.riskcode,a.PolApplyDate,'1',a.payintv,c.payendyearflag,c.payendyear,''," +
					"'','',a.contno,a.prem,a.prem,'',a.amnt,a.sumprem,'',a.proposalcontno,''," +
					"c.BonusGetMode,'','','',a.PolApplyDate,c.firstpaydate,c.payenddate,c.Cvalidate," +
					"c.enddate,c.InsuYearFlag,c.InsuYear,'','','',a.bankaccno,'','1','0'  from " +
					"lccont a inner join lcinsured b on a.contno = b.contno inner join lcpol c on c.contno = a.contno where" +
					" a.signdate >= '"+stateTime+"' and a.signdate <= '"+endTime+"'  and a.bankcode like '10%' and a.salechnl = '04' and a.cardflag in ('6','8','9') and c.riskcode not in ('530701','530801','331401','531001','340201','531101') and a.contno in ( select polno from lktransstatus where bankcode = '10'  and transdate between '"+stateTime+"' and '"+endTime+"' and RBankVSMP = '00' and Resultbalance = '0') union " +
//					" a.contno = '013182196000001' union " +
//					" a.signdate >= '2012-02-12' and a.signdate <= '2012-04-30'  and a.bankcode like '10%' and a.salechnl = '04' and a.cardflag in ('6','8','9') and a.contno in ( select polno from lktransstatus where bankcode = '10'  and transdate between '2012-02-12' and '2012-04-30' and RBankVSMP = '00' and Resultbalance = '0') union " +
					"select '11',a1.prtno, a1.appntidtype,a1.appntidno,b1.relationtoappnt,a1.InsuredIDType,a1.InsuredIDNo," +
					"'1',c1.riskseqno,c1.riskcode,a1.PolApplyDate,'1',a1.payintv,c1.payendyearflag,c1.payendyear,''," +
					"'','',a1.contno,a1.prem,a1.prem,'',a1.amnt,a1.sumprem,'0',a1.proposalcontno,''," +
					"c1.BonusGetMode,'','','',a1.PolApplyDate,c1.firstpaydate,c1.payenddate,c1.Cvalidate," +
					"c1.enddate,c1.InsuYearFlag,c1.InsuYear,'','','',a1.bankaccno,'','2','0'  from " +
					"lbcont a1 inner join lbinsured b1 on a1.contno = b1.contno inner join lbpol c1 on c1.contno = a1.contno where" +
					" a1.signdate >= '"+stateTime+"' and a1.signdate <= '"+endTime+"' and a1.bankcode like '10%' and c1.riskcode not in ('530701','530801','331401','531001','340201','531101') and a1.salechnl = '04' and a1.cardflag = '9'  union "+
//					" a1.contno = '013182196000001'  union "+
//			        " a1.signdate >= '2012-02-12'  and a1.signdate <= '2012-04-30' and a1.bankcode like '10%' and a1.salechnl = '04' and a1.cardflag = '9' union "+
			        "select '11',a2.prtno, a2.appntidtype,a2.appntidno,b2.relationtoappnt,a2.InsuredIDType,a2.InsuredIDNo," +
					"'1',c2.riskseqno,c2.riskcode,a2.PolApplyDate,'1',a2.payintv,c2.payendyearflag,c2.payendyear,''," +
					"'','',a2.contno,a2.prem,a2.prem,'',a2.amnt,a2.sumprem,'0',a2.proposalcontno,''," +
					"c2.BonusGetMode,'','','',a2.PolApplyDate,c2.firstpaydate,c2.payenddate,c2.Cvalidate," +
					"c2.enddate,c2.InsuYearFlag,c2.InsuYear,'','','',a2.bankaccno,'','2','0'  from " +
					"lccont a2 inner join lcinsured b2 on a2.contno = b2.contno inner join lbpol c2 on c2.contno = a2.contno where" +
					" a2.signdate >= '"+stateTime+"' and a2.signdate <= '"+endTime+"' and a2.bankcode like '10%' and c2.riskcode not in ('530701','530801','331401','531001','340201','531101') and a2.salechnl = '04' and a2.cardflag = '9'   ";
//					" a2.contno = '013182196000001'   ";
//			        " a2.signdate >= '2012-02-12' and a2.signdate <= '2012-04-30' and a2.bankcode like '10%' and a2.salechnl = '04' and a2.cardflag = '9'";
			SSRS ssrs = mExeSQL.execSQL(mSQL);
			StringBuffer sb = getSSRSString(ssrs);
			
			mSQL = " select '',a.prtno, a.appntidtype,a.appntidno,b.relationtoappnt,a.InsuredIDType,a.InsuredIDNo,'1',c.riskseqno,c.riskcode,a.PolApplyDate,'1',a.payintv,c.payendyearflag,c.payendyear,'','','',a.contno,a.prem,a.prem,'',a.amnt,a.sumprem,'',a.proposalcontno,'',c.BonusGetMode,'','','',a.PolApplyDate,c.firstpaydate,c.payenddate,c.Cvalidate,c.enddate,c.InsuYearFlag,c.InsuYear,'','','',a.bankaccno,'','1','0' "+ 
				" from lccont a inner join lcinsured b on a.contno = b.contno inner join lcpol c on c.contno = a.contno "+
				" where "+
				" a.bankcode like '10%'"+ 
				" and a.salechnl = '04' and a.cardflag in ('6','8','9')"+ 
				" and c.riskcode not in ('530701','530801','331401','531001','340201','531101')"+ //清除附加险
				" and a.contno in ( select polno from lktransstatus where bankcode = '10'  and RBankVSMP = '00' and Resultbalance = '0')"+
				" and a.contno in ("+
				" select otherno from ljtempfee where makedate between '"+stateTime+"' and '"+endTime+"'"+ 
				" and tempfeetype in ('2','18'))";
			ssrs = mExeSQL.execSQL(mSQL);
			sb.append(getSSRSString(ssrs));
			
			ip = cThisBusiConf.getChildText("ip");
			user = cThisBusiConf.getChildText("userName");
			password = cThisBusiConf.getChildText("passWord");
			port = Integer.valueOf(cThisBusiConf.getChildText("port")).intValue();
			
			StringBuffer URL = new StringBuffer();
			URL.append("310").append("IF10011").append(DateUtil.getCurDate("yyyyMMdd")).append(".ast");
			File file = ReadData.writerFile(sb.toString(), URL.toString());
			cLogger.info("生成的文件路径："+file.getAbsolutePath());
			ReadData.fTPUploadFile(file);
		}
		
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mOutXmlDoc = new Document(mTranData);
		cLogger.info("Out ReadFile.service()...");
		return mOutXmlDoc;
	}
	
	public static void fTPUploadFile(File file){
		
		FTPDealBL tFTPDealBL = new FTPDealBL(ip,user,password,port);
		tFTPDealBL.ApacheFTPUploadFile(file, "\\BCCL");
	}
	
	private static StringBuffer getSSRSString(SSRS ssrs){
		StringBuffer sb = new StringBuffer();
		for (int i = 1; i <= ssrs.MaxRow; i++) {
			StringBuffer line = new StringBuffer();
			for (int j = 1; j <= ssrs.MaxCol; j++) {
				if(j == 1){
					line.append(ReadData.getProValue(ssrs.GetText(i, 19),ssrs.GetText(i, 44))+"IF10011").append(separate);
					continue;
				}
				if(j == 3 || j == 6){
					line.append(ReadData.getAppntIdType(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 5){
					line.append(ReadData.getRela(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 9){  
					line.append(ReadData.getRiskSeqNo(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 11 || j == 32 || j == 33 || j == 34 || j == 35 || j == 36){ 
					line.append(ReadData.getDates(ssrs.GetText(i, j))).append(separate);
					continue;
				}
//				if(j == 13){
//					line.append(ReadData.getPremType(ssrs.GetText(i, j))).append(separate);
//					continue;
//				}
				if(j == 14){
					line.append(ReadData.getPremTermType(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 15){
					if("".equals(ssrs.GetText(i, j))){
						line.append("0").append(separate);
					}else{
						if("0".equals(ssrs.GetText(i, 13))){
							line.append("0").append(separate);
						}else{
							
							line.append(ssrs.GetText(i, j)).append(separate);
						}
					}
					continue;
				}
				if(j >= 20 && j <= 24){
					line.append(ReadData.getPrem(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 25){
					String riskSeqNo = ReadData.getRiskSeqNo(ssrs.GetText(i, 9));
					if("0".equals(riskSeqNo) || "1".equals(riskSeqNo)){
						line.append(ReadData.getAddAmt(ssrs.GetText(i, 19),ssrs.GetText(i, 44))).append(separate);
					}else{
						line.append("000000000000000").append(separate);
					}
					continue;
				}
				if(j == 37){
					line.append(ReadData.getCoverageType(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 38){
					line.append(ReadData.getCoverage(ssrs.GetText(i, j))).append(separate);
					continue;
				}
				if(j == 44){
					if("2".equals(ssrs.GetText(i, j))) {
						line.append(ReadData.getContStat(ssrs.GetText(i, 19))).append(separate);
					}else {
						line.append(ssrs.GetText(i, j)).append(separate);
					}
					continue;
				}
				line.append(ssrs.GetText(i, j)).append(separate);
			}
			sb.append(line.toString().substring(0, line.toString().lastIndexOf(separate))+"\n");
		}
		return sb;
	}
	
	public static String getBefTime(){
		
		String tMonth = new SimpleDateFormat("MM").format(new Date());
		System.out.println(tMonth);
		if(!("01".equals(tMonth) || "04".equals(tMonth) || "07".equals(tMonth) || "10".equals(tMonth))){
			return "";
		}
		
		//系统当前时间的前一个月的第一天
		Calendar cd = Calendar.getInstance();
		cd.add(Calendar.MONTH, -3);
		cd.set(Calendar.DAY_OF_MONTH, cd.getActualMinimum(Calendar.DAY_OF_MONTH));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cd.getTime());
	}
	
	public static String getAftTime(){
		//系统当前时间的前一个月的最后一天
		Calendar cd = Calendar.getInstance();
		cd.add(Calendar.MONTH, -1);
		cd.set(Calendar.DAY_OF_MONTH, cd.getActualMaximum(Calendar.DAY_OF_MONTH));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cd.getTime());
	}
	
	public static String getRiskSeqNo(String riskSeqNo){
		String mRiskSeqNo = "";   //主、附险标志（当lcpol中riskseqno为01或1时为主险，为02或2时为附加险）
		if(!("".equals(riskSeqNo) || riskSeqNo == null) ){
			
			if("01".equals(riskSeqNo) || "1".equals(riskSeqNo)){ 
				mRiskSeqNo =  "0";
			}else if("02".equals(riskSeqNo) || "2".equals(riskSeqNo)) {
				mRiskSeqNo =  "1";
			}
		}
		return mRiskSeqNo;
	}
	
	public static String getAddAmt(String contno,String contStat){  //附加险合计保费
		String sql = "";
		if("1".equals(contStat)){  //为承保状态
			sql = "select sumprem from lcpol where contno = '"+contno+"' and riskcode = (select riskcode from lmriskapp where riskcode  in (select riskcode from lcpol where contno = '"+contno+"') and subriskflag = 'S') ";//subriskflag为M是主险为S是附险			
		}else if("2".equals(contStat)){  //为退保状态
			sql = "select sumprem from lbpol where contno = '"+contno+"' and riskcode = (select riskcode from lmriskapp where riskcode  in (select riskcode from lbpol where contno = '"+contno+"') and subriskflag = 'S') ";
		}
		ExeSQL mExeSQL = new ExeSQL();
		String sumAmt = "";
		SSRS ssrs = mExeSQL.execSQL(sql);
		if(ssrs.MaxRow>0){
			double sum = 0;
			for(int i=1;i<=ssrs.MaxCol ; i++){ //当有多个附加险时累加
				if(!"".equals(ssrs.GetText(i, 1))){
					sum += Double.valueOf(ssrs.GetText(i, 1)).doubleValue();
				}
			}
			sumAmt = sum+"";
		}else{
			sumAmt = "0";
		}
		return getPrem(sumAmt);
	}
	
	public static String getAppntIdType(String idType){
		String AppntIdType = "";     //证件类型转换
		if(idType != null){
			if("0".equals(idType)){
				AppntIdType = "15";
			}else if("1".equals(idType)){
				AppntIdType = "20";
			}else if("2".equals(idType)){
				AppntIdType = "17";
			}else if("3".equals(idType)){
				AppntIdType = "21";
			}else {
				AppntIdType = "21";
			}
		}
		return AppntIdType;
	}
	
	public static String getContStat(String contno){  //保单状态转换
		/* 判断是否已做保全 */
		String sql= "select edortype from lpedoritem where contno = '"+contno+"'";
		SSRS ss = new ExeSQL().execSQL(sql);
		String mStart = "";
		
		if(ss.MaxRow <= 0 ){/* 对未做保全保单进行处理 */
			mStart = "2";
		}else {
			String xStart = ss.GetText(1, 1);
			if("WT".equals(xStart)){
				mStart = "3";
			}else if("CT".equals(xStart) || "XT".equals(xStart)) {
				mStart = "F";
			}else {
				mStart = "2";
			}
		}
		return mStart;
	}
	
	public static String getCoverageType(String termType) {
		String mTermType = "";     //保险年期类型转换
		if(!"".equals(termType)){
			if("A".equals(termType)){
				mTermType = "3";
			}else if("M".equals(termType)) {
				mTermType = "4";
			}else if("D".equals(termType)) {
				mTermType = "5";
			}else if("Y".equals(termType)) {
				mTermType = "2";
			}else {
				mTermType = "0";
			}
		}
		return mTermType;
	}
	
	public static String getProValue(String contNo,String contStat){
		String sql = "";
		if("1".equals(contStat)){  //为承保状态
			sql = "select manageCom from lccont where contno = '"+contNo+"'";			
		}else if("2".equals(contStat)){  //为退保状态
			sql = "select manageCom from lbcont where contno = '"+contNo+"'";
		}
		ExeSQL es = new ExeSQL();
		SSRS ssrs = es.execSQL(sql);
		String manageCom = "";
		if(ssrs.MaxRow > 0){
			manageCom = ssrs.GetText(1, 1);
		}
		if(props == null){   //通过传入key来从properties文件里取到分行号的前三位
			System.out.println("1");
			props =  new Properties();
			try {
		        URL url = ReadData.class.getClassLoader().getResource("/com/sinosoft/midplat/kernel/util/manageCom.properties");  
		        String path = URLDecoder.decode(url.getPath(), "utf-8");
		        
				InputStream in = new BufferedInputStream (
						new FileInputStream(path));
				props.load(in);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if("".equals(manageCom)){
			return "";
		}
		String mSubBank = props.getProperty (manageCom);
//		System.out.println(manageCom+" *************== "+mSubBank);
		if("".equals(mSubBank) || mSubBank == null){
			return manageCom.substring(2,4)+manageCom.substring(5,6);
		}
		return mSubBank;

	}
	
	public static String getPremTermType(String termType) {
		String mTermType = "";     //缴费年期类型转换
		if(!"".equals(termType)){
			if("A".equals(termType)){
				mTermType = "3";
			}else if("M".equals(termType)) {
				mTermType = "5";
			}else if("D".equals(termType)) {
				mTermType = "5";
			}else if("Y".equals(termType)) {
				mTermType = "2";
			}else {
				mTermType = "0";
			}
		}
		return mTermType;
	}
	
	public static String getPrem(String prem){
		byte[] b = new byte[15];
		if(!"".equals(prem)){
			long mPrem = NumberUtil.yuanToFen(prem);
			System.arraycopy("000000000000000".getBytes(),0, b, 0, b.length-((mPrem+"").length()));
			System.arraycopy((mPrem+"").getBytes(),0, b, b.length-((mPrem+"").length()), (mPrem+"").length());
		}else{
			b = "".getBytes();
		}
		return new String(b);
	}
	
	public static String getRela(String rela){
		String mRela = "";   //被保人关系转换
		if(rela != null){
			if("00".equals(rela)){
				mRela = "1";
			}else if("01".equals(rela)){
				mRela = "6";
			}else if("02".equals(rela)){
				mRela = "7";
			}else if("03".equals(rela)){
				mRela = "4";   //儿女转换
			}else if("04".equals(rela)){
				mRela = "2";
			}else if("05".equals(rela)){
				mRela = "3";
			}else if("12".equals(rela)){
				mRela = "16";  //兄弟姐妹转换
			}else {
				mRela = "45";
			}
		}
		return mRela;
	}
	
	public static String getDates(String date){
		StringBuffer sDate = new StringBuffer();   //日期格式转换
		if(date != null && !"".equals(date)){
			sDate.append(date.replaceAll("-", ""));
        }
		return sDate.toString();
	}
	
	public static String getPremType(String premType){
		String mPremType = "";       //缴费方式转换
		if(premType != null){
			if("-2".equals(premType)){//约定缴费
				mPremType = "9";
			}else if("-1".equals(premType)){//不定期缴费
				mPremType = "-1";
			}else if("0".equals(premType)){//趸交
				mPremType = "0";
			}else if("1".equals(premType)){//月缴
				mPremType = "1";
			}else if("12".equals(premType)){//年缴
				mPremType = "12";
			}else if("3".equals(premType)){//季缴
				mPremType = "3";
			}else if("6".equals(premType)){//半年缴
				mPremType = "6";
			}else {
				mPremType = "9";
			}
		}
		return mPremType;
	}
	
	public static String getCoverage(String coverage) {
		String mCoverage =  "";        //保障年期转换
		if(!"".equals(coverage) && coverage != null){
			mCoverage = coverage;
		}else{
			mCoverage = "0";
		}
		return mCoverage;
	}
	
	public static File writerFile(String resource , String fileName){
		File f = null;
		String url = "/wasfs/lis_war/midplat/BalanceFile/"+DateUtil.getCurDate("yyyyMMdd")+"/";
		try{
			f = new File(url);
			if(!f.exists()){
				f.mkdirs();
			}
			fileName = url+fileName;
			f = new File(fileName);
			if(f.exists()){
				FileOutputStream fos = new FileOutputStream(f);
				fos.write("".getBytes());
			}
			FileWriter fw=new FileWriter(fileName,true);   
	        BufferedWriter bw=new BufferedWriter(fw);   
	        bw.write(resource);   
	        bw.newLine();   
	        bw.close();  
	        fw.close();   
		}catch(IOException ie){
			ie.printStackTrace();
		}
		
		return f;
	}
	
	public static void main(String[] args) {
		new ReadData(null).service(null);
//		System.out.println(new ReadData(null).getBefTime());
		
	}
	
}
