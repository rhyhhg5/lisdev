package com.sinosoft.midplat.kernel.service.check;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/***
 * @date 2012-7-19
 * @author wz
 * @title  用于校验电话号码是否在黑名单中
 */
public class BlackPhoneCheck {
	

	public static void checkPhone(String mPhone) throws Exception{
		
		//将传入的号码在中间表中查询
		if(!("".equals(mPhone) || mPhone == null)){
			
			String sql_phone = "select 1 from lkBlackPhone where Phone = '"+ mPhone +"' with ur";
			//如查到有数据，抛出异常
			if("1".equals(new ExeSQL().getOneValue(sql_phone))){
				throw new MidplatException("该电话号码已被使用，请确认后重新输入或转由人工核保！");
			}
		}
		
	} 

	/**
	 * 五行一邮、小银行 卖银保通产品的所有渠道增加电话号码重复性校验 add by LiXiaoLong 20140707
	 * **/
	public static void checkAllPhone(String mAppntNo,String mAppntPhone,String mAppntOfficePhone,String mAppntMobile) throws Exception{
		long one = System.currentTimeMillis();
		ExeSQL mExeSQL = new ExeSQL();
		if(!"".equals(mAppntPhone)&&mAppntPhone != null){
			String tSql = "select 1 from lccont a where stateflag = '1' and appntno !='"+mAppntNo+"' and exists ("+
			"select 1 from lcaddress where customerno = a.appntno and homephone ='"+mAppntPhone+
							"') with ur";
			SSRS mSSRS = mExeSQL.execSQL(tSql);
			if(mSSRS.MaxRow>=4){
				throw new MidplatException("投保人电话:" + mAppntPhone + "与多人重复！");
			}
			
			tSql = "select 1 from laagent where  mobile = '"+mAppntPhone+"' with ur";
			if("1".equals(mExeSQL.getOneValue(tSql))){
				throw new MidplatException("投保人电话与销售人员联系电话:" + mAppntPhone + "重复！");
			}
		}
//		if(!"".equals(mAppntOfficePhone)&&mAppntOfficePhone != null){
//			String tSql = "select 1 from lccont a where  stateflag = '1' and appntno !='"+mAppntNo+"' and exists ("+
//			"select 1 from lcaddress where customerno = a.appntno and companyphone ='"+mAppntOfficePhone+
//							"') with ur";
//			SSRS mSSRS = mExeSQL.execSQL(tSql);
//			if(mSSRS.MaxRow>=4){
//				throw new MidplatException("投保人电话与多人重复！");
//			}	
//			
//			tSql = "select 1 from laagent where  mobile = '"+mAppntOfficePhone+"' with ur";
//			if("1".equals(mExeSQL.getOneValue(tSql))){
//				throw new MidplatException("投保人电话与销售人员联系电话重复！");
//			}
//		}
		if(!"".equals(mAppntMobile)&&mAppntMobile != null){
			String tSql = "select 1 from lccont a where stateflag = '1' and appntno !='"+mAppntNo+"' and exists ("+
			"select 1 from lcaddress where customerno = a.appntno and mobile ='"+mAppntMobile+
							"') with ur";
			SSRS mSSRS = mExeSQL.execSQL(tSql);
			if(mSSRS.MaxRow>=4){
				throw new MidplatException("投保人电话" + mAppntMobile + "与多人重复！");
			}	

			tSql = "select 1 from laagent where  mobile = '"+mAppntMobile+"' with ur";
			if("1".equals(mExeSQL.getOneValue(tSql))){
				System.out.println("校验总耗时："+(System.currentTimeMillis() - one) / 1000.00 + " S");
				throw new MidplatException("投保人电话与销售人员联系电话" + mAppntMobile + "重复！");
			}
		}
	} 
	
	 /*
     * 工行保险销售资质校验    add by LiXiaoLong 20131125
     * */
	public static String checkAgentComAndSaleCdoe(Element pLCCont) {
    	String result = "";
    	ExeSQL exeSQL = new ExeSQL();
		String mSQL;
		SSRS ssrs= new SSRS();
		
    	String mPrtno = pLCCont.getChildText("PrtNo");
    	mSQL = "select AgentCom,AgentSaleCode from lccont where prtno='" + mPrtno + "'";
    	ssrs = exeSQL.execSQL(mSQL);
    	String tAgentCom = ssrs.GetText(1, 1);
		String tCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		
		//代理销售业务员资格证信息校验
    	if(ssrs.MaxRow > 0){
    		String tAgentSaleCode = ssrs.GetText(1, 2);
    		if(tAgentCom == null || tAgentCom.equals("")){
    			result = "中介公司代码必须录入，请核查！";
    		   	return result;    	
    		}
    		if(tAgentSaleCode == null || tAgentSaleCode.equals("")){
    			result = "代理销售业务员编码必须录入，请核查！";
    	   		return result;    	
    	   	}
    		
    		mSQL = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
    		ssrs = exeSQL.execSQL(mSQL);
    		if(ssrs.MaxRow > 0){
    			String tValidStart = ssrs.GetText(1, 1);
    			String tValidEnd = ssrs.GetText(1, 2);
    	   		if(tValidStart == null || tValidStart.equals("")){
    	   			result = "代理销售业务员资格证有效起期为空，请核查！";
    	   			return result;
    	   		}
    	   		if(tValidEnd == null || tValidEnd.equals("")){
    	   			result = "代理销售业务员资格证有效止期为空，请核查！";
    	   			return result;
    	   		}    	   	
    	   		if(compareDate(tCurrentDate,tValidEnd) == 1){
    	   			result = "代理销售业务员资格证已失效，请核查！";
    	   			return result;
    	   		}
    		}
    	}else {
    		result = "代理销售业务员资格证信息不完整，请核查！";
    		return result;
    	}
    	
		//中介公司许可证信息校验
    	mSQL = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
		ssrs = exeSQL.execSQL(mSQL);
		if(ssrs.MaxRow > 0){
			String tLicenseStartDate = ssrs.GetText(1, 1);
			String tLicenseEndDate  = ssrs.GetText(1, 2);
			String tEndFlag = ssrs.GetText(1, 3);
			if(tLicenseStartDate == null || tLicenseStartDate.equals("")){
				result = "中介公司许可证有效起期为空，请核查！";
	   			return result;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate.equals("")){
	   			result = "中介公司许可证有效止期为空，请核查！";
	   			return result;
	   		}
	   		if(tEndFlag == null || tEndFlag.equals("")){
	   			result = "中介机构合作终止状态为空，请核查！";
	   			return result;
	   		}
	   		if(tEndFlag.equals("Y")){
	   			result = "中介机构合作终止状态为失效，请核查！";
	   			return result;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate) == 1){
	   			result = "中介公司许可证已失效，请核查！";
	   			return result;
	   		}
		}else {
			result = "中介公司许可证信息不完整，请核查！";
			return result;
		}
		
		//业务员信息校验
		mSQL = "select AgentCode from lccont where prtno='" + mPrtno + "'";
		ssrs = exeSQL.execSQL(mSQL);
		if(ssrs.MaxRow > 0){
			String tAgentCode  = ssrs.GetText(1, 1);
			if(tAgentCode == null || tAgentCode.equals("")){
				result = "业务员编码必须录入，请核查！";
		   		return result;    	
		   	}
			mSQL = "select branchtype,branchtype2 from laagent where agentcode = '"+tAgentCode+"'";
			ssrs = exeSQL.execSQL(mSQL);
			String mBranchtype = ssrs.GetText(1, 1);
			if(ssrs.MaxRow > 0){
				if(mBranchtype.equals("1")|| mBranchtype.equals("2") || mBranchtype.equals("4")){
		   			mSQL = "select ValidStart,ValidEnd from LAQUALIFICATION where agentcode = '"+tAgentCode+"' and state='0'";
		   			ssrs = exeSQL.execSQL(mSQL);
				}else if(mBranchtype.equals("3")){
		   			mSQL = "select Quafstartdate,QuafEndDate from laagent where agentcode = '"+tAgentCode+"'";
		   			ssrs = exeSQL.execSQL(mSQL);
				}else {
		   			result = "业务员资销售渠道异常，请核查！";
		   			return result;
				}
				if(ssrs.MaxRow > 0){
					System.out.println();
		   			String tValidStart  = ssrs.GetText(1, 1);
		   			String tValidEnd  = ssrs.GetText(1, 2);
		   			if(tValidStart == null || tValidStart.equals("")){
		   				result = "业务员资格证有效起期为空，请核查！";
				   		return result;
				   	}
				   	if(tValidEnd == null || tValidEnd.equals("")){
				   		result = "业务员资格证有效止期为空，请核查！";
				   		return result;
				   	}
				   	if(compareDate(tCurrentDate,tValidEnd) == 1){
				   		result = "业务员资格证已失效，请核查！";
				   		return result;
				   	}
				}else{
				   	result = "业务员不存在有效资格证信息，请核查！";
				   	return result;
				}
			}else{
				result = "业务员不存在，请核查！";
				return result;
			}
		}
		return result;
    }
	
	/***
	 * 广东地区 投被保人关系为父子、母女、母子、父女、关系时，投、被保人年龄之差大于18岁
	 * **/
	public static boolean checkBirthday(String ApntBir, String InsuredBir)
			throws MidplatException {
		int age = 0;
		int staAppY = 0; //投保人
		int staAppM = 0;
		int staAppD = 0;
		int staInsureY = 0; //被保人
		int staInsureM = 0;
		int staInsureD = 0;

		if (ApntBir.length() == 8 && InsuredBir.length() == 8) {
			staAppY = Integer.parseInt(ApntBir.substring(0, 4));
			staAppM = Integer.parseInt(ApntBir.substring(4, 6));
			staAppD = Integer.parseInt(ApntBir.substring(6, 8));
			staInsureY = Integer.parseInt(InsuredBir.substring(0, 4));
			staInsureM = Integer.parseInt(InsuredBir.substring(4, 6));
			staInsureD = Integer.parseInt(InsuredBir.substring(6, 8));
			
		} else if (ApntBir.length() == 10 && InsuredBir.length() == 10) {
			staAppY = Integer.parseInt(ApntBir.substring(0, 4));
			staAppM = Integer.parseInt(ApntBir.substring(5, 7));
			staAppD = Integer.parseInt(ApntBir.substring(7, 10));
			staInsureY = Integer.parseInt(InsuredBir.substring(0, 4));
			staInsureM = Integer.parseInt(InsuredBir.substring(5, 7));
			staInsureD = Integer.parseInt(InsuredBir.substring(7, 10));
		} else {
			throw new MidplatException("投保人生日不符合规则！");
		}

		if (staAppY > staInsureY) {
			if (staAppM > staInsureM) {
				age = staAppY - staInsureY - 1;
			} else if (staAppM == staInsureM) {
				if (staAppD > staInsureD) {
					age = staAppY - staInsureY - 1;
				} else { 
					age = staAppY - staInsureY;
				}
			} else if (staAppM < staInsureM) {
				age = staAppY - staInsureY;
			}
		} else if (staAppY < staInsureY) {
			if (staAppM > staInsureM) {
				age = staInsureY - staAppY;
			} else if (staAppM == staInsureM) {
				if (staAppD >= staInsureD) {
					age = staInsureY - staAppY;
				} else {
					age = staInsureY - staAppY - 1;
				}
			} else if (staAppM < staInsureM) {
				age = staInsureY - staAppY - 1;
			}
		} else { // 同一年出生 零岁
			age = 0;
		}

		if (age >= 18) {
			return true;
		} else {
			return false;
		}
	}
	
	public static int compareDate(String tCurrentDate, String tValidEnd) {
		String mCurrentDate = tCurrentDate.replaceAll("-", "");
		String mValidEnd = tValidEnd.replaceAll("-", "");
		int pCurrentDate = Integer.parseInt(mCurrentDate);
		int pValidEnd = Integer.parseInt(mValidEnd);
		if(pCurrentDate > pValidEnd) {
			return 1;
		}else
		{
		return 0;
		}
	}	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		String mInFile = "C:/Users/Administrator/Desktop/工行添加保险销售资质校验/icbc_test.xml";
		Document pInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild("LCCont");
		long startTime = System.currentTimeMillis();
		String result = BlackPhoneCheck.checkAgentComAndSaleCdoe(mLCCont);
		System.out.println("校验总耗时："+(System.currentTimeMillis() - startTime) / 1000.00 + " S");
		if(!result.equals("")){
			throw new MidplatException(result);
		}else {
			System.out.println("程序结束…");	
		}
	}
}
