package com.sinosoft.midplat.kernel.service.check;

import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.YBTRiskSwtichDB;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LDRiskWrapSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.YBTRiskSwtichSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.lis.vschema.YBTRiskSwtichSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YBTRiskCheck {
	protected final Logger cLogger = Logger.getLogger(getClass());
	
	public void checkRiskForYBT(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into YBTRiskCheck...");
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		Element mLCCont = mTranData.getChild("LCCont");
		List lisLCIns = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
		String mMainRiskCode = mLCCont.getChild("LCInsureds")
				.getChild("LCInsured").getChild("Risks").getChild("Risk")
				.getChildText("MainRiskCode");

		// 查看核心是否停售
		if (!checkKernel(lisLCIns, mMainRiskCode)) {
			throw new MidplatException("该产品已停售!");
		}

		//因泉州银行不传贷款金额，添加泉州银行借款人意外险不能重复购买规则
		if ("53".equals(mBaseInfo.getChild("BankCode"))
				&& "YBT011".equals(mMainRiskCode)) {
			check511101(lisLCIns);
		}
		cLogger.info("Out YBTRiskCheck...");
	}
	
	private void check511101(List lisLCIns)
			throws MidplatException {
		cLogger.info("Into YBTRiskCheck.check511101()...");
		SSRS ssrs = null;
		for(int i=0;i<lisLCIns.size();i++){
			Element LCInsured = (Element)lisLCIns.get(i);
			String mIDNo = LCInsured.getChildTextTrim("LCInsured");
			String tSql = "select amnt from lcpol where stateflag = '1' and riskcode = '511102' and"
					+ " contno in (select contno from lcinsured where idno in ('"
					+ mIDNo + "')) with ur";
			ssrs = new ExeSQL().execSQL(tSql);
			if(ssrs.MaxRow>0){
				throw new MidplatException("该产品不能重复购买!");
			}
		}
		cLogger.info("Out YBTRiskCheck.check511101()...");
	}

	//查看核心是否停售
	private boolean checkKernel(List lisLCIns, String riskCode) {
		LDRiskWrapDB lDRiskWrapDB = new LDRiskWrapDB();
		lDRiskWrapDB.setRiskWrapCode(riskCode);
		LDRiskWrapSet lDRiskWrapSet = lDRiskWrapDB.query();
		List tRiskCode = new ArrayList();
		tRiskCode.add(riskCode);
		String tEndDate = "";
		cLogger.info("查询到" + lDRiskWrapSet.size() + "条套餐信息");
		if (lDRiskWrapSet.size() > 0) {
			for (int i = 1; i <= lDRiskWrapSet.size(); i++) {
				LDRiskWrapSchema lDRiskWrapSchema = lDRiskWrapSet.get(i);
				tRiskCode.add(lDRiskWrapSchema.getRiskCode());
				tEndDate = DateUtil.date10to8(lDRiskWrapSchema.getEndDate());
				cLogger.info(lDRiskWrapSchema.getRiskCode() + "：tEndDate==="
						+ tEndDate);
				if (null != tEndDate && !"".equals(tEndDate)) {
					if (Integer.parseInt(DateUtil.date10to8(tEndDate)) <= DateUtil
							.getCur8Date())
						return false;
				}
			}
		}

		for (int i = 0; i < lisLCIns.size(); i++) {
			Element tLCInsured = (Element) lisLCIns.get(i);
			List lisRisk = tLCInsured.getChild("Risks").getChildren("Risk");
			for (int j = 0; j < lisRisk.size(); j++) {
				Element tRisk = (Element) lisRisk.get(j);
				tRiskCode.add(tRisk.getChildText("RiskCode"));
			}
		}
		// 使用该语句去重
		tRiskCode = new ArrayList(new HashSet(tRiskCode));
		cLogger.info("共有" + tRiskCode.size() + "条险种信息");
		for (int i = 0; i < tRiskCode.size(); i++) {
			cLogger.info("处理第" + (i + 1) + "条信息，险种代码：" + tRiskCode.get(i));
			LMRiskAppDB lMRiskAppDB = new LMRiskAppDB();
			lMRiskAppDB.setRiskCode((String) tRiskCode.get(i));
			LMRiskAppSet lMRiskAppSet = lMRiskAppDB.query();
			cLogger.info("看一下有几条：" + lMRiskAppSet.size() + "条");
			if (lMRiskAppSet.size() > 0) {
				LMRiskAppSchema lMRiskAppSchema = lMRiskAppSet.get(1);
				tEndDate = DateUtil.date10to8(lMRiskAppSchema.getEndDate());
				cLogger.info(tRiskCode.get(i) + "：tEndDate===" + tEndDate);
				if (null != tEndDate && !"".equals(tEndDate)) {
					if (Integer.parseInt(DateUtil.date10to8(tEndDate)) <= DateUtil
							.getCur8Date())
						return false;
				}
			}
		}
		return true;
	}

	/**
	 * 计算投保人年龄
	 * */
	public static int getAppntAge(String ageDate) {

		Date now = new Date();
		String sdf = new SimpleDateFormat("yyyyMMdd").format(now);
		int age = 0;
		int staY = Integer.parseInt(ageDate.substring(0, 4));
		int staM = Integer.parseInt(ageDate.substring(4, 6));
		int staD = Integer.parseInt(ageDate.substring(6, 8));

		int nowY = Integer.parseInt(sdf.substring(0, 4));
		int nowM = Integer.parseInt(sdf.substring(4, 6));
		int nowD = Integer.parseInt(sdf.substring(6, 8));

		if (staM > nowM) {
			age = nowY - staY - 1;
		} else if (staM == nowM) {
			if (nowD > staD) {
				age = nowY - staY;
			} else if (nowD == staD) {
				age = nowY - staY;
			} else if (nowD < staD) {
				age = nowY - staY - 1;
			}

		} else if (staM < nowM) {
			age = nowY - staY;
		}

		return age;
	}
	/*CREATE:
	 *   被保人年龄校验；2017-11-8;孟烁
	 *   此方法只校验28天起，如需其他起期请另写方法；
	 * tLCInsured:被保人节点；endYear:被保人最大年龄
	 */
	
	public void checkAge(String insuBirthday,int endYear) throws MidplatException{
		cLogger.info("Into YBTRiskCheck.checkAge()28天...");
		
		int tInsuAge = getAppntAge(insuBirthday);
		Date date = DateUtil.parseDate(insuBirthday, "yyyyMMdd");
		Calendar  calendar2 = Calendar.getInstance();
		calendar2.setTime(date);
		calendar2.add(Calendar.DAY_OF_MONTH, 28);
		String afterDate = DateUtil.getDateStr(calendar2, "yyyyMMdd");
		String currDate = DateUtil.getCurDate("yyyyMMdd");
		if((Integer.parseInt(afterDate) > Integer.parseInt(currDate) || tInsuAge > endYear)){
			throw new MidplatException("被保人年龄28天到"+endYear+"周岁!!!");
		}
		
		cLogger.info("Out YBTRiskCheck.checkAge()28天...");
	}
	
	/**
	 * 险种停售校验		add by GaoJinfu 20180328
	 * @param args
	 * @throws Exception
	 */
	YBTRiskSwtichSet ybtRiskSwtichSet = null;
	YBTRiskSwtichDB ybtRiskSwtichDB = null;

	public String riskStopCheck(String bankcode, String managecom,
			String riskcode, String channel) {
		String STATUS_ON = "1";
		String STATUS_OFF = "2";
		channel = getChannel(channel);
		ybtRiskSwtichSet = new YBTRiskSwtichSet();
		ybtRiskSwtichDB = new YBTRiskSwtichDB();
		ybtRiskSwtichDB.setriskcode(riskcode);
		ybtRiskSwtichDB.setstatus("1");
		ybtRiskSwtichDB.setdatatype("ST");
		ybtRiskSwtichSet = ybtRiskSwtichDB.query();
		String nowDate = DateUtil.getCur10Date();
		cLogger.info("获取到" + riskcode + "的停售信息" + ybtRiskSwtichSet.size() + "条");
		if (ybtRiskSwtichSet.size() > 0) {
			for (int i = 0; i < ybtRiskSwtichSet.size(); i++) {
				YBTRiskSwtichSchema ybtRiskSwtichSchema = ybtRiskSwtichSet
						.get(i);
				String tchangedate = ybtRiskSwtichSchema.getchangedate();
				String tBankCode = ybtRiskSwtichSchema.getbankcode();
				String tComCode = ybtRiskSwtichSchema.getcomcode();
				String tChannel = ybtRiskSwtichSchema.getchannel();
				if (("00".equals(tBankCode) || bankcode.equals(tBankCode))
						&& ("86".equals(tComCode) || managecom.substring(0, 4).equals(tComCode))
						&& ("00".equals(tChannel) || channel.equals(tChannel))
						&& nowDate.compareTo(tchangedate) >= 0) {
					return STATUS_OFF;
				}else{
					return STATUS_ON;
				}
			}
		} else {
			return STATUS_ON;
		}
		return STATUS_ON;
	}
	
	private String getChannel(String channel) {
		Map map = new HashMap();
		if ("0".equals(channel) || "1".equals(channel) || "8".equals(channel)
				|| "e".equals(channel) || "f".equals(channel) || "a".equals(channel)) {
			map.put("0", "9");
			map.put("1", "c");
			map.put("8", "d");
			map.put("e", "e");
			map.put("f", "f");
			map.put("a", "a");
			return (String) map.get(channel);
		} else {
			return "9";
		}
	}
	
	/**
	 * 产品销售校验	add by GaoJinfu 20180329
	 * 如果产品按套餐销售只配置套餐编码即可，特殊如百万安行，健康天使配置的是主险代码
	 * 如果产品按险种销售只需配置主险代码即可
	 * @param bankCode
	 * @param manageCom
	 * @param riskCode
	 * @param channel
	 * @param dateType
	 * @return
	 */
	public boolean riskValidCheck(String bankCode, String manageCom,
			String riskCode, String channel, String dateType) {
		cLogger.info("Into YBTRiskCheck.gxRiskCheck()...");
		channel = getChannel(channel);
		ybtRiskSwtichSet = new YBTRiskSwtichSet();
		ybtRiskSwtichDB = new YBTRiskSwtichDB();
		ybtRiskSwtichDB.setriskcode(riskCode);
		ybtRiskSwtichDB.setbankcode(bankCode);
		ybtRiskSwtichDB.setstatus("1");
		ybtRiskSwtichDB.setdatatype(dateType);
		ybtRiskSwtichSet = ybtRiskSwtichDB.query();
		String nowDate = DateUtil.getCur10Date();
		cLogger.info(riskCode+"有"+ybtRiskSwtichSet.size()+"条配置信息。。。");
		for (int i = 1; i <= ybtRiskSwtichSet.size(); i++) {
			YBTRiskSwtichSchema ybtRiskSwtichSchema = new YBTRiskSwtichSchema();
			ybtRiskSwtichSchema = ybtRiskSwtichSet.get(i);
			String comCode = ybtRiskSwtichSchema.getcomcode();
			System.out.println("comCode================="+comCode);
			System.out.println("manageCom.substring(0, 4)================="+manageCom.substring(0, 4));
			String changeDate = ybtRiskSwtichSchema.getchangedate();
			String tchannel = ybtRiskSwtichSchema.getchannel();
			if(!"".equals(comCode) && null != comCode){
				if ((comCode.equals(manageCom.substring(0, 4)) || comCode
						.equals(manageCom.substring(0, 2)))
						&& (nowDate.compareTo(changeDate) >= 0)
						&& ("00".equals(tchannel) || channel.equals(tchannel))) {
					return true;
				}
			}else{
				return false;
			}
		}
		cLogger.info("Out YBTRiskCheck.gxRiskCheck()...");
		return false;
	}
	
	/**
	 * 校验保全权限 add by GaoJinfu 20180327
	 * @param pInXmlDoc
	 * @return
	 */
	public boolean checkBQPower(Document pInXmlDoc) {
		cLogger.info("Into ABC_BaoQuanInput.checkBQPower()...");
		Element mDataSet = pInXmlDoc.getRootElement();
		Element mBaseInfo = mDataSet.getChild("BaseInfo");
		Element mEdorItemInfoItem = mDataSet.getChild("EdorItemInfo").getChild(
				"Item");
		String mContNo = mEdorItemInfoItem.getChildText("ContNo");
		ExeSQL exe = new ExeSQL();
		// 借款人意外险不能做保全添加 add by gaojinfu 20170906
		String mSql = "select riskcode from lcpol where contno = '" + mContNo
				+ "' with ur";
		System.out.println("mSql==" + mSql);
		SSRS ssrs = exe.execSQL(mSql);
		if (ssrs.MaxRow > 0) {
			for (int i = 1; i <= ssrs.MaxRow; i++) {
				if ("511101".equals(ssrs.GetText(i, 1))) {
					cLogger.info("该产品不允许做退保、犹豫期退保");
					return false;
				}
			}
		}
		mSql = "select riskcode from lktransstatus where funcflag = '01' and status = '1' and polno = '"
				+ mContNo + "' with ur";
		String tRiskCode = exe.getOneValue(mSql);
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mContNo);
		LCContSet tLCContSet = tLCContDB.query();
		if(tLCContSet.size() == 1){
			LCContSchema tLCContSchema = tLCContSet.get(1);
			mSql = "select substr(managecom,1,4) from lkcodemapping where bankcode = '"
					+ mBaseInfo.getChildText("BankCode")
					+ "' and zoneno='"
					+ mBaseInfo.getChildText("ZoneNo")
					+ "' and banknode='"
					+ mBaseInfo.getChildText("BrNo") + "' with ur";
			String comcode = exe.getOneValue(mSql);
			System.out.println("mSql==" + mSql);
			mSql = "select status,changedate from ybtriskswtich where datatype = 'BQ' and bankcode = '"
					+ mBaseInfo.getChildText("BankCode")
					+ "' and (comcode = '86' or comcode = '"
					+ comcode
					+ "') and (channel = '00' or channel = '"
					+ tLCContSchema.getCardFlag()
					+ "') and riskcode = '"
					+ tRiskCode + "' with ur";
			System.out.println("mSql==" + mSql);
			ssrs = new ExeSQL().execSQL(mSql);
			if (ssrs.MaxRow != 1) {
				cLogger.info("保全权限未开通或保全配置有误，请核实!");
				return false;
			} else {
				String status = ssrs.GetText(1, 1);
				int changedate = Integer.parseInt(DateUtil.date10to8(ssrs.GetText(
						1, 2)));
				int nowDate = DateUtil.getCur8Date();
				if (!("1".equals(status) && (nowDate >= changedate))) {
					cLogger.info("保全权限未开通或保全配置有误，请核实!");
					return false;
				}
			}
		} else {
			return false;
		}
		cLogger.info("Out ABC_BaoQuanInput.checkBQPower()...");
		return true;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		String mInFile = "F:\\000gaojinfu\\bankMassage\\trail.xml";
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));

//		new YBTRiskCheck().checkRiskForYBT(mInXmlDoc);
		boolean bb  = new YBTRiskCheck().riskValidCheck("66", "86110000",
				"232402", "c", "WN");
		System.out.println(bb);
		System.out.println("成功结束！");
	}
}
