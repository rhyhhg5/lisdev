package com.sinosoft.midplat.kernel.service.query;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LKCodeMapping1DB;
import com.sinosoft.lis.db.LKCodeMappingDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LKCodeMapping1Set;
import com.sinosoft.lis.vschema.LKCodeMappingSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YbtContQueryBL implements XmlTag {
	private final String cContNo;
	
	private final static Logger cLogger = Logger.getLogger(YbtContQueryBL.class);
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	
	public YbtContQueryBL(String pContNo) {
		cContNo = pContNo;
	}
	
	public Document deal() throws MidplatException {
		cLogger.info("Into YbtContQueryBL.deal()...");
		
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		
		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		long tStartMillism = System.currentTimeMillis(); 
		Element mLCCont = getLCCont(cContNo);
		cLogger.info("YbtContQueryBL.getLCCont()耗时：" + (System.currentTimeMillis()-tStartMillism)/1000.0 + "s");
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		
		cLogger.info("Out YbtContQueryBL.deal()!");
		return new Document(mTranData);
	}
	
	public Element getLCCont(String pContNo) throws MidplatException {
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(pContNo);
		if (!mLCContDB.getInfo()) {
			throw new MidplatException("查询保单合同信息失败！");
		}
		
		Element mContNo = new Element(ContNo);
		mContNo.setText(mLCContDB.getContNo());
		
		Element mPrtNo = new Element(PrtNo);
		mPrtNo.setText(mLCContDB.getPrtNo());
		
		Element mProposalContNo = new Element(ProposalContNo);
		mProposalContNo.setText(mLCContDB.getProposalContNo());
		
		Element mConsignNo = new Element("ConsignNo");
		mConsignNo.setText(mLCContDB.getConsignNo());
		
		Element mPayIntv = new Element(PayIntv);
		mPayIntv.setText(
				String.valueOf(mLCContDB.getPayIntv()));
		
		Element mPayMode = new Element(PayMode);
		mPayMode.setText(mLCContDB.getPayMode());
		
		Element mAgentCode = new Element(AgentCode);
		//内部程序使用的是老的工号agentcode，但是显示给客户的都是新的集团工号 20141204
//		mAgentCode.setText(mLCContDB.getAgentCode());
		String mSQL = "select GroupAgentCode from laagent where AgentCode = '" + mLCContDB.getAgentCode() + "' with ur";
		mAgentCode.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentGroupCode = new Element("AgentGroupCode");
		mAgentGroupCode.setText(mLCContDB.getAgentGroup());
		
		Element mAgentGroup = new Element(AgentGroup);
		mSQL = "select name from labranchgroup where agentgroup='" + mLCContDB.getAgentGroup() + "' with ur";
		mAgentGroup.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentCom = new Element(AgentCom);//1.银邮网点的名称
		mSQL = "select name from lacom where agentcom='" + mLCContDB.getAgentCom() + "' with ur";
		mAgentCom.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentName = new Element(AgentName);//4.保险机构银保专管员姓名
		mSQL = "select name from laagent where AgentCode='" + mLCContDB.getAgentCode() + "' with ur";
		mAgentName.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mPrem = new Element(Prem);
		mPrem.setText(
				new DecimalFormat("0.00").format(mLCContDB.getPrem()));
		
		Element mAmnt = new Element(Amnt);
		mAmnt.setText(
				new DecimalFormat("0.00").format(mLCContDB.getAmnt()));
		
		Element mAnHuiNXS_Mult = new Element("AnHuiNXS_Mult");	//picch小额信贷险为“保额算保费”，无份数概念，安徽农信社非得要份数，借用此字段
		mAnHuiNXS_Mult.setText(
				String.valueOf((int)mLCContDB.getCopys()));
		
		Element mManageCom = new Element(ManageCom);
		mManageCom.setText(mLCContDB.getManageCom());
		
		Element mPolApplyDate = new Element(PolApplyDate);
		mPolApplyDate.setText(mLCContDB.getPolApplyDate());
		
		Element mSignDate = new Element(SignDate);
		mSignDate.setText(mLCContDB.getSignDate());
		
		Element mCValiDate = new Element(CValiDate);
		mCValiDate.setText(mLCContDB.getCValiDate());
		
		Element mContEndDate = new Element(ContEndDate);
		mContEndDate.setText(mLCContDB.getCInValiDate());	//picch特有，保单级保单终止日期
		
		LDComDB mLDComDB = new LDComDB();
		mLDComDB.setComCode(mLCContDB.getManageCom());
		if (!mLDComDB.getInfo()) {
			throw new MidplatException("查询管理机构信息失败！");
		}
		Element mComLocation = new Element(ComLocation);
		mComLocation.setText(mLDComDB.getAddress());
		
		Element mLetterService = new Element("LetterService");
		mLetterService.setText(mLDComDB.getLetterServiceName());
		
		Element mZipCode = new Element("ZipCode");
		mZipCode.setText(mLDComDB.getZipCode());
		
		Element mComPhone = new Element(ComPhone);
		mComPhone.setText("95591 / 4006695518");
		
		Element mSpecContent = new Element(SpecContent);
		mSpecContent.setText(mLCContDB.getRemark());

		Element mLCAppnt = getLCAppnt(pContNo);
		
		Element mLCInsureds = getLCInsureds(pContNo);
		mAmnt.setText(	//picch这边保额取主险保额。LCCont表中的保额不准确，他会简单累加所有险种(LCPol)保额。
				mLCInsureds.getChild(LCInsured).getChild(Risks).getChild(Risk).getChildText(Amnt));

		//信用方强烈要求在保单上打印贷款的起止日期。请考虑在中间表中插入此字段。 
	    //另，贷款凭证号请一并考虑。
		
		//add by mxk
		ExeSQL mExeSQL = new ExeSQL();
		LKCodeMappingDB LK_Db = new LKCodeMappingDB();
		LK_Db.setAgentCom(mLCContDB.getAgentCom());
		LKCodeMappingSet lk_set = LK_Db.query();
		
		LKCodeMapping1DB LK_Db1 = new LKCodeMapping1DB();
		LK_Db1.setAgentCom(mLCContDB.getAgentCom());
		LKCodeMapping1Set lk_set1 = LK_Db1.query();
		
		if(lk_set.size() <1 && lk_set1.size() <1){
			throw new MidplatException("查询代理人信息失败!");
		}
		String bankcode = "";
		if(lk_set.size()>=1){
			 bankcode = lk_set.get(1).getBankCode();
		}
		else{
			 bankcode = lk_set1.get(1).getBankCode();
		}
	
		String SQL = "";
		cLogger.info("获取到的银行编码是::"+bankcode);   //针对大连农商行做特殊处理
		if(bankcode.equals("58")){
			SQL = "select ServiceStartTime,ServiceEndTime,TempFeeNo ,EdorNo from LkTransStatus where prtno ='"
				+ mLCContDB.getPrtNo()+"' and makedate = '"+mLCContDB.getMakeDate()
				+"' and funcflag = '01'  order by maketime desc with ur";
		}
		else{
		 SQL = "select ServiceStartTime,ServiceEndTime,TempFeeNo ,EdorNo from LkTransStatus where prtno ='"
				+ mLCContDB.getPrtNo()+"' and proposalno = '" +mLCContDB.getPrtNo()+ "' and makedate = '"+mLCContDB.getMakeDate()
				+"' and funcflag = '01'  order by maketime desc with ur";
		}
		SSRS tSSRS = mExeSQL.execSQL(SQL);
		
		String mServiceStartTime = "";
		String mServiceEndTime = "";
		String mTempFeeNo = "";
		String mEdorNo = "";
		
		if(tSSRS.getMaxRow()>= 1){
		 mServiceStartTime = tSSRS.GetText(1, 1);
		 mServiceEndTime = tSSRS.GetText(1, 2);
		 mTempFeeNo = tSSRS.GetText(1, 3);
		 mEdorNo = tSSRS.GetText(1, 4);
		}
		
		Element mLoanDate = new Element("LoanDate");
		mLoanDate.setText(mServiceStartTime);
		
		Element mLoanEndDate = new Element("LoanEndDate");
		mLoanEndDate.setText(mServiceEndTime);
		
		Element mLoanInvoiceNo = new Element("LoanInvoiceNo");
		mLoanInvoiceNo.setText(mTempFeeNo);
		
		Element mLoanContractAmt = new Element("LoanContractAmt");
		mLoanContractAmt.setText(mEdorNo);
		
		Element mBankaccno =  new Element("Bankaccno"); 
		mBankaccno.setText(mLCContDB.getBankAccNo());
		
		Element mLCCont = new Element(LCCont);
		mLCCont.addContent(mContNo);
		mLCCont.addContent(mPrtNo);
		mLCCont.addContent(mProposalContNo);
		mLCCont.addContent(mConsignNo);
		mLCCont.addContent(mPayIntv);
		mLCCont.addContent(mPayMode);
		mLCCont.addContent(mAgentCode);
		mLCCont.addContent(mAgentName);//1.银邮网点的名称
		mLCCont.addContent(mAgentGroupCode);
		mLCCont.addContent(mAgentGroup);
		mLCCont.addContent(mAgentCom);//4.保险机构银保专管员姓名
		mLCCont.addContent(mPrem);
		mLCCont.addContent(mAmnt);
		mLCCont.addContent(mAnHuiNXS_Mult);
		mLCCont.addContent(mManageCom);
		mLCCont.addContent(mPolApplyDate);
		mLCCont.addContent(mSignDate);
		mLCCont.addContent(mCValiDate);
		mLCCont.addContent(mContEndDate);
		mLCCont.addContent(mComLocation);
		mLCCont.addContent(mLetterService);
		mLCCont.addContent(mZipCode);
		mLCCont.addContent(mComPhone);
		mLCCont.addContent(mSpecContent);
		mLCCont.addContent(mLoanDate);
		mLCCont.addContent(mLoanEndDate);
		mLCCont.addContent(mLoanInvoiceNo);
		mLCCont.addContent(mLoanContractAmt);
		mLCCont.addContent(mBankaccno);
		mLCCont.addContent(mLCAppnt);
		mLCCont.addContent(mLCInsureds);
	
		return mLCCont;
	}
	
	private Element getLCAppnt(String pContNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCAppnt()...");
		
		LCAppntDB mLCAppntDB = new LCAppntDB();
		mLCAppntDB.setContNo(pContNo);	//核心只支持单投保人。ContNo是主键
		if (!mLCAppntDB.getInfo()) {
			throw new MidplatException("查询投保人信息失败！");
		}
		LCAddressDB mAddressDB = new LCAddressDB();
		mAddressDB.setCustomerNo(mLCAppntDB.getAppntNo());
		mAddressDB.setAddressNo(mLCAppntDB.getAddressNo());
		if (!mAddressDB.getInfo()) {
			throw new MidplatException("查询投保人地址信息失败！");
		}
		
		Element mAppntNo = new Element(AppntNo);
		mAppntNo.setText(mLCAppntDB.getAppntNo());
		
		Element mAppntName = new Element(AppntName);
		mAppntName.setText(mLCAppntDB.getAppntName());
		
		Element mAppntSex = new Element(AppntSex);
		mAppntSex.setText(mLCAppntDB.getAppntSex());
		
		Element mAppntBirthday = new Element(AppntBirthday);
		mAppntBirthday.setText(mLCAppntDB.getAppntBirthday());
		
		Element mAppntIDType = new Element(AppntIDType);
		mAppntIDType.setText(mLCAppntDB.getIDType());
		
		Element mAppntIDNo = new Element(AppntIDNo);
		mAppntIDNo.setText(mLCAppntDB.getIDNo());
		
		Element mOccupationcode = new Element("OccupationCode");
		mOccupationcode.setText(mLCAppntDB.getOccupationCode());
		
		Element mAddress = new Element(Address);
		mAddress.setText(mAddressDB.getPostalAddress());
		
		Element mZipCode = new Element(ZipCode);
		mZipCode.setText(mAddressDB.getZipCode());
		
		Element mMobile = new Element(Mobile);
		mMobile.setText(mAddressDB.getMobile());
		
		Element mPhone = new Element(Phone);
		mPhone.setText(mAddressDB.getPhone());
		
		Element mEmail = new Element(Email);
		mEmail.setText(mAddressDB.getEMail());
		
		//针对建行保单查询返回投保人信息六段信息处理
		Element GrpName = new Element("GrpName");                //国家
		Element PostalProvince = new Element("PostalProvince");   //省
		Element PostalCity = new Element("PostalCity");   //市
		Element PostalCounty = new Element("PostalCounty");   //县
		Element PostalCommunity = new Element("PostalCommunity"); //详细地址
		try {
		LCContDB LCCont = new LCContDB();
		LCCont.setContNo(pContNo);
		
		LCContSet lcs = LCCont.query() ;
		if(lcs.size() ==0){
			throw new MidplatException("查询保单信息失败");
		}
		LCContSchema lc = lcs.get(1);
		String agentCom = lc.getAgentCom();
		String SQL = "";
		ExeSQL exe = new ExeSQL();
		if(agentCom.startsWith("PY004")){
			try{
					String  code_str = mAddressDB.getGrpName();
					cLogger.info("国家编码::"+code_str);
					if( code_str != null && !code_str.equals(""))
					{
						SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+mAddressDB.getGrpName()+"'";
						SSRS srs = exe.execSQL(SQL);
						GrpName.setText(srs.GetText(1, 1));
					}
			
				
					code_str = mAddressDB.getPostalProvince();
					cLogger.info("省级编码::"+code_str);
					if( code_str != null && !code_str.equals("") )
					{
						SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+mAddressDB.getPostalProvince()+"'";
						SSRS srs1 = exe.execSQL(SQL);
						PostalProvince.setText(srs1.GetText(1, 1));
					}
			
				
					code_str = mAddressDB.getPostalCity();
					cLogger.info("市级编码::"+code_str);
					if( code_str != null && !code_str.equals("") )
					{
						SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+mAddressDB.getPostalCity()+"'";
						SSRS srs2 = exe.execSQL(SQL);
						PostalCity.setText(srs2.GetText(1, 1));
					}
					code_str = mAddressDB.getPostalCounty();
					cLogger.info("县级编码::"+code_str);
					if( code_str != null && !code_str.equals("") )
					{
						SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+mAddressDB.getPostalCounty()+"'";
						SSRS srs3 = exe.execSQL(SQL);
						PostalCounty.setText(srs3.GetText(1, 1));
					}
				//详细地址直接设置
//				SQL = "select code from ldcode where  codetype ='ybtaddress' and code ='"+mAddressDB.getPostalCommunity()+"'";
//				SSRS srs4 = exe.execSQL(SQL);
//				PostalCommunity.setText(srs4.GetText(1, 1));
				PostalCommunity.setText(mAddressDB.getPostalCommunity());
				
			}
			catch(Exception e){
				throw new MidplatException("设置地址信息失败,请检查国家省市县详细地址在投保时时候录入!");
			}
		}
		
		}
		
		catch(Exception e){
			throw new MidplatException("查询银行编码失败!");
		}
		
		Element mLCAppnt = new Element(LCAppnt);
		mLCAppnt.addContent(mAppntNo);
		mLCAppnt.addContent(mAppntName);
		mLCAppnt.addContent(mAppntSex);
		mLCAppnt.addContent(mAppntBirthday);
		mLCAppnt.addContent(mAppntIDType);
		mLCAppnt.addContent(mAppntIDNo);
		mLCAppnt.addContent(mOccupationcode);
		mLCAppnt.addContent(mAddress);
		mLCAppnt.addContent(mZipCode);
		mLCAppnt.addContent(mMobile);
		mLCAppnt.addContent(mPhone);
		mLCAppnt.addContent(mEmail);
		mLCAppnt.addContent(GrpName);
		mLCAppnt.addContent(PostalProvince);
		mLCAppnt.addContent(PostalCity);
		mLCAppnt.addContent(PostalCounty);
		mLCAppnt.addContent(PostalCommunity);
		
		cLogger.info("Out YbtContQueryBL.getLCAppnt()!");
		return mLCAppnt;
	}
	
	private Element getLCInsureds(String pContNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCInsureds()...");
		
		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setContNo(pContNo);
		LCInsuredSet mLCInsuredSet = mLCInsuredDB.query();
		LKTransStatusDB lks = new LKTransStatusDB();
		lks.setPolNo(pContNo); 
		lks.setFuncFlag("01");
		LKTransStatusSet lkss = lks.query();
		if((null==lkss) || mLCInsuredSet.size()<1){
			throw new MidplatException("查询被保人信息失败！");
		}
		if ((null==mLCInsuredSet) || (mLCInsuredSet.size()<1)) {
			throw new MidplatException("查询被保人信息失败！");
		}
		
		Element mLCInsureds = new Element("LCInsureds");
		for (int i = 1; i <= mLCInsuredSet.size(); i++) {
			LCInsuredSchema tLCInsuredSchema = mLCInsuredSet.get(i);
			LCAddressDB tAddressDB = new LCAddressDB();
			tAddressDB.setCustomerNo(tLCInsuredSchema.getInsuredNo());
			tAddressDB.setAddressNo(tLCInsuredSchema.getAddressNo());
			if (!tAddressDB.getInfo()) {
				throw new MidplatException("查询被保人地址信息失败！");
			}
			
			Element tInsuredNo = new Element(InsuredNo);
			tInsuredNo.setText(tLCInsuredSchema.getInsuredNo());
			
			Element tAppntName = new Element(Name);
			tAppntName.setText(tLCInsuredSchema.getName());

			Element tSex = new Element(Sex);
			tSex.setText(tLCInsuredSchema.getSex());
			
			Element tBirthday = new Element(Birthday);
			tBirthday.setText(tLCInsuredSchema.getBirthday());
			
			Element tIDType = new Element(IDType);
			tIDType.setText(tLCInsuredSchema.getIDType());
			
			Element tIDNo = new Element(IDNo);
			tIDNo.setText(tLCInsuredSchema.getIDNo());
			
			Element tOccupationCode = new Element("OccupationCode");
			tOccupationCode.setText(tLCInsuredSchema.getOccupationCode());
			
			Element tAddress = new Element(Address);
			tAddress.setText(tAddressDB.getPostalAddress());
			
			Element tZipCode = new Element(ZipCode);
			tZipCode.setText(tAddressDB.getZipCode());
			
			Element tMobile = new Element(Mobile);
			tMobile.setText(tAddressDB.getMobile());
			
			Element tPhone = new Element(Phone);
			tPhone.setText(tAddressDB.getPhone());
			
			Element tEmail = new Element(Email);
			tEmail.setText(tAddressDB.getEMail());
			
			Element tRelaToMain = new Element(RelaToMain);
			tRelaToMain.setText(tLCInsuredSchema.getRelationToMainInsured());
			
			Element tRelaToAppnt = new Element(RelaToAppnt);
			tRelaToAppnt.setText(tLCInsuredSchema.getRelationToAppnt());
			
			Element tRisks = getRisks(pContNo, tLCInsuredSchema.getInsuredNo());
			
			/*
			 * 被保人添加六段信息
			 * 
			 */
			
			Element GrpName = new Element("GrpName");                 //国家
			Element PostalProvince = new Element("PostalProvince");   //省
			Element PostalCity = new Element("PostalCity");  		  //市
			Element PostalCounty = new Element("PostalCounty");       //县
			Element PostalCommunity = new Element("PostalCommunity"); //详细地址
			try{
			  LCContDB LCCont = new LCContDB();
			  LCCont.setContNo(pContNo);
				
		      LCContSet lcs = LCCont.query() ;
			  if(lcs.size() ==0){
					throw new MidplatException("查询保单信息失败");
			   }
			   LCContSchema lc = lcs.get(1);
			   String agentCom = lc.getAgentCom();
			   String SQL = "";
			   ExeSQL exe = new ExeSQL();
		
			if(agentCom.startsWith("PY004")){
				try{
					
					String  code_str = tAddressDB.getGrpName();
					cLogger.info("国家编码::"+code_str);
					if( code_str != null && !code_str.equals(""))
					{
						SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+tAddressDB.getGrpName()+"'";
						SSRS srs = exe.execSQL(SQL);
						GrpName.setText(srs.GetText(1, 1));
					}
					
					code_str = tAddressDB.getPostalProvince();
					cLogger.info("省级编码::"+code_str);
					if( code_str != null && !code_str.equals("")){
					SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+tAddressDB.getPostalProvince()+"'";
					SSRS srs1 = exe.execSQL(SQL);
					PostalProvince.setText(srs1.GetText(1, 1));
					}
					
					
					code_str = tAddressDB.getPostalCity();
					cLogger.info("市级编码::"+code_str);
					if( code_str != null && !code_str.equals("")){
						SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+tAddressDB.getPostalCity()+"'";
						SSRS srs2 = exe.execSQL(SQL);
						PostalCity.setText(srs2.GetText(1, 1));
					}
					
					code_str = tAddressDB.getPostalCounty();
					cLogger.info("县级编码::"+code_str);
					if( code_str != null && !code_str.equals("")){
					SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+tAddressDB.getPostalCounty()+"'";
					SSRS srs3 = exe.execSQL(SQL);
					PostalCounty.setText(srs3.GetText(1, 1));
					}
					
//					SQL = "select code from ldcode where  codetype ='ybtaddress' and codename ='"+tAddressDB.getPostalCommunity()+"'";
//					SSRS srs4 = exe.execSQL(SQL);
//					PostalCommunity.setText(srs4.GetText(1, 1));
					PostalCommunity.setText(tAddressDB.getPostalCommunity());
				}
				catch(Exception e){
					throw new MidplatException("设置地址信息失败,请检查国家省市县详细地址在投保时时候录入!");
				}
			}
			}
			catch( Exception e){
				throw new MidplatException("查询保单信息失败!");
			}
			
			
			
			Element tLCInsured = new Element("LCInsured");
			tLCInsured.addContent(tAppntName);
			tLCInsured.addContent(tInsuredNo);
			tLCInsured.addContent(tSex);
			tLCInsured.addContent(tBirthday);
			tLCInsured.addContent(tIDType);
			tLCInsured.addContent(tIDNo);
			tLCInsured.addContent(tOccupationCode);
			tLCInsured.addContent(tRelaToAppnt);
			tLCInsured.addContent(tAddress);
			tLCInsured.addContent(tZipCode);
			tLCInsured.addContent(tMobile);
			tLCInsured.addContent(tPhone);
			tLCInsured.addContent(tEmail);
			tLCInsured.addContent(GrpName);   //被保人国家
			tLCInsured.addContent(PostalProvince);  //省
			tLCInsured.addContent(PostalCity);     //市
			tLCInsured.addContent(PostalCounty);   //县
			tLCInsured.addContent(PostalCommunity);//详细地址
			
			tLCInsured.addContent(tRisks);
			
			mLCInsureds.addContent(tLCInsured);
		}

		cLogger.info("Out YbtContQueryBL.getLCInsureds()!");
		return mLCInsureds;
	}
	
	private Element getRisks(String pContNo, String pInsuredNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getRisks()...");
		
		LCPolDB mLCPolDB = new LCPolDB();
		mLCPolDB.setContNo(pContNo);
		mLCPolDB.setInsuredNo(pInsuredNo);
		LCPolSet mLCPolSet = mLCPolDB.query();
		if ((null==mLCPolSet) || (mLCPolSet.size()<1)) {
      	cLogger.error(mLCPolDB.mErrors.getFirstError());
      	throw new MidplatException("查询险种信息失败！");
		}
		
		Element mRisks = new Element(Risks);
		double mInitFeeRate = 0.00;
		System.out.println(":::::::"+mLCPolSet.size());
		for (int i = 1; i <= mLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = (LCPolSchema) mLCPolSet.get(i);

			String tRiskCodeStr = tLCPolSchema.getRiskCode();
			
			System.out.println(tRiskCodeStr);
			
			if (tRiskCodeStr.equals("530701")	//附加健康人生个人意外伤害保险（B款）
					|| tRiskCodeStr.equals("530801")	//附加健康人生个人意外伤害保险(C款)
					|| tRiskCodeStr.equals("331401")	//附加健康专家个人护理保险
					|| tRiskCodeStr.equals("531001")	//附加健康人生个人意外伤害保险（D款）
					|| tRiskCodeStr.equals("531101")    //附加金利宝个人意外伤害保险
                    || tRiskCodeStr.equals("1607")
                    || tRiskCodeStr.equals("260301")
                    || tRiskCodeStr.equals("531301")) {	//附加康利相伴个人意外伤害保险 
				continue;
            }
				
			if(!tLCPolSchema.getAgentCom().startsWith("PY057")){	//江苏银行需要返回附险信息 测试：PY057 生产：PY026
				if(tRiskCodeStr.equals("340201")){//附加安心宝个人护理保险
					continue;
				}
			}
			Element mFeeRate = new Element("FeeRate");
			if (tRiskCodeStr.equals("333001")){
				mFeeRate.setText(String.valueOf(tLCPolSchema.getInitFeeRate()));
			}
            Element tLiaoNFlag = new Element("LiaoNFlag");
            if (tRiskCodeStr.equals("550806")) {
                // 辽宁套餐做特殊标志,ABC三款只能根据险种个数不一样去区分.
                if (mLCPolSet.size() == 3)
                    tLiaoNFlag.setText("WR0145");
                else if (mLCPolSet.size() == 2)
                    tLiaoNFlag.setText("XBT002");
                else
                    tLiaoNFlag.setText("XBT003");
            }
			System.out.println("险种个数标识");
			Element tRiskCode = new Element(RiskCode);
			tRiskCode.setText(tLCPolSchema.getRiskCode());
			
			Element tRiskName = new Element(RiskName);
			String mSQL = "select RiskName from lmrisk where riskcode='" + tLCPolSchema.getRiskCode() + "' with ur";
			tRiskName.setText(new ExeSQL().getOneValue(mSQL));
			
			Element tMainRiskCode = new Element(MainRiskCode);
//			tMainRiskCode.setText(tRiskCode.getText());	//注意：此处暂将主险代码置为与险种代码一致，以后遇到需返回多险种情况时，此处需修改。
			if("333001".equals(tLCPolSchema.getRiskCode()) || "532201".equals(tLCPolSchema.getRiskCode())){ //万能型H区分主副险
//				String mSQL2 = "select riskcode from lmriskapp where subriskflag = 'M' and riskcode = '"+ tLCPolSchema.getRiskCode() +"' with ur";
//				String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
//				tMainRiskCode.setText(mMainRiskCode);
				tMainRiskCode.setText("333001");
			}else if("333502".equals(tLCPolSchema.getRiskCode()) || "532502".equals(tLCPolSchema.getRiskCode()) || "532402".equals(tLCPolSchema.getRiskCode())){ //万能型H区分主副险
				tMainRiskCode.setText("333502");
			}else if("730101".equals(tLCPolSchema.getRiskCode()) || "332401".equals(tLCPolSchema.getRiskCode())){ //康利人生两全保险（分红型）
				//康利人生两全分红型需区分主险附加险
				String mSQL2 = "select riskcode from lcpol where contno='" + tLCPolSchema.getContNo() + "' and polno = mainpolno with ur";
				String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
				tMainRiskCode.setText(mMainRiskCode);
			}else if("231701".equals(tLCPolSchema.getRiskCode()) || "333801".equals(tLCPolSchema.getRiskCode())){ //康乐人生个人重大疾病保险（A款）
				tMainRiskCode.setText("231701");
			}else if("231401".equals(tLCPolSchema.getRiskCode())){ //附加豁免保费个人疾病保险
				tMainRiskCode.setText("231401");
			}else{
				//其余产品区分主附险
				String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '" + tLCPolSchema.getContNo() + "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
				String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
				tMainRiskCode.setText(mMainRiskCode);
			}
			
			Element tCValiDate = new Element(CValiDate);
			tCValiDate.setText(tLCPolSchema.getCValiDate());
			
			Element tAmnt = new Element(Amnt);
			tAmnt.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getAmnt()));
			
			Element tPrem = new Element(Prem);
			tPrem.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getPrem()));
			
			Element tSupplementaryPrem = new Element(SupplementaryPrem);
			tSupplementaryPrem.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getSupplementaryPrem()));
			
			Element tMult = new Element(Mult);
			tMult.setText(
					String.valueOf(tLCPolSchema.getMult()));

			Element tPayIntv = new Element(PayIntv);
			tPayIntv.setText(
					String.valueOf(tLCPolSchema.getPayIntv()));
			
			Element tInsuYearFlag = new Element(InsuYearFlag);
			tInsuYearFlag.setText(tLCPolSchema.getInsuYearFlag());

			Element tInsuYear = new Element(InsuYear);
			tInsuYear.setText(
					String.valueOf(tLCPolSchema.getInsuYear()));
			
			Element tYears = new Element(Years);
			tYears.setText(
					String.valueOf(tLCPolSchema.getYears()));
			
			Element tPayEndYearFlag = new Element(PayEndYearFlag);
			tPayEndYearFlag.setText(tLCPolSchema.getPayEndYearFlag());
			
			Element tPayEndYear = new Element(PayEndYear);
			tPayEndYear.setText(
					String.valueOf(tLCPolSchema.getPayEndYear()));
			
			Element tPayEndDate = new Element(PayEndDate);
			tPayEndDate.setText(tLCPolSchema.getPayEndDate());
			
			Element tPayYears = new Element(PayYears);
			tPayYears.setText(
					String.valueOf(tLCPolSchema.getPayYears()));
			
			Element tGetYearFlag = new Element(GetYearFlag);
			tGetYearFlag.setText(tLCPolSchema.getGetYearFlag());
			
			Element tGetYear = new Element(GetYear);
			tGetYear.setText(
					String.valueOf(tLCPolSchema.getGetYear()));

			Element tLCBnfs = getLCBnfs(tLCPolSchema.getPolNo());
			
			Element tCashValues = getCashValues(tLCPolSchema.getPolNo());
			
			Element tBonusValues = getBonusValues(tLCPolSchema.getPolNo());

			Element tRisk = new Element(Risk);
            tRisk.addContent(tLiaoNFlag);
			tRisk.addContent(tRiskCode);
			tRisk.addContent(tRiskName);
			tRisk.addContent(tMainRiskCode);
			tRisk.addContent(tCValiDate);
			tRisk.addContent(tAmnt);
			tRisk.addContent(tPrem);
			tRisk.addContent(tSupplementaryPrem);
			tRisk.addContent(tMult);
			tRisk.addContent(tPayIntv);
			tRisk.addContent(tInsuYearFlag);
			tRisk.addContent(tInsuYear);
			tRisk.addContent(tYears);
			tRisk.addContent(tPayEndYearFlag);
			tRisk.addContent(tPayEndYear);
			tRisk.addContent(tPayEndDate);
			tRisk.addContent(tPayYears);
			tRisk.addContent(tGetYearFlag);
			tRisk.addContent(tGetYear);
			if (tRiskCodeStr.equals("333001")){
				tRisk.addContent(mFeeRate);
			}
			tRisk.addContent(tLCBnfs);
			tRisk.addContent(tCashValues);
			tRisk.addContent(tBonusValues);

			mRisks.addContent(tRisk);
		}

		cLogger.info("Out YbtContQueryBL.getRisks()!");
		return mRisks;
	}
	
	private Element getLCBnfs(String pPolNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCBnfs()...");
		
		LCBnfDB mLCBnfDB = new LCBnfDB();
		mLCBnfDB.setPolNo(pPolNo);
		LCBnfSet mLCBnfSet = mLCBnfDB.query();
		if (mLCBnfDB.mErrors.needDealError()) {
      	cLogger.error(mLCBnfDB.mErrors.getFirstError());
      	throw new MidplatException("查询受益人信息失败！");
		}
		
		Element mLCBnfs = new Element(LCBnfs);
		for (int i = 1; i <= mLCBnfSet.size(); i++) {
			LCBnfSchema tLCBnfSchema = (LCBnfSchema) mLCBnfSet.get(i);
			
			Element tBnfType = new Element(BnfType);
			tBnfType.setText(tLCBnfSchema.getBnfType());
			
			Element tBnfNo = new Element(BnfNo);
			tBnfNo.setText(
					String.valueOf(tLCBnfSchema.getBnfNo()));
			
			Element tBnfGrade = new Element(BnfGrade);
			tBnfGrade.setText(tLCBnfSchema.getBnfGrade());
			
			Element tName = new Element(Name);
			tName.setText(tLCBnfSchema.getName());
			
			Element tSex = new Element(Sex);
			tSex.setText(tLCBnfSchema.getSex());
			
			Element tBirthday = new Element(Birthday);
			tBirthday.setText(tLCBnfSchema.getBirthday());
			
			Element tIDType = new Element(IDType);
			tIDType.setText(tLCBnfSchema.getIDType());
			
			Element tIDNo = new Element(IDNo);
			tIDNo.setText(tLCBnfSchema.getIDNo());

			Element tBnfLot = new Element(BnfLot);
			tBnfLot.setText(
					String.valueOf(
							(int) (tLCBnfSchema.getBnfLot()*100)));	//核心的受益比例为0.0-1.0，而标准报文是1-100，在此做转换
			
			Element tRelationToInsured = new Element(RelationToInsured);
			tRelationToInsured.setText(tLCBnfSchema.getRelationToInsured());
			
			Element tLCBnf = new Element(LCBnf);
			tLCBnf.addContent(tBnfType);
			tLCBnf.addContent(tBnfNo);
			tLCBnf.addContent(tBnfGrade);
			tLCBnf.addContent(tName);
			tLCBnf.addContent(tSex);
			tLCBnf.addContent(tIDType);
			tLCBnf.addContent(tIDNo);
			tLCBnf.addContent(tRelationToInsured);
			tLCBnf.addContent(tBnfLot);
			
			mLCBnfs.addContent(tLCBnf);
		}
		
		cLogger.info("Out YbtContQueryBL.getLCBnfs()!");
		return mLCBnfs;
	}
	
	private Element getCashValues(String pPolNo) throws MidplatException {
		Element mCashValues = new Element(CashValues);		
		Element mCashValueCount = new Element(CashValueCount);
	    mCashValues.addContent(mCashValueCount);   	

		String  mSQL = "select * from lcpol where polno='" + pPolNo + "' with ur";
		LCPolDB tLCPolDB = new LCPolDB();
		LCPolSet cLCPolSet = new LCPolSet();
		cLCPolSet = tLCPolDB.executeQuery(mSQL);

		int j = 0;		
		//现金价值个数
		int nCount = 0;   
		
		//循环处理险种下的现金价值信息
	    for (int i = 1; i <= cLCPolSet.size(); i++)
        {
            String tRiskCode = cLCPolSet.get(i).getRiskCode();

            //若是绑定型附加险，则不需要打印现金价值表
            if (CommonBL.isAppendRisk(tRiskCode))
            {
                continue;
            }

            //得到现金价值的算法描述
            LMCalModeDB tLMCalModeDB = new LMCalModeDB();
            //获取险种信息
            tLMCalModeDB.setRiskCode(tRiskCode);
            tLMCalModeDB.setType("X");
            LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

            //解析得到的SQL语句
            String strSQL = "";
            //如果描述记录唯一时处理
            if (tLMCalModeSet.size() == 1)
            {
                strSQL = tLMCalModeSet.get(1).getCalSQL();
            }
            // 这个险种不需要取现金价值的数据
            if (strSQL.equals(""))
            {
            }
            else
            {
                j = j + 1;

                Calculator calculator = new Calculator();
                //设置基本的计算参数
                calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
                        .getContNo());
                calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
                        .getInsuredNo());
                calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
                        .getInsuredSex());
                calculator.addBasicFactor("InsuredAppAge", String
                        .valueOf(cLCPolSet.get(i).getInsuredAppAge()));
                calculator.addBasicFactor("PayIntv", String.valueOf(cLCPolSet
                        .get(i).getPayIntv()));
                calculator.addBasicFactor("PayEndYear", String
                        .valueOf(cLCPolSet.get(i).getPayEndYear()));
                calculator.addBasicFactor("PayEndYearFlag", String
                        .valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
                calculator.addBasicFactor("PayYears", String.valueOf(cLCPolSet
                        .get(i).getPayYears()));
                calculator.addBasicFactor("InsuYear", String.valueOf(cLCPolSet
                        .get(i).getInsuYear()));
                calculator.addBasicFactor("Prem", String.valueOf(cLCPolSet.get(
                        i).getPrem()));
                calculator.addBasicFactor("Amnt", String.valueOf(cLCPolSet.get(
                        i).getAmnt()));
                calculator.addBasicFactor("FloatRate", String.valueOf(cLCPolSet
                        .get(i).getFloatRate()));
                calculator.addBasicFactor("Mult", String.valueOf(cLCPolSet.get(
                        i).getMult()));	                
                calculator.addBasicFactor("InsuYearFlag", String
                        .valueOf(cLCPolSet.get(i).getInsuYearFlag()));
                calculator.addBasicFactor("GetYear", String.valueOf(cLCPolSet
                        .get(i).getGetYear()));
                calculator.addBasicFactor("GetYearFlag", String
                        .valueOf(cLCPolSet.get(i).getGetYearFlag()));
                calculator.addBasicFactor("CValiDate", String.valueOf(cLCPolSet
                        .get(i).getCValiDate()));
                calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
                strSQL = calculator.getCalSQL();

                System.out.println(strSQL);
                //执行现金价值计算sql，获取现金价值列表
                Connection conn = DBConnPool.getConnection();
                if (null == conn)
                {
                    buildError("getConnection", "连接数据库失败！");
                    return  null;
                }
                Statement stmt = null;
                ResultSet rs = null;
                try
                {
                    stmt = conn.createStatement();
                    rs = stmt.executeQuery(strSQL);                    
                    while (rs.next())
                    {            
                    	Element CashValue = new Element("CashValue");
				    	Element tCashYear= new Element("End");
				    	tCashYear.addContent(rs.getString(1).trim());				    	
						Element tCashValue = new Element("Cash");
						tCashValue.addContent(format(rs.getDouble(3)));
						
						CashValue.addContent(tCashYear);
						CashValue.addContent(tCashValue);
						mCashValues.addContent(CashValue);
                        nCount++;
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                }
                catch (Exception ex)
                {
                    if (null != rs)
                    {
                        try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
                    }
                    try {
						stmt.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
                    try
                    {
                        conn.close();
                    }
                    catch (Exception e)
                    {
                    };
                    //出错处理
                    buildError("executeQuery", "数据库查询失败！");
                    return null;
                }
            }				
		}	    
	    mCashValueCount.addContent(nCount+"");
		return mCashValues;
	}
	
	private Element getBonusValues(String pPolNo) throws MidplatException {
		Element mBonusValues = new Element(BonusValues);
		
		Element mBonusValueCount = new Element(BonusValueCount);
		mBonusValueCount.setText("0");
		mBonusValues.addContent(mBonusValueCount);
		
		return mBonusValues;
	}

	/**
	 * 出错处理
	 * @param szFunc String
	 * @param szErrMsg String
	 */
	private void buildError(String szFunc, String szErrMsg)
	{
		CError cError = new CError();
		cError.moduleName = "LCContGetCashValueForYB";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
    /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }
    
    
    
}
