package com.sinosoft.midplat.kernel.service.writeOff;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.ContCancel;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class YbtWriteOffBL {
	private final static Logger cLogger = Logger.getLogger(YbtWriteOffBL.class);
	
	private final GlobalInput cGlobalInput;
	private final LCContSchema cLCContSchema;
	
	public YbtWriteOffBL(LCContSchema pLCContSchema, GlobalInput pGlobalInput) {
		cGlobalInput = pGlobalInput;
		cLCContSchema = pLCContSchema;
	}
	
	/**
	 * 查询保单合同数据失败，将会抛出MidplatException
	 */
	public YbtWriteOffBL(String pContNo, GlobalInput pGlobalInput) throws MidplatException {
		cGlobalInput = pGlobalInput;
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(pContNo);
		if (!mLCContDB.getInfo()) {
			throw new MidplatException("查询保单数据(LCCont)失败!");
		}
		cLCContSchema = mLCContDB.getSchema();
	}
	
	public void deal() throws Exception {
		cLogger.info("Into YbtWriteOffBL.deal()...");

		String mContNo = cLCContSchema.getContNo();
		String mEdorNo = "YBT" + mContNo;
		String mPrtNo = cLCContSchema.getPrtNo();
		String mProposalContNo = cLCContSchema.getProposalContNo();
		String mCurDate = DateUtil.getCur10Date();
		String mCurTime = DateUtil.getCur8Time();

		String mLimit = PubFun.getNoLimit(cGlobalInput.ManageCom); // 产生通知书号即暂交费收据号
		String mSQL = "select max(tempfeeno) from ljtempfee where tempfeeno like '" + mPrtNo + "%' with ur";
		String mMaxOldTempFeeNo = new ExeSQL().getOneValue(mSQL);
		String mSuffix = mMaxOldTempFeeNo.substring(mMaxOldTempFeeNo.length() - 3);
		String mNewTempFeeNo = mPrtNo + (Integer.parseInt(mSuffix)+1); // 暂收费号+1

		mSQL = "select tempfeeno from ljtempfee where OtherNo='"+ mContNo + "' fetch first 1 rows only with ur";
		String mOldTempFeeNo = new ExeSQL().getOneValue(mSQL);
		if (mOldTempFeeNo.equals("")) {
			throw new MidplatException("查询原暂收数据(LJTempFee)失败！");
		}

		LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
		mLJTempFeeDB.setTempFeeNo(mOldTempFeeNo);
		mLJTempFeeDB.setOtherNo(mContNo);
		LJTempFeeSet mOldLJTempFeeSet = mLJTempFeeDB.query();
		if (mOldLJTempFeeSet.size() < 1) {
			cLogger.error(mLJTempFeeDB.mErrors.getFirstError());
			throw new MidplatException("查询原暂收数据(LJTempFee)失败！");
		}
		LJTempFeeSet mNewLJTempFeeSet = new LJTempFeeSet();
		for (int i = 1; i <= mOldLJTempFeeSet.size(); i++) {
			LJTempFeeSchema tLJTempFeeSchema = mOldLJTempFeeSet.get(i);				
			tLJTempFeeSchema.setTempFeeNo(mNewTempFeeNo);
			tLJTempFeeSchema.setPayMoney(-Math.abs(tLJTempFeeSchema.getPayMoney()));
			tLJTempFeeSchema.setConfDate(mCurDate);	//签单日期
//			tLJTempFeeSchema.setConfFlag("1");
			tLJTempFeeSchema.setEnterAccDate(mCurDate);	//到账日期
			tLJTempFeeSchema.setConfMakeDate(mCurDate);	//到账确认日期
			tLJTempFeeSchema.setMakeDate(mCurDate);
			tLJTempFeeSchema.setMakeTime(mCurTime);
			tLJTempFeeSchema.setModifyDate(mCurDate);
			tLJTempFeeSchema.setModifyTime(mCurTime);

			mNewLJTempFeeSet.add(tLJTempFeeSchema);
		}			

		LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
		mLJTempFeeClassDB.setTempFeeNo(mOldTempFeeNo);			
		LJTempFeeClassSet mOldLJTempFeeClassSet = mLJTempFeeClassDB.query();
		if (mOldLJTempFeeClassSet.size() < 1) {
			cLogger.error(mLJTempFeeClassDB.mErrors.getFirstError());
			throw new MidplatException("查询原暂收数据(LJTempFeeClass)失败！");
		}
		LJTempFeeClassSet mNewLJTempFeeClassSet = new LJTempFeeClassSet();
		for (int i = 1; i <= mOldLJTempFeeClassSet.size(); i++) {
			LJTempFeeClassSchema tLJTempFeeClassSchema = mOldLJTempFeeClassSet.get(i);
			tLJTempFeeClassSchema.setTempFeeNo(mNewTempFeeNo);
			tLJTempFeeClassSchema.setPayMoney(-Math.abs(tLJTempFeeClassSchema.getPayMoney()));
			tLJTempFeeClassSchema.setConfDate(mCurDate);	//签单日期
//			tLJTempFeeClassSchema.setConfFlag("1");
			tLJTempFeeClassSchema.setEnterAccDate(mCurDate);	//到账日期
			tLJTempFeeClassSchema.setConfMakeDate(mCurDate);	//到账确认日期
			tLJTempFeeClassSchema.setMakeDate(mCurDate);
			tLJTempFeeClassSchema.setMakeTime(mCurTime);
			tLJTempFeeClassSchema.setModifyDate(mCurDate);
			tLJTempFeeClassSchema.setModifyTime(mCurTime);

			mNewLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		}

		String mNewPayNo = "";
		LJAPayDB mLJAPayDB = new LJAPayDB();
		mLJAPayDB.setIncomeNo(mContNo);
		mLJAPayDB.setOperator("ybt");
		LJAPaySet mOldLJAPaySet = mLJAPayDB.query();
		if (mOldLJAPaySet.size() < 1) {
			cLogger.error(mLJAPayDB.mErrors.getFirstError());
			throw new MidplatException("查询原实收数据(LJAPay)失败！");
		}
		LJAPaySet mNewLJAPaySet = new LJAPaySet();
		for (int i = 1; i <= mOldLJAPaySet.size(); i++) {
			mNewPayNo = PubFun1.CreateMaxNo("PayNo", mLimit);
			LJAPaySchema tLJAPaySchema = mOldLJAPaySet.get(i);
			tLJAPaySchema.setSumActuPayMoney(-tLJAPaySchema.getSumActuPayMoney());
			tLJAPaySchema.setPayNo(mNewPayNo);
			tLJAPaySchema.setGetNoticeNo(mNewTempFeeNo);
			tLJAPaySchema.setOperator("-" + tLJAPaySchema.getOperator());
//			tLJAPaySchema.setPayTypeFlag("9");
			tLJAPaySchema.setConfDate(mCurDate);
			tLJAPaySchema.setEnterAccDate(mCurDate);
			tLJAPaySchema.setMakeDate(mCurDate);
			tLJAPaySchema.setMakeTime(mCurTime);
			tLJAPaySchema.setModifyDate(mCurDate);
			tLJAPaySchema.setModifyTime(mCurTime);

			mNewLJAPaySet.add(tLJAPaySchema);
		}			

		LJAPayPersonDB mLJAPayPersonDB = new LJAPayPersonDB();
		mLJAPayPersonDB.setContNo(mContNo);
		mLJAPayPersonDB.setOperator("ybt");
		LJAPayPersonSet mOldLJAPayPersonSet = mLJAPayPersonDB.query();
		if (mOldLJAPaySet.size() < 1) {
			cLogger.error(mLJAPayPersonDB.mErrors.getFirstError());
			throw new MidplatException("查询原实收数据(LJAPayPerson)失败！");
		}
		LJAPayPersonSet mNewLJAPayPersonSet = new LJAPayPersonSet();
		for (int i = 1; i <= mOldLJAPayPersonSet.size(); i++) {
			LJAPayPersonSchema tLJAPayPersonSchema = mOldLJAPayPersonSet.get(i);
			tLJAPayPersonSchema.setSumActuPayMoney(-tLJAPayPersonSchema.getSumActuPayMoney());
			tLJAPayPersonSchema.setSumDuePayMoney(-tLJAPayPersonSchema.getSumDuePayMoney());
			tLJAPayPersonSchema.setPayNo(mNewPayNo);
			tLJAPayPersonSchema.setGetNoticeNo(mNewTempFeeNo);
//			tLJAPayPersonSchema.setPayTypeFlag("9");
			tLJAPayPersonSchema.setOperator("-" + tLJAPayPersonSchema.getOperator());
			tLJAPayPersonSchema.setEnterAccDate(mCurDate);
			tLJAPayPersonSchema.setConfDate(mCurDate);
			tLJAPayPersonSchema.setMakeDate(mCurDate);
			tLJAPayPersonSchema.setMakeTime(mCurTime);
			tLJAPayPersonSchema.setModifyDate(mCurDate);
			tLJAPayPersonSchema.setModifyTime(mCurTime);
			if(tLJAPayPersonSchema.getMoneyNoTax() != null && !"".equals(tLJAPayPersonSchema.getMoneyNoTax())){
				tLJAPayPersonSchema.setMoneyNoTax("-"+tLJAPayPersonSchema.getMoneyNoTax());
			}
			if(tLJAPayPersonSchema.getMoneyTax() != null && !"".equals(tLJAPayPersonSchema.getMoneyTax())){
				tLJAPayPersonSchema.setMoneyTax("-"+tLJAPayPersonSchema.getMoneyTax());
			}
			tLJAPayPersonSchema.setBusiType(tLJAPayPersonSchema.getBusiType());
			tLJAPayPersonSchema.setTaxRate(tLJAPayPersonSchema.getTaxRate());

			mNewLJAPayPersonSet.add(tLJAPayPersonSchema);
		}


		MMap mSubmitMMap = new ContCancel().prepareContData(mContNo, mEdorNo); //调用保全提供的接口,对关联的业务表进行插入删除操作.
		mSubmitMMap.put(mNewLJTempFeeSet, "INSERT");
		mSubmitMMap.put(mNewLJTempFeeClassSet, "INSERT");
		mSubmitMMap.put(mNewLJAPaySet, "INSERT");
		mSubmitMMap.put(mNewLJAPayPersonSet, "INSERT");

		mSubmitMMap.put("delete from LCContReceive where contno = '" + mContNo + "'", "DELETE");
		mSubmitMMap.put("delete from LCContGetPol where contno = '" + mContNo + "'",	"DELETE");
		mSubmitMMap.put("delete from LZSysCertify where CertifyNo = '" + mContNo + "'",	"DELETE");
		mSubmitMMap.put("delete from LWMission where MissionProp1='" + mContNo + "'", "DELETE");
		mSubmitMMap.put("delete from LCRiskDutyWrap where contno='" + mProposalContNo + "'", "DELETE"); // 删除套餐信息

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交数据失败！");
		}
		
		//修改B表状态
		String bSQL = null;
		bSQL = "select stateflag from lbcont where contno = '" + mContNo + "' with ur ";
		String stateFlag = new ExeSQL().getOneValue(mSQL); //获取B表状态
		if(stateFlag != null && !"3".equals(stateFlag)){
			String update_sql = "update lbcont set stateflag = '3' where contno = '" + mContNo + "' with ur";
			new ExeSQL().execUpdateSQL(update_sql);
		}
		cLogger.info("修改lbcont状态成功！");
		bSQL = "select stateflag from lbpol where contno = '" + mContNo + "' with ur ";
		String stateFlag2 = new ExeSQL().getOneValue(mSQL); //获取B表状态
		if(stateFlag2 != null && !"3".equals(stateFlag2)){
			String update_sql = "update lbpol set stateflag = '3' where contno = '" + mContNo + "' with ur";
			new ExeSQL().execUpdateSQL(update_sql);
		}
		cLogger.info("修改lbpol状态成功！");
		
		insertLCContState(mContNo);
		
		//作废原单证！
		String mCertifyCode = null;
		//作废单证时使用,信保通需要去掉头两位
		String mProposalContNoNew = null;
		if ("a".equals(cLCContSchema.getCardFlag())) {
			mSQL = "select RiskCode from LKTransStatus where polno = '"
					+ mContNo + "' "
					+ "and BankCode = '" + cLCContSchema.getBankCode().substring(0,2) + "' "
					+ "and FuncFlag = '01' and Status = '1' with ur";
			String mRiskWrapCode = new ExeSQL().getOneValue(mSQL); //信保通存储的是套餐编码
			// 查询得到单证编码
			ExeSQL mExeSQL = new ExeSQL();
			
			//针对江门邮政地区管理机构特殊处理 add by zengzm 2016-08-25
			String mManagecom = "";
			if("65".equals(cLCContSchema.getBankCode())){
				mManagecom = cGlobalInput.ManageCom;
			}else{
				mManagecom = cGlobalInput.ManageCom.substring(0, 4);
			}
			
			String tSQL = "select certifycode from lkcertifymapping where bankcode = '"
					+ cLCContSchema.getBankCode().substring(0,2) + "' "
					+ "and managecom = '" + mManagecom + "' "
					+ "and RiskWrapCode = '" + mRiskWrapCode + "' " 
					+ "with ur";

			SSRS tSSRS = mExeSQL.execSQL(tSQL);
			if (tSSRS.getMaxRow() < 1) {
				throw new MidplatException("未查询到对应的单证编码!");
			}
			mCertifyCode = tSSRS.GetText(1, 1);
			if(cGlobalInput.ManageCom.startsWith("8635") && cGlobalInput.AgentCom.startsWith("PY053")){
				mProposalContNoNew = mProposalContNo;  //泉州银行传单证直接传
			}
			else  if(cGlobalInput.ManageCom.startsWith("8644") && cGlobalInput.AgentCom.startsWith("PY066")){
				mProposalContNoNew = mProposalContNo.substring(2, 13);  // 针对广州农商行 取11为
			}
			else  if(cGlobalInput.AgentCom.startsWith("PC13")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对河北农信社10位单证 add by zengzm 2015-12-17
			}
			else  if(cGlobalInput.AgentCom.startsWith("PC41")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对河南农信社10位单证 add by zengzm 2017-01-20
			}
			else  if(cLCContSchema.getBankCode().startsWith("60")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对江西单证  这么加就目前情况和下面统一处理一样效果，故注释了
			}
			else  if(cGlobalInput.AgentCom.startsWith("PC43")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对湖南农信社10位单证 add by zengzm 2016-07-26
			}
			else if (cGlobalInput.AgentCom.startsWith("PC21")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对辽宁农信社截取后10位单证号  add by 2016-01-28
			}
			else if (cLCContSchema.getBankCode().startsWith("78")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对陕西农信社截取后9位单证号  add by GaoJinfu 20180517
			}
			else if (cLCContSchema.getBankCode().startsWith("80")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对临商新单证ALW开头  add by gcy 20180919
			}
			else if (cLCContSchema.getBankCode().startsWith("81")){
				mProposalContNoNew = mProposalContNo.substring(3);  //针对湖北农信社新单证AMM开头  add by gcy 20181016
			}
			else{
				mProposalContNoNew = mProposalContNo.substring(2, 12);  //信保通作废单证需要把前两位大写字母去掉,取10位
			}
		} else if ("9".equals(cLCContSchema.getCardFlag())||    //此处需要增加网银,atm出单渠道标示,后期有其他渠道一样需要在这里添加
				"c".equals(cLCContSchema.getCardFlag()) || 
				"d".equals(cLCContSchema.getCardFlag()) || 
				"f".equals(cLCContSchema.getCardFlag()) ||
				"e".equals(cLCContSchema.getCardFlag())){//针对手机银行特殊处理 add by zengzm 2016-11-23
			mCertifyCode = "HB07/07B";
			mProposalContNoNew = mProposalContNo;
		}
		CardManage mCardManage = new CardManage(mCertifyCode, cGlobalInput);
		//add by zengzm 针对江门新一批上线AH打头
		if((mProposalContNo.startsWith("AD") || mProposalContNo.startsWith("AH")) && mProposalContNo.equals(mPrtNo)){
			cLogger.info("江门邮件渠道不进行单证核销!");
		}else if(mProposalContNoNew.startsWith("56") && mProposalContNoNew.equals(mPrtNo)){    //判断出网银渠道
			cLogger.info("网银渠道不需要进行单证核销!!");
		}else {
			if (!mProposalContNo.startsWith("YBT")) {
				mCardManage.invalidated(mProposalContNoNew);
			} 
		}

		cLogger.info("Out YbtWriteOffBL.deal()!");
	}
	
	private int insertLCContState(String  contno) throws MidplatException {
		cLogger.info("Into YbtWriteOffBL.insertLCContState()!");
		int flag = 0;
		String rSQL2 = "select 1 from lccont where contno ='"+ contno + "'  with ur";
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(rSQL2);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
		
			String lctssql = "Select 1 from lccontstate where contno = '" + contno + "' with ur";
			SSRS lctSSRS = new SSRS();
			lctSSRS =  new ExeSQL().execSQL(lctssql);
			if (lctSSRS == null || lctSSRS.getMaxRow() <= 0) {				
			
				LCContStateDB lcContStateDB = null;
				String polSQL = "select insuredno,polno,edorno,makedate,maketime from lbpol where contno = '" + contno + "' with ur";
				SSRS contSSRS =  new ExeSQL().execSQL(polSQL);
				if(contSSRS == null){
					throw new MidplatException("未获取到保单信息！");
				}
				lcContStateDB = new LCContStateDB();
				String insuredno = contSSRS.GetText(1, 1);
				String polno = contSSRS.GetText(1, 2);
				String edorno = contSSRS.GetText(1, 3);
				String makedate = contSSRS.GetText(1, 4);
				String maketime = contSSRS.GetText(1, 5);
				lcContStateDB.setGrpContNo("000000");
				lcContStateDB.setContNo(contno);
				lcContStateDB.setInsuredNo(insuredno);
				lcContStateDB.setPolNo("000000");
				lcContStateDB.setStateType("Terminate");
				lcContStateDB.setOtherNoType("CD");
				lcContStateDB.setOtherNo(edorno);
				lcContStateDB.setState("1");
				lcContStateDB.setStateReason("CD");
				lcContStateDB.setStartDate(makedate);
				lcContStateDB.setEndDate("9999-12-31");
				lcContStateDB.setOperator("000");
				lcContStateDB.setMakeDate(makedate);
				lcContStateDB.setMakeTime(maketime);
				lcContStateDB.setModifyDate(DateUtil.getCur10Date());
				lcContStateDB.setModifyTime(DateUtil.getCur8Time());
				if(!lcContStateDB.insert()){
					flag = 1;
					throw new MidplatException("插入lccontstate失败！");
				}
			}
		}
		
		cLogger.info("Out YbtWriteOffBL.insertLCContState()!");
		return flag;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mContNo = "000927127000001";
		
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.AgentCom = "PY004024";
		mGlobalInput.ManageCom = "86340000";
		mGlobalInput.ComCode = "86340000";
		mGlobalInput.Operator = "ybt";
		
		YbtWriteOffBL mYbtWriteOffBL = new YbtWriteOffBL(mContNo, mGlobalInput);
		mYbtWriteOffBL.deal();

		System.out.println("成功结束！");
	}
}
