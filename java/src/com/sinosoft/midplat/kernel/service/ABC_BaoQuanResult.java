package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ABC_BaoQuanResult extends ServiceImpl  {

	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	
	public ABC_BaoQuanResult(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	@SuppressWarnings("unused")
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_BaoQuanResult.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		LKTransStatusDB mLKTransStatusDB = null;	

		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			//业务处理
			if("59".equals(mBaseInfo.getChildText("FunctionFlag"))){ //犹豫期退保
				mOutXmlDoc = deal();
			}	
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");//执行任务完毕记
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ABC_BaoQuanResult.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal() throws MidplatException {
		cLogger.info("Into ABC_BaoQuanResult.deal()...");
			
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mChkDetails = new Element("ChkDetails");
		String mTransamnt = "";
		
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		mFlag.setText("0");
		mDesc.setText("交易成功！");
 
		ExeSQL mExeSQL = new ExeSQL();
		
		//获取保全处理成功 及 当晚保全对账失败的保单信息  --故农行应先对账再返回结果  
		//同时要区分银行 联合查询日志表确定保单所属银行
		//对应于工行犹豫期也需要做调整
		String mSql = "select distinct(a.Contno),a.Makedate,a.Modifydate,a.appntname,a.appntidtype,a.appntidno,"
				+ "a.Appstate,a.edortype,a.transstatus,a.edorno from db2inst1.LPYBTAppWT a,lktransstatus b "
				+ "where a.Operator = 'ybt'and a.appstate = '2'and a.transstatus = '2' and "
				+ "a.Modifydate = Current Date and a.contno = b.polno and b.funcflag = '121'and b.bankcode = '04' "
				+ "union "
				+ "select distinct(a.Contno),a.Makedate,a.Modifydate,a.appntname,a.appntidtype,a.appntidno,"
				+ "a.Appstate,a.edortype,a.transstatus,a.edorno from db2inst1.LPYBTAppWT a,lktransstatus b "
				+ "where a.Operator = 'ybt'and a.appstate = '1'and a.Modifydate = Current Date "
				+ "and a.contno = b.polno and b.funcflag = '121'and b.bankcode = '04'and a.transstatus = '3'"
				+ "with ur";
		
		String mNoRealTimeSql = "select c.contno,c.makedate,b.confdate,c.AppntName,c.AppntIdType,c.AppntIdNo,(select 2 from dual),a.edortype,(select 2 from dual),a.edorno,a.getmoney " +
				"from lpedoritem a,lpedorapp b,lbcont c where a.edorno=b.edoracceptno " +
				"and a.edorno=c.edorno and a.contno=c.contno and a.edortype in('WT','CT') and b.edorstate='0' " +
				"and c.conttype='1' and c.cardflag='9' and c.salechnl = '04' and c.agentcom like 'PY005%' " +
				"and b.confdate=Current Date " +
//				"and b.confdate between '2015-04-20' and '2015-04-23' " +
				"with ur";

		SSRS mSSRS = mExeSQL.execSQL(mSql);
		System.out.println("申请实时保全保单数：" + mSSRS.MaxRow);

		SSRS mNoRealTimeSSRS = mExeSQL.execSQL(mNoRealTimeSql);
		System.out.println("非银保通渠道保全保单数：" + mNoRealTimeSSRS.MaxRow);
		
		if(mSSRS.MaxRow >= 1 || mNoRealTimeSSRS.MaxRow >= 1){
			for(int i = 1 ; i <= mSSRS.MaxRow ;i++){
				String mContno = mSSRS.GetText(i, 1);
//				String mMakedate = mSSRS.GetText(i, 2);
				String mModifydate = mSSRS.GetText(i, 3);
				String mAppntName = mSSRS.GetText(i, 4);
				String mAppntIdType = mSSRS.GetText(i, 5);
				String mAppntIdNo = mSSRS.GetText(i, 6);
				String mAppstate = mSSRS.GetText(i, 7);
				String mEdorType = mSSRS.GetText(i, 8);
				String mTransStatus = mSSRS.GetText(i, 9);
				String mEdorno = mSSRS.GetText(i, 10);
				
				
				Element tEdorType = new Element("EdorType");//业务种类
				tEdorType.setText(mEdorType);
				
				Element tTranDate = new Element(TranDate);	//交易日期
				tTranDate.setText(mModifydate);
				
				Element tContNo = new Element("ContNo");
				tContNo.setText(mContno);
				
				Element tAppntName = new Element("AppntName");
//				tAppntName.setText(mAppntName);
				tAppntName.setText("");
				
				Element tAppntIdType = new Element("AppntIdType");
				tAppntIdType.setText(mAppntIdType);
				
				Element tAppntIdNo = new Element("AppntIdNo");
				tAppntIdNo.setText(mAppntIdNo);
				
				Element tTransStatus = new Element("TransStatus");	//对账状态
				tTransStatus.setText(mTransStatus);
				
				Element tAppstate = new Element("Appstate");	//对账状态
				tAppstate.setText(mAppstate);
			
				double mSumPrem = 0;	//退保金额 即 领取金额
				if(mTransStatus.equals("3")){	//对账失败的返回保单信息
					String tSql = "select Transno,Transamnt from lktransstatus where bankcode = '04' and funcflag = '56'  and  polno = '" + mContno + "' and makedate = '"+ mCurrentDate +"' and descr is null with ur";
					SSRS tSSRS = mExeSQL.execSQL(tSql);
					if(tSSRS.MaxRow >= 1){
						mTransamnt = tSSRS.GetText(1, 2);
						if(mEdorType.equals("WT")){	//犹豫期返回退保金额
							mSumPrem = Double.parseDouble(mTransamnt);
						}else if(mEdorType.equals("CT")){	//解约
							PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
							mSumPrem = mPedorYBTBudgetBL.getZTmoney(mContno,mModifydate);
						}else{
							cLogger.warn("保单保全标志异常！");
						}
					}else{
						cLogger.warn("日志表中不存在该保单！");
					}	
				}
				
				if(mTransStatus.equals("2") && mAppstate.equals("2")){	//对账及保全处理成功保单
					if(mEdorType.equals("WT")){	//犹豫期返回退保金额
						LBPolDB pol_db = new LBPolDB(); 
						pol_db.setContNo(mContno);
						LBPolSet polSet = pol_db.query();
						for (int j = 1; j <= polSet.size(); j++) {
							LBPolSchema mPol = polSet.get(j);
							mSumPrem += mPol.getPrem();
						}
					}else if(mEdorType.equals("CT")){	//解约
						String tSql = "select -sum(getmoney) from ljagetendorse where endorsementno='" + mEdorno + "' with ur"; 
						String tSumPrem = new ExeSQL().getOneValue(tSql);
						mSumPrem = Double.parseDouble(tSumPrem);
					}else{
						cLogger.warn("保单保全标志异常！");
					}
				}
				Element tTranAmnt = new Element(TranAmnt);
				tTranAmnt.setText(new DecimalFormat("0.00").format(mSumPrem));
				
				Element mChkDetail =  new Element("ChkDetail");
				
				mChkDetail.addContent(tEdorType);
				mChkDetail.addContent(tTranDate);
				mChkDetail.addContent(tContNo);
				mChkDetail.addContent(tAppntName);
				mChkDetail.addContent(tAppntIdType);
				mChkDetail.addContent(tAppntIdNo);
				mChkDetail.addContent(tTransStatus);
				mChkDetail.addContent(tAppstate);
				mChkDetail.addContent(tTranAmnt);
				
				mChkDetails.addContent(mChkDetail);
			}
			if(mNoRealTimeSSRS.MaxRow >= 1){	//非银保通犹退、解约数据
				mChkDetails = dealNoRealTime(mNoRealTimeSSRS,mChkDetails);
			}
		}else {
			mFlag.setText("1");
			mDesc.setText("不存在需要返回的犹豫期保单信息！");
		}
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mChkDetails);
		
		cLogger.info("Out ABC_BaoQuanResult.deal()...");
		return new Document(mTranData);
	}
	
	public Element dealNoRealTime(SSRS mNoRealTimeSSRS,Element mChkDetails) {
		cLogger.info("Into ABC_BaoQuanResult.dealNoRealTime()...");
		
		for(int i = 1 ; i <= mNoRealTimeSSRS.MaxRow ;i++){
			String mContno = mNoRealTimeSSRS.GetText(i, 1);
//			String mMakedate = mNoRealTimeSSRS.GetText(i, 2);
			String mModifydate = mNoRealTimeSSRS.GetText(i, 3);
			String mAppntName = mNoRealTimeSSRS.GetText(i, 4);
			String mAppntIdType = mNoRealTimeSSRS.GetText(i, 5);
			String mAppntIdNo = mNoRealTimeSSRS.GetText(i, 6);
			String mAppstate = mNoRealTimeSSRS.GetText(i, 7);
			String mEdorType = mNoRealTimeSSRS.GetText(i, 8);
			String mTransStatus = mNoRealTimeSSRS.GetText(i, 9);
			String mEdorno = mNoRealTimeSSRS.GetText(i, 10);
			String mGetmoney = mNoRealTimeSSRS.GetText(i, 11);
			
			Element tEdorType = new Element("EdorType");//业务种类
			tEdorType.setText(mEdorType);
			
			Element tTranDate = new Element(TranDate);	//交易日期
			tTranDate.setText(mModifydate);
			
			Element tContNo = new Element("ContNo");
			tContNo.setText(mContno);
			
			Element tAppntName = new Element("AppntName");
//			tAppntName.setText(mAppntName);   
			tAppntName.setText("");
			
			Element tAppntIdType = new Element("AppntIdType");
			tAppntIdType.setText(mAppntIdType);
			
			Element tAppntIdNo = new Element("AppntIdNo");
			tAppntIdNo.setText(mAppntIdNo);
			
			Element tTransStatus = new Element("TransStatus");	//对账状态
			tTransStatus.setText(mTransStatus);
			
			Element tAppstate = new Element("Appstate");	//对账状态
			tAppstate.setText(mAppstate);
			
			Element tEdorno = new Element("Edorno");	//对账状态
			tEdorno.setText(mEdorno);

			//犹退金额，解约金额
			Element tTranAmnt = new Element(TranAmnt);
			mGetmoney = String.valueOf(Math.abs(Double.parseDouble(mGetmoney)));//求金额绝对值
			tTranAmnt.setText(mGetmoney);
			
			Element mChkDetail =  new Element("ChkDetail");
			
			mChkDetail.addContent(tEdorType);
			mChkDetail.addContent(tTranDate);
			mChkDetail.addContent(tContNo);
			mChkDetail.addContent(tAppntName);
			mChkDetail.addContent(tAppntIdType);
			mChkDetail.addContent(tAppntIdNo);
			mChkDetail.addContent(tTransStatus);
			mChkDetail.addContent(tAppstate);
			mChkDetail.addContent(tTranAmnt);
			mChkDetail.addContent(tEdorno);
			
			mChkDetails.addContent(mChkDetail);
		}
		
		cLogger.info("Out ABC_BaoQuanResult.dealNoRealTime()...");
		return mChkDetails;
		
	}
	
	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		cLogger.info("Into ABC_BaoQuanResult.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out ABC_BaoQuanResult.insertTransLog()...");
		return mLKTransStatusDB;
	}

}
