package com.sinosoft.midplat.kernel.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LMCalModeSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.NumberUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class CCB_QueryChangeInfo extends ServiceImpl  {

	public CCB_QueryChangeInfo(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	
	private ExeSQL pExeSQL = new ExeSQL();
	private String pSQL = "";
	private String strSQL="";
	private SSRS pSSRS= new SSRS();
	private String mStartDate;
	private String mEndDate;
	private String mRiskCode;
	private String mContNo;
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_QueryChangeInfo.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
			mLKTransStatusDB.getBankCode(),
			mLKTransStatusDB.getBankBranch(),
			mLKTransStatusDB.getBankNode());
			
			mOutXmlDoc = deal(mLCCont);

			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_QueryChangeInfo.service()!");
		return mOutXmlDoc;
	}
	
	
	public Document deal(Element pLCCont) throws MidplatException {
		cLogger.info("Into CCB_QueryChangeInfo.deal()...");
		
		Document nOutXmlDoc = new Document(new Element(TranData));
		
		Element mTranData = nOutXmlDoc.getRootElement();
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mLCCont = new Element("LCCont");
		
		mRiskCode = pLCCont.getChildText("RiskCode");
		mContNo = pLCCont.getChildText("ContNo");
		
		mStartDate = DateUtil.formatTrans(pLCCont.getChildText("StartDate"), "yyyyMMdd", "yyyy-MM-dd");
		mEndDate = DateUtil.formatTrans(pLCCont.getChildText("EndDate"), "yyyyMMdd", "yyyy-MM-dd");
		
		if(mRiskCode ==null && "".equals(mRiskCode)){
			throw new MidplatException("险种代码不能为空！");
		}
		
		String cx_sql = "select 1 from lccont where contno = '" + mContNo + "' with ur";
		if(pExeSQL.getOneValue(cx_sql).equals("1")){
			//续期信息1
			Element nRenewalPayInfo =  getRenewalPayInfo();
			//分红信息
			Element nCaseBonusInfo = getCaseBonusInfo();
			//保全信息
			Element mEdorInfo = getEdorInfo();
			//保单生效复信息1
			Element mEdorStatusInfo = getEdorStatusInfo();
			//保单保额变动信息
			Element mLCContAmntInfo = getLCContAmntInfo();
			//理赔信息
			Element mClaimInfo = getClaimInfo();
			
			mLCCont.addContent(nRenewalPayInfo);
			mLCCont.addContent(nCaseBonusInfo);
			mLCCont.addContent(mEdorInfo);
			mLCCont.addContent(mEdorStatusInfo);
			mLCCont.addContent(mLCContAmntInfo);
			mLCCont.addContent(mClaimInfo);
		}else {
			throw new MidplatException("未查到保单信息！");
		}
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);

		cLogger.info("Out CCB_QueryChangeInfo.deal()...");
		return nOutXmlDoc;
		
	}
	
	
	public Element getRenewalPayInfo() {
		cLogger.info("Into CCB_QueryChangeInfo.getRenewalPayInfo()...");
		
		strSQL="' and a.riskcode= '"+mRiskCode+"";

		Element mRenewalPayInfo = new Element("RenewalPayInfo");
		Element mRenewalPaySum = new Element("RenewalPaySum");
		pSQL = "select count(distinct payno)  from ljapayperson a where a.contno='"
				+ mContNo + "' and riskcode = '" + mRiskCode + "' and exists (select 1 from ljspayb where getnoticeno=a.getnoticeno and dealstate='1') and a.confdate between '"
				+ mStartDate + "' and '" + mEndDate + "' with ur";
		mRenewalPaySum.setText(pExeSQL.getOneValue(pSQL));
		
		mRenewalPayInfo.addContent(mRenewalPaySum);
		
		if(!pExeSQL.getOneValue(pSQL).equals("0")){
			String xq_sql = "select a.confdate,sum(a.sumactupaymoney) from ljapayperson a where a.contno='"
				+ mContNo + "' and riskcode = '" + mRiskCode + "' and exists (select 1 from ljspayb where getnoticeno=a.getnoticeno and dealstate='1') and a.confdate between '"
				+ mStartDate
				+ "' and '"
				+ mEndDate
				+ "' group by confdate with ur";
		
		pSSRS = pExeSQL.execSQL(xq_sql);
		Element mRenewalPay_List = new Element("RenewalPay_List");
		Element mRenewalPay_Detail = null;
			for(int i= 1;i <= pSSRS.MaxRow; i++){
				mRenewalPay_Detail = new Element("RenewalPay_Detail");
				//保险缴费日期
				Element mRenewalPayDate = new Element("RenewalPayDate");
				mRenewalPayDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 1), "yyyy-MM-dd", "yyyyMMdd"));
				//保险缴费金额
				Element mRenewalPayPrem = new Element("RenewalPayPrem");
				mRenewalPayPrem.setText(new DecimalFormat("0.00").format(Double.parseDouble(pSSRS.GetText(i, 2))));
				
				mRenewalPay_Detail.addContent(mRenewalPayDate);
				mRenewalPay_Detail.addContent(mRenewalPayPrem);
				
				mRenewalPay_List.addContent(mRenewalPay_Detail);
			}
			
			mRenewalPayInfo.addContent(mRenewalPay_List);
		}

		cLogger.info("Out CCB_QueryChangeInfo.getRenewalPayInfo()...");
		return mRenewalPayInfo;
	}
	
	public Element getCaseBonusInfo(){
		cLogger.info("Into CCB_QueryChangeInfo.getCaseBonusInfo()...");
		
		strSQL =" and exists (select 1 from lcpol where contno= a.contno and riskcode='"+mRiskCode+"' and  polno=a.polno union  select 1 from lbpol where contno=a.contno and riskcode='"+mRiskCode+"' and  polno=a.polno) ";
		
		Element mCaseBonusInfo = new Element("CaseBonusInfo");
		Element mRenewalPaySume = new Element("CaseBonusSum");
		
		pSQL = "select count(distinct a.BonusMakeDate) from lobonuspol a where a.contno = '"+ mContNo + "'" + strSQL + " and a.BonusMakeDate between '"+ mStartDate +"' and '"+ mEndDate +"'  with ur";
		mRenewalPaySume.setText(pExeSQL.getOneValue(pSQL));
		
		mCaseBonusInfo.addContent(mRenewalPaySume);
		
		if(!pExeSQL.getOneValue(pSQL).equals("0")){
			String hl_sql = "select a.AGetDate,(case when (select bonusgetmode from lcpol where polno=a.polno) is not null then (select bonusgetmode from lcpol where polno=a.polno) else (select bonusgetmode from lbpol where polno=a.polno) end),a.BonusMoney,(select BonusMoney from lobonuspol where contno=a.contno and polno=a.polno order by BonusMakeDate desc fetch first 1 rows only),(select sum(BonusMoney) from lobonuspol where contno=a.contno and polno=a.polno and BonusMakeDate<=a.BonusMakeDate) from lobonuspol a where a.contno = '"+ mContNo + "'" + strSQL + " and a.BonusMakeDate between '"+ mStartDate +"' and '"+ mEndDate +"'group by agetdate,bonusmoney,polno,contno,BonusMakeDate with ur";
			
			pSSRS = pExeSQL.execSQL(hl_sql);
			Element mCaseBonus_List = new Element("CaseBonus_List");
			Element mCaseBonus_Detail = null;
			for(int i = 1 ; i <= pSSRS.MaxRow ; i++) {
				mCaseBonus_Detail = new Element("CaseBonus_Detail");
				//红利实际发放日期
				Element mCaseBonusGetDate = new Element("CaseBonusGetDate");
				mCaseBonusGetDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 1), "yyyy-MM-dd", "yyyyMMdd"));
				//红利处理方式代码
				Element CaseBonusDealFlag = new Element("CaseBonusDealFlag");
				CaseBonusDealFlag.setText(pSSRS.GetText(i, 2));
				//终了红利金额
				Element mCaseBonusNowMoney = new Element("CaseBonusNowMoney");
				mCaseBonusNowMoney.setText(new DecimalFormat("0.00").format(Double.parseDouble(pSSRS.GetText(i, 3))));
				//累积红利金额
				Element mCaseBonusLastMoney = new Element("CaseBonusLastMoney");
				mCaseBonusLastMoney.setText(new DecimalFormat("0.00").format(Double.parseDouble(pSSRS.GetText(i, 4))));
				
				Element mCaseBonusAllMoney = new Element("CaseBonusAllMoney");
				mCaseBonusAllMoney.setText(new DecimalFormat("0.00").format(Double.parseDouble(pSSRS.GetText(i, 5))));
				
				mCaseBonus_Detail.addContent(mCaseBonusGetDate);
				mCaseBonus_Detail.addContent(CaseBonusDealFlag);
				mCaseBonus_Detail.addContent(mCaseBonusNowMoney);
				mCaseBonus_Detail.addContent(mCaseBonusLastMoney);
				mCaseBonus_Detail.addContent(mCaseBonusAllMoney);
				
				mCaseBonus_List.addContent(mCaseBonus_Detail);
				
			}
			mCaseBonusInfo.addContent(mCaseBonus_List);
		}

		cLogger.info("Out CCB_QueryChangeInfo.getCaseBonusInfo()...");
		return mCaseBonusInfo;
	}
	
	public Element getEdorInfo() {
		cLogger.info("Into CCB_QueryChangeInfo.getEdorInfo()...");
		
		strSQL = "select count(distinct a.edorno) from lpedoritem a,lpedorapp b where a.edorno=b.edoracceptno and a.contno = '" + mContNo + "' and b.edorstate='0' and a.edorvalidate between '"+ mStartDate +"' and '"+ mEndDate +"' with ur";
		
		Element mEdorInfo = new Element("EdorInfo");
		Element mEdorSum = new Element("EdorSum");
		mEdorSum.setText(pExeSQL.getOneValue(strSQL));
		
		mEdorInfo.addContent(mEdorSum);
		
		if(!pExeSQL.getOneValue(strSQL).equals("0")){
			pSQL = "select a.edorvalidate,(select edorname from lmedoritem where edorcode=a.edortype fetch first 1 rows only) from lpedoritem a,lpedorapp b where a.edorno=b.edoracceptno and a.contno = '" + mContNo + "' and b.edorstate='0' and a.edorvalidate between '"+ mStartDate +"' and '"+ mEndDate +"'with ur";
			pSSRS = pExeSQL.execSQL(pSQL);
			Element mEdor_List = new Element("Edor_List");
			Element mEdor_Deatil = null;
			for(int i = 1; i<= pSSRS.MaxRow ; i++ ) {
				mEdor_Deatil = new Element("Edor_Deatil");
				//保全日期
				Element mEdorDate = new Element("EdorDate");
				mEdorDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 1), "yyyy-MM-dd", "yyyyMMdd"));
				//保全变动事项描述
				Element mEdorDesc = new Element("EdorDesc");
				mEdorDesc.setText(pSSRS.GetText(i, 2));
				
				mEdor_Deatil.addContent(mEdorDate);
				mEdor_Deatil.addContent(mEdorDesc);	
				mEdor_List.addContent(mEdor_Deatil);
			}
			mEdorInfo.addContent(mEdor_List);
		}
		
		cLogger.info("Out CCB_QueryChangeInfo.getEdorInfo()...");
		return mEdorInfo;
	}
	
	public Element getEdorStatusInfo() {
		cLogger.info("Into CCB_QueryChangeInfo.getEdorStatusInfo()...");
		
		strSQL = "select (case when count(distinct a.edorno)=0 then (select count(distinct contno) from lccontstate where contno ='"+ mContNo+"' and startdate between '"+ mStartDate+"' and '"+ mEndDate +"')else (select count(distinct a.edorno)*2 from dual) end)from lpedoritem a,lpedorapp b where a.edorno=b.edoracceptno and b.edorstate='0' and a.contno='"+ mContNo+"' and a.edortype in ('FX','TF') and a.edorvalidate between '"+ mStartDate +"' and '"+ mEndDate +"' with ur";
		
		Element mEdorStatusInfo = new Element("EdorStatusInfo");
		Element mEdorStatusSum = new Element("EdorStatusSum");
		mEdorStatusSum.setText(pExeSQL.getOneValue(strSQL));
		
		mEdorStatusInfo.addContent(mEdorStatusSum);
		
		if(!pExeSQL.getOneValue(strSQL).equals("0")){
			pSQL = "select (select startdate from lccontstate where contno=a.contno and statetype='Available' fetch first 1 rows only),(case when a.edortype='TF' then a.edorvalidate else (select edorvalue from lpedorespecialdata where edorno=a.edorno and detailtype='FX_D') end),(select stateflag from lccont where contno=a.contno) from lpedoritem a,lpedorapp b where a.edorno=b.edoracceptno and b.edorstate='0' and a.contno = '"+ mContNo+"' and a.edortype in ('FX','TF') and a.edorvalidate between '"+ mStartDate +"' and '"+ mEndDate+"' with ur";
			pSSRS = pExeSQL.execSQL(pSQL);
			Element mEdorStatus_List = new Element("EdorStatus_List");
			Element mEdorStatus_Detail = null;
			for(int i = 1; i<= pSSRS.MaxRow ; i++ ) {
				mEdorStatus_Detail = new Element("EdorStatus_Detail");
				//保单失效日期
				Element mEdorLostDate = new Element("EdorLostDate");
				mEdorLostDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 1), "yyyy-MM-dd", "yyyyMMdd"));
				//保单复效日期
				Element mEdorUseDate = new Element("EdorUseDate");
				mEdorUseDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 2), "yyyy-MM-dd", "yyyyMMdd"));
				//保单状态
				Element mStateFlag = new Element("StateFlag");
				mStateFlag.setText(pSSRS.GetText(i, 3));
				
				mEdorStatus_Detail.addContent(mEdorLostDate);
				mEdorStatus_Detail.addContent(mEdorUseDate);	
				mEdorStatus_Detail.addContent(mStateFlag);	
				
				mEdorStatus_List.addContent(mEdorStatus_Detail);
			}
			mEdorStatusInfo.addContent(mEdorStatus_List);
		}

		cLogger.info("Out CCB_QueryChangeInfo.getEdorStatusInfo()...");
		return mEdorStatusInfo;	
	}
	
	public Element getLCContAmntInfo() {
		cLogger.info("Into CCB_QueryChangeInfo.getLCContAmntInfo()...");
		
		strSQL = "select count(distinct a.edorno) from lpedoritem a,lpedorapp b where a.edorno=b.edoracceptno and b.edorstate='0' and a.contno = '" + mContNo + "' and a.edortype in ('WT','CT','BF','BA','XT') and a.edorvalidate between '"+ mStartDate +"' and '"+ mEndDate +"' with ur";
		
		Element mLCContAmntInfo = new Element("LCContAmntInfo");
		Element mLCContAmntSum = new Element("LCContAmntSum");
		mLCContAmntSum.setText(pExeSQL.getOneValue(strSQL));
		
		mLCContAmntInfo.addContent(mLCContAmntSum);
		
		if(!pExeSQL.getOneValue(strSQL).equals("0")){
			pSQL = "select a.edortype,a.edorappdate from lpedoritem a,lpedorapp b where a.edorno=b.edoracceptno and b.edorstate='0' and a.contno = '" + mContNo + "' and a.edortype in ('WT','CT','BF','BA','XT') and a.edorvalidate between '"+ mStartDate +"' and '"+ mEndDate +"' with ur";
			pSSRS = pExeSQL.execSQL(pSQL);
			Element mLCContAmnt_List = new Element("LCContAmnt_List");
			Element mLCContAmnt_Deatil = null;
			for(int i = 1; i<= pSSRS.MaxRow ; i++ ) {
				mLCContAmnt_Deatil = new Element("LCContAmnt_Deatil");
				//保单申请业务类别代码
				Element mEdortype = new Element("Edortype");
				mEdortype.setText(pSSRS.GetText(i, 1));
				//业务申请日期
				Element mEdorAppDate = new Element("EdorAppDate");
				mEdorAppDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 2), "yyyy-MM-dd", "yyyyMMdd"));
				
				mLCContAmnt_Deatil.addContent(mEdortype);
				mLCContAmnt_Deatil.addContent(mEdorAppDate);
				
				mLCContAmnt_List.addContent(mLCContAmnt_Deatil);
				
			}
			mLCContAmntInfo.addContent(mLCContAmnt_List);
		}

		cLogger.info("Out CCB_QueryChangeInfo.getLCContAmntInfo()...");
		return mLCContAmntInfo;
		
	}
	
	public Element getClaimInfo() {
		cLogger.info("Into CCB_QueryChangeInfo.getClaimInfo()...");
		
		strSQL = "select count(distinct a.caseno) from llclaimpolicy a,llcase b  where a.caseno=b.caseno and b.rgtstate in('09','11','12')  and a.contno = '"+ mContNo +"' and a.riskcode = '" + mRiskCode + "' and exists (select 1 from ljagetclaim where otherno=a.caseno and makedate between '"+ mStartDate +"' and '"+ mEndDate +"') with ur";
		
		Element mClaimInfo = new Element("ClaimInfo");
		Element mClaimSum = new Element("ClaimSum");
		mClaimSum.setText(pExeSQL.getOneValue(strSQL));
		
		mClaimInfo.addContent(mClaimSum);
		
		if(!pExeSQL.getOneValue(strSQL).equals("0")){
			pSQL = "select (select makedate from ljagetclaim where otherno=a.caseno fetch first 1 rows only),sum(a.realpay) from llclaimpolicy a,llcase b  where a.caseno=b.caseno and b.rgtstate in('09','11','12')  and a.contno = '"+ mContNo +"' and a.riskcode = '" + mRiskCode + "' group by a.caseno having (select makedate from ljagetclaim where otherno=a.caseno fetch first 1 rows only) between '"+ mStartDate +"' and '"+ mEndDate +"' with ur";
			pSSRS = pExeSQL.execSQL(pSQL);
			Element mClaim_List = new Element("Claim_List");
			Element mClaim_Deatil = null;
			for(int i = 1; i<= pSSRS.MaxRow ; i++ ) {
				mClaim_Deatil = new Element("Claim_Deatil");
				//理赔记录日期
				Element mClaimDate = new Element("ClaimDate");
				mClaimDate.setText(DateUtil.formatTrans(pSSRS.GetText(i, 1), "yyyy-MM-dd", "yyyyMMdd"));
				//理赔金额
				Element mClaimMoney = new Element("ClaimMoney");
				mClaimMoney.setText(new DecimalFormat("0.00").format(Double.parseDouble(pSSRS.GetText(i, 2))));
				
				mClaim_Deatil.addContent(mClaimDate);
				mClaim_Deatil.addContent(mClaimMoney);	
				
				mClaim_List.addContent(mClaim_Deatil);
			}
			mClaimInfo.addContent(mClaim_List);
		}
		
		cLogger.info("Out CCB_QueryChangeInfo.getClaimInfo()...");
		return mClaimInfo;
		
	}
	
	//现价
	private Element getCashValues(String pPolNo) throws MidplatException {
		Element mCashValues = new Element(CashValues);		
		Element mCashValueCount = new Element(CashValueCount);
	    mCashValues.addContent(mCashValueCount);   	

		String  mSQL = "select * from lcpol where polno='" + pPolNo + "' with ur";
		LCPolDB tLCPolDB = new LCPolDB();
		LCPolSet cLCPolSet = new LCPolSet();
		cLCPolSet = tLCPolDB.executeQuery(mSQL);

		int j = 0;		
		//现金价值个数
		int nCount = 0;   
		
		//循环处理险种下的现金价值信息
	    for (int i = 1; i <= cLCPolSet.size(); i++)
        {
            String tRiskCode = cLCPolSet.get(i).getRiskCode();

            //若是绑定型附加险，则不需要打印现金价值表
            if (CommonBL.isAppendRisk(tRiskCode))
            {
                continue;
            }

            //得到现金价值的算法描述
            LMCalModeDB tLMCalModeDB = new LMCalModeDB();
            //获取险种信息
            tLMCalModeDB.setRiskCode(tRiskCode);
            tLMCalModeDB.setType("X");
            LMCalModeSet tLMCalModeSet = tLMCalModeDB.query();

            //解析得到的SQL语句
            String strSQL = "";
            //如果描述记录唯一时处理
            if (tLMCalModeSet.size() == 1)
            {
                strSQL = tLMCalModeSet.get(1).getCalSQL();
            }
            // 这个险种不需要取现金价值的数据
            if (strSQL.equals(""))
            {
            }
            else
            {
                j = j + 1;

                Calculator calculator = new Calculator();
                //设置基本的计算参数
                calculator.addBasicFactor("ContNo", cLCPolSet.get(i)
                        .getContNo());
                calculator.addBasicFactor("InsuredNo", cLCPolSet.get(i)
                        .getInsuredNo());
                calculator.addBasicFactor("InsuredSex", cLCPolSet.get(i)
                        .getInsuredSex());
                calculator.addBasicFactor("InsuredAppAge", String
                        .valueOf(cLCPolSet.get(i).getInsuredAppAge()));
                calculator.addBasicFactor("PayIntv", String.valueOf(cLCPolSet
                        .get(i).getPayIntv()));
                calculator.addBasicFactor("PayEndYear", String
                        .valueOf(cLCPolSet.get(i).getPayEndYear()));
                calculator.addBasicFactor("PayEndYearFlag", String
                        .valueOf(cLCPolSet.get(i).getPayEndYearFlag()));
                calculator.addBasicFactor("PayYears", String.valueOf(cLCPolSet
                        .get(i).getPayYears()));
                calculator.addBasicFactor("InsuYear", String.valueOf(cLCPolSet
                        .get(i).getInsuYear()));
                calculator.addBasicFactor("Prem", String.valueOf(cLCPolSet.get(
                        i).getPrem()));
                calculator.addBasicFactor("Amnt", String.valueOf(cLCPolSet.get(
                        i).getAmnt()));
                calculator.addBasicFactor("FloatRate", String.valueOf(cLCPolSet
                        .get(i).getFloatRate()));
                calculator.addBasicFactor("Mult", String.valueOf(cLCPolSet.get(
                        i).getMult()));	                
                calculator.addBasicFactor("InsuYearFlag", String
                        .valueOf(cLCPolSet.get(i).getInsuYearFlag()));
                calculator.addBasicFactor("GetYear", String.valueOf(cLCPolSet
                        .get(i).getGetYear()));
                calculator.addBasicFactor("GetYearFlag", String
                        .valueOf(cLCPolSet.get(i).getGetYearFlag()));
                calculator.addBasicFactor("CValiDate", String.valueOf(cLCPolSet
                        .get(i).getCValiDate()));
                calculator.setCalCode(tLMCalModeSet.get(1).getCalCode());
                strSQL = calculator.getCalSQL();

                System.out.println(strSQL);
                //执行现金价值计算sql，获取现金价值列表
                Connection conn = DBConnPool.getConnection();
                if (null == conn)
                {
                    buildError("getConnection", "连接数据库失败！");
                    return  null;
                }
                Statement stmt = null;
                ResultSet rs = null;
                try
                {
                    stmt = conn.createStatement();
                    rs = stmt.executeQuery(strSQL);                    
                    while (rs.next())
                    {            
                    	Element CashValue = new Element("CashValue");
				    	Element tCashYear= new Element("End");
				    	tCashYear.addContent(rs.getString(1).trim());				    	
						Element tCashValue = new Element("Cash");
						tCashValue.addContent(format(rs.getDouble(3)));
						
						CashValue.addContent(tCashYear);
						CashValue.addContent(tCashValue);
						mCashValues.addContent(CashValue);
                        nCount++;
                    }
                    rs.close();
                    stmt.close();
                    conn.close();
                }
                catch (Exception ex)
                {
                    if (null != rs)
                    {
                        try {
							rs.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
                    }
                    try {
						stmt.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
                    try
                    {
                        conn.close();
                    }
                    catch (Exception e)
                    {
                    };
                    //出错处理
                    buildError("executeQuery", "数据库查询失败！");
                    return null;
                }
            }				
		}	    
	    mCashValueCount.addContent(nCount+"");
		return mCashValues;
	}
	
	/**
	 * 出错处理
	 * @param szFunc String
	 * @param szErrMsg String
	 */
	private void buildError(String szFunc, String szErrMsg)
	{
		CError cError = new CError();
		cError.moduleName = "LCContGetCashValueForYB";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	  /**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }
	

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_QueryChangeInfo.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_QueryChangeInfo.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
