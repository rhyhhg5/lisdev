package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;


import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class QuanZhou_BaoQuanQuery extends ServiceImpl {

	public QuanZhou_BaoQuanQuery(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QuanZhou_BaoQuanQuery.service()...");
		 
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		LKTransStatusDB mLKTransStatusDB = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));

			//需增加申请人和领取人同一人且应为投保人
			// 进行犹豫期、退保 业务处理
			if("WT".equals(mLCCont.getChildText("BaoQuanFlag")) || "CT".equals(mLCCont.getChildText("BaoQuanFlag"))){
				mOutXmlDoc = deal(pInXmlDoc);
			}
			if("MQ".equals(mLCCont.getChildText("BaoQuanFlag"))){ //满期给付
				throw new MidplatException("满期给付交易未开通");
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out QuanZhou_BaoQuanQuery.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into QuanZhou_BaoQuanQuery.deal()...");
		
		Element xTranData = pInXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		String mBaoQuanFlag = xTranData.getChild("LCCont").getChildText("BaoQuanFlag");
		
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);
		LCContSet mCont = cont.query();
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		if(mCont.size() > 0){
			LCContSchema lc = mCont.get(1);
			
			mFlag.setText("1");
			mDesc.setText("交易成功！");
			
			LCPolDB pol_db = new LCPolDB();
			pol_db.setContNo(mContNo);
			LCPolSet polSet = pol_db.query();
			
			double mSumPrem = 0;
			String risk_main = "";
			String risk_fu = "";
			for (int i = 1; i <= polSet.size(); i++) {
				LCPolSchema mPol = polSet.get(i);
				mSumPrem += mPol.getPrem();
				if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
					risk_main = mPol.getRiskCode();
				}
				if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
					risk_fu = mPol.getRiskCode();
				}
			}

			Element nEdorAcceptNo = new Element("EdorAcceptNo");
			nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
			
			Element nContNo = new Element("ContNo");
			nContNo.setText(lc.getContNo());
			
			Element nCValiDate = new Element(CValiDate);
			nCValiDate.setText(lc.getCValiDate());
			
			Element nContEndDate = new Element(ContEndDate);
			nContEndDate.setText(lc.getCInValiDate());	//picch特有，保单级保单终止日期
			
			Element nAttachmentContent = new Element("AttachmentContent");
			Element nAppntName = new Element("AppntName");
			nAppntName.setText(lc.getAppntName());
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String now = sdf.format(new Date());
			Element nEdorAcceptYear = new Element("EdorAcceptYear");
			nEdorAcceptYear.setText(now.substring(0,4));
			Element nEdorAcceptMonth = new Element("EdorAcceptMonth");
			nEdorAcceptMonth.setText(now.substring(4,6));
			Element nEdorAcceptDay = new Element("EdorAcceptDay");
			nEdorAcceptDay.setText(now.substring(6,8));
			
			Element nMainRiskCodeName = new Element("MainRiskCodeName");
			nMainRiskCodeName.setText(getRiskRep(risk_main));
			Element nRiskCodeName = new Element("RiskCodeName");
			if(!"".equals(risk_fu)){
				nRiskCodeName.setText(getRiskRep(risk_fu));
			}
			
			Element nSumPrem = new Element("SumPrem");
			if(mBaoQuanFlag.equals("WT")){	//犹豫期保费
				nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
			}
			if(mBaoQuanFlag.equals("CT")){	//退保保费即解约
				PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
				double pSumPrem = mPedorYBTBudgetBL.getZTmoney(lc.getContNo(),now);
				nSumPrem.setText(new DecimalFormat("0.00").format(pSumPrem));
			}
			
			nAttachmentContent.addContent(nAppntName);
			nAttachmentContent.addContent(nEdorAcceptYear);
			nAttachmentContent.addContent(nEdorAcceptMonth);
			nAttachmentContent.addContent(nEdorAcceptDay);
			nAttachmentContent.addContent(nMainRiskCodeName);
			nAttachmentContent.addContent(nRiskCodeName);
			nAttachmentContent.addContent(nSumPrem);
			mEdorInfo.addContent(nEdorAcceptNo);
			mEdorInfo.addContent(nCValiDate);
			mEdorInfo.addContent(nContEndDate);
			mEdorInfo.addContent(nContNo);
			mEdorInfo.addContent(nAttachmentContent);
		}else {
			mFlag.setText("0");
			mDesc.setText("未查到对应保单信息！");
			throw new MidplatException("未查到对应保单信息！");
		}
		
		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out QuanZhou_BaoQuanQuery.deal()...");
		return new Document(mTranData);
	}
	
	public String getRiskRep(String riskCode){
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into QuanZhou_BaoQuanQuery.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild("LCCont");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText("PrtNo"));
		mLKTransStatusDB.setPolNo(mLCCont.getChildText("ContNo"));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Prem"));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("RiskCode"));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out QuanZhou_BaoQuanQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}
}

