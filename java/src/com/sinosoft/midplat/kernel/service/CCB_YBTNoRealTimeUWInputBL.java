package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKNoRealTimeUWCheckSchema;
import com.sinosoft.lis.vschema.LKNoRealTimeUWCheckSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CCB_YBTNoRealTimeUWInputBL extends ServiceImpl {

	 //得到系统当前时间
	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	
	public CCB_YBTNoRealTimeUWInputBL(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into YBTNoRealTimeUWInputBL.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		Element mTranData = null;  
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
//		Element mBaseInfo = pInXmlDoc.getRootElement().getChild(BaseInfo);
		LKTransStatusDB mLKTransStatusDB = null;	
		LKNoRealTimeUWCheckSet mLKNoRealTimeUWCheckSet = new LKNoRealTimeUWCheckSet();
		
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		
		Element nLCCont = new Element("LCCont");
		Element nPrtNo = new Element("PrtNo");
		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			String DetailCount = pInXmlDoc.getRootElement().getChild("Details").getChildText("DetailCount");
			System.out.println("循环节点数量：" + DetailCount);
			int tDetailCount = pInXmlDoc.getRootElement().getChild("Details").getChildren("Detail").size();
			LKNoRealTimeUWCheckSchema tLKNoRealTimeUWCheckSchema ;
			for (int i = 0; i < tDetailCount; i++) {
				tLKNoRealTimeUWCheckSchema = new LKNoRealTimeUWCheckSchema();
				
				Element tDetail = (Element)pInXmlDoc.getRootElement().getChild("Details").getChildren("Detail").get(i);
				tLKNoRealTimeUWCheckSchema.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
				tLKNoRealTimeUWCheckSchema.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
				tLKNoRealTimeUWCheckSchema.setTransDate(tDetail.getChildTextTrim("TransDate"));
				tLKNoRealTimeUWCheckSchema.setRiskCode(tDetail.getChildTextTrim("RiskCode"));
				tLKNoRealTimeUWCheckSchema.setAppntName(tDetail.getChildTextTrim("AppntName"));
				tLKNoRealTimeUWCheckSchema.setAppntIdType(tDetail.getChildTextTrim("AppntIDType"));
				tLKNoRealTimeUWCheckSchema.setAppntIdNo(tDetail.getChildTextTrim("AppntIDNo"));
				tLKNoRealTimeUWCheckSchema.setProposalContno(tDetail.getChildTextTrim("ProposalNo"));
				tLKNoRealTimeUWCheckSchema.setBankAccNo(tDetail.getChildTextTrim("BankAccNo"));
				
				tLKNoRealTimeUWCheckSchema.setTbDealStatus("0"); //0为初始状态  1为已查询
				tLKNoRealTimeUWCheckSchema.setProductType("1");
				tLKNoRealTimeUWCheckSchema.setPassFlag("0");   //0为未查询保单状态， 
				tLKNoRealTimeUWCheckSchema.setOperator("ybt");
				
				tLKNoRealTimeUWCheckSchema.setMakeDate(mCurrentDate);
				tLKNoRealTimeUWCheckSchema.setMakeTime(mCurrentTime);
				
				mLKNoRealTimeUWCheckSet.add(tLKNoRealTimeUWCheckSchema);
			}
			
			MMap tMMap = new MMap();
			VData tVData = new VData();
			tMMap.put(mLKNoRealTimeUWCheckSet, "INSERT");
			tVData.add(tMMap);
			PubSubmit tPs = new PubSubmit();
			if (!tPs.submitData(tVData, "")) {
				mFlag.setText("1");
				mDesc.setText("执行插入非实时核保信息失败!");
			}else {
				mFlag.setText("0");
				mDesc.setText("交易成功！");
				
				
//				//随机生成13位数
//				String prtRan = "";
//				while(prtRan.length()<7){
//					prtRan +=(int)(Math.random()*10);
//				}
//				
//				String mprtRan = "103000";
//				prtRan = mprtRan + prtRan + "l";
//
//				//后置机生成投保单印刷号给及银行
//				long[] prtNoList = newPrtNo(Long.parseLong(prtRan), 10); //1020002147894l 长度为12  最后是一个L   转为long型的意思
//				for (int i = 0; i < prtNoList.length; i++) {
//					System.out.println(prtNoList[i]);
//					String cx_sql = "select 1 from lccont where prtno = '"+ prtNoList[i] +"' union select 1 from lbcont where prtno = '"+ prtNoList[i] +"' with ur";
//					if(!new ExeSQL().getOneValue(cx_sql).equals("1")){
//						nPrtNo.setText(String.valueOf(prtNoList[i]));
//						break;
//					}
//				}
			}
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		
		
		mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		nLCCont.addContent(nPrtNo); 
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(nLCCont);
		mOutXmlDoc = new Document(mTranData);
		cLogger.info("Out YBTNoRealTimeUWInputBL.service()...");
		return mOutXmlDoc;
	}
	
	//mPrtNo：起始号   num：寻找到指定个数
	public static long[] newPrtNo(long mPrtNo,int num){
		
		long[] prtNoList = new long[num];
		int x = 1;
		for (int i = 0; i < num; i++) {
			int b = 0;
			int last = -1;
			long sum = mPrtNo + x;
//			System.out.println("@@@@@@@@1="+sum);
			last = (int)(sum % 10);
			sum = sum / 10;
			String prt = String.valueOf(sum);
			
			for (int j = 0; j < prt.length(); j++) {
				b += (int)(sum % 10);
				sum = (long)(sum / 10);
			}
//			System.out.println("b = "+b % 10 +"   last = "+ last);
			if(last == (b % 10)){
//				满足规则的prtNo 记录在数组里
				prtNoList[i] = (mPrtNo + x);
			}else{
//				不满足规则的prtNo 让i减一，实现重新循环，以便找到指定个数的prtNo
				i --;
			}
			//循环数
			x ++;
		}
		return prtNoList;
	}
	
	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransCode(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		/******* 建行关于NRT801交易会传相同流水号，所以在这里自动给流水号后加四位数字。 ********/
		String sql_db = "select max(transNo) from lktransstatus where bankcode = '"+mBaseInfo.getChildTextTrim("BankCode")
				+"' and bankbranch = '"+mBaseInfo.getChildTextTrim("ZoneNo")+"'" +"and banknode = '"+mBaseInfo.getChildTextTrim("BrNo")+"' and transno like '"+mBaseInfo.getChildTextTrim("TransrNo")+"%' with ur";
		SSRS ssrs = new ExeSQL().execSQL(sql_db);
		String mTransNo = ssrs.GetText(1, 1);
		if(ssrs.MaxRow > 0 && !"".equals(mTransNo)){
			System.out.println("&&&&&&&&="+mTransNo);
			mLKTransStatusDB.setTransNo(mTransNo.substring(0,mTransNo.length()-4)+(Integer.parseInt(mTransNo.substring(mTransNo.length()-4))+1));
		}else {
			mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo")+"1001");
		}
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		return mLKTransStatusDB;
	}
	
}
