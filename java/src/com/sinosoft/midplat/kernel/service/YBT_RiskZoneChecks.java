package com.sinosoft.midplat.kernel.service;

import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YBT_RiskZoneChecks {

	
	
	private String STATUS_ON = "1";
	private String STATUS_OFF = "2";
	
	public String runChecks(String bankcode,String managecom,String riskcode,String channel){
		
		System.out.println("进入险种停售验证");
		String stopDate = DateUtil.getCur10Date();
		String statusString = STATUS_ON;
		String mSQL = "select channel,swtype,makedate,swvalue from ybtriskswitch where status = '1' and datetype = 'ST' and riskcode = '" + riskcode + "'  with ur";
		SSRS tSSRS = new ExeSQL().execSQL(mSQL);
		int rownum = tSSRS.getMaxRow();
		if (rownum<1) {
			return STATUS_ON;
		}
		for (int i = 0; i < rownum; i++) {
			int j = i+1;
			String tChannel = tSSRS.GetText(j, 1);
			String tSwtype = tSSRS.GetText(j, 2);
			String tMakedate = tSSRS.GetText(j, 3);
			String tSwvalue = tSSRS.GetText(j, 4);
			System.out.println("获取到的信息共：" + rownum +"条，当前第" + j + "条，数据分别为：channel- "+tChannel+";swtype- "+tSwtype+";makedate- "+tMakedate+";");
			if (channel.equals(tChannel)||"00".equals(tChannel)) {
				System.out.println("有该渠道信息");
				statusString = switchType(tSwtype, stopDate, tMakedate, tSwvalue, bankcode, managecom, riskcode);
				return statusString;
				
			}
			
			
			
			
		}
		
		
		
		System.out.println("险种停售验证完毕");
		return STATUS_ON;
	}
	
	
	public String switchType(String tSwtype,String stopDate,String tMakedate,String tSwvalue,String bankcode,String managecom,String riskcode){
		if ("ALL".equals(tSwtype)) {
			if ((tMakedate.compareTo(stopDate) > 0)) {
				return STATUS_OFF;
			}else {
				return STATUS_ON;
			}
		}else if("BK".equals(tSwtype)&&bankcode.equals(tSwvalue)){
			if ((tMakedate.compareTo(stopDate) > 0)) {
				return 	STATUS_OFF;
			}else {
				return STATUS_ON;
			}
		}else if("COM".equals(tSwtype)&&managecom.equals(tSwvalue)){
			if ((tMakedate.compareTo(stopDate) > 0)) {
				return STATUS_OFF;
			}else {
				return STATUS_ON;
			}
		}else if("RK".equals(tSwtype)&&riskcode.equals(tSwvalue)){
			if ((tMakedate.compareTo(stopDate) > 0)) {
				return STATUS_OFF;
			}else {
				return STATUS_ON;
			}
		}else {
			return STATUS_ON;
		}
	}
	
	
	public static void main(String[] args) {
		System.out.println("2017-01-12".compareTo("2017-01-11"));

	}

}
