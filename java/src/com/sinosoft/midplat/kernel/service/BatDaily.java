package com.sinosoft.midplat.kernel.service;

import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKDailyDetailSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LKDailyDetailSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.yinbaotongbank.YouChuInterFace;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatDaily extends ServiceImpl {
    private String cCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");

    private String cCurrentTime = DateUtil.getCurDate("HH:mm:ss");

    public BatDaily(Element pThisBusiConf) {
        super(pThisBusiConf);
    }

    public Document service(Document pInXmlDoc) {
        cLogger.info("Into BatDaily.service()...");
        Document mOutXmlDoc = null;
        LKTransStatusDB mLKTransStatusDB = null;
        GlobalInput mGlobalInput = null;

        try {
            mLKTransStatusDB = insertTransLog(pInXmlDoc);

            mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
                    .getBankCode(), mLKTransStatusDB.getBankBranch(),
                    mLKTransStatusDB.getBankNode());

            /**
             * 调用核心每日取盘接口
             */
            String mBankCode = mLKTransStatusDB.getBankCode();
            String mManageCom = mGlobalInput.ManageCom.substring(0, 2);
            String mSql = "select bankcode from ldbank where operator = 'ybt' and agentcode = '"
                    + mBankCode
                    + "' and comcode = '"
                    + mManageCom
                    + "' with ur";
            mBankCode = new ExeSQL().getOneValue(mSql);
            cLogger.info("银行编码:" + mBankCode + ",每日取盘管理机构：" + mManageCom);
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("ComCode", mManageCom);
            mTransferData.setNameAndValue("BankCode", mBankCode);

            LYSendToBankSet mLYSendToBankSet = new YouChuInterFace()
                    .getFlag(mTransferData);

            Document mDoc = createStdXml(mLYSendToBankSet, mLKTransStatusDB);

            // 存储核心接口报文
            FileOutputStream mFos = new FileOutputStream(cThisBusiConf
                    .getChildText(path)
                    + "REQ_" + mLKTransStatusDB.getBankAcc() + ".xml");
            JdomUtil.output(mDoc, mFos);
            mFos.close();

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
            List mTranDataList = mDoc.getRootElement().getChildren();
            for (int i = 0; i < mTranDataList.size(); i++) {
                mOutXmlDoc.getRootElement().addContent(
                        (Element) ((Element) mTranDataList.get(i)).clone());
            }

            mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
            mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        } catch (Exception ex) {
            cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

            if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
                mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
                mLKTransStatusDB.setDescr(ex.getMessage());
            }

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
        }

        if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
            if (null != mGlobalInput) {
                mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
            }
            mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
            if (!mLKTransStatusDB.update()) {
                cLogger.error("更新日志信息失败！"
                        + mLKTransStatusDB.mErrors.getFirstError());
            }
        }

        cLogger.info("Out BatDaily.service()!");
        return mOutXmlDoc;
    }

    public LKTransStatusDB insertTransLog(Document pXmlDoc)
            throws MidplatException {
        cLogger.info("Into BatDaily.insertTransLog()...");

        Element mTranData = pXmlDoc.getRootElement();
        Element mBaseInfo = mTranData.getChild(BaseInfo);

        LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
        mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
        mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
        mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
        mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
        mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
        mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
        mLKTransStatusDB.setBankAcc(mBaseInfo.getChildText("FileName")); // 借用做文件名称
        mLKTransStatusDB.setTempFeeNo(mBaseInfo.getChildText("DealType")); // 借用做业务类型
        mLKTransStatusDB.setTransDate(cCurrentDate);
        mLKTransStatusDB.setTransTime(cCurrentTime);
        mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        mLKTransStatusDB.setMakeDate(cCurrentDate);
        mLKTransStatusDB.setMakeTime(cCurrentTime);
        mLKTransStatusDB.setModifyDate(cCurrentDate);
        mLKTransStatusDB.setModifyTime(cCurrentTime);

        if (!mLKTransStatusDB.insert()) {
            cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
            throw new MidplatException("插入日志失败！");
        }

        cLogger.info("Out BatDaily.insertTransLog()!");
        return mLKTransStatusDB;
    }

    public Document createStdXml(LYSendToBankSet pLYSendToBankSet,
            LKTransStatusDB pLKTransStatusDB) throws MidplatException {
        cLogger.info("Into BatDaily.createStdXml()...");

        int mTotalNum = pLYSendToBankSet.size();
        cLogger.info("此次每日总笔数为:" + mTotalNum);
        Element mTranData = new Element(TranData);
        Element tTotalNum = new Element("TotalNum");
        tTotalNum.setText(String.valueOf(mTotalNum));
        Element tTotalSum = new Element("TotalSum");
        tTotalSum.setText("0.00");
        mTranData.addContent(tTotalNum);
        mTranData.addContent(tTotalSum);

        LKDailyDetailSet mLKDailyDetailSet = new LKDailyDetailSet();
        for (int i = 1; i <= pLYSendToBankSet.size(); i++) {
            LYSendToBankSchema mLYSendToBankSchema = pLYSendToBankSet.get(i);

            String mSerialNo = mLYSendToBankSchema.getSerialNo();
            String mPayCode = mLYSendToBankSchema.getPayCode();
            String mNoType = mLYSendToBankSchema.getDoType();
            String mPolNo = mLYSendToBankSchema.getPolNo();
            String mTranDate = mLYSendToBankSchema.getModifyDate();
            String mPayMoney = new DecimalFormat("0.00")
                    .format(mLYSendToBankSchema.getPayMoney());
            String mWhetherEdor = mLYSendToBankSchema.getIDType();
            String mPayTimes = mLYSendToBankSchema.getNoType();
            String mLatPayDate = mLYSendToBankSchema.getBankDealDate();
            String mLatPayAmnt = mLYSendToBankSchema.getAccNo();
            String mCurPayDate = mLYSendToBankSchema.getSendDate();

            Element mDetail = new Element(Detail);
            Element tOperType = new Element("OperType");
            tOperType.setText(mNoType);
            Element tPolNo = new Element("PolNo");
            tPolNo.setText(mPolNo);
            Element tTranDate = new Element(TranDate);
            tTranDate.setText(mTranDate);
            Element tPayMoney = new Element("PayMoney");
            tPayMoney.setText(mPayMoney);
            Element tWhetherEdor = new Element("WhetherEdor");
            tWhetherEdor.setText(mWhetherEdor);
            Element tPayTimes = new Element("PayTimes");
            tPayTimes.setText(mPayTimes);
            Element tLatPayDate = new Element("LatPayDate");
            tLatPayDate.setText(mLatPayDate);
            Element tLatPayAmnt = new Element("LatPayAmnt");
            tLatPayAmnt.setText(mLatPayAmnt);
            Element tCurPayDate = new Element("CurPayDate");
            tCurPayDate.setText(mCurPayDate);

            mDetail.addContent(tOperType);
            mDetail.addContent(tPolNo);
            mDetail.addContent(tTranDate);
            mDetail.addContent(tPayMoney);
            mDetail.addContent(tWhetherEdor);
            mDetail.addContent(tPayTimes);
            mDetail.addContent(tLatPayDate);
            mDetail.addContent(tLatPayAmnt);
            mDetail.addContent(tCurPayDate);

            mTranData.addContent(mDetail);

            LKDailyDetailSchema mLKDailyDetailSchema = new LKDailyDetailSchema();
            mLKDailyDetailSchema.setSerialNo(mSerialNo);
            mLKDailyDetailSchema.setPayCode(mPayCode);
            mLKDailyDetailSchema.setTransNo(pLKTransStatusDB.getTransNo());
            mLKDailyDetailSchema.setOperType(mNoType);
            mLKDailyDetailSchema.setPolNo(mPolNo);
            mLKDailyDetailSchema.setTranDate(mTranDate);
            mLKDailyDetailSchema.setPayMoney(mPayMoney);
            mLKDailyDetailSchema.setWhetherEdor(mWhetherEdor);
            mLKDailyDetailSchema.setPayTimes(mPayTimes);
            mLKDailyDetailSchema.setLatPayDate(mLatPayDate);
            mLKDailyDetailSchema.setLatPayAmnt(mLatPayAmnt);
            mLKDailyDetailSchema.setCurPayDate(mCurPayDate);
            mLKDailyDetailSchema.setMakeDate(cCurrentDate);
            mLKDailyDetailSchema.setMakeTime(DateUtil.getCurDate("HH:mm:ss"));
            mLKDailyDetailSchema.setModifyDate(cCurrentDate);
            mLKDailyDetailSchema.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));

            mLKDailyDetailSet.add(mLKDailyDetailSchema);
        }

        MMap mMMap = new MMap();
        mMMap.put(mLKDailyDetailSet, "INSERT");
        VData mVData = new VData();
        mVData.add(mMMap);
        PubSubmit mPubSubmit = new PubSubmit();
        if (!mPubSubmit.submitData(mVData, "")) {
            cLogger.error(mPubSubmit.mErrors.getFirstError());
            throw new MidplatException("提交每日数据失败！");
        }

        cLogger.info("Out BatDaily.createStdXml()!");
        return new Document(mTranData);
    }
}
