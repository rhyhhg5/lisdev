/**
 * 通用简单交易处理类。
 * 此类本身不做任何业务处理，仅插入一条交易日志。
 * 目前使用此类的交易有：中行冲正、建行绿灯
 */

package com.sinosoft.midplat.kernel.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CCB_BatRequest extends ServiceImpl {
	public CCB_BatRequest(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_BatRequest.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			mOutXmlDoc = deal(pInXmlDoc);
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error("通用简单(CCB_BatRequest)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_BatRequest.service()!");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws ParseException {
		cLogger.info("Into CCB_BatRequest.deal()...");
		Document mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		ExeSQL es = new ExeSQL();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String mTranDate = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("TranDate");
		Date date = sdf.parse(mTranDate);
		mTranDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
//		mTranDate = DateUtil.parseDate(mTranDate, "yyyy-MM-dd");
		String tSQL = "select AccName,bankaccno,contno,prem from lccont where cardflag in ('6','8') and salechnl in ('04','13') and bankcode like '03%' and makedate = '"+mTranDate+"' with ur";
//		Element mLCCont = new Element("LCCont");
		SSRS ssrs = es.execSQL(tSQL);
		double mTotalSum = 0; 
		Element tTotalNum = new Element("TotalNum");
		if(ssrs.MaxRow>0){
			for(int i =1;i<=ssrs.MaxRow;i++){
				Element mDetail = new Element("Detail");
				Element mAccName = new Element("AccName");
				mAccName.setText(ssrs.GetText(i, 1));
				Element mAccNo = new Element("AccNo");
				mAccNo.setText(ssrs.GetText(i, 2));
				Element mIdNum = new Element("IdNum");
				mIdNum.setText(i+"");
				Element mPolNo = new Element("PolNo");
				mPolNo.setText(ssrs.GetText(i, 3));
				Element mPayMoney = new Element("PayMoney");
				mPayMoney.setText(ssrs.GetText(i, 4));
				mTotalSum += Double.parseDouble(ssrs.GetText(i, 4));
				mDetail.addContent(mAccName);
				mDetail.addContent(mAccNo);
				mDetail.addContent(mIdNum);
				mDetail.addContent(mPolNo);
				mDetail.addContent(mPayMoney);
				
				mOutXmlDoc.getRootElement().addContent(mDetail);
			}
			tTotalNum.setText(ssrs.MaxRow+"");
		}else{
			tTotalNum.setText("0");
		}
		Element tTotalSum = new Element("TotalSum");
		tTotalSum.setText(mTotalSum+"");
		mOutXmlDoc.getRootElement().addContent(tTotalNum);
		mOutXmlDoc.getRootElement().addContent(tTotalSum);
		
//		Element mBatIncome = new Element("BatIncome");
//		if(Integer.parseInt(es.getOneValue(tSQL))>0){
//			mBatIncome.setText("1");
//		}else{
//			mBatIncome.setText("0");
//		}
//		mLCCont.addContent(mBatIncome);
			
		cLogger.info("Out CCB_BatRequest.deal()...");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
    throws MidplatException {
		cLogger.info("Into BatRequest.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setBankAcc(mBaseInfo.getChildText("FileName")); // 借用做文件名称
		mLKTransStatusDB.setTempFeeNo(mBaseInfo.getChildText("DealType")); // 借用做业务类型S/F
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
		    cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
		    throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out BatRequest.insertTransLog()!");
		return mLKTransStatusDB;
		}
}
