/**
 * 获取保单详情查询
 * 根据日期获取批量包个数并返回
 */

package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKYBTBQTRANSDETAILSchema;
import com.sinosoft.lis.vschema.LKYBTBQTRANSDETAILSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBLForBaoQuan;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CCB_GetContDetailQuery extends ServiceImpl {

	public CCB_GetContDetailQuery(Element pThisConf) {
		super(pThisConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_GetContDetailQuery.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element tTranData = new Element("TranData");
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

			Element mTranData = pInXmlDoc.getRootElement();
			Element mLccont = mTranData.getChild("LCCont");
			String mDate = mLccont.getChildTextTrim("Enqr_Dt");
			mDate = DateUtil.date8to10(mDate);
			// 具体业务 需要修改 重写sql语句 
//			String mSQL = "select count(*)" + " " + "from lccont a" + " "
//					+ "where a.cardflag='9'" + " "
//					+ "and a.agentcom like 'PY004%'" + " "
//					+ "and a.salechnl = '04'" + " " + "and a.stateflag='1'"
//					+ " " + "and modifydate = to_date('" + mDate
//					+ "','yyyy - mm - dd')";
			
			String sql = "SELECT"
		    +" p.contno,p.edortype"
		    +" FROM"
		    +" lpedoritem p, "
		    +" lbcont b "
		    +" WHERE "
		    +" b.contno = p.contno "
		    +" AND b.salechnl ='04'"
		    +" AND b.cardflag ='9'"
		    +" AND b.agentcom LIKE 'PY004%'"
		    +" AND p.makedate =\'"+mDate+"\'"
		    +" AND p.edortype ='WT' "
		    +" WITH UR";
			
			
			
			SSRS ssr_o = new ExeSQL().execSQL(sql);
			int mCount =  ssr_o.MaxRow;
			int mNum = mCount;
			int mBtchBagNum;
			if (mNum % 10 == 0) {
				mBtchBagNum = mNum / 100;
			} else {
				mBtchBagNum = mNum / 100 + 1;
			}
			cLogger.info("-----------------总条数" + mCount);
			cLogger.info("------------------------批量包个数" + mBtchBagNum);
			int startLine = 0;
			int endLine = 0;
			String mContno;
			for (int i = 0; i < mBtchBagNum; i++) {
				if (i == 0 && mBtchBagNum == 1) {
					endLine = mNum;
				} else if ((i + 1) * 100 < mNum) {
					startLine = endLine;
					endLine += 100;
				} else {
					startLine = endLine;
					endLine = mNum;
				}
				Element mDetail_List = new Element("Detail_List");
				// 循环每次查出十条保单信息
				for (startLine += 1; startLine <= endLine; startLine++) {
					mContno = ssr_o.GetText(startLine, 1);
					cLogger.info("做过保全项目的保单号::"+ContNo);
//					mContno = "014060099000003";
					YbtContQueryBLForBaoQuan ybtContQuery = new YbtContQueryBLForBaoQuan(mContno);   //查询保全项目
					Element mLCCont = ybtContQuery.getLCCont(mContno);
					// 添加循环节点
					Element mDetail = new Element("Detail");
					mDetail.addContent(mLCCont);
					mDetail_List.addContent(mDetail);
				}
				tTranData.addContent(mDetail_List);

			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");

			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (MidplatException ex) {
			cLogger.error("获取保单详情查询(CCB_GetContDetailQuery)交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());

		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out CCB_GetContDetailQuery.service()!");
		mOutXmlDoc.getRootElement().addContent(tTranData);
		//插入保全回传数据日志表,为不影响目前功能，另起一线程 add by GaoJinfu 20180322
		new GetContDetailInsertThread(mOutXmlDoc).start();
		
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into CCB_GetContDetailQuery.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_GetContDetailQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}

	/**
	 * 添加一个线程想保全日志表中插入数据 add by GaoJinfu 20180313
	 * @author JiffGao
	 *
	 */
	private class GetContDetailInsertThread extends Thread{
		private final Logger cLogger = Logger.getLogger(GetContDetailInsertThread.class);
		
		private final Document cInXmlDoc;
		
		private GetContDetailInsertThread(Document pInXmlDoc) {
			cInXmlDoc = pInXmlDoc;
		}
		
		public void run() {
			cLogger.info("Into GetContDetailInsertThread.run()...");
			LKYBTBQTRANSDETAILSet mLKYBTBQTRANSDETAILSet = new LKYBTBQTRANSDETAILSet();
			Element mDetail_List = cInXmlDoc.getRootElement().getChild("TranData").getChild("Detail_List");
			List lisDetail = mDetail_List.getChildren("Detail");
			String sql = "";
			ExeSQL exeSQL = new ExeSQL();
			for (int i = 0; i < lisDetail.size(); i++) {
				Element mDetail = (Element) lisDetail.get(i);
				LKYBTBQTRANSDETAILSchema mLKYBTBQTRANSDETAILSchema = new LKYBTBQTRANSDETAILSchema();
				sql = "select lk.bankcode from lkcodemapping lk,lbcont lb where lb.agentcom = lk.agentcom and lb.contno = '"+mDetail.getChild("LCCont").getChildText("ContNo")+"' fetch first 1 row only";
				mLKYBTBQTRANSDETAILSchema.setBANKCODE(exeSQL.getOneValue(sql));
				sql = "select lp.edortype from lpedoritem lp,lbcont lb where lp.contno = lb.contno and lp.edortype in ('WT','CT') and lb.contno = '"+mDetail.getChild("LCCont").getChildText("ContNo")+"' with ur";
				mLKYBTBQTRANSDETAILSchema.setSTATETYPE(exeSQL.getOneValue(sql));
				mLKYBTBQTRANSDETAILSchema.setCONTNO(mDetail.getChild("LCCont").getChildText("ContNo"));
				mLKYBTBQTRANSDETAILSchema.setPRTNO(mDetail.getChild("LCCont").getChildText("PrtNo"));
				mLKYBTBQTRANSDETAILSchema.setAPPNTNAME(mDetail.getChild("LCCont").getChild("LCAppnt").getChildText("AppntName"));
				mLKYBTBQTRANSDETAILSchema.setAPPNTIDTYPE(mDetail.getChild("LCCont").getChild("LCAppnt").getChildText("AppntIDType"));
				mLKYBTBQTRANSDETAILSchema.setAPPNTIDNO(mDetail.getChild("LCCont").getChild("LCAppnt").getChildText("AppntIDNo"));
				mLKYBTBQTRANSDETAILSchema.setMAKEDATE(DateUtil.getCur10Date());
				mLKYBTBQTRANSDETAILSchema.setMAKETIME(DateUtil.getCur8Time());
				mLKYBTBQTRANSDETAILSchema.setMODIFYDATE(DateUtil.getCur10Date());
				mLKYBTBQTRANSDETAILSchema.setMODIFYTIME(DateUtil.getCur8Time());
				mLKYBTBQTRANSDETAILSet.add(mLKYBTBQTRANSDETAILSchema);
			}
			MMap map = new MMap();
			VData mInputData = new VData();
			PubSubmit tPubSubmit = new PubSubmit();
			map.put(mLKYBTBQTRANSDETAILSet, "INSERT");
			mInputData.clear();
 	        mInputData.add(map);
 	        tPubSubmit.submitData(mInputData, "");
			cLogger.info("Out GetContDetailInsertThread.run()!");
		}
	}
	
	public static void main(String[] args) throws IOException {
		// String
		// mSQL="select * from lccont where 1='1' and Contno='2300135836'";
		// Document mDoc=new
		// CCB_GetContDetailQuery(null).getEncodedResult(mSQL);
		 
		String file  = "C:\\Users\\Administrator\\Desktop\\ccb_108011rv11442923541009636_P53817107_200212.xml";
		Document my_DOC  = JdomUtil.build(new FileInputStream(file));
		CCB_GetContDetailQuery aaa = new CCB_GetContDetailQuery(null);
		Document my_Doc = aaa.service(my_DOC);
		FileOutputStream pOs=new FileOutputStream("D:\\getContDetail.xml");
		 JdomUtil.output(my_Doc, pOs);
		 pOs.close();
		
		
	}

}

