package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;


import com.sinosoft.lis.db.LAAgenttempDB;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.vschema.LAAgenttempSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;


public class CCB_QueryAgentInfo extends ServiceImpl  {

	public CCB_QueryAgentInfo(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_QueryDetail.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		try {
			
			Element mTranData = pInXmlDoc.getRootElement();
			Element mBaseInfo = mTranData.getChild(BaseInfo);
			Element mLCCont = mTranData.getChild(LCCont);
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
		
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			String sql = "select agentcode from LAComToAgent where agentcom ='"+ mGlobalInput.AgentCom +"' and RelaType='1' with ur";
			String mAgentcode = "";
			if(new ExeSQL().execSQL(sql).MaxRow > 0){
				mAgentcode = new ExeSQL().execSQL(sql).GetText(1, 1);
			}else {
				cLogger.info("未查到该网点下的代理人编码！");
			}
			
			LAAgenttempDB mLAAgentTempDB = new LAAgenttempDB();
			mLAAgentTempDB.setAgentCode(mAgentcode);
			mLAAgentTempDB.setManageCom(mGlobalInput.ManageCom);
			mLAAgentTempDB.setBranchType("3");
			mLAAgentTempDB.setBranchType2("01");
			mLAAgentTempDB.setAgentState("3");
			if(!mLCCont.getChildText("Name").equals("") && !mLCCont.getChildText("IDNo").equals("")){
				mLAAgentTempDB.setName(mLCCont.getChildText("Name"));
				mLAAgentTempDB.setIDNo(mLCCont.getChildText("IDNo"));
			}
			
			LAAgenttempSet mLAAgenttempSet = mLAAgentTempDB.query();
			if(mLAAgenttempSet.size() > 0){
				Element pChkDetail = null;
				for(int i = 1; i<= mLAAgenttempSet.size();i++){
					LAAgenttempSchema mLAAgenttempSchema = mLAAgenttempSet.get(i);
				
					Element mName = new Element("Name");
					Element mIDNoType = new Element("IDNoType");
					Element mIDNo = new Element("IDNo");
					Element mMobile = new Element("Mobile");
					Element mEmail = new Element("Email");
					Element mPhone = new Element("Phone");
					pChkDetail = new Element("ChkDetail");
					
					mName.setText(mLAAgenttempSchema.getName());
					mIDNoType.setText(mLAAgenttempSchema.getIDNoType());
					mIDNo.setText(mLAAgenttempSchema.getIDNo());
					mMobile.setText(mLAAgenttempSchema.getMobile());
					mEmail.setText(mLAAgenttempSchema.getEMail());
					mPhone.setText(mLAAgenttempSchema.getPhone());
					
					pChkDetail.addContent(mName);
					pChkDetail.addContent(mIDNoType);
					pChkDetail.addContent(mIDNo);
					pChkDetail.addContent(mMobile);
					pChkDetail.addContent(mEmail);
					pChkDetail.addContent(mPhone);
				}
				
				Element pLCCont = new Element(LCCont);
				pLCCont.addContent(pChkDetail);
				
				Element pTranData = new Element(TranData);
				Element mRetData = new Element(RetData);
				
				Element mFlag = new Element(Flag);
				Element mDesc = new Element(Desc);
				mRetData.addContent(mFlag.setText("1"));
				mRetData.addContent(mDesc.setText("交易成功！"));
				
				pTranData.addContent(mRetData);
				pTranData.addContent(pLCCont);
				mOutXmlDoc = new Document(pTranData);
			}else {
				throw new MidplatException("未查到相关驻点员信息！");
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_QueryDetail.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_QueryDetail.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_QueryDetail.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
