package com.sinosoft.midplat.kernel.service;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ProcedureFee extends ServiceImpl {
	public ProcedureFee(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ProcedureFee.service()...");
		
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(), 
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			String tYearAndMonth = 
				pInXmlDoc.getRootElement().getChild(
						Body).getChildText("YearAndMonth");
			String tYearStr = tYearAndMonth.substring(0, 4);
			String tMonthStr = tYearAndMonth.substring(4);
			String tStartDay = tYearStr+"-"+tMonthStr+"-01";
			Calendar tTmpCalendar = new GregorianCalendar(
					Integer.parseInt(tYearStr), Integer.parseInt(tMonthStr)-1, 1);
			tTmpCalendar.add(Calendar.MONTH, 1);
			String tEndDay = DateUtil.getDateStr(tTmpCalendar, "yyyy-MM-dd");
			cLogger.debug("StartDay="+tStartDay+"; EndDay="+tEndDay);
			
			String tSqlStr = "select max(BatchNo) from LAChargeToNxsLN where SendDate is null and TMakeDate>='"+tStartDay+"' and TMakeDate<'"+tEndDay+"'";
			String tBatchNo = new ExeSQL().getOneValue(tSqlStr);
			cLogger.info("BatchNo="+tBatchNo);
			tSqlStr = "select OutCom, sum(Charge*100), count(ContNo) from LAChargeToNxsLN where BatchNo='"+tBatchNo+"' group by OutCom";
			SSRS tSSRS = new ExeSQL().execSQL(tSqlStr);
			Element tDetailsEle = new Element("Details");
			long tTotalFee = 0;
			for (int i = 1; i <= tSSRS.MaxRow; i++) {
				if (tSSRS.GetText(i, 2).equals("0")) {
					continue;
				}
				Element ttCountyEle = new Element("County");
				ttCountyEle.setText(tSSRS.GetText(i, 1));
				
				Element ttCountyFeeEle = new Element("CountyFee");
				ttCountyFeeEle.setText(tSSRS.GetText(i, 2));
				
				Element ttPolicyNumEle = new Element("PolicyNum");
				ttPolicyNumEle.setText(tSSRS.GetText(i, 3));
				
				Element ttCountyDetailEle = new Element("CountyDetail");
				ttCountyDetailEle.addContent(ttCountyEle);
				ttCountyDetailEle.addContent(ttCountyFeeEle);
				ttCountyDetailEle.addContent(ttPolicyNumEle);
				
				tDetailsEle.addContent(ttCountyDetailEle);
				
				tTotalFee += Integer.parseInt(ttCountyFeeEle.getText());
			}
			
			Element tTotalFeeEle = new Element("TotalFee");
			tTotalFeeEle.setText(String.valueOf(tTotalFee));
			Element tTotalNumEle = new Element("TotalNum");
			tTotalNumEle.setText(String.valueOf(
					tDetailsEle.getChildren("CountyDetail").size()));
			
//			Element tTotalFeeEle = new Element("TotalFee");
//			tTotalFeeEle.setText(new ExeSQL().getOneValue(
//					"select sum(Charge*100) from LAChargeToNxsLN where BatchNo='"+tBatchNo+"'"));
//			Element tTotalNumEle = new Element("TotalNum");
//			tTotalNumEle.setText(String.valueOf(tSSRS.MaxRow));
			
			Element tBodyEle = new Element(Body);
			tBodyEle.addContent(tTotalFeeEle);
			tBodyEle.addContent(tTotalNumEle);
			tBodyEle.addContent(tDetailsEle);

			Element tFlagEle = new Element(Flag);
			tFlagEle.setText("0");
			Element tDescEle = new Element(Desc);
			tDescEle.setText("交易成功！");
			Element tHeadEle = new Element(Head);
			tHeadEle.addContent(tFlagEle);
			tHeadEle.addContent(tDescEle);
			
			Element tTranDataEle = new Element(TranData);
			tTranDataEle.addContent(tHeadEle);
			if (tTotalFee == 0) {
				tDescEle.setText("未产生手续费信息！");
			} else 
				tTranDataEle.addContent(tBodyEle);
			mOutXmlDoc = new Document(tTranDataEle);
			
			//置上发送给农信社的时间
			tSqlStr = "update LAChargeToNxsLN set SendDate='"+DateUtil.getCur10Date()+"', SendTime='"+DateUtil.getCur8Time()+"' where BatchNo='"+tBatchNo+"'";
			if (!new ExeSQL().execUpdateSQL(tSqlStr)) {
				cLogger.warn("写入发送时间异常！");
			}
			
			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			Element mFlag = new Element(Flag);
			mFlag.setText("1");
			Element mDesc = new Element(Desc);
			mDesc.setText(ex.getMessage());
			Element mHead = new Element(Head);
			mHead.addContent(mFlag);
			mHead.addContent(mDesc);
			Element mTranData = new Element(TranData);
			mTranData.addContent(mHead);

			mOutXmlDoc = new Document(mTranData);
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"	+mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out ProcedureFee.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into ProcedureFee.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mHead = mTranData.getChild(Head);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mHead.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mHead.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mHead.getChildText(NodeNo));
		mLKTransStatusDB.setBankOperator(mHead.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mHead.getChildText(TranNo));
		mLKTransStatusDB.setFuncFlag(mHead.getChildText(FuncFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out ProcedureFee.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
