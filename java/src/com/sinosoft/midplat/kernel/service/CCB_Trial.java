package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.tb.TBCalculate;
import com.sinosoft.lis.tb.TBCalculateTable;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class CCB_Trial extends ServiceImpl  {

	public CCB_Trial(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_Trial.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
			mLKTransStatusDB.getBankCode(),
			mLKTransStatusDB.getBankBranch(),
			mLKTransStatusDB.getBankNode());

			LCContSchema tLCContSchema = new LCContSchema();
			//传入值
			tLCContSchema.setManageCom(mGlobalInput.ManageCom);
			tLCContSchema.setSaleChnl("04");
			tLCContSchema.setAgentCom(mGlobalInput.AgentCom);
			//默认值
			tLCContSchema.setPrtNo("SS000000001");
			tLCContSchema.setAgentCode("1101000001");
			tLCContSchema.setPolApplyDate("2014-08-28");
			tLCContSchema.setContType("1");
			tLCContSchema.setPolType("0");
			tLCContSchema.setCValiDate("2014-01-01");
			tLCContSchema.setCInValiDate("2015-01-01");
			
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			tLCInsuredSchema.setSex(mLCCont.getChildText("Sex"));
			//银行未传职业 默认是第一职业
			tLCInsuredSchema.setOccupationType("1");
			
			//默认值
			tLCInsuredSchema.setPrtNo("SS000000001");
			tLCInsuredSchema.setName("试算人");
			tLCInsuredSchema.setBirthday("1985-05-05");
			tLCInsuredSchema.setIDType("4");
			tLCInsuredSchema.setIDNo("111111");
			tLCInsuredSchema.setOccupationCode("");
			tLCInsuredSchema.setOccupationType("1");
			
			LCPolSet tLCPolSet = new LCPolSet();
			LCPolSchema tLCPolSchema = new LCPolSchema();
			tLCPolSchema.setRiskCode(mLCCont.getChildText("RiskCode"));
			tLCPolSchema.setAmnt(mLCCont.getChildText("Amnt"));
			tLCPolSchema.setPrem(mLCCont.getChildText("Prem"));
			tLCPolSchema.setCopys(mLCCont.getChildText("Mult"));
			tLCPolSchema.setMult(mLCCont.getChildText("Mult"));
			tLCPolSchema.setInsuredAppAge(mLCCont.getChildText("Age"));
			tLCPolSchema.setInsuYear(mLCCont.getChildText("InsuYear"));
			tLCPolSchema.setInsuYearFlag(mLCCont.getChildText("InsuYearFlag"));
			tLCPolSchema.setPayEndYearFlag(mLCCont.getChildText("PayEndYearFlag"));
			tLCPolSchema.setPayEndYear(mLCCont.getChildText("PayEndYear"));
			tLCPolSchema.setEndDate(mLCCont.getChildText("EndDate"));
			tLCPolSchema.setInsuredSex(mLCCont.getChildText("Sex"));
			//默认值
			tLCPolSchema.setPrtNo("SS000000001");

			tLCPolSet.add(tLCPolSchema);
			VData tVData = new VData();
			tVData.add(tLCContSchema);
		    tVData.add(tLCInsuredSchema);
		    tVData.add(tLCPolSet);

		    TBCalculate mTBCalculate = new TBCalculate();
		    TBCalculateTable mTBCalculateTable = mTBCalculate.deal(tVData);
		    
		    if(mTBCalculateTable.getErrorState().equals("01")) {
		    	throw new MidplatException(mTBCalculateTable.getErrorStateInfo());
		    }else {
		    	mOutXmlDoc = new Document(new Element("TranData"));
		    	Element mTranData = mOutXmlDoc.getRootElement();
		    	
				Element mFlag = new Element("Flag");
				Element mDesc = new Element("Desc");
				Element mRetData = new Element("RetData");
				mFlag.setText("1");
				mDesc.setText("交易成功！");
				
				mRetData.addContent(mFlag);
				mRetData.addContent(mDesc);
				
				Element pLCCont =  new Element("LCCont");
				Element pAmnt =  new Element("Amnt");
				pAmnt.setText(mTBCalculateTable.getAmnt());
				Element pPrem =  new Element("Prem");
				pPrem.setText(mTBCalculateTable.getPrem());
				
				pLCCont.addContent(pAmnt);
				pLCCont.addContent(pPrem);
				
				mTranData.addContent(mRetData);
				mTranData.addContent(pLCCont);
		    }

			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_Trial.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_Trial.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_Trial.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
