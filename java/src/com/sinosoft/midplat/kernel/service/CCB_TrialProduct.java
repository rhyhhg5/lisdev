package com.sinosoft.midplat.kernel.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CCB_TrialProduct extends ServiceImpl  {
	public String BatchNo="";//批次号
	public String MsgType="";//报文类型
	public String Operator="";//操作者
	public double mPrem=0;//保费
	public double mAmnt=0;//保额
	public String errorFlag;
	public String errorInfo;
	
	
	public CCB_TrialProduct(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_TrialProduct.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BankCode"),
					pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("ZoneNo"),
					pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BrNo"));

			LCContSchema tLCContSchema = new LCContSchema();
			//传入值
			tLCContSchema.setManageCom(mGlobalInput.ManageCom);
			tLCContSchema.setSaleChnl("04");
			tLCContSchema.setAgentCom(mGlobalInput.AgentCom);
			//默认值
			
			Element mRisk = mLCCont.getChild(Risks).getChild(Risk);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date curDate = new Date(); 
			
			tLCContSchema.setPolApplyDate(sdf.format(curDate));
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(curDate);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			Date cvalidate = calendar.getTime(); 
			calendar.add(Calendar.YEAR, 1);
			Date cinvalidate = calendar.getTime();
			tLCContSchema.setPayIntv(mRisk.getChildText("PayIntv"));
			tLCContSchema.setPrtNo("SS000000001");
			tLCContSchema.setAgentCode("1101000001");
			tLCContSchema.setContType("1");
			tLCContSchema.setPolType("0");
			tLCContSchema.setCValiDate(sdf.format(cvalidate));
			tLCContSchema.setCInValiDate(sdf.format(cinvalidate));
			
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			tLCInsuredSchema.setSex(mLCCont.getChildText("InsuSex"));
			
			String mInsuBirthday = mLCCont.getChildText("InsuBirthday");
			String Year = mInsuBirthday.substring(0,4) ;
			String Month = mInsuBirthday.substring(4,6) ;
			String Day = mInsuBirthday.substring(6,8) ;
			//默认值
			tLCInsuredSchema.setInsuredNo("100000001");
			tLCInsuredSchema.setPrtNo("SS000000001");
			tLCInsuredSchema.setName("试算人");
			tLCInsuredSchema.setBirthday(Year+"-"+Month+"-"+Day);
			tLCInsuredSchema.setIDType("4");
			tLCInsuredSchema.setIDNo("111111");
			tLCInsuredSchema.setOccupationCode(mLCCont.getChildText("InsuJobCode"));
			String mSQL = "select occupationtype from ldoccupation where OccupationCode='" + mLCCont.getChildText("InsuJobCode") + "' with ur";
			String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
			if ((null==mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
				throw new MidplatException("未查到职业类别！职业代码为：" + mLCCont.getChildText("InsuJobCode"));
			}
			tLCInsuredSchema.setOccupationType(mOccupationTypeStr);
			
			String mMainRiskCode = mRisk.getChildText("MainRiskCode");
			mSQL = "select RiskWrapCode from LDRiskWrap where RiskCode='" + mMainRiskCode + "' with ur";
			
			LDRiskDutyWrapDB mLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
			//网银和单独套餐有冲突需要解决! 
			if(mMainRiskCode.equals("JZJ001")
					||mMainRiskCode.equals("JZJ002")
					||mMainRiskCode.equals("JZJ003")
					||mMainRiskCode.equals("JZJ004")
					){	//工行网银渠道传进来的是套餐编码 故在此做判断
				mLDRiskDutyWrapDB.setRiskWrapCode(mMainRiskCode);//注意:网银定义的是套餐编码
			}else if(mMainRiskCode.equals("333501")){	//单独配置百万套餐 区别于网银 注意:其它网银是否也需要区别于套餐
				mLDRiskDutyWrapDB.setRiskWrapCode("WR0280");
			}else if(mMainRiskCode.equals("232101")){//北肿防癌管家个人疾病保险（A款）投保规则（银保渠道专用）
				mLDRiskDutyWrapDB.setRiskWrapCode("WR0299");
			}
			else if(mMainRiskCode.equals("511101")){ //福佑相伴借款人（B款）意外伤害保险
				mLDRiskDutyWrapDB.setRiskWrapCode("XBT004");
			}
			else {
				mLDRiskDutyWrapDB.setRiskWrapCode(new ExeSQL().getOneValue(mSQL));
			}
			LDRiskDutyWrapSet mLDRiskDutyWrapSet = mLDRiskDutyWrapDB.query();
			LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet(); 
			for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
				LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
				LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
				tLCRiskDutyWrapSchema.setRiskWrapCode(tLDRiskDutyWrapSchema.getRiskWrapCode());
				tLCRiskDutyWrapSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
				tLCRiskDutyWrapSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());
				tLCRiskDutyWrapSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());
				tLCRiskDutyWrapSchema.setCalFactorType(tLDRiskDutyWrapSchema.getCalFactorType());
				tLCRiskDutyWrapSchema.setCalFactorValue(
						mRisk.getChildText(tLDRiskDutyWrapSchema.getCalFactor()));
				
				mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
			}
			
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("ContType", "1");
			tTransferData.setNameAndValue("mMsgType", "WX0001");
			tTransferData.setNameAndValue("Bonusgetmode", mRisk.getChildText("BonusGetMode"));
			
			VData tVData = new VData();
			tVData.add(tLCContSchema);
		    tVData.add(tLCInsuredSchema);
		    tVData.add(tTransferData);
		    tVData.add(mLCRiskDutyWrapSet);
		    
		    mOutXmlDoc = deal(tVData);
		    
		    if("01".equals(errorFlag)) {
		    	throw new MidplatException(errorInfo);
		    }
		    
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_TrialProduct.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_Trial.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_Trial.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public Document deal(VData tVData){
		System.out.println("开始试算业务");
		Document mOutXml = null;
		try{
			
			GlobalInput tGlobalInput = new GlobalInput();
			tGlobalInput.Operator = "001";
			tGlobalInput.ManageCom = "86110000";
						
			tVData.add(tGlobalInput);
			
			BriefSingleContInputBL bl = new BriefSingleContInputBL();
	        MMap tMMap3 = bl.getSubmitRiskMap(tVData, "INSERT||CONTINSURED");
	        CErrors tErrors = bl.mErrors;
	        
	        if(tErrors.getErrorCount() != 0){
	        	errorFlag = "01";
	        	errorInfo = tErrors.getLastError();
	        }else{
	        	LCContSchema tLCContSchema = (LCContSchema)tMMap3.getObjectByObjectName("LCContSchema", 0);
		        LCPolSet cLCPolSet = (LCPolSet)tMMap3.getObjectByObjectName("LCPolSet", 0);
		        
		        mOutXml = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		        Element mTrandata = mOutXml.getRootElement();
		        Element mLCCont = new Element("LCCont");
		        Element mRisks = new Element("Risks");
		        Element mPayIntv = new Element("PayIntv");
		        mPayIntv.setText(tLCContSchema.getPayIntv()+"");
		        Element mTotalPrem = new Element("TotalPrem");
		        mTotalPrem.setText(tLCContSchema.getPrem()+"");
		        Element tMainRiskCode = new Element("MainRiskCode");
		        Element tPayEndYear = new Element("PayEndYear");
		        String mMainRiskCode = "";
		        String mPayEndYear = "";
		        for(int i=0;i<cLCPolSet.size();i++){
		        	LCPolSchema tLCPolSchema = cLCPolSet.get(i+1);
		        	if("1".equals(tLCPolSchema.getRiskSeqNo()) || "01".equals(tLCPolSchema.getRiskSeqNo())){
		        		mMainRiskCode = tLCPolSchema.getRiskCode();
		        		mPayEndYear = tLCPolSchema.getPayEndYear()+"";
		        	}
		        	Element mRisk = new Element("Risk");
		        	Element mRiskSeqNo = new Element("RiakSeqNo");
		        	mRiskSeqNo.setText(tLCPolSchema.getRiskSeqNo());
		        	Element mRiskCode = new Element("RiskCode");
		        	Element ttMainRiskCode = new Element("MainRiskCode");
		        	mRiskCode.setText(tLCPolSchema.getRiskCode());
		     
		        	String sql = "select riskname from lmrisk where riskcode = '"+tLCPolSchema.getRiskCode()+"'";
		        	String riskName = new ExeSQL().getOneValue(sql);
		        	Element mRiskName = new Element("RiskName");
		        	mRiskName.setText(riskName);
		        	Element tPrem = new Element("Prem");
		        	tPrem.setText(tLCPolSchema.getPrem()+"");
		        	Element tAmnt = new Element("Amnt");
		        	tAmnt.setText(tLCPolSchema.getAmnt()+"");
		        	Element tMult = new Element("Mult");
		        	tMult.setText(tLCPolSchema.getMult()+"");
		        	mRisk.addContent(mRiskSeqNo);
		        	mRisk.addContent(mRiskCode);
		        	mRisk.addContent(ttMainRiskCode);
		        	mRisk.addContent(mRiskName);
		        	mRisk.addContent(tPrem);
		        	mRisk.addContent(tAmnt);
		        	mRisk.addContent(tMult);
		        	mRisks.addContent(mRisk);
		        }
		        tMainRiskCode.setText(mMainRiskCode);
		        tPayEndYear.setText(mPayEndYear);
		        
		        mLCCont.addContent(mPayIntv);
		        mLCCont.addContent(mTotalPrem);
		        mLCCont.addContent(tMainRiskCode);
		        mLCCont.addContent(tPayEndYear);
		        mLCCont.addContent(mRisks);
		        mTrandata.addContent(mLCCont);
	        }
	        
	        return mOutXml;
	        
		}catch(Exception ex){
			return null;
		}
	}
	
}
