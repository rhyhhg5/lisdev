package com.sinosoft.midplat.kernel.service;

/**
 * 建行双录对接保信准备工作
 */
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import org.dom4j.io.SAXReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.CiitcTaskDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.TransTaskZBXSLDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CCB_ShuangLuBL extends ServiceImpl {

	// 得到系统当前时间
	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

	// 需要插入Ciitc_Task和transtask_zbx_sl字段信息
	private String tTaskId = null;
	private String tPrtno = null;
	private String tCounterID = null;
	private String tFileName = null;
	private String md5 = "";// 文件的MD5值
	private String tBrNo = null;// 网点编码
	private String tSubBankCode = null;// 分行编码
	private String tProductName = null;// 产品名称
	private String tProductCode = null;// 产品代码
	private String tBusinessNo = null;// 业务识别号 保单号
	private String tFileSize = null;
	private String tCustomerName = null;
	private String tCustomerCardType = null;
	private String tCustomerCardNo = null;
	private String tCustomerBirthDay = null;
	private String tPaymentTerm = null;
	private String tFlag = null;

	private Element mBaseInfo = null;

	public CCB_ShuangLuBL(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_YBTNoRealTimeUWBalanceBL.service()...");
		LKTransStatusDB mLKTransStatusDB = null;

		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild(BaseInfo)
				.clone();
		CiitcTaskDB mCiitcTaskDB = null;
		ExeSQL es = new ExeSQL();
		SSRS ssrs = new SSRS();
		TransTaskZBXSLDB mTransTaskZBXSLDB = null;

		try {
//			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mGlobalInput = YbtSufUtil.getGlobalInput(mBaseInfo
					.getChildText(BankCode), mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));

			Element mDetails = pInXmlDoc.getRootElement()
					.getChild("ChkDetails");
			tFileName = mDetails.getChild("ChkDetail").getChildText("FileName");
			tTaskId = getOrderIdByUUId();
			String tTransNo = mDetails.getChild("ChkDetail").getChildText(
					"TransNo");
			String tSql = "select prtno from lktransstatus where transno = '"
					+ tTransNo + "' with ur";
			tPrtno = es.getOneValue(tSql);
			tSql = "select bankbranch,bankoperator,riskcode,polno from lktransstatus where prtno = '"
					+ tPrtno + "' with ur";
			ssrs = es.execSQL(tSql);
			if (ssrs.getMaxRow() > 0) {
				tBrNo = ssrs.GetText(1, 1);
				tCounterID = ssrs.GetText(1, 2);
				tProductCode = ssrs.GetText(1, 3);
				tBusinessNo = ssrs.GetText(1, 4);
			}
			md5 = mDetails.getChild("ChkDetail").getChildText("FileMD5");
			tFileSize = mDetails.getChild("ChkDetail").getChildText("FileSize");
			tFlag = mDetails.getChild("ChkDetail").getChildText("Flag");

			tSql = "select outcomcode from ldcom where comcode in (select managecom from lktransstatus where prtno = '"
					+ tPrtno + "') with ur";
			tSubBankCode = es.getOneValue(tSql);

			tSql = "select riskname from lmrisk where riskcode in (select riskcode from lktransstatus where prtno = '"
					+ tPrtno + "') with ur";
			if (null == es.getOneValue(tSql) || "".equals(es.getOneValue(tSql))) {
				tSql = "select riskwrapname from ldriskwrap where riskwrapcode in (select riskcode from lktransstatus where prtno = '"
						+ tPrtno + "') with ur";
				tProductName = es.getOneValue(tSql);
			} else {
				tProductName = es.getOneValue(tSql);
			}

			tSql = "select appntname,appntidtype,appntidno,appntbirthday,payintv from lccont where prtno = '"
					+ tPrtno + "' with ur";
			ssrs = es.execSQL(tSql);
			if (ssrs.getMaxRow() == 1) {
				tCustomerName = ssrs.GetText(1, 1);
				tCustomerCardType = StateCode.getcertiType(ssrs.GetText(1, 2));
				tCustomerCardNo = ssrs.GetText(1, 3);
				tCustomerBirthDay = ssrs.GetText(1, 4);
				tPaymentTerm = ssrs.GetText(1, 5);
			}

			mCiitcTaskDB = insertCiitcTask(pInXmlDoc);
			mTransTaskZBXSLDB = insertTransTaskZBXSL(pInXmlDoc);
			mOutXmlDoc = deal(tPrtno); // 数据处理，组织返回标准报文(zenmefanhui)

		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", e);

			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");// 空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}
				mLKTransStatusDB.setDescr(tDesr);
			}
			Element mFlag = new Element("Flag");
			Element mDesc = new Element("Desc");
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out CCB_YBTNoRealTimeUWBalanceBL.service()...");
		mOutXmlDoc.getRootElement().addContent(mBaseInfo);
		return mOutXmlDoc;
	}

	public Document deal(String prtno) throws MidplatException {
		cLogger.info("Into CCB_YBTNoRealTimeUWBalanceBL.deal()...");

		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);

		mFlag.setText("0");
		mDesc.setText("交易成功");

		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);

		Element mTranData = new Element("TranData");
		mTranData.addContent(mRetData);
		cLogger.info("Out CCB_YBTNoRealTimeUWBalanceBL.deal()...");
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pStdXml)
			throws MidplatException {

		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB
				.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransCode(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB
				.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		return mLKTransStatusDB;
	}

	/**
	 * CIITC_TASK上报任务信息表 出入数据对接双录后置机
	 * 
	 * @param pInXmlDoc
	 * @return
	 * @throws MidplatException
	 */
	private CiitcTaskDB insertCiitcTask(Document pInXmlDoc)
			throws MidplatException {
		CiitcTaskDB mCiitcTaskDB = new CiitcTaskDB();
		mCiitcTaskDB.setTranNo(tTaskId);// 流水号
		mCiitcTaskDB.setProposalNo(tPrtno);// 投保单号 业务识别号
		mCiitcTaskDB.setCounterId(tCounterID);// 银行柜员（客户经理）工号
		mCiitcTaskDB.setBankCode(StateCode.JHBANKCODE);// 银行编码
		mCiitcTaskDB.setSubBankCode(tSubBankCode);// 分行编码
		mCiitcTaskDB.setNodeBankCode(tBrNo);// 网点编码
		mCiitcTaskDB.setInsurerCode(StateCode.INSURERCODE);// 保险公司代码（总公司）
		mCiitcTaskDB.setSubInsurerCode(StateCode.SUBINSURERCODE);// 公司代码（省级分公司）（人保与总公司一直）
		mCiitcTaskDB.setProductName(tProductName);// 产品名称
		mCiitcTaskDB.setProductCode(tProductCode);// 产品代码
		String tSql = "select count(businessno) from ciitc_task where businessno like '"
				+ tBusinessNo + "%' with ur";
		int count = Integer.parseInt(new ExeSQL().getOneValue(tSql));
		if (count > 0) {
			tBusinessNo = tBusinessNo + "-" + count;
		}

		mCiitcTaskDB.setBusinessNo(tBusinessNo);// 业务识别号 --保单号
		mCiitcTaskDB.setCustomerName(tCustomerName);// 客户姓名
		mCiitcTaskDB.setCustomerCardType(tCustomerCardType);// 客户证件类型
		mCiitcTaskDB.setCustomerCardNo(tCustomerCardNo);// 客户证件号码
		mCiitcTaskDB.setCustomerBirthDay(DateUtil.date10to8(tCustomerBirthDay));// 客户出生日期
		mCiitcTaskDB.setVideoName(tFileName
				.substring(0, tFileName.indexOf(".")));// 视频文件名
		mCiitcTaskDB.setVideoSize(tFileSize);// 视频大小
		mCiitcTaskDB.setVideoType(tFileName.substring(
				tFileName.indexOf(".") + 1, tFileName.length()));// 视频格式类型
		mCiitcTaskDB.setBusinessSeriaNo(tTaskId);// 业务流水号
		mCiitcTaskDB.setBatchNo(tTaskId);// 批次号
		mCiitcTaskDB.setContentMD5(md5);
		System.out.println("md5============" + md5);
		mCiitcTaskDB.setTaskSource(StateCode.BANKTASKSOURCE);// 业务来源
		mCiitcTaskDB.setPaymentTerm(tPaymentTerm);// 缴费方式

		if (!mCiitcTaskDB.insert()) {
			cLogger.error(mCiitcTaskDB.mErrors.getFirstError());
			throw new MidplatException("插入上报任务日志失败！");
		}
		return null;
	}

	/**
	 * TRANSTASK_ZBX_SL后置机批处理的任务池表
	 * 
	 * @param pInXmlDoc
	 * @return
	 * @throws MidplatException
	 */
	private TransTaskZBXSLDB insertTransTaskZBXSL(Document pInXmlDoc)
			throws MidplatException {
		TransTaskZBXSLDB mTransTaskZBXSLDB = new TransTaskZBXSLDB();

		int i = (int) System.currentTimeMillis();
		if (i < 0) {
			i = -i;
		}

		mTransTaskZBXSLDB.setCiitcTaskNo(tTaskId);
		mTransTaskZBXSLDB.setId(i);
		mTransTaskZBXSLDB.setBatchNo(tTaskId);
		mTransTaskZBXSLDB.setProposalNo(tPrtno);
		mTransTaskZBXSLDB.setFileName(tFileName);
		mTransTaskZBXSLDB.setServerIp("10.253.33.104");//建行测试地址更新是需更改  gaojinfu
		mTransTaskZBXSLDB.setServerUser("Administrator");//建行测试用户
		mTransTaskZBXSLDB.setServerPassWord("Picc1234");//建行测试密码
		mTransTaskZBXSLDB.setServerPort("21");
		mTransTaskZBXSLDB.setLocalFilePaths("E:/dobfile");
		mTransTaskZBXSLDB.setRemoteFilePaths("/shuanglu/" + tFileName);
		mTransTaskZBXSLDB.setSourceChannel("1");
		mTransTaskZBXSLDB.setPriority("1");
		mTransTaskZBXSLDB.setFileMD5(md5);
		// mTransTaskZBXSLDB.setCookie("");
		mTransTaskZBXSLDB.setTaskId(String
				.valueOf((int) (Math.random() * 10000)));// 请求中保信返回的本次任务唯一ID
		mTransTaskZBXSLDB.setReqMessage("");
		mTransTaskZBXSLDB.setFlag(tFlag);
		mTransTaskZBXSLDB.setStatus(StateCode.TRSTASK_STATUS_CHALL_0000);
		mTransTaskZBXSLDB.setTransMSG("登陆挑战请求处理成功待上报");
		mTransTaskZBXSLDB.setFileMakeDate(mCurrentDate);
		mTransTaskZBXSLDB.setMakeDate(mCurrentDate);
		mTransTaskZBXSLDB.setModifyDate(mCurrentDate);

		if (!mTransTaskZBXSLDB.insert()) {
			cLogger.error(mTransTaskZBXSLDB.mErrors.getFirstError());
			throw new MidplatException("插入上报任务日志失败！");
		}
		return null;
	}

	public static String getOrderIdByUUId() {
		int machineId = 1;// 最大支持1-9个集群机器部署
		int hashCodeV = UUID.randomUUID().toString().hashCode();
		if (hashCodeV < 0) {// 有可能是负数
			hashCodeV = -hashCodeV;
		}
		// 0 代表前面补充0
		// 4 代表长度为4
		// d 代表参数为正数型
		return machineId + String.valueOf(hashCodeV);
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "F:\\bankMassage\\ccb\\shuanglutest.xml";
		String mOutFile = "D:/out.xml";

		String elpath = "D:\\Program Files\\myeclipseworkspace\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		SAXReader saxreader = new SAXReader();
		Element root = c.getRootElement().getChild("business");

		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));

		Document mOutXmlDoc = new CCB_ShuangLuBL(root).service(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		//		
		// System.out.println("成功结束！");
		// AIO_NewCont aio = new AIO_NewCont(null);
		// String ageDate = "19900101";
		// System.out.println(aio.getAppntAge(ageDate));
	}

}
