package com.sinosoft.midplat.kernel.service;

import java.io.FileOutputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class BatNrt extends ServiceImpl {
    public BatNrt(Element pThisBusiConf) {
        super(pThisBusiConf);
    }

    public Document service(Document pInXmlDoc) {
        cLogger.info("Into BatNrt.service()...");
        Document mOutXmlDoc = null;
        LKTransStatusDB mLKTransStatusDB = null;
        GlobalInput mGlobalInput = null;

        try {
            mLKTransStatusDB = insertTransLog(pInXmlDoc);

            mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
                    .getBankCode(), mLKTransStatusDB.getBankBranch(),
                    mLKTransStatusDB.getBankNode());

            /**
             * 调用核心非实时取盘接口
             */
            String mBankCode = mLKTransStatusDB.getBankCode();
            String mManageCom = mGlobalInput.ManageCom.substring(0, 2);
            String mSql = "select bankcode from ldbank where operator = 'ybt' and agentcode = '"
                    + mBankCode
                    + "' and comcode = '"
                    + mManageCom
                    + "' with ur";
            mBankCode = new ExeSQL().getOneValue(mSql);
            cLogger.info("银行编码:" + mBankCode + ",非实时取盘管理机构：" + mManageCom);
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("ComCode", mManageCom);
            mTransferData.setNameAndValue("BankCode", mBankCode);

            Document mDoc = createStdXml();

            // 存储核心接口报文
            FileOutputStream mFos = new FileOutputStream(cThisBusiConf
                    .getChildText(path)
                    + "REQ_" + mLKTransStatusDB.getBankAcc() + ".xml");
            JdomUtil.output(mDoc, mFos);
            mFos.close();

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
            List mTranDataList = mDoc.getRootElement().getChildren();
            for (int i = 0; i < mTranDataList.size(); i++) {
                mOutXmlDoc.getRootElement().addContent(
                        (Element) ((Element) mTranDataList.get(i)).clone());
            }

            mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
            mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        } catch (Exception ex) {
            cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

            if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
                mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
                mLKTransStatusDB.setDescr(ex.getMessage());
            }

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
        }

        if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
            if (null != mGlobalInput) {
                mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
            }
            mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
            if (!mLKTransStatusDB.update()) {
                cLogger.error("更新日志信息失败！"
                        + mLKTransStatusDB.mErrors.getFirstError());
            }
        }

        cLogger.info("Out BatNrt.service()!");
        return mOutXmlDoc;
    }

    public LKTransStatusDB insertTransLog(Document pXmlDoc)
            throws MidplatException {
        cLogger.info("Into BatNrt.insertTransLog()...");

        Element mTranData = pXmlDoc.getRootElement();
        Element mBaseInfo = mTranData.getChild(BaseInfo);

        String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
        String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

        LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
        mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
        mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
        mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
        mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
        mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
        mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
        mLKTransStatusDB.setBankAcc(mBaseInfo.getChildText("FileName")); // 借用做文件名称
        mLKTransStatusDB.setTempFeeNo(mBaseInfo.getChildText("DealType")); // 借用做业务类型
        mLKTransStatusDB.setTransDate(mCurrentDate);
        mLKTransStatusDB.setTransTime(mCurrentTime);
        mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        mLKTransStatusDB.setMakeDate(mCurrentDate);
        mLKTransStatusDB.setMakeTime(mCurrentTime);
        mLKTransStatusDB.setModifyDate(mCurrentDate);
        mLKTransStatusDB.setModifyTime(mCurrentTime);

        if (!mLKTransStatusDB.insert()) {
            cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
            throw new MidplatException("插入日志失败！");
        }

        cLogger.info("Out BatNrt.insertTransLog()!");
        return mLKTransStatusDB;
    }

    public Document createStdXml() throws MidplatException {
        cLogger.info("Into BatNrt.createStdXml()...");

        int mTotalNum = 0;
        cLogger.info("此次非实时总笔数为:" + mTotalNum);
        Element mTranData = new Element(TranData);
        Element tTotalNum = new Element("TotalNum");
        tTotalNum.setText(String.valueOf(mTotalNum));
        Element tTotalSum = new Element("TotalSum");
        tTotalSum.setText("0.00");
        mTranData.addContent(tTotalNum);
        mTranData.addContent(tTotalSum);

        cLogger.info("Out BatNrt.createStdXml()!");
        return new Document(mTranData);
    }
}
