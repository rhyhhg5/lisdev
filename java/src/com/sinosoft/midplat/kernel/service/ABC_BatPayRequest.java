package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

public class ABC_BatPayRequest extends SimpService {
	public ABC_BatPayRequest(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_BatPayRequest.service()...");
		
		cLogger.info("农行批量代付置盘交易处理！该交易处理和代收交易一致，直接调用代收处理程序...");
		Document mOutXmlDoc = 
			new ABC_BatIncomeRequest(cThisBusiConf).service(pInXmlDoc);
		
		cLogger.info("Out ABC_BatPayRequest.service()!");
		return mOutXmlDoc;
	}
}