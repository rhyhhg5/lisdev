/**
 * 试算交易。
 * 注意：试算不是一个单独的交易，试算的交易流水号可以重用；
 * 本类在处理时，不插入银保通日志，交易完成后，无论成功与否都删除所有交易数据。
 */

package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKElectronContSchema;
import com.sinosoft.lis.vschema.LKElectronContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class UpdateEpolicyData extends ServiceImpl {
	
	public UpdateEpolicyData(Element pThisBusiConf){
		super(pThisBusiConf);
	}
	
	/* (non-Javadoc)
	 * @see com.sinosoft.midplat.kernel.service.ServiceImpl#service(org.jdom.Document)
	 */
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into Trial.service()...");
		Document mOutXmlDoc = null;		
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element LCCont = mTranData.getChild("LCCont");   //基本执行sql节点
		List lis_Cont =  LCCont.getChildren("ContDetail");
		
	    //返回报文
			try
			{    
				LKElectronContSchema  mLKElectronContSchema = new LKElectronContSchema();
				LKElectronContSet   mLKElectronContSet = new LKElectronContSet();
				ExeSQL exe = new  ExeSQL();
				for(int i =0;i<lis_Cont.size();i++){
					Element in_ele = (Element)lis_Cont.get(i);
					String sql = "select 1 from lkelectroncont where contno = '"+in_ele.getChildText("ContNo")+"'";
					if("1".equals(exe.getOneValue(sql))){
						sql = "update lkelectroncont set temp1 = '"+in_ele.getChildText("ContState")+"' where contno = '"+in_ele.getChildText("ContNo")+"'";
						if(!exe.execUpdateSQL(sql)){
							cLogger.error("电子保单状态修改失败，保单号：" + in_ele.getChildText("ContNo"));
							continue;
						}
					} else {
						sql = "select codename  from ldcode where codetype ='banknum' and code in (select bankcode  from lktransstatus where polno ='"+in_ele.getChildTextTrim("ContNo")+"' and funcflag='01')";
						mLKElectronContSchema.setBankName(exe.getOneValue(sql));
						mLKElectronContSchema.setTransNo(DateUtil.getCurDateTime());
						mLKElectronContSchema.setContNo(in_ele.getChildText("ContNo"));
						mLKElectronContSchema.setContSendDate(DateUtil.getCur10Date());
						mLKElectronContSchema.setTemp1(in_ele.getChildText("ContState"));
						mLKElectronContSet.add(mLKElectronContSchema);
					}
				}
				 //LKElectronContDBDBSet
				MMap tMMap = new MMap();
				VData tVData = new VData();
				tMMap.put(mLKElectronContSet, "INSERT");
				tVData.add(tMMap);
				PubSubmit tPs = new PubSubmit();
				Element mFlag= new Element(Flag);
				Element mDesc= new Element(Desc);
				if (!tPs.submitData(tVData, "")) {
					mFlag.setText("1");
					mDesc.setText("执行插入信息失败!");
				}else {
					mFlag.setText("0");
					mDesc.setText("交易成功！");
				}
				 
				  cLogger.error(cThisBusiConf.getChildText(name)+"交易成功！");
					mOutXmlDoc = YbtSufUtil.getSimpOutXml("0","交易成功");
				
			} catch (Exception ex) 
			{
				cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
				mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
			}
			
			return  mOutXmlDoc;
	}
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "C:\\Users\\Administrator\\Desktop\\编辑4.TXT";
		String mOutFile = "D:/out.xml";
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new UpdateEpolicyData(null).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}

