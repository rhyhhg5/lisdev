/**
 * 山东农信社自动冲正
 * 此类本身不做任何业务处理，仅插入一条交易日志。
 * 并向日志表bak4中加入标志数据.
 */

package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class SDNXS_AutoWriteOff extends ServiceImpl {
	public SDNXS_AutoWriteOff(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into SDNXS_AutoWriteOff.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			//不能区分管理员授权的自动冲正和余额不足情况下的自动冲正,特更新此标志位,yinjiajia
			String mSQL = "update LKTransStatus set bak4 = '1' where rcode = '1' and "
						+"ProposalNo = '"+mLKTransStatusDB.getProposalNo()+"' and "
						+"BankCode = '"+mLKTransStatusDB.getBankCode()+"' and "
						+"FuncFlag = '01' and "
						+"TransDate = '"+DateUtil.getCurDate("yyyy-MM-dd")+"' and "
						+"BankNode = '"+mLKTransStatusDB.getBankNode()+"' and "
						+"BankOperator = '"+mLKTransStatusDB.getBankOperator()+"' and "
						+"BankBranch = '"+mLKTransStatusDB.getBankBranch()+"' "
						+"with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if(!mExeSQL.execUpdateSQL(mSQL)){
				throw new MidplatException("更新日志表信息失败!");
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
						
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (MidplatException ex) {
			cLogger.error("山东农信社自动冲正(SDNXS_AutoWriteOff)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out SDNXS_AutoWriteOff.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into SDNXS_AutoWriteOff.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out SDNXS_AutoWriteOff.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
