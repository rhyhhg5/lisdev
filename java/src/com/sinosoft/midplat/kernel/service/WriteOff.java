package com.sinosoft.midplat.kernel.service;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.writeOff.YbtWriteOffBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.VData;
import org.jdom.Document;
import org.jdom.Element;

import java.text.DecimalFormat;

public class WriteOff extends ServiceImpl {
	public WriteOff(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into WriteOff.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLCCont.getChildText(ContNo));
			tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
			tLCContDB.setProposalContNo(mLCCont.getChildText(ProposalContNo));
			if (tLCContDB.getContNo().equals("")
						&& tLCContDB.getPrtNo().equals("")
						&& tLCContDB.getProposalContNo().equals("")) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if ((null == tLCContSet) || (tLCContSet.size() < 1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			String mCardFlag = tLCContSchema.getCardFlag();
			if ("9".equals(mCardFlag)
						|| "c".equals(mCardFlag)
						|| "d".equals(mCardFlag)
						|| "e".equals(mCardFlag)
						|| "f".equals(mCardFlag)
						|| ("a".equals(mCardFlag)
									&& ("77".equals(mLKTransStatusDB.getBankCode())))
			) {//LKCodeMapping
				mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			} else {//LKCodeMapping1
				mGlobalInput = YbtSufUtil.getGlobalInput1(mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			}

			if (!tLCContSchema.getAppFlag().equals("1")) {
				throw new MidplatException("该保单不处于承保状态！");
			}

			if (!tLCContSchema.getSignDate().equals(
					DateUtil.getCurDate("yyyy-MM-dd"))) {
				throw new MidplatException("非当日保单，不能冲正！");
			}

			mLKTransStatusDB.setPolNo(tLCContSchema.getContNo());
			mLKTransStatusDB.setPrtNo(tLCContSchema.getPrtNo());
			mLKTransStatusDB.setProposalNo(tLCContSchema.getProposalContNo());
			mLKTransStatusDB.setTransAmnt(-tLCContSchema.getPrem());
			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));

			LKTransStatusDB tLKTransStatusDB01 = new LKTransStatusDB();
			tLKTransStatusDB01.setPolNo(mLKTransStatusDB.getPolNo());
			tLKTransStatusDB01.setStatus("1");
			tLKTransStatusDB01.setFuncFlag("01");
			LKTransStatusSet tLKTransStatusSet01 = tLKTransStatusDB01.query();
			if (1 != tLKTransStatusSet01.size()) {
				cLogger.error("查询新契约日志异常：" + tLKTransStatusDB01.mErrors.getFirstError());
				throw new MidplatException("新契约日志异常！");
			}
			LKTransStatusSchema tLKTransStatusSchema01 = tLKTransStatusSet01.get(1);
			tLKTransStatusSchema01.setStatus("2"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
			tLKTransStatusSchema01.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));

			MMap tSubmitMMap = new MMap();
			LKTransStatusSchema tLKTransStatusSchema = mLKTransStatusDB.getSchema();
			tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
			tSubmitMMap.put(tLKTransStatusSchema01, "UPDATE");
			VData tSubmitVData = new VData();
			tSubmitVData.add(tSubmitMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tSubmitVData, "")) {
				cLogger.error(tPubSubmit.mErrors.getFirstError());
				throw new MidplatException("更新日志失败！");
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
			if (mLKTransStatusDB.getBankCode().equals("03")) { // 建行特殊处理
				cLogger.debug("为建行冲正专门组织一些特有返回信息...");
				Element ttRiskCode = new Element(RiskCode);
				ttRiskCode.setText(tLKTransStatusSchema01.getRiskCode());

				Element ttContNo = new Element(ContNo);
				ttContNo.setText(tLCContSchema.getContNo());

				Element ttAppntName = new Element(AppntName);
				ttAppntName.setText(tLCContSchema.getAppntName());

				Element ttInsuredName = new Element("InsuredName");
				ttInsuredName.setText(tLCContSchema.getInsuredName());

				Element ttPrem = new Element(Prem);
				ttPrem.setText(new DecimalFormat("0.00").format(tLCContSchema.getPrem()));

				Element ttBankAccNo = new Element(BankAccNo);
				ttBankAccNo.setText(tLCContSchema.getBankAccNo());

				Element ttLCCont = new Element(LCCont);
				ttLCCont.addContent(ttRiskCode);
				ttLCCont.addContent(ttContNo);
				ttLCCont.addContent(ttAppntName);
				ttLCCont.addContent(ttInsuredName);
				ttLCCont.addContent(ttPrem);
				ttLCCont.addContent(ttBankAccNo);

				mOutXmlDoc.getRootElement().addContent(ttLCCont);
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out WriteOff.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into WriteOff.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out WriteOff.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
