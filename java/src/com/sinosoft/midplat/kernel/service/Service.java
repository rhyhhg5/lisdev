package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;

public interface Service {
	public Document service(Document pInStdXml);
}
