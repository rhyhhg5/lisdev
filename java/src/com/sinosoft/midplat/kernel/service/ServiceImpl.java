package com.sinosoft.midplat.kernel.service;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.common.XmlTag;

public class ServiceImpl implements Service, XmlTag {
	protected final Logger cLogger = Logger.getLogger(getClass());
	
	protected final Element cThisBusiConf;
	
	public ServiceImpl(Element pThisBusiConf) {
		cThisBusiConf = pThisBusiConf;
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ServiceImpl.service()...");
		
		Element mFlag = new Element("Flag");
		mFlag.addContent("1");
		
		Element mDesc = new Element("Desc");
		mDesc.addContent("���׳ɹ���");
		
		Element mRetData = new Element("RetData");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		Element mTranData = new Element("TranData");
		mTranData.addContent(mRetData);
		
		cLogger.info("Out ServiceImpl.service()!");
		return new Document(mTranData);
	}
}
