/**
 * 山东农信社保费试算处理类。
 * 特殊情况:允许重复进行保费试算处理。
 * 
 */

package com.sinosoft.midplat.kernel.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.IdNoCheck;
//import com.sinosoft.midplat.kernel.util.CalAge;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class SDNXS_NewContInput extends ServiceImpl {
	public SDNXS_NewContInput(Element pThisConf) {
		super(pThisConf);
	}
	
	/**
	 * 计算投保人年龄
	 * */
    public  int getAppntAge (String ageDate) {
    	
    	Date now = new Date();
		String sdf = new SimpleDateFormat("yyyyMMdd").format(now);
		
		int age = 0;
		int staY = Integer.parseInt(ageDate.substring(0,4));
		int staM = Integer.parseInt(ageDate.substring(4,6));
		int staD = Integer.parseInt(ageDate.substring(6,8));
		
		int nowY = Integer.parseInt(sdf.substring(0,4));
		int nowM = Integer.parseInt(sdf.substring(4,6));
		int nowD = Integer.parseInt(sdf.substring(6,8));
		
		if(staM > nowM){
			age = nowY - staY -1;
		}else if(staM == nowM) {
			if(nowD > staD){
				age = nowY - staY ;
			}else if(nowD == staD) {
				age = nowY - staY ;
			}else if(nowD < staD) {
				age = nowY - staY -1 ;
			}
				
		}else if(staM < nowM) {
			age = nowY - staY ;
		}
		
    	return age;
    }
	
	public Document service(Document pInXmlDoc) {
		GlobalInput tGlobalInput = null;
		Document mOutXmlDoc = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mFuncFlag = mBaseInfo.getChild(FunctionFlag);
		mFuncFlag.setText("01");//此处必须要置回01,以后对账,撤单等操作会通过funcflag='01'来查询日志表里的记录
		Element mLCCont = mTranData.getChild(LCCont);

		try {
			tGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			//判断管理机构是否是山东
			if(!"8637".equals(tGlobalInput.ManageCom.substring(0, 4))){
				throw new MidplatException("非山东农信社,不能使用此交易!");
			}
			
			String tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			//------------------------投保人，被保人，受益人身份证信息校验～～2017.8.14-----------------------------------------------
			cLogger.info("身份证号校验开始");
			
			//NO1.获取投保人，被保人，受益人身份信息；
			IdNoCheck idNoCheck = new IdNoCheck();
			Element appElement = mLCCont.getChild("LCAppnt");
			String appIdtp = appElement.getChildText("AppntIDType");
			String appIdNo = appElement.getChildText("AppntIDNo");
			if ("0".equals(appIdtp) && null != appIdNo && !"".equals(appIdNo)) {
				appElement.getChild("AppntIDNo").setText(appIdNo.toUpperCase());
				String appSex = appElement.getChildText("AppntSex");
				String appBri = appElement.getChildText("AppntBirthday");
				idNoCheck.isValidatedAllIdcard(appIdNo, appBri, appSex);
				
			}
			List lisIns = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
			for (int i = 0; i < lisIns.size(); i++) {
				Element insElement = (Element) lisIns.get(i);
				String insIdtp = insElement.getChildText("IDType");
				String insIdNo = insElement.getChildText("IDNo");
				if ("0".equals(insIdtp) && null != insIdNo && !"".equals(insIdNo)) {
					insElement.getChild("IDNo").setText(insIdNo.toUpperCase());
					String insSex = insElement.getChildText("Sex");
					String insBri = insElement.getChildText("Birthday");
					System.out.println(insBri);
					idNoCheck.isValidatedAllIdcard(insIdNo, insBri, insSex);
				}
				
				String lcBnfNum = insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildText(LCBnfCount);
				System.out.println("lcBnfNum============"+lcBnfNum);
				if (!"0".equals(lcBnfNum) && null != lcBnfNum) {
					List lisBnf = insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildren("LCBnf");
					for (int j = 0; j < lisBnf.size(); j++) {
						Element bnfElement = (Element) lisBnf.get(j);
						String bnfIdtp = bnfElement.getChildText("IDType");
						String bnfIdNo = bnfElement.getChildText("IDNo");
						if ("0".equals(bnfIdtp) && null != bnfIdNo && !"".equals(bnfIdNo)) {
							bnfElement.getChild("IDNo").setText(bnfIdNo.toUpperCase());
							String bnfSex = bnfElement.getChildText("Sex");
							String bnfBri = bnfElement.getChildText("Birthday");
							idNoCheck.isValidatedAllIdcard(bnfIdNo,bnfBri , bnfSex);
						}
					}
				}else if(null != insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildren("LCBnf")){
					System.out.println("0000000000000000");
					List lisBnf = insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildren("LCBnf");
					for (int j = 0; j < lisBnf.size(); j++) {
						Element bnfElement = (Element) lisBnf.get(j);
						String bnfIdtp = bnfElement.getChildText("IDType");
						String bnfIdNo = bnfElement.getChildText("IDNo");
						if ("0".equals(bnfIdtp) && null != bnfIdNo && !"".equals(bnfIdNo)) {
							System.out.println("bnfIdNo================"+bnfIdNo);
							bnfElement.getChild("IDNo").setText(bnfIdNo.toUpperCase());
							String bnfSex = bnfElement.getChildText("Sex");
							String bnfBri = bnfElement.getChildText("Birthday");
							idNoCheck.isValidatedAllIdcard(bnfIdNo,bnfBri , bnfSex);
						}
					}
				}
			}
			cLogger.info("身份证号校验结束");
			
			//百万安行规则调整 mengshuo 2016-05-18
			if("01".equals(mBaseInfo.getChildText(BankCode)) ||"02".equals(mBaseInfo.getChildText(BankCode)) ||"03".equals(mBaseInfo.getChildText(BankCode)) ||"04".equals(mBaseInfo.getChildText(BankCode)) ||"16".equals(mBaseInfo.getChildText(BankCode)) ||"53".equals(mBaseInfo.getChildText(BankCode)) ||"57".equals(mBaseInfo.getChildText(BankCode))){
				List lisLCIns = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
				
				for(int i=0;i<lisLCIns.size();i++){
					Element LCInsured = (Element)lisLCIns.get(i);
					List lisRisk = LCInsured.getChild("Risks").getChildren("Risk");
					for(int j=0;j<lisLCIns.size();j++){
						Element Risk = (Element)lisRisk.get(i);
						if(Risk != null)
						{
							String mainRiskCode = Risk.getChildText(MainRiskCode);
							if("333501".equals(mainRiskCode)||"WX0012".equals(mainRiskCode)||"JZJ001".equals(mainRiskCode)){
								String jobCode = LCInsured.getChildText(JobCode);
								String tSalary = LCInsured.getChildText("Income");
								cLogger.info("获取被保人收入:"+tSalary);
								String amntStr = Risk.getChildText(Amnt);
								if(Double.parseDouble("100000.00") == Double.parseDouble(amntStr)){
									if("00101".equals(jobCode)||"00102".equals(jobCode)||"00103".equals(jobCode)||"00104".equals(jobCode)||"00105".equals(jobCode)||"00106".equals(jobCode)||"05803".equals(jobCode)||"06903".equals(jobCode)||"06904".equals(jobCode)){
										throw new MidplatException("离退休人员、农业人员、家庭主妇（夫）、学生只能投保5万");
									}
									if(Double.parseDouble(tSalary) < Double.parseDouble("50000")&&(!"05803".equals(jobCode))){
										throw new MidplatException("被保人年收入未达到投保10万元要求");
									}
								}
								if("06902".equals(jobCode)||"06905".equals(jobCode)||"03904".equals(jobCode)||"03704".equals(jobCode)||"03802".equals(jobCode)||"03803".equals(jobCode)||"05503".equals(jobCode)){
									throw new MidplatException("被保险人所填写的职业代码中涵盖禁止销售的职业名称，须人工审核");
								}
								if(Double.parseDouble(tSalary) < Double.parseDouble("30000")&&(!"05803".equals(jobCode))){
									throw new MidplatException("被保人年收入未达到投保要求");
								}
							}
						}
					}
				}
			}
			
			//广东地区所有银行年龄加险种期缴校验
			if(tGlobalInput.ManageCom.startsWith("8644"))
			{
				List<Element> lisRisk = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
				for(Element Risk :lisRisk){
				if(Risk != null)
				{
					String PayIntv = Risk.getChildTextTrim("PayIntv");  //获取缴费频率
					String InsuYear = Risk.getChildTextTrim("InsuYear");  //获取保险期间
					cLogger.info("获取缴费频率"+PayIntv);
					cLogger.info("获取保险期间"+InsuYear);
					
					if(!PayIntv.equals("0")){
						String appAge =  mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");  //获取投保人年龄
						System.out.println("appAge"+appAge);
//						int Age = CalAge.getAppntAge(appAge);
						int Age = this.getAppntAge(appAge);
						cLogger.info("获取投保人年龄:"+appAge);
						if(Age >60 && (Integer.parseInt(InsuYear))>1){
							throw new MidplatException("投保人年龄大于60岁,且保险期间大于1年,不通过...");
						}
					}
				}
				}
			}
			
			
			
			//山东农信社特殊要求,对同一单证号码进行重复试算
			tSQL = "select prtno from lccont where AppFlag='0' and ProposalContNo='" + mLCCont.getChildText(ProposalContNo) + "' with ur";
			String tPrtNoStr = new ExeSQL().getOneValue(tSQL);
			cLogger.debug("PrtNo = " + tPrtNoStr);
			if ((null!=tPrtNoStr) && !tPrtNoStr.equals("")) {	//同一保单合同书印刷号(ProposalContNo)，未签单
				tSQL = "select 1 from LKTransStatus where RCode='1' and FuncFlag='01' "
					+ "and BankCode='" + mBaseInfo.getChildText(BankCode) + "' "
					+ "and BankBranch='" + mBaseInfo.getChildText(ZoneNo) + "' "
					+ "and BankNode='" + mBaseInfo.getChildText(BrNo) + "' "
					+ "and TransDate='" + DateUtil.getCurDate("yyyy-MM-dd") + "' "
					+ "and ProposalNo='" + mLCCont.getChildText(ProposalContNo) + "' "
					+ "and PrtNo='" + tPrtNoStr + "' "
					+ "with ur";
				if (new ExeSQL().getOneValue(tSQL).equals("1")) {	//同一天，同一银行，同一网点
					YbtSufUtil.clearData(tPrtNoStr);//清除之前试算的数据
				}
			}
		
			/**
			 * yinjiajia
			 * 山东农信社特殊要求,在跳号使用,扣款余额不足,客户输入密码错误时,农信社报错退出,但picc这边的保单已承保
			 * 农信社要求"自动冲正"后,此单证号可以再次进行试算而不报错返回.
			 * 注:山东农信社"自动冲正"交易为虚的交易,只对日志表里funcflag为01的记录中bak4字段添加标志位"1",标识进行过冲正交易
			 * 但实际核心中此单证号对应的保单仍是承保状态.
			 * 
			 */
			//判断是否是冲正后重新试算的情况
			LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
			mLKTransStatusDB.setFuncFlag("01");
			mLKTransStatusDB.setbak4("1");
			mLKTransStatusDB.setStatus("1");
			mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
			mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
			mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
			mLKTransStatusDB.setTransDate(DateUtil.getCurDate("yyyy-MM-dd"));
			mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
			mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
			LKTransStatusSet mLKTransStatusSet = mLKTransStatusDB.query();
			if(mLKTransStatusSet.size() >= 1){
				LKTransStatusSchema mLKTransStatusSchema = mLKTransStatusSet.get(1);
				//回收单证
				tSQL = "select riskcode from lktransstatus where funcflag = '01' "
					+"and prtno = '"+mLCCont.getChildText(PrtNo)+"' "
					+"and rcode = '1' "
					+"with ur";
				String mRiskWrapCode = new ExeSQL().getOneValue(tSQL);//套餐编码
				String mCertifyCode = null;//单证编码
				String mProposalContNoNew = null;
				ExeSQL mExeSQL = new ExeSQL();
				tSQL = "select certifycode from lkcertifymapping where bankcode = '"
						+ mBaseInfo.getChildText(BankCode)+"' "
						+ "and managecom = '"+ tGlobalInput.ManageCom.substring(0, 4)+"' "
						+ "and RiskWrapCode = '"+ mRiskWrapCode+"' "
						+ "with ur";
				SSRS tSSRS = mExeSQL.execSQL(tSQL);
				
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的单证编码!");
				}
				mCertifyCode = tSSRS.GetText(1, 1);// 农信社单证编码需要从单证编码定义表中查询
				mProposalContNoNew = mLCCont.getChildText(ProposalContNo).substring(2, 12);//信保通单证操作需要去掉前两位
				if (!mLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
					new CardManage(mCertifyCode, tGlobalInput)
								.tackBack(mProposalContNoNew);		
				}
				//end of  certify take back
				
				//因为要重复进行试算,会造成有多条funcflag='01'的记录,将之前承保的记录PrtNo,ProposalNo加上后缀HM
				//以和后面进行的试算日志记录进行区分.同时需要修正funcflag和polno,以防对账的时候将此数据误认为承保成功数据.
				tSQL = "update  lktransstatus set proposalno = '"
					+mLCCont.getChildText(ProposalContNo)+"HM' , prtno = '"
					+mLCCont.getChildText(PrtNo)+"HM' , polno = '"
					+mLKTransStatusSchema.getPolNo()+"HM' , funcflag = '21' where "				
					+"funcflag = '01' and bak4 = '1' and rcode = '1' and status = '1' "
					+ "and BankCode='" + mBaseInfo.getChildText(BankCode) + "' "
					+ "and BankBranch='" + mBaseInfo.getChildText(ZoneNo) + "' "
					+ "and BankNode='" + mBaseInfo.getChildText(BrNo) + "' "
					+ "and TransDate='" + DateUtil.getCurDate("yyyy-MM-dd") + "' "
					+ "and ProposalNo='" + mLCCont.getChildText(ProposalContNo) + "' "
					+ "and PrtNo='" + mLCCont.getChildText(PrtNo) + "' ";
					
				if(!new ExeSQL().execUpdateSQL(tSQL)){
					throw new MidplatException("3类操作报错返回后重复试算更新上一次日志表信息失败!");
				}
				YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));//清除数据
				
				//删除locktable中相应的记录,否则会报300秒内不能重复提交的错误
				tSQL = "delete from LockTable where NoType = 'SC' and NoLimit = '"
					+mLCCont.getChildText(ProposalContNo)+"' ";
				if(!new ExeSQL().execUpdateSQL(tSQL)){
					throw new MidplatException("删除locktable中相应的记录失败!");
				}
			}
			
		 mOutXmlDoc = new NXS_NewContInput(cThisBusiConf).service(pInXmlDoc);
		 cLogger.info("Out SDNXS_NewContInput.service()!");
	}catch (Exception ex){
		cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
		mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
	
	return mOutXmlDoc;
	}
}
	

