package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vdb.LZCardDBSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.writeOff.YbtWriteOffBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class ProposalNoHeXiao extends ServiceImpl {
	public ProposalNoHeXiao(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ProposalNoHeXiao.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
 
		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);
 
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			List  lis_proposalno = mLCCont.getChildren("ProposalNo");
			
			Element proposalNo = null;
			String   propo = "";
			
			LCContDB tLCContDB = new LCContDB();
			LZCardDB tLZCardDB = new LZCardDB();
			
			for(int  i = 0;i<lis_proposalno.size();i++)
			{
				
				proposalNo = (Element)lis_proposalno.get(i);
				
				cLogger.info("您需要进行单证核销的单证号为::"+proposalNo.getTextTrim()+"!");
				
				tLCContDB.setProposalContNo(proposalNo.getTextTrim());
				LCContSet MCContSet = tLCContDB.query();
				
				if(MCContSet.size() != 1){
				   cLogger.info("您进行单证核销的单证号::"+proposalNo.getTextTrim()+"已经无效,无法核销!");
					continue;
				}
				
				tLZCardDB.setStartNo(proposalNo.getTextTrim());  //通过四个条件判断尚未核销!
				tLZCardDB.setEndNo(proposalNo.getTextTrim());
				tLZCardDB.setStateFlag("5");
				tLZCardDB.setCertifyCode("HB07/07B");
				LZCardSet tLZCardDBSet = tLZCardDB.query();
				
				if(tLZCardDBSet.size()==1){   //已经正常单证核销的保单
					 cLogger.info("您进行单证核销的单证号::"+proposalNo.getTextTrim()+"已进行正常的单证核销,无需重复核销!");
					 continue;
				}
				
				LCContSchema  mLCContSchema =  MCContSet.get(1);
				
				LKTransStatusDB  lktransstaus = new LKTransStatusDB();
				
				lktransstaus.setPolNo(mLCContSchema.getContNo());
				lktransstaus.setFuncFlag("01");
				lktransstaus.setStatus("1");
				LKTransStatusSet mLKTransStatusSet =  lktransstaus.query();
				
				if(mLKTransStatusSet.size() != 1){
					cLogger.info("您进行单证核销的单证号::"+proposalNo.getTextTrim()+"在银保通中记载有误,无法进行核销!");
					continue;
				}
				
				mGlobalInput = YbtSufUtil.getGlobalInput(
						mLKTransStatusSet.get(1).getBankCode(),
						mLKTransStatusSet.get(1).getBankBranch(),
						mLKTransStatusSet.get(1).getBankNode());
				
				try{
					new CardManage("HB07/07B", mGlobalInput).makeUsed(proposalNo.getTextTrim());
				}
				catch (Exception e ){
					cLogger.info("您进行单证核销的单证号::"+proposalNo.getTextTrim()+"核心核销失败,请检查!");
				}
				
			}
			
			
			
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", "交易成功!");
			
			
			// 农信社走此路线，在此判断是否安徽农信社
				
		
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		} catch (Exception ex) {
			cLogger.error("单证核销交易失败"+"交易失败！", ex);

			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		cLogger.info("Out ProposalNoHeXiao.service()!");
		return mOutXmlDoc;
	}
	
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into ProposalNoHeXiao.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo("00000000");
		mLKTransStatusDB.setProposalNo("00000000");
		mLKTransStatusDB.setPrtNo("00000000");
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Into ProposalNoHeXiao.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

//		String mInFile = "F:/working/picch/std/testXml/WriteOff/std_04_in.xml";
//		String mOutFile = "F:/working/picch/std/testXml/WriteOff/std_04_out.xml";
		
		String mInFile = "C:\\Users\\Administrator\\Desktop\\16_384375_04_102519.xml";
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new ProposalNoHeXiao(null).service(mInXmlDoc);

        JdomUtil.print(mOutXmlDoc);
		System.out.println("成功结束！");
	}
}
