package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class QIO_HesitatePreRevokeBL  extends ServiceImpl {

	public QIO_HesitatePreRevokeBL(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QIO_HesitatePreBL.service()...");
		
		Document mOutXmlDoc = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc); //.........
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			// 进行业务处理
			if("122".equals(mBaseInfo.getChildText("FunctionFlag"))){ //退保冲正
				mOutXmlDoc = deal(pInXmlDoc);
			}
			if("63".equals(mBaseInfo.getChildText("FunctionFlag"))){ //退保冲正
				mOutXmlDoc = deal(pInXmlDoc);
			}
			if("66".equals(mBaseInfo.getChildText("FunctionFlag"))){ //追加保费冲正
				mOutXmlDoc = deal(pInXmlDoc);
			}
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Into QIO_HesitatePreBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document inXmlDoc) throws MidplatException{
		String mContNo = inXmlDoc.getRootElement().getChild("LCCont").getChildText("ContNo");
		
//		String sql_tran = "select 1 from lktransstatus where polno = '"+ mContNo +"' and funcflag = '121' with ur";
//		if(!"1".equals(new ExeSQL().getOneValue(sql_tran))){
//			throw new MidplatException(mContNo +" 未做过缴费交易，冲正失败！");
//		}
		
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);
		LCContSet mCont = cont.query();
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		
		if(mCont.size() > 0){
//			LCContSchema lc = mCont.get(1);
			
			mFlag.setText("1");
			mDesc.setText("交易成功！");
			
		}else {
			mFlag.setText("0");
			mDesc.setText("未查到对应保单信息！");
		}
		
		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) inXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into QIO_MQDeliveryQueryBL.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setStatus("1");
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Pay"));
		mLKTransStatusDB.setServiceStartTime(mLCCont.getChildText("ReceivDate")); //应收日期
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out QIO_MQDeliveryQueryBL.insertTransLog()!");
		return mLKTransStatusDB;
	}

}
