/**
 * 通用简单交易处理类。
 * 此类本身不做任何业务处理，仅插入一条交易日志。
 * 目前使用此类的交易有：中行冲正、建行绿灯
 */

package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.jdom.Document;
import org.jdom.Element;






import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.JdomUtil;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_ModBaseInfo extends ServiceImpl {
	public CCB_ModBaseInfo(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_ModBaseInfo.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mLCCont=null;
		Document mDoc = null;
		String mContNo=pInXmlDoc.getRootElement().getChild("LCCont").getChildText("Contno");
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

	        
	        String mRspInfo=queryAgentInfo(CreateXmlDoc(pInXmlDoc));
			cLogger.info("--------返回的报文信息输出----------");
			cLogger.info(mRspInfo);
		    mDoc=JdomUtil.build(mRspInfo.getBytes());
           //根据报文中的State进行判断		    
		    if(mDoc.getRootElement().getChild("MsgResHead").getChild("Item").getChildText("State").equals("00"))
		    {
		    	//从数据库中查找投保信息		    	
		    	//添加投保人信息节点
		    	cLogger.info("------------------"+mContNo);
		    	Element mAppntName=new Element("AppntName");
		    	String mSQL="select appntname from lccont where contno='"+mContNo+"'";
		    	ExeSQL mExeSQL=new ExeSQL();
		    	mAppntName.setText(mExeSQL.getOneValue(mSQL));
		    	
		    	Element mAppntAddress=new Element("AppntAddress");
		    	mSQL="select b.PostalAddress from lccont a,lcaddress b where a.appntno=b.customerno and a.contno='"+mContNo+"'";
		    	mAppntAddress.setText(mExeSQL.getOneValue(mSQL));
		    	
		    	Element mAppntZipCode=new Element("AppntZipCode");
		    	mSQL="select b.ZipCode from lccont a,lcaddress b where a.appntno=b.customerno and a.contno='"+mContNo+"'";
		    	mAppntZipCode.setText(mExeSQL.getOneValue(mSQL));
		    	
		    	Element mAppntPhone=new Element("AppntPhone");
		    	mSQL="select b.Phone from lccont a,lcaddress b where a.appntno=b.customerno and a.contno='"+mContNo+"'";
		    	mAppntPhone.setText(mExeSQL.getOneValue(mSQL));
		    	
		    	Element mAppntMobile=new Element("AppntMobile");
		    	mSQL="select b.Mobile from lccont a,lcaddress b where a.appntno=b.customerno and a.contno='"+mContNo+"'";
		    	mAppntMobile.setText(mExeSQL.getOneValue(mSQL));		    	
		 
		    	Element tContNo=new Element("ContNo");
		    	tContNo.setText(mContNo);
		    	
		    	
		    	Element mRiskCode=new Element("RiskCode");
		    	mSQL="select b.riskcode from lccont a,lcpol b where a.contno ='"+mContNo+"' and  a.contno=b.contno";
		    	mRiskCode.setText(mExeSQL.getOneValue(mSQL));
		    	
//		    	//提示信息描述
//		    	Element mPrmpt_Inf_Dsc=new Element("Prmpt_Inf_Dsc");
//		    	mSQL="select from  where a.=b. and  contno='"+mContNo+"'";
//		    	mPrmpt_Inf_Dsc.setText(mExeSQL.getOneValue(mSQL));
//		    	//代理保险凭证类型代码
//		    	Element mAgIns_Vchr_TpCd=new Element("AgIns_Vchr_TpCd");
//		    	mSQL="select from  where a.=b. and  contno='"+mContNo+"'";
//		    	mAgIns_Vchr_TpCd.setText(mExeSQL.getOneValue(mSQL));

		    	//#经办人
                Element mHandler=new Element("Handler");
                mSQL="select Handler from lccont  where contno='"+mContNo+"'";
                mHandler.setText(mExeSQL.getOneValue(mSQL));
                //保险重空号
                Element mIns_IBVoch_ID=new Element("Ins_IBVoch_ID");
                mSQL="select ProposalContNo from lccont  where contno='"+mContNo+"'";
                mIns_IBVoch_ID.setText(mExeSQL.getOneValue(mSQL));
                
                Element mLCAppnt=new Element("LCAppnt");                
                
                mLCAppnt.addContent(mAppntName);
                mLCAppnt.addContent(mAppntAddress);
                mLCAppnt.addContent(mAppntZipCode);
                mLCAppnt.addContent(mAppntPhone);
                mLCAppnt.addContent(mAppntMobile);
                
                
                mLCCont=new Element("LCCont");
                
                mLCCont.addContent(tContNo);
                mLCCont.addContent(mHandler);
                
                Element mRisk=new Element("Risk");
                mRisk.addContent(mRiskCode);
                Element mRisks=new Element("Risks");
                mRisks.addContent(mRisk);                
                mLCCont.addContent(mRisks);
                
//		    	mLCCont.addContent(mPrmpt_Inf_Dsc);
//		    	mLCCont.addContent(mAgIns_Vchr_TpCd);
//		    	mLCCont.addContent(mIns_IBVoch_ID);                
                mLCCont.addContent(mIns_IBVoch_ID);

                
                mLCCont.addContent(mLCAppnt);
		    	mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		    	
				
				mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束	
				
				Element mDataSet=(Element) mDoc.getRootElement().clone();
				mOutXmlDoc.getRootElement().addContent(mDataSet);
				mOutXmlDoc.getRootElement().addContent(mLCCont);
		    }else{
		    	
		    	mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", mDoc.getRootElement().getChild("MsgResHead").getChild("Item").getChildText("ErrInfo"));
		    }

			
		} catch (MidplatException ex) {
			cLogger.error("修改保单基本信息交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
	
		
		
		cLogger.info("Out CCB_ModBaseInfo.service()!");
		
		return mOutXmlDoc;
	}
	
	private String queryAgentInfo(Document pStdDoc){
		//根据同意工号调用webservice查询业务员信息
		String mResInfo = "";
	      try
	        {

	           String tStrTargetEendPoint = "http://10.136.10.101:900/services/PubServiceProxy";
	            String tStrNamespace = "http://services.core.cbsws.com";

	            
	            RPCServiceClient client = new RPCServiceClient();
	            String mInXmlStr = "";
	    		byte[] mBodyBytes = JdomUtil.toBytes(pStdDoc);
	    		mInXmlStr = new String(mBodyBytes);    		
	    		
	            EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
	            Options option = client.getOptions();
	            option.setTo(erf);
	            option.setTimeOutInMilliSeconds(100000L);
	            option.setAction("service");
	            QName name = new QName(tStrNamespace, "service");
	            Object[] object = new Object[] { mInXmlStr };
	            Class[] returnTypes = new Class[] { String.class };

	            Object[] response = client.invokeBlocking(name, object, returnTypes);
	            mResInfo = (String) response[0];

	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
		return  mResInfo;
	}
	
	private Document CreateXmlDoc(Document pXmlDoc){
		
		Element mLCCont=pXmlDoc.getRootElement().getChild("LCCont");

		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=sdf.format(new Date());
		String newtime=time;
		newtime=newtime.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "");
		//五位流水号
		Random random=new Random();
		int i=random.nextInt(10);
		String serialNo="0000"+(i+1);
		serialNo=serialNo.substring(serialNo.length()-5);
		
		Element mBatchNo=new Element("BatchNo");
		mBatchNo.setText("ybt0001"+"ybt"+newtime+serialNo);
		Element mSendDate=new Element("SendDate");
		mSendDate.setText(time.substring(0, 10).trim());
		Element mSendTime=new Element("SendTime");
		mSendTime.setText(time.substring(10).trim());
		Element mBranchCode=new Element("BranchCode");
		mBranchCode.setText("ybt");
		Element mSendOperator=new Element("SendOperator");
		mSendOperator.setText("ybt");
		Element mMsgType=new Element("MsgType");
		mMsgType.setText("ybt0001");
		
		
		Element mItem1=new Element("Item");
		mItem1.addContent(mBatchNo);
		mItem1.addContent(mSendDate);
		mItem1.addContent(mSendTime);
		mItem1.addContent(mBranchCode);
		mItem1.addContent(mSendOperator);
		mItem1.addContent(mMsgType);
		
		Element mMsgHead=new Element("MsgHead");
		mMsgHead.addContent(mItem1);
		
		Element mContNo=new Element("ContNo");
		mContNo.setText(mLCCont.getChildText("Contno"));
		Element mItem2=new Element("Item");
		mItem2.addContent(mContNo);
		Element mLCContTable=new Element("LCContTable");
		mLCContTable.addContent(mItem2);
		
		/*
		 * Plchd_Prov_Cd   投保人省代码
		 * Plchd_City_Cd    市代码
		 * Plchd_CntyAndDstc_Cd   县级代码
		 * Plchd_Dtl_Adr_Cntnt     详细地址
		 */
		
		String PlchdCtcAdrCtyRgon_Cd =  mLCCont.getChildText("PlchdCtcAdrCtyRgon_Cd");
		String Plchd_Prov_Cd =  mLCCont.getChildText("Plchd_Prov_Cd");
		String Plchd_City_Cd =  mLCCont.getChildText("Plchd_City_Cd");
		String Plchd_CntyAndDstc_Cd =  mLCCont.getChildText("Plchd_CntyAndDstc_Cd");
		String Plchd_Dtl_Adr_Cntnt =  mLCCont.getChildText("Plchd_Dtl_Adr_Cntnt");
		
		cLogger.info("待修改的投保人国家代码:"+PlchdCtcAdrCtyRgon_Cd+";省级代码:"+Plchd_Prov_Cd +";市代码:"
				+Plchd_City_Cd+";县代码:"+Plchd_CntyAndDstc_Cd+";详细地址"+Plchd_Dtl_Adr_Cntnt);
		
		ExeSQL exe = new ExeSQL();
		String temp ="";
		
		//-----------------------------开始处理投保人六段信息拼接  start----------------------------------------
		if(! PlchdCtcAdrCtyRgon_Cd.equals("") ){   //国家
			String sql = "select codename from  ldcode where codetype ='ybtaddress' and code =\'"+PlchdCtcAdrCtyRgon_Cd+"\'";
			cLogger.info("=="+sql);
			PlchdCtcAdrCtyRgon_Cd = exe.getOneValue(sql);    //通过地区编码查询出中文名称
			if(PlchdCtcAdrCtyRgon_Cd.equals("") || ( PlchdCtcAdrCtyRgon_Cd == null)){
				PlchdCtcAdrCtyRgon_Cd ="中国";   //设置默认国家为中国
			}
		}
		if(! Plchd_Prov_Cd.equals("") ){   //省
			String sql = "select codename from  ldcode where codetype ='ybtaddress' and code =\'"+Plchd_Prov_Cd+"\'";
			cLogger.info("=="+sql);
			Plchd_Prov_Cd = exe.getOneValue(sql);    //通过地区编码查询出中文名称
		}
		if(! Plchd_City_Cd.equals("") ){   //市
			String sql = "select codename from  ldcode where codetype ='ybtaddress' and code =\'"+Plchd_City_Cd+"\'";
			cLogger.info("=="+sql);
			Plchd_City_Cd = exe.getOneValue(sql);    //通过地区编码查询出中文名称
		}
		if(! Plchd_CntyAndDstc_Cd.equals("") ){   //县
			String sql = "select codename from  ldcode where codetype ='ybtaddress' and code =\'"+Plchd_CntyAndDstc_Cd+"\'";
			cLogger.info("=="+sql);
			Plchd_CntyAndDstc_Cd = exe.getOneValue(sql);    //通过地区编码查询出中文名称
		}
		if(! Plchd_Dtl_Adr_Cntnt.equals("") ){   //详细地址
			String sql = "select codename from  ldcode where codetype ='ybtaddress' and code =\'"+Plchd_Dtl_Adr_Cntnt+"\'";
			cLogger.info("=="+sql);
			Plchd_Dtl_Adr_Cntnt = exe.getOneValue(sql);    //通过地区编码查询出中文名称
		}
		temp = PlchdCtcAdrCtyRgon_Cd + Plchd_Prov_Cd + Plchd_City_Cd + Plchd_CntyAndDstc_Cd + Plchd_Dtl_Adr_Cntnt;
		cLogger.info("拼接完成的投保人的六段地址是::"+temp);
		
		Element mAppntMobile=new Element("AppntMobile");
		mAppntMobile.setText(mLCCont.getChildText("AppntMobile"));
		Element mAppntPhone=new Element("AppntPhone");
		mAppntPhone.setText(mLCCont.getChildText("AppntPhone"));
		Element mMailZipCode=new Element("MailZipCode");
		mMailZipCode.setText(mLCCont.getChildText("AppntZipCode"));
		Element mHomeAddress=new Element("HomeAddress");
		mHomeAddress.setText(temp);
		//-----------------------------开始处理投保人六段信息拼接  End----------------------------------------
		
		
		
		Element mItem3=new Element("Item");
		mItem3.addContent(mAppntMobile);
		mItem3.addContent(mAppntPhone);
		mItem3.addContent(mMailZipCode);
		mItem3.addContent(mHomeAddress);
		
		Element mLCAppntTable=new Element("LCAppntTable");
		mLCAppntTable.addContent(mItem3);
		
		
		Element mApplyTypeNo=new Element("ApplyTypeNo");
		mApplyTypeNo.setText("0");
		Element mApplyName=new Element("ApplyName");
		mApplyName.setText("ybt");
		Element mCustomerNo=new Element("CustomerNo");
		String contno=pXmlDoc.getRootElement().getChild("LCCont").getChildText("Contno");
		String mSQL="select appntno from lccont where contno='"+contno+"'";
		mCustomerNo.setText(new ExeSQL().getOneValue(mSQL));
		Element mTypeNo=new Element("TypeNo");
		mTypeNo.setText("0300001");
		Element mAcceptWayNo=new Element("AcceptWayNo");
		mAcceptWayNo.setText("6");
		Element mAcceptDate=new Element("AcceptDate");
		mAcceptDate.setText(time.substring(0, 10).trim());
		
		Element mItem4=new Element("Item");
		mItem4.addContent(mApplyTypeNo);
		mItem4.addContent(mApplyName);
		mItem4.addContent(mCustomerNo);
		mItem4.addContent(mTypeNo);
		mItem4.addContent(mAcceptWayNo);
		mItem4.addContent(mAcceptDate);		
		
		Element mLGWorkTable=new Element("LGWorkTable");
		mLGWorkTable.addContent(mItem4);
		
		
		Element mDataSet=new Element("DataSet");
		mDataSet.addContent(mMsgHead);
		mDataSet.addContent(mLCContTable);
		mDataSet.addContent(mLCAppntTable);
		mDataSet.addContent(mLGWorkTable);
		Document mXmlDo=new Document(mDataSet);
		return mXmlDo;
	}
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into SimpService.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out SimpService.insertTransLog()!");
		return mLKTransStatusDB;
	}
public  static void main(String args[])
{
	String path="C:\\Users\\XiaoLong\\Desktop\\建行\\ccb_860910180659_P53819161_180220_修改保单基本信息.xml";
	InputStream is = null;
	try {
		is = new FileInputStream(path);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
//	JdomUtil.build(is);
	CCB_ModBaseInfo mdinfo=new CCB_ModBaseInfo(null);
	mdinfo.service(JdomUtil.build(is));

}
}
