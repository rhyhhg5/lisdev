package com.sinosoft.midplat.kernel.service;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.LCCustomerImpartTable;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.utility.ExeSQL;
import common.Logger;

/**
 * @date 2017-02-27
 * @author zengzm
 * @version 1.0
 */

public class ReCreateEpolicy{
	protected final Logger cLogger = Logger.getLogger(getClass());
	
	public String service(String ContNo, String mBalaDate) {
		cLogger.info("Into ICBC_WxPrint.service()...");
		String returnStr = "";
 
		try {
			//备份sql
			String cx_sql = "select prtno From lktransstatus where bankcode in ('01','02','03','04','16','57','65') and transdate = '" + mBalaDate + "' and polno = '"+ContNo+"'" +
					"and funcflag = '01' and riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701','340401','334901','232401','XBT009','XBT010','XBT011','XBT012','XBT013','XBT014','XBT017') and rcode = '1'" +
					" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' and exists (select 1 from lccont where contno=polno) with ur";
			
			ExeSQL tExeSQL = new ExeSQL();
			String mPrtno = tExeSQL.getOneValue(cx_sql);
			MsgCollection mMsgCollection = creatDocument(mPrtno);
			ICBC_WxPrintBL mWxPrintBL = new ICBC_WxPrintBL();
				
			//调用核心接口生产电子保单
			mWxPrintBL.deal(mMsgCollection,1);

			returnStr = "电子保单正在生成，请稍候...";
		}catch (Exception ex) {
			
			returnStr = "电子保单生成失败！";
		}

		cLogger.info("Out ICBC_WxPrint.service()...");
		return returnStr;
	}
	
	public MsgCollection creatDocument(String mPrtno) {
		cLogger.info("Into ICBC_WxPrint.creatDocument()...");
		
		String pCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String pCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		String cx_sql = "select insuredno From lcinsured where prtno = '"+ mPrtno +"' with ur";
		
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo("123456789");	//
		tMsgHead.setBranchCode("001");	//
		tMsgHead.setMsgType("YBT_001");
		tMsgHead.setSendDate(pCurrentDate);
		tMsgHead.setSendTime(pCurrentTime);
		tMsgHead.setSendOperator("ybt");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(mPrtno);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
		LCCustomerImpartTable tLCCustomerImpartTable = new LCCustomerImpartTable();
		tLCCustomerImpartTable.setInsuredNo(new ExeSQL().getOneValue(cx_sql));
		tLCCustomerImpartTable.setImpartContent("身高(cm)");
		tLCCustomerImpartTable.setImpartParamModle("170");
		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable);
		
		cLogger.info("Out ICBC_WxPrint.creatDocument()...");
		return tMsgCollection;
	}
	
}
