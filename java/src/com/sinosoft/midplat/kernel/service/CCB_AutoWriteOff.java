package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_AutoWriteOff extends ServiceImpl {
	public CCB_AutoWriteOff(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_AutoWriteOff.service()...");
		Document mOutXmlDoc = null;

		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		try {
			//查询上一次交易日志
			LKTransStatusDB tLKTransStatusDB = new LKTransStatusDB();
			tLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
			tLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
			tLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
			tLKTransStatusDB.setTransNo(mLCCont.getChildText("OldTransrNo"));
			if (tLKTransStatusDB.getTransNo().equals("")) {
				throw new MidplatException("上一步交易的交易流水号不能为空！");
			}
			if (!tLKTransStatusDB.getInfo()) {
				cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
				throw new MidplatException("查询上一步交易失败！");
			}
			
			if (tLKTransStatusDB.getRCode().equals("")) {
				throw new MidplatException("上一步交易尚未结束！");
			}
			if(tLKTransStatusDB.getFuncFlag().equals("00")){	//建行缴费交易 没有单证故作处理
				mLCCont.getChild("FunctionFlag").setText("00");
				//建行缴费失败冲正，将lccont表中保单号前加上YBT不让反冲交易做单证撤销操作，因建行三步交易 缴费还没发送单证过来
				String mSQL = "update lccont set proposalcontno = '"+ "YBT" + tLKTransStatusDB.getProposalNo() +"' where contno = '"+tLKTransStatusDB.getPolNo()+"' with ur";
				new ExeSQL().execUpdateSQL(mSQL);
			}else if(tLKTransStatusDB.getFuncFlag().equals("06")){	//建行保单打印交易 有单证
				mLCCont.getChild("FunctionFlag").setText("06");
			}

			if (tLKTransStatusDB.getFuncFlag().equals("02")) {	//重打自动冲正
				RePrint_AutoWriteOff tRePrint_AutoWriteOff = new RePrint_AutoWriteOff(cThisBusiConf);
				mOutXmlDoc = tRePrint_AutoWriteOff.service(pInXmlDoc);
			} else if (tLKTransStatusDB.getFuncFlag().equals("00")) {	//新契约自动冲正
				Element tContNo = new Element(ContNo);
				tContNo.setText(tLKTransStatusDB.getPolNo());
				
				mLCCont.addContent(tContNo);
				
				AIO_AutoWriteOff tAIO_AutoWriteOff = new AIO_AutoWriteOff(cThisBusiConf);
				mOutXmlDoc = tAIO_AutoWriteOff.service(pInXmlDoc);
				
				
			} else {
				throw new MidplatException("冲正交易码有误！");
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out CCB_AutoWriteOff.service()!");
		return mOutXmlDoc;
	}
}
