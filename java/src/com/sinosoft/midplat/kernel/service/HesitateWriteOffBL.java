package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class HesitateWriteOffBL extends ServiceImpl {

	public HesitateWriteOffBL(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into HesitateWriteOffBL.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc); 
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			// 进行业务处理
			mOutXmlDoc = deal(pInXmlDoc);
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setTransStatus("1");
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Into HesitateWriteOffBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document inXmlDoc) {
		cLogger.info("Into HesitateWriteOffBL.deal()...");
		String mBankDate = inXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BankDate");
		Document xOutStdXml = null;
		
		Element mTranData = new Element("TranData");
//		Element mBaseInfo = new Element("BaseInfo");
		
		Element mChkDetails = new Element("ChkDetails");
		
		String mSql = "select lb.contno 保单号1,lb.prtno 投保单号2,lb.appntname 投保人姓名3,lb.polapplydate 投保单申请日期4,lb.prem 保单保费5," +
				"-lpd.getmoney 退保保费6,lpd.edortype 退保类型7 from lbcont lb,lpedoritem lpd,lktransstatus lk" +
				" where lb.contno=lpd.contno and lk.polno=lb.contno and lpd.edortype in ('WT','CT') and lk.bankcode='02'" +
				" and lpd.makedate='"+mBankDate+"' group by lb.contno,lb.prtno,lb.appntname,lb.polapplydate,lb.prem,lpd.getmoney,lpd.edortype with ur";
		ExeSQL mExeSQL = new ExeSQL();
		
		SSRS mSSRS = mExeSQL.execSQL(mSql);
		
		if(mSSRS.MaxRow >= 1){
			for(int i = 1 ; i <= mSSRS.MaxRow ;i++){
				Element mChkDetail = new Element("ChkDetail");
				
				Element tContNo = new Element("ContNo");
				tContNo.setText(mSSRS.GetText(i, 1));
				mChkDetail.addContent(tContNo);
				
				Element tPrtNo = new Element("PrtNo");
				tPrtNo.setText(mSSRS.GetText(i, 2));
				mChkDetail.addContent(tPrtNo);
				
				Element tAppntName = new Element("AppntName");
				tAppntName.setText(mSSRS.GetText(i, 3));
				mChkDetail.addContent(tAppntName);
				
				Element tPolapplyDate = new Element("PolapplyDate");
				tPolapplyDate.setText(mSSRS.GetText(i, 4));
				mChkDetail.addContent(tPolapplyDate);
				
				Element tPrem = new Element("Prem");
				tPrem.setText(mSSRS.GetText(i, 5));
				mChkDetail.addContent(tPrem);
				
				Element tGetMoney = new Element("GetMoney");
				tGetMoney.setText(mSSRS.GetText(i, 6));
				mChkDetail.addContent(tGetMoney);
				
				Element tEdorType = new Element("EdorType");
				tEdorType.setText(mSSRS.GetText(i, 7));
				mChkDetail.addContent(tEdorType);
				
				Element tPremMoney = new Element("PremMoney");
				if("CT".equals(mSSRS.GetText(i, 7))){
					tPremMoney.setText(mSSRS.GetText(i, 6));//犹豫期外为退保金额
				}else{
					tPremMoney.setText(mSSRS.GetText(i, 5));
				}
				
				mChkDetails.addContent(mChkDetail);
			}
		
		}
		
		Element tTotalNum = new Element("TotalNum");
		tTotalNum.setText(mSSRS.MaxRow + "");
		mTranData.addContent(tTotalNum);
		
//		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mChkDetails);
		
		xOutStdXml = new Document(mTranData);
		cLogger.info("Into HesitateWriteOffBL.deal()...");
		return xOutStdXml;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into HesitateWriteOffBL.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setStatus("1");
//		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
//		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Pay"));
//		mLKTransStatusDB.setServiceStartTime(mLCCont.getChildText("ReceivDate")); //应收日期
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out HesitateWriteOffBL.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
}
