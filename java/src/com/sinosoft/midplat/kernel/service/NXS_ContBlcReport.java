package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class NXS_ContBlcReport extends ServiceImpl {

	public NXS_ContBlcReport(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {

		cLogger.info("Into NXS_ContBlcReport.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false; // 删除数据标志
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		String zSQL = "select zoneno from lkcodemapping where bankcode = '"
				+ mBaseInfo.getChildText(BankCode) + "' and banknode = '"
				+ mBaseInfo.getChildText(BrNo) + "'";
		String ZoneNos = new ExeSQL().getOneValue(zSQL);
		mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			Element mFlag = new Element(Flag);
			Element mDesc = new Element(Desc);
			mFlag.setText("1");
			mDesc.setText("交易成功！");
			Element mRetData = new Element(RetData);
			mRetData.addContent(mFlag);
			mRetData.addContent(mDesc);
			Element mTranDataw = new Element(TranData);
			mTranData.addContent(mRetData);
			mOutXmlDoc = new Document(mTranData);
		} catch (MidplatException ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				String ttDesr = ex.getMessage();
				try {
					if (null != ttDesr
							&& ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex) {
				}
				mLKTransStatusDB.setDescr(ttDesr);
			}

			if (mDeleteable) {
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		cLogger.info("Out NXS_ContBlcReport.service()...");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {

		cLogger.info("Into NXS_NewContPrint.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		String sBankCodeString = mBaseInfo.getChildText(BankCode);
		if ("60".equals(sBankCodeString) || "78".equals(sBankCodeString)) {
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"
					+ mBaseInfo.getChildText(BankCode) + "' and banknode = '"
					+ mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);

		}
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setTransTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out NXS_NewContPrint.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "E:\\rb\\jxnxs\\instd\\04.xml";
		String mOutFile = "E:\\rb\\jxnxs\\outstd\\04.xml";

		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new NXS_ContBlcReport(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");

	}

}
