
package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.BlackPhoneCheck;
import com.sinosoft.midplat.kernel.service.check.IdNoCheck;
import com.sinosoft.midplat.kernel.service.check.YBTRiskCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtAutoCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtContBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtContInsuredIntlBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtInsuredProposalBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class OIO_NewCont extends ServiceImpl {
	
	public OIO_NewCont(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into OIO_NewCont.service()...");
		long startTimes = System.currentTimeMillis();
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		LCContSubDB mLCContSubDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false;	//删除数据标志
		
		YBTRiskCheck riskCheck = new YBTRiskCheck();

		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		String  mProposalContNo = mLCCont.getChildText(ProposalContNo);
		String mSalary = mLCCont.getChild(LCAppnt).getChildText("Salary");//投保人年收入
		String mCountyType = mLCCont.getChild(LCAppnt).getChildText("CountyType");//投保人所属区域
		cLogger.info("获取到投保人年收入"+mSalary);
	
		String mAppntPhone = mLCCont.getChild(LCAppnt).getChildText(AppntPhone);
		String mAppntMobile = mLCCont.getChild(LCAppnt).getChildText(AppntMobile);
		String mAppntOfficePhone = mLCCont.getChild(LCAppnt).getChildText(AppntOfficePhone);
		String tRiskCode = pInXmlDoc.getRootElement().getChild(LCCont).getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
		Element mRisk = pInXmlDoc.getRootElement().getChild(LCCont).getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChild(Risk);
		
		//针对福建农信社不传ZoneNo情况进行特殊处理 add by gaojinfu 2017-06-01
		if("69".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String mZoneNo = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild(ZoneNo).setText(mZoneNo);
		}
		
		
		//ManageCom=8621在正式上生产还需要配置成8661
		try { 
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			//产品核心停售校验
			riskCheck.checkRiskForYBT(pInXmlDoc);
			
			if ("1".equals(mLCCont.getChildText("SourceType"))
					|| "8".equals(mLCCont.getChildText("SourceType"))
					|| "e".equals(mLCCont.getChildText("SourceType"))
					|| "f".equals(mLCCont.getChildText("SourceType"))
					|| (mBaseInfo.getChildText(BankCode).equals("69") && ("YBT001"
							.equals(tRiskCode) || "YBT002".equals(tRiskCode)))) {
				cLogger.info("电子渠道动态生成保单印刷号和单证号开始...");

				String wyPrtno = "56" + PubFun1.CreateMaxNo("YBTPRTNO", 11);// 生成印刷号
				mProposalContNo = wyPrtno;
				cLogger.info("生成新的保单印刷号为：" + wyPrtno);

				String mSQL = "select 1 from lccont where ProposalContNo = '"
						+ mProposalContNo
						+ "' union select 1 from lbcont where ProposalContNo = '"
						+ mProposalContNo + "' with ur";
				String tSQL = "select 1 from lktransstatus where proposalno='"
						+ mProposalContNo + "' and rcode is null with ur";
				if (mProposalContNo.equals("")) {
					throw new MidplatException("电子渠道无可提供的单证号！");
				}
				if ("1".equals(new ExeSQL().getOneValue(tSQL))) { // 同时两个人出单
																	// 系统查出相同的单证
					throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
				} else if (new ExeSQL().getOneValue(mSQL).equals("1")) {
					// 一般不会出现这种情况
					throw new MidplatException("该投保单印刷号已使用，请更换！");
				} else {
					mLCCont.getChild(PrtNo).setText(mProposalContNo);
					mLCCont.getChild(ProposalContNo).setText(mProposalContNo);
				}
				cLogger.info("电子渠道动态生成保单印刷号和单证号结束...");
			}
			
			Calendar calendar0 = Calendar.getInstance();
			SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy-MM-dd");
			calendar0.add(Calendar.YEAR, -1);
			Date nowDate0 = calendar0.getTime();	//三号文获取当前日期前一年日期
			String compareDate0 = sdf0.format(nowDate0);	//对比日期
			String nowDate10= DateUtil.getCurDate("yyyy-MM-dd"); //当前日期
			String nowMonth0 = nowDate10.substring(5, 7);//当前日期月份
			
			//交行、江苏银行三号文校验
			if("10".equals(mBaseInfo.getChildText(BankCode))){
				ExeSQL pExeSQL = new ExeSQL();
				String pSQL;
				SSRS pSSRS= new SSRS();
				if("1".equals(mCountyType)){	//城镇
					pSQL = "select RecentPCDI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate0 + "' and enddate >= '" + compareDate0 + "' with ur";
					pSSRS = pExeSQL.execSQL(pSQL);
					if(pSSRS.MaxRow > 0){
						String tRecentPCDI = pSSRS.GetText(1, 1);
						if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCDI)){
							throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
						}
					}else {
						throw new MidplatException("未录入当地人均收入，请检查！！！");
					}	
				}else if("2".equals(mCountyType)){	//乡村
					pSQL = "select RecentPCNI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate0 + "' and enddate >= '" + compareDate0 + "' with ur";
					pSSRS = pExeSQL.execSQL(pSQL);
					if(pSSRS.MaxRow > 0){
						String tRecentPCNI = pSSRS.GetText(1, 1);
						if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCNI)){
							throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
						}
					}else {
						throw new MidplatException("未录入当地人均收入，请检查！！！");
					}	
				}
			}
			
			Element tThisConfRoot = cThisBusiConf.getParent();
			cLogger.info("获取到的险种是"+tRiskCode);
			cLogger.info("获取到的银行编码是="+mBaseInfo.getChildText("BankCode")+"ZoneNo="+mBaseInfo.getChildText(ZoneNo)+"BrNo="+mBaseInfo.getChildText(BrNo));
			cLogger.info("获取到的管理机构是"+ mGlobalInput.ManageCom);
			
			//获取福佑相伴按地区销售套餐编码  modify by GaoJinfu 20171206
			if("511101".equals(tRiskCode) && ("01".equals(mBaseInfo.getChildText(BankCode)) || "02".equals(mBaseInfo.getChildText(BankCode)))){
				String zSQL = "select riskwrapcode,enableflag from lkratemapping where bankcode = '" + mBaseInfo.getChildText(BankCode) + "' and managecom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' with ur";
				SSRS pSSRS= new SSRS();
				pSSRS = new ExeSQL().execSQL(zSQL);
				if(pSSRS.MaxRow != 0){
					if("Y".equals(pSSRS.GetText(1, 2).trim())){
						mRisk.getChild(MainRiskCode).setText(pSSRS.GetText(1, 1));
					}else{
						throw new MidplatException("该地区" + pSSRS.GetText(1, 1) + "套餐已停售或未启用");
					}
				}else{
					mRisk.getChild(MainRiskCode).setText("YBT006");
				}
			}
			
			//这段代码与福佑相伴按地区销售冲突，目前没有出单记录，先进行注释，如果需要再放开   gaojinfu 20170630
//			if(-1 != tThisConfRoot.getChildText("WN_Risk").indexOf(tRiskCode) && 
//					"511101".equals(tRiskCode) &&
//					mBaseInfo.getChildText("BankCode").equals("04") &&                                             //针对浙江农行(8633)福佑相伴511101(XBT004)费率无法配置问题作调整的 add 20151014
//					(mGlobalInput.ManageCom.startsWith("8661") || mGlobalInput.ManageCom.startsWith("8633"))){    //针对陕西农行(8661)福佑相伴511101(XBT004)费率无法配置问题作调整的 add 20150909  
//				 pInXmlDoc.getRootElement().getChild(LCCont).getChild(LCInsureds).
//				 getChild(LCInsured).getChild(Risks).getChild(Risk).getChild(MainRiskCode)
//				 .setText("XBT004");
//				 mOutXmlDoc = new  YbtandXbt_NewCont(this.cThisBusiConf).service(pInXmlDoc);
//			}
//			else{
			try {
				String tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
				if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
					throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
				}
				
				//如果PrtNo不存在，则可以根据PrtNo删除数据
				tSQL = "select 1 from lccont where PrtNo='" + mLCCont.getChildText(PrtNo) + "' with ur";
				String tSQL_b = "select 1 from lbcont where PrtNo='" + mLCCont.getChildText(PrtNo) + "' with ur";
				if ("1".equals(new ExeSQL().getOneValue(tSQL)) || "1".equals(new ExeSQL().getOneValue(tSQL_b))) {
					throw new MidplatException("该投保单印刷号已使用，请更换！");
				} else {
					mDeleteable = true;
				}
				
				tSQL = "select prtno from lccont where AppFlag='0' and ProposalContNo='" + mLCCont.getChildText(ProposalContNo) + "' with ur";
				String tPrtNoStr = new ExeSQL().getOneValue(tSQL);
				cLogger.debug("PrtNo = " + tPrtNoStr);
				if ((null!=tPrtNoStr) && !tPrtNoStr.equals("")) {	//同一保单合同书印刷号(ProposalContNo)，未签单
					tSQL = "select 1 from LKTransStatus where RCode='1' and FuncFlag='01' "
						+ "and BankCode='" + mBaseInfo.getChildText(BankCode) + "' "
						+ "and BankBranch='" + mBaseInfo.getChildText(ZoneNo) + "' "
						+ "and BankNode='" + mBaseInfo.getChildText(BrNo) + "' "
						+ "and TransDate='" + DateUtil.getCurDate("yyyy-MM-dd") + "' "
						+ "and ProposalNo='" + mLCCont.getChildText(ProposalContNo) + "' "
						+ "and PrtNo='" + tPrtNoStr + "' "
						+ "with ur";
					if (new ExeSQL().getOneValue(tSQL).equals("1")) {	//同一天，同一银行，同一网点
						YbtSufUtil.clearData(tPrtNoStr);
					}
				} 

				mLKTransStatusDB = insertTransLog(pInXmlDoc);

//				mGlobalInput = YbtSufUtil.getGlobalInput(
//						mBaseInfo.getChildText(BankCode),
//						mBaseInfo.getChildText(ZoneNo),
//						mBaseInfo.getChildText(BrNo));
				
				//新增加广东地区 投被保人关系为父子、母女、母子、父女关系时 投被保人年龄之差大于18岁
				if(mGlobalInput.ManageCom.startsWith("8644")){
					String mRelaToInsured = mLCCont.getChild("LCAppnt").getChildText("RelaToInsured");
					String mRelaToAppnt = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("RelaToAppnt");
					String mAppntBirthday = mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");
					String mBirthday = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("Birthday");
					if (mRelaToInsured.equals("03") || mRelaToInsured.equals("04")
							|| mRelaToInsured.equals("05")
							|| mRelaToAppnt.equals("03")
							|| mRelaToAppnt.equals("04")
							|| mRelaToAppnt.equals("05")) {
						if(!BlackPhoneCheck.checkBirthday(mAppntBirthday, mBirthday)){
							throw new MidplatException("投被保人关系父子、父女、母女、母子关系时，投被保人年龄差值小于18岁不通过！");
						}
					}
				}
				
				
				
				//广东地区所有银行年龄加险种期缴校验
				//上生产需要将 8611(北京地区)修改成  8644
//				if(mGlobalInput.ManageCom.startsWith("8644"))
				if(mGlobalInput.ManageCom.startsWith("8644"))
				{
					List lisRisk = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
////					for(Element Risk :lisRisk){
					for(int i =0;i <lisRisk.size();i++){
						Element Risk = (Element) lisRisk.get(i);
					if(Risk != null)
					{
						String PayIntv = Risk.getChildTextTrim("PayIntv");  //获取缴费频率
						String InsuYear = Risk.getChildTextTrim("InsuYear");  //获取保险期间
						cLogger.info("获取缴费频率"+PayIntv);
						cLogger.info("获取保险期间"+InsuYear);
						
						if(!"".equals(InsuYear)){
							String appAge =  mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");  //获取投保人年龄
							System.out.println("appAge"+appAge);
							int Age =  this.getAppntAge(appAge);
							cLogger.info("获取投保人年龄:"+appAge);
							//上外侧,需要修改成 && 
							if(Age >60 && (Integer.parseInt(InsuYear))>1){
								throw new MidplatException("投保人年龄大于60岁,且保险期间大于1年,不通过...");
							}
						}
					}
					}
					
					//增加广东地区全部地址增加长度不小于9个汉字校验  add by kangz 
					String adress  = mLCCont.getChild("LCAppnt").getChildTextTrim("HomeAddress"); 
					cLogger.info("adress is :" + adress);	
					if(adress.length()<9){
						throw new MidplatException("投保人家庭住址位数长度不能小于9");
					}
					//增加固定电话填写3或4位区号后面加非“400”和“800”开头的10位数字时不通过校验
					String AppntPhone  = mLCCont.getChild("LCAppnt").getChildTextTrim("AppntPhone");
					cLogger.info("AppntPhone is :" + AppntPhone);
					if(!"".equals(AppntPhone))
					{
						if(AppntPhone.length() ==13)
						{
							AppntPhone = AppntPhone.substring(3, 6);
							if(!AppntPhone.equals("400") && !AppntPhone.equals("800"))
							{
								throw new MidplatException("固定电话为13位时,第4位至6位只能能为400或800");
							}
						}
						else if(AppntPhone.length() == 14)
						{
							AppntPhone = AppntPhone.substring(4, 7);
							if(!AppntPhone.equals("400") && !AppntPhone.equals("800"))
							{
								throw new MidplatException("固定电话为14位时,第5位至7位只能能为400或800");
							}
						}
					}
				}
				
				

				// 山西地区校验电话号码(20121112,增加江苏号码校验)（20121128，增加四川校验、黑龙江、内蒙古、20130109陕西、20130603 山东）
				String mInsuredPhone = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("HomePhone");
				String mInsuredMobile = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("InsuredMobile");
				if (mGlobalInput.ManageCom.startsWith("8614") || mGlobalInput.ManageCom.startsWith("8632") || mGlobalInput.ManageCom.startsWith("8651") || mGlobalInput.ManageCom.startsWith("8615")  || mGlobalInput.ManageCom.startsWith("8661")  || mGlobalInput.ManageCom.startsWith("8637")) {
					//山西 投保人地址、电话不能为空
					String xHomeAddress = mLCCont.getChild("LCAppnt").getChildText("HomeAddress");
					if("".equals(xHomeAddress) || xHomeAddress == null){
						throw new MidplatException("投保人联系地址不能为空！");
					}
					if(("".equals(mAppntPhone) || mAppntPhone == null) 
							&& ("".equals(mAppntMobile) || mAppntMobile == null)
							&& ("".equals(mAppntOfficePhone) || mAppntOfficePhone == null)){
						throw new MidplatException("投保人联系电话至少一个不能为空！");
					}
					
					BlackPhoneCheck.checkPhone(mAppntPhone);//校验投保人号码
					BlackPhoneCheck.checkPhone(mAppntMobile);
					BlackPhoneCheck.checkPhone(mInsuredPhone);// 校验被保人号码
					BlackPhoneCheck.checkPhone(mInsuredMobile);
				}
				
				// 陕西地区校验投被保人地址不能为空且内容在12个字符以上。  add by wz 20130812
				if(mGlobalInput.ManageCom.startsWith("8661")){
					String msg = checkAddre(mLCCont);
					if(!"".equals(msg)){
						throw new MidplatException(msg);
					}
				}
				
				//停售 江西吉安,农行渠道,福利双全产品  add  by   mxk 2016-02-23
				/*if(mGlobalInput.ManageCom.startsWith("86360800") && "04".equals(mBaseInfo.getChildText("BankCode")))
				{
					if(tRiskCode.equals("333701"))//福利双全
					{
						throw new MidplatException("该产品已停售!");
					}
				}*/
				
				//停售 陕西,邮储渠道,福利双全产品  add  by   zengzm 2016-12-29
				if(mGlobalInput.ManageCom.startsWith("8661") && "16".equals(mBaseInfo.getChildText("BankCode")))
				{
					if(tRiskCode.equals("333701"))//福利双全
					{
						throw new MidplatException("该产品在陕西地区已经停售!");
					}
				}
				
				Calendar calendar1 = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				calendar1.add(Calendar.YEAR, -1);
				Date nowDate1 = calendar1.getTime();	//三号文获取当前日期前一年日期
				String compareDate = sdf.format(nowDate1);	//对比日期
				String nowDate = DateUtil.getCurDate("yyyy-MM-dd"); //当前日期
				String nowMonth = nowDate.substring(5, 7);//当前日期月份
				
				//人均收入的校验延迟一个季度 每年前四个月的年收入应对比大前年的平均人收入
				if(Integer.parseInt(nowMonth) < 4){ //每年前四个月
					calendar1.add(Calendar.YEAR, -1);
					nowDate1 = calendar1.getTime();	//三号文获取当前日期前2年日期
					compareDate = sdf.format(nowDate1);	//对比日期
				}
				 
				//新农行、工行、中行、邮储、乌鲁木齐（标准个险）三号文校验  ---为本地测试方便注释  gaojinfu  20170630
				if("04".equals(mBaseInfo.getChildText(BankCode)) ||"03".equals(mBaseInfo.getChildText(BankCode)) || "02".equals(mBaseInfo.getChildText(BankCode))|| "16".equals(mBaseInfo.getChildText(BankCode))|| "52".equals(mBaseInfo.getChildText(BankCode))){
					ExeSQL pExeSQL = new ExeSQL();
					String pSQL; 
					SSRS pSSRS= new SSRS();
					if("1".equals(mCountyType)){	//城镇
						pSQL = "select RecentPCDI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
						pSSRS = pExeSQL.execSQL(pSQL);
						if(pSSRS.MaxRow > 0){
							String tRecentPCDI = pSSRS.GetText(1, 1);
							if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCDI)){
								if("04".equals(mBaseInfo.getChildText(BankCode))){	//增加自核转人核
									throw new MidplatException("9990投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
								}else {
									throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
								}
							}
						}else {
							throw new MidplatException("未录入当地人均收入，请检查！！！");
						}	
					}else if("2".equals(mCountyType)){	//乡村
						pSQL = "select RecentPCNI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
						pSSRS = pExeSQL.execSQL(pSQL);
						if(pSSRS.MaxRow > 0){
							String tRecentPCNI = pSSRS.GetText(1, 1);
							if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCNI)){
								if("04".equals(mBaseInfo.getChildText(BankCode))){	//增加工行自核转人核
									throw new MidplatException("9990投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
								}else {
									throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
								}
							}
						}else {
							throw new MidplatException("未录入当地人均收入，请检查！！！");
						}	
					}
				}
				
				// 风险提示测试 add by mxk 2018-04-20
				if (mGlobalInput.ManageCom.startsWith("8644")
						&& "02".equals(mBaseInfo.getChildText(BankCode))) {
					cLogger.info("开始校验中行广东地区的风险提示...");
					String m_AppntBirthday = mLCCont.getChild("LCAppnt")
							.getChildText("AppntBirthday"); // 获取投保人信息
					cLogger.info("获取到的投保人年龄####::" + m_AppntBirthday);

					if ( m_AppntBirthday != null && ! m_AppntBirthday.equals("")) {
						int age = getAppntAge(m_AppntBirthday);
						List m_LCInsured = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
						for (int i=0;i< m_LCInsured.size();i++) {
							Element LCInsured = (Element)m_LCInsured.get(i);
							Element Risks = LCInsured.getChild("Risks");
							if (Risks != null && !Risks.equals("")) {
								List lis_Risk = Risks.getChildren("Risk");
									for (int k= 0;k<lis_Risk.size();k++) {
										Element risk = (Element)lis_Risk.get(k);
									 if(risk.getChildText("RiskCode").equals(risk.getChildText("MainRiskCode")) && risk.getChildText("PayIntv").equals("12")){   //判断主险,且缴费类型为年缴
										int age_pay = 0;
										int agem_AppntBirthday = 0;
										try {
											age_pay = Integer.parseInt(risk.getChildTextTrim("PayEndYear"));
											cLogger.info("获取到的缴费年限是::"+age_pay);
										} catch (Exception e) {
											throw new MidplatException("获取险种缴费期限或投保人年龄失败..");
										}
										if(age_pay+age >=60){
											throw new MidplatException("缴费年限与投保人年龄数字之和大于或等于60时,请复核...");
										}
										
									}
								
								}
							}
						}
					}

					cLogger.info("结束校验中行广东地区的风险提示...");
				}
				
				
				cLogger.debug("RiskCode = " + tRiskCode);
				
				if(-1 != tThisConfRoot.getChildText("StopSale").indexOf(tRiskCode)){
					 throw new MidplatException("该产品已停售!"); 
				}
				
				//停售校验 add by GaoJinfu 20180329
				String status = riskCheck.riskStopCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"));
				if (status.equals("2")) {
					throw new MidplatException("该产品已停售!");  
				}
				boolean gxFlag = riskCheck.riskValidCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"),"GX");
				boolean wnFlag = riskCheck.riskValidCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"),"WN");
				if (-1 != tThisConfRoot.getChildText("GX_Risk").indexOf(tRiskCode)
						|| ("231701".equals(tRiskCode) && "52".equals(mBaseInfo.getChildText(BankCode)))
						|| gxFlag) {	//走标准个险流程
					//投保单录入
					YbtContBL ttYbtContBL = new YbtContBL(
							pInXmlDoc, mGlobalInput);
					ttYbtContBL.deal();    
	   
					//被保人、险种信息录入 
					YbtInsuredProposalBL ttYbtInsuredProposalBL = new YbtInsuredProposalBL(
							pInXmlDoc, mGlobalInput);
					ttYbtInsuredProposalBL.deal();
	 
					//自核+复核
					YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
							mLCCont.getChildText(PrtNo), mGlobalInput);
					ttYbtAutoCheck.deal();

					//组织返回报文
					String ttSQL = "select contno from lccont where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
					YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
							new ExeSQL().getOneValue(ttSQL));
					mOutXmlDoc = ttYbtContQueryBL.deal();
				} else if (-1 != tThisConfRoot.getChildText("WN_Risk").indexOf(tRiskCode)
						|| wnFlag) {	//走picch特有的国际贸易流程
					
					//泉州针对富有相伴产品走特殊费率配置
					if(mGlobalInput.ManageCom.startsWith("8635") && mBaseInfo.getChildText(BankCode).endsWith("53")){
						//获得信保通费率
						feeRateManage(mGlobalInput, mBaseInfo, mRisk, "XBT004");
					}
					//投保单录入
					YbtContBL ttYbtContBL = new YbtContBL( 
							pInXmlDoc, mGlobalInput);
					ttYbtContBL.deal();

					//被保人、险种信息录入
					YbtContInsuredIntlBL ttYbtContInsuredIntlBL = new YbtContInsuredIntlBL(
							pInXmlDoc, mGlobalInput);
					ttYbtContInsuredIntlBL.deal();

//					//自核+复核
					YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
							mLCCont.getChildText(PrtNo), mGlobalInput);
					ttYbtAutoCheck.deal();

					//组织返回报文
					String ttSQL = "select contno from lccont where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
					YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
							new ExeSQL().getOneValue(ttSQL));
					mOutXmlDoc = ttYbtContQueryBL.deal();
				} else if (tRiskCode.equals("330501")	//常无忧B
						|| tRiskCode.equals("330701")) {	//万能险A
					throw new MidplatException("该产品已停售！");
				} else {
					throw new MidplatException("险种代码有误，或该险种未上线！" + tRiskCode);
				}
				
				//增加所有渠道的电话号码重复性校验 add by LiXiaoLong 2014-07-07 begin
				String mAppntNo = mOutXmlDoc.getRootElement().getChild(LCCont).getChild(LCAppnt).getChildText(AppntNo);
				BlackPhoneCheck.checkAllPhone(mAppntNo, mAppntPhone, mAppntOfficePhone, mAppntMobile);

				/**
				 * 新农行、工行、邮储三号文 add by LiXiaoLong 2014-03-19 begin
				 * */
				if ("04".equals(mBaseInfo.getChildText(BankCode))
						|| "03".equals(mBaseInfo.getChildText(BankCode))
						|| "02".equals(mBaseInfo.getChildText(BankCode))
						|| "16".equals(mBaseInfo.getChildText(BankCode))
						|| "52".equals(mBaseInfo.getChildText(BankCode))) {
					mLCContSubDB = insertLCContSubDB(pInXmlDoc, mGlobalInput);
				}
				
				//建行康乐人生A 增加保费校验  add by LxXiaoLong 20140522
				//由于康乐A+豁免险俩主险 多被保人故特此增加其保费的校验 KLPrem
//				if("03".equals(mBaseInfo.getChildText(BankCode))){
//					String  mKLPrem = mLCCont.getChildText("KLPrem");
//					String  mKLokFlag = mLCCont.getChildText("KLokFlag");
//					if(mKLokFlag.equals("1")){	//康乐产品判断标识
//						
//						String xKLPrem = mOutXmlDoc.getRootElement().getChild("LCCont").getChildText("Prem");
//						if(!mKLPrem.equals(xKLPrem)){
//							throw new MidplatException("传入保费["+ mKLPrem +"]与计算出保费["+ xKLPrem +"]不一致，请重新录入。");
//						}
//					}
//				}

				Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
				mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
				mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				
				//未做系统录入(客户姓名、性别、证件类型（身份证或其他）、证件号码、录入有效期)且超过20万的保单，做拒保处理  add by gcy 20180205
				/*double mPrem = Double.parseDouble(mOutXmlDoc.getRootElement().getChild("LCCont").getChildTextTrim("Prem"));
				if(mPrem > Double.parseDouble("200000.00")){
					Element mLCAppnt = mOutXmlDoc.getRootElement().getChild("LCCont").getChild("LCAppnt");
					String mSQL = "select Name,Sex,IDType,IDNo,EffEctiveDate from LCYBTCustomer where Name='"+mLCAppnt.getChildText("AppntName")+"'" +
							" and IDType='"+mLCAppnt.getChildText("AppntIDType")+"' and IDNo='"+mLCAppnt.getChildText("AppntIDNo")+"'" +
									" order by makedate desc,maketime desc with ur";
					SSRS tSSRS = new ExeSQL().execSQL(mSQL);
					if(tSSRS.getMaxRow()<1){
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
					
					if("".equals(tSSRS.GetText(1, 1)) || "".equals(tSSRS.GetText(1, 2)) 
							|| "".equals(tSSRS.GetText(1, 3)) || "".equals(tSSRS.GetText(1, 4))
							|| "".equals(tSSRS.GetText(1, 5))){
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
					
					if(tSSRS.GetText(1, 5) != null && !"".equals(tSSRS.GetText(1, 5))){
						FDate fDate = new FDate();
						Date effectiveDate = fDate.getDate(tSSRS.GetText(1, 5));
						Date tranDate = fDate.getDate(mBaseInfo.getChildText("BankDate"));
//						if (fDate.mErrors.needDealError())
//						{
//							throw new MidplatException("累计保费超限，需保险公司后台登记");
//						}
						
						GregorianCalendar tCalendar = new GregorianCalendar();
						tCalendar.setTime(tranDate);
						int sYears = tCalendar.get(Calendar.YEAR);
						int sMonths = tCalendar.get(Calendar.MONTH);
						int sDays = tCalendar.get(Calendar.DAY_OF_MONTH);

						GregorianCalendar eCalendar = new GregorianCalendar();
						eCalendar.setTime(effectiveDate);
						int eYears = eCalendar.get(Calendar.YEAR);
						int eMonths = eCalendar.get(Calendar.MONTH);
						int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
						
						tCalendar.set(sYears, sMonths, sDays);
						eCalendar.set(eYears, eMonths, eDays);
						long lInterval = (eCalendar.getTime().getTime() - tCalendar.getTime().getTime()) / 86400000;
						if(lInterval < 0 || lInterval>31){
							throw new MidplatException("累计保费超限，需保险公司后台登记");
						}
					}
				}*/
			} catch (Exception ex) {
				cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);

				if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
					mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回

					String tDesr = ex.getMessage();
					try {
						if (ex.getMessage().getBytes("utf-8").length >= 255) {
							tDesr = tDesr.substring(0,85);
						}
					} catch (UnsupportedEncodingException uex){
						cLogger.error(uex.getMessage());
					}
	 
					mLKTransStatusDB.setDescr(tDesr);
				}
	 
				if (mDeleteable) {
					try {
						YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
					} catch (Exception tBaseEx) {
						cLogger.error("删除新单数据失败！", tBaseEx);
					}
					
				}
				mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
			}

			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
//			}
		} catch (Exception ex) {   //此处的catch主要是防止网点信息未配置发生的错误返回

			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);

			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回

				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
 
				mLKTransStatusDB.setDescr(tDesr);
			}
 
			if (mDeleteable) {
				try {
					YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
				} catch (Exception tBaseEx) {
					cLogger.error("删除新单数据失败！", tBaseEx);
				}
				
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
			// TODO: handle exception
		}
		cLogger.info("Out OIO_NewCont.service()!耗时：" + (System.currentTimeMillis()-startTimes)/1000.0 + "s");
		return mOutXmlDoc;
	}
	
	/**
	 * 计算投保人年龄
	 * */
	public static int getAppntAge(String ageDate) {

		Date now = new Date();
		String sdf = new SimpleDateFormat("yyyyMMdd").format(now);

		int age = 0;
		int staY = Integer.parseInt(ageDate.substring(0, 4));
		int staM = Integer.parseInt(ageDate.substring(4, 6));
		int staD = Integer.parseInt(ageDate.substring(6, 8));

		int nowY = Integer.parseInt(sdf.substring(0, 4));
		int nowM = Integer.parseInt(sdf.substring(4, 6));
		int nowD = Integer.parseInt(sdf.substring(6, 8));

		if (staM > nowM) {
			age = nowY - staY - 1;
		} else if (staM == nowM) {
			if (nowD > staD) {
				age = nowY - staY;
			} else if (nowD == staD) {
				age = nowY - staY;
			} else if (nowD < staD) {
				age = nowY - staY - 1;
			}

		} else if (staM < nowM) {
			age = nowY - staY;
		}

		return age;
	}
	
	private void feeRateManage(GlobalInput mGlobalInput, Element mBaseInfo,Element mRisk, String tRiskWrapCode) throws MidplatException   {
		cLogger.info("Into NXS_NewContInput.feeRateManage()!");
		String tInsuYearFlag = mRisk.getChildText(InsuYearFlag);
		String tInsuYear = mRisk.getChildText(InsuYear);
		String tManageCom = mGlobalInput.ManageCom.substring(0, 4);
		
		cLogger.debug("InsuYearFlag = " + tInsuYearFlag);
		cLogger.debug("InsuYear = " + tInsuYear);
		cLogger.debug("tManageCom="+tManageCom);
		
		if (-1 == cThisBusiConf.getParent().getChildText("NoFeeRate").indexOf(tManageCom)){//midplatsuf.xml中配置不走赔率表算费的农信社
			//信保通费率需从费率配置表中读取
			String mSQL = null;
			mSQL = "Select ProtocolNo,AvailableState from LKFeeRateProtocol where "
				+"BankCode = '"+mBaseInfo.getChildText(BankCode)+"' and "
				+"ManageCom = '"+mGlobalInput.ManageCom.substring(0, 4)+"' and "
				+"RiskWrapCode = '"+tRiskWrapCode+"' and "
				+"AgentCom in ('"+mGlobalInput.AgentCom+"','PC') order by remark1 with ur";
			SSRS tSSRS = new ExeSQL().execSQL(mSQL);
			if(tSSRS.getMaxRow()<1){
				throw new MidplatException("未配置信保通费率信息!");
			}else{
				String mProtocolNo = tSSRS.GetText(1, 1);
				String mAvailableState = tSSRS.GetText(1, 2);
				if("1".equals(mAvailableState)){
					mSQL = "Select feerate,feeratetype from LKFeeRateDetails where "
						+"ProtocolNo = '"+mProtocolNo+"' and "
						+"FeeRateYearFlag = '"+tInsuYearFlag+"' and "
						+"FeeRateYear in ('-1','"+tInsuYear+"') with ur";
					SSRS ttSSRS = new ExeSQL().execSQL(mSQL);
					if(ttSSRS.getMaxRow()!= 1){
						throw new MidplatException("查询此费率明细信息为空或多于1条记录!");
					}
					String mFeeRate = ttSSRS.GetText(1, 1);
					String mFeeRateType = ttSSRS.GetText(1, 2);
					if("".equals(mFeeRate)||null == mFeeRate){
						throw new MidplatException("费率明细信息添加错误!");
					}
					//此处对报文添加费率计算信息字段,目前只支持一个险种,以后如果有多个险种,需要调整.
					Element eFeeRate = new Element("FeeRate");
					eFeeRate.setText(mFeeRate);
					Element eFeeRateType = new Element("FeeRateType");
					eFeeRateType.setText(mFeeRateType);
					mRisk.addContent(eFeeRate);
					mRisk.addContent(eFeeRateType);
				}else{
					cLogger.info("费率表配置为此机构,套餐不通过费率表算费!");
				}
			}
		}
		cLogger.info("Out NXS_NewContInput.feeRateManage()!");
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into OIO_NewCont.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		if("69".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
		}

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		String mRiskCode = pXmlDoc.getRootElement().getChild(LCCont)
		.getChild(LCInsureds).getChild(LCInsured)
		.getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
		mLKTransStatusDB.setRiskCode(mRiskCode);
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		if(mBaseInfo.getChildText(BankCode).equals("60")){	//江西农信社该信息过长所以存入到预留字段bak1中
			mLKTransStatusDB.setbak1(mLCCont.getChildText("LoanInvoiceNo"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("LoanContractNo"));
		}
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		/**
		 * 建行/兴业银行三号文要求增加销售人员工号、销售人员姓名、销售人员资格证号 
		 * add by LiXiaoLong 20140311 begin 
		 * **/
		if(mBaseInfo.getChildText(BankCode).equals("03") || "13".equals(mBaseInfo.getChildText("BankCode"))){
			mLKTransStatusDB.setbak1(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("RckrNo"));
			mLKTransStatusDB.setbak3(mLCCont.getChildText("SaleName"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("SaleCertNo"));
		}
		
		//新农行、中行三号文要求增加出单网点名称、销售人员工号、负责人姓名、网点保险兼业代理业务许可证编码
		// add by LiXiaoLong 20140326 begin     
		if(mBaseInfo.getChildText(BankCode).equals("02") || mBaseInfo.getChildText(BankCode).equals("04")){
			mLKTransStatusDB.setbak1(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("BrCertID"));
			mLKTransStatusDB.setbak3(mLCCont.getChildText("ManagerNo"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("ManagerName"));
		}
		
		//邮储三号文要求增加出单网点名称、销售人员工号
		// add by LiXiaoLong 20140321 begin
		if(mBaseInfo.getChildText(BankCode).equals("16")){
			mLKTransStatusDB.setbak3(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("SaleCertNo"));
		}
		
		//乌鲁木齐三号文要求增加出单网点名称、销售人员工号、负责人姓名
		// add by LiXiaoLong 20140411 begin     
		if(mBaseInfo.getChildText(BankCode).equals("52")){
			mLKTransStatusDB.setbak1(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("SaleCertNo"));
			mLKTransStatusDB.setBankAcc(mLCCont.getChildText("BrCertID"));
			mLKTransStatusDB.setbak3(mLCCont.getChildText("ManagerNo"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("SaleCertName"));
			
		}
		
		
		/**
		 * 	end
		 */

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out OIO_NewCont.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	private String checkAddre(Element xLCCont){
		cLogger.info("Into OIO_NewCont.checkAddre()...");
		String result = "";
		
		Element xAppnt = xLCCont.getChild(LCAppnt);
		Element xInsured = xLCCont.getChild(LCInsureds).getChild(LCInsured);
		
		String xAppntMailAddr = xAppnt.getChildText("MailAddress");
		String xAppntHomeAddr = xAppnt.getChildText("HomeAddress");
		String xInsuredHomeAddr = xInsured.getChildText("HomeAddress");
		String xInsuredMailAddr = xInsured.getChildText("MailAddress");
		
		if("".equals(xAppntMailAddr) || xAppntMailAddr == null){
			result = "投保人邮寄地址不能为空！";
			return result;
		}else {
			if(xAppntMailAddr.length() < 12){
				result = "投保人邮寄地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xAppntHomeAddr) || xAppntHomeAddr == null){
			result = "投保人联系地址不能为空！";
			return result;
		}else {
			if(xAppntHomeAddr.length() < 12){
				result = "投保人联系地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xInsuredHomeAddr) || xInsuredHomeAddr == null){
			result = "被保人联系地址不能为空！";
			return result;
		}else {
			if(xInsuredHomeAddr.length() < 12){
				result = "被保人联系地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xInsuredMailAddr) || xInsuredMailAddr == null){
			result = "被保人邮寄地址不能为空！";
			return result;
		}else {
			if(xInsuredMailAddr.length() < 12){
				result = "被保人邮寄地址长度不足12位！";
				return result;
			}
		}
		cLogger.info("Out OIO_NewCont.checkAddre()...");
		return result;
	}
	
	 private LCContSubDB insertLCContSubDB(Document pXmlDoc,GlobalInput pGlobalInput) throws MidplatException {
	    	cLogger.info("Into OIO_NewCont.insertLCContSubDB()...");
			
			Element mTranData = pXmlDoc.getRootElement();
			Element mLCCont = mTranData.getChild(LCCont);
			Element mLCAppnt = mLCCont.getChild(LCAppnt);

			LCContSubDB mLCContSubDB = new LCContSubDB();
			
			String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
			String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
			
			mLCContSubDB.setPrtNo(mLCCont.getChildText(PrtNo));
			mLCContSubDB.setCountyType(mLCAppnt.getChildText("CountyType"));
			mLCContSubDB.setFamilySalary(mLCAppnt.getChildText("FamilySalary"));
			
			if(mTranData.getChild(BaseInfo).getChildText(BankCode).equals("52")){//乌鲁木齐
				mLCContSubDB.setFamilySalary(mLCAppnt.getChildText("Family"));
			}
			 
			mLCContSubDB.setManageCom(pGlobalInput.ManageCom);
			mLCContSubDB.setOperator("ybt");
			mLCContSubDB.setMakeDate(mCurrentDate);
			mLCContSubDB.setMakeTime(mCurrentTime);
			mLCContSubDB.setModifyDate(mCurrentDate);
			mLCContSubDB.setModifyTime(mCurrentTime);
			
			if (!mLCContSubDB.insert()) {
				cLogger.error(mLCContSubDB.mErrors.getFirstError());
				throw new MidplatException("插入投保人家庭年收入表失败！");
			}
			
			String mSQL = "update LCAppnt set Salary = '" + mLCAppnt.getChildText("Salary") + "' where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if (!mExeSQL.execUpdateSQL(mSQL)) {
				cLogger.error(mExeSQL.mErrors.getFirstError());
				throw new MidplatException("更新投保人家庭年收入失败！！！");
			}
			
			cLogger.info("Out AIO_NewCont.insertLCContSubDB()!");
			return mLCContSubDB;
	    	
	    }
	 
	

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

//		String mInFile = "F:\\000gaojinfu\\bankMassage\\trail.xml";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\ccb\\instd\\trial.xml";
		String mInFile = "F:\\000gaojinfu\\bankMassage\\icbc\\instd\\123.xml";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\xingye\\instd\\shisuan.xml";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\hebeibank\\instd\\trail.xml";
		String mOutFile = "F:/out.xml";
		
		String elpath = "F:\\sinosoft\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		Element root = c.getRootElement().getChild("business");

		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));

		Document mOutXmlDoc = new OIO_NewCont(root).service(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
		System.out.println(getAppntAge("19630619"));
	}
}
