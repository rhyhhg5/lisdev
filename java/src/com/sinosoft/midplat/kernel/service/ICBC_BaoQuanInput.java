package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.MidplatSufConf;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class ICBC_BaoQuanInput extends ServiceImpl {
	
	public ICBC_BaoQuanInput(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_BaoQuanInput.service()...");
		
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		LKTransStatusDB mLKTransStatusDB = null;
		Element mDataSet = pInXmlDoc.getRootElement();
		Element mBaseInfo = mDataSet.getChild(BaseInfo);
		String mEdorType = mDataSet.getChild("EdorItemInfo").getChild("Item").getChildTextTrim("EdorType");
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			//查询保全权限 add by GaoJinfu 20180327
			System.out.println("********************************************************************");
			if(!checkBQPower(pInXmlDoc)){
				throw new MidplatException("系统暂时停止保全业务，请转保险公司办理！");
			}
			System.out.println("********************************************************************");
			
			SSRS ssrs = new SSRS();
			ExeSQL es = new ExeSQL();
			String contNos = mDataSet.getChild("EdorItemInfo").getChild("Item").getChildTextTrim("ContNo");
			String transno = mDataSet.getChild("EdorItemInfo").getChild("Item").getChildTextTrim("ContNo");
			String tSql = "select bak1 from lktransstatus where bankcode = '01' and funcflag = '130' and polno = '"+contNos+"' and makedate = current date  order by maketime desc fetch first 1 rows only ";
			String bak1 = es.getOneValue(tSql);
			
			if("CT".equals(mEdorType)){
				pInXmlDoc.getRootElement().getChild("EdorItemInfo").getChild("Item").getChild("BudGet").setText(bak1);
				System.out.println("BAK1:" + mDataSet.getChild("EdorItemInfo").getChild("Item").getChildTextTrim("BudGet"));
			}
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));

	        //是否需做判断银行传过来的数据与核心表中数据一致性？
			// 进行犹豫期、退保 业务处理
			if("WT".equals(mEdorType) || "CT".equals(mEdorType)){
				mOutXmlDoc = deal(pInXmlDoc);
			}
			if("MQ".equals(mEdorType)){ //满期给付
				dealMQ(pInXmlDoc);
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束

		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
//		JdomUtil.print(mOutXmlDoc);
		cLogger.info("Out ICBC_BaoQuanInput.service()...");
		return mOutXmlDoc;
	}
	
	/**
	 * 校验保全权限 add by GaoJinfu 20180327
	 * @param pInXmlDoc
	 * @return
	 */
	private boolean checkBQPower(Document pInXmlDoc) {
		cLogger.info("Into ABC_BaoQuanInput.checkBQPower()...");
		Element mDataSet = pInXmlDoc.getRootElement();
		Element mBaseInfo = mDataSet.getChild(BaseInfo);
		Element mEdorItemInfoItem = mDataSet.getChild("EdorItemInfo").getChild("Item");
		String mContNo = mEdorItemInfoItem.getChildText("ContNo");
		//借款人意外险不能做保全添加  add by gaojinfu 20170906
		String mSql = "select riskcode from lcpol where contno = '"+mContNo+"' with ur";
		System.out.println("mSql=="+mSql);
		if("511101".equals(new ExeSQL().getOneValue(mSql))){
			cLogger.info("该产品不允许做退保、犹豫期退保");
			return false;
		}
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mContNo);
		LCContSet tLCContSet = tLCContDB.query();
		LCContSchema tLCContSchema = tLCContSet.get(1);
		mSql = "select substr(managecom,1,4) from lkcodemapping where bankcode = '"
				+ mBaseInfo.getChildText("BankCode")
				+ "' and zoneno='"
				+ mBaseInfo.getChildText("ZoneNo")
				+ "' and banknode='"
				+ mBaseInfo.getChildText("BrNo") + "' with ur";
		String comcode = new ExeSQL().getOneValue(mSql);;
		System.out.println("mSql=="+mSql);
		SSRS ssrs = new SSRS();
		mSql = "select status,changedate from ybtriskswtich where datatype = 'BQ' and bankcode = '"
				+ mBaseInfo.getChildText("BankCode")
				+ "' and (comcode = '86' or comcode = '"
				+ comcode
				+ "') and (channel = '00' or channel = '"
				+ tLCContSchema.getCardFlag() + "') with ur";
		System.out.println("mSql=="+mSql);
		ssrs = new ExeSQL().execSQL(mSql);
		if(ssrs.MaxRow!=1){
			cLogger.info("保全权限未开通或保全配置有误，请核实!");
			return false;
		}else{
			String status = ssrs.GetText(1, 1);
			int changedate = Integer.parseInt(DateUtil.date10to8(ssrs.GetText(1, 2)));
			int nowDate = DateUtil.getCur8Date();
			if(!("1".equals(status) && (nowDate >= changedate))){
				cLogger.info("保全权限未开通或保全配置有误，请核实!");
				return false;
			}
		}
		cLogger.info("Out ABC_BaoQuanInput.checkBQPower()...");
		return true;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into ICBC_BaoQuanInput.deal()...");
		
		Document pInXmlDoc_copy = (Document) pInXmlDoc.clone();
		pInXmlDoc_copy.getRootElement().removeChild("BaseInfo");
		Element tBaseInfo = pInXmlDoc.getRootElement().getChild("BaseInfo");
		Element xDataSet = pInXmlDoc_copy.getRootElement();
		String mContNo = xDataSet.getChild("EdorItemInfo").getChild("Item").getChildText("ContNo");
		String mBaoQuanFlag = xDataSet.getChild("EdorItemInfo").getChild("Item").getChildText("EdorType");
		
		SSRS ssrs = new SSRS();
		ExeSQL es = new ExeSQL();
		String tSql = "select contno from lccont where salechnl in ('03','04') and cardflag in ('9','c','d','a','e') and contno='"+mContNo+"' ";
		String tContNo = es.getOneValue(tSql);
		cLogger.info("tSql:"+tSql);
		if(tContNo==null || tContNo.equals("")){
			throw new MidplatException("该保单非银保通渠道出单!");
		}
		
		tSql = "select polno from lktransstatus where bankcode='01' and bankbranch='"+tBaseInfo.getChildText("ZoneNo")+"' " +
				"and banknode='"+tBaseInfo.getChildText("BrNo")+"' and funcflag='01' and polno= '"+mContNo+"'";
		String tPolno = es.getOneValue(tSql);
		cLogger.info("tSql:"+tSql);
		//放开手机银行网点限制
		tSql = "select cardflag from lccont where salechnl in ('03','04') and cardflag in ('9','c','d','a','e') and contno='"+mContNo+"' ";
		String tCardFlag = es.getOneValue(tSql);
		cLogger.info("tSql:"+tSql);
		if((tPolno==null || tPolno.equals("")) && !"e".equals(tCardFlag)){
			throw new MidplatException("该保单保全网点与出单网点不一致!");
		}
		
		tSql = "select 1 from lccont where stateflag='0' and contno='"+mContNo+"'";
		cLogger.info("tSql:"+tSql);
		if("1".equals(new ExeSQL().getOneValue(tSql))){//保单尚未签单
			throw new MidplatException ("该保单尚未签单!");
		}
		
		tSql = "select contno from lbcont where contno='"+mContNo+"' union select contno from lccont where contno='"+mContNo+"' and stateflag <>1";
		ssrs = es.execSQL(tSql);
		cLogger.info("tSql:"+tSql);
		if(ssrs.MaxRow>=1){
			throw new MidplatException("该保单已失效!");
		}
		
		tSql = "select lc.contno from lccont lc,lktransstatus lk where lc.contno=lk.polno and lk.bankcode='01' " +
				"and lk.funcflag='01' and status='1' and lk.rbankvsmp is null and lc.contno='"+mContNo+"' ";
		ssrs = es.execSQL(tSql);
		cLogger.info("tSql:"+tSql);
		if(ssrs.MaxRow>=1){
			throw new MidplatException("该保单未对账!");
		}
		
		if(mBaoQuanFlag.equals("WT")){
			tSql = "select signdate from lccont where contno='"+mContNo+"'";
			String tSigndate = es.getOneValue(tSql);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			Date date = null;
			try {
				date = format.parse(tSigndate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH, 14);
			String dateStr = format.format(cal.getTime());
			dateStr = dateStr.replace("-", "");
//			String tTransdate = tBaseInfo.getChildText("BankDate");
			String tTransdate = DateUtil.getCur10Date();
			tTransdate = tTransdate.replace("-", "");
			cLogger.info("tSql:"+tSql);
			if(Integer.parseInt(tTransdate)>Integer.parseInt(dateStr)){
				throw new MidplatException("此保单超出犹豫期,不能犹豫期退保!");
			}
		}
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);

		LCContSet mCont = cont.query();
		
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		
		if(mCont.size() > 0){
			
			LCContSchema lc = mCont.get(1);
			Element tItem = pInXmlDoc_copy.getRootElement().getChild("EdorItemInfo").getChild("Item");
			tItem.getChild("ManageCom").setText(lc.getManageCom());
			tItem.getChild("CardFlag").setText(lc.getCardFlag());
			tItem.getChild("Salechnl").setText(lc.getSaleChnl());
			tItem.getChild("Salechnl").setText(lc.getSaleChnl());
			pInXmlDoc_copy.getRootElement().getChild("EdorAppInfo").getChild("Item").getChild("CustomerNo").setText(lc.getAppntNo());
			
			System.out.println("------------------给核心保全请求报文-----------------");
			JdomUtil.print(pInXmlDoc_copy);
			System.out.println("-----------------------------------");
			
			//针对农行保全申请 add by zengzm 2015-11-13
			Document outStdXml = queryBaoQuanInfo(pInXmlDoc_copy);
			
			System.out.println("------------------核心保全返回报文-----------------");
			JdomUtil.print(outStdXml);
			System.out.println("-----------------------------------");
			
			if(outStdXml.getRootElement().getChild("MsgResHead").getChild("Item").getChildTextTrim("State").equals("00")){
				mFlag.setText("1");
				mDesc.setText("交易成功！");
				LBPolDB pol_db = new LBPolDB();
				pol_db.setContNo(mContNo);
				LBPolSet polSet = pol_db.query();
				
				double mSumPrem = 0;
				mSumPrem = Double.parseDouble(outStdXml.getRootElement().getChild("EndorsementInfo").getChild("Item").getChildTextTrim("SumGetMoney"));
				
				String risk_main = "";
				String risk_fu1 = "";
				String risk_fu2 = "";
				System.out.println("POLSET:"+polSet.size());
				for (int i =1 ; i <= polSet.size(); i++) {
					LBPolSchema mPol = polSet.get(i);
					if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
						risk_main = mPol.getRiskCode();
					}
					if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
						risk_fu1 = mPol.getRiskCode();
					}
					if("3".equals(mPol.getRiskSeqNo()) || "03".equals(mPol.getRiskSeqNo())){
						risk_fu2 = mPol.getRiskCode();
					}
				}
				
				Element nEdorAcceptNo = new Element("EdorAcceptNo");
				nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
				
				Element nContNo = new Element("ContNo");
				nContNo.setText(lc.getContNo());
				
				Element nCValiDate = new Element(CValiDate);
				nCValiDate.setText(lc.getCValiDate());
				
				Element nContEndDate = new Element(ContEndDate);
				nContEndDate.setText(lc.getCInValiDate());	//picch特有，保单级保单终止日期
				
				Element nAttachmentContent = new Element("AttachmentContent");
				Element nAppntName = new Element("AppntName");
				nAppntName.setText(lc.getAppntName());
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String now = sdf.format(new Date());
				Element nEdorAcceptYear = new Element("EdorAcceptYear");
				nEdorAcceptYear.setText(now.substring(0,4));
				Element nEdorAcceptMonth = new Element("EdorAcceptMonth");
				nEdorAcceptMonth.setText(now.substring(4,6));
				Element nEdorAcceptDay = new Element("EdorAcceptDay");
				nEdorAcceptDay.setText(now.substring(6,8));
				Element nApplyDate = new Element("ApplyDate");
				nApplyDate.setText(now);
				
				Element nMainRiskCodeName = new Element("MainRiskCodeName");
				nMainRiskCodeName.setText(getRiskRep(risk_main));
				Element nRiskCodeName1 = new Element("RiskCodeNamefu");
				Element nRiskCodeName2 = new Element("RiskCodeName_fu");
				if(!"".equals(risk_fu1)){
					nRiskCodeName1.setText(getRiskRep(risk_fu1));
				}
				if(!"".equals(risk_fu2)){
					nRiskCodeName2.setText(getRiskRep(risk_fu2));
				}
				
				Element nSumPrem = new Element("SumPrem");
				if(mBaoQuanFlag.equals("WT")){	//犹豫期保费
					nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
				}
				if(mBaoQuanFlag.equals("CT")){	//退保保费即解约
//					PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
//					double pSumPrem = mPedorYBTBudgetBL.getZTmoney(lc.getContNo(),now);
					nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
				}
	
				nAttachmentContent.addContent(nAppntName);
				nAttachmentContent.addContent(nEdorAcceptYear);
				nAttachmentContent.addContent(nEdorAcceptMonth);
				nAttachmentContent.addContent(nEdorAcceptDay);
				nAttachmentContent.addContent(nApplyDate);
				nAttachmentContent.addContent(nMainRiskCodeName);
				nAttachmentContent.addContent(nRiskCodeName1);
				nAttachmentContent.addContent(nRiskCodeName2);
				nAttachmentContent.addContent(nSumPrem);
				mEdorInfo.addContent(nEdorAcceptNo);
				mEdorInfo.addContent(nCValiDate);
				mEdorInfo.addContent(nContEndDate);
				mEdorInfo.addContent(nContNo);
				mEdorInfo.addContent(nAttachmentContent);
			}else {
				mFlag.setText("0");
				mDesc.setText(outStdXml.getRootElement().getChild("MsgResHead").getChild("Item").getChildText("ErrInfo"));
				throw new MidplatException(outStdXml.getRootElement().getChild("MsgResHead").getChild("Item").getChildText("ErrInfo"));
			}
		}else {
			mFlag.setText("0");
			mDesc.setText("查询保单信息失败");
			throw new MidplatException("查询保单信息失败");
		}

		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out ICBC_BaoQuanInput.deal()...");
		return new Document(mTranData);
	}
	
	private Document queryBaoQuanInfo(Document pInXmlDoc) {
		
		return JdomUtil.build(queryInfo(pInXmlDoc).getBytes());
	}
	
	private String queryInfo(Document pStdDoc){
		//根据同意工号调用webservice查询业务员信息
		String mResInfo = "";
	      try
	        {
	    	  
	    	  Document MidplatSufDoc =  MidplatSufConf.newInstance().getConf();
	    	  String  tStrTargetEendPoint = MidplatSufDoc.getRootElement().getChildTextTrim("WebService");
	    	  if(null == tStrTargetEendPoint || "".equals(tStrTargetEendPoint)){
	    		  throw new  MidplatException("webservice地址解析失败,请检查MidplatSuf.xml配置!");
	    	  }
	    	  
	            String tStrNamespace = "http://services.core.cbsws.com";   
	            
	            RPCServiceClient client = new RPCServiceClient();
	            String mInXmlStr = "";
	    		byte[] mBodyBytes = JdomUtil.toBytes(pStdDoc);
	    		mInXmlStr = new String(mBodyBytes);    		
	    		
	            EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
	            Options option = client.getOptions();
	            option.setTo(erf);
	            option.setTimeOutInMilliSeconds(100000L);
	            option.setAction("service");
	            QName name = new QName(tStrNamespace, "service");
	            Object[] object = new Object[] { mInXmlStr };
	            Class[] returnTypes = new Class[] { String.class };

	            Object[] response = client.invokeBlocking(name, object, returnTypes);
	            mResInfo = (String) response[0];

	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
		return  mResInfo;
	}
	
	//满期
	private Document dealMQ(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into ICBC_BaoQuanInput.deal()...");
		
		Element mEdorAppInfo = pInXmlDoc.getRootElement().getChild("EdorAppInfo"); 
		Element mEdorItemInfo = pInXmlDoc.getRootElement().getChild("EdorItemInfo"); 
		
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		
		Element nEdorAcceptNo = new Element("EdorAcceptNo");
		nEdorAcceptNo.setText(""); //批单号...
		
		Element nContNo = new Element("ContNo");
		nContNo.setText(mEdorItemInfo.getChildTextTrim("ContNo"));
		
		String now = mEdorItemInfo.getChildTextTrim("EdorValidate");
		Element nCValiDate = new Element(CValiDate);
		nCValiDate.setText(now);
		
		Element nContEndDate = new Element(ContEndDate);
		nContEndDate.setText(now);	
		
		Element nAttachmentContent = new Element("AttachmentContent");
		Element nAppntName = new Element("AppntName");
		nAppntName.setText(mEdorAppInfo.getChildTextTrim("ApplyName"));
		
		
		Element nEdorAcceptYear = new Element("EdorAcceptYear");
		nEdorAcceptYear.setText(now.substring(0,4));
		Element nEdorAcceptMonth = new Element("EdorAcceptMonth");
		nEdorAcceptMonth.setText(now.substring(4,6));
		Element nEdorAcceptDay = new Element("EdorAcceptDay");
		nEdorAcceptDay.setText(now.substring(6,8));
		Element nApplyDate = new Element("ApplyDate");
		nApplyDate.setText(now);
		
		Element nMainRiskCodeName = new Element("MainRiskCodeName");
		nMainRiskCodeName.setText("");
		Element nRiskCodeName = new Element("RiskCodeName");
		nRiskCodeName.setText("");
		
		Element nSumPrem = new Element("SumPrem");
		nSumPrem.setText("0");

		nAttachmentContent.addContent(nAppntName);
		nAttachmentContent.addContent(nEdorAcceptYear);
		nAttachmentContent.addContent(nEdorAcceptMonth);
		nAttachmentContent.addContent(nEdorAcceptDay);
		nAttachmentContent.addContent(nApplyDate);
		nAttachmentContent.addContent(nMainRiskCodeName);
		nAttachmentContent.addContent(nRiskCodeName);
		nAttachmentContent.addContent(nSumPrem);
		mEdorInfo.addContent(nEdorAcceptNo);
		mEdorInfo.addContent(nCValiDate);
		mEdorInfo.addContent(nContEndDate);
		mEdorInfo.addContent(nContNo);
		mEdorInfo.addContent(nAttachmentContent);
	
		

		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out ICBC_BaoQuanInput.deal()...");
		return new Document(mTranData);
	}
	
	public String getRiskRep(String riskCode){
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
	}
	
	/*private LPYBTAppWTDB insertLPYBTAppWT(Document pXmlDoc,LCContSchema lc) throws MidplatException {
		cLogger.info("Into ABC_BaoQuanInput.insertLPYBTAppWT()...");
		
		LPYBTAppWTSet mLPYBTAppWTSet = new LPYBTAppWTSet();
		LPYBTAppWTDB mLPYBTAppWTDB = new LPYBTAppWTDB();
		Element xTranData = pXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		String mBaoQuanFlag = xTranData.getChild("LCCont").getChildText("BaoQuanFlag");
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTSet = mLPYBTAppWTDB.query();
		
		if(mLPYBTAppWTSet.size() > 0){	//判断保单是否已存在犹豫期退保表中！
			String mSQL = "update lktransstatus set descr = '该保单已存在保全表中，请核查！' where polno = '" + mContNo + "'and funcflag = '56' and makedate = Current Date with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if (!mExeSQL.execUpdateSQL(mSQL)) {
				cLogger.error(mExeSQL.mErrors.getFirstError());
				cLogger.warn("该保单已存在保全表中，请核查！");
			}
		}
		
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTDB.setManageCom(lc.getManageCom());
		mLPYBTAppWTDB.setAppWTDate(mCurrentDate);
		mLPYBTAppWTDB.setAppntName(lc.getAppntName());
		mLPYBTAppWTDB.setAppntSex(lc.getAppntSex());
		mLPYBTAppWTDB.setAppntBirthday(lc.getAppntBirthday());
		mLPYBTAppWTDB.setAppntIDType(lc.getAppntIDType());
		mLPYBTAppWTDB.setAppntIDNo(lc.getAppntIDNo());
		mLPYBTAppWTDB.setGBFee("0");	//保险公司定为0元
		mLPYBTAppWTDB.setAppState("1");		//需要新的状态标识
		mLPYBTAppWTDB.setoperator("ybt");
		mLPYBTAppWTDB.setTransStatus("1");	//未对账
		mLPYBTAppWTDB.setEdorType(mBaoQuanFlag);	//保全类别 WT-犹豫期退保 CT-解约
		mLPYBTAppWTDB.setMakeDate(mCurrentDate);
		mLPYBTAppWTDB.setMakeTime(mCurrentTime);
		mLPYBTAppWTDB.setModifyDate(mCurrentDate);
		mLPYBTAppWTDB.setModifyTime(mCurrentTime);
		
		if (!mLPYBTAppWTDB.insert()) {
			cLogger.error(mLPYBTAppWTDB.mErrors.getFirstError());
			throw new MidplatException("插入保全表失败！");
		}
		cLogger.info("Out ABC_BaoQuanInput.insertLPYBTAppWT()...");
		return mLPYBTAppWTDB;
	}*/

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into ICBC_BaoQuanInput.insertTransLog()...");

		Element mDataSet = pXmlDoc.getRootElement();
		Element mBaseInfo = mDataSet.getChild(BaseInfo);
//		Element mLCCont = mTranData.getChild("LCCont");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPrtNo("");
		mLKTransStatusDB.setPolNo(pXmlDoc.getRootElement().getChild("EdorItemInfo").getChild("Item").getChildTextTrim("ContNo"));
		mLKTransStatusDB.setTransAmnt("");
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out ICBC_BaoQuanInput.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "E:\\rb\\icbc\\instd\\icbc_259817_1017_110357.xml";
		String mOutFilePath = "E:\\rb\\icbc\\outstd\\hpoutstd.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		ICBC_BaoQuanInput hq = new ICBC_BaoQuanInput(null);
		Document mOutXmlDoc = hq.service(mInXmlDoc);

		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();

		System.out.println("成功结束！");
	}
}

