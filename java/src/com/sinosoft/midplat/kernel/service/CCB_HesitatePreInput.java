package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LPYBTAppWTDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPYBTAppWTSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;


public class CCB_HesitatePreInput extends ServiceImpl {
	
	public CCB_HesitatePreInput(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_HesitatePreInput.service()...");
		 
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		LKTransStatusDB mLKTransStatusDB = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));

	        //是否需做判断银行传过来的数据与核心表中数据一致性？
			// 进行犹豫期、退保 业务处理
			
			String cx_sql = "select 1 from lktransstatus where bankcode = '03'  and transno = '" + mLCCont.getChildText("TransNo") + "' and makedate = Current Date and funcflag = '120' and descr is null with ur";
			String pd_sql = "select BankAccNo from lccont where contno = '" + mLCCont.getChildText("ContNo") + "' with ur";
			
			if(new ExeSQL().getOneValue(cx_sql).equals("1")){
				//判断保单账号与保全确认账号一致性
				if(new ExeSQL().getOneValue(pd_sql).equals(mLCCont.getChildText("BankAccNo"))){
					mOutXmlDoc = deal(pInXmlDoc);
				}else {
					throw new MidplatException("保单账号:" + new ExeSQL().getOneValue(pd_sql) + "与保全确认账号:" + mLCCont.getChildText("BankAccNo") + "不相符！！！ ");
				}
			}else {
				throw new MidplatException("该保单未进行申请退保交易！");
			}
			
			if("MQ".equals(mLCCont.getChildText("BaoQuanFlag"))){ //满期给付
				throw new MidplatException("满期给付交易未开通");
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束

		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_HesitatePreInput.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_HesitatePreInput.deal()...");
		
		Element xTranData = pInXmlDoc.getRootElement();
//		Element xLCCont = xTranData.getChild("LCCont");
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		String mBaoQuanFlag = xTranData.getChild("LCCont").getChildText("BaoQuanFlag");
		
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);
//		cont.setAppntName(xLCCont.getChildText("AppntName"));
//		cont.setAppntIDNo(xLCCont.getChildText("AppntIDNo"));
//		cont.setPrem(xLCCont.getChildText("Prem"));

		LCContSet mCont = cont.query();
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		if(mCont.size() > 0){
			LCContSchema lc = mCont.get(1);
			//插入保全表信息
			LPYBTAppWTDB pLPYBTAppWTDB = insertLPYBTAppWT(pInXmlDoc,lc);
			
			if(pLPYBTAppWTDB != null){
				mFlag.setText("1");
				mDesc.setText("交易成功！");
				LCPolDB pol_db = new LCPolDB();
				pol_db.setContNo(mContNo);
				LCPolSet polSet = pol_db.query();
				
				double mSumPrem = 0;
				String risk_main = "";
				String risk_fu = "";
				for (int i = 1; i <= polSet.size(); i++) {
					LCPolSchema mPol = polSet.get(i);
					mSumPrem += mPol.getPrem();
					if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
						risk_main = mPol.getRiskCode();
					}
					if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
						risk_fu = mPol.getRiskCode();
					}
				}
				Element nEdorAcceptNo = new Element("EdorAcceptNo");
				nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
				
				Element nContNo = new Element("ContNo");
				nContNo.setText(lc.getContNo());
				
				Element nProposalContNo = new Element("ProposalContNo");
				nProposalContNo.setText(lc.getProposalContNo());
				
				Element nCValiDate = new Element(CValiDate);
				nCValiDate.setText(lc.getCValiDate());
				
				Element nContEndDate = new Element(ContEndDate);
				nContEndDate.setText(lc.getCInValiDate());	//picch特有，保单级保单终止日期
				
				Element nAttachmentContent = new Element("AttachmentContent");
				Element nAppntName = new Element("AppntName");
				nAppntName.setText(lc.getAppntName());
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String now = sdf.format(new Date());
				Element nEdorAcceptYear = new Element("EdorAcceptYear");
				nEdorAcceptYear.setText(now.substring(0,4));
				Element nEdorAcceptMonth = new Element("EdorAcceptMonth");
				nEdorAcceptMonth.setText(now.substring(4,6));
				Element nEdorAcceptDay = new Element("EdorAcceptDay");
				nEdorAcceptDay.setText(now.substring(6,8));
				Element nApplyDate = new Element("ApplyDate");
				nApplyDate.setText(now);
				
				Element nMainRiskCodeName = new Element("MainRiskCodeName");
				
				nMainRiskCodeName.setText(getRiskRep(risk_main));
				Element nRiskCodeName = new Element("RiskCodeName");
				if(!"".equals(risk_fu)){
					nRiskCodeName.setText(getRiskRep(risk_fu));
				}
				
				//建行未传退保标志 故根据核心提供sql查询判断退保标志
				String pd_sql = "select case when customgetpoldate is null and days(current date) - days(cvalidate)<=30 then '1'when customgetpoldate is not null and days(current date) - days(customgetpoldate)<15 then '1'else '0' end from lccont where contno='" + mContNo + "' with ur";
				if(new ExeSQL().getOneValue(pd_sql).equals("1")){
					mBaoQuanFlag = "1";
				}else {
					mBaoQuanFlag = "0";
				}
				
				Element nSumPrem = new Element("SumPrem");
				if(mBaoQuanFlag.equals("1")){	//犹豫期保费
					nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
				}
				if(mBaoQuanFlag.equals("0")){	//退保保费即解约
					PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
					double pSumPrem = mPedorYBTBudgetBL.getZTmoney(lc.getContNo(),now);
					nSumPrem.setText(new DecimalFormat("0.00").format(pSumPrem));
				}
	
				nAttachmentContent.addContent(nAppntName);
				nAttachmentContent.addContent(nEdorAcceptYear);
				nAttachmentContent.addContent(nEdorAcceptMonth);
				nAttachmentContent.addContent(nEdorAcceptDay);
				nAttachmentContent.addContent(nApplyDate);
				nAttachmentContent.addContent(nMainRiskCodeName);
				nAttachmentContent.addContent(nRiskCodeName);
				nAttachmentContent.addContent(nSumPrem);
				mEdorInfo.addContent(nEdorAcceptNo);
				mEdorInfo.addContent(nCValiDate);
				mEdorInfo.addContent(nContEndDate);
				mEdorInfo.addContent(nContNo);
				mEdorInfo.addContent(nProposalContNo);
				mEdorInfo.addContent(nAttachmentContent);
			}
		}else {
			mFlag.setText("0");
			mDesc.setText("未查到对应保单信息！");
			throw new MidplatException("未查到对应保单信息！");
		}

		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out CCB_HesitatePreInput.deal()...");
		return new Document(mTranData);
	}
	
	public String getRiskRep(String riskCode){
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
	}
	
	private LPYBTAppWTDB insertLPYBTAppWT(Document pXmlDoc,LCContSchema lc) throws MidplatException {
		cLogger.info("Into CCB_HesitatePreInput.insertLPYBTAppWT()...");
		
		LPYBTAppWTSet mLPYBTAppWTSet = new LPYBTAppWTSet();
		LPYBTAppWTDB mLPYBTAppWTDB = new LPYBTAppWTDB();
		Element xTranData = pXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		
		
		String mBaoQuanFlag = "";
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTSet = mLPYBTAppWTDB.query();
		
		if(mLPYBTAppWTSet.size() > 0){	//判断保单是否已存在犹豫期退保表中！
			String mSQL = "update lktransstatus set descr = '该保单已存在保全表中，请核查！' where polno = '" + mContNo + "'and funcflag = '56' and makedate = Current Date with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if (!mExeSQL.execUpdateSQL(mSQL)) {
				cLogger.error(mExeSQL.mErrors.getFirstError());
				cLogger.warn("该保单已存在保全表中，请核查！");
			}
		}
		
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTDB.setManageCom(lc.getManageCom());
		mLPYBTAppWTDB.setAppWTDate(mCurrentDate);
		mLPYBTAppWTDB.setAppntName(lc.getAppntName());
		mLPYBTAppWTDB.setAppntSex(lc.getAppntSex());
		mLPYBTAppWTDB.setAppntBirthday(lc.getAppntBirthday());
		mLPYBTAppWTDB.setAppntIDType(lc.getAppntIDType());
		mLPYBTAppWTDB.setAppntIDNo(lc.getAppntIDNo());
		mLPYBTAppWTDB.setGBFee("0");	//保险公司定为0元
		mLPYBTAppWTDB.setAppState("1");		//需要新的状态标识
		mLPYBTAppWTDB.setoperator("ybt");
		mLPYBTAppWTDB.setTransStatus("1");	//未对账
		//建行未传退保标志 故根据核心提供sql查询判断退保标志
		String pd_sql = "select case when customgetpoldate is null and days(current date) - days(cvalidate)<=30 then '1'when customgetpoldate is not null and days(current date) - days(customgetpoldate)<15 then '1'else '0' end from lccont where contno='" + mContNo + "' with ur";
		if(new ExeSQL().getOneValue(pd_sql).equals("1")){
			mBaoQuanFlag = "WT";
		}else {
			mBaoQuanFlag = "CT";
		}
		mLPYBTAppWTDB.setEdorType(mBaoQuanFlag);	//保全类别 WT-犹豫期退保 CT-解约
		mLPYBTAppWTDB.setMakeDate(mCurrentDate);
		mLPYBTAppWTDB.setMakeTime(mCurrentTime);
		mLPYBTAppWTDB.setModifyDate(mCurrentDate);
		mLPYBTAppWTDB.setModifyTime(mCurrentTime);
		
		if (!mLPYBTAppWTDB.insert()) {
			cLogger.error(mLPYBTAppWTDB.mErrors.getFirstError());
			throw new MidplatException("插入保全表失败！");
		}
		cLogger.info("Out CCB_HesitatePreInput.insertLPYBTAppWT()...");
		return mLPYBTAppWTDB;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_HesitatePreInput.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild("LCCont");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText("PrtNo"));
		mLKTransStatusDB.setPolNo(mLCCont.getChildText("ContNo"));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Prem"));
		mLKTransStatusDB.setRiskCode(mLCCont.getChildText("RiskCode"));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_HesitatePreInput.insertTransLog()!");
		return mLKTransStatusDB;
	}
}

