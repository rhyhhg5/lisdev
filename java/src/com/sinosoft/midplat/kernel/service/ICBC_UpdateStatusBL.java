package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKNoRealTimeUWCheckDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKNoRealTimeUWCheckSchema;
import com.sinosoft.lis.vschema.LKNoRealTimeUWCheckSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/***
 * 保险公司保单状态变更文件
 * @author wz 
 * @date 20131028
 */

public class ICBC_UpdateStatusBL extends ServiceImpl  {

	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	
	public ICBC_UpdateStatusBL(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_ICBC_UpdateStatusBL.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		LKTransStatusDB mLKTransStatusDB = null;	
		
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			// 数据处理
			mOutXmlDoc = deal();
			
			mFlag.setText("0");
			mDesc.setText("交易成功！");
			mOutXmlDoc.getRootElement().getChild("RetData").addContent(mFlag);
			mOutXmlDoc.getRootElement().getChild("RetData").addContent(mDesc);
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");//执行任务完毕记
//			mLKTransStatusDB.setTransCode("1");//文件生成
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ICBC_ICBC_UpdateStatusBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal() throws Exception {
		// 
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mLCCont = new Element("LCCont");
		
		Element mChkDetails = new Element("ChkDetails");
		
		//099 理赔终止保单 （SQL由理赔提供）
		String sql_lipei = "select '099' 业务种类,a.confdate 保单状态变更日期,'020' 保险公司代码,k.bankbranch 银行地区号,c.prtno 投保单号,c.contno 保单号,p.appntname 客户姓名,p.idtype 客户证件类型,p.idno 客户证件号码,(select codename from ldcode where code = c.stateflag and codetype = 'stateflag') 保单最新状态,c.Cinvalidate  保单到期日期,'','','','' "
					+" from lpedorapp a,llcontdeal b,lccont c,lktransstatus k,lcappnt p"
					+" where 1=1"
					+" and b.contno = c.contno"
					+" and a.edoracceptno = b.edorno"
					+" and c.contno = k.polno"
					+" and c.contno = p.contno"
					+" and a.confdate = '"+ mCurrentDate +"'"
//					+" and a.confdate > '2012-1-1'"
					+" and c.cardflag = '9' and c.salechnl = '04'"
					+" and k.bankcode = '01'"
					+" union"
					+" select '099' 业务种类,a.confdate 保单状态变更日期,'020' 保险公司代码,k.bankbranch 银行地区号,c.prtno 投保单号,c.contno 保单号,p.appntname 客户姓名,p.idtype 客户证件类型,p.idno 客户证件号码,(select codename from ldcode where code = c.stateflag and codetype = 'stateflag') 保单最新状态,c.Cinvalidate  保单到期日期,'','','','' "
					+" from lpedorapp a,llcontdeal b,lbcont c,lktransstatus k,lbappnt p"
					+" where 1=1"
					+" and b.contno = c.contno"
					+" and a.edoracceptno = b.edorno"
					+" and c.contno = k.polno"
					+" and c.contno = p.contno"
					+" and a.confdate = '"+ mCurrentDate +"'"
//					+" and a.confdate > '2012-1-1'"
					+" and c.cardflag = '9' and c.salechnl = '04'"
					+" and k.bankcode = '01'"
					+" with ur";
		
		ExeSQL es = new ExeSQL();
		SSRS ssrs = es.execSQL(sql_lipei);
		cLogger.info("查出理赔终止数据总条数为: "+ ssrs.MaxRow);
		//组织数据
		for (int i = 1; i <= ssrs.MaxRow; i++) {
			Element mChkDetail = new Element("ChkDetail");
			
			//交易日期
			Element mMakeDate = new Element("MakeDate");
			mMakeDate.setText(mCurrentDate.replace("-", ""));
			
			//业务种类
			Element mBusinspec = new Element("Businspec");
			mBusinspec.setText(ssrs.GetText(i, 1));
			
			//业务变更日期
			Element mBusinUpdateDate = new Element("BusinUpdateDate");
			mBusinUpdateDate.setText(ssrs.GetText(i, 2).replace("-", ""));
			
			//保险公司代码
			Element mSafeCode = new Element("SafeCode");
			mSafeCode.setText("020");
			
			//银行地区号
			Element mZoneNo = new Element("ZoneNo");
			String xZoneno = ssrs.GetText(i, 4);
			if(xZoneno.length() <= 5){
				xZoneno = "00000".substring(0,5-xZoneno.length()) + xZoneno;
			}
			mZoneNo.setText(xZoneno);
			
			//投保单号
			Element mPrtNo = new Element("PrtNo");
			mPrtNo.setText(ssrs.GetText(i, 5));
			
			//保单合同号
			Element mContNo = new Element("ContNo");
			mContNo.setText(ssrs.GetText(i, 6));
			
			//客户姓名
			Element mAppntName = new Element("AppntName");
			mAppntName.setText(ssrs.GetText(i, 7));
			
			//客户证件类型
			Element mIDType = new Element("IDType");
			mIDType.setText(ssrs.GetText(i, 8));
			
			//客户证件号码
			Element mIDNo = new Element("IDNo");
			mIDNo.setText(ssrs.GetText(i, 9));
			
			//保单最新状态
			Element mContStatus = new Element("ContStatus");// 默认理赔终止 	14-犹豫期退保保单已终止 20-退保终止 21-理赔终止 22-其他保单失效状态  23-满期给付终止(银行端释义)
			mContStatus.setText("21");
			
			//保单到期日期
			Element mContEndDate = new Element("ContEndDate");
			mContEndDate.setText(ssrs.GetText(i, 11).replace("-", ""));
			
			//备用字段1  (保全业务发生金额)
			Element mTempe1 = new Element("Tempe1");
			mTempe1.setText("0");
			
			//备用字段2 (增量现金价值，以分为单位的整数)
			Element mTempe2 = new Element("Tempe2");
			mTempe2.setText("0");
			
			//备用字段3  (最终现金价值，以分为单位的整数)
			Element mTempe3 = new Element("Tempe3");
			mTempe3.setText("0");
			
			//备用字段4
			Element mTempe4 = new Element("Tempe4");
			mTempe4.setText("");
			
			mChkDetail.addContent(mMakeDate);
			mChkDetail.addContent(mBusinspec);
			mChkDetail.addContent(mBusinUpdateDate);
			mChkDetail.addContent(mSafeCode);
			mChkDetail.addContent(mZoneNo);
			mChkDetail.addContent(mPrtNo);
			mChkDetail.addContent(mContNo);
			mChkDetail.addContent(mAppntName);
			mChkDetail.addContent(mIDType);
			mChkDetail.addContent(mIDNo);
			mChkDetail.addContent(mContStatus);
			mChkDetail.addContent(mContEndDate);
			mChkDetail.addContent(mTempe1);
			mChkDetail.addContent(mTempe2);
			mChkDetail.addContent(mTempe3);
			mChkDetail.addContent(mTempe4);
			
			mChkDetails.addContent(mChkDetail);
		}
		
		//001满期给付(SQL由保全提供)
		String sql_BQ_manqi = "select '001' as 业务种类,current date as 保单状态变更日期,'020' 保险公司代码,'k.bankbranch' 银行地区号,'c.prtno' 投保单号,"
							+" (select distinct contno from ljsgetdraw where getnoticeno = a.actugetno) as 保单号,"
							+" '','','',"
							+" (select codename from ldcode where codetype='stateflag' and code=("
							+" select stateflag from lccont where contno=("
							+" select distinct contno from ljsgetdraw where getnoticeno = a.actugetno))) as 保单最新状态,"
							+" (select cinvalidate from lccont where contno=("
							+" select distinct contno from ljsgetdraw where getnoticeno = a.actugetno)) as 保单到期日期,"
							+" a.sumgetmoney as 保全业务发生金额 ,0,0,0"
							+" from ljaget a"
							+" where exists (select 1 from ljsgetdraw where getnoticeno = a.actugetno)"
							+" and a.makedate = '"+ mCurrentDate +"'"
//							+" and a.makedate > '2013-5-1'"
							+" with ur ";
		ssrs = es.execSQL(sql_BQ_manqi);
		String xContNo = "";
		int manqi_index = 0;
		for (int i = 1; i <= ssrs.MaxRow; i++) {
			String sql_man = "select a.prtno,b.appntname,b.idtype,b.idno,k.bankbranch from lccont a , lcappnt b , lktransstatus k"
						+" where 1 = 1"
						+" and a.contno = b.contno"
						+" and a.contno = k.polno"
						+" and a.cardflag = '9' and a.salechnl = '04'"
						+" and k.bankcode = '01'"
						+" and a.contno = '"+ ssrs.GetText(i, 6) +"' with ur";
			SSRS ssrs_t = es.execSQL(sql_man);
			if(ssrs_t.MaxRow <= 0 || xContNo.equals(ssrs.GetText(i, 6))){
				continue;
			}
			xContNo = ssrs.GetText(i, 6);
			
			Element mChkDetail = new Element("ChkDetail");
			
			//交易日期
			Element mMakeDate = new Element("MakeDate");
			mMakeDate.setText(mCurrentDate.replace("-", ""));
			
			//业务种类
			Element mBusinspec = new Element("Businspec");
			mBusinspec.setText(ssrs.GetText(i, 1));
			
			//业务变更日期
			Element mBusinUpdateDate = new Element("BusinUpdateDate");
			mBusinUpdateDate.setText(ssrs.GetText(i, 2).replace("-", ""));
			
			//保险公司代码
			Element mSafeCode = new Element("SafeCode");
			mSafeCode.setText("020");
			
			//银行地区号
			Element mZoneNo = new Element("ZoneNo");
			String xZoneno = ssrs_t.GetText(1, 5);
			if(xZoneno.length() <= 5){
				xZoneno = "00000".substring(0,5-xZoneno.length()) + xZoneno;
			}
			mZoneNo.setText(xZoneno);
			
			//投保单号
			Element mPrtNo = new Element("PrtNo");
			mPrtNo.setText(ssrs_t.GetText(1, 1));
			
			//保单合同号
			Element mContNo = new Element("ContNo");
			mContNo.setText(ssrs.GetText(i, 6));
			
			//客户姓名
			Element mAppntName = new Element("AppntName");
			mAppntName.setText(ssrs_t.GetText(1, 2));
			
			//客户证件类型
			Element mIDType = new Element("IDType");
			mIDType.setText(ssrs_t.GetText(1, 3));
			
			//客户证件号码
			Element mIDNo = new Element("IDNo");
			mIDNo.setText(ssrs_t.GetText(1, 4));
			
			//保单最新状态
			Element mContStatus = new Element("ContStatus");// 默认理赔终止 	14-犹豫期退保保单已终止 20-退保终止 21-理赔终止 22-其他保单失效状态  23-满期给付终止(银行端释义)
			mContStatus.setText("23");
			
			//保单到期日期
			Element mContEndDate = new Element("ContEndDate");
			mContEndDate.setText(ssrs.GetText(i, 11).replace("-", ""));
			
			//备用字段1  (保全业务发生金额)
			Element mTempe1 = new Element("Tempe1");
			mTempe1.setText((int)(Double.parseDouble(ssrs.GetText(i, 12))*100) + "");
			
			//备用字段2 (增量现金价值，以分为单位的整数)
			Element mTempe2 = new Element("Tempe2");
			mTempe2.setText(ssrs.GetText(i, 13));
			
			//备用字段3  (最终现金价值，以分为单位的整数)
			Element mTempe3 = new Element("Tempe3");
			mTempe3.setText(ssrs.GetText(i, 14));
			
			//备用字段4
			Element mTempe4 = new Element("Tempe4");
			mTempe4.setText("");
			
			mChkDetail.addContent(mMakeDate);
			mChkDetail.addContent(mBusinspec);
			mChkDetail.addContent(mBusinUpdateDate);
			mChkDetail.addContent(mSafeCode);
			mChkDetail.addContent(mZoneNo);
			mChkDetail.addContent(mPrtNo);
			mChkDetail.addContent(mContNo);
			mChkDetail.addContent(mAppntName);
			mChkDetail.addContent(mIDType);
			mChkDetail.addContent(mIDNo);
			mChkDetail.addContent(mContStatus);
			mChkDetail.addContent(mContEndDate);
			mChkDetail.addContent(mTempe1);
			mChkDetail.addContent(mTempe2);
			mChkDetail.addContent(mTempe3);
			mChkDetail.addContent(mTempe4);
			manqi_index ++;
			
			mChkDetails.addContent(mChkDetail);
		}
		cLogger.info("查出满期给付数据总条数为: "+ manqi_index);
		
		//002犹豫期撤保、退保(SQL由保全提供)
		String sql_BQ_tuibao = "select (select edorname from lmedoritem where edorcode=a.edortype FETCH FIRST 1 rows only) as 业务种类,"
							+" (select confdate from lpedorapp where edoracceptno=a.edoracceptno) as  保单状态变更日期 ,"
							+" '020' 保险公司代码,"
							+" 'k.bankbranch' 银行地区号,"
							+" 'c.prtno' 投保单号,"
							+" a.contno as 保单号,"
							+" 'p.appntname' 客户姓名,"
							+" 'p.idtype' 客户证件类型,"
							+" 'p.idno' 客户证件号码,"
							+" (select codename from ldcode where codetype='stateflag' and code=("
							+" select stateflag from lbcont where contno=a.contno"
							+" union select stateflag from lccont where contno=a.contno fetch first 1 rows only"
							+" )) as 保单最新状态,"
							+" (select cinvalidate from lccont where contno=a.contno"
							+" union select cinvalidate from lbcont where contno=a.contno fetch first 1 rows only"
							+" ) as 保单到期日期,"
							+" a.getmoney as 保全业务发生金额,0,0,0"
							+" from lpedoritem a "
							+" where exists (select 1 from lpedorapp "
							+" where edoracceptno=a.edoracceptno"
							+"  and edorstate='0' "
							+" and confdate is not null"
							+" and confdate = '"+ mCurrentDate +"')"
//							+" and confdate > '2013-5-5')"
							+" and edortype in ('WT','XT','CT')"
							+" and operator != 'LP'"
							+"  with ur";
		ssrs = es.execSQL(sql_BQ_tuibao);
		String tContNo = "";
		int tuibao_index = 0;
		for (int i = 1; i <= ssrs.MaxRow; i++) {
			String sql_man = "select a.prtno,b.appntname,b.idtype,b.idno,k.bankbranch from lccont a , lcappnt b , lktransstatus k"
						+" where 1 = 1"
						+" and a.contno = b.contno"
						+" and a.contno = k.polno"
						+" and a.cardflag = '9' and a.salechnl = '04'"
						+" and k.bankcode = '01'"
						+" and a.contno = '"+ ssrs.GetText(i, 6) +"' "
						+" union"
						+" select a.prtno,b.appntname,b.idtype,b.idno,k.bankbranch from lbcont a , lbappnt b , lktransstatus k"
						+" where 1 = 1"
						+" and a.contno = b.contno"
						+" and a.contno = k.polno"
						+" and a.cardflag = '9' and a.salechnl = '04'"
						+" and k.bankcode = '01'"
						+" and a.contno = '"+ ssrs.GetText(i, 6) +"' with ur";
			SSRS ssrs_t = es.execSQL(sql_man);
			if(ssrs_t.MaxRow <= 0 || tContNo.equals(ssrs.GetText(i, 6))){
				continue;
			}
			tContNo = ssrs.GetText(i, 6);
			
			Element mChkDetail = new Element("ChkDetail");
			
			//交易日期
			Element mMakeDate = new Element("MakeDate");
			mMakeDate.setText(mCurrentDate.replace("-", ""));
			
			//业务种类
			Element mBusinspec = new Element("Businspec");
			String tBusin = ssrs.GetText(i, 1);
			if("保单犹豫期退保".equals(tBusin)){
				mBusinspec.setText("002");
			}else {
				mBusinspec.setText("003");
			}
			
			//业务变更日期
			Element mBusinUpdateDate = new Element("BusinUpdateDate");
			mBusinUpdateDate.setText(ssrs.GetText(i, 2).replace("-", ""));
			
			//保险公司代码
			Element mSafeCode = new Element("SafeCode");
			mSafeCode.setText("020");
			
			//银行地区号
			Element mZoneNo = new Element("ZoneNo");
			String xZoneno = ssrs_t.GetText(1, 5);
			if(xZoneno.length() <= 5){
				xZoneno = "00000".substring(0,5-xZoneno.length()) + xZoneno;
			}
			mZoneNo.setText(xZoneno);
			
			//投保单号
			Element mPrtNo = new Element("PrtNo");
			mPrtNo.setText(ssrs_t.GetText(1, 1));
			
			//保单合同号
			Element mContNo = new Element("ContNo");
			mContNo.setText(ssrs.GetText(i, 6));
			
			//客户姓名
			Element mAppntName = new Element("AppntName");
			mAppntName.setText(ssrs_t.GetText(1, 2));
			
			//客户证件类型
			Element mIDType = new Element("IDType");
			mIDType.setText(ssrs_t.GetText(1, 3));
			
			//客户证件号码
			Element mIDNo = new Element("IDNo");
			mIDNo.setText(ssrs_t.GetText(1, 4));
			
			//保单最新状态
			Element mContStatus = new Element("ContStatus");// 默认理赔终止 	14-犹豫期退保保单已终止 20-退保终止 21-理赔终止 22-其他保单失效状态  23-满期给付终止(银行端释义)
			if("保单犹豫期退保".equals(tBusin)){
				mContStatus.setText("14");
			}else {
				mContStatus.setText("20");
			}
			
			//保单到期日期
			Element mContEndDate = new Element("ContEndDate");
			mContEndDate.setText(ssrs.GetText(i, 11).replace("-", ""));
			
			//备用字段1  (保全业务发生金额)
			Element mTempe1 = new Element("Tempe1");
			mTempe1.setText((int)(Double.parseDouble(ssrs.GetText(i, 12))*100) + "");
			
			//备用字段2 (增量现金价值，以分为单位的整数)
			Element mTempe2 = new Element("Tempe2");
			mTempe2.setText(ssrs.GetText(i, 13));
			
			//备用字段3  (最终现金价值，以分为单位的整数)
			Element mTempe3 = new Element("Tempe3");
			mTempe3.setText(ssrs.GetText(i, 14));
			
			//备用字段4
			Element mTempe4 = new Element("Tempe4");
			mTempe4.setText("");
			
			mChkDetail.addContent(mMakeDate);
			mChkDetail.addContent(mBusinspec);
			mChkDetail.addContent(mBusinUpdateDate);
			mChkDetail.addContent(mSafeCode);
			mChkDetail.addContent(mZoneNo);
			mChkDetail.addContent(mPrtNo);
			mChkDetail.addContent(mContNo);
			mChkDetail.addContent(mAppntName);
			mChkDetail.addContent(mIDType);
			mChkDetail.addContent(mIDNo);
			mChkDetail.addContent(mContStatus);
			mChkDetail.addContent(mContEndDate);
			mChkDetail.addContent(mTempe1);
			mChkDetail.addContent(mTempe2);
			mChkDetail.addContent(mTempe3);
			mChkDetail.addContent(mTempe4);
			tuibao_index ++;
			
			mChkDetails.addContent(mChkDetail);
		}
		cLogger.info("退保、犹豫期退保总共条数："+tuibao_index);
		// 004续期交费现金价值更新(理赔反应续期不会有现金价值更新)
		
		// 005追加投保现金价值更新
		
		// 006其他保单失效类业务(SQL由保全提供)
		String sql_BQ_shixiao = "select '006' 业务种类,current date as 保单状态变更日期,'020' 保险公司代码,k.bankbranch 银行地区号,"
							+" a.prtno 投保单号,a.contno as 保单号,"
							+" b.appntname 客户姓名,"
							+" b.idtype 客户证件类型,"
							+" b.idno 客户证件号码,"
							+" '22' as 保单最新状态,"
							+" a.cinvalidate as 保单到期日期,"
							+" 0 as 保全业务发生金额,0,0,0"
							+" from lccont a , lcappnt b , lktransstatus k"
							+" where 1 = 1"
							+" and a.contno = b.contno"
							+" and a.contno = k.polno"
							+" and a.cardflag = '9' and a.salechnl = '04'"
							+" and k.bankcode = '01'"
							+" and a.stateflag='2'"
							+" and a.conttype='1'"
							+" and a.modifydate = '"+ mCurrentDate +"'"
//							+" and a.modifydate > '2013-10-8'"
							+" with ur ";
		ssrs = es.execSQL(sql_BQ_shixiao);
		cLogger.info("查出其他保单失效数据总条数为: "+ ssrs.MaxRow);
		for (int i = 1; i <= ssrs.MaxRow; i++) {
		Element mChkDetail = new Element("ChkDetail");
		
		//交易日期
		Element mMakeDate = new Element("MakeDate");
		mMakeDate.setText(mCurrentDate.replace("-", ""));
		
		//业务种类
		Element mBusinspec = new Element("Businspec");
		mBusinspec.setText(ssrs.GetText(i, 1));
		
		//业务变更日期
		Element mBusinUpdateDate = new Element("BusinUpdateDate");
		mBusinUpdateDate.setText(ssrs.GetText(i, 2).replace("-", ""));
		
		//保险公司代码
		Element mSafeCode = new Element("SafeCode");
		mSafeCode.setText("020");
		
		//银行地区号
		Element mZoneNo = new Element("ZoneNo");
		String xZoneno = ssrs.GetText(i, 4);
		if(xZoneno.length() <= 5){
			xZoneno = "00000".substring(0,5-xZoneno.length()) + xZoneno;
		}
		mZoneNo.setText(xZoneno);
		
		//投保单号
		Element mPrtNo = new Element("PrtNo");
		mPrtNo.setText(ssrs.GetText(i, 5));
		
		//保单合同号
		Element mContNo = new Element("ContNo");
		mContNo.setText(ssrs.GetText(i, 6));
		
		//客户姓名
		Element mAppntName = new Element("AppntName");
		mAppntName.setText(ssrs.GetText(i, 7));
		
		//客户证件类型
		Element mIDType = new Element("IDType");
		mIDType.setText(ssrs.GetText(i, 8));
		
		//客户证件号码
		Element mIDNo = new Element("IDNo");
		mIDNo.setText(ssrs.GetText(i, 9));
		
		//保单最新状态
		Element mContStatus = new Element("ContStatus");// 默认理赔终止 	14-犹豫期退保保单已终止 20-退保终止 21-理赔终止 22-其他保单失效状态  23-满期给付终止(银行端释义)
		mContStatus.setText("22");
		
		//保单到期日期
		Element mContEndDate = new Element("ContEndDate");
		mContEndDate.setText(ssrs.GetText(i, 11).replace("-", ""));
		
		//备用字段1  (保全业务发生金额)
		Element mTempe1 = new Element("Tempe1");
		mTempe1.setText((int)(Double.parseDouble(ssrs.GetText(i, 12))*100) + "");
		
		//备用字段2 (增量现金价值，以分为单位的整数)
		Element mTempe2 = new Element("Tempe2");
		mTempe2.setText(ssrs.GetText(i, 13));
		
		//备用字段3  (最终现金价值，以分为单位的整数)
		Element mTempe3 = new Element("Tempe3");
		mTempe3.setText(ssrs.GetText(i, 14));
		
		//备用字段4
		Element mTempe4 = new Element("Tempe4");
		mTempe4.setText("");
		
		mChkDetail.addContent(mMakeDate);
		mChkDetail.addContent(mBusinspec);
		mChkDetail.addContent(mBusinUpdateDate);
		mChkDetail.addContent(mSafeCode);
		mChkDetail.addContent(mZoneNo);
		mChkDetail.addContent(mPrtNo);
		mChkDetail.addContent(mContNo);
		mChkDetail.addContent(mAppntName);
		mChkDetail.addContent(mIDType);
		mChkDetail.addContent(mIDNo);
		mChkDetail.addContent(mContStatus);
		mChkDetail.addContent(mContEndDate);
		mChkDetail.addContent(mTempe1);
		mChkDetail.addContent(mTempe2);
		mChkDetail.addContent(mTempe3);
		mChkDetail.addContent(mTempe4);
		
		mChkDetails.addContent(mChkDetail);
	}
		
		// 007 分红现金价值更新

		
		mLCCont.addContent(mChkDetails);
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		cLogger.info("Into ICBC_ICBC_UpdateStatusBL.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out ICBC_ICBC_UpdateStatusBL.insertTransLog()...");
		return mLKTransStatusDB;
	}

}
