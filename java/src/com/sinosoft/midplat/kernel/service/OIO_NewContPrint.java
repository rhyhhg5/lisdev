package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.JdomUtil;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class OIO_NewContPrint extends ServiceImpl {
	
	public OIO_NewContPrint(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into OIO_NewContPrint.service()……");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		// add by fengzhijun
		Element mTranData = pInXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);

		try {
			// 插入日志
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

			String mCardNo = mLKTransStatusDB.getProposalNo();
			String mContNo = mLCCont.getChildText("ContNo");
			
			if (null == mContNo || "".equals(mContNo) && null == mCardNo
					|| "".equals(mCardNo)) {
				throw new MidplatException("相关号码不能为空！");
			}

			// 查询保单信息
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mContNo);
			LCContSet tLCContSet = tLCContDB.query();
			if ((null == tLCContSet) || (tLCContSet.size() < 1)) {
				throw new MidplatException("查询保单数据失败！");
			}else{
				// 单证核销！校验在核销的过程中已经做了
				if (!mLKTransStatusDB.getPolNo().startsWith("YBT")) {
					new CardManage("HB07/07B", mGlobalInput).makeUsed(mLKTransStatusDB.getProposalNo());
					
					LCContSchema tLCContSchema = tLCContSet.get(1);
					tLCContDB.setSchema(tLCContSchema);
					tLCContDB.setProposalContNo(mCardNo);
					if (!tLCContDB.update()) {
						throw new MidplatException("更新单证失败");
					}
				}
			}

			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(mContNo);
			mOutXmlDoc = tYbtContQueryBL.deal();
		
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setPrtNo(tOutLCCont.getChildText(PrtNo));
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setStatus("1");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		//更新日志表
		
		cLogger.info("Out OIO_NewContPrint.service()……");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into OIO_NewContPrint.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);

		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		cLogger.info("-----------------------" + mLCCont.getChild("Detail_List").getChild("detail").getChildText(CardNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChild("Detail_List").getChild("detail").getChildText(CardNo));
		// mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));

		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out OIO_NewContPrint.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		String filepath = "E:\\ccb_CCB000000000111118_print_153950.xml";
		InputStream pIs = new FileInputStream(filepath);
		Document doc = JdomUtil.build(pIs);
		OIO_NewContPrint np = new OIO_NewContPrint(null);
		np.service(doc);
	}

}
