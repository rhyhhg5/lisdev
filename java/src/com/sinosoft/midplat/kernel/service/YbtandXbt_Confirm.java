package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class YbtandXbt_Confirm extends SimpService {
	public YbtandXbt_Confirm(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into YbtandXbt_Confirm.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		Element mTranData = pInXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		try {
			String mPrtNo = mLCCont.getChildText(PrtNo);
			String mSQL = "select 1 from lktransstatus where PrtNo='" + mPrtNo
					+ "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(mSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}

			//针对不传ZoneNo情况进行特殊处理
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String mZoneNo = mBaseInfo.getChildText("ZoneNo");
			if(mZoneNo == null || mZoneNo.equals("")){
				String ZoneNos = new ExeSQL().getOneValue(zSQL);
				mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
			}
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			LCContDB mLCContDB = new LCContDB();
			mLCContDB.setPrtNo(mPrtNo);
			LCContSet mLCContSet = mLCContDB.query();
			String mCardFlag = mLCContSet.get(1).getCardFlag();

			if (mCardFlag.equals("9") || mCardFlag.equals("c") || mCardFlag.equals("d") || mCardFlag.equals("e")) { // 银保
				mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
						.getBankCode(), mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			} else if (mCardFlag.equals("a")) { // 信保
				mGlobalInput = YbtSufUtil.getGlobalInput1(mLKTransStatusDB
						.getBankCode(), mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			}

			// 财务收费
			YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(mLKTransStatusDB
					.getPrtNo(), mGlobalInput);
			tYbtTempFeeBL.deal();

			// 签单+回执回销
			YbtLCContSignBL tYbtLCContSignBL = new YbtLCContSignBL(
					mLKTransStatusDB.getPrtNo(), mGlobalInput);
			tYbtLCContSignBL.deal();

			// 组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(
					tYbtLCContSignBL.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();

			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			
			// 查找试算交易流水号
			mSQL = "select transno from lktransstatus where funcflag = '01' "
					+ "and prtno = '" + mLKTransStatusDB.getPrtNo() + "' "
					+ " and makedate = Current Date and descr is null with ur";
			String mOldTranNo = new ExeSQL().getOneValue(mSQL); 

			// 查找险种编码
			mSQL = "select riskcode from lktransstatus where funcflag = '01' "
					+ "and prtno = '" + mLKTransStatusDB.getPrtNo() + "' "
					+ "and rcode = '1' " + "with ur";
			String mRiskCode = new ExeSQL().getOneValue(mSQL); // 险种编码
			if (mCardFlag.equals("9")) { // 走银保
				// 单证核销！
				if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
					new CardManage("HB07/07B", mGlobalInput)
							.makeUsed(tOutLCCont.getChildText(ProposalContNo));
				}
			} else if (mCardFlag.equals("a")) { // 走信保
				String mCertifyCode = null;// 单证编码
				String mProposalContNoNew = null;// 信保通单证操作使用,需要去掉远单证号前两位
				ExeSQL mExeSQL = new ExeSQL();
				mSQL = "select certifycode from lkcertifymapping where bankcode = '"
						+ mLKTransStatusDB.getBankCode()
						+ "' "
						+ "and managecom = '"
						+ mGlobalInput.ManageCom.substring(0, 4)
						+ "' "
						+ "and RiskWrapCode = '" + mRiskCode + "' " + "with ur";
				SSRS tSSRS = mExeSQL.execSQL(mSQL);
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的单证编码!");
				}
				mCertifyCode = tSSRS.GetText(1, 1);// 农信社单证编码需要从单证编码定义表中查询
			
				
				if(mGlobalInput.ManageCom.startsWith("8635") && mGlobalInput.AgentCom.startsWith("PY053")){
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo);
					new CardManage(mCertifyCode, mGlobalInput).makeUsed(mProposalContNoNew);
				}else if(mLKTransStatusDB.getBankCode().equals("60")){
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(3, 12);// 江西 20181102
					if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) 
					{
						new CardManage(mCertifyCode, mGlobalInput)
						.makeUsed(mProposalContNoNew);
				    }
				}
				else{
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(2, 12);// 信保通单证操作需要去掉前两位
					if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) 
					{
						new CardManage(mCertifyCode, mGlobalInput)
						.makeUsed(mProposalContNoNew);
				    }
				}
				
				
			}

			// 银保通交叉销售
			YbtSufUtil
					.ybtMixSale(mGlobalInput, tOutLCCont.getChildText(ContNo));

			// 更新相关日志
			String tCurTime = DateUtil.getCurDate("HH:mm:ss");
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setProposalNo(tOutLCCont
					.getChildText(ProposalContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setModifyTime(tCurTime);

			LKTransStatusDB tOldLKTransStatusDB = new LKTransStatusDB();
			tOldLKTransStatusDB.setBankCode(mLKTransStatusDB.getBankCode());
			tOldLKTransStatusDB.setBankBranch(mLKTransStatusDB.getBankBranch());
			tOldLKTransStatusDB.setBankNode(mLKTransStatusDB.getBankNode());
			tOldLKTransStatusDB.setProposalNo(mLKTransStatusDB.getProposalNo());
			tOldLKTransStatusDB.setRCode("1");
			LKTransStatusSet ttLKTransStatusSet = tOldLKTransStatusDB.query();
			if ((null == ttLKTransStatusSet) || (ttLKTransStatusSet.size() < 1)) {
				throw new MidplatException("查询原始日志信息失败！");
			}
			LKTransStatusSchema tLKTransStatusSchema = ttLKTransStatusSet
					.get(1);
			tLKTransStatusSchema.setPolNo(tOutLCCont.getChildText(ContNo));
			tLKTransStatusSchema.setTransAmnt(tOutLCCont.getChildText(Prem));
			tLKTransStatusSchema.setTransStatus("1");
			tLKTransStatusSchema.setStatus("1");
			tLKTransStatusSchema
					.setModifyDate(mLKTransStatusDB.getModifyDate());
			tLKTransStatusSchema.setModifyTime(tCurTime);

			MMap tSubmitMMap = new MMap();
			tSubmitMMap.put(mLKTransStatusDB.getSchema(), "UPDATE");
			tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
			VData tSubmitVData = new VData();
			tSubmitVData.add(tSubmitMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tSubmitVData, "")) {
				cLogger.error(tPubSubmit.mErrors.getFirstError());
				throw new MidplatException("更新日志失败！");
			}
			
			String ssql = "select ServiceStartTime,ServiceEndTime,TempFeeNo,bak3,EdorNo from lktransStatus where bankcode = '"+mLKTransStatusDB.getBankCode()+"'" +
			" and BankBranch = '"+mLKTransStatusDB.getBankBranch()+"' and BankNode = '"+mLKTransStatusDB.getBankNode()+"'" +
			" and ProposalNo = '"+mLKTransStatusDB.getProposalNo()+"' and RCode = '1' and FuncFlag = '01' order by makedate desc , maketime desc with ur";
			String sql = "SELECT ConsignNo FROM  LCCONT  WHERE CONTNO = "+"\'"+mLKTransStatusDB.getPolNo()+"\'";
			ExeSQL mExeSQL = new ExeSQL();
			SSRS mSSRS = mExeSQL.execSQL(ssql);				
			Element xLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
		    xLCCont.getChild("LoanDate").setText(mSSRS.GetText(1, 1));
		    xLCCont.getChild("LoanEndDate").setText(mSSRS.GetText(1, 2));
		    xLCCont.getChild("LoanInvoiceNo").setText(mSSRS.GetText(1, 3));
		    if(mSSRS.GetText(1, 4).equals("") || mSSRS.GetText(1,4) == null){
		    	SSRS mSS = mExeSQL.execSQL(sql);
		    	xLCCont.getChild("ConsignNo").setText(mSS.GetText(1, 1));
		    	JdomUtil.print(xLCCont);
		    }
		   
		    
		    xLCCont.getChild("LoanContractAmt").setText(mSSRS.GetText(1, 5));
			    
		    
		    /**
			 * 乌鲁木齐三号文要求增加销售人员工号、销售人员姓名、销售人员资格证号 
			 * add by LiXiaoLong 20140411 begin
			 * **/
			if(mLKTransStatusDB.getBankCode().equals("52")){
				
				Element pTranData = mOutXmlDoc.getRootElement();
				Element pLCCont = pTranData.getChild(LCCont);
				
				mSQL = "select bak1,bak2,bankacc,bak3,bak4 from lktransstatus where transno = '" + mOldTranNo + "' and bankcode = '52' and makedate = Current Date and descr is null with ur";
				
				SSRS ssrs= new ExeSQL().execSQL(mSQL);
				String pBrName = ssrs.GetText(1, 1);
				String pSaleCertNo = ssrs.GetText(1, 2);
				String pBrCertID = ssrs.GetText(1, 3);
				String pManagerNo = ssrs.GetText(1, 4);
				String pSaleCertName = ssrs.GetText(1, 5);
				Element mBrName = new Element("BrName").setText(pBrName);
				Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
				Element mBrCertID = new Element("BrCertID").setText(pBrCertID);
				Element ManagerNo = new Element("ManagerNo").setText(pManagerNo);
				Element mSaleCertName = new Element("SaleCertName").setText(pSaleCertName);
				
				pLCCont.addContent(mBrName);
				pLCCont.addContent(mSaleCertNo);
				pLCCont.addContent(mBrCertID);
				pLCCont.addContent(ManagerNo);
				pLCCont.addContent(mSaleCertName);
			}
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			try {
				YbtSufUtil.clearData(mLKTransStatusDB.getPrtNo());
			} catch (Exception tBaseEx) {
				cLogger.error("删除新单数据失败！", tBaseEx);
			}

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回

				String ttDesr = ex.getMessage();
				try {
					if (null != ttDesr
							&& ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex) {
				}
				mLKTransStatusDB.setDescr(ttDesr);

				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！"
							+ mLKTransStatusDB.mErrors.getFirstError());
				}
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out YbtandXbt_Confirm.service()!");
		
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into YbtandXbt_Confirm.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTemp("1"); // 确认/取消标识：0-取消；1-确认
		mLKTransStatusDB.setStatus("0"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out YbtandXbt_Confirm.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
