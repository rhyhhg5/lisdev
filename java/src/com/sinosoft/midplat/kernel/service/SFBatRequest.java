package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.yinbaotongbank.BXHReturnFileSave;
import com.sinosoft.lis.yinbaotongbank.ReturnFileSave;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.NumberUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.JdomUtil;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class SFBatRequest extends ServiceImpl {
    private String cBatType = "";
    private String cSerialNo = "";

    public SFBatRequest(Element pThisBusiConf) {
        super(pThisBusiConf);
    }

    public Document service(Document pInXmlDoc) {
        cLogger.info("Into BatRequest.service()...");
        Document mOutXmlDoc = null;
        LKTransStatusDB mLKTransStatusDB = null;
        GlobalInput mGlobalInput = null;

        try {
            mLKTransStatusDB = insertTransLog(pInXmlDoc);

            mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
                    .getBankCode(), mLKTransStatusDB.getBankBranch(),
                    mLKTransStatusDB.getBankNode());

            /**
             * 调用核心批量取盘接口
             */
            String mManageCom = "";
            String mBankCode = mLKTransStatusDB.getBankCode();
            mManageCom = mGlobalInput.ManageCom;
            cBatType = mLKTransStatusDB.getTempFeeNo();
            String mSql = "select bankcode from ldbank where operator = 'ybt' and agentcode = '"
                    + mBankCode
                    + "' and comcode = '"
                    + mManageCom
                    + "' with ur";
            mBankCode = new ExeSQL().getOneValue(mSql);
            cLogger.info("银行编码:" + mBankCode + ",批量取盘管理机构：" + mManageCom
                    + ",类型:" + cBatType);
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("ComCode", mManageCom);
            mTransferData.setNameAndValue("BankCode", mBankCode);
            mTransferData.setNameAndValue("DealType", cBatType);
            mTransferData.setNameAndValue("SendDate", DateUtil
                    .getCurDate("yyyy-MM-dd"));

            LYSendToBankSet mLYSendToBankSet = (LYSendToBankSet) new BXHReturnFileSave()
                    .getData(mTransferData);

            Document mDoc = createStdXml(mLYSendToBankSet);
			cLogger.info("查看配置文件");
			JdomUtil.print(cThisBusiConf);
			cLogger.info("查看配置文件");
            // 存储核心接口报文
            FileOutputStream mFos = new FileOutputStream(cThisBusiConf
                    .getChildText(path)
                    + "REQ_" + mLKTransStatusDB.getBankAcc() + ".xml");
            JdomUtil.output(mDoc, mFos);
            mFos.close();

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
            List mTranDataList = mDoc.getRootElement().getChildren();
            for (int i = 0; i < mTranDataList.size(); i++) {
                mOutXmlDoc.getRootElement().addContent(
                        (Element) ((Element) mTranDataList.get(i)).clone());
            }

            mLKTransStatusDB.setEdorNo(cSerialNo); // 借用做批次号
            mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
            mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        } catch (Exception ex) {
            cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

            if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
                mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
                mLKTransStatusDB.setDescr(ex.getMessage());
            }

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
        }

        if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
            if (null != mGlobalInput) {
                mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
            }
            mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
            if (!mLKTransStatusDB.update()) {
                cLogger.error("更新日志信息失败！"
                        + mLKTransStatusDB.mErrors.getFirstError());
            }
        }

        cLogger.info("Out BatRequest.service()!");
        return mOutXmlDoc;
    }

    public LKTransStatusDB insertTransLog(Document pXmlDoc)
            throws MidplatException {
        cLogger.info("Into BatRequest.insertTransLog()...");

        Element mTranData = pXmlDoc.getRootElement();
        Element mBaseInfo = mTranData.getChild(BaseInfo);

        String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
        String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

        LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
        mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
        mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
        mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
        mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
        mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
        mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
        mLKTransStatusDB.setBankAcc(mBaseInfo.getChildText("FileName")); // 借用做文件名称
        mLKTransStatusDB.setTempFeeNo(mBaseInfo.getChildText("DealType")); // 借用做业务类型S/F
        mLKTransStatusDB.setTransDate(mCurrentDate);
        mLKTransStatusDB.setTransTime(mCurrentTime);
        mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        mLKTransStatusDB.setMakeDate(mCurrentDate);
        mLKTransStatusDB.setMakeTime(mCurrentTime);
        mLKTransStatusDB.setModifyDate(mCurrentDate);
        mLKTransStatusDB.setModifyTime(mCurrentTime);

        if (!mLKTransStatusDB.insert()) {
            cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
            throw new MidplatException("插入日志失败！");
        }

        cLogger.info("Out BatRequest.insertTransLog()!");
        return mLKTransStatusDB;
    }

    public Document createStdXml(LYSendToBankSet pLYSendToBankSet)
            throws MidplatException {
        cLogger.info("Into BatRequest.createStdXml()...");

        int mTotalNum = 0;
        if (pLYSendToBankSet != null) {
            mTotalNum = pLYSendToBankSet.size();
        }
        String mTotalSum = "0.00";
        long mTotalMoney = 0;

        Element mTranData = new Element(TranData);

        cLogger.info("此批总笔数为:" + mTotalNum);
        for (int i = 1; i <= mTotalNum; i++) {
            LYSendToBankSchema mLYSendToBankSchema = pLYSendToBankSet.get(i);

            String mAccName = mLYSendToBankSchema.getAccName();
            String mAccNo = mLYSendToBankSchema.getAccNo();
            String mNoType = mLYSendToBankSchema.getNoType();
            cSerialNo = mLYSendToBankSchema.getSerialNo();
            String mPayCode = mLYSendToBankSchema.getPayCode();
            double mPayMoney = mLYSendToBankSchema.getPayMoney();
            String mPolNo = mLYSendToBankSchema.getPolNo();
            String mAuthorYear = mLYSendToBankSchema.getSendDate();
            String mSaleType = mLYSendToBankSchema.getDoType();
            String mBusiNo = mLYSendToBankSchema.getAgentCode();
            String mIDType = mLYSendToBankSchema.getIDType();
            String mIDNo = mLYSendToBankSchema.getIDNo();
            String mWhetherBanker = mLYSendToBankSchema.getRemark();

            Element mDetail = new Element(Detail);
            Element tAccName = new Element(AccName);
            tAccName.setText(mAccName);
            Element tAccNo = new Element(AccNo);
            tAccNo.setText(mAccNo);
            Element tOperType = new Element("OperType");
            tOperType.setText(mNoType);
            Element tSerialNo = new Element("SerialNo");
            tSerialNo.setText(cSerialNo);
            Element tPayCode = new Element("PayCode");
            tPayCode.setText(mPayCode);
            Element tPayMoney = new Element("PayMoney");
            tPayMoney.setText(new DecimalFormat("0.00").format(mPayMoney));
            Element tPolNo = new Element("PolNo");
            tPolNo.setText(mPolNo);
            Element tAuthorYear = new Element("AuthorYear");
            tAuthorYear.setText(mAuthorYear);
            Element tSaleType = new Element("SaleType");
            tSaleType.setText(mSaleType);
            Element tBusiNo = new Element("BusiNo");
            tBusiNo.setText(mBusiNo);
            Element tIDType = new Element(IDType);
            tIDType.setText(mIDType);
            Element tIDNo = new Element(IDNo);
            tIDNo.setText(mIDNo);
            Element tWhetherBanker = new Element("WhetherBanker");
            tWhetherBanker.setText(mWhetherBanker);
            
            //建行增加保单序号
            Element tIdNum = new Element("IdNum");
            tIdNum.setText(String.valueOf(i));

            mDetail.addContent(tAccName);
            mDetail.addContent(tAccNo);
            mDetail.addContent(tOperType);
            mDetail.addContent(tSerialNo);
            mDetail.addContent(tPayCode);
            mDetail.addContent(tPayMoney);
            mDetail.addContent(tPolNo);
            mDetail.addContent(tAuthorYear);
            mDetail.addContent(tSaleType);
            mDetail.addContent(tBusiNo);
            mDetail.addContent(tIDType);
            mDetail.addContent(tIDNo);
            mDetail.addContent(tWhetherBanker);
            mDetail.addContent(tIdNum);

            mTranData.addContent(mDetail);

            mTotalMoney += NumberUtil.yuanToFen(mPayMoney);
        }

        mTotalSum = NumberUtil.fenToYuan(mTotalMoney);
        Element tTotalNum = new Element("TotalNum");
        tTotalNum.setText(String.valueOf(mTotalNum));
        Element tTotalSum = new Element("TotalSum");
        tTotalSum.setText(mTotalSum);
        Element tBatType = new Element("BatType");
        tBatType.setText(cBatType);
        mTranData.addContent(tTotalNum);
        mTranData.addContent(tTotalSum);
        mTranData.addContent(tBatType);
        cLogger.info("此批总金额为:" + mTotalSum);

        cLogger.info("Out BatRequest.createStdXml()!");
        return new Document(mTranData);
    }

    public static void main(String[] args) throws Exception {
        System.out.println("程序开始…");

        String mInFile = "E:/BAT901.xml";
        String mOutFile = "E:/BAT901out.xml";

        FileInputStream mFis = new FileInputStream(mInFile);
        InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
        Document mInXmlDoc = new SAXBuilder().build(mIsr);

        Document mOutXmlDoc = new SFBatRequest(null).service(mInXmlDoc);

        XMLOutputter mXMLOutputter = new XMLOutputter();
        mXMLOutputter.setEncoding("GBK");
        mXMLOutputter.setTrimText(true);
        mXMLOutputter.setIndent("   ");
        mXMLOutputter.setNewlines(true);
        mXMLOutputter.output(mOutXmlDoc, System.out);
        mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

        String sql = "delete from lysendtobank where serialno = '00000000000000000603' and senddate = current date";
		new ExeSQL().execUpdateSQL(sql);
        System.out.println("成功结束！");
    }
}
