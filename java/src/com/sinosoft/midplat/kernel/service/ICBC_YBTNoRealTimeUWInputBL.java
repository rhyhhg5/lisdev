package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException; 
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKNoRealTimeUWCheckSchema;
import com.sinosoft.lis.vschema.LKNoRealTimeUWCheckSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ICBC_YBTNoRealTimeUWInputBL extends ServiceImpl  {

	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	
	public ICBC_YBTNoRealTimeUWInputBL(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_YBTNoRealTimeUWInputBL.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		Element mTranData = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
//		Element mBaseInfo = pInXmlDoc.getRootElement().getChild(BaseInfo);
		LKTransStatusDB mLKTransStatusDB = null;	
		LKNoRealTimeUWCheckSet mLKNoRealTimeUWCheckSet = new LKNoRealTimeUWCheckSet();
		
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			List tDetailList = pInXmlDoc.getRootElement().getChild("ChkDetails").getChildren("ChkDetail");
			LKNoRealTimeUWCheckSchema tLKNoRealTimeUWCheckSchema ;
			String mSql_lk = "";
			for (int i = 0; i < tDetailList.size() ; i++) {
				tLKNoRealTimeUWCheckSchema = new LKNoRealTimeUWCheckSchema();
				
				Element tDetail =(Element) tDetailList.get(i);
				String mPreNo = tDetail.getChildTextTrim("PrtNo");
				ExeSQL es = new ExeSQL();
				mSql_lk = "select 1 from LKNoRealTimeUWCheck where ProposalContno = '"+ mPreNo +"'" + " with ur";
				if("1".equals(es.getOneValue(mSql_lk))){
					cLogger.error("此投保单印刷号已存在，PrtNo="+mPreNo);
					continue;
				}
				tLKNoRealTimeUWCheckSchema.setBankCode(tDetail.getChildTextTrim("BankCode"));
				tLKNoRealTimeUWCheckSchema.setZoneNo(tDetail.getChildTextTrim("BankZoneCode"));
				tLKNoRealTimeUWCheckSchema.setBankNode(tDetail.getChildTextTrim("BrNo"));
				tLKNoRealTimeUWCheckSchema.setBankOperator(tDetail.getChildTextTrim("TellerNo"));
				tLKNoRealTimeUWCheckSchema.setTransNo(tDetail.getChildTextTrim("TransrNo"));
				tLKNoRealTimeUWCheckSchema.setProposalContno(tDetail.getChildTextTrim("PrtNo"));
				tLKNoRealTimeUWCheckSchema.setTransDate(tDetail.getChildTextTrim("TranDate"));
				tLKNoRealTimeUWCheckSchema.setBranchType(tDetail.getChildTextTrim("PayChnl"));
//				tLKNoRealTimeUWCheckSchema.setRiskCode(tDetail.getChildTextTrim("RiskCode"));
				tLKNoRealTimeUWCheckSchema.setAppntName(tDetail.getChildTextTrim("AppntName"));
				tLKNoRealTimeUWCheckSchema.setAppntIdType(tDetail.getChildTextTrim("AppntIDType"));
				tLKNoRealTimeUWCheckSchema.setAppntIdNo(tDetail.getChildTextTrim("AppntIDNo"));
				tLKNoRealTimeUWCheckSchema.setBankAccNo(tDetail.getChildTextTrim("AppntCardNo"));
				
				tLKNoRealTimeUWCheckSchema.setPassFlag("0");//是否已经签单 0:未签单 ；1：已签单；2：拒保；3：超时
				tLKNoRealTimeUWCheckSchema.setTbDealStatus("0"); //受理状态，初始状态为0（保单状态）
				
				tLKNoRealTimeUWCheckSchema.setProductType(tDetail.getChildTextTrim("ContFlag"));
				tLKNoRealTimeUWCheckSchema.setOperator("ybt");
				
				tLKNoRealTimeUWCheckSchema.setMakeDate(mCurrentDate);
				tLKNoRealTimeUWCheckSchema.setMakeTime(mCurrentTime);
				tLKNoRealTimeUWCheckSchema.setModifyDate(mCurrentDate);
				tLKNoRealTimeUWCheckSchema.setModifyTime(mCurrentTime);
				
				mLKNoRealTimeUWCheckSet.add(tLKNoRealTimeUWCheckSchema);
			}
			
			MMap tMMap = new MMap();
			VData tVData = new VData();
			tMMap.put(mLKNoRealTimeUWCheckSet, "INSERT");
			tVData.add(tMMap);
			PubSubmit tPs = new PubSubmit();
			if (!tPs.submitData(tVData, "")) {
				mFlag.setText("1");
				mDesc.setText("执行插入非实时核保信息失败!");
			}else {
				mFlag.setText("0");
				mDesc.setText("交易成功！");
			}
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");//执行任务完毕记
//			mLKTransStatusDB.setTransCode("1");//文件生成
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		
		
		mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mOutXmlDoc = new Document(mTranData);
		cLogger.info("Out ICBC_YBTNoRealTimeUWInputBL.service()...");
		return mOutXmlDoc;
	}
	
	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		cLogger.info("Into ICBC_YBTNoRealTimeUWInputBL.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo")+DateUtil.getCurDate("HHmmss"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		mLKTransStatusDB.setRCode("0");//表示未完成整体流程
		mLKTransStatusDB.setTransCode("0");//未签单文件是否生成
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out ICBC_YBTNoRealTimeUWInputBL.insertTransLog()...");
		return mLKTransStatusDB;
	}

}
