package com.sinosoft.midplat.kernel.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ABC_BatInsureData_Trans extends ServiceImpl {

	private String cCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
    private String cCurrentTime = DateUtil.getCurDate("HH:mm:ss");
    
	public ABC_BatInsureData_Trans(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_BatInsureData_Trans.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mOutXmlDoc = getOutDoc();
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ABC_BatInsureData_Trans.service()...");
		return mOutXmlDoc;

//		StringBuffer strTrans = new StringBuffer();

		// for (int i = 1; i <= ssrs.MaxRow; i++) {
		// StringBuffer line = new StringBuffer();
		//			
		// for (int j = 1; j <= ssrs.MaxCol; j++) {
		// if(j == 1){
		// line.append(ABC_BatInsureData_Trans.addSpace(ssrs.GetText(i, j),
		// 2)).append(separate);
		// }
		// if(j == 2){
		// String accNo = ssrs.GetText(i, j);
		// if("".equals(accNo) || accNo == null){
		// accNo = "0";
		// }
		// line.append(ABC_BatInsureData_Trans.addSpace(accNo,
		// 19)).append(separate);
		// }
		// if(j == 3){
		// line.append(ABC_BatInsureData_Trans.addSpace(ssrs.GetText(i, j),
		// 30)).append(separate);
		// }
		// if(j == 4){
		// line.append(ABC_BatInsureData_Trans.addSpace(ssrs.GetText(i, j),
		// 4)).append(separate);
		// }
		// if(j == 5){
		// line.append(ABC_BatInsureData_Trans.addSpace(ssrs.GetText(i, j),
		// 1)).append(separate);
		// }
		// if(j == 6){
		// line.append(ABC_BatInsureData_Trans.addSpace(ABC_BatInsureData_Trans.getIdType(ssrs.GetText(i,
		// j)), 6)).append(separate);
		// }
		// if(j == 7){
		// line.append(ABC_BatInsureData_Trans.addSpace(ssrs.GetText(i, j),
		// 40)).append(separate);
		// }
		// if(j == 8){
		// String varDate = ssrs.GetText(i, j).replace("-", "");
		// line.append(ABC_BatInsureData_Trans.addSpace(varDate, 8));
		// }
		// }
		// strTrans.append(line+"\n");
		// }
		// StringBuffer URL = new StringBuffer();
		// URL.append("YBTPCB").append("@@@@").append(DateUtil.getCurDate("yyyyMMdd")).append(".TXT");
		// File file = ReadData.writerFile(sb.toString(), URL.toString());

	}

	public Document getOutDoc() {

		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		
		Element mFlag = new Element("Flag");
		mFlag.setText("1");
		Element mDesc = new Element("Desc");
		mDesc.setText("交易成功！");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		
		Element mDetails = new Element("Details");
		Element tFileName = new Element("FileName");
		tFileName.setText(getFileName());
		mDetails.addContent(tFileName);
		String startDate = ABC_BatInsureData_Trans.getStartDate();
//		String separate = "|";
		String sql_pl = "select lc.managecom,lc.bankaccno,lc.contno,'3040',"
				+ " '1',app.idtype,app.idno,lc.CInValiDate from lccont lc inner join lktransstatus lk"
				+ " on lc.contno = lk.polno inner join lcappnt app on lc.contno = app.contno"
				+ " where lk.bankcode = '04' "
				+ " and lc.signdate between '"
				+ startDate
				+ "' and current date"
				+ " union "
				+ " select lc.managecom,lc.bankaccno,lc.contno,'3040',"
				+ " '2',su.idtype,su.idno,lc.CInValiDate from lccont lc inner join lktransstatus lk"
				+ " on lc.contno = lk.polno inner join lcinsured su on lc.contno = su.contno"
				+ " where lk.bankcode = '04' "
				+ " and lc.signdate between '"
				+ startDate
				+ "' and current date"
				+ " union "
				+ " select lc.managecom,lc.bankaccno,lc.contno,'3040',"
				+ " '3',bnf.idtype,bnf.idno,lc.CInValiDate from lccont lc inner join lktransstatus lk"
				+ " on lc.contno = lk.polno inner join lcbnf bnf on lc.contno = bnf.contno"
				+ " where lk.bankcode = '04' " + " and lc.signdate between '"
				+ startDate + "' and current date" + " with ur";

		SSRS ssrs = new ExeSQL().execSQL(sql_pl);

		if (ssrs.MaxRow <= 0) {
			return null;
		}
		cLogger.info("此次总笔数为:" + ssrs.MaxRow);
		Element tTotalNum = new Element("TotalNum");
		tTotalNum.setText(ssrs.MaxRow+"");
		
		Element mFileName = new Element("BaseInfo");
		mFileName.setText(getFileName());
		

		Element mZoneNo;
		Element mAccNo;
		Element mContNo;
		Element mCompanNo;
		Element mRelaNo;
		Element mIdType;
		Element mIdNo;
		Element mInValiDate;
		Element mDetail;

		for (int i = 1; i <= ssrs.MaxRow; i++) {
			mDetail = new Element("Detail");
			mZoneNo = new Element("ZoneNo");
			mAccNo = new Element("AccNo");
			mContNo = new Element("ContNo");
			mCompanNo = new Element("CompanNo");
			mRelaNo = new Element("RelaNo");
			mIdType = new Element("IdType");
			mIdNo = new Element("IdNo");
			mInValiDate = new Element("InValiDate");
			for (int j = 1; j <= ssrs.MaxCol; j++) {
				
				mZoneNo.setText(getProv(ssrs.GetText(i, 1)));
				mAccNo.setText(ssrs.GetText(i, 2));
				mContNo.setText(ssrs.GetText(i, 3));
				mCompanNo.setText(ssrs.GetText(i, 4));
				mRelaNo.setText(ssrs.GetText(i, 5));
				mIdType.setText(ABC_BatInsureData_Trans.getIdType(ssrs.GetText(
						i, 6)));
				mIdNo.setText(ssrs.GetText(i, 7));
				mInValiDate.setText(ssrs.GetText(i, 8));

			}
			mDetail.addContent(mZoneNo);
			mDetail.addContent(mAccNo);
			mDetail.addContent(mContNo);
			mDetail.addContent(mCompanNo);
			mDetail.addContent(mRelaNo);
			mDetail.addContent(mIdType);
			mDetail.addContent(mIdNo);
			mDetail.addContent(mInValiDate);
			mDetails.addContent(mDetail);
		}
		mTranData.addContent(mDetails);
		return new Document(mTranData);
	}
	
	public static String getFileName(){
		 StringBuffer fileName = new StringBuffer();
		 fileName.append("YBTPCB").append("3040").append(DateUtil.getCurDate("yyyyMMdd")).append(".TXT");
		return fileName.toString();
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into BatDaily.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setBankAcc(getFileName()); // 借用做文件名称
		mLKTransStatusDB.setTransDate(cCurrentDate);
		mLKTransStatusDB.setTransTime(cCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(cCurrentDate);
		mLKTransStatusDB.setMakeTime(cCurrentTime);
		mLKTransStatusDB.setModifyDate(cCurrentDate);
		mLKTransStatusDB.setModifyTime(cCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out BatDaily.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static String addSpace(String resStr, int leng) {
		String newStr = "";
		if ((leng - resStr.length()) < 0) {
			newStr = resStr;
		} else {

			for (int i = 0; i < leng - resStr.length(); i++) {
				newStr += " ";
			}
			newStr += resStr;
		}
		return newStr;
	}

	public static File getWrite(String source, String fileName) {
		String url = "/wasfs/lis_war/midplat/BalanceFile/ABC/"
				+ DateUtil.getCurDate("yyyyMMdd") + "/";
		File f = new File(url);
		try {
			f = new File(url);
			if (!f.exists()) {
				f.mkdirs();
			}
			fileName = url + fileName;
			f = new File(fileName);
			if (f.exists()) {
				FileOutputStream fos = new FileOutputStream(f);
				fos.write("".getBytes());
			}
			FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(source);
			bw.newLine();
			bw.close();
			fw.close();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return f;
	}

	public static String getStartDate() {

		Date now = new Date();
		now.setMonth(now.getMonth() - 4);
		now.setDate(21);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(now);
	}
	
	// 省市代号转换
	public static String getProv(String prov){
		System.out.println("管理机构 = "+prov);
		String newProv = "";
		if(prov.length() >= 4){
			prov = prov.substring(0,4);
		}
		
		if("8612".equals(prov)){
			newProv = "02";
		}else if("8614".equals(prov)){
			newProv = "04";
		}else if("8615".equals(prov)){
			newProv = "05";
		}else if("8621".equals(prov)){
			newProv = "06";
		}else if("8622".equals(prov)){
			newProv = "07";
		}else if("8623".equals(prov)){
			newProv = "08";
		}else if("8631".equals(prov)){
			newProv = "09";
		}else if("8632".equals(prov)){
			newProv = "10";
		}else if("8611".equals(prov)){
			newProv = "11";
		}else if("8634".equals(prov)){
			newProv = "12";
		}else if("8635".equals(prov)){
			newProv = "13";
		}else if("8636".equals(prov)){
			newProv = "14";
		}else if("8637".equals(prov)){
			newProv = "15";
		}else if("8641".equals(prov)){
			newProv = "16";
		}else if("8642".equals(prov)){
			newProv = "17";
		}else if("8643".equals(prov)){
			newProv = "18";
		}else if("8633".equals(prov)){
			newProv = "19";
		}else if("8645".equals(prov)){
			newProv = "20";
		}else if("8646".equals(prov)){
			newProv = "21";
		}else if("8651".equals(prov)){
			newProv = "22";
		}else if("8652".equals(prov)){
			newProv = "23";
		}else if("8653".equals(prov)){
			newProv = "24";
		}else if("8654".equals(prov)){
			newProv = "25";
		}else if("8661".equals(prov)){
			newProv = "26";
		}else if("8662".equals(prov)){
			newProv = "27";
		}else if("8663".equals(prov)){
			newProv = "28";
		}else if("8664".equals(prov)){
			newProv = "29";
		}else if("8665".equals(prov)){
			newProv = "30";
		}else if("8650".equals(prov)){
			newProv = "31";
		}else if("8691".equals(prov)){
			newProv = "34";
		}else if("8694".equals(prov)){
			newProv = "38";
		}else if("8692".equals(prov)){
			newProv = "39";
		}else if("8693".equals(prov)){
			newProv = "40";
		}else if("8695".equals(prov)){
			newProv = "41";
		}else if("8644".equals(prov)){
			newProv = "44";
		}else if("8613".equals(prov)){
			newProv = "50";
		}else if("86".equals(prov)){
			newProv = "99";
		}
		System.out.println("管理机构2 = "+newProv);
		return newProv;
	}

	public static String getIdType(String idType) {

		String newIdType = "";
		if ("".equals(idType) || idType == null) {
			newIdType = "";
		} else if ("0".equals(idType)) { // 身份证
			newIdType = "110001";
		} else if ("1".equals(idType)) { // 护照
			newIdType = "110025";
		} else if ("2".equals(idType)) { // /军官证
			newIdType = "110027";
		} else if ("3".equals(idType)) { // 工作证
			newIdType = "";
		} else if ("4".equals(idType)) { // 其它
			newIdType = "119999";
		} else if ("5".equals(idType)) { // 户口本
			newIdType = "110005";
		}

		return newIdType;
	}

	public static void main(String[] args) {

		// System.out.println(ABC_BatInsureData_Trans.addSpace("eeeee", 4));
		System.out.println(ABC_BatInsureData_Trans.getProv("86"));

	}

}
