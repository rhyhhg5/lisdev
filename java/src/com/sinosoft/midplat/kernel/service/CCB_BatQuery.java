/**
 * 通用简单交易处理类。
 * 此类本身不做任何业务处理，仅插入一条交易日志。
 * 目前使用此类的交易有：中行冲正、建行绿灯
 */

package com.sinosoft.midplat.kernel.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_BatQuery extends ServiceImpl {
	public CCB_BatQuery(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_BatQuery.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			mOutXmlDoc = deal(pInXmlDoc);
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error("通用简单(CCB_BatQuery)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_BatQuery.service()!");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws ParseException {
		cLogger.info("Into CCB_BatQuery.deal()...");
		Document mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		ExeSQL es = new ExeSQL();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String mTranDate = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("TranDate");
		Date date = sdf.parse(mTranDate);
		mTranDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
//		mTranDate = DateUtil.parseDate(mTranDate, "yyyy-MM-dd");
		String tSQL = "select count(1) from lccont where cardflag in ('6','8') and salechnl in ('04','13') and bankcode like '03%' and makedate = '"+mTranDate+"' with ur";
		Element mLCCont = new Element("LCCont");
		Element mBatIncome = new Element("BatIncome");
		if(Integer.parseInt(es.getOneValue(tSQL))>0){
			mBatIncome.setText("1");
		}else{
			mBatIncome.setText("0");
		}
		mLCCont.addContent(mBatIncome);
		mOutXmlDoc.getRootElement().addContent(mLCCont);
			
		cLogger.info("Out CCB_BatQuery.deal()...");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_BatQuery.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out CCB_BatQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
