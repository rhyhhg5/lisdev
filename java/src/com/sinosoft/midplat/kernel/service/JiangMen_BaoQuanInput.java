package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.MidplatSufConf;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class JiangMen_BaoQuanInput extends ServiceImpl {
	
	public JiangMen_BaoQuanInput(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into JiangMen_BaoQuanInput.service()...");
		
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		LKTransStatusDB mLKTransStatusDB = null;
		Element mDataSet = pInXmlDoc.getRootElement();
		Element mBaseInfo = mDataSet.getChild(BaseInfo);
		String mEdorType = mDataSet.getChild("EdorItemInfo").getChild("Item").getChildTextTrim("EdorType");
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));

	        //是否需做判断银行传过来的数据与核心表中数据一致性？
			// 进行犹豫期、退保 业务处理
			if("WT".equals(mEdorType) || "CT".equals(mEdorType)){
				mOutXmlDoc = deal(pInXmlDoc);
			}
			if("MQ".equals(mEdorType)){ //满期给付
				dealMQ(pInXmlDoc);
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束

		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out JiangMen_BaoQuanInput.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into JiangMen_BaoQuanInput.deal()...");
		
		Document pInXmlDoc_copy = (Document) pInXmlDoc.clone();
		pInXmlDoc_copy.getRootElement().removeChild("BaseInfo");
		Element tBaseInfo = pInXmlDoc.getRootElement().getChild("BaseInfo");
		Element xDataSet = pInXmlDoc_copy.getRootElement();
		String mContNo = xDataSet.getChild("EdorItemInfo").getChild("Item").getChildText("ContNo");
		String mBaoQuanFlag = xDataSet.getChild("EdorItemInfo").getChild("Item").getChildText("EdorType");
		
		SSRS ssrs = new SSRS();
		ExeSQL es = new ExeSQL();
		String tSql = "select contno from lccont where salechnl in ('03','04') and cardflag in ('9','c','d','a') and contno='"+mContNo+"' ";
		String tContNo = es.getOneValue(tSql);
		cLogger.info("tSql:"+tSql);
		if(tContNo==null || tContNo.equals("")){
			throw new MidplatException("该保单非银保通渠道出单!");
		}
		
		tSql = "select polno from lktransstatus where bankcode='04' and bankbranch='"+tBaseInfo.getChildText("ZoneNo")+"' " +
				"and banknode='"+tBaseInfo.getChildText("BrNo")+"' and funcflag='01' and polno= '"+mContNo+"'";
		String tPolno = es.getOneValue(tSql);
		cLogger.info("tSql:"+tSql);
		if(tPolno==null || tPolno.equals("")){
			throw new MidplatException("该保单保全网点与出单网点不一致!");
		}
		
		tSql = "select 1 from lccont where stateflag='0' and contno='"+mContNo+"'";
		cLogger.info("tSql:"+tSql);
		if("1".equals(new ExeSQL().getOneValue(tSql))){//保单尚未签单
			throw new MidplatException ("该保单尚未签单!");
		}
		
		tSql = "select contno from lbcont where contno='"+mContNo+"' union select contno from lccont where contno='"+mContNo+"' and stateflag <>1";
		ssrs = es.execSQL(tSql);
		cLogger.info("tSql:"+tSql);
		if(ssrs.MaxRow>=1){
			throw new MidplatException("该保单已失效!");
		}
		
		tSql = "select lc.contno from lccont lc,lktransstatus lk where lc.contno=lk.polno and lk.bankcode='04' " +
				"and lk.funcflag='01' and status='1' and lk.rbankvsmp is null and lc.contno='"+mContNo+"' ";
		ssrs = es.execSQL(tSql);
		cLogger.info("tSql:"+tSql);
		if(ssrs.MaxRow>=1){
			throw new MidplatException("该保单未对账!");
		}
		
		if(mBaoQuanFlag.equals("WT")){
			tSql = "select signdate from lccont where contno='"+mContNo+"'"; 
			String tSigndate = es.getOneValue(tSql);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			Date date = null;
			try {
				date = format.parse(tSigndate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH, 9);
			String dateStr = format.format(cal.getTime());
			String tTransdate = tBaseInfo.getChildText("BankDate");
			tTransdate = DateUtil.date8to10(tTransdate);
			cLogger.info("tSql:"+tSql);
			if(tTransdate.compareTo(dateStr)>0){
				throw new MidplatException("此保单超出犹豫期,不能犹豫期退保!");
			}
		}
		
		
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);

		LCContSet mCont = cont.query();
		
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		
		if(mCont.size() > 0){
			
			LCContSchema lc = mCont.get(1);
			Element tItem = pInXmlDoc_copy.getRootElement().getChild("EdorItemInfo").getChild("Item");
			tItem.getChild("ManageCom").setText(lc.getManageCom());
			tItem.getChild("CardFlag").setText(lc.getCardFlag());
			tItem.getChild("Salechnl").setText(lc.getSaleChnl());
			tItem.getChild("Salechnl").setText(lc.getSaleChnl());
			pInXmlDoc_copy.getRootElement().getChild("EdorAppInfo").getChild("Item").getChild("CustomerNo").setText(lc.getAppntNo());
			
			System.out.println("------------------给核心保全请求报文-----------------");
			JdomUtil.print(pInXmlDoc_copy);
			System.out.println("-----------------------------------");
			
			//针对农行保全申请 add by zengzm 2015-11-13
			Document outStdXml = queryBaoQuanInfo(pInXmlDoc_copy);
			
			System.out.println("------------------核心保全返回报文-----------------");
			JdomUtil.print(outStdXml);
			System.out.println("-----------------------------------");
			
			if(outStdXml.getRootElement().getChild("MsgResHead").getChild("Item").getChildTextTrim("State").equals("00")){
				mFlag.setText("1");
				mDesc.setText("交易成功！");
				LBPolDB pol_db = new LBPolDB();
				pol_db.setContNo(mContNo);
				LBPolSet polSet = pol_db.query();
				
				double mSumPrem = 0;
				mSumPrem = Double.parseDouble(outStdXml.getRootElement().getChild("EndorsementInfo").getChild("Item").getChildTextTrim("SumGetMoney"));
				
				String risk_main = "";
				String risk_fu1 = "";
				String risk_fu2 = "";
				System.out.println("POLSET:"+polSet.size());
				for (int i =1 ; i <= polSet.size(); i++) {
					LBPolSchema mPol = polSet.get(i);
					if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
						risk_main = mPol.getRiskCode();
					}
					if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
						risk_fu1 = mPol.getRiskCode();
					}
					if("3".equals(mPol.getRiskSeqNo()) || "03".equals(mPol.getRiskSeqNo())){
						risk_fu2 = mPol.getRiskCode();
					}
				}
				
				Element nEdorAcceptNo = new Element("EdorAcceptNo");
				nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
				
				Element nContNo = new Element("ContNo");
				nContNo.setText(lc.getContNo());
				
				Element nCValiDate = new Element(CValiDate);
				nCValiDate.setText(lc.getCValiDate());
				
				Element nContEndDate = new Element(ContEndDate);
				nContEndDate.setText(lc.getCInValiDate());	//picch特有，保单级保单终止日期
				
				Element nAttachmentContent = new Element("AttachmentContent");
				Element nAppntName = new Element("AppntName");
				nAppntName.setText(lc.getAppntName());
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String now = sdf.format(new Date());
				Element nEdorAcceptYear = new Element("EdorAcceptYear");
				nEdorAcceptYear.setText(now.substring(0,4));
				Element nEdorAcceptMonth = new Element("EdorAcceptMonth");
				nEdorAcceptMonth.setText(now.substring(4,6));
				Element nEdorAcceptDay = new Element("EdorAcceptDay");
				nEdorAcceptDay.setText(now.substring(6,8));
				Element nApplyDate = new Element("ApplyDate");
				nApplyDate.setText(now);
				
				Element nMainRiskCodeName = new Element("MainRiskCodeName");
				nMainRiskCodeName.setText(getRiskRep(risk_main));
				Element nRiskCodeName1 = new Element("RiskCodeNamefu");
				Element nRiskCodeName2 = new Element("RiskCodeName_fu");
				if(!"".equals(risk_fu1)){
					nRiskCodeName1.setText(getRiskRep(risk_fu1));
				}
				if(!"".equals(risk_fu2)){
					nRiskCodeName2.setText(getRiskRep(risk_fu2));
				}
				
				Element nSumPrem = new Element("SumPrem");
				if(mBaoQuanFlag.equals("WT")){	//犹豫期保费
					nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
				}
				if(mBaoQuanFlag.equals("CT")){	//退保保费即解约
//					PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
//					double pSumPrem = mPedorYBTBudgetBL.getZTmoney(lc.getContNo(),now);
					nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
				}
	
				nAttachmentContent.addContent(nAppntName);
				nAttachmentContent.addContent(nEdorAcceptYear);
				nAttachmentContent.addContent(nEdorAcceptMonth);
				nAttachmentContent.addContent(nEdorAcceptDay);
				nAttachmentContent.addContent(nApplyDate);
				nAttachmentContent.addContent(nMainRiskCodeName);
				nAttachmentContent.addContent(nRiskCodeName1);
				nAttachmentContent.addContent(nRiskCodeName2);
				nAttachmentContent.addContent(nSumPrem);
				mEdorInfo.addContent(nEdorAcceptNo);
				mEdorInfo.addContent(nCValiDate);
				mEdorInfo.addContent(nContEndDate);
				mEdorInfo.addContent(nContNo);
				mEdorInfo.addContent(nAttachmentContent);
			}else {
				mFlag.setText("0");
				mDesc.setText(outStdXml.getRootElement().getChild("MsgResHead").getChild("Item").getChildText("ErrInfo"));
				throw new MidplatException(outStdXml.getRootElement().getChild("MsgResHead").getChild("Item").getChildText("ErrInfo"));
			}
		}else {
			mFlag.setText("0");
			mDesc.setText("查询保单信息失败");
			throw new MidplatException("查询保单信息失败");
		}

		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out JiangMen_BaoQuanInput.deal()...");
		return new Document(mTranData);
	}
	
	private Document queryBaoQuanInfo(Document pInXmlDoc) {
		
		return JdomUtil.build(queryInfo(pInXmlDoc).getBytes());
	}
	
	private String queryInfo(Document pStdDoc){
		//根据同意工号调用webservice查询业务员信息
		String mResInfo = "";
	      try
	        {
	    	  
	    	  Document MidplatSufDoc =  MidplatSufConf.newInstance().getConf();
	    	  String  tStrTargetEendPoint = MidplatSufDoc.getRootElement().getChildTextTrim("WebService");
	    	  if(null == tStrTargetEendPoint || "".equals(tStrTargetEendPoint)){
	    		  throw new  MidplatException("webservice地址解析失败,请检查MidplatSuf.xml配置!");
	    	  }
	    	  
	            String tStrNamespace = "http://services.core.cbsws.com";   
	            
	            RPCServiceClient client = new RPCServiceClient();
	            String mInXmlStr = "";
	    		byte[] mBodyBytes = JdomUtil.toBytes(pStdDoc);
	    		mInXmlStr = new String(mBodyBytes);    		
	    		
	            EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
	            Options option = client.getOptions();
	            option.setTo(erf);
	            option.setTimeOutInMilliSeconds(100000L);
	            option.setAction("service");
	            QName name = new QName(tStrNamespace, "service");
	            Object[] object = new Object[] { mInXmlStr };
	            Class[] returnTypes = new Class[] { String.class };

	            Object[] response = client.invokeBlocking(name, object, returnTypes);
	            mResInfo = (String) response[0];

	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
		return  mResInfo;
	}
	
	//满期
	private Document dealMQ(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into JiangMen_BaoQuanInput.deal()...");
		
		Element mEdorAppInfo = pInXmlDoc.getRootElement().getChild("EdorAppInfo"); 
		Element mEdorItemInfo = pInXmlDoc.getRootElement().getChild("EdorItemInfo"); 
		
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		
		Element nEdorAcceptNo = new Element("EdorAcceptNo");
		nEdorAcceptNo.setText(""); //批单号...
		
		Element nContNo = new Element("ContNo");
		nContNo.setText(mEdorItemInfo.getChildTextTrim("ContNo"));
		
		String now = mEdorItemInfo.getChildTextTrim("EdorValidate");
		Element nCValiDate = new Element(CValiDate);
		nCValiDate.setText(now);
		
		Element nContEndDate = new Element(ContEndDate);
		nContEndDate.setText(now);	
		
		Element nAttachmentContent = new Element("AttachmentContent");
		Element nAppntName = new Element("AppntName");
		nAppntName.setText(mEdorAppInfo.getChildTextTrim("ApplyName"));
		
		
		Element nEdorAcceptYear = new Element("EdorAcceptYear");
		nEdorAcceptYear.setText(now.substring(0,4));
		Element nEdorAcceptMonth = new Element("EdorAcceptMonth");
		nEdorAcceptMonth.setText(now.substring(4,6));
		Element nEdorAcceptDay = new Element("EdorAcceptDay");
		nEdorAcceptDay.setText(now.substring(6,8));
		Element nApplyDate = new Element("ApplyDate");
		nApplyDate.setText(now);
		
		Element nMainRiskCodeName = new Element("MainRiskCodeName");
		nMainRiskCodeName.setText("");
		Element nRiskCodeName = new Element("RiskCodeName");
		nRiskCodeName.setText("");
		
		Element nSumPrem = new Element("SumPrem");
		nSumPrem.setText("0");

		nAttachmentContent.addContent(nAppntName);
		nAttachmentContent.addContent(nEdorAcceptYear);
		nAttachmentContent.addContent(nEdorAcceptMonth);
		nAttachmentContent.addContent(nEdorAcceptDay);
		nAttachmentContent.addContent(nApplyDate);
		nAttachmentContent.addContent(nMainRiskCodeName);
		nAttachmentContent.addContent(nRiskCodeName);
		nAttachmentContent.addContent(nSumPrem);
		mEdorInfo.addContent(nEdorAcceptNo);
		mEdorInfo.addContent(nCValiDate);
		mEdorInfo.addContent(nContEndDate);
		mEdorInfo.addContent(nContNo);
		mEdorInfo.addContent(nAttachmentContent);
	
		

		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out JiangMen_BaoQuanInput.deal()...");
		return new Document(mTranData);
	}
	
	public String getRiskRep(String riskCode){
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
	}
	
	/*private LPYBTAppWTDB insertLPYBTAppWT(Document pXmlDoc,LCContSchema lc) throws MidplatException {
		cLogger.info("Into JiangMen_BaoQuanInput.insertLPYBTAppWT()...");
		
		LPYBTAppWTSet mLPYBTAppWTSet = new LPYBTAppWTSet();
		LPYBTAppWTDB mLPYBTAppWTDB = new LPYBTAppWTDB();
		Element xTranData = pXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		String mBaoQuanFlag = xTranData.getChild("LCCont").getChildText("BaoQuanFlag");
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTSet = mLPYBTAppWTDB.query();
		
		if(mLPYBTAppWTSet.size() > 0){	//判断保单是否已存在犹豫期退保表中！
			String mSQL = "update lktransstatus set descr = '该保单已存在保全表中，请核查！' where polno = '" + mContNo + "'and funcflag = '56' and makedate = Current Date with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if (!mExeSQL.execUpdateSQL(mSQL)) {
				cLogger.error(mExeSQL.mErrors.getFirstError());
				cLogger.warn("该保单已存在保全表中，请核查！");
			}
		}
		
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTDB.setManageCom(lc.getManageCom());
		mLPYBTAppWTDB.setAppWTDate(mCurrentDate);
		mLPYBTAppWTDB.setAppntName(lc.getAppntName());
		mLPYBTAppWTDB.setAppntSex(lc.getAppntSex());
		mLPYBTAppWTDB.setAppntBirthday(lc.getAppntBirthday());
		mLPYBTAppWTDB.setAppntIDType(lc.getAppntIDType());
		mLPYBTAppWTDB.setAppntIDNo(lc.getAppntIDNo());
		mLPYBTAppWTDB.setGBFee("0");	//保险公司定为0元
		mLPYBTAppWTDB.setAppState("1");		//需要新的状态标识
		mLPYBTAppWTDB.setoperator("ybt");
		mLPYBTAppWTDB.setTransStatus("1");	//未对账
		mLPYBTAppWTDB.setEdorType(mBaoQuanFlag);	//保全类别 WT-犹豫期退保 CT-解约
		mLPYBTAppWTDB.setMakeDate(mCurrentDate);
		mLPYBTAppWTDB.setMakeTime(mCurrentTime);
		mLPYBTAppWTDB.setModifyDate(mCurrentDate);
		mLPYBTAppWTDB.setModifyTime(mCurrentTime);
		
		if (!mLPYBTAppWTDB.insert()) {
			cLogger.error(mLPYBTAppWTDB.mErrors.getFirstError());
			throw new MidplatException("插入保全表失败！");
		}
		cLogger.info("Out JiangMen_BaoQuanInput.insertLPYBTAppWT()...");
		return mLPYBTAppWTDB;
	}*/

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into JiangMen_BaoQuanInput.insertTransLog()...");

		Element mDataSet = pXmlDoc.getRootElement();
		Element mBaseInfo = mDataSet.getChild(BaseInfo);
//		Element mLCCont = mTranData.getChild("LCCont");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPrtNo("");
		mLKTransStatusDB.setPolNo(pXmlDoc.getRootElement().getChild("EdorItemInfo").getChild("Item").getChildTextTrim("ContNo"));
		mLKTransStatusDB.setTransAmnt("");
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out JiangMen_BaoQuanInput.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "E:\\work_folder\\人保\\人保文件\\1005农行\\农行保全需求\\abc_8100002236400000008_1013_180354.xml";
//		String mInFilePath = "E:\\work_folder\\人保\\人保文件\\中行\\中行产品开发（福利双全，百万安行）\\百万安行\\02_20151020_1001.xml";
//		String mOutFilePath = "D:/out.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();

		JiangMen_BaoQuanInput abc =   new JiangMen_BaoQuanInput(null);
		Document my_Doc =   abc.queryBaoQuanInfo(mInXmlDoc);

		
		JdomUtil.print(my_Doc);

		System.out.println("成功结束！");
	}
}

