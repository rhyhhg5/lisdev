package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BatResultNotice extends ServiceImpl {

	public BatResultNotice(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into BatResultNotice.service()...");
		Document mOutXmlDoc = null;

		try {
			Element mTranData = pInXmlDoc.getRootElement();
			Element mBaseInfo = mTranData.getChild(BaseInfo);
			String mFlag = mBaseInfo.getChildText(Flag);
			if (!mFlag.equals("00000")) {
				cLogger.info("开始处理提盘失败信息");
				String mBankCode = mBaseInfo.getChildText(BankCode);
				String mZoneNo = mBaseInfo.getChildText(ZoneNo);
				String mBrNo = mBaseInfo.getChildText(BrNo);
				String mFileName = mBaseInfo.getChildText("FileName");

				String mSQL = "select transno, edorno from lktransstatus where bankcode = '"
						+ mBankCode
						+ "' and bankbranch = '"
						+ mZoneNo
						+ "' and banknode = '"
						+ mBrNo
						+ "' and funcflag = '31' and bankacc = '"
						+ mFileName
						+ "' order by transtime desc with ur";
				SSRS mSSRS = new ExeSQL().execSQL(mSQL);
				if (mSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的日志信息!");
				}
				String mTransNo = mSSRS.GetText(1, 1);
				String mEdorNo = mSSRS.GetText(1, 2);
				mBaseInfo.getChild(TransrNo).setText(mTransNo);
				Element mSerialNo = new Element("SerialNo");
				mSerialNo.setText(mEdorNo);
				Element mLCCont = null;
				if(mBaseInfo.getChildText("BankCode").equals("03")){
					mLCCont = mTranData.getChild("LCCont");
					mLCCont.addContent(mSerialNo);
				} else {
					mLCCont = new Element(LCCont);
					mLCCont.addContent(mSerialNo);
					mTranData.addContent(mLCCont);
				}
			
				mOutXmlDoc = new BatResult(cThisBusiConf).service(pInXmlDoc);
				return mOutXmlDoc;
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");

		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out BatResultNotice.service()!");
		return mOutXmlDoc;
	}
}
