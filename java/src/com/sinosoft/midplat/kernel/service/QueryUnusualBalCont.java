package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LOMixBKAttributeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOMixBKAttributeSchema;
import com.sinosoft.lis.vschema.LOMixBKAttributeSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/*
 * 该类主要查询当天对账异常数据(主要包含对账数据不更新的保单)和财务暂受表中有重复收费的保单(
 * 需结合实收表共同查询
 */

public class QueryUnusualBalCont extends SimpService {
	public QueryUnusualBalCont(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QueryUnusualBalCont.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			Element tTranData = new Element("TranData");
			Element tRetData = new Element("RetData");
			String sql1 = "select polno from   lktransstatus where     makedate = current date  -1 days and funcflag ='01'  and status='1'  and  rmpvskernel='00' and  resultbalance is null";
            SSRS sr1 = new ExeSQL().execSQL(sql1);  
            /*上面sql最好使用具体字段补充完成(查询部分字段不更新情况)
             * 注意需要将sr1  与下边重复数据一并返回但是要区分开不同的数据类型
             */
            /*
             * 组装数据对账异常数据     
             * start     
             */
            Element mDetaillks = new Element("Detaillks");
            for (int li= 0;li<sr1.getMaxRow();li++)
            {
            	Element mDetaillk = new Element("Detaillk");
                Element mContno = new Element("Contno");
                Element mDescr = new Element("Descr");
            	mContno.setText(sr1.GetText(li, 0));
            	mDescr.setText("更新对账结果失败");
            	mDetaillk.addContent(mContno);
            	mDetaillk.addContent(mDescr);
            	mDetaillks.addContent(mDetaillk);
            }
            /*
             * 组装数据对账异常数据     
             * end     
             */
            /*
             * 查询实收表与暂受表收费记录是否不一致,首选查询出实收表情况,其次根据实收表的金额去暂受表用保单号查询金额匹配
             * 
             */
            String   sql2 = "select incomeno,sumactupaymoney  from  ljapay where  incomeno  in " +
            		"(SELECT    polno FROM lktransstatus WHERE makedate >=CURRENT DATE -1 days AND funcflag ='01'AND status='1' " +
            		"AND rmpvskernel='00' and resultbalance ='0' and exists (select 1  from   lccont where  contno = polno))";
            SSRS sr2 = new ExeSQL().execSQL(sql2);
            String   polno ="";
            double sumprem = 0.00;
            String   sql3 = "select  sum(paymoney)  from   ljtempfee  where   otherno = \'";
            
            Element mDetailljs = new Element("Detailljs");
            
            ExeSQL exe = new ExeSQL();
             for  (int i=0; i<sr2.MaxRow;i++){
            	 polno =sr2.GetText(i, 1);
            	 sumprem = Double.parseDouble(sr2.GetText(i,2));
            	 cLogger.info("查询实收表保单号为:"+polno +"的保费:"+sumprem);
            	 sql3 += (polno+"\'");
            	String tempprem =  exe.getOneValue(sql3);
            	 cLogger.info("查询暂收表保单号为:"+polno +"的保费:"+tempprem);
            	 //如果此处不一致,需要返回数据
            	if(sumprem != Double.parseDouble(tempprem)){
            		Element mDetaillj = new Element("Detaillj");
            		System.out.println("我要开始返回了啊!");
            		Element mPolno = new Element("Polno");
                    Element mZSPrem = new Element("ZSPrem");
                    Element mSSPrem = new Element("SSPrem");
            		
            		mPolno.setText(polno);
                 	mZSPrem.setText(tempprem+"");
                 	mSSPrem.setText(sumprem+"");
                 	mDetaillj.addContent(mPolno);
                 	mDetaillj.addContent(mZSPrem);
                 	mDetaillj.addContent(mSSPrem);
                 	mDetailljs.addContent(mDetaillj);
            	}
             }
             tRetData.addContent(mDetaillks);
             tRetData.addContent(mDetailljs);
             tTranData.addContent(tRetData);
             mOutXmlDoc = new Document(tTranData);
             
             mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
 			 mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
 			 mLKTransStatusDB.setStatus("1"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
 			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out QueryUnusualBalCont.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into QueryUnusualBalCont.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch("86");
		mLKTransStatusDB.setBankNode("86");
		mLKTransStatusDB.setBankOperator("ybt");
		mLKTransStatusDB.setTransNo(mCurrentDate+mCurrentTime);
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTemp("1");	//确认/取消标识：0-取消；1-确认
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out QueryUnusualBalCont.insertTransLog()!");
		return mLKTransStatusDB;
	}
    
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "F:/working/picch/std/testXml/ConfirmOrCancel/std_00_in.xml";
		String mOutFile = "F:/working/picch/std/testXml/ConfirmOrCancel/std_00_out.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);
		
		Document mOutXmlDoc = new QueryUnusualBalCont(null).service(mInXmlDoc);
		
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
