package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class NXS_NewContPrint extends ServiceImpl{
	
	public NXS_NewContPrint(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		
		cLogger.info("Into NXS_NewContPrint()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		String LoanContractNo = null;
		String mLoanPrem = null;
		boolean mDeleteable = false;	//删除数据标志
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		if("60".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
		}
		
		if("60".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select bak2 from lktransstatus where prtno = '"+mLCCont.getChildText(PrtNo) +"' ";
			LoanContractNo = new ExeSQL().getOneValue(zSQL);
			if(LoanContractNo == null||"".equals(LoanContractNo)){
				zSQL = "select bak1 from lktransstatus where prtno = '"+mLCCont.getChildText(PrtNo) +"' ";
				LoanContractNo = new ExeSQL().getOneValue(zSQL);
			}
			
			if(mLoanPrem == null || "".equals(mLoanPrem)){
				zSQL = "select edorno from lktransstatus where prtno = '"+mLCCont.getChildText(PrtNo) +"' and funcflag='01'";
				mLoanPrem = new ExeSQL().getOneValue(zSQL);
			}
		}
		
		try {		
			String tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {			
					throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
	
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLKTransStatusDB.getPolNo());
			tLCContDB.setPrtNo(mLKTransStatusDB.getPrtNo());
			tLCContDB.setProposalContNo(mLKTransStatusDB.getProposalNo());
			if ((null==tLCContDB.getContNo() || "".equals(tLCContDB.getContNo()))
				&& (null==tLCContDB.getPrtNo() || "".equals(tLCContDB.getPrtNo()))
				&& (null==tLCContDB.getProposalContNo() || "".equals(tLCContDB.getProposalContNo()))) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if ((null==tLCContSet) || (tLCContSet.size()<1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);
			
			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();

			JdomUtil.print(mOutXmlDoc);
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			Element LoanContractNoE = new Element("LoanContractNo");
			LoanContractNoE.setText(LoanContractNo);
			tOutLCCont.addContent(LoanContractNoE);
			
			//贷款金额
			Element LoanPrem = new Element("LoanPrem");
			LoanPrem.setText(mLoanPrem);
			tOutLCCont.addContent(LoanPrem);
			
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Amnt));
			mLKTransStatusDB.setRiskCode(tOutLCCont.getChildText(RiskCode));
			
			
			
		} catch (MidplatException ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
				
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
					
				String ttDesr = ex.getMessage();
				try {
					if (null!=ttDesr && ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){}
				mLKTransStatusDB.setDescr(ttDesr);
			}
				
			if (mDeleteable) {
				try {
					YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
				} catch (Exception tBaseEx) {
					cLogger.error("删除新单数据失败！", tBaseEx);
				}
			}
				
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		
		}
		
		cLogger.info("Out NXS_NewContPrint.service()!");
		return mOutXmlDoc;
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		
		cLogger.info("Into NXS_NewContPrint.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		String sBankCodeString = mBaseInfo.getChildText(BankCode);
		if("60".equals(sBankCodeString)){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
			
		}
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out NXS_NewContPrint.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "E:\\rb\\jxnxs\\instd\\60_00079394_06_112352.xml";
		String mOutFile = "E:\\rb\\jxnxs\\outstd\\03out.xml";

		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new NXS_NewContPrint(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");

	}

}
