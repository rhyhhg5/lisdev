package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.writeOff.YbtWriteOffBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class AIO_WriteOff extends ServiceImpl {
	public AIO_WriteOff(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into AIO_WriteOff.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);
		String mContNo = mLCCont.getChildTextTrim("ContNo");
		String mProposalContNo = mLCCont.getChildTextTrim("ProposalContNo");
		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
		Element mBaseInfo = pInXmlDoc.getRootElement().getChild(BaseInfo);

		// 针对福建农信社不传ZoneNo情况进行特殊处理 add by gaojinfu 2017-06-01
		if ("69".equals(mBaseInfo.getChildText(BankCode))
				|| "78".equals(mBaseInfo.getChildText(BankCode))) {
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"
					+ mBaseInfo.getChildText(BankCode) + "' and banknode = '"
					+ mBaseInfo.getChildText(BrNo) + "'";
			String mZoneNo = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild(ZoneNo).setText(mZoneNo);
		}

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			LCContDB tLCContDB = new LCContDB();
			if (mContNo != null && !mContNo.equals("")) {
				tLCContDB.setContNo(mContNo);
			} else {
				String sql = "select contno from lccont where proposalcontno='"
						+ mProposalContNo + "'";
				mContNo = new ExeSQL().getOneValue(sql);
				tLCContDB.setContNo(mContNo);
			}
			if (mProposalContNo != null && !mProposalContNo.equals("")) {
				tLCContDB.setProposalContNo(mProposalContNo);
			} else {
				String sql = "select proposalcontno from lccont where contno='"
						+ mContNo + "'";
				mProposalContNo = new ExeSQL().getOneValue(sql);
				tLCContDB.setProposalContNo(mProposalContNo);
			}
			if (mPrtNo != null && !mPrtNo.equals("")) {
				tLCContDB.setPrtNo(mPrtNo);
			} else {
				String sql = "select prtno from lccont where contno='"
						+ mContNo + "'";
				mPrtNo = new ExeSQL().getOneValue(sql);
				tLCContDB.setPrtNo(mPrtNo);
			}
			LCContSet MCContSet = tLCContDB.query();

			// 此处判断工行网银渠道并做特殊处理 通过判断
			// 新单是是否保存有同样的PrtNo与ProsolConNo(且仅有一条)
			if (MCContSet.size() == 1
					&& MCContSet.get(1).getProposalContNo()
							.equals(MCContSet.get(1).getPrtNo())
					&& MCContSet.get(1).getAgentCom().startsWith("PY001")) {
				tLCContDB.setPrtNo(MCContSet.get(1).getPrtNo());
				tLCContDB.setProposalContNo("YBT"
						+ MCContSet.get(1).getProposalContNo());
				String sql = "update LCCont set ProposalContNo=" + "\'YBT"
						+ MCContSet.get(1).getProposalContNo() + "\' "
						+ "where ContNo= \'" + mLCCont.getChildText("ContNo")
						+ "\'";
				cLogger.info("工行网银更新数据SQL:" + sql);
				ExeSQL exe = new ExeSQL();

				if (!exe.execUpdateSQL(sql)) {
					throw new MidplatException("网银客户端更新数据失败!!");
				}
			} else {
				tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
				tLCContDB.setProposalContNo(mLCCont
						.getChildText(ProposalContNo));
			}

			if (tLCContDB.getContNo().equals("")
					&& tLCContDB.getPrtNo().equals("")
					&& tLCContDB.getProposalContNo().equals("")) {
				throw new MidplatException("相关号码不能为空！");
			}

			LCContSet tLCContSet = tLCContDB.query();
			if ((null == tLCContSet) || (tLCContSet.size() < 1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			if (!tLCContSchema.getAppFlag().equals("1")) {
				throw new MidplatException("该保单不处于承保状态！");
			}

			if (!tLCContSchema.getSignDate().equals(
					DateUtil.getCurDate("yyyy-MM-dd"))) {
				throw new MidplatException("非当日保单，不能冲正！");
			}

			// 农信社走此路线，在此判断是否安徽农信社
			if (mLKTransStatusDB.getBankBranch().startsWith("ANHNX")) {
				String mCardFlag = tLCContSchema.getCardFlag();
				if (mCardFlag.equals("9")) { // 银保
					mGlobalInput = YbtSufUtil.getGlobalInput(
							mLKTransStatusDB.getBankCode(),
							mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				} else if (mCardFlag.equals("a")) { // 信保
					mGlobalInput = YbtSufUtil.getGlobalInput1(
							mLKTransStatusDB.getBankCode(),
							mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				}
			} else {

				mGlobalInput = YbtSufUtil.getGlobalInput(
						mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			}

			YbtWriteOffBL tYbtWriteOffBL = new YbtWriteOffBL(tLCContSchema,
					mGlobalInput);
			tYbtWriteOffBL.deal();

			mLKTransStatusDB.setPolNo(tLCContSchema.getContNo());
			mLKTransStatusDB.setPrtNo(tLCContSchema.getPrtNo());
			mLKTransStatusDB.setProposalNo(tLCContSchema.getProposalContNo());
			mLKTransStatusDB.setTransAmnt(-tLCContSchema.getPrem());
			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));

			LKTransStatusDB tLKTransStatusDB01 = new LKTransStatusDB();
			tLKTransStatusDB01.setPolNo(mLKTransStatusDB.getPolNo());
			tLKTransStatusDB01.setStatus("1");
			tLKTransStatusDB01.setFuncFlag("01");
			LKTransStatusSet tLKTransStatusSet01 = tLKTransStatusDB01.query();
			if (1 != tLKTransStatusSet01.size()) {
				cLogger.error("查询新契约日志异常："
						+ tLKTransStatusDB01.mErrors.getFirstError());
				throw new MidplatException("新契约日志异常！");
			}
			LKTransStatusSchema tLKTransStatusSchema01 = tLKTransStatusSet01
					.get(1);
			tLKTransStatusSchema01.setStatus("2"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
			tLKTransStatusSchema01.setModifyTime(DateUtil
					.getCurDate("HH:mm:ss"));

			MMap tSubmitMMap = new MMap();
			LKTransStatusSchema tLKTransStatusSchema = mLKTransStatusDB
					.getSchema();
			tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
			tSubmitMMap.put(tLKTransStatusSchema01, "UPDATE");
			VData tSubmitVData = new VData();
			tSubmitVData.add(tSubmitMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tSubmitVData, "")) {
				cLogger.error(tPubSubmit.mErrors.getFirstError());
				throw new MidplatException("更新日志失败！");
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
			if (mLKTransStatusDB.getBankCode().equals("03")) { // 建行特殊处理
				cLogger.debug("为建行冲正专门组织一些特有返回信息...");
				Element ttRiskCode = new Element(RiskCode);
				ttRiskCode.setText(tLKTransStatusSchema01.getRiskCode());

				Element ttContNo = new Element(ContNo);
				ttContNo.setText(tLCContSchema.getContNo());

				Element ttAppntName = new Element(AppntName);
				ttAppntName.setText(tLCContSchema.getAppntName());

				Element ttInsuredName = new Element("InsuredName");
				ttInsuredName.setText(tLCContSchema.getInsuredName());

				Element ttPrem = new Element(Prem);
				ttPrem.setText(new DecimalFormat("0.00").format(tLCContSchema
						.getPrem()));

				Element ttBankAccNo = new Element(BankAccNo);
				ttBankAccNo.setText(tLCContSchema.getBankAccNo());

				Element ttLCCont = new Element(LCCont);
				ttLCCont.addContent(ttRiskCode);
				ttLCCont.addContent(ttContNo);
				ttLCCont.addContent(ttAppntName);
				ttLCCont.addContent(ttInsuredName);
				ttLCCont.addContent(ttPrem);
				ttLCCont.addContent(ttBankAccNo);

				mOutXmlDoc.getRootElement().addContent(ttLCCont);
			}
			if (mLKTransStatusDB.getBankCode().equals("45")
					&& mLKTransStatusDB.getBankBranch().startsWith("LINNX")) { // 辽宁农信社特别处理
				Element ttLCCont = new Element("LCCont");
				Element ttTransNo = new Element("TransNo");

				String sql_tran = "select transno from lktransstatus where polno = '"
						+ mLKTransStatusDB.getPolNo()
						+ "' and funcflag = '01' and descr is null and rcode = '1' with ur";
				String xTransNo = new ExeSQL().getOneValue(sql_tran);

				ttTransNo.setText(xTransNo);

				ttLCCont.addContent(ttTransNo);
				mOutXmlDoc.getRootElement().addContent(ttLCCont);
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！"
							+ mLKTransStatusDB.mErrors.getFirstError());
				}
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out AIO_WriteOff.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into AIO_WriteOff.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		if ("60".equals(mBaseInfo.getChildText(BankCode))
				|| "69".equals(mBaseInfo.getChildText(BankCode))) {
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"
					+ mBaseInfo.getChildText(BankCode) + "' and banknode = '"
					+ mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild(ZoneNo).setText(ZoneNos);

		}

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out AIO_WriteOff.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		// String mInFile =
		// "F:/working/picch/std/testXml/WriteOff/std_04_in.xml";
		// String mOutFile =
		// "F:/working/picch/std/testXml/WriteOff/std_04_out.xml";

		String mInFile = "E:/Test-haoqt/中行资料/boc/testXml/writeOff/boc_04_in.xml";
		String mOutFile = "E:/Test-haoqt/中行资料/boc/testXml/writeOff/boc_04_out.xml";
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new AIO_WriteOff(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
	}
}
