package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.newCont.NxsContBL;
import com.sinosoft.midplat.kernel.service.newCont.NxsContInsuredIntlBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtAutoCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class NXS_NewContAll extends ServiceImpl {
	public NXS_NewContAll(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into NXS_NewContAll.service()...");
		long mStartMillis = System.currentTimeMillis();
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false;	//删除数据标志
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			String tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			//如果PrtNo不存在，则可以根据PrtNo删除数据
			tSQL = "select 1 from lccont where PrtNo='" + mLCCont.getChildText(PrtNo) + "' with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("该投保单印刷号已使用，请更换！");
			} else {
				mDeleteable = true;
			}
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
//			农信社riskcode传的是套餐编码.根据套餐编码查出险种编码 --yin jiajia 
			String tRiskWrapCode = pInXmlDoc.getRootElement().getChild(
					LCCont).getChild(LCInsureds).getChild(LCInsured).getChild(
							Risks).getChild(Risk).getChildText(MainRiskCode);
			cLogger.debug("RiskWrapCode = " + tRiskWrapCode);
			Element tThisConfRoot = cThisBusiConf.getParent();
			if (-1 != tThisConfRoot.getChildText("GWN_Risk").indexOf(tRiskWrapCode)) {	//走picch特有的国际贸易流程
				//投保单录入
				NxsContBL ttNxsContBL = new NxsContBL(pInXmlDoc, mGlobalInput);
				ttNxsContBL.deal();
				
				//被保人、险种信息录入
				NxsContInsuredIntlBL ttNxsContInsuredIntlBL = 
					new NxsContInsuredIntlBL(pInXmlDoc, mGlobalInput);
				ttNxsContInsuredIntlBL.deal();
				
				//自核+复核
				YbtAutoCheck ttYbtAutoCheck = 
					new YbtAutoCheck(mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();
				
				//财务收费
				YbtTempFeeBL ttYbtTempFeeBL = 
					new YbtTempFeeBL(mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtTempFeeBL.deal();
				
				//签单+单证核销+回执回销
				YbtLCContSignBL ttYbtLCContSignBL = 
					new YbtLCContSignBL(mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtLCContSignBL.deal();
				
				//组织返回报文
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(ttYbtLCContSignBL.getContNo());
				mOutXmlDoc = ttYbtContQueryBL.deal();
			} else {
				throw new MidplatException("套餐编码有误！" + tRiskWrapCode);
			}
			
			//超时自动删除数据
			long tUseTime = System.currentTimeMillis() - mStartMillis;
			long tTimeOutLong = 1 * 60 * 1000;	//默认超时设置为1分钟；如果未配置超时时间，则使用该值。
			try {
				tTimeOutLong = Integer.parseInt(cThisBusiConf.getChildText(timeout)) * 1000L;
			} catch (Exception ex) {	//使用默认超时设置
				cLogger.warn("未配置超时设置，或配置有误！" + ex);
			}
			if (tUseTime > tTimeOutLong) {
				cLogger.error("核心处理超时！UseTime=" + tUseTime/1000.0 + "s；TimeOut=" + tTimeOutLong/1000.0 + "s；投保书号：" + mLKTransStatusDB.getPrtNo());
				throw new MidplatException("系统繁忙，请稍后再试！" + tUseTime);
			}
			
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			
			String mCertifyCode = null;
			ExeSQL mExeSQL = new ExeSQL();
			tSQL = "select certifycode from lkcertifymapping where bankcode = '"
					+ mLKTransStatusDB.getBankCode()+ "' "						
					+ "and managecom = '"+ mGlobalInput.ManageCom.substring(0, 4)+ "' "						
					+ "and RiskWrapCode = '"+ mLKTransStatusDB.getRiskCode() + "' " 
					+ "with ur";
			SSRS tSSRS = mExeSQL.execSQL(tSQL);// 农信社单证编码需要从单证编码定义表中查询
			if (tSSRS.getMaxRow() < 1) {
				throw new MidplatException("未查询到对应的单证编码!");
			}
			mCertifyCode = tSSRS.GetText(1, 1);
			if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
				new CardManage(mCertifyCode, mGlobalInput)
							.makeUsed(tOutLCCont.getChildText(ProposalContNo).substring(2, 12));
			}

			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setStatus("1");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String ttDesr = ex.getMessage();
				try {
					if (null!=ttDesr && ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){}
				mLKTransStatusDB.setDescr(ttDesr);
			}
			
			if (mDeleteable) {
				try {
					YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
				} catch (Exception tBaseEx) {
					cLogger.error("删除新单数据失败！", tBaseEx);
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out NXS_NewContAll.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into NXS_NewContAll.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		String mCurrentDate = DateUtil.getCur10Date();
		String mCurrentTime = DateUtil.getCur8Time();
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		String mRiskWrapCode = pXmlDoc.getRootElement().getChild(LCCont).getChild(
				LCInsureds).getChild(LCInsured).getChild(
						Risks).getChild(Risk).getChildText(MainRiskCode);
		mLKTransStatusDB.setRiskCode(mRiskWrapCode);//信保通传的是套餐编码
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out NXS_NewContAll.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "F:/working/picch/std/testXml/NewCont/std_01_in_331201.xml";
		String mOutFile = "F:/working/picch/std/testXml/NewCont/std_01_out_331201.xml";
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new NXS_NewContAll(null).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
