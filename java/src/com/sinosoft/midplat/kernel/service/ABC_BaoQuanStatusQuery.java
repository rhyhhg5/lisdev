package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LPYBTAppWTDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPYBTAppWTSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPYBTAppWTSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class ABC_BaoQuanStatusQuery extends ServiceImpl {
	
	public ABC_BaoQuanStatusQuery(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_BaoQuanStatusQuery.service()...");
		 
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		LKTransStatusDB mLKTransStatusDB = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));

			// 进行犹豫期、退保 业务处理
			if("WT".equals(mLCCont.getChildText("BaoQuanFlag")) || "CT".equals(mLCCont.getChildText("BaoQuanFlag"))){
				mOutXmlDoc = deal(pInXmlDoc);
			}
			if("MQ".equals(mLCCont.getChildText("BaoQuanFlag"))){ //满期给付
				throw new MidplatException("满期给付交易未开通");
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束

		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Into ABC_BaoQuanStatusQuery.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into ABC_BaoQuanStatusQuery.deal()...");
		
		Element xTranData = pInXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");

		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);
		LCContSet mCont = cont.query();
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		if(mCont.size() > 0){
			LCContSchema lc = mCont.get(1);
			
			Element nEdorAcceptNo = new Element("EdorAcceptNo");
			nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
			
			Element nContNo = new Element("ContNo");
			nContNo.setText(lc.getContNo());
			
			Element nAttachmentContent = new Element("AttachmentContent");
			
			Element nTransStatus = new Element("TransStatus");	//保单申请状态 对账状态
			Element nAppState = new Element("AppState");	//保单状态
			Element AppwtApplyDate = new Element("AppwtApplyDate");	//保全申请日期
			Element nAppwtEndDate = new Element("AppwtEndDate");	//生效日期
			Element nEdorType = new Element("EdorType");	//业务类别
			
			LPYBTAppWTSet mLPYBTAppWTSet = new LPYBTAppWTSet();
			LPYBTAppWTDB mLPYBTAppWTDB = new LPYBTAppWTDB();
			mLPYBTAppWTDB.setContNo(mContNo);
			mLPYBTAppWTSet = mLPYBTAppWTDB.query();
			
			if(mLPYBTAppWTSet.size() > 0){	//判断保单是否已存在犹豫期退保表中！
				LPYBTAppWTSchema mLPYBTAppWTSchema = mLPYBTAppWTSet.get(1);
				
				nTransStatus.setText("2");	//对账成功
				nAppState.setText(mLPYBTAppWTSchema.getAppState());
				AppwtApplyDate.setText(mLPYBTAppWTSchema.getAppWTDate());
				if(mLPYBTAppWTSchema.getAppState().equals("2")){	//申请成功 增加判断如果对账成功
					nAppwtEndDate.setText(mLPYBTAppWTSchema.getModifyDate());
				}else {
					nAppwtEndDate.setText("");
				}
				nEdorType.setText(mLPYBTAppWTSchema.getEdorType());
			}else {
				throw new MidplatException("保全表中无保单信息！");
			}
			
			LCPolDB pol_db = new LCPolDB();
			pol_db.setContNo(mContNo);
			LCPolSet polSet = pol_db.query();
			String risk_main = "";
			String risk_fu = "";
			for (int i = 1; i <= polSet.size(); i++) {
				LCPolSchema mPol = polSet.get(i);
				if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
					risk_main = mPol.getRiskCode();
				}
				if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
					risk_fu = mPol.getRiskCode();
				}
			}
			
			Element nMainRiskCodeName = new Element("MainRiskCodeName");
			nMainRiskCodeName.setText(risk_main);
			Element nRiskCodeName = new Element("RiskCodeName");
			if(!"".equals(risk_fu)){
				nRiskCodeName.setText(risk_fu);
			}
			mFlag.setText("1");
			mDesc.setText("交易成功！");
			
			nAttachmentContent.addContent(nAppState);
			nAttachmentContent.addContent(nTransStatus);
			nAttachmentContent.addContent(AppwtApplyDate);
			nAttachmentContent.addContent(nAppwtEndDate);
			nAttachmentContent.addContent(nEdorType);
			nAttachmentContent.addContent(nMainRiskCodeName);
			nAttachmentContent.addContent(nRiskCodeName);
			mEdorInfo.addContent(nEdorAcceptNo);
			mEdorInfo.addContent(nContNo);
			mEdorInfo.addContent(nAttachmentContent);
		}else {
			mFlag.setText("0");
			mDesc.setText("未查到对应保单信息！");
			throw new MidplatException("未查到对应保单信息！");
		}
		
		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		cLogger.info("Out ABC_BaoQuanStatusQuery.deal()...");
		return new Document(mTranData);
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into ABC_BaoQuanStatusQuery.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild("LCCont");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText("ContNo"));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Prem"));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("RiskCode"));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out ABC_BaoQuanStatusQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}
}

