package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.yinbaotongbank.QueryLYSendToBank;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class BatQuery extends ServiceImpl {
    public BatQuery(Element pThisBusiConf) {
        super(pThisBusiConf);
    }

    public Document service(Document pInXmlDoc) {
        cLogger.info("Into BatQuery.service()...");
        Document mOutXmlDoc = null;
        LKTransStatusDB mLKTransStatusDB = null;
        GlobalInput mGlobalInput = null;

        try {
            mLKTransStatusDB = insertTransLog(pInXmlDoc);

            mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
                    .getBankCode(), mLKTransStatusDB.getBankBranch(),
                    mLKTransStatusDB.getBankNode());

            /**
             * 调用核心批量查询接口
             */
            String mManageCom = mGlobalInput.ManageCom.substring(0, 2);
            String mBankCode = mLKTransStatusDB.getBankCode();
            String mSql = "select bankcode from ldbank where operator = 'ybt' and agentcode = '"
                    + mBankCode
                    + "' and comcode = '"
                    + mManageCom
                    + "' with ur";
            mBankCode = new ExeSQL().getOneValue(mSql);

            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("ComCode", mManageCom);
            mTransferData.setNameAndValue("BankCode", mBankCode);
            mTransferData.setNameAndValue("SendDate", DateUtil
                    .getCurDate("yyyy-MM-dd"));

            TransferData tTransferData = (TransferData) new QueryLYSendToBank()
                    .queryData(mTransferData);

            String mBatIncome = (String) tTransferData
                    .getValueByName("queryDealForS");
            String mBatPay = (String) tTransferData
                    .getValueByName("queryDealForF");
            if (!mBatIncome.equals("1")) {
                mBatIncome = "0";
            }
            if (!mBatPay.equals("1")) {
                mBatPay = "0";
            }

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
            Element BatIncome = new Element("BatIncome");
            Element BatPay = new Element("BatPay");
            BatIncome.setText(mBatIncome);
            BatPay.setText(mBatPay);
            Element mLCCont = new Element(LCCont);
            mLCCont.addContent(BatIncome);
            mLCCont.addContent(BatPay);
            mOutXmlDoc.getRootElement().addContent(mLCCont);

            mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
            mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        } catch (Exception ex) {
            cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

            if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
                mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
                mLKTransStatusDB.setDescr(ex.getMessage());
            }

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
        }

        if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
            if (null != mGlobalInput) {
                mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
            }
            mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
            if (!mLKTransStatusDB.update()) {
                cLogger.error("更新日志信息失败！"
                        + mLKTransStatusDB.mErrors.getFirstError());
            }
        }

        cLogger.info("Out BatQuery.service()!");
        return mOutXmlDoc;
    }

    public LKTransStatusDB insertTransLog(Document pXmlDoc)
            throws MidplatException {
        cLogger.info("Into BatQuery.insertTransLog()...");

        Element mTranData = pXmlDoc.getRootElement();
        Element mBaseInfo = mTranData.getChild(BaseInfo);

        String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
        String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

        LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
        mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
        mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
        mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
        mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
        mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
        mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
        mLKTransStatusDB.setTransDate(mCurrentDate);
        mLKTransStatusDB.setTransTime(mCurrentTime);
        mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        mLKTransStatusDB.setMakeDate(mCurrentDate);
        mLKTransStatusDB.setMakeTime(mCurrentTime);
        mLKTransStatusDB.setModifyDate(mCurrentDate);
        mLKTransStatusDB.setModifyTime(mCurrentTime);

        if (!mLKTransStatusDB.insert()) {
            cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
            throw new MidplatException("插入日志失败！");
        }

        cLogger.info("Out BatQuery.insertTransLog()!");
        return mLKTransStatusDB;
    }

    public static void main(String[] args) throws Exception {
        System.out.println("程序开始…");

        String mInFile = "E:/BAT900.xml";
        String mOutFile = "E:/BAT900out.xml";

        FileInputStream mFis = new FileInputStream(mInFile);
        InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
        Document mInXmlDoc = new SAXBuilder().build(mIsr);

        Document mOutXmlDoc = new BatQuery(null).service(mInXmlDoc);

        XMLOutputter mXMLOutputter = new XMLOutputter();
        mXMLOutputter.setEncoding("GBK");
        mXMLOutputter.setTrimText(true);
        mXMLOutputter.setIndent("   ");
        mXMLOutputter.setNewlines(true);
        mXMLOutputter.output(mOutXmlDoc, System.out);
        mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

        System.out.println("成功结束！");
    }
}
