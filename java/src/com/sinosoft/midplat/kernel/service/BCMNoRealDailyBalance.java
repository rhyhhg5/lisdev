/**
 * 日终对账服务实现类
 */

package com.sinosoft.midplat.kernel.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKBalanceDetailSchema;
import com.sinosoft.lis.vschema.LKBalanceDetailSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.NumberUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.balance.DetailBalanceBL;
import com.sinosoft.midplat.kernel.service.balance.GetherBalance;
import com.sinosoft.midplat.kernel.service.balance.KernelBalanceBL;
import com.sinosoft.midplat.kernel.util.FTPDealBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class BCMNoRealDailyBalance extends ServiceImpl {
	private String cBalanceNum = null;	//对账次数
	
	public BCMNoRealDailyBalance(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into BCMNoRealDailyBalance.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		String mManageCom = null;
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);			
		
		try {
//			mLKTransStatusDB = insertTransLog(pInXmlDoc);		
//			
//			String mSQL = "select ManageCom from LKCodeMapping where Remark5='1' "
//				+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
//				+ "and ZoneNo='" + mLKTransStatusDB.getBankBranch() + "' "
//				+ "and BankNode='" + mLKTransStatusDB.getBankNode() + "' "
//				+ "with ur";
//			mManageCom = new ExeSQL().getOneValue(mSQL);
//			if ((null==mManageCom) || mManageCom.equals("")) {
//				throw new MidplatException("未查到该对帐网点！");
//			}

			//处理前置机传过来的报错信息(扫描超时等)
			String tErrorStr = mTranData.getChildText("Error");
			if (null != tErrorStr) {
				throw new MidplatException(tErrorStr);
			}		
			List tChkDetail = mTranData.getChild(ChkDetails).getChildren(ChkDetail);
			String tLocalDir = cThisBusiConf.getChildText("localdir");
			String tip = cThisBusiConf.getChildText("ftpIP");
			String tusername = cThisBusiConf.getChildText("ftpUserName");
			String tpassword = cThisBusiConf.getChildText("ftpPass");
			String tremotedir = cThisBusiConf.getChildText("remotedir");
			String tFileName = mBaseInfo.getChildText(ZoneNo)+"_"+mBaseInfo.getChildText(ZoneNo).substring(0, 3)+"IF10011_"+DateUtil.getCur8Date()+"_OFF_RET";
			FileOutputStream DXFiles_fos= new FileOutputStream(tLocalDir+tFileName);   		
			SSRS tSSRS = null;
			int tsumamnt = 0;
			int tsum = 0;
			for(int i=0;i<tChkDetail.size();i++){
				String tsql = "select p.riskcode,c.bankaccno,c.polapplydate,c.cvalidate,c.prem,c.amnt,c.contno,p.insuyearflag,p.insuyear,p.payendyearflag,p.payendyear,c.sumprem,c.payintv from lcpol p,lccont c where c.stateflag = '1' and p.contno = c.contno" +
						" and c.prtno= '"+((Element)tChkDetail.get(i)).getChildTextTrim(ProposalContNo)+"' and riskcode = '"+((Element)tChkDetail.get(i)).getChildTextTrim(RiskCode)+"' with ur";
				ExeSQL mExeSQL = new ExeSQL();
				tSSRS = mExeSQL.execSQL(tsql);
				tsum+=tSSRS.MaxRow;
				System.out.println("总条数："+tSSRS.MaxRow);
				for(int j=1;j<=tSSRS.MaxRow;j++){
					String triskflag = "select subriskflag from lmriskapp where riskcode = '"+tSSRS.GetText(j, 1)+"' with ur";
					if("M".equals(new ExeSQL().getOneValue(triskflag))){
						tsumamnt+=NumberUtil.yuanToFen(tSSRS.GetText(j, 12));
					}
					
				}
			}
			//汇总信息
				String tString = "";//				
				tString+="10        ";//银行编码
				tString+=addChar(mBaseInfo.getChildTextTrim(ZoneNo),"RIGHT",10,0," ");//银行分行代码
				tString+=addChar(mBaseInfo.getChildText(ZoneNo).substring(0, 3)+"IF10011","RIGHT",10,0," ");//保险分公司编码
				tString+=mBaseInfo.getChildText(TransrNo).substring(18, 26);//交易日期
				tString+=DateUtil.getCurDate("HHmmss");//交易时间
				tString+=mBaseInfo.getChildTextTrim("BankDate");//当前对账日期
				//总承保笔数
//				tString+="0000000001";
				System.out.println("总比书："+tsum);
				tString+=addChar(String.valueOf(tsum),"LEFT",10,0,"0");
				//总承保金额
				
//				tString+="000000000100000";
				tString+=addChar(String.valueOf(tsumamnt),"LEFT",15,0,"0");
				System.out.println("总金额："+tsumamnt);
				tString+= "\r\n"; 
				DXFiles_fos.write(tString.getBytes());
				DXFiles_fos.flush();
				
		    //明细信息
			for(int i=0;i<tChkDetail.size();i++){				
				Element ttChkDetail = (Element)tChkDetail.get(i);
				String tsql = "select p.riskcode,c.bankaccno,c.polapplydate,c.cvalidate,p.prem,c.amnt,c.contno,p.insuyearflag,p.insuyear,p.payendyearflag,p.payendyear,c.sumprem,c.payintv from lcpol p,lccont c where c.stateflag = '1' and p.contno = c.contno" +
				" and c.prtno= '"+((Element)tChkDetail.get(i)).getChildTextTrim(ProposalContNo)+"' and riskcode = '"+ttChkDetail.getChildTextTrim(RiskCode)+"' with ur";
		        ExeSQL mExeSQL = new ExeSQL();
		        tSSRS = mExeSQL.execSQL(tsql);
		        for(int m=1;m<=tSSRS.MaxRow;m++){
		        String ttString = "";
				ttString+=addChar(ttChkDetail.getChildTextTrim(BrNo),"RIGHT",10,0," ");//银行网点代码
				ttString+=addChar(ttChkDetail.getChildTextTrim(ProposalContNo),"RIGHT",35,0," ");//投保单号
				ttString+=addChar(ttChkDetail.getChildTextTrim(AppntName),"RIGHT",60,0," ");//投保人姓名
				ttString+=addChar(ttChkDetail.getChildTextTrim(IDType),"RIGHT",3,0," ");//投保人证件类型
				ttString+=addChar(ttChkDetail.getChildTextTrim(IDNo),"RIGHT",30,0," ");//投保人证件号码
				ttString+=addChar(ttChkDetail.getChildTextTrim(RiskCode),"RIGHT",10,0," ");//险种代码
				ttString+=addChar(ttChkDetail.getChildTextTrim(BankAccNo),"RIGHT",30,0," ");//缴费账户
				System.out.println("险种代码：");
				String triskflag = "select subriskflag from lmriskapp where riskcode = '"+tSSRS.GetText(m, 1)+"' with ur";
				if("M".equals(new ExeSQL().getOneValue(triskflag))){
					ttString+="0";//主副险标记
				}else{
					ttString+="1";//主副险标记
				}
//				tString+="0";//主副险标记
				ttString+=mBaseInfo.getChildText(TransrNo).substring(18, 26);//投保日期
				ttString+=addChar(ttChkDetail.getChildTextTrim(ZoneNo),"RIGHT",10,0," ");//银行分行代码
				ttString+=addChar(ttChkDetail.getChildTextTrim("InsuNo"),"RIGHT",10,0," ");//保险分公司编码
//				ttString+=mBaseInfo.getChildText(TransrNo).substring(18, 26);//承包日期
				ttString+=DateUtil.formatTrans(tSSRS.GetText(m, 4),"yyyy-MM-DD","yyyyMMDD");
				ttString+=addChar(String.valueOf(NumberUtil.yuanToFen(tSSRS.GetText(m, 5))),"LEFT",15,0,"0");;//保费
//				tString+="000000012345654";//保费
//				tString+="000000012345654";//保额
				ttString+=addChar(String.valueOf(NumberUtil.yuanToFen(tSSRS.GetText(m, 6))),"LEFT",15,0,"0");//保额
				ttString+=addChar(tSSRS.GetText(m, 7),"RIGHT",35,0," ");//保单号
//				tString+="2  ";//保险年期类型
				ttString+=tranInsuYearFlag(tSSRS.GetText(m, 8));//保险年期类型
//				tString+="5  ";//保险年期
				ttString+=addChar(tSSRS.GetText(m, 9).trim(),"RIGHT",3,0," ");				
//				ttString+="2  ";//缴费年期类型 
				ttString+=tranPayendFlag(tSSRS.GetText(m, 10).trim());
//				tString+="5  ";//缴费年期
				ttString+=addChar(tSSRS.GetText(m, 11),"RIGHT",3,0," ");
//				tString+="000000012345654";//总保费
//				ttString+=addChar(String.valueOf(NumberUtil.yuanToFen(tSSRS.GetText(m, 12))),"LEFT",15,0,"0");//总保费
				if("M".equals(new ExeSQL().getOneValue(triskflag))){
					ttString+=addChar(String.valueOf(NumberUtil.yuanToFen(tSSRS.GetText(m, 12))),"LEFT",15,0,"0");//总保费
				}else{
					ttString+=addChar("0","LEFT",15,0,"0");//总保费
				}
//				tString+="5  ";//缴费方式
				ttString+=tranPayIntv(tSSRS.GetText(m, 13).trim());
				ttString+= "\r\n";
				DXFiles_fos.write(ttString.getBytes());
				DXFiles_fos.flush();
		        }
			}
			DXFiles_fos.close();
			FTPDealBL tFTPDealBL = new FTPDealBL(tip,tusername,tpassword,21);
			File tFile = new File(tLocalDir+tFileName);
			tFTPDealBL.ApacheFTPUploadFile(tFile, tremotedir);
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "对账成功！");
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；2-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error("日终对账(BCMNoRealDailyBalance)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；2-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setManageCom(mManageCom);
			mLKTransStatusDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out BCMNoRealDailyBalance.service()!");
		return mOutXmlDoc;
	}
    public static String tranPayendFlag(String tinsureflag){
    	String minsureflag = tinsureflag;
    	if("A".equals(minsureflag)){
    		minsureflag = "3  ";
    	}else if("M".equals(minsureflag)){
    		minsureflag = "4  ";
    	}else if("D".equals(minsureflag)){
    		minsureflag = "5  ";
    	}else if("Y".equals(minsureflag)){
    		minsureflag = "2  ";
    	}else{
    		 minsureflag+="  ";
    	}
    	return minsureflag;
    }
    public static String tranInsuYearFlag(String tinsureflag){
    	String minsureflag = tinsureflag;
    	if("A".equals(minsureflag)){
    		minsureflag = "3  ";
    	}else if("M".equals(minsureflag)){
    		minsureflag = "4  ";
    	}else if("D".equals(minsureflag)){
    		minsureflag = "5  ";
    	}else if("Y".equals(minsureflag)){
    		minsureflag = "2  ";
    	}else{
    		minsureflag = minsureflag+"  ";
    	}
    	return minsureflag;
    }
    public static String tranPayIntv(String tinsureflag){
    	String minsureflag = tinsureflag;
    	if("-2".equals(minsureflag)){
    		minsureflag = "9  ";
    	}else if("-1".equals(minsureflag)){
    		minsureflag = "7  ";
    	}else if("0".equals(minsureflag)){
    		minsureflag = "1  ";
    	}else if("1".equals(minsureflag)){
    		minsureflag = "2  ";
    	}else if("3".equals(minsureflag)){
    		minsureflag = "3  ";
    	}else if("6".equals(minsureflag)){
    		minsureflag = "4  ";
    	}else if("12".equals(minsureflag)){
    		minsureflag = "5  ";
    	}else{
    		minsureflag+="  ";
    	}
    	return minsureflag;
    }
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into BCMNoRealDailyBalance.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo).substring(0, 10)+mCurrentTime);
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		/**
		 * 如果前置机扫描超时，系统当前时间可能已经是第二天0点，
		 * 此处(TransDate)不取系统当前时间，改用前置机传过来的时间。
		 * 前置机必须保证TranData/BaseInfo/BankDate中的时间的正确性，
		 * 如果是ftp方式对账，BankDate应为本轮扫描的开始时间；
		 * 如果是交易方式，需要银行将此字段传为白天交易的时间。
		 */
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out BCMNoRealDailyBalance.insertTransLog()!");
		return mLKTransStatusDB;
	}	
	/**
	 * 给字符串补足字符, 第一第二参数必须填不能默认
	 * 当前只能补充空格和单字节字符，还不能补充中文
	 * 
	 * @param source
	 *            待添加字符的字符串，不能为null
	 * @param type
	 *            补充字符的位置，左、右或是中间
	 * @param length
	 *            补充字符后字符串总长,注意不是总字节数
	 * @param position
	 *            补充字符在字符串中的起始位置
	 * @return
	 */
	 public static String addChar(String source, String type ,int length ,int position ,String insertChar )
	    {	
	    	String tInsertChar = "";
	    	if(insertChar ==null|| "".equals(insertChar))
	    	{
	    		tInsertChar = " ";
	    	}
	    	else
	    	{
	    		tInsertChar = insertChar;
	    	}
	    	
	    	if(source==null)
	    	{
	    		source = ""; 
	    	}
	    	byte temByte[] = source.getBytes();
	    	int sourceLength =temByte.length;
	    	
	    	String resultStr = source;
	    	byte retByte[] = new byte[300];
	    	if("LEFT".equals(type))
	    	{
	    		if(length < sourceLength)
	    		{
	    			System.arraycopy(temByte, 0, retByte, 0, length);
	    			resultStr = (new String(retByte)).trim();
	    		}
	    		
	    		for(int i=1; i<=length - sourceLength ;i++)
	    		{
	    			resultStr = tInsertChar + resultStr;
	    		}
	    	}
	    	else if("RIGHT".equals(type))
	    	{
	    		if(length < sourceLength)
	    		{
	    			System.arraycopy(temByte, sourceLength-length, retByte, 0, length);
	    			resultStr = (new String(retByte)).trim();
	    		}
	    		
	    		for(int i=1; i<=length - sourceLength ;i++)
	    		{
	    			resultStr = resultStr + tInsertChar;
	    		}
	    	}
	    	else if("MIDDLE".equals(type))
	    	{
	    		
	    	}
	    	return resultStr;
	    }
	 public static void main(String[] args)throws Exception{
		 System.out.println("险种代码1："+addChar("5","RIGHT",3,0," "));
		
		}
}
