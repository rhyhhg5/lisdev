package com.sinosoft.midplat.kernel.service;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.message.*;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.ServiceImpl;

import java.io.IOException;
import java.util.*;

import org.jdom.Document;
import org.jdom.Element;

/**
 * <p>Title: 客户短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SendMsgTool extends ServiceImpl{  
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private String mCurrentDate = PubFun.getCurrentDate();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();    

    public SendMsgTool(Element pThisBusiConf) {
    	super(pThisBusiConf);
    }


    public Document service(Document pInXmlDoc) {
    	Document outNoStd = null;
    	try {
    		outNoStd = getMessage();
		} catch (MidplatException e) {
			e.printStackTrace();
		}
		return outNoStd;
    }

    private boolean sendMsg() {
        System.out.println("银保通异常短信通知开始......");
        SmsServiceSoapBindingStub binding = null;
        try {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().getSmsService(); //创建binding对象
        } catch (Exception jre) {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("ybt"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("18：00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        Vector vec = new Vector();
//        vec = getMessage();
        System.out.println("接收者号码："+((SmsMessage)vec.get(0)).getReceiver());

        if (!vec.isEmpty()) {
            msgs.setMessages((SmsMessage[]) vec.toArray(new SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            try {
            	System.out.println("ServiceType: "+msgs.getServiceType());
                value = binding.sendSMS(msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                System.out.println(value.getStatus());
                System.out.println(value.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.print("无符合条件案件！");
        }
        
        
        return true;
    }   	
    	
    	

    private Document getMessage() throws MidplatException {
//        
//        System.out.println(tSQL);
//
//        SSRS tMsgSSRS = tExeSQL.execSQL(tSQL);
//        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
//            String tBankcode = tMsgSSRS.GetText(i, 1);
//            String tproposalno = tMsgSSRS.GetText(i, 2);
//            String tprtno = tMsgSSRS.GetText(i, 3);
//            String transtime = tMsgSSRS.GetText(i, 4);
//            String tMobile ="18211094072";
//            String tContents = "短信测试";
//                tContents = "银行："+tBankcode+"在"+transtime+"发生提交数据失败异常，请核查。"+"投保单号："+tprtno+",保单印刷号："+tproposalno;
//            System.out.println("发送短信：" +tMobile );
//            SmsMessage msg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
//            msg.setReceiver(tMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//            msg.setContents(tContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            //和IT讨论后，归类到二级机构
//            msg.setOrgCode("86000000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
//            SmsMessage msg2 = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
//            msg2.setReceiver("13146756753"); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//            msg2.setContents(tContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            //和IT讨论后，归类到二级机构
//            msg2.setOrgCode("86000000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
//            tVector.add(msg);
//            tVector.add(msg2);
//        }
        
        Element mDetails = new Element("Details");//and bak2 is null
    	String tSQL =
    		"select bankcode,proposalno,prtno,transtime from lktransstatus where descr like '%提交签单数据%' and transdate = '"+DateUtil.getCurDate("yyyy-MM-dd")+"' and bak2 is null  with ur";
    	ExeSQL mExeSQL = new ExeSQL();
        SSRS mResult = mExeSQL.execSQL(tSQL);
        LKTransStatusDB trans = null;
        VData vData = new VData();
        MMap vMap = new MMap();
        PubSubmit tPs = new PubSubmit();
        String sql_lk = "";
        if(mResult.MaxRow > 0){
        	for (int i = 1; i <= mResult.getMaxRow(); i++) {
        		
        		String bankCode = mResult.GetText(i, 1);
        		String proposalNo = mResult.GetText(i, 2);
        		String prtNo = mResult.GetText(i, 3);
        		String transTime = mResult.GetText(i, 4);
        		
                Element mDetail = new Element("Detail");
                Element mBankCode = new Element("BankCode");
                Element mProposalNo = new Element("ProposalNo");
                Element mPrtNo = new Element("PrtNo");
                Element mTransTime = new Element("TransTime");
        		
        		mBankCode.setText(bankCode);
        		mProposalNo.setText(proposalNo);
        		mPrtNo.setText(prtNo);
        		mTransTime.setText(transTime);
        		
        		System.out.println("@@@@@@@@@@"+proposalNo+"   "+prtNo);
        		
        		mDetail.addContent(mBankCode);
        		mDetail.addContent(mProposalNo);
        		mDetail.addContent(mPrtNo);
        		mDetail.addContent(mTransTime);
        		mDetails.addContent(mDetail);
        		
        		sql_lk = "update lktransstatus set bak2 = '1' where proposalno = '"+proposalNo+"' and " +
        				"prtNo = '"+prtNo+"'";
        		vMap.put(sql_lk, "UPDATE");
        		vData.add(vMap);
        	}
        	
        }
        System.out.println("############"+vData.size());
        if (!tPs.submitData(vData, "")) {
        	throw new MidplatException("更新日志失败！");
        }
        
        Element mTranData = new Element("TranData");
        mTranData.addContent(mDetails);
        return new Document(mTranData);
    }

//    public static void main(String[] args) {
//        GlobalInput mGlobalInput = new GlobalInput();
//        LCContSchema tLCContSchema = new LCContSchema();
//        VData mVData = new VData();
//        mGlobalInput.Operator = "001";
//        mGlobalInput.ManageCom = "86";
//        mVData.add(mGlobalInput);
//
//        SendMsgTool tSendMsgTool = new SendMsgTool(null);
//        tSendMsgTool.service(null);
////        tSendMsgTool.submitData(mVData, "LPMSG");
////       System.out.print(mPolicyAbateDealBL.edorseState("00000619801"));
////         mPolicyAbateDealBL.edorseState("0000126302");
//    }


}
