package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YbtBalaMonitor extends ServiceImpl {
	public YbtBalaMonitor(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into YbtBalaMonitor.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;

		try {

			ExeSQL tExeSQL = new ExeSQL();
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			Element tTranData = new Element("TranData");
			Element tRetData = new Element("RetData");
			Element tTimeCostDetail = null;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(calendar.DATE, -1);
			String  mBalaDate =  new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

			//对账汇总情况监控
			String tBalaSQL = "select bankcode,managecom,descr,rcode from lktransstatus a"
					+ " where transdate = '"+ mBalaDate
					+ "' and funcflag = '17' order by bankcode,rcode";
			SSRS tSSRS = tExeSQL.execSQL(tBalaSQL);
			if (tSSRS.MaxRow >= 1) {
		
				String tDesc = "各银行对账情况：";
				for (int a = 1; a <= tSSRS.MaxRow; a++) {
					String tBankCode = tSSRS.GetText(a, 1);
					String tBankName = tBankCode;
					if ("01".equals(tBankCode)) {
						tBankName = "工行";
					}
					if ("02".equals(tBankCode)) {
						tBankName = "中行";
					}
					if ("03".equals(tBankCode)) {
						tBankName = "建行";
					}
					if ("04".equals(tBankCode)) {
						tBankName = "农行";
					}
					if ("10".equals(tBankCode)) {
						tBankName = "交行";
					}
					if ("16".equals(tBankCode)) {
						tBankName = "邮储";
					}
					if ("48".equals(tBankCode)) {
						tBankName = "临商银行";
					}
					if ("49".equals(tBankCode)) {
						tBankName = "四川农信社";
					}
					if ("45".equals(tBankCode)) {
						tBankName = "农信社";
					}
					String tManagecomName = tSSRS.GetText(a, 2);
					String tNameSql = "select name from ldcom where comcode = '"+tSSRS.GetText(a, 2)+"'";
				    SSRS ttSSRS = tExeSQL.execSQL(tNameSql);
				    if(ttSSRS.MaxRow>0){
				    	tManagecomName = ttSSRS.GetText(1, 1);
				    } 
				    String tDetailSQL = "select 1 from lktransstatus a"
						+ " where transdate = '"+ mBalaDate+"' and bankcode = '"+tBankCode
						+ "' and funcflag = '01' and rcode = '1' and status = '1' and substr(managecom,1,4) = '"+tSSRS.GetText(a, 2)+"'";
				    if ("01".equals(tBankCode)||"04".equals(tBankCode)||"16".equals(tBankCode)){
				    	tDetailSQL = "select 1 from lktransstatus a"
							+ " where transdate = '"+ mBalaDate+"' and bankcode = '"+tBankCode
							+ "' and funcflag = '01' and rcode = '1' and status = '1' and substr(managecom,1,2) = '"+tSSRS.GetText(a, 2)+"'";
				    } 
				if("1".equals(tSSRS.GetText(a, 4))){				 
					tDesc = "对账成功:"+tBankName+"：分公司名称："+tManagecomName;
				}else {
					SSRS mSSRS = tExeSQL.execSQL(tDetailSQL);
				if(mSSRS.MaxRow>=1){
					tDesc = "对账失败:"+tBankName+",分公司名称："+tManagecomName+",失败原因："+tSSRS.GetText(a, 3);
				}else{
					tDesc = "无需对账，当日无新单："+tBankName+",分公司名称："+tManagecomName+"";
				}
				}
				tTimeCostDetail = null;
				tTimeCostDetail = new Element("TimeCostDetail");
				tTimeCostDetail.setText(tDesc);
				tRetData.addContent(tTimeCostDetail);
				}
			}else{
				tTimeCostDetail = null;
				tTimeCostDetail = new Element("TimeCostDetail");
				tTimeCostDetail.setText("各银行都没有对账，请检查批处理是否正常");
				tRetData.addContent(tTimeCostDetail);
			}
			//对账明细情况监控
			tBalaSQL = "select bankcode,polno,desbalance from lktransstatus a"
					+ " where transdate = '"+ mBalaDate
					+ "' and funcflag = '01'" + " and resultbalance = '1' and rcode = '1'";
			tSSRS = tExeSQL.execSQL(tBalaSQL);
			if (tSSRS.MaxRow >= 1) {
				for (int a = 1; a <= tSSRS.MaxRow; a++) {
				String tDesc = "对账失败明细情况 ：保单号："+tSSRS.GetText(a, 2)+",失败原因："+tSSRS.GetText(a, 3);
				tTimeCostDetail = null;
				tTimeCostDetail = new Element("TimeCostDetail");
				tTimeCostDetail.setText(tDesc);
				tRetData.addContent(tTimeCostDetail);
				}
			}
			
			//增加邮储批量交易监控
		    tTimeCostDetail = new Element("TimeCostDetail");
		    String psbc_sql = "select descr from lktransstatus where bankcode = '16' and funcflag = '35' and makedate = '" + mBalaDate + "' with ur";
			if(tExeSQL.getOneValue(psbc_sql).startsWith("上传文件失败")){
				tTimeCostDetail.setText("邮储犹撤手续费上传失败：" + tExeSQL.getOneValue(psbc_sql));
			}else {
				tTimeCostDetail.setText("邮储犹撤手续费上传成功");
			}
			tRetData.addContent(tTimeCostDetail);
			
			tTranData.addContent(tRetData);
			mOutXmlDoc = new Document(tTranData);

			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setStatus("1"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex) {
					cLogger.error(uex.getMessage());
				}

				mLKTransStatusDB.setDescr(tDesr);
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out YbtBalaMonitor.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into YbtBalaMonitor.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch("86");
		mLKTransStatusDB.setBankNode("86");
		mLKTransStatusDB.setBankOperator("ybtauto");
		mLKTransStatusDB.setTransNo("monitor" + mCurrentDate + mCurrentTime);
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out YbtBalaMonitor.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		Document tMonitorDoc = null;
		Element tTranData = new Element("TranData");
		Element tBaseInfo = new Element("BaseInfo");
		Element tBankCode = new Element("BankCode");
		tBankCode.setText("57");
		Element tFuncFlag = new Element("FunctionFlag");
		tFuncFlag.setText("57");
		tBaseInfo.addContent(tBankCode);
		tBaseInfo.addContent(tFuncFlag);
		tTranData.addContent(tBaseInfo);
		tMonitorDoc = new Document(tTranData);
		YbtBalaMonitor tYbtBalaMonitor = new YbtBalaMonitor(null);
		tMonitorDoc = tYbtBalaMonitor.service(tMonitorDoc);
		System.out.println(JdomUtil.toString(tMonitorDoc));
	}
}
