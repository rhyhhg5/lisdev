/**
 * 获取保单详情查询
 * 根据日期获取批量包个数并返回
 */

package com.sinosoft.midplat.kernel.service;

import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CCB_NewCallGet extends ServiceImpl {

	public CCB_NewCallGet(Element pThisConf) {
		super(pThisConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_NewCallGet.service()..."); 
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element tTranData = new Element("TranBody");
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

			Element mTranData = pInXmlDoc.getRootElement();
			Element mLccont = mTranData.getChild("LCCont");
			String mDate = mLccont.getChildTextTrim("TranDate");
			cLogger.info("建行方发起查询时间::"+mDate);
			//返回建行要求18个字段值
			String [] Array_GetData = {"","Ins_Co_ID","Cvr_ID","InsPolcy_No","AgIns_Rmndr_TpCd","Plchd_Nm","Plchd_Crdt_TpCd","Plchd_Crdt_No"
			,"TXN_DT","InsPrem_Amt","CnclIns_Amt","Rnew_PyF_Amt","Rnew_Pbl_Dt","InsPolcy_ExDat","CrnPrd_XtDvdAmt","Acm_XtDvdAmt",
			"XtraDvdn_Pcsg_MtdCd","Plchd_AccNo","Ins_PD_TpCd"
			};   
			
			//1.查询8天之后续期缴费
			String mSQL1 = "SELECT '010020',"
						    +"( select riskcode from  lktransstatus where polno = c.contno and funcflag ='01' and bankcode ='03' and status='1'),"//s.riskcode,
						    +" c.contno,'1',c.appntname,c.appntidtype,c.appntidno,"
						    +" c.cvalidate -1 days,c.prem,'','续期缴费金额待定',c.paytodate,'','',"
						    +" '','','',''FROM lccont c where"
						    +" c.paytodate = (to_date('"+mDate+"','YYYY-MM-DD') + 8 days)"
						    +" and exists(select 1 from lktransstatus ls where   ls.status='1'"
							+" AND ls.bankcode ='03'"
							+" AND ls.funcflag ='01'"
							+" and ls.polno = c.contno)"
							 +"WITH ur ";
			
			//2.提醒退保金额到账
			String mSQL2 = "select '010020',s.riskcode,c.contno,'2',c.APPNTNAME,c.APPNTIDTYPE,"
					           +" c.APPNTIDNO,c.cvalidate -1 days ,"
					           +" c.prem,b.sumgetmoney ,'','','','','','',c.bankaccno,''"
						  	   +" from lpedoritem a,ljaget b,lbcont c,lktransstatus s"
							   +" where a.edorno=b.otherno"
							   +" and a.contno=c.contno "
							   +" and a.edorno=c.edorno "
							   +" and c.conttype='1' "
							   +" and b.salechnl in ('04','13')"  
							   +" and a.edortype in ('CT','WT','XT')"
							   +" and a.edorstate='0' and b.confdate ='"+mDate+"'"
							   +" and s.bankcode ='03'"
							   +" and s.polno = c.contno"
							   +" and s.funcflag ='01'"
							   +" and s.descr is null"
							   +" with ur";
			
			//3.红利派发提醒
			String   mSQL3 = " select '010020',ls.riskcode,lb.contno,"
					         + "'3',lc.APPNTNAME,lc.APPNTIDTYPE,"
					         + " lc.APPNTIDNO,lc.cvalidate -1 days ,lc.prem,"
					         + " '','','','', lb.BONUSMONEY,'',"
					         +" lp.BONUSGETMODE,lc.bankaccno,'保险产品类型代码'"
					         + " from  lobonuspol lb left join  lccont lc on lb.contno = lc.contno "
					         + " left join lktransstatus ls on lc.contno = ls.polno "
					         + " left join lcpol lp on lp.contno = ls.polno"
					         + " where"
					         + " lb.bonusmakedate= '"+mDate+"'"
					         + " and lp.riskcode = ls.riskcode"
					         + " and lb.contno is not null"
					         + " and ls.funcflag ='01'"
					         + " and ls.bankcode ='03'"
					         + " and ls.status='1'"
					         + " and rbankvsmp ='00'"; 
			//4.保单到期失效提醒
			
			String mSQL4 = "SELECT '010020',( select riskcode from  lktransstatus where polno = c.contno and funcflag ='01' and bankcode ='03' and status='1'),"
						    +"  c.contno,'4',c.appntname,c.appntidtype,c.appntidno,"
						    +" c.cvalidate -1 days,c.prem,'',' ','',c.paytodate,'','',"
						    +" '','',''FROM lccont c where "
						    +" c.paytodate = (to_date('"+mDate+"','YYYY-MM-DD') + 8 days)"
						    +" and exists(select 1 from lktransstatus ls where   ls.status='1'"
							+"  AND ls.bankcode ='03'"
							+" AND ls.funcflag ='01'"
							+" and ls.polno = c.contno)"
							+" WITH ur";
			
            //4查询保单到期失效
		       cLogger.info("SQL1=="+mSQL1);
		       cLogger.info("SQL2=="+mSQL2);
		       cLogger.info("SQL3=="+mSQL3); 
		       cLogger.info("SQL4=="+mSQL4); 
			SSRS mCount1 = new ExeSQL().execSQL(mSQL1); //查询8天之后续期缴费
			SSRS mCount2 = new ExeSQL().execSQL(mSQL2); //提醒退保金额到账
			SSRS mCount3 = new ExeSQL().execSQL(mSQL3); //红利派发提醒
			SSRS mCount4 = new ExeSQL().execSQL(mSQL3); //红利派发提醒
			cLogger.info("缴费查数::"+mCount1.MaxRow);
			cLogger.info("退保金额到账::"+mCount2.MaxRow);
			cLogger.info("红利派发提醒::"+mCount3.MaxRow);
			cLogger.info("保单到期失效提醒::"+mCount4.MaxRow);
			int package_for_pl = mCount1.MaxRow+mCount2.MaxRow+mCount3.MaxRow+mCount4.MaxRow;
			cLogger.info("------------------------批量包个数" + (package_for_pl%500==0 ? package_for_pl/500 : package_for_pl/500+1));
			Element Ins_List = new Element("Ins_List");
			if(mCount1.MaxRow!=0){
				for(int i = 1;i<=mCount1.MaxRow;i++){
					Element Ins_Detail = new Element("Ins_Detail");
					for(int m =1;m<=18;m++){
						Element el = new Element(Array_GetData[m]);
						el.setText(mCount1.GetText(i,m));
						Ins_Detail.addContent(el);
					}
					Ins_List.addContent(Ins_Detail);
				}
			}
			
			if(mCount2.MaxRow!=0){
				for(int i = 1;i<=mCount2.MaxRow;i++){
					Element Ins_Detail = new Element("Ins_Detail");
					for(int m =1;m<=18;m++){
						Element el = new Element(Array_GetData[m]);
						el.setText(mCount2.GetText(i,m));
						Ins_Detail.addContent(el);
					}
					Ins_List.addContent(Ins_Detail);
				}
			}
			
			if(mCount3.MaxRow!=0){
				for(int i = 1;i<=mCount3.MaxRow;i++){
					Element Ins_Detail = new Element("Ins_Detail");
					for(int m =1;m<=18;m++){
						Element el = new Element(Array_GetData[m]);
						el.setText(mCount3.GetText(i,m));
						Ins_Detail.addContent(el);
					}
					Ins_List.addContent(Ins_Detail);
				}
			}
			
			if(mCount4.MaxRow!=0){
				for(int i = 1;i<=mCount4.MaxRow;i++){
					Element Ins_Detail = new Element("Ins_Detail");
					for(int m =1;m<=18;m++){
						Element el = new Element(Array_GetData[m]);
						el.setText(mCount4.GetText(i,m));
						Ins_Detail.addContent(el);
					}
					Ins_List.addContent(Ins_Detail);
				}
			}
			tTranData.addContent(Ins_List);
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");

//			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
//			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (MidplatException ex) {
			cLogger.error("提醒交易失败(CCB_GetContDetailQuery)交易失败！", ex);

//			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
//				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
//				mLKTransStatusDB.setDescr(ex.getMessage());
//			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());

		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		mOutXmlDoc.getRootElement().addContent(tTranData);
			
			cLogger.info("Out CCB_GetContDetailQuery.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into CCB_GetContDetailQuery.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_GetContDetailQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws IOException {
		CCB_NewCallGet CCB_NewCall = new CCB_NewCallGet(null);
		CCB_NewCall.service(null);
	}

}
