package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CCB_CancelInfoReturn extends ServiceImpl {
	public CCB_CancelInfoReturn(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_CancelInfoReturn.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
			String tCancelDate = 
				pInXmlDoc.getRootElement().getChild(LCCont).getChildText("CancelDate");
			mOutXmlDoc.getRootElement().addContent(
					getCancelDetails(tCancelDate, mGlobalInput.ManageCom));
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_CancelInfoReturn.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_CancelInfoReturn.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setbak1(mLCCont.getChildText("CancelDate"));	//借用此字段暂存银行传过来的撤单日期
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_CancelInfoReturn.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	private Element getCancelDetails(String pCancelDate, String pManageCom) throws MidplatException {
		cLogger.info("Into CCB_CancelInfoReturn.getCancelDetails()...");
		
		cLogger.info("CancelDate = " + pCancelDate);
		
		Element mCancelDetails = new Element("CancelDetails");
			
		ExeSQL mExeSQL = new ExeSQL();
		String mSQL = "select c.riskcode,temp.*,(select distinct mult from lbpol where contno = temp.contno),c.bankbranch,c.banknode,c.bankoperator from "
            +"(select a.contno,appntname,insuredname,sumprem,polapplydate,-sum(getmoney) from "
            +"ljagetendorse a, lbcont b where a.contno = b.contno and feeoperationtype ='WT' and a.feefinatype = 'TF' "
            +"and ((b.cardflag = '9' and b.salechnl = '04') or (b.cardflag in ('6','8') and b.salechnl = '13')) and a.makedate ='" + pCancelDate + "' "
            +"and not exists(select 1 from db2inst1.lkcodemapping where remark5 = '1' and bankcode = '03' "
            +" and managecom <> '" + pManageCom + "' and managecom like '" + pManageCom + "%' "
            +"and 1=Locate(trim(managecom),b.managecom)) "   
            +"group by a.contno, appntname, insuredname, sumprem, polapplydate) temp, lktransstatus c "
            +"where temp.contno = c.polno and c.funcflag = '01' and c.status = '1' and c.bankcode = '03' "
            +"with ur";
		if("86".equals(pManageCom)){
			mSQL = "select c.riskcode,temp.*,(select distinct mult from lbpol where contno = temp.contno),c.bankbranch,c.banknode,c.bankoperator from "
	            +"(select a.contno,appntname,insuredname,sumprem,polapplydate,-sum(getmoney) from "
	            +"ljagetendorse a, lbcont b where a.contno = b.contno and feeoperationtype ='WT' and a.feefinatype = 'TF' "
	            +"and ((b.cardflag = '9' and b.salechnl = '04') or (b.cardflag in ('6','8') and b.salechnl = '13')) and a.makedate ='" + pCancelDate + "' "	            
	            +"group by a.contno, appntname, insuredname, sumprem, polapplydate) temp, lktransstatus c "
	            +"where temp.contno = c.polno and c.funcflag = '01' and c.status = '1' and c.bankcode = '03' "
	            +"with ur";
		}
		cLogger.info("SQL = " + mSQL);
		SSRS tSSRS = mExeSQL.execSQL(mSQL);
		for(int i = 1;i <= tSSRS.getMaxRow();i++){
			Element mRiskCode = new Element(RiskCode);
			mRiskCode.setText(tSSRS.GetText(i, 1));
			
			Element mContNo = new Element(ContNo);
			mContNo.setText(tSSRS.GetText(i, 2));
			
			Element mAppntName = new Element(AppntName);
			mAppntName.setText(tSSRS.GetText(i, 3));
			
			Element mInsuredName = new Element("InsuredName");
			mInsuredName.setText(tSSRS.GetText(i, 4));
			
			Element mPrem = new Element(Prem);
			mPrem.setText(tSSRS.GetText(i, 5));
		
			Element mCancelMult = new Element("CancelMult");
			if("0".equals(tSSRS.GetText(i, 8))){
				mCancelMult.setText("1.00");
			}else{
				mCancelMult.setText(tSSRS.GetText(i, 8));
			}
			
			Element mCancelPrem = new Element("CancelPrem");
			mCancelPrem.setText(tSSRS.GetText(i, 7));
			
			//如果退费金额小于投保金额,退保类型为部分犹豫期退保.
			Element mCancelFlag = new Element("CancelFlag");
			double totalPrem = Double.parseDouble(mPrem.getText());
			double cancelPrem = Double.parseDouble(mCancelPrem.getText());
			if(totalPrem > cancelPrem){
				mCancelFlag.setText("H");//保险部分犹豫期退保
			}else{
				mCancelFlag.setText("G");//保险全单犹豫期退保
			}
			
			Element mCancelType = new Element("CancelType");
			mCancelType.setText("02");//犹豫期内退保
			
			Element mSignDate = new Element(SignDate);
			mSignDate.setText(tSSRS.GetText(i, 6));
			
			Element mCancelDate = new Element("CancelDate");
			mCancelDate.setText(pCancelDate);
			
			Element mBrNo = new Element(BrNo);
			mBrNo.setText(tSSRS.GetText(i, 9).trim() + tSSRS.GetText(i, 10).trim());
			
			Element mTellerNo = new Element(TellerNo);
			mTellerNo.setText(tSSRS.GetText(i, 9).trim().substring(0,2) + tSSRS.GetText(i, 11));
			
			Element mCancelDetail = new Element("CancelDetail");
			mCancelDetail.addContent(mRiskCode);
			mCancelDetail.addContent(mContNo);
			mCancelDetail.addContent(mBrNo);
			mCancelDetail.addContent(mTellerNo);
			mCancelDetail.addContent(mAppntName);
			mCancelDetail.addContent(mInsuredName);
			mCancelDetail.addContent(mPrem);
			mCancelDetail.addContent(mCancelFlag);
			mCancelDetail.addContent(mCancelMult);
			mCancelDetail.addContent(mCancelPrem);
			mCancelDetail.addContent(mCancelType);
			mCancelDetail.addContent(mSignDate);
			mCancelDetail.addContent(mCancelDate);
			
			mCancelDetails.addContent(mCancelDetail);
		}
		
		cLogger.info("Out CCB_CancelInfoReturn.getCancelDetails()!");
		return mCancelDetails;
	}
}
