package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.YBTRiskCheck;
import com.sinosoft.midplat.kernel.service.newCont.NxsContBL;
import com.sinosoft.midplat.kernel.service.newCont.NxsContInsuredIntlBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtAutoCheck;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YbtandXbt_NewCont extends ServiceImpl {
	public YbtandXbt_NewCont(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into YbtandXbt_NewCont.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		LCContSubDB mLCContSubDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false; // 删除数据标志

		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		//针对不传ZoneNo情况进行特殊处理
		String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
		String mZoneNo = mBaseInfo.getChildText("ZoneNo");
		if(mZoneNo == null || mZoneNo.equals("")){
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
		}
		
		String mSalary = mLCCont.getChild(LCAppnt).getChildText("Salary");//投保人年收入
		String mCountyType = mLCCont.getChild(LCAppnt).getChildText("CountyType");//投保人所属区域
		
		Element mRisk = mLCCont.getChild(LCInsureds).getChild(LCInsured)
				.getChild(Risks).getChild(Risk);
		String mRiskCode = mRisk.getChildText(MainRiskCode);
		
		cLogger.debug("RiskCode = " + mRiskCode);
		
		Element mThisConfRoot = cThisBusiConf.getParent();
		if (-1 != mThisConfRoot.getChildText("WN_Risk").indexOf(mRiskCode)||
				"730101".equals(mRiskCode)||
				"333501".equals(mRiskCode)||
				"333001".equals(mRiskCode)||
				"730201".equals(mRiskCode)||
				"334901".equals(mRiskCode)||
				"340401".equals(mRiskCode)||
				"335301".equals(mRiskCode)||//乌鲁木齐商业银行添加美满尊享  gaojinfu    20170726
				"730301".equals(mRiskCode)||//乌鲁木齐商业银行添加新康利  gaojinfu    20170823
				"730302".equals(mRiskCode)||//乌鲁木齐商业银行添加新康利  gaojinfu    20170823
				"232402".equals(mRiskCode)||
				"231702".equals(mRiskCode)||
				"YBT010".equals(mRiskCode)||
				"333502".equals(mRiskCode)||
				"332801".equals(mRiskCode)) { // 走银保

			mOutXmlDoc = new OIO_NewCont(cThisBusiConf).service(pInXmlDoc);

		} else { // 走信保
			try {
				String tSQL = "select 1 from lktransstatus where PrtNo='"
						+ mLCCont.getChildText(PrtNo)
						+ "' and rcode is null with ur";
				if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
					throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
				}

				Element tThisConfRoot = cThisBusiConf.getParent();
				if (-1 != tThisConfRoot.getChildText("StopSale").indexOf(
						mRiskCode)) {//#####################################################
					throw new MidplatException("该产品已停售");
				}
				
				// 如果PrtNo不存在，则可以根据PrtNo删除数据
				tSQL = "select 1 from lccont where PrtNo='"
						+ mLCCont.getChildText(PrtNo)
						+ "' and appflag = '1' with ur";
				if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
					throw new MidplatException("该投保单印刷号已使用，请更换！");
				} else {
					mDeleteable = true;
				}

				tSQL = "select prtno from lccont where AppFlag='0' and ProposalContNo='"
						+ mLCCont.getChildText(ProposalContNo) + "' with ur";
				String tPrtNoStr = new ExeSQL().getOneValue(tSQL);
				cLogger.debug("PrtNo = " + tPrtNoStr);
				if ((null != tPrtNoStr) && !tPrtNoStr.equals("")) { // 同一保单合同书印刷号(ProposalContNo)，未签单
					tSQL = "select 1 from LKTransStatus where RCode='1' and FuncFlag='01' "
							+ "and BankCode='"
							+ mBaseInfo.getChildText(BankCode)
							+ "' "
							+ "and BankBranch='"
							+ mBaseInfo.getChildText(ZoneNo)
							+ "' "
							+ "and BankNode='"
							+ mBaseInfo.getChildText(BrNo)
							+ "' "
							+ "and TransDate='"
							+ DateUtil.getCurDate("yyyy-MM-dd")
							+ "' "
							+ "and ProposalNo='"
							+ mLCCont.getChildText(ProposalContNo)
							+ "' "
							+ "and PrtNo='" + tPrtNoStr + "' " + "with ur";
					if (new ExeSQL().getOneValue(tSQL).equals("1")) { // 同一天，同一银行，同一网点 
						YbtSufUtil.clearData(tPrtNoStr);
					}
				}

				mLKTransStatusDB = insertTransLog(pInXmlDoc);

				mGlobalInput = YbtSufUtil.getGlobalInput1(mBaseInfo
						.getChildText(BankCode),
						mBaseInfo.getChildText(ZoneNo), mBaseInfo
								.getChildText(BrNo));
				
				// 停售校验 add by GaoJinfu 20180329
				YBTRiskCheck riskCheck = new YBTRiskCheck();
				String status = riskCheck.riskStopCheck(
						mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
						mRiskCode, "a");
				boolean gwnFlag = riskCheck.riskValidCheck(
						mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
						mRiskCode, "a", "GWN");
				if (status.equals("2") || !gwnFlag) {
					throw new MidplatException("该产品已停售!");
				}
				
				System.out.println(mBaseInfo.getChildText(BankCode)+"--"+mBaseInfo.getChildText(ZoneNo)+"--"+mBaseInfo.getChildText(BrNo));
				Calendar calendar1 = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				calendar1.add(Calendar.YEAR, -1);
				Date nowDate1 = calendar1.getTime();	//三号文获取当前日期前一年日期
				String compareDate = sdf.format(nowDate1);	//对比日期
				String nowDate = DateUtil.getCurDate("yyyy-MM-dd"); //当前日期
				String nowMonth = nowDate.substring(5, 7);//当前日期月份
				
				//人均收入的校验延迟一个季度 每年前四个月的年收入应对比大前年的平均人收入
				if(Integer.parseInt(nowMonth) < 4){ //每年前四个月
					calendar1.add(Calendar.YEAR, -1);
					nowDate1 = calendar1.getTime();	//三号文获取当前日期前2年日期
					compareDate = sdf.format(nowDate1);	//对比日期
				}
				 
				//乌鲁木齐 三号文校验
				if("52".equals(mBaseInfo.getChildText(BankCode))){
					ExeSQL pExeSQL = new ExeSQL();
					String pSQL;
					SSRS pSSRS= new SSRS();
					if("1".equals(mCountyType)){	//城镇
						pSQL = "select RecentPCDI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
						pSSRS = pExeSQL.execSQL(pSQL);
						if(pSSRS.MaxRow > 0){
							String tRecentPCDI = pSSRS.GetText(1, 1);
							if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCDI)){
								throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
							}
						}else {
							throw new MidplatException("未录入当地人均收入，请检查！！！");
						}	
					}else if("2".equals(mCountyType)){	//乡村
						pSQL = "select RecentPCNI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
						pSSRS = pExeSQL.execSQL(pSQL);
						if(pSSRS.MaxRow > 0){
							String tRecentPCNI = pSSRS.GetText(1, 1);
							if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCNI)){
								throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
							}
						}else {
							throw new MidplatException("未录入当地人均收入，请检查！！！");
						}	
					}
				}
				
				//广东地区所有银行年龄加险种期缴校验
				if(mGlobalInput.ManageCom.startsWith("8644"))
				{
					List  lisRisk = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
//					for(Element Risk :lisRisk){
						for(int i=0;i< lisRisk.size();i++){
							Element Risk = (Element)lisRisk.get(i);
					if(Risk != null)
					{
						String PayIntv = Risk.getChildTextTrim("PayIntv");  //获取缴费频率
						String InsuYear = Risk.getChildTextTrim("InsuYear");  //获取保险期间
						cLogger.info("获取缴费频率"+PayIntv);
						cLogger.info("获取保险期间"+InsuYear);
						
						if(!PayIntv.equals("0")){
							String appAge =  mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");  //获取投保人年龄
							System.out.println("appAge"+appAge);
							int Age = this.getAppntAge(appAge);
							
							cLogger.info("获取投保人年龄:"+appAge);
							if(Age >60 && (Integer.parseInt(InsuYear))>1){
								throw new MidplatException("投保人年龄大于60岁,且保险期间大于1年,不通过...");
							}
						}
					}
					}
				}
				
				//添加江西农信社指定生效日期校验  add by JiffGao 20180531
//				if("60".equals(mBaseInfo.getChildText(BankCode))){
//					new NXS_NewContInput(null).checkValiDate(mLCCont);
//				}
				
				// 获得信保通费率
				feeRateManage(mGlobalInput, mBaseInfo, mRisk, mRiskCode);

				// 投保单录入
				NxsContBL ttNxsContBL = new NxsContBL(pInXmlDoc, mGlobalInput);
				ttNxsContBL.deal();

				// 被保人、险种信息录入
				NxsContInsuredIntlBL ttNxsContInsuredIntlBL = new NxsContInsuredIntlBL(
						pInXmlDoc, mGlobalInput);
				ttNxsContInsuredIntlBL.deal();

				// 自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(mLCCont
						.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();

				// 组织返回报文
				tSQL = "select contno from lccont where PrtNo='"
						+ mLCCont.getChildText(PrtNo) + "' with ur";
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						new ExeSQL().getOneValue(tSQL));
				mOutXmlDoc = ttYbtContQueryBL.deal();
				
				Element xLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			    xLCCont.getChild("LoanDate").setText(pInXmlDoc.getRootElement().getChild(LCCont).getChildText("LoanStartDate"));
			    xLCCont.getChild("LoanEndDate").setText(pInXmlDoc.getRootElement().getChild(LCCont).getChildText("LoanEndDate"));
			    xLCCont.getChild("LoanInvoiceNo").setText(pInXmlDoc.getRootElement().getChild(LCCont).getChildText("LoanInvoiceNo"));
			    xLCCont.getChild("ConsignNo").setText(pInXmlDoc.getRootElement().getChild(LCCont).getChildText("ConsignNo"));
			    xLCCont.getChild("LoanContractAmt").setText(pInXmlDoc.getRootElement().getChild(LCCont).getChildText("LoanContractAmt"));
				
			    
				/**
				 * 乌鲁木齐三号文 add by LiXiaoLong 2014-04-11 begin
				 * */
				if("52".equals(mBaseInfo.getChildText(BankCode))){
					mLCContSubDB = insertLCContSubDB(pInXmlDoc,mGlobalInput);
				}
			    
				Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
				mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
				mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				
				//未做系统录入(客户姓名、性别、证件类型（身份证或其他）、证件号码、录入有效期)且超过20万的保单，做拒保处理  add by gcy 20180205
				double mPrem = Double.parseDouble(mOutXmlDoc.getRootElement().getChild("LCCont").getChildTextTrim("Prem"));
				if(mPrem > Double.parseDouble("200000.00")){
					Element mLCAppnt = mOutXmlDoc.getRootElement().getChild("LCCont").getChild("LCAppnt");
					String mSQL = "select Name,Sex,IDType,IDNo,EffEctiveDate from LCYBTCustomer where Name='"+mLCAppnt.getChildText("AppntName")+"'" +
							" and IDType='"+mLCAppnt.getChildText("AppntIDType")+"' and IDNo='"+mLCAppnt.getChildText("AppntIDNo")+"'" +
									" order by makedate desc,maketime desc with ur";
					SSRS tSSRS = new ExeSQL().execSQL(mSQL);
					if(tSSRS.getMaxRow()<1){
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
					
					if("".equals(tSSRS.GetText(1, 1)) || "".equals(tSSRS.GetText(1, 2)) 
							|| "".equals(tSSRS.GetText(1, 3)) || "".equals(tSSRS.GetText(1, 4))
							|| "".equals(tSSRS.GetText(1, 5))){
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
					
					if(tSSRS.GetText(1, 5) != null && !"".equals(tSSRS.GetText(1, 5))){
						FDate fDate = new FDate();
						Date effectiveDate = fDate.getDate(tSSRS.GetText(1, 5));
						Date tranDate = fDate.getDate(mBaseInfo.getChildText("BankDate"));
						GregorianCalendar tCalendar = new GregorianCalendar();
						tCalendar.setTime(tranDate);
						int sYears = tCalendar.get(Calendar.YEAR);
						int sMonths = tCalendar.get(Calendar.MONTH);
						int sDays = tCalendar.get(Calendar.DAY_OF_MONTH);

						GregorianCalendar eCalendar = new GregorianCalendar();
						eCalendar.setTime(effectiveDate);
						int eYears = eCalendar.get(Calendar.YEAR);
						int eMonths = eCalendar.get(Calendar.MONTH);
						int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
						
						tCalendar.set(sYears, sMonths, sDays);
						eCalendar.set(eYears, eMonths, eDays);
						long lInterval = (eCalendar.getTime().getTime() - tCalendar.getTime().getTime()) / 86400000;
						if(lInterval < 0 || lInterval>31){
							throw new MidplatException("累计保费超限，需保险公司后台登记");
						}
					}
				}
			} catch (Exception ex) {
				cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

				if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
					mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回

					String tDesr = ex.getMessage();
					try {
						if (tDesr.getBytes("utf-8").length >= 255) {
							tDesr = tDesr.substring(0, 85);
						}
					} catch (UnsupportedEncodingException uex) {
						cLogger.error(uex.getMessage());
					}

					mLKTransStatusDB.setDescr(tDesr);
				}

				if (mDeleteable) {
					try {
						YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
					} catch (Exception tBaseEx) {
						cLogger.error("删除新单数据失败！", tBaseEx);
					}
				}

				mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
			}

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！"
							+ mLKTransStatusDB.mErrors.getFirstError());
				}
			}
		}
		
		
		cLogger.info("Out YbtandXbt_NewCont.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into YbtandXbt_NewCont.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		String mRiskCode = pXmlDoc.getRootElement().getChild(LCCont).getChild(
				LCInsureds).getChild(LCInsured).getChild(Risks).getChild(Risk)
				.getChildText(MainRiskCode);
		mLKTransStatusDB.setRiskCode(mRiskCode);
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setServiceStartTime(mLCCont
				.getChildText("LoanStartDate"));// 借款起期
		mLKTransStatusDB.setServiceEndTime(mLCCont.getChildText("LoanEndDate"));// 借款终期
//		mLKTransStatusDB.setTempFeeNo(mLCCont.getChildText("LoanInvoiceNo"));// 贷款凭证编号
		if(mBaseInfo.getChildText(BankCode).equals("60")){	//江西农信社该信息过长所以存入到预留字段bak1中
			mLKTransStatusDB.setbak1(mLCCont.getChildText("LoanInvoiceNo"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("LoanContractNo"));
		}else{
			mLKTransStatusDB.setTempFeeNo(mLCCont.getChildText("LoanInvoiceNo"));//贷款凭证编号
		}
		mLKTransStatusDB.setEdorNo(mLCCont.getChildText("LoanContractAmt"));// 贷款金额
		mLKTransStatusDB.setbak3(mLCCont.getChildText("LoanContractNo"));
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		//乌鲁木齐三号文要求增加出单网点名称、销售人员工号、负责人姓名
		// add by LiXiaoLong 20140411 begin     
		if(mBaseInfo.getChildText(BankCode).equals("52")){
			mLKTransStatusDB.setbak1(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("SaleCertNo"));
			mLKTransStatusDB.setBankAcc(mLCCont.getChildText("BrCertID"));
			mLKTransStatusDB.setbak3(mLCCont.getChildText("ManagerNo"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("SaleCertName"));
			
		}
		
		if(mBaseInfo.getChildText(BankCode).equals("58")){
			mLKTransStatusDB.setReportNo(mBaseInfo.getChildText(ZoneNo));  //ZoneNo 社联编号
		}

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out YbtandXbt_NewCont.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	 public static int getAppntAge (String ageDate) {
	    	
	    	Date now = new Date();
			String sdf = new SimpleDateFormat("yyyyMMdd").format(now);
			
			int age = 0;
			int staY = Integer.parseInt(ageDate.substring(0,4));
			int staM = Integer.parseInt(ageDate.substring(4,6));
			int staD = Integer.parseInt(ageDate.substring(6,8));
			
			int nowY = Integer.parseInt(sdf.substring(0,4));
			int nowM = Integer.parseInt(sdf.substring(4,6));
			int nowD = Integer.parseInt(sdf.substring(6,8));
			
			if(staM > nowM){
				age = nowY - staY -1;
			}else if(staM == nowM) {
				if(nowD > staD){
					age = nowY - staY ;
				}else if(nowD == staD) {
					age = nowY - staY ;
				}else if(nowD < staD) {
					age = nowY - staY -1 ;
				}
					
			}else if(staM < nowM) {
				age = nowY - staY ;
			}
			
	    	return age;
	    }

	private void feeRateManage(GlobalInput mGlobalInput, Element mBaseInfo,
			Element mRisk, String tRiskWrapCode) throws MidplatException {
		cLogger.info("Into YbtandXbt_NewCont.feeRateManage()!");

		String tInsuYearFlag = mRisk.getChildText(InsuYearFlag);
		String tInsuYear = mRisk.getChildText(InsuYear);
		String tManageCom = mGlobalInput.ManageCom.substring(0, 4);

		cLogger.debug("InsuYearFlag = " + tInsuYearFlag);
		cLogger.debug("InsuYear = " + tInsuYear);
		cLogger.debug("tManageCom=" + tManageCom);

		if (-1 == cThisBusiConf.getParent().getChildText("NoFeeRate").indexOf(
				tManageCom)) {// midplatsuf.xml中配置不走赔率表算费的农信社
			// 信保通费率需从费率配置表中读取
			String mSQL = null;
			mSQL = "Select ProtocolNo,AvailableState from LKFeeRateProtocol where "
					+ "BankCode = '"
					+ mBaseInfo.getChildText(BankCode)
					+ "' and "
					+ "ManageCom = '"
					+ mGlobalInput.ManageCom.substring(0, 4)
					+ "' and "
					+ "RiskWrapCode = '"
					+ tRiskWrapCode
					+ "' and "
					+ "AgentCom in ('"
					+ mGlobalInput.AgentCom
					+ "','PC') order by remark1 with ur";
			SSRS tSSRS = new ExeSQL().execSQL(mSQL);
			if (tSSRS.getMaxRow() < 1) {
				throw new MidplatException("未配置信保通费率信息!");
			} else {
				String mProtocolNo = tSSRS.GetText(1, 1);
				String mAvailableState = tSSRS.GetText(1, 2);
				if ("1".equals(mAvailableState)) {
					mSQL = "Select feerate,feeratetype from LKFeeRateDetails where "
							+ "ProtocolNo = '"
							+ mProtocolNo
							+ "' and "
							+ "FeeRateYearFlag = '"
							+ tInsuYearFlag
							+ "' and "
							+ "FeeRateYear in ('-1','"
							+ tInsuYear
							+ "') with ur";
					SSRS ttSSRS = new ExeSQL().execSQL(mSQL);
					if (ttSSRS.getMaxRow() != 1) {
						throw new MidplatException("查询此费率明细信息为空或多于1条记录!");
					}
					String mFeeRate = ttSSRS.GetText(1, 1);
					String mFeeRateType = ttSSRS.GetText(1, 2);
					if ("".equals(mFeeRate) || null == mFeeRate) {
						throw new MidplatException("费率明细信息添加错误!");
					}
					// 此处对报文添加费率计算信息字段,目前只支持一个险种,以后如果有多个险种,需要调整.
					Element eFeeRate = new Element("FeeRate");
					eFeeRate.setText(mFeeRate);
					Element eFeeRateType = new Element("FeeRateType");
					eFeeRateType.setText(mFeeRateType);
					mRisk.addContent(eFeeRate);
					mRisk.addContent(eFeeRateType);
				} else {
					cLogger.info("费率表配置为此机构,套餐不通过费率表算费!");
				}
			}
		}

		cLogger.info("Out YbtandXbt_NewCont.feeRateManage()!");
	}
	
	private LCContSubDB insertLCContSubDB(Document pXmlDoc,GlobalInput pGlobalInput) throws MidplatException {
    	cLogger.info("Into YbtandXbt_NewCont.insertLCContSubDB()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		Element mLCAppnt = mLCCont.getChild(LCAppnt);

		LCContSubDB mLCContSubDB = new LCContSubDB();
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		mLCContSubDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLCContSubDB.setCountyType(mLCAppnt.getChildText("CountyType"));
		mLCContSubDB.setFamilySalary(mLCAppnt.getChildText("Family"));
		mLCContSubDB.setManageCom(pGlobalInput.ManageCom);
		mLCContSubDB.setOperator("ybt");
		mLCContSubDB.setMakeDate(mCurrentDate);
		mLCContSubDB.setMakeTime(mCurrentTime);
		mLCContSubDB.setModifyDate(mCurrentDate);
		mLCContSubDB.setModifyTime(mCurrentTime);
		
		if (!mLCContSubDB.insert()) {
			cLogger.error(mLCContSubDB.mErrors.getFirstError());
			throw new MidplatException("插入投保人家庭年收入表失败！");
		}
		
		String mSQL = "update LCAppnt set Salary = '" + mLCAppnt.getChildText("Salary") + "' where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
		ExeSQL mExeSQL = new ExeSQL();
		if (!mExeSQL.execUpdateSQL(mSQL)) {
			cLogger.error(mExeSQL.mErrors.getFirstError());
			throw new MidplatException("更新投保人家庭年收入失败！！！");
		}
		
		cLogger.info("Out YbtandXbt_NewCont.insertLCContSubDB()!");
		return mLCContSubDB;
    	
    }
	
	public static void main(String[] args) throws Exception, IOException {
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\trail.xml";
		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_jiangxi\\instd\\trail.xml";
		String mOutFile = "D:/out.xml";
		
		String elpath = "F:\\sinosoft\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		Element root = c.getRootElement().getChild("business");
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new YbtandXbt_NewCont(root).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
//		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
	}
	
}
