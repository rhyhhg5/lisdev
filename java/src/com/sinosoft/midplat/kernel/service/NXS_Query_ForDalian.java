package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LBContSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBLForDaLian;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class NXS_Query_ForDalian extends ServiceImpl {
	public NXS_Query_ForDalian(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into NXS_Query.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			
			//查询C表
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLKTransStatusDB.getPolNo());
			tLCContDB.setPrtNo(mLKTransStatusDB.getPrtNo());
			tLCContDB.setProposalContNo(mLKTransStatusDB.getProposalNo());
			
			//查询B表
			
			LBContDB tLBContDB = new LBContDB();
			tLBContDB.setContNo(mLKTransStatusDB.getPolNo());
			tLBContDB.setPrtNo(mLKTransStatusDB.getPrtNo());
			tLBContDB.setProposalContNo(mLKTransStatusDB.getProposalNo());
			
			
			
			if ((null == tLCContDB.getContNo() || "".equals(tLCContDB.getContNo()))
					&& 	(null == tLCContDB.getPrtNo() || "".equals(tLCContDB.getPrtNo()))
					&& (null == tLCContDB.getProposalContNo() || ""
							.equals(tLCContDB.getProposalContNo()))) 
			{
					 if(
						(null == tLBContDB.getContNo() || "".equals(tLBContDB.getContNo()))
						&& (null == tLBContDB.getPrtNo() || "".equals(tLBContDB.getPrtNo()))
					    && (null == tLBContDB.getProposalContNo() || "".equals(tLBContDB.getProposalContNo())))
					 {
						 throw new MidplatException("相关保单号码为空！"); 
				     }
			}
			
			LCContSet tLCContSet = tLCContDB.query();
			LBContSet tLBContSet = tLBContDB.query();
			//判断保单是存在
			if ((null == tLCContSet) || (tLCContSet.size() < 1)) {
				if((null == tLBContSet) || (tLBContSet.size() < 1))
				{
					throw new MidplatException("查询保单数据失败,该保单已撤销或退保!");
				}
			}
			String mCardFlag ="";
			String mCardFlagtem ="0";    //默认定义一个标志,0 位lccont表,1 位lbcont
			LCContSchema tLCContSchema  = new LCContSchema();   //初始化以备后续使用   LCCont
			LBContSchema tLBContSchema  = new LBContSchema();   //初始化以备后续使用   LBCont
			if((null != tLCContSet) && (tLCContSet.size() >= 1))
			{
				 tLCContSchema = tLCContSet.get(1);
				mCardFlag = tLCContSchema.getCardFlag();
			}
			
			if((null != tLBContSet) && (tLBContSet.size() >= 1)){
				 tLBContSchema = tLBContSet.get(1);
				 mCardFlagtem = "1";
				mCardFlag = tLBContSchema.getCardFlag();
			}
			
			

			
			if (mCardFlag.equals("9")) { // 银保
				mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
						.getBankCode(), mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			} else if (mCardFlag.equals("a")) { // 信保
				mGlobalInput = YbtSufUtil.getGlobalInput1(mLKTransStatusDB
						.getBankCode(), mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			}

			// 组织返回报文
			
			//针对正常保单
			if(mCardFlagtem.equals("0")){
				
				YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema
						.getContNo());
				mOutXmlDoc = tYbtContQueryBL.deal();

				Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				Element DALIANFLAF = new Element("DALIANFLAF");
				DALIANFLAF.setText(tLCContSchema.getStateFlag());
				tOutLCCont.addContent(DALIANFLAF);
				Element AppFlag = new Element("AppFlag");
				AppFlag.setText(tLCContSchema.getAppFlag());
				tOutLCCont.addContent(AppFlag);
				
				mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
				
				mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			}
			
			else{   //针对已经退保或者撤单的保单
				
				YbtContQueryBLForDaLian tYbtContQueryBL = new YbtContQueryBLForDaLian(tLBContSchema
						.getContNo());
				mOutXmlDoc = tYbtContQueryBL.deal();

				Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				Element DALIANFLAF = new Element("DALIANFLAF");
				DALIANFLAF.setText("2");
				tOutLCCont.addContent(DALIANFLAF);
				Element AppFlag = new Element("AppFlag");
				AppFlag.setText(tLBContSchema.getAppFlag());
				tOutLCCont.addContent(AppFlag);
				
				mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
				
				mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
				
			}
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out NXS_Query.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into NXS_Query.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out NXS_Query.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
