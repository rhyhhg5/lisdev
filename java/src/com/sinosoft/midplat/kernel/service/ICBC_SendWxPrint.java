package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ICBC_SendWxPrint extends ServiceImpl {

	public ICBC_SendWxPrint(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_SendWxPrint.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		// 电子保单生产日期
		String mTransDate = pInXmlDoc.getRootElement().getChild("BaseInfo")
				.getChildText("TransDate");
 
		try {

	
			mLKTransStatusDB =    insertTransLog(pInXmlDoc);
       //备份sql
			String cx_sql = "select prtno,polno,transdate From lktransstatus lk where bankcode in('01','02','03','04','16','57','65') and 1=1 and transdate = '" + mTransDate + "'" +
				"and funcflag = '01' and riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701','340401','334901','232401','XBT009','XBT010','XBT011','XBT012','XBT013','XBT014','XBT017','XBT015','XBT023','XBT024','XBT025','730301') and rcode = '1' " +
				" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' " +
				" and exists (select 1 from lccont where contno=polno and cardflag in ('c','d','a')) with ur";
			/*String cx_sql = "select prtno,polno,transdate From lktransstatus where bankcode in('01') and 1=1 and transdate = '" + mTransDate + "' " +
				"and funcflag = '01' and riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701','335301','730301') and rcode = '1' " +
				" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' " +
				" and exists (select 1 from lccont where contno=polno) "+
				"union select prtno,polno,transdate From lktransstatus where bankcode in('02') and 1=1 and transdate = '" + mTransDate + "' " +
				"and funcflag = '01' and riskcode in ('333501','333701','335301','730301') and rcode = '1'" +
				" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' " +
				" and exists (select 1 from lccont where contno=polno) "+
				"union select prtno,polno,transdate From lktransstatus where bankcode in('04') and 1=1 and transdate = '" + mTransDate + "' " +
				"and funcflag = '01' and riskcode in ('333501','333701','335301','730301') and rcode = '1'" +
				" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' " +
				" and exists (select 1 from lccont where contno=polno) "+
				"union select prtno,polno,transdate From lktransstatus where bankcode ='65' and transdate = '" + mTransDate + "' " +
					"and funcflag = '01' and riskcode in ('XBT009','XBT010','XBT011','XBT012','XBT013','XBT014','XBT017') and 1=1 " +
					"and rcode = '1' and Prtno = Proposalno and status='1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' " +
					"and exists (select 1 from lccont where contno=polno) with ur";*/
//			String cx_sql = "select k.prtno,k.polno,k.transdate From lktransstatus k where bankcode in('01') and 1=1 and transdate = '" + mTransDate + "' " +
//			"and funcflag = '01' and riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701') and rcode = '1' and polno  in ('033007806000001','','') " +
//			" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' "+
//			"union select prtno,polno,transdate From lktransstatus where bankcode in('02') and 1=1 and transdate = '" + mTransDate + "' " +
//			"and funcflag = '01' and riskcode in ('333501','333701') and   polno in ('033007806000001','','') and rcode = '1'" +
//			" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' with ur";

			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = null;
			tSSRS = tExeSQL.execSQL(cx_sql);

			Element mTranData = new Element("TranData");// 根节点
			Element mRetData = new Element("RetData");
			Element mChkDetails = new Element("ChkDetails");
			Element mChkDetail = null;

			System.out.println("工行网银电子保单数：" + tSSRS.getMaxRow());
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

//				String mPrtno = tSSRS.GetText(i, 1);
				String mPolno = tSSRS.GetText(i, 2);
				String tTransDate = tSSRS.GetText(i, 3);

				cx_sql = "select email From LCAddress where ADDRESSNO in (select ADDRESSNO From lcappnt where contno = '"
						+ mPolno + "') and customerno in (select appntno From lcappnt where contno = '"
						+ mPolno + "') and makedate = '" + mTransDate + "' with ur";
				
				System.out.println("投保人Email:"+ tExeSQL.getOneValue(cx_sql));
				
				String tSQL= "select lc.appntname,lc.insuredname,lk.riskcode from lktransstatus lk,lccont lc where lk.polno = '"+mPolno+"' " +
						"and lk.polno = lc.contno and lk.funcflag='01' and lk.status='1' group by lk.riskcode,lc.appntname,lc.insuredname with ur"; 
				SSRS ssrs = tExeSQL.execSQL(tSQL);
				String mAppntName = ssrs.GetText(1, 1);
				String mInsuredName = ssrs.GetText(1, 2);
				String mCombo = ssrs.GetText(1, 3);
				
				tSQL = "select riskwrapname from ldriskwrap where riskwrapcode='"+mCombo+"' fetch first 1 rows only with ur";
				mCombo = tExeSQL.getOneValue(tSQL);
				
				tSQL = "select cvalidate from lccont where contno='"+mPolno+"'";
				String mCvalidate = tExeSQL.getOneValue(tSQL);
				Element tCvalidate = new Element("Cvalidate");
				tCvalidate.setText(mCvalidate);
				
				LCPolDB mLCPolDB = new LCPolDB();
				mLCPolDB.setContNo(mPolno);
				LCPolSet mLCPolSet = mLCPolDB.query();

				if ((null == mLCPolSet) || (mLCPolSet.size() < 1)) {
					cLogger.error(mLCPolDB.mErrors.getFirstError()); 
					throw new MidplatException("查询险种信息失败！");
				}

				Element mRisks = new Element(Risks);

				for (int j = 1; j <= mLCPolSet.size(); j++) {
					LCPolSchema tLCPolSchema = (LCPolSchema) mLCPolSet.get(j);

					Element tRiskName = new Element(RiskName);
					String mSQL = "select RiskName from lmrisk where riskcode='"
							+ tLCPolSchema.getRiskCode() + "' with ur";
					tRiskName.setText(new ExeSQL().getOneValue(mSQL));

					Element tRiskCode = new Element(RiskCode);
					tRiskCode.setText(tLCPolSchema.getRiskCode());

					Element tMainRiskCode = new Element(MainRiskCode);
					if ("333001".equals(tLCPolSchema.getRiskCode())
							|| "532201".equals(tLCPolSchema.getRiskCode())) { // 万能型H区分主副险
						tMainRiskCode.setText("333001");
					} else if ("333501".equals(tLCPolSchema.getRiskCode())
							|| "532501".equals(tLCPolSchema.getRiskCode())
							|| "532401".equals(tLCPolSchema.getRiskCode())) { // 万能型H区分主副险
						tMainRiskCode.setText("333501");
					} else if ("730301".equals(tLCPolSchema.getRiskCode())
							|| "335401".equals(tLCPolSchema.getRiskCode())) { // 康利人生两全保险（分红型）
						// 康利人生两全分红型需区分主险附加险
						String mSQL2 = "select riskcode from lcpol where contno='"
								+ tLCPolSchema.getContNo()
								+ "' and polno = mainpolno with ur";
						String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
						tMainRiskCode.setText(mMainRiskCode);
					} else if ("231701".equals(tLCPolSchema.getRiskCode())
							|| "333801".equals(tLCPolSchema.getRiskCode())) { // 康乐人生个人重大疾病保险（A款）
						tMainRiskCode.setText("231701");
					} else if ("231401".equals(tLCPolSchema.getRiskCode())) { // 附加豁免保费个人疾病保险
						tMainRiskCode.setText("231401");
					} else {
						// 其余产品区分主附险
						String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '"
								+ tLCPolSchema.getContNo()
								+ "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
						String mMainRiskCode = tExeSQL.getOneValue(mSQL2);
						tMainRiskCode.setText(mMainRiskCode);
					}
					Element tRisk = new Element(Risk);
					tRisk.addContent(tRiskCode);
					tRisk.addContent(tRiskName);
					tRisk.addContent(tMainRiskCode);

					mRisks.addContent(tRisk);
				}

				mChkDetail = new Element("ChkDetail");
				Element mContNo = new Element("ContNo");
				Element mBankCode = new Element("BankCode");
				Element mEmail = new Element("Email");
				Element nTransDate = new Element("TransDate");
                String query_bankcode = "select bankcode from lktransstatus where polno = '"+mPolno+"' fetch  first 1 rows only";
				mContNo.setText(mPolno);
				mEmail.setText(tExeSQL.getOneValue(cx_sql));
				nTransDate.setText(tTransDate);
				mBankCode.setText(tExeSQL.getOneValue(query_bankcode));

				mChkDetail.addContent(mContNo);
				mChkDetail.addContent(mBankCode);
				mChkDetail.addContent(mEmail);
				mChkDetail.addContent(nTransDate);
				mChkDetail.addContent(mRisks);
				
				Element appntName = new Element("AppntName");
				appntName.setText(mAppntName);
				Element insuredName = new Element("InsuredName");
				insuredName.setText(mInsuredName);
				Element combo = new Element("Combo");
				combo.setText(mCombo);
				mChkDetail.addContent(appntName);
				mChkDetail.addContent(insuredName);
				mChkDetail.addContent(combo);
				mChkDetail.addContent(tCvalidate);
				
				mChkDetails.addContent(mChkDetail);
			}

			Element mFlag = new Element(Flag);
			Element mDesc = new Element(Desc);
			mFlag.setText("0");
			mDesc.setText("交易成功！");
			mRetData.addContent(mFlag);
			mRetData.addContent(mDesc);

			mTranData.addContent(mRetData);
			mTranData.addContent(mChkDetails);

			mOutXmlDoc = new Document(mTranData);
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out ICBC_SendWxPrint.service()...");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into ICBC_SendWxPrint.creatDocument()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
//		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo("");
		mLKTransStatusDB.setProposalNo("");
		mLKTransStatusDB.setPrtNo("");
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

//		if (!mLKTransStatusDB.insert()) {
//			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
//			throw new MidplatException("插入日志失败！");
//		}

		cLogger.info("Out ICBC_SendWxPrint.creatDocument()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		String mInFile = "C:/Users/XiaoLong/Desktop/电子报单打印20110701.xml";
//		String mOutFile = "D:/out.xml";

		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "UTF-8");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new ICBC_SendWxPrint(null).service(mInXmlDoc);

		 JdomUtil.print(mOutXmlDoc);
		// JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));

	}

}
