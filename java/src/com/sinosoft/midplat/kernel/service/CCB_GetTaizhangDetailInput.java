package com.sinosoft.midplat.kernel.service;

import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_GetTaizhangDetailInput extends ServiceImpl  {

	public CCB_GetTaizhangDetailInput(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_GetTaizhangDetailInput.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		String mSQL = null;
		ExeSQL es = new ExeSQL();
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");
			String sql = "select polno from lktransstatus where transno= '"+mLCCont.getChildText("oldTransNo")+"'";
			String ContNo = es.getOneValue(sql);
			Element tContNo = new Element("ContNo");
			tContNo.setText(ContNo);
			pInXmlDoc.getRootElement().getChild("LCCont").addContent(tContNo);
			
			if(ContNo == null || ContNo.equals("")){
				throw new MidplatException("此保单无效！");
			}
				
			mSQL = "select 1 from lccont where contno = '" + mLCCont.getChildText("ContNo") + "' with ur";
			if(es.getOneValue(mSQL).equals("1")){	//处理接口
				String mContNo = mLCCont.getChildText("ContNo");

				mOutXmlDoc = getLCCont(mContNo);
				
			}else {
				throw new MidplatException("不存在该保单信息！");
			}
			mLKTransStatusDB.setPolNo(ContNo);	
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_GetTaizhangDetailInput.service()!");
		return mOutXmlDoc;
	}
	
	public Document getLCCont(String pContNo) throws MidplatException {
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(pContNo);
		if (!mLCContDB.getInfo()) {
			throw new MidplatException("查询保单合同信息失败！");
		}
		
		Element mContNo = new Element(ContNo);
		mContNo.setText(mLCContDB.getContNo());
		
		Element mPrtNo = new Element(PrtNo);
		mPrtNo.setText(mLCContDB.getPrtNo());
		
		Element mProposalContNo = new Element(ProposalContNo);
		mProposalContNo.setText(mLCContDB.getProposalContNo());
		
		Element mPayIntv = new Element(PayIntv);
		mPayIntv.setText(
				String.valueOf(mLCContDB.getPayIntv()));
		
		Element mPayMode = new Element(PayMode);
		mPayMode.setText(mLCContDB.getPayMode());
		
		Element mAgentCode = new Element(AgentCode);
		//内部程序使用的是老的工号agentcode，但是显示给客户的都是新的集团工号 20141204
		String mSQL = "select GroupAgentCode from laagent where AgentCode = '" + mLCContDB.getAgentCode() + "' with ur";
		mAgentCode.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentGroupCode = new Element("AgentGroupCode");
		mAgentGroupCode.setText(mLCContDB.getAgentGroup());
		
		Element mAgentGroup = new Element(AgentGroup);
		mSQL = "select name from labranchgroup where agentgroup='" + mLCContDB.getAgentGroup() + "' with ur";
		mAgentGroup.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentCom = new Element(AgentCom);//1.银邮网点的名称
		mSQL = "select name from lacom where agentcom='" + mLCContDB.getAgentCom() + "' with ur";
		mAgentCom.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mAgentName = new Element(AgentName);//4.保险机构银保专管员姓名
		mSQL = "select name from laagent where AgentCode='" + mLCContDB.getAgentCode() + "' with ur";
		mAgentName.setText(new ExeSQL().getOneValue(mSQL));
		
		Element mPrem = new Element(Prem);
		mPrem.setText(
				new DecimalFormat("0.00").format(mLCContDB.getPrem()));
		
		Element mAmnt = new Element(Amnt);
		mAmnt.setText(
				new DecimalFormat("0.00").format(mLCContDB.getAmnt()));
		
		Element mManageCom = new Element(ManageCom);
		mManageCom.setText(mLCContDB.getManageCom());
		
		Element mPolApplyDate = new Element(PolApplyDate);
		mPolApplyDate.setText(mLCContDB.getPolApplyDate());
		
		Element mSignDate = new Element(SignDate);
		mSignDate.setText(mLCContDB.getSignDate());
		
		Element mCValiDate = new Element(CValiDate);
		mCValiDate.setText(mLCContDB.getCValiDate());
		
		Element mContEndDate = new Element(ContEndDate);
		mContEndDate.setText(mLCContDB.getCInValiDate());	//picch特有，保单级保单终止日期
		

		Element mLCAppnt = getLCAppnt(pContNo);
		
		Element mLCInsureds = getLCInsureds(pContNo);
		mAmnt.setText(	//picch这边保额取主险保额。LCCont表中的保额不准确，他会简单累加所有险种(LCPol)保额。
				mLCInsureds.getChild(LCInsured).getChild(Risks).getChild(Risk).getChildText(Amnt));

		
		Element mLCCont = new Element(LCCont);
		mLCCont.addContent(mContNo);
		mLCCont.addContent(mPrtNo);
		mLCCont.addContent(mProposalContNo);
		mLCCont.addContent(mPayIntv);
		mLCCont.addContent(mPayMode);
		mLCCont.addContent(mAgentCode);
		mLCCont.addContent(mAgentName);//1.银邮网点的名称
		mLCCont.addContent(mAgentGroupCode);
		mLCCont.addContent(mAgentGroup);
		mLCCont.addContent(mAgentCom);//4.保险机构银保专管员姓名
		mLCCont.addContent(mPrem);
		mLCCont.addContent(mAmnt);
		mLCCont.addContent(mManageCom);
		mLCCont.addContent(mPolApplyDate);
		mLCCont.addContent(mSignDate);
		mLCCont.addContent(mCValiDate);
		mLCCont.addContent(mContEndDate);
		mLCCont.addContent(mLCAppnt);
		mLCCont.addContent(mLCInsureds);
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		
		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		Element mTranData = new Element(TranData);
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
	
		return new Document(mTranData);
	}
	
	private Element getLCAppnt(String pContNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCAppnt()...");
		
		LCAppntDB mLCAppntDB = new LCAppntDB();
		mLCAppntDB.setContNo(pContNo);	//核心只支持单投保人。ContNo是主键
		if (!mLCAppntDB.getInfo()) {
			throw new MidplatException("查询投保人信息失败！");
		}
		LCAddressDB mAddressDB = new LCAddressDB();
		mAddressDB.setCustomerNo(mLCAppntDB.getAppntNo());
		mAddressDB.setAddressNo(mLCAppntDB.getAddressNo());
		if (!mAddressDB.getInfo()) {
			throw new MidplatException("查询投保人地址信息失败！");
		}
		
		Element mAppntNo = new Element(AppntNo);
		mAppntNo.setText(mLCAppntDB.getAppntNo());
		
		Element mAppntName = new Element(AppntName);
		mAppntName.setText(mLCAppntDB.getAppntName());
		
		Element mAppntSex = new Element(AppntSex);
		mAppntSex.setText(mLCAppntDB.getAppntSex());
		
		Element mAppntBirthday = new Element(AppntBirthday);
		mAppntBirthday.setText(mLCAppntDB.getAppntBirthday());
		
		Element mAppntIDType = new Element(AppntIDType);
		mAppntIDType.setText(mLCAppntDB.getIDType());
		
		Element mAppntIDNo = new Element(AppntIDNo);
		mAppntIDNo.setText(mLCAppntDB.getIDNo());
		
		
		Element mLCAppnt = new Element(LCAppnt);
		mLCAppnt.addContent(mAppntNo);
		mLCAppnt.addContent(mAppntName);
		mLCAppnt.addContent(mAppntSex);
		mLCAppnt.addContent(mAppntBirthday);
		mLCAppnt.addContent(mAppntIDType);
		mLCAppnt.addContent(mAppntIDNo);
		
		cLogger.info("Out YbtContQueryBL.getLCAppnt()!");
		return mLCAppnt;
	}
	
	private Element getLCInsureds(String pContNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getLCInsureds()...");
		
		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setContNo(pContNo);
		LCInsuredSet mLCInsuredSet = mLCInsuredDB.query();
		if ((null==mLCInsuredSet) || (mLCInsuredSet.size()<1)) {
			throw new MidplatException("查询被保人信息失败！");
		}
		
		Element mLCInsureds = new Element("LCInsureds");
		for (int i = 1; i <= mLCInsuredSet.size(); i++) {
			LCInsuredSchema tLCInsuredSchema = mLCInsuredSet.get(i);
			LCAddressDB tAddressDB = new LCAddressDB();
			tAddressDB.setCustomerNo(tLCInsuredSchema.getInsuredNo());
			tAddressDB.setAddressNo(tLCInsuredSchema.getAddressNo());
			if (!tAddressDB.getInfo()) {
				throw new MidplatException("查询被保人地址信息失败！");
			}
			
			Element tInsuredNo = new Element(InsuredNo);
			tInsuredNo.setText(tLCInsuredSchema.getInsuredNo());
			
			Element tAppntName = new Element(Name);
			tAppntName.setText(tLCInsuredSchema.getName());

			Element tSex = new Element(Sex);
			tSex.setText(tLCInsuredSchema.getSex());
			
			Element tBirthday = new Element(Birthday);
			tBirthday.setText(tLCInsuredSchema.getBirthday());
			
			Element tIDType = new Element(IDType);
			tIDType.setText(tLCInsuredSchema.getIDType());
			
			Element tIDNo = new Element(IDNo);
			tIDNo.setText(tLCInsuredSchema.getIDNo());
			
			Element tRisks = getRisks(pContNo, tLCInsuredSchema.getInsuredNo());
			
			Element tLCInsured = new Element("LCInsured");
			tLCInsured.addContent(tAppntName);
			tLCInsured.addContent(tInsuredNo);
			tLCInsured.addContent(tSex);
			tLCInsured.addContent(tBirthday);
			tLCInsured.addContent(tIDType);
			tLCInsured.addContent(tIDNo);
			
			tLCInsured.addContent(tRisks);
			
			mLCInsureds.addContent(tLCInsured);
		}

		cLogger.info("Out YbtContQueryBL.getLCInsureds()!");
		return mLCInsureds;
	}
	
	private Element getRisks(String pContNo, String pInsuredNo) throws MidplatException {
		cLogger.info("Into YbtContQueryBL.getRisks()...");
		
		LCPolDB mLCPolDB = new LCPolDB();
		mLCPolDB.setContNo(pContNo);
		mLCPolDB.setInsuredNo(pInsuredNo);
		LCPolSet mLCPolSet = mLCPolDB.query();
		if ((null==mLCPolSet) || (mLCPolSet.size()<1)) {
      	cLogger.error(mLCPolDB.mErrors.getFirstError());
      	throw new MidplatException("查询险种信息失败！");
		}
		
		Element mRisks = new Element(Risks);
		double mInitFeeRate = 0.00;
		System.out.println(":::::::"+mLCPolSet.size());
		for (int i = 1; i <= mLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = (LCPolSchema) mLCPolSet.get(i);

			String tRiskCodeStr = tLCPolSchema.getRiskCode();
			
			System.out.println(tRiskCodeStr);
				
			System.out.println("险种个数标识");
			Element tRiskCode = new Element(RiskCode);
			tRiskCode.setText(tLCPolSchema.getRiskCode());
			
			Element tRiskName = new Element(RiskName);
			String mSQL = "select RiskName from lmrisk where riskcode='" + tLCPolSchema.getRiskCode() + "' with ur";
			tRiskName.setText(new ExeSQL().getOneValue(mSQL));
			
			Element tMainRiskCode = new Element(MainRiskCode);
			String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '" + tLCPolSchema.getContNo() + "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
			String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
			tMainRiskCode.setText(mMainRiskCode);
			Element tCValiDate = new Element(CValiDate);
			tCValiDate.setText(tLCPolSchema.getCValiDate());
			
			Element tAmnt = new Element(Amnt);
			tAmnt.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getAmnt()));
			
			Element tPrem = new Element(Prem);
			tPrem.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getPrem()));
			
			Element tSupplementaryPrem = new Element(SupplementaryPrem);
			tSupplementaryPrem.setText(
					new DecimalFormat("0.00").format(tLCPolSchema.getSupplementaryPrem()));
			
			Element tMult = new Element(Mult);
			tMult.setText(
					String.valueOf(tLCPolSchema.getCopys()));//获取投保份数 update by zengzm 2016-11-22

			Element tPayIntv = new Element(PayIntv);
			tPayIntv.setText(
					String.valueOf(tLCPolSchema.getPayIntv()));
			
			Element tInsuYearFlag = new Element(InsuYearFlag);
			tInsuYearFlag.setText(tLCPolSchema.getInsuYearFlag());

			Element tInsuYear = new Element(InsuYear);
			tInsuYear.setText(
					String.valueOf(tLCPolSchema.getInsuYear()));
			
			Element tYears = new Element(Years);
			tYears.setText(
					String.valueOf(tLCPolSchema.getYears()));
			
			Element tPayEndYearFlag = new Element(PayEndYearFlag);
			tPayEndYearFlag.setText(tLCPolSchema.getPayEndYearFlag());
			
			Element tPayEndYear = new Element(PayEndYear);
			tPayEndYear.setText(
					String.valueOf(tLCPolSchema.getPayEndYear()));
			
			Element tPayEndDate = new Element(PayEndDate);
			tPayEndDate.setText(tLCPolSchema.getPayEndDate());
			
			Element tPayYears = new Element(PayYears);
			tPayYears.setText(
					String.valueOf(tLCPolSchema.getPayYears()));
			

			Element tRisk = new Element(Risk);
			tRisk.addContent(tRiskCode);
			tRisk.addContent(tRiskName);
			tRisk.addContent(tMainRiskCode);
			tRisk.addContent(tCValiDate);
			tRisk.addContent(tAmnt);
			tRisk.addContent(tPrem);
			tRisk.addContent(tSupplementaryPrem);
			tRisk.addContent(tMult);
			tRisk.addContent(tPayIntv);
			tRisk.addContent(tInsuYearFlag);
			tRisk.addContent(tInsuYear);
			tRisk.addContent(tYears);
			tRisk.addContent(tPayEndYearFlag);
			tRisk.addContent(tPayEndYear);
			tRisk.addContent(tPayEndDate);
			tRisk.addContent(tPayYears);

			mRisks.addContent(tRisk);
		}

		cLogger.info("Out YbtContQueryBL.getRisks()!");
		return mRisks;
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_GetTaizhangDetailInput.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_GetTaizhangDetailInput.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
}