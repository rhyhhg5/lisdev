package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.YBTRiskCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class NXS_NewContConfirm extends SimpService {
	public NXS_NewContConfirm(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into NXS_NewContCOC.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false; // 删除数据标志
		Element tOutLCCont = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		Element mBaseInfo = mTranData.getChild("BaseInfo");
		
		//陕西农信社传的是试算流水号	add by GaoJinfu 20180418
		if ("78".equals(mBaseInfo.getChildText("BankCode"))) {
			String sql = "select prtno from lktransstatus where transno = '"
					+ mLCCont.getChildText("OldTranNo")
					+ "' and funcflag = '01' with ur";
			mLCCont.getChild(PrtNo).setText(new ExeSQL().getOneValue(sql));
			sql = "select zoneno from lkcodemapping where bankcode = '"
					+ mBaseInfo.getChildText(BankCode) + "' and banknode = '"
					+ mBaseInfo.getChildText(BrNo) + "'";
			mBaseInfo.getChild("ZoneNo").setText(new ExeSQL().getOneValue(sql));
		}
		
		try {
			String tSQL = "select 1 from lktransstatus where PrtNo='"
					+ mLCCont.getChildText(PrtNo)
					+ "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			//险种校验需要用到该参数，改到这个位置   modify by GaoJinfu 20180516
			if (mLKTransStatusDB.getBankBranch().startsWith("ANHNX")) {
				mGlobalInput = YbtSufUtil.getGlobalInput1(
						mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			} else {
				mGlobalInput = YbtSufUtil.getGlobalInput(
						mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			}

			tSQL = "select appflag from lccont where PrtNo='"
					+ mLCCont.getChildText(PrtNo)
					+ "' and cardflag = 'a' with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单" + mLCCont.getChildText(PrtNo)
						+ "已承保！");
			}
			//查找套餐编码
			tSQL = "select riskcode from lktransstatus where funcflag = '01' "
					+ "and prtno = '" + mLKTransStatusDB.getPrtNo() + "' "
					+ "and rcode = '1' " + "with ur";
			String mRiskWrapCode = new ExeSQL().getOneValue(tSQL);// 套餐编码
			//停售校验 add by GaoJinfu 20180329
			YBTRiskCheck riskCheck = new YBTRiskCheck();
			String status = riskCheck.riskStopCheck(
					mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
					mRiskWrapCode, mLCCont.getChildText("SourceType"));
			if (status.equals("2")) {
				throw new MidplatException("该产品已停售!");
			}
			boolean gxFlag = riskCheck.riskValidCheck(
					mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
					mRiskWrapCode, "a", "GX");
			boolean gwnFlag = riskCheck.riskValidCheck(
					mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
					mRiskWrapCode, "a", "GWN");
			if (-1 != cThisBusiConf.getParent().getChildText("GWN_Risk")
					.indexOf(mRiskWrapCode)
					|| gwnFlag) { //走picch特有的国际贸易流程
			
				//目前农信社走此流程的只有安徽上了银保产品，故在此判断
//				if(mLKTransStatusDB.getBankBranch().startsWith("ANHNX")){
//					mGlobalInput = YbtSufUtil.getGlobalInput1(
//							mLKTransStatusDB.getBankCode(),
//							mLKTransStatusDB.getBankBranch(),
//							mLKTransStatusDB.getBankNode());
//				}else {
//					mGlobalInput = YbtSufUtil.getGlobalInput(
//							mLKTransStatusDB.getBankCode(),
//							mLKTransStatusDB.getBankBranch(),
//							mLKTransStatusDB.getBankNode());
//				}
				
				//财务收费
				YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtTempFeeBL.deal();
				mDeleteable = true;
				//签单+单证核销+回执回销
				YbtLCContSignBL tYbtLCContSignBL = new YbtLCContSignBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtLCContSignBL.deal();
				
				//组织返回报文
				YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(
						tYbtLCContSignBL.getContNo());
				mOutXmlDoc = tYbtContQueryBL.deal();
				tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				String mCertifyCode = null;// 单证编码
				String mProposalContNoNew = null;// 信保通单证操作使用,需要去掉远单证号前两位
				
				ExeSQL mExeSQL = new ExeSQL();
				tSQL = "select certifycode from lkcertifymapping where bankcode = '"
						+ mLKTransStatusDB.getBankCode()
						+ "' "
						+ "and managecom = '"
						+ mGlobalInput.ManageCom.substring(0, 4)
						+ "'"
						+ "and RiskWrapCode = '"
						+ mRiskWrapCode
						+ "' "
						+ "with ur";
				SSRS tSSRS = mExeSQL.execSQL(tSQL);
				
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的单证编码!");
				}
				mCertifyCode = tSSRS.GetText(1, 1);// 农信社单证编码需要从单证编码定义表中查询
				
				if(mGlobalInput.AgentCom.startsWith("PC21")){ //辽宁农信社13位单证号 去掉3位冠字号 add LiXiaoLong 20170703
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(3);//信保通单证操作需要去掉前两位
				}else if(mGlobalInput.AgentCom.startsWith("PC36")){	//江西农信社测试系统 12位单证 故特殊处理
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(2);//信保通单证操作需要去掉前两位
				}else if(mGlobalInput.ManageCom.startsWith("8635")){ 
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo);//信保通单证操作需要去完整12为
				}     
				else if(mGlobalInput.AgentCom.startsWith("PY066")){ 
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(2);//针对广州农商行取11位
				}
				else if(mGlobalInput.ManageCom.startsWith("8635") && mGlobalInput.AgentCom.startsWith("PY053")){ 
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo);//信保通单证操作需要去完整12为
				}
				else if(mGlobalInput.AgentCom.startsWith("PC13")){ //针对河北单证10位 add zengzm 2015-12-11
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(3);
				}
				else if(mGlobalInput.AgentCom.startsWith("PC41")){ //针对河南单证10位 add zengzm 2017-01-20
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(3);
				}
				else if(mGlobalInput.AgentCom.startsWith("PC43")){ //针对湖南单证10位 add zengzm 2016-07-26
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(3);
				}
				else if("78".equals(mLKTransStatusDB.getBankCode()) || "80".equals(mLKTransStatusDB.getBankCode()) || "81".equals(mLKTransStatusDB.getBankCode())){ //临商、陕西农信社去掉前3位  add by GaoJinfu 20180515
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(3);
				}
				else  {
					mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(2, 12);//信保通单证操作需要去掉前两位
				}
				
				if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
					new CardManage(mCertifyCode, mGlobalInput)
								.makeUsed(mProposalContNoNew);		
				}
				
				// 辽宁农信社组织关系
				if (mLKTransStatusDB.getBankBranch().startsWith("LINNX")) {
					mOutXmlDoc = NXS_NewContInput.setAppnt2Insured(mOutXmlDoc);
				}
			}
			if (-1 != cThisBusiConf.getParent().getChildText("GX_Risk")
					.indexOf(mRiskWrapCode)
					|| gxFlag) { // 走标准个险流程
				
//				mGlobalInput = YbtSufUtil.getGlobalInput(
//						mLKTransStatusDB.getBankCode(),
//						mLKTransStatusDB.getBankBranch(),
//						mLKTransStatusDB.getBankNode());
				
				//财务收费
				YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtTempFeeBL.deal();
				mDeleteable = true;
				//签单+单证核销+回执回销
				YbtLCContSignBL tYbtLCContSignBL = new YbtLCContSignBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtLCContSignBL.deal();
				
				//组织返回报文
				YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(
						tYbtLCContSignBL.getContNo());
				mOutXmlDoc = tYbtContQueryBL.deal();

				tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				
				//单证核销！
				if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
					if (mGlobalInput.AgentCom.startsWith("PY063")) {
						new CardManage("HB07/07B','YB12/10B", mGlobalInput)
								.makeUsed(tOutLCCont.getChildText(
										ProposalContNo).substring(2));
					} else {
						new CardManage("HB07/07B','YB12/10B", mGlobalInput)
								.makeUsed(tOutLCCont
										.getChildText(ProposalContNo));
					}
				}
				
				// 辽宁农信社组织关系
				if (mLKTransStatusDB.getBankBranch().startsWith("LINNX")) {
					mOutXmlDoc = NXS_NewContInput.setAppnt2Insured(mOutXmlDoc);
				}
			}
			//更新相关日志
			
			String tCurTime = DateUtil.getCurDate("HH:mm:ss");
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setProposalNo(tOutLCCont.getChildText(ProposalContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回			
			mLKTransStatusDB.setModifyTime(tCurTime);

			LKTransStatusDB tOldLKTransStatusDB = new LKTransStatusDB();
			tOldLKTransStatusDB.setBankCode(mLKTransStatusDB.getBankCode());
			tOldLKTransStatusDB.setBankBranch(mLKTransStatusDB.getBankBranch());
			tOldLKTransStatusDB.setBankNode(mLKTransStatusDB.getBankNode());
			tOldLKTransStatusDB.setProposalNo(mLKTransStatusDB.getProposalNo());
			tOldLKTransStatusDB.setRCode("1");
			LKTransStatusSet ttLKTransStatusSet = tOldLKTransStatusDB.query();
			if ((null==ttLKTransStatusSet) || (ttLKTransStatusSet.size()<1)) {
				throw new MidplatException("查询原始日志信息失败！");
			}
			LKTransStatusSchema tLKTransStatusSchema = ttLKTransStatusSet.get(1);
			tLKTransStatusSchema.setPolNo(tOutLCCont.getChildText(ContNo));
			tLKTransStatusSchema.setTransAmnt(tOutLCCont.getChildText(Prem));
			tLKTransStatusSchema.setStatus("1");
			tLKTransStatusSchema.setReportNo(mTranData.getChild("BaseInfo").getChildText("ReportNo")); //在试算日志中增加计算手续费值
			tLKTransStatusSchema.setModifyDate(mLKTransStatusDB.getModifyDate());
			tLKTransStatusSchema.setModifyTime(tCurTime);
			
			MMap tSubmitMMap = new MMap();
			tSubmitMMap.put(mLKTransStatusDB.getSchema(), "UPDATE");
			tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
			VData tSubmitVData = new VData();
			tSubmitVData.add(tSubmitMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tSubmitVData, "")) {
				cLogger.error(tPubSubmit.mErrors.getFirstError());
				throw new MidplatException("更新日志失败！");
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);
			
			if (mDeleteable) {
				try {
					YbtSufUtil.clearData(mLKTransStatusDB.getPrtNo());
				} catch (Exception tBaseEx) {
					cLogger.error("删除新单数据失败！", tBaseEx);
				}
			}
			
			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回

				String ttDesr = ex.getMessage();
				try {
					if (null != ttDesr
							&& ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex) {
				}
				mLKTransStatusDB.setDescr(ttDesr);

				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！"
							+ mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		cLogger.info("Out NXS_NewContCOC.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into NXS_NewContCOC.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		String zSQL = "select zoneno from lkcodemapping where bankcode = '"
				+ mBaseInfo.getChildText(BankCode) + "' and banknode = '"
				+ mBaseInfo.getChildText(BrNo) + "'";
		String ZoneNos = new ExeSQL().getOneValue(zSQL);
		mBaseInfo.getChild("ZoneNo").setText(ZoneNos);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTemp("1"); // 确认/取消标识：0-取消；1-确认
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out NXS_NewContCOC.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_shanxi\\instd\\confirm.xml";
		String mOutFile = "d:/out.xml";
		
		String elpath = "F:\\sinosoft\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		Element root = c.getRootElement().getChild("business");
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		Document mOutXmlDoc = new NXS_NewContConfirm(root).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
//		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
