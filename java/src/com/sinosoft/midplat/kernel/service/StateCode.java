package com.sinosoft.midplat.kernel.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class StateCode {

	/**
	 *登陆挑战请求处理成功待上报
	 */
	public static final String TRSTASK_STATUS_CHALL_0000 = "0000";

	/**
	 *登陆挑战请求处理失败
	 */
	public static final String TRSTASK_STATUS_CHALL_0001 = "0001";

	/**
	 *登陆挑战请求处理中
	 */
	public static final String TRSTASK_STATUS_CHALL_0002 = "0002";

	/**
	 *登陆挑战请求待处理
	 */
	public static final String TRSTASK_STATUS_CHALL_0003 = "0003";

	/**
	 *任务报送请求成功
	 */
	public static final String TRSTASK_STATUS_TASKN_1000 = "1000";
	/**
	 *任务报送请求失败
	 */
	public static final String TRSTASK_STATUS_TASKN_1001 = "1001";
	/**
	 *任务报送请求处理中
	 */
	public static final String TRSTASK_STATUS_TASKN_1002 = "1002";

	/**
	 *直接上传请求成功
	 */
	public static final String TRSTASK_STATUS_DIREC_2000 = "2000";
	/**
	 *直接上传请求失败
	 */
	public static final String TRSTASK_STATUS_DIREC_2001 = "2001";
	/**
	 *直接上传请求处理中
	 */
	public static final String TRSTASK_STATUS_DIREC_2002 = "2002";

	/**
	 *初始化分段请求成功
	 */
	public static final String TRSTASK_STATUS_INITS_3000 = "3000";

	/**
	 *初始化分段请求失败
	 */
	public static final String TRSTASK_STATUS_INITS_3001 = "3001";

	/**
	 *初始化分段请求处理中
	 */
	public static final String TRSTASK_STATUS_INITS_3002 = "3002";

	/**
	 *分段上传请求成功
	 */
	public static final String TRSTASK_STATUS_SEGUP_4000 = "4000";

	/**
	 *分段上传请求失败
	 */
	public static final String TRSTASK_STATUS_SEGUP_4001 = "4001";

	/**
	 *分段上传请求处理中
	 */
	public static final String TRSTASK_STATUS_SEGUP_4002 = "4002";

	/**
	 *合并分段请求成功
	 */
	public static final String TRSTASK_STATUS_MEGSG_5000 = "5000";

	/**
	 *合并分段请求失败
	 */
	public static final String TRSTASK_STATUS_MEGSG_5001 = "5001";

	/**
	 *合并分段请求处理中
	 */
	public static final String TRSTASK_STATUS_MEGSG_5002 = "5002";

	/**
	 *通知状态更新成功
	 */
	public static final String TRSTASK_STATUS_UPDST_6000 = "6000";

	/**
	 *通知状态更新失败
	 */
	public static final String TRSTASK_STATUS_UPDST_6001 = "6001";

	/**
	 *通知状态更新处理中
	 */
	public static final String TRSTASK_STATUS_UPDST_6002 = "6002";

	/**
	 *对账成功
	 */
	public static final String TRSTASK_STATUS_RECON_7000 = "7000";

	/**
	 *对账失败
	 */
	public static final String TRSTASK_STATUS_RECON_7001 = "7001";

	/**
	 *对账处理中
	 */
	public static final String TRSTASK_STATUS_RECON_7002 = "7002";

	/**
	 *质检不通过
	 */
	public static final String TRSTASK_STATUS_CHECKRES_8001 = "8001";

	/**
	 * ImageDetailed 交易成功
	 */
	public static final String IMGDTD_STATUS_0000 = "0000";

	/**
	 *ImageDetailed 交易失败
	 */
	public static final String IMGDTD_STATUS_0001 = "0001";

	/**
	 * ImageDetailed 交易处理中
	 */
	public static final String IMGDTD_STATUS_0002 = "0002";

	/**
	 * 登陆挑战
	 */
	public static final String TYPE_FUNCFLAG_00 = "00";

	/**
	 * 任务信息报送请求
	 */
	public static final String TYPE_FUNCFLAG_01 = "01";

	/**
	 * 直接上传双录信息请求
	 */
	public static final String TYPE_FUNCFLAG_02 = "02";

	/**
	 * 初始化分段上传请求
	 */
	public static final String TYPE_FUNCFLAG_03 = "03";

	/**
	 * 文件分段
	 */
	public static final String TYPE_FUNCFLAG_031 = "031";

	/**
	 * 分段上传请求
	 */
	public static final String TYPE_FUNCFLAG_04 = "04";

	/**
	 * 文件分段上传
	 */
	public static final String TYPE_FUNCFLAG_041 = "041";

	/**
	 * 合并分段请求
	 */
	public static final String TYPE_FUNCFLAG_05 = "05";

	/**
	 * 通知保信更新任务状态请求
	 */
	public static final String TYPE_FUNCFLAG_06 = "06";

	/**
	 * 对账交易请求
	 */
	public static final String TYPE_FUNCFLAG_07 = "07";

	/**
	 * 成功
	 */
	public static final int HTTP_STATUS_200 = 200;

	/**
	 * 未授权
	 */
	public static final int HTTP_STATUS_401 = 401;

	/**
	 * 前置机扫描失败/上传失败
	 */
	public static final String TRSLOG_STATUS_1 = "1";
	/**
	 * 前置机下载未处理
	 */
	public static final String TRSLOG_STATUS_5 = "5";
	/**
	 * 前置机下载进行中
	 */
	public static final String TRSLOG_STATUS_2 = "2";
	/**
	 * 前置机下载成功
	 */
	public static final String TRSLOG_STATUS_4 = "4";
	/**
	 * 前置机下载失败
	 */
	public static final String TRSLOG_STATUS_6 = "6";
	/**
	 * 前置机上传成功
	 */
	public static final String TRSLOG_STATUS_0 = "0";

	/**
	 * 直接上传
	 */
	public static final String TRSTASK_FLAG_0 = "0";

	/**
	 * 分片上传
	 */
	public static final String TRSTASK_FLAG_1 = "1";

	/**
	 * 不需要下载
	 */
	public static final String TRSTASK_UPLOAD_FLAG_1 = "1";

	/**
	 * 需要下载
	 */
	public static final String TRSTASK_UPLOAD_FLAG_0 = "0";

	/**
	 * 人保健康保险公司代码（总公司）
	 */
	public static final String INSURERCODE = "20171010122026";

	/**
	 * 人保健康保险公司代码（分公司）
	 */
	public static final String SUBINSURERCODE = "201710101220260001";

	/**
	 * 建设银行渠道编码
	 */
	public static final int JHCODE = 03;
	/**
	 * 建设银行银行代码
	 */
	public static final String JHBANKCODE = "100000000039122";

	/**
	 * 招商银行渠道编码
	 */
	public static final int ZSCODE = 14;
	/**
	 * 上海农业商业银行渠道编码 自定义编码SHNSHCODE=3
	 */
	public static final int SHNSHCODE = 3;

	/**
	 * 业务来源-银行
	 */
	public static final String BANKTASKSOURCE = "1";
	/**
	 * 业务来源-保险公司
	 */
	public static final String INSUREDTASKSOURCE = "2";
	/**
	 * 业务来源-非银行中介机构
	 */
	public static final String UNMEDIATEDTASKSOURCE = "3";

	/**
	 * 任务1号池
	 */
	public static final String TASK_POOL_1 = "1";

	/**
	 * 任务2号池
	 */
	public static final String TASK_POOL_2 = "2";

	/**
	 * 任务3号池
	 */
	public static final String TASK_POOL_3 = "3";

	/**
	 * 任务4号池
	 */
	public static final String TASK_POOL_4 = "4";

	/**
	 * 任务5号池
	 */
	public static final String TASK_POOL_5 = "5";


	public static String getOrderIdByUUId() {
		int machineId = 1;// 最大支持1-9个集群机器部署
		int hashCodeV = UUID.randomUUID().toString().hashCode();
		if (hashCodeV < 0) {// 有可能是负数
			hashCodeV = -hashCodeV;
		}
		// 0 代表前面补充0
		// 4 代表长度为4
		// d 代表参数为正数型
		return machineId + String.format("%015d", hashCodeV);
	}

	static Map<String, String> certiType = new HashMap<String, String>();
	static {
		certiType.put("0", "111");// 居民身份证
		certiType.put("1", "412");// 护照
		certiType.put("2", "123");// 军官证/警官证
		certiType.put("3", "990");// 其他
		certiType.put("4", "990");// 其他
		certiType.put("5", "113");// 户口簿
		certiType.put("6", "990");// 其他
		certiType.put("7", "990");// 其他
		certiType.put("8", "990");// 其他
		certiType.put("9", "990");// 其他
	}

	public static String getcertiType(String key) {
		String value = "";
		value = certiType.get(key);
		return value;
	}

}
