package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LOMixBKAttributeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LOMixBKAttributeSchema;
import com.sinosoft.lis.vschema.LOMixBKAttributeSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ConfirmOrCancel extends SimpService {
	private String mSourceType = ""; //insert by zengzm 针对中行自助终端特别添加 2015-10-20
	public ConfirmOrCancel(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		try {
			Element mTranData = pInXmlDoc.getRootElement();
			Element mConfirmInfo = mTranData.getChild(ConfirmInfo);
			Element mOldTranNo = mConfirmInfo.getChild("OldTranNo");
			ExeSQL es = new ExeSQL();
			
			//add by zengzm 2017-01-12 防止重复签单
			String tSQL = "select polno from lktransstatus where funcflag='01' and status='1' and transno='"+mOldTranNo.getText()+"' and makedate = current date with ur";
			String tContNo = es.getOneValue(tSQL);
			if(tContNo != null && !"".equals(tContNo)){
				throw new MidplatException("该保单已缴费成功，保单号为："+tContNo+"，请勿重复签单!");
			}
			
			Element mBkAcctNo = mConfirmInfo.getChild("BkAcctNo");
			
			mSourceType = mConfirmInfo.getChildText("SourceType");
			String mCheckReSubmitSql = "select 1 from lktransstatus where transcode = '" + mOldTranNo.getText() + "' and rcode is null with ur";
			String mReSubmitFlag = es.getOneValue(mCheckReSubmitSql);
			if ("1".equals(mReSubmitFlag)) {
				throw new MidplatException("此保单确认数据正在提交运行,不能进行二次提交,原流水号: " + mOldTranNo.getText() + "请等待!");
			}
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			//add by zengzm 2016-12-26 针对农行网银试算与签单网点不一致清楚特殊处理
			if(("04".equals(mLKTransStatusDB.getBankCode()) && "1".equals(mSourceType)) || ("02".equals(mLKTransStatusDB.getBankCode()) && "1".equals(mSourceType))){
				String mSQL = "select bankcode,bankbranch,banknode,polno from lktransstatus where transno = '"+mOldTranNo.getText()+"' with ur";
				SSRS ssrs = es.execSQL(mSQL);
				String bankcode = "";
				String bankbranch = ""; 
				String banknode = "";
				String mContNo = "";
				if(ssrs.MaxRow > 0){
					bankcode = ssrs.GetText(1, 1);
					bankbranch = ssrs.GetText(1, 2);
					banknode =  ssrs.GetText(1, 3);
					mContNo = ssrs.GetText(1, 4);
				}else{
					throw new MidplatException("试算流水号有误！");
				}
				if(!(bankcode.equals(mLKTransStatusDB.getBankCode()) && bankbranch.equals(mLKTransStatusDB.getBankBranch()) && banknode.equals(mLKTransStatusDB.getBankNode()))){
					
					cLogger.info("网银针对试算与签单网点不一致特殊处理开始...");
					String mManagecom = mGlobalInput.ManageCom;
					String mAgentcom = mGlobalInput.AgentCom;
					String mAgentcode = "";
					String mAgentgroup = "";
					mSQL = "select agentcode, agentgroup from lacomtoagent where agentcom = '"+mAgentcom+"' and relatype='1' with ur";
					ssrs = es.execSQL(mSQL);
					if(ssrs.MaxRow > 0){
						mAgentcode = ssrs.GetText(1, 1);
						mAgentgroup = ssrs.GetText(1, 2);
					}
					
					MMap mDelMMap = new MMap();
					mDelMMap.put("update lktransstatus set bankbranch = '"+mLKTransStatusDB.getBankBranch()+"' , banknode = '"+mLKTransStatusDB.getBankNode()+"' , managecom = '"+mManagecom+"' where polno='"+mContNo+"'", "UPDATE");
					mDelMMap.put("update lccont set agentcom = '"+mAgentcom+"' ,agentcode = '"+mAgentcode+"' ,agentgroup = '"+mAgentgroup+"' ,managecom = '"+mManagecom+"' ,executecom = '"+mManagecom+"' ,signcom = '"+mManagecom+"'  where contno  = '"+mContNo+"'", "UPDATE");
					mDelMMap.put("update lcpol set agentcom = '"+mAgentcom+"' ,agentcode = '"+mAgentcode+"' ,agentgroup = '"+mAgentgroup+"' ,managecom = '"+mManagecom+"'  where contno  = '"+mContNo+"'", "UPDATE");
					mDelMMap.put("update lcappnt set managecom = '"+mManagecom+"'  where contno  = '"+mContNo+"'", "UPDATE");
					mDelMMap.put("update lcinsured set managecom = '"+mManagecom+"' ,executecom = '"+mManagecom+"'  where contno  = '"+mContNo+"'", "UPDATE");
					mDelMMap.put("update lcprem set managecom = '"+mManagecom+"'  where contno  = '"+mContNo+"'", "UPDATE");
					mDelMMap.put("update lcget set managecom = '"+mManagecom+"'  where contno  = '"+mContNo+"'", "UPDATE");
					
					VData mSubmitVData = new VData();
					mSubmitVData.add(mDelMMap);
					PubSubmit mPubSubmit = new PubSubmit();
					if (!mPubSubmit.submitData(mSubmitVData, "")) {
						cLogger.error(mPubSubmit.mErrors.getFirstError());
						throw new MidplatException("更新保单数据失败！");
					}
					cLogger.info("网银针对试算与签单网点不一致特殊处理结束...");
				}
			}
				
			
			LKTransStatusDB tOldLKTransStatusDB = new LKTransStatusDB();
			tOldLKTransStatusDB.setBankCode(mLKTransStatusDB.getBankCode());
			tOldLKTransStatusDB.setBankBranch(mLKTransStatusDB.getBankBranch());
			tOldLKTransStatusDB.setBankNode(mLKTransStatusDB.getBankNode());
			tOldLKTransStatusDB.setTransNo(mLKTransStatusDB.getTransCode());
			if (!tOldLKTransStatusDB.getInfo()) {
				throw new MidplatException("查找交易失败！");
			}
			String tOldFuncFlag = tOldLKTransStatusDB.getFuncFlag();
			if("23".equals(tOldFuncFlag)){
				tOldFuncFlag = "1";
			}
			tOldLKTransStatusDB.setTransNo(mLKTransStatusDB.getTransNo());
			tOldLKTransStatusDB.setTransCode(mLKTransStatusDB.getTransCode());
			tOldLKTransStatusDB.setFuncFlag(mLKTransStatusDB.getFuncFlag());
			tOldLKTransStatusDB.setTemp(mLKTransStatusDB.getTemp());	//确认/取消标识：0-取消；1-确认
			tOldLKTransStatusDB.setTransDate(mLKTransStatusDB.getTransDate());
			tOldLKTransStatusDB.setTransTime(mLKTransStatusDB.getTransTime());
			tOldLKTransStatusDB.setMakeDate(mLKTransStatusDB.getMakeDate());
			tOldLKTransStatusDB.setMakeTime(mLKTransStatusDB.getMakeTime());
			tOldLKTransStatusDB.setModifyDate(mLKTransStatusDB.getModifyDate());
			tOldLKTransStatusDB.setModifyTime(mLKTransStatusDB.getModifyTime());
			mLKTransStatusDB.setSchema(tOldLKTransStatusDB);
			
			switch (Integer.parseInt(tOldFuncFlag)) {
				case 1 : {	//新单(01)
                    if ((mLKTransStatusDB.getBankCode().equals("16") && !mSourceType.equals("1")&& !mSourceType.equals("e"))//add by zengzm 针对邮储网银添加   添加邮储手机端不传单证号    lisai   2017.12.08
                    		|| (mLKTransStatusDB.getBankCode().equals("02") && !mSourceType.equals("8") && !mSourceType.equals("1") && !mSourceType.equals("f") && !mSourceType.equals("e"))// update by zengzm 2016-06-14针对中行网银不传单证号
							|| ("75".equals(mLKTransStatusDB.getBankCode()) && "0".equals(mSourceType))//add by gaojinfu  20170829 河北银行不传单证号
							|| ("13".equals(mLKTransStatusDB.getBankCode()) && "0".equals(mSourceType))) {//add by gaojinfu  20170829 针对兴业银行网银、自助终端和手机不传单证号
                        String mProposalContNo = mConfirmInfo.getChildText(ProposalContNo);
                        if (!mProposalContNo.startsWith("YBT")) {
                            String mSQL = "select stateflag from lzcard where certifyCode in ('HB07/07B','YB12/10B') "
                                    + "and startno<='" + mProposalContNo + "' and endno>='" + mProposalContNo + "' "
                                    + "and ReceiveCom='E" + mGlobalInput.AgentCom.substring(0, 8) + "' "
                                    + "with ur";
                            String mStateflag = new ExeSQL().getOneValue(mSQL);
                            if (!mStateflag.equals("8")) {
                            	mSQL = "select ReceiveCom from lzcard where certifyCode in ('HB07/07B','YB12/10B') "
                					+ "and startno<='" + mProposalContNo + "' "
                					+ "and endno>='" + mProposalContNo + "' with ur";
                				String mReceiveCom = new ExeSQL().getOneValue(mSQL);
                				String tReceiveCom = "E" + mGlobalInput.AgentCom.substring(0, 8);
                				if(!mReceiveCom.equals("") && !mReceiveCom.equals(tReceiveCom)){
                					throw new MidplatException("单证发放错误，请将保单号下发至网点: " + mGlobalInput.AgentCom.substring(0, 8));
                				}else {
                					throw new MidplatException("保单合同印刷号未下发，或状态无效！");
                				}
                            }
                        }
                        
                        mLKTransStatusDB.setProposalNo(mProposalContNo);
                    }
                    
                    //add by zengzm 2016-11-14 针对农行自助终端试算时不传银行账号信息特殊处理
                    if (mLKTransStatusDB.getBankCode().equals("04") && ("8".equals(mSourceType) || "1".equals(mSourceType))){
                    	LCContDB mLCContDB = new LCContDB();
                    	mLCContDB.setContNo(tOldLKTransStatusDB.getPolNo());
                    	if (!mLCContDB.getInfo()) {
                			throw new MidplatException("查询保单合同信息失败！");
                		}
                    	LCContSchema mLCContSchema = mLCContDB.getSchema();
                    	mLCContSchema.setBankAccNo(mConfirmInfo.getChildText("BankAccNo"));
                    	mLCContSchema.setAccName(mConfirmInfo.getChildText("AccName"));
                    	mLCContDB = mLCContSchema.getDB();
                    	mLCContDB.update();
                    }
                    
					/***
					 * add by LiXiaoLong 20140310 begin
					 * 建行、中行三号文缴费增加首期缴费账号与试算中续期缴费账户是否一致校验
					 * 银行缴费交易中传送首期缴费账号与续期缴费账户必须一致否则返回报错
					 * **/
					if(mLKTransStatusDB.getBankCode().equals("03") || mLKTransStatusDB.getBankCode().equals("02")){	//建行、中行
						
						String mSQL = "select BankAccNo from lccont where prtno = '" + mLKTransStatusDB.getPrtNo() + "' and makedate = Current Date with ur";
						String mBankAccNo = new ExeSQL().getOneValue(mSQL);
						
						if(!mBkAcctNo.getText().equals("") && !mBankAccNo.equals("") && !mBkAcctNo.getText().equals(mBankAccNo)){
							throw new MidplatException("首期缴费账号:"+ mBkAcctNo.getText()+ "与续期缴费账号"+ mBankAccNo +" 信息不符！！！");
						}
			
						if(!mBkAcctNo.getText().equals("") && mBankAccNo.equals("") || mBankAccNo == null){//首期缴费转账，续期缴费现金则更新lccont表中账户信息
							//update
							mSQL = "update LCCont set BankAccNo='" + mBkAcctNo.getText() + "' where prtno='" + mLKTransStatusDB.getPrtNo() + "' with ur";
							ExeSQL mExeSQL = new ExeSQL();
							if (!mExeSQL.execUpdateSQL(mSQL)) {
								cLogger.error(mExeSQL.mErrors.getFirstError());
								throw new MidplatException("更新缴费交易首期缴费账号（BankAccNo）到LCCont失败！");
							}
						}
					} 
					/***
					 * end
					 * **/
                    if(mLKTransStatusDB.getBankCode().equals("10")){
						mLKTransStatusDB.setProposalNo(mConfirmInfo.getChildText(ProposalContNo));
					}
					
					mOutXmlDoc = NewContCOC(mLKTransStatusDB, mGlobalInput);
					/**
					 * 银邮网点的名称	YbtContQueryBL类中已经添加该节点
					 * 《保险兼业代理业务许可证》 编码
					 * 银邮网点分管代理保险业务的负责人姓名及工号 
					 * 银邮网点销售人员姓名
					 * 《保险代理从业人员资格证书》编号
					 * 保险机构银保专管员姓名		YbtContQueryBL类中已经添加该节点
					 * 《保险代理从业人员资格证书》编号
					 * 邮储需要区分烟台和威海并且添加如上节点内容 add by LiXiaoLong 20131104
					 */
					if(mLKTransStatusDB.getBankCode().equals("16")){	//邮储
						if(mLKTransStatusDB.getManageCom().startsWith("863710") || mLKTransStatusDB.getManageCom().startsWith("863706")){ //86371000威海；86370600烟台
							
							Element pTranData = mOutXmlDoc.getRootElement();
							Element pLCCont = pTranData.getChild(LCCont);
							String pProposalContNo = pLCCont.getChildText("ProposalContNo");
							
							String pSQL = "select agentcom,agentcode from lccont where ProposalContNo = '" + pProposalContNo + "' with ur";
							SSRS ssrs= new ExeSQL().execSQL(pSQL);
							String pAgentCom = ssrs.GetText(1, 1);
							String pAgentCode = ssrs.GetText(1, 2);
							
							Element pLinkMan = new Element("LinkMan"); 	//银邮网点分管代理保险业务的负责人姓名
							pSQL = "select LinkMan from lacom where agentcom = '" + pAgentCom + "' with ur";
							pLinkMan.setText(new ExeSQL().getOneValue(pSQL));
							 
							Element pCorporation = new Element("Corporation");	//银邮网点分管代理保险业务的负责人工号
							pSQL = "select Corporation from lacom where agentcom = '" + pAgentCom + "' with ur";
							pCorporation.setText(new ExeSQL().getOneValue(pSQL));
							
							Element pLAComBusiLicenseCode = new Element("LAComBusiLicenseCode");	//《保险兼业代理业务许可证》 编码
							pSQL = "select busiLicenseCode from lacom where agentcom = '" + pAgentCom + "' with ur";
							pLAComBusiLicenseCode.setText(new ExeSQL().getOneValue(pSQL));

							Element pLAAgenttempName = new Element("LAAgenttempName");	//银邮网点销售人员姓名
							pSQL = "select name from LAAgenttemp where AgentCode = '" + pAgentCode + "' with ur";
							pLAAgenttempName.setText(new ExeSQL().getOneValue(pSQL));
										
							Element pQualifno = new Element("Qualifno");	//《保险代理从业人员资格证书》编号
							pSQL = "select qualifno from LAQualification where AgentCode = '" + pAgentCode + "' with ur";
							pQualifno.setText(new ExeSQL().getOneValue(pSQL));
							
							Element pCheckYanTaiWeiHai = new Element("CheckYanTaiWeiHai");	//返回节点内容用来区分烟台和威海
							pCheckYanTaiWeiHai.setText("1");
							
							pLCCont.addContent(pLAComBusiLicenseCode);
							pLCCont.addContent(pLAAgenttempName);
							pLCCont.addContent(pQualifno);
							pLCCont.addContent(pLinkMan);
							pLCCont.addContent(pCorporation);
							pLCCont.addContent(pCheckYanTaiWeiHai);	
						}
					}
					
					/**
					 * 建行/兴业银行三号文要求增加销售人员工号、销售人员姓名、销售人员资格证号 
					 * add by LiXiaoLong 20140311 begin
					 * **/
					if(mLKTransStatusDB.getBankCode().equals("03") || "13".equals(mLKTransStatusDB.getBankCode())){
						
						Element pTranData = mOutXmlDoc.getRootElement();
						Element pLCCont = pTranData.getChild(LCCont);
						
						String mSQL = "select bak2,bak3,bak4,bak1 from lktransstatus where transno = '" + mOldTranNo.getText() + "' and makedate = Current Date and descr is null with ur";
						SSRS ssrs= new ExeSQL().execSQL(mSQL);
						String pRckrNo = ssrs.GetText(1, 1);
						String pSaleName = ssrs.GetText(1, 2);
						String pSaleCertNo = ssrs.GetText(1, 3);
						String pBrName = ssrs.GetText(1, 4);
						
						Element mBrName = new Element("BrName").setText(pBrName);
						Element mRckrNo = new Element("RckrNo").setText(pRckrNo);
						Element mSaleName = new Element("SaleName").setText(pSaleName);
						Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
						
						pLCCont.addContent(mBrName);
						pLCCont.addContent(mRckrNo);
						pLCCont.addContent(mSaleName);
						pLCCont.addContent(mSaleCertNo);
					}
					
					//邮储三号文要求增加出单网点名称、销售人员工号
					if(mLKTransStatusDB.getBankCode().equals("16")){
						
						Element pTranData = mOutXmlDoc.getRootElement();
						Element pLCCont = pTranData.getChild(LCCont);
						
						String mSQL = "select bak3,bak4 from lktransstatus where transno = '" + mOldTranNo.getText() + "' and bankcode = '16' and makedate = Current Date and descr is null with ur";
						SSRS ssrs= new ExeSQL().execSQL(mSQL);
						String pBrName = ssrs.GetText(1, 1);
						String pSaleCertNo = ssrs.GetText(1, 2);
						Element mRckrNo = new Element("BrName").setText(pBrName);
						Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
						
						pLCCont.addContent(mRckrNo);
						pLCCont.addContent(mSaleCertNo);
					}
					
					//新农行、中行三号文要求增加出单网点名称、销售人员工号、负责人姓名、网点保险兼业代理业务许可证编码
					if(mLKTransStatusDB.getBankCode().equals("02")||mLKTransStatusDB.getBankCode().equals("04")){
						
						Element pTranData = mOutXmlDoc.getRootElement();
						Element pLCCont = pTranData.getChild(LCCont);
						
						String mSQL = "select bak1,bak2,bak3,bak4 from lktransstatus where transno = '" + mOldTranNo.getText() + "' and bankcode = '" + mLKTransStatusDB.getBankCode() + "' and makedate = Current Date and descr is null with ur";
						SSRS ssrs= new ExeSQL().execSQL(mSQL);
						String pBrName = ssrs.GetText(1, 1);
						String pBrCertID = ssrs.GetText(1, 2);
						String pManagerNo = ssrs.GetText(1, 3);
						String pManagerName = ssrs.GetText(1, 4);
						
						Element mBrName = new Element("BrName").setText(pBrName);
						Element mBrCertID = new Element("BrCertID").setText(pBrCertID);
						Element mManagerNo = new Element("ManagerNo").setText(pManagerNo);
						Element mManagerName = new Element("ManagerName").setText(pManagerName);
						
						pLCCont.addContent(mBrName);
						pLCCont.addContent(mBrCertID);
						pLCCont.addContent(mManagerNo);
						pLCCont.addContent(mManagerName);
					}
					
					/**
					 * 	end
					 */
					break;
				}
				
				default : {
					throw new MidplatException("交易代码错误！" + tOldFuncFlag);
				}
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out ConfirmOrCancel.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into ConfirmOrCancel.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mConfirmInfo = mTranData.getChild(ConfirmInfo);
		if("69".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
		}

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setTransCode(mConfirmInfo.getChildText("OldTranNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTemp(mConfirmInfo.getChildText(OKFlag));	//确认/取消标识：0-取消；1-确认
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out ConfirmOrCancel.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	/**
	 * 新单确认/取消
	 */
	private Document NewContCOC(LKTransStatusDB pLKTransStatusDB, GlobalInput pGlobalInput) throws Exception {
		cLogger.info("Into ConfirmOrCancel.NewContCOC()...");
		Document mOutXmlDoc = null;
		
		String mFlag = pLKTransStatusDB.getTemp();
		if (mFlag.equals("1")) {	//新单确认
			cLogger.info("新单确认...");
			
			try {
				//财务收费
				YbtTempFeeBL ttYbtTempFeeBL = new YbtTempFeeBL(
						pLKTransStatusDB.getPrtNo(), pGlobalInput);
				ttYbtTempFeeBL.deal();
				
				//签单+单证核销+回执回销
				YbtLCContSignBL ttYbtLCContSignBL = new YbtLCContSignBL(
						pLKTransStatusDB.getPrtNo(), pGlobalInput);
				ttYbtLCContSignBL.deal();
				
				//组织返回报文
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						ttYbtLCContSignBL.getContNo());
				mOutXmlDoc = ttYbtContQueryBL.deal();
				
				//单证核销！(pLKTransStatusDB.getBankCode().equals("02") && !mSourceType.equals("8"))
				//单证核销！
				if (!pLKTransStatusDB.getBankCode().equals("03")
						&& !(pLKTransStatusDB.getBankCode().equals("02") && (mSourceType.equals("8") || mSourceType.equals("1")))
						&& !(pLKTransStatusDB.getBankCode().equals("04") && (mSourceType.equals("1") || "8".equals(mSourceType) || "e".equals(mSourceType)))
						&& !("13".equals(pLKTransStatusDB.getBankCode()) && ("1".equals(mSourceType) || "8".equals(mSourceType) || "e".equals(mSourceType))) // 兴业银行电子渠道不做单证核销 add by GaoJinfu 20170906
						&& !("68".equals(pLKTransStatusDB.getBankCode()) && ("1".equals(mSourceType) || "8".equals(mSourceType) || "e".equals(mSourceType))) // 包商银行电子渠道不做单证核销 add by GaoJinfu 20171205
						&& !(pLKTransStatusDB.getBankCode().equals("16") && ("1".equals(mSourceType) || "8".equals(mSourceType) || "e".equals(mSourceType)))) {// insert by zengzm针对邮储网银签单不传单证 2016-11-07   添加邮储手机银行不传单证    lisai   20171205

					if (!pLKTransStatusDB.getProposalNo().startsWith("YBT")) {	//建行签单是不做单证核销
						new CardManage("HB07/07B", pGlobalInput).makeUsed(pLKTransStatusDB.getProposalNo());
					}
				}else {
					cLogger.info("新建行签单不做单证核销，打印保单进行单证核销！");
				}
				 
				//更新相关日志
				Element ttOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				String ttCurrentTime = DateUtil.getCurDate("HH:mm:ss");
				pLKTransStatusDB.setPolNo(ttOutLCCont.getChildText(ContNo));
				pLKTransStatusDB.setTransAmnt(ttOutLCCont.getChildText(Prem));
				pLKTransStatusDB.setManageCom(pGlobalInput.ManageCom);
				pLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				pLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束				
				pLKTransStatusDB.setModifyTime(ttCurrentTime);
				
				LKTransStatusDB ttOldLKTransStatusDB = new LKTransStatusDB();
				ttOldLKTransStatusDB.setBankCode(pLKTransStatusDB.getBankCode());
				ttOldLKTransStatusDB.setBankBranch(pLKTransStatusDB.getBankBranch());
				ttOldLKTransStatusDB.setBankNode(pLKTransStatusDB.getBankNode());
				ttOldLKTransStatusDB.setTransNo(pLKTransStatusDB.getTransCode());
				if (!ttOldLKTransStatusDB.getInfo()) {
					throw new MidplatException("查询原始日志信息失败！");
				}
				ttOldLKTransStatusDB.setPolNo(ttOutLCCont.getChildText(ContNo));
				ttOldLKTransStatusDB.setTransAmnt(ttOutLCCont.getChildText(Prem));
				ttOldLKTransStatusDB.setTransStatus("1");
				ttOldLKTransStatusDB.setStatus("1");
				ttOldLKTransStatusDB.setModifyDate(pLKTransStatusDB.getModifyDate());
				ttOldLKTransStatusDB.setModifyTime(ttCurrentTime);
                ttOldLKTransStatusDB.setProposalNo(pLKTransStatusDB.getProposalNo());
				
				//银保通交叉销售
                ybtMixSale(pGlobalInput, ttOutLCCont.getChildText(ContNo));
                
				MMap ttSubmitMMap = new MMap();
				//针对邮储更新各个表的proposalcontno字段
                if (pLKTransStatusDB.getBankCode().equals("16") || pLKTransStatusDB.getBankCode().equals("02")) {
                    ttSubmitMMap.put("update lccont set proposalcontno = '" + pLKTransStatusDB.getProposalNo() + "' where contno = '"
                            + ttOutLCCont.getChildText(ContNo) + "' with ur", "UPDATE");
                    ttSubmitMMap.put("update lcpol set proposalcontno = '" + pLKTransStatusDB.getProposalNo() + "' where contno = '"
                            + ttOutLCCont.getChildText(ContNo) + "' with ur", "UPDATE");
                    ttSubmitMMap.put("update LCCUWMaster set proposalcontno = '" + pLKTransStatusDB.getProposalNo() + "' where contno = '"
                            + ttOutLCCont.getChildText(ContNo) + "' with ur", "UPDATE");
                    ttSubmitMMap.put("update LCCUWSub set proposalcontno = '" + pLKTransStatusDB.getProposalNo() + "' where contno = '"
                            + ttOutLCCont.getChildText(ProposalContNo) + "' and proposalcontno = '" + ttOutLCCont.getChildText(ProposalContNo) + "' with ur", "UPDATE");
                
                    mOutXmlDoc.getRootElement().getChild("LCCont").getChild("ProposalContNo").setText(pLKTransStatusDB.getProposalNo());
                }
				ttSubmitMMap.put(pLKTransStatusDB.getSchema(), "UPDATE");
				ttSubmitMMap.put(ttOldLKTransStatusDB.getSchema(), "UPDATE");
				VData ttSubmitVData = new VData();
				ttSubmitVData.add(ttSubmitMMap);
				PubSubmit ttPubSubmit = new PubSubmit();
				if (!ttPubSubmit.submitData(ttSubmitVData, "")) {
					cLogger.error(ttPubSubmit.mErrors.getFirstError());
					throw new MidplatException("更新日志失败！");
				}
			} catch (Exception ex) {
				YbtSufUtil.clearData(pLKTransStatusDB.getPrtNo());
				throw ex;	//将Exception抛出，service()中捕获后统一生成报错返回报文。
			}
		} else if (mFlag.equals("0")) {	//新单取消
			cLogger.info("新单取消...");
		} else {
			throw new MidplatException("确认/取消标志有误！" + mFlag);
		}
		
		cLogger.info("Out ConfirmOrCancel.insertTransLog()!");
		return mOutXmlDoc;
	}
	
    /**
     * 银保通交叉销售.
     * @param pGlobalInput
     * @param pContNo
     * @throws Exception
     */
    private void ybtMixSale(GlobalInput pGlobalInput, String pContNo) throws Exception {
        cLogger.info("Into AIO_NewCont.ybtMixSale()!");
        GlobalInput mGlobalInput = pGlobalInput;
        String mAgentCom = mGlobalInput.AgentCom;
        String mContNo = pContNo;
        String mSQL;
        //根据agentcom判断是否是银保通交叉销售网点.如果此中介机构不在交叉定义表中,查询上级中介机构
        //直到上级中介机构为空或在交叉销售定义表中查到记录为止
        while (mAgentCom != null && !"".equals(mAgentCom)) {
            mSQL = "select 1 from LOMixBKAttribute where bandcode = '" + mAgentCom + "' and state = '1' with ur";
            if ("1".equals(new ExeSQL().getOneValue(mSQL))) {
                ybtMixSaleUpdate(mAgentCom, mContNo);
                break;
            } else {
                mSQL = "select upagentcom from lacom where agentcom = '" + mAgentCom + "' with ur";
                mAgentCom = new ExeSQL().getOneValue(mSQL);
            }
        }
        cLogger.info("Out AIO_NewCont.ybtMixSale()!"); 
    }
    
    /**
     * 更新lccont中标识银保通交叉销售的几个字段.
     * @param mAgentCom
     * @param mContNo
     * @throws ParseException
     * @throws MidplatException
     */
    private void ybtMixSaleUpdate(String mAgentCom, String mContNo) throws ParseException, MidplatException {
        cLogger.info("Into AIO_NewCont.ybtMixSaleUpdate()!");
        
        LOMixBKAttributeDB mLOMixBKAttributeDB = new LOMixBKAttributeDB();
        mLOMixBKAttributeDB.setBandCode(mAgentCom); //此字段存储的是中介机构代码
        LOMixBKAttributeSet mLOMixBKAttributeSet = mLOMixBKAttributeDB.query();
        
        if (mLOMixBKAttributeSet.size() > 0) { //此网点为交叉销售网点
            LOMixBKAttributeSchema mLOMixBKAttributeSchema = mLOMixBKAttributeSet.get(1);
            Date mNowDate = new Date();
            SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date mInvalidDate = DateFormat.parse(mLOMixBKAttributeSchema.getInvalidate());
            if (!mNowDate.after(mInvalidDate)) {
                cLogger.debug("交叉网点在有效期内,进行银保通交叉销售的更新.");
                LCContDB mLCContDB = new LCContDB();
                mLCContDB.setContNo(mContNo);
                mLCContDB.setAppFlag("1");
                if (!mLCContDB.getInfo()) {
                    throw new MidplatException("未查询到保单信息!");
                }
                mLCContDB.setCrs_BussType(mLOMixBKAttributeSchema.getCrs_BussType());
                mLCContDB.setCrs_SaleChnl(mLOMixBKAttributeSchema.getCrs_SaleChnl());
                mLCContDB.setGrpAgentCode(mLOMixBKAttributeSchema.getGrpAgentCode());
                mLCContDB.setGrpAgentCom(mLOMixBKAttributeSchema.getGrpAgentCom());
                mLCContDB.setGrpAgentIDNo(mLOMixBKAttributeSchema.getGrpAgentIDNo());
                mLCContDB.setGrpAgentName(mLOMixBKAttributeSchema.getGrpAgentName());
                if(!mLCContDB.update()){
                    throw new MidplatException("更新保单信息失败!");
                }
            }
        }
        cLogger.info("Out AIO_NewCont.ybtMixSaleUpdate()!");
    }
    
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "F:/working/picch/std/testXml/ConfirmOrCancel/std_00_in.xml";
		String mOutFile = "F:/working/picch/std/testXml/ConfirmOrCancel/std_00_out.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);
		
		Document mOutXmlDoc = new ConfirmOrCancel(null).service(mInXmlDoc);
		
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
