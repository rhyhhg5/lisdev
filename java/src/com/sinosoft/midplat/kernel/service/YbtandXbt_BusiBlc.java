package com.sinosoft.midplat.kernel.service;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.finfee.YBTPayConf;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKBalanceDetailSchema;
import com.sinosoft.lis.vschema.LKBalanceDetailSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.balance.DetailBalanceBL;
import com.sinosoft.midplat.kernel.service.balance.GetherBalance;
import com.sinosoft.midplat.kernel.service.balance.YbtandXbt_KernelBlcBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YbtandXbt_BusiBlc extends ServiceImpl {
	private String cBalanceNum = null; // 对账次数
	// 作为全局变量,存储对账明细的时候需要判断管理机构
	String mManageCom = null;
	private LKBalanceDetailSet mLKBalanceDetailSet_x = new LKBalanceDetailSet();

	public YbtandXbt_BusiBlc(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into YbtandXbt_BusiBlc.service()...");

		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;

		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		// 获取对账次数，设置cBalanceNum
		String mSQL = "select count(1)+1 from LKTransStatus where 1=1 "
				+ "and BankCode='" + mBaseInfo.getChildText(BankCode) + "' "
				+ "and BankBranch='" + mBaseInfo.getChildText(ZoneNo) + "' "
				+ "and BankNode='" + mBaseInfo.getChildText(BrNo) + "' "
				+ "and FuncFlag='" + mBaseInfo.getChildText(FunctionFlag)
				+ "' " + "and TransDate='"
				+ DateUtil.date8to10(mBaseInfo.getChildText(BankDate)) + "' "
				+ "with ur";
		cBalanceNum = new ExeSQL().getOneValue(mSQL);
		cLogger.debug("对账次数：BalanceNum = " + cBalanceNum);

		/**
		 * 同一天、同一网点前置机自动生成的流水号相同， 为防止多次重复对账时交易流水号重复， 在此重新设置流水号：TransrNo + "." +
		 * BalanceNum
		 */
		mBaseInfo.getChild(TransrNo).setText(
				mBaseInfo.getChildText(TransrNo) + "." + cBalanceNum);

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mSQL = "select ManageCom from LKCodeMapping where Remark5='1' "
					+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
					+ "and ZoneNo='" + mLKTransStatusDB.getBankBranch() + "' "
					+ "and BankNode='" + mLKTransStatusDB.getBankNode() + "' "
					+ "with ur";
			mManageCom = new ExeSQL().getOneValue(mSQL);
			if ((null == mManageCom) || mManageCom.equals("")) {
				throw new MidplatException("未查到该对帐网点！");
			}

			// 处理前置机传过来的报错信息(扫描超时等)
			String tErrorStr = mTranData.getChildText(Error);
			if (null != tErrorStr) {
				throw new MidplatException(tErrorStr);
			}

			// 保存对账明细
			LKBalanceDetailSet mLKBalanceDetailSet = saveDetails(pInXmlDoc);

			/**
			 * 为防止多次重复对账后日志表信息混乱，每次对账前清理一次对账信息相关字段
			 */
			mSQL = "update lktransstatus set state_code=null, rbankvsmp=null, desbankvsmp=null, "
					+ "rmpvskernel=null, desmpvskernel=null, resultbalance=null, desbalance=null "
					+ "where transdate='"
					+ mLKTransStatusDB.getTransDate()
					+ "' "
					+ "and bankcode='"
					+ mLKTransStatusDB.getBankCode()
					+ "' "
					+ "and ManageCom like '"
					+ mManageCom
					+ "%' "
					+ "and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
					+ "'"
					+ mLKTransStatusDB.getBankCode()
					+ "' "
					+ "and ManageCom <> '"
					+ mManageCom
					+ "' "
					+ "and ManageCom like '"
					+ mManageCom
					+ "%' "
					+ "and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) "
					+ "with ur";
			new ExeSQL().execUpdateSQL(mSQL);

			/*******************************************************************
			 * ***** 在此增加续期缴费对账 ***** add by wz 2012-04-11
			 ******************************************************************/
			if (mLKBalanceDetailSet_x != null && mLKBalanceDetailSet_x.size() > 0) {

				System.out.println("********************="+mLKBalanceDetailSet_x.get(1).getCardNo());
				NXS_RenewalPayBusiblc tNXS_RenewalPayBusiblc = new NXS_RenewalPayBusiblc(
						mManageCom,mLKTransStatusDB, mLKBalanceDetailSet_x);
				tNXS_RenewalPayBusiblc.deal();

			}

			// 与核心对账
			YbtandXbt_KernelBlcBL mYbtandXbt_KernelBlcBL = new YbtandXbt_KernelBlcBL(
					mLKTransStatusDB.getBankCode(), mLKTransStatusDB
							.getTransDate(), mManageCom);
			mYbtandXbt_KernelBlcBL.deal();
			//清理当日只做试算的保单数据 
			mYbtandXbt_KernelBlcBL.dealNoSignDate();

			// 与银行明细对账
			DetailBalanceBL mDetailBalanceBL = new DetailBalanceBL(
					mLKTransStatusDB.getBankCode(), mLKTransStatusDB
							.getTransDate(), mManageCom, mLKBalanceDetailSet);
			mDetailBalanceBL.deal();

			// 与银行汇总对账
			GetherBalance mGetherBalance = new GetherBalance(mLKTransStatusDB
					.getBankCode(), mLKTransStatusDB.getTransDate(), mManageCom);
			mGetherBalance.deal();

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "对账成功！");

			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；2-交易失败，返回
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；2-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setManageCom(mManageCom);
			mLKTransStatusDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out YbtandXbt_BusiBlc.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into YbtandXbt_BusiBlc.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCur10Date();
		String mCurrentTime = DateUtil.getCur8Time();

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		//大连农信社对账交易流水号超出35个字符，在此做处理 add by 高金富 20170808
		if("58".equals(mBaseInfo.getChildText(BankCode))){
			mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo).replaceAll("_000017070200003", ""));
		}else{
			mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		}
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out YbtandXbt_BusiBlc.insertTransLog()!");
		return mLKTransStatusDB;
	}

	/**
	 * 保存对账明细，返回保存的明细数据(LKBalanceDetailSet)
	 */
	private LKBalanceDetailSet saveDetails(Document pXmlDoc) throws Exception {
		cLogger.info("Into YbtandXbt_BusiBlc.saveDetails()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mChkDetails = mTranData.getChild(ChkDetails);

		String mCurDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurTime = DateUtil.getCurDate("HH:mm:ss");

		List mChkDetailList = mChkDetails.getChildren(ChkDetail);
		LKBalanceDetailSet mLKBalanceDetailSet = new LKBalanceDetailSet();
		for (int i = 0; i < mChkDetailList.size(); i++) {
			Element tChkDetail = (Element) mChkDetailList.get(i);
			LKBalanceDetailSchema tLKBalanceDetailSchema = new LKBalanceDetailSchema();

			//大连农信社对账交易流水号超出35个字符，在此做处理 add by gaojinfu 20170808
			if("58".equals(mBaseInfo.getChildText(BankCode))){
				tLKBalanceDetailSchema.setBalanceNo(mBaseInfo
						.getChildText(TransrNo).replaceAll("_000017070200003", ""));
				tLKBalanceDetailSchema.setTransrNo(tChkDetail
						.getChildText(TransrNo).replaceAll("_000017070200003", ""));
			}else{
				tLKBalanceDetailSchema.setBalanceNo(mBaseInfo
						.getChildText(TransrNo));
				tLKBalanceDetailSchema.setTransrNo(tChkDetail
						.getChildText(TransrNo));
			}
			tLKBalanceDetailSchema
					.setBankCode(mBaseInfo.getChildText(BankCode));
			tLKBalanceDetailSchema.setBankZoneCode(tChkDetail
					.getChildText(BankZoneCode));
			tLKBalanceDetailSchema.setBrNo(tChkDetail.getChildText(BrNo));
			tLKBalanceDetailSchema.setBalanceNum(cBalanceNum);
			tLKBalanceDetailSchema.setTellerNo(tChkDetail
					.getChildText(TellerNo));
			tLKBalanceDetailSchema.setFuncFlag(tChkDetail
					.getChildText(FuncFlag));
			// 山东农信社设传的是单证印刷号,不是保单号,需要转换
			if ("8637".equals(mManageCom.substring(0, 4))
					&& mBaseInfo.getChildText(BankCode).equals("45")) {
				ExeSQL mExeSQL = new ExeSQL();
				String mSQL = "select polno from lktransstatus where bankcode = '"
						+ mBaseInfo.getChildText(BankCode)
						+ "' "
						+ "and proposalno = 'HM"
						+ tChkDetail.getChildText(CardNo)
						+ "' "
						+ "and funcflag = '01' and rcode = '1' and status = '1' with ur";
				SSRS tSSRS = mExeSQL.execSQL(mSQL);
				if (tSSRS.getMaxRow() < 1) {
					// 在日志表里未查到保单号时
					tLKBalanceDetailSchema.setCardNo("HM"
							+ tChkDetail.getChildText(CardNo));
					tLKBalanceDetailSchema.setFuncFlag("99");// 标识在日志表中查询不到的情况,需要人工处理
				} else {
					tLKBalanceDetailSchema.setCardNo(tSSRS.GetText(1, 1));
				}
			} else if("58".equals(mBaseInfo.getChildText(BankCode))) {//大连农信社系统迁移后不传保单号，在此做转换 add by gaojinfu 20170808
				ExeSQL mExeSQL = new ExeSQL();
				String mSQL = "select polno from lktransstatus where bankcode = '" + mBaseInfo.getChildText(BankCode) 
						+ "'" + "and prtno = '" + tChkDetail.getChildText(CardNo) +"' " + "and funcflag = '01' and rcode = '1' and status = '1' with ur" ;
				tLKBalanceDetailSchema.setCardNo(mExeSQL.getOneValue(mSQL));
			} else {
				tLKBalanceDetailSchema.setCardNo(tChkDetail
						.getChildText(CardNo));
			}

			tLKBalanceDetailSchema.setTranDate(tChkDetail
					.getChildText(TranDate));
			tLKBalanceDetailSchema.setTranAmnt(tChkDetail
					.getChildText(TranAmnt));
			tLKBalanceDetailSchema.setConfirmFlag(tChkDetail
					.getChildText(ConfirmFlag));
			tLKBalanceDetailSchema.setTemp1(mBaseInfo.getChildText(ZoneNo));
			tLKBalanceDetailSchema.setTemp2(mBaseInfo.getChildText(BrNo));
			tLKBalanceDetailSchema.setMakeDate(mCurDate);
			tLKBalanceDetailSchema.setMakeTime(mCurTime);
			tLKBalanceDetailSchema.setModifyDate(mCurDate);
			tLKBalanceDetailSchema.setModifyTime(mCurTime);

			if ("X".equals(tChkDetail.getChildText("Type"))) {
				/**
				 * **** 四川农信社将有续期缴费业务，在此判断是否为续期，如果为续期，将对账数据放在
				 * mLKBalanceDetailSet_x 中 *****
				 */
				mLKBalanceDetailSet_x.add(tLKBalanceDetailSchema);
			} else {
				mLKBalanceDetailSet.add(tLKBalanceDetailSchema);
			}
		}

		/**
		 * 将银行发过来的对账明细存储到对账明细表(LKBalanceDetail)中
		 */
		cLogger.info("银行提交的对账明细总数(LKBalanceDetailSet)为："
				+ mLKBalanceDetailSet.size());
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLKBalanceDetailSet, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("保存对账明细失败！");
		}

		cLogger.info("Out YbtandXbt_BusiBlc.saveDetails()!");
		return mLKBalanceDetailSet;
	}
}
