package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.VData;

public class RePrint_AutoWriteOff extends ServiceImpl {
	public RePrint_AutoWriteOff(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into RePrint_AutoWriteOff.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

			LKTransStatusDB tLKTransStatusDB = new LKTransStatusDB();
			LKTransStatusSet tLKTransStatusSet = new LKTransStatusSet();
			LKTransStatusSchema tLKTransStatusSchema = new LKTransStatusSchema();
			tLKTransStatusDB.setTransNo(mLCCont.getChildText("OldTransrNo"));
			tLKTransStatusSet = tLKTransStatusDB.query();
			tLKTransStatusSchema = tLKTransStatusSet.get(1);
			if (tLKTransStatusSchema.getFuncFlag().equals("")
					&& tLKTransStatusSchema.getBankCode().equals("")) {
				throw new MidplatException("相关号码不能为空！");
			}

			updateLccontProNo(tLKTransStatusSchema.getbak1(),tLKTransStatusSchema.getPolNo());

			card2To5(tLKTransStatusSchema.getbak1(),mGlobalInput);
			
			card5To8(tLKTransStatusSchema.getProposalNo(),mGlobalInput);
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
			
			mLKTransStatusDB.setTransDate(mCurrentDate);
			mLKTransStatusDB.setTransTime(mCurrentTime);
			mLKTransStatusDB.setMakeDate(mCurrentDate);
			mLKTransStatusDB.setMakeTime(mCurrentTime);
			mLKTransStatusDB.setModifyDate(mCurrentDate);
			mLKTransStatusDB.setModifyTime(mCurrentTime);
			mLKTransStatusDB.setFuncFlag("02");
			mLKTransStatusDB.setRCode("1");
			mLKTransStatusDB.setTransStatus("1");
			mLKTransStatusDB.setPolNo(tLKTransStatusSchema.getPolNo());
			mLKTransStatusDB.setProposalNo(tLKTransStatusSchema.getProposalNo());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "重打冲正交易成功!");
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);

			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setDescr(ex.getMessage());
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setRCode("0");
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out RePrint_AutoWriteOff.service()!");
		return mOutXmlDoc;
	}

	/**
	 * 单证回收
	 */
	private void card2To5(String pCardNo, GlobalInput pGlobalInput) throws MidplatException {
		cLogger.info("Into RePrint_AutoWriteOff.card2To5()...");

		String mCertifyCode = "HB07/07B";
		cLogger.debug("单证类型码(CertifyCode)：" + mCertifyCode);
		cLogger.debug("待处理的单证号：" + pCardNo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(mCertifyCode);
		mLZCardDB.setStartNo(pCardNo);
		mLZCardDB.setEndNo(pCardNo);
		mLZCardDB.setStateFlag("2");	// 5-核销，2-作废
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError()
				|| (null==mLZCardSet)
				|| (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到对应已核销的单证信息！");
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);

		mLZCardSchema.setStateFlag("5");	// 5-核销，2-作废, 0-未生效
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(mCurrentDate);
		mLZCardSchema.setMakeTime(mCurrentTime);

		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mCertifyCode);
		mLZCardTrackSchema.setSubCode("0");
		mLZCardTrackSchema.setRiskCode("0");
		mLZCardTrackSchema.setRiskVersion("0");
		mLZCardTrackSchema.setStartNo(pCardNo);
		mLZCardTrackSchema.setEndNo(pCardNo);
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(1);
		mLZCardTrackSchema.setPrem(0);
		mLZCardTrackSchema.setAmnt(0);
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(pGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurrentDate);
		mLZCardTrackSchema.setMakeTime(mCurrentTime);
		mLZCardTrackSchema.setModifyDate(mCurrentDate);
		mLZCardTrackSchema.setModifyTime(mCurrentTime);

		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证作废到核销处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交单证作废到核销处理数据失败！");
		}
		cLogger.info("单证作废到核销处理！");

		cLogger.info("Out RePrint_AutoWriteOff.card2To5()!");
	}

	/**
	 * 单证回收
	 */
	private void card5To8(String pCardNo, GlobalInput pGlobalInput) throws MidplatException {
		cLogger.info("Into AutoWriteOff.certTakeBack()...");

		String mCertifyCode = "HB07/07B";
		cLogger.debug("单证类型码(CertifyCode)：" + mCertifyCode);
		cLogger.debug("待处理的单证号：" + pCardNo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(mCertifyCode);
		mLZCardDB.setStartNo(pCardNo);
		mLZCardDB.setEndNo(pCardNo);
		mLZCardDB.setStateFlag("5");	// 5-核销，2-作废
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError()
				|| (null==mLZCardSet)
				|| (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到对应已核销的单证信息！");
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);

		mLZCardSchema.setStateFlag("8");	// 5-核销，2-作废
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(mCurrentDate);
		mLZCardSchema.setMakeTime(mCurrentTime);

		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mCertifyCode);
		mLZCardTrackSchema.setSubCode("0");
		mLZCardTrackSchema.setRiskCode("0");
		mLZCardTrackSchema.setRiskVersion("0");
		mLZCardTrackSchema.setStartNo(pCardNo);
		mLZCardTrackSchema.setEndNo(pCardNo);
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(1);
		mLZCardTrackSchema.setPrem(0);
		mLZCardTrackSchema.setAmnt(0);
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(pGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurrentDate);
		mLZCardTrackSchema.setMakeTime(mCurrentTime);
		mLZCardTrackSchema.setModifyDate(mCurrentDate);
		mLZCardTrackSchema.setModifyTime(mCurrentTime);

		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证作废处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证作废成功！");

		cLogger.info("Out AutoWriteOff.card5To0()!");
	}


	private void updateLccontProNo(String pCardNo,String ContNo) throws MidplatException {
		cLogger.info("Into RePrint_AutoWriteOff.updateLccontProNo()...");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(ContNo);
		LCContSet tLCContSet = tLCContDB.query();
		if (tLCContDB.mErrors.needDealError()) {
			cLogger.error(tLCContDB.mErrors.getFirstError());
			throw new MidplatException("查询原保单数据失败！");
		}
		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema = tLCContSet.get(1);

		tLCContDB.setSchema(mLCContSchema);
		tLCContDB.setProposalContNo(pCardNo);
		tLCContDB.setPrintCount(mLCContSchema.getPrintCount() - 1);
		tLCContDB.setMakeDate(mCurrentDate);
		tLCContDB.setMakeTime(mCurrentTime);
		if (!tLCContDB.update()) {
			cLogger.error(tLCContDB.mErrors.getFirstError());
			throw new MidplatException("更新重打冲正保单表中的打印次数和单证号失败！");
		}

		cLogger.info("Out RePrint_AutoWriteOff.updateLccontProNo()!");
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into AIO_RePrint.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
//		mLKTransStatusDB.setRCode("0");
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out AIO_RePrint.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
