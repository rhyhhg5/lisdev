package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.yinbaotongbank.YBTReturnSInterface;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BatResponse extends ServiceImpl {
    public BatResponse(Element pThisBusiConf) {
        super(pThisBusiConf);
    }

    public Document service(Document pInXmlDoc) {
        cLogger.info("Into BatResponse.service()...");
        Document mOutXmlDoc = null;
        LKTransStatusDB mLKTransStatusDB = null;
        GlobalInput mGlobalInput = null;

        try {
            mLKTransStatusDB = insertTransLog(pInXmlDoc);

            mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
                    .getBankCode(), mLKTransStatusDB.getBankBranch(),
                    mLKTransStatusDB.getBankNode());

            
            /**
             * 调用核心批量回盘接口
             */
            Element mTranData = (Element) pInXmlDoc.getRootElement().getChild(
                    TranData).clone();
            Document mDoc = new Document(mTranData);
            String mDealType = pInXmlDoc.getRootElement().getChild(BaseInfo)
                    .getChildText("DealType");
//            List detailList = mDoc.getRootElement().getChildren(Detail);
//            for (int i = 0; i < detailList.size(); i++) {
//                Element tDealType = new Element("DealType");
//                tDealType.setText(mDealType);
//                ((Element) detailList.get(i)).addContent(tDealType);
//            }
            List detailList = null;
            //建行新接口 不传paycode 故通过sql查询出paycode
            if(mLKTransStatusDB.getBankCode().equals("03")){
            	Element tTranData = pInXmlDoc.getRootElement();
    			Element mBaseInfo = tTranData.getChild(BaseInfo);
				String mBankCode = mBaseInfo.getChildText(BankCode);
				String mZoneNo = mBaseInfo.getChildText(ZoneNo);
				String mBrNo = mBaseInfo.getChildText(BrNo);
				String mFileName = mBaseInfo.getChildText("FileName");
				String mSQL = "select edorno from lktransstatus where bankcode = '"
						+ mBankCode
						+ "' and bankbranch = '"
						+ mZoneNo
						+ "' and banknode = '"
						+ mBrNo
						+ "' and funcflag = '31' and bankacc = '"
						+ mFileName
						+ "' order by transtime desc with ur";
				SSRS mSSRS = new ExeSQL().execSQL(mSQL);
				if (mSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的日志信息!");
				}
				String mEdorNo = mSSRS.GetText(1, 1);

            	detailList = mDoc.getRootElement().getChildren(Detail);
                 for (int i = 0; i < detailList.size(); i++) {
                     Element tDealType = new Element("DealType");
                     tDealType.setText(mDealType);
                     ((Element) detailList.get(i)).addContent(tDealType);
                     
                     Element mAccNo = ((Element) detailList.get(i)).getChild("AccNo");
                     Element mPayCode = ((Element) detailList.get(i)).getChild("PayCode");
                     Element mSerialNo = ((Element) detailList.get(i)).getChild("SerialNo");
                     mSerialNo.setText(mEdorNo);
                     cLogger.info("批次号：" + mSerialNo.getTextTrim());
                     String cx_sql = "select PayCode from LYSendToBank where SerialNo = '"+ mEdorNo +"' and Accno='"+ mAccNo.getText() +"' with ur";
                     mPayCode.setText(new ExeSQL().getOneValue(cx_sql));
                     cLogger.info("代收付号：" + mPayCode.getText());
                 }
            }else {
            	detailList = mDoc.getRootElement().getChildren(Detail);
                for (int i = 0; i < detailList.size(); i++) {
                    Element tDealType = new Element("DealType");
                    tDealType.setText(mDealType);
                    ((Element) detailList.get(i)).addContent(tDealType);
                }
            }
            
            // 存储核心接口报文
//            FileOutputStream mFos = new FileOutputStream(cThisBusiConf
//                    .getChildText(path)
//                    + "RES_" + mLKTransStatusDB.getBankAcc() + ".xml");
//            JdomUtil.output(mDoc, mFos);
//            mFos.close();

            // 另起线程把数据交给核心处理
            new BatReturnThread(mDoc).start();

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
            mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
            mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        } catch (Exception ex) {
            cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

            if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
                mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
                mLKTransStatusDB.setDescr(ex.getMessage());
            }

            mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
        }

        if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
            if (null != mGlobalInput) {
                mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
            }
            mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
            if (!mLKTransStatusDB.update()) {
                cLogger.error("更新日志信息失败！"
                        + mLKTransStatusDB.mErrors.getFirstError());
            }
        }

        cLogger.info("Out BatResponse.service()!");
        return mOutXmlDoc;
    }

    public LKTransStatusDB insertTransLog(Document pXmlDoc)
            throws MidplatException {
        cLogger.info("Into BatResponse.insertTransLog()...");

        Element mTranData = pXmlDoc.getRootElement();
        Element mBaseInfo = mTranData.getChild(BaseInfo);

        String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
        String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

        LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
        mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
        mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
        mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
        mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
        mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
        mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
        mLKTransStatusDB.setBankAcc(mBaseInfo.getChildText("FileName")); // 借用做文件名称
        mLKTransStatusDB.setTempFeeNo(mBaseInfo.getChildText("DealType")); // 借用做业务类型S/F
        mLKTransStatusDB.setTransDate(mCurrentDate);
        mLKTransStatusDB.setTransTime(mCurrentTime);
        mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
        mLKTransStatusDB.setMakeDate(mCurrentDate);
        mLKTransStatusDB.setMakeTime(mCurrentTime);
        mLKTransStatusDB.setModifyDate(mCurrentDate);
        mLKTransStatusDB.setModifyTime(mCurrentTime);

        if (!mLKTransStatusDB.insert()) {
            cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
            throw new MidplatException("插入日志失败！");
        }

        cLogger.info("Out BatResponse.insertTransLog()!");
        return mLKTransStatusDB;
    }

    private class BatReturnThread extends Thread {
        private final Logger cLogger = Logger.getLogger(BatReturnThread.class);

        private final Document cInXmlDoc;

        private BatReturnThread(Document pInXmlDoc) {
            cInXmlDoc = pInXmlDoc;
        }

        public void run() {
            cLogger.info("Into BatReturnThread.run()...");

            try {
                YBTReturnSInterface mYBTReturnSInterface = new YBTReturnSInterface();
                DOMOutputter outputter = new DOMOutputter();
               

                org.w3c.dom.Document w3CDoc = outputter.output(cInXmlDoc);
                mYBTReturnSInterface.submitData(w3CDoc);
            } catch (Exception ex) {
                cLogger.error("调用核心返盘接口失败!", ex);
            }

            cLogger.info("Out BatReturnThread.run()!");
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("程序开始…");

        String mInFile = "E:/BAT901.xml";
        String mOutFile = "E:/BAT901out.xml";

        FileInputStream mFis = new FileInputStream(mInFile);
        InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
        Document mInXmlDoc = new SAXBuilder().build(mIsr);

        Document mOutXmlDoc = new BatResponse(null).service(mInXmlDoc);

        XMLOutputter mXMLOutputter = new XMLOutputter();
        mXMLOutputter.setEncoding("GBK");
        mXMLOutputter.setTrimText(true);
        mXMLOutputter.setIndent("   ");
        mXMLOutputter.setNewlines(true);
        mXMLOutputter.output(mOutXmlDoc, System.out);
        mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

        System.out.println("成功结束！");
    }
}
