package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class SDNXS_NewContConfirm extends SimpService {
	public SDNXS_NewContConfirm(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into SDNXS_NewContConfirm.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		//山东农信社新单确认FunctionFlag不须置回00
		Element mTranData = pInXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			ExeSQL es = new ExeSQL();
			//add by zengzm 2017-01-12 防止重复签单
			String tSQL = "select polno from lktransstatus where funcflag='01' and status='1' and prtno='"+mLCCont.getChildText(PrtNo)+"' and makedate = current date with ur";
			String tContNo = es.getOneValue(tSQL);
			if(tContNo != null && !"".equals(tContNo)){
				throw new MidplatException("该保单已缴费成功，保单号为："+tContNo+"，请勿重复签单!");
			}
							
			 tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			//判断之前是否做过冲正交易(山东农信社冲正交易什么也不做)
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setAppFlag("1");
			tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
			LCContSet tLCContSet = tLCContDB.query();
			
			if(tLCContSet.size()>= 1){
				LCContSchema tLCContSchema = new LCContSchema();
				tLCContSchema = tLCContSet.get(1);
				//组织返回报文
				YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
				mOutXmlDoc = tYbtContQueryBL.deal();
				
				//将bak4字段值置为0,当bak4字段值为1时,标志余额不足时引发的冲正操作,做查询交易时返回无此数据.(实际冲正交易没有撤单)
				String mSQL = "select 1 from LKTransStatus where bankcode = '"+mLKTransStatusDB.getBankCode()+"' "
							+"and PrtNo = '"+mLKTransStatusDB.getPrtNo()+"' "
							+"and rcode = '1' and funcflag = '01' and status = '1' and bak4 = '1' "
							+"with ur";
//				ExeSQL mExeSQL = new ExeSQL();
				if("0".equals(es.getOneValue(mSQL))){
					throw new MidplatException("未查询到日志信息!");
				}
				
				mSQL = "update LKTransStatus set bak4 = '0' where rcode = '1' and "
					+"PrtNo = '"+mLKTransStatusDB.getPrtNo()+"' and "
					+"BankCode = '"+mLKTransStatusDB.getBankCode()+"' and "
					+"FuncFlag = '01' and bak4 = '1' and "
					+"TransDate = '"+DateUtil.getCurDate("yyyy-MM-dd")+"' and "
					+"BankNode = '"+mLKTransStatusDB.getBankNode()+"' and "
					+"BankOperator = '"+mLKTransStatusDB.getBankOperator()+"' and "
					+"BankBranch = '"+mLKTransStatusDB.getBankBranch()+"' ";
					
				if(!es.execUpdateSQL(mSQL)){
					throw new MidplatException("更新日志表信息失败!");
				}
				
				Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
				//更新相关日志
				String tCurTime = DateUtil.getCurDate("HH:mm:ss");
				mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
				mLKTransStatusDB.setProposalNo(tOutLCCont.getChildText(ProposalContNo));
				mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回			
				mLKTransStatusDB.setModifyTime(tCurTime);
				
				MMap tSubmitMMap = new MMap();
				tSubmitMMap.put(mLKTransStatusDB.getSchema(), "UPDATE");
				VData tSubmitVData = new VData();
				tSubmitVData.add(tSubmitMMap);
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tSubmitVData, "")) {
					cLogger.error(tPubSubmit.mErrors.getFirstError());
					throw new MidplatException("更新日志失败！");
				}
			}else{
//				财务收费
				YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtTempFeeBL.deal();

				//签单+单证核销+回执回销
				YbtLCContSignBL tYbtLCContSignBL = 
					new YbtLCContSignBL(mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtLCContSignBL.deal();
				
				//组织返回报文
				YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tYbtLCContSignBL.getContNo());
				mOutXmlDoc = tYbtContQueryBL.deal();
				
				Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);

				//查找套餐编码
				tSQL = "select riskcode from lktransstatus where funcflag = '01' "
					+"and prtno = '"+mLKTransStatusDB.getPrtNo()+"' "
					+"and rcode = '1' "
					+"with ur";
				String mRiskWrapCode = new ExeSQL().getOneValue(tSQL);//套餐编码
				String mCertifyCode = null;//单证编码
				String mProposalContNoNew = null;//信保通单证操作使用,需要去掉远单证号前两位
//				ExeSQL mExeSQL = new ExeSQL();
				tSQL = "select certifycode from lkcertifymapping where bankcode = '"
						+ mLKTransStatusDB.getBankCode()+"' "
						+ "and managecom = '"+ mGlobalInput.ManageCom.substring(0, 4)+"' "
						+ "and RiskWrapCode = '"+ mRiskWrapCode+"' "
						+ "with ur";
				SSRS tSSRS = es.execSQL(tSQL);
				
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的单证编码!");
				}
				mCertifyCode = tSSRS.GetText(1, 1);// 农信社单证编码需要从单证编码定义表中查询
				mProposalContNoNew = tOutLCCont.getChildText(ProposalContNo).substring(2, 12);//信保通单证操作需要去掉前两位
				if (!tOutLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
					new CardManage(mCertifyCode, mGlobalInput)
								.makeUsed(mProposalContNoNew);		
				}
				
				//更新相关日志
				
				String tCurTime = DateUtil.getCurDate("HH:mm:ss");
				mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
				mLKTransStatusDB.setProposalNo(tOutLCCont.getChildText(ProposalContNo));
				mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回			
				mLKTransStatusDB.setModifyTime(tCurTime);

				LKTransStatusDB tOldLKTransStatusDB = new LKTransStatusDB();
				tOldLKTransStatusDB.setBankCode(mLKTransStatusDB.getBankCode());
				tOldLKTransStatusDB.setBankBranch(mLKTransStatusDB.getBankBranch());
				tOldLKTransStatusDB.setBankNode(mLKTransStatusDB.getBankNode());
				tOldLKTransStatusDB.setProposalNo(mLKTransStatusDB.getProposalNo());
				tOldLKTransStatusDB.setFuncFlag("01");//一定要加上,否则会引起山东农信社将之前的确认记录status 置 1
				tOldLKTransStatusDB.setRCode("1");
				LKTransStatusSet ttLKTransStatusSet = tOldLKTransStatusDB.query();
				if ((null==ttLKTransStatusSet) || (ttLKTransStatusSet.size()<1)) {
					throw new MidplatException("查询原始日志信息失败！");
				}
				LKTransStatusSchema tLKTransStatusSchema = ttLKTransStatusSet.get(1);
				tLKTransStatusSchema.setPolNo(tOutLCCont.getChildText(ContNo));
				tLKTransStatusSchema.setTransAmnt(tOutLCCont.getChildText(Prem));
				tLKTransStatusSchema.setStatus("1");
				tLKTransStatusSchema.setModifyDate(mLKTransStatusDB.getModifyDate());
				tLKTransStatusSchema.setModifyTime(tCurTime);
				
				MMap tSubmitMMap = new MMap();
				tSubmitMMap.put(mLKTransStatusDB.getSchema(), "UPDATE");
				tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
				VData tSubmitVData = new VData();
				tSubmitVData.add(tSubmitMMap);
				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tSubmitVData, "")) {
					cLogger.error(tPubSubmit.mErrors.getFirstError());
					throw new MidplatException("更新日志失败！");
				}
			}
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			try {
				YbtSufUtil.clearData(mLKTransStatusDB.getPrtNo());
			} catch (Exception tBaseEx) {
				cLogger.error("删除新单数据失败！", tBaseEx);
			}
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				
				String ttDesr = ex.getMessage();
				try {
					if (null!=ttDesr && ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){}
				mLKTransStatusDB.setDescr(ttDesr);
				
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		cLogger.info("Out SDNXS_NewContConfirm.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into SDNXS_NewContConfirm.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTemp("1");	//确认/取消标识：0-取消；1-确认
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out SDNXS_NewContConfirm.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "d:/in.xml";
		String mOutFile = "d:/out.xml";
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new SDNXS_NewContConfirm(null).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
