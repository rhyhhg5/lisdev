package com.sinosoft.midplat.kernel.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ABC_QueryDetail extends ServiceImpl  {

	public ABC_QueryDetail(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_QueryDetail.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLKTransStatusDB.getPolNo());
			LCContSet tLCContSet = tLCContDB.query();
			if ((null==tLCContSet) || (tLCContSet.size()<1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			Element mTranData = pInXmlDoc.getRootElement();
			Element mBaseInfo = mTranData.getChild(BaseInfo);
			Element mLCCont = mTranData.getChild(LCCont);
			String mBankDate = mBaseInfo.getChildText("BankDate");
			String mContNo = mLCCont.getChildText("ContNo");
			String sql = "select date(cvalidate) + 15 days,cinvalidate from lccont where contno ='"+mContNo+"' and salechnl in ('03','04') and stateflag='1'";
			SSRS mssrs = new SSRS();
			mssrs = new ExeSQL().execSQL(sql);
			if(mssrs.MaxRow<=0){
				throw new MidplatException("请确认保单信息是否正确及有效!");
			}
			String cvalidate = mssrs.GetText(1, 1);
			String cinvalidate = mssrs.GetText(1, 2);
			cvalidate = cvalidate.replace("-", "");
			cinvalidate = cinvalidate.replace("-", "");
			String mBaoQuanFlag = "";
			if(Integer.parseInt(mBankDate) <= Integer.parseInt(cvalidate)){
				mBaoQuanFlag = "WT";
			}else{
				mBaoQuanFlag = "CT";
			}
			if(Integer.parseInt(mBankDate) > Integer.parseInt(cinvalidate)){
				mBaoQuanFlag = "MQ";
			}
//			mLCCont.addContent(mBaoQuanFlag);
//			Document mBqQueryXml = new ABC_BaoQuanQuery(cThisBusiConf).service(pInXmlDoc);
//			String accountValueStr = mBqQueryXml.getRootElement().getChild("EdorInfo").getChild("AttachmentContent").getChildText("SumPrem");
			String accountValueStr = getAccountValue( mContNo, mBaoQuanFlag);
			Element mAccountValue = new Element("AccountValue");
			mAccountValue.setText(accountValueStr);
			
			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();
			mOutXmlDoc.getRootElement().getChild("LCCont").addContent(mAccountValue);
			
			//增加需要返回节点内容
			String tSQL = "select salary from lcappnt where contno='" + mLKTransStatusDB.getPolNo() + "' with ur";
			Element mSalary =  new Element("Salary");
			mSalary.setText(new ExeSQL().getOneValue(tSQL));

			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			
			String mSQL = "select bak2,bak3,bak4 from lktransstatus where polno = '" + mLKTransStatusDB.getPolNo() + "' and bankcode = '03' and funcflag = '01' and descr is null with ur";
			SSRS ssrs= new ExeSQL().execSQL(mSQL);
			if(ssrs.MaxRow > 0){
				String pRckrNo = ssrs.GetText(1, 1);
				String pSaleName = ssrs.GetText(1, 2);
				String pSaleCertNo = ssrs.GetText(1, 3);
				Element mRckrNo = new Element("RckrNo").setText(pRckrNo);
				Element mSaleName = new Element("SaleName").setText(pSaleName);
				Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
			
				tOutLCCont.addContent(mRckrNo);
				tOutLCCont.addContent(mSaleName);
				tOutLCCont.addContent(mSaleCertNo);
			}

			tOutLCCont.getChild("LCAppnt").addContent(mSalary);
			
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_QueryDetail.service()!");
		return mOutXmlDoc;
	}

	private String getAccountValue(String mContNo, String mBaoQuanFlag) throws MidplatException {
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(mContNo);
		if (!mLCContDB.getInfo()) {
			throw new MidplatException("查询保单合同信息失败！");
		}
		LCPolDB pol_db = new LCPolDB();
		pol_db.setContNo(mContNo);
		LCPolSet polSet = pol_db.query();
		
		double mSumPrem = 0;
		String risk_main = "";
		String risk_fu = "";
		for (int i = 1; i <= polSet.size(); i++) {
			LCPolSchema mPol = polSet.get(i);
			mSumPrem += mPol.getPrem();
			if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
				risk_main = mPol.getRiskCode();
			}
			if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
				risk_fu = mPol.getRiskCode();
			}
		}
		String mAccountValue = "";
		if(mBaoQuanFlag.equals("MQ")){	//满期给付查询 add by zengzm 2016-12-14
			mAccountValue = "0.00";
		}
		if(mBaoQuanFlag.equals("WT")){	//犹豫期保费
			mAccountValue = mSumPrem + "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String now = sdf.format(new Date());
		if(mBaoQuanFlag.equals("CT")){	//退保保费即解约
			PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
			double pSumPrem = mPedorYBTBudgetBL.getZTmoney(mContNo,now);
			mAccountValue = pSumPrem+"";
		}
		return mAccountValue;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_QueryDetail.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_QueryDetail.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
