/**
 * 获取保单详情查询
 * 根据日期获取批量包个数并返回
 */

package com.sinosoft.midplat.kernel.service;

import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_NewCall extends ServiceImpl {

	public CCB_NewCall(Element pThisConf) {
		super(pThisConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_NewCall.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element tTranData = new Element("TranBody");
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc); 

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

			Element mTranData = pInXmlDoc.getRootElement();
			Element mLccont = mTranData.getChild("LCCont");
			String mDate = mLccont.getChildTextTrim("Enqr_Dt");
			cLogger.info("建行方发起查询时间::"+mDate);
			
			
			//1.查询7天之后续期缴费
			String mSQL1 = " SELECT count(contno) FROM lccont c WHERE c.paytodate = " 
				                +" (to_date('" +mDate+"','YYYY-MM-DD') + 8 days)"
							    +" AND EXISTS "
							    +" (SELECT 1 FROM lktransstatus lk WHERE"
							    +"  lk.bankcode ='03' "
							    +" AND lk.funcflag ='01'"
							    +" AND status ='1' "
							    +" AND lk.polno = c.contno )";
			
			//2.提醒退保金额到账
			String mSQL2 = "select count(c.contno) from lpedoritem a,ljaget b,lbcont c"
							+" where a.edorno=b.otherno "
							+" and a.contno=c.contno  and  a.edorno=c.edorno "
							 +" and c.conttype='1' and b.salechnl in ('04','13') "
							+" and a.edortype in ('CT','WT','XT')"
							+"  and a.edorstate='0' and b.confdate ='"+mDate+"'"
							+"  and exists("
							+"  select 1 from lktransstatus s where s.bankcode ='03' and s.funcflag ='01' and s.status='1' and s.polno = c.contno"
							+" )with ur";
			
			//3.红利派发提醒
			String   mSQL3 = "select count(a.contno )  from lobonuspol a where " 
								+" bonusmakedate= '2015-08-21' "   					
								+" and exists ("
                                +" select 1 from lktransstatus s where s.funcflag ='01' " 
						        +" and s.bankcode ='03' and s.polno = a.contno and s.status='1'" 
							    +" ) with ur"; 
			
            //4查询保单到期失效
			String mSQL4 = " SELECT count(contno) FROM lccont c WHERE c.paytodate = " 
				                +" (to_date('" +mDate+"','YYYY-MM-DD') + 8 days)"
							    +" AND EXISTS "
							    +" (SELECT 1 FROM lktransstatus lk WHERE"
							    +"  lk.bankcode ='03' "
							    +" AND lk.funcflag ='01'"
							    +" AND status ='1' "
							    +" AND lk.polno = c.contno )";
			
		     long time_start = System.currentTimeMillis();
			String mCount1 = new ExeSQL().getOneValue(mSQL1); //查询7天之后续期缴费
			String mCount2 = new ExeSQL().getOneValue(mSQL2); //提醒退保金额到账
			String mCount3 = new ExeSQL().getOneValue(mSQL3); //红利派发提醒
			String mCount4 = new ExeSQL().getOneValue(mSQL4); //查询保单到期失效
			long time_end = System.currentTimeMillis();
			cLogger.info("执行查询耗时::"+(time_end - time_start)/1000);
			cLogger.info("缴费查数::"+mCount1);
			cLogger.info("退保金额到账::"+mCount2);
			cLogger.info("红利派发提醒::"+mCount3);
			cLogger.info("查询保单到期失效::"+mCount4);
			int mNum = Integer.parseInt(mCount1)+Integer.parseInt(mCount2)+Integer.parseInt(mCount3)+Integer.parseInt(mCount4);
			int mBtchBagNum;
			if (mNum % 100 == 0) {
				mBtchBagNum = mNum / 500;
			} else {
				mBtchBagNum = mNum / 500 + 1;
			}
			cLogger.info("-----------------总条数" + mNum);
			cLogger.info("------------------------批量包个数" + mBtchBagNum);
			Element CallCount = new Element("CallCount");
			CallCount.setText(mBtchBagNum+"");
			tTranData.addContent(CallCount);
			

			

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");

//			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
//			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (MidplatException ex) {
			cLogger.error("建行续期,退保,到期失效提醒查询(CCB_NewCall)交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());

		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out 建行续期,退保,到期失效提醒查询.service()!");
		mOutXmlDoc.getRootElement().addContent(tTranData);
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into CCB_NewCall.service.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_NewCall.service.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws IOException {
		CCB_NewCall c = new CCB_NewCall(null);
		c.service(null);
	
	}

}
