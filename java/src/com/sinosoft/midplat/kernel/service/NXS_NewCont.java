package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.IdNoCheck;
import com.sinosoft.midplat.kernel.service.check.YBTRiskCheck;
import com.sinosoft.midplat.kernel.service.newCont.NxsContBL;
import com.sinosoft.midplat.kernel.service.newCont.NxsContInsuredIntlBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtAutoCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtContBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtInsuredProposalBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class NXS_NewCont extends ServiceImpl {
	public NXS_NewCont(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into NXS_NewContInput.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false; // 删除数据标志

		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		try {
			// 江门邮政 单证号 = 投保单印刷号 add by zengzm 2015-10-29 modify by GaoJinfu
			// 20171202 添加广东邮政77
			if (mBaseInfo.getChildText(BankCode).equals("65")
					|| "77".equals(mBaseInfo.getChildText(BankCode))) {

				cLogger.info("江门邮政处理满期数据开始...");// add by GaoJinfu 20171108
				List insList = mLCCont.getChild("LCInsureds").getChildren(
						"LCInsured");
				String insIdNo = "";
				for (int i = 0; i < insList.size(); i++) {
					Element ins = (Element) insList.get(i);
					insIdNo = ins.getChildText("IDNo");
					if (!dealMQ(insIdNo)) {
						throw new MidplatException("处理满期数据失败");
					}
				}
				cLogger.info("江门邮政江门邮政处理满期数据结束...");

				cLogger.info("江门邮政动态生成保单印刷号和单证号开始...");
				String wyPrtno = "";
				String mProposalContNo = "";
				String mMainRiskCode = mLCCont.getChild("LCInsureds")
						.getChild("LCInsured").getChild("Risks")
						.getChild("Risk").getChildText("MainRiskCode");
				if ("XBT009".equals(mMainRiskCode)
						|| "XBT029".equals(mMainRiskCode)) {
					wyPrtno = "ADP" + PubFun1.CreateMaxNo("YBTADP", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT010".equals(mMainRiskCode)
						|| "XBT030".equals(mMainRiskCode)) {
					wyPrtno = "ADQ" + PubFun1.CreateMaxNo("YBTADQ", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT011".equals(mMainRiskCode)
						|| "XBT031".equals(mMainRiskCode)) {
					wyPrtno = "ADR" + PubFun1.CreateMaxNo("YBTADR", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT012".equals(mMainRiskCode) || "XBT033".equals(mMainRiskCode)) {
					wyPrtno = "ADS" + PubFun1.CreateMaxNo("YBTADS", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT013".equals(mMainRiskCode) || "XBT034".equals(mMainRiskCode)) {
					wyPrtno = "ADT" + PubFun1.CreateMaxNo("YBTADT", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT014".equals(mMainRiskCode)) {
					wyPrtno = "ADU" + PubFun1.CreateMaxNo("YBTADU", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT017".equals(mMainRiskCode)
						|| "XBT036".equals(mMainRiskCode)) {
					wyPrtno = "AHG" + PubFun1.CreateMaxNo("YBTAHG", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT015".equals(mMainRiskCode)) {
					wyPrtno = "AHH" + PubFun1.CreateMaxNo("YBTAHH", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT023".equals(mMainRiskCode) || "XBT032".equals(mMainRiskCode)) {
					wyPrtno = "AHD" + PubFun1.CreateMaxNo("YBTAHD", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT024".equals(mMainRiskCode) || "XBT040".equals(mMainRiskCode)) {
					wyPrtno = "AHE" + PubFun1.CreateMaxNo("YBTAHE", 10);
					mProposalContNo = wyPrtno;
				}
				if ("XBT025".equals(mMainRiskCode) || "XBT041".equals(mMainRiskCode)) {
					wyPrtno = "AHF" + PubFun1.CreateMaxNo("YBTAHF", 10);
					mProposalContNo = wyPrtno;
				}
				cLogger.info("生成新的保单印刷号为：" + wyPrtno);

				String mSQL = "select 1 from lccont where ProposalContNo = '"
						+ mProposalContNo
						+ "' union select 1 from lbcont where ProposalContNo = '"
						+ mProposalContNo + "' with ur";
				String tSQL = "select 1 from lktransstatus where proposalno='"
						+ mProposalContNo + "' and rcode is null with ur";
				if (mProposalContNo.equals("")) {
					throw new MidplatException("江门邮政无可提供的单证号！");
				}
				if ("1".equals(new ExeSQL().getOneValue(tSQL))) { // 同时两个人出单
																	// 系统查出相同的单证
					throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
				} else if (new ExeSQL().getOneValue(mSQL).equals("1")) {
					// 一般不会出现这种情况
					throw new MidplatException("该投保单印刷号已使用，请更换！");
				} else {
					mLCCont.getChild(PrtNo).setText(mProposalContNo);
					mLCCont.getChild(ProposalContNo).setText(mProposalContNo);
				}
				cLogger.info("江门邮政动态生成保单印刷号和单证号结束...");
			}
			String tSQL = "select 1 from lktransstatus where PrtNo='"
					+ mLCCont.getChildText(PrtNo)
					+ "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}

			// 如果PrtNo不存在，则可以根据PrtNo删除数据
			tSQL = "select 1 from lccont where PrtNo='"
					+ mLCCont.getChildText(PrtNo)
					+ "' and appflag = '1' with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("该投保单印刷号已使用，请更换！");
			} else {
				mDeleteable = true;
			}

			tSQL = "select prtno from lccont where AppFlag='0' and ProposalContNo='"
					+ mLCCont.getChildText(ProposalContNo) + "' with ur";
			String tPrtNoStr = new ExeSQL().getOneValue(tSQL);
			cLogger.debug("PrtNo = " + tPrtNoStr);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));

			if ((null != tPrtNoStr) && !tPrtNoStr.equals("")) { // 同一保单合同书印刷号(ProposalContNo)，未签单
				cLogger.info("辽宁需要多单证号重复性校验:::" + tSQL);

				tSQL = "select 1 from LKTransStatus where RCode='1' and FuncFlag='01' "
						+ "and BankCode='"
						+ mBaseInfo.getChildText(BankCode)
						+ "' "
						+ "and BankBranch='"
						+ mBaseInfo.getChildText(ZoneNo)
						+ "' "
						+ "and BankNode='"
						+ mBaseInfo.getChildText(BrNo)
						+ "' "
						+ "and TransDate='"
						+ DateUtil.getCurDate("yyyy-MM-dd")
						+ "' "
						+ "and ProposalNo='"
						+ mLCCont.getChildText(ProposalContNo)
						+ "' "
						+ "and PrtNo='" + tPrtNoStr + "' " + "with ur";
				if (new ExeSQL().getOneValue(tSQL).equals("1")) { // 同一天，同一银行，同一网点
					YbtSufUtil.clearData(tPrtNoStr);
				}
			}

			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			// 信保通传入的是套餐编码--yinjiajia
			Element mRisk = pInXmlDoc.getRootElement().getChild(LCCont)
					.getChild(LCInsureds).getChild(LCInsured).getChild(Risks)
					.getChild(Risk);
			String tRiskWrapCode = mRisk.getChildText(MainRiskCode);
			cLogger.debug("RiskWrapCode = " + tRiskWrapCode);

			Element tThisConfRoot = cThisBusiConf.getParent();
			if (-1 != tThisConfRoot.getChildText("StopSale").indexOf(
					tRiskWrapCode)) {
				// #####################################################
				throw new MidplatException("该产品已停售");
			}

			// 停售校验 add by GaoJinfu 20180329
			YBTRiskCheck riskCheck = new YBTRiskCheck();
			String status = riskCheck.riskStopCheck(
					mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
					tRiskWrapCode, "a");
			if (status.equals("2")) {
				throw new MidplatException("该产品已停售!");
			}
			boolean gxFlag = riskCheck.riskValidCheck(
					mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
					tRiskWrapCode, "a", "GX");
			boolean gwnFlag = riskCheck.riskValidCheck(
					mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom,
					tRiskWrapCode, "a", "GWN");

			// 安徽农信社出分红险
			if (-1 != cThisBusiConf.getParent().getChildText("GWN_Risk")
					.indexOf(tRiskWrapCode)
					|| gwnFlag) { // 走picch特有的国际贸易流程
				// 目前农信社走此流程的只有安徽上了银保产品，故在此判断
				if (mLKTransStatusDB.getBankBranch().startsWith("ANHNX")) {
					mGlobalInput = YbtSufUtil.getGlobalInput1(
							mLKTransStatusDB.getBankCode(),
							mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				} else {
					mGlobalInput = YbtSufUtil.getGlobalInput(
							mLKTransStatusDB.getBankCode(),
							mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				}
				// 获得信保通费率
				// feeRateManage(mGlobalInput, mBaseInfo, mRisk, tRiskWrapCode);
				// 投保单录入
				NxsContBL ttNxsContBL = new NxsContBL(pInXmlDoc, mGlobalInput);
				ttNxsContBL.deal();

				// 被保人、险种信息录入
				NxsContInsuredIntlBL ttNxsContInsuredIntlBL = new NxsContInsuredIntlBL(
						pInXmlDoc, mGlobalInput);
				ttNxsContInsuredIntlBL.deal();

				// 自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();

				// 财务收费
				YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtTempFeeBL.deal();

				mDeleteable = true;

				// 签单+单证核销+回执回销
				YbtLCContSignBL tYbtLCContSignBL = new YbtLCContSignBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtLCContSignBL.deal();

				// 组织返回报文
				tSQL = "select contno from lccont where PrtNo='"
						+ mLCCont.getChildText(PrtNo) + "' with ur";
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						new ExeSQL().getOneValue(tSQL));
				mOutXmlDoc = ttYbtContQueryBL.deal();

				// 辽宁农信社组织关系
				if (mLKTransStatusDB.getBankBranch().startsWith("LINNX")) {
					mOutXmlDoc = NXS_NewCont.setAppnt2Insured(mOutXmlDoc);
				}
			} else if (-1 != cThisBusiConf.getParent().getChildText("GX_Risk")
					.indexOf(tRiskWrapCode)
					|| gxFlag) { // 走标准个险流程

				mGlobalInput = YbtSufUtil.getGlobalInput(
						mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());

				// 投保单录入
				YbtContBL ttYbtContBL = new YbtContBL(pInXmlDoc, mGlobalInput);
				ttYbtContBL.deal();

				// 被保人、险种信息录入
				YbtInsuredProposalBL ttYbtInsuredProposalBL = new YbtInsuredProposalBL(
						pInXmlDoc, mGlobalInput);
				ttYbtInsuredProposalBL.deal();

				// 自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();

				// 财务收费
				YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtTempFeeBL.deal();

				mDeleteable = true;

				// 签单+单证核销+回执回销
				YbtLCContSignBL tYbtLCContSignBL = new YbtLCContSignBL(
						mLKTransStatusDB.getPrtNo(), mGlobalInput);
				tYbtLCContSignBL.deal();

				// 组织返回报文
				tSQL = "select contno from lccont where PrtNo='"
						+ mLCCont.getChildText(PrtNo) + "' with ur";
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						new ExeSQL().getOneValue(tSQL));
				mOutXmlDoc = ttYbtContQueryBL.deal();

				// 辽宁农信社需要返回投被保人关系，在此组织
				if (mLKTransStatusDB.getBankBranch().startsWith("LINNX")) {
					mOutXmlDoc = NXS_NewCont.setAppnt2Insured(mOutXmlDoc);
				}
			}

			else {
				throw new MidplatException("套餐编码有误！" + tRiskWrapCode);
			}

			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setStatus("1"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)

			// 未做系统录入(客户姓名、性别、证件类型（身份证或其他）、证件号码、录入有效期)且超过20万的保单，做拒保处理 add by gcy
			// 20180205
			double mPrem = Double.parseDouble(mOutXmlDoc.getRootElement()
					.getChild("LCCont").getChildTextTrim("Prem"));
			if (mPrem > Double.parseDouble("200000.00")) {
				Element mLCAppnt = mOutXmlDoc.getRootElement()
						.getChild("LCCont").getChild("LCAppnt");
				String mSQL = "select Name,Sex,IDType,IDNo,EffEctiveDate from LCYBTCustomer where Name='"
						+ mLCAppnt.getChildText("AppntName")
						+ "'"
						+ " and IDType='"
						+ mLCAppnt.getChildText("AppntIDType")
						+ "' and IDNo='"
						+ mLCAppnt.getChildText("AppntIDNo")
						+ "'"
						+ " order by makedate desc,maketime desc with ur";
				SSRS tSSRS = new ExeSQL().execSQL(mSQL);
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("累计保费超限，需保险公司后台登记");
				}

				if ("".equals(tSSRS.GetText(1, 1))
						|| "".equals(tSSRS.GetText(1, 2))
						|| "".equals(tSSRS.GetText(1, 3))
						|| "".equals(tSSRS.GetText(1, 4))
						|| "".equals(tSSRS.GetText(1, 5))) {
					throw new MidplatException("累计保费超限，需保险公司后台登记");
				}

				if (tSSRS.GetText(1, 5) != null
						&& !"".equals(tSSRS.GetText(1, 5))) {
					FDate fDate = new FDate();
					Date effectiveDate = fDate.getDate(tSSRS.GetText(1, 5));
					Date tranDate = fDate.getDate(mBaseInfo
							.getChildText("BankDate"));
					// if (fDate.mErrors.needDealError()) {
					// throw new MidplatException("累计保费超限，需保险公司后台登记");
					// }

					GregorianCalendar tCalendar = new GregorianCalendar();
					tCalendar.setTime(tranDate);
					int sYears = tCalendar.get(Calendar.YEAR);
					int sMonths = tCalendar.get(Calendar.MONTH);
					int sDays = tCalendar.get(Calendar.DAY_OF_MONTH);

					GregorianCalendar eCalendar = new GregorianCalendar();
					eCalendar.setTime(effectiveDate);
					int eYears = eCalendar.get(Calendar.YEAR);
					int eMonths = eCalendar.get(Calendar.MONTH);
					int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);

					tCalendar.set(sYears, sMonths, sDays);
					eCalendar.set(eYears, eMonths, eDays);
					long lInterval = (eCalendar.getTime().getTime() - tCalendar
							.getTime().getTime()) / 86400000;
					if (lInterval < 0 || lInterval > 31) {
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
				}
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回

				String ttDesr = ex.getMessage();
				try {
					if (null != ttDesr
							&& ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex) {
				}
				mLKTransStatusDB.setDescr(ttDesr);
			}

			if (mDeleteable) {
				try {
					YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
				} catch (Exception tBaseEx) {
					cLogger.error("删除新单数据失败！", tBaseEx);
				}
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out NXS_NewContInput.service()!");
		return mOutXmlDoc;
	}

	/**
	 * 计算投保人年龄
	 * */
	public int getAppntAge(String ageDate) {

		Date now = new Date();
		String sdf = new SimpleDateFormat("yyyyMMdd").format(now);

		int age = 0;
		int staY = Integer.parseInt(ageDate.substring(0, 4));
		int staM = Integer.parseInt(ageDate.substring(4, 6));
		int staD = Integer.parseInt(ageDate.substring(6, 8));

		int nowY = Integer.parseInt(sdf.substring(0, 4));
		int nowM = Integer.parseInt(sdf.substring(4, 6));
		int nowD = Integer.parseInt(sdf.substring(6, 8));

		if (staM > nowM) {
			age = nowY - staY - 1;
		} else if (staM == nowM) {
			if (nowD > staD) {
				age = nowY - staY;
			} else if (nowD == staD) {
				age = nowY - staY;
			} else if (nowD < staD) {
				age = nowY - staY - 1;
			}

		} else if (staM < nowM) {
			age = nowY - staY;
		}

		return age;
	}

	private void feeRateManage(GlobalInput mGlobalInput, Element mBaseInfo,
			Element mRisk, String tRiskWrapCode) throws MidplatException {
		cLogger.info("Into NXS_NewContInput.feeRateManage()!");
		String tInsuYearFlag = mRisk.getChildText(InsuYearFlag);
		String tInsuYear = mRisk.getChildText(InsuYear);
		String tManageCom = mGlobalInput.ManageCom.substring(0, 4);

		cLogger.debug("InsuYearFlag = " + tInsuYearFlag);
		cLogger.debug("InsuYear = " + tInsuYear);
		cLogger.debug("tManageCom=" + tManageCom);

		if (-1 == cThisBusiConf.getParent().getChildText("NoFeeRate")
				.indexOf(tManageCom)) {// midplatsuf.xml中配置不走赔率表算费的农信社
			// 信保通费率需从费率配置表中读取
			String mSQL = null;
			mSQL = "Select ProtocolNo,AvailableState from LKFeeRateProtocol where "
					+ "BankCode = '"
					+ mBaseInfo.getChildText(BankCode)
					+ "' and "
					+ "ManageCom = '"
					+ mGlobalInput.ManageCom.substring(0, 4)
					+ "' and "
					+ "RiskWrapCode = '"
					+ tRiskWrapCode
					+ "' and "
					+ "AgentCom in ('"
					+ mGlobalInput.AgentCom
					+ "','PC') order by remark1 with ur";
			SSRS tSSRS = new ExeSQL().execSQL(mSQL);
			if (tSSRS.getMaxRow() < 1) {
				throw new MidplatException("未配置信保通费率信息!");
			} else {
				String mProtocolNo = tSSRS.GetText(1, 1);
				String mAvailableState = tSSRS.GetText(1, 2);
				if ("1".equals(mAvailableState)) {
					mSQL = "Select feerate,feeratetype from LKFeeRateDetails where "
							+ "ProtocolNo = '"
							+ mProtocolNo
							+ "' and "
							+ "FeeRateYearFlag = '"
							+ tInsuYearFlag
							+ "' and "
							+ "FeeRateYear in ('-1','"
							+ tInsuYear
							+ "') with ur";
					SSRS ttSSRS = new ExeSQL().execSQL(mSQL);
					if (ttSSRS.getMaxRow() != 1) {
						throw new MidplatException("查询此费率明细信息为空或多于1条记录!");
					}
					String mFeeRate = ttSSRS.GetText(1, 1);
					String mFeeRateType = ttSSRS.GetText(1, 2);
					if ("".equals(mFeeRate) || null == mFeeRate) {
						throw new MidplatException("费率明细信息添加错误!");
					}
					// 此处对报文添加费率计算信息字段,目前只支持一个险种,以后如果有多个险种,需要调整.
					Element eFeeRate = new Element("FeeRate");
					eFeeRate.setText(mFeeRate);
					Element eFeeRateType = new Element("FeeRateType");
					eFeeRateType.setText(mFeeRateType);
					mRisk.addContent(eFeeRate);
					mRisk.addContent(eFeeRateType);
				} else {
					cLogger.info("费率表配置为此机构,套餐不通过费率表算费!");
				}
			}
		}
		cLogger.info("Out NXS_NewContInput.feeRateManage()!");
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into NXS_NewContInput.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		String mRiskWrapCode = pXmlDoc.getRootElement().getChild(LCCont)
				.getChild(LCInsureds).getChild(LCInsured).getChild(Risks)
				.getChild(Risk).getChildText(MainRiskCode);
		mLKTransStatusDB.setRiskCode(mRiskWrapCode);// 信保通传入的是套餐编码
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setStatus("0"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setServiceStartTime(mLCCont
				.getChildText("LoanStartDate"));// 借款起期
		mLKTransStatusDB.setServiceEndTime(mLCCont.getChildText("LoanEndDate"));// 借款终期

		if (mBaseInfo.getChildText(BankCode).equals("60")) { // 江西农信社该信息过长所以存入到预留字段bak1中
			mLKTransStatusDB.setbak1(mLCCont.getChildText("LoanInvoiceNo"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("LoanContractNo"));
		} else {
			mLKTransStatusDB
					.setTempFeeNo(mLCCont.getChildText("LoanInvoiceNo"));// 贷款凭证编号
		}

		mLKTransStatusDB.setEdorNo(mLCCont.getChildText("LoanContractAmt"));// 贷款金额
		if (mBaseInfo.getChild("ReportNo") != null) {
			mLKTransStatusDB.setReportNo(mBaseInfo.getChildText("ReportNo"));
		}
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out NXS_NewContInput.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static Document setAppnt2Insured(Document OutStdDoc) {

		Element mLCCont = OutStdDoc.getRootElement().getChild("LCCont");
		String mContNo = mLCCont.getChildText("ContNo");

		ExeSQL es = new ExeSQL();
		// 投保人与被保人关系未存，直接取被保人与投保人关系，然后在前置机进行转换为对应标准
		String sql_r2i = "select RelationToAppnt from lcinsured where contno = '"
				+ mContNo + "' with ur";
		String mRelationToAppnt = es.getOneValue(sql_r2i);

		Element mRelaToInsured = new Element("RelaToInsured");
		mRelaToInsured.setText(mRelationToAppnt);
		mLCCont.getChild("LCAppnt").addContent(mRelaToInsured);

		String sql_bnf = "";
		List mBnfList = mLCCont.getChild("LCInsureds").getChild("LCInsured")
				.getChild("Risks").getChild("Risk").getChild("LCBnfs")
				.getChildren("LCBnf");
		for (int i = 0; i < mBnfList.size(); i++) {
			Element mBnf = (Element) mBnfList.get(i);
			String mBnfName = mBnf.getChildText("Name");
			sql_bnf = "select relationtoinsured from lcbnf where contno = '"
					+ mContNo + "' and name = '" + mBnfName + "' with ur";
			mBnf.addContent(new Element("RelationToInsured").setText(es
					.getOneValue(sql_bnf)));
		}

		return OutStdDoc;
	}

	// 处理江门邮政满期数据 add by GaoJinfu 20171108
	private boolean dealMQ(String idNo) {
		String sql = "select contno from lccont where stateflag = '1' and cinvalidate <= current timestamp and contno in (select contno from lcinsured where idno = '"
				+ idNo + "')";
		SSRS ss = new SSRS();
		ss = new ExeSQL().execSQL(sql);
		MMap mupdateMMap = new MMap();
		if (ss.getMaxRow() > 0) {
			for (int i = 1; i <= ss.getMaxRow(); i++) {
				mupdateMMap.put(
						"update lcpol set stateflag = '3' where contno = '"
								+ ss.GetText(i, 1) + "'", "UPDATE");
				mupdateMMap.put(
						"update lccont set stateflag = '3' where contno = '"
								+ ss.GetText(i, 1) + "'", "UPDATE");
				mupdateMMap
						.put("INSERT INTO LCCONTSTATE (select '000000', contno, insuredno, '000000', 'Terminate ', 'CD', 'YBT'||contno, '1', 'CD', CURRENT DATE, '9999-12-31', '', '000', CURRENT DATE, maketime, CURRENT DATE,modifytime from lccont where contno ='"
								+ ss.GetText(i, 1) + "')", "INSERT");
			}
			VData mSubmitVData = new VData();
			mSubmitVData.add(mupdateMMap);
			PubSubmit mPubSubmit = new PubSubmit();
			if (!mPubSubmit.submitData(mSubmitVData, "")) {
				cLogger.error(mPubSubmit.mErrors.getFirstError());
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT029.xml";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT030.xml";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT031.xml";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT036.XML";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT033.XML";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT034.XML";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT032.XML";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT040.XML";
//		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT041.XML";
		String mInFile = "F:\\000gaojinfu\\bankMassage\\nxs_guangdong\\instd\\trailXBT014.XML";
		String mOutFile = "d:/out.xml";

		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));

		String elpath = "F:\\sinosoft\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		Element root = c.getRootElement().getChild("business");

		Document mOutXmlDoc = new NXS_NewCont(root).service(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		// JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
	}
}
