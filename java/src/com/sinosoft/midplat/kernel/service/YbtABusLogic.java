/**
 * 2010-8-23
 */
package com.sinosoft.midplat.kernel.service;

import org.apache.log4j.Logger;

import com.cbsws.core.obj.MsgResHead;
import com.cbsws.core.util.MsgUtil;
import com.cbsws.core.xml.ctrl.IBusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.core.xml.xsch.BaseXmlSch;

/**
 * @author LY
 *
 */
public abstract class YbtABusLogic implements YbtIBusLogic
{
    protected Logger mLogger = Logger.getLogger(getClass().getName());

    protected MsgCollection mResMsgCols = null;

    public YbtABusLogic()
    {
        mLogger.info("start:" + getClass().getName());
    }

    public final MsgCollection service(MsgCollection cMsgInfos, int mm)
    {
        mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
        if (!deal(cMsgInfos,mm))
        {
            MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            if (tResMsgHead.getErrCode() == null)
            {
                errLog("逻辑处理中出现，未知异常");
            }
        }
        else
        {
            MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            tResMsgHead.setState("00");
        }
        return getResult();
    }

    protected final void errLog(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);

        String tErrCode = "99999999";
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
        tResMsgHead.setState("01");
        tResMsgHead.setErrCode(tErrCode);
        tResMsgHead.setErrInfo(cErrInfo);
    }

    protected MsgCollection getResult()
    {
        return mResMsgCols;
    }

    protected final void putResult(String cNodeFlag, BaseXmlSch cBxsch)
    {
        mResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
    }

    protected abstract boolean deal(MsgCollection cMsgInfos, int mm);
}
