package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKNoRealTimeUWCheckDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKNoRealTimeUWCheckSchema;
import com.sinosoft.lis.vschema.LKNoRealTimeUWCheckSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ICBC_YBTNoRealTimeUWQueryBL extends ServiceImpl  {

	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	private LKNoRealTimeUWCheckSet mLKNoRealTimeUWCheckSet ;
	
	public ICBC_YBTNoRealTimeUWQueryBL(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_YBTNoRealTimeUWQueryBL.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		Element mTranData = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
//		Element mBaseInfo = pInXmlDoc.getRootElement().getChild(BaseInfo);
		LKTransStatusDB mLKTransStatusDB = null;	
		
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			//业务处理
			
			//1、更新LKNoRealTimeUWCheck表中保单状态
			noRealTimeUWCheckChange();
			
			//2、取投保单受理状态变更文件
			Thread.sleep(5000);
			mOutXmlDoc = deal();
			mFlag.setText("0");
			mDesc.setText("交易成功！");
			mOutXmlDoc.getRootElement().getChild("RetData").addContent(mFlag);
			mOutXmlDoc.getRootElement().getChild("RetData").addContent(mDesc);
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");//执行任务完毕记
//			mLKTransStatusDB.setTransCode("1");//文件生成
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ICBC_YBTNoRealTimeUWQueryBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal() throws Exception {
		// 查出未签单的保单变更数据
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mLCCont = new Element("LCCont");
		
		Element mChkDetails = new Element("ChkDetails");
		
		LKNoRealTimeUWCheckDB mLKNoRealTimeUWCheck = new LKNoRealTimeUWCheckDB();
		mLKNoRealTimeUWCheck.setModifyDate(mCurrentDate);
		mLKNoRealTimeUWCheckSet = mLKNoRealTimeUWCheck.query();
			
		for (int i = 1; i <= mLKNoRealTimeUWCheckSet.size(); i++) {
			LKNoRealTimeUWCheckSchema mLKNoRealTimeUWCheckSchema = mLKNoRealTimeUWCheckSet.get(i);
			if("0".equals(mLKNoRealTimeUWCheckSchema.getTbDealStatus()) || "1".equals(mLKNoRealTimeUWCheckSchema.getPassFlag())){
				//判断是否有刚录入的数据，防止漏网之鱼    passflag = 1  为已签单数据，此批次只提变更且未完成的保单
				cLogger.info("此单为已承保，prtno = "+ mLKNoRealTimeUWCheckSchema.getProposalContno());
				continue;
			}
			Element mChkDetail = new Element("ChkDetail");
			Element mZoneNo = new Element("ZoneNo");
			mZoneNo.setText(mLKNoRealTimeUWCheckSchema.getZoneNo());
			
			Element mInsuredID = new Element("InsuredID");
			mInsuredID.setText("020");
			
			Element mTransNo = new Element("TransNo");
			mTransNo.setText(mLKNoRealTimeUWCheckSchema.getTransNo());
			
			Element mProposalContno = new Element("ProposalContno");
			mProposalContno.setText(mLKNoRealTimeUWCheckSchema.getProposalContno());
			
			Element mAppntName = new Element("AppntName");
			mAppntName.setText(mLKNoRealTimeUWCheckSchema.getAppntName());
			
			Element mTbDealStatus = new Element("TbDealStatus");
			mTbDealStatus.setText(mLKNoRealTimeUWCheckSchema.getTbDealStatus());
			
			Element mDescr = new Element("Descr");
			mDescr.setText(mLKNoRealTimeUWCheckSchema.getDescr());
			
			mChkDetail.addContent(mZoneNo);
			mChkDetail.addContent(mInsuredID);
			mChkDetail.addContent(mTransNo);
			mChkDetail.addContent(mProposalContno);
			mChkDetail.addContent(mAppntName);
			mChkDetail.addContent(mTbDealStatus);
			mChkDetail.addContent(mDescr);
			
			mChkDetails.addContent(mChkDetail);
		}
		
		mLCCont.addContent(mChkDetails);
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		
		return new Document(mTranData);
	}

	private void noRealTimeUWCheckChange() {
		// 调用核心接口，更新中间表中保单状态
//		String sql_check = "select ProposalContno from LKNoRealTimeUWCheck where PassFlag <> '1' with ur";
		
		LKNoRealTimeUWCheckDB mLKNoRealTimeUWCheck = new LKNoRealTimeUWCheckDB();
		mLKNoRealTimeUWCheck.setPassFlag("0");
		mLKNoRealTimeUWCheckSet = mLKNoRealTimeUWCheck.query();
		int mLKNoRealTimeUWCheckSize = mLKNoRealTimeUWCheckSet.size();
		//调用核心接口  传值ProposalContno 得到保单状态
		for (int i = 0; i < mLKNoRealTimeUWCheckSize; i++) {
			LKNoRealTimeUWCheckSchema mLKNoRealTimeUWCheckSchema = mLKNoRealTimeUWCheckSet.get(i+1);
			System.out.println("$$$$$$$$$$$$$$="+mLKNoRealTimeUWCheckSchema.getProposalContno());
			/**
			 * 受理状态 核心契约提供查询状态SQL  add by wz 20130602
			 * 11：已承保  12：已撤单  13：录入中 14：复核中 15：核保通过待收费 
			 *1：谢绝承保 2：延期承保 3：条件承保 4：变更承保 5：自核未通过 6：待上级审核 7：问题件 8：延期承保 9：正常承保 a:撤销申请 b:保险计划变更 z:核保订正
			 * */
			String sql_tbDealStatus = "Select Case"
							         +" When Signdate Is Not Null Then"
							         +" '11'"
							         +" When Uwflag = 'a' Then"
							         +" '12'"
							         +" When Inputdate Is Null Then"
							         +" '13'"
							         +"  When Approvedate Is Null Then"
							         +" '14'"
							         +" When Uwflag = '9' Then"
							         +" '15'"
							         +"  Else"
							         +" (Select Code"
							         +" From Ldcode"
							         +" Where Codetype = 'uwflag'"
							         +" And Code = Uwflag)"
							         +" End"
							         +" From Lccont"
							         +" Where Prtno = '"+ mLKNoRealTimeUWCheckSchema.getProposalContno() +"'";
			
			String hxTbDealStatus = new ExeSQL().getOneValue(sql_tbDealStatus); //调用核心接口得到状态
			if("".equals(hxTbDealStatus) || hxTbDealStatus == null){
				hxTbDealStatus = mLKNoRealTimeUWCheckSchema.getTbDealStatus();
			}
			if(hxTbDealStatus.equals(mLKNoRealTimeUWCheckSchema.getTbDealStatus())){
				cLogger.info("核心查到状态与原来状态一致，不予更新。印刷号为："+ mLKNoRealTimeUWCheckSchema.getProposalContno());
//				mLKNoRealTimeUWCheckSet.remove(mLKNoRealTimeUWCheckSet.get(i+1));
				continue;
			}else {
				mLKNoRealTimeUWCheckSchema.setTbDealStatus(hxTbDealStatus);
				mLKNoRealTimeUWCheckSchema.setModifyDate(mCurrentDate);
				mLKNoRealTimeUWCheckSchema.setModifyTime(mCurrentTime);
			}
			
			//如果返回保单状态为已承保，更新PassFlag字段为1
			if("11".equals(hxTbDealStatus)){
				mLKNoRealTimeUWCheckSchema.setPassFlag("1");
			}
		}
		
		MMap tMMap = new MMap();
		VData tVData = new VData();
		tMMap.put(mLKNoRealTimeUWCheckSet, "UPDATE");
		tVData.add(tMMap);
		PubSubmit tPs = new PubSubmit();
		if (!tPs.submitData(tVData, "")) {
			cLogger.info("执行非实时核保中间表更新失败..." + tPs.mErrors.getContent());
			
		}
		
	}

	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		cLogger.info("Into ICBC_YBTNoRealTimeUWQueryBL.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out ICBC_YBTNoRealTimeUWQueryBL.insertTransLog()...");
		return mLKTransStatusDB;
	}

}
