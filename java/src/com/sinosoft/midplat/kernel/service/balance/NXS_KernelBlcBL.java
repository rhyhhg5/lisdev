package com.sinosoft.midplat.kernel.service.balance;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class NXS_KernelBlcBL {
	private final static Logger cLogger = Logger.getLogger(NXS_KernelBlcBL.class);
	
	private final String cBankCode;
	private final String cTranDate;
	private final String cManageCom;
	
	private final String cCurDate = DateUtil.getCur10Date();
	private final String cCurTime = DateUtil.getCur8Time();
	
	public NXS_KernelBlcBL(String pBankCode, String pTranDate, String pManageCom) {
		cBankCode = pBankCode;
		cTranDate = pTranDate;
		cManageCom = pManageCom;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into NXS_KernelBlcBL.deal()...");

		/**Start-处理核心认为成功承保的保单************************/
		//查询核心表中该银行、该机构、当日银保通有效保单
		String mSQL = "select * from LCCont where 1=1 "
			+ "and SignDate='" + cTranDate + "' "
			+ "and AppFlag='1' "
			+ "and SaleChnl in('03','04') "
			+ "and CardFlag in('a','9') "
			+ "and BankCode like '" + cBankCode + "%' "
			+ "and ManageCom like '" + cManageCom + "%' "
			+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
			+"'" + cBankCode + "' "
			+"and ManageCom <> '" + cManageCom +"' "
			+"and ManageCom like '" +cManageCom +"%' "
			+"and 1=LOCATE(trim(ManageCom),LCCont.ManageCom)) " 
			+ "with ur";
		cLogger.info("SQL: " + mSQL);
		LCContDB mLCContDB = new LCContDB();
		LCContSet cSignSet = mLCContDB.executeQuery(mSQL);
		if (mLCContDB.mErrors.needDealError()) {
			cLogger.error(mLCContDB.mErrors.getFirstError());
			throw new MidplatException("查询核心承保数据失败！");
		} else if (null == cSignSet) {
			cLogger.warn("查询核心处于承保状态的保单，LCContSignSet=null！");
			cSignSet = new LCContSet();
		}
		cLogger.info("核心认为处于承保状态的保单数(Kernel.SignSet)为：" + cSignSet.size());

		//循环比对核心成功承保的保单
		for (int i = 1; i <= cSignSet.size(); i++) {
			LCContSchema tLCContSchema = cSignSet.get(i);
			
			//查询该银行、该机构当日有效保单
			mSQL = "select * from LKTransStatus where FuncFlag='01' "
				+ "and BankCode='" + cBankCode + "' "
				+ "and TransDate='" + cTranDate + "' "
				+ "and PolNo='" + tLCContSchema.getContNo() + "' "
				+ "and ManageCom like '" + cManageCom + "%' "
				+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
				+"'" + cBankCode + "' "
				+"and ManageCom <> '" + cManageCom +"' "
				+"and ManageCom like '" +cManageCom +"%' "
				+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) " 
				+ "with ur";
			cLogger.info("SQL: " + mSQL);
			LKTransStatusDB tLKTransStatusDB = new LKTransStatusDB();
			LKTransStatusSet tLKTransStatusSet = tLKTransStatusDB.executeQuery(mSQL);
			if (tLKTransStatusDB.mErrors.needDealError()) {
				cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
				throw new MidplatException("查询银保通数据失败！");
			}
			
			//未查询到相关日志，补录承保成功日志
			if ((null==tLKTransStatusSet) || (0==tLKTransStatusSet.size())) {
				LKTransStatusDB ttLKTransStatusDB = new LKTransStatusDB();
				ttLKTransStatusDB.setBankCode(cBankCode);
				ttLKTransStatusDB.setBankBranch(cBankCode);
				ttLKTransStatusDB.setBankNode(cBankCode);
				ttLKTransStatusDB.setTransNo(PubFun1.CreateMaxNo("YBTNO", 10) + "BLC");
				ttLKTransStatusDB.setManageCom(tLCContSchema.getManageCom());
				ttLKTransStatusDB.setFuncFlag("01");
				ttLKTransStatusDB.setRiskCode("BLC");
				ttLKTransStatusDB.setPrtNo(tLCContSchema.getPrtNo());
				ttLKTransStatusDB.setPolNo(tLCContSchema.getContNo());
				ttLKTransStatusDB.setProposalNo(tLCContSchema.getProposalContNo());
				ttLKTransStatusDB.setTransAmnt(tLCContSchema.getPrem());
				ttLKTransStatusDB.setTransDate(cTranDate);
				ttLKTransStatusDB.setMakeDate(cCurDate);
				ttLKTransStatusDB.setMakeTime(cCurTime);
				ttLKTransStatusDB.setModifyDate(cCurDate);
				ttLKTransStatusDB.setModifyTime(cCurTime);
				ttLKTransStatusDB.setStatus("1");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
				ttLKTransStatusDB.setState_Code("01");
				ttLKTransStatusDB.setRMPVSKernel("01");
				ttLKTransStatusDB.setDesMPVSKernel("核心认为成功，银保通认为未承保，自动补录新单成功日志！");
				ttLKTransStatusDB.setbak1("1");	//对账生成的日志
				cLogger.info("ContNo = " + ttLKTransStatusDB.getPolNo()
						+ "； RMPVSKernel = " + ttLKTransStatusDB.getRMPVSKernel()
						+ "； DesMPVSKernel = "	+ ttLKTransStatusDB.getDesMPVSKernel());
				if (!ttLKTransStatusDB.insert()) {
					cLogger.error(ttLKTransStatusDB.mErrors.getFirstError());
					throw new MidplatException("与核心对账，自动补录新单成功日志失败！");
				}
			} else if (1 == tLKTransStatusSet.size()) {	//将保单状态和保费强行置为与核心一致
				LKTransStatusDB ttLKTransStatusDB = tLKTransStatusSet.get(1).getDB();
				ttLKTransStatusDB.setTransAmnt(tLCContSchema.getPrem());
				ttLKTransStatusDB.setStatus("1");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
				ttLKTransStatusDB.setState_Code("01");
				ttLKTransStatusDB.setRMPVSKernel("00");
				ttLKTransStatusDB.setDesMPVSKernel("核心认为成功，银保通保单状态(Status)和保费(TransAmnt)强行置为与核心一致！");
				ttLKTransStatusDB.setModifyDate(cCurDate);
				ttLKTransStatusDB.setModifyTime(cCurTime);
				cLogger.info("ContNo = " + ttLKTransStatusDB.getPolNo()
						+ "； RMPVSKernel = " + ttLKTransStatusDB.getRMPVSKernel()
						+ "； DesMPVSKernel = "	+ ttLKTransStatusDB.getDesMPVSKernel());
				if (!ttLKTransStatusDB.update()) {
					cLogger.error(ttLKTransStatusDB.mErrors.getFirstError());
					throw new MidplatException("与核心对账，更新对账信息失败！");
				}
			} else {
				throw new MidplatException("银保通日志信息有误！");
			}
		}
		/**End-处理核心认为成功承保的保单************************/
		
		/**Start-处理银保通认为成功，核心认为无效的保单***********/
		//查询银保通表中该银行、该机构、当日有效，但是核心认为无效的保单
		mSQL = "select * from LKTransStatus where FuncFlag='01' "
			+ "and Status='1' "
			+ "and BankCode='" + cBankCode + "' "
			+ "and TransDate='" + cTranDate + "' "
			+ "and ManageCom like '" + cManageCom + "%' "
			+ "and (State_Code<>'01' or State_Code is null) "
			+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
			+"'" + cBankCode + "' "
			+"and ManageCom <> '" + cManageCom +"' "
			+"and ManageCom like '" +cManageCom +"%' "
			+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) " 
			+ "with ur";
		cLogger.info("SQL: " + mSQL);
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		LKTransStatusSet mLKTransStatusSet = mLKTransStatusDB.executeQuery(mSQL);
		if (mLKTransStatusSet.mErrors.needDealError()) {
			cLogger.error(mLKTransStatusSet.mErrors.getFirstError());
			throw new MidplatException("查询银保通数据失败！");
		} else if (null == mLKTransStatusSet) {
			cLogger.warn("查询银保通处于成功，核心认为失效的保单，LKTransStatusSet=null！");
			mLKTransStatusSet = new LKTransStatusSet();
		}
		cLogger.info("银保通认为成功，核心认为失效的保单数(YbtNotKernel.SignSet)为：" + mLKTransStatusSet.size());
		
		//循环将银保通处于成功核心认为失效的保单的状态置为失效
		for (int i = 1; i <= mLKTransStatusSet.size(); i++) {
			LKTransStatusDB tLKTransStatusDB = mLKTransStatusSet.get(i).getDB();

			tLKTransStatusDB.setStatus("2");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
			tLKTransStatusDB.setState_Code("01");
			tLKTransStatusDB.setRMPVSKernel("02");
			tLKTransStatusDB.setDesMPVSKernel("银保通认为成功，核心认为失效，保单强制置为作废状态！");
			tLKTransStatusDB.setModifyDate(cCurDate);
			tLKTransStatusDB.setModifyTime(cCurTime);
			cLogger.info("ContNo = " + tLKTransStatusDB.getPolNo()
					+ "； RMPVSKernel = " + tLKTransStatusDB.getRMPVSKernel()
					+ "； DesMPVSKernel = "	+ tLKTransStatusDB.getDesMPVSKernel());
			if (!tLKTransStatusDB.update()) {
				cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
				throw new MidplatException("与核心对账，更新对账信息失败！");
			}
		}
		/**End-处理银保通认为成功，核心认为无效的保单*************/
		
		cLogger.info("Out NXS_KernelBlcBL.deal()!");
	}
	
	/**
	 * 处理银保通日间在核心不签单的数据 -直接删除
	 * */ 
	public void dealNoSignDate() throws Exception {
		cLogger.info("Into NXS_KernelBlcBL.dealNoSign()...");
		
		YbtSufUtil ybtSufUtil = new YbtSufUtil();

		/**Start-处理核心认为未签单的保单************************/
		//查询核心表中该银行、该机构、当日银保通只试算保单
		String mSQL = "select * from LCCont where AppFlag='0' "
			+ "and SaleChnl in ('03','04') "
			+ "and CardFlag in ('a','9') "
			+ "and BankCode like '" + cBankCode + "%' "
			+ "and PolApplyDate='" + cTranDate + "' "
			+ "and ManageCom like '" + cManageCom + "%' "
			+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
			+"'" + cBankCode + "' "
			+"and ManageCom <> '" + cManageCom +"' "
			+"and ManageCom like '" +cManageCom +"%' "
			+"and 1=LOCATE(trim(ManageCom),LCCont.ManageCom)) " 
			+ "with ur";
		cLogger.info("SQL: " + mSQL);
		LCContDB mLCContDB = new LCContDB();
		LCContSet cSignSet = mLCContDB.executeQuery(mSQL);
		if (mLCContDB.mErrors.needDealError()) {
			cLogger.error(mLCContDB.mErrors.getFirstError());
			throw new MidplatException("查询核心承保数据失败！");
		} else if (null == cSignSet) {
			cLogger.warn("查询银保通在核心只试算的保单，LCContSignSet=null！");
			cSignSet = new LCContSet();
		}
		cLogger.info("核心未签单的保单数(Kernel.SignSet)为：" + cSignSet.size());

		//循环比对核心成功承保的保单
		for (int i = 1; i <= cSignSet.size(); i++) {
			LCContSchema tLCContSchema = cSignSet.get(i);
			//删除未签单的保单数据
			ybtSufUtil.clearData(tLCContSchema.getPrtNo());
		}
		
		cLogger.info("Out NXS_KernelBlcBL.dealNoSign()...");
	}

}
