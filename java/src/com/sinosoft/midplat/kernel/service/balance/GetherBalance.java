/**
 * 与银行汇总对账
 */

package com.sinosoft.midplat.kernel.service.balance;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;

public class GetherBalance {
	private final static Logger cLogger = Logger.getLogger(GetherBalance.class);
	
	private final String cBankCode;
	private final String cTransDate;
	private final String cManageCom;
	
	/**
	 * 
	 */
	public GetherBalance(String pBankCode, String pTransDate, String pManageCom) {
		cBankCode = pBankCode;
		cTransDate = pTransDate;
		cManageCom = pManageCom;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into GetherBalance.deal()...");
		
		//查询该银行、该机构、当天做过明细对账的保单
		String mSQL = "select * from LKTransStatus where State_Code='02' "
			+ "and BankCode='" + cBankCode + "' "
			+ "and TransDate='" + cTransDate + "' "
			+ "and ManageCom like '" + cManageCom + "%' "
			+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
			+"'" + cBankCode + "' "
			+"and ManageCom <> '" + cManageCom +"' "
			+"and ManageCom like '" +cManageCom +"%' "
			+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) "
			+ "with ur";
		cLogger.info("SQL: " + mSQL);
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		LKTransStatusSet mLKTransStatusSet = mLKTransStatusDB.executeQuery(mSQL);
		if (mLKTransStatusSet.mErrors.needDealError()) {
			cLogger.error(mLKTransStatusSet.mErrors.getFirstError());
			throw new MidplatException("查询明细对账记录失败！");
		} else if (null == mLKTransStatusSet) {
			cLogger.warn("查询明细对账记录，LKTransStatusSet=null！");
			mLKTransStatusSet = new LKTransStatusSet();
		}
		cLogger.info("明细对账记录数(Detail.LKTransStatusSet)为：" + mLKTransStatusSet.size());
		
		for (int i = 1; i <= mLKTransStatusSet.size(); i++) {
			LKTransStatusDB tLKTransStatusDB = mLKTransStatusSet.get(i).getDB();
			String tRBankVSMP = tLKTransStatusDB.getRBankVSMP();
			String tContNO = tLKTransStatusDB.getPolNo();
			
			/**
			 * 数据一致，不作处理
			 */
			if (tRBankVSMP.equals("00")) {
				cLogger.info("汇总对账，数据一致，不作处理！ContNO=" + tContNO);
				
        		tLKTransStatusDB.setResultBalance("0");	//汇总对账处理结果标识：0-成功；1-失败
        		tLKTransStatusDB.setDesBalance("汇总对账成功！");
			}
			/**
			 * 承保状态，保费不一致！
			 */
			else if (tRBankVSMP.equals("01")) {
				cLogger.info("汇总对账，保费金额不一致！ContNO = " + tContNO);
				
        		tLKTransStatusDB.setResultBalance("1");		//汇总对账处理结果标识：0 - 成功；1 - 失败
        		tLKTransStatusDB.setDesBalance(tLKTransStatusDB.getDesBankVSMP());
			}
			/**
			 * 未查询到有效保单信息！
			 */
			else if (tRBankVSMP.equals("02")) {
				cLogger.info("汇总对账，未查询到有效保单信息！ContNO = " + tContNO);
				
        		tLKTransStatusDB.setResultBalance("1");		//汇总对账处理结果标识：0 - 成功；1 - 失败
        		tLKTransStatusDB.setDesBalance(tLKTransStatusDB.getDesBankVSMP());
			}
			/**
			 * 银行认为无效，银保通认为成功！
			 */
			else if (tRBankVSMP.equals("03")) {
				cLogger.info("汇总对账，银行认为无效，银保通认为成功，执行自动冲正！ContNO = " + tContNO);
				
				try {
					AutoWriteOff ttAutoWriteOff = new AutoWriteOff(tLKTransStatusDB);
					ttAutoWriteOff.deal();
					tLKTransStatusDB.setResultBalance("0");		//汇总对账处理结果标识：0 - 成功；1 - 失败
	        		tLKTransStatusDB.setDesBalance("汇总对账成功！");
				} catch (Exception ex) {
					cLogger.error("自动冲正失败！", ex);
					
	        		tLKTransStatusDB.setResultBalance("1");
	        		tLKTransStatusDB.setDesBalance("自动冲正失败！" + ex.getMessage());
				}
			}
			/**
			 * 对账状态有误！
			 */
			else {
				cLogger.error("汇总对账，对账状态有误！经明细对账后的状态代码：" + tRBankVSMP
						+ "；状态描述：" + tLKTransStatusDB.getDesBankVSMP() + "；保单号：" + tContNO);
				
        		tLKTransStatusDB.setResultBalance("1");		//汇总对账处理结果标识：0 - 成功；1 - 失败
        		tLKTransStatusDB.setDesBalance("对账状态有误，错误代码为：" + tRBankVSMP);
			}
			
			tLKTransStatusDB.setState_Code("03");			//对账状态：01-核心对账；02-明细对账；03-汇总对账
			tLKTransStatusDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
			tLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!tLKTransStatusDB.update()) {
				cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
				throw new MidplatException("与银行汇总对账，更新对账信息失败！");
			}
		}
		
		cLogger.info("Out GetherBalance.deal()!");
	}
}
