/**
 * 与银行明细对账
 */

package com.sinosoft.midplat.kernel.service.balance;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKBalanceDetailDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LKBalanceDetailSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LKBalanceDetailSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;

public class DetailBalanceBL {
	private final static Logger cLogger = Logger.getLogger(DetailBalanceBL.class);
	
	private final String cBankCode;
	private final String cTransDate;
	private final String cManageCom;
	private final LKBalanceDetailSet cLKBalanceDetailSet;
	
	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");
	
	public DetailBalanceBL(LKTransStatusSchema pLKTransStatusSchema) throws MidplatException {
		cBankCode = pLKTransStatusSchema.getBankCode();
		cTransDate = pLKTransStatusSchema.getTransDate();
		cManageCom = pLKTransStatusSchema.getBankBranch();
		
		String mSQL = "select * from LKBalanceDetail where FuncFlag='01' "
			+ "and BankCode='" + cBankCode + "' "
			+ "and BalanceNo='" + pLKTransStatusSchema.getTransNo() + "' "
			+ "and BankZoneCode='" + cManageCom + "' "
			+ "and BrNo='" + cManageCom + "' "
			+ "with ur";
		LKBalanceDetailDB mLKBalanceDetailDB = new LKBalanceDetailDB();
		LKBalanceDetailSet mLKBalanceDetailSet = mLKBalanceDetailDB.executeQuery(mSQL);
		if (mLKBalanceDetailDB.mErrors.needDealError()) {
			cLogger.error(mLKBalanceDetailDB.mErrors.getFirstError());
			throw new MidplatException("查询银行对账明细数据失败！");
		} else if (null == mLKBalanceDetailSet) {
			cLogger.warn("查询银行对账明细数据，LKBalanceDetailSet=null！");
			mLKBalanceDetailSet = new LKBalanceDetailSet();
		}
		cLKBalanceDetailSet = mLKBalanceDetailSet;
	}
	
	/**
	 * 
	 */
	public DetailBalanceBL(String pBankCode, String pTransDate, String pManageCom, LKBalanceDetailSet pLKBalanceDetailSet) {
		cBankCode = pBankCode;
		cTransDate = pTransDate;
		cManageCom = pManageCom;
		cLKBalanceDetailSet = new LKBalanceDetailSet();
		for (int i = 1; i <= pLKBalanceDetailSet.size(); i++) {
			LKBalanceDetailSchema tLKBalanceDetailSchema = pLKBalanceDetailSet.get(i);
			if (tLKBalanceDetailSchema.getFuncFlag().equals("01")) {
				cLKBalanceDetailSet.add(tLKBalanceDetailSchema);
			}
		}
	}
	
	public void deal() throws Exception {
		cLogger.info("Into DetailBalanceBL.deal()...");
		
		/**Start-处理银行认为成功承保的保单************************/
		cLogger.info("银行认为处于承保状态的保单数(Bank.SignSet)为：" + cLKBalanceDetailSet.size());
		//循环比对银行认为成功承保的保单明细
		for (int i = 1; i <= cLKBalanceDetailSet.size(); i++) {
			LKBalanceDetailSchema tLKBalanceDetailSchema = cLKBalanceDetailSet.get(i);
      	String tContNo = tLKBalanceDetailSchema.getCardNo();
      	double tPrem = tLKBalanceDetailSchema.getTranAmnt();
			
      	//查询该银行、该机构当日有效保单
			String tSQL = "select * from LKTransStatus where FuncFlag='01' "
				+ "and Status='1' "
				+ "and BankCode='" + cBankCode + "' "
				+ "and TransDate='" + cTransDate + "' "
				+ "and PolNo='" + tContNo + "' "
				+ "and ManageCom like '" + cManageCom + "%' "
				+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
				+"'" + cBankCode + "' "
				+"and ManageCom <> '" + cManageCom +"' "
				+"and ManageCom like '" +cManageCom +"%' "
				+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) " 
				+ "with ur";
			cLogger.info("SQL: " + tSQL);
			LKTransStatusDB tLKTransStatusDB = new LKTransStatusDB();
			LKTransStatusSet tLKTransStatusSet = tLKTransStatusDB.executeQuery(tSQL);
			if (tLKTransStatusDB.mErrors.needDealError()) {
				cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
				throw new MidplatException("查询银保通数据失败！");
			}
			
			/**
			 * 未查询到相关日志，补录“未查询到有效保单”的日志。
			 * 这种情况理论上不该发生，一般会在以下三种情况下出现：
			 *   1. 保险公司方误删除数据(包括冲正和犹豫期撤单)
			 *   2. 保险公司方保单确认未成功(未签单)
			 *   3. 银行误发送错误数据
			 * 由于情况复杂，需要人工处理。
			 */
			if ((null==tLKTransStatusSet) || (0==tLKTransStatusSet.size())) {
				LKTransStatusDB ttLKTransStatusDB = new LKTransStatusDB();
				ttLKTransStatusDB.setTransNo(PubFun1.CreateMaxNo("YBTNO", 10) + "BLC");
				ttLKTransStatusDB.setBankCode(cBankCode);
				ttLKTransStatusDB.setBankBranch(tLKBalanceDetailSchema.getBankZoneCode());
				ttLKTransStatusDB.setBankNode(tLKBalanceDetailSchema.getBrNo());
				String ttSQL = "select ManageCom from LKCodeMapping where 1=1 "
					+ "and BankCode='" + cBankCode + "' "
					+ "and ZoneNo='" + tLKBalanceDetailSchema.getBankZoneCode() + "' " 
					+ "and BankNode='" + tLKBalanceDetailSchema.getBrNo() + "' "
					+ "with ur";
				ttLKTransStatusDB.setManageCom(new ExeSQL().getOneValue(ttSQL));
				ttLKTransStatusDB.setFuncFlag("01");
				ttLKTransStatusDB.setPolNo(tContNo);
				ttLKTransStatusDB.setTransAmnt(tPrem);
				ttLKTransStatusDB.setTransDate(cTransDate);
				ttLKTransStatusDB.setMakeDate(cCurDate);
				ttLKTransStatusDB.setMakeTime(cCurTime);
				ttLKTransStatusDB.setModifyDate(cCurDate);
				ttLKTransStatusDB.setModifyTime(cCurTime);
				ttLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
				ttLKTransStatusDB.setState_Code("02");
				ttLKTransStatusDB.setRBankVSMP("02");
				ttLKTransStatusDB.setDesBankVSMP("未查询到有效保单信息！");
				ttLKTransStatusDB.setbak1("1");
				cLogger.info("ContNo = " + tContNo + "； RBankVSMP = " + ttLKTransStatusDB.getRBankVSMP() 
							+ "； DesBankVSMP = " + ttLKTransStatusDB.getDesBankVSMP());
				if (!ttLKTransStatusDB.insert()) {
					cLogger.error(ttLKTransStatusDB.mErrors.getFirstError());
					throw new MidplatException("与银行明细对账，自动补录“未查询到有效保单”的日志失败！");
				}
			} else if (1 == tLKTransStatusSet.size()) {	//银保双方都认为承保成功
				LKTransStatusDB ttLKTransStatusDB = tLKTransStatusSet.get(1).getDB();
				
   			//核对保费
			//add by wangxt in 2009-4-24 begin	
			if (ttLKTransStatusDB.getRiskCode().equals("331301")) {
				LCPolDB tLCPolDB = new LCPolDB();
				LCPolSchema tLCPolSchema = new LCPolSchema();
				tLCPolDB.setContNo(tContNo);
				tLCPolDB.setRiskCode("331301");
				LCPolSet tLCPolSet = tLCPolDB.query();
				if (tLCPolSet.size() != 1) {
					throw new MidplatException("与银行明细对账，查询LCPol表失败,保单号: " + tContNo);
				} else {
					tLCPolSchema = tLCPolSet.get(1);
					double tSumAmnt = ttLKTransStatusDB.getTransAmnt();
					tSumAmnt = tSumAmnt + tLCPolSchema.getSupplementaryPrem();
					ttLKTransStatusDB.setTransAmnt(tSumAmnt);
				}
			}
			//add by wangxt in 2009-4-24 end
			
   			if (Math.abs(tPrem-ttLKTransStatusDB.getTransAmnt()) < 0.0000000000001) {	//保费一致，状态为00
   				ttLKTransStatusDB.setRBankVSMP("00");
   				ttLKTransStatusDB.setDesBankVSMP("数据一致，对账成功！");
   				ttLKTransStatusDB.setState_Code("02");
   				ttLKTransStatusDB.setModifyDate(cCurDate);
   				ttLKTransStatusDB.setModifyTime(cCurTime);
   			} else {		//保费不一致，状态为11
   				ttLKTransStatusDB.setRBankVSMP("01");
					ttLKTransStatusDB.setDesBankVSMP("承保状态，保费不一致！");
					ttLKTransStatusDB.setState_Code("02");
					ttLKTransStatusDB.setModifyDate(cCurDate);
					ttLKTransStatusDB.setModifyTime(cCurTime);
   			}
				cLogger.info("ContNo = " + tContNo + "； RBankVSMP = " + ttLKTransStatusDB.getRBankVSMP() 
						+ "； DesBankVSMP = " + ttLKTransStatusDB.getDesBankVSMP());
				if (!ttLKTransStatusDB.update()) {
					cLogger.error(ttLKTransStatusDB.mErrors.getFirstError());
					throw new MidplatException("与银行明细对账，更新对账信息失败！");
				}
			} else {
				throw new MidplatException("银保通日志信息有误！");
			}
		}
		/**End-处理银行认为成功承保的保单************************/
		
		/**Start-处理银保通认为成功，银行认为无效的保单***********/
		//查询银保通表中该银行、该机构、当日有效，但是银行认为无效的保单
		String mSQL = "select * from LKTransStatus where FuncFlag='01' "
			+ "and Status='1' "
			+ "and BankCode='" + cBankCode + "' "
			+ "and TransDate='" + cTransDate + "' "
			+ "and ManageCom like '" + cManageCom + "%' "
			+ "and State_Code <> '02' "
			+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
			+"'" + cBankCode + "' "
			+"and ManageCom <> '" + cManageCom +"' "
			+"and ManageCom like '" +cManageCom +"%' "
			+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) " 
			+ "with ur";
		cLogger.info("SQL: " + mSQL);
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		LKTransStatusSet mLKTransStatusSet = mLKTransStatusDB.executeQuery(mSQL);
		if (mLKTransStatusSet.mErrors.needDealError()) {
			cLogger.error(mLKTransStatusSet.mErrors.getFirstError());
			throw new MidplatException("查询银保通数据失败！");
		} else if (null == mLKTransStatusSet) {
			cLogger.warn("查询银保通处于成功，银行认为失效的保单，LKTransStatusSet=null！");
			mLKTransStatusSet = new LKTransStatusSet();
		}
		cLogger.info("银保通认为成功，银行认为无效的保单数(YbtNotBank.SignSet)为：" + mLKTransStatusSet.size());
		
		//循环标识银保通认为成功，银行认为无效的保单
		for (int i = 1; i <= mLKTransStatusSet.size(); i++) {
			LKTransStatusDB tLKTransStatusDB = mLKTransStatusSet.get(i).getDB();

			tLKTransStatusDB.setRBankVSMP("03");
			tLKTransStatusDB.setDesBankVSMP("银行认为无效，银保通认为成功！");
			tLKTransStatusDB.setState_Code("02");
			tLKTransStatusDB.setModifyDate(cCurDate);
			tLKTransStatusDB.setModifyTime(cCurTime);
			cLogger.info("ContNo = " + tLKTransStatusDB.getPolNo() 
					+ "； RBankVSMP = " + tLKTransStatusDB.getRBankVSMP() 
					+ "； DesBankVSMP = " + tLKTransStatusDB.getDesBankVSMP());
			if (!tLKTransStatusDB.update()) {
				cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
				throw new MidplatException("与银行明细对账，更新对账信息失败！");
			}
		}
		/**End-处理银保通认为成功，银行认为无效的保单*************/
		
		
		cLogger.info("Out DetailBalanceBL.deal()!");
	}
}
