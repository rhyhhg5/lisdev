package com.sinosoft.midplat.kernel.service.balance;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.writeOff.YbtWriteOffBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class AutoWriteOff {
	private final static Logger cLogger = Logger.getLogger(AutoWriteOff.class);
	
	private final LKTransStatusSchema cLKTransStatusSchema;
	
	public AutoWriteOff(LKTransStatusSchema pLKTransStatusSchema) {
		cLKTransStatusSchema = pLKTransStatusSchema;
	}
 
	public void deal() throws Exception {
		cLogger.info("Into AutoWriteOff.deal()...");
		
		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
				cLKTransStatusSchema.getBankCode(),
				cLKTransStatusSchema.getBankBranch(),
				cLKTransStatusSchema.getBankNode());
		
		YbtWriteOffBL mYbtWriteOffBL = 
			new YbtWriteOffBL(cLKTransStatusSchema.getPolNo(), mGlobalInput);
		mYbtWriteOffBL.deal();
		
		cLKTransStatusSchema.setStatus("2");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		LKTransStatusDB mLKTransStatusDB = cLKTransStatusSchema.getDB();
		if (!mLKTransStatusDB.update()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("更新保单状态失败！");
		}
		
		//回收单证！
		String mCertifyCode = "HB07/07B";
		String mProposalContNoNew = null;
		ExeSQL mExeSQL = new ExeSQL();
		String tSQL = "select certifycode from lkcertifymapping where bankcode = '"
				+ cLKTransStatusSchema.getBankCode()+"' "
				+ "and managecom = '"+ mGlobalInput.ManageCom.substring(0, 4)+"' "
				+ "and RiskWrapCode = '"+ cLKTransStatusSchema.getRiskCode()+"' "//信保通日志表里存的是套餐编码
				+ "with ur";
		
		SSRS tSSRS = mExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() >= 1) {
			mCertifyCode = tSSRS.GetText(1, 1);
			//add by zengzm 2017-01-20 针对河南农信社3位类型码+10位单证处理
			if(mGlobalInput.AgentCom.startsWith("PC41") && "45".equals(cLKTransStatusSchema.getBankCode())){
				mProposalContNoNew = cLKTransStatusSchema.getProposalNo().substring(3);//河南农信社
			}else{
				mProposalContNoNew = cLKTransStatusSchema.getProposalNo().substring(2, 12);//信保通
			}
		}else{
			mProposalContNoNew = cLKTransStatusSchema.getProposalNo();//银保通
		}
		CardManage mCardManage = new CardManage(mCertifyCode, mGlobalInput);
		
		//判断江门邮政渠道 add by zengzm 2016-08-01
		if(mProposalContNoNew.startsWith("AD") && cLKTransStatusSchema.getPrtNo().equals(cLKTransStatusSchema.getProposalNo())){
			cLogger.info("江门邮政渠道不需要进行单证核销!!");
		}else if(mProposalContNoNew.startsWith("56") && cLKTransStatusSchema.getPrtNo().equals(cLKTransStatusSchema.getProposalNo())){    //判断出网银渠道
			cLogger.info("网银渠道不需要进行单证核销!!");
		}else {
			if (!cLKTransStatusSchema.getProposalNo().startsWith("YBT")) {
				mCardManage.tackBack(mProposalContNoNew);
			}
		}
		cLogger.info("Out AutoWriteOff.deal()!");
	}
}
