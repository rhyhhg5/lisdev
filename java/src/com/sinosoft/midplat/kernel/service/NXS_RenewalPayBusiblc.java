package com.sinosoft.midplat.kernel.service;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.finfee.YBTPayConf;
import com.sinosoft.lis.finfee.YBTTempRecoil;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LKBalanceDetailSchema;
import com.sinosoft.lis.vschema.LKBalanceDetailSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.balance.DetailBalanceBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class NXS_RenewalPayBusiblc {

	private final static Logger cLogger = Logger.getLogger(DetailBalanceBL.class);
	
	private String tManageCom;
	private LKTransStatusDB mLKTransStatusDB;
	private LKBalanceDetailSet mLKBalanceDetailSet_x;
	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");
	
	public NXS_RenewalPayBusiblc(String mManageCom,LKTransStatusDB tLKTransStatusDB,LKBalanceDetailSet tLKBalanceDetailSet_x) {
		tManageCom = mManageCom;
		mLKTransStatusDB = tLKTransStatusDB;
		mLKBalanceDetailSet_x = tLKBalanceDetailSet_x;
	}

	public Document deal() throws Exception {
		cLogger.info("Into NXS_RenewalPayBusiblc.deal()...");
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		try {
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(), mLKTransStatusDB
							.getBankBranch(), mLKTransStatusDB.getBankNode());
			
			// 银行与银保通对账
			TransferData tTransferData = new TransferData();
			YBTPayConf tYBTPayConf = null;
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%======"+mLKBalanceDetailSet_x.size());
			for (int i = 1; i <= mLKBalanceDetailSet_x.size(); i++) {
				LKBalanceDetailSchema tLKBalanceDetailSchema = mLKBalanceDetailSet_x.get(i);
				String tContno = tLKBalanceDetailSchema.getCardNo();
				
				//查询该银行、该机构当日有效保单
				String tSQL = "select * from LKTransStatus where FuncFlag='52' "
					+ "and Status='1' and rcode = '1'"
					+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
					+ "and TransDate='" + tLKBalanceDetailSchema.getTranDate() + "' "
					+ "and PolNo='" + tContno + "' "
					+ "and ManageCom like '" + tManageCom + "%' "
					+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
					+"'" + mLKTransStatusDB.getBankCode() + "' "
					+"and ManageCom <> '" + tManageCom +"' "
					+"and ManageCom like '" +tManageCom +"%' "
					+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) " 
					+ "with ur";
				cLogger.info("SQL: " + tSQL);
				LKTransStatusDB tLKTransStatusDB = new LKTransStatusDB();
				LKTransStatusSet tLKTransStatusSet = tLKTransStatusDB.executeQuery(tSQL);
				if (tLKTransStatusDB.mErrors.needDealError()) {
					cLogger.error(tLKTransStatusDB.mErrors.getFirstError());
					throw new MidplatException("查询银保通数据失败！");
				}
				
				//银行有，银保通没有的数据
				if ((null==tLKTransStatusSet) || (0==tLKTransStatusSet.size())) {
					cLogger.info("tContno = "+tContno+"  银行有，银保通没有");
					LKTransStatusDB ttLKTransStatusDB = new LKTransStatusDB();
					ttLKTransStatusDB.setTransNo(PubFun1.CreateMaxNo("YBTNO", 10) + "BLC");
					ttLKTransStatusDB.setBankCode(mLKTransStatusDB.getBankCode());
					ttLKTransStatusDB.setBankBranch(tLKBalanceDetailSchema.getBankZoneCode());
					ttLKTransStatusDB.setBankNode(tLKBalanceDetailSchema.getBrNo());
					String ttSQL = "select ManageCom from LKCodeMapping where 1=1 "
						+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
						+ "and ZoneNo='" + tLKBalanceDetailSchema.getBankZoneCode() + "' " 
						+ "and BankNode='" + tLKBalanceDetailSchema.getBrNo() + "' "
						+ "with ur";
					ttLKTransStatusDB.setManageCom(new ExeSQL().getOneValue(ttSQL));
					ttLKTransStatusDB.setFuncFlag("52");
					ttLKTransStatusDB.setPolNo(tContno);
					ttLKTransStatusDB.setTransAmnt(tLKBalanceDetailSchema.getTranAmnt());
					ttLKTransStatusDB.setTransDate(mLKTransStatusDB.getTransDate());
					ttLKTransStatusDB.setMakeDate(cCurDate);
					ttLKTransStatusDB.setMakeTime(cCurTime);
					ttLKTransStatusDB.setModifyDate(cCurDate);
					ttLKTransStatusDB.setModifyTime(cCurTime);
					ttLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
					ttLKTransStatusDB.setState_Code("04");
					ttLKTransStatusDB.setRBankVSMP("02");
					ttLKTransStatusDB.setDesBankVSMP("未查询到有效保单信息！");
					ttLKTransStatusDB.setbak1("1");
					cLogger.info("ContNo = " + tContno + "； RBankVSMP = " + ttLKTransStatusDB.getRBankVSMP() 
								+ "； DesBankVSMP = " + ttLKTransStatusDB.getDesBankVSMP());
					if (!ttLKTransStatusDB.insert()) {
						cLogger.error(ttLKTransStatusDB.mErrors.getFirstError());
						throw new MidplatException("与银行明细对账，自动补录“未查询到有效保单”的日志失败！");
					}
				} else if (1 == tLKTransStatusSet.size()) {	//银保双方都认为承保成功
					LKTransStatusDB ttLKTransStatusDB = tLKTransStatusSet.get(1).getDB();
					cLogger.info("tContno = "+tContno+"  银保双方都认为承保成功");
					tTransferData.setNameAndValue("ContNo", tContno);
					tTransferData.setNameAndValue("PayMoney", tLKBalanceDetailSchema.getTranAmnt()+"");
					tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
					
					tYBTPayConf = new YBTPayConf();
					if (!tYBTPayConf.submitData(tTransferData)) {
						// 收费确认失败，可获得返回信息
						CErrors cErrors = new CErrors();
						cErrors = tYBTPayConf.getErrorInf();
						ttLKTransStatusDB.setResultBalance("1");	//汇总对账处理结果标识：0-成功；1-失败
						ttLKTransStatusDB.setDesBalance(cErrors.getLastError());
					}else {
						ttLKTransStatusDB.setState_Code("04");
						ttLKTransStatusDB.setRBankVSMP("00");
						ttLKTransStatusDB.setResultBalance("0");	//汇总对账处理结果标识：0-成功；1-失败
						ttLKTransStatusDB.setDesBalance("数据一致，对账成功！");
					}
					
					cLogger.info("ContNo = " + tContno + "； RBankVSMP = " + ttLKTransStatusDB.getRBankVSMP() 
							+ "； DesBankVSMP = " + ttLKTransStatusDB.getDesBankVSMP());
					if (!ttLKTransStatusDB.update()) {
						cLogger.error(ttLKTransStatusDB.mErrors.getFirstError());
						throw new MidplatException("与银行明细对账，更新对账信息失败！");
					}
					
				}else {
					throw new MidplatException("银保通日志信息有误！");
				}
				
				}
			//  银行没有 银保通与核心有的数据，进行冲正处理，以银行为准。
			String tSQL_c = "select * from LKTransStatus where FuncFlag='52' "
				+ "and Status='1' and rbankvsmp is null "
				+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
				+ "and TransDate='" + mLKTransStatusDB.getTransDate() + "' "
				+ "and ManageCom like '" + tManageCom + "%' "
				+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
				+"'" + mLKTransStatusDB.getBankCode() + "' "
				+"and ManageCom <> '" + tManageCom +"' "
				+"and ManageCom like '" +tManageCom +"%' "
				+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) " 
				+ "with ur";
			cLogger.info("银行没有 银保通与核心有的数据,SQL: " + tSQL_c);
			LKTransStatusDB tLKTransStatusDB_c = new LKTransStatusDB();
			LKTransStatusSet tLKTransStatusSet_c = tLKTransStatusDB_c.executeQuery(tSQL_c);
			for (int j = 1; j <= tLKTransStatusSet_c.size(); j++) {
				LKTransStatusDB ttLKTransStatusDB_c = tLKTransStatusSet_c.get(j).getDB();
				//RBankVSMP 在银行与银保通对账时，都会插入该属性，如果此属性为空，那证明此单没经过上面流程，即银行无此单。
				if("".equals(ttLKTransStatusDB_c.getRBankVSMP()) || ttLKTransStatusDB_c.getRBankVSMP() == null){
					System.out.println("银保通有的数据 =="+tLKTransStatusSet_c.size());
					//如为空，则调取核心冲正接口，将银行认为无效的单子冲掉。
					TransferData tTransferData_c = new TransferData();
					cLogger.info("tContno = "+ttLKTransStatusDB_c.getPolNo()+"  银保通认为成功的数据");
					tTransferData_c.setNameAndValue("ContNo", ttLKTransStatusDB_c.getPolNo());
					tTransferData_c.setNameAndValue("PayMoney", ttLKTransStatusDB_c.getTransAmnt()+"");
					tTransferData_c.setNameAndValue("GlobalInput", mGlobalInput);
					
					YBTTempRecoil tYBTTempRecoil = new YBTTempRecoil();
					
					if (!tYBTTempRecoil.submitData(tTransferData_c)) {
						// 反冲失败，可获得返回信息
						CErrors cErrors = new CErrors();
						cErrors = tYBTTempRecoil.getErrorInf();
						System.out.println(cErrors.getLastError());
						ttLKTransStatusDB_c.setRBankVSMP("03");
						ttLKTransStatusDB_c.setState_Code("04");
						ttLKTransStatusDB_c.setDesBankVSMP(cErrors.getLastError());
					}else{
						ttLKTransStatusDB_c.setRBankVSMP("03");
						ttLKTransStatusDB_c.setDesBankVSMP("银行认为无效，银保通认为成功！");
						ttLKTransStatusDB_c.setState_Code("04");
					}
					ttLKTransStatusDB_c.setModifyDate(cCurDate);
					ttLKTransStatusDB_c.setModifyTime(cCurTime);
					
					if (!ttLKTransStatusDB_c.update()) {
						cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
					}
				}
				
			}
		} catch (Exception ex) {

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；2-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
		}
		
		if (!mLKTransStatusDB.update()) {
			throw new MidplatException("与银行汇总对账，更新对账信息失败！");
		}
		cLogger.info("Out NXS_RenewalPayBusiblc.deal()...");
		return mOutXmlDoc;
	}
}
