package com.sinosoft.midplat.kernel.service;

import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_GetCashDetail extends ServiceImpl  {

	public CCB_GetCashDetail(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_GetCashDetail.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		String mSQL = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");
	
			mSQL = "select 1 from lccont where contno = '" + mLCCont.getChildText("ContNo") + "' and salechnl = '04' and cardflag = '9' with ur";
			if(new ExeSQL().getOneValue(mSQL).equals("1")){	//调用核心接口
				mOutXmlDoc = deal(mLCCont);
			}else {
				throw new MidplatException("不存在该保单信息！");
			}
				
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_GetCashDetail.service()!");
		return mOutXmlDoc;
	}
	
	public Document deal(Element mLCCont) throws MidplatException{
		cLogger.info("Into CCB_GetCashDetail.deal()!");
		String mContNo = mLCCont.getChildText("ContNo");
		String mNowDate = DateUtil.getCurDate("yyyy-MM-dd");
		Element mTranData = null;
		
		PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
		double pSumPrem = mPedorYBTBudgetBL.getZTmoney(mContNo,mNowDate); //核心接口
		if(pSumPrem == 0){
			Document mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", "该保单不存在现价！");
			return mOutXmlDoc;
		}else {
			double mCashAmnt = pSumPrem;
			
			Element pLCCont = new Element("LCCont");
			
			Element pContNo = new Element("ContNo");
			pContNo.setText(mContNo);
			
			Element pCashAmnt = new Element("CashAmnt");
			pCashAmnt.setText(String.valueOf(mCashAmnt));
			
			pLCCont.addContent(pContNo);
			pLCCont.addContent(pCashAmnt);
			
			Element mFlag = new Element("Flag");
			mFlag.setText("1");
			
			Element mDesc = new Element("Desc");
			mDesc.setText("交易成功！");

			Element mRetData = new Element("RetData");
			mRetData.addContent(mFlag);
			mRetData.addContent(mDesc);
			
			mTranData = new Element("TranData");
			mTranData.addContent(mRetData);
			mTranData.addContent(pLCCont);
		}

		cLogger.info("Out CCB_GetCashDetail.deal()!");
		return new Document(mTranData);
		
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_GetCashDetail.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_GetCashDetail.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
}