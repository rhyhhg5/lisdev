package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class AIO_Query extends ServiceImpl {
	public AIO_Query(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into AIO_Query.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			
			
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLKTransStatusDB.getPolNo());
			tLCContDB.setPrtNo(mLKTransStatusDB.getPrtNo());
			tLCContDB.setProposalContNo(mLKTransStatusDB.getProposalNo());
			if ((null==tLCContDB.getContNo() || "".equals(tLCContDB.getContNo()))
				&& (null==tLCContDB.getPrtNo() || "".equals(tLCContDB.getPrtNo()))
				&& (null==tLCContDB.getProposalContNo() || "".equals(tLCContDB.getProposalContNo()))) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if ((null==tLCContSet) || (tLCContSet.size()<1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			//目前农信社走此流程的只有安徽上了银保产品，故在此判断
			if(mLKTransStatusDB.getBankBranch().startsWith("ANHNX")){
				String mCardFlag = tLCContSchema.getCardFlag();
				if (mCardFlag.equals("9")) { // 银保
					mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
							.getBankCode(), mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				} else if (mCardFlag.equals("a")) { // 信保
					mGlobalInput = YbtSufUtil.getGlobalInput1(mLKTransStatusDB
							.getBankCode(), mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				}
			}else{
				mGlobalInput = YbtSufUtil.getGlobalInput(
						mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
			}
			
			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();

			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
			// 辽宁农信社组织关系
			if(mLKTransStatusDB.getBankBranch().startsWith("LINNX")){
				mOutXmlDoc = NXS_NewContInput.setAppnt2Insured(mOutXmlDoc);
				mOutXmlDoc = setContState(mOutXmlDoc);
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out AIO_Query.service()!");
		return mOutXmlDoc;
	}

	private Document setContState(Document mOutXmlDoc) {
		Element mLCCont = mOutXmlDoc.getRootElement().getChild("LCCont");
		String contNo = mLCCont.getChildText("ContNo");
		String sql_state = "select stateflag,appflag from lccont where contno = '"+ contNo +"' with ur";
		SSRS ssrs = new ExeSQL().execSQL(sql_state);
		Element mStateFlag = new Element("StateFlag");
		mStateFlag.setText(ssrs.GetText(1, 1));
		Element mAppFlag = new Element("AppFlag"); 
		mAppFlag.setText(ssrs.GetText(1, 2));
		
		mLCCont.addContent(mStateFlag);
		mLCCont.addContent(mAppFlag);
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into AIO_Query.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		String sBankCodeString = mBaseInfo.getChildText(BankCode);
		if("60".equals(sBankCodeString)){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
			
		}
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out AIO_Query.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "F:/working/picch/std/testXml/Query/std_09_in.xml";
		String mOutFile = "F:/working/picch/std/testXml/Query/std_09_out.xml";

		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new AIO_Query(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
	}
}
