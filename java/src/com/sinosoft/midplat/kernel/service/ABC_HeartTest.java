package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class ABC_HeartTest extends ServiceImpl {
	
	public ABC_HeartTest(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_HeartTest.service()...");
		Document mOutXmlDoc = null;
		
		try {
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out ABC_HeartTest.service()!");
		return mOutXmlDoc;
	}
	
}