package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class AIO_RePrint extends ServiceImpl {
	public AIO_RePrint(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into AIO_RePrint.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		String mOriginTransNo = "";
		Element mBaseInfo = pInXmlDoc.getRootElement().getChild(BaseInfo);
		
		//add by zengzm 2016-01-06 针对兴业银行不传保单号
		if(mBaseInfo.getChildTextTrim("BankCode").equals("13") || "78".equals(mBaseInfo.getChildTextTrim("BankCode"))){
			String sql = "select contno from lccont where prtno='"+pInXmlDoc.getRootElement().getChild(LCCont).getChildTextTrim(PrtNo)+"'";
			cLogger.info("sql:"+sql);
			pInXmlDoc.getRootElement().getChild(LCCont).getChild(ContNo).setText(new ExeSQL().getOneValue(sql));
		}
		//针对福建农信社不传ZoneNo情况进行特殊处理 add by gaojinfu 2017-06-01
		if("69".equals(mBaseInfo.getChildText(BankCode)) || "78".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String mZoneNo = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild(ZoneNo).setText(mZoneNo);
		}
		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLCCont.getChildText(ContNo));
			tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
			
			if("03".equals(mLKTransStatusDB.getBankCode()) || "13".equals(mLKTransStatusDB.getBankCode()) || "02".equals(mLKTransStatusDB.getBankCode())|| "10".equals(mLKTransStatusDB.getBankCode())){	
				String pSQL = "select transno from lktransstatus where funcflag = '00' and  polno = '" + mLCCont.getChildText(ContNo) + "' and makedate = Current Date  and descr is null with ur";
				mOriginTransNo = new ExeSQL().getOneValue(pSQL);
				System.out.println(mOriginTransNo);
			}
			
			if("16".equals(mLKTransStatusDB.getBankCode())||"04".equals(mLKTransStatusDB.getBankCode())){	
				String pSQL = "select transno from lktransstatus where funcflag = '00' and  PrtNo = '" + mLCCont.getChildText(PrtNo) + "' and makedate = Current Date  and descr is null with ur";
				mOriginTransNo = new ExeSQL().getOneValue(pSQL);
				System.out.println(mOriginTransNo);
			}
			
			if("57".equals(mLKTransStatusDB.getBankCode())){	
				String pSQL = "select transno from lktransstatus where funcflag = '01' and  PrtNo = '" + mLCCont.getChildText(PrtNo) + "' and makedate = Current Date  and descr is null with ur";
				mOriginTransNo = new ExeSQL().getOneValue(pSQL);
				System.out.println("流水号:" + mOriginTransNo);
			}
			
			//农行传入的是保单印刷号
			if("04".equals(mLKTransStatusDB.getBankCode())){
				String pSQL = "select polno from lktransstatus where bankcode = '04' and prtno = '" + mLCCont.getChildText(PrtNo) + "' and makedate = Current Date and funcflag = '01' and descr is null with ur";
				String mAbcPrtNo = new ExeSQL().getOneValue(pSQL);
				tLCContDB.setContNo(mAbcPrtNo);
			}else {
				tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
			}
			
			if (tLCContDB.getContNo().equals("")
					&& tLCContDB.getPrtNo().equals("")) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if ((null==tLCContSet) || (tLCContSet.size()<1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			if (!tLCContSchema.getAppFlag().equals("1")) {
				throw new MidplatException("该保单不处于承保状态！");
			}

			if (!tLCContSchema.getSignDate().equals(
					DateUtil.getCurDate("yyyy-MM-dd"))) {
				throw new MidplatException("非当日保单，不能重打！");
			}
			
			String tCertifyCode = null;
			String tProposalContNoNew = null;
			String tProposalContNoOld = null; //原单证号码
			if ("a".equals(tLCContSchema.getCardFlag())) {
				 
				//目前农信社走此流程的只有安徽上了银保产品，故在此判断
				if(mLKTransStatusDB.getBankBranch().startsWith("ANHNX")){
					mGlobalInput = YbtSufUtil.getGlobalInput1(
							mLKTransStatusDB.getBankCode(),
							mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				}else {
					mGlobalInput = YbtSufUtil.getGlobalInput(
							mLKTransStatusDB.getBankCode(),
							mLKTransStatusDB.getBankBranch(),
							mLKTransStatusDB.getBankNode());
				}
				     
				String mSQL = "select RiskCode from LKTransStatus where PolNo = '"
						+ tLCContSchema.getContNo()+ "' "
						+ "and BankCode = '"+tLCContSchema.getBankCode()+"' "
						+ "and FuncFlag = '01' and Status = '1' with ur";
				String mRiskWrapCode = new ExeSQL().getOneValue(mSQL);//信保通riskcode字段存储的是套餐编码
				// 查询得到单证编码
				ExeSQL mExeSQL = new ExeSQL();
				String tSQL = "select certifycode from lkcertifymapping where bankcode = '"
						+ tLCContSchema.getBankCode()+ "' "
						+ "and managecom = '"+ mGlobalInput.ManageCom.substring(0, 4)+ "' "
						+ "and RiskWrapCode = '" + mRiskWrapCode + "' " 
						+ "with ur";

				SSRS tSSRS = mExeSQL.execSQL(tSQL);
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的单证编码!");
				}
				tCertifyCode = tSSRS.GetText(1, 1);
				//农信社必须取后十位,泉州银行直接取
				if(mGlobalInput.ManageCom.startsWith("8635") && mGlobalInput.AgentCom.startsWith("PY053")){
					tProposalContNoNew = mLCCont.getChildText(ProposalContNo); 
					tProposalContNoOld = tLCContSchema.getProposalContNo(); //原单证号
				}  
				else  if(mGlobalInput.ManageCom.startsWith("8644") && pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText(BankCode).equals("63"))
				{
					tProposalContNoNew = mLCCont.getChildText(ProposalContNo).substring(2);
					tProposalContNoOld = tLCContSchema.getProposalContNo().substring(2); //原单证号
				}
				else  if(mGlobalInput.ManageCom.startsWith("8621") && pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText(BankCode).equals("45"))
				{
					tProposalContNoNew = mLCCont.getChildText(ProposalContNo).substring(3);
					tProposalContNoOld = tLCContSchema.getProposalContNo().substring(3); //原单证号
				}
				else  if("78".equals(mBaseInfo.getChildTextTrim("BankCode")))
				{
					tProposalContNoNew = mLCCont.getChildText(ProposalContNo).substring(3);
					tProposalContNoOld = tLCContSchema.getProposalContNo().substring(3);
				}
				else  if("80".equals(mBaseInfo.getChildTextTrim("BankCode")))//临商银行新单证ALW处理  add by gcy 20180920
				{
					tProposalContNoNew = mLCCont.getChildText(ProposalContNo).substring(3);
					tProposalContNoOld = tLCContSchema.getProposalContNo().substring(3);
				}
				else{
					tProposalContNoNew = mLCCont.getChildText(ProposalContNo).substring(2,12);
					tProposalContNoOld = tLCContSchema.getProposalContNo().substring(2,12); //原单证号
				}
			} else if ("9".equals(tLCContSchema.getCardFlag())) {
				
				mGlobalInput = YbtSufUtil.getGlobalInput(
						mLKTransStatusDB.getBankCode(),
						mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());
				
				tCertifyCode = "HB07/07B";
				tProposalContNoNew = mLCCont.getChildText(ProposalContNo);
				tProposalContNoOld = tLCContSchema.getProposalContNo(); //原单证号
			}
			CardManage tCardManage = new CardManage(tCertifyCode, mGlobalInput);
			if (!mLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
				if (!tCardManage.canBeUsed(tProposalContNoNew)) {
					throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				}
			}
			
			tLCContDB.setSchema(tLCContSchema);
			tLCContDB.setProposalContNo(mLCCont.getChildText(ProposalContNo));
			tLCContDB.setPrintCount(tLCContSchema.getPrintCount() + 1);
			if (!tLCContDB.update()) {
				throw new MidplatException("更新保单印刷号失败！");
			}
			
			//核销新单证！
			if (!tLCContDB.getProposalContNo().startsWith("YBT")) {
				tCardManage.makeUsed(tProposalContNoNew);
			}
			//作废原单证！
			if (!tLCContSchema.getProposalContNo().startsWith("YBT")) {
				tCardManage.invalidated(tProposalContNoOld);
			}
			
			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();

			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setbak1(tLCContSchema.getProposalContNo());
			mLKTransStatusDB.setbak2(tLCContSchema.getPrintCount() + "");
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			
			// 辽宁农信社组织关系
			if(mLKTransStatusDB.getBankBranch().startsWith("LINNX")){
				mOutXmlDoc = NXS_NewContInput.setAppnt2Insured(mOutXmlDoc);
			}
			
			if(mLKTransStatusDB.getBankCode().equals("03") || "13".equals(mLKTransStatusDB.getBankCode())){	//建行
				
				Element pTranData = mOutXmlDoc.getRootElement();
				Element pLCCont = pTranData.getChild(LCCont);
				
//				String mSQL = "select bak2,bak3,bak4 from lktransstatus where transno = '" + mOriginTransNo + "' and bankcode = '03' and makedate = Current Date and descr is null with ur";
				String mSQL = "select bak2,bak3,bak4,bak1 from lktransstatus where transno = '" + mOriginTransNo + "' and makedate = Current Date and descr is null with ur";
				SSRS ssrs= new ExeSQL().execSQL(mSQL);
				String pRckrNo = ssrs.GetText(1, 1);
				String pSaleName = ssrs.GetText(1, 2);
				String pSaleCertNo = ssrs.GetText(1, 3);
				String pBrName = ssrs.GetText(1, 4);
				
				Element mBrName = new Element("BrName").setText(pBrName);
				Element mRckrNo = new Element("RckrNo").setText(pRckrNo);
				Element mSaleName = new Element("SaleName").setText(pSaleName);
				Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
				
				pLCCont.addContent(mBrName);
				pLCCont.addContent(mRckrNo);
				pLCCont.addContent(mSaleName);
				pLCCont.addContent(mSaleCertNo);
			}
			
			//中行三号文要求增加出单网点名称、销售人员工号、负责人姓名、网点保险兼业代理业务许可证编码
			if(mLKTransStatusDB.getBankCode().equals("02")||mLKTransStatusDB.getBankCode().equals("04")){
				
				Element pTranData = mOutXmlDoc.getRootElement();
				Element pLCCont = pTranData.getChild(LCCont);
				
				String mSQL = "select bak1,bak2,bak3,bak4 from lktransstatus where transno = '" + mOriginTransNo + "' and bankcode = '" + mLKTransStatusDB.getBankCode() + "' and makedate = Current Date and descr is null with ur";
				SSRS ssrs= new ExeSQL().execSQL(mSQL);
				String pBrName = ssrs.GetText(1, 1);
				String pBrCertID = ssrs.GetText(1, 2);
				String pManagerNo = ssrs.GetText(1, 3);
				String pManagerName = ssrs.GetText(1, 4);
				
				Element mBrName = new Element("BrName").setText(pBrName);
				Element mBrCertID = new Element("BrCertID").setText(pBrCertID);
				Element mManagerNo = new Element("ManagerNo").setText(pManagerNo);
				Element mManagerName = new Element("ManagerName").setText(pManagerName);
				
				pLCCont.addContent(mBrName);
				pLCCont.addContent(mBrCertID);
				pLCCont.addContent(mManagerNo);
				pLCCont.addContent(mManagerName);
			}
			
			//邮储三号文要求增加出单网点名称、销售人员工号
			if(mLKTransStatusDB.getBankCode().equals("16")){
				
				Element pTranData = mOutXmlDoc.getRootElement();
				Element pLCCont = pTranData.getChild(LCCont);
				
				String mSQL = "select bak3,bak4 from lktransstatus where transno = '" + mOriginTransNo + "' and bankcode = '16' and makedate = Current Date and descr is null with ur";
				SSRS ssrs= new ExeSQL().execSQL(mSQL);
				String pBrName = ssrs.GetText(1, 1);
				String pSaleCertNo = ssrs.GetText(1, 2);
				Element mRckrNo = new Element("BrName").setText(pBrName);
				Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
				
				pLCCont.addContent(mRckrNo);
				pLCCont.addContent(mSaleCertNo);
			}	

			 /**
			 * 江苏银行三号文要求增加销售人员工号、销售人员姓名、销售人员资格证号 
			 * add by LiXiaoLong 20140506 begin
			 * **/
			if(mLKTransStatusDB.getBankCode().equals("57")){
				
				Element pTranData = mOutXmlDoc.getRootElement();
				Element pLCCont = pTranData.getChild(LCCont);
				
				String mSQL = "select bak1,bak2,bak4 from lktransstatus where transno = '" + mOriginTransNo + "' and bankcode = '57' and makedate = Current Date and descr is null with ur";
				SSRS ssrs= new ExeSQL().execSQL(mSQL);
				String pBrName = ssrs.GetText(1, 1);
				String pSaleCertNo = ssrs.GetText(1, 2);
				String pSaleCertName = ssrs.GetText(1, 3);
				Element mBrName = new Element("BrName").setText(pBrName);
				Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
				Element mSaleCertName = new Element("SaleCertName").setText(pSaleCertName);
				
				pLCCont.addContent(mBrName);
				pLCCont.addContent(mSaleCertNo);
				pLCCont.addContent(mSaleCertName);
			}
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out AIO_RePrint.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into AIO_RePrint.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		if("69".equals(mBaseInfo.getChildText(BankCode)) || "78".equals(mBaseInfo.getChildText(BankCode))){
			String zSQL = "select zoneno from lkcodemapping where bankcode = '"+mBaseInfo.getChildText(BankCode) +"' and banknode = '" +mBaseInfo.getChildText(BrNo) + "'";
			String ZoneNos = new ExeSQL().getOneValue(zSQL);
			mBaseInfo.getChild("ZoneNo").setText(ZoneNos);
		}

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out AIO_RePrint.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "C:/Documents and Settings/Administrator/桌面/16_4245959_02_94841.xml";
		String mOutFile = "F:/working/picch/std/testXml/RePrint/std_02_out.xml";

		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new AIO_RePrint(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
	}
}
