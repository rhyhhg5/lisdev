package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CCB_QueryDetail extends ServiceImpl  {

	public CCB_QueryDetail(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_QueryDetail.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mFamilysalary = null;
		Element mCountytype = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLKTransStatusDB.getPolNo());
			tLCContDB.setPrtNo(mLKTransStatusDB.getPrtNo());
			tLCContDB.setProposalContNo(mLKTransStatusDB.getProposalNo());
			if ((null==tLCContDB.getContNo() || "".equals(tLCContDB.getContNo()))
				&& (null==tLCContDB.getPrtNo() || "".equals(tLCContDB.getPrtNo()))
				&& (null==tLCContDB.getProposalContNo() || "".equals(tLCContDB.getProposalContNo()))) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if ((null==tLCContSet) || (tLCContSet.size()<1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());

			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();
			
			//增加需要返回节点内容
			String tSQL = "select salary from lcappnt where contno='" + mLKTransStatusDB.getPolNo() + "' with ur";
			Element mSalary =  new Element("Salary");
			mSalary.setText(new ExeSQL().getOneValue(tSQL));

//			LCContSubDB tLCContSubDB = new LCContSubDB();
//			tLCContSubDB.setPrtNo(mLKTransStatusDB.getPrtNo());
//			if(!tLCContSubDB.getInfo()){
//				throw new MidplatException("查询保单家庭年收入失败！");
//			} else {
//				mFamilysalary =  new Element("Familysalary");
//				mFamilysalary.setText(Double.toString(tLCContSubDB.getFamilySalary()));
//				mCountytype =  new Element("Countytype");
//				mCountytype.setText(tLCContSubDB.getCountyType());
//			}
			
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			
			String mSQL = "select bak2,bak3,bak4 from lktransstatus where polno = '" + mLKTransStatusDB.getPolNo() + "' and bankcode = '03' and funcflag = '01' and descr is null with ur";
			SSRS ssrs= new ExeSQL().execSQL(mSQL);
			if(ssrs.MaxRow > 0){
				String pRckrNo = ssrs.GetText(1, 1);
				String pSaleName = ssrs.GetText(1, 2);
				String pSaleCertNo = ssrs.GetText(1, 3);
				Element mRckrNo = new Element("RckrNo").setText(pRckrNo);
				Element mSaleName = new Element("SaleName").setText(pSaleName);
				Element mSaleCertNo = new Element("SaleCertNo").setText(pSaleCertNo);
			
				tOutLCCont.addContent(mRckrNo);
				tOutLCCont.addContent(mSaleName);
				tOutLCCont.addContent(mSaleCertNo);
			}

			tOutLCCont.getChild("LCAppnt").addContent(mSalary);
//			tOutLCCont.getChild("LCAppnt").addContent(mFamilysalary);
//			tOutLCCont.getChild("LCAppnt").addContent(mCountytype);
			
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_QueryDetail.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_QueryDetail.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_QueryDetail.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
