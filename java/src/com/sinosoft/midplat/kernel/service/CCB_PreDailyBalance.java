package com.sinosoft.midplat.kernel.service;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LPYBTAppWTDB;
import com.sinosoft.lis.vschema.LPYBTAppWTSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_PreDailyBalance extends ServiceImpl {
	
	private String cBalanceNum = null;	//对账次数
	
	public CCB_PreDailyBalance(Element cThisBusiConf) {
		super(cThisBusiConf);
	}
	
	
	/**
	 * 建行对账有其独特的地方，暂时先按标准流程走，将来再完善。
	 */
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_PreDailyBalance.getStdXml()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		String mManageCom = null;
		
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		//获取对账次数，设置cBalanceNum
		String mSQL = "select count(1)+1 from LKTransStatus where 1=1 "
			+ "and BankCode='" + mBaseInfo.getChildText(BankCode) + "' "
			+ "and BankBranch='" + mBaseInfo.getChildText(ZoneNo) + "' "
			+ "and BankNode='" + mBaseInfo.getChildText(BrNo) + "' "
			+ "and FuncFlag='" + mBaseInfo.getChildText(FunctionFlag) + "' "
			+ "and TransDate='" + DateUtil.date8to10(mBaseInfo.getChildText(BankDate)) + "' ";
		cBalanceNum = new ExeSQL().getOneValue(mSQL + "with ur");
		cLogger.debug("对账次数：BalanceNum = " + cBalanceNum);
		
		/**
		 * 同一天、同一网点前置机自动生成的流水号相同，
		 * 为防止多次重复对账时交易流水号重复，
		 * 在此重新设置流水号：TransrNo + "." + BalanceNum
		 */
		mBaseInfo.getChild(TransrNo).setText(
				mBaseInfo.getChildText(TransrNo) + "." + cBalanceNum);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mSQL += "and RCode = '1' with ur";
			String mCount = new ExeSQL().getOneValue(mSQL);
			if (mCount.equals("2")) {
				throw new MidplatException("只允许对一次帐！");
			}
			
			mSQL = "select ManageCom from LKCodeMapping where Remark5='1' "
				+ "and BankCode='" + mLKTransStatusDB.getBankCode() + "' "
				+ "and ZoneNo='" + mLKTransStatusDB.getBankBranch() + "' "
				+ "and BankNode='" + mLKTransStatusDB.getBankNode() + "' "
				+ "with ur";
			mManageCom = new ExeSQL().getOneValue(mSQL);
			if ((null==mManageCom) || mManageCom.equals("")) {
				throw new MidplatException("未查到该对帐网点！");
			}
			
			//处理前置机传过来的报错信息(扫描超时等)
			String tErrorStr = mTranData.getChildText("Error");
			if (null != tErrorStr) {
				throw new MidplatException(tErrorStr);
			}
			
		     /**
		     * 为防止多次重复对账后日志表信息混乱，每次对账前清理一次对账信息相关字段
		     */
			mSQL = "update lktransstatus set state_code=null, rbankvsmp=null, desbankvsmp=null, "
				+ "rmpvskernel=null, desmpvskernel=null, resultbalance=null, desbalance=null "
				+ "where transdate='" + mLKTransStatusDB.getTransDate() + "' "
				+ "and bankcode='" + mLKTransStatusDB.getBankCode() + "' "
				+ "and ManageCom like '" + mManageCom + "%' "
				+"and not exists(select 1 from LKCodeMapping where Remark5 = '1' and BankCode ="
				+"'" + mLKTransStatusDB.getBankCode() + "' "
				+"and ManageCom <> '" + mManageCom +"' "
				+"and ManageCom like '" +mManageCom +"%' "
				+"and 1=LOCATE(trim(ManageCom),LKTransStatus.ManageCom)) "  
				+ "with ur";
			
			new ExeSQL().execUpdateSQL(mSQL);
			
			mOutXmlDoc = deal(pInXmlDoc);	//处理对账
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("1", "对账成功");
		
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；2-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		}catch (Exception ex) {
			cLogger.error("保全对账(CCB_PreDailyBalance)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；2-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setManageCom(mManageCom);
			mLKTransStatusDB.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_PreDailyBalance.service()!");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_PreDailyBalance.deal()!");
		String pSQL = null;
		Element mChkDetails = pInXmlDoc.getRootElement().getChild(ChkDetails);
		
		String pCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String pCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		List mChkDetail = mChkDetails.getChildren(ChkDetail);
		cLogger.info("银行认为成功保单数：" + mChkDetail.size());
		for(int i = 0;i < mChkDetail.size(); i++){
			Element pChkDetail = (Element) mChkDetail.get(i);
			
			String mBankDate = DateUtil.date8to10(pChkDetail.getChildText("TranDate"));
		
			//保全类对账
			if(pChkDetail.getChildText("FuncFlag").equals("121")){
				
				//与银保通对账
				pSQL = "select 1 from lktransstatus where bankcode = '03' and bankbranch = '"
					+ pChkDetail.getChildText("BankZoneCode")
					+ "' and banknode = '"
					+ pChkDetail.getChildText("BrNo")
					+ "' and polno = '"
					+ pChkDetail.getChildText("ContNo")
					+ "' and funcflag = '121' and makedate = '"
					+ mBankDate
					+ "' and descr is null with ur";
				
				if(!new ExeSQL().getOneValue(pSQL).equals("1")){	//银行认为成功，银保通认为不成功 补录银保通日志	
					LKTransStatusDB mLKTransStatusDB = insertTransLogQB(pChkDetail);
					if(mLKTransStatusDB == null){
						cLogger.info("插入日志异常！");
					}
				}
				
				String nlksSQL3 = "update lktransstatus set modifydate = '" + pCurrentDate + "',modifytime = '" + pCurrentTime + "', RBankVSMP = '01',DesBankVSMP = '"
				+ "保全表中无该保单信息,补录保单信息失败！"
				+ "'  where bankcode = '03' and funcflag = '121' and polno = '"
				+ pChkDetail.getChildText("ContNo")
				+ "'  and modifydate ='" + mBankDate
				+ "' and descr is null with ur";

				pSQL = "select 1 from LPYBTAppWT where contno = '"+ pChkDetail.getChildText("ContNo") +"' with ur";
				if(!new ExeSQL().getOneValue(pSQL).equals("1")){	//银行保全对账文件保单信息 故补录保全表中保单信息
					LPYBTAppWTDB pLPYBTAppWTDB = insertLPYBTAppWT(pChkDetail);
					if(pLPYBTAppWTDB == null){
						new ExeSQL().getOneValue(nlksSQL3);
						cLogger.info("保全表中无该保单信息，补录保单信息失败！");
					}
				}
					
				String pSQL1 = "select TransStatus  from LPYBTAppWT where contno = '"
					+ pChkDetail.getChildText("ContNo") + "' with ur";
				
				if(new ExeSQL().getOneValue(pSQL1).equals("1")){//查看保全表保单是否已经对账
					
					// 更新保全表对账成功状态set TransStatus = '2',
					String mSQL = "update LPYBTAppWT set ModifyDate = '" + pCurrentDate
							+ "',ModifyTime = '" + pCurrentTime
							+ "',TransStatus = '2' where contno = '"
							+ pChkDetail.getChildText("ContNo") + "' with ur";
					
					String nlksSQL = "update lktransstatus set modifydate = '"
						+ pCurrentDate
						+ "',modifytime = '"
						+ pCurrentTime
						+ "', RBankVSMP = '00', DesBankVSMP = '数据一致,对账成功', transstatus = '1',status = '1',state_code = '03' ,rmpvskernel = '00',resultbalance = '0' ,RCode = '1'  where bankcode = '03' and funcflag = '121' and polno = '"
						+ pChkDetail.getChildText("ContNo") + "' and makedate = '"
						+ mBankDate + "' and descr is null with ur";
					
					new ExeSQL().execUpdateSQL(mSQL);//更新保全表保单对账状态成功		
					new ExeSQL().execUpdateSQL(nlksSQL);//更新日志表 保全对账成功
					
				}else if(new ExeSQL().getOneValue(pSQL1).equals("2")){	//这种情况一般不会发生
					cLogger.info("该保单已经对账成功！");
				}else{//这种情况一般不会发生
					cLogger.info("该保单已经对账失败！");
				}
			}else if("33".equals(pChkDetail.getChildText("FuncFlag"))){
				//与银保通对账
				pSQL = "select 1 from lktransstatus where bankcode = '03' and bankbranch = '"
					+ pChkDetail.getChildText("BankZoneCode")
					+ "' and banknode = '"
					+ pChkDetail.getChildText("BrNo")
					+ "' and polno = '"
					+ pChkDetail.getChildText("ContNo")
					+ "' and funcflag = '33' and makedate = '"
					+ mBankDate
					+ "' and descr is null with ur";
				
				if(!new ExeSQL().getOneValue(pSQL).equals("1")){	//银行认为成功，银保通认为不成功 补录银保通日志	
					LKTransStatusDB mLKTransStatusDB = insertTransLogQB(pChkDetail);
					if(mLKTransStatusDB == null){
						cLogger.info("插入日志异常！");
					}
				}
				
				String nlksSQL = "update lktransstatus set modifydate = '"
					+ pCurrentDate
					+ "',modifytime = '"
					+ pCurrentTime
					+ "', RBankVSMP = '00', DesBankVSMP = '数据一致,对账成功', transstatus = '1',status = '1',state_code = '03' ,rmpvskernel = '00' ,DesBankVSMP='与核心数据一致', resultbalance = '0' ,RCode = '1'  where bankcode = '03' and funcflag = '33' and polno = '"
					+ pChkDetail.getChildText("ContNo") + "' and makedate = '"
					+ mBankDate + "' and descr is null with ur";
				
				new ExeSQL().execUpdateSQL(nlksSQL);
				
			}else{
				//其他保全类 对账包括 修改保单信息的对账 可调用核心保全回退接口 暂未开发
			
			}

		}
		
		cLogger.info("Out CCB_PreDailyBalance.deal()!");
		return pInXmlDoc;	
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_PreDailyBalance.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		/**
		 * 如果前置机扫描超时，系统当前时间可能已经是第二天0点，
		 * 此处(TransDate)不取系统当前时间，改用前置机传过来的时间。
		 * 前置机必须保证TranData/BaseInfo/BankDate中的时间的正确性，
		 * 如果是ftp方式对账，BankDate应为本轮扫描的开始时间；
		 * 如果是交易方式，需要银行将此字段传为白天交易的时间。
		 */
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_PreDailyBalance.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	private LKTransStatusDB insertTransLogQB(Element nChkDetail) throws MidplatException {
		cLogger.info("Into CCB_PreDailyBalance.insertTransLogQB()...");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode("03");
		mLKTransStatusDB.setBankBranch(nChkDetail.getChildText(BankZoneCode));
		mLKTransStatusDB.setBankNode(nChkDetail.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator("ybt");
		//银行保全对账不传流水号故 日期 + 保单号
		String mTransrNo = DateUtil.date10to8(mCurrentDate)+ nChkDetail.getChildText(ContNo);
		mLKTransStatusDB.setTransNo(mTransrNo);
		if("33".equals(nChkDetail.getChildText("FuncFlag"))){
			mLKTransStatusDB.setFuncFlag("33");
		}else{
			mLKTransStatusDB.setFuncFlag("121");
		}
		mLKTransStatusDB.setTransDate(DateUtil.date10to8(nChkDetail.getChildText(TranDate)));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(nChkDetail.getChildText("ContNo"));
		mLKTransStatusDB.setProposalNo(nChkDetail.getChildText("CardNo"));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			cLogger.info("补录保全信息时插入日志异常！");
		}

		cLogger.info("Out CCB_PreDailyBalance.insertTransLogQB()!");
		return mLKTransStatusDB;
	}
	
	private LPYBTAppWTDB insertLPYBTAppWT(Element mChkDetail) throws MidplatException {
		cLogger.info("Into CCB_PreDailyBalance.insertLPYBTAppWT()...");
		
		LPYBTAppWTSet mLPYBTAppWTSet = new LPYBTAppWTSet();
		LPYBTAppWTDB mLPYBTAppWTDB = new LPYBTAppWTDB();
		String mContNo = mChkDetail.getChildText("ContNo");
		String mBaoQuanFlag = "";
		
		String pd_sql = "select case when customgetpoldate is null and days(current date) - days(cvalidate)<=30 then '1'when customgetpoldate is not null and days(current date) - days(customgetpoldate)<15 then '1'else '0' end from lccont where contno='" + mContNo + "' with ur";
		if(new ExeSQL().getOneValue(pd_sql).equals("1")){
			mBaoQuanFlag = "WT";
		}else {
			mBaoQuanFlag = "CT";
		}

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTSet = mLPYBTAppWTDB.query();
		
		if(mLPYBTAppWTSet.size() > 0){	//判断保单是否已存在犹豫期退保表中！
			String mSQL = "update lktransstatus set descr = '该保单已存在保全表中，请核查！' where polno = '" + mContNo + "'and funcflag = '56' and makedate = Current Date with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if (!mExeSQL.execUpdateSQL(mSQL)) {
				cLogger.error(mExeSQL.mErrors.getFirstError());
				cLogger.warn("该保单已存在保全表中，请核查！");
			}
		}
		
		mLPYBTAppWTDB.setContNo(mContNo);
		mLPYBTAppWTDB.setManageCom(mChkDetail.getChildText("ContNo"));
		mLPYBTAppWTDB.setAppWTDate(mCurrentDate);
		mLPYBTAppWTDB.setAppntName(mChkDetail.getChildText("AppntName"));
		mLPYBTAppWTDB.setAppntSex("");	//银行不传
		mLPYBTAppWTDB.setAppntBirthday("");//银行不传
		mLPYBTAppWTDB.setAppntIDType(mChkDetail.getChildText("AppntIDType"));
		mLPYBTAppWTDB.setAppntIDNo(mChkDetail.getChildText("AppntIDNo"));
		mLPYBTAppWTDB.setGBFee("0");	//保险公司定为0元
		mLPYBTAppWTDB.setAppState("1");		//需要新的状态标识
		mLPYBTAppWTDB.setoperator("ybt");
		mLPYBTAppWTDB.setTransStatus("1");	//未对账
		mLPYBTAppWTDB.setEdorType(mBaoQuanFlag);	//保全类别 WT-犹豫期退保 CT-解约
		mLPYBTAppWTDB.setMakeDate(mCurrentDate);
		mLPYBTAppWTDB.setMakeTime(mCurrentTime);
		mLPYBTAppWTDB.setModifyDate(mCurrentDate);
		mLPYBTAppWTDB.setModifyTime(mCurrentTime);
		
		if (!mLPYBTAppWTDB.insert()) {
			cLogger.error(mLPYBTAppWTDB.mErrors.getFirstError());
			cLogger.info("插入保全表失败！");
		}
		cLogger.info("Out CCB_PreDailyBalance.insertLPYBTAppWT()...");
		return mLPYBTAppWTDB;
	}

	
}


