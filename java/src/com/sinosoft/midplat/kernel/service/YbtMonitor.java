package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class YbtMonitor extends ServiceImpl {
	public YbtMonitor(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into YbtMonitor.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;

		try {

			ExeSQL tExeSQL = new ExeSQL();
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			Element tTranData = new Element("TranData");
			Element tRetData = new Element("RetData");
			Element tTimeCostDetail = null;
			List tTimes = pInXmlDoc.getRootElement().getChild("BaseInfo")
					.getChildren("Time");
			String[] tTime = null;
			String[] tBankcode = { "01", "02", "03", "04", "10", "16" };
			for (int i = 0; i < tBankcode.length; i++) {
				System.out.println("tTime" + tTimes.size());
				String tExeSql = "";
				SSRS tSSRS = null;
				String tDesc = "";
				String tBankName = "";
				if ("01".equals(tBankcode[i])) {
					tBankName = "工行";
				}
				if ("02".equals(tBankcode[i])) {
					tBankName = "中行";
				}
				if ("03".equals(tBankcode[i])) {
					tBankName = "建行";
				}
				if ("04".equals(tBankcode[i])) {
					tBankName = "农行";
				}
				if ("10".equals(tBankcode[i])) {
					tBankName = "交行";
				}
				if ("16".equals(tBankcode[i])) {
					tBankName = "邮储";
				}
				if ("02".equals(tBankcode[i]) || "10".equals(tBankcode[i])) {
					tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

							+ " from lktransstatus where transdate = '"
							+ DateUtil.getCur10Date()
							+ "' and bankcode = '"
							+ tBankcode[i]
							+ "'"
							+ " and funcflag = '01' and Rcode = '0' and transstatus = '0' and status = '0' ";
					tSSRS = tExeSQL.execSQL(tExeSql);
					int n = 0;
					for (int a = 1; a <= tSSRS.MaxRow; a++) {
					
						if (tSSRS.MaxRow > 0
								&& tSSRS.GetText(a, 5).substring(0, 4).equals(
										"系统繁忙")) {
							n++;
							cLogger.error("开始明细处理:");
							
						}
					}
					tDesc = tBankName
					+ ":超过30S保单总数（超过30秒自动删除数据并返回超时提示）:"
					+ n;
			tTimeCostDetail = null;
			tTimeCostDetail = new Element("TimeCostDetail");
			tTimeCostDetail.setText(tDesc);
			tRetData.addContent(tTimeCostDetail);
				}
				if ("02".equals(tBankcode[i]) || "10".equals(tBankcode[i])) {

					tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

							+ " from lktransstatus where transdate = '"
							+ DateUtil.getCur10Date()
							+ "' and bankcode = '"
							+ tBankcode[i]
							+ "'"
							// + "2011-03-09"
							+ " and funcflag = '01' and Rcode = '1' and transstatus = '1' and status = '1' and"
							+ "((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
							+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) >= 0"
							+ " and ((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
							+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) <= 30";
					tSSRS = tExeSQL.execSQL(tExeSql);
					if (tSSRS.MaxRow < 1) {
						tDesc = tBankName + ":0到30秒保单总数:" + 0;
					} else {
						tDesc = tBankName + ":0到30秒保单总数:" + tSSRS.MaxRow;
					}
					tTimeCostDetail = null;
					tTimeCostDetail = new Element("TimeCostDetail");
					tTimeCostDetail.setText(tDesc);
					tRetData.addContent(tTimeCostDetail);
				}
				
				if ("04".equals(tBankcode[i])) {
					tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

							+ " from lktransstatus where transdate = '"
							+ DateUtil.getCur10Date()
							+ "' and bankcode = '"
							+ tBankcode[i]
							+ "'"
							+ " and funcflag = '01' and Rcode = '0' and transstatus = '0' and status = '0' ";
					tSSRS = tExeSQL.execSQL(tExeSql);
					int n = 0;
					for (int a = 1; a <= tSSRS.MaxRow; a++) {
					
						if (tSSRS.MaxRow > 0
								&& tSSRS.GetText(a, 5).substring(0, 4).equals(
										"系统繁忙")) {
							n++;
							cLogger.error("开始明细处理:");
							
						}
					}
					tDesc = tBankName
					+ ":超过40S保单总数（超过40秒自动删除数据并返回超时提示）:"
					+ n;
			tTimeCostDetail = null;
			tTimeCostDetail = new Element("TimeCostDetail");
			tTimeCostDetail.setText(tDesc);
			tRetData.addContent(tTimeCostDetail);
				}
				if ("04".equals(tBankcode[i])) {

					tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

							+ " from lktransstatus where transdate = '"
							+ DateUtil.getCur10Date()
							+ "' and bankcode = '"
							+ tBankcode[i]
							+ "'"
							// + "2011-03-09"
							+ " and funcflag = '01' and Rcode = '1' and transstatus = '1' and status = '1' and"
							+ "((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
							+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) >= 0"
							+ " and ((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
							+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) <= 40";
					tSSRS = tExeSQL.execSQL(tExeSql);
					if (tSSRS.MaxRow < 1) {
						tDesc = tBankName + ":0到40秒保单总数:" + 0;
					} else {
						tDesc = tBankName + ":0到40秒保单总数:" + tSSRS.MaxRow;
					}
					tTimeCostDetail = null;
					tTimeCostDetail = new Element("TimeCostDetail");
					tTimeCostDetail.setText(tDesc);
					tRetData.addContent(tTimeCostDetail);
				}

				if ("01".equals(tBankcode[i])) {
					tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

							+ " from lktransstatus where transdate = '"
							+ DateUtil.getCur10Date()
							+ "' and bankcode = '"
							+ tBankcode[i]
							+ "'"
							+ " and funcflag = '01' and Rcode = '0' and transstatus = '0' and status = '0' ";
					tSSRS = tExeSQL.execSQL(tExeSql);
					int m = 0;
					for (int a = 1; a <= tSSRS.MaxRow; a++) {
						if (tSSRS.MaxRow > 0) {
							
							if(null!=tSSRS.GetText(a, 5)&&!"".equals(tSSRS.GetText(a, 5))){
								if(tSSRS.GetText(a, 5).substring(0, 4).equals(
								"系统繁忙")){
									m++;
									
								}
							}
							
						}
					}
					cLogger.error("开始明细处理:");
					tDesc = tBankName
							+ ":超过60S保单总数（超过60秒自动删除数据并返回超时提示）:"
							+ m;
					tTimeCostDetail = null;
					tTimeCostDetail = new Element("TimeCostDetail");
					tTimeCostDetail.setText(tDesc);
					tRetData.addContent(tTimeCostDetail);
				}
				for (int j = 0; j < tTimes.size(); j++) {
					tTime = ((Element) tTimes.get(j)).getText().split("-");
					int tStartTIme = Integer.parseInt(tTime[0]);
					int tEndTIme = Integer.parseInt(tTime[1]);

					if ("01".equals(tBankcode[i])) {

						tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

								+ " from lktransstatus where transdate = '"
								+ DateUtil.getCur10Date()
								+ "' and bankcode = '"
								+ tBankcode[i]
								+ "'"
								+ " and funcflag = '01' and Rcode = '1' and transstatus = '1' and status = '1' and "
								+ "((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
								+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) >= "
								+ tStartTIme
								+ " and ((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
								+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) < "
								+ tEndTIme;

					}
					if ("03".equals(tBankcode[i]) || "16".equals(tBankcode[i])) {
						tExeSql = "select proposalno,managecom,maketime,modifytime,descr"

								+ " from lktransstatus where transdate = '"
								+ DateUtil.getCur10Date()
								+ "' and bankcode = '"
								+ tBankcode[i]
								+ "'"
								+ " and funcflag = '00' and Rcode = '1' and transstatus = '1' and status = '0' and "
								+ "((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
								+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) >= "
								+ tStartTIme
								+ " and ((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
								+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) < "
								+ tEndTIme;
					}
//					if ("04".equals(tBankcode[i])) {
//						tExeSql = "select proposalno,managecom,maketime,modifytime,descr"
//
//								+ " from lktransstatus where transdate = '"
//								+ DateUtil.getCur10Date()
//								+ "' and bankcode = '"
//								+ tBankcode[i]
//								+ "'"
//								+ " and funcflag = '01' and Rcode = '1' and transstatus = '1' and status = '1' and"
//								+ "((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
//								+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) >= "
//								+ tStartTIme
//								+ " and ((int(substr(modifytime, 4, 2)) - int(substr(maketime, 4, 2))) * 60 +"
//								+ " (int(substr(modifytime, 7, 2)) - int(substr(maketime, 7, 2)))) < "
//								+ tEndTIme;
//					}
					cLogger.error("查询超时新单交易sql:" + tExeSql);
					tSSRS = tExeSQL.execSQL(tExeSql);

					if (tSSRS.MaxRow < 1) {
						cLogger.error("开始明细处理:");
						tDesc = tBankName + ":" + tStartTIme + "到" + tEndTIme
								+ "秒保单总数:" + 0;
						if (("01".equals(tBankcode[i]) && tStartTIme >= 60)||"02".equals(tBankcode[i])||"10".equals(tBankcode[i])||"04".equals(tBankcode[i])) {

						} else {
							tTimeCostDetail = null;
							tTimeCostDetail = new Element("TimeCostDetail");
							tTimeCostDetail.setText(tDesc);
							tRetData.addContent(tTimeCostDetail);
						}
					} else {

						if (!"02".equals(tBankcode[i])
								&& !"10".equals(tBankcode[i])&&!"04".equals(tBankcode[i])) {
							cLogger.error("银行代码:" + tBankcode[i] + "时间:"
									+ tStartTIme);
							if ("01".equals(tBankcode[i]) && tStartTIme >= 60) {
								cLogger.error("银行代码1:" + tBankcode[i] + "时间:"
										+ tStartTIme);
							} else {
								tDesc = tBankName + ":" + tStartTIme + "到"
										+ tEndTIme + "秒保单总数:" + tSSRS.MaxRow;

								tTimeCostDetail = null;
								tTimeCostDetail = new Element("TimeCostDetail");
								tTimeCostDetail.setText(tDesc);
								tRetData.addContent(tTimeCostDetail);
							}
						}
					}
				}
			}

			tTranData.addContent(tRetData);
			mOutXmlDoc = new Document(tTranData);

			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setStatus("1"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex) {
					cLogger.error(uex.getMessage());
				}

				mLKTransStatusDB.setDescr(tDesr);
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out YbtMonitor.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into YbtMonitor.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch("86");
		mLKTransStatusDB.setBankNode("86");
		mLKTransStatusDB.setBankOperator("ybtauto");
		mLKTransStatusDB.setTransNo("monitor" + mCurrentDate + mCurrentTime);
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0"); // 保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out YbtMonitor.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		Document tMonitorDoc = null;
		Element tTranData = new Element("TranData");
		Element tBaseInfo = new Element("BaseInfo");
		Element tBankCode = new Element("BankCode");
		tBankCode.setText("57");
		Element tFuncFlag = new Element("FunctionFlag");
		tFuncFlag.setText("57");
		tBaseInfo.addContent(tBankCode);
		tBaseInfo.addContent(tFuncFlag);
		tTranData.addContent(tBaseInfo);
		tMonitorDoc = new Document(tTranData);
		YbtMonitor tYbtMonitor = new YbtMonitor(null);
		tMonitorDoc = tYbtMonitor.service(tMonitorDoc);
		System.out.println(JdomUtil.toString(tMonitorDoc));
	}
}
