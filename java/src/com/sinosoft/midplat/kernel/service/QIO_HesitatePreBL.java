package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LPYBTAppWTDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.schema.LPYBTAppWTSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.lis.vschema.LPYBTAppWTSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class QIO_HesitatePreBL  extends ServiceImpl {

	public QIO_HesitatePreBL(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QIO_HesitatePreBL.service()...");
		 
		Document mOutXmlDoc = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = null;
		LKTransStatusSet mLKBalanceDetailSet = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			//处理前置机传过来的报错信息(扫描超时等)
			String tErrorStr = mTranData.getChildText("Error");
			if (null != tErrorStr) {
				throw new MidplatException(tErrorStr);
			}
			
			//保存犹豫期对账明细
			mLKBalanceDetailSet = saveDetails(pInXmlDoc);
			

			// 进行业务处理
			if("62".equals(mBaseInfo.getChildText("FunctionFlag"))){ //退保
				mOutXmlDoc = dealT(pInXmlDoc);
			}
			if("121".equals(mBaseInfo.getChildText("FunctionFlag"))){ //犹豫期退保
				mOutXmlDoc = deal(pInXmlDoc);
//				if(mGlobalInput.ManageCom.startsWith("8634")){
//					
//					mOutXmlDoc = deal(pInXmlDoc);
//				}else {
//					Element pTranData = new Element("TranData");
//					Element pRetData = new Element("RetData");
//					Element mFlag = new Element("Flag");
//					Element mDesc = new Element("Desc");
//					Element pEdorInfo = new Element("EdorInfo");
//					Element pBaseInfo = new Element("BaseInfo");
//					pBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
//					
//					mFlag.setText("0");
//					mDesc.setText("当前时间段不能进行此交易！");
//					
//					pRetData.addContent(mFlag);
//					pRetData.addContent(mDesc);
//					pTranData.addContent(pRetData);
//					pTranData.addContent(pBaseInfo);
//					pTranData.addContent(pEdorInfo);
//					mOutXmlDoc = new Document(pTranData);
//				}
			}
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Into QIO_HesitatePreBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) throws MidplatException {
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mChkDetails = mTranData.getChild(ChkDetails);
	
		List mChkDetailList = mChkDetails.getChildren(ChkDetail);
		LPYBTAppWTSet mLPYBTAppWTSet = null;
		for (int i = 0; i < mChkDetailList.size(); i++) {
			
			Element tChkDetail = (Element) mChkDetailList.get(i);
			
			String mContNo = tChkDetail.getChildText(CardNo);
			LCContDB cont = new LCContDB();
			cont.setContNo(mContNo);
			LCContSet mCont = cont.query();
			LCContSchema lc = mCont.get(1);
			if(mCont.size() > 0){
				
				LPYBTAppWTDB mLPYBTAppWTDB = new LPYBTAppWTDB();
				
				String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
				String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
				mLPYBTAppWTDB.setContNo(mContNo);
				
				mLPYBTAppWTSet = mLPYBTAppWTDB.query();
				System.out.println(mLPYBTAppWTSet.size());
				
				if(mLPYBTAppWTSet.size() > 0){	//判断保单是否已存在犹豫期退保表中！
					String mSQL = "update lktransstatus set descr = '该保单已存在犹豫期退保表中，请核查！' where polno = '" + mContNo + "' and transno = '" + "ybt" + tChkDetail.getChildText(TransrNo)+ "' and funcflag = '121' and makedate = Current Date with ur";
					ExeSQL mExeSQL = new ExeSQL();
					if (!mExeSQL.execUpdateSQL(mSQL)) {
						cLogger.error(mExeSQL.mErrors.getFirstError());
						cLogger.warn("该保单已存在犹豫期退保表中，请核查！");
					}
//						throw new MidplatException("该保单已存在犹豫期退保表中，请核查！");
				}
				
				LPYBTAppWTSchema mLPYBTAppWTSchema = new LPYBTAppWTSchema();
				mLPYBTAppWTSchema.setContNo(mContNo);
				mLPYBTAppWTSchema.setManageCom(lc.getManageCom());
				mLPYBTAppWTSchema.setAppWTDate(mCurrentDate);
				mLPYBTAppWTSchema.setAppntName(lc.getAppntName());
				mLPYBTAppWTSchema.setAppntSex(lc.getAppntSex());
				mLPYBTAppWTSchema.setAppntBirthday(lc.getAppntBirthday());
				mLPYBTAppWTSchema.setAppntIDType(lc.getAppntIDType());
				mLPYBTAppWTSchema.setAppntIDNo(lc.getAppntIDNo());
				mLPYBTAppWTSchema.setGBFee("0");	//保险公司定为0元
				mLPYBTAppWTSchema.setAppState("1");
				mLPYBTAppWTSchema.setoperator("ybt");
				mLPYBTAppWTSchema.setMakeDate(mCurrentDate);
				mLPYBTAppWTSchema.setMakeTime(mCurrentTime);
				mLPYBTAppWTSchema.setModifyDate(mCurrentDate);
				mLPYBTAppWTSchema.setModifyTime(mCurrentTime);
				
				mLPYBTAppWTSet.add(mLPYBTAppWTSchema);
				
			}else {
				if (mCont.mErrors.needDealError()) {
					cLogger.error(mCont.mErrors.getFirstError());
					String mSQL = "update lktransstatus set descr = '未查到有效保单信息！' where polno = '" + mContNo + "' and transno = '" + "ybt" + tChkDetail.getChildText(TransrNo) + "' and funcflag = '121' and makedate = Current Date with ur";
					ExeSQL mExeSQL = new ExeSQL();
					if (!mExeSQL.execUpdateSQL(mSQL)) {
						cLogger.error(mExeSQL.mErrors.getFirstError());
						cLogger.warn("未查到有效保单信息！");
					}
					
//					throw new MidplatException("未查到该保单信息！");
				}
			}
		}
		
		cLogger.info("银行提交的对账明细总数(LKBalanceDetailSet)为：" + mLPYBTAppWTSet.size());
		MMap pSubmitMMap = new MMap();
		pSubmitMMap.put(mLPYBTAppWTSet, "INSERT");
		VData pSubmitVData = new VData();
		pSubmitVData.add(pSubmitMMap);
		PubSubmit pPubSubmit = new PubSubmit();
		if (!pPubSubmit.submitData(pSubmitVData, "")) {
			cLogger.error(pPubSubmit.mErrors.getFirstError());
			throw new MidplatException("插入犹豫期退保信息失败！");
		}

		return new Document(mTranData);
	}
	
	private Document dealT(Document pInXmlDoc) {

		Element xTranData = pInXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);
		LCContSet mCont = cont.query();
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		if(mCont.size() > 0){
			LCContSchema lc = mCont.get(1);
			
			mFlag.setText("1");
			mDesc.setText("交易成功！");
			
			LCPolDB pol_db = new LCPolDB();
			pol_db.setContNo(mContNo);
			LCPolSet polSet = pol_db.query();
			
			double mSumAmnt = 0;
			String risk_main = "";
			String risk_fu = "";
			for (int i = 1; i <= polSet.size(); i++) {
				LCPolSchema mPol = polSet.get(i);
				mSumAmnt += mPol.getAmnt();
				if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
					risk_main = mPol.getRiskCode();
				}
				if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
					risk_fu = mPol.getRiskCode();
				}
			}
			
			Element nEdorAcceptNo = new Element("EdorAcceptNo");
			nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
			
			Element nAttachmentContent = new Element("AttachmentContent");
			Element nAttachment_1 = new Element("Attachment");
			nAttachment_1.addAttribute("id","Attachment_1");
			Element nAttachmentData_1 = new Element("AttachmentData");
			nAttachmentData_1.setText("尊敬的 "+ lc.getInsuredName() +" 先生/女士：");
			nAttachment_1.addContent(nAttachmentData_1);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String now = sdf.format(new Date());
			Element nAttachment_2 = new Element("Attachment");
			nAttachment_2.addAttribute("id","Attachment_2");
			Element nAttachmentData_2 = new Element("AttachmentData");
			nAttachmentData_2.setText("经您于 "+ now.substring(0,4) +"  年 "+ now.substring(4,6) +" 月 "+ now.substring(6,8) +" 日递交的退保申请，现作如下批注：");
			nAttachment_2.addContent(nAttachmentData_2);
			
			Element nAttachment_3 = new Element("Attachment");
			nAttachment_3.addAttribute("id","Attachment_3");
			Element nAttachmentData_3 = new Element("AttachmentData");
			if("".equals(risk_fu)){
				nAttachmentData_3.setText("本公司同意您的保单（保单号："+ lc.getContNo() +" ）的 "+ getRiskRep(risk_main) +" 的退保，");
			}else {
				nAttachmentData_3.setText("本公司同意您的保单（保单号："+ lc.getContNo() +" ）的 "+ getRiskRep(risk_main) +" 及所 "+ getRiskRep(risk_fu) +" 名称的退保，");
			}
			nAttachment_3.addContent(nAttachmentData_3);
			
			Element nAttachment_4 = new Element("Attachment");
			nAttachment_4.addAttribute("id","Attachment_4");
			Element nAttachmentData_4 = new Element("AttachmentData");
			nAttachmentData_4.setText("退保金￥ "+ mSumAmnt +" 元，保险责任终止。");
			nAttachment_4.addContent(nAttachmentData_4);
			
			Element nAttachment_5 = new Element("Attachment");
			nAttachment_5.addAttribute("id","Attachment_5");
			Element nAttachmentData_5 = new Element("AttachmentData");
			nAttachmentData_5.setText("此批单自 "+ now.substring(0,4) +"  年 "+ now.substring(4,6) +" 月 "+ now.substring(6,8) +" 日起生效。");
			nAttachment_5.addContent(nAttachmentData_5);
			
			
			nAttachmentContent.addContent(nAttachment_1);
			nAttachmentContent.addContent(nAttachment_2);
			nAttachmentContent.addContent(nAttachment_3);
			nAttachmentContent.addContent(nAttachment_4);
			nAttachmentContent.addContent(nAttachment_5);
			mEdorInfo.addContent(nEdorAcceptNo);
			mEdorInfo.addContent(nAttachmentContent);
		}else {
			mFlag.setText("0");
			mDesc.setText("未查到对应保单信息！");
		}
		
		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		return new Document(mTranData);
	}
	
	public String getRiskRep(String riskCode){
		
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
		
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into DailyBalance.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		/**
		 * 如果前置机扫描超时，系统当前时间可能已经是第二天0点，
		 * 此处(TransDate)不取系统当前时间，改用前置机传过来的时间。
		 * 前置机必须保证TranData/BaseInfo/BankDate中的时间的正确性，
		 * 如果是ftp方式对账，BankDate应为本轮扫描的开始时间；
		 * 如果是交易方式，需要银行将此字段传为白天交易的时间。
		 */
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildText(BankDate));
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out DailyBalance.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	/**
	 * 保存对账明细，返回保存的明细数据(LKBalanceDetailSet)
	 */
	private LKTransStatusSet saveDetails(Document pXmlDoc) throws Exception {
		cLogger.info("Into DailyBalance.saveDetails()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mChkDetails = mTranData.getChild(ChkDetails);
		
		String mCurDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurTime = DateUtil.getCurDate("HH:mm:ss");
		
		List mChkDetailList = mChkDetails.getChildren(ChkDetail);
		LKTransStatusSet mLKTransStatusSet = new LKTransStatusSet();
		for (int i = 0; i < mChkDetailList.size(); i++) {
			Element tChkDetail = (Element) mChkDetailList.get(i);
			
			LKTransStatusSchema tLKTransStatusSchema = new LKTransStatusSchema();

			tLKTransStatusSchema.setBankCode(tChkDetail.getChildText(BankCode));
			tLKTransStatusSchema.setBankBranch(tChkDetail.getChildText(BankZoneCode));
			tLKTransStatusSchema.setBankNode(tChkDetail.getChildText(BrNo));
			tLKTransStatusSchema.setBankOperator(tChkDetail.getChildText(TellerNo));
			tLKTransStatusSchema.setTransNo("ybt" + tChkDetail.getChildText(TransrNo));
			tLKTransStatusSchema.setFuncFlag(tChkDetail.getChildText(FuncFlag));
			tLKTransStatusSchema.setTransAmnt(tChkDetail.getChildText(TranAmnt));
			tLKTransStatusSchema.setPolNo(tChkDetail.getChildText(CardNo));
			tLKTransStatusSchema.setTransDate(tChkDetail.getChildText(TranDate));
			tLKTransStatusSchema.setTransTime(mCurTime);
			tLKTransStatusSchema.setMakeDate(mCurDate);
			tLKTransStatusSchema.setMakeTime(mCurTime);
			tLKTransStatusSchema.setModifyDate(mCurDate);
			tLKTransStatusSchema.setModifyTime(mCurTime);
			
			if(!"0".equals(tChkDetail.getChildTextTrim("ConfirmFlag"))){ //从前置机对账文件中判断如果是Type为X则认为是X期
				tLKTransStatusSchema.setDescr("银行处理标志："+ tChkDetail.getChildTextTrim("ConfirmFlag") + "异常！");
			}
			mLKTransStatusSet.add(tLKTransStatusSchema);
		}

		/**
		 * 将银行发过来的对账明细存储到对账明细表(LKBalanceDetail)中
		 */
		cLogger.info("银行提交的对账明细总数(LKBalanceDetailSet)为：" + mLKTransStatusSet.size());
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLKTransStatusSet, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("保存犹豫期对账明细失败！");
		}

		cLogger.info("Out DailyBalance.saveDetails()!");
		return mLKTransStatusSet;
	}
}

