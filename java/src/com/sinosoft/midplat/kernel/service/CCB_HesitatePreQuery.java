package com.sinosoft.midplat.kernel.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bq.PedorYBTBudgetBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_HesitatePreQuery extends ServiceImpl  {

	public CCB_HesitatePreQuery(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_HesitatePreQuery.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		String mSQL = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");
	
			mSQL = "select 1 from lccont where contno = '" + mLCCont.getChildText("ContNo") + "' and salechnl = '04'  with ur";
			if(new ExeSQL().getOneValue(mSQL).equals("1")){
				mOutXmlDoc = deal(mLCCont);
			}else {
				throw new MidplatException("不存在该保单信息！");
			}
				
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_HesitatePreQuery.service()!");
		return mOutXmlDoc;
	}

	private Document deal(Element pLCCont) throws Exception {
		cLogger.info("In CCB_HesitatePreQuery.deal()!");
		
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mLCCont = new Element("LCCont");
		Element mLCappnt = new Element("LCappnt");
		Element mLCInsureds = new Element("LCInsureds");
		Element mEdorInfo = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String now = sdf.format(new Date());
			
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setContNo(pLCCont.getChildText("ContNo"));
		LCContSet mLCContSet = mLCContDB.query();

		for(int i = 1; i <= mLCContSet.size(); i++){
			LCContSchema mLCContSchema = mLCContSet.get(i);
			
			Element mAppntName = new Element("AppntName");
			mAppntName.setText(mLCContSchema.getAppntName());
			Element mAppntIDType = new Element("AppntIDType");
			mAppntIDType.setText(mLCContSchema.getAppntIDType());
			Element mAppntIDNo = new Element("AppntIDNo");
			mAppntIDNo.setText(mLCContSchema.getAppntIDNo());
			Element mContNo = new Element("ContNo");
			mContNo.setText(mLCContSchema.getContNo());
			Element mPrtNo = new Element("PrtNo");
			mPrtNo.setText(mLCContSchema.getPrtNo());
			
			LCInsuredDB mLCInsuredDB = new LCInsuredDB();
			mLCInsuredDB.setContNo(pLCCont.getChildText("ContNo"));
			LCInsuredSet mLCInsuredSet = mLCInsuredDB.query();
			
			for(int j = 1 ;j <= mLCInsuredSet.size(); j++){
				LCInsuredSchema mLCInsuredSchema = mLCInsuredSet.get(j);
				Element mLCInsured = new Element("LCInsured");
				Element mLCInsureName = new Element("LCInsureName");
				mLCInsureName.setText(mLCInsuredSchema.getName());
				mLCInsured.addContent(mLCInsureName);
				mLCInsureds.addContent(mLCInsured);
			}
			
			LCPolDB pol_db = new LCPolDB();
			pol_db.setContNo(pLCCont.getChildText("ContNo"));
			LCPolSet polSet = pol_db.query();
			
			double mSumPrem = 0;
			String risk_main = "";
			String risk_fu = "";
			
			Element mRisks = new Element("Risks");
			Element mRisk = null;
			Element nMainRiskCode = null;
			
			for (int z = 1; z <= polSet.size(); z++) {
				LCPolSchema mPol = polSet.get(z);
				mSumPrem += mPol.getPrem();
				
				mRisk = new Element("Risk");
				
				Element nRiskCode = new Element("RiskCode");
				Element nRiskName = new Element("RiskName");
				
				if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
					risk_main = mPol.getRiskCode();
					nMainRiskCode = new Element("MainRiskCode");
					nMainRiskCode.setText(risk_main);
					nRiskName.setText(getRiskRep(risk_main));
					mRisk.addContent(nMainRiskCode);
					mRisk.addContent(nRiskName);
				}
				if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
					risk_fu = mPol.getRiskCode();
					
					nRiskCode.setText(risk_fu);
					nRiskName.setText(getRiskRep(risk_fu));
					mRisk.addContent(nRiskCode);
					mRisk.addContent(nRiskName);
				}
				mRisks.addContent(mRisk);
			}
			
			mEdorInfo = new Element("EdorInfo");
			Element mEdorNo = new Element("EdorNo");
			Element nSumPrem = new Element("SumPrem");
			Element nEdorType = new Element("EdorType");
			
			String pd_sql = "select case when customgetpoldate is null and days(current date) - days(cvalidate)<=30 then '1'when customgetpoldate is not null and days(current date) - days(customgetpoldate)<15 then '1'else '0' end from lccont where contno='" + pLCCont.getChildText("ContNo") + "' with ur";
			
			if(new ExeSQL().getOneValue(pd_sql).equals("1")){
				nSumPrem.setText(new DecimalFormat("0.00").format(mSumPrem));
				nEdorType.setText("WT");	//犹豫期退保标志
			}else {
				PedorYBTBudgetBL mPedorYBTBudgetBL = new PedorYBTBudgetBL();
				double pSumPrem = mPedorYBTBudgetBL.getZTmoney(pLCCont.getChildText("ContNo"),now);
				nSumPrem.setText(new DecimalFormat("0.00").format(pSumPrem));
				nEdorType.setText("CT");	//解约标志
			}
			
			mEdorInfo.addContent(mEdorNo);
			mEdorInfo.addContent(nSumPrem);
			mEdorInfo.addContent(nEdorType);

			mLCappnt.addContent(mAppntName);
			mLCappnt.addContent(mAppntIDType);
			mLCappnt.addContent(mAppntIDNo);
			
			mLCCont.addContent(mContNo);
			mLCCont.addContent(mPrtNo);
			mLCCont.addContent(mLCappnt);
			mLCCont.addContent(mLCInsureds);
			mLCCont.addContent(mRisks);
			mLCCont.addContent(mEdorInfo);
		}
		Element mFlag = new Element("Flag");
		mFlag.setText("1");
		Element mDesc = new Element("Desc");
		mDesc.setText("交易成功！");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		
		cLogger.info("Out CCB_HesitatePreQuery.deal()!");
		return new Document(mTranData);
	}
	
	public String getRiskRep(String riskCode){
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
	}
		
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_HesitatePreQuery.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_HesitatePreQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
