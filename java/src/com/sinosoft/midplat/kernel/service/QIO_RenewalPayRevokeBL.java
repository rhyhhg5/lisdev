package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.finfee.YBTTempRecoil;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;

public class QIO_RenewalPayRevokeBL extends ServiceImpl {

	public QIO_RenewalPayRevokeBL(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QIO_RenewalPayRevokeBL.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			String mContNo = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("ContNo");
			String mPay = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("Prem").trim();
			System.out.println("pay = "+mPay);
			//調用核心接口
			TransferData tTransferData = new TransferData();
	        tTransferData.setNameAndValue("ContNo", mContNo);
	        tTransferData.setNameAndValue("PayMoney", mPay);
	        tTransferData.setNameAndValue("GlobalInput", mGlobalInput);

	        YBTTempRecoil tYBTTempRecoil = new YBTTempRecoil();

	        if (!tYBTTempRecoil.submitData(tTransferData)) {
	            // 反冲失败，可获得返回信息
	        	CErrors cErrors = new CErrors();
	            cErrors = tYBTTempRecoil.getErrorInf();
	            mLKTransStatusDB.setDescr(cErrors.getLastError());
	            mLKTransStatusDB.setRCode("0");
	            mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", cErrors.getLastError());
	        }else {
	        	// 組織返回報文
	        	mLKTransStatusDB.setRCode("1");
	        	mLKTransStatusDB.setTransStatus("1");
	        	mOutXmlDoc = getOutStd();
	        }
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		
		cLogger.info("Out QIO_RenewalPayRevokeBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document getOutStd() {
		cLogger.info("Into QIO_RenewalPayRevokeBL.getOutStd()...");
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mRetData);
		
		cLogger.info("Out QIO_RenewalPayRevokeBL.getOutStd()...");
		return new Document(mTranData);
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into QIO_RenewalPayRevokeBL.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Pay"));
		mLKTransStatusDB.setRiskCode(mLCCont.getChildText("RiskCode")); 
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out QIO_RenewalPayRevokeBL.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
