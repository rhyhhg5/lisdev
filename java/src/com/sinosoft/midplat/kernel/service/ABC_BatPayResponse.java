package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

public class ABC_BatPayResponse extends ServiceImpl {
	public ABC_BatPayResponse(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_BatPayResponse.service()...");
		
		cLogger.info("农行批量代付回盘交易处理！该交易处理和代收交易一致，直接调用代收处理程序...");
		Document mOutXmlDoc = 
			new ABC_BatIncomeResponse(cThisBusiConf).service(pInXmlDoc);
		
		cLogger.info("Out ABC_BatPayResponse.service()!");
		return mOutXmlDoc;
	}
}