package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class SDNXS_Query extends ServiceImpl {
	public SDNXS_Query(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into SDNXS_Query.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
		
		
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLKTransStatusDB.getPolNo());
			tLCContDB.setPrtNo(mLKTransStatusDB.getPrtNo());
			tLCContDB.setProposalContNo(mLKTransStatusDB.getProposalNo());
			if ((null==tLCContDB.getContNo() || "".equals(tLCContDB.getContNo()))
				&& (null==tLCContDB.getPrtNo() || "".equals(tLCContDB.getPrtNo()))
				&& (null==tLCContDB.getProposalContNo() || "".equals(tLCContDB.getProposalContNo()))) {
				throw new MidplatException("相关号码不能为空！");
			}
			
			//判断是否是自动冲正后的查询(余额不足时)
			String mSQL = "select 1 from LKTransStatus where bak4 = '1'  and "
					+"ProposalNo = '"+mLKTransStatusDB.getProposalNo()+"' and "
					+"BankCode = '"+mLKTransStatusDB.getBankCode()+"' "
					+"and FuncFlag = '01' and rcode = '1' and "
					+"BankNode = '"+mLKTransStatusDB.getBankNode()+"' and "
					+"BankOperator = '"+mLKTransStatusDB.getBankOperator()+"' and "
					+"BankBranch = '"+mLKTransStatusDB.getBankBranch()+"' "
					+"with ur";
			ExeSQL mExeSQL = new ExeSQL();
			if("1".equals(mExeSQL.getOneValue(mSQL))){
				throw new MidplatException("查询保单数据失败!");//不返回保单数据,留待对账时删除数据!(之前冲正交易未做操作)
			}
			
			LCContSet tLCContSet = tLCContDB.query();
			if ((null==tLCContSet) || (tLCContSet.size()<1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			//判断是否处于签单状态
			if("0".equals(tLCContSchema.getAppFlag())){
				throw new MidplatException("查询保单数据失败!");
			}
			
			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();

			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out SDNXS_Query.service()!");
		return mOutXmlDoc;
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into SDNXS_Query.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out SDNXS_Query.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "F:/working/picch/std/testXml/Query/std_09_in.xml";
		String mOutFile = "F:/working/picch/std/testXml/Query/std_09_out.xml";

		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new SDNXS_Query(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
	}
}
