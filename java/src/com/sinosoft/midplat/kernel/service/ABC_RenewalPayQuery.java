package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.finfee.YBTPayQuery;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class ABC_RenewalPayQuery extends ServiceImpl {
	private static String contNo = "";
	private static Element mBaseInfo = null;
	
	public ABC_RenewalPayQuery(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ABC_RenewalPayQuery.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mTranData = pInXmlDoc.getRootElement();
//		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			
			contNo = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("ContNo");
			
			//判断此保单续期与新单是否同一机构
			String sql_xq = "select 1 from lccont where contno = '"+contNo+"' and managecom = '"+mGlobalInput.ManageCom+"' with ur";
			if (!"1".equals(new ExeSQL().getOneValue(sql_xq))) {
				throw new MidplatException("非本市机构所出保单，无法缴费！");
			}
			
			//调取核心查询接口
			TransferData tTransferData = new TransferData();
	        tTransferData.setNameAndValue("ContNo", contNo);
	        
	        YBTPayQuery tYBTPayQuery = new YBTPayQuery();
	        if (!tYBTPayQuery.submitData(tTransferData)) {
	            // 若查询失败，可获得返回信息
	        	CErrors cErrors = new CErrors();
	            cErrors = tYBTPayQuery.getErrorInf();
	            System.out.println(cErrors.getLastError()); // 查询失败原因
	            mLKTransStatusDB.setDescr(cErrors.getLastError());
		        mLKTransStatusDB.setRCode("0");
	            mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", cErrors.getLastError());
	        }else {
	            TransferData result = tYBTPayQuery.getResult(); // getResult 信息返回函数
	            System.out.println(result.getValueByName("ManageCom")); // 保单机构
	            System.out.println(result.getValueByName("AppntName")); // 投保人名称
	            System.out.println(result.getValueByName("ContNo")); // 保单号
	            System.out.println(result.getValueByName("PayMoney")); // 金额
	            System.out.println(result.getValueByName("StartPayDate")); // 缴费起始日期
	            System.out.println(result.getValueByName("PayDate")); // 缴费截至日期
	            System.out.println(result.getValueByName("PayCount")); // 期数
	            
	            //取得核心数据后组织返回报文
	            mLKTransStatusDB.setRCode("1");
	            mOutXmlDoc = getOutStd(result);
	        }
		} catch (Throwable ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ABC_RenewalPayQuery.service()...");
		return mOutXmlDoc;
	}
	
	private static Document getOutStd(TransferData result){
		Element mTranData = new Element(TranData);
		Element mLCCont = new Element(LCCont);
		Element mRetData = new Element(RetData);
		
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		mRetData.addContent(mFlag.setText("1"));
		mRetData.addContent(mDesc.setText("交易成功！"));
		
		contNo = (String) result.getValueByName("ContNo");
		Element mContNo = new Element(ContNo); // 保单号
		mContNo.setText(contNo);
		
		// 查询保单号对应的riskCode（主）
		String sql_riskCode = "select riskcode from lcpol where contno = '"+contNo+"' and riskseqno in ('01','1') with ur";
		String riskCode = new ExeSQL().getOneValue(sql_riskCode);
		String sql_riskName = "select riskname from lmrisk where riskcode = '"+ riskCode +"'  with ur";
		
		//投保人证件信息
		String sql_appntType = "select idtype from lcappnt where contno = '" + contNo + "' with ur";
		String sql_idno = "select idno from lcappnt where contno = '" +  contNo + "' with ur";
		
		Element mRiskCode = new Element(RiskCode);//险种编码
		mRiskCode.setText(riskCode);
		
		Element mAppntName = new Element(AppntName);// 投保人名称
		mAppntName.setText((String) result.getValueByName("AppntName"));
		
		Element mAppntType = new Element("AppntType");// 投保人证件类别
		mAppntType.setText(new ExeSQL().getOneValue(sql_appntType));
		
		Element mAppntIdno = new Element("AppntIdno");// 投保人证件号
		mAppntIdno.setText(new ExeSQL().getOneValue(sql_idno));
		
		Element mReceivDate = new Element("ReceivDate");// 缴费截至日期
		mReceivDate.setText((String) result.getValueByName("PayDate"));
		
		Element mReceivPay = new Element("ReceivPay");// 金额
		mReceivPay.setText((String) result.getValueByName("PayMoney"));
		
		Element mDescr = new Element("Descr");   //备注
		
		Element mReceivNum = new Element("ReceivNum");// 期数
		mReceivNum.setText((String) result.getValueByName("PayCount"));
		
		Element mReceivBegDate = new Element("ReceivBegDate");// 缴费起始日期
		mReceivBegDate.setText((String) result.getValueByName("StartPayDate"));
		
		Element mRiskName = new Element("RiskName"); //险种名称
		mRiskName.setText(new ExeSQL().getOneValue(sql_riskName));
		
		Element mZoneNo = new Element("ZoneNo"); //地区代码
		mZoneNo.setText(mBaseInfo.getChildText("ZoneNo"));
		
		Element mBrNo = new Element("BrNo");   // 网点代码
		mBrNo.setText(mBaseInfo.getChildText("BrNo"));
		
		Element mRemark = new Element("Remark");  //集团、股份标志
		
		
		mLCCont.addContent(mRiskCode);
		mLCCont.addContent(mContNo);
		mLCCont.addContent(mAppntName);
		mLCCont.addContent(mAppntIdno);
		mLCCont.addContent(mAppntType);
		mLCCont.addContent(mReceivDate);
		mLCCont.addContent(mReceivPay);
		mLCCont.addContent(mDescr);
		mLCCont.addContent(mReceivNum);
		mLCCont.addContent(mReceivBegDate);
		mLCCont.addContent(mRiskName);
		mLCCont.addContent(mZoneNo);
		mLCCont.addContent(mBrNo);
		mLCCont.addContent(mRemark);
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		return new Document(mTranData);
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into ABC_RenewalPayQuery.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out ABC_RenewalPayQuery.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
