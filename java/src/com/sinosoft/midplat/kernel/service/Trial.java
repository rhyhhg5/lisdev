/**
 * 试算交易。
 * 注意：试算不是一个单独的交易，试算的交易流水号可以重用；
 * 本类在处理时，不插入银保通日志，交易完成后，无论成功与否都删除所有交易数据。
 */

package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.BlackPhoneCheck;
import com.sinosoft.midplat.kernel.service.check.IdNoCheck;
import com.sinosoft.midplat.kernel.service.check.YBTRiskCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtAutoCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtContBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtContInsuredIntlBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtInsuredProposalBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Trial extends ServiceImpl {
	public Trial(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into Trial.service()...");
		LKTransStatusDB mLKTransStatusDB = null;
		Document mOutXmlDoc = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false;	//删除数据标志 
		
		YBTRiskCheck ycheck = new YBTRiskCheck();
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		Element tThisConfRoot = cThisBusiConf.getParent();
		String mProposalContNo = mLCCont.getChildText(ProposalContNo);//add by gcy 20180427
		String mSalary = mLCCont.getChild(LCAppnt).getChildText("Salary");//投保人年收入
		String mCountyType = mLCCont.getChild(LCAppnt).getChildText("CountyType");//投保人所属区域
		
		String mAppntPhone = mLCCont.getChild(LCAppnt).getChildText(AppntPhone);
		String mAppntMobile = mLCCont.getChild(LCAppnt).getChildText(AppntMobile);
		String mAppntOfficePhone = mLCCont.getChild(LCAppnt).getChildText(AppntOfficePhone);
		String tRiskCode = pInXmlDoc.getRootElement().getChild(LCCont)
							.getChild(LCInsureds).getChild(LCInsured)
							.getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
		Element mRisk = pInXmlDoc.getRootElement().getChild(LCCont).getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChild(Risk);
		try {
			
			//如果PrtNo不存在，则可以根据PrtNo删除数据
			String tSQL = "select 1 from lccont where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
			String tSQL_b = "select 1 from lbcont where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
			if (!new ExeSQL().getOneValue(tSQL).equals("1")) {
				if(!new ExeSQL().getOneValue(tSQL_b).equals("1")){
					mDeleteable = true;
				}else {
					throw new MidplatException("该投保单印刷号已使用，请更换！");
				}
			}else {
				throw new MidplatException("该投保单印刷号已使用，请更换！");
			}
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			ycheck.checkRiskForYBT(pInXmlDoc);
			//工行网银 单证号 = 投保单印刷号 
			if(mBaseInfo.getChildText(BankCode).equals("01") || mBaseInfo.getChildText(BankCode).equals("08")){
				if(mLCCont.getChildText("SourceType").equals("1") || mLCCont.getChildText("SourceType").equals("8") || mLCCont.getChildText("SourceType").equals("e")){
					String wyPrtno = "56"+PubFun1.CreateMaxNo("YBTPRTNO", 11);// 生成印刷号
					mProposalContNo =  wyPrtno;
					
					String mSQL = "select 1 from lccont where ProposalContNo = '" + mProposalContNo + "' union select 1 from lbcont where ProposalContNo = '" + mProposalContNo + "' with ur";	
					tSQL = "select 1 from lktransstatus where proposalno='" + mProposalContNo + "' and rcode is null with ur";
					String zx_sql = "update lzcard set stateflag = '5' where startno = '" + mProposalContNo + "' with ur";
					if(mProposalContNo.equals("")){
						throw new MidplatException("工行网银无可提供的单证号！");
					}
					if ("1".equals(new ExeSQL().getOneValue(tSQL))) { //同时两个人出单 系统查出相同的单证
						throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
					}else if(new ExeSQL().getOneValue(mSQL).equals("1")){ //一般不会出现这种情况
						throw new MidplatException("该投保单印刷号已使用，请更换！");
					}else {
						mLCCont.getChild(PrtNo).setText(mProposalContNo);
						mLCCont.getChild(ProposalContNo).setText(mProposalContNo);
					}
				}
			}
			
			//新增加广东地区 投被保人关系为父子、母女、母子、父女关系时 投被保人年龄之差大于18岁
			if(mGlobalInput.ManageCom.startsWith("8644")){
				String mRelaToInsured = mLCCont.getChild("LCAppnt").getChildText("RelaToInsured");
				String mRelaToAppnt = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("RelaToAppnt");
				String mAppntBirthday = mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");
				String mBirthday = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("Birthday");
				if (mRelaToInsured.equals("03") || mRelaToInsured.equals("04")
						|| mRelaToInsured.equals("05")
						|| mRelaToAppnt.equals("03")
						|| mRelaToAppnt.equals("04")
						|| mRelaToAppnt.equals("05")) {
					if(!BlackPhoneCheck.checkBirthday(mAppntBirthday, mBirthday)){
						throw new MidplatException("投被保人关系父子、父女、母女、母子关系时，投被保人年龄差值小于18岁不通过！");
					}
				} 
			}
			
			//广州农商 add by gcy 20180427
			if ("66".equals(mBaseInfo.getChildText(BankCode))) {
				if ("1".equals(mLCCont.getChildText("SourceType"))
						|| "8".equals(mLCCont.getChildText("SourceType"))
						|| "e".equals(mLCCont.getChildText("SourceType"))) {
					cLogger.info("动态生成保单印刷号和单证号开始..." + mBaseInfo.getChildText(BankCode));
					
					String wyPrtno = "56"+PubFun1.CreateMaxNo("YBTPRTNO", 11);// 生成印刷号
					mProposalContNo =  wyPrtno;
					cLogger.info("生成新的保单印刷号为："+wyPrtno);
					
					String mSQL = "select 1 from lccont where ProposalContNo = '" + mProposalContNo + "' union select 1 from lbcont where ProposalContNo = '" + mProposalContNo + "' with ur";	
					String tSQL0 = "select 1 from lktransstatus where proposalno='" + mProposalContNo + "' and rcode is null with ur";
					if(mProposalContNo.equals("")){
						throw new MidplatException("无可提供的单证号！");
					}
					if ("1".equals(new ExeSQL().getOneValue(tSQL0))) { //同时两个人出单 系统查出相同的单证
						throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
					}else if(new ExeSQL().getOneValue(mSQL).equals("1")){ 
						//一般不会出现这种情况
						throw new MidplatException("该投保单印刷号已使用，请更换！");
					}else {
						mLCCont.getChild(PrtNo).setText(mProposalContNo);
						mLCCont.getChild(ProposalContNo).setText(mProposalContNo);
					}
					cLogger.info("动态生成保单印刷号和单证号结束..." + mBaseInfo.getChildText(BankCode));
				}
			}
			
			//------------------------投保人，被保人，受益人身份证信息校验～～2017.8.14-----------------------------------------------
//			cLogger.info("身份证号校验开始");
//			
//			//NO1.获取投保人，被保人，受益人身份信息；
//			IdNoCheck idNoCheck = new IdNoCheck();
//			Element appElement = mLCCont.getChild("LCAppnt");
//			String appIdtp = appElement.getChildText("AppntIDType");
//			String appIdNo = appElement.getChildText("AppntIDNo");
//			if (("0".equals(appIdtp) || "5".equals(appIdtp)) && null != appIdNo && !"".equals(appIdNo)) {
//				appElement.getChild("AppntIDNo").setText(appIdNo.toUpperCase());
//				String appSex = appElement.getChildText("AppntSex");
//				String appBri = appElement.getChildText("AppntBirthday");
//				idNoCheck.isValidatedAllIdcard(appIdNo, appBri, appSex);
//				
//			}
//			List lisIns = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
//			for (int i = 0; i < lisIns.size(); i++) {
//				Element insElement = (Element) lisIns.get(i);
//				String insIdtp = insElement.getChildText("IDType");
//				String insIdNo = insElement.getChildText("IDNo");
//				if (("0".equals(insIdtp) || "5".equals(insIdtp)) && null != insIdNo && !"".equals(insIdNo)) {
//					insElement.getChild("IDNo").setText(insIdNo.toUpperCase());
//					String insSex = insElement.getChildText("Sex");
//					String insBri = insElement.getChildText("Birthday");
//					System.out.println(insBri);
//					idNoCheck.isValidatedAllIdcard(insIdNo, insBri, insSex);
//				}
//				
//				String lcBnfNum = insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildText(LCBnfCount);
//				System.out.println("lcBnfNum============"+lcBnfNum);
//				if (!"0".equals(lcBnfNum) && null != lcBnfNum) {
//					List lisBnf = insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildren("LCBnf");
//					for (int j = 0; j < lisBnf.size(); j++) {
//						Element bnfElement = (Element) lisBnf.get(j);
//						String bnfIdtp = bnfElement.getChildText("IDType");
//						String bnfIdNo = bnfElement.getChildText("IDNo");
//						if (("0".equals(bnfIdtp) || "5".equals(bnfIdtp)) && null != bnfIdNo && !"".equals(bnfIdNo)) {
//							bnfElement.getChild("IDNo").setText(bnfIdNo.toUpperCase());
//							String bnfSex = bnfElement.getChildText("Sex");
//							String bnfBri = bnfElement.getChildText("Birthday");
//							idNoCheck.isValidatedAllIdcard(bnfIdNo,bnfBri , bnfSex);
//						}
//					}
//				}else if(null != insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildren("LCBnf")){
//					List lisBnf = insElement.getChild("Risks").getChild("Risk").getChild("LCBnfs").getChildren("LCBnf");
//					for (int j = 0; j < lisBnf.size(); j++) {
//						Element bnfElement = (Element) lisBnf.get(j);
//						String bnfIdtp = bnfElement.getChildText("IDType");
//						String bnfIdNo = bnfElement.getChildText("IDNo");
//						if (("0".equals(bnfIdtp) || "5".equals(bnfIdtp)) && null != bnfIdNo && !"".equals(bnfIdNo)) {
//							System.out.println("bnfIdNo================"+bnfIdNo);
//							bnfElement.getChild("IDNo").setText(bnfIdNo.toUpperCase());
//							String bnfSex = bnfElement.getChildText("Sex");
//							String bnfBri = bnfElement.getChildText("Birthday");
//							idNoCheck.isValidatedAllIdcard(bnfIdNo,bnfBri , bnfSex);
//						}
//					}
//				}
//			}
//			cLogger.info("身份证号校验结束");
			
			//广东地区所有银行年龄加险种期缴校验
			if(mGlobalInput.ManageCom.startsWith("8644"))
			{
				List lisRisk = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
//				for(Element Risk :lisRisk){
				
				for(int i=0;i<lisRisk.size();i++){
					Element Risk = (Element)lisRisk.get(i);
				if(Risk != null)
				{
					String PayIntv = Risk.getChildTextTrim("PayIntv");  //获取缴费频率
					String InsuYear = Risk.getChildTextTrim("InsuYear");  //获取保险期间
					cLogger.info("获取缴费频率"+PayIntv);
					cLogger.info("获取保险期间"+InsuYear);
					
					if(!PayIntv.equals("0")){
						String appAge =  mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");  //获取投保人年龄
						System.out.println("appAge"+appAge);   //1978-11-09
						int Age = this.getAppntAge(appAge);
						cLogger.info("获取投保人年龄:"+appAge);
						if(Age >60 && (Integer.parseInt(InsuYear))>1){
							throw new MidplatException("投保人年龄大于60岁,且保险期间大于1年,不通过...");
						}
					}
				}
				}
			}
			
			// 山西地区校验电话号码---(20121112,增加江苏号码校验)（20121128，增加四川校验、黑龙江、内蒙古、20130109陕西、20130603 山东）
//			String mInsuredPhone = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("HomePhone");
//			String mInsuredMobile = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("InsuredMobile");
			if (mGlobalInput.ManageCom.startsWith("8614") || mGlobalInput.ManageCom.startsWith("8632") || mGlobalInput.ManageCom.startsWith("8651") || mGlobalInput.ManageCom.startsWith("8615") || mGlobalInput.ManageCom.startsWith("8661")|| mGlobalInput.ManageCom.startsWith("8637")) {
				//山西 投保人地址、电话不能为空
				String xHomeAddress = mLCCont.getChild("LCAppnt").getChildText("HomeAddress");
				if("".equals(xHomeAddress) || xHomeAddress == null){
					throw new MidplatException("投保人联系地址不能为空！");
				}
				if(("".equals(mAppntPhone) || mAppntPhone == null) 
						&& ("".equals(mAppntMobile) || mAppntMobile == null)
						&& ("".equals(mAppntOfficePhone) || mAppntOfficePhone == null)){
					throw new MidplatException("投保人联系电话至少一个不能为空！");
				}
				
				BlackPhoneCheck.checkPhone(mAppntPhone);//校验投保人号码
				BlackPhoneCheck.checkPhone(mAppntMobile);
//				BlackPhoneCheck.checkPhone(mInsuredPhone);// 校验被保人号码
//				BlackPhoneCheck.checkPhone(mInsuredMobile);
			}
			
			// 陕西地区校验投被保人地址不能为空且内容在12个字符以上。  add by wz 20130812
			if(mGlobalInput.ManageCom.startsWith("8661")){
				String msg = checkAddre(mLCCont);
				if(!"".equals(msg)){
					throw new MidplatException(msg);
				}
			}
			
			Calendar calendar1 = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar1.add(Calendar.YEAR, -1);
			Date nowDate1 = calendar1.getTime();	//三号文获取当前日期前一年日期
			String compareDate = sdf.format(nowDate1);	//对比日期
			String nowDate = DateUtil.getCurDate("yyyy-MM-dd"); //当前日期
			String nowMonth = nowDate.substring(5, 7);//当前日期月份
			
			//人均收入的校验延迟一个季度 每年前四个月的年收入应对比大前年的平均人收入
			if(Integer.parseInt(nowMonth) < 4){ //每年前四个月
				calendar1.add(Calendar.YEAR, -1);
				nowDate1 = calendar1.getTime();	//三号文获取当前日期前2年日期
				compareDate = sdf.format(nowDate1);	//对比日期
			}
			
			
			cLogger.debug("RiskCode = " + tRiskCode);
//			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			//交行、江苏银行三号文校验
			if("57".equals(mBaseInfo.getChildText(BankCode))||"10".equals(mBaseInfo.getChildText(BankCode))){
				ExeSQL pExeSQL = new ExeSQL();
				String pSQL;
				SSRS pSSRS= new SSRS();
				if("1".equals(mCountyType)){	//城镇
					pSQL = "select RecentPCDI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
					pSSRS = pExeSQL.execSQL(pSQL);
					if(pSSRS.MaxRow > 0){
						String tRecentPCDI = pSSRS.GetText(1, 1);
						if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCDI)){
							throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
						}
					}else {
						throw new MidplatException("未录入当地人均收入，请检查！！！");
					}	
				}else if("2".equals(mCountyType)){	//乡村
					pSQL = "select RecentPCNI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
					pSSRS = pExeSQL.execSQL(pSQL);
					if(pSSRS.MaxRow > 0){
						String tRecentPCNI = pSSRS.GetText(1, 1);
						if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCNI)){
							throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
						}
					}else {
						throw new MidplatException("未录入当地人均收入，请检查！！！");
					}	
				}
			}
			
			//获取福佑相伴按地区销售套餐编码
			if("511101".equals(tRiskCode)){
				String zSQL = "select riskwrapcode,enableflag from lkratemapping where bankcode = '" + mBaseInfo.getChildText(BankCode) + "' and managecom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' with ur";
				SSRS pSSRS= new SSRS();
				pSSRS = new ExeSQL().execSQL(zSQL);
				if(pSSRS.MaxRow != 0){
					if("Y".equals(pSSRS.GetText(1, 2).trim())){
						mRisk.getChild(MainRiskCode).setText(pSSRS.GetText(1, 1));
					}else{
						throw new MidplatException("该地区" + pSSRS.GetText(1, 1) + "套餐已停售或未启用");
					}
				}else{
					mRisk.getChild(MainRiskCode).setText("YBT006");
				}
			}
//			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
//			String tRiskCode = pInXmlDoc.getRootElement().getChild(LCCont)
//				.getChild(LCInsureds).getChild(LCInsured)
//				.getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
			cLogger.debug("RiskCode = " + tRiskCode);
			
//			 Element tThisConfRoot = cThisBusiConf.getParent();
			if (-1 != tThisConfRoot.getChildText("StopSale").indexOf(tRiskCode)) {
				throw new MidplatException("该产品已停售");
			}
			//停售校验 add by GaoJinfu 20180329
			String status = ycheck.riskStopCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"));
			if (status.equals("2")) {
				throw new MidplatException("该产品已停售!");  
			}
			boolean gxFlag = ycheck.riskValidCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"),"GX");
			boolean wnFlag = ycheck.riskValidCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"),"WN");
			
			
//			if (tRiskCode.equals("230701")||tRiskCode.equals("240501")||tRiskCode.equals("730101") || tRiskCode.equals("730201") || tRiskCode.equals("340401")) {	//健康专家个人防癌疾病保险(标准个险)
			 if (-1 != tThisConfRoot.getChildText("GX_Risk").indexOf(tRiskCode) || gxFlag) {	//走标准个险流程
				//投保单录入 
				YbtContBL ttYbtContBL = new YbtContBL(
						pInXmlDoc, mGlobalInput);
				ttYbtContBL.deal();
				
				//被保人、险种信息录入
				YbtInsuredProposalBL ttYbtInsuredProposalBL = new YbtInsuredProposalBL(
						pInXmlDoc, mGlobalInput);
				ttYbtInsuredProposalBL.deal();
				
				//自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();
				
				//组织返回报文
				String ttSQL = "select contno from lccont where prtno='" + mLCCont.getChildText("PrtNo") + "' with ur";
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						new ExeSQL().getOneValue(ttSQL));
				mOutXmlDoc = ttYbtContQueryBL.deal();
//			} else if (tRiskCode.equals("331201")	//健康人生个人护理保险(万能型，B款)
//					|| tRiskCode.equals("331301")	//健康人生个人护理保险(万能型，C款)
//					|| tRiskCode.equals("331601")	//健康人生个人护理保险(万能型，D款)
//					|| tRiskCode.equals("331701")
//					|| tRiskCode.equals("332601") 	//健康人生个人护理保险（万能型，F款）
//					|| tRiskCode.equals("333501")  	// 百万安行
//					|| tRiskCode.equals("333701")	//福利双全 
//					|| tRiskCode.equals("334001")	//福惠双全 
//					|| tRiskCode.equals("334901")	//福利双全B款 2016-10-17 add by zengzm 
//					|| tRiskCode.equals("232401")	//健康天使      2016-10-18    add by zengzm
//					) {	
			} else if (-1 != tThisConfRoot.getChildText("WN_Risk").indexOf(tRiskCode) || wnFlag) {	//走picch特有的国际贸易流程
				 
				//投保单录入
				YbtContBL ttYbtContBL = new YbtContBL(
						pInXmlDoc, mGlobalInput);
				ttYbtContBL.deal();
				
				//被保人、险种信息录入
				YbtContInsuredIntlBL ttYbtContInsuredIntlBL = new YbtContInsuredIntlBL(
						pInXmlDoc, mGlobalInput);
				ttYbtContInsuredIntlBL.deal();
				
				//自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();
				
				//组织返回报文
				String ttSQL = "select contno from lccont where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						new ExeSQL().getOneValue(ttSQL));
				mOutXmlDoc = ttYbtContQueryBL.deal();
			} else if (tRiskCode.equals("330501")	//常无忧B
					|| tRiskCode.equals("330701")) {	//万能险A
				throw new MidplatException("该产品已停售！");
			} else {
				throw new MidplatException("险种代码有误！" + tRiskCode);
			}
			
			//增加所有渠道的电话号码重复性校验 add by LiXiaoLong 2014-07-07 begin
			String mAppntNo = mOutXmlDoc.getRootElement().getChild(LCCont).getChild(LCAppnt).getChildText(AppntNo);
			BlackPhoneCheck.checkAllPhone(mAppntNo, mAppntPhone, mAppntOfficePhone, mAppntMobile);
			
			//未做系统录入(客户姓名、性别、证件类型（身份证或其他）、证件号码、录入有效期)且超过20万的保单，做拒保处理  add by gcy 20180205
			/*double mPrem = Double.parseDouble(mOutXmlDoc.getRootElement().getChild("LCCont").getChildTextTrim("Prem"));
			if(mPrem > Double.parseDouble("200000.00")){
				Element mLCAppnt = mOutXmlDoc.getRootElement().getChild("LCCont").getChild("LCAppnt");
				String mSQL = "select Name,Sex,IDType,IDNo,EffEctiveDate from LCYBTCustomer where Name='"+mLCAppnt.getChildText("AppntName")+"'" +
						" and IDType='"+mLCAppnt.getChildText("AppntIDType")+"' and IDNo='"+mLCAppnt.getChildText("AppntIDNo")+"'" +
								" order by makedate desc,maketime desc with ur";
				SSRS tSSRS = new ExeSQL().execSQL(mSQL);
				if(tSSRS.getMaxRow()<1){
					throw new MidplatException("累计保费超限，需保险公司后台登记");
				}
				
				if("".equals(tSSRS.GetText(1, 1)) || "".equals(tSSRS.GetText(1, 2)) 
						|| "".equals(tSSRS.GetText(1, 3)) || "".equals(tSSRS.GetText(1, 4))
						|| "".equals(tSSRS.GetText(1, 5))){
					throw new MidplatException("累计保费超限，需保险公司后台登记");
				}
				
				if(tSSRS.GetText(1, 5) != null && !"".equals(tSSRS.GetText(1, 5))){
					FDate fDate = new FDate();
					Date effectiveDate = fDate.getDate(tSSRS.GetText(1, 5));
					Date tranDate = fDate.getDate(mBaseInfo.getChildText("BankDate"));
//					if (fDate.mErrors.needDealError())
//					{
//						throw new MidplatException("累计保费超限，需保险公司后台登记");
//					}
					
					GregorianCalendar tCalendar = new GregorianCalendar();
					tCalendar.setTime(tranDate);
					int sYears = tCalendar.get(Calendar.YEAR);
					int sMonths = tCalendar.get(Calendar.MONTH);
					int sDays = tCalendar.get(Calendar.DAY_OF_MONTH);

					GregorianCalendar eCalendar = new GregorianCalendar();
					eCalendar.setTime(effectiveDate);
					int eYears = eCalendar.get(Calendar.YEAR);
					int eMonths = eCalendar.get(Calendar.MONTH);
					int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
					
					tCalendar.set(sYears, sMonths, sDays);
					eCalendar.set(eYears, eMonths, eDays);
					long lInterval = (eCalendar.getTime().getTime() - tCalendar.getTime().getTime()) / 86400000;
					if(lInterval < 0 || lInterval>31){
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
				}
			}*/
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		//试算交易并非一个完整交易，要求无论交易成功与否都删除所有数据
		if (mDeleteable) {
			try {
				cLogger.info("Start delete Trial data...");
				YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
				cLogger.info("End delete Trial data！");
			} catch (Exception ex) {
				cLogger.error("删除新单数据失败！", ex);
			}
		}
		
		cLogger.info("Out Trial.service()!");
		return mOutXmlDoc;
	}
	
	
	/**
	 * 计算投保人年龄
	 * */
    public  int getAppntAge (String ageDate) {
    	
    	
    	if(ageDate.indexOf("-")>0){
    		ageDate = ageDate.replaceAll("-", "");
    	}
    	Date now = new Date();
		String sdf = new SimpleDateFormat("yyyyMMdd").format(now);
		
		int age = 0;
		int staY = Integer.parseInt(ageDate.substring(0,4));
		int staM = Integer.parseInt(ageDate.substring(4,6));
		int staD = Integer.parseInt(ageDate.substring(6,8));
		
		int nowY = Integer.parseInt(sdf.substring(0,4));
		int nowM = Integer.parseInt(sdf.substring(4,6));
		int nowD = Integer.parseInt(sdf.substring(6,8));
		
		if(staM > nowM){
			age = nowY - staY -1;
		}else if(staM == nowM) {
			if(nowD > staD){
				age = nowY - staY ;
			}else if(nowD == staD) {
				age = nowY - staY ;
			}else if(nowD < staD) {
				age = nowY - staY -1 ;
			}
				
		}else if(staM < nowM) {
			age = nowY - staY ;
		}
		
    	return age;
    }
	private String checkAddre(Element xLCCont){
		cLogger.info("Into Trial.checkAddre()...");
		String result = "";
		
		Element xAppnt = xLCCont.getChild(LCAppnt);
		Element xInsured = xLCCont.getChild(LCInsureds).getChild(LCInsured);
		
		String xAppntMailAddr = xAppnt.getChildText("MailAddress");
		String xAppntHomeAddr = xAppnt.getChildText("HomeAddress");
		String xInsuredHomeAddr = xInsured.getChildText("HomeAddress");
		String xInsuredMailAddr = xInsured.getChildText("MailAddress");
		
		if("".equals(xAppntMailAddr) || xAppntMailAddr == null){
			result = "投保人邮寄地址不能为空！";
			return result;
		}else {
			if(xAppntMailAddr.length() < 12){
				result = "投保人邮寄地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xAppntHomeAddr) || xAppntHomeAddr == null){
			result = "投保人联系地址不能为空！";
			return result;
		}else {
			if(xAppntHomeAddr.length() < 12){
				result = "投保人联系地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xInsuredHomeAddr) || xInsuredHomeAddr == null){
			result = "被保人联系地址不能为空！";
			return result;
		}else {
			if(xInsuredHomeAddr.length() < 12){
				result = "被保人联系地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xInsuredMailAddr) || xInsuredMailAddr == null){
			result = "被保人邮寄地址不能为空！";
			return result;
		}else {
			if(xInsuredMailAddr.length() < 12){
				result = "被保人邮寄地址长度不足12位！"; 
				return result;
			}
		}
		cLogger.info("Out Trial.checkAddre()...");
		return result;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "F:\\000gaojinfu\\bankMassage\\icbc\\instd\\123.xml";
		String mOutFile = "F:/out.xml";
		
		String elpath = "F:\\sinosoft\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		Element root = c.getRootElement().getChild("business");
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new Trial(root).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
//		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}

