package com.sinosoft.midplat.kernel.service;

import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_QueryCustDetail extends ServiceImpl  {

	public CCB_QueryCustDetail(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_QueryDetail.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		String mSQL = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			Element mLCCont = pInXmlDoc.getRootElement().getChild("LCCont");
			Element mlcappnt = pInXmlDoc.getRootElement().getChild("LCCont").getChild("LCAppnt");

			if(mlcappnt.getChildTextTrim("AppntName").equals("") ||mlcappnt.getChildTextTrim("AppntName") ==null ||
			  mlcappnt.getChildTextTrim("AppntIDType").equals("") ||mlcappnt.getChildTextTrim("AppntIDType") ==null ||
			  mlcappnt.getChildTextTrim("AppntIDNo").equals("") ||mlcappnt.getChildTextTrim("AppntIDNo") ==null ){
				throw new MidplatException("相关号码不能为空！");
			}
			
			mSQL = "select * from lccont where AppntIDNo = '" + mlcappnt.getChildTextTrim("AppntIDNo") + "' and AppntName = '" + mlcappnt.getChildTextTrim("AppntName") + "' and AppntIDType = '" + mlcappnt.getChildTextTrim("AppntIDType") + "' and signdate  between '" + mLCCont.getChildTextTrim("StartDate") + "' and '" + mLCCont.getChildTextTrim("EndDate") + "' with ur";
			if(new ExeSQL().getOneValue(mSQL).equals("")){
				throw new MidplatException("无客户投保信息！");
			}else {
				mOutXmlDoc = deal(mlcappnt,mLCCont);
			}
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_QueryDetail.service()!");
		return mOutXmlDoc;
	}

	private Document deal(Element mlcappnt, Element pLCCont) throws Exception {
		cLogger.info("Out CCB_QueryDetail.service()!");
		
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mLCCont = new Element("LCCont");
		
		Element mChkDetails = null;
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setAppntIDNo(mlcappnt.getChildTextTrim("AppntIDNo"));
		mLCContDB.setAppntName(mlcappnt.getChildTextTrim("AppntName"));
		mLCContDB.setAppntIDType(mlcappnt.getChildTextTrim("AppntIDType"));
		
		String mSQL = "select * from lccont where AppntIDNo = '" + mlcappnt.getChildTextTrim("AppntIDNo") + "' and AppntName = '" + mlcappnt.getChildTextTrim("AppntName") + "' and AppntIDType = '" + mlcappnt.getChildTextTrim("AppntIDType") + "' and signdate  between '" + pLCCont.getChildTextTrim("StartDate") + "' and '" + pLCCont.getChildTextTrim("EndDate") + "' with ur";
		LCContSet mLCContSet =  mLCContDB.executeQuery(mSQL);
		for (int i = 1; i <= mLCContSet.size(); i++) {
			LCContSchema mLCContSchema = mLCContSet.get(i);
			
			String tContno = mLCContSchema.getContNo();
			String mPrem = mLCContSchema.getPrem() + "";
			
			LCPolDB mLCPolDB = new LCPolDB();
			mLCPolDB.setContNo(tContno);
			LCPolSet mLCPolSet = mLCPolDB.query();
			
			if(mLCPolSet.size()<1 || mLCPolSet == null){
				throw new MidplatException("查询保单数据失败！");
			}else {
				for(int j = 1; j <= mLCPolSet.size(); j++){
					LCPolSchema mLCPolSchema = mLCPolSet.get(j);
					String tRiskCodeStr = mLCPolSchema.getRiskCode();
					mChkDetails = new Element("ChkDetails");
					
					if (tRiskCodeStr.equals("530701")	//附加健康人生个人意外伤害保险（B款）
							|| tRiskCodeStr.equals("530801")	//附加健康人生个人意外伤害保险(C款)
							|| tRiskCodeStr.equals("331401")	//附加健康专家个人护理保险
							|| tRiskCodeStr.equals("531001")	//附加健康人生个人意外伤害保险（D款）
							|| tRiskCodeStr.equals("531101")    //附加金利宝个人意外伤害保险
		                    || tRiskCodeStr.equals("1607")
		                    || tRiskCodeStr.equals("260301")
		                    || tRiskCodeStr.equals("533301")
		                    || tRiskCodeStr.equals("335201")
		                    || tRiskCodeStr.equals("531301")) {	//附加康利相伴个人意外伤害保险
						continue;
		            }
					Element tRiskCode = new Element(RiskCode);
					tRiskCode.setText(mLCPolSchema.getRiskCode());
					
					Element tRiskName = new Element(RiskName);
					mSQL = "select RiskName from lmrisk where riskcode='" + mLCPolSchema.getRiskCode() + "' with ur";
					tRiskName.setText(new ExeSQL().getOneValue(mSQL));
					
					Element tMainRiskCode = new Element(MainRiskCode);
					if("333001".equals(mLCPolSchema.getRiskCode()) || "532201".equals(mLCPolSchema.getRiskCode())){ //万能型H区分主副险
						tMainRiskCode.setText("333001");
					}else if("333501".equals(mLCPolSchema.getRiskCode()) || "532501".equals(mLCPolSchema.getRiskCode()) || "532401".equals(mLCPolSchema.getRiskCode())){ //万能型H区分主副险
					tMainRiskCode.setText("333501");
					}else if("730101".equals(mLCPolSchema.getRiskCode()) || "332401".equals(mLCPolSchema.getRiskCode())){ //康利人生两全保险（分红型）
					//康利人生两全分红型需区分主险附加险
					String mSQL2 = "select riskcode from lcpol where contno='" + mLCPolSchema.getContNo() + "' and polno = mainpolno with ur";
					String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
					tMainRiskCode.setText(mMainRiskCode);
					}else if("231701".equals(mLCPolSchema.getRiskCode()) || "333801".equals(mLCPolSchema.getRiskCode())){ //康乐人生个人重大疾病保险（A款）
					tMainRiskCode.setText("231701");
					}else if("231401".equals(mLCPolSchema.getRiskCode())){ //附加豁免保费个人疾病保险
					tMainRiskCode.setText("231401");
					}else{
					//其余产品区分主附险
					String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '" + mLCPolSchema.getContNo() + "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
					String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
					tMainRiskCode.setText(mMainRiskCode);
					}
					
					Element tPrem = new Element(Prem);
					
					if("232201".equals(tMainRiskCode.getText()) || "232401".equals(tMainRiskCode.getText()) || "334901".equals(tMainRiskCode.getText()) || "333501".equals(tMainRiskCode.getText())){
						tPrem.setText(mPrem);
					}else{
						tPrem.setText(new DecimalFormat("0.00").format(mLCPolSchema.getPrem()));
					}

					Element mMakeDate = new Element("MakeDate");
					mMakeDate.setText(mLCPolSchema.getMakeDate());
					Element mStateFlag = new Element("StateFlag");
					mStateFlag.setText(mLCPolSchema.getStateFlag());
					Element mContno = new Element("Contno");
					mContno.setText(tContno);
					
					mChkDetails.addContent(tRiskCode);
					mChkDetails.addContent(tRiskName);
					mChkDetails.addContent(tMainRiskCode);
					mChkDetails.addContent(tPrem);
					mChkDetails.addContent(mContno);
					mChkDetails.addContent(mMakeDate);
					mChkDetails.addContent(mStateFlag);
					
					mLCCont.addContent(mChkDetails);
				}
			}
		}
		
		Element mFlag = new Element("Flag");
		mFlag.setText("1");
		Element mDesc = new Element("Desc");
		mDesc.setText("交易成功！");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		
		cLogger.info("Out CCB_QueryDetail.service()!");
		return new Document(mTranData);
	}
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into CCB_QueryDetail.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_QueryDetail.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
