package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;


import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;


public class ICBC_HesitatePreResultBL extends ServiceImpl  {

	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	private Element mBaseInfo = null;
	
	public ICBC_HesitatePreResultBL(Element thisBusiConf) {
		super(thisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_HesitatePreResultBL.service()...");
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		LKTransStatusDB mLKTransStatusDB = null;	

		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			//业务处理
			if("101".equals(mBaseInfo.getChildText("FunctionFlag"))){ //犹豫期退保
				mOutXmlDoc = deal();
			}	
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");//执行任务完毕记
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out ICBC_HesitatePreResultBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal() throws MidplatException {
		cLogger.info("Into ICBC_HesitatePreResultBL.deal()...");
			
		Element mTranData = new Element("TranData");//根节点
		Element mRetData = new Element("RetData");
		Element mChkDetails = new Element("ChkDetails");
		String mTransno = "";
		String mTransamnt = "";
		
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		mFlag.setText("0");
		mDesc.setText("交易成功！");

		ExeSQL mExeSQL = new ExeSQL();
		String mSql = "select Contno,Makedate,Modifydate,Appstate from LPYBTAppWT where Operator = 'ybt' and Modifydate = Current Date with ur";
		SSRS mSSRS = mExeSQL.execSQL(mSql);
		System.out.println(mSSRS.MaxRow);
		if(mSSRS.MaxRow >= 1){
			for(int i = 1 ; i <= mSSRS.MaxRow ;i++){
				String mContno = mSSRS.GetText(i, 1);
				String mMakedate = mSSRS.GetText(i, 2);
				String mModifydate = mSSRS.GetText(i, 3);
				String mAppstate = mSSRS.GetText(i, 4);
		
				String tSql = "select Transno,Transamnt from lktransstatus where bankcode = '01' and funcflag = '121' and Transno like 'ybt%' and  polno = '" + mContno + "' with ur";
				
				SSRS tSSRS = mExeSQL.execSQL(tSql);
				if(tSSRS.MaxRow >= 1){
					mTransno = tSSRS.GetText(1, 1);
					mTransamnt = tSSRS.GetText(1, 2);
				}else{
					cLogger.error(mExeSQL.mErrors.getFirstError());
					cLogger.warn("日志表中不存在该保单！");
				}
				
				Element tTranDate = new Element(TranDate);	//交易日期
				tTranDate.setText(mModifydate);
				
				Element tType = new Element(Type);//业务种类
				tType.setText("002");
				
				Element tMakeDate = new Element("MakeDate");	//银行受理日期
				tMakeDate.setText(mMakedate);
				
				Element tTransrNo = new Element(TransrNo);
				tTransrNo.setText(mTransno.substring(3));
				
				Element tCardNo = new Element(CardNo);
				tCardNo.setText(mContno);
				
				Element tTranAmnt = new Element(TranAmnt);
				tTranAmnt.setText(mTransamnt);

				Element tAppstate = new Element("Appstate");	//处理状态
				tAppstate.setText(mAppstate);
				
				Element tDescr = new Element("Descr");
			
				Element mChkDetail =  new Element("ChkDetail");
				
				mChkDetail.addContent(tType);
				mChkDetail.addContent(tTranDate);
				mChkDetail.addContent(tMakeDate);
				mChkDetail.addContent(tDescr);
				mChkDetail.addContent(tTransrNo);
				mChkDetail.addContent(tCardNo);
				mChkDetail.addContent(tTranAmnt);
				mChkDetail.addContent(tAppstate);
				
				mChkDetails.addContent(mChkDetail);
			}		
		}else {
			mFlag.setText("1");
			mDesc.setText("交易失败！");
		}
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mChkDetails);
		
		cLogger.info("Out ICBC_HesitatePreResultBL.deal()...");
		return new Document(mTranData);
	}
	
	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		cLogger.info("Into ICBC_HesitatePreResultBL.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out ICBC_HesitatePreResultBL.insertTransLog()...");
		return mLKTransStatusDB;
	}

}
