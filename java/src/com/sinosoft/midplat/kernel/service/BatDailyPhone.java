package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKBlackPhoneSchema;
import com.sinosoft.lis.vschema.LKBlackPhoneSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
/***
 * @date 2012-7-19
 * @author Administrator
 * @title 每日数据提取，将满足条件的数据存于中间表，在录单时进行电话号码校验
 */
public class BatDailyPhone extends ServiceImpl {

	public BatDailyPhone(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into BatDailyPhone.service()...");
		Document mOutXmlDoc = null;
		
		try {
			// 进行业务处理
			mOutXmlDoc = deal();
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+" 交易失败！", ex);
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		cLogger.info("Out BatDailyPhone.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal() throws MidplatException{
		cLogger.info("Into BatDailyPhone.deal()...");
		
		//获得当前系统时间
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		// 根据SQL 查出满足条件的所有数据 将其封装成一个set
		String mSQL = "Select Cad.Mobile From Lcappnt Ca"
					+" Inner Join Lcaddress Cad On Ca.Appntno = Cad.Customerno"
					+" And Ca.Addressno = Cad.Addressno where not exists "
					+" (select 1 from lkblackphone where phone = cad.mobile) and cad.mobile <> '' and cad.mobile is not null"
					+" Group By Cad.Mobile  Having Count(Distinct Ca.Appntname) > 1 With Ur";
		SSRS ssrs = new ExeSQL().execSQL(mSQL);
		
		LKBlackPhoneSet lkMobileSet = new LKBlackPhoneSet();
		//将set里的值循环丢入中间表
		int a = ssrs.MaxRow;
		
		for (int i = 1; i <= ssrs.MaxRow; i++) {
			LKBlackPhoneSchema lkPhone = new LKBlackPhoneSchema();
			String mPhone = ssrs.GetText(i, 1);
			if(mPhone == null){
				mPhone = "";
			}
			lkPhone.setPhone(mPhone);
			lkPhone.setOperator("ybt");
			lkPhone.setMakeDate(mCurrentDate);
			lkPhone.setMakeTime(mCurrentTime);
			lkPhone.setModifyDate(mCurrentDate);
			lkPhone.setModifyTime(mCurrentTime);
			
			lkMobileSet.add(lkPhone);
		}
		//插入数据库
		MMap mMMap = new MMap();
        mMMap.put(lkMobileSet, "INSERT");
        VData mVData = new VData();
        mVData.add(mMMap);
        PubSubmit mPubSubmit = new PubSubmit();
        if (!mPubSubmit.submitData(mVData, "")) {
            cLogger.error(mPubSubmit.mErrors.getFirstError());
            throw new MidplatException("提交每日数据失败！");
        }
        
        
     // 根据SQL 查出满足条件的所有数据 将其封装成一个set
		String mSQL_phone = "Select Cad.phone From Lcappnt Ca"
					+" Inner Join Lcaddress Cad On Ca.Appntno = Cad.Customerno"
					+" And Ca.Addressno = Cad.Addressno where not exists "
					+" (select 1 from lkblackphone where phone = cad.phone) and cad.phone <> '' and cad.phone is not null"
					+" Group By Cad.phone  Having Count(Distinct Ca.Appntname) > 1 With Ur";
		ssrs = new ExeSQL().execSQL(mSQL_phone);
		
		LKBlackPhoneSet lkPhoneSet = new LKBlackPhoneSet();
		//将set里的值循环丢入中间表
		a += ssrs.MaxRow;
		cLogger.info("当日满足条件的投保人号码有："+a+" 单");
		
		for (int i = 1; i <= ssrs.MaxRow; i++) {
			LKBlackPhoneSchema lkPhone = new LKBlackPhoneSchema();
			String mPhone = ssrs.GetText(i, 1);
			if(mPhone == null){
				mPhone = "";
			}
			lkPhone.setPhone(mPhone);
			lkPhone.setOperator("ybt");
			lkPhone.setMakeDate(mCurrentDate);
			lkPhone.setMakeTime(mCurrentTime);
			lkPhone.setModifyDate(mCurrentDate);
			lkPhone.setModifyTime(mCurrentTime);
			
			lkPhoneSet.add(lkPhone);
		}
		//插入数据库
		mMMap = new MMap();
        mMMap.put(lkPhoneSet, "INSERT");
        mVData = new VData();
        mVData.add(mMMap);
        mPubSubmit = new PubSubmit();
        if (!mPubSubmit.submitData(mVData, "")) {
            cLogger.error(mPubSubmit.mErrors.getFirstError());
            throw new MidplatException("提交每日数据失败！");
        }
		
		// 拼接返回报文
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mSum = new Element("SumNum");
		mSum.setText(a+"");
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mRetData.addContent(mSum);
		mTranData.addContent(mRetData);
		cLogger.info("Out BatDailyPhone.deal()...");
		return new Document(mTranData);
	}

}
