package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vdb.LKTransStatusDBSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class NXS_RePrint extends ServiceImpl {
	public NXS_RePrint(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into NXS_RePrint.service()...");
		
		String bankCode = pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BankCode");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLCCont.getChildText(ContNo));
			tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
			if (tLCContDB.getContNo().equals("")
					&& tLCContDB.getPrtNo().equals("")) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if ((null == tLCContSet) || (tLCContSet.size() < 1)) {
				throw new MidplatException("查询保单数据失败！");
			}
			LCContSchema tLCContSchema = tLCContSet.get(1);

			if (!tLCContSchema.getAppFlag().equals("1")) {
				throw new MidplatException("该保单不处于承保状态！");
			}

			if (!tLCContSchema.getSignDate().equals(
					DateUtil.getCurDate("yyyy-MM-dd"))) {
				throw new MidplatException("非当日保单，不能重打！");
			}

			String tCertifyCode = null;
			String tProposalContNoNew = null;
			String tProposalContNoOld = null; // 原单证号码
			if ("a".equals(tLCContSchema.getCardFlag())) {
				mGlobalInput = YbtSufUtil.getGlobalInput1(mLKTransStatusDB
						.getBankCode(), mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());

				String mSQL = "select RiskCode from LKTransStatus where PolNo = '"
						+ tLCContSchema.getContNo()
						+ "' "
						+ "and BankCode = '"
						+ tLCContSchema.getBankCode()
						+ "' "
						+ "and FuncFlag = '01' and Status = '1' with ur";
				String mRiskWrapCode = new ExeSQL().getOneValue(mSQL);// 信保通riskcode字段存储的是套餐编码
				// 查询得到单证编码
				ExeSQL mExeSQL = new ExeSQL();
				String tSQL = "select certifycode from lkcertifymapping where bankcode = '"
						+ tLCContSchema.getBankCode()
						+ "' "
						+ "and managecom = '"
						+ mGlobalInput.ManageCom.substring(0, 4)
						+ "' "
						+ "and RiskWrapCode = '"
						+ mRiskWrapCode
						+ "' "
						+ "with ur";

				SSRS tSSRS = mExeSQL.execSQL(tSQL);
				if (tSSRS.getMaxRow() < 1) {
					throw new MidplatException("未查询到对应的单证编码!");
				}
				tCertifyCode = tSSRS.GetText(1, 1);
				// 农信社必须取后十位
				tProposalContNoNew = mLCCont.getChildText(ProposalContNo)
						.substring(2, 12);
				tProposalContNoOld = tLCContSchema.getProposalContNo()
						.substring(2, 12); // 原单证号
			} else if ("9".equals(tLCContSchema.getCardFlag())) {
				mGlobalInput = YbtSufUtil.getGlobalInput(mLKTransStatusDB
						.getBankCode(), mLKTransStatusDB.getBankBranch(),
						mLKTransStatusDB.getBankNode());

				tCertifyCode = "HB07/07B";
				tProposalContNoNew = mLCCont.getChildText(ProposalContNo);
				tProposalContNoOld = tLCContSchema.getProposalContNo(); // 原单证号
			}
			CardManage tCardManage = new CardManage(tCertifyCode, mGlobalInput);
			if (!mLCCont.getChildText(ProposalContNo).startsWith("YBT")) {
				if (!tCardManage.canBeUsed(tProposalContNoNew)) {
					throw new MidplatException("保单合同印刷号未正确下发，或状态无效！");
				}
			}

			tLCContDB.setSchema(tLCContSchema);
			tLCContDB.setProposalContNo(mLCCont.getChildText(ProposalContNo));
			tLCContDB.setPrintCount(tLCContSchema.getPrintCount() + 1);
			if (!tLCContDB.update()) {
				throw new MidplatException("更新保单印刷号失败！");
			}

			// 核销新单证！
			if (!tLCContDB.getProposalContNo().startsWith("YBT")) {
				tCardManage.makeUsed(tProposalContNoNew);
			}
			// 作废原单证！
			if (!tLCContSchema.getProposalContNo().startsWith("YBT")) {
				tCardManage.invalidated(tProposalContNoOld);
			}

			// 组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tLCContSchema
					.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();
			
			if(bankCode.equals("49")){
					
				String ssql = "select ServiceStartTime,ServiceEndTime,TempFeeNo,bak3,EdorNo from lktransStatus where bankcode = '"+mLKTransStatusDB.getBankCode()+"'" +
				" and BankBranch = '"+mLKTransStatusDB.getBankBranch()+"' and BankNode = '"+mLKTransStatusDB.getBankNode()+"'" +
				" and prtno = '"+tLCContSchema.getPrtNo()+"' and RCode = '1' and FuncFlag = '01' order by makedate desc , maketime desc with ur";
				
				ExeSQL mExeSQL = new ExeSQL();
				SSRS mSSRS = mExeSQL.execSQL(ssql);				
				
				Element xLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			    xLCCont.getChild("LoanDate").setText(mSSRS.GetText(1, 1));
			    xLCCont.getChild("LoanEndDate").setText(mSSRS.GetText(1, 2));
			    xLCCont.getChild("LoanInvoiceNo").setText(mSSRS.GetText(1, 3));
			    xLCCont.getChild("ConsignNo").setText(mSSRS.GetText(1, 4));
			    xLCCont.getChild("LoanContractAmt").setText(mSSRS.GetText(1, 5));
				   
			}
			
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setbak1(tLCContSchema.getProposalContNo());
			mLKTransStatusDB.setbak2(tLCContSchema.getPrintCount() + "");
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out NXS_RePrint.service()!");
		return mOutXmlDoc;
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into NXS_RePrint.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out NXS_RePrint.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
