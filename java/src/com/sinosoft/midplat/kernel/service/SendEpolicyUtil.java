package com.sinosoft.midplat.kernel.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKElectronContDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LKElectronContSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LKElectronContSet;
import com.sinosoft.lis.yibaotong.ConfigFactory;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import common.Logger;


public class SendEpolicyUtil  {
	protected final Logger cLogger = Logger.getLogger(getClass());
	private String Contno="";
	Socket socket = null;
	private String newMailAddress="";
	 Element mChkDetail = null;
	public SendEpolicyUtil(String ContNo,String newMailAddress){
		this.Contno = ContNo;
		this.newMailAddress = newMailAddress;
	}
	
	public  byte [] receive(InputStream inputStream){
		byte[] by = null;
		try {
			by = JdomUtil.toBytes(JdomUtil.build(inputStream));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     return by;
	} 
	public Document getInNoStdXml(byte[] pBytes){
		Document inputDoc = JdomUtil.build(pBytes);
		return inputDoc;
	}
	//getInStdXml
	public Document getInStdXml(Document pInNoStdXml){
		return pInNoStdXml;
	}
	

	public String service() {
		cLogger.info("Into SendEpolicyUtil.service()...");

		String ret_Str ="";   //返回详细信息,全局
 
		try {
			String cx_sql = "select  prtno,ContNo,makedate from lccont where ContNo = '"+this.Contno +"'";

			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = null;
			tSSRS = tExeSQL.execSQL(cx_sql);
            if(tSSRS.MaxRow !=1){
            	throw new MidplatException("该保单已失效,无法发送电子保单!");
            }

			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

//				String mPrtno = tSSRS.GetText(i, 1);
				String mPolno = tSSRS.GetText(i, 2);
				String tTransDate = tSSRS.GetText(i, 3);
				
                cLogger.info("前台传入保单号:"+this.Contno);
                cLogger.info("前台传入邮箱地址:"+this.newMailAddress);
				cx_sql = "select email From LCAddress where addressNo in(select addressNo From lcappnt where contno = '"
						+ mPolno + "') and  customerno in (select appntno From lcappnt where contno = '"
						+ mPolno + "') and makedate = '" + tTransDate + "' with ur";
				
				System.out.println("投保人Email:"+ new ExeSQL().getOneValue(cx_sql));
				
				
				LCPolDB mLCPolDB = new LCPolDB();
				mLCPolDB.setContNo(mPolno);
				LCPolSet mLCPolSet = mLCPolDB.query();

				if ((null == mLCPolSet) || (mLCPolSet.size() < 1)) {
					cLogger.error(mLCPolDB.mErrors.getFirstError()); 
					throw new MidplatException("查询险种信息失败！");
				}

				Element mRisks = new Element("Risks");

				for (int j = 1; j <= mLCPolSet.size(); j++) {
					LCPolSchema tLCPolSchema = (LCPolSchema) mLCPolSet.get(j);

					Element tRiskName = new Element("RiskName");
					String mSQL = "select RiskName from lmrisk where riskcode='"
							+ tLCPolSchema.getRiskCode() + "' with ur";
					tRiskName.setText(new ExeSQL().getOneValue(mSQL));

					Element tRiskCode = new Element("RiskCode");
					tRiskCode.setText(tLCPolSchema.getRiskCode());

					Element tMainRiskCode = new Element("MainRiskCode");
					if ("333001".equals(tLCPolSchema.getRiskCode())
							|| "532201".equals(tLCPolSchema.getRiskCode())) { // 万能型H区分主副险
						tMainRiskCode.setText("333001");
					} else if ("333501".equals(tLCPolSchema.getRiskCode())
							|| "532501".equals(tLCPolSchema.getRiskCode())
							|| "532401".equals(tLCPolSchema.getRiskCode())) { // 万能型H区分主副险
						tMainRiskCode.setText("333501");
					} else if ("730101".equals(tLCPolSchema.getRiskCode())
							|| "332401".equals(tLCPolSchema.getRiskCode())) { // 康利人生两全保险（分红型）
						// 康利人生两全分红型需区分主险附加险
						String mSQL2 = "select riskcode from lcpol where contno='"
								+ tLCPolSchema.getContNo()
								+ "' and polno = mainpolno with ur";
						String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
						tMainRiskCode.setText(mMainRiskCode);
					} else if ("231701".equals(tLCPolSchema.getRiskCode())
							|| "333801".equals(tLCPolSchema.getRiskCode())) { // 康乐人生个人重大疾病保险（A款）
						tMainRiskCode.setText("231701");
					} else if ("231401".equals(tLCPolSchema.getRiskCode())) { // 附加豁免保费个人疾病保险
						tMainRiskCode.setText("231401");
					} else {
						// 其余产品区分主附险
						String mSQL2 = "select pol.riskcode from lcpol pol where pol.contno = '"
								+ tLCPolSchema.getContNo()
								+ "' and 'M' = (select subriskflag from lmriskapp where riskcode = pol.riskcode) with ur";
						String mMainRiskCode = new ExeSQL().getOneValue(mSQL2);
						tMainRiskCode.setText(mMainRiskCode);
					}
					Element tRisk = new Element("Risk");
					tRisk.addContent(tRiskCode);
					tRisk.addContent(tRiskName);
					tRisk.addContent(tMainRiskCode);

					mRisks.addContent(tRisk);
				}
             
				mChkDetail = new Element("ChkDetail");
				Element mContNo = new Element("ContNo");
				Element mEmail = new Element("Email");
				Element nTransDate = new Element("TransDate");

				mContNo.setText(mPolno);
				//此处添加判断,如果从前台传入的新邮件不为空,及使用新邮箱覆盖
				if(null !=newMailAddress && !"".equals(newMailAddress))
				{
					mEmail.setText(newMailAddress);
				}
				else{
				mEmail.setText(new ExeSQL().getOneValue(cx_sql));
				}
				nTransDate.setText(tTransDate);

				mChkDetail.addContent(mContNo);
				mChkDetail.addContent(mEmail);
				mChkDetail.addContent(nTransDate);
				mChkDetail.addContent(mRisks);
				Document outStdDoc =  new  Document(mChkDetail);
				JdomUtil.print(outStdDoc);
				//以上主要做了查询险种信息的工作
				//一下主要开始发送电子保单
				String str = sendMail(outStdDoc);
				ret_Str = str;
			}
		} catch (Exception ex) {
			ret_Str = ex.toString();
		}

		cLogger.info("Out ICBC_SendWxPrint.service()...");
		return ret_Str;
	}

	public String sendMail(Document pChkDetail){
		        String message = "Hello!";   //主题
		        String mLocalDir ="";        //电子保单存放路劲
				String mContNo = pChkDetail.getRootElement().getChildText("ContNo");
				String  ret_str = "";
				try {
				   //开始访问前置机socket,发送数据
					System.out.println("1.组织测试报文信息");
					ExeSQL exe2 = new ExeSQL();
					String sql2 = "select code from ldcode where  codetype ='send_data218'";
					SSRS sr = exe2.execSQL(sql2);
					String str2 = sr.GetText(1, 1);
					String [] str_list  = str2.split(":"); 
					String  ip = str_list[0];
					String  port = str_list[1];
					int port_int = Integer.parseInt(port);
					System.out.println(str_list[0]);
					System.out.println(str_list[1]);
					cLogger.info("连接前置机ip端口::"+str_list[0]+"端口:"+str_list[1]);
		            socket = new Socket(ip, port_int);
		            socket.setSoTimeout(60*1000);
		            
		            long mCurTimeMillis = System.currentTimeMillis();
		           
		            int len_of_com = JdomUtil.toBytes(pChkDetail).length;
		            
		            byte [] newbyte = new byte[len_of_com+8];
		            byte [] byteData = JdomUtil.toBytes(pChkDetail);
		            
		            String temp_a = "00000000".substring((len_of_com+"").length())+len_of_com;
		            
		            System.out.println(temp_a);
		            System.out.println(new String(byteData));
		            
		            
		            System.arraycopy(temp_a.getBytes(), 0, newbyte, 0, 8);
		            System.arraycopy(byteData, 0, newbyte, 8, byteData.length);
		            cLogger.info("发送数据:"+new String (newbyte));
		            
		            socket.getOutputStream().write(newbyte);
		            InputStream inputstream = socket.getInputStream();
		            Document outputDoc = JdomUtil.build(inputstream);
		            cLogger.info("接受返回报文完成...");
		            JdomUtil.print(outputDoc);
		            Element ContDetail = outputDoc.getRootElement().getChild("LCCont").getChild("ContDetail");
		            
		            
		            
					String sql = "";
					ExeSQL exe = new ExeSQL();
				
					if("1".equals(ContDetail.getChildTextTrim("ContState"))){
						sql = "update lkelectroncont set temp1 ='1' where contno ='"+mContNo+"'";
						exe.execUpdateSQL(sql);
						socket.close();
						ret_str = "发送成功";
					}
					
				} catch (Exception ex) {
					System.out.println("发送邮件异常:" + ex);
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ret_str = "发送失败";
				}
				return ret_str;
			}
	
	

	
	
	public static void main(String[] args) {
		SendEpolicyUtil   s  = new SendEpolicyUtil("032385432000001", "wangao@picchealth.com");
		s.service();
		
		
		
		
		}
}
