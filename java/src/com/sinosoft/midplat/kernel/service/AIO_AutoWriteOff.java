/**
 * 自动冲正。
 * 目前使用银行：交行。
 */

package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.writeOff.YbtWriteOffBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AIO_AutoWriteOff extends ServiceImpl {
	public AIO_AutoWriteOff(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into AIO_AutoWriteOff.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;

		Element mLCCont = pInXmlDoc.getRootElement().getChild(LCCont);

		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			MMap tSubmitMMap = new MMap();
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(mLCCont.getChildText(ContNo));
			tLCContDB.setPrtNo(mLCCont.getChildText(PrtNo));
			tLCContDB.setProposalContNo(mLCCont.getChildText(ProposalContNo));
			if (tLCContDB.getContNo().equals("")
					&& tLCContDB.getPrtNo().equals("")
					&& tLCContDB.getProposalContNo().equals("")) {
				throw new MidplatException("相关号码不能为空！");
			}
			LCContSet tLCContSet = tLCContDB.query();
			if (tLCContDB.mErrors.needDealError()) {
				cLogger.error(tLCContDB.mErrors.getFirstError());
				throw new MidplatException("查询保单数据异常！");
			}
			
			if (tLCContSet.size() > 0) {
				LCContSchema ttLCContSchema = tLCContSet.get(1);
				
				String uSQL = "select 1 from lktransstatus where PrtNo='" + ttLCContSchema.getPrtNo() + "' and rcode is null and funcflag != '03' with ur";
				if ("1".equals(new ExeSQL().getOneValue(uSQL))) {
					throw new MidplatException("此保单数据被别的交易挂起，请稍后冲正！");
				}
				
				YbtWriteOffBL ttYbtWriteOffBL =new YbtWriteOffBL(ttLCContSchema, mGlobalInput);
				ttYbtWriteOffBL.deal();
				
				String tCertifyCode = null;
				String tProposalContNoNew = null;//对单证进行操作时使用,必须有,信保通需要去掉前两位字母
				if ("a".equals(ttLCContSchema.getCardFlag())) {
					String mSQL = "select RiskCode from LKTransStatus where polno = '"
							+ ttLCContSchema.getContNo()+ "' "
							+ "and BankCode = '"+ttLCContSchema.getBankCode().substring(0,2) +"' "
							+ "and FuncFlag = '01' and Status = '1' with ur";
					String mRiskWrapCode = new ExeSQL().getOneValue(mSQL);
					// 查询得到单证编码
					ExeSQL mExeSQL = new ExeSQL();
					String tSQL = "select certifycode from lkcertifymapping where bankcode = '"
							+ ttLCContSchema.getBankCode().substring(0,2)	+ "' "
							+ "and managecom = '"+ mGlobalInput.ManageCom.substring(0, 4)+ "' "
							+ "and RiskWrapCode = '" + mRiskWrapCode + "' " 
							+ "with ur";

					SSRS tSSRS = mExeSQL.execSQL(tSQL);

					if (tSSRS.getMaxRow() < 1) {
						throw new MidplatException("未查询到对应的单证编码!");
					}
					tCertifyCode = tSSRS.GetText(1, 1);
					tProposalContNoNew = ttLCContSchema.getProposalContNo().substring(2, 12);//信保通单证操作时截取后十位
				} else if ("9".equals(ttLCContSchema.getCardFlag())) {
					tCertifyCode = "HB07/07B";
					tProposalContNoNew = ttLCContSchema.getProposalContNo();//银保通不变
				}
				
				//原来的单证号码
				String mProposalContNo = ttLCContSchema.getProposalContNo();
				CardManage mCardManage = new CardManage(tCertifyCode,
						mGlobalInput);
				if (!mProposalContNo.startsWith("YBT")) {
					mCardManage.tackBack(tProposalContNoNew);//回收单证！
				}
				
				mLKTransStatusDB.setPolNo(ttLCContSchema.getContNo());
				mLKTransStatusDB.setPrtNo(ttLCContSchema.getPrtNo());
				mLKTransStatusDB.setProposalNo(ttLCContSchema.getProposalContNo());
				mLKTransStatusDB.setTransAmnt(-ttLCContSchema.getPrem());
				
				LKTransStatusDB ttLKTransStatusDB01 = new LKTransStatusDB();
				ttLKTransStatusDB01.setPolNo(mLKTransStatusDB.getPolNo());
				ttLKTransStatusDB01.setStatus("1");
				ttLKTransStatusDB01.setFuncFlag("01");
				LKTransStatusSet ttLKTransStatusSet01 = ttLKTransStatusDB01.query();
				if (1 != ttLKTransStatusSet01.size()) {
					cLogger.error("查询新契约日志异常：" + ttLKTransStatusDB01.mErrors.getFirstError());
					throw new MidplatException("新契约日志异常！");
				}
				LKTransStatusSchema ttLKTransStatusSchema01 = ttLKTransStatusSet01.get(1);
				ttLKTransStatusSchema01.setStatus("2");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
				tSubmitMMap.put(ttLKTransStatusSchema01, "UPDATE");
			}

			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			LKTransStatusSchema tLKTransStatusSchema = mLKTransStatusDB.getSchema();
			tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
			
			VData tSubmitVData = new VData();
			tSubmitVData.add(tSubmitMMap);
			PubSubmit ttPubSubmit = new PubSubmit();
			if (!ttPubSubmit.submitData(tSubmitVData, "")) {
				cLogger.error(ttPubSubmit.mErrors.getFirstError());
				throw new MidplatException("更新日志失败！");
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}

				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		cLogger.info("Out AIO_AutoWriteOff.service()!");
		return mOutXmlDoc;
	}
	
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into AIO_AutoWriteOff.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out AIO_AutoWriteOff.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

//		String mInFile = "F:/working/picch/std/testXml/WriteOff/std_04_in.xml";
//		String mOutFile = "F:/working/picch/std/testXml/WriteOff/std_04_out.xml";
		
		String mInFile = "E:/Test-haoqt/中行资料/boc/testXml/writeOff/boc_04_in.xml";
		String mOutFile = "E:/Test-haoqt/中行资料/boc/testXml/writeOff/boc_04_out.xml";
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);

		Document mOutXmlDoc = new AIO_AutoWriteOff(null).service(mInXmlDoc);

		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(mOutXmlDoc, System.out);
		mXMLOutputter.output(mOutXmlDoc, new FileOutputStream(mOutFile));

		System.out.println("成功结束！");
	}
}
