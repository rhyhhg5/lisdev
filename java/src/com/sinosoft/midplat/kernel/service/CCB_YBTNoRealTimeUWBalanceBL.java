package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class CCB_YBTNoRealTimeUWBalanceBL  extends ServiceImpl {

	//得到系统当前时间
	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
	
	private Element mBaseInfo = null;
	public CCB_YBTNoRealTimeUWBalanceBL(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_YBTNoRealTimeUWBalanceBL.service()...");
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Document mOutXmlDoc = null;
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild(BaseInfo).clone();
		
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			Element mDetails = pInXmlDoc.getRootElement().getChild("Details");
			mOutXmlDoc = getResultDis(mDetails);  //数据处理，组织返回标准报文
			
		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", e);
			
			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}				
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out CCB_YBTNoRealTimeUWBalanceBL.service()...");
		mOutXmlDoc.getRootElement().addContent(mBaseInfo);
		return mOutXmlDoc ;
	}
	
	
	public Document getResultDis(Element details) {
		
		//1.得到保单号
		List detailList = details.getChildren("Detail");
		List contList = new ArrayList();
		System.out.println("得到请求报文中Detail节点数: "+detailList.size());
		for (int i = 0; i < detailList.size(); i++) {
			
			String mProposalContNo = ((Element)detailList.get(i)).getChildTextTrim("ProposalNo");
			if ((null==mProposalContNo || "".equals(mProposalContNo))){
				continue;
			}
			
			contList.add(mProposalContNo);
			
		}
		Document pOutStdXml = null;
		try {
			
			pOutStdXml = deal(contList);
		
		} catch (MidplatException e) {
			e.printStackTrace();
		}
		//2.将保单号作为条件查询LKNoRealTimeUWCheck表中是否有数据
		
		
		
		
		
		return pOutStdXml;
	}
	
	
	public Document deal(List contList) throws MidplatException {
		cLogger.info("Into CCB_YBTNoRealTimeUWBalanceBL.deal()...");
		
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		
		mFlag.setText("0");
		mDesc.setText("交易成功！");
		
		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		
		
		Element mDetailList = new Element("DetailList");
		Element mDetailCount = new Element("DetailCount");
		mDetailCount.setText(contList.size()+"");
		mDetailList.addContent(mDetailCount);
		
		Element mDetail = null;
		for (int i = 0; i < contList.size(); i++) {
			mDetail =  new Element("Detail");
			Element mTranDatas = new Element("TranData");
			
			String mProposalContNo = (String) contList.get(i);
			String sql_lk = "select 1 from LKNoRealTimeUWCheck where ProposalContNo = '"+mProposalContNo +"' and TBDealStatus in ('1','2') and PassFlag <> '0'";
			//3.如果有则进入下一步，如果没有则直接返回此单	
			if ("1".equals(new ExeSQL().getOneValue(sql_lk))) { //如果有值，则此保单进入过 NRT802 交易。
				//4.查询对应数据组织成返回报文(调用重打交易接口)
				String sql_lcc = "select contno from lccont where prtno = '"+mProposalContNo+"' with ur ";
				String contNo = new ExeSQL().getOneValue(sql_lcc);
				if("".equals(contNo) || contNo == null){
					mTranDatas.addContent(new Element("LCCont"));
				}else {
					Element mLCCont = new YbtContQueryBL("").getLCCont(contNo);
					if(mLCCont == null){
						System.out.println("怎么是空的??");
					}
					LCInsuredDB mLCInsuredDB = new LCInsuredDB();
					mLCInsuredDB.setContNo(contNo);
					LCInsuredSet mLCInsuredSet = mLCInsuredDB.query();
					mLCCont.getChild("LCAppnt").addContent(new Element("RelaToInsured").setText(
							mLCInsuredSet.get(1).getRelationToAppnt()));
					
					List insureList = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
					
					String sql_lp= "select sumactupaymoney from ljapayperson where getnoticeno is null and " +
					"contno = '"+contNo+"'and Riskcode = (select distinct Riskcode from " +
					"lcpol where Polno = Mainpolno and contno = '"+contNo+"' ) with ur";
					for (int j = 0; j < insureList.size(); j++) {
						List riskList = ((Element) insureList.get(j)).getChild("Risks").getChildren("Risk");				
						if(riskList.size() > 1){ //附险中的MainRiskCode赋值。
							((Element)riskList.get(1)).getChild("MainRiskCode").setText(
									((Element)riskList.get(0)).getChildText("MainRiskCode"));
						}
						
						for (int k = 0; k < riskList.size(); k++) {
							((Element)riskList.get(0)).getChild("Prem").setText(new ExeSQL().getOneValue(sql_lp));
							List lcbnfList = ((Element)riskList.get(k)).getChild("LCBnfs").getChildren("LCBnf");
							System.out.println("查到受益人个数为="+lcbnfList.size());
							((Element)riskList.get(k)).getChild("LCBnfs").addContent(new Element("LCBnfCount").setText(lcbnfList.size()+""));
						}
					}
					String sql_lj = "select sumactupaymoney from ljapay where incomeno = '"+contNo+"' and duefeetype = '0' with ur";
					mLCCont.addContent(new Element("FirstPrem").setText(new ExeSQL().getOneValue(sql_lj))); //获得首期保费
	
					mTranDatas.addContent(mLCCont);
				}
			}else{
				mTranDatas.addContent(new Element("LCCont"));
			}
			mDetail.addContent(mTranDatas);
			mDetailList.addContent(mDetail);
		}
		Element mTranData = new Element("TranData");
		mTranData.addContent(mRetData);
		mTranData.addContent(mDetailList);
		cLogger.info("Out CCB_YBTNoRealTimeUWBalanceBL.deal()...");
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pStdXml)  throws MidplatException {
		
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransCode(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		return mLKTransStatusDB;
	}
}
