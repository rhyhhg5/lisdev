package com.sinosoft.midplat.kernel.service;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class CCB_DailyBalance extends ServiceImpl {
	public CCB_DailyBalance(Element cThisBusiConf) {
		super(cThisBusiConf);
	}
	
	/**
	 * 建行对账有其独特的地方，暂时先按标准流程走，将来再完善。
	 */
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_DailyBalance.getStdXml()...");
		
		//启动对账线程
		new BalanceThread(pInXmlDoc).start();
		
		Document mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "对账成功！");
		
		cLogger.info("Out CCB_DailyBalance.service()!");
		return mOutXmlDoc;
	}
	
	private class BalanceThread extends Thread {
		private final Logger cLogger = Logger.getLogger(BalanceThread.class);
		
		private final Document cInXmlDoc;
		
		private BalanceThread(Document pInXmlDoc) {
			cInXmlDoc = pInXmlDoc;
		}
		
		public void run() {
			cLogger.info("Into BalanceThread.run()...");
			
			Document mOutXmlDoc = new DailyBalance(cThisBusiConf).service(cInXmlDoc);
			JdomUtil.print(mOutXmlDoc);
			
			cLogger.info("Out BalanceThread.run()!");
		}
	}
}
