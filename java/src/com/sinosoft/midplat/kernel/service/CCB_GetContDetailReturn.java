/**
 * 获取保单详情回盘结果处理
 * 
 */

package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.informix.util.dateUtil;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LKYBTBQTRANSDETAILDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class CCB_GetContDetailReturn extends ServiceImpl {

	public CCB_GetContDetailReturn(Element pThisConf) {
		super(pThisConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into CCB_GetContDetailQuery.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			//建行要求先返回结果，另起一个线程处理数据
			new GetContDetailReturnThread(pInXmlDoc).start();
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");

			mLKTransStatusDB.setRCode("1"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (MidplatException ex) {
			cLogger.error("获取保单详情查询回盘(CCB_GetContDetailReturn)交易失败！", ex);

			if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0"); // 空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());

		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out CCB_GetContDetailQuery.service()!");
//		mOutXmlDoc.getRootElement().addContent(tTranData);
		
		return mOutXmlDoc;
	}
	
	private class GetContDetailReturnThread extends Thread{
		private final Logger cLogger = Logger.getLogger(GetContDetailReturnThread.class);
		
		private final Document cInXmlDoc;
		
		private GetContDetailReturnThread(Document pInXmlDoc) {
			cInXmlDoc = pInXmlDoc;
		}
		
		public void run() {
			cLogger.info("Into GetContDetailReturnThread.run()...");
			LKYBTBQTRANSDETAILDB mLKYBTBQtransDetailDB = new LKYBTBQTRANSDETAILDB();
			Element tTranData = cInXmlDoc.getRootElement();
			Element tChkDetails = tTranData.getChild("ChkDetails");
			List lisChkDetail = tChkDetails.getChildren("ChkDetail");
			String tContno = "";
			String tErrorInfo = "";
			if(lisChkDetail.size() > 0){
				for (int i = 0; i < lisChkDetail.size(); i++) {
					Element tChkDetail = (Element)lisChkDetail.get(i);
					tContno = tChkDetail.getChildText("ContNo");
					mLKYBTBQtransDetailDB.setCONTNO(tContno);
					mLKYBTBQtransDetailDB.getInfo();
					tErrorInfo = tChkDetail.getChildText("ErrorInfo");
					mLKYBTBQtransDetailDB.setMODIFYDATE(DateUtil.getCur10Date());
					mLKYBTBQtransDetailDB.setMODIFYTIME(DateUtil.getCur8Time());
					mLKYBTBQtransDetailDB.setRETURNSTATE("0000");//成功
					mLKYBTBQtransDetailDB.setRETURNERRINFO(tErrorInfo);
					if(!mLKYBTBQtransDetailDB.update()){
						cLogger.error("获取保单详情查询回盘数据更新失败，保单号："+tContno);
					}
				}
			}else{
				cLogger.info("回盘数据包文件为空，不需要处理");
			}
			
			cLogger.info("Out GetContDetailReturnThread.run()!");
		}
	}

	private LKTransStatusDB insertTransLog(Document pXmlDoc)
			throws MidplatException {
		cLogger.info("Into CCB_GetContDetailReturn.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0"); // 业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out CCB_GetContDetailReturn.insertTransLog()!");
		return mLKTransStatusDB;
	}

	public static void main(String[] args) throws IOException {
		// String
		// mSQL="select * from lccont where 1='1' and Contno='2300135836'";
		// Document mDoc=new
		// CCB_GetContDetailQuery(null).getEncodedResult(mSQL);
		 
		String file  = "F:\\000gaojinfu\\bankMassage\\ccb\\instd\\CCB_GetContDetailReturn.xml";
		Document my_DOC  = JdomUtil.build(new FileInputStream(file));
		CCB_GetContDetailReturn aaa = new CCB_GetContDetailReturn(null);
		Document my_Doc = aaa.service(my_DOC);
		JdomUtil.print(my_Doc);
		FileOutputStream pOs=new FileOutputStream("F:\\getContDetail.xml");
		JdomUtil.output(my_Doc, pOs);
		pOs.close();
		
		
	}

}

