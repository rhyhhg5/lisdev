package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.vschema.LKTransStatusSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LsBank_NewContConfirm extends SimpService {
	public LsBank_NewContConfirm(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into LsBank_NewContConfirm.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			if(null==mLCCont.getChildText(PrtNo)||("").equals(mLCCont.getChildText(PrtNo))){
				throw new MidplatException("投保书印刷号不能为空!");
			}
			String tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
			//财务收费
			YbtTempFeeBL tYbtTempFeeBL = new YbtTempFeeBL(
					mLKTransStatusDB.getPrtNo(), mGlobalInput);
			tYbtTempFeeBL.deal();

			//签单+单证核销+回执回销
			YbtLCContSignBL tYbtLCContSignBL = 
				new YbtLCContSignBL(mLKTransStatusDB.getPrtNo(), mGlobalInput);
			tYbtLCContSignBL.deal();
			
			//组织返回报文
			YbtContQueryBL tYbtContQueryBL = new YbtContQueryBL(tYbtLCContSignBL.getContNo());
			mOutXmlDoc = tYbtContQueryBL.deal();
			
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			
			new CardManage("HB07/07B", mGlobalInput).makeUsed(tOutLCCont.getChildText(ProposalContNo));
			
			//更新相关日志
			
			String tCurTime = DateUtil.getCurDate("HH:mm:ss");
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setProposalNo(tOutLCCont.getChildText(ProposalContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回			
			mLKTransStatusDB.setModifyTime(tCurTime);

			LKTransStatusDB tOldLKTransStatusDB = new LKTransStatusDB();
			tOldLKTransStatusDB.setBankCode(mLKTransStatusDB.getBankCode());
			tOldLKTransStatusDB.setBankBranch(mLKTransStatusDB.getBankBranch());
			tOldLKTransStatusDB.setBankNode(mLKTransStatusDB.getBankNode());
			tOldLKTransStatusDB.setProposalNo(mLKTransStatusDB.getProposalNo());
			tOldLKTransStatusDB.setRCode("1");
			LKTransStatusSet ttLKTransStatusSet = tOldLKTransStatusDB.query();
			if ((null==ttLKTransStatusSet) || (ttLKTransStatusSet.size()<1)) {
				throw new MidplatException("查询原始日志信息失败！");
			}
			LKTransStatusSchema tLKTransStatusSchema = ttLKTransStatusSet.get(1);
			tLKTransStatusSchema.setPolNo(tOutLCCont.getChildText(ContNo));
			tLKTransStatusSchema.setTransAmnt(tOutLCCont.getChildText(Prem));
			tLKTransStatusSchema.setStatus("1");
			tLKTransStatusSchema.setModifyDate(mLKTransStatusDB.getModifyDate());
			tLKTransStatusSchema.setModifyTime(tCurTime);
			
			MMap tSubmitMMap = new MMap();
			tSubmitMMap.put(mLKTransStatusDB.getSchema(), "UPDATE");
			tSubmitMMap.put(tLKTransStatusSchema, "UPDATE");
			VData tSubmitVData = new VData();
			tSubmitVData.add(tSubmitMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tSubmitVData, "")) {
				cLogger.error(tPubSubmit.mErrors.getFirstError());
				throw new MidplatException("更新日志失败！");
			}
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			try {
				YbtSufUtil.clearData(mLKTransStatusDB.getPrtNo());
			} catch (Exception tBaseEx) {
				cLogger.error("删除新单数据失败！", tBaseEx);
			}
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				
				String ttDesr = ex.getMessage();
				try {
					if (null!=ttDesr && ttDesr.getBytes("utf-8").length >= 255) {
						ttDesr = ttDesr.substring(0,85);
					}
				} catch (UnsupportedEncodingException uex){}
				mLKTransStatusDB.setDescr(ttDesr);
				
				if (null != mGlobalInput) {
					mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
				}
				mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
				if (!mLKTransStatusDB.update()) {
					cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
				}
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		cLogger.info("Out LsBank_NewContConfirm.service()!");
		return mOutXmlDoc;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into LsBank_NewContConfirm.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTemp("1");	//确认/取消标识：0-取消；1-确认
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out LsBank_NewContConfirm.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "d:/in.xml";
		String mOutFile = "d:/out.xml";
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new LsBank_NewContConfirm(null).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
