package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.finfee.YBTPayQuery;
import com.sinosoft.lis.finfee.YBTTempFee;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class NXS_RenewalPayConfirmBL extends ServiceImpl {

	public NXS_RenewalPayConfirmBL(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into NXS_RenewalPayConfirmBL.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			String mContNo = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("ContNo");
			String mPay = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("Pay").trim();
			String mPayMode = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("PayMode").trim();
			String mBankAccNo = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("BankAccNo").trim();
			String mAccName = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("AccName").trim();
			String mBankCode = pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BankCode");
			
			//判断此保单续期与新单是否同一机构
			String sql_xq = "select 1 from lccont where contno = '"+mContNo+"' and managecom = '"+mGlobalInput.ManageCom+"' with ur";
			if (!"1".equals(new ExeSQL().getOneValue(sql_xq))) {
				throw new MidplatException("非本市机构所出保单，无法缴费！");
			}
			
			
			//收费确认前调取查询接口进行校验
			TransferData tTransferData = new TransferData();
			YBTPayQuery tYBTPayQuery = new YBTPayQuery();
			tTransferData.setNameAndValue("ContNo", mContNo);
			
			if(!tYBTPayQuery.submitData(tTransferData)){
				CErrors cErrors = new CErrors();
	            cErrors = tYBTPayQuery.getErrorInf();
	            System.out.println(cErrors.getLastError()); // 查询失败原因
	            mLKTransStatusDB.setDescr(cErrors.getLastError());
	            mLKTransStatusDB.setRCode("0");//空-未返回；1-交易成功，返回；0-交易失败，
	            throw new MidplatException(cErrors.getLastError());
			}
			//判断缴费日期是否在起期和止期之间
			TransferData _result = tYBTPayQuery.getResult();
			Calendar now = Calendar.getInstance();
			String mStartPayDate = (String) _result.getValueByName("StartPayDate");
			String mEndPayDate = (String) _result.getValueByName("PayDate");
			Calendar _c1 = Calendar.getInstance();
			_c1.set(Calendar.YEAR, Integer.parseInt(mStartPayDate.substring(0,4)));
			_c1.set(Calendar.MONTH, Integer.parseInt(mStartPayDate.substring(5,7))-1);
			_c1.set(Calendar.DAY_OF_MONTH, Integer.parseInt(mStartPayDate.substring(8,10))-1);
			
			Calendar _c2 = Calendar.getInstance();
			_c2.set(Calendar.YEAR, Integer.parseInt(mEndPayDate.substring(0,4)));
			_c2.set(Calendar.MONTH, Integer.parseInt(mEndPayDate.substring(5,7))-1);
			_c2.set(Calendar.DAY_OF_MONTH, Integer.parseInt(mEndPayDate.substring(8,10))+1);
			if(!(now.after(_c1) && now.before(_c2))){
				throw new MidplatException("缴费日期不在此续期起始和终止日期之间，银保通不能缴费！");
			}
			System.out.println("数据库中的金额："+Double.parseDouble((String)_result.getValueByName("PayMoney")));
			if(Double.parseDouble(mPay) - Double.parseDouble((String)_result.getValueByName("PayMoney")) != 0){
				 mLKTransStatusDB.setDescr("传入的金额与缴费金额不匹配！");
		         mLKTransStatusDB.setRCode("0");
				throw new MidplatException("传入的金额与缴费金额不匹配！");
			}
			
			// 调用核心接口 传值  收费确认
	        tTransferData.setNameAndValue("PayMoney", mPay);
	        tTransferData.setNameAndValue("GlobalInput", mGlobalInput);
	        tTransferData.setNameAndValue("PayMode", mPayMode);
	        tTransferData.setNameAndValue("BankAccNo", mBankAccNo);
	        tTransferData.setNameAndValue("AccName", mAccName);
	        tTransferData.setNameAndValue("BankCode", mBankCode);
	        
	        YBTTempFee tYBTTempFee = new YBTTempFee();
	        if (!tYBTTempFee.submitData(tTransferData)) {
	            // 收费失败，可获得返回信息
	        	CErrors cErrors = new CErrors();
	            cErrors = tYBTTempFee.getErrorInf();
	            mLKTransStatusDB.setDescr(cErrors.getLastError());
	            mLKTransStatusDB.setRCode("0");
	            mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", cErrors.getLastError());
	        }else {
	        	// 組織返回報文
	        	mLKTransStatusDB.setRCode("1");
	        	mOutXmlDoc = getOutStd();
	        }
	       
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out NXS_RenewalPayConfirmBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document getOutStd() {
		cLogger.info("Into NXS_RenewalPayConfirmBL.getOutStd()...");
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		
		mFlag.setText("1");
		mDesc.setText("交易成功！");
		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mRetData);
		
		cLogger.info("Out NXS_RenewalPayConfirmBL.getOutStd()...");
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into NXS_RenewalPayConfirmBL.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setStatus("1");
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Pay"));
		mLKTransStatusDB.setServiceStartTime(mLCCont.getChildText("ReceivDate")); //应收日期
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out NXS_RenewalPayConfirmBL.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
