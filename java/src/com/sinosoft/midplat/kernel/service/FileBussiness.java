package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.dom4j.io.SAXReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class FileBussiness extends ServiceImpl {
	private String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
	private String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

	public FileBussiness(Element pThisBusiConf) {
		super(pThisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into FileBussiness.service()...");

		Document mOutXmlDoc = null;
		
		LKTransStatusDB mLKTransStatusDB = null;

		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		try {

			mLKTransStatusDB = insertTransLog(pInXmlDoc);

			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", "交易成功!");

		} catch (Exception e) {
			cLogger.error(cThisBusiConf.getChildText(name) + "交易失败！", e);

			if (null != mLKTransStatusDB) {
				mLKTransStatusDB.setRCode("0");// 空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = e.getMessage();
				mLKTransStatusDB.setDescr(tDesr);
				try {
					if (e.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException e1) {
					cLogger.error(e1.getMessage());
				}
				mLKTransStatusDB.setDescr(tDesr);
			}
			mFlag.setText("1");
			mDesc.setText(e.getMessage());
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", e.getMessage());
		}

		if (null != mLKTransStatusDB) { // 插入日志失败时mLKTransStatusDB=null

			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			mLKTransStatusDB.setRCode("1");// 执行任务完毕记

			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！"
						+ mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out FileBussiness.service()...");
		return mOutXmlDoc;
	}
	
	public LKTransStatusDB insertTransLog(Document pStdXml)
			throws MidplatException {
		cLogger.info("Into FileBussiness.insertTransLog()...");
		Element mBaseInfo = pStdXml.getRootElement().getChild(BaseInfo);

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildTextTrim("BankCode"));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildTextTrim("ZoneNo"));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildTextTrim("BrNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB.setTransDate(mBaseInfo.getChildTextTrim("BankDate"));
		mLKTransStatusDB
				.setBankOperator(mBaseInfo.getChildTextTrim("TellerNo"));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildTextTrim("TransrNo"));
		mLKTransStatusDB
				.setFuncFlag(mBaseInfo.getChildTextTrim("FunctionFlag"));
		mLKTransStatusDB.setManageCom(mBaseInfo.getChildText("InsuID"));
		mLKTransStatusDB.setbak1(mBaseInfo.getChildText("mFileName")); 
		mLKTransStatusDB.setbak2(mBaseInfo.getChildText("mFileState")); 

		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		cLogger.info("Out FileBussiness.insertTransLog()...");
		return mLKTransStatusDB;
	}
	//E:\rb\接口\jh\bcm_20161011100003532918_B60911_141010.xml
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "E:\\rb\\接口\\jh\\bcm_20161011100003532918_B60911_141010.xml";
		String mOutFile = "E:\\rb\\接口\\jh\\bcm_out.xml";
//		String elpath = "E:\\rb\\jxnxs\\el.xml";
//		FileInputStream a = new FileInputStream(elpath);
//		InputStreamReader b = new InputStreamReader(a, "GBK");
//		Document c = new SAXBuilder().build(b);
//		SAXReader saxreader = new SAXReader();
//		Element root = c.getRootElement().getChild("business");
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new FileBussiness(null).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
	
	
}
