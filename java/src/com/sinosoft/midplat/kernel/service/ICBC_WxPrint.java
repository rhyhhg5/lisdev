package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CertPrintTable;
import com.cbsws.obj.LCCustomerImpartTable;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ICBC_WxPrint extends ServiceImpl  {

	public ICBC_WxPrint(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into ICBC_WxPrint.service()...");
		
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		//电子保单生产日期
		String mBalaDate = pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("TransDate");
 
		try {
			
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
//			mBalaDate = "2014-09-26";
			String cx_sql = "select prtno,polno From lktransstatus where bankcode in ('01','02','03','04','16','57','65') and transdate = '" + mBalaDate + "' " +
					"and funcflag = '01' and riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701','340401','334901','232401','XBT009','XBT010','XBT011','XBT012','XBT013','XBT014','XBT017','335301','730301') and rcode = '1'" +
					" and Prtno = Proposalno and status = '1' and state_code = '03' and rmpvskernel = '00' and resultbalance = '0' and exists (select 1 from lccont where contno=polno) with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = null;
			tSSRS = tExeSQL.execSQL(cx_sql);
			System.out.println("工行及中行网银出单数：" + tSSRS.getMaxRow());
			for(int i = 1;i<= tSSRS.getMaxRow() ; i++){
				String mPrtno = tSSRS.GetText(i, 1);
				String mPolno = tSSRS.GetText(i, 2);
				MsgCollection mMsgCollection = creatDocument(mPrtno);
				
				//生成电子保单前向LKElectronCont中插入数据
				try {
					cx_sql = "select 1 from lkelectroncont where contno = '"+mPolno+"'";
					if(!"1".equals(tExeSQL.getOneValue(cx_sql))){
						cx_sql = "insert into LKElectronCont ( TransNo,ContNo,ContSendDate,BankName,Temp1 ) values ( '"+DateUtil.getCurDateTime()+"','"+mPolno+"','"+DateUtil.getCur10Date()+"',(select codename  from ldcode where   codetype ='banknum' and code in (select bankcode  from lktransstatus where polno ='"+mPolno+"' and funcflag='01')),'0' )";
						tExeSQL.execUpdateSQL(cx_sql);
					}
				} catch (Exception ex) {
					cLogger.error("插入电子保单信息失败，保单号：" + mPolno, ex);
					continue;
				}
				
				ICBC_WxPrintBL mWxPrintBL = new ICBC_WxPrintBL();
				//调用核心接口生产电子保单
//				mWxPrintBL.deal(mMsgCollection,i);
				try {
					if(mWxPrintBL.deal(mMsgCollection,i)){//如果电子保单已生成，将状态改为未发送2   gaojinfu 20170801
						cx_sql = "update lkelectroncont set temp1 = '2' where contno = '" + mPolno + "'";
						if(!tExeSQL.execUpdateSQL(cx_sql)){
							cLogger.error("电子保单状态修改失败，保单号：" + mPolno);
						}
					}
				} catch (Exception ex) {
					// TODO: handle exception
					cLogger.error("电子保单生成失败，保单号：" + mPolno, ex);//生成电子保单报错   gaojinfu 20170713
					continue;
				}
				//如果电子保单已生成，将状态改为未发送2   gaojinfu 20170713
//				try {
//					cx_sql = "update lkelectroncont set temp1 = '2' where contno = '" + mPolno + "'";
//					tExeSQL.execUpdateSQL(cx_sql);
//				} catch (Exception ex) {
//					cLogger.error("电子保单状态修改失败，保单号：" + mPolno, ex);
//					continue;
//				}
				//备份生产电子保单的数据
				boolean mboolean= saveDetails(mPolno);
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1","交易成功！");
		}catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}

		cLogger.info("Out ICBC_WxPrint.service()...");
		return mOutXmlDoc;
	}
	
	public MsgCollection creatDocument(String mPrtno) {
		cLogger.info("Into ICBC_WxPrint.creatDocument()...");
		
		String pCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String pCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		String cx_sql = "select insuredno From lcinsured where prtno = '"+ mPrtno +"' with ur";
		
		MsgCollection tMsgCollection = new MsgCollection();
		MsgHead tMsgHead = new MsgHead();
		tMsgHead.setBatchNo("123456789");	//
		tMsgHead.setBranchCode("001");	//
		tMsgHead.setMsgType("YBT_001");
		tMsgHead.setSendDate(pCurrentDate);
		tMsgHead.setSendTime(pCurrentTime);
		tMsgHead.setSendOperator("ybt");
		tMsgCollection.setMsgHead(tMsgHead);
		CertPrintTable tCertPrintTable = new CertPrintTable();
		tCertPrintTable.setCardNo(mPrtno);
		tMsgCollection.setBodyByFlag("CertPrintTable", tCertPrintTable);		
		LCCustomerImpartTable tLCCustomerImpartTable = new LCCustomerImpartTable();
		tLCCustomerImpartTable.setInsuredNo(new ExeSQL().getOneValue(cx_sql));
		tLCCustomerImpartTable.setImpartContent("身高(cm)");
		tLCCustomerImpartTable.setImpartParamModle("170");
		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable);
//		LCCustomerImpartTable tLCCustomerImpartTable1 = new LCCustomerImpartTable();
//		tLCCustomerImpartTable1.setInsuredNo("000996002");
//		tLCCustomerImpartTable1.setImpartContent("体重(kg)");
//		tLCCustomerImpartTable1.setImpartParamModle("70");
//		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable1);
//		LCCustomerImpartTable tLCCustomerImpartTable2 = new LCCustomerImpartTable();
//		tLCCustomerImpartTable2.setInsuredNo("000996002");
//		tLCCustomerImpartTable2.setImpartContent("是否吸烟");
//		tLCCustomerImpartTable2.setImpartParamModle("否");
//		tMsgCollection.setBodyByFlag("LCCustomerImpartTable", tLCCustomerImpartTable2);
//		OnlineDeclareTable tOnlineDeclareTable = new OnlineDeclareTable();
//		tOnlineDeclareTable.setContentTag("燃气地址");
//		tOnlineDeclareTable.setReportedContent("地址信息");
//		tMsgCollection.setBodyByFlag("OnlineDeclareTable", tOnlineDeclareTable);
//		OnlineDeclareTable tOnlineDeclareTable1 = new OnlineDeclareTable();
//		tOnlineDeclareTable1.setContentTag("联系电话");
//		tOnlineDeclareTable1.setReportedContent("1301010101");
//		tMsgCollection.setBodyByFlag("OnlineDeclareTable", tOnlineDeclareTable1);
		
		
		cLogger.info("Out ICBC_WxPrint.creatDocument()...");
		return tMsgCollection;
	}
	
	/**
	 * 保存对账明细，返回保存的明细数据(LKBalanceDetailSet)
	 */
	private boolean saveDetails(String mPolno) throws Exception {
		cLogger.info("Into ICBC_WxPrint.saveDetails()...");
		
//		String cx_sql = "select email From LCAddress where customerno in (select appntno From lcappnt where contno = '"+ mPolno +"') with ur";
		
//		System.out.println("投保人Email:"+new ExeSQL().getOneValue(cx_sql));
		String gx_sql = "update lktransstatus set bankacc = '1' where polno='"+ mPolno +"'";
		new ExeSQL().execUpdateSQL(gx_sql);
		
		cLogger.info("Out ICBC_WxPrint.saveDetails()!");
		return true;
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into ICBC_WxPrint.creatDocument()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo("");
		mLKTransStatusDB.setProposalNo("");
		mLKTransStatusDB.setPrtNo("");
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out ICBC_WxPrint.creatDocument()!");
		return mLKTransStatusDB;
	}
	public static void main(String[] args) throws Exception {
		String mInFile = "C:\\Users\\XiaoLong\\Desktop\\调用indifo生成PDF模板.xml";
		String mOutFile = "D:/out.xml";
		
		FileInputStream mFis = new FileInputStream(mInFile);
		InputStreamReader mIsr = new InputStreamReader(mFis, "UTF-8");
		Document mInXmlDoc = new SAXBuilder().build(mIsr);
		
		Document mOutXmlDoc = new ICBC_WxPrint(null).service(mInXmlDoc);
		
//		JdomUtil.print(mOutXmlDoc);
//		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		
	}
	
}
