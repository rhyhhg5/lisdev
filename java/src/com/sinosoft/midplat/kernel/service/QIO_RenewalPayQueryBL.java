package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.finfee.YBTPayQuery;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;

public class QIO_RenewalPayQueryBL extends ServiceImpl {

	private static String contNo = "";
	private static Element mBaseInfo = null;
	
	public QIO_RenewalPayQueryBL(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QIO_RenewalPayQueryBL.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		Element mTranData = pInXmlDoc.getRootElement();
//		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			
			contNo = pInXmlDoc.getRootElement().getChild("LCCont").getChildText("ContNo");
			
			//判断此保单续期与新单是否同一机构
			ExeSQL es = new ExeSQL();
			String sql_xq = "select 1 from lccont where contno = '"+contNo+"' and managecom = '"+mGlobalInput.ManageCom+"' with ur";
			if (!"1".equals(es.getOneValue(sql_xq))) {
				throw new MidplatException("非本市机构所出保单，无法缴费！");
			}
//			校验是否为银保通所出保单 20120712
			sql_xq = "select 1 from lccont where contno = '"+contNo+"' and cardflag = '9' and salechnl = '04' with ur";
			if (!"1".equals(es.getOneValue(sql_xq))) {
				throw new MidplatException("非银保通所出保单，无法缴费！");
			}
			
			//调取核心查询接口
			TransferData tTransferData = new TransferData();
	        tTransferData.setNameAndValue("ContNo", contNo);
	        
	        YBTPayQuery tYBTPayQuery = new YBTPayQuery();
	        if (!tYBTPayQuery.submitData(tTransferData)) {
	            // 若查询失败，可获得返回信息
	        	CErrors cErrors = new CErrors();
	            cErrors = tYBTPayQuery.getErrorInf();
	            System.out.println(cErrors.getLastError()); // 查询失败原因
	            mLKTransStatusDB.setDescr(cErrors.getLastError());
		        mLKTransStatusDB.setRCode("0");
	            mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", cErrors.getLastError());
	        }else {
	            TransferData result = tYBTPayQuery.getResult(); // getResult 信息返回函数
	            System.out.println(result.getValueByName("ManageCom")); // 保单机构
	            System.out.println(result.getValueByName("AppntName")); // 投保人名称
	            System.out.println(result.getValueByName("ContNo")); // 保单号
	            System.out.println(result.getValueByName("PayMoney")); // 金额
	            System.out.println(result.getValueByName("StartPayDate")); // 缴费起始日期
	            System.out.println(result.getValueByName("PayDate")); // 缴费截至日期
	            System.out.println(result.getValueByName("PayCount")); // 期数
	            
	            //取得核心数据后组织返回报文
	            mLKTransStatusDB.setRCode("1");
	            mLKTransStatusDB.setTransStatus("1");
	            mOutXmlDoc = getOutStd(result);
	        }
		} catch (Throwable ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out QIO_RenewalPayQueryBL.service()...");
		return mOutXmlDoc;
	}
	
	private static Document getOutStd(TransferData result){
		Element mTranData = new Element(TranData);
		Element mLCCont = new Element(LCCont);
		Element mRetData = new Element(RetData);
		
		Element mFlag = new Element(Flag);
		Element mDesc = new Element(Desc);
		mRetData.addContent(mFlag.setText("1"));
		mRetData.addContent(mDesc.setText("交易成功！"));
		
		contNo = (String) result.getValueByName("ContNo");
		Element mContNo = new Element(ContNo); // 保单号
		mContNo.setText(contNo);
		
		// 查询保单号对应的riskCode（主）
		String sql_riskCode = "select riskcode from lcpol where contno = '"+contNo+"' and riskseqno in ('01','1') with ur";
		String riskCode = new ExeSQL().getOneValue(sql_riskCode);
		String sql_riskName = "select riskname from lmrisk where riskcode = '"+ riskCode +"'  with ur";
		
		Element mRiskCode = new Element(RiskCode);//险种编码
		mRiskCode.setText(riskCode);
		
		Element mAppntName = new Element(AppntName);// 投保人名称
		mAppntName.setText((String) result.getValueByName("AppntName"));
		
		Element mReceivDate = new Element("ReceivDate");// 缴费截至日期
		mReceivDate.setText((String) result.getValueByName("PayDate"));
		
		Element mReceivPay = new Element("ReceivPay");// 金额
		mReceivPay.setText((String) result.getValueByName("PayMoney"));
		
		Element mDescr = new Element("Descr");   //备注
		
		Element mReceivNum = new Element("ReceivNum");// 期数
		mReceivNum.setText((String) result.getValueByName("PayCount"));
		
		Element mReceivBegDate = new Element("ReceivBegDate");// 缴费起始日期
		mReceivBegDate.setText((String) result.getValueByName("StartPayDate"));
		
		Element mRiskName = new Element("RiskName"); //险种名称
		mRiskName.setText(new ExeSQL().getOneValue(sql_riskName));
		
		Element mZoneNo = new Element("ZoneNo"); //地区代码
		mZoneNo.setText(mBaseInfo.getChildText("ZoneNo"));
		
		Element mBrNo = new Element("BrNo");   // 网点代码
		mBrNo.setText(mBaseInfo.getChildText("BrNo"));
		
		Element mRemark = new Element("Remark");  //集团、股份标志
		
		LCAppntDB lcapp = new LCAppntDB();
		lcapp.setContNo(contNo);
		LCAppntSet appSet = lcapp.query();
		LCAppntSchema appnt = appSet.get(1); //只取第一个
		Element mAppntIdType = new Element("AppntIdType"); //投保人证件类型
		mAppntIdType.setText(appnt.getIDType());
		
		Element mAppntIdNo = new Element("AppntIdNo"); //投保人证件号码
		mAppntIdNo.setText(appnt.getIDNo());
		
		Element mPayYears = new Element("PayYears"); //缴费年期
		
		
		LCPolDB polDB = new LCPolDB();
		polDB.setContNo(contNo);
		LCPolSet xPol = polDB.query();
		double mSumPrem = 0;
		for (int i = 0; i < xPol.size(); i++) {  //pol表中保单号对应数据中保费累加
			LCPolSchema mpol = xPol.get(i+1);
			mSumPrem += mpol.getPrem();
			String mYears = mpol.getRiskSeqNo();
			if("01".equals(mYears) || "1".equals(mYears)){
				mPayYears.setText(mYears);
			}
		}
		Element mSumPolPrem = new Element("SumPolPrem"); //缴费金额
		mSumPolPrem.setText(mSumPrem+"");
		
		mLCCont.addContent(mRiskCode);
		mLCCont.addContent(mContNo);
		mLCCont.addContent(mAppntName);
		mLCCont.addContent(mReceivDate);
		mLCCont.addContent(mReceivPay);
		mLCCont.addContent(mDescr);
		mLCCont.addContent(mReceivNum);
		mLCCont.addContent(mReceivBegDate);
		mLCCont.addContent(mRiskName);
		mLCCont.addContent(mZoneNo);
		mLCCont.addContent(mBrNo);
		mLCCont.addContent(mRemark);
		mLCCont.addContent(mAppntIdType);
		mLCCont.addContent(mAppntIdNo);
		mLCCont.addContent(mPayYears);
		mLCCont.addContent(mSumPolPrem);
		
		
		mTranData.addContent(mRetData);
		mTranData.addContent(mLCCont);
		return new Document(mTranData);
	}
	
	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into QIO_RenewalPayQueryBL.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out QIO_RenewalPayQueryBL.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
//	private Element getRisks(String pContNo, String pInsuredNo) throws MidplatException {
//		cLogger.info("Into YbtContQueryBL.getRisks()...");
//		
//		LCPolDB mLCPolDB = new LCPolDB();
//		mLCPolDB.setContNo(pContNo);
//		mLCPolDB.setInsuredNo(pInsuredNo);
//		LCPolSet mLCPolSet = mLCPolDB.query();
//		if ((null==mLCPolSet) || (mLCPolSet.size()<1)) {
//      	cLogger.error(mLCPolDB.mErrors.getFirstError());
//      	throw new MidplatException("查询险种信息失败！");
//		}
//		
//		Element mRisks = new Element(Risks);
//		System.out.println(pContNo + " 险种对应险种个数为= "+mLCPolSet.size());
//		for (int i = 1; i <= mLCPolSet.size(); i++) {
//			LCPolSchema tLCPolSchema = (LCPolSchema) mLCPolSet.get(i);
//
//			String tRiskCodeStr = tLCPolSchema.getRiskCode();
//			if (tRiskCodeStr.equals("530701")	//附加健康人生个人意外伤害保险（B款）
//					|| tRiskCodeStr.equals("530801")	//附加健康人生个人意外伤害保险(C款)
//					|| tRiskCodeStr.equals("331401")	//附加健康专家个人护理保险
//					|| tRiskCodeStr.equals("531001")	//附加健康人生个人意外伤害保险（D款）
//					|| tRiskCodeStr.equals("340201")	//附加安心宝个人护理保险
//					|| tRiskCodeStr.equals("531101")    //附加金利宝个人意外伤害保险
//                    || tRiskCodeStr.equals("1607")
//                    || tRiskCodeStr.equals("260301")
//                    || tRiskCodeStr.equals("531301")) {	//附加康利相伴个人意外伤害保险
//				continue;
//            }
//            Element tLiaoNFlag = new Element("LiaoNFlag");
//            if (tRiskCodeStr.equals("550806")) {
//                // 辽宁套餐做特殊标志,ABC三款只能根据险种个数不一样去区分.
//                if (mLCPolSet.size() == 3)
//                    tLiaoNFlag.setText("WR0145");
//                else if (mLCPolSet.size() == 2)
//                    tLiaoNFlag.setText("XBT002");
//                else
//                    tLiaoNFlag.setText("XBT003");
//            }
//			
//			Element tRiskCode = new Element(RiskCode);
//			tRiskCode.setText(tLCPolSchema.getRiskCode());
//			
//			Element tRiskName = new Element(RiskName);
//			String mSQL = "select RiskName from lmrisk where riskcode='" + tLCPolSchema.getRiskCode() + "' with ur";
//			tRiskName.setText(new ExeSQL().getOneValue(mSQL));
//			
//			Element tMainRiskCode = new Element(MainRiskCode);
//			tMainRiskCode.setText(tRiskCode.getText());	//注意：此处暂将主险代码置为与险种代码一致，以后遇到需返回多险种情况时，此处需修改。
//			
//			Element tCValiDate = new Element(CValiDate);
//			tCValiDate.setText(tLCPolSchema.getCValiDate());
//			
//			Element tAmnt = new Element(Amnt);
//			tAmnt.setText(
//					new DecimalFormat("0.00").format(tLCPolSchema.getAmnt()));
//			
//			Element tPrem = new Element(Prem);
//			tPrem.setText(
//					new DecimalFormat("0.00").format(tLCPolSchema.getPrem()));
//			
//			Element tSupplementaryPrem = new Element(SupplementaryPrem);
//			tSupplementaryPrem.setText(
//					new DecimalFormat("0.00").format(tLCPolSchema.getSupplementaryPrem()));
//			
//			Element tMult = new Element(Mult);
//			tMult.setText(
//					String.valueOf(tLCPolSchema.getMult()));
//
//			Element tPayIntv = new Element(PayIntv);
//			tPayIntv.setText(
//					String.valueOf(tLCPolSchema.getPayIntv()));
//			
//			Element tInsuYearFlag = new Element(InsuYearFlag);
//			tInsuYearFlag.setText(tLCPolSchema.getInsuYearFlag());
//
//			Element tInsuYear = new Element(InsuYear);
//			tInsuYear.setText(
//					String.valueOf(tLCPolSchema.getInsuYear()));
//			
//			Element tYears = new Element(Years);
//			tYears.setText(
//					String.valueOf(tLCPolSchema.getYears()));
//			
//			Element tPayEndYearFlag = new Element(PayEndYearFlag);
//			tPayEndYearFlag.setText(tLCPolSchema.getPayEndYearFlag());
//			
//			Element tPayEndYear = new Element(PayEndYear);
//			tPayEndYear.setText(
//					String.valueOf(tLCPolSchema.getPayEndYear()));
//			
//			Element tPayEndDate = new Element(PayEndDate);
//			tPayEndDate.setText(tLCPolSchema.getPayEndDate());
//			
//			Element tPayYears = new Element(PayYears);
//			tPayYears.setText(
//					String.valueOf(tLCPolSchema.getPayYears()));
//			
//			Element tGetYearFlag = new Element(GetYearFlag);
//			tGetYearFlag.setText(tLCPolSchema.getGetYearFlag());
//			
//			Element tGetYear = new Element(GetYear);
//			tGetYear.setText(
//					String.valueOf(tLCPolSchema.getGetYear()));
//
//			Element tRisk = new Element(Risk);
//            tRisk.addContent(tLiaoNFlag);
//			tRisk.addContent(tRiskCode);
//			tRisk.addContent(tRiskName);
//			tRisk.addContent(tMainRiskCode);
//			tRisk.addContent(tCValiDate);
//			tRisk.addContent(tAmnt);
//			tRisk.addContent(tPrem);
//			tRisk.addContent(tSupplementaryPrem);
//			tRisk.addContent(tMult);
//			tRisk.addContent(tPayIntv);
//			tRisk.addContent(tInsuYearFlag);
//			tRisk.addContent(tInsuYear);
//			tRisk.addContent(tYears);
//			tRisk.addContent(tPayEndYearFlag);
//			tRisk.addContent(tPayEndYear);
//			tRisk.addContent(tPayEndDate);
//			tRisk.addContent(tPayYears);
//			tRisk.addContent(tGetYearFlag);
//			tRisk.addContent(tGetYear);
//
//			mRisks.addContent(tRisk);
//		}
//
//		cLogger.info("Out YbtContQueryBL.getRisks()!");
//		return mRisks;
//	}
	
}
