package com.sinosoft.midplat.kernel.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.dom4j.io.SAXReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LOMixBKAttributeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LOMixBKAttributeSchema;
import com.sinosoft.lis.vschema.LOMixBKAttributeSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.check.BlackPhoneCheck;
import com.sinosoft.midplat.kernel.service.check.IdNoCheck;
import com.sinosoft.midplat.kernel.service.check.YBTRiskCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtAutoCheck;
import com.sinosoft.midplat.kernel.service.newCont.YbtContBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtContInsuredIntlBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtInsuredProposalBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtLCContSignBL;
import com.sinosoft.midplat.kernel.service.newCont.YbtTempFeeBL;
import com.sinosoft.midplat.kernel.service.query.YbtContQueryBL;
import com.sinosoft.midplat.kernel.util.CardManage;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class AIO_NewCont extends ServiceImpl {
	public AIO_NewCont(Element pThisBusiConf) {
		super(pThisBusiConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into AIO_NewCont.service()...");
		long mStartMillis = System.currentTimeMillis();
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		LCContSubDB mLCContSubDB = null;
		GlobalInput mGlobalInput = null;
		boolean mDeleteable = false;	//删除数据标志 
		
		YBTRiskCheck ycheck = new YBTRiskCheck();
		
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		Element tThisConfRoot = cThisBusiConf.getParent();
		String  mProposalContNo = mLCCont.getChildText(ProposalContNo);
		String mSalary = mLCCont.getChild(LCAppnt).getChildText("Salary");//投保人年收入
		String mCountyType = mLCCont.getChild(LCAppnt).getChildText("CountyType");//投保人所属区域
		
		String mAppntPhone = mLCCont.getChild(LCAppnt).getChildText(AppntPhone);
		String mAppntMobile = mLCCont.getChild(LCAppnt).getChildText(AppntMobile);
		String mAppntOfficePhone = mLCCont.getChild(LCAppnt).getChildText(AppntOfficePhone);
		String tRiskCode = pInXmlDoc.getRootElement().getChild(LCCont)
									.getChild(LCInsureds).getChild(LCInsured)
									.getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
		Element mRisk = pInXmlDoc.getRootElement().getChild(LCCont).getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChild(Risk);
		
		try {
			
			String tSQL = "select 1 from lktransstatus where PrtNo='" + mLCCont.getChildText(PrtNo) + "' and rcode is null with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL))) {
				throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
			}
			
			//如果PrtNo不存在，则可以根据PrtNo删除数据
			tSQL = "select 1 from lccont where PrtNo='" + mLCCont.getChildText(PrtNo) + "' with ur";
			String tSQL_b = "select 1 from lbcont where PrtNo='" + mLCCont.getChildText(PrtNo) + "' with ur";
			if ("1".equals(new ExeSQL().getOneValue(tSQL)) || "1".equals(new ExeSQL().getOneValue(tSQL_b))) {
				throw new MidplatException("该投保单印刷号已使用，请更换！");
			} else {  
				mDeleteable = true;
			}
		
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo),
					mBaseInfo.getChildText(BrNo));
			
			//工行网银 单证号 = 投保单印刷号 
			if (mLCCont.getChildText("SourceType").equals("1")
					|| mLCCont.getChildText("SourceType").equals("8")
					|| mLCCont.getChildText("SourceType").equals("e")
					|| mLCCont.getChildText("SourceType").equals("f")) {
				String wyPrtno = "56" + PubFun1.CreateMaxNo("YBTPRTNO", 11);// 生成印刷号
				mProposalContNo = wyPrtno;

				String mSQL = "select 1 from lccont where ProposalContNo = '"
						+ mProposalContNo
						+ "' union select 1 from lbcont where ProposalContNo = '"
						+ mProposalContNo + "' with ur";
				tSQL = "select 1 from lktransstatus where proposalno='"
						+ mProposalContNo + "' and rcode is null with ur";
				String zx_sql = "update lzcard set stateflag = '5' where startno = '"
						+ mProposalContNo + "' with ur";
				if (mProposalContNo.equals("")) {
					throw new MidplatException("工行网银无可提供的单证号！");
				}
				if ("1".equals(new ExeSQL().getOneValue(tSQL))) { // 同时两个人出单  系统查出相同的单证
					throw new MidplatException("此保单数据被别的交易挂起，请稍后！");
				} else if (new ExeSQL().getOneValue(mSQL).equals("1")) { // 一般不会出现这种情况
					throw new MidplatException("该投保单印刷号已使用，请更换！");
				} else {
					mLCCont.getChild(PrtNo).setText(mProposalContNo);
					mLCCont.getChild(ProposalContNo).setText(mProposalContNo);
				}
			}
	
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			//新增加广东地区 投被保人关系为父子、母女、母子、父女关系时 投被保人年龄之差大于18岁
			if(mGlobalInput.ManageCom.startsWith("8644")){
				String mRelaToInsured = mLCCont.getChild("LCAppnt").getChildText("RelaToInsured");
				String mRelaToAppnt = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("RelaToAppnt");
				String mAppntBirthday = mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");
				String mBirthday = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("Birthday");
				if (mRelaToInsured.equals("03") || mRelaToInsured.equals("04")
						|| mRelaToInsured.equals("05")
						|| mRelaToAppnt.equals("03")
						|| mRelaToAppnt.equals("04")
						|| mRelaToAppnt.equals("05")) {
					if(!BlackPhoneCheck.checkBirthday(mAppntBirthday, mBirthday)){
						throw new MidplatException("投被保人关系父子、父女、母女、母子关系时，投被保人年龄差值小于18岁不通过！");
					}
				}
			} 
			
			//广东地区所有银行年龄加险种期缴校验
			if(mGlobalInput.ManageCom.startsWith("8644"))
			{
				
				List lisRisk = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
				for( int i= 0;i<lisRisk.size() ;i++){
					Element Risk = (Element)lisRisk.get(i);
				if(Risk != null)
				{
					
					String PayIntv = Risk.getChildTextTrim("PayIntv");  //获取缴费频率
					String InsuYear = Risk.getChildTextTrim("InsuYear");  //获取保险期间
					cLogger.info("获取缴费频率"+PayIntv);
					cLogger.info("获取保险期间"+InsuYear);
					
					if(!"".equals(InsuYear)){
						String appAge =  mLCCont.getChild("LCAppnt").getChildText("AppntBirthday");  //获取投保人年龄
						System.out.println("appAge"+appAge);
//						int Age = CalAge.getAppntAge(appAge);
						int Age = this.getAppntAge(appAge);
						cLogger.info("获取投保人年龄:"+appAge);
						
						if(Age >60 && (Integer.parseInt(InsuYear))>1){
							throw new MidplatException("投保人年龄大于60岁,且保险期间大于1年,不通过...");
						}
					}
				}
				}
				
				//增加广东地区全部地址增加长度不小于9个汉字校验  add by kangz
				String adress  = mLCCont.getChild("LCAppnt").getChildTextTrim("HomeAddress"); 
				cLogger.info("adress is :" + adress);	
				if(adress.length()<9){
					throw new MidplatException("投保人家庭住址位数长度不能小于9");
				}
				//增加固定电话填写3或4位区号后面加非“400”和“800”开头的10位数字时不通过校验
				String AppntPhone  = mLCCont.getChild("LCAppnt").getChildTextTrim("AppntPhone");
				cLogger.info("AppntPhone is :" + AppntPhone);
				if(!"".equals(AppntPhone))
				{
					if(AppntPhone.length() ==13)
					{
						AppntPhone = AppntPhone.substring(3, 6);
						if(!AppntPhone.equals("400") && !AppntPhone.equals("800"))
						{
							throw new MidplatException("固定电话为13位时,第4位至6位只能能为400或800");
						}
					}
					else if(AppntPhone.length() == 14)
					{
						AppntPhone = AppntPhone.substring(4, 7);
						if(!AppntPhone.equals("400") && !AppntPhone.equals("800"))
						{
							throw new MidplatException("固定电话为14位时,第5位至7位只能能为400或800");
						}
					}
				}
				
			}

			// 山西地区校验电话号码(20121112,增加江苏号码校验)（20121128，增加四川校验、黑龙江、内蒙古、20130109陕西、20130603 山东）
//			String mInsuredPhone = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("HomePhone");
//			String mInsuredMobile = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChildText("InsuredMobile");
			
			if (mGlobalInput.ManageCom.startsWith("8614") || mGlobalInput.ManageCom.startsWith("8632") || mGlobalInput.ManageCom.startsWith("8651") || mGlobalInput.ManageCom.startsWith("8615") || mGlobalInput.ManageCom.startsWith("8661")|| mGlobalInput.ManageCom.startsWith("8637")) {
				//山西 投保人地址、电话不能为空
				String xHomeAddress = mLCCont.getChild("LCAppnt").getChildText("HomeAddress");
				if("".equals(xHomeAddress) || xHomeAddress == null){
					throw new MidplatException("投保人联系地址不能为空！");
				}
				if(("".equals(mAppntPhone) || mAppntPhone == null) 
						&& ("".equals(mAppntMobile) || mAppntMobile == null)
						&& ("".equals(mAppntOfficePhone) || mAppntOfficePhone == null)){
					throw new MidplatException("投保人联系电话至少一个不能为空！");
				}
				
				BlackPhoneCheck.checkPhone(mAppntPhone);//校验投保人号码
				BlackPhoneCheck.checkPhone(mAppntMobile);
//				BlackPhoneCheck.checkPhone(mInsuredPhone);// 校验被保人号码
//				BlackPhoneCheck.checkPhone(mInsuredMobile);
				
			}
			
			//对工行山西地区新增校验 add  by  mxk
			if(mGlobalInput.ManageCom.startsWith("8614") && mBaseInfo.getChildTextTrim("BankCode").equals("01"))
			{
				String m_AppntName = mLCCont.getChild("LCAppnt").getChildTextTrim("AppntName");
				List Lis_Insu = mLCCont.getChild("LCInsureds").getChildren("LCInsured");
				String insu_Name = "";
				String HomeAddress = "";
				String RelaToAppnt = "";
				
				cLogger.info("获取到投保人姓名是::"+m_AppntName);
				if(m_AppntName.getBytes().length<4){
					throw new MidplatException("投保人姓名长度必须大于或等于两个汉字!");
				}
				
				for(int i = 0 ;i <Lis_Insu.size();i++)
				{
					Element insu = (Element)Lis_Insu.get(i);
					insu_Name = insu.getChildTextTrim("Name");
					HomeAddress = insu.getChildTextTrim("HomeAddress");
					RelaToAppnt = insu.getChildTextTrim("RelaToAppnt");
					cLogger.info("获取到被保人姓名是::"+insu_Name);
					cLogger.info("获取到被保人地址是::"+HomeAddress);
					cLogger.info("获取到的主被保人与投保人关系是::"+RelaToAppnt);
					if("".equals(HomeAddress) || HomeAddress.equals(null)){
						throw new MidplatException("被保人联系地址不能为空！");
					}
					
					if(("".equals(RelaToAppnt))){
						throw new MidplatException("主被保人与投保人关系不能为空!");
					}
					
					
					if(insu_Name.getBytes().length <4)
					{
						throw new MidplatException("被保人姓名长度必须大于或等于两个汉字!");
					}
					
					List my_Risk = insu.getChild("Risks").getChildren("Risk");
					for(int m = 0;m<my_Risk.size();m++)
					{
						   Element mmy_Risk = (Element)my_Risk.get(m);
						   String coun_Bnf = mmy_Risk.getChildText("mmy_Risk");
						   
						   if(!("0".equals(coun_Bnf)))
						   {
							 Element my_LCBnfs =   mmy_Risk.getChild("LCBnfs");
							 List mmLCBnfs = my_LCBnfs.getChildren("LCBnf");
							 
							 for(int n=0;n<mmLCBnfs.size();n++)
							 {
								 Element lCBnf =  (Element)mmLCBnfs.get(n);
								 String bnf_name = lCBnf.getChildText("Name");
								 cLogger.info("获取到被保人姓名是::"+insu_Name);
								 if(bnf_name.getBytes().length<4)
								 {
									 throw new MidplatException("指定受益人姓名长度必须大于或等于两个汉字!");
								 }
							 }
						   }
					}
				}
			}
			
			// 陕西地区校验投被保人地址不能为空且内容在12个字符以上。  add by wz 20130812
			if(mGlobalInput.ManageCom.startsWith("8661")){
				String msg = checkAddre(mLCCont);
				if(!"".equals(msg)){
					throw new MidplatException(msg);
				}
			}
			
			Calendar calendar1 = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar1.add(Calendar.YEAR, -1);
			Date nowDate1 = calendar1.getTime();	//三号文获取当前日期前一年日期
			String compareDate = sdf.format(nowDate1);	//对比日期
			String nowDate = DateUtil.getCurDate("yyyy-MM-dd"); //当前日期
			String nowMonth = nowDate.substring(5, 7);//当前日期月份
			
			//人均收入的校验延迟一个季度 每年前四个月的年收入应对比大前年的平均人收入
			if(Integer.parseInt(nowMonth) < 4){ //每年前四个月
				calendar1.add(Calendar.YEAR, -1);
				nowDate1 = calendar1.getTime();	//三号文获取当前日期前2年日期
				compareDate = sdf.format(nowDate1);	//对比日期
			}
			
			//工行、交行三号文校验
			if("01".equals(mBaseInfo.getChildText(BankCode)) || "57".equals(mBaseInfo.getChildText(BankCode))|| "10".equals(mBaseInfo.getChildText(BankCode))){
				ExeSQL pExeSQL = new ExeSQL();
				String pSQL;
				SSRS pSSRS= new SSRS();
				if("1".equals(mCountyType)){	//城镇
					pSQL = "select RecentPCDI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
					pSSRS = pExeSQL.execSQL(pSQL);
					if(pSSRS.MaxRow > 0){
						String tRecentPCDI = pSSRS.GetText(1, 1);
						if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCDI)){
							if("01".equals(mBaseInfo.getChildText(BankCode))){	//增加工行自核转人核
								throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
							}else {
								throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCDI +"不允许投保！！！");
							}
						}
					}else {
						throw new MidplatException("未录入当地人均收入，请检查！！！");
					}	
				}else if("2".equals(mCountyType)){	//乡村
					pSQL = "select RecentPCNI from LAResidentIncome where  ManageCom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' and ResidentType = '" + mCountyType + "' and  startdate <= '" + compareDate + "' and enddate >= '" + compareDate + "' with ur";
					pSSRS = pExeSQL.execSQL(pSQL);
					if(pSSRS.MaxRow > 0){
						String tRecentPCNI = pSSRS.GetText(1, 1);
						if(Double.parseDouble(mSalary) < Double.parseDouble(tRecentPCNI)){
							if("01".equals(mBaseInfo.getChildText(BankCode))){	//增加工行自核转人核
								throw new MidplatException("1222投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
							}else {
								throw new MidplatException("投保人年收入:"+ mSalary +"低于当地人均收入:"+ tRecentPCNI +"不允许投保！！！");
							}
						}
					}else {
						throw new MidplatException("未录入当地人均收入，请检查！！！");
					}	
				}
			}
			
			ycheck.checkRiskForYBT(pInXmlDoc);//add by gcy 20180427
			
			//获取福佑相伴按地区销售套餐编码
			if("511101".equals(tRiskCode) && "01".equals(mBaseInfo.getChildText(BankCode))){
				String zSQL = "select riskwrapcode,enableflag from lkratemapping where bankcode = '" + mBaseInfo.getChildText(BankCode) + "' and managecom = '" + mGlobalInput.ManageCom.substring(0, 4) + "' with ur";
				SSRS pSSRS= new SSRS();
				pSSRS = new ExeSQL().execSQL(zSQL);
				if(pSSRS.MaxRow != 0){
					if("Y".equals(pSSRS.GetText(1, 2).trim())){
						mRisk.getChild(MainRiskCode).setText(pSSRS.GetText(1, 1));
					}else{
						throw new MidplatException("该地区" + pSSRS.GetText(1, 1) + "套餐已停售或未启用");
					}
				}else{
					mRisk.getChild(MainRiskCode).setText("YBT006");
				}
			}
//			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
//			String tRiskCode = pInXmlDoc.getRootElement().getChild(LCCont)
//			.getChild(LCInsureds).getChild(LCInsured)
//			.getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
			
			//工行 陕西-8661 by lxl 20150410
//			if ("01".equals(mBaseInfo.getChildText(BankCode)) && tRiskCode.equals("333701")) {
//				if (mGlobalInput.ManageCom.startsWith("8661")) {
//					throw new MidplatException("福利双全该地区已停售！");
//				}
//			}
			
			cLogger.debug("RiskCode = " + tRiskCode);
//			Element tThisConfRoot = cThisBusiConf.getParent();
			
			//人保30款万能型人身保险费率不合规停售,针对所有分公司   add  by mxk 20150629
			 if(-1 != tThisConfRoot.getChildText("StopSale").indexOf(tRiskCode)){
				 throw new MidplatException("该产品已停售");    
			 }
			 
			//停售校验 add by GaoJinfu 20180329
			String status = ycheck.riskStopCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"));
			if (status.equals("2")) {
				throw new MidplatException("该产品已停售!");  
			}
			boolean gxFlag = ycheck.riskValidCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"),"GX");
			boolean wnFlag = ycheck.riskValidCheck(mBaseInfo.getChildText(BankCode), mGlobalInput.ManageCom, tRiskCode, mLCCont.getChildText("SourceType"),"WN");
			
			if (-1 != tThisConfRoot.getChildText("GX_Risk").indexOf(tRiskCode) || gxFlag) {	//走标准个险流程
				//投保单录入
				YbtContBL ttYbtContBL = new YbtContBL(
						pInXmlDoc, mGlobalInput);
				ttYbtContBL.deal();
				
				//被保人、险种信息录入
				YbtInsuredProposalBL ttYbtInsuredProposalBL = new YbtInsuredProposalBL(
						pInXmlDoc, mGlobalInput);
				ttYbtInsuredProposalBL.deal();
				
				//自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();
				
				//财务收费
				YbtTempFeeBL ttYbtTempFeeBL = new YbtTempFeeBL(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtTempFeeBL.deal();
				
				//签单+单证核销+回执回销
				YbtLCContSignBL ttYbtLCContSignBL = new YbtLCContSignBL(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtLCContSignBL.deal();
				
				//组织返回报文
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						ttYbtLCContSignBL.getContNo());
				mOutXmlDoc = ttYbtContQueryBL.deal();
			} else if (-1 != tThisConfRoot.getChildText("WN_Risk").indexOf(tRiskCode) || wnFlag) {	//走picch特有的国际贸易流程
				//投保单录入
				YbtContBL ttYbtContBL = new YbtContBL(
						pInXmlDoc, mGlobalInput);
				ttYbtContBL.deal();
				
				//被保人、险种信息录入
				YbtContInsuredIntlBL ttYbtContInsuredIntlBL = new YbtContInsuredIntlBL(
						pInXmlDoc, mGlobalInput);
				ttYbtContInsuredIntlBL.deal();
				
				//自核+复核
				YbtAutoCheck ttYbtAutoCheck = new YbtAutoCheck(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtAutoCheck.deal();
				
				//财务收费
				YbtTempFeeBL ttYbtTempFeeBL = new YbtTempFeeBL(
						mLCCont.getChildText(PrtNo), mGlobalInput);
				ttYbtTempFeeBL.deal();
				
				//签单+单证核销+回执回销
				YbtLCContSignBL ttYbtLCContSignBL = new YbtLCContSignBL(
					mLCCont.getChildText("PrtNo"), mGlobalInput);
				ttYbtLCContSignBL.deal();


				//组织返回报文
				YbtContQueryBL ttYbtContQueryBL = new YbtContQueryBL(
						ttYbtLCContSignBL.getContNo());
				mOutXmlDoc = ttYbtContQueryBL.deal();			
			}
			else {
				throw new MidplatException("险种代码有误！" + tRiskCode);
			}
			
			//单证核销！
			if (mBaseInfo.getChildText(BankCode).equals("01")
					|| mBaseInfo.getChildText(BankCode).equals("08")
					|| mBaseInfo.getChildText(BankCode).equals("66")) {// 广州农商add by gcy 20180427
				if (mLCCont.getChildText("SourceType").equals("1") || mLCCont.getChildText("SourceType").equals("8") || mLCCont.getChildText("SourceType").equals("e")) {
					cLogger.info("工行网银不进行单证核销！！！");
				}else {
					if (!mLKTransStatusDB.getProposalNo().startsWith("YBT")) {
						new CardManage("HB07/07B", mGlobalInput).makeUsed(mLKTransStatusDB.getProposalNo());
					}
				}
			}else {
				if (!mLKTransStatusDB.getProposalNo().startsWith("YBT")) {
					new CardManage("HB07/07B", mGlobalInput).makeUsed(mLKTransStatusDB.getProposalNo());
				}
			}
			
			//增加所有渠道的电话号码重复性校验 add by LiXiaoLong 2014-07-07 begin
			String mAppntNo = mOutXmlDoc.getRootElement().getChild(LCCont).getChild(LCAppnt).getChildText(AppntNo);
			BlackPhoneCheck.checkAllPhone(mAppntNo, mAppntPhone, mAppntOfficePhone, mAppntMobile);
			
			//工行、增加投保人收入、家庭年收入、所属区域 add by LiXiaoLong 2014-03-19 begin
			if("01".equals(mBaseInfo.getChildText(BankCode)) || "57".equals(mBaseInfo.getChildText(BankCode)) || "10".equals(mBaseInfo.getChildText(BankCode))){
				mLCContSubDB = insertLCContSubDB(pInXmlDoc,mGlobalInput);
			}
			
			//超时自动删除数据
			long tUseTime = System.currentTimeMillis() - mStartMillis;
			long tTimeOutLong = 10 * 60 * 1000;	//默认超时设置为1分钟；如果未配置超时时间，则使用该值。
			
			try {
				tTimeOutLong = Integer.parseInt(cThisBusiConf.getChildText(timeout)) * 1000L;
			} catch (Exception ex) {	//使用默认超时设置
				cLogger.warn("未配置超时设置，或配置有误！" + ex);
			}
			if (tUseTime > tTimeOutLong) {
				cLogger.error("核心处理超时！UseTime=" + tUseTime/1000.0 + "s；TimeOut=" + tTimeOutLong/1000.0 + "s；投保书号：" + mLKTransStatusDB.getPrtNo());
				throw new MidplatException("系统繁忙，请稍后再试！" + tUseTime);
			}
			
			//工行百万安行、福利双全、 北肿删除锁机制 add by wz 20131104
			if("01".equals(mBaseInfo.getChildText(BankCode))){
				List xRiskList = mLCCont.getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
				for (int i = 0; i < xRiskList.size(); i++) {
					Element xRisk = (Element)xRiskList.get(i);
					if (xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
							&& "333701".equals(xRisk.getChildText("MainRiskCode"))
							|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
							&& "232101".equals(xRisk.getChildText("MainRiskCode"))
							|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
							&& "JZJ001".equals(xRisk.getChildText("MainRiskCode"))
							|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
							&& "JZJ002".equals(xRisk.getChildText("MainRiskCode"))
							|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
							&& "JZJ003".equals(xRisk.getChildText("MainRiskCode"))
							|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
							&& "JZJ004".equals(xRisk.getChildText("MainRiskCode")) 
							) {
						//比较工行传入的保费与核心算出的保费是否一致。
						double xPrem = 0;
						String iPrem = xRisk.getChildText("Prem");
						if(!"".equals(iPrem) && iPrem != null){
							xPrem = Double.parseDouble(iPrem);
						}
						
						List tRiskList = mOutXmlDoc.getRootElement().getChild("LCCont").getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
						double tPrem = 0;
						for (int j = 0; j < tRiskList.size(); j++) {
							tPrem += Double.parseDouble(((Element)tRiskList.get(j)).getChildText("Prem"));
						}
						
						if(xPrem != tPrem){
							if(xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
								&& "232101".equals(xRisk.getChildText("MainRiskCode"))
								|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
								&& "JZJ001".equals(xRisk.getChildText("MainRiskCode"))
								|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
								&& "JZJ002".equals(xRisk.getChildText("MainRiskCode"))
								|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
								&& "JZJ003".equals(xRisk.getChildText("MainRiskCode"))
								|| xRisk.getChildText("MainRiskCode").equals(xRisk.getChildText("MainRiskCode"))
								&& "JZJ004".equals(xRisk.getChildText("MainRiskCode"))
							){
								//北肿删除锁机制
								String delete_sql = "delete From LockTable where NoLimit = '" + mProposalContNo + "' with ur";
								new ExeSQL().execUpdateSQL(delete_sql);
							}
							throw new MidplatException("正确的保费为：" + tPrem + "，请返回修改。");
						}
					}
				}
			}
						
			Element tOutLCCont = mOutXmlDoc.getRootElement().getChild(LCCont);
			mLKTransStatusDB.setPolNo(tOutLCCont.getChildText(ContNo));
			mLKTransStatusDB.setTransAmnt(tOutLCCont.getChildText(Prem));
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
			mLKTransStatusDB.setStatus("1");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
			
			//银保通交叉销售
			ybtMixSale(mGlobalInput, tOutLCCont.getChildText(ContNo));
			
			//未做系统录入(客户姓名、性别、证件类型（身份证或其他）、证件号码、录入有效期)且超过20万的保单，做拒保处理  add by gcy 20180205
			/*double mPrem = Double.parseDouble(mOutXmlDoc.getRootElement().getChild("LCCont").getChildTextTrim("Prem"));
			if(mPrem > Double.parseDouble("200000.00")){
				Element mLCAppnt = mOutXmlDoc.getRootElement().getChild("LCCont").getChild("LCAppnt");
				String mSQL = "select Name,Sex,IDType,IDNo,EffEctiveDate from LCYBTCustomer where Name='"+mLCAppnt.getChildText("AppntName")+"'" +
						" and IDType='"+mLCAppnt.getChildText("AppntIDType")+"' and IDNo='"+mLCAppnt.getChildText("AppntIDNo")+"'" +
								" order by makedate desc,maketime desc with ur";
				SSRS tSSRS = new ExeSQL().execSQL(mSQL);
				if(tSSRS.getMaxRow()<1){
					throw new MidplatException("累计保费超限，需保险公司后台登记");
				}
				
				if("".equals(tSSRS.GetText(1, 1)) || "".equals(tSSRS.GetText(1, 2)) 
						|| "".equals(tSSRS.GetText(1, 3)) || "".equals(tSSRS.GetText(1, 4))
						|| "".equals(tSSRS.GetText(1, 5))){
					throw new MidplatException("累计保费超限，需保险公司后台登记");
				}
				
				if(tSSRS.GetText(1, 5) != null && !"".equals(tSSRS.GetText(1, 5))){
					FDate fDate = new FDate();
					Date effectiveDate = fDate.getDate(tSSRS.GetText(1, 5));
					Date tranDate = fDate.getDate(mBaseInfo.getChildText("BankDate"));
//					if (fDate.mErrors.needDealError())
//					{
//						throw new MidplatException("累计保费超限，需保险公司后台登记");
//					}
					
					GregorianCalendar tCalendar = new GregorianCalendar();
					tCalendar.setTime(tranDate);
					int sYears = tCalendar.get(Calendar.YEAR);
					int sMonths = tCalendar.get(Calendar.MONTH);
					int sDays = tCalendar.get(Calendar.DAY_OF_MONTH);

					GregorianCalendar eCalendar = new GregorianCalendar();
					eCalendar.setTime(effectiveDate);
					int eYears = eCalendar.get(Calendar.YEAR);
					int eMonths = eCalendar.get(Calendar.MONTH);
					int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
					
					tCalendar.set(sYears, sMonths, sDays);
					eCalendar.set(eYears, eMonths, eDays);
					long lInterval = (eCalendar.getTime().getTime() - tCalendar.getTime().getTime()) / 86400000;
					if(lInterval < 0 || lInterval>31){
						throw new MidplatException("累计保费超限，需保险公司后台登记");
					}
				}
			}*/
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			} 
			if (mDeleteable) {
				try {
					YbtSufUtil.clearData(mLCCont.getChildText(PrtNo));
				} catch (Exception tBaseEx) {
					cLogger.error("删除新单数据失败！", tBaseEx);
				}
			}
			
			//---------- 增加工行自核不过转人工核保返回值   add by 20140714 lxl  --------
			if("01".equals(mBaseInfo.getChildText(BankCode))){
				if(ex.getMessage().startsWith("1222")){
					mOutXmlDoc = YbtSufUtil.getSimpOutXml("3", ex.getMessage().substring(4));
				}else{
					mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
				}
			}else {
				mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
			}
			//-------------- end ---------------
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}

		cLogger.info("Out AIO_NewCont.service()!");
		return mOutXmlDoc;
	}
	
	
	 public  int getAppntAge (String ageDate) {
	    	
	    	Date now = new Date();
	    	if(ageDate.indexOf("-")>0){
	    		ageDate = ageDate.replaceAll("-", "");
	    	}
			String sdf = new SimpleDateFormat("yyyyMMdd").format(now);
			
			int age = 0;
			int staY = Integer.parseInt(ageDate.substring(0,4));
			int staM = Integer.parseInt(ageDate.substring(4,6));
			int staD = Integer.parseInt(ageDate.substring(6,8));
			
			int nowY = Integer.parseInt(sdf.substring(0,4));
			int nowM = Integer.parseInt(sdf.substring(4,6));
			int nowD = Integer.parseInt(sdf.substring(6,8));
			
			if(staM > nowM){
				age = nowY - staY -1;
			}else if(staM == nowM) {
				if(nowD > staD){
					age = nowY - staY ;
				}else if(nowD == staD) {
					age = nowY - staY ;
				}else if(nowD < staD) {
					age = nowY - staY -1 ;
				}
					
			}else if(staM < nowM) {
				age = nowY - staY ;
			}
			
	    	return age;
	    }
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into AIO_NewCont.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		String mRiskCode = pXmlDoc.getRootElement().getChild(LCCont)
		.getChild(LCInsureds).getChild(LCInsured)
		.getChild(Risks).getChild(Risk).getChildText(MainRiskCode);
		mLKTransStatusDB.setRiskCode(mRiskCode);
		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setStatus("0");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		//存储区分工行网银标识 add by lxl 网银：SourceType = 1，终端：SourceType = 8
		if(mBaseInfo.getChildText(BankCode).equals("01")){
			if(mLCCont.getChildText("SourceType").equals("1") || mLCCont.getChildText("SourceType").equals("8")){
				mLKTransStatusDB.setbak1(mLCCont.getChildText("SourceType"));
			}
		}
		
		//交行三号文要求增加出单网点名称、销售人员工号、负责人姓名
		// add by LiXiaoLong 20140506 begin     
		if(mBaseInfo.getChildText(BankCode).equals("10")){
			mLKTransStatusDB.setbak1(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("SaleCertNo"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("SaleCertName"));
		}
		
		//江苏银行三号文要求增加出单网点名称、销售人员工号、负责人姓名
		// add by LiXiaoLong 20140507 begin     
		if(mBaseInfo.getChildText(BankCode).equals("57")){
			mLKTransStatusDB.setbak1(mLCCont.getChildText("BrName"));
			mLKTransStatusDB.setbak2(mLCCont.getChildText("SaleCertNo"));
			mLKTransStatusDB.setbak4(mLCCont.getChildText("SaleCertName"));
		}
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out AIO_NewCont.insertTransLog()!");
		return mLKTransStatusDB;
	}
    
	/**
     * 银保通交叉销售.
     * @param pGlobalInput
     * @param pContNo
     * @throws Exception
	 */
	private void ybtMixSale(GlobalInput pGlobalInput, String pContNo) throws Exception {
		cLogger.info("Into AIO_NewCont.ybtMixSale()!");
        GlobalInput mGlobalInput = pGlobalInput;
        String mAgentCom = mGlobalInput.AgentCom;
        String mContNo = pContNo;
        String mSQL;
		//根据agentcom判断是否是银保通交叉销售网点.如果此中介机构不在交叉定义表中,查询上级中介机构
        //直到上级中介机构为空或在交叉销售定义表中查到记录为止
        while (mAgentCom != null && !"".equals(mAgentCom)) {
            mSQL = "select 1 from LOMixBKAttribute where bandcode = '" + mAgentCom + "' and state = '1' with ur";
            if ("1".equals(new ExeSQL().getOneValue(mSQL))) {
                ybtMixSaleUpdate(mAgentCom, mContNo);
                break;
            } else {
                mSQL = "select upagentcom from lacom where agentcom = '" + mAgentCom + "' with ur";
                mAgentCom = new ExeSQL().getOneValue(mSQL);
            }
        }
		cLogger.info("Out AIO_NewCont.ybtMixSale()!");
	}
	
    /**
     * 更新lccont中标识银保通交叉销售的几个字段.
     * @param mAgentCom
     * @param mContNo
     * @throws ParseException
     * @throws MidplatException
     */
    private void ybtMixSaleUpdate(String mAgentCom, String mContNo) throws ParseException, MidplatException {
        cLogger.info("Into AIO_NewCont.ybtMixSaleUpdate()!");
        
        LOMixBKAttributeDB mLOMixBKAttributeDB = new LOMixBKAttributeDB();
		mLOMixBKAttributeDB.setBandCode(mAgentCom); //此字段存储的是中介机构代码
		LOMixBKAttributeSet mLOMixBKAttributeSet = mLOMixBKAttributeDB.query();
        
		if (mLOMixBKAttributeSet.size() > 0) { //此网点为交叉销售网点
			LOMixBKAttributeSchema mLOMixBKAttributeSchema = mLOMixBKAttributeSet.get(1);
			Date mNowDate = new Date();
			SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date mInvalidDate = DateFormat.parse(mLOMixBKAttributeSchema.getInvalidate());
			if (!mNowDate.after(mInvalidDate)) {
				cLogger.debug("交叉网点在有效期内,进行银保通交叉销售的更新.");
				LCContDB mLCContDB = new LCContDB();
				mLCContDB.setContNo(mContNo);
				mLCContDB.setAppFlag("1");
				if (!mLCContDB.getInfo()) {
					throw new MidplatException("未查询到保单信息!");
				}
				mLCContDB.setCrs_BussType(mLOMixBKAttributeSchema.getCrs_BussType());
				mLCContDB.setCrs_SaleChnl(mLOMixBKAttributeSchema.getCrs_SaleChnl());
				mLCContDB.setGrpAgentCode(mLOMixBKAttributeSchema.getGrpAgentCode());
				mLCContDB.setGrpAgentCom(mLOMixBKAttributeSchema.getGrpAgentCom());
				mLCContDB.setGrpAgentIDNo(mLOMixBKAttributeSchema.getGrpAgentIDNo());
				mLCContDB.setGrpAgentName(mLOMixBKAttributeSchema.getGrpAgentName());
				if(!mLCContDB.update()){
					throw new MidplatException("更新保单信息失败!");
				}
			}
		}
        cLogger.info("Out AIO_NewCont.ybtMixSaleUpdate()!");
    }
    
    private String checkAddre(Element xLCCont){
		cLogger.info("Into AIO_NewCont.checkAddre()...");
		String result = "";
		
		Element xAppnt = xLCCont.getChild(LCAppnt);
		Element xInsured = xLCCont.getChild(LCInsureds).getChild(LCInsured);
		
		String xAppntMailAddr = xAppnt.getChildText("MailAddress");
		String xAppntHomeAddr = xAppnt.getChildText("HomeAddress");
		String xInsuredHomeAddr = xInsured.getChildText("HomeAddress");
		String xInsuredMailAddr = xInsured.getChildText("MailAddress");
		
		if("".equals(xAppntMailAddr) || xAppntMailAddr == null){
			result = "投保人邮寄地址不能为空！";
			return result;
		}else {
			if(xAppntMailAddr.length() < 12){
				result = "投保人邮寄地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xAppntHomeAddr) || xAppntHomeAddr == null){
			result = "投保人联系地址不能为空！";
			return result;
		}else {
			if(xAppntHomeAddr.length() < 12){
				result = "投保人联系地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xInsuredHomeAddr) || xInsuredHomeAddr == null){
			result = "被保人联系地址不能为空！";
			return result;
		}else {
			if(xInsuredHomeAddr.length() < 12){
				result = "被保人联系地址长度不足12位！";
				return result;
			}
		}
		if("".equals(xInsuredMailAddr) || xInsuredMailAddr == null){
			result = "被保人邮寄地址不能为空！";
			return result;
		}else {
			if(xInsuredMailAddr.length() < 12){
				result = "被保人邮寄地址长度不足12位！";
				return result;
			}
		}
		cLogger.info("Out AIO_NewCont.checkAddre()...");
		return result;
	}
    
    private LCContSubDB insertLCContSubDB(Document pXmlDoc,GlobalInput pGlobalInput) throws MidplatException {
    	cLogger.info("Into AIO_NewCont.insertLCContSubDB()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		Element mLCAppnt = mLCCont.getChild(LCAppnt);

		LCContSubDB mLCContSubDB = new LCContSubDB();
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		mLCContSubDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLCContSubDB.setCountyType(mLCAppnt.getChildText("CountyType"));
		mLCContSubDB.setFamilySalary(mLCAppnt.getChildText("FamilySalary"));
		mLCContSubDB.setManageCom(pGlobalInput.ManageCom);
		mLCContSubDB.setOperator("ybt");
		mLCContSubDB.setMakeDate(mCurrentDate);
		mLCContSubDB.setMakeTime(mCurrentTime);
		mLCContSubDB.setModifyDate(mCurrentDate);
		mLCContSubDB.setModifyTime(mCurrentTime);
		
		if (!mLCContSubDB.insert()) {
			cLogger.error(mLCContSubDB.mErrors.getFirstError());
			throw new MidplatException("插入投保人家庭年收入表失败！");
		}
		
		String mSQL = "update LCAppnt set Salary = '" + mLCAppnt.getChildText("Salary") + "' where prtno='" + mLCCont.getChildText(PrtNo) + "' with ur";
		ExeSQL mExeSQL = new ExeSQL();
		if (!mExeSQL.execUpdateSQL(mSQL)) {
			cLogger.error(mExeSQL.mErrors.getFirstError());
			throw new MidplatException("更新投保人家庭年收入失败！！！");
		}
		
		cLogger.info("Out AIO_NewCont.insertLCContSubDB()!");
		return mLCContSubDB;
    	
    }
    
        private void checkIDNo(String idno, String birthday, String sex) throws MidplatException{
		
    	cLogger.info("身份校验开始");
    	cLogger.info("获取到身份证号" + idno);
    	cLogger.info("获取到生日" + birthday);
    	cLogger.info("获取到性别" + sex);
    	
    	//根据身份证号判断出生年月和性别是否与身份证相符
		Pattern idNumPattern = Pattern.compile("(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9xX])");  
		Matcher idNumMatcher = idNumPattern.matcher(idno); 
		String tmpbirthday = "";
		String tmpsex = "";
		String tmppri = "";
		String tmplast = "";
		String priflag = "0";
		String lastflag = "0";
		String pricode[] = { "11", "12", "13", "14", "15", "21",  
	            "22", "23", "31", "32", "33", "34", "35", "36", "37", "41", "42",  
	            "43", "44", "45", "46", "50", "51", "52", "53", "54", "61", "62",  
	            "63", "64", "65", "71", "81", "82", "91" };
		String lastcode[] = { "1", "2", "3", "4", "5", "6",  
	            "7", "8", "9", "0", "X" };
		if (idno.trim().length() == 18 && idNumMatcher.matches()) {
			tmpbirthday = idno.substring(6, 14);
			tmpsex = idno.substring(16, 17);
			tmppri = idno.substring(0, 2);
			tmplast = idno.substring(17, 18);
		} else if (idno.trim().length() == 15) {
			tmpbirthday = idno.substring(6, 12);
			tmpbirthday = "19" + tmpbirthday;
			tmpsex = idno.substring(14, 15);
			tmppri = idno.substring(0, 2);
			tmplast = idno.substring(17, 18);
		} else {
			throw new MidplatException("身份证号有误！" + idno);
		}
		
		/**
		 * add by wangxt in 2009-4-23
		 */
		birthday = birthday.replaceAll("-", "");
		System.out.println(birthday);
		System.out.println(tmpbirthday);
		if (!birthday.equals(tmpbirthday)) {
			throw new MidplatException("身份证号与出生日期不符！" + idno);
		}
		//省份有效性校验
		for (int i = 0; i < pricode.length; i++) {
			if (tmppri.equals(pricode[i])) {
				priflag = "1";
			}
		}
		if ("0".equals(priflag)) {
			throw new MidplatException("身份证号的省份信息无效！" + idno);
		}
		//身份证最后一位校验
		for (int i = 0; i < lastcode.length; i++) {
			if (tmplast.equals(lastcode[i])) {
				lastflag = "1";
			}
		}
		if ("0".equals(lastflag)) {
			throw new MidplatException("身份证号的最后一位只能为0-9或X！" + idno);
		}
		
		if (tmpsex.equals("0") || tmpsex.equals("2") || tmpsex.equals("4") ||
				tmpsex.equals("6") || tmpsex.equals("8")) {
			if (sex == null || sex.trim().equals("0")) {
				throw new MidplatException("身份证号与性别不符！" + idno);
			}
		} else {
			if (sex == null || sex.trim().equals("1")) {
				throw new MidplatException("身份证号与性别不符！" + idno);
			}
		}
		cLogger.info("身份校验结束!");
	}
	
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "F:\\000gaojinfu\\bankMassage\\icbc\\instd\\123.xml";
		String mOutFile = "F:/out.xml";
		
		String elpath = "F:\\sinosoft\\picch_suf\\WebRoot\\WEB-INF\\conf\\midplatSuf.xml";
		FileInputStream a = new FileInputStream(elpath);
		InputStreamReader b = new InputStreamReader(a, "GBK");
		Document c = new SAXBuilder().build(b);
		SAXReader saxreader = new SAXReader();
		Element root = c.getRootElement().getChild("business");
		
		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		
		Document mOutXmlDoc = new AIO_NewCont(root).service(mInXmlDoc);
		
		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
//		
//		System.out.println("成功结束！");
//		AIO_NewCont aio = new AIO_NewCont(null);
//		String ageDate = "19900101";
//		System.out.println(aio.getAppntAge(ageDate));
	}
}
