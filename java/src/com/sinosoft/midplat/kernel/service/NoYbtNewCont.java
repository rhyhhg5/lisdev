/**
 * 通用简单交易处理类。
 * 此类本身不做任何业务处理，仅插入一条交易日志。
 * 目前使用此类的交易有：中行冲正、建行绿灯
 */

package com.sinosoft.midplat.kernel.service;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class NoYbtNewCont extends ServiceImpl {
	public NoYbtNewCont(Element pThisConf) {
		super(pThisConf);
	}
	
	public Document service(Document pInXmlDoc) {
		cLogger.info("Into SimpService.service()...");
		Document mOutXmlDoc = null;
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc);
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mLKTransStatusDB.getBankCode(),
					mLKTransStatusDB.getBankBranch(),
					mLKTransStatusDB.getBankNode());
			
//			mOutXmlDoc = YbtSufUtil.getSimpOutXml("1", "交易成功！");
			mOutXmlDoc = deal(pInXmlDoc);
			
			mLKTransStatusDB.setRCode("1");	//空-未返回；1-交易成功，返回；0-交易失败，返回
			mLKTransStatusDB.setTransStatus("1");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		} catch (Exception ex) {
			cLogger.error("通用简单(SimpService)交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				mLKTransStatusDB.setDescr(ex.getMessage());
			}
			
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out SimpService.service()!");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc){
		Document outDocXml = YbtSufUtil.getSimpOutXml("1", "交易成功！");
		Element mDetails = new Element("Details");
		ExeSQL es = new ExeSQL();
		String tSQL = "select lp.riskcode,lc.prtno,lc.contno,lc.makedate,lc.appntname,lc.appntidno,lc.appntidtype,lc.appntbirthday,lc.appntsex," +
				"lca.nativeplace,ld.email,ld.mobile,ld.zipcode,ld.homephone,ld.homeaddress,lc.insuredname,lc.insuredidtype," +
				"lc.insuredidno,lc.insuredbirthday,lc.insuredsex,lcinsu.nativeplace,lp.payendyearflag,lp.payendyear,lp.insuyearflag,lp.insuyear,lc.payintv," +
				"lc.paymode,lc.bankaccno,lc.stateflag,lc.cvalidate,lc.cinvalidate,lp.copys,lc.amnt,lc.prem,ld.companyphone " +
				"from lccont lc, lcpol lp,lcappnt lca,lcaddress ld,lcinsured lcinsu " +
				"where lc.contno=lcinsu.contno and lc.contno=lp.contno and lc.contno=lca.contno and lca.appntno=ld.customerno " +
				"and lc.salechnl in ('04','13') and cardflag in ('6','8') and lc.bankcode like '02%' and lca.addressno=ld.addressno and lp.riskseqno in ('1','01')" +
				"and lc.makedate = '"+pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BankDate")+"' " +
						"group by lc.prtno,lc.makedate,lc.contno,lc.appntname,lc.appntidno,lc.appntidtype,lc.appntbirthday,lc.appntsex," +
						"lca.nativeplace,ld.email,ld.mobile,ld.zipcode,ld.homephone,ld.companyphone,ld.homeaddress,lc.insuredname," +
						"lc.insuredidtype,lc.insuredidno,lc.insuredbirthday,lc.insuredsex,lcinsu.nativeplace,lp.payendyearflag,lp.payendyear,lp.insuyearflag," +
						"lp.insuyear,lc.payintv,lc.paymode,lc.bankaccno,lc.stateflag,lc.cvalidate,lc.cinvalidate,lp.copys,lc.amnt,lc.prem,lp.riskcode with ur";
		SSRS ssrs = es.execSQL(tSQL);
		if(ssrs.MaxRow > 0){
			for(int i=1;i<=ssrs.MaxRow;i++){
				Element mDetail = new Element("Detail");
				Element mRiskCode = new Element("RiskCode");
				mRiskCode.setText(ssrs.GetText(i, 1));
				Element mPrtNo = new Element("PrtNo");
				mPrtNo.setText(ssrs.GetText(i, 2));
				Element mContNo = new Element("ContNo");
				mContNo.setText(ssrs.GetText(i, 3));
				Element mMakeDate = new Element("MakeDate");
				mMakeDate.setText(ssrs.GetText(i, 4));
				Element mAppntName = new Element("AppntName");
				mAppntName.setText(ssrs.GetText(i, 5));
				Element mAppntIDNo = new Element("AppntIDNo");
				mAppntIDNo.setText(ssrs.GetText(i, 6));
				Element mAppntIDType = new Element("AppntIDType");
				mAppntIDType.setText(ssrs.GetText(i, 7));
				Element mAppntBirthday = new Element("AppntBirthday");
				mAppntBirthday.setText(ssrs.GetText(i, 8));
				Element mAppntSex = new Element("AppntSex");
				mAppntSex.setText(ssrs.GetText(i, 9));
				Element mAppntNativePlace = new Element("AppntNativePlace");
				mAppntNativePlace.setText(ssrs.GetText(i, 10));
				Element mAppntEmail = new Element("AppntEmail");
				mAppntEmail.setText(ssrs.GetText(i, 11));
				Element mAppntMobile = new Element("AppntMobile");
				mAppntMobile.setText(ssrs.GetText(i, 12));
				Element mAppntZipCode = new Element("AppntZipCode");
				mAppntZipCode.setText(ssrs.GetText(i, 13));
				Element mAppntHomePhone = new Element("AppntHomePhone");
				mAppntHomePhone.setText(ssrs.GetText(i, 14));
				Element mAppntCompanyPhone = new Element("AppntCompanyPhone");
				mAppntCompanyPhone.setText(ssrs.GetText(i,35));
				Element mAppntAddress = new Element("AppntAddress");
				mAppntAddress.setText(ssrs.GetText(i, 15));
				Element mInsuredName = new Element("InsuredName");
				mInsuredName.setText(ssrs.GetText(i, 16));
				Element mInsuredIDType = new Element("InsuredIDType");
				mInsuredIDType.setText(ssrs.GetText(i, 17));
				Element mInsuredIDNo = new Element("InsuredIDNo");
				mInsuredIDNo.setText(ssrs.GetText(i, 18));
				Element mInsuredBirthday = new Element("InsuredBirthday");
				mInsuredBirthday.setText(ssrs.GetText(i, 19));
				Element mInsuredSex = new Element("InsuredSex");
				mInsuredSex.setText(ssrs.GetText(i, 20));
				Element mInsuredNativePlace = new Element("InsuredNativePlace");
				mInsuredNativePlace.setText(ssrs.GetText(i, 21));
				Element mInsuredEmail = new Element("InsuredEmail");
				mInsuredEmail.setText(ssrs.GetText(i, 11));
				Element mPayEndYearFlag = new Element("PayEndYearFlag");
				mPayEndYearFlag.setText(ssrs.GetText(i, 22));
				Element mPayEndYear = new Element("PayEndYear");
				mPayEndYear.setText(ssrs.GetText(i, 23));
				Element mInsuYearFlag = new Element("InsuYearFlag");
				mInsuYearFlag.setText(ssrs.GetText(i, 24));
				Element mInsuYear = new Element("InsuYear");
				mInsuYear.setText(ssrs.GetText(i, 25));
				Element mPayIntv = new Element("PayIntv");
				mPayIntv.setText(ssrs.GetText(i, 26));
				Element mPayMethod = new Element("PayMethod");
				mPayMethod.setText(ssrs.GetText(i, 27));
				Element mBankAccNo = new Element("BankAccNo");
				mBankAccNo.setText(ssrs.GetText(i, 28));
				Element mCurrcy = new Element("Currcy");
//				mCurrcy.setText(ssrs.GetText(1, 8));
				Element mStatus = new Element("Status");
				mStatus.setText(ssrs.GetText(i, 29));
				Element mFaileInfo = new Element("FaileInfo");
//				mFaileInfo.setText(ssrs.GetText(1, ));
				Element mCvalidate = new Element("Cvalidate");
				mCvalidate.setText(ssrs.GetText(i, 30));
				Element mCinvalidate = new Element("Cinvalidate");
				mCinvalidate.setText(ssrs.GetText(i, 31));
				Element mMult = new Element("Mult");
				mMult.setText(ssrs.GetText(i, 32));
				Element mAmnt = new Element("Amnt");
				mAmnt.setText(ssrs.GetText(i, 33));
				Element mPrem = new Element("Prem");
				mPrem.setText(ssrs.GetText(i, 34));
				Element mBankNode = new Element("BankNode");
//				mBankNode.setText(ssrs.GetText(1, 8));
				mDetail.addContent(mRiskCode);
				mDetail.addContent(mPrtNo);
				mDetail.addContent(mMakeDate);
				mDetail.addContent(mContNo);
				mDetail.addContent(mAppntName);
				mDetail.addContent(mAppntIDType);
				mDetail.addContent(mAppntIDNo);
				mDetail.addContent(mAppntBirthday);
				mDetail.addContent(mAppntSex);
				mDetail.addContent(mAppntNativePlace);
				mDetail.addContent(mAppntEmail);
				mDetail.addContent(mAppntMobile);
				mDetail.addContent(mAppntZipCode);
				mDetail.addContent(mAppntHomePhone);
				mDetail.addContent(mAppntCompanyPhone);
				mDetail.addContent(mAppntAddress);
				mDetail.addContent(mInsuredName);
				mDetail.addContent(mInsuredIDType);
				mDetail.addContent(mInsuredIDNo);
				mDetail.addContent(mInsuredBirthday);
				mDetail.addContent(mInsuredSex);
				mDetail.addContent(mInsuredNativePlace);
				mDetail.addContent(mInsuredEmail);
				mDetail.addContent(mPayEndYearFlag);
				mDetail.addContent(mPayEndYear);
				mDetail.addContent(mInsuYearFlag);
				mDetail.addContent(mInsuYear);
				mDetail.addContent(mPayIntv);
				mDetail.addContent(mPayMethod);
				mDetail.addContent(mBankAccNo);
				mDetail.addContent(mCurrcy);
				mDetail.addContent(mStatus);
				mDetail.addContent(mFaileInfo);
				mDetail.addContent(mCvalidate);
				mDetail.addContent(mCinvalidate);
				mDetail.addContent(mMult);
				mDetail.addContent(mAmnt);
				mDetail.addContent(mPrem);
				mDetail.addContent(mBankNode);
				
				mDetails.addContent(mDetail);
			}
		}
		Element mTrandate = new Element("Trandate");
		mTrandate.setText(pInXmlDoc.getRootElement().getChild("BaseInfo").getChildText("BankDate"));
		Element mTotal = new Element("Total");
		mTotal.setText(mDetails.getChildren("Detail").size()+"");
		outDocXml.getRootElement().addContent(mTrandate);
		outDocXml.getRootElement().addContent(mTotal);
		outDocXml.getRootElement().addContent(mDetails);
		
		return outDocXml;
	}
	
	private LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into SimpService.insertTransLog()...");
		
		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);
		
		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}
		
		cLogger.info("Out SimpService.insertTransLog()!");
		return mLKTransStatusDB;
	}
}
