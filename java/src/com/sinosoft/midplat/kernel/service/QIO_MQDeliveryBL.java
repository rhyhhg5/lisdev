package com.sinosoft.midplat.kernel.service;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.ExeSQL;

public class QIO_MQDeliveryBL extends ServiceImpl {

	public QIO_MQDeliveryBL(Element thisBusiConf) {
		super(thisBusiConf);
	}

	public Document service(Document pInXmlDoc) {
		cLogger.info("Into QIO_MQDeliveryBL.service()...");
		Document mOutXmlDoc = null;
		Element mTranData = pInXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		LKTransStatusDB mLKTransStatusDB = null;
		GlobalInput mGlobalInput = null;
		
		try {
			mLKTransStatusDB = insertTransLog(pInXmlDoc); //.........
			
			mGlobalInput = YbtSufUtil.getGlobalInput(
					mBaseInfo.getChildText(BankCode),
					mBaseInfo.getChildText(ZoneNo), 
					mBaseInfo.getChildText(BrNo));
			
			// 进行业务处理
			mOutXmlDoc = deal(pInXmlDoc);
			
		} catch (Exception ex) {
			cLogger.error(cThisBusiConf.getChildText(name)+"交易失败！", ex);
			
			if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
				mLKTransStatusDB.setRCode("0");	//空-未返回；1-交易成功，返回；0-交易失败，返回
				String tDesr = ex.getMessage();
				try {
					if (ex.getMessage().getBytes("utf-8").length >= 255) {
						tDesr = tDesr.substring(0, 85);
					}
				} catch (UnsupportedEncodingException uex){
					cLogger.error(uex.getMessage());
				}
				
				mLKTransStatusDB.setDescr(tDesr);
			}
			
			mOutXmlDoc =  YbtSufUtil.getSimpOutXml("0", ex.getMessage());
		}
		if (null != mLKTransStatusDB) {	//插入日志失败时mLKTransStatusDB=null
			if (null != mGlobalInput) {
				mLKTransStatusDB.setManageCom(mGlobalInput.ManageCom);
			}
			mLKTransStatusDB.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
			if (!mLKTransStatusDB.update()) {
				cLogger.error("更新日志信息失败！" + mLKTransStatusDB.mErrors.getFirstError());
			}
		}
		
		cLogger.info("Out QIO_MQDeliveryBL.service()...");
		return mOutXmlDoc;
	}
	
	private Document deal(Document pInXmlDoc) {
		
		Element xTranData = pInXmlDoc.getRootElement();
		String mContNo = xTranData.getChild("LCCont").getChildText("ContNo");
		LCContDB cont = new LCContDB();
		cont.setContNo(mContNo);
		LCContSet mCont = cont.query();
		Element mTranData = new Element("TranData");
		Element mRetData = new Element("RetData");
		Element mFlag = new Element("Flag");
		Element mDesc = new Element("Desc");
		Element mEdorInfo = new Element("EdorInfo");
		if(mCont.size() > 0){
			LCContSchema lc = mCont.get(1);
			
			mFlag.setText("1");
			mDesc.setText("交易成功！");
			
			LCPolDB pol_db = new LCPolDB();
			pol_db.setContNo(mContNo);
			LCPolSet polSet = pol_db.query();
			
			double mSumAmnt = 0;
			String risk_main = "";
			String risk_fu = "";
			for (int i = 1; i <= polSet.size(); i++) {
				LCPolSchema mPol = polSet.get(i);
				mSumAmnt += mPol.getAmnt();
				if("1".equals(mPol.getRiskSeqNo()) || "01".equals(mPol.getRiskSeqNo())){
					risk_main = mPol.getRiskCode();
				}
				if("2".equals(mPol.getRiskSeqNo()) || "02".equals(mPol.getRiskSeqNo())){
					risk_fu = mPol.getRiskCode();
				}
			}
			
			Element nEdorAcceptNo = new Element("EdorAcceptNo");
			nEdorAcceptNo.setText(lc.getProposalContNo()); //批单号...
			
			Element nAttachmentContent = new Element("AttachmentContent");
			Element nAttachment_1 = new Element("Attachment");
			nAttachment_1.addAttribute("id","Attachment_1");
			Element nAttachmentData_1 = new Element("AttachmentData");
			nAttachmentData_1.setText("尊敬的 "+ lc.getInsuredName() +" 先生/女士：");
			nAttachment_1.addContent(nAttachmentData_1);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String now = sdf.format(new Date());
			Element nAttachment_2 = new Element("Attachment");
			nAttachment_2.addAttribute("id","Attachment_2");
			Element nAttachmentData_2 = new Element("AttachmentData");
			nAttachmentData_2.setText("经您于 "+ now.substring(0,4) +"  年 "+ now.substring(4,6) +" 月 "+ now.substring(6,8) +" 日递交的满期给付申请，现作如下批注：");
			nAttachment_2.addContent(nAttachmentData_2);
			
			Element nAttachment_3 = new Element("Attachment");
			nAttachment_3.addAttribute("id","Attachment_3");
			Element nAttachmentData_3 = new Element("AttachmentData");
			if("".equals(risk_fu)){
				nAttachmentData_3.setText("本公司同意您的保单（保单号："+ lc.getContNo() +" ）的 "+ getRiskRep(risk_main) +" 的满期给付，");
			}else {
				nAttachmentData_3.setText("本公司同意您的保单（保单号："+ lc.getContNo() +" ）的 "+ getRiskRep(risk_main) +" 及所 "+ getRiskRep(risk_fu) +" 名称的满期给付，");
			}
			nAttachment_3.addContent(nAttachmentData_3);
			
			Element nAttachment_4 = new Element("Attachment");
			nAttachment_4.addAttribute("id","Attachment_4");
			Element nAttachmentData_4 = new Element("AttachmentData");
			nAttachmentData_4.setText("满期给付金￥ "+ mSumAmnt +" 元，保险责任终止。");
			nAttachment_4.addContent(nAttachmentData_4);
			
			Element nAttachment_5 = new Element("Attachment");
			nAttachment_5.addAttribute("id","Attachment_5");
			Element nAttachmentData_5 = new Element("AttachmentData");
			nAttachmentData_5.setText("此批单自 "+ now.substring(0,4) +"  年 "+ now.substring(4,6) +" 月 "+ now.substring(6,8) +" 日起生效。");
			nAttachment_5.addContent(nAttachmentData_5);
			
			
			nAttachmentContent.addContent(nAttachment_1);
			nAttachmentContent.addContent(nAttachment_2);
			nAttachmentContent.addContent(nAttachment_3);
			nAttachmentContent.addContent(nAttachment_4);
			nAttachmentContent.addContent(nAttachment_5);
			mEdorInfo.addContent(nEdorAcceptNo);
			mEdorInfo.addContent(nAttachmentContent);
		}else {
			mFlag.setText("0");
			mDesc.setText("未查到对应保单信息！");
		}
		
		Element mBaseInfo = new Element("BaseInfo");
		mBaseInfo = (Element) pInXmlDoc.getRootElement().getChild("BaseInfo").clone();
		
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		mTranData.addContent(mRetData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mEdorInfo);
		
		return new Document(mTranData);
	}

	public LKTransStatusDB insertTransLog(Document pXmlDoc) throws MidplatException {
		cLogger.info("Into QIO_MQDeliveryBL.insertTransLog()...");

		Element mTranData = pXmlDoc.getRootElement();
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		Element mLCCont = mTranData.getChild(LCCont);

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");

		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setBankCode(mBaseInfo.getChildText(BankCode));
		mLKTransStatusDB.setBankBranch(mBaseInfo.getChildText(ZoneNo));
		mLKTransStatusDB.setBankNode(mBaseInfo.getChildText(BrNo));
		mLKTransStatusDB.setBankOperator(mBaseInfo.getChildText(TellerNo));
		mLKTransStatusDB.setTransNo(mBaseInfo.getChildText(TransrNo));
		mLKTransStatusDB.setFuncFlag(mBaseInfo.getChildText(FunctionFlag));
		mLKTransStatusDB.setTransDate(mCurrentDate);
		mLKTransStatusDB.setTransTime(mCurrentTime);
		mLKTransStatusDB.setStatus("1");
		mLKTransStatusDB.setPolNo(mLCCont.getChildText(ContNo));
		mLKTransStatusDB.setTransAmnt(mLCCont.getChildText("Pay"));
		mLKTransStatusDB.setServiceStartTime(mLCCont.getChildText("ReceivDate")); //应收日期
//		mLKTransStatusDB.setProposalNo(mLCCont.getChildText(ProposalContNo));
//		mLKTransStatusDB.setPrtNo(mLCCont.getChildText(PrtNo));
		mLKTransStatusDB.setTransStatus("0");	//业务状态(标识本业务所包含的一系列子业务是否全部完成，包括BL和COC)：0-未结束；1-已结束
		mLKTransStatusDB.setMakeDate(mCurrentDate);
		mLKTransStatusDB.setMakeTime(mCurrentTime);
		mLKTransStatusDB.setModifyDate(mCurrentDate);
		mLKTransStatusDB.setModifyTime(mCurrentTime);

		if (!mLKTransStatusDB.insert()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("插入日志失败！");
		}

		cLogger.info("Out QIO_MQDeliveryBL.insertTransLog()!");
		return mLKTransStatusDB;
	}
	
	public String getRiskRep(String riskCode){
		
		String sql_risk = "select riskname from lmrisk where riskcode = '"+ riskCode +"' with ur";
		return new ExeSQL().getOneValue(sql_risk);
		
	}
	
}
