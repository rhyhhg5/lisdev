/**
 * 信保通单证类型管理。
 */

package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKCertifyMappingDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyManageUI {
	private final static Logger cLogger = Logger.getLogger(CertifyManageUI.class);
	
	private final GlobalInput cGlobalInput;
	
	private final TransferData cTransferData;
	
	private final String cOperType;
	
	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");
	
	public CertifyManageUI(TransferData pTransferData, GlobalInput pGlobalInput, String pOperType) {
		cTransferData = pTransferData;
		cGlobalInput = pGlobalInput;
		cOperType = pOperType;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into CertifyManageUI.deal()...");
		
		if (cOperType.equals("INSERT||MAIN")) {
			insertNode();
		} else if (cOperType.equals("UPDATE||MAIN")) {
			updateNode();
		} else if (cOperType.equals("DEL||MAIN")) {
			deleteNode();
		}
		
      cLogger.info("Out CeretifyManageUI.deal()!");
	}
	
	private void insertNode() throws MidplatException {
		cLogger.info("Into CertifyManageUI.insertNode()...");
		
		LKCertifyMappingDB mLKCertifyMappingDB = new LKCertifyMappingDB();
		mLKCertifyMappingDB.setBankCode((String)cTransferData.getValueByName("BankCode"));
		mLKCertifyMappingDB.setRiskWrapCode((String)cTransferData.getValueByName("RiskWrapCode"));
		mLKCertifyMappingDB.setManageCom((String)cTransferData.getValueByName("ManageCom"));
		mLKCertifyMappingDB.setCertifyCode((String)cTransferData.getValueByName("CertifyCode"));
		mLKCertifyMappingDB.setCertifyClass((String)cTransferData.getValueByName("CertifyClass"));
//		mLKCodeMappingDB.setOperator((String)cTransferData.getValueByName("Operator"));
		mLKCertifyMappingDB.setOperator(cGlobalInput.Operator);
		mLKCertifyMappingDB.setRemark1((String)cTransferData.getValueByName("Remark1"));
		mLKCertifyMappingDB.setRemark2((String)cTransferData.getValueByName("Remark2"));
		
		mLKCertifyMappingDB.setMakeDate(cCurDate);
		mLKCertifyMappingDB.setMakeTime(cCurTime);
		mLKCertifyMappingDB.setModifyDate(cCurDate);
		mLKCertifyMappingDB.setModifyTime(cCurTime);
		if (!mLKCertifyMappingDB.insert()) {
			cLogger.error(mLKCertifyMappingDB.mErrors.getFirstError());
			throw new MidplatException("插入网点信息失败！");
		}
		
		cLogger.info("Out CertifyManageUI.insertNode()!");
	}
	
	private void updateNode() throws MidplatException {
		cLogger.info("Into CertifyManageUI.updateNode()...");

		String BankCode = (String)cTransferData.getValueByName("BankCode");
		String RiskWrapCode = (String)cTransferData.getValueByName("RiskWrapCode");
		String ManageCom=(String)cTransferData.getValueByName("ManageCom");
		String CertifyCode = (String)cTransferData.getValueByName("CertifyCode");
		String CertifyClass = (String)cTransferData.getValueByName("CertifyClass");
//		String Operator=(String)cTransferData.getValueByName("Operator");
		String Operator = cGlobalInput.Operator;

		String Remark1=(String)cTransferData.getValueByName("Remark1");
		String Remark2=(String)cTransferData.getValueByName("Remark2");
	

		String hideBankCode=(String)cTransferData.getValueByName("hideBankCode");
		String hideRiskWrapCode=(String)cTransferData.getValueByName("hideRiskWrapCode");
		String hideManageCom=(String)cTransferData.getValueByName("hideManageCom");

		MMap mSubmitMMap = new MMap();
		String mSQLStr1 = "update LKCertifyMapping set BankCode='"+ BankCode.trim()
				+ "', ManageCom='" + ManageCom.trim()
				+ "',RiskWrapCode='" + RiskWrapCode.trim() 
				+ "',CertifyCode='"+ CertifyCode.trim()
				+ "' ,CertifyClass='"+ CertifyClass.trim()
				+ "' ,Operator='" + Operator.trim()
				+ "',Remark1='" + Remark1.trim() 
				+ "',Remark2='"+ Remark2.trim()
				+ "',ModifyDate='" + cCurDate
				+ "',ModifyTime='" + cCurTime 
				+ "' where BankCode='"+ hideBankCode.trim()
				+ "' and  RiskWrapCode='"+ hideRiskWrapCode.trim()
				+ "' and ManageCom='"+ hideManageCom.trim() + "'";
				
		mSubmitMMap.put(mSQLStr1, "UPDATE");


		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("更新单证类型信息失败！");
		}

		cLogger.info("Out CerfiyManageUI.updateNode()!");
	}
	
	private void deleteNode() throws MidplatException {
		cLogger.info("Into CertifyManageUI.deleteNode()...");
		
		String mBankCode=(String)cTransferData.getValueByName("hideBankCode");
		String mManageCom=(String)cTransferData.getValueByName("hideManageCom");
		String mRiskWrapCode=(String)cTransferData.getValueByName("hideRiskWrapCode");
		cLogger.info(mBankCode);
		cLogger.info(mManageCom);
		cLogger.info(mRiskWrapCode);
      String mSQLStr = "DELETE from LKCertifyMapping where BankCode='"
				+ mBankCode.trim() + "' and  ManageCom='" + mManageCom.trim()
				+ "' and RiskWrapCode='" + mRiskWrapCode.trim() + "'";
      cLogger.info(mSQLStr);
      MMap mSubmitMMap = new MMap();
      mSubmitMMap.put(mSQLStr,"DELETE");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("删除单证类型定义信息失败！");
		}
      
		cLogger.info("Out CertifyManageUI.deleteNode()!");
	}
}
