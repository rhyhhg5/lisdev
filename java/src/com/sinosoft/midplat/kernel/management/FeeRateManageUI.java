/**
 * @author yinjj
 */
package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKFeeRateDetailsDB;
import com.sinosoft.lis.db.LKFeeRateProtocolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FeeRateManageUI {
	private final static Logger cLogger = Logger.getLogger(FeeRateManageUI.class);
	
	private final GlobalInput cGlobalInput;
	
	private final TransferData cTransferData;
	
	private final String cOperType;
	private final String cType;
	private String mSQL = null;
	private static ExeSQL mExeSQL = new ExeSQL();
	private String mProtocolNo = null;
	private String mBankCode = null;
	private String mManageCom = null;
	private String mRiskWrapCode = null;
	private String mCommonFeeRateFlag = null;
	private String mAgentCom = null;
	private String mAvailableState = null;
	private String mFeeRate = null; 
	private String mFeeRateYear = null;
	private String mFeeRateType = null;
	private String mFeeRateYearFlag = null;
	
	private String mHideFeeRate = null;
	private String mHideFeeRateType = null;
	private String mHideFeeRateYear = null;
		
	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");
	
	public FeeRateManageUI(TransferData pTransferData, GlobalInput pGlobalInput, String pOperType,String pType) {
		cTransferData = pTransferData;
		cGlobalInput = pGlobalInput;
		cOperType = pOperType;
		cType = pType; //新加入的标志 ,标志3类状态
		//组织数据
		mProtocolNo = (String)cTransferData.getValueByName("protocolno");
		mBankCode = (String)cTransferData.getValueByName("bankcode");
		mManageCom = (String)cTransferData.getValueByName("managecom");
		mRiskWrapCode = (String)cTransferData.getValueByName("riskwrapcode");
		mCommonFeeRateFlag = (String)cTransferData.getValueByName("commonfeerateflag");
		mAgentCom = (String)cTransferData.getValueByName("agentcom");
		mAvailableState = (String)cTransferData.getValueByName("availablestate");
		mFeeRate = (String)cTransferData.getValueByName("feerate");
		mFeeRateYear = (String)cTransferData.getValueByName("feerateyear");
		mFeeRateType = (String)cTransferData.getValueByName("feeratetype");
		mFeeRateYearFlag = (String)cTransferData.getValueByName("feerateyearflag");
		
		mHideFeeRate = (String)cTransferData.getValueByName("hidefeerate");
		mHideFeeRateType = (String)cTransferData.getValueByName("hidefeeratetype");
		mHideFeeRateYear = (String)cTransferData.getValueByName("hidefeerateyear");
	}
	
	public void deal() throws Exception {
		cLogger.info("Into FeeRateManageUI.deal()...");
		
		if (cOperType.equals("INSERT||MAIN")) {
			insertNode();
		} else if (cOperType.equals("UPDATE||MAIN")) {
			updateNode();
		} else if (cOperType.equals("DEL||MAIN")) {
			deleteNode();
		}
		
      cLogger.info("Out FeeRateManageUI.deal()!");
	}
	
	private void insertNode() throws MidplatException {
		cLogger.info("Into FeeRateManageUI.insertNode()...");
	
		if(cType.equals("TypeInvalid")){
			invalidInsert();
		}else if (cType.equals("TypeCommon")){
			commonInsert();
		}else if(cType.equals("TypePersonnal")){
			personnalInsert();
		}
		cLogger.info("Out FeeRateManageUI.insertNode()!");
	}
	
	
	/**
	 * 功能:对中介机构费率数据进行插入操作
	 * @throws MidplatException
	 */
	private void personnalInsert() throws MidplatException {
		cLogger.info("Into FeeRateManageUI InsertNode -- TypePersonnal");

		//首先判断协议表中是否已经生成了协议号,根据情况再进行插入操作.
		mSQL = "select ProtocolNo from lkfeerateprotocol where "
			+"bankcode = '"+mBankCode+"' and "
			+"riskwrapcode = '"+mRiskWrapCode +"' and "
			+"managecom = '"+mManageCom+"' and "
			+"commonfeerateflag = '"+mCommonFeeRateFlag+"' and "
			+"AvailableState = '"+mAvailableState+"' and "
			+"agentcom = '"+mAgentCom +"' with ur";
		//判断是否有协议号
		SSRS tSSRS = mExeSQL.execSQL(mSQL);
		if(tSSRS.getMaxRow() > 0){
			mProtocolNo = tSSRS.GetText(1, 1);
			LKFeeRateDetailsDB mLKFeeRateDetailsDB = new LKFeeRateDetailsDB();
			mLKFeeRateDetailsDB.setProtocolNo(mProtocolNo);
			mLKFeeRateDetailsDB.setFeeRate(mFeeRate);
			mLKFeeRateDetailsDB.setFeeRateType(mFeeRateType);
			mLKFeeRateDetailsDB.setFeeRateYear(mFeeRateYear);
			mLKFeeRateDetailsDB.setFeeRateYearFlag(mFeeRateYearFlag);
			mLKFeeRateDetailsDB.setMakeDate(cCurDate);
			mLKFeeRateDetailsDB.setMakeTime(cCurTime);
			mLKFeeRateDetailsDB.setModifyDate(cCurDate);
			mLKFeeRateDetailsDB.setModifyTime(cCurTime);
			mLKFeeRateDetailsDB.setOperator(cGlobalInput.Operator);
			
			if(!mLKFeeRateDetailsDB.insert()){
				cLogger.error(mLKFeeRateDetailsDB.mErrors.getFirstError());
				throw new MidplatException("插入费率明细信息失败！");
			}
			cLogger.info("Out FeeRateManageUI InsertNode -- TypePersonnal");
		}else{
			//此种情况下需要先插入协议表,再插入明细表
			mProtocolNo = CreateMaxNo(mBankCode,mManageCom);//生成协议号
			
			//生成LKFeeRateProtocolDB 的插入代码;
			LKFeeRateProtocolDB mLKFeeRateProtocolDB = new LKFeeRateProtocolDB();
			mLKFeeRateProtocolDB.setBankcode(mBankCode);
			mLKFeeRateProtocolDB.setManageCom(mManageCom);
			mLKFeeRateProtocolDB.setRiskWrapCode(mRiskWrapCode);
			mLKFeeRateProtocolDB.setAvailableState(mAvailableState);
			mLKFeeRateProtocolDB.setCommonFeeRateFlag(mCommonFeeRateFlag);
			mLKFeeRateProtocolDB.setAgentCom(mAgentCom);
			mLKFeeRateProtocolDB.setProtocolNo(mProtocolNo);
			mLKFeeRateProtocolDB.setMakeDate(cCurDate);
			mLKFeeRateProtocolDB.setMakeTime(cCurTime);
			mLKFeeRateProtocolDB.setModifyDate(cCurDate);
			mLKFeeRateProtocolDB.setModifyTime(cCurTime);
			mLKFeeRateProtocolDB.setOperator(cGlobalInput.Operator);
			mLKFeeRateProtocolDB.setRemark1("1");//TypePersonnal 查询优先级最高
			
			if(!mLKFeeRateProtocolDB.insert()){
				cLogger.error(mLKFeeRateProtocolDB.mErrors.getFirstError());
				throw new MidplatException("插入费率协议信息失败！");
			}
			
			//插入明细表
			LKFeeRateDetailsDB mLKFeeRateDetailsDB = new LKFeeRateDetailsDB();
			mLKFeeRateDetailsDB.setProtocolNo(mProtocolNo);
			mLKFeeRateDetailsDB.setFeeRate(mFeeRate);
			mLKFeeRateDetailsDB.setFeeRateType(mFeeRateType);
			mLKFeeRateDetailsDB.setFeeRateYear(mFeeRateYear);
			mLKFeeRateDetailsDB.setFeeRateYearFlag(mFeeRateYearFlag);
			mLKFeeRateDetailsDB.setMakeDate(cCurDate);
			mLKFeeRateDetailsDB.setMakeTime(cCurTime);
			mLKFeeRateDetailsDB.setModifyDate(cCurDate);
			mLKFeeRateDetailsDB.setModifyTime(cCurTime);
			mLKFeeRateDetailsDB.setOperator(cGlobalInput.Operator);
			
			if(!mLKFeeRateDetailsDB.insert()){
				cLogger.error(mLKFeeRateDetailsDB.mErrors.getFirstError());
				mLKFeeRateProtocolDB.delete();
				throw new MidplatException("插入费率明细信息失败！");
			}
			
			//如果2个表中有一个插入失败,会存在数据不一致性,要进行数据回滚的操作.
			cLogger.info("Out FeeRateManageUI InsertNode -- TypePersonnal");
		}
	}
	
	
	/**
	 * 功能:对分公司默认(启用)费率数据进行插入操作
	 * @throws MidplatException
	 */
	private void commonInsert() throws MidplatException {
		cLogger.info("Into FeeRateManageUI InsertNode -- TypeCommon");
		//首先判断协议表中是否已经生成了协议号,根据情况再进行插入操作.
		mSQL = "select ProtocolNo from lkfeerateprotocol where "
			+"bankcode = '"+mBankCode+"' and "
			+"riskwrapcode = '"+mRiskWrapCode +"' and "
			+"managecom = '"+mManageCom+"' and "
			+"commonfeerateflag = '"+mCommonFeeRateFlag+"' and "
			+"AvailableState = '"+mAvailableState+"' with ur";
		//判断是否有协议号
		SSRS tSSRS = mExeSQL.execSQL(mSQL);
		if(tSSRS.getMaxRow() > 0){
			mProtocolNo = tSSRS.GetText(1, 1);
			LKFeeRateDetailsDB mLKFeeRateDetailsDB = new LKFeeRateDetailsDB();
			mLKFeeRateDetailsDB.setProtocolNo(mProtocolNo);
			mLKFeeRateDetailsDB.setFeeRate(mFeeRate);
			mLKFeeRateDetailsDB.setFeeRateType(mFeeRateType);
			mLKFeeRateDetailsDB.setFeeRateYear(mFeeRateYear);
			mLKFeeRateDetailsDB.setFeeRateYearFlag(mFeeRateYearFlag);
			mLKFeeRateDetailsDB.setMakeDate(cCurDate);
			mLKFeeRateDetailsDB.setMakeTime(cCurTime);
			mLKFeeRateDetailsDB.setModifyDate(cCurDate);
			mLKFeeRateDetailsDB.setModifyTime(cCurTime);
			mLKFeeRateDetailsDB.setOperator(cGlobalInput.Operator);
			
			if(!mLKFeeRateDetailsDB.insert()){
				cLogger.error(mLKFeeRateDetailsDB.mErrors.getFirstError());
				throw new MidplatException("插入费率明细信息失败！");
			}
			cLogger.info("Out FeeRateManageUI InsertNode -- TypeCommon");
		}else{
			//此种情况下需要先插入费率表,再插入明细表
			mProtocolNo = CreateMaxNo(mBankCode,mManageCom);//生成协议号
			
			//生成LKFeeRateProtocolDB 的插入代码;
			LKFeeRateProtocolDB mLKFeeRateProtocolDB = new LKFeeRateProtocolDB();
			mLKFeeRateProtocolDB.setBankcode(mBankCode);
			mLKFeeRateProtocolDB.setManageCom(mManageCom);
			mLKFeeRateProtocolDB.setRiskWrapCode(mRiskWrapCode);
			mLKFeeRateProtocolDB.setAvailableState(mAvailableState);
			mLKFeeRateProtocolDB.setCommonFeeRateFlag(mCommonFeeRateFlag);
			mLKFeeRateProtocolDB.setAgentCom("PC");
			mLKFeeRateProtocolDB.setProtocolNo(mProtocolNo);
			mLKFeeRateProtocolDB.setMakeDate(cCurDate);
			mLKFeeRateProtocolDB.setMakeTime(cCurTime);
			mLKFeeRateProtocolDB.setModifyDate(cCurDate);
			mLKFeeRateProtocolDB.setModifyTime(cCurTime);
			mLKFeeRateProtocolDB.setOperator(cGlobalInput.Operator);
			mLKFeeRateProtocolDB.setRemark1("2");//TypeCommon 查询优先级居中
			
			if(!mLKFeeRateProtocolDB.insert()){
				cLogger.error(mLKFeeRateProtocolDB.mErrors.getFirstError());
				throw new MidplatException("插入费率协议信息失败！");
			}
			
			//插入协议表
			LKFeeRateDetailsDB mLKFeeRateDetailsDB = new LKFeeRateDetailsDB();
			mLKFeeRateDetailsDB.setProtocolNo(mProtocolNo);
			mLKFeeRateDetailsDB.setFeeRate(mFeeRate);
			mLKFeeRateDetailsDB.setFeeRateType(mFeeRateType);
			mLKFeeRateDetailsDB.setFeeRateYear(mFeeRateYear);
			mLKFeeRateDetailsDB.setFeeRateYearFlag(mFeeRateYearFlag);
			mLKFeeRateDetailsDB.setMakeDate(cCurDate);
			mLKFeeRateDetailsDB.setMakeTime(cCurTime);
			mLKFeeRateDetailsDB.setModifyDate(cCurDate);
			mLKFeeRateDetailsDB.setModifyTime(cCurTime);
			mLKFeeRateDetailsDB.setOperator(cGlobalInput.Operator);
			
			if(!mLKFeeRateDetailsDB.insert()){
				cLogger.error(mLKFeeRateDetailsDB.mErrors.getFirstError());
				mLKFeeRateProtocolDB.delete();//确保数据的一致性,回滚操作.
				throw new MidplatException("插入费率明细信息失败！");
			}
			
			//如果2个表中有一个插入失败,会存在数据不一致性,要进行数据回滚的操作.
			cLogger.info("Out FeeRateManageUI InsertNode -- TypeCommon");
		}
	}
	
	/**
	 * 功能:对分公司默认(禁用)费率进行插入操作
	 * @throws MidplatException
	 */
	private void invalidInsert() throws MidplatException {
		cLogger.info("Into FeeRateManageUI InsertNode -- TypeInvalid");
		mProtocolNo = CreateMaxNo(mBankCode,mManageCom);//生成协议号
		LKFeeRateProtocolDB mLKFeeRateProtocolDB = new LKFeeRateProtocolDB();
		mLKFeeRateProtocolDB.setProtocolNo(mProtocolNo);
		mLKFeeRateProtocolDB.setBankcode(mBankCode);
		mLKFeeRateProtocolDB.setManageCom(mManageCom);
		mLKFeeRateProtocolDB.setRiskWrapCode(mRiskWrapCode);
		mLKFeeRateProtocolDB.setAvailableState(mAvailableState); //0:阻断状态
		mLKFeeRateProtocolDB.setCommonFeeRateFlag(mCommonFeeRateFlag); //1:公共标志
		mLKFeeRateProtocolDB.setAgentCom("PC"); //公共标志下不设具体中介机构,用PC做默认值
		mLKFeeRateProtocolDB.setRemark1("3");//TypeInvalid 查询时优先级最低
		mLKFeeRateProtocolDB.setMakeDate(cCurDate);
		mLKFeeRateProtocolDB.setMakeTime(cCurTime);
		mLKFeeRateProtocolDB.setModifyDate(cCurDate);
		mLKFeeRateProtocolDB.setModifyTime(cCurTime);
		mLKFeeRateProtocolDB.setOperator(cGlobalInput.Operator);
		
		if(!mLKFeeRateProtocolDB.insert()){
			cLogger.error(mLKFeeRateProtocolDB.mErrors.getFirstError());
			throw new MidplatException("插入费率协议信息失败！");
		}
		cLogger.info("Out FeeRateManageUI InsertNode -- TypeInvalid");
	}
	
	private void updateNode() throws MidplatException {
		cLogger.info("Into FeeRateManageUI.updateNode()...");
		String mSQLStr1 = null;
		if(cType.equals("TypeInvalid")){
			throw new MidplatException("此种费率不允许进行修改,若您确实需要修改此记录,请先进行删除操作,再进行重新添加!");
		}else if(cType.equals("TypeCommon")||cType.equals("TypePersonnal")){
			//只允许修改feerate,feeratetype,feerateyear
			mSQLStr1 = "update LKFeeRateDetails set Feerate= "+mFeeRate.trim()+" , FeerateType='"+mFeeRateType.trim()+"', FeerateYear='"+mFeeRateYear.trim()+"',FeeRateYearFlag = '"+mFeeRateYearFlag+"',ModifyDate='"+cCurDate+"',ModifyTime='"+cCurTime+"' ,Operator='"+cGlobalInput.Operator+"' where ProtocolNo='"+mProtocolNo.trim()+"' and  FeeRate= "+mHideFeeRate.trim()+"  and FeeRateType='"+mHideFeeRateType.trim()+"' and FeeRateYear = '"+mHideFeeRateYear.trim()+"'" ;
			cLogger.debug("mSQLStr1 ="+mSQLStr1);
		}
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mSQLStr1, "UPDATE");

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("更新费率明细信息失败！");
		}
		cLogger.info("Out FeeRateManageUI.updateNode()!");
	}
	
	private void deleteNode() throws MidplatException {
		cLogger.info("Into FeeRateManageUI.deleteNode()...");
		String mSQLStr = null;
		String mSQLStr1 = null;
		if(cType.equals("TypeInvalid")){
			mSQLStr = "DELETE from LKFeeRateProtocol where ProtocolNo='"+mProtocolNo.trim()+"'" ;
		}else if(cType.equals("TypeCommon")||cType.equals("TypePersonnal")){
			mSQL = "Select * from LKFeeRateDetails where protocolNo = '"+mProtocolNo.trim()+"' with ur";
			cLogger.debug(mSQL);
			SSRS tSSRS = mExeSQL.execSQL(mSQL);
			if(tSSRS.getMaxRow() > 1){
				//只删除明细表中的数据.
				mSQLStr = "DELETE from LKFeeRateDetails where ProtocolNo = '"+mProtocolNo.trim()+"' and FeeRate = "+mFeeRate.trim()+" and FeeRateType = '"+mFeeRateType.trim()+"' and FeeRateYear = '"+mFeeRateYear.trim()+"'";
				
			}else{
				mSQLStr = "DELETE from lKFeeRateDetails where protocolNo ='"+mProtocolNo.trim()+"'";
				mSQLStr1 = "DELETE from LKFeeRateProtocol where ProtocolNo = '"+mProtocolNo.trim()+"'";
			}
		}else{
			throw new MidplatException("不支持的类型!");
		}
		
      cLogger.debug("mSQLStr = (DELETE)"+mSQLStr);
      cLogger.debug("mSQLStr1 = (DELETE)"+mSQLStr1);
      MMap mSubmitMMap = new MMap();
      mSubmitMMap.put(mSQLStr,"DELETE");
      if(mSQLStr1 !=null){
    	  mSubmitMMap.put(mSQLStr1, "DELETE");
      }
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("删除费率信息失败！");
		}
      
		cLogger.info("Out FeeRateManageUI.deleteNode()!");
	}
	
    /**
     * 功能：产生指定长度的流水号
     * @param pBankCode 银行编码
     * @param pManageCom 管理机构
     * @return String 返回产生的流水号码
     */
    public static String CreateMaxNo(String pBankCode, String pManageCom)
			throws MidplatException {
    	cLogger.info("Into FeeRateManageUI.CreateMaxNo()!");
		String mManageCom = pManageCom.substring(0, 4);
		String mBankCode = pBankCode;
		String mNo = null;
		String mSQL = "select max(protocolNo) From LKFeeRateProtocol where "
				+ "managecom like '" + mManageCom + "%'"
				+ " with ur";//使用max函数时,如果没有对应的记录,也会有一条空记录
		SSRS tSSRS = mExeSQL.execSQL(mSQL);
		cLogger.debug("tSSRS.getMaxRow() = "+tSSRS.getMaxRow());
		mNo = tSSRS.GetText(1, 1);
		if("".equals(mNo)|| null == mNo){//该机构下没有任何记录.选择生成一个
			mNo = mManageCom + "00001";
		}else{
			mNo = String.valueOf(1 + Integer.parseInt(mNo));//协议号是String型,+ 1 会在后面追加 "1"
		}
		cLogger.info("生成的协议号为:"+mNo);
		cLogger.info("Out FeeRateManageUI.CreateMaxNo()!");
		return mNo;

	}
}
