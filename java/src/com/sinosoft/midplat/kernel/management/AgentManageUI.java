/**
 * 网点管理。
 */

package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKCodeMapping1DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AgentManageUI {
	private final static Logger cLogger = Logger.getLogger(AgentManageUI.class);

	private final GlobalInput cGlobalInput;

	private final TransferData cTransferData;

	private final String cOperType;

	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");

	public AgentManageUI(TransferData pTransferData, GlobalInput pGlobalInput,
			String pOperType) {
		cTransferData = pTransferData;
		cGlobalInput = pGlobalInput;
		cOperType = pOperType;
	}

	public void deal() throws Exception {
		cLogger.info("Into AgentManageUI.deal()...");

		if (cOperType.equals("INSERT||MAIN")) {
			insertNode();
		} else if (cOperType.equals("UPDATE||MAIN")) {
			updateNode();
		} else if (cOperType.equals("DEL||MAIN")) {
			deleteNode();
		}

		cLogger.info("Out AgentManageUI.deal()!");
	}

	private void insertNode() throws MidplatException {
		cLogger.info("Into AgentManageUI.insertNode()...");

		LKCodeMapping1DB mLKCodeMapping1DB = new LKCodeMapping1DB();
		mLKCodeMapping1DB.setBankCode((String) cTransferData
				.getValueByName("BankCode"));
		mLKCodeMapping1DB.setZoneNo((String) cTransferData
				.getValueByName("ZoneNo"));
		mLKCodeMapping1DB.setBankNode((String) cTransferData
				.getValueByName("BankNode"));
		mLKCodeMapping1DB.setAgentCom((String) cTransferData
				.getValueByName("UpAgentCom"));
		mLKCodeMapping1DB.setComCode((String) cTransferData
				.getValueByName("ComCode"));
		mLKCodeMapping1DB.setManageCom((String) cTransferData
				.getValueByName("ManageCom"));
		mLKCodeMapping1DB.setRemark((String) cTransferData
				.getValueByName("Remark"));
		mLKCodeMapping1DB.setOperator(cGlobalInput.Operator);
		mLKCodeMapping1DB.setRemark1((String) cTransferData
				.getValueByName("Remark1"));
		mLKCodeMapping1DB.setRemark5((String) cTransferData
				.getValueByName("Remark5"));
		mLKCodeMapping1DB.setRemark3((String) cTransferData
				.getValueByName("Remark3"));
		mLKCodeMapping1DB.setFTPAddress((String) cTransferData
				.getValueByName("FTPAddress"));
		mLKCodeMapping1DB.setFTPDir((String) cTransferData
				.getValueByName("FTPDir"));
		mLKCodeMapping1DB.setFTPUser((String) cTransferData
				.getValueByName("FTPUser"));
		mLKCodeMapping1DB.setFTPPassWord((String) cTransferData
				.getValueByName("FTPPassWord"));
		mLKCodeMapping1DB.setMakeDate(cCurDate);
		mLKCodeMapping1DB.setMakeTime(cCurTime);
		mLKCodeMapping1DB.setModifyDate(cCurDate);
		mLKCodeMapping1DB.setModifyTime(cCurTime);
		if (!mLKCodeMapping1DB.insert()) {
			cLogger.error(mLKCodeMapping1DB.mErrors.getFirstError());
			throw new MidplatException("插入网点信息失败！");
		}

		cLogger.info("Out AgentManageUI.insertNode()!");
	}

	private void updateNode() throws MidplatException {
		cLogger.info("Into AgentManageUI.updateNode()...");

		String BankCode = (String) cTransferData.getValueByName("BankCode");
		String ZoneNo = (String) cTransferData.getValueByName("ZoneNo");
		String BankNode = (String) cTransferData.getValueByName("BankNode");
		String UpAgentCom = (String) cTransferData.getValueByName("UpAgentCom");
		String ComCode = (String) cTransferData.getValueByName("ComCode");
		String ManageCom = (String) cTransferData.getValueByName("ManageCom");
		String Remark = (String) cTransferData.getValueByName("Remark");
		String Operator = cGlobalInput.Operator;
		String Remark5 = (String) cTransferData.getValueByName("Remark5");
		String Remark1 = (String) cTransferData.getValueByName("Remark1");
		String Remark3 = (String) cTransferData.getValueByName("Remark3");
		String FTPAddress = (String) cTransferData.getValueByName("FTPAddress");
		String FTPDir = (String) cTransferData.getValueByName("FTPDir");
		String FTPUser = (String) cTransferData.getValueByName("FTPUser");
		String FTPPassWord = (String) cTransferData
				.getValueByName("FTPPassWord");

		String hideBankCode = (String) cTransferData
				.getValueByName("hideBankCode");
		String hideZoneNo = (String) cTransferData.getValueByName("hideZoneNo");
		String hideBankNode = (String) cTransferData
				.getValueByName("hideBankNode");

		MMap mSubmitMMap = new MMap();
		String mSQLStr1 = "update LKCodeMapping1 set BankCode='"
				+ BankCode.trim() + "', ZoneNo='" + ZoneNo.trim()
				+ "', BankNode='" + BankNode.trim() + "',AgentCom='"
				+ UpAgentCom.trim() + "',ComCode='" + ComCode.trim()
				+ "', ManageCom='" + ManageCom.trim() + "',Remark='"
				+ Remark.trim() + "',Remark5='" + Remark5.trim()
				+ "' ,Operator='" + Operator.trim() + "',Remark1='"
				+ Remark1.trim() + "',Remark3='" + Remark3.trim()
				+ "',ModifyDate='" + cCurDate + "',ModifyTime='" + cCurTime
				+ "',FTPAddress='" + FTPAddress.trim() + "',FTPDir='"
				+ FTPDir.trim() + "',FTPUser='" + FTPUser.trim()
				+ "',FTPPassWord='" + FTPPassWord.trim() + "' where BankCode='"
				+ hideBankCode.trim() + "' and  ZoneNo='" + hideZoneNo.trim()
				+ "' and BankNode='" + hideBankNode.trim() + "'";
		mSubmitMMap.put(mSQLStr1, "UPDATE");
		if (Remark5.equals("1")) {
			String tSQLStr2 = "update LKCodeMapping1 set Remark1='"
					+ BankNode.trim() + "', Remark3='" + ZoneNo.trim()
					+ "' where remark5 ='0' and managecom like '" + ManageCom
					+ "%' and trim(BankCode) ='" + BankCode.trim() + "'";
			mSubmitMMap.put(tSQLStr2, "UPDATE");
		}

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("更新网点失败！");
		}

		cLogger.info("Out AgentManageUI.updateNode()!");
	}

	private void deleteNode() throws MidplatException {
		cLogger.info("Into AgentManageUI.deleteNode()...");

		String mBankCode = (String) cTransferData
				.getValueByName("hideBankCode");
		String mZoneNo = (String) cTransferData.getValueByName("hideZoneNo");
		String mBankNode = (String) cTransferData
				.getValueByName("hideBankNode");

		String mSQLStr = "DELETE from LKCodeMapping1 where BankCode='"
				+ mBankCode.trim() + "' and  ZoneNo='" + mZoneNo.trim()
				+ "' and BankNode='" + mBankNode.trim() + "'";

		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mSQLStr, "DELETE");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("删除网点失败！");
		}

		cLogger.info("Out AgentManageUI.deleteNode()!");
	}
}
