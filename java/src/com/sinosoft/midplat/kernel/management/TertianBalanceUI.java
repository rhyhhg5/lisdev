package com.sinosoft.midplat.kernel.management;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.DailyBalance;

public class TertianBalanceUI implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(TertianBalanceUI.class);
	
	private final GlobalInput cGlobalInput;
	
	private final String cBankCode;
	private final String cZoneNo;
	private final String cBankNode;
	private final String cTransDate;

	public TertianBalanceUI(HashMap pHashMap, GlobalInput pGlobalInput) throws MidplatException {
		String mBankCode = (String) pHashMap.get("BankCode");
		if ((null==mBankCode) || mBankCode.trim().equals("")) {
			throw new MidplatException("银行代码为空！");
		}
		
		String mZoneNo = (String) pHashMap.get("ZoneNo");
		if ((null==mZoneNo) || mZoneNo.trim().equals("")) {
			throw new MidplatException("对账地区代码为空！");
		}
		
		String mBankNode = (String) pHashMap.get("BankNode");
		if ((null==mBankNode) || mBankNode.trim().equals("")) {
			throw new MidplatException("对账网点代码为空！");
		}
		
		String mTransDate = (String) pHashMap.get("TransDate");
		if ((null==mTransDate) || mTransDate.trim().equals("")) {
			throw new MidplatException("交易日期为空！");
		}
		
		cBankCode = mBankCode;
		cZoneNo = mZoneNo;
		cBankNode = mBankNode;
		cTransDate = DateUtil.formatTrans(mTransDate, "yyyy-MM-dd", "yyyyMMdd");
		cGlobalInput = pGlobalInput;
	}

	public void deal() throws Exception {
		cLogger.info("Into TertianBalanceUI.deal()...");
		
		Element mBaseInfo = getBaseInfo();
		
		Element mChkDetails = null;
		if (cBankCode.equals("01")) {	//工行
			mChkDetails = getIcbcDetails();
			mBaseInfo.getChild(TransrNo).setText("02001" + cTransDate + "01");
		} else if (cBankCode.equals("10")) {	//交行
			mChkDetails = getBcmDetails();
			mBaseInfo.getChild(TransrNo).setText("RBJK_" + cZoneNo + "_" + cTransDate);
		} else {
			throw new MidplatException("银行代码有误！BankCode=" + cBankCode);
		}
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mChkDetails);
		
		Document mInXmlDoc = new Document(mTranData);
		JdomUtil.print(mInXmlDoc);
		
		DailyBalance mDailyBalance = new DailyBalance(null);
		Document mOutXmlDoc = mDailyBalance.service(mInXmlDoc);
		
		Element mRetData = mOutXmlDoc.getRootElement().getChild("RetData");
		if (mRetData.getChildText("Flag").equals("0")) {
			throw new MidplatException(mRetData.getChildText("Desc"));
		}
		
		JdomUtil.print(mOutXmlDoc);
		
      cLogger.info("Out TertianBalanceUI.deal()!");
	}
	
	private Element getBaseInfo() {
		cLogger.info("Into TertianBalanceUI.getBaseInfo()...");
      
		Element mBankDate = new Element(BankDate);
		mBankDate.setText(cTransDate);
		
		Element mBankCode = new Element(BankCode);
		mBankCode.setText(cBankCode);
		
		Element mZoneNo = new Element(ZoneNo);
		mZoneNo.setText(cZoneNo);
		
		Element mBrNo = new Element("BrNo");
		mBrNo.setText(cBankNode);
		
		Element mTellerNo = new Element(TellerNo);
		mTellerNo.setText("ybtAuto");
		
		Element mTransrNo = new Element(TransrNo);
		
		Element mFunctionFlag = new Element(FunctionFlag);
		mFunctionFlag.setText("17");
		
		Element mInsuID = new Element(InsuID);
		mInsuID.setText("02");
		
		Element mBaseInfo = new Element(BaseInfo);
      mBaseInfo.addContent(mBankDate);
      mBaseInfo.addContent(mBankCode);
      mBaseInfo.addContent(mZoneNo);
      mBaseInfo.addContent(mBrNo);
      mBaseInfo.addContent(mTellerNo);
      mBaseInfo.addContent(mTransrNo);
      mBaseInfo.addContent(mFunctionFlag);
      mBaseInfo.addContent(mInsuID);
		
      cLogger.info("Out TertianBalanceUI.getBaseInfo()!");
      return mBaseInfo;
	}
	
	/**
	 * 获取工行的标准对账明细
	 */
	private Element getIcbcDetails() throws Exception {
		cLogger.info("Into TertianBalanceUI.getIcbcXml()...");
		
		String mFileName = "02001" + cTransDate + "01.txt";
		URL mURL = new URL("ftp://PICCH:PICCHealth@10.252.4.10/" + mFileName);
		cLogger.info("URL = " + mURL);
		Reader mURLReader = new InputStreamReader(mURL.openStream());
		BufferedReader mBufReader = new BufferedReader(mURLReader);
		
		Element mChkDetails = new Element(ChkDetails);
		for (String tLineMsg; null != (tLineMsg=mBufReader.readLine());) {
			cLogger.info(tLineMsg);
			
			String[] tSubMsgs = tLineMsg.split("\\|");

			Element tBankCode = new Element(BankCode);
			tBankCode.setText(tSubMsgs[0]);
			
			Element tTranDate = new Element(TranDate);
			tTranDate.setText(tSubMsgs[1]);
			
			Element tBankZoneCode = new Element(BankZoneCode);
			tBankZoneCode.setText(tSubMsgs[2]);
			
			Element tBrNo = new Element(BrNo);
			tBrNo.setText(tSubMsgs[3]);
			
			Element tTellerNo = new Element(TellerNo);
			
			Element tFuncFlag = new Element(FuncFlag);
			tFuncFlag.setText("01");
			
			Element tTransrNo = new Element(TransrNo);
			tTransrNo.setText(tSubMsgs[5]);

			Element tCardNo = new Element(CardNo);
			tCardNo.setText(tSubMsgs[6]);
			
			Element tAppntName = new Element(AppntName);
			
			Element tTranAmnt = new Element(TranAmnt);
			tTranAmnt.setText(tSubMsgs[7]);
			
			Element tConfirmFlag = new Element(ConfirmFlag);
			tConfirmFlag.setText("1");
			
			Element tChkDetail = new Element(ChkDetail);
			tChkDetail.addContent(tBankCode);
			tChkDetail.addContent(tTranDate);
			tChkDetail.addContent(tBankZoneCode);
			tChkDetail.addContent(tBrNo);
			tChkDetail.addContent(tTellerNo);
			tChkDetail.addContent(tFuncFlag);
			tChkDetail.addContent(tTransrNo);
			tChkDetail.addContent(tCardNo);
			tChkDetail.addContent(tAppntName);
			tChkDetail.addContent(tTranAmnt);
			tChkDetail.addContent(tConfirmFlag);
			
			mChkDetails.addContent(tChkDetail);
		}
		
		mBufReader.close();	//关闭流
		
		cLogger.info("Out TertianBalanceUI.getIcbcXml()!");
		return mChkDetails;
	}

	/**
	 * 获取交行的标准对账明细
	 */
	private Element getBcmDetails() throws Exception {
		cLogger.info("Into TertianBalanceUI.getIcbcXml()...");
		
		String mFileName = "RBJK_" + cZoneNo + "_" + cTransDate;
		URL mURL = new URL("ftp://BCM_PICCH:BCM_PICCHealth@10.252.4.10/BC/" + mFileName);
		cLogger.info("URL = " + mURL);
		Reader mURLReader = new InputStreamReader(mURL.openStream());
		BufferedReader mBufReader = new BufferedReader(mURLReader);
		
		Element mChkDetails = new Element(ChkDetails);
		for (String tLineMsg; null != (tLineMsg=mBufReader.readLine());) {
			cLogger.info(tLineMsg);
			
			String[] tSubMsgs = tLineMsg.split("\\|");

			Element tBankCode = new Element(BankCode);
			tBankCode.setText(tSubMsgs[0]);
			
			Element tTranDate = new Element(TranDate);
			tTranDate.setText(tSubMsgs[1]);
			
			Element tBankZoneCode = new Element(BankZoneCode);
			tBankZoneCode.setText(tSubMsgs[2]);
			
			Element tBrNo = new Element(BrNo);
			tBrNo.setText(tSubMsgs[3]);
			
			Element tTellerNo = new Element(TellerNo);
			
			Element tFuncFlag = new Element(FuncFlag);
			tFuncFlag.setText(tSubMsgs[4]);
			
			Element tTransrNo = new Element(TransrNo);
			tTransrNo.setText(tSubMsgs[5]);

			Element tCardNo = new Element(CardNo);
			tCardNo.setText(tSubMsgs[6]);
			
			Element tAppntName = new Element(AppntName);
			
			Element tTranAmnt = new Element(TranAmnt);
			tTranAmnt.setText(tSubMsgs[7]);
			
			Element tConfirmFlag = new Element(ConfirmFlag);
			tConfirmFlag.setText(tSubMsgs[8]);
			
			Element tChkDetail = new Element(ChkDetail);
			tChkDetail.addContent(tBankCode);
			tChkDetail.addContent(tTranDate);
			tChkDetail.addContent(tBankZoneCode);
			tChkDetail.addContent(tBrNo);
			tChkDetail.addContent(tTellerNo);
			tChkDetail.addContent(tFuncFlag);
			tChkDetail.addContent(tTransrNo);
			tChkDetail.addContent(tCardNo);
			tChkDetail.addContent(tAppntName);
			tChkDetail.addContent(tTranAmnt);
			tChkDetail.addContent(tConfirmFlag);
			
			mChkDetails.addContent(tChkDetail);
		}
		
		cLogger.info("Out TertianBalanceUI.getIcbcXml()!");
		return mChkDetails;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mBankCode = "01";
		String mZoneNo = "86";
		String mBankNode = "86";
		String mTransDate = "2008-08-06";
		
		GlobalInput mGlobalInput = new GlobalInput();

		HashMap mHashMap = new HashMap();
		mHashMap.put("BankCode", mBankCode);
		mHashMap.put("ZoneNo", mZoneNo);
		mHashMap.put("BankNode", mBankNode);
		mHashMap.put("TransDate", mTransDate);
		
		TertianBalanceUI mTertianBalanceUI = new TertianBalanceUI(
				mHashMap, mGlobalInput);
		mTertianBalanceUI.deal();
		
		System.out.println("成功结束！");
	}
}

