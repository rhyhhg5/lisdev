package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKAmntUpLimitDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AmntUpLimitUI {
	private final static Logger cLogger = Logger.getLogger(AmntUpLimitUI.class);

	private final GlobalInput cGlobalInput;

	private final TransferData cTransferData;

	private final String cOperType;

	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");

	public AmntUpLimitUI(TransferData pTransferData, GlobalInput pGlobalInput,
			String pOperType) {
		cTransferData = pTransferData;
		cGlobalInput = pGlobalInput;
		cOperType = pOperType;
	}

	public void deal() throws Exception {
		cLogger.info("Into AmntUpLimitUI.deal()...");

		if (cOperType.equals("INSERT||MAIN")) {
			insertAmnt();
		} else if (cOperType.equals("UPDATE||MAIN")) {
			updateAmnt();
		} else if (cOperType.equals("DEL||MAIN")) {
			deleteAmnt();
		}

		cLogger.info("Out AmntUpLimitUI.deal()!");
	}

	private void insertAmnt() throws MidplatException {
		cLogger.info("Into AmntUpLimitUI.insertAmnt()...");

		LKAmntUpLimitDB mLKAmntUpLimitDB = new LKAmntUpLimitDB();
		mLKAmntUpLimitDB.setBankCode((String) cTransferData
				.getValueByName("BankCode"));
		mLKAmntUpLimitDB.setRiskWrapCode((String) cTransferData
				.getValueByName("RiskWrapCode"));
		mLKAmntUpLimitDB.setManageCom((String) cTransferData
				.getValueByName("ManageCom"));
		mLKAmntUpLimitDB.setAgentCom((String) cTransferData
				.getValueByName("AgentCom"));
		mLKAmntUpLimitDB.setAmntUpLimit((String) cTransferData
				.getValueByName("Amnt"));
		mLKAmntUpLimitDB.setOperator(cGlobalInput.Operator);
		mLKAmntUpLimitDB.setMakeDate(cCurDate);
		mLKAmntUpLimitDB.setMakeTime(cCurTime);
		mLKAmntUpLimitDB.setModifyDate(cCurDate);
		mLKAmntUpLimitDB.setModifyTime(cCurTime);
		if (!mLKAmntUpLimitDB.insert()) {
			cLogger.error(mLKAmntUpLimitDB.mErrors.getFirstError());
			throw new MidplatException("插入保额上限信息失败！");
		}

		cLogger.info("Out AmntUpLimitUI.insertAmnt()!");
	}

	private void updateAmnt() throws MidplatException {
		cLogger.info("Into AmntUpLimitUI.updateAmnt()...");

		String BankCode = (String) cTransferData.getValueByName("BankCode");
		String RiskWrapCode = (String) cTransferData
				.getValueByName("RiskWrapCode");
		String ManageCom = (String) cTransferData.getValueByName("ManageCom");
		String AgentCom = (String) cTransferData.getValueByName("AgentCom");
		String mAmntUpLimit = (String) cTransferData.getValueByName("Amnt");
		String Operator = cGlobalInput.Operator;

		String hideBankCode = (String) cTransferData
				.getValueByName("hideBankCode");
		String hideRiskWrapCode = (String) cTransferData
				.getValueByName("hideRiskWrapCode");
		String hideManageCom = (String) cTransferData
				.getValueByName("hideManageCom");
		String hideAgentCom = (String) cTransferData
				.getValueByName("hideAgentCom");
		String hideAmnt = (String) cTransferData.getValueByName("hideAmnt");

		MMap mSubmitMMap = new MMap();
		String mSQLStr1 = "update lkamntuplimit set BankCode='"
				+ BankCode.trim() + "', ManageCom='" + ManageCom.trim()
				+ "',RiskWrapCode='" + RiskWrapCode.trim() + "',AgentCom='"
				+ AgentCom + "',AmntUpLimit=" + mAmntUpLimit + ",Operator='"
				+ Operator.trim() + "',ModifyDate='" + cCurDate
				+ "',ModifyTime='" + cCurTime + "' where BankCode='"
				+ hideBankCode.trim() + "' and  RiskWrapCode='"
				+ hideRiskWrapCode.trim() + "' and ManageCom='"
				+ hideManageCom.trim() + "' and AgentCom='" + hideAgentCom
				+ "' and AmntUpLimit = " + hideAmnt.trim();

		mSubmitMMap.put(mSQLStr1, "UPDATE");

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("更新保额上限信息失败！");
		}

		cLogger.info("Out AmntUpLimitUI.updateAmnt()!");
	}

	private void deleteAmnt() throws MidplatException {
		cLogger.info("Into AmntUpLimitUI.deleteAmnt()...");

		String mBankCode = (String) cTransferData
				.getValueByName("hideBankCode");
		String mManageCom = (String) cTransferData
				.getValueByName("hideManageCom");
		String mRiskWrapCode = (String) cTransferData
				.getValueByName("hideRiskWrapCode");
		String mAgentCom = (String) cTransferData
				.getValueByName("hideAgentCom");
		String mAmntUpLimit = (String) cTransferData.getValueByName("hideAmnt");
		cLogger.info(mBankCode);
		cLogger.info(mManageCom);
		cLogger.info(mRiskWrapCode);
		cLogger.info(mAgentCom);
		cLogger.info(mAmntUpLimit);
		String mSQLStr = "DELETE from lkamntuplimit where BankCode='"
				+ mBankCode.trim() + "' and  ManageCom='" + mManageCom.trim()
				+ "' and RiskWrapCode='" + mRiskWrapCode.trim()
				+ "' and AgentCom='" + mAgentCom.trim()
				+ "' and AmntUpLimit = " + mAmntUpLimit.trim();
		cLogger.info(mSQLStr);
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mSQLStr, "DELETE");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("删除保额上限配置信息失败！");
		}

		cLogger.info("Out AmntUpLimitUI.deleteAmnt()!");
	}
}
