/**
 * 网点管理。
 */

package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LKCodeMappingDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NodeManageUI {
	private final static Logger cLogger = Logger.getLogger(NodeManageUI.class);
	
	private final GlobalInput cGlobalInput;
	
	private final TransferData cTransferData;
	
	private final String cOperType;
	
	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");
	
	public NodeManageUI(TransferData pTransferData, GlobalInput pGlobalInput, String pOperType) {
		cTransferData = pTransferData;
		cGlobalInput = pGlobalInput;
		cOperType = pOperType;
	}
	
	public void deal() throws Exception {
		cLogger.info("Into NodeManageUI.deal()...");
		
		if (cOperType.equals("INSERT||MAIN")) {
			insertNode();
		} else if (cOperType.equals("UPDATE||MAIN")) {
			updateNode();
		} else if (cOperType.equals("DEL||MAIN")) {
			deleteNode();
		}
		
      cLogger.info("Out NodeManageUI.deal()!");
	}
	
	private void insertNode() throws MidplatException {
		cLogger.info("Into NodeManageUI.insertNode()...");
		
		LKCodeMappingDB mLKCodeMappingDB = new LKCodeMappingDB();
		mLKCodeMappingDB.setBankCode((String)cTransferData.getValueByName("BankCode"));
		mLKCodeMappingDB.setZoneNo((String)cTransferData.getValueByName("ZoneNo"));
		mLKCodeMappingDB.setBankNode((String)cTransferData.getValueByName("BankNode"));
		mLKCodeMappingDB.setAgentCom((String)cTransferData.getValueByName("UpAgentCom"));
		mLKCodeMappingDB.setComCode((String)cTransferData.getValueByName("ComCode"));
		mLKCodeMappingDB.setManageCom((String)cTransferData.getValueByName("ManageCom"));
		mLKCodeMappingDB.setRemark((String)cTransferData.getValueByName("Remark"));
//		mLKCodeMappingDB.setOperator((String)cTransferData.getValueByName("Operator"));
		mLKCodeMappingDB.setOperator(cGlobalInput.Operator);
		mLKCodeMappingDB.setRemark1((String)cTransferData.getValueByName("Remark1"));
		mLKCodeMappingDB.setRemark2((String)cTransferData.getValueByName("Remark2"));
		mLKCodeMappingDB.setRemark5((String)cTransferData.getValueByName("Remark5"));
		mLKCodeMappingDB.setRemark3((String)cTransferData.getValueByName("Remark3"));
		mLKCodeMappingDB.setFTPAddress((String)cTransferData.getValueByName("FTPAddress"));
		mLKCodeMappingDB.setFTPDir((String)cTransferData.getValueByName("FTPDir"));
		mLKCodeMappingDB.setFTPUser((String)cTransferData.getValueByName("FTPUser"));
		mLKCodeMappingDB.setFTPPassWord((String)cTransferData.getValueByName("FTPPassWord"));
		mLKCodeMappingDB.setMakeDate(cCurDate);
		mLKCodeMappingDB.setMakeTime(cCurTime);
		mLKCodeMappingDB.setModifyDate(cCurDate);
		mLKCodeMappingDB.setModifyTime(cCurTime);
		if (!mLKCodeMappingDB.insert()) {
			cLogger.error(mLKCodeMappingDB.mErrors.getFirstError());
			throw new MidplatException("插入网点信息失败！");
		}
		
		cLogger.info("Out NodeManageUI.insertNode()!");
	}
	
	private void updateNode() throws MidplatException {
		cLogger.info("Into NodeManageUI.updateNode()...");

		String BankCode = (String)cTransferData.getValueByName("BankCode");
		String ZoneNo = (String)cTransferData.getValueByName("ZoneNo");
		String BankNode=(String)cTransferData.getValueByName("BankNode");
		String UpAgentCom=(String)cTransferData.getValueByName("UpAgentCom");
		String ComCode=(String)cTransferData.getValueByName("ComCode");
		String ManageCom=(String)cTransferData.getValueByName("ManageCom");
		String Remark=(String)cTransferData.getValueByName("Remark");
//		String Operator=(String)cTransferData.getValueByName("Operator");
		String Operator = cGlobalInput.Operator;
		String Remark5 = (String)cTransferData.getValueByName("Remark5");
		String Remark1=(String)cTransferData.getValueByName("Remark1");
//		String Remark2=(String)cTransferData.getValueByName("Remark2");
		String Remark3=(String)cTransferData.getValueByName("Remark3");
		String FTPAddress=(String)cTransferData.getValueByName("FTPAddress");
		String FTPDir=(String)cTransferData.getValueByName("FTPDir");
		String FTPUser=(String)cTransferData.getValueByName("FTPUser");
		String FTPPassWord=(String)cTransferData.getValueByName("FTPPassWord");

		String hideBankCode=(String)cTransferData.getValueByName("hideBankCode");
		String hideZoneNo=(String)cTransferData.getValueByName("hideZoneNo");
		String hideBankNode=(String)cTransferData.getValueByName("hideBankNode");

		MMap mSubmitMMap = new MMap();
		String mSQLStr1 = "update LKCodeMapping set BankCode='"+BankCode.trim()+"', ZoneNo='"+ZoneNo.trim()+"', BankNode='"+BankNode.trim()+"',AgentCom='"+UpAgentCom.trim()+"',ComCode='"+ComCode.trim()+"', ManageCom='"+ManageCom.trim()+"',Remark='"+Remark.trim()+"',Remark5='"+Remark5.trim()+"' ,Operator='"+Operator.trim()+"',Remark1='"+Remark1.trim()+"',Remark3='"+Remark3.trim()+"',ModifyDate='"+cCurDate+"',ModifyTime='"+cCurTime+"',FTPAddress='"+FTPAddress.trim()+"',FTPDir='"+FTPDir.trim()+"',FTPUser='"+FTPUser.trim()+"',FTPPassWord='"+FTPPassWord.trim()+"' where BankCode='"+hideBankCode.trim()+"' and  ZoneNo='"+hideZoneNo.trim()+"' and BankNode='"+hideBankNode.trim()+"'" ;
		mSubmitMMap.put(mSQLStr1, "UPDATE");
		if(Remark5.equals("1")) {
			String tSQLStr2 = "update LKCodeMapping set Remark1='"+ BankNode.trim()+"', Remark3='"+ ZoneNo.trim()+"' where remark5 ='0' and managecom like '"+ ManageCom+"%' and trim(BankCode) ='"+BankCode.trim()+"'";
			mSubmitMMap.put(tSQLStr2, "UPDATE");
		}

		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("更新网点失败！");
		}

		cLogger.info("Out NodeManageUI.updateNode()!");
	}
	
	private void deleteNode() throws MidplatException {
		cLogger.info("Into NodeManageUI.deleteNode()...");
		
		String mBankCode=(String)cTransferData.getValueByName("hideBankCode");
		String mZoneNo=(String)cTransferData.getValueByName("hideZoneNo");
		String mBankNode=(String)cTransferData.getValueByName("hideBankNode");
		
      String mSQLStr= "DELETE from LKCodeMapping where BankCode='"+mBankCode.trim()+"' and  ZoneNo='"+mZoneNo.trim()+"' and BankNode='"+mBankNode.trim()+"'" ;
      
      MMap mSubmitMMap = new MMap();
      mSubmitMMap.put(mSQLStr,"DELETE");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("删除网点失败！");
		}
      
		cLogger.info("Out NodeManageUI.deleteNode()!");
	}
}
