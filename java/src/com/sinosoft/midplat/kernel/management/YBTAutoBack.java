package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LBAppntDB;
import com.sinosoft.lis.db.LBBnfDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBDutyDB;
import com.sinosoft.lis.db.LBGetDB;
import com.sinosoft.lis.db.LBInsureAccClassDB;
import com.sinosoft.lis.db.LBInsureAccClassFeeDB;
import com.sinosoft.lis.db.LBInsureAccDB;
import com.sinosoft.lis.db.LBInsureAccFeeDB;
import com.sinosoft.lis.db.LBInsureAccFeeTraceDB;
import com.sinosoft.lis.db.LBInsureAccTraceDB;
import com.sinosoft.lis.db.LBInsuredDB;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LBPremDB;
import com.sinosoft.lis.db.LBSpecDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LKTransStatusDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBAppntSchema;
import com.sinosoft.lis.schema.LBBnfSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBDutySchema;
import com.sinosoft.lis.schema.LBGetSchema;
import com.sinosoft.lis.schema.LBInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LBInsureAccClassSchema;
import com.sinosoft.lis.schema.LBInsureAccFeeSchema;
import com.sinosoft.lis.schema.LBInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LBInsureAccSchema;
import com.sinosoft.lis.schema.LBInsureAccTraceSchema;
import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LBPremSchema;
import com.sinosoft.lis.schema.LBSpecSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LCSpecSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LKTransStatusSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.tb.ContReceiveUI;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.lis.vschema.LBAppntSet;
import com.sinosoft.lis.vschema.LBBnfSet;
import com.sinosoft.lis.vschema.LBContSet;
import com.sinosoft.lis.vschema.LBDutySet;
import com.sinosoft.lis.vschema.LBGetSet;
import com.sinosoft.lis.vschema.LBInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LBInsureAccClassSet;
import com.sinosoft.lis.vschema.LBInsureAccFeeSet;
import com.sinosoft.lis.vschema.LBInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LBInsureAccSet;
import com.sinosoft.lis.vschema.LBInsureAccTraceSet;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LBPremSet;
import com.sinosoft.lis.vschema.LBSpecSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeTraceSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YBTAutoBack {	
	private GlobalInput cGlobalInput = new GlobalInput();

	public CErrors mErrors = new CErrors();

	private Logger cLogger = Logger.getLogger(YBTAutoBack.class);

	private final LBContSchema cLBContSchema;
	private final LKTransStatusSchema cLKTransStatusSchema;

	public YBTAutoBack(String pContNo) throws MidplatException {
		//查询保单信息
		LBContDB mLBContDB = new LBContDB();
		mLBContDB.setContNo(pContNo);
        mLBContDB.setCardFlag("9"); //标识银保通
		if (!mLBContDB.getInfo()) {
			throw new MidplatException("查询保单数据(LBCont)失败!");
		}
		cLBContSchema = mLBContDB.getSchema();
		
        //判断是否是对账后的撤单.
        String mSQL = "select stateflag from lzcard where startno = '"+ cLBContSchema.getProposalContNo() 
                    +"' and endno = '" +  cLBContSchema.getProposalContNo() + "' with ur";
        if(!"8".equals(new ExeSQL().getOneValue(mSQL))) {
            throw new MidplatException("此单不是对账时撤销的保单!");
        }        
        
		//查询该保单对应的新契约日志信息
		LKTransStatusDB mLKTransStatusDB = new LKTransStatusDB();
		mLKTransStatusDB.setPolNo(pContNo);
		mLKTransStatusDB.setStatus("2");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
		mLKTransStatusDB.setFuncFlag("01");
		com.sinosoft.lis.vschema.LKTransStatusSet mLKTransStatusSet = mLKTransStatusDB.query();
		if (1 != mLKTransStatusSet.size()) {
			cLogger.error("查询新契约日志异常：" + mLKTransStatusSet.mErrors.getFirstError());
			throw new MidplatException("新契约日志异常！");
		}
		cLKTransStatusSchema = mLKTransStatusSet.get(1);
		
		cGlobalInput = YbtSufUtil.getGlobalInput(
				cLKTransStatusSchema.getBankCode(),
				cLKTransStatusSchema.getBankBranch(),
				cLKTransStatusSchema.getBankNode());
	}
	
	/**
	 * 银行认为承保成功，midplat核心显示承保后冲正
	 * @throws MidplatException 
	 */
	public void deal() throws MidplatException {
		cLogger.info("Into YBTAutoBack.submitData()...");
		
		RollBankWriteOff(cLBContSchema.getContNo());		

		//对应新契约日志中保单状态恢复为：1-生效
		cLKTransStatusSchema.setStatus("1");	//保单当前状态：0-未承保；1-生效；2-作废(当日撤单)
        cLKTransStatusSchema.setModifyDate(DateUtil.getCurDate("yyyy-MM-dd"));
		cLKTransStatusSchema.setModifyTime(DateUtil.getCurDate("HH:mm:ss"));
        cLKTransStatusSchema.setReportNo("R"); //借用此字段标识此数据是经恢复后的数据.
        cLKTransStatusSchema.setResultBalance("0"); //改写对账标志,以便此单能进行犹豫期退保操作.
		LKTransStatusDB mLKTransStatusDB = cLKTransStatusSchema.getDB();
		if (!mLKTransStatusDB.update()) {
			cLogger.error(mLKTransStatusDB.mErrors.getFirstError());
			throw new MidplatException("更新保单状态失败！");
		}
		
		//单证核销! 对账时自动进行的撤单操作会将单证状态置为有效.
		if (!cLBContSchema.getProposalContNo().startsWith("YBT")) {
			certMakeUsed();
		}
		
		//回执回销!	
		autoReceive(cLBContSchema.getContNo());
        
        //重新回执回销后,回执回销日期会发生变化,将其维护成签单日期,保持数据一致性.
        String mSQL = "update lccont set GetPolDate = '" + cLBContSchema.getSignDate() + "' , CustomGetPolDate = '"
                    + cLBContSchema.getSignDate() + "' where contno = '" +cLBContSchema.getContNo()+ "' ";
        if(!new ExeSQL().execUpdateSQL(mSQL)){
            throw new MidplatException("更新回执回销日期失败!");
        }
        
		cLogger.info("Out YBTAutoBack.submitData()!");

	}

	/**
	 * 如果冲正确认时,数据从B表中会恢复，将修改时间置为对账时间，
	 * 并在ljtemfee和ljtempfeeclass表中插入一条正记录
	 * @throws MidplatException 
	 */
	private void RollBankWriteOff(String pContNo) throws MidplatException {
		cLogger.info("Into YBTAutoBack.RollBankWriteOff()...");

		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");	
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		MMap mSubmitMMap = new MMap();

		Reflections mReflections = new Reflections();
		LBContSchema mLBContSchema = new LBContSchema();
		LBContDB mLBContDB = new LBContDB();
		String mContNo = pContNo;
		mLBContDB.setContNo(mContNo);
		LBContSet mLBContSet = mLBContDB.query();
		if ((null!=mLBContSet) && (mLBContSet.size()>0)) {
			mLBContSchema = mLBContSet.get(1);				
			LCContSchema tLCContSchema = new LCContSchema();
			mReflections.transFields(tLCContSchema, mLBContSchema);
			mSubmitMMap.put(tLCContSchema, "INSERT");
			mSubmitMMap.put(mLBContSchema, "DELETE");
		}

		LBPolDB mLBPolDB = new LBPolDB();
		mLBPolDB.setContNo(mContNo);
		LBPolSet mLBPolSet = mLBPolDB.query();			
		LCPolSet mLCPolSet = new LCPolSet();
		for (int i = 1; i <= mLBPolSet.size(); i++) {
			LBPolSchema tLBPolSchema = mLBPolSet.get(i);
			LCPolSchema tLCPolSchema = new LCPolSchema();
			mReflections.transFields(tLCPolSchema, tLBPolSchema);
			tLCPolSchema.setSignDate(tLBPolSchema.getSignDate());
			mLCPolSet.add(tLCPolSchema);

			LBSpecDB tLBSpecDB = new LBSpecDB();
			tLBSpecDB.setPolNo(tLBPolSchema.getPolNo());
			LBSpecSet tLBSpecSet = tLBSpecDB.query();
			LCSpecSet tLCSpecSet = new LCSpecSet();
			for (int j = 1; j <= tLBSpecSet.size(); j++) {
				LBSpecSchema tLBSpecSchema = tLBSpecSet.get(j);
				LCSpecSchema tLCSpecSchema = new LCSpecSchema();
				mReflections.transFields(tLCSpecSchema, tLBSpecSchema);

				tLCSpecSet.add(tLCSpecSchema);
			}
			mSubmitMMap.put(tLCSpecSet, "INSERT");
			mSubmitMMap.put(tLBSpecSet, "DELETE");
		}
		mSubmitMMap.put(mLCPolSet, "INSERT");
		mSubmitMMap.put(mLBPolSet, "DELETE");

		LBAppntDB mLBAppntDB = new LBAppntDB();
		mLBAppntDB.setContNo(mContNo);
		LBAppntSet mLBAppntSet = mLBAppntDB.query();
		LCAppntSet mLCAppntSet = new LCAppntSet();
		for (int i = 1; i <= mLBAppntSet.size(); i++) {
			LBAppntSchema tLBAppntSchema = new LBAppntSchema();
			tLBAppntSchema = mLBAppntSet.get(i);
			LCAppntSchema tLCAppntSchema = new LCAppntSchema();
			mReflections.transFields(tLCAppntSchema, tLBAppntSchema);

			mLCAppntSet.add(tLCAppntSchema);
		}
		mSubmitMMap.put(mLCAppntSet, "INSERT");
		mSubmitMMap.put(mLBAppntSet, "DELETE");

		LBInsuredDB mLBInsuredDB = new LBInsuredDB();
		mLBInsuredDB.setContNo(mContNo);
		LBInsuredSet mLBInsuredSet = mLBInsuredDB.query();
		LCInsuredSet mLCInsuredSet = new LCInsuredSet();
		for (int i = 1; i <= mLBInsuredSet.size(); i++) {
			LBInsuredSchema tLBInsuredSchema = mLBInsuredSet.get(i);
			LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
			mReflections.transFields(tLCInsuredSchema, tLBInsuredSchema);

			mLCInsuredSet.add(tLCInsuredSchema);
		}
		mSubmitMMap.put(mLCInsuredSet, "INSERT");
		mSubmitMMap.put(mLBInsuredSet, "DELETE");

		LBPremDB mLBPremDB = new LBPremDB();
		mLBPremDB.setContNo(mContNo);
		LBPremSet mLBPremSet = mLBPremDB.query();
		LCPremSet mLCPremSet = new LCPremSet();
		for (int i = 1; i <= mLBPremSet.size(); i++) {
			LBPremSchema tLBPremSchema = new LBPremSchema();
			tLBPremSchema = mLBPremSet.get(i);
			LCPremSchema tLCPremSchema = new LCPremSchema();
			mReflections.transFields(tLCPremSchema, tLBPremSchema);

			mLCPremSet.add(tLCPremSchema);
		}
		mSubmitMMap.put(mLCPremSet, "INSERT");
		mSubmitMMap.put(mLBPremSet, "DELETE");

		LBGetDB mLBGetDB = new LBGetDB();
		mLBGetDB.setContNo(mContNo);
		LBGetSet mLBGetSet = mLBGetDB.query();
		LCGetSet mLCGetSet = new LCGetSet();
		for (int i = 1; i <= mLBGetSet.size(); i++) {
			LBGetSchema tLBGetSchema = mLBGetSet.get(i);
			LCGetSchema tLCGetSchema = new LCGetSchema();
			mReflections.transFields(tLCGetSchema, tLBGetSchema);

			mLCGetSet.add(tLCGetSchema);
		}
		mSubmitMMap.put(mLCGetSet, "INSERT");
		mSubmitMMap.put(mLBGetSet, "DELETE");

		LBDutyDB mLBDutyDB = new LBDutyDB();
		mLBDutyDB.setContNo(mContNo);
		LBDutySet mLBDutySet = mLBDutyDB.query();
		LCDutySet mLCDutySet = new LCDutySet();
		for (int i = 1; i <= mLBDutySet.size(); i++) {
			LBDutySchema tLBDutySchema = mLBDutySet.get(i);
			LCDutySchema tLCDutySchema = new LCDutySchema();
			mReflections.transFields(tLCDutySchema, tLBDutySchema);

			mLCDutySet.add(tLCDutySchema);
		}
		mSubmitMMap.put(mLCDutySet, "INSERT");
		mSubmitMMap.put(mLBDutySet, "DELETE");
        
        //缺少lbbnf的恢复 yinjj
        LBBnfDB mLBBnfDB = new LBBnfDB();
        mLBBnfDB.setContNo(mContNo);
        LBBnfSet mLBBnfSet = mLBBnfDB.query();
        LCBnfSet mLCBnfSet = new LCBnfSet();
        for (int i = 1; i <= mLBBnfSet.size(); i++) {
            LBBnfSchema tLBBnfSchema = mLBBnfSet.get(i);
            LCBnfSchema tLCBnfSchema = new LCBnfSchema();
            mReflections.transFields(tLCBnfSchema, tLBBnfSchema);

            mLCBnfSet.add(tLCBnfSchema);
        }
        mSubmitMMap.put(mLCBnfSet, "INSERT");
        mSubmitMMap.put(mLBBnfSet, "DELETE");
        
        //对帐户相关的表进行操作.
        //lcinsureacc
        LBInsureAccDB mLBInsureAccDB = new LBInsureAccDB();
        mLBInsureAccDB.setContNo(mContNo);
        LBInsureAccSet mLBInsureAccSet = mLBInsureAccDB.query();
        LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
        for (int i = 1;i <= mLBInsureAccSet.size(); i++) {
            LBInsureAccSchema tLBInsureAccSchema = mLBInsureAccSet.get(i);
            LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
            mReflections.transFields(tLCInsureAccSchema, tLBInsureAccSchema);
            
            mLCInsureAccSet.add(tLCInsureAccSchema);
        }
        mSubmitMMap.put(mLCInsureAccSet, "INSERT");
        mSubmitMMap.put(mLBInsureAccSet, "DELETE");
        
        //lcinsureacctrace
        LBInsureAccTraceDB  mLBInsureAccTraceDB = new LBInsureAccTraceDB();
        mLBInsureAccTraceDB.setContNo(mContNo);
        LBInsureAccTraceSet mLBInsureAccTraceSet = mLBInsureAccTraceDB.query();
        LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
        for (int i = 1;i <= mLBInsureAccTraceSet.size(); i++) {
            LBInsureAccTraceSchema tLBInsureAccTraceSchema = mLBInsureAccTraceSet.get(i);
            LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
            mReflections.transFields(tLCInsureAccTraceSchema, tLBInsureAccTraceSchema);
            
            mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
        }
        mSubmitMMap.put(mLCInsureAccTraceSet, "INSERT");
        mSubmitMMap.put(mLBInsureAccTraceSet, "DELETE");
        
        //lcinsureaccfee
        LBInsureAccFeeDB mLBInsureAccFeeDB = new LBInsureAccFeeDB();
        mLBInsureAccFeeDB.setContNo(mContNo);
        LBInsureAccFeeSet mLBInsureAccFeeSet = mLBInsureAccFeeDB.query();
        LCInsureAccFeeSet mLCInsureAccFeeSet = new LCInsureAccFeeSet();
        for (int i = 1; i <= mLBInsureAccFeeSet.size(); i++) {
            LBInsureAccFeeSchema tLBInsureAccFeeSchema = mLBInsureAccFeeSet.get(i);
            LCInsureAccFeeSchema tLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
            mReflections.transFields(tLCInsureAccFeeSchema, tLBInsureAccFeeSchema);
            
            mLCInsureAccFeeSet.add(tLCInsureAccFeeSchema);
        }
        mSubmitMMap.put(mLCInsureAccFeeSet, "INSERT");
        mSubmitMMap.put(mLBInsureAccFeeSet, "DELETE");
        
        //LCInsureAccFeeTrace
        LBInsureAccFeeTraceDB mLBInsureAccFeeTraceDB = new LBInsureAccFeeTraceDB();
        mLBInsureAccFeeTraceDB.setContNo(mContNo);
        LBInsureAccFeeTraceSet mLBInsureAccFeeTraceSet = mLBInsureAccFeeTraceDB.query();
        LCInsureAccFeeTraceSet mLCInsureAccFeeTraceSet = new LCInsureAccFeeTraceSet();
        for (int i = 1; i <= mLBInsureAccFeeTraceSet.size(); i++) {
            LBInsureAccFeeTraceSchema tLBInsureAccFeeTraceSchema = mLBInsureAccFeeTraceSet.get(i);
            LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
            mReflections.transFields(tLCInsureAccFeeTraceSchema, tLBInsureAccFeeTraceSchema);
            
            mLCInsureAccFeeTraceSet.add(tLCInsureAccFeeTraceSchema);
        }
        mSubmitMMap.put(mLCInsureAccFeeTraceSet, "INSERT");
        mSubmitMMap.put(mLBInsureAccFeeTraceSet, "DELETE");
        
        //lcinsureaccclass
        LBInsureAccClassDB mLBInsureAccClassDB = new LBInsureAccClassDB();
        mLBInsureAccClassDB.setContNo(mContNo);
        LBInsureAccClassSet mLBInsureAccClassSet = mLBInsureAccClassDB.query();
        LCInsureAccClassSet mLCInsureAccClassSet = new LCInsureAccClassSet();
        for (int i = 1; i <= mLBInsureAccClassSet.size(); i++) {
            LBInsureAccClassSchema tLBInsureAccClassSchema = mLBInsureAccClassSet.get(i);
            LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
            mReflections.transFields(tLCInsureAccClassSchema, tLBInsureAccClassSchema);
            
            mLCInsureAccClassSet.add(tLCInsureAccClassSchema);
        }
        mSubmitMMap.put(mLCInsureAccClassSet, "INSERT");
        mSubmitMMap.put(mLBInsureAccClassSet, "DELETE");
        
        //lcinsureaccclassfee
        LBInsureAccClassFeeDB mLBInsureAccClassFeeDB = new LBInsureAccClassFeeDB();
        mLBInsureAccClassFeeDB.setContNo(mContNo);
        LBInsureAccClassFeeSet mLBInsureAccClassFeeSet = mLBInsureAccClassFeeDB.query();
        LCInsureAccClassFeeSet mLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
        for (int i = 1; i <= mLBInsureAccClassFeeSet.size(); i++) {
            LBInsureAccClassFeeSchema tLBInsureAccClassFeeSchema = mLBInsureAccClassFeeSet.get(i);
            LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new LCInsureAccClassFeeSchema();
            mReflections.transFields(tLCInsureAccClassFeeSchema, tLBInsureAccClassFeeSchema);
            
            mLCInsureAccClassFeeSet.add(tLCInsureAccClassFeeSchema);
        }
        mSubmitMMap.put(mLCInsureAccClassFeeSet, "INSERT");
        mSubmitMMap.put(mLBInsureAccClassFeeSet, "DELETE");
        
        

		String mLimit = PubFun.getNoLimit(mLBContSchema.getManageCom()); // 产生通知书号即暂交费收据号
		// String tNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit);//生成暂交费收据号
		// mTempFeeNo = "YBT" + tNo; //自动生成一个流水号，为防止与核心冲突，头部加上YBT
		String mSQL = "select max(tempfeeno) from ljtempfee where tempfeeno like '" + mLBContSchema.getPrtNo() + "%' with ur";
		String mMaxOldTempFeeNo = new ExeSQL().getOneValue(mSQL);
		String mNewTempFeeNo = null;
		try {
			String tSuffix = mMaxOldTempFeeNo.substring(mMaxOldTempFeeNo.length() - 3);
			mNewTempFeeNo = mLBContSchema.getPrtNo() + String.valueOf(Integer.parseInt(tSuffix) + 1); // 暂收费号+1
		} catch (NumberFormatException e) {
			if (mMaxOldTempFeeNo.equals("")) {
				throw new MidplatException("查询原暂收费号失败!");
			}
		}
		mSQL = "select tempfeeno from ljtempfee where OtherNo='"+ mContNo + "' fetch first 1 rows only with ur";
		String mOldTempFeeNo = new ExeSQL().getOneValue(mSQL);
		if (mOldTempFeeNo.equals("")) {
			throw new MidplatException("查询原暂收数据(LJTempFee)失败！");
		}

		LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
		mLJTempFeeDB.setTempFeeNo(mOldTempFeeNo);
		mLJTempFeeDB.setOtherNo(mContNo);
		LJTempFeeSet mOldLJTempFeeSet = mLJTempFeeDB.query();
		if (mOldLJTempFeeSet.size() < 1) {
			throw new MidplatException("查询原暂收数据(LJTempFee)失败！"+mLJTempFeeDB.mErrors.getFirstError());
		}
		LJTempFeeSet mNewLJTempFeeSet = new LJTempFeeSet();
		for (int i = 1; i <= mOldLJTempFeeSet.size(); i++) {
			LJTempFeeSchema tLJTempFeeSchema = mOldLJTempFeeSet.get(i);				
			tLJTempFeeSchema.setTempFeeNo(mNewTempFeeNo);
			tLJTempFeeSchema.setPayMoney(Math.abs(tLJTempFeeSchema.getPayMoney()));
			tLJTempFeeSchema.setConfDate(mCurrentDate);	//签单日期
			tLJTempFeeSchema.setEnterAccDate(mCurrentDate);	//到账日期
			tLJTempFeeSchema.setConfMakeDate(mCurrentDate);	//到账确认日期
			tLJTempFeeSchema.setMakeDate(mCurrentDate);
			tLJTempFeeSchema.setMakeTime(mCurrentTime);
			tLJTempFeeSchema.setModifyDate(mCurrentDate);
			tLJTempFeeSchema.setModifyTime(mCurrentTime);

			mNewLJTempFeeSet.add(tLJTempFeeSchema);
		}			

		LJTempFeeClassDB mLJTempFeeClassDB = new LJTempFeeClassDB();
		mLJTempFeeClassDB.setTempFeeNo(mOldTempFeeNo);			
		LJTempFeeClassSet mOldLJTempFeeClassSet = mLJTempFeeClassDB.query();
		if (mOldLJTempFeeClassSet.size() < 1) {
			throw new MidplatException("查询原暂收数据(LJTempFeeClass)失败！"+mLJTempFeeClassDB.mErrors.getFirstError());
		}
		LJTempFeeClassSet mNewLJTempFeeClassSet = new LJTempFeeClassSet();
		for (int i = 1; i <= mOldLJTempFeeClassSet.size(); i++) {
			LJTempFeeClassSchema tLJTempFeeClassSchema = mOldLJTempFeeClassSet.get(i);
			tLJTempFeeClassSchema.setTempFeeNo(mNewTempFeeNo);
			tLJTempFeeClassSchema.setPayMoney(Math.abs(tLJTempFeeClassSchema.getPayMoney()));
			tLJTempFeeClassSchema.setConfDate(mCurrentDate);	//签单日期
			tLJTempFeeClassSchema.setEnterAccDate(mCurrentDate);	//到账日期
			tLJTempFeeClassSchema.setConfMakeDate(mCurrentDate);	//到账确认日期
			tLJTempFeeClassSchema.setMakeDate(mCurrentDate);
			tLJTempFeeClassSchema.setMakeTime(mCurrentTime);
			tLJTempFeeClassSchema.setModifyDate(mCurrentDate);
			tLJTempFeeClassSchema.setModifyTime(mCurrentTime);

			mNewLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		}

		String mNewPayNo = "";
		LJAPayDB mLJAPayDB = new LJAPayDB();
		mLJAPayDB.setIncomeNo(mContNo);
		mLJAPayDB.setOperator("ybt");
		LJAPaySet mOldLJAPaySet = mLJAPayDB.query();
		if (mOldLJAPaySet.size() < 1) {
			throw new MidplatException("查询原实收数据(LJAPay)失败！"+mLJAPayDB.mErrors.getFirstError());
		}
		LJAPaySet mNewLJAPaySet = new LJAPaySet();
		for (int i = 1; i <= mOldLJAPaySet.size(); i++) {
			mNewPayNo = PubFun1.CreateMaxNo("PayNo", mLimit);
			LJAPaySchema tLJAPaySchema = mOldLJAPaySet.get(i);
			tLJAPaySchema.setSumActuPayMoney(tLJAPaySchema.getSumActuPayMoney());
			tLJAPaySchema.setPayNo(mNewPayNo);
			tLJAPaySchema.setGetNoticeNo(mNewTempFeeNo);
			tLJAPaySchema.setOperator("-" + tLJAPaySchema.getOperator());
//			tLJAPaySchema.setPayTypeFlag("");
			tLJAPaySchema.setConfDate(mCurrentDate);
			tLJAPaySchema.setEnterAccDate(mCurrentDate);
			tLJAPaySchema.setMakeDate(mCurrentDate);
			tLJAPaySchema.setMakeTime(mCurrentTime);
			tLJAPaySchema.setModifyDate(mCurrentDate);
			tLJAPaySchema.setModifyTime(mCurrentTime);

			mNewLJAPaySet.add(tLJAPaySchema);
		}			

		LJAPayPersonDB mLJAPayPersonDB = new LJAPayPersonDB();
		mLJAPayPersonDB.setContNo(mContNo);
		mLJAPayPersonDB.setOperator("ybt");
		LJAPayPersonSet mOldLJAPayPersonSet = mLJAPayPersonDB.query();
		if (mOldLJAPaySet.size() < 1) {
			throw new MidplatException("查询原实收数据(LJAPayPerson)失败！"+mLJAPayPersonDB.mErrors.getFirstError());
		}
		LJAPayPersonSet mNewLJAPayPersonSet = new LJAPayPersonSet();
		for (int i = 1; i <= mOldLJAPayPersonSet.size(); i++) {
			LJAPayPersonSchema tLJAPayPersonSchema = mOldLJAPayPersonSet.get(i);
			tLJAPayPersonSchema.setSumActuPayMoney(tLJAPayPersonSchema.getSumActuPayMoney());
			tLJAPayPersonSchema.setSumDuePayMoney(Math.abs(tLJAPayPersonSchema.getSumDuePayMoney()));
			tLJAPayPersonSchema.setPayNo(mNewPayNo);
			tLJAPayPersonSchema.setGetNoticeNo(mNewTempFeeNo);
			tLJAPayPersonSchema.setOperator("-" + tLJAPayPersonSchema.getOperator());
//			tLJAPayPersonSchema.setPayTypeFlag("");
			tLJAPayPersonSchema.setEnterAccDate(mCurrentDate);
			tLJAPayPersonSchema.setConfDate(mCurrentDate);
			tLJAPayPersonSchema.setMakeDate(mCurrentDate);
			tLJAPayPersonSchema.setMakeTime(mCurrentTime);
			tLJAPayPersonSchema.setModifyDate(mCurrentDate);
			tLJAPayPersonSchema.setModifyTime(mCurrentTime);

			mNewLJAPayPersonSet.add(tLJAPayPersonSchema);
		}

		mSubmitMMap.put(mNewLJTempFeeSet, "INSERT");
		mSubmitMMap.put(mNewLJTempFeeClassSet, "INSERT");
		mSubmitMMap.put(mNewLJAPaySet, "INSERT");
		mSubmitMMap.put(mNewLJAPayPersonSet, "INSERT");

		VData mVData = new VData();
		mVData.add(mSubmitMMap);
		PubSubmit mSubmit = new PubSubmit();
		if (!mSubmit.submitData(mVData, "")) {
			cLogger.error("汇总对账，自动恢复时数据库操作失败！");
			throw new MidplatException(mSubmit.mErrors.getFirstError());
		}

		cLogger.info("Out YBTAutoBack.RollBankWriteOff()!");
	}

	/**
	 * 单证核销！
	 * @throws MidplatException 
	 */
	private void certMakeUsed() throws MidplatException {
		cLogger.info("Into AIO_AutoWriteOff.certTakeBack()...");
		
		String mCertifyCode = "HB07/07B";
		cLogger.debug("单证类型码(CertifyCode)：" + mCertifyCode);
		cLogger.debug("待处理的单证号：" + cLBContSchema.getProposalContNo());
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		String mCurrentTime = DateUtil.getCurDate("HH:mm:ss");
		
		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(mCertifyCode);
		mLZCardDB.setStartNo(cLBContSchema.getProposalContNo());
		mLZCardDB.setEndNo(cLBContSchema.getProposalContNo());
		//要改成对帐后撤单的恢复,对账后撤单后单证状态为可用.8
        mLZCardDB.setStateFlag("8");	// 5-核销，2-作废 8-可用
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError()
				|| (null==mLZCardSet)
				|| (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到对应已核销的单证信息！");
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setStateFlag("5");	// 5-核销，2-作废
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(mCurrentDate);
		mLZCardSchema.setMakeTime(mCurrentTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mCertifyCode);
		mLZCardTrackSchema.setSubCode("0");
		mLZCardTrackSchema.setRiskCode("0");
		mLZCardTrackSchema.setRiskVersion("0");
		mLZCardTrackSchema.setStartNo(cLBContSchema.getProposalContNo());
		mLZCardTrackSchema.setEndNo(cLBContSchema.getProposalContNo());
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(1);
		mLZCardTrackSchema.setPrem(0);
		mLZCardTrackSchema.setAmnt(0);
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(cGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurrentDate);
		mLZCardTrackSchema.setMakeTime(mCurrentTime);
		mLZCardTrackSchema.setModifyDate(mCurrentDate);
		mLZCardTrackSchema.setModifyTime(mCurrentTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证核销处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证作废成功！");
		
		cLogger.info("Out AIO_AutoWriteOff.certTakeBack()!");
	}

	private void autoReceive(String pContNo) throws MidplatException {
		TransferData mTmpTransferData = new TransferData();
		mTmpTransferData.setNameAndValue("ContNo", pContNo);

		VData mContReceiveVData = new VData();
		mContReceiveVData.add(cGlobalInput);
		mContReceiveVData.add(mTmpTransferData);

		cLogger.info("开始回执…");
		ContReceiveUI mContReceiveUI = new ContReceiveUI();
		if (!mContReceiveUI.submitData(mContReceiveVData, "CreateReceive")) {
			throw new MidplatException(mContReceiveUI.mErrors.getFirstError());
		}		
		//如果成功生成接收轨迹，目前银保险种需要自动进行合同接收。
		if (!mContReceiveUI.submitData(mContReceiveVData, "ReceiveCont")) {
			throw new MidplatException(mContReceiveUI.mErrors.getFirstError());
		}
		cLogger.info("回执成功！");

		cLogger.info("开始回销…");
		LCContGetPolSchema mLCContGetPolSchema = new LCContGetPolSchema();
		mLCContGetPolSchema.setContNo(pContNo);
		mLCContGetPolSchema.setGetpolDate(DateUtil.getCurDate("yyyy-MM-dd"));
		mLCContGetPolSchema.setGetpolMan("YBT");
		mLCContGetPolSchema.setSendPolMan("YBT");
		mLCContGetPolSchema.setGetPolOperator("YBT");
		mLCContGetPolSchema.setManageCom(cGlobalInput.ManageCom);

		VData mContGetPolVData = new VData();
		mContGetPolVData.addElement(mLCContGetPolSchema);
		mContGetPolVData.add(cGlobalInput);

		LCContGetPolUI mLCContGetPolUI = new LCContGetPolUI();
		if (!mLCContGetPolUI.submitData(mContGetPolVData, "UPDATE")) {
			throw new MidplatException(mLCContGetPolUI.mErrors.getFirstError());
		}
		cLogger.info("回销成功！");
	}

	public static void main(String [] args) throws MidplatException{
		
		YBTAutoBack auto = new YBTAutoBack("000929913000001");
		
		auto.deal();
	}
}