package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.service.writeOff.YbtWriteOffBL;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ManualWriteOffUI {
	private final static Logger cLogger = Logger.getLogger(ManualWriteOffUI.class);
	
	private final String cContNo;
	private final GlobalInput cGlobalInput;
	
	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");
	
	public ManualWriteOffUI(String pContNo, GlobalInput pGlobalInput) throws MidplatException {
		if ((null==pContNo) || (pContNo.trim().equals(""))) {
			throw new MidplatException("保单合同号(ContNo)为空！");
		}
		
		if (null == pGlobalInput) {
			throw new MidplatException("GlobalInput为空！");
		}
		
		cContNo = pContNo.trim();
		cGlobalInput = pGlobalInput;
	}

	public void deal() throws Exception {
		cLogger.info("Into ManualWriteOffUI.deal()...");
		
		//校验保单状态
		String mSQL = "select AppFlag from lccont where ContNo='" + cContNo + "' with ur";
		if (!new ExeSQL().getOneValue(mSQL).equals("1")) {
			throw new MidplatException("保单状态有误！");
		}
		
		//查询保单类型,为方便下方做单证回收
		String mSQL1 = "select cardflag,salechnl from lccont where ContNo='" + cContNo + "' with ur";
		SSRS SR = new ExeSQL().execSQL(mSQL1);
		
		if(SR.getMaxRow()!=1){
			throw new MidplatException("查询保单失败！");
		}
	
		
		if (!SR.GetText(1, 1).equals("9") && !SR.GetText(1, 1).equals("c") && !SR.GetText(1, 1).equals("d")) {
			throw new MidplatException("保单渠道有误！");
		}
		
		//冲正处理
		YbtWriteOffBL mYbtWriteOffBL = new YbtWriteOffBL(cContNo, cGlobalInput);
		mYbtWriteOffBL.deal();
		
		//更新日志表中保单状态
		mSQL = "update LKTransStatus set Status='2', ModifyDate='" + cCurDate + "', ModifyTime='" + cCurTime + "' where FuncFlag='01' and Status='1' and polno='" + cContNo + "' with ur";
		ExeSQL mExeSQL = new ExeSQL();
		if (!mExeSQL.execUpdateSQL(mSQL)) {
			cLogger.error(mExeSQL.mErrors.getFirstError());
			throw new Exception("更新新契约日志中保单状态失败！");
      }
		
		//单证回收
		if(SR.GetText(1, 1).equals("9")){
		    mSQL = "select ProposalContNo from LBCont where ContNo='" + cContNo + "' with ur";
			certTakeBack(new ExeSQL().getOneValue(mSQL), cGlobalInput);
		}
		
		cLogger.info("Out ManualWriteOffUI.deal()!");
	}

   /**
    * 单证回收
    */
	private void certTakeBack(String pCardNo, GlobalInput pGlobalInput) throws MidplatException {
		cLogger.info("Into ManualWriteOffUI.certTakeBack()...");
		
		String mCertifyCode = "HB07/07B";
		cLogger.debug("单证类型码(CertifyCode)：" + mCertifyCode);
		cLogger.debug("待处理的单证号：" + pCardNo);
		
		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(mCertifyCode);
		mLZCardDB.setStartNo(pCardNo);
		mLZCardDB.setEndNo(pCardNo);
		mLZCardDB.setStateFlag("2");	// 5-核销，2-作废
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError()
				|| (null==mLZCardSet)
				|| (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到对应已核销的单证信息！");
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setStateFlag("0");	// 5-核销，2-作废
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(cCurDate);
		mLZCardSchema.setMakeTime(cCurTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mCertifyCode);
		mLZCardTrackSchema.setSubCode("0");
		mLZCardTrackSchema.setRiskCode("0");
		mLZCardTrackSchema.setRiskVersion("0");
		mLZCardTrackSchema.setStartNo(pCardNo);
		mLZCardTrackSchema.setEndNo(pCardNo);
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(1);
		mLZCardTrackSchema.setPrem(0);
		mLZCardTrackSchema.setAmnt(0);
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(pGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(cCurDate);
		mLZCardTrackSchema.setMakeTime(cCurTime);
		mLZCardTrackSchema.setModifyDate(cCurDate);
		mLZCardTrackSchema.setModifyTime(cCurTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证作废处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证作废成功！");
		
		cLogger.info("Out ManualWriteOffUI.certTakeBack()!");
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mContNo = "000929468000001";

		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.AgentCom = "";
		mGlobalInput.ManageCom = "86000000";
		mGlobalInput.ComCode = "86000000";
		mGlobalInput.Operator = "ybt";
		
		ManualWriteOffUI mManualWriteOffUI = new ManualWriteOffUI(
				mContNo, mGlobalInput);
		mManualWriteOffUI.deal();
		
		System.out.println("成功结束！");
	}
}