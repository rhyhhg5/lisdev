/**
 * 银保通险种停售。
 */

package com.sinosoft.midplat.kernel.management;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.YBTRiskSwtichDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class YBTStopSaleUI {
	private final static Logger cLogger = Logger.getLogger(YBTStopSaleUI.class);

	private final GlobalInput cGlobalInput;

	private final TransferData cTransferData;

	private final String cOperType;

	private final String cCurDate = DateUtil.getCurDate("yyyy-MM-dd");
	private final String cCurTime = DateUtil.getCurDate("HH:mm:ss");

	public YBTStopSaleUI(TransferData pTransferData, GlobalInput pGlobalInput,
			String pOperType) {
		cTransferData = pTransferData;
		cGlobalInput = pGlobalInput;
		cOperType = pOperType;
	}

	public void deal() throws Exception {
		cLogger.info("Into YBTStopSaleUI.deal()...");

		if (cOperType.equals("INSERT||MAIN")) {
			insert();
		} else if (cOperType.equals("UPDATE||MAIN")) {
			update();
		} else if (cOperType.equals("DEL||MAIN")) {
			delete();
		}

		cLogger.info("Out YBTStopSaleUI.deal()!");
	}

	private void insert() throws MidplatException {
		cLogger.info("Into YBTStopSaleUI.insert()...");

		YBTRiskSwtichDB mYBTRiskSwtichDB = new YBTRiskSwtichDB();
		String sql = "select max(id) from ybtriskswtich with ur";
		String sid = new ExeSQL().getOneValue(sql);
		if(null == sid || "".equals(sid)){//主要针对第一次插入数据
			sid = "0";
		}
		int id = Integer.parseInt(sid) + 1;
		mYBTRiskSwtichDB.setid(id);
		mYBTRiskSwtichDB.setbankcode((String)cTransferData.getValueByName("BankCode"));
		mYBTRiskSwtichDB.setcomcode((String)cTransferData.getValueByName("ComCode"));
		mYBTRiskSwtichDB.setriskcode((String)cTransferData.getValueByName("RiskCode"));
		mYBTRiskSwtichDB.setmainriskcode((String)cTransferData.getValueByName("MainRiskCode"));
		mYBTRiskSwtichDB.setdatatype((String)cTransferData.getValueByName("DataType"));
		mYBTRiskSwtichDB.setswtype((String)cTransferData.getValueByName("swtype"));
		mYBTRiskSwtichDB.setswvalue((String)cTransferData.getValueByName("hideSwvalue"));
		mYBTRiskSwtichDB.setstatus((String)cTransferData.getValueByName("Status"));
		mYBTRiskSwtichDB.setchannel((String)cTransferData.getValueByName("Channel"));
		mYBTRiskSwtichDB.setoperator((String)cTransferData.getValueByName("Operator"));
		mYBTRiskSwtichDB.setchangedate((String)cTransferData.getValueByName("StartDate"));
		mYBTRiskSwtichDB.setmakedate(DateUtil.getCur10Date());
		if (!mYBTRiskSwtichDB.insert()) {
			cLogger.error(mYBTRiskSwtichDB.mErrors.getFirstError());
			throw new MidplatException("新增信息失败！");
		}

		cLogger.info("Out YBTStopSaleUI.insert()!");
	}

	private void update() throws MidplatException {
		cLogger.info("Into YBTStopSaleUI.update()...");

		String bankCode = (String) cTransferData.getValueByName("BankCode");
		String comCode = (String) cTransferData.getValueByName("ComCode");
		String riskCode = (String) cTransferData.getValueByName("RiskCode");
		String dataType = (String) cTransferData.getValueByName("DataType");
		String swtype = (String) cTransferData.getValueByName("swtype");
		String hideSwvalue = (String) cTransferData.getValueByName("hideSwvalue");
		String Status = (String) cTransferData.getValueByName("Status");
		String Channel = (String) cTransferData.getValueByName("Channel");
		String operator = (String) cTransferData.getValueByName("Operator");
		String startDate = (String) cTransferData.getValueByName("StartDate");
		String ID = (String) cTransferData.getValueByName("ID");
		MMap mSubmitMMap = new MMap();
		//modifydate = bak1 上生产时改过来  
		String mSQLStr1 = "update YBTRiskSwtich set bak1 = '"+cCurDate+"'"
			+ getWherePart("BankCode",bankCode)
			+ getWherePart("comcode",comCode)
			+ getWherePart("riskcode",riskCode)
			+ getWherePart("datatype",dataType)
			+ getWherePart("swtype",swtype)
			+ getWherePart("swvalue",hideSwvalue)
			+ getWherePart("status",Status)
			+ getWherePart("channel",Channel)
			+ getWherePart("operator",operator)
			+ getWherePart("changedate",startDate)
			+ " where id = "+ID+" with ur";
		mSubmitMMap.put(mSQLStr1, "UPDATE");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("更新信息失败！");
		}
		cLogger.info("Out YBTStopSaleUI.update()!");
	}

	private static String getWherePart(String strName,String value){
		String str = "";
		if(null != value && !"".equals(value)){
			str = ","+strName + " = '" + value + "'";
		}
		return str;
	}
	
	private void delete() throws MidplatException {
		cLogger.info("Into YBTStopSaleUI.delete()...");
		String ID = (String) cTransferData.getValueByName("ID");

		String mSQLStr = "DELETE from YBTRiskSwtich where ID = "+ID+" with ur";
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mSQLStr, "DELETE");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("删除信息失败！");
		}

		cLogger.info("Out YBTStopSaleUI.delete()!");
	}
}
