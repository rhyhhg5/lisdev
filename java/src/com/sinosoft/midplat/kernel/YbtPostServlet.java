/**
 * 后置机与前置机通讯的接口，各交易的后置机入口程序。
 */

package com.sinosoft.midplat.kernel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.JdomUtil;
import com.sinosoft.midplat.common.cache.FileCacheManage;
import com.sinosoft.midplat.kernel.service.Service;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;

public class YbtPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger cLogger = Logger
			.getLogger(YbtPostServlet.class);

	public YbtPostServlet() {
		// 启动配置文件缓存管理系统
		try {
			FileCacheManage.newInstance().start();
			cLogger.info("缓存管理系统启动成功，开始工作..");
		} catch (IllegalThreadStateException ex) {
			cLogger.warn("缓存管理系统启动失败，很可能该系统已经被启动...");
		}
	}

	protected void doGet(HttpServletRequest pRequest,
			HttpServletResponse pResponse) throws ServletException, IOException {

		cLogger.debug("Into YbtPostServlet.doGet()...");
		pResponse.getOutputStream().println(
				"<h1>YbtPostServlet.doGet() is working well!</h1>" + "Now: "
						+ DateUtil.getCurDateTime());

		cLogger.info("Out YbtPostServlet.doGet()!");
	}

	protected void doPost(HttpServletRequest pRequest,
			HttpServletResponse pResponse) {
		cLogger.info("Into YbtPostServlet.doPost()...");
		Document mOutXmlDoc = null;

		try {

			InputStream tRequestIs = pRequest.getInputStream();
			Document tInXmlDoc = JdomUtil.build(tRequestIs);
			tRequestIs.close();
			Element tBaseInfo = tInXmlDoc.getRootElement().getChild("BaseInfo");

			List tBusiList = MidplatSufConf.newInstance().getConf()
					.getRootElement().getChildren("business");

			int tSize = tBusiList.size();
			Element tThisServEle = null;
			for (int i = 0; i < tSize; i++) {
				Element ttServiceEle = (Element) tBusiList.get(i);
				if (ttServiceEle.getChildText("bank").equals(
						tBaseInfo.getChildText("BankCode"))
						&& ttServiceEle.getChildText("funcFlag").equals(
								tBaseInfo.getChildText("FunctionFlag"))) {
					tThisServEle = ttServiceEle;
					break;
				}
				// 银保通改造，简化midplatSuf.xml配置文件
//				if ((ttServiceEle.getChildText("bank").equals(
//						tBaseInfo.getChildText("BankCode")) || ttServiceEle
//						.getChildText("serviceid").equals(
//								tBaseInfo.getChildText("ServiceId")))
//						&& ttServiceEle.getChildText("funcFlag").equals(
//								tBaseInfo.getChildText("FunctionFlag"))) {
//					tThisServEle = ttServiceEle;
//					break;
//				}
			}

			cLogger.info("服务类：" + tThisServEle.getChildText("service"));
			Constructor tServiceConstructor = Class.forName(
					tThisServEle.getChildText("service")).getConstructor(
					new Class[] { Element.class });
			Service tService = (Service) tServiceConstructor
					.newInstance(new Object[] { tThisServEle });
			mOutXmlDoc = tService.service(tInXmlDoc);
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
			mOutXmlDoc = YbtSufUtil.getSimpOutXml("0", ex.toString());
		}

		// Document to byte[]
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		try {
			OutputStream tResponseOs = pResponse.getOutputStream();
			mXMLOutputter.output(mOutXmlDoc, tResponseOs);
			tResponseOs.close();
		} catch (IOException ex) {
			cLogger.error("xml转换失败！", ex);
		}

		cLogger.info("Out YbtPostServlet.doPost()!");
	}
}
