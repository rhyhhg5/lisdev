/**
 * 银保通常用处理！(后置机专用)
 */

package com.sinosoft.midplat.kernel.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LKCodeMapping1DB;
import com.sinosoft.lis.db.LKCodeMappingDB;
import com.sinosoft.lis.db.LOMixBKAttributeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LOMixBKAttributeSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LOMixBKAttributeSet;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class YbtSufUtil {
	private static final Logger cLogger = Logger.getLogger(YbtSufUtil.class);
	
	/**
	 * 校验网点信息，生成GlobalInput。
	 * 很多lis核心调用需要GlobalInput。
	 */
	public static GlobalInput getGlobalInput(
			String pBankCode, String pZoneNo, String pBankNode) throws MidplatException {
		LKCodeMappingDB mLKCodeMappingDB = new LKCodeMappingDB();
		mLKCodeMappingDB.setBankCode(pBankCode);
		mLKCodeMappingDB.setZoneNo(pZoneNo);
		mLKCodeMappingDB.setBankNode(pBankNode);
		if (!mLKCodeMappingDB.getInfo()) {
			throw new MidplatException("未查到对应网点！BankCode=" + pBankCode
					+ "；ZoneNo=" + pZoneNo
					+ "；BankNode=" + pBankNode);
		}
		
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.AgentCom = mLKCodeMappingDB.getAgentCom();
		mGlobalInput.ManageCom = mLKCodeMappingDB.getManageCom();
		mGlobalInput.ComCode = mLKCodeMappingDB.getComCode();
		mGlobalInput.Operator = "ybt";
		return mGlobalInput;
	}
	
	/**
	 * 专门为信保通提供网点配置
	 */
	public static GlobalInput getGlobalInput1(
			String pBankCode, String pZoneNo, String pBankNode) throws MidplatException {
		LKCodeMapping1DB mLKCodeMapping1DB = new LKCodeMapping1DB();
		mLKCodeMapping1DB.setBankCode(pBankCode);
		mLKCodeMapping1DB.setZoneNo(pZoneNo);
		mLKCodeMapping1DB.setBankNode(pBankNode);
		if (!mLKCodeMapping1DB.getInfo()) {
			throw new MidplatException("未查到对应网点！BankCode=" + pBankCode
					+ "；ZoneNo=" + pZoneNo
					+ "；BankNode=" + pBankNode);
		}
		
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.AgentCom = mLKCodeMapping1DB.getAgentCom();
		mGlobalInput.ManageCom = mLKCodeMapping1DB.getManageCom();
		mGlobalInput.ComCode = mLKCodeMapping1DB.getComCode();
		mGlobalInput.Operator = "ybt";
		
		return mGlobalInput;
	}
	
	public static void ybtMixSale(GlobalInput pGlobalInput, String pContNo)
			throws Exception {
		cLogger.info("Into YbtSufUtil.ybtMixSale()!");

		String mAgentCom = pGlobalInput.AgentCom;
		// 根据agentcom判断是否是银保通交叉销售网点.如果此中介机构不在交叉定义表中,查询上级中介机构
		// 直到上级中介机构为空或在交叉销售定义表中查到记录为止
		while (mAgentCom != null && !"".equals(mAgentCom)) {
			String mSQL = "select 1 from LOMixBKAttribute where bandcode = '"
					+ mAgentCom + "' with ur";
			if ("1".equals(new ExeSQL().getOneValue(mSQL))) {
				// 更新表字段
				LOMixBKAttributeDB mLOMixBKAttributeDB = new LOMixBKAttributeDB();
				mLOMixBKAttributeDB.setBandCode(mAgentCom); // 此字段存储的是中介机构代码
				LOMixBKAttributeSet mLOMixBKAttributeSet = mLOMixBKAttributeDB.query();

				if (mLOMixBKAttributeSet.size() > 0) { // 此网点为交叉销售网点
					LOMixBKAttributeSchema mLOMixBKAttributeSchema = mLOMixBKAttributeSet
							.get(1);
					Date mNowDate = new Date();
					SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date mInvalidDate = DateFormat.parse(mLOMixBKAttributeSchema
							.getInvalidate());
					if (!mNowDate.after(mInvalidDate)) {
						cLogger.debug("交叉网点在有效期内,进行银保通交叉销售的更新.");
						LCContDB mLCContDB = new LCContDB();
						mLCContDB.setContNo(pContNo);
						mLCContDB.setAppFlag("1");
						if (!mLCContDB.getInfo()) {
							throw new MidplatException("未查询到保单信息!");
						}
						mLCContDB.setCrs_BussType(mLOMixBKAttributeSchema
								.getCrs_BussType());
						mLCContDB.setCrs_SaleChnl(mLOMixBKAttributeSchema
								.getCrs_SaleChnl());
						mLCContDB.setGrpAgentCode(mLOMixBKAttributeSchema
								.getGrpAgentCode());
						mLCContDB.setGrpAgentCom(mLOMixBKAttributeSchema
								.getGrpAgentCom());
						mLCContDB.setGrpAgentIDNo(mLOMixBKAttributeSchema
								.getGrpAgentIDNo());
						mLCContDB.setGrpAgentName(mLOMixBKAttributeSchema
								.getGrpAgentName());
						if (!mLCContDB.update()) {
							throw new MidplatException("更新保单信息失败!");
						}
					}
				}
				break;
			} else {
				mSQL = "select upagentcom from lacom where agentcom = '"
						+ mAgentCom + "' with ur";
				mAgentCom = new ExeSQL().getOneValue(mSQL);
			}
		}

		cLogger.info("Out YbtSufUtil.ybtMixSale()!");
}
	
	/**
	 * 根据pFlag和pErrorMsg，生成简单的标准返回报文。
	 */
	public static Document getSimpOutXml(String pFlag, String pErrorMsg) {
		Element mFlag = new Element("Flag");
		mFlag.addContent(pFlag);
		
		Element mDesc = new Element("Desc");
		mDesc.addContent(pErrorMsg);

		Element mRetData = new Element("RetData");
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		Element mTranData = new Element("TranData");
		mTranData.addContent(mRetData);
		
		return new Document(mTranData);
	}
	
	/**
	 * 根据投保书印刷号(PrtNo)清理数据
	 */
	public static void clearData(String pPrtNo) throws MidplatException {
		if ((null==pPrtNo) || pPrtNo.trim().equals("")) {
			cLogger.debug("pPrtNo = " + pPrtNo);
			throw new MidplatException("传入数据有误，投保书印刷号(PrtNo)不能为空！");
		}
		
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(pPrtNo);
		LCContSet mLCContSet =  mLCContDB.query();
		if (1 != mLCContSet.size()) {
			cLogger.warn("根据投保书印刷号(PrtNo)查询到的需清理保单数：" + mLCContSet.size());
			return;
		}
		LCContSchema mLCContSchema = mLCContSet.get(1);
		
		MMap mDelMMap = new MMap();
		mDelMMap.put("delete from LCRiskDutyWrap where contno='" + mLCContSchema.getProposalContNo() + "'", "DELETE");
		mDelMMap.put("delete from LWMission where MissionProp1='" + mLCContSchema.getContNo() + "'", "DELETE");
		// mDelMMap.put("delete from LACommisionDetail where grpcontno='" + mContNo + "'", "DELETE"); //PICCH承保未用到此表
		mDelMMap.put("delete from LCCustomerImpart where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCCustomerImpartParams where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from lcpol where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from lccont where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCAppnt where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCInsured where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCSpec where polno in (select polno from lcpol where contno='"	+ mLCContSchema.getContNo() + "')", "DELETE");
		mDelMMap.put("delete from LCPrem where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCGet where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCDuty where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCBnf where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from LCCUWMaster where contno='" + mLCContSchema.getContNo() + "'",	"DELETE");
		mDelMMap.put("delete from LCCUWSub where contno='" + mLCContSchema.getContNo() + "'",	"DELETE");
		mDelMMap.put("delete from LJTempFee where tempfeeno='" + mLCContSchema.getTempFeeNo() + "'", "DELETE");
		mDelMMap.put("delete from LJTempFeeClass where tempfeeno='" + mLCContSchema.getTempFeeNo() + "'", "DELETE");
		mDelMMap.put("delete from LJAPay where incomeno='" + mLCContSchema.getContNo() + "'",	"DELETE");
		mDelMMap.put("delete from LJAPayPerson where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		mDelMMap.put("delete from lcriskdutywrap where contno='" + mLCContSchema.getContNo() + "'", "DELETE");
		// mapDel.put("delete from ljaget where otherno in (select prtno from lccont where contno='" + mContNo + "')", "DELETE"); //PICCH承保未用到此表
		// mapDel.put("delete from ljagetother where otherno in (select prtno from lccont where contno='" + mContNo + "')", "DELETE");//PICCH承保未用到此表
		// mapDel.put("delete from LCContState where contno='" + mContNo + "'", "DELETE"); //PICCH承保未用到此表
		mDelMMap.put("delete from lcinsureacc where contno='" + mLCContSchema.getContNo() + "'",	"DELETE"); // 签单才用到此表
		mDelMMap.put("delete from lcinsureaccclass where contno='" + mLCContSchema.getContNo() + "'", "DELETE"); // 签单才用到此表
		mDelMMap.put("delete from lcinsureaccclassfee where contno='" + mLCContSchema.getContNo() + "'", "DELETE");// 签单才用到此表
		mDelMMap.put("delete from lcinsureaccfee where contno='" + mLCContSchema.getContNo() + "'", "DELETE"); // 签单才用到此表
		mDelMMap.put("delete from lcinsureacctrace where contno='" + mLCContSchema.getContNo() + "'", "DELETE"); // 签单才用到此表
        mDelMMap.put("delete from LCInsureAccFeeTrace where contno='" + mLCContSchema.getContNo() + "'", "DELETE"); // 签单才用到此表
        mDelMMap.put("delete from lccontreceive where contno='" + mLCContSchema.getContNo() + "'", "DELETE"); // 删除回执回销记录
		mDelMMap.put("delete from lccontgetpol where contno='" + mLCContSchema.getContNo() + "'", "DELETE"); // 删除回执回销记录
		mDelMMap.put("delete from LCRiskFeeRate where prtno='" + pPrtNo + "'", "DELETE"); // 删除万能H款比例记录
		mDelMMap.put("delete from lccontsub where prtno='" + pPrtNo + "'", "DELETE"); // 删除投保人家庭年收入记录
		
		VData mSubmitVData = new VData();
		mSubmitVData.add(mDelMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("清除保单数据失败！");
		}
	}
}
