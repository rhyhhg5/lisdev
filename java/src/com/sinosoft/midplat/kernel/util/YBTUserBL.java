package com.sinosoft.midplat.kernel.util;

import com.sinosoft.lis.db.LCYBTCustomerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCYBTCustomerSchema;
import com.sinosoft.lis.vschema.LCYBTCustomerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class YBTUserBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局数据 */
	private Reflections ref = new Reflections();

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap map = new MMap();

	// 统一更新日期，时间
	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();


	/** 业务处理相关变量 */
	private LCYBTCustomerSchema mLCYBTCustomerSchema = new LCYBTCustomerSchema();
	
	private LCYBTCustomerSet mLCYBTCustomerSet = new LCYBTCustomerSet();
	

	private String mSavePolType = "";

	private String GrpNo = "";

	private String GrpName = "";


	private boolean updateldperson = false;

	// @Constructor
	public YBTUserBL() {
	}

	/**
	 * 数据提交的公共方法
	 *
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将传入的数据拷贝到本类中
		mInputData = (VData) cInputData.clone();
		System.out.println(mInputData);
		this.mOperate = cOperate;
		System.out.println("now in YBTUserBL submit");
		// 将外部传入的数据分解到本类的属性中，准备处理
		if (this.getInputData() == false) {
			return false;
		}
		System.out.println("---getInputData---");
		
		if (this.checkData() == false)
        {
            return false;
        }
        System.out.println("---checkData---");

        if (this.dealData() == false)
        {
            return false;
        }
		System.out.println("---dealData---");

		// 装配处理好的数据，准备给后台进行保存
		this.prepareOutputData();
		System.out.println("---prepareOutputData---");

		// 　数据提交、保存

		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start Submit...");

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "YBTUserBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		System.out.println("---commitData---");

		
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 *
	 * @return boolean
	 */
	private boolean getInputData() {
		try {
			//银保通授权客户表
			mLCYBTCustomerSet = ((LCYBTCustomerSet) mInputData.getObjectByObjectName("LCYBTCustomerSet", 0));
			if (mOperate.equals("UPDATE||CONT")) {
				LCYBTCustomerDB tLCYBTCustomerDB = new LCYBTCustomerDB();
				tLCYBTCustomerDB.setAuthorizeNo(mLCYBTCustomerSchema.getAuthorizeNo());
				if (tLCYBTCustomerDB.getInfo() == false) {
					// @@错误处理
					this.mErrors.copyAllErrors(tLCYBTCustomerDB.mErrors);
					return false;
				}
			}

			return true;
		} catch (Exception ex) {
			CError tError = new CError();
			tError.moduleName = "YBTYserBL";
			tError.functionName = "checkData";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);
			return false;

		}

	}

	/**
	 * 校验传入的数据
	 *
	 * @return boolean
	 */
	private boolean checkData() {
		LCYBTCustomerSchema tLCYBTCustomerSchema = new LCYBTCustomerSchema();
		tLCYBTCustomerSchema.setSchema(mLCYBTCustomerSet.get(1));
		
		if(!mOperate.equals("INSERT")){
			if(tLCYBTCustomerSchema.getAuthorizeNo()==null 
					|| tLCYBTCustomerSchema.getAuthorizeNo().trim().equals("")){
				// @@错误处理
                CError tError = new CError();
                tError.moduleName = "YBTUserBL";
                tError.functionName = "checkData";
                tError.errorMessage = "未查询到数据!";
                this.mErrors.addOneError(tError);
                return false;
			}
		}
		if (mOperate.equals("INSERT")) {
			System.out.println("******INSERT*****");
//			if(tLCYBTCustomerSchema.getAuthorizeNo()!=null 
//				|| !("").equals(tLCYBTCustomerSchema.getAuthorizeNo())){
//				// @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "YBTUserBL";
//                tError.functionName = "checkData";
//                tError.errorMessage = "已经保存的数据只能进行修改!!";
//                this.mErrors.addOneError(tError);
//                return false;
//			}
			//检验录入有效日期
			
			
        }
		
		
		return true;
	}

	/**
	 * 根据业务逻辑对数据进行处理
	 *
	 * @return boolean
	 */
	private boolean dealData() {
		mLCYBTCustomerSchema.setSchema(mLCYBTCustomerSet.get(1));
		System.out.println("aaaa" + mLCYBTCustomerSchema.getAuthorizeNo());
		mLCYBTCustomerSchema.setModifyDate(theCurrentDate);
		mLCYBTCustomerSchema.setModifyTime(theCurrentTime);
		// 将客户身份证号码中的x转换成大写
		if (mLCYBTCustomerSchema.getIDType() != null && mLCYBTCustomerSchema.getIDNo() != null) {
			if (mLCYBTCustomerSchema.getIDType().equals("0")) {
				String tLCYBTCustomerIdNo = mLCYBTCustomerSchema.getIDNo().toUpperCase();
				mLCYBTCustomerSchema.setIDNo(tLCYBTCustomerIdNo);
			}
		}
		mLCYBTCustomerSchema.setState("1");
		if(mOperate.equals("INSERT")){
			LCYBTCustomerDB lcybtCustomerDB = new LCYBTCustomerDB();
			String sql = "select * from LCYBTCustomer where Name='"+mLCYBTCustomerSchema.getName()+"' and Sex='"+mLCYBTCustomerSchema.getSex()+"' "
					+ "and IDType='"+mLCYBTCustomerSchema.getIDType()+"' and IDNo='"+mLCYBTCustomerSchema.getIDNo()+"' and Birthday='"+mLCYBTCustomerSchema.getBirthday()+"' and effectivedate >= current date order by effectivedate desc  ";
			LCYBTCustomerSet lcybtCustomerSet =  lcybtCustomerDB.executeQuery(sql);
			if(lcybtCustomerSet.size()<=0){
			String tNo = "";
			tNo = PubFun1.CreateMaxNo("AUTHORIZENO", "SN");
			mLCYBTCustomerSchema.setAuthorizeNo(tNo);
			mLCYBTCustomerSchema.setMakeDate(theCurrentDate);
			mLCYBTCustomerSchema.setMakeTime(theCurrentTime);
			map.put(mLCYBTCustomerSchema, mOperate);
			}else{
				  CError tError = new CError();
	                tError.moduleName = "YBTUserBL";
	                tError.functionName = "checkData";
	                tError.errorMessage = "该用户登记尚在有效期内,不允许新增！";
	                this.mErrors.addOneError(tError);
	                return false;
			}
		}
		if(mOperate.equals("UPDATE")){
			 ExeSQL tExeSQL = new ExeSQL();
             SSRS tSSRS = new SSRS();
             tSSRS = tExeSQL
                     .execSQL("select MakeDate,MakeTime from LCYBTCustomer where AuthorizeNo='"
                             + mLCYBTCustomerSchema.getAuthorizeNo()
                             + "' with ur");
             String MakeDate = tSSRS.GetText(1, 1);
             String MakeTime = tSSRS.GetText(1, 2);
             mLCYBTCustomerSchema.setMakeDate(MakeDate);
             mLCYBTCustomerSchema.setMakeTime(MakeTime);
             map.put(mLCYBTCustomerSchema, mOperate);
		}
		if(mOperate.equals("DELETE")){
			map.put(mLCYBTCustomerSchema, mOperate);
		}
		return true;
	}
	/**
     * 根据业务逻辑对数据进行处理
     *
     */
    private void prepareOutputData()
    {
        mInputData.clear();
        mInputData.add(map);
        mResult.clear();
        mResult.add(mLCYBTCustomerSet);
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult()
    {
        return mResult;
    }
	
	
	
}
