package com.sinosoft.midplat.kernel.util;

import com.sinosoft.utility.CErrors;

public class CustomerInforUI {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	// @Constructor
	public CustomerInforUI() {
	}


	/**
	 * 数据提交方法
	 * 
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @return: boolean
	 **/
	public boolean submitData(String prtno) {
		// 数据操作字符串拷贝到本类
		System.out.println("---UI BEGIN---");
		CustomerInforBL customerInforBL = new CustomerInforBL();
		if (customerInforBL.submitData(prtno) == false) {
			// @@错误处理
			this.mErrors.copyAllErrors(customerInforBL.mErrors);
			return false;
		}
		return true;
	}

}
