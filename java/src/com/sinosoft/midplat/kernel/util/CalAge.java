package com.sinosoft.midplat.kernel.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CalAge {
	
	/**
	 * 计算投保人年龄
	 * */
    public static int getAppntAge (String ageDate) {
    	
    	Date now = new Date();
		String sdf = new SimpleDateFormat("yyyyMMdd").format(now);
		
		int age = 0;
		int staY = Integer.parseInt(ageDate.substring(0,4));
		int staM = Integer.parseInt(ageDate.substring(4,6));
		int staD = Integer.parseInt(ageDate.substring(6,8));
		
		int nowY = Integer.parseInt(sdf.substring(0,4));
		int nowM = Integer.parseInt(sdf.substring(4,6));
		int nowD = Integer.parseInt(sdf.substring(6,8));
		
		if(staM > nowM){
			age = nowY - staY -1;
		}else if(staM == nowM) {
			if(nowD > staD){
				age = nowY - staY ;
			}else if(nowD == staD) {
				age = nowY - staY ;
			}else if(nowD < staD) {
				age = nowY - staY -1 ;
			}
				
		}else if(staM < nowM) {
			age = nowY - staY ;
		}
		
    	return age;
    }

}
