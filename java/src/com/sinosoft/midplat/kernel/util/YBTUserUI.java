package com.sinosoft.midplat.kernel.util;

import com.sinosoft.lis.schema.LCYBTCustomerSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class YBTUserUI {

	/** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 往前面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	// @Constructor
	public YBTUserUI() {
	}

	// @Main
	public static void main(String[] args) {
		YBTUserUI tYBTUserUI = new YBTUserUI();
		LCYBTCustomerSchema mLCYBTCustomerSchema = new LCYBTCustomerSchema();
		mLCYBTCustomerSchema.setName("zs");
		mLCYBTCustomerSchema.setSex("0");
		mLCYBTCustomerSchema.setIDType("4");
		mLCYBTCustomerSchema.setIDNo("111111");
		mLCYBTCustomerSchema.setEffectiveDate("20180126");

		VData tInputData = new VData();
		tInputData.add(mLCYBTCustomerSchema);
		tYBTUserUI.submitData(tInputData, "INSERT||CONT");
	}

	// @Method
	/**
	 * 数据提交方法
	 * 
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @return: boolean
	 **/
	public boolean submitData(VData cInputData, String cOperate) {
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		YBTUserBL tYBTUserBL = new YBTUserBL();

		System.out.println("---UI BEGIN---");
		if (tYBTUserBL.submitData(cInputData, mOperate) == false) {
			// @@错误处理
			this.mErrors.copyAllErrors(tYBTUserBL.mErrors);
			return false;
		} else {
			mResult = tYBTUserBL.getResult();
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

}
