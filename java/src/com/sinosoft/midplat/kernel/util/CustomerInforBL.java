package com.sinosoft.midplat.kernel.util;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCYBTCustomerDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCYBTCustomerSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCYBTCustomerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class CustomerInforBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	// @Constructor
	public CustomerInforBL() {
	}

	/**
	 * 数据提交的公共方法
	 *
	 * @param: cInputData 传入的数据 cOperate 数据操作字符串
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(String prtno) {
		// 将传入的数据拷贝到本类中
		System.out.println("now in CustomerInforBL submit");
       LCContDB lcContDB = new LCContDB();
       lcContDB.setPrtNo(prtno);
       LCContSet lcContSet =  lcContDB.query();
       if(lcContSet.size()<=0){
    	   CError tError = new CError();
			tError.moduleName = "CustomerInforBL";
			tError.functionName = "submitData";
			tError.errorMessage = "查询数据出错!";
			this.mErrors.addOneError(tError);
			return false; 
       }
       LCContSchema lcContSchema = lcContSet.get(1);
       LCYBTCustomerDB lcybtCustomerDB = new LCYBTCustomerDB();
       lcybtCustomerDB.setName(lcContSchema.getInsuredName());
       lcybtCustomerDB.setSex(lcContSchema.getInsuredSex());
       lcybtCustomerDB.setBirthday(lcContSchema.getInsuredBirthday());
       lcybtCustomerDB.setIDNo(lcContSchema.getInsuredIDNo());
       lcybtCustomerDB.setIDType(lcContSchema.getInsuredIDType());
       lcybtCustomerDB.setAuthorizeNo(lcContSet.get(1).getContNo());
       
       LCYBTCustomerSet lcybtCustomerSet =  lcybtCustomerDB.query();
       if(lcybtCustomerSet.size()<=0){
    	    CError tError = new CError();
			tError.moduleName = "CustomerInforBL";
			tError.functionName = "submitData";
			tError.errorMessage = "客户未进行登记授权！";
			this.mErrors.addOneError(tError);
			return false; 
       }
        LCYBTCustomerSchema lcybtCustomerSchema = lcybtCustomerSet.get(1);
        lcybtCustomerSchema.setState("3");
        lcybtCustomerSchema.setMakeDate(PubFun.getCurrentDate());
        lcybtCustomerSchema.setModifyTime(PubFun.getCurrentTime());
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start Submit...");
		String mOperate = "UPDATE";
	
		 VData mInputData = new VData();
		  MMap map = new MMap();
		  map.put(lcybtCustomerSchema, mOperate);
		 mInputData.add(map);
		if (!tPubSubmit.submitData(mInputData, "")) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "CustomerInforBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}

		System.out.println("---commitData---");

		
		return true;
	}
	
}
