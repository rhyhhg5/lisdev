package com.sinosoft.midplat.kernel.util;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.sinosoft.lis.certify.CertTakeBackUI;
import com.sinosoft.lis.certify.CertifyFunc;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class CardManage {
	private static final Logger cLogger = Logger.getLogger(CardManage.class);
	
	private final GlobalInput cGlobalInput;
	private final String cCertifyCode; 
	private final String cCertifyClass;
	
	public CardManage(String pCertifyCode, GlobalInput pGlobalInput) {
		cCertifyCode = pCertifyCode;
		cGlobalInput = pGlobalInput;
		String mSQL = "select certifyclass from lmcertifydes where CertifyCode in ('" + cCertifyCode + "')";
		cCertifyClass = new ExeSQL().getOneValue(mSQL);
	} 
	
	/**
	 * D:定额; P:普通
	 */
	public String getCardType() {
		return cCertifyClass;
	}
	
	public boolean canBeUsed(String pCardNo) {
		cLogger.info("Into CardManage.canBeUsed()...");
		
		boolean mUsedFlag = false;
		
		if ("P".equals(cCertifyClass)) {	//普通单证
			mUsedFlag =  canBeUsed4P(pCardNo);
		} else if ("D".equals(cCertifyClass)) {	//定额单证
			mUsedFlag =  canBeUsed4D(pCardNo);
		}
		
		cLogger.info("Out CardManage.canBeUsed()!");
		return mUsedFlag;
	}
	
	/**
	 * 单证核销
	 */
	public void makeUsed(String pCardNo) throws MidplatException {
		cLogger.info("Into CardManage.makeUsed()...");
		
		if ("P".equals(cCertifyClass)) {	//普通单证
			makeUsed4P(pCardNo);
		} else if ("D".equals(cCertifyClass)) {	//定额单证
			makeUsed4D(pCardNo);
		} else {
			throw new MidplatException("单证类别有误！" + cCertifyClass);
		}
		
		cLogger.info("Out CardManage.makeUsed()!");
	}
	
	/**
	 * 单证作废
	 */
	public void invalidated(String pCardNo) throws MidplatException {
		cLogger.info("Into CardManage.invalidated()...");
		
		if ("P".equals(cCertifyClass)) {	//普通单证
			invalidated4P(pCardNo);
		} else if ("D".equals(cCertifyClass)) {	//定额单证
			invalidated4D(pCardNo);
		} else {
			throw new MidplatException("单证类别有误！" + cCertifyClass);
		}
		
		cLogger.info("Out CardManage.invalidated()!");
	}
	
	/**
	 * 单证回收
	 */
	public void tackBack(String pCardNo) throws MidplatException {
		cLogger.info("Into CardManage.tackBack()...");
		
		if ("P".equals(cCertifyClass)) {	//普通单证
			tackBack4P(pCardNo);
		} else if ("D".equals(cCertifyClass)) {	//定额单证
			tackBack4D(pCardNo);
		} else {
			throw new MidplatException("单证类别有误！" + cCertifyClass);
		}
		
		cLogger.info("Out CardManage.tackBack()!");
	}
	
	private boolean canBeUsed4P(String pCardNo) {
		cLogger.info("Into CardManage.canBeUsed4P()...");

		cLogger.debug("CardNo = " + pCardNo);
//		String mSQL = "select stateflag from lzcard where certifyCode='" + cCertifyCode + "' "
		String mSQL = "select stateflag from lzcard where 1=1 "
			+ "and startno<='" + pCardNo + "' "
			+ "and endno>='" + pCardNo + "' "
			+ "and ReceiveCom='E" + cGlobalInput.AgentCom.substring(0, 8) + "' "
			+ "with ur";
		if (!"8".equals(new ExeSQL().getOneValue(mSQL))) {	//11-可用; 2-正常回销; 6-作废
			return false;
		}

		cLogger.info("Out CardManage.canBeUsed4P()!");
		return true;
	}
	
	/**
	 * 普通单证核销
	 */
	private void makeUsed4P(String pCardNo) throws MidplatException {
		cLogger.debug("Into CardManage.makeUsed4P()...");
		
		cLogger.debug("CertifyClass=" + cCertifyClass + "; CertifyCode=" + cCertifyCode + "; CardNo=" + pCardNo);
		
		String mSQL = "select * from LZCARD where 1=1 "
//			+ "and CertifyCode='" + cCertifyCode + "' "
			+ "and ReceiveCom='E" + cGlobalInput.AgentCom.substring(0, 8) + "' "
			+ "and startno<='" + pCardNo + "' "
			+ "and endno>='" + pCardNo + "' "
			+ "with ur";
		cLogger.info("SQL：" + mSQL);
		LZCardSet mLZCardSet = new LZCardDB().executeQuery(mSQL);
		if ((null==mLZCardSet) || (mLZCardSet.size()<1)) {
			throw new MidplatException("未查到相应单证！" + pCardNo);
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);

		LZCardSchema mUpdateLZCardSchema = new LZCardSchema();
		mUpdateLZCardSchema.setCertifyCode(cCertifyCode);
		mUpdateLZCardSchema.setCertifyCode(mLZCardSchema.getCertifyCode());
		mUpdateLZCardSchema.setStartNo(pCardNo);
		mUpdateLZCardSchema.setEndNo(pCardNo);
		mUpdateLZCardSchema.setStateFlag("5"); //5-核销，2-作废
		mUpdateLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mUpdateLZCardSchema.setReceiveCom("SYS");
		mUpdateLZCardSchema.setSumCount(0);
		mUpdateLZCardSchema.setPrem(0);
		mUpdateLZCardSchema.setAmnt(0);
		mUpdateLZCardSchema.setHandler(mLZCardSchema.getHandler());
		mUpdateLZCardSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mUpdateLZCardSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		
		LZCardSet mUpdateLZCardSet = new LZCardSet();
		mUpdateLZCardSet.add(mUpdateLZCardSchema);
		
		Hashtable hashParams = new Hashtable();
		hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY); // 普通单证
		
		VData mCertTakeBackVData = new VData();
		mCertTakeBackVData.addElement(cGlobalInput);
		mCertTakeBackVData.addElement(mUpdateLZCardSet);
		mCertTakeBackVData.addElement(hashParams);
		
		cLogger.info("开始核销...");
		CertTakeBackUI mCertTakeBackUI = new CertTakeBackUI();
		if (!mCertTakeBackUI.submitData(mCertTakeBackVData, "HEXIAO")) {
			cLogger.error("核销失败！" + mCertTakeBackUI.mErrors.getFirstError());
			throw new MidplatException("核销失败！" + mCertTakeBackUI.mErrors.getFirstError());
		}
		cLogger.info("核销成功！");
		
		cLogger.debug("Out CardManage.makeUsed4P()!");
	}
	
	/**
	 * 普通单证作废
	 */
	private void invalidated4P(String pCardNo) throws MidplatException {
		cLogger.debug("Into CardManage.invalidated4P()...");
		
		cLogger.debug("CertifyClass=" + cCertifyClass + "; CertifyCode=" + cCertifyCode + "; CardNo=" + pCardNo);
		
		String mCurDate = DateUtil.getCur10Date();
		String mCurTime = DateUtil.getCur8Time();
		
//		LZCardDB mLZCardDB = new LZCardDB();
////		mLZCardDB.setCertifyCode(cCertifyCode);
//		mLZCardDB.setStartNo(pCardNo);
//		mLZCardDB.setEndNo(pCardNo);
//		LZCardSet mLZCardSet = mLZCardDB.query();
		String mSQL = "select * from LZCARD where 1=1 "
//			+ "and CertifyCode='" + cCertifyCode + "' "
			+ "and certifycode in ('HB07/07B','YB12/10B') "
			+ "and startno='"+pCardNo+"' "
			+ "and endno='"+pCardNo+"' "
			+ "with ur";
		//cLogger.info("SQL：" + mSQL);
		LZCardSet mLZCardSet = new LZCardDB().executeQuery(mSQL);
		if ((null==mLZCardSet) || (mLZCardSet.size()<1)) {
			throw new MidplatException("未查到相应单证！" + pCardNo);
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setStateFlag("2");	//5-核销，2-作废
		mLZCardSchema.setModifyDate(mCurDate);
		mLZCardSchema.setMakeTime(mCurTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mLZCardSchema.getCertifyCode());
		mLZCardTrackSchema.setSubCode(mLZCardSchema.getSubCode());
		mLZCardTrackSchema.setRiskCode(mLZCardSchema.getRiskCode());
		mLZCardTrackSchema.setRiskVersion(mLZCardSchema.getRiskVersion());
		mLZCardTrackSchema.setStartNo(mLZCardSchema.getStartNo());
		mLZCardTrackSchema.setEndNo(mLZCardSchema.getEndNo());
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(mLZCardSchema.getSumCount());
		mLZCardTrackSchema.setPrem(mLZCardSchema.getPrem());
		mLZCardTrackSchema.setAmnt(mLZCardSchema.getAmnt());
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(cGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurDate);
		mLZCardTrackSchema.setMakeTime(mCurTime);
		mLZCardTrackSchema.setModifyDate(mCurDate);
		mLZCardTrackSchema.setModifyTime(mCurTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证作废处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证作废成功！");
		
		cLogger.debug("Out CardManage.invalidated4P()!");
	}
	
	/**
	 * 普通单证回收
	 */
	private void tackBack4P(String pCardNo) throws MidplatException {
		cLogger.debug("Into CardManage.tackBack4P()...");
		
		cLogger.debug("CertifyClass=" + cCertifyClass + "; CertifyCode=" + cCertifyCode + "; CardNo=" + pCardNo);
		
		String mCurDate = DateUtil.getCur10Date();
		String mCurTime = DateUtil.getCur8Time();
		
//		LZCardDB mLZCardDB = new LZCardDB();
////		mLZCardDB.setCertifyCode(cCertifyCode);
//		mLZCardDB.setStartNo(pCardNo);
//		mLZCardDB.setEndNo(pCardNo);
//		LZCardSet mLZCardSet = mLZCardDB.query();
//		if (mLZCardDB.mErrors.needDealError()
//				|| (null==mLZCardSet)
//				|| (1!=mLZCardSet.size())) {
//			cLogger.error(mLZCardDB.mErrors.getFirstError());
//			throw new MidplatException("未查到相应单证！" + pCardNo);
//		}
		String mSQL = "select * from LZCARD where 1=1 "
//			+ "and CertifyCode='" + cCertifyCode + "' "
			+ "and certifycode in ('HB07/07B','YB12/10B') "
			+ "and startno='"+pCardNo+"' "
			+ "and endno='"+pCardNo+"' "
			+ "with ur";
		//cLogger.info("SQL：" + mSQL);
		LZCardSet mLZCardSet = new LZCardDB().executeQuery(mSQL);
		if ((null==mLZCardSet) || (mLZCardSet.size()<1)) {
			throw new MidplatException("未查到相应单证！" + pCardNo);
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setStateFlag("8");	// 5-核销，2-作废
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(mCurDate);
		mLZCardSchema.setMakeTime(mCurTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mLZCardSchema.getCertifyCode());
		mLZCardTrackSchema.setSubCode(mLZCardSchema.getSubCode());
		mLZCardTrackSchema.setRiskCode(mLZCardSchema.getRiskCode());
		mLZCardTrackSchema.setRiskVersion(mLZCardSchema.getRiskVersion());
		mLZCardTrackSchema.setStartNo(mLZCardSchema.getStartNo());
		mLZCardTrackSchema.setEndNo(mLZCardSchema.getEndNo());
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(mLZCardSchema.getSumCount());
		mLZCardTrackSchema.setPrem(mLZCardSchema.getPrem());
		mLZCardTrackSchema.setAmnt(mLZCardSchema.getAmnt());
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(cGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurDate);
		mLZCardTrackSchema.setMakeTime(mCurTime);
		mLZCardTrackSchema.setModifyDate(mCurDate);
		mLZCardTrackSchema.setModifyTime(mCurTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证回收处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证回收成功！");
		
		cLogger.debug("Out CardManage.tackBack4P()!");
	}
	
	private boolean canBeUsed4D(String pCardNo) {
		cLogger.info("Into CardManage.canBeUsed4D()...");

		cLogger.debug("CardNo = " + pCardNo);
		String mSQL = "select state from lzcard where certifyCode='" + cCertifyCode + "' "
			+ "and startno='" + pCardNo + "' "
			+ "and endno='" + pCardNo + "' "
			+ "and ReceiveCom='E" + cGlobalInput.AgentCom + "' "
			+ "with ur";
		if (!"11".equals(new ExeSQL().getOneValue(mSQL))) {	//11-可用; 2-正常回销; 6-作废
			return false;
		}

		cLogger.info("Out CardManage.canBeUsed4D()!");
		return true;
	}
	
	/**
	 * 定额单证核销
	 */
	private void makeUsed4D(String pCardNo) throws MidplatException {
		cLogger.debug("Into CardManage.makeUsed4D()...");
		
		cLogger.debug("CertifyClass=" + cCertifyClass + "; CertifyCode=" + cCertifyCode + "; CardNo=" + pCardNo);
		
		String mCurDate = DateUtil.getCur10Date();
		String mCurTime = DateUtil.getCur8Time();
		
		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(cCertifyCode);
		mLZCardDB.setStartNo(pCardNo);
		mLZCardDB.setEndNo(pCardNo);
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError()
				|| (null==mLZCardSet)
				|| (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到相应单证！" + pCardNo);
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setState("2");	//11-可用; 2-正常回销; 6-作废
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(mCurDate);
		mLZCardSchema.setMakeTime(mCurTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mLZCardSchema.getCertifyCode());
		mLZCardTrackSchema.setSubCode(mLZCardSchema.getSubCode());
		mLZCardTrackSchema.setRiskCode(mLZCardSchema.getRiskCode());
		mLZCardTrackSchema.setRiskVersion(mLZCardSchema.getRiskVersion());
		mLZCardTrackSchema.setStartNo(mLZCardSchema.getStartNo());
		mLZCardTrackSchema.setEndNo(mLZCardSchema.getEndNo());
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(mLZCardSchema.getSumCount());
		mLZCardTrackSchema.setPrem(mLZCardSchema.getPrem());
		mLZCardTrackSchema.setAmnt(mLZCardSchema.getAmnt());
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(cGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurDate);
		mLZCardTrackSchema.setMakeTime(mCurTime);
		mLZCardTrackSchema.setModifyDate(mCurDate);
		mLZCardTrackSchema.setModifyTime(mCurTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证核销处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证核销成功！");
		
		cLogger.debug("Out CardManage.makeUsed4D()!");
	}
	
	/**
	 * 定额单证作废
	 */
	private void invalidated4D(String pCardNo) throws MidplatException {
		cLogger.debug("Into CardManage.invalidated4D()...");
		
		cLogger.debug("CertifyClass=" + cCertifyClass + "; CertifyCode=" + cCertifyCode + "; CardNo=" + pCardNo);
		
		String mCurDate = DateUtil.getCur10Date();
		String mCurTime = DateUtil.getCur8Time();
		
		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(cCertifyCode);
		mLZCardDB.setStartNo(pCardNo);
		mLZCardDB.setEndNo(pCardNo);
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError() || (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到相应单证！" + pCardNo);
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setState("6");	//11-可用; 2-正常回销; 6-作废
		mLZCardSchema.setModifyDate(mCurDate);
		mLZCardSchema.setMakeTime(mCurTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mLZCardSchema.getCertifyCode());
		mLZCardTrackSchema.setSubCode(mLZCardSchema.getSubCode());
		mLZCardTrackSchema.setRiskCode(mLZCardSchema.getRiskCode());
		mLZCardTrackSchema.setRiskVersion(mLZCardSchema.getRiskVersion());
		mLZCardTrackSchema.setStartNo(mLZCardSchema.getStartNo());
		mLZCardTrackSchema.setEndNo(mLZCardSchema.getEndNo());
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(mLZCardSchema.getSumCount());
		mLZCardTrackSchema.setPrem(mLZCardSchema.getPrem());
		mLZCardTrackSchema.setAmnt(mLZCardSchema.getAmnt());
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(cGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurDate);
		mLZCardTrackSchema.setMakeTime(mCurTime);
		mLZCardTrackSchema.setModifyDate(mCurDate);
		mLZCardTrackSchema.setModifyTime(mCurTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证作废处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证作废成功！");
		
		cLogger.debug("Out CardManage.invalidated4D()!");
	}
	
	/**
	 * 定额单证回收
	 */
	private void tackBack4D(String pCardNo) throws MidplatException {
		cLogger.debug("Into CardManage.tackBack4D()...");
		
		cLogger.debug("CertifyClass=" + cCertifyClass + "; CertifyCode=" + cCertifyCode + "; CardNo=" + pCardNo);
		
		String mCurDate = DateUtil.getCur10Date();
		String mCurTime = DateUtil.getCur8Time();
		
		LZCardDB mLZCardDB = new LZCardDB();
		mLZCardDB.setCertifyCode(cCertifyCode);
		mLZCardDB.setStartNo(pCardNo);
		mLZCardDB.setEndNo(pCardNo);
		LZCardSet mLZCardSet = mLZCardDB.query();
		if (mLZCardDB.mErrors.needDealError()
				|| (null==mLZCardSet)
				|| (1!=mLZCardSet.size())) {
			cLogger.error(mLZCardDB.mErrors.getFirstError());
			throw new MidplatException("未查到相应单证！" + pCardNo);
		}
		LZCardSchema mLZCardSchema = mLZCardSet.get(1);
		
		mLZCardSchema.setState("11");	//11-可用; 2-正常回销; 6-作废
		String mTempSendOutCom = mLZCardSchema.getSendOutCom();
		mLZCardSchema.setSendOutCom(mLZCardSchema.getReceiveCom());
		mLZCardSchema.setReceiveCom(mTempSendOutCom);
		mLZCardSchema.setModifyDate(mCurDate);
		mLZCardSchema.setMakeTime(mCurTime);
		
		LZCardTrackSchema mLZCardTrackSchema = new LZCardTrackSchema();
		mLZCardTrackSchema.setCertifyCode(mLZCardSchema.getCertifyCode());
		mLZCardTrackSchema.setSubCode(mLZCardSchema.getSubCode());
		mLZCardTrackSchema.setRiskCode(mLZCardSchema.getRiskCode());
		mLZCardTrackSchema.setRiskVersion(mLZCardSchema.getRiskVersion());
		mLZCardTrackSchema.setStartNo(mLZCardSchema.getStartNo());
		mLZCardTrackSchema.setEndNo(mLZCardSchema.getEndNo());
		mLZCardTrackSchema.setSendOutCom(mLZCardSchema.getSendOutCom());
		mLZCardTrackSchema.setReceiveCom(mLZCardSchema.getReceiveCom());
		mLZCardTrackSchema.setSumCount(mLZCardSchema.getSumCount());
		mLZCardTrackSchema.setPrem(mLZCardSchema.getPrem());
		mLZCardTrackSchema.setAmnt(mLZCardSchema.getAmnt());
		mLZCardTrackSchema.setHandler(mLZCardSchema.getHandler());
		mLZCardTrackSchema.setHandleDate(mLZCardSchema.getHandleDate());
		mLZCardTrackSchema.setInvaliDate(mLZCardSchema.getInvaliDate());
		String mNoLimit = PubFun.getNoLimit(cGlobalInput.ComCode);
		String mTakeBackNo = PubFun1.CreateMaxNo("TAKEBACKNO", mNoLimit);
		mLZCardTrackSchema.setTakeBackNo(mTakeBackNo);
		mLZCardTrackSchema.setStateFlag(mLZCardSchema.getStateFlag());
		mLZCardTrackSchema.setOperator(mLZCardSchema.getOperator());
		mLZCardTrackSchema.setMakeDate(mCurDate);
		mLZCardTrackSchema.setMakeTime(mCurTime);
		mLZCardTrackSchema.setModifyDate(mCurDate);
		mLZCardTrackSchema.setModifyTime(mCurTime);
		
		MMap mSubmitMMap = new MMap();
		mSubmitMMap.put(mLZCardSchema, "UPDATE");
		mSubmitMMap.put(mLZCardTrackSchema, "INSERT");
		VData mSubmitVData = new VData();
		mSubmitVData.add(mSubmitMMap);
		PubSubmit mPubSubmit = new PubSubmit();
		cLogger.info("开始单证回收处理...");
		if (!mPubSubmit.submitData(mSubmitVData, "")) {
			cLogger.error(mPubSubmit.mErrors.getFirstError());
			throw new MidplatException("提交签单数据失败！");
		}
		cLogger.info("单证回收成功！");
		
		cLogger.debug("Out CardManage.tackBack4D()!");
	}
}
