package com.sinosoft.midplat.kernel.util;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import java.rmi.*;
import com.sinosoft.lis.message.*;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.kernel.service.ServiceImpl;

import java.util.*;

import org.jdom.Document;
import org.jdom.Element;

/**
 * <p>Title: 客户短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SendMsgTool extends ServiceImpl{
    private GlobalInput mG = new GlobalInput();    
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mLastDate = "";    
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();    

    public SendMsgTool(Element pThisBusiConf) {
    	super(pThisBusiConf);
    }


    public Document service(Document pInXmlDoc) {
            sendMsg();
        return null;
    }

    private boolean sendMsg() {
        System.out.println("银保通异常短信通知开始......");
        SmsServiceSoapBindingStub binding = null;
        try {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
                      getSmsService(); //创建binding对象
        } catch (javax.xml.rpc.ServiceException jre) {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("ybt"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("18：00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        Vector vec = new Vector();
        vec = getMessage();

        if (!vec.isEmpty()) {
            msgs.setMessages((SmsMessage[]) vec.toArray(new SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            try {
                value = binding.sendSMS(msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                System.out.println(value.getStatus());
                System.out.println(value.getMessage());
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.print("无符合条件案件！");
        }
        return true;
    }

    private Vector getMessage() {
        Vector tVector = new Vector();

        String tSQL =
        	"select bankcode,proposalno,prtno,transtime from lktransstatus where descr like '%提交签单数据%' and transdate = '"+DateUtil.getCurDate("yyyy-MM-dd")+"' with ur";
        
        System.out.println(tSQL);

//        SSRS tMsgSSRS = tExeSQL.execSQL(tSQL);

//        for (int i = 1; i <= tMsgSSRS.getMaxRow(); i++) {
//            String tBankcode = tMsgSSRS.GetText(i, 1);
//            String tproposalno = tMsgSSRS.GetText(i, 2);
//            String tprtno = tMsgSSRS.GetText(i, 3);
//            String transtime = tMsgSSRS.GetText(i, 4);
        
            String tMobile ="13911068472";
         

            String tContents = "哈哈哈";

//                tContents = "银行："+tBankcode+"在"+transtime+"发生提交数据失败异常，请核查。"+"投保单号："+tprtno+",保单印刷号："+tproposalno;

            System.out.println("发送短信：" +tMobile );

            SmsMessage msg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            msg.setReceiver(tMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            msg.setContents(tContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            //和IT讨论后，归类到二级机构
            msg.setOrgCode("86000000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            tVector.add(msg);
//        }
        return tVector;
    }


    /*数据处理*/
    private boolean SubmitMap() {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, "")) {
            System.out.println("提交数据失败！");
//            return false;
        }
        this.mMap = new MMap();
        return true;
    }

    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        LCContSchema tLCContSchema = new LCContSchema();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);

//        SendMsgTool tSendMsgTool = new SendMsgTool();

//        tSendMsgTool.submitData(mVData, "LPMSG");
//       System.out.print(mPolicyAbateDealBL.edorseState("00000619801"));
//         mPolicyAbateDealBL.edorseState("0000126302");
    }


}
