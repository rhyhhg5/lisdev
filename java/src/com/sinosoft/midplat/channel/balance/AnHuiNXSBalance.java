package com.sinosoft.midplat.channel.balance;

import java.io.BufferedReader;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.config.ConfigCache;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.DateUtil;

public class AnHuiNXSBalance extends Balance {
	public AnHuiNXSBalance(Element pConfigEle) {
		super(pConfigEle);
	}
	
	protected String getFileName() {
		return "ANHNX00001" + DateUtil.getCurrentDate("yyyyMMdd") + ".txt";
	}
	
	protected Element getChkDetails(BufferedReader pBufReader) throws Exception {
		cLogger.info("Into AnHuiNXSBalance.getChkDetails()...");
		
		Element mChkDetails = new Element(ChkDetails);
		for (String tLineMsg; null != (tLineMsg=pBufReader.readLine());) {
			cLogger.info(tLineMsg);
			
			//空行，直接跳过
			tLineMsg = tLineMsg.trim();
			if ("".equals(tLineMsg)) {
				cLogger.warn("空行，直接跳过！");
				break;
			}
			
			String[] tSubMsgs = tLineMsg.split("\\|", -1);
			
			Element tTranDate = new Element(TranDate);
			tTranDate.setText(
					DateUtil.formatTrans(tSubMsgs[1], "yyyyMMdd", "yyyy-MM-dd"));
			
			Element tFuncFlag = new Element(FuncFlag);
			tFuncFlag.setText("01");
			
			Element tBankCode = new Element(BankCode);
			tBankCode.setText("33");
			
			Element tBankZoneCode = new Element(BankZoneCode);
			tBankZoneCode.setText(tSubMsgs[2]);
			
			Element tBrNo = new Element(BrNo);
			tBrNo.setText(tSubMsgs[3]);
			
			Element tTransrNo = new Element(TransrNo);
			tTransrNo.setText(tSubMsgs[5]);

			Element tCardNo = new Element(CardNo);
			tCardNo.setText(tSubMsgs[6]);
			
			Element tTranAmnt = new Element(TranAmnt);
			tTranAmnt.setText(tSubMsgs[7]);
			
			Element tTellerNo = new Element(TellerNo);
			
			Element tConfirmFlag = new Element(ConfirmFlag);
			tConfirmFlag.setText("1");
			
			Element tAppntName = new Element(AppntName);
			
			Element tChkDetail = new Element(ChkDetail);
			tChkDetail.addContent(tBankCode);
			tChkDetail.addContent(tTranDate);
			tChkDetail.addContent(tBankZoneCode);
			tChkDetail.addContent(tBrNo);
			tChkDetail.addContent(tTellerNo);
			tChkDetail.addContent(tFuncFlag);
			tChkDetail.addContent(tTransrNo);
			tChkDetail.addContent(tCardNo);
			tChkDetail.addContent(tAppntName);
			tChkDetail.addContent(tTranAmnt);
			tChkDetail.addContent(tConfirmFlag);
			
			mChkDetails.addContent(tChkDetail);
		}

		pBufReader.close();
		
		cLogger.info("Out AnHuiNXSBalance.getChkDetails()!");
		return mChkDetails;
	}
	
	public static void main(String[] args) throws Exception {
		Logger mLogger = Logger.getLogger("com.sinosoft.midplat.channel.balance.AnHuiNXSBalance.main");
		mLogger.info("手工补对账开始...");
		
		XPath mXPath = XPath.newInstance(
				"/balanceConfigs/balanceConfig[bank='99' and ZoneNo='8634' and BrNo='8634']");
		Element mBalanceEle = 
			(Element) mXPath.selectSingleNode(ConfigCache.getBalanceConfig());
		Balance mBalance = new AnHuiNXSBalance(mBalanceEle);
		
		//用于补对账，设置补对账日期
		if (0 == args.length) {
			throw new BaseException("未传入要补对账的日期！");
		} 
		/**
		 * 严格日期校验的正则表达式：\\d{4}-((0\\d)|(1[012]))-(([012]\\d)|(3[01]))。
		 * 4位年-2位月-2位日。
		 * 4位年：4位[0-9]的数字。
		 * 1或2位月：单数月为0加[0-9]的数字；双数月必须以1开头，尾数为0、1或2三个数之一。
		 * 1或2位日：以0、1或2开头加[0-9]的数字，或者以3开头加0或1。
		 * 
		 * 简单日期校验的正则表达式：\\d{4}-\\d{2}-\\d{2}。
		 */
		else if (!args[0].matches("\\d{4}-((0\\d)|(1[012]))-(([012]\\d)|(3[01]))")) {
			throw new BaseException("日期格式有误，应为yyyy-MM-dd！" + args[0]);
		}
		
		mLogger.info("args[0] = " + args[0]);
//		mBalance.setDate(args[0]);
		mBalance.run();
		
		mLogger.info("手工补对账结束！");
	}
}
