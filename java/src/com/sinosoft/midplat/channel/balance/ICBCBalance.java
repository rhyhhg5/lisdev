package com.sinosoft.midplat.channel.balance;

import java.io.BufferedReader;

import org.jdom.Element;

import com.sinosoft.midplat.channel.util.DateUtil;

public class ICBCBalance extends Balance {
	public ICBCBalance(Element pConfigEle) {
		super(pConfigEle);
	}

	protected String getFileName() {
		return "02001" + DateUtil.getCurrentDate("yyyyMMdd") + "01.txt";
	}

	protected Element getChkDetails(BufferedReader pBufReader) throws Exception {
		cLogger.info("Into ICBCBalance.getChkDetails()...");
		
		Element mChkDetails = new Element(ChkDetails);
		for (String tLineMsg; null != (tLineMsg=pBufReader.readLine());) {
			cLogger.info(tLineMsg);
			
			String[] tSubMsgs = tLineMsg.split("\\|");

			Element tBankCode = new Element(BankCode);
			tBankCode.setText(tSubMsgs[0]);
			
			Element tTranDate = new Element(TranDate);
			tTranDate.setText(tSubMsgs[1]);
			
			Element tBankZoneCode = new Element(BankZoneCode);
			tBankZoneCode.setText(tSubMsgs[2]);
			
			Element tBrNo = new Element(BrNo);
			tBrNo.setText(tSubMsgs[3]);
			
			Element tTellerNo = new Element(TellerNo);
			
			Element tFuncFlag = new Element(FuncFlag);
			tFuncFlag.setText("01");
			
			Element tTransrNo = new Element(TransrNo);
			tTransrNo.setText(tSubMsgs[5]);

			Element tCardNo = new Element(CardNo);
			tCardNo.setText(tSubMsgs[6]);
			
			Element tAppntName = new Element(AppntName);
			
			Element tTranAmnt = new Element(TranAmnt);
			tTranAmnt.setText(tSubMsgs[7]);
			
			Element tConfirmFlag = new Element(ConfirmFlag);
			tConfirmFlag.setText("1");
			
			Element tChkDetail = new Element(ChkDetail);
			tChkDetail.addContent(tBankCode);
			tChkDetail.addContent(tTranDate);
			tChkDetail.addContent(tBankZoneCode);
			tChkDetail.addContent(tBrNo);
			tChkDetail.addContent(tTellerNo);
			tChkDetail.addContent(tFuncFlag);
			tChkDetail.addContent(tTransrNo);
			tChkDetail.addContent(tCardNo);
			tChkDetail.addContent(tAppntName);
			tChkDetail.addContent(tTranAmnt);
			tChkDetail.addContent(tConfirmFlag);
			
			mChkDetails.addContent(tChkDetail);
		}
		
		pBufReader.close();	//关闭流
		
		cLogger.info("Out ICBCBalance.getChkDetails()!");
		return mChkDetails;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始...");
		
		String tFileName = "010386432008111201.txt";
		
		String tTransNoStr = tFileName.substring(0, tFileName.indexOf("."));
		System.out.println(tTransNoStr);
		
		System.out.println("成功结束！");
	}
}
