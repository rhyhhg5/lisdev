package com.sinosoft.midplat.channel.balance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class ABCBalance extends Balance{
	public ABCBalance (Element pConfigEle) {
		super(pConfigEle);
	}

	protected String getFileName() {
		return "B304004" + DateUtil.getCurrentDate("yyyyMMdd") + ".TXT";
	}
	
	protected Element getChkDetails(BufferedReader pBufReader) throws Exception {
		cLogger.info("Into ABCBalance.getAbcDetails()...");
		
		String filePath = ConfigFactory.getPreConfig().getString("ABC.Balance.Path");
		File mFileDir = new File(filePath);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
				filePath + getFileName())));
		
		Element mChkDetails = new Element(ChkDetails);
		String tLineMsg = pBufReader.readLine();
		for (; null != (tLineMsg=pBufReader.readLine());) {
			cLogger.info(tLineMsg);
			
			String[] tSubMsgs = tLineMsg.split("\\|");

			Element tTranDate = new Element(TranDate);
			tTranDate.setText(tSubMsgs[0]);
			
			Element tTransrNo = new Element(TransrNo);
			tTransrNo.setText(tSubMsgs[2]);
			
			Element tBankCode = new Element(BankCode);
			tBankCode.setText("04");
			
			String mBankZoneCodeStr = tSubMsgs[3] + tSubMsgs[4];	//农行对账文件把地区代码分成了两个
			Element tBankZoneCode = new Element(BankZoneCode);
			tBankZoneCode.setText(mBankZoneCodeStr);
			
			Element tBrNo = new Element(BrNo);
			tBrNo.setText(tSubMsgs[5]);
			
			Element tCardNo = new Element(CardNo);
			tCardNo.setText(tSubMsgs[8]);
			
			Element tTranAmnt = new Element(TranAmnt);
			tTranAmnt.setText(tSubMsgs[9]);
			
			Element tTellerNo = new Element(TellerNo);

			Element tFuncFlag = new Element(FuncFlag);
			tFuncFlag.setText(tSubMsgs[11]);
			
			Element tConfirmFlag = new Element(ConfirmFlag);
			tConfirmFlag.setText("1");

			Element tAppntName = new Element(AppntName);
			
			Element tChkDetail = new Element(ChkDetail);
			tChkDetail.addContent(tBankCode);
			tChkDetail.addContent(tTranDate);
			tChkDetail.addContent(tBankZoneCode);
			tChkDetail.addContent(tBrNo);
			tChkDetail.addContent(tTellerNo);
			tChkDetail.addContent(tFuncFlag);
			tChkDetail.addContent(tTransrNo);
			tChkDetail.addContent(tCardNo);
			tChkDetail.addContent(tAppntName);
			tChkDetail.addContent(tTranAmnt);
			tChkDetail.addContent(tConfirmFlag);
			
			mChkDetails.addContent(tChkDetail);
			
			pw.println(tLineMsg);
			cLogger.info("保存农行对账文件成功");
		}
		pBufReader.close();
		
		pw.flush();
		pw.close();
		
		cLogger.info("Out ABCBalance.getAbcDetails()!");
		return mChkDetails;
	}
	
	public static void main(String[] args) {
		Element root = new Element("balanceConfig");
		Element bank = new Element("bank");
		bank.setText("04");
		Element ZoneNo = new Element("ZoneNo");
		ZoneNo.setText("86");
		Element BrNo = new Element("BrNo");
		BrNo.setText("86");
		Element localDir = new Element("localDir");
		localDir.setText("D:/");
		root.addContent(bank);
		root.addContent(ZoneNo);
		root.addContent(BrNo);
		root.addContent(localDir);
		
		Document doc = new Document(root);
		JdomUtil.print(doc);
		ABCBalance tABCBalance = new ABCBalance(root);
		tABCBalance.run();
	}
}
