package com.sinosoft.midplat.channel.balance;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.TimerTask;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.IOTrans;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;
import com.sinosoft.midplat.channel.util.XmlTag;

public abstract class Balance extends TimerTask implements XmlTag {
	protected final Logger cLogger = Logger.getLogger(getClass());
	
	protected final Element cConfigEle;
	
	public Balance(Element pConfigEle) {
		cConfigEle = pConfigEle;
	}

	/**
	 * 为保证对账Timer不会因为某天的一次异常而终止，这里必须捕获run()中的所有异常。
	 */
	public void run() {
		cLogger.info("Into Balance.run()...");

		try {
			String tFileName = getFileName();
			
			Element tTranData = new Element(TranData);
			Document tInStdXml = new Document(tTranData);
			
			Element tBaseInfo = getBaseInfo();
			tTranData.addContent(tBaseInfo);
			//设置交易流水号为文件名
			String tTransNoStr = tFileName;
			int tIndex = tTransNoStr.indexOf(".");
			if (-1 != tIndex) {	//如果有后缀，则去除后缀
				tTransNoStr = tTransNoStr.substring(0, tIndex);
			}
			tBaseInfo.getChild(TransrNo).setText(tTransNoStr);
			
			String tBalanceFilePath = cConfigEle.getChildText("localDir") + tFileName;
			cLogger.info("对账文件路径：" + tBalanceFilePath);
			
			try {
				String ttLocalDir = cConfigEle.getChildTextTrim("localDir");
				Element ttFtpEle = cConfigEle.getChild("ftp");
				BufferedReader ttBufReader = null;
				if ((null!=ttLocalDir) && !ttLocalDir.equals("")) {	//如果localDir有配置，优先取本地文件
					ttBufReader = getLocalFile(ttLocalDir, tFileName);
				} else if (null != ttFtpEle) {	//未配置localDir，如果ftp有配置，通过ftp取文件
					ttBufReader = getFtpFile(ttFtpEle, tFileName);
				} else {	//localDir和ftp都未配置，报错
					throw new BaseException("对账配置有误！");
				}
				
				//获取标准对账报文
				Element ttChkDetails = getChkDetails(ttBufReader);
				tTranData.addContent(ttChkDetails);
			} catch (Exception ex) {
				cLogger.error("生成标准对账报文出错！", ex);
				
				//获取标准对账报文
				Element ttError = new Element("Error");
				String ttErrorStr = ex.getMessage();
				if (ttErrorStr.equals("")) {
					ttErrorStr = ex.toString();
				}
				ttError.setText(ttErrorStr);
				tTranData.addContent(ttError);
			}

			SaveMessage.save(tInStdXml, tTransNoStr+".xml", "InStd");
			cLogger.info("保存标准请求报文完毕！");
			
			//调用后置机业务处理，获取标准返回报文
			Document tOutStdXml = callPostServlet(tInStdXml);
			
			SaveMessage.save(tOutStdXml, tTransNoStr+".xml", "OutStd");
			cLogger.info("保存标准返回报文完毕！");
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
		}
		
		cLogger.info("Out Balance.run()!");
	}
	
	protected Element getBaseInfo() {
		cLogger.info("Into Balance.getBaseInfo()...");
		
		Element mBankDate = new Element(BankDate);
		mBankDate.setText(DateUtil.getCurrentDate("yyyyMMdd"));
		
		Element mBankCode = new Element(BankCode);
		mBankCode.setText(cConfigEle.getChildText("bank"));
		
		Element mZoneNo = new Element(ZoneNo);
		mZoneNo.setText(cConfigEle.getChildText("ZoneNo"));
		
		Element mBrNo = new Element(BrNo);
		mBrNo.setText(cConfigEle.getChildText("BrNo"));
		
		Element mTellerNo = new Element(TellerNo);
		mTellerNo.setText("ybtAuto");
		
		Element mTransrNo = new Element(TransrNo);
		
		Element mFunctionFlag = new Element(FunctionFlag);
		mFunctionFlag.setText("17");
		
		Element mInsuID = new Element(InsuID);
		mInsuID.setText("02");
		
		Element mBaseInfo = new Element(BaseInfo);
      mBaseInfo.addContent(mBankDate);
      mBaseInfo.addContent(mBankCode);
      mBaseInfo.addContent(mZoneNo);
      mBaseInfo.addContent(mBrNo);
      mBaseInfo.addContent(mTellerNo);
      mBaseInfo.addContent(mTransrNo);
      mBaseInfo.addContent(mFunctionFlag);
      mBaseInfo.addContent(mInsuID);
      
      cLogger.info("Out Balance.getBaseInfo()!");
      return mBaseInfo;
	}
	
	protected BufferedReader getFtpFile(Element pFtpEle, String pFileName) throws Exception {
		cLogger.info("Into Balance.getFtpFile()...");
		
		String mIP = pFtpEle.getAttributeValue("ip");
		if ((null == mIP) || mIP.equals("")) {
			mIP = "127.0.0.1";
		}
		
		String mPort = pFtpEle.getAttributeValue("port");
		if ((null == mPort) || mPort.equals("")) {
			mPort = "21";
		}
		
		String mFilePath = pFtpEle.getAttributeValue("path");
		if ((null == mFilePath) || "".equals(mFilePath)) {
			mFilePath = "./";
		}
		mFilePath = mFilePath.replace('\\', '/');
		if (!mFilePath.endsWith("/")) {
			mFilePath = mFilePath + "/";
		}
		
		String mUserName = pFtpEle.getAttributeValue("user");
		if ((null == mUserName) || "".equals(mUserName)) {
			mUserName = " ";
		}
		
		String mPassword = pFtpEle.getAttributeValue("password");
		if ((null == mPassword) || "".equals(mPassword)) {
			mPassword = " ";
		}

		String mPathName = mFilePath + pFileName;
		
		BufferedReader mBufReader = null;
		
		//ftp到银行
		FTPClient mFTPClient = new FTPClient();
		
		mFTPClient.setDefaultPort(Integer.parseInt(mPort));
		try {
			//建立ftp连接
			mFTPClient.connect(mIP);
			int tReplyCode = mFTPClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(tReplyCode)) {
				throw new BaseException("ftp连接失败！" + mIP + ": " + tReplyCode);
			}
			cLogger.info("ftp连接成功！" + mIP);
			
			//登录
			if (!mFTPClient.login(mUserName, mPassword)) {
				throw new BaseException("ftp登录失败！" + mUserName + ":" + mPassword);
			}
			cLogger.info("ftp登录成功！");
			mFTPClient.enterLocalPassiveMode();
			cLogger.error("ftp被动模式！");
			InputStream mRemoteIs = 
				mFTPClient.retrieveFileStream(mPathName);
			if (null == mRemoteIs) {
				throw new BaseException("未找到对账文件！" + (mPathName));
			}
			cLogger.info("ftp下载数据成功！");
			
			byte[] tBytes = IOTrans.InputStreamToBytes(mRemoteIs);
			Reader tBalanceReader = new InputStreamReader(
						new ByteArrayInputStream(tBytes));
			mBufReader = new BufferedReader(tBalanceReader);
			
			//退出登陆
			mFTPClient.logout();
			cLogger.info("ftp退出成功！");
		} finally {
			if (mFTPClient.isConnected()) {
				try {
					mFTPClient.disconnect();
					cLogger.info("ftp连接断开！");
				} catch(IOException ex) {
					cLogger.warn("服务端连接已断开！", ex);
				}
			}
		}
		
		cLogger.info("Out Balance.getFtpFile()!");
		return mBufReader;
	}
	
	private BufferedReader getLocalFile(String pFilePath, String pFileName) throws BaseException {
		cLogger.info("Into Balance.getLocalFile()...");
		
		pFilePath = pFilePath.replace('\\', '/');
		if (!pFilePath.startsWith("/")) {
			pFilePath = "/" + pFilePath;
		}
		if (!pFilePath.endsWith("/")) {
			pFilePath = pFilePath + "/";
		}
		
		String mPathName = pFilePath + pFileName;
		cLogger.info("LocalPath = " + mPathName);
		
		BufferedReader mBufReader = null;
		try {
			mBufReader = new BufferedReader(
					new FileReader(mPathName));
		} catch (IOException ex) {
			//未找到对账文件
			throw new BaseException("未找到对账文件！" + mPathName);
		}
		
		cLogger.info("Out Balance.getLocalFile()!");
		return mBufReader;
	}
	
	protected Element getChkDetails(BufferedReader pBufReader) throws Exception {
		cLogger.info("Into Balance.getChkDetails()...");
		
//		BufferedReader mBufReader = new BufferedReader(new FileReader(pFilePath));
		
		Element mChkDetails = new Element(ChkDetails);
		for (String tLineMsg; null != (tLineMsg=pBufReader.readLine());) {
			cLogger.info(tLineMsg);
			
			String[] tSubMsgs = tLineMsg.split("\\|");

			Element tBankCode = new Element(BankCode);
			tBankCode.setText(tSubMsgs[0]);
			
			Element tTranDate = new Element(TranDate);
			tTranDate.setText(tSubMsgs[1]);
			
			Element tBankZoneCode = new Element(BankZoneCode);
			tBankZoneCode.setText(tSubMsgs[2]);
			
			Element tBrNo = new Element(BrNo);
			tBrNo.setText(tSubMsgs[3]);
			
			Element tTellerNo = new Element(TellerNo);
			
			Element tFuncFlag = new Element(FuncFlag);
			tFuncFlag.setText(tSubMsgs[4]);
			
			Element tTransrNo = new Element(TransrNo);
			tTransrNo.setText(tSubMsgs[5]);

			Element tCardNo = new Element(CardNo);
			tCardNo.setText(tSubMsgs[6]);
			
			Element tAppntName = new Element(AppntName);
			
			Element tTranAmnt = new Element(TranAmnt);
			tTranAmnt.setText(tSubMsgs[7]);
			
			Element tConfirmFlag = new Element(ConfirmFlag);
			tConfirmFlag.setText(tSubMsgs[8]);
			
			Element tChkDetail = new Element(ChkDetail);
			tChkDetail.addContent(tBankCode);
			tChkDetail.addContent(tTranDate);
			tChkDetail.addContent(tBankZoneCode);
			tChkDetail.addContent(tBrNo);
			tChkDetail.addContent(tTellerNo);
			tChkDetail.addContent(tFuncFlag);
			tChkDetail.addContent(tTransrNo);
			tChkDetail.addContent(tCardNo);
			tChkDetail.addContent(tAppntName);
			tChkDetail.addContent(tTranAmnt);
			tChkDetail.addContent(tConfirmFlag);
			
			mChkDetails.addContent(tChkDetail);
		}
		pBufReader.close();
		
		cLogger.info("Out Balance.getChkDetails()!");
		return mChkDetails;
	}

	/**
	 * 根据对账文件命名规则，生成当天对账文件名
	 */
	protected abstract String getFileName();
	
	protected Document callPostServlet(Document pInStdXmlDoc) throws Exception {
		cLogger.info("Into Balance.callPostServlet()...");
		
		String mPostServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
		cLogger.info("PostServletURL = " + mPostServletURL);
		URL mURL = new URL(mPostServletURL);
		URLConnection mURLConnection = mURL.openConnection();
		mURLConnection.setDoOutput(true);
		mURLConnection.setDoInput(true);
      XMLOutputter mXMLOutputter = new XMLOutputter(Format.getCompactFormat().setEncoding("GBK"));
      cLogger.info("前置机开始向后置机发送报文...");
		OutputStream mURLOs = mURLConnection.getOutputStream();
		mXMLOutputter.output(pInStdXmlDoc, mURLOs);
		mURLOs.close();
      
      long mStartMillis = System.currentTimeMillis();
		InputStream mURLIs = mURLConnection.getInputStream();
		Document mOutStdXml = JdomUtil.build(mURLIs);	//统一使用GBK编码
		mURLIs.close();
		cLogger.debug("后置机处理耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("接收到后置机返回报文，并解析为Document！");

		cLogger.info("Out Balance.callPostServlet()!");
		return mOutStdXml;
	}
}
