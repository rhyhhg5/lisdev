package com.sinosoft.midplat.channel.balance;

import org.jdom.Element;

import com.sinosoft.midplat.channel.util.DateUtil;

public class BCMBalance extends Balance {
	public BCMBalance(Element pConfigEle) {
		super(pConfigEle);
	}

	protected String getFileName() {
		return "RBJK_" + cConfigEle.getChildText("ZoneNo") + "_" + DateUtil.getCurrentDate("yyyyMMdd");
	}
}