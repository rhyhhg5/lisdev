package com.sinosoft.midplat.channel.balance;

import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.config.ConfigFactory;


public class BalanceListener implements ServletContextListener {
	private final static Logger cLogger = Logger.getLogger(BalanceListener.class);

	private Timer[] cTimers = null;
	
	public static void main(String[] args) {
		new BalanceListener().contextInitialized(null);
	}
	
	public void contextInitialized(ServletContextEvent event) {
		cLogger.info("Into BalanceListener.contextInitialized()...");
		cLogger.info("BalanceListener监听已启动！");

		Document mBalanceConfigDoc = ConfigFactory.getBalanceConfig();
		List mConfigList = mBalanceConfigDoc.getRootElement().getChildren();
		cTimers = new Timer[mConfigList.size()];
		for (int i = 0; i < mConfigList.size(); i++) {
			try {
				Element ttBalanceConfig = (Element) mConfigList.get(i);
				
				cLogger.info(i + "_BalanceConfig: bank=" + ttBalanceConfig.getChildText("bank")
						+ "; ZoneNo=" + ttBalanceConfig.getChildText("ZoneNo")
						+ "; BrNo=" + ttBalanceConfig.getChildText("BrNo"));
				String ttClassName = ttBalanceConfig.getChildText("class");
				Class ttPreControlClass = Class.forName(ttClassName);
				TimerTask ttTimerTask = (TimerTask) ttPreControlClass
					.getConstructor(new Class[]{Element.class})
					.newInstance(new Object[]{ttBalanceConfig});

				int ttStartHour = 11;
				int ttStartMinute = 00;

				try {
					ttStartHour = Integer.parseInt(
							ttBalanceConfig.getChildText("startHour"));
					ttStartMinute = Integer.parseInt(
							ttBalanceConfig.getChildText("startMinute"));
				} catch (Exception ex) {
					cLogger.warn("对账时间配置有误，系统自动采用默认置：startHour=" + ttStartHour + "; startMinute=" + ttStartMinute);
					cLogger.debug("对账时间配置有误！", ex);		//debug模式下输出全部错误堆栈，便于在测试环境调试  
				}
				Calendar ttCalendar = Calendar.getInstance();
				ttCalendar.set(Calendar.HOUR_OF_DAY, ttStartHour);
				ttCalendar.set(Calendar.MINUTE, ttStartMinute);
				ttCalendar.set(Calendar.SECOND, 0);
				
				Timer ttTimer = new Timer(true);
				ttTimer.scheduleAtFixedRate(ttTimerTask, ttCalendar.getTime(), 24*60*60*1000);
				cTimers[i] = ttTimer;
				cLogger.info(i + "_BalanceConfig加载成功!");
			} catch (Exception ex) {
				cLogger.error(i + "_BalanceConfig加载失败!", ex);
			}
		}
		
		cLogger.info("Out BalanceListener.contextInitialized()!");
	}
	
	public void contextDestroyed(ServletContextEvent event) {
		cLogger.info("Into BalanceListener.contextDestroyed()...");
		
		cLogger.info("开始关闭对账Timer");
		/**
		 * 一定要关闭，否则重启时上一个对账任务线程会继续跑，造成多个执行相同对账任务的对账线程同时跑的情况，
		 * 进而造成同一天同一银行多次对账。
		 */
		for (int i = 0; i < cTimers.length; i++) {
			/**
			 * 在加载balanceConfig.xml中某项失败时，cTimers中对应的值会为null，在此需过滤掉。
			 */
			if (null != cTimers[i]) {
				cTimers[i].cancel();
			}
		}
		
		cLogger.warn("BalanceListener监听销毁！");
		cLogger.info("Out BalanceListener.contextDestroyed()!");
	}
}
