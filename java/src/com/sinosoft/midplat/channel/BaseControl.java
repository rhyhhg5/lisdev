/**
 * 实现了报文转换和后置机通讯等公用方法。
 */

package com.sinosoft.midplat.channel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;
import com.sinosoft.midplat.channel.util.XmlTag;

public abstract class BaseControl extends Thread implements XmlTag {
	protected final Logger cLogger = Logger.getLogger(getClass());
	
	protected Socket cSocket = null;
	
	protected HashMap cBaseInfoMap = new HashMap();
	
	private BaseFormat cFormat = null;

	public BaseControl(Socket pSocket) {
		cSocket = pSocket;
		cLogger.info("客户端IP：" + cSocket.getInetAddress().getHostAddress()
				+ "；服务端口：" + cSocket.getLocalPort());
	}
	
	public void run() {
		cLogger.info("Into BaseControl.run()...");
		
		long mStartMillis = System.currentTimeMillis();
		Document mOutNoStdXml = null;
		
		try {
			//解析报文
			byte[] tBodyBytes = receive(cSocket.getInputStream());
			
			Document tInNoStdXml = getInNoStdXml(tBodyBytes);
			tBodyBytes = null;
			
			//转换为标准请求报文
			Document tInStdXml = getInStdXml(tInNoStdXml);
			tInNoStdXml = null;
			
			//调用后置机业务处理，获取标准返回报文
			Document tOutStdXml = callPostServlet(tInStdXml);
			tInStdXml = null;
			
			//转换为非标准返回报文
			mOutNoStdXml = getOutNoStdXml(tOutStdXml);
			tOutStdXml = null;
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
			mOutNoStdXml = getErrorXml(ex.getMessage());
		} finally {
			//保存非标准返回报文
			StringBuffer mSaveName = new StringBuffer();
			mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
				.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
			SaveMessage.save(mOutNoStdXml, mSaveName.toString(), "OutNoStd");
			cLogger.info("保存非标准返回报文完毕！");
			
			try {
				byte[] ttOutBytes = getOutBytes(mOutNoStdXml);
				cSocket.getOutputStream().write(ttOutBytes);
			} catch (Exception ex) {
				cLogger.error("向银行发送返回报文失败！", ex);
			}
			
			try {
				cSocket.close();
				cLogger.debug("Socket已关闭！");
			} catch (IOException e) {
				cLogger.error("关闭Socket出错！", e);
			}
		}
		
		long mDealMillis = System.currentTimeMillis() - mStartMillis;
		cLogger.info("处理总耗时：" + mDealMillis/1000.0 + "s");
		
		cLogger.info("返回报文发送给银行完毕，交易结束！");
		cLogger.info("Out BaseControl.run()!");
	}
	
	protected abstract byte[] receive(InputStream inputStream) throws Exception;

	protected Document getInNoStdXml(byte[] pBytes) throws Exception {
		cLogger.info("Into BaseControl.getInNoStdXml()...");
		
		Document mXmlDoc = JdomUtil.build(pBytes);
		
		Element mBaseInfo = mXmlDoc.getRootElement().getChild(BaseInfo);
		cBaseInfoMap.put("TransNo", mBaseInfo.getChildTextTrim(TransrNo));
		cLogger.info("交易流水号：" + cBaseInfoMap.get("TransNo"));
		cBaseInfoMap.put("FuncFlag", mBaseInfo.getChildTextTrim(FunctionFlag));
		cLogger.info("交易代码：" + cBaseInfoMap.get("FuncFlag"));
		
		//保存非标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mXmlDoc, mSaveName.toString(), "InNoStd");
		cLogger.info("保存非标准请求报文完毕！");
		
		cLogger.info("Out BaseControl.getInNoStdXml()!");
		return mXmlDoc;
	}
	
	protected Document getInStdXml(Document pInNoStdXml) throws Exception {
		cLogger.info("Into BaseControl.getInStdXml()...");
		
		Document mFormatConfigDoc = ConfigFactory.getFormatConfig();
		
		XPath mXPath = XPath.newInstance(
				"/formatConfigs/formatConfig[bank='" + cBaseInfoMap.get("BankName")
				+ "' and funcFlag='" + cBaseInfoMap.get("FuncFlag") + "']/class");
		String mClassName = mXPath.valueOf(mFormatConfigDoc);
		cLogger.debug("转换类：" + mClassName);
		Class mFormatClass = Class.forName(mClassName);
		cFormat = (BaseFormat) mFormatClass.newInstance();
		
		Document mInStdXml = cFormat.getStdXml(pInNoStdXml);
		cLogger.info("非标准报文到标准报文转换完成！");

		//保存标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mInStdXml, mSaveName.toString(), "InStd");
		cLogger.info("保存标准请求报文完毕！");
		
		cLogger.info("Out BaseControl.getInStdXml()!");
		return mInStdXml;
	}
	
	protected Document getOutNoStdXml(Document pOutStdXml) throws Exception {
		cLogger.info("Into BaseControl.getOutNoStdXml()...");
		
		Document mOutNoStdXml = cFormat.getNoStdXml(pOutStdXml);
		cLogger.info("标准报文到非标准报文转换完成！");

		cLogger.info("Out BaseControl.getOutNoStdXml()!");
		return mOutNoStdXml;
	}
	
	protected Document callPostServlet(Document pInStdXmlDoc) throws Exception {
		cLogger.info("Into BaseControl.callPostServlet()...");
		
		String mPostServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
		cLogger.info("PostServletURL = " + mPostServletURL);
		URL mURL = new URL(mPostServletURL);
		URLConnection mURLConnection = mURL.openConnection();
		mURLConnection.setDoOutput(true);
		mURLConnection.setDoInput(true);
		XMLOutputter mXMLOutputter = new XMLOutputter(Format.getCompactFormat().setEncoding("GBK"));
		cLogger.info("前置机开始向后置机发送报文...");
		OutputStream mURLOs = mURLConnection.getOutputStream();
		mXMLOutputter.output(pInStdXmlDoc, mURLOs);
		mURLOs.close();
		
		long mStartMillis = System.currentTimeMillis();
		InputStream mURLIs = mURLConnection.getInputStream();
		Document mOutStdXml = JdomUtil.build(mURLIs);	//统一使用GBK编码
		mURLIs.close();
		cLogger.info("后置机处理耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
		cLogger.info("接收到后置机返回报文，并解析为Document！");
		
		//保存非标准返回报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mOutStdXml, mSaveName.toString(), "OutStd");
		cLogger.info("保存标准返回报文完毕！");
		
		cLogger.info("Out BaseControl.callPostServlet()!");
		return mOutStdXml;
	}
	
	protected Document getErrorXml(String pErrorMsg) {
		cLogger.info("Into BaseControl.getErrorXml()...");
		
		Element mFlag = new Element(Flag);
		mFlag.addContent("0");	//0-失败；1-成功
		
		Element mDesc = new Element(Desc);
		mDesc.addContent(pErrorMsg);

		Element mRetData = new Element(RetData);
		mRetData.addContent(mFlag);
		mRetData.addContent(mDesc);
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mRetData);

		cLogger.info("Out BaseControl.getErrorXml()!");
		return new Document(mTranData);
	}
	
	protected abstract byte[] getOutBytes(Document pXmlDoc) throws Exception;
}
