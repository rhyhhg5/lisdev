package com.sinosoft.midplat.channel.reformat.anhui_nxs;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.util.LoadResource;

public class AnHui_NXS_XslCache {
	private final static Logger cLogger = Logger.getLogger(AnHui_NXS_XslCache.class);
	
	private final static String BasePath = "com/sinosoft/midplat/channel/reformat/anhui_nxs/";
	
	private static XSLTransformer cNewContIn = null;	//新契约投保请求(InNoStd-->InStd)
	private static XSLTransformer cNewContOut = null;	//新契约投保、承保、查询、重打返回(OutStd-->OutNoStd)
	private static XSLTransformer cContConfirmIn = null;	//新契约承保收费请求(InNoStd-->InStd)
	private static XSLTransformer cRePrintIn = null;	//保单重打请求(InNoStd-->InStd)
	private static XSLTransformer cWriteOffIn = null;	//当日撤单请求(InNoStd-->InStd)
	
	/**
	 * 加载本缓存中定义的所有配置，返回加载信息列表。
	 */
	public static List initAll() {
		cLogger.info("Into AnHui_NXS_XslCache.initAll()...");
		
		ArrayList mArrayList = new ArrayList();
		
		mArrayList.add(init(BasePath + "NewContIn.xsl"));
		mArrayList.add(init(BasePath + "NewContOut.xsl"));
		mArrayList.add(init(BasePath + "WriteOffIn.xsl"));
		mArrayList.add(init(BasePath + "ContConfirmIn.xsl"));
		mArrayList.add(init(BasePath + "RePrintIn.xsl"));
		
		cLogger.info("Out AnHui_NXS_XslCache.initAll()!");
		return mArrayList;
	}
	
	public static String init(String pPath) {
		cLogger.info("Into AnHui_NXS_XslCache.init()!");
		
		String mResultMsg = null;
		if (pPath.endsWith("NewContIn.xsl")) {
			cNewContIn = LoadResource.loadXslByPath(pPath);
			if (null == cNewContIn) {
				mResultMsg = "fail load " + pPath;
			} else {
				mResultMsg = "succeed load " + pPath;
			}
		} else if (pPath.endsWith("NewContOut.xsl")) {
			cNewContOut = LoadResource.loadXslByPath(pPath);
			if (null == cNewContOut) {
				mResultMsg = "fail load " + pPath;
			} else {
				mResultMsg = "succeed load " + pPath;
			}
		} else if (pPath.endsWith("WriteOffIn.xsl")) {
			cWriteOffIn = LoadResource.loadXslByPath(pPath);
			if (null == cWriteOffIn) {
				mResultMsg = "fail load " + pPath;
			} else {
				mResultMsg = "succeed load " + pPath;
			}
		} else if (pPath.endsWith("ContConfirmIn.xsl")) {
			cContConfirmIn = LoadResource.loadXslByPath(pPath);
			if (null == cContConfirmIn) {
				mResultMsg = "fail load " + pPath;
			} else {
				mResultMsg = "succeed load " + pPath;
			}
		} else if (pPath.endsWith("RePrintIn.xsl")) {
			cRePrintIn = LoadResource.loadXslByPath(pPath);
			if (null == cRePrintIn) {
				mResultMsg = "fail load " + pPath;
			} else {
				mResultMsg = "succeed load " + pPath;
			}
		} else {
			mResultMsg = "no define file: " + pPath;
		}
		cLogger.debug(mResultMsg);
		
		cLogger.info("Out AnHui_NXS_XslCache.init()!");
		return mResultMsg;
	}
	
	
	/**
	 * 返回NewContIn.xsl(新契约投保请求[InNoStd-->InStd])对应实例。
	 * 加载出错，返回null。
	 */
	public static XSLTransformer getNewContIn() {
		if (null == cNewContIn) {
			String tResultMsg = init(BasePath + "NewContIn.xsl");
			cLogger.info(tResultMsg);
		}

		return cNewContIn;
	}
	
	/**
	 * 返回NewContOut.xsl(新契约投保、承保、查询、重打返回[OutStd-->OutNoStd])对应实例。
	 * 加载出错，返回null。
	 */
	public static XSLTransformer getNewContOut() {
		if (null == cNewContOut) {
			String tResultMsg = init(BasePath + "NewContOut.xsl");
			cLogger.info(tResultMsg);
		}

		return cNewContOut;
	}
	
	/**
	 * 返回ContConfirmIn.xsl(新契约承保收费请求[InNoStd-->InStd])对应实例。
	 * 加载出错，返回null。
	 */
	public static XSLTransformer getContConfirmIn() {
		if (null == cContConfirmIn) {
			String tResultMsg = init(BasePath + "ContConfirmIn.xsl");
			cLogger.info(tResultMsg);
		}

		return cContConfirmIn;
	}
	
	/**
	 * 返回ReprintIn.xsl(保单重打请求[InNoStd-->InStd])对应实例。
	 * 加载出错，返回null。
	 */
	public static XSLTransformer getReprintIn() {
		if (null == cRePrintIn) {
			String tResultMsg = init(BasePath + "RePrintIn.xsl");
			cLogger.info(tResultMsg);
		}

		return cRePrintIn;
	}
	
	/**
	 * 返回WriteOffIn.xsl(当日撤单请求[InNoStd-->InStd])对应实例。
	 * 加载出错，返回null。
	 */
	public static XSLTransformer getWriteOffIn() {
		if (null == cWriteOffIn) {
			String tResultMsg = init(BasePath + "WriteOffIn.xsl");
			cLogger.info(tResultMsg);
		}

		return cWriteOffIn;
	}
}
