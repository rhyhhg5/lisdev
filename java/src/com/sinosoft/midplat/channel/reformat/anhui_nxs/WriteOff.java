package com.sinosoft.midplat.channel.reformat.anhui_nxs;

import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class WriteOff extends BaseFormat {
	private final List cHeadList = new ArrayList();
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into WriteOff.getStdXml()...");
		
		setHead(pNoStdXml);
		
		Document mStdXml = 
			AnHui_NXS_XslCache.getWriteOffIn().transform(pNoStdXml);
		
		cLogger.info("Out WriteOff.getStdXml()!");
		return mStdXml;
	}
	
	private void setHead(Document pXmlDoc) {
		Element mTXLife = pXmlDoc.getRootElement();
		cHeadList.add(
				mTXLife.getChild(TransNo).clone());
		cHeadList.add(
				mTXLife.getChild("QdBankCode").clone());
		cHeadList.add(
				mTXLife.getChild(BankCode).clone());
		cHeadList.add(
				mTXLife.getChild(Branch).clone());
		cHeadList.add(
				mTXLife.getChild(CarrierCode).clone());
		cHeadList.add(
				mTXLife.getChild(TransExeDate).clone());
		cHeadList.add(
				mTXLife.getChild(TransExeTime).clone());
		
		Element mTransRefGUID = mTXLife.getChild(TransRefGUID);
		cHeadList.add(mTransRefGUID.clone());
		
		Element mCpicWater = new Element("CpicWater");
		mCpicWater.setText(mTransRefGUID.getText());
		cHeadList.add(mCpicWater);
		
		cHeadList.add(
				mTXLife.getChild(Teller).clone());
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into WriteOff.getNoStdXml()...");
		
		Element mResultCode = new Element(ResultCode);
		Element mResultInfoDesc = new Element(ResultInfoDesc);
		Element mRetData = pStdXml.getRootElement().getChild(RetData);
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			mResultCode.setText("00");
			mResultInfoDesc.setText("交易成功");
		} else {	//交易失败
			mResultCode.setText("01");
			mResultInfoDesc.setText(
					mRetData.getChildText(Desc));
		}
		
		//设置报文头信息
		Element mTXLife = new Element(TXLife);
		mTXLife.addContent(cHeadList);
		mTXLife.addContent(mResultCode);
		mTXLife.addContent(mResultInfoDesc);
		
		cLogger.info("Out WriteOff.getNoStdXml()!");
		return new Document(mTXLife);
	}
}
