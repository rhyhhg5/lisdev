<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="TXLife">
<TranData>
	<BaseInfo>
		<BankDate />	<!--此处无需设值，后台自动取系统当前日期-->
		<BankCode>33</BankCode>
		<ZoneNo><xsl:value-of select="QdBankCode"/></ZoneNo>
		<BrNo><xsl:value-of select="Branch"/></BrNo>
		<TellerNo><xsl:value-of select="Teller"/></TellerNo>
		<TransrNo><xsl:value-of select="TransRefGUID"/></TransrNo>
		<FunctionFlag>00</FunctionFlag>
	</BaseInfo>

	<LCCont>
		<ContNo></ContNo>
		<PrtNo><xsl:value-of select="HOAppFormNumber"/></PrtNo>
    	<ProposalContNo></ProposalContNo>
	</LCCont>
</TranData>
</xsl:template>

</xsl:stylesheet>