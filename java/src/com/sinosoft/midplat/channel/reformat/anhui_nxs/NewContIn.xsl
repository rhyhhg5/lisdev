<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">

<xsl:template match="TXLife">
<TranData>
	<BaseInfo>
		<BankDate />	<!--此处无需设值，后台自动取系统当前日期-->
		<BankCode>33</BankCode>
		<ZoneNo><xsl:value-of select="QdBankCode"/></ZoneNo>
		<BrNo><xsl:value-of select="Branch"/></BrNo>
		<TellerNo><xsl:value-of select="Teller"/></TellerNo>
		<TransrNo><xsl:value-of select="TransRefGUID"/></TransrNo>
		<FunctionFlag>01</FunctionFlag>
		<InsuID />
	</BaseInfo>

	<LCCont>
		<AccName />
		<BankAccNo />
		<PolApplyDate><xsl:value-of select="SubmissionDate"/></PolApplyDate>
		<PrtNo><xsl:value-of select="HOAppFormNumber"/></PrtNo>
		<PayIntv>0</PayIntv>
    	<ProposalContNo><xsl:value-of select="ContractInfo/LoanContractNo"/></ProposalContNo>
		<AccBankCode />
		<SpecContent />
		<Password />
		<!--投保人-->
		<xsl:apply-templates select="PolicyHolder"/>

		<!--被保人-->
		<LCInsureds>
			<LCInsuredCount>1</LCInsuredCount>
			<xsl:apply-templates select="Insured"/>
		</LCInsureds>
	</LCCont>
</TranData>
</xsl:template>

<!--投保人-->
<xsl:template match="PolicyHolder">
<LCAppnt>
	<AppntName><xsl:value-of select="FullName"/></AppntName>
	<AppntSex><xsl:apply-templates select="Gender"/></AppntSex>
	<AppntBirthday>
		<xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(BirthDate, 'yyyyMMdd', 'yyyy-MM-dd')"/>
	</AppntBirthday>
	<AppntIDType>0</AppntIDType>	<!--写死为身份证-->
	<AppntIDNo><xsl:value-of select="GovtID"/></AppntIDNo>

	<AppntMobile><xsl:value-of select="DialNumber"/></AppntMobile>
	<AppntPhone><xsl:value-of select="DialNumber"/></AppntPhone>
	<MailAddress><xsl:value-of select="Line1"/></MailAddress>
	<MailZipCode><xsl:value-of select="Zip"/></MailZipCode>
	<HomeAddress><xsl:value-of select="Line1"/></HomeAddress>
	<HomeZipCode><xsl:value-of select="Zip"/></HomeZipCode>
	<JobCode>04704</JobCode>
	<Email></Email>
	<RelaToInsured />
</LCAppnt>
</xsl:template>

<!--被保人-->
<xsl:template match="Insured">
<LCInsured>
	<Name><xsl:value-of select="FullName"/></Name>
	<Sex><xsl:apply-templates select="Gender"/></Sex>
	<Birthday>
		<xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(BirthDate, 'yyyyMMdd', 'yyyy-MM-dd')"/>
	</Birthday>
	<IDType>0</IDType>	<!--写死为身份证-->
	<IDNo><xsl:value-of select="GovtID"/></IDNo>

	<InsuredMobile><xsl:value-of select="DialNumber"/></InsuredMobile>
	<HomePhone><xsl:value-of select="DialNumber"/></HomePhone>
	<MailAddress><xsl:value-of select="Line1"/></MailAddress>
	<MailZipCode><xsl:value-of select="Zip"/></MailZipCode>
	<HomeAddress><xsl:value-of select="Line1"/></HomeAddress>
	<HomeZipCode><xsl:value-of select="Zip"/></HomeZipCode>
	<JobCode>04704</JobCode>
	<Email></Email>
	<RelaToMain>00</RelaToMain>
	<RelaToAppnt>00</RelaToAppnt>	<!--写死为本人-->
	
	<Risks>
		<RiskCount>1</RiskCount>
	    	<Risk>
	    		<RiskCode><xsl:value-of select="/TXLife/ProductCode"/></RiskCode>
	    		<MainRiskCode><xsl:value-of select="/TXLife/ProductCode"/></MainRiskCode>
	    		<Amnt><xsl:value-of select="(/TXLife/IntialNumberOfUnits)*1000"/></Amnt>
	    		<Prem>0</Prem>
	    		<Mult><xsl:value-of select="/TXLife/IntialNumberOfUnits"/></Mult>
	    		<CValiDate />
	    		<PayIntv>0</PayIntv>
	    		<PayEndYearFlag></PayEndYearFlag>
	    		<PayEndYear></PayEndYear>
	    		<GetYearFlag />
	    		<GetYear />
	    		<InsuYearFlag>D</InsuYearFlag>
	    		<InsuYear></InsuYear>
	    		<HealthFlag>N</HealthFlag>
	    		<InsuStartDate>
	    			<xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(/TXLife/ContractInfo/LoanStartDate, 'yyyyMMdd', 'yyyy-MM-dd')"/>
	    		</InsuStartDate>
	    		<InsuEndDate>
	    			<xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(/TXLife/ContractInfo/LoanEndDate, 'yyyyMMdd', 'yyyy-MM-dd')"/>
	    		</InsuEndDate>
	    		<LCBnfs> <!-- 受益人列表 -->
					<LCBnfCount>0</LCBnfCount>
				</LCBnfs>
			</Risk>
		</Risks>
</LCInsured>
</xsl:template>

<xsl:template match="Gender">
<xsl:choose>
	<xsl:when test=".=0">2</xsl:when>
	<xsl:when test=".=1">0</xsl:when>
	<xsl:when test=".=2">1</xsl:when>
	<xsl:otherwise>2</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>