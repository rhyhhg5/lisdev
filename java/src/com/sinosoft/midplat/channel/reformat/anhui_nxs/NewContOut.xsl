<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">

<xsl:template match="/">
	<xsl:apply-templates select="TranData/LCCont"/>
</xsl:template>

<xsl:template match="TranData/LCCont">
<TXLife>
  <HOAppFormNumber><xsl:value-of select="substring(PrtNo, 5)"/></HOAppFormNumber>
  <ProviderPolicyNumber><xsl:value-of select="PrtNo"/></ProviderPolicyNumber>
  <ProviderFormNumber><xsl:value-of select="ProposalContNo"/></ProviderFormNumber>
  <PolNumber><xsl:value-of select="ContNo"/></PolNumber>

  <!--投保人-->
  <PolicyHolder>
    <GovtID><xsl:value-of select="LCAppnt/AppntIDNo"/></GovtID>
    <GovtIDTC>1</GovtIDTC>
    <FullName><xsl:value-of select="LCAppnt/AppntName"/></FullName>
    <Gender><xsl:apply-templates select="LCAppnt/AppntSex"/></Gender>
    <BirthDate><xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(LCAppnt/AppntBirthday, 'yyyy-MM-dd', 'yyyyMMdd')"/></BirthDate>
    
    <Line1></Line1>
    <DialNumber></DialNumber>
    <Zip></Zip>
    <RelatedToInsuredRoleName>本人</RelatedToInsuredRoleName>
  </PolicyHolder>
  
  <!--被保人-->
  <xsl:apply-templates select="LCInsureds"/>
  
  <PaymentAmt><xsl:value-of select="Prem"/></PaymentAmt>
  <PaymentAmtUper><xsl:value-of select="PremText"/></PaymentAmtUper>
  <ProductCode></ProductCode>
  <ModalPremAmt><xsl:value-of select="Prem"/></ModalPremAmt>
  <ModalPremAmtUper><xsl:value-of select="PremText"/></ModalPremAmtUper>
  <PlanName></PlanName>
  <SubmissionDate><xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(PolApplyDate, 'yyyy-MM-dd', 'yyyyMMdd')"/></SubmissionDate>
  <IntialNumberOfUnits>1</IntialNumberOfUnits>
  <AgeTouBao></AgeTouBao>
  <PaymentModeName>趸缴</PaymentModeName>
  <PaymentDuration ></PaymentDuration>
  <CpicTeller><xsl:value-of select="AgentCode"/></CpicTeller>
  <BankManagerCode></BankManagerCode>
  <BankManagerName></BankManagerName>
  <BeneficiaryIndicator>Y</BeneficiaryIndicator>
  <BeneficiaryCount>2</BeneficiaryCount>
  <Beneficiary1>
    <BeneficiaryMethodName />
    <GovtID />
    <FullName><xsl:value-of select="AgentCom"/></FullName>
    <Gender />
    <RelatedToInsuredRoleName />
    <InterestPercent />
  </Beneficiary1>
  <Beneficiary2>
    <BeneficiaryMethodName />
    <GovtID />
    <FullName>法定</FullName>
    <Gender />
    <RelatedToInsuredRoleName />
    <InterestPercent />
  </Beneficiary2>
  <Beneficiary3>
    <BeneficiaryMethodName />
    <GovtID />
    <FullName />
    <Gender />
    <RelatedToInsuredRoleName />
    <InterestPercent />
  </Beneficiary3>
  <Beneficiary4>
    <BeneficiaryMethodName />
    <GovtID />
    <FullName />
    <Gender />
    <RelatedToInsuredRoleName />
    <InterestPercent />
  </Beneficiary4>
  <Beneficiary5>
    <BeneficiaryMethodName />
    <GovtID />
    <FullName />
    <Gender />
    <RelatedToInsuredRoleName />
    <InterestPercent />
  </Beneficiary5>
  <Beneficiary6>
    <BeneficiaryMethodName />
    <GovtID />
    <FullName />
    <Gender />
    <RelatedToInsuredRoleName />
    <InterestPercent />
  </Beneficiary6>
  <PayoutStart></PayoutStart>
  <DivTypeName></DivTypeName>
  <BenefitModeName></BenefitModeName>
  <FirstPayOutDate></FirstPayOutDate>
  <BasePreCount>0</BasePreCount>
  <BasePre1>
    <BasePerName></BasePerName>
    <BasePeramount></BasePeramount>
  </BasePre1>
  <BasePre2>
    <BasePerName />
    <BasePeramount />
  </BasePre2>
  <BasePre3>
    <BasePerName />
    <BasePeramount />
  </BasePre3>
  <BasePre4>
    <BasePerName />
    <BasePeramount />
  </BasePre4>
  <FeeStd></FeeStd>
  <RatioStd></RatioStd>
  <FeeCon></FeeCon>
  <FeePro></FeePro>
  <PaymentEndAge></PaymentEndAge>
  <PaymentDueDate></PaymentDueDate>
  <FinalPaymentDate></FinalPaymentDate>
  <EffDate><xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(CValiDate, 'yyyy-MM-dd', 'yyyyMMdd')"/></EffDate>
  <TermDate><xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(ContEndDate, 'yyyy-MM-dd', 'yyyyMMdd')"/></TermDate>
  <CoverageCount>0</CoverageCount>
  <Extension1>
    <ProductCode />
    <ModalPremAmt />
    <PlanName>本栏空白</PlanName>
    <IntialNumberOfUnits />
    <FeeStd />
    <FeeCon />
    <FeePro />
    <BasePremAmt />
    <PaymentEndAge />
    <FinalPaymentDate />
    <TermDate />
    <PaymentModeName />
    <PayoutDuration />
    <BenefitModeName />
    <DivTypeName />
    <PayOutDate />
    <PayoutStart />
  </Extension1>
  <Extension2>
    <ProductCode />
    <ModalPremAmt />
    <PlanName />
    <IntialNumberOfUnits />
    <FeeStd />
    <FeeCon />
    <FeePro />
    <BasePremAmt />
    <PaymentEndAge />
    <FinalPaymentDate />
    <TermDate />
    <PaymentModeName />
    <PayoutDuration />
    <BenefitModeName />
    <DivTypeName />
    <PayOutDate />
    <PayoutStart />
  </Extension2>
  <Extension3>
    <ProductCode />
    <ModalPremAmt />
    <PlanName />
    <IntialNumberOfUnits />
    <FeeStd />
    <FeeCon />
    <FeePro />
    <BasePremAmt />
    <PaymentEndAge />
    <FinalPaymentDate />
    <TermDate />
    <PaymentModeName />
    <PayoutDuration />
    <BenefitModeName />
    <DivTypeName />
    <PayOutDate />
    <PayoutStart />
  </Extension3>
  <Extension4>
    <ProductCode />
    <ModalPremAmt />
    <PlanName />
    <IntialNumberOfUnits />
    <FeeStd />
    <FeeCon />
    <FeePro />
    <BasePremAmt />
    <PaymentEndAge />
    <FinalPaymentDate />
    <TermDate />
    <PaymentModeName />
    <PayoutDuration />
    <BenefitModeName />
    <DivTypeName />
    <PayOutDate />
    <PayoutStart />
  </Extension4>
  <CashValue1>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue1>
  <CashValue2>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue2>
  <CashValue3>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue3>
  <CashValue4>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue4>
  <CashValue5>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue5>
  <CashValue6>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue6>
  <CashValue7>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue7>
  <CashValue8>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue8>
  <CashValue9>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue9>
  <CashValue10>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue10>
  <CashValue11>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue11>
  <CashValue12>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue12>
  <CashValue13>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue13>
  <CashValue14>
    <Cash>0.00</Cash>
    <Year>第00保单年度末</Year>
  </CashValue14>
  <CpicAddr></CpicAddr>
  <CpicDialNumber></CpicDialNumber>
  <ResultInfo />
  <ContractInfo>
    <RiskAmt><xsl:value-of select="AmntText"/></RiskAmt>
    <TotalRiskAmt><xsl:value-of select="AmntText"/></TotalRiskAmt>
    <LoanBenefitDesc>贷款本息总额</LoanBenefitDesc>
    <LoanContVerifyNo></LoanContVerifyNo>
    <LoanContractNo><xsl:value-of select="ProposalContNo"/></LoanContractNo>	<!--贷款合同编号-->
    <LoanInvoiceNo></LoanInvoiceNo>	<!--贷款凭证编号-->
  </ContractInfo>
  <BeiYong />
</TXLife>
</xsl:template>

<xsl:template match="LCInsureds">
  	<Insured>
    	<GovtID><xsl:value-of select="LCInsured/IDNo"/></GovtID>
    	<GovtIDTC>1</GovtIDTC>
    	<FullName><xsl:value-of select="LCInsured/Name"/></FullName>
    	<Gender><xsl:apply-templates select="LCInsured/Sex"/></Gender>
    	<BirthDate><xsl:value-of select="java:com.sinosoft.midplat.channel.util.DateUtil.formatTrans(LCInsured/Birthday, 'yyyy-MM-dd', 'yyyyMMdd')"/></BirthDate>
    
    	<OccupationType></OccupationType>
  	</Insured>
</xsl:template>

<xsl:template match="Sex">
<xsl:choose>
	<xsl:when test=".=2">0</xsl:when>
	<xsl:when test=".=0">1</xsl:when>
	<xsl:when test=".=1">2</xsl:when>
	<xsl:otherwise>0</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="AppntSex">
<xsl:choose>
	<xsl:when test=".=2">0</xsl:when>
	<xsl:when test=".=0">1</xsl:when>
	<xsl:when test=".=1">2</xsl:when>
	<xsl:otherwise>0</xsl:otherwise>
</xsl:choose>
</xsl:template>
</xsl:stylesheet>