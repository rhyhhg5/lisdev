package com.sinosoft.midplat.channel.reformat.anhui_nxs;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.jdom.Document;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class RePrint extends BaseFormat {
	private final NewCont cNewCont = new NewCont();
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into RePrint.getStdXml()...");
		
		//设置头信息，以备返回时使用
		cNewCont.setHead(pNoStdXml);
		
		Document mStdXml = 
			AnHui_NXS_XslCache.getReprintIn().transform(pNoStdXml);
		
		cLogger.info("Out RePrint.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into RePrint.getNoStdXml()...");
		
		//保单重打和投保交易返回报文完全一样
		Document mNoStdXml = cNewCont.getNoStdXml(pStdXml);
		
		cLogger.info("Out RePrint.getNoStdXml()!");
		return mNoStdXml;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Request.xml";
		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Request_ST.xml";

//		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Response.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Response_NST.xml";
		
		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new RePrint().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
}
