package com.sinosoft.midplat.channel.reformat.anhui_nxs;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.NumberUtil;

public class NewCont extends BaseFormat {
	private final List cHeadList = new ArrayList();
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into NewCont.getStdXml()...");
		
		Element mTXLife = pNoStdXml.getRootElement();
		
		//小额信贷的第一受益人为贷款发放方，其余受益人必须为法定
		if (!"Y".equals(mTXLife.getChildTextTrim("BeneficiaryIndicator"))) {
			throw new BaseException("该险种受益人应为法定！");
		}
		
		setHead(pNoStdXml);
		
		Document mStdXml = 
			AnHui_NXS_XslCache.getNewContIn().transform(pNoStdXml);
		
		String mPath1 = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/InsuStartDate";
		String mPath2 = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/InsuEndDate";
		Element mInsuStartDate = (Element) XPath.selectSingleNode(mStdXml, mPath1);
		Element mInsuEndDate = (Element) XPath.selectSingleNode(mStdXml, mPath2);
		if (null != mInsuStartDate && null != mInsuEndDate) {
			int days = DateUtil.calInterval(mInsuStartDate.getText(), mInsuEndDate.getText(), "D");
			String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/InsuYear";
			Element mInsuYear = (Element) XPath.selectSingleNode(mStdXml, mPath);
			mInsuYear.setText(String.valueOf(days));
		}
		
		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);
		
		cLogger.info("Out NewCont.getStdXml()!");
		return mStdXml;
	}
	
	/**
	 * 该方法设置为同一个package内可访问，以便重打等可以使用，同时外面的类不能使用。
	 */
	void setHead(Document pXmlDoc) {
		Element mTXLife = pXmlDoc.getRootElement();
		cHeadList.add(
				mTXLife.getChild(TransNo).clone());
		cHeadList.add(
				mTXLife.getChild("QdBankCode").clone());
		cHeadList.add(
				mTXLife.getChild(BankCode).clone());
		cHeadList.add(
				mTXLife.getChild(Branch).clone());
		cHeadList.add(
				mTXLife.getChild(CarrierCode).clone());
		cHeadList.add(
				mTXLife.getChild(TransExeDate).clone());
		cHeadList.add(
				mTXLife.getChild(TransExeTime).clone());
		
		Element mTransRefGUID = mTXLife.getChild(TransRefGUID);
		cHeadList.add(mTransRefGUID.clone());
		
		Element mCpicWater = new Element("CpicWater");
		mCpicWater.setText(mTransRefGUID.getText());
		cHeadList.add(mCpicWater);
		
		cHeadList.add(
				mTXLife.getChild(Teller).clone());
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");
		
		Document mNoStdXml = null;
		
		Element mResultCode = new Element(ResultCode);
		Element mResultInfoDesc = new Element(ResultInfoDesc);
		Element mRetData = pStdXml.getRootElement().getChild(RetData);
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			Element tLCCont = pStdXml.getRootElement().getChild(LCCont);
			
			Element tPremText = new Element("PremText");
			tPremText.setText(
					NumberUtil.getChnMoney(tLCCont.getChildText(Prem)));
			tLCCont.addContent(tPremText);
			
			Element tAmntText = new Element("AmntText");
			tAmntText.setText(
					NumberUtil.getChnMoney(tLCCont.getChildText(Amnt)));
			tLCCont.addContent(tAmntText);
			
			//农信社方面打印保险止期格式：yyyy-MM-dd 24:00:00，故，需要在核心保险止期基础上减一天
			Element tContEndDateEle = tLCCont.getChild("ContEndDate");
			Date tTempDate = DateUtil.parseDate(tContEndDateEle.getText(), "yyyy-MM-dd");
			GregorianCalendar mInsuEndCalendar = new GregorianCalendar();
			mInsuEndCalendar.setTime(tTempDate);
			mInsuEndCalendar.add(GregorianCalendar.DAY_OF_MONTH, -1);
			tContEndDateEle.setText(
					DateUtil.getDateStr(mInsuEndCalendar, "yyyy-MM-dd"));
			
			mNoStdXml = 
				AnHui_NXS_XslCache.getNewContOut().transform(pStdXml);
			
			mResultCode.setText("00");
			mResultInfoDesc.setText("交易成功");
		} else {	//交易失败
			mNoStdXml = new Document(new Element(TXLife));
			
			mResultCode.setText("01");
			mResultInfoDesc.setText(
					mRetData.getChildText(Desc));
		}
		
		//设置报文头信息
		Element mTXLife = mNoStdXml.getRootElement();
		mTXLife.addContent(cHeadList);
		mTXLife.addContent(mResultCode);
		mTXLife.addContent(mResultInfoDesc);
		
		cLogger.info("Out NewCont.getNoStdXml()!");
		return mNoStdXml;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFilePath = "D:/华夏人寿/NXS/TestXml/NewCont/nxs_01_in_211610.xml";
		String mOutFilePath = "D:/华夏人寿/NXS/TestXml/NewCont/std_01_in_211610.xml";
		
//		String mInFilePath = "D:/华夏人寿/NXS/TestXml/NewCont/std_01_out_211610.xml";
//		String mOutFilePath = "D:/华夏人寿/NXS/TestXml/NewCont/nxs_01_out_211610.xml";
		
		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		
		Document mOutXmlDoc = new NewCont().getStdXml(mInXmlDoc);
//		Document mOutXmlDoc = 
//			AnHui_NXS_XslCache.getNewContOut().transform(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
}