package com.sinosoft.midplat.channel.reformat.anhui_nxs;

import org.jdom.Document;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class ContConfirm extends BaseFormat {
	private final NewCont cNewCont = new NewCont();
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into ContConfirm.getStdXml()...");
		
		//设置头信息，以备返回时使用
		cNewCont.setHead(pNoStdXml);
		
		Document mStdXml = 
			AnHui_NXS_XslCache.getContConfirmIn().transform(pNoStdXml);
		
		cLogger.info("Out ContConfirm.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into ContConfirm.getNoStdXml()...");
		
		//收费和投保交易返回报文完全一样
		Document mNoStdXml = cNewCont.getNoStdXml(pStdXml);
		
		cLogger.info("Out ContConfirm.getNoStdXml()!");
		return mNoStdXml;
	}
}
