package com.sinosoft.midplat.channel.reformat.icbc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.NumberUtil;

public class NewCont extends BaseFormat {
    private int count; //工行需要判断裁剪后的现金价值的数目.
    private String mBasePAmt ;
    private String mRiderPAmt ;
    
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into NewCont.getStdXml()...");

		InputStream mSheetIs = getClass().getResourceAsStream("NewContIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);

		Element mTranData = mStdXml.getRootElement();
		Element mLCCont = mTranData.getChild(LCCont);
		String mPath = "/TranData/LCCont/LCAppnt/AppntName";
		Element mAppntName = (Element) XPath.selectSingleNode(mStdXml, mPath);
        mAppntName.setText(mAppntName.getText().replaceAll("'", "＇"));
		if (null != mAppntName) {
			mLCCont.getChild(AccName).setText(mAppntName.getText());
		}
		mLCCont.getChild(AccBankCode).setText("01");

		mPath = "/TranData/LCCont/LCAppnt/MailAddress";
		Element mAppntMailAddress = (Element) XPath.selectSingleNode(mStdXml, mPath);
		if (mAppntMailAddress.getTextTrim().equals("")) {
			mAppntMailAddress.setText(
					mAppntMailAddress.getParentElement().getChildTextTrim(HomeAddress));
		}

		mPath = "/TranData/LCCont/LCAppnt/MailZipCode";
		Element mAppntMailZipCode = (Element) XPath.selectSingleNode(mStdXml, mPath);
		if (mAppntMailZipCode.getTextTrim().equals("")) {
			mAppntMailZipCode.setText(
					mAppntMailZipCode.getParentElement().getChildTextTrim(HomeZipCode));
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/MailAddress";
		List mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tMailAddress = (Element) mList.get(i);
			if (tMailAddress.getTextTrim().equals("")) {
				tMailAddress.setText(
						tMailAddress.getParentElement().getChildTextTrim(HomeAddress));
			}
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/MailZipCode";
		mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tMailZipCode = (Element) mList.get(i);
			if (tMailZipCode.getTextTrim().equals("")) {
				tMailZipCode.setText(
						tMailZipCode.getParentElement().getChildTextTrim(HomeZipCode));
			}
		}
        
        mPath = "/TranData/LCCont/LCInsureds/LCInsured/Name";
        mList = XPath.selectNodes(mStdXml, mPath);
        for (int i = 0; i < mList.size(); i++) {
            Element mName = (Element) mList.get(i);
            mName.setText(mName.getText().replaceAll("'", "＇"));
        }

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/Amnt";
		mList = XPath.selectNodes(mStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tAmnt = (Element) mList.get(i);
			if(tAmnt!=null && tAmnt.getTextTrim()!=null && !"".equals(tAmnt.getTextTrim()) ){
				tAmnt.setText(NumberUtil.fenToYuan(tAmnt.getTextTrim()));
			}

		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/TellInfos/TellInfo";
		mList = XPath.selectNodes(mStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tTellInfo = (Element) mList.get(i);
			if(tTellInfo.getChildText("TellCode") != null 
					&& "001".equals(tTellInfo.getChildText("TellCode")) 
					&& "Y".equals(tTellInfo.getChildText("TellContent"))) {
				throw new BaseException("因存在健康告知，故该单不能承保!");
			}

			if(tTellInfo.getChildText("TellCode") != null 
					&& "002".equals(tTellInfo.getChildText("TellCode")) 
					&& "Y".equals(tTellInfo.getChildText("TellContent"))) {
				throw new BaseException("因存在职业告知，故该单不能承保!");
			}
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/Prem";
		mList = XPath.selectNodes(mStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tPrem = (Element) mList.get(i);
			if(tPrem!=null && tPrem.getTextTrim()!=null && !"".equals(tPrem.getTextTrim()) ){
				tPrem.setText(NumberUtil.fenToYuan(tPrem.getTextTrim()));
			}
		}
		
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/SupplementaryPrem";
		mList = XPath.selectNodes(mStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tSupplementaryPrem = (Element) mList.get(i);
			if(tSupplementaryPrem!=null && tSupplementaryPrem.getTextTrim()!=null && !"".equals(tSupplementaryPrem.getTextTrim()) ){
				tSupplementaryPrem.setText(NumberUtil.fenToYuan(tSupplementaryPrem.getTextTrim()));
			}
		}

		/*Start-将工行关系转换为核心能支持的标准关系*/
		mPath = "/TranData/LCCont/LCInsureds/LCInsured";
		mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCInsured = (Element) mList.get(i);

			Element tRelaToAppnt = tLCInsured.getChild(RelaToAppnt);
			String tIcbcRelation = getStdRelation(
					tRelaToAppnt.getTextTrim(), tLCInsured.getChildTextTrim(Sex));
			tRelaToAppnt.setText(tIcbcRelation);
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf";
		mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf = (Element) mList.get(i);

			Element tRelationToInsured = tLCBnf.getChild(RelationToInsured);
			String tIcbcRelation = getStdRelation(
					tRelationToInsured.getTextTrim(), tLCBnf.getChildTextTrim(Sex));
			tRelationToInsured.setText(tIcbcRelation);
		}
		/*End-将工行关系转换为核心能支持的标准关系*/

		/**
		 * add by wangxt in 2009-5-6 begin
		 * 如果主险代码是 230701 时,组织一个附加险 331401
		 */
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(mStdXml, mPath);
		Element mRisk = mRisks.getChild(Risk);
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {//安心宝
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
		}
		/**
		 * add by wangxt in 2009-5-6 end
		 */

		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);

		cLogger.info("Out NewCont.getStdXml()!");
		return mStdXml;
	}

	/**
	 * 关系映射的转换
	 * @author zhangd 20080222
	 * @return 
	 */
	private  String getStdRelation(String relation , String sex){
		String tranRelation = null;
		if (relation.equals("1")){
			cLogger.debug("配偶关系转换");
			if (sex.equals("0")){
				tranRelation = "02";
			}else if (sex.equals("1")){
				tranRelation = "01";
			}else {
				tranRelation = "02";
			}
		}else if (relation.equals("2")){
			cLogger.debug("父母关系转换");
			if (sex.equals("0")){
				tranRelation = "04";
			}else if (sex.equals("1")){
				tranRelation = "05";
			}else {
				tranRelation = "04";
			}
		}else if (relation.equals("3")){
			cLogger.debug("子女关系转换");
			tranRelation = "03";
		}else if (relation.equals("4")){
			cLogger.debug("祖父祖母关系转换");
			if (sex.equals("0")){
				tranRelation = "19";
			}else if (sex.equals("1")){
				tranRelation = "20";
			}else {
				tranRelation = "19";
			}
		}else if (relation.equals("5")){
			cLogger.debug("孙子女关系转换");
			tranRelation = "21";
		}else if (relation.equals("6")){
			cLogger.debug("兄弟姐妹关系转换");
			tranRelation = "12";
		}else if (relation.equals("7")){
			cLogger.debug("其他亲属关系转换");
			tranRelation = "25";
		}else if (relation.equals("8")){
			cLogger.debug("本人关系转换");
			tranRelation = "00";
		}else if (relation.equals("9")){
			cLogger.debug("朋友关系转换");
			tranRelation = "27";
		}else if (relation.equals("99")){
			cLogger.debug("兄弟姐妹关系转换");
			tranRelation = "30";
		}else {
			tranRelation = relation;
		}

		return tranRelation;
	}

	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");

		Element mTranData = pStdXml.getRootElement();
		Element mRetData = mTranData.getChild(RetData);
		Element mLCCont = mTranData.getChild(LCCont);

		Element mTXLife = new Element(TXLife);

		Element mTXLifeResponse = new Element(TXLifeResponse);
		mTXLife.addContent(mTXLifeResponse);

		//头部
		Element mTransRefGUID = new Element(TransRefGUID);
		mTXLifeResponse.addContent(mTransRefGUID);

		Element mTransType = new Element(TransType);
		mTXLifeResponse.addContent(mTransType);

		Element mTransExeDate = new Element(TransExeDate);
		mTXLifeResponse.addContent(mTransExeDate);

		Element mTransExeTime = new Element(TransExeTime);
		mTXLifeResponse.addContent(mTransExeTime);

		Element mTransResult = new Element(TransResult);
		mTXLifeResponse.addContent(mTransResult);

		Element mResultCode = new Element(ResultCode);
		mTransResult.addContent(mResultCode);

		Element mResultInfo = new Element(ResultInfo);
		mTransResult.addContent(mResultInfo);

		Element mResultInfoDesc = new Element(ResultInfoDesc);
		mResultInfo.addContent(mResultInfoDesc);

		if(mRetData.getChildText(Flag).equals("0")) {	//交易出错
			mResultCode.setText("1001");
			mResultInfoDesc.setText(mRetData.getChildText(Desc));

			return new Document(mTXLife);
		} else {
			mResultCode.setText("0000");
			mResultInfoDesc.setText("交易成功！");
		}
		//带附加险时后置机返回的报文中险种顺序不定，所以此处处理把主险放到第一位
		Element pRisks = mLCCont.getChild(LCInsureds).getChild(LCInsured).getChild(Risks);
		List mList = pRisks.getChildren(Risk);
	
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk  = (Element) mList.get(i);
			String tMainRiskCode = tRisk.getChildText(MainRiskCode);
			String tRiskCode = tRisk.getChildText(RiskCode);
			if(tMainRiskCode.equals(tRiskCode)){
				Element tRiskCopy = (Element)tRisk.clone();
				Element tRisk1  = (Element) mList.get(0);
				Element tRiskCopy1 = (Element)tRisk1.clone();
				mList.remove(0);
				mList.add(0, tRiskCopy);
				mList.remove(i);
				mList.add(i, tRiskCopy1);				
			}
		}
		/**
		 * add by wangxt in 2009-5-26 begin
		 * 取得总保费
		 */
		String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(pStdXml, mPath);
		Element mRisk = mRisks.getChild(Risk);
		String tRiskCode = mRisk.getChildText(RiskCode);
		if(!"730101".equals(tRiskCode)&&!"332401".equals(tRiskCode)){
		mRisk.getChild(Prem).setText(mLCCont.getChildText(Prem));
		}
		//万能C要返回追加保费和总保费之和,所以在这里进行处理
		String tMainRiskCodeStr = mRisk.getChildText(MainRiskCode);
		Element tLCContPrem = 
			mRisk.getParentElement().getParentElement().getParentElement().getParentElement().getChild(Prem);
		if (tMainRiskCodeStr.equals("331201")	//健康人生个人护理保险(万能型，B款)
				|| tMainRiskCodeStr.equals("331301")	//健康人生个人护理保险(万能型，C款)
				|| tMainRiskCodeStr.equals("331601")	//健康人生个人护理保险(万能型，C款)
				|| tMainRiskCodeStr.equals("230701")    //健康专家个人防癌疾病保险
				|| tMainRiskCodeStr.equals("240501")	//安心宝个人终身重大疾病保险
				|| tMainRiskCodeStr.equals("331701")	//金利宝个人护理保险（万能型）
				|| tMainRiskCodeStr.equals("331901")) { //康利相伴个人护理保险（万能型）
			mRisk.getChild(Prem).setText(tLCContPrem.getText());	//银行方1个险种对应系统内两险种，银行要求返回总保费
		}

		if (tMainRiskCodeStr.equals("331301")) {	//picch万能C有个追加保费，需要累加上去
			double ttTotalPrem = 
				Double.parseDouble(tLCContPrem.getText()) + Double.parseDouble(mRisk.getChildText(SupplementaryPrem));
			tLCContPrem.setText(new DecimalFormat("0.00").format(ttTotalPrem));
		}
		
		if(tMainRiskCodeStr.equals("240501")){//安心宝
			mRisk.getChild(RiskName).setText("安心宝个人终身重大疾病保障计划");
			mLCCont.getChild(SpecContent).setText("安心宝个人终身重大疾病保障计划是由《安心宝个人终身重大疾病保险》、《附加安心宝个人护理保险》组成。");
			mRisk.getChild(InsuYear).setText("1000"); //终身.
		}
		if(tMainRiskCodeStr.equals("331701")){//金利宝
			mRisk.getChild(RiskName).setText("金利宝个人护理增值计划（万能型）");
			mLCCont.getChild(SpecContent).setText("金利宝个人护理增值计划（万能型）是由《金利宝个人护理保险（万能型）》、《附加金利宝个人意外伤害保险》组成。");
			mRisk.getChild(Amnt).setText("0.0");//此处不能直接置为"详见条款规定",因为654行会对Amnt转换成double,如果直接置,会报错.
		}
		
		Element mSupplementaryPrem = mRisk.getChild(SupplementaryPrem);
		mSupplementaryPrem.setText(
				NumberUtil.yuanToFen(mSupplementaryPrem.getText()));
		/**
		 * add by wangxt in 2009-5-26 end
		 */

		Element mOLifE = getOLifE(mLCCont);
		mTXLifeResponse.addContent(mOLifE);

		Element mOLifEExtension = new Element(OLifEExtension);
		mTXLifeResponse.addContent(mOLifEExtension);
		mOLifEExtension.setAttribute("VendorCode", "1");

		Element mPingAnDepNo = new Element("PingAnDepNo");
		mOLifEExtension.addContent(mPingAnDepNo);
		mPingAnDepNo.setText(mLCCont.getChildText("AgentGroup"));

		Element mPingAnAgentNo = new Element("PingAnAgentNo");
		mOLifEExtension.addContent(mPingAnAgentNo);
		mPingAnAgentNo.setText(
				mLCCont.getChildText(AgentName)
				+ "(" + mLCCont.getChildText(AgentCode) + ")");

		Element mBasePlanPrintInfo1 = new Element("BasePlanPrintInfo1");
		mOLifEExtension.addContent(mBasePlanPrintInfo1);

		Element mBasePlanPrintInfo2 = new Element("BasePlanPrintInfo2");
		mOLifEExtension.addContent(mBasePlanPrintInfo2);

		Element mBasePlanPrintInfo3 = new Element("BasePlanPrintInfo3");
		mOLifEExtension.addContent(mBasePlanPrintInfo3);

		Element mBasePlanPrintInfo4 = new Element("BasePlanPrintInfo4");
		mOLifEExtension.addContent(mBasePlanPrintInfo4);
		
	
		if("240501".equals(tMainRiskCodeStr)){//安心宝
            //生成安心宝现金价值链的备注打印节点
            Element mBasePlanPrintInfo5 = new Element("BasePlanPrintInfo5");
            Element mBasePlanPrintInfo6 = new Element("BasePlanPrintInfo6");
            Element mBasePlanPrintInfo7 = new Element("BasePlanPrintInfo7");
            Element mBasePlanPrintInfo8 = new Element("BasePlanPrintInfo8");
            Element mBasePlanPrintInfo9 = new Element("BasePlanPrintInfo9");
            
			mBasePlanPrintInfo5.setText("备注：");
			mBasePlanPrintInfo6.setText("1.此现金价值表是本公司按照中国保险监督管理委员会的相关规定计算确定的，其他未列明的保单");
			mBasePlanPrintInfo7.setText("年度末的现金价值可向我公司客户服务热线95591或4006695518咨询。");
			mBasePlanPrintInfo8.setText("2.保单年度末解除合同时，现金价值根据表中对应保单年度末的现金价值计算；其他时间解除合同");
			mBasePlanPrintInfo9.setText("时，以本现金价值表为基础，按照我公司规定的计算方法确定。");
			mOLifEExtension.addContent(mBasePlanPrintInfo5);
			mOLifEExtension.addContent(mBasePlanPrintInfo6);
			mOLifEExtension.addContent(mBasePlanPrintInfo7);
			mOLifEExtension.addContent(mBasePlanPrintInfo8);
			mOLifEExtension.addContent(mBasePlanPrintInfo9);
		}
		
		Document mNoStdXml = new Document(mTXLife);

		cLogger.info("Out NewCont.getNoStdXml()!");
		return mNoStdXml;
	}

	private Element getOLifE(Element pLCCont) throws Exception {
		cLogger.debug("Into NewCont.getOLifE()...");

		Element mOLifE = new Element(OLifE);

		//险种
		Element mPolicy = getPolicy(pLCCont);
		
		//投资帐户
		Element mInvestment = getInvestment();
		
		//钞汇标志
		Element mOLifEExtension = getOLifEExtension();

		Element mHolding_1 = new Element(Holding);
		mHolding_1.setAttribute("id", "Holding_1");
		mHolding_1.addContent(mPolicy);
		mHolding_1.addContent(mInvestment);
		mHolding_1.addContent(mOLifEExtension);
		mOLifE.addContent(mHolding_1);

		//帐户
//		Element mAccountNumber = new Element("AccountNumber");
//		mAccountNumber.setText(pLCCont.getChildText(BankAccNo));
//
//		Element mBanking = new Element("Banking");
//		mBanking.addContent(mAccountNumber);
//
//		Element mHolding_Acct_1 = new Element(Holding);
//		mHolding_Acct_1.setAttribute("id", "Acct_1");
//		mHolding_Acct_1.addContent(mBanking);
//		mOLifE.addContent(mHolding_Acct_1);

		//保单合同印刷号
		Element mFormName = new Element(FormName);
		mFormName.setText("2");

		Element mDocumentControlNumber = new Element("DocumentControlNumber");
		mDocumentControlNumber.setText(pLCCont.getChildText(ProposalContNo));

		Element mDescription = new Element("Description");
		Element mAttachmentData = new Element("AttachmentData");
		Element mAttachmentType = new Element("AttachmentType");
		Element mAttachmentLocation = new Element("AttachmentLocation");

		Element mAttachment = new Element("Attachment");
		mAttachment.addContent(mDescription);
		mAttachment.addContent(mAttachmentData);
		mAttachment.addContent(mAttachmentType);
		mAttachment.addContent(mAttachmentLocation);

		Element mFormInstance = new Element("FormInstance");
		mFormInstance.setAttribute("id", "Form_2");
		mFormInstance.addContent(mFormName);
		mFormInstance.addContent(mDocumentControlNumber);
		mFormInstance.addContent(mAttachment);
		mOLifE.addContent(mFormInstance);

		//投保人信息
		Element mParty_1 = getAppntParty(pLCCont.getChild(LCAppnt));
		mOLifE.addContent(mParty_1);

		//被保人信息(目前只处理单被保人情况)
		Element mLCInsured = pLCCont.getChild(LCInsureds).getChild(LCInsured);
		Element mParty_2 = getInsuredParty(mLCInsured);
		mOLifE.addContent(mParty_2);

		//循环处理多受益人
		String mPath = "Risks/Risk/LCBnfs/LCBnf";
		List mList = XPath.selectNodes(mLCInsured, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf = (Element) mList.get(i);

			Element tParty = getBnfParty(tLCBnf, i+1);	//为了便于理解，编号从1开始，所以i+1
			mOLifE.addContent(tParty);
		}
        
		//保险公司信息
		Element mParty_CAR_1 = getCARParty(pLCCont);
		mOLifE.addContent(mParty_CAR_1);

		//投保人关系
		Element mAppntRelation = getAppntRelation(pLCCont.getChild(LCAppnt));
		mOLifE.addContent(mAppntRelation);

		//被保人关系
		List mInsuredRelationList = getInsuredRelation(mLCInsured);
		mOLifE.addContent(mInsuredRelationList);

		//受益人关系
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf = (Element) mList.get(i);

			List tRelationList = getBnfRelation(tLCBnf, i+1);	//为了便于理解，编号从1开始，所以i+1
			mOLifE.addContent(tRelationList);
		}
		
		Element mTempBnf = getTempBnf();
		mOLifE.addContent(mTempBnf);
		
		Element mCarRelation = getCarRelation();
		mOLifE.addContent(mCarRelation);

		cLogger.debug("Out NewCont.getOLifE()!");
		return mOLifE;
	}

	private Element getTempBnf() {
		
		Element mParty = new Element("Party").setAttribute("id", "Party_5");
		
		return mParty;
	}

	private Element getOLifEExtension() {
		cLogger.debug("Into NewCont.getOLifEExtension()...");
		
		Element mOLifEExtension = new Element("OLifEExtension");
		mOLifEExtension.setAttribute("VendorCode", "3");
		
		Element mCashEXF = new Element("CashEXF"); //钞汇标志
		mOLifEExtension.addContent(mCashEXF);
		
		Element mInvestDateInd = new Element("InvestDateInd"); //钞汇标志
		mOLifEExtension.addContent(mInvestDateInd);
		
		cLogger.debug("Out NewCont.getOLifEExtension()...");
		return mOLifEExtension;
	}

	private Element getInvestment() {
		cLogger.debug("Into NewCont.getInvestment()...");
		
		Element mInvestment = new Element("Investment");
		mInvestment.setAttribute("id", "Invest_1");
		
		Element mSubAccountCount = new Element("SubAccountCount");//投资帐户个数
		Element mSubAccount = new Element("SubAccount"); 
		mSubAccount.setAttribute("id", "SubAcct_1");
		
		Element mProductCode = new Element("ProductCode"); //投资帐户代码
		mSubAccount.addContent(mProductCode);
		Element mProductFullName = new Element("ProductFullName"); //投资账户名称
		mSubAccount.addContent(mProductFullName);
		Element mAllocPercent = new Element("AllocPercent"); //投资账户比率
		mSubAccount.addContent(mAllocPercent);
		
		mInvestment.addContent(mSubAccountCount);
		mInvestment.addContent(mSubAccount);
		cLogger.debug("Out NewCont.getInvestment()...");
		return mInvestment;
	}

	private Element getPolicy(Element pLCCont) throws Exception {
		cLogger.debug("Into NewCont.getPolicy()...");

		Element mPolNumber = new Element(PolNumber);
		mPolNumber.setText(pLCCont.getChildText(ContNo));

//		Element mEffDate = new Element("EffDate");
//		mEffDate.setText(pLCCont.getChildText(CValiDate));

		Element mProductCode = new Element(ProductCode);
		String mPath = "LCInsureds/LCInsured/Risks/Risk/MainRiskCode";
		Element mMainRiskCode = (Element) XPath.selectSingleNode(pLCCont, mPath);
		mProductCode.setText(
				getIcbcRiskCode(mMainRiskCode.getText()));

		Element mEffDate = new Element(EffDate);
		mEffDate.setText(pLCCont.getChildText(CValiDate));

		Element mPaymentMode = new Element(PaymentMode);
		mPaymentMode.setText(
				getIcbcPayIntv(pLCCont.getChildText(PayIntv)));

		Element mPaymentMethod = new Element(PaymentMethod);	//picch统一使用“银行转账”
		mPaymentMethod.setText("1"); //工行接口文档 ‘1’ 为银行转账
		mPaymentMethod.setAttribute("tc", "1");

		Element mPaymentAmt = new Element(PaymentAmt);
		mPaymentAmt.setText(
				NumberUtil.yuanToFen(pLCCont.getChildText(Prem))
//				pLCCont.getChildText(Prem) //2012年5月13日修改，工行模板以元为单位
		);

		Element mLCInsured = pLCCont.getChild(LCInsureds).getChild(LCInsured);
		Element mRisks = mLCInsured.getChild(Risks);

		Element mLife = getLife(mRisks);

		Element mHOAppFormNumber = new Element(HOAppFormNumber);	//投保书印刷号
		mHOAppFormNumber.setText(pLCCont.getChildText(PrtNo));

		Element mSubmissionDate = new Element("SubmissionDate");
		mSubmissionDate.setText(pLCCont.getChildText("SignDate"));  //投保日期 

		Element mApplicationInfo = new Element(ApplicationInfo);
		mApplicationInfo.addContent(mHOAppFormNumber);
		mApplicationInfo.addContent(mSubmissionDate);

		Element mSpecialClause = new Element("SpecialClause");	//特别约定
		mSpecialClause.setText(pLCCont.getChildText(SpecContent));

		Element mSpecialClausePrtInd = new Element("SpecialClausePrtInd");	//特别约定打印标志
		mSpecialClausePrtInd.setText("1");

		Element mCashValueIndicator = new Element("CashValueIndicator");	//现金价值打印标志
//		不能写死为1,否则不需打印现金价值的险种也会有空白的现金价值打印.
        
        cLogger.debug("裁剪后的现价数目为:"+count);
        if (0 == count) {
            mCashValueIndicator.setText("0");
        } else {
            mCashValueIndicator.setText("1");//现价节点数量大于0时,打印现金价值链
        }
    //730101分红险打现价
        if("730101".equals(mMainRiskCode.getText())){
        	mCashValueIndicator.setText("1");
        }
		Element mPaymentAmtText = new Element("PaymentAmtText");	//保费大写
		mPaymentAmtText.setText(
				NumberUtil.getChnMoney(pLCCont.getChildText(Prem)));
		
		/**
		 * add by wangxt in 2009-7-16 begin
		 * 由于工行端重打和新契约的报文追加保费放的位置不一样,所以现在做如下调整
		 */
		Element mRisk = mRisks.getChild(Risk);
		Element mFirstSuperaddAmt = new Element("FirstSuperaddAmt");
		double addAmt = Double.parseDouble(mRisk.getChildText(SupplementaryPrem));
		if(addAmt != 0){
			mFirstSuperaddAmt.setText(addAmt+"");
		}
		/**
		 * add by wangxt in 2009-7-16 end
		 */
		
		Element mOLifEExtension = new Element(OLifEExtension);
		mOLifEExtension.setAttribute("VendorCode", "2");
		mOLifEExtension.addContent(mSpecialClause);
		mOLifEExtension.addContent(mSpecialClausePrtInd);
		mOLifEExtension.addContent(mCashValueIndicator);
		mOLifEExtension.addContent(mPaymentAmtText);
		mOLifEExtension.addContent(mFirstSuperaddAmt);
		mOLifEExtension.addContent(new Element("CashValueDecLeft"));//现金价值列表描述左下
		mOLifEExtension.addContent(new Element("CashValueDecRight"));//现金价值列表描述右下
		mOLifEExtension.addContent(new Element("FinFunction"));//理财功能选择
		mOLifEExtension.addContent(new Element("FixedProfitPercent"));//锁定收益（%）
		mOLifEExtension.addContent(new Element("WithdrawalDescription"));//犹豫期退保说明
		

		Element mPolicy = new Element(Policy).setAttribute("carrierPartyId", "CAR_PTY_1");
		mPolicy.addContent(mPolNumber);
		mPolicy.addContent(mProductCode);
		mPolicy.addContent(mEffDate);
		mPolicy.addContent(mPaymentMode);
		mPolicy.addContent(mPaymentMethod);
		mPolicy.addContent(mPaymentAmt);
		mPolicy.addContent(mLife);
		mPolicy.addContent(mApplicationInfo);
		mPolicy.addContent(mOLifEExtension);

		cLogger.debug("Out NewCont.getPolicy()!");
		return mPolicy;
	}

	private Element getLife(Element pRisks) throws Exception {
		cLogger.debug("Into NewCont.getLife()...");

		List mRiskList = pRisks.getChildren(Risk);
		Element mLife = new Element(Life);

		Element mFaceAmt = new Element(FaceAmt);
		String mLCContAmntStr = pRisks.getParentElement().getParentElement().getParentElement().getChildText(Amnt);
		if (Double.parseDouble(mLCContAmntStr) > 0.0) {
			mFaceAmt.setText(mLCContAmntStr + "元");
		} else {	//针对无保额的险种，即保额为0.0
			mFaceAmt.setText("详见条款规定");
		}
		mLife.addContent(mFaceAmt);

		Element mDivType = new Element("DivType");	//红利领取方式
		mLife.addContent(mDivType);

		Element mCoverageCount = new Element(CoverageCount);
		mCoverageCount.setText(
				String.valueOf(mRiskList.size()));
		mLife.addContent(mCoverageCount);

		//循环处理险种
		for (int i = 0; i < mRiskList.size(); i++) {
			Element tRisk = (Element) mRiskList.get(i);

			Element tCoverage = getCoverage(tRisk, i+1);	//编号从1开始，所以i+1
			mLife.addContent(tCoverage);
		}

		Element mBasePremAmt = new Element("BasePremAmt");
//		mBasePremAmt.setText(mBasePAmt);//主险保费总额

		Element mRiderPremAmt = new Element("RiderPremAmt");
//		mRiderPremAmt.setText(mRiderPAmt);  //附加险保费总额

		Element mPaymentModeDec = new Element("PaymentModeDec");
		
		Element mRisk = pRisks.getChild(Risk);
		Element mFirstSuperaddAmt = new Element("FirstSuperaddAmt");
		double addAmt = Double.parseDouble(mRisk.getChildText(SupplementaryPrem));
		if(addAmt != 0){
			mFirstSuperaddAmt.setText(addAmt+"");
		}
		
		Element mOLifEExtension = new Element(OLifEExtension).setAttribute("VendorCode","11");
		mOLifEExtension.addContent(mBasePremAmt);
		mOLifEExtension.addContent(mRiderPremAmt);
		mOLifEExtension.addContent(mPaymentModeDec);
		mOLifEExtension.addContent(mFirstSuperaddAmt);
		mLife.addContent(mOLifEExtension);
		
		cLogger.debug("Out NewCont.getLife()!");
		return mLife;
	}

	private Element getCoverage(Element pRisk, int pIndex) throws Exception {
		cLogger.debug("Into NewCont.getCoverage()...");

		Element mPlanName = new Element(PlanName);
		mPlanName.setText(pRisk.getChildText(RiskName));

		Element mProductCode = new Element(ProductCode);
		mProductCode.setText(
				getIcbcRiskCode(pRisk.getChildText(RiskCode)));

		Element mLifeCovTypeCode = new Element(LifeCovTypeCode);
		mLifeCovTypeCode.setText("9");	//非终身寿险

		Element mIndicatorCode = new Element(IndicatorCode);	//主副险标志(1-主险； 2-附险)
		if (pRisk.getChildText(RiskCode).equals(pRisk.getChildText(MainRiskCode))) {
			mIndicatorCode.setText("1");
			mIndicatorCode.setAttribute("tc", "1");
			
			mBasePAmt = pRisk.getChildText("Prem");
		} else {
			mIndicatorCode.setText("2");
			mIndicatorCode.setAttribute("tc", "2");
			mRiderPAmt = pRisk.getChildText("Prem");
		}

		Element mPaymentMode = new Element(PaymentMode);
		String mPayIntvStr = pRisk.getChildText(PayIntv);
		mPaymentMode.setText(
				getIcbcPayIntv(mPayIntvStr));

		Element mInitCovAmt = new Element(InitCovAmt);
		String mAmntStr = pRisk.getChildText(Amnt);
		if (Double.parseDouble(mAmntStr) > 0.0) {
			mInitCovAmt.setText(mAmntStr + "元");
		} else {	//针对无保额的险种，即保额为0.0
			mInitCovAmt.setText("详见条款规定");
		}

		Element mIntialNumberOfUnits = new Element(IntialNumberOfUnits);
		String mMultStr = pRisk.getChildText(Mult);
		if (Double.parseDouble(mMultStr) > 0.0) {
			mIntialNumberOfUnits.setText(mMultStr);
		} else {	//针对无份数的险种，即份数为0.0
			mIntialNumberOfUnits.setText("--");
		}

		Element mModalPremAmt = new Element(ModalPremAmt);
		mModalPremAmt.setText(
				NumberUtil.yuanToFen(pRisk.getChildText(Prem)));

//		Element mFirstSuperaddAmt = new Element("FirstSuperaddAmt");
//		mFirstSuperaddAmt.setText(pRisk.getChildText(SupplementaryPrem));

		Element mEffDate = new Element(EffDate);
		mEffDate.setText(pRisk.getChildText(CValiDate));

		Element mFinalPaymentDate = new Element(FinalPaymentDate);
		mFinalPaymentDate.setText(pRisk.getChildText(PayEndDate));

		//--
		Element mCashValues = (Element) pRisk.getChild(CashValues).detach();
		
		//添加对现金价值链的操作,因为安心宝返回的现金价值表过长,做一个规则裁剪
		//yinjj
		if("240501".equals(pRisk.getChildText(RiskCode))){
			List mList = mCashValues.getChildren();
			Iterator i = mList.iterator();
			cLogger.debug("mList = mCashValues.getChildren()"+mList.size());
			
			int mOldCount = mList.size() -1;//要求返回的现金价值链中最后一条现价必须返回
			int flag = 0;//第一条是现金价值链的统计数目.不对第一个子节点进行操作.
			while (i.hasNext()) {

				if (flag != 0) {

					Element mCashValue = (Element) i.next();
					int j = 0;//判断end值
//					mCashValue.addContent(new Element("Live"));  //  生存年金
					mCashValue.addContent(new Element("IllDeathBenefitAmt"));  //  疾病身故保险金
					mCashValue.addContent(new Element("AciDeathBenefitAmt"));  // 意外身故保险金
					mCashValue.addContent(new Element("Year"));  // 保单年度
					mCashValue.addContent(new Element("PaidAmt"));  // 交清保额
					mCashValue.addContent(new Element("CashValueDescription"));  // 现金价值表说明
					
					
					String m = mCashValue.getChildText("End");
					j = Integer.parseInt(m);
					if ((j > 5) && (j % 5 != 0)&&(j<mOldCount)) {
						i.remove();
						cLogger.debug("i.remove(),i=" + i);
					}

				} else {
					i.next();//如果是第一个自己点,不做处理,转到下一条子节点.
				}

				flag++;
			}
			count = mList.size() - 1;//操作后剩余的价值链子节点数量
			mCashValues.getChild(CashValueCount).setText(String.valueOf(count));
			
			
			//end 现价裁剪
		}else{
			
			List tCashValueList = mCashValues.getChildren("CashValue");
			for (int i = 0; i < tCashValueList.size(); i++) {
				Element mCashValue = (Element) tCashValueList.get(i);
				mCashValue.addContent(new Element("Live"));  //  生存年金
				mCashValue.addContent(new Element("IllDeathBenefitAmt"));  //  疾病身故保险金
				mCashValue.addContent(new Element("AciDeathBenefitAmt"));  // 意外身故保险金
				mCashValue.addContent(new Element("Year"));  // 保单年度
				mCashValue.addContent(new Element("PaidAmt"));  // 交清保额
				mCashValue.addContent(new Element("CashValueDescription"));  // 现金价值表说明
				
			}
		}
		mCashValues.addContent(1,new Element("SurrenderPctFY")); //首年退保比例 
		mCashValues.addContent(2,new Element("SurrenderPctSY")); //第二年退保比例
		
		
		Element mPaymentDurationMode = new Element(PaymentDurationMode);
		String mPayEndYearFlagStr = mPayIntvStr;
		mPaymentDurationMode.setText(
//				getIcbcPayEndYearFlag(mPayEndYearFlagStr));
				getIcbcPayIntv(mPayEndYearFlagStr));

		Element mPaymentDuration = new Element(PaymentDuration);
//		String mPayEndYearStr = pRisk.getChildText(PayEndYear);
		String mPayYearsStr = pRisk.getChildText(PayYears);  //update 2012-5-17 
		System.out.println("######===="+mPayIntvStr);
		if (mPayIntvStr.equals("0")) {	//趸缴
			mPaymentDuration.setText("--");
		} else {
//			if (pRisk.getChildText("PayEndYearFlag").equals("A")) {	//缴至xxAge
				mPaymentDuration.setText(mPayYearsStr);
//			} else if (pRisk.getChildText("PayEndYearFlag").equals("Y")) {	//缴xxYear
//				mPaymentDuration.setText(mPayYearsStr);
//			} else {
//				mPaymentDuration.setText("--");
//			}
		}

		Element mDurationMode = new Element(DurationMode);
		String mInsuYearFlagStr = pRisk.getChildText(InsuYearFlag);
		mDurationMode.setText(
				getIcbcInsuYearFlag(mInsuYearFlagStr));

		Element mDuration = new Element(Duration);
		String mInsuYearStr = pRisk.getChildText(InsuYear);
		if (mInsuYearFlagStr.equals("A")) {	//保至xxAge
			if (mInsuYearStr.equals("1000")) {
				mDuration.setText("终身");
			} else {
				mDuration.setText("至" + mInsuYearStr + "周岁");
			}
		} else if (mInsuYearFlagStr.equals("Y")) {	//保xxYear
			mDuration.setText(mInsuYearStr + "年");
		}

		Element mOLifEExtension = new Element(OLifEExtension);
		mOLifEExtension.setAttribute("VendorCode", "10");
		mOLifEExtension.addContent(mCashValues);
		Element xBonusValues = (Element) pRisk.getChild("BonusValues").clone();
		mOLifEExtension.addContent(xBonusValues);//红利保额保单年度末现金价值表
		
		mOLifEExtension.addContent(mPaymentDurationMode);
		mOLifEExtension.addContent(mPaymentDuration);
		mOLifEExtension.addContent(mDurationMode);
		mOLifEExtension.addContent(mDuration);

		Element mCoverage = new Element(Coverage);
		mCoverage.setAttribute("id", "Cov_"+pIndex);
		mCoverage.addContent(mPlanName);
		mCoverage.addContent(mProductCode);
		mCoverage.addContent(mLifeCovTypeCode);
		mCoverage.addContent(mIndicatorCode);
		mCoverage.addContent(mPaymentMode);
		mCoverage.addContent(mInitCovAmt);
		mCoverage.addContent(mIntialNumberOfUnits);
		mCoverage.addContent(mModalPremAmt);
//		mCoverage.addContent(mFirstSuperaddAmt);
		mCoverage.addContent(mEffDate);
		mCoverage.addContent(mFinalPaymentDate);
		mCoverage.addContent(mOLifEExtension);

		cLogger.debug("Out NewCont.getCoverage()!");
		return mCoverage;
	}

	private Element getAppntParty(Element pLCAppnt) throws Exception {
		cLogger.debug("Into NewCont.getAppntParty()...");

		Element mFullName = new Element(FullName);
		mFullName.setText(pLCAppnt.getChildText(AppntName).replaceAll("＇", "'"));

		Element mGovtIDTC = new Element(GovtIDTC);
		String xID = getIcbcIDType(pLCAppnt.getChildText(AppntIDType));
		mGovtIDTC.setText(xID);
		mGovtIDTC.setAttribute("tc", xID);

		Element mGovtID = new Element(GovtID);
		mGovtID.setText(pLCAppnt.getChildText(AppntIDNo));

		Element mGender = new Element(Gender);
		String gd = getIcbcSex(pLCAppnt.getChildText(AppntSex));
		mGender.setText(gd);
		mGender.setAttribute("tc", gd);

		Element mBirthDate = new Element(BirthDate);
		mBirthDate.setText(pLCAppnt.getChildText(AppntBirthday));

		Element mOccupationType = new Element("OccupationType");
		Element mEstSalary = new Element("EstSalary");
		Element mNationality = new Element("Nationality");

		Element mPerson = new Element(Person);
		mPerson.addContent(mGender);
		mPerson.addContent(mBirthDate);
		mPerson.addContent(mOccupationType);
		mPerson.addContent(mEstSalary);
		mPerson.addContent(mNationality);

		Element mParty = new Element(Party);
		mParty.setAttribute("id", "Party_1");   // add by 2012-5-13
		
		Element mAddress_1 = new Element("Address");
		mAddress_1.setAttribute("id", "Address_1");
		Element mAddressTypeCode = new Element("AddressTypeCode");
		mAddressTypeCode.setAttribute("tc", "17");
		mAddressTypeCode.setText("Mailing");
		Element mLine1 = new Element("Line1");
		mLine1.setText(pLCAppnt.getChildText("Address"));
		Element mZip = new Element("Zip");
		mZip.setText(pLCAppnt.getChildText("ZipCode"));
		mAddress_1.addContent(mAddressTypeCode);
		mAddress_1.addContent(mLine1);
		mAddress_1.addContent(mZip);
		
		Element mAddress_2 = new Element("Address");
		mAddress_2.setAttribute("id", "Address_2");
		Element mAddressTypeCode_2 = new Element("AddressTypeCode");
		mAddressTypeCode_2.setAttribute("tc", "1");
		mAddressTypeCode_2.setText("Residence");
		Element mLine1_2 = new Element("Line1");
		mLine1_2.setText(pLCAppnt.getChildText("Address"));
		Element mZip_2 = new Element("Zip");
		mZip_2.setText(pLCAppnt.getChildText("ZipCode"));
		mAddress_2.addContent(mAddressTypeCode_2);
		mAddress_2.addContent(mLine1_2);
		mAddress_2.addContent(mZip_2);
		
		Element mPhone_1 = new Element("Phone");
		mPhone_1.setAttribute("id", "Phone_1");
		mPhone_1.addContent(new Element("PhoneTypeCode").setAttribute("tc", "2").setText("Business"));
		mPhone_1.addContent(new Element("DialNumber").setText(pLCAppnt.getChildTextTrim("Phone")));
		
		Element mPhone_2 = new Element("Phone");
		mPhone_2.setAttribute("id", "Phone_2");
		mPhone_2.addContent(new Element("PhoneTypeCode").setAttribute("tc", "1").setText("Home"));
		mPhone_2.addContent(new Element("DialNumber").setText(""));
		
		Element mPhone_3 = new Element("Phone");
		mPhone_3.setAttribute("id", "Phone_3");
		mPhone_3.addContent(new Element("PhoneTypeCode").setAttribute("tc", "12").setText("Mobile"));
		mPhone_3.addContent(new Element("DialNumber").setText(pLCAppnt.getChildTextTrim("Mobile")));
		
		Element mEMailAddress = new Element("EMailAddress").setAttribute("id", "EMailAddress_1");
		mEMailAddress.addContent(new Element("AddrLine").setText(pLCAppnt.getChildTextTrim("Email")));
		
		Element mOLifEExtension = new Element("OLifEExtension").setAttribute("VendorCode", "200");
		Element mTellInfos = new Element("TellInfos");
		mOLifEExtension.addContent(mTellInfos);
		
		Element mPartyKey = new Element("PartyKey"); 
		mParty.addContent(mPartyKey);
		mParty.addContent(mFullName);
		mParty.addContent(mGovtID);
		mParty.addContent(mGovtIDTC);
		mParty.addContent(mPerson);
		mParty.addContent(mAddress_1);
		mParty.addContent(mAddress_2);
		mParty.addContent(mPhone_1);
		mParty.addContent(mPhone_2);
		mParty.addContent(mPhone_3);
		mParty.addContent(mEMailAddress);
		mParty.addContent(mOLifEExtension);
		
		cLogger.debug("Out NewCont.getAppntParty()!");
		return mParty;
	}

	private Element getInsuredParty(Element pLCInsured) throws Exception {
		cLogger.debug("Into NewCont.getInsuredParty()...");

		Element mFullName = new Element(FullName);
		mFullName.setText(pLCInsured.getChildText(Name).replaceAll("＇", "'"));

		Element mGovtIDTC = new Element(GovtIDTC);
		String xIdType = getIcbcIDType(pLCInsured.getChildText(IDType));
		mGovtIDTC.setAttribute("tc", xIdType);
		mGovtIDTC.setText(xIdType);

		Element mGovtID = new Element(GovtID);
		mGovtID.setText(pLCInsured.getChildText(IDNo));

		Element mGender = new Element(Gender);
		String xSex = getIcbcSex(pLCInsured.getChildText(Sex));
		mGender.setAttribute("id", xSex);
		mGender.setText(xSex);

		Element mBirthDate = new Element(BirthDate);
		mBirthDate.setText(pLCInsured.getChildText(Birthday));

		Element mOccupationType = new Element("OccupationType");
		
		Element mEstSalary = new Element("EstSalary");
		Element mNationality = new Element("Nationality");

		Element mPerson = new Element(Person);
		mPerson.addContent(mGender);
		mPerson.addContent(mBirthDate);
		mPerson.addContent(mOccupationType);
		mPerson.addContent(mEstSalary);
		mPerson.addContent(mNationality);
		
		Element mAddress_1 = new Element("Address");
		mAddress_1.setAttribute("id", "Address_3");
		Element mAddressTypeCode = new Element("AddressTypeCode");
		mAddressTypeCode.setAttribute("tc", "17");
		mAddressTypeCode.setText("Mailing");
		Element mLine1 = new Element("Line1");
		mLine1.setText(pLCInsured.getChildText("Address"));
		Element mZip = new Element("Zip");
		mZip.setText(pLCInsured.getChildText("ZipCode"));
		mAddress_1.addContent(mAddressTypeCode);
		mAddress_1.addContent(mLine1);
		mAddress_1.addContent(mZip);
		
		Element mAddress_2 = new Element("Address");
		mAddress_2.setAttribute("id", "Address_7");
		Element mAddressTypeCode_2 = new Element("AddressTypeCode");
		mAddressTypeCode_2.setAttribute("tc", "1");
		mAddressTypeCode_2.setText("Residence");
		Element mLine1_2 = new Element("Line1");
		mLine1_2.setText(pLCInsured.getChildText("Address"));
		Element mZip_2 = new Element("Zip");
		mZip_2.setText(pLCInsured.getChildText("ZipCode"));
		mAddress_2.addContent(mAddressTypeCode_2);
		mAddress_2.addContent(mLine1_2);
		mAddress_2.addContent(mZip_2);
		
		Element mPhone_1 = new Element("Phone");
		mPhone_1.setAttribute("id", "Phone_4");
		mPhone_1.addContent(new Element("PhoneTypeCode").setAttribute("tc", "1").setText("Home"));
		mPhone_1.addContent(new Element("DialNumber").setText(pLCInsured.getChildTextTrim("Phone")));
		
		Element mPhone_2 = new Element("Phone");
		mPhone_2.setAttribute("id", "Phone_5");
		mPhone_2.addContent(new Element("PhoneTypeCode").setAttribute("tc", "12").setText("Mobile"));
		mPhone_2.addContent(new Element("DialNumber").setText(pLCInsured.getChildTextTrim("Mobile")));
		
		Element mEMailAddress = new Element("EMailAddress").setAttribute("id", "EMailAddress_2");
		mEMailAddress.addContent(new Element("AddrLine").setText(pLCInsured.getChildTextTrim("Email")));
		
		Element mOLifEExtension = new Element("OLifEExtension").setAttribute("VendorCode", "200");
		Element mTellInfos = new Element("TellInfos");
		mOLifEExtension.addContent(mTellInfos);
		

		Element mParty = new Element(Party);
		mParty.setAttribute("id", "Party_2");
		mParty.addContent(new Element("PartyKey").setText("2"));
		mParty.addContent(mFullName);
		mParty.addContent(mGovtIDTC);
		mParty.addContent(mGovtID);
		mParty.addContent(mPerson);
		mParty.addContent(mAddress_1);
		mParty.addContent(mAddress_2);
		mParty.addContent(mPhone_1);
		mParty.addContent(mPhone_2);
		mParty.addContent(mEMailAddress);
		mParty.addContent(mOLifEExtension);

		cLogger.debug("Out NewCont.getInsuredParty()!");
		return mParty;
	}

	private Element getBnfParty(Element pLCBnf, int pIndex) throws Exception {
		cLogger.debug("Into NewCont.getBnfParty()...");

		Element mFullName = new Element(FullName);
		mFullName.setText(pLCBnf.getChildText(Name));

		Element mGovtIDTC = new Element(GovtIDTC);
		String xIdx = getIcbcIDType(pLCBnf.getChildText(IDType));
		mGovtIDTC.setAttribute("tc",xIdx);
		mGovtIDTC.setText(xIdx);

		Element mGovtID = new Element(GovtID);
		mGovtID.setText(pLCBnf.getChildText(IDNo));

		Element mGender = new Element(Gender);
		String xSex = getIcbcSex(pLCBnf.getChildText(Sex));
		mGender.setAttribute("tc", xSex);
		mGender.setText(xSex);

		Element mBirthDate = new Element(BirthDate);
		mBirthDate.setText(pLCBnf.getChildText(Birthday));

		Element mPerson = new Element(Person);
		mPerson.addContent(mGender);
		mPerson.addContent(mBirthDate);
		mPerson.addContent(new Element("Nationality"));
		
		Element mAddress = new Element("Address");
		mAddress.setAttribute("id", "Address_4");
		mAddress.addContent(new Element("AddressTypeCode").setAttribute("tc", "17").setText("Mailing"));
		mAddress.addContent(new Element("Line1").setText(pLCBnf.getChildText(Address)));
		
		Element mParty = new Element(Party);
		mParty.setAttribute("id", "Party_3");
		mParty.addContent(new Element("PartyKey").setText("3"));
		mParty.addContent(mFullName);
		mParty.addContent(mGovtIDTC);
		mParty.addContent(mGovtID);
		mParty.addContent(mPerson);

		cLogger.debug("Out NewCont.getBnfParty()!");
		return mParty;
	}

	private Element getCARParty(Element pLCCont) throws Exception {
		cLogger.debug("Into NewCont.getCARParty()...");

		Element mFullName = new Element(FullName);
		mFullName.setText(pLCCont.getChildText("LetterService"));

		Element mGovtID = new Element(GovtID);

		Element mOrganization = new Element("Organization");

		//公司地址信息
		Element mLine1 = new Element(Line1);
		mLine1.setText(pLCCont.getChildText(ComLocation));

		Element mLine2 = new Element("Line2");

		Element mCity = new Element("City");

		Element mAddressStateTC = new Element("AddressStateTC");

		Element mZip = new Element(Zip);
		mZip.setText(pLCCont.getChildText("ZipCode"));

		Element mAddress = new Element(Address);
		mAddress.addContent(mLine1);
		mAddress.addContent(mLine2);
		mAddress.addContent(mCity);
		mAddress.addContent(mAddressStateTC);
		mAddress.addContent(mZip);

		//公司电话信息
		Element mPhoneTypeCode = new Element(PhoneTypeCode);
		mPhoneTypeCode.setAttribute("tc", "2");
		mPhoneTypeCode.setText("2");

		Element mAreaCode = new Element("AreaCode");

		Element mDialNumber = new Element(DialNumber);
		mDialNumber.setText(pLCCont.getChildText(ComPhone));

		Element mPhone = new Element(Phone);
		mPhone.addContent(mPhoneTypeCode);
		mPhone.addContent(mAreaCode);
		mPhone.addContent(mDialNumber);
		
		Element mCarrier = new Element("Carrier");
		mCarrier.addContent(new Element("CarrierCode").setText("020"));

		Element mParty = new Element(Party);
		mParty.setAttribute("id", "CAR_PTY_1");
		mParty.addContent(mFullName);
		mParty.addContent(mGovtID);
		mParty.addContent(mOrganization);
		mParty.addContent(mAddress);
		mParty.addContent(mPhone);
		mParty.addContent(mCarrier);

		cLogger.debug("Out NewCont.getCARParty()!");
		return mParty;
	}

	private Element getAppntRelation(Element pLCAppnt) throws Exception {
		cLogger.debug("Into NewCont.getAppntRelation()...");

		Element mOriginatingObjectType = new Element(OriginatingObjectType);
		mOriginatingObjectType.addContent("4");	//4 - Holding

		Element mRelatedObjectType = new Element(RelatedObjectType);
		mRelatedObjectType.addContent("6");	//6 - Party

		Element mRelationRoleCode = new Element(RelationRoleCode);
		mRelationRoleCode.setAttribute("tc", "80");	//80 - 投保人关系
		mRelationRoleCode.addContent("80");

		Element mCustomerNo = new Element("CustomerNo");
		mCustomerNo.setText(pLCAppnt.getChildText(AppntNo));

		Element mOLifEExtension = new Element(OLifEExtension);
		mOLifEExtension.setAttribute("VendorCode", "100");
		mOLifEExtension.addContent(mCustomerNo);

		Element mRelation = new Element(Relation);
		mRelation.setAttribute("OriginatingObjectID", "Holding_1");
		mRelation.setAttribute("RelatedObjectID", "Party_1");
		mRelation.setAttribute("id", "Relation_2");
		mRelation.addContent(mOriginatingObjectType);
		mRelation.addContent(mRelatedObjectType);
		mRelation.addContent(mRelationRoleCode);
		mRelation.addContent(mOLifEExtension);

		cLogger.debug("Out NewCont.getAppntRelation()!");
		return mRelation;
	}

	private List getInsuredRelation(Element pLCInsured) throws Exception {
		cLogger.debug("Into NewCont.getInsuredRelation()...");

		//与Holding_1关系
		Element mOriginatingObjectType = new Element(OriginatingObjectType);
		mOriginatingObjectType.addContent("4");	//4 - Holding

		Element mRelatedObjectType = new Element(RelatedObjectType);
		mRelatedObjectType.addContent("6");	//6 - Party

		Element mRelationRoleCode = new Element(RelationRoleCode);
		mRelationRoleCode.setAttribute("tc", "81");	//81 - 被保人关系
		mRelationRoleCode.addContent("81");

		Element mCustomerNo = new Element("CustomerNo");
		mCustomerNo.setText(pLCInsured.getChildText(InsuredNo));

		Element mOLifEExtension = new Element(OLifEExtension);
		mOLifEExtension.setAttribute("VendorCode", "100");
		mOLifEExtension.addContent(mCustomerNo);

		Element mRelationToHolding = new Element(Relation);
		mRelationToHolding.setAttribute("OriginatingObjectID", "Holding_1");
		mRelationToHolding.setAttribute("RelatedObjectID", "Party_2");
		mRelationToHolding.setAttribute("id", "Relation_3");
		mRelationToHolding.addContent(mOriginatingObjectType);
		mRelationToHolding.addContent(mRelatedObjectType);
		mRelationToHolding.addContent(mRelationRoleCode);
		mRelationToHolding.addContent(mOLifEExtension);

		//与投保人关系
		mOriginatingObjectType = new Element(OriginatingObjectType);
		mOriginatingObjectType.addContent("6");	//6 - Party

		mRelatedObjectType = new Element(RelatedObjectType);
		mRelatedObjectType.addContent("6");	//6 - Party

		mRelationRoleCode = new Element(RelationRoleCode);
		mRelationRoleCode.addContent(
				getIcbcRelation(pLCInsured.getChildText(RelaToAppnt), "1"));

		Element mRelationToParty = new Element(Relation);
		mRelationToParty.setAttribute("OriginatingObjectID", "Party_1");
		mRelationToParty.setAttribute("RelatedObjectID", "Party_2");
		mRelationToParty.setAttribute("id", "Relation_4");
		mRelationToParty.addContent(mOriginatingObjectType);
		mRelationToParty.addContent(mRelatedObjectType);
		mRelationToParty.addContent(mRelationRoleCode);

		ArrayList mArrayList = new ArrayList();
		mArrayList.add(mRelationToHolding);
		mArrayList.add(mRelationToParty);

		cLogger.debug("Out NewCont.getInsuredRelation()!");
		return mArrayList;
	}

	private List getBnfRelation(Element pLCBnf, int pIndex) throws Exception {
		cLogger.debug("Into NewCont.getBnfRelation()...");

		//与Holding_1关系
		Element mOriginatingObjectType = new Element(OriginatingObjectType);
		mOriginatingObjectType.addContent("4");	//4 - Holding

		Element mRelatedObjectType = new Element(RelatedObjectType);
		mRelatedObjectType.addContent("6");	//6 - Party

		Element mRelationRoleCode = new Element(RelationRoleCode);
		mRelationRoleCode.setAttribute("tc", "82");	//82 - 受益人关系
		mRelationRoleCode.addContent("82");

		Element mInterestPercent = new Element("InterestPercent");
		mInterestPercent.addContent(
				String.valueOf(Integer.parseInt(pLCBnf.getChildText(BnfLot)) * 100));

		Element mSequence = new Element("Sequence");
		mSequence.setText(pLCBnf.getChildText(BnfGrade));

		Element mRelationToHolding = new Element(Relation);
		mRelationToHolding.setAttribute("OriginatingObjectID", "Holding_1");
		mRelationToHolding.setAttribute("RelatedObjectID", "Party_3");
		mRelationToHolding.setAttribute("id", "Relation_5");
		mRelationToHolding.addContent(mOriginatingObjectType);
		mRelationToHolding.addContent(mRelatedObjectType);
		mRelationToHolding.addContent(mRelationRoleCode);
		mRelationToHolding.addContent(mInterestPercent);
		mRelationToHolding.addContent(mSequence);

		//与投保人关系
		mOriginatingObjectType = new Element(OriginatingObjectType);
		mOriginatingObjectType.addContent("6");	//6 - Party

		mRelatedObjectType = new Element(RelatedObjectType);
		mRelatedObjectType.addContent("6");	//6 - Party

		mRelationRoleCode = new Element(RelationRoleCode);
		mRelationRoleCode.addContent(
				getIcbcRelation(pLCBnf.getChildText(RelationToInsured), "2"));

		Element mRelationToParty = new Element(Relation);
		mRelationToParty.setAttribute("OriginatingObjectID", "PartyInsured");
		mRelationToParty.setAttribute("RelatedObjectID", "Party_3");
		mRelationToParty.setAttribute("id", "Relation_9");
		mRelationToParty.addContent(mOriginatingObjectType);
		mRelationToParty.addContent(mRelatedObjectType);
		mRelationToParty.addContent(mRelationRoleCode);

		ArrayList mArrayList = new ArrayList();
		mArrayList.add(mRelationToHolding);
		mArrayList.add(mRelationToParty);

		cLogger.debug("Out NewCont.getBnfRelation()!");
		return mArrayList;
	}

	private Element getCarRelation() throws Exception {
		cLogger.debug("Into NewCont.getCarRelation()...");

		Element mOriginatingObjectType = new Element(OriginatingObjectType);
		mOriginatingObjectType.addContent("4");	//4 - Holding

		Element mRelatedObjectType = new Element(RelatedObjectType);
		mRelatedObjectType.addContent("6");	//6 - Party

		Element mRelationRoleCode = new Element(RelationRoleCode);
		mRelationRoleCode.setAttribute("tc", "85");	//85 - 承保公司关系
		mRelationRoleCode.addContent("85");

		Element mRelation = new Element(Relation);
		mRelation.setAttribute("OriginatingObjectID", "Holding_1");
		mRelation.setAttribute("RelatedObjectID", "CAR_PTY_1");
		mRelation.setAttribute("id", "RLN_HLDP_1C");
		mRelation.addContent(mOriginatingObjectType);
		mRelation.addContent(mRelatedObjectType);
		mRelation.addContent(mRelationRoleCode);

		cLogger.debug("Out NewCont.getCarRelation()!");
		return mRelation;
	}

	private String getIcbcPayIntv(String pPayIntvCode) {
		int mPayIntvInt = 0;
		try {
			mPayIntvInt = Integer.parseInt(pPayIntvCode);
		} catch (NumberFormatException ex) {
			cLogger.error("缴费方式有误！", ex);
			return pPayIntvCode;
		}

		switch (mPayIntvInt) {
		case -1:	//不定期缴
			return "6";
		case 0:	//	趸缴
			return "5";
		case 1:	//月缴
			return "2";
		case 3:	//季缴
			return "4";
		case 6:	//半年缴
			return "3";
		case 12:	//年缴
			return "1";
		default:
			return pPayIntvCode;
		}
	}

	private String getIcbcRiskCode(String riskCode) {
		if (riskCode.equals("331201")) {	//健康人生个人护理保险(万能型，B款)
			return "204";
		} else if (riskCode.equals("331301")) {	//健康人生个人护理保险(万能型，C款)
			return "205";
		} else if (riskCode.equals("230701")) {	//防癌险
			return "206";
		} else if (riskCode.equals("331601")) {	//健康人生个人护理保险(万能型，D款)
			return "001";	
		} else if (riskCode.equals("240501")){//安心宝
			return "003";
		}else if(riskCode.equals("331701")){//金利宝
			return "002";
		} else if (riskCode.equals("331901")) { //康利相伴个人护理保险（万能型）
			return "005";
		}else if (riskCode.equals("730101")) { 
			return "004";
		}else if (riskCode.equals("332401")) {
			return "104";
		}else if (riskCode.equals("332501")) { 
			return "332501";
		}else if (riskCode.equals("531701")) {
			return "531701";
		}else {
			return riskCode;
		}
	}

	private String getIcbcPayEndYearFlag(String pPayEndYearFlag) {
		if (pPayEndYearFlag.equals("A")) {	//缴至xx岁
			return "1";
		} else if (pPayEndYearFlag.equals("Y")) {	//缴xx年
			return "2";
		} else if (pPayEndYearFlag.equals("M")) {	//缴xx月
			return "3";
		} else if (pPayEndYearFlag.equals("D")) {	//缴xx日
			return "4";
		} else {	//其他
			return "9";
		}
	}

	private String getIcbcInsuYearFlag(String pInsuYearFlag) {
		if (pInsuYearFlag.equals("A")) {	//保至xx岁
			return "1";
		} else if (pInsuYearFlag.equals("Y")) {	//保xx年
			return "2";
		} else if (pInsuYearFlag.equals("M")) {	//保xx月
			return "3";
		} else if (pInsuYearFlag.equals("D")) {	//保xx日
			return "4";
		} else {	//其他
			return "9";
		}
	}

	private String getIcbcIDType(String pIdType) {
		int mIdTypeInt = 0;
		try {
			mIdTypeInt = Integer.parseInt(pIdType);
		} catch (NumberFormatException ex) {
			cLogger.error("证件类型有误！", ex);
			return pIdType;
		}

		switch (mIdTypeInt) {
		case 0:	//身份证
			return "0";
		case 1:	//护照
			return "1";
		case 2:	//军官证
			return "2";
		default:	//其它
			return "7";
		}
	}

	private String getIcbcSex(String pSex) {
		int mSexInt = 0;
		try {
			mSexInt = Integer.parseInt(pSex);
		} catch (NumberFormatException ex) {
			cLogger.error("证件类型有误！", ex);
			return pSex;
		}

		switch (mSexInt) {
		case 0:	//男
			return "1";
		case 1:	//女
			return "2";
		case 2:	//不详
			return "3";
		default:
			return pSex;
		}
	}

	/**
	 * tInsuredOrBnf:
	 *   1 - 投被保人关系，默认“其他”；
	 *   2 - 受益人关系，默认“法定”
	 */
	private String getIcbcRelation(String tRelation, String tInsuredOrBnf) {
		String tempRelation = "";
		if (tRelation == null || tRelation.equals("")) {
			if (tInsuredOrBnf.equals("1")) {
				tempRelation = "99";
			} else if (tInsuredOrBnf.equals("2")) {
				tempRelation = "28";
			} else
				tempRelation = "99";
		} else {
			if (tRelation.equals("00"))
				tempRelation = "8";
			else if (tRelation.equals("01"))
				tempRelation = "1";
			else if (tRelation.equals("02"))
				tempRelation = "1";
			else if (tRelation.equals("03"))
				tempRelation = "3";
			else if (tRelation.equals("04"))
				tempRelation = "2";
			else if (tRelation.equals("05"))
				tempRelation = "2";
			else if (tRelation.equals("12"))
				tempRelation = "6";
			else if (tRelation.equals("19"))
				tempRelation = "4";
			else if (tRelation.equals("20"))
				tempRelation = "4";
			else if (tRelation.equals("21"))
				tempRelation = "5";
			else if (tRelation.equals("25"))
				tempRelation = "7";
			else if (tRelation.equals("27"))
				tempRelation = "9";
			else
				tempRelation = "99";
		}
		return tempRelation;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "C:/Documents and Settings/Administrator/桌面/icbc_1477341_1013_154001.xml";
		String mOutFilePath = "D:/outnostd.xml";
		File f = new File(mOutFilePath);
		if(!f.exists()){
			f.createNewFile();
		}
//		String mInFilePath = "E:/icbc230701/icbc_330701.xml";
//		String mOutFilePath = "E:/icbc230701/icbc.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();

		Document mOutXmlDoc = new NewCont().getNoStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);

		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();

		System.out.println("成功结束！");
	}
}