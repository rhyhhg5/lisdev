package com.sinosoft.midplat.channel.reformat.icbc;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class Query extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into Query.getStdXml()...");
		
		InputStream mSheetIs = getClass().getResourceAsStream("QueryIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		
		cLogger.info("Out Query.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Query.getNoStdXml()...");
		
		//重打和新单返回报文基本完全一样，所以直接调用
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		String mPath = "/TXLife/TXLifeResponse/OLifE/Holding/Policy/PaymentAmt";
		Element mPaymentAmt = (Element) XPath.selectSingleNode(mNoStdXml, mPath);
		String mPrem = pStdXml.getRootElement().getChild(LCCont).getChildTextTrim(Prem);
		mPaymentAmt.setText(mPrem);
		
		cLogger.info("Out Query.getNoStdXml()!");
		return mNoStdXml;
	}
}