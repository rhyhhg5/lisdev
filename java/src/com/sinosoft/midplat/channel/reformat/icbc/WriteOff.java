package com.sinosoft.midplat.channel.reformat.icbc;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.NumberUtil;

public class WriteOff extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into WriteOff.getStdXml()...");
		
		InputStream mSheetIs = getClass().getResourceAsStream("WriteOffIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		
		String mPath = "/TranData/BaseInfo/BankDate";
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/Prem";
		List mList = XPath.selectNodes(mStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tPrem = (Element) mList.get(i);
			tPrem.setText(NumberUtil.fenToYuan(tPrem.getTextTrim()));
		}
		
		cLogger.info("Out WriteOff.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into WriteOff.getNoStdXml()...");
		
		InputStream mSheetIs = getClass().getResourceAsStream("WriteOffOut.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mNoStdXml = new XSLTransformer(mSheetIsr).transform(pStdXml);
		
		cLogger.info("Out WriteOff.getNoStdXml()!");
		return mNoStdXml;
	}
}