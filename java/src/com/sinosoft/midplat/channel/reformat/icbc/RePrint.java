package com.sinosoft.midplat.channel.reformat.icbc;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class RePrint extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into RePrint.getStdXml()...");
		
		InputStream mSheetIs = getClass().getResourceAsStream("RePrintIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		
		cLogger.info("Out RePrint.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into RePrint.getNoStdXml()...");
		
		//重打和新单返回报文基本完全一样，所以直接调用
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		
		cLogger.info("Out RePrint.getNoStdXml()!");
		return mNoStdXml;
	}
}