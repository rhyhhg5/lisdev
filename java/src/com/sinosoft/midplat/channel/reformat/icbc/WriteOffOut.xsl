<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent='yes'/>
<xsl:template match="/">
	<TXLife>
		<TXLifeResponse>
			<TransRefGUID></TransRefGUID>
			<TransType>1015</TransType>
			<TransExeDate><xsl:value-of select="TranData/LCCont/BankDate"/></TransExeDate>
			<TransExeTime><xsl:value-of select="TranData/LCCont/BankTime"/></TransExeTime>
			<TransMode tc="8"/>
			<xsl:apply-templates select="TranData/RetData"/>
			<OLifE>
				<Holding id="Holding_1">
					<HoldingTypeCode tc="2"/>
					<Policy>
						<PolNumber><xsl:value-of select="TranData/LCCont/ContNo"/></PolNumber>
						<Life/>
					</Policy>
					<Arrangement>
						<Arrtype tc="36"/>
						<ArrDestination HoldingID="Acct_1"/>
					</Arrangement>
				</Holding>
				<Holding id="Acct_1">
					<Banking>
						<AccountNumber></AccountNumber>
					</Banking>
				</Holding>
				<FormInstance id="Form_1">
					<FormName/>
					<DocumentControlNumber/>
					<Attachment id="Attachment_Form_1">
						<Description/>
						<AttachmentData/>
						<AttachmentType/>
						<AttachmentLocation/>
					</Attachment>
				</FormInstance>
			</OLifE>
		</TXLifeResponse>
	</TXLife>
</xsl:template>

<xsl:template name="RetData" match="TranData/RetData">
	<TransResult>
  	<xsl:if test="Flag='1'">
			<ResultCode>0000</ResultCode>
			<ResultInfo>
				<ResultInfoDesc>���׳ɹ���</ResultInfoDesc>
			</ResultInfo>
		</xsl:if>
   	<xsl:if test="Flag='0'">
			<ResultCode>1001</ResultCode>
			<ResultInfo>
				<ResultInfoDesc>
					<xsl:value-of select="Desc"/>
				</ResultInfoDesc>
			</ResultInfo>
		</xsl:if>
	</TransResult>
</xsl:template>

</xsl:stylesheet>
