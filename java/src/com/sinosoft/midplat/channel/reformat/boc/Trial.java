/**
 * 中行试算交易报文转换
 */

package com.sinosoft.midplat.channel.reformat.boc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class Trial extends BaseFormat  {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into Trial.getStdXml()...");
		
		//去除中行报文的外壳
		Element mTranData = (Element) pNoStdXml.getRootElement().getChild(
				TranData).detach();
		Document mStdXml = new Document(mTranData);

		//中行试算不传保单合同印刷号(ProposalContNo)，自动生成(ProposalContNo="YBT"+PrtNo)
		Element mLCCont = mTranData.getChild(LCCont);
		String mPrtNoStr = mLCCont.getChildText(PrtNo);
		if ((null==mPrtNoStr) || mPrtNoStr.equals("")) {
			throw new BaseException("投保书印刷号不能为空!");
		}
		cLogger.info("mPrtNoStr===="+mPrtNoStr);
		String mProposalContNoStr = "YBT" + mPrtNoStr;
		mLCCont.getChild(ProposalContNo).setText(mProposalContNoStr);
		
		//如果主险代码是 230701 时,组织一个附加险 331401
		String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(mStdXml, mPath);
		Element mRisk = mRisks.getChild(Risk);
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		cLogger.info("mRisks===="+mRisk.getChildTextTrim(MainRiskCode));
		//如果主险代码是 240501 时,组织一个附加险 340201
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
		}
			
		/**
		 * 
		 * 中行规定,如果健康告知flag为1,则说明有健康问题,不能进行投保
		 */

		if (("").equals(mRisk.getChild("HealthFlag").getTextTrim()) || ("1").equals(mRisk.getChild("HealthFlag").getTextTrim())) {
			throw new BaseException("有健康告知,不能进行投保 或 健康告知为空");
		}

		/**
		 *  
		 */
		
		/**
		 * 中行方面传入的受益人数是固定的，如果没有受益人，他们也会传送姓名为空的受益人。
		 * 这里将姓名为空的受益人剔除。
		 */
		cLogger.info("HealthFlag===="+111111);
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf";
		List mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf = (Element) mList.get(i);
			String tNameStr = tLCBnf.getChildTextTrim(Name);
			if ((null==tNameStr) || tNameStr.equals("")) {
				tLCBnf.detach();
			}
		}
		cLogger.info("mList===="+mRisk.getChildTextTrim(MainRiskCode));
		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);
		
		cLogger.info("Out Trial.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Trial.getNoStdXml()...");

		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		Element mRoot = mNoStdXml.getRootElement();
		if (!mRoot.getName().equals(Fault)) {	//SOAP报错报文无需转换，正常报文需要更换根节点标签
			mRoot.setName(onLineUnderWriteResponse);
		}

		cLogger.info("Out Trial.getNoStdXml()!");
		return mNoStdXml;
	}
	
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFilePath = "C:\\Documents and Settings\\Administrator\\桌面\\boc_201200005_23_145241.xml";
		String mOutFilePath = "E:/Test-haoqt/picch/bcm/testXml/newCont/bcm_yy_out.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new Trial().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
	
}
