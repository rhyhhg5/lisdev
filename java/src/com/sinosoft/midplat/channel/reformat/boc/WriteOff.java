/**
 * 中行当日撤单交易报文转换
 */

package com.sinosoft.midplat.channel.reformat.boc;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;

import com.sinosoft.midplat.channel.BOCControl;
import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class WriteOff extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into WriteOff.getStdXml()...");
		
		//去除中行报文的外壳
		Element mTranData = (Element) pNoStdXml.getRootElement().getChild(
				TranData).detach();
		//去掉null
		Document doc = new Document(mTranData);
		Element mProposalContNo = doc.getRootElement().getChild(LCCont).getChild(ProposalContNo);
		String mProposalNo = mProposalContNo.getTextTrim();
		if(mProposalNo.startsWith("null")) {
			mProposalContNo.setText(mProposalNo.substring(4));
		}

		cLogger.info("Out WriteOff.getStdXml()!");
		return doc;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into WriteOff.getNoStdXml()...");
		
		Element mTranData = pStdXml.detachRootElement();		

		Element mRetData = mTranData.getChild(RetData);
		String mFlag = mRetData.getChildText(Flag); //1-成功 0-失败
		//交易失败，组织SOAP报错报文
		if (mFlag.equals("0")) {
			return BOCControl.getSoapError(mRetData.getChildText(Desc));
		}
		
		//添加中行报文的外壳
		Namespace mNamespace = Namespace.getNamespace("m", BOC_URI);
		Element mOnLineCancelResponse = new Element(onLineCancelResponse, mNamespace);
		mOnLineCancelResponse.addContent(mTranData);

		cLogger.info("Out WriteOff.getNoStdXml()!");
		return new Document(mOnLineCancelResponse);
	}
}