/**
 * 中行对账交易报文转换
 */

package com.sinosoft.midplat.channel.reformat.boc;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;

import com.sinosoft.midplat.channel.BOCControl;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.NumberUtil;

public class Balance extends BaseFormat {
	public Document getStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Balance.getStdXml()...");
		
		//去除中行报文的外壳
		Element mTranData = (Element) pStdXml.getRootElement().getChild(
				TranData).detach();
		Document mStdXml = new Document(mTranData);
		
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		
		mBaseInfo.getChild(FunctionFlag).setText("17");
		
		String mBrNo = mBaseInfo.getChildText(ZoneNo).substring(0, 4);
		mBaseInfo.getChild(ZoneNo).setText(mBrNo);
		mBaseInfo.getChild(BrNo).setText(mBrNo);
		
		/**
		 * 自动生成交易流水号(银行不传！)
		 * 注意：此处生成的只是流水号的前缀，在核心、明细、汇总对账中各自加上自己的后缀作为实际的流水号。
		 */
		mBaseInfo.getChild(TransrNo).setText(
				mBaseInfo.getChildText(BankCode) + mBrNo + mBaseInfo.getChildText("BankDate"));

		//将中行明细转换为标准明细
		Element mChkDetails = mTranData.getChild(ChkDetails);
		for (Element tBOCChkDetail; null != (tBOCChkDetail=mChkDetails.getChild("BOCChkDetail"));) {
			Element tTranAmnt = tBOCChkDetail.getChild(TranAmnt);
			tTranAmnt.setText(
					NumberUtil.fenToYuan(tTranAmnt.getText()));
			
			//保单状态转换(由中行格式转换为标准格式)
			String tState = tBOCChkDetail.getChildText("State");			
			Element tFuncFlag = tBOCChkDetail.getChild(FuncFlag);
			if (tState.equals("S")) {
				tFuncFlag.setText("01");
			} else if (tState.equals("W")) {
				tFuncFlag.setText("04");
			} else {
				throw new BaseException("保单状态有误！" + tBOCChkDetail.getChildText(CardNo) + "，" + tState);
			}			
			tBOCChkDetail.getChild("ConfirmFlag").setText("1");
			tBOCChkDetail.removeChildren("State");
			tBOCChkDetail.setName("ChkDetail");
		}

		cLogger.info("Out Balance.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into Balance.getNoStdXml()...");
		
		Element mTranData = pNoStdXml.detachRootElement();
		
		Element mRetData = mTranData.getChild(RetData);
		String mFlag = mRetData.getChildText(Flag); //1-成功 0-失败
		//交易失败，组织SOAP报错报文
		if (mFlag.equals("0")) {
			return BOCControl.getSoapError(mRetData.getChildText(Desc));
		}
		
		//添加中行报文的外壳
		Namespace mNamespace = Namespace.getNamespace("m", BOC_URI);
		Element mOnLineSendMatchResponse = new Element(onLineSendMatchResponse, mNamespace);
		mOnLineSendMatchResponse.addContent(mTranData);
		
		cLogger.info("Out Balance.getNoStdXml()!");
		return new Document(mOnLineSendMatchResponse);
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFile = "E:/Test-haoqt/picch/boc/testXml/balance/boc_17_in.xml";
		String mOutFile = "E:/Test-haoqt/picch/boc/testXml/balance/boc_17_out.xml";

		Document mInXmlDoc = JdomUtil.build(new FileInputStream(mInFile));
		
		Document mOutXmlDoc = new Balance().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		JdomUtil.output(mOutXmlDoc, new FileOutputStream(mOutFile));
		
		System.out.println("成功结束！");
	}
}
