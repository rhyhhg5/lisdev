/**
 * 中行新单交易报文转换
 */

package com.sinosoft.midplat.channel.reformat.boc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.BOCControl;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.NumberUtil;

public class NewCont extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into NewCont.getStdXml()...");

		//去除中行报文的外壳
		Element mTranData = (Element) pNoStdXml.getRootElement().getChild(
				TranData).detach();
		Document mStdXml = new Document(mTranData);

		//如果主险代码是 230701 时,组织一个附加险 331401
		String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(mStdXml, mPath);
		List mRisklist = mRisks.getChildren(Risk);
		for(int a=0;a<mRisklist.size();a++){
			//红利领取方式
				if("1".equals(((Element)mRisklist.get(a)).getChildText("BonusGetMode"))){
					((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("2");
				}
				else{
					((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("1");
				}
		}
		Element mRisk = mRisks.getChild(Risk);
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		//如果主险是 240501 时 ,组织一个附加险 340201
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
		}
		/**
		 * add by wangxt in 2009-3-20 begin
		 * 中行规定,如果健康告知flag为1,则说明有健康问题,不能进行投保
		 */
		String mHealthFlag = mRisk.getChild("HealthFlag").getTextTrim();
		if ((null != mHealthFlag) && mHealthFlag.equals("1")) {
			throw new BaseException("有健康告知,不能进行投保！");
		}
		/**
		 * add by wangxt in 2009-3-20 end
		 */

		/**
		 * 中行方面传入的受益人数是固定的，如果没有受益人，他们也会传送姓名为空的受益人。
		 * 这里将姓名为空的受益人剔除。
		 */
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf";
		List mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf = (Element) mList.get(i);
			String tNameStr = tLCBnf.getChildTextTrim(Name);
			if ((null==tNameStr) || tNameStr.equals("")) {
				tLCBnf.detach();
			}
		}

		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);
		//校验单证是否以YBT开头
		NewContCheck.proposalNoCheck(mStdXml.getRootElement().getChild("LCCont").getChildText("ProposalContNo"));
		
		cLogger.info("Out NewCont.getStdXml()!");
		return mStdXml;
	}

	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");

		Element mTranData = pStdXml.detachRootElement();

		Element mRetData = mTranData.getChild(RetData);
		String mFlag = mRetData.getChildText(Flag); //1-成功 0-失败
		//交易失败，组织SOAP报错报文
		if (mFlag.equals("0")) {
			return BOCControl.getSoapError(mRetData.getChildText(Desc));
		}

		//交易成功
		Element mLCCont = mTranData.getChild(LCCont);
		
		/**
		 * add by wangxt in 2009-06-19 begin
		 * 如果是万能C款则把追加保费加上原保费返回
		 */
		String mPath = "LCInsureds/LCInsured/Risks/Risk";
		List mList = XPath.selectNodes(mLCCont, mPath);
		
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk = (Element) mList.get(i);
			String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
			Element tLCContPrem = 
				tRisk.getParentElement().getParentElement().getParentElement().getParentElement().getChild(Prem);
			if (tMainRiskCodeStr.equals("331201")	//健康人生个人护理保险(万能型，B款)
					|| tMainRiskCodeStr.equals("331301")	//健康人生个人护理保险(万能型，C款)
					|| tMainRiskCodeStr.equals("331601")	//健康人生个人护理保险(万能型，C款)
					|| tMainRiskCodeStr.equals("230701")	//健康专家个人防癌疾病保险
					|| tMainRiskCodeStr.equals("240501")	//安心宝个人终身重大疾病保险    
 					|| tMainRiskCodeStr.equals("331701")    //金利宝个人护理保险（万能型）
 					|| tMainRiskCodeStr.equals("331901")) {	//康利相伴个人护理保险（万能型）
				tRisk.getChild(Prem).setText(tLCContPrem.getText());	//银行方1个险种对应系统内两险种，银行要求返回总保费
			}

			if (tMainRiskCodeStr.equals("331301")) {	//picch万能C有个追加保费，需要累加上去
				double ttTotalPrem = 
					Double.parseDouble(tLCContPrem.getText())	+ Double.parseDouble(tRisk.getChildText(SupplementaryPrem));
				tLCContPrem.setText(new DecimalFormat("0.00").format(ttTotalPrem));
			}
 			//趸缴产品的 保额 栏内容：原“--”更改为：“详见条款规定”
 			String mPayIntv =  tRisk.getChildText(PayIntv);
 			
 			if(mPayIntv.equals("0")&&!"730101".equals(tMainRiskCodeStr)){
 				Element mAmnt = tRisk.getChild(Amnt);
 				mAmnt.setText("详见条款规定");
 				
 			} 
 			if(mPayIntv.equals("0")&&"730101".equals(tMainRiskCodeStr)){
 				Element mPayYears = tRisk.getChild(PayYears);
 				mPayYears.setText("--");
 				
 			} 
			if(tMainRiskCodeStr.equals("240501")){//安心宝
				tRisk.getChild(RiskName).setText("安心宝个人终身重大疾病保障计划");
                tRisk.getChild(InsuYear).setText("终身");
				mLCCont.getChild(SpecContent).setText("安心宝个人终身重大疾病保障计划是由《安心宝个人终身重大疾病保险》、《附加安心宝个人护理保险》组成。");
			}
			
			if(tMainRiskCodeStr.equals("331701")){//金利宝
				tRisk.getChild(RiskName).setText("金利宝个人护理增值计划（万能型）");
				mLCCont.getChild(SpecContent).setText("金利宝个人护理增值计划（万能型）是由《金利宝个人护理保险（万能型）》、《附加金利宝个人意外伤害保险》组成。");
				tRisk.getChild(Amnt).setText("详见条款规定");
			}
		}	
		//带附加险时后置机返回的报文中险种顺序不定，所以此处处理把主险放到第一位
//		String mPath1 = "LCInsureds/LCInsured/Risks/Risk";
		List mList1 = mLCCont.getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChildren(Risk);
		for (int i = 0; i < mList1.size(); i++) {
			Element tRisk  = (Element) mList1.get(i);
			String tMainRiskCode = tRisk.getChildText(MainRiskCode);
			String tRiskCode = tRisk.getChildText(RiskCode);
			if(tMainRiskCode.equals(tRiskCode)){
				Element tRiskCopy = (Element)tRisk.clone();
				Element tRisk1  = (Element) mList1.get(0);
				Element tRiskCopy1 = (Element)tRisk1.clone();
				mList1.remove(0);
				mList1.add(0, tRiskCopy);
				mList1.remove(i);
				mList1.add(i, tRiskCopy1);				
			}
		}
		/**
		 * add by wangxt in 2009-06-19 end 
		 */

		/*Start-中行增加字段*/
		Element mPremText = new Element("PremText");	
		mPremText.setText(
				NumberUtil.getChnMoney(
						mLCCont.getChildText(Prem)));
		mLCCont.addContent(mPremText);

		Element mCashValuePrintDesc = new Element("CashValuePrintDesc");	
		mCashValuePrintDesc.setText("-------------------------(本栏以下空白)-------------------------");
		mLCCont.addContent(mCashValuePrintDesc);
		/*End-中行增加字段*/

		/*Start-将编码转换为汉字*/
		Element mPayIntv = mLCCont.getChild(PayIntv);
		mPayIntv.setText(
				getPayIntvName(mPayIntv.getText()));

		mPath = "LCAppnt/AppntSex";
		Element mAppntSex = (Element) XPath.selectSingleNode(mLCCont, mPath);
		mAppntSex.setText(
				getSexName(mAppntSex.getText()));

		mPath = "LCInsureds/LCInsured/Sex";
		mList = XPath.selectNodes(mLCCont, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tSex = (Element) mList.get(i);
			tSex.setText(
					getSexName(tSex.getText()));
		}

		mPath = "LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/Sex";
		mList = XPath.selectNodes(mLCCont, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tSex = (Element) mList.get(i);
			tSex.setText(
					getSexName(tSex.getText()));
		}
		/*End-将编码转换为汉字*/

		//picch核心规定无收益人即受益人为法定，这里根据中行需要生成一个“法定”受益人
		mPath = "LCInsureds/LCInsured/Risks/Risk/LCBnfs";
		Element mLCBnfs = (Element) XPath.selectSingleNode(mLCCont, mPath);
		if (0 == mLCBnfs.getChildren(LCBnf).size()) {
			Element tName = new Element(Name);
			tName.setText("法定");

			Element tLCBnf = new Element(LCBnf);
			tLCBnf.addContent(tName);

			mLCBnfs.addContent(tLCBnf);
		}

		//应客户要求,中行打印的现金价值链10年以后,每10年打印一次.
		mPath = "LCInsureds/LCInsured/Risks/Risk/CashValues";
		//目前农行只返回主险,以后若要返回附加险,需要修改.
		Element mCashValues = (Element) XPath.selectSingleNode(mLCCont, mPath);
		cLogger.debug("XPath=" + mPath);
		mList = mCashValues.getChildren();
		Iterator i = mList.iterator();
		cLogger.debug("mList = mCashValues.getChildren()"+mList.size());
		
		int mOldCount = mList.size() -1;//要求返回的现金价值链中最后一条现价必须返回
		int flag = 0;//第一条是现金价值链的统计数目.不对第一个子节点进行操作.
		while (i.hasNext()) {
		    if (flag != 0) {
		        Element mCashValue = (Element) i.next();
		        int j = 0;//判断end值
		        String m = mCashValue.getChildText("End");
		        j = Integer.parseInt(m);
		        if ((j>10) && (j%10 != 0) && (j<mOldCount)) {
		            i.remove();
		            cLogger.debug("i.remove(),i=" + i);
		        }
		    } else {
		        i.next();//如果是第一个自己点,不做处理,转到下一条子节点.
		    }
		    
		    flag++;
		}
		int count = mList.size() - 1;//操作后剩余的价值链子节点数量
		mCashValues.getChild("CashValueCount").setText(String.valueOf(count));
        
		//添加中行报文的外壳
		Namespace mNamespace = Namespace.getNamespace("m", BOC_URI);
		Element mOnLineAuditPolicyResponse = new Element(onLineAuditPolicyResponse, mNamespace);
		mOnLineAuditPolicyResponse.addContent(mTranData);

		cLogger.info("Out NewCont.getNoStdXml()!");
		return new Document(mOnLineAuditPolicyResponse);
	}

	private String getPayIntvName(String pPayIntvCode) {
		int mPayIntvInt = 0;
		try {
			mPayIntvInt = Integer.parseInt(pPayIntvCode);
		} catch (NumberFormatException ex) {
			cLogger.warn("缴费方式有误！" + pPayIntvCode);	//精简日志
//			cLogger.error("缴费方式有误！", ex);
			return pPayIntvCode;
		}

		switch (mPayIntvInt) {
		case -2 :
			return "约定缴费";
		case -1 :
			return "不定期缴";
		case 0 :
			return "趸缴";
		case 1 :
			return "月缴";
		case 3 :
			return "季缴";
		case 6 :
			return "半年缴";
		case 12 :
			return "年缴";
		default:
			return String.valueOf(pPayIntvCode);
		}
	}

	private String getSexName(String pSexCode) {
		int mSexCodeInt = 0;
		try {
			mSexCodeInt = Integer.parseInt(pSexCode);
		} catch (NumberFormatException ex) {
			cLogger.warn("性别有误！" + pSexCode);
//			cLogger.error("性别有误！", ex);
			return pSexCode;
		}

		switch (mSexCodeInt) {
		case 0 :
			return "男";
		case 1 :
			return "女";
		case 2 :
			return "不详";
		default:
			return pSexCode;
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "D:/instd.xml";
		String mOutFilePath = "D:/out.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();

		Document mOutXmlDoc = new NewCont().getNoStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);

		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();

		System.out.println("成功结束！");
	}
}