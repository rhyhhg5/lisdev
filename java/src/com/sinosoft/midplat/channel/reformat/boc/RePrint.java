/**
 * 中行保单重打交易报文转换
 */

package com.sinosoft.midplat.channel.reformat.boc;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class RePrint extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into RePrint.getStdXml()...");
		
		//去除中行报文的外壳
		Element mTranData = (Element) pNoStdXml.getRootElement().getChild(
				TranData).detach();
		
		Element mBaseInfo = mTranData.getChild(BaseInfo);
		mBaseInfo.getChild(FunctionFlag).setText("02");	//中行重打和新单都传01，这里需要重置一下。

		cLogger.info("Out RePrint.getStdXml()!");
		return new Document(mTranData);
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into RePrint.getNoStdXml()...");

		//重打和新单返回报文基本完全一样，所以直接调用
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		
		cLogger.info("Out RePrint.getNoStdXml()!");
		return mNoStdXml;
	}
}