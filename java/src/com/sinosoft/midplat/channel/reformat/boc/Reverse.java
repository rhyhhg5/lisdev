/**
 * 中行冲正。
 * 此交易是重打交易的前置交易，本身不做任何业务处理，只是用来标识将要进行保单重打。
 */

package com.sinosoft.midplat.channel.reformat.boc;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;

import com.sinosoft.midplat.channel.BOCControl;
import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class Reverse extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into Reverse.getStdXml()...");
		
		//去除中行报文的外壳
		Element mTranData = (Element) pNoStdXml.getRootElement().getChild(
				TranData).detach();

		cLogger.info("Out Reverse.getStdXml()!");
		return new Document(mTranData);
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Reverse.getNoStdXml()...");
		
		Element mTranData = pStdXml.detachRootElement();
		
		Element mRetData = mTranData.getChild(RetData);
		String mFlag = mRetData.getChildText(Flag); //1-成功 0-失败
		//交易失败，组织SOAP报错报文
		if (mFlag.equals("0")) {
			return BOCControl.getSoapError(mRetData.getChildText(Desc));
		}
		
		//添加中行报文的外壳
		Namespace mNamespace = Namespace.getNamespace("m", BOC_URI);
		Element mOnLineReverseResponse = new Element(onLineReverseResponse, mNamespace);
		mOnLineReverseResponse.addContent(mTranData);
		
		cLogger.info("Out Reverse.getNoStdXml()!");
		return new Document(mOnLineReverseResponse);
	}
}
