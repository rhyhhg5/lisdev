<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<!-- 临时给建行这帮人渣做的-->
	<xsl:template match="/">
		<Transaction>
			<Transaction_Header>
				<LiBankID>CCB</LiBankID>
				<!--建行银行代码,固定为CCB三个大写字母-->
				<PbInsuId>010020</PbInsuId>
				<!--保险公司代码-->
				<BkPlatSeqNo>
					<xsl:value-of select="/TranData/BaseInfo/TransrNo"/>
				</BkPlatSeqNo>
				<!--请求方交易流水号-->
				<BkTxCode>OPR999</BkTxCode>
				<!--请求方交易码-->
				<BkChnlNo>1</BkChnlNo>
				<!--交易渠道代号-->
				<BkBrchNo>
					<xsl:value-of select="/TranData/BaseInfo/BrNo"/>
				</BkBrchNo>
				<!--原始请求方机构代码-->
				<BkTellerNo>
					<xsl:value-of select="/TranData/BaseInfo/TellerNo"/>
				</BkTellerNo>
				<!--请求方交易柜员号-->
				<BkPlatDate>
					<xsl:value-of select="/TranData/BaseInfo/BankDate"/>
				</BkPlatDate>
				<!--请求方交易日期（周期）-->
				<BkPlatTime>
					<xsl:value-of select="/TranData/BaseInfo/BankTime"/>
				</BkPlatTime>
				<!--请求方交易系统时间-->
				<Tran_Response>
					<BkOthDate><xsl:value-of select="TranData/BaseInfo/BankDate"/></BkOthDate>
					<BkPlatSeqNo><xsl:value-of select="TranData/BaseInfo/TransrNo"/></BkPlatSeqNo>							
					<BkOthRetCode>00000</BkOthRetCode>
					<BkOthRetMsg>交易成功</BkOthRetMsg>			
				</Tran_Response>
			</Transaction_Header>
			<!--报文体-->
			<!-- 
			<Transaction_Body>

			</Transaction_Body>
			-->
		</Transaction>
	</xsl:template>
</xsl:stylesheet>
