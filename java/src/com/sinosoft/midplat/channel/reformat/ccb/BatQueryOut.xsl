<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<Transaction>
			<!--������-->
			<Transaction_Body>
				<BkNum1><xsl:value-of select="/TranData/LCCont/BatIncome"/></BkNum1>
				<BkNum2><xsl:value-of select="/TranData/LCCont/BatPay"/></BkNum2>
			</Transaction_Body>
		</Transaction>
	</xsl:template>
</xsl:stylesheet>
