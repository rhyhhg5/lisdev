<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes" />
	<xsl:template match="/">
		<!--送盘-->
		<TranData>
			<xsl:variable name="FileName"
				select="/Transaction/Transaction_Body/BkFileName" />
			<BaseInfo>
				<xsl:for-each
					select="/Transaction/Transaction_Header">
					<BankDate>
						<xsl:value-of select="BkPlatDate" />
					</BankDate>
					<!--银行交易日期-->
					<BankCode>03</BankCode>
					<!--银行代码(cd05)-->
					<ZoneNo>
						<xsl:value-of
							select="substring(BkBrchNo, 1, 3)" />
					</ZoneNo>
					<!--地区代码 -->
					<BrNo>
						<xsl:value-of
							select="substring(BkBrchNo, 4, 9)" />
					</BrNo>
					<!--网点代码-->
					<TellerNo><!--建行银行柜员代码为12位，我方数据库(LKTRANSSTATUS)中对应字段(BANKOPERATOR)为10位，自动截取后10位。-->
						<xsl:value-of select="substring(BkTellerNo, 3)" />
					</TellerNo>
					<!--柜员代码-->
					<TransrNo>
						<xsl:value-of select="BkPlatSeqNo" />
					</TransrNo>
					<!--交易流水号-->
					<SaleChannel>
						<xsl:value-of select="BkChnlNo" />
					</SaleChannel>
					<!--交易渠道代号-->
					<FunctionFlag>32</FunctionFlag>
					<!--处理标志(cd02) <xsl:value-of select="BkTxCode"/>-->
					<InsuID>
						<xsl:value-of select="PbInsuId" />
					</InsuID>
					<!--保险公司代码(cd03) -->
				</xsl:for-each>
				<FileName>
					<xsl:value-of select="$FileName" />
				</FileName>
				<!-- 批量文件名 -->
				<DealType>
					<xsl:call-template name="tran_type">
						<xsl:with-param name="type">
							<xsl:value-of
								select="substring($FileName, 3, 1)" />
						</xsl:with-param>
					</xsl:call-template>
				</DealType><!-- 业务类型 -->
			</BaseInfo>
			<TranData>
				<TotalNum>
					<xsl:value-of
						select="/Transaction/Transaction_Body/PbOperSuccNum" />
				</TotalNum><!--当前批明细总笔数-->
				<TotalMoney>
					<xsl:value-of
						select="/Transaction/Transaction_Body/PbOperSuccSum" />
				</TotalMoney><!--当前批明细总金额-->
				<BankSuccNum></BankSuccNum>
				<BankSuccMoney></BankSuccMoney>

				<xsl:for-each
					select="/Transaction/Transaction_Body/Detail_List/Detail">
					<Detail>
						<AccName>
							<xsl:value-of select="BkCustName" />
						</AccName><!--客户姓名-->
						<AccNo>
							<xsl:value-of select="BkAcctNo" />
						</AccNo><!--帐号-->
						<IDType></IDType>
						<IDNo></IDNo>
						<PayCode>
							<xsl:value-of select="BkOthRetSeq" />
						</PayCode><!--保险公司方明细序号-->
						<NoType>
							<xsl:apply-templates select="LiOperType" />
						</NoType><!--业务类型-->
						<PolNo>
							<xsl:value-of select="PbRemark1" />
						</PolNo><!--保单号-->
						<PayMoney>
							<xsl:value-of select="BkAmt1" />
						</PayMoney><!--金额-->
						<SerialNo>
							<xsl:value-of select="PbInsuSlipNo" />
						</SerialNo><!--批次号-->
						<AgentCode></AgentCode>
						<BankSuccFlag>
							<xsl:apply-templates select="BkRetCode" />
						</BankSuccFlag><!--处理响应码-->
					</Detail>
				</xsl:for-each>
			</TranData>
		</TranData>
	</xsl:template>

	<xsl:template match="LiOperType">
		<xsl:choose>
			<xsl:when test=".='01'">02</xsl:when>
			<xsl:when test=".='02'">01</xsl:when>
			<xsl:when test=".='11'">06</xsl:when>
			<xsl:when test=".='12'">07</xsl:when>
			<xsl:when test=".='13'">08</xsl:when>
			<xsl:when test=".='14'">04</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="BkRetCode">
		<xsl:choose>
			<xsl:when test=".=00000">0000</xsl:when>
			<xsl:otherwise>1111</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="tran_type">
		<xsl:param name="type">0</xsl:param>
		<xsl:choose>
			<xsl:when test="$type = 0">S</xsl:when>
			<xsl:when test="$type = 1">F</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
