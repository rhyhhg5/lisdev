<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<!--冲正-->
		<TranData>
		<xsl:for-each select="/Transaction/Transaction_Header">
			<BaseInfo>
				<BankDate>
					<xsl:value-of select="BkPlatDate"/>
				</BankDate>
				<!--银行交易日期-->
				<BankCode>03</BankCode>
				<!--银行代码(cd05)-->
				<ZoneNo>
						<xsl:value-of select="substring(BkBrchNo, 1, 3)"/>
					</ZoneNo>
					<!--地区代码 -->
					<BrNo>
						<xsl:value-of select="substring(BkBrchNo, 4, 9)"/>
					</BrNo>
					<!--网点代码-->
					<TellerNo>	<!--建行银行柜员代码为12位，我方数据库(LKTRANSSTATUS)中对应字段(BANKOPERATOR)为10位，自动截取后10位。-->
						<xsl:value-of select="substring(BkTellerNo, 3)"/>
					</TellerNo>
				<!--柜员代码-->
				<TransrNo>
					<xsl:value-of select="BkPlatSeqNo"/>
				</TransrNo>
				<!--交易流水号-->
				<SaleChannel>
					<xsl:value-of select="BkChnlNo"/>
				</SaleChannel>
				<!--交易渠道代号-->
				<FunctionFlag>05</FunctionFlag>
				<!--处理标志(cd02) <xsl:value-of select="BkTxCode"/>-->
				<InsuID>
					<xsl:value-of select="PbInsuId"/>
				</InsuID>
				<!--保险公司代码(cd03) -->
			</BaseInfo>
			</xsl:for-each>
			<LCCont>
				<CardType>
					<xsl:value-of select="/Transaction/Transaction_Body/BkType1"/>
				</CardType>
				<StartNo>
					<xsl:value-of select="/Transaction/Transaction_Body/BkVchNo1"/>
				</StartNo>
				<EndNo>
					<xsl:value-of select="/Transaction/Transaction_Body/BkVchNo2"/>
				</EndNo>
			</LCCont>
		</TranData>
	</xsl:template>
</xsl:stylesheet>
