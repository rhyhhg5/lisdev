package com.sinosoft.midplat.channel.reformat.ccb;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.DateUtil;

public class BatRequest extends BaseFormat {
	private Element cTransaction_Header = null;
	private Element cBkFileName = null;

	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into BatRequest.getStdXml()...");
		
		cTransaction_Header =
			(Element) pNoStdXml.getRootElement().getChild("Transaction_Header").clone();
		cBkFileName = (Element)pNoStdXml.getRootElement().getChild("Transaction_Body").getChild("BkFileName").clone();
		InputStream mSheetIs = getClass().getResourceAsStream("BatRequestIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();

		cLogger.info("Out BatRequest.getStdXml()!");
		return mStdXml;
	}

	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into BatRequest.getNoStdXml()...");
		
		InputStream mSheetIs = getClass().getResourceAsStream("BatRequestOut.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document pNoStdXml = new XSLTransformer(mSheetIsr).transform(pStdXml);
		mSheetIsr.close();

		/*Start-组织返回报文头*/
		Element mBkOthDate = new Element(BkOthDate);
		mBkOthDate.setText(
				DateUtil.getCurrentDate("yyyyMMdd"));

		Element mBkOthSeq = new Element(BkOthSeq);
		mBkOthSeq.setText(cTransaction_Header.getChildText(BkPlatSeqNo));

		Element mBkOthRetCode = new Element(BkOthRetCode);
		Element mBkOthRetMsg = new Element(BkOthRetMsg);
		Element mRetData = pStdXml.getRootElement().getChild(RetData);
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			mBkOthRetCode.setText("00000");
			mBkOthRetMsg.setText("交易成功！");
		} else {	//交易失败
			mBkOthRetCode.setText("11111");
			mBkOthRetMsg.setText(
					mRetData.getChildText(Desc));
		}

		Element mTran_Response = new Element(Tran_Response);
		mTran_Response.addContent(mBkOthDate);
		mTran_Response.addContent(mBkOthSeq);
		mTran_Response.addContent(mBkOthRetCode);
		mTran_Response.addContent(mBkOthRetMsg);

		cTransaction_Header.addContent(mTran_Response);

		pNoStdXml.getRootElement().getChild("Transaction_Body").addContent(0, cBkFileName);
		pNoStdXml.getRootElement().addContent(0, cTransaction_Header);
		/*End-组织返回报文头*/

		cLogger.info("Out BatRequest.getNoStdXml()!");
		return pNoStdXml;
	}
}
