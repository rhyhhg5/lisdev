<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output indent="yes"/>
	<xsl:template match="/">
		<Transaction>
			<Transaction_Body>
				<!--# # # # 险种信息 # # # # -->
				<!--主险险种信息-->
				<PbInsuType>
					<xsl:call-template name="tran_RiskCode">
						<xsl:with-param name="RiskCode">
							<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/MainRiskCode"/>
						</xsl:with-param>
					</xsl:call-template>
				</PbInsuType>
				<PiEndDate>
					<xsl:value-of select="/TranData/LCCont/ContEndDate"/>
				</PiEndDate>
				<PbFinishDate>
					<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/PayToDate"/>
				</PbFinishDate>
				<LiDrawstring>
					<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/GetStartDate"/>
				</LiDrawstring>
				<!--# # # # 现金价值表# # # #因为建行不要一定传，所以直接置零先-->
				<LiCashValueCount>0</LiCashValueCount>
				<!--<xsl:value-of select="/TranData/LCCont/CashValues/CashValueCount"/>暂时置零-->
				<!--现金价值表循环，循环标签用Cash_List，每条现金价值的标签用Cash_Detail-->
				<!--<Cash_List>
					<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
						<xsl:variable name="RiskCD" select="RiskCode"/>
						<xsl:variable name="MainRiskCD" select="MainRiskCode"/>
						<xsl:if test="$RiskCD=$MainRiskCD">
							<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValues/CashValue">
								<Cash_Detail>
									<LiCashYearEnd>
										<xsl:value-of select="End"/>
									</LiCashYearEnd>
									<LiCashYearCash>
										<xsl:value-of select="Cash"/>
									</LiCashYearCash>
								</Cash_Detail>
							</xsl:for-each>
						</xsl:if>
					</xsl:for-each>
				</Cash_List>-->
				<!--循环结束-->
				<!--# # # # 红利保额保单年度末现金价值表 # # # #-->
				<LiBonusValueCount>0</LiBonusValueCount>
				<!--Bonus  红利保额保单年度末现金价值表循环-->
				<!--循环标签用Bonus_List，每条现金价值的标签用Bonus_Detail-->
				<!--<Bonus_List>
					<Bonus_Detail>
						<LiBonusEnd/>
						<LiBonusCash/>
					</Bonus_Detail>
				</Bonus_List>-->
				<!--循环结束-->
				<PbInsuSlipNo>
					<xsl:value-of select="/TranData/LCCont/ContNo"/>
				</PbInsuSlipNo>
				<!--保单号-->
				<BkTotAmt>
					<xsl:value-of select="/TranData/LCCont/Prem"/>
				</BkTotAmt>
				<!--总保费-->
				<LiSureRate>
					<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/SureRate"/>
				</LiSureRate>
				<!--保证利率-->
				<PbBrokId>
					<xsl:value-of select="/TranData/LCCont/AgentCode"/>
				</PbBrokId>
				<!--业务员代码-->
				<LiBrokName>
					<xsl:value-of select="/TranData/LCCont/AgentName"/>
				</LiBrokName>
				<!--业务员姓名-->
				<LiBrokGroupNo>
					<xsl:value-of select="/TranData/LCCont/AgentGroupCode"/>
				</LiBrokGroupNo>
				<!--业务员组号-->
				<BkOthName>
					<xsl:value-of select="/TranData/LCCont/LetterService"/>
				</BkOthName>
				<!--保险公司名称-->
				<BkOthAddr>
					<xsl:value-of select="/TranData/LCCont/ComLocation"/>
				</BkOthAddr>
				<!--保险公司地址-->
				<PiCpicZipcode/>
				<!--保险公司邮编-->
				<PiCpicTelno>
					<xsl:value-of select="/TranData/LCCont/ComPhone"/>
				</PiCpicTelno>
				<!--保险公司电话-->
				<xsl:variable name="SpecContent" select="/TranData/LCCont/SpecContent"/><!-- 新加入特别约定变量 -->
				<xsl:variable name="AppntName" select="/TranData/LCCont/LCAppnt/AppntName" />
				<xsl:variable name="AppntSex" select="/TranData/LCCont/LCAppnt/AppntSex" />
				<xsl:variable name="AppntBirthday" select="/TranData/LCCont/LCAppnt/AppntBirthday" />
				<xsl:variable name="AppntIDNo" select="/TranData/LCCont/LCAppnt/AppntIDNo" />
				
				<xsl:variable name="Name" select="/TranData/LCCont/LCInsureds/LCInsured/Name" />
				<xsl:variable name="Sex" select="/TranData/LCCont/LCInsureds/LCInsured/Sex" />
				<xsl:variable name="Birthday" select="/TranData/LCCont/LCInsureds/LCInsured/Birthday" />
				<xsl:variable name="IDNo" select="/TranData/LCCont/LCInsureds/LCInsured/IDNo" />
				
				<xsl:variable name="RiskName" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/RiskName" />
				<xsl:variable name="InsuYear" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/InsuYear" />
				<xsl:variable name="PayIntv" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/PayIntv" />
				<xsl:variable name="PayEndYear" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/PayEndYear" />
				<xsl:variable name="Prem" select="/TranData/LCCont/Prem" />
				<xsl:variable name="Mult" select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/Mult" />
				<xsl:variable name="Amnt" select="/TranData/LCCont/Amnt" />
				
				<xsl:variable name="ComLocation" select="/TranData/LCCont/ComLocation" />
				<xsl:variable name="AgentGroup" select="/TranData/LCCont/AgentGroup" />
				<xsl:variable name="AgentName" select="/TranData/LCCont/AgentName" />
				<xsl:variable name="ComPhone" select="/TranData/LCCont/ComPhone" />
				<xsl:variable name="PrintDate" select="/TranData/LCCont/PrintDate" />
				<xsl:variable name="BrNo" select="/TranData/LCCont/BrNo" />
				<xsl:variable name="SumPrem" select="/TranData/LCCont/SumPrem" />
				<xsl:variable name="ManageCom4" select="substring(/TranData/LCCont/ManageCom, 1, 4)" />
				
				<BkFileNum>1</BkFileNum>
				<Detail_List>
					<BkFileDesc>保单合同书</BkFileDesc>
					<BkType1>
						<xsl:if test="$ManageCom4=8637">370020000001</xsl:if>	<!-- 山东建行 -->
						<xsl:if test="$ManageCom4=8653">530020000001</xsl:if>	<!-- 云南建行 -->
						<xsl:if test="$ManageCom4=8641">410020000001</xsl:if>	<!-- 河南建行 -->
						<xsl:if test="$ManageCom4=8613">010020000001</xsl:if>	<!-- 河北建行 -->
						<xsl:if test="$ManageCom4=8632">320020000001</xsl:if>	<!-- 江苏建行 -->
						<xsl:if test="$ManageCom4=8642">420020000001</xsl:if>	<!-- 湖北建行 -->
						<xsl:if test="$ManageCom4=8651">510020000001</xsl:if>	<!-- 四川建行 -->
						<xsl:if test="$ManageCom4=8633">330020000001</xsl:if>	<!-- 浙江建行 -->
						<xsl:if test="$ManageCom4=8621">210020000001</xsl:if>	<!-- 辽宁建行 -->
						<xsl:if test="$ManageCom4=8636">360020000001</xsl:if>	<!-- 江西建行 -->
						<xsl:if test="$ManageCom4=8615">150020000001</xsl:if>	<!-- 内蒙古建行 -->
						<xsl:if test="$ManageCom4=8622">220020000001</xsl:if>	<!-- 吉林建行 -->
						<xsl:if test="$ManageCom4=8644">440020000001</xsl:if>	<!-- 广东建行 -->
						<xsl:if test="$ManageCom4=8635">350020000001</xsl:if>	<!-- 福建建行 -->
						<xsl:if test="$ManageCom4=8665">650020000001</xsl:if>	<!-- 新疆建行 -->
					</BkType1>
					<BkVchNo>
						<xsl:value-of select="/TranData/LCCont/ProposalContNo"/>
					</BkVchNo>
					<BkRecNum>52</BkRecNum>
					<Detail>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        保单合同号(保单号)：<xsl:value-of select="/TranData/LCCont/ContNo"/></BkDetail1>
						<BkDetail1>        保险合同生效日期：<xsl:value-of select="/TranData/LCCont/CValiDate"/>缴费方式：<xsl:value-of select="$PayIntv"/></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        投保人：<xsl:value-of select="$AppntName"/>性别：<xsl:value-of select="$AppntSex"/>生日：<xsl:value-of select="$AppntBirthday"/>   证件号码：<xsl:value-of select="$AppntIDNo"/> </BkDetail1>
						<BkDetail1>        被保险人：<xsl:value-of select="$Name"/>性别：<xsl:value-of select="$Sex"/>生日：<xsl:value-of select="$Birthday"/>   证件号码：<xsl:value-of select="$IDNo"/> </BkDetail1>
						<BkDetail1></BkDetail1>
						
						<!--  xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf"-->
						
						<!-- BkDetail1>        身故受益人：<xsl:value-of select="Name"/>受益比例：<xsl:value-of select="BnfLot"/>受益顺序：<xsl:value-of select="BnfNo"/></BkDetail1-->\
						<BkDetail1>        身故受益人：<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/Name"/>受益比例：<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/BnfLot"/>受益顺序：<xsl:value-of select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/BnfNo"/></BkDetail1>
						
						<!-- /xsl:for-each-->
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>　　　　　        险种名称                               保险期间 缴费年期     保额/份数      保险费/基本保险费</BkDetail1>
						<!-- BkDetail1><xsl:value-of select="$RiskName"/><xsl:value-of select="$InsuYear"/><xsl:value-of select="$PayEndYear"/><xsl:value-of select="$Amnt"/><xsl:value-of select="$Prem"/>元</BkDetail1-->
						<xsl:for-each select="/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk">
						<xsl:variable name="RiskName" select="RiskName" />
				        <xsl:variable name="InsuYear" select="InsuYear" />
				        <xsl:variable name="PayIntv" select="PayIntv" />
				        <xsl:variable name="PayEndYear" select="PayEndYear" />
				        <xsl:variable name="Prem" select="Prem" />
				        <xsl:variable name="Amnt" select="Amnt" />
						<BkDetail1><xsl:value-of select="$RiskName"/><xsl:value-of select="$InsuYear"/><xsl:value-of select="$PayEndYear"/><xsl:value-of select="$Amnt"/><xsl:value-of select="$Prem"/>元</BkDetail1>
						</xsl:for-each>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        本期保险费合计：<xsl:value-of select="$SumPrem"/></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        特别约定:<xsl:value-of select="$SpecContent"/></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1></BkDetail1>
						<BkDetail1>        公司地址：<xsl:value-of select="$ComLocation"/> 全国咨询电话：<xsl:value-of select="$ComPhone"/></BkDetail1>
						<BkDetail1>        营业网点：<xsl:value-of select="$BrNo"/>所属团队：<xsl:value-of select="$AgentGroup"/></BkDetail1>
						<BkDetail1>        保单打印时间：<xsl:value-of select="$PrintDate"/>业务员：<xsl:value-of select="$AgentName"/></BkDetail1>
					</Detail>
				</Detail_List>
			</Transaction_Body>
		</Transaction>
	</xsl:template>
	<xsl:template name="tran_RiskCode">
		<xsl:param name="RiskCode">0</xsl:param>
		<xsl:if test="$RiskCode = 331201">0002</xsl:if>	<!--健康人生个人护理保险(万能型，B款)-->
		<xsl:if test="$RiskCode = 331301">0003</xsl:if>	<!--健康人生个人护理保险(万能型，C款)-->
		<xsl:if test="$RiskCode = 331601">0004</xsl:if>	<!--健康人生个人护理保险(万能型，D款)-->
		<xsl:if test="$RiskCode = 331701">0006</xsl:if>	<!--金利宝个人护理保险（万能型）-->
		<xsl:if test="$RiskCode = 331901">0007</xsl:if>	<!--康利相伴个人护理保险（万能型）-->
	</xsl:template>
</xsl:stylesheet>
