package com.sinosoft.midplat.channel.reformat.ccb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.transform.XSLTransformer;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.NumberUtil;

public class ConfirmOrCancel extends BaseFormat {
	private Element cTransaction_Header = null;

	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into ConfirmOrCancel.getStdXml()...");
		
		cTransaction_Header =
			(Element) pNoStdXml.getRootElement().getChild("Transaction_Header").clone();

		InputStream mSheetIs = getClass().getResourceAsStream("NewContCOCIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();
		
		cLogger.info("Out ConfirmOrCancel.getStdXml()!");
		return mStdXml;
	}

	/**
	 * 供重打和查询使用。
	 */
	void setHeader(Element pTransaction_Header) {
		cTransaction_Header = pTransaction_Header;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into ConfirmOrCancel.getNoStdXml()...");

		Element mTranData = pStdXml.getRootElement();
		Element mRetData = mTranData.getChild(RetData);
		Element mLCCont = mTranData.getChild(LCCont);
		
		Document mNoStdXml = null;
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			//新加的两个险种调整打印内容


			String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk";
			List mList = XPath.selectNodes(pStdXml, mPath);
			cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
			for (int i = 0; i < mList.size(); i++) {

				Element tRisk = (Element) mList.get(i);						
				String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
				Element tLCContPrem = 
					tRisk.getParentElement().getParentElement().getParentElement().getParentElement().getChild(Prem);
				if (tMainRiskCodeStr.equals("331201")	//健康人生个人护理保险(万能型，B款)
						|| tMainRiskCodeStr.equals("331301")	//健康人生个人护理保险(万能型，C款)
						|| tMainRiskCodeStr.equals("331601")	//健康人生个人护理保险(万能型，D款)
						|| tMainRiskCodeStr.equals("230701")	//健康专家个人防癌疾病保险
						|| tMainRiskCodeStr.equals("240501")    //安心宝个人终身疾病重大保险
						|| tMainRiskCodeStr.equals("331701")    //金利宝个人护理保险(万能型)
						|| tMainRiskCodeStr.equals("331901")) {	//康利相伴个人护理保险（万能型）
					tRisk.getChild(Prem).setText(tLCContPrem.getText());	//银行方1个险种对应系统内两险种，银行要求返回总保费
				}


				//新增加的两个险种需要特别约定.而之前的四个险种无特别约定.根据险种,分别对特别约定赋值
				mPath = "/TranData/LCCont/SpecContent";
				Element mSpecContent = (Element)XPath.selectSingleNode(pStdXml, mPath);
				mSpecContent.setText("(无)");
				
				if(tMainRiskCodeStr.equals("240501")){//安心宝
					tRisk.getChild(RiskName).setText("安心宝个人终身重大疾病保障计划");
					tRisk.getChild(InsuYear).setText("终身");
                    tRisk.getChild(InsuYearFlag).setText("--");
					mSpecContent.setText("安心宝个人终身重大疾病保障计划是由《安心宝个人终身重大疾病保险》、《附加安心宝个人护理保险》组成。");
				}
				
				if(tMainRiskCodeStr.equals("331701")){//金利宝
					tRisk.getChild(RiskName).setText("金利宝个人护理增值计划（万能型）");
					mSpecContent.setText("金利宝个人护理增值计划（万能型）是由《金利宝个人护理保险（万能型）》、《附加金利宝个人意外伤害保险》组成。");
					tRisk.getChild(Amnt).setText("详见条款规定");
					mLCCont.getChild(Amnt).setText("0.00");//打印的时候取的是LCCont下的保额.后面会判断Amnt,不能直接置成"详见条款规定"
				}
			}
			
//			建行要求返回的日期格式必须是yyyyMMdd格式的,否则会报错.对日期进行转换
						
			/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
			Element mContEndDate = mLCCont.getChild("ContEndDate");
			mContEndDate.setText(
					DateUtil.formatTrans(mContEndDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
			
			/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
			Element mCValiDate = mLCCont.getChild(CValiDate);
			mCValiDate.setText(
					DateUtil.formatTrans(mCValiDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));

			fillLCCont(mLCCont);
			
			XPath tXPath = XPath.newInstance(
					"/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/MainRiskCode");
			String tMainRiskCode = tXPath.valueOf(pStdXml);
			String tXslName = null;
			if (tMainRiskCode.equals("331201")	//健康人生个人护理保险(万能型，B款)
					|| tMainRiskCode.equals("331301")	//健康人生个人护理保险(万能型，C款)
					|| tMainRiskCode.equals("331601")	//健康人生个人护理保险(万能型，D款)
					|| tMainRiskCode.equals("331701")   //金利宝个人护理保险(万能型)
					|| tMainRiskCode.equals("331901")
					|| tMainRiskCode.equals("332501")) {//康利相伴个人护理保险（万能型）
				tXslName = "NewContCOCOut.xsl";
			} else if (tMainRiskCode.equals("230701")||tMainRiskCode.equals("240501")||tMainRiskCode.equals("730101")) {	//健康专家个人防癌疾病保险和安心宝
				tXslName = "NewContCashCOCOut.xsl";
			} else {
				throw new BaseException("未配置该险种的报文转换文件(xsl)！" + tMainRiskCode);
			}
			
			InputStream tSheetIs = getClass().getResourceAsStream(tXslName);
			InputStreamReader tSheetIsr = new InputStreamReader(tSheetIs, "GBK");
			mNoStdXml = new XSLTransformer(tSheetIsr).transform(pStdXml);
			tSheetIsr.close();
		} else {	//交易失败
			Element mTransaction = new Element(Transaction);
			mNoStdXml = new Document(mTransaction);
		}

		/*Start-组织返回报文头*/
		Element mBkOthDate = new Element(BkOthDate);
		mBkOthDate.setText(
				DateUtil.getCurrentDate("yyyyMMdd"));

		Element mBkOthSeq = new Element(BkOthSeq);
		mBkOthSeq.setText(cTransaction_Header.getChildText(BkPlatSeqNo));

		Element mBkOthRetCode = new Element(BkOthRetCode);
		Element mBkOthRetMsg = new Element(BkOthRetMsg);
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			mBkOthRetCode.setText("00000");
			mBkOthRetMsg.setText("交易成功！");
		} else {	//交易失败
			mBkOthRetCode.setText("11111");
			mBkOthRetMsg.setText(
					mRetData.getChildText(Desc));
		}

		Element mTran_Response = new Element(Tran_Response);
		mTran_Response.addContent(mBkOthDate);
		mTran_Response.addContent(mBkOthSeq);
		mTran_Response.addContent(mBkOthRetCode);
		mTran_Response.addContent(mBkOthRetMsg);
    
		cTransaction_Header.addContent(mTran_Response);

		mNoStdXml.getRootElement().addContent(cTransaction_Header);
		/*End-组织返回报文头*/

		cLogger.info("Out ConfirmOrCancel.getStdXml()!");
		return mNoStdXml;
	}

	private void fillLCCont(Element pLCCont) throws JDOMException {
		cLogger.debug("Into ConfirmOrCancel.fillLCCont()...");
		
		//添加保单合同打印时间
		Element mPrintDate = new Element(PrintDate);
		mPrintDate.setText(
				fillString(DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss"), 53));
		pLCCont.addContent(mPrintDate);

		//添加网点代码
		Element mBrNo = new Element(BrNo);
//		mBrNo.setText(
//				fillString(cTransaction_Header.getChildTextTrim(BkBrchNo), 57));
		pLCCont.addContent(mBrNo);

		//添加保费总和(包括追加保费)
		Element mSumPrem = new Element(SumPrem);
		mSumPrem.setText(pLCCont.getChildText(Prem));
		String mPath = "LCInsureds/LCInsured/Risks/Risk";
		Element mRisk = (Element) XPath.selectSingleNode(pLCCont, mPath);
		if (mRisk.getChildText(MainRiskCode).equals("331301")) {	//picch万能C有个追加保费，需要累加上去
			double ttTotalPrem = 
				Double.parseDouble(pLCCont.getChildText(Prem))	+ Double.parseDouble(mRisk.getChildText(SupplementaryPrem));
			mSumPrem.setText(new DecimalFormat("0.00").format(ttTotalPrem));
		}
		String mSumPremStr = mSumPrem.getText();
		mSumPremStr = NumberUtil.getChnMoney(mSumPremStr) + " (RMB" + mSumPremStr + "元)";
		mSumPrem.setText(mSumPremStr);
		pLCCont.addContent(mSumPrem);
		
		Element mAmnt = pLCCont.getChild(Amnt);
		if (Double.parseDouble(mAmnt.getText()) < 0.01) {	//如果保额为0，则置为"--"
			mAmnt.setText("详见条款规定");
		}
		mAmnt.setText(
				fillString(mAmnt.getText(), 19));
		
		//公司地址
		Element mComLocation = pLCCont.getChild(ComLocation);
		mComLocation.setText(
				fillString(mComLocation.getText(), 56));
		
		//保单合同生效日期
		Element mCValiDate = pLCCont.getChild(CValiDate);
		mCValiDate.setText(
				fillString(mCValiDate.getText(), 65));
		
		fillLCAppnt(pLCCont.getChild(LCAppnt));
		
		fillLCInsureds(pLCCont.getChild(LCInsureds));
		
		cLogger.debug("Out ConfirmOrCancel.fillLCCont()!");
	}
	
	private void fillLCAppnt(Element pLCAppnt) {
		cLogger.debug("Into ConfirmOrCancel.fillLCAppnt()...");
		
		//投保人姓名
		Element mAppntName = pLCAppnt.getChild(AppntName);
		mAppntName.setText(
				fillString(mAppntName.getText(), 42));

		//投保人性别
		Element mAppntSex = pLCAppnt.getChild(AppntSex);
		mAppntSex.setText(
				fillString(
						getSexName(mAppntSex.getText()), 4));

		//投保人生日
		Element mAppntBirthday = pLCAppnt.getChild(AppntBirthday);
		mAppntBirthday.setText(
				fillString(mAppntBirthday.getText(), 9));
		
		cLogger.debug("Out ConfirmOrCancel.fillLCAppnt()!");
	}
	
	private void fillLCInsureds(Element pLCInsureds) {
		cLogger.debug("Into ConfirmOrCancel.fillLCInsureds()...");
		
		List mList = pLCInsureds.getChildren(LCInsured);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCInsured  = (Element) mList.get(i);

			//被保人名称
			Element tName = tLCInsured.getChild(Name);
			tName.setText(
					fillString(tName.getText(), 40));

			//被保人性别
			Element tSex = tLCInsured.getChild(Sex);
			tSex.setText(
					fillString(
							getSexName(tSex.getText()), 4));

			//被保人生日
			Element tBirthday = tLCInsured.getChild(Birthday);
			tBirthday.setText(
					fillString(tBirthday.getText(), 9));
			
			fillRisks(tLCInsured.getChild(Risks));
		}
		
		cLogger.debug("Out ConfirmOrCancel.fillLCInsureds()!");
	}
	
	private void fillRisks(Element pRisks) {
		cLogger.debug("Into ConfirmOrCancel.fillRisks()...");
		
		List mList = pRisks.getChildren(Risk);
		//带附加险时后置机返回的报文中险种顺序不定，所以此处处理把主险放到第一位
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk  = (Element) mList.get(i);
			String tMainRiskCode = tRisk.getChildText(MainRiskCode);
			String tRiskCode = tRisk.getChildText(RiskCode);
			if(tMainRiskCode.equals(tRiskCode)){
				Element tRiskCopy = (Element)tRisk.clone();
				Element tRisk1  = (Element) mList.get(0);
				Element tRiskCopy1 = (Element)tRisk1.clone();
				mList.remove(0);
				mList.add(0, tRiskCopy);
				mList.remove(i);
				mList.add(i, tRiskCopy1);				
			}
		}
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk  = (Element) mList.get(i);
			//险种名称(现在先按循环处理)
			Element tRiskName = tRisk.getChild(RiskName);
			String tRiskNameStr = "        " + tRiskName.getText();
			tRiskName.setText(
					fillString(tRiskNameStr, 60));
			
			//缴费方式
			Element tPayIntv = tRisk.getChild(PayIntv);
			String tPayIntvStr = tPayIntv.getText();
			tPayIntv.setText(
					getPayIntvName(tPayIntvStr));

			//缴费年期
			Element tPayEndYear = tRisk.getChild(PayEndYear);
			if (tPayIntvStr.equals("0")) {	//趸缴
				tPayEndYear.setText("--");
			}
			tPayEndYear.setText(
					fillString(tPayEndYear.getText(), 10));

			//保险期间
            String tInsuYearFlagStr = tRisk.getChildText(InsuYearFlag);
            Element tInsuYear = tRisk.getChild(InsuYear);
            String tInsuYearStr = tInsuYear.getText();
            if (tInsuYearFlagStr.equals("A")) {
                tInsuYearStr = "至"+ tInsuYearStr + "周岁";
                tInsuYearStr = fillString(tInsuYearStr, 12);
                tInsuYear.setText(tInsuYearStr);
            } else if (tInsuYearFlagStr.equals("--")) {//安心宝时需要返回终身,不加限制会返回终身岁
                tInsuYearStr = fillString(tInsuYearStr, 12);
                tInsuYear.setText(tInsuYearStr);
            } else {
                tInsuYearStr = tInsuYearStr + "年";
                tInsuYearStr = fillString(tInsuYearStr, 12);
                tInsuYear.setText(tInsuYearStr);
            }
            Element tAmnt = tRisk.getChild(Amnt);
            tAmnt.setText(fillString(tAmnt.getText(), 12));
            
			List tCashValueList = tRisk.getChild(CashValues).getChildren(CashValue);
			//现金价值
			for (int j = 0; j < tCashValueList.size(); j++) {
				Element ttCashValue = (Element) tCashValueList.get(j);
				
				//保单年度
				Element ttEnd = ttCashValue.getChild(End);
				String ttEndStr = "         " + ttEnd.getText();
				ttEnd.setText(
						fillString(ttEndStr, 22));
				
				//保单周年日
				Element ttCashYear = new Element("CashDate");
				String ttCValiDate = tRisk.getChildText(CValiDate);
				int ttYearInt = Integer.parseInt(
						ttCValiDate.substring(0, 4));
				ttYearInt += j + 1;
				String ttCashDateStr = ttYearInt + ttCValiDate.substring(4);
				ttCashYear.setText(
						fillString(ttCashDateStr, 18));
				ttCashValue.addContent(ttCashYear);
				
				//现金价值
				Element ttCash = ttCashValue.getChild(Cash);
				ttCash.setText(
						fillString(ttCash.getText(), 14));
			}
			
			
			if("240501".equals(tRisk.getChildText(RiskCode))){//安心宝
				//安心宝返回的现金价值数目过长,做一些条件裁剪
				List mListToCut = tRisk.getChild(CashValues).getChildren();
				Iterator mIterator = mListToCut.iterator();
				cLogger.debug("mList = CashValues.getChildren()"+mListToCut.size());
				
				int mOldCount = mListToCut.size() -1;//要求返回的现金价值链中最后一条现价必须返回
				int flag = 0;//第一条是现金价值链的统计数目.不对第一个子节点进行操作.
				while (mIterator.hasNext()) {

					if (flag != 0) {

						Element mCashValue = (Element) mIterator.next();
						int j = 0;//判断end值

						String m = mCashValue.getChildTextTrim("End");
						j = Integer.parseInt(m);
						if ((j > 5) && (j % 5 != 0)&&(j<mOldCount)) {
							mIterator.remove();
							cLogger.debug("mIterator.remove(),mIterator=" + mIterator);
						}

					} else {
						mIterator.next();//如果是第一个自己点,不做处理,转到下一条子节点.
					}

					flag++;
				}
				int count = mListToCut.size() - 1;//操作后剩余的价值链子节点数量
				tRisk.getChild(CashValues).getChild("CashValueCount").setText(String.valueOf(count));
				
				//end 现价裁剪
			}
			
			
			fillLCBnfs(tRisk.getChild(LCBnfs));
		}
		
		cLogger.debug("Out ConfirmOrCancel.fillRisks()!");
	}
	
	private void fillLCBnfs(Element pLCBnfs) {
		cLogger.debug("Into ConfirmOrCancel.fillLCBnfs()...");
		
		List mList = pLCBnfs.getChildren(LCBnf);
		
		/**
		 * picch核心规定无收益人即受益人为法定，这里根据需要生成一个“法定”受益人
		 */
		if (0 == mList.size()) {
			Element tName = new Element(Name);
			tName.setText("法定");
			
			Element tBnfLot = new Element(BnfLot);
			tBnfLot.setText("100");
			
			Element tBnfNo = new Element(BnfNo);
			tBnfNo.setText("1");

			Element tLCBnf = new Element(LCBnf);
			tLCBnf.addContent(tName);
			tLCBnf.addContent(tBnfLot);
			tLCBnf.addContent(tBnfNo);

			pLCBnfs.addContent(tLCBnf);
		}
		
		
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf  = (Element) mList.get(i);
			
			//身故受益人
			Element tName = tLCBnf.getChild(Name);
			tName.setText(
					fillString(tName.getText(), 40));

			//受益比例
			Element tBnfLot = tLCBnf.getChild(BnfLot);
			tBnfLot.setText(
					fillString(tBnfLot.getText()+'%', 25));
		}
		
		cLogger.debug("Out ConfirmOrCancel.fillLCBnfs()!");
	}
	
	/**
	 * 扩充指定字符串(pSrcStr)到指定列宽(pWidth)，并返回。
	 * 不足位，右补空格；超出指定列宽，返回原串。
	 */
	private String fillString(String pSrcStr, int pWidth) {
		int mSrcLength = 0;
		try {
			mSrcLength = pSrcStr.getBytes("GBK").length;	//打印时一个中文字符占两字节
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
			return pSrcStr;
		}

		StringBuffer mNewStrBuf = new StringBuffer(pSrcStr);
		for (int i = mSrcLength; i < pWidth; i++) {
			mNewStrBuf.append(" ");
		}

		return mNewStrBuf.toString();
	}
	

	private String getSexName(String pSex) {
		int mSexInt = 0;
		try {
			mSexInt = Integer.parseInt(pSex);
		} catch (NumberFormatException ex) {
			cLogger.error("证件类型有误！", ex);
			return pSex;
		}

		switch (mSexInt) {
		case 0 :
			return "男";
		case 1 :
			return "女";
		case 2 :
			return "不详";
		default:
			return pSex;
		}
	}

	private String getPayIntvName(String pPayIntvCode) {
		int mPayIntvInt = 0;
		try {
			mPayIntvInt = Integer.parseInt(pPayIntvCode);
		} catch (NumberFormatException ex) {
			cLogger.warn("缴费方式有误！" + pPayIntvCode);	//精简日志
			return pPayIntvCode;
		}

		switch (mPayIntvInt) {
		case -2 :
			return "约定缴费";
		case -1 :
			return "不定期缴";
		case 0 :
			return "趸缴";
		case 1 :
			return "月缴";
		case 3 :
			return "季缴";
		case 6 :
			return "半年缴";
		case 12 :
			return "年缴";
		default:
			return String.valueOf(pPayIntvCode);
		}
	}
	

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "D:/out.xml";
		String mOutFilePath = "D:/outnostd.xml";

//		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/newCont/ccb_01_NST_In.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/newCont/ccb_01_out_230701.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();

		Document mOutXmlDoc = new ConfirmOrCancel().getNoStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);

		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();

		System.out.println("成功结束！");
	}
}
