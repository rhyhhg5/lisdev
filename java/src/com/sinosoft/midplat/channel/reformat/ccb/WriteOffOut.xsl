<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes" />
	<xsl:template match="/">
		<Transaction>
			<!--报文体-->
			<Transaction_Body>
				<PbInsuType>
					<xsl:call-template name="tran_RiskCode">
						<xsl:with-param name="RiskCode">
							<xsl:value-of
								select="/TranData/LCCont/RiskCode" />
						</xsl:with-param>
					</xsl:call-template>
				</PbInsuType>
				<!--险种代码-->
				<PbInsuSlipNo>
					<xsl:value-of select="/TranData/LCCont/ContNo" />
				</PbInsuSlipNo>
				<!--保单号码-->
				<PbHoldName>
					<xsl:value-of select="/TranData/LCCont/AppntName" />
				</PbHoldName>
				<!--投保人姓名-->
				<LiRcgnName>
					<xsl:value-of select="/TranData/LCCont/InsuredName" />
				</LiRcgnName>
				<!--被保人姓名-->
				<LiLoanValue>
					<xsl:value-of select="/TranData/LCCont/Prem" />
				</LiLoanValue>
				<!--退保金额-->
				<BkAcctNo>
					<xsl:value-of select="/TranData/LCCont/BankAccNo" />
				</BkAcctNo>
				<!--银行帐号-->
			</Transaction_Body>
		</Transaction>
	</xsl:template>
	<xsl:template name="tran_RiskCode">
		<xsl:param name="RiskCode">0</xsl:param>
		<xsl:if test="$RiskCode = 331201">0002</xsl:if>
		<xsl:if test="$RiskCode = 230701">0001</xsl:if>
		<xsl:if test="$RiskCode = 331301">0003</xsl:if>
		<xsl:if test="$RiskCode = 331601">0004</xsl:if>
		<xsl:if test="$RiskCode = 240501">0005</xsl:if>
		<xsl:if test="$RiskCode = 331701">0006</xsl:if>
	</xsl:template>
</xsl:stylesheet>
