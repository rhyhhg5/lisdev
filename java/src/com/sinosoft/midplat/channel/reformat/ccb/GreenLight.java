package com.sinosoft.midplat.channel.reformat.ccb;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.DateUtil;

public class GreenLight extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into GreenLight.getStdXml()...");

		InputStream mSheetIs = getClass().getResourceAsStream("GreenLightIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();
		
		cLogger.info("Out GreenLight.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");
		
		//建行要规范日期返回格式,在此先转换一下
		if("1".equals(pStdXml.getRootElement().getChild(RetData).getChildText(Flag))){
			Element mBankDate = pStdXml.getRootElement().getChild(BaseInfo).getChild(BankDate);
			mBankDate.setText(
					DateUtil.formatTrans(mBankDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
		}
		
		InputStream mSheetIs = getClass().getResourceAsStream("GreenLightOut.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mNoStdXml = new XSLTransformer(mSheetIsr).transform(pStdXml);
		mSheetIsr.close();
		
		cLogger.info("Out NewCont.getNoStdXml()!");
		return mNoStdXml;
	}
}
