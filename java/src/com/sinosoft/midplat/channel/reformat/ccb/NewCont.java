package com.sinosoft.midplat.channel.reformat.ccb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class NewCont extends BaseFormat {
	private Element cTransaction_Header = null;

	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into NewCont.getStdXml()...");

		//此处备份一下请求报文头相关信息，组织返回报文时会用到
		cTransaction_Header =
			(Element) pNoStdXml.getRootElement().getChild("Transaction_Header").clone();

		InputStream mSheetIs = getClass().getResourceAsStream("NewContIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();

		/*Start-如果银行方邮寄地址不传，则自动置为家庭地址*/
		String mPath = "/TranData/LCCont/LCAppnt/MailAddress";
		Element mAppntMailAddress = (Element) XPath.selectSingleNode(mStdXml, mPath);
		if (mAppntMailAddress.getTextTrim().equals("")) {
			mAppntMailAddress.setText(
					mAppntMailAddress.getParentElement().getChildTextTrim(HomeAddress));
		}

		mPath = "/TranData/LCCont/LCAppnt/MailZipCode";
		Element mAppntMailZipCode = (Element) XPath.selectSingleNode(mStdXml, mPath);
		if (mAppntMailZipCode.getTextTrim().equals("")) {
			mAppntMailZipCode.setText(
					mAppntMailZipCode.getParentElement().getChildTextTrim(HomeZipCode));
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/MailAddress";
		List mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tMailAddress = (Element) mList.get(i);
			if (tMailAddress.getTextTrim().equals("")) {
				tMailAddress.setText(
						tMailAddress.getParentElement().getChildTextTrim(HomeAddress));
			}
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/MailZipCode";
		mList = XPath.selectNodes(mStdXml, mPath);
		for (int i = 0; i < mList.size(); i++) {
			Element tMailZipCode = (Element) mList.get(i);
			if (tMailZipCode.getTextTrim().equals("")) {
				tMailZipCode.setText(
						tMailZipCode.getParentElement().getChildTextTrim(HomeZipCode));
			}
		}
		/*End-如果银行方邮寄地址不传，则自动置为家庭地址*/

		//如果主险代码是 230701 时,组织一个附加险 331401
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(mStdXml, mPath);
		Element mRisk = mRisks.getChild(Risk);
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			/**
			 * 建行方无法录入保额，约定录入份数，每份10000元，
			 * 在此将份数转换为标准报文中的保额。
			 * 由于核心定义此产品为保额算保费的，无份数概念，所以将份数置为0，与核心一致。
			 */
			Element tMult = mRisk.getChild(Mult);
			Element tAmnt = mRisk.getChild(Amnt);
			double tAmntDbl = Double.parseDouble(tMult.getTextTrim()) * 10000;
			tAmnt.setText(String.valueOf(tAmntDbl));
			tMult.setText("0.00");

			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		
		//如果主险是 240501 时 ,组织一个附加险 340201
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {
			/**
			 * 建行方无法录入保额，约定录入份数，每份1000元，
			 * 在此将份数转换为标准报文中的保额。
			 * 安心宝是根据份数算保费.
			 */
//			Element tMult = mRisk.getChild(Mult);
//			Element tAmnt = mRisk.getChild(Amnt);
//			double tAmntDbl = Double.parseDouble(tMult.getTextTrim()) * 1000;
//			tAmnt.setText(String.valueOf(tAmntDbl));
			
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
		}

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf";
		List mListi = XPath.selectNodes(mStdXml, mPath);		
		for (int i = 0; i < mListi.size(); i ++) {
			Element tLCBnf = (Element) mListi.get(i);
			String tBnfLot = tLCBnf.getChildText(BnfLot);
			double tBnfLoti = Double.parseDouble(tBnfLot);
			int tBnfLotii = (int) tBnfLoti;
			tBnfLot = tBnfLotii + "";
			tLCBnf.getChild(BnfLot).setText(tBnfLot);
			tLCBnf.getChild(BnfGrade).setText(String.valueOf("1"));			
		}

		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);

		cLogger.info("Out NewCont.getStdXml()!");
		return mStdXml;
	}

	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");

		/**
		 * add by wangxt in 2009-3-25 begin
		 */		
		//判断是否是报错报文
		String mPath = "/TranData/RetData/Flag";
		Element mflag = (Element) XPath.selectSingleNode(pStdXml, mPath);
		if(mflag.getText().equals("1")) {

			mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk";
			List mList = XPath.selectNodes(pStdXml, mPath);
			cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
			for (int i = 0; i < mList.size(); i++) {

				Element tRisk = (Element) mList.get(i);						
				String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
				Element tLCContPrem = 
					tRisk.getParentElement().getParentElement().getParentElement().getParentElement().getChild(Prem);
				if (tMainRiskCodeStr.equals("331201")	//健康人生个人护理保险(万能型，B款)
						|| tMainRiskCodeStr.equals("331301")	//健康人生个人护理保险(万能型，C款)
						|| tMainRiskCodeStr.equals("331601")	//健康人生个人护理保险(万能型，D款)
						|| tMainRiskCodeStr.equals("230701")	//健康专家个人防癌疾病保险
						|| tMainRiskCodeStr.equals("240501")    //安心宝个人终身疾病重大保险
						|| tMainRiskCodeStr.equals("331701")    //金利宝个人护理保险(万能型)
						|| tMainRiskCodeStr.equals("331901")) {	//康利相伴个人护理保险（万能型）
					tRisk.getChild(Prem).setText(tLCContPrem.getText());	//银行方1个险种对应系统内两险种，银行要求返回总保费
				}

				if (tMainRiskCodeStr.equals("331301")) {	//picch万能C有个追加保费，需要累加上去
					double ttTotalPrem = 
						Double.parseDouble(tLCContPrem.getText())	+ Double.parseDouble(tRisk.getChildText(SupplementaryPrem));
					tLCContPrem.setText(new DecimalFormat("0.00").format(ttTotalPrem));
				}
				
				//新增加的两个险种需要特别约定.而之前的四个险种无特别约定.根据险种,分别对特别约定赋值
				mPath = "/TranData/LCCont/SpecContent";
				Element mSpecContent = (Element)XPath.selectSingleNode(pStdXml, mPath);
				mSpecContent.setText("(无)");
				
				if(tMainRiskCodeStr.equals("240501")){//安心宝
					tRisk.getChild(RiskName).setText("安心宝个人终身重大疾病保障计划");
					mSpecContent.setText("安心宝个人终身重大疾病保障计划是由《安心宝个人终身重大疾病保险》、《附加安心宝个人护理保险》组成。");
				}
				
				if(tMainRiskCodeStr.equals("331701")){//金利宝
					tRisk.getChild(RiskName).setText("金利宝个人护理增值计划（万能型）");
					mSpecContent.setText("金利宝个人护理增值计划（万能型）是由《金利宝个人护理保险（万能型）》、《附加金利宝个人意外伤害保险》组成。");
					tRisk.getChild(Amnt).setText("详见条款规定");
				}
			}
			//建行要求返回的日期格式必须是yyyyMMdd格式的,否则会报错.对日期进行转换
			mPath = "/TranData/LCCont";
			Element mLCCont = (Element)XPath.selectSingleNode(pStdXml, mPath);
			
			/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
			Element mContEndDate = mLCCont.getChild("ContEndDate");
			mContEndDate.setText(
					DateUtil.formatTrans(mContEndDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
			
			/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
			Element mCValiDate = mLCCont.getChild(CValiDate);
			mCValiDate.setText(
					DateUtil.formatTrans(mCValiDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));

		}
		/**
		 * add by wangxt in 2009-3-25 end
		 */

		InputStream mSheetIs = getClass().getResourceAsStream("NewContOut.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mNoStdXml = new XSLTransformer(mSheetIsr).transform(pStdXml);
		mSheetIsr.close();

		/*Start-组织返回报文头*/
		Element mBkOthDate = new Element(BkOthDate);
		mBkOthDate.setText(
				DateUtil.getCurrentDate("yyyyMMdd"));

		Element mBkOthSeq = new Element(BkOthSeq);
		mBkOthSeq.setText(cTransaction_Header.getChildText(BkPlatSeqNo));

		Element mBkOthRetCode = new Element(BkOthRetCode);
		Element mBkOthRetMsg = new Element(BkOthRetMsg);
		Element mRetData = pStdXml.getRootElement().getChild(RetData);
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			mBkOthRetCode.setText("00000");
			mBkOthRetMsg.setText("交易成功！");
		} else {	//交易失败
			mBkOthRetCode.setText("11111");
			mBkOthRetMsg.setText(
					mRetData.getChildText(Desc));
		}

		Element mTran_Response = new Element(Tran_Response);
		mTran_Response.addContent(mBkOthDate);
		mTran_Response.addContent(mBkOthSeq);
		mTran_Response.addContent(mBkOthRetCode);
		mTran_Response.addContent(mBkOthRetMsg);

		cTransaction_Header.addContent(mTran_Response);

		mNoStdXml.getRootElement().addContent(cTransaction_Header);
		/*End-组织返回报文头*/

		cLogger.info("Out NewCont.getNoStdXml()!");
		return mNoStdXml;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "D://innostd.xml";
		String mOutFilePath = "D:/instd.xml";

//		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/newCont/ccb_01_NST_In.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/newCont/ccb_01_out_230701.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();

		Document mOutXmlDoc = new NewCont().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);

		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();

		System.out.println("成功结束！");
	}
}