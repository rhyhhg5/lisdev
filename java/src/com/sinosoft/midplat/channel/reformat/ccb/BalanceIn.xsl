<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">
	<xsl:output indent="yes" />
	<xsl:template match="/">
		<TranData>
			<xsl:for-each select="/Transaction/Transaction_Header">
				<BaseInfo>
					<BankDate>
						<xsl:value-of select="BkPlatDate"/>
					</BankDate>
					<!--银行交易日期-->
					<BankCode>03</BankCode>
					<!--银行代码-->
					<ZoneNo>
						<xsl:value-of select="substring(BkBrchNo, 1, 3)"/>
					</ZoneNo>
					<!--地区代码-->
					<BrNo>
						<xsl:value-of select="substring(BkBrchNo, 4, 9)"/>
					</BrNo>
					<!--网点代码-->
					<TellerNo>	<!--建行银行柜员代码为12位，我方数据库(LKTRANSSTATUS)中对应字段(BANKOPERATOR)为10位，自动截取后10位。-->
						<xsl:value-of select="substring(BkTellerNo, 3)"/>
					</TellerNo>
					<!--柜员代码-->
					<TransrNo>
						<xsl:value-of select="BkPlatSeqNo"/>
					</TransrNo>
					<!--交易流水号-->
					<FunctionFlag>17</FunctionFlag>
					<!--处理标志-->
					<InsuID>
						<xsl:value-of select="PbInsuId"/>
					</InsuID>
					<!--保险公司代码-->
				</BaseInfo>
			</xsl:for-each>
			<ChkDetails>
				<TotalCount>
					<xsl:value-of select="/Transaction/Transaction_Body/BkTotNum"/>
					<!--对账明细数总笔数-->
				</TotalCount>
				<!--日终对帐明细交易数-->
				<xsl:for-each select="/Transaction/Transaction_Body/Detail_List/Detail">
					<ChkDetail>
						<!--日终对帐明细信息-->
						<BankCode>03</BankCode>
						<!--请求方银行代码-->
						<TranDate>
							<xsl:value-of select="BkPlatDate10"/>
						</TranDate>
						<!--请求方交易日期-->
						<BankZoneCode>
							<xsl:value-of select="substring(BkPlatBrch, 1, 3)"/>
						</BankZoneCode>
						<!--地区代码 -->
						<BrNo>
							<xsl:value-of select="substring(BkPlatBrch, 4, 9)"/>
						</BrNo>
						<!--网点代码-->
						<TellerNo />
						<FuncFlag>01</FuncFlag>
						<!--请求方交易码-->
						<TransrNo>
							<xsl:value-of select="BkChnlSeq"/>
						</TransrNo>
						<!--请求方交易流水号-->
						<CardNo>
							<xsl:value-of select="PbInsuSlipNo"/>
						</CardNo>
						<!--保单号-->
						<AppntName />
						<TranAmnt>
							<xsl:value-of select="BkTotAmt"/>
						</TranAmnt>
						<!--交易金额-->
						<ConfirmFlag>1</ConfirmFlag>
						<!--确认标志-->
					</ChkDetail>
				</xsl:for-each>
			</ChkDetails>
		</TranData>
	</xsl:template>
</xsl:stylesheet>