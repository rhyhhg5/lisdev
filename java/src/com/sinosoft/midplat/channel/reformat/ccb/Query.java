package com.sinosoft.midplat.channel.reformat.ccb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class Query extends BaseFormat {
	private Element cTransaction_Header = null;
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into Query.getStdXml()...");
		
		cTransaction_Header =
			(Element) pNoStdXml.getRootElement().getChild("Transaction_Header").clone();
		
		InputStream mSheetIs = getClass().getResourceAsStream("QueryIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();

		cLogger.info("Out Query.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Query.getNoStdXml()...");

		//查询和新单返回报文基本完全一样，所以直接调用
		ConfirmOrCancel mConfirmOrCancel = new ConfirmOrCancel();
		mConfirmOrCancel.setHeader(cTransaction_Header);
		Document mNoStdXml = mConfirmOrCancel.getNoStdXml(pStdXml);
		
		cLogger.info("Out Query.getNoStdXml()!");
		return mNoStdXml;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/query/ccb_09_in_NST.xml";
		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/query/ccb_09_in_ST.xml";

//		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/query/ccb_09_out.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/query/ccb_09_out_NST.xml";
		
		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new Query().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
}
