package com.sinosoft.midplat.channel.reformat.ccb;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class CancelInfoReturn extends BaseFormat {
	private Element cTransaction_Header = null;
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into CancelInfoReturn.getStdXml()...");

		//此处备份一下请求报文头相关信息，组织返回报文时会用到
		cTransaction_Header =
			(Element) pNoStdXml.getRootElement().getChild("Transaction_Header").clone();
		
		InputStream mSheetIs = getClass().getResourceAsStream("CancelInfoReturnIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();
		Element mCancelDate = mStdXml.getRootElement().getChild(LCCont).getChild("CancelDate");
		int mCancelDateB = Integer.parseInt(mCancelDate.getText().replaceAll("-", ""));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(calendar.DAY_OF_MONTH, -1);
		String mPayenddate =  new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
		mCancelDate.setText(mPayenddate);
		cLogger.info("Out CancelInfoReturn.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into CancelInfoReturn.getNoStdXml()...");

		InputStream mSheetIs = getClass().getResourceAsStream("CancelInfoReturnOut.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mNoStdXml = new XSLTransformer(mSheetIsr).transform(pStdXml);
		mSheetIsr.close();
		
		/*Start-组织返回报文头*/
		Element mBkOthDate = new Element(BkOthDate);
		mBkOthDate.setText(
				DateUtil.getCurrentDate("yyyyMMdd"));
		
		Element mBkOthSeq = new Element(BkOthSeq);
		mBkOthSeq.setText(cTransaction_Header.getChildText(BkPlatSeqNo));
		
		Element mBkOthRetCode = new Element(BkOthRetCode);
		Element mBkOthRetMsg = new Element(BkOthRetMsg);
		Element mRetData = pStdXml.getRootElement().getChild(RetData);
		if (mRetData.getChildText(Flag).equals("1")) {	//交易成功
			mBkOthRetCode.setText("00000");
			mBkOthRetMsg.setText("交易成功！");
		} else {	//交易失败
			mBkOthRetCode.setText("11111");
			mBkOthRetMsg.setText(
					mRetData.getChildText(Desc));
		}
		
		Element mTran_Response = new Element(Tran_Response);
		mTran_Response.addContent(mBkOthDate);
		mTran_Response.addContent(mBkOthSeq);
		mTran_Response.addContent(mBkOthRetCode);
		mTran_Response.addContent(mBkOthRetMsg);
		
		cTransaction_Header.addContent(mTran_Response);
		
		mNoStdXml.getRootElement().addContent(0, cTransaction_Header);
		/*End-组织返回报文头*/
		 
		cLogger.info("Out CancelInfoReturn.getNoStdXml()!");
		return mNoStdXml;
	}
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "D://innostd.xml";
		String mOutFilePath = "D:/outstd.xml";

//		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/newCont/ccb_01_NST_In.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/newCont/ccb_01_out_230701.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();

		Document mOutXmlDoc = new CancelInfoReturn().getNoStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);

		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();

		System.out.println("成功结束！");
	}
}
