package com.sinosoft.midplat.channel.reformat.bcm;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.JdomUtil;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

public class Trial extends BaseFormat
{
  public Document getStdXml(Document pNoStdXml)
    throws Exception
  {
    this.cLogger.info("Into Trial.getStdXml()...");

    List lcInsuredList = pNoStdXml.getRootElement().getChild("LCCont").getChild("LCInsureds").getChildren("LCInsured");
    for (int i = 0; i < lcInsuredList.size(); i++) {
      Element risk2 = ((Element)lcInsuredList.get(i)).getChild("Risks").getChild("Risk2");
      Element risk = ((Element)lcInsuredList.get(i)).getChild("Risks").getChild("Risk");
      if (risk2 != null) {
        risk2.getChild("MainRiskCode").setText(risk.getChildText("MainRiskCode"));
        risk2.setName("Risk");
      }

    }

    Element mTranData = pNoStdXml.getRootElement();

    Element mLCCont = mTranData.getChild("LCCont");

    String mPrtNoStr = mLCCont.getChildText("PrtNo");
    if ((mPrtNoStr == null) || (mPrtNoStr.equals(""))) {
      throw new BaseException("投保书印刷号不能为空!");
    }
    String mProposalContNoStr = "YBT" + mPrtNoStr;
    mLCCont.getChild("ProposalContNo").setText(mProposalContNoStr);

    String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
    Element mRisks = (Element)XPath.selectSingleNode(pNoStdXml, mPath);
    Element mRisk = mRisks.getChild("Risk");

    List tRiskList = pNoStdXml.getRootElement().getChild("LCCont").getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
    for (int i = 0; i < tRiskList.size(); i++) {
      String tMainRiskCode = ((Element)tRiskList.get(i)).getChildText("MainRiskCode");
      String tRiskCode = ((Element)tRiskList.get(i)).getChildText("RiskCode");
      if (("".equals(((Element)tRiskList.get(i)).getChildText("Mult"))) && (tRiskCode.equals(tMainRiskCode)))
      {
        ((Element)tRiskList.get(i)).getChild("Mult").setText("0.00");
      }

      if (("332401".equals(tRiskCode)) && ("".equals(((Element)tRiskList.get(i)).getChildText("Mult")))) {
        ((Element)tRiskList.get(i)).getChild("Mult").setText("1");
      }

      String tBonusGetMode = ((Element)tRiskList.get(i)).getChildText("BonusGetMode");
      if ("0".equals(tBonusGetMode))
        ((Element)tRiskList.get(i)).getChild("BonusGetMode").setText("1");
      else if ("2".equals(tBonusGetMode))
        ((Element)tRiskList.get(i)).getChild("BonusGetMode").setText("2");
      else {
        ((Element)tRiskList.get(i)).getChild("BonusGetMode").setText("");
      }
    }

    if (mRisk.getChildTextTrim("MainRiskCode").equals("230701")) {
      Element tRisk = (Element)mRisk.clone();
      tRisk.getChild("RiskCode").setText("331401");
      mRisks.addContent(tRisk);
    }

    if (mRisk.getChildTextTrim("MainRiskCode").equals("240501"))
    {
      if (("".equals(mRisk.getChildText("Prem"))) || (mRisk.getChildText("Prem") == null)) {
        throw new BaseException("安心宝保费不能为空,必须是1000的整数倍!");
      }

      Double mPrem1 = Double.valueOf(mRisk.getChildText("Prem"));
      int mPrem = mPrem1.intValue();
      if (mPrem % 1000 == 0)
        mRisk.getChild("Mult").setText(String.valueOf(mPrem / 1000));
      else {
        throw new BaseException("安心宝保费必须是1000的整数倍!");
      }

      mRisk.getChild("InsuYearFlag").setText("A");
    }

    if (mRisk.getChildTextTrim("MainRiskCode").equals("240501")) {
      Element tRisk = (Element)mRisk.clone();
      tRisk.getChild("RiskCode").setText("340201");
      mRisks.addContent(tRisk);
    }

    NewContCheck.check(pNoStdXml);

    this.cLogger.info("Out Trial.getStdXml()!");
    return pNoStdXml;
  }

  public Document getNoStdXml(Document pStdXml) throws Exception {
    this.cLogger.info("Into Trial.getNoStdXml()...");

    Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);

    "1".equals(mNoStdXml.getRootElement().getChild("RetData").getChildText("Flag"));

    this.cLogger.info("Out Trial.getNoStdXml()!");
    return mNoStdXml;
  }

  public static void main(String[] args) throws Exception {
    System.out.println("程序开始…");

    String mInFilePath = "E:/workplace/wuzhen_2011/报文格式/银行/交行/bcm_740803_23_133010.xml";
    String mOutFilePath = "E:/Test-haoqt/picch/bcm/testXml/newCont/bcm_yy_out.xml";

    InputStream mIs = new FileInputStream(mInFilePath);
    Document mInXmlDoc = JdomUtil.build(mIs);
    mIs.close();

    Document mOutXmlDoc = new Trial().getStdXml(mInXmlDoc);

    JdomUtil.print(mOutXmlDoc);

    OutputStream mOs = new FileOutputStream(mOutFilePath);
    JdomUtil.output(mOutXmlDoc, mOs);
    mOs.flush();
    mOs.close();

    System.out.println("成功结束！");
  }
}