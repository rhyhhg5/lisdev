package com.sinosoft.midplat.channel.reformat.bcm;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class RePrint extends BaseFormat {
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into RePrint.getNoStdXml()...");

		//重打和新单返回报文基本完全一样，所以直接调用
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		
		
		cLogger.info("Out RePrint.getNoStdXml()!");
		return mNoStdXml;
	}	
}