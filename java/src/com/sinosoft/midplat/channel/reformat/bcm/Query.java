package com.sinosoft.midplat.channel.reformat.bcm;

import org.jdom.Document;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class Query extends BaseFormat {
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Query.getNoStdXml()...");

		//查询和新单返回报文基本完全一样，所以直接调用
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		
		cLogger.info("Out Query.getNoStdXml()!");
		return mNoStdXml;
	}	
}