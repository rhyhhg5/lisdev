package com.sinosoft.midplat.channel.reformat.bcm;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class NewCont extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into NewCont.getStdXml()...");

//		Element mTranData = (Element) pNoStdXml.getRootElement();
//		Element mLCCont = mTranData.getChild(LCCont);
//		
//		Element mAccBankCode = mLCCont.getChild(AccBankCode);
//		if (mAccBankCode.equals("")) {
//			mAccBankCode.setText("10");
//		}
		
		//交行修改非标准请求报文格式，所以对应的标准报文格式也需调整
		//（附险节点为Risk2，转换到标准请求报文节点名为Risk）
		List lcInsuredList = pNoStdXml.getRootElement().getChild("LCCont").getChild("LCInsureds").getChildren("LCInsured");
		for (int i = 0; i < lcInsuredList.size(); i++) {
			Element risk2 = ((Element) lcInsuredList.get(i)).getChild("Risks").getChild("Risk2");
			Element risk = ((Element) lcInsuredList.get(i)).getChild("Risks").getChild("Risk");
			if(risk2 != null){
	//农行在附险节点里不传主险险种，银保通端自动赋值。 （add by 2012-03-20）
				risk2.getChild("MainRiskCode").setText(risk.getChildText("MainRiskCode"));
	//农行附险节点名为Risk2，将其转换为Risk （add by 2012-03-10）
				risk2.setName("Risk");
			}
			
		}
		
		List tRiskList = pNoStdXml.getRootElement().getChild("LCCont").getChild("LCInsureds").getChild("LCInsured").getChild("Risks").getChildren("Risk");
		System.out.println("############="+tRiskList.size());
		for (int i = 0; i < tRiskList.size(); i++) {
			if("332401".equals(((Element) tRiskList.get(i)).getChildText(RiskCode))){
				((Element) tRiskList.get(i)).getChild(Mult).setText("1");
			}
		}
		
		//如果主险代码是 230701 时,组织一个附加险 331401
		String mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(pNoStdXml, mPath);
		List mRisklist = mRisks.getChildren(Risk);
		for(int a=0;a<mRisklist.size();a++){
			//红利领取方式默认
//			((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("2");
			
			// 交行红利领取方式转换  
			String tBonusGetMode = ((Element) mRisklist.get(a)).getChildText("BonusGetMode");
			if("0".equals(tBonusGetMode)){
				((Element) mRisklist.get(a)).getChild("BonusGetMode").setText("1");
			}else if("2".equals(tBonusGetMode)){
				((Element) mRisklist.get(a)).getChild("BonusGetMode").setText("2");
			}else{
				((Element) mRisklist.get(a)).getChild("BonusGetMode").setText("");
			}
		}
		Element mRisk = mRisks.getChild(Risk);
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		
		if(mRisk.getChildTextTrim(MainRiskCode).equals("240501")){
			//校验保费是否为空
			if("".equals(mRisk.getChildText(Prem))|| null == mRisk.getChildText(Prem)){
				throw new BaseException("安心宝保费不能为空,必须是1000的整数倍!");
			}
			
			//交行不传份数,需要根据保费来进行转换(新加)
			Double mPrem1 = Double.valueOf(mRisk.getChildText(Prem));
			int mPrem = mPrem1.intValue();
			if(mPrem % 1000 == 0){
				mRisk.getChild(Mult).setText(String.valueOf(mPrem / 1000));
			}else{
				throw new BaseException("安心宝保费必须是1000的整数倍!");
			}
			//当主险代码是 240501 安心宝时,将insuyearflag置为A
			mRisk.getChild(InsuYearFlag).setText("A");
		}
		
		//如果主险是 240501 时 ,组织一个附加险 340201
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
		}
		//关于《未成年被保险人在其他保险公司累计身故保额》的问题
//		mPath = "/TranData/LCCont/InsCovSumAmt";
//		Element mInsCovSumAmt = (Element) XPath.selectSingleNode(pNoStdXml, mPath);
//		String tInsCovSumAmt = mInsCovSumAmt.getText();
//		mPath = "/TranData/LCCont/LCInsureds/LCInsured";
//		Element mInsCovSumAmt1 = (Element) XPath.selectSingleNode(pNoStdXml, mPath);
//		Element mInsCovSumAmt2 = new Element("InsCovSumAmt");
//		mInsCovSumAmt2.setText(tInsCovSumAmt);
//		mInsCovSumAmt1.addContent(mInsCovSumAmt2);
       //关于《未成年被保险人在其他保险公司累计身故保额》的问题
		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(pNoStdXml);
		
		cLogger.info("Out NewCont.getStdXml()!");
		return pNoStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");
		
		Element mTranDataEle = pStdXml.getRootElement();
		Element mRetDataEle = mTranDataEle.getChild(RetData);
		Element mLCContEle = mTranDataEle.getChild(LCCont);
		
				
		//判断是否是报错报文
		if("0".equals(mRetDataEle.getChildText(Flag))) {
			return pStdXml;
		}
		
		mLCContEle.removeChild("ContEndDate");
		
		Element mAmntEle = mLCContEle.getChild(Amnt);
		String mAmntStr = mAmntEle.getText();
		if ("".equals(mAmntStr)
			|| Double.parseDouble(mAmntStr) < 0.01) {	//针对无保额的险种，即保额为0.0
			mAmntEle.setText("--");
		}
		
		/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
		Element mCValiDate = mLCContEle.getChild(CValiDate);
		mCValiDate.setText(mCValiDate.getText().replaceAll("-", ""));
		
		Element mSignDate = mLCContEle.getChild("SignDate");
		mSignDate.setText(mSignDate.getText().replaceAll("-", ""));
		
		String mPath = "/TranData/LCCont/LCAppnt/AppntBirthday";
		Element mAppntBirthday = (Element) XPath.selectSingleNode(pStdXml, mPath);
		mAppntBirthday.setText(mAppntBirthday.getText().replaceAll("-", ""));
		
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Birthday";
		List mList = XPath.selectNodes(pStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tBirthday = (Element) mList.get(i);
			tBirthday.setText(tBirthday.getText().replaceAll("-", ""));
		}
		
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk";
		mList = XPath.selectNodes(pStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk = (Element) mList.get(i);
 			
 			
 			Element tAmntEle = tRisk.getChild(Amnt);
 			String tAmntStr = tAmntEle.getText();
 			if ("".equals(tAmntStr)
 				|| Double.parseDouble(tAmntStr) < 0.01) {	//针对无保额的险种，即保额为0.0
 				tAmntEle.setText("--");
 			}
 			
 			//趸缴产品的 保额 栏内容：原“--”更改为：“详见条款规定”
 			String tPayIntv = tRisk.getChildText(PayIntv);
 			//0代表趸交
 			if (tPayIntv.equals("0")) {
// 				tRisk.getChild(PayYears).setText("--");
 				tRisk.getChild(PayYears).setText("0");
 				tAmntEle.setText("详见条款规定");
 			}
 			
 			
 			Element tCValiDate = tRisk.getChild(CValiDate);
 			tCValiDate.setText(tCValiDate.getText().replaceAll("-", ""));
 			
 			String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
 			//康利人生两全保险（分红型）显示保额
 			if(tMainRiskCodeStr.equals("730101")){
 				tAmntEle.setText(tAmntStr);
 			}
 			Element tLCContPrem = 
					tRisk.getParentElement().getParentElement().getParentElement().getParentElement().getChild(Prem);
 			if (tMainRiskCodeStr.equals("331201")	//健康人生个人护理保险(万能型，B款)
 					|| tMainRiskCodeStr.equals("331301")	//健康人生个人护理保险(万能型，C款)
 					|| tMainRiskCodeStr.equals("331601")	//健康人生个人护理保险(万能型，C款)
 					|| tMainRiskCodeStr.equals("230701")	//健康专家个人防癌疾病保险
 					|| tMainRiskCodeStr.equals("240501")	//安心宝个人终身重大疾病保险    
 					|| tMainRiskCodeStr.equals("331701")    //金利宝个人护理保险（万能型）
 					|| tMainRiskCodeStr.equals("331901")) {	//康利相伴个人护理保险（万能型）
 						
 				tRisk.getChild(Prem).setText(tLCContPrem.getText());	//银行方1个险种对应系统内两险种，银行要求返回总保费
 			}
 			
 			if (tMainRiskCodeStr.equals("331301")) {	//picch万能C有个追加保费，需要累加上去
 				double ttTotalPrem = 
 					Double.parseDouble(tLCContPrem.getText())	+ Double.parseDouble(tRisk.getChildText(SupplementaryPrem));
 				tLCContPrem.setText(new DecimalFormat("0.00").format(ttTotalPrem));
 			}
 			

		}
		List lcInsuredList = pStdXml.getRootElement().getChild("LCCont").getChild("LCInsureds").getChildren("LCInsured");
		List riskList_1 = ((Element) lcInsuredList.get(0)).getChild("Risks").getChildren("Risk");
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk";
		Element mRiskForBnf = (Element) riskList_1.get(0);
		
		if(! mRiskForBnf.getChildText(MainRiskCode).equals(mRiskForBnf.getChildText(RiskCode))){
			mRiskForBnf = (Element) riskList_1.get(1);
		}
		
		//经与交行IT开发人员沟通，康利人生两全保险（分红型）针对保单中“保险期间”字段的问题，采用如下方案解决：
        //原字段以星号（*）代替，打印在保单上为“至*周岁”的形式；
         //在保单特别约定处打印相关内容，格式为：“本保险合同的保险期间为x年”，x根据保单实际情况进行变化。
		if ("730101".equals(mRiskForBnf.getChildText(MainRiskCode))) {
			if (!mLCContEle.getChildText(ProposalContNo).startsWith("YBT")) {
				Element mYears = mRiskForBnf.getChild(Years);
				Element mSpecContent = mLCContEle.getChild(SpecContent);
				mSpecContent.setText("本保险合同的保险期间为" + mYears.getText() + "年");
				mYears.setText("*");
				Element mInsuYears = mRiskForBnf.getChild(InsuYear);
				mInsuYears.setText("*");
			}
		}

		if("240501".equals(mRiskForBnf.getChildText(MainRiskCode))){
			//如果主险是 240501 安心宝,将受益人里的受益比例加上百分号
//			mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/BnfLot";
//			mList = XPath.selectNodes(pStdXml, mPath);
//			cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
//			for (int i = 0; i < mList.size(); i++) {
//				Element tBnfLot = (Element) mList.get(i);
//				String mOldtBnfLot = tBnfLot.getText();
//				tBnfLot.setText(mOldtBnfLot+"%");
//			}
			Element mRiskName = mRiskForBnf.getChild(RiskName);
			mRiskName.setText("安心宝个人终身重大疾病保障计划");
			
			Element mSpecContent = mLCContEle.getChild(SpecContent);
			mSpecContent.setText("安心宝个人终身重大疾病保障计划是由《安心宝个人终身重大疾病保险》、《附加安心宝个人护理保险》组成。");
            
//        在生产上发现安心宝返回给交行的现金价值链过长, 要求按照农行模式打印现金价值链,20年以后,每五年打印一次.
            Element mCashValues = mRiskForBnf.getChild(CashValues);
            mList = mCashValues.getChildren();
            Iterator i = mList.iterator();
            cLogger.debug("mList = mCashValues.getChildren()"+mList.size());
            
            int mOldCount = mList.size() -1;//要求返回的现金价值链中最后一条现价必须返回
            int flag = 0;//第一条是现金价值链的统计数目.不对第一个子节点进行操作.
            while (i.hasNext()) {

                if (flag != 0) {

                    Element mCashValue = (Element) i.next();
                    int j = 0;//判断end值

                    String m = mCashValue.getChildText("End");
                    j = Integer.parseInt(m);
                    if ((j > 20) && (j % 5 != 0)&&(j<mOldCount)) {
                        i.remove();
                        cLogger.debug("i.remove(),i=" + i);
                    }

                } else {
                    i.next();//如果是第一个自己点,不做处理,转到下一条子节点.
                }

                flag++;
            }
            int count = mList.size() - 1;//操作后剩余的价值链子节点数量
            mCashValues.getChild(CashValueCount).setText(String.valueOf(count));
			
			//speccontent
//			mLCContEle.getChild(SpecContent).setText("根据本保险条款第3.3条规定，本保险单保险期间“至106周岁”准确含意是指本合同保险期间为被保险人终身。");
		}
		
		if("331701".equals(mRiskForBnf.getChildText(MainRiskCode))){
			//如果主险是 331701 金利宝,将受益人里的受益比例加上百分号
//			mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/BnfLot";
//			mList = XPath.selectNodes(pStdXml, mPath);
//			cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
//			for (int i = 0; i < mList.size(); i++) {
//				Element tBnfLot = (Element) mList.get(i);
//				String mOldtBnfLot = tBnfLot.getText();
//				tBnfLot.setText(mOldtBnfLot+"%");
//			}
			mRiskForBnf.getChild(Amnt).setText("详见条款规定");
			
			Element mRiskName = mRiskForBnf.getChild(RiskName);
			mRiskName.setText("金利宝个人护理增值计划（万能型）");
			
			Element mSpecContent = mLCContEle.getChild(SpecContent);
			mSpecContent.setText("金利宝个人护理增值计划（万能型）是由《金利宝个人护理保险（万能型）》、《附加金利宝个人意外伤害保险》组成。");
		}
		if("332501".equals(mRiskForBnf.getChildText(MainRiskCode))){
			Element mSpecContent = mLCContEle.getChild(SpecContent);
			mSpecContent.setText("本保险增值计划(万能型,E款)由《健康人生个人护理保险(万能型,E款)》和《附加健康人生个人意外伤害保险(E款)》组成.");
		} 
		//带附加险时后置机返回的报文中险种顺序不定，所以此处处理把主险放到第一位
		List mList1 = mLCContEle.getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChildren(Risk);
		for (int i = 0; i < mList1.size(); i++) {
			Element tRisk  = (Element) mList1.get(i);
			String tMainRiskCode = tRisk.getChildText(MainRiskCode);
			String tRiskCode = tRisk.getChildText(RiskCode);			
			if(tMainRiskCode.equals(tRiskCode)){
				Element tRiskCopy = (Element)tRisk.clone();
				Element tRisk1  = (Element) mList1.get(0);
				Element tRiskCopy1 = (Element)tRisk1.clone();
				mList1.remove(0);
				mList1.add(0, tRiskCopy);
				mList1.remove(i);
				mList1.add(i, tRiskCopy1);				
			}
		}
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/Birthday";
		mList = XPath.selectNodes(pStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tBirthday = (Element) mList.get(i);
			tBirthday.setText(tBirthday.getText().replaceAll("-", ""));
		}
		/*End-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/

		for (int i = 0; i < lcInsuredList.size(); i++) {
			List riskList = ((Element) lcInsuredList.get(i)).getChild("Risks").getChildren("Risk");
			Element riskCount = new Element("RiskCount");
			((Element) lcInsuredList.get(i)).getChild("Risks").addContent(0,riskCount.setText(riskList.size()+""));
			for (int j = 1; j < riskList.size(); j++) {
				((Element)riskList.get(1)).setName("Risk2");
			} 
		}
		
		
		cLogger.info("Out NewCont.getNoStdXml()!");
		return pStdXml;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
//		String mInFilePath = "C:\\Documents and Settings\\Administrator\\桌面\\交行文档\\bcm_949941_01_102840_inNoStd.xml";
		String mInFilePath = "E:/workplace/wuzhen_2011/报文格式/银行/交行/bcm_919169_01_093434.xml";
		String mOutFilePath = "d:/out.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new NewCont().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
}