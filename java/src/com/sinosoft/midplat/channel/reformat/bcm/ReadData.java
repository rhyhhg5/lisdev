package com.sinosoft.midplat.channel.reformat.bcm;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class ReadData {

	public static Document getDocument(){
		
		Document mStaXML = null;
		Element tTranData = new Element("TranData");
		Element tBaseInfo = new Element("BaseInfo");
		Element bank = new Element("BankCode");
		bank.setText("10");
		Element funcFlag = new Element("FunctionFlag");
		funcFlag.setText("18");
		tBaseInfo.addContent(bank);
		tBaseInfo.addContent(funcFlag);
		tTranData.addContent(tBaseInfo);
		mStaXML = new Document(tTranData);
		return mStaXML;
	}
	
	public static void main(String[] args) throws Exception{
		
		Document pInStdXmlDoc = ReadData.getDocument();
		String mPostServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
		System.out.println(mPostServletURL);  
		URL mURL = new URL(mPostServletURL);
		URLConnection mURLConnection = mURL.openConnection();
		mURLConnection.setDoOutput(true);
		mURLConnection.setDoInput(true);
		OutputStream mURLOs = mURLConnection.getOutputStream();
		XMLOutputter mXMLOutputter = new XMLOutputter(Format.getCompactFormat().setEncoding("GBK"));
		mXMLOutputter.output(pInStdXmlDoc, mURLOs);
		mURLOs.close();
		System.out.println("后置机返回报文接收！");
		InputStream mURLIs = mURLConnection.getInputStream();
		Document mOutStdXml = JdomUtil.build(mURLIs);	//统一使用GBK编码
		mURLIs.close();
		
		
	}
}
