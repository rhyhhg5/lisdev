package com.sinosoft.midplat.channel.reformat;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.reformat.bcm.ReadData;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;

public class BatDailyPhone {

	private static Document inStdDoc;
public static Document getDocument(){
		
		Document mStaXML = null;
		Element tTranData = new Element("TranData");
		Element tBaseInfo = new Element("BaseInfo");
		Element bank = new Element("BankCode");
		bank.setText("59");
		Element funcFlag = new Element("FunctionFlag");
		funcFlag.setText("60");
		tBaseInfo.addContent(bank);
		tBaseInfo.addContent(funcFlag);
		tTranData.addContent(tBaseInfo);
		mStaXML = new Document(tTranData);
		inStdDoc = mStaXML;
		return mStaXML;
	}
	
	public static void main(String[] args) throws Exception{
		
		Document pInStdXmlDoc = BatDailyPhone.getDocument();
		String mPostServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
		URL mURL = new URL(mPostServletURL);
		URLConnection mURLConnection = mURL.openConnection();
		mURLConnection.setDoOutput(true);
		mURLConnection.setDoInput(true);
		OutputStream mURLOs = mURLConnection.getOutputStream();
		XMLOutputter mXMLOutputter = new XMLOutputter(Format.getCompactFormat().setEncoding("GBK"));
		mXMLOutputter.output(pInStdXmlDoc, mURLOs);
		mURLOs.close();
		System.out.println("后置机返回报文接收！");
		InputStream mURLIs = mURLConnection.getInputStream();
		Document mOutStdXml = JdomUtil.build(mURLIs);	//统一使用GBK编码
		
		//保存返回报文，以便查看当晚批处理是否成功
		StringBuffer mSaveName = new StringBuffer();
		Element xBaseInfo = inStdDoc.getRootElement().getChild("BaseInfo");
		mSaveName.append(xBaseInfo.getChildText("BankCode")).append("_").append(xBaseInfo.getChildText("TransNo")).append("_")
			.append(xBaseInfo.getChildText("FunctionFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mOutStdXml, mSaveName.toString(), "OutStd");
		
		JdomUtil.print(mOutStdXml);
		mURLIs.close();
		
		
	}
	
}
