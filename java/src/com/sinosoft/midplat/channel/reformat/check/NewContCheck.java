/**
 * 遍历标准请求报文，做空值校验。
 */

package com.sinosoft.midplat.channel.reformat.check;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.XmlTag;

public class NewContCheck implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(NewContCheck.class);
	private static String mBankCode = null;
	private static String regex = "^[a-zA-Z\u4e00-\u9fa5\\.\\·]*"; 
	private static String reg_phone = "(^[0-9]+[\\-]?[0-9]+)";
	
	public static void check(Document pStdXml) throws Exception {
		cLogger.info("Into NewContCheck.check()...");

		Element mTranData = pStdXml.getRootElement();

		Element mBaseInfo = mTranData.getChild(BaseInfo);
		checkBaseInfo(mBaseInfo);

		Element mLCCont = mTranData.getChild(LCCont);
		checkLCCont(mLCCont);

		cLogger.info("Out NewContCheck.check()!");
	}

	/**
	 * 1. 交易流水号不能为空
	 * 2. 地区代码、网点代码不能为空
	 * 3. 交易代码不能为空
	 * 4. 银行代码不能为空
	 */
	private static void checkBaseInfo(Element pBaseInfo) throws Exception {
		cLogger.debug("Into NewContCheck.checkBaseInfo()...");

		if (null == pBaseInfo) {
			throw new BaseException("BaseInfo节点缺失！");
		}

		String mTransrNo = pBaseInfo.getChildTextTrim(TransrNo);
		if ((null == mTransrNo) || mTransrNo.equals("")) {
			throw new BaseException("交易流水号不能为空！");
		}

		String mZoneNo = pBaseInfo.getChildTextTrim(ZoneNo);
		if ((null == mZoneNo) || mZoneNo.equals("")) {
			throw new BaseException("地区代码不能为空！");
		}

		String mBrNo = pBaseInfo.getChildTextTrim(BrNo);
		if ((null == mBrNo) || mBrNo.equals("")) {
			throw new BaseException("网点代码不能为空！");
		}

		String mFunctionFlag = pBaseInfo.getChildTextTrim(FunctionFlag);
		if ((null == mFunctionFlag) || mFunctionFlag.equals("")) {
			throw new BaseException("交易代码不能为空！");
		}

		mBankCode = pBaseInfo.getChildTextTrim("BankCode");
		if ((null == mBankCode) || mBankCode.equals("")) {
			throw new BaseException("银行代码不能为空！");
		}
		cLogger.debug("Out NewContCheck.checkBaseInfo()!");
	}

	/**
	 * 1. 投保书印刷号(PrtNo)不能为空
	 * 2. 保单合同书印刷号(ProposalContNo)不能为空
	 * 3. 帐户号码、账户名称、开户银行不能为空
	 */
	private static void checkLCCont(Element pLCCont) throws Exception {
		cLogger.debug("Into NewContCheck.checkLCCont()...");

		if (null == pLCCont) {
			throw new BaseException("LCCont节点缺失！");
		}

		String mPrtNo = pLCCont.getChildTextTrim(PrtNo);
		if ((null == mPrtNo) || mPrtNo.equals("")) {
			throw new BaseException("投保书印刷号不能为空！");
		}

		String mProposalContNo = pLCCont.getChildTextTrim(ProposalContNo);
		if ((null == mProposalContNo) || mProposalContNo.equals("")) {
			throw new BaseException("保单合同书印刷号不能为空！");
		}

		String mBankAccNo = pLCCont.getChildTextTrim(BankAccNo);
		if ((mBankAccNo != null) && !mBankAccNo.equals("")) {

			String mAccName = pLCCont.getChildTextTrim(AccName);
			if ((null == mAccName) || mAccName.equals("")) {
				throw new BaseException("银行帐户名称不能为空！");
			}

			String mAccBankCode = pLCCont.getChildTextTrim(AccBankCode);
			if ((null == mAccBankCode) || mAccBankCode.equals("")) {
				throw new BaseException("开户银行不能为空！");
			}
		}
		String mPayIntv = pLCCont.getChildTextTrim(PayIntv);
		if(!"0".equals(mPayIntv)){
			if((null == mBankAccNo) || mBankAccNo.equals("")){
				throw new BaseException("期交产品如未提供续期账号，不予承保！");
			}
		}
		checkLCAppnt(pLCCont.getChild(LCAppnt)); 

		checkLCInsureds(pLCCont.getChild(LCInsureds));

		cLogger.debug("Out NewContCheck.checkLCCont()!");
	}

	/**
	 * 1. 投保人姓名、性别、证件类型、证件号码、职业不能为空
	 * 2. 投保人地址、电话不能为空(先不校验)
	 */
	private static void checkLCAppnt(Element pLCAppnt) throws Exception {
		cLogger.debug("Into NewContCheck.checkLCAppnt()...");

		if (null == pLCAppnt) {
			throw new BaseException("LCAppnt节点缺失！");
		}

		String mAppntName = pLCAppnt.getChildTextTrim(AppntName);
		if ((null == mAppntName) || mAppntName.equals("")) {
			throw new BaseException("投保人姓名不能为空！");
		}else{
		    if(!Pattern.compile(regex).matcher(mAppntName).matches()){
		    	throw new BaseException("投保人姓名只能为字母或中文！");
		    }
		}

		String mAppntSex = pLCAppnt.getChildTextTrim(AppntSex);
		if ((null == mAppntSex) || mAppntSex.equals("")) {
			throw new BaseException("投保人性别不能为空！");
		}else if(!("1".equals(mAppntSex) || "0".equals(mAppntSex))){
			throw new BaseException("输入性别错误：请检查录入的投保人性别是否有误！");
		}

		String mAppntIDType = pLCAppnt.getChildTextTrim(AppntIDType);
		if ((null == mAppntIDType) || mAppntIDType.equals("")) {
			throw new BaseException("投保人证件类型不能为空！");
		}else {
			// 当证件类型转换完后不为0,1,2,3,4,5将其转换为4.
			if(!("0".equals(mAppntIDType) || "1".equals(mAppntIDType) || "2".equals(mAppntIDType) || "3".equals(mAppntIDType) || "4".equals(mAppntIDType) || "5".equals(mAppntIDType)) ){
				System.out.println("tIDType="+mAppntIDType);
				pLCAppnt.getChild(AppntIDType).setText("4");
			}
		}

		String mAppntIDNo = pLCAppnt.getChildTextTrim(AppntIDNo);
		if ((null == mAppntIDNo) || mAppntIDNo.equals("")) {
			throw new BaseException("投保人证件号码不能为空！");
		}

		String mJobCode = pLCAppnt.getChildTextTrim(JobCode);
		if ((null == mJobCode) || mJobCode.equals("")) {
			throw new BaseException("投保人职业不能为空！");
		}

		String mMailAddress = pLCAppnt.getChildTextTrim(MailAddress);
		if ((null == mMailAddress) || mMailAddress.equals("")) {
			throw new BaseException("投保人邮寄地址不能为空！");
		}

		String mAppntBirthday = pLCAppnt.getChildTextTrim(AppntBirthday);
		if((null == mAppntBirthday) || mAppntBirthday.equals("")) {
			throw new BaseException("投保人生日不能为空！");
		}
		
		if (mAppntIDType.equals("0")) {	//身份证
			checkIDNo (mAppntIDNo,mAppntBirthday,mAppntSex);
		}
		
		// 当投保人电话号码不为空时判断其规则
		String xAppntOfficePhone = pLCAppnt.getChildText("AppntOfficePhone");
		String xAppntMobile = pLCAppnt.getChildText("AppntMobile");
		String xAppntPhone = pLCAppnt.getChildText("AppntPhone");
		if(!("".equals(xAppntPhone) || xAppntPhone == null)){
			checkTelLength11(xAppntPhone);
			
			if(!Pattern.compile(reg_phone).matcher(xAppntPhone).matches()){
				throw new BaseException("投保人固定电话不符合规则！");
			}
				
			
		}
		if(!("".equals(xAppntMobile) || xAppntMobile == null)){
			checkTelLength11(xAppntMobile);
			
			if(!Pattern.compile(reg_phone).matcher(xAppntMobile).matches()){
				throw new BaseException("投保人移动电话不符合规则！");
			}
				
		}
		if(!("".equals(xAppntOfficePhone) || xAppntOfficePhone == null)){
			checkTelLength11(xAppntOfficePhone);
			if(!Pattern.compile(reg_phone).matcher(xAppntOfficePhone).matches()){
				throw new BaseException("投保人移动电话不符合规则！");
			}
		}
		cLogger.debug("Out NewContCheck.checkLCAppnt()!");
	}

	private static void checkIDNo(String idno, String birthday, String sex) throws BaseException {
		//根据身份证号判断出生年月和性别是否与身份证相符
		String tmpbirthday = "";
		String tmpsex = "";
		if (idno.trim().length() == 18) {
			tmpbirthday = idno.substring(6, 14);
			tmpsex = idno.substring(16, 17);
		} else if (idno.trim().length() == 15) {
			tmpbirthday = idno.substring(6, 12);
			tmpbirthday = "19" + tmpbirthday;
			tmpsex = idno.substring(14, 15);
		} else {
			throw new BaseException("身份证号有误！" + idno);
		}
		
		/**
		 * add by wangxt in 2009-4-23
		 */
		birthday = birthday.replaceAll("-", "");
		if (!birthday.equals(tmpbirthday)) {
			throw new BaseException("身份证号与出生日期不符！" + idno);
		}
		if (tmpsex.equals("0") || tmpsex.equals("2") || tmpsex.equals("4") ||
				tmpsex.equals("6") || tmpsex.equals("8")) {
			if (sex == null || sex.trim().equals("0")) {
				throw new BaseException("身份证号与性别不符！" + idno);
			}
		} else {
			if (sex == null || sex.trim().equals("1")) {
				throw new BaseException("身份证号与性别不符！" + idno);
			}
		}
	}

	/**
	 * 1. 被保人姓名、性别、证件类型、证件号码、职业不能为空
	 * 2. 被保人地址、电话不能为空
	 * 3. 与投保人关系不能为空
	 * 4. 与主被投保人关系不能为空
	 */
	private static void checkLCInsureds(Element pLCInsureds) throws Exception {
		cLogger.debug("Into NewContCheck.checkLCInsureds()...");

		if (null == pLCInsureds) {
			throw new BaseException("LCInsureds节点缺失！");
		}

		List mList = pLCInsureds.getChildren(LCInsured);
		for (int i = 0; i < mList.size(); i++) {
			Element tLCInsured  = (Element) mList.get(i);
			
			String tName = tLCInsured.getChildTextTrim(Name);
			if ((null == tName) || tName.equals("")) {
				throw new BaseException("被保人姓名不能为空！");
			}else{
			    if(!Pattern.compile(regex).matcher(tName).matches()){
			    	throw new BaseException("被保人姓名只能为字母或中文！");
			    }
			}

			String tSex = tLCInsured.getChildTextTrim(Sex);
			if ((null == tSex) || tSex.equals("")) {
				throw new BaseException("被保人性别不能为空！");
			}else if(!("0".equals(tSex) || "1".equals(tSex))){
				throw new BaseException("输入性别错误：请检查录入的被保人性别是否有误！");
			}

			String tIDType = tLCInsured.getChildTextTrim(IDType);
			if ((null == tIDType) || tIDType.equals("")) {
				throw new BaseException("被保人证件类型不能为空！");
			}else {
				// 当证件类型转换完后不为0,1,2,3,4,5将其转换为4.
				if(!("0".equals(tIDType) || "1".equals(tIDType) || "2".equals(tIDType) || "3".equals(tIDType) || "4".equals(tIDType) || "5".equals(tIDType)) ){
					System.out.println("tIDType="+tIDType);
					tLCInsured.getChild(IDType).setText("4");
				}
			}

			String tIDNo = tLCInsured.getChildTextTrim(IDNo);
			if ((null == tIDNo) || tIDNo.equals("")) {
				throw new BaseException("被保人证件号码不能为空！");
			}

			String tJobCode = tLCInsured.getChildTextTrim(JobCode);
			if ((null == tJobCode) || tJobCode.equals("")) {
				throw new BaseException("被保人职业不能为空！");
			}

			String tMailAddress = tLCInsured.getChildTextTrim(MailAddress);
			if ((null == tMailAddress) || tMailAddress.equals("")) {
				throw new BaseException("被保人地址不能为空！");
			}

			String tInsuredMobile = tLCInsured.getChildTextTrim(InsuredMobile);
			String tHomePhone = tLCInsured.getChildTextTrim(HomePhone);
			if ((null == tHomePhone) || tHomePhone.equals("")) {
				
				if((null == tInsuredMobile) || tInsuredMobile.equals("")) {
					throw new BaseException("被保人电话或手机至少一个不能为空！");
				}else{
					checkTelLength11(tInsuredMobile);
				}
			}else{
				checkTelLength11(tHomePhone);
				checkTelLength11(tInsuredMobile);
			}

			String tRelaToMain = tLCInsured.getChildTextTrim(RelaToMain);
			if ((null == tRelaToMain) || tRelaToMain.equals("")) {
				throw new BaseException("被保人与主被保人关系不能为空！");
			}

			String tRelaToAppnt = tLCInsured.getChildTextTrim(RelaToAppnt);
			if ((null == tRelaToAppnt) || tRelaToAppnt.equals("")) {
				throw new BaseException("被保人与投保人关系不能为空！");
			}
			
			String tBirthday = tLCInsured.getChildTextTrim(Birthday);
			if ((null == tBirthday) || tBirthday.equals("")) {
				throw new BaseException("被保人生日不能为空！");
			}
			
			if (tIDType.equals("0")) {	//身份证
				checkIDNo (tIDNo,tBirthday,tSex);
			}
			
			checkRisks(tLCInsured.getChild(Risks));
		}

		cLogger.debug("Out NewContCheck.checkLCInsureds()!");
	}

	/**
	 * 1. 险种代码、缴费方式、保费不能为空
	 * 2. 主险代码不能为空
	 */
	private static void checkRisks(Element pRisks) throws Exception {
		cLogger.debug("Into NewContCheck.checkRisks()...");

		if (null == pRisks) {
			throw new BaseException("Risks节点缺失！");
		}

		List mList = pRisks.getChildren(Risk);
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk  = (Element) mList.get(i);

			String tRiskCode = tRisk.getChildTextTrim(RiskCode);
			if ((null == tRiskCode) || tRiskCode.equals("")) {
				throw new BaseException("险种代码不能为空！");
			}

			String tMainRiskCode = tRisk.getChildTextTrim(MainRiskCode);
			if ((null == tMainRiskCode) || tMainRiskCode.equals("")) {
				throw new BaseException("主险种代码不能为空！");
			}

			String tPayIntv = tRisk.getChildTextTrim(PayIntv);
			if ((null == tPayIntv) || tPayIntv.equals("")) {
				throw new BaseException("缴费方式不能为空！");
			}

			String tPrem = tRisk.getChildTextTrim(Prem);
			if(!"332401".equals(tRiskCode)){
				if ((null == tPrem) || tPrem.equals("")) {
					throw new BaseException("保险费不能为空！");
				}
			}
			
			//红利领取方式校验 20120409( 如果是分红险，才校验红利领取方式)
			cLogger.debug("%%%%%%%%%=" + mBankCode);
		    if (("730101".equals(tRiskCode))) {
				String tBonusGetMode = tRisk.getChildTextTrim("BonusGetMode");
				if("".equals(tBonusGetMode) || tBonusGetMode == null){
					throw new BaseException("红利领取方式不能为空！");
				}
			}
//			String tMult = tRisk.getChildTextTrim(Mult);   
//			if ((null == tMult) || tMult.equals("")) {
//				throw new BaseException("投保份数不能为空！");
//			}

			checkLCBnfs(tRisk.getChild(LCBnfs));
		}

		cLogger.debug("Out NewContCheck.checkRisks()!");
	}

	/**
	 * 1. 受益人姓名、性别不能为空
	 * 3. 受益人受益级别、收益比例不能为空
	 * 4. 与被保人关系不能为空
	 * 5. 受益人类别不能为空
	 */
	private static void checkLCBnfs(Element pLCBnfs) throws Exception {
		cLogger.debug("Into NewContCheck.checkLCBnfs()...");

		if (null == pLCBnfs) {
			throw new BaseException("LCBnfs节点缺失！");
		}

		List mList = pLCBnfs.getChildren(LCBnf);
		HashMap mHashMap = new HashMap();
		for (int i = 0; i < mList.size(); i++) {
			Element tLCBnf  = (Element) mList.get(i);

			String tName = tLCBnf.getChildTextTrim(Name);
			if ((null == tName) || tName.equals("")) {
				throw new BaseException("受益人姓名不能为空！");
			}else{
			    if(!Pattern.compile(regex).matcher(tName).matches()){
			    	throw new BaseException("受益人姓名只能为字母或中文！");
			    }
			}

//			picch客户方要求不校验受益人性别
			String tSex = tLCBnf.getChildTextTrim(Sex);
//			if ((null == tSex) || tSex.equals("")) {
//			throw new BaseException("受益人性别不能为空！");
//			}
			if(!"".equals(tSex) && tSex != null){
				if(!("1".equals(tSex) || "0".equals(tSex))){
					throw new BaseException("输入性别错误：请检查录入的受益人性别是否有误！");
				}
			}
			
			String tBnfGrade = tLCBnf.getChildTextTrim(BnfGrade);
			if ((null == tBnfGrade) || tBnfGrade.equals("")) {
				throw new BaseException("受益人级别不能为空！");
			}

			String tBnfLot = tLCBnf.getChildTextTrim(BnfLot);
			if ((null == tBnfLot) || tBnfLot.equals("")) {
				throw new BaseException("受益比例不能为空！");
			}
			Integer tBnfLotInt = new Integer(tBnfLot);

			String tRelationToInsured = tLCBnf.getChildTextTrim(RelationToInsured);
			if ((null == tRelationToInsured) || tRelationToInsured.equals("")) {
				throw new BaseException("受益人与被保人关系不能为空！");
			}

			String tBnfType = tLCBnf.getChildTextTrim(BnfType);
			if ((null == tBnfType) || tBnfType.equals("")) {
				throw new BaseException("受益人类别不能为空！");
			}

			if (mHashMap.containsKey(tBnfGrade)) {	//为下面校验“同一顺序的受益人份额”准备数据
				Integer ttOldBnfLot = (Integer) mHashMap.get(tBnfGrade);

				tBnfLotInt = new Integer(
						tBnfLotInt.intValue() + ttOldBnfLot.intValue());
			}
			mHashMap.put(tBnfGrade, tBnfLotInt);
		}

		//校验同一顺序的受益人份额是否为100%
		Iterator mIterator = mHashMap.values().iterator();
		for (;mIterator.hasNext();) {
			Integer tBnfLot = (Integer)mIterator.next();
			if (100 != tBnfLot.intValue()) {
				throw new BaseException("同一顺序的受益人份额应为100%！");
			}
		}

		cLogger.debug("Out NewContCheck.checkLCBnfs()!");
	}
	
	// 单证校验（农、交、中行签单；建、工、邮储试算时不能让YBT开头的单证通过）
	public static void proposalNoCheck (String mProposalNo) throws Exception {
		
		if(mProposalNo.startsWith("YBT")){
			throw new BaseException("保单合同印刷号未下发，或状态无效！");
		}
	}
	
	//校验当电话号码为1开头时，长度必须为11位
	public static void checkTelLength11(String mTel) throws BaseException{
		if(mTel.length() < 7){
			throw new BaseException("客户提供的电话号码有误，请确认！");
		}
		if(mTel.startsWith("1") && mTel.length() != 11){
			throw new BaseException("客户提供的电话号码有误，请确认！");
		}
		 
	}

	public static void main(String args[]) throws Exception {
		System.out.println("程序开始…");

		String mInFile = "C:/Documents and Settings/Administrator/桌面/abc_9900042100000000017_01_161625.xml";

		Document mInXmlDoc = JdomUtil.build(
				new FileInputStream(mInFile));

		NewContCheck.check(mInXmlDoc);

		System.out.println("成功结束！");
	}
}
