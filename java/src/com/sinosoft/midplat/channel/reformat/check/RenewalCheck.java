package com.sinosoft.midplat.channel.reformat.check;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.XmlTag;

public class RenewalCheck implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(RenewalCheck.class);
	
	public static void check(Document pStdXml) throws Exception{
    	cLogger.info("Into RenewalCheck.check()!");
    	Element mBaseInfo = pStdXml.getRootElement().getChild("BaseInfo");
    	
    	String mBankCode = mBaseInfo.getChildTextTrim(BankCode);
		if ((null == mBankCode) || mBankCode.equals("")) {
			throw new BaseException("银行代码不能为空！");
		}

		String mZoneNo = mBaseInfo.getChildTextTrim(ZoneNo);
		if ((null == mZoneNo) || mZoneNo.equals("")) {
			throw new BaseException("地区代码不能为空！");
		}

		String mBrNo = mBaseInfo.getChildTextTrim("BrNo");
		if ((null == mBrNo) || mBrNo.equals("")) {
			throw new BaseException("网点代码不能为空！");
		}

		String mTransrNo = mBaseInfo.getChildTextTrim("TransrNo");
		if ((null == mTransrNo) || mTransrNo.equals("")) {
			throw new BaseException("交易流水号不能为空！");
		}

		String mFunctionFlag = mBaseInfo.getChildTextTrim("FunctionFlag");
		if ((null == mFunctionFlag) || mFunctionFlag.equals("")) {
			throw new BaseException("交易代码不能为空！");
		}
    	
    	cLogger.info("Out RenewalCheck.check()!");
    }
	
}
