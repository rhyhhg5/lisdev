package com.sinosoft.midplat.channel.reformat.abc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class NewCont extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into NewCont.getStdXml()...");
		
		Document mStdXml = pNoStdXml;
		
		String mPath = "/TranData/LCCont/LCBnfs";
		Element mLCBnfs = (Element) XPath.selectSingleNode(mStdXml, mPath);
//		mLCBnfs = (Element) mLCBnfs.detach();
		List mLCBnfList = mLCBnfs.getChildren(LCBnf);
		int mSize = mLCBnfList.size();
		for (int i = 0; i < mSize; i++) {
			Element tLCBnfEle = (Element) mLCBnfList.get(i);
			
			/**
			 * 有些银行会传一些空的受益人节点，如果受益人姓名为空(null或"")，则认为该受益人无效，并删除该节点。
			 */
			String tNameStr = tLCBnfEle.getChildText(Name);
			if (null==tNameStr || "".equals(tNameStr)) {
				tLCBnfEle.detach();
			} else {
				tLCBnfEle.getChild(BnfType).setText("1");
				
				Element tRelaEle = tLCBnfEle.getChild(RelationToInsured);
				tRelaEle.setText(
						Trial.getStdRelation(tRelaEle.getText()));
			}
		}
		
		//如果主险代码是 230701 时,组织一个附加险 331401
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(mStdXml, mPath);
		List mRisklist = mRisks.getChildren(Risk);
		for(int a=0;a<mRisklist.size();a++){
			//红利领取方式转换
//			((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("2");
			if(((Element)mRisklist.get(a)).getChildText("BonusGetMode").equals("1")){
				((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("2");
			}
			else{
				((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("1");
			}
			 mPath = "/TranData/LCCont/LCBnfs";
				Element mLCBnfsTemp = (Element) XPath.selectSingleNode(mStdXml, mPath);
				mLCBnfsTemp = (Element) mLCBnfsTemp.clone();
				Element mRisk = (Element)mRisklist.get(a);
				mRisk.addContent(mLCBnfsTemp);
		}
		//转换农行投保人与被保人的关系 为 核心系统被保人与投保人的关系
		mPath = "/TranData/LCCont/LCAppnt";
		Element mLCAppntEle = (Element) XPath.selectSingleNode(mStdXml, mPath);
		String mRelaToAppntStr = Trial.getRelaToAppnt(
				mLCAppntEle.getChildText("RelaToInsured"), 
				mLCAppntEle.getChildText(AppntSex));
		mLCAppntEle.getChild("RelaToInsured").setText(null);
		mRisks.getParentElement().getChild(
				RelaToAppnt).setText(mRelaToAppntStr);
		
		Element mRelaToMain = mRisks.getParentElement().getChild(RelaToMain);
		mRelaToMain.setText("00");
//		mPath = "/TranData/LCCont/LCBnfs";
//		mLCBnfs = (Element) XPath.selectSingleNode(mStdXml, mPath);
//		mLCBnfs = (Element) mLCBnfs.detach();
		Element mRisk = mRisks.getChild(Risk);
		
//		mRisk.addContent(mLCBnfs);
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		//如果主险代码是 240501 时.组织一个附件险 340201
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
			
			//安心宝农行传过来的保险期间为0,需要转换为106
			mRisk.getChild(InsuYear).setText("106");
		}

		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);
		
		cLogger.info("Out NewCont.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into NewCont.getNoStdXml()...");
		
		//判断是否是报错报文
		String mPath = "/TranData/RetData/Flag";
		Element mflag = (Element) XPath.selectSingleNode(pStdXml, mPath);
		if(mflag.getText().equals("0")) {
			return pStdXml;
		}
		
		/**
		 * modified by wangxt in 2009-7-3
		 * 农行要求返回保单到期日
		 */
		mPath = "/TranData/LCCont";
		Element mLCCont = (Element) XPath.selectSingleNode(pStdXml, mPath);
		
		/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
		Element mContEndDate = mLCCont.getChild("ContEndDate");
		mContEndDate.setText(
				DateUtil.formatTrans(mContEndDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
		
		/*Start-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
		Element mCValiDate = mLCCont.getChild(CValiDate);
		mCValiDate.setText(
				DateUtil.formatTrans(mCValiDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
		
		mPath = "/TranData/LCCont/SignDate";
		Element mSignDate = mLCCont.getChild("SignDate");
		cLogger.debug("SignDate = " + mSignDate.getText());
		if (!mSignDate.getText().equals("")) {	//试算时并未签单，SignDate为空
			mSignDate.setText(
					DateUtil.formatTrans(mSignDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
		}
		
		mPath = "/TranData/LCCont/LCAppnt/AppntBirthday";
		Element mAppntBirthday = mLCCont.getChild(LCAppnt).getChild(AppntBirthday);
		mAppntBirthday.setText(
				DateUtil.formatTrans(mAppntBirthday.getText(), "yyyy-MM-dd", "yyyyMMdd"));

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Birthday";
		List mList = XPath.selectNodes(pStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tBirthday = (Element) mList.get(i);
			tBirthday.setText(
					DateUtil.formatTrans(tBirthday.getText(), "yyyy-MM-dd", "yyyyMMdd"));
		}

		/**
		 * add by wangxt in 2009-3-25 begin
		 */
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk";
		mList = XPath.selectNodes(pStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tRisk = (Element) mList.get(i);
 			
 			String tPayIntv = tRisk.getChildText(PayIntv);
 			//0代表趸交
 			if (tPayIntv.equals("0")) {
 				tRisk.getChild(PayYears).setText("--");
 			}
 			
 			Element tCValiDate = tRisk.getChild(CValiDate);
 			tCValiDate.setText(
					DateUtil.formatTrans(tCValiDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
 			
 			Element tPayEndDate = tRisk.getChild(PayEndDate);
 			tPayEndDate.setText(
 					DateUtil.formatTrans(tPayEndDate.getText(), "yyyy-MM-dd", "yyyyMMdd"));
 			
 			String tMainRiskCodeStr = tRisk.getChildText(MainRiskCode);
 			Element tLCContPrem = 
					tRisk.getParentElement().getParentElement().getParentElement().getParentElement().getChild(Prem);
 			if (tMainRiskCodeStr.equals("331201")	//健康人生个人护理保险(万能型，B款)
 					|| tMainRiskCodeStr.equals("331301")	//健康人生个人护理保险(万能型，C款)
 					|| tMainRiskCodeStr.equals("331601")	//健康人生个人护理保险(万能型，D款)
 					|| tMainRiskCodeStr.equals("230701")    //健康专家个人防癌疾病保险
 					|| tMainRiskCodeStr.equals("240501")    //安心宝个人终身疾病重大保险
 					|| tMainRiskCodeStr.equals("331701")    //金利宝个人护理保险(万能型)
 					|| tMainRiskCodeStr.equals("331901")) {	//康利相伴个人护理保险（万能型）
 				tRisk.getChild(Prem).setText(tLCContPrem.getText());	//银行方1个险种对应系统内两险种，银行要求返回总保费
 			}
 			
 			if (tMainRiskCodeStr.equals("331301")) {	//picch万能C有个追加保费，需要累加上去
 				double ttTotalPrem = 
 					Double.parseDouble(tLCContPrem.getText())	+ Double.parseDouble(tRisk.getChildText(SupplementaryPrem));
 				tLCContPrem.setText(new DecimalFormat("0.00").format(ttTotalPrem));
 				tRisk.getChild(Amnt).setText("详见条款规定");
 			}

 			
 			//趸缴产品的 保额 栏内容：原“--”更改为：“详见条款规定”
 			String mPayIntv =  tRisk.getChildText(PayIntv);
 			//康利人生两全保险（分红型）显示保额
 			if(!"730101".equals(tMainRiskCodeStr)&&mPayIntv.equals("0")){
 				Element mAmnt = tRisk.getChild(Amnt);
 				mAmnt.setText("详见条款规定");
 			}
 			if(tMainRiskCodeStr.equals("240501")){//安心宝
 				tRisk.getChild(RiskName).setText("安心宝个人终身重大疾病保障计划");
 				tRisk.getChild(InsuYear).setText("0");
 				mLCCont.getChild(SpecContent).setText("安心宝个人终身重大疾病保障计划是由《安心宝个人终身重大疾病保险》、《附加安心宝个人护理保险》组成。");
 			}
 			if(tMainRiskCodeStr.equals("331701")){//金利宝
 				tRisk.getChild(RiskName).setText("金利宝个人护理增值计划（万能型）");
 				mLCCont.getChild(SpecContent).setText("金利宝个人护理增值计划（万能型）是由《金利宝个人护理保险（万能型）》、《附加金利宝个人意外伤害保险》组成。");
 				tRisk.getChild(Amnt).setText("详见条款规定");
 			}
		}
		List mList1 = mLCCont.getChild(LCInsureds).getChild(LCInsured).getChild(Risks).getChildren(Risk);
		for (int k = 0; k < mList1.size(); k++) {			
			
			//在安心宝的调试过程中发现,返回给农行的现金价值链过长,导致农行无法解析.经协商,农行打印的现金价值链20年以后,每五年打印一次.			
			Element mCashValues = ((Element)mList1.get(k)).getChild(CashValues);			
			mList = mCashValues.getChildren();
			Iterator i = mList.iterator();
			cLogger.debug("mList = mCashValues.getChildren()"+mList.size());
			
			int mOldCount = mList.size() -1;//要求返回的现金价值链中最后一条现价必须返回
			int flag = 0;//第一条是现金价值链的统计数目.不对第一个子节点进行操作.
			while (i.hasNext()) {

				if (flag != 0) {

					Element mCashValue = (Element) i.next();
					int j = 0;//判断end值

					String m = mCashValue.getChildText("End");
					j = Integer.parseInt(m);
					if ((j > 20) && (j % 5 != 0)&&(j<mOldCount)) {
						i.remove();
						cLogger.debug("i.remove(),i=" + i);
					}

				} else {
					i.next();//如果是第一个自己点,不做处理,转到下一条子节点.
				}

				flag++;
			}
			int count = mList.size() - 1;//操作后剩余的价值链子节点数量
			mCashValues.getChild("CashValueCount").setText(String.valueOf(count));
			
			//给农行现金价值链添加备注信息 
			Element RemarkDetails0 = new Element("RemarkDetails");
			Element RemarkDetails1 = new Element("RemarkDetails");
			Element RemarkDetails2 = new Element("RemarkDetails");
			Element RemarkDetails3 = new Element("RemarkDetails");
			Element RemarkDetails4 = new Element("RemarkDetails");
			
			Element RemarkDetail0 = new Element("RemarkDetail");
			RemarkDetail0.setText("备注：");
			RemarkDetails0.addContent(RemarkDetail0);
			
			Element RemarkDetail1 = new Element("RemarkDetail");
			RemarkDetail1.setText("1.此现金价值表是本公司按照中国保险监督管理委员会的相关规定计算确定的，其他未列明的保单");
			RemarkDetails1.addContent(RemarkDetail1);
			
			Element RemarkDetail2 = new Element("RemarkDetail");
			RemarkDetail2.setText("年度末的现金价值可向我公司客户服务热线95591或4006695518咨询。");
			RemarkDetails2.addContent(RemarkDetail2);
			
			Element RemarkDetail3 = new Element("RemarkDetail");
			RemarkDetail3.setText("2.保单年度末解除合同时，现金价值根据表中对应保单年度末的现金价值计算；其他时间解除合同");
			RemarkDetails3.addContent(RemarkDetail3);
			
			Element RemarkDetail4 = new Element("RemarkDetail");
			RemarkDetail4.setText("时，以本现金价值表为基础，按照我公司规定的计算方法确定。");
			RemarkDetails4.addContent(RemarkDetail4);
			
			mCashValues.addContent(RemarkDetails0);
			mCashValues.addContent(RemarkDetails1);
	     	mCashValues.addContent(RemarkDetails2);
	     	mCashValues.addContent(RemarkDetails3);
	     	mCashValues.addContent(RemarkDetails4);
	     	
		}
		for (int k = 0; k < mList1.size(); k++) {		
		Element tRisk  = (Element) mList1.get(k);
		String tMainRiskCode = tRisk.getChildText(MainRiskCode);
		String tRiskCode = tRisk.getChildText(RiskCode);
		if(tMainRiskCode.equals(tRiskCode)){
			Element tRiskCopy = (Element)tRisk.clone();
			Element tRisk1  = (Element) mList1.get(0);
			Element tRiskCopy1 = (Element)tRisk1.clone();
			mList1.remove(0);
			mList1.add(0, tRiskCopy);
			mList1.remove(k);
			mList1.add(k, tRiskCopy1);				
		}
		}
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(pStdXml, mPath);
		Element mRiskCount = new Element(RiskCount);
		mRiskCount.setText(String.valueOf(mList1.size()));
		mRisks.addContent(mRiskCount);
//		
		Element mLCBnfs = mRisks.getChild(Risk).getChild(LCBnfs);
		Element mLCBnfCount = new Element(LCBnfCount);
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf";
		mList = XPath.selectNodes(pStdXml, mPath);
		mLCBnfCount.setText(String.valueOf(mList.size()));
		mLCBnfs.addContent(mLCBnfCount);
//		
		Element mLCBnfsNode = (Element)mLCBnfs.detach();
		mLCCont.addContent(mLCBnfsNode);
//		/**
//		 * add by wangxt in 2009-3-25 end
//		 */
//
		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/LCBnfs/LCBnf/Birthday";
		mList = XPath.selectNodes(pStdXml, mPath);
		cLogger.debug("XPath=" + mPath + "; size=" + mList.size());
		for (int i = 0; i < mList.size(); i++) {
			Element tBirthday = (Element) mList.get(i);
			tBirthday.setText(
					DateUtil.formatTrans(tBirthday.getText(), "yyyy-MM-dd", "yyyyMMdd"));
		}
//		/*End-日期格式转换(yyyy-MM-dd --> yyyyMMdd)*/
//		
//		
//		//在安心宝的调试过程中发现,返回给农行的现金价值链过长,导致农行无法解析.经协商,农行打印的现金价值链20年以后,每五年打印一次.
//		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks/Risk/CashValues";
//		//目前农行只返回主险,以后若要返回附加险,需要修改.
//		Element mCashValues = (Element) XPath.selectSingleNode(pStdXml, mPath);
//		cLogger.debug("XPath=" + mPath);
//		mList = mCashValues.getChildren();
//		Iterator i = mList.iterator();
//		cLogger.debug("mList = mCashValues.getChildren()"+mList.size());
//		
//		int mOldCount = mList.size() -1;//要求返回的现金价值链中最后一条现价必须返回
//		int flag = 0;//第一条是现金价值链的统计数目.不对第一个子节点进行操作.
//		while (i.hasNext()) {
//
//			if (flag != 0) {
//
//				Element mCashValue = (Element) i.next();
//				int j = 0;//判断end值
//
//				String m = mCashValue.getChildText("End");
//				j = Integer.parseInt(m);
//				if ((j > 20) && (j % 5 != 0)&&(j<mOldCount)) {
//					i.remove();
//					cLogger.debug("i.remove(),i=" + i);
//				}
//
//			} else {
//				i.next();//如果是第一个自己点,不做处理,转到下一条子节点.
//			}
//
//			flag++;
//		}
//		int count = mList.size() - 1;//操作后剩余的价值链子节点数量
//		mCashValues.getChild("CashValueCount").setText(String.valueOf(count));
//		
//		//给农行现金价值链添加备注信息 
//		Element RemarkDetails0 = new Element("RemarkDetails");
//		Element RemarkDetails1 = new Element("RemarkDetails");
//		Element RemarkDetails2 = new Element("RemarkDetails");
//		Element RemarkDetails3 = new Element("RemarkDetails");
//		Element RemarkDetails4 = new Element("RemarkDetails");
//		
//		Element RemarkDetail0 = new Element("RemarkDetail");
//		RemarkDetail0.setText("备注：");
//		RemarkDetails0.addContent(RemarkDetail0);
//		
//		Element RemarkDetail1 = new Element("RemarkDetail");
//		RemarkDetail1.setText("1.此现金价值表是本公司按照中国保险监督管理委员会的相关规定计算确定的，其他未列明的保单");
//		RemarkDetails1.addContent(RemarkDetail1);
//		
//		Element RemarkDetail2 = new Element("RemarkDetail");
//		RemarkDetail2.setText("年度末的现金价值可向我公司客户服务热线95591或4006695518咨询。");
//		RemarkDetails2.addContent(RemarkDetail2);
//		
//		Element RemarkDetail3 = new Element("RemarkDetail");
//		RemarkDetail3.setText("2.保单年度末解除合同时，现金价值根据表中对应保单年度末的现金价值计算；其他时间解除合同");
//		RemarkDetails3.addContent(RemarkDetail3);
//		
//		Element RemarkDetail4 = new Element("RemarkDetail");
//		RemarkDetail4.setText("时，以本现金价值表为基础，按照我公司规定的计算方法确定。");
//		RemarkDetails4.addContent(RemarkDetail4);
//		
//		mCashValues.addContent(RemarkDetails0);
//		mCashValues.addContent(RemarkDetails1);
//     	mCashValues.addContent(RemarkDetails2);
//     	mCashValues.addContent(RemarkDetails3);
//     	mCashValues.addContent(RemarkDetails4);
		cLogger.info("Out NewCont.getNoStdXml()!");
		return pStdXml;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFilePath = "D:/innostd.xml";
		String mOutFilePath = "D:/instd.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new NewCont().getNoStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
}