package com.sinosoft.midplat.channel.reformat.abc;

import org.jdom.Document;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class AutoWriteOff extends BaseFormat {

	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into AutoWriteOff.getStdXml()...");

		cLogger.info("Out AutoWriteOff.getStdXml()!");
		return pNoStdXml;
	}

	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into AutoWriteOff.getNoStdXml()...");

		cLogger.info("Out AutoWriteOff.getNoStdXml()!");
		return pStdXml;
	}
}
