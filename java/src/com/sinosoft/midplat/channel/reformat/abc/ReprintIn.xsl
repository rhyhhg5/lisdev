<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<!--重打请求-->
		<TranData>

			<BaseInfo>
				<!--银行交易日期-->
				<BankDate>
					<xsl:value-of select="Req/BankDate"/>
				</BankDate>
				<!--银行代码(cd05)-->
				<BankCode><xsl:value-of select="Req/BankCode" /></BankCode>
				<!--地区代码 -->
				<ZoneNo>
					<xsl:value-of select="Req/ZoneNo"/>
				</ZoneNo>
				<!--网点代码-->
				<BrNo>
					<xsl:value-of select="Req/BrNo"/>
				</BrNo>
				<!--柜员代码-->
				<TellerNo>	
					<xsl:value-of select="Req/TellerNo"/>
				</TellerNo>
				<!--交易流水号-->
				<TransrNo>
					<xsl:value-of select="Req/dealNo"/>
				</TransrNo>
				<!--交易渠道代号-->
				<SaleChannel/>
				<FunctionFlag><xsl:value-of select="Req/FunctionFlag" /></FunctionFlag>
			 	<!-- 保险公司代码 -->
				<InsuID>
					<xsl:value-of select="Req/InsuID"/>
				</InsuID>
 			</BaseInfo>     
			
			<LCCont>
				
				<PrtNo><xsl:value-of select="Req/Base/PrtNo" /></PrtNo>
				
				<ContNo><xsl:value-of select="Req/Base/OldContNo" /></ContNo>
				
				<ProposalContNo></ProposalContNo>				
				<TempFeeNo>
					
				</TempFeeNo>
				<Password></Password>
			</LCCont>
		</TranData>
	</xsl:template>
</xsl:stylesheet>
