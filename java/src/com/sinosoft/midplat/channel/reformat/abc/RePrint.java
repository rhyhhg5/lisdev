package com.sinosoft.midplat.channel.reformat.abc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.jdom.Document;
import org.jdom.transform.XSLTransformer;

import com.sinosoft.midplat.channel.reformat.BaseFormat;

import com.sinosoft.midplat.channel.util.JdomUtil;

public class RePrint extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into RePrint.getNoStdXml()...");
		
		InputStream mSheetIs = getClass().getResourceAsStream("ReprintIn.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document mStdXml = new XSLTransformer(mSheetIsr).transform(pNoStdXml);
		mSheetIsr.close();
		
//		Document mNoStdXml = new NewCont().getNoStdXml(mStdXml);
		
		cLogger.info("Out RePrint.getNoStdXml()!");
		return mStdXml;
	}	
	
	public Document getNoStdXml(Document pStdXml)throws Exception {
		cLogger.info("Into RePrint.getNoStd2StdXml(dangerous)...");
		
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		InputStream mSheetIs = getClass().getResourceAsStream("ReprintOut.xsl");
		InputStreamReader mSheetIsr = new InputStreamReader(mSheetIs, "GBK");
		Document xNoStdXml = new XSLTransformer(mSheetIsr).transform(mNoStdXml);
		mSheetIsr.close();
		
		cLogger.info("Out RePrint.getNoStd2Std()...");
		return xNoStdXml;
	}
	
	public static void main(String[] args) throws Exception {
		
		System.out.println("程序开始…");
		
//		String mInFilePath = "D:\\picch\\pingan\\ABC/abc_RePrint.xml";
		
		String mInFilePath = "D:\\picch\\pingan\\ABC\\abc_outSta_20111107.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Request_ST.xml";

//		String mInFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Response.xml";
//		String mOutFilePath = "E:/Test-haoqt/picch/ccb/testXml/rePrint/SPE002_Response_NST.xml";
		
		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new RePrint().getNoStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
//		OutputStream mOs = new FileOutputStream(mOutFilePath);
//		JdomUtil.output(mOutXmlDoc, mOs);
//		mOs.flush();
//		mOs.close();
		
		System.out.println("成功结束！");
	
		
	}
}