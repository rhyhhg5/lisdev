package com.sinosoft.midplat.channel.reformat.abc;

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.IOTrans;

public class BatResponse extends BaseFormat {
	private String cBatInMsg = null;
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into BatResponse.getStdXml()...");
		
		cBatInMsg = pNoStdXml.getRootElement().getTextTrim();
		String[] mSubMsgs = cBatInMsg.split("\\|");
		
		Element mBankCode = new Element(BankCode);
		mBankCode.setText("04");
		
		Element mZoneNo = new Element(ZoneNo);
		mZoneNo.setText(mSubMsgs[3] + mSubMsgs[4]);
		
		Element mBrNo = new Element(BrNo);
		mBrNo.setText(mSubMsgs[5]);
		
		Element mTellerNo = new Element(TellerNo);
		mTellerNo.setText(mSubMsgs[6]);
		
		Element mTransrNo = new Element(TransrNo);
		mTransrNo.setText(mSubMsgs[7]);
		
		Element mFunctionFlag = new Element(FunctionFlag);
		if ("S".equals(mSubMsgs[10])) {	//代收返盘
			mFunctionFlag.setText("32");
		} else {	//代付返盘
			mFunctionFlag.setText("34");
		}
		
		Element mFileName = new Element("FileName");
		mFileName.setText(mSubMsgs[11]);
		
		Element mBaseInfo = new Element(BaseInfo);
		mBaseInfo.addContent(mBankCode);
		mBaseInfo.addContent(mZoneNo);
		mBaseInfo.addContent(mBrNo);
		mBaseInfo.addContent(mTellerNo);
		mBaseInfo.addContent(mTransrNo);
		mBaseInfo.addContent(mFunctionFlag);
		mBaseInfo.addContent(mFileName);
		
		//ftp下载返盘文件
		Element mBatData = new Element("BatData");
		String mBatDataStr = ftpDownload(mSubMsgs[11]);
		System.out.println(mBatDataStr);
		mBatData.setText(mBatDataStr);
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mBaseInfo);
		mTranData.addContent(mBatData);
		
		cLogger.info("Out BatResponse.getStdXml()!");
		return new Document(mTranData);
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into BatResponse.getNoStdXml()...");
		
		Element mTranData = pStdXml.getRootElement();
		Element mRetData = mTranData.getChild(RetData);
		
		String[] mSubMsgs = cBatInMsg.split("\\|");
		StringBuffer mOutMsg = new StringBuffer(mSubMsgs[0]).append('|')	//报文编号
			.append(mSubMsgs[1]).append('|')	//业务甲方
			.append(mSubMsgs[2]).append('|')	//业务乙方
			.append(mSubMsgs[3]).append('|')	//乙方分行代码
			.append(mSubMsgs[4]).append('|')	//乙方地区码
			.append(mSubMsgs[5]).append('|')	//乙方网点代码
			.append(mSubMsgs[8]).append('|')	//交易日期
			.append(mSubMsgs[9]).append('|')	//交易时间
			.append(mSubMsgs[10]).append('|');	//业务性质
		if ("0".equals(mRetData.getChildText(Flag))) {	//交易失败
			mOutMsg.append("1111").append('|');	//响应码
		} else {	//交易成功
			mOutMsg.append("0000").append('|');	//响应码
		}
		mOutMsg.append(mRetData.getChildText(Desc)).append('|')	//响应信息
			.append(mSubMsgs[11]).append('|')	//批量文件名
			.append("").append('|');	//备用
		
		//模拟生成一个xml：<TempTag>||批量数据|</TempTag>
		Element mTempTag = new Element("TempTag");
		mTempTag.setText(mOutMsg.toString());
		
		cLogger.info("Out BatResponse.getNoStdXml()!");
		return new Document(mTempTag);
	}
	
	private String ftpDownload(String pBatFileName) throws Exception {
		cLogger.debug("Into BatResponse.ftpDownload()...");
		
		ResourceBundle mPreConfig = ConfigFactory.getPreConfig();
		
		String mBatFtpIp = mPreConfig.getString("ABC.Bat.FtpIp");
		cLogger.debug("ABC.Bat.FtpIp = " + mBatFtpIp);
		if ((null == mBatFtpIp) || mBatFtpIp.equals("")) {
			throw new BaseException("未配置批量代收付ftp的ip！");
		}
		
		String mBatFtpPort = null;
		try {
			mBatFtpPort = mPreConfig.getString("ABC.Bat.FtpPort");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if ((null == mBatFtpPort) || mBatFtpPort.equals("")) {
			mBatFtpPort = "21";
		}
		cLogger.debug("ABC.Bat.FtpPort = " + mBatFtpPort);
		
		String mBatFtpPath = null;
		try {
			mBatFtpPath = mPreConfig.getString("ABC.Bat.FtpPath");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if ((null == mBatFtpPath) || "".equals(mBatFtpPath)) {
			mBatFtpPath = "./";
		}
		mBatFtpPath = mBatFtpPath.replace('\\', '/');
		if (!mBatFtpPath.endsWith("/")) {
			mBatFtpPath = mBatFtpPath + "/";
		}
		cLogger.debug("ABC.Bat.FtpPath = " + mBatFtpPath);
		
		String mBatFtpUser = mPreConfig.getString("ABC.Bat.FtpUser");
		if ((null == mBatFtpUser) || "".equals(mBatFtpUser)) {
			mBatFtpUser = " ";
		}
		cLogger.debug("ABC.Bat.FtpUser = " + mBatFtpUser);
		
		String mFtpPassword = mPreConfig.getString("ABC.Bat.FtpPassword");
		if ((null == mFtpPassword) || "".equals(mFtpPassword)) {
			mFtpPassword = " ";
		}
		cLogger.debug("ABC.Bat.FtpPassword = " + mFtpPassword);
		
		String mPathName = mBatFtpPath + pBatFileName;
		
		byte[] mBatDateBytes = null;
		
		//ftp到银行
		FTPClient mFTPClient = new FTPClient();
		mFTPClient.setDefaultPort(Integer.parseInt(mBatFtpPort));
		try {
			//建立ftp连接
			mFTPClient.connect(mBatFtpIp);
			int tReplyCode = mFTPClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(tReplyCode)) {
				throw new BaseException("ftp连接失败！" + mBatFtpIp + ": " + tReplyCode);
			}
			cLogger.info("ftp连接成功！" + mBatFtpIp);
			
			//登录
			if (!mFTPClient.login(mBatFtpUser, mFtpPassword)) {
				throw new BaseException("ftp登录失败！" + mBatFtpUser + ":" + mFtpPassword);
			}
			cLogger.info("ftp登录成功！");
			
			InputStream mRemoteIs = 
				mFTPClient.retrieveFileStream(mPathName);
			if (null == mRemoteIs) {
				throw new BaseException("未找到批量文件！" + (mPathName));
			}
			cLogger.info("ftp下载数据成功！");
			
			mBatDateBytes = 
				IOTrans.InputStreamToBytes(mRemoteIs);
			
			//退出登陆
			mFTPClient.logout();
			cLogger.info("ftp退出成功！");
		} catch (IOException ex) {
			throw new BaseException("ftp获取返盘数据失败！");
		} finally {
			if (mFTPClient.isConnected()) {
				try {
					mFTPClient.disconnect();
					cLogger.info("ftp连接断开！");
				} catch(IOException ex) {
					cLogger.warn("服务端连接已断开！", ex);
				}
			}
		}
		
		cLogger.debug("Out BatResponse.ftpDownload()!");
		return new String(mBatDateBytes, "GBK");
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		System.out.println(
				new BatResponse().ftpDownload("12345"));
		
		System.out.println("成功结束！");
	}
}
