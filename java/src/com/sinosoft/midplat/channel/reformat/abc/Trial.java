package com.sinosoft.midplat.channel.reformat.abc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.reformat.check.NewContCheck;
import com.sinosoft.midplat.channel.util.JdomUtil;

public class Trial extends BaseFormat {
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into Trial.getStdXml()...");
		
		Document mStdXml = pNoStdXml;
		
		String mPath = "/TranData/LCCont/LCBnfs";
		Element mLCBnfs = (Element) XPath.selectSingleNode(mStdXml, mPath);
//		mLCBnfs = (Element) mLCBnfs.detach();
		List mLCBnfList = mLCBnfs.getChildren(LCBnf);
		int mSize = mLCBnfList.size();
		for (int i = 0; i < mSize; i++) {
			Element tLCBnfEle = (Element) mLCBnfList.get(i);
			
			/**
			 * 有些银行会传一些空的受益人节点，如果受益人姓名为空(null或"")，则认为该受益人无效，并删除该节点。
			 */
			String tNameStr = tLCBnfEle.getChildText(Name);
			if (null==tNameStr || "".equals(tNameStr)) {
				tLCBnfEle.detach();
			} else {
				tLCBnfEle.getChild(BnfType).setText("1");
				
				Element tRelaEle = tLCBnfEle.getChild(RelationToInsured);
				tRelaEle.setText(
						getStdRelation(tRelaEle.getText()));
			}
		}
		
		//试算不传保单合同印刷号(ProposalContNo)，自动生成(ProposalContNo="YBT"+PrtNo)
		Element mLCCont = mStdXml.getRootElement().getChild(LCCont);
		String mPrtNoStr = mLCCont.getChildText(PrtNo);
		if ((null==mPrtNoStr) || mPrtNoStr.equals("")) {
			throw new BaseException("投保书印刷号不能为空!");
		}
		String mProposalContNoStr = "YBT" + mPrtNoStr;
		mLCCont.getChild(ProposalContNo).setText(mProposalContNoStr);

		mPath = "/TranData/LCCont/LCInsureds/LCInsured/Risks";
		Element mRisks = (Element) XPath.selectSingleNode(mStdXml, mPath);
		List mRisklist = mRisks.getChildren(Risk);
		for(int a=0;a<mRisklist.size();a++){
			//红利领取方式转换
//			((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("2");
			if(((Element)mRisklist.get(a)).getChildText("BonusGetMode").equals("1")){
				((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("2");
			}
			if(((Element)mRisklist.get(a)).getChildText("BonusGetMode").equals("2")){
				((Element)mRisklist.get(a)).getChild("BonusGetMode").setText("1");
			}
			 mPath = "/TranData/LCCont/LCBnfs";
			Element mLCBnfsTemp = (Element) XPath.selectSingleNode(mStdXml, mPath);
			mLCBnfsTemp = (Element) mLCBnfsTemp.clone();
			Element mRisk = (Element)mRisklist.get(a);
			mRisk.addContent(mLCBnfsTemp);
		}
		//转换农行投保人与被保人的关系 为 核心系统被保人与投保人的关系
		Element mLCAppntEle = mLCCont.getChild(LCAppnt);
		String mRelaToAppntStr = getRelaToAppnt(
				mLCAppntEle.getChildText("RelaToInsured"), 
				mLCAppntEle.getChildText(AppntSex));
		mLCAppntEle.getChild("RelaToInsured").setText(null);
		mRisks.getParentElement().getChild(
				RelaToAppnt).setText(mRelaToAppntStr);
		
		Element mRelaToMain = mRisks.getParentElement().getChild(RelaToMain);
		mRelaToMain.setText("00");
//		mPath = "/TranData/LCCont/LCBnfs";
//		mLCBnfs = (Element) XPath.selectSingleNode(mStdXml, mPath);
//		mLCBnfs = (Element) mLCBnfs.detach();
		Element mRisk = mRisks.getChild(Risk);
//		mRisk.addContent(mLCBnfs);
//		mRisk.getChild(Mult).setText("0.00");	//交行试算不传份数，这里置一个默认值(0.00)
		if("".equals(mRisk.getChildText(Mult))){
			mRisk.getChild(Mult).setText("0.00");//安心宝个人终身重大疾病保险 需要录入档次,其他不需录入的置为0.00
		}
		//如果主险代码是 230701 时,组织一个附加险 331401
		if (mRisk.getChildTextTrim(MainRiskCode).equals("230701")) {
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("331401");
			mRisks.addContent(tRisk);
		}
		//如果主险代码是 240501 时.组织一个附件险 340201
		if (mRisk.getChildTextTrim(MainRiskCode).equals("240501")) {//安心宝
			Element tRisk = (Element) mRisk.clone();
			tRisk.getChild(RiskCode).setText("340201");
			mRisks.addContent(tRisk);
			
			//安心宝银行传过来的保险期间是0,要转成106
			mRisk.getChild(InsuYear).setText("106");
		}
		
		//针对标准报文进行一些简单的校验，主要是空值校验
		NewContCheck.check(mStdXml);
		
		cLogger.info("Out Trial.getStdXml()!");
		return mStdXml;
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Trial.getNoStdXml()...");
		
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		
		cLogger.info("Out Trial.getNoStdXml()!");
		return mNoStdXml;
	}
	
	/**
	 * 获取被保人与投保人的关系，农行传递的是投保人与被保人的关系，需要做一个关系的反转。
	 */
	static String getRelaToAppnt(String pAbcRelation, String pSex) {
		if ("1".equals(pAbcRelation)) {	//本身
			return "00";	//本人
		} else if ("2".equals(pAbcRelation)) {	//丈夫
			return "01";	//妻子
		} else if ("3".equals(pAbcRelation)) {	//妻子
			return "02";	//丈夫
		} else if ("4".equals(pAbcRelation)) {	//父亲
			return "03";	//儿女
		} else if ("5".equals(pAbcRelation)) {	//母亲
			return "03";	//儿女
		} else if ("6".equals(pAbcRelation) || "7".equals(pAbcRelation)) {	//6-儿子/7-女儿
			if ("0".equals(pSex)) {
				return "04";	//父亲
			} else {
				return "05";	//母亲
			}
		} else if ("8".equals(pAbcRelation)) {	//祖父
			return "21";	//孙子/女
		} else if ("9".equals(pAbcRelation)) {	//祖母
			return "21";	//孙子/女
		} else if ("10".equals(pAbcRelation)) {	//孙子
			return "19";	//爷爷
		} else if ("11".equals(pAbcRelation)) {	//孙女
			return "20";	//奶奶
		} else if ("12".equals(pAbcRelation)) {	//外祖父
			return "24";	//外孙子/女
		} else if ("13".equals(pAbcRelation)) {	//外祖母
			return "24";	//外孙子/女
		} else if ("14".equals(pAbcRelation) || "15".equals(pAbcRelation)) {	//14-外孙/15-外孙女
			if ("0".equals(pSex)) {
				return "22";	//外公
			} else {
				return "23";	//外婆
			}
		} else if ("16".equals(pAbcRelation)) {	//哥哥
			return "12";	//兄弟姐妹
		} else if ("17".equals(pAbcRelation)) {	//姐姐
			return "12";	//兄弟姐妹
		} else if ("18".equals(pAbcRelation)) {	//弟弟
			return "12";	//兄弟姐妹
		} else if ("19".equals(pAbcRelation)) {	//妹妹
			return "12";	//兄弟姐妹
		} else if ("20".equals(pAbcRelation)) {	//公公
			return "11";	//媳妇
		} else if ("21".equals(pAbcRelation)) {	//婆婆
			return "11";	//媳妇
		} else if ("22".equals(pAbcRelation)) {	//儿媳
			if ("0".equals(pSex)) {
				return "09";	//公公
			} else {
				return "25";	//其他亲戚
			}
		} else if ("23".equals(pAbcRelation)) {	//岳父
			return "08";	//女婿
		} else if ("24".equals(pAbcRelation)) {	//岳母
			return "08";	//女婿
		} else if ("25".equals(pAbcRelation)) {	//女婿
			if ("0".equals(pSex)) {
				return "06";	//岳父
			} else {
				return "07";	//岳母
			}
		} else if ("26".equals(pAbcRelation)) {	//其他亲戚
			return "25";	//其他亲戚
		} else if ("27".equals(pAbcRelation)) {	//同事
			return "26";	//同事
		} else if ("28".equals(pAbcRelation)) {	//朋友
			return "27";	//朋友
		} else if ("29".equals(pAbcRelation)) {	//雇主
			return "29";	//雇员
		} else {	//其他
			return "30";	//其他
		}
	}
	
	/**
	 * 获取受益人与被保人的关系。
	 */
	static String getStdRelation(String pAbcRelation) {
		if ("1".equals(pAbcRelation)) {	//本身
			return "00";	//本人
		} else if ("2".equals(pAbcRelation)) {	//丈夫
			return "02";	//丈夫
		} else if ("3".equals(pAbcRelation)) {	//妻子
			return "01";	//妻子
		} else if ("4".equals(pAbcRelation)) {	//父亲
			return "04";	//父亲
		} else if ("5".equals(pAbcRelation)) {	//母亲
			return "05";	//母亲
		} else if ("6".equals(pAbcRelation)) {	//儿子
			return "03";	//儿女
		} else if ("7".equals(pAbcRelation)) {	//女儿
			return "03";	//儿女
		} else if ("8".equals(pAbcRelation)) {	//祖父
			return "19";	//爷爷
		} else if ("9".equals(pAbcRelation)) {	//祖母
			return "20";	//奶奶
		} else if ("10".equals(pAbcRelation)) {	//孙子
			return "21";	//孙子/女
		} else if ("11".equals(pAbcRelation)) {	//孙女
			return "21";	//孙子/女
		} else if ("12".equals(pAbcRelation)) {	//外祖父
			return "22";	//外公
		} else if ("13".equals(pAbcRelation)) {	//外祖母
			return "23";	//外婆
		} else if ("14".equals(pAbcRelation)) {	//外孙
			return "24";	//外孙子/女
		} else if ("15".equals(pAbcRelation)) {	//外孙女
			return "24";	//外孙子/女
		} else if ("16".equals(pAbcRelation)) {	//哥哥
			return "12";	//兄弟姐妹
		} else if ("17".equals(pAbcRelation)) {	//姐姐
			return "12";	//兄弟姐妹
		} else if ("18".equals(pAbcRelation)) {	//弟弟
			return "12";	//兄弟姐妹
		} else if ("19".equals(pAbcRelation)) {	//妹妹
			return "12";	//兄弟姐妹
		} else if ("20".equals(pAbcRelation)) {	//公公
			return "09";	//公公
		} else if ("21".equals(pAbcRelation)) {	//婆婆
			return "25";	//其他亲戚
		} else if ("22".equals(pAbcRelation)) {	//儿媳
			return "11";	//媳妇
		} else if ("23".equals(pAbcRelation)) {	//岳父
			return "06";	//岳父
		} else if ("24".equals(pAbcRelation)) {	//岳母
			return "07";	//岳母
		} else if ("25".equals(pAbcRelation)) {	//女婿
			return "08";	//女婿
		} else if ("26".equals(pAbcRelation)) {	//其他亲戚
			return "25";	//其他亲戚
		} else if ("27".equals(pAbcRelation)) {	//同事
			return "26";	//同事
		} else if ("28".equals(pAbcRelation)) {	//朋友
			return "27";	//朋友
		} else if ("29".equals(pAbcRelation)) {	//雇主
			return "28";	//雇主
		} else {	//其他
			return "30";	//其他
		}
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		String mInFilePath = "D:/innostd.xml";
		String mOutFilePath = "D:/instd.xml";

		InputStream mIs = new FileInputStream(mInFilePath);
		Document mInXmlDoc = JdomUtil.build(mIs);
		mIs.close();
		
		Document mOutXmlDoc = new Trial().getStdXml(mInXmlDoc);

		JdomUtil.print(mOutXmlDoc);
		
		OutputStream mOs = new FileOutputStream(mOutFilePath);
		JdomUtil.output(mOutXmlDoc, mOs);
		mOs.flush();
		mOs.close();
		
		System.out.println("成功结束！");
	}
	
}
