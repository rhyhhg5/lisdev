package com.sinosoft.midplat.channel.reformat.abc;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;




public class Query extends BaseFormat {
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Query.getNoStdXml()...");

		//查询和新单返回报文基本完全一样，所以直接调用
		Document mNoStdXml = new NewCont().getNoStdXml(pStdXml);
		
		cLogger.info("Out Query.getNoStdXml()!");
		return mNoStdXml;
	}	
	public Document getStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into Query.getStdXml()...");
		
		String mPath = "/TranData/LCCont";
		Element mLCCont = (Element) XPath.selectSingleNode(pStdXml, mPath);
		if ((mLCCont.getChildText(ContNo).equals("") || mLCCont.getChildText(ContNo) == null)
				&& (mLCCont.getChildText(ProposalContNo).equals("") || mLCCont
						.getChildText(ProposalContNo) == null)
				&& (mLCCont.getChildText(PrtNo).equals("") || mLCCont
						.getChildText(PrtNo) == null)
				) {
			cLogger.debug("传入报文缺少字段错误!!ContNo,PrtNo,ProPosalContNo 不能同时为空");
			throw new BaseException("传入报文缺少字段错误!ContNo,PrtNo,ProPosalContNo 不能同时为空");
		}
		cLogger.info("Out Query.getStdXml()!");
		return pStdXml;
	}
	
}