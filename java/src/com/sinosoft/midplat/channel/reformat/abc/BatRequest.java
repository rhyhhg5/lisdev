package com.sinosoft.midplat.channel.reformat.abc;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ResourceBundle;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;

public class BatRequest extends BaseFormat {
	private String cBatInMsg = null;
	
	public Document getStdXml(Document pNoStdXml) throws Exception {
		cLogger.info("Into BatRequest.getStdXml()...");
		
		cBatInMsg = pNoStdXml.getRootElement().getTextTrim();
		String[] mSubMsgs = cBatInMsg.split("\\|");
		
		Element mBankCode = new Element(BankCode);
		mBankCode.setText("04");
		
		Element mZoneNo = new Element(ZoneNo);
		mZoneNo.setText(mSubMsgs[3] + mSubMsgs[4]);
		
		Element mBrNo = new Element(BrNo);
		mBrNo.setText(mSubMsgs[5]);
		
		Element mTellerNo = new Element(TellerNo);
		mTellerNo.setText(mSubMsgs[6]);
		
		Element mTransrNo = new Element(TransrNo);
		mTransrNo.setText(mSubMsgs[7]);
		
		Element mFunctionFlag = new Element(FunctionFlag);
		if ("S".equals(mSubMsgs[10])) {	//代收置盘
			mFunctionFlag.setText("31");
		} else {	//代付置盘
			mFunctionFlag.setText("33");
		}
		
		Element mFileName = new Element("FileName");
		mFileName.setText(mSubMsgs[11]);
		
		Element mBaseInfo = new Element(BaseInfo);
		mBaseInfo.addContent(mBankCode);
		mBaseInfo.addContent(mZoneNo);
		mBaseInfo.addContent(mBrNo);
		mBaseInfo.addContent(mTellerNo);
		mBaseInfo.addContent(mTransrNo);
		mBaseInfo.addContent(mFunctionFlag);
		mBaseInfo.addContent(mFileName);
		
		Element mTranData = new Element(TranData);
		mTranData.addContent(mBaseInfo);
		
		cLogger.info("Out BatRequest.getStdXml()!");
		return new Document(mTranData);
	}
	
	public Document getNoStdXml(Document pStdXml) throws Exception {
		cLogger.info("Into BatRequest.getNoStdXml()...");

		Element mTranData = pStdXml.getRootElement();
		Element mRetData = mTranData.getChild(RetData);
		Element mBatData = mTranData.getChild("BatData");
		
		String[] mSubMsgs = cBatInMsg.split("\\|");
		StringBuffer mOutMsg = new StringBuffer(mSubMsgs[0]).append('|')	//报文编号
			.append(mSubMsgs[1]).append('|')	//业务甲方
			.append(mSubMsgs[2]).append('|')	//业务乙方
			.append(mSubMsgs[3]).append('|')	//乙方分行代码
			.append(mSubMsgs[4]).append('|')	//乙方地区码
			.append(mSubMsgs[5]).append('|')	//乙方网点代码
			.append(mSubMsgs[8]).append('|')	//交易日期
			.append(mSubMsgs[9]).append('|')	//交易时间
			.append(mSubMsgs[10]).append('|');	//业务性质
			
		//交易失败
		if ("0".equals(mRetData.getChildText(Flag))) {
			mOutMsg.append("1111").append('|')	//响应码
				.append(mRetData.getChildText(Desc)).append('|')	//响应信息
				.append("0.00").append('|')	//总金额
				.append("0").append('|')	//总笔数
				.append(mSubMsgs[11]).append('|')	//批量文件名
				.append("").append('|');	//备用
			
			//模拟生成一个xml：<TempTag>||批量数据|</TempTag>
			Element tTempTag = new Element("TempTag");
			tTempTag.setText(mOutMsg.toString());
			return new Document(tTempTag);
		}
		
		/**交易成功，进行以下处理********************/
		
		String mBatDataStr = mBatData.getText();
		System.out.println(mBatDataStr);
		
		//ftp上传文件至银行
		ftpUpload(mBatDataStr, mSubMsgs[11]);
		
		//组织正确返回的相关信息
		
		BufferedReader mBatDataBuf = new BufferedReader(
				new StringReader(mBatDataStr));
		String mOutHeadStr = mBatDataBuf.readLine();
		String[] mSubHeadMsgs = mOutHeadStr.split("\\|");
		
		mOutMsg.append("0000").append('|')	//响应码
			.append(mRetData.getChildText(Desc)).append('|')	//响应信息
			.append(mSubHeadMsgs[7]).append('|')	//总金额
			.append(mSubHeadMsgs[8]).append('|')	//总笔数
			.append(mSubMsgs[11]).append('|')	//批量文件名
			.append("").append('|');	//备用

		//模拟生成一个xml：<TempTag>||批量数据|</TempTag>
		Element mTempTag = new Element("TempTag");
		mTempTag.setText(mOutMsg.toString());
		
		cLogger.info("Out BatRequest.getNoStdXml()!");
		return new Document(mTempTag);
	}
	
	private void ftpUpload(String pBatData, String pBatFileName) throws Exception {
		cLogger.debug("Into BatRequest.ftpUpload()...");
		
		ResourceBundle mPreConfig = ConfigFactory.getPreConfig();
		
		String mBatFtpIp = mPreConfig.getString("ABC.Bat.FtpIp");
		cLogger.debug("ABC.Bat.FtpIp = " + mBatFtpIp);
		if ((null == mBatFtpIp) || mBatFtpIp.equals("")) {
			throw new BaseException("未配置批量代收付ftp的ip！");
		}
		
		String mBatFtpPort = null;
		try {
			mBatFtpPort = mPreConfig.getString("ABC.Bat.FtpPort");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if ((null == mBatFtpPort) || mBatFtpPort.equals("")) {
			mBatFtpPort = "21";
		}
		cLogger.debug("ABC.Bat.FtpPort = " + mBatFtpPort);
		
		String mBatFtpPath = null;
		try {
			mBatFtpPath = mPreConfig.getString("ABC.Bat.FtpPath");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if ((null == mBatFtpPath) || "".equals(mBatFtpPath)) {
			mBatFtpPath = "./";
		}
		mBatFtpPath = mBatFtpPath.replace('\\', '/');

		if (!mBatFtpPath.endsWith("/")) {
			mBatFtpPath = mBatFtpPath + "/";
		}
		cLogger.debug("ABC.Bat.FtpPath = " + mBatFtpPath);
		
		String mBatFtpUser = mPreConfig.getString("ABC.Bat.FtpUser");
		if ((null == mBatFtpUser) || "".equals(mBatFtpUser)) {
			mBatFtpUser = " ";
		}
		cLogger.debug("ABC.Bat.FtpUser = " + mBatFtpUser);
		
		String mFtpPassword = mPreConfig.getString("ABC.Bat.FtpPassword");
		if ((null == mFtpPassword) || "".equals(mFtpPassword)) {
			mFtpPassword = " ";
		}
		cLogger.debug("ABC.Bat.FtpPassword = " + mFtpPassword);
		
		//ftp到银行
		FTPClient mFTPClient = new FTPClient();
		mFTPClient.setDefaultPort(Integer.parseInt(mBatFtpPort));
		
		try {
			//建立ftp连接
			mFTPClient.connect(mBatFtpIp);
			int tReplyCode = mFTPClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(tReplyCode)) {
				mFTPClient.disconnect();
				throw new BaseException("ftp连接失败！" + mBatFtpIp + ": " + tReplyCode);
			}
			cLogger.debug("ftp连接成功！" + mBatFtpIp);

			//登录
			if (!mFTPClient.login(mBatFtpUser, mFtpPassword)) {
				mFTPClient.logout();
				throw new BaseException("ftp登录失败！" + mBatFtpUser + ":" + mFtpPassword);
			}
			cLogger.debug("ftp登录成功！");
			
			mFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
			//上传数据
			if (!mFTPClient.storeFile(
					mBatFtpPath+pBatFileName, new ByteArrayInputStream(pBatData.getBytes("GBK")))) {
				mFTPClient.logout();
				throw new BaseException("ftp上传数据失败！");
			}
			cLogger.debug("ftp上传数据成功！");
			
			//退出登陆
			mFTPClient.logout();
			cLogger.debug("ftp退出成功！");
		} finally {
			if (mFTPClient.isConnected()) {
				try {
					mFTPClient.disconnect();
					cLogger.debug("ftp连接断开！");
				} catch(IOException ex) {
					cLogger.debug("服务端连接已断开！", ex);
				}
			}
		}
		
		cLogger.debug("Out BatRequest.ftpUpload()!");
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
		new BatRequest().ftpUpload("464645|154|daoa||||", "12345");
		
		System.out.println("成功结束！");
	}
}
