package com.sinosoft.midplat.channel;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.Socket;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.security.CCBCipher;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;

public class CCBControl extends BaseControl {
	private CCBCipher cCCBCipher = null;
	
	public CCBControl(Socket pSocket) {
		super(pSocket);
		
		cBaseInfoMap.put("BankName", "ccb");
		
		try {
			cCCBCipher = new CCBCipher(cSocket.getLocalPort());
		} catch (Exception ex) {
			cLogger.error("读取密钥失败！", ex);
		}
	}

	protected byte[] receive(InputStream pIs) throws Exception {
		cLogger.info("Into CCBControl.receive()...");
		
		//处理报文头
		byte[] mHeadBytes = new byte[6];
		for (int tReadSize = 0; tReadSize < mHeadBytes.length;) {
			int tRead = pIs.read(mHeadBytes, tReadSize, mHeadBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文头出错！实际读入：" + new String(mHeadBytes));
			}
			tReadSize += tRead;
		}
		int mBodyLength = Integer.parseInt(new String(mHeadBytes).trim());
		cLogger.debug("请求报文长度：" + mBodyLength);
		
		//处理报文体
		byte[] mBodyBytes = new byte[mBodyLength];
		for (int tReadSize = 0; tReadSize < mBodyBytes.length;) {
			int tRead = pIs.read(mBodyBytes, tReadSize, mBodyBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文体出错！实际读入长度为：" + tReadSize);
			}
			tReadSize += tRead;
		}
		
		//保存原始报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(new java.rmi.server.UID().toString().replaceAll(":", ""))
			.append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".txt");
		SaveMessage.save(mBodyBytes, mSaveName.toString(), "original");
		cLogger.info("保存原始请求报文完毕！文件名：" + mSaveName);
		
		cLogger.info("开始解密...");
		byte[] mClearBytes = cCCBCipher.decode(mBodyBytes);
		cLogger.info("解密成功！");
		
		cLogger.info("Out CCBControl.receive()!");
		return mClearBytes;
	}
	
	protected Document getInNoStdXml(byte[] pBytes) throws Exception {
		cLogger.info("Into CCBControl.getInNoStdXml()...");
		
		Document mXmlDoc = JdomUtil.build(pBytes);
		
		Element mTransaction_Header = mXmlDoc.getRootElement().getChild(Transaction_Header);
		cBaseInfoMap.put("TransNo", mTransaction_Header.getChildText(BkPlatSeqNo));
		cLogger.info("交易流水号：" + cBaseInfoMap.get("TransNo"));
		cBaseInfoMap.put("FuncFlag", mTransaction_Header.getChildText(BkTxCode));
		cLogger.info("交易代码：" + cBaseInfoMap.get("FuncFlag"));
		
		//保存非标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mXmlDoc, mSaveName.toString(), "InNoStd");
		cLogger.info("保存非标准请求报文完毕！");
		
		cLogger.info("Out CCBControl.getInNoStdXml()!");
		return mXmlDoc;
	}

	protected Document getErrorXml(String pErrorMsg) {
		cLogger.info("Into CCBControl.getErrorXml()...");
		
//		Element mLiBankID = new Element(LiBankID);
//		
//		Element mPbInsuId = new Element(PbInsuId);
//		
//		Element mBkPlatSeqNo = new Element(BkPlatSeqNo);
//		
//		Element mBkTxCode = new Element(BkTxCode);
//		
//		Element mBkChnlNo = new Element(BkChnlNo);
//		
//		Element mBkBrchNo = new Element(BkBrchNo);
//		
//		Element mBkTellerNo = new Element(BkTellerNo);
//		
//		Element mBkPlatDate = new Element(BkPlatDate);
//		mBkPlatDate.setText(DateUtil.getCurrentDate("yyyyMMdd"));
//		
//		Element mBkPlatTime = new Element(BkPlatTime);
//		mBkPlatTime.setText(DateUtil.getCurrentDate("HH:mm:ss"));
		
		Element mBkOthDate = new Element(BkOthDate);
		mBkOthDate.setText(DateUtil.getCurrentDate("yyyyMMdd"));
		
		Element mBkPlatSeqNo = new Element(BkPlatSeqNo);
		
		Element mBkOthRetCode = new Element(BkOthRetCode);
		mBkOthRetCode.setText("10001");
		
		Element mBkOthRetMsg = new Element(BkOthRetMsg);
		mBkOthRetMsg.setText(pErrorMsg);
		
		Element mTran_Response = new Element(Tran_Response);
		mTran_Response.addContent(mBkOthDate);
		mTran_Response.addContent(mBkPlatSeqNo);
		mTran_Response.addContent(mBkOthRetCode);
		mTran_Response.addContent(mBkOthRetMsg);
		
		Element mTransaction_Header = new Element(Transaction_Header);
		mTransaction_Header.addContent(mTran_Response);

		Element mTransaction = new Element(Transaction);
		mTransaction.addContent(mTransaction_Header);
		
		cLogger.info("Out CCBControl.getErrorXml()!");
		return new Document(mTransaction);
	}
	
	protected byte[] getOutBytes(Document pXmlDoc) throws Exception {
		cLogger.info("Into CCBControl.getOutBytes()...");
		
		// 建行要求退保信息传递和批量交易要格式化
		Format mFormat = null;
		if (cBaseInfoMap.get("FuncFlag").equals("SPE801") || cBaseInfoMap.get("FuncFlag").equals("BAT900")
				|| cBaseInfoMap.get("FuncFlag").equals("BAT901") || cBaseInfoMap.get("FuncFlag").equals("BAT902")
				|| cBaseInfoMap.get("FuncFlag").equals("BAT911")) {
			mFormat = Format.getRawFormat().setEncoding("GBK").setIndent("   ").setLineSeparator("\n");
		} else {
			//建行需要我方排版保单打印格式，所以转换为字节时需保留原格式，不做任何格式化处理
			mFormat = Format.getRawFormat().setEncoding("GBK");
		}
		
		XMLOutputter mXMLOutputter = new XMLOutputter(mFormat);
		ByteArrayOutputStream mBaos = new ByteArrayOutputStream();
		mXMLOutputter.output(pXmlDoc, mBaos);
		byte[] mClearBytes = mBaos.toByteArray();
		
		cLogger.info("开始加密...");
		byte[] mCipherBytes = cCCBCipher.encode(
				new String(mClearBytes, "GBK"));
		cLogger.info("加密成功！");

		byte[] mTotalBytes = new byte[mCipherBytes.length + 6];
		
		//报文体长度
		String mLengthString = String.valueOf(mCipherBytes.length);
		cLogger.info("返回报文长度：" + mLengthString);
		byte[] mLengthBytes = mLengthString.getBytes();
		System.arraycopy(mLengthBytes, 0, mTotalBytes, 0, mLengthBytes.length);

		System.arraycopy(mCipherBytes, 0, mTotalBytes, 6, mCipherBytes.length);
		
		cLogger.info("Out CCBControl.getOutBytes()!");
		return mTotalBytes;
	}
}
