/**
 * 中行前置机总控程序！
 */

package com.sinosoft.midplat.channel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.DOMBuilder;
import org.jdom.output.DOMOutputter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.xml.sax.InputSource;

import com.sinosoft.midplat.channel.config.ConfigFactory;
import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.reformat.BaseFormat;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;
import com.sinosoft.midplat.channel.util.XmlTag;

public class BOCControl implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(BOCControl.class);
	
	private HashMap cBaseInfoMap = new HashMap();
	
	private BaseFormat cFormat = null;
	
	public BOCControl() {
		cLogger.info("客户端访问中行WebService服务(BocYbtService)！");
		cBaseInfoMap.put("BankName", "boc");
	}
	
	public org.w3c.dom.Element[] service(org.w3c.dom.Element[] pDomElements) {
		cLogger.info("Into BOCControl.service()...");
		
		long mStartMillis = System.currentTimeMillis();
		Document mOutNoStdXml = null;
		
		try {
			Document tInNoStdXml = getInNoStdXml(pDomElements[0]);
			/**
			 * add by wangxt in 2009-06-16 begin
			 * 保存原始报文
			 */
			byte[] mBodyBytes = JdomUtil.toBytes(tInNoStdXml);
			//保存原始报文
			StringBuffer mSaveName = new StringBuffer();
			mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(new java.rmi.server.UID().toString().replaceAll(":", ""))
				.append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".txt");
			SaveMessage.save(mBodyBytes, mSaveName.toString(), "original");
			cLogger.info("保存原始请求报文完毕！文件名：" + mSaveName);
			/**
			 * add by wangxt in 2009-06-16 end
			 */
			
			pDomElements = null;
			
			//转换为标准请求报文
			Document tInStdXml = getInStdXml(tInNoStdXml);
			tInNoStdXml = null;
			
			//调用后置机业务处理，获取标准返回报文
			Document tOutStdXml = callPostServlet(tInStdXml);
			tInStdXml = null;
			
			//转换为非标准返回报文
			mOutNoStdXml = getOutNoStdXml(tOutStdXml);
			tOutStdXml = null;
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
			mOutNoStdXml = getSoapError(ex.getMessage());
		}
		
		//保存非标准返回报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mOutNoStdXml, mSaveName.toString(), "OutNoStd");
		cLogger.info("保存非标准返回报文完毕！");
		
		org.w3c.dom.Document mOutDomDoc = null;
		try {
			mOutDomDoc = new DOMOutputter().output(mOutNoStdXml);
		} catch (JDOMException ex) {
			cLogger.error("jdom.Document --> dom.Document转换失败！", ex);
		}
		org.w3c.dom.Element mOutDomElements[] = new org.w3c.dom.Element[1];
		mOutDomElements[0] = mOutDomDoc.getDocumentElement();
		
		long mDealMillis = System.currentTimeMillis() - mStartMillis;
		cLogger.info("处理总耗时：" + mDealMillis/1000.0 + "s");
		
		cLogger.info("Out BOCControl.service()!");
		return mOutDomElements;
	}
	
	private Document getInNoStdXml(org.w3c.dom.Element pDomElement) throws Exception {
		cLogger.info("Into BOCControl.getInNoStdXml()...");
		
		Element mJdomElement = new DOMBuilder().build(pDomElement);
		Document mXmlDoc = mJdomElement.getDocument();
		Element mBaseInfo = mJdomElement.getChild(TranData).getChild(BaseInfo);
		cBaseInfoMap.put("TransNo", mBaseInfo.getChildTextTrim(TransrNo));
		cLogger.info("交易流水号：" + cBaseInfoMap.get("TransNo"));
		
		//转换交易代码
		String mFuncName = mJdomElement.getName();
		if (mFuncName.equals(onLineUnderWrite)) {
			cBaseInfoMap.put("FuncFlag", "23"); //试算
		} else if (mFuncName.equals(onLineAuditPolicy)) {
			String tProposalState = mJdomElement.getChild(TranData).getChild(
					LCCont).getChildTextTrim(ProposalState);
			if(tProposalState.equals("K")){	//承保交易
				cBaseInfoMap.put("FuncFlag", "01");
			}else if(tProposalState.equals("R")){	//重打交易
				cBaseInfoMap.put("FuncFlag", "02");
			}
		} else if(mFuncName.equals(onLineCancel)) {
			cBaseInfoMap.put("FuncFlag", "04"); //当日撤单
		} else if(mFuncName.equals(onLineReverse)) {
			cBaseInfoMap.put("FuncFlag", "19"); //冲正交易(用于重打之前)
		} else if(mFuncName.equals(onLineSendMatch)) {
			cBaseInfoMap.put("FuncFlag", "17"); //日终对账
		} else {
			throw new BaseException("交易头信息有误！FuncName=" + mFuncName);
		}
		cLogger.info("交易代码：" + cBaseInfoMap.get("FuncFlag"));
		
		//保存非标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mXmlDoc, mSaveName.toString(), "InNoStd");
		cLogger.info("保存非标准请求报文完毕！");
		
		cLogger.info("Out BOCControl.getInNoStdXml()!");
		return mXmlDoc;
	}
	
	private Document getInStdXml(Document pInNoStdXml) throws Exception {
		cLogger.info("Into BOCControl.getInStdXml()...");
		
		Document mFormatConfigDoc = ConfigFactory.getFormatConfig();		
		XPath mXPath = XPath.newInstance(
				"/formatConfigs/formatConfig[bank='" + cBaseInfoMap.get("BankName")
				+ "' and funcFlag='" + cBaseInfoMap.get("FuncFlag") + "']/class");
		String mClassName = mXPath.valueOf(mFormatConfigDoc);
		cLogger.debug("转换类：" + mClassName);
		Class mFormatClass = Class.forName(mClassName);
		cFormat = (BaseFormat) mFormatClass.newInstance();
		
		Document mInStdXml = cFormat.getStdXml(pInNoStdXml);
		cLogger.info("非标准报文到标准报文转换完成！");

		//保存标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mInStdXml, mSaveName.toString(), "InStd");
		cLogger.info("保存标准请求报文完毕！");
		
		cLogger.info("Out BOCControl.getInStdXml()!");
		return mInStdXml;
	}
	
	protected Document callPostServlet(Document pInStdXmlDoc) throws Exception {
		cLogger.info("Into BOCControl.callPostServlet()...");
		
		String mPostServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
		cLogger.info("PostServletURL = " + mPostServletURL);
		URL mURL = new URL(mPostServletURL);
		URLConnection mURLConnection = mURL.openConnection();
		mURLConnection.setDoOutput(true);
		mURLConnection.setDoInput(true);
		XMLOutputter mXMLOutputter = new XMLOutputter(Format.getCompactFormat().setEncoding("GBK"));
		cLogger.info("前置机开始向后置机发送报文...");
		OutputStream mURLOs = mURLConnection.getOutputStream();
		mXMLOutputter.output(pInStdXmlDoc, mURLOs);
		mURLOs.close();
      
		Document mOutStdXml = null;
		if (cBaseInfoMap.get("FuncFlag").equals("17")) {	//对账后台可能处理需要很长时间，另起一个线程处理，本线程直接造一个报文返回
			cLogger.info("中行对账，直接返回成功！(具体对账操作在后台进行...)");
			
			new WaitPostThread(mURLConnection).start();
			
			Element tFlag = new Element(Flag);
			tFlag.addContent("1");	//0-失败；1-成功
			
			Element tDesc = new Element(Desc);
			tDesc.addContent("对账成功！");

			Element tRetData = new Element(RetData);
			tRetData.addContent(tFlag);
			tRetData.addContent(tDesc);
			
			Element tTranData = new Element(TranData);
			tTranData.addContent(tRetData);

			mOutStdXml = new Document(tTranData);
		} else {	//其他正常交易
			long tStartMillis = System.currentTimeMillis();
			InputStream tURLIs = mURLConnection.getInputStream();
			mOutStdXml = JdomUtil.build(tURLIs);	//统一使用GBK编码
			tURLIs.close();
			cLogger.info("后置机处理耗时：" + (System.currentTimeMillis()-tStartMillis)/1000.0 + "s");
			cLogger.info("接收到后置机返回报文，并解析为Document！");
		}
		
		//保存非标准返回报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mOutStdXml, mSaveName.toString(), "OutStd");
		cLogger.info("保存标准返回报文完毕！");
		
		cLogger.info("Out BOCControl.callPostServlet()!");
		return mOutStdXml;
	}
	
	protected Document getOutNoStdXml(Document pOutStdXml) throws Exception {
		cLogger.info("Into BOCControl.getOutNoStdXml()...");
		
		Document mOutNoStdXml = cFormat.getNoStdXml(pOutStdXml);
		cLogger.info("标准报文到非标准报文转换完成！");

		cLogger.info("Out BOCControl.getOutNoStdXml()!");
		return mOutNoStdXml;
	}
	
	public static Document getSoapError(String pErrorMsg) {
		cLogger.info("Into BOCControl.getErrorXml()...");
		
		Element mFaultcode = new Element(faultcode);
		mFaultcode.addContent("0");	//0-失败, 1-成功
		
		Element mFaultstring = new Element(faultstring);
		mFaultstring.addContent(pErrorMsg);
		
		Namespace mNamespace = Namespace.getNamespace(
				"SOAP-ENV", SOAP_URI);
		Element mFault = new Element(Fault, mNamespace);
		mFault.addContent(mFaultcode);
		mFault.addContent(mFaultstring);
		Document mNoStdXml = new Document(mFault);

		cLogger.info("Out BOCControl.getErrorXml()!");
		return mNoStdXml;
	}
	
	private class WaitPostThread extends Thread {
		private final Logger cLogger = Logger.getLogger(WaitPostThread.class);
		
		private final URLConnection cURLConnection;
		
		public WaitPostThread(URLConnection pURLConnection) {
			cURLConnection = pURLConnection;
		}
		
		public void run() {
			cLogger.info("Into WaitPostThread.run()...");

			long mStartMillis = System.currentTimeMillis();
			
			Document mOutStdXml = null;
			try {
				mOutStdXml = JdomUtil.build(cURLConnection.getInputStream());	//统一使用GBK编码
				JdomUtil.print(mOutStdXml);
			} catch (IOException ex) {
				cLogger.error("获取对账结果失败！", ex);
			}
			cLogger.info("后置机对账耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s");
			
			cLogger.info("Out WaitPostThread.run()!");
		}
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");

		String mInFilePath = "E:/Test-haoqt/picch/boc/testXml/newCont/boc_01_in_230701.xml";
		String mOutFilePath = "E:/Test-haoqt/picch/boc/testXml/newCont/boc_01_in_230701.xml";
		
//		String mInFilePath = "E:/Test-haoqt/picch/boc/testXml/trial/boc_23_in_230701.xml";
//		String mOutFilePath = "E:/Test-haoqt/中行资料/boc/testXml/newCont/boc_01_out_330501.xml";
		
//		String mInFilePath = "E:/Test-haoqt/picch/boc/testXml/writeOff/boc_04_in.xml";
//		String mInFilePath = "E:/Test-haoqt/picch/boc/testXml/rePrint/boc_02_in.xml";		

		InputSource mInputSource = new InputSource(new FileInputStream(mInFilePath));
		mInputSource.setEncoding("GBK");
		DocumentBuilderFactory mDocFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder mDocbuilder = mDocFactory.newDocumentBuilder();
		org.w3c.dom.Document mInDomDoc = mDocbuilder.parse(mInputSource);
		org.w3c.dom.Element mInDomElements[] = new org.w3c.dom.Element[1];
		mInDomElements[0] = mInDomDoc.getDocumentElement();

		BOCControl mBOCControl = new BOCControl();
		org.w3c.dom.Element mOutDomElements[] = mBOCControl.service(mInDomElements);

		TransformerFactory mTransformerFactory = TransformerFactory.newInstance();
		DOMSource mDOMSource = new DOMSource(mOutDomElements[0]);
		StreamResult mStreamResult = new StreamResult(new FileOutputStream(mOutFilePath));
//		StreamResult mStreamResult = new StreamResult(System.out);
		Transformer mTransformer = mTransformerFactory.newTransformer();
		mTransformer.setOutputProperty(OutputKeys.ENCODING, "GBK");
		mTransformer.transform(mDOMSource, mStreamResult);

		System.out.println("成功结束！");
	}
}
