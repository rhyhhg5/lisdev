package com.sinosoft.midplat.channel.security;

import java.security.MessageDigest;

import org.apache.log4j.Logger;

public class MD5Cipher {
	private final static Logger cLogger = Logger.getLogger(MD5Cipher.class);
	
	private final static byte cHexDigits[] = {       //16进制字符表
		     '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',  'e', 'f'};
	
	public static String getMD5HexMsg(byte[] pBytes) throws Exception {
		cLogger.info("Into MD5Cipher.getMD5HexMsg()...");
		
		MessageDigest mMessageDigest = MessageDigest.getInstance( "MD5" );
		byte[] mMsgBytes = mMessageDigest.digest(pBytes);
		
		byte[] mHexBytes = new byte[mMsgBytes.length * 2];	//1字节需要两个16进制字符表示
	   for (int i = 0; i < mMsgBytes.length; i++) {
	   	mHexBytes[i * 2] = cHexDigits[mMsgBytes[i] >>> 4 & 0xf];	//将符号位一起右移4位，转换高4位字节
	   	mHexBytes[i*2 + 1] = cHexDigits[mMsgBytes[i] & 0xf];	//转换低4位字节
	   }
		
		cLogger.info("Out MD5Cipher.getMD5HexMsg()!");
		return new String(mHexBytes);
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
			
		System.out.println("HexMsg = "
				+ MD5Cipher.getMD5HexMsg("message digest".getBytes()));
		
		System.out.println("成功结束！");
	}
}