package com.sinosoft.midplat.channel.security;

import org.apache.log4j.Logger;

import com.adtec.security.Nobis;

public class CCBCipher {
	private Logger cLogger = Logger.getLogger(getClass());

	private Nobis cNobis = null;
	
	public CCBCipher(int pPort) throws Exception {
		String mKeyPath = getClass().getResource(pPort + "/" + pPort + ".dat").getPath();
		cLogger.debug("密钥文件路径：" + mKeyPath);
		cNobis = Nobis.nobisFactory();
		cNobis.bisReadKey(mKeyPath);
	}
	
	/**
	 * 加密
	 */
	public byte[] encode(String pClearStr) throws Exception {
		return cNobis.bisPkgCompressEnc(pClearStr);
	}
	
	/**
	 * 解密
	 */
	public byte[] decode(byte[] pCipherBytes) throws Exception {
		return cNobis.bisPkgDecompressDec(pCipherBytes);
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始...");
		
		String mTestClearStr = "djiejdijeijd陈贵菠ijadijaidjidei";
		System.out.println("\n原始：\n" + mTestClearStr);
		
		CCBCipher mCCBCipher = new CCBCipher(52003);
		byte[] mCipherBytes = mCCBCipher.encode(mTestClearStr);
		System.out.println("\n加密：\n" + new String(mCipherBytes));
		
		byte[] mClearBytes = mCCBCipher.decode(mCipherBytes);
		System.out.println("\n解密：\n" + new String(mClearBytes, "GBK"));
		
		System.out.println("\n成功结束！");
	}
}
