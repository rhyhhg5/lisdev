package com.sinosoft.midplat.channel;

import java.io.InputStream;
import java.net.Socket;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;

public class ABCControl extends BaseControl {
	private String cBatInMsg = null;
	
	public ABCControl(Socket pSocket) {
		super(pSocket);
		
		cBaseInfoMap.put("BankName", "abc");
	}
	
	protected byte[] receive(InputStream pIs) throws Exception {
		cLogger.info("Into ABCControl.receive()...");
		
		//处理报文头
		byte[] mHeadBytes = new byte[8];
		for (int tReadSize = 0; tReadSize < mHeadBytes.length;) {
			int tRead = pIs.read(mHeadBytes, tReadSize, mHeadBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文头出错！实际读入：" + new String(mHeadBytes));
			}
			tReadSize += tRead;
		}
		int mBodyLength = Integer.parseInt(new String(mHeadBytes, 0, 6).trim());
		cLogger.debug("请求报文长度：" + mBodyLength);
		cBaseInfoMap.put("FuncFlag", new String(mHeadBytes, 6, 2).trim());
		cLogger.info("交易代码：" + cBaseInfoMap.get("FuncFlag"));
		
		//处理报文体
		byte[] mBodyBytes = new byte[mBodyLength];
		for (int tReadSize = 0; tReadSize < mBodyBytes.length;) {
			int tRead = pIs.read(mBodyBytes, tReadSize, mBodyBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文体出错！实际读入长度为：" + tReadSize);
			}
			tReadSize += tRead;
		}
		
		//保存原始报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(new java.rmi.server.UID().toString().replaceAll(":", ""))
			.append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".txt");
		SaveMessage.save(mBodyBytes, mSaveName.toString(), "original");
		cLogger.info("保存原始请求报文完毕！文件名：" + mSaveName);
		
		cLogger.info("Out ABCControl.receive()!");
		return mBodyBytes;
	}
	
	protected Document getInNoStdXml(byte[] pBytes) throws Exception {
		cLogger.info("Into ABCControl.getInNoStdXml()...");

		Document mXmlDoc = null;
		
		String mFuncFlag = (String) cBaseInfoMap.get("FuncFlag");
		if ("31".equals(mFuncFlag)			//代收付置盘
			|| "32".equals(mFuncFlag)) {	//代收付返盘
			cBatInMsg = new String(pBytes, "GBK");
			
			//模拟生成一个xml：<TempTag>||批量数据|</TempTag>
			Element tTempTag = new Element("TempTag");
			tTempTag.setText(cBatInMsg);
			mXmlDoc = new Document(tTempTag);
		} else {	//xml格式的常规交易
			mXmlDoc = JdomUtil.build(pBytes);

			Element tBaseInfo = mXmlDoc.getRootElement().getChild(BaseInfo);
			cBaseInfoMap.put("TransNo", tBaseInfo.getChildTextTrim(TransrNo));
			cLogger.info("交易流水号：" + cBaseInfoMap.get("TransNo"));
		}
		
		//保存非标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mXmlDoc, mSaveName.toString(), "InNoStd");
		cLogger.info("保存非标准请求报文完毕！");

		cLogger.info("Out ABCControl.getInNoStdXml()!");
		return mXmlDoc;
	}
	
	protected Document getErrorXml(String pErrorMsg) {
		cLogger.info("Into ABCControl.getErrorXml()...");
		
		Document mErrorXml = null;
		
		String mFuncFlag = (String) cBaseInfoMap.get("FuncFlag");
		if ("31".equals(mFuncFlag)			//代收付置盘
				|| "32".equals(mFuncFlag)) {	//代收付返盘
			String[] tSubMsgs = cBatInMsg.split("\\|");
			
			StringBuffer mErrorMsg = new StringBuffer(tSubMsgs[0]).append('|')	//报文编号
				.append(tSubMsgs[1]).append('|')	//业务甲方
				.append(tSubMsgs[2]).append('|')	//业务乙方
				.append(tSubMsgs[3]).append('|')	//乙方分行代码
				.append(tSubMsgs[4]).append('|')	//乙方地区码
				.append(tSubMsgs[5]).append('|')	//乙方网点代码
				.append(tSubMsgs[8]).append('|')	//交易日期
				.append(tSubMsgs[9]).append('|')	//交易时间
				.append(tSubMsgs[10]).append('|')	//业务性质
				.append("1111").append('|')	//响应码
				.append(pErrorMsg).append('|');	//响应信息
			
			if ("31".equals(mFuncFlag)) {	//代收付置盘
				mErrorMsg.append("0.00").append('|')	//总金额
					.append("0").append('|');	//总笔数
			}
			
			mErrorMsg.append(tSubMsgs[11]).append('|')	//批量文件名
				.append("").append('|');	//备用
			
			//模拟生成一个xml：<TempTag>||批量数据|</TempTag>
			Element tTempTag = new Element("TempTag");
			tTempTag.setText(mErrorMsg.toString());
			mErrorXml = new Document(tTempTag);
		} else {	//xml格式的常规交易
			//正常xml交易
			Element tFlag = new Element(Flag);
			tFlag.addContent("0");	//0-失败；1-成功
			
			Element tDesc = new Element(Desc);
			tDesc.addContent(pErrorMsg);

			Element tRetData = new Element(RetData);
			tRetData.addContent(tFlag);
			tRetData.addContent(tDesc);
			
			Element tTranData = new Element(TranData);
			tTranData.addContent(tRetData);
			
			mErrorXml = new Document(tTranData);
		}
		
		cLogger.info("Out ABCControl.getErrorXml()!");
		return mErrorXml;
	}
	
	protected byte[] getOutBytes(Document pXmlDoc) throws Exception {
		cLogger.info("Into ABCControl.getOutBytes()...");
		
		byte[] mBodyBytes = JdomUtil.toBytes(pXmlDoc);
		
		String mFuncFlag = (String) cBaseInfoMap.get("FuncFlag");
		if ("31".equals(mFuncFlag)			//代收付置盘
				|| "32".equals(mFuncFlag)) {	//代收付返盘
			mBodyBytes = pXmlDoc.getRootElement().getText().getBytes("GBK");
		} else {	//xml格式的常规交易
			mBodyBytes = JdomUtil.toBytes(pXmlDoc);
		}
		
		byte[] mTotalBytes = new byte[mBodyBytes.length + 8];
		
		//报文体长度
		String mLengthString = String.valueOf(mBodyBytes.length);
		cLogger.info("返回报文长度：" + mLengthString);
		byte[] mLengthBytes = mLengthString.getBytes();
		System.arraycopy(mLengthBytes, 0, mTotalBytes, 0, mLengthBytes.length);

		//交易代码
		byte[] mFuncFlagBytes = mFuncFlag.getBytes();
		System.arraycopy(mFuncFlagBytes, 0, mTotalBytes, 6, mFuncFlagBytes.length);
		
		System.arraycopy(mBodyBytes, 0, mTotalBytes, 8, mBodyBytes.length);
		
		cLogger.info("Out ABCControl.getOutBytes()!");
		return mTotalBytes;
	}
}