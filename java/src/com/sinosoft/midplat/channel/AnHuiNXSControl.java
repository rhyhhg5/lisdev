/**
 * 安徽农信社信保通前置机总控程序
 */

package com.sinosoft.midplat.channel;

import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;

public class AnHuiNXSControl extends BaseControl {
	private final List cHeadList = new ArrayList();
	
	public AnHuiNXSControl(Socket pSocket) {
		super(pSocket);
		
		cBaseInfoMap.put("BankName", "AnHuiNXS");
	}

	protected byte[] receive(InputStream pIs) throws Exception {
		cLogger.info("Into AnHuiNXSControl.receive()...");
		
		//处理报文头
		byte[] mHeadBytes = new byte[19];
		for (int tReadSize = 0; tReadSize < mHeadBytes.length;) {
			int tRead = pIs.read(mHeadBytes, tReadSize, mHeadBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				cLogger.error("获取报文头出错！读入" + tReadSize + "字节数据："+ new String(mHeadBytes));
				throw new BaseException("获取报文头出错！读入" + tReadSize + "字节数据");
			}
			tReadSize += tRead;
		}
		int mBodyLength = Integer.parseInt(new String(mHeadBytes, 0, 6).trim());
		cLogger.debug("请求报文长度：" + mBodyLength);
		cBaseInfoMap.put("FuncFlag", new String(mHeadBytes, 6, 7).trim());
		cLogger.info("交易代码：" + cBaseInfoMap.get("FuncFlag"));
		cBaseInfoMap.put("InsuID", new String(mHeadBytes, 13, 6).trim());
		cLogger.debug("保险公司代码：" + cBaseInfoMap.get("InsuID"));
		
		//处理报文体
		byte[] mBodyBytes = new byte[mBodyLength];
		for (int tReadSize = 0; tReadSize < mBodyBytes.length;) {
			int tRead = pIs.read(mBodyBytes, tReadSize, mBodyBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文体出错！实际读入长度为：" + tReadSize);
			}
			tReadSize += tRead;
		}
		
		//保存原始报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(new java.rmi.server.UID().toString().replaceAll(":", ""))
			.append("_").append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".txt");
		SaveMessage.save(mBodyBytes, mSaveName.toString(), "original");
		cLogger.info("保存原始请求报文完毕！文件名：" + mSaveName);
		
		cLogger.info("Out AnHuiNXSControl.receive()!");
		return mBodyBytes;
	}
	
	protected Document getInNoStdXml(byte[] pBytes) throws Exception {
		cLogger.info("Into AnHuiNXSControl.getInNoStdXml()...");
		
		Document mXmlDoc = JdomUtil.build(pBytes);
		
		//保存头信息，组织前置机报错返回报文使用
		setHead(mXmlDoc);
		
		cBaseInfoMap.put("TransNo", mXmlDoc.getRootElement().getChildTextTrim(TransRefGUID));
		cLogger.info("交易流水号：" + cBaseInfoMap.get("TransNo"));
		
		//保存非标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mXmlDoc, mSaveName.toString(), "InNoStd");
		cLogger.info("保存非标准请求报文完毕！");
		
		cLogger.info("Out AnHuiNXSControl.getInNoStdXml()!");
		return mXmlDoc;
	}
	
	protected Document getErrorXml(String pErrorMsg) {
		cLogger.info("Into AnHuiNXSControl.getErrorXml()...");
		
		Element mResultCode = new Element(ResultCode);
		mResultCode.addContent("01");
		
		Element mResultInfoDesc = new Element(ResultInfoDesc);
		mResultInfoDesc.addContent(pErrorMsg);
		
		Element mTXLife = new Element(TXLife);
		mTXLife.addContent(cHeadList);
		mTXLife.addContent(mResultCode);
		mTXLife.addContent(mResultInfoDesc);
		
		cLogger.info("Out AnHuiNXSControl.getErrorXml()!");
		return new Document(mTXLife);
	}
	
	private void setHead(Document pXmlDoc) {
		Element mTXLife = pXmlDoc.getRootElement();
		cHeadList.add(
				mTXLife.getChild(TransNo).clone());
		cHeadList.add(
				mTXLife.getChild("QdBankCode").clone());
		cHeadList.add(
				mTXLife.getChild(BankCode).clone());
		cHeadList.add(
				mTXLife.getChild(Branch).clone());
		cHeadList.add(
				mTXLife.getChild(CarrierCode).clone());
		cHeadList.add(
				mTXLife.getChild(TransExeDate).clone());
		cHeadList.add(
				mTXLife.getChild(TransExeTime).clone());
		
		Element mTransRefGUID = mTXLife.getChild(TransRefGUID);
		cHeadList.add(mTransRefGUID.clone());
		
		Element mCpicWater = new Element("CpicWater");
		mCpicWater.setText(mTransRefGUID.getText());
		cHeadList.add(mCpicWater);
		
		cHeadList.add(
				mTXLife.getChild(Teller).clone());
	}
	
	protected byte[] getOutBytes(Document pXmlDoc) throws Exception {
		cLogger.info("Into AnHuiNXSControl.getOutBytes()...");
		
		byte[] mBodyBytes = JdomUtil.toBytes(pXmlDoc);

		byte[] mTotalBytes = new byte[mBodyBytes.length + 19];
		//初始化前19位报文头为空格
		for (int i = 0; i < 19; i++) {
			mTotalBytes[i] = ' ';
		}
		
		//报文体长度
		String mLengthString = String.valueOf(mBodyBytes.length);
		cLogger.info("返回报文长度：" + mLengthString);
		byte[] mLengthBytes = mLengthString.getBytes();
		System.arraycopy(mLengthBytes, 0, mTotalBytes, 0, mLengthBytes.length);
		
		//交易代码
		byte[] mFuncFlagBytes = ((String) cBaseInfoMap.get("FuncFlag")).getBytes();
		System.arraycopy(mFuncFlagBytes, 0, mTotalBytes, 6, mFuncFlagBytes.length);
		
		//公司代码
		byte[] mInsuIDBytes = ((String) cBaseInfoMap.get("InsuID")).getBytes();
		System.arraycopy(mInsuIDBytes, 0, mTotalBytes, 13, mInsuIDBytes.length);
		
		System.arraycopy(mBodyBytes, 0, mTotalBytes, 19, mBodyBytes.length);
		
		cLogger.info("Out AnHuiNXSControl.getOutBytes()!");
		return mTotalBytes;
	}
}
