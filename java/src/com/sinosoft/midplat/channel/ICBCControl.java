package com.sinosoft.midplat.channel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.security.DESCipher;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;

public class ICBCControl extends BaseControl {
	private DESCipher cDESCipher = null;
	
	public ICBCControl(Socket pSocket) {
		super(pSocket);
		
		cBaseInfoMap.put("BankName", "icbc");
		
		try {
			cDESCipher = new DESCipher(cSocket.getLocalPort());
		} catch (IOException ex) {
			cLogger.error("读取密钥失败！", ex);
		}
	}
	
	public void run() {
		cLogger.info("Into ICBCControl.run()...");
		
		long mStartMillis = System.currentTimeMillis();
		Document mOutNoStdXml = null;
		
		try {
			cDESCipher = new DESCipher(cSocket.getLocalPort());	//更新为新密钥
			cLogger.info("更新后的密钥"+cDESCipher.toString());
			//解析报文
			byte[] tBodyBytes = receive(cSocket.getInputStream());
			
			Document tInNoStdXml = getInNoStdXml(tBodyBytes);
			tBodyBytes = null;
			
			//密钥更换
			if (cBaseInfoMap.get("FuncFlag").equals("0001")) {
				mOutNoStdXml = keyChange(tInNoStdXml);
				cDESCipher = new DESCipher(cSocket.getLocalPort());	//更新为新密钥
				return;
			}
			
			//转换为标准请求报文
			Document tInStdXml = getInStdXml(tInNoStdXml);
			tInNoStdXml = null;
			
			//调用后置机业务处理，获取标准返回报文
			Document mOutStdXml = callPostServlet(tInStdXml);
			tInStdXml = null;
			
			//转换为非标准返回报文
			mOutNoStdXml = getOutNoStdXml(mOutStdXml);
			mOutStdXml = null;
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
			mOutNoStdXml = getErrorXml(ex.getMessage());
		} finally {
			//保存非标准返回报文
			StringBuffer mSaveName = new StringBuffer();
			mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
				.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
			SaveMessage.save(mOutNoStdXml, mSaveName.toString(), "OutNoStd");
			cLogger.info("保存非标准返回报文完毕！");
			
			try {
				byte[] ttOutBytes = getOutBytes(mOutNoStdXml);
				cSocket.getOutputStream().write(ttOutBytes);
			} catch (Exception ex) {
				cLogger.error("向银行发送返回报文失败！", ex);
			}
			
			try {
				cSocket.close();
				cLogger.debug("Socket已关闭！");
			} catch (IOException e) {
				cLogger.error("关闭Socket出错！", e);
			}
		}
		
		long mDealMillis = System.currentTimeMillis() - mStartMillis;
		cLogger.info("处理总耗时：" + mDealMillis/1000.0 + "s");
		
		cLogger.info("返回报文发送给银行完毕，交易结束！");
		cLogger.info("Out ICBCControl.run()!");
	}
	
	protected byte[] receive(InputStream pIs) throws Exception {
		cLogger.info("Into ICBCControl.receive()...");
		
		//处理报文头
		byte[] mHeadBytes = new byte[16];
		for (int tReadSize = 0; tReadSize < mHeadBytes.length;) {
			int tRead = pIs.read(mHeadBytes, tReadSize, mHeadBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文头出错！实际读入：" + new String(mHeadBytes));
			}
			tReadSize += tRead;
		}
		int mBodyLength = Integer.parseInt(new String(mHeadBytes, 0, 6).trim());
		cLogger.debug("请求报文长度：" + mBodyLength);
		cBaseInfoMap.put("FuncFlag", new String(mHeadBytes, 6, 4).trim());
		cLogger.info("交易代码：" + cBaseInfoMap.get("FuncFlag"));
		cBaseInfoMap.put("InsuID", new String(mHeadBytes, 10, 6).trim());
		cLogger.debug("保险公司代码：" + cBaseInfoMap.get("InsuID"));
		
		//处理报文体
		byte[] mBodyBytes = new byte[mBodyLength];
		for (int tReadSize = 0; tReadSize < mBodyBytes.length;) {
			int tRead = pIs.read(mBodyBytes, tReadSize, mBodyBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文体出错！实际读入长度为：" + tReadSize);
			}
			tReadSize += tRead;
		}
		
		//保存原始报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(new java.rmi.server.UID().toString().replaceAll(":", ""))
			.append("_").append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".txt");
		SaveMessage.save(mBodyBytes, mSaveName.toString(), "original");
		cLogger.info("保存原始请求报文完毕！文件名：" + mSaveName);
		
		cLogger.info("开始解密...");
		byte[] mClearBytes = cDESCipher.decode(mBodyBytes);
		cLogger.info("解密成功！");
		
		cLogger.info("Out ICBCControl.receive()!");
		return mClearBytes;
	}
	
	protected Document getInNoStdXml(byte[] pBytes) throws Exception{
		cLogger.info("Into ICBCControl.getInNoStdXml()...");
		
		Document mXmlDoc = JdomUtil.build(pBytes);
		
		Element mTXLifeRequest = mXmlDoc.getRootElement().getChild(TXLifeRequest);
		if (null != mTXLifeRequest) {	//工行密钥更换交易没有此节点
			cBaseInfoMap.put("TransNo", mTXLifeRequest.getChildTextTrim(TransRefGUID));
		}
		cLogger.info("交易流水号：" + cBaseInfoMap.get("TransNo"));
		
		//保存非标准请求报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(cBaseInfoMap.get("TransNo")).append("_")
			.append(cBaseInfoMap.get("FuncFlag")).append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".xml");
		SaveMessage.save(mXmlDoc, mSaveName.toString(), "InNoStd");
		cLogger.info("保存非标准请求报文完毕！");
		
		cLogger.info("Out ICBCControl.getInNoStdXml()!");
		return mXmlDoc;
	}
	
	protected Document getErrorXml(String pErrorMsg) {
		cLogger.info("Into ICBCControl.getErrorXml()...");
		
		Element mResultInfoDesc = new Element(ResultInfoDesc);
		mResultInfoDesc.addContent(pErrorMsg);
		
		Element mResultInfo = new Element(ResultInfo);
		mResultInfo.addContent(mResultInfoDesc);
		
		Element mResultCode = new Element(ResultCode);
		mResultCode.addContent("1001");
		
		Element mTransResult = new Element(TransResult);
		mTransResult.addContent(mResultCode);
		mTransResult.addContent(mResultInfo);
		
		Element mTransRefGUID = new Element(TransRefGUID);
		mTransRefGUID.addContent((String) cBaseInfoMap.get("TransrNo"));
		
		Element mTransType = new Element(TransType);
		mTransType.addContent((String) cBaseInfoMap.get("FuncFlag"));
		
		Element mTransExeDate = new Element(TransExeDate);
		mTransExeDate.addContent(DateUtil.getCurrentDate("yyyy-MM-dd"));
		
		Element mTransExeTime = new Element(TransExeTime);
		mTransExeTime.addContent(DateUtil.getCurrentDate("HH:mm:ss"));
		
		Element mTXLifeResponse = new Element(TXLifeResponse);
		mTXLifeResponse.addContent(mTransRefGUID);
		mTXLifeResponse.addContent(mTransType);
		mTXLifeResponse.addContent(mTransExeDate);
		mTXLifeResponse.addContent(mTransExeTime);
		mTXLifeResponse.addContent(mTransResult);
		
		Element mTXLife = new Element(TXLife);
		mTXLife.addContent(mTXLifeResponse);
		
		cLogger.info("Out ICBCControl.getErrorXml()!");
		return new Document(mTXLife);
	}
	
	private Document keyChange(Document pXmlDoc) throws Exception {
		cLogger.info("Into ICBCControl.keyChange()...");
		
		Element mDesKeyNotifyRequest = pXmlDoc.getRootElement();
		String mTransRefGUID = mDesKeyNotifyRequest.getChildTextTrim(TransRefGUID);
		String mDesKey = mDesKeyNotifyRequest.getChildTextTrim(DesKey);
		
		String mResultCode = "0000";
		String mResultMsg = "";
		try {
			if(16 != mDesKey.length()) {
				throw new BaseException("密钥长度不是16！" + mDesKey);
			}
			
			int mPort = cSocket.getLocalPort();
			String mKeyPath = getClass().getResource(
					"/com/sinosoft/midplat/channel/security/" + mPort + "/").getPath();
			FileOutputStream mKeyFos = new FileOutputStream(mKeyPath + mPort + ".dat");
			FileOutputStream mBackupFos = new FileOutputStream(mKeyPath + mTransRefGUID + ".dat");
			mKeyFos.write(mDesKey.getBytes());
			mKeyFos.close();
			mBackupFos.write(mDesKey.getBytes());
			mBackupFos.close();
			cLogger.info("密钥更新成功！");
		} catch (Exception ex) {
			mResultCode = "0001";
			mResultMsg = "更新密钥出错！" + ex.toString();
			cLogger.error("更新密钥出错！", ex);
		}
		
		StringBuffer mResultXml = new StringBuffer()
			.append("<?xml version=\"1.0\" encoding=\"GBK\" ?>")
			.append("<DesKeyNotifyResponse><TransRefGUID>").append(mTransRefGUID)
			.append("</TransRefGUID><TransExeDate>").append(DateUtil.getCurrentDate("yyyy-MM-dd"))
			.append("</TransExeDate><TransExeTime>").append(DateUtil.getCurrentDate("HH:mm:ss"))
			.append("</TransExeTime><TransNo>0001</TransNo>") 
			.append("<TransResult><ResultCode>").append(mResultCode)
			.append("</ResultCode><ResultInfo><ResultInfoDesc>").append(mResultMsg)
			.append("</ResultInfoDesc></ResultInfo></TransResult></DesKeyNotifyResponse>");
		
		Document mOutXml = JdomUtil.build(
				mResultXml.toString().getBytes("GBK"));
		
		cLogger.info("Out ICBCControl.keyChange()!");
		return mOutXml;
	}

	protected byte[] getOutBytes(Document pXmlDoc) throws Exception {
		cLogger.info("Into ICBCControl.getOutBytes()...");
		
		byte[] mBodyBytes = JdomUtil.toBytes(pXmlDoc);
		
		cLogger.info("开始加密...");
		byte[] mCipherBytes = cDESCipher.encode(mBodyBytes);
		cLogger.info("加密成功！");
		
		byte[] mTotalBytes = new byte[mCipherBytes.length + 16];
		
		//报文体长度
		String mLengthString = String.valueOf(mCipherBytes.length);
		cLogger.info("返回报文长度：" + mLengthString);
		byte[] mLengthBytes = mLengthString.getBytes();
//		System.arraycopy(mLengthBytes, 0, mTotalBytes, 0, mLengthBytes.length);
		System.arraycopy(mLengthBytes, 0, mTotalBytes, 0, mLengthBytes.length);
		System.arraycopy("      ".getBytes(), 0, mTotalBytes, mLengthBytes.length,6-mLengthBytes.length);
		//交易代码
		byte[] mFuncFlagBytes = ((String) cBaseInfoMap.get("FuncFlag")).getBytes();
//		System.arraycopy(mFuncFlagBytes, 0, mTotalBytes, 6, mFuncFlagBytes.length);
		System.arraycopy(mFuncFlagBytes, 0, mTotalBytes, 6, mFuncFlagBytes.length);
		System.arraycopy("      ".getBytes(), 0, mTotalBytes, 6+mFuncFlagBytes.length,4-mFuncFlagBytes.length);
		//公司代码
		byte[] mInsuIDBytes = ((String) cBaseInfoMap.get("InsuID")).getBytes();
//		System.arraycopy(mInsuIDBytes, 0, mTotalBytes, 10, mInsuIDBytes.length);
		System.arraycopy(mInsuIDBytes, 0, mTotalBytes, 10, mInsuIDBytes.length);
		System.arraycopy("      ".getBytes(), 0, mTotalBytes, 10+mInsuIDBytes.length,6-mInsuIDBytes.length);
		
		System.arraycopy(mCipherBytes, 0, mTotalBytes, 16, mCipherBytes.length);
		
		cLogger.info("Out ICBCControl.getOutBytes()!");
		return mTotalBytes;
	}
}
