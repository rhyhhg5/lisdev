package com.sinosoft.midplat.channel.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ResourceBundle;

import org.jdom.Document;

import com.sinosoft.midplat.channel.config.ConfigFactory;

public class SaveMessage {	
	public static void save(Document pXmlDoc, String pName, String pType) {
		ResourceBundle mPreConfig = ConfigFactory.getPreConfig();
		
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(mPreConfig.getString("YBT_HOME")).append('/')
					.append("FileContent").append('/')
					.append(DateUtil.getCurrentDate("yyyy")).append('/')
					.append(DateUtil.getCurrentDate("yyyyMM")).append('/')
					.append(DateUtil.getCurrentDate("yyyyMMdd")).append('/')
					.append(pType).append('/');
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString() + pName);
			JdomUtil.output(pXmlDoc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void save(byte[] pBytes, String pName, String pType) {
		ResourceBundle mPreConfig = ConfigFactory.getPreConfig();
		
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(mPreConfig.getString("YBT_HOME")).append('/')
					.append("FileContent").append('/')
					.append(DateUtil.getCurrentDate("yyyy")).append('/')
					.append(DateUtil.getCurrentDate("yyyyMM")).append('/')
					.append(DateUtil.getCurrentDate("yyyyMMdd")).append('/')
					.append(pType).append('/');
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}
		
		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString() + pName);
			tFos.write(pBytes);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
