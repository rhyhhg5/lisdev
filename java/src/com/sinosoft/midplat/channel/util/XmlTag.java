package com.sinosoft.midplat.channel.util;

public interface XmlTag {
	String TranData = "TranData";

	String BaseInfo = "BaseInfo";

	String BankDate = "BankDate";
	String BankCode = "BankCode";
	String ZoneNo = "ZoneNo";
	String BrNo = "BrNo";
	String TellerNo = "TellerNo";
	String TransrNo = "TransrNo";
	String FunctionFlag = "FunctionFlag";
	String InsuID = "InsuID";

	String LCCont = "LCCont";

	String PrtNo = "PrtNo";
	String ProposalContNo = "ProposalContNo";
	String ContNo = "ContNo";
	String OperType = "OperType";
	String Prem = "Prem";
	String Password = "Password";
	String TranDate = "TranDate";
	String EdorPrtNo = "EdorPrtNo";
	String PayType = "PayType";
	String PayIntv = "PayIntv";
	String PayMode = "PayMode";
	String AccBankCode = "AccBankCode";
	String BankAccNo = "BankAccNo";
	String AccName = "AccName";
	String AgentCode = "AgentCode";
	String AgentName = "AgentName";
	String AgentGroup = "AgentGroup";
	String CValiDate = "CValiDate";
	String PrintDate = "PrintDate";
	String SumPrem = "SumPrem";
	String ComLocation = "ComLocation";
	String ComPhone = "ComPhone";
	String SpecialDesc = "SpecialDesc";
	String SpecContent = "SpecContent";

	String LPAppnt = "LPAppnt";
	String LCAppnt = "LCAppnt";

	String AppntNo = "AppntNo";
	String AppntName = "AppntName";
	String AppntIDNo = "AppntIDNo";
	String AppntIDType = "AppntIDType";
	String AppntSex = "AppntSex";
	String AppntBirthday = "AppntBirthday";
	String HomeAddress = "HomeAddress";
	String HomeZipCode = "HomeZipCode";
	String MailAddress = "MailAddress";
	String MailZipCode = "MailZipCode";
	String AppntPhone = "AppntPhone";
	String AppntFax = "AppntFax";
	String AppntOfficePhone = "AppntOfficePhone";
	String AppntEMail = "AppntEMail";
	String AppntMobile = "AppntMobile";
	String Email = "Email";
	String JobCode = "JobCode";
	
	String LCInsureds = "LCInsureds";

	String LCInsuredCount = "LCInsuredCount";
	String LCInsured = "LCInsured";

	String InsuredNo = "InsuredNo";
	String Name = "Name";
	String Sex = "Sex";
	String Birthday = "Birthday";
	String InsuAddess = "InsuAddess";
	String InsuPhone = "InsuPhone";
	String InsuMobile = "InsuMobile";
	String InsuCompanyPhone = "InsuCompanyPhone";
	String InsuZipCode = "InsuZipCode";
	String InsuEMail = "InsuEMail";
	String RelaToMain = "RelaToMain";
	String RelaToAppnt = "RelaToAppnt";
	String HomePhone = "HomePhone";
	String InsuredMobile = "InsuredMobile";

	String Risks = "Risks";

	String RiskCount = "RiskCount";
	String Risk = "Risk";

	String RiskCode = "RiskCode";
	String RiskName = "RiskName";
	String MainRiskCode = "MainRiskCode";
	String MainRiskName = "MainRiskName";
	String Amnt = "Amnt";
	String Mult = "Mult";
	String InsuYearFlag = "InsuYearFlag";
	String InsuYear = "InsuYear";
	String Years = "Years";
	String PayEndYearFlag = "PayEndYearFlag";
	String PayEndYear = "PayEndYear";
	String PayEndDate = "PayEndDate";
	String PayYears = "PayYears";
	String GetYearFlag = "GetYearFlag";
	String GetYear = "GetYear";
	String SupplementaryPrem = "SupplementaryPrem";
	
	String LCBnfs = "LCBnfs";
	
	String LCBnfCount = "LCBnfCount";
	String LCBnf = "LCBnf";
	
	String BnfType = "BnfType";
	String BnfNo = "BnfNo";
	String BnfGrade = "BnfGrade";
	String BelongToInsured = "BelongToInsured";
	String RelationToInsured = "RelationToInsured";
	String BnfLot = "BnfLot";

	String CashValues = "CashValues";
	
	String CashValueCount = "CashValueCount";
	String CashValue = "CashValue";
	
	String End = "End";
	String Cash = "Cash";
	
	String BonusValues = "BonusValues";
	
	String BonusValueCount = "BonusValueCount";
	String BonusValue = "BonusValue";
	
	String ConfirmInfo = "ConfirmInfo";

	String OKFlag = "OKFlag";

	String ChkDetails = "ChkDetails";
	
	String Message = "Message";
	String ChkDetailCount = "ChkDetailCount";
	String ChkDetail = "ChkDetail";
	
	String BankZoneCode = "BankZoneCode";
	String FuncFlag = "FuncFlag";
	String CardNo = "CardNo";
	String TranAmnt = "TranAmnt";
	String ConfirmFlag = "ConfirmFlag";
	
	String RetData = "RetData";

	String Flag = "Flag";
	String Desc = "Desc";

	String MainRiskPrem = "MainRiskPrem";
	String SubRiskPrem = "SubRiskPrem";
	String EdorNo = "EdorNo";
	String EdorValiDate = "EdorValiDate";
	String IDNo = "IDNo";
	String IDType = "IDType";
	String FeeRela = "FeeRela";
	String FeeCost = "FeeCost";
	String FeeActu = "FeeActu";
	String OperCode = "OperCode";
	String OperName = "OperName";

	String ManageCom = "ManageCom";
	String AgentCom = "AgentCom";
	String ComCode = "ComCode";
	
	/*中行******************/
	String BOC_URI = "www.bocsoft.com.cn";
	String onLineUnderWrite = "onLineUnderWrite";
	String onLineUnderWriteResponse = "onLineUnderWriteResponse";
	String onLineAuditPolicy = "onLineAuditPolicy";
	String onLineAuditPolicyResponse = "onLineAuditPolicyResponse";
	String onLineCancel = "onLineCancel";
	String onLineCancelResponse = "onLineCancelResponse";
	String onLineReverse = "onLineReverse";
	String onLineReverseResponse = "onLineReverseResponse";
	String onLineSendMatch = "onLineSendMatch";
	String onLineSendMatchResponse = "onLineSendMatchResponse";
	String ProposalState = "ProposalState";
	
	/*soap******************/
	String SOAP_URI = "http://schemas.xmlsoap.org/soap/envelope/";
	String Fault = "Fault";
	String faultcode = "faultcode";
	String faultstring = "faultstring";
	
	/*工行******************/
	String TXLife = "TXLife";
	
	String TXLifeRequest = "TXLifeRequest";
	
	String TransRefGUID = "TransRefGUID";
	String TransType = "TransType";
	String TransExeDate = "TransExeDate";
	String TransExeTime = "TransExeTime";
	
	String OLifE = "OLifE";
	
	String Holding = "Holding";
	
	String Policy = "Policy";
	
	
	
	String ApplicationInfo = "ApplicationInfo";
	
	String HOAppFormNumber = "HOAppFormNumber";
	
	String Life = "Life";
	
	String CoverageCount = "CoverageCount";
	
	String Coverage = "Coverage";
	
	String ProductCode = "ProductCode";
	String IntialNumberOfUnits = "IntialNumberOfUnits";
	
	String Party = "Party";
	
	String FullName = "FullName";
	String Person = "Person";
	
	String Gender = "Gender";
	String BirthDate = "BirthDate";
	
	String GovtIDTC = "GovtIDTC";
	String GovtID = "GovtID";
	
	String Address = "Address";
	
	String AddressTypeCode = "AddressTypeCode";
	String Line1 = "Line1";
	String Zip = "Zip";
	
	String EMailAddress = "EMailAddress";
	String AddrLine = "AddrLine";
	
	String Phone = "Phone";
	
	String PhoneTypeCode = "PhoneTypeCode";
	String DialNumber = "DialNumber";
	
	String Relation = "Relation";
	
	String OriginatingObjectType = "OriginatingObjectType";
	String RelatedObjectType = "RelatedObjectType";
	String RelationRoleCode = "RelationRoleCode";
	
	String FormInstance = "FormInstance";
	
	String FormName = "FormName";
	String ProviderFormNumber = "ProviderFormNumber";
	
	String OLifEExtension = "OLifEExtension";
	
	String CarrierCode = "CarrierCode";
	String RegionCode = "RegionCode";
	String Branch = "Branch";
	String Teller = "Teller";
	String TransNo = "TransNo";
	String SpecialInfo = "SpecialInfo";
	String SourceType = "SourceType";
	
	String DesKey = "DesKey";
	
	String TXLifeResponse = "TXLifeResponse";
	
	String TransResult = "TransResult";
	
	String ResultCode = "ResultCode";
	String ResultInfo = "ResultInfo";
	String ResultInfoDesc = "ResultInfoDesc";
	
	String PolNumber = "PolNumber";
	String PlanName = "PlanName";
	String PaymentMode = "PaymentMode";
	String PaymentMethod = "PaymentMethod";
	String FinalPaymentDate = "FinalPaymentDate";
	String PaymentAmt = "PaymentAmt";
	
	String FaceAmt = "FaceAmt";
	String LifeCovTypeCode = "LifeCovTypeCode";
	String IndicatorCode = "IndicatorCode";
	String InitCovAmt = "InitCovAmt";
	String ModalPremAmt = "ModalPremAmt";
	String EffDate = "EffDate";
	
	String PaymentDurationMode = "PaymentDurationMode";
	String PaymentDuration = "PaymentDuration";
	String DurationMode = "DurationMode";
	String Duration = "Duration";
	
	String PaymentAmtText = "PaymentAmtText";
	
	/*建行******************/
	String Transaction = "Transaction";
	
	String Transaction_Header = "Transaction_Header";
	
	String LiBankID = "LiBankID";
	String PbInsuId = "PbInsuId";
	String BkPlatSeqNo = "BkPlatSeqNo";
	String BkTxCode = "BkTxCode";
	String BkChnlNo = "BkChnlNo";
	String BkBrchNo = "BkBrchNo";
	String BkTellerNo = "BkTellerNo";
	String BkPlatDate = "BkPlatDate";
	String BkPlatTime = "BkPlatTime";
	
	String Tran_Response = "Tran_Response";
	
	String BkOthDate = "BkOthDate";
	String BkOthSeq = "BkOthSeq";
	String BkOthRetCode = "BkOthRetCode";
	String BkOthRetMsg = "BkOthRetMsg";
}

