/**
 * 此类当前仅支持JDOM 1.1
 */

package com.sinosoft.midplat.channel.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.xml.sax.InputSource;

public class JdomUtil {
	/**
	 * 采用GBK编码构建一个Document对象，忽略标签之间的空字符(空格、换行、制表符等)。
	 * 构建失败，返回null。
	 */
	public static Document build(byte[] pBytes) {
		return build(pBytes, "GBK");
	}
	
	/**
	 * 采用GBK编码构建一个Document对象，忽略标签之间的空字符(空格、换行、制表符等)。
	 * 构建失败，返回null。
	 */
	public static Document build(InputStream pIs) {
		return build(pIs, "GBK");
	}
	
	/**
	 * 采用指定字符集编码构建一个Document对象，忽略标签之间的空字符(空格、换行、制表符等)。
	 * 构建失败，返回null。
	 */
	public static Document build(byte[] pBytes, String pCharset) {
		return build(new ByteArrayInputStream(pBytes), pCharset);
	}
	
	/**
	 * 采用指定字符集编码构建一个Document对象，忽略标签之间的空字符(空格、换行、制表符等)。
	 * 构建失败，返回null。
	 */
	public static Document build(InputStream pIs, String pCharset) {
		try {
			InputSource mInputSource = new InputSource(pIs);
			mInputSource.setEncoding(pCharset);
		
			SAXBuilder mSAXBuilder = new SAXBuilder();
			mSAXBuilder.setIgnoringBoundaryWhitespace(true);
			return mSAXBuilder.build(mInputSource);
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 将Document输出到指定的输出流。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进2空格。
	 * 注意：此方法不自动关闭流，如有需要，请在调用后手动关闭。
	 */
	public static void output(Document pXmlDoc, OutputStream pOs) throws IOException {
		Format mFormat = Format.getPrettyFormat().setEncoding("GBK");
		XMLOutputter mXMLOutputter = new XMLOutputter(mFormat);
		mXMLOutputter.output(pXmlDoc, pOs);
	}
	
	/**
	 * 将Element输出到指定的输出流。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进2空格。
	 * 注意：此方法不自动关闭流，如有需要，请在调用后手动关闭。
	 */
	public static void output(Element pElement, OutputStream pOs) throws IOException {
		Format mFormat = Format.getPrettyFormat().setEncoding("GBK");
		XMLOutputter mXMLOutputter = new XMLOutputter(mFormat);
		mXMLOutputter.output(pElement, pOs);
	}
	
	/**
	 * 将Document打印到控制台。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进2空格。
	 */
	public static void print(Document pXmlDoc) {
		try {
			output(pXmlDoc, System.out);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * 将Element打印到控制台。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进2空格。
	 */
	public static void print(Element pElement) {
		try {
			output(pElement, System.out);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * 将Document转换为GBK编码的字节数组，去首尾空格，中间连续多个空格转换为一个。
	 */
	public static byte[] toBytes(Document pXmlDoc) throws IOException {
		return toBytes(pXmlDoc, "GBK");
	}
	
	/**
	 * 将Element转换为GBK编码的字节数组，去首尾空格，中间连续多个空格转换为一个。
	 */
	public static byte[] toBytes(Element pElement) throws IOException {
		return toBytes(pElement, "GBK");
	}
	
	/**
	 * 将Document转换为指定字符集编码的字节数组，去首尾空格，中间连续多个空格转换为一个。
	 */
	public static byte[] toBytes(Document pXmlDoc, String pCharset) throws IOException {
		Format mFormat = Format.getCompactFormat().setEncoding(pCharset);
		XMLOutputter mXMLOutputter = new XMLOutputter(mFormat);
		ByteArrayOutputStream mBaos = new ByteArrayOutputStream();
		mXMLOutputter.output(pXmlDoc, mBaos);
		return mBaos.toByteArray();
	}
	
	/**
	 * 将Element转换为指定字符集编码的字节数组，去首尾空格，中间连续多个空格转换为一个。
	 */
	public static byte[] toBytes(Element pElement, String pCharset) throws IOException {
		Format mFormat = Format.getCompactFormat().setEncoding(pCharset);
		XMLOutputter mXMLOutputter = new XMLOutputter(mFormat);
		ByteArrayOutputStream mBaos = new ByteArrayOutputStream();
		mXMLOutputter.output(pElement, mBaos);
		return mBaos.toByteArray();
	}
}
