package com.sinosoft.midplat.channel.util;

import java.io.File;
import java.io.FileNotFoundException;

public final class SysInfo {
	private final static Class cThisClass = SysInfo.class;
	
	public final static String ClassBasePath = cThisClass.getResource("/").getPath();
	
	public static String getRealPath(String mPath) {
		mPath = mPath.replace('\\', '/');	//注意此处，一定要为"\\\\"
		return new File(ClassBasePath).toURI().resolve(mPath).getPath();
	}
	
	public static String getCurPath(Class pClass) {
		return pClass.getResource(".").getPath();
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		System.out.println(getRealPath("..\\a/b/c\\d//t"));
	}
}
