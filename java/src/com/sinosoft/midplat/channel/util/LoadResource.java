package com.sinosoft.midplat.channel.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.transform.XSLTransformer;

public class LoadResource {
	private final static Class cThisClass = LoadResource.class;
	
	private final static Logger cLogger = Logger.getLogger(cThisClass);
	
	public static Document loadXml(String pResourcePath) {
		return loadXml(pResourcePath, "GBK");
	}
	
	/**
	 * 加载指定的xsl，使用指定编码，解析为XSLTransformer，并返回。
	 * 加载出现异常，返回null。
	 */
	public static Document loadXml(String pResourcePath, String pCharset) {
		InputStream mXmlIs = cThisClass.getResourceAsStream(pResourcePath);
		return JdomUtil.build(mXmlIs, pCharset);
	}
	
	/**
	 * 加载指定的xsl，使用GBK编码，解析为XSLTransformer，并返回。
	 * 加载出现异常，返回null。
	 */
	public static XSLTransformer loadXsl(String pResourcePath) {
		return loadXsl(pResourcePath, "GBK");
	}
	
	/**
	 * 加载指定的xsl，使用指定编码，解析为XSLTransformer，并返回。
	 * 加载出现异常，返回null。
	 */
	public static XSLTransformer loadXsl(String pResourcePath, String pCharset) {
		XSLTransformer mXSLTransformer = null;
		
		try {
			InputStream tXslIs = cThisClass.getResourceAsStream(pResourcePath);
			InputStreamReader tXslIsr = new InputStreamReader(tXslIs, pCharset);
			mXSLTransformer = new XSLTransformer(tXslIsr);
		} catch (Exception ex) {
			cLogger.error("加载" + pResourcePath + "失败！", ex);
		}
		
		return mXSLTransformer;
	}
	
	public static Document loadXmlByPath(String pPath) {
		return loadXmlByPath(pPath, "GBK");
	}
	
	public static Document loadXmlByPath(String pPath, String pCharset) {
		InputStream mXmlIs = null;
		try {
			mXmlIs = new FileInputStream(SysInfo.getRealPath(pPath));
		} catch (FileNotFoundException ex) {
			cLogger.error("路径有误，未找到指定文件！", ex);
		}
		return JdomUtil.build(mXmlIs, pCharset);
	}
	
	public static XSLTransformer loadXslByPath(String pPath) {
		return loadXslByPath(pPath, "GBK");
	}
	
	public static XSLTransformer loadXslByPath(String pPath, String pCharset) {
		XSLTransformer mXSLTransformer = null;
		
		try {
			InputStream tXslIs = new FileInputStream(SysInfo.getRealPath(pPath));
			InputStreamReader tXslIsr = new InputStreamReader(tXslIs, pCharset);
			mXSLTransformer = new XSLTransformer(tXslIsr);
		} catch (Exception ex) {
			cLogger.error("加载" + pPath + "失败！", ex);
		}
		
		return mXSLTransformer;
	}
}
