package com.sinosoft.midplat.channel;

import java.io.InputStream;
import java.net.Socket;

import org.jdom.Document;

import com.sinosoft.midplat.channel.exception.BaseException;
import com.sinosoft.midplat.channel.util.DateUtil;
import com.sinosoft.midplat.channel.util.JdomUtil;
import com.sinosoft.midplat.channel.util.SaveMessage;

public class BCMControl extends BaseControl {
	public BCMControl(Socket pSocket) {
		super(pSocket);
		
		cBaseInfoMap.put("BankName", "bcm");
	}
	
	protected byte[] receive(InputStream pIs) throws Exception {
		cLogger.info("Into BCMControl.receive()...");
		
		//处理报文头
		byte[] mHeadBytes = new byte[6];
		for (int tReadSize = 0; tReadSize < mHeadBytes.length;) {
			int tRead = pIs.read(mHeadBytes, tReadSize, mHeadBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文头出错！实际读入：" + new String(mHeadBytes));
			}
			tReadSize += tRead;
		}
		int mBodyLength = Integer.parseInt(new String(mHeadBytes).trim());
		cLogger.debug("请求报文长度：" + mBodyLength);
		
		//处理报文体
		byte[] mBodyBytes = new byte[mBodyLength];
		for (int tReadSize = 0; tReadSize < mBodyBytes.length;) {
			int tRead = pIs.read(mBodyBytes, tReadSize, mBodyBytes.length-tReadSize);
			cLogger.debug("本次读取：" + tRead);
			if (-1 == tRead) {
				throw new BaseException("获取报文体出错！实际读入长度为：" + tReadSize);
			}
			tReadSize += tRead;
		}
		
		//保存原始报文
		StringBuffer mSaveName = new StringBuffer();
		mSaveName.append(cBaseInfoMap.get("BankName")).append("_").append(new java.rmi.server.UID().toString().replaceAll(":", ""))
			.append("_").append(DateUtil.getCurrentDate("HHmmss")).append(".txt");
		SaveMessage.save(mBodyBytes, mSaveName.toString(), "original");
		cLogger.info("保存原始请求报文完毕！文件名：" + mSaveName);
		
		cLogger.info("Out BCMControl.receive()!");
		return mBodyBytes;
	}
	
	protected byte[] getOutBytes(Document pXmlDoc) throws Exception {
		cLogger.info("Into BCMControl.getOutBytes()...");
		
		byte[] mBodyBytes = JdomUtil.toBytes(pXmlDoc);
		
		byte[] mTotalBytes = new byte[mBodyBytes.length + 6];
		
		//报文体长度
		String mLengthString = String.valueOf(mBodyBytes.length);
		cLogger.info("返回报文长度：" + mLengthString);
		byte[] mLengthBytes = mLengthString.getBytes();
		System.arraycopy(mLengthBytes, 0, mTotalBytes, 0, mLengthBytes.length);

		System.arraycopy(mBodyBytes, 0, mTotalBytes, 6, mBodyBytes.length);
		
		cLogger.info("Out BCMControl.getOutBytes()!");
		return mTotalBytes;
	}
}