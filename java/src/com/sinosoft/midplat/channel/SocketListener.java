package com.sinosoft.midplat.channel;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.midplat.channel.config.ConfigFactory;

public class SocketListener implements ServletContextListener {
	private final static Logger cLogger = Logger.getLogger(SocketListener.class);
	
	private Server[] cServers = null;
	
	public static void main(String[] args) {
		new SocketListener().contextInitialized(null);
	}
	
	public void contextInitialized(ServletContextEvent pEvent) {
		cLogger.info("Into SocketListener.contextDestroyed()...");
		cLogger.info("SocketListener已启动！");
		
		Document mSocketConfigDoc = ConfigFactory.getSocketConfig();
		List mConfigList = mSocketConfigDoc.getRootElement().getChildren();
		cServers = new Server[mConfigList.size()];
		for (int i = 0; i < mConfigList.size(); i++) {
			try {
				Element ttSocketConfig = (Element) mConfigList.get(i);
				cLogger.info(i + "_SocketConfig: port=" + ttSocketConfig.getChildText("port")
						+ "; class=" + ttSocketConfig.getChildText("class"));
				int ttPort = Integer.parseInt(ttSocketConfig.getChildText("port"));
				String ttClassName = ttSocketConfig.getChildText("class");
				Server ttServer = new Server(ttPort, ttClassName);
				cServers[i] = ttServer;
				ttServer.start();
				cLogger.info(i + "_SocketConfig加载成功!");
			} catch (Throwable ex) {
				cLogger.error(i + "_SocketConfig加载失败!", ex);
			}
		}
		
		cLogger.info("Out SocketListener.contextInitialized()!");
	}
	
	public void contextDestroyed(ServletContextEvent pEvent) {
		cLogger.info("Into SocketListener.contextDestroyed()...");
		
		cLogger.info("开始关闭ServerSocket监听...");
		/**
		 * 一定要关闭ServerSocket，否则更新class时自动重启服务，会导致SocketListener重启加载，
		 * 重复监听同一个端口，抛出端口已占用异常(java.net.BindException)。
		 */
		for (int i = 0; i < cServers.length; i++) {
			/**
			 * 在加载socketConfig.xml中某项失败时，cServers中对应的值会为null，在此需过滤掉。
			 */
			if (null != cServers[i]) {
				cServers[i].close();
			}
		}
		
		cLogger.warn("SocketListener销毁！");
		cLogger.info("Out SocketListener.contextDestroyed()!");
	}
	
	private class Server extends Thread {
		private int cPort = -1;
		private Constructor cConstructor = null;
		private ServerSocket cServerSocket = null;
		
		private Logger cLogger = Logger.getLogger(getClass());
		
		public Server(int pPort, String pClassName) throws Exception {
			cPort = pPort;
			cConstructor = Class.forName(pClassName).getConstructor(
					new Class[]{Socket.class});
		}

		public void run() {
			try {
				cServerSocket = new ServerSocket(cPort);
				cLogger.info("监听端口 " + cServerSocket.getLocalPort());
			} catch (IOException ex) {
				cLogger.error("创建服务器套接字失败！" + cPort, ex);
				return;
			}
			
			for (; true;) {
				try {
					Socket ttSocket = cServerSocket.accept();
					Thread ttPreControl = (Thread) cConstructor.newInstance(
							new Object[]{ttSocket});
					ttPreControl.setName(
							ttSocket.getInetAddress().getHostAddress());
					ttPreControl.start();
				} catch (IOException ex) {
					cLogger.error("等待客户端连接时发生I/O错误！" + cServerSocket.getLocalPort(), ex);
					return;
				} catch (Throwable ex) {
					cLogger.error("加载本次请求的处理线程失败!", ex);
				}
			}
		}
		
		public void close() {
			try {
				cServerSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
