package com.sinosoft.midplat.channel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.List;

import org.apache.log4j.Logger;

import com.sinosoft.midplat.channel.config.ConfigCache;
import com.sinosoft.midplat.channel.reformat.ccb.XslCacheCCB;
import com.sinosoft.midplat.channel.reformat.icbc.XslCacheICBC;

public class LoadConfig extends Thread {
	private static Logger cLogger = Logger.getLogger(LoadConfig.class);
	
	private final Socket cSocket;
	
	public LoadConfig(Socket pSocket) {
		cSocket = pSocket;
	}
	
	public void run() {
		cLogger.info("Into BaseControl.run()...");
		
		BufferedReader mSocketBufReader = null;
		PrintStream mSocketPrtStream = null;
		
		try {
			mSocketBufReader = new BufferedReader(
					new InputStreamReader(cSocket.getInputStream(), "GBK"));
			
			mSocketPrtStream = new PrintStream(cSocket.getOutputStream(), true, "GBK");
			
			String tInCmd = 
				mSocketBufReader.readLine().toLowerCase().replace('\\', '/').replace('.', '/');
			
			if ("load -all".equalsIgnoreCase(tInCmd)) {	//加载所有缓存配置
				//加载公用(config)配置文件
				List tResultList = ConfigCache.initAll();
				for (int i = 0; i < tResultList.size(); i++) {
					mSocketPrtStream.println(tResultList.get(i));
				}
				mSocketPrtStream.println();
				
				//加载工行报文转换xsl文件
				tResultList = XslCacheICBC.initAll();
				for (int i = 0; i < tResultList.size(); i++) {
					mSocketPrtStream.println(tResultList.get(i));
				}
				mSocketPrtStream.println();
				
				//加载建行报文转换xsl文件
				tResultList = XslCacheCCB.initAll();
				for (int i = 0; i < tResultList.size(); i++) {
					mSocketPrtStream.println(tResultList.get(i));
				}
			} else if ("load -all config".equals(tInCmd)) {	//加载所有公用(config)配置文件
				List tResultList = ConfigCache.initAll();
				for (int i = 0; i < tResultList.size(); i++) {
					mSocketPrtStream.println(tResultList.get(i));
				}
			} else if ("load -all icbc.xsl".equals(tInCmd)) {	//加载所有工行报文转换xsl文件
				List tResultList = XslCacheICBC.initAll();
				for (int i = 0; i < tResultList.size(); i++) {
					mSocketPrtStream.println(tResultList.get(i));
				}
				mSocketPrtStream.println();
			} else if ("load -all ccb.xsl".equals(tInCmd)) {	//加载所有建行报文转换xsl文件
				List tResultList = XslCacheCCB.initAll();
				for (int i = 0; i < tResultList.size(); i++) {
					mSocketPrtStream.println(tResultList.get(i));
				}
			} else if (tInCmd.startsWith("load com/sinosoft/midplat/channel/config/")) {	//加载指定的某个公用(config)配置文件
				mSocketPrtStream.println(
						ConfigCache.init(tInCmd.substring(5)));
			} else if (tInCmd.startsWith("load com/sinosoft/midplat/channel/reformat/icbc/")) {	//加载指定的工行报文转换xsl文件
				mSocketPrtStream.println(
						XslCacheICBC.init(tInCmd.substring(5)));
			} else if (tInCmd.startsWith("load com/sinosoft/midplat/channel/reformat/ccb/")) {	//加载指定的建行报文转换xsl文件
				mSocketPrtStream.println(
						XslCacheCCB.init(tInCmd.substring(5)));
			} else {
				mSocketPrtStream.println("bad cmd!");
			}
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
			mSocketPrtStream.println(ex.toString());
		} finally {
			try {
				mSocketBufReader.close();
				mSocketPrtStream.close();
				cSocket.close();
				cLogger.debug("Socket已关闭！");
			} catch (IOException e) {
				cLogger.error("关闭Socket出错！", e);
			}
		}
		
		cLogger.info("Out BaseControl.run()!");
	}
}
