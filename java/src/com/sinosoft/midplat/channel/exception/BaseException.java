package com.sinosoft.midplat.channel.exception;

public class BaseException extends Exception {
	private static final long serialVersionUID = -3990760945741513477L;

	/**
	 * 异常的类型。
	 * 初步设想的类型(非预期异常，预期异常，逻辑报错)
	 * 暂未使用，以后可根据实际需求实现。
	 */
	private int cType = 0;
	
	/**
	 * 异常的严重级别。
	 * 暂未使用，以后可根据实际需求实现。
	 */
	private int cLevel = 0;
	
	/**
	 * 异常发生的位置。
	 * 用以定位在何处发生的异常，具体到方法(package.class.Method)。
	 * 暂未使用，以后可根据实际需求实现。
	 */
	private String cPosition = null;
	
	public BaseException(String pMessage) {
		super(pMessage);
	}
	
	public BaseException(Throwable pCause) {
		super(pCause);
	}
	
	public BaseException(String pMessage, Throwable pCause) {
		super(pMessage, pCause);
	}
	
	public int getType() {
		return cType;
	}
	
	public int getLevel() {
		return cLevel;
	}
	
	public String getPosition() {
		return cPosition;
	}
	
	public static void main(String[] args) {}
}
