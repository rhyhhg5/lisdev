/**
 * 生成配置文件对象。
 * 提供一种缓存机制，只在第一次调用时加载配置文件生成对应的配置文件实例，
 * 然后缓存该实例，后续调用直接返回该实例。
 */

package com.sinosoft.midplat.channel.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.midplat.channel.util.JdomUtil;

public class ConfigFactory {
	private static Logger cLogger = Logger.getLogger(ConfigFactory.class);
	
	private static Document cSocketConfig = null;	//端口-处理类 配置
	private static Document cFormatConfig = null;	//各银行各交易报文转换类配置
	private static Document cBalanceConfig = null;	//对账相关配置
	private static ResourceBundle cPreConfig = null;	//前置机通用配置
	
	/**
	 * 返回socketConfig.xml(端口-处理类 配置)对应实例。
	 * 加载出错，返回null。
	 */
	public static Document getSocketConfig() {
		if (null == cSocketConfig) {
			cLogger.debug("Start load socketConfig.xml...");
			InputStream tIs = ConfigFactory.class.getResourceAsStream(
					"/com/sinosoft/midplat/channel/config/socketConfig.xml");
			cSocketConfig = JdomUtil.build(tIs);
			try {
				tIs.close();
			} catch (IOException ex) {
				cLogger.error("关闭文件流失败(socketConfig.xml)！", ex);
			}
			cLogger.debug("End load socketConfig.xml!");
		}

		return cSocketConfig;
	}
	
	/**
	 * 返回formatConfig.xml(各银行各交易报文转换类配置)对应实例。
	 * 加载出错，返回null。
	 */
	public static Document getFormatConfig() {
		if (null == cFormatConfig) {
			cLogger.debug("Start load formatConfig.xml...");
			InputStream tIs = ConfigFactory.class.getResourceAsStream(
					"/com/sinosoft/midplat/channel/config/formatConfig.xml");
			cFormatConfig = JdomUtil.build(tIs);
			try {
				tIs.close();
			} catch (IOException ex) {
				cLogger.error("关闭文件流失败(formatConfig.xml)！", ex);
			}
			cLogger.debug("End load formatConfig.xml!");
		}

		return cFormatConfig;
	}
	
	/**
	 * 返回balanceConfig.xml(对账相关配置)对应实例。
	 * 加载出错，返回null。
	 */
	public static Document getBalanceConfig() {
		if (null == cFormatConfig) {
			cLogger.debug("Start load balanceConfig.xml...");
			InputStream tIs = ConfigFactory.class.getResourceAsStream(
					"/com/sinosoft/midplat/channel/config/balanceConfig.xml");
			cBalanceConfig = JdomUtil.build(tIs);
			try {
				tIs.close();
			} catch (IOException ex) {
				cLogger.error("关闭文件流失败(balanceConfig.xml)！", ex);
			}
			cLogger.debug("End load balanceConfig.xml!");
		}

		return cBalanceConfig;
	}
	
	/**
	 * 返回PreConfig.properties(前置机通用配置)对应实例。
	 * 加载出错，返回null。
	 */
	public static ResourceBundle getPreConfig() {
		if (null == cPreConfig) {
			cLogger.debug("Start load PreConfig.properties...");
			cPreConfig = ResourceBundle.getBundle(
					"com.sinosoft.midplat.channel.config.PreConfig");
			cLogger.debug("End load PreConfig.properties!");
		}

		return cPreConfig;
	}
}
