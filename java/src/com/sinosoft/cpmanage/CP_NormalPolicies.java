package com.sinosoft.cpmanage;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.ContInfo;
import com.cbsws.obj.ContList;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSentSchema;
import com.sinosoft.lis.vschema.LCContSentSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class CP_NormalPolicies extends ABusLogic{
	
	private String BatchNo = "";
	private String MsgType = "";
	private String SendDate = "";
	private String SendTime = "";
	
	private MsgHead mMsgHead = null;
	private ContInfo mContInfo = null;
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private SSRS mSSRS = null;
	private ContList[] mContListArray = null;
	
	protected boolean deal(MsgCollection cMsgInfos){
		
		System.out.println("开始解析云平台报送至核心的JG002报文...");
		if(!parseData(cMsgInfos)){
			return false;
		}
		System.out.println("报文解析完成。");
		
		System.out.println("执行数据校验...");
//		if(!checkData()){
//			return false;
//		}
		System.out.println("数据校验完成。");
		
		System.out.println("开始处理业务...");
		if(!dealData()){
			return false;
		}
		System.out.println("业务处理完成。");
		
		System.out.println("开始封装返回报文...");
		getXmlResult();
		System.out.println("返回报文封装完成。");
		
		return true;
	}
	
	private boolean parseData(MsgCollection cMsgInfos){
		//获取报文头
		mMsgHead = cMsgInfos.getMsgHead();
		if(mMsgHead==null){
			errLog("报文头信息缺失。");
            return false;
		}
		BatchNo = mMsgHead.getBatchNo();
		MsgType = mMsgHead.getMsgType();
		SendDate = mMsgHead.getSendDate();
		SendTime = mMsgHead.getSendTime();
		mGlobalInput.Operator = mMsgHead.getSendOperator();
		
		try{
			List tContList = cMsgInfos.getBodyByFlag("ContInfo");
			if(tContList==null || tContList.size()!=1){
				errLog("获取保单信息失败。");
				return false;
			}
			mContInfo = (ContInfo)tContList.get(0);
			mGlobalInput.ManageCom = mContInfo.getManageCom();
			mGlobalInput.ComCode = mContInfo.getManageCom();
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean checkData(){
		String tMsgType = MsgType;
		if(tMsgType==null || "".equals(tMsgType)){
			errLog("没有获得报文类型，或传来的报文类型为空。");
			return false;
		}
		if(!"JG002".equals(tMsgType)){
			errLog("传来的报文类型不是约定好的JG002。");
			return false;
		}
		String tRiskCode = mContInfo.getRiskCode();
		if(tRiskCode==null){
			errLog("获取产品代码失败。");
			return false;
		}
		if(!"333701".equals(tRiskCode) && !"334001".equals(tRiskCode)){
			errLog("传的产品代码不在约定内。");
			return false;
		}
		return true;
	}
	
	private boolean dealData(){
		//获取正常承保数据
		getNormalPolicies();
	
		if(mSSRS.MaxNumber<=0){
			errLog("没有查询到相关数据。");
			return false;
		}
		
		//将查询到的正常承保数据存储到LCContSent表中，SendType='JG002'，防止重复发送
		storeData();
		
		return true;
	}
	
	private void getNormalPolicies(){
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select distinct ");
		tSQL.append("lcc.managecom,(Select Name From Ldcom Where Comcode=lcc.Managecom)");
		tSQL.append(",db2inst1.codename('lcsalechnl',lcc.salechnl),lcc.contno");
		tSQL.append(",lcp.riskcode,(select riskname from lmrisk where riskcode=lcp.riskcode)");
		tSQL.append(",lcc.amnt,lcc.prem,lcc.cvalidate");
		tSQL.append(",lca.appntno,lca.appntname,lca.appntsex,lca.appntbirthday");
		tSQL.append(",lca.idtype,lca.idno");
		tSQL.append(",(select mobile from lcaddress where customerno=lca.appntno and addressno=lca.addressno)");
		tSQL.append(",lci.insuredno,lci.name,lci.sex,lci.birthday");
		tSQL.append(",lci.idtype,lci.idno");
		tSQL.append(",(select mobile from lcaddress where customerno=lca.appntno and addressno=lca.addressno)");
		tSQL.append(",laa.groupagentcode,laa.name,laa.sex,laa.birthday");
		tSQL.append(",laa.idnotype,laa.idno,laa.mobile ");
		tSQL.append("from lccont lcc ");
		tSQL.append("inner join lcappnt lca on lcc.prtno = lca.prtno ");
		tSQL.append("inner join lcinsured lci on lcc.prtno = lci.prtno ");
		tSQL.append("inner join lcpol lcp on lci.prtno = lcp.prtno and lci.insuredno = lcp.insuredno ");
		tSQL.append("inner join laagent laa on lcc.agentcode = laa.agentcode ");
		tSQL.append("where 1=1 ");
		tSQL.append("and exists (select 1 from ldcode where codetype='CP_salechnl' and code = lcc.salechnl) ");
		tSQL.append("and lcc.conttype='1' and lcc.appflag='1' ");
		tSQL.append("and lcc.prem>=20000.00 ");
		tSQL.append("and exists (select 1 from ldcode where codetype='CP_riskcode' and code = lcp.riskcode) ");
		String tManageCom = mContInfo.getManageCom();
		if(tManageCom==null){
			tSQL.append("and lcc.managecom like '86%' ");
		}else{
			tSQL.append("and lcc.managecom like '"+tManageCom+"%' ");
		}
		String tCvaliDateStart = mContInfo.getCvaliDateStart();
		if(tCvaliDateStart!=null){
			tSQL.append("and lcc.cvalidate >= '"+tCvaliDateStart+"' ");
		}
		String tCvaliDateEnd = mContInfo.getCvaliDateEnd();
		if(tCvaliDateStart!=null){
			tSQL.append("and lcc.cvalidate <= '"+tCvaliDateEnd+"' ");
		}
		tSQL.append("and not exists (select 1 from lccontsent lcs where lcs.contno=lcc.contno and sendtype='JG002') ");
		mSSRS = new ExeSQL().execSQL(tSQL.toString());
		if(mSSRS != null){
			mContListArray = new ContList[mSSRS.MaxRow];
			for(int i=1; i<=mSSRS.MaxRow; i++){
				ContList tContList = new ContList();
				tContList.setComCode(mSSRS.GetText(i,1));
				tContList.setComName(mSSRS.GetText(i,2));
				tContList.setSalChannel(mSSRS.GetText(i,3));
				tContList.setContNo(mSSRS.GetText(i,4));
				tContList.setRiskCode(mSSRS.GetText(i,5));
				tContList.setRiskName(mSSRS.GetText(i,6));
				tContList.setAmnt(mSSRS.GetText(i,7));
				tContList.setPrem(mSSRS.GetText(i,8));
				tContList.setCValiDate(mSSRS.GetText(i,9));
				tContList.setAppntNo(mSSRS.GetText(i,10));
				tContList.setAppntName(mSSRS.GetText(i,11));
				tContList.setAppntSex(mSSRS.GetText(i,12));
				tContList.setAppntBirthday(mSSRS.GetText(i,13));
				tContList.setAppntIDType(mSSRS.GetText(i,14));
				tContList.setAppntIDNo(mSSRS.GetText(i,15));
				tContList.setAppntMobile(mSSRS.GetText(i,16));
				tContList.setInsuredNo(mSSRS.GetText(i,17));
				tContList.setInsuredNname(mSSRS.GetText(i,18));
				tContList.setInsuredSex(mSSRS.GetText(i,19));
				tContList.setInsuredBirthday(mSSRS.GetText(i,20));
				tContList.setInsuredIDType(mSSRS.GetText(i,21));
				tContList.setInsuredIDNo(mSSRS.GetText(i,22));
				tContList.setInsuredMobile(mSSRS.GetText(i,23));
				tContList.setAgentCode(mSSRS.GetText(i,24));
				tContList.setAgentName(mSSRS.GetText(i,25));
				tContList.setAgentSex(mSSRS.GetText(i,26));
				tContList.setAgentBirthday(mSSRS.GetText(i,27));
				tContList.setAgentIDType(mSSRS.GetText(i,28));
				tContList.setAgentIDNo(mSSRS.GetText(i,29));
				tContList.setAgentMobile(mSSRS.GetText(i,30));
				
				mContListArray[i-1] = tContList;
			}
		}
	}
	
	private void storeData(){
		LCContSentSet tLCContSentSet = new LCContSentSet();
		for(int i=1; i<=mSSRS.MaxRow; i++){
			LCContSentSchema tLCContSentSchema = new LCContSentSchema();
			tLCContSentSchema.setContNo(mSSRS.GetText(i,4));
			tLCContSentSchema.setSendType("JG002");
			tLCContSentSchema.setBatchNo(BatchNo);
			tLCContSentSchema.setManageCom(mSSRS.GetText(i,1));
			tLCContSentSchema.setSentDate(SendDate);
			tLCContSentSchema.setSentTime(SendTime);
			tLCContSentSchema.setMakeDate(PubFun.getCurrentDate());
			tLCContSentSchema.setMakeTime(PubFun.getCurrentTime());
			tLCContSentSchema.setModifyDate(PubFun.getCurrentDate());
			tLCContSentSchema.setModifyTime(PubFun.getCurrentTime());
			tLCContSentSet.add(tLCContSentSchema);
		}
		MMap tMMap = new MMap();
		tMMap.put(tLCContSentSet, SysConst.INSERT);
		VData tVData = new VData();
		tVData.add(tMMap);
		PubSubmit p = new PubSubmit();
		try{
			p.submitData(tVData, "");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void getXmlResult(){
		for(int i=0; i<mContListArray.length; i++){
			putResult("ContList",mContListArray[i]);
		}
	}
	
}
