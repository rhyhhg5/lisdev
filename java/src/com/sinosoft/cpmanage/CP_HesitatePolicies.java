package com.sinosoft.cpmanage;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CTList;
import com.cbsws.obj.ContInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSentSchema;
import com.sinosoft.lis.vschema.LCContSentSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class CP_HesitatePolicies extends ABusLogic{
	
	private String BatchNo = "";
	private String MsgType = "";
	private String SendDate = "";
	private String SendTime = "";
	
	private MsgHead mMsgHead = null;
	private ContInfo mContInfo = null;
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private SSRS mSSRS = null;
	private CTList[] mCTListArray = null;
	
	protected boolean deal(MsgCollection cMsgInfos){
		
		System.out.println("开始解析云平台报送至核心的JG003报文...");
		if(!parseData(cMsgInfos)){
			return false;
		}
		System.out.println("报文解析完成。");
		
		System.out.println("执行数据校验...");
//		if(!checkData()){
//			return false;
//		}
		System.out.println("数据校验完成。");
		
		System.out.println("开始处理业务...");
		if(!dealData()){
			return false;
		}
		System.out.println("业务处理完成。");
		
		System.out.println("开始封装返回报文...");
		getXmlResult();
		System.out.println("返回报文封装完成。");
		
		return true;
	}
	
	//解析报文,获得报文数据
	private boolean parseData(MsgCollection cMsgInfos){
		mMsgHead = cMsgInfos.getMsgHead();
		if(mMsgHead==null){
			errLog("报文头信息缺失。");
            return false;
		}
		BatchNo = mMsgHead.getBatchNo();
		MsgType = mMsgHead.getMsgType();
		SendDate = mMsgHead.getSendDate();
		SendTime = mMsgHead.getSendTime();
		mGlobalInput.Operator = mMsgHead.getSendOperator();
		
		try{
			List tContList = cMsgInfos.getBodyByFlag("ContInfo");
			if(tContList==null || tContList.size()!=1){
				errLog("获取保单信息失败。");
				return false;
			}
			mContInfo = (ContInfo)tContList.get(0);
			mGlobalInput.ManageCom = mContInfo.getManageCom();
			mGlobalInput.ComCode = mContInfo.getManageCom();
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	//检查报文某些节点中数据的正确性
	private boolean checkData(){
		String tMsgType = MsgType;
		if(tMsgType==null || "".equals(tMsgType)){
			errLog("没有获得报文类型，或传来的报文类型为空。");
			return false;
		}
		if(!"JG003".equals(tMsgType)){
			errLog("传来的报文类型不是约定好的JG003。");
			return false;
		}
		String tRiskCode = mContInfo.getRiskCode();
		if(tRiskCode==null){
			errLog("获取产品代码失败。");
			return false;
		}
		if(!"333701".equals(tRiskCode) && !"334001".equals(tRiskCode)){
			errLog("传的产品代码不在约定内。");
			return false;
		}
		return true;
	}
	
	//业务处理
	private boolean dealData(){
		//获取犹豫期退保数据
		getHesitationPolicies();
		
		if(mSSRS.MaxNumber<=0){
			errLog("没有查询到相关数据。");
			return false;
		}
		
		//将查询到的犹豫期退保数据存储到LCContSent表中，SendType='JG003'，防止重复发送
		storeData();
		
		return true;
	}
	
	//依据条件获取犹豫期退保数据
	private void getHesitationPolicies(){
		StringBuffer tSQL = new StringBuffer();
		tSQL.append("select lbc.contno,(select confdate from lpedorapp where edoracceptno=lbc.edorno),lbc.managecom ");
		tSQL.append("from lbcont lbc ,lbpol lbp ");
		tSQL.append("where lbc.contno=lbp.contno ");
		tSQL.append("and exists (select 1 from ldcode where codetype='CP_salechnl' and code = lbc.salechnl) ");
		tSQL.append("and lbc.conttype='1' ");
		tSQL.append("and lbc.prem>=20000.00 ");
		tSQL.append("and exists (select 1 from ldcode where codetype='CP_riskcode' and code = lbp.riskcode) ");
		String tManageCom = mContInfo.getManageCom();
		if(tManageCom==null){
			tSQL.append("and lbc.managecom like '86%' ");
		}else{
			tSQL.append("and lbc.managecom like '"+tManageCom+"%' ");
		}
		String tCvaliDateStart = mContInfo.getCvaliDateStart();
		if(tCvaliDateStart!=null){
			tSQL.append("and lbc.cvalidate >= '"+tCvaliDateStart+"' ");
		}
		String tCvaliDateEnd = mContInfo.getCvaliDateEnd();
		if(tCvaliDateStart!=null){
			tSQL.append("and lbc.cvalidate <= '"+tCvaliDateEnd+"' ");
		}
		tSQL.append("and not exists (select 1 from lccontsent lcs where lcs.contno=lbc.contno and sendtype='JG003') ");
		tSQL.append("and exists (select 1 from lpedoritem where edorno=lbc.edorno and edortype='WT') ");
		mSSRS = new ExeSQL().execSQL(tSQL.toString());
		if(mSSRS != null){
			mCTListArray = new CTList[mSSRS.MaxRow];
			for(int i=1; i<=mSSRS.MaxRow; i++){
				CTList tCTList = new CTList();
				tCTList.setContNo(mSSRS.GetText(i,1));
				tCTList.setCTDate(mSSRS.GetText(i,2));
				
				mCTListArray[i-1] = tCTList;
			}
		}
	}
	
	//将查询到的犹豫期退保数据存进LCContSent表中
	private void storeData(){
		LCContSentSet tLCContSentSet = new LCContSentSet();
		for(int i=1; i<=mSSRS.MaxRow; i++){
			LCContSentSchema tLCContSentSchema = new LCContSentSchema();
			tLCContSentSchema.setContNo(mSSRS.GetText(i,1));
			tLCContSentSchema.setSendType("JG003");
			tLCContSentSchema.setBatchNo(BatchNo);
			tLCContSentSchema.setManageCom(mSSRS.GetText(i,3));
			tLCContSentSchema.setSentDate(SendDate);
			tLCContSentSchema.setSentTime(SendTime);
			tLCContSentSchema.setMakeDate(PubFun.getCurrentDate());
			tLCContSentSchema.setMakeTime(PubFun.getCurrentTime());
			tLCContSentSchema.setModifyDate(PubFun.getCurrentDate());
			tLCContSentSchema.setModifyTime(PubFun.getCurrentTime());
			tLCContSentSet.add(tLCContSentSchema);
		}
		MMap tMMap = new MMap();
		tMMap.put(tLCContSentSet, SysConst.INSERT);
		VData tVData = new VData();
		tVData.add(tMMap);
		PubSubmit p = new PubSubmit();
		try{
			p.submitData(tVData, "");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//封装返回报文
	private void getXmlResult(){
		for(int i=0; i<mCTListArray.length; i++){
			putResult("CTList",mCTListArray[i]);
		}
	}
}
