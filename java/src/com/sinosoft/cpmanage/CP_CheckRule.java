package com.sinosoft.cpmanage;

import java.util.List;

import com.cbsws.core.obj.MsgHead;
import com.cbsws.core.xml.ctrl.ABusLogic;
import com.cbsws.core.xml.ctrl.MsgCollection;
import com.cbsws.obj.CheckInfo;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class CP_CheckRule extends ABusLogic{
	
	private MsgHead mMsgHead = null;
	private String BatchNo = "";
	private String MsgType = "";
	private String SendDate = "";
	private String SendTime = "";
	
	private CheckInfo mCheckInfo = null;
	private String mCheckType = "";
	private String mCardNo = "";
	private String mName = "";
	private String mCheckDate = "";
	
	
	protected boolean deal(MsgCollection cMsgInfos){
		
		System.out.println("开始解析云平台报送至核心的JG001报文...");
		if(!parseData(cMsgInfos)){
			return false;
		}
		System.out.println("报文解析完成。");
		
		System.out.println("执行数据校验...");
		if(!checkData()){
			return false;
		}
		System.out.println("数据校验完成。");

		System.out.println("开始处理业务...");
		if(!dealData()){
			return false;
		}
		System.out.println("业务处理完成。");
		
		System.out.println("开始封装返回报文...");
//		getXmlResult();
		System.out.println("返回报文封装完成。");
		
		return true;
	}
	
	private boolean parseData(MsgCollection cMsgInfos){
		//获取报文头
		mMsgHead = cMsgInfos.getMsgHead();
		if(mMsgHead==null){
			errLog("报文头信息缺失。");
            return false;
		}
		BatchNo = mMsgHead.getBatchNo();
		MsgType = mMsgHead.getMsgType();
		SendDate = mMsgHead.getSendDate();
		SendTime = mMsgHead.getSendTime();
		
		try{
			List tCheckList = cMsgInfos.getBodyByFlag("CheckInfo");
			if(tCheckList==null || tCheckList.size()!=1){
				errLog("获取保单信息失败。");
				return false;
			}
			mCheckInfo = (CheckInfo)tCheckList.get(0);
			mCheckType = mCheckInfo.getCheckType();
			mCardNo = mCheckInfo.getCardNo();
			mName = mCheckInfo.getName();
			mCheckDate = mCheckInfo.getCheckDate();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean checkData(){
		String tMsgType = MsgType;
		if(tMsgType==null || "".equals(tMsgType)){
			errLog("没有获得报文类型，或传来的报文类型为空。");
			return false;
		}
		if(!"JG001".equals(tMsgType)){
			errLog("传来的报文类型不是约定好的JG001。");
			return false;
		}
		if(BatchNo==null || "".equals(BatchNo)){
			errLog("批次号不能为空。");
			return false;
		}
		if(SendDate==null || "".equals(SendDate)
			|| SendTime==null || "".equals(SendTime)){
			errLog("报送日期、时间不能为空。");
			return false;
		}
		if(mCheckType==null || "".equals(mCheckType)
		   || mCardNo==null || "".equals(mCardNo)
		   || mName==null || "".equals(mName)
		   || mCheckDate==null || "".equals(mCheckDate)){
			errLog("校验规则类型、卡号、姓名、日期不能为空，请检查！");
			return false;
		}
		if(!"1".equals(mCheckType) && !"2".equals(mCheckType)
			&& !"3".equals(mCheckType) && !"4".equals(mCheckType)){
			errLog("校验规则不在约定范围内");
			return false;
		}
		return true;
	}
	
	private boolean dealData(){
		
		if("1".equals(mCheckType)){
			ruleOne();
		}else if("2".equals(mCheckType)){
			ruleTwo();
		}else if("3".equals(mCheckType)){
			ruleThree();
		}else if("4".equals(mCheckType)){
			ruleFour();
		}
		
		return true;
	}
	
	private boolean ruleOne(){
		String havePolicy = new ExeSQL().getOneValue("select 1 from lccont where contno='"+mCardNo+"' ");
		if(havePolicy==null){
			errLog("01-该保险卡未在我公司承保");
			return false;
		}
		String insuName = new ExeSQL().getOneValue("select insuredname from lccont where contno='"+mCardNo+"' ");
		if(!mName.equals(insuName)){
			errLog("02-传入的客户信息与该保险卡对应的被保险人信息不一致，暂不能使用本项服务");
			return false;
		}
		return true;
	}
	private boolean ruleTwo(){
		ruleOne();
		SSRS tSSRS = new ExeSQL().execSQL("select stateflag,codename('stateflag',stateflag) from lccont where contno='"+mCardNo+"'");
		String stateFlag="", state="";
		if(tSSRS!=null){
			stateFlag = tSSRS.GetText(0, 0);
			state = tSSRS.GetText(0, 1);
			if(!"1".equals(stateFlag)){
				errLog("03-该保险卡当前状态为"+state+"，暂不能使用本项服务");
				return false;
			}
		}
		return true;
	}
	private boolean ruleThree(){
		ruleOne();
		String tSQL = "select case when customgetpoldate is not null then days('"+mCheckDate+"') - days(customgetpoldate) + 1 else days('"+mCheckDate+"') - days(cvalidate) + 1 end from lccont where contno='"+mCardNo+"' ";
		String period = new ExeSQL().getOneValue(tSQL);
		if(Integer.parseInt(period) <= 15){
			errLog("04-该保险卡处于犹豫期内，暂不能使用本项服务");
			return false;
		}
		
		if(PubFun.calInterval(new ExeSQL().getOneValue("select cvalidate from lccont where contno='"+mCardNo+"'"), mCheckDate, "D") <= 0){
			errLog("05-本次服务申请不在该保险卡有效期内，暂不能使用本项服务");
			return false;
		}
		if(PubFun.calInterval(mCheckDate, new ExeSQL().getOneValue("select cinvalidate from lccont where contno='"+mCardNo+"'"), "D") <= 0){
			errLog("05-本次服务申请不在该保险卡有效期内，暂不能使用本项服务");
			return false;
		}
		
		return true;
	}
	private boolean ruleFour(){
		ruleOne();
		String hasClaimRecord = new ExeSQL().getOneValue("select 1 from llclaimpolicy where contno='"+mCardNo+"'");
		if(hasClaimRecord==null || "".equals(hasClaimRecord)){
			errLog("06-被保险人未发送过赔案，暂不能使用本项服务");
			return false;
		}
		return true;
	}
	
}
