package com.sinosoft.filters;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;

public class JSPAccessFilter implements Filter {
	public JSPAccessFilter() {
	}

	public String indexpagename = "../indexlis.jsp";
	public String errorpagename = "../ErrorPage.jsp";
	public String configFile = "ExcludeFilter.xml";
	private String[] excludePages = null;
	private String[] excludeDirs = null;
	private boolean filterFlag = false;

	public void init(FilterConfig filterConfig) throws ServletException {
		InputStream in = null;
		try {
			// System.out.println("init JSPAccessFilter......");
			SAXBuilder builder = new SAXBuilder();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			in = loader.getResourceAsStream(this.configFile);
			Document doc = builder.build(in);

			Element root = doc.getRootElement();

			this.filterFlag = this.getFilterFlag(root);
			if (!filterFlag) {
				System.out.println("Have Stopped JSPAccessFilter.");
				return;
			}

			ArrayList pageList = this.getExcludePageList(root);
			if (pageList.size() > 0) {
				excludePages = new String[pageList.size()];
				excludePages = (String[]) pageList.toArray(excludePages);
			}

			ArrayList dirList = this.getExcludeDirList(root);
			if (pageList.size() > 0) {
				excludeDirs = new String[dirList.size()];
				excludeDirs = (String[]) dirList.toArray(excludeDirs);
			}

			in.close();
		} catch (Exception ex) {
			System.out.println("not found XML file::" + this.configFile);
			try {
				in.close();
			} catch (Exception ex2) {
			}
			ex.printStackTrace();
			return ;
		}
		System.out.println("init JSPAccessFilter Success!");
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws java.io.IOException, ServletException {
		if (this.filterFlag) {
			HttpSession session = ((HttpServletRequest) request).getSession();
			GlobalInput tGI = (GlobalInput) session.getAttribute("GI");
			HttpServletRequest req = (HttpServletRequest) request;
			String servletPath = req.getServletPath();
//			System.out.println("servletPath == " + servletPath);
			
			if (isExcludePage(servletPath)) {
				// chain.doFilter(request, response);
			} else {
				String[] sPath = servletPath.split("/");
				String sDir = sPath[1];
				// System.out.println("directory == " + sDir);

				if (isExcludeDir(sDir)) {// easyscan下JSP文件访问不作此过滤或校验
				// chain.doFilter(request, response);
				} else if (tGI == null) {
					System.out.println("JSPAccessFilter::Session is null");
					HttpServletResponse hres = (HttpServletResponse) response;
					hres.sendRedirect(indexpagename);
				} else if (!this.canIDo(tGI, ".." + servletPath, "menu")) {
					HttpServletResponse hres = (HttpServletResponse) response;
					hres.sendRedirect(errorpagename);
				}
			}
		}
		chain.doFilter(request, response);
	}

	public void destroy() {
		System.out.println("destroy JSPAccessFilter......");
		excludePages = null;
		excludeDirs = null;
	}
	
	/**
	 * 判断是否是不用校验的访问页面
	 * @param servletPath
	 * @return
	 */
	private boolean isExcludePage(String servletPath) {
		boolean excluded = false;
		if (excludePages != null) {
			for (int i = 0; i < excludePages.length; i++) {
				if (servletPath.equals(excludePages[i])) {
					excluded = true;
					break;
				}
			}
		}
		return excluded;
	}
	
	/**
	 * 判断是否是不用校验的访问目录
	 * @param sDir
	 * @return
	 */
	private boolean isExcludeDir(String sDir) {
		boolean excluded = false;
		if (excludeDirs != null) {
			for (int i = 0; i < excludeDirs.length; i++) {
				if (sDir.equals(excludeDirs[i])) {
					excluded = true;
					break;
				}
			}
		}
		return excluded;
	}

	/**
	 * 用户页面权限判断
	 * 
	 * @param cGlobalInput
	 *            GlobalInput
	 * @param RunScript
	 *            String
	 * @return boolean
	 */
	private boolean canIDo(GlobalInput cGlobalInput, String RunScript,
			String pagesign) {
		String Operator = cGlobalInput.Operator;
		// String ComCode = cGlobalInput.ComCode;
		// String ManageCom = cGlobalInput.ManageCom;
		
		if ("001".equals(Operator)){//超级用户不用作权限校验
			return true;
		}
		
		String searchSql = "select count(1) from ldmenu where runscript like '%"
			+ RunScript + "%' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		String tMenuFlag = tExeSQL.getOneValue(searchSql);
		if (tMenuFlag.equals("0")){
				return true;
		}
		
		// 通过用户编码查询用户页面权限集合,NodeSign = 2为用户页面权限菜单标志
		String sqlStr = "select count(1) from LDMenu ";
		sqlStr = sqlStr + "where RunScript like '%" + RunScript + "%' ";
		// "where NodeSign = '2' and RunScript = '" + RunScript + "' ";
		if (pagesign.equals("page"))
			sqlStr = sqlStr
					+ "and parentnodecode in ( select distinct NodeCode from LDMenuGrpToMenu ";
		if (pagesign.equals("menu"))
			sqlStr = sqlStr
					+ "and NodeCode in ( select distinct NodeCode from LDMenuGrpToMenu ";
		sqlStr = sqlStr
				+ "where MenuGrpCode in ( select distinct MenuGrpCode from LDMenuGrp ";
		sqlStr = sqlStr
				+ "where MenuGrpCode in (select distinct MenuGrpCode from LDUserToMenuGrp where UserCode = '";
		sqlStr = sqlStr + Operator;
		sqlStr = sqlStr + "') ) )  with ur";
		SSRS tSSRS = tExeSQL.execSQL(sqlStr);
		if (tSSRS != null) {
			String tt[] = tSSRS.getRowData(1);
			if (tt[0].equals("0")) {
				return false;
			}
		}
		// System.out.println("Yes can do");
		return true;
	}
	
	private boolean getFilterFlag(Element root){
		String tFilterFlag = root.getChildText("filterFlag");
		if("0".equals(tFilterFlag)){
//			System.out.println("Stop Filter.");
			return false;
		}
		return true;
	}

	/**
	 * 从配置文件中读取ExcludePageList
	 * @return
	 */
	private ArrayList getExcludePageList(Element root) {
	    try {
//	    	SAXBuilder builder = new SAXBuilder();
//	        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//	        InputStream in = loader.getResourceAsStream(this.configFile);
//	        Document doc = builder.build(in);
//
//	        Element root = doc.getRootElement();
	        ArrayList list = new ArrayList();
	        List tExcludePageList = root.getChild("excludePages").getChildren();
			for (int i=0; i < tExcludePageList.size(); i++) {
				Element tExcludePage = (Element) tExcludePageList.get(i);
				list.add(tExcludePage.getChildText("PageName"));
//				System.out.println(tExcludePage.getChildText("PageName"));
			}
			return list;
	      }
	      catch (Exception e) {
	        e.printStackTrace();
	        return null;
	      }
	    }
	
	/**
	 * 从配置文件中读取ExcludeDirList
	 * @return
	 */
	private ArrayList getExcludeDirList(Element root) {
	    try {
//	    	SAXBuilder builder = new SAXBuilder();
//	        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//	        InputStream in = loader.getResourceAsStream(this.configFile);
//	        Document doc = builder.build(in);
//
//	        Element root = doc.getRootElement();
	        ArrayList list = new ArrayList();
	        List tExcludePageList = root.getChild("excludeDirs").getChildren();
			for (int i=0; i < tExcludePageList.size(); i++) {
				Element tExcludePage = (Element) tExcludePageList.get(i);
				list.add(tExcludePage.getChildText("DirName"));
//				System.out.println(tExcludePage.getChildText("DirName"));
			}
			return list;
	      }
	      catch (Exception e) {
	        e.printStackTrace();
	        return null;
	      }
	    }
	
	/**
	 * 测试主程序
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			JSPAccessFilter tJSPAccessFilter = new JSPAccessFilter();
			SAXBuilder builder = new SAXBuilder();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream in = loader
					.getResourceAsStream(tJSPAccessFilter.configFile);
			Document doc = builder.build(in);

			Element root = doc.getRootElement();
			tJSPAccessFilter.getFilterFlag(root);
			tJSPAccessFilter.getExcludePageList(root);
			tJSPAccessFilter.getExcludeDirList(root);
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}
		return;
	}
}
