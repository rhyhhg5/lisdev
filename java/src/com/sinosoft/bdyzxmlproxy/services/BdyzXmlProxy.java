package com.sinosoft.bdyzxmlproxy.services;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import com.sinosoft.bdyzxmlproxy.handle.Bdyz;
/**
 *@author yjz
 *@date: 2016-6-3
 */
public class BdyzXmlProxy{
	 private final static Logger mLogger = Logger.getLogger(BdyzXmlProxy.class);	 
	    public BdyzXmlProxy()	    {
	        mLogger.info("init:" + BdyzXmlProxy.class.toString());
	    }
	    /**
	     * 接收并返回加密报文
	     * @param cInXmlStr-接收的加密报文
	     * @return  返回加密报文
	     */
	    public String service(String cInXmlStr){
	        mLogger.info("Start Service");
	        
	        System.out.println("PubServiceProxy：第三方报送核心报文");
	        System.out.println(cInXmlStr);
	        // 记录服务启动时间
	        long mStartMillis = System.currentTimeMillis();
	        mLogger.debug(cInXmlStr);
	        // --------------------
	        Bdyz bdyz = new Bdyz();
	        //加密报文字符串
	        String OutXmlStr = null;
	        try{
	        	// 将接受报文转为DOM对象
	            Document tInXmlDoc = DocumentHelper.parseText(cInXmlStr);
	            // 调用核心后台处理
	            Document tOutXmlDoc = bdyz.deal(tInXmlDoc);	
	            // 将返回报文的DOM对象转化为报文（xml字符串）
	            OutXmlStr=tOutXmlDoc.asXML();                     
	        }catch (Exception e) {
	            mLogger.error("交易出错！", e);
	            OutXmlStr = getError(e.toString());
	        }finally{
	            System.out.println("PubServiceProxy：核心返回报文："+OutXmlStr);  
	        }
	        mLogger.debug(OutXmlStr);
	        // 记录服务结束时间
	        long mDealMillis = System.currentTimeMillis() - mStartMillis;
	        mLogger.debug("处理总耗时：" + mDealMillis / 1000.0 + "s");
	        mLogger.info("End Service");
	        return OutXmlStr;
	    }
	    /**
	     *非预期异常处理
	     * @param pErrorMsg  
	     * @return  异常时返回加密报文
	     */
	    public String getError(String pErrorMsg){
	        mLogger.info("Into CrsPubService.getError()...");
	        mLogger.info("Out CrsPubService.getError()!");
	        String tOutXmlStr="<?xml version='1.0' encoding='UTF-8'?><respData><errorCode>13F8AC1C51D445D3C38AB73AA1A9906C</errorCode><errorReason>F856F46537283D27282C2658BBAD7124F4DF05CC57A71F2C5EA3B1D70CD16FF5CF5234F62F55A8007D1BF6E2CE6B0857</errorReason></respData>";
	        System.out.println("业务处理类报错,请核查相关逻辑");
	        return tOutXmlStr;	        
	    }
}
