package com.sinosoft.bdyzxmlproxy.services;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

public class BdyzOfEC {
	
	public String service(String aInXmlStr){
		System.out.println("发送报文 ："+aInXmlStr);
//		webservice 地址
//		String tStrTargetEendPoint = "http://10.252.4.69:8080/bdyz/services/BdyzXmlProxy";
		String tStrTargetEendPoint = "http://localhost:8080/bdyz/services/BdyzXmlProxy";
  		String tStrNamespace = "http://services.bdyzxmlproxy.sinosoft.com";
		try 
		{
			RPCServiceClient client = new RPCServiceClient();
			EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
			Options option = client.getOptions();
			option.setTo(erf);
			option.setAction("service");//调用方法名
			QName name = new QName(tStrNamespace, "service");//调用方法名
			Object[] object = new Object[] { aInXmlStr };
			Class[] returnTypes = new Class[] { String.	class };
			Object[] response = client.invokeBlocking(name, object, returnTypes);
			String result = (String) response[0];
			System.out.println("BdyzOfEC UI return:" + result);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
