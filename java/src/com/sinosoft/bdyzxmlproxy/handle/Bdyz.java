package com.sinosoft.bdyzxmlproxy.handle;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
/**
 *@author yjz
 *@date: 2016-6-3
 */
public class Bdyz {
	private String queryFlag;
	private String	idType;
	private String	contno;
	private Element code;
	private Element reason;
	private final static  String  mkey="PICCH_ZYDB";
	private SSRS mSSRS1 = new SSRS();
	private SSRS mSSRS2 = new SSRS();
	private SSRS mSSRS3 = new SSRS();
	public Bdyz(){
	}
	/**
	 * 封装返回报文的DOM对象	
	 * @param tInXmlDoc-接收报文的DOM对象
	 * @return
	 */
	public Document deal(Document tInXmlDoc){
		//获取报文的DOM对象的节点
		Element root = tInXmlDoc.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list =root.elements();
		//获取保单号
		contno=decrypt(list.get(0).getText(),mkey);	
		//获取团、个验真标识
		queryFlag=decrypt(list.get(1).getText(),mkey);
		//获取投保、被保人标识
		String insuredFlag=decrypt(list.get(2).getText(),mkey);	
		//创建返回报文的DOM对象
		Document tOutDoc = DocumentHelper.createDocument();
		//设置编码格式
		tOutDoc.setXMLEncoding("UTF-8"); 
		Element respData=DocumentHelper.createElement("respData");
		tOutDoc.setRootElement(respData);
		code=respData.addElement("errorCode");
		reason=respData.addElement("errorReason");
		if(verify(list)){
			mSSRS1 = getData(contno,"policyLevel");
			//数据库未找到记录
			if(null==mSSRS1||0==mSSRS1.getMaxRow()){
				errLog("d");
				return tOutDoc;
			}
			mSSRS2 = getData(contno,"insuredLevel");
			//数据库未找到记录
			if(null==mSSRS2||0==mSSRS2.getMaxRow()){
				errLog("d");
				return tOutDoc;
			}
			mSSRS3 = getData(contno,"riskLevel");
			//数据库未找到记录
			if(null==mSSRS3||0==mSSRS3.getMaxRow()){
				errLog("d");
				return tOutDoc;
			}
			//个单检验数据库证件类型与用户输入类型是否相符	
			if("02".equals(queryFlag)){
				String type=null;//数据库里的证件类型
				if("02".equals(insuredFlag)){;
					type=mSSRS1.getAllData()[0][11];
				System.out.println(type);
				}else if("01".equals(insuredFlag)){
					type=mSSRS1.getAllData()[0][12];
				}
				String tSQL = "select 1 from ldcode1 where codetype='IDTypeCheck' and code='"+idType+"'and code1='"+type+"'";
				String result =  new ExeSQL().getOneValue(tSQL);
				if(result==null || !"1".equals(result)){
					errLog("b");
					return tOutDoc;
			    }
			}
			 Element policyLevel=respData.addElement("policyLevel");
			 policyLevel.addElement("policyNo").addText(encrypt(mSSRS1.getAllData()[0][0],mkey));
			 policyLevel.addElement("groupFlag").addText(encrypt(mSSRS1.getAllData()[0][1],mkey));
			 policyLevel.addElement("realSign").addText(encrypt(mSSRS1.getAllData()[0][2],mkey));
			 policyLevel.addElement("downloadUrl").addText("");
			 policyLevel.addElement("policyType").addText(encrypt(mSSRS1.getAllData()[0][3],mkey));
			 String startDate=mSSRS1.getAllData()[0][4].replace('-', '/');
			 policyLevel.addElement("startDate").addText(encrypt(startDate,mkey));		
			 policyLevel.addElement("startTime").addText(encrypt("00:00:00",mkey));
			 String endDate=mSSRS1.getAllData()[0][5].replace('-', '/');
			 policyLevel.addElement("endDate").addText(encrypt(endDate,mkey));
			 policyLevel.addElement("endTime").addText(encrypt("24:00:00",mkey));
			 policyLevel.addElement("alreadyPay").addText(encrypt(mSSRS1.getAllData()[0][6],mkey));
			 String payDate=mSSRS1.getAllData()[0][7].replace('-', '/');
			 policyLevel.addElement("payDate").addText(encrypt(payDate,mkey));
			 policyLevel.addElement("sumAmnt").addText(encrypt(mSSRS1.getAllData()[0][8],mkey));
			 Element appntLevel=policyLevel.addElement("appntLevel");
			 appntLevel.addElement("appntName").addText(encrypt(mSSRS1.getAllData()[0][9],mkey));;
			 policyLevel.addElement("insuredPeoples").addText(encrypt(mSSRS1.getAllData()[0][10],mkey));
			 //保单为个人时，增加被保险人相关信息层级
			 if("02".equals(mSSRS1.getAllData()[0][1])){
				 for(int i=0;i<mSSRS2.getMaxRow();i++){
				 Element insuredLevel= policyLevel.addElement("insuredLevel");
				 insuredLevel.addElement("insuredName").addText(encrypt(mSSRS2.getAllData()[i][0],mkey));
				 Element riskList=insuredLevel.addElement("riskList");
					 for(int j=0;j<mSSRS3.getMaxRow();j++){
					 Element riskLevel=riskList.addElement("riskLevel");
					 riskLevel.addElement("riskCode").addText(encrypt(mSSRS3.getAllData()[j][1],mkey));
					 riskLevel.addElement("riskName").addText(encrypt(mSSRS3.getAllData()[j][2],mkey));
					 riskLevel.addElement("amnt").addText(encrypt(mSSRS3.getAllData()[j][3],mkey));
					 riskLevel.addElement("prem").addText(encrypt(mSSRS3.getAllData()[j][4],mkey));
					 }			
				 }
			 }
		}else{
			return tOutDoc;
		}		
	        return tOutDoc;
	}
	/**
	 * 校验数据:验证码格式为BDYZ_机构代码_保单号_证件类型_证件号码
	 * @param list-接收报文的节点集合
	 * @return
	 */
	 public boolean verify(List<Element> list){
			idType=decrypt(list.get(3).getText(),mkey);
			String	customerId=decrypt(list.get(4).getText(),mkey);
			String	verificationCode=decrypt(list.get(5).getText(),mkey);
			boolean sw=verificationCode.startsWith("BDYZ_000085_"); 
			boolean nw=verificationCode.endsWith(contno+"_"+idType+"_"+customerId);
			System.out.println("end"+contno+"_"+idType+"_"+customerId);
			System.out.println("sw="+sw+"----------------nw"+nw);
			if(!(sw&&nw)){
				errLog("a");
				return false;
			}
		return true;
	 }
	
	/**
	 * 返回具体的错误代码及错误信息
	 * @param cErrInfo-错误代号
	 * @return
	 */
	 private void errLog(String cErrInfo){	
		 String errorCode = "";
		 String errorReason = "";
        if("a".equals(cErrInfo)){
        	errorCode = encrypt("02",mkey);
        	errorReason = encrypt("参数核对时发现数据不一致，可能信息被篡改！",mkey);
        	System.out.println("参数核对时发现数据不一致，可能信息被篡改");
        }
        if("b".equals(cErrInfo)){
        	errorCode = encrypt("04",mkey);
        	errorReason = encrypt("请选择投保时使用的个人证件类型",mkey);
        	System.out.println("请选择投保时使用的个人证件类型");
        }
        if("c".equals(cErrInfo)){
        	errorCode = encrypt("01",mkey);
        	errorReason = encrypt("接口处理过程中程序发生异常!",mkey);
        	System.out.println("接口处理过程中程序发生异常");
        }
        if("d".equals(cErrInfo)){
        	errorCode = encrypt("03",mkey);
        	errorReason = encrypt("无符合查询条件的保单数据，请确认输入信息的正确性或拨打该公司客服电话,再次查询确认",mkey);
        	System.out.println("无符合查询条件的保单数据，请确认输入信息的正确性或拨打该公司客服电话,再次查询确认");
        }
        code.addText(errorCode);
		reason.addText(errorReason);
       
    }
	 	/**
		 * 从数据库查询数据
		 * @param contno-保单号
		 * @return
		 */
	 public  SSRS getData(String contno,String tFlag ){
	    	String tSQL1="",tSQL2="",tSQL3="";
	    	SSRS tSSRS = new SSRS();
	    	if(contno!=null && !"".equals(contno)){
	    		tSQL1 = "select contno,'02',db2inst1.codename('stateflag',stateflag),db2inst1.codename('cardflag1',cardflag),cvalidate "
	    			  + ",cinvalidate,sumprem,PaytoDate,amnt,appntName,peoples,appntIDType,insuredIDType "
	    			  + "from lccont "
	    			  + "where contno='"+contno+"' "
	    			  + "union "
	    			  + "select contno,'02', "
	    			  + "(select edorname from lmedoritem where edorcode=(select edortype from lpedoritem where contno=lbcont.contno and edorno=lbcont.edorno and edortype in ('ZC','WT','CT','XT'))) "
	    			  + ",db2inst1.codename('cardflag1',cardflag),cvalidate,cinvalidate,sumprem,PaytoDate,amnt,appntName,peoples "
	    			  + ",appntIDType,insuredIDType "
	    			  + "from lbcont "  
	    			  + "where contno='"+contno+"' "
	    			  + "union "
	    			  + "select grpcontno,'01',db2inst1.codename('stateflag',stateflag),cardflag,cvalidate,cinvalidate,sumprem "
	    			  + ",(select paytodate from lcgrppol where grpcontno=lcgrpcont.grpcontno fetch first rows only) "
	    			  + ",amnt,grpname,peoples2,'','' "
	    			  + "from lcgrpcont "
	    			  + "where grpcontno='"+contno+"' "
	    			  + "union "
	    			  + "select grpcontno,'01' "
	    			  + ",(select edorname from lmedoritem where edorcode=(select edortype from lpgrpedoritem where grpcontno=lbgrpcont.grpcontno and edorno=lbgrpcont.edorno and edortype in ('WT','CT','XT'))) "
	    			  + ",cardflag,cvalidate,cinvalidate,sumprem "
	    			  + ",(select paytodate from lbgrppol where grpcontno=lbgrpcont.grpcontno fetch first rows only) "
	    			  + ",amnt,grpname,peoples2,'','' "
	    			  + "from lbgrpcont "
	    			  + "where grpcontno='"+contno+"' ";
	    			  
	    		tSQL2 = "select name,insuredno from lcinsured where contno='"+contno+"'";
	    		tSQL3 = "select p.insuredno,p.riskcode,r.riskname,p.amnt,p.prem from lcpol p,lmriskapp r where p.contno='"+contno+"' and p.riskcode=r.riskcode";
	    	}
	    	if("policyLevel".equals(tFlag))
	    	{
	    		tSSRS = new ExeSQL().execSQL(tSQL1);
	    	}
	    	else if("insuredLevel".equals(tFlag))
	    	{
	    		tSSRS = new ExeSQL().execSQL(tSQL2);
	    	}
	    	else if("riskLevel".equals(tFlag))
	    	{
	    		tSSRS = new ExeSQL().execSQL(tSQL3);
	    	}
	    	return tSSRS;
	    }
	   /**
	     * 解密接收报文的节点内容
	     * @param content-待解密内容
	     * @param key-解密的密钥
	     * @return  解密后报文
	     */
	    public static String decrypt(String content, String key) {
	    if (content.length() < 1)
	    return null;
	    byte[] byteRresult = new byte[content.length() / 2];
	    for (int i = 0; i < content.length() / 2; i++) {
	    int high = Integer.parseInt(content.substring(i * 2, i * 2 + 1), 16);
	    int low = Integer.parseInt(content.substring(i * 2 + 1, i * 2 + 2), 16);
	    byteRresult[i] = (byte) (high * 16 + low);
	    }
	    try {
	    KeyGenerator kgen = KeyGenerator.getInstance("AES");
	    SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(key.getBytes());
 	    kgen.init(128, random);
	    SecretKey secretKey = kgen.generateKey();
	    byte[] enCodeFormat = secretKey.getEncoded();
	    SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");
	    System.out.println(secretKeySpec.getFormat());
	    Cipher cipher = Cipher.getInstance("AES");
	    cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
	    byte[] result = cipher.doFinal(byteRresult);
	    return new String(result,"UTF-8");
	    } catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	    } catch (NoSuchPaddingException e) {
	    e.printStackTrace();
	    } catch (InvalidKeyException e) {
	    e.printStackTrace();
	    } catch (IllegalBlockSizeException e) {
	    e.printStackTrace();
	    } catch (BadPaddingException e) {
	    e.printStackTrace();
	    }catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    return null;
	    }
	    /**
	     * 加密返回报文的节点内容
	     * @param content-待加密内容
	     * @param key-加密的密钥
	     * @return
	     */
	    public static String encrypt(String content, String key) {
	    	try {
	    	KeyGenerator kgen = KeyGenerator.getInstance("AES");
	    	SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
	        random.setSeed(key.getBytes());
	 	    kgen.init(128, random);
	    	SecretKey secretKey = kgen.generateKey();
	    	byte[] enCodeFormat = secretKey.getEncoded();
	    	SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");
	    	Cipher cipher = Cipher.getInstance("AES");
	    	byte[] byteContent = content.getBytes("UTF-8");
	    	cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
	    	byte[] byteRresult = cipher.doFinal(byteContent);
	    	StringBuffer sb = new StringBuffer();
	    	for (int i = 0; i < byteRresult.length; i++) {
	    	String hex = Integer.toHexString(byteRresult[i] & 0xFF);
	    	if (hex.length() == 1) {
	    	hex = '0' + hex;
	    	}
	    	sb.append(hex.toUpperCase());
	    	}
	    	return sb.toString();
	    	} catch (NoSuchAlgorithmException e) {
	    	e.printStackTrace();
	    	} catch (NoSuchPaddingException e) {
	    	e.printStackTrace();
	    	} catch (InvalidKeyException e) {
	    	e.printStackTrace();
	    	} catch (UnsupportedEncodingException e) {
	    	e.printStackTrace();
	    	} catch (IllegalBlockSizeException e) {
	    	e.printStackTrace();
	    	} catch (BadPaddingException e) {
	    	e.printStackTrace();
	    	}
	    	return null;
	    	}
}
