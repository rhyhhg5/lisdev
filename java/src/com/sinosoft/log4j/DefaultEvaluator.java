package com.sinosoft.log4j;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.TriggeringEventEvaluator;

public class DefaultEvaluator implements TriggeringEventEvaluator {   
	  
    /**  
     * Is this <code>event</code> the e-mail triggering event?  
     *   
     * <p>  
     * This method returns <code>true</code>, if the event level has ERROR  
     * level or higher. Otherwise it returns <code>false</code>.  
     */  
    public boolean isTriggeringEvent(LoggingEvent event) {   
        return event.getLevel().isGreaterOrEqual(Level.ERROR);   
    }   
}
