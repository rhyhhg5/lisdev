package com.sinosoft.workflow.circ;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

import java.io.File;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ReportYWExportBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的时间
    private String mDay[] = null;

    //输入的查询sql语句
    private String msql = "";

    private String mOperate = "";

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public ReportYWExportBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDay = (String[]) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "FinGetDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        String strArr[][] = null;
        int SumNum = 0;
        String strStatevalue = "";

        msql = "select comcodeisc ,itemcode , "
                + "(select itemname from lfitemrela where itemcode=a.itemcode) ,"
                + "statvalue from lfexcelcoll a where statyear="
                + mDay[0]
                + " and statmon="
                + mDay[1]
                + " and itemcode in(select itemcode from lfitemrela where itemflag='0')";
        tSSRS = tExeSQL.execSQL(msql + "with ur ");
        SumNum = tSSRS.MaxRow + 1;
        strArr = new String[SumNum][tSSRS.MaxCol];
        for (int i = 1; i < SumNum; i++)
        {
            for (int j = 1; j <= tSSRS.MaxCol; j++)
            {
                if (j == 4)
                {
                    strArr[i][j - 1] = tSSRS.GetText(i, j);
                    if (strArr[i][j - 1] != null
                            && !strArr[i][j - 1].trim().equals("")
                            && !strArr[i][j - 1].equals("null"))
                    {
                        strStatevalue = new DecimalFormat("0.00").format(Double
                                .valueOf(strArr[i][j - 1]));
                        strArr[i][j - 1] = strStatevalue;
                    }
                    else
                    {
                        strArr[i][j - 1] = "0.00";
                    }

                }
                else
                {
                    strArr[i][j - 1] = tSSRS.GetText(i, j);
                }
            }
        }

        try
        {
            WriteToExcel t = null;
            String[] title = null;
            String tFileName = "";
            tFileName = "DATA000085" + mDay[0] + mDay[1] + ".xls";
            t = new WriteToExcel(tFileName);
            title = new String[4];
            title[0] = "机构编码";
            title[1] = "内部科目代码";
            title[2] = "科目名称";
            title[3] = "科目值";
            t.createExcelFile();
            String[] sheetName = { "sheet1" };
            t.addSheet(sheetName);

            //                strArr[0][0]="统计日期：";
            //                strArr[0][1]=mDay[0];
            //                strArr[0][2]="至";
            //                strArr[0][3]=mDay[1];
            //                
            //                strArr[1][0]="统计机构：";
            //                strArr[1][1]=mGlobalInput.ManageCom;

            for (int i = 0; i < title.length; i++)
            {
                strArr[0][i] = title[i];
            }

            t.setData(0, strArr);
            String tServerPath = (new ExeSQL())
                    .getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
            File f = new File(tServerPath + "circresult/excel/" + tFileName);
            f.delete();
            t.write(tServerPath + "circresult/excel/");

            String tURL = (new ExeSQL())
                    .getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerURL' ");
            String resultURL = tURL + "circresult/excel/" + tFileName;
            mResult.clear();
            mResult.addElement(resultURL);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }

}