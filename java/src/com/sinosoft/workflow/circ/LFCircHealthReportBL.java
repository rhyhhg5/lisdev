package com.sinosoft.workflow.circ;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

public class LFCircHealthReportBL {
	public LFCircHealthReportBL() {
	}

	private String mOperate;
	private VData mInputData;
	private String mExcelFile;
	private HashMap mCache = new HashMap();
	private String mManageCom;
	private String mStatYear;
	private String mStatMonth;
	public CErrors mErrors = new CErrors();
	
	/** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

	public boolean submitData(VData tInputData, String tOperate) {

		if (!getInputData(tInputData, tOperate))
			return false;

		if (!checkData())
			return false;

		if (!dealData())
			return false;

		return true;
	}

	private boolean getInputData(VData tInputData, String tOperate) {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput = ((GlobalInput) tInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) tInputData.getObjectByObjectName(
                "TransferData", 0);
		this.mInputData = tInputData;
		if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
		
         //获得业务数据
        if (mTransferData == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mManageCom = (String) mTransferData.getValueByName("ManageCom");
        if (mManageCom == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mStatYear = (String) mTransferData.getValueByName("StatYear");
        if (mStatYear == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中StatYear失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mStatMonth = (String) mTransferData.getValueByName("StatMon");
        if (mStatMonth == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中StatMonth失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

		this.mOperate = tOperate;
		if (mOperate == null || mOperate.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
		
        //取得模版文件所在位置的路径
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("ReportExcelPath");
        if (!tLDSysVarDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LFCircHealthReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "生成路径出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        String strFileName = tLDSysVarDB.getSysVarValue() + "/report.xls";
		this.mExcelFile = strFileName;

		return true;
	}

	private boolean dealData() {

		/**
		 * 1、首先生成Excel对象，然后从Excel对象中查到需要从数据库中查找的关键要素 2、使用关键要素，在数据库中查找对应的数值。
		 * 3、查找完成之后将要素存储到缓存中 4、查询出的结果需要根据计算符进行 + - 运算。
		 */

		POIFSFileSystem fs;
		HSSFWorkbook wb = null;

		try {
			fs = new POIFSFileSystem(new FileInputStream(this.mExcelFile));
			wb = new HSSFWorkbook(fs);

			if (!dealSheet(wb))
				return false;

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			System.out.println("wanc");
            //取得生成文件所在位置的路径
	        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
	        tLDSysVarDB.setSysVar("ReportExcelPath");
	        if (!tLDSysVarDB.getInfo())
	        {
	            CError tError = new CError();
	            tError.moduleName = "LFCircHealthReportBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "生成路径出错！";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        String CurrentDate = PubFun.getCurrentDate();
	        String CurrentTime = PubFun.getCurrentTime();
	        String strFileName = tLDSysVarDB.getSysVarValue() + "DATA" +
            SysConst.CorpCode + mStatYear + mStatMonth + CurrentDate + 
            CurrentTime.substring(0,2) + CurrentTime.substring(3,5) + 
            CurrentTime.substring(6,8) + ".xls";
			FileOutputStream fileOut = new FileOutputStream(strFileName);
			wb.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "LFCircHealthReportBL.java";
			tError.functionName = "dealData";
			tError.errorMessage = "异常错误!" + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 循环处理每一个Sheet，我们这个模板有11个Sheet
	 * 
	 * @param wb
	 * @return
	 */
	private boolean dealSheet(HSSFWorkbook wb) {

		for (int sheetNum = 0; sheetNum < 11; sheetNum++) {
			HSSFSheet sheet = wb.getSheetAt(sheetNum);
			if (sheet != null) {

				if (!dealRow(sheet))
					return false;

			}
		}
		return true;
	}

	private boolean dealRow(HSSFSheet sheet) {

		for (int row = 1; row <= sheet.getLastRowNum(); row++) {
			HSSFRow hssRow = sheet.getRow(row);

			if (hssRow != null) {

				if (!dealCol(hssRow))
					return false;

			}
		}
		return true;
	}

	private boolean dealCol(HSSFRow hssRow) {

		for (int col = 0; col < hssRow.getLastCellNum(); col++) {
			HSSFCell cell = hssRow.getCell((short) col);

			if (cell != null) {

				if (!dealCell(cell))
					return false;
				
			}
		}
		return true;
	}

	/**
	 * 处理一个单元格，只处理字符串类型单元格
	 * 
	 * @param cell
	 * @return
	 */
	private boolean dealCell(HSSFCell cell) {

		if (cell != null) {
			/**
			 * 只处理单元格格式是字符串类型，我们约定的变量是以$xxx/$；
			 */
			if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
				String value = cell.getStringCellValue();
				if (!dealValue(cell, value))
					return false;
			}
		}
		return true;
	}

	/**
	 * 处理模板中的变量。模板中的变量是采取$开头，/$结尾的变量。 将变量作为SQL条件传入进行查询（此处需要注意，会进+ -运算）
	 * 
	 * @param cell
	 * @param value
	 * @return
	 */
	private boolean dealValue(HSSFCell cell, String value) {

		double resule = 0.00;
		if (value != null && !value.equals("")) {
			if (value.startsWith("$") && value.endsWith("/$")) {
				String strCod = value.substring(1, value.length() - 2);
				int len = strCod.length();
				int star = 0;
				boolean needCal = false;
				String[] addCal = strCod.split("-|\\+");
				if(addCal!= null && addCal.length >1){
					String strOper = strCod.replaceAll("[A-Za-z0-9]", "");
					String[] oper = null;
					if(strOper != null && !strOper.equals("")){
						oper = strOper.split("|");
					}
					
					if(addCal.length != oper.length){
						CError tError = new CError();
						tError.moduleName = "LFCircHealthReportBL.java";
						tError.functionName = "dealValue";
						tError.errorMessage = "异常错误，要素和操作符数量不符!";
						this.mErrors.addOneError(tError);
						return false;
					}
					double firstResult = getResule(addCal[0]);
					//if (firstResult < 0)
						//return false;
					resule = firstResult + resule;
					
					for (int i = 1; i < oper.length; i++) {
						double oneRs = getResule(addCal[i]);
						//if (oneRs < 0)
							//return false;
						
						if(oper[i].equals("-")){
							resule = resule - oneRs;
						}else if (oper[i].equals("+")){
							resule = resule + oneRs;
						}
					}
				}else {
					resule = getResule(strCod);
					//if (resule < 0)
						//return false;
				}
				cell.setCellValue(resule);
			}
		}

		return true;
	}

	private double getResule(String strCod) {

		if (strCod == null || strCod.equals("")) {
			CError tError = new CError();
			tError.moduleName = "LFCircHealthReportBL.java";
			tError.functionName = "getResule";
			tError.errorMessage = "没有传入需要计算要素!";
			this.mErrors.addOneError(tError);
			return -1;
		}
		Object rs = mCache.get(strCod);
		if (rs != null) {
			return ((Double) rs).doubleValue();
		}

		StringBuffer strSql = new StringBuffer();
		strSql.append("select statvalue ");
		strSql.append("   from lfxmlcoll ");
		strSql.append("  where comcodeisc in ");
		strSql
				.append("	        (select comcodeisc from lfcomtoisccom where comcode = '");
		strSql.append(this.mManageCom);
		strSql.append("')");
		strSql.append("   and statyear = ");
		strSql.append(this.mStatYear);
		strSql.append("   and statmon = ");
		strSql.append(this.mStatMonth);
		strSql.append("   and itemcode in ");
		strSql
				.append("        (select itemcode from lfitemrela where outitemcode = '");
		strSql.append(strCod);
		strSql.append("') ");
		strSql.append("with ur ");
		String strValue = (new ExeSQL()).getOneValue(strSql.toString());
		double dValue = 0.00;
          
		if (strValue == null || strValue.equals("") || strValue.equals("null")) {
			strValue = "0.00";
		}
		dValue = Arith.round(Double.parseDouble(strValue), 2);
		Double objValue = new Double(dValue);
		mCache.put(objValue, strCod);
		return dValue;
	}

	private boolean checkData() {

		if (this.mExcelFile == null || this.mExcelFile.equals("")) {
			CError tError = new CError();
			tError.moduleName = "LFCircHealthReportBL.java";
			tError.functionName = "checkData";
			tError.errorMessage = "没有传入Excel模板文件!";
			this.mErrors.addOneError(tError);
			return false;
		}

		File tExcelFile = new File(mExcelFile);
		if (!tExcelFile.exists()) {
			CError tError = new CError();
			tError.moduleName = "LFCircHealthReportBL.java";
			tError.functionName = "checkData";
			tError.errorMessage = "没有找到Excle模板文件!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public boolean testDealData() {
		this.mManageCom = "86110000";
		this.mStatYear = "2008";
		this.mStatMonth = "05";
		this.mExcelFile = "c:\\报表.xls";
		return dealData();
	}

	public static void main(String[] args) {
//		String s = "sdfsf-342342+fsdfsdf-qweqweqe+fwfsdf";
//		String[] a = s.split("-|\\+");
//		String b = s.replaceAll("[A-Za-z0-9]", "");
//		System.out.println(b);
		LFCircHealthReportBL t = new LFCircHealthReportBL();
		t.testDealData();
	}
}