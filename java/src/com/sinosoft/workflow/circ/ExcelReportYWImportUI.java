/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.workflow.circ;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ExcelReportYWImportUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
//    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public ExcelReportYWImportUI()
    {}

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        ExcelReportYWImportBL tExcelReportYWImportBL = new ExcelReportYWImportBL();

        System.out.println("---CircReportWorkFlowBL UI BEGIN---");
        if (tExcelReportYWImportBL.submitData(cInputData, mOperate))
        {
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExcelReportYWImportBL.mErrors);
            mResult.clear();
            return false;
        }
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();

        /** 全局变量 */
        mGlobalInput.Operator = "circ";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
        /** 传递变量 */

//汇总计算
        mTransferData.setNameAndValue("RepType", "1");
        mTransferData.setNameAndValue("StatYear", "2005");
        mTransferData.setNameAndValue("StatMon", "09");
       // mTransferData.setNameAndValue("sYearDate", "2005-01-01");
        mTransferData.setNameAndValue("SubMissionID", "18");
        mTransferData.setNameAndValue("MissionID", "00000000000000000175");


        /**总变量*/
        tVData.add(mGlobalInput);
        tVData.add(mTransferData);

        ExcelReportYWImportBL tExcelReportYWImportBL = new ExcelReportYWImportBL();
        try
        {
            if (tExcelReportYWImportBL.submitData(tVData, "0000000224"))
            {
//                VData tResult = new VData();

                //tResult = tActivityOperator.getResult() ;
            }
            else
            {
                System.out.println(tExcelReportYWImportBL.mErrors.getError(0).
                                   errorMessage);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
