/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.workflow.circ;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description:工作流节点任务:保监会报表工作流XML汇总提数撤销服务类 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ExcelReportYWImportCancelBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mmap = new MMap();
    private Connection mConn = null;
    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();
//    private String mOperate;
    /** 业务数据操作字符串 */
    private String mStatYear;
    private String mStatMon;
    private String mDeleteSQL;
//    private Reflections mReflections = new Reflections();

    public ExcelReportYWImportCancelBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        System.out.println("Start  Submit...");

        //mResult.clear();
        return true;
    }

    /**
     * 校验业务数据
     * @return boolean
     */
    private static boolean checkData()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
//        mInputData = cInputData;
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ExcelReportYWImportCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得业务数据
        if (mTransferData == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ExcelReportYWImportCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mStatYear = (String) mTransferData.getValueByName("StatYear");
        if (mStatYear == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ExcelReportYWImportCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中StatYear失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mStatMon = (String) mTransferData.getValueByName("StatMon");
        if (mStatMon == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ExcelReportYWImportCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中StatMon失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {

        String tString = "delete from lfexcelcoll where StatYear=" +
                         mStatYear.trim() + " and StatMon=" +
                         mStatMon.trim();
        mDeleteSQL = tString;
        System.out.println("特殊sql输出" + mDeleteSQL);
        if (!mDeleteSQL.trim().equals(""))
        {
            mmap.put(mDeleteSQL, "DELETE");
        }
        System.out.println("map:" + mmap);
        System.out.println("size:" + mmap.keySet().size());
        
        //准备公共提交数据
        if (mmap != null && mmap.keySet().size() > 0)
        {
            mResult.clear();
            mResult.add(mmap);
        }
        
        //提交到数据库
        try
        {
            PubSubmit sPubSubmit = new PubSubmit();
            
            if (mConn == null)
            {
                mConn = DBConnPool.getConnection();
            }
            if (mConn == null)
            {
                // @@错误处理
                CError.buildErr(this, "保监会报表业务数据提数撤销，申请数据库连接失败");
                return false;
            }
            
            if (mConn != null)
            {
                sPubSubmit.setConnection(mConn);// 传入连接
                sPubSubmit.setCommitFlag(true);// 传入提交方式
//                mResult.clear();
                if (sPubSubmit.submitData(mResult, "") == false)
                {
                    this.mErrors.addOneError("PubSubmit:处理数据库失败!");
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError.buildErr(this, ex.toString());
            try
            {
                mConn.rollback();
                mConn.close();
            }
            catch (Exception x)
            {}
            return false;
        }
        finally
        {// 释放没有释放的数据库连接
            try
            {
                if (mConn != null && !mConn.isClosed())
                {
                    mConn.close();
                }
            }
            catch (Exception x)
            {}
        }
        
        return true;

    }

    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }
}
