package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;
import com.sinosoft.lis.brieftb.BriefSingleContInputBL;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefTbWorkFlowBL
{
    public BriefTbWorkFlowBL()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /**工作流引擎 */
    ActivityOperator mActivityOperator = new ActivityOperator();

    /** 数据操作字符串 */
    private String mOperater;

    private String mManageCom;

    private String mOperate;

    private String mContType;

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        // 数据操作业务处理
        if (!dealData())
        {
            return false;
        }

        System.out.println("---BriefTbWorkFlowBL dealData---");

        //准备给后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("---BriefTbWorkFlowBL prepareOutputData---");

        //数据提交
        GrpBriefTbWorkFlowBLS tGrpBriefTbWorkFlowBLS = new GrpBriefTbWorkFlowBLS();
        System.out.println("Start GrpBriefTbWorkFlowBLS Submit...");

        if (!tGrpBriefTbWorkFlowBLS.submitData(mResult, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpBriefTbWorkFlowBLS.mErrors);
            return false;
        }

        System.out.println("---GrpBriefTbWorkFlowBLS commitData End ---");

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mInputData = cInputData;
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        mOperate = cOperate;
        if ((mOperate == null) || mOperate.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate任务节点编码失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        //生成申请和同首节点
        System.out.println("mOperate2 :" + mOperate);
        System.out.println("ContType1 =-=-=-=-= :" + mContType);
        mContType = (String) mTransferData.getValueByName("ContType");
        System.out.println("ContType2 =-=-=-=-= :" + mContType);

        //个单意外险平台定额单证校验
        if (mOperate.trim().equals("7999999999"))
        {
            String prtNo = (String) mTransferData.getValueByName("PrtNo");

            // 根据简易件保单类型校验印刷号规则。
            if (!CommonBL.isBriefContPrtNoOfRule(prtNo, mContType))
            {
                mErrors.addOneError("申请的印刷号与保单类型不匹配，请核实印刷号位数，以及印刷号规则。");
                return false;
            }
            // ---------------------------------

            BriefSingleContInputBL bl = new BriefSingleContInputBL();
            if (!bl.queryCertifyD(prtNo))
            {
                mErrors.copyAllErrors(bl.mErrors);
                return false;
            }
        }

        if (mOperate.trim().equals("7999999999") && !mContType.equals("6"))
        {
            if (!Execute0000007001())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }
        else if (mOperate.trim().equals("7999999999") && mContType.equals("6"))
        {
            if (!Execute0000007003())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }else if (mOperate.trim().equals("9999997100"))//电销导入业务起始节点
        {
            if (!Execute0000007100())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }else if (mOperate.trim().equals("9999997101"))//电销导入每个保单起始节点
        {
            if (!Execute0000007101())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }
        else
        {
            if (!Execute())
            {
                // @@错误处理
                return false;
            }
        }
        return true;
    }

    /**
     * 执行承保工作流待人工核保活动表任务
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean Execute()
    { //*
        mResult.clear();

        VData tVData = new VData();
        ActivityOperator tActivityOperator = new ActivityOperator();

        //获得当前工作任务的任务ID
        String tMissionID = (String) mTransferData.getValueByName("MissionID");
        String tSubMissionID = (String) mTransferData
                .getValueByName("SubMissionID");
        if (tMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "Execute";
            tError.errorMessage = "前台传输数据TransferData中的必要参数MissionID失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (tSubMissionID == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "Execute";
            tError.errorMessage = "前台传输数据TransferData中的必要参数SubMissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            if (!mActivityOperator.ExecuteMission(tMissionID, tSubMissionID,
                    mOperate, mInputData))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);

                return false;
            }

            //获得执行承保工作流待人工核保活动表任务的结果
            tVData = mActivityOperator.getResult();
            if (tVData != null)
            {
                for (int i = 0; i < tVData.size(); i++)
                {
                    VData tempVData = new VData();
                    tempVData = (VData) tVData.get(i);
                    mResult.add(tempVData);
                }
            }

            //产生执行完承保工作流待人工核保活动表任务后的任务节点
            if (tActivityOperator.CreateNextMission(tMissionID, tSubMissionID,
                    mOperate, mInputData))
            {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                if ((tempVData != null) && (tempVData.size() > 0))
                {
                    mResult.add(tempVData);
                    tempVData = null;
                }
            }
            else
            {
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);

                return false;
            }

            tActivityOperator = new ActivityOperator();
            if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
                    mOperate, mInputData))
            {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                if ((tempVData != null) && (tempVData.size() > 0))
                {
                    mResult.add(tempVData);
                    tempVData = null;
                }
            }
            else
            {
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);

                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理

            this.mErrors.copyAllErrors(mActivityOperator.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "dealData";
            tError.errorMessage = "工作流引擎执行新契约活动表任务出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean Execute0000007001()
    {
        System.out.println("开始创建工作流节点");
        mResult.clear();
        VData tVData = new VData();
        ActivityOperator tActivityOperator = new ActivityOperator();
        try
        {
            //产生执行完发放体检通知书任务后的下一打印体检通知书任务节点
            LWMissionSchema tLWMissionSchema = new LWMissionSchema();
            if (tActivityOperator.CreateStartMission("0000000007",
                    "0000007001", mInputData))
            {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                mResult.add(tempVData);
                tempVData = null;
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(mActivityOperator.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "Execute0000004001";
            tError.errorMessage = "工作流引擎工作出现异常!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean Execute0000007003()
    {
        System.out.println("开始创建工作流节点");
        mResult.clear();
        VData tVData = new VData();
        ActivityOperator tActivityOperator = new ActivityOperator();
        try
        {
            //产生执行完发放体检通知书任务后的下一打印体检通知书任务节点
            LWMissionSchema tLWMissionSchema = new LWMissionSchema();
            if (tActivityOperator.CreateStartMission("0000000007",
                    "0000007003", mInputData))
            {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                mResult.add(tempVData);
                tempVData = null;
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(mActivityOperator.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "Execute0000004001";
            tError.errorMessage = "工作流引擎工作出现异常!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 电销导入业务起始节点
     *
     * @return boolean
     */
    private boolean Execute0000007100()
    {
        System.out.println("开始创建工作流节点");
        mResult.clear();
        ActivityOperator tActivityOperator = new ActivityOperator();
        try
        {
            //产生执行完发放体检通知书任务后的下一打印体检通知书任务节点
            if (tActivityOperator.CreateStartMission("0000000007","0000007100", mInputData))
            {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                mResult.add(tempVData);
                tempVData = null;
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(mActivityOperator.mErrors);
            CError tError = new CError();
            tError.moduleName = "BriefTbWorkFlowBL";
            tError.functionName = "Execute0000007100";
            tError.errorMessage = "工作流引擎工作出现异常!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 电销导入业务起始节点
     *
     * @return boolean
     */
    private boolean Execute0000007101()
    {
        System.out.println("开始创建电销保单工作流节点");
        mResult.clear();
        ActivityOperator tActivityOperator = new ActivityOperator();
        try
        {
            //产生执行完发放体检通知书任务后的下一打印体检通知书任务节点
            if (tActivityOperator.CreateStartMission("0000000007","0000007101", mInputData))
            {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                mResult.add(tempVData);
                tempVData = null;
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);
                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(mActivityOperator.mErrors);
            CError tError = new CError();
            tError.moduleName = "BriefTbWorkFlowBL";
            tError.functionName = "Execute0000007101";
            tError.errorMessage = "工作流引擎工作出现异常!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 准备需要保存的数据
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        //mInputData.add( mGlobalInput );
        return true;
    }

}
