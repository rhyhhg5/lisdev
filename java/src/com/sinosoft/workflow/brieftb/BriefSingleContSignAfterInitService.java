package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.LCContSignBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import java.util.GregorianCalendar;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vbl.LCPolBLSet;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.cbcheck.UWSendPrintUI;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.db.LOPRTManagerDB;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefSingleContSignAfterInitService implements AfterInitService
{
    /** 报错信息 */
    public CErrors mErrors = new CErrors();

    /** 操作参数 */
    private TransferData mTransferData;

    /** 传入后台结果 */
    private VData mResult = new VData();

    /** 前台传入数据 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 合同信息 */
    private LCContSchema mLCContSchema;

    private String tContType;

    private String mContNo;

    private String tLoadFlag;

    private GlobalInput mGlobalInput = new GlobalInput();

    public BriefSingleContSignAfterInitService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        /** 获取数据 */
        if (!getInputData())
        {
            return false;
        }

        /** 教研数据 */
        if (!checkData())
        {
            return false;
        }
        if ("INPUT_PAGES".equals(tLoadFlag) && "5".equals(tContType))
        {
            //            if (!prepareBankData())
            //            {
            //                return false;
            //            }
        }
        if ("5".equals(tContType) && "INPUT_PAGES".equals(tLoadFlag))
        {
            return true;
        }
        /** 处里业务逻辑 */
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * perpareOutputData
     *
     * @return boolean
     */
    private boolean prepareHealthManage()
    {
        LCContSchema tLCContSchema = (LCContSchema) this.mResult
                .getObjectByObjectName("LCContSchema", 0);
        if (tLCContSchema == null)
        {
            buildError("prepareHealthManage", "签单参数没有传入！");
            return false;
        }
        for (int i = 1; i <= tLCContSchema.getFieldCount(); i++)
        {
            this.mTransferData.setNameAndValue(tLCContSchema.getFieldName(i),
                    tLCContSchema.getV(i));
        }
        return true;
    }

    /**
     * 比较保单的交费 --精确到险种 --没查到报错
     *
     * @param inPolSet LCPolSchema
     * @return LJTempFeeSet
     */
    private boolean compareTempFeeRiskErr()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(this.mContNo);
        LCPolSet inPolSet = tLCPolDB.query();
        if (inPolSet == null || inPolSet.size() <= 0)
        {
            String str = "查询险种失败!";
            buildError("compareTempFeeRiskErr", str);
            System.out
                    .println("在程序BriefSingleContSignAfterInitService.compareTempFeeRiskErr() - 115 : "
                            + str);
            return false;
        }
        //安险种分组保单交费求和
        String sql = "select sum( paymoney ),riskcode from ljtempfee where otherno='"
                + inPolSet.get(1).getPrtNo()
                + "' and othernotype='4' and "
                + " confflag='0'  and (EnterAccDate is not null ) group by riskcode  ";
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = exesql.execSQL(sql);
        if (ssrs == null)
        {
            CError.buildErr(this, "交费不足或交费没有到帐");
            return false;
        }
        double sumtmpfeemoney = 0;
        double sumpolprem = 0;
        //按险种比较交费信息
        for (int i = 1; i <= ssrs.getMaxRow(); i++)
        {
            String riskcode = (String) ssrs.GetText(i, 2);
            double tempfeemoney = Double.parseDouble(ssrs.GetText(i, 1));
            sumtmpfeemoney += tempfeemoney;
            double polprem = 0;
            for (int j = 1; j <= inPolSet.size(); j++)
            {
                if (inPolSet.get(j).getRiskCode().equals(riskcode))
                {
                    polprem += inPolSet.get(j).getPrem();
                }
            }
            double differ = Math.abs(tempfeemoney - polprem);
            if (differ > 0.001)
            {
                CError.buildErr(this, "财务暂收费'" + tempfeemoney + "'保单保费'"
                        + polprem + "',保费不一致请确认!");
                return false;
            }
        }

        //整个个人保单交费保存
        for (int j = 1; j <= inPolSet.size(); j++)
        {
            sumpolprem += inPolSet.get(j).getPrem();
        }
        double differ = sumtmpfeemoney - sumpolprem;
        if (differ < 0.0)
        {
            CError.buildErr(this, "保单总交费不足或交费没有到帐");
            return false;
        }

        if (differ < -0.001)
        {
            CError.buildErr(this, "保单总交费大于总保费！");
            return false;
        }

        return true;

    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        LCContSet tLCContSet = new LCContSet();
        boolean isSucc = false;
        if (mLCContSchema.getAppFlag().equals("1"))
        {
            return true;
        }
        /** 签单签判断是否财务到帐，如果到账就签发保单，否则进行直接跳过 */
        if ("INPUT_PAGES".equals(tLoadFlag))
        {
            if (!compareTempFeeRiskErr())
            {
                return true;
            }
        }
        System.out.println("******************");
        System.out.println(mLCContSchema.getCardFlag());
        System.out.println("******************");
        if (mLCContSchema.getCardFlag() != null
                && (mLCContSchema.getCardFlag().equals("6") || mLCContSchema
                        .getCardFlag().equals("9")))
        {
            mLCContSchema.setContNo(mLCContSchema.getProposalContNo());
        }
        tLCContSet.add(this.mLCContSchema);

        VData tInputData = new VData();
        tInputData.add(tLCContSet);
        tInputData.add(this.mGlobalInput);
        /** 首先签单 */
        LCContSignBL tLCContSignBL = new LCContSignBL();
        try
        {
            isSucc = tLCContSignBL.submitData(tInputData, "");
        }
        catch (Exception ex)
        {
            buildError("dealData", "发生错误原因是：" + ex.getMessage());
            return false;
        }

        if (!isSucc)
        {
            this.mErrors.copyAllErrors(tLCContSignBL.mErrors);
            return false;
        }
        else if(tLCContSignBL.mErrors != null)        //将签单报错信息传出    by zhangyang 2011-03-14 
        {
            this.mErrors.copyAllErrors(tLCContSignBL.mErrors);
            this.mResult = tLCContSignBL.getResult();
            //--------------
        }
        else
        {
            this.mResult = tLCContSignBL.getResult();
        }

        if (!delAllMission())
        {
            return false;
        }
        
        if (!addGetPolDate())
        {
        	return false;
        }

        /** 发送首期缴费通知书 */

        /** 成功后短信提醒 */
        //取消短信发送
        //if (!dealMassage())
        //{
        //    return false;
        //}
        //        if (isSucc || mLCContSchema.getAppFlag().equals("1")) {
        //            mResult.add(this.mTransferData);
        //            LCSuccorUI tLCSuccorUI = new LCSuccorUI();
        //            if (!tLCSuccorUI.submitData(this.mResult, "PRINT")) {
        //                this.mErrors.copyAllErrors(tLCSuccorUI.mErrors);
        //                return false;
        //            }
        //        }
        //原先简易保单签单，没有流转到健管系统。这里新增该功能。 added by huxl @ 20070910
        if (!prepareHealthManage())
        {
            return false;
        }
        return true;
    }

    private boolean SendPayInfo()
    {
        //        if ("2".equals(tContType) || "5".equals(tContType))
        //        {

        // 判断是否需要打印缴费通知书。
        String tNoPrtFlag = "0";//默认：0-需要打印
        if (!"2".equals(tContType) && !"5".equals(tContType))
        {
            tNoPrtFlag = "1";
            System.out.println("不需要打印缴费通知书。");
        }
        // --------------------

        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode("07");
        //校验是否发过首期交费通知书 ，如果发过则不再重发
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
        if (tLOPRTManagerDB.query().size() <= 0)
        {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("NoPrtFlag", tNoPrtFlag);
            
            VData tempVData = new VData();
            tempVData.add(tLOPRTManagerSchema);
            tempVData.add(tTransferData);
            tempVData.add(mGlobalInput);

            UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
            if (!tUWSendPrintUI.submitData(tempVData, "INSERT"))
            {
                this.mErrors.copyAllErrors(tUWSendPrintUI.mErrors);
                return false;
            }
        }
        //        }
        return true;
    }

    /**
     * dealMassage
     *
     * @return boolean
     */
    private boolean dealMassage()
    {
        /**
         * add by yangming
         * 只在生产环境上发送短信，其他环境一律不发送短信
         */
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("Mobile");
        if (tLDSysVarDB.getInfo() == false)
        {
            return true;
        }
        if (tLDSysVarDB.getSysVarValue() != null
                && tLDSysVarDB.getSysVarValue().equals("1"))
        {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setProposalContNo(this.mLCContSchema.getProposalContNo());
            LCContSet tLCContSet = tLCContDB.query();
            if (tLCContSet != null && tLCContSet.size() > 0)
            {
                mLCContSchema = tLCContSet.get(1);
            }
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(mLCContSchema.getContNo());
            if (!tLCAppntDB.getInfo())
            {
                return false;
            }
            LCAddressDB tLCAddressDB = new LCAddressDB();
            tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
            tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
            if (!tLCAddressDB.getInfo())
            {
                return true;
            }
            if (tLCAddressDB.getMobile() == null
                    || tLCAddressDB.getMobile().equals(""))
            {
                return true;
            }

            String tAppntName = tLCAppntDB.getAppntName();
            String tAppntSex = getSex(tLCAppntDB.getAppntSex());
            String tRiskName = getRiskName();
            String tInsuredName = mLCContSchema.getInsuredName();
            String tCValiDate = mLCContSchema.getCValiDate();
            String tInsuredIDNo = mLCContSchema.getInsuredIDNo();
            String tInsuredSex = getSex(mLCContSchema.getInsuredSex());

            StringBuffer msgAppnt = new StringBuffer(255);
            StringBuffer msgInsured = new StringBuffer(255);

            msgAppnt.append("您为 ").append(tInsuredName).append("（证件号").append(
                    tInsuredIDNo).append("）投保的").append(tRiskName).append("将于")
                    .append(tCValiDate).append("正式生效。－中国人保健康");
            try
            {
                PubFun.sendMessage(tLCAddressDB.getMobile(), msgAppnt
                        .toString());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                String str = ex.getMessage();
                buildError("dealMassage", str);
                System.out
                        .println("在程序BriefSingleContSignAfterInitService.dealMassage() - 211 : "
                                + str);
                return false;
            }

            //            if (this.mLCContSchema.getInsuredNo() != null && this.mLCContSchema.getAppntNo() != null
            //                && !this.mLCContSchema.getInsuredNo().equals(this.mLCContSchema.getAppntNo())) {
            //                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            //                tLCInsuredDB.setContNo(this.mLCContSchema.getContNo());
            //                tLCInsuredDB.setInsuredNo(this.mLCContSchema.getInsuredNo());
            //                if (!tLCInsuredDB.getInfo()) {
            //                    return false;
            //                }
            //
            //                tLCAddressDB = new LCAddressDB();
            //                tLCAddressDB.setCustomerNo(tLCInsuredDB.getInsuredNo());
            //                tLCAddressDB.setAddressNo(tLCInsuredDB.getAddressNo());
            //                if (!tLCAddressDB.getInfo()) {
            //                    return false;
            //                }
            //                if (tLCAddressDB.getMobile() == null || tLCAddressDB.getMobile().equals("")) {
            //                    return false;
            //                }
            //                msgInsured.append("尊敬的 ")
            //                        .append(tInsuredName)
            //                        .append(tInsuredSex)
            //                        .append("，")
            //                        .append(tAppntName)
            //                        .append("已为您在我公司投保了")
            //                        .append(tRiskName)
            //                        .append("，您的证件号是")
            //                        .append(tInsuredIDNo)
            //                        .append("，保单生效日为")
            //                        .append(tCValiDate)
            //                        .append("，如有疑问可致电4006695518（中国人保健康）");
            //                try {
            //                    PubFun.sendMessage(tLCAddressDB.getMobile(), msgInsured.toString());
            //                } catch (Exception ex) {
            //                    ex.printStackTrace();
            //                }
            //
            //            }

        }
        return true;
    }

    /**
     * getSex
     *
     * @param tSex String
     * @return String
     */
    private String getSex(String tSex)
    {
        return tSex.equals("1") ? "女士" : "先生";
    }

    /**
     * getRiskName
     *
     * @return String
     */
    private String getRiskName()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(this.mLCContSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        String tRiskName = "";
        if (tLCPolSet != null && tLCPolSet.size() > 0)
        {
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                String tRiskCode = tLCPolSet.get(i).getRiskCode();
                CachedRiskInfo tCRI = CachedRiskInfo.getInstance();
                tRiskName += tCRI.findRiskByRiskCode(tRiskCode)
                        .getRiskShortName();
                if (i < tLCPolSet.size())
                {
                    tRiskName += ",";
                }
            }
        }
        return tRiskName;
    }

    /**
     * delAllMission
     *
     * @return boolean
     */
    private boolean delAllMission()
    {
        String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        String sql_insert = "insert into LBmission (select '"
                + tSerielNo
                + "',lwmission.* from lwmission where activityid='0000007001' and "
                + "MissionProp1 = '" + mLCContSchema.getPrtNo() + "')";
        String sql_delete = "delete from lwmission where activityid='0000007001' and MissionProp1='"
                + this.mLCContSchema.getPrtNo() + "'";
        MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        map.put(sql_insert, "INSERT");
        map.put(sql_delete, "DELETE");
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (this.mLCContSchema == null)
        {
            buildError("checkData", "没有传入和同信息！");
            return false;
        }
        if (StrTool.cTrim(this.mLCContSchema.getContNo()).equals(""))
        {
            buildError("checkData", "没有出入合同号码！");
            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mLCContSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            buildError("checkData", "没有查询到保单信息！");
            return false;
        }
        this.mLCContSchema = tLCContDB.getSchema();
        if (this.mTransferData == null)
        {
            buildError("checkData", "没有传入参数信息！");
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        if (this.mInputData == null)
        {
            buildError("getInputData", "没有传入信息！");
            return false;
        }
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName(
                "LCContSchema", 0);
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        //        mContNo =  (String) mTransferData.getValueByName("ContNo");
        mContNo = (String) mLCContSchema.getContNo();
        System.out.println("保单号为yyyyyy" + mContNo);
        tContType = (String) mTransferData.getValueByName("ContType");
        tLoadFlag = (String) mTransferData.getValueByName("LoadFlag");
        System.out.println("保单类型为yyyyyy" + tContType);
        System.out.println("该页面是vvvvvvvvv" + tLoadFlag);
        if (!SendPayInfo())
        {
            return false;
        }
        return true;
    }

    /**
     * 用于客户选择交费方式为银行转帐
     * 自动在财务收费里面添加一条银行
     * 转帐信息
     * 2005-05-23 杨明 添加
     * @return boolean
     */
    private boolean prepareBankData()
    {

        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
        //存储各种银行信息,银行编码,户名,帐号
        String BankFlag = "";
        String BankCode = "";
        String BankAccNo = "";
        String AccName = "";
        /**
         * 合同信息
         */

        BankFlag = this.mLCContSchema.getPayMode();
        BankCode = this.mLCContSchema.getBankCode();
        BankAccNo = this.mLCContSchema.getBankAccNo();
        AccName = this.mLCContSchema.getAccName();

        if (!"4".equals(BankFlag))
        { //4代表银行转账
            return true;
        }
        if (StrTool.cTrim(BankAccNo).equals("")
                || StrTool.cTrim(BankCode).equals("") || BankAccNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "交费方式选择银行转账,但银行代码,银行账号,户名信息不完整!";
            this.mErrors.addOneError(tError);
            return false;
        }

        String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", this.mLCContSchema
                .getPrtNo());

        //String TempFeeNo = PubFun1.CreateMaxNo("GPAYNOTICENO",this.mLCContSchema.getPrtNo());
        String sql = "select riskcode, sum(prem) from lcpol where ContNo='"
                + mContNo + "' and uwflag in ('4','9') group by riskcode ";
        ExeSQL tExeSQL = new ExeSQL();
        //自核通过的情况

        sql = "select riskcode, sum(prem) from lcpol where ContNo='" + mContNo
                + "' group by riskcode ";
        SSRS ssrs = tExeSQL.execSQL(sql);
        if (ssrs.equals("null"))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "查询费用失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String[][] prem = ssrs.getAllData();
        String serNo = PubFun1.CreateMaxNo("SERIALNO", this.mLCContSchema
                .getManageCom());
        GregorianCalendar Calendar = new GregorianCalendar();
        Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
        Calendar.add(Calendar.DATE, 0);
        double sumPrem = 0.00;
        for (int i = 0; i + 1 <= prem.length; i++)
        {
            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(prtSeq);
            tLJTempFeeSchema.setTempFeeType("1");
            tLJTempFeeSchema.setRiskCode(prem[i][0]);
            tLJTempFeeSchema.setAgentGroup(this.mLCContSchema.getAgentGroup());
            tLJTempFeeSchema.setAPPntName(this.mLCContSchema.getAppntName());
            tLJTempFeeSchema.setAgentCode(this.mLCContSchema.getAgentCode());
            tLJTempFeeSchema.setPayDate(Calendar.getTime());
            tLJTempFeeSchema.setPayMoney(prem[i][1]);
            sumPrem += Arith.round(Double.parseDouble(prem[i][1]), 2);
            tLJTempFeeSchema.setManageCom(this.mLCContSchema.getManageCom());
            tLJTempFeeSchema.setOtherNo(this.mLCContSchema.getPrtNo());
            tLJTempFeeSchema.setOtherNoType("4");
            tLJTempFeeSchema.setPolicyCom(this.mLCContSchema.getManageCom());
            tLJTempFeeSchema.setSerialNo(serNo);
            tLJTempFeeSchema.setConfFlag("0");
            tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
            tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
            tLJTempFeeSet.add(tLJTempFeeSchema);
        }
        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(prtSeq);
        tLJTempFeeClassSchema.setPayMode("4");
        tLJTempFeeClassSchema.setPayDate(Calendar.getTime());
        tLJTempFeeClassSchema.setPayMoney(Arith.round(sumPrem, 2));
        tLJTempFeeClassSchema.setManageCom(this.mLCContSchema.getManageCom());
        tLJTempFeeClassSchema.setPolicyCom(this.mLCContSchema.getManageCom());
        tLJTempFeeClassSchema.setBankCode(BankCode);
        tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
        tLJTempFeeClassSchema.setAccName(AccName);
        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
        MMap map = new MMap();
        if (tLJTempFeeSet.size() > 0 && tLJTempFeeClassSet.size() > 0)
        {
            map.put(tLJTempFeeSet, "DELETE&INSERT");
            map.put(tLJTempFeeClassSet, "DELETE&INSERT");
        }
        this.mResult.add(map);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriefSingleContSignAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    /**
     * addGetPolDate
     *
     * @return boolean
     */
    private boolean addGetPolDate()
    {
    	if(!mLCContSchema.getCardFlag().equals("2")){
    		String PrtNo = this.mLCContSchema.getPrtNo();
        	String sql = "select 1 from ldcode where codetype = 'getpoldateflag' " 
        				+ "and code in (select distinct riskwrapcode from lcriskdutywrap where contno = '" + this.mLCContSchema.getContNo() + "')";
        	ExeSQL tExeSQL = new ExeSQL();
        	String result = tExeSQL.getOneValue(sql);
        	if(result == "")
        	{
    	    	String sql_update = "update lccont set " 
    	    			+ "CustomGetPolDate = '" + PubFun.getCurrentDate() + "',"
    	    			+ "GetPolDate = '" + PubFun.getCurrentDate() +"',"
    	    			+ "ModifyDate = '" + PubFun.getCurrentDate() + "',"
    	    			+ "ModifyTime = '" + PubFun.getCurrentTime() + "'," 
    	    			+ "Operator = '" + this.mGlobalInput.Operator + "' "
    	    			+ " where PrtNo = '" + PrtNo + "'";
    	        MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
    	        map.put(sql_update, "UPDATE");
            }
    	}
        return true;
    }
}
