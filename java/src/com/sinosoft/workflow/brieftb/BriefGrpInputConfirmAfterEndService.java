package com.sinosoft.workflow.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.workflowengine.AfterEndService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.cbcheck.GrpUWSendPrintUI;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;

public class BriefGrpInputConfirmAfterEndService implements AfterEndService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 记录传递参数 */
    private TransferData mTransferData = new TransferData();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    public BriefGrpInputConfirmAfterEndService() {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        //校验是否有未打印的体检通知书
        if (!checkData()) {
            return false;
        }
        System.out.println("Start  dealData...");
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        System.out.println("dealData successful!");
        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start  Submit...");
        return true;
    }

    /**
     * 获取引擎传入数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate) {
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName(
                "LCGrpContSchema", 0);
        mLCGrpPolSet = (LCGrpPolSet) cInputData.getObjectByObjectName(
                "LCGrpPolSet", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 校验
     * @return boolean
     */
    private boolean checkData() {
        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    private boolean dealData() {
        //将保单和险种的各种核保标志设置为通过
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setOtherNo(this.mLCGrpContSchema.getProposalGrpContNo());
        tLOPRTManagerDB.setOtherNoType("01");
        tLOPRTManagerDB.setCode("57");
        if (tLOPRTManagerDB.query().size() > 0) {
            return true;
        }
        String isPayMoney = (new ExeSQL()).getOneValue(
                "select count(paymoney) from ljtempfee where otherno='" +
                this.mLCGrpContSchema.getPrtNo() +
                "' and enteraccdate is not null and  TempFeeType in ('1','5') ");
        if (Integer.parseInt(isPayMoney) > 0) {
            return true;
        }
        GrpUWSendPrintUI tGrpUWSendPrintUI = new GrpUWSendPrintUI();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(this.mLCGrpContSchema.getProposalGrpContNo());
        tLOPRTManagerSchema.setOtherNoType("01");
        tLOPRTManagerSchema.setCode("57");
        VData tVData = new VData();
        tVData.add(tLOPRTManagerSchema);
        tVData.add(this.mGlobalInput);
        if (!tGrpUWSendPrintUI.submitData(tVData, "INSERT")) {
            this.mErrors.copyAllErrors(tGrpUWSendPrintUI.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 传输处理
     * @return boolean
     */
    private boolean prepareTransferData() {
        return true;
    }

    /**
     * 后台传输处理
     * @return boolean
     */
    private boolean prepareOutputData() {
        MMap map = new MMap();
        map.put(this.mLCGrpContSchema, "UPDATE");
        map.put(this.mLCGrpPolSet, "UPDATE");
        this.mResult.add(this.mLCGrpContSchema);
        this.mResult.add(this.mLCGrpPolSet);
        this.mResult.add(map);
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public TransferData getReturnTransferData() {
        return mTransferData;
    }

    public CErrors getErrors() {
        return mErrors;
    }

}
