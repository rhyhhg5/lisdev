package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;
import com.sinosoft.lis.cbcheck.UWSendPrintUI;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefSingleContConfirmAfterInitService implements AfterInitService {
    public BriefSingleContConfirmAfterInitService() {
    }

    /**存放结果*/
    private VData mVResult = new VData();


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 传入参数 */
    private TransferData mTransferData = new TransferData();


    /** 往后面传输数据的容器 */
    private VData mInputData;

    private LCContSchema mLCContSchema;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 操作符 */
    private String mOperate;
    /** 个单合同号 */
    private String mContNo;
    /**
     * 工作流传入借口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!perpareOutPutData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        if (this.mInputData == null) {
            buildError("getInputData", "传入数据为空检查程序！");
            return false;
        }
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (this.mTransferData == null) {
            buildError("checkData", "传入参数信息为空，检查程序！");
            return false;
        }
        if (this.mGlobalInput == null) {
            buildError("checkData", "传入操作人信息为空！");
            return false;
        }
        this.mContNo = (String) mTransferData.getValueByName("ContNo");
        if (StrTool.cTrim(this.mContNo).equals("")) {
            buildError("checkData", "传入合同信息为空，请检查程序！");
            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mContNo);
        if (!tLCContDB.getInfo()) {
            buildError("checkData", "查询合同信息失败！");
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode("07");
        //校验是否发过首期交费通知书 ，如果发过则不再重发
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
        if (tLOPRTManagerDB.query().size() <= 0) {
            VData tempVData = new VData();
            tempVData.add(tLOPRTManagerSchema);
            tempVData.add(mGlobalInput);
            UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
            if (!tUWSendPrintUI.submitData(tempVData, "INSERT")) {
                this.mErrors.copyAllErrors(tUWSendPrintUI.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * perpareOutPutData
     *
     * @return boolean
     */
    private boolean perpareOutPutData() {
        return true;
    }

    public VData getResult() {
        return mVResult;
    }

    public TransferData getReturnTransferData() {
        return mTransferData;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefSingleContConfirmAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
