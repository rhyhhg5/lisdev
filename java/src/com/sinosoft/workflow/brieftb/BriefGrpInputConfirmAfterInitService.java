package com.sinosoft.workflow.brieftb;

import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.db.LCPolDB;

public class BriefGrpInputConfirmAfterInitService implements AfterInitService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 记录传递参数 */
    private TransferData mTransferData = new TransferData();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCPolSet mLCPolSet = new LCPolSet();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    private LCContSet mLCContSet = new LCContSet();
    private String strSql_1 = "";
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    public BriefGrpInputConfirmAfterInitService() {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        //校验是否有未打印的体检通知书
        if (!checkData()) {
            return false;
        }
        System.out.println("Start  dealData...");
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        System.out.println("dealData successful!");
        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start  Submit...");
        return true;
    }

    /**
     * 获取引擎传入数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate) {
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName(
                "LCGrpContSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setSchema(mLCGrpContSchema);
        if (!tLCGrpContDB.getInfo()) {
            System.out.println(
                    "程序第65行出错，请检查BriefGrpInputConfirmAfterInitService.java中的getInputData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGrpInputConfirmAfterInitService.java";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询合同信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCGrpPolSet = tLCGrpPolDB.query();
        return true;
    }

    /**
     * 校验
     * @return boolean
     */
    private boolean checkData() {
        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    private boolean dealData() {
        //需要进行自核操作，如果自核不通过怎自核标志至不通过
        //在afterend中如果是自核不通过则返回报错
        //先校验保单信息
        //将保单和险种的各种核保标志设置为通过
        this.mLCGrpContSchema.setUWFlag("9"); //正常通过
        this.mLCGrpContSchema.setUWOperator(mGlobalInput.Operator);
        this.mLCGrpContSchema.setUWTime(PubFun.getCurrentTime());
        this.mLCGrpContSchema.setUWDate(PubFun.getCurrentDate());
        this.mLCGrpContSchema.setApproveDate(PubFun.getCurrentDate());
        this.mLCGrpContSchema.setApproveTime(PubFun.getCurrentTime());
        this.mLCGrpContSchema.setApproveFlag("9");
        for (int i = 1; i <= this.tLCGrpPolSet.size(); i++) {
            tLCGrpPolSet.get(i).setUWFlag("9");
            tLCGrpPolSet.get(i).setUWOperator(mGlobalInput.Operator);
            tLCGrpPolSet.get(i).setUWTime(PubFun.getCurrentTime());
            tLCGrpPolSet.get(i).setUWDate(PubFun.getCurrentDate());
            tLCGrpPolSet.get(i).setApproveDate(PubFun.getCurrentDate());
            tLCGrpPolSet.get(i).setApproveTime(PubFun.getCurrentTime());
            tLCGrpPolSet.get(i).setApproveFlag("9");
        }
        //暂时全部通过，真是操作应该每一个合同复核
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        mLCContSet = tLCContDB.query();
        for (int i = 1; i <= this.mLCContSet.size(); i++) {
            mLCContSet.get(i).setUWDate(PubFun.getCurrentDate());
            mLCContSet.get(i).setUWTime(PubFun.getCurrentTime());
            mLCContSet.get(i).setApproveTime(PubFun.getCurrentTime());
            mLCContSet.get(i).setApproveDate(PubFun.getCurrentDate());
            mLCContSet.get(i).setUWFlag("9");
            mLCContSet.get(i).setUWOperator(mGlobalInput.Operator);
            mLCContSet.get(i).setApproveFlag("9");
        }
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        this.mLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= this.mLCPolSet.size(); i++) {
            mLCPolSet.get(i).setUWDate(PubFun.getCurrentDate());
            mLCPolSet.get(i).setUWTime(PubFun.getCurrentTime());
            mLCPolSet.get(i).setApproveTime(PubFun.getCurrentTime());
            mLCPolSet.get(i).setApproveDate(PubFun.getCurrentDate());
            mLCPolSet.get(i).setUWFlag("9");
            mLCPolSet.get(i).setApproveFlag("9");
        }

        return true;
    }

    /**
     * 传输处理
     * @return boolean
     */
    private boolean prepareTransferData() {
        return true;
    }

    /**
     * 后台传输处理
     * @return boolean
     */
    private boolean prepareOutputData() {
        MMap map = new MMap();
        map.put(this.mLCGrpContSchema, "UPDATE");
        map.put(this.tLCGrpPolSet, "UPDATE");
        map.put(this.mLCContSet, "UPDATE");
        map.put(this.mLCPolSet, "UPDATE");
        this.mResult.add(this.mLCGrpContSchema);
        this.mResult.add(this.tLCGrpPolSet);
        this.mResult.add(map);
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public TransferData getReturnTransferData() {
        return mTransferData;
    }

    public CErrors getErrors() {
        return mErrors;
    }

}
