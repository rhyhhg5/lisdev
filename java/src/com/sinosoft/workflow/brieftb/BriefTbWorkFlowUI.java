package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefTbWorkFlowUI {
    public BriefTbWorkFlowUI() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] args) {
        BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
        VData inputData = new VData();
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "group";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        String ActivityID;
        /** 新单录入，录入完毕节点 */
//        ActivityID = "0000007001";
//        tTransferData.setNameAndValue("MissionID", "00000000000000002749");
//        tTransferData.setNameAndValue("SubMissionID", "1");
//        tTransferData.setNameAndValue("ContNo", "86110000000034");
//        tTransferData.setNameAndValue("PrtNo", "11000000010");
//        tTransferData.setNameAndValue("ManageCom", "");
//        tTransferData.setNameAndValue("AgentCode", "");

        /** 签单 */
        ActivityID = "0000007003";
        tTransferData.setNameAndValue("MissionID", "00000000000000051219");
        tTransferData.setNameAndValue("SubMissionID", "1");
        tTransferData.setNameAndValue("ContNo", "13001925162");
        tTransferData.setNameAndValue("PrtNo", "300000128060");
        tTransferData.setNameAndValue("ManageCom", "");
        tTransferData.setNameAndValue("AgentCode", "");
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("13001925162");
        inputData.add(tLCContSchema);


        inputData.add(tG);
        inputData.add(tTransferData);
        tBriefTbWorkFlowUI.submitData(inputData, ActivityID);
        if (tBriefTbWorkFlowUI.mErrors.needDealError()) {
            System.out.println("错误 : " +
                               tBriefTbWorkFlowUI.mErrors.getFirstError());
        }
    }


    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("mOperate : " + cOperate);
        BriefTbWorkFlowBL tBriefTbWorkFlowBL = new BriefTbWorkFlowBL();

        System.out.println("---GrpTbWorkFlowBL UI BEGIN---");
        if (tBriefTbWorkFlowBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tBriefTbWorkFlowBL.mErrors);
            mResult.clear();
            return false;
        }
        return true;
    }
}
