/*
 * @(#)ParseGuideIn.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团单合同签单处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author wujs
 * @version 6.0
 */

public class BriefGrpContSignAfterInitService implements AfterInitService {
    public BriefGrpContSignAfterInitService() {
    }

    /**存放结果*/
    private VData mVResult = new VData();


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    private TransferData mTransferData = new TransferData();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();


    /** 往后面传输数据的容器 */
    private VData mInputData;

    private String mGrpContNo = "";
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();


    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println(">>>>>>submitData");
        //将操作数据拷贝到本类中
        //  mInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }

        LCGrpContSignBL tLCGrpContSignBL = new LCGrpContSignBL();
        boolean tSignResult = tLCGrpContSignBL.submitData(mInputData, null);
        if (!tSignResult) {
            if (!tLCGrpContSignBL.mErrors.needDealError()) {
                CError.buildErr(this, "部分签单完成");
            } else {
                this.mErrors.copyAllErrors(tLCGrpContSignBL.mErrors);
            }
            mTransferData.setNameAndValue("FinishFlag", "0");
            // return false;
        } else {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~" + mGrpContNo);
            tLCGrpContDB.setProposalGrpContNo(mGrpContNo);
            LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
            if (tLCGrpContSet.size() <= 0 || tLCGrpContSet.size() != 1) {
                System.out.println(
                        "程序第81行出错，请检查BriefGrpContSignAfterInitService.java中的submitData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGrpContSignAfterInitService.java";
                tError.functionName = "submitData";
                tError.errorMessage = "团体合同信息查询为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
//            VData tVData = new VData();
//            tVData.add(tLCGrpContSet.get(1).getSchema());
//            tVData.add(mGlobalInput);
//            tVData.add((String) mTransferData.getValueByName("TemplatePath"));
//            tVData.add((String) mTransferData.getValueByName("OutXmlPath"));
//            tVData.add(mGlobalInput);
//            LCGrpSuccorUI tLCGrpSuccorUI = new LCGrpSuccorUI();
//            if (!tLCGrpSuccorUI.submitData(tVData, "PRINT")) {
//                System.out.println(
//                        "程序第93行出错，请检查BriefGrpContSignAfterInitService.java中的submitData方法！");
//                CError tError = new CError();
//                tError.moduleName = "BriefGrpContSignAfterInitService.java";
//                tError.functionName = "submitData";
//                tError.errorMessage = "生成Xml文件失败！";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
            mTransferData.setNameAndValue("FinishFlag", "1");
            /**@author:Yangming 查询全部的工作流节点,将其全部都删掉 */
            MMap map = new MMap();
            Reflections mReflections = new Reflections();
            LWMissionDB tLWMissionDB = new LWMissionDB();
            tLWMissionDB.setMissionID((String) mTransferData.getValueByName(
                    "MissionID"));
            LWMissionSet tLWMissionSet = tLWMissionDB.query();
            LBMissionSet tLBMissionSet = new LBMissionSet();
            for (int i = 1; i <= tLWMissionSet.size(); i++) {
                LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
                mReflections.transFields(tLBMissionSchema, tLWMissionSet.get(i));
                tLBMissionSchema.setSerialNo(tSerielNo);
                tLBMissionSet.add(tLBMissionSchema);
            }
            map.put(tLWMissionSet, "DELETE");
            map.put(tLBMissionSet, "INSERT");
            if (!dealFirstPay(tLCGrpContSet.get(1))) {
                return false;
            }
            map.put(this.mLOPRTManagerSchema, "DELETE&INSERT");
            this.mVResult.add(map);
        }

        this.mVResult.add(mTransferData);
        return tSignResult;
        //return true;
    }

    /**
     * dealFirstPay
     *
     * @return boolean
     */
    private boolean dealFirstPay(LCGrpContSchema tLCGrpContSchema) {

        if (!checkGrpMain(tLCGrpContSchema)) {
            return false;
        }

        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_GRP_GRPFIRSTPAY);
        String prtSeq = PubFun1.CreateMaxNo("GPAYNOTICENO",
                                            tLCGrpContSchema.getPrtNo());
        if (StrTool.cTrim(prtSeq).equals("")) {
            buildError("preparePrint", "生成号码错误！");
            return false;
        }
        mLOPRTManagerSchema.setPrtSeq(prtSeq);
        mLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getProposalGrpContNo());
        mLOPRTManagerSchema.setOtherNoType("01");
        mLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * checkGrpMain
     *
     * @return boolean
     */
    private boolean checkGrpMain(LCGrpContSchema tLCGrpContSchema) {
        double tTemperFee = 0.0;
        String tRealPayMoney = "";

        String tStrSQL;
        ExeSQL tExeSQL = new ExeSQL();

        tStrSQL = "select sum(PayMoney) from LJTempFee where "
                  +
                  " TempFeeType in ('1','5')and otherno in (select prtno from lcGrpCont where GrpContNo='" +
                  tLCGrpContSchema.getGrpContNo() + "')";

        try {
            tRealPayMoney = tExeSQL.getOneValue(tStrSQL);
        } catch (Exception ex) {
            tRealPayMoney = "0";
        }

        System.out.println("实际交的金额:" + tRealPayMoney);

        if (tRealPayMoney == null || tRealPayMoney.trim().equals("")) {
            tTemperFee = 0.0;
        } else {
            tTemperFee = Double.parseDouble(tRealPayMoney);
        }

        System.out.println("实际交的金额:" + tTemperFee);
        //计算应交保费
        double tPrem = 0.0;
        String tShouldPayMoney = "";
        tStrSQL = "select sum(prem) from lcpol where GrpContNo='" +
                  tLCGrpContSchema.getGrpContNo() + "'";

        tShouldPayMoney = tExeSQL.getOneValue(tStrSQL);

        System.out.println("应该交的金额:" + tShouldPayMoney);

        if (tShouldPayMoney == null || tShouldPayMoney.trim().equals("")) {
            tPrem = 0.0;
        } else {
            tPrem = Double.parseDouble(tShouldPayMoney);
        }

        //实交与应交进行比较
        if (tTemperFee >= tPrem) {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "checkMain";
            tError.errorMessage = "该投保单到帐的金额已足,不必再发发首期交费通知书";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    public VData getResult() {
        return this.mVResult;
    }

    public TransferData getReturnTransferData() {
        return this.mTransferData;
    }

    public CErrors getErrors() {
        return this.mErrors;
    }

    private boolean getInputData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCGrpContSchema = (LCGrpContSchema) cInputData.
                           getObjectByObjectName("LCGrpContSchema", 0);
        this.mGrpContNo = mLCGrpContSchema.getGrpContNo();
        if (mTransferData == null) {
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefGrpContSignAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
