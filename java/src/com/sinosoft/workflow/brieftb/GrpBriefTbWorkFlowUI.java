package com.sinosoft.workflow.brieftb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCGrpContSchema;

public class GrpBriefTbWorkFlowUI {
    public GrpBriefTbWorkFlowUI() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] args) {
        GrpBriefTbWorkFlowUI grpbrieftbworkflowui = new GrpBriefTbWorkFlowUI();
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        TransferData tTransferData = new TransferData();
        tG.Operator = "group";
        tG.ManageCom = "86";
        tG.ComCode = "86";

        /**
         * 初始节点5999999999
         */
//        tTransferData.setNameAndValue("PrtNo", "PrtNo");
//        tTransferData.setNameAndValue("ManageCom", "ManageCom");
//        tTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
//        tTransferData.setNameAndValue("Operator", tG.Operator);
        /**
         * 初始节点0000005001
         */
//        tTransferData.setNameAndValue("GrpContNo", "1400002222");
//        tTransferData.setNameAndValue("PrtNo", "12000000531");
//        tTransferData.setNameAndValue("ManageCom", "86110000");
//        tTransferData.setNameAndValue("AgentCode", "1102000001");
//        tTransferData.setNameAndValue("MissionID", "00000000000000008494");
//        tTransferData.setNameAndValue("SubMissionID", "2");
//        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
//        tLCGrpContSchema.setGrpContNo("0000052103");
//        tVData.add(tLCGrpContSchema);
        /**
         * 签单
         */
        tTransferData.setNameAndValue("GrpContNo", "1400002222");
        tTransferData.setNameAndValue("PrtNo", "12000000531");
        tTransferData.setNameAndValue("ManageCom", "86110000");
        tTransferData.setNameAndValue("AgentCode", "1102000001");
        tTransferData.setNameAndValue("MissionID", "00000000000000008494");
        tTransferData.setNameAndValue("SubMissionID", "1");
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("1400002222");
        tLCGrpContSchema.setAppFlag("9");
        tVData.add(tLCGrpContSchema);
        tVData.add(tTransferData);

        tVData.add(tG);
        tVData.add(tTransferData);
        grpbrieftbworkflowui.submitData(tVData, "0000005002");
    }

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("mOperate : " + cOperate);
        GrpBriefTbWorkFlowBL tGrpBriefTbWorkFlowBL = new GrpBriefTbWorkFlowBL();

        System.out.println("---GrpTbWorkFlowBL UI BEGIN---");
        if (tGrpBriefTbWorkFlowBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpBriefTbWorkFlowBL.mErrors);
            mResult.clear();
            return false;
        }
        return true;
    }
}
