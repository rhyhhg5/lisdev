package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LWActivitySchema;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.BeforeInitService;

/**
 * 简易险生效日期，处理收费日次日的情况
 * 
 * @author 张成轩
 *
 */
public class BriefSingleContSignBeforeInit implements BeforeInitService {
	
	/** 存放结果 */
	private VData mVResult = new VData();

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private String mMissionID;

	private MMap tmpMap = new MMap();

	private TransferData mTransferData = new TransferData();

	private LCContSchema mLCContSchema = new LCContSchema();

	private LCPolSet mLCPolSet = new LCPolSet();

	private LWActivitySchema mLWActivitySchema = new LWActivitySchema();

	private String mActivityID = "";

	private String mCvaliDate = "";

	private String mContNo = "";


	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("BriefSingleContSignBeforeInit----submitData");

		if (!getInputData(cInputData)) {
			return false;
		}
		
		if (!getBasicData()) {
			return false;
		}
		
		if (!preSubmitData()) {
			return false;
		}
		return true;
	}

	public BriefSingleContSignBeforeInit() {

	}

	public VData getResult() {
		return this.mVResult;
	}

	public TransferData getReturnTransferData() {
		return this.mTransferData;
	}

	public CErrors getErrors() {
		return this.mErrors;
	}

	/**
	 * 获取前台数据
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		mLWActivitySchema.setSchema((LWActivitySchema) cInputData
				.getObjectByObjectName("LWActivitySchema", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mMissionID = (String) mTransferData.getValueByName("MissionID");
		System.out.println("mMissionID=" + mMissionID);
		if (mLWActivitySchema == null) {
			mErrors.addOneError("传入的数据不完整.");
			return false;
		}
		if (mMissionID == null) {
			mErrors.addOneError("传入的数据不完整.");
			return false;
		}

		return true;
	}

	/**
	 * 提交数据
	 * 
	 * @return
	 */
	private boolean preSubmitData() {

		mResult.clear();
		mResult.add(tmpMap);

		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, null)) {
			CError.buildErr(this, "数据库保存失败");
			return false;
		}
		return true;
	}

	/**
	 * 基础信息处理
	 * 
	 * @return
	 */
	private boolean getBasicData() {
		mActivityID = mLWActivitySchema.getActivityID();
		LWMissionSet tLWMissionSet = new LWMissionSet();
		LWMissionDB tLWMissionDB = new LWMissionDB();
		tLWMissionDB.setMissionID(mMissionID);
		tLWMissionDB.setActivityID(mActivityID);
		tLWMissionSet = tLWMissionDB.query();
		if (tLWMissionSet.size() != 1) {
			mErrors.addOneError("查询责任信息LWMissionSet失败.");
			return false;
		}

		mContNo = tLWMissionSet.get(1).getMissionProp1();

		mLCContSchema = getLCContSchema(mContNo);
		
		// 判断是否需要处理生效日期 不需要则直接跳出
		if(!"2".equals(mLCContSchema.getCardFlag())){
			if (!checkRisk(mContNo)) {
				return true;
			}
		}

		// 获取生效日期，为收费日次日
		mCvaliDate = getULIValidate(mLCContSchema);
		if (mCvaliDate == null || "".equals(mCvaliDate)) {
			mErrors.addOneError("保单生效日期处理失败，请确认是否已正常进行收费");
			return false;
		}

		String tTmpContNo = mLCContSchema.getContNo();
		// 设置SpecifyValidate 不重算生效日期
		String tStrSql = "update LCPol set SpecifyValidate = 'Y' where ContNo = '"
				+ tTmpContNo + "'";
		tmpMap.put(tStrSql, SysConst.UPDATE);

		mLCContSchema.setCValiDate(mCvaliDate);
		tmpMap.put(mLCContSchema, "UPDATE");
		mLCPolSet = getLCPolSet(mContNo);

		for (int i = 1; i <= mLCPolSet.size(); i++) {

			// 得到基础的lcpol数据
			LCPolSchema tBasicLCPolSchema = new LCPolSchema();
			tBasicLCPolSchema = mLCPolSet.get(i);
			String tPolNo = tBasicLCPolSchema.getPolNo();
			tBasicLCPolSchema.setCValiDate(mCvaliDate);
			tBasicLCPolSchema.setEndDate("");
			tBasicLCPolSchema.setSignDate("");
			tBasicLCPolSchema.setSignTime("");

			// 得到基础的lcduty的数据
			LCDutySet tBasicLCDutySet = new LCDutySet();
			LCDutyDB tLCDutyDB = new LCDutyDB();
			tLCDutyDB.setPolNo(tPolNo);
			tBasicLCDutySet = tLCDutyDB.query();
			if (tBasicLCDutySet.size() < 1) {
				mErrors.addOneError("查询责任信息lcduty失败.");
				return false;
			}
			for (int j = 1; j <= tBasicLCDutySet.size(); j++) {
				tBasicLCDutySet.get(j).setEndDate("");
				tBasicLCDutySet.get(j).setFirstPayDate("");
				tBasicLCDutySet.get(j).setPaytoDate("");
			}

			// 得到基础的lcprem的数据
			LCPremSet tBasicLCPremSet = new LCPremSet();
			LCPremDB tLCPremDB = new LCPremDB();
			tLCPremDB.setPolNo(tPolNo);
			tBasicLCPremSet = tLCPremDB.query();
			if (tBasicLCPremSet.size() < 1) {
				mErrors.addOneError("查询责任信息lcduty失败.");
				return false;
			}
			// 得到基础的lcget的数据
			LCGetSet tBasicLCGetSet = new LCGetSet();
			LCGetDB tLCGetDB = new LCGetDB();
			tLCGetDB.setPolNo(tPolNo);
			tBasicLCGetSet = tLCGetDB.query();
			if (tBasicLCGetSet.size() < 1) {
				mErrors.addOneError("查询责任信息lcduty失败.");
				return false;
			}

			CalBL tCalBL;
			TransferData tTransferData = new TransferData();
			String tSQL = "select distinct riskwrapcode from lcriskdutywrap where prtno = '"+tBasicLCPolSchema.getPrtNo()+"' and riskcode = '"+tBasicLCPolSchema.getRiskCode()+"' ";
			String tRiskWrapCode = new ExeSQL().getOneValue(tSQL);
			tTransferData.setNameAndValue("RiskWrapCode", tRiskWrapCode);
			tCalBL = new CalBL(tBasicLCPolSchema, tBasicLCDutySet, null, tTransferData);
			if (!tCalBL.calPol()) {
				this.mErrors.copyAllErrors(tCalBL.mErrors);
				return false;
			}
			if (tCalBL.mErrors.needDealError()) {
				this.mErrors.copyAllErrors(tCalBL.mErrors);
				return false;
			}

			// 从这个区域的数据中取出所需的字段。
			LCPolSchema tChangeLCPolSchema = new LCPolSchema();
			tChangeLCPolSchema.setSchema(tCalBL.getLCPol());
			System.out.println(tChangeLCPolSchema.getInsuredAppAge());
			System.out.println(tChangeLCPolSchema.getCValiDate());
			System.out.println(tChangeLCPolSchema.getGetStartDate());
			System.out.println(tChangeLCPolSchema.getLastRevDate());
			System.out.println(tChangeLCPolSchema.getPayEndDate());
			System.out.println(tChangeLCPolSchema.getPaytoDate());
			System.out.println(tChangeLCPolSchema.getEndDate());
			tBasicLCPolSchema.setCValiDate(tChangeLCPolSchema.getCValiDate());
			tBasicLCPolSchema.setGetStartDate(tChangeLCPolSchema
					.getGetStartDate());
			tBasicLCPolSchema.setLastRevDate(tChangeLCPolSchema
					.getLastRevDate());
			tBasicLCPolSchema.setPayEndDate(tChangeLCPolSchema.getPayEndDate());
			tBasicLCPolSchema.setPaytoDate(tChangeLCPolSchema.getPaytoDate());
			tBasicLCPolSchema.setEndDate(tChangeLCPolSchema.getEndDate());
			tBasicLCPolSchema.setSpecifyValiDate("Y");

			LCDutySet tChangeLCDutySet = new LCDutySet();
			tChangeLCDutySet = tCalBL.getLCDuty();
			for (int k = 1; k <= tBasicLCDutySet.size(); k++)
            {
                for(int m=1;m<=tChangeLCDutySet.size();m++){
                	LCDutySchema tmpLCDutySchema = tChangeLCDutySet.get(m);
                	if(tBasicLCDutySet.get(k).getPolNo().equals(tmpLCDutySchema.getPolNo()) && tBasicLCDutySet.get(k).getDutyCode().equals(tmpLCDutySchema.getDutyCode())){
                		tBasicLCDutySet.get(k).setGetStartDate(tmpLCDutySchema.getGetStartDate());
                        tBasicLCDutySet.get(k).setPayEndDate(tmpLCDutySchema.getPayEndDate());
                        tBasicLCDutySet.get(k).setPaytoDate(tmpLCDutySchema.getPaytoDate());
                        tBasicLCDutySet.get(k).setEndDate(tmpLCDutySchema.getEndDate());
                	}
                }
            }

			LCPremSet tChangeLCPremSet = tCalBL.getLCPrem();
			for (int k = 1; k <= tBasicLCPremSet.size(); k++)
            {
                for(int m=1;m<=tChangeLCPremSet.size();m++){
                	LCPremSchema tmpLCPremSchema = tChangeLCPremSet.get(m);
                	if(tBasicLCPremSet.get(k).getPolNo().equals(tmpLCPremSchema.getPolNo()) && tBasicLCPremSet.get(k).getDutyCode().equals(tmpLCPremSchema.getDutyCode()) && tBasicLCPremSet.get(k).getPayPlanCode().equals(tmpLCPremSchema.getPayPlanCode())){
                		tBasicLCPremSet.get(k).setPayStartDate(tmpLCPremSchema.getPayStartDate());
                        tBasicLCPremSet.get(k).setPaytoDate(tmpLCPremSchema.getPaytoDate());
                        tBasicLCPremSet.get(k).setPayEndDate(tmpLCPremSchema.getPayEndDate());
                	}
                }
            }

			LCGetSet tChangeLCGetSet = tCalBL.getLCGet();
			for (int k = 1; k <= tBasicLCGetSet.size(); k++)
            {
                for(int m = 1;m<=tChangeLCGetSet.size();m++){
                	LCGetSchema tmpLCGetSchema =  tChangeLCGetSet.get(m);
                	if(tBasicLCGetSet.get(k).getPolNo().equals(tmpLCGetSchema.getPolNo()) && tBasicLCGetSet.get(k).getDutyCode().equals(tmpLCGetSchema.getDutyCode()) && tBasicLCGetSet.get(k).getGetDutyCode().equals(tmpLCGetSchema.getGetDutyCode())){
                		tBasicLCGetSet.get(k).setGettoDate(tmpLCGetSchema.getGettoDate());
                        tBasicLCGetSet.get(k).setGetStartDate(tmpLCGetSchema.getGetStartDate());
                        tBasicLCGetSet.get(k).setGetEndDate(tmpLCGetSchema.getGetEndDate());
                	}
                }
            }

			tmpMap.put(tBasicLCPolSchema, "UPDATE");
			tmpMap.put(tBasicLCDutySet, "UPDATE");
			tmpMap.put(tBasicLCPremSet, "UPDATE");
			tmpMap.put(tBasicLCGetSet, "UPDATE");

		}
		return true;
	}

	/**
	 * 获取险种信息
	 * 
	 * @param ContNo
	 * @return
	 */
	private LCPolSet getLCPolSet(String ContNo) {
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		tLCPolSet = tLCPolDB.query();
		return tLCPolSet;
	}

	/**
	 * 获取保单缴费到帐日期的次日。
	 * 
	 * @param cPtrNo
	 * @return
	 */
	private String getULIValidate(LCContSchema cLCContSchema) {
		String tULIValidate = null;

		// 获取最大的到账日
		String tStrSql = " select max(EnterAccDate) From LJTempFee  "
				+ " where OtherNoType = '4' and ConfFlag = '0' "
				+ " and OtherNo = '" + cLCContSchema.getPrtNo() + "' ";
		tULIValidate = new ExeSQL().getOneValue(tStrSql);
		//#1666 完善个险简易平台-意外险平台出单流程 签单日次日
        if(cLCContSchema.getCardFlag().equals("2")){
        	tULIValidate = PubFun.getCurrentDate();        	
        }
		// 到账日次日
		if (tULIValidate != null && !"".equals(tULIValidate)) {
			String DelayDays = "1";
			tULIValidate = PubFun.calDate(tULIValidate, Integer
					.parseInt(DelayDays), "D", null);
		}

		return tULIValidate;
	}
	
    /**
     * 获取保单Schema
     * 
     * @param ContNo
     * @return
     */
    private LCContSchema getLCContSchema(String ContNo)
    {
        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(ContNo);
        if (tLCContDB.getInfo())
        {
            tLCContSchema.setSchema(tLCContDB.getSchema());
        }
        return tLCContSchema;
    }

	/**
	 * 判断保单下是否存在生效日期为收费日次日的情况
	 * 
	 * @return boolean
	 */
	private boolean checkRisk(String ContNo) {
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		LCPolSet tLCPolSet = tLCPolDB.query();
		for (int i = 1; i <= tLCPolSet.size(); i++) {
			LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
			tLMRiskAppDB.setRiskCode(tLCPolSet.get(i).getRiskCode());
			if (!tLMRiskAppDB.getInfo()) {
				return false;
			}
			int signDateCalMode = tLMRiskAppDB.getSignDateCalMode();
			// SignDateCalMode -- 2 代表收费日次日
			if (signDateCalMode == 2) {
				return true;
			}
		}
		return false;
	}
}
