/**
 * created 2008-11-21
 * by LY
 */
package com.sinosoft.workflow.brieftb;

import com.sinosoft.lis.cbcheck.GrpUWSendPrintUI;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;

/**
 * @author LY
 *
 */
public class BriGrpContInputAfterInitService implements AfterInitService
{
    /**存放结果*/
    private VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = new TransferData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 工作流信息 */
    private String mMissionID;

    private String mSubMissionID;

    private String mGrpContNo;

    public BriGrpContInputAfterInitService()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println(">>>>>>submitData");

        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        try
        {
            LCGrpContDB tGrpContInfoDB = new LCGrpContDB();
            tGrpContInfoDB.setGrpContNo(mGrpContNo);
            if (!tGrpContInfoDB.getInfo())
            {
                String tStrErr = " 保单信息未找到。";
                buildError("submitData", tStrErr);
                return false;
            }

            LCGrpContSchema tGrpContInfo = tGrpContInfoDB.getSchema();

            String tOperator = mGlobalInput.Operator;

            String tCurDate = PubFun.getCurrentDate();
            String tCurTime = PubFun.getCurrentTime();

            StringBuffer tStrSql = null;

            MMap tMMap = new MMap();

            // LCCont
            tStrSql = new StringBuffer();
            tStrSql.append(" update LCCont set ");

            tStrSql.append(" UWFlag = '9', ");
            tStrSql.append(" UWDate = '" + tCurDate + "', ");
            tStrSql.append(" UWOperator = '" + tOperator + "', ");

            tStrSql.append(" ApproveFlag = '9', ");
            tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
            tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
            tStrSql.append(" ApproveCode = '" + tOperator + "' ");

            tStrSql.append(" where 1 = 1 ");
            tStrSql.append(" and GrpContNo = '" + mGrpContNo + "' ");
            System.out.println(tStrSql.toString());
            tMMap.put(tStrSql.toString(), SysConst.UPDATE);
            // --------------------

            // LCPol
            tStrSql = new StringBuffer();
            tStrSql.append(" update LCPol set ");

            tStrSql.append(" UWFlag = '9', ");
            tStrSql.append(" UWDate = '" + tCurDate + "', ");
            tStrSql.append(" UWCode = '" + tOperator + "', ");

            tStrSql.append(" ApproveFlag = '9', ");
            tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
            tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
            tStrSql.append(" ApproveCode = '" + tOperator + "' ");

            tStrSql.append(" where 1 = 1 ");
            tStrSql.append(" and GrpContNo = '" + mGrpContNo + "' ");
            System.out.println(tStrSql.toString());
            tMMap.put(tStrSql.toString(), SysConst.UPDATE);
            // --------------------

            // LCGrpCont
            tStrSql = new StringBuffer();
            tStrSql.append(" update LCGrpCont set ");

            tStrSql.append(" UWFlag = '9', ");
            tStrSql.append(" UWDate = '" + tCurDate + "', ");
            tStrSql.append(" UWOperator = '" + tOperator + "', ");

            tStrSql.append(" ApproveFlag = '9', ");
            tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
            tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
            tStrSql.append(" ApproveCode = '" + tOperator + "' ");

            tStrSql.append(" where 1 = 1 ");
            tStrSql.append(" and GrpContNo = '" + mGrpContNo + "' ");
            System.out.println(tStrSql.toString());
            tMMap.put(tStrSql.toString(), SysConst.UPDATE);
            // --------------------

            // LCGrpPol
            tStrSql = new StringBuffer();
            tStrSql.append(" update LCGrpPol set ");

            tStrSql.append(" UWFlag = '9', ");
            tStrSql.append(" UWDate = '" + tCurDate + "', ");
            tStrSql.append(" UWOperator = '" + tOperator + "', ");

            tStrSql.append(" ApproveFlag = '9', ");
            tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
            tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
            tStrSql.append(" ApproveCode = '" + tOperator + "' ");

            tStrSql.append(" where 1 = 1 ");
            tStrSql.append(" and GrpContNo = '" + mGrpContNo + "' ");
            System.out.println(tStrSql.toString());
            tMMap.put(tStrSql.toString(), SysConst.UPDATE);
            // --------------------

            this.mVResult.add(tMMap);

            // 发送缴费通知书
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setOtherNo(mGrpContNo);
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode("57");
            //校验是否发过首期交费通知书 ，如果发过则不再重发
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
            if (tLOPRTManagerDB.query().size() <= 0)
            {
                VData tempVData = new VData();
                tempVData.add(tLOPRTManagerSchema);
                tempVData.add(mGlobalInput);

                GrpUWSendPrintUI tGrpUWSendPrintUI = new GrpUWSendPrintUI();
                if (!tGrpUWSendPrintUI.submitData(tempVData, "INSERT"))
                {
                    this.mErrors.copyAllErrors(tGrpUWSendPrintUI.mErrors);
                    return false;
                }
            }
            // --------------------

            mTransferData.setNameAndValue("GrpContNo", tGrpContInfo.getGrpContNo());
            mTransferData.setNameAndValue("PrtNo", tGrpContInfo.getPrtNo());
            mTransferData.setNameAndValue("SaleChnl", tGrpContInfo.getSaleChnl());
            mTransferData.setNameAndValue("ManageCom", tGrpContInfo.getManageCom());
            mTransferData.setNameAndValue("AgentCode", tGrpContInfo.getAgentCode());
            mTransferData.setNameAndValue("AgentGroup", tGrpContInfo.getAgentGroup());
            mTransferData.setNameAndValue("GrpName", tGrpContInfo.getGrpName());
            mTransferData.setNameAndValue("CValiDate", tGrpContInfo.getCValiDate());
            mTransferData.setNameAndValue("GrpNo", tGrpContInfo.getAppntNo());
            mTransferData.setNameAndValue("UWDate", tGrpContInfo.getUWDate());
            mTransferData.setNameAndValue("UWTime", tGrpContInfo.getUWTime());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", "发生异常错误 原因是: " + ex + "！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mVResult;
    }

    public TransferData getReturnTransferData()
    {
        return this.mTransferData;
    }

    public CErrors getErrors()
    {
        return this.mErrors;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        this.mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            return false;
        }
        this.mMissionID = (String) mTransferData.getValueByName("MissionID");
        this.mSubMissionID = (String) mTransferData.getValueByName("SubMissionID");
        if (this.mMissionID == null)
        {
            buildError("getInputData", "MissionID为null！");
            return false;
        }
        if (this.mSubMissionID == null)
        {
            buildError("getInputData", "SubMissionID为null！");
            return false;
        }
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (mGrpContNo == null || mGrpContNo.equals(""))
        {
            buildError("getInputData", "获取团体保单号失败。");
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyGrpContInputAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
