package com.sinosoft.workflow.polchk;

import com.sinosoft.utility.DBConnPool;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import com.sinosoft.utility.CError;
import java.sql.PreparedStatement;
import com.sinosoft.utility.StrTool;
import java.sql.Connection;
import java.sql.Types;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


/**
 * <p>ClassName: PolChkExecSql </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * <p>@author not attributable</p>
 * <p>@version 1.0</p>
 */

public class PolChkExecSql {

    private Connection con;
    private boolean mflag = false;
    private FDate fDate = new FDate();
    public CErrors mErrors = new CErrors(); // 错误信息

    public PolChkExecSql() {
    }

    /**
     * 功能：可以执行输入的任意查询SQL语句。
     * 输入：任意一个查询语句的字符串csql
     * 返回：一个SSRS类的实例，内为查询结果
     */
    public VData execSQL(String sql)
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ResultSetMetaData rsmd = null;
        VData tSSRS = null;

        System.out.println("ExecSQL : " + sql.trim());
        //add by yt
        if (mflag == false)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            pstmt = con.prepareStatement(StrTool.GBKToUnicode(sql),
                                         ResultSet.TYPE_FORWARD_ONLY,
                                         ResultSet.CONCUR_READ_ONLY);
            rs = pstmt.executeQuery();
            rsmd = rs.getMetaData();

            int n = rsmd.getColumnCount();
            tSSRS = new VData();

            // 取得总记录数
            while (rs.next())
            {
                for (int j = 1; j <= n; j++)
                {
                    String strField = rsmd.getColumnName(j);
                    tSSRS.addElement(strField);
                }
            }

            rs.close();
            pstmt.close();

            if (mflag == false)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("####ExeSQL at exeSQL1: " + sql);
            e.printStackTrace();

            // @@错误处理
            CError.buildErr(this, e.toString(), mErrors);

            tSSRS = null;

            //      tSSRS.ErrorFlag = true;
            try
            {
                if(rs!=null)
                {
                    rs.close();
                }
                if(pstmt!=null)
                {
                    pstmt.close();
                }

                if (mflag == false)
                {
                    con.close();
                }
            }
            catch (Exception ex)
            {
            }
        }
        return tSSRS;
    }


}
