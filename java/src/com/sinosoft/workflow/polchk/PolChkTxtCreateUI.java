/**
 * Copyright (c) 2005 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.workflow.polchk;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.workflow.polchk.PolChkExecSql;
import com.sinosoft.lis.db.LDBankDB;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import java.io.File;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Transformer;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class PolChkTxtCreateUI {


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
//    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;
//    private String mOperate;
    /** 业务数据操作字符串 */
    private String mStatDate;
    private String mEndDate;

    //当前生成的XML 用到的表
    private String mTableName;

    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

//    private Reflections mReflections = new Reflections();
    private SSRS mSSRS;
    private VData mFieldData;

    //转换文件格式

    private String mfileName = "";
    private static final String mPre = "lfc";
    /**生成xml 用到的表名*/
    private static final String mPolMain = mPre + "pol_main";
    private static final String mEndoFee = mPre + "endo_fee";
    private static final String mPremInfo = mPre + "prem_info";
    private static final String mClaimMain = mPre + "claim_main";
    private static final String mClaimSettled = mPre + "claim_settled";
    private static final String mPayDue = mPre + "pay_due";
    private static final String mAgentInfo = mPre + "agent_info";
    private static final String mAgtCode = mPre + "agt_code";
    private static final String mPlanInfo = mPre + "plan_info";

    public PolChkTxtCreateUI() {
    }
    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData)
    {
        System.out.println(" start  submit");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        System.out.println("------deal data over---:" + PubFun.getCurrentTime());

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("end  Submit");

        //mResult.clear();
        return true;
    }

     /**
      * 从输入数据中得到所有对象
      * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      * @param cInputData VData
      * @param cOperate String
      * @return boolean
      */
     private boolean getInputData(VData cInputData)
     {
         //从输入数据中得到所有对象
         //获得全局公共数据
         mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                 "GlobalInput", 0));
         mTransferData = (TransferData) cInputData.getObjectByObjectName(
                 "TransferData", 0);
//        mInputData = cInputData;
         if (mGlobalInput == null)
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         //获得操作员编码
         mOperater = mGlobalInput.Operator;
         if (mOperater == null || mOperater.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据Operate失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         //获得登陆机构编码
         mManageCom = mGlobalInput.ManageCom;
         if (mManageCom == null || mManageCom.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         //获得业务数据
         if (mTransferData == null)
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输业务数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         mStatDate = (String) mTransferData.getValueByName("StatDate");
         if (mStatDate == null)
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输业务数据中StatYear失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         mEndDate = (String) mTransferData.getValueByName("EndDate");
         if (mEndDate == null)
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "PEdorPrintAutoHealthAfterInitService";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输业务数据中StatMon失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         return true;
    }

    /**
     * 校验业务数据
     * @return boolean
     */
    private static boolean checkData()
    {

        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        //添加核保通知书打印管理表数据

        mResult.add(map);
        return true;
    }

    /**
     * 根据前面的输入数据，生成xml文件
     * 如果在生成XML文件过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean makeFile(String cFileName)
    {
        try
        {

            //取得文件生成位置的路径
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("ReportXmlPath");
            if (!tLDSysVarDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "MakeXMLBL";
                tError.functionName = "makeFile";
                tError.errorMessage = "生成路径出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //生成XML文件`
            String strFileName =tLDSysVarDB.getSysVarValue() + cFileName + ".xml";
            mfileName = strFileName;
            PrintWriter out = new PrintWriter(new FileOutputStream(strFileName));
            out.println("<?xml version=\"1.0\" encoding=\"gb2312\" ?>");
            out.flush();
            out.println("<POLDATA>");
            out.flush();
            for (int i = 1; i <= mSSRS.getMaxRow(); i++)
            {
                if (i==1){
                    out.println("  <ROW>");
                    out.flush();
                    for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                        out.print("    <" + (String) mFieldData.getObject(j - 1) +
                                  ">");
                        out.print((String) mFieldData.getObject(j - 1));
                        out.println("</" + (String) mFieldData.getObject(j - 1) +
                                    ">");
                    }
                    out.println("  </ROW>");
                    out.println("");
                    out.flush();

                }
                out.println("  <ROW>");
                out.flush();
                for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                    out.print("    <" + (String) mFieldData.getObject(j-1) + ">");
                    out.print(mSSRS.GetText(i, j));
                    out.println("</" + (String) mFieldData.getObject(j-1)+ ">");
                }
                out.println("  </ROW>");
                out.println("");
                out.flush();
            }
            out.println("</POLDATA>");
            out.flush();
            out.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "makeFile";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()    {

        //Pol_Main数据处理
        if (!dealPolMain()) {
            return false;
        }

        //Endo_Fee数据处理
        if (!dealEndoFee()) {
            return false;
        }

        //Prem_Info数据处理
        if (!dealPremInfo()) {
            return false;
        }

        //Claim_Main数据处理
        if (!dealClaimMain()) {
            return false;
        }

        //Claim_Settled数据处理
        if (!dealClaimSettled()) {
            return false;
        }

        //Pay_Due数据处理
        if (!dealPayDue()) {
            return false;
        }

        //Agent_Info数据处理
        if (!dealAgentInfo()) {
            return false;
        }

        //Agt_Code数据处理
        if (!dealAgtCode()) {
            return false;
        }

        //Plan_Info数据处理
        if (!dealPlanInfo()) {
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行Pol_Main处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealPolMain()    {
            // 生成 pol_main 文件
          String tSQL = "select * from " + mPolMain + " a where a.Eff_date >='" + this.mStatDate
                          + "' and a.Eff_date <='" + this.mEndDate + "'"
                          + " order by a.PolNo";
          mTableName = mPolMain;

          if (!creatFile(tSQL)) {
              return false;
          }
          System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

          return true;
    }

    /**
     * 根据前面的输入数据，进行Endo_Fee处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
     private boolean dealEndoFee()    {

         // 生成 endo_fee 文件
          String tSQL = "select * from " + mEndoFee + " a where a.Proc_date >='" + this.mStatDate
                          + "' and a.Proc_date <='" + this.mEndDate + "'"
                          + " order by a.PolNo";
          mTableName = mEndoFee;
          if (!creatFile(tSQL)) {
              return false;
          }
          System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

           return true;
    }

    /**
     * 根据前面的输入数据，进行Prem_Info处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
     private boolean dealPremInfo()    {

             // 生成 prem_info 文件
            String tSQL = "select * from " + mPremInfo  + " a where a.Gained_date >='" + this.mStatDate
                           + "' and a.Gained_date  <='" + this.mEndDate + "'"
                           + " order by a.PolNo";
           mTableName = mPremInfo;
           if (!creatFile(tSQL)) {
               return false;
           }
           System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());
           return true;
    }

    /**
     * 根据前面的输入数据，进行Claim_Main处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealClaimMain ()    {

       // 生成 claim_main文件
        String  tSQL = "select * from " + mClaimMain  + " a where a.Docu_date >='" + this.mStatDate
                        + "' and a.Docu_date <='" + this.mEndDate + "'"
                        + " union all "
                        + " select a.* from " + mClaimMain  + " a ," + mClaimSettled + " b where a.Docu_date <'" + this.mStatDate
                        + "' and b.GAINED_DATE >'" + this.mEndDate + "'"
                        + " and a.CASENO = b.CASENO and a.PolNo = b.PolNo ";


        mTableName = mClaimMain;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

        return true;
   }

   /**
     * 根据前面的输入数据，进行Claim_Settled处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealClaimSettled()    {

        // 生成 claim_settled 文件
        String tSQL = "select * from " + mClaimSettled + " a where a.Gained_date >='" +
               this.mStatDate
               + "' and a.Gained_date <='" + this.mEndDate + "'"
               + " union all "
               + " select b.* from " + mClaimMain  + " a ," + mClaimSettled + " b where a.Docu_date <'" + this.mStatDate
               + "' and b.GAINED_DATE >'" + this.mEndDate + "'"
               + " and a.CASENO = b.CASENO and a.PolNo = b.PolNo ";

        mTableName = mClaimSettled;
        if (!creatFile(tSQL)) {
            return false;
        }
        System.out.println(mTableName + "-----xml over---:" + PubFun.getCurrentTime());

        return true;
   }

   /**
    * 根据前面的输入数据，进行Pay_Due处理
    * 如果在处理过程中出错，则返回false,否则返回true
    * @return boolean
    */
     private boolean dealPayDue()    {

         // 生成 pay_due 文件
      String tSQL = "select * from " + mPayDue  + " a where a.Accru_date >='" + this.mStatDate
                       + "' and a.Accru_date  <='" + this.mEndDate + "'"
                       + " and a.Gained_date >='" + this.mStatDate
                       + "' and a.Gained_date  <='" + this.mEndDate + "'"
                       + " order by a.PolNo";
       mTableName = mPayDue ;
       if (!creatFile(tSQL)) {
           return false;
       }
       System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

       return true;
     }
     /**
    * 根据前面的输入数据，进行Agent_Info处理
    * 如果在处理过程中出错，则返回false,否则返回true
    * @return boolean
    */
     private boolean dealAgentInfo()    {

       // 生成 agent_info 文件
       String tSQL = "select * from " + mAgentInfo
                       + " order by Agentno";
       mTableName = mAgentInfo ;
       if (!creatFile(tSQL)) {
           return false;
       }
       System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

       return true;
     }

     /**
    * 根据前面的输入数据，进行Agt_Code处理
    * 如果在处理过程中出错，则返回false,否则返回true
    * @return boolean
    */
     private boolean dealAgtCode()    {

     // 生成 agt_code 文件
       String tSQL = "select * from " + mAgtCode
                       + " order by Agt_Code";
       mTableName = mAgtCode ;
       if (!creatFile(tSQL)) {
           return false;
       }
       System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

       return true;
     }

     /**
    * 根据前面的输入数据，进行Plan_Info处理
    * 如果在处理过程中出错，则返回false,否则返回true
    * @return boolean
    */
     private boolean dealPlanInfo()    {

       // 生成 plan_code 文件
       String tSQL = "select * from " + mPlanInfo
                       + " order by plan_code";
       mTableName = mPlanInfo ;
       if (!creatFile(tSQL)) {
           return false;
       }
       System.out.println(mTableName+"-----xml over---:" + PubFun.getCurrentTime());

       return true;
     }

     /**
          * 根据前面的输入数据，进行BL逻辑处理
          * 如果在处理过程中出错，则返回false,否则返回true
          * @return boolean
          */
       private boolean creatFile(String tSQL) {

        String strFileName = "";
        //取得所有要生成文件的数据
         ExeSQL tExeSQL = new ExeSQL();
         mSSRS = tExeSQL.execSQL(tSQL);
         if (tExeSQL.mErrors.needDealError())
         {
             CError tError = new CError();
             tError.moduleName = "MakeXMLBL";
             tError.functionName = "creatFile";
             tError.errorMessage = "查询XML数据" + mTableName + "出错！";
             this.mErrors.addOneError(tError);
             return false;
         }

         //取得所有要生成文件的表的字段名
         PolChkExecSql tPolChkExecSql = new PolChkExecSql();
         mFieldData =  tPolChkExecSql.execSQL(tSQL);
         if (tPolChkExecSql.mErrors.needDealError())
         {
             CError tError = new CError();
             tError.moduleName = "MakeXMLBL";
             tError.functionName = "creatFile";
             tError.errorMessage = "查询XML字段名" + mTableName + "出错！";
             this.mErrors.addOneError(tError);
             return false;
         }

         //生成XML文件

         //文件名称
         strFileName = mTableName.substring(3,mTableName.length());

         if (!makeFile(strFileName)) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "MakeXMLBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据处理失败MakeXMLBL-->makeFile!";
             this.mErrors.addOneError(tError);
             return false;
         }
         //转换xml
         if (!xmlTransform()) return false;

          return true;
       }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return boolean
     */
    private static boolean prepareTransferData()
    {
        //为工作流中回收体检通知书节点准备属性数据
        return true;
    }


    public VData getResult()
    {
        mResult = new VData(); //不保证事务一致性
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * Simple sample code to show how to run the XSL processor
     * from the API.
     */
    public boolean xmlTransform() {
      // Have the XSLTProcessorFactory obtain a interface to a
      // new XSLTProcessor object.
      String strFileName = "";
      String strfilePath = "";
      try {
          if(mfileName !="") {
              strfilePath = mfileName.substring(0,mfileName.lastIndexOf("/"));
          }
          System.out.println("txtPath:" + strfilePath);
          strFileName = mTableName.substring(3,mTableName.length());
          String xslPath = strfilePath+ "/" + "xsl/" + strFileName + ".xsl";
          System.out.println("xslPath:" + xslPath);

          File fSource = new File(mfileName);
          File fStyle = new File(xslPath);

          Source source = new StreamSource(fSource);
          Result result = new StreamResult(new FileOutputStream(mfileName.
                  substring(0, mfileName.lastIndexOf(".")) + ".txt"));
          Source style = new StreamSource(fStyle);

          //Create the Transformer
          TransformerFactory transFactory = TransformerFactory.newInstance();
          Transformer transformer = transFactory.newTransformer(style);

          //Transform the Document
          transformer.transform(source, result);

          System.out.println("Transform Success!");
      }
      catch(Exception e) {
        e.printStackTrace();
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "WriteToFileBLS";
        tError.functionName = "SimpleTransform";
        tError.errorMessage = "Xml处理失败!!";
        this.mErrors .addOneError(tError);
        return false;
      }

      return true;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();

        /** 全局变量 */
        mGlobalInput.Operator = "circ";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
        /** 传递变量 */
        //生成XML 文件
        mTransferData.setNameAndValue("StatDate","2005-05-01");
        mTransferData.setNameAndValue("EndDate","2006-06-20");


        /**总变量*/
        tVData.add(mGlobalInput);
        tVData.add(mTransferData);

        PolChkTxtCreateUI tPolChkTxtCreateUI = new PolChkTxtCreateUI();
        try {
            if (tPolChkTxtCreateUI.submitData(tVData)) {
//                VData tResult = new VData();
                //tResult = tActivityOperator.getResult() ;
            } else {
                System.out.println(tPolChkTxtCreateUI.mErrors.getError(0).
                                   errorMessage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }




}
