package com.sinosoft.workflow.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCRReportItemSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LCCUWMasterSet;
import com.sinosoft.lis.vschema.LCPENoticeItemSet;

/**
 * <p>Title:新契约工作流 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class TbWorkFlowUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public TbWorkFlowUI() {}

    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();

        /** 全局变量 */
        mGlobalInput.Operator = "UW5304";
        mGlobalInput.ComCode = "8653";
        mGlobalInput.ManageCom = "8653";
        String ActivityID = "";
        //创建起始节点

//        mTransferData.setNameAndValue("PrtNo", "43232323232");
//        mTransferData.setNameAndValue("Operator", "001");
//        mTransferData.setNameAndValue("ManageCom", "86110000");
//        mTransferData.setNameAndValue("InputDate", "2004-12-10");

        //自动初始化
//        mTransferData.setNameAndValue("ContNo","130110000013723");
//        mTransferData.setNameAndValue("PrtNo","19800000004");
//        mTransferData.setNameAndValue("ManageCom", "86110000");
//        mTransferData.setNameAndValue("MissionID","00000000000000005134");
//        mTransferData.setNameAndValue("SubMissionID","1");
//        mTransferData.setNameAndValue("PrtSeq","810000000000595");
//        //问题件打印确认
//        mTransferData.setNameAndValue("ContNo","130110000008969");
//        mTransferData.setNameAndValue("PrtNo","444444444");
//        mTransferData.setNameAndValue("MissionID","00000000000000000559");
//        mTransferData.setNameAndValue("SubMissionID","6");;


//        mTransferData.setNameAndValue("AppntNo", "0000314850");
//        mTransferData.setNameAndValue("AppntName", "zhangxing");
//        mTransferData.setNameAndValue("AgentCode", "8611000433");

//        mTransferData.setNameAndValue("MissionID", "00000000000000004978");
//        mTransferData.setNameAndValue("SubMissionID", "1");
//        mTransferData.setNameAndValue("UWFlag", "9");
//        mTransferData.setNameAndValue("UWIdea2", "9");

        //执行起始节点
//    mTransferData.setNameAndValue("ContNo", "00000000000000000006");
//    mTransferData.setNameAndValue("PrtNo", "00002004111207");
//    mTransferData.setNameAndValue("Operator", "001");
//    mTransferData.setNameAndValue("ApproveCode", "001");
//    mTransferData.setNameAndValue("MakeDate", "2004-11-22");
//    mTransferData.setNameAndValue("AppntNo", "0000031226");
//    mTransferData.setNameAndValue("AppntName", "张三");
//    mTransferData.setNameAndValue("InsuredNo", "0000031226");
//    mTransferData.setNameAndValue("InsuredName", "张三");
//    mTransferData.setNameAndValue("MissionID", "00000000000000000166");
//    mTransferData.setNameAndValue("SubMissionID", "1");
///** 传递变量 */
        //自动核保
//    LCContSchema tLCContSchema = new LCContSchema();
//    tLCContSchema.setContNo( "99999020040990000036" );
//    mTransferData.setNameAndValue("LCContSchema",tLCContSchema);
//    mTransferData.setNameAndValue("MissionID", "00000000000000000200");
//    mTransferData.setNameAndValue("SubMissionID", "1");

        //新单复核 0000001001
//        ActivityID = "0000001001";
//        mTransferData.setNameAndValue("ContNo", "13000477487");
//        mTransferData.setNameAndValue("MissionID", "00000000000000018040");
//        mTransferData.setNameAndValue("SubMissionID", "1");
//        mTransferData.setNameAndValue("PrtNo", "16000014408");
//        mTransferData.setNameAndValue("AppntNo", "000888876");
//        mTransferData.setNameAndValue("AppntName", "核保王国武");
//        mTransferData.setNameAndValue("AgentCode", "1101000099");
//        mTransferData.setNameAndValue("ManageCom", "86");
//        mTransferData.setNameAndValue("Operator", "group");


        //复核修改
//    mTransferData.setNameAndValue("ContNo", "00000000000000000006");
//    mTransferData.setNameAndValue("MissionID", "00000000000000000166");
//    mTransferData.setNameAndValue("SubMissionID", "1");
//    核保订正
//        mTransferData.setNameAndValue("ContNo", "130110000000139");
//        mTransferData.setNameAndValue("PrtNo", "21210000000009");
//        mTransferData.setNameAndValue("AgentCode", "8611000433");
//        mTransferData.setNameAndValue("MakeDate", "2004-11-22");
//        mTransferData.setNameAndValue("AppntNo", "0000031226");
//        mTransferData.setNameAndValue("AppntName", "张三");
//        mTransferData.setNameAndValue("InsuredNo", "0000031226");
//        mTransferData.setNameAndValue("InsuredName", "张三");
//        mTransferData.setNameAndValue("UWFlag", "a");
//        mTransferData.setNameAndValue("MissionID", "00000000000000000202");
//        mTransferData.setNameAndValue("SubMissionID", "4");

        /** 传递变量 */
    //生成检通知书测试
//    ActivityID = "0000001101";
//
//    String prtNo = "160000722851";
//    String contNo = "13002387156";
//    String customerNo = "001997275";
//
//	LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
//    tLCPENoticeSchema.setContNo(contNo);
//	tLCPENoticeSchema.setPEAddress("");
//	tLCPENoticeSchema.setPEDate("2007-06-07");
//	tLCPENoticeSchema.setPEBeforeCond("N");
//	tLCPENoticeSchema.setRemark("ReMark");
//	tLCPENoticeSchema.setCustomerNo(customerNo);
////
//	LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
//    LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();
//    tLCPENoticeItemSchema.setContNo(contNo);
//    tLCPENoticeItemSchema.setPEItemCode("ZH0001");
//    tLCPENoticeItemSchema.setPEItemName("肝功三项（ALT＋AST＋GGT）");
//    tLCPENoticeItemSchema.setFreePE("N");
//    tLCPENoticeItemSchema.setTestGrpCode("HB0005");
//    tLCPENoticeItemSet.add(tLCPENoticeItemSchema);
//
//    LCPENoticeItemSchema tLCPENoticeItemSchema2 = new LCPENoticeItemSchema();
//    tLCPENoticeItemSchema2.setContNo(contNo);
//    tLCPENoticeItemSchema2.setPEItemCode("ZH0002");
//    tLCPENoticeItemSchema2.setPEItemName("血脂（TC+TG)");
//    tLCPENoticeItemSchema2.setFreePE("N");
//    tLCPENoticeItemSchema2.setTestGrpCode(tLCPENoticeItemSchema.getTestGrpCode());
//    tLCPENoticeItemSet.add(tLCPENoticeItemSchema);
//
//    mTransferData.setNameAndValue("ContNo", contNo);
//    mTransferData.setNameAndValue("PrtNo", prtNo);
//    mTransferData.setNameAndValue("CustomerNo", customerNo);
//    mTransferData.setNameAndValue("MissionID", "00000000000000069170");
//    mTransferData.setNameAndValue("SubMissionID", "1");
//    mTransferData.setNameAndValue("LCPENoticeSchema", tLCPENoticeSchema);
//    mTransferData.setNameAndValue("LCPENoticeItemSet", tLCPENoticeItemSet);

    //发体检通知书测试
//    LCContSchema tLCContSchema = new LCContSchema();
//    tLCContSchema.setContNo(contNo);
//    tLCContSchema.setInsuredNo(customerNo);

//        mTransferData.setNameAndValue("LCPENoticeItemSet",tLCPENoticeItemSet);
//        mTransferData.setNameAndValue("LCPENoticeSchema",tLCPENoticeSchema);
//        mTransferData.setNameAndValue("MissionID","00000000000000000001");
//	mTransferData.setNameAndValue("SubMissionID","1");
//        mTransferData.setNameAndValue("SubMissionID","1");
//        mTransferData.setNameAndValue("CustomerNo","0000031226");
//        mTransferData.setNameAndValue("ContNo","00000000000000000005");
        /**
         * 打印体检通知书 0000001106
         */
//        ActivityID = "0000001022";
//        mTransferData.setNameAndValue("PrtSeq","160000722851");
//    	mTransferData.setNameAndValue("Code","03") ;
//    	mTransferData.setNameAndValue("ContNo","13002387156") ;
//    	mTransferData.setNameAndValue("MissionID","00000000000000069170") ;
//        mTransferData.setNameAndValue("SubMissionID","1") ;
        //回收体检通知书
//	  LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
//	   tLZSysCertifySchema.setCertifyCode( "8888" );
//	   tLZSysCertifySchema.setCertifyNo( "810000000000634" );
//	   tLZSysCertifySchema.setTakeBackOperator( "001" );
//	   tLZSysCertifySchema.setTakeBackDate( "2004-03-31" );
//	   tLZSysCertifySchema.setTakeBackMakeDate( "2004-03-31" );
//	   tLZSysCertifySchema.setSendOutCom( "A86" );
//	   tLZSysCertifySchema.setReceiveCom( "D86110484" );
//	   String tOperate = new String();
//	   TransferData tTransferData = new TransferData();
//	   mTransferData.setNameAndValue("CertifyNo","810000000000634");
//	   mTransferData.setNameAndValue("CertifyCode","7777") ;
//	   mTransferData.setNameAndValue("ContNo","130110000008969") ;
//	   mTransferData.setNameAndValue("MissionID","00000000000000000559") ;
//	   mTransferData.setNameAndValue("SubMissionID","1") ;
//	   mTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
        /**
         * 生调通知书测试 0000001104
         */
//        ActivityID = "0000001104";
//        LCRReportSchema tLCRReportSchema = new LCRReportSchema();
//        LCRReportItemSchema tLCRReportItemSchema = new LCRReportItemSchema();
//        LCRReportItemSet tLCRReportItemSet = new LCRReportItemSet();
//
//        tLCRReportSchema.setContNo("13000017530");
//        tLCRReportSchema.setContente("4564646456456456456");
//
//        tLCRReportItemSchema.setRReportItemCode("2");
//        tLCRReportItemSchema.setRReportItemName("健康状况");
//        tLCRReportItemSet.add(tLCRReportItemSchema);
//
//        mTransferData.setNameAndValue("ContNo", "00001069902");
//        mTransferData.setNameAndValue("PrtNo", "20050627888");
//        mTransferData.setNameAndValue("MissionID", "00000000000000002125");
//        mTransferData.setNameAndValue("CustomerNo", "000010508");
//        mTransferData.setNameAndValue("SubMissionID", "1");
//        mTransferData.setNameAndValue("LCRReportItemSet", tLCRReportItemSet);
//        mTransferData.setNameAndValue("LCRReportSchema",tLCRReportSchema);
//打印生调通知书
//    mTransferData.setNameAndValue("PrtSeq", "810000000000654");
//    mTransferData.setNameAndValue("Code", "85");
//    mTransferData.setNameAndValue("ContNo", "130110000013771");
//    mTransferData.setNameAndValue("PrtNo", "000324005");
//    mTransferData.setNameAndValue("MissionID", "00000000000000005155");
//    mTransferData.setNameAndValue("SubMissionID", "2");
        //补打生调通知书
//    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//    tLOPRTManagerSchema.setPrtSeq("86000020040810000157") ;
//	mTransferData.setNameAndValue("Code","04") ;
//	mTransferData.setNameAndValue("ContNo","00000000000000000006") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000028") ;
//	mTransferData.setNameAndValue("SubMissionID","2") ;
//    mTransferData.setNameAndValue("LOPRTManagerSchema",tLOPRTManagerSchema) ;
//    //回收生调通知书
//    mTransferData.setNameAndValue("ContNo", "00000000000000000006");
//    mTransferData.setNameAndValue("MissionID", "00000000000000000028");
//    mTransferData.setNameAndValue("SubMissionID", "1");
//    mTransferData.setNameAndValue("PrtNo", "00002004111207");
//    mTransferData.setNameAndValue("PrtSeq", "86000020040810000163");
//    LCRReportSchema tLCRReportSchema = new LCRReportSchema();
//    tLCRReportSchema.setContNo("00000000000000000006");
//    tLCRReportSchema.setPrtSeq("86000020040810000163");
//    tLCRReportSchema.setReplyContente("回复内容:dfdfdfd");
//    mTransferData.setNameAndValue("LCRReportSchema", tLCRReportSchema);
//	//加费录入测试
//	//生调通知书测试
//    //发送核保通知书
//	mTransferData.setNameAndValue("ContNo","130110000000174");
//	mTransferData.setNameAndValue("PrtNo","00002004120211") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000219");
//	mTransferData.setNameAndValue("SubMissionID","1");
        //下核保结论
//	mTransferData.setNameAndValue("ContNo","130110000000188");
//	mTransferData.setNameAndValue("PrtNo","21210000000021") ;
//        mTransferData.setNameAndValue("UWFlag","1") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000227");
//	mTransferData.setNameAndValue("SubMissionID","1");

        //机构问题件回收
//        mTransferData.setNameAndValue("ContNo","130110000000175");
//        mTransferData.setNameAndValue("MissionID","00000000000000000220");
//        mTransferData.setNameAndValue("SubMissionID","1");

//	//打印核保通知书
//	mTransferData.setNameAndValue("PrtSeq","810000000000611");
//	mTransferData.setNameAndValue("Code","04");
//	mTransferData.setNameAndValue("ContNo","130110000013726") ;
//	mTransferData.setNameAndValue("PrtNo","19800000012") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000005138") ;
//        mTransferData.setNameAndValue("SubMissionID","3") ;
        //补核保通知书
//    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//    tLOPRTManagerSchema.setPrtSeq("810000000000035") ;
//	mTransferData.setNameAndValue("Code","03") ;
//	mTransferData.setNameAndValue("ContNo","00000000000000000006") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000001") ;
//	mTransferData.setNameAndValue("SubMissionID","1") ;
//    mTransferData.setNameAndValue("LOPRTManagerSchema",tLOPRTManagerSchema) ;

        // 准备传输工作流数据 VData
//	mTransferData.setNameAndValue("PolNo","86110020030210001017");
//	mTransferData.setNameAndValue("EdorNo","86110020030410000195");
//	mTransferData.setNameAndValue("MissionID","00000000000000000017");
        //mTransferData.setNameAndValue("LCUWMasterMainSchema",mLCUWMasterMainSchema);
//
        //回收核保通知书
//        LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
//         tLZSysCertifySchema.setCertifyCode( "9999" );
//         tLZSysCertifySchema.setCertifyNo( "86000020040810000190" );
//         tLZSysCertifySchema.setTakeBackOperator( "001" );
//         tLZSysCertifySchema.setTakeBackDate( "2004-12-01" );
//         tLZSysCertifySchema.setTakeBackMakeDate( "2004-12-01" );
//         tLZSysCertifySchema.setSendOutCom( "A86" );
//         tLZSysCertifySchema.setReceiveCom( "D8611000439 " );

//         String tOperate = new String();
//         TransferData tTransferData = new TransferData();
//         mTransferData.setNameAndValue("CertifyNo","86000020040810000190");
//         mTransferData.setNameAndValue("CertifyCode","9999") ;
//         mTransferData.setNameAndValue("ContNo","130110000000139") ;
//         mTransferData.setNameAndValue("MissionID","00000000000000000202") ;
//         mTransferData.setNameAndValue("SubMissionID","4") ;
//         mTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
//
//	//打印业务员通知书
//	mTransferData.setNameAndValue("PrtSeq","810000000000010");
//	mTransferData.setNameAndValue("Code","14") ;
//	mTransferData.setNameAndValue("ContNo","00000000000000000006") ;
//	mTransferData.setNameAndValue("PrtNo","00002004111207") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000001") ;
//        mTransferData.setNameAndValue("SubMissionID","1") ;
        //补业务员通知书
//    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
//    tLOPRTManagerSchema.setPrtSeq("810000000000010") ;
//	mTransferData.setNameAndValue("Code","14") ;
//	mTransferData.setNameAndValue("ContNo","00000000000000000006") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000001") ;
//	mTransferData.setNameAndValue("SubMissionID","1") ;
//    mTransferData.setNameAndValue("LOPRTManagerSchema",tLOPRTManagerSchema) ;

        //回收业务员通知书
//        LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
//         tLZSysCertifySchema.setCertifyCode( "1113" );
//         tLZSysCertifySchema.setCertifyNo("810000000000611" );
//         tLZSysCertifySchema.setTakeBackOperator( "001" );
//         tLZSysCertifySchema.setTakeBackDate( "2004-03-31" );
//         tLZSysCertifySchema.setTakeBackMakeDate( "2004-03-31" );
//         tLZSysCertifySchema.setSendOutCom( "A86" );
//         tLZSysCertifySchema.setReceiveCom( "D8611000434 " );
//
//         String tOperate = new String();
//         TransferData tTransferData = new TransferData();
//         mTransferData.setNameAndValue("CertifyNo","810000000000611");
//         mTransferData.setNameAndValue("CertifyCode","1113") ;
//         mTransferData.setNameAndValue("ContNo","130110000013726") ;
//         mTransferData.setNameAndValue("MissionID","00000000000000005138") ;;
//         mTransferData.setNameAndValue("SubMissionID","3") ;
//         mTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
//
//         LCRReportSchema tLCRReportSchema = new LCRReportSchema();
//         tLCRReportSchema.setPrtSeq("810000000000611");
//         mTransferData.setNameAndValue("LCRReportSchema",tLCRReportSchema);

//   复核修改确认
//    mTransferData.setNameAndValue("ContNo", "86110020040990000060");
//    mTransferData.setNameAndValue("PrtNo", "21210000000004");
//    mTransferData.setNameAndValue("MissionID", "00000000000000000193");
//    mTransferData.setNameAndValue("SubMissionID", "2");
//    //精算数据导入
//	mTransferData.setNameAndValue("FileName","Data000000JS2004062.xls");
//	mTransferData.setNameAndValue("ConfigFileName","ExcelImportLFJSConfig.xml");
//    mTransferData.setNameAndValue("ItemType","04") ;
//	mTransferData.setNameAndValue("StatYear","2004") ;
//	mTransferData.setNameAndValue("StatMon","01");
//	mTransferData.setNameAndValue("MissionID","00000000000000000116");
//	mTransferData.setNameAndValue("SubMissionID","1");00000000000000000117
//    //精算数据导入确认
//	mTransferData.setNameAndValue("FileName","Data000000JS2004062.xls");
//	mTransferData.setNameAndValue("ConfigFileName","ExcelImportLFJSConfig.xml");
//    mTransferData.setNameAndValue("ItemType","07") ;
//	mTransferData.setNameAndValue("StatYear","2004") ;
//	mTransferData.setNameAndValue("StatMon","01");
//	mTransferData.setNameAndValue("MissionID","00000000000000000117");
//	mTransferData.setNameAndValue("SubMissionID","1");
//    //业务数据导入确认
//	mTransferData.setNameAndValue("FileName","Data000000JS2004062.xls");
//	mTransferData.setNameAndValue("ConfigFileName","ExcelImportLFJSConfig.xml");
//	mTransferData.setNameAndValue("ItemType","01") ;
//	mTransferData.setNameAndValue("StatYear","2004") ;
//	mTransferData.setNameAndValue("StatMon","07");
//	mTransferData.setNameAndValue("MissionID","00000000000000000117");
//	mTransferData.setNameAndValue("SubMissionID","1");
//
//    LCContSchema tLCContSchema = new LCContSchema();
//    tLCContSchema.setContNo("00000000000000000005");
//    mTransferData.setNameAndValue("LCContSchema", tLCContSchema);
//    mTransferData.setNameAndValue("MissionID", "00000000000000000001");
//    mTransferData.setNameAndValue("SubMissionID", "1");
//
        //准备公共传输信息
//	mTransferData.setNameAndValue("WhereSQL", " ||  ||AND ItemType In ('X1','X2','X3','X4','X5','X6','X7')  AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsMon='1') order by ItemNum");
//	mTransferData.setNameAndValue("NeedItemKey", "1");
//	mTransferData.setNameAndValue("ReportDate", "2003-10-01");
//	mTransferData.setNameAndValue("makedate","2004-07-12");
//	mTransferData.setNameAndValue("maketime","10:10:10");
//	mTransferData.setNameAndValue("sDate", "2003-10-01");
//    mTransferData.setNameAndValue("eDate", "2003-10-31");

//
//    //准备公共传输信息
//	mTransferData.setNameAndValue("PolNo","86110020030210004500");
//	mTransferData.setNameAndValue("EdorNo","86110020030410000213") ;
//	mTransferData.setNameAndValue("MissionID","00000000000000000028");
//	mTransferData.setNameAndValue("PrtSeq","86000020030810002084");
//	mTransferData.setNameAndValue("SubMissionID","3");
//	mTransferData.setNameAndValue("LCRReportSchema",tLCRReportSchema);

        // 准备传输工作流数据 VData
//	mTransferData.setNameAndValue("StatYear","2004") ;
//	mTransferData.setNameAndValue("StatMonth","01");


//    mTransferData.setNameAndValue("MissionID","00000000000000000110");
//    mTransferData.setNameAndValue("SubMissionID","1");

//	VData tVData3 = new VData();
//    mTransferData.setNameAndValue("PolNo","86110020030210035008");
//    mTransferData.setNameAndValue("EdorNo","86110020040410000039") ;
//    mTransferData.setNameAndValue("MissionID","00000000000000000023");
//    mTransferData.setNameAndValue("SubMissionID","1");

        /**
         * 签单测试
         * 0000001150
         */
        ActivityID = "0000002006";

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("1400012787");

        mTransferData.setNameAndValue("GrpContNo", tLCGrpContSchema.getGrpContNo());
        mTransferData.setNameAndValue("MissionID", "00000000000000081424");
        mTransferData.setNameAndValue("SubMissionID", "1");

        tVData.add(tLCGrpContSchema);


        //下核保结论
//    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
//    tLCUWMasterSchema.setProposalNo("86110020040110000987");
//    tLCUWMasterSchema.setUWIdea("延期承保");
//    tLCUWMasterSchema.setPostponeDay("2天");
//    tLCUWMasterSchema.setPassFlag("2");

        // 准备传输工作流数据 VData
//    mTransferData.setNameAndValue("ContNo","130110000000289");
//    mTransferData.setNameAndValue("PrtNo","21210000000072");
//    mTransferData.setNameAndValue("AppUser","001");
//    mTransferData.setNameAndValue("UWFlag","1");
//    mTransferData.setNameAndValue("UWIdea","asdf");
//    mTransferData.setNameAndValue("MissionID","00000000000000000255");
//    mTransferData.setNameAndValue("SubMissionID","1");
//    mTransferData.setNameAndValue("LCUWMasterSchema",tLCUWMasterSchema);
        /**
         * 客户内容变更通知书
         */
//        mTransferData.setNameAndValue("ContNo", "13000002628");
//        mTransferData.setNameAndValue("PrtNo", "86110030004");
//        mTransferData.setNameAndValue("MissionID", "00000000000000002758");
//        mTransferData.setNameAndValue("SubMissionID", "1");
//        mTransferData.setNameAndValue("UWResult", "UWResult");
        /**
         * 录入完毕
         */
//        mTransferData.setNameAndValue("ContNo",
//                                      "13000000053");
//        mTransferData.setNameAndValue("PrtNo", "11000000000");
//        mTransferData.setNameAndValue("AppntNo", "000000022");
//        mTransferData.setNameAndValue("AppntName", "测试2");
//        mTransferData.setNameAndValue("AgentCode", "1101000001");
//        mTransferData.setNameAndValue("ManageCom", "86110000");
//        mTransferData.setNameAndValue("Operator", "picch");
//        mTransferData.setNameAndValue("MakeDate", PubFun.getCurrentDate());
//        mTransferData.setNameAndValue("MissionID", "00000000000000000019");
//        mTransferData.setNameAndValue("SubMissionID",
//                                      "1");
        /**
         * 人工核保0000001110
         */
//        ActivityID = "0000001110";
//        mTransferData.setNameAndValue("ContNo","13000031278");
//        mTransferData.setNameAndValue("PrtNo","20050817002");
//        mTransferData.setNameAndValue("UWFlag","4");
//        mTransferData.setNameAndValue("UWIdea","21123");
//        mTransferData.setNameAndValue("MissionID","00000000000000001654");
//        mTransferData.setNameAndValue("SubMissionID","1");
//        LCContSet tLCContSet = new LCContSet();
//        LCContSchema tLCContSchema = new LCContSchema();
//        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
//        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
//        tLCContSchema.setContNo("13000031278");
//        tLCContSchema.setUWFlag("4");
//        tLCContSchema.setRemark("sdfsdf");
//        tLCCUWMasterSchema.setContNo("13000031278");
//
//        tLCCUWMasterSchema.setUWIdea("234234234");
//        tLCCUWMasterSchema.setSugPassFlag("4");
//        tLCCUWMasterSchema.setSugUWIdea("fsdfsfsd");
//
//        tLCContSet.add(tLCContSchema);
//        tLCCUWMasterSet.add(tLCCUWMasterSchema);
//        mTransferData.setNameAndValue("LCCUWMasterSchema", tLCCUWMasterSchema);
//        tVData.add(tLCContSet);
//        tVData.add(tLCCUWMasterSet);

        /**
         * 发问题件通知书 0000001022
         */
//        ActivityID = "0000001022";
//        mTransferData.setNameAndValue("ContNo", "13002396484");
//        mTransferData.setNameAndValue("PrtNo", "160000837690");
//        mTransferData.setNameAndValue("MissionID", "00000000000000069966");
//        mTransferData.setNameAndValue("SubMissionID", "1");

        /**
         * 首次进人工核保
         * 0000001110
         */
//        ActivityID = "0000001110";
//        mTransferData.setNameAndValue("ContNo", "13002385823");
//        mTransferData.setNameAndValue("PrtNo", "160000861383");
//        mTransferData.setNameAndValue("UWFlag", "4");
//        mTransferData.setNameAndValue("UWIdea", "");
//        mTransferData.setNameAndValue("MissionID", "00000000000000069009");
//        mTransferData.setNameAndValue("SubMissionID", "1");
//        mTransferData.setNameAndValue("NeedSendNotice", "1");
//
//        LCContSchema tLCContSchema = new LCContSchema();
//        tLCContSchema.setContNo("13002385823");
//        tLCContSchema.setProposalContNo(tLCContSchema.getContNo());
//        tLCContSchema.setUWFlag("4");
//        tLCContSchema.setRemark("");
//        LCContSet tLCContSet = new LCContSet();
//        tLCContSet.add(tLCContSchema);
//
//        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
//        tLCCUWMasterSchema.setProposalContNo(tLCContSchema.getProposalContNo());
//        tLCCUWMasterSchema.setUWIdea("");
//        tLCCUWMasterSchema.setSugPassFlag("4");
//        tLCCUWMasterSchema.setSugUWIdea("");
//        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
//        tLCCUWMasterSet.add(tLCCUWMasterSchema);
//
//        tVData.add(tLCContSet);
//        tVData.add(tLCCUWMasterSet);

        /**
         * 核保订正
         *
         */
//        ActivityID = "0000001149";
//        LCPolSet tLCPolSet = new LCPolSet();
//        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
//        mTransferData.setNameAndValue("UWFlag", "z");
//        mTransferData.setNameAndValue("ContNo", "13000035695");
//        mTransferData.setNameAndValue("AppntName", "");
//        mTransferData.setNameAndValue("InsuredName", "");
//        mTransferData.setNameAndValue("AgentGroup", "");
//        mTransferData.setNameAndValue("MissionID", "00000000000000002002");
//        mTransferData.setNameAndValue("SubMissionID", "2");

        /**
         * 保全送核
         *
         */
//        ActivityID = "0000001180";
//        LCPolSet tLCPolSet = new LCPolSet();
//        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
//        mTransferData.setNameAndValue("UWFlag", "z");
//        mTransferData.setNameAndValue("ContNo", "00001285501");
//        mTransferData.setNameAndValue("PrtNo", "20050802089");
//        mTransferData.setNameAndValue("MissionID", "00000000000000002127");
//        mTransferData.setNameAndValue("SubMissionID", "1");


        //无扫描录入
//        ActivityID = "7799999999";
//        mTransferData.setNameAndValue("PrtNo", "20070423001");
//        mTransferData.setNameAndValue("ManageCom", "86110000");
//        mTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
//        mTransferData.setNameAndValue("Operator", mGlobalInput.Operator);

        /**总变量*/
        tVData.add(mGlobalInput);
        tVData.add(mTransferData);
        TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
        try {
            if (tTbWorkFlowUI.submitData(tVData, ActivityID)) {
                VData tResult = new VData();

                //tResult = tActivityOperator.getResult() ;
            } else {
                System.out.println(tTbWorkFlowUI.mErrors.getError(0).
                                   errorMessage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        TbWorkFlowBL tTbWorkFlowBL = new TbWorkFlowBL();

        System.out.println("---TbWorkFlowBL UI BEGIN---");
        if (tTbWorkFlowBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tTbWorkFlowBL.mErrors);
            mResult.clear();
            return false;
        }
        mResult = tTbWorkFlowBL.getResult();
        return true;
    }

    /**
     * 传输数据的公共方法
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
