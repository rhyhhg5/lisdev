package com.sinosoft.workflow.tb;

import com.sinosoft.lis.cbcheck.GrpUWSendPrintUI;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.AfterInitService;

/**
 * <p>Title: 工作流服务类:再保审核 </p>
 * <p>Description: 再保审核工作流AfterInit服务类</p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class GrpRIUWChkAfterInitService implements AfterInitService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 业务处理相关变量 */
    private String mGrpContNo = "";

    private String mConclusion = ""; //结论 0 同意；1不同意；2需核保审核

    private String mAuditOpinion = ""; //审核意见

    /**团体合同表*/
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    /** 数据操作字符串 */
    private String mOperator;
    
    private String mManagecom;
    
    private LRUWMasterSet mLRUWMasterSet = new LRUWMasterSet();
    
    private LWMissionSet mLWMissionSet = new LWMissionSet();
    
    private String mMissionID;

    public GrpRIUWChkAfterInitService()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        System.out.println("---GrpRIUWChkBL getInputData OK---");

        //判断核保级别
        if (!checkUWGrade(mLCGrpContSchema))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        
        System.out.println("dealData successful!");
        
        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start  Submit...");

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	LRUWMasterSchema tLRUWMasterSchema = new LRUWMasterSchema();
    	String tserialno = PubFun1.CreateMaxNo("LRUWMSerNo",20);
    	String tSql = "select max(audittimes) from lruwmaster where prtno = '"+mLCGrpContSchema.getPrtNo()+"' and auditflag = '0' ";
        String uwtimes = new ExeSQL().getOneValue(tSql);
        if(uwtimes == null || "".equals(uwtimes)){
        	uwtimes = "0";
        }
        int mUwTimes = Integer.parseInt(uwtimes)+1;
    	tLRUWMasterSchema.setSerialNo(tserialno);
    	tLRUWMasterSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
    	tLRUWMasterSchema.setConclusion(mConclusion);
    	tLRUWMasterSchema.setAuditOpinion(mAuditOpinion);
    	tLRUWMasterSchema.setAuditFlag("0");
    	tLRUWMasterSchema.setAuditTimes(mUwTimes+"");
    	tLRUWMasterSchema.setManageCom(mManagecom);
    	tLRUWMasterSchema.setOperator(mOperator);
    	tLRUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
    	tLRUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
    	tLRUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
    	tLRUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
    	mLRUWMasterSet.add(tLRUWMasterSchema);
    	
    	/**
         * 添加核保结论是0(同意)的自动发首期交费通知书
         */
        if ("0".equals(mConclusion))
        {
        	mLCGrpContSchema.setUWFlag("9");//将保单的核保状态变为核保通过。
        	
        	LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode("57");
            //校验是否发过首期交费通知书 ，如果发过则不再重发
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
            if (tLOPRTManagerDB.query().size() <= 0)
            {
                VData tempVData = new VData();
                tempVData.add(tLOPRTManagerSchema);
                tempVData.add(mGlobalInput);

                GrpUWSendPrintUI tGrpUWSendPrintUI = new GrpUWSendPrintUI();
                if (!tGrpUWSendPrintUI.submitData(tempVData, "INSERT"))
                {
                    this.mErrors.copyAllErrors(tGrpUWSendPrintUI.mErrors);
                    return false;
                }
            }
        }
        return true;
    }




    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mOperator = mGlobalInput.Operator;
        mManagecom = mGlobalInput.ManageCom;
        
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null || "".equals(mMissionID))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpUWModifyAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //取投保单
        mLCGrpContSet.set((LCGrpContSet) cInputData.getObjectByObjectName("LCGrpContSet", 0));

        int n = mLCGrpContSet.size();
        if (n == 1)
        {
            LCGrpContSchema tLCGrpContSchema = mLCGrpContSet.get(1);
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();

            mGrpContNo = tLCGrpContSchema.getGrpContNo();
            mConclusion = tLCGrpContSchema.getUWFlag();
            mAuditOpinion = tLCGrpContSchema.getRemark();
            System.out.println("mConclusion=" + mConclusion);

            //校验是不是以下核保结论
            if (mConclusion == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpUWManuNormChkBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有选择再保审核结论";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (mConclusion.equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpUWManuNormChkBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有选择再保审核结论";
                this.mErrors.addOneError(tError);
                return false;
            }

            tLCGrpContDB.setGrpContNo(mGrpContNo);
            if (tLCGrpContDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpUWManuNormChkBL";
                tError.functionName = "getInputData";
                tError.errorMessage = mGrpContNo + "集体合同单查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                tLCGrpContSchema.setSchema(tLCGrpContDB);
                mLCGrpContSchema.setSchema(tLCGrpContDB);
            }

        }
        else
        {
            return false;
        }
        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();
        
//      获取核保订正工作流，并删除
        if(!prepareMission()){
        	CError tError = new CError();
            tError.moduleName = "GrpRICUWChkBL";
            tError.functionName = "prepareMission";
            tError.errorMessage = "获取核保订正工作流失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLWMissionSet != null && mLWMissionSet.size() > 0)
        {
            map.put(mLWMissionSet, "DELETE");
        }

        map.put(mLRUWMasterSet, "INSERT");
        map.put(mLCGrpContSchema, "UPDATE");
        mResult.add(map);
        return true;
    }

    /**
     * 校验核保员级别
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkUWGrade(LCGrpContSchema tLCGrpContSchema)
    {
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mOperator);

        if (!tLDUserDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "GrpUWManuNormChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此操作员信息，不能再保审核!（操作员：" + mOperator + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return
     */
    private boolean prepareTransferData()
    {
        mTransferData.setNameAndValue("Conclusion",mConclusion );
        mTransferData.setNameAndValue("GrpContNo", mGrpContNo);
        mTransferData.setNameAndValue("PrtNo", mLCGrpContSchema.getPrtNo());
        mTransferData.setNameAndValue("AgentCode", mLCGrpContSchema.getAgentCode());
        mTransferData.setNameAndValue("AgentGroup", mLCGrpContSchema.getAgentGroup());
        mTransferData.setNameAndValue("SaleChnl", mLCGrpContSchema.getSaleChnl());
        mTransferData.setNameAndValue("ManageCom", mLCGrpContSchema.getManageCom());
        mTransferData.setNameAndValue("GrpNo", mLCGrpContSchema.getAppntNo());
        mTransferData.setNameAndValue("GrpName", mLCGrpContSchema.getGrpName());
        mTransferData.setNameAndValue("CValiDate", mLCGrpContSchema.getCValiDate());
        return true;
    }

    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }
    
    /**
     * 获取核保订正工作流
     * */
    private boolean prepareMission()
    {
        String tStr = "Select * from LWMission where MissionID = '" +
                      mMissionID + "'"
                      + " and ActivityID = '0000002005'"
                      ;
        LWMissionDB tLWMissionDB = new LWMissionDB();
        mLWMissionSet = tLWMissionDB.executeQuery(tStr);
        if (mLWMissionSet == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "prepareMission";
            tError.errorMessage = "工作流起始任务节点查询出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLWMissionSet.size() < 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "prepareMission";
            tError.errorMessage = "工作流起始任务节点LWMission查询出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
