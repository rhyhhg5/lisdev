package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.LBMissionDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.*;

public class ContReturnBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	Reflections tReflections = new Reflections();
	private GlobalInput mGlobalInput = new GlobalInput();
	private TransferData mTransferData = new TransferData();
    private LCContSchema mLCContSchema = new LCContSchema();
    private LJSPaySet mLJSPaySet;
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
	/** 数据操作字符串 */
	private String mOperater;
	private String mManageCom;
	private MMap mMap = new MMap();
	VData tVData = new VData();
	LWMissionSchema mLWMissionDB = new LWMissionSchema();

	public ContReturnBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据,将数据备份到本类中
		if (getSubmitMap(cInputData, cOperate) == null) {
			return false;
		}
		System.out.println("---ContReturnBL getInputData---");
		// 数据操作业务处理

		if (!submit()) {
			return false;
		}

		return true;
	}

	private boolean submit() {
		VData data = new VData();
		data.add(mMap);

		PubSubmit p = new PubSubmit();
		if (!p.submitData(data, "")) {
			System.out.println("提交数据失败");
			buildError("submitData", "提交数据失败");
			return false;
		}

		return true;
	}

	private MMap getSubmitMap(VData cInputData, String cOperate) {

		if (!getInputData(cInputData, cOperate)) {
			return null;
		}

		if (!dealData()) {
			return null;
		}

		return mMap;
	}

	private boolean getInputData(VData cInputData, String cOperate) { // 从输入数据中得到所有对象
		// 获得全局公共数据
		mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		if (mGlobalInput == null) {
			CError tError = new CError();
			tError.moduleName = "ContReturnBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		// 获得操作员编码
		mOperater = mGlobalInput.Operator;
		if ((mOperater == null) || mOperater.trim().equals("")) {
			CError tError = new CError();
			tError.moduleName = "ContReturnBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据Operate失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		// 获得登陆机构编码
		mManageCom = mGlobalInput.ManageCom;
		if ((mManageCom == null) || mManageCom.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "ContReturnBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}

	private boolean dealData() {
		MMap tTmpMap = null;
		String ActivityID = (String) mTransferData.getValueByName("ActivityID");
		String MissionId = (String) mTransferData.getValueByName("MissionID");
		String SubMissionID = (String) mTransferData
				.getValueByName("SubMissionID");
		// String PrtNo = (String) mTransferData.getValueByName("PrtNo");
		// String ContNo = (String) mTransferData.getValueByName("ContNo");
		System.out.println("ActivityID" + ActivityID);
		System.out.println("MissionId" + MissionId);
		 //首期交费通知书问题
		   if (!prepareSendInfo())
	        {
	            return false;
	        }
	    /**
	     * 处理财务数据，未到帐确认
	     * @return boolean
	     */
		if (!prepareFinFee())
        {
            return false;
        }
		LWMissionSet tLWMissionSet = getMissionTrace(MissionId, SubMissionID,
				ActivityID);
		if (tLWMissionSet.size() <= 0) {
			buildError("dealData", "未找到要处理的工作流信息。");
			return false;
		}
		// 备份W表的工作流。
		tTmpMap = backMissionTrace(tLWMissionSet);
		if (tTmpMap == null) {
			return false;
		}
		mMap.add(tTmpMap);

		tTmpMap = delMissionTrace(tLWMissionSet);
		if (tTmpMap == null) {
			return false;
		}
		mMap.add(tTmpMap);
		//	// 备份B表的工作流
		LBMissionSet tLBMissionSet = getMissionTrace1(MissionId, SubMissionID,
				ActivityID);
		if (tLBMissionSet.size() <= 0) {
			buildError("dealData", "未找到要处理的工作流信息。");
			return false;
		}
	

			tTmpMap = backMissionTrace(tLBMissionSet);
			if (tTmpMap == null) {
				return false;
			}

		mMap.add(tTmpMap);

		return true;
	}


	private boolean prepareSendInfo() {
		String tContNo = (String) mTransferData.getValueByName("ContNo");
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String StrSQL;
        tLOPRTManagerDB.setOtherNo(tContNo);
        tLOPRTManagerDB.setCode("07");
        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB
                .executeQuery("select * from loprtmanager where otherno='"
                        + tContNo
                        + "' and code in ('07','05','06','05_T')");
        if (tLOPRTManagerDB != null)
        {
            if (tLOPRTManagerSet.size() > 0)
            {
                mLOPRTManagerSet.set(tLOPRTManagerSet);
            }
            if (tLOPRTManagerSet.size() > 0)
            {
                for (int i = 1; i <= tLOPRTManagerSet.size(); i++)
                {
                    tLOPRTManagerSchema = tLOPRTManagerSet.get(i);
                    if (StrTool.cTrim(tLOPRTManagerSchema.getCode()).equals(
                            "05")
                            || StrTool.cTrim(tLOPRTManagerSchema.getCode())
                                    .equals("05_T"))
                    {
                        StrSQL = "delete from LZSysCertify where CertifyNo='"
                                + tLOPRTManagerSchema.getPrtSeq() + "'";
                        mMap.put(StrSQL, "DETELE");

                        // 如果核保件回销，需同时清除核保件。
                        try
                        {
                            MMap tTmpMap = dealUWResultPrint(tLOPRTManagerSchema
                                    .getPrtSeq());
                            if (tTmpMap != null)
                            {
                                mMap.add(tTmpMap);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            return false;
                        }
                        // ------------------------------
                    }
                }
            }
        }

        return true;
    }

    private MMap dealUWResultPrint(String tSUWResultPrtSeq) throws Exception
    {
        MMap tTmpMap = null;

        String tStrSql = "select DocId from Es_Doc_Main edm "
                + " where SubType = 'TB21' " + " and edm.DocCode = '"
                + tSUWResultPrtSeq + "' ";
        SSRS tSsDocId = new ExeSQL().execSQL(tStrSql);
        if (tSsDocId.getMaxRow() >= 2)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "dealUWResultPrint";
            tError.errorMessage = "已回复的核保件查询有误。核保通知书号：" + tSUWResultPrtSeq;
            this.mErrors.addOneError(tError);
            throw new Exception(tError.errorMessage);
        }

        if (tSsDocId.getMaxRow() == 1)
        {
            String tDocId = tSsDocId.GetText(1, 1);
            tTmpMap = new MMap();

            tTmpMap.put(
                    "insert into Es_Doc_MainB (select * from Es_Doc_Main edm where edm.DocId = "
                            + tDocId + ")", SysConst.INSERT);
            tTmpMap.put("delete from Es_Doc_Main edm where edm.DocId = "
                    + tDocId, SysConst.DELETE);

            tTmpMap.put(
                    "insert into Es_Doc_PagesB (select * from Es_Doc_Pages edp where edp.DocId = "
                            + tDocId + ")", SysConst.INSERT);
            tTmpMap.put("delete from Es_Doc_Pages edp where edp.DocId = "
                    + tDocId, SysConst.DELETE);

            tTmpMap
                    .put(
                            "insert into Es_Doc_RelationB (select * from Es_Doc_Relation edr where edr.DocId = "
                                    + tDocId + ")", SysConst.INSERT);
            tTmpMap.put("delete from Es_Doc_Relation edr where edr.DocId = "
                    + tDocId, SysConst.DELETE);
        }

        return tTmpMap;
    }

	private boolean prepareFinFee() {
	//	String tPrtNo = (String) mTransferData.getValueByName("PrtNo");
		String tContNo = (String) mTransferData.getValueByName("ContNo");
		  LCContDB tLCContDB = new LCContDB();
	        tLCContDB.setContNo(tContNo);
	        if (!tLCContDB.getInfo())
	        {
	            CError tError = new CError();
	            tError.moduleName = "ContReturnBL";
	            tError.functionName = "checkData";
	            tError.errorMessage = "保单" + tContNo + "信息查询失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        mLCContSchema.setSchema(tLCContDB);
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mLCContSchema.getPrtNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() > 0)
        {
            for (int i = 1; i <= tLJSPaySet.size(); i++)
            {
                if (StrTool.cTrim(tLJSPaySet.get(i).getBankOnTheWayFlag())
                        .equals("1"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ContReturnBL";
                    tError.functionName = "prepareFinFee";
                    tError.errorMessage = "银行在途，不能回退！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        mLJSPaySet = tLJSPaySet;
        if (mLJSPaySet != null)
        {
            mMap.put(this.mLJSPaySet, "delete".toUpperCase());
        }

        // 处理财务暂收
        if (!dealTempFeeDatas(mLCContSchema))
        {
            return false;
        }
        // --------------------

        return true;
    }


	private boolean dealTempFeeDatas(LCContSchema cContInfo) {
        ExeSQL tExeSQL = new ExeSQL();
        String tResult = null;
        String tStrSql = null;

        // 如果存在银行在途，发盘数据未锁定，或财务录入但未作财务确认的数据时，不允许回退。
        tResult = null;
        tStrSql = null;
        tStrSql =/* " select 1 from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is not null "
                + " and ljtf.ConfMakeDate is null and ljtf.OtherNoType = '4' "
                + " and ljtf.OtherNo = '"
                + cContInfo.getPrtNo()
                + "' union all "  + */
               " select 1 from LJSPay ljsp where 1 = 1 and (ljsp.CanSendBank is null or ljsp.CanSendBank in ('0')) "
                + " and ljsp.OtherNo = '" + cContInfo.getPrtNo() + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("ContReturnBL_LJTempFee",
                    "存在银行在途、发盘数据未锁定的数据，不允许回退。");
            return false;
        }
        tResult = null;
        tStrSql = null;
        // --------------------

        // 处理非到帐暂收
        tStrSql = null;

        String tPrtNo = cContInfo.getPrtNo();
        tStrSql = " select * from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        LJTempFeeSet tTempFeeSet = new LJTempFeeDB().executeQuery(tStrSql);
        mLJTempFeeSet.set(tTempFeeSet);
        mMap.put(this.mLJTempFeeSet, "delete".toUpperCase());
        tStrSql = null;
        tStrSql = " select * from LJTempFeeClass ljtfc "
                + " where ljtfc.TempFeeNo in ("
                + " select ljtf.TempFeeNo from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "') ";
        LJTempFeeClassSet tTempFeeClassSet = new LJTempFeeClassDB()
                .executeQuery(tStrSql);
        mLJTempFeeClassSet.set(tTempFeeClassSet);
        mMap.put(this.mLJTempFeeClassSet, "delete".toUpperCase());
   
        tStrSql = null;
        // --------------------

        return true;
    }

	private MMap backMissionTrace(LBMissionSet tLBMissionSet) {
		MMap tMMap = null;

		try {
			tMMap = new MMap();

			LWMissionSet cLWMissionSet = new LWMissionSet();
			Reflections tReflections = new Reflections();

			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			tReflections.transFields(tLWMissionSchema, tLBMissionSet.get(1));
			tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
			tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());

			cLWMissionSet.add(tLWMissionSchema);

			tMMap.put(cLWMissionSet, SysConst.INSERT);
		} catch (Exception ex) {
			buildError("backMissionTrace", ex.getMessage());
			return null;
		}

		return tMMap;
	}

	private MMap delMissionTrace(LWMissionSet cLWMissionSet) {
		MMap tMMap = new MMap();

		tMMap.put(cLWMissionSet, SysConst.DELETE);

		return tMMap;
	}

	private MMap backMissionTrace(LWMissionSet cLWMissionSet) {
		MMap tMMap = null;

		try {
			tMMap = new MMap();

			LBMissionSet tLBMissionSet = new LBMissionSet();
			Reflections tReflections = new Reflections();

			for (int i = 1; i <= cLWMissionSet.size(); i++) {
				String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
				System.out.println("生成最大号++++++++++++++++++" + tSerielNo);
				LBMissionSchema tLBMissionSchema = new LBMissionSchema();
				tReflections
						.transFields(tLBMissionSchema, cLWMissionSet.get(i));
				tLBMissionSchema.setSerialNo(tSerielNo);

				tLBMissionSet.add(tLBMissionSchema);
			}
			tMMap.put(tLBMissionSet, SysConst.INSERT);
		} catch (Exception ex) {
			buildError("backMissionTrace", ex.getMessage());
			return null;
		}

		return tMMap;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "UWQuestBackBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private LWMissionSet getMissionTrace(String cMissionID,
			String cSubMissionID, String cActivityID) {
		String tStrSql = " select * from LWMission lwm "
				+ " where MissionID = '" + cMissionID + "' " ;
//				+"and ActivityID not in ('0000001022','0000001101')";
		// + " and SubMissionID = '" + cSubMissionID + "' "
		// + " and ActivityID = '" + cActivityID + "' ";
System.out.println("tStrSql======"+tStrSql);
		LWMissionDB tLWMissionDB = new LWMissionDB();

		return tLWMissionDB.executeQuery(tStrSql);
	}

	private LBMissionSet getMissionTrace1(String cMissionID,
			String cSubMissionID, String cActivityID) {
		String tStrSql = " select * from LBMission lwm "
				+ " where MissionID = '" + cMissionID + "' "
				+"and ActivityID in ('0000007001','0000007003','0000013001')";
		System.out.println("tStrSql======"+tStrSql);
		LBMissionDB tLBMissionDB = new LBMissionDB();

		return tLBMissionDB.executeQuery(tStrSql);
	}

}
