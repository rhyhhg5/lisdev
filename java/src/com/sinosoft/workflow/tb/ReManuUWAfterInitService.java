package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.AfterInitService;

/**
 * <p>Title: 工作流服务类:新契约核保订正</p>
 * <p>Description: 核保订正</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class ReManuUWAfterInitService implements AfterInitService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LWActivitySchema mLWActivitySchema = new LWActivitySchema();

    private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();

    private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();

    private LCUWSendTraceSet mLCUWSendTraceSet = new LCUWSendTraceSet();

    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();

    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();

    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();

    private MMap tMMap = new MMap();

    private LJSPaySet mLJSPaySet;

    /** 数据操作字符串 */
    private String mOperater;

    private String mManageCom;

    private String mActivityID;

    /** 业务数据操作字符串 */
    private String mContNo;

    private String mUWFlag;

    private String mBackUWGrade;

    private String mBackAppGrade;

    private String mOperator;

    private String mUWPopedom; //核保级别

    private String mAppGrade; //申请级别

    private String mMissionID;

    private String mPrtNo;

    /**保单表*/
    private LCContSchema mLCContSchema = new LCContSchema();

    private LCPolSet mLCPolSet = new LCPolSet();

    public ReManuUWAfterInitService()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start  Submit...");

        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();

        tMMap.put(mLCContSchema, "UPDATE");
        tMMap.put(mLCPolSet, "UPDATE");
        tMMap.put(mLCCUWMasterSet, "UPDATE");
        tMMap.put(mLCCUWSubSet, "INSERT");
        tMMap.put(mLCUWSendTraceSet, "INSERT");
        tMMap.put(mLCUWMasterSet, "UPDATE");
        tMMap.put(mLCUWSubSet, "INSERT");
        tMMap.put(this.mLJTempFeeClassSet, "delete".toUpperCase());
        tMMap.put(this.mLJTempFeeSet, "delete".toUpperCase());
        tMMap.put(this.mLOPRTManagerSet, "delete".toUpperCase());
        if (mLJSPaySet != null)
        {
            tMMap.put(this.mLJSPaySet, "delete".toUpperCase());
        }
        mResult.add(tMMap);
        return true;
    }

    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {
        //校验核保员级别
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mOperater);
        System.out.println("mOperate" + mOperater);
        if (!tLDUserDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperater + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tUWPopedom = tLDUserDB.getUWPopedom();
        mUWPopedom = tUWPopedom;
        mAppGrade = mUWPopedom;
        //校验保单信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "checkData";
            tError.errorMessage = "保单" + mContNo + "信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema.setSchema(tLCContDB);
        mPrtNo = mLCContSchema.getPrtNo();
        /** 校验是否可以以回复 */
        //        LWMissionDB tLWMissionDB = new LWMissionDB();
        //        tLWMissionDB.setMissionID(this.mMissionID);
        //        tLWMissionDB.setActivityID("0000001200");
        //        LWMissionSet tLWMissionSet = tLWMissionDB.query();
        //        if (tLWMissionSet != null && tLWMissionSet.size() > 0) {
        //            for (int i = 1; i <= tLWMissionSet.size(); i++) {
        //                if (tLWMissionSet.get(i).getMissionProp7() != null) {
        //                    buildError("checkData", "客户意见已扫描，不能进行核保订正！");
        //                    return false;
        //                }
        //            }
        //        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLWActivitySchema.setSchema((LWActivitySchema) cInputData
                .getObjectByObjectName("LWActivitySchema", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mActivityID = mLWActivitySchema.getActivityID();
        System.out.println(mActivityID);
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if (mOperater == null || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的mCont
        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (mContNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mUWFlag = (String) mTransferData.getValueByName("UWFlag");
        if (mUWFlag == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中UWFlag失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        mUWFlag = "z"; //核保订正标志
        //准备保单的复核标志
        mLCContSchema.setUWFlag(mUWFlag);
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        mLCPolSet = tLCPolDB.query();
        //准备险种保单的复核标志
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            mLCPolSet.get(i).setUWFlag(mUWFlag);
        }

        //准备合同复核表数据
        if (!prepareContUW())
        {
            return false;
        }

        //准备险种复核表数据
        if (!prepareAllUW())
        {
            return false;
        }

        //首期交费通知书问题
        if (!prepareSendInfo())
        {
            return false;
        }

        if (!prepareFinFee())
        {
            return false;
        }
        return true;
    }

    /**
     * 处理财务数据，未到帐确认
     * @return boolean
     */
    private boolean prepareFinFee()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mLCContSchema.getPrtNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() > 0)
        {
            for (int i = 1; i <= tLJSPaySet.size(); i++)
            {
                if (StrTool.cTrim(tLJSPaySet.get(i).getBankOnTheWayFlag())
                        .equals("1"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ReManuUWAfterInitService";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "银行在途，不能核保订正！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        mLJSPaySet = tLJSPaySet;

        // 处理财务暂收
        if (!dealTempFeeDatas(mLCContSchema))
        {
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 处理已发送的首期交费通知书和财务数据回退
     * @return boolean
     */
    private boolean prepareSendInfo()
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        String StrSQL;
        tLOPRTManagerDB.setOtherNo(this.mContNo);
        tLOPRTManagerDB.setCode("07");
        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB
                .executeQuery("select * from loprtmanager where otherno='"
                        + this.mContNo
                        + "' and code in ('07','05','06','05_T')");
        if (tLOPRTManagerDB != null)
        {
            if (tLOPRTManagerSet.size() > 0)
            {
                mLOPRTManagerSet.set(tLOPRTManagerSet);
            }
            if (tLOPRTManagerSet.size() > 0)
            {
                for (int i = 1; i <= tLOPRTManagerSet.size(); i++)
                {
                    tLOPRTManagerSchema = tLOPRTManagerSet.get(i);
                    if (StrTool.cTrim(tLOPRTManagerSchema.getCode()).equals(
                            "05")
                            || StrTool.cTrim(tLOPRTManagerSchema.getCode())
                                    .equals("05_T"))
                    {
                        StrSQL = "delete from LZSysCertify where CertifyNo='"
                                + tLOPRTManagerSchema.getPrtSeq() + "'";
                        tMMap.put(StrSQL, "DETELE");

                        // 如果核保件回销，需同时清除核保件。
                        try
                        {
                            MMap tTmpMap = dealUWResultPrint(tLOPRTManagerSchema
                                    .getPrtSeq());
                            if (tTmpMap != null)
                            {
                                tMMap.add(tTmpMap);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            return false;
                        }
                        // ------------------------------
                    }
                }
            }
        }

        return true;
    }

    /**
     * 准备主附险核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareAllUW()
    {
        mLCUWMasterSet.clear();
        mLCUWSubSet.clear();

        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();

            tLCPolSchema = mLCPolSet.get(i);

            LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCUWMasterDB.setPolNo(tLCPolSchema.getPolNo());
            LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
            tLCUWMasterSet = tLCUWMasterDB.query();
            if (tLCUWMasterDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWMaster表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            int n = tLCUWMasterSet.size();
            System.out.println("该投保单的核保主表当前记录条数:  " + n);
            if (n == 1)
            {
                tLCUWMasterSchema = tLCUWMasterSet.get(1);

                //为核保订正回退保存核保级别和核保人
                mBackUWGrade = tLCUWMasterSchema.getUWGrade();
                mBackAppGrade = tLCUWMasterSchema.getAppGrade();
                mOperator = tLCUWMasterSchema.getOperator();

                //tLCUWMasterSchema.setUWNo(tLCUWMasterSchema.getUWNo()+1);核保主表中的UWNo表示该投保单经过几次人工核保(等价于经过几次自动核保次数),而不是人工核保结论(包括核保通知书,上报等)下过几次.所以将其注释.sxy-2003-09-19
                tLCUWMasterSchema.setPassFlag(mUWFlag); //通过标志
                tLCUWMasterSchema.setState(mUWFlag);
                tLCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
                tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

                //恢复核保级别和核保员
                tLCUWMasterSchema.setUWGrade(mBackUWGrade);
                tLCUWMasterSchema.setAppGrade(mBackAppGrade);
                tLCUWMasterSchema.setOperator(mOperator);
                //解锁
                LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();

                tLDSysTraceSchema.setPolNo(mContNo);
                tLDSysTraceSchema.setCreatePos("人工核保");
                tLDSysTraceSchema.setPolState("1001");
                if (mActivityID.trim().equals("0000009003"))
                {
                    tLDSysTraceSchema.setPolNo(mPrtNo);
                    tLDSysTraceSchema.setCreatePos("承保复核");
                    tLDSysTraceSchema.setPolState("1003");
                }

                LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
                inLDSysTraceSet.add(tLDSysTraceSchema);

                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.add(inLDSysTraceSet);

                LockTableBL LockTableBL1 = new LockTableBL();
                if (!LockTableBL1.submitData(tVData, "DELETE"))
                {
                    System.out.println("解锁失败！");
                }

            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWMaster表取数据不唯一!";
                this.mErrors.addOneError(tError);
                return false;
            }

            mLCUWMasterSet.add(tLCUWMasterSchema);

            // 核保轨迹表
            LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
            LCUWSubDB tLCUWSubDB = new LCUWSubDB();
            tLCUWSubDB.setPolNo(tLCPolSchema.getPolNo());
            LCUWSubSet tLCUWSubSet = new LCUWSubSet();
            tLCUWSubSet = tLCUWSubDB.query();
            if (tLCUWSubDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWSub表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            int m = tLCUWSubSet.size();
            System.out.println("subcount=" + m);
            if (m > 0)
            {
                m++; //核保次数
                tLCUWSubSchema = new LCUWSubSchema();
                tLCUWSubSchema.setUWNo(m); //第几次核保
                tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
                tLCUWSubSchema.setContNo(tLCUWMasterSchema.getContNo());
                tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
                tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema
                        .getProposalContNo());
                tLCUWSubSchema.setPolNo(tLCUWMasterSchema.getPolNo());
                tLCUWSubSchema.setOperator(tLCUWMasterSchema.getOperator());
                tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());

                tLCUWSubSchema.setPassFlag(mUWFlag); //核保意见
                tLCUWSubSchema.setUWGrade(mUWPopedom); //核保级别
                tLCUWSubSchema.setAppGrade(mAppGrade); //申请级别
                tLCUWSubSchema.setAutoUWFlag("2");
                tLCUWSubSchema.setState(mUWFlag);
                tLCUWSubSchema.setOperator(mOperater); //操作员

                tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
                tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
                tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
                tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWSub表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCUWSubSet.add(tLCUWSubSchema);
        }
        return true;
    }

    /**
     * 准备主附险核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareContUW()
    {
        mLCCUWMasterSet.clear();
        mLCCUWSubSet.clear();

        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(mLCContSchema.getContNo());
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet = tLCCUWMasterDB.query();
        if (tLCCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWMaster表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int n = tLCCUWMasterSet.size();
        System.out.println("该投保单的核保主表当前记录条数:  " + n);
        if (n == 1)
        {
            tLCCUWMasterSchema = tLCCUWMasterSet.get(1);

            //为核保订正回退保存核保级别和核保人
            mBackUWGrade = tLCCUWMasterSchema.getUWGrade();
            mBackAppGrade = tLCCUWMasterSchema.getAppGrade();
            mOperator = tLCCUWMasterSchema.getOperator();

            //tLCCUWMasterSchema.setUWNo(tLCCUWMasterSchema.getUWNo()+1);核保主表中的UWNo表示该投保单经过几次人工核保(等价于经过几次自动核保次数),而不是人工核保结论(包括核保通知书,上报等)下过几次.所以将其注释.sxy-2003-09-19
            tLCCUWMasterSchema.setPassFlag(mUWFlag); //通过标志
            tLCCUWMasterSchema.setState(mUWFlag);
            tLCCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

            //恢复核保级别和核保员
            tLCCUWMasterSchema.setUWGrade(mBackUWGrade);
            tLCCUWMasterSchema.setAppGrade(mBackAppGrade);
            tLCCUWMasterSchema.setOperator(mOperator);
            //解锁
            LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
            tLDSysTraceSchema.setPolNo(mContNo);
            tLDSysTraceSchema.setCreatePos("人工核保");
            tLDSysTraceSchema.setPolState("1001");
            if (mActivityID.trim().equals("0000009003"))
            {
                tLDSysTraceSchema.setPolNo(mPrtNo);
                tLDSysTraceSchema.setCreatePos("承保复核");
                tLDSysTraceSchema.setPolState("1003");
            }

            LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
            inLDSysTraceSet.add(tLDSysTraceSchema);

            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.add(inLDSysTraceSet);

            LockTableBL LockTableBL1 = new LockTableBL();
            if (!LockTableBL1.submitData(tVData, "DELETE"))
            {
                System.out.println("解锁失败！");
            }

        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWMaster表取数据不唯一!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCCUWMasterSet.add(tLCCUWMasterSchema);

        // 核保轨迹表
        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
        tLCCUWSubDB.setContNo(mLCContSchema.getContNo());
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet = tLCCUWSubDB.query();
        if (tLCCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWSub表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int m = tLCCUWSubSet.size();
        System.out.println("subcount=" + m);
        if (m > 0)
        {
            m++; //核保次数
            tLCCUWSubSchema = new LCCUWSubSchema();
            tLCCUWSubSchema.setUWNo(m); //第几次核保
            tLCCUWSubSchema.setContNo(tLCCUWMasterSchema.getContNo());
            tLCCUWSubSchema.setGrpContNo(tLCCUWMasterSchema.getGrpContNo());
            tLCCUWSubSchema.setProposalContNo(tLCCUWMasterSchema
                    .getProposalContNo());
            tLCCUWSubSchema.setOperator(mOperater);

            tLCCUWSubSchema.setPassFlag(mUWFlag); //核保意见
            tLCCUWSubSchema.setUWGrade(mUWPopedom); //核保级别
            tLCCUWSubSchema.setAppGrade(mAppGrade); //申请级别
            tLCCUWSubSchema.setAutoUWFlag("2");
            tLCCUWSubSchema.setState(mUWFlag);
            tLCCUWSubSchema.setOperator(mOperater); //操作员

            tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
            tLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWSub表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCCUWSubSet.add(tLCCUWSubSchema);

        // 核保订正轨迹表
        LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
        LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
        tLCUWSendTraceDB.setOtherNo(mLCContSchema.getContNo());
        LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
        tLCUWSendTraceSet = tLCUWSendTraceDB.query();
        if (tLCUWSendTraceDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWSendTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCUWSendTrace表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int a = tLCUWSendTraceSet.size();
        System.out.println("subcount=" + a);
        if (a >= 0)
        {
            a++; //核保次数
            tLCUWSendTraceSchema = new LCUWSendTraceSchema();
            tLCUWSendTraceSchema.setUWNo(a); //第几次核保
            tLCUWSendTraceSchema.setOtherNo(tLCCUWMasterSchema.getContNo());
            tLCUWSendTraceSchema.setOtherNoType("1");
            tLCUWSendTraceSchema.setSendFlag("1");
            tLCUWSendTraceSchema.setUpUserCode(mOperater);
            tLCUWSendTraceSchema.setSendType("7");
            tLCUWSendTraceSchema.setUWFlag("z");
            tLCUWSendTraceSchema
                    .setDownUWCode(tLCCUWMasterSchema.getOperator()); //原核保人
            tLCUWSendTraceSchema.setUWCode(tLCCUWMasterSchema.getOperator()); //原核保人
            tLCUWSendTraceSchema.setOperator(mOperater);//当前操作员

            tLCUWSendTraceSchema
                    .setManageCom(tLCCUWMasterSchema.getManageCom());
            tLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWSendTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCUWSendTrace表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCUWSendTraceSet.add(tLCUWSendTraceSchema);

        return true;
    }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return
     */
    private boolean prepareTransferData()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (tLAAgentSet == null || tLAAgentSet.size() != 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人表LAAgent查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLAAgentSet.get(1).getAgentGroup() == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人表LAAgent中的代理机构数据丢失!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setAgentGroup(tLAAgentSet.get(1).getAgentGroup());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() != 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人展业机构表LABranchGroup查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLABranchGroupSet.get(1).getBranchAttr() == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人展业机构表LABranchGroup中展业机构信息丢失!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mTransferData
                .setNameAndValue("AgentCode", mLCContSchema.getAgentCode());
        System.out.println("agentcode==" + mLCContSchema.getAgentCode());
        mTransferData.setNameAndValue("ManageCom", mManageCom);
        System.out.println("manageCom=" + mManageCom);
        mTransferData.setNameAndValue("PrtNo", mLCContSchema.getPrtNo());
        System.out.println("prtNo==" + mLCContSchema.getPrtNo());
        mTransferData.setNameAndValue("ContNo", mLCContSchema.getContNo());
        System.out.println("ContNo==" + mLCContSchema.getContNo());
        mTransferData.setNameAndValue("AgentGroup", tLAAgentSet.get(1)
                .getAgentGroup());
        mTransferData
                .setNameAndValue("AgentName", tLAAgentSet.get(1).getName());
        mTransferData.setNameAndValue("AppntCode", mLCContSchema.getAppntNo());
        System.out.println("AppntName = " + mLCContSchema.getAppntName());
        mTransferData
                .setNameAndValue("AppntName", mLCContSchema.getAppntName());
        return true;
    }

    /**
     * 处理核保通知书。
     * <br />如果已经核保回复过的保单进行核保订正，则需清除回销的核保件。
     * @param tSUWResultPrtSeq  核保通知书号
     * @return
     * @throws Exception 
     */
    private MMap dealUWResultPrint(String tSUWResultPrtSeq) throws Exception
    {
        MMap tTmpMap = null;

        String tStrSql = "select DocId from Es_Doc_Main edm "
                + " where SubType = 'TB21' " + " and edm.DocCode = '"
                + tSUWResultPrtSeq + "' ";
        SSRS tSsDocId = new ExeSQL().execSQL(tStrSql);
        if (tSsDocId.getMaxRow() >= 2)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ReManuUWAfterInitService";
            tError.functionName = "dealUWResultPrint";
            tError.errorMessage = "已回复的核保件查询有误。核保通知书号：" + tSUWResultPrtSeq;
            this.mErrors.addOneError(tError);
            throw new Exception(tError.errorMessage);
        }

        if (tSsDocId.getMaxRow() == 1)
        {
            String tDocId = tSsDocId.GetText(1, 1);
            tTmpMap = new MMap();

            tTmpMap.put(
                    "insert into Es_Doc_MainB (select * from Es_Doc_Main edm where edm.DocId = "
                            + tDocId + ")", SysConst.INSERT);
            tTmpMap.put("delete from Es_Doc_Main edm where edm.DocId = "
                    + tDocId, SysConst.DELETE);

            tTmpMap.put(
                    "insert into Es_Doc_PagesB (select * from Es_Doc_Pages edp where edp.DocId = "
                            + tDocId + ")", SysConst.INSERT);
            tTmpMap.put("delete from Es_Doc_Pages edp where edp.DocId = "
                    + tDocId, SysConst.DELETE);

            tTmpMap
                    .put(
                            "insert into Es_Doc_RelationB (select * from Es_Doc_Relation edr where edr.DocId = "
                                    + tDocId + ")", SysConst.INSERT);
            tTmpMap.put("delete from Es_Doc_Relation edr where edr.DocId = "
                    + tDocId, SysConst.DELETE);
        }

        return tTmpMap;
    }

    /**
     * 处理暂收财务数据。
     * @param cContInfo
     * @return
     */
    private boolean dealTempFeeDatas(LCContSchema cContInfo)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tResult = null;
        String tStrSql = null;

        // 如果存在银行在途，发盘数据未锁定，或财务录入但未作财务确认的数据时，不允许核保订正。
        tResult = null;
        tStrSql = null;
        tStrSql = " select 1 from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is not null "
                + " and ljtf.ConfMakeDate is null and ljtf.OtherNoType = '4' "
                + " and ljtf.OtherNo = '"
                + cContInfo.getPrtNo()
                + "' union all "
                + " select 1 from LJSPay ljsp where 1 = 1 and (ljsp.BankOnTheWayFlag = '1' or ljsp.CanSendBank = '0') "
                + " and ljsp.OtherNo = '" + cContInfo.getPrtNo() + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("dealTempFeeDatas",
                    "存在银行在途、发盘数据未锁定，或财务录入但未作财务确认的数据，不允许核保订正。");
            return false;
        }
        tResult = null;
        tStrSql = null;
        // --------------------

        // 处理非到帐暂收
        tStrSql = null;

        String tPrtNo = cContInfo.getPrtNo();
        tStrSql = " select * from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        LJTempFeeSet tTempFeeSet = new LJTempFeeDB().executeQuery(tStrSql);
        mLJTempFeeSet.set(tTempFeeSet);

        tStrSql = null;
        tStrSql = " select * from LJTempFeeClass ljtfc "
                + " where ljtfc.TempFeeNo in ("
                + " select ljtf.TempFeeNo from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "') ";
        LJTempFeeClassSet tTempFeeClassSet = new LJTempFeeClassDB()
                .executeQuery(tStrSql);
        mLJTempFeeClassSet.set(tTempFeeClassSet);
        tStrSql = null;
        // --------------------

        return true;
    }

    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ReManuUWAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
