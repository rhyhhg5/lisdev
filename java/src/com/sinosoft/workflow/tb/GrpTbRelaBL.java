package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.pubfun.*;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.workflowengine.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */
public class GrpTbRelaBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /**工作流引擎 */
    ActivityOperator mActivityOperator = new ActivityOperator();

    /** 数据操作字符串 */
    private String mOperater;

    private String mManageCom;

    private String mOperate;
    
    private String mszOperate = "";
   
    private LDGrpSet mLDGrpSet = new LDGrpSet();

    public GrpTbRelaBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---UI-- submitData--begin-");

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 数据操作业务处理
        if (!dealData())
        {
            return false;
        }

       
        

        System.out.println("---GrpTbWorkFlowBLS commitData End ---");

        return true;
    }
    
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        
//        mTransferData = (TransferData) cInputData.getObjectByObjectName(
//                "TransferData", 0);        //-----????
//        if (mTransferData == null)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "GrpTbRelaBL";
//            tError.functionName = "getInputData";
//            tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
//            this.mErrors.addOneError(tError);
//
//            return false;
//        }
//        
//        if(mTransferData.getValueByName("CustomerNo") == null)
//        {
//            //@@错误处理
//            CError tError = new CError();
//            tError.moduleName = "GrpTbRelaBL";
//            tError.functionName = "getInputData";
//            tError.errorMessage = "获取客户号失败!";
//            this.mErrors.addOneError(tError);
//
//            return false;
//        }
//        
        mLDGrpSet.set((LDGrpSet) cInputData.getObjectByObjectName("LDGrpSet",0));
        System.out.println("mLDGrpSet.size()----->>>"+mLDGrpSet.size());
        if(mLDGrpSet == null || mLDGrpSet.size()<=0){
            
            buildError("getInputData","获取数据失败！");
            return false;
        }

       
        return true;
       
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    private boolean dealData()
    {
        boolean flag = true;
        for(int i=1;i<=mLDGrpSet.size();i++)
        {
            MMap tMMap = new MMap();
            LDGrpSchema tLDGrpSchema = mLDGrpSet.get(i);

            String tSQL = "select * From LDGrp where CustomerNo = '"+tLDGrpSchema.getCustomerNo()+"' ";
            LDGrpDB tLDGrpDB = new LDGrpDB();
            LDGrpSet tempLDGrpSet = tLDGrpDB.executeQuery(tSQL);
            if(tempLDGrpSet == null || tempLDGrpSet.size()!=1){
                buildError("dealData","客户编号["+tLDGrpSchema.getCustomerNo()+"]不可执行修改操作！");
                flag = false;
                continue;
            }
            LDGrpSchema tempLDGrpSchema = tempLDGrpSet.get(1);
            tempLDGrpSchema.setRelationFlag(tLDGrpSchema.getRelationFlag());
            tempLDGrpSchema.setRelaName(tLDGrpSchema.getRelaName());
            tMMap.put(tempLDGrpSchema, "UPDATE");
            
       
           
            if(!submit(tMMap))
            {
                return false;
            }
            tMMap = null;
            
                
            }
            //LDGrpSchema tempLDGrpSchema = mLDGrpSet.get(1);
            //tMMap.put(tempLDGrpSchema, SysConst.UPDATE);
           
        
            return flag; 
        }
    
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
    }

   