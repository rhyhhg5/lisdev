/*
 * @(#)ParseGuideIn.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.workflow.tb;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 合同签单处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author dongjf
 * @version 6.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.workflowengine.BeforeInitService;
import com.sinosoft.lis.bq.CommonBL;

public class ProposalOmnipotenceBeforeInitService implements BeforeInitService {
	/** 存放结果 */
	private VData mVResult = new VData();

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private VData inPutData = new VData();

	private VData mInputData;

	private String mOperate;

	private String mMissionID;

	private MMap tmpMap = new MMap();

	// private VData mIputData = new VData();
	private TransferData mTransferData = new TransferData();

	private LCContSchema mLCContSchema = new LCContSchema();

	private LCPolSet mLCPolSet = new LCPolSet();

	private LWActivitySchema mLWActivitySchema = new LWActivitySchema();

	private String mActivityID = "";

	private String mCvaliDate = "";

	private String mContNo = "";

	private String mExpectedDate = "";

	/** 往后面传输数据的容器 */

	/** 全局数据 */

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println(">>>>>>submitData");
		// 将操作数据拷贝到本类中
		// 得到外部传入的数据,将数据备份到本类中
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;
		if (this.getInputData(cInputData) == false) {
			return false;
		}// getBasicData
		if (this.getBasicData() == false) {
			return false;
		}
		if (this.preSubmitData() == false) {
			return false;
		}
		return true;
	}

	public ProposalOmnipotenceBeforeInitService() {

	}

	public VData getResult() {
		return this.mVResult;
	}

	public TransferData getReturnTransferData() {
		return this.mTransferData;
	}

	public CErrors getErrors() {
		return this.mErrors;
	}

	private boolean getInputData(VData cInputData) {
		mLWActivitySchema.setSchema((LWActivitySchema) cInputData
				.getObjectByObjectName("LWActivitySchema", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mMissionID = (String) mTransferData.getValueByName("MissionID");
		System.out.println("mMissionID=" + mMissionID);
		if (mLWActivitySchema == null) {
			mErrors.addOneError("传入的数据不完整.");
			return false;
		}
		if (mMissionID == null) {
			mErrors.addOneError("传入的数据不完整.");
			return false;
		}

		return true;
	}

	private boolean preSubmitData() {

		mResult.clear();
		mResult.add(tmpMap);

		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, null)) {
			CError.buildErr(this, "数据库保存失败");
			return false;
		}
		return true;
	}

	// 取得处理所需的基本数据
	private boolean getBasicData() {
		mActivityID = mLWActivitySchema.getActivityID();
		LWMissionSet tLWMissionSet = new LWMissionSet();
		LWMissionDB tLWMissionDB = new LWMissionDB();
		tLWMissionDB.setMissionID(mMissionID);
		tLWMissionDB.setActivityID(mActivityID);
		tLWMissionSet = tLWMissionDB.query();
		if (tLWMissionSet.size() != 1) {
			mErrors.addOneError("查询责任信息LWMissionSet失败.");
			return false;
		}

		mContNo = tLWMissionSet.get(1).getMissionProp1();

		boolean tIsFHXRisk = hasFHXRisk(mContNo);

		// 如果不含万能险，直接退出。
		if (!CommonBL.hasULIRisk(mContNo) && !tIsFHXRisk) {
			return true;
		}
		mLCContSchema = getLCContSchema(mContNo);
		if (!tIsFHXRisk) {
			// 获取万能类险种的保单生效日期。
			mCvaliDate = getULIValidate(mLCContSchema);
			if (mCvaliDate == null || "".equals(mCvaliDate)) {
				mErrors.addOneError("万能类险种获取保单生效日期失败。");
				return false;
			}
			// ----------------------------------------
		} else {
			String tTmpContNo = mLCContSchema.getContNo();
			String tStrSql = "update LCPol set SpecifyValidate = 'N' where ContNo = '"
					+ tTmpContNo + "'";
			tmpMap.put(tStrSql, SysConst.UPDATE);
			return true;
		}

		// update LCCont: cvalidate
		// update LCCont: cinvalidate
		mLCContSchema.setCValiDate(mCvaliDate);
		tmpMap.put(mLCContSchema, "UPDATE");
		mLCPolSet = getLCPolSet(mContNo);

		for (int i = 1; i <= mLCPolSet.size(); i++) {

			// 得到基础的lcpol数据
			LCPolSchema tBasicLCPolSchema = new LCPolSchema();
			tBasicLCPolSchema = mLCPolSet.get(i);
			String tPolNo = tBasicLCPolSchema.getPolNo();
			tBasicLCPolSchema.setCValiDate(mCvaliDate);
			tBasicLCPolSchema.setEndDate("");
			tBasicLCPolSchema.setSignDate("");
			tBasicLCPolSchema.setSignTime("");

			// 得到基础的lcduty的数据
			LCDutySet tBasicLCDutySet = new LCDutySet();
			LCDutyDB tLCDutyDB = new LCDutyDB();
			tLCDutyDB.setPolNo(tPolNo);
			tBasicLCDutySet = tLCDutyDB.query();
			if (tBasicLCDutySet.size() < 1) {
				mErrors.addOneError("查询责任信息lcduty失败.");
				return false;
			}
			for (int j = 1; j <= tBasicLCDutySet.size(); j++) {
				tBasicLCDutySet.get(j).setEndDate("");
				tBasicLCDutySet.get(j).setFirstPayDate("");
				tBasicLCDutySet.get(j).setPaytoDate("");
			}

			// 得到基础的lcprem的数据
			LCPremSet tBasicLCPremSet = new LCPremSet();
			LCPremDB tLCPremDB = new LCPremDB();
			tLCPremDB.setPolNo(tPolNo);
			tBasicLCPremSet = tLCPremDB.query();
			if (tBasicLCPremSet.size() < 1) {
				mErrors.addOneError("查询责任信息lcduty失败.");
				return false;
			}
			// 得到基础的lcget的数据
			LCGetSet tBasicLCGetSet = new LCGetSet();
			LCGetDB tLCGetDB = new LCGetDB();
			tLCGetDB.setPolNo(tPolNo);
			tBasicLCGetSet = tLCGetDB.query();
			if (tBasicLCGetSet.size() < 1) {
				mErrors.addOneError("查询责任信息lcduty失败.");
				return false;
			}

			CalBL tCalBL;
			tCalBL = new CalBL(tBasicLCPolSchema, tBasicLCDutySet, null, null);
			if (!tCalBL.calPol()) {
				this.mErrors.copyAllErrors(tCalBL.mErrors);
				return false;
			}
			if (tCalBL.mErrors.needDealError()) {
				this.mErrors.copyAllErrors(tCalBL.mErrors);
				return false;
			}

			// 从这个区域的数据中取出所需的字段。
			LCPolSchema tChangeLCPolSchema = new LCPolSchema();
			tChangeLCPolSchema.setSchema(tCalBL.getLCPol());
			// update lcpol: InsuredAppAge, cvalidate,getstartdate,lastrevdate
			// update lcpol: payenddate,paytodate,enddate
			System.out.println(tChangeLCPolSchema.getInsuredAppAge());
			// System.out.println(tChangeLCPolSchema);
			System.out.println(tChangeLCPolSchema.getCValiDate());
			System.out.println(tChangeLCPolSchema.getGetStartDate());
			System.out.println(tChangeLCPolSchema.getLastRevDate());
			System.out.println(tChangeLCPolSchema.getPayEndDate());
			System.out.println(tChangeLCPolSchema.getPaytoDate());
			System.out.println(tChangeLCPolSchema.getEndDate());
			tBasicLCPolSchema.setCValiDate(tChangeLCPolSchema.getCValiDate());
			tBasicLCPolSchema.setGetStartDate(tChangeLCPolSchema
					.getGetStartDate());
			tBasicLCPolSchema.setLastRevDate(tChangeLCPolSchema
					.getLastRevDate());
			tBasicLCPolSchema.setPayEndDate(tChangeLCPolSchema.getPayEndDate());
			tBasicLCPolSchema.setPaytoDate(tChangeLCPolSchema.getPaytoDate());
			tBasicLCPolSchema.setEndDate(tChangeLCPolSchema.getEndDate());

			// 将万能险设置标志，不进行签单时的生效日期回溯
			tBasicLCPolSchema.setSpecifyValiDate("Y");
			// --------------------

			LCDutySet tChangeLCDutySet = new LCDutySet();
			// update LCDuty: getstartdate
			// update LCDuty: payenddate,paytodate,enddate
			tChangeLCDutySet = tCalBL.getLCDuty();
			for (int k = 1; k <= tBasicLCDutySet.size(); k++) {
				for (int m = 1; m <= tChangeLCDutySet.size(); m++) {
					LCDutySchema tmpLCDutySchema = tChangeLCDutySet.get(m);
					if (tBasicLCDutySet.get(k).getPolNo()
							.equals(tmpLCDutySchema.getPolNo())
							&& tBasicLCDutySet.get(k).getDutyCode()
									.equals(tmpLCDutySchema.getDutyCode())) {
						tBasicLCDutySet.get(k).setGetStartDate(
								tmpLCDutySchema.getGetStartDate());
						tBasicLCDutySet.get(k).setPayEndDate(
								tmpLCDutySchema.getPayEndDate());
						tBasicLCDutySet.get(k).setPaytoDate(
								tmpLCDutySchema.getPaytoDate());
						tBasicLCDutySet.get(k).setEndDate(
								tmpLCDutySchema.getEndDate());
					}
				}
			}

			LCPremSet tChangeLCPremSet = tCalBL.getLCPrem();
			// update LCPrem: paystartdate
			// update LCPrem: payenddate,paytodate
			for (int k = 1; k <= tBasicLCPremSet.size(); k++) {
				for (int m = 1; m <= tChangeLCPremSet.size(); m++) {
					LCPremSchema tmpLCPremSchema = tChangeLCPremSet.get(m);
					if (tBasicLCPremSet.get(k).getPolNo()
							.equals(tmpLCPremSchema.getPolNo())
							&& tBasicLCPremSet.get(k).getDutyCode()
									.equals(tmpLCPremSchema.getDutyCode())
							&& tBasicLCPremSet.get(k).getPayPlanCode()
									.equals(tmpLCPremSchema.getPayPlanCode())) {
						tBasicLCPremSet.get(k).setPayStartDate(
								tmpLCPremSchema.getPayStartDate());
						tBasicLCPremSet.get(k).setPaytoDate(
								tmpLCPremSchema.getPaytoDate());
						tBasicLCPremSet.get(k).setPayEndDate(
								tmpLCPremSchema.getPayEndDate());
					}
				}
			}

			LCGetSet tChangeLCGetSet = tCalBL.getLCGet();
			// update LCGet: gettodate,getstartdate
			// update LCGet: getenddate
			for (int k = 1; k <= tBasicLCGetSet.size(); k++) {
				for (int m = 1; m <= tChangeLCGetSet.size(); m++) {
					LCGetSchema tmpLCGetSchema = tChangeLCGetSet.get(m);
					if (tBasicLCGetSet.get(k).getPolNo()
							.equals(tmpLCGetSchema.getPolNo())
							&& tBasicLCGetSet.get(k).getDutyCode()
									.equals(tmpLCGetSchema.getDutyCode())
							&& tBasicLCGetSet.get(k).getGetDutyCode()
									.equals(tmpLCGetSchema.getGetDutyCode())) {
						tBasicLCGetSet.get(k).setGettoDate(
								tmpLCGetSchema.getGettoDate());
						tBasicLCGetSet.get(k).setGetStartDate(
								tmpLCGetSchema.getGetStartDate());
						tBasicLCGetSet.get(k).setGetEndDate(
								tmpLCGetSchema.getGetEndDate());
					}
				}
			}

			tmpMap.put(tBasicLCPolSchema, "UPDATE");
			tmpMap.put(tBasicLCDutySet, "UPDATE");
			tmpMap.put(tBasicLCPremSet, "UPDATE");
			tmpMap.put(tBasicLCGetSet, "UPDATE");

		}
		return true;
	}

	private LCContSchema getLCContSchema(String ContNo) {
		LCContSchema tLCContSchema = new LCContSchema();
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(ContNo);
		if (tLCContDB.getInfo()) {
			tLCContSchema.setSchema(tLCContDB.getSchema());
		}
		return tLCContSchema;
	}

	private LCPolSet getLCPolSet(String ContNo) {
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		tLCPolSet = tLCPolDB.query();
		return tLCPolSet;
	}

	/**
	 * 获取万能类险种的保单生效日期，该日期为缴费到帐日期的次日。
	 * 
	 * @param cPtrNo
	 * @return
	 */
	private String getULIValidate(LCContSchema cLCContSchema) {
		String tULIValidate = null;
		boolean tSYTransRisk = hasSYTransRisk(mContNo);
		if (tSYTransRisk) {
			tULIValidate = mExpectedDate;
		} else if ("8".equals(cLCContSchema.getCardFlag())) {
			// 银保万能，获取缴费到帐日期。如果该日期不同，取最近一次日期。
			// String tStrSql = " select max(EnterAccDate) From LJTempFee  "
			// + " where OtherNoType = '4' and ConfFlag = '0' " +
			// " and OtherNo = '" + cLCContSchema.getPrtNo()
			// + "' ";
			// tULIValidate = new ExeSQL().getOneValue(tStrSql);
			// ----------------------------------------------
			// 银保万能，#1546 调整为签单日次日。
			tULIValidate = PubFun.getCurrentDate();
		} else if ("0".equals(cLCContSchema.getCardFlag())) {
			// 个险万能，取签单日期
			tULIValidate = PubFun.getCurrentDate();
		} else if ("b".equals(cLCContSchema.getCardFlag())) {
			String sql = "select "
					+ "1"
					+ " from lcpol lp where lp.prtno='"
					+ cLCContSchema.getPrtNo()
					+ "' and "
					+ " exists (select 1 from lmriskapp where taxoptimal='Y' and riskcode=lp.riskcode) ";
			ExeSQL e1=new ExeSQL();
			SSRS s1=e1.execSQL(sql);
			if(s1.getMaxRow()>0){
				tULIValidate = PubFun.getCurrentDate();
			}		
		}

		// 保单生效日期为缴费到帐日期的次日。
		if (tULIValidate != null && !"".equals(tULIValidate)) {
			String DelayDays = "1";
			tULIValidate = PubFun.calDate(tULIValidate,
					Integer.parseInt(DelayDays), "D", null);
		}
		// ----------------------------------------------

		return tULIValidate;
	}

	/**
	 * 判断保单下是否含有万能险种
	 * 
	 * @return boolean
	 */
	private boolean hasFHXRisk(String ContNo) {
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(ContNo);
		LCPolSet tLCPolSet = tLCPolDB.query();
		for (int i = 1; i <= tLCPolSet.size(); i++) {
			LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
			tLMRiskAppDB.setRiskCode(tLCPolSet.get(i).getRiskCode());
			if (!tLMRiskAppDB.getInfo()) {
				return false;
			}
			String riskType4 = tLMRiskAppDB.getRiskType4();
			if ((riskType4 != null) && (riskType4.equals("2"))) {
				return true;
			}
		}
		return false;
	}

	private boolean hasSYTransRisk(String ContNo) {
		String sql = " select lst.ExpectedDate from lcpol lcp,lstransinfo lst where lcp.contno='"
				+ ContNo
				+ "' "
				+ " and exists (select 1 from lmriskapp where riskcode=lcp.riskcode and TaxOptimal ='Y')"
				+ " and exists (select 1 from lccontsub where prtno=lcp.prtno and TransFlag='1' )"
				+ " and lcp.prtno=lst.prtno and lst.SuccFlag='01' and lst.ExpectedDate is not null and lst.ExpectedDate>'"
				+ PubFun.getCurrentDate() + "' ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			mExpectedDate = tSSRS.GetText(1, 1);
			return true;
		}
		return false;
	}

	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "ProposalOmnipotenceBeforeInitService";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.println("程序报错：" + cError.errorMessage);
	}

}
