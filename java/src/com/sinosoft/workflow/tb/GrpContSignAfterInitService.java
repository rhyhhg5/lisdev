/*
 * @(#)ParseGuideIn.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.workflow.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.schema.LCGrpContSchema;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 团单合同签单处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author wujs
 * @version 6.0
 */

public class GrpContSignAfterInitService implements AfterInitService {
    public GrpContSignAfterInitService() {
    }

    /**存放结果*/
    private VData mVResult = new VData();


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    private TransferData mTransferData = new TransferData();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 工作流信息 */
    private String mMissionID;
    private String mSubMissionID;
    private String mGrpContNo;
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println(">>>>>>submitData");
        //将操作数据拷贝到本类中
        //  mInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        LCGrpContSignBL tLCGrpContSignBL = new LCGrpContSignBL();

        boolean tSignResult = false;
        try {
            tSignResult = tLCGrpContSignBL.submitData(mInputData, null);
        } catch (Exception ex) {
            buildError("submitData", "签单异常失败" + ex.getMessage());
            StringBuffer sql = new StringBuffer();
            sql.append(
                    "UPDATE LWMISSION Set ActivityStatus='1' where MissionID ='");
            sql.append(this.mMissionID);
            sql.append("'and ActivityID = '0000002006'");
            (new ExeSQL()).execUpdateSQL(sql.toString());
            return false;
        }

        if (!tSignResult) {
            if (!tLCGrpContSignBL.mErrors.needDealError()) {
                CError.buildErr(this, "部分签单完成");
            } else {
                this.mErrors.copyAllErrors(tLCGrpContSignBL.mErrors);
            }
            mTransferData.setNameAndValue("FinishFlag", "0");
            // return false;
            StringBuffer sql = new StringBuffer();
            sql.append(
                    "UPDATE LWMISSION Set ActivityStatus='1' where MissionID ='");
            sql.append(this.mMissionID);
            sql.append("'and ActivityID = '0000002006'");
            (new ExeSQL()).execUpdateSQL(sql.toString());
        } else {
            mTransferData.setNameAndValue("FinishFlag", "1");
        }
        this.mVResult.add(mTransferData);
        String tProposalGrpContNo = (String) mTransferData.getValueByName(
                "GrpContNo");
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = null;
        if (tProposalGrpContNo != null && !tProposalGrpContNo.equals("")) {
            tLCGrpContDB.setProposalGrpContNo(tProposalGrpContNo);
            tLCGrpContSet = tLCGrpContDB.query();
            if (tLCGrpContSet == null || tLCGrpContSet.size() <= 0 ||
                tLCGrpContSet.size() > 1) {
                buildError("submitData", "签单后查询保单信息失败！");
                return false;
            }
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
            for (int i = 1; i <= tLCGrpContSchema.getFieldCount(); i++) {
                mTransferData.removeByName(tLCGrpContSchema.getFieldName(i - 1));
                mTransferData.setNameAndValue(tLCGrpContSchema.getFieldName(i -
                        1),
                                              tLCGrpContSchema.getV(i - 1));
            }
        }

        return tSignResult;
        //return true;
    }

    public VData getResult() {
        return this.mVResult;
    }

    public TransferData getReturnTransferData() {
        return this.mTransferData;
    }

    public CErrors getErrors() {
        return this.mErrors;
    }

    private boolean getInputData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null) {
            return false;
        }
        this.mMissionID = (String) mTransferData.getValueByName("MissionID");
        this.mSubMissionID = (String) mTransferData.getValueByName(
                "SubMissionID");
        this.mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (this.mMissionID == null) {
            buildError("getInputData", "MissionID为null！");
            return false;
        }
        if (this.mSubMissionID == null) {
            buildError("getInputData", "SubMissionID为null！");
            return false;
        }
        if (this.mGrpContNo == null) {
            buildError("getInputData", "GrpContNo为null！");
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpContSignAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
