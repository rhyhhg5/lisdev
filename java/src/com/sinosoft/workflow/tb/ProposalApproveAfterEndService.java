package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.AfterEndService;
import com.sinosoft.lis.finfee.TempFeeUI;
import com.sinosoft.lis.vbl.LCPolBLSet;
import com.sinosoft.lis.cbcheck.UWSendPrintUI;
import com.sinosoft.brms.databus.client.RuleService;
import com.sinosoft.brms.databus.client.RuleServiceImplServiceLocator;
import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.magnum.request.PadMagNumReadJson;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.jdom.Document;
//import com.sinosoft.lis.reinsure.CalRiskAmntBL;

/**
 * <p>Title:工作流节点任务:新契约自动核保 </p>
 * <p>Description: 自动核保工作流后台AfterInit服务类 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: SinoSoft </p>
 * @author HYQ
 * @version 1.0
 */

public class ProposalApproveAfterEndService implements AfterEndService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    MMap mMap = new MMap();

    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperator;

    private String mManageCom;

    /** 业务数据操作字符串 */
    private String mMissionID;

    /**保单表*/
    private LCContSchema mLCContSchema = new LCContSchema();

    private String mPolPassFlag = "0"; //险种通过标记

    private String mContPassFlag = "0"; //合同通过标记

    private String mUWGrade = "";

    private String mCalCode; //计算编码

    private double mValue;

    private int AppntFlag = 0; //投保人信息只校验一遍的标记

    private LCContSet mAllLCContSet = new LCContSet();

    private LCPolSet mAllLCPolSet = new LCPolSet();

    private String mContNo = "";

    private String mPContNo = "";

    private String mPrtNo = "";

    private String mOldPolNo = "";

    private int m = 0;

    /** 合同核保主表*/
    private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();

    private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();

    /** 合同核保子表*/
    private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();

    private LCCUWSubSet mAllLCCUWSubSet = new LCCUWSubSet();

    /** 合同核保错误信息表*/
    private LCCUWErrorSet mLCCUWErrorSet = new LCCUWErrorSet();

    private LCCUWErrorSet mAllLCCUWErrorSet = new LCCUWErrorSet();

    /** 各险种核保主表 */
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();

    private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();

    /** 各险种核保子表 */
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();

    private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();

    /** 核保错误信息表 */
    private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

    private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();

    private LCPolSet mLCPolSet = new LCPolSet();

    private CalBase mCalBase = new CalBase();
    
    private String mMagNumJson = "";
    
    String mianDecison ="";
    String caseUuid ="";
    String mianDecisonpassflag="1";

    public ProposalApproveAfterEndService()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        System.out.println("---UWAutoChkBL getInputData---");

        if (!checkData())
        {
            return false;
        }

        if (!dealData(mLCContSchema))
        {
            return false;
        }
        System.out.println("---UWAutoChkBL dealData END---");

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
        {
            return false;
        }

        //准备给后台的数据
        if (prepareOutputData(mLCContSchema))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交的数据准备失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("Start  Submit...");

        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    /**
     * @param tLCContSchema
     * @return
     */
    private boolean dealData(LCContSchema tLCContSchema)
    {
    	ExeSQL tExeSQL=new ExeSQL();
        String tSQL="select code from ldcode where codetype='QYBZRuleButton' with ur";
    	String tRuleButton=tExeSQL.getOneValue(tSQL);
    	tSQL="select comcode from ldcode where codetype='CardFlagRuleButton' and code='" +
    	tLCContSchema.getCardFlag()+"' with ur";
    	String tCardFlagButton=tExeSQL.getOneValue(tSQL);
    	//上海医保卡险种不调规则引擎
    	System.out.println("修正之后的印刷号："+ tLCContSchema.getPrtNo());
    	String SQLRisk= "select 1 from lcpol where prtno ='"+tLCContSchema.getPrtNo()+"' and riskcode in (select code from ldcode where codetype='ybkriskcode')";
    	SSRS ttSSRS = new ExeSQL().execSQL(SQLRisk);
		if(ttSSRS.getMaxRow()>0){
			tRuleButton="01";
    		tCardFlagButton="12";
		}
		
		//沈阳医保卡险种不调规则引擎
//		String SQLSy = "select 1 from lcpol where prtno like 'SYW%' and operator='YBK'";
//		SSRS sySSRS = new ExeSQL().execSQL(SQLSy);
//		if(sySSRS.getMaxRow()>0){
//			return true;
//		}
    	System.out.println("投保规则开关1啊：" + tRuleButton);
    	System.out.println("投保规则开关2啊：" + tCardFlagButton);
    	if(tRuleButton!=null && tRuleButton.equals("00") 
        		&& (tCardFlagButton==null||!tCardFlagButton.equals("11"))){
    		try{
    		//调用333501和333502险种累计保额校验
    			FJCheckEngineBL tFJCheckEngineBL=new FJCheckEngineBL();
    			VData fjVData = new VData();
    			fjVData.add(tLCContSchema);
    			try{
    				if(tFJCheckEngineBL.submitData(fjVData, "check")==false){
    					CError tError = new CError();
    	                tError.moduleName = "FJCheckEngineBL";
    	                tError.functionName = "checkData";
    	                tError.errorMessage =tFJCheckEngineBL.mErrors.getFirstError();
    	                this.mErrors.addOneError(tError);
    	                return false;
    				}
    			}catch(Exception ex){
    	    		CError tError = new CError();
    	            tError.moduleName = "FJCheckEngineBL";
    	            tError.functionName = "checkData";
    	            tError.errorMessage = "校验333501和333502险种出错";
    	            this.mErrors.addOneError(tError);
    	            return false;
    	    	}

        	//调用规则引擎	
    		RuleTransfer ruleTransfer = new RuleTransfer();
    		//获得规则引擎返回的校验信息 调用投保规则
    		System.out.println("开始调用规则引擎，时间"+ PubFun.getCurrentDate() + " "+ PubFun.getCurrentTime());
    		long startTime = System.currentTimeMillis();
			String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NP", mContNo, false);
			//将校验信息转换为Xml的document对象
			Document doc = ruleTransfer.stringToDoc(xmlStr);
			String approved = ruleTransfer.getApproved(doc);
			long endTime = System.currentTimeMillis();
			System.out.println("调用规则引擎结束，时间"+ PubFun.getCurrentDate() + " "+ PubFun.getCurrentTime() +"用时："+((endTime -startTime)/1000/60)+"分钟");
			if(approved==null){
				CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "执行规则引擎投保规则时出错";
                this.mErrors.addOneError(tError);
                return false;
			}
			if("1".equals(approved)){
    		}else if("0".equals(approved) || "2".equals(approved)){
    		}else if("-1".equals(approved)){
    			CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "规则引擎投保规则执行异常";
                this.mErrors.addOneError(tError);
                return false;
    		}else {
    			CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "规则引擎投保规则返回了未知的错误类型";
                this.mErrors.addOneError(tError);
                return false;
   		    }
			// modify by zxs 调用核保新规则引擎
			NewEngineRuleService newEngineRule = new NewEngineRuleService();
			MMap map = newEngineRule.dealNPData(tLCContSchema,"NewQYBZRuleButton");
			VData mData = new VData();
			
			LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的投保规则
			LMUWSchema tempLMUWSchema=new LMUWSchema();
			String errorMessage="";
//			String errorMessage="<br>";
			String tempRiskcode="";
			String tempInsured="";
			if(tLMUWSetPolAllUnpass.size()>0){
				for(int m=1;m<=tLMUWSetPolAllUnpass.size();m++){
					// modify by zxs
					LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
					EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
					engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
					engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
					engineRuleSchema.setApproved(approved);
					engineRuleSchema.setRuleResource("old");
					engineRuleSchema.setRuleType("NP");
					engineRuleSchema.setContType("标准保单");
					engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
					engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
					engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
					if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
						engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
					}else{
						engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
					}
					engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
					map.put(engineRuleSchema, "INSERT");
					// modify by zxs
					tempRiskcode="";
					tempInsured="";
					if(tLMUWSetPolAllUnpass.get(m).getRiskName()!=null 
							&& !tLMUWSetPolAllUnpass.get(m).getRiskName().equals("")){
						tempInsured+="被保人"+tLMUWSetPolAllUnpass.get(m).getRiskName();
						if(tLMUWSetPolAllUnpass.get(m).getRiskCode()!=null
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("")){
							tempRiskcode+="，险种代码"+tLMUWSetPolAllUnpass.get(m).getRiskCode()+"，";
						}else{
							tempInsured+="，";
						}
					}else{
						if(tLMUWSetPolAllUnpass.get(m).getRiskCode()!=null
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("")){
							tempRiskcode+="险种代码"+tLMUWSetPolAllUnpass.get(m).getRiskCode()+"，";
						}
					}
					errorMessage+=m+":";
					errorMessage+=tLMUWSetPolAllUnpass.get(m).getUWCode();
					errorMessage+=tempInsured;
					errorMessage+=tempRiskcode;
					errorMessage+=tLMUWSetPolAllUnpass.get(m).getRemark();
					errorMessage+="。";
				}
				
				mData.add(map);
				PubSubmit pubSubmit = new PubSubmit();
				if (!pubSubmit.submitData(mData, "")) {
					this.mErrors.addOneError(pubSubmit.mErrors.getContent());
					return false;
				}
				// modify by zxs
				
				// @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = errorMessage;
                this.mErrors.addOneError(tError);
				return false;
			}else{
				//modify by zxs
				EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
				engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
				engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
				engineRuleSchema.setApproved(approved);
				engineRuleSchema.setRuleResource("old");
				engineRuleSchema.setRuleType("NP");
				engineRuleSchema.setContType("标准保单");
				engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
				engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
				map.put(engineRuleSchema, "INSERT");
				//modify by zxs
			}
			

    		//获得规则引擎返回的校验信息 调用核保规则
			xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NU", mContNo, false);
			//将校验信息转换为Xml的document对象
			doc = ruleTransfer.stringToDoc(xmlStr);   		
    		//获得校验结果 1自核通过，-1执行异常，0自核不通过
    		approved = ruleTransfer.getApproved(doc);
    		
				// 调用MagNum
				if (!("".equals(mMagNumJson) || (null == mMagNumJson))) {
					System.out.println("调用MagNum了，调用时间是："+PubFun.getCurrentDate() + " " + PubFun.getCurrentTime());
					long startTime2 = System.currentTimeMillis();
					if (!dealMagNumJson()) {
						System.out.println("调用MagNum失败，时间是："+PubFun.getCurrentDate() + " " + PubFun.getCurrentTime());
						return false;
					}
					long endTime2 = System.currentTimeMillis();
					System.out.println("调用MagNum成功，时间是："+PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + "用时："+((endTime2 -startTime2)/1000/60)+"分钟");

					LCContSubSchema tLCContSubSchema = new LCContSubSchema();
					LCContSubDB tLCContSubDB = new LCContSubDB();
					tLCContSubDB.setPrtNo(mLCContSchema.getPrtNo());
					boolean exists = tLCContSubDB.getInfo();
					if (exists) {
						tLCContSubSchema.setSchema(tLCContSubDB.getSchema());
						tLCContSubSchema.setUuid(caseUuid);
						tLCContSubSchema.setModifyDate(PubFun.getCurrentDate());
						tLCContSubSchema.setModifyTime(PubFun.getCurrentTime());
						mMap.put(tLCContSubSchema, "UPDATE");
					} else {

						tLCContSubSchema.setPrtNo(mLCContSchema.getPrtNo());
						tLCContSubSchema.setManageCom(mLCContSchema
								.getManageCom());
						tLCContSubSchema.setOperator(mLCContSchema
								.getOperator());
						tLCContSubSchema.setMakeDate(PubFun.getCurrentDate());
						tLCContSubSchema.setMakeTime(PubFun.getCurrentTime());
						tLCContSubSchema.setModifyDate(PubFun.getCurrentDate());
						tLCContSubSchema.setModifyTime(PubFun.getCurrentTime());
						tLCContSubSchema.setUuid(caseUuid);
						mMap.put(tLCContSubSchema, "INSERT");
					}
				}

    		if(approved==null){
				CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "执行规则引擎核保规则时出错";
                this.mErrors.addOneError(tError);
                return false;
			}
    		if("1".equals(approved)&&"1".equals(mianDecisonpassflag)){
    			
    			mPolPassFlag = "9";
                mContPassFlag = "9";
    		}
    		else if (("1".equals(approved)&&"0".equals(mianDecisonpassflag))){
//    			mPolPassFlag = "5";
                mContPassFlag = "5";
    		}
    		else if("0".equals(approved) || "2".equals(approved)){
    			
    			
    		}else if("-1".equals(approved)){
    			CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "规则引擎核保规则执行异常";
                this.mErrors.addOneError(tError);
                return false;
    		}else {
    			CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "规则引擎核保规则返回了未知的错误类型";
                this.mErrors.addOneError(tError);
                return false;
    		}
    		
    		LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = new LCPolSet();
            LCPolSchema tLCPolSchema = null;
            mContNo = tLCContSchema.getContNo(); //获得保单号
            mPContNo = tLCContSchema.getProposalContNo();
            LMUWSet tLMUWSetSpecial = null; //需要风险检测的特殊核保规则
            LMUWSchema tLMUWSchema = null;
            LMUWSet tLMUWSetPolUnpass=null;
            LMUWSet tLMUWSetCommonPolUnpass=null;
            LMUWSet tLMUWSetInsuredPolUnpass=null;
            tLCPolDB.setContNo(tLCContSchema.getContNo());
            tLCPolSet = tLCPolDB.query();
            this.mPrtNo = tLCPolSet.get(1).getPrtNo();
            
         // modify by zxs 调用新核保规则引擎
         			NewEngineRuleService newEngineRule1 = new NewEngineRuleService();
         			MMap map1 = newEngineRule1.dealNUData(tLCContSchema,"NewQYBZRuleButton");
         			
//          获取所有险种的未通过信息
            tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的核保规则
			if (tLMUWSetPolAllUnpass.size() > 0) {
				for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++) {
					LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
					 EngineRuleSchema nuEngineRuleSchema = new EngineRuleSchema();
			         nuEngineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
			         nuEngineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
			         nuEngineRuleSchema.setApproved(approved);
			         nuEngineRuleSchema.setRuleResource("old");
			         nuEngineRuleSchema.setRuleType("NU");
			         nuEngineRuleSchema.setContType("标准保单");
			         nuEngineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					nuEngineRuleSchema.setMakeTime(PubFun.getCurrentTime());
					nuEngineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
					nuEngineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
					if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
						nuEngineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
					}else{
						nuEngineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
					}
					nuEngineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
					map1.put(nuEngineRuleSchema, "INSERT");
				}
			}else{
				 	EngineRuleSchema nuEngineRuleSchema = new EngineRuleSchema();
		            nuEngineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
		            nuEngineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
		            nuEngineRuleSchema.setApproved(approved);
		            nuEngineRuleSchema.setRuleResource("old");
		            nuEngineRuleSchema.setRuleType("NU");
		            nuEngineRuleSchema.setContType("标准保单");
		        	nuEngineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					nuEngineRuleSchema.setMakeTime(PubFun.getCurrentTime());
					map1.put(nuEngineRuleSchema, "INSERT");
			}
			mData.add(map);
			PubSubmit pubSubmit = new PubSubmit();
			if (!pubSubmit.submitData(mData, "")) {
				this.mErrors.addOneError(pubSubmit.mErrors.getContent());
				return false;
			}
			VData vData = new VData();
			PubSubmit pubSubmit1 = new PubSubmit();
			vData.add(map1);
			if (!pubSubmit1.submitData(vData, "")) {
				this.mErrors.addOneError(pubSubmit1.mErrors.getContent());
				return false;
			}
			// modify by zxs
            
            tLMUWSetInsuredPolUnpass=ruleTransfer.getInsuredUWMSG(tLMUWSetPolAllUnpass);
            //          循环处理险种层核保信息
            for (int i = 1; i <=  tLCPolSet.size(); i++) {
            	mPolPassFlag = "0";
            	if ("1".equals(mLCContSchema.getIntlFlag()))
                {
                    mPolPassFlag = "9"; //无核保规则则置标志为通过
                }else{
                	tLCPolSchema = tLCPolSet.get(i);
                	mOldPolNo = tLCPolSchema.getPolNo(); //获得保单险种号
                	String riskcode = tLCPolSchema.getRiskCode(); //获得保单险种号
                	//获取当前险种的未通过信息
                	tLMUWSetPolUnpass = ruleTransfer.getSinglePolUWMSG(tLMUWSetPolAllUnpass, 
                			riskcode,tLCPolSchema.getInsuredNo());
                	if (tLMUWSetSpecial != null)
                	{
                    	tLMUWSetSpecial.clear();
                	}
                	tLMUWSetSpecial = CheckKinds2(tLCPolSchema);
                	if (tLMUWSetSpecial == null)
                	{
                    	return false;
                	}
//                  准备数据，从险种信息中获取各项计算信息
                    CheckPolInit(tLCPolSchema);
                	if(tLMUWSetPolUnpass.size()>0){
                		for(int j=1;j<=tLMUWSetPolUnpass.size();j++){
                			tLMUWSchema=tLMUWSetPolUnpass.get(j);
//                      	取核保级别
                        	String tuwgrade = tLMUWSchema.getUWGrade();
                        	if (tuwgrade == null)
                        	{
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "UWAutoChkBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "取自核规则编码为："
                                    + tLMUWSchema.getCalCode() + " 的核保级别错误";
                            this.mErrors.addOneError(tError);
                            return false;
                        	}

                        	if (j == 1)
                        	{
                            	mUWGrade = tuwgrade;
                        	}
                        	else
                        	{
                            	if (mUWGrade.compareTo(tuwgrade) < 0)
                            	{
                                	mUWGrade = tuwgrade;
                            	}
                        	}
                		}
                		mPolPassFlag = "5";
                		mContPassFlag = "5";
                		
                	}
                	//需要人工核保时候，校验核保返回核保员核保级别
                	if (tLMUWSetPolUnpass.size() > 0 && tLMUWSetSpecial.size() > 0)
                	{
                    	for (int k = 1; k <= tLMUWSetSpecial.size(); k++)
                    	{
                        	LMUWSchema t2LMUWSchema = new LMUWSchema();
                        	t2LMUWSchema = tLMUWSetSpecial.get(k);
                        	mCalCode = t2LMUWSchema.getCalCode();

                        	String tempuwgrade = checkRiskAmnt(tLCPolSchema);
                        	if (tempuwgrade != null)
                        	{
                            	if (mUWGrade == null
                                    || mUWGrade.compareTo(tempuwgrade) < 0)
                            	{ //当需要人工核保时候当即tLMUWSetUnpass.size()>0时,mUWGrade应该不为null,否则是自动核保规则中核保级别字段缺少了数据
                                	mUWGrade = tempuwgrade;
                            	}
                        	}
                    	}
                	}
                	else
                	{ //当所有的自动核保不成功规则均不与该投保单匹配时核保级别会为空,但一旦要进行核保订正会出现无核保级别的异常保错.所以给所有无核保级别的投保单一个最低默认级别
                    	if (mUWGrade == null || mUWGrade.equals(""))
                    	{
                        	mUWGrade = "A";
                    	}
                	}
                }
//              modify by zhangxing
                LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
                mLCUWErrorSet = CheckCommonError(tLCUWErrorSchema, tLCPolSchema,
                		tLMUWSetPolUnpass);
                if (mLCUWErrorSet == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "在取得公共自核规则的时候发生错误!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (mLCUWErrorSet.size() > 0)
                {
                	mContPassFlag = "5";
                }
                if(mPolPassFlag.equals("0")){
                	mPolPassFlag="9";
                }
                if(tLMUWSetInsuredPolUnpass.size()>0){
                	for(int mm=1;mm<=tLMUWSetInsuredPolUnpass.size();mm++){
                		if(tLCPolSchema.getInsuredNo()
                				.equals(tLMUWSetInsuredPolUnpass.get(mm).getOthCalCode())){
                			mPolPassFlag = "5";
                    		mContPassFlag = "5";
                		}
                	}
                }
                //处理当前险种
                if (dealOnePol(tLCPolSchema, tLMUWSetPolUnpass) == false) {
                    return false;
                }
                
            }
//          处理保单层核保信息
            tLMUWSetCommonPolUnpass = ruleTransfer.getCommonPolUWMSG(tLMUWSetPolAllUnpass);
            if ("1".equals(mLCContSchema.getIntlFlag()))
            {
            	mContPassFlag = "9"; //无核保规则则置标志为通过
            }else{
            	for(int n=1;n<=tLMUWSetCommonPolUnpass.size();n++){
            		tLMUWSchema=tLMUWSetCommonPolUnpass.get(n);
//            		取核保级别
                	String tuwgrade = tLMUWSchema.getUWGrade();
                	if (tuwgrade == null)
                	{
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "合同核保时取自核规则编码为："
                            + tLMUWSchema.getCalCode() + " 的核保级别错误";
                    this.mErrors.addOneError(tError);
                    return false;
                	}

                	if (n == 1 && (mUWGrade == null || mUWGrade.equals("")))
                	{
                    	mUWGrade = tuwgrade;
                	}
                	else
                	{
                    	if (mUWGrade.compareTo(tuwgrade) < 0)
                    	{
                        	mUWGrade = tuwgrade;
                    	}
                	}
                	mContPassFlag = "5";
            	}
            	if (mUWGrade == null || mUWGrade.equals(""))
            	{
                	mUWGrade = "A";
            	}

            	if (mContPassFlag.equals("0"))
            	{
                	mContPassFlag = "9";
            	}
            }
            dealOneCont(tLCContSchema, tLMUWSetCommonPolUnpass);
    	}catch(Exception ex){
    		CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行规则引擎时出错";
            this.mErrors.addOneError(tError);
            return false;
    	}
            
            
            
    		
        }else{	
        	LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = new LCPolSet();
            LCPolSchema tLCPolSchema = null;

            mContNo = tLCContSchema.getContNo(); //获得保单号
            mPContNo = tLCContSchema.getProposalContNo();
            tLCPolDB.setContNo(mContNo);
            tLCPolSet = tLCPolDB.query();
            this.mPrtNo = tLCPolSet.get(1).getPrtNo();
            int nPolCount = tLCPolSet.size();
            int nPolIndex = 0;

            LMUWSet tLMUWSetUnpass = new LMUWSet(); //未通过的核保规则
            LMUWSet tLMUWSetAll = null; //所有核保规则
            LMUWSet tLMUWSetSpecial = null; //需要风险检测的特殊核保规则
            LMUWSchema tLMUWSchema = null;
            for (nPolIndex = 1; nPolIndex <= nPolCount; nPolIndex++)
            {
                tLCPolSchema = tLCPolSet.get(nPolIndex);
                mOldPolNo = tLCPolSchema.getPolNo(); //获得保单险种号

                //准备算法，获取某险种的所有核保规则的集合
                tLMUWSetUnpass.clear();
                if (tLMUWSetAll != null)
                {
                    tLMUWSetAll.clear();
                }
                if (tLMUWSetSpecial != null)
                {
                    tLMUWSetSpecial.clear();
                }
                tLMUWSetAll = CheckKinds(tLCPolSchema);
                if (tLMUWSetAll == null)
                {
                    return false;
                }
                tLMUWSetSpecial = CheckKinds2(tLCPolSchema);
                if (tLMUWSetSpecial == null)
                {
                    return false;
                }

                //准备数据，从险种信息中获取各项计算信息
                CheckPolInit(tLCPolSchema);

                //个人单核保
                mPolPassFlag = "0"; //核保通过标志，初始为未核保
                int n = tLMUWSetAll.size(); //核保规则数量
                if (n == 0 || "1".equals(mLCContSchema.getIntlFlag()))
                {
                    mPolPassFlag = "9"; //无核保规则则置标志为通过
                }
                else
                { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
                    int j = 0;
                    for (int i = 1; i <= n; i++)
                    {
                        //取计算编码
                        tLMUWSchema = new LMUWSchema();
                        tLMUWSchema = tLMUWSetAll.get(i);
                        mCalCode = tLMUWSchema.getCalCode();
                        if (CheckPol(tLCPolSchema.getInsuredNo(), tLCPolSchema.getRiskCode()) == 0)
                        {
                        }
                        else
                        {
                            j++;
                            tLMUWSetUnpass.add(tLMUWSchema);
                            mPolPassFlag = "5"; //待人工核保
                            mContPassFlag = "5";

                            //取核保级别
                            String tuwgrade = tLMUWSchema.getUWGrade();
                            if (tuwgrade == null)
                            {
                                // @@错误处理
                                CError tError = new CError();
                                tError.moduleName = "UWAutoChkBL";
                                tError.functionName = "dealData";
                                tError.errorMessage = "取自核规则编码为："
                                        + tLMUWSchema.getCalCode() + " 的核保级别错误";
                                this.mErrors.addOneError(tError);
                                return false;
                            }

                            if (j == 1)
                            {
                                mUWGrade = tuwgrade;
                            }
                            else
                            {
                                if (mUWGrade.compareTo(tuwgrade) < 0)
                                {
                                    mUWGrade = tuwgrade;
                                }
                            }
                        }
                    }

                    //需要人工核保时候，校验核保返回核保员核保级别
                    if (tLMUWSetUnpass.size() > 0 && tLMUWSetSpecial.size() > 0)
                    {
                        for (int k = 1; k <= tLMUWSetSpecial.size(); k++)
                        {
                            LMUWSchema t2LMUWSchema = new LMUWSchema();
                            t2LMUWSchema = tLMUWSetSpecial.get(k);
                            mCalCode = t2LMUWSchema.getCalCode();

                            String tempuwgrade = checkRiskAmnt(tLCPolSchema);
                            if (tempuwgrade != null)
                            {
                                if (mUWGrade == null
                                        || mUWGrade.compareTo(tempuwgrade) < 0)
                                { //当需要人工核保时候当即tLMUWSetUnpass.size()>0时,mUWGrade应该不为null,否则是自动核保规则中核保级别字段缺少了数据
                                    mUWGrade = tempuwgrade;
                                }
                            }
                        }
                    }
                    else
                    { //当所有的自动核保不成功规则均不与该投保单匹配时核保级别会为空,但一旦要进行核保订正会出现无核保级别的异常保错.所以给所有无核保级别的投保单一个最低默认级别
                        if (mUWGrade == null || mUWGrade.equals(""))
                        {
                            mUWGrade = "A";
                        }
                    }

                    if (mPolPassFlag.equals("0"))
                    {
                        mPolPassFlag = "9";
                    }
                    System.out.println("匹配数:" + tLMUWSetAll.size() + "级别计算:"
                            + tLMUWSetSpecial.size() + "级别:" + mUWGrade);
                }

                //modify by zhangxing
                LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
                mLCUWErrorSet = CheckCommonError(tLCUWErrorSchema, tLCPolSchema,
                        tLMUWSetUnpass);
                if (mLCUWErrorSet == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "在取得公共自核规则的时候发生错误!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (mLCUWErrorSet.size() > 0)
                {
                    mContPassFlag = "5";
                }

                if (dealOnePol(tLCPolSchema, tLMUWSetUnpass) == false)
                {
                    return false;
                }
            }
            /* 合同核保 */
            LMUWSet tLMUWSetContUnpass = new LMUWSet(); //未通过的合同核保规则
            LMUWSet tLMUWSetContAll = CheckKinds3(); //所有合同核保规则

            //准备数据，从险种信息中获取各项计算信息
            CheckContInit(tLCContSchema);

            //个人合同核保
            int tCount = tLMUWSetContAll.size(); //核保规则数量
            if (tCount == 0 || "1".equals(mLCContSchema.getIntlFlag()))
            {
                mContPassFlag = "9"; //无核保规则则置标志为通过
            }
            else
            { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
                int j = 0;
                for (int index = 1; index <= tCount; index++)
                {
                    //取计算编码
                    tLMUWSchema = new LMUWSchema();
                    tLMUWSchema = tLMUWSetContAll.get(index);
                    mCalCode = tLMUWSchema.getCalCode();
                    if (Math
                            .abs(CheckPol(tLCContSchema.getInsuredNo(), "000000") - 0) < 0.001)
                    {
                    }
                    else
                    {
                        j++;
                        tLMUWSetContUnpass.add(tLMUWSchema);
                        mContPassFlag = "5"; //核保不通过，待人工核保

                        //取核保级别
                        String tuwgrade = tLMUWSchema.getUWGrade();
                        if (tuwgrade == null)
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "UWAutoChkBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "合同核保时取自核规则编码为："
                                    + tLMUWSchema.getCalCode() + " 的核保级别错误";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                        if (j == 1 && (mUWGrade == null || mUWGrade.equals("")))
                        {
                            mUWGrade = tuwgrade;
                        }
                        else
                        {
                            if (mUWGrade.compareTo(tuwgrade) < 0)
                            {
                                mUWGrade = tuwgrade;
                            }
                        }
                    }
                }

                if (mUWGrade == null || mUWGrade.equals(""))
                {
                    mUWGrade = "A";
                }

                if (mContPassFlag.equals("0"))
                {
                    mContPassFlag = "9";
                }
                System.out.println("合同核保匹配数:" + tLMUWSetContAll.size()
                        + "合同核保未通过数:" + tLMUWSetContUnpass.size() + "级别:"
                        + mUWGrade);
            }

            dealOneCont(tLCContSchema, tLMUWSetContUnpass);
        	
        }
        /**
         * 处理自核通过的银行转帐信息
         */
        if (mContPassFlag.equals("9"))
        {
            if (!prepareSendFirstPayNotice())
            {
                return false;
            }
        }
        addTempReinsure();

        return true;
    }

	private boolean dealMagNumJson() {//Uuid存哪
		PadMagNumReadJson tPadMagNumReadJson = new PadMagNumReadJson();
		String resultJson = "";
		String statusCode = "";
		try {
			Map<String, String> data = new PadMagNumReadJson().dealPadMagNumRequestJson(mMagNumJson,mLCContSchema.getPrtNo());
			resultJson = data.get("ResultJson");
			statusCode = data.get("StatusCode");
	    	//int statusCode1=Integer.valueOf(statusCode);
			System.out.println("=== MagNum 调用返回Json：  " + resultJson);
			System.out.println("====== 啊 ~~ 五环： " + statusCode);
			if("200".equals(statusCode)){
					Map<String, String> dataDecison = tPadMagNumReadJson.dealResultMainDecison(resultJson);
					//主核保结论
					mianDecison = dataDecison.get("MainDecision");
					//案件CaseUUid
					caseUuid = dataDecison.get("CaseUuid");
					System.out.println("caseUuid===================="+caseUuid);
					//supportCodes
					String	SupportCodes = dataDecison.get("SupportCodes");
					System.out.println("Size===================="+SupportCodes);
					System.out.println("======MagNum 调用返回主结论： " + mianDecison);
				if ("ACCEPT".equals(mianDecison) && "0".equals(SupportCodes)) {
					System.out.println("MagNum返回为"+mianDecison+"..size="+SupportCodes);
				} else if ("ACCEPT".equals(mianDecison) && !"0".equals(SupportCodes)) {
					mianDecisonpassflag = "0";
				} else if ("DECLINE".equals(mianDecison)) {
					mianDecisonpassflag = "0";
				} else if ("POSTPONE".equals(mianDecison)) {
					mianDecisonpassflag = "0";
				} else if ("REFER".equals(mianDecison)) {
					mianDecisonpassflag = "0";
				} else {
					CError tError = new CError();
					tError.moduleName = "ProposalApproveAfterEndService";
					tError.functionName = "dealMagNumJson";
					tError.errorMessage = "MagNum返回主结论返回异常";
					this.mErrors.addOneError(tError);
					return false;
				}
			} else {
				CError tError = new CError();
				tError.moduleName = "ProposalApproveAfterEndService";
				tError.functionName = "dealMagNumJson";
				tError.errorMessage = "调用MagNum出现异常";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			CError tError = new CError();
			tError.moduleName = "ProposalApproveAfterEndService";
			tError.functionName = "dealMagNumJson";
			tError.errorMessage = "调用MagNum出现异常";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
     * 给满足再保条件的个人险种保单置标记
     * zhangbin 添加
     */
    private void addTempReinsure()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        for (int i = 1; i <= mLCUWErrorSet.size(); i++)
        {
            if (StrTool.cTrim(mLCUWErrorSet.get(i).getSugPassFlag())
                    .equals("R"))
            {
                tLCPolDB.setPolNo(mLCUWErrorSet.get(i).getPolNo());
                if (tLCPolDB.getInfo())
                {
                    tLCPolDB.getSchema().setReinsureFlag("1");
                    mLCPolSet.add(tLCPolDB.getSchema());
                }
            }
        }
    }

    /**
     * 根据保额校验核保级别
     * @return
     */
    private String checkRiskAmnt(LCPolSchema tLCPolSchema)
    {
        String tUWGrade = "";
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);

        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", tLCPolSchema.getInsuredNo());
        mCalculator.addBasicFactor("RiskCode", tLCPolSchema.getRiskCode());
        mCalculator.addBasicFactor("IntlFlag",
                (mLCContSchema.getIntlFlag() == null || mLCContSchema
                        .getIntlFlag().equals("")) ? "0" : mLCContSchema
                        .getIntlFlag());

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals(""))
        {
            tUWGrade = "";
        }
        else
        {
            tUWGrade = tStr.trim();
        }

        System.out.println("AmntGrade:" + tUWGrade);

        return tUWGrade;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol(LCPolSchema tLCPolSchema, LMUWSet tLMUWSetUnpass)
    {
        // 保单
        if (preparePol(tLCPolSchema) == false)
        {
            return false;
        }
        // 核保信息
        if (preparePolUW(tLCPolSchema, tLMUWSetUnpass) == false)
        {
            return false;
        }

        LCPolSchema tLCPolSchemaDup = new LCPolSchema();
        tLCPolSchemaDup.setSchema(tLCPolSchema);
        mAllLCPolSet.add(tLCPolSchemaDup);

        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        tLCUWMasterSet.set(mLCUWMasterSet);
        mAllLCUWMasterSet.add(tLCUWMasterSet);

        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        tLCUWSubSet.set(mLCUWSubSet);
        mAllLCUWSubSet.add(tLCUWSubSet);

        LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
        tLCUWErrorSet.set(mLCUWErrorSet);
        mAllLCErrSet.add(tLCUWErrorSet);
        parseUWResult();
        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOneCont(LCContSchema tLCContSchema,
            LMUWSet tLMUWSetContUnpass)
    {
        prepareContUW(tLCContSchema, tLMUWSetContUnpass);
        if (mLCCUWErrorSet.size() > 0)
        {
            mContPassFlag = "5";
        }
        LCContSchema tLCContSchemaDup = new LCContSchema();
        tLCContSchemaDup.setSchema(tLCContSchema);
        mAllLCContSet.add(tLCContSchemaDup);

        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet.set(mLCCUWMasterSet);
        mAllLCCUWMasterSet.add(tLCCUWMasterSet);

        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet.set(mLCCUWSubSet);
        mAllLCCUWSubSet.add(tLCCUWSubSet);

        LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
        tLCCUWErrorSet.set(mLCCUWErrorSet);
        mAllLCCUWErrorSet.add(tLCCUWErrorSet);

        return true;
    }

    /**
     * 校验投保单是否复核
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkApprove(LCContSchema tLCContSchema)
    {
        if (tLCContSchema.getApproveFlag() == null
                || !tLCContSchema.getApproveFlag().equals("9"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "checkApprove";
            tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号："
                    + tLCContSchema.getProposalContNo().trim() + "）";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验核保员级别
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkUWGrade()
    {
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mOperator);

        if (!tLDUserDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperator + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tUWPopedom = tLDUserDB.getUWPopedom();
        if (tUWPopedom == null || tUWPopedom.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "操作员无核保权限，不能核保!（操作员：" + mOperator + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds(LCPolSchema tLCPolSchema)
    {
        String tsql = "";
        LMUWSchema tLMUWSchema = new LMUWSchema();
        //查询算法编码
        tsql = "select * from lmuw where (riskcode = '000000' and relapoltype = 'I' and uwtype = '11') or (riskcode = '"
                + tLCPolSchema.getRiskCode().trim()
                + "' and relapoltype = 'I' and uwtype = '1')  order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds";
            tError.errorMessage = tLCPolSchema.getRiskCode().trim()
                    + "险种核保信息查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds2(LCPolSchema tLCPolSchema)
    {
        String tsql = "";
        LMUWSchema tLMUWSchema = new LMUWSchema();
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '000000' and relapoltype = 'I' and uwtype = '12'";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds2";
            tError.errorMessage = tLCPolSchema.getRiskCode().trim()
                    + "险种信息核保查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds3()
    {
        String tsql = "";
        LMUWSchema tLMUWSchema = new LMUWSchema();
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '000000' and relapoltype = 'I' and uwtype = '19'";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds3";
            tError.errorMessage = "合同险种核保信息查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(LCPolSchema tLCPolSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCPolSchema.getPrem());
        mCalBase.setGet(tLCPolSchema.getAmnt());
        mCalBase.setMult(tLCPolSchema.getMult());
        mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setJob(tLCPolSchema.getOccupationType());
        mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
        mCalBase.setPolNo(tLCPolSchema.getPolNo());
        mCalBase.setContNo(mContNo);
        mCalBase.setCValiDate(tLCPolSchema.getCValiDate());
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckContInit(LCContSchema tLCContSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCContSchema.getPrem());
        mCalBase.setGet(tLCContSchema.getAmnt());
        mCalBase.setMult(tLCContSchema.getMult());
        //            mCalBase.setAppAge( tLCContSchema.getInsuredAppAge() );
        mCalBase.setSex(tLCContSchema.getInsuredSex());
        mCalBase.setCValiDate(tLCContSchema.getCValiDate());
        //            mCalBase.setJob( tLCContSchema.getOccupationType() );
        //            mCalBase.setCount( tLCContSchema.getInsuredPeoples() );
        mCalBase.setContNo(mContNo);
    }

    /**
     * 个人单核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private double CheckPol(String tInsuredNo, String tRiskCode)
    { //LCPolSchema tLCPolSchema)
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", tInsuredNo); //tLCPolSchema.getInsuredNo());;
        mCalculator.addBasicFactor("RiskCode", tRiskCode); //tLCPolSchema.getRiskCode());;
        mCalculator.addBasicFactor("PrtNo", this.mPrtNo);
        mCalculator.addBasicFactor("IntlFlag",
                (mLCContSchema.getIntlFlag() == null || mLCContSchema
                        .getIntlFlag().equals("")) ? "0" : mLCContSchema
                        .getIntlFlag());

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);
        return mValue;
    }

    /**
     * 准备保单信息,自核通过时默认操作员为UW9999,不通过则置为空
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePol(LCPolSchema tLCPolSchema)
    {
        /*-----------自核与复核作为同一个节点时需要置复核状态-----------*/
        tLCPolSchema.setApproveFlag("9");
        tLCPolSchema.setApproveCode(mOperator);
        tLCPolSchema.setApproveDate(PubFun.getCurrentDate());
        tLCPolSchema.setApproveTime(PubFun.getCurrentTime());
        /*--------------------------------------------------------*/

        System.out.println("险种核保标志" + mPolPassFlag);
        if (StrTool.cTrim(this.mPolPassFlag).equals("9"))
        {
            tLCPolSchema.setUWCode("UW9999");
            tLCPolSchema.setUWDate(PubFun.getCurrentDate());
            tLCPolSchema.setUWTime(PubFun.getCurrentTime());
        }
        else
        {
            tLCPolSchema.setUWCode("");
            tLCPolSchema.setUWDate("");
            tLCPolSchema.setUWTime("");
        }
        tLCPolSchema.setUWFlag(mPolPassFlag);
        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 准备合同核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareContUW(LCContSchema tLCContSchema,
            LMUWSet tLMUWSetContUnpass)
    {
        /*-----------自核与复核作为同一个节点时需要置复核状态-----------*/
        tLCContSchema.setApproveFlag("9");
        tLCContSchema.setApproveCode(mOperator);
        tLCContSchema.setApproveDate(PubFun.getCurrentDate());
        tLCContSchema.setApproveTime(PubFun.getCurrentTime());
        /*--------------------------------------------------------*/
        if (StrTool.cTrim(mContPassFlag).equals("9"))
        {
            tLCContSchema.setUWOperator("UW9999");
            tLCContSchema.setUWDate(PubFun.getCurrentDate());
            tLCContSchema.setUWTime(PubFun.getCurrentTime());
        }
        else
        {
            tLCContSchema.setUWOperator("");
            tLCContSchema.setUWDate("");
            tLCContSchema.setUWTime("");
        }
        tLCContSchema.setUWFlag(mContPassFlag);
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());

        //合同核保主表
        boolean firstUW = true;
        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(mContNo);
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet = tLCCUWMasterDB.query();
        if (tLCCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkAfterInitService";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mContNo + "合同核保总表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLCCUWMasterSet.size() == 0)
        {
            tLCCUWMasterSchema.setContNo(mContNo);
            tLCCUWMasterSchema.setGrpContNo(tLCContSchema.getGrpContNo());
            tLCCUWMasterSchema.setProposalContNo(tLCContSchema
                    .getProposalContNo());
            tLCCUWMasterSchema.setUWNo(1);
            tLCCUWMasterSchema.setInsuredNo(tLCContSchema.getInsuredNo());
            tLCCUWMasterSchema.setInsuredName(tLCContSchema.getInsuredName());
            tLCCUWMasterSchema.setAppntNo(tLCContSchema.getAppntNo());
            tLCCUWMasterSchema.setAppntName(tLCContSchema.getAppntName());
            tLCCUWMasterSchema.setAgentCode(tLCContSchema.getAgentCode());
            tLCCUWMasterSchema.setAgentGroup(tLCContSchema.getAgentGroup());
            tLCCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCCUWMasterSchema.setPostponeDay("");
            tLCCUWMasterSchema.setPostponeDate("");
            tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setState(mContPassFlag);
            tLCCUWMasterSchema.setPassFlag(mContPassFlag);
            tLCCUWMasterSchema.setHealthFlag("0");
            tLCCUWMasterSchema.setSpecFlag("0");
            tLCCUWMasterSchema.setQuesFlag("0");
            tLCCUWMasterSchema.setReportFlag("0");
            tLCCUWMasterSchema.setChangePolFlag("0");
            tLCCUWMasterSchema.setPrintFlag("0");
            tLCCUWMasterSchema.setPrintFlag2("0");
            tLCCUWMasterSchema.setManageCom(tLCContSchema.getManageCom());
            tLCCUWMasterSchema.setUWIdea("");
            tLCCUWMasterSchema.setUpReportContent("");
            if (StrTool.cTrim(this.mContPassFlag).equals("9"))
            {
                tLCCUWMasterSchema.setOperator("UW9999"); //操作员
            }
            else
            {
                tLCCUWMasterSchema.setOperator(mOperator); //操作员
            }
            tLCCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            firstUW = false;
            tLCCUWMasterSchema = tLCCUWMasterSet.get(1);
            tLCCUWMasterSchema.setUWNo(tLCCUWMasterSchema.getUWNo() + 1);
            tLCCUWMasterSchema.setState(mContPassFlag);
            tLCCUWMasterSchema.setPassFlag(mContPassFlag);
            tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            if (StrTool.cTrim(this.mContPassFlag).equals("9"))
            {
                tLCCUWMasterSchema.setOperator("UW9999"); //操作员
            }
            else
            {
                tLCCUWMasterSchema.setOperator(mOperator); //操作员
            }
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        mLCCUWMasterSet.clear();
        mLCCUWMasterSet.add(tLCCUWMasterSchema);

        // 合同核保轨迹表
        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
        tLCCUWSubDB.setContNo(mContNo);
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet = tLCCUWSubDB.query();
        if (tLCCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkAfterInitService";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mContNo + "合同核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int nUWNo = tLCCUWSubSet.size();
        if (nUWNo > 0)
        {
            tLCCUWSubSchema.setUWNo(++nUWNo); //第几次核保
        }
        else
        {
            tLCCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCCUWSubSchema.setContNo(tLCCUWMasterSchema.getContNo());
        tLCCUWSubSchema.setGrpContNo(tLCCUWMasterSchema.getGrpContNo());
        tLCCUWSubSchema.setProposalContNo(tLCCUWMasterSchema
                .getProposalContNo());
        tLCCUWSubSchema.setInsuredNo(tLCCUWMasterSchema.getInsuredNo());
        tLCCUWSubSchema.setInsuredName(tLCCUWMasterSchema.getInsuredName());
        tLCCUWSubSchema.setAppntNo(tLCCUWMasterSchema.getAppntNo());
        tLCCUWSubSchema.setAppntName(tLCCUWMasterSchema.getAppntName());
        tLCCUWSubSchema.setAgentCode(tLCCUWMasterSchema.getAgentCode());
        tLCCUWSubSchema.setAgentGroup(tLCCUWMasterSchema.getAgentGroup());
        tLCCUWSubSchema.setUWGrade(tLCCUWMasterSchema.getUWGrade()); //核保级别
        tLCCUWSubSchema.setAppGrade(tLCCUWMasterSchema.getAppGrade()); //申请级别
        tLCCUWSubSchema.setAutoUWFlag(tLCCUWMasterSchema.getAutoUWFlag());
        tLCCUWSubSchema.setState(tLCCUWMasterSchema.getState());
        tLCCUWSubSchema.setPassFlag(tLCCUWMasterSchema.getState());
        tLCCUWSubSchema.setPostponeDay(tLCCUWMasterSchema.getPostponeDay());
        tLCCUWSubSchema.setPostponeDate(tLCCUWMasterSchema.getPostponeDate());
        tLCCUWSubSchema.setUpReportContent(tLCCUWMasterSchema
                .getUpReportContent());
        tLCCUWSubSchema.setHealthFlag(tLCCUWMasterSchema.getHealthFlag());
        tLCCUWSubSchema.setSpecFlag(tLCCUWMasterSchema.getSpecFlag());
        tLCCUWSubSchema.setSpecReason(tLCCUWMasterSchema.getSpecReason());
        tLCCUWSubSchema.setQuesFlag(tLCCUWMasterSchema.getQuesFlag());
        tLCCUWSubSchema.setReportFlag(tLCCUWMasterSchema.getReportFlag());
        tLCCUWSubSchema.setChangePolFlag(tLCCUWMasterSchema.getChangePolFlag());
        tLCCUWSubSchema.setChangePolReason(tLCCUWMasterSchema
                .getChangePolReason());
        tLCCUWSubSchema.setAddPremReason(tLCCUWMasterSchema.getAddPremReason());
        tLCCUWSubSchema.setPrintFlag(tLCCUWMasterSchema.getPrintFlag());
        tLCCUWSubSchema.setPrintFlag2(tLCCUWMasterSchema.getPrintFlag2());
        tLCCUWSubSchema.setUWIdea(tLCCUWMasterSchema.getUWIdea());
        if (StrTool.cTrim(this.mContPassFlag).equals("9"))
        {
            tLCCUWSubSchema.setOperator("UW9999"); //操作员
        }
        else
        {
            tLCCUWSubSchema.setOperator(mOperator); //操作员
        }
        tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
        tLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        mLCCUWSubSet.clear();
        mLCCUWSubSet.add(tLCCUWSubSchema);

        // 核保错误信息表
        LCCUWErrorSchema tLCCUWErrorSchema = new LCCUWErrorSchema();
        LCCUWErrorDB tLCCUWErrorDB = new LCCUWErrorDB();
        tLCCUWErrorDB.setContNo(mContNo);
        LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
        tLCCUWErrorSet = tLCCUWErrorDB.query();
        if (tLCCUWErrorDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWErrorDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkAfterInitService";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mContNo + "合同错误信息表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLCCUWErrorSchema.setSerialNo("0");
        if (nUWNo > 0)
        {
            tLCCUWErrorSchema.setUWNo(nUWNo);
        }
        else
        {
            tLCCUWErrorSchema.setUWNo(1);
        }
        tLCCUWErrorSchema.setContNo(mContNo);
        tLCCUWErrorSchema.setGrpContNo(tLCCUWSubSchema.getGrpContNo());
        tLCCUWErrorSchema
                .setProposalContNo(tLCCUWSubSchema.getProposalContNo());
        // tLCCUWErrorSchema.setInsuredNo(tLCCUWSubSchema.getInsuredNo());
        // tLCCUWErrorSchema.setInsuredName(tLCCUWSubSchema.getInsuredName());
        tLCCUWErrorSchema.setAppntNo(tLCCUWSubSchema.getAppntNo());
        tLCCUWErrorSchema.setAppntName(tLCCUWSubSchema.getAppntName());
        tLCCUWErrorSchema.setManageCom(tLCCUWSubSchema.getManageCom());
        tLCCUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCCUWErrorSchema.setUWError(""); //核保出错信息
        tLCCUWErrorSchema.setCurrValue(""); //当前值
        tLCCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCCUWErrorSchema.setUWPassFlag(mPolPassFlag);

        //取核保错误信息
        mLCCUWErrorSet.clear();
        int merrcount = tLMUWSetContUnpass.size();
        if (merrcount > 0)
        {
            for (int i = 1; i <= merrcount; i++)
            {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetContUnpass.get(i);
                //生成流水号
                String tserialno = "" + i;

                tLCCUWErrorSchema.setSerialNo(tserialno);
                tLCCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                tLCCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息，即核保规则的文字描述内容
                tLCCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCCUWErrorSchema.setCurrValue(""); //当前值

                LCCUWErrorSchema ttLCCUWErrorSchema = new LCCUWErrorSchema();
                ttLCCUWErrorSchema.setSchema(tLCCUWErrorSchema);
                mLCCUWErrorSet.add(ttLCCUWErrorSchema);
            }
        }

        return true;
    }

    /**
     * 准备险种核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePolUW(LCPolSchema tLCPolSchema,
            LMUWSet tLMUWSetUnpass)
    {
        int tuwno = 0;
        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setPolNo(mOldPolNo);
        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        tLCUWMasterSet = tLCUWMasterDB.query();
        if (tLCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保总表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int n = tLCUWMasterSet.size();
        if (n == 0)
        {
            tLCUWMasterSchema.setContNo(mContNo);
            tLCUWMasterSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCUWMasterSchema.setPolNo(mOldPolNo);
            tLCUWMasterSchema.setProposalContNo(mPContNo);
            tLCUWMasterSchema.setProposalNo(tLCPolSchema.getProposalNo());
            tLCUWMasterSchema.setUWNo(1);
            tLCUWMasterSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
            tLCUWMasterSchema.setInsuredName(tLCPolSchema.getInsuredName());
            tLCUWMasterSchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLCUWMasterSchema.setAppntName(tLCPolSchema.getAppntName());
            tLCUWMasterSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLCUWMasterSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCUWMasterSchema.setPostponeDay("");
            tLCUWMasterSchema.setPostponeDate("");
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setState(mPolPassFlag);
            tLCUWMasterSchema.setPassFlag(mPolPassFlag);
            tLCUWMasterSchema.setHealthFlag("0");
            tLCUWMasterSchema.setSpecFlag("0");
            tLCUWMasterSchema.setQuesFlag("0");
            tLCUWMasterSchema.setReportFlag("0");
            tLCUWMasterSchema.setChangePolFlag("0");
            tLCUWMasterSchema.setPrintFlag("0");
            tLCUWMasterSchema.setManageCom(tLCPolSchema.getManageCom());
            tLCUWMasterSchema.setUWIdea("");
            tLCUWMasterSchema.setUpReportContent("");
            if (StrTool.cTrim(this.mPolPassFlag).equals("9"))
            {
                tLCUWMasterSchema.setOperator("UW9999"); //操作员
            }
            else
            {
                tLCUWMasterSchema.setOperator(mOperator); //操作员
            }
            tLCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else if (n == 1)
        {
            tLCUWMasterSchema = tLCUWMasterSet.get(1);

            tuwno = tLCUWMasterSchema.getUWNo();
            tuwno = tuwno + 1;

            tLCUWMasterSchema.setUWNo(tuwno);
            tLCUWMasterSchema.setProposalContNo(mPContNo);
            tLCUWMasterSchema.setState(mPolPassFlag);
            tLCUWMasterSchema.setPassFlag(mPolPassFlag);
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            if (StrTool.cTrim(this.mPolPassFlag).equals("9"))
            {
                tLCUWMasterSchema.setOperator("UW9999"); //操作员
            }
            else
            {
                tLCUWMasterSchema.setOperator(mOperator); //操作员
            }
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保总表取数据不唯一!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCUWMasterSet.clear();
        mLCUWMasterSet.add(tLCUWMasterSchema);

        // 核保轨迹表
        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
        tLCUWSubDB.setPolNo(mOldPolNo);
        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        tLCUWSubSet = tLCUWSubDB.query();
        if (tLCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        m = tLCUWSubSet.size();
        if (m > 0)
        {
            tLCUWSubSchema.setUWNo(++m); //第几次核保
        }
        else
        {
            tLCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCUWSubSchema.setContNo(mContNo);
        tLCUWSubSchema.setPolNo(mOldPolNo);
        tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
        tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema.getProposalContNo());
        tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
        tLCUWSubSchema.setInsuredNo(tLCUWMasterSchema.getInsuredNo());
        tLCUWSubSchema.setInsuredName(tLCUWMasterSchema.getInsuredName());
        tLCUWSubSchema.setAppntNo(tLCUWMasterSchema.getAppntNo());
        tLCUWSubSchema.setAppntName(tLCUWMasterSchema.getAppntName());
        tLCUWSubSchema.setAgentCode(tLCUWMasterSchema.getAgentCode());
        tLCUWSubSchema.setAgentGroup(tLCUWMasterSchema.getAgentGroup());
        tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade()); //核保级别
        tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade()); //申请级别
        tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
        tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPassFlag(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPostponeDay(tLCUWMasterSchema.getPostponeDay());
        tLCUWSubSchema.setPostponeDate(tLCUWMasterSchema.getPostponeDate());
        tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema
                .getUpReportContent());
        tLCUWSubSchema.setHealthFlag(tLCUWMasterSchema.getHealthFlag());
        tLCUWSubSchema.setSpecFlag(tLCUWMasterSchema.getSpecFlag());
        tLCUWSubSchema.setSpecReason(tLCUWMasterSchema.getSpecReason());
        tLCUWSubSchema.setQuesFlag(tLCUWMasterSchema.getQuesFlag());
        tLCUWSubSchema.setReportFlag(tLCUWMasterSchema.getReportFlag());
        tLCUWSubSchema.setChangePolFlag(tLCUWMasterSchema.getChangePolFlag());
        tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema
                .getChangePolReason());
        tLCUWSubSchema.setAddPremReason(tLCUWMasterSchema.getAddPremReason());
        tLCUWSubSchema.setPrintFlag(tLCUWMasterSchema.getPrintFlag());
        tLCUWSubSchema.setPrintFlag2(tLCUWMasterSchema.getPrintFlag2());
        tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
        if (StrTool.cTrim(this.mPolPassFlag).equals("9"))
        {
            tLCUWSubSchema.setOperator("UW9999"); //操作员
        }
        else
        {
            tLCUWSubSchema.setOperator(mOperator); //操作员
        }
        tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
        tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        mLCUWSubSet.clear();
        mLCUWSubSet.add(tLCUWSubSchema);

        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(LCContSchema tLCContSchema)
    {

        mMap.put(tLCContSchema, "UPDATE");
        //          if(mLCCUWMasterSet.size() ==1)
        mMap.put(mLCCUWMasterSet.get(1), "DELETE&INSERT");
        mMap.put(mLCCUWSubSet, "INSERT");

        // 解决人工核保回退后，再次自核时，自核报告冲突。
        // mMap.put(mLCCUWErrorSet, "INSERT");
        mMap.put(mLCCUWErrorSet, SysConst.DELETE_AND_INSERT);
        // --------------------------------

        mMap.put(mAllLCPolSet, "UPDATE");
        mMap.put(mLCPolSet, "UPDATE"); //zhangbin添加
        int n = mAllLCUWMasterSet.size();
        for (int i = 1; i <= n; i++)
        {
            LCUWMasterSchema tLCUWMasterSchema = mAllLCUWMasterSet.get(i);
            mMap.put(tLCUWMasterSchema, "DELETE&INSERT");
        }
        mMap.put(mAllLCUWSubSet, "INSERT");

        // 解决人工核保回退后，再次自核时，自核报告冲突。
        //mMap.put(mAllLCErrSet, "INSERT");
        mMap.put(mAllLCErrSet, SysConst.DELETE_AND_INSERT);
        // --------------------------------

        return false;
    }

    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {
        //校验核保级别
        if (!checkUWGrade())
        {
            return false;
        }

        //校验是否复核
        //        if (!checkApprove(mLCContSchema))
        //            return false;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        if (mOperator == null || mOperator.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PEdorUWSendNoticeAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (mContNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中mContNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (tLCContDB.getInfo())
        { //验证LCCont表中是否存在该合同项记录
            mLCContSchema.setSchema(tLCContDB.getSchema());
            System.out.println("....................mLCContSchema.getProposalContNo()"+mLCContSchema.getProposalContNo());
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "合同号为" + mLCContSchema.getContNo() + "未查询到!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获取MagNumJson请求数据
        mMagNumJson = (String) mTransferData.getValueByName("MagNumJson");
        System.out.println("=====  MagNum 请求Json：  "+mMagNumJson);
        return true;
    }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return
     */
    private boolean prepareTransferData()
    {
        mTransferData.setNameAndValue("ContNo", mLCContSchema.getContNo());
        mTransferData.setNameAndValue("PrtNo", mLCContSchema.getPrtNo());
        mTransferData.setNameAndValue("AppntNo", mLCContSchema.getAppntNo());
        mTransferData
                .setNameAndValue("AppntName", mLCContSchema.getAppntName());
        mTransferData.setNameAndValue("ProposalContNo", mLCContSchema
                .getProposalContNo());
        mTransferData
                .setNameAndValue("AgentCode", mLCContSchema.getAgentCode());
        
        String tUwCode=getDefaultOperator(mLCContSchema.getContNo());
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (tLAAgentSet == null || tLAAgentSet.size() != 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人表LAAgent查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mTransferData
                .setNameAndValue("AgentName", tLAAgentSet.get(1).getName());
        mTransferData
                .setNameAndValue("ManageCom", mLCContSchema.getManageCom());
        
        if (mPolPassFlag.equals("5"))
        {
            mContPassFlag = "5";
        }
        mTransferData.setNameAndValue("UwCode", tUwCode);
        mTransferData.setNameAndValue("UWFlag", mContPassFlag);
        mTransferData.setNameAndValue("UWDate", PubFun.getCurrentDate());
        return true;
    }
    public String getDefaultOperator(String tContNo){
    	
    	String tDefaultOperator="";
    	String tUwCodeSql="select UwCode from lcuwsendtrace where sendtype = '4' " +
    						"and othernotype = '3' and otherno='"+tContNo+"' " +
    								"order by  UWNO desc  fetch first 1 rows only with ur";
    	    	SSRS ttSSRS = new ExeSQL().execSQL(tUwCodeSql);
    	    	if(ttSSRS.MaxRow>0&&!ttSSRS.GetText(1, 1).equals("") && ttSSRS.GetText(1, 1) != null)
    	    		tDefaultOperator=ttSSRS.GetText(1, 1);
    	return tDefaultOperator;
    }
    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 被保险人的信息校验包括:
     * 生日校验
     * 姓名校验
     * 身份证位数校验
     * 性别校验
     * @param chkLCInsuredSet LCCUWErrorSet
     * @return boolean
     * @author YangMing
     */
    private String[] checkInsured(LCInsuredSchema chkLCInsuredSchema)
    {
        String[] Err = new String[12];
        int n = 0;
        String Birthday = chkLCInsuredSchema.getBirthday();
        if (Birthday.length() != 10)
        {
            Err[n] = "被保险人[" + chkLCInsuredSchema.getName() + "]的生日信息有误";
            n++;
        }
        String Year = Birthday.substring(0, 4);
        String Month = Birthday.substring(5, 7);
        String Day = Birthday.substring(8);
        if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500)
        {
            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName() + "]的出生年份是否有误";
            n++;
        }
        if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12)
        {
            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName() + "]的出生月份是否有误";
            n++;
        }
        if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32)
        {
            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName() + "]的出生日期份是否有误";
            n++;
        }
        if (chkLCInsuredSchema.getName() == null
                || "".equals(chkLCInsuredSchema.getName()))
        {
            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName() + "]的姓名是否有误 ";
            n++;
        }
        if (chkLCInsuredSchema.getSex() == null
                || "".equals(chkLCInsuredSchema.getSex())
                || (!"1".equals(chkLCInsuredSchema.getSex()) && !"0"
                        .equals(chkLCInsuredSchema.getSex())))
        {
            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName() + "]的性别是否有误 ";
            n++;
        }
        String IDNo = chkLCInsuredSchema.getIDNo();
//        if (chkLCInsuredSchema.getIDType() == null
//                || "".equals(chkLCInsuredSchema.getIDType()))
//        {
//            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName() + "]的证件类型否有误 ";
//            n++;
//        }
        if (chkLCInsuredSchema.getIDType() != null)
        {
            if (chkLCInsuredSchema.getIDType().equals("0")
                    && chkLCInsuredSchema.getIDType() != null)
            {
                int IDNoBit = IDNo.length();
                if ("0".equals(chkLCInsuredSchema.getIDType()))
                {
                    if (IDNoBit != 15 && IDNoBit != 18)
                    {
                        Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName()
                                + "]的身份证号码位数是否有误！";
                        n++;
                    }
                    if (IDNoBit == 15)
                    {

                        String subYear = IDNo.substring(6, 8);
                        String subMonth = IDNo.substring(8, 10);
                        String subDay = IDNo.substring(10, 12);
                        String subSex = IDNo.substring(13);
                        String Choice = "";
                        String Sex = chkLCInsuredSchema.getSex();
                        if (Integer.parseInt(subSex) % 2 == 1)
                        {
                            Choice = "0";
                        }
                        if (Integer.parseInt(subSex) % 2 == 0)
                        {
                            Choice = "1";
                        }
                        if (!Choice.equals(Sex))
                        {
                            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人性别是否对应！";
                            n++;
                        }
                        if (!Year.substring(2).equals(subYear)
                                || !subMonth.equals(Month)
                                || !subDay.equals(Day))
                        {
                            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人生日是否对应！";
                            n++;
                        }
                    }
                    if (IDNoBit == 18)
                    {
                        String subYear = IDNo.substring(6, 10);
                        String subMonth = IDNo.substring(10, 12);
                        String subDay = IDNo.substring(12, 14);
                        String subSex = IDNo.substring(16, 17);
                        String Sex = chkLCInsuredSchema.getSex();
                        String Choice = "";
                        if (Integer.parseInt(subSex) % 2 == 1)
                        {
                            Choice = "0";
                        }
                        if (Integer.parseInt(subSex) % 2 == 0)
                        {
                            Choice = "1";
                        }
                        if (!Choice.equals(Sex))
                        {
                            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人性别是否对应！";
                            n++;
                        }

                        if (!Year.equals(subYear) || !subMonth.equals(Month)
                                || !subDay.equals(Day))
                        {
                            Err[n] = "请检查被保险人[" + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人生日是否对应！";
                            n++;
                        }
                    }
                }
            }
        }
        return Err;
    }

    /**
     * 投保人的信息校验包括:
     * 生日校验
     * 姓名校验
     * 身份证位数校验
     * 性别校验
     * @param chkLCAppntSchema LCAppntSchema
     * @return boolean
     * @author YangMing
     */
    private String[] checkAppnt(LCAppntSchema chkLCAppntSchema)
    {
        String[] Err = new String[12];
        int n = 0;
        String Birthday = chkLCAppntSchema.getAppntBirthday();
        if (Birthday.length() != 10)
        {
            Err[n] = "投保人与被保人为本人，请检查投保人[" + chkLCAppntSchema.getAppntName()
                    + "]的生日信息有误";
            n++;
        }
        String Year = Birthday.substring(0, 4);
        String Month = Birthday.substring(5, 7);
        String Day = Birthday.substring(8);
        if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500)
        {
            Err[n] = "投保人与被保人为本人，请检查投保人[" + chkLCAppntSchema.getAppntName()
                    + "]的出生年份是否有误";
            n++;
        }
        if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12)
        {
            Err[n] = "投保人与被保人为本人，请检查投保人[" + chkLCAppntSchema.getAppntName()
                    + "]的出生月份是否有误";
            n++;
        }
        if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32)
        {
            Err[n] = "请检查投保人[" + chkLCAppntSchema.getAppntName()
                    + "]的出生日期份是否有误";
            n++;
        }
        if (chkLCAppntSchema.getAppntName() == null
                || "".equals(chkLCAppntSchema.getAppntName()))
        {
            Err[n] = "投保人与被保人为本人，请检查投保人[" + chkLCAppntSchema.getAppntName()
                    + "]的姓名是否有误 ";
            n++;
        }
        if (chkLCAppntSchema.getAppntSex() == null
                || "".equals(chkLCAppntSchema.getAppntSex())
                || (!"1".equals(chkLCAppntSchema.getAppntSex()) && !"0"
                        .equals(chkLCAppntSchema.getAppntSex())))
        {
            Err[n] = "投保人与被保人为本人，请检查投保人[" + chkLCAppntSchema.getAppntName()
                    + "]的性别是否有误 ";
            n++;
        }
        String IDNo = chkLCAppntSchema.getIDNo();

//        if (chkLCAppntSchema.getIDType() == null
//                || "".equals(chkLCAppntSchema.getIDType()))
//        {
//            Err[n] = "投保人与被保人为本人，请检查投保人[" + chkLCAppntSchema.getAppntName()
//                    + "]的证件类型否有误 ";
//            n++;
//        }
        if ("0".equals(chkLCAppntSchema.getIDType()))
        {
            int IDNoBit = IDNo.length();
            if ("0".equals(chkLCAppntSchema.getIDType()))
            {
                if (IDNoBit != 15 && IDNoBit != 18)
                {
                    Err[n] = "投保人与被保人为本人，请检查投保人["
                            + chkLCAppntSchema.getAppntName()
                            + "]的身份证号码位数是否有误！";
                    n++;
                }
                if (IDNoBit == 15)
                {

                    String subYear = IDNo.substring(6, 8);
                    String subMonth = IDNo.substring(8, 10);
                    String subDay = IDNo.substring(10, 12);
                    String subSex = IDNo.substring(13);
                    String Choice = "";
                    String Sex = chkLCAppntSchema.getAppntSex();
                    if (Integer.parseInt(subSex) % 2 == 1)
                    {
                        Choice = "0";
                    }
                    if (Integer.parseInt(subSex) % 2 == 0)
                    {
                        Choice = "1";
                    }
                    if (!Choice.equals(Sex))
                    {
                        Err[n] = "投保人与被保人为本人，请检查投保人["
                                + chkLCAppntSchema.getAppntName()
                                + "]的身份证号码与投保人性别是否对应！";
                        n++;
                    }
                    if (!Year.substring(2).equals(subYear)
                            || !subMonth.equals(Month) || !subDay.equals(Day))
                    {
                        Err[n] = "投保人与被保人为本人，请检查投保人["
                                + chkLCAppntSchema.getAppntName()
                                + "]的身份证号码与投保人生日是否对应！";
                        n++;
                    }
                }
                if (IDNoBit == 18)
                {
                    String subYear = IDNo.substring(6, 10);
                    String subMonth = IDNo.substring(10, 12);
                    String subDay = IDNo.substring(12, 14);
                    String subSex = IDNo.substring(16, 17);
                    String Sex = chkLCAppntSchema.getAppntSex();
                    String Choice = "";
                    if (Integer.parseInt(subSex) % 2 == 1)
                    {
                        Choice = "0";
                    }
                    if (Integer.parseInt(subSex) % 2 == 0)
                    {
                        Choice = "1";
                    }
                    if (!Choice.equals(Sex))
                    {
                        Err[n] = "投保人与被保人为本人，请检查投保人["
                                + chkLCAppntSchema.getAppntName()
                                + "]的身份证号码与被保险人性别是否对应！";
                        n++;
                    }

                    if (!Year.equals(subYear) || !subMonth.equals(Month)
                            || !subDay.equals(Day))
                    {
                        Err[n] = "投保人与被保人为本人，请检查投保人["
                                + chkLCAppntSchema.getAppntName()
                                + "]的身份证号码与被保险人生日是否对应！";
                        n++;
                    }
                }
            }
        }
        return Err;
    }

    /**
     * 校验投保人与被保人的各种家庭关系,其中包块身份与性别的匹配
     * 身份与年龄的匹配,逻辑关系是否正确.
     * @author Yangming
     * @param chkLCInsuredSet LCInsuredSet
     * @param chkLCAppntSchema LCAppntSchema
     * @return String[]
     */
    private String[] chkFamilyRelation(LCInsuredSchema chkLCInsuredSchema,
            LCAppntSchema chkLCAppntSchema)
    {
        String[] Err = null;
        //    int insuredNum = chkLCInsuredSet.size();
        int AppntAge = PubFun.calInterval2(chkLCAppntSchema.getAppntBirthday(),
                PubFun.getCurrentDate(), "Y");
        String InsuredToAppnt;
        String InsuredToAppntErr = "";
        // LCInsuredSchema MainInsuredSchema = new LCInsuredSchema();
        InsuredToAppnt = chkLCInsuredSchema.getRelationToAppnt();
        System.out.println("InsuredToAppnt" + InsuredToAppnt);
        if (!StrTool.cTrim(InsuredToAppnt).equals(""))
        {
            int InsuredAge = PubFun.calInterval2(chkLCInsuredSchema
                    .getBirthday(), PubFun.getCurrentDate(), "Y");
            InsuredToAppntErr += showDifferent(chkLCInsuredSchema.getName(),
                    chkLCInsuredSchema.getSex(), InsuredAge, chkLCAppntSchema
                            .getAppntName(), chkLCAppntSchema.getAppntSex(),
                    AppntAge, InsuredToAppnt);
            System.out.println("chkLCInsuredSchema.getMarriage()"
                    + chkLCInsuredSchema.getMarriage());
            if (chkLCInsuredSchema.getMarriage() == null
                    && !"1".equals(mLCContSchema.getIntlFlag()))
            {
                InsuredToAppntErr += "[" + chkLCInsuredSchema.getName()
                        + "]的婚姻状况录入有误,"; //国际业务不需要人工核保
            }
        }
        /*
         String MainName = MainInsuredSchema.getName();
         String MainSex = MainInsuredSchema.getSex();
         String MainBirthday = MainInsuredSchema.getBirthday();
         int MainAge = PubFun.calInterval2(MainBirthday,
         PubFun.getCurrentDate(), "Y");
         for (int i = 1; i <= chkLCInsuredSet.size(); i++) {
         if (chkLCInsuredSet.get(i).getMarriage() == null) {
         InsuredToAppntErr += "[" + chkLCInsuredSet.get(i).getName() +
         "]的婚姻状况录入有误,";
         }
         String InsuredToMainInsured = chkLCInsuredSet.get(i).
         getRelationToMainInsured();
         int InsuredAge = PubFun.calInterval2(chkLCInsuredSet.get(i).getBirthday(),
         PubFun.getCurrentDate(), "Y");
         if (InsuredToAppnt[i - 1] != null) {
         if (InsuredToAppnt[i - 1] != null || !InsuredToAppnt[i - 1].equals("")) {
         InsuredToAppntErr += showDifferent(chkLCInsuredSet.get(i).getName(),
         chkLCInsuredSet.get(i).getSex(),
         InsuredAge,
         MainName,
         MainSex,
         MainAge, InsuredToMainInsured);
         }
         }
         }
         */
        System.out.println("InsuredToAppntErr" + InsuredToAppntErr);
        if (!InsuredToAppntErr.equals(""))
        {
            Err = InsuredToAppntErr.split(",");
            System.out.println("Err" + Err);
            return Err;
        }
        else
        {
            return null;
        }
    }

    /**
     * 将校验信息抽象成一个函数，参数是比较者的性别，年龄，被比较者的性别年龄，两者之间的关系
     * @author Yangming
     */
    private String showDifferent(String chkName, String chkSex, int chkAge,
            String reChkName, String reChkSex, int reChkAge, String Relation)
    {
        String sql = "select codename from ldcode where codetype='relation' and"
                + " code='" + Relation + "'";
        ExeSQL tExecSQL = new ExeSQL();
        String relationName = tExecSQL.getOneValue(sql);
        String ErrInfo = "";
        /**
         * 本人00	妻子01	丈夫02	儿女03	父亲04	母亲05	岳父06	岳母07
         * 女婿08	公公09	婆婆10	媳妇11	兄弟姐妹12 	表兄弟姐妹13
         * 叔伯14 	姑姑15	舅舅16	姨17	侄子/女18	爷爷19	奶奶20
         * 孙子/女21	外公22	外婆23	外孙子/女24
         */
        //1、性别是女的关系
        if (Relation.equals("01") || Relation.equals("05")
                || Relation.equals("07") || Relation.equals("10")
                || Relation.equals("11") || Relation.equals("15")
                || Relation.equals("17") || Relation.equals("20")
                || Relation.equals("23"))
        {
            if (!chkSex.equals("1"))
            {
                ErrInfo += chkName + "与" + reChkName + "的关系是" + relationName
                        + "与" + chkName + "的性别不符,";
            }
        }
        /**
         * 本人00	妻子01	丈夫02	儿女03	父亲04	母亲05	岳父06	岳母07
         * 女婿08	公公09	婆婆10	媳妇11	兄弟姐妹12 	表兄弟姐妹13
         * 叔伯14 	姑姑15	舅舅16	姨17	侄子/女18	爷爷19	奶奶20
         * 孙子/女21	外公22	外婆23	外孙子/女24
         */
        //2、姓名是男的关系
        if (Relation.equals("02") || Relation.equals("04")
                || Relation.equals("06") || Relation.equals("08")
                || Relation.equals("09") || Relation.equals("14")
                || Relation.equals("16") || Relation.equals("19")
                || Relation.equals("22"))
        {
            if (!chkSex.equals("0"))
            {
                ErrInfo += chkName + "与" + reChkName + "的关系是" + relationName
                        + "与" + chkName + "的性别不符,";
            }
        }
        /**
         * 本人00	妻子01	丈夫02	儿女03	父亲04	母亲05	岳父06	岳母07
         * 女婿08	公公09	婆婆10	媳妇11	兄弟姐妹12 	表兄弟姐妹13
         * 叔伯14 	姑姑15	舅舅16	姨17	侄子/女18	爷爷19	奶奶20
         * 孙子/女21	外公22	外婆23	外孙子/女24
         */
        //3、被保人与投保人的关系可以决定投保人的性别的关系
        if (Relation.equals("02"))
        {
            //如果是丈夫关系,投保人应为女
            if (!StrTool.cTrim(reChkSex).equals("1"))
            {
                ErrInfo += reChkName + "与" + chkName + "的关系是" + relationName
                        + "与" + reChkName + "的性别不符,";
                //如果是妻子校验是否成年
//                if (reChkAge < 18)
//                {
//                    ErrInfo += reChkName + "的关系是妻子但未成年 请检查,";
//                }
            }
        }
        if (Relation.equals("01"))
        {
            //如果是妻子关系,投保人应为男
            if (!reChkSex.equals("0"))
            {
                ErrInfo += reChkName + "与" + chkName + "的关系是" + relationName
                        + "与" + reChkName + "的性别不符,";
                //如果是丈夫校验是否成年
//                if (chkAge < 18)
//                {
//                    ErrInfo += reChkName + "有婚姻关系但未成年 请检查,";
//                }
            }
        }
        /**
         * 本人00	妻子01	丈夫02	儿女03	父亲04	母亲05	岳父06	岳母07
         * 女婿08	公公09	婆婆10	媳妇11	兄弟姐妹12 	表兄弟姐妹13
         * 叔伯14 	姑姑15	舅舅16	姨17	侄子/女18	爷爷19	奶奶20
         * 孙子/女21	外公22	外婆23	外孙子/女24
         */
        //4、如果是上下长辈晚辈关系校验年龄大小
        if (Relation.equals("03") || Relation.equals("21")
                || Relation.equals("24"))
        {
            //如果是儿女、孙子/女、外孙子/女,年龄应小于投保人
//            if (chkAge > reChkAge)
//            {
//                ErrInfo += chkName + "与" + reChkName + "的关系是" + relationName
//                        + "但年龄大于" + reChkName + " 请检查,";
//            }
        }
        if (Relation.equals("04") || Relation.equals("05")
                || Relation.equals("09") || Relation.equals("10")
                || Relation.equals("19") || Relation.equals("20")
                || Relation.equals("22") || Relation.equals("23"))
        {
            //如果是父亲、母亲、岳父、岳母、爷爷、奶奶、外公、外婆,年龄应大于投保人
//            if (chkAge < reChkAge)
//            {
//                ErrInfo += chkName + "与" + reChkName + "的关系是" + relationName
//                        + "但年龄小于" + reChkName + " 请检查,";
//            }
        }

        return ErrInfo;
    }

    private String[] checkBnf(LCInsuredSet chkLCInsuredSet,
            LCPolSchema chktLCPolSchema)
    {
        LCBnfDB tLCBnfDB = new LCBnfDB();
        String[][] Err = new String[chkLCInsuredSet.size()][];
        for (int i = 1; i <= chkLCInsuredSet.size(); i++)
        {
            tLCBnfDB.setPolNo(chktLCPolSchema.getPolNo());
            tLCBnfDB.setInsuredNo(chkLCInsuredSet.get(i).getInsuredNo());
            LCBnfSet tLCBnfSet = new LCBnfSet();
            tLCBnfSet = tLCBnfDB.query();
            System.out.println("存在受益人 : " + tLCBnfSet.size());
            for (int m = 1; m <= tLCBnfSet.size(); m++)
            {
                Err[i - 1] = checkBnfInfo(tLCBnfSet.get(m));
            }
        }
        String ErrInfo = "";
        if (Err != null)
        {
            for (int i = 0; i < Err.length; i++)
            {
                if (Err[i] != null)
                {
                    for (int a = 0; a < Err[i].length; a++)
                    {
                        if (Err[i][a] != null)
                        {
                            ErrInfo += Err[i][a] + ";";
                        }
                    }
                }
            }
            return ErrInfo.split(";");
        }
        else
        {
            return null;
        }
    }

    /**
     * 投保人的信息校验包括:
     * 生日校验
     * 姓名校验
     * 身份证位数校验
     * 性别校验
     * @param chkLCAppntSchema LCAppntSchema
     * @return boolean
     * @author YangMing
     */
    private String[] checkBnfInfo(LCBnfSchema chkLCBnfSchema)
    {
        String[] Err = new String[12];
        int n = 0;
        String Year = "";
        String Month = "";
        String Day = "";
        String Birthday = chkLCBnfSchema.getBirthday();
        if (Birthday == null)
        {
            return null;
        }
        if (Birthday.length() != 10)
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的生日信息有误";
            n++;
        }
        Year = Birthday.substring(0, 4);
        Month = Birthday.substring(5, 7);
        Day = Birthday.substring(8);
        if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500)
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的出生年份是否有误";
            n++;
        }
        if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12)
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的出生月份是否有误";
            n++;
        }
        if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32)
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的出生日期份是否有误";
            n++;
        }
        if (chkLCBnfSchema.getName() == null
                || "".equals(chkLCBnfSchema.getName()))
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的姓名是否有误 ";
            n++;
        }
        if (chkLCBnfSchema.getSex() == null
                || "".equals(chkLCBnfSchema.getSex())
                || (!"1".equals(chkLCBnfSchema.getSex()) && !"0"
                        .equals(chkLCBnfSchema.getSex())))
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的性别是否有误 ";
            n++;
        }
        String IDNo = chkLCBnfSchema.getIDNo();

        if (chkLCBnfSchema.getIDType() == null
                || "".equals(chkLCBnfSchema.getIDType()))
        {
            Err[n] = "请检查受益人[" + chkLCBnfSchema.getName() + "]的证件类型否有误 ";
            n++;
        }
        if (chkLCBnfSchema.getIDType().equals("0"))
        {
            int IDNoBit = IDNo.length();
            if ("0".equals(chkLCBnfSchema.getIDType()))
            {
                if (IDNoBit != 15 && IDNoBit != 18)
                {
                    Err[n] = "请检查受益人[" + chkLCBnfSchema.getName()
                            + "]的身份证号码位数是否有误！";
                    n++;
                }
                if (IDNoBit == 15)
                {

                    String subYear = IDNo.substring(6, 8);
                    String subMonth = IDNo.substring(8, 10);
                    String subDay = IDNo.substring(10, 12);
                    String subSex = IDNo.substring(13);
                    String Choice = "";
                    String Sex = chkLCBnfSchema.getSex();
                    if (Integer.parseInt(subSex) % 2 == 1)
                    {
                        Choice = "0";
                    }
                    if (Integer.parseInt(subSex) % 2 == 0)
                    {
                        Choice = "1";
                    }
                    if (!Choice.equals(Sex))
                    {
                        Err[n] = "请检查受益人[" + chkLCBnfSchema.getName()
                                + "]的身份证号码与受益人性别是否对应！";
                        n++;
                    }
                    if (!Year.substring(2).equals(subYear)
                            || !subMonth.equals(Month) || !subDay.equals(Day))
                    {
                        Err[n] = "请检查受益人[" + chkLCBnfSchema.getName()
                                + "]的身份证号码与受益人生日是否对应！";
                        n++;
                    }
                }
                if (IDNoBit == 18)
                {
                    String subYear = IDNo.substring(6, 10);
                    String subMonth = IDNo.substring(10, 12);
                    String subDay = IDNo.substring(12, 14);
                    String subSex = IDNo.substring(16, 17);
                    String Sex = chkLCBnfSchema.getSex();
                    String Choice = "";
                    if (Integer.parseInt(subSex) % 2 == 1)
                    {
                        Choice = "0";
                    }
                    if (Integer.parseInt(subSex) % 2 == 0)
                    {
                        Choice = "1";
                    }
                    if (!Choice.equals(Sex))
                    {
                        Err[n] = "请检查受益人[" + chkLCBnfSchema.getName()
                                + "]的身份证号码与受益人性别是否对应！";
                        n++;
                    }

                    if (!Year.equals(subYear) || !subMonth.equals(Month)
                            || !subDay.equals(Day))
                    {
                        Err[n] = "请检查受益人[" + chkLCBnfSchema.getName()
                                + "]的身份证号码与受益人生日是否对应！";
                        n++;
                    }
                }
            }
        }
        return Err;
    }

    /**
     * 用于客户选择交费方式为银行转帐
     * 自动在财务收费里面添加一条银行
     * 转帐信息
     * 2005-05-23 杨明 添加
     * @return boolean
     */
    private boolean prepareSendFirstPayNotice()
    {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode("07");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AutoUWFlag", "1");
        VData tempVData = new VData();
        tempVData.add(tLOPRTManagerSchema);
        tempVData.add(mGlobalInput);
        tempVData.add(tTransferData);
        
        //修改复核完毕时，打印队列仍有数据的错误                   by zhangyang
        CheckLOPrtManager(mLCContSchema); 
        //-----------------------------------------------------
        
        UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
        if(tUWSendPrintUI.submitData(tempVData, "INSERT")== false){
        	System.out.println(tUWSendPrintUI.mErrors.getFirstError());
            this.mErrors.copyAllErrors(tUWSendPrintUI.mErrors);
            return false;
        };
        
        return true;
    }

    private String parseUWResult(LMUWSchema tLMUWSchema)
    {
        System.out
                .println("GrpUWAutoChkBL.parseUWResult(tLMUWSchema)  \n--Line:1350  --Author:YangMing");
        String result = null;
        PubCalculator tPubCalculator = new PubCalculator();
        tPubCalculator.addBasicFactor("GrpContNo", this.mContNo);
        tPubCalculator.addBasicFactor("polno", this.mOldPolNo);
        String tSql = tLMUWSchema.getRemark().trim();
        String tStr = "";
        String tStr1 = "";
        try
        {
            while (true)
            {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals(""))
                {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator
                        .calculate());
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "interpretFactorInSQL";
            tError.errorMessage = "解释" + tSql + "的变量:" + tStr + "时出错。";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tSql.trim().equals(""))
        {
            return tLMUWSchema.getRemark();
        }
        return tSql;
    }

    private void parseUWResult()
    {
        for (int i = 1; i <= mAllLCErrSet.size(); i++)
        {
            LMUWSchema tLMUWSchema = new LMUWSchema();
            tLMUWSchema.setRemark(mAllLCErrSet.get(i).getUWError());
            mAllLCErrSet.get(i).setUWError(parseUWResult(tLMUWSchema));
        }
    }

    private LCUWErrorSet CheckCommonError(LCUWErrorSchema tLCUWErrorSchema,
            LCPolSchema tLCPolSchema, LMUWSet tLMUWSetUnpass)
    {
        //国际业务不需要自核
        if ("1".equals(mLCContSchema.getIntlFlag()))
        {
            return new LCUWErrorSet();
        }

        //取核保错误信息
        // 核保错误信息表
        LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
        tLCUWErrorDB.setPolNo(mOldPolNo);
        LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
        tLCUWErrorSet = tLCUWErrorDB.query();
        if (tLCUWErrorDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人错误信息表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }

        tLCUWErrorSchema.setSerialNo("0");
        if (m > 0)
        {
            tLCUWErrorSchema.setUWNo(m);
        }
        else
        {
            tLCUWErrorSchema.setUWNo(1);
        }
        tLCUWErrorSchema.setContNo(mContNo);
        tLCUWErrorSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        tLCUWErrorSchema.setProposalContNo(mPContNo);
        tLCUWErrorSchema.setPolNo(mOldPolNo);
        tLCUWErrorSchema.setProposalNo(tLCPolSchema.getProposalNo());
        tLCUWErrorSchema.setAppntNo(tLCPolSchema.getAppntNo());
        tLCUWErrorSchema.setAppntName(tLCPolSchema.getAppntName());
        tLCUWErrorSchema.setManageCom(tLCPolSchema.getManageCom());
        tLCUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCUWErrorSchema.setUWError(""); //核保出错信息
        tLCUWErrorSchema.setCurrValue(""); //当前值
        tLCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCUWErrorSchema.setUWPassFlag(mPolPassFlag);

        mLCUWErrorSet.clear();
        int merrcount = tLMUWSetUnpass.size();
        if (merrcount > 0)
        {
            for (int i = 1; i <= merrcount; i++)
            {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetUnpass.get(i);
                //生成流水号
                String tserialno = "" + i;

                tLCUWErrorSchema.setSerialNo(tserialno);
                tLCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                tLCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息，即核保规则的文字描述内容
                tLCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCUWErrorSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                tLCUWErrorSchema.setInsuredName(tLCPolSchema.getInsuredName());
                tLCUWErrorSchema.setCurrValue(""); //当前值
                tLCUWErrorSchema.setSugPassFlag(tLMUWSchema.getPassFlag()); //picch需求对自核规则分类（体检、契调）

                LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                mLCUWErrorSet.add(ttLCUWErrorSchema);
            }
        }

        /**
         * 处理校验投保人与被保人，处理投保人与被保人的基本信息
         * 身份证，姓名，性别，生日，年龄是否过大。然后将所有的
         * 信息填充到未通过自核的原因中去。
         */
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        tLCInsuredDB.setContNo(this.mContNo);
        tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());
        if (!tLCInsuredDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "DealData";
            tError.errorMessage = "查询被保险人信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLCInsuredSchema = tLCInsuredDB.getSchema();
        String[] insured = checkInsured(tLCInsuredSchema);
        System.out.println("在处理流水号时: " + merrcount);
        int Serial = merrcount + 1;
        for (int i = 0; i < insured.length; i++)
        {
            if (insured[i] != null)
            {
                String tserialno = String.valueOf(Serial++);
                tLCUWErrorSchema.setSerialNo(tserialno);
                tLCUWErrorSchema.setUWRuleCode("000000"); //核保规则编码
                tLCUWErrorSchema.setUWError(insured[i].trim()); //核保出错信息，即核保规则的文字描述内容
                tLCUWErrorSchema.setUWGrade("1");
                tLCUWErrorSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                tLCUWErrorSchema.setInsuredName(tLCInsuredSchema.getName());
                tLCUWErrorSchema.setCurrValue(""); //当前值
                tLCUWErrorSchema.setSugPassFlag("C"); //picch需求对自核规则分类（体检、契调）
                LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                mLCUWErrorSet.add(ttLCUWErrorSchema);
            }
            else
            {
                break;
            }
        }

        LCAppntDB tLCAppntDB = new LCAppntDB();

        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        tLCAppntDB.setContNo(this.mContNo);
        if (!tLCAppntDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "DealData";
            tError.errorMessage = "查询被保险人信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLCAppntSchema = tLCAppntDB.getSchema();
        if (AppntFlag == 0
                & tLCPolSchema.getInsuredNo().equals(tLCPolSchema.getAppntNo()))
        {
            String[] appnt = checkAppnt(tLCAppntSchema);
            AppntFlag++;
            for (int i = 0; i < appnt.length; i++)
            {
                if (appnt[i] != null)
                {
                    String tserialno = String.valueOf(Serial++);
                    tLCUWErrorSchema.setSerialNo(tserialno);
                    tLCUWErrorSchema.setUWRuleCode("000000"); //核保规则编码
                    tLCUWErrorSchema.setUWError(appnt[i].trim()); //核保出错信息，即核保规则的文字描述内容
                    tLCUWErrorSchema.setInsuredNo(tLCAppntSchema.getAppntNo());
                    tLCUWErrorSchema.setInsuredName(tLCAppntSchema
                            .getAppntName());
                    tLCUWErrorSchema.setUWGrade("1");
                    tLCUWErrorSchema.setCurrValue(""); //当前值
                    tLCUWErrorSchema.setSugPassFlag("C"); //picch需求对自核规则分类（体检、契调）
                    LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                    ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                    mLCUWErrorSet.add(ttLCUWErrorSchema);
                }
                else
                {
                    break;
                }
            }
        }
        tLCInsuredDB = new LCInsuredDB();
        LCInsuredSchema tcLCInsuredSchema = new LCInsuredSchema();
        tLCInsuredDB.setContNo(this.mContNo);
        tLCInsuredDB.setInsuredNo(tLCPolSchema.getInsuredNo());
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "DealData";
            tError.errorMessage = "查询被保险人信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        tcLCInsuredSchema = tLCInsuredSet.get(1);
        String[] relation = chkFamilyRelation(tcLCInsuredSchema, tLCAppntSchema);
        if (relation != null)
        {
            for (int i = 0; i < relation.length; i++)
            {
                if (relation[i] != null)
                {
                    String tserialno = String.valueOf(Serial++);
                    tLCUWErrorSchema.setSerialNo(tserialno);
                    tLCUWErrorSchema.setUWRuleCode("000000"); //核保规则编码
                    tLCUWErrorSchema.setUWError(relation[i].trim()); //核保出错信息，即核保规则的文字描述内容
                    tLCUWErrorSchema.setInsuredNo(tcLCInsuredSchema
                            .getInsuredNo());
                    tLCUWErrorSchema
                            .setInsuredName(tcLCInsuredSchema.getName());
                    tLCUWErrorSchema.setUWGrade("1");
                    tLCUWErrorSchema.setCurrValue(""); //当前值
                    tLCUWErrorSchema.setSugPassFlag("C"); //picch需求对自核规则分类（体检、契调）
                    LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                    ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                    mLCUWErrorSet.add(ttLCUWErrorSchema);
                }
            }
        }
        String[] BnfErr = checkBnf(tLCInsuredSet, tLCPolSchema);
        if (BnfErr != null)
        {
            for (int i = 0; i < BnfErr.length; i++)
            {
                if (BnfErr[i] != null && !BnfErr[i].equals(""))
                {
                    String tserialno = String.valueOf(Serial++);
                    tLCUWErrorSchema.setSerialNo(tserialno);
                    tLCUWErrorSchema.setUWRuleCode("000000"); //核保规则编码
                    tLCUWErrorSchema.setUWError(BnfErr[i].trim()); //核保出错信息，即核保规则的文字描述内容
                    tLCUWErrorSchema.setInsuredNo(tLCInsuredSet.get(1)
                            .getInsuredNo());
                    tLCUWErrorSchema.setInsuredName(tLCInsuredSet.get(1)
                            .getName());
                    tLCUWErrorSchema.setUWGrade("1");
                    tLCUWErrorSchema.setCurrValue(""); //当前值
                    tLCUWErrorSchema.setSugPassFlag("C"); //picch需求对自核规则分类（体检、契调）
                    LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                    ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                    mLCUWErrorSet.add(ttLCUWErrorSchema);
                }
            }
        }

        /**
         * zhangbin添加，判断保单累积风险是否属于
         */
        //        CalRiskAmntBL tCalRiskAmntBL = new CalRiskAmntBL();
        //        System.out.println("正在进行临分自核规则校验....................... ");
        //        if (tCalRiskAmntBL.isTempCess(tLCPolSchema))
        //        {
        //            ////将达到临分条件的险种保单的ReinsureFlag状态置1
        //            //tLCPolSchema.setReinsureFlag("1");
        //
        //            String tserialno = String.valueOf(Serial++);
        //            tLCUWErrorSchema.setSerialNo(tserialno);
        //            tLCUWErrorSchema.setUWRuleCode("000000"); //核保规则编码
        //            tLCUWErrorSchema.setUWError("被保险人" + tLCInsuredSchema.getName()
        //                    + "的" + tLCPolSchema.getRiskCode()
        //                    + "险种累计风险保额超过自动接受险额，达到临分条件"); //核保出错信息，即核保规则的文字描述内容
        //            tLCUWErrorSchema.setInsuredNo(tLCInsuredSet.get(1).getInsuredNo());
        //            tLCUWErrorSchema.setInsuredName(tLCInsuredSet.get(1).getName());
        //            tLCUWErrorSchema.setUWGrade("1");
        //            tLCUWErrorSchema.setCurrValue(""); //当前值
        //            tLCUWErrorSchema.setSugPassFlag("R");
        //            LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
        //            ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
        //            mLCUWErrorSet.add(ttLCUWErrorSchema);
        //        }
        return mLCUWErrorSet;
    }
    
    private void CheckLOPrtManager(LCContSchema tLCContSchema)
    {
        String strSQL = "select 1 from loprtmanager where prtseq like '" 
            + tLCContSchema.getPrtNo() + "%' and otherno = '" 
            + tLCContSchema.getProposalContNo() + "' and code = '07' ";
        ExeSQL tExeSQL = new ExeSQL();
        String strCount = tExeSQL.getOneValue(strSQL);
        if(strCount != null && !strCount.equals(""))
        {
            String deleteSQL = "delete from loprtmanager where prtseq like '" 
            + tLCContSchema.getPrtNo() + "%' and otherno = '" 
            + tLCContSchema.getProposalContNo() + "' and code = '07' ";
            System.out.println("DeleteSQL:" + deleteSQL);
            MMap tMMap = new MMap();
            tMMap.put(deleteSQL, "DELETE");
            
            VData tVData = new VData();
            tVData.add(tMMap);
            
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(tVData, ""))
            {
                System.out.println("CheckLOPrtManager中数据删除失败！");
            }
        }
    }
}
