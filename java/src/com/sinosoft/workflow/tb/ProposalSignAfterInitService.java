/*
 * @(#)ParseGuideIn.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.workflow.tb;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 合同签单处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author wujs
 * @version 6.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.tb.LCContSignBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;

public class ProposalSignAfterInitService implements AfterInitService
{
    /**存放结果*/
    private VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println(">>>>>>submitData");
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        //得到外部传入的数据,将数据备份到本类中
        if (this.getInputData() == false)
        {
            return false;
        }

        LCContSignBL tLCContSignBL = new LCContSignBL();
        boolean tSignResult = tLCContSignBL.submitData(mInputData, null);
        if (!tSignResult)
        {
            if (!tLCContSignBL.mErrors.needDealError())
            {
                CError.buildErr(this, "签单没有通过");
            }
            else
            {
                this.mErrors.copyAllErrors(tLCContSignBL.mErrors);
            }
            return false;
        }

        if (tLCContSignBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLCContSignBL.mErrors);
            return false;
        }
        this.mVResult = tLCContSignBL.mVResult;
        if (!prepareHealthManage())
        {
            return false;
        }
        return true;
    }

    /**
     * prepareHealthManage
     *
     * @return boolean
     */
    private boolean prepareHealthManage()
    {
        LCContSchema tLCContSchema = (LCContSchema) this.mVResult
                .getObjectByObjectName("LCContSchema", 0);
        if (tLCContSchema == null)
        {
            buildError("prepareHealthManage", "签单参数没有传入！");
            return false;
        }
        for (int i = 1; i <= tLCContSchema.getFieldCount(); i++)
        {
            this.mTransferData.setNameAndValue(tLCContSchema.getFieldName(i),
                    tLCContSchema.getV(i));
        }
        return true;
    }

    public ProposalSignAfterInitService()
    {

    }

    public VData getResult()
    {
        return this.mVResult;
    }

    public TransferData getReturnTransferData()
    {
        return this.mTransferData;
    }

    public CErrors getErrors()
    {
        return this.mErrors;
    }

    private boolean getInputData()
    {
        this.mTransferData = (TransferData) this.mInputData
                .getObjectByObjectName("TransferData", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ProposalSignAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
