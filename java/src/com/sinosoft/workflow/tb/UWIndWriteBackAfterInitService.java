package com.sinosoft.workflow.tb;

import com.sinosoft.lis.cbcheck.BindRiskDeal;
import com.sinosoft.lis.cbcheck.UWSendPrintUI;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class UWIndWriteBackAfterInitService implements AfterInitService
{

    /** 报错信息 */
    private CErrors mErrors = new CErrors();

    /** 参数信息 */
    private TransferData mTransferData;

    /** 结果集信息 */
    private VData mResult = new VData();

    /** 传入数据 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 合同号码 */
    private String mProposalContNo;

    /** 险种信息 */
    private LCPolSet mLCPolSet;

    /** 合同信息 */
    private LCContSchema mLCContSchema;

    /** 登陆信息 */
    private GlobalInput mGlobalInput;

    /** 递交信息 */
    private MMap map = new MMap();

    /** MissionID */
    private String mMissionID;

    /** UWFlag */
    private String mUWFlag;

    public UWIndWriteBackAfterInitService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        /** 传入数据 */
        if (!getInputData())
        {
            return false;
        }
        /** 校验数据 */
        if (!checkData())
        {
            return false;
        }
        /** 处理业务数据 */
        if (!dealData())
        {
            return false;
        }

        if (!perpareTranserfData())
        {
            return false;
        }

        return true;
    }

    /**
     * perpareTranserfData
     *
     * @return boolean
     */
    private boolean perpareTranserfData()
    {
        System.out.println("开始传播送参数");
        mTransferData.setNameAndValue("ContNo", this.mLCContSchema.getContNo());
        mTransferData.setNameAndValue("PrtNo", this.mLCContSchema.getPrtNo());
        mTransferData.setNameAndValue("AppntName", this.mLCContSchema
                .getAppntName());
        mTransferData.setNameAndValue("AppntNo", this.mLCContSchema
                .getAppntNo());
        mTransferData.setNameAndValue("ManageCom", this.mLCContSchema
                .getManageCom());
        mTransferData.setNameAndValue("CValiDate", this.mLCContSchema
                .getCValiDate());
        mTransferData.setNameAndValue("UWCode", this.mLCContSchema.getUWFlag());
        mTransferData.setNameAndValue("UWDate", this.mLCContSchema.getUWDate());
        this.mResult.add(this.map);
        return true;
    }

    /**
     * 两步操作：
     * 1、判断险种核保结论，如果险种全部撤销申请，整单撤销申请，如果存在变更承保则则整单变更
     * ，如果仅存在撤销申请、谢绝承保整单为撤销
     * 2、发送首期交费通知书
     *
     * @return boolean
     */
    private boolean dealData()
    {
    	/** 绑定产品的险种核保结论(变更承保处理) by zhangchengxuan 2012-6-21 */
        if (!dealBindRisk())
        {
            return false;
        }
        /** 首先处理核保结论 */
        if (!dealUWFlag())
        {
            return false;
        }
        /** 处理发送首期交费通知书 */
        if (!dealSendPrint())
        {
            return false;
        }
        return true;
    }

    /**
     * dealSendPrint
     *
     * @return boolean
     */
    private boolean dealSendPrint()
    {
        System.out.println("开始处理发送首期交费通知书问题");
        String tStrUWFlag = this.mLCContSchema.getUWFlag();
        if (this.mLCContSchema.getUWFlag().equals("4")
                || this.mLCContSchema.getUWDate().equals("9"))
        {
            if (!sendFirstPayPrint())
            {
                return false;
            }
        }
        // 如果撤销、延期、拒保的先收费的保单，如果已到帐，发退费通知书（首期缴费通知书）。
        else if ("1".equals(tStrUWFlag) || "8".equals(tStrUWFlag)
                || "a".equals(tStrUWFlag))
        {
            MMap tTmpMap = sendDeferPrint(mLCContSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        }
        // ------------------------------------------

        return true;
    }

    /**
     * dealUWFlag
     *
     * @return boolean
     */
    private boolean dealUWFlag()
    {
        String chk = "4";
        int decline = 0;
        int recall = 0;
        int delay = 0;
        for (int i = 1; i <= this.mLCPolSet.size(); i++)
        {
            /** 如果核保结论存在变更承保整单就为变更承保 */
            if (mLCPolSet.get(i).getUWFlag().equals("4")
                    || mLCPolSet.get(i).getUWFlag().equals("9"))
            {
                break;
            }
            /** 需要判断是否存在拒保 */
            if (mLCPolSet.get(i).getUWFlag().equals("1"))
            {
                decline++;
            }
            /** 需要判断是否存在撤销申请 */
            if (mLCPolSet.get(i).getUWFlag().equals("a"))
            {
                recall++;
            }
            /** 需要判断是否存在延期 */
            if (mLCPolSet.get(i).getUWFlag().equals("8"))
            {
                delay++;
            }
        }
        System.out.println("################################");
        System.out.println("保单存在:");
        System.out.println("拒保:" + decline);
        System.out.println("撤销:" + recall);
        System.out.println("延期:" + delay);
        System.out.println("################################");

        if (decline == mLCPolSet.size())
        {
            /** 整单核保结论 */
            chk = "1";
        }
        else if (delay == mLCPolSet.size())
        {
            /** 整单核保结论 */
            chk = "8";
        }
        else if ((recall + decline + delay) == mLCPolSet.size())
        {
            /** 整单核保结论 */
            chk = "a";
        }
        /** 如果整单撤销申请,删除全部工作流节点 */
        if (chk.equals("a") || "1".equals(chk) || "8".equals(chk))
        {
        	if(!isBankOnTheWay())
        	{
        		return false;
        	}
            if (!deleteMission())
            {
                return false;
            }
        }
        this.mLCContSchema.setUWFlag(chk);
        this.map.put(this.mLCContSchema, "UPDATE");
        return true;
    }
    
    /**
     * deleteMission
     *
     * @return boolean
     */
    private boolean deleteMission()
    {
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionID);
        LWMissionSet tLWMissionSet = tLWMissionDB.query();
        LBMissionSet tLBMissionSet = new LBMissionSet();
        Reflections rs = new Reflections();
        LBMissionSchema tLBMissionSchema = new LBMissionSchema();
        tLBMissionSet.add(tLBMissionSchema);
        rs.transFields(tLBMissionSet, tLWMissionSet);
        for (int i = 1; i <= tLBMissionSet.size(); i++)
        {
            String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
            tLBMissionSet.get(i).setSerialNo(tSerielNo);
            tLBMissionSet.get(i).setLastOperator(mGlobalInput.Operator);
            tLBMissionSet.get(i).setModifyDate(PubFun.getCurrentDate());
            tLBMissionSet.get(i).setModifyTime(PubFun.getCurrentTime());
        }
        map.put(tLBMissionSet, "INSERT");
        map.put(tLWMissionSet, "DELETE");
        return true;
    }

    
    /**
     * isBankOnTheWay
     * 
     * @return boolean
     */
    
    private boolean isBankOnTheWay()
    {
    	
    	//校验是否银行在途
    	String bankOnTheWay = new ExeSQL().getOneValue("select db2inst1.nvl(count(1), 0) from LJSPay where OtherNo = '" 
    			+ this.mLCContSchema.getPrtNo() + "' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1'");
    	if(!bankOnTheWay.equals("0.0"))
    	{
    		CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "保单银行在途，请待回盘之后进行相同操作！";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	MMap mmap = new MMap();
    	mmap.put("update ljspay set CanSendBank = '1' where otherno = '"
                 + this.mLCContSchema.getPrtNo() + "' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0')", "UPDATE");
    	VData tVData = new VData();
        tVData.add(mmap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, "UPDATE"))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
    	//删除非银行在途，且锁定的财务应收记录
        SSRS getNoticeNo = new ExeSQL().execSQL("select distinct a.tempfeeno from ljtempfee "
                                + " a left join ljspay b "
                                + " on a.tempfeeno = b.getnoticeno "
                                + " and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag = '0') "
                                + " and b.OtherNoType = '9' "
                                + " and b.CanSendBank = '1' "
                                + " where a.otherno='"
                                + mLCContSchema.getPrtNo()+"'"
                                + " and a.enteraccdate is null "
                                );
         for (int i = 1; i <= getNoticeNo.MaxRow; i++) {
             String getNoticeNoString = getNoticeNo.GetText(i, 1);
             System.out.println("******  " + getNoticeNoString + "  ******");
             map.put("delete from LJSPayB where GetNoticeNo = '"
                      + getNoticeNoString + "'", "DELETE");
             //备份到B表
             map.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
                      + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
                      + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
                      + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1',markettype,salechnl "
                      + "from LJSPay where GetNoticeNo = '"
                      + getNoticeNoString + "')","INSERT");
             //删除未到帐的财务暂收记录
             map.put("delete from LJSPay where GetNoticeNo = '"
                      + getNoticeNoString + "'", "DELETE");
             map.put("delete from ljtempfeeclass where tempfeeno = '"
                      + getNoticeNoString+"'"+" and EnterAccDate is null ", "DELETE");
             map.put("delete from ljtempfee where tempfeeno = '"
                      + getNoticeNoString + "'"+" and OtherNoType = '4' and EnterAccDate is null ", "DELETE");
         }//end modify
        return true;
    }
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {

        System.out.println("进入checkData");

        if (this.mTransferData == null)
        {
            buildError("checkData", "传入参数信息为null！");
            return false;
        }

        mProposalContNo = (String) mTransferData
                .getValueByName("ProposalContNo");

        if (this.mProposalContNo == null)
        {
            buildError("checkData", "传入合同号码为null！");
            return false;
        }

        this.mLCContSchema = getContInfo();

        if (mLCContSchema == null)
        {
            return false;
        }

        this.mLCPolSet = getPolInfo();

        if (mLCPolSet == null || mLCPolSet.size() <= 0)
        {
            buildError("checkData", "查询险种信息失败！");
            return false;
        }

        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            buildError("checkData", "没有传入MissionID！");
            return false;
        }

        /** 判断全部变更承保都必须录入回复意见后才能进行整单确认 */
        String sql = "select count(1) from LCUWMaster where proposalcontno = '"
                + mProposalContNo
                + "' and passflag='4' and customerreply is null";
        System.out.println("校验是否全部录入回复意见：" + sql);
        int chk = Integer.parseInt((new ExeSQL()).getOneValue(sql));
        if (chk > 0)
        {
            buildError("checkData", "未对所有核保结论为变更承保的险种进行意见录入！");
            return false;
        }
        return true;
    }

    /**
     * getPolInfo
     *
     * @return LCPolSet
     */
    private LCPolSet getPolInfo()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setProposalContNo(this.mProposalContNo);
        return tLCPolDB.query();
    }

    private LCContSchema getContInfo()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setProposalContNo(this.mProposalContNo);
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet == null || tLCContSet.size() <= 0
                || tLCContSet.size() > 1)
        {
            buildError("checkData", "查询合同信息失败，原因可能是没有查询出来或是查询结果不唯一！");
            return null;
        }
        return tLCContSet.get(1);
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {

        System.out.println("进入getInputData");

        if (this.mInputData == null)
        {
            buildError("getInputData", "传入数据为Null！");
            return false;
        }

        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 发送缴费通书。
     * @return 返回成功状态。
     */
    private boolean sendFirstPayPrint()
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setOtherNo(this.mProposalContNo);
        tLOPRTManagerDB.setOtherNoType("02");
        tLOPRTManagerDB.setCode("07");
        if (tLOPRTManagerDB.query().size() <= 0)
        {
            System.out.println("需要发送首期交费通知书");
            VData tempVData = new VData();
            tempVData.add(tLOPRTManagerDB.getSchema());
            tempVData.add(mGlobalInput);
            UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
            if (!tUWSendPrintUI.submitData(tempVData, "INSERT"))
            {
                this.mErrors.copyAllErrors(tUWSendPrintUI.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * 生成拒保通知书待打印数据。
     * @return
     */
    private MMap sendDeferPrint(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        LOPRTManagerSchema tPayPrintInfo = new LOPRTManagerSchema();

        // 获取结算单缴费凭证号
        String tPrtSeq = null;
        tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", cContInfo.getPrtNo());

        if (tPrtSeq == null || "".equals(tPrtSeq))
        {
            String tStrErr = "获取拒保通知书凭证号失败。";
            buildError("sendDeferPrint", tStrErr);
            return null;
        }
        tPayPrintInfo.setPrtSeq(tPrtSeq);
        // ----------------------------------

        tPayPrintInfo.setOtherNo(cContInfo.getContNo());
        tPayPrintInfo.setOtherNoType(PrintPDFManagerBL.ONT_CONT);

        tPayPrintInfo.setCode("05_T");

        tPayPrintInfo.setAgentCode(cContInfo.getAgentCode());
        tPayPrintInfo.setManageCom(cContInfo.getManageCom());

        tPayPrintInfo.setReqCom(mGlobalInput.ManageCom);
        tPayPrintInfo.setReqOperator(mGlobalInput.Operator);

        tPayPrintInfo.setPrtType(PrintPDFManagerBL.PT_FRONT);

        tPayPrintInfo.setStateFlag("0");

        tPayPrintInfo.setMakeDate(PubFun.getCurrentDate());
        tPayPrintInfo.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tPayPrintInfo, SysConst.INSERT);

        return tMMap;
    }
    
    /**
	 * 绑定产品的险种核保结论(变更承保处理)
	 * 
	 * @return 成功标志
	 */
    private boolean dealBindRisk(){
    	BindRiskDeal tBindRiskDeal = new BindRiskDeal();
    	if(!tBindRiskDeal.bindRiskDeal(this.mLCContSchema)){
    		this.mErrors.copyAllErrors(tBindRiskDeal.getErrors());
    		return false;
    	}
    	LCPolSet tLCPolSet = tBindRiskDeal.getLCPolSet();
    	LCUWMasterSet tLCUWMasterSet = tBindRiskDeal.getLCUWMasterSet();
    	if(tLCPolSet.size() > 0){
    		map.put(tLCPolSet, "UPDATE");
    		// 绑定产品变更承保处理，替换绑定产品LCPol中的核保结论 by 张成轩 2012-6-21
        	dealLCPolSet(tLCPolSet);
    	}
    	if(tLCUWMasterSet.size() > 0){
    		map.put(tLCUWMasterSet, "UPDATE");
    	}
    	return true;
    }

    /**
     * 绑定产品置换mLCPolSet中核保结论
     * 
     * @param tLCPolSet
     */
    private void dealLCPolSet(LCPolSet tLCPolSet) {
    	// 循环待更新LCPol集合
    	for(int row = 1; row <= tLCPolSet.size(); row++){
    		String tPolNo = tLCPolSet.get(row).getPolNo();
    		// 循环传入的LCPol集合
    		for(int tRow = 1; tRow <= mLCPolSet.size(); tRow++){
    			if(tPolNo.equals(mLCPolSet.get(tRow).getPolNo())){
    				// 替换LCPol中的UWFlag
    				mLCPolSet.get(tRow).setUWFlag(tLCPolSet.get(row).getUWFlag());
    			}
    		}
    	}
    }
    
    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWIndWriteBackAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
