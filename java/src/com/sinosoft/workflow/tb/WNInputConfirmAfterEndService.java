package com.sinosoft.workflow.tb;

import org.jdom.Document;
 
import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.AfterEndService;


/**
 * <p>Title: 工作流服务类:个人新契约录入完毕 </p>
 * <p>Description: 进行录入完毕后的校验 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class WNInputConfirmAfterEndService implements AfterEndService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();


    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();


    /** 业务处理类 */
    private LCContSchema mLCContSchema = new LCContSchema();
    private LCPolSet mLCPolSet = new LCPolSet();
    private LMCheckFieldSet mLMCheckFieldSet = new LMCheckFieldSet();
    private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
    private LWActivitySchema mLWActivitySchema=new LWActivitySchema();
    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;
    private String mMissionID;
    private String mActivityID=null;


    /** 业务数据字段 */
    private String mContNo;

    public WNInputConfirmAfterEndService()
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        
    	if (!getInputData(cInputData, cOperate))
            return false;

        //校验是否有未打印的体检通知书
        if (!checkData())
            return false;

        System.out.println("Start  dealData...");

        //进行业务处理
        if (!dealData())
            return false;

        System.out.println("dealData successful!");

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        System.out.println("Start  Submit...");

        return true;
    }


    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        map.put(mLCIssuePolSet,"INSERT");

        mResult.add(map);
        return true;
    }


    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "WNInputConfirmAfterEndService";
            tError.functionName = "checkData";
            tError.errorMessage = "合同保单信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "WNInputConfirmAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if (mOperater == null || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "WNInputConfirmAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得管理机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "WNInputConfirmAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "WNInputConfirmAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ContNo
        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (mContNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "WNInputConfirmAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据ContNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	ExeSQL tExeSQL=new ExeSQL();
        String tSQL="select code from ldcode where codetype='QYWNRuleButton' with ur";
    	String tRuleButton=tExeSQL.getOneValue(tSQL);
    	tSQL="select comcode from ldcode where codetype='CardFlagRuleButton' and code='" +
    	this.mLCContSchema.getCardFlag()+"' with ur";
    	String tCardFlagButton=tExeSQL.getOneValue(tSQL);
    	if(tRuleButton!=null && tRuleButton.equals("00") 
        		&& (tCardFlagButton==null||!tCardFlagButton.equals("11"))){
    		try{
    			
    			// modify by zxs
				NewEngineRuleService newEngineRule = new NewEngineRuleService();
				MMap map = newEngineRule.dealNUData(mLCContSchema, "NewQYWNRuleButton");
				VData mData = new VData();
				// modify by zxs
				
//        	调用规则引擎	
    		RuleTransfer ruleTransfer = new RuleTransfer();
    		//获得规则引擎返回的校验信息 调用投保规则
			String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NU", this.mLCContSchema.getContNo(), false);
			//将校验信息转换为Xml的document对象
			Document doc = ruleTransfer.stringToDoc(xmlStr);
			String approved = ruleTransfer.getApproved(doc);
			LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的投保规则
			String errorMessage="<br>";
			
			if(approved==null){
				CError tError = new CError();
	            tError.moduleName = "UWAutoChkBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "执行规则引擎时出错";
	            this.mErrors.addOneError(tError);
	            return false;
			}
			if("1".equals(approved)){
    		}else if("0".equals(approved) || "2".equals(approved)){
    		}else if("-1".equals(approved)){
    			CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "规则引擎执行异常";
                this.mErrors.addOneError(tError);
                return false;
    		}else {
    			CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "规则引擎返回了未知的错误类型";
                this.mErrors.addOneError(tError);
                return false;
   		    }
			String tempRiskcode="";
			String tempInsured="";
			if(tLMUWSetPolAllUnpass.size()>0){
				for(int m=1;m<=tLMUWSetPolAllUnpass.size();m++){
					// modify by zxs
					LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
					EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
					engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
					engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
					engineRuleSchema.setApproved(approved);
					engineRuleSchema.setRuleResource("old");
					engineRuleSchema.setRuleType("NU");
					engineRuleSchema.setContType("万能保单");
					engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
					engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
					engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
					if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
						engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
					}else{
						engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
					}
					engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
					map.put(engineRuleSchema, "INSERT");
					// modify by zxs
					tempRiskcode="";
					tempInsured="";
					if(tLMUWSetPolAllUnpass.get(m).getRiskName()!=null 
							&& !tLMUWSetPolAllUnpass.get(m).getRiskName().equals("")){
						tempInsured+="被保人"+tLMUWSetPolAllUnpass.get(m).getRiskName();
						if(tLMUWSetPolAllUnpass.get(m).getRiskCode()!=null
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("")){
							tempRiskcode+="，险种代码"+tLMUWSetPolAllUnpass.get(m).getRiskCode()+"，";
						}else{
							tempInsured+="，";
						}
					}else{
						if(tLMUWSetPolAllUnpass.get(m).getRiskCode()!=null
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
								&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("")){
							tempRiskcode+="险种代码"+tLMUWSetPolAllUnpass.get(m).getRiskCode()+"，";
						}
					}
					errorMessage+=tLMUWSetPolAllUnpass.get(m).getUWCode();
					errorMessage+="：";
					errorMessage+=tempInsured;
					errorMessage+=tempRiskcode;
					errorMessage+=tLMUWSetPolAllUnpass.get(m).getRemark();
					errorMessage+="<br>";
				}
				// modify by zxs
				mData.add(map);
				PubSubmit pubSubmit = new PubSubmit();
				if (!pubSubmit.submitData(mData, "")) {
					this.mErrors.addOneError(pubSubmit.mErrors.getContent());
					return false;
				}
				// modify by zxs
				CError tError = new CError();
                tError.moduleName = "WNInputConfirmAfterEndService";
                tError.functionName = "getInputData";
                tError.errorMessage = errorMessage;
                this.mErrors.addOneError(tError);
                return false;
			}else{
				//// modify by zxs
				EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
				engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
				engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
				engineRuleSchema.setApproved(approved);
				engineRuleSchema.setRuleResource("old");
				engineRuleSchema.setRuleType("NU");
				engineRuleSchema.setContType("万能保单");
				engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
				engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
				map.put(engineRuleSchema, "INSERT");
			}
			// modify by zxs
			mData.add(map);
			PubSubmit pubSubmit = new PubSubmit();
			if (!pubSubmit.submitData(mData, "")) {
				this.mErrors.addOneError(pubSubmit.mErrors.getContent());
				return false;
			}
			// modify by zxs
    		}catch(Exception ex){
    			CError tError = new CError();
	            tError.moduleName = "UWAutoChkBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "执行规则引擎时出错";
	            this.mErrors.addOneError(tError);
	            return false;
    		}
        }
        return true;
    }


    /**
     * preparePolIssue
     *
     * @param lCPolSchema LCPolSchema
     * @param lMCheckFieldSchema LMCheckFieldSchema
     * @return boolean
     */
    private boolean preparePolIssue(LCPolSchema tlCPolSchema,
                                    LMCheckFieldSchema tlMCheckFieldSchema)
    {
        LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();

        tLCIssuePolSchema.setGrpContNo(tlCPolSchema.getGrpContNo());
        tLCIssuePolSchema.setContNo(tlCPolSchema.getContNo());
        tLCIssuePolSchema.setProposalContNo(tlCPolSchema.getContNo());
//        tLCIssuePolSchema.setPrtSeq("");
        tLCIssuePolSchema.setSerialNo(PubFun1.CreateMaxNo("QustSerlNo", 20));
        tLCIssuePolSchema.setFieldName(tlMCheckFieldSchema.getFieldName());
        tLCIssuePolSchema.setLocation(tlMCheckFieldSchema.getLocation());
        tLCIssuePolSchema.setIssueType("9999999999");             //暂时存为十个“9”
        tLCIssuePolSchema.setOperatePos("0");
        tLCIssuePolSchema.setBackObjType("5");
//        tLCIssuePolSchema.setBackObj("");
        tLCIssuePolSchema.setIsueManageCom(mManageCom);
        tLCIssuePolSchema.setIssueCont(tlMCheckFieldSchema.getMsg());
        tLCIssuePolSchema.setPrintCount(0);
//        tLCIssuePolSchema.setNeedPrint("");
//        tLCIssuePolSchema.setReplyMan("");
//        tLCIssuePolSchema.setReplyResult("");
        tLCIssuePolSchema.setState("0");
        tLCIssuePolSchema.setOperator(tlCPolSchema.getOperator());
        tLCIssuePolSchema.setManageCom(tlCPolSchema.getManageCom());
        tLCIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCIssuePolSchema.setModifyTime(PubFun.getCurrentTime());

        mLCIssuePolSet.add(tLCIssuePolSchema);
        return true;
    }


    /**
     * prepareIssue
     *
     * @param lMCheckFieldSchema LMCheckFieldSchema
     * @return boolean
     */
    private boolean prepareContIssue(LMCheckFieldSchema tlMCheckFieldSchema)
    {
        LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();

        tLCIssuePolSchema.setGrpContNo(mLCContSchema.getContNo());
        tLCIssuePolSchema.setContNo(mLCContSchema.getContNo());
        tLCIssuePolSchema.setProposalContNo(mLCContSchema.getContNo());
//        tLCIssuePolSchema.setPrtSeq("");
        tLCIssuePolSchema.setSerialNo(PubFun1.CreateMaxNo("QustSerlNo", 20));
        tLCIssuePolSchema.setFieldName(tlMCheckFieldSchema.getFieldName());
        tLCIssuePolSchema.setLocation(tlMCheckFieldSchema.getLocation());
        tLCIssuePolSchema.setIssueType("9999999999");             //暂时存为十个“9”
        tLCIssuePolSchema.setOperatePos("0");
        tLCIssuePolSchema.setBackObjType("5");
//        tLCIssuePolSchema.setBackObj("");
        tLCIssuePolSchema.setIsueManageCom(mManageCom);
        tLCIssuePolSchema.setIssueCont(tlMCheckFieldSchema.getMsg());
        tLCIssuePolSchema.setPrintCount(0);
//        tLCIssuePolSchema.setNeedPrint("");
//        tLCIssuePolSchema.setReplyMan("");
//        tLCIssuePolSchema.setReplyResult("");
        tLCIssuePolSchema.setState("0");
        tLCIssuePolSchema.setOperator(mOperater);
        tLCIssuePolSchema.setManageCom(mManageCom);
        tLCIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCIssuePolSchema.setModifyTime(PubFun.getCurrentTime());

        mLCIssuePolSet.add(tLCIssuePolSchema);

        return true;
    }


    /**
     * CheckCont
     *
     * @param tCalCode String
     * @return String
     */
    private String CheckCont(String tCalCode)
    {
        String tCheckResult = "";
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(tCalCode);

        mCalculator.addBasicFactor("ContNo", mLCContSchema.getContNo());

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals(""))
            tCheckResult = "";
        else
            tCheckResult = tStr.trim();

        return tCheckResult;
    }


    /**
     * CheckPol
     *
     * @param lCPolSchema LCPolSchema
     * @param tCalCode String
     * @return String
     */
    private String CheckPol(LCPolSchema tlCPolSchema, String tCalCode)
    {
        String tCheckResult = "";
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(tCalCode);

        mCalculator.addBasicFactor("PolNo", tlCPolSchema.getPolNo());

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals(""))
            tCheckResult = "";
        else
            tCheckResult = tStr.trim();

        return tCheckResult;
    }


    /**
     * getCheckFieldSet
     *
     * @return LMCheckFieldSet
     */
    private LMCheckFieldSet getCheckFieldSetCont()
    {
        LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
        LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
        String tSql = "select * from lmcheckfield where 1=1 "
                      + " and riskcode = '000000'"
                      + " and pagelocation = 'TBINPUT#TBTYPEIC' "    //个单合同自动复核规则
                      ;

        tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
        if(tLMCheckFieldSet==null||tLMCheckFieldSet.size()<=0)
        {
           CError tError = new CError();
           tError.moduleName = "WNInputConfirmAfterEndService";
           tError.functionName = "getCheckFieldSet";
           tError.errorMessage = "查询算法描述表失败!";
           this.mErrors.addOneError(tError);
           return null;
        }
        return tLMCheckFieldSet;
    }


    /**
     * getCheckFieldSet
     *
     * @return LMCheckFieldSet
     */
    private LMCheckFieldSet getCheckFieldSetPol(LCPolSchema tLCPolSchema)
    {
        LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
        LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
        String tSql = "select * from lmcheckfield where 1=1 "
                      + " and (riskcode = '000000' or riskcode = '" + tLCPolSchema.getRiskCode() +"'"
                      + " and pagelocation = 'TBINPUT#TBTYPEI' "    //个单合同自动复核规则
                      ;

        tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
        if(tLMCheckFieldSet==null||tLMCheckFieldSet.size()<=0)
        {
           CError tError = new CError();
           tError.moduleName = "WNInputConfirmAfterEndService";
           tError.functionName = "getCheckFieldSet";
           tError.errorMessage = "查询算法描述表失败!";
           this.mErrors.addOneError(tError);
           return null;
        }
        return tLMCheckFieldSet;
    }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return
     */
    private boolean prepareTransferData()
    {

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
