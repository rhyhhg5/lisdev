package com.sinosoft.workflow.tb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class ContReturnUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 数据操作字符串 */
    private String mOperate;

    public ContReturnUI() {}

   
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        ContReturnBL tContReturnBL = new ContReturnBL();

        System.out.println("---TbWorkFlowBL UI BEGIN---");
        if (tContReturnBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tContReturnBL.mErrors);
            return false;
        }

        return true;
    }

}
