package com.sinosoft.workflow.tb;

import com.sinosoft.lis.bl.LCAppntBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.Vector;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.cbcheck.RecalculationPremBL;

public class FJCheckEngineBL {
	/** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局数据 */
    private Reflections ref = new Reflections();

    private GlobalInput mGlobalInput = new GlobalInput();

    private MMap map = new MMap();

    private boolean changecvalidateflag = false; //判断是否修改生效日期标记

    private boolean TakeBackCertifyFalg = false; //是否回收单证标记
    /** 业务处理相关变量 */
    private LCContSchema mLCContSchema = new LCContSchema();

    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

    private LCPolSchema mmLCPolSchema = new LCPolSchema();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    double SumPrem = 0; //合计保费

    /** 处理投保人换号被保人同时换号问题 */
    private MMap insured_map;

    private MMap mMap = new MMap();

    /** 解决 LCPol 表被二次覆盖的问题，备份要更新信息 */
    private String mStrLcpolup = null;

    private String mStrLcpolPaymodeup = null;

    // 校验是否百万护乘、百万护驾险种 1是老险种 2是百万护乘、百万护驾新险种 默认0不为上述两个险种
	private String newpolflag = "0";

    ExeSQL tExeSQL = new ExeSQL();
    
    public FJCheckEngineBL(){
    	
    }
    
    /**
     * 数据提交的公共方法
     *
     * @param: cInputData 传入的数据 cOperate 数据操作字符串
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate){
    	// 将传入的数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        System.out.println("now in FJCheckEngineBL submit");
        // 将外部传入的数据分解到本类的属性中，准备处理
        System.out.println("---getInputData---");
        if (this.getInputData() == false)
        {
            return false;
        }
        System.out.println("---checkData开始---");
        //校验
        if (this.checkData() == false)
        {
            return false;
        }
    	
    	return true;
    }
    
    
    /**
     * 将外部传入的数据分解到本类的属性中
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            //合同表
            mLCContSchema.setSchema((LCContSchema) mInputData
                    .getObjectByObjectName("LCContSchema", 0));
            
            return true;
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "checkData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;

        }

    }

    /**
     * 校验传入的数据
     *
     * @return boolean
     */
    private boolean checkData(){
	    String riskFlag = CheckRiskCode();
		if ("0".equals(riskFlag)) {
			return true;
		} else if ("1".equals(riskFlag)) {
			// 百万安行校验
			SSRS tSSRS = new SSRS();
			String polnoSql = "select a.polno from lcpol a left join lccont lcc on a.contno = lcc.contno where lcc.contno='"
					+ mLCContSchema.getContNo() + "' and riskcode in ('333501','333502') ";
			tSSRS = tExeSQL.execSQL(polnoSql);
			String polno = tSSRS.GetText(1, 1);
			// 校验险种333502与333501累计保额
			if (this.checkTotalPrem(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "百万安行累计保额不得超过10万元";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (this.checkOccupation(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "离退休人员、农业人员、流动摊贩、家庭主妇（夫）、学生只能投保5万";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (this.checkAnnualIncome(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "年收入未达到投保要求";
				this.mErrors.addOneError(tError);
				return false;
			}
			// 百万护乘、百万护驾投保校验 ************这个位置能不能购买两个百万系列主险险种
		} else {
			SSRS tSSRS = new SSRS();
			String polnoSql = "select a.polno,a.riskcode from lcpol a left join lccont lcc on a.contno = lcc.contno where lcc.contno='"
					+ mLCContSchema.getContNo() + "' and riskcode in ('336001','336101') ";
			tSSRS = tExeSQL.execSQL(polnoSql);
			String polno = tSSRS.GetText(1, 1);
			String riskcode = tSSRS.GetText(1, 2);
			// 百万驾乘产品累计保额不得超过20万元 累计保额包括336101、336001、333502与333501险种
			if (this.checkTotalPremForNewRisk(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "百万驾乘产品累计保额不得超过20万元";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (this.checkOccupationForNewRisk(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "离退休人员、农业人员、流动摊贩、家庭主妇（夫）、学生最高只能投保10万";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (this.checkAnnualIncomeForNewRisk(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "年收入未达到投保要求";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (this.checkHigherAnnualIncomeForNewRisk(polno) == false) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "FJCheckEngineBL";
				tError.functionName = "checkData";
				tError.errorMessage = "年收入未达到投保要求";
				this.mErrors.addOneError(tError);
				return false;
			}
			// 如果投保险种是百万护乘 加入被保人年龄/保额限制
			if ("336101".equals(riskcode)) {
				if (this.checkAgeLimitForNewRisk(polno) == false) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "FJCheckEngineBL";
					tError.functionName = "checkData";
					tError.errorMessage = "被保险人年龄大于60周岁，百万驾乘类产品累计保额不能超过10万";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
		}
		return true;
    }
    
    /**
	 * 校验百万安行累计保额不得超过10万元, 累计保额包括333502与333501 现阶段累计保额校验加入新险种336001 和 336101
	 * 
	 * @return boolean
	 */
	private boolean checkTotalPrem(String polno) {
		String premSql = "select count(1) from LCPol a, lcinsured b where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno "
				+ " and (select sum(amnt) from lcpol c,  (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) > 100000";
		String num = tExeSQL.execSQL(premSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}
    
    /**
	 * 校验离退休人员、农业人员、流动摊贩、家庭主妇（夫）、学生只能投保5万, 累计保额包括333502与333501 现阶段累计保额校验加入新险种336001
	 * 和 336101
	 * 
	 * @return boolean
	 */
	private boolean checkOccupation(String polno) {
		String occupationSql = "select count(1) from LCPol a, lcinsured b " + " where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno "
				+ " and b.occupationcode in ('00101','00102','00103','00104','00105','00106','05803','06903','06904','04804') "
				+ " and (select sum(amnt) from lcpol c, (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) >= 100000";
		String num = tExeSQL.execSQL(occupationSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}
    
    /**
	 * 校验年收入＜5万，累计保额等于10万, 累计保额包括333502与333501 现阶段累计保额校验加入新险种336001 和 336101
	 *
	 * @return boolean
	 */
	private boolean checkAnnualIncome(String polno) {
		String incomeSql = "select count(1) from LCPol a, lcinsured b " + " where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno " + " and b.Salary < 5 "
				+ " and (select sum(amnt) from lcpol c, (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) = 100000";
		String num = tExeSQL.execSQL(incomeSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}
    
    /**
     * 校验简易平台累计保额是否大于5万,累计保额包括333502与333501
     *
     * @return boolean
     */
    public boolean jyTotalPrem(LCContSchema tLCContSchema){
    	if(this.jyCheckRiskCode(tLCContSchema)==true){
    		return true;
    	}
    	SSRS tSSRS = new SSRS();
    	String polnoSql = "select a.polno from lcpol a left join lccont lcc on a.contno = lcc.contno where lcc.contno='"+tLCContSchema.getContNo()+"' ";
    	tSSRS=tExeSQL.execSQL(polnoSql);
    	String polno = tSSRS.GetText(1, 1);
    	String premSql = "select count(1) from LCPol a, lcinsured b where polno = '"+polno+"' "
    			+ " and a.contno = b.contno "
    			+ " and a.insuredno = b.insuredno "
    			+ " and (select sum(amnt) from lcpol c,  (select insuredno from lcpol where polno = '"+polno+"') as I "
    			+ " where c.insuredno = I.insuredno "
    			+ " and c.riskcode in ('333501', '333502') "
    			+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) > 50000";
    	String num=tExeSQL.execSQL(premSql).GetText(1, 1);
    	if(!"0".equals(num)){
    		// @@错误处理
            CError tError = new CError();
            tError.moduleName = "FJCheckEngineBL";
            tError.functionName = "checkData";
            tError.errorMessage = "百万安行简易出单累计保额不得超过5万元";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	return true;
    }

    /**
	 * 校验百万驾乘产品累计保额不得超过20万元, 累计保额包括336101、336001、333502与333501
	 *
	 * @return boolean
	 */
	private boolean checkTotalPremForNewRisk(String polno) {
		String premSql = "select count(1) from LCPol a, lcinsured b where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno "
				+ " and (select sum(amnt) from lcpol c,  (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) > 200000";
		String num = tExeSQL.execSQL(premSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}

	/**
	 * 校验职业为离退休人员、农业人员、流动摊贩、家庭主妇（夫）、学生时投保保额最多为10万, 累计保额包括336101、336001、333502与333501
	 * 职业代码：06903，06904，00105，05803，04804
	 * 
	 * @return boolean
	 */
	private boolean checkOccupationForNewRisk(String polno) {
		String occupationSql = "select count(1) from LCPol a, lcinsured b " + " where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno "
				+ " and b.occupationcode in ('06903','06904','00105','05803','04804') "
				+ " and (select sum(amnt) from lcpol c, (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) > 100000";
		String num = tExeSQL.execSQL(occupationSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}

	/**
	 * 检验年收入＜10万，累计保额等于10万, 累计保额包括336101、336001、333502与333501 职业代码为05803代码不进行此规则校验。
	 *
	 * @return boolean
	 */
	private boolean checkAnnualIncomeForNewRisk(String polno) {
		String incomeSql = "select count(1) from LCPol a, lcinsured b " + " where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno " + " and b.Salary < 10 "
				+ " and (select sum(amnt) from lcpol c, (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno and b.occupationcode != '05803' "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) >= 100000";
		String num = tExeSQL.execSQL(incomeSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}

	/**
	 * 检验年收入＜15万，累计保额等于20万 累计保额包括336101、336001、333502与333501
	 *
	 * @return boolean
	 */
	private boolean checkHigherAnnualIncomeForNewRisk(String polno) {
		String incomeSql = "select count(1) from LCPol a, lcinsured b " + " where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno " + " and b.Salary < 15 "
				+ " and (select sum(amnt) from lcpol c, (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) >= 200000";
		String num = tExeSQL.execSQL(incomeSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}

	/**
	 * 检验被保险人年龄大于60周岁，百万驾乘类产品累计保额不能超过10万 累计保额包括336101、336001、333502与333501
	 *
	 * @return boolean
	 */
	private boolean checkAgeLimitForNewRisk(String polno) {
		String incomeSql = "select count(1) from LCPol a, lcinsured b " + " where polno = '" + polno + "' "
				+ " and a.contno = b.contno " + " and a.insuredno = b.insuredno " + " and a.InsuredAppAge > 60 "
				+ " and (select sum(amnt) from lcpol c, (select insuredno from lcpol where polno = '" + polno
				+ "') as I " + " where c.insuredno = I.insuredno "
				+ " and c.riskcode in ('333501', '333502','336001','336101') "
				+ " and (((stateflag = '0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag = '1' and stateflag = '1'))) > 100000";
		String num = tExeSQL.execSQL(incomeSql).GetText(1, 1);
		if (!"0".equals(num)) {
			return false;
		}
		return true;
	}
    
    public String CheckRiskCode(){
    	if (mLCContSchema.getPrtNo() != null && !"".equals(mLCContSchema.getPrtNo())) {
			// 校验是否为百万安行险种333501、新百万安行险种333502
			String checkOldRiskSql = "select 1 from lcpol where prtno='" + mLCContSchema.getPrtNo()
					+ "' and riskcode in('333501','333502')";
			SSRS oldRiskSSRS = new ExeSQL().execSQL(checkOldRiskSql);
			if (oldRiskSSRS != null && oldRiskSSRS.getMaxRow() > 0) {
				newpolflag = "1";
			} else {
				newpolflag = "0";
			}
			// 校验是否为人保健康百万护驾护理险种336001、人保健康百万护乘护理险种336101
			String checkNewRiskSql = "select 1 from lcpol where prtno='" + mLCContSchema.getPrtNo()
					+ "' and riskcode in('336001','336101')";
			SSRS newRiskSSRS = new ExeSQL().execSQL(checkNewRiskSql);
			if (newRiskSSRS != null && newRiskSSRS.getMaxRow() > 0) {
				newpolflag = "2";
			} else {
				newpolflag = "0";
			}
		} else {
			newpolflag = "0";
		}
		return newpolflag;
    }
    
    public boolean jyCheckRiskCode(LCContSchema tLCContSchema){
    	String Sql="select 1 from lcpol where prtno='"+tLCContSchema.getPrtNo()+"' and riskcode in('333501','333502')";
    	SSRS tSSRS=new ExeSQL().execSQL(Sql);
    	if(tSSRS != null && tSSRS.getMaxRow()>0){
    		return false;
    	}
    	return true;
    }
}