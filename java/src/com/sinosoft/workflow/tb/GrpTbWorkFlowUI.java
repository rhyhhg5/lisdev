package com.sinosoft.workflow.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCUWSendTraceSchema;
import com.sinosoft.lis.schema.LCGCUWMasterSchema;
import com.sinosoft.lis.cbcheck.UWSendTraceAllUI;
import java.util.Vector;

/**
 * <p>Title: 团体工作流 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: SinoSoft </p>
 * @author HYQ
 * @version 1.0
 */

public class GrpTbWorkFlowUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public GrpTbWorkFlowUI() {}

    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();
        String Activity = "";

        /** 全局变量 */
        mGlobalInput.Operator = "picch";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";
//        mTransferData.setNameAndValue("PrtNo", "00000020054");
//        mTransferData.setNameAndValue("ManageCom", "86");
//        mTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
//        mTransferData.setNameAndValue("Operator", mGlobalInput.Operator);

        //创建起始节点

//        mTransferData.setNameAndValue("GrpContNo", "140110000000201");
//        mTransferData.setNameAndValue("MissionID", "00000000000000000628");
//        mTransferData.setNameAndValue("SubMissionID", "1");

        //准备团单Sign核保节点
//    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
////
//    tLCGrpContSchema.setGrpContNo("1400000295");
//    tLCGrpContSchema.setAppFlag("9");
//    mTransferData.setNameAndValue("MissionID", "00000000000000001728");
//    mTransferData.setNameAndValue("SubMissionID", "1");

        /**
         * 团体签单 0000002006
         */
//        Activity = "0000002006";
//        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
//        tLCGrpContSchema.setGrpContNo("1400000504");
//        mTransferData.setNameAndValue("MissionID", "00000000000000001631");
//        mTransferData.setNameAndValue("SubMissionID", "1");
//        tVData.add(tLCGrpContSchema);
        /**
         * 团体人工核保
         */
//        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
//        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
//        tLCGrpContSchema.setGrpContNo("1400000013");
//        tLCGrpContSchema.setUWFlag("9");
//        tLCGrpContSchema.setRemark("234234");
//        tLCGrpContSet.add(tLCGrpContSchema);
//        LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
//        tLCUWSendTraceSchema.setOtherNoType("2"); //团险上报
//        tLCUWSendTraceSchema.setOtherNo("1400000221");
//        tLCUWSendTraceSchema.setUWFlag("9");
//        tLCUWSendTraceSchema.setUWIdea("sdhfkshf");
//        tLCUWSendTraceSchema.setYesOrNo("Y");
//
//        LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
//        tLCGCUWMasterSchema.setGrpContNo("1400000221");
//        tLCGCUWMasterSchema.setPassFlag("9");
//        tLCGCUWMasterSchema.setUWIdea("234242342");
//
//        mTransferData.setNameAndValue("LCUWSendTraceSchema",
//                                      tLCUWSendTraceSchema);
//        mTransferData.setNameAndValue("LCGCUWMasterSchema", tLCGCUWMasterSchema);
//
//        tVData.add(mGlobalInput);
//        tVData.add(mTransferData);
//        tVData.add(tLCGrpContSet);
//        UWSendTraceAllUI tUWSendTraceAllUI = new UWSendTraceAllUI();
//        if (!tUWSendTraceAllUI.submitData(tVData, "submit")) {
//            int n = tUWSendTraceAllUI.mErrors.getErrorCount();
//        }
//        TransferData tTransferData = new TransferData();
//        tTransferData = (TransferData) tUWSendTraceAllUI.getResult().
//                        getObjectByObjectName("TransferData", 0);
//        String tUWSendFlag = (String) tTransferData.getValueByName("SendFlag");
//        String tUserCode = (String) tTransferData.getValueByName("UserCode");
//        TransferData nTransferData = new TransferData();
//        Activity = "0000002004";
//        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
//        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
//        tLCGrpContSchema.setGrpContNo("1400000246");
//        tLCGrpContSchema.setUWFlag("9");
//        tLCGrpContSchema.setRemark("234234");
//        tLCGrpContSet.add(tLCGrpContSchema);
//        mTransferData.setNameAndValue("MissionID","00000000000000001637");
//        mTransferData.setNameAndValue("SubMissionID",
//                                    "1");
//        tVData.add(tLCGrpContSet);
        /**
         * 团体新单复核0000002001
         */
        Activity = "0000002001";
        mTransferData.setNameAndValue("GrpContNo", "1400000971");
        mTransferData.setNameAndValue("PrtNo", "18000000078");
        mTransferData.setNameAndValue("SaleChnl", "01");
        mTransferData.setNameAndValue("ManageCom", "86110000");
        mTransferData.setNameAndValue("GrpName", "清华大学");
        mTransferData.setNameAndValue("CValiDate", "2006-01-01");
        mTransferData.setNameAndValue("MissionID", "00000000000000003104");
        mTransferData.setNameAndValue("SubMissionID",
                                      "2");

        /**总变量*/

        tVData.add(mGlobalInput);

//    tVData.add( tLCGrpContSet );

        tVData.add(mTransferData);
//        tVData.add(mTransferData);
        tVData.add(mGlobalInput);
        GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
        try {
            if (tGrpTbWorkFlowUI.submitData(tVData, Activity)) {
                VData tResult = new VData();

                //tResult = tActivityOperator.getResult() ;
            } else {
                System.out.println(tGrpTbWorkFlowUI.mErrors.getError(0).
                                   errorMessage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        GrpTbWorkFlowBL tGrpTbWorkFlowBL = new GrpTbWorkFlowBL();

        System.out.println("---GrpTbWorkFlowBL UI BEGIN---");
        if (tGrpTbWorkFlowBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpTbWorkFlowBL.mErrors);
            mResult.clear();
            return false;
        }
        return true;
    }
}
