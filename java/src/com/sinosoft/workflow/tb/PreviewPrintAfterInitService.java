package com.sinosoft.workflow.tb;

import java.util.Date;
import java.util.Vector;

import com.sinosoft.lis.cbcheck.UWSendPrintUI;
import com.sinosoft.lis.cbcheck.UWSendPrintUIS;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LCSpecDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPolSpecRelaSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.PolicyAppAccPayBL;
import com.sinosoft.lis.tb.PreviewRiskSignBL;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPolSpecRelaSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCSpecSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;

import java.util.GregorianCalendar;

import com.sinosoft.lis.schema.LJTempFeeSchema;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class PreviewPrintAfterInitService implements AfterInitService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 记录传递参数 */
    private TransferData mTransferData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 传入对象 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 合同号 */
    private String mContNo;

    private int tAppntnum;

    /** 合同信息 */
    private LCContSchema mLCContSchema;

    /** 险种信息 */
    private LCPolSet mLCPolSet;

    /** 合同号码 */
    private String newContNo;

    private GlobalInput mGlobalInput;

    /** ＭＭａｐ */
    private MMap map = new MMap();

    private MMap map1 = new MMap();

    private Date mPayToDate;

    private Date maxPayDate;

    private LCPolSpecRelaSet mLCPolSpecRelaSet = new LCPolSpecRelaSet();

    public PreviewPrintAfterInitService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        System.out.println("@@完成程序submitData第41行");

        if (!getInputData())
        {
            return false;
        }
        System.out.println("????????????????????????????");
        System.out.println("mContNo=" + mContNo);
        System.out.println("????????????????????????????");

        if (checkcardflag(mContNo))
        {
            LCContDB tTmpContInfo = new LCContDB();
            tTmpContInfo.setContNo(mContNo);
            if (!tTmpContInfo.getInfo())
            {
                buildError("submitData", "未找到[" + mContNo + "]该合同号的保单信息");
                return false;
            }

            String tPayMethod = tTmpContInfo.getPayMethod();
            if ("01".equals(tPayMethod))
            {
                // 处理投保人帐户缴费模式
                MMap tMMap = dealAppAccount(tTmpContInfo);
                if (tMMap == null)
                {
                    return false;
                }
                this.mResult.add(tMMap);
                tMMap = null;
                // --------------------
            }
            else
            {
                if (!prepareBankData(mContNo))
                {
                    return false;
                }
            }
            
            

            return true;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        this.mResult.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        /** 进行签单操作 */
        if (!signCont())
        {
            return false;
        }
        return true;
    }

    /**
     * signCont
     *
     * @return boolean
     */
    private boolean signCont()
    {
        System.out.println("首先判断是否签单,如果已签单或以预打保单,跳出");
        if (StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("1")
                || StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("9"))
        {
            return true;
        }
        //校验客户投保次数
        if (!CheckAppntNum())
        {
            return false;
        }
        newContNo = PubFun1.CreateMaxNo("CONTNO", mLCContSchema.getAppntNo());
        System.out.println("生成团体合同号！ : " + newContNo);
        if (!checkContNoIsNew())
        {
            return false;
        }

        if (!perpareSignPolData())
        {
            return false;
        }

        if (!perpareSignContData())
        {
            return false;
        }

        if (!changeContNo())
        {
            return false;
        }

        if (!perpareTransferData())
        {
            return false;
        }
        return true;
    }

    /**
     * checkContNoIsNew
     *
     * @return boolean
     */
    private boolean checkContNoIsNew()
    {
        String strSql = "select 1 from lccont where contno='" + this.newContNo + "' union "
                + "select 1 from lbcont where contno='" + this.newContNo + "' union "
                + "select 1 from lobcont where contno='" + this.newContNo + "' union "
                + "select 1 from lpcont where contno='" + this.newContNo + "'";
        String chk = (new ExeSQL()).getOneValue(strSql);
        if (chk == null || chk.equals("") || chk.equals("null"))
        {

            return true;
        }
        else
        {
            buildError("checkContNoIsNew", "生成的保单号码有误请程序员解决！");
            return false;
        }
    }

    /**
     * 处理传递参数问题
     *
     * @return boolean
     */
    private boolean perpareTransferData()
    {
        this.mTransferData.removeByName("ContNo");
        this.mTransferData.setNameAndValue("ContNo", this.newContNo);
        if (!StrTool.cTrim(mLCContSchema.getAppFlag()).equals("9"))
        {
            this.mTransferData.setNameAndValue("ContNo", mContNo);
        }
        this.mTransferData.setNameAndValue("AppntName", this.mLCContSchema.getAppntName());
        this.mTransferData.setNameAndValue("AppntNo", this.mLCContSchema.getAppntNo());
        this.mTransferData.setNameAndValue("ManageCom", this.mLCContSchema.getManageCom());
        this.mTransferData.setNameAndValue("CValiDate", this.mLCContSchema.getCValiDate());
        this.mTransferData.setNameAndValue("UWCode", this.mLCContSchema.getUWOperator());
        this.mTransferData.setNameAndValue("UWDate", this.mLCContSchema.getUWDate());
        this.mTransferData.setNameAndValue("ProposalContNo", this.mLCContSchema.getProposalContNo());
        return true;
    }

    /**
     * changeContNo
     *
     * @return boolean
     */
    private boolean changeContNo()
    {
        /** 一个是换险种号码，一个是换合同号码 */
        String[] tables = { "LCInsured", "LCAppnt", "LCCustomerImpart", "LCCustomerImpartParams", "LCCUWMaster",
                "LCNation", "LCPENotice" };
        String condition = " contno='" + newContNo + "', ModifyDate='" + PubFun.getCurrentDate() + "', ModifyTime='"
                + PubFun.getCurrentTime() + "' ";
        String wherepart = " contno='" + mContNo + "'";
        Vector vec = PubFun.formUpdateSql(tables, condition, wherepart);
        if (vec != null)
        {
            MMap tmpMap = new MMap();
            for (int i = 0; i < vec.size(); i++)
            {
                tmpMap.put((String) vec.get(i), "UPDATE");
            }
            this.map.add(tmpMap);
        }
        String sql = "update LDPerson set AppntNum=to_zero(AppntNum)+1 where CustomerNo='"
                + this.mLCContSchema.getAppntNo() + "'";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * perpareSignContData
     *
     * @return boolean
     */
    private boolean perpareSignContData()
    {
        FDate tFDate = new FDate();
        if (mPayToDate == null)
        {
            mPayToDate = tFDate.getDate("1900-01-01");
        }
        if (maxPayDate == null)
        {
            maxPayDate = tFDate.getDate("1900-01-01");
        }
        for (int m = 1; m <= this.mLCPolSet.size(); m++)
        {
            if (mPayToDate.before(tFDate.getDate(mLCPolSet.get(m).getPaytoDate())))
            {
                this.mPayToDate = tFDate.getDate(mLCPolSet.get(m).getPaytoDate());
            }
            getLCPolSpecRela(mLCPolSet.get(m), m);
        }
        if (maxPayDate.before(mPayToDate))
        {
            maxPayDate = mPayToDate;
        }
        //指定合同生效日
        FDate fdate = new FDate();
        Date cValidate = null;
        Date tPayToDate = null;
        Date paytodate;
        for (int l = 1; l <= mLCPolSet.size(); l++)
        {
            paytodate = fdate.getDate(mLCPolSet.get(l).getPaytoDate());
            if (tPayToDate == null || tPayToDate.after(paytodate))
            {
                tPayToDate = paytodate;
            }
            if (mLCPolSet.get(l).getCValiDate() != null)
            {
                if (cValidate == null)
                {
                    cValidate = fdate.getDate(mLCPolSet.get(l).getCValiDate());
                }
                if (fdate.getDate(mLCPolSet.get(l).getCValiDate()).before(cValidate))
                {
                    cValidate = fdate.getDate(mLCPolSet.get(l).getCValiDate());
                }
            }
        }
        this.mLCContSchema.setPaytoDate(this.mPayToDate);
        System.out.println("PayTodate : " + mLCContSchema.getPaytoDate());
        /**
         * 总期交保费
         */
        double tempSumPrem = 0.0;
        for (int m = 1; m <= mLCPolSet.size(); m++)
        {
            tempSumPrem += mLCPolSet.get(m).getSumPrem();
        }
        mLCContSchema.setSumPrem(tempSumPrem);
        if (mLCContSchema.getCValiDate() == null)
        {
            if (cValidate == null)
            {
                cValidate = PubFun.calDate(new Date(), 1, "D", new Date());
            }
            mLCContSchema.setCValiDate(cValidate);
        }

        /**@author:Yangming 在处理个单时根据保险起见计算出满期日期 */
        if (!dealCValiDate(mLCContSchema))
        {
            return false;
        }
        /** 完成上述操作后进行保单后续操作，以后扩展个单合同签单需要扩展此处 */
        if (!dealOtherContInfo())
        {
            return false;
        }
        return true;
    }

    /**
     * dealOtherContInfo
     *
     * @return boolean
     */
    private boolean dealOtherContInfo()
    {
        String tContPremFeeNo = PubFun1.CreateMaxNo("ContPremFeeNo", "");
        //客户回执号
        String tCusReceiptNo = PubFun1.CreateMaxNo("CusReceiptNo", newContNo);
        //保费收据号
        this.mLCContSchema.setContPremFeeNo(tContPremFeeNo);
        //客户回执号
        this.mLCContSchema.setCustomerReceiptNo(tCusReceiptNo);
        mLCContSchema.setPaytoDate(mPayToDate);
        this.mLCContSchema.setAppFlag("9");
        this.map.put(this.mLCContSchema.getSchema(), "DELETE");
        this.mLCContSchema.setContNo(this.newContNo);
        this.map.put(this.mLCContSchema, "INSERT");
        return true;
    }

    /**
     * perpareSignPolData
     *
     * @return boolean
     */
    private boolean perpareSignPolData()
    {
        VData tInput = new VData();
        tInput.add(this.mGlobalInput);
        tInput.add(this.mLCPolSet);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ContNo", newContNo);
        tInput.add(tTransferData);
        tInput.add(newContNo);
        tInput.add(this.mGlobalInput);
        PreviewRiskSignBL tPreviewRiskSignBL = new PreviewRiskSignBL();
        if (!tPreviewRiskSignBL.submitData(tInput, "PREVIEW"))
        {
            this.mErrors.copyAllErrors(tPreviewRiskSignBL.mErrors);
            return false;
        }
        tInput.clear();
        tInput = tPreviewRiskSignBL.getResult();
        map.add((MMap) tInput.getObjectByObjectName("MMap", 0));
        this.mLCPolSet = (LCPolSet) tInput.getObjectByObjectName("LCPolSet", 0);
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mContNo);
        if (!tLCContDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            return false;
        }
        this.mLCContSchema = tLCContDB.getSchema();
        if (!StrTool.cTrim(this.mLCContSchema.getUWFlag()).equals("4")
                && !StrTool.cTrim(this.mLCContSchema.getUWFlag()).equals("9"))
        {
            buildError("checkData", "核保结论不对,不能签单！");
            return false;
        }
        LCPolDB tLCPolDB = new LCPolDB();
        this.mLCPolSet = tLCPolDB.executeQuery("select * from lcpol where contno='" + this.mContNo
                + "' and uwflag in ('4','9') and appflag='0' order by InsuredNo,RiskCode ");
        if (mLCPolSet.size() <= 0)
        {
            buildError("checkData", "查询险种信息失败！");
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        if (this.mInputData == null)
        {
            buildError("getInputData", "传入信息为空,程序有错,找程序员处理！");
            return false;
        }
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        if (this.mTransferData == null)
        {
            buildError("getInputData", "传入的参数信息为null！");
            return false;
        }
        this.mContNo = (String) mTransferData.getValueByName("ContNo");
        if (StrTool.cTrim(this.mContNo).equals(""))
        {
            buildError("getInputData", "传入合同号码为null,找程序员处理此问题！");
            return false;
        }
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PreviewPrintAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    /**
     *
     * @param tLCPolSchema LCPolSchema
     * @param m int
     */
    private void getLCPolSpecRela(LCPolSchema tLCPolSchema, int m)
    {
        LCSpecSet mLCSpecSet = null;

        LCSpecDB mLCSpecDB = new LCSpecDB();
        mLCSpecDB.setPolNo(tLCPolSchema.getProposalNo());
        mLCSpecSet = mLCSpecDB.query();
        if (mLCSpecSet.size() > 0)
        {
            for (int i = 1; i <= mLCSpecSet.size(); i++)
            {
                LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(mLCSpecSet.get(i).getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(m));
                tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i));
                tLCPolSpecRelaSchema.setSpecContent(mLCSpecSet.get(i).getSpecContent());
                mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            }
        }
        String addFeeSql = "select * from lcprem where polno='" + tLCPolSchema.getProposalNo()
                + "' and payplancode like '000000%'";
        LCPremDB tLCPremDB = new LCPremDB();
        LCPremSet tLCPremSet = tLCPremDB.executeQuery(addFeeSql);
        if (tLCPremSet.size() > 0)
        {
            for (int i = 1; i <= tLCPremSet.size(); i++)
            {
                LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(tLCPremSet.get(i).getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(m));
                tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i + mLCSpecSet.size()));
                double addfee = tLCPremSet.get(i).getPrem();
                double addFeeRate = tLCPremSet.get(i).getRate();
                String StrarDate = tLCPremSet.get(i).getPayStartDate();
                String EndDate = tLCPremSet.get(i).getPayEndDate();
                String addFeeResult = "";
                if (addfee > 0 || addFeeRate > 0)
                {
                    addFeeResult = "附加风险保费";
                    addFeeResult += (addFeeRate > 0 ? String.valueOf(addFeeRate * 100) + "％" : String.valueOf(addfee))
                            + "; ";
                    addFeeResult += "加费自" + StrarDate.split("-")[0] + "年" + StrarDate.split("-")[1] + "月"
                            + StrarDate.split("-")[2] + "日" + "开始 ";
                    if (!StrTool.cTrim(EndDate).equals(""))
                    {
                        addFeeResult += "至" + EndDate.split("-")[0] + "年" + EndDate.split("-")[1] + "月"
                                + EndDate.split("-")[2] + "日" + "终止";
                    }
                }
                tLCPolSpecRelaSchema.setSpecContent("附加保费" + tLCPremSet.get(i).getPrem() + "元");
                mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            }
        }
        getBnfInfo(tLCPolSchema, m, mLCSpecSet);
        if (mLCPolSpecRelaSet.size() > 0)
        {
            map.put(mLCPolSpecRelaSet, "INSERT");
        }
    }

    /**
     *
     * @param tLCPolSchema LCPolSchema
     * @param m int
     * @param mLCSpecSet LCSpecSet
     */
    private void getBnfInfo(LCPolSchema tLCPolSchema, int m, LCSpecSet mLCSpecSet)
    {
        LCBnfDB tLCBnfDB = new LCBnfDB();
        tLCBnfDB.setPolNo(tLCPolSchema.getProposalNo());
        String strSql = "select * from lcbnf where polno in ('" + tLCPolSchema.getPolNo() + "','"
                + tLCPolSchema.getProposalNo() + "')";
        LCBnfSet tLCBnfSet = tLCBnfDB.executeQuery(strSql);
        /** 当受益人大于一人时添加注视 */
        int count = this.mLCPolSpecRelaSet.size();
        if (tLCBnfSet.size() > 0)
        {
            for (int i = 1; i <= tLCBnfSet.size(); i++)
            {
                LCPolSpecRelaSchema tLCPolSpecRelaSchema = new LCPolSpecRelaSchema();
                tLCPolSpecRelaSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                tLCPolSpecRelaSchema.setContNo(tLCPolSchema.getContNo());
                tLCPolSpecRelaSchema.setPolNo(tLCPolSchema.getPolNo());
                tLCPolSpecRelaSchema.setSpecCode(String.valueOf(m));
                tLCPolSpecRelaSchema.setOperator(mGlobalInput.Operator);
                tLCPolSpecRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLCPolSpecRelaSchema.setSpecNo(String.valueOf(i + count));
                tLCPolSpecRelaSchema.setSpecContent(getBnfContent(tLCBnfSet.get(i)));
                mLCPolSpecRelaSet.add(tLCPolSpecRelaSchema);
            }
        }
    }

    /**
     * getBnfInfo
     *
     * @param tLCBnfSchema LCBnfSchema
     * @return String
     */
    private String getBnfContent(LCBnfSchema tLCBnfSchema)
    {
        if (tLCBnfSchema != null)
        {
            return "受益人：" + tLCBnfSchema.getName() + " 证件号码：" + StrTool.cTrim(tLCBnfSchema.getIDNo()) + " 受益顺位："
                    + tLCBnfSchema.getBnfGrade() + " 受益比例：" + tLCBnfSchema.getBnfLot() * 100 + "％";
        }
        return "";
    }

    /**
     * dealCValiDate
     *
     * @param tLCContSchema LCContSchema
     * @return boolean
     */
    private boolean dealCValiDate(LCContSchema tLCContSchema)
    {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setContNo(tLCContSchema.getContNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();
        /**@author:Yangming 这个时候Duty里面不应该没有东西 */
        if (tLCDutySet.size() <= 0)
        {
            // @@错误处理
            buildError("dealCValiDate", "！");
            return false;
        }
        String strDate = tLCDutySet.get(1).getEndDate();
        FDate fDate = new FDate();
        Date CValiDate = fDate.getDate(strDate);
        for (int i = 1; i < tLCDutySet.size(); i++)
        {
            if (CValiDate.before(fDate.getDate(tLCDutySet.get(i).getEndDate())))
            {
                CValiDate = fDate.getDate(tLCDutySet.get(i).getEndDate());
            }
        }
        /**@author:Yangming 兼容性调整：原来存储的时候
         * LCDuty中的EndDaty日期比较乱，以后终身的将全部采取3000-1-1 */
        int year = fDate.getDate(PubFun.getCurrentDate()).getYear();
        int end = CValiDate.getYear();
        if (end - year > 200)
        {
            CValiDate = fDate.getDate("3000-1-1");
        }
        tLCContSchema.setCInValiDate(CValiDate);
        return true;
    }

    private boolean CheckAppntNum()
    {
        int Appntnum = 0;
        StringBuffer sql = new StringBuffer();

        // 原校验sql，现注掉。
        //        sql.append(" select A+B+C from ");
        //        sql.append(" ( ");
        //        sql.append(" select ");
        //        sql
        //                .append(" (select count(1) from lccont where appntno='"
        //                        + mLCContSchema.getAppntNo()
        //                        + "' and appflag in('9','1')) A, ");
        //        sql.append(" (select count(1) from lbcont where appntno='"
        //                + mLCContSchema.getAppntNo() + "') B, ");
        //        sql.append(" (select count(1) from lobcont where appntno='"
        //                + mLCContSchema.getAppntNo() + "') C ");
        //        sql.append(" from dual ");
        //        sql.append(" )as X ");

        // 修正投保人校验问题。
        // 如果投保人投过多次保单，而最近一次的保单做过保全投保人变更的话，
        // 会造成ContNo生成冲突。
        int numString = mLCContSchema.getAppntNo().length();

        sql.append(" select max(maxno) from ( ");
        sql.append(" select max(int(substr(contno," + (numString + 1) + "))) maxno from ( ");
        sql.append(" select contno from lccont where appntno='" + mLCContSchema.getAppntNo()
                + "' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lbcont where appntno='" + mLCContSchema.getAppntNo() + "' and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lobcont where appntno='" + mLCContSchema.getAppntNo()
                + "' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" ) as X  ");
        sql.append(" Union ");
        sql.append(" select max(int(substr(contno," + (numString + 1) + "))) maxno from ( ");
        sql.append(" select contno from lccont where ContNo like '" + mLCContSchema.getAppntNo()
                + "%' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lbcont where ContNo like '" + mLCContSchema.getAppntNo() + "%' and contno not like '%E%' ");
        sql.append(" union ");
        sql.append(" select contno from lobcont where ContNo like '" + mLCContSchema.getAppntNo()
                + "%' and appflag in('1','9') and contno not like '%E%' ");
        sql.append(" ) as Y  ");
        sql.append(" ) as temp ");
        // -----------------------------------------------------

        System.out.println("SQL" + sql.toString());
        SSRS tSSRS = (new ExeSQL()).execSQL(sql.toString());
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSet tLDPersonSet = new LDPersonSet();
        tLDPersonDB.setCustomerNo(mLCContSchema.getAppntNo());
        tLDPersonSet = tLDPersonDB.query();
        if (tLDPersonSet.size() == 1)
        {
            Appntnum = tLDPersonSet.get(1).getAppntNum();
            System.out.println("Appntnum" + Appntnum);
        }
        else
        {
            buildError("CheckAppntNum", "该客户在客户表中的记录有误！");
            return false;
        }
        int newAppntnum = Integer.parseInt(tSSRS.GetText(1, 1));
        System.out.println("newAppntnum" + newAppntnum);
        if (Appntnum != newAppntnum)
        {
            tAppntnum = newAppntnum;
        }
        else
        {
            tAppntnum = newAppntnum;
        }
        String sql1 = "update LDPerson set AppntNum=" + tAppntnum + " where CustomerNo='"
                + this.mLCContSchema.getAppntNo() + "'";
        map1.put(sql1, "UPDATE");
        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map1);
        if (!tPubSubmit.submitData(tVData, ""))
        {
            buildError("CheckAppntNum", "更新客户信息表出现错误！");
            return false;
        }
        return true;
    }

    private boolean checkcardflag(String tContNo)
    {
        if (tContNo == null)
        {
            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo())
        {
            return false;
        }
        else if (tLCContDB.getCardFlag().equals("6") && tLCContDB.getTempFeeNo() == null )
        {
            return true;
        }
        return false;
    }

    private boolean prepareBankData(String tContNo)
    {

        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
        //存储各种银行信息,银行编码,户名,帐号
        String BankFlag = "";
        String BankCode = "";
        String BankAccNo = "";
        String AccName = "";
        /**
         * 合同信息
         */
        if (tContNo == null)
        {
            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo())
        {
            return false;
        }
        BankFlag = tLCContDB.getPayMode();
        BankCode = tLCContDB.getBankCode();
        BankAccNo = tLCContDB.getBankAccNo();
        AccName = tLCContDB.getAccName();
        
        System.out.println("缴费方式：---------"+BankFlag);
        if (!"4".equals(BankFlag))
        { //4代表银行转账
        	System.out.println("缴费通知书打印："+BankFlag);
        	SendPayInfo();
            return true;
        }else{
            // 判断是否需要打印缴费通知书。
            String tNoPrtFlag = "0";//默认：0-需要打印
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setOtherNo(mContNo);
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode("07");
            //校验是否发过首期交费通知书 ，如果发过则不再重发
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
            if (tLOPRTManagerDB.query().size() <= 0)
            {
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("NoPrtFlag", tNoPrtFlag);
                
                VData tempVData = new VData();
                tempVData.add(tLOPRTManagerSchema);
                tempVData.add(tTransferData);
                tempVData.add(mGlobalInput);
                //单独生成打印管理表数据
                UWSendPrintUIS tUWSendPrintUIS = new UWSendPrintUIS();
                if (!tUWSendPrintUIS.submitData(tempVData, "INSERT"))
                {
                    this.mErrors.copyAllErrors(tUWSendPrintUIS.mErrors);
//                    return false;
                }
            }
        }
        if (StrTool.cTrim(BankAccNo).equals("") || StrTool.cTrim(BankCode).equals("") || BankAccNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "交费方式选择银行转账,但银行代码,银行账号,户名信息不完整!";
            this.mErrors.addOneError(tError);
            return false;
        }

        String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", tLCContDB.getPrtNo());

        //String TempFeeNo = PubFun1.CreateMaxNo("GPAYNOTICENO",this.mLCContSchema.getPrtNo());
        String sql = "select riskcode, sum(prem) from lcpol where ContNo='" + mContNo
                + "' and uwflag in ('4','9') group by riskcode ";
        ExeSQL tExeSQL = new ExeSQL();
        //自核通过的情况

        sql = "select riskcode, sum(prem) from lcpol where ContNo='" + mContNo + "' group by riskcode ";
        SSRS ssrs = tExeSQL.execSQL(sql);
        if (ssrs.equals("null"))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareBankData";
            tError.errorMessage = "查询费用失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String[][] prem = ssrs.getAllData();
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLCContDB.getManageCom());
        GregorianCalendar Calendar = new GregorianCalendar();
        Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
        Calendar.add(Calendar.DATE, 0);
        double sumPrem = 0.00;
        for (int i = 0; i + 1 <= prem.length; i++)
        {
            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(prtSeq);
            tLJTempFeeSchema.setTempFeeType("1");
            tLJTempFeeSchema.setRiskCode(prem[i][0]);
            tLJTempFeeSchema.setAgentGroup(tLCContDB.getAgentGroup());
            tLJTempFeeSchema.setAPPntName(tLCContDB.getAppntName());
            tLJTempFeeSchema.setAgentCode(tLCContDB.getAgentCode());
            tLJTempFeeSchema.setPayDate(Calendar.getTime());
            tLJTempFeeSchema.setPayMoney(prem[i][1]);
            sumPrem += Arith.round(Double.parseDouble(prem[i][1]), 2);
            tLJTempFeeSchema.setManageCom(tLCContDB.getManageCom());
            tLJTempFeeSchema.setOtherNo(tLCContDB.getPrtNo());
            tLJTempFeeSchema.setOtherNoType("4");
            tLJTempFeeSchema.setPolicyCom(tLCContDB.getManageCom());
            tLJTempFeeSchema.setSerialNo(serNo);
            tLJTempFeeSchema.setConfFlag("0");
            tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
            tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
            tLJTempFeeSet.add(tLJTempFeeSchema);
        }
        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(prtSeq);
        tLJTempFeeClassSchema.setPayMode("4");
        tLJTempFeeClassSchema.setPayDate(Calendar.getTime());
        tLJTempFeeClassSchema.setPayMoney(Arith.round(sumPrem, 2));
        tLJTempFeeClassSchema.setManageCom(tLCContDB.getManageCom());
        tLJTempFeeClassSchema.setPolicyCom(tLCContDB.getManageCom());
        tLJTempFeeClassSchema.setBankCode(BankCode);
        tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
        tLJTempFeeClassSchema.setAccName(AccName);
        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
        MMap map = new MMap();
        if (tLJTempFeeSet.size() > 0 && tLJTempFeeClassSet.size() > 0)
        {
            map.put(tLJTempFeeSet, "DELETE&INSERT");
            map.put(tLJTempFeeClassSet, "DELETE&INSERT");
        }
        this.mResult.add(map);
        return true;
    }

    /**
     * 处理投保人帐户转保费
     * @return
     */
    private MMap dealAppAccount(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        VData tVData = new VData();

        TransferData tTransferData = new TransferData();

        String tContNo = cContInfo.getContNo();
        tTransferData.setNameAndValue("ContNo", tContNo);

        String tAppntNo = cContInfo.getAppntNo();
        tTransferData.setNameAndValue("AppAccNo", tAppntNo);

        String tStrSql = "select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) from LCPol lcp "
                + " where lcp.UWFlag in ('4', '9') and lcp.ContNo = '" + tContNo + "' ";
        String tPolicySumPrem = new ExeSQL().getOneValue(tStrSql);
        tTransferData.setNameAndValue("AppAccMoney", tPolicySumPrem);

        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        PolicyAppAccPayBL tPolicyAppAccPayBL = new PolicyAppAccPayBL();
        tTmpMap = null;
        tTmpMap = tPolicyAppAccPayBL.getSubmitMap(tVData, "AppAccPay");
        if (tTmpMap == null)
        {
            buildError("dealCertInsuRela", tPolicyAppAccPayBL.mErrors.getFirstError());
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;

        return tMMap;
    }
    
    private boolean SendPayInfo()
    {

        // 判断是否需要打印缴费通知书。
        String tNoPrtFlag = "0";//默认：0-需要打印
//        if (!"2".equals(tContType) && !"5".equals(tContType))
//        {
//            tNoPrtFlag = "1";
//            System.out.println("不需要打印缴费通知书。");
//        }
        // --------------------

        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mContNo);
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode("07");
        //校验是否发过首期交费通知书 ，如果发过则不再重发
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
        if (tLOPRTManagerDB.query().size() <= 0)
        {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("NoPrtFlag", tNoPrtFlag);
            
            VData tempVData = new VData();
            tempVData.add(tLOPRTManagerSchema);
            tempVData.add(tTransferData);
            tempVData.add(mGlobalInput);

            UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
            if (!tUWSendPrintUI.submitData(tempVData, "INSERT"))
            {
                this.mErrors.copyAllErrors(tUWSendPrintUI.mErrors);
                return false;
            }
        }
        //        }
        return true;
    }

}
