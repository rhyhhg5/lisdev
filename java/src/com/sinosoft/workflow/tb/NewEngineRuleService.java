package com.sinosoft.workflow.tb;

import org.jdom.Document;

import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.EngineRuleSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class NewEngineRuleService {
	private PubSubmit pub = new PubSubmit();

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	/**
	 * @param tLCContSchema
	 * @return
	 */
	public MMap dealNPData(LCContSchema tLCContSchema,String codetype) {
		// VData mData = new VData();
		MMap map = new MMap();
		ExeSQL tExeSQL = new ExeSQL();
		String tSQL = "select code from ldcode where codetype='"+codetype+"' with ur";
		String tRuleButton = tExeSQL.getOneValue(tSQL);

		System.out.println("新投保规则开关1啊：" + tRuleButton);
		if (tRuleButton != null && tRuleButton.equals("00")) {
			try {
				// 调用规则引擎
				RuleTransfer ruleTransfer = new RuleTransfer();
				// 获得规则引擎返回的校验信息 调用投保规则
				System.out.println("开始调用新规则引擎，时间" + PubFun.getCurrentDate() + " " + PubFun.getCurrentTime());
				long startTime = System.currentTimeMillis();
				String xmlStr = ruleTransfer.getNewXmlStr("lis", "uw", "NP", tLCContSchema.getContNo(), false);
				// 将校验信息转换为Xml的document对象
				Document doc = ruleTransfer.stringToDoc(xmlStr);
				System.out.println("xmlStr=" + xmlStr);
				String approved = ruleTransfer.getApproved(doc);
				long endTime = System.currentTimeMillis();
				System.out.println("调用新规则引擎结束，时间" + PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + "用时："
						+ ((endTime - startTime) / 1000 / 60) + "分钟");
				LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); // 未通过的投保规则
				System.out.println("NP新规则引擎未通过规则size:"+tLMUWSetPolAllUnpass.size());

				//modify by zxs
				if (tLMUWSetPolAllUnpass.size() > 0) {
					for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++) {
						LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
						EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
						engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
						engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
						engineRuleSchema.setApproved(approved);
						engineRuleSchema.setRuleResource("new");
						engineRuleSchema.setRuleType("NP");
						engineRuleSchema.setContType("标准保单");
						engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
						engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
						engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
						engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
						if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
							engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
						}else{
							engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
						}
						engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
						// prtno :印刷号approved:
						// 核保标识ruleresource:规则来源ruletype:规则类型conttype:保单类型
						// ruleName：规则名称riskCode：险种编码insuredNo：被保人编码returnInfo：返回信息
						System.out.println("approved=" + approved + "ruleName=" + tLMUWSChema.getUWCode() + "riskcode="
								+ tLMUWSChema.getRiskCode() + "insuredNo=" + tLMUWSChema.getOthCalCode() + "returnInfo="
								+ tLMUWSChema.getRemark());
						map.put(engineRuleSchema, "INSERT");
					}

				}else{
					EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
					engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
					engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
					engineRuleSchema.setApproved(approved);
					engineRuleSchema.setRuleResource("new");
					engineRuleSchema.setRuleType("NP");
					engineRuleSchema.setContType("标准保单");
					engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
					map.put(engineRuleSchema, "INSERT");
				}
			} catch (Exception e) {
				e.printStackTrace();

			}
		}

		return map;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回, /**
	 * 
	 * @param tLCContSchema
	 * @return
	 */
	public MMap dealNUData(LCContSchema tLCContSchema, String codetype) {
		VData mData = new VData();
		MMap map = new MMap();
		ExeSQL tExeSQL = new ExeSQL();
		String tSQL = "select code from ldcode where codetype='" + codetype + "' with ur";
		String tRuleButton = tExeSQL.getOneValue(tSQL);
	
		if (tRuleButton == null || !tRuleButton.equals("00")) {
			System.out.println("规则引擎开关是关闭状态，请打开规则引擎！");
		} else {
			try {
				RuleTransfer ruleTransfer = new RuleTransfer();
				// 获得规则引擎返回的校验信息 调用投保规则
				String xmlStr = ruleTransfer.getNewXmlStr("lis", "uw", "NU", tLCContSchema.getContNo(), false);
				System.out.println("new xmlStr=" + xmlStr);
				// 将校验信息转换为Xml的document对象
				Document doc = ruleTransfer.stringToDoc(xmlStr);
				String approved = ruleTransfer.getApproved(doc);
				LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); // 未通过的投保规则
				if (tLMUWSetPolAllUnpass.size() > 0) {
					for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++) {
						LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
						EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
						engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
						engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
						engineRuleSchema.setApproved(approved);
						engineRuleSchema.setRuleResource("new");
						engineRuleSchema.setRuleType("NU");
						if (codetype.equals("NewQYJYRuleButton")) {
							engineRuleSchema.setContType("简易保单");
						} else if (codetype.equals("NewQYWNRuleButton")) {
							engineRuleSchema.setContType("万能保单");
						}else if(codetype.equals("NewQYBZRuleButton")){
							engineRuleSchema.setContType("标准保单");
						}
						engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
						engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
						engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
						engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
						if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
							engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
						}else{
							engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
						}
						engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());	
						map.put(engineRuleSchema, "INSERT");
					}

				}else{
					EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
					engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
					engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
					engineRuleSchema.setApproved(approved);
					engineRuleSchema.setRuleResource("new");
					engineRuleSchema.setRuleType("NU");
					if (codetype.equals("NewQYJYRuleButton")) {
						engineRuleSchema.setContType("简易保单");
					} else if (codetype.equals("NewQYWNRuleButton")) {
						engineRuleSchema.setContType("万能保单");
					} else if(codetype.equals("NewQYBZRuleButton")){
							engineRuleSchema.setContType("标准保单");
						}
					engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					engineRuleSchema.setMakeTime(PubFun.getCurrentTime());	
					map.put(engineRuleSchema, "INSERT");

				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

		return map;

	}

}
