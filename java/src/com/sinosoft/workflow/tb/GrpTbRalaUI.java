package com.sinosoft.workflow.tb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCUWSendTraceSchema;
import com.sinosoft.lis.schema.LCGCUWMasterSchema;
import com.sinosoft.lis.cbcheck.UWSendTraceAllUI;
import java.util.Vector;

/**
 * <p>Title: 关联方 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: SinoSoft </p>
 * @author tianjingxia
 * @version 1.0
 */

public class GrpTbRalaUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    public GrpTbRalaUI() {}

    public static void main(String[] args) {
        VData tVData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        TransferData mTransferData = new TransferData();
        String Activity = "";

        /** 全局变量 */
        mGlobalInput.Operator = "picch";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";

     
   
        /**
         * 团体新单复核0000002001
         */
        //Activity = "0000002001";
        //mTransferData.setNameAndValue("GrpContNo", "1400000971");
        //mTransferData.setNameAndValue("PrtNo", "18000000078");
        //mTransferData.setNameAndValue("SaleChnl", "01");
        //mTransferData.setNameAndValue("ManageCom", "86110000");
        //mTransferData.setNameAndValue("GrpName", "清华大学");
        //mTransferData.setNameAndValue("CValiDate", "2006-01-01");
        //mTransferData.setNameAndValue("MissionID", "00000000000000003104");
        //mTransferData.setNameAndValue("SubMissionID",
        //                              "2");

        /**总变量*/

        tVData.add(mGlobalInput);
        //tVData.add(mTransferData);
        GrpTbRalaUI tGrpTbRalaUI = new GrpTbRalaUI();
        try {
            if (tGrpTbRalaUI.submitData(tVData, Activity)) {
                VData tResult = new VData();
                System.out.println("UI--VDATE--TRESULT");

                //tResult = tActivityOperator.getResult() ;
            } else {
                System.out.println(tGrpTbRalaUI.mErrors.getError(0).
                                   errorMessage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        GrpTbRelaBL tGrpTbRelaBL = new GrpTbRelaBL();

        System.out.println("---GrpTbRelaBL UI BEGIN---");
        if (tGrpTbRelaBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpTbRelaBL.mErrors);
            mResult.clear();
            return false;
        }
        return true;
    }
}
