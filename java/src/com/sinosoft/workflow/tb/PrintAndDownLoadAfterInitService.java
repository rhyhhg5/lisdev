package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.f1print.LCContF1PUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.lis.db.LCContPrintDB;
import com.sinosoft.lis.vschema.LCContPrintSet;
import com.sinosoft.lis.easyscan.ProposalDownloadUI;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class PrintAndDownLoadAfterInitService implements AfterInitService {
    /** 报错信息 */
    private CErrors mErrors = new CErrors();
    /** 操作参数 */
    private TransferData mTransferData;
    /** 传入后台结果 */
    private VData mResult = new VData();
    /** 前台传入数据 */
    private VData mInputData;
    /** 操作符 */
    private String mOperate;
    /** 合同信息 */
    private LCContSchema mLCContSchema;
    /** 合同号码 */
    private String mContNo;
    /** 操作信息 */
    private GlobalInput mGlobalInput;
    public PrintAndDownLoadAfterInitService() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        /** 获取数据 */
        if (!getInputData()) {
            return false;
        }
        /** 教研数据 */
        if (!checkData()) {
            return false;
        }
        /** 处里业务逻辑 */
        if (!dealData()) {
            return false;
        }
        if (!perpareOutputData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        if (this.mInputData == null) {
            buildError("getInputData", "传入信息为空！");
            return false;
        }
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (this.mTransferData == null) {
            buildError("getInputData", "传入参数为空！");
            return false;
        }
        this.mContNo = (String) mTransferData.getValueByName("ContNo");
        this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (StrTool.cTrim(this.mContNo).equals("")) {
            buildError("checkData", "传入合同号码为空！");
            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mContNo);
        if (!tLCContDB.getInfo()) {
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            return false;
        }
        this.mLCContSchema = tLCContDB.getSchema();
        if (!StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("1") && //签单
            !StrTool.cTrim(this.mLCContSchema.getAppFlag()).equals("9")) { //预打保单
            buildError("checkData", "保单未签单，不能进行保单打印！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCContSchema.getUWFlag()).equals("4") &&
            !StrTool.cTrim(this.mLCContSchema.getUWFlag()).equals("9")) {
            buildError("checkData", "人工核保没有通过！");
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        LCContF1PUI tLCContF1PUI = new LCContF1PUI();
        VData vData = new VData();
        vData.add(mGlobalInput);
        vData.addElement(this.mLCContSchema);
        vData.add(this.mTransferData.getValueByName("TemplatePath"));
        vData.add(this.mTransferData.getValueByName("OutXmlPath"));
        if (!tLCContF1PUI.submitData(vData, "PRINT")) {
            this.mErrors.copyAllErrors(tLCContF1PUI.mErrors);
            return false;
        }
        /** 完成保单数据生成，需要将其打成ｚｉｐ包进行下载 */
        if (!creatZipFile()) {
            return false;
        }
        return true;
    }

    /**
     * creatZipFile
     *
     * @return boolean
     */
    private boolean creatZipFile() {
        LCContPrintDB tLCContPrintDB = new LCContPrintDB();
        tLCContPrintDB.setPrtNo(this.mLCContSchema.getPrtNo());
        LCContPrintSet tLCContPrintSet = tLCContPrintDB.query();
        if (tLCContPrintSet.size() <= 0) {
            buildError("creatZipFile", "生成打印下载数据失败！");
            return false;
        }
        VData vData = new VData();
        vData.add(tLCContPrintSet);
        vData.add(mGlobalInput);
        vData.add(this.mTransferData);
        ProposalDownloadUI tProposalDownloadUI = new ProposalDownloadUI();
        try {
            if (!tProposalDownloadUI.submitData(vData, "Download")) {
                buildError("creatZipFile",
                           "生成文件错误原因是！" +
                           tProposalDownloadUI.mErrors.getFirstError());
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("creatZipFile",
                       "生成文件错误原因是！" + tProposalDownloadUI.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * perpareOutputData
     *
     * @return boolean
     */
    private boolean perpareOutputData() {
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public TransferData getReturnTransferData() {
        return mTransferData;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "PrintAndDownLoadAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
