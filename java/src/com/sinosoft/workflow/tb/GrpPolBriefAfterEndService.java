package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.AfterEndService;


/**
 * <p>Title: 工作流服务类:团体新契约自动核保 </p>
 * <p>Description: 自动核保工作流AfterInit服务类</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class GrpPolBriefAfterEndService implements AfterEndService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private VData mInputData = new VData();
    private TransferData mTransferData = new TransferData();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private GlobalInput mGlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String mGrpContNo = "";

    public GrpPolBriefAfterEndService()
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println(">>>>>>submitData");

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        
        if (!dealData())
            return false;

        if (!prepareTransferData())
            return false;

        return true;
        //return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        
    	mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        //获得当前工作任务的GrpContNo
        mGrpContNo = (String) mTransferData.getValueByName("ProposalGrpContNo");
        if (mGrpContNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpPolApproveAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中GrpContNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo()) //验证LCGrpCont表中是否存在该合同项记录
        {
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "集体合同号为" +mGrpContNo +
                                  "的合同信息未查询到!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema = tLCGrpContDB.getSchema();

        mInputData.clear();
        mInputData.add(mGlobalInput);
        mInputData.add(mLCGrpContSchema);

        return true;
    }


    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return
     */
    private boolean prepareTransferData()
    {

        mTransferData.setNameAndValue("GrpContNo",mLCGrpContSchema.getGrpContNo());
        mTransferData.setNameAndValue("PrtNo", mLCGrpContSchema.getPrtNo());
        mTransferData.setNameAndValue("AgentCode",mLCGrpContSchema.getAgentCode());
        mTransferData.setNameAndValue("AgentGroup",mLCGrpContSchema.getAgentGroup());
        mTransferData.setNameAndValue("SaleChnl", mLCGrpContSchema.getSaleChnl());
        mTransferData.setNameAndValue("ManageCom",mLCGrpContSchema.getManageCom());
        mTransferData.setNameAndValue("GrpNo", mLCGrpContSchema.getAppntNo());
        mTransferData.setNameAndValue("GrpName", mLCGrpContSchema.getGrpName());
        mTransferData.setNameAndValue("CValiDate",mLCGrpContSchema.getCValiDate());
        mTransferData.setNameAndValue("UWFlag", mLCGrpContSchema.getUWFlag());
        mTransferData.setNameAndValue("FinishFlag", "1");
        mTransferData.setNameAndValue("ContPrintType", mLCGrpContSchema.getContPrintType());

        return true;
    }


    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }


    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }


    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

    	if("4".equals(mLCGrpContSchema.getContPrintType())){
    		mResult.clear();
            MMap map = new MMap();
            String tCurrentDate = PubFun.getCurrentDate();
            String tCurrentTime = PubFun.getCurrentTime();

            //修改险种保单表
            String mPolSql = "update LCPol "
                    + "set ApproveCode = '" + mGlobalInput.Operator + "', "
                    + "ApproveDate = '" + tCurrentDate + "', "
                    + "ApproveTime = '" + tCurrentTime + "', "
                    + "ApproveFlag = '9', "
                    + "UWCode = '" + mGlobalInput.Operator + "', "
                    + "UWDate = '" + tCurrentDate + "', "
                    + "UWTime = '" + tCurrentTime + "', "
                    + "UWFlag = '9', "
                    + "ModifyDate = '" + tCurrentDate + "', "
                    + "ModifyTime = '" + tCurrentTime + "' "
                    + "where GrpContNo = '"+mGrpContNo+"'"
                    + "and appflag = '0'"
                    + "and approveflag <> '9'";

            //修改合同表
            String mContSql = "update LCCont "
                    + "set ApproveCode = '" + mGlobalInput.Operator + "', "
                    + "ApproveDate = '" + tCurrentDate + "', "
                    + "ApproveTime = '" + tCurrentTime + "', "
                    + "ApproveFlag = '9', "
                    + "UWOperator = '" + mGlobalInput.Operator + "', "
                    + "UWDate = '" + tCurrentDate + "', "
                    + "UWTime = '" + tCurrentTime + "', "
                    + "UWFlag = '9', "
                    + "ModifyDate = '" + tCurrentDate + "', "
                    + "ModifyTime = '" + tCurrentTime + "' "
                    + "where GrpContNo = '"+mGrpContNo+"'"
                    + "and appflag = '0'"
                    + "and approveflag <> '9'";
            //修改集体合同表
            String mGrpContSql = "update LCGrpCont "
                    + "set ApproveCode = '" + mGlobalInput.Operator + "', "
                    + "ApproveDate = '" + tCurrentDate + "', "
                    + "ApproveTime = '" + tCurrentTime + "', "
                    + "ApproveFlag = '9', "
                    + "UWOperator = '" + mGlobalInput.Operator + "', "
                    + "UWDate = '" + tCurrentDate + "', "
                    + "UWTime = '" + tCurrentTime + "', "
                    + "UWFlag = '9', "
                    + "ModifyDate = '" + tCurrentDate + "', "
                    + "ModifyTime = '" + tCurrentTime + "' "
                    + "where GrpContNo = '"+mGrpContNo+"'"
                    + "and appflag = '0'"
                    + "and approveflag <> '9'";

            String mGrpPolSql = "update LCGrpPol "
                    + "set ApproveCode = '" + mGlobalInput.Operator + "', "
                    + "ApproveDate = '" + tCurrentDate + "', "
                    + "ApproveTime = '" + tCurrentTime + "', "
                    + "ApproveFlag = '9', "
                    + "UWOperator = '" + mGlobalInput.Operator + "', "
                    + "UWDate = '" + tCurrentDate + "', "
                    + "UWTime = '" + tCurrentTime + "', "
                    + "UWFlag = '9', "
                    + "ModifyDate = '" + tCurrentDate + "', "
                    + "ModifyTime = '" + tCurrentTime + "' "
                    + "where PrtNo = '" + mLCGrpContSchema.getPrtNo() + "'"
                    + "and appflag = '0'"
                    + "and approveflag <> '9'";
            
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    		String prtSeq = PubFun1.CreateMaxNo("GPAYNOTICENO",mLCGrpContSchema.getPrtNo());
    		tLOPRTManagerSchema.setPrtSeq(prtSeq);
            tLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode("57");
            tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
            tLOPRTManagerSchema.setPrtType("0");//前台打印
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(tCurrentDate);
            tLOPRTManagerSchema.setMakeTime(tCurrentTime);
            
            map.put(mPolSql, "UPDATE");
            map.put(mContSql, "UPDATE");
            map.put(mGrpContSql, "UPDATE");
            map.put(mGrpPolSql, "UPDATE");
            map.put(tLOPRTManagerSchema, SysConst.INSERT);
            
            mResult.add(map);
    	}
    	
        return true;

    }
}
