/**
 * created 2008-11-21
 * by LY
 */
package com.sinosoft.workflow.tb;

import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.certifybusiness.CertifyGrpContUI;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;

/**
 * @author LY
 *
 */
public class CertifyGrpContInputAfterInitService implements AfterInitService
{
    /**存放结果*/
    private VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = new TransferData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mPrtNo = null;

    /** 工作流信息 */
    private String mMissionID;

    private String mSubMissionID;

    private String mGrpContNo;

    public CertifyGrpContInputAfterInitService()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println(">>>>>>submitData");

        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        String tGrpContNo = new ExeSQL()
                .getOneValue("select GrpContNo from LCGrpCont where PrtNo = '"
                        + mPrtNo + "' ");

        if (tGrpContNo == null || tGrpContNo.equals(""))
        {
            CertifyGrpContUI tCertifyGrpContUI = new CertifyGrpContUI();

            if (!tCertifyGrpContUI.submitData(mInputData, "Create"))
            {
                String tStrErr = " 处理团体合同数据失败! 原因是: "
                        + tCertifyGrpContUI.mErrors.getLastError();
                buildError("submitData", tStrErr);
                return false;
            }
        }

        tGrpContNo = new ExeSQL()
                .getOneValue("select GrpContNo from LCGrpCont where PrtNo = '"
                        + mPrtNo + "' ");
        
        //调用江苏中介校验接口
        /* 获取销售渠道 */
        String tJSSalechnl = new ExeSQL().getOneValue("select salechnl from lcgrpcont where prtno='"+mPrtNo+"'");
        /* 获取管理机构 */
        String tJSManagecom = new ExeSQL().getOneValue("select managecom from lcgrpcont where prtno='"+mPrtNo+"'");
        /* 截取管理机构前四位 */
        String tSubJSManagecom = tJSManagecom.substring(0,4);
        String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
		  	    + "and code = '"+tJSSalechnl+"'";
        SSRS tJSSSRS = new ExeSQL().execSQL(tSql);
        if(!(tJSSSRS == null || tJSSSRS.MaxRow != 1) && "8632".equals(tSubJSManagecom)){
    		ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
  	  	  	TransferData jTransferData = new TransferData();
  	  	  	jTransferData.setNameAndValue("ContNo",tGrpContNo);
  	  	  	VData jVData = new VData();
  	  	  	jVData.add(jTransferData);
  	  	  	if(!tDealQueryAgentInfoBL.submitData(jVData, "check")){
  	  	  		String Content = "印刷号为【" + mPrtNo + "】的保单，发生错误：" + tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
  	  	  		buildError("submitData",Content);
  	  	  		return false;
  	  	  	}
    	}//End JS check
        
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo", tGrpContNo);

        tVData.add(tTransferData);
        tVData.add(this.mGlobalInput);

        ParseGuideIn tPGI = new ParseGuideIn();
        try
        {
            if (!tPGI.submitData(tVData, "INSERT||DATABASE"))
            {
                String tStrErr = " 产生无名单分单信息失败! 原因是: "
                        + tPGI.mErrors.getLastError();
                buildError("submitData", tStrErr);
                return false;
            }

            MMap map = new MMap();
            //map.add(tPGI.getSubmitData());
            map.put(
                    "update lccont set UWFlag='9',ApproveFlag='9' where grpcontno='"
                            + tGrpContNo + "'", "UPDATE");
            map.put(
                    "update lcpol set UWFlag='9', ApproveFlag='9' where grpcontno='"
                            + tGrpContNo + "'", "UPDATE");
            map.put(
                    "update lcgrpcont set UWFlag='9',ApproveFlag='9' where grpcontno='"
                            + tGrpContNo + "'", "UPDATE");
            map.put(
                    "update lcgrppol set UWFlag='9',ApproveFlag='9' where grpcontno='"
                            + tGrpContNo + "'", "UPDATE");

            VData data = new VData();
            data.add(map);

            PubSubmit p = new PubSubmit();
            if (!p.submitData(data, ""))
            {
                System.out.println("提交数据失败");
                buildError("submitData", "提交数据失败");
                return false;
            }

            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(tGrpContNo);
            if (!tLCGrpContDB.getInfo())
            {
                buildError("submitData", "团单信息获取失败！");
                return false;
            }

            mTransferData.setNameAndValue("GrpContNo", tLCGrpContDB
                    .getGrpContNo());
            mTransferData.setNameAndValue("PrtNo", tLCGrpContDB.getPrtNo());
            mTransferData.setNameAndValue("SaleChnl", tLCGrpContDB
                    .getSaleChnl());
            mTransferData.setNameAndValue("ManageCom", tLCGrpContDB
                    .getManageCom());
            mTransferData.setNameAndValue("AgentCode", tLCGrpContDB
                    .getAgentCode());
            mTransferData.setNameAndValue("AgentGroup", tLCGrpContDB
                    .getAgentGroup());
            mTransferData.setNameAndValue("GrpName", tLCGrpContDB.getGrpName());
            mTransferData.setNameAndValue("CValiDate", tLCGrpContDB
                    .getCValiDate());
            mTransferData.setNameAndValue("GrpNo", tLCGrpContDB.getAppntNo());
            mTransferData.setNameAndValue("UWDate", tLCGrpContDB.getUWDate());
            mTransferData.setNameAndValue("UWTime", tLCGrpContDB.getUWTime());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", "发生异常错误 原因是: " + ex + "！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mVResult;
    }

    public TransferData getReturnTransferData()
    {
        return this.mTransferData;
    }

    public CErrors getErrors()
    {
        return this.mErrors;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;

        mGlobalInput = (GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        this.mTransferData = (TransferData) mInputData
                .getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            return false;
        }
        this.mMissionID = (String) mTransferData.getValueByName("MissionID");
        this.mSubMissionID = (String) mTransferData
                .getValueByName("SubMissionID");
        if (this.mMissionID == null)
        {
            buildError("getInputData", "MissionID为null！");
            return false;
        }
        if (this.mSubMissionID == null)
        {
            buildError("getInputData", "SubMissionID为null！");
            return false;
        }
        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null || mPrtNo.equals(""))
        {
            buildError("getInputData", "获取结算单号失败。");
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyGrpContInputAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
