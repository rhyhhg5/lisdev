package com.sinosoft.workflow.tb;

//import java.math.BigDecimal;
import java.util.Date;

import com.sinosoft.lis.cbcheck.SplitFamilyBL;
import com.sinosoft.lis.cbcheck.UWSendPrintUI;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.db.LCCUWSubDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LCUWSubDB;
import com.sinosoft.lis.db.LDUWUserDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LWActivityDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
//import com.sinosoft.lis.finfee.TempFeeBL;
import com.sinosoft.lis.finfee.TempFeeWithdrawBL;
import com.sinosoft.lis.pubfun.CalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GetPayType;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBTempFeeClassSchema;
import com.sinosoft.lis.schema.LBTempFeeSchema;
import com.sinosoft.lis.schema.LCCUWMasterSchema;
import com.sinosoft.lis.schema.LCCUWSubSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUWMasterSchema;
import com.sinosoft.lis.schema.LCUWSubSchema;
import com.sinosoft.lis.schema.LDSysTraceSchema;
import com.sinosoft.lis.schema.LJAGetTempFeeSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
//import com.sinosoft.lis.tb.CommonBL;
//import com.sinosoft.lis.tb.TempFeeAppBL;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LBTempFeeClassSet;
import com.sinosoft.lis.vschema.LBTempFeeSet;
import com.sinosoft.lis.vschema.LCCUWMasterSet;
import com.sinosoft.lis.vschema.LCCUWSubSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.lis.vschema.LCUWSubSet;
import com.sinosoft.lis.vschema.LDSysTraceSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.AfterInitService;
import com.sinosoft.utility.StrTool;

/**
 * <p>Title: 新契约核保确认 </p>
 * <p>Description: 工作流服务类:执行新契约核保确认</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class UWConfirmAfterInitService implements AfterInitService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mReturnFeeData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mSplitMap = new MMap();

    /** 数据操作字符串 */
    private String mManageCom;

    private String mCalCode; //计算编码

    /** 业务处理相关变量 */
    private LCPolSet mLCPolSet = new LCPolSet();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private String mOldPolNo = "";

    private String mUWFlag = ""; //核保标志

    private String mUWIdea = ""; //核保意见

    private String mStopFlag = "";

    private String mUWPopedom = ""; //操作员核保级别

    private String mAppGrade = ""; //上报级别

    private String mPolType = ""; //保单类型

    private String mNewContNo = "";

    private String SendtoFirstPayNotice = "false";//下发首期交费通知书标志

    //modify by Minim
    private String mBackUWGrade = "";

    private String mBackAppGrade = "";

    private String mOperator = "";

    private String mOperatorUWGrade = "";

    private Reflections mReflections = new Reflections();

    private MMap mmap = new MMap();
    
    private MMap amap = new MMap();

    private String mPrtNo = "";

    private LJSPaySet outLJSPaySet = new LJSPaySet();

    private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();

    private LBTempFeeSet outLBTempFeeSet = new LBTempFeeSet();

    private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();

    private LBTempFeeClassSet outLBTempFeeClassSet = new LBTempFeeClassSet();

    /** 核保主表 */
    private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();

    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();

    /** 核保子表 */
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();

    private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();

    /** 打印管理表 */
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

    private LMUWSet mLMUWSet = new LMUWSet();

    private CalBase mCalBase = new CalBase();

    private GlobalInput mGlobalInput = new GlobalInput();

    private String mGetNoticeNo = "";

    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperater;

    /** 业务数据操作字符串 */
    private String mContNo;

    private String mMissionID;

    /**保单表*/
    private LCContSchema mLCContSchema = new LCContSchema();

    //  /** 打印拒保通知书标志 */
    //  private boolean isRefuse = false;
    /** 打印变更承保通知书 */
    private boolean isModify = false;

    /** 打印撤销申请通知书 */
    private boolean isPrint = false;

    /**
     * 是否转向客户回复岗位
     * 1-需要跳转
     * 0-不需要跳转
     */
    private String mNeedWriteBack = "0";

    /** 如果是0 表示不需要发放，如果是1表示需要发放 */
    private String mNeedSendNotice;

    public UWConfirmAfterInitService()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        //生成给付通知书号
        String tLimit = PubFun.getNoLimit(mManageCom);
        mGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号
        System.out.println("---tLimit---" + tLimit);

        //拒保、延期或撤单时，校验银行在途数据
        if (mUWFlag.equals("a") || mUWFlag.equals("1") || mUWFlag.equals("8"))
        {
            //查询应收总表数据
            SendtoFirstPayNotice = "false";
            String strSql = "select * from ljspay where trim(otherno) in "
                    + " (select trim(contno) from lccont where prtno='"
                    + mPrtNo + "' " + " union "
                    + " select trim(proposalcontno) from lccont where prtno='"
                    + mPrtNo + "' " + " union "
                    + " select trim(prtno) from lccont where prtno='" + mPrtNo
                    + "' )";
            System.out.println("strSql=" + strSql);
            outLJSPaySet = (new LJSPayDB()).executeQuery(strSql);

            for (int i = 0; i < outLJSPaySet.size(); i++)
            {
                if (outLJSPaySet.get(i + 1).getBankOnTheWayFlag().equals("1"))
                {
                    System.out.println("有银行在途数据，不允许拒保、延期或撤单!");
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "UWManuNormChkBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "有银行在途数据，不允许拒保、延期或撤单!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            
            MMap tmap = new MMap();
            tmap.put("update ljspay set CanSendBank = '1' where otherno = '"
                     + mPrtNo + "' and (BankOnTheWayFlag is null or BankOnTheWayFlag = '0')", "UPDATE");
        	VData mVData = new VData();
            mVData.add(tmap);
            PubSubmit mPubSubmit = new PubSubmit();
            if (!mPubSubmit.submitData(mVData, "UPDATE"))
            {
                this.mErrors.copyAllErrors(mPubSubmit.mErrors);
                return false;
            }
        	//删除非银行在途，且锁定的财务应收记录
            SSRS getNoticeNo = new ExeSQL().execSQL("select distinct a.tempfeeno from ljtempfee "
                                    + " a left join ljspay b "
                                    + " on a.tempfeeno = b.getnoticeno "
                                    + " and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag = '0') "
                                    + " and b.OtherNoType in ('9','16') "
                                    + " and b.CanSendBank = '1' "
                                    + " where a.otherno='"
                                    + mPrtNo+"'"
                                    + " and a.enteraccdate is null "
                                    );
             for (int i = 1; i <= getNoticeNo.MaxRow; i++) {
                 String getNoticeNoString = getNoticeNo.GetText(i, 1);
                 amap.put("delete from LJSPayB where GetNoticeNo = '"
                          + getNoticeNoString + "'", "DELETE");
                 //备份到B表
                 amap.put("insert into LJSPayB (select GetNoticeNo, OtherNo, OtherNoType, AppntNo, SumDuePayMoney, PayDate, "
                          + "BankOnTheWayFlag, BankSuccFlag, SendBankCount, ApproveCode, ApproveDate, SerialNo, Operator, MakeDate, "
                          + "MakeTime, ModifyDate, ModifyTime, ManageCom, AgentCom, AgentType, BankCode, BankAccNo, RiskCode, "
                          + "AgentCode, AgentGroup, AccName, StartPayDate, PayTypeFlag, '7', '', '1',markettype,salechnl "
                          + "from LJSPay where GetNoticeNo = '"
                          + getNoticeNoString + "')","INSERT");
                 //删除未到帐的财务暂收记录
                 amap.put("delete from LJSPay where GetNoticeNo = '"
                          + getNoticeNoString + "'", "DELETE");
                 amap.put("delete from ljtempfeeclass where tempfeeno = '"
                          + getNoticeNoString+"'"+" and EnterAccDate is null ", "DELETE");
                 amap.put("delete from ljtempfee where tempfeeno = '"
                          + getNoticeNoString + "'"+" and OtherNoType = '4' and EnterAccDate is null ", "DELETE");
             }//end modify

            //查询暂交费表数据
            strSql = "select * from ljtempfee where EnterAccDate is not null and otherno=(select proposalcontno from lccont where prtno='"
                    + mPrtNo
                    + "')"
                    + " union "
                    + " select * from ljtempfee where EnterAccDate is not null and otherno=(select prtno from lccont where prtno='"
                    + mPrtNo + "')";
            //                    "select * from ljtempfee where EnterAccDate is not null and trim(otherno) in "
            //                    + " (select trim(contno) from lccont where prtno='" +
            //                    mPrtNo + "' "
            //                    + " union "
            //                    + " select trim(proposalcontno) from lccont where prtno='" +
            //                    mPrtNo + "' "
            //                    + " union "
            //                    + " select trim(prtno) from lccont where prtno='" + mPrtNo +
            //                    "' )";
            System.out.println("strSql=" + strSql);
            outLJTempFeeSet = (new LJTempFeeDB()).executeQuery(strSql);

            if (outLJTempFeeSet.size() > 0)
            {
                for (int i = 0; i < outLJTempFeeSet.size(); i++)
                {
                    LBTempFeeSchema tLBTempFeeSchema = new LBTempFeeSchema();
                    mReflections.transFields(tLBTempFeeSchema, outLJTempFeeSet
                            .get(i + 1));
                    tLBTempFeeSchema.setBackUpSerialNo(PubFun1.CreateMaxNo(
                            "LBTempFee", 20));
                    outLBTempFeeSet.add(tLBTempFeeSchema);

                    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
                    tLJTempFeeClassDB.setTempFeeNo(outLJTempFeeSet.get(i + 1)
                            .getTempFeeNo());
                    outLJTempFeeClassSet.add(tLJTempFeeClassDB.query());
                }

                for (int i = 0; i < outLJTempFeeClassSet.size(); i++)
                {
                    LBTempFeeClassSchema tLBTempFeeClassSchema = new LBTempFeeClassSchema();
                    mReflections.transFields(tLBTempFeeClassSchema,
                            outLJTempFeeClassSet.get(i + 1));
                    tLBTempFeeClassSchema.setBackUpSerialNo(PubFun1
                            .CreateMaxNo("LBTFClass", 20));
                    outLBTempFeeClassSet.add(tLBTempFeeClassSchema);
                }
            }
        }

        if (mUWFlag.equals("1") || mUWFlag.equals("2") || mUWFlag.equals("4")
                || mUWFlag.equals("9"))
        {
            //次标准体校验核保员级别
            if (!checkStandGrade())
            {
                return false;
            }
        }

        //拒保或延期要校验核保员级别
        if (mUWFlag.equals("1") || mUWFlag.equals("2"))
        {
            if (!checkUserGrade())
            {
                return false;
            }
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //撤单退费处理
        System.out.println("Start Return Tempfee");
        if (mUWFlag.equals("a") || mUWFlag.equals("1") || mUWFlag.equals("8"))
        {
            //            if (!returnFee())
            //            {
            //                return false;
            //            }
        }

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start  Submit...");

        //mResult.clear();
        return true;
    }

    /**
     * 数据操作撤单业务处理 输出：如果出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean returnFee()
    {
        System.out.println("In ReturnFee");

        String payMode = ""; //交费方式
        String BankCode = ""; //银行编码
        String BankAccNo = ""; //银行账号
        String AccName = ""; //户名

        //准备TransferData数据
        String strSql = "";

        //测试该投保单是否有暂交费待退
        strSql = "select * from ljtempfee where trim(otherno) in ( "
                + "  select '" + mPrtNo + "' from dual )"
                + " and EnterAccDate is not null " + " and confdate is  null";

        LJTempFeeDB sLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet sLJTempFeeSet = new LJTempFeeSet();
        sLJTempFeeSet = sLJTempFeeDB.executeQuery(strSql);
        System.out.println("暂交费数量:  " + sLJTempFeeSet.size());
        if (sLJTempFeeSet.size() == 0)
        {
            System.out.println("Out ReturnFee");
            return true;
        }

        //如果通知书号不为空，找出退费方式（优先级依次为支票，银行，现金）
        GetPayType tGetPayType = new GetPayType();
        if (tGetPayType.getPayTypeForLCPol(mPrtNo) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGetPayType.mErrors);
            System.out.println("查询出错信息  :" + tGetPayType.mErrors);
            return false;
        }
        else
        {
            System.out.println("BankCode:  " + tGetPayType.getPayMode());
            System.out.println("AccNo" + tGetPayType.getBankCode());
            System.out.println("AccName" + tGetPayType.getBankAccNo());

            payMode = tGetPayType.getPayMode(); //交费方式
            BankCode = tGetPayType.getBankCode(); //银行编码
            BankAccNo = tGetPayType.getBankAccNo(); //银行账号
            AccName = tGetPayType.getAccName(); //户名
        }

        TransferData sTansferData = new TransferData();
        sTansferData.setNameAndValue("PayMode", payMode);
        sTansferData.setNameAndValue("NotBLS", "1");
        if (payMode.equals("1"))
        {
            sTansferData.setNameAndValue("BankFlag", "0");
        }
        else
        {
            sTansferData.setNameAndValue("BankCode", BankCode);
            sTansferData.setNameAndValue("AccNo", BankAccNo);
            sTansferData.setNameAndValue("AccName", AccName);
            sTansferData.setNameAndValue("BankFlag", "1");
        }
        //    String tLimit = PubFun.getNoLimit(sLJTempFeeClassSet.get(1).getManageCom());
        sTansferData.setNameAndValue("GetNoticeNo", mGetNoticeNo);

        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();

        for (int index = 1; index <= sLJTempFeeSet.size(); index++)
        {
            System.out.println("HaveDate In Second1");

            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(sLJTempFeeSet.get(index)
                    .getTempFeeNo());
            tLJTempFeeSchema.setTempFeeType(sLJTempFeeSet.get(index)
                    .getTempFeeType());
            tLJTempFeeSchema
                    .setRiskCode(sLJTempFeeSet.get(index).getRiskCode());
            tLJTempFeeSet.add(tLJTempFeeSchema);

            LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
            //tLJAGetTempFeeSchema.setGetReasonCode(mAllLCUWMasterSet.get(1).getUWIdea());
            tLJAGetTempFeeSchema.setGetReasonCode("99");

            tLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);

            System.out.println("HaveDate In Second2");
            try
            {
                System.out.println("TempFeeNo:  "
                        + sLJTempFeeSet.get(index).getTempFeeNo());
                System.out.println("TempFeeType:  "
                        + sLJTempFeeSet.get(index).getTempFeeType());
                System.out.println("RiskCode:  "
                        + sLJTempFeeSet.get(index).getRiskCode());
            }
            catch (Exception e)
            {
                System.out.println("无银行数据!");
            }
        }

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLJTempFeeSet);
        tVData.add(tLJAGetTempFeeSet);
        tVData.add(sTansferData);
        tVData.add(mGlobalInput);

        // 数据传输
        System.out.println("--------开始传输数据---------");
        TempFeeWithdrawBL tTempFeeWithdrawBL = new TempFeeWithdrawBL();
        if (tTempFeeWithdrawBL.submitData(tVData, "INSERT") == true)
        {
            System.out.println("---ok---");
        }
        else
        {
            System.out.println("---NO---");
        }

        if (tTempFeeWithdrawBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tTempFeeWithdrawBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "核保成功,但退费失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mReturnFeeData = tTempFeeWithdrawBL.getResult();

        mmap = (MMap) mReturnFeeData.getObjectByObjectName("MMap", 0);

        System.out.println("Out ReturnFee");
        return true;
    }

    /**
     * 次标准体校验核保员级别 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean checkStandGrade()
    {
        CheckKinds("1");
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        mLCPolSet = tLCPolDB.query();
        //准备险种保单的复核标志
        for (int j = 1; j < mLCPolSet.size(); j++)
        {
            mOldPolNo = mLCPolSet.get(j).getProposalNo();

            LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();

            tLCUWMasterDB.setProposalNo(mOldPolNo);

            if (tLCUWMasterDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "checkStandGrade";
                tError.errorMessage = "LCUWMaster表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                tLCUWMasterSchema = tLCUWMasterDB.getSchema();
            }

            //有特约，加费，保险计划变更为次标准体
            if (!tLCUWMasterSchema.getSpecFlag().equals("0")
                    || !tLCUWMasterSchema.getChangePolFlag().equals("0"))
            {
                if (mLMUWSet.size() > 0)
                {
                    for (int i = 1; i <= mLMUWSet.size(); i++)
                    {
                        LMUWSchema tLMUWSchema = new LMUWSchema();
                        tLMUWSchema = mLMUWSet.get(i);

                        mCalCode = tLMUWSchema.getCalCode(); //次标准体计算公式代码
                        String tempuwgrade = CheckPol();
                        if (tempuwgrade != null)
                        {
                            if (mUWPopedom.compareTo(tempuwgrade) < 0)
                            {
                                CError tError = new CError();
                                tError.moduleName = "UWManuNormChkBL";
                                tError.functionName = "prepareAllPol";
                                tError.errorMessage = "无此次标准体投保件核保权限，需要上报上级核保师!";
                                this.mErrors.addOneError(tError);
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * 核保险种信息校验,准备核保算法 输出：如果发生错误则返回false,否则返回true
     *
     * @param tFlag String
     * @return boolean
     */
    private boolean CheckKinds(String tFlag)
    {
        mLMUWSet.clear();
        LMUWSchema tLMUWSchema = new LMUWSchema();
        //查询算法编码
        if (tFlag.equals("1"))
        {
            tLMUWSchema.setUWType("13"); //非标准体
        }

        if (tFlag.equals("2"))
        {
            tLMUWSchema.setUWType("14"); //拒保延期
        }

        tLMUWSchema.setRiskCode("000000");
        tLMUWSchema.setRelaPolType("I");

        LMUWDB tLMUWDB = new LMUWDB();
        tLMUWDB.setSchema(tLMUWSchema);

        mLMUWSet = tLMUWDB.query();
        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "CheckKinds";
            tError.errorMessage = "核保级别校验算法读取失败!";
            this.mErrors.addOneError(tError);
            //mLMUWDBSet.clear();
            return false;
        }
        return true;
    }

    /**
     * 拒保，撤单校验核保员级别 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean checkUserGrade()
    {
        CheckKinds("2");

        if (mLMUWSet.size() > 0)
        {
            for (int i = 1; i <= mLMUWSet.size(); i++)
            {
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = mLMUWSet.get(i);

                mCalCode = tLMUWSchema.getCalCode(); //延期拒保计算公式代码
                String tempuwgrade = CheckPol();
                if (tempuwgrade != null)
                {
                    if (mUWPopedom.compareTo(tempuwgrade) < 0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "UWManuNormChkBL";
                        tError.functionName = "prepareAllPol";
                        tError.errorMessage = "无此单拒保，延期权限，需上报上级核保师!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * 个人单核保 输出：如果发生错误则返回false,否则返回true
     *
     * @return String
     */
    private String CheckPol()
    {
        //准备数据
        CheckPolInit(mLCPolSchema);

        String tUWGrade = "";

        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
        //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
        //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", mLCPolSchema.getInsuredNo());
        ;

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals(""))
        {
            tUWGrade = "";
        }
        else
        {
            tUWGrade = tStr.trim();
        }

        System.out.println("AmntGrade:" + tUWGrade);

        return tUWGrade;
    }

    /**
     * 个人单核保数据准备 输出：如果发生错误则返回false,否则返回true
     *
     * @param tLCPolSchema LCPolSchema
     */
    private void CheckPolInit(LCPolSchema tLCPolSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCPolSchema.getPrem());
        mCalBase.setGet(tLCPolSchema.getAmnt());
        mCalBase.setMult(tLCPolSchema.getMult());
        //mCalBase.setYears( tLCPolSchema.getYears() );
        mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setJob(tLCPolSchema.getOccupationType());
        mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
        mCalBase.setPolNo(tLCPolSchema.getPolNo());
    }

    /**
     * 准备返回前台统一存储数据 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        //        mResult.add(mSplitMap);

        MMap map = new MMap();

        map.put(mLCContSchema, "UPDATE");
        map.put(mLCPolSet, "UPDATE");
        map.put(mLCCUWMasterSet, "UPDATE");
        map.put(mLCCUWSubSet, "INSERT");
        //        map.put(mLCUWMasterSet, "UPDATE");
        //        map.put(mLCUWSubSet, "INSERT");
        map.put(outLJSPaySet, "DELETE");
        //20041207待测试
        //    map.put(outLJTempFeeSet, "DELETE");
        //        map.put(outLBTempFeeSet, "INSERT");
        //    map.put(outLJTempFeeClassSet, "DELETE");
        //        map.put(outLBTempFeeClassSet, "INSERT");
        map.put(mLOPRTManagerSet, "INSERT");

        map.add(mmap);
        map.add(amap);
        map.add(mSplitMap);

        mResult.add(map);
        return true;
    }

    /**
     * 校验业务数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        //校验核保员级别
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mOperater);
        System.out.println("mOperate" + mOperater);
        if (!tLDUserDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperater + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tUWPopedom = tLDUserDB.getUWPopedom();
        mUWPopedom = tUWPopedom;
        mAppGrade = mUWPopedom;

        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setUserCode(mOperater);
        tLDUWUserDB.setUWType("01");
        if (!tLDUWUserDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此核保师信息，不能核保!（操作员：" + mOperater + "）";
            this.mErrors.addOneError(tError);
            return false;
        }
        mOperatorUWGrade = tLDUWUserDB.getUWPopedom();

        //校验保单信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setProposalContNo(mContNo);
        //        if (!tLCContDB.getInfo()) {
        //            CError tError = new CError();
        //            tError.moduleName = "UWRReportAfterInitService";
        //            tError.functionName = "checkData";
        //            tError.errorMessage = "保单" + mContNo + "信息查询失败!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() <= 0 || tLCContSet.size() > 1)
        {
            buildError("checkData", "查询合同信息失败！");
            return false;
        }
        mLCContSchema.setSchema(tLCContSet.get(1).getSchema());

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        mLCPolSet = tLCPolDB.query();

        ExeSQL tExeSQL;
        String tSql = "";
        String rs = "";
        //准备险种保单的复核标志
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            //核保结论是8 代表延期,如果是延期处理,效果同拒保,但在程序
            //后面自动发首期交费通知书时会自动发拒保延期通知书
            if (!mLCPolSet.get(i).getUWFlag().equals("1")
                    && !mLCPolSet.get(i).getUWFlag().equals("4")
                    && !mLCPolSet.get(i).getUWFlag().equals("9")
                    && !mLCPolSet.get(i).getUWFlag().equals("8")
                    && !mLCPolSet.get(i).getUWFlag().equals("a"))
            {
                tExeSQL = new ExeSQL();
                //校验此保单被保人是否暂停。如果暂停则不校验是否以下核保结论
                tSql = "select INSUREDSTAT from lcinsured where 1=1 "
                        + " and ContNo = '" + this.mLCContSchema.getContNo()
                        + "'" + " and insuredno = '"
                        + mLCPolSet.get(i).getInsuredNo() + "'";
                try
                {
                    rs = tExeSQL.getOneValue(tSql);
                }
                catch (Exception e)
                {
                    rs = "";
                }
                if (rs != null && rs.length() > 0 && rs.equals("1"))
                {
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "UWRReportAfterInitService";
                    tError.functionName = "checkData";
                    tError.errorMessage = "保单下险种未下核保结论";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWConfirmAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if (mOperater == null || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的mCont
        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (mContNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中ContNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的mPrtNo
        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中PrtNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mUWFlag = (String) mTransferData.getValueByName("UWFlag");
        if (mUWFlag == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中UWFlag失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mUWIdea = (String) mTransferData.getValueByName("UWIdea");
        if (mUWIdea == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWConfirmAfterInitService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中UWIdea失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mNeedSendNotice = (String) mTransferData
                .getValueByName("NeedSendNotice");
        if (mNeedSendNotice == null || mNeedSendNotice.equals(""))
        {
            buildError("getInputData", "没有受到前台传入的是否发放通知书的标志！");
            return false;
        }
        return true;
    }

    /**
     * stopInsured
     * 暂停被保人
     * @return boolean
     */
    private boolean stopInsured()
    {
        SplitFamilyBL tSplitFamilyBL = new SplitFamilyBL();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ContNo", mContNo);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        tSplitFamilyBL.submitData(tVData, "");

        if (tSplitFamilyBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tSplitFamilyBL.mErrors);
            return false;
        }
        else
        {
            mNewContNo = (String) tSplitFamilyBL.getResult().getObject(0);
            System.out.println("NewContNo==" + mNewContNo);
            mSplitMap = (MMap) tSplitFamilyBL.getResult()
                    .getObjectByObjectName("MMap", 0);
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        int tflag = 0;
        //准备合同表数据
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setProposalContNo(mContNo);
        //        if (!tLCContDB.getInfo()) {
        //            CError tError = new CError();
        //            tError.moduleName = "UWRReportAfterInitService";
        //            tError.functionName = "checkData";
        //            tError.errorMessage = "合同" + mContNo + "信息查询失败!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        LCContSet tLCContSet = tLCContDB.query();
        if (tLCContSet.size() <= 0 || tLCContSet.size() > 1)
        {
            buildError("checkData", "查询合同信息失败！");
            return false;
        }
        mLCContSchema.setSchema(tLCContSet.get(1).getSchema());
        //准备保单的复核标志
        mLCContSchema.setUWFlag(mUWFlag);
        if (StrTool.cTrim(mLCContSchema.getUWDate()).equals(""))
        {
            mLCContSchema.setUWDate(PubFun.getCurrentDate());
        }
        //险种核保结论和合同核保结论分开下

        LCPolDB mLCPolDB = new LCPolDB();
        mLCPolDB.setContNo(mLCContSchema.getContNo());
        mLCPolSet = mLCPolDB.query();
        //准备险种保单的复核标志
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            if (StrTool.cTrim(mLCPolSet.get(i).getUWDate()).equals(""))
            {
                mLCPolSet.get(i).setUWDate(PubFun.getCurrentDate());
            }
        }

        //准备合同复核表数据
        if (prepareContUW() == false)
        {
            return false;
        }
        //准备险种复核表数据
        //        if (prepareAllUW() == false)
        //            return false;

        //不是团体下个人单
        if (!mPolType.equals("2"))
        {
            // 由于此时数据尚未提交数据库，因此，暂时根据险种核保情况来判断整单核保结论。
            boolean tTmpContUWFlag = false;//承保或变更承保为：true；否则为：false
            for (int idx = 1; idx <= this.mLCPolSet.size(); idx++)
            {
                String tTmpPolUWFlag = mLCPolSet.get(idx).getUWFlag();

                if ("4".equals(tTmpPolUWFlag) || "9".equals(tTmpPolUWFlag))
                {
                    tTmpContUWFlag = true;
                    break;
                }
            }
            // ----------------------------------

            /** 拒保延期撤单 根据险种核保结论判断,不是整单核保结论 */
            for (int i = 1; i <= this.mLCPolSet.size(); i++)
            {
                String tUWFlag = mLCPolSet.get(i).getUWFlag();
                if (tUWFlag.equals("1") || tUWFlag.equals("2")
                        || tUWFlag.equals("8") || tUWFlag.equals("a")
                        || tUWFlag.equals("9") || tUWFlag.equals("4"))
                {
                    if (print(tUWFlag, tTmpContUWFlag) == false)
                    {
                        return false;
                    }
                }
            }
        }
        //解决不能下发首期交费通知书的普遍问题
        int flag = 0;
        if (StrTool.cTrim(mUWFlag).equals("4"))
        {
            if (mLOPRTManagerSet.size() > 0)
            {
                for (int j = 1; j <= mLOPRTManagerSet.size(); j++)
                {
                    if (!StrTool.cTrim(mLOPRTManagerSet.get(j).getCode())
                            .equals("05"))
                    {
                        flag++;
                    }
                }
                if (mLOPRTManagerSet.size() == flag)
                {
                    LCPolDB tLCPolDB = new LCPolDB();
                    LCPolSet tLCPolSet = new LCPolSet();
                    tLCPolDB.setPrtNo(mLCContSchema.getPrtNo());
                    tLCPolSet = tLCPolDB.query();
                    //System.out.println("mNeedSendNotice"+mNeedSendNotice);
                    if (tLCPolSet.size() > 0)
                    {
                        for (int i = 0; i < tLCPolSet.size(); i++)
                        {
                            if (StrTool.cTrim(tLCPolSet.get(i + 1).getUWFlag())
                                    .equals("4")
                                    || StrTool.cTrim(
                                            tLCPolSet.get(i + 1).getUWFlag())
                                            .equals("9"))
                            {
                                SendtoFirstPayNotice = "true";
                                break;
                            }
                        }
                    }
                }

            }
            if (mLOPRTManagerSet.size() <= 0)
            {
                LCPolDB tLCPolDB = new LCPolDB();
                LCPolSet tLCPolSet = new LCPolSet();
                tLCPolDB.setPrtNo(mLCContSchema.getPrtNo());
                tLCPolSet = tLCPolDB.query();
                System.out.println("mNeedSendNotice" + mNeedSendNotice);
                if (tLCPolSet.size() > 0)
                {
                    for (int i = 0; i < tLCPolSet.size(); i++)
                    {
                        if (StrTool.cTrim(tLCPolSet.get(i + 1).getUWFlag())
                                .equals("4")
                                || StrTool.cTrim(
                                        tLCPolSet.get(i + 1).getUWFlag())
                                        .equals("9"))
                        {
                            SendtoFirstPayNotice = "true";
                            break;
                        }
                    }
                }

            }

        }

        /**
         * 添加核保结论是9 自动发首期交费通知书
         */
        System.out.println("SendtoFirstPayNotice" + SendtoFirstPayNotice);
        if ("9".equals(mUWFlag)
                || StrTool.cTrim(SendtoFirstPayNotice).equals("true"))
        {
            //if (checkCanSendFirstPay())
            //{

            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode("07");
            //校验是否发过首期交费通知书 ，如果发过则不再重发
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
            if (tLOPRTManagerDB.query().size() <= 0)
            {
                VData tempVData = new VData();
                tempVData.add(tLOPRTManagerSchema);
                tempVData.add(mGlobalInput);

                UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
                if (!tUWSendPrintUI.submitData(tempVData, "INSERT"))
                {
                    this.mErrors.copyAllErrors(tUWSendPrintUI.mErrors);
                    return false;
                }
            }
            //}
        }

        if (mUWFlag.equals("2"))
        {
            TimeAccept();
        }
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(this.mLCContSchema.getContNo());
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet == null || tLCInsuredSet.size() <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "UWRReportAfterInitService";
            tError.functionName = "dealData";
            tError.errorMessage = "被保人信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            if (tLCInsuredSet.get(i).getInsuredStat() != null
                    && tLCInsuredSet.get(i).getInsuredStat().length() > 0
                    && tLCInsuredSet.get(i).getInsuredStat().equals("1"))
            {
                tflag = 1;
            }
        }
        if (tflag == 1)
        {
            mStopFlag = "1";
            if (!stopInsured())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 延期承保 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean TimeAccept()
    {
        Date temp;
        String temp1 = "D";
        Date temp2;

        FDate tFDate = new FDate();
        temp = null;
        temp2 = tFDate.getDate(mLCPolSchema.getCValiDate());
        //delete by yt 20030719,将延期日期的类型修改为字符串，该部分代码不再有效
        //    mvalidate = PubFun.calDate(temp2,mpostday,temp1,temp);

        //System.out.println("---TimeAccept---");
        //mLCPolSchema.setCValiDate(mvalidate);
        //System.out.println("---mvalidate---"+mvalidate);
        return true;
    }

    /**
     * 打印信息表
     *
     * @return boolean
     * @param mUWFlag String
     */
    private boolean print(String printUWFlag)
    {
        String tPrtSeq = "";
        String tLimit = this.mPrtNo;
        System.out.println("生成号参数2 : " + this.mPrtNo);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(this.mLCContSchema.getProposalContNo());
        System.out.println("ContNo:" + mContNo);
        tLOPRTManagerSchema.setManageCom(mLCPolSchema.getManageCom());
        tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        tLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT);
        tLOPRTManagerSchema.setReqOperator(mOperator);
        tLOPRTManagerSchema.setReqCom(mManageCom);
        tLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        tLOPRTManagerSchema.setStateFlag("0"); //打印标志“0”表示未打
        //    if (mUWFlag.equals("1")&&!this.isRefuse)
        //    { //拒保通知书
        //      this.isRefuse = true;
        //      tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tLimit);
        //      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_DECLINE);
        //      tLOPRTManagerSchema.setStandbyFlag1(mContNo);
        //      System.out.println("生成号 : "+tPrtSeq);
        //      tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
        //      tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
        //      tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        //      tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        //      mLOPRTManagerSet.add(tLOPRTManagerSchema);
        //    }
        //    if (mUWFlag.equals("8")&&!this.isDefer)
        //    { //延期通知书
        //      this.isDefer = true;
        //      tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tLimit);
        //      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_DEFER);
        //      tLOPRTManagerSchema.setStandbyFlag1(mContNo);
        //      tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
        //      System.out.println("生成号 : "+tPrtSeq);
        //      tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
        //      tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        //      tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        //      mLOPRTManagerSet.add(tLOPRTManagerSchema);
        //    }
        //生成拒保延期撤销申请通知书
        //        if ((printUWFlag.equals("1") || printUWFlag.equals("2") ||
        //             printUWFlag.equals("8") || printUWFlag.equals("a")) && !this.isPrint) { //撤单
        //            this.isPrint = true;
        //            tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tLimit);
        //            if (isModify) {
        //                String sub = tPrtSeq.substring(tPrtSeq.length() - 2);
        //                String modify = String.valueOf(Integer.parseInt(sub) + 1);
        //                if (modify.length() == 1) {
        //                    modify = "0" + modify;
        //                }
        //                tPrtSeq = tPrtSeq.substring(0, tPrtSeq.length() - 2) + modify;
        //            }
        //            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_DEFER);
        //            tLOPRTManagerSchema.setStandbyFlag1(this.mLCContSchema.
        //                                                getProposalContNo());
        //            tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
        //            System.out.println("生成号 : " + tPrtSeq);
        //            tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
        //            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        //            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        //            mLOPRTManagerSet.add(tLOPRTManagerSchema.getSchema());
        //        }
        if ((printUWFlag.equals("4") || printUWFlag.equals("1")
                || printUWFlag.equals("2") || printUWFlag.equals("8") || printUWFlag
                .equals("a"))
                && !this.isModify && mNeedSendNotice.equals("1"))
        { //撤单
            this.isModify = true;
            this.mNeedWriteBack = "1";
            tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tLimit);
            if (isPrint)
            {
                String sub = tPrtSeq.substring(tPrtSeq.length() - 2);
                String modify = String.valueOf(Integer.parseInt(sub) + 1);
                if (modify.length() == 1)
                {
                    modify = "0" + modify;
                }
                tPrtSeq = tPrtSeq.substring(0, tPrtSeq.length() - 2) + modify;
            }

            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_UW);
            tLOPRTManagerSchema.setStandbyFlag1(this.mLCContSchema
                    .getProposalContNo());
            tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
            System.out.println("生成号 : " + tPrtSeq);
            tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSet.add(tLOPRTManagerSchema.getSchema());
            /** 有下发单证问题 */
            if (!sendSysCertSendOut(tLOPRTManagerSchema))
            {
                return false;
            }
            if (!dealUWInfo())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 打印信息表
     *
     * @return boolean
     * @param mUWFlag String
     */
    private boolean print(String printUWFlag, boolean cContUWPrintFlag)
    {
        String tPrtSeq = "";
        String tLimit = this.mPrtNo;
        System.out.println("生成号参数2 : " + this.mPrtNo);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(this.mLCContSchema.getProposalContNo());
        System.out.println("ContNo:" + mContNo);
        tLOPRTManagerSchema.setManageCom(mLCPolSchema.getManageCom());
        tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        tLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT);
        tLOPRTManagerSchema.setReqOperator(mOperator);
        tLOPRTManagerSchema.setReqCom(mManageCom);
        tLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        tLOPRTManagerSchema.setStateFlag("0"); //打印标志“0”表示未打
        if ((printUWFlag.equals("4") || printUWFlag.equals("1")
                || printUWFlag.equals("2") || printUWFlag.equals("8") || printUWFlag
                .equals("a"))
                && !this.isModify && mNeedSendNotice.equals("1"))
        { //撤单
            this.isModify = true;
            this.mNeedWriteBack = "1";
            tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tLimit);
            if (isPrint)
            {
                String sub = tPrtSeq.substring(tPrtSeq.length() - 2);
                String modify = String.valueOf(Integer.parseInt(sub) + 1);
                if (modify.length() == 1)
                {
                    modify = "0" + modify;
                }
                tPrtSeq = tPrtSeq.substring(0, tPrtSeq.length() - 2) + modify;
            }

            // 根据通知书标志，下发核保或拒保延期通知书。
            if (cContUWPrintFlag)
            {
                tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_UW);
            }
            else
            {
                tLOPRTManagerSchema.setCode("05_T");
            }
            // -----------------------------------

            tLOPRTManagerSchema.setStandbyFlag1(this.mLCContSchema
                    .getProposalContNo());
            tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
            System.out.println("生成号 : " + tPrtSeq);
            tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTManagerSet.add(tLOPRTManagerSchema.getSchema());
            /** 有下发单证问题 */
            if (!sendSysCertSendOut(tLOPRTManagerSchema))
            {
                return false;
            }
            if (!dealUWInfo())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * dealUWInfo
     *
     * @return boolean
     */
    private boolean dealUWInfo()
    {
        for (int i = 1; i <= this.mLCCUWMasterSet.size(); i++)
        {
            mLCCUWMasterSet.get(i).setPrintFlag("2");
            mLCCUWMasterSet.get(i).setChangePolFlag("2");
        }
        return true;
    }

    /**
     * sendSysCertSendOut
     *
     * @return boolean
     * @param tLOPRTManagerSchema LOPRTManagerSchema
     */
    private boolean sendSysCertSendOut(LOPRTManagerSchema tLOPRTManagerSchema)
    {
        /** 调用工作流的接口程序 */
        System.out.println("调用工作流接口执行单证下发....");
        VData tInputData = new VData();

        mTransferData.setNameAndValue("Code", tLOPRTManagerSchema.getCode());
        mTransferData
                .setNameAndValue("PrtSeq", tLOPRTManagerSchema.getPrtSeq());
        tInputData.add(tLOPRTManagerSchema);
        tInputData.add(mTransferData);
        tInputData.add(mLCCUWMasterSet);
        tInputData.add(mGlobalInput);

        LWActivityDB tLWActivityDB = new LWActivityDB();
        tLWActivityDB.setActivityID("0000001107");
        if (!tLWActivityDB.getInfo())
        {
            buildError("sendSysCertSendOut", "查询工作流表节点描述信息失败！");
            return false;
        }
        try
        {
            Class tClass = Class.forName(tLWActivityDB.getAfterInit());
            AfterInitService tAfterInitService = (AfterInitService) tClass
                    .newInstance();
            if (!tAfterInitService.submitData(tInputData, "WORKFLAG"))
            {
                this.mErrors.copyAllErrors(tAfterInitService.getErrors());
                return false;
            }
            this.mmap.put(tAfterInitService.getResult().getObjectByObjectName(
                    "LZSysCertifySchema", 0), "INSERT");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("sendSysCertSendOut", "执行下发单证错误！" + ex.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 测试程序 为了测试 sendSysCertSendOut()方法
     * @return boolean
     */
    public boolean testSendSysCertSendOut()
    {

        mGlobalInput.Operator = "";
        mGlobalInput.ManageCom = "";
        mGlobalInput.ComCode = "";
        String tContNo = "";
        String tPrtNo = "";
        String tManageCom = "";
        String tAgentCode = "";

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo())
        {
            return false;
        }
        this.mLCContSchema = tLCContDB.getSchema();
        
        //by gzh 20111207 人工核保置核保人员
        if(mLCContSchema.getUWOperator() == null || "".equals(mLCContSchema.getUWOperator())){
        	mLCContSchema.setUWOperator(mOperater);
        }
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(tContNo);
        tLOPRTManagerSchema.setManageCom(tManageCom);
        tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
        tLOPRTManagerSchema.setAgentCode(tAgentCode);
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT);
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setManageCom(tManageCom);
        tLOPRTManagerSchema.setStateFlag("0"); //打印标志“0”表示未打
        String tPrtSeq = PubFun1.CreateMaxNo("UWRESULTNOTICE", tPrtNo);
        tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_UW);
        tLOPRTManagerSchema.setStandbyFlag1(tContNo);
        tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
        tLOPRTManagerSchema.setOldPrtSeq(tPrtSeq);
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        prepareContUW();
        mTransferData
                .setNameAndValue("AgentCode", mLCContSchema.getAgentCode());
        mTransferData.setNameAndValue("AgentGroup", "");
        mTransferData
                .setNameAndValue("ManageCom", mLCContSchema.getManageCom());
        mTransferData.setNameAndValue("PrtNo", mLCContSchema.getPrtNo());
        mTransferData.setNameAndValue("ContNo", mLCContSchema
                .getProposalContNo());
        mTransferData.setNameAndValue("AppntCode", mLCContSchema.getAppntNo());
        mTransferData
                .setNameAndValue("AppntName", mLCContSchema.getAppntName());
        mTransferData.setNameAndValue("UWDate", PubFun.getCurrentDate());
        mTransferData.setNameAndValue("AppntNo", mLCContSchema.getAppntNo());
        mTransferData
                .setNameAndValue("CValiDate", mLCContSchema.getCValiDate());
        mTransferData.setNameAndValue("UWCode", mLCContSchema.getUWOperator());
        mTransferData.setNameAndValue("StopFlag", mStopFlag);
        mTransferData.setNameAndValue("NewContNo", mLCContSchema.getContNo());
        mTransferData.setNameAndValue("NeedWriteBack", mNeedWriteBack);

        return true;
    }

    /**
     * 检查是不是需要送核保通知书到打印队列
     *
     * @return java.lang.String
     * @param tPrintFlag String
     */
    private String checkBackOperator(String tPrintFlag)
    {
        LCPolSet tLCPolSet = new LCPolSet();

        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();

            tLCPolSchema = mLCPolSet.get(i);

            //有返回保户需要打印
            //String tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where ((makedate >= (select max(makedate) from lcissuepol where backobjtype in ('1','4') and ProposalNo = '"+tLCPolSchema.getPolNo()+"' and makedate is not null)) or ((select max(makedate) from lcissuepol where backobjtype in ('1','4') and ProposalNo = '"+tLCPolSchema.getPolNo()+"') is null))"
            String tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where 1 = 1 "
                    + " and backobjtype  = '3'"
                    + " and ProposalNo = '"
                    + tLCPolSchema.getPolNo()
                    + "'"
                    + " and makedate is not null"
                    + " and replyresult is null"
                    + " and needprint = 'Y')";

            System.out.println("printchecksql:" + tsql);
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet t2LCPolSet = new LCPolSet();
            t2LCPolSet = tLCPolDB.executeQuery(tsql);
            if (t2LCPolSet.size() > 0)
            {
                tPrintFlag = "2";
            }
            //只返回给操作员,机构不打印
            //tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where ((makedate >= (select max(makedate) from lcissuepol where backobjtype in ('2','3') and ProposalNo = '"+tLCPolSchema.getPolNo()+"' and makedate is not null)) or ((select max(makedate) from lcissuepol where backobjtype in ('3','2') and ProposalNo = '"+tLCPolSchema.getPolNo()+"') is null))"
            tsql = "select * from lcpol where  ProposalNo in ( select ProposalNo from LCIssuePol where 1 = 1 "
                    + " and backobjtype = '1'"
                    + " and ProposalNo = '"
                    + tLCPolSchema.getPolNo()
                    + "'"
                    + " and makedate is not null"
                    + " and replyresult is null)"
                    + " and ProposalNo not in ( select ProposalNo from LCIssuePol where 1 = 1 "
                    + " and backobjtype in ('2','3')"
                    + " and ProposalNo = '"
                    + tLCPolSchema.getPolNo()
                    + "'"
                    + " and makedate is not null"
                    + " and replyresult is null"
                    + " and needprint = 'Y')"
                    + " and ProposalNo not in ( select ProposalNo from LCIssuePol where 1 = 1 "
                    + " and backobjtype = '4'"
                    + " and ProposalNo = '"
                    + tLCPolSchema.getPolNo()
                    + "'"
                    + " and makedate is not null" + " and replyresult is null)";

            System.out.println("printchecksql2:" + tsql);
            tLCPolDB = new LCPolDB();
            t2LCPolSet = new LCPolSet();

            t2LCPolSet = tLCPolDB.executeQuery(tsql);
            if (t2LCPolSet.size() > 0)
            {
                //复核标记
                tLCPolSchema.setApproveFlag("1");
            }
            tLCPolSet.add(tLCPolSchema);

        }
        mLCPolSet.clear();
        mLCPolSet.add(tLCPolSet);
        if (tPrintFlag.equals("2"))
        {
            tPrintFlag = "0";
        }
        else
        {
            tPrintFlag = "1";
        }
        return tPrintFlag;
    }

    /**
     * 准备主附险核保信息 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareAllUW()
    {
        mLCUWMasterSet.clear();
        mLCUWSubSet.clear();

        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = new LCPolSchema();

            tLCPolSchema = mLCPolSet.get(i);
            LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
            LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCUWMasterDB.setContNo(tLCPolSchema.getContNo());
            LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
            tLCUWMasterSet = tLCUWMasterDB.query();
            if (tLCUWMasterDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWMaster表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            int n = tLCUWMasterSet.size();
            System.out.println("该投保单的核保主表当前记录条数:  " + n);

            tLCUWMasterSchema = tLCUWMasterSet.get(i);

            //为核保订正回退保存核保级别和核保人
            mBackUWGrade = tLCUWMasterSchema.getUWGrade();
            mBackAppGrade = tLCUWMasterSchema.getAppGrade();
            mOperator = tLCUWMasterSchema.getOperator();

            tLCUWMasterSchema.setPassFlag(mUWFlag); //通过标志
            tLCUWMasterSchema.setState(mUWFlag);
            tLCUWMasterSchema.setUWIdea(mUWIdea);
            tLCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
            tLCUWMasterSchema.setGrpContNo(mLCContSchema.getGrpContNo());

            //核保订正
            if (mUWFlag.equals("z"))
            {
                tLCUWMasterSchema.setAutoUWFlag("1");
                tLCUWMasterSchema.setState("5");
                tLCUWMasterSchema.setPassFlag("5");
                //恢复核保级别和核保员
                tLCUWMasterSchema.setUWGrade(mBackUWGrade);
                tLCUWMasterSchema.setAppGrade(mBackAppGrade);
                tLCUWMasterSchema.setOperator(mOperator);
                //解锁
                LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
                tLDSysTraceSchema.setPolNo(mContNo);
                tLDSysTraceSchema.setCreatePos("人工核保");
                tLDSysTraceSchema.setPolState("1001");
                LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
                inLDSysTraceSet.add(tLDSysTraceSchema);

                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.add(inLDSysTraceSet);

                LockTableBL LockTableBL1 = new LockTableBL();
                if (!LockTableBL1.submitData(tVData, "DELETE"))
                {
                    System.out.println("解锁失败！");
                }
            }
            mLCUWMasterSet.add(tLCUWMasterSchema);
            // 核保轨迹表
            LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
            LCUWSubDB tLCUWSubDB = new LCUWSubDB();
            tLCUWSubDB.setContNo(tLCPolSchema.getContNo());
            LCUWSubSet tLCUWSubSet = new LCUWSubSet();
            tLCUWSubSet = tLCUWSubDB.query();
            if (tLCUWSubDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWSub表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            int m = tLCUWSubSet.size();
            System.out.println("subcount=" + m);
            if (m > 0)
            {
                m++; //核保次数
                tLCUWSubSchema = new LCUWSubSchema();
                tLCUWSubSchema.setUWNo(m); //第几次核保
                tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
                tLCUWSubSchema.setContNo(tLCUWMasterSchema.getContNo());
                tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
                tLCUWSubSchema.setPolNo(tLCUWMasterSchema.getPolNo());
                tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema
                        .getProposalContNo());
                if (mUWFlag != null && mUWFlag.equals("z"))
                { //核保订正
                    tLCUWSubSchema.setPassFlag(mUWFlag); //核保意见
                    tLCUWSubSchema.setUWGrade(mUWPopedom); //核保级别
                    tLCUWSubSchema.setAppGrade(mAppGrade); //申请级别
                    tLCUWSubSchema.setAutoUWFlag("2");
                    tLCUWSubSchema.setState(mUWFlag);
                    tLCUWSubSchema.setOperator(mOperater); //操作员
                }
                tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
                tLCUWSubSchema.setOperator(mOperator);
                tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
                tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
                tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBL";
                tError.functionName = "prepareAllUW";
                tError.errorMessage = "LCUWSub表取数失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCUWSubSet.add(tLCUWSubSchema);
        }
        return true;
    }

    /**
     * 准备主附险核保信息 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareContUW()
    {
        mLCCUWMasterSet.clear();
        mLCCUWSubSet.clear();

        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(mLCContSchema.getContNo());
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet = tLCCUWMasterDB.query();
        if (tLCCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWMaster表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int n = tLCCUWMasterSet.size();
        System.out.println("该投保单的核保主表当前记录条数:  " + n);
        if (n == 1)
        {
            tLCCUWMasterSchema = tLCCUWMasterSet.get(1);

            //为核保订正回退保存核保级别和核保人
            mBackUWGrade = tLCCUWMasterSchema.getUWGrade();
            mBackAppGrade = tLCCUWMasterSchema.getAppGrade();
            mOperator = tLCCUWMasterSchema.getOperator();

            //tLCCUWMasterSchema.setUWNo(tLCCUWMasterSchema.getUWNo()+1);核保主表中的UWNo表示该投保单经过几次人工核保(等价于经过几次自动核保次数),而不是人工核保结论(包括核保通知书,上报等)下过几次.所以将其注释.sxy-2003-09-19
            tLCCUWMasterSchema.setPassFlag(mUWFlag); //通过标志
            tLCCUWMasterSchema.setState(mUWFlag);
            tLCCUWMasterSchema.setUWIdea(mUWIdea);
            tLCCUWMasterSchema.setAutoUWFlag("2"); // 1 自动核保 2 人工核保
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());

            //核保订正
            if (mUWFlag.equals("z"))
            {
                tLCCUWMasterSchema.setAutoUWFlag("1");
                tLCCUWMasterSchema.setState("5");
                tLCCUWMasterSchema.setPassFlag("5");
                //恢复核保级别和核保员
                tLCCUWMasterSchema.setUWGrade(mBackUWGrade);
                tLCCUWMasterSchema.setAppGrade(mBackAppGrade);
                tLCCUWMasterSchema.setOperator(mOperator);
                //解锁
                LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
                tLDSysTraceSchema.setPolNo(mContNo);
                tLDSysTraceSchema.setCreatePos("人工核保");
                tLDSysTraceSchema.setPolState("1001");
                LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
                inLDSysTraceSet.add(tLDSysTraceSchema);

                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.add(inLDSysTraceSet);

                LockTableBL LockTableBL1 = new LockTableBL();
                if (!LockTableBL1.submitData(tVData, "DELETE"))
                {
                    System.out.println("解锁失败！");
                }
            }
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWMaster表取数据不唯一!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCCUWMasterSet.add(tLCCUWMasterSchema);

        // 核保轨迹表
        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
        tLCCUWSubDB.setProposalContNo(mLCContSchema.getProposalContNo());
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet = tLCCUWSubDB.query();
        if (tLCCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWSub表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int m = tLCCUWSubSet.size();
        System.out.println("subcount=" + m);
        if (m > 0)
        {
            m++; //核保次数
            tLCCUWSubSchema = new LCCUWSubSchema();
            tLCCUWSubSchema.setUWNo(m); //第几次核保
            tLCCUWSubSchema.setContNo(tLCCUWMasterSchema.getContNo());
            tLCCUWSubSchema.setGrpContNo(tLCCUWMasterSchema.getGrpContNo());
            tLCCUWSubSchema.setProposalContNo(tLCCUWMasterSchema
                    .getProposalContNo());
            tLCCUWSubSchema.setOperator(mOperater);
            if (mUWFlag != null && mUWFlag.equals("z"))
            { //核保订正
                tLCCUWSubSchema.setPassFlag(mUWFlag); //核保意见
                tLCCUWSubSchema.setUWGrade(mUWPopedom); //核保级别
                tLCCUWSubSchema.setAppGrade(mAppGrade); //申请级别
                tLCCUWSubSchema.setAutoUWFlag("2");
                tLCCUWSubSchema.setState(mUWFlag);
                tLCCUWSubSchema.setOperator(mOperater); //操作员
            }

            tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
            tLCCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWManuNormChkBL";
            tError.functionName = "prepareAllUW";
            tError.errorMessage = "LCCUWSub表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCCUWSubSet.add(tLCCUWSubSchema);

        return true;
    }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     *
     * @return boolean
     */
    private boolean prepareTransferData()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (tLAAgentSet == null || tLAAgentSet.size() != 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWRReportAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人表LAAgent查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLAAgentSet.get(1).getAgentGroup() == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWRReportAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人表LAAgent中的代理机构数据丢失!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setAgentGroup(tLAAgentSet.get(1).getAgentGroup());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() != 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWRReportAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人展业机构表LABranchGroup查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLABranchGroupSet.get(1).getBranchAttr() == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWRReportAfterInitService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人展业机构表LABranchGroup中展业机构信息丢失!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mTransferData
                .setNameAndValue("AgentCode", mLCContSchema.getAgentCode());
        mTransferData.setNameAndValue("AgentGroup", tLAAgentSet.get(1)
                .getAgentGroup());
        mTransferData
                .setNameAndValue("ManageCom", mLCContSchema.getManageCom());
        System.out.println("manageCom=" + mManageCom);
        mTransferData.setNameAndValue("PrtNo", mLCContSchema.getPrtNo());
        mTransferData.setNameAndValue("ContNo", mLCContSchema
                .getProposalContNo());
        mTransferData.setNameAndValue("AppntCode", mLCContSchema.getAppntNo());
        mTransferData
                .setNameAndValue("AppntName", mLCContSchema.getAppntName());
        mTransferData.setNameAndValue("UWDate", PubFun.getCurrentDate());
        mTransferData.setNameAndValue("AppntNo", mLCContSchema.getAppntNo());
        mTransferData
                .setNameAndValue("CValiDate", mLCContSchema.getCValiDate());
//      by gzh 20111207 人工核保置核保人员
        if(mLCContSchema.getUWOperator() == null || "".equals(mLCContSchema.getUWOperator())){
        	mLCContSchema.setUWOperator(mOperater);
        }
        mTransferData.setNameAndValue("UWCode", mLCContSchema.getUWOperator());
        mTransferData.setNameAndValue("StopFlag", mStopFlag);
        mTransferData.setNameAndValue("NewContNo", mNewContNo);
        mTransferData.setNameAndValue("NeedWriteBack", mNeedWriteBack);
        return true;
    }

    /**
     * 校验先收费的保单是否需要发送缴费通知书。
     * 目前足额，不发缴费通知书。
     * @return true-需发送；false-不需要发送
     */
    //    private boolean checkCanSendFirstPay()
    //    {
    //        // 只对先收费个单进行校验。
    //        if (!CommonBL.getIsFristPayOfSingleCont(mLCContSchema.getContNo()))
    //            return true;
    //
    //        // 校验到帐保费是否足额。
    //        String tStrSql = "select nvl(sum(prem), 0) from LCPol where uwflag in ('4', '9') "
    //                + " and ContNo = '" + mLCContSchema.getContNo() + "'";
    //        String tPrem = new ExeSQL().getOneValue(tStrSql);
    //        BigDecimal tEnterAccFee = TempFeeAppBL
    //                .getContEnterAccOfTempFee(mLCContSchema.getTempFeeNo());
    //        double tDifPayMoney = tEnterAccFee.subtract(new BigDecimal(tPrem))
    //                .doubleValue();
    //
    //        return tDifPayMoney < 0;
    //    }
    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        UWConfirmAfterInitService tUWConfirmAfterInitService = new UWConfirmAfterInitService();
        VData tVData = new VData();
        TransferData nTransferData = new TransferData();
        nTransferData.setNameAndValue("ContNo", "13000606122");
        nTransferData.setNameAndValue("PrtNo", "16000032452");
        nTransferData.setNameAndValue("UWFlag", "8");
        nTransferData.setNameAndValue("UWIdea", "");
        nTransferData.setNameAndValue("MissionID", "00000000000000021805");
        //System.out.println(request.getParameter("NeedSendNotice"));
        nTransferData.setNameAndValue("NeedSendNotice", "1");
        nTransferData.setNameAndValue("SubMissionID", "1");
        tVData.clear();
        LCContSchema tLCContSchema = new LCContSchema();
        LCContSet tLCContSet = new LCContSet();
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        //	    tLCContSchema.setPolNo( tContNo);
        tLCContSchema.setProposalContNo("13000606122");
        tLCContSchema.setUWFlag("8");
        tLCContSchema.setRemark("");
        tLCCUWMasterSchema.setProposalContNo("13000606122");
        //tLCCUWMasterSchema.setPostponeDay(tvalidate);
        tLCCUWMasterSchema.setUWIdea("");
        tLCCUWMasterSchema.setSugPassFlag("8");
        tLCCUWMasterSchema.setSugUWIdea("");
        tLCContSet.add(tLCContSchema);
        tLCCUWMasterSet.add(tLCCUWMasterSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "UW0007";
        tG.ManageCom = "86";
        tG.ComCode = "86";

        tVData.add(nTransferData);
        tVData.add(tLCCUWMasterSet);
        tVData.add(tG);
        if (tUWConfirmAfterInitService.submitData(tVData, "") == true)
            System.out.println("---ok---");
        else
            System.out.println("---NO---");

    }
}
