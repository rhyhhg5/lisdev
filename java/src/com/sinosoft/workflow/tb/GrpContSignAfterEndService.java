package com.sinosoft.workflow.tb;

import com.sinosoft.lis.db.LCGrpContSubDB;
import com.sinosoft.lis.db.LCProjectInfoDB;
import com.sinosoft.lis.db.LCProjectUWDB;
import com.sinosoft.lis.db.LCProjectYearInfoDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCProjectInfoSchema;
import com.sinosoft.lis.schema.LCProjectTraceSchema;
import com.sinosoft.lis.schema.LCProjectUWSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LCGrpContSubSet;
import com.sinosoft.lis.vschema.LCProjectInfoSet;
import com.sinosoft.lis.vschema.LCProjectUWSet;
import com.sinosoft.lis.vschema.LCProjectYearInfoSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.AfterEndService;


/**
 * <p>Title: 工作流服务类:新契约人工核保 </p>
 * <p>Description: 人工核保工作流AfterEnd服务类 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class GrpContSignAfterEndService implements AfterEndService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private MMap map = new MMap();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();


    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();


    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;


    /** 业务数据操作字符串 */
    private String mMissionID;
    private String mSubMissionID;

    //项目制签单立项状态修改
    private String mProjectNo;

    /** 工作流任务节点表*/
    private LWMissionSchema mLWMissionSchema = new LWMissionSchema();
    private LWMissionSet mLWMissionSet = new LWMissionSet();
    Reflections tReflections = new Reflections();
    public GrpContSignAfterEndService()
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
            return false;

        //校验是否有未打印的体检通知书
        if (!checkData())
            return false;

        //进行业务处理
        if (!dealData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        return true;
    }


    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        

        //添加相关工作流同步执行完毕的任务节点表数据
        if (mLWMissionSet != null && mLWMissionSet.size() > 0)
        {
            map.put(mLWMissionSet, "DELETE");
        }

        mResult.add(map);
        return true;
    }


    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {
        //查询工作流当前任务轨迹表
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LWMissionSet tLWMissionSet = new LWMissionSet();
        tLWMissionDB.setMissionID(mMissionID);
        tLWMissionDB.setActivityID("0000002006");
        tLWMissionDB.setSubMissionID(mSubMissionID);
        tLWMissionSet = tLWMissionDB.query();
        if (tLWMissionSet == null || tLWMissionSet.size() != 1)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "checkData";
            tError.errorMessage = "查询工作流轨迹表LWMission失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLWMissionSchema = tLWMissionSet.get(1);
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if (mOperater == null || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得业务数据
        if (mTransferData == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //核保工作流起始节点状态改变
        if (prepareMission() == false)
            return false;
        if (projectUW()==false){
        	return false;
        }
        return true;
    }


    /**
     * 准备打印信息表
     * @return
     */
    private boolean prepareMission()
    {
        String tStr = "Select * from LWMission where MissionID = '" +
                      mMissionID + "'"
                      + " and ActivityID = '0000002005'"
                      ;
        LWMissionDB tLWMissionDB = new LWMissionDB();
        mLWMissionSet = tLWMissionDB.executeQuery(tStr);
        if (mLWMissionSet == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "prepareMission";
            tError.errorMessage = "工作流起始任务节点查询出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLWMissionSet.size() < 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpContSignAfterEndService";
            tError.functionName = "prepareMission";
            tError.errorMessage = "工作流起始任务节点LWMission查询出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLWMissionSet.add(mLWMissionSchema);
        return true;
    }
    /**
     * add by  zjd 20150130 项目制
     * @return VData
     */
    public boolean projectUW(){
    	ExeSQL tExeSQL=new ExeSQL();
    	String mprtno=mLWMissionSchema.getMissionProp2();
    	String muwoperator=tExeSQL.getOneValue("select UWOperator from LCGrpCont where prtno='"+mprtno+"' ");
    	String tsql="select * from LCGrpContSub where prtno='"+mprtno+"' ";
    	LCGrpContSubDB tLCGrpContSubDB=new LCGrpContSubDB();
    	LCGrpContSubSet tLCGrpContSubSet=tLCGrpContSubDB.executeQuery(tsql);
    	LCProjectUWSet ttLCProjectUW=new LCProjectUWSet();//自动新增的结论
    	if(tLCGrpContSubSet!=null && tLCGrpContSubSet.size()>0){
    		String tprostate="select state from LCProjectInfo where ProjectNo='"+tLCGrpContSubSet.get(1).getProjectNo()+"' ";
    	    mProjectNo=tLCGrpContSubSet.get(1).getProjectNo();
    		String tstate=tExeSQL.getOneValue(tprostate);
    		if(!"05".equals(tstate) && !"07".equals(tstate)){//项目制状态只要不是 05 的都 都变为 立项状态 。分工是和总公司默认审核通过
    			//处理项目信息，并添加项目轨迹
    			LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
    			LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
    			String tProjectSQL = "select * from LCProjectInfo where ProjectNo = '"+mProjectNo+"'";
    			tLCProjectInfoSet = tLCProjectInfoDB.executeQuery(tProjectSQL);
    			LCProjectInfoSchema tLCProjectInfoSchema = new LCProjectInfoSchema();
    			tLCProjectInfoSchema = tLCProjectInfoSet.get(1);
    			//处理年度信息
    			LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
    			LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
    			String tYearSQL = "select * from LCProjectYearInfo where ProjectNo = '"+mProjectNo+"'";
    			tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery(tYearSQL);
    			//处理审核结论
    			LCProjectUWSet tLCProjectUWSet = new LCProjectUWSet();
    			LCProjectUWDB tLCProjectUWDB = new LCProjectUWDB();
    			String tUWSQL = "select * from LCProjectUW where ProjectNo = '"+mProjectNo+"' ";
    			tLCProjectUWSet = tLCProjectUWDB.executeQuery(tUWSQL);
    			if(tLCProjectUWSet == null || tLCProjectUWSet.size()==0){
    				for(int i=1 ;i<=2;i++){
    				String tProjectSerialNo = getProjectSerialNo();
    			    LCProjectTraceSchema tLCProjectTraceSchema = new LCProjectTraceSchema();
    				tReflections.transFields(tLCProjectTraceSchema,tLCProjectInfoSchema);
    				tLCProjectTraceSchema.setProjectSerialNo(tProjectSerialNo);
    				map.put(tLCProjectTraceSchema, SysConst.INSERT);
    				LCProjectUWSchema tLCProjectUWSchema=new LCProjectUWSchema();
        	    	String tUWSerialNo = PubFun1.CreateMaxNo("PUWSerialNo", 20);
        	    	tLCProjectUWSchema.setSerialNo(tUWSerialNo);
        	    	
        	    	if(tProjectSerialNo == null || "".equals(tProjectSerialNo)){
        	    		     CError tError = new CError();
        	    			 tError.moduleName = "ProjectUWBL";
        	    			 tError.functionName = "dealData";
        	    			 tError.errorMessage = "获取项目流水号失败！";
        	    			 this.mErrors.addOneError(tError);
        	    			 return false;
        	    		     }
        	    	tLCProjectUWSchema.setProjectSerialNo(tProjectSerialNo);
        	    	tLCProjectUWSchema.setProjectNo(mProjectNo);
        	    	if(i==1){
        	    		tLCProjectUWSchema.setSendOperator("sys");
        	    		
        	    	}else{
        	    		tLCProjectUWSchema.setSendOperator(muwoperator);
        	    	}
        	    	tLCProjectUWSchema.setConclusion("2");
        	    	tLCProjectUWSchema.setSendDate(PubFun.getCurrentDate());
        	        tLCProjectUWSchema.setAuditFlag("0"+i);
        	        tLCProjectUWSchema.setAuditOpinion("同意。");
        	    	String tAuditTimes = getAuditTimes();
        	        tLCProjectUWSchema.setAuditTimes(tAuditTimes);
        	        tLCProjectUWSchema.setUWDate(PubFun.getCurrentDate());
        	        tLCProjectUWSchema.setUWTime(PubFun.getCurrentTime());
        	        tLCProjectUWSchema.setManageCom(mGlobalInput.ManageCom);
        	    	tLCProjectUWSchema.setOperator(mGlobalInput.Operator);
        	    	tLCProjectUWSchema.setMakeDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setMakeTime(PubFun.getCurrentTime());
        	    	tLCProjectUWSchema.setModifyDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setModifyTime(PubFun.getCurrentTime());
        	    	ttLCProjectUW.add(tLCProjectUWSchema);
        	    	//mMap.put(tLCProjectUWSchema, SysConst.INSERT);
    				}
    			}else if(tLCProjectUWSet != null && tLCProjectUWSet.size()==1){//到分公司审核
    				tLCProjectUWSet.get(1).setConclusion("2");
    				tLCProjectUWSet.get(1).setAuditOpinion("同意。");
    				String tAuditTimes = getAuditTimes();
    				tLCProjectUWSet.get(1).setAuditTimes(tAuditTimes);
    				tLCProjectUWSet.get(1).setUWDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(1).setUWTime(PubFun.getCurrentTime());
    				tLCProjectUWSet.get(1).setOperator(mGlobalInput.Operator);
    				tLCProjectUWSet.get(1).setMakeDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(1).setMakeTime(PubFun.getCurrentTime());
    				tLCProjectUWSet.get(1).setModifyDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(1).setModifyTime(PubFun.getCurrentTime());
    				
    				String tProjectSerialNo = getProjectSerialNo();
    				//增加轨迹
    				LCProjectTraceSchema tLCProjectTraceSchema = new LCProjectTraceSchema();
    				tReflections.transFields(tLCProjectTraceSchema,tLCProjectInfoSchema);
    				tLCProjectTraceSchema.setProjectSerialNo(tProjectSerialNo);
    				map.put(tLCProjectTraceSchema, SysConst.INSERT);
    				//增加总公司审核结论
    				LCProjectUWSchema tLCProjectUWSchema=new LCProjectUWSchema();
        	    	String tUWSerialNo = PubFun1.CreateMaxNo("PUWSerialNo", 20);
        	    	tLCProjectUWSchema.setSerialNo(tUWSerialNo);
        	    	
        	    	if(tProjectSerialNo == null || "".equals(tProjectSerialNo)){
        	    		     CError tError = new CError();
        	    			 tError.moduleName = "ProjectUWBL";
        	    			 tError.functionName = "dealData";
        	    			 tError.errorMessage = "获取项目流水号失败！";
        	    			 this.mErrors.addOneError(tError);
        	    			 return false;
        	    		     }
        	    	tLCProjectUWSchema.setProjectSerialNo(tProjectSerialNo);
        	    	tLCProjectUWSchema.setProjectNo(mProjectNo);
        	    	tLCProjectUWSchema.setSendOperator(muwoperator);
        	    	tLCProjectUWSchema.setConclusion("2");
        	    	tLCProjectUWSchema.setSendDate(PubFun.getCurrentDate());
        	        tLCProjectUWSchema.setAuditFlag("02");
        	        tLCProjectUWSchema.setAuditOpinion("同意。");
        	        tLCProjectUWSchema.setAuditTimes(tAuditTimes);
        	        tLCProjectUWSchema.setUWDate(PubFun.getCurrentDate());
        	        tLCProjectUWSchema.setUWTime(PubFun.getCurrentTime());
        	        tLCProjectUWSchema.setManageCom(mGlobalInput.ManageCom);
        	    	tLCProjectUWSchema.setOperator(mGlobalInput.Operator);
        	    	tLCProjectUWSchema.setMakeDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setMakeTime(PubFun.getCurrentTime());
        	    	tLCProjectUWSchema.setModifyDate(PubFun.getCurrentDate());
        	    	tLCProjectUWSchema.setModifyTime(PubFun.getCurrentTime());
        	    	ttLCProjectUW.add(tLCProjectUWSchema);
        	    	//mMap.put(tLCProjectUWSchema, SysConst.INSERT);
    			}else if(tLCProjectUWSet != null && tLCProjectUWSet.size()==2){//到总公司审核
    				tLCProjectUWSet.get(2).setConclusion("2");
    				tLCProjectUWSet.get(2).setAuditOpinion("同意。");
    				tLCProjectUWSet.get(2).setSendOperator(muwoperator);
    				String tAuditTimes = getAuditTimes();
    				tLCProjectUWSet.get(2).setAuditTimes(tAuditTimes);
    				tLCProjectUWSet.get(2).setOperator(muwoperator);
    				tLCProjectUWSet.get(2).setUWDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(2).setUWTime(PubFun.getCurrentTime());
    				tLCProjectUWSet.get(2).setMakeDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(2).setMakeTime(PubFun.getCurrentTime());
    				tLCProjectUWSet.get(2).setModifyDate(PubFun.getCurrentDate());
    				tLCProjectUWSet.get(2).setModifyTime(PubFun.getCurrentTime());
    			}
    			
    	        //年度信息处理
    			for(int i=1;i<=tLCProjectYearInfoSet.size();i++){
		        	tLCProjectYearInfoSet.get(i).setState("05");
		        	tLCProjectYearInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
		        	tLCProjectYearInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
		        }
    			map.put(tLCProjectYearInfoSet, SysConst.UPDATE);
    			//处理项目制状态
    			tLCProjectInfoSchema.setState("05");
    			tLCProjectInfoSchema.setModifyDate(PubFun.getCurrentDate());
    			tLCProjectInfoSchema.setModifyTime(PubFun.getCurrentTime());
    			map.put(ttLCProjectUW, SysConst.DELETE_AND_INSERT);
    			map.put(tLCProjectInfoSchema, SysConst.UPDATE);
    			map.put(tLCProjectUWSet, SysConst.UPDATE);
    			
    		}
    	}
    	return true;
    }

private String getAuditTimes(){
	String aAuditTimes = "";
	String aAuditTimesSql = "select count(distinct ProjectSerialNo) from LCProjectUW where ProjectNo = '"+mProjectNo+"' and AuditFlag = '02'";
	aAuditTimes = new ExeSQL().getOneValue(aAuditTimesSql);
	if(aAuditTimes == null || "".equals(aAuditTimes)){
		aAuditTimes = "0";
	}else{
		aAuditTimes = (Integer.parseInt(aAuditTimes) +1)+"";
	}
	return aAuditTimes;
}

private String getProjectSerialNo(){
	String aProjectSerialNo = "";
	 aProjectSerialNo = PubFun1.CreateMaxNo("ProjectSerialNo", 20);
	return aProjectSerialNo;
}

    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }


    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }


    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }
}
