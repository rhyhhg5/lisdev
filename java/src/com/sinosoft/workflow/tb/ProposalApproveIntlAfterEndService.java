package com.sinosoft.workflow.tb;

import org.jdom.Document;

import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.PolicyAppAccPayBL;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;

/**
 * <p>Title:工作流节点任务:新契约自动核保 </p>
 * <p>Description: 自动核保工作流后台AfterInit服务类 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: SinoSoft </p>
 * @author Yangyalin
 * @version 1.0
 */

public class ProposalApproveIntlAfterEndService implements AfterEndService
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    MMap mMap = new MMap();

    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 业务数据操作字符串 */
    private String mMissionID;

    /**保单表*/
    private LCContSchema mLCContSchema = new LCContSchema();

    private String mPolPassFlag = "0"; //险种通过标记

    private String mContPassFlag = "0"; //合同通过标记

    private String mUWGrade = "";

    private LCContSet mAllLCContSet = new LCContSet();

    private LCPolSet mAllLCPolSet = new LCPolSet();

    private String mContNo = "";

    private String mPContNo = "";

    private String mOldPolNo = "";

    /** 合同核保主表*/
    private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();

    private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();

    /** 合同核保子表*/
    private LCCUWSubSet mLCCUWSubSet = new LCCUWSubSet();

    private LCCUWSubSet mAllLCCUWSubSet = new LCCUWSubSet();

    /** 合同核保错误信息表*/
    private LCCUWErrorSet mLCCUWErrorSet = new LCCUWErrorSet();

    private LCCUWErrorSet mAllLCCUWErrorSet = new LCCUWErrorSet();

    /** 各险种核保主表 */
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();

    private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();

    private LCPolSet mLCPolSet = new LCPolSet();

    private CalBase mCalBase = new CalBase();

    public ProposalApproveIntlAfterEndService()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData(mLCContSchema))
        {
            return false;
        }

        //为工作流下一节点属性字段准备数据
        if (!prepareTransferData())
        {
            return false;
        }

        //准备给后台的数据
        prepareOutputData(mLCContSchema);

        mResult.clear();
        mResult.add(mMap);

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData(LCContSchema tLCContSchema)
    {
        String tUWConfirmNo = getUWConfirmNo(tLCContSchema.getPrtNo());

        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolSchema tLCPolSchema = null;

        mContNo = tLCContSchema.getContNo(); //获得保单号
        mPContNo = tLCContSchema.getProposalContNo();
        tLCPolDB.setContNo(mContNo);
        tLCPolSet = tLCPolDB.query();
        ExeSQL tExeSQL = new ExeSQL();
        String tSQL = "select code from ldcode where codetype='QYWNRuleButton' with ur";
        String tRuleButton = tExeSQL.getOneValue(tSQL);
        tSQL = "select comcode from ldcode where codetype='CardFlagRuleButton' and code='"
                + this.mLCContSchema.getCardFlag() + "' with ur";
        String tCardFlagButton = tExeSQL.getOneValue(tSQL);
        if (tRuleButton != null && tRuleButton.equals("00")
                && (tCardFlagButton == null || !tCardFlagButton.equals("11")))
        {
            try
            {
            	// modify by zxs
				NewEngineRuleService newEngineRule = new NewEngineRuleService();
				MMap map = newEngineRule.dealNUData(mLCContSchema, "NewQYWNRuleButton");
				VData mData = new VData();
				// modify by zxs
                //        	调用规则引擎	
                RuleTransfer ruleTransfer = new RuleTransfer();
                //获得规则引擎返回的校验信息 调用投保规则
                String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NU", mContNo, false);
                //将校验信息转换为Xml的document对象
                Document doc = ruleTransfer.stringToDoc(xmlStr);
                String approved = ruleTransfer.getApproved(doc);
                if (approved == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "执行规则引擎时出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if ("1".equals(approved))
                {
                }
                else if ("0".equals(approved) || "2".equals(approved))
                {
                }
                else if ("-1".equals(approved))
                {
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "规则引擎执行异常";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "规则引擎返回了未知的错误类型";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的投保规则
                String errorMessage = "<br>";
                String tempRiskcode = "";
                String tempInsured = "";
                if (tLMUWSetPolAllUnpass.size() > 0 && !checkUWConfirmNo(tLCContSchema, tUWConfirmNo))
                {
                    for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++)
                    {
                    	// modify by zxs
						LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
						EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
						engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
						engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
						engineRuleSchema.setApproved(approved);
						engineRuleSchema.setRuleResource("old");
						engineRuleSchema.setRuleType("NU");
						engineRuleSchema.setContType("万能保单");
						engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
						engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
						engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
						engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
						if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
							engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
						}else{
							engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
						}
						engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
						map.put(engineRuleSchema, "INSERT");
						// modify by zxs
						
                        tempRiskcode = "";
                        tempInsured = "";
                        if (tLMUWSetPolAllUnpass.get(m).getRiskName() != null
                                && !tLMUWSetPolAllUnpass.get(m).getRiskName().equals(""))
                        {
                            tempInsured += "被保人" + tLMUWSetPolAllUnpass.get(m).getRiskName();
                            if (tLMUWSetPolAllUnpass.get(m).getRiskCode() != null
                                    && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
                                    && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals(""))
                            {
                                tempRiskcode += "，险种代码" + tLMUWSetPolAllUnpass.get(m).getRiskCode() + "，";
                            }
                            else
                            {
                                tempInsured += "，";
                            }
                        }
                        else
                        {
                            if (tLMUWSetPolAllUnpass.get(m).getRiskCode() != null
                                    && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
                                    && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals(""))
                            {
                                tempRiskcode += "险种代码" + tLMUWSetPolAllUnpass.get(m).getRiskCode() + "，";
                            }
                        }
                        errorMessage += tLMUWSetPolAllUnpass.get(m).getUWCode();
                        errorMessage += "：";
                        errorMessage += tempInsured;
                        errorMessage += tempRiskcode;
                        errorMessage += tLMUWSetPolAllUnpass.get(m).getRemark();
                        errorMessage += "<br>";
                    }
                    
                    // modify by zxs
    				mData.add(map);
    				PubSubmit pubSubmit = new PubSubmit();
    				if (!pubSubmit.submitData(mData, "")) {
    					this.mErrors.addOneError(pubSubmit.mErrors.getContent());
    					return false;
    				}
    				// modify by zxs
                    
                    if (errorMessage != null)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalApproveIntlAfterEndService";
                        tError.functionName = "dealData";
                        tError.errorMessage = errorMessage;
                        mErrors.addOneError(tError);
                        System.out.println(tError.errorMessage);
                    }
                    return false;
                }else{
                	//// modify by zxs
                	EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
					engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
					engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
					engineRuleSchema.setApproved(approved);
					engineRuleSchema.setRuleResource("old");
					engineRuleSchema.setRuleType("NU");
					engineRuleSchema.setContType("万能保单");
					engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
					engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
					map.put(engineRuleSchema, "INSERT");
					// modify by zxs
                }
             // modify by zxs
				mData.add(map);
				PubSubmit pubSubmit = new PubSubmit();
				if (!pubSubmit.submitData(mData, "")) {
					this.mErrors.addOneError(pubSubmit.mErrors.getContent());
					return false;
				}
				// modify by zxs
                
                //险种核保
                LMUWSet tLMUWSetPolUnPase = new LMUWSet();
                LMUWSet tLMUWSetCommonPolUnpass = new LMUWSet();

                for (int nPolIndex = 1; nPolIndex <= tLCPolSet.size(); nPolIndex++)
                {
                    mPolPassFlag = "9"; //核保通过标志

                    tLCPolSchema = tLCPolSet.get(nPolIndex);
                    mOldPolNo = tLCPolSchema.getPolNo(); //获得保单险种号
                    if (mOldPolNo == null || mOldPolNo.equals(""))
                    {
                        mErrors.addOneError("请传入险种号");
                        return false;
                    }
                    String riskcode = tLCPolSchema.getRiskCode();

                    if (dealOnePol(tLCPolSchema) == false)
                    {
                        return false;
                    }
                }
                mContPassFlag = "9"; //无核保规则则置标志为通过

                //          万能险自核通过，保单上不需要录入核保审核号
                if ("8".equals(tLCContSchema.getCardFlag()) && tLMUWSetPolUnPase.size() == 0
                        && tLMUWSetCommonPolUnpass.size() == 0 && tLCContSchema.getUWConfirmNo() != null
                        && !tLCContSchema.getUWConfirmNo().equals(""))
                {
                    mErrors.addOneError("自核通过，不需要录入核保审核号，请修改");
                    return false;
                }

                if (!dealOneCont(tLCContSchema))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CError tError = new CError();
                tError.moduleName = "UWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "执行规则引擎时出错";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        else
        {
            //        	险种核保
            LMUWSet tLMUWSetPolUnPase = new LMUWSet();
            for (int nPolIndex = 1; nPolIndex <= tLCPolSet.size(); nPolIndex++)
            {
                mPolPassFlag = "9"; //核保通过标志

                tLCPolSchema = tLCPolSet.get(nPolIndex);
                mOldPolNo = tLCPolSchema.getPolNo(); //获得保单险种号
                if (mOldPolNo == null || mOldPolNo.equals(""))
                {
                    mErrors.addOneError("请传入险种号");
                    return false;
                }

                LMUWSet tLMUWSet = getPolUWSet(tLCPolSchema);
                if (tLMUWSet == null)
                {
                    return false;
                }

                this.autoUWPolInit(tLCPolSchema);

                for (int i = 1; i <= tLMUWSet.size(); i++)
                {
                    double rs = checkAutoUW(tLMUWSet.get(i).getCalCode(), tLCPolSchema.getInsuredNo(), tLMUWSet.get(i)
                            .getRiskCode());
                    if (Math.abs(rs - 1) < 0.001)
                    {
                        tLMUWSetPolUnPase.add(tLMUWSet.get(i));
                    }
                }

                if (tLMUWSetPolUnPase.size() > 0 && !checkUWConfirmNo(tLCContSchema, tUWConfirmNo))
                {
                    String errorInfo = getUWErrorInfo(tLMUWSetPolUnPase);
                    if (errorInfo != null)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalApproveIntlAfterEndService";
                        tError.functionName = "dealData";
                        tError.errorMessage = errorInfo;
                        mErrors.addOneError(tError);
                        System.out.println(tError.errorMessage);
                    }
                    return false;
                }

                if (dealOnePol(tLCPolSchema) == false)
                {
                    return false;
                }
            }

            mContPassFlag = "9"; //无核保规则则置标志为通过

            LMUWSet tLMUWSetContAll = getContUWSet();
            if (tLMUWSetContAll == null)
            {
                return false;
            }

            LMUWSet tLMUWSetContUnPase = new LMUWSet();
            this.autoUWContInit(tLCContSchema);
            for (int i = 1; i <= tLMUWSetContAll.size(); i++)
            {
                double rs = checkAutoUW(tLMUWSetContAll.get(i).getCalCode(), mLCContSchema.getInsuredNo(), "000000");
                if (Math.abs(rs - 1) < 0.001)
                {
                    tLMUWSetContUnPase.add(tLMUWSetContAll.get(i));
                }
            }

            if (tLMUWSetContUnPase.size() > 0 && !checkUWConfirmNo(tLCContSchema, tUWConfirmNo))
            {
                String errorInfo = getUWErrorInfo(tLMUWSetContUnPase);
                if (errorInfo != null)
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalApproveIntlAfterEndService";
                    tError.functionName = "dealData";
                    tError.errorMessage = errorInfo;
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                }
                return false;
            }

            //万能险自核通过，保单上不需要录入核保审核号
            if ("8".equals(tLCContSchema.getCardFlag()) && tLMUWSetPolUnPase.size() == 0
                    && tLMUWSetContUnPase.size() == 0 && tLCContSchema.getUWConfirmNo() != null
                    && !tLCContSchema.getUWConfirmNo().equals(""))
            {
                mErrors.addOneError("自核通过，不需要录入核保审核号，请修改");
                return false;
            }

            if (!dealOneCont(tLCContSchema))
            {
                return false;
            }
        }
        /**
         * 处理自核通过的银行转帐信息并生成首期缴费通知书管理信息
         */
        if (mContPassFlag.equals("9"))
        {
            String tPayMethod = mLCContSchema.getPayMethod();
            if ("01".equals(tPayMethod))
            {
                // 处理投保人帐户缴费模式
                MMap tMMap = dealAppAccount(mLCContSchema);
                if (tMMap == null)
                {
                    return false;
                }
                mMap.add(tMMap);
                // --------------------
            }
            else
            {
                if (!prepareSendFirstPayNotice())
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * checkUWConfirmNo
     * 校验万能险核保审核号是否相同
     * @param tLCContSchema LCContSchema
     * @param tUWConfirmNo String
     * @return boolean:若保单上和核保审核号表中录入且相等，返回true
     */
    private boolean checkUWConfirmNo(LCContSchema tLCContSchema, String tUWConfirmNo)
    {
        String tContUWConfirmNo = tLCContSchema.getUWConfirmNo();
        if ("8".equals(tLCContSchema.getCardFlag()) && tContUWConfirmNo != null && !tContUWConfirmNo.equals("")
                && tUWConfirmNo != null && !tUWConfirmNo.equals("") && tContUWConfirmNo.equals(tUWConfirmNo))
        {
            return true;
        }

        return false;
    }

    /**
     * getUWConfirmNo
     *
     * @param cContNo String
     * @return String
     */
    private String getUWConfirmNo(String cPrtNo)
    {
        LCUWConfirmInfoDB tLCUWConfirmInfoDB = new LCUWConfirmInfoDB();
        tLCUWConfirmInfoDB.setPrtNo(cPrtNo);
        if (!tLCUWConfirmInfoDB.getInfo())
        {
            return null;
        }

        return tLCUWConfirmInfoDB.getConfirmNo();
    }

    /**
     * 个人险种核保数据准备
     */
    private void autoUWPolInit(LCPolSchema tLCPolSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCPolSchema.getPrem());
        mCalBase.setGet(tLCPolSchema.getAmnt());
        mCalBase.setMult(tLCPolSchema.getMult());
        mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setJob(tLCPolSchema.getOccupationType());
        mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
        mCalBase.setPolNo(tLCPolSchema.getPolNo());
        mCalBase.setContNo(mContNo);
        mCalBase.setCValiDate(tLCPolSchema.getCValiDate());
    }

    /**
     * 个人单核保数据准备
     */
    private void autoUWContInit(LCContSchema tLCContSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCContSchema.getPrem());
        mCalBase.setGet(tLCContSchema.getAmnt());
        mCalBase.setMult(tLCContSchema.getMult());
        mCalBase.setSex(tLCContSchema.getInsuredSex());
        mCalBase.setCValiDate(tLCContSchema.getCValiDate());
        mCalBase.setContNo(mContNo);
    }

    /**
     * getContUWSet
     *
     * @param mPContNo String
     * @return LMUWSet
     */
    private LMUWSet getContUWSet()
    {
        String tsql = "select * from lmuw where riskcode = '000000' and relapoltype = 'I' and uwtype = '19'";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProposalApproveIntlAfterEndService";
            tError.functionName = "getContUWSet";
            tError.errorMessage = "合同险种核保信息查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * getUWErrorInfo
     * 查询自核不通过的信息
     * @param tLMUWSetUnPase LMUWSet
     * @return String
     */
    private String getUWErrorInfo(LMUWSet tLMUWSetUnPase)
    {
        String errInfo = "";
        for (int i = 1; i <= tLMUWSetUnPase.size(); i++)
        {
            String errorInfo = parseUWResult(tLMUWSetUnPase.get(i));
            if (errorInfo == null)
            {
                return null;
            }
            errInfo += errorInfo + "；\n";
        }

        return errInfo;
    }

    /**
     * 险种核保
     * 输出：通过1，否则0
     */
    private double checkAutoUW(String cCalCode, String cInsuredNo, String cRiskCode)
    {
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(cCalCode);

        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", "" + mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", "" + mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", "" + mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", cInsuredNo);
        mCalculator.addBasicFactor("RiskCode", cRiskCode);
        mCalculator.addBasicFactor("PrtNo", mLCContSchema.getPrtNo());
        mCalculator.addBasicFactor("CardFlag", mLCContSchema.getCardFlag());
        mCalculator.addBasicFactor("IntlFlag", (mLCContSchema.getIntlFlag() == null || mLCContSchema.getIntlFlag()
                .equals("")) ? "0" : mLCContSchema.getIntlFlag());

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr == null || tStr.trim().equals(""))
        {
            return 0;
        }
        else
        {
            return Double.parseDouble(tStr);
        }
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet getPolUWSet(LCPolSchema tLCPolSchema)
    {
        //查询算法编码
        String tsql = "select * from LMUW " + "where RiskCode = '000000' and RelapolType = 'I' "
                + "      and UWType = '11' " + "   or RiskCode = '" + tLCPolSchema.getRiskCode().trim() + "' "
                + "      and RelapolType = 'I' and UWType = '1'  " + "order by CalCode ";

        LMUWDB tLMUWDB = new LMUWDB();

        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds";
            tError.errorMessage = tLCPolSchema.getRiskCode().trim() + "险种核保信息查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();
            return null;
        }
        return tLMUWSet;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol(LCPolSchema tLCPolSchema)
    {
        //置险种复核信息
        if (preparePol(tLCPolSchema) == false)
        {
            return false;
        }
        //生成险种核保信息LCUWMaster和LCUWSub
        if (preparePolUW(tLCPolSchema) == false)
        {
            return false;
        }

        mAllLCPolSet.add(tLCPolSchema);
        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOneCont(LCContSchema tLCContSchema)
    {
        if (!prepareContUW(tLCContSchema))
        {
            return false;
        }

        if (mLCCUWErrorSet.size() > 0)
        {
            mContPassFlag = "5";
        }
        LCContSchema tLCContSchemaDup = new LCContSchema();
        tLCContSchemaDup.setSchema(tLCContSchema);
        mAllLCContSet.add(tLCContSchemaDup);

        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet.set(mLCCUWMasterSet);
        mAllLCCUWMasterSet.add(tLCCUWMasterSet);

        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet.set(mLCCUWSubSet);
        mAllLCCUWSubSet.add(tLCCUWSubSet);

        LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
        tLCCUWErrorSet.set(mLCCUWErrorSet);
        mAllLCCUWErrorSet.add(tLCCUWErrorSet);

        return true;
    }

    /**
     * 准备保单信息,自核通过时默认操作员为UW9999,不通过则置为空
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePol(LCPolSchema tLCPolSchema)
    {
        tLCPolSchema.setApproveFlag("9");
        tLCPolSchema.setApproveCode(mGlobalInput.Operator);
        tLCPolSchema.setApproveDate(PubFun.getCurrentDate());
        tLCPolSchema.setApproveTime(PubFun.getCurrentTime());

        System.out.println("险种核保标志" + mPolPassFlag);
        if (StrTool.cTrim(this.mPolPassFlag).equals("9"))
        {
            tLCPolSchema.setUWCode("UW9999");
            tLCPolSchema.setUWDate(PubFun.getCurrentDate());
            tLCPolSchema.setUWTime(PubFun.getCurrentTime());
        }
        else
        {
            tLCPolSchema.setUWCode("");
            tLCPolSchema.setUWDate("");
            tLCPolSchema.setUWTime("");
        }
        tLCPolSchema.setUWFlag(mPolPassFlag);
        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 准备合同核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareContUW(LCContSchema tLCContSchema)
    {
        tLCContSchema.setApproveFlag("9");
        tLCContSchema.setApproveCode(mGlobalInput.Operator);
        tLCContSchema.setApproveDate(PubFun.getCurrentDate());
        tLCContSchema.setApproveTime(PubFun.getCurrentTime());

        if (StrTool.cTrim(mContPassFlag).equals("9"))
        {
            tLCContSchema.setUWOperator("UW9999");
            tLCContSchema.setUWDate(PubFun.getCurrentDate());
            tLCContSchema.setUWTime(PubFun.getCurrentTime());
        }
        else
        {
            tLCContSchema.setUWOperator("");
            tLCContSchema.setUWDate("");
            tLCContSchema.setUWTime("");
        }
        tLCContSchema.setUWFlag(mContPassFlag);
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());

        //合同核保主表
        LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
        tLCCUWMasterDB.setContNo(mContNo);
        LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
        tLCCUWMasterSet = tLCCUWMasterDB.query();

        if (tLCCUWMasterSet.size() == 0)
        {
            tLCCUWMasterSchema.setContNo(mContNo);
            tLCCUWMasterSchema.setGrpContNo(tLCContSchema.getGrpContNo());
            tLCCUWMasterSchema.setProposalContNo(tLCContSchema.getProposalContNo());
            tLCCUWMasterSchema.setUWNo(1);
            tLCCUWMasterSchema.setInsuredNo(tLCContSchema.getInsuredNo());
            tLCCUWMasterSchema.setInsuredName(tLCContSchema.getInsuredName());
            tLCCUWMasterSchema.setAppntNo(tLCContSchema.getAppntNo());
            tLCCUWMasterSchema.setAppntName(tLCContSchema.getAppntName());
            tLCCUWMasterSchema.setAgentCode(tLCContSchema.getAgentCode());
            tLCCUWMasterSchema.setAgentGroup(tLCContSchema.getAgentGroup());
            tLCCUWMasterSchema.setPostponeDay("");
            tLCCUWMasterSchema.setPostponeDate("");
            tLCCUWMasterSchema.setHealthFlag("0");
            tLCCUWMasterSchema.setSpecFlag("0");
            tLCCUWMasterSchema.setQuesFlag("0");
            tLCCUWMasterSchema.setReportFlag("0");
            tLCCUWMasterSchema.setChangePolFlag("0");
            tLCCUWMasterSchema.setPrintFlag("0");
            tLCCUWMasterSchema.setPrintFlag2("0");
            tLCCUWMasterSchema.setManageCom(tLCContSchema.getManageCom());
            tLCCUWMasterSchema.setUWIdea("");
            tLCCUWMasterSchema.setUpReportContent("");
            tLCCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            tLCCUWMasterSchema = tLCCUWMasterSet.get(1);
            tLCCUWMasterSchema.setUWNo(tLCCUWMasterSchema.getUWNo() + 1);
            tLCCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        tLCCUWMasterSchema.setState(mContPassFlag);
        tLCCUWMasterSchema.setPassFlag(mContPassFlag);
        tLCCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
        tLCCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
        tLCCUWMasterSchema.setAppGrade(mUWGrade); //申报级别

        String tUWOperate = StrTool.cTrim(this.mContPassFlag).equals("9") ? "UW9999" : mGlobalInput.Operator;
        tLCCUWMasterSchema.setOperator(tUWOperate); //操作员

        // mLCCUWMasterSet.clear();
        mLCCUWMasterSet.add(tLCCUWMasterSchema);

        // 合同核保轨迹表
        LCCUWSubSchema tLCCUWSubSchema = new LCCUWSubSchema();
        LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB();
        tLCCUWSubDB.setContNo(mContNo);
        LCCUWSubSet tLCCUWSubSet = new LCCUWSubSet();
        tLCCUWSubSet = tLCCUWSubDB.query();

        if (tLCCUWSubSet.size() > 0)
        {
            tLCCUWSubSchema.setUWNo(tLCCUWSubSet.size() + 1); //第几次核保
        }
        else
        {
            tLCCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCCUWSubSchema.setContNo(tLCCUWMasterSchema.getContNo());
        tLCCUWSubSchema.setGrpContNo(tLCCUWMasterSchema.getGrpContNo());
        tLCCUWSubSchema.setProposalContNo(tLCCUWMasterSchema.getProposalContNo());
        tLCCUWSubSchema.setInsuredNo(tLCCUWMasterSchema.getInsuredNo());
        tLCCUWSubSchema.setInsuredName(tLCCUWMasterSchema.getInsuredName());
        tLCCUWSubSchema.setAppntNo(tLCCUWMasterSchema.getAppntNo());
        tLCCUWSubSchema.setAppntName(tLCCUWMasterSchema.getAppntName());
        tLCCUWSubSchema.setAgentCode(tLCCUWMasterSchema.getAgentCode());
        tLCCUWSubSchema.setAgentGroup(tLCCUWMasterSchema.getAgentGroup());
        tLCCUWSubSchema.setUWGrade(tLCCUWMasterSchema.getUWGrade()); //核保级别
        tLCCUWSubSchema.setAppGrade(tLCCUWMasterSchema.getAppGrade()); //申请级别
        tLCCUWSubSchema.setAutoUWFlag(tLCCUWMasterSchema.getAutoUWFlag());
        tLCCUWSubSchema.setState(tLCCUWMasterSchema.getState());
        tLCCUWSubSchema.setPassFlag(tLCCUWMasterSchema.getState());
        tLCCUWSubSchema.setPostponeDay(tLCCUWMasterSchema.getPostponeDay());
        tLCCUWSubSchema.setPostponeDate(tLCCUWMasterSchema.getPostponeDate());
        tLCCUWSubSchema.setUpReportContent(tLCCUWMasterSchema.getUpReportContent());
        tLCCUWSubSchema.setHealthFlag(tLCCUWMasterSchema.getHealthFlag());
        tLCCUWSubSchema.setSpecFlag(tLCCUWMasterSchema.getSpecFlag());
        tLCCUWSubSchema.setSpecReason(tLCCUWMasterSchema.getSpecReason());
        tLCCUWSubSchema.setQuesFlag(tLCCUWMasterSchema.getQuesFlag());
        tLCCUWSubSchema.setReportFlag(tLCCUWMasterSchema.getReportFlag());
        tLCCUWSubSchema.setChangePolFlag(tLCCUWMasterSchema.getChangePolFlag());
        tLCCUWSubSchema.setChangePolReason(tLCCUWMasterSchema.getChangePolReason());
        tLCCUWSubSchema.setAddPremReason(tLCCUWMasterSchema.getAddPremReason());
        tLCCUWSubSchema.setPrintFlag(tLCCUWMasterSchema.getPrintFlag());
        tLCCUWSubSchema.setPrintFlag2(tLCCUWMasterSchema.getPrintFlag2());
        tLCCUWSubSchema.setUWIdea(tLCCUWMasterSchema.getUWIdea());
        tLCCUWSubSchema.setOperator(tUWOperate); //操作员
        tLCCUWSubSchema.setManageCom(tLCCUWMasterSchema.getManageCom());
        PubFun.fillDefaultField(tLCCUWSubSchema);

        //mLCCUWSubSet.clear();
        mLCCUWSubSet.add(tLCCUWSubSchema);

        //取核保错误信息
        mLCCUWErrorSet.clear();

        return true;
    }

    /**
     * 准备险种核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePolUW(LCPolSchema tLCPolSchema)
    {

        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setPolNo(mOldPolNo);
        LCUWMasterSet tLCUWMasterSet = tLCUWMasterDB.query();
        if (tLCUWMasterSet.size() > 1)
        {
            this.mErrors.addOneError(mOldPolNo + "个人核保总表取数据不唯一!");
            return false;
        }

        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        if (tLCUWMasterSet.size() == 0)
        {
            tLCUWMasterSchema.setContNo(mContNo);
            tLCUWMasterSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
            tLCUWMasterSchema.setPolNo(mOldPolNo);
            tLCUWMasterSchema.setProposalNo(tLCPolSchema.getProposalNo());
            tLCUWMasterSchema.setUWNo(1);
            tLCUWMasterSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
            tLCUWMasterSchema.setInsuredName(tLCPolSchema.getInsuredName());
            tLCUWMasterSchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLCUWMasterSchema.setAppntName(tLCPolSchema.getAppntName());
            tLCUWMasterSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLCUWMasterSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLCUWMasterSchema.setPostponeDay("");
            tLCUWMasterSchema.setPostponeDate("");
            tLCUWMasterSchema.setHealthFlag("0");
            tLCUWMasterSchema.setSpecFlag("0");
            tLCUWMasterSchema.setQuesFlag("0");
            tLCUWMasterSchema.setReportFlag("0");
            tLCUWMasterSchema.setChangePolFlag("0");
            tLCUWMasterSchema.setPrintFlag("0");
            tLCUWMasterSchema.setManageCom(tLCPolSchema.getManageCom());
            tLCUWMasterSchema.setUWIdea("");
            tLCUWMasterSchema.setUpReportContent("");
            PubFun.fillDefaultField(tLCUWMasterSchema);
        }
        else if (tLCUWMasterSet.size() == 1)
        {
            tLCUWMasterSchema = tLCUWMasterSet.get(1);
            tLCUWMasterSchema.setUWNo(tLCUWMasterSchema.getUWNo() + 1);
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        tLCUWMasterSchema.setProposalContNo(mPContNo);
        tLCUWMasterSchema.setState(mPolPassFlag);
        tLCUWMasterSchema.setPassFlag(mPolPassFlag);
        tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
        tLCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
        tLCUWMasterSchema.setAppGrade(mUWGrade); //申报级别

        String tUWOperator = StrTool.cTrim(this.mPolPassFlag).equals("9") ? "UW9999" : mGlobalInput.Operator;
        tLCUWMasterSchema.setOperator(tUWOperator); //操作员

        //mLCUWMasterSet.clear();
        mLCUWMasterSet.add(tLCUWMasterSchema);

        // 核保轨迹表
        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
        tLCUWSubDB.setPolNo(mOldPolNo);
        LCUWSubSet tLCUWSubSet = tLCUWSubDB.query();
        if (tLCUWSubSet.size() > 0)
        {
            tLCUWSubSchema.setUWNo(tLCUWSubSet.size() + 1); //第几次核保
        }
        else
        {
            tLCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCUWSubSchema.setContNo(mContNo);
        tLCUWSubSchema.setPolNo(mOldPolNo);
        tLCUWSubSchema.setGrpContNo(tLCUWMasterSchema.getGrpContNo());
        tLCUWSubSchema.setProposalContNo(tLCUWMasterSchema.getProposalContNo());
        tLCUWSubSchema.setProposalNo(tLCUWMasterSchema.getProposalNo());
        tLCUWSubSchema.setInsuredNo(tLCUWMasterSchema.getInsuredNo());
        tLCUWSubSchema.setInsuredName(tLCUWMasterSchema.getInsuredName());
        tLCUWSubSchema.setAppntNo(tLCUWMasterSchema.getAppntNo());
        tLCUWSubSchema.setAppntName(tLCUWMasterSchema.getAppntName());
        tLCUWSubSchema.setAgentCode(tLCUWMasterSchema.getAgentCode());
        tLCUWSubSchema.setAgentGroup(tLCUWMasterSchema.getAgentGroup());
        tLCUWSubSchema.setUWGrade(tLCUWMasterSchema.getUWGrade()); //核保级别
        tLCUWSubSchema.setAppGrade(tLCUWMasterSchema.getAppGrade()); //申请级别
        tLCUWSubSchema.setAutoUWFlag(tLCUWMasterSchema.getAutoUWFlag());
        tLCUWSubSchema.setState(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPassFlag(tLCUWMasterSchema.getState());
        tLCUWSubSchema.setPostponeDay(tLCUWMasterSchema.getPostponeDay());
        tLCUWSubSchema.setPostponeDate(tLCUWMasterSchema.getPostponeDate());
        tLCUWSubSchema.setUpReportContent(tLCUWMasterSchema.getUpReportContent());
        tLCUWSubSchema.setHealthFlag(tLCUWMasterSchema.getHealthFlag());
        tLCUWSubSchema.setSpecFlag(tLCUWMasterSchema.getSpecFlag());
        tLCUWSubSchema.setSpecReason(tLCUWMasterSchema.getSpecReason());
        tLCUWSubSchema.setQuesFlag(tLCUWMasterSchema.getQuesFlag());
        tLCUWSubSchema.setReportFlag(tLCUWMasterSchema.getReportFlag());
        tLCUWSubSchema.setChangePolFlag(tLCUWMasterSchema.getChangePolFlag());
        tLCUWSubSchema.setChangePolReason(tLCUWMasterSchema.getChangePolReason());
        tLCUWSubSchema.setAddPremReason(tLCUWMasterSchema.getAddPremReason());
        tLCUWSubSchema.setPrintFlag(tLCUWMasterSchema.getPrintFlag());
        tLCUWSubSchema.setPrintFlag2(tLCUWMasterSchema.getPrintFlag2());
        tLCUWSubSchema.setUWIdea(tLCUWMasterSchema.getUWIdea());
        tLCUWSubSchema.setOperator(tUWOperator); //操作员
        tLCUWSubSchema.setManageCom(tLCUWMasterSchema.getManageCom());
        PubFun.fillDefaultField(tLCUWSubSchema);

        mAllLCUWSubSet.add(tLCUWSubSchema);

        return true;
    }

    /**
     * 准备返回前台统一存储数据
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void prepareOutputData(LCContSchema tLCContSchema)
    {
        mMap.put(tLCContSchema, "UPDATE");
        mMap.put(mLCCUWMasterSet.get(1), "DELETE&INSERT");
        mMap.put(mLCCUWSubSet, "INSERT");
        mMap.put(mLCCUWErrorSet, "INSERT");
        mMap.put(mLCUWMasterSet, SysConst.DELETE_AND_INSERT);

        mMap.put(mAllLCPolSet, "UPDATE");
        mMap.put(mLCPolSet, "UPDATE"); //zhangbin添加
        mMap.put(mAllLCUWSubSet, "INSERT");
    }

    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中MissionID失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得当前工作任务的任务ID
        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (mContNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中mContNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if (tLCContDB.getInfo())
        { //验证LCCont表中是否存在该合同项记录
            mLCContSchema.setSchema(tLCContDB.getSchema());
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "getInputData";
            tError.errorMessage = "合同号为" + mLCContSchema.getContNo() + "未查询到!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 为公共传输数据集合中添加工作流下一节点属性字段数据
     * @return
     */
    private boolean prepareTransferData()
    {
        mTransferData.setNameAndValue("ContNo", mLCContSchema.getContNo());
        mTransferData.setNameAndValue("PrtNo", mLCContSchema.getPrtNo());
        mTransferData.setNameAndValue("AppntNo", mLCContSchema.getAppntNo());
        mTransferData.setNameAndValue("AppntName", mLCContSchema.getAppntName());
        mTransferData.setNameAndValue("ProposalContNo", mLCContSchema.getProposalContNo());
        mTransferData.setNameAndValue("AgentCode", mLCContSchema.getAgentCode());
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (tLAAgentSet == null || tLAAgentSet.size() != 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalApproveAfterEndService";
            tError.functionName = "prepareTransferData";
            tError.errorMessage = "代理人表LAAgent查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mTransferData.setNameAndValue("AgentName", tLAAgentSet.get(1).getName());
        mTransferData.setNameAndValue("ManageCom", mLCContSchema.getManageCom());
        if (mPolPassFlag.equals("5"))
        {
            mContPassFlag = "5";
        }
        mTransferData.setNameAndValue("UWFlag", mContPassFlag);
        mTransferData.setNameAndValue("UWDate", PubFun.getCurrentDate());
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 用于客户选择交费方式为银行转帐
     * 自动在财务收费里面添加一条银行
     * 转帐信息
     * 2005-05-23 杨明 添加
     * @return boolean
     */
    private boolean prepareSendFirstPayNotice()
    {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode("07");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AutoUWFlag", "1");
        VData tempVData = new VData();
        tempVData.add(tLOPRTManagerSchema);
        tempVData.add(mGlobalInput);
        tempVData.add(tTransferData);
        UWSendPrintUI tUWSendPrintUI = new UWSendPrintUI();
        tUWSendPrintUI.submitData(tempVData, "INSERT");
        return true;
    }

    /**
     * 解析自核不通过的原因
     * @param tLMUWSchema LMUWSchema
     * @return String
     */
    private String parseUWResult(LMUWSchema tLMUWSchema)
    {
        PubCalculator tPubCalculator = new PubCalculator();
        tPubCalculator.addBasicFactor("GrpContNo", this.mContNo);
        tPubCalculator.addBasicFactor("polno", this.mOldPolNo);
        String tSql = tLMUWSchema.getRemark().trim();
        String tStr = "";
        String tStr1 = "";
        try
        {
            while (true)
            {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals(""))
                {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "interpretFactorInSQL";
            tError.errorMessage = "解释" + tSql + "的变量:" + tStr + "时出错。";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tSql.trim().equals(""))
        {
            return tLMUWSchema.getRemark();
        }
        return tSql;
    }

    /**
     * 处理投保人帐户转保费
     * @return
     */
    private MMap dealAppAccount(LCContSchema cContInfo)
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        VData tVData = new VData();

        TransferData tTransferData = new TransferData();

        String tContNo = cContInfo.getContNo();
        tTransferData.setNameAndValue("ContNo", tContNo);

        String tAppntNo = cContInfo.getAppntNo();
        tTransferData.setNameAndValue("AppAccNo", tAppntNo);

        String tStrSql = "select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) from LCPol lcp "
                + " where lcp.ContNo = '" + tContNo + "' ";
        String tPolicySumPrem = new ExeSQL().getOneValue(tStrSql);
        tTransferData.setNameAndValue("AppAccMoney", tPolicySumPrem);

        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        PolicyAppAccPayBL tPolicyAppAccPayBL = new PolicyAppAccPayBL();
        tTmpMap = null;
        tTmpMap = tPolicyAppAccPayBL.getSubmitMap(tVData, "AppAccPay");
        if (tTmpMap == null)
        {
            buildError("dealCertInsuRela", tPolicyAppAccPayBL.mErrors.getFirstError());
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
