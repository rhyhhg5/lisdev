/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.util;

import org.apache.log4j.Logger;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.obj.MsgResHead;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;


public final class MsgUtil
{
    private final static Logger mLogger = Logger.getLogger(MsgUtil.class);

    public static MsgCollection getResMsgCol(MsgCollection cMsgCols)
    {
        MsgCollection tResMsgCols = new MsgCollection();

        MsgHead tSrcMsgHead = cMsgCols.getMsgHead();
        if (tSrcMsgHead == null)
        {
            mLogger.error("未获取到报头信息。");
            return null;
        }

        MsgResHead tResMsgHead = new MsgResHead();
        tResMsgHead.setBatchNo(tSrcMsgHead.getBatchNo());
        tResMsgHead.setSendDate(tSrcMsgHead.getSendDate());
        tResMsgHead.setSendTime(tSrcMsgHead.getSendTime());

        tResMsgHead.setBranchCode(tSrcMsgHead.getBranchCode());
        tResMsgHead.setSendOperator(tSrcMsgHead.getSendOperator());

        tResMsgHead.setMsgType(tSrcMsgHead.getMsgType());

        tResMsgCols.setMsgResHead(tResMsgHead);

        return tResMsgCols;
    }
}
