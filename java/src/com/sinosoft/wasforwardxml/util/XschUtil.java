/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public final class XschUtil
{
    private final static Logger mLogger = Logger.getLogger(XschUtil.class);

    public static BaseXmlSch swapX2Sch(Element cEleNode, String cNodeName, String cPackage_Name)
    {
        final String Package_Name = cPackage_Name;

        try
        {
            String tClassName = Package_Name + "." + cNodeName;
            BaseXmlSch tXsch = (BaseXmlSch) (Class.forName(tClassName).newInstance());

            // 对相同字段进行自动赋值
            List tFieldList = cEleNode.getChildren();
            for (int i = 0; i < tFieldList.size(); i++)
            {
                Object tXSNode = tFieldList.get(i);
                if (!(tXSNode instanceof Element))
                    continue;
                if ((((Element) tXSNode).getChildren()).size() > 0)
                    continue;

                Element tEleSubNote = (Element) tXSNode;
                String tFiledName = tEleSubNote.getName();
                String tFiledValue = tEleSubNote.getTextTrim();
                tXsch.setField(tFiledName, tFiledValue);
            }
            // --------------------

            return tXsch;
        }
        catch (Exception e)
        {
            mLogger.error("不存在对应Xsch对象。");
            return null;
        }
    }

    public static BaseXmlSch swapX2Sch(Element cEleNode, String cNodeName)
    {
        String tPackage_Name = "com.sinosoft.wasforwardxml.obj";
        return swapX2Sch(cEleNode, cNodeName, tPackage_Name);
    }

    public static Element swapSch2X(BaseXmlSch cXsch, String cNodeName)
    {
        try
        {
            Element tEleRoot = new Element(cNodeName);

            Class tClz = cXsch.getClass();
            Field[] tFields = tClz.getDeclaredFields();
            for (int idx = 0; idx < tFields.length; idx++)
            {
                Field tSubField = tFields[idx];
                String tFieldName = tSubField.getName();
                try
                {
                    PropertyDescriptor tProD = new PropertyDescriptor(tFieldName, tClz);
                    Method tGetMethod = tProD.getReadMethod();
                    Object tResObj = tGetMethod.invoke(cXsch, null);

                    Element tEleSubNode = new Element(tFieldName);
                    tEleSubNode.setText((String) tResObj);
                    tEleRoot.addContent(tEleSubNode);
                }
                catch (Exception e)
                {
                    continue;
                }
            }

            return tEleRoot;
        }
        catch (Exception e)
        {
            mLogger.error("生成xml节点数据失败。");
            return null;
        }
    }
}
