/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;


public class MsgHead extends BaseXmlSch
{
    private static final long serialVersionUID = 8315415570568211491L;

    private String BatchNo = null;

    private String SendDate = null;

    private String SendTime = null;

    private String BranchCode = null;

    private String SendOperator = null;

    private String MsgType = null;

    /**
     * @return batchNo
     */
    public String getBatchNo()
    {
        return BatchNo;
    }

    /**
     * @param batchNo 要设置的 batchNo
     */
    public void setBatchNo(final String batchNo)
    {
        BatchNo = batchNo;
    }

    /**
     * @return branchCode
     */
    public String getBranchCode()
    {
        return BranchCode;
    }

    /**
     * @param branchCode 要设置的 branchCode
     */
    public void setBranchCode(final String branchCode)
    {
        BranchCode = branchCode;
    }

    /**
     * @return msgType
     */
    public String getMsgType()
    {
        return MsgType;
    }

    /**
     * @param msgType 要设置的 msgType
     */
    public void setMsgType(final String msgType)
    {
        MsgType = msgType;
    }

    /**
     * @return sendDate
     */
    public String getSendDate()
    {
        return SendDate;
    }

    /**
     * @param sendDate 要设置的 sendDate
     */
    public void setSendDate(final String sendDate)
    {
        SendDate = sendDate;
    }

    /**
     * @return sendOperator
     */
    public String getSendOperator()
    {
        return SendOperator;
    }

    /**
     * @param sendOperator 要设置的 sendOperator
     */
    public void setSendOperator(final String sendOperator)
    {
        SendOperator = sendOperator;
    }

    /**
     * @return sendTime
     */
    public String getSendTime()
    {
        return SendTime;
    }

    /**
     * @param sendTime 要设置的 sendTime
     */
    public void setSendTime(final String sendTime)
    {
        SendTime = sendTime;
    }
}
