/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class MsgResHead extends BaseXmlSch
{
    private static final long serialVersionUID = -4202748903354843378L;

    private String BatchNo = null;

    private String SendDate = null;

    private String SendTime = null;

    private String BranchCode = null;

    private String SendOperator = null;

    private String MsgType = null;

    private String State = null;

    private String ErrCode = null;

    private String ErrInfo = null;

    /**
     * @return batchNo
     */
    public String getBatchNo()
    {
        return BatchNo;
    }

    /**
     * @param batchNo 要设置的 batchNo
     */
    public void setBatchNo(final String batchNo)
    {
        BatchNo = batchNo;
    }

    /**
     * @return branchCode
     */
    public String getBranchCode()
    {
        return BranchCode;
    }

    /**
     * @param branchCode 要设置的 branchCode
     */
    public void setBranchCode(final String branchCode)
    {
        BranchCode = branchCode;
    }

    /**
     * @return errCode
     */
    public String getErrCode()
    {
        return ErrCode;
    }

    /**
     * @param errCode 要设置的 errCode
     */
    public void setErrCode(final String errCode)
    {
        ErrCode = errCode;
    }

    /**
     * @return errInfo
     */
    public String getErrInfo()
    {
        return ErrInfo;
    }

    /**
     * @param errInfo 要设置的 errInfo
     */
    public void setErrInfo(final String errInfo)
    {
        ErrInfo = errInfo;
    }

    /**
     * @return msgType
     */
    public String getMsgType()
    {
        return MsgType;
    }

    /**
     * @param msgType 要设置的 msgType
     */
    public void setMsgType(final String msgType)
    {
        MsgType = msgType;
    }

    /**
     * @return sendDate
     */
    public String getSendDate()
    {
        return SendDate;
    }

    /**
     * @param sendDate 要设置的 sendDate
     */
    public void setSendDate(final String sendDate)
    {
        SendDate = sendDate;
    }

    /**
     * @return sendOperator
     */
    public String getSendOperator()
    {
        return SendOperator;
    }

    /**
     * @param sendOperator 要设置的 sendOperator
     */
    public void setSendOperator(final String sendOperator)
    {
        SendOperator = sendOperator;
    }

    /**
     * @return sendTime
     */
    public String getSendTime()
    {
        return SendTime;
    }

    /**
     * @param sendTime 要设置的 sendTime
     */
    public void setSendTime(final String sendTime)
    {
        SendTime = sendTime;
    }

    /**
     * @return state
     */
    public String getState()
    {
        return State;
    }

    /**
     * @param state 要设置的 state
     */
    public void setState(final String state)
    {
        State = state;
    }

}
