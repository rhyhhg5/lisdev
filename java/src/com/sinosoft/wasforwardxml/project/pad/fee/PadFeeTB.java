package com.sinosoft.wasforwardxml.project.pad.fee;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCAppntTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.WrapTable;
import com.sinosoft.lis.bank.RealTimeSendBankUI;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class PadFeeTB extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCContTable cLCContTable;

	public OutputDataTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码

	String serialno = "";

	String getnoticeno = "";
	protected boolean deal(MsgCollection cMsgInfos,Document cInXmlDoc){		
		return true;
	}
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad出单投保过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();
			

			String sql = "select contno from lccont where prtno = '"
					+ cLCContTable.getPrtNo() + "' and prtno like 'PD%' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单不存在");
				return false;
			}

			String bankinfo = "select bankcode,accname,bankaccno from lccont where prtno = '"
					+ cLCContTable.getPrtNo() + "' ";
			tSSRS = new ExeSQL().execSQL(bankinfo);
			if (!cLCContTable.getBankCode().equals(tSSRS.GetText(1, 1))
					|| !cLCContTable.getBankAccNo().equals(tSSRS.GetText(1, 3))
					|| !cLCContTable.getAccName().equals(tSSRS.GetText(1, 2))) {
				errLog("银行信息与系统不一致");
				return false;
			}

			String tempinfo = "select 1 from ljtempfee where otherno='"
					+ cLCContTable.getPrtNo() + "' and enteraccdate is null";
			tSSRS = new ExeSQL().execSQL(tempinfo);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单无收费信息");
				return false;
			}

			String ljsinfo = "select getnoticeno,cansendbank from ljspay where otherno='"
					+ cLCContTable.getPrtNo() + "' and othernotype='9'";
			tSSRS = new ExeSQL().execSQL(ljsinfo);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单应收数据错误");
				return false;
			} else {
				getnoticeno = tSSRS.GetText(1, 1);
				String cansend=tSSRS.GetText(1, 2);
				if(!"p".equals(cansend)){
					errLog("不能进行实时收费，需从核心进行批量收费！");
					return false;
				}
			}

			RealTimeSendBankUI tRealTimeSendBankUI = new RealTimeSendBankUI();
			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("getNoticeNo", getnoticeno);
			transferData1.setNameAndValue("bankCode", "7705");
			transferData1.setNameAndValue("uniteBankCode", cLCContTable
					.getBankCode());

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(mGlobalInput);

			if (!tRealTimeSendBankUI.submitData(tVData, "GETMONEY")) {
				VData rVData = tRealTimeSendBankUI.getResult();
				errLog((String) rVData.get(0));
				return false;
			} else {
				serialno = tRealTimeSendBankUI.getSerialNo();
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String temp = "select 1 from ljspay where otherno='"
				+ cLCContTable.getPrtNo() + "' and othernotype='9'";
		SSRS mSSRS = new ExeSQL().execSQL(temp);
		if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
			String sql = "select contno,amnt,prem,prtno from lccont where prtno = '"
					+ cLCContTable.getPrtNo() + "'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				mError = "获取保单信息失败！";
				return false;
			} else {
				cOutputDataTable = new OutputDataTable();
				cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
				cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
				cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
				cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
				cOutputDataTable.setStateFlag("5");
			}
			return true;
		} else {
			temp = "select ld.codename from lyreturnfrombank ly ,ldcode1 ld where ly.serialno='"
					+ serialno
					+ "' and ly.paycode='"
					+ getnoticeno
					+ "' and ly.dealtype='R' and ld.codetype='bankerror' and ld.code='7705'"
					+ " and ld.code1=ly.BankSuccFlag ";
			mSSRS = new ExeSQL().execSQL(temp);
			if (mSSRS.getMaxRow() > 0){
				mError="银行收费失败，失败原因："+mSSRS.GetText(1, 1);
				return false;
			}
			
		}
		
		return true;

	}

}
