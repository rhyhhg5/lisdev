/**
 * 
 * 收费状态查询
 * @author zxs
 * @date 2018-05-18
 *
 */
package com.sinosoft.wasforwardxml.project.pad.fee;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.ReturnDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class BatchQuery extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public LCContTable cLCContTable;

	public ReturnDataTable cReturnDataTable;

	public String succStr;// 成功提示

	public String error;// 错误原因

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理查询收费状态接口");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单

			String sql = "select lybank.banksuccflag from lyreturnfrombank lybank,ljtempfee ljt where lybank.paycode = ljt.tempfeeno "
					+ "and ljt.enteraccdate is not null  and ljt.otherno in (select contno from lccont where prtno = '"
					+ cLCContTable.getPrtNo() + "') order by senddate desc fetch first 1 rows only with ur";
			String succflag = new ExeSQL().getOneValue(sql);
			if (succflag != null && succflag != "" && ("0000").equals(succflag)) {
				succStr = "收费成功";
			} else {
				succStr = "收费失败";
				sql = "select banksuccflag from lyreturnfrombank where paycode in (select tempfeeno from ljtempfee where otherno = '"
						+ cLCContTable.getPrtNo()
						+ "' and enteraccdate is null) order by senddate desc fetch first 1 rows only with ur";
				String bankFlag = new ExeSQL().getOneValue(sql);
				if (bankFlag != "" && bankFlag != null) {
					sql = "select codename  from ldcode1 where codetype='bankerror' and code1='" + bankFlag
							+ "' and ldcode1.code='7705'";
					error = new ExeSQL().getOneValue(sql);
				}
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("ReturnDataTable", cReturnDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		cReturnDataTable = new ReturnDataTable();
		cReturnDataTable.setPrtNo(cLCContTable.getPrtNo());
		cReturnDataTable.setSuccFlag(succStr);
		cReturnDataTable.setDesc(error);
		return true;

	}

}
