/**
 * 
 * 更新银行卡信息接口,保单层.收费层银行卡信息
 * @author zxs
 * @date 2018-05-14
 *
 */

package com.sinosoft.wasforwardxml.project.pad.fee;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.ReturnDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class ChangeBankInfor extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public LCContTable cLCContTable;

	public ReturnDataTable cReturnDataTable;

	public LCContSchema mLCContSchema;

	public LJSPaySchema mLJSPaySchema;

	LJTempFeeSchema mLJTempFeeSchema;

	LCAppntSchema mLCAppntSchema;

	LJTempFeeSet mLJTempFeeSet;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	private ExeSQL tExeSQL = new ExeSQL();

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理PAD银行卡信息变更过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			if (!CheckData()) {
				errLog(mError);
				return false;
			}

			if (!UpdateBankInfor()) {
				errLog(mError);
				return false;
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("ReturnDataTable", cReturnDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		cReturnDataTable = new ReturnDataTable();
		cReturnDataTable.setPrtNo(cLCContTable.getPrtNo());
		cReturnDataTable.setSuccFlag("1");

		return true;
	}

	// 检查表单数据信息
	public boolean CheckData() {
		String prtno = cLCContTable.getPrtNo();
		if (prtno.equals("")) {
			mError = "请传入印刷号！";
			return false;
		}
		if ("".equals(cLCContTable.getBankAccNo()) || "".equals(cLCContTable.getBankCode())
				|| "".equals(cLCContTable.getAccName())) {
			mError = "请传入银行代码、账户、账号";
			return false;
		}

		String sql = "select 1 from ldbankunite where bankunitecode in ('7705','7706','7709','7701','7703','7714') and bankcode = '"
				+ cLCContTable.getBankCode() + "'";
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "传入的银行编码不存在！";
			return false;
		}
		// 判断是否存在该表单
		sql = "select 1 from lccont where prtno = '" + prtno + "' with ur";
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "保单不存在！";
			return false;
		}

		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(prtno);
		LCContSet mLCContSet = mLCContDB.query();
		if (mLCContSet.size() > 0) {
			mLCContSchema = mLCContSet.get(1);
			if (!mLCContSchema.getAccName().equals(cLCContTable.getAccName())) {
				mError = "传入账户和保单账户姓名不一致！";
				return false;
			}
		} else {
			mError = "保单信息不存在";
			return false;
		}

		sql = "select 1 from lcappnt where prtno = '" + prtno + "' with ur";
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "投保人信息不存在！";
			return false;
		}

		LCAppntDB mLCAppntDB = new LCAppntDB();
		mLCAppntDB.setPrtNo(cLCContTable.getPrtNo());
		LCAppntSet mLCAppntSet = mLCAppntDB.query();
		if (mLCAppntSet != null && mLCAppntSet.size() > 0) {
			mLCAppntSchema = mLCAppntSet.get(1);
			if (!mLCAppntSchema.getAccName().equals(cLCContTable.getAccName())) {
				mError = "传入账户和保单账户姓名不一致！";
				return false;
			}
		} else {
			mError = "保单信息不存在！";
			return false;
		}

		LJSPayDB mLJSPayDB = new LJSPayDB();
		mLJSPayDB.setOtherNo(prtno);
		LJSPaySet mLJSPaySet = mLJSPayDB.query();
		if (mLJSPaySet != null && mLJSPaySet.size() > 0) {
			mLJSPaySchema = mLJSPaySet.get(1);
			if (!mLJSPaySchema.getAccName().equals(cLCContTable.getAccName())) {
				mError = "传入账户和保单账户姓名不一致！";
				return false;
			}
		}
		LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
		mLJTempFeeDB.setOtherNo(prtno);
		mLJTempFeeSet = mLJTempFeeDB.query();
		if (mLJTempFeeSet == null || mLJTempFeeSet.size() < 0) {
			mError = "暂收表数据不存在！";
			return false;
		}

		sql = "select 1 from ljspay where otherno = '" + cLCContTable.getPrtNo()
				+ "' and bankonthewayflag = '1' with ur";
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			mError = "银行信息在途不能修改！";
			return false;
		}

		return true;
	}

	// 更新银行卡信息
	public boolean UpdateBankInfor() {

		VData mData = new VData();
		String bankcode = cLCContTable.getBankCode();
		String bankno = cLCContTable.getBankAccNo();
		String modifyDate = PubFun.getCurrentDate();
		String modifyTime = PubFun.getCurrentTime();

		String strSql = "select * from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where otherno='"
				+ cLCContTable.getPrtNo() + "')";
		LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSchema().getDB().executeQuery(strSql);

		mLCContSchema.setBankAccNo(bankno);
		mLCContSchema.setBankCode(bankcode);
		mLCContSchema.setModifyDate(modifyDate);
		mLCContSchema.setModifyTime(modifyTime);

		mLCAppntSchema.setBankAccNo(bankno);
		mLCAppntSchema.setBankCode(bankcode);
		mLCAppntSchema.setModifyDate(modifyDate);
		mLCAppntSchema.setModifyTime(modifyTime);
		MMap mMap = new MMap();
		if (mLJSPaySchema != null) {
			mLJSPaySchema.setBankAccNo(bankno);
			mLJSPaySchema.setBankCode(bankcode);
			mLJSPaySchema.setModifyDate(modifyDate);
			mLJSPaySchema.setModifyTime(modifyTime);
			mMap.put(mLJSPaySchema, SysConst.UPDATE);
		}

		if (mLJTempFeeSet != null && mLJTempFeeSet.size() > 0) {
			for (int k = 1; k <= mLJTempFeeSet.size(); k++) {
				mLJTempFeeSchema = mLJTempFeeSet.get(k);
				mLJTempFeeSchema.setModifyDate(modifyDate);
				mLJTempFeeSchema.setModifyTime(modifyTime);
				mMap.put(mLJTempFeeSchema, SysConst.UPDATE);
			}

		}

		mMap.put(mLCContSchema, SysConst.UPDATE);
		mMap.put(mLCAppntSchema, SysConst.UPDATE);

		if (mLJTempFeeClassSet.size() > 0) {
			for (int i = 1; i <= mLJTempFeeClassSet.size(); i++) {
				LJTempFeeClassSchema mLJTempFeeClassSchema = mLJTempFeeClassSet.get(i);

				mLJTempFeeClassSchema.setBankAccNo(bankno);
				mLJTempFeeClassSchema.setBankCode(bankcode);
				mLJTempFeeClassSchema.setModifyDate(modifyDate);
				mLJTempFeeClassSchema.setModifyTime(modifyTime);

				mMap.put(mLJTempFeeClassSchema, SysConst.UPDATE);
			}
		}
		mData.add(mMap);
		PubSubmit pubSubmit = new PubSubmit();

		if (!pubSubmit.submitData(mData, null)) {
			errLog("银行卡信息变更失败！");
			return false;
		}
		return true;
	}

}
