/**
 * 
 * 实时收费转批量接口
 * @author zxs
 * @date 2018-05-11
 *
 */
package com.sinosoft.wasforwardxml.project.pad.fee;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.ReturnDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class TransZero extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public LCContTable cLCContTable;

	public ReturnDataTable cReturnDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	private ExeSQL tExeSQL = new ExeSQL();

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理PAD实时收费转批扣过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单

			// 判断保单是否存在
			String sql = "select contno from lccont where prtno = '" + cLCContTable.getPrtNo() + "' with ur";
			SSRS tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单不存在！");
				return false;
			}
			// 判断保单是否处于未收费状态

			sql = "select 1 from ljspay ljs ,ljtempfee ljt where ljt.tempfeeno=ljs.getnoticeno and ljs.otherno='"
					+ cLCContTable.getPrtNo() + "' " + "and ljt.otherno='" + cLCContTable.getPrtNo()
					+ "' and ljt.enteraccdate is null " + "and ljt.confmakedate is null";
			tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("该保单不处于未收费状态！");
				return false;
			}
			// 判断保单收费状态以及是否和银行有交互
			sql = "select cansendbank,bankonthewayflag from ljspay where otherno = '" + cLCContTable.getPrtNo()
					+ "' with ur";
			tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS != null && tSSRS.getMaxRow() > 0) {
				String cansendbank = tSSRS.GetText(1, 1);
				String bankonthewayflag = tSSRS.GetText(1, 2);
				if (!cansendbank.equals("p")) {
					errLog("保单收费状态不为实时收费状态！");
					return false;
				}

				if ((!bankonthewayflag.equals("0")) && (!bankonthewayflag.equals(""))) {
					errLog("保单缴费数据和银行有交互！");
					return false;
				}
			}
			// 更新为批量扣费
			sql = "update ljspay set cansendbank='0' where otherno = '" + cLCContTable.getPrtNo() + "'";
			boolean succ = tExeSQL.execUpdateSQL(sql);
			if (!succ) {
				errLog("数据更新失败！");
				return false;
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("ReturnDataTable", cReturnDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		cReturnDataTable = new ReturnDataTable();
		cReturnDataTable.setPrtNo(cLCContTable.getPrtNo());
		cReturnDataTable.setSuccFlag("1");
		return true;

	}

}
