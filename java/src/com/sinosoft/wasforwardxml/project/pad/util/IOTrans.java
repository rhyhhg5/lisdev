package com.sinosoft.wasforwardxml.project.pad.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;

public class IOTrans {
	public static byte[] InputStreamToBytes(InputStream pIns) {
		ByteArrayOutputStream mByteArrayOutputStream = new ByteArrayOutputStream();
		
		try {
			byte[] tBytes = new byte[8*1024];
			for (int tReadSize; -1 != (tReadSize=pIns.read(tBytes)); ) {
				mByteArrayOutputStream.write(tBytes, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pIns.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mByteArrayOutputStream.toByteArray();
	}
	
	public static String ReaderToString(Reader pReader) {
		StringWriter mStringWriter = new StringWriter();
		
		try {
			char[] tChars = new char[4*1024];
			for (int tReadSize; -1 != (tReadSize=pReader.read(tChars)); ) {
				mStringWriter.write(tChars, 0, tReadSize);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				pReader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		return mStringWriter.toString();
	}
}
