package com.sinosoft.wasforwardxml.project.pad.util;
import java.util.*;
public class IndexMap extends HashMap{
	
	private static final long serialVersionUID = 1L;   
	  
    private List list=new ArrayList();   
       
  
    public Object put(Object key, Object value) {   
        if (!containsKey(key)){   
            list.add(key);   
        }   
        return super.put(key, value);   
    }  
    
    public Object put(Object key, Object value, int index) {   
        if (!containsKey(key)){   
            list.set(index, key);   
        }   
        return super.put(key, value);   
    }  
  
    public void setListSize(int size) {
    	for (int i = 0; i < size; i++) {
			list.add("");
		}
    }
    
    public Object get(int idx){   
        return super.get(getKey(idx));   
    }
    
    public int getIndex(Object key){   
        return list.indexOf(key);   
    }
    
    public Object getKey(int idx){   
        if (idx>=list.size()) return null;   
        return list.get(idx);   
    }   
       
    public void remove(int idx){   
        Object key=getKey(idx);   
        removeFromList(getIndex(key));   
        super.remove(key);   
    }   
       
    public Object remove(Object key) {   
        removeFromList(getIndex(key));   
        return super.remove(key);   
    }   
       
    public void clear() {   
        this.list = new ArrayList();   
        super.clear();   
    }
    
    private void removeFromList(int idx){   
    	if (idx<list.size() && idx>=0) {   
            list.remove(idx);   
        }   
    }   
}