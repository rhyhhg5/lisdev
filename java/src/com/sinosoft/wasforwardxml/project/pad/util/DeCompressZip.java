package com.sinosoft.wasforwardxml.project.pad.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class DeCompressZip {
	
	public boolean DeCompress(String tFile,String fileDir){
		try {
			String newFileDir = "";
			File file = new File(tFile);//压缩文件
			ZipFile zipFile = new ZipFile(file);//实例化ZipFile，每一个zip压缩文件都可以表示为一个ZipFile
			//实例化一个Zip压缩文件的ZipInputStream对象，可以利用该类的getNextEntry()方法依次拿到每一个ZipEntry对象
			ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file));
			ZipEntry zipEntry = null;
			while ((zipEntry = zipInputStream.getNextEntry()) != null) {
				String fileName = zipEntry.getName();				
				if(!fileName.contains(".")){
					break;
				}
				fileName=fileName.replace("\\", "/");
				newFileDir = fileDir +  fileName;
				File temp = new File(newFileDir);
				if (! temp.getParentFile().exists())
					temp.getParentFile().mkdirs();
				OutputStream os = new FileOutputStream(temp);
				//通过ZipFile的getInputStream方法拿到具体的ZipEntry的输入流
				InputStream is = zipFile.getInputStream(zipEntry);
				int len = 0;
				while ((len = is.read()) != -1)
					os.write(len);
				os.close();
				is.close();
			}
			zipInputStream.close();
			System.out.println("解压缩成功！");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return false;
		}
		return true;
		
	}
	
	public static void main(String args[]) throws IOException {
		String tFile ="D:\\test.zip";
		String fileDir = "D:\\unpackTest";
		DeCompressZip dec = new DeCompressZip();
		dec.DeCompress(tFile, fileDir);
	}
	
}