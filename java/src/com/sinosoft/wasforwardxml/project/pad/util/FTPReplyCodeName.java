package com.sinosoft.wasforwardxml.project.pad.util;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.net.ftp.FTPReply;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 本类用来进行FTP反馈信息代码到汉字的转换
 * 从方便ftp包作为组件打包到其他地方使用的方便程度，将代码和汉字的对应关系写到本类。
 * 代码编号<600的是FTP协议的标准代码，>=600是用户自定义代码
 *
 * 若反馈信息可能经常修改，可以将对应关系描述成xml文件
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public final class FTPReplyCodeName
{
    /**
     * 语言类型，英文
     */
    public static final int LANGUSGE_ENGLIST = new Integer(0).intValue();

    /**
     * 语言类型，中文
     */
    public static final int LANGUSGE_CHINESE = new Integer(1).intValue();

    /**
     * 服务器为空
     */
    public static final int MCODE_600 = new Integer(600).intValue();

    /**
     * 无法打开文件
     */
    public static final int MCODE_601 = new Integer(601).intValue();

    /**
     * Socket使用异常
     */
    public static final int MCODE_602 = new Integer(602).intValue();

    /**
     本地目录不存在
     */
    public static final int MCODE_603 = new Integer(603).intValue();

    /**
     * 推出ftp服务器异常
     */
    public static final int MCODE_604 = new Integer(604).intValue();

    /**
     * 代码-中英文对照表
     */
    public static final ConcurrentHashMap mContrastMap = new ConcurrentHashMap();

    public FTPReplyCodeName()
    {
        setCodeName();
    }

    /**
     * 添加中文对照表
     */
    private void setCodeName()
    {
        String[] CODE_110 = {"Restart marker reply. In this case, the text is exact and not left to the particular implementation; it must read: MARK yyyy = mmmm where yyyy is User-process data stream marker, and mmmm servers equivalent marker (note the spaces between markers and =).", "重新启动标志回应。这种情况下，信息是精确的并且不用特别的处理；可以这样看：标记 yyyy = mmm 中 yyyy是 用户进程数据流标记，mmmm是服务器端相应的标记（注意在标记和等号间的空格）"};
        String[] CODE_120 = {"Service ready in nnn minutes."                                                                                                                                                                                                                                 , "服务在NNN时间内可用"};
        String[] CODE_125 = {"Data connection already open; transfer starting."                                                                                                                                                                                                              , "数据连接已经打开，开始传送数据."};
        String[] CODE_150 = {"File status okay; about to open data connection."                                                                                                                                                                                                              , "文件状态正确，正在打开数据连接."};
        String[] CODE_200 = {"Command okay."                                                                                                                                                                                                                                                 , "命令执行正常结束."};
        String[] CODE_202 = {"Command not implemented, superfluous at this site.ommand not implemented, superfluous at this site."                                                                                                                                                           , "命令未被执行，此站点不支持此命令."};
        String[] CODE_211 = {"System status, or system help reply."                                                                                                                                                                                                                          , "系统状态或系统帮助信息回应."};
        String[] CODE_212 = {"Directory status."                                                                                                                                                                                                                                             , "目录状态信息."};
        String[] CODE_213 = {"File status."                                                                                                                                                                                                                                                  , "文件状态信息."};
        String[] CODE_214 = {"Help message.On how to use the server or the meaning of a particular non-        standard command. This reply is useful only to the human user. "                                                                                                              , "帮助信息。关于如何使用本服务 器或特殊的非标准命令。此回复只对人有用。"};
        String[] CODE_215 = {"NAME system type. Where NAME is an official system name from the list in the Assigned Numbers document."                                                                                                                                                       , "NAME系统类型。"};
        String[] CODE_220 = {"Service ready for new user."                                                                                                                                                                                                                                   , "连接的用户的服务已就绪"};
        String[] CODE_221 = {"Service closing control connection."                                                                                                                                                                                                                           , "控制连接关闭"};
        String[] CODE_225 = {"Data connection open; no transfer in progress."                                                                                                                                                                                                                , "数据连接已打开，没有进行中的数据传送"};
        String[] CODE_226 = {"Closing data connection. Requested file action successful (for example, file transfer or file abort)."                                                                                                                                                         , "正在关闭数据连接。请求文件动作成功结束（例如，文件传送或终止）"};
        String[] CODE_227 = {"Entering Passive Mode (h1,h2,h3,h4,p1,p2)."                                                                                                                                                                                                                    , "进入被动模式"};
        String[] CODE_230 = {"User logged in, proceed. Logged out if appropriate."                                                                                                                                                                                                           , "户已登入。 如果不需要可以登出。"};
        String[] CODE_250 = {"Requested file action okay, completed."                                                                                                                                                                                                                        , "被请求文件操作成功完成"};
        String[] CODE_257 = {"PATHNAME created."                                                                                                                                                                                                                                             , "路径已建立"};
        String[] CODE_331 = {"User name okay, need password."                                                                                                                                                                                                                                , "用户名存在，需要输入密码"};
        String[] CODE_332 = {"Need account for login."                                                                                                                                                                                                                                       , "需要登陆的账户"};
        String[] CODE_350 = {"Requested file action pending further information"                                                                                                                                                                                                             , "对被请求文件的操作需要进一步更多的信息"};
        String[] CODE_421 = {"Service not available, closing control connection.This may be a reply to any command if the service knows it must shut down."                                                                                                                                  , "服务不可用，控制连接关闭。这可能是对任何命令的回应，如果服务认为它必须关闭"};
        String[] CODE_425 = {"Cant open data connection."                                                                                                                                                                                                                                    , "打开数据连接失败"};
        String[] CODE_426 = {"Connection closed; transfer aborted."                                                                                                                                                                                                                          , "连接关闭，传送中止。"};
        String[] CODE_450 = {"Requested file action not taken."                                                                                                                                                                                                                              , "对被请求文件的操作未被执行"};
        String[] CODE_451 = {"Requested action aborted. Local error in processing."                                                                                                                                                                                                          , "请求的操作中止。处理中发生本地错误。"};
        String[] CODE_452 = {"Requested action not taken. Insufficient storage space in system.File unavailable (e.g., file busy)."                                                                                                                                                          , "请求的操作没有被执行。 系统存储空间不足。 文件不可用"};
        String[] CODE_500 = {"Syntax error, command unrecognized. This may include errors such as command line too long."                                                                                                                                                                    , "语法错误，不可识别的命令。 这可能是命令行过长。"};
        String[] CODE_501 = {"Syntax error in parameters or arguments."                                                                                                                                                                                                                      , "参数错误导致的语法错误"};
        String[] CODE_502 = {"Command not implemented."                                                                                                                                                                                                                                      , "命令未被执行"};
        String[] CODE_503 = {"Bad sequence of commands."                                                                                                                                                                                                                                     , "命令的次序错误。"};
        String[] CODE_504 = {"Command not implemented for that parameter."                                                                                                                                                                                                                   , "由于参数错误，命令未被执行"};
        String[] CODE_530 = {"Not logged in."                                                                                                                                                                                                                                                , "没有登录"};
        String[] CODE_532 = {"Need account for storing files."                                                                                                                                                                                                                               , "存储文件需要账户信息"};
        String[] CODE_550 = {"Requested action not taken. File unavailable (e.g., file not found, no access)."                                                                                                                                                                               , "请求操作未被执行，文件不可用。"};
        String[] CODE_551 = {"Requested action aborted. Page type unknown."                                                                                                                                                                                                                  , "请求操作中止，页面类型未知"};
        String[] CODE_552 = {"Requested file action aborted. Exceeded storage allocation (for current directory or dataset)."                                                                                                                                                                , "请求文件的操作中止。 超出存储分配"};
        String[] CODE_553 = {"Requested action not taken. File name not allowed"                                                                                                                                                                                                             , "请求操作未被执行。 文件名不允许"};
        String[] CODE_600 = {"Server is null", "服务器名为空"};
        String[] CODE_601 = {"Can not open file", "无法打开文件"};
        String[] CODE_602 = {"Socket exception", "Socket使用异常"};
        String[] CODE_603 = {"Local Directory not exists ", "退出ftp服务器异常"};
        String[] CODE_604 = {"Logon exception", "退出ftp服务器异常"};

        mContrastMap.put(String.valueOf(FTPReply.CODE_110), CODE_110);
        mContrastMap.put(String.valueOf(FTPReply.CODE_120), CODE_120);
        mContrastMap.put(String.valueOf(FTPReply.CODE_125), CODE_125);
        mContrastMap.put(String.valueOf(FTPReply.CODE_150), CODE_150);
        mContrastMap.put(String.valueOf(FTPReply.CODE_200), CODE_200);
        mContrastMap.put(String.valueOf(FTPReply.CODE_202), CODE_202);
        mContrastMap.put(String.valueOf(FTPReply.CODE_211), CODE_211);
        mContrastMap.put(String.valueOf(FTPReply.CODE_212), CODE_212);
        mContrastMap.put(String.valueOf(FTPReply.CODE_213), CODE_213);
        mContrastMap.put(String.valueOf(FTPReply.CODE_214), CODE_214);
        mContrastMap.put(String.valueOf(FTPReply.CODE_215), CODE_215);
        mContrastMap.put(String.valueOf(FTPReply.CODE_220), CODE_220);
        mContrastMap.put(String.valueOf(FTPReply.CODE_221), CODE_221);
        mContrastMap.put(String.valueOf(FTPReply.CODE_225), CODE_225);
        mContrastMap.put(String.valueOf(FTPReply.CODE_226), CODE_226);
        mContrastMap.put(String.valueOf(FTPReply.CODE_227), CODE_227);
        mContrastMap.put(String.valueOf(FTPReply.CODE_230), CODE_230);
        mContrastMap.put(String.valueOf(FTPReply.CODE_250), CODE_250);
        mContrastMap.put(String.valueOf(FTPReply.CODE_257), CODE_257);
        mContrastMap.put(String.valueOf(FTPReply.CODE_331), CODE_331);
        mContrastMap.put(String.valueOf(FTPReply.CODE_332), CODE_332);
        mContrastMap.put(String.valueOf(FTPReply.CODE_350), CODE_350);
        mContrastMap.put(String.valueOf(FTPReply.CODE_421), CODE_421);
        mContrastMap.put(String.valueOf(FTPReply.CODE_425), CODE_425);
        mContrastMap.put(String.valueOf(FTPReply.CODE_426), CODE_426);
        mContrastMap.put(String.valueOf(FTPReply.CODE_450), CODE_450);
        mContrastMap.put(String.valueOf(FTPReply.CODE_451), CODE_451);
        mContrastMap.put(String.valueOf(FTPReply.CODE_452), CODE_452);
        mContrastMap.put(String.valueOf(FTPReply.CODE_500), CODE_500);
        mContrastMap.put(String.valueOf(FTPReply.CODE_501), CODE_501);
        mContrastMap.put(String.valueOf(FTPReply.CODE_502), CODE_502);
        mContrastMap.put(String.valueOf(FTPReply.CODE_503), CODE_503);
        mContrastMap.put(String.valueOf(FTPReply.CODE_504), CODE_504);
        mContrastMap.put(String.valueOf(FTPReply.CODE_530), CODE_530);
        mContrastMap.put(String.valueOf(FTPReply.CODE_532), CODE_532);
        mContrastMap.put(String.valueOf(FTPReply.CODE_550), CODE_550);
        mContrastMap.put(String.valueOf(FTPReply.CODE_551), CODE_551);
        mContrastMap.put(String.valueOf(FTPReply.CODE_552), CODE_552);
        mContrastMap.put(String.valueOf(FTPReply.CODE_553), CODE_553);
        mContrastMap.put(String.valueOf(MCODE_600), CODE_600);
        mContrastMap.put(String.valueOf(MCODE_601), CODE_601);
        mContrastMap.put(String.valueOf(MCODE_602), CODE_602);
        mContrastMap.put(String.valueOf(MCODE_603), CODE_603);
        mContrastMap.put(String.valueOf(MCODE_604), CODE_604);
    }

    /**
     * 得到传入代码对应的含义
     * @param code String：代码
     * @param cLanguage:语言类型，0英语、1中文
     * @return String：cCode所代表的汉字
     */
    public String getCodeName(int cCode, int cLanguage)
    {
        String[] name = (String[])mContrastMap.get(String.valueOf(cCode));
        return name[cLanguage];
    }

    public static void main(String[] args)
    {
        FTPReplyCodeName f = new FTPReplyCodeName();
        System.out.println(f.getCodeName(110, 1));
    }
}
