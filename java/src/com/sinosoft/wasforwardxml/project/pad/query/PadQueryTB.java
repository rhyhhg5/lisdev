package com.sinosoft.wasforwardxml.project.pad.query;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCAppntTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.WrapTable;
import com.sinosoft.lis.bank.RealTimeSendBankUI;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class PadQueryTB extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCContTable cLCContTable;

	public OutputDataTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码

	String serialno = "";

	String getnoticeno = "";
	protected boolean deal(MsgCollection cMsgInfos,Document cInXmlDoc){		
		return true;
	}
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad查询保单状态过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();

			String sql = "select contno from lccont where prtno = '"
					+ cLCContTable.getPrtNo()
					+ "' union select contno from lbcont where prtno ='"
					+ cLCContTable.getPrtNo() + "' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单不存在");
				return false;
			}
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,appflag,stateflag,codename('stateflag',stateflag),uwflag from lccont where prtno = '"
				+ cLCContTable.getPrtNo()
				+ "' union "
				+ " select contno,amnt,prem,prtno,appflag,stateflag,codename('stateflag',stateflag),uwflag from lbcont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			String appflag= tSSRS.GetText(1, 5);
			if("0".equals(appflag)){
				PadQueryStateBL tPadQueryStateBL = new PadQueryStateBL();
				String stateflag = tPadQueryStateBL.submitData(tSSRS.GetText(1, 1), tSSRS.GetText(1, 4));
				cOutputDataTable.setStateFlag(stateflag);
			}else{
				cOutputDataTable.setStateFlag(tSSRS.GetText(1, 6));
			}
            			
		}
		return true;

	}
}
