package com.sinosoft.wasforwardxml.project.pad.query;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class PadQueryStateBL {

	public String submitData(String tContno,String tPrtno) {
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		//新单复核
		String mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp1='"+tContno+"' and activityid='0000001001' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "10";
        }
        //人工核保
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001100' ";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "4";
        }
        //撤单
        mSQL ="select 1 from LCCont where ProposalContNo='"+tContno+"' and UWFlag='a' with ur ";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "7";
        }
        //拒绝承保
        mSQL ="select 1 from LCCont where ProposalContNo='"+tContno+"' and UWFlag='1' with ur ";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "6";
        }
        //签单工作流查询
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp1='"+tContno+"' and activityid='0000001150' ";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	mSQL = "select  case when db2inst1.LF_PayMoneyNo('"+tPrtno+"') >=0 then '5' else '9' end  from dual where 1=1 ";
        	tSSRS = tExeSQL.execSQL(mSQL);
        	return tSSRS.GetText(1, 1);
        }
        
        //问题件已下发未打印 
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001023' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "11";
        }
        //问题件已下发，待扫描回销 
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001025' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "12";
        }
        
        //核保通知书已下发，待扫描回销
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001112' and (select  uwflag from lccont where contno='"+tContno+"') not in ('1','8','a') with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "13";
        }

        //核保通知书已下发，待客户回复
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001200' and (select  uwflag from lccont where contno='"+tContno+"') not in ('1','8','a') with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "14";
        }
        
        //契调通知书已下发，未打印
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001108' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "15";
        }
        
        //契调通知书已下发，待扫描回销
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001113' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "16";
        }
        
        //体检通知书待打印
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001106' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "17";
        }
        
        //体检通知书待扫描回销
        mSQL ="select 1 from LWMission where processid ='0000000003' and MissionProp2='"+tContno+"' and activityid='0000001111' with ur";
        tSSRS = tExeSQL.execSQL(mSQL);
        if(tSSRS.getMaxRow()>0){
        	return "18";
        }
        
		return "8";
	}

}