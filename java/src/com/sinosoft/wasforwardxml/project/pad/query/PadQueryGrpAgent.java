package com.sinosoft.wasforwardxml.project.pad.query;

import java.util.List;

import org.jdom.Document;

import com.cbsws.obj.RspSaleInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.sys.MixedSalesAgentQueryUI;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.GrpAgentInfo;
import com.sinosoft.wasforwardxml.project.pad.obj.GrpAgentQueryInfo;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class PadQueryGrpAgent extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者
	
	public String GrpAgentCode=""; //业务员
	
	public String manageCom=""; //管理机构

	public GrpAgentQueryInfo cGrpAgentTable;

	public GrpAgentInfo cGrpAgentInfo;
	
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码

	String serialno = "";
	private String Codetype="";
	String getnoticeno = "";
	protected boolean deal(MsgCollection cMsgInfos,Document cInXmlDoc){		
		return true;
	}
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad查询保单状态过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		if(!"PADQUERYAGENT".equals(MsgType)){
			errLog1("类型错误");
			return false;
		}
		try {
			List tGrpAgentCodeList = cMsgInfos.getBodyByFlag("GrpAgentQueryInfo");
			if (tGrpAgentCodeList == null || tGrpAgentCodeList.size() != 1) {
				errLog1("获取业务员报文信息失败。");
				return false;
			}

			cGrpAgentTable = (GrpAgentQueryInfo) tGrpAgentCodeList.get(0); // 获取保单信息，只有一个保单

			GrpAgentCode=cGrpAgentTable.getGrpAgentCode();
			manageCom=cGrpAgentTable.getManageCom();
			
			mGlobalInput.ManageCom = cGrpAgentTable.getManageCom();
			mGlobalInput.ComCode = cGrpAgentTable.getManageCom();
			RspSaleInfo rspSaleInfo = new RspSaleInfo();
			  MixedSalesAgentQueryUI mixedSalesAgentQueryUI = new MixedSalesAgentQueryUI();
			  try{
				  rspSaleInfo = mixedSalesAgentQueryUI.getSaleInfo(GrpAgentCode,manageCom);
			  }catch(Exception ex){
					Codetype="0001";
					errLog("核心在调用远程接口调取业务员信息过程中出现未知异常");
				  ex.getStackTrace();
return false;
			  }
			  String type = rspSaleInfo.getMESSAGETYPE();
			  String typeInfo = rspSaleInfo.getERRDESC();

			  if("01".equals(type)){
			  	//如果进入这里，返回报文信息为typeInfo
				  Codetype="9999";
				  errLog1("错误："+typeInfo);
		  	 
			  }else{
			  		String UNI_SALES_COD = (rspSaleInfo.getUNI_SALES_COD()+""); // 统一编码(对方业务员代码)
			  		String SALES_NAM = (rspSaleInfo.getSALES_NAM()+"");//姓名
			  		String MAN_ORG_COD = (rspSaleInfo.getMAN_ORG_COD()+"");//机构代码
			  		String MAN_ORG_NAM = (rspSaleInfo.getMAN_ORG_NAM()+"");//机构名称
			  		String ID_NO = (rspSaleInfo.getID_NO()+"");//业务员的证件号码
			  Codetype="0000";
					  errLog2("");
			  		cGrpAgentInfo = new GrpAgentInfo();
			  		cGrpAgentInfo.setUNI_SALES_COD(UNI_SALES_COD);
			  		cGrpAgentInfo.setSALES_NAM(SALES_NAM);
			  		cGrpAgentInfo.setMAN_ORG_COD(MAN_ORG_COD);
			  		cGrpAgentInfo.setMAN_ORG_NAM(MAN_ORG_NAM);
			  		cGrpAgentInfo.setID_NO(ID_NO);
			  } 
				//封装完毕
		} catch (Exception ex) {
			Codetype="0001";
			errLog("执行过程中出现未知异常");
			return false;
		}
  		getXmlResult();
		return true;
	}

	// 返回报文
	public void getXmlResult() {
		
		if("0000".equals(Codetype)){
			// 返回数据节点
			putResult("GrpAgentInfo", cGrpAgentInfo);
		}
	}
}
