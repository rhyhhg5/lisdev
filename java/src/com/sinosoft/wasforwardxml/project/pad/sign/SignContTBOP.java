package com.sinosoft.wasforwardxml.project.pad.sign;

import java.util.List;
import org.jdom.Document;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LDFinBankDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LDFinBankSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.project.pad.obj.TempFee;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class SignContTBOP extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCContTable cLCContTable;

	public OutputDataTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码

	String serialno = "";

	String getnoticeno = "";

	private VData mResult = new VData();

	String merror = "";

	public TempFee cTempFee;// 收费信息

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad出单签单过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();

			// modify by zxs
			List tTempFee = cMsgInfos.getBodyByFlag("TempFee");
			if (tTempFee == null || tTempFee.size() != 1) {
				errLog("获取收费信息失败。");
				return false;
			}
			cTempFee = (TempFee) tTempFee.get(0);
			if (!updateTempFee()) {
				errLog(merror);
				return false;
			}
			//

			String sql = "select contno from lccont where prtno = '" + cLCContTable.getPrtNo()
					+ "' and prtno like 'PD%' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单不存在！");
				return false;
			}

			sql = "select 1 from ljtempfee where Othernotype='4' and Confflag='0' and Enteraccdate is not null and Otherno='"
					+ cLCContTable.getPrtNo() + "'";
			tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单未缴费！");
				return false;
			}

			sql = "Select Distinct Lwmission.Missionprop1,Lwmission.Missionprop2,Lwmission.Missionid,Lwmission.Submissionid"
					+ " From Lwmission,Lccont, Ljtempfee " + " Where Lwmission.Processid = '0000000003' "
					+ " and Lwmission.Missionprop1 = Lccont.Proposalcontno "
					+ " and Lwmission.Missionprop2 = Ljtempfee.Otherno " + " and Ljtempfee.Othernotype = '4' "
					+ " and Ljtempfee.Confflag = '0' " + " and (Ljtempfee.Enteraccdate Is Not Null) "
					+ " and Lwmission.Activityid = '0000001150' " + " and Lwmission.Missionprop2 = '"
					+ cLCContTable.getPrtNo() + "' " + " with ur ";
			tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("未查询到满足条件的签单数据！");
				return false;
			}

			LCContSet tLCContSet = new LCContSet();
			LCContDB tLCContDB = new LCContDB();
			tLCContSet = tLCContDB.executeQuery(
					"select * from lccont where prtno='" + cLCContTable.getPrtNo() + "'  and prtno like 'PD%' ");
			boolean isSucc = false;

			VData tInputData = new VData();
			tInputData.add(tLCContSet);
			tInputData.add(this.mGlobalInput);
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("MissionID", tSSRS.GetText(1, 3));
			tTransferData.setNameAndValue("SubMissionID", tSSRS.GetText(1, 4));
			tInputData.add(tTransferData);

			TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
			if (tTbWorkFlowUI.submitData(tInputData, "0000001150") == false) {
				errLog(tTbWorkFlowUI.mErrors.getError(0).errorMessage);
				return false;
			}

			// 自动回执回销
			// if(!addGetPolDate()){
			// return false;
			// }

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			cOutputDataTable.setStateFlag(tSSRS.GetText(1, 5));

		}
		return true;

	}

	/**
	 * addGetPolDate
	 * 
	 * @return boolean
	 */
	private boolean addGetPolDate() {

		MMap tmpMap = new MMap();
		String sql_update = "update lccont set " + "CustomGetPolDate = '" + PubFun.getCurrentDate() + "',"
				+ "GetPolDate = '" + PubFun.getCurrentDate() + "'," + "ModifyDate = '" + PubFun.getCurrentDate() + "',"
				+ "ModifyTime = '" + PubFun.getCurrentTime() + "'," + "Operator = '" + this.mGlobalInput.Operator + "' "
				+ " where PrtNo = '" + cLCContTable.getPrtNo() + "'";
		tmpMap.put(sql_update, "UPDATE");
		mResult.add(tmpMap);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, null)) {
			CError.buildErr(this, "数据库保存失败");
			return false;
		}
		return true;
	}

	/**
	 * 更新暂收费信息
	 * 
	 * @return boolean
	 */
	private boolean updateTempFee() {
		MMap mMap = new MMap();

		String amount = cTempFee.getChargeAmount();
		String date = cTempFee.getChargeDate();
		String method = cTempFee.getChargeMethod();
		String succFlag = cTempFee.getChargeSuccFlag();
		String time = cTempFee.getChargeTime();
		String prtno = cLCContTable.getPrtNo();
		String insManagecom = cTempFee.getManagecom();//归集账户管理机构
		String Insbankaccno = cTempFee.getInsbankaccno();//归集账户
		String insBankcode = cTempFee.getInsbankcode();//归集账户银行编码
		String modifyDate = PubFun.getCurrentDate();
		String modifyTime = PubFun.getCurrentTime();
		if(insManagecom==null||"".equals(insManagecom)){
			merror = "归集账户管理机构不能为空！";
			return false;
		}
		if(Insbankaccno==null||"".equals(Insbankaccno)){
			merror = "归集账户不能为空！";
			return false;
		}
		if(insBankcode==null||"".equals(insBankcode)){
			merror = "归集账户银行编码不能为空！";
			return false;
		}
		 LDFinBankDB tLDFinBankDB = new LDFinBankDB();
		 tLDFinBankDB.setManageCom(insManagecom);
		 tLDFinBankDB.setBankAccNo(Insbankaccno);
		 tLDFinBankDB.setBankCode(insBankcode);
		 LDFinBankSet tLDFinBankSet = tLDFinBankDB.query();
		 if(tLDFinBankSet.size()<=0){
			    merror = "归集账户相关信息不匹配！";
				return false; 
		 }
		String sql1 = "select 1 from ljtempfee where otherno = '" + prtno + "' and enteraccdate is not null";
		SSRS tSSR = new ExeSQL().execSQL(sql1);
		if (tSSR == null || tSSR.getMaxRow() <= 0) {

			String strSQL = "select 1 from ljtempfee where otherno = '" + prtno
					+ "' and enteraccdate is null and not exists (select 1 from ljapay where getnoticeno = ljtempfee.tempfeeno)";
			SSRS tSSRS = new ExeSQL().execSQL(strSQL);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				merror = "保单不处于收费状态！";
				return false;
			}

			strSQL = "select 1 from ljtempfeeclass where tempfeeno in(select tempfeeno from ljtempfee where otherno = '"
					+ prtno + "') and paymoney = '" + amount + "'";
			tSSRS = new ExeSQL().execSQL(strSQL);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				merror = "收费金额和暂收表金额不一致";
				return false;
			}

			LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
			tLJTempFeeDB.setOtherNo(prtno);
			LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
			
			LCContDB tLcContDB = new LCContDB();
			tLcContDB.setPrtNo(prtno);
			LCContSet tLcContSet = tLcContDB.query();
			if(tLcContSet.size()==0){
				merror = "保单数据有问题！";
				return false;
			}
		/*
			String bankunitecode = new ExeSQL().getOneValue("select  bankunitecode  from ldbankunite where bankcode='"+tLcContSet.get(1).getBankCode()+"'");
			String SQL1 = null;
			if(bankunitecode!=null&&(bankunitecode.equals("7705")||bankunitecode.equals("7706")||bankunitecode.equals("7709"))){
				SQL1 = "select * from ldfinbank where finbankcode='7705/7706' and finflag = 'S' and managecom = '86000000'";
			}else if(bankunitecode!=null&&bankunitecode.equals("7701")){
				String uniteSQL = "select unitebankcode from ldbankunite where bankunitecode='7701' and bankcode='"+tLcContSet.get(1).getBankCode()+"'";
				String unitebankcode = new ExeSQL().getOneValue(uniteSQL);
				if(unitebankcode!=null&&unitebankcode!=""){
				SQL1 =  "select * from ldfinbank where finbankcode='"+unitebankcode+"' and finflag = 'S'";
				}
			}else if(bankunitecode!=null&&bankunitecode.equals("7703")){
				SQL1 = "select * from ldfinbank where bankcode = '16' and operater = 'YBT' and finflag = 'S' and managecom = '86000000'";
			}else {
				merror = "银行编码不正确，请核实！";
            	return false;
			}
            LDFinBankDB ttLDFinBankDB = new LDFinBankDB();
            LDFinBankSet ttLDFinBankSet = ttLDFinBankDB.executeQuery(SQL1);
            if (ttLDFinBankSet.size() == 0){
                System.out.println("---查询银行失败！---");
            	merror = "查询银行失败！";
            	return false;
            }
            */
			if (tLJTempFeeSet != null || tLJTempFeeSet.size() > 0) {
				for (int j = 1; j <= tLJTempFeeSet.size(); j++) {

					LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(j);
					tLJTempFeeSchema.setEnterAccDate(date);
					tLJTempFeeSchema.setConfMakeDate(date);
					tLJTempFeeSchema.setConfMakeTime(time);
					tLJTempFeeSchema.setManageCom(insManagecom);
					tLJTempFeeSchema.setModifyDate(modifyDate);
					tLJTempFeeSchema.setModifyTime(modifyTime);

					mMap.put(tLJTempFeeSchema, SysConst.UPDATE);

					String sql = "select * from ljtempfeeclass where tempfeeno = '" + tLJTempFeeSchema.getTempFeeNo()
							+ "'";
					LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSchema().getDB().executeQuery(sql);
					if (tLJTempFeeClassSet != null && tLJTempFeeClassSet.size() > 0) {
						for (int i = 1; i <= tLJTempFeeClassSet.size(); i++) {
							LJTempFeeClassSchema mLJTempFeeClassSchema = tLJTempFeeClassSet.get(i);

							mLJTempFeeClassSchema.setEnterAccDate(date);
							mLJTempFeeClassSchema.setConfMakeDate(date);
							mLJTempFeeClassSchema.setConfMakeTime(time);
							mLJTempFeeClassSchema.setPayMode(method);
							mLJTempFeeClassSchema.setManageCom(insManagecom);
							mLJTempFeeClassSchema.setInsBankAccNo(Insbankaccno);
							mLJTempFeeClassSchema.setInsBankCode(insBankcode);
							mLJTempFeeClassSchema.setModifyDate(modifyDate);
							mLJTempFeeClassSchema.setModifyTime(modifyTime);
							mMap.put(mLJTempFeeClassSchema, SysConst.UPDATE);
						}
					}
				}
			} else {
				merror = "暂无收费信息";
				return false;
			}
			VData mData = new VData();
			mData.add(mMap);
			PubSubmit pubSubmit = new PubSubmit();
			if (succFlag.equals("1")) {
				if (!pubSubmit.submitData(mData, null)) {
					merror = "更新收费信息失败";
					return false;
				}
			} else {
				merror = "传入信息有误";
				return false;
			}
		}
		return true;

	}

}
