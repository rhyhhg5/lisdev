package com.sinosoft.wasforwardxml.project.pad.sign;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.tb.LCContSignBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class SignContTB extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCContTable cLCContTable;

	public OutputDataTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码

	String serialno = "";

	String getnoticeno = "";
	
	private VData mResult = new VData();
	protected boolean deal(MsgCollection cMsgInfos,Document cInXmlDoc){		
		return true;
	}
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad出单签单过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();

			String sql = "select contno from lccont where prtno = '"
					+ cLCContTable.getPrtNo() + "' and prtno like 'PD%' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单不存在！");
				return false;
			}
			
			sql = "select 1 from ljtempfee where Othernotype='4' and Confflag='0' and Enteraccdate is not null and Otherno='"+ cLCContTable.getPrtNo() + "'";
			tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单未缴费！");
				return false;
			}
			
			sql = "Select Distinct Lwmission.Missionprop1,Lwmission.Missionprop2,Lwmission.Missionid,Lwmission.Submissionid"
                + " From Lwmission,Lccont, Ljtempfee "
                + " Where Lwmission.Processid = '0000000003' "
                + " and Lwmission.Missionprop1 = Lccont.Proposalcontno "
                + " and Lwmission.Missionprop2 = Ljtempfee.Otherno "
                + " and Ljtempfee.Othernotype = '4' "
                + " and Ljtempfee.Confflag = '0' "
                + " and (Ljtempfee.Enteraccdate Is Not Null) "
                + " and Lwmission.Activityid = '0000001150' "
                + " and Lwmission.Missionprop2 = '"+cLCContTable.getPrtNo()+"' "
                + " with ur ";
			tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("未查询到满足条件的签单数据！");
				return false;
			}

			LCContSet tLCContSet = new LCContSet();
			LCContDB tLCContDB = new LCContDB();
			tLCContSet=tLCContDB.executeQuery("select * from lccont where prtno='"+ cLCContTable.getPrtNo() + "'  and prtno like 'PD%' ");
			boolean isSucc = false;

			VData tInputData = new VData();
			tInputData.add(tLCContSet);
			tInputData.add(this.mGlobalInput);
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("MissionID",tSSRS.GetText(1, 3));		   		
     		tTransferData.setNameAndValue("SubMissionID",tSSRS.GetText(1, 4) );
     		tInputData.add( tTransferData );
			
     		TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
			if ( tTbWorkFlowUI.submitData( tInputData, "0000001150")== false ){
				errLog(tTbWorkFlowUI.mErrors.getError(0).errorMessage);
				return false;
			}

			//自动回执回销
//			if(!addGetPolDate()){
//				return false;
//			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			cOutputDataTable.setStateFlag(tSSRS.GetText(1, 5));
			
		}
		return true;

	}

	/**
	 * addGetPolDate
	 * 
	 * @return boolean
	 */
	private boolean addGetPolDate() {

		MMap tmpMap = new MMap();
		String sql_update = "update lccont set " + "CustomGetPolDate = '"
				+ PubFun.getCurrentDate() + "'," + "GetPolDate = '"
				+ PubFun.getCurrentDate() + "'," + "ModifyDate = '"
				+ PubFun.getCurrentDate() + "'," + "ModifyTime = '"
				+ PubFun.getCurrentTime() + "'," + "Operator = '"
				+ this.mGlobalInput.Operator + "' " + " where PrtNo = '"
				+ cLCContTable.getPrtNo() + "'";
		tmpMap.put(sql_update, "UPDATE");
		mResult.add(tmpMap);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, null)) {
			CError.buildErr(this, "数据库保存失败");
			return false;
		}
		return true;
	}	

}
