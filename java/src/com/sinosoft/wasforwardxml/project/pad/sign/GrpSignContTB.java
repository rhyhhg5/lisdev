package com.sinosoft.wasforwardxml.project.pad.sign;

import java.util.List;

import org.jdom.Document;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDFinBankDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.finfee.TempFeeUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.vdb.LCGrpContDBSet;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LDFinBankSet;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.GRPPayInfo;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCGRPContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputGrpDataTable;
import com.sinosoft.wasforwardxml.project.pad.obj.TempFee;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class GrpSignContTB extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCGRPContTable cLCGRPContTable;

	public OutputGrpDataTable cOutputGrpDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码

	String serialno = "";

	String getnoticeno = "";

	private VData mResult = new VData();

	String merror = "";

//	public TempFee cTempFee;// 收费信息
	public GRPPayInfo cGRPPayInfo;
	
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad团单出单收费签单过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCGRPContList = cMsgInfos.getBodyByFlag("LCGRPContTable");
			if (tLCGRPContList == null || tLCGRPContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCGRPContTable = (LCGRPContTable) tLCGRPContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCGRPContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCGRPContTable.getManageCom();
			mGlobalInput.ComCode = cLCGRPContTable.getManageCom();

			List tGRPPayInfo = cMsgInfos.getBodyByFlag("GRPPayInfo");
			if (tGRPPayInfo == null || tGRPPayInfo.size() != 1) {
				errLog("获取收费信息失败。");
				return false;
			}
			cGRPPayInfo = (GRPPayInfo) tGRPPayInfo.get(0);
			
			if(!checkInfo()){
				errLog(merror);
				return false;
			}
			
			if(!checkAppnt(cLCGRPContTable.getPrtNo())){
				errLog(merror);
				return false;
			}
			
			if (!updateTempFee()) {
				errLog(merror);
				return false;
			}
			//

			String sql = "select grpcontno from lcgrpcont where prtno = '" + cLCGRPContTable.getPrtNo() +"' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单不存在！");
				return false;
			}

			sql = "select 1 from ljtempfee where Othernotype='5' and Confflag='0' and Enteraccdate is not null and Otherno='"
					+ cLCGRPContTable.getPrtNo() + "'";
			tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
				errLog("保单未缴费！");
				return false;
			}
			if (!signCount()) {
				errLog(merror);
				return false;
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputGrpDataTable", cOutputGrpDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select grpcontno,prtno,amnt,prem,stateflag from lcgrpcont where prtno = '"
				+ cLCGRPContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputGrpDataTable = new OutputGrpDataTable();
			cOutputGrpDataTable.setGrpContNo(tSSRS.GetText(1, 1));
			cOutputGrpDataTable.setPrtNo(tSSRS.GetText(1, 2));
			cOutputGrpDataTable.setAmnt(tSSRS.GetText(1, 3));
			cOutputGrpDataTable.setPrem(tSSRS.GetText(1, 4));
			cOutputGrpDataTable.setStateFlag(tSSRS.GetText(1, 5));

		}
		return true;

	}

	/**
	 * 更新暂收费信息
	 * 
	 * @return boolean
	 */
	private boolean updateTempFee() {
		MMap mMap = new MMap();

		String amount = cGRPPayInfo.getPayMoney();
		String enteraccdate = cGRPPayInfo.getPayEntAccDate();
		String prtno = cLCGRPContTable.getPrtNo();
		String insManagecom = cGRPPayInfo.getManageCom();//归集账户管理机构
		String Insbankaccno = cGRPPayInfo.getInsbankaccno();//归集账户
		String insBankcode = cGRPPayInfo.getInsbankcode();//归集账户银行编码
		if(insManagecom==null||"".equals(insManagecom)){
			merror = "归集账户管理机构不能为空！";
			return false;
		}
		if(Insbankaccno==null||"".equals(Insbankaccno)){
			merror = "归集账户不能为空！";
			return false;
		}
		if(insBankcode==null||"".equals(insBankcode)){
			merror = "归集账户银行编码不能为空！";
			return false;
		}
		 LDFinBankDB tLDFinBankDB = new LDFinBankDB();
		 tLDFinBankDB.setManageCom(insManagecom);
		 tLDFinBankDB.setBankAccNo(Insbankaccno);
		 tLDFinBankDB.setBankCode(insBankcode);
		 LDFinBankSet tLDFinBankSet = tLDFinBankDB.query();
		 if(tLDFinBankSet.size()<=0){
			    merror = "归集账户相关信息不匹配！";
				return false; 
		 }
		String sql1 = "select 1 from ljtempfee where otherno = '" + prtno + "' and enteraccdate is not null";
		SSRS tSSR = new ExeSQL().execSQL(sql1);
		if (tSSR == null || tSSR.getMaxRow() <= 0) {

			LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
			tLJTempFeeDB.setOtherNo(prtno);
			LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

			LCGrpContDB tLcGrpContDB = new LCGrpContDB();
			tLcGrpContDB.setPrtNo(prtno);
			LCGrpContSet tLCGrpContSet = tLcGrpContDB.query();
			if (tLCGrpContSet.size() != 1) {
				merror = "保单数据有问题！";
				return false;
			}
			
			
			
			String tempfeeno = prtno + "801";
			String strSql1 = "select a.riskcode,b.riskname,sum(a.prem)-(Select nvl(Sum(Mo.Paymoney),0) From Ljtempfee Mo Where Mo.Riskcode = a.Riskcode And Mo.Otherno = '"
					+ prtno
					+ "' And Exists (Select 1 From Ljtempfeeclass Where Tempfeeno = Mo.Tempfeeno And Paymode = 'YS')) from lcgrppol a,lmrisk b where prtno='"
					+ prtno
					+ "' and a.riskcode=b.riskcode group by a.riskcode,b.riskname";
			String strSql2 = "select paymode,codename,prem - (select nvl(Sum(Mo.Paymoney),0) from ljtempfeeclass mo where exists (select 1 from ljtempfee where tempfeeno=mo.tempfeeno and otherno='"
					+ prtno
					+ "')),'','','',bankcode,bankaccno,accname from lcgrpcont,ldcode where prtno='"
					+ prtno + "' and  codetype='paymode' and paymode=code";
			SSRS ssrs1 = new ExeSQL().execSQL(strSql1);
			SSRS ssrs2 = new ExeSQL().execSQL(strSql2);
			
			double PayMoney2 = Double.parseDouble(ssrs2.GetText(1, 3));
			if(PayMoney2!=Double.parseDouble(amount)){
				merror = "传入的保费与计算的保费不一致！";
				return false;
			}
			
			String serNo = PubFun1.CreateMaxNo("SERIALNO",
					cLCGRPContTable.getManageCom());
			double CashValue = 0;
			// 校验缴费方式，总金额，
			LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
			for (int i = 1; i <= ssrs2.getMaxRow(); i++) {
				// 暂收分类表记录集
				LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
				tLJTempFeeClassSchema.setTempFeeNo(tempfeeno);
				tLJTempFeeClassSchema.setPayMode(ssrs2.GetText(i, 1));
				tLJTempFeeClassSchema.setPayDate(enteraccdate);
				tLJTempFeeClassSchema.setPayMoney(ssrs2.GetText(i, 3));
				tLJTempFeeClassSchema.setConfFlag("0");
				tLJTempFeeClassSchema.setSerialNo(serNo);
				tLJTempFeeClassSchema.setChequeNo(ssrs2.GetText(i, 4));
				tLJTempFeeClassSchema.setChequeDate(ssrs2.GetText(i, 5));
				tLJTempFeeClassSchema.setEnterAccDate(enteraccdate);
				tLJTempFeeClassSchema.setManageCom(insManagecom);
				tLJTempFeeClassSchema.setPolicyCom(cLCGRPContTable
						.getManageCom());
				tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
				tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
				tLJTempFeeClassSchema.setBankCode(ssrs2.GetText(i, 7));
				tLJTempFeeClassSchema.setBankAccNo(ssrs2.GetText(i, 8));
				tLJTempFeeClassSchema.setAccName(ssrs2.GetText(i, 9));
				tLJTempFeeClassSchema.setInsBankCode(insBankcode);
				tLJTempFeeClassSchema.setInsBankAccNo(Insbankaccno);
				tLJTempFeeClassSchema.setOperator(Operator);
				tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
				tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
				tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
				tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
				tLJTempFeeClassSchema.setCashier(Operator);
				tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
				CashValue = CashValue + Double.parseDouble(ssrs2.GetText(i, 3));
			}
			for (int i = 1; i <= ssrs1.getMaxRow(); i++) {
				// 暂收表信息记录集
				LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
				tLJTempFeeSchema.setTempFeeNo(tempfeeno);
				tLJTempFeeSchema.setTempFeeType("1");
				tLJTempFeeSchema.setRiskCode(ssrs1.GetText(i, 1));
				tLJTempFeeSchema.setAgentCode(cLCGRPContTable.getAgentCode());
				LAAgentDB tLAAgentDB = new LAAgentDB();
				tLAAgentDB.setAgentCode(cLCGRPContTable.getAgentCode());
				if (tLAAgentDB.getInfo() == false) {
					merror = "没有找到代理人编码：" + cLCGRPContTable.getAgentCode()
							+ "对应的代理人组别";
					return false;
				}
				tLJTempFeeSchema.setAgentGroup(tLAAgentDB.getBranchCode());
				tLJTempFeeSchema
						.setAPPntName(tLCGrpContSet.get(1).getGrpName());
				tLJTempFeeSchema.setPayDate(enteraccdate);
				tLJTempFeeSchema.setEnterAccDate(enteraccdate); // 到帐日期
				tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
				tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
				tLJTempFeeSchema.setPayMoney(ssrs1.GetText(i, 3));
				tLJTempFeeSchema.setManageCom(cLCGRPContTable.getManageCom());
				tLJTempFeeSchema.setOtherNo(prtno);
				tLJTempFeeSchema.setOtherNoType("5");
				tLJTempFeeSchema.setOperator(Operator);
				tLJTempFeeSchema.setCashier(Operator);

				tLJTempFeeSchema.setPolicyCom(cLCGRPContTable.getManageCom());
				tLJTempFeeSchema.setSerialNo(serNo);
				tLJTempFeeSchema.setConfFlag("0");
				tLJTempFeeSchema.setOperator(Operator);
				tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
				tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
				tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
				tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
				tLJTempFeeSet.add(tLJTempFeeSchema);
			}

			mMap.put(tLJTempFeeSet, "DELETE&INSERT");
			mMap.put(tLJTempFeeClassSet, "DELETE&INSERT");
			VData mData = new VData();
			mData.add(mMap);
			PubSubmit pubSubmit = new PubSubmit();
			if (!pubSubmit.submitData(mData, null)) {
				merror = "更新收费信息失败";
				return false;
			}
		}
		return true;

	}
	
	
	private boolean signCount(){
		VData cInputData = new VData();
        String cOperate = "";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = Operator;
        tGlobalInput.ManageCom = cLCGRPContTable.getManageCom();
        tGlobalInput.ComCode = cLCGRPContTable.getManageCom();
        cInputData.add(tGlobalInput);
        String sql = "select grpcontno from lcgrpcont where prtno='"+cLCGRPContTable.getPrtNo()+"' with ur";
        String grpcontno = new ExeSQL().getOneValue(sql);
        if(grpcontno==null || "".equals(grpcontno)){
        	merror = "获取保单号失败";
        	return false;
        }
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContSchema tLCContSchema = new LCGrpContSchema();
        tLCContSchema.setGrpContNo(grpcontno);
        tLCGrpContSet.add(tLCContSchema);
        cInputData.add(tLCGrpContSet);
        LCGrpContSignBL lCGrpContSignBL = new LCGrpContSignBL();
        if(!lCGrpContSignBL.submitData(cInputData, cOperate)){
        	if (lCGrpContSignBL.mErrors.needDealError()) {
        		this.mErrors.copyAllErrors(lCGrpContSignBL.mErrors);
                for (int m = 0; m < lCGrpContSignBL.mErrors.getErrorCount(); m++) {
                	
                	merror=lCGrpContSignBL.mErrors.getError(0).errorMessage;
                	System.out.println(lCGrpContSignBL.mErrors.getError(0).errorMessage);
                	return false;
                }
        	}else{
	        	merror = "生成签单数据失败";
		    	return false;
        	}
        }
        return true;
	}

	private boolean checkInfo(){
		if(cLCGRPContTable.getPrtNo()==null || "".endsWith(cLCGRPContTable.getPrtNo())){
			merror = "印刷号不能为空";
			return false;
		}
		LCGrpContDB clcgrpcontdb = new LCGrpContDB();
		clcgrpcontdb.setPrtNo(cLCGRPContTable.getPrtNo());
		LCGrpContSet clcgrpcontset = clcgrpcontdb.query();
		if(clcgrpcontset.size()<1){
			merror = "该保单不存在";
			return false;
		}
//		ES_DOC_MAINSet tES_DOC_MAINSet=new ES_DOC_MAINSet();
//		ES_DOC_MAINDB tES_DOC_MAINDB=new ES_DOC_MAINDB();
//		tES_DOC_MAINSet=tES_DOC_MAINDB.executeQuery("select * from ES_DOC_MAIN where doccode='"+ cLCGRPContTable.getPrtNo()+ "' and subtype='TB31' with ur ");
//		if(tES_DOC_MAINSet.size()<1){
//			merror = "扫描件尚未上载完成，请稍后点击发送！";
//			return false;
//		}
		
		LCGrpContSchema clcgrpcontschema = new LCGrpContSchema();
		clcgrpcontschema = clcgrpcontset.get(1);
		String sql = "select 1 from lcgrpcont where prtno = '"
				+ cLCGRPContTable.getPrtNo() + "' and stateflag='1' and appflag='1' with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS != null && tSSRS.getMaxRow() == 1) {
			merror = "该保单已经承保！";
			return false;
		}
		String sql2 = "select 1 from lcgrpcont where prtno='"+cLCGRPContTable.getPrtNo()+"' and approveflag='9' and uwflag='9' with ur";
		SSRS res2 = new ExeSQL().execSQL(sql2);
		if(res2 == null || res2.getMaxRow()<1){
			merror = "该保单不处于收费签单状态";
			return false;
		}
		
		if(cLCGRPContTable.getGrpContNo()==null || "".equals(cLCGRPContTable.getGrpContNo())){
			merror = "保单号不能为空";
			return false;
		} 
		if(cLCGRPContTable.getManageCom()==null || "".equals(cLCGRPContTable.getManageCom())){
			merror = "管理机构不能为空";
			return false;
		}else{
			if(cLCGRPContTable.getManageCom().length()!=8){
				merror = "管理机构必须为8位机构";
				return false;
			}
		}
		if(cLCGRPContTable.getSaleChnl()==null || "".equals(cLCGRPContTable.getSaleChnl())){
			merror = "销售渠道不能为空";
			return false;
		}
		if(cLCGRPContTable.getAgentCode()==null || "".equals(cLCGRPContTable.getAgentCode())){
			merror = "业务员代码不能为空";
			return false;
		}else{
			if(!clcgrpcontschema.getAgentCode().equals(cLCGRPContTable.getAgentCode())){
				merror = "代理人和保单代理人不一致";
				return false;
			}
		}
		if(!clcgrpcontschema.getManageCom().equals(cLCGRPContTable.getManageCom())){
			merror = "管理机构和保单管理机构不一致";
			return false;
		}
		if(cLCGRPContTable.getCValiDate()==null || "".equals(cLCGRPContTable.getCValiDate())){
			merror = "生效日期不能为空";
			return false;
		}
		if(cLCGRPContTable.getPayIntv()==null || "".equals(cLCGRPContTable.getPayIntv())){
			merror = "缴费频次不能为空";
			return false;
		}
		if(cLCGRPContTable.getPayMode()==null || "".equals(cLCGRPContTable.getPayMode())){
			merror = "缴费方式不能为空";
			return false;
		}
		if(cLCGRPContTable.getPolApplyDate()==null || "".equals(cLCGRPContTable.getPolApplyDate())){
			merror = "保单申请日期不能为空";
			return false;
		}
		if(cGRPPayInfo.getPayMode()==null || "".equals(cGRPPayInfo.getPayMode())){
			merror = "缴费方式不能为空";
			return false;
		}
		if(cGRPPayInfo.getPayMoney()==null || "".equals(cGRPPayInfo.getPayMoney())){
			merror = "缴费总金额不能为空";
			return false;
		}
		if(cGRPPayInfo.getPayEntAccDate()==null || "".equals(cGRPPayInfo.getPayEntAccDate())){
			merror = "到账时间不能为空";
			return false;
		}
		return true;
	}
	
	private boolean checkAppnt(String prtno){
		String sqlappnt = "select appntno from lcgrpcont where prtno='"+prtno+"' with ur";
		String appntno = new ExeSQL().getOneValue(sqlappnt);
		if ((appntno == null) || "".equals(appntno) || (appntno.length() != 8))
        {
            merror = "团体客户号有误!";
            return false;
        }
		StringBuffer sql = new StringBuffer();
        int numString = appntno.length();
        sql.append(" select max(int(substr(grpcontno," + (numString + 1)
                + "))) from ( ");
        sql.append(" select grpcontno from lcgrpcont where appntno='"
                + appntno + "' and appflag in('1','9') ");
        sql.append(" union ");
        sql.append(" select grpcontno from lbgrpcont where appntno='"
                + appntno + "' ");
        sql.append(" union ");
        sql.append(" select grpcontno from lobgrpcont where appntno='"
                + appntno + "' and appflag in('1','9') ");
        sql.append(" ) as X  ");
        System.out.println("SQL" + sql.toString());
        SSRS mSSRS = (new ExeSQL()).execSQL(sql.toString());
        LDGrpDB tLDGrpDB = new LDGrpDB();
        LDGrpSet tLDGrpSet = new LDGrpSet();
        tLDGrpDB.setCustomerNo(appntno);
        tLDGrpSet = tLDGrpDB.query();
        if (tLDGrpSet.size() != 1)
        {
        	merror = "团体客户号有误!";
            return false;
        }
        int newAppntnum = Integer.parseInt(mSSRS.GetText(1, 1));
        newAppntnum = newAppntnum+1;
        
        String newnum = String.valueOf(newAppntnum);
        int length = 6;
        if (newnum.length() < length)
        {
            //tMaxNo = "0" + tMaxNo; //补齐两位
            //已修改为六位长度
        	newnum = PubFun.LCh(newnum, "0", length);
        }
        String newgrpcontno = appntno + newnum;
        String sql1 = "select 1 from lcgrpcont where grpcontno='"
                + newgrpcontno + "' with ur ";
        SSRS res = new ExeSQL().execSQL(sql1);
        if(res!=null && res.MaxRow>0){
        	merror = "该投保客户有未完成的签单保单，保单号："+newgrpcontno+"，请稍后再试！";
        	return false;
        }
        return true;
        
	}
}
