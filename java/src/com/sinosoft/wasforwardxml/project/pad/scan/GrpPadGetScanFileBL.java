package com.sinosoft.wasforwardxml.project.pad.scan;

import java.io.IOException;
import java.net.SocketException;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class GrpPadGetScanFileBL {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mPrtNo = "";

	private TransferData mTransferData;

	private FTPTool tFTPTool;

	private String localPath;

	private String tFileName;

	private String serverpath;

	private VData mVData;
	
	private String mCardFlag;
	
	private String mSubType;

	public boolean submitData(VData cInputData, String operate) {

		mVData = cInputData;

		if (!getInputData(mVData)) {
			return false;
		}

		if (!getConn()) {
			return false;
		}

		if (!getFile()) {
			try {
				tFTPTool.logout();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}

		deleteFile();

		if (!DealGrpFile()) {
			return false;
		}

		return true;
	}

	private void deleteFile() {
		try {
			tFTPTool.deleteFile(serverpath + tFileName);
			tFTPTool.logout();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean DealGrpFile() {
		DealGrpFile tDealGrpFile = new DealGrpFile();
		VData tVData = new VData();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("PrtNo", mPrtNo);
		transferData.setNameAndValue("LocalPath", localPath);
	    transferData.setNameAndValue("FileName", tFileName);
	    transferData.setNameAndValue("CardFlag",mCardFlag);
	    transferData.setNameAndValue("SubType",mSubType);
	    
		tVData.add(mGI);
		tVData.add(transferData);

		try {
			if (!tDealGrpFile.submitData(tVData, "")) {
				CError tError = new CError();
				tError.moduleName = "GrpPadGetScanFileBL";
				tError.functionName = "DealGrpFile";
				tError.errorMessage = tDealGrpFile.mErrors.getFirstError();
				mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	private boolean getFile() {
		String getPath = "";
		if("pg".equals(mCardFlag)){
			getPath = "select codename ,codealias from ldcode where codetype='padscan' and code='Server/LocalPath' ";
		}else if("cd".equals(mCardFlag)){
			getPath = "select codename ,codealias from ldcode where codetype='cardscan' and code='Server/LocalPath' ";
		}else if("ds".equals(mCardFlag)){
			getPath = "select codename ,codealias from ldcode where codetype='dsscan' and code='Server/LocalPath' ";
		}
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "GrpPadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp服务器路径未配置";
			mErrors.addOneError(tError);
			return false;
		}
		serverpath = tPathSSRS.GetText(1, 1);
		localPath = tPathSSRS.GetText(1, 2)+PubFun.getCurrentDate2()+"/";
//		localPath="E:\\ftp\\download\\"+PubFun.getCurrentDate2()+"\\";
		
		String sql = "select 1 from es_doc_main where doccode like '" + mPrtNo + "%' and subtype='"+mSubType+"' ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			tFileName = mPrtNo + "101.zip";
		} else {
			tFileName = mPrtNo + ".zip";
		}
		boolean downflag = true;
		
		System.out.println("下载开始时间" + new Date());
		String time1=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String tsq1 = "INSERT INTO ldtimetest VALUES ('PAD',current time,'"+time1+"','"+mPrtNo+"','包下载',NULL) ";
    	new ExeSQL().execUpdateSQL(tsq1);
		downflag = tFTPTool.downloadFileExists(serverpath, localPath, tFileName);
		System.out.println("下载结束时间" + new Date());
		String time2=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
		String tsq2 = "INSERT INTO ldtimetest VALUES ('PAD',current time,'"+time2+"','"+mPrtNo+"','包结束',NULL) ";
    	new ExeSQL().execUpdateSQL(tsq2);
		System.out.println("下载成功标识为：" + downflag);
		if (!downflag) {
			System.out.println(tFTPTool.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "GrpPadGetScanFileBL";
			tError.functionName = "getFile";
			tError.errorMessage = tFTPTool.mErrors.getFirstError();
			mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	private boolean getInputData(VData inputData) {
		mGI = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) inputData.getObjectByObjectName("TransferData", 0);
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mCardFlag = (String) mTransferData.getValueByName("CardFlag");
		mSubType = (String) mTransferData.getValueByName("SubType");
		System.out.println("上传扫描件印刷号为：" + mPrtNo);
		System.out.println("保单类型为：" + mCardFlag);
		System.out.println("上传扫描件为：" + mSubType);
		return true;
	}

	private boolean getConn() {
		String getIPPort = "";
		String getUserPs = "";
		if("pg".equals(mCardFlag)){
			getIPPort = "select codename ,codealias from ldcode where codetype='padscan' and code='IP/Port' ";
			getUserPs = "select codename ,codealias from ldcode where codetype='padscan' and code='User/Pass' ";
		}else if("cd".equals(mCardFlag)){
			getIPPort = "select codename ,codealias from ldcode where codetype='cardscan' and code='IP/Port' ";
			getUserPs = "select codename ,codealias from ldcode where codetype='cardscan' and code='User/Pass' ";
		}else if("ds".equals(mCardFlag)){
			getIPPort = "select codename ,codealias from ldcode where codetype='dsscan' and code='IP/Port' ";
			getUserPs = "select codename ,codealias from ldcode where codetype='dsscan' and code='User/Pass' ";
		}
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "GrpPadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp配置有误";
			mErrors.addOneError(tError);
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 2), tUPSSRS.GetText(1, 1),
				Integer.parseInt(tIPSSRS.GetText(1, 2)));

		try {
			if (!tFTPTool.loginFTP()) {
				CError tError = new CError();
				tError.moduleName = "GrpPadGetScanFileBL";
				tError.functionName = "getConn";
				tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				mErrors.addOneError(tError);
				return false;
			}
		} catch (SocketException e) {
			CError tError = new CError();
			tError.moduleName = "GrpPadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			CError tError = new CError();
			tError.moduleName = "GrpPadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		// 160000000001

		GrpPadGetScanFileBL tPadGetFile = new GrpPadGetScanFileBL();
		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("PrtNo", "PD00000192880");
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.Operator = "wn";
		mGlobalInput.AgentCom = "";
		mGlobalInput.ManageCom = "86110000";
		mGlobalInput.ComCode = "86110000";

		VData tVData = new VData();
		tVData.add(transferData1);
		tVData.add(mGlobalInput);
		tPadGetFile.submitData(tVData, "");
	}

}
