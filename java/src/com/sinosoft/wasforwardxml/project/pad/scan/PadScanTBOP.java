package com.sinosoft.wasforwardxml.project.pad.scan;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.project.pad.query.PadQueryStateBL;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class PadScanTBOP extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCContTable cLCContTable;

	public OutputDataTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	private String mPrtNo = ""; // 印刷号

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad上传扫描件过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();
			mPrtNo = cLCContTable.getPrtNo();
			if ("".equals(mPrtNo) || mPrtNo == null) {
				errLog("保单印刷号有误，请检查");
				return false;
			}

		    String sql = "select distinct picpathftp  from es_doc_pages where docid  in (select docid from es_doc_main where doccode like '" + mPrtNo + "%' and subtype='TB28') ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			// if (tSSRS.getMaxRow() > 0) {
			// modify by zxs 2018-05-08
			if (tSSRS.getMaxRow() > 1) {
				errLog("该投保单已上传过扫描件");
				return false;
			}

			PadGetScanFileBL tPadGetFile = new PadGetScanFileBL();
			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("PrtNo", mPrtNo);

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(mGlobalInput);

			if (!tPadGetFile.submitData(tVData, "")) {
				errLog(tPadGetFile.mErrors.getFirstError());
				return false;
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,codename('stateflag',stateflag) from lccont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setPrtNo(mPrtNo);
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			if ("0".equals(tSSRS.GetText(1, 5))) {
				PadQueryStateBL tPadQueryStateBL = new PadQueryStateBL();
				String stateflag = tPadQueryStateBL.submitData(tSSRS.GetText(1, 1), tSSRS.GetText(1, 4));
				cOutputDataTable.setStateFlag(stateflag);
			} else {
				cOutputDataTable.setStateFlag(tSSRS.GetText(1, 5));
			}

		}
		return true;

	}

}
