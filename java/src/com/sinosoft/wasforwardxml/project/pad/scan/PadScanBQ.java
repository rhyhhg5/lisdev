package com.sinosoft.wasforwardxml.project.pad.scan;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.BQReturnInfo;
import com.sinosoft.wasforwardxml.project.pad.obj.EdorAPPInfo;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class PadScanBQ extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public EdorAPPInfo mEdorAPPInfo;

	public BQReturnInfo mBQReturnInfo;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息
	
	private String mEdorAcceptNo = ""; //保全受理号

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad上传扫描件过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tEdorAPPInfoList = cMsgInfos.getBodyByFlag("EdorAPPInfo");
			if (tEdorAPPInfoList == null || tEdorAPPInfoList.size() != 1) {
				errLog("获取工单信息失败。");
				return false;
			}
			mEdorAPPInfo = (EdorAPPInfo) tEdorAPPInfoList.get(0); // 获取保单信息，只有一个保单
			mEdorAcceptNo = mEdorAPPInfo.getEdorAcceptNo();
			if("".equals(mEdorAcceptNo)||mEdorAcceptNo == null){
				errLog("保全受理号有误，请检查");
				return false;
			}
			
			String selectNO = "select 1 from lgwork where workno = '"+mEdorAcceptNo+"'";
			String tSSRSNO = new ExeSQL().getOneValue(selectNO);
			if (!"1".equals(tSSRSNO)) {
				errLog("保全受理号无效，请检查");
				return false;
			}
			
			String sql = "select 1 from es_doc_main where doccode = '"
					+ mEdorAcceptNo + "' and subtype='BQ01' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS.getMaxRow() > 0) {
				errLog("该投保单已上传过扫描件");
				return false;
			}

			PadGetBQScanFileBL tPadGetFile = new PadGetBQScanFileBL();;
			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("EdorAcceptNo", mEdorAcceptNo);

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(mGlobalInput);

			if(!tPadGetFile.submitData(tVData,Operator,MsgType )){
				if (!PrepareInfo("Fail")) {
					errLog(mError);
					return false;
				}	
				errLog(tPadGetFile.mErrors.getFirstError());
				return false;
			}else{
				if (!PrepareInfo("Success")) {
					errLog(mError);
					return false;
				}				
			}
			
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("BQReturnInfo", mBQReturnInfo);

	}

	// 获取保单信息
	public boolean PrepareInfo(String state) {
		mBQReturnInfo = new BQReturnInfo();
		mBQReturnInfo.setEdorAcceptNo(mEdorAcceptNo);
		mBQReturnInfo.setState(state);
		return true;

	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}

}
