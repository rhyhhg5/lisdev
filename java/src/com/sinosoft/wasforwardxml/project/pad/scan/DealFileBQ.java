package com.sinosoft.wasforwardxml.project.pad.scan;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.BQFileInfoForm;
import com.sinosoft.wasforwardxml.project.pad.util.AnimatedGifEncoder;
import com.sinosoft.wasforwardxml.project.pad.util.DeCompressZip;
import com.sinosoft.wasforwardxml.project.pad.util.IOTrans;
import com.sinosoft.wasforwardxml.project.pad.util.IndexMap;
import com.sinosoft.wasforwardxml.project.pad.util.PraseXmlUtil;
import com.sinosoft.xreport.dl.test;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.TIFFEncodeParam;

public class DealFileBQ {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mEdorAcceptNo = "";

	private TransferData mTransferData;

	private String mLocalPath;

	private String mFileName;

	private String operator = "";

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private VData mResult = new VData();

	private static final long serialVersionUID = 1L;

	private ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();

	private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();

	private ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

	private MMap map = new MMap();
	
	private IndexMap iMap = new IndexMap();
	
	private String HostName = "";

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		try {
			if (!deal(mLocalPath, mFileName)) {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	private boolean getInputData(VData inputData) {
		mGI = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) inputData.getObjectByObjectName(
				"TransferData", 0);
		mEdorAcceptNo = (String) mTransferData.getValueByName("EdorAcceptNo");
		mLocalPath = (String) mTransferData.getValueByName("LocalPath");
		mFileName = (String) mTransferData.getValueByName("FileName");
		operator = mGI.Operator;
		System.out.println("上传扫描件工单号为：" + mEdorAcceptNo);
		return true;
	}

	private boolean deal(String path, String file) throws Exception {

		System.out.println("path:" + path + " ,file:" + file + ",edoracceptno:"
				+ mEdorAcceptNo);
		String dir = path + file;// 完整路径

		boolean flag = true;

		
		// 1、解压zip包
		String unRar="select codename,codealias from ldcode where codetype='padscan' and code='UnRarPath' ";
		SSRS tPathSSRS = new ExeSQL().execSQL(unRar);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "PadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "未设置文件解压路径";
			mErrors.addOneError(tError);
			return false;
		}
		String unpath = tPathSSRS.GetText(1, 1);
		HostName = tPathSSRS.GetText(1, 2);
		if (!newFolder(unpath))
        {
			mErrors.addOneError("新建保存扫描件目录失败");
            return false;
        }		
		flag = unZip(dir, unpath);
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "DealFile";
			tError.functionName = "deal";
			tError.errorMessage = "文件解压失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("**************** ③解压zip包完毕 ****************");

		// 2、解析xml文件 生成扫描一套表
		
		String sql = "select 1 from es_doc_main where doccode like '" + mEdorAcceptNo + "%' and subtype='BQ01' ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			unpath = unpath + mEdorAcceptNo + "101";
		} else {
			unpath = unpath + mEdorAcceptNo;
		}
		String fileDetail = unpath + "/" + mEdorAcceptNo + ".xml";
	    if(!parseXml(fileDetail)){
	    	deletefile(unpath);
			deleteFiles(path, file);
	    	return false;
	    }
		String tEdorAcceptNo = (String) iMap.get("EdorAcceptNo");
		String tManageCom = (String) iMap.get("MANAGECOM");
		if (tEdorAcceptNo.equals("") || !tEdorAcceptNo.equals(mEdorAcceptNo)) {
			CError tError = new CError();
			tError.moduleName = "DealFile";
			tError.functionName = "deal";
			tError.errorMessage = "解析xml文件失败 或 扫描件与印刷号不匹配";
			this.mErrors.addOneError(tError);
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		System.out.println("**************** ④解析xml文件完毕 ****************");

		// 3.1 将jpg图片转换为tif 文件 删除jpg文件
//		flag = convertImage(unpath);
//		if (!flag) {
//			CError tError = new CError();
//			tError.moduleName = "DealFile";
//			tError.functionName = "deal";
//			tError.errorMessage = "图片转换失败";
//			this.mErrors.addOneError(tError);
//			deletefile(unpath);
//			deleteFiles(path, file);
//			return false;
//		}
//		System.out.println("**************** ⑤图片转换完毕 ****************");

		// 4、处理扫描一套表
		flag = sendtoCore(tEdorAcceptNo, tManageCom);
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "DealFile";
			tError.functionName = "deal";
			tError.errorMessage = "扫描数据上传核心错误";
			this.mErrors.addOneError(tError);
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		System.out.println("**************** ⑥保单核心处理完毕 ****************");
		
		String sql1 = "select 1 from es_doc_main where doccode like '" + mEdorAcceptNo + "%' and subtype='BQ01' ";
		SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
		if (tSSRS1.getMaxRow() > 1) {
			if (!insert(tEdorAcceptNo)) {
				CError tError = new CError();
				tError.moduleName = "DealFile";
				tError.functionName = "deal";
				tError.errorMessage = "扫描件处理错误";
				this.mErrors.addOneError(tError);
				deletefile(unpath);
				deleteFiles(path, file);
				return false;
			}
		}
		
		return true;

	}

	
	/**
	 * 修改扫描件相应信息
	 */
	public boolean insert(String edoracceptno) {
		VData Result = new VData();
		PubSubmit tPubSubmit = new PubSubmit();
		MMap tmap = new MMap();
		String getTbSql = " select docid,numpages from es_doc_main where doccode like '" + edoracceptno
				+ "%' and subtype = 'BQ01'  order by makedate,maketime asc fetch first 1 rows only with ur";

		String getTbSql1 = " select docid,numpages from es_doc_main where doccode like '" + edoracceptno
				+ "%' and subtype = 'BQ01'  order by makedate,maketime desc fetch first 1 rows only with ur";
		SSRS ssrs = new ExeSQL().execSQL(getTbSql);
		String tbDocid = ssrs.GetText(1, 1);
		int DocPages = Integer.parseInt(ssrs.GetText(1, 2));

		SSRS ssrs1 = new ExeSQL().execSQL(getTbSql1);
		String tbDocid1 = ssrs1.GetText(1, 1);
		int DocPages1 = Integer.parseInt(ssrs1.GetText(1, 2));

		String sql1 = "delete from es_doc_main where docid = '" + tbDocid1 + "' and subtype='BQ01'";
		String sql2 = "update es_doc_main set numpages='" + (DocPages + DocPages1)
				+ "',modifydate=current date, modifytime=current time where Docid='" + tbDocid
				+ "' and subtype='BQ01' with ur";

		tmap.put(sql1, "DELETE");
		tmap.put(sql2, "UPDATE");
		for (int i = 1; i <= DocPages1; i++) {
			String sql3 = "update es_doc_pages set docid = '" + tbDocid + "',pagecode ='" + (DocPages + i)
					+ "',modifydate=current date, modifytime=current time where docid ='" + tbDocid1
					+ "' and pagecode ='" + (i) + "' with ur";
			tmap.put(sql3, "UPDATE");
		}
		String sql4 = "delete from es_doc_relation where docid='" + tbDocid1 + "' and subtype='BQ01' with ur";
		tmap.put(sql4, "DELETE");
		Result.clear();
		Result.add(tmap);
		if (!tPubSubmit.submitData(Result, "")) {
			System.out.println("数据出错");
			return false;
		} else {
			return true;
		}

	}

	/**
	 * 解压zip包
	 */
	public boolean unZip(String file, String unpath) {
		DeCompressZip dec = new DeCompressZip();
		if (!dec.DeCompress(file, unpath)) {
			return false;
		}
		return true;
	}

	/**
	 * 解析xml报文
	 */
	public boolean parseXml(String fileDetail) {
		try {
			
			// 将xml文件转换为字符串形式的
			InputStream mIs = new FileInputStream(fileDetail);
			byte[] mInXmlBytes = IOTrans.InputStreamToBytes(mIs);
			String mInXmlStr = new String(mInXmlBytes, "gbk");
			System.out.println(mInXmlStr);
			// 将xml映射成form对象
			BQFileInfoForm form = null;
			try{
				form = (BQFileInfoForm) PraseXmlUtil.praseXml(
						"//FILEINFO", mInXmlStr, new BQFileInfoForm());
			}catch (Exception e) {
				CError tError = new CError();
				tError.moduleName = "DealFile";
				tError.functionName = "deal";
				tError.errorMessage = "扫描文件不正确！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			System.out.println(form.getEdoracceptno());
			String agentcode = form.getAgentcode();
			String managecom = form.getManagecom();
			String date = form.getDate();
			String edoracceptno = form.getEdoracceptno();
			mES_DOC_MAINSchema.setDocCode(edoracceptno);
			mES_DOC_MAINSchema.setInputStartDate(date);
			mES_DOC_MAINSchema.setInputEndDate(date);
			mES_DOC_MAINSchema.setSubType("BQ01");
			mES_DOC_MAINSchema.setBussType("BQ");
			mES_DOC_MAINSchema.setManageCom(managecom);
			mES_DOC_MAINSchema.setVersion("01");
			mES_DOC_MAINSchema.setScanNo("0");
			mES_DOC_MAINSchema.setState("01");
			mES_DOC_MAINSchema.setDocFlag("1");
			System.out.println(operator);
			mES_DOC_MAINSchema.setOperator(operator);
			mES_DOC_MAINSchema.setScanOperator(operator);
			mES_DOC_MAINSchema.setArchiveNo(createArchiveNo(managecom));

			iMap.put("AGENTCODE", agentcode);
			iMap.put("MANAGECOM", managecom);
			iMap.put("DATE", date);
			iMap.put("EdorAcceptNo", edoracceptno);
			List pageList = new ArrayList();
			IndexMap pageImap = null;
			String[] pages = form.getPage();
			mES_DOC_MAINSchema.setNumPages(pages.length);
			
			String pithpath="select codename,codealias from ldcode where codetype='padscan' and code='ServerP/Picpath' ";
	
			// modify by zxs
			String aPicPathFTP;
			String aPicPath;
			String sql = "select 1 from es_doc_main where doccode like '" + mEdorAcceptNo + "%' and subtype='BQ01' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS.getMaxRow() > 0) {
				aPicPathFTP = new ExeSQL().execSQL(pithpath).GetText(1, 1) + "/padunrar/" + mEdorAcceptNo + "101/";
				aPicPath = new ExeSQL().execSQL(pithpath).GetText(1, 2) + "padunrar/" + mEdorAcceptNo + "101/";
			} else {
				aPicPathFTP = new ExeSQL().execSQL(pithpath).GetText(1, 1) + "/padunrar/" + mEdorAcceptNo + "/";
				aPicPath = new ExeSQL().execSQL(pithpath).GetText(1, 2) + "padunrar/" + mEdorAcceptNo + "/";
			}
			
//			aPicPathFTP = "C:/Users/LiuDe/Desktop/testsccanr/";
//			aPicPath = "xerox/EasyScan2016/padunrar/" + mEdorAcceptNo + "/";
			if (pages != null) {
				for (int i = 0; i < pages.length; i++) {
					ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
					pageImap = new IndexMap();
					String page = form.getPage()[i].trim();
					pageImap.put("PAGE", page);
					pageList.add(pageImap);
					tES_DOC_PAGESSchema.setHostName(HostName);
					tES_DOC_PAGESSchema.setPageCode(i + 1);
					tES_DOC_PAGESSchema.setPageName(page.substring(0, page.lastIndexOf('.')));
					tES_DOC_PAGESSchema.setPageSuffix(".gif");
					tES_DOC_PAGESSchema.setPageFlag("1");
					tES_DOC_PAGESSchema.setPageType("0");
					tES_DOC_PAGESSchema.setPicPath(aPicPath);
					tES_DOC_PAGESSchema.setPicPathFTP(aPicPathFTP);
					tES_DOC_PAGESSchema.setManageCom(managecom);
					tES_DOC_PAGESSchema.setOperator(operator);
					mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
				}
			}
			iMap.put("pageList", pageList);
		} catch (Exception e) {
			CError tError = new CError();
			tError.moduleName = "PadGetBQScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "解压缩后解析xml失败";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
			// TODO: handle exception
		}
		return true;
	}

	public boolean sendtoCore(String edoracceptno, String manageCom) {

		String strDocID = getMaxNo("DocID");
		mES_DOC_MAINSchema.setDocID(strDocID);
		mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
		mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
		mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
		mES_DOC_MAINSchema.setModifyTime(mCurrentTime);

		for (int i = 0; i < mES_DOC_PAGESSet.size(); i++) {
			String strPageID = getMaxNo("PageID");
			mES_DOC_PAGESSet.get(i + 1).setPageID(strPageID);
			mES_DOC_PAGESSet.get(i + 1).setDocID(strDocID);
			mES_DOC_PAGESSet.get(i + 1).setMakeDate(mCurrentDate);
			mES_DOC_PAGESSet.get(i + 1).setModifyDate(mCurrentDate);
			mES_DOC_PAGESSet.get(i + 1).setMakeTime(mCurrentTime);
			mES_DOC_PAGESSet.get(i + 1).setModifyTime(mCurrentTime);
		}
		mES_DOC_RELATIONSchema.setBussNo(edoracceptno);
		mES_DOC_RELATIONSchema.setDocCode(edoracceptno);
		mES_DOC_RELATIONSchema.setBussNoType("11");
		mES_DOC_RELATIONSchema.setDocID(strDocID);
		mES_DOC_RELATIONSchema.setBussType(mES_DOC_MAINSchema.getBussType());
		mES_DOC_RELATIONSchema.setSubType(mES_DOC_MAINSchema.getSubType());
		mES_DOC_RELATIONSchema.setRelaFlag("0");

		map.put(mES_DOC_RELATIONSchema, "INSERT");
		map.put(mES_DOC_MAINSchema, "INSERT");
		map.put(mES_DOC_PAGESSet, "INSERT");
		this.mResult.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(this.mResult, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

		return true;
	}

	public boolean convertImage(String path) {
		System.out.println(PubFun.getCurrentTime3());
		try {
			File root = new File(path);
			File[] tfile = root.listFiles();
			for (int i=0;i<tfile.length;i++) {
				File a =tfile[i];
				if (a.getName().endsWith(".jpg")) {
					// 转换成tif
					String input2 = a.toString();
					String output2 = path
							+ "/"
							+ a.getName().substring(0,
									a.getName().lastIndexOf(".")) + ".tif";

					RenderedOp src2 = JAI.create("fileload", input2);
					OutputStream os2 = new FileOutputStream(output2);

					TIFFEncodeParam param3 = new TIFFEncodeParam();
					
					param3.setCompression(TIFFEncodeParam.COMPRESSION_JPEG_TTN2);

					ImageEncoder enc2 = ImageCodec.createImageEncoder("TIFF",
							os2, param3);

					enc2.encode(src2);
					os2.flush();
					os2.close();
					// 转换成gif
					String input3 = a.toString();
					String output3 = path
							+ "/"
							+ a.getName().substring(0,
									a.getName().lastIndexOf(".")) + ".gif";

					File PNFFile = new File(input3);
					BufferedImage src1 = ImageIO.read(PNFFile);
					AnimatedGifEncoder e = new AnimatedGifEncoder();
					e.setRepeat(0);
					e.start(output3);
					e.setDelay(100); 
					e.addFrame(src1);
					e.finish();
					//ImageIO.write(src1, "gif", new File(output3)); 

				}
			}
			System.out.println(PubFun.getCurrentTime3());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	// 删除指定文件
	/*
	 * 各处理过程操作失败都需要删除本地缓存文件
	 */
	public boolean deleteFiles(String path, String file) {

		String folder = path + "/" + file;

		File zipFile = new File(folder);

		if (zipFile.exists()) {
			if (!zipFile.delete()) {
				System.out.println("111111");
				return false;
			}
		}

		return true;
	}

	// 删除目录下所有文件
	public boolean deletefile(String delpath) throws Exception {
		try {

			File file = new File(delpath);
			// 当且仅当此抽象路径名表示的文件存在且 是一个目录时，返回 true
			if (!file.isDirectory()) {
				file.delete();
			} else if (file.isDirectory()) {
				String[] filelist = file.list();
				for (int i = 0; i < filelist.length; i++) {
					File delfile = new File(delpath + "/" + filelist[i]);
					if (!delfile.isDirectory()) {
						delfile.delete();
						System.out
								.println(delfile.getAbsolutePath() + "删除文件成功");
					} else if (delfile.isDirectory()) {
						deletefile(delpath + "/" + filelist[i]);
					}
				}
				System.out.println(file.getAbsolutePath() + "删除成功");
				file.delete();
			}

		} catch (FileNotFoundException e) {
			System.out.println("deletefile() Exception:" + e.getMessage());
		}
		return true;
	}

	// 生成流水号，包含错误处理
	private String getMaxNo(String cNoType) {
		String strNo = PubFun1.CreateMaxNo(cNoType, 1);

		if (strNo.equals("") || strNo.equals("0")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UploadPrepareBQ";
			tError.functionName = "getReturnData";
			tError.errorNo = "-90";
			tError.errorMessage = "生成流水号失败!";
			this.mErrors.addOneError(tError);
			strNo = "";
		}
		return strNo;
	}
	
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
	
	public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "G" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

	public static void main(String[] args) {
		DealFileBQ a = new DealFileBQ();
		// VData tVData = new VData();
		// TransferData transferData = new TransferData();
		// transferData.setNameAndValue("PrtNo", "160000000001");
		// transferData.setNameAndValue("LocalPath", "F:\\printdata\\");
		// transferData.setNameAndValue("FileName", "160000000001.zip");
		// GlobalInput mGlobalInput = new GlobalInput();
		// mGlobalInput.Operator = "001";
		// tVData.add(mGlobalInput);
		// tVData.add(transferData);
		// try {
		// a.submitData(tVData, "");
		// ;
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// String ab = "F:\\printdata\\160000000001.zip";
		// a.ceshi(ab);
		// File m=new File("F:\\printdata\\160000000001\\");
		// try {
		// a.deletefile("F:\\printdata\\160000000001.zip");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		String path = "C:\\Users\\LiuDe\\Desktop\\testsccan\\20181127000049\\20181127000049\\";
		
		String file = "20181127000049_3.jpg";
		try {
			a.convertImage(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}