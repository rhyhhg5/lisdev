package com.sinosoft.wasforwardxml.project.pad.scan;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCGRPContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputGrpDataTable;
import com.sinosoft.wasforwardxml.project.pad.query.PadQueryStateBL;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class GrpPadScanTB extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	public LCGRPContTable cLCGRPContTable;

	public OutputGrpDataTable cOutputGrpDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息
	
	private String mPrtNo = ""; //印刷号
	
	private String mCardFlag = "";//保单标识
	
	private String mSubtype = "";//扫描件类型
	protected boolean deal(MsgCollection cMsgInfos,Document cInXmlDoc){		
		return true;
	}
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad上传扫描件过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCGRPContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCGRPContTable = (LCGRPContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCGRPContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCGRPContTable.getManageCom();
			mGlobalInput.ComCode = cLCGRPContTable.getManageCom();
			mPrtNo = cLCGRPContTable.getPrtNo();
			mCardFlag = cLCGRPContTable.getCardFlag();
			if("".equals(mPrtNo)||mPrtNo == null){
				errLog("保单印刷号有误，请检查");
				return false;
			}
			if(mCardFlag==null || "".equals(mCardFlag)){
				errLog("保单标识不能为空！");
				return false;
			}
			mSubtype = new ExeSQL().getOneValue("select codename from ldcode where codetype = 'padscan' and code ='"+mCardFlag+"' ");
			String sql = "select 1 from es_doc_main where doccode = '"+ mPrtNo + "' and subtype='"+mSubtype+"' ";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS.getMaxRow() > 0) {
				errLog("该投保单已上传过扫描件");
				return false;
			}
		
			if(!"pg".equals(mCardFlag) && !"cd".equals(mCardFlag)&&!"ds".equals(mCardFlag)){
				errLog("保单标识错误，请核实！");
				return false;
			}
			String time1=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
	    	String tsq1 = "INSERT INTO ldtimetest VALUES ('PAD',current time,'"+time1+"','"+mPrtNo+"','开始',NULL) ";
	    	new ExeSQL().execUpdateSQL(tsq1);
			GrpPadGetScanFileBL tGrpPadGetFile = new GrpPadGetScanFileBL();;
			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("PrtNo", mPrtNo);
			transferData1.setNameAndValue("CardFlag",mCardFlag);
			transferData1.setNameAndValue("SubType", mSubtype);
			
			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(mGlobalInput);

			if(!tGrpPadGetFile.submitData(tVData, "")){
				errLog(tGrpPadGetFile.mErrors.getFirstError());
				return false;
			}
			
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
			
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputGrpDataTable", cOutputGrpDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,codename('stateflag',stateflag) from lccont where prtno = '"
				+ cLCGRPContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			cOutputGrpDataTable = new OutputGrpDataTable();
			cOutputGrpDataTable.setPrtNo(mPrtNo);
		} else {
			cOutputGrpDataTable = new OutputGrpDataTable();
			cOutputGrpDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputGrpDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputGrpDataTable.setGrpContNo(tSSRS.GetText(1, 1));
			cOutputGrpDataTable.setPrtNo(tSSRS.GetText(1, 4));
			if("0".equals(tSSRS.GetText(1, 5))){
				PadQueryStateBL tPadQueryStateBL = new PadQueryStateBL();
				String stateflag = tPadQueryStateBL.submitData(tSSRS.GetText(1, 1), tSSRS.GetText(1, 4));
				cOutputGrpDataTable.setStateFlag(stateflag);
			}else{
				cOutputGrpDataTable.setStateFlag(tSSRS.GetText(1, 5));
			}
			
		}
		String time2=PubFun.getCurrentDate()+" "+PubFun.getCurrentTime();
    	String tsq2 = "INSERT INTO ldtimetest VALUES ('PAD',current time,'"+time2+"','"+mPrtNo+"','结束',NULL) ";
    	new ExeSQL().execUpdateSQL(tsq2);
		return true;
	}

}
