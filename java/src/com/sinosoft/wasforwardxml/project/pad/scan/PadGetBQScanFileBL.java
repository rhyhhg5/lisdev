package com.sinosoft.wasforwardxml.project.pad.scan;

import java.io.IOException;
import java.net.SocketException;
import java.util.Date;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class PadGetBQScanFileBL {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mEdorAcceptNo = "";

	private TransferData mTransferData;

	private FTPTool tFTPTool;

	private String localPath;

	private String tFileName;

	private String serverpath;

	private VData mVData;
	
	private String mMsgType;

	public boolean submitData(VData cInputData, String operate , String MsgType) {
		mVData = cInputData;
		mMsgType = MsgType;
		if (!getInputData(mVData)) {
			return false;
		}

		if (!getConn()) {
			return false;
		}

		if (!getFile()) {
			try {
				tFTPTool.logout();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}

		deleteFile();

		if (!dealFile()) {
			return false;
		}

		return true;
	}

	private void deleteFile() {
		try {
			tFTPTool.deleteFile(serverpath + tFileName);
			tFTPTool.logout();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean dealFile() {
//		String testpath = "C:/Users/LiuDe/Desktop/testsccan/";
		DealFileBQ tDealFileBQ = new DealFileBQ();
		VData tVData = new VData();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("EdorAcceptNo", mEdorAcceptNo);
		transferData.setNameAndValue("LocalPath", localPath);
	    transferData.setNameAndValue("FileName", tFileName);
		// modify by zxs 2018-05-08
		tVData.add(mGI);
		tVData.add(transferData);

		try {
			if (!tDealFileBQ.submitData(tVData, "")) {
				CError tError = new CError();
				tError.moduleName = "PadGetBQScanFileBL";
				tError.functionName = "dealFile";
				tError.errorMessage = tDealFileBQ.mErrors.getFirstError();
				mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	private boolean getFile() {
		String getPath;
		if(mMsgType == "PADSCAN_WX" || "PADSCAN_WX".equals(mMsgType)){
			getPath = "select codename ,codealias from ldcode where codetype='padscan_wx' and code='Server/LocalPath' ";
		}else{
			getPath = "select codename ,codealias from ldcode where codetype='padscan' and code='Server/LocalPath' ";
		}
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "PadGetBQScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp服务器路径未配置";
			mErrors.addOneError(tError);
			return false;
		}
		serverpath = tPathSSRS.GetText(1, 1);
		localPath = tPathSSRS.GetText(1, 2)+PubFun.getCurrentDate2()+"/";
		
	   // modify by ld
		String sql = "select 1 from es_doc_main where doccode like '" + mEdorAcceptNo + "%' and subtype='BQ01' ";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			tFileName = mEdorAcceptNo + "101.zip";
		} else {
			tFileName = mEdorAcceptNo + ".zip";
		}
		boolean downflag = true;
		//
		System.out.println("下载开始时间" + new Date());
		downflag = tFTPTool.downloadFileExists(serverpath, localPath, tFileName);
		System.out.println("下载结束时间" + new Date());
		System.out.println("下载成功标识为：" + downflag);
		if (!downflag) {
			System.out.println(tFTPTool.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PadGetBQScanFileBL";
			tError.functionName = "getFile";
			tError.errorMessage = tFTPTool.mErrors.getFirstError();
			mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	private boolean getInputData(VData inputData) {
		mGI = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) inputData.getObjectByObjectName("TransferData", 0);
		mEdorAcceptNo = (String) mTransferData.getValueByName("EdorAcceptNo");
		System.out.println("上传扫描件印刷号为：" + mEdorAcceptNo);
		return true;
	}

	private boolean getConn() {
		String getIPPort;
		String getUserPs;
		if(mMsgType == "PADSCAN_WX" || "PADSCAN_WX".equals(mMsgType)){
			getIPPort = "select codename ,codealias from ldcode where codetype='padscan_wx' and code='IP/Port' ";
			getUserPs = "select codename ,codealias from ldcode where codetype='padscan_wx' and code='User/Pass' ";
		}else{
			getIPPort = "select codename ,codealias from ldcode where codetype='padscan' and code='IP/Port' ";
			getUserPs = "select codename ,codealias from ldcode where codetype='padscan' and code='User/Pass' ";
		}
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "PadGetBQScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp配置有误";
			mErrors.addOneError(tError);
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 2), tUPSSRS.GetText(1, 1),
				Integer.parseInt(tIPSSRS.GetText(1, 2)));

		try {
			if (!tFTPTool.loginFTP()) {
				CError tError = new CError();
				tError.moduleName = "PadGetBQScanFileBL";
				tError.functionName = "getConn";
				tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				mErrors.addOneError(tError);
				return false;
			}
		} catch (SocketException e) {
			CError tError = new CError();
			tError.moduleName = "PadGetBQScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			CError tError = new CError();
			tError.moduleName = "PadGetBQScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		// 160000000001

		PadGetBQScanFileBL tPadGetFile = new PadGetBQScanFileBL();
		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("EdorAcceptNo", "552014071533");
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.Operator = "wn";
		mGlobalInput.AgentCom = "";
		mGlobalInput.ManageCom = "86110000";
		mGlobalInput.ComCode = "86110000";

		VData tVData = new VData();
		tVData.add(transferData1);
		tVData.add(mGlobalInput);
		tPadGetFile.submitData(tVData, "","");
	}

}
