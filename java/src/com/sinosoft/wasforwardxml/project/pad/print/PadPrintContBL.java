package com.sinosoft.wasforwardxml.project.pad.print;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.PadXmlTable;
import com.sinosoft.lis.cbcheck.GetContTaxCodeBL;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.f1print.LCContF1PBL;
import com.sinosoft.lis.f1print.LCContF1PUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.lis.tb.LCContSignBL;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.workflow.tb.TbWorkFlowUI;

public class PadPrintContBL extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public LCContTable cLCContTable;

	public PadXmlTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息
	
	private LCContSchema mLCContSchema;
	
	private PubFun mPubFun = new PubFun();
	LCContF1PBL tLCContF1PBL = new LCContF1PBL();

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad出单保单打印");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();

			LCContSet tLCContSet = new LCContSet();
			LCContDB tLCContDB = new LCContDB();
			tLCContSet = tLCContDB
					.executeQuery("select * from lccont where prtno='"
							+ cLCContTable.getPrtNo()
							+ "'  and prtno like 'PD%' and appflag='1' ");
			if (tLCContSet.size()<1) {
				errLog("保单不存在或未承保！");
				return false;
			}
			mLCContSchema = tLCContSet.get(1);		
			String sql = "select SysVarValue from LDSysvar where SysVar='UIRoot'";
	        String tUIRoot = new ExeSQL().getOneValue(sql);
//	        tUIRoot = "E:\\lisdev\\ui\\";
	        String mPadFlagsql = "select PrintType from lccontsub where prtno='"+ cLCContTable.getPrtNo()+ "'";
	        String mPadFlag = new ExeSQL().getOneValue(mPadFlagsql);
	        if(!("1".equals(mPadFlag))){
	        	errLog("非电子保单不能通过此接口打印！");
				return false;
	        }
	        sql = "select ScanCheckFlag from lccontsub where prtno = '"+cLCContTable.getPrtNo()+"' with ur";
	        mPadFlag = new ExeSQL().getOneValue(sql);
	        if(!"1".equals(mPadFlag)){
	        	mError = "此保单尚未完成影像件复查，无法打印！";
	        	errLog(mError);
	        	return false;
	        }
			String sqlC = "select distinct 1 from lcpol lc,lmriskapp lm where lm.riskcode=lc.riskcode and lm.TaxOptimal='Y' and lc.contno='"+mLCContSchema.getContNo()+"' ";
	        String TaxFlag=new ExeSQL().getOneValue("select Taxcode from lccontsub where prtno='"+mLCContSchema.getPrtNo()+"'");
	        boolean isSYFlag = new ExeSQL().getOneValue(sqlC).equals("1");
	        if(isSYFlag && (TaxFlag == null || "".equals(TaxFlag))){
	        	TransferData nTransferData = new TransferData();
	        	nTransferData.setNameAndValue("ContNo", mLCContSchema.getContNo());
	        	nTransferData.setNameAndValue("PrtNo", mLCContSchema.getPrtNo());
				VData tVData1 = new VData();
				tVData1.add(nTransferData);
				tVData1.add(mGlobalInput);
				GetContTaxCodeBL tGetContTaxCodeBL = new GetContTaxCodeBL();//调用处理类
				if (!tGetContTaxCodeBL.submitData(tVData1, "GET")) {
					String Content = tGetContTaxCodeBL.mErrors.getError(0).errorMessage;
					errLog(Content);
		            return false;
				}
	        }
	        
	       ES_DOC_MAINSet tES_DOC_MAINSet=new ES_DOC_MAINSet();
	       ES_DOC_MAINDB tES_DOC_MAINDB=new ES_DOC_MAINDB();
	       tES_DOC_MAINSet=tES_DOC_MAINDB.executeQuery("select * from ES_DOC_MAIN where doccode='"+ cLCContTable.getPrtNo()+ "' and subtype='TB28' ");
	        if(tES_DOC_MAINSet.size()<1){
	        	errLog("扫描件尚未上载完成，请稍后点击发送！");
				return false;
	        }
			VData vData = new VData();
			vData.add(this.mGlobalInput);
			vData.addElement(mLCContSchema);
			vData.add(tUIRoot+"f1print/template/");
			vData.add(tUIRoot);
			vData.add("1");//保单是否打印标记：0，不打印；1，打印
//			vData.add(mPadFlag);//电子保单标志 不传这个标志了 改从数据库查
			if(!tLCContF1PBL.submitData(vData, "PRINT")){
				errLog(tLCContF1PBL.mErrors.getFirstError());
				return false;
			}
			tLCContF1PBL.getPadXmlFile();
			
			System.out.println(tLCContF1PBL.getPadXmlFile());
			
			// 自动回执回销
//			if (!addGetPolDate()) {
//				return false;
//			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,printcount,uwflag from lccont where prtno = '"
				+ cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputDataTable = new PadXmlTable();
			cOutputDataTable.setReturnData(tLCContF1PBL.getPadXmlFile());
		}
		return true;

	}

//	/**
//	 * addGetPolDate
//	 * 
//	 * @return boolean
//	 */
//	private boolean addGetPolDate() {
//
//		String mCurrentDate = mPubFun.getCurrentDate();
//		String sql = "select name from laagent where agentcode='"+mLCContSchema.getAgentCode()+"'";
//        String tAgentName = new ExeSQL().getOneValue(sql);
//        
//		LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
//		LCContGetPolUI tLCContGetPolUI = new LCContGetPolUI();
//		tLCContGetPolSchema.setContNo(mLCContSchema.getContNo());
//		tLCContGetPolSchema.setGetpolDate(mCurrentDate);
//		tLCContGetPolSchema.setGetpolMan(mLCContSchema.getAppntName());
//		tLCContGetPolSchema.setSendPolMan(tAgentName);
//		tLCContGetPolSchema.setGetPolOperator("PAD");
//		tLCContGetPolSchema.setManageCom(mLCContSchema.getManageCom());
//		tLCContGetPolSchema.setAgentCode(mLCContSchema.getAgentCode());
//		tLCContGetPolSchema.setContType("1");
//		VData vData = new VData();
//		vData.add(this.mGlobalInput);
//		vData.addElement(tLCContGetPolSchema);
//		if(!tLCContGetPolUI.submitData(vData, "INSERT")){
//			mError=tLCContGetPolUI.mErrors.getFirstError();
//		}
//		return true;
//	}

}
