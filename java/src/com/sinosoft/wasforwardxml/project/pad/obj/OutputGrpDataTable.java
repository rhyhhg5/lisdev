package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class OutputGrpDataTable extends BaseXmlSch{

	private static final long serialVersionUID = 1L;

	/** 保单号*/
    private String GrpContNo = null;
    
	 /** 印刷号*/
    private String PrtNo = null;

    /** 保额 */
    private String Amnt = null;

    /** 保费 */
    private String Prem = null;
    
    /** 保单状态 */
    private String StateFlag = null;
    
    /** 新单复核失败原因 */
    private String ErrorInfo = null;

	/**
	 * @return the grpContNo
	 */
	public String getGrpContNo() {
		return GrpContNo;
	}

	/**
	 * @param grpContNo the grpContNo to set
	 */
	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}

	/**
	 * @return the prtNo
	 */
	public String getPrtNo() {
		return PrtNo;
	}

	/**
	 * @param prtNo the prtNo to set
	 */
	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	/**
	 * @return the amnt
	 */
	public String getAmnt() {
		return Amnt;
	}

	/**
	 * @param amnt the amnt to set
	 */
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}

	/**
	 * @return the prem
	 */
	public String getPrem() {
		return Prem;
	}

	/**
	 * @param prem the prem to set
	 */
	public void setPrem(String prem) {
		Prem = prem;
	}

	/**
	 * @return the stateFlag
	 */
	public String getStateFlag() {
		return StateFlag;
	}

	/**
	 * @param stateFlag the stateFlag to set
	 */
	public void setStateFlag(String stateFlag) {
		StateFlag = stateFlag;
	}

	/**
	 * @return the errorInfo
	 */
	public String getErrorInfo() {
		return ErrorInfo;
	}

	/**
	 * @param errorInfo the errorInfo to set
	 */
	public void setErrorInfo(String errorInfo) {
		ErrorInfo = errorInfo;
	}
    
    
}
