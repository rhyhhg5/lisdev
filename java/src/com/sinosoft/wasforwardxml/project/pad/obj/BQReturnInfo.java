package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class BQReturnInfo extends BaseXmlSch{

	private static final long serialVersionUID = -577326075123324509L;
	
	/**
	 * ������
	 */
	private String EdorAcceptNo;
	
	/**
	 * ״̬
	 */
	private String State;

	public String getEdorAcceptNo() {
		return EdorAcceptNo;
	}

	public void setEdorAcceptNo(String edorAcceptNo) {
		EdorAcceptNo = edorAcceptNo;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}
	
}
