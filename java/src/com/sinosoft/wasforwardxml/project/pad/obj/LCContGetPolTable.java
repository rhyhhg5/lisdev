package com.sinosoft.wasforwardxml.project.pad.obj;
import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class LCContGetPolTable  extends BaseXmlSch{

    /** 保单号*/
    private String ContNo = null;

    /** 保单号*/
    private String Prtno = null;
	/** 管理机构 */
    private String ManageCom = null;
    /** 签收日期*/
    private String GetpolDate = null;

    /** 签收人员 */
    private String GetpolMan = null;
    /** 递交人员*/
    private String SendPolMan = null;

    /** 操作人员*/
    private String GetPolOperator = null;
    /** 个团标识*/
    private String ContType = null;
    /**业务员代码 */
    private String AgentCode = null;
    public String getPrtno() {
		return Prtno;
	}

	public void setPrtno(String prtno) {
		Prtno = prtno;
	}
    public String getContNo() {
		return ContNo;
	}

	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getGetpolDate() {
		return GetpolDate;
	}

	public void setGetpolDate(String getpolDate) {
		GetpolDate = getpolDate;
	}

	public String getGetpolMan() {
		return GetpolMan;
	}

	public void setGetpolMan(String getpolMan) {
		GetpolMan = getpolMan;
	}

	public String getSendPolMan() {
		return SendPolMan;
	}

	public void setSendPolMan(String sendPolMan) {
		SendPolMan = sendPolMan;
	}

	public String getGetPolOperator() {
		return GetPolOperator;
	}

	public void setGetPolOperator(String getPolOperator) {
		GetPolOperator = getPolOperator;
	}

	public String getContType() {
		return ContType;
	}

	public void setContType(String contType) {
		ContType = contType;
	}

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	
    
}
