package com.sinosoft.wasforwardxml.project.pad.obj;

public class BQFileInfoForm {

	private String agentcode;
	private String managecom;
	private String date;
	private String edoracceptno;
	private String[] page;
	public String getAgentcode() {
		return agentcode;
	}
	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getManagecom() {
		return managecom;
	}
	public void setManagecom(String managecom) {
		this.managecom = managecom;
	}
	public String[] getPage() {
		return page;
	}
	public void setPage(String[] page) {
		this.page = page;
	}
	/**
	 * @return the edoracceptno
	 */
	public String getEdoracceptno() {
		return edoracceptno;
	}
	/**
	 * @param edoracceptno the edoracceptno to set
	 */
	public void setEdoracceptno(String edoracceptno) {
		this.edoracceptno = edoracceptno;
	}
}
