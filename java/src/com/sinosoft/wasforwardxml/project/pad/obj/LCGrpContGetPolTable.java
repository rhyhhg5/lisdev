package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class LCGrpContGetPolTable extends BaseXmlSch{
	/** 保单号*/
    private String ContNo = null;
    /** 保单号*/
    private String Prtno = null;
	/** 管理机构 */
    private String ManageCom = null;
    /** 签收日期*/
    private String GetpolDate = null;
    /** 签收人员 */
    private String GetpolMan = null;
    /** 递交人员*/
    private String SendPolMan = null;
    /** 操作人员*/
    private String GetPolOperator = null;
    /** 个团标识*/
    private String ContType = null;
    /**业务员代码 */
    private String AgentCode = null;
    /** 打印日期 **/
    private String PrintDate = null;
    /** 打印时间 **/
    private String PrintTime = null;
    /** 打印次数 **/
    private String PrintCount = null;
	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}
	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	/**
	 * @return the prtno
	 */
	public String getPrtno() {
		return Prtno;
	}
	/**
	 * @param prtno the prtno to set
	 */
	public void setPrtno(String prtno) {
		Prtno = prtno;
	}
	/**
	 * @return the manageCom
	 */
	public String getManageCom() {
		return ManageCom;
	}
	/**
	 * @param manageCom the manageCom to set
	 */
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
	/**
	 * @return the getpolDate
	 */
	public String getGetpolDate() {
		return GetpolDate;
	}
	/**
	 * @param getpolDate the getpolDate to set
	 */
	public void setGetpolDate(String getpolDate) {
		GetpolDate = getpolDate;
	}
	/**
	 * @return the getpolMan
	 */
	public String getGetpolMan() {
		return GetpolMan;
	}
	/**
	 * @param getpolMan the getpolMan to set
	 */
	public void setGetpolMan(String getpolMan) {
		GetpolMan = getpolMan;
	}
	/**
	 * @return the sendPolMan
	 */
	public String getSendPolMan() {
		return SendPolMan;
	}
	/**
	 * @param sendPolMan the sendPolMan to set
	 */
	public void setSendPolMan(String sendPolMan) {
		SendPolMan = sendPolMan;
	}
	/**
	 * @return the getPolOperator
	 */
	public String getGetPolOperator() {
		return GetPolOperator;
	}
	/**
	 * @param getPolOperator the getPolOperator to set
	 */
	public void setGetPolOperator(String getPolOperator) {
		GetPolOperator = getPolOperator;
	}
	/**
	 * @return the contType
	 */
	public String getContType() {
		return ContType;
	}
	/**
	 * @param contType the contType to set
	 */
	public void setContType(String contType) {
		ContType = contType;
	}
	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return AgentCode;
	}
	/**
	 * @param agentCode the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}
	/**
	 * @return the printDate
	 */
	public String getPrintDate() {
		return PrintDate;
	}
	/**
	 * @param printDate the printDate to set
	 */
	public void setPrintDate(String printDate) {
		PrintDate = printDate;
	}
	/**
	 * @return the printTime
	 */
	public String getPrintTime() {
		return PrintTime;
	}
	/**
	 * @param printTime the printTime to set
	 */
	public void setPrintTime(String printTime) {
		PrintTime = printTime;
	}
	/**
	 * @return the printCount
	 */
	public String getPrintCount() {
		return PrintCount;
	}
	/**
	 * @param printCount the printCount to set
	 */
	public void setPrintCount(String printCount) {
		PrintCount = printCount;
	}
    
}
