package com.sinosoft.wasforwardxml.project.pad.obj;

public class FileInfoForm {

	private String agentcode;
	private String managecom;
	private String date;
	private String prtno;
	private String[] page;
	public String getAgentcode() {
		return agentcode;
	}
	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getManagecom() {
		return managecom;
	}
	public void setManagecom(String managecom) {
		this.managecom = managecom;
	}
	public String[] getPage() {
		return page;
	}
	public void setPage(String[] page) {
		this.page = page;
	}
	public String getPrtno() {
		return prtno;
	}
	public void setPrtno(String prtno) {
		this.prtno = prtno;
	}

}
