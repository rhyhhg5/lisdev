package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class LCGRPContTable extends BaseXmlSch{

	private static final long serialVersionUID = -4794467791337930966L;

    /** 印刷号 */
    private String PrtNo = null;

    /** 保单号*/
    private String GrpContNo = null;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String SaleChnl = null;

    /** 销售机构 */
    private String AgentCom = null;

    /** 销售人员 */
    private String AgentCode = null;

    /** 保险生效日期 */
    private String CValiDate = null;
    
    /** 保险失效日期 */
    private String CInValiDate = null;
    
    /** 缴费频次（趸缴、年缴、季缴、月缴） */
    private String PayIntv = null;
    
    /** 缴费方式 */
    private String PayMode = null;
    /** 投保单申请日期 */
    private String PolApplyDate = null;
    
    /** 银行编码 */
    private String BankCode = null;

    /** 银行账户名 */
    private String AccName = null;
    
    /** 银行账号 */
    private String BankAccNo = null;

    /** 保单标识 **/
    private String CardFlag = null;
	/**
	 * @return the prtNo
	 */
	public String getPrtNo() {
		return PrtNo;
	}

	/**
	 * @param prtNo the prtNo to set
	 */
	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	/**
	 * @return the grpContNo
	 */
	public String getGrpContNo() {
		return GrpContNo;
	}

	/**
	 * @param grpContNo the grpContNo to set
	 */
	public void setGrpContNo(String grpContNo) {
		GrpContNo = grpContNo;
	}

	/**
	 * @return the manageCom
	 */
	public String getManageCom() {
		return ManageCom;
	}

	/**
	 * @param manageCom the manageCom to set
	 */
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	/**
	 * @return the saleChnl
	 */
	public String getSaleChnl() {
		return SaleChnl;
	}

	/**
	 * @param saleChnl the saleChnl to set
	 */
	public void setSaleChnl(String saleChnl) {
		SaleChnl = saleChnl;
	}

	/**
	 * @return the agentCom
	 */
	public String getAgentCom() {
		return AgentCom;
	}

	/**
	 * @param agentCom the agentCom to set
	 */
	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return AgentCode;
	}

	/**
	 * @param agentCode the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	/**
	 * @return the cValiDate
	 */
	public String getCValiDate() {
		return CValiDate;
	}

	/**
	 * @param cValiDate the cValiDate to set
	 */
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}

	/**
	 * @return the cInValiDate
	 */
	public String getCInValiDate() {
		return CInValiDate;
	}

	/**
	 * @param cInValiDate the cInValiDate to set
	 */
	public void setCInValiDate(String cInValiDate) {
		CInValiDate = cInValiDate;
	}

	/**
	 * @return the payIntv
	 */
	public String getPayIntv() {
		return PayIntv;
	}

	/**
	 * @param payIntv the payIntv to set
	 */
	public void setPayIntv(String payIntv) {
		PayIntv = payIntv;
	}

	/**
	 * @return the payMode
	 */
	public String getPayMode() {
		return PayMode;
	}

	/**
	 * @param payMode the payMode to set
	 */
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	/**
	 * @return the polApplyDate
	 */
	public String getPolApplyDate() {
		return PolApplyDate;
	}

	/**
	 * @param polApplyDate the polApplyDate to set
	 */
	public void setPolApplyDate(String polApplyDate) {
		PolApplyDate = polApplyDate;
	}

	/**
	 * @return the bankCode
	 */
	public String getBankCode() {
		return BankCode;
	}

	/**
	 * @param bankCode the bankCode to set
	 */
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	/**
	 * @return the accName
	 */
	public String getAccName() {
		return AccName;
	}

	/**
	 * @param accName the accName to set
	 */
	public void setAccName(String accName) {
		AccName = accName;
	}

	/**
	 * @return the bankAccNo
	 */
	public String getBankAccNo() {
		return BankAccNo;
	}

	/**
	 * @param bankAccNo the bankAccNo to set
	 */
	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	/**
	 * @return the cardFlag
	 */
	public String getCardFlag() {
		return CardFlag;
	}

	/**
	 * @param cardFlag the cardFlag to set
	 */
	public void setCardFlag(String cardFlag) {
		CardFlag = cardFlag;
	}
    
}
