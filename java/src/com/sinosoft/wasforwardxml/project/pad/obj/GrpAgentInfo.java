package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;


public class GrpAgentInfo extends BaseXmlSch{

	private static final long serialVersionUID = 1L;

	/** 保单号*/
    private String UNI_SALES_COD = null;
    
	 /** 印刷号*/
    private String SALES_NAM = null;

    /** 保额 */
    private String MAN_ORG_COD = null;

    /** 保费 */
    private String MAN_ORG_NAM = null;
    
    /** 保单状态 */
    private String ID_NO = null;
    

	public String getMAN_ORG_COD() {
		return MAN_ORG_COD;
	}

	public void setMAN_ORG_COD(String mAN_ORG_COD) {
		MAN_ORG_COD = mAN_ORG_COD;
	}

	public String getMAN_ORG_NAM() {
		return MAN_ORG_NAM;
	}

	public void setMAN_ORG_NAM(String mAN_ORG_NAM) {
		MAN_ORG_NAM = mAN_ORG_NAM;
	}

	public String getSALES_NAM() {
		return SALES_NAM;
	}

	public void setSALES_NAM(String sALES_NAM) {
		SALES_NAM = sALES_NAM;
	}

	public String getUNI_SALES_COD() {
		return UNI_SALES_COD;
	}

	public void setUNI_SALES_COD(String uNI_SALES_COD) {
		UNI_SALES_COD = uNI_SALES_COD;
	}

	public void setID_NO(String iD_NO) {
		ID_NO = iD_NO;
	}

	public String getID_NO() {
		return ID_NO;
	}

}
