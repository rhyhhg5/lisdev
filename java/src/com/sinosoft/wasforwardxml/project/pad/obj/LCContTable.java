package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class LCContTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 印刷号 */
    private String PrtNo = null;

    /** 保单号*/
    private String ContNo = null;

    /** 管理机构 */
    private String ManageCom = null;

    /** 销售渠道 */
    private String SaleChnl = null;

    /** 销售机构 */
    private String AgentCom = null;

    /** 销售人员 */
    private String AgentCode = null;

    /** 保险生效日期 */
    private String CValiDate = null;
    
    /** 保险失效日期 */
    private String CInValiDate = null;
    
    /** 缴费频次（趸缴、年缴、季缴、月缴） */
    private String PayIntv = null;
    
    /** 缴费方式 */
    private String PayMode = null;
    /** 投保单申请日期 */
    private String PolApplyDate = null;
    
    /** 银行编码 */
    private String BankCode = null;

    /** 银行账户名 */
    private String AccName = null;
    
    /** 银行账号 */
    private String BankAccNo = null;

    /** 代理销售业务员编码 */
    private String AgentSaleCode = null;
    
    /** 交叉销售渠道 */
    private String Crs_SaleChnl = null;
    /** 销售业务类型 */
    private String Crs_BussType = null;
    /** 代理机构 */
    private String GrpAgentCom = null;
    /** 代理人编码 */
    private String GrpAgentCode = null;
    /** 代理人姓名 */
    private String GrpAgentName = null;
    /** 代理人身份证  */
    private String GrpAgentIDNo = null;
    /** 约定缴费标识 */
    private String AgreedPayFlag = null;
    /** 约定缴费日期 */
    private String AgreedPayDate = null;
    
	public String getAgentSaleCode() {
		return AgentSaleCode;
	}

	public void setAgentSaleCode(String agentSaleCode) {
		AgentSaleCode = agentSaleCode;
	}

	/**
	 * @return the polApplyDate
	 */
	public String getPolApplyDate() {
		return PolApplyDate;
	}

	/**
	 * @param polApplyDate the polApplyDate to set
	 */
	public void setPolApplyDate(String polApplyDate) {
		PolApplyDate = polApplyDate;
	}

	/**
	 * @return the payMode
	 */
	public String getPayMode() {
		return PayMode;
	}

	/**
	 * @param payMode the payMode to set
	 */
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return AgentCode;
	}

	/**
	 * @param agentCode the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	/**
	 * @return the agentCom
	 */
	public String getAgentCom() {
		return AgentCom;
	}

	/**
	 * @param agentCom the agentCom to set
	 */
	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the cValiDate
	 */
	public String getCValiDate() {
		return CValiDate;
	}

	/**
	 * @param valiDate the cValiDate to set
	 */
	public void setCValiDate(String valiDate) {
		CValiDate = valiDate;
	}

	/**
	 * @return the mangeCom
	 */
	public String getManageCom() {
		return ManageCom;
	}

	/**
	 * @param mangeCom the mangeCom to set
	 */
	public void setManageCom(String mangeCom) {
		ManageCom = mangeCom;
	}

	/**
	 * @return the payIntv
	 */
	public String getPayIntv() {
		return PayIntv;
	}

	/**
	 * @param payIntv the payIntv to set
	 */
	public void setPayIntv(String payIntv) {
		PayIntv = payIntv;
	}

	/**
	 * @return the prtNo
	 */
	public String getPrtNo() {
		return PrtNo;
	}

	/**
	 * @param prtNo the prtNo to set
	 */
	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	/**
	 * @return the saleChnl
	 */
	public String getSaleChnl() {
		return SaleChnl;
	}

	/**
	 * @param saleChnl the saleChnl to set
	 */
	public void setSaleChnl(String saleChnl) {
		SaleChnl = saleChnl;
	}

	/**
	 * @return the cInValiDate
	 */
	public String getCInValiDate() {
		return CInValiDate;
	}

	/**
	 * @param inValiDate the cInValiDate to set
	 */
	public void setCInValiDate(String inValiDate) {
		CInValiDate = inValiDate;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getAccName() {
		return AccName;
	}

	public void setAccName(String accName) {
		AccName = accName;
	}

	public String getBankAccNo() {
		return BankAccNo;
	}

	public void setBankAccNo(String bankAccNo) {
		BankAccNo = bankAccNo;
	}

	/**
	 * @return the crs_SaleChnl
	 */
	public String getCrs_SaleChnl() {
		return Crs_SaleChnl;
	}

	/**
	 * @param crs_SaleChnl the crs_SaleChnl to set
	 */
	public void setCrs_SaleChnl(String crs_SaleChnl) {
		Crs_SaleChnl = crs_SaleChnl;
	}

	/**
	 * @return the crs_BussType
	 */
	public String getCrs_BussType() {
		return Crs_BussType;
	}

	/**
	 * @param crs_BussType the crs_BussType to set
	 */
	public void setCrs_BussType(String crs_BussType) {
		Crs_BussType = crs_BussType;
	}

	/**
	 * @return the grpAgentCom
	 */
	public String getGrpAgentCom() {
		return GrpAgentCom;
	}

	/**
	 * @param grpAgentCom the grpAgentCom to set
	 */
	public void setGrpAgentCom(String grpAgentCom) {
		GrpAgentCom = grpAgentCom;
	}

	/**
	 * @return the grpAgentCode
	 */
	public String getGrpAgentCode() {
		return GrpAgentCode;
	}

	/**
	 * @param grpAgentCode the grpAgentCode to set
	 */
	public void setGrpAgentCode(String grpAgentCode) {
		GrpAgentCode = grpAgentCode;
	}

	/**
	 * @return the grpAgentName
	 */
	public String getGrpAgentName() {
		return GrpAgentName;
	}

	/**
	 * @param grpAgentName the grpAgentName to set
	 */
	public void setGrpAgentName(String grpAgentName) {
		GrpAgentName = grpAgentName;
	}

	/**
	 * @return the grpAgentIDNo
	 */
	public String getGrpAgentIDNo() {
		return GrpAgentIDNo;
	}

	/**
	 * @param grpAgentIDNo the grpAgentIDNo to set
	 */
	public void setGrpAgentIDNo(String grpAgentIDNo) {
		GrpAgentIDNo = grpAgentIDNo;
	}

	/**
	 * @return the agreedPayFlag
	 */
	public String getAgreedPayFlag() {
		return AgreedPayFlag;
	}

	/**
	 * @param agreedPayFlag the agreedPayFlag to set
	 */
	public void setAgreedPayFlag(String agreedPayFlag) {
		AgreedPayFlag = agreedPayFlag;
	}

	/**
	 * @return the agreedPayDate
	 */
	public String getAgreedPayDate() {
		return AgreedPayDate;
	}

	/**
	 * @param agreedPayDate the agreedPayDate to set
	 */
	public void setAgreedPayDate(String agreedPayDate) {
		AgreedPayDate = agreedPayDate;
	}


}
