package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class ReturnDataTable extends BaseXmlSch {
	private static final long serialVersionUID = -4794467791337930966L;

	/** 印刷号 */
	private String PrtNo = null;

	/** 成功标志 */
	private String SuccFlag = null;

	/** 描述信息 */
	private String desc = null;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPrtNo() {
		return PrtNo;
	}

	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}

	public String getSuccFlag() {
		return SuccFlag;
	}

	public void setSuccFlag(String succFlag) {
		SuccFlag = succFlag;
	}

}
