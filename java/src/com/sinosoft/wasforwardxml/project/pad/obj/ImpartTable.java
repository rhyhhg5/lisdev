package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class ImpartTable extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;

    /** 印刷号 */
    private String ContID = null;

    /** 保单号*/
    private String CustomerNoType = null;

    /** 管理机构 */
    private String CustomerNo = null;

    /** 销售渠道 */
    private String ImpartVer = null;

    /** 销售机构 */
    private String ImpartCode = null;

	/** 销售人员 */
    private String ImpartParamModle = null;

    /** 保险生效日期 */
    private String DiseaseContent = null;
    
    public String getContID() {
		return ContID;
	}

	public void setContID(String contID) {
		ContID = contID;
	}

	public String getCustomerNoType() {
		return CustomerNoType;
	}

	public void setCustomerNoType(String customerNoType) {
		CustomerNoType = customerNoType;
	}

	public String getCustomerNo() {
		return CustomerNo;
	}

	public void setCustomerNo(String customerNo) {
		CustomerNo = customerNo;
	}

	public String getImpartVer() {
		return ImpartVer;
	}

	public void setImpartVer(String impartVer) {
		ImpartVer = impartVer;
	}

	public String getImpartCode() {
		return ImpartCode;
	}

	public void setImpartCode(String impartCode) {
		ImpartCode = impartCode;
	}

	public String getImpartParamModle() {
		return ImpartParamModle;
	}

	public void setImpartParamModle(String impartParamModle) {
		ImpartParamModle = impartParamModle;
	}

	public String getDiseaseContent() {
		return DiseaseContent;
	}

	public void setDiseaseContent(String diseaseContent) {
		DiseaseContent = diseaseContent;
	}


}
