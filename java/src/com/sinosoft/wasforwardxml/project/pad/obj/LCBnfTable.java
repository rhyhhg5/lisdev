package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class LCBnfTable extends BaseXmlSch
{

	private static final long serialVersionUID = -4794467791337930966L;
    /** 保单号*/
    private String ContNo = null;

    /** 被保人序号 */
    private String InsuredNo = null;

    /** 受益人姓名 */
    private String Name = null;

    /** 性别 */
    private String Sex = null;

    /** 出生日期 */
    private String Birthday = null;

    /** 证件类型 */
    private String IDType = null;
    
    /** 证件号码 */
    private String IDNo = null;
    
    /** 与被保人关系 */
    private String RelationToInsured = null;
    
    /** 受益比例 */
    private String BnfLot = null;
    
    /** 受益顺序 */
    private String BnfGrade = null;

    /** 受益类型 */
    private String BnfType = null;

	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return Birthday;
	}

	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		Birthday = birthday;
	}

	/**
	 * @return the bnfGrade
	 */
	public String getBnfGrade() {
		return BnfGrade;
	}

	/**
	 * @param bnfGrade the bnfGrade to set
	 */
	public void setBnfGrade(String bnfGrade) {
		BnfGrade = bnfGrade;
	}

	/**
	 * @return the bnfLot
	 */
	public String getBnfLot() {
		return BnfLot;
	}

	/**
	 * @param bnfLot the bnfLot to set
	 */
	public void setBnfLot(String bnfLot) {
		BnfLot = bnfLot;
	}

	/**
	 * @return the bnfType
	 */
	public String getBnfType() {
		return BnfType;
	}

	/**
	 * @param bnfType the bnfType to set
	 */
	public void setBnfType(String bnfType) {
		BnfType = bnfType;
	}

	/**
	 * @return the contNo
	 */
	public String getContNo() {
		return ContNo;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setContNo(String contNo) {
		ContNo = contNo;
	}

	/**
	 * @return the iDNo
	 */
	public String getIDNo() {
		return IDNo;
	}

	/**
	 * @param no the iDNo to set
	 */
	public void setIDNo(String no) {
		IDNo = no;
	}

	/**
	 * @return the iDType
	 */
	public String getIDType() {
		return IDType;
	}

	/**
	 * @param type the iDType to set
	 */
	public void setIDType(String type) {
		IDType = type;
	}

	/**
	 * @return the insuredNo
	 */
	public String getInsuredNo() {
		return InsuredNo;
	}

	/**
	 * @param insuredNo the insuredNo to set
	 */
	public void setInsuredNo(String insuredNo) {
		InsuredNo = insuredNo;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return Name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		Name = name;
	}

	/**
	 * @return the relationToInsured
	 */
	public String getRelationToInsured() {
		return RelationToInsured;
	}

	/**
	 * @param relationToInsured the relationToInsured to set
	 */
	public void setRelationToInsured(String relationToInsured) {
		RelationToInsured = relationToInsured;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return Sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		Sex = sex;
	}
    
    
}
