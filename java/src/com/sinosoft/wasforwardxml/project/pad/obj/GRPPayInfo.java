package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class GRPPayInfo extends BaseXmlSch {
	private static final long serialVersionUID = -4794467791337930966L;

	/** 缴费方式 */
	private String PayMode = null;

	/** 缴费总金额 */
	private String PayMoney = null;
	
	/** 收费成功标志 */
	private String ChargeSuccFlag = null;

	/** 到账日期 */
	private String PayEntAccDate = null;
	
	/** 到账时间*/
	private String PayEntAccTime = null;
	
//	/** 收费日期 */
//	private String ChargeDate = null;
//
//	/** 收费时间 */
//	private String ChargeTime = null;
	
	/**归集账号管理机构**/
	private String ManageCom = null;
	
	/**归集账户银行卡号*/
	private String Insbankaccno = null;
	
	/**归集账户银行编码*/
	private String Insbankcode = null;

	public String getPayMode() {
		return PayMode;
	}

	public void setPayMode(String payMode) {
		PayMode = payMode;
	}
	
	public String getChargeSuccFlag() {
		return ChargeSuccFlag;
	}

	public void setChargeSuccFlag(String chargeSuccFlag) {
		ChargeSuccFlag = chargeSuccFlag;
	}

	public String getPayMoney() {
		return PayMoney;
	}

	public void setPayMoney(String payMoney) {
		PayMoney = payMoney;
	}

	public String getPayEntAccDate() {
		return PayEntAccDate;
	}

	public void setPayEntAccDate(String payEntAccDate) {
		PayEntAccDate = payEntAccDate;
	}
	
//	public String getChargeDate() {
//		return ChargeDate;
//	}
//
//	public void setChargeDate(String chargeDate) {
//		ChargeDate = chargeDate;
//	}
//
//	public String getChargeTime() {
//		return ChargeTime;
//	}
//
//	public void setChargeTime(String chargeTime) {
//		ChargeTime = chargeTime;
//	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String managecom) {
		ManageCom = managecom;
	}
	public String getInsbankaccno() {
		return Insbankaccno;
	}

	public void setInsbankaccno(String insbankaccno) {
		Insbankaccno = insbankaccno;
	}
	public String getInsbankcode() {
		return Insbankcode;
	}

	public void setInsbankcode(String insbankcode) {
		Insbankcode = insbankcode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
