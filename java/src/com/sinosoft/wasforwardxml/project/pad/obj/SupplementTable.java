package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class SupplementTable extends BaseXmlSch {
	private static final long serialVersionUID = -4794467791337930966L;

	/** 投保人国籍 */
	private String AppntNativePlace = null;

	/** 投保人职位 */
	private String AppntPosition = null;

	/** 投保人工资 */
	private String AppntSalary = null;

	/** 投保人婚姻状况 */
	private String AppntMarriage = null;

	/** 投保人国家 */
	private String AppntNativeCity = null;

	/** 被保人个国籍 */
	private String InsureNativePlace = null;

	/** 被保人职位 */
	private String InsurePosition = null;

	/** 被保人工资 */
	private String InsureSalary = null;

	/** 被保人婚姻状况 */
	private String InsureMarriage = null;

	/** 被保人国家 */
	private String InsureNativeCity = null;

	/** 红利金领取方式 */
	private String Bonusgetmode = null;

	/** 单位名称 */
	private String AppntGrpName = null;

	/** 投保人证件失效日期 */
	private String AppntIDEndDate = null;

	/** 被保人证件失效日期 */
	private String InsureIDEndDate = null;

	/** 总保费 */
	private String PremScope = null;

	/** 续期缴费提醒 */
	private String DueFeeMsgFlag = null;

	/** 投保人身份证有效期起 */
	private String AppntIDStartDate = null;

	/** 被保人工作单位 */
	private String InsureIDStartDate = null;

	/** 被保人身份证有效期起 */
	private String InsureGrpName = null;

	/** 保单打印类型 */
	private String PrintType = null;
	
	public String getPrintType() {
		return PrintType;
	}

	public void setPrintType(String printType) {
		PrintType = printType;
	}

	public String getAppntGrpName() {
		return AppntGrpName;
	}

	public void setAppntGrpName(String appntGrpName) {
		AppntGrpName = appntGrpName;
	}

	public String getPremScope() {
		return PremScope;
	}

	public void setPremScope(String premScope) {
		PremScope = premScope;
	}

	public String getDueFeeMsgFlag() {
		return DueFeeMsgFlag;
	}

	public void setDueFeeMsgFlag(String dueFeeMsgFlag) {
		DueFeeMsgFlag = dueFeeMsgFlag;
	}

	public String getAppntIDStartDate() {
		return AppntIDStartDate;
	}

	public void setAppntIDStartDate(String appntIDStartDate) {
		AppntIDStartDate = appntIDStartDate;
	}

	public String getInsureIDStartDate() {
		return InsureIDStartDate;
	}

	public void setInsureIDStartDate(String insureIDStartDate) {
		InsureIDStartDate = insureIDStartDate;
	}

	public String getInsureGrpName() {
		return InsureGrpName;
	}

	public void setInsureGrpName(String insureGrpName) {
		InsureGrpName = insureGrpName;
	}

	public String getAppntIDEndDate() {
		return AppntIDEndDate;
	}

	public void setAppntIDEndDate(String appntIDEndDate) {
		AppntIDEndDate = appntIDEndDate;
	}

	public String getInsureIDEndDate() {
		return InsureIDEndDate;
	}

	public void setInsureIDEndDate(String insureIDEndDate) {
		InsureIDEndDate = insureIDEndDate;
	}

	public String getBonusgetmode() {
		return Bonusgetmode;
	}

	public void setBonusgetmode(String bonusgetmode) {
		Bonusgetmode = bonusgetmode;
	}

	/**
	 * @return the InsureNativeCity
	 */
	public String getInsureNativeCity() {
		return InsureNativeCity;
	}

	/**
	 * @param InsureNativeCity
	 *            the InsureNativeCity to set
	 */
	public void setInsureNativeCity(String insureNativeCity) {
		InsureNativeCity = insureNativeCity;
	}

	/**
	 * @return the InsureNativePlace
	 */
	public String getInsureNativePlace() {
		return InsureNativePlace;
	}

	/**
	 * @param InsureNativePlace
	 *            the InsureNativePlace to set
	 */
	public void setInsureNativePlace(String insureNativePlace) {
		InsureNativePlace = insureNativePlace;
	}

	/**
	 * @return the AppntNativeCity
	 */
	public String getAppntNativeCity() {
		return AppntNativeCity;
	}

	/**
	 * @param AppntNativeCity
	 *            the AppntNativeCity to set
	 */
	public void setAppntNativeCity(String appntNativeCity) {
		AppntNativeCity = appntNativeCity;
	}

	/**
	 * @return the AppntPosition
	 */
	public String getAppntPosition() {
		return AppntPosition;
	}

	/**
	 * @param AppntPosition
	 *            the AppntPosition to set
	 */
	public void setAppntPosition(String appntPosition) {
		AppntPosition = appntPosition;
	}

	/**
	 * @return the InsurePosition
	 */
	public String getInsurePosition() {
		return InsurePosition;
	}

	/**
	 * @param valiDate
	 *            the InsurePosition to set
	 */
	public void setInsurePosition(String insurePosition) {
		InsurePosition = insurePosition;
	}

	/**
	 * @return the mangeCom
	 */
	public String getAppntSalary() {
		return AppntSalary;
	}

	/**
	 * @param mangeCom
	 *            the mangeCom to set
	 */
	public void setAppntSalary(String appntSalary) {
		AppntSalary = appntSalary;
	}

	/**
	 * @return the InsureMarriage
	 */
	public String getInsureMarriage() {
		return InsureMarriage;
	}

	/**
	 * @param InsureMarriage
	 *            the InsureMarriage to set
	 */
	public void setInsureMarriage(String insureMarriage) {
		InsureMarriage = insureMarriage;
	}

	/**
	 * @return the AppntNativePlace
	 */
	public String getAppntNativePlace() {
		return AppntNativePlace;
	}

	/**
	 * @param AppntNativePlace
	 *            the AppntNativePlace to set
	 */
	public void setAppntNativePlace(String appntNativePlace) {
		AppntNativePlace = appntNativePlace;
	}

	/**
	 * @return the AppntMarriage
	 */
	public String getAppntMarriage() {
		return AppntMarriage;
	}

	/**
	 * @param AppntMarriage
	 *            the AppntMarriage to set
	 */
	public void setAppntMarriage(String appntMarriage) {
		AppntMarriage = appntMarriage;
	}

	/**
	 * @return the InsureSalary
	 */
	public String getInsureSalary() {
		return InsureSalary;
	}

	/**
	 * @param InsureSalary
	 *            the InsureSalary to set
	 */
	public void setInsureSalary(String insureSalary) {
		InsureSalary = insureSalary;
	}

}
