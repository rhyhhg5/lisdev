package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class GrpAgentQueryInfo extends BaseXmlSch
{
    private static final long serialVersionUID = -4794467791337930966L;


    /** 保单号*/
    private String GrpAgentCode = null;

    /** 管理机构 */
    private String ManageCom = null;
 

	 

	/**
	 * @return the contNo
	 */
	public String getGrpAgentCode() {
		return GrpAgentCode;
	}

	/**
	 * @param contNo the contNo to set
	 */
	public void setGrpAgentCode(String grpAgentCode) {
		GrpAgentCode = grpAgentCode;
	}

	 

	/**
	 * @return the mangeCom
	 */
	public String getManageCom() {
		return ManageCom;
	}

	/**
	 * @param mangeCom the mangeCom to set
	 */
	public void setManageCom(String mangeCom) {
		ManageCom = mangeCom;
	}

}
