package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class PadXmlTable extends BaseXmlSch{
	 private String ReturnData = null;

	public String getReturnData() {
		return ReturnData;
	}

	public void setReturnData(String returnData) {
		ReturnData = returnData;
	}

}
