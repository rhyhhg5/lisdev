package com.sinosoft.wasforwardxml.project.pad.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class TempFee extends BaseXmlSch {
	private static final long serialVersionUID = -4794467791337930966L;

	/** 收费金额 */
	private String ChargeAmount = null;

	/** 收费成功标志 */
	private String ChargeSuccFlag = null;

	/** 收费方式 */
	private String ChargeMethod = null;

	/** 收费日期 */
	private String ChargeDate = null;

	/** 收费时间 */
	private String ChargeTime = null;
	
	/**归集账号管理机构**/
	private String Managecom = null;
	
	/**归集账号*/
	private String Insbankaccno = null;
	
	/**归集银行*/
	private String Insbankcode = null;

	public String getChargeAmount() {
		return ChargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		ChargeAmount = chargeAmount;
	}

	public String getChargeSuccFlag() {
		return ChargeSuccFlag;
	}

	public void setChargeSuccFlag(String chargeSuccFlag) {
		ChargeSuccFlag = chargeSuccFlag;
	}

	public String getChargeMethod() {
		return ChargeMethod;
	}

	public void setChargeMethod(String chargeMethod) {
		ChargeMethod = chargeMethod;
	}

	public String getChargeDate() {
		return ChargeDate;
	}

	public void setChargeDate(String chargeDate) {
		ChargeDate = chargeDate;
	}
	
	public String getChargeTime() {
		return ChargeTime;
	}

	public void setChargeTime(String chargeTime) {
		ChargeTime = chargeTime;
	}

	public String getManagecom() {
		return Managecom;
	}

	public void setManagecom(String managecom) {
		Managecom = managecom;
	}
	public String getInsbankaccno() {
		return Insbankaccno;
	}

	public void setInsbankaccno(String insbankaccno) {
		Insbankaccno = insbankaccno;
	}
	public String getInsbankcode() {
		return Insbankcode;
	}

	public void setInsbankcode(String insbankcode) {
		Insbankcode = insbankcode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
