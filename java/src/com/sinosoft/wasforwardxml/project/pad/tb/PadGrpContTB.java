package com.sinosoft.wasforwardxml.project.pad.tb;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.OMNamespaceImpl;

import com.cbsws.obj.RspSaleInfo;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpContRoadDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.lis.db.LMRiskInterestDB;
import com.sinosoft.lis.db.LMRiskToAccDB;
import com.sinosoft.lis.finfee.TempFeeUI;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContRoadSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpContSubSchema;
import com.sinosoft.lis.schema.LCGrpFeeSchema;
import com.sinosoft.lis.schema.LCGrpInterestSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LCPersonTraceSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LMRiskInterestSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.sys.MixedSalesAgentQueryUI;
import com.sinosoft.lis.tb.GroupContDeleteUI;
import com.sinosoft.lis.tb.GrpPubAccBL;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContRoadSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpFeeSet;
import com.sinosoft.lis.vschema.LCGrpInterestSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LCPersonTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDPromiseRateSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.vschema.LMRiskInterestSet;
import com.sinosoft.lis.vschema.LMRiskToAccSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PadGrpContTB {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mGlobalInput = new GlobalInput();
	
	private OMElement mOME = null;
	
	private String batchno = null;// 批次号

	private String sendDate = null;// 报文发送日期

	private String sendTime = null;// 报文发送时间
	
	private String branchcode = "";// 网点
	
	private String sendoperator = "";// 操作员

	private String messageType = "";// 报文类型

	private OMElement responseOME = null;
	
	private String mPrtNo = null;
	private String mProposalGrpContNo = null;
	
	private MMap tMMap = new MMap();
	
	private String theCurrentDate = PubFun.getCurrentDate();
	private String theCurrentTime = PubFun.getCurrentTime();
	
	private LoginVerifyTool mTool = new LoginVerifyTool();
	//保单信息
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	//投保单位信息
	private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();
	private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();
	private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
	
	//保障计划
	LCContPlanSet mLCContPlanSet = new LCContPlanSet();
	LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
	LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
	
	//默认保障计划
	LCContPlanDutyParamSet mLCContPlanDutyParamSetDefault = new LCContPlanDutyParamSet();
	
	//团体账户信息
	LCContPlanDutyParamSet mLCContPlanDutyParamSet2 = new LCContPlanDutyParamSet();
	LCContPlanRiskSchema mLCContPlanRiskSchema = new LCContPlanRiskSchema();
	LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();
	LMRiskFeeSet mLMRiskFeeSet = new LMRiskFeeSet();
	LCGrpInterestSet mLCGrpInterestSet = new LCGrpInterestSet();
	LCInsuredListSet mLCInsuredListSet2 = new LCInsuredListSet();
	LCGrpFeeSet mLCGrpFeeSet = new LCGrpFeeSet();
	String ClaimNum ="";
	LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
	LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
	LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
	String payMoney = "";
	double PayMoney = 0;
	boolean ldGrpFlag = false;
	String message = "";
	String result;
	boolean customerflag = false;
	String mixComFlag = "";
	
	//被保人
	LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
	
	//共享标识
	LCPersonTraceSchema mLCPersonTraceSchema = new LCPersonTraceSchema();
	LCPersonTraceSet mLCPersonTraceSet = new LCPersonTraceSet();
	
	//jxhw(162601、162801)保障计划
	LCContPlanDutyParamSet mLCContPlanDutyParamSetjxhw = new LCContPlanDutyParamSet();
	//一带一路要素
	LCGrpContRoadSchema mLCGrpContRoadSchema= new LCGrpContRoadSchema();
	//建工险要素
	LCContPlanDutyParamSet mLCContPlanDutyParamSetjgx = new LCContPlanDutyParamSet();
	LCGrpContSubSchema mLCGrpContSubSchemajgx = new LCGrpContSubSchema();
	boolean jgxflag = false;
	//主附险
	private MMap riskmap = new MMap();
	
	Reflections tReflections = new Reflections();
	
	public OMElement queryData(OMElement Oms) {
		this.mOME = Oms.cloneOMElement();
		if(!load()){
			responseOME = buildResponseOME("01","01", "加载团单数据失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","01", "WX", "加载团单数据失败!");
			System.out.println("responseOME:" + responseOME);
			return responseOME;
		}
		if(!check()){
			return responseOME;
		}
		if(!deal()){
			return responseOME;
		}
		if(!save(tMMap)){
			return responseOME;
		}
		if(!calPrem()){
			delete();
			return responseOME;
		}
		if(!setShared()){
			delete();
			return responseOME;
		}
		if(!creatMission()){
			delete();
			return responseOME;
		}
		
		responseOME = buildResponseOME("00","01", "");
		System.out.println("Create OK");
		return responseOME;
	}
	
	//加载卡激活信息
	private boolean load(){
		try{
			Iterator iter1 = this.mOME.getChildren();
			while(iter1.hasNext()){
				OMElement Om = (OMElement) iter1.next();
				if(Om.getLocalName().equals("GRPPADTB02")){
					Iterator child = Om.getChildren();
					while(child.hasNext()){
						OMElement child_element = (OMElement) child.next();
						if(child_element.getLocalName().equals("BATCHNO")){
							this.batchno=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDDATE")){
							this.sendDate=child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDTIME")){
							this.sendTime= child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("SENDOPERATOR")){
							this.mLCGrpContSchema.setInputOperator(child_element.getText());
							mGlobalInput.Operator = child_element.getText();
							this.sendoperator = child_element.getText();
							continue;
						}
						if(child_element.getLocalName().equals("MESSAGETYPE")){
							this.messageType=child_element.getText();
							this.messageType="cd";
							continue;
						}
						if(child_element.getLocalName().equals("GRPCONTLIST")){
							Iterator childItem = child_element.getChildren();
							while(childItem.hasNext()){
								OMElement childOME = (OMElement) childItem.next();
								if(childOME.getLocalName().equals("ITEM")){
									Iterator child1OME = childOME.getChildren();
									while(child1OME.hasNext()){
										OMElement childleaf = (OMElement) child1OME.next();
										if(childleaf.getLocalName().equals("PrtNo")){
											this.mLCGrpContSchema.setPrtNo(childleaf.getText());
											this.mLCGrpAppntSchema.setPrtNo(childleaf.getText());
											this.mLCGrpContRoadSchema.setPrtNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ManageCom")){
											this.mLCGrpContSchema.setManageCom(childleaf.getText());
											mGlobalInput.ManageCom = childleaf.getText();
											this.mLCGrpContRoadSchema.setManageCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CardFlag")){
											this.mLCGrpContSchema.setCardFlag(childleaf.getText());
										}
										if(childleaf.getLocalName().equals("SaleChnl")){
											this.mLCGrpContSchema.setSaleChnl(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("MixComFlag")){
											this.mixComFlag = childleaf.getText();
											continue;
										}
										if(childleaf.getLocalName().equals("CrsSaleChnl")){
											this.mLCGrpContSchema.setCrs_SaleChnl(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CrsBussType")){
											this.mLCGrpContSchema.setCrs_BussType(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("GrpAgentCode")){
											this.mLCGrpContSchema.setGrpAgentCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("GrpAgentCom")){
											this.mLCGrpContSchema.setGrpAgentCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("GrpAgentName")){
											this.mLCGrpContSchema.setGrpAgentName(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("GrpAgentIDNo")){
											this.mLCGrpContSchema.setGrpAgentIDNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("HandlerDate")){
											this.mLCGrpContSchema.setHandlerDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentCom")){
											this.mLCGrpContSchema.setAgentCom(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentCode")){
											if(!AgentCode(childleaf.getText(), "N")){
												System.out.println(getMessage());
											}
											this.mLCGrpContSchema.setAgentCode(getResult());
											continue;
										}
										if(childleaf.getLocalName().equals("AgentSaleCode")){
											this.mLCGrpContSchema.setAgentSaleCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("OutPayFlag")){
											this.mLCGrpContSchema.setOutPayFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("FirstTrialOperator")){
											this.mLCGrpContSchema.setFirstTrialOperator(childleaf.getText());
											
											continue;
										}
										if(childleaf.getLocalName().equals("MarketType")){
											this.mLCGrpContSchema.setMarketType(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("ReceiveDate")){
											this.mLCGrpContSchema.setReceiveDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CoInsuranceFlag")){
											this.mLCGrpContSchema.setCoInsuranceFlag(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PayMode")){
											this.mLCGrpContSchema.setPayMode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BankCode")){
											this.mLCGrpContSchema.setBankCode(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("BankAccNo")){
											this.mLCGrpContSchema.setBankAccNo(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("AccName")){
											this.mLCGrpContSchema.setAccName(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("PayIntv")){
											this.mLCGrpContSchema.setPayIntv(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CValiDate")){
											this.mLCGrpContSchema.setCValiDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("CInValiDate")){
											this.mLCGrpContSchema.setCInValiDate(childleaf.getText());
											continue;
										}
										if(childleaf.getLocalName().equals("Remark")){
											this.mLCGrpContSchema.setRemark(childleaf.getText());
											continue;
										}
									}
								}
							}
						}
						
						if(child_element.getLocalName().equals("GRPAPPNTLIST")){
							Iterator child_appnt = child_element.getChildren();
							while(child_appnt.hasNext()){
								OMElement appnt_item = (OMElement) child_appnt.next();
								if(appnt_item.getLocalName().equals("ITEM")){
									Iterator appnt_element= appnt_item.getChildren();
									while(appnt_element.hasNext()){
										OMElement appntleaf = (OMElement)appnt_element.next();
										
										if(appntleaf.getLocalName().equals("GrpNo")){
//											this.mLCGrpContSchema.setAppntNo(appntleaf.getText());
//											this.mLCGrpAppntSchema.setCustomerNo(appntleaf.getText());
//											this.mLDGrpSchema.setCustomerNo(appntleaf.getText());
//											this.mLCGrpAddressSchema.setCustomerNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpName")){
//											String tLimit = "SN";
//									        String GrpNo = PubFun1.CreateMaxNo("GRPNO", tLimit);
//											this.mLCGrpContSchema.setAppntNo(GrpNo);
//											this.mLCGrpAppntSchema.setCustomerNo(GrpNo);
//											this.mLDGrpSchema.setCustomerNo(GrpNo);
//											this.mLCGrpAddressSchema.setCustomerNo(GrpNo);
											this.mLCGrpContSchema.setGrpName(appntleaf.getText());
											this.mLCGrpAppntSchema.setName(appntleaf.getText());
											this.mLDGrpSchema.setGrpName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Phone")){
											this.mLCGrpContSchema.setPhone(appntleaf.getText());
											this.mLCGrpAppntSchema.setPhone(appntleaf.getText());
											this.mLDGrpSchema.setPhone(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Phone1")){
											this.mLCGrpAddressSchema.setPhone1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Mobile1")){
											this.mLCGrpAddressSchema.setMobile1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OrgancomCode")){
											this.mLCGrpAppntSchema.setOrganComCode(appntleaf.getText());
											this.mLDGrpSchema.setOrganComCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("UnifiedSocialCreditNo")){
											this.mLDGrpSchema.setUnifiedSocialCreditNo(appntleaf.getText());
											this.mLCGrpAppntSchema.setUnifiedSocialCreditNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PostalProvince")){
											this.mLCGrpAddressSchema.setPostalProvince(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PostalCity")){
											this.mLCGrpAddressSchema.setPostalCity(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("PostalCounty")){
											this.mLCGrpAddressSchema.setPostalCounty(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpAddress")){
											this.mLCGrpAddressSchema.setDetailAddress(appntleaf.getText());
//											this.mLCGrpAppntSchema.setPostalAddress(appntleaf.getText());
//											this.mLCGrpAddressSchema.setGrpAddress(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpZipCode")){
											this.mLCGrpAppntSchema.setZipCode(appntleaf.getText());
											this.mLCGrpAddressSchema.setGrpZipCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("TaxNo")){
											this.mLCGrpAppntSchema.setTaxNo(appntleaf.getText());
											continue;
										}
										//增加生僻字标识
										if(appntleaf.getLocalName().equals("UnCommonChar")){
											this.mLCGrpAppntSchema.setUnCommonChar(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("InsuredProperty")){
											this.mLDGrpSchema.setInsuredProperty(appntleaf.getText());
											this.mLCGrpAppntSchema.setInsuredProperty(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LinkMan1")){
											this.mLCGrpAddressSchema.setLinkMan1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDType")){
											this.mLCGrpAppntSchema.setIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDNo")){
											this.mLCGrpAppntSchema.setIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDStartDate")){
											this.mLCGrpAppntSchema.setIDStartDate(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDEndDate")){
											this.mLCGrpAppntSchema.setIDEndDate(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("IDLongEffFlag")){
											this.mLCGrpAppntSchema.setIDLongEffFlag(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Sex")){
											this.mLCGrpAddressSchema.setSex(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("NativePlace")){
											this.mLCGrpAddressSchema.setNativePlace(appntleaf.getText());
											continue;
										}
//										if(appntleaf.getLocalName().equals("NativeCity")){
//											this.mLCGrpAddressSchema.setNativeCity(appntleaf.getText());
//											continue;
//										}
										if(appntleaf.getLocalName().equals("OccupationCode")){
											this.mLCGrpAddressSchema.setOccupationCode(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OccupationtType")){
											this.mLCGrpAddressSchema.setOccupationType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderName")){
											this.mLCGrpAppntSchema.setShareholderName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDType")){
											this.mLCGrpAppntSchema.setShareholderIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDNo")){
											this.mLCGrpAppntSchema.setShareholderIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDStart")){
											this.mLCGrpAppntSchema.setShareholderIDStart(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ShareholderIDEnd")){
											this.mLCGrpAppntSchema.setShareholderIDEnd(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonName1")){
											this.mLCGrpAppntSchema.setLegalPersonName1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDType1")){
											this.mLCGrpAppntSchema.setLegalPersonIDType1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDNo1")){
											this.mLCGrpAppntSchema.setLegalPersonIDNo1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDStart1")){
											this.mLCGrpAppntSchema.setLegalPersonIDStart1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("LegalPersonIDEnd1")){
											this.mLCGrpAppntSchema.setLegalPersonIDEnd1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleName")){
											this.mLCGrpAppntSchema.setResponsibleName(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDType")){
											this.mLCGrpAppntSchema.setResponsibleIDType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDNo")){
											this.mLCGrpAppntSchema.setResponsibleIDNo(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDStart")){
											this.mLCGrpAppntSchema.setResponsibleIDStart(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDEnd")){
											this.mLCGrpAppntSchema.setResponsibleIDEnd(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("ResponsibleIDLongFlag")){
											this.mLCGrpAppntSchema.setResponsibleIDLongFlag(appntleaf.getText());
											continue;
										}

										
										if(appntleaf.getLocalName().equals("Fax1")){
											this.mLCGrpAddressSchema.setFax1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("E_Mail1")){
											this.mLCGrpAddressSchema.setE_Mail1(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessType")){
											this.mLCGrpContSchema.setBusinessType(appntleaf.getText());
											this.mLDGrpSchema.setBusinessType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessBigType")){
											this.mLCGrpContSchema.setBusinessBigType(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("GrpNature")){
											this.mLCGrpContSchema.setGrpNature(appntleaf.getText());
											this.mLDGrpSchema.setGrpNature(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Peoples")){
											this.mLCGrpContSchema.setPeoples(appntleaf.getText());
											this.mLCGrpAppntSchema.setPeoples(appntleaf.getText());
											this.mLDGrpSchema.setPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AppntOnWorkPeoples")){
											this.mLCGrpAppntSchema.setOnWorkPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOnWorkPeoples(appntleaf.getText());
											continue;
										}										
										if(appntleaf.getLocalName().equals("AppntOffWorkPeoples")){
											this.mLCGrpAppntSchema.setOffWorkPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOffWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AppntOtherPeoples")){
											this.mLCGrpAppntSchema.setOtherPeoples(appntleaf.getText());
											this.mLDGrpSchema.setOtherPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Peoples3")){
											this.mLCGrpContSchema.setPeoples3(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OnWorkPeoples")){
											this.mLCGrpContSchema.setOnWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OffWorkPeoples")){
											this.mLCGrpContSchema.setOffWorkPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("OtherPeoples")){
											this.mLCGrpContSchema.setOtherPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaPeoples")){
											this.mLCGrpContSchema.setRelaPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaMatePeoples")){
											this.mLCGrpContSchema.setRelaMatePeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("RelaYoungPeoples")){
											this.mLCGrpContSchema.setRelaYoungPeoples(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("Authorization")){
											this.mLCGrpAppntSchema.setAuthorization(appntleaf.getText());
											this.mLCPersonTraceSchema.setAuthorization(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("SharedMark")){
											this.mLCPersonTraceSchema.setSharedMark(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("SpecialLimitMark")){
											this.mLCPersonTraceSchema.setSpecialLimitMark(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("BusinessLink")){
											this.mLCPersonTraceSchema.setBusinessLink(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("CustomerContact")){
											this.mLCPersonTraceSchema.setCustomerContact(appntleaf.getText());
											continue;
										}
										if(appntleaf.getLocalName().equals("AuthType")){
											this.mLCPersonTraceSchema.setAuthType(appntleaf.getText());
											continue;
										}
									}
								}
							}
						}
						//险种信息
						if(child_element.getLocalName().equals("GRPRISK")){
							Iterator child_risk = child_element.getChildren();
							while(child_risk.hasNext()){
								OMElement contplan_item = (OMElement) child_risk.next();
								LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
								LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
								if(contplan_item.getLocalName().equals("ITEM")){
									Iterator risk_element = contplan_item.getChildren();
									while(risk_element.hasNext()){
										OMElement risk_info = (OMElement)risk_element.next();
										if(risk_info.getLocalName().equals("RiskID")){
											//暂时存到kindcode字段
//											tLCGrpPolSchema.setKindCode(risk_info.getText());
											continue;
										}
										if(risk_info.getLocalName().equals("RiskCode")){
											tLCGrpPolSchema.setRiskCode(risk_info.getText());
										    tLCContPlanDutyParamSchema.setRiskCode(risk_info.getText());
											continue;
										}
										if(risk_info.getLocalName().equals("MainRiskID")){
											//暂存到riskversion字段
//											tLCGrpPolSchema.setRiskVersion(risk_info.getText());
											continue;
										}
										if(risk_info.getLocalName().equals("CalRule")){
										    tLCContPlanDutyParamSchema.setCalFactorValue(risk_info.getText());
										    tLCContPlanDutyParamSchema.setCalFactor("CalRule");
											continue;
										}
									}
									this.mLCGrpPolSet.add(tLCGrpPolSchema);
									this.mLCContPlanDutyParamSetDefault.add(tLCContPlanDutyParamSchema);
								}
							}
						}
						//保障计划
						if(child_element.getLocalName().equals("CONTPLAN")){
							Iterator child_contplan = child_element.getChildren();
							while(child_contplan.hasNext()){
								OMElement contplan_item = (OMElement) child_contplan.next();
								if(contplan_item.getLocalName().equals("ITEM")){
									Iterator contplan_element = contplan_item.getChildren();
									LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
									LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
									String contplancode = "";
									while(contplan_element.hasNext()){
										OMElement contplanleaf = (OMElement)contplan_element.next();
										if(contplanleaf.getLocalName().equals("ContPlanCode")){
											tLCContPlanRiskSchema.setContPlanCode(contplanleaf.getText());
											tLCContPlanSchema.setContPlanCode(contplanleaf.getText());
											contplancode=contplanleaf.getText();
											continue;
										}
										if(contplanleaf.getLocalName().equals("ContPlanName")){
											tLCContPlanRiskSchema.setContPlanName(contplanleaf.getText());
											tLCContPlanSchema.setContPlanName(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("Peoples3")){
											tLCContPlanSchema.setPeoples3(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("Peoples2")){
											tLCContPlanSchema.setPeoples2(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("RiskCode")){
											tLCContPlanRiskSchema.setRiskCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("MainRiskCode")){
											tLCContPlanRiskSchema.setMainRiskCode(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("SumPrem")){
											tLCContPlanRiskSchema.setRiskPrem(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("RiskAmnt")){
											tLCContPlanRiskSchema.setRiskAmnt(contplanleaf.getText());
											continue;
										}
										if(contplanleaf.getLocalName().equals("AgeRange")){
											LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
											tLCContPlanDutyParamSchema.setContPlanCode(contplancode);
											tLCContPlanDutyParamSchema.setCalFactor("AgeRange");
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanleaf.getText());
											mLCContPlanDutyParamSetjxhw.add(tLCContPlanDutyParamSchema);
											continue;
										}
										if(contplanleaf.getLocalName().equals("Plan")){
											LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
											tLCContPlanDutyParamSchema.setContPlanCode(contplancode);
											tLCContPlanDutyParamSchema.setCalFactor("Plan");
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanleaf.getText());
											mLCContPlanDutyParamSetjxhw.add(tLCContPlanDutyParamSchema);
											continue;
										}
									}
									this.mLCContPlanSet.add(tLCContPlanSchema);
									this.mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
								}
							}
						}
						
						//保障计划要素
						if(child_element.getLocalName().equals("CONTPLANDUTYPARAM")){
							Iterator child_contplanparam = child_element.getChildren();
							while(child_contplanparam.hasNext()){
								OMElement contplanparam_item = (OMElement) child_contplanparam.next();
								LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("ContPlanCode")){
											tLCContPlanDutyParamSchema.setContPlanCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("ContPlanName")){
											tLCContPlanDutyParamSchema.setContPlanName(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("RiskCode")){
											tLCContPlanDutyParamSchema.setRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("MainRiskCode")){
											tLCContPlanDutyParamSchema.setMainRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("DutyCode")){
											tLCContPlanDutyParamSchema.setDutyCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactor")){
											if("AccGetRateP".equals(contplanparamleaf.getText()) || "AccGetRateG".equals(contplanparamleaf.getText())){
												tLCContPlanDutyParamSchema.setCalFactor("AccGetRate");
											}else{
												tLCContPlanDutyParamSchema.setCalFactor(contplanparamleaf.getText());
											}
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorType")){
											tLCContPlanDutyParamSchema.setCalFactorType(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorValue")){
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanparamleaf.getText());
											continue;
										}
									}
									this.mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
								}
							}
						}
						
						if(child_element.getLocalName().equals("INSULIST")){
							Iterator child_insu = child_element.getChildren();
							while(child_insu.hasNext()){
								OMElement insu_item = (OMElement) child_insu.next();
								LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
								if(insu_item.getLocalName().equals("ITEM")){
									Iterator insu_element = insu_item.getChildren();
									while(insu_element.hasNext()){
										OMElement insuleaf = (OMElement)insu_element.next();
										if(insuleaf.getLocalName().equals("InsuredID")){
											tLCInsuredListSchema.setInsuredID(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ContID")){
											tLCInsuredListSchema.setContNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Retire")){
											tLCInsuredListSchema.setRetire(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("RelationToInsured")){
											tLCInsuredListSchema.setRelation(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("MainInsuredName")){
											tLCInsuredListSchema.setEmployeeName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("InsuredName")){
											tLCInsuredListSchema.setInsuredName(insuleaf.getText());
											continue;
										}
										//增加生僻字标识
										if(insuleaf.getLocalName().equals("UnCommonChar")){
											tLCInsuredListSchema.setUnCommonChar(insuleaf.getText());
											continue;
										}
//										if(insuleaf.getLocalName().equals("NativePlace")){
//											tLCInsuredListSchema.setNativePlace(insuleaf.getText());
//											continue;
//										}
//										if(insuleaf.getLocalName().equals("NativeCity")){
//											tLCInsuredListSchema.setNativeCity(insuleaf.getText());
//											continue;
//										}
										if(insuleaf.getLocalName().equals("Sex")){
											tLCInsuredListSchema.setSex(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Birthday")){
											tLCInsuredListSchema.setBirthday(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDType")){
											tLCInsuredListSchema.setIDType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("IDNo")){
											tLCInsuredListSchema.setIDNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("ContPlanCode")){
											tLCInsuredListSchema.setContPlanCode(insuleaf.getText());
											continue;
										}
//										if(insuleaf.getLocalName().equals("Position2")){
//											tLCInsuredListSchema.setPosition2(insuleaf.getText());
//											continue;
//										}
//										if(insuleaf.getLocalName().equals("OccupationCode")){
//											tLCInsuredListSchema.setOccupationCode(insuleaf.getText());
//											continue;
//										}
										if(insuleaf.getLocalName().equals("OccupationType")){
											tLCInsuredListSchema.setOccupationType(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BankCode")){
											tLCInsuredListSchema.setBankCode(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("BankAccNo")){
											tLCInsuredListSchema.setBankAccNo(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("AccName")){
											tLCInsuredListSchema.setAccName(insuleaf.getText());
											continue;
										}
										if(insuleaf.getLocalName().equals("Phone")){
											tLCInsuredListSchema.setPhone(insuleaf.getText());
											continue;
										}
									}
									this.mLCInsuredListSet.add(tLCInsuredListSchema);
								}
							}
						}
						//团体账户信息
						if(child_element.getLocalName().equals("GRPACCOUNT")){
							Iterator child_grpaccount = child_element.getChildren();
							while(child_grpaccount.hasNext()){
								OMElement contplanparam_item = (OMElement) child_grpaccount.next();
								LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("RiskCode")){
											tLCContPlanDutyParamSchema.setRiskCode(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("InsuAccNo")){
											tLCContPlanDutyParamSchema.setInsuAccNo(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactor")){
											tLCContPlanDutyParamSchema.setCalFactor(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorType")){
											tLCContPlanDutyParamSchema.setCalFactorType(contplanparamleaf.getText());
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("CalFactorValue")){
											tLCContPlanDutyParamSchema.setCalFactorValue(contplanparamleaf.getText());
											continue;
										}
									}
									this.mLCContPlanDutyParamSet2.add(tLCContPlanDutyParamSchema);
								}
							}
						}
						//管理费信息
						if(child_element.getLocalName().equals("ManageFee")){
							Iterator child_managefee = child_element.getChildren();
							while(child_managefee.hasNext()){
								OMElement contplanparam_item = (OMElement) child_managefee.next();
								if(contplanparam_item.getLocalName().equals("ITEM")){
									Iterator contplanparam_element = contplanparam_item.getChildren();
									while(contplanparam_element.hasNext()){
										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
										if(contplanparamleaf.getLocalName().equals("AccManageFee")){
											LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
											tLCGrpFeeSchema.setFeeValue(contplanparamleaf.getText());
											tLCGrpFeeSchema.setFeeCode("000002");
											this.mLCGrpFeeSet.add(tLCGrpFeeSchema);
//											tLCGrpFeeSchema.setInsuAccNo("1000001");
//											tLCGrpFeeSchema.setPayPlanCode("652102");
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("AccReliefFee")){
											LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
											tLCGrpFeeSchema.setFeeValue(contplanparamleaf.getText());
											tLCGrpFeeSchema.setFeeCode("000006");
											this.mLCGrpFeeSet.add(tLCGrpFeeSchema);
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("FixAccManageFee")){
											continue;
										}
										if(contplanparamleaf.getLocalName().equals("ClaimNum")){
											ClaimNum=contplanparamleaf.getText();
											continue;
										}
									}
								}
							}
						}
						if(child_element.getLocalName().equals("CONTROAD")){
							Iterator child_controad = child_element.getChildren();
							while(child_controad.hasNext()){
								OMElement controadparam_item = (OMElement) child_controad.next();
								if(controadparam_item.getLocalName().equals("ITEM")){
									Iterator controadparam_element = controadparam_item.getChildren();
									while(controadparam_element.hasNext()){
										OMElement controadparamleaf = (OMElement) controadparam_element.next();
										if(controadparamleaf.getLocalName().equals("CountryCategory")){
											this.mLCGrpContRoadSchema.setCountryCategory(controadparamleaf.getText());
											continue;
										}
										if(controadparamleaf.getLocalName().equals("Country")){
											this.mLCGrpContRoadSchema.setCountry(controadparamleaf.getText());
											continue;
										}
										if(controadparamleaf.getLocalName().equals("EngineeringFlag")){
											this.mLCGrpContRoadSchema.setEngineeringFlag(controadparamleaf.getText());
											continue;
										}
										if(controadparamleaf.getLocalName().equals("EngineeringCategory")){
											this.mLCGrpContRoadSchema.setEngineeringCategory(controadparamleaf.getText());
											continue;
										}
									}
								}
							}
						}
						if(child_element.getLocalName().equals("CONSTRUCTION")){
							Iterator child_construction = child_element.getChildren();
							while(child_construction.hasNext()){
								OMElement constructionparam_item = (OMElement) child_construction.next();
								if(constructionparam_item.getLocalName().equals("ITEM")){
									Iterator constructionparam_element = constructionparam_item.getChildren();
									while(constructionparam_element.hasNext()){
										OMElement constructionparamleaf = (OMElement) constructionparam_element.next();
										if(constructionparamleaf.getLocalName().equals("ProvinceID")){
											this.mLCGrpContSubSchemajgx.setbak2(constructionparamleaf.getText());
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("CityID")){
											this.mLCGrpContSubSchemajgx.setbak3(constructionparamleaf.getText());
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("CountyID")){
											this.mLCGrpContSubSchemajgx.setbak4(constructionparamleaf.getText());
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("DetailAddress")){
											this.mLCGrpContSubSchemajgx.setbak5(constructionparamleaf.getText());
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("ProjectAddress")){
											LCContPlanDutyParamSchema lccontplandutyparam = new LCContPlanDutyParamSchema();
											lccontplandutyparam.setCalFactor("ProjectAddress");
											lccontplandutyparam.setCalFactorType("0");
											lccontplandutyparam.setCalFactorValue(constructionparamleaf.getText());
											this.mLCContPlanDutyParamSetjgx.add(lccontplandutyparam);
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("ProjectName")){
											LCContPlanDutyParamSchema lccontplandutyparam = new LCContPlanDutyParamSchema();
											lccontplandutyparam.setCalFactor("ProjectName");
											lccontplandutyparam.setCalFactorType("0");
											lccontplandutyparam.setCalFactorValue(constructionparamleaf.getText());
											this.mLCContPlanDutyParamSetjgx.add(lccontplandutyparam);
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("ProjectNo")){
											LCContPlanDutyParamSchema lccontplandutyparam = new LCContPlanDutyParamSchema();
											lccontplandutyparam.setCalFactor("ProjectNo");
											lccontplandutyparam.setCalFactorType("0");
											lccontplandutyparam.setCalFactorValue(constructionparamleaf.getText());
											this.mLCContPlanDutyParamSetjgx.add(lccontplandutyparam);
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("StandbyFlag1")){
											LCContPlanDutyParamSchema lccontplandutyparam = new LCContPlanDutyParamSchema();
											lccontplandutyparam.setCalFactor("StandbyFlag1");
											lccontplandutyparam.setCalFactorType("0");
											lccontplandutyparam.setCalFactorValue(constructionparamleaf.getText());
											this.mLCContPlanDutyParamSetjgx.add(lccontplandutyparam);
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("StandbyFlag2")){
											LCContPlanDutyParamSchema lccontplandutyparam = new LCContPlanDutyParamSchema();
											lccontplandutyparam.setCalFactor("StandbyFlag2");
											lccontplandutyparam.setCalFactorType("0");
											lccontplandutyparam.setCalFactorValue(constructionparamleaf.getText());
											this.mLCContPlanDutyParamSetjgx.add(lccontplandutyparam);
											continue;
										}
										if(constructionparamleaf.getLocalName().equals("StandbyFlag3")){
											LCContPlanDutyParamSchema lccontplandutyparam = new LCContPlanDutyParamSchema();
											lccontplandutyparam.setCalFactor("StandbyFlag3");
											lccontplandutyparam.setCalFactorType("0");
											lccontplandutyparam.setCalFactorValue(constructionparamleaf.getText());
											this.mLCContPlanDutyParamSetjgx.add(lccontplandutyparam);
											continue;
										}
									}
								}
							}
						}
						//收费信息
//						if(child_element.getLocalName().equals("PayInfo")){
//							Iterator child_managefee = child_element.getChildren();
//							while(child_managefee.hasNext()){
//								OMElement contplanparam_item = (OMElement) child_managefee.next();
//								if(contplanparam_item.getLocalName().equals("ITEM")){
//									Iterator contplanparam_element = contplanparam_item.getChildren();
//									while(contplanparam_element.hasNext()){
//										OMElement contplanparamleaf = (OMElement)contplanparam_element.next();
//										if(contplanparamleaf.getLocalName().equals("PayMode")){
//											mLJTempFeeClassSchema.setPayMode(contplanparamleaf.getText());
//											continue;
//										}
//										if(contplanparamleaf.getLocalName().equals("PayMoney")){
//											mLJTempFeeClassSchema.setPayMoney(contplanparamleaf.getText());
//											payMoney = contplanparamleaf.getText();
//											continue;
//										}
//										if(contplanparamleaf.getLocalName().equals("PayEntAccDate")){
//											mLJTempFeeClassSchema.setEnterAccDate(contplanparamleaf.getText());
//											continue;
//										}
//									}
//								}
//							}
//						}
						
						
					}
				}else{
					System.out.println("报文类型不匹配");
					return false;
				}
			}
		}catch(Exception e){
			System.out.println("加载报文信息异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 校验
	 * @return
	 */
	private boolean check(){
		if(!checkInfo()){
			System.out.println("校验基本信息失败");
			return false;
		}
		if(!"1".equals(mLCGrpAppntSchema.getUnCommonChar())){
		//保单登记联系人姓名校验
				String name= PubFun.checkGrpName(mLCGrpAppntSchema.getIDType(),mLCGrpAddressSchema.getLinkMan1());
				if(!"".equals(name)&&name!=null){
		     		responseOME = buildResponseOME("01","04", "联系人："+mLCGrpAddressSchema.getLinkMan1()+name);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "联系人："+mLCGrpAddressSchema.getLinkMan1()+name);
					System.out.println("responseOME:" + responseOME);
		     		return false;
		     	 }	
		}
				//保单登记联系人国籍校验
				String nation= PubFun.checkNation(mLCGrpAppntSchema.getIDType(),mLCGrpAddressSchema.getNativePlace());
				if(!"".equals(nation)&&nation!=null){
		     		responseOME = buildResponseOME("01","04", "联系人："+mLCGrpAddressSchema.getLinkMan1()+nation);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "联系人："+mLCGrpAddressSchema.getLinkMan1()+nation);
					System.out.println("responseOME:" + responseOME);
		     		return false;
		     	 }
				//保单登记联系人证件校验 by BXF
				String chkIDNo1=PubFun.CheckIDNo(mLCGrpAppntSchema.getIDType(), mLCGrpAppntSchema.getIDNo(),"","");
		   	 if(!"".equals(chkIDNo1)&&chkIDNo1!=null){
		   		responseOME = buildResponseOME("01","04", "联系人："+mLCGrpAddressSchema.getLinkMan1()+chkIDNo1);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "联系人："+mLCGrpAddressSchema.getLinkMan1()+chkIDNo1);
				System.out.println("responseOME:" + responseOME);
		   		 return false;
			 }
		   	if("8".equals(mLCGrpAppntSchema.getIDType())){
      	    	int years=PubFun.calInterval(mLCGrpAppntSchema.getIDStartDate(), mLCGrpAppntSchema.getIDEndDate(), "Y");
      	    	if(years>10){
      	    		responseOME = buildResponseOME("01","04", "控股人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
      				System.out.println("responseOME:" + responseOME);
      	     		return false;
      	    	}
      	     }
		   	if(this.mLCGrpAppntSchema.getInsuredProperty().equals("1")){
		      	 //法人校验
		      	 if(mLCGrpAppntSchema.getLegalPersonName1()!=null&&!mLCGrpAppntSchema.getLegalPersonName1().equals("")){
		      			 if(!"1".equals(mLCGrpAppntSchema.getUnCommonChar())){
		      			 String legalName = PubFun.checkGrpName(mLCGrpAppntSchema.getLegalPersonIDType1(),mLCGrpAppntSchema.getLegalPersonName1());
		      	     	if(!"".equals(legalName)&&legalName!=null){
		      	     		responseOME = buildResponseOME("01","04", "法人："+mLCGrpAppntSchema.getLegalPersonName1()+legalName);
		      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "法人："+mLCGrpAppntSchema.getLegalPersonName1()+legalName);
		      				System.out.println("responseOME:" + responseOME);
		      	     		return false;
		      	     	 }
		      			 }
		      	    	 //法人证件类型校验
		      	    	String prsonIDNo=PubFun.CheckIDNo(mLCGrpAppntSchema.getLegalPersonIDType1(), mLCGrpAppntSchema.getLegalPersonIDNo1(),"","");
		             	 if(!"".equals(prsonIDNo)&&prsonIDNo!=null){
		             		responseOME = buildResponseOME("01","04", "法人："+mLCGrpAppntSchema.getShareholderName()+prsonIDNo);
		      			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "法人："+mLCGrpAppntSchema.getShareholderName()+prsonIDNo);
		      			System.out.println("responseOME:" + responseOME);
		             		 return false;
		          	 }
		      	     
		      	     if("8".equals(mLCGrpAppntSchema.getLegalPersonIDType1())){
		      	    	int years=PubFun.calInterval(mLCGrpAppntSchema.getLegalPersonIDStart1(), mLCGrpAppntSchema.getLegalPersonIDEnd1(), "Y");
		      	    	if(years>10){
		      	    		responseOME = buildResponseOME("01","04", "法人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
		      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "法人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
		      				System.out.println("responseOME:" + responseOME);
		      	     		return false;
		      	    	}
		      	     }
		      	 }
		      	 
		      //控股人校验
		      	 if(mLCGrpAppntSchema.getShareholderName()!=null&&!mLCGrpAppntSchema.getShareholderName().equals("")){
		      			 if(!"1".equals(mLCGrpAppntSchema.getUnCommonChar())){
		      			 String holderName = PubFun.checkGrpName(mLCGrpAppntSchema.getShareholderIDType(),mLCGrpAppntSchema.getShareholderName());
		      	     	if(!"".equals(holderName)&&holderName!=null){
		      	     		responseOME = buildResponseOME("01","04", "控股人："+mLCGrpAppntSchema.getShareholderName()+holderName);
		      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人："+mLCGrpAppntSchema.getShareholderName()+holderName);
		      				System.out.println("responseOME:" + responseOME);
		      	     		return false;
		      	     	 }
		      			 }
		      	    	 //控股人证件类型校验
		      	    	String prsonIDNo=PubFun.CheckIDNo(mLCGrpAppntSchema.getShareholderIDType(), mLCGrpAppntSchema.getShareholderIDNo(),"","");
		             	 if(!"".equals(prsonIDNo)&&prsonIDNo!=null){
		             		responseOME = buildResponseOME("01","04", "控股人："+mLCGrpAppntSchema.getShareholderName()+prsonIDNo);
		      			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人："+mLCGrpAppntSchema.getShareholderName()+prsonIDNo);
		      			System.out.println("responseOME:" + responseOME);
		             		 return false;
		          	 }
		      	     if("8".equals(mLCGrpAppntSchema.getShareholderIDType())){
		      	    	int years=PubFun.calInterval(mLCGrpAppntSchema.getShareholderIDStart(), mLCGrpAppntSchema.getShareholderIDEnd(), "Y");
		      	    	if(years>10){
		      	    		responseOME = buildResponseOME("01","04", "控股人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
		      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
		      				System.out.println("responseOME:" + responseOME);
		      	     		return false;
		      	    	}
		      	     }
		      		 
		      	 }
		      //控股人校验
		      	 if(mLCGrpAppntSchema.getResponsibleName()!=null&&!mLCGrpAppntSchema.getResponsibleName().equals("")){
		      			 if(!"1".equals(mLCGrpAppntSchema.getUnCommonChar())){
		      			 String responsibleName = PubFun.checkGrpName(mLCGrpAppntSchema.getResponsibleIDType(),mLCGrpAppntSchema.getResponsibleName());
		      	     	if(!"".equals(responsibleName)&&responsibleName!=null){
		      	     		responseOME = buildResponseOME("01","04", "控股人："+mLCGrpAppntSchema.getResponsibleName()+responsibleName);
		      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人："+mLCGrpAppntSchema.getResponsibleName()+responsibleName);
		      				System.out.println("responseOME:" + responseOME);
		      	     		return false;
		      	     	 }
		      			 }
		      	    	 //控股人证件类型校验
		      	    	String prsonIDNo=PubFun.CheckIDNo(mLCGrpAppntSchema.getResponsibleIDType(), mLCGrpAppntSchema.getResponsibleIDNo(),"","");
		             	 if(!"".equals(prsonIDNo)&&prsonIDNo!=null){
		             		responseOME = buildResponseOME("01","04", "控股人："+mLCGrpAppntSchema.getResponsibleName()+prsonIDNo);
		      			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人："+mLCGrpAppntSchema.getResponsibleName()+prsonIDNo);
		      			System.out.println("responseOME:" + responseOME);
		             		 return false;
		          	 }
		      	     if("8".equals(mLCGrpAppntSchema.getShareholderIDType())){
		      	    	int years=PubFun.calInterval(mLCGrpAppntSchema.getResponsibleIDStart(), mLCGrpAppntSchema.getResponsibleIDEnd(), "Y");
		      	    	if(years>10){
		      	    		responseOME = buildResponseOME("01","04", "控股人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
		      				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "控股人证件类型为外国人永久居留身份证时,证件有效期最长为10年");
		      				System.out.println("responseOME:" + responseOME);
		      	     		return false;
		      	    	}
		      	     }
		      		 
		      	 }
		      	 }
		//处理团体投保客户，是否需要生成新客户
		if(!checkGrpName()){
			System.out.println("客户信息生成失败");
			return false;
		}
		//校验险种关系
		if(!checkRiskInfo()){
			System.out.println("校验险种关系失败");
			return false;
		}
		
		return true;
	}
	
	/**
	 * 处理
	 * @return
	 */ 
	private boolean deal(){
		try{
	        mProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",mLCGrpAppntSchema.getCustomerNo());
	        if("".equals(mProposalGrpContNo) || mProposalGrpContNo == null){
	        	responseOME = buildResponseOME("01","11", "生成保单号码失败!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","11", "WX", "生成保单号码失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
	        }
	        if(!creatGrpCont()){
	        	delete();
				return false;
	        }
	        if(!creatGrpPol()){
	        	delete();
	        	return false;
	        }
	        if(check163002()){//处理一带一路相关信息
	        	if(!creatRoad()){
	        		delete();
	        		return false;
	        	}
	        }
	        //判断是否生成公共账户信息和管理费信息(690101险种)
	        if(check690101()){
	        	if(!creatPubAccContPlan()){
	        		delete();
	        		return false;
	        	}
	        	if(!createManageFee()){
	        		delete();
	        		return false;
	        	}
	        }else{
	        	if(!createdeDefaultContplan()){
	        		delete();
	        		return false;
	        	}
	        }
	        if(!creatContPlan()){
	        	delete();
	        	return false;
	        }
	        if(!creatInsured()){
	        	delete();
	        	return false;
	        }
		}catch(Exception e){
			System.out.println("处理团单电子商务复杂产品异常！");
			e.printStackTrace();
			return false;
		}
									
		return true;
	}
	
	/**
	 *校验基本信息 
	 * @return
	 */
	private boolean checkInfo(){
		if(this.batchno ==null || this.batchno.equals("")){
			responseOME = buildResponseOME("01","02", "没有得到批次号!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","02", "WX", "没有得到批次号!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if("".equals(this.mLCGrpContSchema.getPrtNo()) || this.mLCGrpContSchema.getPrtNo() == null){
			responseOME = buildResponseOME("01","03", "获取保单印刷号码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","03", "WX", "获取保单印刷号码失败!");
			return false;
		}else{
			String sql = "select 1 from lcgrpcont where prtno='"+this.mLCGrpContSchema.getPrtNo()+"'";
			String sqlRes = new ExeSQL().getOneValue(sql);
			if (!StrTool.cTrim(sqlRes).equals("") && !StrTool.cTrim(sqlRes).equals("null")){
				responseOME = buildResponseOME("01","04", "该保单印刷号码已存在!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "该保单印刷号码已存在!");
				return false;
			}
		}
		if(this.sendDate ==null || this.sendDate.equals("") || this.sendTime ==null || this.sendTime.equals("") || !PubFun.checkDateForm(this.sendDate)){
			responseOME = buildResponseOME("01","02", "获取报文发送日期或时间失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取报文发送日期或时间失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.sendoperator == null || this.sendoperator.equals("")){
			responseOME = buildResponseOME("01","02", "获取操作员失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取操作员失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getManageCom() == null || this.mLCGrpContSchema.getManageCom().equals("")){
			responseOME = buildResponseOME("01","02", "获取管理机构失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取管理机构失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getCardFlag() == null || this.mLCGrpContSchema.getCardFlag().equals("")){
			responseOME = buildResponseOME("01","02", "获取团单标识失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取团单标识失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from ldcode where codetype = 'cardflagforgrp' and code='"+this.mLCGrpContSchema.getCardFlag()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取团单标识失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取团单标识失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getSaleChnl() == null || this.mLCGrpContSchema.getSaleChnl().equals("")){
			responseOME = buildResponseOME("01","02", "获取销售渠道失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取销售渠道失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from ldcode where codetype = 'unitesalechnlsgrp' and code='"+this.mLCGrpContSchema.getSaleChnl()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取销售渠道失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取销售渠道失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}else{
				String a = this.mLCGrpContSchema.getSaleChnl();
				if("03".equals(a) || "04".equals(a) || "10".equals(a) || "15".equals(a) || "20".equals(a) || "23".equals(a) ) {
					if(this.mLCGrpContSchema.getAgentCom()==null || "".equals(this.mLCGrpContSchema.getAgentCom()) || "null".equals(this.mLCGrpContSchema.getAgentCom())){
						responseOME = buildResponseOME("01","02", "销售渠道为中介，请录入中介公司代码！");
						LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "销售渠道为中介，请录入中介公司代码！");
						System.out.println("rsponseOME:" + responseOME);
						return false;
					}
				}else{
					this.mLCGrpContSchema.setAgentCom("");
				}
			}
		}
		if("".equals(this.mixComFlag)||this.mixComFlag==null||(!"0".equals(this.mixComFlag)&&!"1".equals(this.mixComFlag))){
			responseOME = buildResponseOME("01","02", "获取交叉销售标识失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取交叉销售标识失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if("16".equals(this.mLCGrpContSchema.getSaleChnl()) || "18".equals(this.mLCGrpContSchema.getSaleChnl()) || "20".equals(this.mLCGrpContSchema.getSaleChnl())){
			if("1".equals(this.mixComFlag)){
				responseOME = buildResponseOME("01","02", "销售渠道为社保直销、社保综拓直销、社保综拓中介时不支持交叉销售的保单！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "销售渠道为社保直销、社保综拓直销、社保综拓中介时不支持交叉销售的保单！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if("1".equals(mixComFlag)){
			if(this.mLCGrpContSchema.getCrs_SaleChnl()==null||"".equals(this.mLCGrpContSchema.getCrs_SaleChnl())
					||this.mLCGrpContSchema.getCrs_BussType()==null||"".equals(this.mLCGrpContSchema.getCrs_BussType())
					||this.mLCGrpContSchema.getGrpAgentCode()==null||"".equals(this.mLCGrpContSchema.getGrpAgentCode())
					||this.mLCGrpContSchema.getGrpAgentCom()==null||"".equals(this.mLCGrpContSchema.getGrpAgentCom())
					||this.mLCGrpContSchema.getGrpAgentName()==null||"".equals(this.mLCGrpContSchema.getGrpAgentName())
					||this.mLCGrpContSchema.getGrpAgentIDNo()==null||"".equals(this.mLCGrpContSchema.getGrpAgentIDNo())){
				responseOME = buildResponseOME("01","02", "交叉销售保单的交叉销售渠道、集团交叉业务类型、对方业务员机构、代码、姓名、身份证都不能为空!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "交叉销售保单的交叉销售渠道、集团交叉业务类型、对方业务员机构、代码、姓名、身份证都不能为空!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			String sql = "select 1 from ldcode where codetype = 'crs_salechnl' and code='"+this.mLCGrpContSchema.getCrs_SaleChnl()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取交叉销售渠道失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取交叉销售渠道失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			String sql2 = "select 1 from ldcode where codetype = 'crs_busstype' and code='"+this.mLCGrpContSchema.getCrs_BussType()+"'";
			SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
			if(tSSRS2 == null || tSSRS2.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取交叉销售业务类型失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取交叉销售业务类型失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			String tWhereSQL  = "othersign in('2')";
			if("15".equals(this.mLCGrpContSchema.getSaleChnl())){
				tWhereSQL = "othersign in('1')";
			}
			if("14".equals(this.mLCGrpContSchema.getSaleChnl())){
				tWhereSQL = "othersign in('3','2')";
			}
			String mSQL = "select 1 from ldcode where codetype = 'crs_busstype' and code='"+this.mLCGrpContSchema.getCrs_BussType()+
					"' and  " + tWhereSQL + "with ur";
			SSRS tSSRS3 = new ExeSQL().execSQL(mSQL);
			if(tSSRS3 == null || tSSRS3.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "销售渠道与交叉销售业务类型不匹配！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "销售渠道与交叉销售业务类型不匹配！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			//交叉销售增加正确性校验
			if(this.mLCGrpContSchema.getGrpAgentCode().trim().length()!=10){
				responseOME = buildResponseOME("01","02", "交叉销售对方业务员代码字段必须为10位，请核实!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "交叉销售对方业务员代码字段必须为10位，请核实!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}else if(this.mLCGrpContSchema.getCrs_SaleChnl().equals("01")&&!this.mLCGrpContSchema.getGrpAgentCode().substring(0,1).equals("1")){
				responseOME = buildResponseOME("01","02", "交叉销售渠道是产代健，对方业务员代码需以数字1开头!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "交叉销售渠道是产代健，对方业务员代码需以数字1开头!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}else if(this.mLCGrpContSchema.getCrs_SaleChnl().equals("02")&&!this.mLCGrpContSchema.getGrpAgentCode().substring(0,1).equals("3")){
				responseOME = buildResponseOME("01","02", "交叉销售渠道是寿代健，对方业务员代码需以数字3开头!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "交叉销售渠道是寿代健，对方业务员代码需以数字3开头!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			//通过调用集团接口获取交叉销售数据，然后跟录入比对是否一致
			MixedSalesAgentQueryUI mixedSalesAgentQueryUI = new MixedSalesAgentQueryUI();
			RspSaleInfo rspSaleInfo = new RspSaleInfo();
			 try{
			rspSaleInfo = mixedSalesAgentQueryUI.getSaleInfo(this.mLCGrpContSchema.getGrpAgentCode(),this.mLCGrpContSchema.getManageCom());
			 }catch(Exception ex){
				  responseOME = buildResponseOME("01","02", "集团获取交叉销售数据失败！");
					LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "集团获取交叉销售数据失败！");
					System.out.println("rsponseOME:" + responseOME);
					return false;
			  }
			  String str = "";
			  String type = rspSaleInfo.getMESSAGETYPE();
			  String typeInfo = rspSaleInfo.getERRDESC();
			  if("01".equals(type)){
				responseOME = buildResponseOME("01","02", "交叉销售数据错误："+typeInfo);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX",  "交叉销售数据错误："+typeInfo);
				System.out.println("rsponseOME:" + responseOME);
				return false;	  	
			  }else {
				  if(!this.mLCGrpContSchema.getGrpAgentCode().equals(rspSaleInfo.getUNI_SALES_COD())||
						  !this.mLCGrpContSchema.getGrpAgentCom().equals(rspSaleInfo.getMAN_ORG_COD())|| 
						  !this.mLCGrpContSchema.getGrpAgentName().equals(rspSaleInfo.getSALES_NAM())||
						  !this.mLCGrpContSchema.getGrpAgentIDNo().equals(rspSaleInfo.getID_NO())){
				responseOME = buildResponseOME("01","02", "录入交叉销售信息与集团返回结果不一致，请核实!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "录入交叉销售信息与集团返回结果不一致，请核实!");
				System.out.println("rsponseOME:" + responseOME);
				return false;					  
				  }			  	
			  	} 	
		}else{
			if(this.mLCGrpContSchema.getCrs_SaleChnl()!=null&&!"".equals(this.mLCGrpContSchema.getCrs_SaleChnl())){
				responseOME = buildResponseOME("01","02", "保单为非交叉销售时请不要传入交叉销售渠道！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "保单为非交叉销售时请不要传入交叉销售渠道！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			if(this.mLCGrpContSchema.getCrs_BussType()!=null&&!"".equals(this.mLCGrpContSchema.getCrs_BussType())){
				responseOME = buildResponseOME("01","02", "保单为非交叉销售时请不要传入集团交叉业务类型！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "保单为非交叉销售时请不要传入集团交叉业务类型！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			if(this.mLCGrpContSchema.getGrpAgentCode()!=null&&!"".equals(this.mLCGrpContSchema.getGrpAgentCode())){
				responseOME = buildResponseOME("01","02", "保单为非交叉销售时请不要传入对方业务员代码！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "保单为非交叉销售时请不要传入对方业务员代码！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			if(this.mLCGrpContSchema.getGrpAgentCom()!=null&&!"".equals(this.mLCGrpContSchema.getGrpAgentCom())){
				responseOME = buildResponseOME("01","02", "保单为非交叉销售时请不要传入对方业务员机构！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "保单为非交叉销售时请不要传入对方业务员机构！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			if(this.mLCGrpContSchema.getGrpAgentName()!=null&&!"".equals(this.mLCGrpContSchema.getGrpAgentName())){
				responseOME = buildResponseOME("01","02", "保单为非交叉销售时请不要传入对方业务员姓名！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "保单为非交叉销售时请不要传入对方业务员姓名！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
			if(this.mLCGrpContSchema.getGrpAgentIDNo()!=null&&!"".equals(this.mLCGrpContSchema.getGrpAgentIDNo())){
				responseOME = buildResponseOME("01","02", "保单为非交叉销售时请不要传入对方业务员身份证！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "保单为非交叉销售时请不要传入对方业务员身份证！");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getAgentSaleCode()!=null && !"".equals(this.mLCGrpContSchema.getAgentSaleCode()) && ("03".equals(this.mLCGrpContSchema.getSaleChnl())||"04".equals(this.mLCGrpContSchema.getSaleChnl())||"15".equals(this.mLCGrpContSchema.getSaleChnl())||"20".equals(this.mLCGrpContSchema.getSaleChnl())||"23".equals(this.mLCGrpContSchema.getSaleChnl()))){
			String strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + this.mLCGrpContSchema.getAgentCode() + "' and entryno = '" + this.mLCGrpContSchema.getAgentCom() + "' and managecom like '"+this.mLCGrpContSchema.getManageCom()+"%'";
	        SSRS arrResult = new ExeSQL().execSQL(strSql);
	        if (arrResult.getMaxRow()<1)
	        {
	            responseOME = buildResponseOME("01","02", "代理销售业务员不存在!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "代理销售业务员不存在!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
	        }
		}else{
			this.mLCGrpContSchema.setAgentSaleCode("");
		}
		if(this.mLCGrpContSchema.getOutPayFlag()==null || this.mLCGrpContSchema.getOutPayFlag().equals("")){
			responseOME = buildResponseOME("01","02", "获取溢交保费方式失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取溢交保费方式失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			if(!"1".equals(this.mLCGrpContSchema.getOutPayFlag())&&!"2".equals(this.mLCGrpContSchema.getOutPayFlag())){
				responseOME = buildResponseOME("01","02", "获取溢交保费方式失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取溢交保费方式失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getHandlerDate() == null || this.mLCGrpContSchema.getHandlerDate().equals("") || !PubFun.checkDateForm(this.mLCGrpContSchema.getHandlerDate())){
			responseOME = buildResponseOME("01","02", "获取投保单填写日期失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取投保单填写日期失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getFirstTrialOperator() == null || this.mLCGrpContSchema.getFirstTrialOperator().equals("")){
			responseOME = buildResponseOME("01","02", "获取初审人员代码失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取初审人员代码失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getMarketType()==null || this.mLCGrpContSchema.getMarketType().equals("")){
			responseOME = buildResponseOME("01","02", "获取市场类型失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取市场类型失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from ldcode where codetype = 'markettype' and code='"+this.mLCGrpContSchema.getMarketType()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取市场类型失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取市场类型失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getReceiveDate()==null || this.mLCGrpContSchema.getReceiveDate().equals("") ||!PubFun.checkDateForm(this.mLCGrpContSchema.getReceiveDate()) ){
			responseOME = buildResponseOME("01","02", "获取接收日期失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取接受日期失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		String strCheckDate = checkDate();
		if(!"".equals(strCheckDate)){
			responseOME = buildResponseOME("01","02", strCheckDate);
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", strCheckDate);
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		
		if(this.mLCGrpContSchema.getCoInsuranceFlag()==null || this.mLCGrpContSchema.getCoInsuranceFlag().equals("")){
			responseOME = buildResponseOME("01","02", "获取保单属性是否为共保保单失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取保单属性共保保单失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		// 校验中介机构是否是共保机构
		if(!checkAgentCom()){
			responseOME = buildResponseOME("01","02", "团险中介的中介机构不能为共保的机构!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "团险中介的中介机构不能为共保的机构!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}
		if(this.mLCGrpContSchema.getPayMode()==null || this.mLCGrpContSchema.getPayMode().equals("")){
			responseOME = buildResponseOME("01","02", "获取缴费方式失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费方式失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql = "select 1 from dual where '"+this.mLCGrpContSchema.getPayMode()+"' in ('1','3','4')";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取缴费方式失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费方式失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getPayIntv()!=0 && this.mLCGrpContSchema.getPayIntv()!=1 && this.mLCGrpContSchema.getPayIntv()!=3 && this.mLCGrpContSchema.getPayIntv()!=6 && this.mLCGrpContSchema.getPayIntv()!=12){
			responseOME = buildResponseOME("01","02", "获取缴费频次失败!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费频次失败!");
			System.out.println("rsponseOME:" + responseOME);
			return false;
		}else{
			String sql ="select 1 from ldcode where codetype = 'grppayintv' and code='"+this.mLCGrpContSchema.getPayIntv()+"'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS == null || tSSRS.MaxRow<=0){
				responseOME = buildResponseOME("01","02", "获取缴费频次失败!");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType, sendDate, sendTime, "01", "02", "WX", "获取缴费频次失败!");
				System.out.println("rsponseOME:" + responseOME);
				return false;
			}
		}
		if(this.mLCGrpContSchema.getCValiDate()==null || this.mLCGrpContSchema.getCValiDate().equals("") || !PubFun.checkDateForm(this.mLCGrpContSchema.getCValiDate()) ){
			responseOME = buildResponseOME("01","05", "获取保单生效日期失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保单生效日期失败!");
			return false;
		}
		if(this.mLCGrpContSchema.getCInValiDate()==null || this.mLCGrpContSchema.getCInValiDate().equals("") || !PubFun.checkDateForm(this.mLCGrpContSchema.getCInValiDate()) ){
			responseOME = buildResponseOME("01","05", "获取保单失效日期失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保单失效日期失败!");
			return false;
		}
		//保单生效日期必须小于保单失效日期
		if(!"".equals(diffdate(this.mLCGrpContSchema.getCValiDate(),this.mLCGrpContSchema.getCInValiDate()))){
			String a = diffdate(this.mLCGrpContSchema.getCValiDate(),this.mLCGrpContSchema.getCInValiDate());
			responseOME = buildResponseOME("01","05", a);
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "保单失效日期不应在保单生效日期之前!");
			return false;
		}
		//保单生效日期最多为投保日期后一年
		if(PubFun.calInterval2(this.mLCGrpContSchema.getHandlerDate(), this.mLCGrpContSchema.getCValiDate(),"D") > 365) {
			responseOME = buildResponseOME("01","05", "保单生效日期最多为投保填写日期后一年!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "保单生效日期最多为投保填写日期后一年!");
		    return false;
		}
		if(PubFun.calInterval2(this.mLCGrpContSchema.getCValiDate(), this.mLCGrpContSchema.getHandlerDate(),"D") > 365) {
			responseOME = buildResponseOME("01","05", "保单生效日期最多为投保填写日期前一年!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "保单生效日期最多为投保填写日期前一年!");
		    return false;
		}
		if(this.mLCGrpContSchema.getGrpName()==null || this.mLCGrpContSchema.getGrpName().equals("")){
			responseOME = buildResponseOME("01","05", "获取投保单位名称失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保单位名称失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getPhone()==null || this.mLCGrpAppntSchema.getPhone().equals("")){
			responseOME = buildResponseOME("01","05", "获取投保单位联系电话失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保单位联系电话失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getInsuredProperty()==null || this.mLCGrpAppntSchema.getInsuredProperty().equals("")){
			responseOME = buildResponseOME("01","05", "获取投保人属性失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保人属性失败!");
			return false;
		}else{
			if(this.mLCGrpAppntSchema.getInsuredProperty().equals("1")){//法人
				if(this.mLCGrpAppntSchema.getOrganComCode()==null || this.mLCGrpAppntSchema.getOrganComCode().equals("")){
					if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo()==null || this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().equals("")){
						responseOME = buildResponseOME("01","05", "投保人属性为法人时，组织机构代码、统一社会信用代码至少录入其一!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，组织机构代码、统一社会信用代码至少录入其一!");
						return false;
					}else{
						if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().length()!=18){
							responseOME = buildResponseOME("01","05", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							return false;
						}
					}
				}else{
					if(this.mLCGrpAppntSchema.getOrganComCode().length()!=10){
						responseOME = buildResponseOME("01","05", "组织机构代码不符合规定的录入长度!长度需要等于10!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "组织机构代码不符合规定的录入长度!长度需要等于10!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo()!=null && !this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().equals("")){
						if(this.mLCGrpAppntSchema.getUnifiedSocialCreditNo().length()!=18){
							responseOME = buildResponseOME("01","05", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "统一社会信用代码不符合规定的录入长度!长度需要等于18!");
							return false;
						}
					}
				}
				if(this.mLCGrpContSchema.getBusinessType()==null || this.mLCGrpContSchema.getBusinessType().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，行业编码不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，行业编码不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype = 'businesstype' and code='"+this.mLCGrpContSchema.getBusinessType()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取行业编码失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取行业编码失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getBusinessBigType()==null || this.mLCGrpContSchema.getBusinessBigType().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，行业性质不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，行业性质不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype='businesstype' and code='"+this.mLCGrpContSchema.getBusinessType()+"' and codealias='"+this.mLCGrpContSchema.getBusinessBigType()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取行业性质失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取行业性质失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getGrpNature()==null || this.mLCGrpContSchema.getGrpNature().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，企业类型编码不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，企业类型编码不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype = 'grpnature' and code='"+this.mLCGrpContSchema.getGrpNature()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取企业类型编码失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取企业类型编码失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getPeoples()==0){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，员工总人数不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，员工总人数不能为空!");
					return false;
				}
				if(this.mLCGrpAppntSchema.getOnWorkPeoples()==0){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，在职人数不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，在职人数不能为空!");
					return false;
				}
				if(!checkPeoples()){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，员工总人数应该等于在职人数与退休人数与其他人员人数的和!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，员工总人数应该等于在职人数与退休人数与其他人员人数的和!");
					return false;
				}
				if(this.mLCGrpContSchema.getPayMode().equals("4")){
					responseOME = buildResponseOME("01","05", "投保人属性为法人时，不支持银行转账缴费方式！");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为法人时，不支持银行转账缴费方式！");
					return false;
				}
				if((this.mLCGrpAppntSchema.getLegalPersonName1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonName1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDType1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDType1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDNo1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDNo1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDStart1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDStart1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDEnd1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDEnd1()))){
					
				}else{
					if(this.mLCGrpAppntSchema.getLegalPersonName1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonName1())){
						responseOME = buildResponseOME("01","05", "法人姓名不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "法人姓名不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getLegalPersonIDType1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDType1())){
						responseOME = buildResponseOME("01","05", "法人证件类型不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "法人证件类型不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getLegalPersonIDNo1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDNo1())){
						responseOME = buildResponseOME("01","05", "法人证件号码不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "法人证件号码不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getLegalPersonIDStart1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDStart1())){
						responseOME = buildResponseOME("01","05", "法人证件生效日期不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "法人证件生效日期不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getLegalPersonIDEnd1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDEnd1())){
						responseOME = buildResponseOME("01","05", "法人证件失效日期不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "法人证件失效日期不能为空!");
						return false;
					}
					
				}
				if((this.mLCGrpAppntSchema.getShareholderName()==null||"".equals(this.mLCGrpAppntSchema.getShareholderName()))&&
				(this.mLCGrpAppntSchema.getShareholderIDType()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDType()))&&
				(this.mLCGrpAppntSchema.getShareholderIDNo()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDNo()))&&
				(this.mLCGrpAppntSchema.getShareholderIDStart()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDStart()))&&
				(this.mLCGrpAppntSchema.getShareholderIDEnd()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDEnd()))){
					
				}else{
					if(this.mLCGrpAppntSchema.getShareholderName()==null||"".equals(this.mLCGrpAppntSchema.getShareholderName())){
						responseOME = buildResponseOME("01","05", "控股股东/实际控制人姓名不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "控股股东/实际控制人姓名不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getShareholderIDType()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDType())){
						responseOME = buildResponseOME("01","05", "控股股东/实际控制人证件类型不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "控股股东/实际控制人证件类型不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getShareholderIDNo()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDNo())){
						responseOME = buildResponseOME("01","05", "控股股东/实际控制人证件号码不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "控股股东/实际控制人证件号码不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getShareholderIDStart()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDNo())){
						responseOME = buildResponseOME("01","05", "控股股东/实际控制人证件生效日期不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "控股股东/实际控制人证件生效日期不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getShareholderIDEnd()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDEnd())){
						responseOME = buildResponseOME("01","05", "控股股东/实际控制人证件失效日期不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "控股股东/实际控制人证件失效日期不能为空!");
						return false;
					}
					
				}
				if((this.mLCGrpAppntSchema.getResponsibleName()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleName()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDType()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDType()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDNo()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDNo()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDStart()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDStart()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDEnd()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDEnd()))){
					
				}else{
					if(this.mLCGrpAppntSchema.getResponsibleName()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleName())){
						responseOME = buildResponseOME("01","05", "负责人姓名不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "负责人姓名不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getResponsibleIDType()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDType())){
						responseOME = buildResponseOME("01","05", "负责人证件类型不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "负责人证件类型不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getResponsibleIDNo()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDNo())){
						responseOME = buildResponseOME("01","05", "负责人证件号码不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "负责人证件号码不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getResponsibleIDStart()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDStart())){
						responseOME = buildResponseOME("01","05", "负责人证件生效日期不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "负责人证件生效日期不能为空!");
						return false;
					}
					if(this.mLCGrpAppntSchema.getResponsibleIDEnd()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDEnd())){
						responseOME = buildResponseOME("01","05", "负责人证件失效日期不能为空!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "负责人证件失效日期不能为空!");
						return false;
					}
				}
			}else if(this.mLCGrpAppntSchema.getInsuredProperty().equals("2")){//自然人
				if(this.mLCGrpAddressSchema.getSex()==null || this.mLCGrpAddressSchema.getSex().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人性别不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人性别不能为空!");
					return false;
				}else{
					String sql="select 1 from ldcode where codetype = 'sex' and code='"+this.mLCGrpAddressSchema.getSex()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人性别失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人性别失败!");
						return false;
					}
				}
				if(this.mLCGrpAddressSchema.getNativePlace()==null || this.mLCGrpAddressSchema.getNativePlace().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人国籍不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人国籍不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldcode where codetype = 'nativeplace' and code='"+this.mLCGrpAddressSchema.getNativePlace()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人国籍失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人国籍失败!");
						return false;
					}
				}
				if(this.mLCGrpAddressSchema.getOccupationCode()==null || this.mLCGrpAddressSchema.getOccupationCode().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人职业代码不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人职业代码不能为空!");
					return false;
				}else{
					String sql = "select 1 from ldoccupation where occupationcode='"+this.mLCGrpAddressSchema.getOccupationCode()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人职业代码失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人职业代码失败!");
						return false;
					}
				}
				if(this.mLCGrpAddressSchema.getOccupationType()==null || this.mLCGrpAddressSchema.getOccupationType().equals("")){
					responseOME = buildResponseOME("01","05", "投保人属性为自然人时，联系人职业类别不能为空!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人时，联系人职业类别不能为空!");
					return false;
				}else{
					String sql = "select 1 from LDOccupation where occupationcode='"+this.mLCGrpAddressSchema.getOccupationCode()+"' and OccupationType='"+this.mLCGrpAddressSchema.getOccupationType()+"'";
					SSRS tSSRS = new ExeSQL().execSQL(sql);
					if(tSSRS==null ||tSSRS.getMaxRow()<=0){
						responseOME = buildResponseOME("01","05", "获取联系人职业类别失败!");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人职业类别失败!");
						return false;
					}
				}
				if(this.mLCGrpContSchema.getPayMode().equals("4")){
					if("".equals(this.mLCGrpContSchema.getAccName())||this.mLCGrpContSchema.getAccName()==null
							|| "".equals(this.mLCGrpContSchema.getBankCode())||this.mLCGrpContSchema.getBankCode()==null
							|| "".equals(this.mLCGrpContSchema.getBankAccNo())||this.mLCGrpContSchema.getBankAccNo()==null){
						responseOME = buildResponseOME("01","05", "投保人属性为自然人，且缴费方式为银行转账时，开户银行、账号任何一项不能为空！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人属性为自然人，且缴费方式为银行转账时，开户银行、账号任何一项不能为空！");
						return false;
					}else{
						String sql = "select 1 from ldbank where bankcode='" + this.mLCGrpContSchema.getBankCode() + "' and comcode like '"
								+ this.mLCGrpContSchema.getManageCom().substring(0, 4) + "%' ";
						String canBank = new ExeSQL().getOneValue(sql);
						if ("".equals(StrTool.cTrim(canBank))) {
							responseOME = buildResponseOME("01","05", "录入银行不存在!");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "录入银行不存在!");
							return false;
						}
					}
				}
				if((this.mLCGrpAppntSchema.getShareholderName()==null||"".equals(this.mLCGrpAppntSchema.getShareholderName()))&&
						(this.mLCGrpAppntSchema.getShareholderIDType()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDType()))&&
						(this.mLCGrpAppntSchema.getShareholderIDNo()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDNo()))&&
						(this.mLCGrpAppntSchema.getShareholderIDStart()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDStart()))&&
						(this.mLCGrpAppntSchema.getShareholderIDEnd()==null||"".equals(this.mLCGrpAppntSchema.getShareholderIDEnd()))){
					
				}else{
					responseOME = buildResponseOME("01","05", "投保人为自然人时，控股股东信息不允许录入!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人为自然人时，控股股东信息不允许录入!");
					return false;
				}
				if((this.mLCGrpAppntSchema.getLegalPersonName1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonName1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDType1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDType1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDNo1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDNo1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDStart1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDStart1()))&&
						(this.mLCGrpAppntSchema.getLegalPersonIDEnd1()==null||"".equals(this.mLCGrpAppntSchema.getLegalPersonIDEnd1()))){
					
				}else{
					responseOME = buildResponseOME("01","05", "投保人为自然人时，法人信息不允许录入!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人为自然人时，法人信息不允许录入!");
					return false;
				}
				if((this.mLCGrpAppntSchema.getResponsibleName()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleName()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDType()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDType()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDNo()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDNo()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDStart()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDStart()))&&
						(this.mLCGrpAppntSchema.getResponsibleIDEnd()==null||"".equals(this.mLCGrpAppntSchema.getResponsibleIDEnd()))){
					
				}else{
					responseOME = buildResponseOME("01","05", "投保人为自然人时，负责人信息不允许录入!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "投保人为自然人时，负责人信息不允许录入!");
					return false;
				}
			}else{
				responseOME = buildResponseOME("01","05", "获取投保人属性失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取投保人属性失败!");
				return false;
			}
		}
		
		if("".equals(this.mLCGrpAddressSchema.getPostalProvince()) || this.mLCGrpAddressSchema.getPostalProvince() == null
				|| "".equals(this.mLCGrpAddressSchema.getPostalCity()) || this.mLCGrpAddressSchema.getPostalCity() == null
				|| "".equals(this.mLCGrpAddressSchema.getPostalCounty()) || this.mLCGrpAddressSchema.getPostalCounty() == null
				|| "".equals(this.mLCGrpAddressSchema.getDetailAddress()) || this.mLCGrpAddressSchema.getDetailAddress() == null){
			responseOME = buildResponseOME("01","05", "获取团体基本资料中的省，市，县和详细地址失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体基本资料中的省，市，县和详细地址失败!");
			return false;
		}else{
			//校验省和市是否与匹配
			String provinceID = this.mLCGrpAddressSchema.getPostalProvince();
			String cityID = this.mLCGrpAddressSchema.getPostalCity();
			String countyID = this.mLCGrpAddressSchema.getPostalCounty();
			String strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
			String strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
			String strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
			String strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
			SSRS arrResult = new ExeSQL().execSQL(strSql);
			SSRS arrResult2 = new ExeSQL().execSQL(strSql2);
			SSRS arrResult3 = new ExeSQL().execSQL(strSql3);
			SSRS arrResult4 = new ExeSQL().execSQL(strSql4);
			
			if ("710000".equals(provinceID) || "810000".equals(provinceID) || "820000".equals(provinceID)||"990000".equals(provinceID)) {
				if(!"000000".equals(cityID)){
					responseOME = buildResponseOME("01","05", "省和市不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "省和市不匹配!");
					return false;
				}
				if(!"000000".equals(countyID)){
					responseOME = buildResponseOME("01","05", "市和县不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "市和县不匹配!");
					return false;
				}
			}else if (("620000".equals(provinceID)&&"620200".equals(cityID)) || ("440000".equals(provinceID)&&("442000".equals(cityID)||"441900".equals(cityID))) ) {
				if(!"000000".equals(countyID)){
					responseOME = buildResponseOME("01","05", "市和县不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "市和县不匹配!");
					return false;
				}
			}else{
				if(arrResult!=null && arrResult.getMaxRow()>0 &&
						arrResult2!=null && arrResult2.getMaxRow()>0 &&
						arrResult3!=null && arrResult3.getMaxRow()>0 &&
						arrResult4!=null && arrResult4.getMaxRow()>0){
		 		if (!arrResult.GetText(1, 1).equals(arrResult2.GetText(1, 1))) {
		 			responseOME = buildResponseOME("01","05", "省和市不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "省和市不匹配!");
					return false;
		 		}
		 		if (!arrResult3.GetText(1, 1).equals(arrResult4.GetText(1, 1))) {
		 			responseOME = buildResponseOME("01","05", "市和县不匹配!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "市和县不匹配!");
					return false;
		 		}}else{
		 			responseOME = buildResponseOME("01","05", "获取省、市、县编码失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取省、市、县编码失败!");
					return false;
		 		}
			}
			
			String sql1 = "select CodeName from LDCode1 where codetype = 'province1' and code = '"+mLCGrpAddressSchema.getPostalProvince()+"'";
			String sql2 = "select CodeName from LDCode1 where codetype = 'city1' and Code1 = '"+mLCGrpAddressSchema.getPostalProvince()+"' and code ='"+mLCGrpAddressSchema.getPostalCity()+"' ";
			String sql3 = "select CodeName from LDCode1 where codetype = 'county1' and Code1 = '"+mLCGrpAddressSchema.getPostalCity()+"' and code='"+mLCGrpAddressSchema.getPostalCounty()+"'";
			String grpAddress = new ExeSQL().getOneValue(sql1) + new ExeSQL().getOneValue(sql2) + new ExeSQL().getOneValue(sql3) + this.mLCGrpAddressSchema.getDetailAddress();
			this.mLCGrpAppntSchema.setPostalAddress(grpAddress);
			this.mLCGrpAddressSchema.setGrpAddress(grpAddress);
		}

		if(this.mLCGrpAddressSchema.getGrpZipCode()==null || this.mLCGrpAddressSchema.getGrpZipCode().equals("")){
			responseOME = buildResponseOME("01","05", "获取邮政编码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取邮政编码失败!");
			return false;
		}
		
		if(this.mLCGrpAddressSchema.getLinkMan1()==null || this.mLCGrpAddressSchema.getLinkMan1().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人姓名失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人姓名失败!");
			return false;
		}
		if(this.mLCGrpAddressSchema.getPhone1()==null || this.mLCGrpAddressSchema.getPhone1().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人联系电话失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人联系电话失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getIDType()==null || this.mLCGrpAppntSchema.getIDType().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人证件类型失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件类型失败!");
			return false;
		}else{
			if(!this.mLCGrpAppntSchema.getIDType().equals("4") && !this.mLCGrpAppntSchema.getIDType().equals("0") && !this.mLCGrpAppntSchema.getIDType().equals("a") && !this.mLCGrpAppntSchema.getIDType().equals("b")){
				responseOME = buildResponseOME("01","05", "获取联系人证件类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件类型失败!");
				return false;
			}
//			//校验联系人姓名
//			String chkname = PubFun.checkValidateName(this.mLCGrpAppntSchema.getIDType(),this.mLCGrpAddressSchema.getLinkMan1());
//			if(chkname!=""){
//				responseOME = buildResponseOME("01","05", "联系人"+chkname);
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "联系人"+chkname);
//				return false;
//			}
		}
		if(this.mLCGrpAppntSchema.getIDNo()==null || this.mLCGrpAppntSchema.getIDNo().equals("")){
			responseOME = buildResponseOME("01","05", "获取联系人证件号码失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件号码失败!");
			return false;
		}
		if(!"4".equals(this.mLCGrpAppntSchema.getIDType())){
			if(this.mLCGrpAppntSchema.getIDStartDate()==null || this.mLCGrpAppntSchema.getIDStartDate().equals("") || !PubFun.checkDateForm(this.mLCGrpAppntSchema.getIDStartDate())){
				responseOME = buildResponseOME("01","05", "获取联系人证件生效日期失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件生效日期失败!");
				return false;
			}
			if(this.mLCGrpAppntSchema.getIDLongEffFlag()==null || this.mLCGrpAppntSchema.getIDLongEffFlag().equals("")){
				if(this.mLCGrpAppntSchema.getIDEndDate()==null || this.mLCGrpAppntSchema.getIDEndDate().equals("") || !PubFun.checkDateForm(this.mLCGrpAppntSchema.getIDEndDate())){
					responseOME = buildResponseOME("01","05", "获取联系人证件失效日期失败，当证件长期有效标志为空时，证件失效日期为必录项!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件失效日期失败，当证件长期有效标志为空时，证件失效日期为必录项!");
					return false;
				}
			}else{
				if(!"Y".equals(this.mLCGrpAppntSchema.getIDLongEffFlag())){
					responseOME = buildResponseOME("01","05", "获取联系人证件长期有效标志失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人证件长期有效标志失败!");
					return false;
				}
			}
		}
//		if(this.mLCGrpAddressSchema.getNativePlace()==null || this.mLCGrpAddressSchema.getNativePlace().equals("")){
//			responseOME = buildResponseOME("01","05", "联系人国籍不能为空!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "联系人国籍不能为空!");
//			return false;
//		}else{
//			String sql = "select 1 from ldcode where codetype = 'grpnativeplace' and code='"+this.mLCGrpAddressSchema.getNativePlace()+"'";
//			SSRS tSSRS = new ExeSQL().execSQL(sql);
//			if(tSSRS==null ||tSSRS.getMaxRow()<=0){
//				responseOME = buildResponseOME("01","05", "获取联系人国籍失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取联系人国籍失败!");
//				return false;
//			}
//		}
//		String strocc = chkOccupation(this.mLCGrpAddressSchema.getNativePlace(), "", "", this.mLCGrpAddressSchema.getNativeCity(), this.mLCGrpAppntSchema.getIDType(), this.mLCGrpAppntSchema.getIDNo(), "1");
//		if(!"".equals(strocc)){
//			responseOME = buildResponseOME("01","05", "联系人"+strocc);
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "联系人"+strocc);
//			return false;
//		}
		String strIdNo=PubFun.CheckIDNo(this.mLCGrpAppntSchema.getIDType(),this.mLCGrpAppntSchema.getIDNo(),"",this.mLCGrpAddressSchema.getSex());
 		if(strIdNo !=""){
 			responseOME = buildResponseOME("01","05", "联系人"+strIdNo);
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "联系人"+strIdNo);
 			return false;
 		}
		if(this.mLCGrpContSchema.getPeoples3()==0){
			responseOME = buildResponseOME("01","05", "获取被保险人数失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人数失败!");
			return false;
		}
		if(this.mLCGrpContSchema.getOnWorkPeoples()==0){
			responseOME = buildResponseOME("01","05", "获取在职人数失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取在职人数失败!");
			return false;
		}
		if(this.mLCGrpAppntSchema.getAuthorization()==null || this.mLCGrpAppntSchema.getAuthorization().equals("")){
			responseOME = buildResponseOME("01","05", "授权使用客户信息不能为空!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "授权使用客户信息不能为空!");
			return false;
		}else{
			if(!this.mLCGrpAppntSchema.getAuthorization().equals("1")){
				responseOME = buildResponseOME("01","05", "授权使用客户信息只能为“是”!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "授权使用客户信息只能为“是”!");
				return false;
			}
		}
		for(int i=1;i<=this.mLCGrpPolSet.size();i++){
			String riskcode = this.mLCGrpPolSet.get(i).getRiskCode();
			LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(riskcode);
            if (tLMRiskAppDB.getInfo() == false) {
            	responseOME = buildResponseOME("01","05", "没有查到险种信息!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "没有查到险种信息!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
            int k=0;
			for(int j=1;j<=this.mLCContPlanDutyParamSetDefault.size();j++){
				String risk = this.mLCContPlanDutyParamSetDefault.get(j).getRiskCode();
				String calrule = this.mLCContPlanDutyParamSetDefault.get(j).getCalFactorValue();
				if(risk.equals(riskcode)){
					k++;
					if(calrule==null || "".equals(calrule)){
						responseOME = buildResponseOME("01","05", "保费计算方式不能为空！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "保费计算方式不能为空！");
						return false;
					}else{
						if(!"0".equals(calrule)&&!"2".equals(calrule)&&!"3".equals(calrule)&&!"4".endsWith(calrule)){
							responseOME = buildResponseOME("01","05", "获取保费计算方式失败！");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保费计算方式失败！");
							return false;
						}
					}
				}
			}
			if(k!=1){
				responseOME = buildResponseOME("01","05", "获取险种失败！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取险种失败！");
				return false;
			}
		}
		for(int i=1;i<=this.mLCContPlanSet.size();i++){
			if(mLCContPlanSet.get(i).getContPlanCode()==null || mLCContPlanSet.get(i).getContPlanCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取保障计划代码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划代码失败!");
				return false;
			}
			if(mLCContPlanSet.get(i).getContPlanName()==null || mLCContPlanSet.get(i).getContPlanName().equals("")){
				responseOME = buildResponseOME("01","05", "获取保障计划名称失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划名称失败!");
				return false;
			}
			if(mLCContPlanSet.get(i).getPeoples3()==0){
				responseOME = buildResponseOME("01","05", "获取可投保人数失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取可投保人数失败!");
				return false;
			}
			if(mLCContPlanSet.get(i).getPeoples2()==0){
				responseOME = buildResponseOME("01","05", "获取参保人数失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取参保人数失败!");
				return false;
			}
			
		}
		for(int i=1;i<=this.mLCContPlanRiskSet.size();i++){
			if(mLCContPlanRiskSet.get(i).getRiskCode()==null || mLCContPlanRiskSet.get(i).getRiskCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取险种编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取险种编码失败!");
				return false;
			}
			if(mLCContPlanRiskSet.get(i).getMainRiskCode()==null || mLCContPlanRiskSet.get(i).getMainRiskCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取主险编码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取主险编码失败!");
				return false;
			}
		}
		if(check162601()==1){
			for(int i=1;i<=this.mLCContPlanDutyParamSet.size();i++){
				if(mLCContPlanDutyParamSet.get(i).getContPlanCode()==null || mLCContPlanDutyParamSet.get(i).getContPlanCode().equals("")){
					responseOME = buildResponseOME("01","05", "获取保障计划代码失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划代码失败!");
					return false;
				}
				if(mLCContPlanDutyParamSet.get(i).getContPlanName()==null || mLCContPlanDutyParamSet.get(i).getContPlanName().equals("")){
					responseOME = buildResponseOME("01","05", "获取保障计划名称失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取保障计划名称失败!");
					return false;
				}
				if(mLCContPlanDutyParamSet.get(i).getRiskCode()==null || mLCContPlanDutyParamSet.get(i).getRiskCode().equals("")){
					responseOME = buildResponseOME("01","05", "获取险种编码失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取险种编码失败!");
					return false;
				}
				if(mLCContPlanDutyParamSet.get(i).getMainRiskCode()==null || mLCContPlanDutyParamSet.get(i).getMainRiskCode().equals("")){
					responseOME = buildResponseOME("01","05", "获取主险编码失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取主险编码失败!");
					return false;
				}
				if(mLCContPlanDutyParamSet.get(i).getDutyCode()==null || mLCContPlanDutyParamSet.get(i).getDutyCode().equals("")){
					responseOME = buildResponseOME("01","05", "获取责任编码失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取责任编码失败!");
					return false;
				}
				if(mLCContPlanDutyParamSet.get(i).getCalFactor()==null || mLCContPlanDutyParamSet.get(i).getCalFactor().equals("")){
					responseOME = buildResponseOME("01","05", "获取计划要素名称失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取计划要素名称失败!");
					return false;
				}
				if(mLCContPlanDutyParamSet.get(i).getCalFactorType()==null || mLCContPlanDutyParamSet.get(i).getCalFactorType().equals("")){
					responseOME = buildResponseOME("01","05", "获取计划要素类型失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取计划要素类型失败!");
					return false;
				}
				
			}
		}
		for(int i=1;i<=this.mLCInsuredListSet.size();i++){
			if(mLCInsuredListSet.get(i).getInsuredID()==null || mLCInsuredListSet.get(i).getInsuredID().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保人序号失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人序号失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getContNo()==null || mLCInsuredListSet.get(i).getContNo().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保人合同序号失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人合同序号失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getRetire()==null || mLCInsuredListSet.get(i).getRetire().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保人状态失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人状态失败!");
				return false;
			}else{
				if(!mLCInsuredListSet.get(i).getRetire().equals("1") && !mLCInsuredListSet.get(i).equals("2")){
					responseOME = buildResponseOME("01","05", "获取被保人状态失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保人状态失败!");
					return false;
				}
			}
			if(mLCInsuredListSet.get(i).getRelation()==null || mLCInsuredListSet.get(i).getRelation().equals("")){
				responseOME = buildResponseOME("01","05", "获取员工关系失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取员工关系失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getEmployeeName()==null || mLCInsuredListSet.get(i).getEmployeeName().equals("")){
				responseOME = buildResponseOME("01","05", "获取主被保险人姓名失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取主被保险人姓名失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getInsuredName()==null || mLCInsuredListSet.get(i).getInsuredName().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人姓名失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人姓名失败!");
				return false;
			}
//			if(mLCInsuredListSet.get(i).getNativePlace()==null || mLCInsuredListSet.get(i).getNativePlace().equals("")){
//				responseOME = buildResponseOME("01","05", "获取被保险人国籍失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人国籍失败!");
//				return false;
//			}
			if(mLCInsuredListSet.get(i).getSex()==null || mLCInsuredListSet.get(i).getSex().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人性别失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人性别失败!");
				return false;
			}else{
				String sql="select 1 from ldcode where codetype = 'sex' and code='"+mLCInsuredListSet.get(i).getSex()+"'";
				SSRS tSSRS = new ExeSQL().execSQL(sql);
				if(tSSRS==null ||tSSRS.getMaxRow()<=0){
					responseOME = buildResponseOME("01","05", "获取被保险人性别失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人性别失败!");
					return false;
				}
			}
			if(mLCInsuredListSet.get(i).getBirthday()==null || mLCInsuredListSet.get(i).getBirthday().equals("") || !PubFun.checkDateForm(this.mLCInsuredListSet.get(i).getBirthday())){
				responseOME = buildResponseOME("01","05", "获取被保险人出生日期失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人出生日期失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getIDType()==null || mLCInsuredListSet.get(i).getIDType().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人证件类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人证件类型失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getIDNo()==null || mLCInsuredListSet.get(i).getIDNo().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人证件号码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人证件号码失败!");
				return false;
			}
			if(mLCInsuredListSet.get(i).getContPlanCode()==null || mLCInsuredListSet.get(i).getContPlanCode().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人保障计划代码失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人保障计划代码失败!");
				return false;
			}
//			if(mLCInsuredListSet.get(i).getPosition2()==null || mLCInsuredListSet.get(i).getPosition2().equals("")){
//				responseOME = buildResponseOME("01","05", "获取被保险人岗位失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人岗位失败!");
//				return false;
//			}
//			if(mLCInsuredListSet.get(i).getOccupationCode()==null || mLCInsuredListSet.get(i).getOccupationCode().equals("")){
//				responseOME = buildResponseOME("01","05", "获取被保险人职业代码失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人职业代码失败!");
//				return false;
//			}
			if(mLCInsuredListSet.get(i).getOccupationType()==null || mLCInsuredListSet.get(i).getOccupationType().equals("")){
				responseOME = buildResponseOME("01","05", "获取被保险人职业类别失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取被保险人职业类别失败!");
				return false;
			}
		}
		for(int i=1;i<=this.mLCContPlanDutyParamSet2.size();i++){
			if(mLCContPlanDutyParamSet2.get(i).getInsuAccNo()==null || mLCContPlanDutyParamSet2.get(i).getInsuAccNo().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户类型失败!");
				return false;
			}else{
				if(!mLCContPlanDutyParamSet2.get(i).getInsuAccNo().equals("1000001") && !mLCContPlanDutyParamSet2.get(i).getInsuAccNo().equals("1000002")){
					responseOME = buildResponseOME("01","05", "获取团体账户类型失败!");
					System.out.println("responseOME:" + responseOME);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户类型失败!");
					return false;
				}
			}
			
			if(mLCContPlanDutyParamSet2.get(i).getCalFactor()==null || mLCContPlanDutyParamSet2.get(i).getCalFactor().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户计划要素名称失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户计划要素名称失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet2.get(i).getCalFactorType()==null || mLCContPlanDutyParamSet2.get(i).getCalFactorType().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户计划要素类型失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户计划要素类型失败!");
				return false;
			}
			if(mLCContPlanDutyParamSet2.get(i).getCalFactorValue()==null || mLCContPlanDutyParamSet2.get(i).getCalFactorValue().equals("")){
				responseOME = buildResponseOME("01","05", "获取团体账户计划要素值失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取团体账户计划要素值失败!");
				return false;
			}
		}
		if(check690101()){
			if(mLCGrpFeeSet.size()!=2){
				responseOME = buildResponseOME("01","05", "获取管理费信息失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取管理费信息失败!");
				return false;
			}
		}

//		if(mLJTempFeeClassSchema.getPayMode()==null || mLJTempFeeClassSchema.getPayMode().equals("")){
//			responseOME = buildResponseOME("01","05", "获取缴费方式失败!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取缴费方式失败!");
//			return false;
//		}else{
//			if(!mLCGrpContSchema.getPayMode().equals(mLJTempFeeClassSchema.getPayMode())){
//				responseOME = buildResponseOME("01","05", "获取缴费方式失败!");
//				System.out.println("responseOME:" + responseOME);
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取缴费方式失败!");
//				return false;
//			}
//		}
//		if(mLJTempFeeClassSchema.getEnterAccDate()==null || mLJTempFeeClassSchema.getEnterAccDate().equals("")){
//			responseOME = buildResponseOME("01","05", "获取到账时间失败!");
//			System.out.println("responseOME:" + responseOME);
//			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取到账时间失败!");
//			return false;
//		}
//		PayMoney = Double.parseDouble(payMoney);
		
		
		
		// 校验被保人身份证号 
		if(this.mLCInsuredListSet!=null&& mLCInsuredListSet.size() != 0){
			LCInsuredListSet tmLCInsuredListSet=this.mLCInsuredListSet;
			for(int i=1;i<=tmLCInsuredListSet.size();i++){
				String name=tmLCInsuredListSet.get(i).getInsuredName();
				String idtype=tmLCInsuredListSet.get(i).getIDType();
				String idno=tmLCInsuredListSet.get(i).getIDNo();
				String sex=tmLCInsuredListSet.get(i).getSex();
				String birthday=tmLCInsuredListSet.get(i).getBirthday();
				String nativeplace = tmLCInsuredListSet.get(i).getNativePlace();
				String nativecity = tmLCInsuredListSet.get(i).getNativeCity();
				String occupationcode = tmLCInsuredListSet.get(i).getOccupationCode();
				String occupationtype = tmLCInsuredListSet.get(i).getOccupationType();
//				String insuredid=tmLCInsuredListSet.get(i).getInsuredID();
//				if(name!=null&&!"".equals(name)){
//             		String chkinsuredname = PubFun.checkValidateName(idtype,name);
//             		if(chkinsuredname!=null&&!"".equals(chkinsuredname)){
//             			mErrors.addOneError("导入被保人信息列表中,被保人号:"+insuredid+","+chkinsuredname);
//                 		return false;
//             		}
//             	}
//				String chk = chkOccupation(nativeplace,occupationcode,occupationtype,nativecity,idtype,idno,"2");
//				if(chk!=null && !chk.equals("")){
//					responseOME = buildResponseOME("01","07", "被保人:"+name+","+chk);
//					System.out.println("responseOME:" + responseOME);
//					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "被保人:"+name+","+chk);
//					return false;
//				}
				if(idno!=null && idno!=""){
					String strChkIdNo=PubFun.CheckIDNo(idtype, idno, birthday, sex);
					if(strChkIdNo !=""){
						responseOME = buildResponseOME("01","07", "被保人:"+name+","+strChkIdNo);
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "被保人:"+name+","+strChkIdNo);
						return false;
					}
				}
				if(!"1".equals(mLCInsuredListSet.get(i).getUnCommonChar())){
					//保单登记校验姓名 by BXF
			    	 String chk2 = PubFun.checkGrpName(idtype,name);
			     	if(!"".equals(chk2)&&chk2!=null){
			     		responseOME = buildResponseOME("01","04", "被保人："+name+chk2);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","04", "WX", "被保人："+name+chk2);
						System.out.println("responseOME:" + responseOME);
			     		return false;
			     	 }
						 }
			}
		}
		

		String tManageCom = this.mLCGrpContSchema.getManageCom();
		String tAgentCom = this.mLCGrpContSchema.getAgentCom();
		String tAgentCode = this.mLCGrpContSchema.getAgentCode();
		String tSaleChnl = this.mLCGrpContSchema.getSaleChnl();
		
		//校验业务员是否离职
        String tAgentState = new ExeSQL()
                    .getOneValue("SELECT agentstate FROM laagent where agentcode='"
                            + tAgentCode + "'");
        if ((tAgentState == null) || tAgentState.equals(""))
        {
            responseOME = buildResponseOME("01","07", "无法查到保单业务员[" + tAgentCode +"]的状态!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "无法查到保单业务员[" + tAgentCode +"]的状态!");
			return false;
        }
        if (Integer.parseInt(tAgentState) >= 6)
        {
            responseOME = buildResponseOME("01","07", "该保单业务员[" + tAgentCode + "]已经离职!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "该保单业务员[" + tAgentCode + "]已经离职!");
			return false;
        }
		
		String tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
		SSRS arrCode = new ExeSQL().execSQL(tSQLCode);
		if(arrCode.getMaxRow()<1){
			responseOME = buildResponseOME("01","07", "业务员与管理机构不匹配！");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员与管理机构不匹配！");
			return false;
		}
		if("02".equals(tSaleChnl) || "03".equals(tSaleChnl)){
			String agentCodeSql = " select 1 from LAAgent a where a.AgentCode = '"+ tAgentCode+ "'" 
		                 + " and a.BranchType = '2' and a.BranchType2 in ('01','02') "
		                 + " union all "
						 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
						 + " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
		    SSRS arrAgentCode = new ExeSQL().execSQL(agentCodeSql);
		    if(arrAgentCode.getMaxRow()<1){
		    	responseOME = buildResponseOME("01","07", "业务员和销售渠道不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员和销售渠道不匹配！");
				return false;
		    }
		}else{
			String agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
		                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
	                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
	                     + " union all "
						 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
						 + " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
		    SSRS arrAgentCode = new ExeSQL().execSQL(agentCodeSql);
		    if(arrAgentCode.getMaxRow()<1){
		    	responseOME = buildResponseOME("01","07", "业务员和销售渠道不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员和销售渠道不匹配！");
				return false;
		    }
		}
		if("03".equals(tSaleChnl) || "04".equals(tSaleChnl) || "10".equals(tSaleChnl) || "15".equals(tSaleChnl) || "20".equals(tSaleChnl) || "23".equals(tSaleChnl) ) {
			String tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
			SSRS arrCom = new ExeSQL().execSQL(tSQLCom);
			if(arrCom.getMaxRow()<1){
				responseOME = buildResponseOME("01","07", "中介机构与管理机构不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "中介机构与管理机构不匹配！");
				return false;
			}
			String tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
			SSRS arrComCode = new ExeSQL().execSQL(tSQLComCode);
			if(arrComCode.getMaxRow()<1){
				responseOME = buildResponseOME("01","07", "业务员与中介机构不匹配！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "业务员与中介机构不匹配！");
				return false;
			}
		}
		
		String tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+this.mLCGrpContSchema.getMarketType()+"' and code1 = '"+this.mLCGrpContSchema.getSaleChnl()+"' ";
		SSRS arrCheck = new ExeSQL().execSQL(tcheckSQL);
		if(arrCheck.getMaxRow()<1){
			responseOME = buildResponseOME("01","07", "市场类型和销售渠道不匹配！");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "市场类型和销售渠道不匹配！");
			return false;
		}
		
		if(check690101()){
			if(!"pg".equals(this.mLCGrpContSchema.getCardFlag())){
				return true;
			}
			if(this.mLCGrpPolSet.size()!=2){
				responseOME = buildResponseOME("01","07", "险种须包含690101和690188，请核实！");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "险种须包含690101和690188，请核实！");
				return false;
			}else{
				String riskcode1 = this.mLCGrpPolSet.get(1).getRiskCode();
				String riskcode2 = this.mLCGrpPolSet.get(2).getRiskCode();
				if(riskcode1!=null && !"".equals(riskcode1)
						&& riskcode2!=null && !"".equals(riskcode2)){
					if(riskcode1.equals(riskcode2)){
						responseOME = buildResponseOME("01","07", "险种须包含690101和690188，请核实！");
						System.out.println("responseOME:" + responseOME);
						LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "险种须包含690101和690188，请核实！");
						return false;
					}else{
						if((!"690101".equals(riskcode1)&&!"690101".equals(riskcode2))||
								!"690188".equals(riskcode1)&&!"690188".equals(riskcode2)){
							responseOME = buildResponseOME("01","07", "险种须包含690101和690188，请核实！");
							System.out.println("responseOME:" + responseOME);
							LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","07", "WX", "险种须包含690101和690188，请核实！");
							return false;
						}
					}
				}
			}
		}
		
		return true;

	}
	
	//提交信息
	private boolean save(MMap mMMap){
		try{
			VData v = new VData();
			v.add(mMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				System.out.println("数据提交失败************************");
				responseOME = buildResponseOME("01","99", "数据提交失败!");
				System.out.println("responseOME:" + responseOME);
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","99", "WX", "数据提交失败!");
				delete();
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01","99", "数据提交失败!");
			System.out.println("数据提交异常");
			e.printStackTrace();
			delete();
			return false;
		}
		
		return true;
	}
	private boolean delete(){
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		LCGrpContSet tLCGrpContSet = new LCGrpContSet();
		tLCGrpContDB.setGrpContNo(mProposalGrpContNo);
		tLCGrpContSet = tLCGrpContDB.query();
		LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
		VData tVData = new VData();
		tVData.add( tLCGrpContSchema );
		tVData.add( tG );
		// 数据传输
		GroupContDeleteUI tGroupContDeleteUI = new GroupContDeleteUI();
		tGroupContDeleteUI.submitData(tVData,"DELETE");
		return true;
	}
	
	private OMElement buildResponseOME(String state, String errCode,String errInfo) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		
		OMElement DATAEND = fac.createOMElement("queryDataResponse", null);
//		OMNamespace ns = new OMNamespaceImpl("http://services.ecwebservice.com", "ns2");
//		DATAEND.setNamespace(ns);
		OMElement DATASET = fac.createOMElement("CDGRPCONTDATA", null);
		LoginVerifyTool tool = new LoginVerifyTool();
		tool.addOm(DATASET, "BATCHNO", batchno);
		tool.addOm(DATASET, "SENDDATE", sendDate);
		tool.addOm(DATASET, "SENDTIME", sendTime);
		tool.addOm(DATASET, "PrtNo", mLCGrpContSchema.getPrtNo());
		if("00".equals(state)){
			String sql1 = "select grpcontno from lcgrpcont where prtno='"+mLCGrpContSchema.getPrtNo()+"'";
			String sqlRes1 = new ExeSQL().getOneValue(sql1);
			tool.addOm(DATASET, "GrpContNo", sqlRes1);
			tool.addOm(DATASET, "CustomerNo", this.mLDGrpSchema.getCustomerNo());
		}
		tool.addOm(DATASET, "STATE", state);
		tool.addOm(DATASET, "ERRINFO", errInfo);
			
//		if("00".equals(state)){
//			
////			addOm(GRPCONTITEM, "PrtNo", "18180716090");
//			OMElement CONTINFO = fac.createOMElement("ContInfo", null);
//			String sql2 = "select contno,insuredno,insuredname,insuredidtype,insuredidno from lccont where prtno='"+mLCGrpContSchema.getPrtNo()+"'";
//			SSRS tSSRS = new ExeSQL().execSQL(sql2);
//			for(int i=1;i<=tSSRS.getMaxRow();i++){
//				OMElement GRPCONTITEM = fac.createOMElement("ITEM", null);
//				tool.addOm(GRPCONTITEM, "ContNo", tSSRS.GetText(i, 1));
//				tool.addOm(GRPCONTITEM, "InsuredNo", tSSRS.GetText(i, 2));
//				tool.addOm(GRPCONTITEM, "Name", tSSRS.GetText(i, 3));
//				tool.addOm(GRPCONTITEM, "IDType", tSSRS.GetText(i, 4));
//				tool.addOm(GRPCONTITEM, "IDNo", tSSRS.GetText(i, 5));
//				CONTINFO.addChild(GRPCONTITEM);
//			}
//			DATASET.addChild(CONTINFO);
//		}
		DATAEND.addChild(DATASET);
		return DATAEND;
	}
	
	private boolean creatGrpCont(){
		
        mLCGrpContSchema.setProposalGrpContNo(mProposalGrpContNo);
        mLCGrpContSchema.setGrpContNo(mProposalGrpContNo);
        mLCGrpAppntSchema.setGrpContNo(mProposalGrpContNo);
        if (mLCGrpContSchema.getAddressNo() == null || mLCGrpContSchema.getAddressNo().trim().equals(""))
        {
            try
            {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(integer(AddressNo)) Is Null Then 0 Else max(integer(AddressNo)) End from LCGrpAddress where CustomerNo='"
                        + mLCGrpAppntSchema.getCustomerNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                String AddressNo = integer.toString();
                System.out.println("得到的地址码是：" + AddressNo);
                if (!"".equals(AddressNo))
                {
                    mLCGrpContSchema.setAddressNo(AddressNo);
                    mLCGrpAppntSchema.setAddressNo(AddressNo);
                    mLCGrpAddressSchema.setAddressNo(AddressNo);
                }
                else
                {
                	responseOME = buildResponseOME("01","13", "客户地址号码生成失败!");
    				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "客户地址号码生成失败!");
    				System.out.println("responseOME:" + responseOME);
    				return false;
                }
            }
            catch (Exception e)
            {
            	responseOME = buildResponseOME("01","13", "地址码超长,生成号码失败,请先删除原来的超长地址码!!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "地址码超长,生成号码失败,请先删除原来的超长地址码!!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
        }
        if (mLCGrpContSchema.getAppFlag() == null
                || mLCGrpContSchema.getAppFlag().equals(""))
        {
            mLCGrpContSchema.setAppFlag("0");
            mLCGrpContSchema.setStateFlag("0");
        }
        if (StrTool.cTrim(mLCGrpContSchema.getPolApplyDate()).equals(""))
        {
            mLCGrpContSchema.setPolApplyDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputDate()).equals(""))
        {
            mLCGrpContSchema.setInputDate(theCurrentDate);
        }
        if (StrTool.cTrim(mLCGrpContSchema.getInputTime()).equals(""))
        {
            mLCGrpContSchema.setInputTime(theCurrentTime);
        }
        
        String tSQL = "select agentgroup from laagent where agentcode = '"+mLCGrpContSchema.getAgentCode()+"' ";
        String tAgentGroup = new ExeSQL().getOneValue(tSQL);
        
        if(tAgentGroup==null || tAgentGroup.equals("")){
        	responseOME = buildResponseOME("01","13", "获取业务员编码失败!");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","13", "WX", "获取业务员编码失败!");
			System.out.println("responseOME:" + responseOME);
			return false;
        }
        
        if(check690101()){
        	mLCGrpContSchema.setContPrintType("2");
        }else{
        	mLCGrpContSchema.setContPrintType("0");
        }
        mLCGrpContSchema.setPrintCount(0);
        mLCGrpContSchema.setAgentGroup(tAgentGroup);
        mLCGrpContSchema.setApproveFlag("0");
        mLCGrpContSchema.setUWFlag("0");
        mLCGrpContSchema.setSpecFlag("0");
        mLCGrpContSchema.setSaleChnlDetail("01");
        //记录入机日期
        mLCGrpContSchema.setMakeDate(theCurrentDate);
        mLCGrpAppntSchema.setMakeDate(theCurrentDate);
        //记录入机时间
        mLCGrpContSchema.setMakeTime(theCurrentTime);
        mLCGrpAppntSchema.setMakeTime(theCurrentTime);

        mLDGrpSchema.setMakeDate(theCurrentDate);
        mLDGrpSchema.setMakeTime(theCurrentTime);
        
        //记录入机时间
        mLCGrpAddressSchema.setMakeTime(theCurrentTime);
        mLCGrpAddressSchema.setMakeDate(theCurrentDate);
        
//      记录当前操作员
        mLCGrpContSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAppntSchema.setOperator(mGlobalInput.Operator);
        mLCGrpAddressSchema.setOperator(mGlobalInput.Operator);
        mLDGrpSchema.setOperator(mGlobalInput.Operator);
        //记录最后一次修改日期
        mLCGrpContSchema.setModifyDate(theCurrentDate);
        mLCGrpAppntSchema.setModifyDate(theCurrentDate);
        mLCGrpAddressSchema.setModifyDate(theCurrentDate);
        mLDGrpSchema.setModifyDate(theCurrentDate);
        //记录最后一次修改时间
        mLCGrpContSchema.setModifyTime(theCurrentTime);
        mLCGrpAppntSchema.setModifyTime(theCurrentTime);
        mLCGrpAddressSchema.setModifyTime(theCurrentTime);
        mLDGrpSchema.setModifyTime(theCurrentTime);
        
//        /** 如果页面没有传入行业性质,就从数据库里查询出来添上 */
//        if ("1".equals(mLCGrpAppntSchema.getInsuredProperty()) && StrTool.cTrim(mLCGrpContSchema.getBusinessBigType()).equals(""))
//        {
//            String sql = "select CodeAlias from ldcode where codetype='businesstype'"
//                    + " and code='" + mLCGrpContSchema.getBusinessType() + "'";
//            String bigType = (new ExeSQL()).getOneValue(sql);
//            if (StrTool.cTrim(bigType).equals("")
//                    || StrTool.cTrim(bigType).equals("null"))
//            {
//            	responseOME = buildResponseOME("01","12", "获取行业性质失败!");
//				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","12", "WX", "获取行业性质失败!");
//				System.out.println("responseOME:" + responseOME);
//				return false;
//            }
//            mLCGrpContSchema.setBusinessBigType(bigType);
//        }
//        tMMap.put(mLDGrpSchema, "INSERT");
        MMap mMMap = new MMap();
        if(customerflag){
        	mMMap.put(mLDGrpSchema, "INSERT");
        }
        mMMap.put(mLCGrpAddressSchema, "INSERT");
        mMMap.put(mLCGrpContSchema, "INSERT");
        mMMap.put(mLCGrpAppntSchema, "INSERT");
        boolean b =save(mMMap);
		return b;
	}
	
	private boolean creatGrpPol(){
		//校验集团交叉销售相互代理业务的销售渠道与中介机构是否符合描述
		if(this.mLCGrpContSchema.getCrs_SaleChnl() != null && !"".equals(this.mLCGrpContSchema.getCrs_SaleChnl()))
		{
			String strSQL = "select 1 from lcgrpcont lgc where 1 = 1"  
				+ " and lgc.Crs_SaleChnl is not null " 
				+ " and lgc.Crs_BussType = '01' " 
				+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
				+ " and lgc.PrtNo = '" + this.mLCGrpContSchema.getPrtNo() + "' ";
			SSRS arrResult = new ExeSQL().execSQL(strSQL);
	        if (arrResult.MaxRow>0)
	        {
	        	responseOME = buildResponseOME("01","14", "选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","14", "WX", "选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
				System.out.println("responseOME:" + responseOME);
	        	return false;
	        }
		} 
		LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
		String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        for(int i=1;i<=mLCGrpPolSet.size();i++){
        	String tRiskCode = mLCGrpPolSet.get(i).getRiskCode();
        	LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(tRiskCode);
            if (tLMRiskAppDB.getInfo() == false) {
            	responseOME = buildResponseOME("01","14", "没有查到险种信息!");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","14", "WX", "没有查到险种信息!");
				System.out.println("responseOME:" + responseOME);
				return false;
            }
            String tNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
        	mLCGrpPolSet.get(i).setGrpPolNo(tNo); //如果是新增
        	mLCGrpPolSet.get(i).setGrpProposalNo(tNo);
        	mLCGrpPolSet.get(i).setPrtNo(mLCGrpContSchema.getPrtNo());
        	mLCGrpPolSet.get(i).setGrpContNo(mLCGrpContSchema.getGrpContNo());
        	mLCGrpPolSet.get(i).setSaleChnl(mLCGrpContSchema.getSaleChnl());
        	mLCGrpPolSet.get(i).setManageCom(mLCGrpContSchema.getManageCom());
        	mLCGrpPolSet.get(i).setAgentCom(mLCGrpContSchema.getAgentCom());
        	mLCGrpPolSet.get(i).setAgentType(mLCGrpContSchema.getAgentType());
        	mLCGrpPolSet.get(i).setAgentCode(mLCGrpContSchema.getAgentCode());
        	mLCGrpPolSet.get(i).setAgentGroup(mLCGrpContSchema.getAgentGroup());
        	mLCGrpPolSet.get(i).setCustomerNo(mLCGrpContSchema.getAppntNo());
        	mLCGrpPolSet.get(i).setAddressNo(mLCGrpContSchema.getAddressNo());
        	mLCGrpPolSet.get(i).setCValiDate(mLCGrpContSchema.getCValiDate());
        	mLCGrpPolSet.get(i).setPayIntv(mLCGrpContSchema.getPayIntv());
        	mLCGrpPolSet.get(i).setPayMode(mLCGrpContSchema.getPayMode());
        	mLCGrpPolSet.get(i).setExpPeoples(mLCGrpContSchema.getExpPeoples());
        	mLCGrpPolSet.get(i).setGrpName(mLCGrpContSchema.getGrpName());
        	mLCGrpPolSet.get(i).setComFeeRate(tLMRiskAppDB.getAppInterest());
        	mLCGrpPolSet.get(i).setBranchFeeRate(tLMRiskAppDB.getAppPremRate());
        	mLCGrpPolSet.get(i).setAppFlag("0");
        	mLCGrpPolSet.get(i).setStateFlag("0");
        	mLCGrpPolSet.get(i).setUWFlag("0");
        	mLCGrpPolSet.get(i).setApproveFlag("0");
        	mLCGrpPolSet.get(i).setModifyDate(theCurrentDate);
        	mLCGrpPolSet.get(i).setModifyTime(theCurrentTime);
        	mLCGrpPolSet.get(i).setOperator(mGlobalInput.Operator);
        	mLCGrpPolSet.get(i).setMakeDate(theCurrentDate);
        	mLCGrpPolSet.get(i).setMakeTime(theCurrentTime);
        	mLCGrpPolSet.get(i).setPayMode(mLCGrpContSchema.getPayMode());
        	mLCGrpPolSet.get(i).setRiskWrapFlag("N");
        	mLCGrpPolSet.get(i).setSaleChnlDetail(mLCGrpContSchema.getSaleChnlDetail());
            
        	LMRiskDB tLMRiskDB = new LMRiskDB();
            tLMRiskDB.setRiskCode(mLCGrpPolSet.get(i).getRiskCode());
            if (tLMRiskDB.getInfo() == false) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GroupRiskBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有查到险种信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
        	LCContPlanRiskSchema tLCContPlanRiskSchema = new
                    LCContPlanRiskSchema();
            tLCContPlanRiskSchema.setGrpContNo(mLCGrpContSchema.
                    getGrpContNo());
            tLCContPlanRiskSchema.setProposalGrpContNo(mLCGrpContSchema.
                    getGrpContNo());
            tLCContPlanRiskSchema.setMainRiskCode(tLMRiskDB.getRiskCode());
            tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskDB.getRiskVer());
            tLCContPlanRiskSchema.setRiskCode(tLMRiskDB.getRiskCode());
            tLCContPlanRiskSchema.setRiskVersion(tLMRiskDB.getRiskVer());
            tLCContPlanRiskSchema.setContPlanCode("11");
            tLCContPlanRiskSchema.setContPlanName("默认计划");
            tLCContPlanRiskSchema.setPlanType("0");
            tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
            tLCContPlanRiskSchema.setMakeDate(theCurrentDate);
            tLCContPlanRiskSchema.setMakeTime(theCurrentTime);
            tLCContPlanRiskSchema.setModifyDate(theCurrentDate);
            tLCContPlanRiskSchema.setModifyTime(theCurrentTime);
            tLCContPlanRiskSet.add(tLCContPlanRiskSchema);
        }
        
        MMap mMMap = new MMap();
        mMMap.put(mLCGrpPolSet, "INSERT");
        mMMap.put(tLCContPlanRiskSet, "INSERT");
        boolean b = save(mMMap);
		return b;
	}
	
	private boolean creatContPlan(){
		
		String tContPlanCode = "";
		HashMap tContPlanCodeMap = new HashMap();
		LCContPlanSet tLCContPlanSet = new LCContPlanSet();
        for(int i=1;i<=mLCContPlanSet.size();i++){
        	for(int j=i+1;j<=mLCContPlanSet.size();j++){
        		if(mLCContPlanSet.get(i).getContPlanCode().equals(mLCContPlanSet.get(j).getContPlanCode())){
        			if(mLCContPlanSet.get(i).getPeoples2() != mLCContPlanSet.get(j).getPeoples2() || mLCContPlanSet.get(i).getPeoples3() != mLCContPlanSet.get(j).getPeoples3()){
        				responseOME = buildResponseOME("01","17", "相同保障计划中应保人数和实保人数不一致!");
        				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "相同保障计划中应保人数和实保人数不一致!");
        				System.out.println("responseOME:" + responseOME);
        				return false;
        			}
        		}
        	}
        	tContPlanCode = mLCContPlanSet.get(i).getContPlanCode();
        	if(!tContPlanCodeMap.containsKey(tContPlanCode)){
        		tContPlanCodeMap.put(tContPlanCode, "");
        		LCContPlanSchema tLCContPlanSchema = mLCContPlanSet.get(i);
        		tLCContPlanSchema.setGrpContNo(mProposalGrpContNo);
        		tLCContPlanSchema.setProposalGrpContNo(mProposalGrpContNo);
        		tLCContPlanSchema.setPlanType("0");
        		tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        		tLCContPlanSchema.setMakeDate(theCurrentDate);
        		tLCContPlanSchema.setMakeTime(theCurrentTime);
        		tLCContPlanSchema.setModifyDate(theCurrentDate);
        		tLCContPlanSchema.setModifyTime(theCurrentTime);
        		tLCContPlanSet.add(tLCContPlanSchema);
        	}
        }
       
        tMMap.put(tLCContPlanSet, "INSERT");
		
        String tRiskCode = "";
		HashMap tRiskMap = new HashMap();
		LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
        for(int i=1;i<=mLCContPlanRiskSet.size();i++){
        	mLCContPlanRiskSet.get(i).setGrpContNo(mProposalGrpContNo);
        	mLCContPlanRiskSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
        	mLCContPlanRiskSet.get(i).setMainRiskVersion("2002");
        	mLCContPlanRiskSet.get(i).setRiskVersion("2002");
        	mLCContPlanRiskSet.get(i).setPlanType("0");
        	mLCContPlanRiskSet.get(i).setOperator(mGlobalInput.Operator);
        	mLCContPlanRiskSet.get(i).setMakeDate(theCurrentDate);
        	mLCContPlanRiskSet.get(i).setMakeTime(theCurrentTime);
        	mLCContPlanRiskSet.get(i).setModifyDate(theCurrentDate);
        	mLCContPlanRiskSet.get(i).setModifyTime(theCurrentTime);
        	mLCContPlanRiskSet.get(i).setRiskWrapFlag("N");
        	
        }
        
        //mlccontplanriskset  位置往后调整
        //ContPlanInput.jsp  校验
        //将保障计划详情根据保障计划划分
//        ArrayList codelist = new ArrayList();
//        for(int i=1;i<=mLCContPlanSet.size();i++){
//        	String code = mLCContPlanSet.get(i).getContPlanCode();
//        	if(!codelist.contains(code)){
//        		codelist.add(code);
//        	}
//        }
        
        //针对jxhw162601、162801险种，生成lccontplandutyparam
        //k=0 正常的162601、k=1 正常的其他险种、k=2 海外医疗非单独承保
        //多保障计划  tLCContPlanSet 该单的保障计划、保障计划编码、保费、年龄区间、计划非空
        if(check162601()==0){
        	this.mLCContPlanDutyParamSet.clear();
    		for(int i=1;i<=tLCContPlanSet.size();i++){
    			String tsumprem = "";
    			String tagerange = "";
    			String tplan = "";
    			String contplancode = tLCContPlanSet.get(i).getContPlanCode();
    			String contplanname = "";
    			String riskcode = "";
    			String riskamnt = "";
    			for(int j=1;j<=mLCContPlanRiskSet.size();j++){
    				if(contplancode.equals(mLCContPlanRiskSet.get(j).getContPlanCode())){
    					tsumprem = String.valueOf(mLCContPlanRiskSet.get(j).getRiskPrem());
    					contplanname = mLCContPlanRiskSet.get(j).getContPlanName();
    					riskcode = mLCContPlanRiskSet.get(j).getRiskCode();
    				}
    			}
    			for(int j=1;j<=mLCContPlanDutyParamSetjxhw.size();j++){
    				if(contplancode.equals(mLCContPlanDutyParamSetjxhw.get(j).getContPlanCode()) && 
    						"AgeRange".equals(mLCContPlanDutyParamSetjxhw.get(j).getCalFactor())){
    					tagerange = mLCContPlanDutyParamSetjxhw.get(j).getCalFactorValue();
    				}
    				if(contplancode.equals(mLCContPlanDutyParamSetjxhw.get(j).getContPlanCode()) && 
    						"Plan".equals(mLCContPlanDutyParamSetjxhw.get(j).getCalFactor())){
    					tplan = mLCContPlanDutyParamSetjxhw.get(j).getCalFactorValue();
    				}
    			}
    			if(tagerange==null || "".equals(tagerange) || tplan==null || "".equals(tplan) || tsumprem==null || "".equals(tsumprem)){
    				responseOME = buildResponseOME("01","17", "险种"+mLCGrpPolSet.get(1).getRiskCode()+"年龄区间、总保费及险种计划为必录项！");
    				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "险种"+mLCGrpPolSet.get(1).getRiskCode()+"年龄区间、总保费及险种计划为必录项！");
    				System.out.println("responseOME:" + responseOME);
    				return false;
    			}
    			if("162601".equals(riskcode)||"162701".equals(riskcode)){
    				if("1".equals(tplan)){
    					riskamnt="6000000";
    				}else if("2".equals(tplan)){
    					riskamnt="12000000";
    				}
    		    }else if("162801".equals(riskcode)||"162901".equals(riskcode)){
    		    	if("1".equals(tplan)){
    		    		riskamnt="3000000";
    				}else if("2".equals(tplan)){
    					riskamnt="6000000";
    				}
    		    }
    			for(int j=1;j<=mLCContPlanRiskSet.size();j++){
    				if(contplancode.equals(mLCContPlanRiskSet.get(j).getContPlanCode()) && 
    						riskcode.equals(mLCContPlanRiskSet.get(j).getRiskCode())){
    					mLCContPlanRiskSet.get(j).setRiskAmnt(riskamnt);
    				}
    			}
//    			LCContPlanRiskDB lccontplanriskdb = new LCContPlanRiskDB();
//    			lccontplanriskdb.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
//    			lccontplanriskdb.setContPlanCode(contplancode);
//    			lccontplanriskdb.setRiskCode(riskcode);
//    			LCContPlanRiskSet lccontplanriskset = lccontplanriskdb.query();
//    			if(lccontplanriskset.size()!=1){
//    				
//    			}
//    			lccontplanriskset.get(1).setRiskAmnt(riskamnt);
//    			tMMap.put(lccontplanriskset, "UPDATE");
//    			String amntsql = "update lccontplanrisk set riskamnt='"+riskamnt+"' where grpcontno='"+this.mLCGrpContSchema.getGrpContNo()+"' and riskcode='"+riskcode+"' and contplancode='"+contplancode+"' ";
//    			System.out.println(amntsql);
//    			new ExeSQL().execUpdateSQL(amntsql);
    			

    			String tprem = calculatePrem(contplancode,tsumprem);
    			System.out.println("保费++++++++++++："+tprem);
    			//暂时不清
//    			mLCContPlanDutyParamSetjxhw.remove();
    			String jxhwsql = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName," 
    					+ "a.FactorNoti,e.calfactorvalue,'',b.RiskVer,d.GrpPolNo,'"+mLCGrpPolSet.get(1).getRiskCode()+"',a.CalFactorType,c.CalMode,'',a.PayPlanCode,a.GetDutyCode,a.InsuAccNo "
    					+ " from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCGrpPol d ,planJXHW e "
    					+ " where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
    					+ " and a.ChooseFlag in ('0','2') "
    					+ " and GrpContNo = '"+mLCGrpContSchema.getGrpContNo()+"' and a.RiskCode = '"+mLCGrpPolSet.get(1).getRiskCode()+"' "
    					+ " and e.dutycode = a.dutycode"
    					+ " and e.CalFactor = a.CalFactor "
    					+ " and e.plancode = '"+tplan+"' "
    					+ " order by a.RiskCode,a.DutyCode,a.FactorOrder";
    			SSRS jxhwssrs = new ExeSQL().execSQL(jxhwsql);
    			if(jxhwssrs != null && jxhwssrs.MaxRow>0){
    				for(int z=1;z<=jxhwssrs.getMaxRow();z++){
    					LCContPlanDutyParamSchema tlccontplandutyparamjxhw = new LCContPlanDutyParamSchema();
    					tlccontplandutyparamjxhw.setGrpContNo(mProposalGrpContNo);
    					tlccontplandutyparamjxhw.setProposalGrpContNo(mProposalGrpContNo);	
    					tlccontplandutyparamjxhw.setContPlanCode(contplancode);
    					tlccontplandutyparamjxhw.setContPlanName(contplanname);
    					tlccontplandutyparamjxhw.setRiskCode(jxhwssrs.GetText(z, 2));
    					tlccontplandutyparamjxhw.setDutyCode(jxhwssrs.GetText(z, 3));
    					tlccontplandutyparamjxhw.setCalFactor(jxhwssrs.GetText(z, 5));
    					if("Prem".equals(jxhwssrs.GetText(z, 5))){
    						tlccontplandutyparamjxhw.setCalFactorValue(tprem);
    					}else if("agerange".equals(jxhwssrs.GetText(z, 5))){
    						tlccontplandutyparamjxhw.setCalFactorValue(tagerange);
    					}else{
    						tlccontplandutyparamjxhw.setCalFactorValue(jxhwssrs.GetText(z, 8));
    					}
    					tlccontplandutyparamjxhw.setRemark(jxhwssrs.GetText(z, 9));
    					tlccontplandutyparamjxhw.setRiskVersion(jxhwssrs.GetText(z, 10));
    					tlccontplandutyparamjxhw.setGrpPolNo(jxhwssrs.GetText(z, 11));
    					tlccontplandutyparamjxhw.setMainRiskCode(jxhwssrs.GetText(z, 12));
    					tlccontplandutyparamjxhw.setMainRiskVersion(jxhwssrs.GetText(z, 10));
    					tlccontplandutyparamjxhw.setCalFactorType(jxhwssrs.GetText(z, 13));
    					tlccontplandutyparamjxhw.setPlanType("0");
    					tlccontplandutyparamjxhw.setPayPlanCode(jxhwssrs.GetText(z, 16));
    					tlccontplandutyparamjxhw.setGetDutyCode(jxhwssrs.GetText(z, 17));
    					tlccontplandutyparamjxhw.setInsuAccNo(jxhwssrs.GetText(z, 18));
    					tlccontplandutyparamjxhw.setOrder(z-1);
    					this.mLCContPlanDutyParamSet.add(tlccontplandutyparamjxhw);
    				}
    			}else{
    				responseOME = buildResponseOME("01","17", "生成保障计划详情失败！");
    				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "生成保障计划详情失败！");
    				System.out.println("responseOME:" + responseOME);
    				return false;
    			}
    		}
        }else if(check162601()==1){
        	this.mLCContPlanDutyParamSetjxhw.clear();
        }else if(check162601()==2){
			responseOME = buildResponseOME("01","17", "海外医疗险种只能单独承保！");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "海外医疗险种只能单独承保！");
			System.out.println("responseOME:" + responseOME);
			return false;
        }
        this.mLCContPlanDutyParamSetjxhw.clear();
        
        //建工险
        
        for(int i=1;i<=mLCGrpPolSet.size();i++){
        	String risk = mLCGrpPolSet.get(i).getRiskCode();
        	String jgxsql = "select 1 from LMRiskApp where risktype8='4' and riskcode='"+risk+"'";
        	SSRS jgxssrs = new ExeSQL().execSQL(jgxsql);
        	if(jgxssrs.getMaxRow()>0){
        		for(int j=1;j<=mLCContPlanDutyParamSetjgx.size();j++){
        			LCContPlanDutyParamSchema lccontplandutyparamschema = new LCContPlanDutyParamSchema();
        			lccontplandutyparamschema.setContPlanCode("11");
        			lccontplandutyparamschema.setContPlanName("默认计划");
        			lccontplandutyparamschema.setDutyCode("000000");
        			lccontplandutyparamschema.setGetDutyCode("000000");
        			lccontplandutyparamschema.setInsuAccNo("000000");
        			lccontplandutyparamschema.setPayPlanCode("000000");
        			lccontplandutyparamschema.setPlanType("0");
        			lccontplandutyparamschema.setGrpPolNo(
        					mLCGrpPolSet.get(i).getGrpPolNo());
        			lccontplandutyparamschema.setRiskCode(
        					mLCGrpPolSet.get(i).getRiskCode());
        			lccontplandutyparamschema.setRiskVersion(
                            getRiskVersion(mLCGrpPolSet.get(i).getRiskCode()));
        			lccontplandutyparamschema.setMainRiskCode(
                            getRiskVersion(mLCGrpPolSet.get(i).getRiskCode()));
        			lccontplandutyparamschema.setMainRiskVersion(
        					mLCGrpPolSet.get(i).getRiskCode());
        			lccontplandutyparamschema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        			lccontplandutyparamschema.setProposalGrpContNo(mLCGrpContSchema.getGrpContNo());
        			lccontplandutyparamschema.setCalFactor(mLCContPlanDutyParamSetjgx.get(j).getCalFactor());
        			lccontplandutyparamschema.setCalFactorType(mLCContPlanDutyParamSetjgx.get(j).getCalFactorType());
        			lccontplandutyparamschema.setCalFactorValue(mLCContPlanDutyParamSetjgx.get(j).getCalFactorValue());
        			mLCContPlanDutyParamSet.add(lccontplandutyparamschema);
        		}
        		jgxflag = true;
        	}
        }
        if(jgxflag){
        	//校验
        	if(!checkjgx()){
        		return false;
        	}
        	
        	mLCGrpContSubSchemajgx.setPrtNo(this.mLCGrpContSchema.getPrtNo());
        	mLCGrpContSubSchemajgx.setManageCom(this.mLCGrpContSchema.getManageCom());
        	mLCGrpContSubSchemajgx.setOperator(mGlobalInput.Operator);
        	mLCGrpContSubSchemajgx.setMakeDate(theCurrentDate);
        	mLCGrpContSubSchemajgx.setMakeTime(theCurrentTime);
        	mLCGrpContSubSchemajgx.setModifyDate(theCurrentDate);
        	mLCGrpContSubSchemajgx.setModifyTime(theCurrentTime);
        }
        
        
        String tParamRiskCode = "";
		LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
		for(int z=1;z<=tLCContPlanSet.size();z++){
			int k = 0;
			String tcontplancode = tLCContPlanSet.get(z).getContPlanCode();
			for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
				String contplancode = mLCContPlanDutyParamSet.get(i).getContPlanCode();
				if(!contplancode.equals(tcontplancode)){
					continue;
				}
				k++;
				String insuAccNo = mLCContPlanDutyParamSet.get(i).getInsuAccNo();
	        	if(insuAccNo!=null && insuAccNo!=""){
	        		continue;
	        	}
	        	tParamRiskCode = mLCContPlanDutyParamSet.get(i).getRiskCode();
	        	mLCContPlanDutyParamSet.get(i).setGrpContNo(mProposalGrpContNo);
	        	mLCContPlanDutyParamSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
	        	String tGrpPolNo = "";
	        	for(int j=1;j<=mLCGrpPolSet.size();j++){
	            	if(mLCContPlanDutyParamSet.get(i).getRiskCode().equals(mLCGrpPolSet.get(j).getRiskCode())){
	            		tGrpPolNo = mLCGrpPolSet.get(j).getGrpPolNo();
	            		break;
	            	}
	            }
	        	mLCContPlanDutyParamSet.get(i).setGrpPolNo(tGrpPolNo);
	        	mLCContPlanDutyParamSet.get(i).setMainRiskVersion("2002");
	        	mLCContPlanDutyParamSet.get(i).setRiskVersion("2002");
	        	mLCContPlanDutyParamSet.get(i).setPlanType("0");
	        	String tSQL = "select PayPlanCode,GetDutyCode,InsuAccNo from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and chooseflag in('0','2') and factororder='"+k+"'";
	        	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
	        	if(tSSRS != null && tSSRS.MaxRow>0){
	        		mLCContPlanDutyParamSet.get(i).setPayPlanCode(tSSRS.GetText(1, 1));
	            	mLCContPlanDutyParamSet.get(i).setGetDutyCode(tSSRS.GetText(1, 2));
	            	mLCContPlanDutyParamSet.get(i).setInsuAccNo(tSSRS.GetText(1, 3));
	        	}else{
	        		mLCContPlanDutyParamSet.get(i).setPayPlanCode("000000");
	            	mLCContPlanDutyParamSet.get(i).setGetDutyCode("000000");
	            	mLCContPlanDutyParamSet.get(i).setInsuAccNo("000000");
	        	}
	        	
	        	if("DutyAmnt".equals(mLCContPlanDutyParamSet.get(i).getCalFactor()) && !"".equals(StrTool.cTrim(mLCContPlanDutyParamSet.get(i).getCalFactorValue()))){
	        		String tSQL1 = "select 1 from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and calfactor = 'DutyAmnt' ";
	            	String tDutyAmntFlag = new ExeSQL().getOneValue(tSQL1);
	            	if(!"1".equals(tDutyAmntFlag)){
	            		for(int m=1;m<=mLCContPlanDutyParamSet.size();m++){
	            			if("Amnt".equals(mLCContPlanDutyParamSet.get(m).getCalFactor())){
	            				mLCContPlanDutyParamSet.get(m).setCalFactorValue(mLCContPlanDutyParamSet.get(i).getCalFactorValue());
	            			}
	            		}
	            	}
	        	}
	        	
	        	mLCContPlanDutyParamSet.get(i).setOrder(k-1);
			}
		}
//        for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
//        	String insuAccNo = mLCContPlanDutyParamSet.get(i).getInsuAccNo();
//        	if(insuAccNo!=null && insuAccNo!=""){
//        		continue;
//        	}
//        	tParamRiskCode = mLCContPlanDutyParamSet.get(i).getRiskCode();
//        	mLCContPlanDutyParamSet.get(i).setGrpContNo(mProposalGrpContNo);
//        	mLCContPlanDutyParamSet.get(i).setProposalGrpContNo(mProposalGrpContNo);
//        	String tGrpPolNo = "";
//        	for(int j=1;j<=mLCGrpPolSet.size();j++){
//            	if(mLCContPlanDutyParamSet.get(i).getRiskCode().equals(mLCGrpPolSet.get(j).getRiskCode())){
//            		tGrpPolNo = mLCGrpPolSet.get(j).getGrpPolNo();
//            		break;
//            	}
//            }
//        	mLCContPlanDutyParamSet.get(i).setGrpPolNo(tGrpPolNo);
//        	mLCContPlanDutyParamSet.get(i).setMainRiskVersion("2002");
//        	mLCContPlanDutyParamSet.get(i).setRiskVersion("2002");
//        	mLCContPlanDutyParamSet.get(i).setPlanType("0");
//        	String tSQL = "select PayPlanCode,GetDutyCode,InsuAccNo from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and chooseflag in('0','2') and factororder='"+i+"'";
//        	SSRS tSSRS = new ExeSQL().execSQL(tSQL);
//        	if(tSSRS != null && tSSRS.MaxRow>0){
//        		mLCContPlanDutyParamSet.get(i).setPayPlanCode(tSSRS.GetText(1, 1));
//            	mLCContPlanDutyParamSet.get(i).setGetDutyCode(tSSRS.GetText(1, 2));
//            	mLCContPlanDutyParamSet.get(i).setInsuAccNo(tSSRS.GetText(1, 3));
//        	}else{
//        		mLCContPlanDutyParamSet.get(i).setPayPlanCode("000000");
//            	mLCContPlanDutyParamSet.get(i).setGetDutyCode("000000");
//            	mLCContPlanDutyParamSet.get(i).setInsuAccNo("000000");
//        	}
//        	
//        	if("DutyAmnt".equals(mLCContPlanDutyParamSet.get(i).getCalFactor()) && !"".equals(StrTool.cTrim(mLCContPlanDutyParamSet.get(i).getCalFactorValue()))){
//        		String tSQL1 = "select 1 from lmriskdutyfactor where riskcode = '"+mLCContPlanDutyParamSet.get(i).getRiskCode()+"' and dutycode = '"+mLCContPlanDutyParamSet.get(i).getDutyCode()+"' and calfactor = 'DutyAmnt' ";
//            	String tDutyAmntFlag = new ExeSQL().getOneValue(tSQL1);
//            	if(!"1".equals(tDutyAmntFlag)){
//            		for(int m=1;m<=mLCContPlanDutyParamSet.size();m++){
//            			if("Amnt".equals(mLCContPlanDutyParamSet.get(m).getCalFactor())){
//            				mLCContPlanDutyParamSet.get(m).setCalFactorValue(mLCContPlanDutyParamSet.get(i).getCalFactorValue());
//            			}
//            		}
//            	}
//        	}
//        	
//        	mLCContPlanDutyParamSet.get(i).setOrder(i-1);
//        	
//        	
////        	//默认保障计划
////        	if(!tParamRiskMap.containsKey(tParamRiskCode)){
////        		tParamRiskMap.put(tParamRiskCode, "");
////        		LCContPlanDutyParamSchema tempLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
////        		tReflections.transFields(tempLCContPlanDutyParamSchema,mLCContPlanDutyParamSet.get(i));
////        		tempLCContPlanDutyParamSchema.setContPlanCode("11");
////        		tempLCContPlanDutyParamSchema.setContPlanName("默认计划");
////        		tempLCContPlanDutyParamSchema.setDutyCode("000000");
////        		tempLCContPlanDutyParamSchema.setCalFactor("CalRule");
////        		tempLCContPlanDutyParamSchema.setCalFactorType("1");
////        		tempLCContPlanDutyParamSchema.setCalFactorValue("0");
////        		tempLCContPlanDutyParamSchema.setRiskVersion(tParamRiskCode);
////        		tLCContPlanDutyParamSet.add(tempLCContPlanDutyParamSchema);
////        	}
//        	
//        }
        //险种默认保障计划x
        HashMap tParamRiskMap = new HashMap();
        if (mLCContPlanDutyParamSetDefault != null) {
            for (int i = 1; i <= this.mLCContPlanDutyParamSetDefault.size(); i++) {
            	String deRisk = "";
            	deRisk = mLCContPlanDutyParamSetDefault.get(i).getRiskCode();
            	if(!tParamRiskMap.containsKey(deRisk)){
	            	tParamRiskMap.put(deRisk, "");
	                LMRiskDutyFactorDB tLMRiskDutyFactorDB = new
	                        LMRiskDutyFactorDB();
	                tLMRiskDutyFactorDB.setRiskCode(mLCContPlanDutyParamSetDefault.get(
	                        i).getRiskCode());
	                tLMRiskDutyFactorDB.setCalFactor(mLCContPlanDutyParamSetDefault.
	                        get(i).getCalFactor());
	                tLMRiskDutyFactorDB.setDutyCode("000000");
	                tLMRiskDutyFactorDB.setPayPlanCode("000000");
	                tLMRiskDutyFactorDB.setGetDutyCode("000000");
	                tLMRiskDutyFactorDB.setInsuAccNo("000000");
	                if (!tLMRiskDutyFactorDB.getInfo()) {
	                	responseOME = buildResponseOME("01", "15", "未查询到" +
                                mLCContPlanDutyParamSet.get(i).
                                getCalFactor() +
                                "默认要素！");
	    				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
	    						sendDate, sendTime, "01", "15", "WX","未查询到" +
                                        mLCContPlanDutyParamSet.get(i).
                                        getCalFactor() +
                                        "默认要素！");
	    				System.out.println("responseOME:" + responseOME);
	                    return false;
	                }
	                LMRiskDB tLMRiskDB = new LMRiskDB();
	                tLMRiskDB.setRiskCode(deRisk);
	                if (tLMRiskDB.getInfo() == false) {
	                    // @@错误处理
	                    CError tError = new CError();
	                    tError.moduleName = "GroupRiskBL";
	                    tError.functionName = "getInputData";
	                    tError.errorMessage = "没有查到险种信息!";
	                    this.mErrors.addOneError(tError);
	                    return false;
	                }
	                String tGrpPolNo = "";
	                for(int j=1;j<=mLCGrpPolSet.size();j++){
	                	if(mLCContPlanDutyParamSetDefault.get(i).getRiskCode().equals(mLCGrpPolSet.get(j).getRiskCode())){
	                		tGrpPolNo = mLCGrpPolSet.get(j).getGrpPolNo();
	                		break;
	                	}
	                }
	                mLCContPlanDutyParamSetDefault.get(i).setGrpContNo(mProposalGrpContNo);
	                mLCContPlanDutyParamSetDefault.get(i).setProposalGrpContNo(mProposalGrpContNo);
	                mLCContPlanDutyParamSetDefault.get(i).setGrpPolNo(tGrpPolNo);
	                mLCContPlanDutyParamSetDefault.get(i).setMainRiskCode(tLMRiskDB.
	                        getRiskCode());
	                mLCContPlanDutyParamSetDefault.get(i).setMainRiskVersion(tLMRiskDB.
	                        getRiskVer());
	                mLCContPlanDutyParamSetDefault.get(i).setRiskCode(tLMRiskDB.
	                        getRiskCode());
	                mLCContPlanDutyParamSetDefault.get(i).setRiskVersion(tLMRiskDB.
	                        getRiskCode());
	                mLCContPlanDutyParamSetDefault.get(i).setContPlanCode("11");
	                mLCContPlanDutyParamSetDefault.get(i).setContPlanName("默认计划");
	                mLCContPlanDutyParamSetDefault.get(i).setDutyCode(
	                        tLMRiskDutyFactorDB.getDutyCode());
	                mLCContPlanDutyParamSetDefault.get(i).setCalFactorType(
	                        tLMRiskDutyFactorDB.
	                        getCalFactorType());
	                mLCContPlanDutyParamSetDefault.get(i).setPlanType("0");
	                mLCContPlanDutyParamSetDefault.get(i).setPayPlanCode(
	                        tLMRiskDutyFactorDB.
	                        getPayPlanCode());
	                mLCContPlanDutyParamSetDefault.get(i).setGetDutyCode(
	                        tLMRiskDutyFactorDB.
	                        getGetDutyCode());
	                mLCContPlanDutyParamSetDefault.get(i).setInsuAccNo(
	                        tLMRiskDutyFactorDB.getInsuAccNo());
            		}
            }
        }
        mLCContPlanDutyParamSet.add(tLCContPlanDutyParamSet);
        
        
//        if(check690101()){
	        /**
	         * 将账户录入的要素信息补充到计划的责任下；
	         */
	        //contplancode为11 表示帐户计划
	        LCContPlanDutyParamSet tLCContPlanDutyParamSet3 = new LCContPlanDutyParamSet();
	        LCContPlanDutyParamSet tLCContPlanDutyParamSet4 = new LCContPlanDutyParamSet();
	        String risksql = "select distinct riskcode from lcgrppol where grpcontno = '"+this.mLCGrpContSchema.getGrpContNo()+"'";
	        SSRS riskssrs = new ExeSQL().execSQL(risksql);
	        if(riskssrs.getMaxRow()<1){
	        	responseOME = buildResponseOME("01", "17", "获取险种信息失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "17", "WX","获取险种信息失败！");
				System.out.println("responseOME:" + responseOME);
		    	return false;
	        }
	        for(int i=1;i<=mLCContPlanDutyParamSet.size();i++){
	        	if("11".equals(mLCContPlanDutyParamSet.get(i).getContPlanCode()) 
//	        			&& riskssrs.GetText(k, 1).equals(mLCContPlanDutyParamSet.get(i).getRiskCode())
	        			){
	        		for(int k=1;k<=riskssrs.getMaxRow();k++){
	        			if(riskssrs.GetText(k, 1).equals(mLCContPlanDutyParamSet.get(i).getRiskCode())){
	        				tLCContPlanDutyParamSet3.add(mLCContPlanDutyParamSet.get(i));
	        			}
	        		}
	        	}else{
	        		tLCContPlanDutyParamSet4.add(mLCContPlanDutyParamSet.get(i));
	        	}
	        }
	        for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
	        	if("11".equals(mLCContPlanDutyParamSet2.get(i).getContPlanCode()) 
//	        			&& riskssrs.GetText(k, 1).equals(mLCContPlanDutyParamSet2.get(i).getRiskCode())
	        			){
	        		for(int k=1;k<=riskssrs.getMaxRow();k++){
	        			if(riskssrs.GetText(k, 1).equals(mLCContPlanDutyParamSet2.get(i).getRiskCode())){
	        				tLCContPlanDutyParamSet3.add(mLCContPlanDutyParamSet2.get(i));
	        			}
	        		}
	        	}
	        }
	        tLCContPlanDutyParamSet3.add(mLCContPlanDutyParamSetDefault);
	        for(int i=1;i<=tLCContPlanSet.size();i++){
	        	String contplancode = tLCContPlanSet.get(i).getContPlanCode();
	        	int theFinal = tLCContPlanDutyParamSet4.size();
	        	//把报文中的保障计划详情按照保障计划分类
	        	LCContPlanDutyParamSet tLCContPlanDutyParamSet5 = new LCContPlanDutyParamSet();
	        	for(int k = 1; k <= theFinal; k++){
	        		if(contplancode.equals(tLCContPlanDutyParamSet4.get(k).getContPlanCode())){
	        			tLCContPlanDutyParamSet5.add(tLCContPlanDutyParamSet4.get(k));
	        		}
	        	}
	        	String dutyCode = tLCContPlanDutyParamSet5.get(1).getDutyCode();
	        	int theFinal5 = tLCContPlanDutyParamSet5.size();
	        	for (int j = 1; j <= theFinal5; j++)
	              {
	                  if (j == 1
	                          || (!dutyCode.equals(tLCContPlanDutyParamSet5.get(j)
	                                  .getDutyCode())))
	                  {
	                      for (int n = 1; n <= tLCContPlanDutyParamSet3.size(); n++)
	                      {
	                          if (tLCContPlanDutyParamSet3.get(n).getRiskCode()
	                                  .equals(
	                                  		tLCContPlanDutyParamSet5.get(j)
	                                                  .getRiskCode()))
	                          {
	                              LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = tLCContPlanDutyParamSet3
	                                      .get(n).getSchema();
	                              tLCContPlanDutyParamSchema
	                                      .setDutyCode(tLCContPlanDutyParamSet5.get(j)
	                                              .getDutyCode());
	                              tLCContPlanDutyParamSchema
	                                      .setMainRiskCode(tLCContPlanDutyParamSet5
	                                              .get(j).getMainRiskCode());
	                              tLCContPlanDutyParamSchema
	                                      .setContPlanCode(tLCContPlanDutyParamSet5
	                                              .get(j).getContPlanCode());
	                              tLCContPlanDutyParamSchema
	                                      .setContPlanName(tLCContPlanDutyParamSet5
	                                              .get(j).getContPlanName());
	                              mLCContPlanDutyParamSet
	                                      .add(tLCContPlanDutyParamSchema);
	                          }
	                      }
	                  }
	                  dutyCode = tLCContPlanDutyParamSet5.get(j).getDutyCode();
	              }
	        	
//	        }
	//        String dutyCode = tLCContPlanDutyParamSet4.get(1).getDutyCode();//get（1）set3（11） set4（普通）
	//        String contplancode = tLCContPlanDutyParamSet4.get(1).getContPlanCode();
	//        int theFinal = tLCContPlanDutyParamSet4.size();
	//        for (int i = 1; i <= theFinal; i++)
	//        {
	//            if (i == 1
	//                    || (!dutyCode.equals(tLCContPlanDutyParamSet4.get(i)
	//                            .getDutyCode())) 
	//                            || (dutyCode.equals(tLCContPlanDutyParamSet4.get(i)
	//                                    .getDutyCode()) && (!contplancode.equals(tLCContPlanDutyParamSet4.get(i).getContPlanCode())) ) 
	//                                    )
	//            {
	//                for (int n = 1; n <= tLCContPlanDutyParamSet3.size(); n++)
	//                {
	//                    if (tLCContPlanDutyParamSet3.get(n).getRiskCode()
	//                            .equals(
	//                            		tLCContPlanDutyParamSet4.get(i)
	//                                            .getRiskCode()))
	//                    {
	//                        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = tLCContPlanDutyParamSet3
	//                                .get(n).getSchema();
	//                        tLCContPlanDutyParamSchema
	//                                .setDutyCode(tLCContPlanDutyParamSet4.get(i)
	//                                        .getDutyCode());
	//                        tLCContPlanDutyParamSchema
	//                                .setMainRiskCode(tLCContPlanDutyParamSet4
	//                                        .get(i).getMainRiskCode());
	//                        tLCContPlanDutyParamSchema
	//                                .setContPlanCode(tLCContPlanDutyParamSet4
	//                                        .get(i).getContPlanCode());
	//                        tLCContPlanDutyParamSchema
	//                                .setContPlanName(tLCContPlanDutyParamSet4
	//                                        .get(i).getContPlanName());
	//                        mLCContPlanDutyParamSet
	//                                .add(tLCContPlanDutyParamSchema);
	//                    }
	//                }
	//            }
	//            dutyCode = tLCContPlanDutyParamSet4.get(i).getDutyCode();
	//        }
        }
        mLCContPlanDutyParamSet.add(mLCContPlanDutyParamSetDefault);
        if(jgxflag){
        	tMMap.put(mLCGrpContSubSchemajgx, "INSERT");
        }
        tMMap.put(mLCContPlanRiskSet, "INSERT");
        tMMap.put(tLCContPlanRiskSet, "INSERT");
        tMMap.put(mLCContPlanDutyParamSet, "INSERT");
		return true;
	}
	
	private boolean creatPubAccContPlan(){
		
		
		
		LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
		LCGrpInterestSet tLCGrpInterestSet = new LCGrpInterestSet();
		LDPromiseRateSet tLDPromiseRateSet = new LDPromiseRateSet();
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		
		  //实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
	      String insuAccNo = mLCContPlanDutyParamSet2.get(1).getInsuAccNo();
		  String strSql = "select acctype from LMRiskInsuAcc where  InsuAccNo='"+insuAccNo+"'";
		  String strSqlRes = new ExeSQL().getOneValue(strSql); 
		  //团体账户名称，类型
      	String tInsuredName ;
      	String tPublicAccType ;
  		if("001".equals(strSqlRes))
  		{
  			tPublicAccType="C";
  			tInsuredName="团体理赔帐户";
  		}else if("004".equals(strSqlRes))
  		{
  			tPublicAccType="G";
  			tInsuredName="团体固定账户";
  		}else{
	    		//"没有指定帐户类型!";
	    		return false;
  		}
			//账户金额
  		String tPublicAcc = "";
  		for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
  			if("PublicAcc".equals(mLCContPlanDutyParamSet2.get(i).getCalFactor())){
  				tPublicAcc = mLCContPlanDutyParamSet2.get(i).getCalFactorValue();
  			}
  		}
  			
			//险种代码
  			String tRiskCode = "";
  			for(int i=1;i<=mLCGrpPolSet.size();i++){
  				tRiskCode = mLCGrpPolSet.get(i).getRiskCode();
  				String sql = "select 1 from lmriskapp where riskcode='"+tRiskCode+"' and SubRiskFlag='M' with ur ";
  				SSRS res = new ExeSQL().execSQL(sql);
  				if(res != null && res.getMaxRow()>0){
  					break;
  				}
  			}
			//处理要素信息
			String tInsuAccNo = insuAccNo;

			LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
			tLCInsuredListSchema.setGrpContNo(mProposalGrpContNo);
			tLCInsuredListSchema.setInsuredName(tInsuredName);
			tLCInsuredListSchema.setEmployeeName(tInsuredName);
			tLCInsuredListSchema.setRelation("00");
			tLCInsuredListSchema.setRiskCode(tRiskCode);
			tLCInsuredListSchema.setPublicAcc(tPublicAcc);
			tLCInsuredListSchema.setPublicAccType(tPublicAccType);
			mLCInsuredListSet2.add(tLCInsuredListSchema);
			
			String tGrpPolNosql = "select grppolno from lcgrppol where grpcontno='"+mProposalGrpContNo+"' and riskcode = '"+tRiskCode+"' with ur ";
			SSRS tSSRS = new ExeSQL().execSQL(tGrpPolNosql);
			String tGrpPolNo1 = tSSRS.GetText(1, 1);
			for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
				mLCContPlanDutyParamSet2.get(i).setGrpContNo(mProposalGrpContNo);
				mLCContPlanDutyParamSet2.get(i).setProposalGrpContNo(mProposalGrpContNo);
	        	mLCContPlanDutyParamSet2.get(i).setGrpPolNo(tGrpPolNo1);
	        	mLCContPlanDutyParamSet2.get(i).setMainRiskCode(tRiskCode);
	        	mLCContPlanDutyParamSet2.get(i).setRiskCode(tRiskCode);
	        	mLCContPlanDutyParamSet2.get(i).setContPlanCode("11");
	        	mLCContPlanDutyParamSet2.get(i).setDutyCode("000000");
	        	mLCContPlanDutyParamSet2.get(i).setPlanType("0");
	        	mLCContPlanDutyParamSet2.get(i).setPayPlanCode("000000");
	        	mLCContPlanDutyParamSet2.get(i).setGetDutyCode("000000");
	        	
			}
		
			//INSERT||MAIN
			// 准备传输数据 VData
			String transact = "INSERT||MAIN";
		  	VData tVData = new VData();
		  	TransferData tTransferData = new TransferData();
		  	tTransferData.setNameAndValue("GrpContNo",mProposalGrpContNo);
		  	tTransferData.setNameAndValue("RiskCode",tRiskCode);
		  	tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo1);
		  	tTransferData.setNameAndValue("InsuAccNo",mLCContPlanDutyParamSet2.get(1).getInsuAccNo());
		  	String claimnum ="";
		  	tTransferData.setNameAndValue("ClaimNum",claimnum);
		  	String tFlag = "";
		  	tTransferData.setNameAndValue("Flag",tFlag);
		  	tVData.add(tTransferData);
			tVData.add(mLCInsuredListSet2);
			tVData.add(mLCContPlanDutyParamSet2);
			tVData.add(tLCGrpFeeSet);
			tVData.add(tLCGrpInterestSet);
			tVData.add(tLDPromiseRateSet);
		  	tVData.add(tG);
		  	GrpPubAccBL tGrpPubAccBL = new GrpPubAccBL();
		  	if(!tGrpPubAccBL.submitData(tVData,transact)){
		  		responseOME = buildResponseOME("01", "15", "生成公共账户数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "15", "WX","生成公共账户数据失败!");
				System.out.println("responseOME:" + responseOME);
		    	return false;
		  	}
	        return true;
	    
		
	}
	
	private boolean createManageFee(){
		
		if(mLCGrpFeeSet.size()!=2){
			responseOME = buildResponseOME("01","05", "获取管理费信息失败!");
			System.out.println("responseOME:" + responseOME);
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","05", "WX", "获取管理费信息失败!");
			return false;
		}
		
		String mainrisksql = "select riskcode from lmriskapp where riskcode in (select riskcode from lcgrppol where prtno='"+this.mLCGrpContSchema.getPrtNo()+"') and subriskflag='M' with ur";
		String mainrisk = new ExeSQL().execSQL(mainrisksql).GetText(1, 1);
		
		
		String tGrpPolNosql = "select grppolno from lcgrppol where grpcontno='"+mProposalGrpContNo+"' and riskcode = '"+mainrisk+"'";
		SSRS tSSRS3 = new ExeSQL().execSQL(tGrpPolNosql);
		String tGrpPolNo1 = tSSRS3.GetText(1, 1);
		
		String strSql = "select distinct GrpPolNo,GrpContNo,RiskCode,"
				+"FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
				+"FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
				+"FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
				+"CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
				+"MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"+tGrpPolNo1+"' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"+mainrisk+"' and AccType<>'002' union select '000000' from dual)";
		SSRS tSSRS = new ExeSQL().execSQL(strSql);
		for(int i=1;i<=tSSRS.getMaxRow();i++){
			for(int j=1;j<=mLCGrpFeeSet.size();j++){
				if(mLCGrpFeeSet.get(j).getFeeCode().equals(tSSRS.GetText(i, 4))){
					mLCGrpFeeSet.get(j).setGrpPolNo(tSSRS.GetText(i, 1));
					mLCGrpFeeSet.get(j).setGrpContNo(tSSRS.GetText(i, 2));
					mLCGrpFeeSet.get(j).setRiskCode(tSSRS.GetText(i, 3));
					mLCGrpFeeSet.get(j).setFeeCode(tSSRS.GetText(i, 4));
					mLCGrpFeeSet.get(j).setInsuAccNo(tSSRS.GetText(i, 5));
					mLCGrpFeeSet.get(j).setPayPlanCode(tSSRS.GetText(i, 6));
					mLCGrpFeeSet.get(j).setPayInsuAccName(tSSRS.GetText(i, 7));
					mLCGrpFeeSet.get(j).setFeeCalMode(tSSRS.GetText(i, 8));
					mLCGrpFeeSet.get(j).setFeeCalModeType(tSSRS.GetText(i, 9));
					mLCGrpFeeSet.get(j).setFeeCalCode(tSSRS.GetText(i, 10));
					mLCGrpFeeSet.get(j).setCompareValue(tSSRS.GetText(i, 13));
					mLCGrpFeeSet.get(j).setFeePeriod(tSSRS.GetText(i, 14));
					mLCGrpFeeSet.get(j).setMaxTime(tSSRS.GetText(i, 15));
					mLCGrpFeeSet.get(j).setDefaultFlag(tSSRS.GetText(i, 16));
					mLCGrpFeeSet.get(j).setOperator(tSSRS.GetText(i, 17));
					mLCGrpFeeSet.get(j).setMakeDate(tSSRS.GetText(i, 18));
					mLCGrpFeeSet.get(j).setMakeTime(tSSRS.GetText(i, 19));
					mLCGrpFeeSet.get(j).setModifyDate(tSSRS.GetText(i, 20));
					mLCGrpFeeSet.get(j).setModifyTime(tSSRS.GetText(i, 21));
					mLCGrpFeeSet.get(j).setClaimNum(ClaimNum);
				}
			}
		}
		
		String strSql2 = "select distinct GrpPolNo,GrpContNo,RiskCode,"
				+"FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
				+"FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
				+"FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
				+"CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
				+"MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"+tGrpPolNo1+"' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"+mainrisk+"' and AccType='002')";
		SSRS tSSRS2 = new ExeSQL().execSQL(strSql2);
		for(int i=1;i<=tSSRS2.getMaxRow();i++){
			LCGrpFeeSchema tLCGrpFeeSchema = new LCGrpFeeSchema();
			
			tLCGrpFeeSchema.setGrpPolNo(tSSRS2.GetText(i, 1));
			tLCGrpFeeSchema.setGrpContNo(tSSRS2.GetText(i, 2));
			tLCGrpFeeSchema.setRiskCode(tSSRS2.GetText(i, 3));
			tLCGrpFeeSchema.setFeeCode(tSSRS2.GetText(i, 4));
			tLCGrpFeeSchema.setInsuAccNo(tSSRS2.GetText(i, 5));
			tLCGrpFeeSchema.setPayPlanCode(tSSRS2.GetText(i, 6));
			tLCGrpFeeSchema.setPayInsuAccName(tSSRS2.GetText(i, 7));
			tLCGrpFeeSchema.setFeeCalMode(tSSRS2.GetText(i, 8));
			tLCGrpFeeSchema.setFeeCalModeType(tSSRS2.GetText(i, 9));
			tLCGrpFeeSchema.setFeeCalCode(tSSRS2.GetText(i, 10));
//			for(int j=1;j<=tSSRS.getMaxRow();j++){
//				if(tSSRS2.GetText(i, 11).equals(tSSRS.GetText(j, 11))){
//					tLCGrpFeeSchema.setFeeValue(mLCGrpFeeSet.get(j).getFeeValue());
//				}
//			}
			for(int j=1;j<=mLCGrpFeeSet.size();j++){
				if("000002".equals(mLCGrpFeeSet.get(j).getFeeCode())){
					tLCGrpFeeSchema.setFeeValue(mLCGrpFeeSet.get(j).getFeeValue());
				}
			}
			
			tLCGrpFeeSchema.setCompareValue(tSSRS2.GetText(i, 13));
			tLCGrpFeeSchema.setFeePeriod(tSSRS2.GetText(i, 14));
			tLCGrpFeeSchema.setMaxTime(tSSRS2.GetText(i, 15));
			tLCGrpFeeSchema.setDefaultFlag(tSSRS2.GetText(i, 16));
			tLCGrpFeeSchema.setOperator(tSSRS2.GetText(i, 17));
			tLCGrpFeeSchema.setMakeDate(tSSRS2.GetText(i, 18));
			tLCGrpFeeSchema.setMakeTime(tSSRS2.GetText(i, 19));
			tLCGrpFeeSchema.setModifyDate(tSSRS2.GetText(i, 20));
			tLCGrpFeeSchema.setModifyTime(tSSRS2.GetText(i, 21));
			tLCGrpFeeSchema.setClaimNum(ClaimNum);
			mLCGrpFeeSet.add(tLCGrpFeeSchema);
		}
		
		LCGrpInterestSet tLCGrpInterestSet = new LCGrpInterestSet();
		LDPromiseRateSet tLDPromiseRateSet = new LDPromiseRateSet();
		GlobalInput tG = new GlobalInput(); 
		tG.ComCode=mLCGrpContSchema.getManageCom();
		tG.ManageCom=mLCGrpContSchema.getManageCom();
		tG.Operator=mLCGrpContSchema.getOperator();
		
		  //实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
	      String insuAccNo = mLCContPlanDutyParamSet2.get(1).getInsuAccNo();
		  String strSql3 = "select acctype from LMRiskInsuAcc where  InsuAccNo='"+insuAccNo+"'";
		  String strSqlRes = new ExeSQL().getOneValue(strSql3); 
		  //团体账户名称，类型
      	String tInsuredName ;
      	String tPublicAccType ;
  		if("001".equals(strSqlRes))
  		{
  			tPublicAccType="C";
  			tInsuredName="团体理赔帐户";
  		}else if("004".equals(strSqlRes))
  		{
  			tPublicAccType="G";
  			tInsuredName="团体固定账户";
  		}else{
	    		//"没有指定帐户类型!";
	    		return false;
  		}
			//账户金额
  		String tPublicAcc = "";
  		for(int i=1;i<=mLCContPlanDutyParamSet2.size();i++){
  			if("PublicAcc".equals(mLCContPlanDutyParamSet2.get(i).getCalFactor())){
  				tPublicAcc = mLCContPlanDutyParamSet2.get(i).getCalFactorValue();
  			}
  		}
  			
			//险种代码
			String tRiskCode = mLCContPlanRiskSet.get(1).getRiskCode();
			//处理要素信息
			String tInsuAccNo = insuAccNo;

			LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
			LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
			tLCInsuredListSchema.setGrpContNo(mProposalGrpContNo);
			tLCInsuredListSchema.setInsuredName(tInsuredName);
			tLCInsuredListSchema.setEmployeeName(tInsuredName);
			tLCInsuredListSchema.setRelation("00");
			tLCInsuredListSchema.setRiskCode(tRiskCode);
			tLCInsuredListSchema.setPublicAcc(tPublicAcc);
			tLCInsuredListSchema.setPublicAccType(tPublicAccType);
			mLCInsuredListSet.add(tLCInsuredListSchema);
		
			LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
			//INSERT||MAIN
			// 准备传输数据 VData
			String transact = "MANAGEFEE||MAIN";
		  	VData tVData = new VData();
		  	TransferData tTransferData = new TransferData();
		  	tTransferData.setNameAndValue("GrpContNo",mProposalGrpContNo);
		  	tTransferData.setNameAndValue("RiskCode",mLCContPlanRiskSet.get(1).getRiskCode());
		  	tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo1);
		  	tTransferData.setNameAndValue("InsuAccNo",mLCContPlanDutyParamSet2.get(1).getInsuAccNo());
		  	tTransferData.setNameAndValue("ClaimNum",ClaimNum);
		  	String tFlag = "";
		  	tTransferData.setNameAndValue("Flag",tFlag);
		  	tVData.add(tTransferData);
				tVData.add(mLCInsuredListSet);
				tVData.add(tLCContPlanDutyParamSet);
				tVData.add(mLCGrpFeeSet);
				tVData.add(tLCGrpInterestSet);
				tVData.add(tLDPromiseRateSet);
		  	tVData.add(tG);
		  	GrpPubAccBL tGrpPubAccBL = new GrpPubAccBL();
		  	if(!tGrpPubAccBL.submitData(tVData,transact)){
		  		responseOME = buildResponseOME("01", "16", "生成管理费数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "16", "WX","生成管理费数据失败!");
				System.out.println("responseOME:" + responseOME);
		    	return false;
		  	}
	        return true;
	        

	}
	
	
	
	
	private boolean creatInsured(){
		for(int i=1;i<=mLCInsuredListSet.size();i++){
			mLCInsuredListSet.get(i).setGrpContNo(mProposalGrpContNo);
			mLCInsuredListSet.get(i).setState("0");
			mLCInsuredListSet.get(i).setBatchNo(mLCGrpContSchema.getPrtNo());
			mLCInsuredListSet.get(i).setOperator(mGlobalInput.Operator);
			mLCInsuredListSet.get(i).setMakeDate(theCurrentDate);
			mLCInsuredListSet.get(i).setMakeTime(theCurrentTime);
			mLCInsuredListSet.get(i).setModifyDate(theCurrentDate);
			mLCInsuredListSet.get(i).setModifyTime(theCurrentTime);
		}
		tMMap.put(mLCInsuredListSet, SysConst.INSERT);
		
		return true;
	}
	
	private boolean calPrem(){
		ParseGuideIn tParseGuideIn = new ParseGuideIn();
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("GrpContNo", mProposalGrpContNo);
		
		tVData.add(tTransferData);
		tVData.add(mGlobalInput);
		if (tParseGuideIn.submitData(tVData, "INSERT||DATABASE") == false) {
			// @@错误处理
			responseOME = buildResponseOME("01", "18", "保费计算失败，原因为："+tParseGuideIn.mErrors.getLastError()+"!");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "18", "WX","保费计算失败，原因为："+tParseGuideIn.mErrors.getLastError()+"!");
			System.out.println("responseOME:" + responseOME);
			return false;
		}else{
			//获取报错详细信息
			String sql = "select contid,ErrorInfo from LCGrpImportLog where grpcontno=(select grpcontno from lcgrpcont where prtno='"+mLCGrpContSchema.getPrtNo()+"') and Errorstate='1' with ur";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS.getMaxRow()>0){
				String errinfo = "";
				String errid = "";
				for(int i=1;i<=tSSRS.getMaxRow();i++){
					errinfo = errinfo + i +"." + tSSRS.GetText(i, 2);
					if(i==1){
						errid = errid + tSSRS.GetText(i, 1);
					}else{
						errid = errid + "、" + tSSRS.GetText(i, 1);
					}
				}
				responseOME = buildResponseOME("01", "18", errinfo);
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "18", "WX","被保人"+errid+"信息有误！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if(!checkPeople()){
			responseOME = buildResponseOME("01", "18", "被保险人数应大于等于3！");
			LoginVerifyTool.writeLog(responseOME, batchno, messageType,
					sendDate, sendTime, "01", "18", "WX","被保险人数应大于等于3！");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		//校验浮动费率
		if(!checkTotalFactor()){
	    	return false;
	    }
		return true;
	}
	
	//生成共享标识
		private boolean setShared(){
			MMap tMMap = new MMap();
			String trace = "delete from lcpersontrace where contractno='"+this.mLCGrpContSchema.getPrtNo()+"'";
			boolean b = new ExeSQL().execUpdateSQL(trace);
	    	if(!b){
	    		responseOME = buildResponseOME("01", "19", "生成共享标识数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "19", "WX","生成共享标识数据失败!");
				System.out.println("responseOME:" + responseOME);
				return false;

	    	}
			String customernosql = "select insuredno from lcinsured where prtno='"+this.mLCGrpContSchema.getPrtNo()+"' and name not in ('公共账户') with ur";
			SSRS customernossrs = new ExeSQL().execSQL(customernosql);
			if(customernossrs.getMaxRow()>0){
				for(int i=1;i<=customernossrs.getMaxRow();i++){
					LCPersonTraceSchema tLCPersonTraceSchema = new LCPersonTraceSchema();
					tLCPersonTraceSchema.setCustomerNo(customernossrs.GetText(i, 1));
					//查询最大号
					String traceno = "select case when varchar(max(int(TraceNo))) is null then '0' else varchar(max(int(TraceNo))) end from lcpersontrace where customerno='"+customernossrs.GetText(i, 1)+"' with ur ";
					int index = Integer.parseInt(new ExeSQL().getOneValue(traceno));
					tLCPersonTraceSchema.setTraceNo(String.valueOf(index+1));
					tLCPersonTraceSchema.setContractNo(this.mLCGrpContSchema.getPrtNo());
					tLCPersonTraceSchema.setAuthorization(mLCPersonTraceSchema.getAuthorization());
					tLCPersonTraceSchema.setSharedMark(mLCPersonTraceSchema.getSharedMark());
					tLCPersonTraceSchema.setSpecialLimitMark(mLCPersonTraceSchema.getSpecialLimitMark());
					tLCPersonTraceSchema.setBusinessLink(mLCPersonTraceSchema.getBusinessLink());
					tLCPersonTraceSchema.setCustomerContact(mLCPersonTraceSchema.getCustomerContact());
					tLCPersonTraceSchema.setAuthType(mLCPersonTraceSchema.getAuthType());
					tLCPersonTraceSchema.setAuthVersion("1.0");
					tLCPersonTraceSchema.setCompanySource("2");
					tLCPersonTraceSchema.setInstitutionSource(this.mLCGrpContSchema.getManageCom());
					tLCPersonTraceSchema.setSendDate(theCurrentDate);
					tLCPersonTraceSchema.setSendTime(theCurrentTime);
					tLCPersonTraceSchema.setModifyDate(theCurrentDate);
					tLCPersonTraceSchema.setModifyTime(theCurrentTime);
					mLCPersonTraceSet.add(tLCPersonTraceSchema);
				}
				tMMap.put(mLCPersonTraceSet, "INSERT");
				try{
					VData v = new VData();
					v.add(tMMap);
					PubSubmit p = new PubSubmit();
					if(!p.submitData(v, "")){
						responseOME = buildResponseOME("01", "19", "生成共享标识数据失败！");
						LoginVerifyTool.writeLog(responseOME, batchno, messageType,
								sendDate, sendTime, "01", "19", "WX","生成共享标识数据失败!");
						System.out.println("responseOME:" + responseOME);
						return false;
					}
				}catch(Exception e){
					responseOME = buildResponseOME("01", "19", "生成共享标识数据失败！");
					System.out.println("生成共享标识数据失败！");
					e.printStackTrace();
					return false;
				}
			}
			return true;
		}
	
	private boolean creatMission(){
		//pad 人工核保完成       大连 人工核保完成，生成一条签单工作流       天津  录入完毕，生成一条待复核工作流
		MMap tempMMap = new MMap();
		if("pg".equals(this.mLCGrpContSchema.getCardFlag())||"dl".equals(this.mLCGrpContSchema.getCardFlag())||"dj".equals(this.mLCGrpContSchema.getCardFlag())){
			String tSQL1 = "update lcgrpcont set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
			String tSQL2 = "update lcgrppol set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
			String tSQL3 = "update lccont set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwoperator = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
			String tSQL4 = "update lcpol set approveflag = '9',approvecode = '"+mGlobalInput.Operator+"',approvedate = '"+theCurrentDate+"',approvetime = '"+theCurrentTime+"',uwcode = '"+mGlobalInput.Operator+"',uwflag = '9',uwdate = '"+theCurrentDate+"',uwtime = '"+theCurrentTime+"' where grpcontno = '"+mProposalGrpContNo+"' ";
			tempMMap.put(tSQL1, SysConst.UPDATE);
			tempMMap.put(tSQL2, SysConst.UPDATE);
			tempMMap.put(tSQL3, SysConst.UPDATE);
			tempMMap.put(tSQL4, SysConst.UPDATE);
			
			LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
			String prtSeq = PubFun1.CreateMaxNo("GPAYNOTICENO",mLCGrpContSchema.getPrtNo());
			tLOPRTManagerSchema.setPrtSeq(prtSeq);
			tLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
			tLOPRTManagerSchema.setOtherNoType("01");
			tLOPRTManagerSchema.setCode("57");
			tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
			tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
			tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
			tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
			tLOPRTManagerSchema.setPrtType("0");//前台打印
			tLOPRTManagerSchema.setStateFlag("0");
			tLOPRTManagerSchema.setMakeDate(theCurrentDate);
			tLOPRTManagerSchema.setMakeTime(theCurrentTime);
			
			tempMMap.put(tLOPRTManagerSchema, SysConst.INSERT);
		
			if(this.mLCGrpContSchema.getCardFlag().equals("dl")){
				String tMissionID = PubFun1.CreateMaxNo("MissionID", 20);
				String tSQL5 = "INSERT INTO lwmission VALUES ('"+tMissionID+"','1','0000000004','0000002006','1','"+mProposalGrpContNo+"','"+mLCGrpContSchema.getPrtNo()+"','02','"+mLCGrpContSchema.getManageCom()+"','"+mLCGrpContSchema.getAgentCode()+"','"+mLCGrpContSchema.getAgentGroup()+"','"+mLCGrpContSchema.getGrpName()+"','"+mLCGrpContSchema.getCValiDate()+"','"+mLCGrpContSchema.getAppntNo()+"','"+theCurrentDate+"','"+theCurrentTime+"',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'"+mGlobalInput.Operator+"','"+mGlobalInput.Operator+"','"+theCurrentDate+"','"+theCurrentTime+"','"+theCurrentDate+"','"+theCurrentTime+"',NULL,NULL,NULL,NULL) ";
				tempMMap.put(tSQL5, SysConst.INSERT);
			}
		}else if("tj".equals(this.mLCGrpContSchema.getCardFlag())){
			String tMissionID = PubFun1.CreateMaxNo("MissionID", 20);
			String tSQL6 = "INSERT INTO lwmission VALUES ('"+tMissionID+"','1','0000000004','0000002001','1','"+mProposalGrpContNo+"','"+mLCGrpContSchema.getPrtNo()+"','02','"+mLCGrpContSchema.getManageCom()+"','"+mLCGrpContSchema.getAgentCode()+"','"+mLCGrpContSchema.getAgentGroup()+"','"+mLCGrpContSchema.getGrpName()+"','"+mLCGrpContSchema.getCValiDate()+"','"+mLCGrpContSchema.getAppntNo()+"','"+theCurrentDate+"','"+theCurrentTime+"',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'"+mGlobalInput.Operator+"','"+mGlobalInput.Operator+"','"+theCurrentDate+"','"+theCurrentTime+"','"+theCurrentDate+"','"+theCurrentTime+"',NULL,NULL,NULL,NULL) ";
			tempMMap.put(tSQL6, SysConst.INSERT);
		}
		
        
		try{
			VData v = new VData();
			v.add(tempMMap);
			PubSubmit p = new PubSubmit();
			if(!p.submitData(v, "")){
				responseOME = buildResponseOME("01", "19", "生成核保数据失败！");
				LoginVerifyTool.writeLog(responseOME, batchno, messageType,
						sendDate, sendTime, "01", "19", "WX","生成核保数据失败!");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}catch(Exception e){
			responseOME = buildResponseOME("01", "19", "生成核保数据失败！");
			System.out.println("数据提交异常");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean AgentCode(String AgentCode,String state) {
		System.out.println("输入的agentcode="+AgentCode);
		boolean cheak = false;

		if(AgentCode!=null&&!"".equals(AgentCode)){
			//TODO
			//String querySql = "select groupagentcode from laagent where (agentcode ='"+AgentCode+"' and groupagentcode !='') or (groupagentcode ='"+AgentCode+"' and agentcode !='')";
			String querySql = "select agentcode from laagent where (agentcode ='"+AgentCode+"' and groupagentcode !='') or (groupagentcode ='"+AgentCode+"' and agentcode !='')";
			
			SSRS resultSSRS = new ExeSQL().execSQL(querySql);
			if(resultSSRS==null || resultSSRS.MaxRow==0){
				message = "业务员号码有误！";
				if("Y".equals(state)){
					cheak = false;
				}else if("N".equals(state)){
					cheak = true;
					result = AgentCode ;
				}
				return cheak ;
			}
			result = resultSSRS.GetText(1, 1);
			System.out.println("resultSSRS不为空，result: " + result);
		}else{
			if("Y".equals(state)){
				cheak = true;
			}else if("N".equals(state)){
				cheak = true;
			}
			result = AgentCode ;
		}
		System.out.println("输出的agentcode="+result);
		return cheak ;
	}
	//返回信息
	public String getMessage(){
	    return message;
	}
	//返回业务员代码
	public String getResult(){
	    return result;
	}
	
	public String diffdate(String start,String end){
		String a = "";
		String sql = "select 1 from dual where to_Date('"+start+"', 'YYYY-MM-DD')<=to_Date('"+end+"','YYYY-MM-DD')";
		SSRS res = new ExeSQL().execSQL(sql);
		if(res == null || res.getMaxRow()<1){
			a = "保单失效日期不应在保单生效日期之前!";
			return a;
		}
		return a;
	}
//	public boolean checkPayintv(){
//		if(this.mLCGrpContSchema.getPayIntv()==0){
//			return true;
//		}
//		FDate fDate = new FDate();
//	    Date startDate = fDate.getDate(this.mLCGrpContSchema.getCValiDate());
//	    Date endDate = fDate.getDate(this.mLCGrpContSchema.getCInValiDate());
//	    GregorianCalendar sCalendar = new GregorianCalendar();
//        sCalendar.setTime(startDate);
//        int sYears = sCalendar.get(Calendar.YEAR);
//        int sMonths = sCalendar.get(Calendar.MONTH);
//        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
//	    GregorianCalendar eCalendar = new GregorianCalendar();
//        eCalendar.setTime(endDate);
//        eCalendar.add(Calendar.DAY_OF_MONTH, 1);
//        int eYears = eCalendar.get(Calendar.YEAR);
//        int eMonths = eCalendar.get(Calendar.MONTH);
//        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
//        if(sMonths==eMonths && sDays==eDays){
//        	return true;
//        }
//        eCalendar.add(Calendar.DAY_OF_MONTH, -1);
//        if(sMonths==eMonths && sMonths==1 && sDays==29 && eCalendar.get(Calendar.DAY_OF_MONTH)==28){
//        	return true;
//        }
//		
//	}
	public boolean checkPeoples(){
//		if ( this.mLCGrpContSchema.getPeoples()==null || this.mLCGrpContSchema.getPeoples()=="") {
//			this.mLCGrpContSchema.setPeoples(0);
//		}
//		if ( this.mLCGrpContSchema.getOnWorkPeoples()==null || this.mLCGrpContSchema.getOnWorkPeoples()=="") {
//			this.mLCGrpContSchema.setOnWorkPeoples(0);
//		}
//		if ( this.mLCGrpContSchema.getOffWorkPeoples()==null || this.mLCGrpContSchema.getOffWorkPeoples()=="") {
//			this.mLCGrpContSchema.setOnWorkPeoples(0);
//		}
//		if ( this.mLCGrpContSchema.getOtherPeoples()==null || this.mLCGrpContSchema.getOtherPeoples()=="") {
//			this.mLCGrpContSchema.setOtherPeoples(0);
//		}
		int intPeoples=this.mLCGrpAppntSchema.getPeoples();
		int intAppntOnWorkPeoples=this.mLCGrpAppntSchema.getOnWorkPeoples();
		int intAppntOffWorkPeoples=this.mLCGrpAppntSchema.getOffWorkPeoples();
		int intAppntOtherPeoples=this.mLCGrpAppntSchema.getOtherPeoples();

		if (intPeoples!=intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples) {
			return false;
		}
		return true;
	}
	public String checkDate(){
		String str ="";
		String handlerDate = this.mLCGrpContSchema.getHandlerDate();
		String receiveDate = this.mLCGrpContSchema.getReceiveDate();
		String tCurDate = theCurrentDate;
		FDate fd = new FDate();
        Date d1 = fd.getDate(handlerDate);
        Date d2 = fd.getDate(receiveDate);
        Date d3 = fd.getDate(tCurDate);
        if(d3.before(d2)){
        	str = "接收日期应早于或等于当天!";
        }
        if(d2.before(d1)){
        	str = "投保单填写日期应该早于或等于接收日期!";
        }
        return str;
	}
	public boolean checkAgentCom(){
			String tSaleChnl = this.mLCGrpContSchema.getSaleChnl();
			String tAgentCom = this.mLCGrpContSchema.getAgentCom();
			
			if(tSaleChnl != null && tSaleChnl != "" && tSaleChnl == "03")
			{
				if(tAgentCom != null && tAgentCom != "")
				{
					String strSQL = "select actype from lacom where agentcom = '" + tAgentCom + "' ";
					SSRS arrResult = new ExeSQL().execSQL(strSQL);
		    		if(arrResult != null && "05".equals(arrResult.GetText(1, 1)))
		    		{
		    			return false;
		    		}
				}
			}
			return true;
		
	}
	public String chkOccupation(String nativePlace,String occupationcode,String occupationtype,String nativeCity,String idtype,String idno,String flag){
		String str = "";
		if("HK".equals(nativePlace)||"OS".endsWith(nativePlace)){
			if(nativeCity==null || nativeCity==""){
				str = "国籍为港澳台人士、外籍人士时，国家/地区不能为空！";
				return str;
			}
			String strsql = "select 1 from ldcode where codetype='nativecity' and code='"+nativeCity+"' with ur ";
			SSRS res = new ExeSQL().execSQL(strsql);
			if(res.getMaxRow()<1){
				str = "获取国家/地区失败！";
				return str;
			}
			String sql = "select 1 from ldcode where codetype='nativecity' and comcode='"+nativePlace+"' and code='"+nativeCity+"' with ur ";
			SSRS arrResult = new ExeSQL().execSQL(sql);
			if(arrResult.getMaxRow()<1){
				str = "国籍与国家/地区不匹配！";
				return str;
			}
		}
		if("a".equals(idtype)||"b".equals(idtype)){
			if(!"HK".equals(nativePlace)){
				str = "国籍与证件类型不匹配！";
				return str;
			}
			if("a".equals(idtype)){
				if(!"1".equals(nativeCity)&&!"2".equals(nativeCity)){
					str = "证件类型与地区不匹配！";
					return str;
				}
			}
			if("b".equals(idtype)){
				if(!"3".equals(nativeCity)){
					str = "证件类型与地区不匹配！";
					return str;
				}
			}
			if("1".equals(nativeCity)){
				if(idno.indexOf("810000")!=0){
					str = "证件号码与地区不匹配！";
					return str;
				}
			}
			if("2".equals(nativeCity)){
				if(idno.indexOf("820000")!=0){
					str = "证件号码与地区不匹配！";
					return str;
				}
			}
			if("3".equals(nativeCity)){
				if(idno.indexOf("830000")!=0){
					str = "证件号码与地区不匹配！";
					return str;
				}
			}
			if("1".equals(flag)){
				FDate fDate = new FDate();
				Date startDate = fDate.getDate(this.mLCGrpAppntSchema.getIDStartDate());
				Date endDate = fDate.getDate(this.mLCGrpAppntSchema.getIDEndDate());
				GregorianCalendar sCalendar = new GregorianCalendar();
				sCalendar.setTime(startDate);
				int sYears = sCalendar.get(Calendar.YEAR);
				GregorianCalendar eCalendar = new GregorianCalendar();
				eCalendar.setTime(endDate);
				int eYears = eCalendar.get(Calendar.YEAR);
				if("Y".equals(this.mLCGrpAppntSchema.getIDLongEffFlag())||((eYears-sYears)!=5)){
					str = "证件类型为港澳台居民居住证时，证件类型有效期必须为5年!";
					return str;
				}
			}
		}
		if("2".equals(flag)){
			String sql = "select 1 from LDOccupation where occupationcode='"+occupationcode+"' and OccupationType='"+occupationtype+"' with ur";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if(tSSRS==null ||tSSRS.getMaxRow()<=0){
				str = "职业代码与职业类别不匹配!";
				return str;
			}
		}
		return str;
	}
	public boolean checkPeople(){
		String tSqlCount="select 1 from dual where  (select a.peoples2 - b.code from lcgrpcont a,ldcode b where prtno='"
				 +this.mLCGrpContSchema.getPrtNo()+"' and codetype ='checkpeople')<0";
		SSRS arrCount = new ExeSQL().execSQL(tSqlCount);
		if(arrCount.MaxRow>0){
			return false;
		}
		return true;
	}
	
	public boolean checkGrpName(){
		String insuredProp = this.mLCGrpAppntSchema.getInsuredProperty();
		String grpName = this.mLCGrpContSchema.getGrpName();
		//法人
		String customerno = "";
		boolean b = false;
		if(this.mLCGrpAppntSchema.getInsuredProperty().equals("1")){
			//组织机构代码
			String organComCode = this.mLCGrpAppntSchema.getOrganComCode();
			//统一社会信用代码
			String unifiedSocialCreditNo = this.mLCGrpAppntSchema.getUnifiedSocialCreditNo();
			if(organComCode!=null && !"".equals(organComCode)){
				if(unifiedSocialCreditNo!=null && !"".equals(unifiedSocialCreditNo)){
					String sql1 = "select customerno from ldgrp where grpname='"+grpName+"' and organcomcode='"+organComCode+"'";
					SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
					if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
						String sql2 = "select customerno from ldgrp where grpname='"+grpName+"' and unifiedsocialcreditno='"+unifiedSocialCreditNo+"'";
						SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
						if(tSSRS2 == null || tSSRS2.getMaxRow()<=0){
							b = true;
						}else{
							customerno = tSSRS2.GetText(1, 1);
						}
					}else{
						customerno = tSSRS1.GetText(1, 1);
					}
					
					
				}else{
					String sql1 = "select customerno from ldgrp where grpname='"+grpName+"' and organcomcode='"+organComCode+"'";
					SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
					if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
						b = true;
					}else{
						customerno = tSSRS1.GetText(1, 1);
					}
				}
			}else{
				String sql1 = "select customerno from ldgrp where grpname='"+grpName+"' and unifiedsocialcreditno='"+unifiedSocialCreditNo+"'";
				SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
				if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
					b = true;
				}else{
					customerno = tSSRS1.GetText(1, 1);
				}
			}
		}else if(this.mLCGrpAppntSchema.getInsuredProperty().equals("2")){//自然人
			String linkManName = this.mLCGrpAddressSchema.getLinkMan1();
			if(!grpName.equals(linkManName)){
				responseOME = buildResponseOME("01","17", "投保人属性为自然人时，投保人名称和联系人姓名不同，请检查！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "投保人属性为自然人时，投保人名称和联系人姓名不同，请检查！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}else{
				String sql1 = "select customerno from ldgrp where grpname='"+grpName+"'";
				SSRS tSSRS1 = new ExeSQL().execSQL(sql1);
				if(tSSRS1 == null || tSSRS1.getMaxRow()<=0){
					b = true;
				}else{
					boolean c = false;
					String custno = "";
					for(int i=1;i<=tSSRS1.getMaxRow();i++){
						String sql2 = "select grpcontno from lcgrpcont where appntno='"+tSSRS1.GetText(i, 1)+"'";
						SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
						if(tSSRS2 != null || tSSRS2.getMaxRow()>0){
							for(int j=1;j<=tSSRS2.getMaxRow();j++){
								String sql3 = "select a.customerno,b.linkman1,a.idtype,a.idno from lcgrpappnt a,lcgrpaddress b,lcgrpcont c where a.grpcontno=c.grpcontno and a.prtno=c.prtno and a.addressno=c.addressno and a.customerno=c.appntno and b.customerno=c.appntno and b.addressno=c.addressno and c.grpcontno ='"+tSSRS2.GetText(j, 1)+"' "
										+ "union "
										+ "select a.customerno,b.linkman1,a.idtype,a.idno from lbgrpappnt a,lcgrpaddress b,lbgrpcont c where a.grpcontno=c.grpcontno and a.prtno=c.prtno and a.addressno=c.addressno and a.customerno=c.appntno and b.customerno=c.appntno and b.addressno=c.addressno and c.grpcontno ='"+tSSRS2.GetText(j, 1)+"' with ur";
								SSRS tSSRS3 = new ExeSQL().execSQL(sql3);
								if(tSSRS3 != null || tSSRS3.getMaxRow()>0){
									for(int z=1;z<=tSSRS3.getMaxRow();z++){
										if(this.mLCGrpAddressSchema.getLinkMan1().equals(tSSRS3.GetText(z, 2)) && this.mLCGrpAppntSchema.getIDType().equals(tSSRS3.GetText(z, 3)) && this.mLCGrpAppntSchema.getIDNo().equals(tSSRS3.GetText(z, 4))){
											c = true;
											custno = tSSRS3.GetText(z, 1);
										}
									}
								}
							}
						}
					}
					if(c){
						customerno = custno;
					}else{
						b = true;
					}
				}
			}
		}
		if(b){
			customerflag = true;
			//新建客户
			String tLimit = "SN";
	        String grpNo = PubFun1.CreateMaxNo("GRPNO", tLimit);
	        if("".equals(grpNo)){
	        	responseOME = buildResponseOME("01","17", "客户号码生成失败！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "客户号码生成失败！");
				System.out.println("responseOME:" + responseOME);
				return false;
	        }
			this.mLCGrpContSchema.setAppntNo(grpNo);
			this.mLCGrpAppntSchema.setCustomerNo(grpNo);
			this.mLDGrpSchema.setCustomerNo(grpNo);
			this.mLCGrpAddressSchema.setCustomerNo(grpNo);
		}else{
			//使用历史客户
			this.mLCGrpContSchema.setAppntNo(customerno);
			this.mLCGrpAppntSchema.setCustomerNo(customerno);
			this.mLDGrpSchema.setCustomerNo(customerno);
			this.mLCGrpAddressSchema.setCustomerNo(customerno);
		}
		return true;
	}
	
	private boolean checkRiskInfo(){
		if(this.mLCGrpContSchema.getCardFlag().equals("pg")){
			return true;
		}
//		for(int i=1;i<=this.mLCGrpPolSet.size();i++){
//			String riskid = this.mLCGrpPolSet.get(i).getKindCode();
//			String riskcode = this.mLCGrpPolSet.get(i).getRiskCode();
//			String mainriskid = this.mLCGrpPolSet.get(i).getRiskVersion();
//			
//		}
		return true;
	}
	
	private boolean check690101(){
		//判断是否要生成公共账户信息
		boolean b = false;
		for(int i=1;i<=this.mLCGrpPolSet.size();i++){
			if("690101".equals(this.mLCGrpPolSet.get(i).getRiskCode())){
				b=true;
			}
		}
		return b;
	}
	
	//单独承保校验、
	private int check162601(){
		boolean b = false;
		int k=-1;//k=0 正常的162601、k=1 正常的其他险种、k=2 海外医疗非单独承保
		for(int i=1;i<=this.mLCGrpPolSet.size();i++){
			String sql = "select 1 from ldcode where codetype='jxhwpad' and code='"+this.mLCGrpPolSet.get(i).getRiskCode()+"' with ur ";//162601  162801
			SSRS sqlres = new ExeSQL().execSQL(sql);
			if(sqlres.getMaxRow()>0){
				b = true;
			}
		}
		if(b){//是海外医疗、判断单独承保
			if(this.mLCGrpPolSet.size()!=1){
				k = 2;
			}else{
				k=0;
			}
		}else{//不是海外医疗、将lccontplandutyparam的值删了
			k=1;
		}
		return k;
	}
	
	private boolean check163002(){//单个险种、主险、单个险种、附加险、多个险种包含主险、多个险种包含附加险、两个险种同时包含主险附加险
		String sql = "select 1 from lcgrppol where prtno='"+this.mLCGrpContSchema.getPrtNo()+"' and riskcode in ('163002') ";
		SSRS ssrs = new ExeSQL().execSQL(sql);
		if(ssrs.getMaxRow()!=0){
			return true;
		}
		return false;
	}
	//生成一带一路信息
	private boolean creatRoad(){
		String tEngineeringFactor=null;
		String tprtno = mLCGrpContRoadSchema.getPrtNo();
		String tCountry = mLCGrpContRoadSchema.getCountry();
		String tCountryCategory = mLCGrpContRoadSchema.getCountryCategory();
		String tEngineeringFlag = mLCGrpContRoadSchema.getEngineeringFlag();
		String tEngineeringCategory = mLCGrpContRoadSchema
				.getEngineeringCategory();
		int day = 0;
		String tinsureyear="";
		// 根据国家查询算费因子
		System.out.println("a:"+tCountry);
		System.out.println("b:"+tEngineeringCategory);
		//根据国家类型查询算费因子
		String sqlCountry = "select code,codealias from ldcode where codetype='country' and code='"
				+ tCountryCategory + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sqlCountry);
		if (tSSRS.getMaxRow() <= 0) {
			responseOME = buildResponseOME("01","17", "录入国家查询失败！");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "录入国家查询失败！");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		// 国家类别及国家系数
		String tCountryFactor = tSSRS.GetText(1, 2);

		if(tEngineeringCategory==null||"".equals(tEngineeringCategory)){
			tEngineeringFactor="1";
			tEngineeringCategory=null;
		}else{
			String sqlengineering = "select codealias from ldcode where codetype='engineering' and code='"
					+ tEngineeringCategory + "'";
			tSSRS = tExeSQL.execSQL(sqlengineering);
			if (tSSRS.getMaxRow() <= 0) {
				responseOME = buildResponseOME("01","17", "录入工程查询失败！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "录入工程查询失败！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			// 工程类别及工程系数
			tEngineeringFactor = tSSRS.GetText(1, 1);
		}
		
		day = PubFun.calInterval(this.mLCGrpContSchema.getCValiDate(), this.mLCGrpContSchema.getCInValiDate(), "D")+1;
		String sql = "select case when "+day+" <= 15 then "
			       + "case when "+day+" <= 7 then 0.10 when "+day+" >= 8 and "+day+" <= 15 then 0.15 end "
			       + "when "+day+" > 15 then "
			       + "case (select monthdiff('"+this.mLCGrpContSchema.getCInValiDate()+"', '"+this.mLCGrpContSchema.getCValiDate()+"') from dual) "
			       + "when 1 then 0.25 "
			       + "when 2 then 0.35 "
			       + "when 3 then 0.45 "
			       + "when 4 then 0.55 "
			       + "when 5 then 0.65 "
			       + "when 6 then 0.70 "
			       + "when 7 then 0.75 "
			       + "when 8 then 0.80 "
			       + "when 9 then 0.85 "
			       + "when 10 then 0.90 "
			       + "when 11 then 0.95 "
			       + "when 12 then 1.00 end end "
			       + "from dual where 1=1 ";
		tSSRS = tExeSQL.execSQL(sql);
		if(tSSRS.getMaxRow()> 0){
			tinsureyear=tSSRS.GetText(1, 1);
		}
		mLCGrpContRoadSchema.setCountryCategory(tCountryCategory);
		mLCGrpContRoadSchema.setCountryFactor(tCountryFactor);
		mLCGrpContRoadSchema.setEngineeringFactor(tEngineeringFactor);
		mLCGrpContRoadSchema.setInsureyear(tinsureyear);
		mLCGrpContRoadSchema.setMakeDate(theCurrentDate);
		mLCGrpContRoadSchema.setMakeTime(theCurrentTime);
		mLCGrpContRoadSchema.setModifyDate(theCurrentDate);
		mLCGrpContRoadSchema.setModifyTime(theCurrentTime);
		
		float sumage = 0;
		for(int i=1;i<=this.mLCInsuredListSet.size();i++){
			String ttsql1 = "select sum(yeardiff('"+this.mLCGrpContSchema.getCValiDate()+"','"+this.mLCInsuredListSet.get(i).getBirthday()+"')) from dual where 1=1 ";
			SSRS ssrs1 = new ExeSQL().execSQL(ttsql1);
			if(ssrs1.getMaxRow()!=0){
				sumage += Float.valueOf(ssrs1.GetText(1, 1));
			}
		}
		float count = Float.valueOf(this.mLCInsuredListSet.size());
		System.out.println("总年龄："+sumage);
		float avage = sumage / count;
		DecimalFormat fnum = new DecimalFormat("##0.00");
		String tavage = fnum.format(avage);
		System.out.println("平均年龄："+tavage);
		mLCGrpContRoadSchema.setAvage(tavage);
		mLCGrpContRoadSchema.setScaleFactor(String.valueOf(this.mLCInsuredListSet.size()));
		
		MMap mMMap = new MMap();
		mMMap.put(mLCGrpContRoadSchema, SysConst.DELETE_AND_INSERT);
        boolean b = save(mMMap);
		return b;
	}
	
	private boolean checkTotalFactor(){
		String tSqlRisk = "select 1 from LCGrpPol where PrtNo='" + this.mLCGrpContSchema.getPrtNo() + "' and riskcode='163002' ";
		SSRS ssrs = new ExeSQL().execSQL(tSqlRisk);
		if(ssrs.getMaxRow()!=0){
			String tSql = "select replace(TotalFactor,'%',''),TotalFactor from LCGrpcontroad where PrtNo='" + this.mLCGrpContSchema.getPrtNo() + "'";
			SSRS ssrs1 = new ExeSQL().execSQL(tSql);
			if(ssrs1.getMaxRow()==0){
				responseOME = buildResponseOME("01","17", "获取一带一路产品要素失败！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "获取一带一路产品要素失败！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}else{
				double tTotalFactor = Double.parseDouble(ssrs1.GetText(1, 1));
				String tTotal = ssrs1.GetText(1, 2);
				//获取浮动比例的最大值和最小值
				String tSQL = "select code,codename from ldcode where codetype ='163002'";
				SSRS arrssrs = new ExeSQL().execSQL(tSQL);
				double min=Double.parseDouble(arrssrs.GetText(1, 1));
				double max=Double.parseDouble(arrssrs.GetText(1, 2));
				if(tTotalFactor < min ||tTotalFactor > max){
					responseOME = buildResponseOME("01","17", "一带一路产品的浮动比例不得小于"+min+"%且不得大于"+max+"%，当前浮动比例为:"+tTotal);
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "一带一路产品的浮动比例不得小于"+min+"%且不得大于"+max+"%，当前浮动比例为:"+tTotal);
					System.out.println("responseOME:" + responseOME);
					return false;
				}
			}
		}
		return true;
	}
	
	//保障计划保费生成
	private String calculatePrem(String contplancode,String sumprem){
//
	      String tsumprem = sumprem;
	      String divprem = "";
	      String chkDivSum = "";
	      double chkSum = 0;
	      if("".equals(tsumprem)){
	        divprem = "";
	      }else{
	    	int people = 0;
	    	for(int i=1;i<=mLCContPlanSet.size();i++){
	    		if(contplancode.equals(mLCContPlanSet.get(i).getContPlanCode())){
	    			people = mLCContPlanSet.get(i).getPeoples2();
	    		}
	    	}
	      	if(people==0){
	      		people=1;
	      	}
	        divprem=String.valueOf((floatRound(accDiv(tsumprem,people),5)));
	        divprem=String.valueOf((Math.floor(floatRound(Double.valueOf(divprem)*100,5))/100));
	        chkDivSum = divprem;
	      }//每个人多少钱
//	      for(var n=0;n<ContPlanGrid.mulLineCount;n++){
//	        if(ContPlanGrid.getRowColData(n,6)=="保费"){
//	          sumarray[sumarraycount]=n;
//	          sumarraycount++;
//	        }
//	      }
//	      sumarray[0]=1;
//	      divprem=String(divprem/sumarray.length);//每个责任多少钱
//	      divprem=String(Math.floor(divprem*100)/100);
	      //处理误差问题，如果拆分责任时出现误差问题，由一个责任承担误差
//	      var chkSum = divprem*sumarray.length;
//	      var chkSub =((chkDivSum*100) - (chkSum*100))/100;
//	      for(var m=0;m<sumarray.length;m++){
//	        if(m==sumarray.length-1){
//	          if(chkSub!=0){
//	           var StringSub = floatRound(parseFloat((parseFloat(divprem)+parseFloat(chkSub))*100,5)/100,3);
//	            //alert((parseFloat(divprem)+parseFloat(chkSub)));
//	            StringSub +="";
//	            ContPlanGrid.setRowColData(sumarray[m],8,StringSub);
//	          }else{
//	            ContPlanGrid.setRowColData(sumarray[m],8,divprem);
//	          }
//	        }else{
//	          ContPlanGrid.setRowColData(sumarray[m],8,divprem);
//	        }
//	      }
	    
		return divprem;
	}
	
	//tsumprem,people  3333.33  3  t1=2  t2=
	private double accDiv(String arg1,int arg2){
	    int t1=0,t2=0;
	    double r1,r2;
	    if(arg1.indexOf(".")!=-1){
	    	t1 = arg1.substring(arg1.indexOf(".")+1).length();System.out.println(t1);
	    	r1 = Double.valueOf(arg1.toString().replace(".",""));
	    }else{//123123
	    	r1 = Double.valueOf(arg1);
	    }
	    r2=Double.valueOf(arg2);
	    return (r1/r2)*Math.pow(10,t2-t1);
	}
	
	//
	private double floatRound(double myFloat,int mfNumber){
	  double cutNumber = Math.pow(10,mfNumber-1);
	  return Math.round(myFloat * cutNumber)/cutNumber;
	}
	
	private boolean createdeDefaultContplan(){
		LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
        tLCContPlanSchema.setGrpContNo(mLCGrpContSchema.
                getGrpContNo());
        tLCContPlanSchema.setProposalGrpContNo(mLCGrpContSchema.
                getGrpContNo());
        tLCContPlanSchema.setContPlanName("默认计划");
        tLCContPlanSchema.setContPlanCode("11");
        tLCContPlanSchema.setPlanType("0");
        tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCContPlanSchema);
        
        MMap mMMap = new MMap();
        mMMap.put(tLCContPlanSchema, "INSERT");
        boolean b = save(mMMap);
		return b;
	}
	
	private String getRiskVersion(String tRiskCode) {
        String sql = "select riskver from lmrisk where riskcode = '"+tRiskCode+"'";
        SSRS ssrs = new ExeSQL().execSQL(sql);
        if(ssrs.getMaxRow()>0){
        	return ssrs.GetText(1, 1);
        }else{
        	return "2002";
        }
    }
	
	private boolean checkjgx(){
		String provinceID="";
		String cityID="";
		String countyID="";
		String detailAddress="";
		String projectAddress="";
		String projectName="";
		String projectNo="";
		String standbyFlag1="";
		String standbyFlag2="";
		String standbyFlag3="";
		for(int i=1;i<=mLCContPlanDutyParamSetjgx.size();i++){
			if("ProjectAddress".equals(mLCContPlanDutyParamSetjgx.get(i).getCalFactor())){
				projectAddress=mLCContPlanDutyParamSetjgx.get(i).getCalFactorValue();
				continue;
			}
			if("ProjectName".equals(mLCContPlanDutyParamSetjgx.get(i).getCalFactor())){
				projectName=mLCContPlanDutyParamSetjgx.get(i).getCalFactorValue();
				continue;		
			}
			if("ProjectNo".equals(mLCContPlanDutyParamSetjgx.get(i).getCalFactor())){
				projectNo=mLCContPlanDutyParamSetjgx.get(i).getCalFactorValue();
				continue;
			}
			if("StandbyFlag1".equals(mLCContPlanDutyParamSetjgx.get(i).getCalFactor())){
				standbyFlag1=mLCContPlanDutyParamSetjgx.get(i).getCalFactorValue();
				continue;
			}
			if("StandbyFlag2".equals(mLCContPlanDutyParamSetjgx.get(i).getCalFactor())){
				standbyFlag2=mLCContPlanDutyParamSetjgx.get(i).getCalFactorValue();
				continue;
			}
			if("StandbyFlag3".equals(mLCContPlanDutyParamSetjgx.get(i).getCalFactor())){
				standbyFlag3=mLCContPlanDutyParamSetjgx.get(i).getCalFactorValue();
				continue;
			}
		}
		provinceID = mLCGrpContSubSchemajgx.getbak2();
		cityID = mLCGrpContSubSchemajgx.getbak3();
		countyID = mLCGrpContSubSchemajgx.getbak4();
		detailAddress = mLCGrpContSubSchemajgx.getbak5();
		//校验判断联系人地址是否为空
		if ("".equals(provinceID) || "".equals(cityID) || "".equals(countyID) || "".equals(detailAddress)) {
			responseOME = buildResponseOME("01","17", "建工险要素中的省，市，县和详细地址不能为空，请核实！");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "建工险要素中的省，市，县和详细地址不能为空，请核实！");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		
		//校验省和市是否与匹配
		String strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
		String strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
		String strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
		String strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
		SSRS arrResult = new ExeSQL().execSQL(strSql);
		SSRS arrResult2 = new ExeSQL().execSQL(strSql2);
		SSRS arrResult3 = new ExeSQL().execSQL(strSql3);
		SSRS arrResult4 = new ExeSQL().execSQL(strSql4);
		
		if (provinceID=="710000" || provinceID=="810000" || provinceID=="820000"||provinceID=="990000") {
			if(cityID != "000000"){
	 			responseOME = buildResponseOME("01","17", "省和市不匹配，请核实！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "省和市不匹配，请核实！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
			if(countyID != "000000"){
				responseOME = buildResponseOME("01","17", "市和县不匹配，请核实！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "市和县不匹配，请核实！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}

		}else if ((provinceID=="620000"&&cityID=="620200") || (provinceID=="440000"&&(cityID=="442000"||cityID=="441900")) ) {
			if(countyID != "000000"){
				responseOME = buildResponseOME("01","17", "市和县不匹配，请核实！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "市和县不匹配，请核实！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}else{
			if(arrResult.getMaxRow()>0&&arrResult2.getMaxRow()>0&&arrResult3.getMaxRow()>0&&arrResult4.getMaxRow()>0){
				if (!arrResult.GetText(1, 1).equals(arrResult2.GetText(1, 1))) {
					responseOME = buildResponseOME("01","17", "省和市不匹配，请核实！");
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "省和市不匹配，请核实！");
					System.out.println("responseOME:" + responseOME);
					return false;
				}
				if (!arrResult3.GetText(1, 1).equals(arrResult4.GetText(1, 1))) {
					responseOME = buildResponseOME("01","17", "市和县不匹配，请核实！");
					LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "市和县不匹配，请核实！");
					System.out.println("responseOME:" + responseOME);
					return false;
				}
			}else{
				responseOME = buildResponseOME("01","17", "省、市、县编码有误，请核实！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "省、市、县编码有误，请核实！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if("".equals(projectAddress)){
			responseOME = buildResponseOME("01","17", "施工地址未录入！");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "施工地址未录入！");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if("".equals(projectName)){
			responseOME = buildResponseOME("01","17", "项目名称未录入！");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "项目名称未录入！");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if("".equals(standbyFlag1)){
			responseOME = buildResponseOME("01","17", "保费计算方式未录入！");
			LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "保费计算方式未录入！");
			System.out.println("responseOME:" + responseOME);
			return false;
		}
		if("1".equals(standbyFlag1)){
			if("".equals(standbyFlag2)){
				responseOME = buildResponseOME("01","17", "保费计算方式为：按承包合同价计算，施工承包合同价必须录入！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "保费计算方式为：按承包合同价计算，施工承包合同价必须录入！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		if("2".equals(standbyFlag1)){
			if("".equals(standbyFlag3)){
				responseOME = buildResponseOME("01","17", "保费计算方式为：按建筑面积计算，施工建筑总面积必须录入！");
				LoginVerifyTool.writeLog(responseOME, batchno,messageType, sendDate, sendTime, "01","17", "WX", "保费计算方式为：按建筑面积计算，施工建筑总面积必须录入！");
				System.out.println("responseOME:" + responseOME);
				return false;
			}
		}
		return true;
	}
}
