package com.sinosoft.wasforwardxml.project.pad.tb;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.schema.LCContPrintSchema;
import com.sinosoft.lis.schema.LCContReceiveSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DealGrpPadPrintInfo {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 全局数据 */
    private String mGrpContNo = "";

    private VData mInputData;
    
    private VData mResult = new VData();

    private MMap map = new MMap();
    
    private String mPrtNo;
    
    private String mPrintDate;
    
    private String mPrintTime;
    
    private String mPrintCount;

    private String mCvalidate;

    private String mManageCom;

    private String mdate;

    private String mtime;

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private TransferData mTransferData;
    
    public DealGrpPadPrintInfo()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;

        if(!getInputData()){
        	return false;
        }
        if (!deal())
        {
            return false;
        }
        mResult.add(map);
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(mResult, cOperate)) {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        System.out.println("---End pubsubmit---");
        return true;
    }

    /**
     * 打印操作
     * @param mInputData VData
     * @return boolean
     */
    private boolean deal()
    {
        System.out.println("Start Save...");
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();
        try
        {
            System.out.println("Start ....");
            // 获取要保存的数据
            //更新LCGrpCont表数据
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setPrtNo(mPrtNo);
            LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
            mLCGrpContSchema = tLCGrpContSet.get(1).getSchema();
            mGrpContNo = mLCGrpContSchema.getGrpContNo();
            mCvalidate = mLCGrpContSchema.getCValiDate();
            mManageCom = mLCGrpContSchema.getManageCom();
            mLCGrpContSchema.setPrintCount(1);//设置团单层默认打印次数为1
            mLCGrpContSchema.setModifyDate(mdate);
            mLCGrpContSchema.setModifyTime(mtime);
            map.put(mLCGrpContSchema, "UPDATE");
            
            dealprint();

            dealreceive();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DealGrpPadPrintInfo";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
        }
        return true;
    }

    /**
     * 处理合同接收
     */
    private void dealreceive()
    {
        System.out.println("in dealreceive");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentSet tLAAgentSet = tLAAgentDB.executeQuery("select * from laagent where agentcode='"
        		+ mLCGrpContSchema.getAgentCode()
        		+ "'");
        tLAAgentSchema = tLAAgentSet.get(1);
        String tAgentName = tLAAgentSchema.getName();

        LCContReceiveSchema tLCContReceiveSchema = new LCContReceiveSchema();
        tLCContReceiveSchema.setReceiveID(1);
        tLCContReceiveSchema.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContReceiveSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCContReceiveSchema.setContType("2");
        tLCContReceiveSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
        tLCContReceiveSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLCContReceiveSchema.setAppntName(mLCGrpContSchema.getGrpName());
        tLCContReceiveSchema.setPrem(mLCGrpContSchema.getPrem());
        tLCContReceiveSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCContReceiveSchema.setAgentName(tAgentName);
        tLCContReceiveSchema.setSignDate(mLCGrpContSchema.getSignDate());
        tLCContReceiveSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCContReceiveSchema.setPrintManageCom(mLCGrpContSchema.getManageCom());
        tLCContReceiveSchema.setPrintOperator("PAD");
        tLCContReceiveSchema.setPrintDate(mPrintDate);
        tLCContReceiveSchema.setPrintTime(mPrintTime);
        tLCContReceiveSchema.setPrintCount(mPrintCount);
        tLCContReceiveSchema.setReceiveState("1");
        tLCContReceiveSchema.setDealState("0");
        tLCContReceiveSchema.setMakeDate(mdate);
        tLCContReceiveSchema.setMakeTime(mtime);
        tLCContReceiveSchema.setModifyDate(mdate);
        tLCContReceiveSchema.setModifyTime(mtime);
        tLCContReceiveSchema.setReceiveManageCom(mLCGrpContSchema.getManageCom());
        tLCContReceiveSchema.setReceiveOperator("PAD");
        tLCContReceiveSchema.setReceiveDate(mPrintDate);
        tLCContReceiveSchema.setReceiveTime(mPrintTime);
        
        LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
        tLCContGetPolSchema.setContNo(mLCGrpContSchema.getGrpContNo());
        tLCContGetPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCContGetPolSchema.setContType("2");
        tLCContGetPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
        tLCContGetPolSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLCContGetPolSchema.setAppntName(mLCGrpContSchema.getGrpName());
        tLCContGetPolSchema.setPrem(mLCGrpContSchema.getPrem());
        tLCContGetPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCContGetPolSchema.setAgentName(tAgentName);
        tLCContGetPolSchema.setSignDate(mLCGrpContSchema.getSignDate());
        tLCContGetPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCContGetPolSchema.setReceiveManageCom(mManageCom);
        tLCContGetPolSchema.setReceiveOperator("PAD");
        tLCContGetPolSchema.setReceiveDate(mPrintDate);
        tLCContGetPolSchema.setReceiveTime(mPrintTime);
        tLCContGetPolSchema.setGetpolState("0");
        tLCContGetPolSchema.setMakeDate(mdate);
        tLCContGetPolSchema.setMakeTime(mtime);
        tLCContGetPolSchema.setModifyDate(mdate);
        tLCContGetPolSchema.setModifyTime(mtime);

        map.put(tLCContReceiveSchema, "DELETE&INSERT");
        map.put(tLCContGetPolSchema, "DELETE&INSERT");
    }
    //获取数据
    public boolean getInputData(){
    	if (this.mInputData == null) {
			buildError("getInputData", "传入信息为空,程序有错,找程序员处理！");
			return false;
		}
		this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		if (this.mTransferData == null) {
			buildError("getInputData", "传入的参数信息为null！");
			return false;
		}
		mPrtNo=(String) mTransferData.getValueByName("PrtNo");
		mPrintDate=(String) mTransferData.getValueByName("PrintDate");
		mPrintTime=(String) mTransferData.getValueByName("PrintTime");
		mPrintCount=(String) mTransferData.getValueByName("PrintCount");
    	return true;
    }

    /*
     * 插入LCContPrint表数据
     */
    public void dealprint(){
        LCContPrintSchema tLCContPrintSchema = new LCContPrintSchema();
        System.out.println("开始放入xml");
        tLCContPrintSchema.setPrintID(PubFun1.CreateMaxNo("PrintID", 11));
        System.out.println("PrintID="+tLCContPrintSchema.getPrintID());
        tLCContPrintSchema.setPrtNo(mPrtNo);
        tLCContPrintSchema.setOtherNo(mGrpContNo);
        tLCContPrintSchema.setOtherNoType("2");
        tLCContPrintSchema.setPrintCount(mPrintCount);
        tLCContPrintSchema.setFilePath("printdata/data/" + mGrpContNo + ".xml");
        System.out.println(tLCContPrintSchema.getFilePath());
        tLCContPrintSchema.setCValiDate(mCvalidate);
        tLCContPrintSchema.setManageCom(mManageCom);
        tLCContPrintSchema.setOperator("PAD");
        tLCContPrintSchema.setMakeDate(mdate);
        tLCContPrintSchema.setMakeTime(mtime);
        tLCContPrintSchema.setModifyDate(mdate);
        tLCContPrintSchema.setModifyTime(mtime);
        tLCContPrintSchema.setDownloadCount(1);
        tLCContPrintSchema.setDownloadDate(mPrintDate);
        tLCContPrintSchema.setDownloadTime(mPrintTime);
        tLCContPrintSchema.setDownOperator("PAD");
        map.put(tLCContPrintSchema,"DELETE&INSERT");
    }
    /**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PreviewPrintAfterInitService";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.println("程序报错：" + cError.errorMessage);
	}
}
