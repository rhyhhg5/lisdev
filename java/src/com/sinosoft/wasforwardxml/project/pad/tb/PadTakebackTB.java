package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContGetPolTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;

import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class PadTakebackTB extends ABusLogic {
	private MsgHead mMsgHead = null;
	public OutputDataTable cOutputDataTable;
	LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
	LCContGetPolTable tLCContGetPolTable = new LCContGetPolTable();
	VData vData = new VData();
	public CErrors mErrors = new CErrors();
	GlobalInput globalInput = new GlobalInput();

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {

		return true;

	}

	private boolean parseDatas(MsgCollection cMsgInfos) {
		// 报文头
		mMsgHead = cMsgInfos.getMsgHead();
		if (mMsgHead == null) {
			errLog("报文头信息缺失。");
			return false;
		}

		try {
			List tLCContGetPol = cMsgInfos.getBodyByFlag("LCContGetPolTable");
			if (tLCContGetPol == null || tLCContGetPol.size() == 0) {
				errLog("保单信息缺失。");
				return false;
			}

			// for (int i = 0; i < tLCContGetPol.size(); i++) {
			tLCContGetPolTable = (LCContGetPolTable) tLCContGetPol.get(0);
			String prtno = tLCContGetPolTable.getPrtno();
			String contno = new ExeSQL()
					.getOneValue("select contno from lccont where prtno= '"
							+ prtno + "'");
			if ("".equals(contno)) {
				errLog("未查到该印刷号对应的保单号，请检查数据。");
				return false;
			}
			if (tLCContGetPolTable.getGetpolDate() == ""
					|| tLCContGetPolTable.getGetpolMan() == ""
					|| tLCContGetPolTable.getSendPolMan() == "") {
				errLog("签收日期或签收人员或递交人员为空，无法保存！");
				return false;
			}
			FDate nowdate = new FDate();

			if (nowdate.getDate(tLCContGetPolTable.getGetpolDate()).compareTo(
					nowdate.getDate(PubFun.getCurrentDate())) > 0) {
				errLog("客户签收日期不得晚于系统的回销日期！");
				return false;
			}
			//需要确认合同打印时间是否取LCContReceive的printdate
			String printdate = new ExeSQL()
			.getOneValue("select printdate from LCContReceive where dealstate='0' and contno= '"
					+ contno + "'");
			if ("".equals(printdate)) {
				errLog("回执回销失败，原因是：该合同还未打印、或已经操作过回执回销。");
				return false;
			}
			if (nowdate.getDate(tLCContGetPolTable.getGetpolDate()).compareTo(
					nowdate.getDate(printdate)) < 0) {
				errLog("客户签收日期不能早于合同打印时间！");
				return false;
			}
			/*
			 * String currentdate = new
			 * ExeSQL().getOneValue("select Current Date from dual "); String
			 * GetpolDate=tLCContGetPolTable.getGetpolDate(); if (
			 * currentdate.compareTo(GetpolDate)>"0") {
			 * errLog("客户签收日期不得晚于系统的回销日期！"); return false; } if
			 * (dateDiff(fmSave.mGetpolDate.value, fmSave.mPrintDate.value, "D")
			 * > 0) { errLog("客户签收日期不能早于合同打印时间！"); return false; }
			 */
		
			
			tLCContGetPolSchema.setContNo(contno);
			tLCContGetPolSchema.setGetpolDate(tLCContGetPolTable
					.getGetpolDate());
			tLCContGetPolSchema.setGetpolMan(tLCContGetPolTable.getGetpolMan());
			tLCContGetPolSchema.setSendPolMan(tLCContGetPolTable
					.getSendPolMan());
			tLCContGetPolSchema.setGetPolOperator(tLCContGetPolTable
					.getGetPolOperator());
			tLCContGetPolSchema.setManageCom(tLCContGetPolTable.getManageCom());
			tLCContGetPolSchema.setAgentCode(tLCContGetPolTable.getAgentCode());
			tLCContGetPolSchema.setContType(tLCContGetPolTable.getContType());
			vData.clear();

			globalInput.Operator = "PAD";
			globalInput.ComCode = "86";
			globalInput.ManageCom = "86";
			globalInput.setSchema(globalInput);
			vData.add(globalInput);
			vData.addElement(tLCContGetPolSchema);
			LCContGetPolUI tLCContGetPolUI = new LCContGetPolUI();
			if (tLCContGetPolUI.submitData(vData, "INSERT") == false) {
				if (tLCContGetPolUI.mErrors.needDealError()) {
					errLog(tLCContGetPolUI.mErrors.getFirstError());
					cOutputDataTable.setErrorInfo(tLCContGetPolUI.mErrors
							.getFirstError());
					getXmlResult();
					return false;
				} else {
					errLog(" 保存失败，但是没有详细的原因。");
					cOutputDataTable.setErrorInfo(" 保存失败，但是没有详细的原因。");
					getXmlResult();
					return false;
				}
			}
			if (!PrepareInfo()) {
				errLog("获取回执回销任务状态失败");
				return false;
			}

			getXmlResult();
		} catch (Exception ex) {
			return false;
			// errLog(ex.getMessage());
		}

		return true;

	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '"
				+ tLCContGetPolTable.getPrtno() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			errLog("获取保单信息失败");
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			cOutputDataTable.setStateFlag("回销成功");

		}
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos) {// 解析报文，获得报文数据
		if (!parseDatas(cMsgInfos)) {
			return false;
		}

		return true;
	}
}
