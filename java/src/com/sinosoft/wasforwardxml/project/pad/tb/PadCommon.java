package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.List;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.wasforwardxml.project.pad.obj.LCInsuredTable;

public class PadCommon {
	private String cRiskWrapCode = "";
	
	
	public PadCommon(String tRiskWrapCode){
		cRiskWrapCode = tRiskWrapCode;
	}
	//  校验被保人年龄
    public boolean checkInsuredAge(List cWrapParamList){
//      校验年龄
        return true;
    }
//  校验职业类别
    public boolean checkOccupationType(List cLCInsuredList){
    	String occutypeWrapSql = "select maxoccutype from ldwrap where riskwrapcode = '"+cRiskWrapCode+"' ";
        SSRS occutypeSSRS = new ExeSQL().execSQL(occutypeWrapSql);
        if(occutypeSSRS != null && occutypeSSRS.MaxRow>0){
        	if(!"".equals(StrTool.cTrim(occutypeSSRS.GetText(1, 1)))){
        		for(int i = 0;i<cLCInsuredList.size();i++){
        			LCInsuredTable tLCInsuredTable = (LCInsuredTable)cLCInsuredList.get(i);
        			String OccupationType = tLCInsuredTable.getOccupationType();
        			if(Integer.parseInt(occutypeSSRS.GetText(1, 1))<Integer.parseInt(OccupationType)
            				|| Integer.parseInt(OccupationType)<1){
            			return false;
            		}
        		}
        	}
        }
        return true;
    }
//    校验套餐与被保人只能是一个
    public boolean checkTheOne(List aList){
    	if(aList.size()>1){
    		return false;
    	}
    	return true;
    }
    /***
     * @author 
     * @date 
     * @todo 校验产品是否已经停售
     * */
    public String checkWrapStop(String aPolApplyDate){

    	String sql = "select startdate,enddate from ldriskwrap where riskwrapcode = '"+cRiskWrapCode+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(sql);
    	if(tSSRS != null && tSSRS.MaxRow>0){
    		if("".equals(tSSRS.GetText(1, 1)) || tSSRS.GetText(1, 1) == null){ //没有开始时间，则该产品还未开始销售
    			return "该产品未定义开始时间，不允许销售！";  
    		}else if("".equals(tSSRS.GetText(1, 2)) || tSSRS.GetText(1, 2) == null){//没有结束时间，则该产品未停止销售
    			return "";
    		}else{
    			FDate tFDate = new FDate();
    			if(tFDate.getDate(aPolApplyDate).after(tFDate.getDate(tSSRS.GetText(1, 2)))){
    				return "该产品已停售，不可购买！";
    			}
    		}
    	}else{
    		return "未获取到产品信息！";
    	}
    	return "";
    }
}
