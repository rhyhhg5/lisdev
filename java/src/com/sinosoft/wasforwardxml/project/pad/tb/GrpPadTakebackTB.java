package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCGrpContGetPolTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContGetPolSchema;
import com.sinosoft.lis.tb.LCContGetPolUI;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class GrpPadTakebackTB extends ABusLogic{

	private MsgHead mMsgHead = null;
	public OutputDataTable cOutputDataTable;
	LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
	LCGrpContGetPolTable tLCGrpContGetPolTable = new LCGrpContGetPolTable();
	VData vData = new VData();
	public CErrors mErrors = new CErrors();
	GlobalInput globalInput = new GlobalInput();
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		if (!parseDatas(cMsgInfos)) {
			return false;
		}
		return true;
	}
	
	//解析报文
	private boolean parseDatas(MsgCollection cMsgInfos) {
		// 报文头
		mMsgHead = cMsgInfos.getMsgHead();
		if (mMsgHead == null) {
			errLog("报文头信息缺失。");
			return false;
		}

		try {
			List tLCContGetPol = cMsgInfos.getBodyByFlag("LCGrpContGetPolTable");
			if (tLCContGetPol == null || tLCContGetPol.size() == 0) {
				errLog("保单信息缺失。");
				return false;
			}

			// for (int i = 0; i < tLCContGetPol.size(); i++) {
			tLCGrpContGetPolTable = (LCGrpContGetPolTable) tLCContGetPol.get(0);
			String prtno = tLCGrpContGetPolTable.getPrtno();
			String grpcontno = new ExeSQL()
					.getOneValue("select grpcontno from lcgrpcont where prtno= '"
							+ prtno + "'");
			if ("".equals(grpcontno)) {
				errLog("未查到该印刷号对应的保单号，请检查数据。");
				return false;
			}
			
			//回执回销状态
			String GetPolState=new ExeSQL().getOneValue("select GetPolState from LCContGetPol a,LCContReceive b where 1=1 "
					+ "and a.contno=b.contno "
					+ "and a.contno='"+grpcontno+"' "
					+ "and b.dealstate='0' ");
			if(GetPolState!=null && "1".equals(GetPolState)){
				errLog("该保单已回执回销，请核实！");
				return false;
			}
			
			String ContType=tLCGrpContGetPolTable.getContType();
			if(ContType==null || "".equals(ContType)){
				errLog("保单类型不能为空！");
				return false;
			}
			if(!"2".equals(ContType)){
				errLog("团单保单类型为2！");
				return false;
			}
			
			//校验保单保全状态
			String Csql="select a.* from LPEdorApp a, LPGrpEdorMain b where a.edorAcceptNO = b.edorAcceptNo "
				    +" and b.grpContNo = '"+grpcontno+"' and a.edorState != '0'";
			SSRS num=new ExeSQL().execSQL(Csql);
			if(num!=null && num.MaxRow>0){
				errLog("该单正在保全操作中，待保全确认后再进行回执回销操作！");
				return false;
			}
			
			if (tLCGrpContGetPolTable.getGetpolDate()==null || "".equals(tLCGrpContGetPolTable.getGetpolDate())
				|| tLCGrpContGetPolTable.getGetpolMan()==null || "".equals(tLCGrpContGetPolTable.getGetpolMan())
				|| tLCGrpContGetPolTable.getSendPolMan()==null	|| "".equals(tLCGrpContGetPolTable.getSendPolMan())) {
				errLog("签收日期或签收人员或递交人员为空，请核实！");
				return false;
			}
			if(tLCGrpContGetPolTable.getPrintDate()==null || "".equals(tLCGrpContGetPolTable.getPrintDate())
					|| tLCGrpContGetPolTable.getPrintTime()==null || "".equals(tLCGrpContGetPolTable.getPrintTime())
					|| tLCGrpContGetPolTable.getPrintCount()==null || "".equals(tLCGrpContGetPolTable.getPrintCount())){
				errLog("打印日期或打印时间或打印次数不能为空，请核实！");
				return false;
			}
			String signdate=new ExeSQL().getOneValue("select signdate from lcgrpcont where prtno='"+ prtno + "'");
			System.out.println("签单日期为："+signdate);
			if(signdate==null || "".equals(signdate)){
				errLog("该保单未签单，请核实！");
				return false;
			}
			VData tVData= new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("PrtNo", prtno);
			tTransferData.setNameAndValue("PrintDate", tLCGrpContGetPolTable.getPrintDate());
			tTransferData.setNameAndValue("PrintTime", tLCGrpContGetPolTable.getPrintTime());
			tTransferData.setNameAndValue("PrintCount", tLCGrpContGetPolTable.getPrintCount());
			tVData.add(tTransferData);
			DealGrpPadPrintInfo tDealGrpPadPrintInfo = new DealGrpPadPrintInfo();
			if(!tDealGrpPadPrintInfo.submitData(tVData, "INSERT")){
				errLog(tDealGrpPadPrintInfo.mErrors.getFirstError());
				cOutputDataTable.setErrorInfo(tDealGrpPadPrintInfo.mErrors
						.getFirstError());
				getXmlResult();
				return false;
			}
			FDate nowdate = new FDate();

			if (nowdate.getDate(tLCGrpContGetPolTable.getGetpolDate()).compareTo(
					nowdate.getDate(PubFun.getCurrentDate())) > 0) {
				errLog("客户签收日期不得晚于系统的回销日期！");
				return false;
			}
			//需要确认合同打印时间是否取LCContReceive的printdate
			String printdate = new ExeSQL()
			.getOneValue("select printdate from LCContReceive where dealstate='0' and contno= '"
					+ grpcontno + "'");
			if ("".equals(printdate)) {
				errLog("该合同还未打印，请核实！");
				return false;
			}
			if (nowdate.getDate(tLCGrpContGetPolTable.getGetpolDate()).compareTo(
					nowdate.getDate(printdate)) < 0) {
				errLog("客户签收日期不能早于合同打印时间！");
				return false;
			}
			
			tLCContGetPolSchema.setContNo(grpcontno);
			tLCContGetPolSchema.setGetpolDate(tLCGrpContGetPolTable.getGetpolDate());
			tLCContGetPolSchema.setGetpolMan(tLCGrpContGetPolTable.getGetpolMan());
			tLCContGetPolSchema.setSendPolMan(tLCGrpContGetPolTable.getSendPolMan());
			tLCContGetPolSchema.setGetPolOperator(tLCGrpContGetPolTable.getGetPolOperator());
			tLCContGetPolSchema.setManageCom(tLCGrpContGetPolTable.getManageCom());
			tLCContGetPolSchema.setAgentCode(tLCGrpContGetPolTable.getAgentCode());
			tLCContGetPolSchema.setContType(tLCGrpContGetPolTable.getContType());
			vData.clear();

			globalInput.Operator = "GPAD";
			globalInput.ComCode = tLCGrpContGetPolTable.getManageCom();
			globalInput.ManageCom = tLCGrpContGetPolTable.getManageCom();
			globalInput.setSchema(globalInput);
			vData.add(globalInput);
			vData.addElement(tLCContGetPolSchema);
			LCContGetPolUI tLCContGetPolUI = new LCContGetPolUI();
			if (tLCContGetPolUI.submitData(vData, "INSERT") == false) {
				if (tLCContGetPolUI.mErrors.needDealError()) {
					errLog(tLCContGetPolUI.mErrors.getFirstError());
					cOutputDataTable.setErrorInfo(tLCContGetPolUI.mErrors
							.getFirstError());
					getXmlResult();
					return false;
				} else {
					errLog(" 保存失败，但是没有详细的原因。");
					cOutputDataTable.setErrorInfo(" 保存失败，但是没有详细的原因。");
					getXmlResult();
					return false;
				}
			}
			if (!PrepareInfo()) {
				errLog("获取回执回销任务状态失败");
				return false;
			}
			getXmlResult();
		} catch (Exception ex) {
			return false;
			// errLog(ex.getMessage());
		}
		return true;
	}
	
	// 返回报文
	public void getXmlResult() {
		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}
	
	public boolean PrepareInfo() {
		String sql = "select grpcontno,amnt,prem,prtno,stateflag,uwflag from lcgrpcont where prtno = '"
				+ tLCGrpContGetPolTable.getPrtno() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			errLog("获取保单信息失败");
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			cOutputDataTable.setStateFlag("回销成功");
		}
		return true;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}

}
