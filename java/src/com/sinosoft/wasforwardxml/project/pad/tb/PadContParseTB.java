package com.sinosoft.wasforwardxml.project.pad.tb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;

import javax.xml.parsers.DocumentBuilderFactory;

import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cbsws.obj.RspSaleInfo;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.cbcheck.RecordFlagBL;
import com.sinosoft.lis.cbcheck.TaxCheckContUI;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDImpartDB;
import com.sinosoft.lis.db.LDOccupationDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCCustomerImpartDetailSchema;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPersonTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.sys.MixedSalesAgentQueryUI;
import com.sinosoft.lis.tb.BPO;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.tb.CustomerImpartBL;
import com.sinosoft.lis.tb.ExemptionRiskBL;
import com.sinosoft.lis.tb.LCPolImpInfo;
import com.sinosoft.lis.tb.LCPolParser;
import com.sinosoft.lis.tb.ProposalBL;
import com.sinosoft.lis.vbl.LCBnfBLSet;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCContSubSet;
import com.sinosoft.lis.vschema.LCCustomerImpartDetailSet;
import com.sinosoft.lis.vschema.LCCustomerImpartParamsSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsuredRelatedSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPersonTraceSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLPathTool;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.workflow.tb.TbWorkFlowUI;
import com.sinosoft.workflowengine.ActivityOperator;

public class PadContParseTB extends ABusLogic {

	public String BatchNo = "";// 批次号

	public String MsgType = "";// 报文类型

	public String Operator = "";// 操作者

	public double mPrem = 0;// 保费

	public double mAmnt = 0;// 保额

	private String mUrl = "";

	private String mFilePath = "";

	private Document mDocument;

	private String ParsePath = "/DATASET/CONTTABLE/ROW";

	private String mBPOMissionState = "";

	private String[] mPrtNoArr;

	private String mPrtNo = "";
	
	//约定缴费标识
	private String mAgreedPayFlag = "";
	//约定缴费日期
	private String mAgreedPayDate = "";
	
	private String mAgentSaleCode = "";

	private HashSet mNoDealContIDSet = new HashSet(); // 不需要处理的保单的ContID

	private org.w3c.dom.Document m_doc = null;

	private String mContID = null;

	private LCPolImpInfo m_LCPolImpInfo = new LCPolImpInfo();

	private String mBatchNo = "";

	private String mContNo = "";

	private String mManageCom = "";

	private String mAppntNo = "";

	private String mAppntName = "";

	private String mAgentCode = "";

	private String mImportDate = "";

	private LCPolBL mainPolBL = new LCPolBL();

	private int iSequenceNo = 1;

	private int mRiskSeqNo = 1;

	private int mPayIntv = 0;
	
	private boolean bl = false;

	private static final String STATIC_GRPCONTNO = "00000000000000000000";

	private static final String STATIC_CONTTYPE = "1";

	private static final String STATIC_PROCESSID = "0000000003";

	private static final String STATIC_ActivityID = "0000001001";

	private static final String STATIC_EActivityID = "0000001022";

	private String mMissionid = "";

	public OutputDataTable cOutputDataTable;

	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	public String mError = "";// 处理过程中的错误信息

	String cRiskWrapCode = "";// 套餐编码
	
	private double padPrem=0.0;
	
	private boolean delbl = false;
	
	private String mMagNumJson = "";//magnum请求json
	
	private String PrintType="";
	
	private String haveMagnumJson = "";
	
	private boolean magnumJsonFlag = false;
	
	LCContSubSet tLCContSubSet = null;//20170919-新增pad税优，不过信息没有校验。
	
	public String Sysdate = PubFun.getCurrentDate();
	
	private LCPersonTraceSet mAppntLCPersonTraceSet ;
	private LCPersonTraceSet mInsuredLCPersonTraceSet;

	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		System.out.println("开始处理Pad出单标准平台投保过程");
		System.out.println("开始时间："+PubFun.getCurrentDate() + " "+ PubFun.getCurrentTime());
		long startTime = System.currentTimeMillis();
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		mBatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mDocument = cInXmlDoc;
		mGlobalInput.Operator = "PAD";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86";

		try {
			if (!getSendUrlName()) {
				errLog(mError);
				return false;
			}
			if (!saveXmlFile()) {
				errLog(mError);
				return false;
			}

			bulidDocument();

			if (!ParseXml()) {
				if(delbl){
					System.out.println("删除新单开始，Prtno为：" + mPrtNo);
					if(!"".equals(mPrtNo)||null==mPrtNo){
						VData tVData = new VData();
						String tDeleteReason = "pad出单新单复核失败需要进行新单删除";
						TransferData tTransferData = new TransferData();
						LCContDB tLCContDB = new LCContDB();
						tLCContDB.setPrtNo(mPrtNo);
						LCContSchema tLCContSchema = new LCContSchema();
						LCContSet tLCContSet = tLCContDB.query();
						if (tLCContSet.size() > 0 && !"1".equals(tLCContSet.get(1).getAppFlag())) {
							tLCContSchema = tLCContSet.get(1);
							tTransferData
									.setNameAndValue("DeleteReason", tDeleteReason);
							tVData.add(tLCContSchema);
							tVData.add(tTransferData);
							tVData.add(mGlobalInput);
							ContDeleteUI tContDeleteUI = new ContDeleteUI();
							tContDeleteUI.submitData(tVData, "DELETE");
							System.out.println("删除新单结束");
						}
					}					
				}
				errLog(mError);
				return false;
			}
			
			VData ttVData1 = new VData();
			TransferData ttTransferData = new TransferData();
			ttTransferData.setNameAndValue("ContNo", mContNo);
			ttTransferData.setNameAndValue("PrtNo", mPrtNo);
			ttVData1.add(ttTransferData);
			RecordFlagBL tRecordFlagBL = new RecordFlagBL();
			if(!tRecordFlagBL.submitData(ttVData1, "")){
				errLog("获取双录标识失败！");
                return false;
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			if(!createSharedMark()){
				errLog("创建共享标识数据失败！");
				return false;
			}
			getXmlResult();
			long endTime = System.currentTimeMillis();
			System.out.println("结束处理Pad出单标准平台投保过程，结束时间"+PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() +"，用时" +((endTime -startTime)/1000/60)+"分钟");
			String lockSQL = "update lccontsub set ScanCheckFlag = '0' where prtno = '"+mPrtNo+"'";
			new ExeSQL().execUpdateSQL(lockSQL);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag,uwflag from lccont where prtno = '" + mPrtNo + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			if("9".equals(tSSRS.GetText(1, 6))){
				cOutputDataTable.setStateFlag("0"); //投保成功状态
			}else if("5".equals(tSSRS.GetText(1, 6))){				
				cOutputDataTable.setStateFlag("4");//待人工核保状态
			}else{
				cOutputDataTable.setStateFlag("10");//新单复核状态
				cOutputDataTable.setErrorInfo(mError);//新单复核失败原因
			}
			
		}

		return true;
	}

	private boolean getSendUrlName() {
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PadTBXml'";// 报文的存放路径
		mUrl = new ExeSQL().getOneValue(sqlurl);
	    //mUrl = "D:\\";
		System.out.println("报文的存放路径:" + mUrl);// 调试用－－－－－
		if (mUrl == null || mUrl.equals("")) {
			mError = "获取文件存放路径出错";
			return false;
		}
		return true;
	}

	private boolean saveXmlFile() {
		if (!newFolder(mUrl)) {
			mError = "建立文件存放目录出错";
			return false;
		}
		System.out.println("文件存放路径" + mUrl);
		mFilePath = mUrl + mBatchNo + ".xml";
		XMLOutputter XMLOut = new XMLOutputter();
		try {
			XMLOut.output(mDocument, new FileOutputStream(mFilePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在");
				return true;
			} else {
				myFilePath.mkdir();
				System.out.println("新建目录成功");
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}

	private boolean ParseXml() {
		// 得到保单的传入信息
		XMLPathTool tXPT = new XMLPathTool(mFilePath);
		NodeList nodeList = tXPT.parseN(ParsePath);
		// 循环处理每个保单节点，生成保单数据
		try {
			if (!createCont(nodeList)) {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			mError = "生成保单信息失败！";
			return false;
		}
		return true;
	}

	/**
	 * createCont 循环处理每个保单节点，生成保单数据
	 * 
	 * @return boolean
	 */
	private boolean createCont(NodeList nodeList) {
		LCPolParser tLCPolParser = new LCPolParser();
		mPrtNoArr = new String[nodeList.getLength()];
		// 循环每个保单
		for (int i = 0; i < nodeList.getLength(); i++) {
			mPrtNo = "";
			mBPOMissionState = BPO.MISSION_STATE_DATA_ERR; // 数据错误

			Node node = nodeList.item(i);
			try {
				node = transformNode(node);
			} catch (Exception ex) {
				ex.printStackTrace();
				node = null;
			}

			NodeList contNodeList = node.getChildNodes();
			if (contNodeList.getLength() <= 0) {
				continue;
			}

			TransferData tContData;
			// 解析保单数据
			try {
				tContData = dealOneCont(tLCPolParser, contNodeList);

				if (tContData == null) {
					return false;
				}

				if(mAgreedPayFlag==null || "".equals(mAgreedPayFlag)){
					mError="约定缴费标识不能为空！";
	        		errLog(mError);
	                return false;
	        	}
	        	if(!"0".equals(mAgreedPayFlag) && !"1".equals(mAgreedPayFlag)){
	        		mError="约定缴费标识只能为0或1！";
	        		errLog(mError);
	                return false;
	        	}
	        	if("0".equals(mAgreedPayFlag) && mAgreedPayDate!=null && !"".equals(mAgreedPayDate)){
	        		mError="非约定缴费,约定缴费日期只能为空！";
	        		errLog(mError);
	                return false;
	        	}
	        	if("1".equals(mAgreedPayFlag) && (mAgreedPayDate==null || "".equals(mAgreedPayDate))){
	        		mError="约定缴费,约定缴费日期不能为空！";
	        		errLog(mError);
	                return false;
	        	}
	        	if("1".equals(mAgreedPayFlag) && Sysdate.compareTo(mAgreedPayDate)>0){
	        		mError="约定缴费日期不能早于当前系统日期！";
	        		errLog(mError);
	                return false;
	        	}
	        	
				LCContSchema tLCContSchema = (LCContSchema) tContData.getValueByName("tLCContSchema"); // 合同
				VData tAppntVData = (VData) tContData	.getValueByName("tAppntVData"); // 投保人
				VData tInsuredVData = (VData) tContData.getValueByName("tInsuredVData"); // 被保人
				LCBnfSet tLCBnfSet = (LCBnfSet) tContData.getValueByName("tLCBnfSet"); // 受益人
				LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = (LCCustomerImpartDetailSet) tContData.getValueByName("tLCCustomerImpartDetailSet"); // 客户告知
				tLCContSubSet =(LCContSubSet)tContData.getValueByName("tLCContSubSet"); // 税优信息
				tLCContSchema.setAgentSaleCode(mAgentSaleCode);
 
				
				//校验受益人证件
				if (tLCBnfSet != null && tLCBnfSet.size() != 0) {
					for (int j = 1; j <= tLCBnfSet.size(); j++) {
						String bnfSex=tLCBnfSet.get(j).getSex();
			            String bnfBirthday=tLCBnfSet.get(j).getBirthday();
			            String bnfIDType=tLCBnfSet.get(j).getIDType();
			            String bnfIDNo=tLCBnfSet.get(j).getIDNo();
						if (bnfIDNo != null&& !"".equals(bnfIDNo)) {
							String chkbnfIDNo=PubFun.CheckIDNo(bnfIDType, bnfIDNo, bnfBirthday, bnfSex);
			            	if(chkbnfIDNo!=null&&!"".equals(chkbnfIDNo)){
			            		mError="受益人"+chkbnfIDNo;
			            		errLog(mError);
			            		return false;
			            	}
						}
					}
				}
				// 将客户身份证号码中的x转换成大写（被保险人） 2009-02-05 liuyp
				LCInsuredSet tLCInsuredSet = (LCInsuredSet) tInsuredVData.getObjectByObjectName("LCInsuredSet", 0);
				mInsuredLCPersonTraceSet = (LCPersonTraceSet) tInsuredVData.getObjectByObjectName("LCPersonTraceSet", 0);

				if (tLCInsuredSet != null && tLCInsuredSet.size() != 0) {
					for (int j = 1; j <= tLCInsuredSet.size(); j++) {
						if (tLCInsuredSet.get(j).getIDType() != null
								&& tLCInsuredSet.get(j).getIDNo() != null) {
							if (tLCInsuredSet.get(j).getIDType().equals("0")) {
								String tLCInsuredIdNo = tLCInsuredSet.get(j)
										.getIDNo().toUpperCase();
								tLCInsuredSet.get(j).setIDNo(tLCInsuredIdNo);
							}
						}

					}
					
					for (int j = 1; j <= tLCInsuredSet.size(); j++) {
						String InsuredSex=tLCInsuredSet.get(j).getSex();
			            String InsuredBirthday=tLCInsuredSet.get(j).getBirthday();
			            String InsuredIDType=tLCInsuredSet.get(j).getIDType();
			            String InsuredIDNo=tLCInsuredSet.get(j).getIDNo();
			            String InsuredOccupationCode=tLCInsuredSet.get(j).getOccupationCode();
			          //被保人授权使用客户信息只能为1
			            String InsuredAuthorization=tLCInsuredSet.get(j).getAuthorization();
			            if(InsuredAuthorization!=null && !InsuredAuthorization.equals("")){
			            	if(!InsuredAuthorization.equals("1")){
			            		mError="被保人授权使用客户信息只能为1！";
			            		errLog(mError);
			            		return false;
			            	}
			            }else{
			            	mError="被保人授权使用客户信息不能为空！";
			            	errLog(mError);
		            		return false;
			            }
						if (InsuredIDNo != null&& !"".equals(InsuredIDNo)) {
							String chkInsuredIDNo=PubFun.CheckIDNo(InsuredIDType, InsuredIDNo, InsuredBirthday, InsuredSex);
			            	if(chkInsuredIDNo!=null&&!"".equals(chkInsuredIDNo)){
			            		mError="被保人"+chkInsuredIDNo;
			            		errLog(mError);
			            		return false;
			            	}
						}
						// #4477 被保人职业代码校验  by yuchunjian 20190726
						if (InsuredOccupationCode != null && !"".equals(InsuredOccupationCode)){
							String InsuredOCSQL = new ExeSQL().getOneValue("select 1 from LDOccupation where OccupationCode = '" + InsuredOccupationCode + "'");
							if(InsuredOCSQL == null || "".equals(InsuredOCSQL)){
								mError="被保险人职业代码不正确!";
								errLog(mError);
			            		return false;
							}
						}
					}

				}
				
				//被保人电话重复校验
				LCAddressSet insuredSet = (LCAddressSet) tInsuredVData.getObjectByObjectName("LCAddressSet", 0);
//				mError=checkInsured(insuredSet,tLCInsuredSet);
//	            if(!"".equals(mError) && mError!= null){
//	            	errLog(mError);
//	            	return false;
//	            }
				
				LCAppntSet tLCAppntSet = (LCAppntSet) tAppntVData.getObjectByObjectName("LCAppntSet", 0);
				mAppntLCPersonTraceSet = (LCPersonTraceSet)tAppntVData.getObjectByObjectName("LCPersonTraceSet", 0);

				
				padPrem = tLCAppntSet.get(1).getBMI();

				// 将客户身份证号码中的x转换成大写（投保人） 2009-02-05 liuyp
				if (tLCAppntSet.get(1).getIDType() != null && tLCAppntSet.get(1).getIDNo() != null) {
					if (tLCAppntSet.get(1).getIDType().equals("0")) {
						String tLCAppntIdNo = tLCAppntSet.get(1).getIDNo().toUpperCase();
						tLCAppntSet.get(1).setIDNo(tLCAppntIdNo);
					}
				}
				String appntSex=tLCAppntSet.get(1).getAppntSex();
	            String appntBirthday=tLCAppntSet.get(1).getAppntBirthday();
	            String appntIDType=tLCAppntSet.get(1).getIDType();
	            String appntIDNo=tLCAppntSet.get(1).getIDNo();
	            String appntOccupationCode=tLCAppntSet.get(1).getOccupationCode();
	          //投保人授权使用客户信息只能为1
	            String appntAuthorization=tLCAppntSet.get(1).getAuthorization();
	            if(appntAuthorization!=null && !appntAuthorization.equals("")){
	            	if(!appntAuthorization.equals("1")){
	            		errLog("投保人授权使用客户信息只能为1！");
	            		return false;
	            	}
	            }else{
	            	mError="投保人授权使用客户信息不能为空！";
	            	errLog(mError);
//	            	errLog("投保人授权使用客户信息不能为空！");
	        		return false;
	            }
	            if(!"".equals(appntIDNo)&&appntIDNo!=null){
	            	String chkAppntIDNo=PubFun.CheckIDNo(appntIDType, appntIDNo, appntBirthday, appntSex);
	            	if(chkAppntIDNo!=null&&!"".equals(chkAppntIDNo)){
	            		mError="投保人"+chkAppntIDNo;
	            		errLog(mError);
	            		return false;
	            	}
	            }
	            
				// #4477 投保人职业代码校验  by yuchunjian 20190726
				if (appntOccupationCode != null && !"".equals(appntOccupationCode)){
					String appntOCSQL = new ExeSQL().getOneValue("select 1 from LDOccupation where OccupationCode = '" + appntOccupationCode + "'");
					if(appntOCSQL == null || "".equals(appntOCSQL)){
						mError="投保险人职业代码不正确!";
						errLog(mError);
	            		return false;
					}
				}
				// 2018-07-13
				LCAddressSet tLCAddressSet = (LCAddressSet) tAppntVData
						.getObjectByObjectName("LCAddressSet", 0);
				String grpName = tLCAddressSet.get(1).getGrpName();
				String grpNo = tLCContSubSet.get(1).getGrpNo();
				String taxFlag = tLCContSubSet.get(1).getTaxFlag();
				System.out.println(taxFlag + "+" + grpNo + "+" + grpName);
				if (taxFlag != null && !"".equals(taxFlag)) {
					if (grpName == null || "".equals(grpName)) {
						mError = "投保税优系列产品，工作单位必录！";
						errLog(mError);
						return false;
					}
				}
	            
				//投保人电话重复校验
				LCAddressSet appntSet=(LCAddressSet)tAppntVData.getObjectByObjectName("LCAddressSet", 0);
				mError=checkAppnt(appntSet,tLCAppntSet,tLCContSchema);
	            if(!"".equals(mError) && mError!= null){
	            	errLog(mError);
	            	return false;
	            }
				
				boolean tInsuredCheck = true;
				if (tInsuredVData == null && !mNoDealContIDSet.contains(mContID)) {
					mError = tLCPolParser.mErrors.getFirstError();
					tInsuredCheck = false;
					return false;
				}
				if (tInsuredCheck && !mNoDealContIDSet.contains(mContID)) {
					if (!m_LCPolImpInfo.init(mBatchNo, this.mGlobalInput,	tLCContSchema, tInsuredVData, tAppntVData,	tLCBnfSet, tLCCustomerImpartDetailSet)) {
						tInsuredCheck = false;
						mError = m_LCPolImpInfo.mErrors.getContent();
						return false;
					}
				}
				

				if (tInsuredCheck && !mNoDealContIDSet.contains(mContID)) {
					// 按合同导入
					System.out.println("保费计算开始============，时间是："+PubFun.getCurrentDate() + " " + PubFun.getCurrentTime());
					long startTime = System.currentTimeMillis();
					bl = DiskContImport(mContID);
					long endTime = System.currentTimeMillis();
					System.out.println("保费计算结束============，时间是："+PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() +"，用时" +((endTime -startTime)/1000/60)+"分钟");
					
					if (!bl) {
						System.out.println("导入失败");
						return false;
					} else {
						System.out.println("导入成功");
						mBPOMissionState = BPO.MISSION_STATE_CREATE_CONT; // 生成保单
//						江苏中介
						String checkSQL="select  contno from lccont where contno='"+mContNo+"'"
				        +" and managecom like '8632%' "
				        +" and salechnl in (select  code  from ldcode  where  codetype='JSZJsalechnl')"; 
						System.out.println("comcode"+mManageCom);
							ExeSQL tExeSQL= new ExeSQL();
							SSRS tSSRS = tExeSQL.execSQL(checkSQL);
							if(tSSRS.getMaxRow()>0){
							TransferData  tdata=new TransferData();
							tdata.setNameAndValue("ContNo", tSSRS.GetText(1, 1));
							VData vdata=new VData();
							vdata.add(tdata);     
							ContInputAgentcomChkBL cacb=new ContInputAgentcomChkBL();
							if(!cacb.submitData(vdata, "check")){
							    mError=cacb.mErrors.getError(0).errorMessage;
							    System.out.println(cacb.mErrors.getError(0).errorMessage);
						     	errLog(mError);
						     	return false;		     	
							  }
							} 
					}
				}
				//zxs 20190815 #4502
	        	//交叉销售增加正确性校验 
				String crs_Salechnl = tLCContSchema.getCrs_SaleChnl();
				String crs_BussType = tLCContSchema.getCrs_BussType();
				String grpAgentCom = tLCContSchema.getGrpAgentCom();
				String grpAgentCode = tLCContSchema.getGrpAgentCode();
				String grpAgentName = tLCContSchema.getGrpAgentName();
				String grpAgentIdno = tLCContSchema.getGrpAgentIDNo();
				System.out.println("crs_Salechnl="+crs_Salechnl+",crs_BussType="+crs_BussType+",grpAgentCom="+grpAgentCom+",grpAgentCode="+grpAgentCode+",grpAgentName="+grpAgentName+",grpAgentIdno="+grpAgentIdno);
				if((crs_Salechnl==null||("").equals(crs_Salechnl))&&(crs_BussType==null||("").equals(crs_BussType))&&
						(grpAgentCom==null||("").equals(grpAgentCom))&&(grpAgentCode==null||("").equals(grpAgentCode))
						&&(grpAgentName==null||("").equals(grpAgentName))&&(grpAgentIdno==null||("").equals(grpAgentIdno))){
					
				}else if((crs_Salechnl!=null&&!("").equals(crs_Salechnl))&&(crs_BussType!=null&&!("").equals(crs_BussType))&&
						(grpAgentCom!=null&&!("").equals(grpAgentCom))&&(grpAgentCode!=null&&!("").equals(grpAgentCode))
						&&(grpAgentName!=null&&!("").equals(grpAgentName))&&(grpAgentIdno!=null&&!("").equals(grpAgentIdno))){
					if(tLCContSchema.getGrpAgentCode().trim().length()!=10){
						mError="交叉销售对方业务员代码字段必须为10位，请核实!";
						errLog(mError);
						return false;
					}else if(tLCContSchema.getCrs_SaleChnl().equals("01")&&!tLCContSchema.getGrpAgentCode().substring(0,1).equals("1")){
						mError="交叉销售渠道是产代健，对方业务员代码需以数字1开头!";
						errLog(mError);
						return false;
					}else if(tLCContSchema.getCrs_SaleChnl().equals("02")&&!tLCContSchema.getGrpAgentCode().substring(0,1).equals("3")){
						mError="交叉销售渠道是寿代健，对方业务员代码需以数字3开头!";
						errLog(mError);
						return false;
					}
				
					MixedSalesAgentQueryUI mixedSalesAgentQueryUI = new MixedSalesAgentQueryUI();
					RspSaleInfo rspSaleInfo = new RspSaleInfo();
					try{
						System.out.println("code="+tLCContSchema.getGrpAgentCode()+",managecom="+mManageCom);
						rspSaleInfo = mixedSalesAgentQueryUI.getSaleInfo(tLCContSchema.getGrpAgentCode(),mManageCom);
					}catch(Exception ex){
						mError="集团获取交叉销售数据失败！";
						errLog(mError);
						return false;
					}
					String type = rspSaleInfo.getMESSAGETYPE();
					String typeInfo = rspSaleInfo.getERRDESC();
					if("01".equals(type)){
						mError = "交叉销售数据错误："+typeInfo;
						errLog(mError);
						return false;	  	
					}else {
					System.out.println("grpAgentCom="+rspSaleInfo.getMAN_ORG_COD()+",grpAgentCode="+rspSaleInfo.getUNI_SALES_COD()+",grpAgentName="+rspSaleInfo.getSALES_NAM()+",grpAgentIdno="+rspSaleInfo.getID_NO());

						if(!tLCContSchema.getGrpAgentCode().equals(rspSaleInfo.getUNI_SALES_COD())||
								!tLCContSchema.getGrpAgentCom().equals(rspSaleInfo.getMAN_ORG_COD())|| 
								!tLCContSchema.getGrpAgentName().equals(rspSaleInfo.getSALES_NAM())||
								!tLCContSchema.getGrpAgentIDNo().equals(rspSaleInfo.getID_NO())){
							mError = "录入交叉销售信息与集团返回结果不一致，请核实!";
							errLog(mError);
							return false;					  
						}			  	
					} 
					
				}else{
					mError = "交叉销售信息不完整，请核实!";
					errLog(mError);
					return false;	
				}
				//增加税优唯一性验证
				String checkSY = "select TaxOptimal from lmriskapp where riskcode in (select riskcode from lcpol where contno='"+mContNo+"')";
				ExeSQL tExeSQL= new ExeSQL();
				SSRS tSSRS = tExeSQL.execSQL(checkSY);
				for(int m = 1;m <=tSSRS.getMaxRow();m++){
					if(!"".equals(tSSRS.GetText(m, 1)) && "Y".equals(tSSRS.GetText(m, 1))){
						TransferData mTransferData = new TransferData();
						mTransferData.setNameAndValue("ContNo", mContNo);
						mTransferData.setNameAndValue("PrtNo", mPrtNo);
						VData tVData = new VData();
						tVData.add(mTransferData);
						tVData.add(mGlobalInput);
						TaxCheckContUI tTaxCheckContUI = new TaxCheckContUI();//调用处理类
						if (!tTaxCheckContUI.submitData(tVData, "check")) {
							mError = tTaxCheckContUI.mErrors.getError(0).errorMessage;
							return false;
						}
					}
				}
				if (BPO.MISSION_STATE_CREATE_CONT.equals(mBPOMissionState)) {
					// 工作流跳转到新单复核
					if (inputConfirm("", mPrtNo)) {
						mBPOMissionState = BPO.MISSION_STATE_INPUT_CONT; // 已录入
					} else {
						return false;
					}
				}
			} catch (NumberFormatException ex) {
				mError = "数据类型转换失败:" + ex.getMessage();
				ex.printStackTrace();
				return false;
			} catch (Exception ex) {
				mError = "未知异常导致数据导入失败：" + ex.getMessage();
				return false;
			}

		}

		return true;
	}

	private TransferData dealOneCont(LCPolParser tLCPolParser,
			NodeList contNodeList) {
		TransferData tTransferData = new TransferData();

		Node contNode = null;
		String nodeName = "";

		for (int j = 0; j < contNodeList.getLength(); j++) {
			contNode = contNodeList.item(j);
			nodeName = contNode.getNodeName();
			if (nodeName.equals("#text")) {
				continue;
			}
			if (nodeName.equals("CONTID")) {
				mContID = contNode.getFirstChild().getNodeValue();
				System.out.println(PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime() + ":生成保单Contid:" + mContID);
				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}
				continue;
			}
			if (nodeName.equals("PrtNo")) {
				mPrtNo = contNode.getFirstChild().getNodeValue();
				System.out.println(PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime() + ":生成保单PrtNo:" + mPrtNo);

				if (!checkExists(mPrtNo)) {
					return null;
				}

				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}

				continue;
			}
			
			if(nodeName.equals("AgentSaleCode")){
				try{
					mAgentSaleCode = contNode.getFirstChild().getNodeValue();}
				catch(Exception e){
					System.out.println("PadContParseTB----->获取AgentSaleCode节点出错");
				}
				System.out.println(PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime() + ":生成AgentSaleCode:" + mAgentSaleCode);
				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}


				continue;
			}
			
			if (nodeName.equals("AgreedPayFlag")) {
				try {
					mAgreedPayFlag = contNode.getFirstChild().getNodeValue();
				} catch (Exception e) {
					System.out.println("PadContParseTB----->获取AgreedPayFlag节点出错");
				}
				System.out.println(PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + ":生成AgentSaleCode:" + mAgentSaleCode);
				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}
			}
			if (nodeName.equals("AgreedPayDate")) {
				try {
					mAgreedPayDate = contNode.getFirstChild().getNodeValue();
				} catch (Exception e) {
					System.out.println("PadContParseTB----->获取AgreedPayDate节点出错");
				}
				System.out.println(
						PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + ":生成AgentSaleCode:" + mAgentSaleCode);
				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}
			}
			
			if (nodeName.equals("APPNTTABLE")) {
				// 解析投保人XML
				VData tAppntVData = tLCPolParser.getLCAppntData(contNode);
				tTransferData.setNameAndValue("tAppntVData", tAppntVData);
				continue;
			}

			if (nodeName.equals("INSUREDTABLE")) {
				// 解析被保险人XML
				VData tInsuredVData = tLCPolParser.getLCInsuredData(contNode);
				tTransferData.setNameAndValue("tInsuredVData", tInsuredVData);
				continue;
			}
			if (nodeName.equals("LCPOLTABLE")) {
				// 解析一个险种保单节点
				VData tVData = tLCPolParser.parseLCPolNode(contNode);
				if (tVData == null || tVData.size() == 0) {
					return new TransferData();
				}
				LCContSchema tLCContSchema = (LCContSchema) tVData.getObjectByObjectName("LCContSchema", 0);
				if (mPrtNo == null || mPrtNo.equals("")) {
					mPrtNo = (String) ((VData) tVData.getObjectByObjectName(
							"VData", 0)).getObjectByObjectName("String", 0);
				}
				tVData.removeElement(tLCContSchema);

				// 以合同号为索引保存险种保单信息
				m_LCPolImpInfo.addContPolData(mContID, tVData);
				tLCContSchema.setDueFeeMsgFlag(tLCPolParser.getMDueFeeMsgFlag());
				tLCContSchema.setExiSpec(tLCPolParser.getMExiSpec());
				tTransferData.setNameAndValue("tLCContSchema", tLCContSchema);

				continue;
			}
			if (nodeName.equals("BNFTABLE")) {
				// 解析受益人XML
				LCBnfSet tLCBnfSet = tLCPolParser.getLCBnfSet(contNode);
				tTransferData.setNameAndValue("tLCBnfSet", tLCBnfSet);

				continue;
			}
			if (nodeName.equals("IMPARTTABLE")) {
				// 解析客户告知XML
				LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = tLCPolParser
						.getImpartSet(contNode);
				tTransferData.setNameAndValue("tLCCustomerImpartDetailSet",
						tLCCustomerImpartDetailSet);
				continue;
			}
			
			//增加SYTABLE税优 20170915
			if (nodeName.equals("SYTABLE")) {
				// 解析客户告知XML
				LCContSubSet tLCContSubSet = tLCPolParser.getLCContSubData(contNode); 
				tTransferData.setNameAndValue("tLCContSubSet",tLCContSubSet);
				continue;
			}
			//增加有无magnumJson字段，若有，则进行magnumJson解析2018-07 ByCLH
//			if(nodeName.equals("HaveMagnumJson")){
//				try{
//					haveMagnumJson = contNode.getFirstChild().getNodeValue();
//					System.out.println(haveMagnumJson);}
//				catch(Exception e){
//					System.out.println("HaveMagnumJson是空的");
//				}
//				if("Y".equals(haveMagnumJson)){
//					magnumJsonFlag = true;
//				} else{
//					magnumJsonFlag = false;
//				} 
//				continue;
//			}
			//解析封装MagNumJson
//			if(nodeName.equals("MagnumJson") && magnumJsonFlag){
			if(nodeName.equals("MagnumJson")){
				try{
				mMagNumJson = contNode.getFirstChild().getNodeValue();}
				catch(Exception e){
					System.out.println("MagnumJson是空的");
				}
				System.out.println(PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime() + ":生成MagNum_Json:" + mMagNumJson);
				tTransferData.setNameAndValue("MagNumJson",mMagNumJson);
				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}

				continue;
			}
			if(nodeName.equals("PrintType")){
				try{
					PrintType = contNode.getFirstChild().getNodeValue();}
				catch(Exception e){
					System.out.println("PrintType是空的");
				}
				System.out.println(PubFun.getCurrentDate() + " "
						+ PubFun.getCurrentTime() + ":生成PrintType:" + PrintType);
//				tTransferData.setNameAndValue("PrintType",PrintType);
				// 不需要处理
				if (mNoDealContIDSet.contains(mContID)) {
					break;
				}

				continue;
			}
		}

		return tTransferData;
	}

	private boolean checkExists(String prtNo) {
		if ("".equals(StrTool.cTrim(prtNo))) {
			mError = "获取保单印刷号失败。";
			return false;
		}

		String sql = "select 1 from lccont where prtno = '"
				+ prtNo
				+ "'"
				+ " union "
				+ " select 1 from lbcont where prtno = '"
				+ prtNo
				+ "'"
				+ " union "
				+ " select 1 from lwmission where processid='0000000003' and activityid='0000001099' and missionprop1 = '"
				+ prtNo + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {// 第一次核保，保单号为空，以便后面生成保单号吗
			mError = "保单印刷号已存在，不可再次使用。";
			return false;
		}
		return true;
	}

	private void bulidDocument() {
		DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
		dfactory.setNamespaceAware(true);

		try {
			m_doc = dfactory.newDocumentBuilder().newDocument();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private org.w3c.dom.Node transformNode(org.w3c.dom.Node node)
			throws Exception {
		Node nodeNew = m_doc.importNode(node, true);

		return nodeNew;
	}

	private boolean DiskContImport(String contIndex) {
		LCInsuredSchema insuredSchema = null;
		MMap subMap = null; // 提交结果集缓存
		boolean state = true; // 导入状态，
		boolean saveState = true;
		String logRiskCode = "";
		String logPolId = "";
		String insuredIndex = "";
		subMap = null;
		int Skind=0;

		// 全局变量，每个合同重新清空
		mContNo = "";
		mManageCom = "";
		mAppntNo = "";
		mAppntName = "";
		mAgentCode = "";
		mImportDate = "";
		iSequenceNo = 1;
		this.mRiskSeqNo = 1;

		// 根据合同ID取得该合同ID下的险种保单数据集
		VData tContPolData = (VData) m_LCPolImpInfo.getContPolData(contIndex);

		if (tContPolData != null) {
			LCPolSchema tLCPolSchema = null;
			VData tPolData = null;
			for (int u = 0; u < tContPolData.size(); u++) {
				tPolData = (VData) tContPolData.get(u);

				if (state == false) {
					break;
				}
				/** 需要取所有险种最小的缴费频次 */
				for (int i = 0; i < tPolData.size(); i++) {
					LCPolSchema tmpLCPolSchema = (LCPolSchema) ((VData) tPolData.get(i)).getObjectByObjectName("LCPolSchema", 0);
					if (mPayIntv < tmpLCPolSchema.getPayIntv()) {
						mPayIntv = tmpLCPolSchema.getPayIntv();
					}
				}

				// 总保费/豁免险总保额
				double sumPrem = 0.0;
				for (int i = 0; i < tPolData.size(); i++) {
					// 清空，避免重复使用
					logRiskCode = "";
					logPolId = "";

					if (state == false) {
						break;
					}

					VData onePolData = (VData) tPolData.get(i);
					if (onePolData == null) {
						continue;
					}
					// 缓存保单数据备用
					tLCPolSchema = (LCPolSchema) onePolData.getObjectByObjectName("LCPolSchema", 0);

					String riskcode = tLCPolSchema.getRiskCode();
					// 判断是否为豁免险 kingcode=S 为豁免险
					String sql = "select 1 from lmriskapp where riskcode='" + riskcode + "' and kindcode='S'";
					ExeSQL tExeSQL = new ExeSQL();
					if ("1".equals(tExeSQL.getOneValue(sql))) {
						tLCPolSchema.setAmnt(0);
						Skind=1;
					}

					logRiskCode = tLCPolSchema.getRiskCode();
					insuredIndex = tLCPolSchema.getInsuredNo();
					contIndex = tLCPolSchema.getContNo();

					VData tPrepareData = prepareProposalBLData(onePolData);
					if (tPrepareData == null) {
						System.out.println("向ProposalBL准备数据的时候出错！");
						state = false;
						return false;
					} else {
						delbl = true;
						MMap tMap = submitDatatToProposalBL(tPrepareData);
						if (tMap == null) {
							state = false;
						} else {
							LCPolSchema tLCPol = (LCPolSchema) tMap.getObjectByObjectName("LCPolSchema", 1);
							sumPrem=sumPrem+tLCPol.getPrem();
							sumPrem = PubFun.setPrecision(sumPrem, "0.00");
						}
						if (subMap == null) {
							subMap = tMap;
						} else {
							subMap.add(tMap);
						}
					}
				}
				if(Skind==0){
					if(sumPrem!=padPrem){
						mError = "保费计算不一致，请与相关人员联系！原因是："+mError;
						return false;
					}
				}				
			}
		}

		if (state == false) {
			saveState = false;
			return false;
		}

		// ======ADD===2005-03-22======ZHANGTAO============BGN========================
		// 客户告知信息处理
		LCCustomerImpartDetailSet tImpartDetailSet = m_LCPolImpInfo	.findImpartDetailSet(contIndex);
		LCCustomerImpartDetailSet newImpartDetailSet = new LCCustomerImpartDetailSet();

		LCCustomerImpartSet tImpartSet = new LCCustomerImpartSet();
		LCCustomerImpartSchema tImpartSchema = new LCCustomerImpartSchema();
		LCCustomerImpartParamsSet tImpartParamsSet = new LCCustomerImpartParamsSet();
		LCContSchema tLCContSchema = m_LCPolImpInfo	.findLCContfromCache(contIndex);

		Reflections ref = new Reflections();

		String strNo;
		String strCustomerID = "";
		String strCustomerNo = "";
		String strCustomerNoType = "";

		if (tImpartDetailSet != null && tImpartDetailSet.size() > 0) {
			int ttNo = 0;
			for (int t = 1; t <= tImpartDetailSet.size(); t++) {//报文中的告知集合
				LCCustomerImpartDetailSchema tImpartDetailSchema = new LCCustomerImpartDetailSchema();
				LCCustomerImpartSchema mImpartSchema = new LCCustomerImpartSchema();
				tImpartDetailSchema.setSchema(tImpartDetailSet.get(t));
				for (int count = 1; count <= tImpartDetailSet.size(); count++) {
					if (StrTool.cTrim(tImpartDetailSet.get(t).getImpartVer())
							.equals(tImpartDetailSet.get(count).getImpartVer())
							& StrTool.cTrim(
									tImpartDetailSet.get(t).getImpartCode())
									.equals(
											tImpartDetailSet.get(count)
													.getImpartCode())
							& StrTool
									.cTrim(
											tImpartDetailSet.get(t)
													.getDiseaseContent())
									.equals(
											tImpartDetailSet.get(count)
													.getDiseaseContent())
							& StrTool.cTrim(
									tImpartDetailSet.get(t).getStartDate())
									.equals(
											tImpartDetailSet.get(count)
													.getStartDate())
							& StrTool.cTrim(
									tImpartDetailSet.get(t).getEndDate())
									.equals(
											tImpartDetailSet.get(count)
													.getEndDate())
							& StrTool
									.cTrim(tImpartDetailSet.get(t).getProver())
									.equals(
											tImpartDetailSet.get(count)
													.getProver())
							& StrTool.cTrim(
									tImpartDetailSet.get(t).getCurrCondition())
									.equals(
											tImpartDetailSet.get(count)
													.getCurrCondition())
							& StrTool.cTrim(
									tImpartDetailSet.get(t).getIsProved())
									.equals(
											tImpartDetailSet.get(count)
													.getIsProved())
							& StrTool.cTrim(
									tImpartDetailSet.get(t)
											.getImpartDetailContent()).equals(
									tImpartDetailSet.get(count)
											.getImpartDetailContent())
							& count != t) {
						// @@错误处理
						mError = "被保人ID：" + strCustomerID
								+ "健康状况告知中的告知版本，告知编码，告知内容，疾病内容录入重复，请检查！";
						state = false;
						saveState = false;
						return false;
					}
				}

				strCustomerID = tImpartDetailSchema.getCustomerNo();
				strCustomerNoType = tImpartDetailSchema.getCustomerNoType();
				if ("0".equals(strCustomerNoType)) {
					// 投保人
					LCAppntSchema ttLCAppntSchema = m_LCPolImpInfo.findAppntfromCache(strCustomerID);
					if (ttLCAppntSchema == null) {
						mError = "投保人信息查询失败！";
						state = false;
						saveState = false;
						return false;

					}
					strCustomerNo = ttLCAppntSchema.getAppntNo();
				}
				if ("I".equals(strCustomerNoType)) {
					// 被保人
					LCInsuredSchema ttLCInsuredSchema = m_LCPolImpInfo
							.findInsuredfromCache(strCustomerID);
					if (ttLCInsuredSchema == null) {
						mError = "被保人信息查询失败！" + "被保人ID：" + strCustomerID;
						state = false;
						saveState = false;
						return false;

					}
					strCustomerNo = ttLCInsuredSchema.getInsuredNo();
				}

				tImpartDetailSchema.setGrpContNo(tLCContSchema.getGrpContNo());
				tImpartDetailSchema.setContNo(tLCContSchema.getContNo());
				tImpartDetailSchema.setProposalContNo(tLCContSchema.getProposalContNo());
				tImpartDetailSchema.setPrtNo(tLCContSchema.getPrtNo());

				tImpartDetailSchema.setSubSerialNo(String.valueOf(t));
				tImpartDetailSchema.setCustomerNo(strCustomerNo);

				tImpartDetailSchema.setOperator(mGlobalInput.Operator);
				tImpartDetailSchema.setMakeDate(PubFun.getCurrentDate());
				tImpartDetailSchema.setMakeTime(PubFun.getCurrentTime());
				tImpartDetailSchema.setModifyDate(PubFun.getCurrentDate());
				tImpartDetailSchema.setModifyTime(PubFun.getCurrentTime());
				// 同样告知编码不进行保存
				int SameFlag = 0;
				if (tImpartSet.size() > 0) {
					for (int c1 = 1; c1 <= tImpartSet.size(); c1++) {
						if (StrTool.cTrim(tImpartSet.get(c1).getImpartCode())
								.equals(tImpartDetailSchema.getImpartCode())
								& StrTool.cTrim(
										tImpartSet.get(c1).getImpartVer())
										.equals(
												tImpartDetailSchema
														.getImpartVer())
								& StrTool.cTrim(
										tImpartSet.get(c1).getProposalContNo())
										.equals(
												tImpartDetailSchema
														.getProposalContNo())
								& StrTool.cTrim(
										tImpartSet.get(c1).getGrpContNo())
										.equals(
												tImpartDetailSchema
														.getGrpContNo())
								& StrTool.cTrim(
										tImpartSet.get(c1).getCustomerNo())
										.equals(
												tImpartDetailSchema
														.getCustomerNo())
								& StrTool.cTrim(
										tImpartSet.get(c1).getCustomerNoType())
										.equals(
												tImpartDetailSchema
														.getCustomerNoType())) {
							SameFlag = 1;
							break;
						}
					}
					if (SameFlag != 1) {
						ref.transFields(mImpartSchema, tImpartDetailSchema);
					}
				}
				if (tImpartSet == null | tImpartSet.size() == 0) {
					ref.transFields(mImpartSchema, tImpartDetailSchema);
				}
				mImpartSchema.setImpartParamModle(tImpartDetailSchema
						.getImpartDetailContent());
				LDImpartDB tLDImpartDB = new LDImpartDB();
				tLDImpartDB.setImpartVer(tImpartDetailSchema.getImpartVer());
				tLDImpartDB.setImpartCode(tImpartDetailSchema.getImpartCode());
				if (!tLDImpartDB.getInfo()) {
					mError = "客户告知内容查询失败！" + "请检查告知版别和告知编码是否正确!" + "被保人ID：" + strCustomerID;
					state = false;
					saveState = false;
					return false;

				}
				tImpartDetailSchema.setImpartDetailContent(tLDImpartDB
						.getImpartContent());

				mImpartSchema.setImpartContent(tImpartDetailSchema
						.getImpartDetailContent());

				if (tImpartDetailSchema.getDiseaseContent() != null
						&& !"".equals(tImpartDetailSchema.getDiseaseContent())) {
					newImpartDetailSet.add(tImpartDetailSchema);
				}
				if (SameFlag != 1) {
					tImpartSet.add(mImpartSchema);
				}
			}

			CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
			VData tempVData = new VData();
			tempVData.add(tImpartSet);
			tempVData.add(mGlobalInput);
			mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
			if (mCustomerImpartBL.mErrors.needDealError()) {
				mError = mCustomerImpartBL.mErrors.getFirstError();
				state = false;
				saveState = false;
				return false;
			}

			tempVData.clear();
			tempVData = mCustomerImpartBL.getResult();
			if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
					"LCCustomerImpartSet", 0)) {
				tImpartSet = (LCCustomerImpartSet) tempVData
						.getObjectByObjectName("LCCustomerImpartSet", 0);
				System.out.println("告知条数" + tImpartSet.size());
			} else {
				System.out.println("告知条数为空");
			}
			if (null != (LCCustomerImpartParamsSet) tempVData
					.getObjectByObjectName("LCCustomerImpartParamsSet", 0)) {
				tImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
						.getObjectByObjectName("LCCustomerImpartParamsSet", 0);
			}

		}

		subMap.put(tImpartSet, "INSERT");
		subMap.put(tImpartParamsSet, "INSERT");
		if (newImpartDetailSet != null && newImpartDetailSet.size() > 0) {
			subMap.put(newImpartDetailSet, "INSERT");
		}
		// ======ADD===2005-03-22======ZHANGTAO============BGN========================

		if (state == false) {
			saveState = false;
			return false;
		}

		// 准备新单复核的工作流
		if (true) {
			MMap missionMap = new MMap();
			mMissionid = PubFun1.CreateMaxNo("MissionID", 20);
			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			tLWMissionSchema.setMissionID(mMissionid);
			tLWMissionSchema.setSubMissionID("1");
			tLWMissionSchema.setProcessID(STATIC_PROCESSID);
			tLWMissionSchema.setActivityID(STATIC_ActivityID);
			tLWMissionSchema.setActivityStatus("1");
			tLWMissionSchema.setMissionProp1(mContNo);
			tLWMissionSchema.setMissionProp2(mPrtNo);
			tLWMissionSchema.setMissionProp3(mAppntNo);
			tLWMissionSchema.setMissionProp4(mAppntName);
			tLWMissionSchema.setMissionProp5(mGlobalInput.Operator);
			tLWMissionSchema.setMissionProp6(mImportDate);
			tLWMissionSchema.setMissionProp7(mAgentCode);
			tLWMissionSchema.setMissionProp8(mManageCom);
			tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
			tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
			tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
			tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
			tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
			tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
			missionMap.put(tLWMissionSchema, "INSERT");
			LWMissionSchema ttLWMissionSchema = new LWMissionSchema();
			ttLWMissionSchema.setMissionID(mMissionid);
			ttLWMissionSchema.setSubMissionID("1");
			ttLWMissionSchema.setProcessID(STATIC_PROCESSID);
			ttLWMissionSchema.setActivityID(STATIC_EActivityID);
			ttLWMissionSchema.setActivityStatus("1");
			ttLWMissionSchema.setMissionProp1(mPrtNo);
			ttLWMissionSchema.setMissionProp2(mContNo);
			ttLWMissionSchema.setLastOperator(mGlobalInput.Operator);
			ttLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
			ttLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
			ttLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
			ttLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
			ttLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
			missionMap.put(ttLWMissionSchema, "INSERT");
			if (missionMap == null) {
				state = false;
			} else {
				subMap.add(missionMap);
			}

			if (state == false) {
				saveState = false;
				return false;
			}
		}

		boolean bs = true;
			if (state && subMap != null && subMap.keySet().size() > 0) {
				MMap logMap = m_LCPolImpInfo.logSucc(mBatchNo, contIndex, mPrtNo,	mContNo, insuredIndex, mGlobalInput);
				subMap.add(logMap);
					if(!("".equals(PrintType)&&!(PrintType==null)&&!("null".equals(PrintType)))){
						if(!"0".equals(PrintType) && !"1".equals(PrintType)){
							mError = "传入的保单打印类型必须是“0”或“1”!";
							return false;
						}
						LCContSubSchema tLCContSubSchema = new LCContSubSchema();
						LCContSubDB tLCContSubDB = new LCContSubDB();
						tLCContSubDB.setPrtNo(mPrtNo);
						boolean exists = tLCContSubDB.getInfo();
							if (exists) {
								String mlccontsubsql = "UPDATE lccontsub set PrintType= '"+PrintType+"' where prtno='"+ mPrtNo + "'";
					         	boolean flag=new ExeSQL().execUpdateSQL(mlccontsubsql);
					            if (!flag)
					            {
					            	mError = "更新lccontsub表信息失败!";
									return false;
					            }
							}else {
								if(null !=tLCContSubSet && tLCContSubSet.size() !=0){
									tLCContSubSchema.setSchema(tLCContSubSet.get(1));
								}
								tLCContSubSchema.setPrtNo(mPrtNo);
								tLCContSubSchema.setManageCom(mManageCom);
								tLCContSubSchema.setOperator(mGlobalInput.Operator);
								tLCContSubSchema.setMakeDate(PubFun.getCurrentDate());
								tLCContSubSchema.setMakeTime(PubFun.getCurrentTime());
								tLCContSubSchema.setModifyDate(PubFun.getCurrentDate());
								tLCContSubSchema.setModifyTime(PubFun.getCurrentTime());
								tLCContSubSchema.setPrintType(PrintType);
								tLCContSubSchema.setAgreedPayFlag(mAgreedPayFlag);
								tLCContSubSchema.setAgreedPayDate(mAgreedPayDate);
								subMap.put(tLCContSubSchema, "INSERT");
						}
					}else {
						mError = "获取保单打印类型失败！";
						return false;
					}
				// 导入所有准备的Map
				bs = batchSave(subMap);
			}

			if (!bs || !state) {
				mError = "导入合同信息失败！";
				saveState = false;
				} else {
					if(Skind!=0){
						LCPolSchema tLCPolSchema = new LCPolSchema();
			           	tLCPolSchema.setPrtNo(mPrtNo);
			           
			           	VData exemptionVD = new VData();
			    		exemptionVD.addElement(mGlobalInput);
			    		exemptionVD.addElement(tLCPolSchema);
			    		ExemptionRiskBL tExemptionRiskBL = new ExemptionRiskBL();
			    		// 豁免险处理
			    		if(!tExemptionRiskBL.submitData(exemptionVD, "")){
			    			mError = "豁免险保费计算失败！";
							return false;
			    		}
			    		String sumPP = new ExeSQL().getOneValue("select prem from lccont where prtno='"+mPrtNo+"'");
			    		if(Double.parseDouble(sumPP)!=padPrem){
							mError = "保费计算不一致，请与相关人员联系！";
							return false;
						}
					}
				System.out.println("合同[" + contIndex + "]导入成功！！");
			}

		return saveState;
	}

	/**
	 * 险种保单数据准备(按险种单导入)
	 * 
	 * @param polData
	 *            VData 险种保单数据集
	 * @return VData 返回数据集
	 */
	private VData prepareProposalBLData(VData polData) {
		VData prepareData = new VData();
		MMap prepareMap = new MMap();

		TransferData tTransferData = (TransferData) polData.getObjectByObjectName("TransferData", 0);

		LCPolSchema tLCPolSchema = (LCPolSchema) polData.getObjectByObjectName("LCPolSchema", 0);

		LCDutySet tLCDutySet = (LCDutySet) polData.getObjectByObjectName(	"LCDutySet", 0);

		// 合同ID
		String strContId = tLCPolSchema.getContNo();
		// 险种ID
		String strPolID = (String) tTransferData.getValueByName("ID");
		// 主险ID
		String strMainPolId = StrTool.cTrim(tLCPolSchema.getMainPolNo());
		// 险种代码
		String strRiskCode = tLCPolSchema.getRiskCode();
		// 投保人Id
		String strAppntId = tLCPolSchema.getAppntNo();
		// 被保人ID
		String strInsuredId = tLCPolSchema.getInsuredNo();
		// 连身被保人ID 缓存在 appflag 字段
		String strRelaInsId = tLCPolSchema.getAppFlag();
		// 借用完及时清空字段
		tLCPolSchema.setAppFlag(null);

		mManageCom = tLCPolSchema.getManageCom();

		// 合同、投保人、被保人
		LCContSchema contSchema = null;
		LCInsuredSchema insuredSchema = null;
		LCAppntSchema appntSchema = null;

		// ************************************************
		// 判断是主险还是附加险
		// 如果为附加险，则直接用主险相关信息设置险种保单的信息
		// 如果为主险，需要查找主险相应的合同、被保人、投保人
		// 利用合同、被保人、投保人来设置险种保单的信息
		// ************************************************

		// 根据代理人编码查询代理人组别
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(tLCPolSchema.getAgentCode());
		if (!tLAAgentDB.getInfo()) {
			mError = "业务员信息查询失败！";
			return null;
		}
		if (Integer.parseInt(tLAAgentDB.getAgentState()) > 2) {
			mError = "业务员已离职！";
			return null;
		}

		// 销售渠道与业务员匹配校验
		String tSaleChnl = "select 1 from LAAgent a, LDCode1 b where a.AgentCode ='"
				+ tLCPolSchema.getAgentCode()
				+ "' "
				+ " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
				+ " and b.CodeType = 'salechnl' and b.Code = '"
				+ tLCPolSchema.getSaleChnl()
				+ "' "
				+ " union all "
				+ " select 1 from LAAgent a where a.AgentCode ='"
				+ tLCPolSchema.getAgentCode()
				+ "' "
				+ "and '10' = '"
				+ tLCPolSchema.getSaleChnl()
				+ "' and a.BranchType2 = '04'  "
				+ "union all "
				+ "select 1 from LAAgent a where a.AgentCode = '"
				+ tLCPolSchema.getAgentCode()
				+ "' "
				+ "and '03' = '"
				+ tLCPolSchema.getSaleChnl() + "' and a.BranchType2 = '04'  ";
		String tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
		if ("".equals(StrTool.cTrim(tSaleChnlc))) {
			mError = "业务员和销售渠道不相符！";
			return null;
		}
		
		tSaleChnl = "select 1 from LAAgent a  where a.AgentCode ='"
			+ tLCPolSchema.getAgentCode()
			+ "' "
			+ " and a.managecom = '"
			+ tLCPolSchema.getManageCom()
			+ "' ";
		if ("".equals(StrTool.cTrim(tSaleChnlc))) {
			mError = "业务员和管理机构不相符！";
			return null;
		}

		if ("04".equals(tLCPolSchema.getSaleChnl())
				|| "03".equals(tLCPolSchema.getSaleChnl())
				|| "10".equals(tLCPolSchema.getSaleChnl())
				|| "15".equals(tLCPolSchema.getSaleChnl())|| "20".equals(tLCPolSchema.getSaleChnl())) {
			if ("".equals(tLCPolSchema.getAgentCom())) {
				mError = "中介机构不能为空！";
				return null;
			}
			if ("04".equals(tLCPolSchema.getSaleChnl())) {
				tSaleChnl = "Select 1 From Lacom Where Managecom Like '"
						+ tLCPolSchema.getManageCom()
						+ "%'"
						+ " And Actype = '01' And (Endflag = 'N' Or Endflag Is Null) And Agentcom Like 'PY%' "
						+ " and agentcom ='" + tLCPolSchema.getAgentCom() + "'";
				tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
				if ("".equals(StrTool.cTrim(tSaleChnlc))) {
					mError = "中介机构不存在！";
					return null;
				}
			}
			tSaleChnl = "select 1 from lacomtoagent where agentcode = '"
					+ tLCPolSchema.getAgentCode() + "' " + " and agentcom = '"
					+ tLCPolSchema.getAgentCom() + "' ";
			tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
			if ("".equals(StrTool.cTrim(tSaleChnlc))) {
				mError = "业务员与中介机构不匹配！";
				return null;
			}
			if(mAgentSaleCode != null && !"".equals(mAgentSaleCode)){
				String bAgentSale = new ExeSQL().getOneValue("select 1 from laagenttemp where AgentCode='" + mAgentSaleCode + "' and entryno = '" + tLCPolSchema.getAgentCom()+ "'");
				if( !"1".equals(bAgentSale) ){
					mError= "代码为:["+mAgentSaleCode+"]的代理销售业务员不存在，请确认!";
					return null;
				}
			}

		}

		tLCPolSchema.setRemark("");
		tLCPolSchema.setAgentGroup(tLAAgentDB.getAgentGroup());

		// 判断是主险还是附加险
		if (!"".equals(strMainPolId) && !strPolID.equals(strMainPolId)) {
			// 为附加险，则查找主险 (主险是在附险之前创建)
			LCPolSchema tMainLCPolSchema = m_LCPolImpInfo.findCacheLCPolSchema(strMainPolId);
			if (tMainLCPolSchema == null) {
				// 主险还没有被创建
				mError = "找不到附加险对应主险信息！";
				return null;
			}

			mainPolBL.setSchema(tMainLCPolSchema);

			// 将主险的相应信息赋给该附加险
			tLCPolSchema.setMainPolNo(tMainLCPolSchema.getPolNo());
			tLCPolSchema.setContNo(StrTool.cTrim(tMainLCPolSchema.getContNo()
					.trim()));
			tLCPolSchema.setInsuredNo(StrTool.cTrim(tMainLCPolSchema
					.getInsuredNo().trim()));
			tLCPolSchema.setAppntNo(StrTool
					.cTrim(tMainLCPolSchema.getAppntNo()));
			tLCPolSchema.setInsuredPeoples("1");
			if (tLCPolSchema.getCValiDate() == null
					|| "".equals(tLCPolSchema.getCValiDate())
					|| "N".equals(tLCPolSchema.getSpecifyValiDate())) {

				dealULIValiDate(tLCPolSchema);
			} else {
				tLCPolSchema.setSpecifyValiDate("1");
			}

			// 为防止错误的校验
			String insuredIndex = tLCPolSchema.getInsuredNo();
			insuredSchema = m_LCPolImpInfo.findInsuredfromCache(strInsuredId);
			if (insuredSchema == null) {
				mError = "新增附加险:找不到对应主险的被保人信息！";
				return null;
			}

		} else {
			// 本身为主险保单,设置主险保单号为空
			tLCPolSchema.setMainPolNo("");

			// 处理生效日期
			if (tLCPolSchema.getCValiDate() == null
					|| "".equals(tLCPolSchema.getCValiDate())
					|| "N".equals(tLCPolSchema.getSpecifyValiDate())) {
				dealULIValiDate(tLCPolSchema);
			} else {
				tLCPolSchema.setSpecifyValiDate("1");
			}
			// --------------------
		}
		// 判断合同是否已经创建过
		contSchema = m_LCPolImpInfo.findLCContfromCache(strContId);
		if (contSchema == null) {
			// *******************************************
			// 合同未创建
			// 创建 合同、被保人、投保人
			// 将 合同、被保人、投保人相互赋值
			// 将新创建的 合同、被保人、投保人缓存起来(cache)
			// 将 合同、被保人、投保人一起扔进 map (insert)
			// *******************************************

			// 检验该印刷号是否已经产生合同(同一印刷号只能产生一条合同)
			if (!m_LCPolImpInfo.checkPrtNO(tLCPolSchema.getPrtNo())) {
				mError = "印刷号["+tLCPolSchema.getPrtNo()+"]已经产生合同";
				return null;
			}
			if (!"00".equals(tLCPolSchema.getMasterPolNo())) {
				mError = "主被保人必须放在最前!";
				return null;

			}
			// 创建 被保人
			if (strInsuredId != null) {
				insuredSchema = m_LCPolImpInfo.prepareInsured(strInsuredId);
				if (insuredSchema == null) {
					// 创建被保人出错
					mError = "在准备被保人["+strInsuredId+"]的时候出现错误";
					return null;
				}
			}
			if (StrTool.cTrim(insuredSchema.getOccupationCode()).equals("")
					|| insuredSchema.getOccupationCode().equals("null")) {
				mError = "被保险人" + insuredSchema.getName() + "没有录入职业代码!";
				return null;
			}
			if (StrTool.cTrim(insuredSchema.getOccupationType()).equals("")
					|| insuredSchema.getOccupationCode().equals("null")) {
				mError = "被保险人" + insuredSchema.getName() + "没有录入职业代码!";
				return null;
			}
			if (StrTool.cTrim(insuredSchema.getName()).equals("")) {
				mError = "没有录入被保险人姓名";
				return null;
			}

			// *******************************************
			// 判断被保人与投保人的关系
			// 如果为同一人，则用被保人信息赋给投保人
			// 不为同一人，则创建投保人
			// *******************************************

			// 判断被保人与投保人的关系
			/**
			 * yangming:上一程序员使用lcpol中的签单机构字段缓存一投保人关系 此方法不好容易混淆.有待改进.
			 */
			LCAppntSchema tempLCAppntSchema = new LCAppntSchema();
			if (m_LCPolImpInfo.prepareAppntAccInfo(strAppntId) == null) {
				mError = "找不到ID为:" + strAppntId + "的投保人！";
				return null;
			} else {
				tempLCAppntSchema.setSchema(m_LCPolImpInfo.prepareAppntAccInfo(strAppntId));
			}
			
			
			//缴费方式校验modify by renwei
			String payMode = tLCPolSchema.getPayMode();
			String existence=new ExeSQL().getOneValue("select 1 from ldcode where codetype = 'paymode' and code = '" + payMode + "' ");
			if(existence==null || "".equals(existence)){
				mError = "缴费方式错误，请核实！";
				return null;
			}
			if("4".equals(payMode)){
				String bankcode = tempLCAppntSchema.getBankCode();
				String bankE = "select 1 from ldbank where bankcode='" + bankcode + "' and comcode like '"
						+ mManageCom.substring(0, 4) + "%'   with ur";
				String canBank = new ExeSQL().getOneValue(bankE);
				if ("".equals(StrTool.cTrim(canBank))) {
					mError = "录入银行不存在！";
					return null;
				}
	
				bankE = "select 1 from ldbankunite where bankcode='" + bankcode
						+ "' and bankunitecode in ('7703','7705','7706','7709','7701','7714') and ( UniteGroupCode in ('1','3','') or UniteGroupCode is null)  with ur";
				canBank = new ExeSQL().getOneValue(bankE);
				if ("".equals(StrTool.cTrim(canBank))) {
					mError = "录入银行不支持实时代收！";
					return null;
				}
			}
			if("8".equals(payMode)){
				String mBankCode = tempLCAppntSchema.getBankCode();
				String medicalCom = new ExeSQL().getOneValue("select 1 from LDMedicalCom "
						+ " where ComCode like '"+mManageCom.substring(0, 4)+"%' "
								+ " and CanSendFlag='1' "
								+ " and MedicalComCode = '"+mBankCode+"' ");
				if(medicalCom == null || "".equals(medicalCom)){
					mError = "医保中心不存在，请核实！";
					return null;	
				}
			}
			if ("00".equals(tLCPolSchema.getSignCom())) {
				// 为同一人，用被保人信息设置投保人
				/**
				 * yangMing: 虽然投保人和被保人是同一个人信息应该一样 但是银行帐户信息的概念不同,被保人银行帐户时理赔金帐户
				 * 与投保帐户有概念上的不同,个人感觉应该注掉此段,但是 考虑到上一个编程者可能另有用意,因此,在此处只把银行帐户
				 * 覆盖回投保人帐户.
				 */
				Reflections ref = new Reflections();
				appntSchema = new LCAppntSchema();
				ref.transFields(appntSchema, insuredSchema);
				appntSchema.setAccName(tempLCAppntSchema.getAccName());
				appntSchema.setBankAccNo(tempLCAppntSchema.getBankAccNo());
				appntSchema.setBankCode(tempLCAppntSchema.getBankCode());
				appntSchema.setPosition(tempLCAppntSchema.getPosition());
				appntSchema.setSalary(tempLCAppntSchema.getSalary());
				appntSchema.setAppntNo(insuredSchema.getInsuredNo());
				appntSchema.setAppntName(insuredSchema.getName());
				appntSchema.setAppntSex(insuredSchema.getSex());
				appntSchema.setAppntBirthday(insuredSchema.getBirthday());
				appntSchema.setAuthorization("1");
				appntSchema.setExiSpec(tempLCAppntSchema.getExiSpec());

				appntSchema.setOperator(mGlobalInput.Operator);
				appntSchema.setMakeDate(PubFun.getCurrentDate());
				appntSchema.setMakeTime(PubFun.getCurrentTime());
				appntSchema.setModifyDate(appntSchema.getMakeDate());
				appntSchema.setModifyTime(appntSchema.getMakeTime());
			} else {
				// 不为同一人,创建投保人
				// 将暂存特约信息的职位清空。

				if (strAppntId != null) {
					appntSchema = m_LCPolImpInfo.prepareAppnt(strAppntId);

					if (appntSchema == null) {
						mError = m_LCPolImpInfo.mErrors.getFirstError();
						// 创建投保人出错
						return null;
					}
					if (StrTool.cTrim(appntSchema.getAppntName()).equals("")) {
						mError = "没有录入投保人姓名！";
						return null;
					}
				}
			}
			// 创建完成投保人信息将投保人客户号填入LCPol表
			tLCPolSchema.setAppntNo(appntSchema.getAppntNo());
			// 设置合同的关键信息
			contSchema = new LCContSchema();
			contSchema.setSchema(m_LCPolImpInfo.getLCContSchema());

			// 处理“续期提醒”标志的默认值，如果不填，视为：否-N
			String tTmpDueFeeMsgFlag = StrTool.cTrim(contSchema
					.getDueFeeMsgFlag());
			if (tTmpDueFeeMsgFlag.equals("")) {
				contSchema.setDueFeeMsgFlag("N");
			}
			// --------------------

			String nolimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
			String strProposalContNo = PubFun1.CreateMaxNo("ProposalContNo",
					nolimit);

			Reflections ref = new Reflections();
			contSchema.setManageCom(tLCPolSchema.getManageCom());
			if (!contSchema.getManageCom().startsWith(mGlobalInput.ManageCom)) {
				mError = "保单管理机构为：" + contSchema.getManageCom() + "当前登陆机构为："
						+ mGlobalInput.ManageCom + "请使用保单管理机构或直属上级机构导入！";
				return null;
			}
			contSchema.setRemark(tempLCAppntSchema.getWorkType());
			contSchema.setPremScope(tempLCAppntSchema.getBMI());
			contSchema.setGrpContNo(STATIC_GRPCONTNO);
			contSchema.setContNo(strProposalContNo);
			contSchema.setProposalContNo(strProposalContNo);
			contSchema.setPrtNo(tLCPolSchema.getPrtNo());
			contSchema.setContType(STATIC_CONTTYPE);
			contSchema.setAppFlag("0");
			contSchema.setStateFlag("0");
			/**
			 * yangming:对保单最新的标志ProposalType 01-普通个单投保书 02-普通团单投保书 03-家庭个单投保书
			 * 04-询价团单投保书 05-银代个单投保书
			 */
			contSchema.setProposalType("03");
			// 个单保单类型默认值
			contSchema.setPolType("0");
			// yangming:签单机构应在签单时填写,不应在现在,因此注掉
			contSchema.setSignCom(mGlobalInput.ManageCom);
			// 家庭单类型为P，PAD出单
//			contSchema.setFamilyType("P");

			contSchema.setManageCom(tLCPolSchema.getManageCom());
			contSchema.setExecuteCom(tLCPolSchema.getManageCom());
			contSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
			contSchema.setAgentCode(tLCPolSchema.getAgentCode());
			contSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
			contSchema.setAgentCom(tLCPolSchema.getAgentCom());
			contSchema.setPolApplyDate(tLCPolSchema.getPolApplyDate());
			contSchema.setCValiDate(tLCPolSchema.getCValiDate());
			contSchema.setPayIntv(this.mPayIntv);
			/** 缴费频次存储完成后将其恢复为12,用于下一被保险人 */
			mPayIntv = 0;
			contSchema.setPayMode(tLCPolSchema.getPayMode());

			// 设置合同的被保人信息
			contSchema.setInsuredNo(insuredSchema.getInsuredNo());
			contSchema.setInsuredName(insuredSchema.getName());
			contSchema.setInsuredSex(insuredSchema.getSex());
			contSchema.setInsuredBirthday(insuredSchema.getBirthday());
			contSchema.setInsuredIDType(insuredSchema.getIDType());
			contSchema.setInsuredIDNo(insuredSchema.getIDNo());

			// 设置合同的投保人信息
			contSchema.setAppntNo(appntSchema.getAppntNo());
			contSchema.setAppntName(appntSchema.getAppntName());
			contSchema.setAppntSex(appntSchema.getAppntSex());
			contSchema.setAppntBirthday(appntSchema.getAppntBirthday());
			contSchema.setAppntIDType(appntSchema.getIDType());
			contSchema.setAppntIDNo(appntSchema.getIDNo());

			// 设置合同的银行账户信息
			contSchema.setBankCode(appntSchema.getBankCode());
			contSchema.setBankAccNo(appntSchema.getBankAccNo());
			contSchema.setAccName(appntSchema.getAccName());

			contSchema.setOperator(mGlobalInput.Operator);
			contSchema.setMakeDate(PubFun.getCurrentDate());
			contSchema.setMakeTime(PubFun.getCurrentTime());
			contSchema.setModifyDate(contSchema.getMakeDate());
			contSchema.setModifyTime(contSchema.getMakeTime());

			// 设置合同的录入时间（代替了新单录入工作流）
			contSchema.setInputDate(PubFun.getCurrentDate());
			contSchema.setInputTime(PubFun.getCurrentTime());
			contSchema.setInputOperator(mGlobalInput.Operator);

			// 设置投保人的有关合同信息
			appntSchema.setGrpContNo(contSchema.getGrpContNo());
			appntSchema.setContNo(contSchema.getContNo());
			appntSchema.setPrtNo(contSchema.getPrtNo());
			appntSchema.setManageCom(contSchema.getManageCom());
				//modify by zxs
			appntSchema.setAuthorization("1");

			// 设置被保人的有关合同信息
			insuredSchema.setSequenceNo("1");
			insuredSchema.setGrpContNo(contSchema.getGrpContNo());
			insuredSchema.setContNo(contSchema.getContNo());
			insuredSchema.setPrtNo(contSchema.getPrtNo());
			insuredSchema.setManageCom(contSchema.getManageCom());
			insuredSchema.setExecuteCom(tLCPolSchema.getManageCom());
			insuredSchema.setAppntNo(appntSchema.getAppntNo());
			insuredSchema.setManageCom(contSchema.getManageCom());
			//modify by zxs 
				insuredSchema.setAuthorization("1");

			if (!"".equals(insuredSchema.getOccupationCode())) {
				LDOccupationDB tLDOccupationDB = new LDOccupationDB();
				tLDOccupationDB.setOccupationCode(insuredSchema
						.getOccupationCode());
				if (!tLDOccupationDB.getInfo()) {
					mError = "被保人职业工种查询失败!" + "被保人:" + insuredSchema.getName()
							+ "职业代码:" + insuredSchema.getOccupationCode();
					return null;
				}
				insuredSchema.setOccupationType(tLDOccupationDB
						.getOccupationType());
			}

			// 与投保人关系 被缓存在 签单机构 字段
			insuredSchema.setRelationToAppnt(tLCPolSchema.getSignCom());
			// 与主被保人关系 缓存在 [主被保人保单号]
			insuredSchema.setRelationToMainInsured(tLCPolSchema
					.getMasterPolNo());

			// 将新创建的 合同、被保人、投保人缓存起来
			m_LCPolImpInfo.cacheLCContSchema(strContId, contSchema);
			m_LCPolImpInfo.cacheLCInsuredSchema(strInsuredId, insuredSchema);
			m_LCPolImpInfo.cacheLCAppntSchema(strAppntId, appntSchema);

			// 将新创建的 合同、被保人、投保人一起扔进 map
			prepareMap.put(contSchema, "INSERT");
			prepareMap.put(insuredSchema, "INSERT");
			prepareMap.put(appntSchema, "INSERT");
		} else {
			// 合同已创建
			// ====== ADD ===== zhangtao ====== 2005-03-17 ============= BGN
			// ===============
			// 创建 被保人
			if (strInsuredId != null) {
				insuredSchema = m_LCPolImpInfo.prepareInsured(strInsuredId);
				if (insuredSchema == null) {
					mError = m_LCPolImpInfo.mErrors.getFirstError();
					// 创建被保人出错
					return null;
				}
			}

			if (!insuredSchema.getContNo().equals(contSchema.getContNo())) {
				if ("00".equals(tLCPolSchema.getMasterPolNo())) {
					mError = "同一合同不能有多个主被保人!";
					return null;

				}

				iSequenceNo++;
				/** 只要新建一个被保险人救就重置险种序号 */
				this.mRiskSeqNo = 1;
				// 设置被保人的有关合同信息
				insuredSchema.setSequenceNo(String.valueOf(iSequenceNo));
				insuredSchema.setGrpContNo(contSchema.getGrpContNo());
				insuredSchema.setContNo(contSchema.getContNo());
				insuredSchema.setPrtNo(contSchema.getPrtNo());
				insuredSchema.setManageCom(contSchema.getManageCom());
				insuredSchema.setExecuteCom(tLCPolSchema.getManageCom());
				insuredSchema.setAppntNo(contSchema.getAppntNo());
				insuredSchema.setManageCom(tLCPolSchema.getManageCom());
				
				//modify by zxs 
				insuredSchema.setAuthorization("1");

				// 与投保人关系 被缓存在 签单机构 字段
				insuredSchema.setRelationToAppnt(tLCPolSchema.getSignCom());
				// 与主被保人关系 缓存在 [主被保人保单号]
				insuredSchema.setRelationToMainInsured(tLCPolSchema
						.getMasterPolNo());

				if (!"".equals(insuredSchema.getOccupationCode())) {
					LDOccupationDB tLDOccupationDB = new LDOccupationDB();
					tLDOccupationDB.setOccupationCode(insuredSchema
							.getOccupationCode());
					if (!tLDOccupationDB.getInfo()) {
						mError = "被保人职业工种查询失败!" + "被保人:"
								+ insuredSchema.getName() + "职业代码:"
								+ insuredSchema.getOccupationCode();
						return null;
					}
					insuredSchema.setOccupationType(tLDOccupationDB
							.getOccupationType());
				}

				// 将新创建的被保人缓存起来
				m_LCPolImpInfo.cacheLCInsuredSchema(strInsuredId, insuredSchema);

				// 将新创建的被保人扔进 map
				prepareMap.put(insuredSchema, "INSERT");

			}

			// ====== ADD ===== zhangtao ====== 2005-03-17 ============= END
			// ===============
			// }

			// *******************************************
			// 从缓存中取出 合同、投保人、被保人
			// 利用合同、被保人、投保人来设定险种保单的信息
			// *******************************************

			// 从缓存中取出 合同
			contSchema = m_LCPolImpInfo.findLCContfromCache(strContId);
			if (contSchema == null) {
				mError = "未找到合同[" + strContId + "]";
				return null;
			}

			// 从缓存中取出 投保人
			appntSchema = m_LCPolImpInfo.findAppntfromCache(strAppntId);
			if (appntSchema == null) {
				mError = "未找到投保人[" + strAppntId + "]";
				return null;
			}

			// 从缓存中取出 被保人
			insuredSchema = m_LCPolImpInfo.findInsuredfromCache(strInsuredId);
			if (insuredSchema == null) {
				mError = "未找到被保险人[" + strInsuredId + "]";
				return null;
			}

			// 利用合同、被保人、投保人来设定险种保单的信息
			tLCPolSchema.setContNo(contSchema.getContNo().trim());
			tLCPolSchema.setPolTypeFlag(contSchema.getPolType());
			tLCPolSchema.setPrtNo(insuredSchema.getPrtNo().trim());
			tLCPolSchema.setInsuredNo(insuredSchema.getInsuredNo());
			tLCPolSchema.setInsuredName(insuredSchema.getName());
			tLCPolSchema.setInsuredSex(insuredSchema.getSex());
			tLCPolSchema.setInsuredBirthday(insuredSchema.getBirthday());
			tLCPolSchema.setOccupationType(insuredSchema.getOccupationType());
			tLCPolSchema.setAppntNo(appntSchema.getAppntNo());
			tLCPolSchema.setAppntName(appntSchema.getAppntName());
			tLCPolSchema.setInsuredPeoples("1");

			// 处理生效日期
			if (tLCPolSchema.getCValiDate() == null
					|| "".equals(tLCPolSchema.getCValiDate())
					|| "N".equals(tLCPolSchema.getSpecifyValiDate())) {
				dealULIValiDate(tLCPolSchema);
			} else {
				tLCPolSchema.setSpecifyValiDate("1");
			}
			// --------------------

			mainPolBL.setSchema(tLCPolSchema);
		}

		// 处理连身被保险人
		LCInsuredRelatedSet tRelaInsSet = null;
		// 连身被保人数组 (连身被保人可能有多个)
		String[] relaIns = null;
		if (!"".equals(StrTool.cTrim(strRelaInsId))) {
			relaIns = strRelaInsId.split(",");
			if (relaIns == null) {
				strRelaInsId.split(";");
			}
			if (relaIns != null) {
				tRelaInsSet = m_LCPolImpInfo.prepareInsuredRela(tLCPolSchema
						.getInsuredNo(), // 主被保人客户号
						relaIns);
				if (tRelaInsSet == null) {
					// 准备连身被保险人出错
					return null;
				}
			}
		}

		// 处理受益人
		LCBnfSet tLCBnfSet = m_LCPolImpInfo.findContBnfSet(strContId,strInsuredId);
		if (tLCBnfSet != null && tLCBnfSet.size() > 0) {
			tLCBnfSet = m_LCPolImpInfo.prepareBnf(tLCBnfSet, insuredSchema);
		}

		// 处理责任项
		LCDutySet tempDutySet = null;
		if (tLCDutySet == null || tLCDutySet.size() <= 1) {
			// 责任项页签的数据为空或少于一条责任项
			// 则以险种保单页签的数据为准
			LCDutySchema ttempDutySchema = new LCDutySchema();
			setDutyByPolInfo(ttempDutySchema, tLCPolSchema);
			tempDutySet = new LCDutySet();
			tempDutySet.add(ttempDutySchema);
		} else {
			tempDutySet = tLCDutySet;
		}

		// 为防止出错的校验
		if (tempDutySet == null || tempDutySet.size() <= 0) {
			mError = "险种[" + strRiskCode + "]责任不能为空";
			return null;
		}

		// 借用完及时清空字段
		tLCPolSchema.setUWCode("");
		tLCPolSchema.setAppFlag(null);
		tLCPolSchema.setApproveFlag("");
		tLCPolSchema.setUWFlag("");
		tLCPolSchema.setSignCom("");
		tLCPolSchema.setPolTypeFlag("");
		tLCPolSchema.setMasterPolNo("");

		// 从缓存中查找险种描述信息
		LMRiskAppSchema tLMRiskAppSchema = m_LCPolImpInfo.findLMRiskAppFromCache(strRiskCode);

		if (tLMRiskAppSchema == null) {
			// 缓存中没有，去数据库查找
			tLMRiskAppSchema = m_LCPolImpInfo.findLMRiskAppFromDB(strRiskCode);

			if (tLMRiskAppSchema == null) {
				mError = strRiskCode + "险种对应的险种承保描述没有找到!";
				return null;
			}
			// 将查找的险种描述信息缓存！！
			m_LCPolImpInfo.cacheLMRiskApp(strRiskCode, tLMRiskAppSchema);
		}

		// 从缓存中查找险种描述信息
		LMRiskSchema tLMRiskSchema = m_LCPolImpInfo.findLMRiskFromCache(strRiskCode);
		if (tLMRiskSchema == null) {
			tLMRiskSchema = m_LCPolImpInfo.findLMRiskFromDB(strRiskCode);

			if (tLMRiskSchema == null) {
				mError = strRiskCode + "险种对应的险种承保描述没有找到!";
				return null;
			}
			// 将查找的险种描述信息缓存！！
			m_LCPolImpInfo.cacheLMRisk(strRiskCode, tLMRiskSchema);
		}

		String PolKey = m_LCPolImpInfo.getPolKey(strContId, strInsuredId,
				strRiskCode);

		tTransferData.setNameAndValue("PolKey", PolKey);
		tTransferData.setNameAndValue("samePersonFlag", 0);
		tTransferData.setNameAndValue("GrpImport", 1);

		// 合同数据创建成功，保存全局变量备用
		mContNo = contSchema.getContNo();
		mManageCom = contSchema.getManageCom();
		// mPrtNo = contSchema.getPrtNo();
		mAppntNo = contSchema.getAppntNo();
		mAppntName = contSchema.getAppntName();
		mAgentCode = contSchema.getAgentCode();
		mImportDate = contSchema.getMakeDate();

		prepareData.add(tTransferData);
		prepareData.add(mGlobalInput);
		prepareData.add(contSchema);
		prepareData.add(insuredSchema);
		prepareData.add(appntSchema);
		prepareData.add(tLCPolSchema);
		prepareData.add(tRelaInsSet);
		prepareData.add(tLCBnfSet);
		prepareData.add(tempDutySet);

		prepareData.add(tLMRiskAppSchema);
		prepareData.add(tLMRiskSchema);
		prepareData.add(mainPolBL);
		prepareData.add(prepareMap);

		return prepareData;
	}

	/**
	 * 根据险种保单的数据设置责任项的数据
	 * 
	 * @param dutySchema
	 *            LCDutySchema
	 * @param tLCPolSchema
	 *            LCPolSchema
	 */
	private void setDutyByPolInfo(LCDutySchema dutySchema,
			LCPolSchema tLCPolSchema) {
		if (tLCPolSchema.getPayIntv() > 0) {
			dutySchema.setPayIntv(tLCPolSchema.getPayIntv());
		}
		if (tLCPolSchema.getInsuYear() > 0) {
			dutySchema.setInsuYear(tLCPolSchema.getInsuYear());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getInsuYearFlag()))) {
			dutySchema.setInsuYearFlag(tLCPolSchema.getInsuYearFlag());
		}
		if (tLCPolSchema.getPayEndYear() > 0) {
			dutySchema.setPayEndYear(tLCPolSchema.getPayEndYear());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getPayEndYearFlag()))) {
			dutySchema.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
		}
		if (tLCPolSchema.getGetYear() > 0) {
			dutySchema.setGetYear(tLCPolSchema.getGetYear());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getGetYearFlag()))) {
			dutySchema.setGetYearFlag(tLCPolSchema.getGetYearFlag());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getGetStartType()))) {
			dutySchema.setGetStartType(tLCPolSchema.getGetStartType());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getBonusGetMode()))) {
			dutySchema.setBonusGetMode(tLCPolSchema.getBonusGetMode());
		}

		if (!"".equals(StrTool.cTrim(tLCPolSchema.getPremToAmnt()))) {
			dutySchema.setPremToAmnt(tLCPolSchema.getPremToAmnt());
		}
		if (tLCPolSchema.getMult() > 0) {
			dutySchema.setMult(tLCPolSchema.getMult());
		}
		if (tLCPolSchema.getPrem() > 0) {
			dutySchema.setPrem(tLCPolSchema.getPrem());
		}
		if (tLCPolSchema.getAmnt() > 0) {
			dutySchema.setAmnt(tLCPolSchema.getAmnt());
		}
		// 计算规则被缓存在 最终核保人编码 UWCode 字段
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getUWCode()))) {
			dutySchema.setCalRule(tLCPolSchema.getUWCode());
		}
		// 费率
		if (tLCPolSchema.getFloatRate() > 0) {
			dutySchema.setFloatRate(tLCPolSchema.getFloatRate());
		}
		// 免赔额
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getApproveFlag()))) {
			dutySchema.setGetLimit(tLCPolSchema.getApproveFlag());
		}
		// 赔付比例
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getUWFlag()))) {
			dutySchema.setGetRate(tLCPolSchema.getUWFlag());
		}
		// 备用字段
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag1()))) {
			dutySchema.setStandbyFlag1(tLCPolSchema.getStandbyFlag1());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag2()))) {
			dutySchema.setStandbyFlag2(tLCPolSchema.getStandbyFlag2());
		}
		if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag3()))) {
			dutySchema.setStandbyFlag3(tLCPolSchema.getStandbyFlag3());
		}

	}

	/**
	 * 处理万能类险种生效日期，该险种生效日期录入时默认为系统当天。
	 * 
	 * @param tLCPolSchema
	 */
	private boolean dealULIValiDate(LCPolSchema tLCPolSchema) {
		String tPolApplyDate = tLCPolSchema.getPolApplyDate();
		if (tPolApplyDate == null || tPolApplyDate.equals("")) {
			mError = "未录入投保单投保日期！";
			return false;
		}
		tLCPolSchema.setCValiDate(tPolApplyDate);
		tLCPolSchema.setSpecifyValiDate("N");

		return true;
	}

	/**
	 * 将准备好的数据提交给 proposalBL 处理险种保单,计算责任项,保费项,给付项
	 * 
	 * @param prepareData
	 *            VData
	 * @return MMap
	 */
	private MMap submitDatatToProposalBL(VData prepareData) {

		MMap submitMap = new MMap();

		// 取出合同、被保人、投保人的创建信息
		MMap prepareMap = (MMap) prepareData.getObjectByObjectName("MMap", 0);
		if (prepareMap != null) {
			submitMap.add(prepareMap);
		}

		// 险种ID
		String strPolId = (String) ((TransferData) prepareData.getObjectByObjectName("TransferData", 0)).getValueByName("ID");
		// 险种保单关键字
		String PolKey = (String) ((TransferData) prepareData.getObjectByObjectName("TransferData", 0)).getValueByName("PolKey");

		// 从险种保单关键字中解析出合同ID
		String strContId = m_LCPolImpInfo.getContKey(PolKey);

		ProposalBL tProposalBL = new ProposalBL();
		if (!tProposalBL.PrepareSubmitData(prepareData, "INSERT||PROPOSAL")) {
			// 保单提交计算失败
			mError = tProposalBL.mErrors.getContent()+tProposalBL.mErrors.getFirstError();
			return null;
		} else {
			VData resultData = tProposalBL.getSubmitResult();
			if (resultData == null) {
				mError = "保单提交计算失败!";
				return null;
			}

			// 险种保单
			LCPolSchema rPolSchema = (LCPolSchema) resultData.getObjectByObjectName("LCPolSchema", 0);

			rPolSchema.setSaleChnlDetail("01");
			rPolSchema.setAppntNo(mAppntNo);
			rPolSchema.setAppntName(mAppntName);
			rPolSchema.setRiskSeqNo(String.valueOf(this.mRiskSeqNo++));
			// 保费项
			LCPremBLSet rPremBLSet = (LCPremBLSet) resultData.getObjectByObjectName("LCPremBLSet", 0);
			if (rPremBLSet == null) {
				mError = "保单提交计算保费项数据准备有误!";
				return null;
			}
			LCPremSet rPremSet = new LCPremSet();
			for (int i = 1; i <= rPremBLSet.size(); i++) {
				rPremSet.add(rPremBLSet.get(i));
			}

			// 责任项
			LCDutyBLSet rDutyBLSet = (LCDutyBLSet) resultData.getObjectByObjectName("LCDutyBLSet", 0);
			if (rDutyBLSet == null) {
				mError = "保单提交计算责任项数据准备有误!";
				return null;
			}
			LCDutySet rDutySet = new LCDutySet();
			for (int i = 1; i <= rDutyBLSet.size(); i++) {
				rDutySet.add(rDutyBLSet.get(i));
			}

			// 给付项
			LCGetBLSet rGetBLSet = (LCGetBLSet) resultData.getObjectByObjectName("LCGetBLSet", 0);
			if (rGetBLSet == null) {
				mError = "保单提交计算给付项数据准备有误!";
				return null;
			}
			LCGetSet rGetSet = new LCGetSet();
			for (int i = 1; i <= rGetBLSet.size(); i++) {
				rGetSet.add(rGetBLSet.get(i));
			}

			// 受益人
			LCBnfBLSet rBnfBLSet = (LCBnfBLSet) resultData.getObjectByObjectName("LCBnfBLSet", 0);
			LCBnfSet rBnfSet = new LCBnfSet();
			if (rBnfBLSet != null) {
				for (int g = 1; g <= rBnfBLSet.size(); g++) {
					String tSql11 = "select 1 from LMRiskEdorItem where edorcode='BC' and riskcode ='" + rPolSchema.getRiskCode() + "'";
					System.out.println("tSql11" + tSql11);
					SSRS tSSRS = new SSRS();
					ExeSQL tExeSQL = new ExeSQL();
					tSSRS = tExeSQL.execSQL(tSql11);
					System.out.println("tSSRS" + tSSRS.getMaxRow());
					if (tSSRS.getMaxRow() == 1) {
						System.out.println("zhuzhuzhu!!!!!!!!!!!");
						rBnfSet.add(rBnfBLSet.get(g));
					}
				}
			}

			// 连身被保人
			LCInsuredRelatedSet tLCInsuredRelatedSet = (LCInsuredRelatedSet) resultData.getObjectByObjectName("LCInsuredRelatedSet", 0);

			LCContSchema tLCContSchema = m_LCPolImpInfo.findLCContfromCache(strContId);
			if (tLCContSchema == null) {
				mError = "查找合同[" + strContId + "]失败";
				return null;
			}

			// //取出合同号
			// mContNo = tLCContSchema.getContNo();

			// 更新合同的相关数据
			tLCContSchema.setPrem(PubFun.setPrecision(tLCContSchema.getPrem() + rPolSchema.getPrem(), "0.00"));
			tLCContSchema.setAmnt(PubFun.setPrecision(tLCContSchema.getAmnt() + rPolSchema.getAmnt(), "0.00"));
			tLCContSchema.setSumPrem(PubFun.setPrecision(tLCContSchema.getSumPrem() + rPolSchema.getSumPrem(), "0.00"));
			tLCContSchema.setMult(PubFun.setPrecision(tLCContSchema.getMult() + rPolSchema.getMult(), "0.00"));
			tLCContSchema.setCardFlag("0");
			tLCContSchema.setSaleChnlDetail("01");

			// 将更新过的合同信息重新缓存
			m_LCPolImpInfo.cacheLCContSchema(strContId, tLCContSchema);

			// 防止引用同一对象
			LCContSchema tContSchema = new LCContSchema();
			tContSchema.setSchema(tLCContSchema);
			LCContSet tContSet = new LCContSet();
			tContSet.add(tContSchema);

			submitMap.put(tContSet, "UPDATE");
			submitMap.put(rPolSchema, "INSERT");
			submitMap.put(rPremSet, "INSERT");
			submitMap.put(rDutySet, "INSERT");
			submitMap.put(rGetSet, "INSERT");
			submitMap.put(rBnfSet, "INSERT");
			submitMap.put(tLCInsuredRelatedSet, "INSERT");

			Date date = new Date();
			Random rd = new Random(date.getTime());
			long u = rd.nextLong();

			StringBuffer sbSql = new StringBuffer();
			sbSql.append(" update lccont set ").append(" peoples = ( ").append(
					" select count(distinct insuredno) from lcpol ").append(
					" where ").append(u).append(" = ").append(u).append(
					" and contno = '").append(tContSchema.getContNo()).append(
					"')").append(" where PolType = '0' and contno = '").append(
					tContSchema.getContNo()).append("'");

			submitMap.put(sbSql.toString(), "UPDATE");

			// 缓存险种保单信息
			m_LCPolImpInfo.cachePolInfo(strPolId, rPolSchema);

		}

		return submitMap;
	}

	private boolean batchSave(MMap map) {
		PubSubmit pubSubmit = new PubSubmit();
		VData sData = new VData();
		sData.add(map);
		boolean tr = pubSubmit.submitData(sData, "");

		if (!tr) {
			if (pubSubmit.mErrors.getErrorCount() > 0) {
				// 错误回退
				// mErrors.copyAllErrors(pubSubmit.mErrors);
				pubSubmit.mErrors.clearErrors();
			} else {
				CError.buildErr(this, "保存数据库的时候失败！");
			}
			return false;
		}
		return true;
	}

	private boolean inputConfirm(String cBPOBatchNo, String cPrtNo) {

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setPrtNo(cPrtNo);
		LCContSet tLCContSet = tLCContDB.query();
		if (tLCContSet.size() == 0) {
			mError = "没有查询到以下印刷号的保单" + cPrtNo;
			return false;
		}

		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("ContNo", tLCContSet.get(1).getContNo());
		mTransferData.setNameAndValue("PrtNo", cPrtNo);
		mTransferData
				.setNameAndValue("AppntNo", tLCContSet.get(1).getAppntNo());
		mTransferData.setNameAndValue("AppntName", tLCContSet.get(1)
				.getAppntName());
		mTransferData.setNameAndValue("AgentCode", tLCContSet.get(1)
				.getAgentCode());
		mTransferData.setNameAndValue("ManageCom", tLCContSet.get(1)
				.getManageCom());
		mTransferData.setNameAndValue("Operator", mGlobalInput.Operator);
		mTransferData.setNameAndValue("MakeDate", tLCContSet.get(1)
				.getMakeDate());
		mTransferData.setNameAndValue("MissionID", mMissionid);
		mTransferData.setNameAndValue("SubMissionID", "1");
		
		// add by 2016-08-22 liyt MagNum Json 
//		if(magnumJsonFlag){
//			mTransferData.setNameAndValue("MagNumJson", mMagNumJson);
//		}
		mTransferData.setNameAndValue("MagNumJson", mMagNumJson);
		VData tVData = new VData();
		tVData.add(mTransferData);
		tVData.add(mGlobalInput);
		TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
		if (!tTbWorkFlowUI.submitData(tVData, STATIC_ActivityID)) {
			mError = tTbWorkFlowUI.mErrors.getError(0).errorMessage;
			return false;
		}

		return true;
	}

	public MMap createMission(VData missionData, String activityid) {
		ActivityOperator tActivityOperator = new ActivityOperator();
		boolean missionCreate = tActivityOperator.CreateStartMission(
				STATIC_PROCESSID, activityid, missionData);
		if (!missionCreate) {
			CError.buildErr(this, "创建工作流节点失败");
			return null;
		}

		MMap missionMap = (MMap) tActivityOperator.getResult()
				.getObjectByObjectName("MMap", 0);

		return missionMap;
	}

	private void main() {
		inputConfirm("", "PAD00000001");

	}

	//投保人电话重复校验
    public String checkAppnt(LCAddressSet appntSet,LCAppntSet tLCAppntSet,LCContSchema tLCContSchema){
    	String phone=appntSet.get(1).getPhone();
    	String mobile=appntSet.get(1).getMobile();
    	String homephone=appntSet.get(1).getHomePhone();
    	String name=tLCAppntSet.get(1).getAppntName();
    	String idType=tLCAppntSet.get(1).getIDType();
    	String id=tLCAppntSet.get(1).getIDNo();

    	if (!isNull(phone)) {
			String phoneSql = "select count(distinct customerno) from lcaddress "
					+ "where phone='"+ phone + "' "
					+ " and customerno not in (select customerno from ldperson where name='"+name+"' and idno='"+id+"' and idtype='"+idType+"') "
					+ " and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
			SSRS result = new ExeSQL().execSQL(phoneSql);
			if (result!=null && result.getMaxRow()>0) {
				int count = Integer.parseInt(result.GetText(1, 1));
				if (count >= 2) {
					mError="该投保人联系电话已在三个以上不同投保人的保单中出现，请核实！";
					return mError;
				}
			}
		}

		if (!isNull(mobile)) {
			String mobilSql = "select count(distinct customerno) from lcaddress "
					+ " where mobile='"+ mobile + "' "
					+ " and customerno not in (select customerno from ldperson where name='"+name+"' and idno='"+id+"' and idtype='"+idType+"') "
					+ " and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
			SSRS result = new ExeSQL().execSQL(mobilSql);
			if (result!=null && result.getMaxRow()>0) {
				int count = Integer.parseInt(result.GetText(1, 1));
				if (count >= 2) {
					mError="该投保人移动电话已在三个以上不同投保人的保单中出现，请核实！";
					return mError;
				}
			}
		}

		if (!isNull(homephone)) {
			String mobilSql = "select count(distinct customerno) from lcaddress "
					+ " where homephone='"+ homephone + "' "
					+ " and customerno not in (select customerno from ldperson where name='"+name+"' and idno='"+id+"' and idtype='"+idType+"') "
					+ " and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
			SSRS result = new ExeSQL().execSQL(mobilSql);
			if (result!=null && result.getMaxRow()>0) {
				int count = Integer.parseInt(result.GetText(1, 1));
				if (count >= 2) {
					mError="该投保人家庭电话已在三个以上不同投保人的保单中出现，请核实！";
					return mError;
				}
			}
		}
	
//		if (!isNull(phone)) {
//			String phoneSql1 = "select groupagentcode from laagent where phone='"
//					+ phone + "' and idno <> '" + id + "' with ur";
//			SSRS result1 = new ExeSQL().execSQL(phoneSql1);
//			if (result1!=null && result1.getMaxRow()>0) {
//				mError="该投保人联系电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//			String phoneSql2 = "select groupagentcode from laagent where mobile='"
//					+ phone + "' and idno <> '" + id + "' with ur";
//			SSRS result2 = new ExeSQL().execSQL(phoneSql2);
//			if (result2!=null && result2.getMaxRow()>0) {
//				mError="该投保人联系电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//			String phoneSql3 = "select agentcode from laagenttemp where phone='"
//					+ phone + "' and name <> '" + name + "' with ur";
//			SSRS result3 = new ExeSQL().execSQL(phoneSql3);
//			if (result3!=null && result3.getMaxRow()>0) {
//				mError="该投保人联系电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//		}
//
//		if (!isNull(mobile)) {
//			String mobileSql1 = "select groupagentcode from laagent where mobile='"
//					+ mobile + "' and idno <> '" + id + "' with ur";
//			SSRS result1 = new ExeSQL().execSQL(mobileSql1);
//			if (result1!=null && result1.getMaxRow()>0) {
//				mError="该投保人移动电话与本方业务员" + result1.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//			String mobileSql2 = "select groupagentcode from laagent where phone='"
//					+ mobile + "' and idno <> '" + id + "' with ur";
//			SSRS result2 = new ExeSQL().execSQL(mobileSql2);
//			if (result2!=null && result2.getMaxRow()>0) {
//				mError="该投保人移动电话与本方业务员" + result2.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//			String mobileSql3 = "select agentcode from laagenttemp where mobile='"
//					+ mobile + "' and name <> '" + name + "' with ur";
//			SSRS result3 = new ExeSQL().execSQL(mobileSql3);
//			if (result3!=null && result3.getMaxRow()>0) {
//				mError="该投保人移动电话与代理销售业务员" + result3.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//		}
//		if (!isNull(homephone)) {
//			String homephoneSql1 = "select groupagentcode from laagent where phone='"
//					+ homephone + "' and idno <> '" + id + "' with ur";
//			SSRS result1 = new ExeSQL().execSQL(homephoneSql1);
//			if (result1!=null && result1.getMaxRow()>0) {
//				mError="该投保人固定电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//			String homephoneSql2 = "select groupagentcode from laagent where mobile='"
//					+ homephone + "' and idno <> '" + id + "' with ur";
//			SSRS result2 = new ExeSQL().execSQL(homephoneSql2);
//			if (result2!=null && result2.getMaxRow()>0) {
//				mError="该投保人固定电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//			String homephoneSql3 = "select agentcode from laagenttemp where phone='"
//					+ homephone + "' and name <> '" + name + "' with ur";
//			SSRS result3 = new ExeSQL().execSQL(homephoneSql3);
//			if (result3!=null && result3.getMaxRow()>0) {
//				mError="该投保人固定电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//		}
    	return "";
    }

    public String checkInsured(LCAddressSet insuredSet,LCInsuredSet tLCInsuredSet){
    	int size=insuredSet.size();//被保人人数
    	for(int i=1;i<=size;i++){
	    	String phone=insuredSet.get(i).getPhone();
	    	String mobile=insuredSet.get(i).getMobile();
	    	String homephone=insuredSet.get(i).getHomePhone();
	    	String name=tLCInsuredSet.get(i).getName();
	    	String id=tLCInsuredSet.get(i).getIDNo();
	    	if (!isNull(phone)) {
				String phoneSql1 = "select groupagentcode from laagent where phone='"
						+ phone + "' and idno <> '" + id + "' with ur";
				SSRS result1 = new ExeSQL().execSQL(phoneSql1);
				if (result1!=null && result1.getMaxRow()>0) {
					mError="被保人" + name + "联系电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
				String phoneSql2 = "select groupagentcode from laagent where mobile='"
						+ phone + "' and idno <> '" + id + "' with ur";
				SSRS result2 = new ExeSQL().execSQL(phoneSql2);
				if (result2!=null && result2.getMaxRow()>0) {
					mError="被保人" + name + "联系电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
					return mError;
				}
				String phoneSql3 = "select agentcode from laagenttemp where phone='"
						+ phone + "' and name <> '" + name + "' with ur";
				SSRS result3 = new ExeSQL().execSQL(phoneSql3);
				if (result3!=null && result3.getMaxRow()>0) {
					mError="被保人" + name + "联系电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
			}
			if (!isNull(mobile)) {
				String mobileSql1 = "select groupagentcode from laagent where mobile='"
						+ mobile + "' and idno <> '" + id + "' with ur";
				SSRS result1 = new ExeSQL().execSQL(mobileSql1);
				if (result1!=null && result1.getMaxRow()>0) {
					mError="被保人" + name + "移动电话与本方业务员" + result1.GetText(1, 1)
							+ "手机号码相同，请核查！";
					return mError;
				}
				String mobileSql2 = "select groupagentcode from laagent where phone='"
						+ mobile + "' and idno <> '" + id + "' with ur";
				SSRS result2 = new ExeSQL().execSQL(mobileSql2);
				if (result2!=null && result2.getMaxRow()>0) {
					mError="被保人" + name + "移动电话与本方业务员" + result2.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
				String mobileSql3 = "select agentcode from laagenttemp where mobile='"
						+ mobile + "' and name <> '" + name + "' with ur";
				SSRS result3 = new ExeSQL().execSQL(mobileSql3);
				if (result3!=null && result3.getMaxRow()>0) {
					mError="被保人" + name + "移动电话与代理销售业务员" + result1.GetText(1, 1) + "手机号码相同，请核查！";
					return mError;
				}
	
			}
			if (!isNull(homephone)) {
				String homephoneSql1 = "select groupagentcode from laagent where phone='"
						+ homephone + "' and idno <> '" + id + "' with ur";
				SSRS result1 = new ExeSQL().execSQL(homephoneSql1);
				if (result1!=null && result1.getMaxRow()>0) {
					mError="被保人" + name + "固定电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
				String homephoneSql2 = "select groupagentcode from laagent where mobile='"
						+ homephone + "' and idno <> '" + id + "' with ur";
				SSRS result2 = new ExeSQL().execSQL(homephoneSql2);
				if (result2!=null && result2.getMaxRow()>0) {
					mError="被保人" + name + "固定电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
					return mError;
				}
				String homephoneSql3 = "select agentcode from laagenttemp where phone='"
						+ homephone + "' and name <> '" + name + "' with ur";
				SSRS result3 = new ExeSQL().execSQL(homephoneSql3);
				if (result3!=null && result3.getMaxRow()>0) {
					mError="被保人" + name + "固定电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
			}
    	}
    	return "";
    }
    //校验非空公共方法
    public boolean isNull(String checkStr) {
    	if (checkStr == null || "".equals(checkStr)) {
    		return true;
    	}
    	return false;
    }
    //创建共享标识数据
    public boolean createSharedMark(){
         String theCurrentDate = PubFun.getCurrentDate();
         String theCurrentTime = PubFun.getCurrentTime();
         MMap mMap = new MMap();
         mMap.put("delete from LCPersonTrace where  ContractNo='"+mPrtNo+"' ", "DELETE");
         VData vData = new VData();
         vData.add(mMap);
         PubSubmit pubSubmit = new PubSubmit();
         if (!pubSubmit.submitData(vData, ""))
         {
            errLog(pubSubmit.mErrors.getContent());;
            return false;
         }
         mMap = new MMap();
         vData.clear();
    	LCAppntDB tlLcAppntDB = new LCAppntDB();
    	tlLcAppntDB.setPrtNo(mPrtNo);
    	LCAppntSet tLcAppntSet =  tlLcAppntDB.query();
    	LCPersonTraceSchema tLcPersonTraceSchema = new LCPersonTraceSchema();
    	if(mAppntLCPersonTraceSet.size()>0){
    		tLcPersonTraceSchema = mAppntLCPersonTraceSet.get(1);
    	}
    	tLcPersonTraceSchema.setCustomerNo(tLcAppntSet.get(1).getAppntNo());
    	tLcPersonTraceSchema.setContractNo(mPrtNo);
    	 SSRS tSSRS2 = new SSRS();
         String sql1 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                 +tLcPersonTraceSchema.getCustomerNo()+ "'";
         ExeSQL tExeSQL1 = new ExeSQL();
         tSSRS2 = tExeSQL1.execSQL(sql1);
         Integer firstinteger1 = Integer.valueOf(tSSRS2.GetText(1, 1));
         int tTraceNo = firstinteger1.intValue() + 1;
         Integer sTraceNo = new Integer(tTraceNo);
         String mTraceNo = sTraceNo.toString();
         tLcPersonTraceSchema.setTraceNo(mTraceNo);
         tLcPersonTraceSchema.setCompanySource("2");//人保健康
         tLcPersonTraceSchema.setInstitutionSource(tLcAppntSet.get(1).getManageCom());
         tLcPersonTraceSchema.setAuthVersion("1.0");
         tLcPersonTraceSchema.setSendDate(theCurrentDate);
         tLcPersonTraceSchema.setSendTime(theCurrentTime);
         tLcPersonTraceSchema.setModifyDate(theCurrentDate);
         tLcPersonTraceSchema.setModifyTime(theCurrentTime);
         tLcPersonTraceSchema.setSpare1("");
         mMap.put(tLcPersonTraceSchema, "INSERT");
         
         LCInsuredDB tLcInsuredDB = new LCInsuredDB();
         tLcInsuredDB.setPrtNo(mPrtNo);
         LCInsuredSet tLcInsuredSet  = tLcInsuredDB.query();
         LCPersonTraceSchema mLcPersonTraceSchema = new LCPersonTraceSchema();
         if(tLcInsuredSet.size()>0){
        	for(int i=1;i<=tLcInsuredSet.size();i++){
        		for(int j=1;j<=mInsuredLCPersonTraceSet.size();j++){
        			if(tLcInsuredSet.get(i).getName().equals(mInsuredLCPersonTraceSet.get(j).getSpare1())&&!"00".equals(tLcInsuredSet.get(i).getRelationToAppnt())){
        				mLcPersonTraceSchema = mInsuredLCPersonTraceSet.get(j);
        				mLcPersonTraceSchema.setCustomerNo(tLcInsuredSet.get(i).getInsuredNo());
        				mLcPersonTraceSchema.setContractNo(mPrtNo);
        		    	 SSRS tSSRS3 = new SSRS();
        		         String sql2 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
        		                 +mLcPersonTraceSchema.getCustomerNo()+ "'";
        		         ExeSQL tExeSQL2 = new ExeSQL();
        		         tSSRS3 = tExeSQL2.execSQL(sql2);
        		         Integer firstinteger2 = Integer.valueOf(tSSRS3.GetText(1, 1));
        		         int tTraceNo1 = firstinteger2.intValue() + 1;
        		         Integer sTraceNo1 = new Integer(tTraceNo1);
        		         String mTraceNo1 = sTraceNo1.toString();
        		         mLcPersonTraceSchema.setTraceNo(mTraceNo1);
        		         mLcPersonTraceSchema.setCompanySource("2");//人保健康
        		         mLcPersonTraceSchema.setInstitutionSource(tLcInsuredSet.get(i).getManageCom());
        		         mLcPersonTraceSchema.setAuthVersion("1.0");
        		         mLcPersonTraceSchema.setSendDate(theCurrentDate);
        		         mLcPersonTraceSchema.setSendTime(theCurrentTime);
        		         mLcPersonTraceSchema.setModifyDate(theCurrentDate);
        		         mLcPersonTraceSchema.setModifyTime(theCurrentTime);
        		         mLcPersonTraceSchema.setSpare1("");
        		         mMap.put(mLcPersonTraceSchema, "INSERT");
        			}
        		}
        		
        	}
         }
         
         vData.add(mMap);
         PubSubmit pubSubmit1 = new PubSubmit();
         if (!pubSubmit1.submitData(vData, ""))
         {
            errLog(pubSubmit1.mErrors.getContent());;
            return false;
         } 
         mAppntLCPersonTraceSet = new LCPersonTraceSet();
         mInsuredLCPersonTraceSet =  new LCPersonTraceSet();
         mMap=new MMap();
         vData.clear();
    return true;	
    }
}
