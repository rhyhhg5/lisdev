package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.*;

import org.apache.log4j.*;
import org.jdom.Document;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflow.tb.NewEngineRuleService;

import examples.newsgroups;

public class PadContInputBL {
	/** 报错对象 */
	public CErrors mErrors = new CErrors();

	/** 操作完成保存结果 */
	private VData mResult = new VData();

	/** 前台传入的封装对象 */
	private VData mInputData;

	/** 操作符 */
	private String mOperate;

	/** 个人合同信息 */
	private LCContSchema mLCContSchema;

	/** 客户登陆信息 */
	private GlobalInput mGlobalInput;

	/** 被保险人信息 */
	private LCInsuredSchema mLCInsuredSchema;

	/** 受益人信息 */
	private LCBnfSchema mLCBnfSchema;

	/** 客户信息（受益人不算客户） */
	private LDPersonSet mLDPersonSet = new LDPersonSet();

	/** 地址信息 */
	private LCAddressSet mLCAddressSet = new LCAddressSet();

	/** 套餐险种表 */
	private LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

	/** 责任信息 */
	private LCDutySet mLCDutySet;

	/** 创建客户标志 */
	private boolean needCreatAppnt = false;

	/** 险种号码 */
	private String mPolNo;

	/** 投保客户信息 */
	private LCAppntSchema mLCAppntSchema;

	/** 创建被保客户标志 */
	private boolean needCreatInsured = false;

	/** 投保人客户号 */
	private String mAppntNo;

	/** 险种信息 */
	private LCPolSet mLCPolSet;

	/** 被保人客户号 */
	private String mInsuredNo;

	/** 团体合同号 */
	private String mContNo;

	/** 交通意外标志 */
	private String mMissionProp5;

	private String mWorkName;

	/** 保单类型标记* */
	private String mContType;

	/** 险种信息 */
	// private LMRiskAppSet mLMRiskAppSet;
	/** 当前时间 */
	private String mCurrentData = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	/** 缴费信息 */
	private LCPremSet mLCPremSet;

	/** 给付 */
	private LCGetSet mLCGetSet;

	/** 递交数据 */
	private MMap map;

	/** 保单级的保费，保额，档次 */
	private double sumPrem, sumAmnt, sumMult;

	/** 封装地址信息的 */
	private TransferData mTransferData = new TransferData();

	/** 投保人地址信息 */
	private LCAddressSchema mAppntAddressSchema;

	/** 被保人信息 */
	private LCAddressSchema mInsuredAddressSchema;

	/** 生成地址编码标志 */
	private boolean needCreatAddressNo = false;

	/** 抵达国家 */
	private LCNationSet mLCNationSet;

	/** 打印管理表 */
	private LOPRTManagerSchema mLOPRTManagerSchema;

	/** 告知信息 */
	private LCCustomerImpartSchema mLCCustomerImpartSchema;

	/** 前台传入的受益人信息 */
	private LCBnfSet mInputLCBnfSet;

	/** 数据库递交的受益人信息 */
	private LCBnfSet mLCBnfet = new LCBnfSet();

	private LCCustomerImpartSet mCusImpartSet = null;

	private LCCustomerImpartParamsSet mCusImpartParamsSet = null;

	private LCCustomerImpartDetailSet mCusImpartDetailSet = null;

	/** 需要进行制定保险期间 */
	private boolean needInsuYear = true;

	/** 添加日志对象 */
	private static Logger log = Logger.getLogger(PadContInputBL.class);

	/** 描述套餐要素信息 */
	private LDRiskDutyWrapSet mLDRiskDutyWrapSet = new LDRiskDutyWrapSet();

	// 录入日期
	private String polApplyDate;

	// 报文类型
	private String mMsgType = "";

	// 折扣
	private String FeeRate = "";

	// 红利领取方式
	private String mBonusgetmode = "";
	
	private String tComparePrem = "";

	public PadContInputBL() {
	}

	/**
	 * submitData
	 * 
	 * @param nInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData nInputData, String cOperate) {
		System.out.println("Into BriefSingleContInputBL.submitData()...");
		getSubmitMap(nInputData, cOperate) ;
		

//		PubSubmit ps = new PubSubmit();
//		if (!ps.submitData(this.mResult, "INSERT")) {
//			this.mErrors.copyAllErrors(ps.mErrors);
//			return false;
//		}
		if (!this.mOperate.equals("DELETE||MAIN")) {
			System.out
					.println("contno  is:" + this.mLCContSchema.getContNo());
			ExeSQL tExeSQL = new ExeSQL();
			String tSQL = "select code from ldcode where codetype='QYJYRuleButton' with ur";
			String tRuleButton = tExeSQL.getOneValue(tSQL);
			if (tRuleButton != null
					&& tRuleButton.equals("00")) {

				try {
					// modify by zxs
					NewEngineRuleService newEngineRule = new NewEngineRuleService();
					MMap map = newEngineRule.dealNUData(mLCContSchema, "NewQYJYRuleButton");
					VData mData = new VData();
					// modify by zxs
					// 调用规则引擎
					RuleTransfer ruleTransfer = new RuleTransfer();
					// 获得规则引擎返回的校验信息 调用投保规则
					String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NU",
							this.mLCContSchema.getContNo(), false);
					// 将校验信息转换为Xml的document对象
					Document doc = ruleTransfer.stringToDoc(xmlStr);
					String approved = ruleTransfer.getApproved(doc);
					if (approved == null) {
						CError tError = new CError();
						tError.moduleName = "UWAutoChkBL";
						tError.functionName = "dealData";
						tError.errorMessage = "执行规则引擎时出错";
						this.mErrors.addOneError(tError);
						return false;
						// return true;
					}
					if ("1".equals(approved)) {
					} else if ("0".equals(approved) || "2".equals(approved)) {
					} else if ("-1".equals(approved)) {
						CError tError = new CError();
						tError.moduleName = "UWAutoChkBL";
						tError.functionName = "dealData";
						tError.errorMessage = "规则引擎执行异常";
						this.mErrors.addOneError(tError);
						return false;
					} else {
						CError tError = new CError();
						tError.moduleName = "UWAutoChkBL";
						tError.functionName = "dealData";
						tError.errorMessage = "规则引擎返回了未知的错误类型";
						this.mErrors.addOneError(tError);
						return false;
					}
					LMUWSet tLMUWSetPolAllUnpass = ruleTransfer
							.getAllPolUWMSG(doc); // 未通过的投保规则
					String errorMessage = "";
					String tempRiskcode = "";
					String tempInsured = "";
					if (tLMUWSetPolAllUnpass.size() > 0) {
						for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++) {
							// modify by zxs
							LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
							EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
							engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
							engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
							engineRuleSchema.setApproved(approved);
							engineRuleSchema.setRuleResource("old");
							engineRuleSchema.setRuleType("NU");
							engineRuleSchema.setContType("简易保单");
							engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
							engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
							engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
							engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
							if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
								engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
							}else{
								engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
							}
							engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
							map.put(engineRuleSchema, "INSERT");
							// modify by zxs

							tempRiskcode = "";
							tempInsured = "";
							if (tLMUWSetPolAllUnpass.get(m).getRiskName() != null
									&& !tLMUWSetPolAllUnpass.get(m)
											.getRiskName().equals("")) {
								tempInsured += "被保人"
										+ tLMUWSetPolAllUnpass.get(m)
												.getRiskName();
								if (tLMUWSetPolAllUnpass.get(m).getRiskCode() != null
										&& !tLMUWSetPolAllUnpass.get(m)
												.getRiskCode().equals("000000")
										&& !tLMUWSetPolAllUnpass.get(m)
												.getRiskCode().equals("")) {
									tempRiskcode += "，险种代码"
											+ tLMUWSetPolAllUnpass.get(m)
													.getRiskCode() + "，";
								} else {
									tempInsured += "，";
								}
							} else {
								if (tLMUWSetPolAllUnpass.get(m).getRiskCode() != null
										&& !tLMUWSetPolAllUnpass.get(m)
												.getRiskCode().equals("000000")
										&& !tLMUWSetPolAllUnpass.get(m)
												.getRiskCode().equals("")) {
									tempRiskcode += "险种代码"
											+ tLMUWSetPolAllUnpass.get(m)
													.getRiskCode() + "，";
								}
							}
							errorMessage += tLMUWSetPolAllUnpass.get(m)
									.getUWCode();
							errorMessage += "：";
							errorMessage += tempInsured;
							errorMessage += tempRiskcode;
							errorMessage += tLMUWSetPolAllUnpass.get(m)
									.getRemark();
							errorMessage += "";
						}
						// modify by zxs
						mData.add(map);
						PubSubmit pubSubmit = new PubSubmit();
						if (!pubSubmit.submitData(mData, "")) {
							this.mErrors.addOneError(pubSubmit.mErrors.getContent());
							return false;
						}
						// modify by zxs
						
						// @@错误处理
						CError tError = new CError();
						tError.moduleName = "UWAutoChkBL";
						tError.functionName = "dealData";
						tError.errorMessage = errorMessage;
						this.mErrors.addOneError(tError);
						return false;
					}else{
						EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
						engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
						engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
						engineRuleSchema.setApproved(approved);
						engineRuleSchema.setRuleResource("old");
						engineRuleSchema.setRuleType("NU");
						engineRuleSchema.setContType("简易保单");
						engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
						engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
						map.put(engineRuleSchema, "INSERT");
					}
					// modify by zxs
					mData.add(map);
					PubSubmit pubSubmit = new PubSubmit();
					if (!pubSubmit.submitData(mData, "")) {
						this.mErrors.addOneError(pubSubmit.mErrors.getContent());
						return false;
					}
					// modify by zxs
				} catch (Exception ex) {
					CError tError = new CError();
					tError.moduleName = "UWAutoChkBL";
					tError.functionName = "dealData";
					tError.errorMessage = "执行规则引擎时出错";
					this.mErrors.addOneError(tError);
					return false;
				}
			} else {

			}
		}
		return true;
	}

	/**
	 * 生成简易平台保单信息，并返回MMap结果集
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return MMap
	 */
	public MMap getSubmitMap(VData cInputData, String cOperate) {
		System.out.println("Into BriefSingleContInputBL.getSubmitMap()...");
		this.mInputData = cInputData;
		this.mOperate = cOperate;
		System.out.println("@@完成程序submitData第41行");

		if (!getInputData()) {
			return null;
		}

		if (!checkData()) {
			return null;
		}

		if (!dealData()) {
			return null;
		}
		
		if(!ComparePrem()){
			return null;
		}

		if (!fillApprovUWField()) {
			return null;
		}

//		if (!prepareOutputData()) {
//			return null;
//		}

		return map;
	}

	private boolean ComparePrem() {
		String prem=new ExeSQL().getOneValue("select prem from lccont where contno='"+mLCContSchema.getContNo()+"'");
		if(mLCContSchema.getPrem()!=Double.parseDouble(tComparePrem)){
			buildError("ComparePrem", "保费计算值为"+mLCContSchema.getPrem()+"，与所传保费不一致，请与相关人员联系！");
			return false;
		}
		return true;
	}

	/**
	 * prepareOutputData
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		mResult.add(this.mLCContSchema);
		mResult.add(this.mLCPolSet);
		mResult.add(this.mLCPremSet);
		mResult.add(this.mLCGetSet);
		mResult.add(this.mLCAppntSchema);
		mResult.add(this.mLDPersonSet);

		if (this.mOperate.equals("INSERT||MAIN")
				|| this.mOperate.equals("UPDATE||MAIN")) {
			if (map == null) {
				map = new MMap();
			}
			map.put(this.mLCContSchema, "DELETE&INSERT");
			map.put(this.mLCPolSet, "INSERT");
			map.put(this.mLCDutySet, "INSERT");
			map.put(this.mLCPremSet, "INSERT");
			map.put(this.mLCGetSet, "INSERT");
			map.put(this.mLCAppntSchema, "DELETE&INSERT");
			map.put(this.mLCInsuredSchema, "DELETE&INSERT");
			map.put(this.mLCBnfSchema, "DELETE&INSERT");
			map.put(this.mLDPersonSet, "INSERT");
			map.put(this.mLOPRTManagerSchema, "DELETE&INSERT");
			if (this.mLCAddressSet.size() > 0) {
				for (int i = 1; i <= mLCAddressSet.size(); i++) {
					LCAddressSchema tLCAddressSchema = new LCAddressSchema();
					tLCAddressSchema = mLCAddressSet.get(i);
					map.put(tLCAddressSchema, "DELETE&INSERT");
				}
			}
			if (this.mLCNationSet.size() > 0) {
				map.put(this.mLCNationSet, "DELETE&INSERT");
			}
			if (this.mLCCustomerImpartSchema != null) {
				map.put(mLCCustomerImpartSchema, "DELETE&INSERT");
			}
			if (this.mLCBnfet != null && this.mLCBnfet.size() > 0) {
				for (int i = 1; i <= mLCBnfet.size(); i++) {
					System.out.println("测试多受益人 PolNo: "
							+ mLCBnfet.get(i).getPolNo());
				}
				map.put(mLCBnfet, "INSERT");
			}

			// /** 处理特殊字符，否则入库时会报错。
			if (mLCRiskDutyWrapSet != null && mLCRiskDutyWrapSet.size() > 0) {
				for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++) {
					String tStrCalSql = mLCRiskDutyWrapSet.get(i).getCalSql();
					if (tStrCalSql != null)
						tStrCalSql = tStrCalSql.replaceAll("'", "''");
					mLCRiskDutyWrapSet.get(i).setCalSql(tStrCalSql); // CalSql无用，不需要赋值
				}
			}
			// ------------------------------------------------*/

			map.put(mLCRiskDutyWrapSet, "DELETE&INSERT");

			// 处理被保人健康告知
			if (mCusImpartSet != null && mCusImpartSet.size() > 0) {
				map.put(mCusImpartSet, SysConst.DELETE_AND_INSERT);
			}

			if (mCusImpartParamsSet != null && mCusImpartParamsSet.size() > 0) {
				map.put(mCusImpartParamsSet, SysConst.DELETE_AND_INSERT);
			}

			if (mCusImpartDetailSet != null && mCusImpartDetailSet.size() > 0) {
				map.put(mCusImpartDetailSet, SysConst.DELETE_AND_INSERT);
			}
			// --------------------

			mResult.add(map);
		}
		if (this.mOperate.equals("DELETE||MAIN")) {
			if (this.map.size() > 0) {
				this.mResult.add(map);
			} else {
				buildError("prepareOutputData", "进行删除操作,但没有删除数据！");
				return false;
			}
		}
		return true;
	}

	/**
	 * dealData
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		System.out.println("Into BriefSingleContInputBL.dealData()..."
				+ mOperate);
		/** 执行新单录入过程 */
		if (this.mOperate.equals("INSERT||MAIN")) {
			if (!insertData()) {
				return false;
			}
		} else if (this.mOperate.equals("DELETE||MAIN")) {
			if (!deleteData()) {
				return false;
			}
		} else if (this.mOperate.equals("UPDATE||MAIN")) {
			if (!updateData()) {
				return false;
			}
		} else {
			buildError("dealData", "目前只支持保存修改,删除功能不支持其它操作！");
			return false;
		}
		return true;
	}

	/**
	 * updateData
	 * 
	 * @return boolean
	 */
	private boolean updateData() {
		/** 执行删除和执行插入 */
		if (!deleteData()) {
			return false;
		}
		if (!insertData()) {
			return false;
		}

		if (!updatePrtManager()) {
			return false;
		}
		return true;
	}

	/**
	 * updatePrtManager
	 * 
	 * @return boolean
	 */
	private boolean updatePrtManager() {
		/** 处理首期缴费通知书丢失问题 */
		String oldContNo = (new ExeSQL())
				.getOneValue("select contno from lccont where prtno='"
						+ this.mLCContSchema.getPrtNo() + "'");
		if (StrTool.cTrim(oldContNo).equals("")) {
			buildError("updatePrtManager", "查询合同号码失败！");
			return false;
		}
		LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
		tLOPRTManagerDB.setCode("07");
		tLOPRTManagerDB.setOtherNo(oldContNo);
		LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
		if (tLOPRTManagerSet.size() > 1) {
			buildError("updatePrtManager", "查询出缴费通知书有多条！");
			return false;
		}
		if (tLOPRTManagerSet.size() <= 0) {
			return true;
		}
		mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
		mLOPRTManagerSchema.setOtherNo(this.mContNo);
		return true;
	}

	/**
	 * deleteData
	 * 
	 * @return boolean
	 */
	private boolean deleteData() {
		if (!checkData()) {
			return false;
		}
		String wherePart_ContNo = " and ContNo = '"
				+ this.mLCContSchema.getContNo() + "'";
		if (map == null) {
			map = new MMap();
		}
		map.put("delete from lccont where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from lcpol where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from lcduty where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from lcprem where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from lcget where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from lcappnt where 1=1 " + wherePart_ContNo, "DELETE");
		map
				.put("delete from lcinsured where 1=1 " + wherePart_ContNo,
						"DELETE");
		map.put("delete from LCCustomerImpart where 1=1 " + wherePart_ContNo,
				"DELETE");
		map.put("delete from LCCustomerImpartParams where 1=1 "
				+ wherePart_ContNo, "DELETE");
		map.put("delete from LCCustomerImpartDetail where 1=1 "
				+ wherePart_ContNo, "DELETE");
		map.put("delete from LCNation where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from LCBnf where 1=1 " + wherePart_ContNo, "DELETE");
		map.put("delete from LCRiskDutyWrap where 1=1 " + wherePart_ContNo,
				"DELETE");
		return true;
	}

	/**
	 * insertData
	 * 
	 * @return boolean
	 */
	private boolean insertData() {
		System.out.println("Into BriefSingleContInputBL.insertData()...");
//		/** 先处理客户信息，被保人投保人 */
//		if (!dealAppnt()) {
//			return false;
//		}
//		/** 处理完成投保人开始处理被保人信息 */
//		System.out.println("开始处理被保险人");
//		if (!dealInsured()) {
//			return false;
//		}
//		System.out.println("完成被保人信息录入！");
//		if (!dealAddress()) {
//			return false;
//		}
//		if (!dealCont()) {
//			return false;
//		}
//
//		// 处理被保人健康告知
//		if (!dealCustomerImpartInfo()) {
//			return false;
//		}
//		// --------------------
//
//		if (!dealRisk()) {
//			return false;
//		}
//
//		if (!dealNation()) {
//			return false;
//		}
//
//		if (!dealImpart()) {
//			return false;
//		}

		return true;
	}

	/**
	 * dealImpart
	 * 
	 * @return boolean
	 */
	private boolean dealImpart() {
		if (mLCCustomerImpartSchema != null) {
			mLCCustomerImpartSchema.setContNo(this.mContNo);
			mLCCustomerImpartSchema.setProposalContNo(this.mLCContSchema
					.getProposalContNo());
			mLCCustomerImpartSchema.setCustomerNo(this.mLCInsuredSchema
					.getInsuredNo());
			mLCCustomerImpartSchema.setOperator(this.mGlobalInput.Operator);
			PubFun.fillDefaultField(mLCCustomerImpartSchema);
		}
		return true;
	}

	/**
	 * dealBnf
	 * 
	 * @return boolean
	 * @param tLCPolSchema
	 *            LCPolSchema
	 */
	private boolean dealBnf(LCPolSchema tLCPolSchema) {
		/** 意外 */
		this.mLCBnfSchema.setContNo(tLCPolSchema.getContNo());
		this.mLCBnfSchema.setPolNo(tLCPolSchema.getPolNo());
		this.mLCBnfSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
		this.mLCBnfSchema.setBnfType("1");
		this.mLCBnfSchema.setBnfNo(1);
		this.mLCBnfSchema.setBnfGrade("1");
		PubFun.fillDefaultField(mLCBnfSchema);
		mLCBnfSchema.setOperator(this.mGlobalInput.Operator);
		System.out.println("受益人姓名 : " + mLCBnfSchema.getName());
		System.out.println("受益人性别 : " + mLCBnfSchema.getSex());
		System.out.println("受益人证件号码 : " + mLCBnfSchema.getIDNo());
		return true;
	}

	/**
	 * dealNation
	 * 
	 * @return boolean
	 */
	private boolean dealNation() {
		System.out.println("Into BriefSingleContInputBL.dealNation()...");
		for (int i = 1; i <= mLCNationSet.size(); i++) {
			System.out.println("国家代码　：");
			System.out.println(mLCNationSet.get(i).getNationNo());
		}

		for (int i = 1; i <= this.mLCNationSet.size(); i++) {
			if (StrTool.cTrim(mLCNationSet.get(i).getNationNo()).equals("")) {
				buildError("dealNation", "没有国家代码！");
				return false;
			}
			LDNationDB tLDNationDB = new LDNationDB();
			tLDNationDB.setNationNo(mLCNationSet.get(i).getNationNo());
			if (!tLDNationDB.getInfo()) {
				buildError("dealNation", "没有找到国家代码对应的国家名称！");
				return false;
			}
			tLDNationDB.setNationNo(mLCNationSet.get(i).getNationNo());
			mLCNationSet.get(i).setContNo(this.mContNo);
			mLCNationSet.get(i).setGrpContNo(SysConst.ZERONO);
			mLCNationSet.get(i).setEnglishName(tLDNationDB.getEnglishName());
			mLCNationSet.get(i).setOperator(this.mGlobalInput.Operator);
		}
		PubFun.fillDefaultField(mLCNationSet);
		System.out.println("完成抵达国家处理");
		return true;
	}

	/**
	 * dealAddress
	 * 
	 * @return boolean
	 */
	private boolean dealAddress() {
		/** 处理投保人被保人地址信息 */
		System.out.println("处理地址信息！");
		if (!dealAppntAddress()) {
			return false;
		}
		/** 处理被保人地址信息 */
		if (!dealInsuredAddress()) {
			return false;
		}
		return true;
	}

	/**
	 * dealAppntAddress
	 * 
	 * @return boolean
	 */
	private boolean dealAppntAddress() {
		if (this.mAppntAddressSchema == null) {
			buildError("dealAppntAddress", "没有传入投保人地址信息！");
			return false;
		}
		if (!checkAppntAddress()) {
			return false;
		}
		return true;
	}

	/**
	 * checkAppntAddress
	 * 
	 * @return boolean
	 */
	private boolean checkAppntAddress() {
		String mAddressNo = "";
		System.out.println("投保人" + "地址代码 : "
				+ this.mAppntAddressSchema.getAddressNo());
		System.out.println("是否需要创建客户信息 :　" + (needCreatAppnt ? "是" : "否"));
		if (!StrTool.cTrim(this.mAppntAddressSchema.getAddressNo()).equals("")
				&& !this.needCreatAppnt) {
			System.out.println("需要生成地址代码！");
			LCAddressDB tLCAddressDB = new LCAddressDB();
			tLCAddressDB.setCustomerNo(this.mAppntNo);
			tLCAddressDB.setAddressNo(mAppntAddressSchema.getAddressNo());
			if (!tLCAddressDB.getInfo()) {
				buildError("checkAppntAddress", "录入客户号码与地址编码，但没有在系统中查询到该地址信息！");
				return false;
			}
			/**
			 * 前台之会录入 ： 1、联系地址 PostalAddress 2、邮政编码 ZipCode 3、家庭电话 HomePhone
			 * 4、移动电话 Mobile 5、办公电话 CompanyPhone 6、电子邮箱 EMail
			 * 只校验上述信息如果不同则生成新的地址编码
			 */
			if (!StrTool.unicodeToGBK(
					StrTool.cTrim(mAppntAddressSchema.getPostalAddress()))
					.equals(StrTool.cTrim(tLCAddressDB.getPostalAddress()))) {
				needCreatAddressNo = true;
			}
			if (!StrTool.cTrim(mAppntAddressSchema.getZipCode()).equals(
					StrTool.cTrim(tLCAddressDB.getZipCode()))) {
				needCreatAddressNo = true;
			}
			if (!StrTool.cTrim(mAppntAddressSchema.getHomePhone()).equals(
					StrTool.cTrim(tLCAddressDB.getHomePhone()))) {
				needCreatAddressNo = true;
			}
			if (!StrTool.cTrim(mAppntAddressSchema.getMobile()).equals(
					StrTool.cTrim(tLCAddressDB.getMobile()))) {
				needCreatAddressNo = true;
			}
			if (!StrTool.cTrim(mAppntAddressSchema.getCompanyPhone()).equals(
					StrTool.cTrim(tLCAddressDB.getCompanyPhone()))) {
				needCreatAddressNo = true;
			}
			System.out.println(needCreatAddressNo ? "需要生成地址信息" : "不需要生成地址信息");
			if (needCreatAddressNo) {
				ExeSQL tExeSQL = new ExeSQL();
				mAddressNo = tExeSQL
						.getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
								+ mAppntNo + "'");
				System.out.println("生成的地址代码为：　" + mAddressNo);
				if (StrTool.cTrim(mAddressNo).equals("")) {
					buildError("checkAppntAddress", "生成地址代码错误！");
					return false;
				}
				mAppntAddressSchema.setAddressNo(mAddressNo);
			}
		} else {
			mAppntAddressSchema.setCustomerNo(this.mAppntNo);
			mAppntAddressSchema.setAddressNo("1");
		}
		this.mAppntAddressSchema.setOperator(this.mGlobalInput.Operator);
		PubFun.fillDefaultField(this.mAppntAddressSchema);
		mAppntAddressSchema.setCustomerNo(this.mAppntNo);
		mLCAddressSet.add(mAppntAddressSchema);
		System.out.println("完成地址信息 \n地址代码："
				+ mAppntAddressSchema.getAddressNo() + "\n客户号码"
				+ mAppntAddressSchema.getCustomerNo());
		this.mLCAppntSchema.setAddressNo(mAppntAddressSchema.getAddressNo());
		return true;
	}

	/**
	 * dealInsuredAddress
	 * 
	 * @return boolean
	 */
	private boolean dealInsuredAddress() {
		if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).equals(
				"00")) {
			/** 投保人和被保人是本人,不需要生成地址编码 */
			mLCInsuredSchema.setAddressNo(this.mLCAppntSchema.getAddressNo());
			return true;
		} else {
			if (!checkInsuredAddress()) {
				return false;
			}
			return true;
		}
	}

	/**
	 * checkInsuredAddress
	 * 
	 * @return boolean
	 */
	private boolean checkInsuredAddress() {
		String mAddressNo = "";
		System.out.println("投保人" + "地址代码 : "
				+ this.mInsuredAddressSchema.getAddressNo());
		System.out.println("是否需要创建客户信息 :　" + (needCreatInsured ? "是" : "否"));
		if (!StrTool.cTrim(this.mInsuredAddressSchema.getAddressNo())
				.equals("")
				&& !this.needCreatInsured) {
			LCAddressDB tLCAddressDB = new LCAddressDB();
			tLCAddressDB.setCustomerNo(this.mInsuredNo);
			tLCAddressDB.setAddressNo(mInsuredAddressSchema.getAddressNo());
			if (!tLCAddressDB.getInfo()) {
				buildError("checkInsuredAddress",
						"录入客户号码与地址编码，但没有在系统中查询到该地址信息！");
				return false;
			}
			/**
			 * 前台之会录入 ： 1、联系地址 PostalAddress 2、邮政编码 ZipCode 3、家庭电话 HomePhone
			 * 4、移动电话 Mobile 5、办公电话 CompanyPhone 6、电子邮箱 EMail
			 * 只校验上述信息如果不同则生成新的地址编码
			 */
			if (!StrTool.cTrim(mInsuredAddressSchema.getPostalAddress())
					.equals(StrTool.cTrim(tLCAddressDB.getPostalAddress()))) {
				needCreatAddressNo = true;
			} else if (!StrTool.cTrim(mInsuredAddressSchema.getZipCode())
					.equals(StrTool.cTrim(tLCAddressDB.getZipCode()))) {
				needCreatAddressNo = true;
			} else if (!StrTool.cTrim(mInsuredAddressSchema.getHomePhone())
					.equals(StrTool.cTrim(tLCAddressDB.getHomePhone()))) {
				needCreatAddressNo = true;
			} else if (!StrTool.cTrim(mInsuredAddressSchema.getMobile())
					.equals(StrTool.cTrim(tLCAddressDB.getMobile()))) {
				needCreatAddressNo = true;
			} else if (!StrTool.cTrim(mInsuredAddressSchema.getCompanyPhone())
					.equals(StrTool.cTrim(tLCAddressDB.getCompanyPhone()))) {
				needCreatAddressNo = true;
			}
			if (needCreatAddressNo) {
				ExeSQL tExeSQL = new ExeSQL();
				mAddressNo = tExeSQL
						.getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
								+ mInsuredNo + "'");
				if (StrTool.cTrim(mAddressNo).equals("")) {
					buildError("checkAppntAddress", "生成地址代码错误！");
					return false;
				}
				mInsuredAddressSchema.setCustomerNo(this.mInsuredNo);
				mInsuredAddressSchema.setAddressNo(mAddressNo);
			}
		} else {
			mInsuredAddressSchema.setCustomerNo(this.mInsuredNo);
			mInsuredAddressSchema.setAddressNo("1");
		}
		this.mInsuredAddressSchema.setOperator(this.mGlobalInput.Operator);
		PubFun.fillDefaultField(this.mInsuredAddressSchema);
		mInsuredAddressSchema.setCustomerNo(this.mInsuredNo);
		this.mLCAddressSet.add(mInsuredAddressSchema);
		System.out.println("完成地址信息 \n地址代码："
				+ mInsuredAddressSchema.getAddressNo() + "\n客户号码"
				+ mInsuredAddressSchema.getCustomerNo());
		this.mLCInsuredSchema.setAddressNo(this.mInsuredAddressSchema
				.getAddressNo());
		return true;
	}

	/**
	 * dealCont<br>
	 * modify by NicolE 2007.06.15
	 * 
	 * @return boolean
	 * 
	 */
	private boolean dealCont() {
		System.out.println("Into BriefSingleContInputBL.dealCont()...");
		if (!StrTool.cTrim(this.mOperate).equals("DELETE||MAIN")) {
			/** 生成号码 */
			System.out.println("ContNo：" + mLCContSchema.getContNo());
			LCContDB tLCContDB = new LCContDB();
			LCContSet tLCContSet = new LCContSet();
			tLCContDB.setPrtNo(mLCContSchema.getPrtNo());
			tLCContSet = tLCContDB.query();
			System.out.println("zhuzhuzhzu++++" + tLCContSet.size());
			if (tLCContSet.size() == 0) {
				String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
				this.mContNo = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
			} else {
				mContNo = tLCContSet.get(1).getContNo();
			}

			if (StrTool.cTrim(mContNo).equals("")) {
				buildError("dealCont", "生成合同号码错误！");
				return false;
			}
			System.out.println("合同号码 " + this.mContNo);
			/** 保存合同号码 */
			this.mLCContSchema.setContNo(this.mContNo);
			if (mLCContSchema.getProposalContNo() == null
					|| mLCContSchema.getProposalContNo().equals("")) {
				this.mLCContSchema.setProposalContNo(this.mContNo);
			}
			this.mLCContSchema.setGrpContNo(SysConst.ZERONO);
			/** 添加管理机构 */
			if (!dealAgentAndMangeCom()) {
				return false;
			}
			this.mLCContSchema.setContType("1"); // 个单
			this.mLCContSchema.setInputOperator(mGlobalInput.Operator);
			if (StrTool.cTrim(this.mLCContSchema.getInputDate()).equals("")) {
				this.mLCContSchema.setInputDate(mCurrentData);
			}
			this.mLCContSchema.setInputTime(mCurrentTime);
			// this.mLCContSchema.setApproveCode(mGlobalInput.Operator);
			// this.mLCContSchema.setApproveDate(mCurrentData);
			// this.mLCContSchema.setApproveFlag("9"); // 符合通过
			// this.mLCContSchema.setApproveTime(mCurrentTime);
			this.mLCContSchema.setUWFlag("9");
			this.mLCContSchema.setUWOperator(mGlobalInput.Operator);
			this.mLCContSchema.setUWDate(mCurrentData);
			this.mLCContSchema.setUWTime(mCurrentTime);
			this.mLCContSchema.setAppFlag("0");
			this.mLCContSchema.setStateFlag("0");
			// this.mLCContSchema.setCustomGetPolDate(mCurrentData);
			this.mLCContSchema.setPrintCount(0);
			// this.mLCContSchema.setGetPolDate(mCurrentData);
			this.mLCContSchema.setPolApplyDate(polApplyDate);
			this.mLCContSchema.setOperator(mGlobalInput.Operator);
			mLCContSchema.setSignCom(mGlobalInput.ManageCom);
			PubFun.fillDefaultField(mLCContSchema);
			this.mLCContSchema.setProposalType("01");
			this.mLCContSchema.setIntlFlag("0"); // 非国际业务
			this.mLCContSchema.setPayerType("2"); // 主被保人

			/**
			 * mMissionProp5 —— 1 境外救援 mMissionProp5 —— 2 交通意外 mMissionProp5 ——
			 * 3 高原疾病
			 */
			this.mLCContSchema.setCardFlag(this.mMissionProp5);
			/** 保险终止日期需要处理一下,要加一天 */
			System.out.println(mLCContSchema.getCInValiDate());
			if (mLCContSchema.getCInValiDate() != null) {
				mLCContSchema.setCInValiDate(PubFun.calDate(this.mLCContSchema
						.getCInValiDate(), 1, "D", null));
				System.out
						.println("连锁店缴费连锁店缴费连锁店缴费三大件福连锁店缴费连锁店就弗里敦缴费连锁店缴费连锁店飞机 "
								+ mLCContSchema.getCInValiDate());
			}
			if (!fillOtherTable()) {
				return false;
			}
			System.out.println(" LCCont deal..........");
			/** 完成合同信息 */
		}
		return true;
	}

	/**
	 * fillOtherTable
	 * 
	 * @return boolean
	 */
	private boolean fillOtherTable() {
		this.mLCInsuredSchema.setContNo(this.mContNo);
		this.mLCInsuredSchema.setPrtNo(this.mLCContSchema.getPrtNo());
		this.mLCInsuredSchema.setGrpContNo(SysConst.ZERONO);
		this.mLCInsuredSchema.setManageCom(this.mLCContSchema.getManageCom());
		this.mLCInsuredSchema.setExecuteCom(this.mGlobalInput.ManageCom);
		this.mLCAppntSchema.setContNo(this.mContNo);
		this.mLCAppntSchema.setPrtNo(this.mLCContSchema.getPrtNo());
		this.mLCAppntSchema.setGrpContNo(SysConst.ZERONO);
		if (mLCRiskDutyWrapSet != null) {
			for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++) {
				mLCRiskDutyWrapSet.get(i).setContNo(this.mContNo);
				mLCRiskDutyWrapSet.get(i).setPrtNo(
						this.mLCContSchema.getPrtNo());
			}
		}
		return true;
	}

	/**
	 * 为LCCont、LCPol的复核和自动核保字段复制
	 * 
	 * @return boolean
	 */
	private boolean fillApprovUWField() {
		if (this.mLCContSchema == null || this.mLCPolSet == null) {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "fillApprovUWField";
			tError.errorMessage = "请先处理合同和险种信息";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		this.mLCContSchema.setApproveCode(mGlobalInput.Operator);
		this.mLCContSchema.setApproveDate(mCurrentData);
		this.mLCContSchema.setApproveFlag("9"); // 符合通过
		this.mLCContSchema.setApproveTime(mCurrentTime);

		for (int i = 1; i <= mLCPolSet.size(); i++) {
			this.mLCPolSet.get(i).setApproveCode(mGlobalInput.Operator);
			this.mLCPolSet.get(i).setApproveDate(mCurrentData);
			this.mLCPolSet.get(i).setApproveFlag("9"); // 符合通过
			this.mLCPolSet.get(i).setApproveTime(mCurrentTime);
		}

		return true;
	}

	/**
	 * dealAgentAndMangeCom
	 * 
	 * @return boolean
	 */
	private boolean dealAgentAndMangeCom() {
		LDComDB tLDComDB = new LDComDB();
		tLDComDB.setComCode(this.mGlobalInput.ManageCom);
		if (!tLDComDB.getInfo()) {
			buildError("dealAgentAndMangeCom", "查询管理机构失败请确认管理机构是否录入正确！");
			return false;
		}
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
		if (!tLAAgentDB.getInfo()
				&& !StrTool.cTrim(this.mMissionProp5).equals("4")) {
			buildError("dealAgentAndMangeCom", "查询代理人失败！");
			return false;
		}
		if (!StrTool.cTrim(tLAAgentDB.getManageCom()).equals(
				this.mLCContSchema.getManageCom())
				&& !StrTool.cTrim(this.mMissionProp5).equals("4")) {
			buildError("dealAgentAndMangeCom",
					"代理人与所录入的管理机构不对应,请确认代理人或管理机构是否录入正确！");
			return false;
		}
		/** 校验代理人组别,如果传入代理人组别则校验代理组别是否对应,如果没有传入,就填充 */
		if (this.mLCContSchema.getAgentGroup() == null
				|| "".equals(this.mLCContSchema.getAgentGroup())
				|| mMissionProp5.equals("2") || mMissionProp5.equals("3")
				|| mMissionProp5.equals("p")) {
			this.mLCContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
		} else {
			if (!this.mLCContSchema.getAgentGroup().equals(
					tLAAgentDB.getAgentGroup())
					&& !StrTool.cTrim(this.mMissionProp5).equals("4")) {
				buildError("dealAgentAndMangeCom", "代理人组别录入错误,请检查该代理人是否录入错误！");
				return false;
			}
		}
		// if (this.mLCContSchema.getAgentType() == null) {
		// this.mLCContSchema.setAgentType(tLAAgentDB.getagent);
		// }

		if (this.mLCContSchema.getPayMode() == null) {
			buildError("dealAgentAndMangeCom", "没有录入缴费方式！");
			return false;
		}

		return true;
	}

	/**
	 * dealRisk
	 * 
	 * @return boolean
	 */
	private boolean dealRisk() {
		for (int i = 1; i <= this.mLCRiskDutyWrapSet.size(); i++) {
			mLCRiskDutyWrapSet.get(i).setInsuredNo(
					mLCInsuredSchema.getInsuredNo());
			if ("FeeRate".equals(mLCRiskDutyWrapSet.get(i).getCalFactor())
					&& "2".equals(mLCRiskDutyWrapSet.get(i).getCalFactorType())) {
				// 每个保单统一一个折扣
				FeeRate = mLCRiskDutyWrapSet.get(i).getCalFactorValue();
			}
		}

		/** 处理险种信息，主要是取出LMRiskApp中的险种信息，放到LCPol表中 */
		System.out.println("开始处理险种信息");
		System.out.println("本保单有" + this.mLCPolSet.size() + "个险种");
		mLCDutySet = new LCDutySet();
		mLCPremSet = new LCPremSet();
		mLCGetSet = new LCGetSet();
		CalBL tCalBL;
		for (int i = 1; i <= this.mLCPolSet.size(); i++) {
			/**
			 * 针对每一个险种的信息进行的操作 : 1、首先生成PolNo 2、将一般险种信息封装。
			 * 3、处理责任信息，把责任信息封装后传入CalBL中进行保费计算。
			 * 4、获取封装信息，回填LCPol，LCCont的保费保额，缴至日期，生效日期，失效日期，责任终止日期
			 */

			LCPolSchema tLCPolSchema = this.mLCPolSet.get(i);
			if (!dealLCPol(tLCPolSchema, i)) {
				return false;
			}
			if (!CheckTBField(tLCPolSchema, "INSERT")) {
				return false;
			}
			if (!checkInsuredAge(tLCPolSchema)) {
				return false;
			}
			String mRiskCode = mLCPolSet.get(i).getRiskCode();
			/** 封装责任信息 */
			LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
			tLMRiskDutyDB.setRiskCode(mRiskCode);
			LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
			// System.out.println("险种数：" + mLCPolSet.size());
			LCDutySet tLCDutySet = getDuty(tLMRiskDutySet, mLCPolSet.get(i));
			// System.out.println("责任个数：" + tLCDutySet.size());
			if (tLCDutySet == null || tLCDutySet.size() <= 0) {
				return false;
			}
			System.out.println("测试档次才决定；送风机司法苏联法扫雷；冬季"
					+ tLCDutySet.get(1).getMult());
			System.out.println("责任准备完毕");
			/** 责任信息封装完毕 */
			if (FeeRate != null && !"".equals(FeeRate)) {
				TransferData FTransferData = new TransferData();
				FTransferData.setNameAndValue("FeeRate", FeeRate);
				tCalBL = new CalBL(tLCPolSchema, tLCDutySet, null,
						FTransferData);
			} else {
				tCalBL = new CalBL(tLCPolSchema, tLCDutySet, null, null);
			}
			tCalBL.setRiskWrapCode(getRiskWrapCode(tLCPolSchema)); // 注意
			if (!tCalBL.calPol()) {
				this.mErrors.copyAllErrors(tCalBL.mErrors);
				return false;
			}
			if (tCalBL.mErrors.needDealError()) {
				this.mErrors.copyAllErrors(tCalBL.mErrors);
				return false;
			}
			tLCPolSchema.setSchema(tCalBL.getLCPol());
			tLCDutySet = tCalBL.getLCDuty();
			LCPremSet tLCPremSet = tCalBL.getLCPrem();
			LCGetSet tLCGetSet = tCalBL.getLCGet();
			for (int m = 1; m <= tLCDutySet.size(); m++) {
				System.out.println("验证档次 :" + tLCDutySet.get(m).getMult());
				tLCDutySet.get(m).setPolNo(this.mPolNo);
				tLCPolSchema.setGetYear(tLCDutySet.get(m).getGetYear());
				tLCPolSchema.setGetYearFlag(tLCDutySet.get(m).getGetYearFlag());
				tLCPolSchema.setPayEndYearFlag(tLCDutySet.get(m)
						.getPayEndYearFlag());
				tLCPolSchema.setPayEndYear(tLCDutySet.get(m).getPayEndYear());
				tLCPolSchema.setInsuYearFlag(tLCDutySet.get(m)
						.getInsuYearFlag());
				tLCPolSchema.setInsuYear(tLCDutySet.get(m).getInsuYear());
				tLCPolSchema.setMult(tLCDutySet.get(m).getMult());
			}
			for (int m = 1; m <= tLCPremSet.size(); m++) {
				tLCPremSet.get(m).setPolNo(this.mPolNo);
				tLCPremSet.get(m).setGrpContNo(SysConst.ZERONO);
				tLCPremSet.get(m).setContNo(this.mContNo);
				tLCPremSet.get(m).setOperator(this.mGlobalInput.Operator);
			}
			for (int m = 1; m <= tLCGetSet.size(); m++) {
				tLCGetSet.get(m).setPolNo(this.mPolNo);
				tLCGetSet.get(m).setGrpContNo(SysConst.ZERONO);
				tLCGetSet.get(m).setContNo(this.mContNo);
				tLCGetSet.get(m).setOperator(this.mGlobalInput.Operator);
			}
			PubFun.fillDefaultField(tLCPremSet);
			PubFun.fillDefaultField(tLCGetSet);
			PubFun.fillDefaultField(tLCDutySet);
			this.mLCDutySet.add(tLCDutySet);
			this.mLCPremSet.add(tLCPremSet);
			this.mLCGetSet.add(tLCGetSet);
		}
		/** 完成保存险种之后校验受益人是否保存成功 */
		if (this.mLCBnfSchema != null && this.mLCBnfSchema.getPolNo() == null) {
			String str = "没有存在身故受益人的险种,不需要录入身故受益人!";
			buildError("dealRisk", str);
			return false;
		}
		/** 将保费维护到LCCont表中 */
		for (int i = 1; i <= this.mLCPolSet.size(); i++) {
			sumPrem += this.mLCPolSet.get(i).getPrem();
			sumAmnt += this.mLCPolSet.get(i).getAmnt();
			sumMult += this.mLCPolSet.get(i).getMult();
		}
		this.mLCContSchema.setPrem(sumPrem);
		this.mLCContSchema.setAmnt(sumAmnt);
		this.mLCContSchema.setMult(sumMult);
		if (mLCContSchema.getCInValiDate() == null) {
			Date tCinValidate = null;
			for (int i = 1; i <= this.mLCPolSet.size(); i++) {
				Date lcpolDate = (new FDate()).getDate(mLCPolSet.get(i)
						.getEndDate());
				if (tCinValidate == null || tCinValidate.before(lcpolDate)) {
					tCinValidate = lcpolDate;
				}
			}
			this.mLCContSchema.setCInValiDate(tCinValidate);
		}

		return true;
	}

	/**
	 * 得到险种所属套餐编码
	 * 
	 * @param tLCPolSchema
	 *            LCPolSchema
	 * @return String
	 */
	private String getRiskWrapCode(LCPolSchema tLCPolSchema) {
		String tRiskWrapCode = "";

		for (int i = 1; i < mLCRiskDutyWrapSet.size(); i++) {
			if (mLCRiskDutyWrapSet.get(i).getRiskCode().equals(
					tLCPolSchema.getRiskCode())
					&& mLCRiskDutyWrapSet.get(i).getInsuredNo().equals(
							tLCPolSchema.getInsuredNo())) {
				return mLCRiskDutyWrapSet.get(i).getRiskWrapCode();
			}
		}

		return tRiskWrapCode;
	}

	/**
	 * 得到计算险种保费的结果集合
	 * 
	 * @return MMap
	 */
	public MMap getSubmitRiskMap(VData cInputData, String cOperate) {
		System.out.println("beginning of getSubmitRiskMap");

		this.mInputData = cInputData;
		this.mOperate = cOperate;

		System.out.println("before this.getInputData()");

		if (!this.getInputData()) {
			return null;
		}

		this.mContNo = mLCContSchema.getContNo();

		if (!this.dealRisk()) {
			return null;
		}

		if (map == null) {
			map = new MMap();
		}

		map.put(this.mLCPolSet, SysConst.INSERT);
		map.put(this.mLCDutySet, SysConst.INSERT);
		map.put(this.mLCPremSet, SysConst.INSERT);
		map.put(this.mLCGetSet, SysConst.INSERT);
		map.put(this.mLCContSchema, SysConst.UPDATE);

		if (mLCRiskDutyWrapSet != null && mLCRiskDutyWrapSet.size() > 0) {
			for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++) {
				String tStrCalSql = mLCRiskDutyWrapSet.get(i).getCalSql();
				if (tStrCalSql != null)
					tStrCalSql = tStrCalSql.replaceAll("'", "''");
				mLCRiskDutyWrapSet.get(i).setCalSql(tStrCalSql); // CalSql无用，不需要赋值
			}
			map.put(mLCRiskDutyWrapSet, SysConst.INSERT);
		}
		map.put(this.mLCBnfet, SysConst.INSERT);

		return map;
	}

	/**
	 * dealLCPol
	 * 
	 * @return boolean
	 * @param tLCPolSchema
	 *            LCPolSchema
	 * @param seq
	 *            int
	 */
	private boolean dealLCPol(LCPolSchema tLCPolSchema, int seq) {
		LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
		LMRiskDB tLMRiskDB = new LMRiskDB();
		mPolNo = PubFun1.CreateMaxNo("ProposalNo", PubFun
				.getNoLimit(mLCContSchema.getManageCom()));
		if (StrTool.cTrim(this.mPolNo).equals("")) {
			buildError("dealRisk", "生成险种号码错误！");
			return false;
		}
		System.out.println("生成的险种号码是 ：" + this.mPolNo);
		System.out.println("险种是" + tLCPolSchema.getRiskCode());
		tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
		tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
		if (!tLMRiskAppDB.getInfo() || !tLMRiskDB.getInfo()) {
			buildError("dealRisk", "查询险种信息失败！");
			return false;
		}
		if ("2".equals(tLMRiskAppDB.getRiskType4())) {// 如果是分红险
			if (mBonusgetmode == null || "".equals(mBonusgetmode)) {
				buildError("dealRisk", "分红险未填写红利领取方式！");
				return false;
			} else {
				tLCPolSchema.setBonusGetMode(mBonusgetmode);
			}
		}
		tLCPolSchema.setPolNo(this.mPolNo);
		tLCPolSchema.setProposalNo(this.mPolNo);
		tLCPolSchema.setContNo(this.mContNo);
		tLCPolSchema.setPrtNo(this.mLCContSchema.getPrtNo());
		tLCPolSchema.setGrpContNo(SysConst.ZERONO);
		tLCPolSchema.setGrpPolNo(SysConst.ZERONO);
		tLCPolSchema.setContType("1"); // 个单
		tLCPolSchema.setPolTypeFlag("0");
		tLCPolSchema.setMainPolNo(this.mPolNo);
		tLCPolSchema.setProposalContNo(this.mContNo);
		tLCPolSchema.setKindCode(tLMRiskAppDB.getKindCode());
		tLCPolSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
		tLCPolSchema.setManageCom(this.mLCContSchema.getManageCom());
		tLCPolSchema.setAgentCom(this.mLCContSchema.getAgentCom());
		tLCPolSchema.setAgentType(this.mLCContSchema.getAgentType());
		tLCPolSchema.setAgentCode(this.mLCContSchema.getAgentCode());
		tLCPolSchema.setAgentGroup(this.mLCContSchema.getAgentGroup());
		tLCPolSchema.setSaleChnl(this.mLCContSchema.getSaleChnl());
		tLCPolSchema.setSaleChnlDetail(this.mLCContSchema.getSaleChnlDetail());
		tLCPolSchema.setCValiDate(this.mLCContSchema.getCValiDate());
		tLCPolSchema.setAppntNo(this.mLCContSchema.getAppntNo());
		tLCPolSchema.setAppntName(this.mLCContSchema.getAppntName());
		// tLCPolSchema.setApproveCode(this.mGlobalInput.Operator);
		System.out.println(this.mLCContSchema.getCValiDate());
		System.out.println(this.mLCInsuredSchema.getBirthday());
		tLCPolSchema.setInsuredAppAge(PubFun.getInsuredAppAge(
				this.mLCContSchema.getCValiDate(), this.mLCInsuredSchema
						.getBirthday()));
		tLCPolSchema.setInsuredPeoples(1);
		tLCPolSchema.setOccupationType(this.mLCInsuredSchema
				.getOccupationType());
		tLCPolSchema.setInsuredSex(this.mLCInsuredSchema.getSex());
		tLCPolSchema.setInsuredBirthday(mLCInsuredSchema.getBirthday());
		tLCPolSchema.setInsuredName(mLCInsuredSchema.getName());
		tLCPolSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());

		tLCPolSchema.setAutoPayFlag(tLMRiskAppDB.getAutoPayFlag());
		if (StrTool.cTrim(tLMRiskDB.getRnewFlag()).equals("N")) {
			tLCPolSchema.setRnewFlag(-2);
		}
		tLCPolSchema.setComFeeRate(tLMRiskAppDB.getAppInterest());
		tLCPolSchema.setBranchFeeRate(tLMRiskAppDB.getAppPremRate());
		/** 不指定生效日期 */
		// if ("5".equals(mContType)||"6".equals(mContType))
		// {
		tLCPolSchema.setSpecifyValiDate("N");
		// }
		// else
		// {
		// tLCPolSchema.setSpecifyValiDate("1");
		// }
		tLCPolSchema.setPayMode(mLCContSchema.getPayMode());
		tLCPolSchema.setPayIntv(mLCContSchema.getPayIntv());
		// tLCPolSchema.setApproveDate(mCurrentData);
		// tLCPolSchema.setApproveFlag("9"); // 符合通过
		// tLCPolSchema.setApproveTime(mCurrentTime);
		tLCPolSchema.setUWFlag("9");

		// 处理万能险的首期追加保费。
		String tTmpRiskCode = tLCPolSchema.getRiskCode();
		if (CommonBL.isULIRisk(tTmpRiskCode)) {
			System.out.println("tTmpRiskCode:" + tTmpRiskCode);
			if (!dealSupplementaryPrem(tLCPolSchema)) {
				buildError("dealRisk", "处理万能险的首期追加保费失败！");
				return false;
			}
		}
		// ---------------------------

		// 处理万能初始扣费
		if (CommonBL.isULIRisk(tTmpRiskCode)) {
			System.out.println("tTmpRiskCode:" + tTmpRiskCode);
			if (!dealInitFeeRate(tLCPolSchema)) {
				buildError("dealRisk", "处理万能险的首期追加保费失败！");
				return false;
			}
		}
		// --------------------

		tLCPolSchema.setUWCode(mGlobalInput.Operator);
		tLCPolSchema.setUWDate(mCurrentData);
		tLCPolSchema.setUWTime(mCurrentTime);
		tLCPolSchema.setAppFlag("0");
		tLCPolSchema.setStateFlag("0");
		tLCPolSchema.setPolApplyDate(polApplyDate);
		tLCPolSchema.setOperator(mGlobalInput.Operator);
		// tLCPolSchema.setProposalContNo(mLCContSchema.getProposalContNo());
		PubFun.fillDefaultField(tLCPolSchema);
		tLCPolSchema.setRiskSeqNo(String.valueOf(seq).length() <= 1 ? "0"
				+ String.valueOf(seq) : String.valueOf(seq));
		System.out.println(mLCBnfSchema);
		System.out.println(StrTool.cTrim(tLMRiskAppDB.getBnfFlag()));
		if (this.mLCBnfSchema != null
				&& StrTool.cTrim(tLMRiskAppDB.getBnfFlag()).equals("N")) {
			System.out.println("开始处理受益人信息");
			if (!dealBnf(tLCPolSchema)) {
				return false;
			}
		}
		if (this.mInputLCBnfSet != null && this.mInputLCBnfSet.size() >= 0) {
			System.out.println("开始处理多受益人信息");
			if (!dealMulBnf(tLCPolSchema)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * dealMulBnf
	 * 
	 * @param tLCPolSchema
	 *            LCPolSchema
	 * @return boolean
	 */
	private boolean dealMulBnf(LCPolSchema tLCPolSchema) {
		LCBnfSet tLCBnfSet = new LCBnfSet();
		for (int i = 1; i <= this.mInputLCBnfSet.size(); i++) {
			System.out.println("测试 多受益人 问题: " + mInputLCBnfSet.size());
			mInputLCBnfSet.get(i).setContNo(tLCPolSchema.getContNo());
			mInputLCBnfSet.get(i).setPolNo(tLCPolSchema.getPolNo());
			mInputLCBnfSet.get(i).setBnfNo(i);
			mInputLCBnfSet.get(i).setInsuredNo(tLCPolSchema.getInsuredNo());
			mInputLCBnfSet.get(i).setMakeDate(PubFun.getCurrentDate());
			mInputLCBnfSet.get(i).setModifyDate(PubFun.getCurrentDate());
			mInputLCBnfSet.get(i).setMakeTime(PubFun.getCurrentTime());
			mInputLCBnfSet.get(i).setModifyTime(PubFun.getCurrentTime());
			mInputLCBnfSet.get(i).setOperator(mGlobalInput.Operator);
			System.out.println("受益人姓名 : " + mInputLCBnfSet.get(i).getContNo());
			System.out.println("受益人性别 : " + mInputLCBnfSet.get(i).getPolNo());
			System.out.println("受益人证件号码 : " + mInputLCBnfSet.get(i).getBnfNo());
			tLCBnfSet.add(mInputLCBnfSet.get(i).getSchema());
		}
		this.mLCBnfet.add(tLCBnfSet);
		for (int i = 1; i <= mLCBnfet.size(); i++) {
			System.out.println("测试多受益人 PolNo: " + mLCBnfet.get(i).getPolNo());
		}
		System.out.println("处理多受益人完成 mLCBnfet : " + mLCBnfet.size());
		return true;
	}

	/**
	 * getDuty
	 * 
	 * @param tLMRiskDutySet
	 *            LMRiskDutySet
	 * @param tLCPolSchema
	 *            LCPolSchema
	 * @return LCDutySet
	 */
	private LCDutySet getDuty(LMRiskDutySet tLMRiskDutySet,
			LCPolSchema tLCPolSchema) {
		System.out.println(tLMRiskDutySet.get(1).getDutyCode());
		LCDutySet tLCDutySet = new LCDutySet();
		HashMap tDutyMap = null;
		if (mLDRiskDutyWrapSet != null && mLDRiskDutyWrapSet.size() > 0) {
			tDutyMap = new HashMap();
			for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
				tDutyMap.put(mLDRiskDutyWrapSet.get(i).getDutyCode(), "1");
			}
		}
		boolean tFlag = false;
		for (int k = 1; k <= mLDRiskDutyWrapSet.size(); k++) {
			if (mLDRiskDutyWrapSet.get(k).getRiskCode().trim().equals(
					tLMRiskDutySet.get(1).getRiskCode())) {
				tFlag = true;
				break;
			}
		}

		for (int i = 1; i <= tLMRiskDutySet.size(); i++) {
			/** 如果是套餐类险种，并且制定责任 */

			if (tDutyMap != null && tFlag) {
				System.out.println((String) tDutyMap.get(tLMRiskDutySet.get(i)
						.getDutyCode()));
				String chk = (String) tDutyMap.get(tLMRiskDutySet.get(i)
						.getDutyCode());
				if (chk == null) {
					continue;
				}
			}
			LCDutySchema tLCDutySchema = new LCDutySchema();
			/** 查询责任描述 */
			LMDutyDB tLMDutyDB = new LMDutyDB();
			tLMDutyDB.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
			if (!tLMDutyDB.getInfo()) {
				buildError("getDuty", "查询险种责任出错！");
				return null;
			}
			tLCDutySchema.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
			/** 境外救援是旅行次数 */
			tLCDutySchema.setStandbyFlag1(this.mLCContSchema.getDegreeType());
			/** 保额 */
			tLCDutySchema.setAmnt(tLCPolSchema.getAmnt());
			/** 保费 */
			tLCDutySchema.setMult(tLCPolSchema.getMult());
			/** 份数 */
			tLCDutySchema.setCopys(tLCPolSchema.getCopys());
			/** 保费 */
			tLCDutySchema.setPrem(tLCPolSchema.getPrem());
			/** 保险期间 */
			tLCDutySchema.setPayIntv(tLCPolSchema.getPayIntv());
			/** 缴费频次 */
			System.out.println("hsafhkdlashfdioahfkdalsfh "
					+ mLCContSchema.getCInValiDate());
			System.out.println("baoxianqijian shi "
					+ tLCPolSchema.getInsuYear());
			System.out.println(" needInsuYear : " + needInsuYear);
			if (needInsuYear) {
				int insuYear = getInsuYear(mLCContSchema.getCValiDate(),
						mLCContSchema.getCInValiDate(), tLMDutyDB
								.getInsuYearFlag());

				tLCDutySchema.setInsuYear(insuYear);
				tLCDutySchema.setInsuYearFlag(tLMDutyDB.getInsuYearFlag());
				tLCDutySet.add(tLCDutySchema);
			} else {
				tLCDutySchema.setInsuYear(tLCPolSchema.getInsuYear());
				tLCDutySchema.setInsuYearFlag(tLMDutyDB.getInsuYearFlag());
				tLCDutySet.add(tLCDutySchema);
			}
			/** 当描述指定责任的要素和要素值得情况下，以描述为主 */
			String tWrapCode = null;
			for (int m = 1; m <= this.mLDRiskDutyWrapSet.size(); m++) {
				LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet
						.get(m);
				String tRiskCode = tLCPolSchema.getRiskCode();
				String tDutyCode = tLCDutySchema.getDutyCode();

				if (tLDRiskDutyWrapSchema.getRiskCode().equals(tRiskCode)
						&& tLDRiskDutyWrapSchema.getDutyCode()
								.equals(tDutyCode)) {
					if (tWrapCode == null) {
						tWrapCode = tLDRiskDutyWrapSchema.getRiskWrapCode();
					} else if (!tWrapCode.equals(tLDRiskDutyWrapSchema
							.getRiskWrapCode())) {
						String str = "系统选择两个套餐中有相同的险种，不允许出现单个被保人拥有相同险种的情况!";
						buildError("getDuty", str);
						log.debug(str);
						return null;
					}
					// /** 此处应该判断要素的存放方式，如果是需要计算的应是用计算引擎计算，
					// * 如果是直接取值的应直接取值，但由于时间紧张，咱不支持套餐责任要素的
					// * 多种计算形式 */
					// tLCDutySchema.setV(tLDRiskDutyWrapSchema.getCalFactor(),
					// tLDRiskDutyWrapSchema.
					// getCalFactorValue());
					if (tLDRiskDutyWrapSchema.getCalFactorType().equals("1")) {
						/** 直接取值 */
						tLCDutySchema.setV(mLCRiskDutyWrapSet.get(m)
								.getCalFactor(), tLDRiskDutyWrapSchema
								.getCalFactorValue());
					} else if (tLDRiskDutyWrapSchema.getCalFactorType().equals(
							"2")) {
						/** 准备要素 calbase */
						PubCalculator tCal = new PubCalculator();
						tCal.addBasicFactor("RiskCode", tRiskCode);
						for (int n = 1; n <= mLCRiskDutyWrapSet.size(); n++) {
							if (mLCRiskDutyWrapSet.get(n).getDutyCode().equals(
									tLCDutySchema.getDutyCode())) {
								tCal.addBasicFactor(mLCRiskDutyWrapSet.get(n)
										.getCalFactor(), mLCRiskDutyWrapSet
										.get(n).getCalFactorValue());
								System.out.println("********************");
								System.out.println(mLCRiskDutyWrapSet.get(n)
										.getCalFactor()
										+ ":"
										+ mLCRiskDutyWrapSet.get(n)
												.getCalFactorValue());
								System.out.println("********************");
							}
						}
						mLCRiskDutyWrapSet.get(m).setCalSql(
								tLDRiskDutyWrapSchema.getCalSql());
						tCal.setCalSql(mLCRiskDutyWrapSet.get(m).getCalSql());
						System.out.println(mLCRiskDutyWrapSet.get(m)
								.getCalSql());

						/** 计算 */
						System.out.println("rrrrrrrr"
								+ mLCRiskDutyWrapSet.get(m));
						String result = tCal.calculate();
						if (result == null) {
							log.error("套餐要素计算失败！");
						}
						tLCDutySchema.setV(mLCRiskDutyWrapSet.get(m)
								.getCalFactor(), result);
						// if (mLCRiskDutyWrapSet.get(m).getCalSql() == null
						// && mLCRiskDutyWrapSet.get(m).getCalFactor()
						// .equals("PayEndYearFlag"))
						// {
						// tLCDutySchema.setPayEndYearFlag(mLCRiskDutyWrapSet
						// .get(m).getCalFactorValue());
						// }
						if (mLCRiskDutyWrapSet.get(m).getCalFactor().equals(
								"FeeRate")) {
							tLCDutySchema.setV("FreeRate", result);
						}
					}
				}
				System.out.println("查看保额" + tLCDutySchema.getAmnt());
				System.out.println("查看保费" + tLCDutySchema.getPrem());
				System.out.println("查看折扣" + tLCDutySchema.getFreeRate());
				System.out.println("查看缴费期间标记 "
						+ tLCDutySchema.getPayEndYearFlag());
			}

			// 若不录入给付比例，则系统默认为1
			if (Math.abs(0 - tLCDutySchema.getGetRate()) < 0.00001) {
				tLCDutySchema.setGetRate(1);
			}
		}
		return tLCDutySet;
	}

	/**
	 * getInsuYear
	 * 
	 * @param tCValiDate
	 *            String
	 * @param tCInValiDate
	 *            String
	 * @param tInsuYearFlag
	 *            String
	 * @return int
	 */
	private int getInsuYear(String tCValiDate, String tCInValiDate,
			String tInsuYearFlag) {
		return PubFun.calInterval2(tCValiDate, tCInValiDate, tInsuYearFlag);
	}

	/**
	 * <h1>处理被保险人总体思路</h1>
	 * <li>1.首先判断投保人和被保人是否是本人.</li>
	 * <li>2.如果有客户号校验客户信息是否匹配,如果没有客户号,根据客户信息查询出客户号码</li>
	 * <li>3.生成的被保客户信息填充到险种表和合同表.</li>
	 * 
	 * @return boolean
	 */
	private boolean dealInsured() {
		/** 首先判断被保人与投保人的关系,如果是投保人被人,则不需要上面那么多校验 */
		if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).equals(
				"00")) {
			if (!dealAppntToInsured()) {
				return false;
			}
		} else {
			/** 新建被保险客户 */
			if (!checkInsured()) {
				return false;
			}
			if (this.needCreatInsured) {
				/** 检查完被保人，根据创建标志，判断是否新建客户 */
				this.mInsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
				if (StrTool.cTrim(this.mInsuredNo).equals("")) {
					// @@错误处理
					System.out.println("BriefSingleContInputBL中"
							+ "insertData方法报错，" + "在程序202行，Author:Yangming");
					CError tError = new CError();
					tError.moduleName = "BriefSingleContInputBL";
					tError.functionName = "insertData";
					tError.errorMessage = "生成客户号码错误！";
					this.mErrors.addOneError(tError);
					return false;
				} else {
					this.mLCInsuredSchema.setAppntNo(this.mAppntNo);
					this.mLCInsuredSchema.setInsuredNo(this.mInsuredNo);

					// 将客户身份证号码中的x转换成大写(被保险人) 2009-02-05 liuyp
					if (mLCInsuredSchema.getIDType() != null
							&& mLCInsuredSchema.getIDNo() != null) {
						if (mLCInsuredSchema.getIDType().equals("0")) {
							String tLCInsuredIdNo = mLCInsuredSchema.getIDNo()
									.toUpperCase();
							mLCInsuredSchema.setIDNo(tLCInsuredIdNo);
						}
					}

					/** 生成客户 */
					LDPersonSchema tLDPersonSchema = new LDPersonSchema();
					tLDPersonSchema.setCustomerNo(this.mInsuredNo);
					tLDPersonSchema.setName(this.mLCInsuredSchema.getName());
					tLDPersonSchema.setSex(this.mLCInsuredSchema.getSex());
					tLDPersonSchema.setBirthday(this.mLCInsuredSchema
							.getBirthday());
					tLDPersonSchema
							.setIDType(this.mLCInsuredSchema.getIDType());
					tLDPersonSchema.setIDNo(this.mLCInsuredSchema.getIDNo());
					tLDPersonSchema.setOperator(this.mGlobalInput.Operator);
					System.out.println("工作单位是...." + mWorkName);
					tLDPersonSchema.setGrpName(mWorkName);
					PubFun.fillDefaultField(tLDPersonSchema);

					// 保证客户信息数据的实时性
					// this.mLDPersonSet.add(tLDPersonSchema);
					LDPersonDB mLDPersonDB = new LDPersonDB();
					mLDPersonDB.setSchema(tLDPersonSchema);
					if (!mLDPersonDB.insert()) {
						// 错误处理
						System.out
								.println("BriefSingleContInputBL中，插入被保人信息错误！");
						CError tError = new CError();
						tError.moduleName = "BriefSingleContInputBL";
						tError.functionName = "dealInsured";
						tError.errorMessage = "保存客户信息错误！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
				/** 封装Makedate等信息 */
				PubFun.fillDefaultField(mLCInsuredSchema);
				this.mLCInsuredSchema.setOperator(this.mGlobalInput.Operator);
				this.mLCInsuredSchema.setManageCom(this.mGlobalInput.ManageCom);
				System.out.println("处理完成被保人");
			}
		}
		if (!dealInsuredToCont()) {
			return false;
		}

		return true;
	}

	/**
	 * dealInsuredToCont
	 * 
	 * @return boolean
	 */
	private boolean dealInsuredToCont() {
		this.mLCContSchema.setInsuredBirthday(this.mLCInsuredSchema
				.getBirthday());
		this.mLCContSchema.setInsuredIDNo(this.mLCInsuredSchema.getIDNo());
		this.mLCContSchema.setInsuredIDType(this.mLCInsuredSchema.getIDType());
		this.mLCContSchema.setInsuredName(this.mLCInsuredSchema.getName());
		this.mLCContSchema.setInsuredNo(this.mLCInsuredSchema.getInsuredNo());
		this.mLCContSchema.setInsuredSex(this.mLCInsuredSchema.getSex());
		for (int i = 1; i <= this.mLCPolSet.size(); i++) {
			this.mLCPolSet.get(i).setInsuredBirthday(
					this.mLCInsuredSchema.getBirthday());
			this.mLCPolSet.get(i).setInsuredName(
					this.mLCInsuredSchema.getName());
			this.mLCPolSet.get(i).setInsuredNo(
					this.mLCInsuredSchema.getInsuredNo());
			this.mLCPolSet.get(i).setInsuredSex(this.mLCInsuredSchema.getSex());
		}
		return true;
	}

	/**
	 * <h1>处理投保人的总体思路:</h1>
	 * <br>
	 * 1,首先判断是否录入客户号码,如果录入客户号码,校验姓名性别等基本信息是否相同,如果不同提示需要重新生成客户号<br>
	 * 2.如果没有录入客户号码,首先根据客户基本信息查询系统中可客户,如果找到填充客户号码.如果没有找到生成新客户.<br>
	 * 3.生成投保人信息后,将投报人信息填充到合同表中.
	 * 
	 * @return boolean
	 */
	private boolean dealAppnt() {

		System.out.println("开始处理投保客户");
		LDPersonDB tLDPersonDB = new LDPersonDB();
		if (!StrTool.cTrim(this.mLCAppntSchema.getAppntNo()).equals("")) {
			this.mAppntNo = this.mLCAppntSchema.getAppntNo();
			tLDPersonDB.setCustomerNo(mLCAppntSchema.getAppntNo());
			if (!tLDPersonDB.getInfo()) {
				buildError("dealAppnt", "录入投保客户号码但是系统中没有此刻户信息，请确认客户号码是否录入错误！！");
				return false;
			} else { // 查询到客户信息
				/** 如果是老客户校验客户信息与投保信息是否相同 */
				this.mAppntNo = mLCAppntSchema.getAppntNo();
				if (!checkAppnt(tLDPersonDB.getSchema())) {
					return false;
				}
			}
		} else { // 没有录入客户号码
			/** 如果没有客户号码信息,根据五个基本信息教研是否是同一个客户 */
			if (!queryAppnt()) {
				return false;
			}
		}
		/** 完成上面的操作,如果判断是新客户则生成客户号 */
		if (this.needCreatAppnt) {
			mAppntNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
			mLDPersonSet = new LDPersonSet();
			if (StrTool.cTrim(this.mAppntNo).equals("")) {
				CError tError = new CError();
				tError.moduleName = "BriefSingleContInputBL";
				tError.functionName = "insertData";
				tError.errorMessage = "生成团体客户号错误！";
				System.out.println("程序第157行出错，" + "请检查BriefSingleContInputBL中的"
						+ "insertData方法！" + tError.errorMessage);
				this.mErrors.addOneError(tError);
				return false;
			}
			this.mLCAppntSchema.setAppntNo(this.mAppntNo);

			// 校验证件类型是否符合系统的规范(0到4) by zhangyang 2011-01-12
			String tIDType = mLCAppntSchema.getIDType();

			if (tIDType != null && !tIDType.equals("")) {
				if (!tIDType.equals("0") && !tIDType.equals("1")
						&& !tIDType.equals("2") && !tIDType.equals("3")
						&& !tIDType.equals("4") && !tIDType.equals("5")&& !tIDType.equals("6")&& !tIDType.equals("7")) {
					CError tError = new CError();
					tError.moduleName = "BriefSingleContInputBL";
					tError.functionName = "insertData";
					tError.errorMessage = "投保客户的证件类型为" + tIDType
							+ ", 不符合系统规范，系统规定证件类型必须为数字0到7！";
					System.out.println("程序第2061行出错，"
							+ "请检查BriefSingleContInputBL中的" + "dealAppnt方法！"
							+ tError.errorMessage);
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			// ----------------------------------------------

			// 将客户身份证号码中的x转换成大写（投保人） 2009-02-05 liuyp

			if (mLCAppntSchema.getIDType() != null
					&& mLCAppntSchema.getIDNo() != null) {
				if (mLCAppntSchema.getIDType().equals("0")) {
					String tLCAppntIdNo = mLCAppntSchema.getIDNo()
							.toUpperCase();
					mLCAppntSchema.setIDNo(tLCAppntIdNo);
				}
			}

			/** 生成客户 */
			LDPersonSchema tLDPersonSchema = new LDPersonSchema();
			tLDPersonSchema.setCustomerNo(this.mAppntNo);
			tLDPersonSchema.setName(this.mLCAppntSchema.getAppntName());
			tLDPersonSchema.setSex(this.mLCAppntSchema.getAppntSex());
			tLDPersonSchema.setBirthday(this.mLCAppntSchema.getAppntBirthday());
			tLDPersonSchema.setIDType(this.mLCAppntSchema.getIDType());
			tLDPersonSchema.setIDNo(this.mLCAppntSchema.getIDNo());
			tLDPersonSchema
					.setEnglishName(this.mLCAppntSchema.getEnglishName());
			tLDPersonSchema.setOperator(this.mGlobalInput.Operator);
			tLDPersonSchema.setGrpName(mWorkName);
			PubFun.fillDefaultField(tLDPersonSchema);

			// 保证客户信息数据的实时性
			// this.mLDPersonSet.add(tLDPersonSchema);
			LDPersonDB mLDPersonDB = new LDPersonDB();
			mLDPersonDB.setSchema(tLDPersonSchema);
			if (!mLDPersonDB.insert()) {
				// 错误处理
				System.out.println("BriefSingleContInputBL中，插入投保人信息错误！");
				CError tError = new CError();
				tError.moduleName = "BriefSingleContInputBL";
				tError.functionName = "dealInsured";
				tError.errorMessage = "保存客户信息错误！";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		/** 封装Makedate等信息 */
		PubFun.fillDefaultField(mLCAppntSchema);
		this.mLCAppntSchema.setOperator(this.mGlobalInput.Operator);
		this.mLCAppntSchema.setManageCom(this.mGlobalInput.ManageCom);
		System.out.println("处理完成投保人");
		/** 处理完成投保人,将投保人信息填充到合同信息中 */
		if (!dealAppntToCont()) {
			return false;
		}

		return true;
	}

	/**
	 * checkInsured
	 * 
	 * @return boolean
	 */
	private boolean checkInsured() {
		System.out.println("系统同录入的投保人客户号码，开始验证客户信息");
		System.out.println("录入_" + "被保人" + "姓名 : "
				+ StrTool.unicodeToGBK(mLCInsuredSchema.getName()));
		System.out.println("录入_" + "被保人" + "性别 : "
				+ StrTool.unicodeToGBK(mLCInsuredSchema.getSex()));
		System.out.println("录入_" + "被保人" + "生日 :　"
				+ StrTool.unicodeToGBK(mLCInsuredSchema.getBirthday()));
		System.out.println("录入_" + "被保人" + "证件类型 :　"
				+ StrTool.unicodeToGBK(mLCInsuredSchema.getIDType()));
		System.out.println("录入_" + "被保人" + "证件号码 :　"
				+ StrTool.unicodeToGBK(mLCInsuredSchema.getIDNo()));

		LDPersonDB tLDPersonDB = new LDPersonDB();
		if (!StrTool.unicodeToGBK(
				StrTool.cTrim(this.mLCInsuredSchema.getInsuredNo())).equals("")) {
			tLDPersonDB.setCustomerNo(mLCInsuredSchema.getInsuredNo());
			if (!tLDPersonDB.getInfo()) {
				// @@错误处理
				buildError("checkInsured", "录入被保人客户号，但是没有找到此客户号对应的客户信息！！");
				return false;
			} else {
				this.mInsuredNo = mLCInsuredSchema.getInsuredNo();
				if (!StrTool.unicodeToGBK(
						StrTool.cTrim(this.mLCInsuredSchema.getName())).equals(
						StrTool.cTrim(tLDPersonDB.getName()))) {
					buildError("checkInsured",
							"被保客户姓名与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
					return false;
				}
				if (!StrTool.cTrim(this.mLCInsuredSchema.getBirthday()).equals(
						StrTool.cTrim(tLDPersonDB.getBirthday()))) {
					buildError("checkInsured",
							"被保客户生日与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
					return false;
				}
				/** 性别 */
				if (!StrTool.cTrim(this.mLCInsuredSchema.getSex()).equals(
						StrTool.cTrim(tLDPersonDB.getSex()))) {
					buildError("checkInsured",
							"被保客户性别与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
					return false;
				}
				/** 证件类型 */
				if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).equals(
						StrTool.cTrim(tLDPersonDB.getIDType()))) {
					buildError("checkInsured",
							"被保客户证件类型与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
					return false;
				}

				/** 证件号码 */
				// 将客户身份证号码中的x转换成大写（被保险人） 2009-02-17 liuyp
				if (this.mLCInsuredSchema.getIDType() != null
						&& tLDPersonDB.getIDType() != null) {
					if (this.mLCInsuredSchema.getIDType().equals("0")
							&& tLDPersonDB.getIDType().equals("0")) {
						if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo())
								.toUpperCase().equals(
										StrTool.cTrim(tLDPersonDB.getIDNo())
												.toUpperCase())) {
							buildError("checkInsured",
									"被保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
							return false;
						}
					}
				} else {
					if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).equals(
							StrTool.cTrim(tLDPersonDB.getIDNo()))) {
						buildError("checkInsured",
								"被保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
						return false;
					}
				}
			}
		} else {
			/** 如果没有录入客户号码，根据基本信息校验是否是老客户 */
			if (!queryInsuerd()) {
				return false;
			}
		}
		System.out.println("校验完成，无论是否应该生成客户都应该把投保人号码存入" + this.mAppntNo);
		mLCInsuredSchema.setAppntNo(this.mAppntNo);
		mLCInsuredSchema.setOperator(this.mGlobalInput.Operator);
		PubFun.fillDefaultField(mLCInsuredSchema);
		return true;
	}

	/**
	 * queryInsuerd
	 * 
	 * @return boolean
	 */
	private boolean queryInsuerd() {
		LDPersonDB tLDPersonDB = new LDPersonDB();
		String Sex = "";
		String Birthday = "";
		if (!StrTool.cTrim(this.mLCInsuredSchema.getName()).equals("")) {
			tLDPersonDB.setName(this.mLCInsuredSchema.getName());
		} else {
			buildError("queryInsuerd", "没有录入被保人姓名！");
			return false;
		}
		if (!StrTool.cTrim(this.mLCInsuredSchema.getSex()).equals("")) {
			Sex = this.mLCInsuredSchema.getSex();
		} else {
			buildError("queryInsuerd", "没有录入被保人性别！");
			return false;
		}
		if (!StrTool.cTrim(this.mLCInsuredSchema.getBirthday()).equals("")) {
			Birthday = this.mLCInsuredSchema.getBirthday();
		} else {
			buildError("queryInsuerd", "没有录入被保人生日！");
			return false;
		}
		if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).equals("")
				|| mMissionProp5.equals("2") || mMissionProp5.equals("3")) {
			// 证件类型校验 by zhangyang 2011-01-12
			String tIDType = this.mLCInsuredSchema.getIDType();
			if (!tIDType.equals("0") && !tIDType.equals("1")
					&& !tIDType.equals("2") && !tIDType.equals("3")
					&& !tIDType.equals("4") && !tIDType.equals("5") && !tIDType.equals("6") && !tIDType.equals("7")) {
				buildError("queryInsuerd", "被保险人的证件类型为" + tIDType
						+ ", 不符合系统规范，系统规定证件类型必须为数字的0到7！");
				return false;
			}
			// -------------------------------------

			tLDPersonDB.setIDType(this.mLCInsuredSchema.getIDType());
		} else {
			buildError("queryInsuerd", "没有录入被保人证件类型！！");
			return false;
		}
		if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).equals("")) {
			tLDPersonDB.setIDNo(this.mLCInsuredSchema.getIDNo());
		} else if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType())
				.equals("4")) {
			buildError("queryInsuerd", "被保人证件号码录入错误！！");
			return false;
		}

		LDPersonSet tLDPersonSet = new LDPersonSet();

		if (tLDPersonDB.getIDType().equals("0")) {
			tLDPersonSet = tLDPersonDB.query();
		} else {
			tLDPersonDB.setSex(Sex);
			tLDPersonDB.setBirthday(Birthday);
			tLDPersonSet = tLDPersonDB.query();
		}

		if (tLDPersonSet.size() >= 1) {
			/** 找到为一个客户信息 */
			/** 取出当前客户的客户号码 */
			this.mLCInsuredSchema.setInsuredNo(tLDPersonSet.get(1)
					.getCustomerNo());
			this.mInsuredNo = mLCInsuredSchema.getInsuredNo();
		} else if (tLDPersonSet.size() == 0) {
			this.needCreatInsured = true;
		} else {
			buildError("queryInsuerd", "不会这么巧吧,存在相近客户,请检查客户信息！");
			return false;
		}
		return true;
	}

	/**
	 * dealAppntToInsured
	 * 
	 * @return boolean
	 */
	private boolean dealAppntToInsured() {
		System.out.println("投保人和被保人是本人处理");
		mLCInsuredSchema.setInsuredNo(mLCAppntSchema.getAppntNo());
		mLCInsuredSchema.setAccName(mLCAppntSchema.getAccName());
		mLCInsuredSchema.setAddressNo(mLCAppntSchema.getAddressNo());
		mLCInsuredSchema.setAppntNo(mLCAppntSchema.getAppntNo());
		// mLCInsuredSchema.setAvoirdupois(mLCAppntSchema);
		// mLCInsuredSchema.setAvoirdupois();
		// mLCInsuredSchema.setBankAccNo(mLCAppntSchema);
		// mLCInsuredSchema.setBankCode();
		mLCInsuredSchema.setBirthday(mLCAppntSchema.getAppntBirthday());
		mLCInsuredSchema.setIDStartDate(mLCAppntSchema.getIDStartDate());
		mLCInsuredSchema.setIDEndDate(mLCAppntSchema.getIDEndDate());
		// mLCInsuredSchema.setBMI();
		mLCInsuredSchema.setEnglishName(mLCAppntSchema.getEnglishName());
		mLCInsuredSchema.setOccupationCode(mLCAppntSchema.getOccupationCode());
		mLCInsuredSchema.setOccupationType(mLCAppntSchema.getOccupationType());
		mLCInsuredSchema.setName(mLCAppntSchema.getAppntName());
		mLCInsuredSchema.setSex(mLCAppntSchema.getAppntSex());
		mLCInsuredSchema.setIDType(mLCAppntSchema.getIDType());
		mLCInsuredSchema.setIDNo(mLCAppntSchema.getIDNo());
		mLCInsuredSchema.setOperator(mGlobalInput.Operator);
		mLCInsuredSchema.setManageCom(mLCContSchema.getManageCom());
		mLCInsuredSchema.setExecuteCom(mGlobalInput.ManageCom);
		PubFun.fillDefaultField(mLCInsuredSchema);
		return true;
	}

	/**
	 * dealAppntToCont
	 * 
	 * @return boolean
	 */
	private boolean dealAppntToCont() {
		this.mLCContSchema.setAppntBirthday(this.mLCAppntSchema
				.getAppntBirthday());
		this.mLCContSchema.setAppntIDNo(this.mLCAppntSchema.getIDNo());
		this.mLCContSchema.setAppntIDType(this.mLCAppntSchema.getIDType());
		this.mLCContSchema.setAppntName(this.mLCAppntSchema.getAppntName());
		this.mLCContSchema.setAppntNo(this.mLCAppntSchema.getAppntNo());
		this.mLCContSchema.setAppntSex(this.mLCAppntSchema.getAppntSex());
		// this.mLCContSchema.setBankAccNo(this.mLCAppntSchema.getBankAccNo());
		// this.mLCContSchema.setBankCode(this.mLCAppntSchema.getBankCode());
		// this.mLCContSchema.setAccName(this.mLCAppntSchema.getAccName());
		for (int i = 1; i <= this.mLCPolSet.size(); i++) {
			this.mLCPolSet.get(i).setAppntName(
					this.mLCAppntSchema.getAppntName());
			this.mLCPolSet.get(i).setAppntNo(this.mLCAppntSchema.getAppntNo());
		}

		return true;
	}

	/**
	 * queryAppnt
	 * 
	 * @return boolean
	 */
	private boolean queryAppnt() {
		LDPersonDB tLDPersonDB = new LDPersonDB();
		String Sex = "";
		String Birthday = "";

		if (!StrTool.cTrim(this.mLCAppntSchema.getAppntName()).equals("")) {
			tLDPersonDB.setName(this.mLCAppntSchema.getAppntName());
		} else {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有录入投保人姓名！";
			System.out.println("程序第138行出错，" + "请检查BriefSingleContInputBL中的"
					+ "dealData方法！" + tError.errorMessage);
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!StrTool.cTrim(this.mLCAppntSchema.getAppntSex()).equals("")) {
			Sex = this.mLCAppntSchema.getAppntSex();
		} else {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有录入投保人性别！";
			System.out.println("程序第152行出错，" + "请检查BriefSingleContInputBL中的"
					+ "dealData方法！" + tError.errorMessage);
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!StrTool.cTrim(this.mLCAppntSchema.getAppntBirthday()).equals("")) {
			Birthday = this.mLCAppntSchema.getAppntBirthday();
		} else {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有录入投保人生日！";
			System.out.println("程序第167行出错，" + "请检查BriefSingleContInputBL中的"
					+ "dealData方法！" + tError.errorMessage);
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).equals("")
				|| mMissionProp5.equals("2") || mMissionProp5.equals("3")) {
			tLDPersonDB.setIDType(this.mLCAppntSchema.getIDType());
		} else {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有录入投保人证件类型！";
			System.out.println("程序第181行出错，" + "请检查BriefSingleContInputBL中的"
					+ "dealData方法！" + tError.errorMessage);
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).equals("")) {
			tLDPersonDB.setIDNo(this.mLCAppntSchema.getIDNo());
		} else if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).equals("4")) {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "投保人证件号码录入错误！";
			System.out.println("程序第194行出错，" + "请检查BriefSingleContInputBL中的"
					+ "dealData方法！" + tError.errorMessage);
			this.mErrors.addOneError(tError);
			return false;
		}

		LDPersonSet tLDPersonSet = new LDPersonSet();

		if (tLDPersonDB.getIDType().equals("0")) {
			tLDPersonSet = tLDPersonDB.query();
		} else {
			tLDPersonDB.setSex(Sex);
			tLDPersonDB.setBirthday(Birthday);
			tLDPersonSet = tLDPersonDB.query();
		}

		if (tLDPersonSet.size() == 1) {
			/** 找到为一个客户信息 */
			/** 取出当前客户的客户号码 */
			this.mLCAppntSchema.setAppntNo(tLDPersonSet.get(1).getCustomerNo());
			this.mAppntNo = tLDPersonSet.get(1).getCustomerNo();
			if (!StrTool.cTrim(mWorkName).equals("")) {
				if (map == null) {
					map = new MMap();
				}
				map.put("update LDPerson set GrpName='" + mWorkName
						+ "' where customerno='" + mAppntNo + "'", "UPDATE");
			}
		} else if (tLDPersonSet.size() == 0) {
			this.needCreatAppnt = true;
		} else {
			CError tError = new CError();
			tError.moduleName = "BriefSingleContInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "不会这么巧吧,存在相近客户,请检查客户信息！";
			System.out.println("程序第214行出错，" + "请检查BriefSingleContInputBL中的"
					+ "dealData方法！" + tError.errorMessage);
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * checkAppnt
	 * 
	 * @return boolean
	 * @param tLDPersonSchema
	 *            LDPersonSchema
	 */
	private boolean checkAppnt(LDPersonSchema tLDPersonSchema) {
		System.out.println("系统同录入的投保人客户号码，开始验证客户信息");
		System.out.println("录入_投保人姓名 : "
				+ StrTool.unicodeToGBK(mLCAppntSchema.getAppntName()));
		System.out.println("录入_投保人性别 : "
				+ StrTool.unicodeToGBK(mLCAppntSchema.getAppntSex()));
		System.out.println("录入_投保人生日 :　"
				+ StrTool.unicodeToGBK(mLCAppntSchema.getAppntBirthday()));
		System.out.println("录入_投保人证件类型 :　"
				+ StrTool.unicodeToGBK(mLCAppntSchema.getIDType()));
		System.out.println("录入_投保人证件号码 :　"
				+ StrTool.unicodeToGBK(mLCAppntSchema.getIDNo()));
		if (!StrTool.unicodeToGBK(
				StrTool.cTrim(this.mLCAppntSchema.getAppntName())).equals(
				StrTool.cTrim(tLDPersonSchema.getName()))) {
			System.out.println("系统中的客户姓名为 : "
					+ StrTool.cTrim(tLDPersonSchema.getName()));
			buildError("checkAppnt",
					"投保客户姓名与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
			return false;
		}
		if (!StrTool.cTrim(this.mLCAppntSchema.getAppntBirthday()).equals(
				StrTool.cTrim(tLDPersonSchema.getBirthday()))) {
			buildError("checkAppnt",
					"投保客户生日与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
			return false;
		}
		/** 性别 */
		if (!StrTool.cTrim(this.mLCAppntSchema.getAppntSex()).equals(
				StrTool.cTrim(tLDPersonSchema.getSex()))) {
			buildError("checkAppnt",
					"投保客户性别与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
			return false;
		}
		/** 证件类型 */
		if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).equals(
				StrTool.cTrim(tLDPersonSchema.getIDType()))) {
			buildError("checkAppnt",
					"投保客户证件类型与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
			return false;
		}

		/** 证件号码 */
		// 将客户身份证号码中的x转换成大写（投保人） 2009-02-17 liuyp
		if (this.mLCAppntSchema.getIDType() != null
				&& tLDPersonSchema.getIDType() != null) {
			if (this.mLCAppntSchema.getIDType().equals("0")
					&& tLDPersonSchema.getIDType().equals("0")) {
				if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).toUpperCase()
						.equals(
								StrTool.cTrim(tLDPersonSchema.getIDNo())
										.toUpperCase())) {
					buildError("checkAppnt",
							"投保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
					return false;
				}
			}
		} else {
			if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).equals(
					StrTool.cTrim(tLDPersonSchema.getIDNo()))) {
				buildError("checkAppnt",
						"投保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
				return false;
			}
		}
		return true;
	}

	/**
	 * checkData
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		if (mLCContSchema.getCValiDate() == null
				|| mLCContSchema.getCValiDate().equals("")) {
			mErrors.addOneError("请录入生效日期");
			return false;
		}

		System.out.println("Into BriefSingleContInputBL.checkData()...");
		/** 开始校验前台传入的数据 */
		/** 被保人与投保人是 */
		if (StrTool.cTrim(this.mOperate).equals("INSERT||MAIN")) {
			/** 删除操作或者更新操作 */
//			if (!StrTool.cTrim(mLCContSchema.getContNo()).equals("")) {
//				buildError("checkData", "合同信息已保存,如果需要修改保存后的数据需要点击修改按钮！");
//				return false;
//			}
			if (StrTool.cTrim(this.mLCContSchema.getPrtNo()).equals("")) {
				buildError("checkData", "保存合同信息失败,原因是没有录入印刷号码！");
				return false;
			}
//			if (StrTool.cTrim(this.mLCAppntSchema.getAppntName()).equals("")) {
//				buildError("checkData", "没有录入投保人姓名！");
//				return false;
//			}

		}
		if (!StrTool.cTrim(this.mOperate).equals("INSERT||MAIN")) {
			if (StrTool.cTrim(this.mLCContSchema.getContNo()).equals("")) {
				buildError("checkData", "执行删除或修改失败,原因是没有传入合同号码,请确认合同是否保存！");
				return false;
			}
		}

		for (int i = 1; i <= mLCNationSet.size(); i++) {
			System.out.println("国家代码　：");
			System.out.println(mLCNationSet.get(i).getNationNo());
		}
		/** 校验险种信息 */
		if (this.mLCPolSet == null || this.mLCPolSet.size() <= 0) {
			buildError("checkData", "录入险种信息不完整，请确认是否选择险种信息！");
			return false;
		}
		/** 增加校验 */
		if (mLCContSchema.getAppFlag() != null
				&& mLCContSchema.getAppFlag().equals("1")) {
			buildError("checkData", "保单已签单完成,不能进行任何后续操作！");
			return false;
		}
		return true;
	}

	/**
	 * getInputData
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		try {
			System.out.println("Into BriefSingleContInputBL.getInputData()...");
			if (this.mInputData == null) {
				CError tError = new CError();
				tError.moduleName = "BriefSingleContInputBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "传入的封装对象为空！";
				System.out.println("程序第97行出错，" + "请检查BriefSingleContInputBL中的"
						+ "getInputData方法！" + tError.errorMessage);
				this.mErrors.addOneError(tError);
				return false;
			}
			if (this.mOperate == null) {
				CError tError = new CError();
				tError.moduleName = "BriefSingleContInputBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "没有传入操作符！";
				System.out.println("程序第169行出错，" + "请检查BriefSingleContInputBL中的"
						+ "getInputData方法！" + tError.errorMessage);
				this.mErrors.addOneError(tError);
				return false;
			}
			mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName(
					"LCContSchema", 0);
			System.out.println("银行帐号银行帐号是yyyyyyyyyyy:"
					+ mLCContSchema.getBankAccNo());
			if (mLCContSchema.getPolApplyDate() != null
					&& !"".equals(mLCContSchema.getPolApplyDate())) {
				polApplyDate = mLCContSchema.getPolApplyDate();
			} else {
				polApplyDate = PubFun.getCurrentDate();
			}
			System.out.println("************  PolApplyDate : " + polApplyDate
					+ "************");
			mLCAppntSchema = (LCAppntSchema) mInputData.getObjectByObjectName(
					"LCAppntSchema", 0);
			mLCInsuredSchema = (LCInsuredSchema) mInputData
					.getObjectByObjectName("LCInsuredSchema", 0);
			mLCBnfSchema = (LCBnfSchema) mInputData.getObjectByObjectName(
					"LCBnfSchema", 0);
			// mLDPersonSet = (LDPersonSet) mInputData.getObjectByObjectName(
			// "LDPersonSet", 0);
			mLCPolSet = (LCPolSet) mInputData.getObjectByObjectName("LCPolSet",
					0);
			mLCDutySet = (LCDutySet) mInputData.getObjectByObjectName(
					"LCDutySet", 0);
			mLCRiskDutyWrapSet = (LCRiskDutyWrapSet) mInputData
					.getObjectByObjectName("LCRiskDutyWrapSet", 0);

			// 客户告知
			mCusImpartSet = (LCCustomerImpartSet) mInputData
					.getObjectByObjectName("LCCustomerImpartSet", 0);

			mCusImpartDetailSet = (LCCustomerImpartDetailSet) mInputData
					.getObjectByObjectName("LCCustomerImpartDetailSet", 0);
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
			mTransferData = (TransferData) mInputData.getObjectByObjectName(
					"TransferData", 0);
			if (mTransferData == null) {
				buildError("getInputData", "传入的地址信息为空！");
				return false;
			}

			for (int i = 1; mLCPolSet != null && i <= mLCPolSet.size(); i++) {
				// 险种编码是"000000"的险种不需要处理
				// （在外包导入的时候，由于部分合同级别信息同过POLTABLE传入，保障按套餐录入的保单的POLTABLE的RiskCode=000000）
				if ("000000".equals(mLCPolSet.get(i).getRiskCode())) {
					mLCPolSet.removeRange(i, i);
					i--;
					continue;
				}
			}

			this.mAppntAddressSchema = (LCAddressSchema) mTransferData
					.getValueByName("AppntAddress");
			this.mInsuredAddressSchema = (LCAddressSchema) mTransferData
					.getValueByName("InsuredAddress");
			// add by zhangxing
			mMissionProp5 = (String) mTransferData
					.getValueByName("MissionProp5");
			mMsgType = (String) mTransferData.getValueByName("mMsgType");
			mWorkName = (String) mTransferData.getValueByName("WorkName");
			tComparePrem = (String) mTransferData.getValueByName("tComparePrem");
			System.out.println("mWorkName的值&&&&&&&&&&&:" + mWorkName);
			System.out.println("mMissionProp5的值&&&&&&&&&&&:" + mMissionProp5);
			/** 获取前台传入的国家代码 */
			this.mLCNationSet = (LCNationSet) mInputData.getObjectByObjectName(
					"LCNationSet", 0);
			for (int i = 1; mLCNationSet != null && i <= mLCNationSet.size(); i++) {
				System.out.println("国家代码　：");
				System.out.println(mLCNationSet.get(i).getNationNo());
			}
			this.mLCBnfSchema = (LCBnfSchema) mInputData.getObjectByObjectName(
					"LCBnfSchema", 0);
			
			this.mInputLCBnfSet = (LCBnfSet) mInputData.getObjectByObjectName(
					"LCBnfSet", 0);
			/** 套餐险种 */
//			if (mLCRiskDutyWrapSet != null && mLCRiskDutyWrapSet.size() > 0) {
//				System.out.println("*********************"
//						+ mLCRiskDutyWrapSet.size());
//				if (!dealWrap()) {
//					return false;
//				}
//			}

			if (mLCAppntSchema != null
					&& this.mLCAppntSchema.getOccupationCode() != null
					&& !this.mLCAppntSchema.getOccupationCode().equals("")) {
				LDOccupationDB tLDOccupationDB = new LDOccupationDB();
				tLDOccupationDB.setOccupationCode(mLCAppntSchema
						.getOccupationCode());
				if (!tLDOccupationDB.getInfo()) {
					String str = "查询投保人职业代码失败！";
					buildError("getInputData", str);
					System.out
							.println("在程序BriefSingleContInputBL.getInputData() - 1923 : "
									+ str);
					return false;
				}
				mLCAppntSchema.setOccupationType(tLDOccupationDB
						.getOccupationType());
			}
			if (mLCInsuredSchema != null
					&& this.mLCInsuredSchema.getOccupationCode() != null
					&& !this.mLCInsuredSchema.getOccupationCode().equals("")) {
				LDOccupationDB tLDOccupationDB = new LDOccupationDB();
				tLDOccupationDB.setOccupationCode(mLCInsuredSchema
						.getOccupationCode());
				if (!tLDOccupationDB.getInfo()) {
					String str = "查询被保人职业代码失败！";
					buildError("getInputData", str);
					System.out
							.println("在程序BriefSingleContInputBL.getInputData() - 1923 : "
									+ str);
					return false;
				}
				mLCInsuredSchema.setOccupationType(tLDOccupationDB
						.getOccupationType());
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("获取数据失败，具体原因是：" + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * 处理套餐信息
	 * 
	 * @return boolean
	 */
	private boolean dealWrap() {
		getWrapPolInfo(); // 得到待处理套餐的险种信息

		getLDRiskDutyWrapInfo(); // 得到传入险种或责任套餐要素的描述信息

		setDefaultCalFactor(); // 从系统获取相关默认要素值

		parseWrapInfoToLCPol(); // 将套餐信息解析到险种

		if (mLCRiskDutyWrapSet != null && mLCRiskDutyWrapSet.size() > 0) {
			for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++) {
				// 注意，简易险平台中，这个地方可能没有生成ContNo、所以在后面还有对ContNo、PrtNo的赋值
				// 但国际业务中存在
				mLCRiskDutyWrapSet.get(i).setContNo(mLCContSchema.getContNo());
				mLCRiskDutyWrapSet.get(i).setPrtNo(mLCContSchema.getPrtNo());

				String tStrCalSql = mLCRiskDutyWrapSet.get(i).getCalSql();
				if (tStrCalSql != null)
					tStrCalSql = tStrCalSql.replaceAll("'", "''");
				mLCRiskDutyWrapSet.get(i).setCalSql(tStrCalSql); // CalSql无用，不需要赋值
			}
		}

		return true;
	}

	/**
	 * 将套餐信息解析到险种
	 */
	private void parseWrapInfoToLCPol() {
		Reflections ref = new Reflections();
		ref.transFields(mLCRiskDutyWrapSet, mLDRiskDutyWrapSet);

		for (int i = 1; i <= mLCPolSet.size(); i++) {
			for (int n = 1; n <= mLCRiskDutyWrapSet.size(); n++) {
				LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = mLCRiskDutyWrapSet
						.get(n);

				// 只处理本险种的计算要素
				if (tLCRiskDutyWrapSchema.getRiskCode() != null
						&& !tLCRiskDutyWrapSchema.getRiskCode().equals("")
						&& !tLCRiskDutyWrapSchema.getRiskCode().equals("null")
						&& !tLCRiskDutyWrapSchema.getRiskCode()
								.equals("000000")
						&& !tLCRiskDutyWrapSchema.getRiskCode().equals(
								mLCPolSet.get(i).getRiskCode())) {
					continue;
				}

				if (tLCRiskDutyWrapSchema.getCalFactor().equals("InsuYear")) {
					needInsuYear = false;
				}
				System.out.println(tLCRiskDutyWrapSchema.getCalFactorType());
				if (tLCRiskDutyWrapSchema.getCalFactorType().equals("1")) {
					/** 直接取值 */
					mLCPolSet.get(i).setV(tLCRiskDutyWrapSchema.getCalFactor(),
							tLCRiskDutyWrapSchema.getCalFactorValue());
				} else if (tLCRiskDutyWrapSchema.getCalFactorType().equals("2")) {
					/** 准备要素 calbase */
					PubCalculator tCal = new PubCalculator();
					tCal.addBasicFactor("RiskCode", mLCPolSet.get(i)
							.getRiskCode());
					for (int m = 1; m <= mLCRiskDutyWrapSet.size(); m++) {
						// 得到对应险种的计算要素
						if (mLCRiskDutyWrapSet.get(m).getRiskCode().equals(
								mLCPolSet.get(i).getRiskCode())) {
							tCal.addBasicFactor(mLCRiskDutyWrapSet.get(m)
									.getCalFactor(), mLCRiskDutyWrapSet.get(m)
									.getCalFactorValue());
						}
					}
					/** 计算 */
					tCal.setCalSql(tLCRiskDutyWrapSchema.getCalSql());
					System.out.println(tLCRiskDutyWrapSchema.getCalSql());
					mLCPolSet.get(i).setV(tLCRiskDutyWrapSchema.getCalFactor(),
							tCal.calculate());
				}
			} // for (int n = 1; n <= mLCRiskDutyWrapSet.size(); n++)
		} // for (int i = 1; i <= mLCPolSet.size(); i++)
	}

	/**
	 * 得到待处理的套餐信息的描述信息
	 */
	private void getLDRiskDutyWrapInfo() {
		HashSet keys = new HashSet();
		for (int i = 1; i <= mLCPolSet.size(); i++) {
			System.out.println(mLCRiskDutyWrapSet.size());
			for (int m = 1; m <= mLCRiskDutyWrapSet.size(); m++) {
				LDRiskDutyWrapDB tLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
				tLDRiskDutyWrapDB.setRiskCode(mLCPolSet.get(i).getRiskCode());
				tLDRiskDutyWrapDB.setRiskWrapCode(mLCRiskDutyWrapSet.get(m)
						.getRiskWrapCode());

				if (mLCRiskDutyWrapSet.get(m).getDutyCode() != null
						&& !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("")
						&& !mLCRiskDutyWrapSet.get(m).getDutyCode().equals(
								"null")
						&& !mLCRiskDutyWrapSet.get(m).getDutyCode().equals(
								"000000")) {
					tLDRiskDutyWrapDB.setDutyCode(mLCRiskDutyWrapSet.get(m)
							.getDutyCode());
				}

				tLDRiskDutyWrapDB.setCalFactor(mLCRiskDutyWrapSet.get(m)
						.getCalFactor());
				LDRiskDutyWrapSet tLDRiskDutyWrapSet = tLDRiskDutyWrapDB
						.query();
				if (tLDRiskDutyWrapSet == null
						|| tLDRiskDutyWrapSet.size() <= 0) {
					continue;
				}

				// 下面是为了为避免套餐责任信息的重复
				for (int temp = 1; temp <= tLDRiskDutyWrapSet.size(); temp++) {
					String key = tLDRiskDutyWrapSet.get(temp).getRiskWrapCode()
							+ tLDRiskDutyWrapSet.get(temp).getRiskCode()
							+ tLDRiskDutyWrapSet.get(temp).getDutyCode()
							+ tLDRiskDutyWrapSet.get(temp).getCalFactor();
					if (!keys.contains(key)) {
						keys.add(key);
						mLDRiskDutyWrapSet.add(tLDRiskDutyWrapSet.get(temp));
					}
				}
			}// for(int m = 1; m <= mLCRiskDutyWrapSet.size(); m++)
		}// for(int i = 1; i <= mLCPolSet.size(); i++)

		// 将传入的要素值存储到mLDRiskDutyWrapSet
		for (int i = 1; i <= this.mLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet
					.get(i);
			if (!"2".equals(tLDRiskDutyWrapSchema.getCalFactorType())) {
				continue;
			}
			for (int m = 1; m <= mLCRiskDutyWrapSet.size(); m++) {
				LCRiskDutyWrapSchema tLCRiskdutyWrapSchema = mLCRiskDutyWrapSet
						.get(m).getSchema();

				// 如果传入了责任编码，必须用责任编码和计算要素进行判断
				if (mLCRiskDutyWrapSet.get(m).getDutyCode() != null
						&& !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("")
						&& !mLCRiskDutyWrapSet.get(m).getDutyCode().equals(
								"null")
						&& !mLCRiskDutyWrapSet.get(m).getDutyCode().equals(
								"000000")) {
					if (tLCRiskdutyWrapSchema.getDutyCode().equals(
							tLDRiskDutyWrapSchema.getDutyCode())
							&& tLCRiskdutyWrapSchema.getCalFactor().equals(
									tLDRiskDutyWrapSchema.getCalFactor())
							&& tLCRiskdutyWrapSchema.getRiskWrapCode().equals(
									tLDRiskDutyWrapSchema.getRiskWrapCode())) {
						tLDRiskDutyWrapSchema
								.setCalFactorValue(tLCRiskdutyWrapSchema
										.getCalFactorValue());
					}
				}
				// 否则只用计算要素就可以
				else if (tLCRiskdutyWrapSchema.getCalFactor().equals(
						tLDRiskDutyWrapSchema.getCalFactor())
						&& tLCRiskdutyWrapSchema.getRiskWrapCode().equals(
								tLDRiskDutyWrapSchema.getRiskWrapCode())) {
					System.out.println("LDRiskDutyWrap : "
							+ tLDRiskDutyWrapSchema.getCalFactor()
							+ " value : "
							+ tLDRiskDutyWrapSchema.getCalFactorValue());
					tLDRiskDutyWrapSchema
							.setCalFactorValue(tLCRiskdutyWrapSchema
									.getCalFactorValue());
				}
			} // for(int m = 1; m <= mLCRiskDutyWrapSet.size(); m++)
		} // for(int i = 1; i <= this.mLDRiskDutyWrapSet.size(); i++)
	}

	/**
	 * 得到待处理套餐的险种信息
	 */
	private void getWrapPolInfo() {
		if (mLCPolSet == null) {
			mLCPolSet = new LCPolSet();
		}

		// 得到本次需要处理的险种信息
		String tWrapCode = "";
		String tRiskcode = "";
		// 考虑传递参数可能无顺序，故不采用continue方式，采用HashSet过滤重复险种编码
		HashSet RiskKeys = new HashSet();
		for (int i = 1; i <= this.mLCRiskDutyWrapSet.size(); i++) {

			if (tWrapCode.equals(mLCRiskDutyWrapSet.get(i).getRiskWrapCode())) {
				continue;
			}
			tWrapCode = mLCRiskDutyWrapSet.get(i).getRiskWrapCode();
			LDRiskWrapDB tLDRiskWrapDB = new LDRiskWrapDB();
			tLDRiskWrapDB.setRiskWrapCode(tWrapCode);
			LDRiskWrapSet tLDRiskWrapSet = tLDRiskWrapDB.query();
			for (int j = 1; j <= tLDRiskWrapSet.size(); j++) {
				LCPolSchema tLCPolSchema = new LCPolSchema();
				tLCPolSchema.setRiskCode(tLDRiskWrapSet.get(j).getRiskCode());
				mLCPolSet.add(tLCPolSchema);
			}

		}
	}

	/**
	 * setDefaultCalFactor 读取险种或责任对应套餐的默认要素
	 * 
	 * @param tLCPolSchema
	 *            LCPolSchema
	 */
	private void setDefaultCalFactor() {
		HashSet keys = new HashSet(); // 存储每个要素的“套餐编码+险种编码+责任编码+计算要素”，用来进行重复要素的过滤
		for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
			keys.add(mLDRiskDutyWrapSet.get(i).getRiskWrapCode()
					+ mLDRiskDutyWrapSet.get(i).getRiskCode()
					+ mLDRiskDutyWrapSet.get(i).getDutyCode()
					+ mLDRiskDutyWrapSet.get(i).getCalFactor());
		}

		// 循环每个套餐要素，这样做是为了避免查询到业务不需要的责任的要素，如国际业务的责任是可选的
		int tWrapCount = mLDRiskDutyWrapSet.size(); // 查询到默认套餐后，mLDRiskDutyWrapSet的大小可能会动态增加，此处用tWrapCount保证最少循环
		for (int m = 1; m <= tWrapCount; m++) {
			LDRiskDutyWrapDB tLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
			tLDRiskDutyWrapDB.setRiskWrapCode(mLDRiskDutyWrapSet.get(m)
					.getRiskWrapCode());
			tLDRiskDutyWrapDB.setRiskCode(mLDRiskDutyWrapSet.get(m)
					.getRiskCode());
			tLDRiskDutyWrapDB.setDutyCode(mLDRiskDutyWrapSet.get(m)
					.getDutyCode());
			tLDRiskDutyWrapDB.setCalFactorType("1"); // 默认要素
			LDRiskDutyWrapSet tLDRiskDutyWrapSet = tLDRiskDutyWrapDB.query();
			if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() <= 0) {
				continue;
			}

			for (int temp = 1; temp <= tLDRiskDutyWrapSet.size(); temp++) {
				// 下面是为了为避免套餐责任信息的重复
				String key = tLDRiskDutyWrapSet.get(temp).getRiskWrapCode()
						+ tLDRiskDutyWrapSet.get(temp).getRiskCode()
						+ tLDRiskDutyWrapSet.get(temp).getDutyCode()
						+ tLDRiskDutyWrapSet.get(temp).getCalFactor();
				if (!keys.contains(key)) {
					keys.add(key);
					mLDRiskDutyWrapSet.add(tLDRiskDutyWrapSet.get(temp));
				}
			}
		}
	}

	/**
	 * getResult
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "BriefSingleContInputBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.err.println("程序报错：" + cError.errorMessage);
	}

	/**
	 * 调试函数
	 * 
	 * @param args
	 *            String[]
	 */
	public static void main(String[] args) {

	}

	/**
	 * 校验字段
	 * 
	 * @return boolean
	 * @param tLCPolSchema
	 *            LCPolSchema
	 * @param operType
	 *            String
	 */
	private boolean CheckTBField(LCPolSchema tLCPolSchema, String operType) {
		// 保单 mLCPolBL mLCGrpPolBL
		// 投保人 mLCAppntBL mLCAppntGrpBL
		// 被保人 mLCInsuredBLSet mLCInsuredBLSetNew
		// 受益人 mLCBnfBLSet mLCBnfBLSetNew
		// 告知信息 mLCCustomerImpartBLSet mLCCustomerImpartBLSetNew
		// 特别约定 mLCSpecBLSet mLCSpecBLSetNew
		// 保费项表 mLCPremBLSet 保存特殊的保费项数据(目前针对磁盘投保，不用计算保费保额类型)
		// 给付项表 mLCGetBLSet
		// 一般的责任信息 mLCDutyBL
		// 责任表 mLCDutyBLSet
		String strMsg = "";
		boolean MsgFlag = false;

		String RiskCode = tLCPolSchema.getRiskCode();

		try {
			VData tVData = new VData();
			CheckFieldCom tCheckFieldCom = new CheckFieldCom();

			// 计算要素
			FieldCarrier tFieldCarrier = new FieldCarrier();
			tFieldCarrier.setAppAge(tLCPolSchema.getInsuredAppAge()); // 被保人年龄
			tFieldCarrier.setInsuredName(tLCPolSchema.getInsuredName()); // 被保人姓名
			tFieldCarrier.setSex(tLCPolSchema.getInsuredSex()); // 被保人性别
			tFieldCarrier.setInsuredNo(tLCPolSchema.getInsuredNo()); // 被保人号
			tFieldCarrier.setMult(tLCPolSchema.getMult()); // 投保份数
			tFieldCarrier.setPolNo(tLCPolSchema.getPolNo()); // 投保单号码
			tFieldCarrier.setContNo(tLCPolSchema.getContNo()); // 投保单合同号码
			tFieldCarrier.setMainPolNo(tLCPolSchema.getMainPolNo()); // 主险号码
			tFieldCarrier.setRiskCode(tLCPolSchema.getRiskCode()); // 险种编码
			tFieldCarrier.setCValiDate(tLCPolSchema.getCValiDate()); // 生效日期
			tFieldCarrier.setAmnt(tLCPolSchema.getAmnt()); // 保额
			tFieldCarrier.setInsuredBirthday(tLCPolSchema.getInsuredBirthday()); // 被保人出生日期
			tFieldCarrier.setInsuYear(tLCPolSchema.getInsuYear()); // 保险期间
			tFieldCarrier.setInsuYearFlag(tLCPolSchema.getInsuYearFlag()); // 保险期间单位
			tFieldCarrier.setPayEndYear(tLCPolSchema.getPayEndYear()); // 交费期间
			tFieldCarrier.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag()); // 交费期间单位
			tFieldCarrier.setPayIntv(tLCPolSchema.getPayIntv()); // 交费方式
			tFieldCarrier.setPayYears(tLCPolSchema.getPayYears()); // 交费年期
			tFieldCarrier.setOccupationType(tLCPolSchema.getOccupationType()); // 被保人职业类别
			tFieldCarrier.setGrpPolNo(tLCPolSchema.getGrpPolNo());
			tFieldCarrier.setEndDate(tLCPolSchema.getEndDate());
			// System.out.println("保单类型为："+mLCPolBL.getPolTypeFlag());
			tFieldCarrier.setPolTypeFlag(tLCPolSchema.getPolTypeFlag());
			tFieldCarrier.setPrem(tLCPolSchema.getPrem());

			tFieldCarrier.setSupplementaryPrem(tLCPolSchema
					.getSupplementaryPrem());

			if (tLCPolSchema.getStandbyFlag1() != null) {
				tFieldCarrier.setStandbyFlag1(tLCPolSchema.getStandbyFlag1());
			}
			if (tLCPolSchema.getStandbyFlag2() != null) {
				tFieldCarrier.setStandbyFlag2(tLCPolSchema.getStandbyFlag2());
			}
			if (tLCPolSchema.getStandbyFlag3() != null) {
				tFieldCarrier.setStandbyFlag3(tLCPolSchema.getStandbyFlag3());
			}

			// Added By Liuyp At 2009-12-24 为信保通借款人校验规则而加
			tFieldCarrier.setCardFlag(this.mLCContSchema.getCardFlag());

			tVData.add(tFieldCarrier);

			LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
			tLMCheckFieldSchema.setRiskCode(RiskCode);

			tLMCheckFieldSchema.setFieldName("TB" + operType); // 投保
			tVData.add(tLMCheckFieldSchema);
			if (tCheckFieldCom.CheckField(tVData) == false) {
				this.mErrors.copyAllErrors(tCheckFieldCom.mErrors);
				return false;
			} else {
				LMCheckFieldSet mLMCheckFieldSet = tCheckFieldCom
						.GetCheckFieldSet();
				for (int n = 1; n <= mLMCheckFieldSet.size(); n++) {
					LMCheckFieldSchema tField = mLMCheckFieldSet.get(n);
					if ((tField.getReturnValiFlag() != null)
							&& tField.getReturnValiFlag().equals("N")) {
						if ((tField.getMsgFlag() != null)
								&& tField.getMsgFlag().equals("Y")) {
							MsgFlag = true;
							strMsg = strMsg + tField.getMsg() + " ; ";

							break;
						}
					}
				}
				if (MsgFlag == true) {
					// @@错误处理
					String str = "数据有误：" + strMsg;
					buildError("CheckTBField", str);
					log.debug(str);
					return false;
				}
			}
		} catch (Exception ex) {
			// @@错误处理
			String str = "异常错误!" + ex.getMessage();
			buildError("CheckTBField", str);
			log.debug(str, ex);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * 
	 */
	private boolean checkInsuredAge(LCPolSchema tLCPolSchema) {
		int insuredAge = tLCPolSchema.getInsuredAppAge();
		String sql = "select count(1) from LMRiskApp where RiskCode = '"
				+ tLCPolSchema.getRiskCode()
				+ "' and (MinInsuredAge is null or MinInsuredAge <= "
				+ insuredAge
				+ ") and (MaxInsuredAge is null or MaxInsuredAge >= "
				+ insuredAge + ")";
		String count = new ExeSQL().getOneValue(sql);
		if (count != null && count.equals("1")) {
			return true;
		} else {
			buildError("checkInsuredAge", "被保人年龄不在允许范围内！");
			return false;
		}
	}

	/**
	 * 处理万能首期追加保费。
	 * 
	 * @param cLCPolSchema
	 * @return
	 */
	private boolean dealSupplementaryPrem(LCPolSchema cLCPolSchema) {
		String tPolRiskCode = cLCPolSchema.getRiskCode();

		if (tPolRiskCode == null || tPolRiskCode.equals("")) {
			buildError("dealSupplementaryPrem", "险种代码出现空值！");
			return false;
		}

		// 处理标志。原则上同一个万能险种中，只能有一个首期追加保费责任。
		boolean tBDealFlag = false;

		for (int i = 1; i <= this.mLCRiskDutyWrapSet.size(); i++) {
			LCRiskDutyWrapSchema tRiskDutyParams = null;
			tRiskDutyParams = mLCRiskDutyWrapSet.get(i);

			String tRiskCode = tRiskDutyParams.getRiskCode();

			if (tPolRiskCode.equals(tRiskCode)) {
				String tCalFactor = tRiskDutyParams.getCalFactor();

				if ("SupplementaryPrem".equals(tCalFactor)) {
					if (tBDealFlag) {
						buildError("dealSupplementaryPrem",
								"首期追加保费要素[SupplementaryPrem]出现多次！");
						return false;
					}

					tBDealFlag = true;

					String tCalFactorValue = tRiskDutyParams
							.getCalFactorValue();

					cLCPolSchema.setSupplementaryPrem(tCalFactorValue);
				}
			}

		}

		return true;
	}

	/**
	 * 处理万能初始扣费。
	 * 
	 * @param cLCPolSchema
	 * @return
	 */
	private boolean dealInitFeeRate(LCPolSchema cLCPolSchema) {
		String tPolRiskCode = cLCPolSchema.getRiskCode();

		if (tPolRiskCode == null || tPolRiskCode.equals("")) {
			buildError("dealInitFeeRate", "险种代码出现空值！");
			return false;
		}

		// 处理标志。原则上同一个万能险种中，只能有一个初始扣费要素责任。
		boolean tBDealFlag = false;

		for (int i = 1; i <= this.mLCRiskDutyWrapSet.size(); i++) {
			LCRiskDutyWrapSchema tRiskDutyParams = null;
			tRiskDutyParams = mLCRiskDutyWrapSet.get(i);

			String tRiskCode = tRiskDutyParams.getRiskCode();

			if (tPolRiskCode.equals(tRiskCode)) {
				String tCalFactor = tRiskDutyParams.getCalFactor();

				if ("InitFeeRate".equals(tCalFactor)) {
					if (tBDealFlag) {
						buildError("dealInitFeeRate",
								"万能初始扣费要素[InitFeeRate]出现多次！");
						return false;
					}

					tBDealFlag = true;

					String tCalFactorValue = tRiskDutyParams
							.getCalFactorValue();

					cLCPolSchema.setInitFeeRate(tCalFactorValue);
				}
			}

		}

		return true;
	}

	private boolean dealCustomerImpartInfo() {
		// 处理告知信息
		if (mCusImpartSet != null && mCusImpartSet.size() > 0) {
			// 设置所有告知信息得客户号码
			for (int i = 1; i <= mCusImpartSet.size(); i++) {
				mCusImpartSet.get(i).setContNo(mLCContSchema.getContNo());
				mCusImpartSet.get(i).setGrpContNo(mLCContSchema.getGrpContNo());
				mCusImpartSet.get(i).setPrtNo(mLCContSchema.getPrtNo());
				mCusImpartSet.get(i).setProposalContNo(
						mLCContSchema.getProposalContNo());
				mCusImpartSet.get(i).setCustomerNo(
						mLCInsuredSchema.getInsuredNo());
			}

			VData tempVData = new VData();
			tempVData.add(mCusImpartSet);
			tempVData.add(mGlobalInput);

			CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
			mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
			if (mCustomerImpartBL.mErrors.needDealError()) {

				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "dealData";
				tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError()
						.toString();
				this.mErrors.addOneError(tError);
				return false;
			}

			tempVData.clear();
			tempVData = mCustomerImpartBL.getResult();
			if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
					"LCCustomerImpartSet", 0)) {
				mCusImpartSet = (LCCustomerImpartSet) tempVData
						.getObjectByObjectName("LCCustomerImpartSet", 0);
				System.out.println("告知条数" + mCusImpartSet.size());
			} else {
				System.out.println("告知条数为空");
			}

			if (null != (LCCustomerImpartParamsSet) tempVData
					.getObjectByObjectName("LCCustomerImpartParamsSet", 0)) {
				mCusImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
						.getObjectByObjectName("LCCustomerImpartParamsSet", 0);
			}
		}

		// 处理告知明细信息
		if (mCusImpartDetailSet != null && mCusImpartDetailSet.size() > 0) {
			// 设置所有告知明细信息得客户号码
			for (int i = 1; i <= mCusImpartDetailSet.size(); i++) {
				if (mCusImpartDetailSet.get(i).getImpartVer().trim().equals("")
						|| mCusImpartDetailSet.get(i).getImpartCode().trim()
								.equals("")
						|| mCusImpartDetailSet.get(i).getImpartDetailContent()
								.trim().equals("")
						|| mCusImpartDetailSet.get(i).getDiseaseContent()
								.trim().equals("")) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "dealData";
					tError.errorMessage = "健康状况告知中的告知版本，告知编码，告知内容，疾病内容录入不能为空，请检查！";
					this.mErrors.addOneError(tError);
					return false;
				}

				// 增加对同一告知不同告知内容的存储
				for (int count = 1; count <= mCusImpartDetailSet.size(); count++) {
					if (StrTool
							.cTrim(mCusImpartDetailSet.get(i).getImpartVer())
							.equals(
									mCusImpartDetailSet.get(count)
											.getImpartVer())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i).getImpartCode())
									.equals(
											mCusImpartDetailSet.get(count)
													.getImpartCode())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i)
											.getDiseaseContent()).equals(
									mCusImpartDetailSet.get(count)
											.getDiseaseContent())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i).getStartDate())
									.equals(
											mCusImpartDetailSet.get(count)
													.getStartDate())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i).getEndDate())
									.equals(
											mCusImpartDetailSet.get(count)
													.getEndDate())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i).getProver())
									.equals(
											mCusImpartDetailSet.get(count)
													.getProver())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i)
											.getCurrCondition()).equals(
									mCusImpartDetailSet.get(count)
											.getCurrCondition())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i).getIsProved())
									.equals(
											mCusImpartDetailSet.get(count)
													.getIsProved())
							& StrTool.cTrim(
									mCusImpartDetailSet.get(i)
											.getImpartDetailContent()).equals(
									mCusImpartDetailSet.get(count)
											.getImpartDetailContent())
							& count != i) {
						// @@错误处理
						CError tError = new CError();
						tError.moduleName = "ContInsuredBL";
						tError.functionName = "dealData";
						tError.errorMessage = "健康状况告知中的告知版本，告知编码，告知内容，疾病内容录入重复，请检查！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
				mCusImpartDetailSet.get(i).setSubSerialNo(String.valueOf(i));
				mCusImpartDetailSet.get(i).setContNo(mLCContSchema.getContNo());
				mCusImpartDetailSet.get(i).setGrpContNo(
						mLCContSchema.getGrpContNo());
				mCusImpartDetailSet.get(i).setPrtNo(mLCContSchema.getPrtNo());
				mCusImpartDetailSet.get(i).setProposalContNo(
						mLCContSchema.getProposalContNo());
				mCusImpartDetailSet.get(i).setCustomerNo(
						mLCInsuredSchema.getInsuredNo());
				mCusImpartDetailSet.get(i).setOperator(mGlobalInput.Operator);
				mCusImpartDetailSet.get(i).setMakeDate(mCurrentData);
				mCusImpartDetailSet.get(i).setMakeTime(mCurrentTime);
				mCusImpartDetailSet.get(i).setModifyDate(mCurrentData);
				mCusImpartDetailSet.get(i).setModifyTime(mCurrentTime);
			}
		}

		return true;
	}

}
