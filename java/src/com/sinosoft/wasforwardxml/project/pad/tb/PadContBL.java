/**
 * 保单信息录入！
 */

package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;









import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAccountSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.tb.ContBL;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.midplat.common.DateUtil;
import com.sinosoft.midplat.common.XmlTag;
import com.sinosoft.midplat.exception.MidplatException;
import com.sinosoft.midplat.kernel.util.YbtSufUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.LCAppntTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCInsuredTable;
import com.sinosoft.wasforwardxml.project.pad.obj.SupplementTable;

public class PadContBL implements XmlTag {
	private final static Logger cLogger = Logger.getLogger(PadContBL.class);
	
	private final LCContTable cLCContTable;
	private final LCAppntTable cLCAppntTable;
	private final GlobalInput cGlobalInput;
	private final List cLCInsuredList;
	public String mcardflag;
	private String cError="";
	public SupplementTable supplementTable;
	
	public PadContBL(SupplementTable supplementTable,GlobalInput pGlobalInput,LCContTable contTable, LCAppntTable appntTable,List tLCInsuredList) {
		this.supplementTable=supplementTable;
		cLCContTable = contTable;
		cLCAppntTable = appntTable;
		cGlobalInput = pGlobalInput;
		cLCInsuredList = tLCInsuredList;
	}
	
	public String deal() throws Exception {
		cLogger.info("Into WxContBL.deal()...");
		
		//校验数据
		cError = checkContBL();
		
		if("".equals(cError)){
//			录入保单合同信息
			VData mContBLData = getContBLVData();
			ContBL mContBL = new ContBL();
			cLogger.info("Start call ContBL.submitData()...");
			long mStartMillis = System.currentTimeMillis();
			if (!mContBL.submitData(mContBLData, "INSERT||CONT")) {
				return mContBL.mErrors.getFirstError();
			}
			//pad简易开始必须保存printtype字段 增加lccontsub表的存储
			if(!lccontsubSave()){
				return "保存数据库的时候失败！-->PadContBL.lccontsubSave();";
			}
			
			cLogger.info("ContBL耗时：" + (System.currentTimeMillis()-mStartMillis)/1000.0 + "s"
					+ "；报文类型：WX0002" );
			cLogger.info("End call ContBL.submitData()!");
		}
		cLogger.info("Out WxContBL.deal()!");
		return cError;
	}
	
	private String checkContBL() throws MidplatException {
		cLogger.info("Into WxContBL.checkContBL()...");
		
		//校验投保单号
		LCContDB mLCContDB = new LCContDB();
		mLCContDB.setPrtNo(cLCContTable.getPrtNo());
		
		
		LCContSet mLCContSet = mLCContDB.query();
		if (mLCContSet.size() > 0) {
			return "该投保单印刷号已使用，请更换！";
		}
		
		String mSQL = "select 1 from LDBank where 1=1 "
					+ "and BankCode = '" + cLCContTable.getBankCode() + "' "
					+ "and ComCode='"+ cGlobalInput.ComCode + "' "
					+ "and CansendFlag='1' "
					+ "with ur";
		SSRS mSSRS = new ExeSQL().execSQL(mSQL);
		if(mSSRS == null || mSSRS.getMaxRow() < 0) {
			return "银行编码"+cLCContTable.getBankCode()+"不存在，请更换！";
		}
			
		cLogger.info("Out WXContBL.checkContBL()!");
		return "";
	}
	
	private VData getContBLVData() throws MidplatException {
		cLogger.info("Into WXContBL.getContBLVData()...");

		//保单信息
		LCContSchema mLCContSchema = getLCContSchema();

		//投保人
		LCAppntSchema mLCAppntSchema = new LCAppntSchema();
		mLCAppntSchema.setAppntNo("");	//注意此处必须设置！picch核心判断AppntNo为""时才进行投保人五要素判断，AppntNo为null时会直接生成新客户号
		mLCAppntSchema.setPrtNo(cLCContTable.getPrtNo());
		mLCAppntSchema.setAppntName(cLCAppntTable.getAppntName());
		mLCAppntSchema.setAppntSex(cLCAppntTable.getAppntSex());
		mLCAppntSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		mLCAppntSchema.setIDType(cLCAppntTable.getAppntIDType());
		mLCAppntSchema.setIDNo(cLCAppntTable.getAppntIDNo());
		mLCAppntSchema.setOccupationCode(cLCAppntTable.getOccupationCode());
		mLCAppntSchema.setOccupationType(cLCAppntTable.getOccupationType());
		mLCAppntSchema.setBankCode(mLCContSchema.getBankCode());
		mLCAppntSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLCAppntSchema.setAccName(mLCContSchema.getAccName());
		mLCAppntSchema.setNativeCity(supplementTable.getAppntNativeCity());
		mLCAppntSchema.setNativePlace(supplementTable.getAppntNativePlace());
		mLCAppntSchema.setPosition(supplementTable.getAppntPosition());
		mLCAppntSchema.setSalary(supplementTable.getAppntSalary());
		mLCAppntSchema.setMarriage(supplementTable.getAppntMarriage());
		mLCAppntSchema.setIDEndDate(supplementTable.getAppntIDEndDate());
		mLCAppntSchema.setIDStartDate(supplementTable.getAppntIDStartDate());
		mLCAppntSchema.setAuthorization(cLCAppntTable.getAuthorization());//授权标记

		//投保人地址
		LCAddressSchema mAppntAddress = new LCAddressSchema();
		mAppntAddress.setCompanyPhone("");
		mAppntAddress.setMobile(cLCAppntTable.getAppntMobile());
		mAppntAddress.setPhone(cLCAppntTable.getAppntPhone());
		mAppntAddress.setHomePhone(cLCAppntTable.getAppntPhone());
		mAppntAddress.setPostalAddress(cLCAppntTable.getMailAddress());
		mAppntAddress.setZipCode(cLCAppntTable.getMailZipCode());
		mAppntAddress.setHomeAddress(cLCAppntTable.getHomeAddress());
		mAppntAddress.setHomeZipCode(cLCAppntTable.getMailZipCode());
		mAppntAddress.setEMail(cLCAppntTable.getEmail());
		mAppntAddress.setPostalCity(cLCAppntTable.getAppntPostalCity());
		mAppntAddress.setPostalCommunity(cLCAppntTable
				.getAppntPostalCommunity());
		mAppntAddress.setPostalCounty(cLCAppntTable.getAppntPostalCounty());
		mAppntAddress.setPostalProvince(cLCAppntTable
				.getAppntPostalProvince());
		mAppntAddress.setPostalStreet(cLCAppntTable.getAppntPostalStreet());
		mAppntAddress.setHomeCode(cLCAppntTable.getAppntHomeCode());
		mAppntAddress.setHomeNumber(cLCAppntTable.getAppntHomePhone());
		mAppntAddress.setGrpName(supplementTable.getAppntGrpName());

		//投保帐户
		LCAccountSchema mLCAccountSchema = new LCAccountSchema();
		mLCAccountSchema.setBankCode(mLCContSchema.getBankCode());
		mLCAccountSchema.setBankAccNo(mLCContSchema.getBankAccNo());
		mLCAccountSchema.setAccName(mLCContSchema.getAccName());
		mLCAccountSchema.setAccKind("Y");

		//投保人个人信息
		LDPersonSchema mAppntPerson = new LDPersonSchema();
		mAppntPerson.setName(mLCAppntSchema.getAppntName());
		mAppntPerson.setSex(mLCAppntSchema.getAppntSex());
		mAppntPerson.setIDNo(mLCAppntSchema.getIDNo());
		mAppntPerson.setIDType(mLCAppntSchema.getIDType());
		mAppntPerson.setBirthday(mLCAppntSchema.getAppntBirthday());
		mAppntPerson.setOccupationCode(mLCAppntSchema.getOccupationCode());
		mAppntPerson.setOccupationType(mLCAppntSchema.getOccupationType()); 
		mAppntPerson.setNativeCity(supplementTable.getAppntNativeCity());
		mAppntPerson.setNativePlace(supplementTable.getAppntNativePlace());
		mAppntPerson.setPosition(supplementTable.getAppntPosition());
		mAppntPerson.setSalary(supplementTable.getAppntSalary());
		mAppntPerson.setMarriage(supplementTable.getAppntMarriage());
		mAppntPerson.setAuthorization(cLCAppntTable.getAuthorization());
		
		
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("GrpNo", "");
		mTransferData.setNameAndValue("GrpName", "");
		mTransferData.setNameAndValue("Authorization", cLCAppntTable.getAuthorization());//授权标记
		System.out.println("managecom"+mLCContSchema.getManageCom());
		LCContSubSchema lcContSubSchema=new LCContSubSchema();
		lcContSubSchema.setPrtNo(cLCContTable.getPrtNo());
		lcContSubSchema.setPrintType(supplementTable.getPrintType());
		lcContSubSchema.setOperator(cGlobalInput.Operator);
		lcContSubSchema.setMakeDate(PubFun.getCurrentDate());
		lcContSubSchema.setMakeTime(PubFun.getCurrentTime());
		lcContSubSchema.setModifyDate(PubFun.getCurrentDate());
		lcContSubSchema.setModifyTime(PubFun.getCurrentTime());
		lcContSubSchema.setAgreedPayFlag(cLCContTable.getAgreedPayFlag());
		lcContSubSchema.setAgreedPayDate(cLCContTable.getAgreedPayDate());
		VData mVData = new VData();
		mVData.add(cGlobalInput);
		mVData.add(mLCContSchema);
		mVData.add(mLCAppntSchema);
		mVData.add(mAppntAddress);
		mVData.add(mLCAccountSchema);
//		mVData.add(mLCCustomerImpartSet);
		mVData.add(mAppntPerson);
		mVData.add(mTransferData);
		mVData.add(lcContSubSchema);

		cLogger.info("Out WXContBL.getContBLVData()!");
		return mVData;
	}
	
	private LCContSchema getLCContSchema() throws MidplatException {
		cLogger.info("Into WXContBL.getLCContSchema()...");
		
		String mCurrentDate = DateUtil.getCurDate("yyyy-MM-dd");
		
		LCContSchema mLCContSchema = new LCContSchema();
		mLCContSchema.setGrpContNo("00000000000000000000");
		mLCContSchema.setContNo(cLCContTable.getContNo());	//用保单合同印刷号录单
		mLCContSchema.setProposalContNo(cLCContTable.getContNo());
		mLCContSchema.setPrtNo(cLCContTable.getPrtNo());
		mLCContSchema.setContType("1");	//保单类别(个单-1，团单-2)
		mLCContSchema.setPolType("0");
		mLCContSchema.setCardFlag(mcardflag);	//CardFlag"b"，网销复杂产品！
		mLCContSchema.setManageCom(cGlobalInput.ManageCom);
		mLCContSchema.setAgentCom(cGlobalInput.AgentCom);
		mLCContSchema.setAgentCode(cLCContTable.getAgentCode());
		mLCContSchema.setAgentGroup("");
		mLCContSchema.setSaleChnl(cLCContTable.getSaleChnl());	//渠道"04"，CardFlag"9"，共同识别银保通交易！
		mLCContSchema.setPassword("");	//保单密码
		mLCContSchema.setInputOperator(cGlobalInput.Operator);
		mLCContSchema.setInputDate(PubFun.getCurrentDate());
		mLCContSchema.setInputTime(PubFun.getCurrentTime());
		mLCContSchema.setCrs_SaleChnl(cLCContTable.getCrs_SaleChnl());
		mLCContSchema.setCrs_BussType(cLCContTable.getCrs_BussType());
		mLCContSchema.setGrpAgentCom(cLCContTable.getGrpAgentCom());
		mLCContSchema.setGrpAgentCode(cLCContTable.getGrpAgentCode());
		mLCContSchema.setGrpAgentName(cLCContTable.getGrpAgentName());
		mLCContSchema.setGrpAgentIDNo(cLCContTable.getGrpAgentIDNo());
		//投保人
		mLCContSchema.setAppntName(cLCAppntTable.getAppntName());
		mLCContSchema.setAppntSex(cLCAppntTable.getAppntSex());
		mLCContSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		mLCContSchema.setAppntIDType(cLCAppntTable.getAppntIDType());
		mLCContSchema.setAppntIDNo(cLCAppntTable.getAppntIDNo());
		//被保人
		for(int i=0;i<cLCInsuredList.size();i++){
			LCInsuredTable tLCInsuredTable = (LCInsuredTable)cLCInsuredList.get(i);
			if("00".equals(tLCInsuredTable.getRelaToMain())){
				mLCContSchema.setInsuredName(tLCInsuredTable.getName());
				mLCContSchema.setInsuredSex(tLCInsuredTable.getSex());
				mLCContSchema.setInsuredBirthday(tLCInsuredTable.getBirthday());
				mLCContSchema.setInsuredIDType(tLCInsuredTable.getIDType());
				mLCContSchema.setInsuredIDNo(tLCInsuredTable.getIDNo());
			}
			
		}
		mLCContSchema.setPayIntv(cLCContTable.getPayIntv());	//缴费方式
		mLCContSchema.setPayMode("4");
		mLCContSchema.setPayLocation("0");	//银行转帐
		
		mLCContSchema.setBankAccNo(cLCContTable.getBankAccNo());
		mLCContSchema.setBankCode(cLCContTable.getBankCode());
		mLCContSchema.setAccName(cLCContTable.getAccName());
		mLCContSchema.setPrintCount(0);
		GregorianCalendar mNowCalendar = new GregorianCalendar();
		mNowCalendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
		mLCContSchema.setCValiDate(cLCContTable.getCValiDate());	//保单生效日期(取自电子商务)
		mLCContSchema.setPolApplyDate(mCurrentDate);	//投保日期
		mLCContSchema.setProposalType("01");	//投保书类型：01-普通个单投保书
		mLCContSchema.setPremScope(supplementTable.getPremScope());
		mLCContSchema.setDueFeeMsgFlag(supplementTable.getDueFeeMsgFlag());
		mLCContSchema.setAgentSaleCode(cLCContTable.getAgentSaleCode());
		cLogger.info("Out YbtContBL.getLCContSchema()!");
		return mLCContSchema;
	}
	
	private boolean lccontsubSave() {
		SSRS mSSRS = new ExeSQL().execSQL("select 1 from lccontsub where prtno='"+cLCContTable.getPrtNo()+"' with ur");
		if(mSSRS == null || mSSRS.getMaxRow() <= 0) {
			System.out.println("开始单独添加lccontsub表数据");
			LCContSubSchema lcContSubSchema=new LCContSubSchema();
			lcContSubSchema.setPrtNo(cLCContTable.getPrtNo());
			lcContSubSchema.setPrintType(supplementTable.getPrintType());
			lcContSubSchema.setOperator(cGlobalInput.Operator);
			lcContSubSchema.setManageCom(cGlobalInput.ManageCom);
			lcContSubSchema.setMakeDate(PubFun.getCurrentDate());
			lcContSubSchema.setMakeTime(PubFun.getCurrentTime());
			lcContSubSchema.setModifyDate(PubFun.getCurrentDate());
			lcContSubSchema.setModifyTime(PubFun.getCurrentTime());
			lcContSubSchema.setAgreedPayFlag(cLCContTable.getAgreedPayFlag());
			lcContSubSchema.setAgreedPayDate(cLCContTable.getAgreedPayDate());
			
			MMap subMap= new MMap();
			subMap.put(lcContSubSchema, "INSERT");
			PubSubmit pubSubmit = new PubSubmit();
			VData sData = new VData();
			sData.add(subMap);
			boolean tr = pubSubmit.submitData(sData, "");
			
			if (!tr) {
				System.out.println("保存数据库的时候失败！-->PadContBL.lccontsubSave();");
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		
//		String mInFile = "D:/request/ICBC_std/UW.xml";
//		
//		FileInputStream mFis = new FileInputStream(mInFile);
//		InputStreamReader mIsr = new InputStreamReader(mFis, "GBK");
//		Document mXmlDoc = new SAXBuilder().build(mIsr);
//		
//		Element mTranData = mXmlDoc.getRootElement();
//		Element mBaseInfo = mTranData.getChild("BaseInfo");
//		Element mLCCont = mTranData.getChild("LCCont");
		
		//换号。picch特殊，PrtNo和ProposalContNo需要调换
//		String mPrtNo = mLCCont.getChildTextTrim("PrtNo");
//		String mProposalNo = mLCCont.getChildTextTrim("ProposalContNo");
//		mLCCont.getChild("PrtNo").setText(mProposalNo);
//		mLCCont.getChild("ProposalContNo").setText(mPrtNo);
//		
//		GlobalInput mGlobalInput = YbtSufUtil.getGlobalInput(
//				mBaseInfo.getChildText("BankCode"),
//				mBaseInfo.getChildText("ZoneNo"),
//				mBaseInfo.getChildText("BrNo"));
//		
//		new WxContBL(mXmlDoc, mGlobalInput).deal();
		
		System.out.println("成功结束！");
	}
}
