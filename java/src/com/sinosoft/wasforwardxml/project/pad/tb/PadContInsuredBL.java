package com.sinosoft.wasforwardxml.project.pad.tb;

/**
 * 录入被保人和险种信息！
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.cbsws.xml.ctrl.complexpds.WxContInsuredIntlBL;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.*;
import com.sinosoft.lis.cbcheck.UWSendPrintUIS;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LDWrapDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.intltb.ContInsuredIntlBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDWrapSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDiseaseResultSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import examples.newsgroups;

public class PadContInsuredBL {
	private final static Logger cLogger = Logger
			.getLogger(PadContInsuredBL.class);

	private MsgHead cMsgHead;

	private LCContTable cLCContTable;// 保单信息

	private LCAppntTable cLCAppntTable;// 投保人
	public List cSupplementTable;
	public List cWrapParamList;// 套餐参数问题
	private List cLCInsuredList;// 被保人
	private List cDutyCodeList;
	private WrapTable cWrapTable;// 套餐
	public List cImpartTable;
	private List cLCBnfList;// 受益人

	private String Errors = "";// 错误信息
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

	private LCContSchema cLCContSchema = null;

	private LCAppntSchema cLCAppntSchema = null;

	private LCAddressSchema cLCAppntAddress = null;
	private SupplementTable supplementTable=null;
	

	private String mMsgType = "";

	public double mPrem = 0;

	public double mAmnt = 0;

	public LCDutySet cLCDutySet = new LCDutySet();

	public LCPolSet cLCPolSet = new LCPolSet();

	public GlobalInput cGlobalInput = new GlobalInput();

	private VData mVData = new VData();

	private TransferData mTransferData = new TransferData();

	public LCRiskDutyWrapSet cLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
	public LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
	private String mcardflag="";

	public PadContInsuredBL(GlobalInput mGlobalInput, MsgHead tMsgHead,
			LCContTable tLCContTable, LCAppntTable tLAppntTable,
			List tLCInsuredList, WrapTable tWrapTable, List tLCBnfList) {
		cMsgHead = tMsgHead;
		cLCContTable = tLCContTable;
		cLCAppntTable = tLAppntTable;
		cLCInsuredList = tLCInsuredList;
		cWrapTable = tWrapTable;
		cLCBnfList = tLCBnfList;
		cGlobalInput = mGlobalInput;
	}
	
	public PadContInsuredBL(MsgHead cMsgHead, LCContTable cLCContTable,
			LCAppntTable cLCAppntTable, List cSupplementTable,
			List cWrapParamList, List cLCInsuredList, WrapTable cWrapTable,
			List cLCBnfList, GlobalInput cGlobalInput,List cImpartTable) {
		
		this.cMsgHead = cMsgHead;
		this.cLCContTable = cLCContTable;
		this.cLCAppntTable = cLCAppntTable;
		this.cSupplementTable = cSupplementTable;
		this.cWrapParamList = cWrapParamList;
		this.cLCInsuredList = cLCInsuredList;
		this.cWrapTable = cWrapTable;
		this.cLCBnfList = cLCBnfList;
		this.cGlobalInput = cGlobalInput;
		this.cImpartTable=cImpartTable;
		supplementTable=(SupplementTable) cSupplementTable.get(0);
	}

	public String deal() {
		cLogger.info("Into PadContInsuredIntlBL.deal()...");

		mMsgType = cMsgHead.getMsgType();
		
		System.out.println(cLCContTable.getContNo());
		String mRiskWrap = cWrapTable.getRiskWrapCode();
		LDWrapDB tLDWrapDB = new LDWrapDB();
		tLDWrapDB.setRiskWrapCode(mRiskWrap);
		LDWrapSchema tLDWrapSchema = tLDWrapDB.query().get(1);
		
		mcardflag=tLDWrapSchema.getWrapType();
		PadContBL ttWXContBL = new PadContBL(supplementTable,cGlobalInput,cLCContTable, cLCAppntTable,cLCInsuredList);
		ttWXContBL.mcardflag=mcardflag;
		try {
			Errors = ttWXContBL.deal();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!getContinfo()) {
			Errors = "获取保单信息失败";
			return Errors;
		}
		System.out.println(444);
		getDutyCodeList();
		System.out.println(444);
		for (int i = 0; i < cLCInsuredList.size(); i++) {
			LCInsuredTable tLCInsuredTable = (LCInsuredTable) cLCInsuredList
					.get(i);

			checkContInsuredIntlBL(tLCInsuredTable);

			cLogger.info("Start call ContInsuredIntlBL.submitData()...");
			long tStartMillism = System.currentTimeMillis();

			VData tContInsuredIntlBLVData = getContInsuredIntlBLVData(tLCInsuredTable);
			if (tContInsuredIntlBLVData == null) {
				return Errors;
			}
			
			
			
			if(!"".equals(Errors)){
				
                return Errors;
			}
			PadContInsuredIntlBL ttWxContInsuredIntlBL = new PadContInsuredIntlBL(tLCCustomerImpartSet,supplementTable,cGlobalInput,cMsgHead,cLCContTable,cLCAppntTable,cLCInsuredList,cWrapTable,cLCBnfList,cWrapParamList);
			Errors = ttWxContInsuredIntlBL.deal();
			if(!"".equals(Errors)){
				
                return Errors;
			}
			
			tContInsuredIntlBLVData.clear();
			tContInsuredIntlBLVData= getContInsuredIntlBLVData();
			PadContInputBL bl = new PadContInputBL();
			if (!bl.submitData(tContInsuredIntlBLVData, "INSERT||MAIN")) {
				return bl.mErrors.getFirstError();
			}
			
			//生成打印管理表数据
			if(!SendPayInfo()){
				System.out.println("生成打印管理表数据失败！");
//				return Errors;
			}

			cLogger.info("ContInsuredIntlBL耗时："
					+ (System.currentTimeMillis() - tStartMillism) / 1000.0
					+ "s" + "；报文类型：" + mMsgType);
			cLogger.info("End call ContInsuredIntlBL.submitData()!");
		}

		cLogger.info("Out PadContInsuredIntlBL.deal()!");
		return Errors;
	}

	private boolean getContinfo() {

		cLCContSchema = new LCContSchema();
		cLCContSchema.setGrpContNo("00000000000000000000");
		cLCContSchema.setContNo(cLCContTable.getContNo()); // 用保单合同印刷号录单
		cLCContSchema.setProposalContNo(cLCContTable.getContNo());
		cLCContSchema.setPrtNo(cLCContTable.getPrtNo());
		cLCContSchema.setContType("1"); // 保单类别(个单-1，团单-2)
		cLCContSchema.setPolType("0");
		cLCContSchema.setFamilyType("P");// FamilyType "P"，pad产品！
		cLCContSchema.setCardFlag(mcardflag);
		
		cLCContSchema.setManageCom(cGlobalInput.ManageCom);
		cLCContSchema.setAgentCom(cGlobalInput.AgentCom);
		cLCContSchema.setAgentCode(cLCContTable.getAgentCode());
		cLCContSchema.setAgentGroup("");
		cLCContSchema.setSaleChnl(cLCContTable.getSaleChnl());
		cLCContSchema.setPassword(""); // 保单密码
		cLCContSchema.setInputOperator(cGlobalInput.Operator);
		cLCContSchema.setInputDate(PubFun.getCurrentDate());
		cLCContSchema.setInputTime(PubFun.getCurrentTime());
		// 投保人
		cLCContSchema.setAppntName(cLCAppntTable.getAppntName());
		cLCContSchema.setAppntSex(cLCAppntTable.getAppntSex());
		cLCContSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		cLCContSchema.setAppntIDType(cLCAppntTable.getAppntIDType());
		cLCContSchema.setAppntIDNo(cLCAppntTable.getAppntIDNo());
		// 被保人
		for (int i = 0; i < cLCInsuredList.size(); i++) {
			LCInsuredTable tLCInsuredTable = (LCInsuredTable) cLCInsuredList
					.get(i);
			if ("00".equals(tLCInsuredTable.getRelaToMain())) {
				cLCContSchema.setInsuredName(tLCInsuredTable.getName());
				cLCContSchema.setInsuredSex(tLCInsuredTable.getSex());
				cLCContSchema.setInsuredBirthday(tLCInsuredTable.getBirthday());
				cLCContSchema.setInsuredIDType(tLCInsuredTable.getIDType());
				cLCContSchema.setInsuredIDNo(tLCInsuredTable.getIDNo());
			}

		}
		cLCContSchema.setPayIntv(cLCContTable.getPayIntv()); // 缴费方式
		cLCContSchema.setPayMode("4");
		cLCContSchema.setPayLocation("0"); // 银行转帐
		cLCContSchema.setBankCode(cLCContTable.getBankCode());
		cLCContSchema.setBankAccNo(cLCContTable.getBankAccNo()); // 投保帐户号
		cLCContSchema.setAccName(cLCContTable.getAccName()); // 投保帐户姓名
		cLCContSchema.setPrintCount(0);
		cLCContSchema.setCValiDate(cLCContTable.getCValiDate()); // 保单生效日期
		cLCContSchema.setPolApplyDate(cLCContTable.getPolApplyDate()); // 投保日期
		cLCContSchema.setProposalType("01"); // 投保书类型：01-普通个单投保书

		// 投保人
		cLCAppntSchema = new LCAppntSchema();
		cLCAppntSchema.setAppntNo(""); // 注意此处必须设置！picch核心判断AppntNo为""时才进行投保人五要素判断
		cLCAppntSchema.setPrtNo(cLCContTable.getPrtNo());
		cLCAppntSchema.setAppntName(cLCAppntTable.getAppntName());
		cLCAppntSchema.setAppntSex(cLCAppntTable.getAppntSex());
		cLCAppntSchema.setAppntBirthday(cLCAppntTable.getAppntBirthday());
		cLCAppntSchema.setIDType(cLCAppntTable.getAppntIDType());
		cLCAppntSchema.setIDNo(cLCAppntTable.getAppntIDNo());
		cLCAppntSchema.setOccupationCode(cLCAppntTable.getOccupationCode());
		cLCAppntSchema.setIDEndDate(cLCAppntTable.getIDEndDate());
		cLCAppntSchema.setIDStartDate(cLCAppntTable.getIDStartDate());
		cLCAppntSchema.setNativeCity(supplementTable.getAppntNativeCity());
		cLCAppntSchema.setNativePlace(supplementTable.getAppntNativePlace());
		cLCAppntSchema.setPosition(supplementTable.getAppntPosition());
		cLCAppntSchema.setSalary(supplementTable.getAppntSalary());
		cLCAppntSchema.setMarriage(supplementTable.getAppntMarriage());
		
		// 投保人地址
		cLCAppntAddress = new LCAddressSchema();
		cLCAppntAddress.setCompanyPhone("");
		cLCAppntAddress.setMobile(cLCAppntTable.getAppntMobile());
		cLCAppntAddress.setPhone(cLCAppntTable.getAppntPhone());
		cLCAppntAddress.setHomePhone(cLCAppntTable.getAppntPhone());
		cLCAppntAddress.setPostalAddress(cLCAppntTable.getMailAddress());
		cLCAppntAddress.setZipCode(cLCAppntTable.getMailZipCode());
		cLCAppntAddress.setHomeAddress(cLCAppntTable.getHomeAddress());
		cLCAppntAddress.setHomeZipCode(cLCAppntTable.getMailZipCode());
		cLCAppntAddress.setEMail(cLCAppntTable.getEmail());
		cLCAppntAddress.setPostalCity(cLCAppntTable.getAppntPostalCity());
		cLCAppntAddress.setPostalCommunity(cLCAppntTable
				.getAppntPostalCommunity());
		cLCAppntAddress.setPostalCounty(cLCAppntTable.getAppntPostalCounty());
		cLCAppntAddress.setPostalProvince(cLCAppntTable
				.getAppntPostalProvince());
		cLCAppntAddress.setPostalStreet(cLCAppntTable.getAppntPostalStreet());
		cLCAppntAddress.setHomeCode(cLCAppntTable.getAppntHomeCode());
		cLCAppntAddress.setHomeNumber(cLCAppntTable.getAppntHomePhone());

		return true;
	}

	private void checkContInsuredIntlBL(LCInsuredTable pLCInsured) {
		cLogger.info("Into PadContInsuredIntlBL.checkContInsuredIntlBL()...");

		cLogger.info("Out PadContInsuredIntlBL.checkContInsuredIntlBL()!");
	}

	private VData getContInsuredIntlBLVData(LCInsuredTable pLCInsured) {
		cLogger
				.info("Into PadContInsuredIntlBL.getContInsuredIntlBLVData()...");
		// 被保人
		LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
		mLCInsuredSchema.setName(pLCInsured.getName());
		mLCInsuredSchema.setSex(pLCInsured.getSex());
		mLCInsuredSchema.setBirthday(pLCInsured.getBirthday());
		mLCInsuredSchema.setIDType(pLCInsured.getIDType());
		mLCInsuredSchema.setIDNo(pLCInsured.getIDNo());
		mLCInsuredSchema.setOccupationCode(pLCInsured.getOccupationCode());
		mLCInsuredSchema.setNativePlace(supplementTable.getInsureNativePlace());
		mLCInsuredSchema.setNativeCity(supplementTable.getInsureNativeCity());
		mLCInsuredSchema.setPosition(supplementTable.getInsurePosition());
		mLCInsuredSchema.setSalary(supplementTable.getInsureSalary());
		mLCInsuredSchema.setMarriage(supplementTable.getInsureMarriage());
		mLCInsuredSchema.setIDStartDate(supplementTable.getInsureIDStartDate());
		mLCInsuredSchema.setIDEndDate(supplementTable.getInsureIDEndDate());

		if ("PADTB01".equals(mMsgType)||"PADTBOP".equals(mMsgType)) {
			String mSQL = "select occupationtype from ldoccupation where OccupationCode='"
					+ pLCInsured.getOccupationCode() + "' with ur";
			String mOccupationTypeStr = new ExeSQL().getOneValue(mSQL);
			if ((null == mOccupationTypeStr) || mOccupationTypeStr.equals("")) {
				mLCInsuredSchema.setOccupationType("1"); // 职业类别为空的话，将职业类别置为1
			} else {
				mLCInsuredSchema.setOccupationType(mOccupationTypeStr); // 职业类别;
			}
		} else {
			mLCInsuredSchema.setOccupationType("1"); // 职业类别;
			// 按责任计算，试算时给默认值，对应责任默认的算费可能会用到。
		}
		mLCInsuredSchema.setPrtNo(cLCContSchema.getPrtNo());
		mLCInsuredSchema.setContNo(cLCContSchema.getContNo());
		mLCInsuredSchema.setRelationToMainInsured(pLCInsured.getRelaToMain());
		mLCInsuredSchema.setRelationToAppnt(pLCInsured.getRelaToAppnt());
		mLCInsuredSchema.setInsuredNo(pLCInsured.getInsuredNo());
		mLCInsuredSchema.setAuthorization(pLCInsured.getAuthorization());//授权标记

		// 被保人地址
		LCAddressSchema mInsuredAddress = new LCAddressSchema();
		mInsuredAddress.setHomeAddress(pLCInsured.getHomeAddress());
		mInsuredAddress.setPostalAddress(pLCInsured.getMailAddress());
		mInsuredAddress.setZipCode(pLCInsured.getMailZipCode());
		mInsuredAddress.setPhone(pLCInsured.getHomePhone());
		mInsuredAddress.setHomePhone(pLCInsured.getHomePhone());
		mInsuredAddress.setMobile(pLCInsured.getInsuredMobile());
		mInsuredAddress.setEMail(pLCInsured.getEmail());
		mInsuredAddress.setGrpName(supplementTable.getInsureGrpName());

		// 被保人个人信息
		LDPersonSchema mInsuredPerson = new LDPersonSchema();
		mInsuredPerson.setCustomerNo(""); // 注意此处必须设置！picch核心判断CustomerNo为""时才进行被保人五要素判断，CustomerNo为null时会直接生成新客户号
		mInsuredPerson.setName(mLCInsuredSchema.getName());
		mInsuredPerson.setSex(mLCInsuredSchema.getSex());
		mInsuredPerson.setIDNo(mLCInsuredSchema.getIDNo());
		mInsuredPerson.setIDType(mLCInsuredSchema.getIDType());
		mInsuredPerson.setBirthday(mLCInsuredSchema.getBirthday());
		mInsuredPerson.setOccupationCode(mLCInsuredSchema.getOccupationCode());
		mInsuredPerson.setOccupationType(mLCInsuredSchema.getOccupationType()); 
		mInsuredPerson.setNativeCity(supplementTable.getInsureNativeCity());
		mInsuredPerson.setNativePlace(supplementTable.getInsureNativePlace());
		mInsuredPerson.setPosition(supplementTable.getInsurePosition());
		mInsuredPerson.setSalary(supplementTable.getInsureSalary());
		mInsuredPerson.setMarriage(supplementTable.getInsureMarriage());
		mInsuredPerson.setAuthorization(pLCInsured.getAuthorization());

		LCInsuredDB mLCInsuredDB = new LCInsuredDB();
		mLCInsuredDB.setSchema(mLCInsuredSchema);

		mTransferData.setNameAndValue("FamilyType", "0");
		mTransferData.setNameAndValue("PolTypeFlag", "0");
		mTransferData.setNameAndValue("SequenceNo", pLCInsured.getInsuredNo());
		mTransferData.setNameAndValue("ContType", "1");
		mTransferData.setNameAndValue("SavePolType", "0");
		mTransferData.setNameAndValue("mMsgType", mMsgType);
		mTransferData.setNameAndValue("tComparePrem",cWrapTable.getPrem() );
		mTransferData.setNameAndValue("InsuredAuth", pLCInsured.getAuthorization());//授权标记

		LDRiskDutyWrapDB mLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
		mLDRiskDutyWrapDB.setRiskWrapCode(cWrapTable.getRiskWrapCode());
		LDRiskDutyWrapSet mLDRiskDutyWrapSet = mLDRiskDutyWrapDB.query();
		LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
		for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++) {
			LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
			if(cDutyCodeList.contains(tLDRiskDutyWrapSchema.getDutyCode())){
				LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
				tLCRiskDutyWrapSchema.setRiskWrapCode(tLDRiskDutyWrapSchema.getRiskWrapCode());
				tLCRiskDutyWrapSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
				tLCRiskDutyWrapSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());
				tLCRiskDutyWrapSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());
				tLCRiskDutyWrapSchema.setCalFactorType(tLDRiskDutyWrapSchema.getCalFactorType());
				for(int j = 1;j <= cWrapParamList.size();j++){
					WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(j-1);
					if(tLDRiskDutyWrapSchema.getRiskWrapCode().equals(tWrapParamTable.getRiskWrapCode()) 
							&& tLDRiskDutyWrapSchema.getRiskCode().equals(tWrapParamTable.getRiskCode())
							&& tLDRiskDutyWrapSchema.getDutyCode().equals(tWrapParamTable.getDutyCode()) 
							&& tLDRiskDutyWrapSchema.getCalFactor().equals(tWrapParamTable.getCalfactor())){
						if("1".equals(tLDRiskDutyWrapSchema.getCalFactorType()) 
								&& (!"".equals(tWrapParamTable.getCalfactorValue()))
								&& (!tLDRiskDutyWrapSchema.getCalFactorValue().equals(tWrapParamTable.getCalfactorValue()))){
							System.out.println(tWrapParamTable.getCalfactor());
							System.out.println(tWrapParamTable.getCalfactorValue());
							System.out.println(tLDRiskDutyWrapSchema.getCalFactorValue());
							Errors = "算费要素与产品描述不符，请核实！";
							return null;
							
						}
						tLCRiskDutyWrapSchema.setCalFactorValue(tWrapParamTable.getCalfactorValue());
					}
				}
				tLCRiskDutyWrapSchema.setInsuredNo(pLCInsured.getInsuredNo());//虚拟客户号码，避免后面生成客户号码。
				mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
			}
		}
		System.out.println("需要算费的算费参数："+mLCRiskDutyWrapSet.size());

		System.out.println("需要算费的算费参数：" + mLCRiskDutyWrapSet.size());
		// 受益人
		LCBnfSet mLCBnfSet = new LCBnfSet();
		if (cLCBnfList != null && cLCBnfList.size() > 0) {
			for (int i = 0; i < cLCBnfList.size(); i++) {
				LCBnfTable tLCBnfTable = (LCBnfTable) cLCBnfList.get(i);
				if (pLCInsured.getInsuredNo()
						.equals(tLCBnfTable.getInsuredNo())) {
					LCBnfSchema tLCBnfSchema = new LCBnfSchema();
					tLCBnfSchema.setContNo(cLCContSchema.getContNo());
					tLCBnfSchema.setBnfType(tLCBnfTable.getBnfType());
					tLCBnfSchema.setBnfGrade(tLCBnfTable.getBnfGrade());
					tLCBnfSchema.setName(tLCBnfTable.getName());
					tLCBnfSchema.setBirthday(tLCBnfTable.getBirthday());
					tLCBnfSchema.setSex(tLCBnfTable.getSex());
					tLCBnfSchema.setIDType(tLCBnfTable.getIDType());
					tLCBnfSchema.setIDNo(tLCBnfTable.getIDNo());
					tLCBnfSchema.setRelationToInsured(tLCBnfTable
							.getRelationToInsured());
					tLCBnfSchema.setBnfLot(tLCBnfTable.getBnfLot());
					/*
					 * 核心要求的受益比例在0.0-1.0之间，而标准报文的受益比例为1-100，所以需要在此做转换
					 */
					tLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot() / 100.0);
					mLCBnfSet.add(tLCBnfSchema);
				}
			}
		}
		mTransferData.setNameAndValue("AppntAddress", cLCAppntAddress);
		mTransferData.setNameAndValue("InsuredAddress", mInsuredAddress);
		mTransferData.setNameAndValue("WorkName", "");
		mTransferData.setNameAndValue("MissionProp5", mcardflag);
		mTransferData.setNameAndValue("WrapCode", "");

		

		// 客户告知
		if(cImpartTable!=null){
			for(int i=0;i<cImpartTable.size();i++){
				ImpartTable impartTable=(ImpartTable) cImpartTable.get(i);
				String ImpartCode = impartTable.getImpartCode();
				String impaetVer = impartTable.getImpartVer();
				SSRS s1=new SSRS();
				String content;
				
				content = new ExeSQL()
						.getOneValue("SELECT impartcontent FROM LDImpart WHERE ImpartVer = '"
								+ impaetVer
								+ "' and impartcode='"
								+ ImpartCode
								+ "'");
				
				LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
				
				tLCCustomerImpartSchema.setCustomerNoType("I");
				
				tLCCustomerImpartSchema.setImpartCode(ImpartCode);
				if("110".equals(ImpartCode)){
					tLCCustomerImpartSchema.setImpartContent("过去一年内是否有过医院门诊检查或治疗？○是  ○否");
				}else{
					tLCCustomerImpartSchema.setImpartContent(content);
				}
				
				if(impartTable.getImpartParamModle()==null){
					tLCCustomerImpartSchema
					.setImpartParamModle("");
				}else{
					tLCCustomerImpartSchema
					.setImpartParamModle(impartTable
							.getImpartParamModle());
				}
				
				tLCCustomerImpartSchema.setImpartVer(impaetVer);
				tLCCustomerImpartSchema.setPrtNo(cLCContTable.getPrtNo());
				tLCCustomerImpartSchema.setMakeDate(PubFun.getCurrentDate());
				tLCCustomerImpartSchema.setMakeTime(PubFun.getCurrentTime());
				tLCCustomerImpartSchema.setModifyDate(PubFun.getCurrentDate());
				tLCCustomerImpartSchema.setModifyTime(PubFun.getCurrentTime());
				tLCCustomerImpartSchema.setOperator(cGlobalInput.Operator);
				tLCCustomerImpartSchema.setGrpContNo("000000000000000");
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
				
			}
		}else{
			tLCCustomerImpartSet=null;
		}
		
		
		mVData.add(mTransferData);
		mVData.add(cGlobalInput);
		mVData.add(cLCContSchema);
		mVData.add(mLCInsuredSchema);
		mVData.add(mInsuredAddress);
		mVData.add(mLCInsuredDB);
		mVData.add(mInsuredPerson);
		mVData.add(mTransferData);
		mVData.add(mLCRiskDutyWrapSet);
		mVData.add(cLCAppntSchema);		
		mVData.add(mLCBnfSet);
		mVData.add(new LCDiseaseResultSet()); // 告知信息
		mVData.add(new LCNationSet()); // 抵达国家			    
		mVData.add(tLCCustomerImpartSet);

		cLogger.info("Out PadContInsuredIntlBL.getContInsuredIntlBLVData()!");
		return mVData;
	}
	private VData getContInsuredIntlBLVData() {
		mVData.clear();
		LCContSchema mLCContSchema=new LCContDB().executeQuery("select * from lccont where prtno='"+cLCContTable.getPrtNo()+"'").get(1);
		LCPolSet mLCPolSet=new LCPolDB().executeQuery("select * from lcpol where prtno='"+cLCContTable.getPrtNo()+"'");
		System.out.println(mLCPolSet.size());
		mVData.add(mLCContSchema);	
		mVData.add(mLCPolSet);
		mVData.add(mTransferData);
		mVData.add(cGlobalInput);
		mVData.add(new LCDiseaseResultSet()); // 告知信息
		mVData.add(new LCNationSet()); // 抵达国家		
		return mVData;
		
		
	}
	public double getPrem() {
		return mPrem;
	}

	public double getAmnt() {
		return mAmnt;
	}

	public LCDutySet getLCDutySet() {
		return cLCDutySet;
	}

	public LCPolSet getLCPolSet() {
		return cLCPolSet;
	}

	public LCRiskDutyWrapSet getLCRiskDutyWrapSet() {
		return cLCRiskDutyWrapSet;
	}
	private void getDutyCodeList(){
		cDutyCodeList = new ArrayList();
		for(int i = 0;i<cWrapParamList.size();i++){
			WrapParamTable tWrapParamTable = (WrapParamTable)cWrapParamList.get(i);
			String DutyCode = tWrapParamTable.getDutyCode();
			if(!cDutyCodeList.contains(DutyCode)){
				cDutyCodeList.add(DutyCode);
			}
		}
	}

	/**
	 * 生成打印管理表数据
	 * @return
	 */
	public boolean SendPayInfo(){
		// 判断是否需要打印缴费通知书。
        String tNoPrtFlag = "0";//默认：0-需要打印
        SSRS tSSRS= new ExeSQL().execSQL("select distinct(contno) from lccont where prtno='"+cLCContTable.getPrtNo()+"' ");
        if(tSSRS==null || tSSRS.getMaxRow()!=1){
        	cLogger.info("获取保单信息失败！！");
        	return false;
        }
        String mContNo=tSSRS.GetText(1,1);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mContNo);
        tLOPRTManagerSchema.setOtherNoType("00");
        tLOPRTManagerSchema.setCode("07");
        //校验是否发过首期交费通知书 ，如果发过则不再重发
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
        if (tLOPRTManagerDB.query().size() <= 0)
        {
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("NoPrtFlag", tNoPrtFlag);
            
            VData tempVData = new VData();
            tempVData.add(tLOPRTManagerSchema);
            tempVData.add(tTransferData);
            tempVData.add(cGlobalInput);
            //单独生成打印管理表数据
            UWSendPrintUIS tUWSendPrintUIS = new UWSendPrintUIS();
            if (!tUWSendPrintUIS.submitData(tempVData, "INSERT"))
            {
            	this.mErrors.copyAllErrors(tUWSendPrintUIS.mErrors);
                return false;
            }
        }
        return true;
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("程序开始…");
		System.out.println("成功结束！");
	}
}
