package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.Date;

import com.sinosoft.lis.bank.RealTimeSendBankUI;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;

import java.util.GregorianCalendar;

import com.sinosoft.lis.schema.LJTempFeeSchema;

public class PadTempfeeBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 记录传递参数 */
	private TransferData mTransferData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 传入对象 */
	private VData mInputData;

	/** 合同号 */
	private String mContNo;
	private String mAgreedPayDate;
	String getnoticeno="";
	String serialno="";

	private GlobalInput mGlobalInput;

	public PadTempfeeBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		this.mInputData = cInputData;

		System.out.println("@@完成程序submitData第41行");

		if (!getInputData()) {
			return false;
		}
		System.out.println("????????????????????????????");
		System.out.println("mContNo=" + mContNo);
		System.out.println("????????????????????????????");

		if (!prepareBankData(mContNo)) {
			return false;
		}
		
		PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, "INSERT"))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }

		return true;
	}

	/**
	 * getInputData
	 * 
	 * @return boolean
	 */
	private boolean getInputData() {
		if (this.mInputData == null) {
			buildError("getInputData", "传入信息为空,程序有错,找程序员处理！");
			return false;
		}
		this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		if (this.mTransferData == null) {
			buildError("getInputData", "传入的参数信息为null！");
			return false;
		}
		this.mContNo = (String) mTransferData.getValueByName("ContNo");
		this.mAgreedPayDate = (String) mTransferData.getValueByName("tAgreedPayDate");
		if (StrTool.cTrim(this.mContNo).equals("")) {
			buildError("getInputData", "传入合同号码为null,找程序员处理此问题！");
			return false;
		}
		this.mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0);
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	public TransferData getReturnTransferData() {
		return mTransferData;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PreviewPrintAfterInitService";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.println("程序报错：" + cError.errorMessage);
	}

	private boolean prepareBankData(String tContNo) {

		LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
		LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
		// 存储各种银行信息,银行编码,户名,帐号
		String BankFlag = "";
		String BankCode = "";
		String BankAccNo = "";
		String AccName = "";
		/**
		 * 合同信息
		 */
		if (tContNo == null) {
			return false;
		}
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(tContNo);
		if (!tLCContDB.getInfo()) {
			return false;
		}
		BankFlag = tLCContDB.getPayMode();
		BankCode = tLCContDB.getBankCode();
		BankAccNo = tLCContDB.getBankAccNo();
		AccName = tLCContDB.getAccName();

		System.out.println("缴费方式：---------" + BankFlag);

		if (StrTool.cTrim(BankAccNo).equals("")
				|| StrTool.cTrim(BankCode).equals("") || BankAccNo.equals("")) {
			CError tError = new CError();
			tError.moduleName = "UWSendPrintBL";
			tError.functionName = "prepareBankData";
			tError.errorMessage = "交费方式选择银行转账,但银行代码,银行账号,户名信息不完整!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		String mSQL = "select max(tempfeeno) from ljtempfee where tempfeeno like '" + tLCContDB.getPrtNo() + "%' with ur";
		String prtSeq = new ExeSQL().getOneValue(mSQL);
		if (prtSeq.equals("")) {
			prtSeq = tLCContDB.getPrtNo() + "801";
		} else {
			String tSuffix = prtSeq.substring(prtSeq.length() - 3);
			prtSeq = tLCContDB.getPrtNo() + (Integer.parseInt(tSuffix)+1); // 暂收费号+1
		}

		String sql = "select riskcode, sum(prem) from lcpol where ContNo='"
				+ mContNo + "' and uwflag in ('4','9') group by riskcode ";
		ExeSQL tExeSQL = new ExeSQL();
		// 自核通过的情况

		sql = "select riskcode, sum(prem) from lcpol where ContNo='" + mContNo
				+ "' group by riskcode ";
		SSRS ssrs = tExeSQL.execSQL(sql);
		if (ssrs.equals("null")) {
			CError tError = new CError();
			tError.moduleName = "UWSendPrintBL";
			tError.functionName = "prepareBankData";
			tError.errorMessage = "查询费用失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		String[][] prem = ssrs.getAllData();
		String serNo = PubFun1
				.CreateMaxNo("SERIALNO", tLCContDB.getManageCom());
		GregorianCalendar Calendar = new GregorianCalendar();
		Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
		Calendar.add(Calendar.DATE, 0);
		double sumPrem = 0.00;
		for (int i = 0; i + 1 <= prem.length; i++) {
			LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
			tLJTempFeeSchema.setTempFeeNo(prtSeq);
			tLJTempFeeSchema.setTempFeeType("1");
			tLJTempFeeSchema.setRiskCode(prem[i][0]);
			tLJTempFeeSchema.setAgentGroup(tLCContDB.getAgentGroup());
			tLJTempFeeSchema.setAPPntName(tLCContDB.getAppntName());
			tLJTempFeeSchema.setAgentCode(tLCContDB.getAgentCode());
			tLJTempFeeSchema.setPayDate(Calendar.getTime());
			if(mAgreedPayDate!=null && !"".equals(mAgreedPayDate)){
				tLJTempFeeSchema.setPayDate(mAgreedPayDate);
			}
			tLJTempFeeSchema.setPayMoney(prem[i][1]);
			sumPrem += Arith.round(Double.parseDouble(prem[i][1]), 2);
			tLJTempFeeSchema.setManageCom(tLCContDB.getManageCom());
			tLJTempFeeSchema.setOtherNo(tLCContDB.getPrtNo());
			tLJTempFeeSchema.setOtherNoType("4");
			tLJTempFeeSchema.setPolicyCom(tLCContDB.getManageCom());
			tLJTempFeeSchema.setSerialNo(serNo);
			tLJTempFeeSchema.setConfFlag("0");
			tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
			tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
			tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
			tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
			tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
//			tLJTempFeeSchema.setEnterAccDate(tLCContDB.getPolApplyDate());
//			tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
//			tLJTempFeeSchema.setConfDate(PubFun.getCurrentDate());
			tLJTempFeeSet.add(tLJTempFeeSchema);
		}
		LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
		tLJTempFeeClassSchema.setTempFeeNo(prtSeq);
		tLJTempFeeClassSchema.setPayMode("4");
		tLJTempFeeClassSchema.setPayDate(Calendar.getTime());
		if(mAgreedPayDate!=null && !"".equals(mAgreedPayDate)){
			tLJTempFeeClassSchema.setPayDate(mAgreedPayDate);
		}
		tLJTempFeeClassSchema.setPayMoney(Arith.round(sumPrem, 2));
		tLJTempFeeClassSchema.setManageCom(tLCContDB.getManageCom());
		tLJTempFeeClassSchema.setPolicyCom(tLCContDB.getManageCom());
		tLJTempFeeClassSchema.setBankCode(BankCode);
		tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
		tLJTempFeeClassSchema.setAccName(AccName);
		tLJTempFeeClassSchema.setSerialNo(serNo);
		tLJTempFeeClassSchema.setConfFlag("0");
		tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
		tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
		tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
		tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
		tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
//		tLJTempFeeClassSchema.setEnterAccDate(tLCContDB.getPolApplyDate());
//		tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
//		tLJTempFeeClassSchema.setConfDate(PubFun.getCurrentDate());
		tLJTempFeeClassSet.add(tLJTempFeeClassSchema);

		LCPolSchema tLCPolSchema = new LCPolSchema();
		LCPolSet mLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setPrtNo(tLCContDB.getPrtNo());
		mLCPolSet = tLCPolDB.query();
		if (tLCPolDB.mErrors.needDealError()) {
			this.mErrors.copyAllErrors(tLCPolDB.mErrors);
			return false;
		}
		if (mLCPolSet == null || mLCPolSet.size() == 0) {
			this.mErrors.addOneError("没有找到印刷号对应的投保单");
			return false;
		}
		
		LCPolSchema mLCPolSchema=new LCPolSchema();
		LCPolSet newLCPolSet=new LCPolSet();
		for (int j = 1; j <= mLCPolSet.size(); j++) {
			mLCPolSchema = mLCPolSet.get(j);
			if("1".equals(mLCPolSchema.getSpecifyValiDate())){
				mLCPolSchema.setSpecifyValiDate("N");
			}
			newLCPolSet.add(mLCPolSchema);
		}

		// 查询出主险保单数据
		for (int j = 1; j <= mLCPolSet.size(); j++) {
			tLCPolSchema = mLCPolSet.get(j);
			if (tLCPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo())) {
				break;
			}
		}

		
		FDate fDate = new FDate();
		Date paytoDate = new Date(); // 交费日期
		paytoDate = fDate.getDate(tLCPolSchema.getCValiDate());
		String strPayDate = fDate.getString(PubFun.calDate(paytoDate, 2, "M",
				null));

		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
		// 如果有多条暂交费纪录，其暂交费号是相同的
		tLJSPaySchema.setGetNoticeNo(tLJTempFeeClassSchema.getTempFeeNo());
		tLJSPaySchema.setOtherNo(tLCContDB.getPrtNo());
		tLJSPaySchema.setOtherNoType("9");
		tLJSPaySchema.setAppntNo(tLCContDB.getAppntNo());
		tLJSPaySchema.setPayDate(strPayDate);
		tLJSPaySchema.setBankOnTheWayFlag("0");
		tLJSPaySchema.setStartPayDate(tLCPolSchema.getCValiDate());
		if(mAgreedPayDate!=null && !"".equals(mAgreedPayDate)){
			tLJSPaySchema.setStartPayDate(mAgreedPayDate);
		}
		tLJSPaySchema.setBankSuccFlag("0");
		tLJSPaySchema.setSendBankCount(0);// 送银行次数
		tLJSPaySchema.setSumDuePayMoney(Arith.round(sumPrem, 2));
		tLJSPaySchema.setApproveCode(tLCPolSchema.getApproveCode());
		tLJSPaySchema.setApproveDate(tLCPolSchema.getApproveDate());
		tLJSPaySchema.setRiskCode(tLCPolSchema.getRiskCode());
		tLJSPaySchema.setBankAccNo(tLJTempFeeClassSchema.getBankAccNo());// 个人保单表没有，从暂交费子表中取
		tLJSPaySchema.setBankCode(tLJTempFeeClassSchema.getBankCode());
		tLJSPaySchema.setAccName(tLJTempFeeClassSchema.getAccName());
		tLJSPaySchema.setManageCom(tLCPolSchema.getManageCom());
		tLJSPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
		tLJSPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
		tLJSPaySchema.setSerialNo(serNo); // 流水号
		tLJSPaySchema.setCanSendBank("p");
		tLJSPaySchema.setOperator(mGlobalInput.Operator);
		tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
		tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
		tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
		tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());

		MMap map = new MMap();
		if (tLJTempFeeSet.size() > 0 && tLJTempFeeClassSet.size() > 0) {
			map.put(tLJTempFeeSet, "DELETE&INSERT");
			map.put(tLJTempFeeClassSet, "DELETE&INSERT");
			map.put(tLJSPaySchema, "DELETE&INSERT");
			map.put(newLCPolSet, "DELETE&INSERT");
		}
		this.mResult.add(map);
		return true;
	}

}
