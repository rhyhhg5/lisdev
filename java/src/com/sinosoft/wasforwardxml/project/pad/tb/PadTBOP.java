package com.sinosoft.wasforwardxml.project.pad.tb;

import java.util.List;

import org.jdom.Document;

import com.cbsws.obj.RspSaleInfo;
import com.sinosoft.lis.cbcheck.RecordFlagBL;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPersonTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.sys.MixedSalesAgentQueryUI;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.pad.obj.LCAppntTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCBnfTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCContTable;
import com.sinosoft.wasforwardxml.project.pad.obj.LCInsuredTable;
import com.sinosoft.wasforwardxml.project.pad.obj.OutputDataTable;
import com.sinosoft.wasforwardxml.project.pad.obj.SupplementTable;
import com.sinosoft.wasforwardxml.project.pad.obj.WrapTable;
import com.sinosoft.wasforwardxml.project.pad.query.PadQueryStateBL;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.workflowengine.ActivityOperator;

public class PadTBOP extends ABusLogic {

	public String BatchNo = "";// 批次号
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	public double mPrem = 0;// 保费
	public double mAmnt = 0;// 保额
	public List cWrapParamList;// 套餐参数问题
	public LCContTable cLCContTable;
	public LCAppntTable cLCAppntTable;
	public List cLCInsuredList;
	public LCInsuredTable cLCInsuredTable;
	public WrapTable cWrapTable;
	public List cLCBnfList;
	public LCBnfTable cLCBnfTable;
	public List cSupplementTable;
	public List cImpartTable;
	public OutputDataTable cOutputDataTable;
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String mError = "";// 处理过程中的错误信息
	public String Sysdate = PubFun.getCurrentDate();
	public String AgreedPayDate="";
	public String AgreedPayFlag="";
	String cRiskWrapCode = "";// 套餐编码
	
	private LCPersonTraceSchema mAppntLCPersonTraceSchema = new LCPersonTraceSchema();
	private LCPersonTraceSchema mInsuredLCPersonTraceSchema = new LCPersonTraceSchema();

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		return true;
	}

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理Pad简易出单投保过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		BatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();

		mGlobalInput.Operator = tMsgHead.getSendOperator();

		try {
			List tLCContList = cMsgInfos.getBodyByFlag("LCContTable");
			if (tLCContList == null || tLCContList.size() != 1) {
				errLog("获取保单信息失败。");
				return false;
			}
			cLCContTable = (LCContTable) tLCContList.get(0); // 获取保单信息，只有一个保单
			mGlobalInput.AgentCom = cLCContTable.getAgentCom();
			mGlobalInput.ManageCom = cLCContTable.getManageCom();
			mGlobalInput.ComCode = cLCContTable.getManageCom();
			List tWrapList = cMsgInfos.getBodyByFlag("WrapTable");
			if (tWrapList == null || tWrapList.size() != 1) {
				errLog("获取投保险种信息失败。");
				return false;
			}
			//约定缴费校验，标识：0-非约定缴费，1-约定缴费
        	AgreedPayFlag=cLCContTable.getAgreedPayFlag();
        	AgreedPayDate=cLCContTable.getAgreedPayDate();
        	if(AgreedPayFlag==null || "".equals(AgreedPayFlag)){
        		errLog("约定缴费标识不能为空！");
                return false;
        	}
        	if(!"0".equals(AgreedPayFlag) && !"1".equals(AgreedPayFlag)){
        		errLog("约定缴费标识只能为0或1！");
                return false;
        	}
        	if("0".equals(AgreedPayFlag) && AgreedPayDate!=null && !"".equals(AgreedPayDate)){
        		errLog("非约定缴费,约定缴费日期只能为空！");
                return false;
        	}
        	if("1".equals(AgreedPayFlag) && (AgreedPayDate==null || "".equals(AgreedPayDate))){
        		errLog("约定缴费,约定缴费日期不能为空！");
                return false;
        	}
        	if("1".equals(AgreedPayFlag) && Sysdate.compareTo(AgreedPayDate)>0){
        		errLog("约定缴费日期不能早于当前系统日期！");
                return false;
        	}
			cWrapTable = (WrapTable) tWrapList.get(0);
			cRiskWrapCode = cWrapTable.getRiskWrapCode();

			if (cRiskWrapCode == null || "".equals(cRiskWrapCode)) {
				errLog("在报文中获取套餐编码失败。");
				return false;
			}
			
			//zxs 20190815 #4502
			//交叉销售增加正确性校验 
			String crs_Salechnl = cLCContTable.getCrs_SaleChnl();
			String crs_BussType = cLCContTable.getCrs_BussType();
			String grpAgentCom = cLCContTable.getGrpAgentCom();
			String grpAgentCode = cLCContTable.getGrpAgentCode();
			String grpAgentName = cLCContTable.getGrpAgentName();
			String grpAgentIdno = cLCContTable.getGrpAgentIDNo();
			if((crs_Salechnl==null||("").equals(crs_Salechnl))&&(crs_BussType==null||("").equals(crs_BussType))&&
					(grpAgentCom==null||("").equals(grpAgentCom))&&(grpAgentCode==null||("").equals(grpAgentCode))
					&&(grpAgentName==null||("").equals(grpAgentName))&&(grpAgentIdno==null||("").equals(grpAgentIdno))){

			}else if((crs_Salechnl!=null&&!("").equals(crs_Salechnl))&&(crs_BussType!=null&&!("").equals(crs_BussType))&&
					(grpAgentCom!=null&&!("").equals(grpAgentCom))&&(grpAgentCode!=null&&!("").equals(grpAgentCode))
					&&(grpAgentName!=null&&!("").equals(grpAgentName))&&(grpAgentIdno!=null&&!("").equals(grpAgentIdno))){
				if(cLCContTable.getGrpAgentCode().trim().length()!=10){
					errLog("交叉销售对方业务员代码字段必须为10位，请核实!");
					return false;
				}else if(cLCContTable.getCrs_SaleChnl().equals("01")&&!cLCContTable.getGrpAgentCode().substring(0,1).equals("1")){
					errLog("交叉销售渠道是产代健，对方业务员代码需以数字1开头!");
					return false;
				}else if(cLCContTable.getCrs_SaleChnl().equals("02")&&!cLCContTable.getGrpAgentCode().substring(0,1).equals("3")){
					errLog("交叉销售渠道是寿代健，对方业务员代码需以数字3开头!");
					return false;
				}

				MixedSalesAgentQueryUI mixedSalesAgentQueryUI = new MixedSalesAgentQueryUI();
				RspSaleInfo rspSaleInfo = new RspSaleInfo();
				try{
					rspSaleInfo = mixedSalesAgentQueryUI.getSaleInfo(cLCContTable.getGrpAgentCode(),cLCContTable.getManageCom());
				}catch(Exception ex){
					errLog("集团获取交叉销售数据失败！");
					return false;
				}
				String type = rspSaleInfo.getMESSAGETYPE();
				String typeInfo = rspSaleInfo.getERRDESC();
				if("01".equals(type)){
					errLog("交叉销售数据错误："+typeInfo);
					return false;	  	
				}else {
					if(!cLCContTable.getGrpAgentCode().equals(rspSaleInfo.getUNI_SALES_COD())||
							!cLCContTable.getGrpAgentCom().equals(rspSaleInfo.getMAN_ORG_COD())|| 
							!cLCContTable.getGrpAgentName().equals(rspSaleInfo.getSALES_NAM())||
							!cLCContTable.getGrpAgentIDNo().equals(rspSaleInfo.getID_NO())){
						errLog("录入交叉销售信息与集团返回结果不一致，请核实!");
						return false;					  
					}			  	
				} 

			}else{
				errLog("交叉销售信息不完整，请核实!");
				return false;	
			}

//			String exRiskWrap = "select 1 from ldcode where codetype='padRiskWrap' and code='" + cRiskWrapCode + "'";
//			SSRS tRiskWrapSSRS = new ExeSQL().execSQL(exRiskWrap);
//			if (tRiskWrapSSRS == null || tRiskWrapSSRS.getMaxRow() <= 0) {
//				errLog("该套餐不能进行pad出单！");
//				return false;
//			}

			PadCommon tPadCommon = new PadCommon(cRiskWrapCode);
			if (!tPadCommon.checkTheOne(tWrapList)) {
				errLog("每次投保只能购买一个产品。");
				return false;
			}
			// 根据当前日期校验产品是否已停售
			mError = tPadCommon.checkWrapStop(PubFun.getCurrentDate());
			if (!"".equals(mError)) {
				errLog(mError);
				return false;
			}
			List tLCAppntList = cMsgInfos.getBodyByFlag("LCAppntTable");
			if (tLCAppntList == null || tLCAppntList.size() != 1) {
				errLog("获取投保人信息失败。");
				return false;
			}
			cLCAppntTable = (LCAppntTable) tLCAppntList.get(0);// 获取投保人信息，每个保单只有一个投保人

			String appntSex = cLCAppntTable.getAppntSex();
			String appntBirthday = cLCAppntTable.getAppntBirthday();
			String appntIDType = cLCAppntTable.getAppntIDType();
			String appntIDNo = cLCAppntTable.getAppntIDNo();
			String appntOccupationCode = cLCAppntTable.getOccupationCode();
			//投保人授权使用客户信息只能为1
            String appntAuthorization=cLCAppntTable.getAuthorization();
            if(appntAuthorization!=null && !appntAuthorization.equals("")){
            	if(!appntAuthorization.equals("1")){
            		errLog("投保人授权使用客户信息只能为1！");
            		return false;
            	}
            }else{
            	errLog("投保人授权使用客户信息不能为空！");
        		return false;
            }
            
			if (!"".equals(appntIDNo) && appntIDNo != null) {
				String chkAppntIDNo = PubFun.CheckIDNo(appntIDType, appntIDNo, appntBirthday, appntSex);
				if (chkAppntIDNo != null && !"".equals(chkAppntIDNo)) {
					errLog("投保人" + chkAppntIDNo);
					return false;
				}
			}
			   mAppntLCPersonTraceSchema.setAuthorization(cLCAppntTable.getAuthorization());
	            mAppntLCPersonTraceSchema.setBusinessLink(cLCAppntTable.getBusinessLink());
	            mAppntLCPersonTraceSchema.setAuthType(cLCAppntTable.getAuthType());
	            mAppntLCPersonTraceSchema.setSpecialLimitMark(cLCAppntTable.getSpecialLimitMark());
	            mAppntLCPersonTraceSchema.setCustomerContact(cLCAppntTable.getCustomerContact());
	            mAppntLCPersonTraceSchema.setSharedMark(cLCAppntTable.getSharedMark());
			// #4477 投保人职业代码校验  by yuchunjian 20190726
			if (appntOccupationCode != null && !"".equals(appntOccupationCode)){
				String appntOCSQL = new ExeSQL().getOneValue("select 1 from LDOccupation where OccupationCode = '" + appntOccupationCode + "'");
				if(appntOCSQL == null || "".equals(appntOCSQL)){
					mError="投保险人职业代码不正确!";
					errLog(mError);
            		return false;
				}
			}
			//投保人电话重复校验
			mError=checkAppnt();
            if(!"".equals(mError) && mError!= null){
            	errLog(mError);
            	return false;
            }
			cLCInsuredList = cMsgInfos.getBodyByFlag("LCInsuredTable");// 获取被保人
			cImpartTable = cMsgInfos.getBodyByFlag("ImpartTable");
			if (cLCInsuredList == null) {
				errLog("获取被保人信息失败。");
				return false;
			}
			if (!tPadCommon.checkTheOne(cLCInsuredList)) {
				errLog("该产品的被保人只能为一人！");
				return false;
			}
			cLCInsuredTable = (LCInsuredTable) cLCInsuredList.get(0);// 获取投保人信息，每个保单只有一个投保人
			String InsuredSex = cLCInsuredTable.getSex();
			String InsuredBirthday = cLCInsuredTable.getBirthday();
			String InsuredIDType = cLCInsuredTable.getIDType();
			String InsuredIDNo = cLCInsuredTable.getIDNo();
			String InsuredOccupationCode=cLCInsuredTable.getOccupationCode();
			//被保人授权使用客户信息只能为1
            String InsuredAuthorization=cLCInsuredTable.getAuthorization();
            if(InsuredAuthorization!=null && !InsuredAuthorization.equals("")){
            	if(!InsuredAuthorization.equals("1")){
            		errLog("被保人授权使用客户信息只能为1！");
            		return false;
            	}
            }else{
            	errLog("被保人授权使用客户信息不能为空！");
        		return false;
            }
			if (!"".equals(InsuredIDNo) && InsuredIDNo != null) {
				String chkInsuredIDNo = PubFun.CheckIDNo(InsuredIDType, InsuredIDNo, InsuredBirthday, InsuredSex);
				if (chkInsuredIDNo != null && !"".equals(chkInsuredIDNo)) {
					errLog("被保人" + chkInsuredIDNo);
					return false;
				}
			}
			 mInsuredLCPersonTraceSchema.setAuthorization(cLCInsuredTable.getAuthorization());
	         mInsuredLCPersonTraceSchema.setBusinessLink(cLCInsuredTable.getBusinessLink());
	         mInsuredLCPersonTraceSchema.setAuthType(cLCInsuredTable.getAuthType());
	         mInsuredLCPersonTraceSchema.setSpecialLimitMark(cLCInsuredTable.getSpecialLimitMark());
	         mInsuredLCPersonTraceSchema.setCustomerContact(cLCInsuredTable.getCustomerContact());
	         mInsuredLCPersonTraceSchema.setSharedMark(cLCInsuredTable.getSharedMark());
			// #4477 被保人职业代码校验  by yuchunjian 20190726
			if (InsuredOccupationCode != null && !"".equals(InsuredOccupationCode)){
				String InsuredOCSQL = new ExeSQL().getOneValue("select 1 from LDOccupation where OccupationCode = '" + InsuredOccupationCode + "'");
				if(InsuredOCSQL == null || "".equals(InsuredOCSQL)){
					mError="被保险人职业代码不正确!";
					errLog(mError);
            		return false;
				}
			}
			//被保人电话重复校验
//			mError=checkInsured();
//            if(!"".equals(mError) && mError!= null){
//            	errLog(mError);
//            	return false;
//            }
			cLCBnfList = cMsgInfos.getBodyByFlag("LCBnfTable");
			if (cLCBnfList != null && cLCBnfList.size() > 0) {
				for (int bnfsize = 0; bnfsize < cLCBnfList.size(); bnfsize++) {
					cLCBnfTable = (LCBnfTable) cLCBnfList.get(bnfsize);
					String bnfSex = cLCBnfTable.getSex();
					String bnfBirthday = cLCBnfTable.getBirthday();
					String bnfIDType = cLCBnfTable.getIDType();
					String bnfIDNo = cLCBnfTable.getIDNo();
					if (!"".equals(bnfIDNo) && bnfIDNo != null) {
						String chkbnfIDNo = PubFun.CheckIDNo(bnfIDType, bnfIDNo, bnfBirthday, bnfSex);
						if (chkbnfIDNo != null && !"".equals(chkbnfIDNo)) {
							errLog("受益人" + chkbnfIDNo);
							return false;
						}
					}
				}
			}
			cWrapParamList = cMsgInfos.getBodyByFlag("WrapParamTable");
			if (cWrapParamList == null) {
				errLog("获取计算保费参数信息失败。");
				return false;
			}
			cSupplementTable = cMsgInfos.getBodyByFlag("SupplementTable");

			// 增加printtype的校验
			SupplementTable tSupplementTable = (SupplementTable) cSupplementTable.get(0);
			if (tSupplementTable.getPrintType() == null || "".equals(tSupplementTable.getPrintType())
					|| "null".equals(tSupplementTable.getPrintType())) {
				errLog("获取保单打印类型信息失败。");
				return false;
			} else if (!"0".equals(tSupplementTable.getPrintType()) && !"1".equals(tSupplementTable.getPrintType())) {
				errLog("传入的保单打印类型必须是“0”或“1”!");
				return false;
			}

			// 增加销售渠道为中介时传入AgentSaleCode的校验
			String bSaleChnl = cLCContTable.getSaleChnl();
			String bAgentSaleCode = cLCContTable.getAgentSaleCode();
			if (("15".equals(bSaleChnl) || "04".equals(bSaleChnl) || "03".equals(bSaleChnl) || "10".equals(bSaleChnl))
					&& !"".equals(bAgentSaleCode) && null != bAgentSaleCode) {
				String bAgentSale = new ExeSQL().getOneValue("select 1 from laagenttemp where AgentCode='"
						+ bAgentSaleCode + "' and entryno = '" + cLCContTable.getAgentCom() + "'");
				if (!"1".equals(bAgentSale)) {
					errLog("代码为:[" + bAgentSaleCode + "]的代理销售业务员不存在，请确认!");
					return false;
				}
			} else {

				if (bAgentSaleCode != null && !"".equals(bAgentSaleCode)) {
					errLog("销售渠道为非中介时请不要传入代理销售业务员编码。");
					return false;
				}
			}
			// 投保单录入
			if ("".equals(StrTool.cTrim(cLCContTable.getPrtNo()))) {
				errLog("获取保单印刷号失败。");
				return false;
			}

			String sql = "select contno from lccont where prtno = '" + cLCContTable.getPrtNo() + "'" + " union "
					+ " select contno from lbcont where prtno = '" + cLCContTable.getPrtNo() + "'";
			SSRS tSSRS = new ExeSQL().execSQL(sql);
			if (tSSRS == null || tSSRS.getMaxRow() <= 0) {// 第一次核保，保单号为空，以便后面生成保单号吗
				cLCContTable.setContNo("");
			} else {
				errLog("保单印刷号已存在，不可再次使用。");
				return false;
			}
			// 根据管理机构获取agentcode
			String AgentCodesql = "select 1 from laagent where managecom = '" + cLCContTable.getManageCom() + "' "
					+ "and  agentcode='" + cLCContTable.getAgentCode() + "' ";
			String tAgentCode = new ExeSQL().getOneValue(AgentCodesql);
			if ("".equals(StrTool.cTrim(tAgentCode))) {
				errLog("销售人员不存在！");
				return false;
			}

			AgentCodesql = "select 1 from LAAgent where AgentCode = '" + cLCContTable.getAgentCode()
					+ "' and AgentState < '06' ";
			tAgentCode = new ExeSQL().getOneValue(AgentCodesql);
			if ("".equals(StrTool.cTrim(tAgentCode))) {
				errLog("销售人员已离职！");
				return false;
			}

			// 销售渠道校验
			String tSaleChnl = "select 1 from ldcode Where (Codetype = 'lcsalechnl' Or Codetype = 'salechnl') And Codename Not Like '%其他%' And  code='"
					+ cLCContTable.getSaleChnl() + "' ";
			String tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
			if ("".equals(StrTool.cTrim(tSaleChnlc))) {
				errLog("销售渠道录入不正确！");
				return false;
			}

			// 销售渠道与业务员匹配校验
			tSaleChnl = "select 1 from LAAgent a, LDCode1 b where a.AgentCode ='" + cLCContTable.getAgentCode() + "' "
					+ " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
					+ " and b.CodeType = 'salechnl' and b.Code = '" + cLCContTable.getSaleChnl() + "' " + " union all "
					+ " select 1 from LAAgent a where a.AgentCode ='" + cLCContTable.getAgentCode() + "' "
					+ "and '10' = '" + cLCContTable.getSaleChnl() + "' and a.BranchType2 = '04'  ";
			tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
			if ("".equals(StrTool.cTrim(tSaleChnlc))) {
				errLog("业务员和销售渠道不相符！");
				return false;
			}

			if ("04".equals(cLCContTable.getSaleChnl())) {
				if ("".equals(cLCContTable.getAgentCom())) {
					errLog("请录入中介机构！");
					return false;
				}
				tSaleChnl = "Select 1 From Lacom Where Managecom Like '" + cLCContTable.getManageCom() + "%'"
						+ " And Actype = '01' And (Endflag = 'N' Or Endflag Is Null) And Agentcom Like 'PY%' "
						+ " and agentcom ='" + cLCContTable.getAgentCom() + "'";
				tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
				if ("".equals(StrTool.cTrim(tSaleChnlc))) {
					errLog("中介机构不存在！");
					return false;
				}
				tSaleChnl = "select 1 from lacomtoagent where agentcode = '" + cLCContTable.getAgentCode() + "' "
						+ " and agentcom = '" + cLCContTable.getAgentCom() + "' ";
				tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
				if ("".equals(StrTool.cTrim(tSaleChnlc))) {
					errLog("业务员与中介机构不匹配！");
					return false;
				}
			} else if ("03".equals(cLCContTable.getSaleChnl()) || "10".equals(cLCContTable.getSaleChnl())
					|| "15".equals(cLCContTable.getSaleChnl())) {
				if ("".equals(cLCContTable.getAgentCom())) {
					errLog("请录入中介机构！");
					return false;
				}
				tSaleChnl = "select 1 from lacomtoagent where agentcode = '" + cLCContTable.getAgentCode() + "' "
						+ " and agentcom = '" + cLCContTable.getAgentCom() + "' ";
				tSaleChnlc = new ExeSQL().getOneValue(tSaleChnl);
				if ("".equals(StrTool.cTrim(tSaleChnlc))) {
					errLog("业务员与中介机构不匹配！");
					return false;
				}

			} else {
				if (!"".equals(cLCContTable.getAgentCom())) {
					errLog("该销售渠道中介机构应该为空！");
					return false;
				}
			}

			String ManageCom = cLCContTable.getManageCom();
			String bankE = "select 1 from ldbank where bankcode='" + cLCContTable.getBankCode() + "' and comcode like '"
					+ ManageCom.substring(0, 4) + "%' ";
			String canBank = new ExeSQL().getOneValue(bankE);
			if ("".equals(StrTool.cTrim(canBank))) {
				errLog("录入银行不存在！");
				return false;
			}

			bankE = "select 1 from ldbankunite where bankcode='" + cLCContTable.getBankCode()
					+ "' and bankunitecode in ('7705','7706','7709','7703','7701','7714') and (UniteGroupCode in ('1','3','') or UniteGroupCode is null) ";
			canBank = new ExeSQL().getOneValue(bankE);
			if ("".equals(StrTool.cTrim(canBank))) {
				errLog("录入银行不支持实时代收！");
				return false;
			}

			PadContInsuredBL ttPadContInsuredIntlBL = new PadContInsuredBL(tMsgHead, cLCContTable, cLCAppntTable,
					cSupplementTable, cWrapParamList, cLCInsuredList, cWrapTable, cLCBnfList, mGlobalInput,
					cImpartTable);
			mError = ttPadContInsuredIntlBL.deal();
			if (!"".equals(mError)) {
				System.out.println("删除新单开始，Prtno为：" + cLCContTable.getPrtNo());
				VData tVData = new VData();
				String tDeleteReason = "pad出单新单录入失败需要进行新单删除";
				TransferData tTransferData = new TransferData();
				LCContDB tLCContDB = new LCContDB();
				tLCContDB.setPrtNo(cLCContTable.getPrtNo());
				LCContSchema tLCContSchema = new LCContSchema();
				LCContSet tLCContSet = tLCContDB.query();
				if (tLCContSet.size() > 0) {
					tLCContSchema = tLCContSet.get(1);
					tTransferData.setNameAndValue("DeleteReason", tDeleteReason);
					tVData.add(tLCContSchema);
					tVData.add(tTransferData);
					tVData.add(mGlobalInput);
					ContDeleteUI tContDeleteUI = new ContDeleteUI();
					tContDeleteUI.submitData(tVData, "DELETE");
					System.out.println("删除新单结束");
				}
				errLog(mError);
				return false;
			}

			PadTempfeeBL tPadTempfeeBL = new PadTempfeeBL();
			String contnoSql = "select contno from lccont where prtno = '" + cLCContTable.getPrtNo() + "'";
			String contno = new ExeSQL().getOneValue(contnoSql);
			VData tVData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("ContNo", contno);
			tTransferData.setNameAndValue("tAgreedPayDate", AgreedPayDate);
			tVData.add(tTransferData);
			tVData.add(mGlobalInput);
			if (!tPadTempfeeBL.submitData(tVData, "")) {
				mError = tPadTempfeeBL.getErrors().getFirstError();
				errLog(mError);
				deleteData();
				return false;
			}

			String getnoticeno = "";
			String tsql = "select contno from lccont where prtno = '" + cLCContTable.getPrtNo()
					+ "' and prtno like 'PD%' ";
			SSRS ttSSRS = new ExeSQL().execSQL(tsql);
			if (ttSSRS == null || ttSSRS.getMaxRow() <= 0) {
				errLog("保单不存在");
				return false;
			}

			String tempinfo = "select 1 from ljtempfee where otherno='" + cLCContTable.getPrtNo()
					+ "' and enteraccdate is null";
			ttSSRS = new ExeSQL().execSQL(tempinfo);
			if (ttSSRS == null || ttSSRS.getMaxRow() <= 0) {
				errLog("保单无收费信息");
				return false;
			}

			String ljsinfo = "select getnoticeno,cansendbank from ljspay where otherno='" + cLCContTable.getPrtNo()
					+ "' and othernotype='9'";
			ttSSRS = new ExeSQL().execSQL(ljsinfo);
			if (ttSSRS == null || ttSSRS.getMaxRow() <= 0) {
				errLog("保单应收数据错误");
				return false;
			} else {
				getnoticeno = ttSSRS.GetText(1, 1);
				String cansend = ttSSRS.GetText(1, 2);
				if (!"p".equals(cansend)) {
					errLog("不能进行实时收费，需从核心进行批量收费！");
					return false;
				}
			}

			// RealTimeSendBankUI tRealTimeSendBankUI = new
			// RealTimeSendBankUI();
			// TransferData transferData1 = new TransferData();
			// transferData1.setNameAndValue("getNoticeNo", getnoticeno);
			// transferData1.setNameAndValue("bankCode", "7705");
			// transferData1.setNameAndValue("uniteBankCode",
			// cLCContTable.getBankCode());
			//
			// VData ttVData = new VData();
			// ttVData.add(transferData1);
			// ttVData.add(mGlobalInput);
			//
			// String serialno = "";
			//
			// if (!tRealTimeSendBankUI.submitData(ttVData, "GETMONEY")) {
			// VData rVData = tRealTimeSendBankUI.getResult();
			// errLog((String) rVData.get(0));
			// return false;
			// } else {
			// serialno = tRealTimeSendBankUI.getSerialNo();
			// }
			//
			// String temp = "select 1 from ljspay where otherno='" +
			// cLCContTable.getPrtNo() + "' and othernotype='9'";
			// SSRS mSSRS = new ExeSQL().execSQL(temp);
			// if (mSSRS == null || mSSRS.getMaxRow() <= 0) {
			// System.out.println("划款成功！！");
			// } else {
			// temp = "select ld.codename from lyreturnfrombank ly ,ldcode1 ld
			// where ly.serialno='" + serialno
			// + "' and ly.paycode='" + getnoticeno
			// + "' and ly.dealtype='R' and ld.codetype='bankerror' and
			// ld.code='7705'"
			// + " and ld.code1=ly.BankSuccFlag ";
			// mSSRS = new ExeSQL().execSQL(temp);
			// if (mSSRS.getMaxRow() > 0) {
			// System.out.println("" + mSSRS.GetText(1, 1));
			// errLog("银行收费失败，失败原因：" + mSSRS.GetText(1, 1));
			// }
			// }
			PadTBOP tb = new PadTBOP();
			String tSQL = "select * from lccont where contno='" + contno + "' ";
			LCContDB tLCContDB = new LCContDB();
			LCContSet tLCContSet = tLCContDB.executeQuery(tSQL);

			if (tLCContSet.size() == 0) {
				System.out.println("查询保单失败！");
				return false;
			}

			for (int i = 1; i <= tLCContSet.size(); i++) {
				LCContSchema tLCContSchema = new LCContSchema();
				tLCContSchema = tLCContSet.get(i);
				if (!tb.createmission(tLCContSchema)) {
					errLog("生成签单工作流失败！");
					return false;
				}
			}

			VData ttVData1 = new VData();
			TransferData ttTransferData = new TransferData();
			ttTransferData.setNameAndValue("ContNo", contno);
			ttTransferData.setNameAndValue("PrtNo", cLCContTable.getPrtNo());
			ttVData1.add(ttTransferData);
			RecordFlagBL tRecordFlagBL = new RecordFlagBL();
			if (!tRecordFlagBL.submitData(ttVData1, "")) {
				errLog("获取双录标识失败！");
				return false;
			}

			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
			if(!createSharedMark()){
				errLog("生成共享标识数据失败！");
                return false;
			}
			getXmlResult();
			String lockSQL = "update lccontsub set ScanCheckFlag = '0' where prtno = '"+cLCContTable.getPrtNo()+"'";
			new ExeSQL().execUpdateSQL(lockSQL);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 返回报文
	public void getXmlResult() {

		// 返回数据节点
		putResult("OutputDataTable", cOutputDataTable);

	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select contno,amnt,prem,prtno,stateflag from lccont where prtno = '" + cLCContTable.getPrtNo()
				+ "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取保单信息失败！";
			return false;
		} else {
			cOutputDataTable = new OutputDataTable();
			cOutputDataTable.setAmnt(tSSRS.GetText(1, 2));
			cOutputDataTable.setPrem(tSSRS.GetText(1, 3));
			cOutputDataTable.setContNo(tSSRS.GetText(1, 1));
			cOutputDataTable.setPrtNo(tSSRS.GetText(1, 4));
			if ("0".equals(tSSRS.GetText(1, 5))) {
				PadQueryStateBL tPadQueryStateBL = new PadQueryStateBL();
				String stateflag = tPadQueryStateBL.submitData(tSSRS.GetText(1, 1), tSSRS.GetText(1, 4));
				cOutputDataTable.setStateFlag(stateflag);
			} else {
				cOutputDataTable.setStateFlag(tSSRS.GetText(1, 5));
			}
		}

		return true;
	}

	/**
	 * deleteData
	 *
	 * @return boolean
	 */
	private boolean deleteData() {
		String sql = "select contno from lccont where prtno = '" + cLCContTable.getPrtNo() + "'";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {// 第一次核保，保单号为空，以便后面生成保单号吗
			return true;
		} else {// 多次核保，先删除保单数据
			cLCContTable.setContNo(tSSRS.GetText(1, 1));
		}
		String wherePart_ContNo = " and ContNo = '" + cLCContTable.getContNo() + "'";
		MMap DeleteMap = new MMap();
		DeleteMap.put("delete from lccont where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from lcpol where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from lcduty where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from lcprem where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from lcget where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from lcappnt where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from lcinsured where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCCustomerImpart where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCCustomerImpartParams where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCCustomerImpartDetail where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCNation where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCBnf where 1=1 " + wherePart_ContNo, "DELETE");
		DeleteMap.put("delete from LCRiskDutyWrap where 1=1 " + wherePart_ContNo, "DELETE");

		// 数据提交、保存
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("保存失败，删除原有保单信息！");
		VData mInputData = new VData();
		mInputData.add(DeleteMap);
		if (!tPubSubmit.submitData(mInputData, "")) {
			// @@错误处理
			mError = "保存失败时删除原有数据失败";
			return false;
		}

		return true;
	}

	/**
	 * 生成签单工作流
	 *
	 * @return boolean
	 */
	private boolean createmission(LCContSchema lcContSchema) {
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", lcContSchema.getPrtNo());
		tTransferData.setNameAndValue("AgentCode", lcContSchema.getAgentCode());
		tTransferData.setNameAndValue("ManageCom", lcContSchema.getManageCom());
		tTransferData.setNameAndValue("ContNo", lcContSchema.getContNo());
		tTransferData.setNameAndValue("AgentGroup", lcContSchema.getAgentGroup());
		tTransferData.setNameAndValue("AppntNo", lcContSchema.getAppntNo());
		tTransferData.setNameAndValue("AppntName", lcContSchema.getAppntName());

		GlobalInput aGlobalInput = new GlobalInput();
		aGlobalInput.AgentCom = lcContSchema.getAgentCom();
		aGlobalInput.ManageCom = lcContSchema.getManageCom();
		aGlobalInput.Operator = lcContSchema.getOperator();
		/** 总变量 */
		VData tmData = new VData();
		tmData.add(aGlobalInput);
		tmData.add(tTransferData);
		ActivityOperator aOperator = new ActivityOperator();
		String tProcessID = "0000000003";
		String[] s1 = { "0000001150" };
		LWMissionSchema tLwMissionSchema = aOperator.CreateStartMission(tProcessID, tmData);
		System.out.println(tLwMissionSchema.getMissionProp1());
		tLwMissionSchema.setMissionProp1(lcContSchema.getContNo());
		tLwMissionSchema.setMissionProp2(lcContSchema.getPrtNo());
		tLwMissionSchema.setMissionProp10(lcContSchema.getManageCom());

		MMap tMMap = new MMap();
		for (int i = 0; i < s1.length; i++) {
			LWMissionSchema l1 = new LWMissionSchema();
			l1 = tLwMissionSchema.getSchema();
			l1.setActivityID(s1[i]);
			tMMap.put(l1, "INSERT");
		}

		lcContSchema.setApproveFlag("9");
		lcContSchema.setApproveDate(PubFun.getCurrentDate());
		lcContSchema.setApproveTime(PubFun.getCurrentTime());
		lcContSchema.setUWFlag("9");
		lcContSchema.setUWOperator(Operator);
		lcContSchema.setUWDate(PubFun.getCurrentDate());
		lcContSchema.setUWTime(PubFun.getCurrentTime());

		tMMap.put(lcContSchema, "DELETE&INSERT");

		LCPolSchema tLCPolSchema = new LCPolSchema();
		LCPolSet mLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setPrtNo(lcContSchema.getPrtNo());
		mLCPolSet = tLCPolDB.query();
		if (mLCPolSet == null || mLCPolSet.size() == 0) {
			this.mErrors.addOneError("没有找到印刷号对应的投保单");
			return false;
		}
		// 查询出主险保单数据
		for (int j = 1; j <= mLCPolSet.size(); j++) {
			tLCPolSchema = mLCPolSet.get(j);
			tMMap.put(tLCPolSchema, "DELETE&INSERT");
		}

		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "FamilyTaxBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "生成日志失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	//投保人电话重复校验
    public String checkAppnt(){
    	String phone=cLCAppntTable.getAppntPhone();
    	String mobile=cLCAppntTable.getAppntMobile();
    	String homephone=cLCAppntTable.getAppntHomePhone();
    	String name=cLCAppntTable.getAppntName();
    	String idType=cLCAppntTable.getAppntIDType();
    	String id=cLCAppntTable.getAppntIDNo();

		if (!isNull(phone)) {
			String phoneSql = "select count(distinct customerno) from lcaddress "
					+ "where phone='"+ phone + "' "
					+ " and customerno not in (select customerno from ldperson where name='"+name+"' and idno='"+id+"' and idtype='"+idType+"') "
					+ " and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
			SSRS result = new ExeSQL().execSQL(phoneSql);
			if (result!=null && result.getMaxRow()>0) {
				int count = Integer.parseInt(result.GetText(1, 1));
				if (count >= 2) {
					mError="该投保人联系电话已在三个以上不同投保人的保单中出现，请核实！";
					return mError;
				}
			}
		}

		if (!isNull(mobile)) {
			String mobilSql = "select count(distinct customerno) from lcaddress "
					+ " where mobile='"+ mobile + "' "
					+ " and customerno not in (select customerno from ldperson where name='"+name+"' and idno='"+id+"' and idtype='"+idType+"') "
					+ " and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
			SSRS result = new ExeSQL().execSQL(mobilSql);
			if (result!=null && result.getMaxRow()>0) {
				int count = Integer.parseInt(result.GetText(1, 1));
				if (count >= 2) {
					mError="该投保人移动电话已在三个以上不同投保人的保单中出现，请核实！";
					return mError;
				}
			}
		}

		if (!isNull(homephone)) {
			String mobilSql = "select count(distinct customerno) from lcaddress "
					+ " where homephone='"+ homephone + "' "
					+ " and customerno not in (select customerno from ldperson where name='"+name+"' and idno='"+id+"' and idtype='"+idType+"') "
					+ " and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
			SSRS result = new ExeSQL().execSQL(mobilSql);
			if (result!=null && result.getMaxRow()>0) {
				int count = Integer.parseInt(result.GetText(1, 1));
				if (count >= 2) {
					mError="该投保人家庭电话已在三个以上不同投保人的保单中出现，请核实！";
					return mError;
				}
			}
		}
	
//		if (!isNull(phone)) {
//			String phoneSql1 = "select groupagentcode from laagent where phone='"
//					+ phone + "' and idno <> '" + id + "' with ur";
//			SSRS result1 = new ExeSQL().execSQL(phoneSql1);
//			if (result1!=null && result1.getMaxRow()>0) {
//				mError="该投保人联系电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//			String phoneSql2 = "select groupagentcode from laagent where mobile='"
//					+ phone + "' and idno <> '" + id + "' with ur";
//			SSRS result2 = new ExeSQL().execSQL(phoneSql2);
//			if (result2!=null && result2.getMaxRow()>0) {
//				mError="该投保人联系电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//			String phoneSql3 = "select agentcode from laagenttemp where phone='"
//					+ phone + "' and name <> '" + name + "' with ur";
//			SSRS result3 = new ExeSQL().execSQL(phoneSql3);
//			if (result3!=null && result3.getMaxRow()>0) {
//				mError="该投保人联系电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//		}
//
//		if (!isNull(mobile)) {
//			String mobileSql1 = "select groupagentcode from laagent where mobile='"
//					+ mobile + "' and idno <> '" + id + "' with ur";
//			SSRS result1 = new ExeSQL().execSQL(mobileSql1);
//			if (result1!=null && result1.getMaxRow()>0) {
//				mError="该投保人移动电话与本方业务员" + result1.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//			String mobileSql2 = "select groupagentcode from laagent where phone='"
//					+ mobile + "' and idno <> '" + id + "' with ur";
//			SSRS result2 = new ExeSQL().execSQL(mobileSql2);
//			if (result2!=null && result2.getMaxRow()>0) {
//				mError="该投保人移动电话与本方业务员" + result2.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//			String mobileSql3 = "select agentcode from laagenttemp where mobile='"
//					+ mobile + "' and name <> '" + name + "' with ur";
//			SSRS result3 = new ExeSQL().execSQL(mobileSql3);
//			if (result3!=null && result3.getMaxRow()>0) {
//				mError="该投保人移动电话与代理销售业务员" + result3.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//		}
//		if (!isNull(homephone)) {
//			String homephoneSql1 = "select groupagentcode from laagent where phone='"
//					+ homephone + "' and idno <> '" + id + "' with ur";
//			SSRS result1 = new ExeSQL().execSQL(homephoneSql1);
//			if (result1!=null && result1.getMaxRow()>0) {
//				mError="该投保人固定电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//			String homephoneSql2 = "select groupagentcode from laagent where mobile='"
//					+ homephone + "' and idno <> '" + id + "' with ur";
//			SSRS result2 = new ExeSQL().execSQL(homephoneSql2);
//			if (result2!=null && result2.getMaxRow()>0) {
//				mError="该投保人固定电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
//				return mError;
//			}
//			String homephoneSql3 = "select agentcode from laagenttemp where phone='"
//					+ homephone + "' and name <> '" + name + "' with ur";
//			SSRS result3 = new ExeSQL().execSQL(homephoneSql3);
//			if (result3!=null && result3.getMaxRow()>0) {
//				mError="该投保人固定电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
//				return mError;
//			}
//		}
    	return "";
    }

    public String checkInsured(){
    	int size=cLCInsuredList.size();//被保人人数
    	for(int i=0;i<size;i++){
    		LCInsuredTable tLCInsuredTable=new LCInsuredTable();
    		tLCInsuredTable=(LCInsuredTable)cLCInsuredList.get(i);
	    	String phone=tLCInsuredTable.getInsuredHomePhone();
	    	String mobile=tLCInsuredTable.getInsuredMobile();
	    	String homephone=tLCInsuredTable.getHomePhone();
	    	String name=tLCInsuredTable.getName();
	    	String id=tLCInsuredTable.getIDNo();
	    	if (!isNull(phone)) {
				String phoneSql1 = "select groupagentcode from laagent where phone='"
						+ phone + "' and idno <> '" + id + "' with ur";
				SSRS result1 = new ExeSQL().execSQL(phoneSql1);
				if (result1!=null && result1.getMaxRow()>0) {
					mError="被保人" + name + "联系电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
				String phoneSql2 = "select groupagentcode from laagent where mobile='"
						+ phone + "' and idno <> '" + id + "' with ur";
				SSRS result2 = new ExeSQL().execSQL(phoneSql2);
				if (result2!=null && result2.getMaxRow()>0) {
					mError="被保人" + name + "联系电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
					return mError;
				}
				String phoneSql3 = "select agentcode from laagenttemp where phone='"
						+ phone + "' and name <> '" + name + "' with ur";
				SSRS result3 = new ExeSQL().execSQL(phoneSql3);
				if (result3!=null && result3.getMaxRow()>0) {
					mError="被保人" + name + "联系电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
			}
			if (!isNull(mobile)) {
				String mobileSql1 = "select groupagentcode from laagent where mobile='"
						+ mobile + "' and idno <> '" + id + "' with ur";
				SSRS result1 = new ExeSQL().execSQL(mobileSql1);
				if (result1!=null && result1.getMaxRow()>0) {
					mError="被保人" + name + "移动电话与本方业务员" + result1.GetText(1, 1)
							+ "手机号码相同，请核查！";
					return mError;
				}
				String mobileSql2 = "select groupagentcode from laagent where phone='"
						+ mobile + "' and idno <> '" + id + "' with ur";
				SSRS result2 = new ExeSQL().execSQL(mobileSql2);
				if (result2!=null && result2.getMaxRow()>0) {
					mError="被保人" + name + "移动电话与本方业务员" + result2.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
				String mobileSql3 = "select agentcode from laagenttemp where mobile='"
						+ mobile + "' and name <> '" + name + "' with ur";
				SSRS result3 = new ExeSQL().execSQL(mobileSql3);
				if (result3!=null && result3.getMaxRow()>0) {
					mError="被保人" + name + "移动电话与代理销售业务员" + result1.GetText(1, 1) + "手机号码相同，请核查！";
					return mError;
				}
	
			}
			if (!isNull(homephone)) {
				String homephoneSql1 = "select groupagentcode from laagent where phone='"
						+ homephone + "' and idno <> '" + id + "' with ur";
				SSRS result1 = new ExeSQL().execSQL(homephoneSql1);
				if (result1!=null && result1.getMaxRow()>0) {
					mError="被保人" + name + "固定电话与本方业务员" + result1.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
				String homephoneSql2 = "select groupagentcode from laagent where mobile='"
						+ homephone + "' and idno <> '" + id + "' with ur";
				SSRS result2 = new ExeSQL().execSQL(homephoneSql2);
				if (result2!=null && result2.getMaxRow()>0) {
					mError="被保人" + name + "固定电话与本方业务员" + result2.GetText(1, 1) + "手机号码相同，请核查！";
					return mError;
				}
				String homephoneSql3 = "select agentcode from laagenttemp where phone='"
						+ homephone + "' and name <> '" + name + "' with ur";
				SSRS result3 = new ExeSQL().execSQL(homephoneSql3);
				if (result3!=null && result3.getMaxRow()>0) {
					mError="被保人" + name + "固定电话与代理销售业务员" + result3.GetText(1, 1) + "电话号码相同，请核查！";
					return mError;
				}
			}
    	}
    	return "";
    }
    //校验非空公共方法
    public boolean isNull(String checkStr) {
    	if (checkStr == null || "".equals(checkStr)) {
    		return true;
    	}
    	return false;
    }
    //创建共享标识数据
    public boolean createSharedMark(){
         String theCurrentDate = PubFun.getCurrentDate();
         String theCurrentTime = PubFun.getCurrentTime();
         MMap mMap = new MMap();
         mMap.put("delete from LCPersonTrace where  ContractNo='"+cLCContTable.getPrtNo()+"' ", "DELETE");
         VData vData = new VData();
         vData.add(mMap);
         PubSubmit pubSubmit = new PubSubmit();
         if (!pubSubmit.submitData(vData, ""))
         {
            errLog("生成共享标识数据失败！");
            return false;
         }
         mMap = new MMap();
         vData.clear();
    	LCAppntDB tlLcAppntDB = new LCAppntDB();
    	tlLcAppntDB.setPrtNo(cLCContTable.getPrtNo());
    	LCAppntSet tLcAppntSet =  tlLcAppntDB.query();
    	LCPersonTraceSchema tLcPersonTraceSchema = new LCPersonTraceSchema();
    	tLcPersonTraceSchema = mAppntLCPersonTraceSchema;
    	tLcPersonTraceSchema.setCustomerNo(tLcAppntSet.get(1).getAppntNo());
    	tLcPersonTraceSchema.setContractNo(cLCContTable.getPrtNo());
    	 SSRS tSSRS2 = new SSRS();
         String sql1 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                 +tLcPersonTraceSchema.getCustomerNo()+ "'";
         ExeSQL tExeSQL1 = new ExeSQL();
         tSSRS2 = tExeSQL1.execSQL(sql1);
         Integer firstinteger1 = Integer.valueOf(tSSRS2.GetText(1, 1));
         int tTraceNo = firstinteger1.intValue() + 1;
         Integer sTraceNo = new Integer(tTraceNo);
         String mTraceNo = sTraceNo.toString();
         tLcPersonTraceSchema.setTraceNo(mTraceNo);
         tLcPersonTraceSchema.setCompanySource("2");//人保健康
         tLcPersonTraceSchema.setInstitutionSource(tLcAppntSet.get(1).getManageCom());
         tLcPersonTraceSchema.setAuthVersion("1.0");
         tLcPersonTraceSchema.setSendDate(theCurrentDate);
         tLcPersonTraceSchema.setSendTime(theCurrentTime);
         tLcPersonTraceSchema.setModifyDate(theCurrentDate);
         tLcPersonTraceSchema.setModifyTime(theCurrentTime);
         mMap.put(tLcPersonTraceSchema, "INSERT");
         
         LCInsuredDB tLcInsuredDB = new LCInsuredDB();
         tLcInsuredDB.setPrtNo(cLCContTable.getPrtNo());
         LCInsuredSet tLcInsuredSet  = tLcInsuredDB.query();
         if(tLcInsuredSet.size()>0&&!"00".equals(tLcInsuredSet.get(1).getRelationToAppnt())){
         LCPersonTraceSchema mLcPersonTraceSchema = new LCPersonTraceSchema();
         mLcPersonTraceSchema = mInsuredLCPersonTraceSchema;
         mLcPersonTraceSchema.setCustomerNo(tLcInsuredSet.get(1).getInsuredNo());
         mLcPersonTraceSchema.setContractNo(cLCContTable.getPrtNo());
     	 SSRS tSSRS3 = new SSRS();
          String sql2 = "Select Case When varchar(max(int(TraceNo))) Is Null Then '0' Else varchar(max(int(TraceNo))) End from LCPersonTrace where CustomerNo='"
                  +mLcPersonTraceSchema.getCustomerNo()+ "'";
          ExeSQL tExeSQL2 = new ExeSQL();
          tSSRS3 = tExeSQL2.execSQL(sql2);
          Integer firstinteger2 = Integer.valueOf(tSSRS3.GetText(1, 1));
          int tTraceNo1 = firstinteger2.intValue() + 1;
          Integer sTraceNo1 = new Integer(tTraceNo1);
          String mTraceNo1 = sTraceNo1.toString();
          mLcPersonTraceSchema.setTraceNo(mTraceNo1);
          mLcPersonTraceSchema.setCompanySource("2");//人保健康
          mLcPersonTraceSchema.setInstitutionSource(tLcAppntSet.get(1).getManageCom());
          mLcPersonTraceSchema.setAuthVersion("1.0");
          mLcPersonTraceSchema.setSendDate(theCurrentDate);
          mLcPersonTraceSchema.setSendTime(theCurrentTime);
          mLcPersonTraceSchema.setModifyDate(theCurrentDate);
          mLcPersonTraceSchema.setModifyTime(theCurrentTime);
          mMap.put(mLcPersonTraceSchema, "INSERT");
         }
         
         vData.add(mMap);
         PubSubmit pubSubmit1 = new PubSubmit();
         if (!pubSubmit1.submitData(vData, ""))
         {
            errLog("生成共享标识数据失败！");;
            return false;
         } 
         mAppntLCPersonTraceSchema = new LCPersonTraceSchema();
         mInsuredLCPersonTraceSchema =  new LCPersonTraceSchema();
         mMap=new MMap();
         vData.clear();
    return true;	
    }
}
