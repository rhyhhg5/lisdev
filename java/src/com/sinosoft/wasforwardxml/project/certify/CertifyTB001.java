package com.sinosoft.wasforwardxml.project.certify;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.certify.input.CONTLISTTABLE;
import com.sinosoft.wasforwardxml.project.certify.input.INSULISTTABLE;
import com.sinosoft.wasforwardxml.project.certify.output.CertifyTBTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class CertifyTB001   extends ABusLogic{

	/**发送承保报文——保险卡信息*/
	public CONTLISTTABLE mCONTLISTTABLE;
	
	/**发送承保报文—— 被保人信息*/
	public INSULISTTABLE mINSULISTTABLE;
	
	/**返回承保报文*/
	public CertifyTBTable mCertifyTBTable;
	
	/**获取公共信息*/
	public GlobalInput mGlobalInput = new GlobalInput();

	public ExeSQL mExeSQL=new ExeSQL();
	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理发送承保报文过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		try {
			List tCONTLISTTABLE = cMsgInfos.getBodyByFlag("CONTLISTTABLE");
			List tINSULISTTABLE = cMsgInfos.getBodyByFlag("INSULISTTABLE");
			if (tCONTLISTTABLE == null || tCONTLISTTABLE.size() != 1) {
				errLog("获取保险卡信息失败。");
				return false;
			}
			if (tINSULISTTABLE == null || tINSULISTTABLE.size() != 1) {
				errLog("获取被保人信息失败。");
				return false;
			}
			mCONTLISTTABLE = (CONTLISTTABLE) tCONTLISTTABLE.get(0); // 保险卡信息
			mINSULISTTABLE = (INSULISTTABLE) tINSULISTTABLE.get(0); // 被保人信息
			if(null==mCONTLISTTABLE.getCARDNO()||"".equals(mCONTLISTTABLE.getCARDNO()))
			{
				errLog("保险卡信息中保险卡号不能为空。");
				return false;
			}
			if(null==mCONTLISTTABLE.getCVALIDATE()||"".equals(mCONTLISTTABLE.getCVALIDATE()))
			{
				errLog("保险卡信息中生效日期不能为空。");
				return false;
			}
			if(null==mCONTLISTTABLE.getCINVALIDATE()||"".equals(mCONTLISTTABLE.getCINVALIDATE()))
			{
				errLog("保险卡信息中满期日期不能为空。");
				return false;
			}
			if(null==mINSULISTTABLE.getINSUNO()||"".equals(mINSULISTTABLE.getINSUNO()))
			{
				errLog("被保人信息中编号不能为空。");
				return false;
			}
			if(null==mINSULISTTABLE.getNAME()||"".equals(mINSULISTTABLE.getNAME()))
			{
				errLog("被保人信息中被保人姓名不能为空。");
				return false;
			}
//			if(null==mINSULISTTABLE.getSEX()||"".equals(mINSULISTTABLE.getSEX()))
//			{
//				errLog("被保人信息中性别不能为空。");
//				return false;
//			}
//			if(!"1".equals(mINSULISTTABLE.getSEX())&&!"0".equals(mINSULISTTABLE.getSEX()))
//			{
//				errLog("被保人信息中性别不对，只能为0，1。");
//				return false;
//			}
//			if(null==mINSULISTTABLE.getBIRTHDAY()||"".equals(mINSULISTTABLE.getBIRTHDAY()))
//			{
//				errLog("被保人信息中出生日期不能为空。");
//				return false;
//			}
//			if(null==mINSULISTTABLE.getIDTYPE()||"".equals(mINSULISTTABLE.getIDTYPE()))
//			{
//				errLog("被保人信息中证件类型不能为空。");
//				return false;
//			}
//			if(null==mINSULISTTABLE.getIDNO()||"".equals(mINSULISTTABLE.getIDNO()))
//			{
//				errLog("被保人信息中证件号码不能为空。");
//				return false;
//			}
			String tsql=" select riskcode from lmcardrisk where certifycode = '"+mCONTLISTTABLE.getCERTIFYCODE()+"'  and risktype ='W' with ur";
			String triskcode=mExeSQL.getOneValue(tsql);
			if("".equals(triskcode)|| triskcode==null){
				errLog("单证编码"+mCONTLISTTABLE.getCERTIFYCODE()+"在系统中不存在或未定义！");
				return false;
			}
			String tcSql=" select cardno from LICardActiveInfoList where cardno = '"+mCONTLISTTABLE.getCARDNO()+"' with ur";
			String tcardno=mExeSQL.getOneValue(tcSql);
			if(!"".equals(tcardno)&& tcardno!=null){
				errLog("卡号"+mCONTLISTTABLE.getCARDNO()+"在系统中已存在不能重复承保！");
				return false;
			}
			if (!PrepareInfo()) {
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 获取保单信息
	public boolean PrepareInfo() {

		LICardActiveInfoListSchema tLICardActiveInfoListSchema = new LICardActiveInfoListSchema();
		tLICardActiveInfoListSchema.setCardNo(mCONTLISTTABLE.getCARDNO());
		String sql = "select riskcode from lmcardrisk where certifycode = '"+mCONTLISTTABLE.getCERTIFYCODE()+"'  and risktype ='W' with ur";
		String cardType = new ExeSQL().getOneValue(sql);
		tLICardActiveInfoListSchema.setCardType(cardType);
		tLICardActiveInfoListSchema.setActiveDate(mCONTLISTTABLE.getACTIVEDATE());
		tLICardActiveInfoListSchema.setCValidate(mCONTLISTTABLE.getCVALIDATE());
		tLICardActiveInfoListSchema.setInActiveDate(mCONTLISTTABLE.getCINVALIDATE());
		tLICardActiveInfoListSchema.setInsuYear(mCONTLISTTABLE.getINSUYEAR());
		tLICardActiveInfoListSchema.setInsuYearFlag(mCONTLISTTABLE.getINSUYEARFLAG());
		tLICardActiveInfoListSchema.setPrem(mCONTLISTTABLE.getPREM());
		tLICardActiveInfoListSchema.setAmnt(mCONTLISTTABLE.getAMNT());
		tLICardActiveInfoListSchema.setMult(mCONTLISTTABLE.getMULT());
		tLICardActiveInfoListSchema.setCopys(mCONTLISTTABLE.getCOPYS());
		tLICardActiveInfoListSchema.setTicketNo(mCONTLISTTABLE.getTICKETNO());
		tLICardActiveInfoListSchema.setTeamNo(mCONTLISTTABLE.getTEAMNO());
		tLICardActiveInfoListSchema.setSeatNo(mCONTLISTTABLE.getSEATNO());
		tLICardActiveInfoListSchema.setFrom(mCONTLISTTABLE.getFROM());
		tLICardActiveInfoListSchema.setTo(mCONTLISTTABLE.getTO());
		tLICardActiveInfoListSchema.setRemark(mCONTLISTTABLE.getREMARK());
		
		tLICardActiveInfoListSchema.setSequenceNo(mINSULISTTABLE.getINSUNO());
		tLICardActiveInfoListSchema.setName(mINSULISTTABLE.getNAME());
		tLICardActiveInfoListSchema.setSex(mINSULISTTABLE.getSEX());
		tLICardActiveInfoListSchema.setBirthday(mINSULISTTABLE.getBIRTHDAY());
		tLICardActiveInfoListSchema.setIdType(mINSULISTTABLE.getIDTYPE());
		tLICardActiveInfoListSchema.setIdNo(mINSULISTTABLE.getIDNO());
		tLICardActiveInfoListSchema.setOccupationType(mINSULISTTABLE.getOCCUPATIONTYPE());
		tLICardActiveInfoListSchema.setOccupationCode(mINSULISTTABLE.getOCCUPATIONCODE());
		tLICardActiveInfoListSchema.setPostalAddress(mINSULISTTABLE.getPOSTALADDRESS());
		tLICardActiveInfoListSchema.setZipCode(mINSULISTTABLE.getZIPCODE());
		tLICardActiveInfoListSchema.setPhone(mINSULISTTABLE.getPHONT());
		tLICardActiveInfoListSchema.setMobile(mINSULISTTABLE.getMOBILE());
		tLICardActiveInfoListSchema.setEMail(mINSULISTTABLE.getEMAIL());
		tLICardActiveInfoListSchema.setGrpName(mINSULISTTABLE.getGRPNAME());
		tLICardActiveInfoListSchema.setCompanyPhone(mINSULISTTABLE.getCOMPANYPHONE());
		tLICardActiveInfoListSchema.setState("00");
		tLICardActiveInfoListSchema.setCardStatus("01");
		tLICardActiveInfoListSchema.setOperator(mGlobalInput.Operator);
		tLICardActiveInfoListSchema.setMakeDate(PubFun.getCurrentDate());
		tLICardActiveInfoListSchema.setMakeTime(PubFun.getCurrentTime());
		tLICardActiveInfoListSchema.setModifyDate(PubFun.getCurrentDate());
		tLICardActiveInfoListSchema.setModifyTime(PubFun.getCurrentTime());
		
//		LZCardSchema tLZCardSchema=new LZCardSchema();
//		LZCardDB tLZCardDB=new LZCardDB();
//		tLZCardDB.setCertifyCode(aCertifyCode)
		String tlzcardsql=" update lzcard set state='14' where certifycode='"+mCONTLISTTABLE.getCERTIFYCODE()+"' and startno='"+mCONTLISTTABLE.getCARDNO().substring(2)+"' and endno='"+mCONTLISTTABLE.getCARDNO().substring(2)+"' and subcode='"+mCONTLISTTABLE.getCARDNO().substring(0,2)+"' ";
		
		MMap mMap = new MMap();
		mMap.put(tLICardActiveInfoListSchema,"INSERT");
		mMap.put(tlzcardsql,"UPDATE");
		VData mVData = new VData();
		mVData.add(mMap);
		PubSubmit mPubSubmit = new PubSubmit();
		if(!mPubSubmit.submitData(mVData, ""))
		{
			errLog("承保失败。");
			return false;
		}
		mCertifyTBTable = new CertifyTBTable();
		mCertifyTBTable.setCardNo(mCONTLISTTABLE.getCARDNO());
		putResult("CertifyTBTable", mCertifyTBTable);
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public static void main(String args[]){
		System.out.println("LJ00000001".substring(2)+"-----"+"LJ00000001".substring(2,2));
	}
}
