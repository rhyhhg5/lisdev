package com.sinosoft.wasforwardxml.project.certify;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.certify.input.CONTLISTTABLE;
import com.sinosoft.wasforwardxml.project.certify.output.CertifyTBTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

/**
 * 卡单退保
 * @author 杨阳    2014-10-23
 *
 */
public class CertifyTB002 extends ABusLogic{
	
	/**发送退保报文——保险卡信息*/
	public CONTLISTTABLE mCONTLISTTABLE;
	
	/**返回退保报文*/
	public CertifyTBTable mCertifyTBTable;
	
	/**获取公共信息*/
	public GlobalInput mGlobalInput = new GlobalInput();

	protected boolean deal(MsgCollection cMsgInfos) {
		System.out.println("开始处理发送承保报文过程");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		mGlobalInput.Operator = tMsgHead.getSendOperator();
		try {
			List tCONTLISTTABLE = cMsgInfos.getBodyByFlag("CONTLISTTABLE");
			if (tCONTLISTTABLE == null || tCONTLISTTABLE.size() != 1) {
				errLog("获取保险卡信息失败。");
				return false;
			}
			mCONTLISTTABLE = (CONTLISTTABLE) tCONTLISTTABLE.get(0); // 保险卡信息
			if(null==mCONTLISTTABLE.getCARDNO()||"".equals(mCONTLISTTABLE.getCARDNO()))
			{
				errLog("保险卡信息中保险卡号不能为空。");
				return false;
			}
			if(null==mCONTLISTTABLE.getCVALIDATE()||"".equals(mCONTLISTTABLE.getCVALIDATE()))
			{
				errLog("保险卡信息中生效日期不能为空。");
				return false;
			}
			if(null==mCONTLISTTABLE.getCINVALIDATE()||"".equals(mCONTLISTTABLE.getCINVALIDATE()))
			{
				errLog("保险卡信息中满期日期不能为空。");
				return false;
			}
			if (!PrepareInfo()) {
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql ="select 1 from LICardActiveInfoList   " +
				"where cardno ='"+mCONTLISTTABLE.getCARDNO()+"' " +
				"and cvalidate ='"+mCONTLISTTABLE.getCVALIDATE()+"' " +
				"and inactiveDate ='"+mCONTLISTTABLE.getCINVALIDATE()+"' with ur" ;
		String count = new ExeSQL().getOneValue(sql);
		if(""==count||null==count)
		{
			errLog("没有承保的卡单信息。");
			return false;
		}
		String sql1 = "update LICardActiveInfoList set CardStatus ='03',modifydate = '"+PubFun.getCurrentDate()+"',modifytime ='"+PubFun.getCurrentTime()+"' " +
				"where cardno ='"+mCONTLISTTABLE.getCARDNO()+"' " +
				"and cvalidate ='"+mCONTLISTTABLE.getCVALIDATE()+"' " +
				"and inactiveDate ='"+mCONTLISTTABLE.getCINVALIDATE()+"' " +
			    "with ur" ;
		if(!new ExeSQL().execUpdateSQL(sql1))
		{
			errLog("退保失败。");
			return false;
		}
		mCertifyTBTable = new CertifyTBTable();
		mCertifyTBTable.setCardNo(mCONTLISTTABLE.getCARDNO());
		putResult("CertifyTBTable", mCertifyTBTable);
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
}