package com.sinosoft.wasforwardxml.project.certify;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.wasforwardxml.project.certify.input.AgentCodeQueryTable;
import com.sinosoft.wasforwardxml.project.certify.output.AgentCodeTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

/**
 * 同步业务员
 * @author 杨阳     2014-10-22
 *
 */
public class CertifyAgentCode extends ABusLogic{
	
	/**查询标识*/            
	public String mQueryFlag  = null;
	
	/**业务员编码*/          
	public String mAgentCode  = null; 
	
	/**代理人展业机构代码*/ 
	public String mAgentGroup  = null; 
	
	/**管理机构编码*/        
	public String mManageCom  = null;  
	
	/**展业类型*/           
	public String mBranchType  = null;  
	
	/**渠道*/               
	public String mBranchType2 = null;
	
	/**同步业务员发送报文——业务员信息*/
	public AgentCodeQueryTable mAgentCodeQueryTable;
	
	/**同步业务员返回报文——业务员信息*/
	public AgentCodeTable mAgentCodeTable;
	
	/**处理过程中的错误信息*/
	public String mError = "";
	
	protected boolean deal(MsgCollection cMsgInfos) {
 		System.out.println("开始处理同步业务员过程");
		try {
			List tManageComQueryList = cMsgInfos.getBodyByFlag("AgentCodeQueryTable");
			if (tManageComQueryList == null || tManageComQueryList.size() != 1) {
				errLog("获取业务员信息失败。");
				return false;
			}
			mAgentCodeQueryTable = (AgentCodeQueryTable) tManageComQueryList.get(0); // 获取保单信息，只有一个保单
			mQueryFlag = mAgentCodeQueryTable.getQueryFlag();
			mAgentCode = mAgentCodeQueryTable.getAgentCode();
			mAgentGroup = mAgentCodeQueryTable.getAgentGroup();
			mManageCom = mAgentCodeQueryTable.getManageCom();
			mBranchType = mAgentCodeQueryTable.getBranchType();
			mBranchType2 = mAgentCodeQueryTable.getBranchType2();
			String sql = "select count from LAAgent where 1=1  and ManageCom like '8641%' ";
			if("01".equals(mQueryFlag))
			{
				if(null!=mAgentCode&&!"".equals(mAgentCode))
				{
					sql+=" and AgentCode='"+mAgentCode+"'";
				}
				if(null!=mAgentGroup&&!"".equals(mAgentGroup))
				{
					sql+=" and AgentGroup='"+mAgentGroup+"'";
				}
				if(null!=mManageCom&&!"".equals(mManageCom))
				{
					sql+=" and ManageCom='"+mManageCom+"'";
				}
				if(null!=mBranchType&&!"".equals(mBranchType))
				{
					sql+=" and BranchType='"+mBranchType+"'";
				}
				if(null!=mBranchType2&&!"".equals(mBranchType2))
				{
					sql+=" and BranchType2='"+mBranchType2+"'";
				}
			}
			sql+=" with ur";
			String count = new ExeSQL().getOneValue(sql);
			System.out.println(count);
			if (Integer.parseInt(count)<=0) {
				errLog("业务员不存在");
				return false;
			}
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select AgentCode,Name,AgentGroup,ManageCom,AgentState,BranchType,BranchType2,Phone,Mobile from LAAgent where 1=1 and ManageCom like '8641%'  ";
		if("01".equals(mQueryFlag))
		{
			if(null!=mAgentCode&&!"".equals(mAgentCode))
			{
				sql+=" and GroupAgentCode='"+mAgentCode+"'";
			}
			if(null!=mAgentGroup&&!"".equals(mAgentGroup))
			{
				sql+=" and AgentGroup='"+mAgentGroup+"'";
			}
			if(null!=mManageCom&&!"".equals(mManageCom))
			{
				sql+=" and ManageCom='"+mManageCom+"'";
			}
			if(null!=mBranchType&&!"".equals(mBranchType))
			{
				sql+=" and BranchType='"+mBranchType+"'";
			}
			if(null!=mBranchType2&&!"".equals(mBranchType2))
			{
				sql+=" and BranchType2='"+mBranchType2+"'";
			}
		}
		sql+=" with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取业务员信息失败！";
			return false;
		} else {
			for(int i = 1; i<=tSSRS.MaxRow;i++)
			{
				mAgentCodeTable = new AgentCodeTable();
				mAgentCodeTable.setAgentCode(tSSRS.GetText(i, 1));
				mAgentCodeTable.setName(tSSRS.GetText(i, 2));
				mAgentCodeTable.setAgentGroup(tSSRS.GetText(i, 3));
				mAgentCodeTable.setManageCom(tSSRS.GetText(i, 4));
				mAgentCodeTable.setAgentState(tSSRS.GetText(i, 5));
				mAgentCodeTable.setBranchType(tSSRS.GetText(i, 6));
				mAgentCodeTable.setBranchType2(tSSRS.GetText(i, 7));
				mAgentCodeTable.setPhone(tSSRS.GetText(i, 8));
				mAgentCodeTable.setMobile(tSSRS.GetText(i, 9));
				//返回报文
				putResult("AgentCodeTable", mAgentCodeTable);
			}
		}
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
}
