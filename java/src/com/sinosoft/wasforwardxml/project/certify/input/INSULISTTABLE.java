package com.sinosoft.wasforwardxml.project.certify.input;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 发送承保报文_被保人信息对应对象
 * @author 杨阳       2014-10-23
 *
 */
public class INSULISTTABLE extends BaseXmlSch {

	private static final long serialVersionUID = 8445947987668171795L;
	/**编号 */
	public String INSUNO         = null;     
	
	/**与投保人关系 */
	public String TOAPPNTRELA    = null;          
	
	/**被保人姓名 */
	public String NAME           = null;      
	
	/**性别 */
	public String SEX            = null;         
	
	/**出生日期 */
	public String BIRTHDAY       = null;  
	
	/**证件类型 */
	public String IDTYPE         = null;         
	
	/**证件号码 */
	public String IDNO           = null;
	
	/**被保人职业类别 */
	public String OCCUPATIONTYPE = null;          
	
	/**职业代码 */
	public String OCCUPATIONCODE = null;          
	
	/**联系地址 */
	public String POSTALADDRESS  = null;          
	
	/**联系地址邮编 */
	public String ZIPCODE        = null;          
	
	/**联系电话 */
	public String PHONT          = null;          
	
	/**手机 */
	public String MOBILE         = null;          
	
	/**电子邮箱 */
	public String EMAIL          = null;          
	
	/**单位名称 */
	public String GRPNAME        = null;          
	
	/**单位电话 */
	public String COMPANYPHONE   = null;

	public String getINSUNO() {
		return INSUNO;
	}

	public void setINSUNO(String iNSUNO) {
		INSUNO = iNSUNO;
	}

	public String getTOAPPNTRELA() {
		return TOAPPNTRELA;
	}

	public void setTOAPPNTRELA(String tOAPPNTRELA) {
		TOAPPNTRELA = tOAPPNTRELA;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getSEX() {
		return SEX;
	}

	public void setSEX(String sEX) {
		SEX = sEX;
	}

	public String getBIRTHDAY() {
		return BIRTHDAY;
	}

	public void setBIRTHDAY(String bIRTHDAY) {
		BIRTHDAY = bIRTHDAY;
	}

	public String getIDTYPE() {
		return IDTYPE;
	}

	public void setIDTYPE(String iDTYPE) {
		IDTYPE = iDTYPE;
	}

	public String getIDNO() {
		return IDNO;
	}

	public void setIDNO(String iDNO) {
		IDNO = iDNO;
	}

	public String getOCCUPATIONTYPE() {
		return OCCUPATIONTYPE;
	}

	public void setOCCUPATIONTYPE(String oCCUPATIONTYPE) {
		OCCUPATIONTYPE = oCCUPATIONTYPE;
	}

	public String getOCCUPATIONCODE() {
		return OCCUPATIONCODE;
	}

	public void setOCCUPATIONCODE(String oCCUPATIONCODE) {
		OCCUPATIONCODE = oCCUPATIONCODE;
	}

	public String getPOSTALADDRESS() {
		return POSTALADDRESS;
	}

	public void setPOSTALADDRESS(String pOSTALADDRESS) {
		POSTALADDRESS = pOSTALADDRESS;
	}

	public String getZIPCODE() {
		return ZIPCODE;
	}

	public void setZIPCODE(String zIPCODE) {
		ZIPCODE = zIPCODE;
	}

	public String getPHONT() {
		return PHONT;
	}

	public void setPHONT(String pHONT) {
		PHONT = pHONT;
	}

	public String getMOBILE() {
		return MOBILE;
	}

	public void setMOBILE(String mOBILE) {
		MOBILE = mOBILE;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getGRPNAME() {
		return GRPNAME;
	}

	public void setGRPNAME(String gRPNAME) {
		GRPNAME = gRPNAME;
	}

	public String getCOMPANYPHONE() {
		return COMPANYPHONE;
	}

	public void setCOMPANYPHONE(String cOMPANYPHONE) {
		COMPANYPHONE = cOMPANYPHONE;
	}
	
}
