package com.sinosoft.wasforwardxml.project.certify.input;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 发送承保报文_保险卡信息对应对象
 * @author 杨阳       2014-10-23
 *
 */
public class CONTLISTTABLE extends BaseXmlSch {

	private static final long serialVersionUID = 2717529948454747846L;

	/** 保险卡号 */
	public String CARDNO       = null;
	
	/** 保险卡类型 */
	public String CERTIFYCODE  = null;
	
	/** 保险卡名称 */
	public String CERTIFYNAME  = null;
	
	/** 激活日期 */
	public String ACTIVEDATE   = null;
	
	/** 生效日期 */
	public String CVALIDATE    = null;
	
	/** 满期日期 */
	public String CINVALIDATE  = null;
	
	/** 保障期间 */
	public String INSUYEAR     = null;
	
	/** 保障期间标志（D：天；M：月；Y：年） */
	public String INSUYEARFLAG = null;      
	
	/** 保险卡保费 */
	public String PREM         = null;
	
	/** 保险卡保额 */
	public String AMNT         = null;
	
	/** 保险卡险种档次 */
	public String MULT         = null;
	
	/** 份数 */
	public String COPYS        = null;
	
	/** 乘意险票号（如：机票号、车票号） */
	public String TICKETNO     = null;  
	
	/** 班次 */
	public String TEAMNO       = null;
	
	/** 座位号 */
	public String SEATNO       = null;
	
	/** 起始站 */
	public String FROM         = null;
	
	/** 终点站 */
	public String TO           = null;
	
	/** 说明（备注） */
	public String REMARK       = null;

	public String getCARDNO() {
		return CARDNO;
	}

	public void setCARDNO(String cARDNO) {
		CARDNO = cARDNO;
	}

	public String getCERTIFYCODE() {
		return CERTIFYCODE;
	}

	public void setCERTIFYCODE(String cERTIFYCODE) {
		CERTIFYCODE = cERTIFYCODE;
	}

	public String getCERTIFYNAME() {
		return CERTIFYNAME;
	}

	public void setCERTIFYNAME(String cERTIFYNAME) {
		CERTIFYNAME = cERTIFYNAME;
	}

	public String getACTIVEDATE() {
		return ACTIVEDATE;
	}

	public void setACTIVEDATE(String aCTIVEDATE) {
		ACTIVEDATE = aCTIVEDATE;
	}

	public String getCVALIDATE() {
		return CVALIDATE;
	}

	public void setCVALIDATE(String cVALIDATE) {
		CVALIDATE = cVALIDATE;
	}

	public String getCINVALIDATE() {
		return CINVALIDATE;
	}

	public void setCINVALIDATE(String cINVALIDATE) {
		CINVALIDATE = cINVALIDATE;
	}

	public String getINSUYEAR() {
		return INSUYEAR;
	}

	public void setINSUYEAR(String iNSUYEAR) {
		INSUYEAR = iNSUYEAR;
	}

	public String getINSUYEARFLAG() {
		return INSUYEARFLAG;
	}

	public void setINSUYEARFLAG(String iNSUYEARFLAG) {
		INSUYEARFLAG = iNSUYEARFLAG;
	}

	public String getPREM() {
		return PREM;
	}

	public void setPREM(String pREM) {
		PREM = pREM;
	}

	public String getAMNT() {
		return AMNT;
	}

	public void setAMNT(String aMNT) {
		AMNT = aMNT;
	}

	public String getMULT() {
		return MULT;
	}

	public void setMULT(String mULT) {
		MULT = mULT;
	}

	public String getCOPYS() {
		return COPYS;
	}

	public void setCOPYS(String cOPYS) {
		COPYS = cOPYS;
	}

	public String getTICKETNO() {
		return TICKETNO;
	}

	public void setTICKETNO(String tICKETNO) {
		TICKETNO = tICKETNO;
	}

	public String getTEAMNO() {
		return TEAMNO;
	}

	public void setTEAMNO(String tEAMNO) {
		TEAMNO = tEAMNO;
	}

	public String getSEATNO() {
		return SEATNO;
	}

	public void setSEATNO(String sEATNO) {
		SEATNO = sEATNO;
	}

	public String getFROM() {
		return FROM;
	}

	public void setFROM(String fROM) {
		FROM = fROM;
	}

	public String getTO() {
		return TO;
	}

	public void setTO(String tO) {
		TO = tO;
	}

	public String getREMARK() {
		return REMARK;
	}

	public void setREMARK(String rEMARK) {
		REMARK = rEMARK;
	}
	
}

