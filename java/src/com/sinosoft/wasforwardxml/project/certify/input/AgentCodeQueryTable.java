package com.sinosoft.wasforwardxml.project.certify.input;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 同步业务员对应对象
 * @author 杨阳        2014-10-22
 *
 */
public class AgentCodeQueryTable extends BaseXmlSch {

	private static final long serialVersionUID = 1L;

	/**查询标识*/            
	public String QueryFlag  = null;
	
	/**业务员编码*/          
	public String AgentCode  = null; 
	
	/**代理人展业机构代码*/ 
	public String AgentGroup  = null; 
	
	/**管理机构编码*/        
	public String ManageCom  = null;  
	
	/**展业类型*/           
	public String BranchType  = null;  
	
	/**渠道*/               
	public String BranchType2 = null;

	public String getQueryFlag() {
		return QueryFlag;
	}

	public void setQueryFlag(String queryFlag) {
		QueryFlag = queryFlag;
	}

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	public String getAgentGroup() {
		return AgentGroup;
	}

	public void setAgentGroup(String agentGroup) {
		AgentGroup = agentGroup;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getBranchType() {
		return BranchType;
	}

	public void setBranchType(String branchType) {
		BranchType = branchType;
	}

	public String getBranchType2() {
		return BranchType2;
	}

	public void setBranchType2(String branchType2) {
		BranchType2 = branchType2;
	}   

}
