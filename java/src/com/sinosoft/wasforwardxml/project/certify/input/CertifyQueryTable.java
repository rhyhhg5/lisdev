package com.sinosoft.wasforwardxml.project.certify.input;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;
/**
 * 同步单证发送报文对应对象
 * @author 杨阳   2014-10-22
 *
 */
public class CertifyQueryTable extends BaseXmlSch {

	private static final long serialVersionUID = 1L;

	/**查询标识*/
	 public String QueryFlag=null;
	 
	/**卡号    */
	 public String CardNo   =null;
	 
	/**单证类型*/
	 public String CardType =null;
	 
	/**起始日期*/
	 public String StartDate=null;
	 
	/**终止日期*/
	 public String EndDate  =null;

	public String getQueryFlag() {
		return QueryFlag;
	}

	public void setQueryFlag(String queryFlag) {
		QueryFlag = queryFlag;
	}

	public String getCardNo() {
		return CardNo;
	}

	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}

	public String getCardType() {
		return CardType;
	}

	public void setCardType(String cardType) {
		CardType = cardType;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	 
}
