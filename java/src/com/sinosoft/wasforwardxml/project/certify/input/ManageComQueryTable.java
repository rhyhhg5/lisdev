package com.sinosoft.wasforwardxml.project.certify.input;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;
/**
 * 同步管理机构发送报文对应对象
 * @author 杨阳   2014-10-22
 *
 */
public class ManageComQueryTable extends BaseXmlSch{

	private static final long serialVersionUID = 1L;
	
	/**查询标识*/
	private String QueryFlag =null;
	
	/**管理机构编码*/
	private String ManageCom=null;

	public String getQueryFlag() {
		return QueryFlag;
	}

	public void setQueryFlag(String queryFlag) {
		QueryFlag = queryFlag;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
}
