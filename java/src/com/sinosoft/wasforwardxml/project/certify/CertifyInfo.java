package com.sinosoft.wasforwardxml.project.certify;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.wasforwardxml.project.certify.input.CertifyQueryTable;
import com.sinosoft.wasforwardxml.project.certify.output.CertifyTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
/**
 *  同步单证
 * @author 杨阳     2014-10-22
 *
 */
public class CertifyInfo extends ABusLogic{

	/**查询标识*/
	 public String mQueryFlag="";
	 
	/**卡号    */
	 public String mCardNo   ="";
	 
	/**单证类型*/
	 public String mCardType ="";
	 
	/**起始日期*/
	 public String mStartDate="";
	 
	/**终止日期*/
	 public String mEndDate  ="";
	
	/**同步单证发送报文——单证信息*/
	public CertifyQueryTable mCertifyQueryTable;
	
	/**同步单证机构返回报文——单证信息*/
	public CertifyTable mCertifyTable;
	
	/**处理过程中的错误信息*/
	public String mError = "";
	
	protected boolean deal(MsgCollection cMsgInfos) {
 		System.out.println("开始处理同步单证过程");
		try {
			List tManageComQueryList = cMsgInfos.getBodyByFlag("CertifyQueryTable");
			if (tManageComQueryList == null || tManageComQueryList.size() != 1) {
				errLog("获取单证信息失败。");
				return false;
			}
			mCertifyQueryTable = (CertifyQueryTable) tManageComQueryList.get(0); // 获取保单信息，只有一个保单
			mQueryFlag = mCertifyQueryTable.getQueryFlag();
			mCardNo = mCertifyQueryTable.getCardNo();
			mCardType = mCertifyQueryTable.getCardType();
			mStartDate = mCertifyQueryTable.getStartDate();
			mEndDate = mCertifyQueryTable.getEndDate();
			if(null==mCardType&&"".equals(mCardType))
			{
				errLog("单证类型不能为空。");
				return false;
			}
			FDate fd = new FDate();
			String tEndDate = PubFun.getCurrentDate();
			String tStartDate = PubFun.calDate(tEndDate, -1, "M", "");
			//同步数据时只能同步一个月的数据
			if(null!=mStartDate&&!"".equals(mStartDate))
			{
				tStartDate = fd.getString(fd.getDate(mStartDate));
				tEndDate = PubFun.calDate(tStartDate, 1, "M", "");
				if(null!=mEndDate&&!"".equals(mEndDate))
				{
					//开始时间和结束时间只能相差一个月，如果超过一个月，提示：只能同步一个月的数据
					if(tEndDate.compareTo(mEndDate)<0)
					{
						errLog("只能同步一个月的数据！");
						return false;
					}
					else
					{
						tEndDate = mEndDate;
					}
				}
			}
			else if(null!=mEndDate&&!"".equals(mEndDate))
			{
				tEndDate= fd.getString(fd.getDate(tEndDate));
				tStartDate  = PubFun.calDate(tEndDate, -1, "M", "");
			}
			mStartDate = tStartDate;
			mEndDate = tEndDate;
			String sqlQuery = "select  count";
			String sqlFrom =" from LZCardNumber lzcn inner join LZCard lzc on lzcn.CardType = lzc.SubCode  and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo"+
							" where 1 = 1  and lzc.subcode in (select subcode from lmcertifydes where certifyclass = 'D' )";
			String sqlCond =" and exists (select 1  from lzcardtrack where subcode = lzc.subcode and startno = lzc.startno and endno = lzc.endno ";
			StringBuffer sql1 = new StringBuffer();
			StringBuffer sql2 = new StringBuffer();
			StringBuffer sql3 = new StringBuffer();
			sql1.append(sqlQuery).append(sqlFrom).append(" and lzc.state in ('4', '5', '6') ");
			sql2.append(sqlQuery).append(sqlFrom).append(" and lzc.state in ('3') ");
			sql3.append(sqlQuery).append(sqlFrom).append(" and lzc.state in ('10', '11') ");
			sql1.append(" and lzcn.CardType='"+mCardType+"' ");
			sql2.append(" and lzcn.CardType='"+mCardType+"' ");
			sql3.append(" and lzcn.CardType='"+mCardType+"' ");
			if("01".equals(mQueryFlag))
			{
				if(null!=mCardNo&&!"".equals(mCardNo))
				{
					sql1.append(" and lzcn.CardNo='"+mCardNo+"' ");
					sql2.append(" and lzcn.CardNo='"+mCardNo+"' ");
					sql3.append(" and lzcn.CardNo='"+mCardNo+"' ");
				}
				sql1.append(" and lzc.modifydate >='"+mStartDate+"' ");
				sql2.append(" and lzc.modifydate >='"+mStartDate+"' ");
				sql3.append(" and lzc.modifydate >='"+mStartDate+"' ");
				sql1.append(sqlCond).append(" and state in ('4', '5', '6') ").append(" and modifydate >='"+mStartDate+"' ");
				sql2.append(sqlCond).append(" and state in ('3') ").append(" and modifydate >='"+mStartDate+"' ");
				sql3.append(sqlCond).append(" and state in ('10', '11') ").append(" and modifydate >='"+mStartDate+"' ");
				sql1.append(" and modifydate <='"+mEndDate+"') ");
				sql2.append(" and modifydate <='"+mEndDate+"') ");
				sql3.append(" and modifydate <='"+mEndDate+"') ");
				sql1.append(" and lzc.modifydate <='"+mEndDate+"' ");
				sql2.append(" and lzc.modifydate <='"+mEndDate+"' ");
				sql3.append(" and lzc.modifydate <='"+mEndDate+"' ");
			}
			String count1 = new ExeSQL().getOneValue(sql1.toString());
			String count2 = new ExeSQL().getOneValue(sql2.toString());
			String count3 = new ExeSQL().getOneValue(sql3.toString());
			int count = Integer.parseInt(count1) + Integer.parseInt(count2) + Integer.parseInt(count3);
			if (count<=0) {
				errLog("单证不存在");
				return false;
			}
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 获取保单信息
	public boolean PrepareInfo() {

		StringBuffer sql = new StringBuffer();
		String sqlQuery = "select  cardno S_VoucherID,receivecom S_CompanyID,cardtype S_VoucherPre, cardserno N_VCFlowID,";
		String sqlFrom =" from LZCardNumber lzcn inner join LZCard lzc on lzcn.CardType = lzc.SubCode  and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo"+
						" where 1 = 1  and lzc.subcode in (select subcode from lmcertifydes where certifyclass = 'D' )";
		String sqlCond =" and exists (select 1  from lzcardtrack where subcode = lzc.subcode and startno = lzc.startno and endno = lzc.endno ";
		StringBuffer sql1 = new StringBuffer();
		StringBuffer sql2 = new StringBuffer();
		StringBuffer sql3 = new StringBuffer();
		sql1.append(sqlQuery).append("'1' S_State ").append(sqlFrom).append(" and lzc.state in ('4', '5', '6') ");
		sql2.append(sqlQuery).append("'2' S_State ").append(sqlFrom).append(" and lzc.state in ('3') ");
		sql3.append(sqlQuery).append("'0' S_State ").append(sqlFrom).append(" and lzc.state in ('10', '11') ");
		sql1.append(" and lzcn.CardType='"+mCardType+"' ");
		sql2.append(" and lzcn.CardType='"+mCardType+"' ");
		sql3.append(" and lzcn.CardType='"+mCardType+"' ");
		if("01".equals(mQueryFlag))
		{
			if(null!=mCardNo&&!"".equals(mCardNo))
			{
				sql1.append(" and lzcn.CardNo='"+mCardNo+"' ");
				sql2.append(" and lzcn.CardNo='"+mCardNo+"' ");
				sql3.append(" and lzcn.CardNo='"+mCardNo+"' ");
			}
			sql1.append(" and lzc.modifydate >='"+mStartDate+"' ");
			sql2.append(" and lzc.modifydate >='"+mStartDate+"' ");
			sql3.append(" and lzc.modifydate >='"+mStartDate+"' ");
			sql1.append(sqlCond).append(" and state in ('4', '5', '6') ").append(" and modifydate >='"+mStartDate+"' ");
			sql2.append(sqlCond).append(" and state in ('3') ").append(" and modifydate >='"+mStartDate+"' ");
			sql3.append(sqlCond).append(" and state in ('10', '11') ").append(" and modifydate >='"+mStartDate+"' ");
			sql1.append(" and modifydate <='"+mEndDate+"') ");
			sql2.append(" and modifydate <='"+mEndDate+"') ");
			sql3.append(" and modifydate <='"+mEndDate+"') ");
			sql1.append(" and lzc.modifydate <='"+mEndDate+"' ");
			sql2.append(" and lzc.modifydate <='"+mEndDate+"' ");
			sql3.append(" and lzc.modifydate <='"+mEndDate+"' ");
		}
		sql.append(sql1).append(" union ").append(sql2).append(" union ").append(sql3).append(" with ur");
		SSRS tSSRS = new ExeSQL().execSQL(sql.toString());
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取单证信息失败！";
			return false;
		} else {
			for(int i = 1; i<=tSSRS.MaxRow;i++)
			{
				mCertifyTable = new CertifyTable();
				mCertifyTable.setCardNo(tSSRS.GetText(i, 1));
				mCertifyTable.setReceiveCom(tSSRS.GetText(i, 2));
				mCertifyTable.setCardType(tSSRS.GetText(i, 3));
				mCertifyTable.setCardSerNo(tSSRS.GetText(i, 4));
				mCertifyTable.setCardState(tSSRS.GetText(i, 5));
				//返回报文
				putResult("CertifyTable", mCertifyTable);
			}
		}
		return true;
		
	}
	public static void main(String[] args) {
		String tEndDate = PubFun.getCurrentDate();
		String tStartDate = PubFun.calDate(tEndDate, -1, "M", "");
		System.out.println(tStartDate);
	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
}
