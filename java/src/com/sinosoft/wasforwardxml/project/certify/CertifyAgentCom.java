package com.sinosoft.wasforwardxml.project.certify;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.wasforwardxml.project.certify.input.AgentComQueryTable;
import com.sinosoft.wasforwardxml.project.certify.output.AgentComTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public class CertifyAgentCom extends ABusLogic{
	
	/**查询标识*/
	public String mQueryFlag = null;
	
	/**中介机构编码*/
	public String mAgentCom = null;
	
	/**管理机构编码*/
	public String mManageCom = null;
	
	/**同步中介机构发送报文——中介机构信息*/
	public AgentComQueryTable mAgentComQueryTable;
	
	/**同步中介机构返回报文——中介机构信息*/
	public AgentComTable mAgentComTable;
	
	/**处理过程中的错误信息*/
	public String mError = "";
	
	protected boolean deal(MsgCollection cMsgInfos) {
 		System.out.println("开始处理同步中介机构过程");
		try {
			List tManageComQueryList = cMsgInfos.getBodyByFlag("AgentComQueryTable");
			if (tManageComQueryList == null || tManageComQueryList.size() != 1) {
				errLog("获取中介机构信息失败。");
				return false;
			}
			mAgentComQueryTable = (AgentComQueryTable) tManageComQueryList.get(0); // 获取保单信息，只有一个保单
			mQueryFlag = mAgentComQueryTable.getQueryFlag();
			mAgentCom = mAgentComQueryTable.getAgentCom();
			mManageCom = mAgentComQueryTable.getManageCom();
			String sql = "select count from LACom where 1=1 and ManageCom like '8641%'  ";
			if("01".equals(mQueryFlag))
			{
				if(null!=mAgentCom&&!"".equals(mAgentCom))
				{
					sql+=" and AgentCom='"+mAgentCom+"'";
				}
				if(null!=mManageCom&&!"".equals(mManageCom))
				{
					sql+=" and ManageCom='"+mManageCom+"'";
				}
			}
			sql+=" with ur";
			String count = new ExeSQL().getOneValue(sql);
			if (Integer.parseInt(count)<=0) {
				errLog("中介机构不存在");
				return false;
			}
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 获取中介机构信息
	public boolean PrepareInfo() {

		String sql = "select AgentCom,ManageCom,Name,EndFlag,Address,ChiefBusiness," +
				" (select laa.Name from LAAgent laa inner join LAComToAgent lacta on laa.AgentCode = lacta.AgentCode where lacta.AgentCom = lac.AgentCom fetch first row only) ," +
				"(case  when " +
				"(select laa.Phone from LAAgent laa inner join LAComToAgent lacta on laa.AgentCode =lacta.AgentCode"+
                " where lacta.AgentCom = lac.AgentCom fetch first row only) is null " +
                "then   (select laa.Phone  from LAAgent laa inner join LAComToAgent lacta on laa.AgentCode = lacta.AgentCode"+
                " where lacta.AgentCom = lac.AgentCom fetch first row only)"+
                " else    (select laainner.Mobile from LAAgent laainner join LAComToAgent lacta on laainner.AgentCode = lacta.AgentCode"+
                " where lacta.AgentCom = lac.AgentCom fetch first row only) end)," +
				"LicenseNo from LACom lac where 1=1 and ManageCom like '8641%'  ";
		if("01".equals(mQueryFlag))
		{
			if(null!=mAgentCom&&!"".equals(mAgentCom))
			{
				sql+=" and AgentCom='"+mAgentCom+"'";
			}
			if(null!=mManageCom&&!"".equals(mManageCom))
			{
				sql+=" and ManageCom='"+mManageCom+"'";
			}
		}
		sql+=" fetch first 3000 rows only  with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取中介机构信息失败！";
			return false;
		} else {
			for(int i = 1; i<=tSSRS.MaxRow;i++)
			{
				mAgentComTable = new AgentComTable();
				mAgentComTable.setAgentCom(tSSRS.GetText(i, 1));
				mAgentComTable.setManageCom(tSSRS.GetText(i, 2));
				mAgentComTable.setName(tSSRS.GetText(i, 3));
				mAgentComTable.setEndFlag(tSSRS.GetText(i, 4));
				mAgentComTable.setAddress(tSSRS.GetText(i, 5));
				mAgentComTable.setChiefBusiness(tSSRS.GetText(i, 6));
				mAgentComTable.setAgentCodeName(tSSRS.GetText(i, 7));
				mAgentComTable.setAgentCodePhone(tSSRS.GetText(i, 8));
				mAgentComTable.setLicenseNo(tSSRS.GetText(i, 9));
				//返回报文
				putResult("AgentComTable", mAgentComTable);
			}
		}
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
}
