package com.sinosoft.wasforwardxml.project.certify.output;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 同步中介机构批回报文对应对象
 * @author 杨阳   2014-10-22
 *
 */
public class AgentComTable extends BaseXmlSch {

	private static final long serialVersionUID = -8386155799757281737L;

	/** 中介机构编码*/
	public String AgentCom       = null; 
	
	/** 管理机构编码*/
	public String ManageCom      = null;
	
	/** 中介机构名称 */
	public String Name           = null;
	
	/** 停业标志 */
	public String EndFlag        = null; 
	
	/** 中介机构地址 */
	public String Address        = null; 
	
	/** 主营业务 */
	public String ChiefBusiness  = null; 
	
	/** 业务员名称 */
	public String AgentCodeName  = null; 
	
	/** 业务员联系电话*/
	public String AgentCodePhone = null; 
	
	/** 许可证号码 */
	public String LicenseNo      = null;

	public String getAgentCom() {
		return AgentCom;
	}

	public void setAgentCom(String agentCom) {
		AgentCom = agentCom;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEndFlag() {
		return EndFlag;
	}

	public void setEndFlag(String endFlag) {
		EndFlag = endFlag;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getChiefBusiness() {
		return ChiefBusiness;
	}

	public void setChiefBusiness(String chiefBusiness) {
		ChiefBusiness = chiefBusiness;
	}

	public String getAgentCodeName() {
		return AgentCodeName;
	}

	public void setAgentCodeName(String agentCodeName) {
		AgentCodeName = agentCodeName;
	}

	public String getAgentCodePhone() {
		return AgentCodePhone;
	}

	public void setAgentCodePhone(String agentCodePhone) {
		AgentCodePhone = agentCodePhone;
	}

	public String getLicenseNo() {
		return LicenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		LicenseNo = licenseNo;
	}
	
}
