package com.sinosoft.wasforwardxml.project.certify.output;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 返回承保报文对应对象
 * @author 杨阳   2014-10-22
 *
 */
public class CertifyTBTable extends BaseXmlSch {

	private static final long serialVersionUID = -7597468566775056190L;
	
	/**卡号*/
	public String CardNo = null;

	public String getCardNo() {
		return CardNo;
	}

	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}
	
}
