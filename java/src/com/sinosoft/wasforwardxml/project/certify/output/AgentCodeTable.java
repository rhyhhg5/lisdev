package com.sinosoft.wasforwardxml.project.certify.output;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 同步业务员返回报文对应对象
 * @author 杨阳   2014-10-22
 *
 */
public class AgentCodeTable extends BaseXmlSch {

	private static final long serialVersionUID = 1L;

	/**业务员编码*/
	public String AgentCode   = null;
	
	/**业务员名称 */
	public String Name        = null;
	
	/**代理人展业机构代码 */
	public String AgentGroup  = null;
	
	/**管理机构编码*/
	public String ManageCom   = null;
	
	/**代理人状态 */
	public String AgentState  = null;
	
	/**展业类型 */
	public String BranchType  = null;
	
	/**渠道 */
	public String BranchType2 = null;
	
	/**电话 */
	public String Phone       = null;
	
	/**手机 */
	public String Mobile      = null;

	public String getAgentCode() {
		return AgentCode;
	}

	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAgentGroup() {
		return AgentGroup;
	}

	public void setAgentGroup(String agentGroup) {
		AgentGroup = agentGroup;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getAgentState() {
		return AgentState;
	}

	public void setAgentState(String agentState) {
		AgentState = agentState;
	}

	public String getBranchType() {
		return BranchType;
	}

	public void setBranchType(String branchType) {
		BranchType = branchType;
	}

	public String getBranchType2() {
		return BranchType2;
	}

	public void setBranchType2(String branchType2) {
		BranchType2 = branchType2;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	
}
