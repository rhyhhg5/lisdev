package com.sinosoft.wasforwardxml.project.certify.output;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 同步单证返回报文对应对对象
 * @author 杨阳   2014-10-22
 *
 */
public class CertifyTable extends BaseXmlSch{

	private static final long serialVersionUID = 1L;

	/** 卡号*/           
	public String CardNo = null;    
	
	/** 接收机构*/   
	public String ReceiveCom = null; 
	
	/** 单证类型 */    
	public String CardType = null;   
	
	/** 单证流水号 */ 
	public String CardSerNo = null;  
	
	/** 单证状态 */   
	public String CardState = null;

	public String getCardNo() {
		return CardNo;
	}

	public void setCardNo(String cardNo) {
		CardNo = cardNo;
	}

	public String getReceiveCom() {
		return ReceiveCom;
	}

	public void setReceiveCom(String receiveCom) {
		ReceiveCom = receiveCom;
	}

	public String getCardType() {
		return CardType;
	}

	public void setCardType(String cardType) {
		CardType = cardType;
	}

	public String getCardSerNo() {
		return CardSerNo;
	}

	public void setCardSerNo(String cardSerNo) {
		CardSerNo = cardSerNo;
	}

	public String getCardState() {
		return CardState;
	}

	public void setCardState(String cardState) {
		CardState = cardState;
	}  
	
}
