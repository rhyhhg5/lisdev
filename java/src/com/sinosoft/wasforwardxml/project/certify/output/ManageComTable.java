package com.sinosoft.wasforwardxml.project.certify.output;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

/**
 * 同步管理机构返回报文对应对象
 * @author 杨阳   2014-10-22
 *
 */
public class ManageComTable  extends BaseXmlSch{

	private static final long serialVersionUID = 1L;
	
	/**管理机构编码*/
	public String ManageCom = null;
	
	/**管理机构名称*/
	public String Name = null;
	
	/**管理机构地址*/
	public String Address = null;
	
	/**管理机构联系电话*/
	public String Phone = null;
	
	/**是否开业标识*/
	public String Sign = null;

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getSign() {
		return Sign;
	}

	public void setSign(String sign) {
		Sign = sign;
	}

	
}
