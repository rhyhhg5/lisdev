package com.sinosoft.wasforwardxml.project.certify;

import java.util.List;

import org.jdom.Document;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.wasforwardxml.project.certify.input.ManageComQueryTable;
import com.sinosoft.wasforwardxml.project.certify.output.ManageComTable;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
/**
 *  同步管理机构
 * @author 杨阳     2014-10-22
 *
 */
public class CertifyManageCom  extends ABusLogic{
	
	/**查询标识*/
	public String mQueryFlag ="";
	
	/**管理机构编码*/
	public String  mManageCom ="";
	
	/**同步管理机构发送报文——管理机构信息*/
	public ManageComQueryTable mManageComQueryTable;
	
	/**同步管理机构返回报文——管理机构信息*/
	public ManageComTable mManageComTable;
	
	/**处理过程中的错误信息*/
	public String mError = "";
	
	protected boolean deal(MsgCollection cMsgInfos) {
 		System.out.println("开始同步管理机构过程");
		try {
			List tManageComQueryList = cMsgInfos.getBodyByFlag("ManageComQueryTable");
			if (tManageComQueryList == null || tManageComQueryList.size() != 1) {
				errLog("获取管理机构信息失败。");
				return false;
			}
			mManageComQueryTable = (ManageComQueryTable) tManageComQueryList.get(0); // 获取保单信息，只有一个保单
			mQueryFlag = mManageComQueryTable.getQueryFlag();
			mManageCom = mManageComQueryTable.getManageCom();
			String sql = "select count from LDCom where 1=1  and ComCode like '8641%' ";
			if("01".equals(mQueryFlag))
			{
				sql+=" and ComCode='"+mManageCom+"'";
			}
			sql+=" with ur";
			String count = new ExeSQL().getOneValue(sql);
			if (Integer.parseInt(count)<=0) {
				errLog("管理机构不存在");
				return false;
			}
			if (!PrepareInfo()) {
				errLog(mError);
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	// 获取保单信息
	public boolean PrepareInfo() {

		String sql = "select comcode,Name,Address,Phone,  (case when Sign = '1' then '0' else '1' end) from ldcom where 1=1 and ComCode like '8641%' ";
		if("01".equals(mQueryFlag))
		{
			sql+=" and ComCode='"+mManageCom+"'";
		}
		sql+="  with ur";
		SSRS tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
			mError = "获取管理机构信息失败！";
			return false;
		} else {
			for(int i = 1; i<=tSSRS.MaxRow;i++)
			{
				mManageComTable = new ManageComTable();
				mManageComTable.setManageCom(tSSRS.GetText(i, 1));
				mManageComTable.setName(tSSRS.GetText(i, 2));
				mManageComTable.setAddress(tSSRS.GetText(i, 3));
				mManageComTable.setPhone(tSSRS.GetText(i, 4));
				mManageComTable.setSign(tSSRS.GetText(i, 5));
				//返回报文
				putResult("ManageComTable", mManageComTable);
			}
		}
		return true;

	}

	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		// TODO Auto-generated method stub
		return false;
	}
}
