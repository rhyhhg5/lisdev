package com.sinosoft.wasforwardxml.project.shybk.obj;

import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class OutputStatusInform extends BaseXmlSch{

	private static final long serialVersionUID = 1L;
	
	/** 印刷号*/
    private String PrtNo = null;
	/** 保单号*/
    private String ContNo = null;
    /** 险种代码*/
    private String RiskCode = null;
    /** 保障开始时间*/
    private String CValiDate = null;
    /** 保障终止时间*/
    private String CInValiDate = null;
    /** 承保保费*/
    private String Prem = null;
    /** 保额*/
    private String Amnt = null;
    /** 档次*/
    private String Mult = null;
    /** 签单日期*/
    private String SignDate = null;
    /** 销售渠道*/
    private String SaleChnl = null;
    /** 业务员编码*/
    private String AgentCode = null;
    /** 业务员姓名*/
    private String name = null;
    /** 营销机构编码*/
    private String ManageCom = null;
    /** 营销机构名称*/
    private String McName = null;
    /** 保单状态*/
    private String StateFlag = null;
    /** 医保验证是否成功*/
    private String Medicalifsucess = null;
    /** 医保扣款是否成功*/
    private String DecuctMoney = null;
    /** 医保扣款失败原因*/
    private String Failreason = null;
    /** 扣款方式*/
    private String PayMode = null;
    /** 是否退保*/
    private String IfCancel = null;
    /** 退保金额*/
    private String GetMoney = null;
    /** 退保日期*/
    private String AcceptDate = null;
    /** 退款方式*/
    private String ExPayMode = null;
    /** 2017-05-19新增字段---核保状态*/
    private String UwFlag = null;
    
	public String getPrtNo() {
		return PrtNo;
	}
	public void setPrtNo(String prtNo) {
		PrtNo = prtNo;
	}
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getRiskCode() {
		return RiskCode;
	}
	public void setRiskCode(String riskCode) {
		RiskCode = riskCode;
	}
	public String getCValiDate() {
		return CValiDate;
	}
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}
	public String getCInValiDate() {
		return CInValiDate;
	}
	public void setCInValiDate(String cInValiDate) {
		CInValiDate = cInValiDate;
	}
	public String getPrem() {
		return Prem;
	}
	public void setPrem(String prem) {
		Prem = prem;
	}
	public String getAmnt() {
		return Amnt;
	}
	public void setAmnt(String amnt) {
		Amnt = amnt;
	}
	public String getMult() {
		return Mult;
	}
	public void setMult(String mult) {
		Mult = mult;
	}
	public String getSignDate() {
		return SignDate;
	}
	public void setSignDate(String signDate) {
		SignDate = signDate;
	}
	public String getSaleChnl() {
		return SaleChnl;
	}
	public void setSaleChnl(String saleChnl) {
		SaleChnl = saleChnl;
	}
	public String getAgentCode() {
		return AgentCode;
	}
	public void setAgentCode(String agentCode) {
		AgentCode = agentCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getManageCom() {
		return ManageCom;
	}
	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}
	public String getMcName() {
		return McName;
	}
	public void setMcName(String mcName) {
		McName = mcName;
	}
	public String getStateFlag() {
		return StateFlag;
	}
	public void setStateFlag(String stateFlag) {
		StateFlag = stateFlag;
	}
	public String getMedicalifsucess() {
		return Medicalifsucess;
	}
	public void setMedicalifsucess(String medicalifsucess) {
		Medicalifsucess = medicalifsucess;
	}
	public String getDecuctMoney() {
		return DecuctMoney;
	}
	public void setDecuctMoney(String decuctMoney) {
		DecuctMoney = decuctMoney;
	}
	public String getFailreason() {
		return Failreason;
	}
	public void setFailreason(String failreason) {
		Failreason = failreason;
	}
	public String getPayMode() {
		return PayMode;
	}
	public void setPayMode(String payMode) {
		PayMode = payMode;
	}
	public String getIfCancel() {
		return IfCancel;
	}
	public void setIfCancel(String ifCancel) {
		IfCancel = ifCancel;
	}
	public String getGetMoney() {
		return GetMoney;
	}
	public void setGetMoney(String getMoney) {
		GetMoney = getMoney;
	}
	public String getAcceptDate() {
		return AcceptDate;
	}
	public void setAcceptDate(String acceptDate) {
		AcceptDate = acceptDate;
	}
	public String getExPayMode() {
		return ExPayMode;
	}
	public void setExPayMode(String exPayMode) {
		ExPayMode = exPayMode;
	}
	public String getUwFlag() {
		return UwFlag;
	}
	public void setUwFlag(String uwFlag) {
		UwFlag = uwFlag;
	}
}
