package com.sinosoft.wasforwardxml.project.shybk.tb;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.XMLPathTool;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.project.shybk.obj.OutputStatusInform;
import com.sinosoft.wasforwardxml.xml.ctrl.ABusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

/*
 * 投保状态告知外挂系统
 */
public class YbkStatusInform extends ABusLogic {

	public String BatchNo = "";// 批次号
	public String MsgType = "";// 报文类型
	public String Operator = "";// 操作者
	private String mPrtNo = "";//平台传过来的印刷号
	
	public OutputStatusInform mOpStatusInform = new OutputStatusInform();
	public GlobalInput mGlobalInput = new GlobalInput();// 公共信息
	public String mError = "";// 处理过程中的错误信息
	public String mState = "";// 处理过程中的错误状态
	
	private String mBatchNo = "";
	private Document mDocument;
	private String ParsePath = "/DATASET/CONTENT";
	private String mUrl = "";
	private String mFilePath = "";
	
	private final static String STATE_01 = "01";
	private final static String STATE_02 = "02";//重复投保返回状态
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos, Document cInXmlDoc) {
		System.out.println("---------------start----------------");
		MsgHead tMsgHead = cMsgInfos.getMsgHead();
		mBatchNo = tMsgHead.getBatchNo();
		MsgType = tMsgHead.getMsgType();
		Operator = tMsgHead.getSendOperator();
		mDocument = cInXmlDoc;
		mGlobalInput.Operator = "YBK";
		mGlobalInput.ComCode = "86";
		mGlobalInput.ManageCom = "86310000";
		
		try {
			if (!getSendUrlName()) {
				errLog1(mError);
				return false;
			}
			if (!saveXmlFile()) {
				errLog1(mError);
				return false;
			}
			if (!ParseXml()) {
				errLog3(mState,mError);
				return false;
			}
		}catch (Exception ex) {
				errLog1("发生未知异常");
				return false;
		}
		errLog2("处理成功");
		getXmlResult();
		System.out.println("---------------end---------------");
		return true;
	}

	private boolean getSendUrlName() {
		//TODO 需要增加存放的路径
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PadTBXml'";// 报文的存放路径
	    mUrl = new ExeSQL().getOneValue(sqlurl);
        //本地测试路径
//	    mUrl = "F:/YBK/temp_his";
		System.out.println("报文的存放路径:" + mUrl);// 调试用－－－－－
		if (mUrl == null || mUrl.equals("")) {
			mError = "获取文件存放路径出错";
			return false;
		}
		return true;
	}

	private boolean saveXmlFile() {
		if (!newFolder(mUrl)) {
			mError = "建立文件存放目录出错";
			return false;
		}
		System.out.println("文件存放路径" + mUrl);
		mFilePath = mUrl + "/" + mBatchNo + ".xml";
		XMLOutputter XMLOut = new XMLOutputter();
		try {
			XMLOut.output(mDocument, new FileOutputStream(mFilePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在");
				return true;
			} else {
				myFilePath.mkdir();
				System.out.println("新建目录成功");
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean ParseXml() {
		// 得到保单的传入信息
		XMLPathTool tXPT = new XMLPathTool(mFilePath);
		NodeList nodeList = tXPT.parseN(ParsePath);
		/* 业务处理 */
		try {
			if (!search(nodeList)) {
				//查询失败的也删除报文
				if(!deleteFile(mFilePath)){
					//删除失败不报错，继续返回查询结果
					System.out.println("报文删除失败");
				}
				return false;
			}
			//查询成功删除报文
			if(!deleteFile(mFilePath)){
				//删除失败不报错，继续返回查询结果
				System.out.println("报文删除失败");
			}
    	}catch (Exception ex) {
			errLog1("业务处理失败!");
			mState = STATE_01;
			mError = "业务处理失败!";
			return false;
		}
		return true;
	}
	
	/*
	 * 报文删除
	 * */
	public boolean deleteFile(String filePath){
		File tFile = new File(filePath);
		if (tFile.exists()) {
			if (tFile.isFile()) {
				if (!tFile.delete()) {
					System.out.println("原文件删除错误");
				}
			}
		}
		return true;
	}
	
	/**
     * 业务处理
     * @return boolean
     */
    private boolean search(NodeList nodeList) {
        System.out.println("----------------search()--------------");
        Node pNode = null;
		for (int i = 0; i < nodeList.getLength(); i++) {
			pNode = nodeList.item(i);
			NodeList cNodeList = pNode.getChildNodes();

			Node childrenNode = null;
			String nodeName = "";
			for(int j = 1; j < cNodeList.getLength(); j++){
				childrenNode = cNodeList.item(j);
				nodeName = childrenNode.getNodeName();
				if (nodeName.equals("#text")) {
					continue;
				}
				if (nodeName.equals("PrtNo")) {
					mPrtNo = childrenNode.getFirstChild().getNodeValue();
					System.out.println("查询PrtNo---------"+mPrtNo);
				}
			}
		}
        
		if("".equals(mPrtNo)||mPrtNo.equals(null)||mPrtNo==null){
			mError = "印刷号获取失败";
			mState = STATE_01;
			return false;
		}
		SetPrtNo(mPrtNo);
		String tempfeeno = mPrtNo+"801";
		/*查询b表看是否退保 */
		String execute = "select edorno from lpedoritem where edorno in (select edorno from lbcont where prtno = '"+mPrtNo+"') and edortype in ('CT','WT','XT') with ur";
		SSRS tSSRS=new SSRS();
    	ExeSQL tExeSQL=new ExeSQL();
    	tSSRS=tExeSQL.execSQL(execute);
    	if (tSSRS == null || tSSRS.getMaxRow() <= 0) {
    		System.out.println("--------------未退保--------------");
    		/* 根据印刷号获取所需信息 */
    	    String sql="select lc.contno,lp.riskcode,lc.CValiDate,lc.CInValiDate,lc.prem,lc.amnt,lc.mult,lc.signdate,"
    	    		+" lc.SaleChnl,lc.agentcode,"
    	    		+" (select name from laagent where agentcode=lc.agentcode),"
    	    		+" lp.ManageCom,"
    	    		+" (select name from ldcom where comcode =lp.ManageCom fetch first 1 rows only),"
    	    		+" lc.stateflag,"
    	    		+" (select Medicalifsucess from YBK_G03_LIS_RESPONSEINFO where Policyformerno=(select ybkserialno from lccontsub where prtno='"+mPrtNo+"')),"
    	    		+" (select enteraccdate from ljtempfee where tempfeeno='"+tempfeeno+"'),"
    	    		+" (select Failreason from YBK_G03_LIS_RESPONSEINFO where Policyformerno=(select ybkserialno from lccontsub where prtno='"+mPrtNo+"')),"
    	    		+" lc.paymode,lc.uwflag"
    	    		+" from lccont lc,lcpol lp"
    	    		+" where" 
    	    		+" lc.ContNo=lp.ContNo"
    	    		+" and lc.prtno='"+mPrtNo+"'"
    	    		+" with ur";
    		SSRS tSSRS1=new SSRS();
        	ExeSQL tExeSQL1=new ExeSQL();
        	tSSRS1=tExeSQL1.execSQL(sql);
        	if (tSSRS1 == null || tSSRS1.getMaxRow() <= 0) {
        		String lobSql = " select lcd.deletereason from lobcont lc,lobpol lp,LCDelPolLog lcd where  1=1 "
							  + " and lc.contno = lp.contno "
							  + " and lc.prtno = lcd.prtno "
						      +	" and lc.prtno = '" + mPrtNo + "'"
						      +	" order by lcd.makedate desc,lcd.maketime desc fetch first 1 rows only "
						      + " with ur "; 
        		SSRS tSSRS3 = new ExeSQL().execSQL(lobSql);
        		if(tSSRS3 != null && tSSRS3.getMaxRow() > 0){
        			mError = tSSRS3.GetText(1, 1);
        			mState = STATE_02;
        		} else {
        			String ybkErroListSql = " select errorinfo from ybkerrorlist where 1=1 "
        					+ " and transtype = 'Y01' and businessno = '"+ mPrtNo +"' "
        					+ " and errorinfo in (select codename from ldcode where codetype = 'ybkhxduplicateinfo') "	
        					+ " order by makedate desc,maketime desc fetch first 1 rows only "
        					+ " with ur ";
        			SSRS tSSRS4 = new ExeSQL().execSQL(ybkErroListSql);
        			if(tSSRS4 != null && tSSRS4.getMaxRow() > 0){
        				mError = tSSRS4.GetText(1, 1);
						mState = STATE_02;
        			} else {
        				mError = "数据信息获取失败!";
						mState = STATE_01;
					}       			
        		}       		       		
        		return false;
        	}else{
        		mOpStatusInform.setPrtNo(mPrtNo);
        		mOpStatusInform.setContNo(tSSRS1.GetText(1, 1));
        		mOpStatusInform.setRiskCode(tSSRS1.GetText(1, 2));
        		mOpStatusInform.setCValiDate(tSSRS1.GetText(1, 3));
        		mOpStatusInform.setCInValiDate(tSSRS1.GetText(1, 4));
        		mOpStatusInform.setPrem(tSSRS1.GetText(1, 5));
        		mOpStatusInform.setAmnt(tSSRS1.GetText(1, 6));
        		mOpStatusInform.setMult(tSSRS1.GetText(1, 7));
        		mOpStatusInform.setSignDate(tSSRS1.GetText(1, 8));
        		mOpStatusInform.setSaleChnl(tSSRS1.GetText(1, 9));
        		mOpStatusInform.setAgentCode(tSSRS1.GetText(1, 10));
        		mOpStatusInform.setName(tSSRS1.GetText(1, 11));
        		mOpStatusInform.setManageCom(tSSRS1.GetText(1, 12));
        		mOpStatusInform.setMcName(tSSRS1.GetText(1, 13));
        		mOpStatusInform.setStateFlag(tSSRS1.GetText(1, 14));
        		mOpStatusInform.setMedicalifsucess(tSSRS1.GetText(1, 15));
        		String date = tSSRS1.GetText(1, 16);
        		if(!"".equals(date)&&date!=null){
        			mOpStatusInform.setDecuctMoney("1");
        		}else{
        			mOpStatusInform.setDecuctMoney("0");
        		}
        		mOpStatusInform.setFailreason(tSSRS1.GetText(1, 17));
        		mOpStatusInform.setPayMode(tSSRS1.GetText(1, 18));
        		mOpStatusInform.setIfCancel("0");
        		mOpStatusInform.setExPayMode(tSSRS1.GetText(1, 18));
        		mOpStatusInform.setUwFlag(tSSRS1.GetText(1, 19));
        	}
    	}else{
    		System.out.println("--------------已退保--------------");
    		/* 根据印刷号获取所需信息 */
    	    String sql="select lb.contno,lp.riskcode,lb.CValiDate,lb.CInValiDate,lb.prem,lb.amnt,lb.mult,lb.signdate,"
    	    		+" lb.SaleChnl,lb.agentcode,"
    	    		+" (select name from laagent where agentcode=lb.agentcode),"
    	    		+" lp.ManageCom,"
    	    		+" (select name from ldcom where comcode =lp.ManageCom  fetch first 1 rows only),"
    	    		+" lb.stateflag,"
    	    		+" (select Medicalifsucess from YBK_G03_LIS_RESPONSEINFO where Policyformerno=(select ybkserialno from lccontsub where prtno='"+mPrtNo+"')),"
    	    		+" (select enteraccdate from ljtempfee where tempfeeno='"+tempfeeno+"'),"
    	    		+" (select Failreason from YBK_G03_LIS_RESPONSEINFO where Policyformerno=(select ybkserialno from lccontsub where prtno='"+mPrtNo+"')),"
    	    		+" lb.paymode,"
    	    		+" p.getmoney,"
    	    		+" p.confdate,"
    	    		+" (select paymode from ljaget where otherno=lb.EdorNo),"
    	    		+" lb.uwflag"
    	    		+" from lbcont lb,lbpol lp,lpedorapp p"
    	    		+" where" 
    	    		+" lb.ContNo=lp.ContNo"
    	    		+" and lb.EdorNo=p.EdorAcceptNo"
    	    		+" and lb.prtno='"+mPrtNo+"'"
    	    		+" with ur";
    		SSRS tSSRS2=new SSRS();
        	ExeSQL tExeSQL2=new ExeSQL();
        	tSSRS2=tExeSQL2.execSQL(sql);
        	if (tSSRS2 == null || tSSRS2.getMaxRow() <= 0) {
        		mError = "数据信息获取失败!";
        		mState = STATE_01;
        		return false;
        	}else{
        		mOpStatusInform.setPrtNo(mPrtNo);
        		mOpStatusInform.setContNo(tSSRS2.GetText(1, 1));
        		mOpStatusInform.setRiskCode(tSSRS2.GetText(1, 2));
        		mOpStatusInform.setCValiDate(tSSRS2.GetText(1, 3));
        		mOpStatusInform.setCInValiDate(tSSRS2.GetText(1, 4));
        		mOpStatusInform.setPrem(tSSRS2.GetText(1, 5));
        		mOpStatusInform.setAmnt(tSSRS2.GetText(1, 6));
        		mOpStatusInform.setMult(tSSRS2.GetText(1, 7));
        		mOpStatusInform.setSignDate(tSSRS2.GetText(1, 8));
        		mOpStatusInform.setSaleChnl(tSSRS2.GetText(1, 9));
        		mOpStatusInform.setAgentCode(tSSRS2.GetText(1, 10));
        		mOpStatusInform.setName(tSSRS2.GetText(1, 11));
        		mOpStatusInform.setManageCom(tSSRS2.GetText(1, 12));
        		mOpStatusInform.setMcName(tSSRS2.GetText(1, 13));
        		mOpStatusInform.setStateFlag(tSSRS2.GetText(1, 14));
        		mOpStatusInform.setMedicalifsucess(tSSRS2.GetText(1, 15));
        		String date = tSSRS2.GetText(1, 16);
        		if(!"".equals(date)&&date!=null){
        			mOpStatusInform.setDecuctMoney("1");
        		}else{
        			mOpStatusInform.setDecuctMoney("0");
        		}
        		mOpStatusInform.setFailreason(tSSRS2.GetText(1, 17));
        		mOpStatusInform.setPayMode(tSSRS2.GetText(1, 18));
    			mOpStatusInform.setIfCancel("1");
    			mOpStatusInform.setGetMoney(tSSRS2.GetText(1, 19));
        		mOpStatusInform.setAcceptDate(tSSRS2.GetText(1, 20));
        		mOpStatusInform.setExPayMode(tSSRS2.GetText(1, 21));
        		mOpStatusInform.setUwFlag(tSSRS2.GetText(1, 22));
        	}
    	}
        return true;
    }
	
	@Override
	protected boolean deal(MsgCollection cMsgInfos) {
		return true;
	}
	// 返回报文
	public void getXmlResult() {
		// 返回数据节点
		putResult("OutputDataTable", mOpStatusInform);
	}
}
