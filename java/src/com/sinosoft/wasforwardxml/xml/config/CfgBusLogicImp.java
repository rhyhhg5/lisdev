/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.config;

import java.io.InputStream;
import java.util.Properties;

public final class CfgBusLogicImp extends ACfgProp
{
    private static CfgBusLogicImp mInstance = null;

    private Properties mProp = null;

    private CfgBusLogicImp()
    {
        loadCfgProp();
    }

    private void loadCfgProp()
    {
        mLogger.info("加载属性文件 BusLogicConfig.properties");

        String tPropName = "BusLogicConfig.properties";
        try
        {
            Properties prop = new Properties();
            InputStream tIs = null;
            tIs = CfgBusLogicImp.class.getResourceAsStream("/" + tPropName);
            if (tIs == null)
            {
                tIs = CfgBusLogicImp.class.getResourceAsStream(tPropName);
            }
            if (tIs != null)
            {
                prop.load(tIs);
                //prop.list(System.out);
                mProp = prop;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mLogger.error("加载属性文件 BusLogicConfig.properties 时，出现未知异常。");
        }

        mLogger.info("属性文件加载完毕。");
    }

    public static ICfgProp getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new CfgBusLogicImp();
        }
        return mInstance;
    }

    public String getProperty(String cPropKey)
    {
        if (mProp == null)
        {
            return null;
        }
        String tPropValue = mProp.getProperty(cPropKey);
        tPropValue = tPropValue != null ? tPropValue.trim() : null;
        return tPropValue;
    }

    public void cleanCache()
    {
        mInstance = null;
    }

}
