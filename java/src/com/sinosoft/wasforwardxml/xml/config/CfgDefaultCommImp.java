/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.config;

import java.io.InputStream;
import java.util.Properties;

public class CfgDefaultCommImp extends ACfgProp
{
    private Properties mProp = null;

    public CfgDefaultCommImp(String cPropName) throws Exception
    {
        loadCfgProp(cPropName);
    }

    private void loadCfgProp(String cPropName) throws Exception
    {
        if (cPropName == null || cPropName.equals(""))
        {
            mLogger.info("属性文件名称为空，不进行加载");
            throw new Exception("属性文件名称为空，不进行加载");
        }

        String tPropName = cPropName;

        mLogger.info("加载属性文件[" + cPropName + "]");

        try
        {
            Properties prop = new Properties();
            InputStream tIs = null;
            tIs = CfgDefaultCommImp.class.getResourceAsStream("/" + tPropName);
            if (tIs == null)
            {
                tIs = CfgDefaultCommImp.class.getResourceAsStream(tPropName);
            }
            if (tIs != null)
            {
                prop.load(tIs);
                mProp = prop;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            mLogger.error("加载属性文件[" + cPropName + "]时，出现未知异常。");
        }

        mLogger.info("属性文件加载完毕。");
    }

    public void cleanCache()
    {
        return;
    }

    public String getProperty(String cPropKey)
    {
        if (mProp == null)
        {
            return null;
        }
        String tPropValue = mProp.getProperty(cPropKey);
        tPropValue = tPropValue != null ? tPropValue.trim() : null;
        return tPropValue;
    }

}
