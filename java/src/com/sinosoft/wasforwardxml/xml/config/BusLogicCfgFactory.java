/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.config;

import org.apache.log4j.Logger;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.xml.ctrl.IBusLogic;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;

public final class BusLogicCfgFactory
{
    private final static Logger mLogger = Logger.getLogger(BusLogicCfgFactory.class);

    private BusLogicCfgFactory()
    {
    }

    public static IBusLogic getNewInstanceBusLogic(MsgCollection cMsgInfos)
    {
    	MsgHead tMsgHead = cMsgInfos.getMsgHead();
        System.out.println("tMsgHead="+tMsgHead);
        if (tMsgHead == null)
        {
            mLogger.error("获取报头信息失败。");
            return null;
        }

        String tBusLogicFlag = tMsgHead.getMsgType();
        System.out.println("tBusLogicFlag="+tBusLogicFlag);
        String tClassSQL = "select codename from ldcode where codetype = 'msgtypeclass' and code = '"+tBusLogicFlag+"' ";
        String tBusLogicSeriveClass = new ExeSQL().getOneValue(tClassSQL);
        System.out.println("tBusLogicSeriveClass="+tBusLogicSeriveClass);
        if (tBusLogicSeriveClass == null)
        {
        	System.out.println("tBusLogicSeriveClass="+tBusLogicSeriveClass);
            return null;
        }

        IBusLogic tBusLogic = null;
        try
        {
            tBusLogic = (IBusLogic) Class.forName(tBusLogicSeriveClass).newInstance();
        }
        catch (Exception ex)
        {
        	System.out.println("ex");
            ex.printStackTrace();
            return null;
        }

        return tBusLogic;
    }
}
