/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.config;

import org.apache.log4j.Logger;

public final class CfgFactory
{
    private final static Logger mLogger = Logger.getLogger(CfgFactory.class.getName());

    private CfgFactory()
    {
    }

    public static ICfgProp getConfigProperties(String cCfgType)
    {

        ICfgProp tICfgProp = null;

        if ("CfgBusLogicCode".equals(cCfgType))
        {
            tICfgProp = CfgBusLogicImp.getInstance();
            return tICfgProp;
        }

        try
        {
            if (cCfgType == null || cCfgType.equals(""))
            {
                mLogger.error("配置文件名称为空。");
                return null;
            }

            String tPropName = cCfgType + ".properties";
            tICfgProp = new CfgDefaultCommImp(tPropName);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return tICfgProp;
    }
}
