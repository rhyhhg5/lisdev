/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.config;

import org.apache.log4j.Logger;

public abstract class ACfgProp implements ICfgProp
{
    protected Logger mLogger = Logger.getLogger(getClass().getName());

    public abstract String getProperty(String cPropKey);

    public abstract void cleanCache();
}
