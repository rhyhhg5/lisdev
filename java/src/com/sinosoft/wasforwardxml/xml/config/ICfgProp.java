/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.config;

public interface ICfgProp
{
    public String getProperty(String cPropKey);

    public void cleanCache();
}
