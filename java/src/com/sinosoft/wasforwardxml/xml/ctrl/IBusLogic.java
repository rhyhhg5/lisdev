/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.ctrl;

import org.jdom.Document;

public interface IBusLogic
{
    public MsgCollection service(MsgCollection cMsgInfos);
    
    public MsgCollection service(MsgCollection cMsgInfos,Document cInXmlDoc);
    //上海医保用
    public MsgCollection service1(MsgCollection cMsgInfos,Document cInXmlDoc);
}
