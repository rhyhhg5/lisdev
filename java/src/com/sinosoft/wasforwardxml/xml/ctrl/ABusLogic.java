/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.ctrl;

import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.YBKErrorListSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.obj.MsgResHead;
import com.sinosoft.wasforwardxml.util.MsgUtil;
import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public abstract class ABusLogic implements IBusLogic
{
    protected Logger mLogger = Logger.getLogger(getClass().getName());

    protected MsgCollection mResMsgCols = null;
    protected String mPrtNo;
	protected MMap map = new MMap();
	protected VData mLogMsg = new VData();
	protected CErrors mErrors = new CErrors();

    public ABusLogic()
    {
        mLogger.info("start:" + getClass().getName());
    }

    public final MsgCollection service(MsgCollection cMsgInfos)
    {
        mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
        if (!deal(cMsgInfos))
        {
            MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            if (tResMsgHead.getErrCode() == null)
            {
                errLog("逻辑处理中出现，未知异常");
            }
        }
        else
        {
            MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            tResMsgHead.setState("00");
        }
        return getResult();
    }

    protected final void errLog(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);

        String tErrCode = "99999999";
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
        tResMsgHead.setState("01");
        tResMsgHead.setErrCode(tErrCode);
        tResMsgHead.setErrInfo(cErrInfo);
    }

   
    protected MsgCollection getResult()
    {
        return mResMsgCols;
    }

    protected final void putResult(String cNodeFlag, BaseXmlSch cBxsch)
    {
        mResMsgCols.setBodyByFlag(cNodeFlag, cBxsch);
    }
    
    public final MsgCollection service(MsgCollection cMsgInfos,Document cInXmlDoc)
    {
        mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
        
        if (!deal(cMsgInfos,cInXmlDoc))
        {MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            if (tResMsgHead.getErrCode() == null)
            {
                errLog("逻辑处理中出现，未知异常");
            }
        }
        else
        {
        	MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
            tResMsgHead.setState("00");
        }
        return getResult();
    }
    
    //上海医保专用
    public final MsgCollection service1(MsgCollection cMsgInfos,Document cInXmlDoc)
    {
        mResMsgCols = MsgUtil.getResMsgCol(cMsgInfos);
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
        if (!deal(cMsgInfos,cInXmlDoc))
        {
            if (tResMsgHead.getErrCode() == null)
            {
                errLog("逻辑处理中出现，未知异常");
            }
        }
        else
        {
            
            tResMsgHead.setState("00");
        }
        tResMsgHead.setBranchCode(PubFun1.CreateMaxNo("SEQ_YBK_BATCHNO", 20));
		saveLogMsg(tResMsgHead);
        return getResult();
    }
    
    
    protected final boolean saveLogMsg(MsgResHead cResMsgHead){
		YBKErrorListSchema tYBKErrorListSchema = new YBKErrorListSchema();
		tYBKErrorListSchema.setSerialNo(cResMsgHead.getBranchCode());
		tYBKErrorListSchema.setTransType(cResMsgHead.getMsgType());
		tYBKErrorListSchema.setBusinessNo(mPrtNo);
		tYBKErrorListSchema.setResponseCode(cResMsgHead.getErrCode());
		tYBKErrorListSchema.setErrorInfo(cResMsgHead.getErrInfo());
		tYBKErrorListSchema.setMakeDate(PubFun.getCurrentDate());
		tYBKErrorListSchema.setMakeTime(PubFun.getCurrentTime());
		tYBKErrorListSchema.setModifyDate(PubFun.getCurrentDate());
		tYBKErrorListSchema.setModifyTime(PubFun.getCurrentTime());
		map.put(tYBKErrorListSchema, "INSERT");
		mLogMsg.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mLogMsg, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}
    protected final void SetPrtNo(String PrtNo){
		this.mPrtNo = PrtNo;
	}
    
    protected final void errLog1(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);
        //错误编码
        String tErrCode = "9999";
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
        //错误返回01
        tResMsgHead.setState("01");
        tResMsgHead.setErrCode(tErrCode);
        tResMsgHead.setErrInfo(cErrInfo);
    }
    protected final void errLog2(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);
        //正确编码
        String tErrCode = "0000";
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();     
        tResMsgHead.setErrCode(tErrCode);
        tResMsgHead.setErrInfo(cErrInfo);
    }
    protected final void errLog3(String cState,String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);
        //错误编码
        String tErrCode = "9999";
        MsgResHead tResMsgHead = mResMsgCols.getMsgResHead();
        //重复投保返回02
        tResMsgHead.setState(cState);
        tResMsgHead.setErrCode(tErrCode);
        tResMsgHead.setErrInfo(cErrInfo);
    }
    protected abstract boolean deal(MsgCollection cMsgInfos);
    protected abstract boolean deal(MsgCollection cMsgInfos,Document cInXmlDoc);
}
