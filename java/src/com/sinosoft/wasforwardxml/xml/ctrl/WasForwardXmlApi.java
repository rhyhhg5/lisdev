/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.ctrl;

import org.jdom.Document;

import com.sinosoft.wasforwardxml.xml.config.BusLogicCfgFactory;
import com.sinosoft.wasforwardxml.xml.ctrl.pack.MsgXmlParse;
import com.sinosoft.wasforwardxml.xml.ctrl.pack.MsgXschParse;

public class WasForwardXmlApi {
	public Document deal(Document cInXmlDoc) {
		MsgCollection tInMsgInfos = null;
		MsgCollection tOutMsgInfos = null;

		// 将报文UnPack，并解析xml数据，自动封装报文对象集 MsgCollection
		MsgXmlParse tMsgXParse = new MsgXmlParse();
		System.out.println("执行WasForwardXmlApi的tMsgXParse.deal方法");
		tInMsgInfos = tMsgXParse.deal(cInXmlDoc);
		// --------------------

		// 业务处理
		System.out.println("执行WasForwardXmlApi方法");
		System.out.println("报文格式如下：");
		System.out.println(tInMsgInfos.toString());
		try {
			IBusLogic tBusLogic = BusLogicCfgFactory.getNewInstanceBusLogic(tInMsgInfos);
			if (tBusLogic == null) {
				System.out.println("程序异常，加载业务处理类失败。");
				return null;
			}
			System.out.println("实例化tBusLogic完成:" + tInMsgInfos.getMsgHead().getMsgType());
			if ("PADTB02".equals(tInMsgInfos.getMsgHead().getMsgType())
					|| "PADTB02OP".equals(tInMsgInfos.getMsgHead().getMsgType())) {
				tOutMsgInfos = tBusLogic.service(tInMsgInfos, cInXmlDoc);
			} else if ("Y01".equals(tInMsgInfos.getMsgHead().getMsgType())) {
				tOutMsgInfos = tBusLogic.service1(tInMsgInfos, cInXmlDoc);
			} else if ("S01".equals(tInMsgInfos.getMsgHead().getMsgType())) {
				tOutMsgInfos = tBusLogic.service1(tInMsgInfos, cInXmlDoc);
			} else {
				tOutMsgInfos = tBusLogic.service(tInMsgInfos);
			}

		} catch (Exception e) {
			System.out.println("异常了");
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			return null;
		}

		// --------------------

		// 将返回报文对象集Pack，并转换返回xml数据。
		Document tOutDoc = null;
		MsgXschParse tMsgXschParse = new MsgXschParse();
		tOutDoc = tMsgXschParse.deal(tOutMsgInfos);
		// --------------------

		return tOutDoc;
	}

}
