/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.ctrl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.obj.MsgResHead;
import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public final class MsgCollection
{
    private final static Logger mLogger = Logger.getLogger(MsgCollection.class);

    /** 报文头 */
    private MsgHead mMsgHead = null;

    /** 反馈报文头 */
    private MsgResHead mMsgResHead = null;

    /** 节点信息索引 */
    private List mMsgBodyIdx = null;

    /** 节点信息清单 */
    private Map mMsgBodyMapping = null;

    public MsgCollection()
    {
        clean();
    }

    public void clean()
    {
        mMsgHead = null;
        mMsgResHead = null;
        mMsgBodyIdx = new ArrayList();
        mMsgBodyMapping = new HashMap();
    }

    public void setMsgHead(MsgHead cMsgHead)
    {
        mMsgHead = cMsgHead;
    }

    public MsgHead getMsgHead()
    {
        return mMsgHead;
    }

    public void setMsgResHead(MsgResHead cMsgResHead)
    {
        mMsgResHead = cMsgResHead;
    }

    public MsgResHead getMsgResHead()
    {
        return mMsgResHead;
    }

    public void setBodyByFlag(String cNodeFlag, BaseXmlSch cBxsch)
    {
        List tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);
        if (tCurPos == -1)
        {
            tCurPos = mMsgBodyIdx.size();
            tCurNode = new ArrayList();
            mMsgBodyIdx.add(cNodeFlag);
            mMsgBodyMapping.put(new Integer(tCurPos), tCurNode);
        }
        else
        {
            tCurNode = (List) mMsgBodyMapping.get(new Integer(tCurPos));
        }

        if (tCurNode == null)
            mLogger.error("获取报文中节点信息数据异常。");

        tCurNode.add(cBxsch);
    }

    public List getBodyByFlag(String cNodeFlag)
    {
        List tCurNode = null;
        int tCurPos = -1;

        tCurPos = mMsgBodyIdx.indexOf(cNodeFlag);

        if (tCurPos != -1)
        {
            tCurNode = (List) mMsgBodyMapping.get(new Integer(tCurPos));
        }

        return tCurNode;
    }

    public List getBodyIdxList()
    {
        return mMsgBodyIdx;
    }
}
