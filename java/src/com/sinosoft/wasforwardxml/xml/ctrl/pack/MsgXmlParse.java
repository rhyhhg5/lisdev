/**
 * 
 * webservice接口，核心对外（前置机）
 * @author gzh
 * @date 2014-06-09
 *
 */
package com.sinosoft.wasforwardxml.xml.ctrl.pack;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.wasforwardxml.obj.MsgHead;
import com.sinosoft.wasforwardxml.util.XschUtil;
import com.sinosoft.wasforwardxml.xml.ctrl.MsgCollection;
import com.sinosoft.wasforwardxml.xml.xsch.BaseXmlSch;

public class MsgXmlParse {
	private final static Logger mLogger = Logger.getLogger(MsgXmlParse.class);

	private String mMsgType = null;

	// private Document mXmlDoc = null;

	public MsgCollection deal(Document cInXmlDoc) {
		MsgCollection tMsgCols = new MsgCollection();

		Document tXmlDoc = (Document) cInXmlDoc.clone();
		Element tEleRoot = tXmlDoc.getRootElement();

		// 处理报头
		if (!loadMsgHead(tEleRoot, tMsgCols)) {
			return null;
		}
		// 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
		if (!"PADTB02".equals(mMsgType) && !"Y01".equals(mMsgType) && !"S01".equals(mMsgType)
				&& !"PADTB02OP".equals(mMsgType)) {
			if (!loadBodyNotes(tEleRoot, tMsgCols, mMsgType)) {
				mLogger.info("部分节点不存在对应对象信息，或Xsch自动加载失败。");
			}
		}

		// --------------------

		return tMsgCols;
	}

	/**
	 * 自动加载报头信息。 <br />
	 * 报头信息有且仅有一个。 <br />
	 * 注：报头加载成功后，自动删除报头，用于后续报文主体信息的相关加载。
	 * 
	 * @param cEleRoot
	 * @param cMsgInfo
	 * @return
	 */
	private boolean loadMsgHead(Element cEleRoot, MsgCollection cMsgInfo) {
		String MsgHead_Name = "MsgHead";
		Element[] tEleMsgHead = getNodeByName(cEleRoot, MsgHead_Name);
		if (tEleMsgHead == null || tEleMsgHead.length != 1) {
			mLogger.error("报文头信息不存在或与约定不符，报头信息应该有且仅有一个。");
			return false;
		}

		for (int i = 0; i < tEleMsgHead.length; i++) {
			Element tEleNode = tEleMsgHead[i];
			MsgHead tBXsch = (MsgHead) XschUtil.swapX2Sch(tEleNode, "MsgHead");
			if (tBXsch == null) {
				return false;
			}
			mMsgType = tBXsch.getMsgType();
			cMsgInfo.setMsgHead(tBXsch);
		}

		cEleRoot.removeChild(MsgHead_Name);

		return true;
	}

	private boolean loadBodyNotes(Element cEleRoot, MsgCollection cMsgInfo, String cMsgType) {
		boolean tResFlag = true;

		String tPackage_Name = "com.sinosoft.wasforwardxml.obj";

		if (mMsgType.substring(0, 3).equals("PAD") || mMsgType.equals("QUERYSTATE") || mMsgType.substring(0, 6).equals("GRPPAD")) {
			tPackage_Name = "com.sinosoft.wasforwardxml.project.pad.obj";
		} else if (mMsgType.substring(0, 3).equals("Y01") || mMsgType.equals("QUERYSTATE")) {
			tPackage_Name = "com.sinosoft.wasforwardxml.project.shybk.obj";
		} else if (mMsgType.substring(0, 7).equals("Certify")) {
			tPackage_Name = "com.sinosoft.wasforwardxml.project.certify.input";
		}
		/**
		 * add by liyt 2016-06-29 发票回写package_Name
		 */
		else if (mMsgType.equals("BILLHX01")) {
			tPackage_Name = "com.sinosoft.lis.ygz";
		}
		List tFieldList = cEleRoot.getChildren();
		for (int idx = 0; idx < tFieldList.size(); idx++) {
			Object tXSNode = tFieldList.get(idx);
			if (!(tXSNode instanceof Element))
				continue;
			if ((((Element) tXSNode).getChildren()).size() == 0)
				continue;

			Element tEleSubNote = (Element) tXSNode;
			String tNoteName = tEleSubNote.getName();

			Element[] tEleMsgNode = getNodeByName(cEleRoot, tNoteName);
			if (tEleMsgNode == null) {
				tResFlag = false;
				mLogger.error("报文数据节点[" + tNoteName + "]未找到相关xml数据信息。");
				continue;
			}

			for (int i = 0; i < tEleMsgNode.length; i++) {
				Element tEleNode = tEleMsgNode[i];
				BaseXmlSch tBXsch = (BaseXmlSch) XschUtil.swapX2Sch(tEleNode, tNoteName, tPackage_Name);
				if (tBXsch == null) {
					tResFlag = false;
					mLogger.error("报文数据节点[" + tNoteName + "]未找到对应Xsch对象，或装载时失败。");
					break;
				}
				cMsgInfo.setBodyByFlag(tNoteName, tBXsch);
			}
		}

		return tResFlag;
	}

	private Element[] getNodeByName(Element cEleNodeList, String cNodeName) {
		String tSubNodeFlag = "Item";
		Element[] tResNode = null;

		Element tEleCurNode = cEleNodeList.getChild(cNodeName);
		if (tEleCurNode == null) {
			mLogger.error("获取节点[" + cNodeName + "]信息失败。");
			return null;
		}

		List tCurNodeList = tEleCurNode.getChildren(tSubNodeFlag);
		if (tCurNodeList == null || tCurNodeList.size() == 0) {
			mLogger.error("获取节点内容（Item）清单信息失败。");
			return null;
		}

		tResNode = new Element[tCurNodeList.size()];
		for (int i = 0; i < tCurNodeList.size(); i++) {
			tResNode[i] = (Element) tCurNodeList.get(i);
		}

		return tResNode;
	}
}
