package com.sinosoft.jkxpt.datacatch;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>
 * Title:健康险平台数据采集类 Description:
 * 
 * Company: 中科软科技股份有限公司
 * 
 * @author : czl
 * @date:2010-12-17
 * @version 1.0
 */
public class InterReportUI {
	public CErrors mErrors = new CErrors();
	//传来的参数
	public String mStartDate = "";
	public String mEndDate = "";
	public GlobalInput tG = null;
	//先后传递的参数
	public VData mResult = new VData();
	public InterReportUI() {
	}
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			System.out.println("*********InterReportUI**********");
			tG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0); 
			TransferData inputTransfer = (TransferData) cInputData.getObjectByObjectName("TransferData",0); 
			mResult.add(tG);
			mResult.add(inputTransfer);
			InterReportBL tInterReportBL = new InterReportBL();
			if (!tInterReportBL.submitData(mResult, "")) {
				if (tInterReportBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tInterReportBL.mErrors);
					return false;
				} else {
					buildError("submitData","InterReportUI.submitData出错");
					return false;
				}
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "InterReportUI";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}
	private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "InterReportUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
