package com.sinosoft.jkxpt.datacatch;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.xreport.dl.BufferDataSourceImpl;
import com.sinosoft.lis.schema.ITImportDataCatchSchema;
import com.sinosoft.lis.vschema.ITImportDataCatchSet;
import com.sinosoft.lis.db.ITImportDataCatchDB;

/**
 * 数据抓取及发送页面调用，按顺序调用承保、保全、理赔DTO处理类，抓取数据，生成DTO，发送，返回结果
 * @author xuzy
 *
 */
public class ImportDataCatchBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData;
	public String mStartDate = "";
	public String mEndDate = "";
	public String mManageCom = "";
	public String mCatchType = "";
	public String mResultType = "02";  //01-成功  02-失败
	public String mCatchFlag = ""; //01--提数  02--补提  03--重提
	public String mUploadSNO ="";
	public String mEorrorLog = "";
	
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 全局数据 */
	private GlobalInput tG = null;
	private MMap map = new MMap();
	public ImportDataCatchBL() {
	}
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			//获取时间、Session等参数
			if (!getInputData(cInputData)) {
				return false;
			}
			if(!dealData()){
				return false;
			}	
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "HealthDataCatchUI";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}finally{
        	try{
        		//dealCatchData();
        	}catch(Exception ex){
        		ex.printStackTrace();
        	}
	    }
		
		return true;
	}
	/**
	 * 获得传入参数
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		tG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		TransferData inputTransfer = (TransferData) cInputData.getObjectByObjectName("TransferData",0); 
		mStartDate = (String) inputTransfer.getValueByName("StartDate");
		mEndDate = (String) inputTransfer.getValueByName("EndDate");
		mManageCom = (String) inputTransfer.getValueByName("ManageCom");
		mCatchType = (String) inputTransfer.getValueByName("CatchType");
		mCatchFlag = (String) inputTransfer.getValueByName("CatchFlag");
		if(mCatchFlag == null||mCatchFlag.equals("")||mCatchFlag.equals("null"))
		{
			mCatchFlag = "01";
		}
		else if (!mCatchFlag.equals("01")|| !mCatchFlag.equals("02"))
		{
			return false;
		}

		return true;
	}
	/**
	 * 解析targetKey和targetValue，查询LFDesbModeSet
	 * @return
	 */
	private boolean dealData(){

		String sql="select * from itimportdatacatch";
		ITImportDataCatchDB tITImportDataCatchDB=new ITImportDataCatchDB();
		ITImportDataCatchSet tITImportDataCatchSet = tITImportDataCatchDB.executeQuery(sql);
		for(int i=1;i<=tITImportDataCatchSet.size();i++){
			ITImportDataCatchSchema tITImportDataCatchSchema=tITImportDataCatchSet.get(i);
			String telementName = tITImportDataCatchSchema.getTELEMENTNAME();
			String texName = tITImportDataCatchSchema.getCALSQL4();  //获取文件名
			String catchSQL="select calsql1||calsql2||calsql3||calcondition from ITImportDataCatch " 
					+" where telementname='"+telementName+"' ";
			String oneValue = new ExeSQL().getOneValue(catchSQL);
			PubCalculator pc=new PubCalculator();
			pc.addBasicFactor("StartDate", mStartDate);
			pc.addBasicFactor("EndDate", mEndDate);
			pc.setCalSql(oneValue);
			String newSQL = pc.calculateEx();
			SSRS st = new  ExeSQL().execSQL(newSQL);
			List list=new ArrayList();
			int maxCol = st.getMaxCol();
			int maxRow = st.getMaxRow();
			String info="|";
			for(int j=1;j<=maxRow;j++){
				String[] rowData = st.getRowData(j);
				
				for(int m=0;m<maxCol;m++){
					if(m!=maxCol-1){
					info=info+rowData[m]+"|,|";
					}
					else {
					info=info+rowData[m]+"|";
					}
					
				}
				list.add(info);
				System.out.println(info);
				info="|";
			}
	
			
			if(!commitData(list,telementName,texName)){
				return false;
			}
		}
		
		return true;
	}
	
	
	//将获取到的数据写入到硬盘里
	private boolean commitData(List list,String telementname,String texName)
	{
		System.out.println("telementname               "+telementname);
		System.out.println("texName               "+texName);
		File file=null;
		if(telementname.equals("")||telementname!=null){
		 file = new File("D:/out/"+texName.trim()+".txt");
		}
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		BufferedWriter br = null;
			try {
				 br = new BufferedWriter(new FileWriter(file));
				for(int i=0;i<list.size();i++){
					String value=(String)list.get(i);
					br.write(value);
					br.newLine();
					br.flush();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			}
		finally{
			try {
				if(br!=null){
				br.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
        return true;
	}
	
	/**
	 * 生成错误信息
	 * @param szFunc	报错方法名
	 * @param szErrMsg	错误信息
	 */
	private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "HealthDataCathUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	private boolean checkInputData() {
		return true;
	}
	public static void main(String[] args) {
		ImportDataCatchBL t = new ImportDataCatchBL();
		
		t.mStartDate="2011-8-1";
		t.mEndDate="2011-9-1";
	
		t.dealData();
	}
}
