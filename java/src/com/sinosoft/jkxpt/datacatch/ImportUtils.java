package com.sinosoft.jkxpt.datacatch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.CUSTOMERRESULTSchema;
import com.sinosoft.utility.VData;

public class ImportUtils {

	public boolean importData(File file){
		if(!file.exists()){
			return false;
		}
		BufferedReader br = null;
		try {
			 br =new BufferedReader(new FileReader(file));
			try {
				String line = null;
				while((line=br.readLine())!=null){
					//System.out.println(line);
					String[] splitLine = line.split(",");
					CUSTOMERRESULTSchema tCUSTOMERRESULTSchema = new CUSTOMERRESULTSchema();
					//tImpIccustomerResult_ArraySchema.setCompanyCode(splitLine[0].substring(1, splitLine[0].length()-1));
					List list =new ArrayList();
					for(int i=0;i<splitLine.length;i++){
						String temp = splitLine[i].substring(1, splitLine[i].length()-1);
						list.add(temp);
					}
					try{
						tCUSTOMERRESULTSchema.setCompanyCode(list.get(0).toString());
						tCUSTOMERRESULTSchema.setName(list.get(1).toString());
						tCUSTOMERRESULTSchema.setGender(list.get(2).toString());
						tCUSTOMERRESULTSchema.setBirthday(list.get(3).toString());
						tCUSTOMERRESULTSchema.setCritType(list.get(4).toString());
						tCUSTOMERRESULTSchema.setCritCode(list.get(5).toString());
						tCUSTOMERRESULTSchema.setCustomerno(list.get(6).toString());
						tCUSTOMERRESULTSchema.setCustomer_sequence_no(list.get(7).toString());
						tCUSTOMERRESULTSchema.setErrorMessage(list.get(8).toString());
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("截取字符与数据库不匹配");
					}
					if(!commitData( tCUSTOMERRESULTSchema)){
						return false;
					}
				}
				//br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(br!=null){
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return true;
	}
	public boolean commitData(CUSTOMERRESULTSchema tCUSTOMERRESULTSchema){
		
		VData data = new VData();
		MMap map = new MMap();
	    map.put(tCUSTOMERRESULTSchema, "DELETE&INSERT");
	    data.add(map);
	    PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
           System.out.println("提交数据库失败");
            return false;
        }
		return true;
	}
	public static void main(String[] args) {
		ImportUtils util =new ImportUtils();
		util.importData(new File("D:/input/456.txt"));
	}
}
