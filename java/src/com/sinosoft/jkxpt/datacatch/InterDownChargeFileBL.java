package com.sinosoft.jkxpt.datacatch;

import java.io.*;
import java.util.*;
import java.io.File.*;
import java.net.SocketException;

import org.apache.tools.ant.taskdefs.Delete;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;




/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class InterDownChargeFileBL  {
	 private String interFilePath = "";
	    private String mLogDate="";
	    private String mToday = PubFun.getCurrentDate();
	    private String mTime = PubFun.getCurrentTime();
	    private MMap mMap = new MMap();
	    private VData mResult = new VData();
	    private String CrmPath = "";
	    private String mFileName="";
	    private ExeSQL mExeSQL = new ExeSQL();
	    private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);
	    private String mIP = "";
		private String mUserName = "";
		private String mPassword = "";
		private int mPort=21;
		private String mTransMode = "";
		private String mURL = "";
		public CErrors mErrors = new CErrors();    
    public InterDownChargeFileBL() { 
    }
    
    public boolean submitData(VData cInpuptData,String cOpearate)
    {
		try {
			//获取时间、Session等参数
			if (!getInputData()) {
				return false;
			}
			if(!dealData()){
				return false;
			}	
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "InterReportBL";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}finally{
        	try{
        		//dealCatchData();
        	}catch(Exception ex){
        		ex.printStackTrace();
        	}
	    }
    	return true;
    }
    public boolean dealData(){
 // 进行业务处理
       try{
    	   if (!readTextDate()){
    		   return false;
    	   }
      }catch(Exception ex){
    	  System.out.println("readTextDate END With Exception...");
    	  ex.printStackTrace();
    	  return false;
      }
    	 return true;
    }
    
    
    
    private boolean getInputData()
   {
       String tSql = "select sysvarvalue from ldsysvar where sysvar = 'MixedComGetUrl'";
       CrmPath = mExeSQL.getOneValue(tSql);
       
       CrmPath = "D:\\CRMDate\\";
       return true;
   }

    private boolean readTextDate() throws java.io.FileNotFoundException,
            java.io.IOException {
    	List tResult=new ArrayList();
    	
//		FtpDownFiles();
    	File dir = new File(CrmPath);
    	File[] files = dir.listFiles();
    	if(files == null)
    	{
    	  return false;	
    	}
    	for (int k = 0; k < files.length; k++) 
    	{
    	   String strFileName = files[k].getName().toLowerCase();
    	   System.out.println(strFileName);
   // 判断是否数据库中是否已经存在此文件
    	   String judgefileSql ="select distinct 1 from lapayowncharge where F1 ='"+strFileName+"' ";
    	    ExeSQL tExeSQL = new ExeSQL();
            String FileExist = tExeSQL.getOneValue(judgefileSql);
            if("1".equals(FileExist))
            {
             	System.out.println("文件已经在数据库存中存在！");
             	continue;
            }
		//file
        String str[]= new String[7];
//        String mLogName = FindLogFile(CrmPath);
//        if(mLogName.equals(""))
//            return false;
//        interFilePath = CrmPath + mLogName;
//        String mFileName="";
        interFilePath = CrmPath + strFileName;
        String toDelete ="I[F001]:";
//        interFilePath="d:\\crmdata\\" + mFileName;
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(interFilePath), "GBK"));  
        //FileReader fr = new FileReader(interFilePath);
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(interFilePath),"GBK"));
        //BufferedReader br = new BufferedReader(fr);
        String Line = reader.readLine() ;

        int intFileNum=0;
        String SingleData[] = new String[28];
        String SingleFactDate[] = new String[28];
        LAPayOwnChargeSet mLAPayOwnChargeSet = new LAPayOwnChargeSet();
        while(Line!=null)
       {
          tResult.add(Line);
          System.out.println("第"+intFileNum+"行 :"+Line);
          intFileNum = intFileNum + 1;
          if(intFileNum>1){
        	   mLAPayOwnChargeSet = new LAPayOwnChargeSet();
      	        for (int i=0;i<intFileNum;i++)
              {
                  SingleData = null;
                  if(i%2 != 0){
                  SingleData = ((String)tResult.get(i)).split("\\,\\'");
                  
                  System.out.println("here1:"+tResult.get(i));
                  
                  for(int j=0;j<28;j++){
                  	if(SingleData[j].endsWith("'")){
                  		SingleFactDate[j] = (String) SingleData[j].replace("'", "");
                  		System.out.println(SingleFactDate[j]);
                  	}
                  }
                  LAPayOwnChargeSchema mLAPayOwnChargeSchema = new LAPayOwnChargeSchema();
                  mLAPayOwnChargeSchema.setContNo(SingleFactDate[0].replace(toDelete,""));
                  mLAPayOwnChargeSchema.setCompCode(SingleFactDate[1]);
                  mLAPayOwnChargeSchema.setCompName(SingleFactDate[2]);
                  mLAPayOwnChargeSchema.setCValiDate(SingleFactDate[3]);
                  mLAPayOwnChargeSchema.setSignDate(SingleFactDate[4]);
                  mLAPayOwnChargeSchema.setRiskCode(SingleFactDate[5]);
                  mLAPayOwnChargeSchema.setRiskName(SingleFactDate[6]);
                  mLAPayOwnChargeSchema.setActNo(SingleFactDate[7]);
                  mLAPayOwnChargeSchema.setTmakeDate(SingleFactDate[8]);
                  mLAPayOwnChargeSchema.setFeePayTypeCode(SingleFactDate[9]);
                  mLAPayOwnChargeSchema.setFeePayTypeName(SingleFactDate[10]);
                  mLAPayOwnChargeSchema.setCharge(SingleFactDate[11]);
                  mLAPayOwnChargeSchema.setChargeRate(SingleFactDate[12]);
                  mLAPayOwnChargeSchema.setActCode(SingleFactDate[13]);
                  mLAPayOwnChargeSchema.setActName(SingleFactDate[14]);
                  mLAPayOwnChargeSchema.setOrgBelgCode(SingleFactDate[15]);
                  mLAPayOwnChargeSchema.setOrgBelgName(SingleFactDate[16]);
                  mLAPayOwnChargeSchema.setOrgCrsCode(SingleFactDate[17]);
                  mLAPayOwnChargeSchema.setOrgCrsName(SingleFactDate[18]);
                  mLAPayOwnChargeSchema.setOrgCrsCompCode(SingleFactDate[19]);
                  mLAPayOwnChargeSchema.setPolSalesCode(SingleFactDate[20]);
                  mLAPayOwnChargeSchema.setPolSalesName(SingleFactDate[21]);
                  mLAPayOwnChargeSchema.setCrsSalesCode(SingleFactDate[22]);
                  mLAPayOwnChargeSchema.setCrsSalesName(SingleFactDate[23]);
                  mLAPayOwnChargeSchema.setCstName(SingleFactDate[24]);
                  mLAPayOwnChargeSchema.setInsrTarg(SingleFactDate[25]);
                  mLAPayOwnChargeSchema.setPrem(SingleFactDate[26]);
                  mLAPayOwnChargeSchema.setDateSend(SingleFactDate[27]);
                  mLAPayOwnChargeSchema.setOperator("001");
                  mLAPayOwnChargeSchema.setMakeDate(this.mToday);
                  mLAPayOwnChargeSchema.setMakeTime(this.mTime);
                  mLAPayOwnChargeSchema.setModifyDate(this.mToday);
                  mLAPayOwnChargeSchema.setModifyTime(this.mTime);
                  mLAPayOwnChargeSchema.setF1(strFileName);
                  mLAPayOwnChargeSet.add(mLAPayOwnChargeSchema);
                  } 
              }
      	      mMap = new MMap();
              mMap.put(mLAPayOwnChargeSet, "DELETE&INSERT");
              if (!SubmitMap() )
                    return false;
              intFileNum=0;
              tResult=new ArrayList();
          }
          Line   =   reader.readLine();
        }
        reader.close();
//        fr.close();
//        mLAPayOwnChargeSet= new LAPayOwnChargeSet();
//        for (int i=0;i<intFileNum;i++)
//        {
//            SingleData = null;
//            if(i%2 != 0 ){
//            SingleData = ((String)tResult.get(i)).split("\\,\\'");
//            System.out.println("here1:"+tResult.get(i));
//            for(int j=0;j<28;j++){
//            	if(SingleData[j].endsWith("'")){
//              		SingleFactDate[j] = (String) SingleData[j].replace("'", "");
//              		System.out.println(SingleFactDate[j]);
//              	}
//            }
//            LAPayOwnChargeSchema mLAPayOwnChargeSchema = new LAPayOwnChargeSchema();
//            mLAPayOwnChargeSchema.setContNo(SingleFactDate[0].replace(toDelete,""));
//            mLAPayOwnChargeSchema.setCompCode(SingleFactDate[1]);
//            mLAPayOwnChargeSchema.setCompName(SingleFactDate[2]);
//            mLAPayOwnChargeSchema.setCValiDate(SingleFactDate[3]);
//            mLAPayOwnChargeSchema.setSignDate(SingleFactDate[4]);
//            mLAPayOwnChargeSchema.setRiskCode(SingleFactDate[5]);
//            mLAPayOwnChargeSchema.setRiskName(SingleFactDate[6]);
//            mLAPayOwnChargeSchema.setActNo(SingleFactDate[7]);
//            mLAPayOwnChargeSchema.setTmakeDate(SingleFactDate[8]);
//            mLAPayOwnChargeSchema.setFeePayTypeCode(SingleFactDate[9]);
//            mLAPayOwnChargeSchema.setFeePayTypeName(SingleFactDate[10]);
//            mLAPayOwnChargeSchema.setCharge(SingleFactDate[11]);
//            mLAPayOwnChargeSchema.setChargeRate(SingleFactDate[12]);
//            mLAPayOwnChargeSchema.setActCode(SingleFactDate[13]);
//            mLAPayOwnChargeSchema.setActName(SingleFactDate[14]);
//            mLAPayOwnChargeSchema.setOrgBelgCode(SingleFactDate[15]);
//            mLAPayOwnChargeSchema.setOrgBelgName(SingleFactDate[16]);
//            mLAPayOwnChargeSchema.setOrgCrsCode(SingleFactDate[17]);
//            mLAPayOwnChargeSchema.setOrgCrsName(SingleFactDate[18]);
//            mLAPayOwnChargeSchema.setOrgCrsCompCode(SingleFactDate[19]);
//            mLAPayOwnChargeSchema.setPolSalesCode(SingleFactDate[20]);
//            mLAPayOwnChargeSchema.setPolSalesName(SingleFactDate[21]);
//            mLAPayOwnChargeSchema.setCrsSalesCode(SingleFactDate[22]);
//            mLAPayOwnChargeSchema.setCrsSalesName(SingleFactDate[23]);
//            mLAPayOwnChargeSchema.setCstName(SingleFactDate[24]);
//            mLAPayOwnChargeSchema.setInsrTarg(SingleFactDate[25]);
//            mLAPayOwnChargeSchema.setPrem(SingleFactDate[26]);
//            mLAPayOwnChargeSchema.setDateSend(SingleFactDate[27]);
//            mLAPayOwnChargeSchema.setOperator("001");
//            mLAPayOwnChargeSchema.setMakeDate(this.mToday);
//            mLAPayOwnChargeSchema.setMakeTime(this.mTime);
//            mLAPayOwnChargeSchema.setModifyDate(this.mToday);
//            mLAPayOwnChargeSchema.setModifyTime(this.mTime);
//            mLAPayOwnChargeSet.add(mLAPayOwnChargeSchema);
//          }
//        }
//        mMap = new MMap();
//        mMap.put(mLAPayOwnChargeSet, "DELETE&INSERT");
//        if (!SubmitMap() )
//              return false;
    	}
    	return true;
    }


/*数据处理*/
private boolean SubmitMap()
{
    PubSubmit tPubSubmit = new PubSubmit();
    mResult= new VData();
    mResult.add(mMap);
    if (!tPubSubmit.submitData(mResult, ""))
    {
        System.out.println("提交数据失败！");
        return false;
    }
//    System.gc();
    return true;
}
public String GetBatchNo()
{
    return mBatchNo;
}

private boolean FtpDownFiles(){
	
//	String CrmPath = "D:\\CRMDate\\";
	String tFtpSQL = "select code,codename from ldcode where codetype = 'jtjxftpnl' ";
	SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
	if (tSSRS == null || tSSRS.MaxRow < 1) {
		System.out.println("没有找到集团交叉ftp配置信息！");
		return false;
	}
	for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
		if ("IP".equals(tSSRS.GetText(i, 1))) {
			mIP = tSSRS.GetText(i, 2);
		} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
			mUserName = tSSRS.GetText(i, 2);
		} else if ("Password".equals(tSSRS.GetText(i, 1))) {
			mPassword = tSSRS.GetText(i, 2);
		} else if ("Port".equals(tSSRS.GetText(i, 1))) {
			mPort = Integer.parseInt(tSSRS.GetText(i, 2));
		} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
			mTransMode = tSSRS.GetText(i, 2);
		}
	}
    System.out.println(CrmPath);
	FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
			mTransMode);
	try {
		if (!tFTPTool.loginFTP()) {
			System.out.println(tFTPTool.getErrContent(1));
		} else {
				
					tFTPTool.download("/gather/gather/mqm/rec/mainData/",
							CrmPath);
					System.out.println("文件下载成功了！");
					
				
			
		}
	} catch (SocketException ex) {
		ex.printStackTrace();
	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		tFTPTool.logoutFTP();
	}
       return true;
}

 private String  getFile(String StrPath){
	File dir = new File(StrPath);
	File[] files = dir.listFiles();
	if(files == null)
	{
	  return null;	
	}
	for (int i = 0; i < files.length; i++) {
 	   String strFileName = files[i].getAbsolutePath().toLowerCase();
       return strFileName;    
	}
	return null;

 }

    public static void main(String[] args) throws Exception
    {
//    	MixedReadChargeFileTask mMixedReadChargeFileTask = new MixedReadChargeFileTask();
//    	mMixedReadChargeFileTask.run();
//    
//        return;

    }
   
}
