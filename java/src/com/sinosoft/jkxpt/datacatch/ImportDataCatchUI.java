package com.sinosoft.jkxpt.datacatch;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>
 * Title:健康险平台数据采集类 Description:
 * 
 * Company: 中科软科技股份有限公司
 * 
 * @author : czl
 * @date:2010-12-17
 * @version 1.0
 */
public class ImportDataCatchUI {
	public CErrors mErrors = new CErrors();
	//传来的参数
	public String mStartDate = "";
	public String mEndDate = "";
	public GlobalInput tG = null;
	//先后传递的参数
	public VData mResult = new VData();
	public ImportDataCatchUI() {
	}
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			System.out.println("*********ImportDataCatchUI**********");
			tG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0); 
			TransferData inputTransfer = (TransferData) cInputData.getObjectByObjectName("TransferData",0); 
			mStartDate = (String) inputTransfer.getValueByName("StartDate");
			mEndDate = (String) inputTransfer.getValueByName("EndDate");
			mResult.add(tG);
			mResult.add(inputTransfer);
			ImportDataCatchBL tImportDataCatchBL = new ImportDataCatchBL();
			if (!tImportDataCatchBL.submitData(mResult, "")) {
				if (tImportDataCatchBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tImportDataCatchBL.mErrors);
					return false;
				} else {
					buildError("submitData","HealthDataCatchUI.submitData出错");
					return false;
				}
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "HealthDataCatchUI";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}
	private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "HealthDataCathUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
