package com.sinosoft.jkxpt.datacatch;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 数据抓取及发送页面调用，按顺序调用承保、保全、理赔DTO处理类，抓取数据，生成DTO，发送，返回结果
 * @author xuzy
 *
 */
public class InterReportBL {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData;
	public String mStartDate = "";
	public String mEndDate = "";
	public String mManageCom = "";
	public String mCatchType = "";
	public String mResultType = "02";  //01-成功  02-失败
	public String mCatchFlag = ""; //01--提数  02--补提  03--重提
	public String mUploadSNO ="";
	public String mEorrorLog = "";
	
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	/** 全局数据 */
	private GlobalInput tG = null;
	private MMap map = new MMap();
	public String mCurrentDate = PubFun.getCurrentDate();
	private String mURL = "";
	private String mIP = "";
	private String mUserName = "";
	private String mPassword = "";
	private int mPort;
	private BufferedWriter mBufferedWriter;
	private String mTransMode = "";
	public InterReportBL() {
	}
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			//获取时间、Session等参数
			if (!getInputData(cInputData)) {
				return false;
			}
			if(!dealData()){
				return false;
			}	
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "InterReportBL";
			cError.functionName = "submit";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}finally{
        	try{
        		//dealCatchData();
        	}catch(Exception ex){
        		ex.printStackTrace();
        	}
	    }
		
		return true;
	}
	/**
	 * 获得传入参数
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		tG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		

		return true;
	}
	/**
	 * 解析targetKey和targetValue，查询LFDesbModeSet
	 * @return
	 */
	private boolean dealData(){
		System.out.println("开始提取" + this.mCurrentDate + "日的交叉销售数据,开始时间是"
				+ PubFun.getCurrentTime());
		if (!getMixedAgent()) {
			return false;
		}
		System.out.println("提取" + this.mCurrentDate + "日的交叉销售数据完成,完成时间是"
				+ PubFun.getCurrentTime());
		return true;
	}
	
	private boolean getMixedAgent() {

		String tSQL = "select SysVarValue from LDSYSVAR where Sysvar='InterSale'";
		this.mURL = new ExeSQL().getOneValue(tSQL);
//		this.mURL = "D:\\xmlbackup\\";
		if (mURL == null || mURL.equals("")) {
			System.out.println("没有找到集团交叉销售存储文件夹！");
			return false;
		}

		String tFtpSQL = "select code,codename from ldcode where codetype = 'jtjxftpnl' ";
		SSRS tSSRS = new ExeSQL().execSQL(tFtpSQL);
		if (tSSRS == null || tSSRS.MaxRow < 1) {
			System.out.println("没有找到集团交叉ftp配置信息！");
			return false;
		}
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			if ("IP".equals(tSSRS.GetText(i, 1))) {
				mIP = tSSRS.GetText(i, 2);
			} else if ("UserName".equals(tSSRS.GetText(i, 1))) {
				mUserName = tSSRS.GetText(i, 2);
			} else if ("Password".equals(tSSRS.GetText(i, 1))) {
				mPassword = tSSRS.GetText(i, 2);
			} else if ("Port".equals(tSSRS.GetText(i, 1))) {
				mPort = Integer.parseInt(tSSRS.GetText(i, 2));
			} else if ("TransMode".equals(tSSRS.GetText(i, 1))) {
				mTransMode = tSSRS.GetText(i, 2);
			}
		}
		// 健康险应付寿险
		 getS001();
		// 健康险应付财产险
		 getS002();
         
			// 健康险实付寿险
		 getP001();
		// 健康险实付财产险
		 getP002();
		  
		 
		 
		 FtpTransferFiles();
		 try
		 {
		 del(mURL);
		 } catch (IOException ex)
		 {
		 ex.printStackTrace();
		 }




		return true;

	}
	
	/**
	 * 修改生成的文件夹名称 （添加MD5后的名称）
	 * @param cFilePath  生成的原文件路径
	 * @param cChargeType 手续费类型
	 * @param cToCompare  发给财或寿类型
	 * @return
	 */
	private boolean changeFileName(String cFilePath,String cChargeType,String cToCompare)
	{
		File tOldFile = new File (cFilePath);
		if (!tOldFile.exists()) {
			System.out.println("原文件夹不存在！");
			return false;// 重命名文件不存在
		}
		String tMD5 ="";
		try {
			FileInputStream tFileInputStream = new FileInputStream(cFilePath);
//			tMD5 = DigestUtils.md5Hex(tFileInputStream.toString()).substring(8,
//					24);
				tMD5 =getMD5(tFileInputStream).substring(8,
						24);
			System.out.println("tMD5:"+tMD5);
			tFileInputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			finally {

		}
		File tNewFile = new File(mURL + "000085"+cChargeType
				+ mCurrentDate.replaceAll("-", "") + tMD5 + cToCompare+".txt");
		if (tNewFile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
		{
			System.out.println("新文件已经存在！");
			return false;// 重命名文件不存在
		}
		boolean flag = tOldFile.renameTo(tNewFile);
		return flag;
	}

private void getS002() {
	String filename = mURL+"000085F001" + mCurrentDate.replaceAll("-", "")
			+ "000002.txt";
	String tCalDate =PubFun.getCurrentDate();
	tCalDate = PubFun.calDate(tCalDate, -24, "D", "");
//	String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
//	String tEndDate = PubFun.getCurrentDate();
//	System.out.println("@@@@@@@@@@@寿险&&&&&&&&");
	System.out.println(tCalDate+"----");
	String tSQL =
	// 个险首期
	"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = b.contno fetch first 1 rows only ) 代理方机构代码 ,''  代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and  c.commisionsn = b.commisionsn   and a.othernotype ='BC'and b.transtype ='ZC'and b.endorsementno is null and d.agentcom = b.agentcom and b.payyear = 0  and d.actype ='06' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> 03 and  a.shoulddate = '"+tCalDate +"'  union  "
			// 团险首期
			+"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only ) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and b.endorsementno is null and a.othernotype ='BC'and b.payyear = 0 and  b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='06' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> 03  and  a.shoulddate = '"+tCalDate +"'    union "
			+
			// 个险续期
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate), 'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " 'XQ' 业务活动代码,'续期' 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only ) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and b.endorsementno is null and a.otherno = b.receiptno and c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and   a.shoulddate = '"+tCalDate +"'    union "
			+
			// 团险续期
			" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate), 'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, (select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例, "
			+ " 'XQ'  业务活动代码,'续期' 业务活动名称,"
			+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.endorsementno is null and b.transtype ='ZC' and  d.actype ='06' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate ='"+tCalDate+"'    union  "
			+
			// 个险保全
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 , '' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='06' and b.endorsementno is not  null and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and   a.shoulddate ='"+tCalDate+"'   union  "
			+
			// 个险保全追加
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = c.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.endorsementno is not null  and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and    a.shoulddate ='"+tCalDate+"'   union  "
			+
			// 团险保全
			" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03'))  and b.endorsementno is not null and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and    a.shoulddate ='"+tCalDate+"'  union   "
			+
			// 团险保全追加
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = c.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000002' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='06'and b.branchtype ='5' and b.endorsementno is not null and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000'  and   a.shoulddate ='"+tCalDate+"'   ";
	System.out.println(tSQL);
	int start = 1;
	int nCount = 200;
	while (true) {
		SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
		if (tSSRS.getMaxRow() <= 0) {
			FileOutputStream tFileOutputStream =  null;
			try {
				tFileOutputStream = new FileOutputStream(filename, true);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally
			{
				try {
					tFileOutputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			new File(filename);
			break;
		}
		String tempString = tSSRS.encode();
		String[] tempStringArr = tempString.split("\\^");
		// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
		// mFileWriter = new FileWriter("C:\\000085S001" +
		// mCurrentDate.replaceAll("-", "") +
		// ".txt");
		// System.out.println(mFileWriter.getEncoding());
		// mBufferedWriter = new BufferedWriter(mFileWriter);
		// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
		FileOutputStream tFileOutputStream =  null;
		OutputStreamWriter tOutputStreamWriter = null;
		try{
			tFileOutputStream = new FileOutputStream(filename, true);
			tOutputStreamWriter = new OutputStreamWriter(
				tFileOutputStream, "GBK");
		mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			mBufferedWriter.write("D[F001]:(1=0)");
			mBufferedWriter.newLine();
			String t = tempStringArr[i].replaceAll("\\|", "','");
			String[] tArr = t.split("\\,");
			mBufferedWriter.write("I[F001]:'");
			for (int j = 0; j < tArr.length - 1; j++) {
				mBufferedWriter.write(tArr[j] + ",");
			}
			mBufferedWriter.write(tArr[tArr.length - 1] + "'");
			mBufferedWriter.newLine();
			mBufferedWriter.flush();
		}
		mBufferedWriter.close();
		start += nCount;
	} catch (IOException ex2) {
		System.out.println(ex2.getStackTrace());
	}
	finally
	{
		try {
			tFileOutputStream.close();
			tOutputStreamWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
changeFileName(filename,"F001","000002");
}
  
private void getS001() {
	
	String tDate =PubFun.getCurrentDate2().substring(0,4)+"-"+PubFun.getCurrentDate2().substring(4,6)+"-01";
	tDate =  PubFun.calDate(tDate, -1, "M", "");
	String tStartdate = PubFun.calDate(tDate, 9, "D", "");
	
//	String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
	String tEndDate = PubFun.getCurrentDate();
	tEndDate =  PubFun.calDate(tEndDate, -1, "D", "");
//	String tEndDate = PubFun.getCurrentDate();
	System.out.println(tEndDate+"----"+tStartdate);
	String filename = mURL+"000085F001" + mCurrentDate.replaceAll("-", "")
			+ "000010.txt";
	
	String tSQL =
	// 个险承保首期
	"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode ) 险种名称,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = b.contno fetch first 1 rows only ) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,  getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.othernotype ='BC'and b.transtype ='ZC' and b.endorsementno is null  and b.payyear =0  and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> '03' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'    union  "
			+
			// 团险承保首期
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " 'QD' 业务活动代码,'签单' 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and b.endorsementno is null and c.commisionsn = b.commisionsn and b.payyear = 0 and a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='07' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> '03' and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'    union  "
			+
			// 个险续期
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " 'XQ' 业务活动代码,'续期' 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = b.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码, getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and b.endorsementno is null and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'  union "
			+
			// 团险续期
			" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, (select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例, "
			+ " 'XQ'  业务活动代码,'续期' 业务活动名称,"
			+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码, getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno ) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称, b.p11 承保标的,c.transmoney 保费收入 ,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and b.endorsementno is null and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='07' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' union  "
			+
			// 个险保全
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称,b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lccont where contno = b.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and  a.getnoticeno = b.commisionsn and b.endorsementno is not null and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 个险保全追加
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ "b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select contno from lccont where contno = b.contno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode =b.agentcode) 委托方人员姓名,(select grpagentcode from lccont where contno = b.contno ) 代理方人员代码,(select grpagentname from lccont where contno = b.contno) 代理方人员姓名,b.p11 客户名称,b.p13 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and b.endorsementno is not null  and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"' union  "
			+
			// 团险保全
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno  业务单证ID,ts_fmt((a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode=b.transtype  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码,getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and b.endorsementno is not null  and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate   between '"+tStartdate+"' and '"+tEndDate+"'  union "
			+
			// 团险保全追加
			" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select riskName from lmrisk where riskcode = b.riskcode )险种名称, b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,c.charge 应付业务佣金,c.chargerate 应付佣金比例,"
			+ " b.transtype 业务活动代码,(select edorname from lmedoritem  where edorcode='ZB'  fetch first 1 rows only) 业务活动名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "(select grpagentcom from lcgrpcont where grpcontno = b.grpcontno fetch first 1 rows only) 代理方机构代码 ,'' 代理方机构名称,'000100' 代理方子公司代码, getUniteCode(b.agentcode) 委托方人员代码,(select name from laagent where agentcode = b.agentcode) 委托方人员姓名,(select grpagentcode from lcgrpcont where grpcontno = b.grpcontno) 代理方人员代码,(select grpagentname from lcgrpcont where grpcontno = b.grpcontno) 代理方人员姓名,b.p11 客户名称,b.p11 承保标的,c.transmoney 保费收入, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01' and b.endorsementno is not null  and b.grpcontno <>'00000000000000000000' and a.shoulddate   between '"+tStartdate+"' and '"+tEndDate+"' ";

	System.out.println(tSQL);
	int start = 1;
	int nCount = 200;
	while (true) {
		SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
		if (tSSRS.getMaxRow() <= 0) {
			FileOutputStream tFileOutputStream =  null;
			try {
				tFileOutputStream = new FileOutputStream(filename, true);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally
			{
				try {
					tFileOutputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			new File(filename);
			break;
		}
		String tempString = tSSRS.encode();
		String[] tempStringArr = tempString.split("\\^");
		// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
		// mFileWriter = new FileWriter("C:\\000085S001" +
		// mCurrentDate.replaceAll("-", "") +
		// ".txt");
		// System.out.println(mFileWriter.getEncoding());
		// mBufferedWriter = new BufferedWriter(mFileWriter);
		// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
		FileOutputStream tFileOutputStream = null;
		OutputStreamWriter tOutputStreamWriter = null;
		try
		{
			tFileOutputStream = new FileOutputStream(filename, true);
			tOutputStreamWriter = new OutputStreamWriter(
				tFileOutputStream, "GBK");
		mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			mBufferedWriter.write("D[F001]:(pol_no = '"
					+ tSSRS.GetText(i, 1) + "' AND comp_cod = '"
					+ tSSRS.GetText(i, 2) + "' AND ACT_NO = '"
					+ tSSRS.GetText(i, 8) + "' and DATE_ACT ='"
					+ tSSRS.GetText(i, 4) + "' and DATE_SIGN ='"
					+ tSSRS.GetText(i, 5) + "' and Risk_cod ='"
					+ tSSRS.GetText(i, 6) + "' and Fee_pay_typecod ='"
					+ tSSRS.GetText(i, 10) + "')");
			mBufferedWriter.newLine();
			String t = tempStringArr[i].replaceAll("\\|", "','");
			String[] tArr = t.split("\\,");
			mBufferedWriter.write("I[F001]:'");
			for (int j = 0; j < tArr.length - 1; j++) {
				mBufferedWriter.write(tArr[j] + ",");
			}
			mBufferedWriter.write(tArr[tArr.length - 1] + "'");
			mBufferedWriter.newLine();
			mBufferedWriter.flush();
		}
		mBufferedWriter.close();
		start += nCount;
	} catch (IOException ex2) {
		System.out.println(ex2.getStackTrace());
	}
	finally
	{
		try {
			tFileOutputStream.close();
			tOutputStreamWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
changeFileName(filename,"F001","000010");
}
private void getP001() {
	String tDate =PubFun.getCurrentDate2().substring(0,4)+"-"+PubFun.getCurrentDate2().substring(4,6)+"-01";
	tDate =  PubFun.calDate(tDate, -1, "M", "");
	String tStartdate = PubFun.calDate(tDate, 9, "D", "");
	
//	String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
	String tEndDate = PubFun.getCurrentDate();
	tEndDate =  PubFun.calDate(tEndDate, -1, "D", "");
//	String tEndDate = PubFun.getCurrentDate();
	System.out.println(tEndDate+"----"+tStartdate);
	String filename = mURL+"000085F002" + mCurrentDate.replaceAll("-", "")
			+ "000010.txt";
	String tSQL =
	// 个险承保首期
	"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 委托方支付佣金 ,ts_fmt((a.confdate),'yyyymmdd')|| a.modifytime 支付时间, a.actugetno 支付单号,'' 银行凭证号 , ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间   "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null and a.othernotype ='BC'and b.transtype ='ZC' and d.agentcom = b.agentcom  and b.payyear = 0 and  d.actype ='07' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> '03' and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'     union  "
			+
			// 团险承保首期
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金, ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间,a.actugetno 支付单号,''银行凭证号, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间   "
			+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null  and b.payyear = 0 and  a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='07' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> '03' and  a.shoulddate  between '"+tStartdate+"' and '"+tEndDate+"'     union  "
			+
			// 个险续期
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间 ,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间   "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='07' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union "
			+
			// 团险续期
			" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称, "
			+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='07' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 个险保全
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and  a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 个险保全追加
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,  ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='07' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 团险保全
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and a.confdate is not null  and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union "
			+
			// 团险保全追加
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='07'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  ";
	System.out.println(tSQL);
	int start = 1;
	int nCount = 200;
	while (true) {
		SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
		if (tSSRS.getMaxRow() <= 0) {
			FileOutputStream tFileOutputStream =  null;
			try {
				tFileOutputStream = new FileOutputStream(filename, true);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally
			{
				try {
					tFileOutputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			new File(filename);
			break;
		}
		String tempString = tSSRS.encode();
		String[] tempStringArr = tempString.split("\\^");
		// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
		// mFileWriter = new FileWriter("C:\\000085S001" +
		// mCurrentDate.replaceAll("-", "") +
		// ".txt");
		// System.out.println(mFileWriter.getEncoding());
		// mBufferedWriter = new BufferedWriter(mFileWriter);
		// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
		FileOutputStream tFileOutputStream = null;
		OutputStreamWriter tOutputStreamWriter = null;
		try
		{
			tFileOutputStream = new FileOutputStream(filename, true);
			tOutputStreamWriter = new OutputStreamWriter(
				tFileOutputStream, "GBK");
		mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			mBufferedWriter.write("D[F001]:(pol_no = '"
					+ tSSRS.GetText(i, 1) + "' AND comp_cod = '"
					+ tSSRS.GetText(i, 2) + "' AND ACT_NO = '"
					+ tSSRS.GetText(i, 7) + "' and DATE_ACT ='"
					+ tSSRS.GetText(i, 4) + "' and DATE_SIGN ='"
					+ tSSRS.GetText(i, 5) + "' and Risk_cod ='"
					+ tSSRS.GetText(i, 6) + "' and Fee_pay_typecod ='"
					+ tSSRS.GetText(i, 9) + "')");
			mBufferedWriter.newLine();
			String t = tempStringArr[i].replaceAll("\\|", "','");
			String[] tArr = t.split("\\,");
			mBufferedWriter.write("I[F001]:'");
			for (int j = 0; j < tArr.length - 1; j++) {
				mBufferedWriter.write(tArr[j] + ",");
			}
			mBufferedWriter.write(tArr[tArr.length - 1] + "'");
			mBufferedWriter.newLine();
			mBufferedWriter.flush();
		}
		mBufferedWriter.close();
		start += nCount;
		tFileOutputStream.close();
		//changeFileName(filename,"F002","000010");
		} catch (IOException ex2) {
			System.out.println(ex2.getStackTrace());
		}
		finally
		{
			try {
				tFileOutputStream.close();
				tOutputStreamWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

changeFileName(filename,"F002","000010");
}

private void getP002() {
	String tDate =PubFun.getCurrentDate2().substring(0,4)+"-"+PubFun.getCurrentDate2().substring(4,6)+"-01";
	tDate =  PubFun.calDate(tDate, -1, "M", "");
	String tStartdate = PubFun.calDate(tDate, 9, "D", "");
	
//	String tEndDate = PubFun.calDate(tStartdate, 8, "D", "");
	String tEndDate = PubFun.getCurrentDate();
	tEndDate =  PubFun.calDate(tEndDate, -1, "D", "");
//	String tEndDate = PubFun.getCurrentDate();
	System.out.println(tEndDate+"----"+tStartdate);
	String filename = mURL+"000085F002" + mCurrentDate.replaceAll("-", "")
			+ "000002.txt";
	String tSQL =
	// 个险承保首期
	"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate), 'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate), 'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 委托方支付佣金 ,ts_fmt((a.confdate),'yyyymmdd')|| a.modifytime 支付时间, a.actugetno 支付单号,'' 银行凭证号 , ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null and b.payyear = 0 and  a.othernotype ='BC'and b.transtype ='ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.TransState <> '03' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'   union  "
			+
			// 团险承保首期
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.signdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.grppolno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金, ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间,a.actugetno 支付单号,''银行凭证号, ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c,lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and c.commisionsn = b.commisionsn and a.confdate is not null and b.payyear = 0  and a.othernotype ='BC'and b.transtype ='ZC'and d.agentcom = b.agentcom and d.actype ='06' and  b.grpcontno <> '00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.transstate <> '03' and  a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'     union  "
			+
			// 个险续期
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select serialno from ljapayperson where polno = b.polno and payno = b.receiptno and dutycode = b.dutycode  and payplancode = b.payplancode)||b.polno 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付时间 ,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and b.transtype= 'ZC' and d.agentcom = b.agentcom  and d.actype ='06' and b.grpcontno ='00000000000000000000' and b.payyear>0   and b.branchtype ='5' and b.branchtype2='01' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union "
			+
			// 团险续期
			" select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(b.tconfdate),'yyyymmdd')  业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,(select serialno from ljapaygrp where grppolno = b.grppolno and payno = b.receiptno) 业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称, "
			+ " a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,''银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b, lacharge c, lacom d where a.getnoticeno = b.commisionsn and a.otherno = b.receiptno and a.confdate is not null and  c.commisionsn = b.commisionsn  and d.agentcom = b.agentcom  and b.transtype ='ZC' and  d.actype ='06' and b.grpcontno <>'00000000000000000000'and b.branchtype ='5' and b.branchtype2='01' and b.payyear>0 and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 个险保全
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01' and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and b.grpcontno ='00000000000000000000' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 个险保全追加
			"select b.commisionsn 佣金id,b.contno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno||b.polno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,  ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.othernotype ='BC' and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn  and b.transtype='ZC' and b.transstate='03' and d.agentcom = b.agentcom  and d.actype ='06' and b.branchtype ='5' and b.branchtype2='01'  and b.grpcontno ='00000000000000000000' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union  "
			+
			// 团险保全
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and a.confdate is not null  and  d.agentcom = b.agentcom   and (c.transtype<>'ZC' or (b.transtype='ZC' and b.transstate='03')) and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and b.grpcontno <>'00000000000000000000' and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"'  union "
			+
			// 团险促使追加
			"select b.commisionsn 佣金id,b.grpcontno 保单号,'000085' 子公司代码,'中国人民健康保险股份有限公司' 子公司名称,ts_fmt(date(c.makedate),'yyyymmdd') 业务活动日期,ts_fmt(date(b.signdate),'yyyymmdd') 签单日期 ,b.riskcode 险种ID,b.endorsementno  业务单证ID,ts_fmt(date(a.shoulddate),'yyyymmdd') 佣金生成日期,'01' 佣金类型代码,'业务佣金' 佣金类型名称,"
			+ "a.managecom 委托方机构代码,(select name from ldcom where comcode = a.managecom ) 委托方机构名称,"
			+ "c.charge 支付佣金,ts_fmt(date(a.confdate),'yyyymmdd')|| a.modifytime 支付佣金时间,a.actugetno 支付单号,'' 银行凭证号,ts_fmt(current TIMESTAMP,'yyyymmdd hh:mi:ss') 报送时间  "
			+ "from ljaget a ,lacommision b,lacharge c, lacom d where a.actugetno = b.receiptno and a.confdate is not null and  a.getnoticeno = b.commisionsn and c.commisionsn = b.commisionsn and  d.agentcom = b.agentcom   and b.transtype='ZC' and b.transstate='03' and  d.actype ='06'and b.branchtype ='5' and b.branchtype2 ='01'  and a.shoulddate between  '"+tStartdate+"' and '"+tEndDate+"' and   b.grpcontno <>'00000000000000000000' ";
	System.out.println(tSQL);
	int start = 1;
	int nCount = 200;
	while (true) {
		SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
		if (tSSRS.getMaxRow() <= 0) {
			FileOutputStream tFileOutputStream =  null;
			try {
				tFileOutputStream = new FileOutputStream(filename, true);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally
			{
				try {
					tFileOutputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			new File(filename);
			break;
		}
		String tempString = tSSRS.encode();
		String[] tempStringArr = tempString.split("\\^");
		// 默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
		// mFileWriter = new FileWriter("C:\\000085S001" +
		// mCurrentDate.replaceAll("-", "") +
		// ".txt");
		// System.out.println(mFileWriter.getEncoding());
		// mBufferedWriter = new BufferedWriter(mFileWriter);
		// 指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
		FileOutputStream tFileOutputStream = null;
		OutputStreamWriter tOutputStreamWriter = null;
		try
		{
			tFileOutputStream = new FileOutputStream(filename, true);
			tOutputStreamWriter = new OutputStreamWriter(
				tFileOutputStream, "GBK");
		mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			mBufferedWriter.write("D[F001]:(1=0)");
			mBufferedWriter.newLine();
			String t = tempStringArr[i].replaceAll("\\|", "','");
			String[] tArr = t.split("\\,");
			mBufferedWriter.write("I[F001]:'");
			for (int j = 0; j < tArr.length - 1; j++) {
				mBufferedWriter.write(tArr[j] + ",");
			}
			mBufferedWriter.write(tArr[tArr.length - 1] + "'");
			mBufferedWriter.newLine();
			mBufferedWriter.flush();
		}
		mBufferedWriter.close();
		start += nCount;
		tFileOutputStream.close();
		} catch (IOException ex2) {
			System.out.println(ex2.getStackTrace());
		}
		finally
		{
			try {
				tFileOutputStream.close();
				tOutputStreamWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	changeFileName(filename,"F002","000002");
}
	
/**
 * 删除目录下所有文件和子目录，根目录不删除
 * 
 * @param filepath
 *            String
 * @throws IOException
 */
private void del(String filepath) throws IOException {
	File f = new File(filepath); // 定义文件路径
	if (f.exists() && f.isDirectory()) { // 判断是文件还是目录
		if (f.listFiles().length == 0) { // 若目录下没有文件则直接返回，这里不再删除根目录
			return;
		} else { // 若有则把文件放进数组，并判断是否有下级目录
			File delFile[] = f.listFiles();
			int i = f.listFiles().length;
			for (int j = 0; j < i; j++) {
				if (delFile[j].isDirectory()) {
					del(delFile[j].getAbsolutePath()); // 递归调用del方法并取得子目录路径
					System.out.println("SHUACHUWENJIAN");
				}
				delFile[j].delete(); // 删除文件，或者递归传入的子目录
			}
		}
	}
}
    
/**
 * 将生成文件传送到10.252.1.112的/home/gather/dat/目录下.
 */
private void FtpTransferFiles() {
	FTPTool tFTPTool = new FTPTool(mIP, mUserName, mPassword, mPort,
			mTransMode);
	try {
		if (!tFTPTool.loginFTP()) {
			System.out.println(tFTPTool.getErrContent(1));
		} else {
			String[] tFileNames = new File(mURL).list();
			if (tFileNames == null) {
				System.out.println("集团交叉销售Error:获取目录下的文件失败，请检查该目录是否存在！");
			} else {
				for (int i = 0; i < tFileNames.length; i++) {
					// 寿险报送文件夹/gather/gather/mqm/sales/send/mainData/
					tFTPTool.upload("/gather/gather/mqm/sales/send/mainData/",
							mURL + tFileNames[i]);
				}
			}
		}
	} catch (SocketException ex) {
		ex.printStackTrace();
	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
		tFTPTool.logoutFTP();
	}
}
 
/** */
/**
 * 文件重命名
 * 
 * @param path
 *            文件目录
 * @param oldname
 *            原来的文件名
 * @param newname
 *            新文件名
 */
public boolean renameFile(File cOldFIle, File cNewFile) {
	if (!cOldFIle.exists()) {
		System.out.println("原文件夹不存在！");
		return false;// 重命名文件不存在
	}
	if (cNewFile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
	{
		System.out.println("新文件已经存在！");
		return false;// 重命名文件不存在
	}
	boolean flag = cOldFIle.renameTo(cNewFile);
	return flag;
}
         
public void changeFile(String strPath,String chargeType,String changeType) throws Exception {
    File dir = new File(strPath); 
    File[] files = dir.listFiles();
    String currdate = PubFun.getCurrentDate2();
    String strNewFileName="";
    String tMD5="";
       if (files == null) 
           return; 
       for (int i = 0; i < files.length; i++) {
    	   String strFileName = files[i].getAbsolutePath().toLowerCase();
    	   FileInputStream tFileInputStream = new FileInputStream(strFileName);
  			tMD5 = DigestUtils.md5Hex(tFileInputStream.toString()).substring(8,
  					24);
    	   strNewFileName= "000085"+chargeType+currdate+tMD5+changeType+".txt";
    	   
       }
}

/**
 * 使用文件通道的方式复制文件
 * 
 * @param s
 *            源文件
 * @param t
 *            复制到的新文件
 */
 public void fileChannelCopy(File s, File t) {
     FileInputStream fi = null;
     FileOutputStream fo = null;
     FileChannel in = null;
     FileChannel out = null;
     try {
         fi = new FileInputStream(s);
         fo = new FileOutputStream(t);
         in = fi.getChannel();//得到对应的文件通道
         out = fo.getChannel();//得到对应的文件通道
         in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道
     } catch (IOException e) {
         e.printStackTrace();
     } finally {
         try {
             fi.close();
             in.close();
             fo.close();
             out.close();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
 }


    /**
	 * 生成错误信息
	 * @param szFunc	报错方法名
	 * @param szErrMsg	错误信息
	 */
	private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "HealthDataCathUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	private boolean checkInputData() {
		return true;
	}
	public static void main(String[] args) {
		ImportDataCatchBL t = new ImportDataCatchBL();
//		for (int i = 0; i < 10000; i++) {
//			File  file = new File("D:\\xmlbackup\\000085F0012016012895367a120e31ee19000002.txt");
//			try {
//				System.out.println(getMD5(file));
//					
//				} catch (NoSuchAlgorithmException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		}
	//	File  file = new File("D:\\xmlbackup\\000085F0012016012895367a120e31ee19000002.txt");
		
		
	
	}
	  public static String getMD5(InputStream is) throws NoSuchAlgorithmException, IOException {
	  		StringBuffer md5 = new StringBuffer();
	  		MessageDigest md = MessageDigest.getInstance("MD5");
	  		byte[] dataBytes = new byte[1024];
//	  	String 	tMD5 = DigestUtils.md5Hex(is.toString()).substring(8,
//					24);
	  		int nread = 0; 
	  		while ((nread = is.read(dataBytes)) != -1) {
	  			md.update(dataBytes, 0, nread);
	  		};
	  		byte[] mdbytes = md.digest();
	  		
	  		// convert the byte to hex format
	  		for (int i = 0; i < mdbytes.length; i++) {
	  			md5.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	  		}
	  		return md5.toString();
	  	}
	  	
	  	/**
	  	 * 获取该文件的MD5值
	  	 * 
	  	 * @param file
	  	 * @return
	  	 * @throws NoSuchAlgorithmException
	  	 * @throws IOException
	  	 */
	  	public static String getMD5(File file) throws NoSuchAlgorithmException, IOException {
	  		FileInputStream fis = new FileInputStream(file);
	  		return getMD5(fis);
	  	}
     
} 
