package com.sinosoft.maintain;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 续保换号时把B表的同保单已退保的保单保单级别表的ContNo修改为新号
 * 程序已修改，现维护为原保单号
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class MTContNoXB
{
    public MTContNoXB()
    {
    }

    private boolean mainTain()
    {
        MMap map = new MMap();
        String[] tables = {"LBInsured", "LBCustomerImpart",
                          "LBCustomerImpartParams", "LBPol"};

        String sql = "select b.EdorNo 退保工单号, b.ContNo 续保新保单号, "
                     + "a.ContNo 原保单号, a.InsuredNo 被保人号码 "
                     + "from LBPol a, LBInsured b "
                     + "where a.EdorNo = b.EdorNo "
                     + "   and a.InsuredNo = b.InsuredNo "
                     + "   and a.ContNo != b.ContNo "
                     + "   and a.ContType = '1' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            for(int j = 0; j < tables.length; j++)
            {
                sql = "update " + tables[j]
                      + " set ContNo = '" + tSSRS.GetText(i, 3) + "' "
                      + "where EdorNo = '" + tSSRS.GetText(i, 1) + "' "
                      + "   and ContNo = '" + tSSRS.GetText(i, 2) + "' ";
                map.put(sql, SysConst.UPDATE);

                System.out.println("客户" + tSSRS.GetText(i, 4)
                                   + "的表" + tables[j]
                                   + "的保单号" + tSSRS.GetText(i, 2)
                                   + "该为" + tSSRS.GetText(i, 3));
            }
        }

        VData d = new VData();
        d.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        MTContNoXB mt = new MTContNoXB();
        if(!mt.mainTain())
        {
            System.out.println("维护失败");
        }
    }
}
