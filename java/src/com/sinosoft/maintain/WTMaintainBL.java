package com.sinosoft.maintain;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 早期团单犹豫期退保没有将个单所有信息转移到B表
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.3
 */
public class WTMaintainBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private String mEdorNo = null;
    private String mGrpContNo = null;

    public WTMaintainBL(String edorNo, String grpContNo)
    {
        mEdorNo = edorNo;
        mGrpContNo = grpContNo;
    }

    //个险退保
    private boolean polCancel()
    {
        if(!checkData())
        {
            return false;
        }

        MMap map = new MMap();
        ContCancel tContCancel = new ContCancel();

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mGrpContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            String polNo = tLCPolSet.get(i).getPolNo();
            MMap tmap = new MMap();
            tmap = tContCancel.preparePolData(polNo, mEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备险种保单号" + polNo + "数据失败！");
                return false;
            }
            map.add(tmap);
        }

        //为屏蔽之前数据维护导致的数据不一致，需要同时从C、B表进行查询退保
        LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setGrpContNo(mGrpContNo);
        LBPolSet tLBPolSet = tLBPolDB.query();
        for(int i = 1; i <= tLBPolSet.size(); i++)
        {
            String polNo = tLBPolSet.get(i).getPolNo();
            MMap tmap = new MMap();
            tmap = tContCancel.preparePolData(polNo, mEdorNo);
            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备险种保单号" + polNo + "数据失败！");
                return false;
            }
            map.add(tmap);
        }


        VData data = new VData();
        data.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, "INSERT"))
        {
            System.out.println(p.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
        return true;
    }

    /**
     * 被保人退保
     */
    private boolean insuredCancel()
    {
        if(!checkData())
        {
            return false;
        }

        MMap map = new MMap();
        ContCancel tContCancel = new ContCancel();

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(mGrpContNo);
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        for(int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            MMap tMMap = tContCancel.prepareInsuredData(
                tLCInsuredSet.get(i).getContNo(),
                tLCInsuredSet.get(i).getInsuredNo(),
                mEdorNo);

            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备被保人"
                                + tLCInsuredSet.get(i).getInsuredNo()
                                + "数据失败！");
                return false;
            }
            map.add(tMMap);
        }

        //为屏蔽之前数据维护导致的数据不一致，需要同时从C、B表进行查询退保
        LBInsuredDB tLBInsuredDB = new LBInsuredDB();
        tLBInsuredDB.setGrpContNo(mGrpContNo);
        LBInsuredSet tLBInsuredSet = tLBInsuredDB.query();
        for(int i = 1; i <= tLBInsuredSet.size(); i++)
        {
            MMap tMMap = tContCancel.prepareInsuredData(
                tLBInsuredSet.get(i).getContNo(),
                tLBInsuredSet.get(i).getInsuredNo(),
                mEdorNo);

            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备被保人"
                                + tLBInsuredSet.get(i).getInsuredNo()
                                + "数据失败！");
                return false;
            }
            map.add(tMMap);
        }


        VData data = new VData();
        data.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, "INSERT"))
        {
            System.out.println(p.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
        return true;
    }

    /**
     * 个单退保
     */
    private boolean contCancel()
    {
        if(!checkData())
        {
            return false;
        }

        MMap map = new MMap();
        ContCancel tContCancel = new ContCancel();

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(mGrpContNo);
        LCContSet tLCContSet = tLCContDB.query();
        for(int i = 1; i <= tLCContSet.size(); i++)
        {
            MMap tMMap = tContCancel.prepareContData(
                tLCContSet.get(i).getContNo(),
                mEdorNo);

            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个单"
                                + tLCContSet.get(i).getContNo() + "数据失败！");
                return false;
            }
            map.add(tMMap);
        }

        //为屏蔽之前数据维护导致的数据不一致，需要同时从C、B表进行查询退保
        LBContDB tLBContDB = new LBContDB();
        tLBContDB.setGrpContNo(mGrpContNo);
        LBContSet tLBContSet = tLBContDB.query();
        for(int i = 1; i <= tLBContSet.size(); i++)
        {
            MMap tMMap = tContCancel.prepareContData(
                tLBContSet.get(i).getContNo(),
                mEdorNo);

            if (tContCancel.mErrors.needDealError())
            {
                CError.buildErr(this, "准备个单"
                                + tLBContSet.get(i).getContNo() + "数据失败！");
                return false;
            }
            map.add(tMMap);
        }


        VData data = new VData();
        data.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, "INSERT"))
        {
            System.out.println(p.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }

        return true;
    }

    /**
     * mEdorNo和mGrpContNo都必须传入
     * @return boolean
     */
    private boolean checkData()
    {
        if(mEdorNo == null || mEdorNo.equals("")
            || mGrpContNo == null || mGrpContNo.equals(""))
         {
             mErrors.addOneError("mEdorNo和mGrpContNo都必须传入");
             return false;
         }

         return true;
    }

    public static void main(String args[])
    {
//        String sql = "  select edorNo, grpContNo from LBGrpCont "
//                     + "where grpContNo in "
//                     + "   (select grpContNo from LPGrpEdorItem "
//                     + "   where edorType = 'WT') "
//                     + "order by edorNo ";
//        System.out.println(sql);
//        SSRS tSSRS = new ExeSQL().execSQL(sql);

        WTMaintainBL bl = null;
        String[][] tables = {{"20060118000001", "0000020802"},
                            {"20060220000002", "0000037601"}
        };
        for(int i = 0; i < tables.length; i++)
//        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
//            bl = new WTMaintainBL(tSSRS.GetText(i, 1), tSSRS.GetText(i, 2));
            bl = new WTMaintainBL(tables[i][0], tables[i][1]);
            if(!bl.polCancel())
            {
                System.out.println(bl.mErrors.getErrContent());
            }
            else
            {
                System.out.println("All OK");
            }
            if(!bl.insuredCancel())
            {
                System.out.println(bl.mErrors.getErrContent());
            }
            else
            {
                System.out.println("All OK");
            }
            if(!bl.contCancel())
            {
                System.out.println(bl.mErrors.getErrContent());
            }
            else
            {
                System.out.println("All OK");
            }
        }
        if(bl.mErrors.needDealError())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("全部处理成功");
        }
    }

}
