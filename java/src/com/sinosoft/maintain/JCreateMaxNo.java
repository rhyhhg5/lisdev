/**
 * 2007-12-5
 */
package com.sinosoft.maintain;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * @author LY
 *
 */
public class JCreateMaxNo
{
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        System.out.println("begin...");
        String strManageCom = "86120000";
        String tLimit = PubFun.getNoLimit(strManageCom);
        String polNo = PubFun1.CreateMaxNo("POLNO", tLimit);
        System.out.println(polNo);
        System.out.println("finished...");
    }

}
