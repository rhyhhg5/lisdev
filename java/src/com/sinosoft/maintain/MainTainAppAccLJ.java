package com.sinosoft.maintain;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import java.util.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 余额抵扣的实收实付保费维护，将实收实付维护成除了YEI、YEO的LJAGetEndrose之和，
 * 无实收实付时，本类可以生成实收实付
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class MainTainAppAccLJ
{
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private String mManageCom = null;

    private String mApproveCode = null;

    private String mApproveDate = null;

    private String mAgentCode = null;

    private String mAgentGroup = null;

    private String mAgentCom = null;

    private String mAgentType = null;

    private String mAppntNo = null;

    private CErrors mErrors = new CErrors();

    /**
     * 生成实收数据
     * @param cLJAGetEndorseSchema LJAGetEndorseSchema
     * @return boolean
     */
    public boolean setLJAPay(LJAGetEndorseSchema cLJAGetEndorseSchema)
    {
        LJAPaySchema tLJAPaySchema = getLJAPay(cLJAGetEndorseSchema);
        double bqMoney = Math.abs(getSumGetMoney(cLJAGetEndorseSchema));

        //若没有实收记录，则生成实收记录
        if(tLJAPaySchema == null)
        {
            if(!getObjectVar(cLJAGetEndorseSchema.getOtherNoType(),
                             cLJAGetEndorseSchema.getOtherNo()))
            {
                return false;
            }

            tLJAPaySchema = new LJAPaySchema();
            tLJAPaySchema.setPayNo(PubFun1.CreateMaxNo("PAYNO", null));
            tLJAPaySchema.setGetNoticeNo(PubFun1.CreateMaxNo("PAYNOTICENO", null));
            tLJAPaySchema.setIncomeNo(cLJAGetEndorseSchema.getOtherNo());
            tLJAPaySchema.setIncomeType(cLJAGetEndorseSchema.getOtherNoType());
            tLJAPaySchema.setManageCom(mManageCom);
            tLJAPaySchema.setApproveCode(mApproveCode);
            tLJAPaySchema.setApproveDate(mApproveDate);
            tLJAPaySchema.setAgentCode(mAgentCode);
            tLJAPaySchema.setAgentGroup(mAgentGroup);
            tLJAPaySchema.setAgentCom(mAgentCom);
            tLJAPaySchema.setAgentType(mAgentType);
            tLJAPaySchema.setAppntNo(mAppntNo);
            tLJAPaySchema.setRiskCode(BQ.FILLDATA);
            tLJAPaySchema.setPayDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAPaySchema.setEnterAccDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAPaySchema.setConfDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAPaySchema.setOperator(cLJAGetEndorseSchema.getOperator());
            tLJAPaySchema.setMakeDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAPaySchema.setMakeTime(cLJAGetEndorseSchema.getMakeTime());
            tLJAPaySchema.setModifyDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAPaySchema.setModifyTime(cLJAGetEndorseSchema.getMakeTime());
        }
        else
        {
            //若LJAPay钱与LJAGetEndorse中非余额部分相等，则不需要处理
            if(Math.abs(bqMoney - tLJAPaySchema.getSumActuPayMoney()) < 0.0001)
            {
                System.out.println("本条数据正常");
                return true;
            }

        }

        tLJAPaySchema.setSumActuPayMoney(bqMoney);

        MMap map = new MMap();
        map.put(tLJAPaySchema, "DELETE&INSERT");

        VData v = new VData();
        v.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(v, ""))
        {
            CError tError = new CError();
            tError.moduleName = "Untitled1";
            tError.functionName = "setLJAPay";
            tError.errorMessage = "生成实收出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 保全确认时把应转为实收
     * 若非定期结算，则将实付修改为总保费
     */
    private boolean setLJAGet(LJAGetEndorseSchema cLJAGetEndorseSchema)
    {
        LJAGetSchema tLJAGetSchema
            = com.sinosoft.lis.bq.CommonBL
              .getLJAGet(cLJAGetEndorseSchema.getOtherNo(),
                         cLJAGetEndorseSchema.getOtherNoType());
        double bqMoney = Math.abs(getSumGetMoney(cLJAGetEndorseSchema));

        if(tLJAGetSchema == null)
        {
            if(!getObjectVar(cLJAGetEndorseSchema.getOtherNoType(),
                             cLJAGetEndorseSchema.getOtherNo()))
            {
                return false;
            }

            tLJAGetSchema = new LJAGetSchema();
            tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("GETNO", null));
            tLJAGetSchema.setOtherNo(cLJAGetEndorseSchema.getOtherNo());
            tLJAGetSchema.setOtherNoType(cLJAGetEndorseSchema.getOtherNoType());
            tLJAGetSchema.setPayMode("1");  //现金
            tLJAGetSchema.setManageCom(mManageCom);
            tLJAGetSchema.setApproveCode(mApproveCode);
            tLJAGetSchema.setApproveDate(mApproveDate);
            tLJAGetSchema.setAgentCode(mAgentCode);
            tLJAGetSchema.setAgentGroup(mAgentGroup);
            tLJAGetSchema.setAgentCom(mAgentCom);
            tLJAGetSchema.setAgentType(mAgentType);
            tLJAGetSchema.setAppntNo(mAppntNo);
            tLJAGetSchema.setShouldDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAGetSchema.setEnterAccDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAGetSchema.setConfDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAGetSchema.setOperator(cLJAGetEndorseSchema.getOperator());
            tLJAGetSchema.setMakeDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAGetSchema.setMakeTime(cLJAGetEndorseSchema.getMakeTime());
            tLJAGetSchema.setModifyDate(cLJAGetEndorseSchema.getMakeDate());
            tLJAGetSchema.setModifyTime(cLJAGetEndorseSchema.getMakeTime());
        }
        else
        {
            //若LJAGet钱与LJAGetEndorse中非余额部分相等，则不需要处理
            if(Math.abs(bqMoney - tLJAGetSchema.getSumGetMoney()) < 0.0001)
            {
                System.out.println("本条数据正常");
                return true;
            }
        }

        tLJAGetSchema.setSumGetMoney(bqMoney);

        MMap map = new MMap();
        map.put(tLJAGetSchema, "DELETE&INSERT");

        VData v = new VData();
        v.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(v, ""))
        {
            CError tError = new CError();
            tError.moduleName = "Untitled1";
            tError.functionName = "setLJAGet";
            tError.errorMessage = "生成实付出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSumGetMoney
     *
     * @param cLJAGetEndorseSchema LJAGetEndorseSchema
     * @return double
     */
    private double getSumGetMoney(LJAGetEndorseSchema cLJAGetEndorseSchema)
    {
        String sql = "select sum(GetMoney) "
                     + "from LJAGetEndorse "
                     + "where EndorsementNo = '"
                     + cLJAGetEndorseSchema.getOtherNo() + "' "
                     + "   and FeeOperationType not in('YEI', 'YEO') ";
        return Double.parseDouble(new ExeSQL().getOneValue(sql));
    }

    /**
     * 查询实例便利的值
     * 主要是生成实付应付所需的保单变量
     * @return boolean
     */
    private boolean getObjectVar(String cContType, String cEdorNo)
    {
        String sql = "";
        if(BQ.NOTICETYPE_P.equals(cContType))
        {
            sql = "select '' EdorNo, a.* from LCCont a, LPEdorItem b "
                  + "where a.ContNo = b.ContNo "
                  + "   and b.EdorNo = '" + cEdorNo + "' "
                  + "union "
                  + "select a.* from LBCont a, LPEdorItem b "
                  + "where a.ContNo = b.ContNo "
                  + "   and b.EdorNo = '" + cEdorNo + "' ";
            System.out.println(sql);
            LBContSet set = new LBContDB().executeQuery(sql);
            if(set.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ValidateFinanceData";
                tError.functionName = "getObjectVar";
                tError.errorMessage = "生成财务数据时没有查询到保单信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            mManageCom = set.get(1).getManageCom();
            mApproveCode = set.get(1).getApproveCode();
            mApproveDate = set.get(1).getApproveDate();
            mAgentCode = set.get(1).getAgentCode();
            mAgentGroup = set.get(1).getAgentGroup();
            mAgentCom = set.get(1).getAgentCom();
            mAgentType = set.get(1).getAgentGroup();
            mAppntNo = set.get(1).getAppntNo();
        }
        else
        {
            sql = "select '' EdorNo, a.* from LCGrpCont a, LPGrpEdorItem b "
                  + "where a.GrpContNo = b.GrpContNo "
                  + "   and b.EdorNo = '" + cEdorNo + "' "
                  + "union "
                  + "select a.* from LBGrpCont a, LPGrpEdorItem b "
                  + "where a.GrpContNo = b.GrpContNo "
                  + "   and b.EdorNo = '" + cEdorNo + "' ";
            System.out.println(sql);

            LBGrpContSet set = new LBGrpContDB().executeQuery(sql);
            if(set.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ValidateFinanceData";
                tError.functionName = "getObjectVar";
                tError.errorMessage = "生成财务数据时没有查询到保单信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            mManageCom = set.get(1).getManageCom();
            mApproveCode = set.get(1).getApproveCode();
            mApproveDate = set.get(1).getApproveDate();
            mAgentCode = set.get(1).getAgentCode();
            mAgentGroup = set.get(1).getAgentGroup();
            mAgentCom = set.get(1).getAgentCom();
            mAgentType = set.get(1).getAgentGroup();
            mAppntNo = set.get(1).getAppntNo();
        }

        return true;
    }

    /**
     * getLJAPay
     *
     * @param cEdorNo String
     * @return LJAPaySchema
     */
    private LJAPaySchema getLJAPay(LJAGetEndorseSchema cLJAGetEndorseSchema)
    {
        String sql = "select * "
                     + "from LJAPay "
                     + "where InComeNo = '"
                     + cLJAGetEndorseSchema.getOtherNo() + "' ";
        System.out.println(sql);
        return new LJAPayDB().executeQuery(sql).get(1);
    }

    /**
     * 余额抵扣的实收保费维护
     * @return boolean
     */
    private boolean maintain()
    {
        String sql = "select * from LJAGetEndorse where FeeOperationType in('YEI','YEO') ";
        LJAGetEndorseSet set = new LJAGetEndorseDB().executeQuery(sql);

        for(int i = 1; i <= set.size(); i++)
        {
            System.out.println("\n\n处理工单" + i);
            double money = this.getSumGetMoney(set.get(i));
            if(money > 0)
            {
                if(!setLJAPay(set.get(i)))
                {
                    System.out.println(mErrors.getErrContent());
                    return false;
                }
            }
            else if(money < 0)
            {
                if(!setLJAGet(set.get(i)))
                {
                    System.out.println(mErrors.getErrContent());
                    return false;
                }
            }
        }

        return true;
    }

    public static void main(String args[])
    {
        MainTainAppAccLJ bl = new MainTainAppAccLJ();
        if(!bl.maintain())
        {
            System.out.println("维护失败" + bl.mErrors.getErrContent());
        }
    }
}
