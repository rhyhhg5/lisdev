/**
 * 
 */
package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * @author maning
 * 
 */
public class LLHospOutpatientRegister implements ServiceInterface {

	/**
	 * 
	 */
	public LLHospOutpatientRegister() {
		// TODO Auto-generated constructor stub
	}

	public CErrors mErrors = new CErrors();

	private VData mInputData = new VData();

	public VData mResult = new VData();

	private GlobalInput mGlobalInput = new GlobalInput();

	private MMap mMMap = new MMap();

	// 传入报文
	private Document mInXmlDoc;

	// 报文类型
	private String mDocType = "";

	// 处理标志
	private boolean mDealState = true;

	// 返回类型代码
	private String mResponseCode = "1";

	// 请求类型
	private String mRequestType = "";

	// 错误描述
	private String mErrorMessage = "";

	// 交互编码
	private String mTransactionNum = "";

	// 交互编码中医院代码
	private String mTranHospCode = "";

	// 报文BASE_PART部分
	private Element mBaseData;

	// 报文保单部分
	private Element mContList;

	// 报文处方部分
	private Element mPrescriptionList;

	// 报文费用明细
	private Element mFeeItemList;

	// 机构编码
	private String mManageCom = "";
    private String newManageCom="86210100";//#3470 增加支公司
	// 客户号
	private String mInsuredNo = "";

	// 保单信息
	private LCPolSet mLCPolSet = new LCPolSet();

	// 医院编码
	private String mHospitalCode = "";

	// 出险日期
	private String mAccDate = "";

	// 临时案件号
	private String mTCaseNo = "";

	// 案件处理人
	private String mHandler = "";

	// 医院信息
	private LDHospitalDB mLDHospitalDB = new LDHospitalDB();

	// 被保险人信息
	private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

	// 疾病信息
	private LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();

	// 手术信息
	private LLOperationSchema mLLOperationSchema = new LLOperationSchema();

	// 意外信息
	private LLAccidentSchema mLLAccidentSchema = new LLAccidentSchema();

	// 账单表
	private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();

	// 社保账单
	private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();

	// 社保明细
	private LLFeeOtherItemSet mLLFeeOtherItemSet = new LLFeeOtherItemSet();

	// 医保通案件号
	private LLHospAccTraceSchema mLLHospAccTraceSchema = new LLHospAccTraceSchema();

	private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
	

	// 事件关联号
	private String mCaseRelaNo = "";

	// 账单号码
	private String mReceiptNo = "";

	// 已存在账单的案件号
	private String mOldCaseNo = "";

	private String mLimit = "";

	private String mMakeDate = PubFun.getCurrentDate();

	private String mMakeTime = PubFun.getCurrentTime();

	// 处理类型 1 实时 2非实时
	private String mDealType = "1";

	private String mGetRate = "";
	
	private double mRealPay= 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sinosoft.lis.yibaotong.ServiceInterface#service(org.jdom.Document)
	 */
	public Document service(Document pInXmlDoc) {
		System.out.println("LLHospOutpatientRegister--service");
		mInXmlDoc = pInXmlDoc;

		try {
			if (!getInputData(mInXmlDoc)) {
				mDealState = false;
			} else {
				if (!checkData()) {
					mDealState = false;
				} else {
					if (!dealData()) {
						mDealState = false;
					}
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			mDealState = false;
			mResponseCode = "E";
			buildError("service()", "系统未知错误");
		} finally {
			return createXML();
		}
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param cInXml
	 *            Document
	 * @return boolean
	 */
	private boolean getInputData(Document cInXml) throws Exception {
		System.out.println("LLHospOutpatientRegister--getInputData");
		if (cInXml == null) {
			buildError("getInputData()", "未获得报文");
			return false;
		}

		Element tRootData = cInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");

		// 获取HEAD部分内容
		Element tHeadData = tRootData.getChild("HEAD");

		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

		System.out.println("LLHospOutpatientRegister--TransactionNum:"
				+ mTransactionNum);

		// 获取BODY部分内容
		Element tBodyData = tRootData.getChild("BODY");
		mBaseData = tBodyData.getChild("BASE_PART");
		mContList = tBodyData.getChild("INSURANCE_POLICY_LIST");
		mPrescriptionList = tBodyData.getChild("PRESCRIPTION_LIST");
		mFeeItemList = tBodyData.getChild("FEEITEM_LIST");
		return true;
	}

	/**
	 * 校验基本数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() throws Exception {
		System.out.println("LLHospAutoRegister--checkData");
		if (!"REQUEST".equals(mDocType)) {
			buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
			return false;
		}

		if (!"11".equals(mRequestType)) {
			buildError("checkData()", "【请求类型】的值不存在或匹配错误");
			return false;
		}

		if (mTransactionNum == null || mTransactionNum == ""
				|| mTransactionNum.length() < 9) {
			buildError("checkData()", "【交互编码】编码错误");
			return false;
		}

		if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
			buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
			return false;
		}

		mTranHospCode = mTransactionNum.substring(2, 9);

		LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
		tLLHospComRightDB.setHospitCode(mTranHospCode);
		tLLHospComRightDB.setRequestType(mRequestType);
		tLLHospComRightDB.setState("0");
		LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
		tLLHospComRightSet = tLLHospComRightDB.query();

		if (tLLHospComRightSet.size() < 1) {
			buildError("checkData()", "该医院无通信权限");
			return false;
		}

		mLDHospitalDB.setHospitCode(mTranHospCode);
		if (!mLDHospitalDB.getInfo()) {
			buildError("checkData()", "医院查询失败");
			return false;
		}

		LDCode1DB tLDCode1DB = new LDCode1DB();
		tLDCode1DB.setCodeType("ybt_service");
		tLDCode1DB.setCode("11");
		tLDCode1DB.setCode1(mTranHospCode);

		if (!tLDCode1DB.getInfo()) {
			buildError("checkData()", "医院配置信息查询失败");
			return false;
		}

		mManageCom = tLDCode1DB.getComCode();
		if ("".equals(mManageCom) || mManageCom.length() < 4) {
			buildError("checkData()", "医院所属机构错误");
			return false;
		}

		return true;
	}

	/**
	 * 处理报文
	 * 
	 * @return boolean
	 */
	private boolean dealData() throws Exception {
		System.out.println("LLHospOutpatientRegister--dealData");
		// 处理保单信息
		if (!dealCont()) {
			return false;
		}

		// 案件受理
		if (!caseRegister()) {
			return false;
		}

		// 处理账单信息
		if (!dealFee()) {
			return false;
		}

		// 生成案件信息
		if (!dealCase()) {
			return false;
		}

		return true;
	}

	/**
	 * 解析校验保单部分
	 * 
	 * @return boolean
	 */
	private boolean dealCont() throws Exception {
		System.out.println("LLHospOutpatientRegister--dealCont");
		List tContList = new ArrayList();
		tContList = (List) mContList.getChildren();

		// 循环校验每个保单的数据,将正常保单计入mLCPolSet
		out: for (int i = 0; i < tContList.size(); i++) {
			Element tContData = (Element) tContList.get(i);

			String tInsuredNo = tContData.getChildText("INSUREDNO");
			if (!"".equals(mInsuredNo)) {
				if (!mInsuredNo.equals(tInsuredNo)) {
					buildError("dealCont", "一次只能处理一个客户的信息");
					return false;
				}
			} else {
				mInsuredNo = tInsuredNo;
			}

			if (mInsuredNo == null || "".equals(mInsuredNo)) {
				buildError("dealCont", "【客户号】的值不能为空");
				return false;
			}

			// 用客户号校验该客户是否有正在处理的保全项目,返回的是工单号，而且校验的是全部保单，考虑以后是否按照保单校验
			if (!"".equals(LLCaseCommon.checkPerson(mInsuredNo))) {
				buildError("dealCont", "该客户正在进行保全处理，不能进行理赔");
				return false;
			}

			String tGrpContNo = tContData
					.getChildText("INSURANCE_POLICY_NUMBER");
			if (tGrpContNo == null || "".equals(tGrpContNo)) {
				buildError("dealCont", "【保单号】的值不能为空");
				return false;
			}

			String tContNo = tContData
					.getChildText("SUB_INSURANCE_POLICY_NUMBER");
			if (tContNo == null || "".equals(tContNo)) {
				buildError("dealCont", "【分单号】的值不能为空");
				return false;
			}

			String tRiskCode = tContData.getChildText("RISK_CODE");
			if (tRiskCode == null || "".equals(tRiskCode)) {
				buildError("dealCont", "【险种编码】的值不能为空");
				return false;
			}

			String tPolSQL = "select a.polno from lcpol a where (a.managecom like '"
					+ mManageCom
					+ "%' or a.managecom like '"
					+ newManageCom
					+ "%') and poltypeflag='0' and a.contno='"
					+ tContNo
					+ "' and a.riskcode='"
					+ tRiskCode
					+ "' and a.insuredno='"
					+ mInsuredNo
					+ "' and a.appflag='1' and a.stateflag='1' "
					+ " and exists (select 1 from llhospcomright where riskcode=a.riskcode and hospitcode='"
					+ mTranHospCode + "' and requesttype='11' and state='0')";		
			
			System.out.println(tPolSQL);

			ExeSQL tExeSQL = new ExeSQL();
			SSRS tPolSSRS = tExeSQL.execSQL(tPolSQL);

			if (tPolSSRS.getMaxRow() != 1) {
				buildError("dealCont", "分单" + tContNo + "下险种" + tRiskCode
						+ "查询失败");
				return false;
			}

			// LCPolBL tLCPolDB = new LCPolBL();
			// LCPolSet tLCPolSet = tLCPolDB.executeQuery(tPolSQL);
			// if (tLCPolSet.size() <= 0) {
			// buildError("dealCont", "分单" + tContNo + "查询失败");
			// return false;
			// }

			String tPolNo = tPolSSRS.GetText(1, 1);

			if ("".equals(tPolNo)) {
				buildError("dealCont", "分单" + tContNo + "下险种" + tRiskCode
						+ "查询失败");
				return false;
			}

			LCPolBL tLCPolBL = new LCPolBL();
			tLCPolBL.setPolNo(tPolNo);

			if (!tLCPolBL.getInfo()) {
				buildError("dealCont", "分单" + tContNo + "下险种" + tRiskCode
						+ "查询失败");
				return false;
			}

			LCPolSchema tLCPolSchema = tLCPolBL.getSchema();

			String tPolNoNew = tLCPolSchema.getPolNo();

			// 剔除重复的保单
			for (int j = 1; j <= mLCPolSet.size(); j++) {
				String tPolNoOld = mLCPolSet.get(j).getPolNo();
				if (tPolNoNew.equals(tPolNoOld)) {
					continue out;
				}
			}
			mLCPolSet.add(tLCPolSchema);
		}

		return true;
	}

	/**
	 * 解析受理信息
	 * 
	 * @return boolean
	 */
	private boolean caseRegister() throws Exception {
		System.out.println("LLHospOutpatientRegister--caseRegister");
		mHospitalCode = mBaseData.getChildText("HOSPITAL_NUMBER");
		if (!mTranHospCode.equals(mHospitalCode)) {
			buildError("caseRegister", "【医院编码】的值与【交互编码】不符");
			return false;
		}

		mAccDate = mBaseData.getChildText("ACCDATE");
		if (mAccDate == null || "".equals(mAccDate)) {
			buildError("caseRegister", "【出险日期】的值不能为空");
			return false;
		}
		if (!PubFun.checkDateForm(mAccDate)) {
			buildError("caseRegister", "【出险日期】格式错误");
			return false;
		}

		mDealType = mBaseData.getChildText("DEALTYPE");

		if (mDealType == null || "".equals(mDealType)) {
			buildError("caseRegister", "【处理类型】的值不能为空");
			return false;
		}

		if (!"1".equals(mDealType) && !"2".equals(mDealType)) {
			buildError("caseRegister", "【处理类型】的值错误");
			return false;
		}

		mReceiptNo = mBaseData.getChildText("RECEIPTNO");
		if (mReceiptNo == null || "".equals(mReceiptNo)) {
			buildError("dealFee", "【账单号码】的值不能为空");
			return false;
		}

		String tReceiptNoSQL = "select a.tcaseno from llhospacctrace a,llfeemain b "
				+ " where a.caseno=b.caseno and a.confirmstate='0' "
				+ " and a.hospitcode='"
				+ mTranHospCode
				+ "' and b.receiptno='"
				+ mReceiptNo + "'";
		System.out.println(tReceiptNoSQL);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tReceiptSSRS = tExeSQL.execSQL(tReceiptNoSQL);

		if (tReceiptSSRS.getMaxRow() > 0) {
			mOldCaseNo = tReceiptSSRS.GetText(1, 1);
			return false;
		}



      //不取传送过来的处理人了，取核心自己配置的处理人  #1557  2013-10-14
	  //mHandler = mBaseData.getChildText("STAFF_NUMBER");
      String mHandlerSQl = "select Code from ldcode2 where codetype = 'SYybt_Handler'";
      ExeSQL exesql = new ExeSQL();
      mHandler = exesql.getOneValue(mHandlerSQl);
      
      
		if (mHandler == null || "".equals(mHandler)) {
			buildError("caseRegister", "【案件处理人】的值不能为空");
			return false;
		}

		LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
		tLLClaimUserDB.setUserCode(mHandler);
		tLLClaimUserDB.setClaimDeal("1");
		tLLClaimUserDB.setHandleFlag("1");
		tLLClaimUserDB.setStateFlag("1");
		// tLLClaimUserDB.setComCode(mManageCom);
		// tLLClaimUserDB.setComCode("86");
		System.out.println("理赔自动理算--------------------------------"
				+ tLLClaimUserDB.getComCode()
				+ "-------------------------------");
		LLClaimUserSet tLLClaimUserSet = tLLClaimUserDB.query();

		if (tLLClaimUserSet.size() <= 0) {
			buildError("caseRegister", "案件处理人查询失败或没有权限");
			return false;
		}

		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setInsuredNo(mInsuredNo);
		LCInsuredSet tLCinsuredSet = tLCInsuredDB.query();
		if (tLCinsuredSet.size() < 1) {
			buildError("caseRegister", "被保险人查询失败");
			return false;
		}
		// 只需要取一个就可以，只要基本信息
		mLCInsuredSchema = tLCinsuredSet.get(1);

		mLimit = PubFun.getNoLimit(mManageCom);
		mTCaseNo = PubFun1.CreateMaxNo("TCASENO", mLimit);

		mCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", mLimit);

		if (!getSubInfo()) {
			return false;
		}

		return true;
	}

	/**
	 * 生成事件信息、疾病信息、手术信息
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getSubInfo() throws Exception {
		System.out.println("LLHospOutpatientRegister--getSubInfo");
		// 疾病信息
		String tDiseaseCode = mBaseData.getChildText("DISEASECODE");
		if ("".equals(tDiseaseCode) || "null".equals(tDiseaseCode)) {
			buildError("getSubInfo", "【疾病编码】的值不能为空");
			return false;
		}

		LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
		tLDDiseaseDB.setICDCode(tDiseaseCode);
		if (!tLDDiseaseDB.getInfo()) {
			buildError("getSubInfo", "【疾病编码】不存在");
			return false;
		}

		String tCureNo = PubFun1.CreateMaxNo("CURENO", mLimit);
		mLLCaseCureSchema.setSerialNo(tCureNo);
		mLLCaseCureSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
		mLLCaseCureSchema.setCustomerName(mLCInsuredSchema.getName());
		mLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
		mLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());
		mLLCaseCureSchema.setHospitalCode(mLDHospitalDB.getHospitCode());
		mLLCaseCureSchema.setHospitalName(mLDHospitalDB.getHospitName());
		mLLCaseCureSchema.setCaseNo(mTCaseNo);
		mLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo);
		mLLCaseCureSchema.setSeriousFlag("0");
		mLLCaseCureSchema.setOperator(mHandler);
		mLLCaseCureSchema.setMngCom(mManageCom);
		mLLCaseCureSchema.setMakeDate(mMakeDate);
		mLLCaseCureSchema.setMakeTime(mMakeTime);
		mLLCaseCureSchema.setModifyDate(mMakeDate);
		mLLCaseCureSchema.setModifyTime(mMakeTime);

		// 手术信息
		String tOperationCode = mBaseData.getChildText("OPERATIONCODE");
		if (!"".equals(tOperationCode) && !"null".equals(tOperationCode)) {
			LDICDOPSDB tLDICDOPSDB = new LDICDOPSDB();
			tLDICDOPSDB.setICDOPSCode(tOperationCode);
			if (!tLDICDOPSDB.getInfo()) {
				buildError("getSubInfo", "【手术编码】不存在");
				return false;
			}

			String tOperationNo = PubFun1.CreateMaxNo("OPERATION", mLimit);
			mLLOperationSchema.setSerialNo(tOperationNo);
			mLLOperationSchema.setCaseNo(mTCaseNo);
			mLLOperationSchema.setCaseRelaNo(mCaseRelaNo);
			mLLOperationSchema.setOperationCode(tLDICDOPSDB.getICDOPSCode());
			mLLOperationSchema.setOperationName(tLDICDOPSDB.getICDOPSName());
			mLLOperationSchema.setOpLevel(tLDICDOPSDB.getFrequencyFlag());
			mLLOperationSchema.setOperator(mHandler);
			mLLOperationSchema.setMngCom(mManageCom);
			mLLOperationSchema.setMakeDate(mMakeDate);
			mLLOperationSchema.setMakeTime(mMakeTime);
			mLLOperationSchema.setModifyDate(mMakeDate);
			mLLOperationSchema.setModifyTime(mMakeTime);

		}

		// 事件类型只能是疾病、意外或者空
		String tAccidentType = mBaseData.getChildText("ACCIDENTTYPE");

		// 意外信息
		String tAccidentCode = mBaseData.getChildText("ACCIDENTNO");
		if ("2".equals(tAccidentType)) {
			if ("".equals(tAccidentCode) || "null".equals(tAccidentCode)) {
				buildError("getSubInfo", "【意外编码】不能为空");
				return false;
			}

			LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
			tLLAccidentTypeDB.setAccidentNo(tAccidentCode);
			if (!tLLAccidentTypeDB.getInfo()) {
				buildError("getSubInfo", "【意外编码】不存在");
				return false;
			}

			String tAccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mLimit);
			mLLAccidentSchema.setAccidentNo(tAccidentNo);
			mLLAccidentSchema.setCaseNo(mTCaseNo);
			mLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
			mLLAccidentSchema.setCode(tAccidentCode);
			mLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());
		}

		return true;
	}

	/**
	 * 解析账单信息
	 * 
	 * @return boolean
	 */
	private boolean dealFee() throws Exception {
		System.out.println("LLHospOutpatientRegister--dealFee");
		String tFeeType = mBaseData.getChildText("FEETYPE");
		if (!"1".equals(tFeeType) && !"2".equals(tFeeType)) {
			buildError("dealFee", "【账单类型】的值不存在");
			return false;
		}

		String tInHosNo = "";
		if ("2".equals(tFeeType)) {
			tInHosNo = mBaseData.getChildText("PATIENT_NUMBER");
		}

		// 总费用
		String tSumFee = mBaseData.getChildText("TOTAL_MEDICAL_COST");
		if (tSumFee == null || "".equals(tSumFee)) {
			buildError("dealFee", "【总费用】的值不能为空");
			return false;
		}
		double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee), 2);

		if (tSumFeeValue <= 0) {
			buildError("dealFee", "【总费用】的值必须大于0");
			return false;
		}

		mLLFeeMainSchema.setSumFee(tSumFeeValue);

		if (!dealAccTrace()) {
			return false;
		}

		String tMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
		mLLFeeMainSchema.setMainFeeNo(tMainFeeNo);
		mLLFeeMainSchema.setCaseNo(mTCaseNo);
		mLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
		mLLFeeMainSchema.setRgtNo(mTCaseNo);
		mLLFeeMainSchema.setCustomerName(mLCInsuredSchema.getName());
		mLLFeeMainSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
		mLLFeeMainSchema.setCustomerSex(mLCInsuredSchema.getSex());
		mLLFeeMainSchema.setHospitalCode(mLDHospitalDB.getHospitCode());
		mLLFeeMainSchema.setHospitalName(mLDHospitalDB.getHospitName());
		mLLFeeMainSchema.setHosGrade(mLDHospitalDB.getLevelCode());
		mLLFeeMainSchema.setReceiptNo(mReceiptNo);

		if (!"".equals(tInHosNo) && tInHosNo != null) {
			mLLFeeMainSchema.setInHosNo(tInHosNo);
		}

		mLLFeeMainSchema.setFeeType(tFeeType);
		// 账单类型默认简易社保结算
		mLLFeeMainSchema.setFeeAtti("4");
		mLLFeeMainSchema.setFeeDate(mMakeDate);
		mLLFeeMainSchema.setHospStartDate(mMakeDate);
		mLLFeeMainSchema
				.setHospEndDate(mMakeDate);
		// 计算实际住院天数
		int tRealHospDay = PubFun.calInterval(mLLFeeMainSchema
				.getHospStartDate(), mLLFeeMainSchema.getHospEndDate(), "D") + 1;
		mLLFeeMainSchema.setRealHospDate(tRealHospDay);
		mLLFeeMainSchema.setAge((int) LLCaseCommon.calAge(mLCInsuredSchema
				.getBirthday()));
		mLLFeeMainSchema.setSecurityNo(mLCInsuredSchema.getOthIDNo());
		mLLFeeMainSchema.setInsuredStat(mLCInsuredSchema.getInsuredStat());

		mLLFeeMainSchema.setOperator(mHandler);
		mLLFeeMainSchema.setMngCom(mManageCom);
		mLLFeeMainSchema.setMakeDate(mMakeDate);
		mLLFeeMainSchema.setMakeTime(mMakeTime);
		mLLFeeMainSchema.setModifyDate(mMakeDate);
		mLLFeeMainSchema.setModifyTime(mMakeTime);

		if ("4".equals(mLLFeeMainSchema.getFeeAtti())) {
			if (!getSecurityInfo()) {
				return false;
			}

			mLLFeeMainSchema.setRefuseAmnt(mLLSecurityReceiptSchema
					.getSelfPay2());
			mLLFeeMainSchema
					.setSelfAmnt(mLLSecurityReceiptSchema.getSelfAmnt());
		}

		return true;
	}

	/**
	 * 获取社保账单
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean getSecurityInfo() throws Exception {
		System.out.println("LLHospOutpatientRegister--getSecurityInfo");
		// 社保起付线
		String tGetLimit = mBaseData.getChildText("UNDERWAY_CRITERION");
		if (tGetLimit == null || "".equals(tGetLimit)) {
			buildError("getSecurityInfo", "【社保起付线】的值不能为空");
			return false;
		}
		double tGetLimitValue = Arith.round(Double.parseDouble(tGetLimit), 2);

		// 统筹支付金额
		String tLowAmnt = mBaseData.getChildText("LOWAMNT");
		if (tLowAmnt == null || "".equals(tLowAmnt)) {
			buildError("getSecurityInfo", "【低段金额】的值不能为空");
			return false;
		}
		double tLowAmntValue = Arith.round(Double.parseDouble(tLowAmnt), 2);

		// 统筹支付金额
		String tPlanFee = mBaseData.getChildText("BASE_INSURANCE");
		if (tPlanFee == null || "".equals(tPlanFee)) {
			buildError("getSecurityInfo", "【统筹支付金额】的值不能为空");
			return false;
		}
		double tPlanFeeValue = Arith.round(Double.parseDouble(tPlanFee), 2);

		// 统筹自付金额
		String tMidAmnt = mBaseData.getChildText("MIDAMNT");
		if (tMidAmnt == null || "".equals(tMidAmnt)) {
			buildError("getSecurityInfo", "【统筹自付金额】的值不能为空");
			return false;
		}
		double tMidAmntValue = Arith.round(Double.parseDouble(tMidAmnt), 2);

		// 大额救助支付金额
		String tHighAmnt2 = mBaseData.getChildText("HIGHAMNT2");
		if (tHighAmnt2 == null || "".equals(tHighAmnt2)) {
			buildError("getSecurityInfo", "【大额救助支付金额】的值不能为空");
			return false;
		}
		double tHighAmnt2Value = Arith.round(Double.parseDouble(tHighAmnt2), 2);

		// 大额救助自付金额
		String tHighAmnt1 = mBaseData.getChildText("HIGHAMNT1");
		if (tHighAmnt1 == null || "".equals(tHighAmnt1)) {
			buildError("getSecurityInfo", "【大额救助自付金额】的值不能为空");
			return false;
		}
		double tHighAmnt1Value = Arith.round(Double.parseDouble(tHighAmnt1), 2);

		// 自费金额
		String tSelfAmnt = mBaseData.getChildText("SELFAMNT");
		if (tSelfAmnt == null || "".equals(tSelfAmnt)) {
			buildError("getSecurityInfo", "【自费金额】的值不能为空");
			return false;
		}
		double tSelfAmntValue = Arith.round(Double.parseDouble(tSelfAmnt), 2);

		// 年度累计统筹基金医疗费用
		String tYearPlayFee = mBaseData.getChildText("YEAR_PLAY_FEE");
		if (tYearPlayFee == null || "".equals(tYearPlayFee)) {
			buildError("getSecurityInfo", "【年度累计统筹基金医疗费用】的值不能为空");
			return false;
		}
		double tYearPlayFeeValue = Arith.round(
				Double.parseDouble(tYearPlayFee), 2);

		// 补充医疗保险赔付金额(其他公司赔付的 没什么用)
		// String tSelfPay1 =
		// mBaseData.getChildText("COMPLEMENTARITY_INSURANCE");
		// if ("".equals(tSelfPay1) || "null".equals(tSelfPay1)) {
		// buildError("getSecurityInfo", "【补充医疗保险赔付金额】的值不能为空");
		// return false;
		// }
		// double tSelfPay1Value = Arith.round(Double.parseDouble(tSelfPay1),
		// 2);

		// 其他保险赔付金额
		String tOtherPay = mBaseData.getChildText("ELSE_INSURANCE_MONEY");
		if (tOtherPay == null || "".equals(tOtherPay)) {
			buildError("getSecurityInfo", "【其他保险赔付金额】的值不能为空");
			return false;
		}
		double tOtherPayValue = Arith.round(Double.parseDouble(tOtherPay), 2);

		// 总自费（社保外：全自费+部分自付）
		String tFeeOutSecu = mBaseData.getChildText("TOTAL_SELF_PAYMENT");
		if (tFeeOutSecu == null || "".equals(tFeeOutSecu)) {
			buildError("getSecurityInfo", "【总自费】的值不能为空");
			return false;
		}
		double tFeeOutSecuValue = Arith.round(Double.parseDouble(tFeeOutSecu),
				2);

		// 自付2(部分自付)
		double tSelfPay2Value = tFeeOutSecuValue - tSelfAmntValue;

		if (Arith.round(tFeeOutSecuValue + tLowAmntValue + tMidAmntValue
				+ tHighAmnt1Value + tPlanFeeValue + tOtherPayValue, 2) > mLLFeeMainSchema
				.getSumFee()) {
			buildError("getSecurityInfo", "账单各部分金额合计不能大于账单总费用");
			return false;
		}

		// if (tMidAmntValue + tHighAmnt1Value + tGetLimitValue >
		// tSelfPay1Value) {
		// buildError("getSecurityInfo",
		// "统筹自付金额+大额救助自付金额+社保起付线不能大于补充医疗保险赔付金额");
		// return false;
		// }

		mLLSecurityReceiptSchema
				.setFeeDetailNo(mLLFeeMainSchema.getMainFeeNo());
		mLLSecurityReceiptSchema.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());
		mLLSecurityReceiptSchema.setRgtNo(mTCaseNo);
		mLLSecurityReceiptSchema.setCaseNo(mTCaseNo);
		mLLSecurityReceiptSchema.setApplyAmnt(mLLFeeMainSchema.getSumFee());
		mLLSecurityReceiptSchema.setFeeInSecu(mLLSecurityReceiptSchema
				.getApplyAmnt()
				- tFeeOutSecuValue);
		mLLSecurityReceiptSchema.setFeeOutSecu(tFeeOutSecuValue);
		mLLSecurityReceiptSchema.setPlanFee(tPlanFeeValue);
		mLLSecurityReceiptSchema.setLowAmnt(tLowAmntValue);
		mLLSecurityReceiptSchema.setYearPlayFee(tYearPlayFeeValue);
		mLLSecurityReceiptSchema.setSelfPay2(tSelfPay2Value);
		mLLSecurityReceiptSchema.setSelfAmnt(tSelfAmntValue);
		mLLSecurityReceiptSchema.setGetLimit(tGetLimitValue);
		mLLSecurityReceiptSchema.setMidAmnt(tMidAmntValue);
		mLLSecurityReceiptSchema.setHighAmnt1(tHighAmnt1Value);
		mLLSecurityReceiptSchema.setHighAmnt2(tHighAmnt2Value);

		mLLSecurityReceiptSchema.setOperator(mHandler);
		mLLSecurityReceiptSchema.setMngCom(mManageCom);
		mLLSecurityReceiptSchema.setMakeDate(mMakeDate);
		mLLSecurityReceiptSchema.setMakeTime(mMakeTime);
		mLLSecurityReceiptSchema.setModifyDate(mMakeDate);
		mLLSecurityReceiptSchema.setModifyTime(mMakeTime);

		// 保存社保明细
		if (!saveFeeItem()) {
			return false;
		}

		return true;
	}

	/**
	 * 保存社保明细
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean saveFeeItem() throws Exception {
		System.out.println("LLHospOutpatientRegister--saveFeeItem");
		// 合计金额
		if (mLLSecurityReceiptSchema.getApplyAmnt() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getApplyAmnt());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("301");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("301");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}

		}

		// 统筹基金支付
		if (mLLSecurityReceiptSchema.getPlanFee() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getPlanFee());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("302");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("302");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		// 全自费
		if (mLLSecurityReceiptSchema.getSelfAmnt() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getSelfAmnt());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("305");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("305");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		// 自付二
		if (mLLSecurityReceiptSchema.getSelfPay2() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getSelfPay2());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("306");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("306");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		// 自付一
		if (mLLSecurityReceiptSchema.getSelfPay1() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getSelfPay1());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("307");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("307");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		// 起付线
		if (mLLSecurityReceiptSchema.getGetLimit() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getGetLimit());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("308");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("308");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		// 中段
		if (mLLSecurityReceiptSchema.getMidAmnt() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getMidAmnt());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("309");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("309");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		// 高段1
		if (mLLSecurityReceiptSchema.getHighAmnt1() > 0) {
			LLFeeOtherItemSchema tLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
			String tFeeDetailNo = PubFun1
					.CreateMaxNo("FEEDETAILNO", mManageCom);
			Reflections tReflection = new Reflections();
			tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
			tReflection.transFields(tLLFeeOtherItemSchema,
					mLLSecurityReceiptSchema);
			tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema
					.getHighAmnt1());
			tLLFeeOtherItemSchema.setItemClass("S");
			tLLFeeOtherItemSchema.setAvliFlag("0");
			tLLFeeOtherItemSchema.setItemCode("310");

			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("llsecufeeitem");
			tLDCodeDB.setCode("310");

			if (tLDCodeDB.getInfo()) {
				tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
				tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
				mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
			}
		}

		return true;
	}

	/**
	 * 存取账户信息
	 * 
	 * @return
	 * @throws Exception
	 */
	private boolean dealAccTrace() throws Exception {
		LLCaseCommon tLLCaseCommon = new LLCaseCommon();
		if (mLCPolSet.size() != 1) {
			buildError("dealAccTrace", "没有可以理算的保单");
			return false;
		}

		for (int i = 1; i <= mLCPolSet.size(); i++) {
			LCPolSchema tLCPolSchema = mLCPolSet.get(i);

			// 出险日期、入院日期、出院日期必须都在保单年度里-- 特需本来也不校验
			if (!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(), mAccDate)) {
				buildError("dealAccTrace", "出险日期不在保单有效期内");
				return false;
			}

			String tGetDutySQL = "select 1 from llhospcomright a,lmdutygetrela b,lmdutyget c,lcget d"
					+ " where a.riskcode='"
					+ tLCPolSchema.getRiskCode()
					+ "' and a.HospitCode='"
					+ mTranHospCode
					+ "' and a.RequestType='"
					+ mRequestType
					+ "' and a.state='0' and a.dutycode=b.dutycode"
					+ " and b.getdutycode=c.getdutycode and b.getdutycode=d.getdutycode"
					+ " and c.NeedAcc='1' and d.polno='"
					+ tLCPolSchema.getPolNo() + "'";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tGetDutyCode = tExeSQL.execSQL(tGetDutySQL);
			if (tGetDutyCode.getMaxRow() > 0) {
				GetAccAmnt tGetAccAmnt = new GetAccAmnt();
				mRealPay = tGetAccAmnt.getAccPay(
						tLCPolSchema.getPolNo(), mTCaseNo, mLLFeeMainSchema
								.getSumFee());
				mLCInsureAccTraceSet.add(tGetAccAmnt.getTrace());

				if (Arith.round(mRealPay, 2) != Arith.round(mLLFeeMainSchema
						.getSumFee(), 2)) {
					buildError("dealAccTrace", "账户余额不足");
					return false;
				}

				for (int j = 1; j <= mLCInsureAccTraceSet.size(); j++) {
					mLCInsureAccTraceSet.get(j).setPayDate(mMakeDate);
				}
			} else {
				buildError("dealAccTrace", "没有可以理算的责任");
				return false;
			}

		}
		return true;
	}

	/**
	 * 生成案件信息
	 * 
	 * @return boolean
	 * @throws Exception
	 */
	private boolean dealCase() throws Exception {
		mLLHospAccTraceSchema.setTCaseNo(mTCaseNo);
		mLLHospAccTraceSchema.setCaseNo(mTCaseNo);
		mLLHospAccTraceSchema.setHospitCode(mTranHospCode);
		mLLHospAccTraceSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
		mLLHospAccTraceSchema.setHandler(mHandler);
		mLLHospAccTraceSchema.setAppTranNo(mTransactionNum);
		mLLHospAccTraceSchema.setAppDate(mMakeDate);
		mLLHospAccTraceSchema.setAppTime(mMakeTime);
		
		mLLHospAccTraceSchema.setRealpay(mRealPay);
		
		// 为结算
		mLLHospAccTraceSchema.setDealState("0");
        //未确认
		mLLHospAccTraceSchema.setConfirmState("0");
		mLLHospAccTraceSchema.setMakeDate(mMakeDate);
		mLLHospAccTraceSchema.setMakeTime(mMakeTime);
		mLLHospAccTraceSchema.setModifyDate(mMakeDate);
		mLLHospAccTraceSchema.setModifyTime(mMakeTime);

		if (mLLOperationSchema.getCaseNo() != null
				&& !"".equals(mLLOperationSchema.getCaseNo())
				&& !"null".equals(mLLOperationSchema.getCaseNo())) {
			mMMap.put(mLLOperationSchema, "DELETE&INSERT");
		}

		if (mLLAccidentSchema.getCaseNo() != null
				&& !"".equals(mLLAccidentSchema.getCaseNo())
				&& !"null".equals(mLLAccidentSchema.getCaseNo())) {
			mMMap.put(mLLAccidentSchema, "DELETE&INSERT");
		}

		mMMap.put(mLLCaseCureSchema, "DELETE&INSERT");

		mMMap.put(mLLFeeMainSchema, "DELETE&INSERT");
		mMMap.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
		mMMap.put(mLCInsureAccTraceSet, "DELETE&INSERT");
		mMMap.put(mLLHospAccTraceSchema, "DELETE&INSERT");
		

		try {
			this.mInputData.clear();
			this.mInputData.add(mMMap);
			mResult.clear();
			mResult.add(this.mLLHospAccTraceSchema);
		} catch (Exception ex) {
			// @@错误处理
			buildError("dealCase", "准备提交数据失败");
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			buildError("dealCase", "数据提交失败");
			return false;
		}

		return true;
	}
	
	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState && "".equals(mOldCaseNo)) {
            return createFalseXML();
        } else {
        	try {
        	if (!"".equals(mOldCaseNo)) {
        		return createResultXML(mOldCaseNo);
        	} else {
        		return createResultXML(mTCaseNo);
        	}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
        DecimalFormat tDF = new DecimalFormat("0.##");

        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        //Head部分
        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText("1");
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText("");
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        //Body部分
        Element tBodyData = new Element("BODY");
        Element tBasePartData = new Element("BASE_PART");

        Element tInsuranceList = new Element("INSURANCE_POLICY_LIST");
        
        double tSumPay = 0.0;

        String tPaySQL =
                "select a.otherno,a.grpcontno,a.contno,a.riskcode,coalesce(sum(-a.money),0), "
                + " (select riskname from lmrisk where riskcode=a.riskcode)"
                + " From lcinsureacctrace a where a.otherno='" + aCaseNo +
                "' group by a.otherno,a.grpcontno,a.contno,a.riskcode";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tPaySQL);

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            Element tInsuranceData = new Element("INSURANCE_POLICY_DATA");

            Element tInsuranceNo = new Element("INSURANCE_POLICY_NUMBER");
            if ("00000000000000000000".equals(tSSRS.GetText(i, 2))) {
            	tInsuranceNo.addContent(tSSRS.GetText(i, 3));
            } else {
            	tInsuranceNo.addContent(tSSRS.GetText(i, 2));
            }
            tInsuranceData.addContent(tInsuranceNo);

            Element tRiskCode = new Element("RISK_CODE");
            tRiskCode.addContent(tSSRS.GetText(i, 4));
            tInsuranceData.addContent(tRiskCode);

            Element tRiskName = new Element("RISK_NAME");
            tRiskName.addContent(tSSRS.GetText(i, 6));
            tInsuranceData.addContent(tRiskName);
            
            tSumPay += Double.parseDouble(tSSRS.GetText(i, 5));
            
            Element tRealPay = new Element("TOTAL_PAYMENT_BY_INSURANCE");
            if ("1".equals(mDealType)) {
                tRealPay.addContent(tSSRS.GetText(i, 5));
            } else {
                tRealPay.addContent("0.00");
            }
            tInsuranceData.addContent(tRealPay);

            Element tCompensateInfo = new Element("COMPENSATE_INFORMATION");
            String tPayInfo = "";
            

            if ("1".equals(mDealType)) {
                tCompensateInfo.addContent(tPayInfo);
            } else {
                tCompensateInfo.addContent("");
            }
            tInsuranceData.addContent(tCompensateInfo);
            tInsuranceList.addContent(tInsuranceData);
        }
        
        Element tTotalPayMent = new Element("TOTAL_PAYMENT");
        
        String tSumRealPay = tDF.format(Arith.round(tSumPay, 2));
        if ("1".equals(mDealType)) {
            tTotalPayMent.setText(tSumRealPay);
        } else {
            tTotalPayMent.setText("0.00");
        }
        tBasePartData.addContent(tTotalPayMent);

        Element tCaseNo = new Element("CASE_NUMBER");
        tCaseNo.setText(aCaseNo);
        tBasePartData.addContent(tCaseNo);

        tBodyData.addContent(tBasePartData);

        tBodyData.addContent(tInsuranceList);

        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLHospOutpatientRegister";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:/xml/LLHosp11.xml"), "GBK");

            LLHospOutpatientRegister tBusinessDeal = new LLHospOutpatientRegister();
            long tStartMillis0 = System.currentTimeMillis();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

}
