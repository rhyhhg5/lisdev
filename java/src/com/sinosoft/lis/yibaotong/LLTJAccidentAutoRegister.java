package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;
import java.io.FileInputStream;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLTJAccidentAutoRegister implements ServiceInterface {
    public LLTJAccidentAutoRegister() {
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput(); 
    private MMap mMMap = new MMap();
    private String mErrorMessage = "";	//错误描述    
   
    private Document mInXmlDoc;	 			//传入报文    
    private String mDocType = "";			//报文类型    
    private boolean mDealState = true;		//处理标志    
    private String mResponseCode = "1";		//返回类型代码    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码
    private String mDealType = "1";    		//处理类型 1 实时 2非实时   
    
    private Element mBaseData;			//报文BASE_PART被保险人信息部分    
    private Element mReceiptList;		//报文RECEIPTLIST账单信息部分    
    private Element mBnfList;			//报文BNFLIST受益人部分   
    private Element mDiseaseList;		//报文DISEASELIST疾病信息部分
   // private Element mSecurityList;		//社保补充账单列表
    		
    private String mManageCom = "";				//机构编码   
    private String mOperator  = "";				//操作员
    private String mInsuredNo = "";			 	//客户号    
    private String mContNo = "";				//保单号     
    
    private String mClaimNo = "";
    private String mInsuredName = "" ;
    private String mInsuredSex = "" ;
    private String mInsuredBirthDay = "" ;
    private String mIDType = "" ;
    private String mIDNo = "" ;
    private String mInsuredStat = "" ;
    private String mAccDate = "";
    private String mAccidentType = "";
    private String mDeathDate = "";
    private String mDeformityCode = "";
    private String mAccidentNo = "";
    private String mRgtanName = "";
    private String mRgtantRelation = "";
    private String mRgtantPhone = "";
    private String mRgtMobile = "";
    private String mRgtantAddress = "";
    private String mRgtantPostCode = "";       
    private String mReasonCode = "";
    
    private String mBnfNo = "";
    private String mBnfName = ""; 
    private String mBnfSex = ""; 
    private String mBnfBirthDay = ""; 
    private String mBnfIDType = ""; 
    private String mBnfIDNO = ""; 
    private String mBnfRealtion = ""; 
    private String mBnfGrade = "";  
    private String mBnfRate = ""; 
    private String mSumPay = ""; 
    private String mBankCode = ""; 
    private String mAccNo = ""; 
    private String mAccName = ""; 
    private String mDiaoCha = "";
    private double mTableSumFee = 0;
    private String mClaimType=""; //业务标识符
    private String mClaimYear=""; //保单年度
   // private String mGrpContNoSB="";	//保单号  社保里面必传
    
    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();   
    private LLCasePayAdvancedTraceSet mLLCasePayAdvancedTraceSet = new LLCasePayAdvancedTraceSet(); 
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();    
    private LLCaseSet mLLCaseSet = new LLCaseSet();  
    private LJAGetSet mLJAGetSet = new LJAGetSet();      
    private LLCaseRelaSet mLLCaseRelaSet = new LLCaseRelaSet();
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    private LLSubReportSet mLLSubReportSet = null;
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();  
    LLTJAccidentSurvey tLLTJAccidentSurvey = new LLTJAccidentSurvey();
//  社保手工报销收据明细
	private LLSecurityReceiptSchema tLLSecurityReceiptSchema= new LLSecurityReceiptSchema();
    private String mCaseNo = "";
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public Document service(Document pInXmlDoc) {
        //兄弟别怨我，我也是被逼的，时间太紧，都写到一个类里了
        System.out.println("LLTJAccidentAutoRegister--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } 
            if (!"DiaoChaChengGong".equals(mDiaoCha)){
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                    	cancelCase();
                        mDealState = false;
                    }else {                                 	                    	
                    	mMMap.put(mLLCasePayAdvancedTraceSet, "INSERT");
                    	mMMap.put(mLLHospCaseSet, "DELETE&INSERT");
    	                mMMap.put(mLJAGetSet, "INSERT");
                    	mResult.clear();
                    	mResult.add(mMMap);
                    	try{
	                    	PubSubmit tPubSubmit = new PubSubmit();
	                        if (!tPubSubmit.submitData(mResult, "")) {
	                        	 mDealState = false;
	                        	 cancelCase();
	                        }
                    	} catch (Exception ex) {
                    		 cancelCase();
                             mDealState = false;
                             mResponseCode = "E";
                             buildError("service()", "系统未知错误");
                    	}
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            cancelCase();
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	return createXML();
        }
    }
    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState || "".equals(mClaimNo)) {
            return createFalseXML();
        } else {
        	try {
        	if (!"".equals(mClaimNo)) {
        		if("TJ04".equals(mRequestType)){
        			return tLLTJAccidentSurvey.createResultXML(mClaimNo, tLLTJAccidentSurvey.mCaseNo);
        		}
        		else{
        		return createResultXML(mClaimNo);
        		}
        	} else {
        		if("TJ04".equals(mRequestType)){
        			return tLLTJAccidentSurvey.createResultXML(mClaimNo, tLLTJAccidentSurvey.mCaseNo);
        		}
        		else{
        		return createResultXML(mClaimNo);
        		}
        	}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }     

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJAccidentAutoRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
        System.out.println("LLTJAccidentAutoRegister--TransactionNum:" + mTransactionNum);
        
        if("TJ04".equals(mRequestType))
        {
        	
        	tLLTJAccidentSurvey.service(cInXml);
        	if(!"null".equals(tLLTJAccidentSurvey.mErrors.getFirstError()) && !"".equals(tLLTJAccidentSurvey.mErrors.getFirstError()) && tLLTJAccidentSurvey.mErrors.getFirstError() != null){
        		buildError("getInputData()", tLLTJAccidentSurvey.mErrors.getFirstError());
        		return false;
        	}
        	else{
        		mDiaoCha = "DiaoChaChengGong";
        		mDealState = true;
        		mClaimNo = tLLTJAccidentSurvey.mClaimNo;
        	}
        }
        
        else{
        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mBaseData = tBodyData.getChild("BASE_PART");		//出险人基本信息        
        mReceiptList = tBodyData.getChild("RECEIPTLIST");	//账单信息
        mBnfList = tBodyData.getChild("BNFLIST");			//受益人信息
        mDiseaseList = tBodyData.getChild("DISEASELIST");	//疾病信息            
      //  mSecurityList = tBodyData.getChild("SECURITYLIST"); //社保补充账单列表
        }
        return true;

    }
    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLTJAccidentAutoRegister--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"TJ01".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误");
            return false;
        }

        if (!"TJ01".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86120000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
        
        mClaimNo = mBaseData.getChildText("CLAIM_NO");
        
        LLHospCaseDB mLLHospCaseDB = new LLHospCaseDB();        
        mLLHospCaseDB.setClaimNo(mClaimNo);
        mLLHospCaseDB.setHospitCode("00000000");
        mLLHospCaseSet = mLLHospCaseDB.query();
        if(mLLHospCaseSet.size()>=1)
        {
        	 buildError("checkData()", "【平台理赔号】在系统中已经存在");
             return false;
        }
        LLCasePayAdvancedTraceDB mLLCasePayAdvancedTraceDB=new LLCasePayAdvancedTraceDB();
        mLLCasePayAdvancedTraceDB.setClaimNo(mClaimNo);
        LLCasePayAdvancedTraceSet mLLCasePayAdvancedTraceSet=mLLCasePayAdvancedTraceDB.query();
        if(mLLCasePayAdvancedTraceSet.size()>=1)
        {
        	 buildError("checkData()", "【平台垫付理赔号】在系统中已经存在");
             return false;
        }
        return true;
    }
    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLTJAccidentAutoRegister--dealData");  
        
        List tBnfList = new ArrayList();
    	tBnfList = (List)mBnfList.getChildren();
    	String CheckIDNo = "";
    	
        for (int i = 0; i < tBnfList.size(); i++) 
        {
            Element tBnfData = (Element) tBnfList.get(i);
           
            mBnfNo = tBnfData.getChildText("BNFNO");
            mBnfName = tBnfData.getChildText("BNFNAME");
            mBnfSex = tBnfData.getChildText("BNFSEX");
            mBnfBirthDay = tBnfData.getChildText("BNFBIRTHDAY");
            mBnfIDType = tBnfData.getChildText("BNFIDTYPE");
            mBnfIDNO = tBnfData.getChildText("BNFIDNO");
            mBnfRealtion = tBnfData.getChildText("BNFRELATION");
            mBnfGrade = tBnfData.getChildText("BNFGRADE");

            mBnfRate = tBnfData.getChildText("BNFRATE");
            mSumPay = tBnfData.getChildText("SUMPAY");
            mBankCode = tBnfData.getChildText("BANKCODE");
            mAccNo = tBnfData.getChildText("ACCNO");
            mAccName = tBnfData.getChildText("ACCNAME");
                  
            if (mBnfNo == null || "".equals(mBnfNo) || mBnfNo.equals("null")) {
                buildError("dealData", "【受益人编号】的值不能为空");
                return false;
            }
            if (mBnfName == null || "".equals(mBnfName) || mBnfName.equals("null")) {
                buildError("dealData", "【受益人姓名】的值不能为空");
                return false;
            }
            if (mBnfSex == null || "".equals(mBnfSex)|| mBnfSex.equals("null")) {
                buildError("dealData", "【受益人性别】的值不能为空");
                return false;
            }           
            if (mBnfIDType == null || "".equals(mBnfIDType)|| mBnfIDType.equals("null")) {
                buildError("dealData", "【受益人证件类型】的值不能为空");
                return false;
            }
            if (mBnfIDNO == null || "".equals(mBnfIDNO)|| mBnfIDNO.equals("null")) {
                buildError("dealData", "【受益人证件号码】的值不能为空");
                return false;
            }
           if(mBnfBirthDay == null || "".equals(mBnfBirthDay)|| mBnfBirthDay.equals("null"))
            {
            	mBnfBirthDay = PubFun.getBirthdayFromId(mBnfIDNO);
            }
//           if("0".equals(mBnfIDType)){
//	           CheckIDNo = PubFun.CheckIDNo(mBnfIDType, mBnfIDNO, "", mBnfSex);
//	           if(CheckIDNo != ""){
//	        	   buildError("dealData", "【受益人身份证号】不符合规则");
//	        	   CheckIDNo="";
//	               return false;
//	           }
//           }
           //modify by panrm 2014-4-25 
           //不要此校驗  转发: Re: 拒付案件的报错报文
           /* 
            if (mBnfBirthDay == null || "".equals(mBnfBirthDay)|| mBnfBirthDay.equals("null")) {            	
                buildError("dealData", "【受益人出生日期】的值不能为空");
                return false;
            }*/
            if (mBnfRealtion == null || "".equals(mBnfRealtion)|| mBnfRealtion.equals("null")) {
                buildError("dealData", "【与被保险人关系】的值不能为空");
                return false;
            }
            if (mBnfGrade == null || "".equals(mBnfGrade)|| mBnfGrade.equals("null")) {
                buildError("dealData", "【受益人领取顺序】的值不能为空");
                return false;
            }
            if (mBnfRate == null || "".equals(mBnfRate)|| mBnfRate.equals("null")) {
                buildError("dealData", "【受益比例】的值不能为空");
                return false;
            }
            if (mSumPay == null || "".equals(mSumPay)|| mSumPay.equals("null")) {
                buildError("dealData", "【领取金额】的值不能为空");
                return false;
            }
            //modify by panrm 2014-5-12 拒赔不需要传银行编码和账信息  
            //回复: 转发: 天津意外险升级
            /*if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
                buildError("dealData", "【银行编码】的值不能为空");
                return false;
            }
            if (mAccNo == null || "".equals(mAccNo)|| mAccNo.equals("null")) {
                buildError("dealData", "【银行账号】的值不能为空或账号长度超过16位");
                return false;
            }
            
            Pattern tPattern = Pattern.compile("(\\d)+");
		       	Matcher isNum = tPattern.matcher(mAccNo);

						if (!isNum.matches()) {
							buildError("dealData", "【银行账号】只能是数字组成");
							return false;
						}
            
            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
                buildError("dealData", "【银行账户名】的值不能为空");
                return false;
            }          
             */

            //案件受理
        	if (!caseRegister(tBnfData)) {                        	
        		return false;
        	}          
            
            Element mPayList = (Element) tBnfData.getChild("PAYLIST");
            List tPayList = new ArrayList();
            tPayList = mPayList.getChildren();
               
            int tCaseCount = 0;           
            double mRealSum =0;  
            double mCaseSum =0;  
            LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
            for (int j = 0; j < tPayList.size(); j++) 
            {
                Element tPayData = (Element) tPayList.get(j);
                String tGetDutyCode = tPayData.getChildText("GETDUTYCODE");    
                String mRealPay = tPayData.getChildText("REALPAY"); 
                mRealSum += Arith.round(Double.parseDouble(mRealPay),2);
                               
                if(tGetDutyCode.equals("DSBZF")){
                	//垫付信息            	   
					String sNoLimit = PubFun.getNoLimit(mManageCom);
	           	  	String ActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
	           	 
	           	 	LLCasePayAdvancedTraceSchema mLLCasePayAdvancedTraceSchema = new LLCasePayAdvancedTraceSchema();	           	 
	                mLLCasePayAdvancedTraceSchema.setClaimNo(mClaimNo);
	                mLLCasePayAdvancedTraceSchema.setCaseNo(mClaimNo);
	                mLLCasePayAdvancedTraceSchema.setActugetNo(ActuGetNo);
	                mLLCasePayAdvancedTraceSchema.setAppTranNo(mTransactionNum);
	                mLLCasePayAdvancedTraceSchema.setAppDate(mCurrentDate);
	                mLLCasePayAdvancedTraceSchema.setAppTime(mCurrentTime);
	                mLLCasePayAdvancedTraceSchema.setGrpContNo(mLCInsuredSchema.getGrpContNo());
	                mLLCasePayAdvancedTraceSchema.setContNo(mLCInsuredSchema.getContNo());
	                mLLCasePayAdvancedTraceSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());
	                mLLCasePayAdvancedTraceSchema.setMoney(mRealPay);
	                mLLCasePayAdvancedTraceSchema.setPayMode("4");
	                mLLCasePayAdvancedTraceSchema.setDealState("01");
	                mLLCasePayAdvancedTraceSchema.setBnfNo(mBnfNo);
	                mLLCasePayAdvancedTraceSchema.setActugetNo(ActuGetNo);
	                mLLCasePayAdvancedTraceSchema.setOperator(mOperator);
	                mLLCasePayAdvancedTraceSchema.setMakeDate(mCurrentDate);
	                mLLCasePayAdvancedTraceSchema.setMakeTime(mCurrentTime);
	                mLLCasePayAdvancedTraceSchema.setManageCom(mManageCom);
	                mLLCasePayAdvancedTraceSchema.setModifyDate(mCurrentDate);
	                mLLCasePayAdvancedTraceSchema.setModifyTime(mCurrentTime);	                
	                mLLCasePayAdvancedTraceSet.add(mLLCasePayAdvancedTraceSchema);
	                	          
		           	LJAGetSchema mLJAGetSchema = new LJAGetSchema();
		           	Reflections LJAGettref = new Reflections();
		           	LJAGettref.transFields(mLJAGetSchema, mLLCasePayAdvancedTraceSchema);
		           	mLJAGetSchema.setActuGetNo(ActuGetNo);
		           	mLJAGetSchema.setSumGetMoney(mRealPay);       
		           	mLJAGetSchema.setOtherNoType("D"); 
		           	mLJAGetSchema.setOtherNo(mClaimNo);
		           	mLJAGetSchema.setDrawer(tBnfData.getChildText("BNFNAME"));
		           	mLJAGetSchema.setDrawerID(tBnfData.getChildText("BNFIDNO"));
		           	mLJAGetSchema.setPayMode("4");
		           	mLJAGetSchema.setAccName(tBnfData.getChildText("ACCNAME"));
		           	mLJAGetSchema.setBankAccNo(tBnfData.getChildText("ACCNO"));		           
		           	mLJAGetSet.add(mLJAGetSchema);
		            /*LDBankDB tLDBankDB = new LDBankDB();
				    tLDBankDB.setBankCode(tBnfData.getChildText("BANKCODE"));
				    tLDBankDB.setCanSendFlag("1");
				    if(tLDBankDB.query().size()<= 0)
				    {
				    	mErrors.addOneError("银行代码在系统中查询失败");
						return false;
				    }*/
	                mLJAGetSchema.setBankCode(tBnfData.getChildText("BANKCODE"));  
	                mLJAGetSchema.setShouldDate(mCurrentDate);
	                mLJAGetSchema.setSendBankCount("0");
	                mLJAGetSchema.setManageCom(mManageCom);
	                mLJAGetSchema.setBankOnTheWayFlag("");
	                	                                                   	                
                }else{       
                	tCaseCount = tCaseCount + 1;   
                	mCaseSum += Double.parseDouble(mRealPay); 
                	if(tCaseCount == 1)
                	{                      	
                       //申请材料选择
                        if (!caseAffix(tGetDutyCode)) {	                        	
                            return false;
                        }
                        
                        LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();   
                        //处理账单信息
                        if (!caseReceiptSave(mLLFeeMainSet)) {	                        	
                            return false;
                        } 
                        //处理检录信息
                        if (!caseRegisterCheck(mLLFeeMainSet)) {	                        	
                            return false;
                        }          
	                	LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();                  
	                    //处理理算信息
	                    if (!caseClaim(tBnfData,mLLClaimPolicySet)) {		                    	
	                        return false;
	                    }
	                    //	                  赔付结论(code)
	                    String mGiveType=tPayData.getChildText("GIVETYPE");
	                    
	                    //如果赔付结论为“协议给付”，则人工审批审定
	                    if(!mGiveType.equals("4") )
	                    {
	                    	 //处理结案信息
	                        if (!caseUnderWrite(mLLClaimPolicySet)) {	                        	
	                            return false;
	                    }
	                   
                        }
                       
                	}                           	 	                       	              	                                     	                 	                  
                }                 
            }     
            if(mCaseSum >= 0)
            {
            	 if("".equals(mCaseNo) || mCaseNo == null || "null".equals(mCaseNo))
            	 {
            		 mCaseNo = mClaimNo;
            	 }
            	 mLLHospCaseSchema.setCaseNo(mCaseNo);            	
                 mLLHospCaseSchema.setDealType("1");
                 mLLHospCaseSchema.setHospitCode("00000000");
                 mLLHospCaseSchema.setHandler(mOperator);
                 mLLHospCaseSchema.setAppTranNo(mTransactionNum);
                 mLLHospCaseSchema.setAppDate(mCurrentDate);
                 mLLHospCaseSchema.setAppTime(mCurrentTime);                         
                 mLLHospCaseSchema.setConfirmState("1");
                 mLLHospCaseSchema.setCaseType("01");
                 mLLHospCaseSchema.setClaimNo(mClaimNo);
                 mLLHospCaseSchema.setBnfNo(mBnfNo);   
                 mLLHospCaseSchema.setRealpay(mCaseSum);
                 mLLHospCaseSchema.setMakeDate(mCurrentDate);
                 mLLHospCaseSchema.setMakeTime(mCurrentTime);
                 mLLHospCaseSchema.setModifyDate(mCurrentDate);
                 mLLHospCaseSchema.setModifyTime(mCurrentTime);               
                 mLLHospCaseSet.add(mLLHospCaseSchema);                  
            }
   		    
            if(Arith.round(Double.parseDouble(mSumPay),2)- Arith.round(mRealSum,2) != 0)
            {
           	  buildError("caseClaim", "每个受益人给付责任的累计金额不等于受益人赔付金额");
               return false;
            }                  
        }       

        return true;
    }
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
    	 System.out.println("LLTJAccidentAutoRegister--createResultXML(正常返回报文)");
    	 Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);

         Element tResponseCode = new Element("RESPONSE_CODE");
         tResponseCode.setText("1");
         tHeadData.addContent(tResponseCode);

         Element tErrorMessage = new Element("ERROR_MESSAGE");
         tErrorMessage.setText(mErrors.getFirstError());
         tHeadData.addContent(tErrorMessage);

         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tClaimNo = new Element("CLAIM_NO");
         tClaimNo.setText(aCaseNo);
         tBodyData.addContent(tClaimNo);
         
         Element tCaseList = new Element("CASELIST");
                 
         List tBnfList = new ArrayList();        
     	 tBnfList = (List)mBnfList.getChildren();

         for (int i = 0; i < mLLHospCaseSet.size(); i++) 
         {             
        	 Element tCaseData = new Element("CASEDATA");        	
    		 LLHospCaseSchema mLLHospCaseSchema = mLLHospCaseSet.get(i+1);             	
        	 Element tCaseNo = new Element("CASENO");        	
             tCaseNo.setText(mLLHospCaseSchema.getCaseNo());
             tCaseData.addContent(tCaseNo);    
             tCaseList.addContent(tCaseData);          	
             
             Element tBnfData = (Element) tBnfList.get(i);
             Element tBnfNo = new Element("BNFNO");
             tBnfNo.setText(tBnfData.getChildText("BNFNO"));            
             tCaseData.addContent(tBnfNo);             
                       
         }   
         if(mLLHospCaseSet.size()==0){
        	 Element tCaseData = new Element("CASEDATA");        	             	
        	 Element tCaseNo = new Element("CASENO");        	
             tCaseNo.setText("");
             tCaseData.addContent(tCaseNo);    
             tCaseList.addContent(tCaseData);          	
             
             Element tBnfData = (Element) tBnfList.get(0);
             Element tBnfNo = new Element("BNFNO");
             tBnfNo.setText(tBnfData.getChildText("BNFNO"));            
             tCaseData.addContent(tBnfNo); 
         }
         tBodyData.addContent(tCaseList);
         tRootData.addContent(tBodyData); 

         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
    	System.out.println("LLTJAccidentAutoRegister--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
   
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJAccidentAutoRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
   
    /**
     * 客户信息实名化
     * @return boolean
     */
    private boolean customerWS() throws Exception{
    	 System.out.println("LLTJAccidentAutoRegister--CustomerWS");
    	 String  mGrpContNo ="";
    	 
//    	 mClaimType为空为天津社保，并且报文中传递保单号码
    	/* if(mClaimType == null || "".equals(mClaimType)){
    		 // 天津社保
    		 mGrpContNo=mGrpContNoSB;
    	 }else{*/
    		 // 如果是意外险 还要区分意外险和油田 报文中没有保单号码
    		 if(!"".equals(mClaimYear) && mClaimYear != null && !"null".equals(mClaimYear)){
                 String GrpContSql="select codealias from ldcode where codetype='TJAccident' and code='"+mClaimYear+mClaimType+"GrpContNo' and comcode='"+ mManageCom +"'";
                 ExeSQL GrpExeSQL = new ExeSQL();
                 mGrpContNo = GrpExeSQL.getOneValue(GrpContSql);
    		 }
    		 else{
    		 String mAccDateYear=mAccDate.substring(0, 4);
             String GrpContSql="select codealias from ldcode where codetype='TJAccident' and code='"+mAccDateYear+mClaimType+"GrpContNo' and comcode='"+ mManageCom +"'";
             ExeSQL GrpExeSQL = new ExeSQL();
             mGrpContNo = GrpExeSQL.getOneValue(GrpContSql);
    		 }
    	// }
    	
    	 
         if("".equals(mGrpContNo) || null == mGrpContNo)
         {
        	 buildError("CustomerWS", "【团体保单号】在系统中没有配置");
             return false;
         }
         
         String ContPSql="select codealias from ldcode where codetype='TJAccident' and code='ContPlancode' and comcode='"+ mManageCom +"'";
         ExeSQL ContPExeSQL = new ExeSQL();
         String  mContPlanCode = ContPExeSQL.getOneValue(ContPSql);
         if("".equals(mContPlanCode) || null == mContPlanCode)
         {
        	 buildError("CustomerWS", "【保单的保障计划】在系统中没有配置");
             return false;
         }
         
         String OperaSql="select codealias from ldcode where codetype='TJAccident' and code='Operator' and comcode='"+ mManageCom +"'";
         ExeSQL OperaExeSQL = new ExeSQL();
         mOperator = OperaExeSQL.getOneValue(OperaSql);
         if("".equals(mOperator) || null == mOperator)
         {
        	 buildError("CustomerWS", "【案件的处理人】在系统中没有配置");
             return false;
         }
         
         LCGrpContDB mLCGrpContDB = new LCGrpContDB();
         String LCGrpContSql="select * from lcgrpcont where grpcontno='"+ mGrpContNo +"' ";         	         
         LCGrpContSet mLCGrpContSet= mLCGrpContDB.executeQuery(LCGrpContSql);
         if(mLCGrpContSet.size()<= 0)
         {
        	 buildError("CustomerWS", "团体保单信息在系统中查询失败");
             return false;
         }
         
         LCInsuredDB mLCInsuredDB = new LCInsuredDB();
         String LCInsurSql="select * from lcinsured where grpcontno='"+ mGrpContNo +"' "
         	+" and name='"+ mInsuredName +"' and idtype='"+ mIDType +"' and idno='"+ mIDNo +"' and Sex='"+ mInsuredSex +"' ";         
         LCInsuredSet mLCInsuredSet= mLCInsuredDB.executeQuery(LCInsurSql);
                 
         if(mLCInsuredSet.size()>=1 )
         {
        	mLCInsuredSchema = mLCInsuredSet.get(1).getSchema();
        	mInsuredNo = mLCInsuredSchema.getInsuredNo();
            mContNo = mLCInsuredSchema.getContNo();
        	return true;
         }else{        	             
             LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();
             mLPDiskImportSchema.setInsuredName(mInsuredName);
             mLPDiskImportSchema.setSex(mInsuredSex);
             mLPDiskImportSchema.setBirthday(mInsuredBirthDay);
             mLPDiskImportSchema.setIDType(mIDType);
             mLPDiskImportSchema.setIDNo(mIDNo);
             mLPDiskImportSchema.setEdorValiDate(mLCGrpContSet.get(1).getCValiDate());
             mLPDiskImportSchema.setRetire(mInsuredStat);
             mLPDiskImportSchema.setGrpContNo(mGrpContNo);
             mLPDiskImportSchema.setContPlanCode(mContPlanCode); 
             mLPDiskImportSchema.setSerialNo(mClaimNo);
             mLPDiskImportSchema.setImportFileName(mTransactionNum);
                     
             mGlobalInput.ManageCom = mManageCom ;
             mGlobalInput.Operator = mOperator ;
            	 
             VData tVData = new VData();
             tVData.add(mGlobalInput);
             tVData.add(mLPDiskImportSchema);
             LLCustomerWS tLLCustomerWS = new LLCustomerWS();
             try
             {
                 if(!tLLCustomerWS.submitData(tVData, ""))
                 {
                     CError.buildErr(this, tLLCustomerWS.mErrors.getErrContent());
                     return false;
                 }

              }catch(Exception ex)
              {
                  ex.printStackTrace();
                  CError.buildErr(this, "结算出现未知异常");
                  return false;
              }                   
              System.out.println("客户实名化完成===");
              LCInsuredDB tLCInsuredDB = new LCInsuredDB();
              String tLCInsurSql="select * from lcinsured where grpcontno='"+ mGrpContNo +"' "
              	+" and name='"+ mInsuredName +"' and idtype='"+ mIDType +"' and idno='"+ mIDNo +"' and Sex='"+ mInsuredSex +"' ";         
              LCInsuredSet tLCInsuredSet= tLCInsuredDB.executeQuery(tLCInsurSql);
              if(tLCInsuredSet.size()<=0 )
              {
            	  buildError("CustomerWS", "实名化后客户信息查询失败");
                  return false;
              }
              mLCInsuredSchema = tLCInsuredSet.get(1).getSchema();
          	  mInsuredNo = mLCInsuredSchema.getInsuredNo();
              mContNo = mLCInsuredSchema.getContNo();
         }        
         
    	return true;
    }
    /**
     * 解析受理信息
     * @return boolean
     */
    private boolean caseRegister(Element tBnfData) throws Exception {
        System.out.println("LLTJAccidentAutoRegister--caseRegister");                                                       
      
        mClaimNo = mBaseData.getChildText("CLAIM_NO");
        mClaimType=mBaseData.getChildText("CLAIM_TYPE");
        mClaimYear=mBaseData.getChildText("CLAIM_YEAR");
       // mGrpContNoSB=mBaseData.getChildText("CONT_NO");
        
      	mInsuredName = mBaseData.getChildText("INSURED_NAME");
      	mInsuredSex = mBaseData.getChildText("INSURED_SEX");    
      	mInsuredBirthDay = mBaseData.getChildText("INSURED_BIRTHDAY");    
      	mIDType = mBaseData.getChildText("ID_TYPE");    
      	mIDNo = mBaseData.getChildText("ID_NO");    
      	mInsuredStat = mBaseData.getChildText("INSURED_STAT");            	       	     
        mAccDate = mBaseData.getChildText("ACCDATE");    
        mAccidentType = mBaseData.getChildText("ACCIDENTTYPE");  
        mRgtanName = mBaseData.getChildText("RGTANTNAME");    
        String tRgtantIDType = mBaseData.getChildText("RGTANTIDTYPE");
        String tRgtantIDNo = mBaseData.getChildText("RGTANTIDNO");
        mRgtantRelation = mBaseData.getChildText("RGTANTRELATIOIN");            
        mReasonCode = mBaseData.getChildText("CLAIMREASON");  
        mDeformityCode = mBaseData.getChildText("DEFORMITYCODE");  
        
        mDeathDate = mBaseData.getChildText("DEATHDATE");  
   	       
        

        if (mClaimNo == null || "".equals(mClaimNo)|| mClaimNo.equals("null")) {
            buildError("CustomerWS", "【平台理赔号】的值不能为空");
            return false;
        }
//        对于天津社保业务，mClaimType可为空，但此时保单号码不能为空
       /* if ((mClaimType == null || "".equals(mClaimType)|| ("null").equals(mClaimType)) && (mGrpContNoSB == null || "".equals(mGrpContNoSB)|| ("null").equals(mGrpContNoSB)) ) {
            buildError("CustomerWS", "对于社保业务，【保单号码】的值不能为空");
            return false;
        }*/
        if (mInsuredName == null || "".equals(mInsuredName)|| mInsuredName.equals("null")) {
            buildError("CustomerWS", "【姓名】的值不能为空");
            return false;
        }
        if (mInsuredSex == null || "".equals(mInsuredSex)|| mInsuredSex.equals("null")) {
            buildError("CustomerWS", "【性别】的值不能为空");
            return false;
        }
     
        if (mIDType == null || "".equals(mIDType)|| mIDType.equals("null") || !"0".equals(mIDType)) {
            buildError("CustomerWS", "【证件类型】的值不能为空或非身份证");
            return false;
        }
        if (mIDNo == null || "".equals(mIDNo)|| mInsuredSex.equals("null")|| mIDNo.equals("null")) {
            buildError("CustomerWS", "【证件号码】的值不能为空");
            return false;
        }
        if(mInsuredBirthDay == null || "".equals(mInsuredBirthDay)|| mInsuredBirthDay.equals("null"))
        {
        	mInsuredBirthDay = PubFun.getBirthdayFromId(mIDNo);
        }
       /* if (mInsuredBirthDay == null || "".equals(mInsuredBirthDay)|| mInsuredBirthDay.equals("null")) {
            buildError("CustomerWS", "【出生日期】的值不能为空");
            return false;
        }   */
        
        if (mAccDate == null || "".equals(mAccDate)|| mAccDate.equals("null")) {
            buildError("caseRegister", "【出险日期】的值不能为空");
            return false;
        }                          
        
        if (mRgtanName == null || "".equals(mRgtanName)|| mRgtanName.equals("null")) {
            buildError("caseRegister", "【申请人姓名】的值不能为空");
            return false;
        }   
        if (mRgtantRelation == null || "".equals(mRgtantRelation)|| mRgtantRelation.equals("null")) {
            buildError("caseRegister", "【申请人与被保险人关系】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(mAccDate)) {
            buildError("caseRegister", "【出险日期】格式错误");
            return false;
        }                     
        if (!PubFun.checkDateForm(mInsuredBirthDay)) {
            buildError("caseRegister", "【出生日期】格式错误");
            return false;
        }
//        if("0".equals(tRgtantIDType)&&((tRgtantIDNo != null ||! "".equals(tRgtantIDNo)|| !tRgtantIDNo.equals("null")))){
//        	String CheckIDNo = PubFun.CheckIDNo(tRgtantIDType, tRgtantIDNo, "", "");
//        	if(CheckIDNo!=""){
//        		 buildError("caseRegister", "【申请人身份证号】不符合规则");
//                 return false;
//        	}
//        }
        if (tRgtantIDType == null || "".equals(tRgtantIDType)|| tRgtantIDType.equals("null")) {
        	tRgtantIDType = mIDType;
        }
        if (tRgtantIDNo == null || "".equals(tRgtantIDNo)|| tRgtantIDNo.equals("null")) {
        	tRgtantIDNo = mIDNo;
        }
        //客户信息实名化
        if (!customerWS()) {
            return false;
        }    
        Element mPayList = (Element) tBnfData.getChild("PAYLIST");
        List tPayList = new ArrayList();
        tPayList = mPayList.getChildren();
                  
        int tCase = 0 ;
        for (int j = 0; j < tPayList.size(); j++) 
        {
            Element tPayData = (Element) tPayList.get(j);
            String tGetDutyCode = tPayData.getChildText("GETDUTYCODE");  
          
            if(!tGetDutyCode.equals("DSBZF"))
            {
            	 tCase = tCase +1;
            	 if(tCase !=1 ) continue ;
            	 if(tGetDutyCode.equals("YWSG") && (mDeathDate.equals("")|| null == mDeathDate))
            	 {
            		  buildError("caseRegister", "意外身故必须录入死亡日期");
                      return false;
            	 }
//            	 if(tGetDutyCode.equals("YWSC") && (mDeformityCode.equals("")|| null == mDeformityCode))
//            	 {
//            		  buildError("caseRegister", "意外伤残必须录入残疾信息");
//                      return false;
//            	 }
            	 mLLCaseSchema.setCustomerNo(mInsuredNo);
                 mLLCaseSchema.setCustomerName(mInsuredName);
                 mLLCaseSchema.setCustBirthday(mInsuredBirthDay);
                 mLLCaseSchema.setCustomerSex(mInsuredSex);
                 mLLCaseSchema.setIDType(mIDType);
                 mLLCaseSchema.setIDNo(mIDNo);
                 mLLCaseSchema.setCaseGetMode("4"); 
                 mLLCaseSchema.setBankCode(mBankCode);
                 mLLCaseSchema.setBankAccNo(mAccNo);
                 mLLCaseSchema.setAccName(mAccName);
                 mLLCaseSchema.setAccidentDate(mAccDate);
                 mLLCaseSchema.setAccidentType(mAccidentType);
                 mLLCaseSchema.setCaseProp("10");
                 
                 
                 LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
                 mLLRegisterSchema.setRelation(mRgtantRelation);
                 mLLRegisterSchema.setRgtantName(mRgtanName);
                 mLLRegisterSchema.setIDType(tRgtantIDType);
                 mLLRegisterSchema.setIDNo(tRgtantIDNo);
                 mLLRegisterSchema.setRgtantMobile(mRgtMobile);
                 mLLRegisterSchema.setRgtantPhone(mRgtantPhone);
                 mLLRegisterSchema.setRgtantAddress(mRgtantAddress);
                 mLLRegisterSchema.setPostCode(mRgtantPostCode);
	           	/*  if(mClaimType == null || "".equals(mClaimType))
	        	  {
	           		mLLRegisterSchema.setRgtType("4");
	           		mLLRegisterSchema.setAppPeoples(1);
	           		mLLRegisterSchema.setTogetherFlag("1");
	           		
	        	  }
	        	  else
	        	  {*/
	        		mLLRegisterSchema.setRgtType("7");
	        	 // }
                   
                 if(null == mLLSubReportSet ){
                	   mLLSubReportSet = new LLSubReportSet();
                 	   LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
                       mLLSubReportSchema.setCustomerNo(mInsuredNo);
                       mLLSubReportSchema.setCustomerName(mInsuredName);
                       mLLSubReportSchema.setAccidentType(mAccidentType);
                       mLLSubReportSchema.setAccDate(mAccDate);    
                       mLLSubReportSchema.setDieDate(mDeathDate);
                       // 3544 增加发生地点和医保类型**start**
//                 		String tProvinceAccCode=mBaseData.getChildText("ACCPROVINCECODE");	//发生地点省
//                 		String tCityAccCode =mBaseData.getChildText("ACCCITYCODE");			//发生地点市     
//                 		String tCountyAccCode =mBaseData.getChildText("ACCCOUNTYCODE");       //发生地点县
//                 		// 校验录入的省、市、县编码信息
//                 		String tAccCode=LLCaseCommon.checkAccPlace(tProvinceAccCode,tCityAccCode,tCountyAccCode); 
//                     		if(!"".equals(tAccCode)){
//                     			buildError("caseRegister", tAccCode);
//                               return false;
//                     		}else{
//                     			mLLSubReportSchema.setAccProvinceCode(tProvinceAccCode);
//                     			mLLSubReportSchema.setAccCityCode(tCityAccCode);
//                     			mLLSubReportSchema.setAccCountyCode(tCountyAccCode);
//                     		}
                 		
                 // 3544 增加发生地点和医保类型**end**	
                       
                       mLLSubReportSet.add(mLLSubReportSchema);
                 }      
                 
                 if(mReasonCode == null || "".equals(mReasonCode))
                 {
                 	if(tGetDutyCode.equals("YWSG")) mReasonCode = "05";
                 	if(tGetDutyCode.equals("YWSC")) mReasonCode = "08";
                 	if(tGetDutyCode.equals("YWYL")) mReasonCode = "03";
                 	if(tGetDutyCode.equals("SBBC")) mReasonCode = "02";
                 }
                 
                 LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet();
                 LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
                 mLLAppClaimReasonSchema.setCustomerNo(mInsuredNo);
                 mLLAppClaimReasonSchema.setReasonCode(mReasonCode);
                 
                 mLLAppClaimReasonSet.add(mLLAppClaimReasonSchema);
              
                 mGlobalInput.ManageCom = mManageCom ;
                 mGlobalInput.Operator = mOperator ;
                 
                 VData tVData = new VData();
                 tVData.add(mGlobalInput);
                 tVData.add(mLLCaseSchema);
                 tVData.add(mLLRegisterSchema);
                 tVData.add(mLLSubReportSet);
                 tVData.add(mLLAppClaimReasonSet);
                 
                 LLCaseRegister tLLCaseRegister = new LLCaseRegister();
                 try
                 {
                     if(!tLLCaseRegister.submitData(tVData, ""))
                     {
                         CError.buildErr(this, tLLCaseRegister.mErrors.getErrContent());
                         return false;
                     }

                  }catch(Exception ex)
                  {
                      ex.printStackTrace();
                      CError.buildErr(this, "结算出现未知异常");
                      return false;
                  }                 
                  mLLCaseSchema = (LLCaseSchema)tLLCaseRegister.getResult().getObjectByObjectName("LLCaseSchema", 0);
                  mLLCaseRelaSet = (LLCaseRelaSet)tLLCaseRegister.getResult().getObjectByObjectName("LLCaseRelaSet", 0);
                  mLLSubReportSet = (LLSubReportSet)tLLCaseRegister.getResult().getObjectByObjectName("LLSubReportSet", 0);
                  mCaseNo = mLLCaseSchema.getCaseNo();
                 
                  mLLCaseSet.add(mLLCaseSchema);
                  System.out.println("CaseNO==="+mLLCaseSchema.getCaseNo());         

            }
        }       
        return true;
    }
    /**
     * 申请材料信息
     * @return boolean
     */
    private boolean caseAffix(String tGetDutyCode) throws Exception {
    	 System.out.println("LLTJAccidentAutoRegister----caseAffix");
    	 mReasonCode = mBaseData.getChildText("CLAIMREASON");    	     	
    	 
    	 LLAffixSet mLLAffixSet = new LLAffixSet();
    	 String tSQL = "SELECT b.affixtypecode,b.AffixCode,b.AffixName "
             + "FROM LLMAppReasonAffix a,LLMAffix b WHERE "
             + "a.affixcode=b.affixcode AND reasoncode='"+tGetDutyCode.substring(2, 4)+"' ORDER BY b.affixtypecode,b.AffixCode WITH UR";
		 ExeSQL tExeSQl = new ExeSQL();
		 SSRS tSSRS = new SSRS();
		 tSSRS = tExeSQl.execSQL(tSQL);
		 if (tSSRS.getMaxRow()>0){
		     for (int i=1; i<=tSSRS.getMaxRow(); i++){
		         LLAffixSchema mLLAffixSchema = new LLAffixSchema();
		         mLLAffixSchema.setReasonCode(tGetDutyCode.substring(2, 4));
		         mLLAffixSchema.setAffixType(tSSRS.GetText(i, 1));
		         mLLAffixSchema.setAffixCode(tSSRS.GetText(i, 2));
		         mLLAffixSchema.setCount(1);
		         mLLAffixSchema.setShortCount(0);
		         mLLAffixSet.add(mLLAffixSchema);
		     }
		 }else{
		     CError.buildErr(this, "未配置申请材料");
		     return false;
		 }
    	 
    	 VData tVData = new VData();
         tVData.add(mGlobalInput);
         tVData.add(mLLCaseSchema);
         tVData.add(mLLAffixSet);         
         LLCaseAffix tLLCaseAffix = new LLCaseAffix();
         try
         {
             if(!tLLCaseAffix.submitData(tVData, ""))
             {
                 CError.buildErr(this, tLLCaseAffix.mErrors.getErrContent());
                 return false;
             }

          }catch(Exception ex)
          {
              ex.printStackTrace();
              CError.buildErr(this, "结算出现未知异常");
              return false;
          }
        
          mLLCaseSchema = (LLCaseSchema)tLLCaseAffix.getResult().getObjectByObjectName("LLCaseSchema", 0);         
          if( null == mLLCaseSchema ) {
   			mErrors.addOneError("案件返回信息获取失败");
   			return false;
   		 }                           
          System.out.println("CaseNO==="+mLLCaseSchema.getRgtState());
    	 return true;
    }
    
    /**
     * 处理账单信息
     * @return boolean
     */
    private boolean caseReceiptSave(LLFeeMainSet mLLFeeMainSet) throws Exception {
    	VData tVData = new VData();
        System.out.println("LLTJAccidentAutoRegister----caseReceiptSave");
    	List tReceiptList = new ArrayList();
    	tReceiptList = (List) mReceiptList.getChildren();    	
    	//账单列表（多条）RECEIPTLIST 隶属于案件信息
        for (int i = 0; i < tReceiptList.size(); i++) 
        {
            Element tReceiptData = (Element) tReceiptList.get(i);
            String mFeeType = tReceiptData.getChildText("FEETYPE");
            String mMainFeeNo = tReceiptData.getChildText("MAINFEENO");
            String mHosipitName = tReceiptData.getChildText("HOSIPITALNAME");
            String mFeeDate = tReceiptData.getChildText("FEEDATE");
            String mHospStartDate = tReceiptData.getChildText("HOSPSTARTDATE");
            String mHospEndDate = tReceiptData.getChildText("HOSPENDDATE");
            String mSumFee = tReceiptData.getChildText("SUMFEE");
            String mGetLimit = tReceiptData.getChildText("GETLIMIT");
            String mPlanFee = tReceiptData.getChildText("PLANFEE");
            String mSupInHosFee = tReceiptData.getChildText("SUPINHOSFEE");
            String mSelfAmnt = tReceiptData.getChildText("SELFAMNT");
            String mSelfPay2 = tReceiptData.getChildText("SELFPAY2");
            
            if (mFeeType == null || "".equals(mFeeType)) {
                buildError("caseReceiptSave", "【账单类型】的值不能为空");
                return false;
            }  
            if (mMainFeeNo == null || "".equals(mMainFeeNo)) {
                buildError("caseReceiptSave", "【账单号码】的值不能为空");
                return false;
            }  
            if (mHosipitName == null || "".equals(mHosipitName)) {
                buildError("caseReceiptSave", "【医院名称】的值不能为空");
                return false;
            }  
            if (mFeeDate == null || "".equals(mFeeDate)) {
                buildError("caseReceiptSave", "【结算日期】的值不能为空");
                return false;
            }  
            if (mHospStartDate == null || "".equals(mHospStartDate)) {
                buildError("caseReceiptSave", "【入院日期】的值不能为空");
                return false;
            }  
            if (mHospEndDate == null || "".equals(mHospEndDate)) {
                buildError("caseReceiptSave", "【出院日期】的值不能为空");
                return false;
            }  
            if (mSumFee == null || "".equals(mSumFee)) {
                buildError("caseReceiptSave", "【账单金额】的值不能为空");
                return false;
            }  
            if (mGetLimit == null || "".equals(mGetLimit)) {
                buildError("caseReceiptSave", "【社保起伏线】的值不能为空");
                return false;
            }  
            if (mPlanFee == null || "".equals(mPlanFee)) {
                buildError("caseReceiptSave", "【统筹支付金额】的值不能为空");
                return false;
            }  
            if (mSupInHosFee == null || "".equals(mSupInHosFee)) {
                buildError("caseReceiptSave", "【大额救助支付金额】的值不能为空");
                return false;
            } 
            if (mSelfAmnt == null || "".equals(mSelfAmnt)) {
                buildError("caseReceiptSave", "【自费金额】的值不能为空");
                return false;
            }  
            if (mSelfPay2 == null || "".equals(mSelfPay2)) {
                buildError("caseReceiptSave", "【自付二金额】的值不能为空");
                return false;
            }                
            Element mFeeitemList = (Element) tReceiptData.getChild("FEEITEMLIST");
            List tFeeitemList = new ArrayList();
            tFeeitemList = mFeeitemList.getChildren();
            //账单分类列表（多条）FEEITEMLIST 隶属于账单信息
            LLCaseReceiptSet mLLCaseReceiptSet = null;  
            for (int j = 0; j < tFeeitemList.size(); j++) 
            {
                Element tFeeitemData = (Element) tFeeitemList.get(j);
                String mFeeitemCode = tFeeitemData.getChildText("FEEITEM_CODE");
                String mFee = tFeeitemData.getChildText("SUMFEE");
                String mReSelfAmnt = tFeeitemData.getChildText("SELFAMNT");
                String mPreAmnt = tFeeitemData.getChildText("PREAMNT");
                String mRefuseAmnt = tFeeitemData.getChildText("REFUSEAMNT");
                               
                if(!"".equals(mFeeitemCode) && null != mFeeitemCode)
                {                	                 
                	CachedLPInfo mCachedLPInfo = new CachedLPInfo();
                	if(!mFeeType.equals("1") && !mFeeType.equals("2")&& !mFeeType.equals("3"))
                	{
                		CError.buildErr(this, "【账单类型】的值传入错误");
                        return false;
                	}
                    if(mCachedLPInfo.findLLAFeeItemTypeByCode(mFeeType,mFeeitemCode).getCodeAlias()== null)
                	{
                    	CError.buildErr(this, "没有对应的账单费用明细信息");
                        return false;
                	}else{
                		mFeeitemCode = mCachedLPInfo.findLLAFeeItemTypeByCode(mFeeType,mFeeitemCode).getCodeAlias();
                	}
                    
	                mLLCaseReceiptSet = new LLCaseReceiptSet();
	                LLCaseReceiptSchema mLLCaseReceiptSchema = new LLCaseReceiptSchema();
	                mLLCaseReceiptSchema.setCaseNo(mLLCaseSchema.getCaseNo());
	                mLLCaseReceiptSchema.setRgtNo(mLLCaseSchema.getRgtNo());
	                mLLCaseReceiptSchema.setFeeItemCode(mFeeitemCode);
	                mLLCaseReceiptSchema.setAvaliFlag("0");
	                mLLCaseReceiptSchema.setFee(mFee);
	                mLLCaseReceiptSchema.setSelfAmnt(mReSelfAmnt);
	                mLLCaseReceiptSchema.setPreAmnt(mPreAmnt);
	                mLLCaseReceiptSchema.setRefuseAmnt(mRefuseAmnt);
	                mLLCaseReceiptSet.add(mLLCaseReceiptSchema);
                }
            }
            
            //------------------------------------------------------
            //账单分类列表（多条）DOCUMNENTATIONLIST 隶属于账单信息
            /*if(mClaimType == null || "".equals(mClaimType)){
	            Element mDocumnentationList = (Element) tReceiptData.getChild("DOCUMNENTATIONLIST");
	            List tDocumnentationList = new ArrayList();
	            tDocumnentationList = mDocumnentationList.getChildren();
	            LLReceiptSet mLLReceiptSet = null;  
	            for (int j = 0; j < tDocumnentationList.size(); j++) 
	            {
	                Element tDocumnentationData = (Element) tDocumnentationList.get(j);
	                String mReceiptNo = tDocumnentationData.getChildText("RECEIPT_NO");
	                String mReceiptNum = tDocumnentationData.getChildText("RECEIPT_NUM");
	                String mServiceDate = tDocumnentationData.getChildText("SERVICEDATE");
	                String mProductCode = tDocumnentationData.getChildText("PRODUCTCODE");
	                String mProductName = tDocumnentationData.getChildText("PRODUCTNAME");
	                String mProductType = tDocumnentationData.getChildText("PRODUCTTYPE");
	                String mDrugCode = tDocumnentationData.getChildText("DRUGCODE");
	                String mPayService = tDocumnentationData.getChildText("PAYSERVICE");
	                String mDrugName = tDocumnentationData.getChildText("DRUGNAME");
	                String mUnitPrice = tDocumnentationData.getChildText("UNITPRICE");
	                
	                String mNumber = tDocumnentationData.getChildText("NUMBER");
	                String mSpecifications = tDocumnentationData.getChildText("SPECIFICATIONS");
	                String mDosage = tDocumnentationData.getChildText("DOSAGE");
	                String mSumprice = tDocumnentationData.getChildText("SUMPRICE");
	                String mProfessionalCode = tDocumnentationData.getChildText("PROFESSIONALCODE");
	                String mDoctorName = tDocumnentationData.getChildText("DOCTORNAME");
	                String mDepartmentCode = tDocumnentationData.getChildText("DEPARTMENTCODE");
	                String mDepartmentName = tDocumnentationData.getChildText("DEPARTMENTNAME");
	                String mUsage = tDocumnentationData.getChildText("USAGE");
	                String mDrugType = tDocumnentationData.getChildText("DRUGTYPE");
	                
	                String mConsumption = tDocumnentationData.getChildText("CONSUMPTION");
	                String mFrequency = tDocumnentationData.getChildText("FREQUENCY");
	                String mDrugDay = tDocumnentationData.getChildText("DRUGDAY");
	                String mReceiptfeeinsecu = tDocumnentationData.getChildText("RECEIPTFEEINSECU");
	                               
	                if(!"".equals(mReceiptNo) && null != mReceiptNo)
	                {                	            
		                mLLReceiptSet = new LLReceiptSet();
		                LLReceiptSchema mLLReceiptSchema = new LLReceiptSchema(); 
		                mLLReceiptSchema.setCaseNo(mLLCaseSchema.getCaseNo());
		                mLLReceiptSchema.setMainfeeNo(mMainFeeNo);
		                mLLReceiptSchema.setReceiptNo(mReceiptNo);
		                mLLReceiptSchema.setReceiptNum(mReceiptNum);
		                mLLReceiptSchema.setServiceDate(mServiceDate);
		                mLLReceiptSchema.setProductCode(mProductCode);
		                mLLReceiptSchema.setProductName(mProductName);
		                mLLReceiptSchema.setProductType(mProductType);
		                
		                mLLReceiptSchema.setDrugCode(mDrugCode);
		                mLLReceiptSchema.setPayService(mPayService);
		                mLLReceiptSchema.setDrugName(mDrugName);
		                mLLReceiptSchema.setUnitPrice(mUnitPrice);
		                mLLReceiptSchema.setNumber(mNumber);
		                mLLReceiptSchema.setSpecifications(mSpecifications);
		                mLLReceiptSchema.setDosage(mDosage);
		                mLLReceiptSchema.setSumprice(mSumprice);
		                
		                mLLReceiptSchema.setProfessionalCode(mProfessionalCode);
		                mLLReceiptSchema.setDoctorName(mDoctorName);
		                mLLReceiptSchema.setDepartmentCode(mDepartmentCode);
		                mLLReceiptSchema.setDepartmentName(mDepartmentName);
		                mLLReceiptSchema.setUsage(mUsage);
		                mLLReceiptSchema.setDrugType(mDrugType);
		                mLLReceiptSchema.setConsumption(mConsumption);
		                mLLReceiptSchema.setFrequency(mFrequency);
		                mLLReceiptSchema.setDrugDay(mDrugDay);
		                mLLReceiptSchema.setReceiptfeeinsecu(mReceiptfeeinsecu);
		                
		                mLLReceiptSchema.setOperator(mOperator);
		                mLLReceiptSchema.setMakeDate(mCurrentDate);
		                mLLReceiptSchema.setMakeTime(mCurrentTime);
		                mLLReceiptSchema.setModifyDate(mCurrentDate);
		                mLLReceiptSchema.setModifyTime(mCurrentTime);
		                	                
		                mLLReceiptSet.add(mLLReceiptSchema);
	                }
	            }
            tVData.add(mLLReceiptSet);
            }*/
            //------------------------------------------------------
            
            LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
            LLSecurityReceiptSchema mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
             mLLFeeMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
	       	 mLLFeeMainSchema.setRgtNo(mLLCaseSchema.getRgtNo());
	       	 mLLFeeMainSchema.setCaseRelaNo(mLLCaseRelaSet.get(1).getCaseRelaNo());
	       	 mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
	       	 mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	       	 mLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
	       	 mLLFeeMainSchema.setAge((int)mLLCaseSchema.getCustomerAge());
	       	 mLLFeeMainSchema.setSecurityNo(mLCInsuredSchema.getOthIDNo());
	       	 mLLFeeMainSchema.setInsuredStat(mLCInsuredSchema.getInsuredStat());
	       	 mLLFeeMainSchema.setFeeType(mFeeType);
	       	 mLLFeeMainSchema.setFeeAtti("4");
	       	 mLLFeeMainSchema.setReceiptNo(mMainFeeNo);
	       	 mLLFeeMainSchema.setHospitalName(mHosipitName);
	       	 mLLFeeMainSchema.setFeeDate(mFeeDate);    
	       	 mLLFeeMainSchema.setHospStartDate(mHospStartDate);
	       	 mLLFeeMainSchema.setHospEndDate(mHospEndDate);
	       	 int tRealHospDay = PubFun.calInterval(mLLFeeMainSchema.getHospStartDate(),mLLFeeMainSchema.getHospEndDate(),"D") + 1;
	       	 mLLFeeMainSchema.setRealHospDate(tRealHospDay);	       	 
	       	 mLLFeeMainSchema.setSumFee(mSumFee);
	       	 mLLFeeMainSchema.setSelfAmnt(mSelfAmnt);
	       	 mTableSumFee += Double.parseDouble(mSumFee);
//	       	String tMedicareType =tReceiptData.getChildText("MEDICARETYPE");			//医保类型
      		//3544
      		//校验录入的医保类型信息
//      		String tMedicareCode =LLCaseCommon.checkMedicareType(tMedicareType);
//          		if(!"".equals(tMedicareCode)){
//          			buildError("caseRegister", tMedicareCode);
//                    return false;
//          		}else{
//          			mLLFeeMainSchema.setMedicareType(tMedicareType);
//          		}
          	//3544
	       	//  不需要社保判断 
           /* if(mClaimType == null || "".equals(mClaimType)){
            	if(getLLSecurityReceipt(mMainFeeNo)){
            		mLLSecurityReceiptSchema=tLLSecurityReceiptSchema;
            	}
            	else
            	{
            		return false;
            	}
            }
            else{*/
    	       	 mLLSecurityReceiptSchema.setFeeDetailNo(mMainFeeNo);
    	       	 mLLSecurityReceiptSchema.setMainFeeNo(mMainFeeNo);
    	       	 mLLSecurityReceiptSchema.setCaseNo(mLLCaseSchema.getCaseNo());
    	       	 mLLSecurityReceiptSchema.setRgtNo(mLLCaseSchema.getRgtNo());
    	       	 mLLSecurityReceiptSchema.setApplyAmnt(mSumFee);
    	       	 mLLSecurityReceiptSchema.setGetLimit(mGetLimit);
    	       	 mLLSecurityReceiptSchema.setPlanFee(mPlanFee);
    	       	 mLLSecurityReceiptSchema.setSupInHosFee(mSupInHosFee);
    	       	 mLLSecurityReceiptSchema.setSelfAmnt(mSelfAmnt);
    	       	 mLLSecurityReceiptSchema.setSelfPay2(mSelfPay2);
          //  }
	       	
	       	 
	       	 
             tVData.add(mGlobalInput);
             tVData.add(mLLCaseSchema);
             tVData.add(mLLFeeMainSchema);
             tVData.add(mLLCaseReceiptSet);
             tVData.add(mLLSecurityReceiptSchema);
             

             LLCaseReceiptSave tLLCaseReceiptSave = new LLCaseReceiptSave();
             try
             {
                if(!tLLCaseReceiptSave.submitData(tVData, ""))
                {
                    CError.buildErr(this, tLLCaseReceiptSave.mErrors.getErrContent());
                    return false;
                }

             }catch(Exception ex)
             {
                 ex.printStackTrace();
                 CError.buildErr(this, "结算出现未知异常");
                 return false;
             }
                                              
             mLLFeeMainSchema = (LLFeeMainSchema)tLLCaseReceiptSave.getResult().getObjectByObjectName("LLFeeMainSchema", 0);
             mLLFeeMainSet.add(mLLFeeMainSchema);
        }

    	 return true;
    }
    /**
     * 处理检录信息
     * @return boolean
     */
    private boolean caseRegisterCheck(LLFeeMainSet mLLFeeMainSet) throws Exception{
    	System.out.println("LLTJAccidentAutoRegister----caseRegisterCheck");
        mDeformityCode = mBaseData.getChildText("DEFORMITYCODE");  
        mAccidentNo = mBaseData.getChildText("ACCIDENTNO");
        
    	List tDiseaseList = new ArrayList();
    	tDiseaseList = (List) mDiseaseList.getChildren();

    	LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();     	
        for (int i = 0; i < tDiseaseList.size(); i++) 
        {
            Element tDiseaseData = (Element) tDiseaseList.get(i);
            String mDiseaseCode = tDiseaseData.getChildText("DISEASECODE");
            String mDiseaseName = tDiseaseData.getChildText("DISEASENAME");
            
            if (mDiseaseCode == null || "".equals(mDiseaseCode)) {
                buildError("CustomerWS", "【疾病代码】的值不能为空");
                return false;
            }
            if (mDiseaseName == null || "".equals(mDiseaseName)) {
                buildError("CustomerWS", "【疾病名称】的值不能为空");
                return false;
            }           
            LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
            mLLCaseCureSchema.setDiseaseCode(mDiseaseCode);
            mLLCaseCureSchema.setDiseaseName(mDiseaseName);
            mLLCaseCureSet.add(mLLCaseCureSchema);                       
        }
        LLAccidentSet mLLAccidentSet = null;
        if(mAccidentNo != null &&  !mAccidentNo.equals(""))
        {       
        	mLLAccidentSet = new LLAccidentSet();
            LLAccidentSchema mLLAccidentSchema = new LLAccidentSchema();
            mLLAccidentSchema.setCode(mAccidentNo);    
            mLLAccidentSet.add(mLLAccidentSchema);
        }
        LLCaseInfoSet mLLCaseInfoSet = null;
        if(mDeformityCode != null && !mDeformityCode.equals("")) 
        {        	
        	mLLCaseInfoSet =  new LLCaseInfoSet();
            LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
            mLLCaseInfoSchema.setCode(mDeformityCode);
            mLLCaseInfoSet.add(mLLCaseInfoSchema);            
        }
        
        mLLCaseRelaSchema = mLLCaseRelaSet.get(1).getSchema();
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(mLLCaseSchema);
        tVData.add(mLLCaseRelaSchema);
        tVData.add(mLLFeeMainSet);
        tVData.add(mLLCaseCureSet);
        tVData.add(mLLCaseInfoSet);
        tVData.add(mLLAccidentSet);

        LLCaseRegisterCheck tLLCaseRegisterCheck = new LLCaseRegisterCheck();
        try
        {
           if(!tLLCaseRegisterCheck.submitData(tVData, ""))
           {
               CError.buildErr(this, tLLCaseRegisterCheck.mErrors.getErrContent());
               return false;
           }

        }catch(Exception ex)
        {
            ex.printStackTrace();
            CError.buildErr(this, "结算出现未知异常");
            return false;
        }
                     
    	return true;
    }
    /**
     * 处理理算信息
     * @return boolean
     */
    private boolean caseClaim(Element tBnfData,LLClaimPolicySet mLLClaimPolicySet) throws Exception{    
    	 System.out.println("LLTJAccidentAutoRegister----caseClaim");   
         double mSum =0;
         String strLimit = PubFun.getNoLimit(mManageCom);
 	     String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit); 	     
 	     System.out.println("tGetNoticeNo=="+tGetNoticeNo);
 	     
 	     String limit = PubFun.getNoLimit(this.mManageCom);
	     String mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
	     System.out.println("mClmNo======"+mClmNo);
	     
	     
	     Element mPayList = (Element) tBnfData.getChild("PAYLIST");
         List tPayList = new ArrayList();
         tPayList = mPayList.getChildren();
         
         LLCasePolicySet aLLCasePolicySet = new LLCasePolicySet();
         LLToClaimDutySet aLLToClaimDutySet = new LLToClaimDutySet();                
         LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
         LLClaimDetailSet aLLClaimDetailSet = new LLClaimDetailSet();
         LJSGetClaimSet	aLJSGetClaimSet = new LJSGetClaimSet();
         LJSGetSchema mLJSGetSchema = new LJSGetSchema();       
         LLBnfSchema mLLBnfSchema = new LLBnfSchema();
         
         for (int j = 0; j < tPayList.size(); j++) 
         {             
             Element tPayData = (Element) tPayList.get(j);
             String tGetDutyCode = tPayData.getChildText("GETDUTYCODE");
             double mRealPay = Double.parseDouble(tPayData.getChildText("REALPAY"));
             double mDeclineAmnt = Double.parseDouble(tPayData.getChildText("DeclineAmnt"));

             //赔付结论(code)
             String mGiveType=tPayData.getChildText("GIVETYPE");
             //赔付结论依据(code)
             String mGiveReason = tPayData.getChildText("GIVEREASON");
             if (mGiveReason == null || "".equals(mGiveReason)) {
                 buildError("mGiveReason", "【赔付结论依据】的值不能为空");
                 return false;
             } 
             //赔付结论(name)
             String mGiveTypeDesc="";
             //  赔付结论依据(name)
             String mGiveReasonDesc="";
             
             if(!tGetDutyCode.equals("DSBZF"))
             {     
            	 String mGrpContNo ="";
            	/* if(mClaimType == null || "".equals(mClaimType)){
            		 mGrpContNo=mGrpContNoSB;
            	 }else{*/
            		 if(!"".equals(mClaimYear) && mClaimYear != null && !"null".equals(mClaimYear)){
                         String GrpContSql="select codealias from ldcode where codetype='TJAccident' and code='"+mClaimYear+mClaimType+"GrpContNo' and comcode='"+ mManageCom +"'";
                         ExeSQL GrpExeSQL = new ExeSQL();
                         mGrpContNo = GrpExeSQL.getOneValue(GrpContSql);
            		 }
            		 else{
            		 String mAccDateYear=mAccDate.substring(0, 4);
                     String GrpContSql="select codealias from ldcode where codetype='TJAccident' and code='"+mAccDateYear+mClaimType+"GrpContNo' and comcode='"+ mManageCom +"'";
                     ExeSQL GrpExeSQL = new ExeSQL();
                     mGrpContNo = GrpExeSQL.getOneValue(GrpContSql);
            		 }
            	// }
            	
            	 ExeSQL GrpRiskExeSQL = new ExeSQL();
            	 String GrpRiskSQL = "select 1 from lcgrppol where grpcontno='"+mGrpContNo+"' and riskcode in('161301','560901') union select 1 from lbgrppol where grpcontno='"+mGrpContNo+"' and riskcode in('161301','560901') with ur";
            	 String GrpRisk = GrpRiskExeSQL.getOneValue(GrpRiskSQL);
            	 LDCodeDB tLDCodeDB = new LDCodeDB();
                 LDCodeSet tLDCodeSet = new LDCodeSet();
                 if(("00090166000003").equals(mGrpContNo) || ("00173336000001").equals(mGrpContNo))
                 {
                 String LDCodeSql ="select * from ldcode where codetype='TJAccident2013' and code='"+ tGetDutyCode +"' and comcode='"+mManageCom+"'";
                 System.out.println("LDCodeSql===="+LDCodeSql);
                 tLDCodeSet = tLDCodeDB.executeQuery(LDCodeSql);
                 }
                 //增加了2014年的保单理赔 by panruiming at 2014-05-08  00173336000002  00090166000005 
                 else if(("00173336000002").equals(mGrpContNo) || ("00090166000005").equals(mGrpContNo))
                 {
                	 String LDCodeSql ="select * from ldcode where codetype='TJAccident2014' and code='"+ tGetDutyCode +"' and comcode='"+mManageCom+"'";
                     System.out.println("LDCodeSql===="+LDCodeSql);
                     tLDCodeSet = tLDCodeDB.executeQuery(LDCodeSql); 
                 }
                 //add by zjd 2015-02-27 增加保单：00203082000002  2015-03-16 增加油田 00173336000003
                 else if(("00203082000002").equals(mGrpContNo) || ("00173336000003").equals(mGrpContNo))
                 {
                	 String LDCodeSql ="select * from ldcode where codetype='TJAccident2015' and code='"+ tGetDutyCode +"' and comcode='"+mManageCom+"'";
                     System.out.println("LDCodeSql===="+LDCodeSql);
                     tLDCodeSet = tLDCodeDB.executeQuery(LDCodeSql); 
                 }
                 //2016年3月1日  增加意外险保单：00203082000006   
                 else if(("00203082000006").equals(mGrpContNo))
                 {
                	 String LDCodeSql ="select * from ldcode where codetype='TJAccident2016' and code='"+ tGetDutyCode +"' and comcode='"+mManageCom+"'";
                     System.out.println("LDCodeSql===="+LDCodeSql);
                     tLDCodeSet = tLDCodeDB.executeQuery(LDCodeSql); 
                 }
                 //2017年3月27日  增加意外险保单：00090166000006   
                 else if(("00090166000006").equals(mGrpContNo)||"1".equals(GrpRisk))
                 {
                	 String LDCodeSql ="select * from ldcode where codetype='TJAccident2017' and code='"+ tGetDutyCode +"' and comcode='"+mManageCom+"'";
                     System.out.println("LDCodeSql===="+LDCodeSql);
                     tLDCodeSet = tLDCodeDB.executeQuery(LDCodeSql); 
                 }
                 
     
                 else
                 {
                 String LDCodeSql ="select * from ldcode where codetype='TJAccident' and code='"+ tGetDutyCode +"' and comcode='"+mManageCom+"'";
                 System.out.println("LDCodeSql===="+LDCodeSql);
                 tLDCodeSet = tLDCodeDB.executeQuery(LDCodeSql);
                 }

                 String RiskCodeSql="";
                 String GetDudyCodeSql="";
                 if(tLDCodeSet == null || tLDCodeSet.size()<=0 || "".equals( tLDCodeSet.get(1).getCodeName()) || null ==  tLDCodeSet.get(1).getCodeName())
                 {
                	 buildError("caseClaim", "未在系统中配置险种信息");
                     return false;
                 }else{	    
                	 	 LDCodeSchema tLDCodeSchema = new LDCodeSchema();
                	     tLDCodeSchema = tLDCodeSet.get(1).getSchema();
                	 	 RiskCodeSql +=" and riskcode='"+tLDCodeSchema.getCodeName()+"'";
                		 if(!"".equals(tLDCodeSchema.getCodeAlias()) && null !=tLDCodeSchema.getCodeAlias())
                		 {
                			 GetDudyCodeSql +=" and getdutycode='"+tLDCodeSchema.getCodeAlias()+"'";
                		 }
                 }	           
                                       
                 String LCPolSql ="select * from lcpol where contno='"+ mContNo +"'" + RiskCodeSql;
                 LCPolDB mLCPolDB = new LCPolDB();
                 System.out.println("LCPolSql===="+LCPolSql);
                 LCPolSet mLCPolSet= mLCPolDB.executeQuery(LCPolSql);	             
                 if(mLCPolSet == null || mLCPolSet.size()<= 0)
                 {
                	 buildError("caseClaim", "保单险种信息查询失败");
                     return false;
                 }
                 LCPolSchema mLCPolSchema = new LCPolSchema();
                 mLCPolSchema = mLCPolSet.get(1);
                 
                 String LCGetSql ="select * from lcget where polno='"+ mLCPolSchema.getPolNo() +"'" + GetDudyCodeSql;
                 LCGetDB mLCGetDB = new LCGetDB();
                 System.out.println("LCGetSql===="+LCGetSql);
                 LCGetSet mLCGetSet= mLCGetDB.executeQuery(LCGetSql);
                 	           
                 if(mLCGetSet == null || mLCGetSet.size()<= 0)
                 {
                	 buildError("caseClaim", "保单给付责任信息查询失败");
                     return false;
                 }
                 LCGetSchema mLCGetSchema = new LCGetSchema();
                 mLCGetSchema = mLCGetSet.get(1);
                                
                 for(int u =1 ; u<=aLLCasePolicySet.size() ; u++)
                 {
                	 if(aLLCasePolicySet.get(u).getCaseNo().equals(mLLCaseSchema.getCaseNo())
                			 && aLLCasePolicySet.get(u).getRiskCode().equals(mLCPolSchema.getRiskCode()))
                	 {
                		 buildError("caseClaim", "每个受益人只能赔付一种类型责任");
                         return false; 
                	 }
                 }
                 //得到赔付结论和赔付结论依据(name)
                 String giveTypeSql = "select CodeName from ldcode where 1 = 1 and codetype = 'llclaimdecision'  and code='"+mGiveType+"' order by Code";
                 String giveReasonSql ="select a.CodeName from ldcode a where  trim(a.codetype)=(select trim(b.codeaLias) from ldcode b where b.codetype='llclaimdecision' and b.code='"+mGiveType+"')  and a.code='"+mGiveReason+"' order by a.code";
                 ExeSQL giveTypeSQL = new ExeSQL();
                 mGiveTypeDesc = giveTypeSQL.getOneValue(giveTypeSql);
                 mGiveReasonDesc =giveTypeSQL.getOneValue(giveReasonSql);
                 
    			 LLCasePolicySchema mLLCasePolicySchema = new LLCasePolicySchema();
             	 mLLCasePolicySchema.setCaseNo(mLLCaseSchema.getCaseNo());
                 mLLCasePolicySchema.setRgtNo(mLLCaseSchema.getRgtNo());
                 mLLCasePolicySchema.setCasePolType("0");
                 mLLCasePolicySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
                 mLLCasePolicySchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
                 mLLCasePolicySchema.setContNo(mLCPolSchema.getContNo());
                 mLLCasePolicySchema.setPolNo(mLCPolSchema.getPolNo());
                 mLLCasePolicySchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
                 mLLCasePolicySchema.setKindCode(mLCPolSchema.getKindCode());
                 mLLCasePolicySchema.setRiskVer(mLCPolSchema.getRiskVersion());
                 mLLCasePolicySchema.setRiskCode(mLCPolSchema.getRiskCode());
                 mLLCasePolicySchema.setPolMngCom(mLCPolSchema.getManageCom());
                 mLLCasePolicySchema.setSaleChnl(mLCPolSchema.getSaleChnl());
                 mLLCasePolicySchema.setAgentCode(mLCPolSchema.getAgentCode());
                 mLLCasePolicySchema.setAgentGroup(mLCPolSchema.getAgentGroup());
                 mLLCasePolicySchema.setInsuredNo(mLCPolSchema.getInsuredNo());
                 mLLCasePolicySchema.setInsuredName(mLCPolSchema.getInsuredName());
                 mLLCasePolicySchema.setInsuredSex(mLCPolSchema.getInsuredSex());
                 mLLCasePolicySchema.setInsuredBirthday(mLCPolSchema.getInsuredBirthday());
                 mLLCasePolicySchema.setAppntNo(mLCPolSchema.getAppntNo());
                 mLLCasePolicySchema.setAppntName(mLCPolSchema.getAppntName());
                 mLLCasePolicySchema.setCValiDate(mLCPolSchema.getCValiDate());
                 mLLCasePolicySchema.setPolType("1");
                 mLLCasePolicySchema.setMngCom(mManageCom);
                 mLLCasePolicySchema.setOperator(mOperator);
                 mLLCasePolicySchema.setMakeDate(mCurrentDate);
                 mLLCasePolicySchema.setMakeTime(mCurrentTime);
                 mLLCasePolicySchema.setModifyDate(mCurrentDate);
                 mLLCasePolicySchema.setModifyTime(mCurrentTime);
                 aLLCasePolicySet.add(mLLCasePolicySchema);
     
                 LLToClaimDutySchema mLLToClaimDutySchema = new LLToClaimDutySchema();
                 Reflections LLToClaimDutytref = new Reflections();
                 LLToClaimDutytref.transFields(mLLToClaimDutySchema, mLLCasePolicySchema);
                 
                 mLLToClaimDutySchema.setGetDutyCode(mLCGetSchema.getGetDutyCode());
                 LMDutyGetClmDB mLMDutyGetClmDB = new LMDutyGetClmDB();
                 mLMDutyGetClmDB.setGetDutyCode(mLCGetSchema.getGetDutyCode());
                 if(mLMDutyGetClmDB.query().size()!=1)
                 {
                	 buildError("caseClaim", "查询出多条给付责任类型");
                     return false;
                 }
                 mLLToClaimDutySchema.setGetDutyKind(mLMDutyGetClmDB.query().get(1).getGetDutyKind());
                 mLLToClaimDutySchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
                 mLLToClaimDutySchema.setSubRptNo(mLLSubReportSet.get(1).getSubRptNo());	             
                 mLLToClaimDutySchema.setRiskVer(mLCPolSchema.getRiskVersion());
                 mLLToClaimDutySchema.setPolMngCom(mLCPolSchema.getManageCom());
                 mLLToClaimDutySchema.setClaimCount("0");
                 mLLToClaimDutySchema.setEstClaimMoney("0");
                 mLLToClaimDutySchema.setDutyCode(mLCGetSchema.getDutyCode());	
                 aLLToClaimDutySet.add(mLLToClaimDutySchema);
    		     	
                 mSum += mRealPay;
                 Reflections LLClaimtref = new Reflections();
                 LLClaimtref.transFields(mLLClaimSchema, mLLCasePolicySchema);
                 
    			 mLLClaimSchema.setClmNo(mClmNo);
                 mLLClaimSchema.setRgtNo(mLLCaseSchema.getRgtNo());
                 mLLClaimSchema.setCaseNo(mLLCaseSchema.getCaseNo());
                 mLLClaimSchema.setGetDutyKind(mLLToClaimDutySchema.getGetDutyKind());
                 mLLClaimSchema.setClmState("2");
                 mLLClaimSchema.setStandPay(mSum);
                 mLLClaimSchema.setRealPay(mSum);
                 mLLClaimSchema.setGiveType(mGiveType);
                 mLLClaimSchema.setGiveTypeDesc(mGiveTypeDesc);
                 mLLClaimSchema.setClmUWer(mOperator);
                 mLLClaimSchema.setCheckType("0");
                 mLLClaimSchema.setMngCom(mManageCom);	                
    	                       	             
                 Reflections LLClaimPolicytref = new Reflections();
                 LLClaimPolicytref.transFields(mLLClaimPolicySchema, mLLCasePolicySchema);
                 
                 mLLClaimPolicySchema.setClmNo(mClmNo);
                 mLLClaimPolicySchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());	
                 mLLClaimPolicySchema.setGetDutyKind(mLLToClaimDutySchema.getGetDutyKind());	            
                 mLLClaimPolicySchema.setRiskVer(mLCPolSchema.getRiskVersion());
                 mLLClaimPolicySchema.setPolMngCom(mLCPolSchema.getManageCom());	            
                 mLLClaimPolicySchema.setAppntNo(mLCPolSchema.getAppntNo());
                 mLLClaimPolicySchema.setAppntName(mLCPolSchema.getAppntName());
                 mLLClaimPolicySchema.setCValiDate(mLCPolSchema.getCValiDate());
                 mLLClaimPolicySchema.setClmState("1");
                 mLLClaimPolicySchema.setStandPay(mSum);
                 mLLClaimPolicySchema.setRealPay(mSum);
                 mLLClaimPolicySchema.setPreGiveAmnt("0");
                 mLLClaimPolicySchema.setSelfGiveAmnt("0");
                 mLLClaimPolicySchema.setRefuseAmnt("0");
                 mLLClaimPolicySchema.setApproveAmnt("0");
                 mLLClaimPolicySchema.setAgreeAmnt("0");
                 mLLClaimPolicySchema.setGiveType(mGiveType);
                 mLLClaimPolicySchema.setGiveTypeDesc(mGiveTypeDesc);
                 mLLClaimPolicySchema.setGiveReason(mGiveReason);
                 mLLClaimPolicySchema.setGiveReasonDesc(mGiveReasonDesc);
                 mLLClaimPolicySchema.setClmUWer("mOperator");
                 mLLClaimPolicySchema.setMngCom(mManageCom);
                 mLLClaimPolicySet.add(mLLClaimPolicySchema);                 
                 
                 LLClaimDetailSchema mLLClaimDetailSchema = new LLClaimDetailSchema();
                 Reflections LLClaimDetailtref = new Reflections();
                 LLClaimDetailtref.transFields(mLLClaimDetailSchema, mLLCasePolicySchema);
                 
                 mLLClaimDetailSchema.setRgtNo(mLLCaseSchema.getRgtNo());
                 mLLClaimDetailSchema.setClmNo(mClmNo);	             
                 mLLClaimDetailSchema.setGetDutyCode(mLCGetSchema.getGetDutyCode());
                 mLLClaimDetailSchema.setGetDutyKind(mLLToClaimDutySchema.getGetDutyKind());	             
                 mLLClaimDetailSchema.setCaseRelaNo(mLLCaseRelaSet.get(1).getCaseRelaNo());	             
                 mLLClaimDetailSchema.setRiskVer(mLCPolSchema.getRiskVersion());
                 mLLClaimDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());	             
                 mLLClaimDetailSchema.setTabFeeMoney(mTableSumFee);
                 mLLClaimDetailSchema.setClaimMoney(mRealPay);
                 mLLClaimDetailSchema.setDeclineAmnt(mDeclineAmnt);
                 mLLClaimDetailSchema.setOverAmnt("0");
                 mLLClaimDetailSchema.setStandPay(mRealPay);
                 mLLClaimDetailSchema.setRealPay(mRealPay);
                 mLLClaimDetailSchema.setPreGiveAmnt("0");
                 mLLClaimDetailSchema.setSelfGiveAmnt("0");
                 mLLClaimDetailSchema.setRefuseAmnt("0");
                 mLLClaimDetailSchema.setOtherAmnt("0");
                 mLLClaimDetailSchema.setOutDutyAmnt("0");
                 mLLClaimDetailSchema.setOutDutyRate("1");
                 mLLClaimDetailSchema.setApproveAmnt("0");
                 mLLClaimDetailSchema.setAgreeAmnt("0");
                 mLLClaimDetailSchema.setDutyCode(mLCGetSchema.getDutyCode());
                 mLLClaimDetailSchema.setGiveType(mGiveType);
                 mLLClaimDetailSchema.setGiveTypeDesc(mGiveTypeDesc);
                 mLLClaimDetailSchema.setGiveReason(mGiveReason);
                 mLLClaimDetailSchema.setGiveReasonDesc(mGiveReasonDesc);
                 mLLClaimDetailSchema.setMngCom(mManageCom);
                 mLLClaimDetailSchema.setStatType(mLMDutyGetClmDB.query().get(1).getStatType());
                 aLLClaimDetailSet.add(mLLClaimDetailSchema);
                 	            
                 LJSGetClaimSchema mLJSGetClaimSchema = new LJSGetClaimSchema();    
                 Reflections LJSGetClaimtref = new Reflections();
                 LJSGetClaimtref.transFields(mLJSGetClaimSchema, mLLCasePolicySchema);
          	    
    			 mLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
                 mLJSGetClaimSchema.setFeeOperationType(mLLToClaimDutySchema.getGetDutyKind());
                 mLJSGetClaimSchema.setFeeFinaType(mLLClaimDetailSchema.getStatType());
                 mLJSGetClaimSchema.setOtherNo(mLLCaseSchema.getCaseNo());
                 mLJSGetClaimSchema.setOtherNoType("5");
                 mLJSGetClaimSchema.setGetDutyKind(mLLToClaimDutySchema.getGetDutyKind());
                 mLJSGetClaimSchema.setRiskVersion(mLCPolSchema.getRiskVersion());
                 mLJSGetClaimSchema.setPay(mRealPay);
                 mLJSGetClaimSchema.setManageCom(mLCPolSchema.getManageCom());
                 mLJSGetClaimSchema.setAgentCom(mLCPolSchema.getAgentCom());
                 aLJSGetClaimSet.add(mLJSGetClaimSchema);                                                                   
                	     	   
	     	    Reflections LJSGettref = new Reflections();
	     	    LJSGettref.transFields(mLJSGetSchema, aLJSGetClaimSet.get(1)); 
	            mLJSGetSchema.setSumGetMoney(mSum);  	            
	            	           	            
	            Reflections LLBnftref = new Reflections();
	            LLBnftref.transFields(mLLBnfSchema, aLJSGetClaimSet.get(1));
	           
	            mLLBnfSchema.setContNo(mContNo);	             
	            mLLBnfSchema.setInsuredNo(mInsuredNo);	//被保险人客户号
	            mLLBnfSchema.setBnfType("1");
	            mLLBnfSchema.setBnfNo(mBnfGrade);            
	            mLLBnfSchema.setRelationToInsured(mBnfRealtion);
	            mLLBnfSchema.setBnfGrade(mBnfGrade);
	            mLLBnfSchema.setBnfLot(mBnfRate);
	            mLLBnfSchema.setCaseNo(mLLClaimSchema.getCaseNo());
	            mLLBnfSchema.setCaseGetMode("4");
	            mLLBnfSchema.setGetMoney(mSum);
	            mLLBnfSchema.setBankCode(mBankCode);
	            mLLBnfSchema.setBankAccNo(mAccNo);
	            mLLBnfSchema.setAccName(mAccName);
	            mLLBnfSchema.setBnfNo(mBnfGrade);			//受益人序号
	            mLLBnfSchema.setCustomerNo(mBnfNo);
	            mLLBnfSchema.setName(mBnfName);
	            mLLBnfSchema.setSex(mBnfSex);
	            mLLBnfSchema.setBirthday(mBnfBirthDay);
	            mLLBnfSchema.setIDType(mBnfIDType);
	            mLLBnfSchema.setIDNo(mBnfIDNO);	            
	    	               	     	  
	       	    mLLCaseSchema.setRgtState("04"); 
	       	    mLLCaseSchema.setModifyDate(mCurrentDate);
	       	    mLLCaseSchema.setModifyTime(mCurrentTime);
	       	    		       	
             	}                 	
             }  
         MMap aMMap = new MMap();           
         aMMap.put(aLLCasePolicySet, "DELETE&INSERT");
         aMMap.put(aLLToClaimDutySet, "DELETE&INSERT");
         aMMap.put(mLLClaimSchema, "DELETE&INSERT");
         aMMap.put(mLLClaimPolicySchema, "DELETE&INSERT");
         aMMap.put(aLLClaimDetailSet, "DELETE&INSERT");
         aMMap.put(aLJSGetClaimSet, "DELETE&INSERT");   
	     aMMap.put(mLJSGetSchema, "DELETE&INSERT");	 
	     aMMap.put(mLLBnfSchema, "DELETE&INSERT");
	     aMMap.put(mLLCaseSchema, "DELETE&INSERT");
    	    
	     //提交数据库
         PubSubmit tPubSubmit = new PubSubmit();
         mInputData.clear();       
         mInputData.add(aMMap);
         if (!tPubSubmit.submitData(mInputData, ""))
         {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "LLTJAccidentAutoRegister";
                 tError.functionName = "dealData";
                 tError.errorMessage = "理赔理算数据提交失败!";
                 this.mErrors.addOneError(tError);
                 return false;
         }

    	return true;
    }
    
    /**
     * 处理理算信息
     * @return boolean
     */
    private boolean caseUnderWrite(LLClaimPolicySet mLLClaimPolicySet) throws Exception{
    	  System.out.println("LLTJAccidentAutoRegister----caseUnderWrite");
    	  LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
    	 /* if(mClaimType == null || "".equals(mClaimType))
    	  {
    		  mLLClaimUWMainSchema.setRemark1("社保险平台前置审核");
    	  }
    	  else
    	  {*/
    		  mLLClaimUWMainSchema.setRemark1("意外险平台前置审核");
    	//  }
    	  VData tVData = new VData();
          tVData.add(mGlobalInput);
          tVData.add(mLLCaseSchema);
          tVData.add(mLLClaimSchema);
          tVData.add(mLLClaimPolicySet);
          tVData.add(mLLClaimUWMainSchema);

          LLCaseUnderWrite tLLCaseUnderWrite = new LLCaseUnderWrite();
          try
          {
        	/* if(mClaimType == null || "".equals(mClaimType)){
        		 if(!tLLCaseUnderWrite.submitData(tVData, "UWCASE"))
                 {
                     CError.buildErr(this, tLLCaseUnderWrite.mErrors.getErrContent());
                     return false;
                 }
        	 }
        	 else{*/
	             if(!tLLCaseUnderWrite.submitData(tVData, "ENDCASE"))
	             {
	                 CError.buildErr(this, tLLCaseUnderWrite.mErrors.getErrContent());
	                 return false;
	             }
             //}

          }catch(Exception ex)
          {
              ex.printStackTrace();
              CError.buildErr(this, "结算出现未知异常");
              return false;
          }         
          
          mLLCaseSchema = (LLCaseSchema)tLLCaseUnderWrite.getResult().getObjectByObjectName("LLCaseSchema", 0);
          System.out.println("案件状态=="+mLLCaseSchema.getRgtState());
    	  return true;
    }    
    
    //
    
    
    
    private boolean cancelCase() throws Exception {
    	MMap tmpMap = new MMap();

		for(int i =1 ; i <= mLLCaseSet.size() ; i++)
    	{
    		String aCaseNo = mLLCaseSet.get(i).getCaseNo();
    		System.out.println("LLTJAccidentAutoRegister--cancelCase");
    	    
    		//理算数据        
    		String tLJSSQL = "delete from ljsget where otherno='" + aCaseNo + "' and othernotype='5'";
    		String tLJSGSQL = "delete from ljsgetclaim where otherno='" + aCaseNo + "' and othernotype='5'";
    		String tBnfSQL = "delete from llbnf where caseno='" + aCaseNo + "' ";
    		
    		String tUwmSQL = "delete from llclaimuwmain where CaseNo='" + aCaseNo + "'";
    		String tUwdeSQL = "delete from llclaimuwdetail where CaseNo='" + aCaseNo + "'";
    		String tUnderSQL = "delete from llclaimunderwrite  where CaseNo='" + aCaseNo + "'";
    		String tUwmdSQL = "delete from llclaimuwmdetail  where CaseNo='" + aCaseNo + "'";
	        String tClaimSQL = "delete from LLClaim where CaseNo='" + aCaseNo + "'";
	        String tClaimPolicySQL = "delete from LLClaimPolicy where CaseNo='" + aCaseNo + "'";
	        String tClaimDetailSQL = "delete from LLClaimDetail where CaseNo='" + aCaseNo + "'";
	        String tInsureAccTrace = "delete from LCInsureAccTrace where OtherNo='" + aCaseNo + "'";
	        String tToClaimDutySQL = "delete from LLToClaimDuty where CaseNo ='" + aCaseNo + "'";
	        String tCasePolicySQL = "delete from LLCasePolicy where CaseNo ='"+ aCaseNo + "'";
	        String tFeeMainSQL = "delete from LLFeeMain where CaseNo='" + aCaseNo + "'";
	        String tSecuritySQL = "delete from LLSecurityReceipt where CaseNo = '" +aCaseNo + "'";
	        String tFeeItemSQL = "delete from LLFeeOtherItem where CaseNo = '" +aCaseNo + "'";
	        String tCaseReceiptSQL = "delete from LLCaseReceipt where CaseNo = '" + aCaseNo + "'";
	        String tCaseDrugSQL = "delete from LLCaseDrug where CaseNo = '" +aCaseNo + "'";
	        String tCaseCureSQL = "delete from LLCaseCure where CaseNo = '" +aCaseNo + "'";
	        String tOperationSQL = "delete from LLOperation where CaseNo = '" +aCaseNo + "'";
	        String tAccidentSQL = "delete from LLAccident where CaseNo = '" +aCaseNo + "'";
	        String tSubReportSQL = "delete from LLSubReport a where exists (select 1 from llcaserela where a.subrptno=subrptno and CaseNo='" +aCaseNo + "')";
	        String tCaseRelaSQL = "delete from LLCaseRela where CaseNo='" + aCaseNo +"'";
	        String tCaseSQL = "delete from LLCase where CaseNo='" + aCaseNo + "'";
	        String tRegisterSQL = "delete from LLRegister where RgtNo ='" + aCaseNo +"'";
	        String tHospCaseSQL = "delete from LLHospCase where CaseNo ='" +aCaseNo + "'";
	       // String tLLReceiptSQL = "delete from LLReceipt where CaseNo ='" +aCaseNo + "'";
	       
	        tmpMap.put(tLJSSQL, "DELETE");
	        tmpMap.put(tLJSGSQL, "DELETE");
	        tmpMap.put(tBnfSQL, "DELETE");
	        
	        tmpMap.put(tUwmSQL, "DELETE");
	        tmpMap.put(tUwdeSQL, "DELETE");
	        tmpMap.put(tUnderSQL, "DELETE");
	        tmpMap.put(tUwmdSQL, "DELETE");
	        
	        tmpMap.put(tClaimSQL, "DELETE");
	        tmpMap.put(tClaimPolicySQL, "DELETE");
	        tmpMap.put(tClaimDetailSQL, "DELETE");
	        tmpMap.put(tInsureAccTrace, "DELETE");
	        tmpMap.put(tToClaimDutySQL, "DELETE");
	        tmpMap.put(tCasePolicySQL, "DELETE");
	        tmpMap.put(tFeeMainSQL, "DELETE");
	        tmpMap.put(tSecuritySQL, "DELETE");
	        tmpMap.put(tCaseReceiptSQL, "DELETE");
	        tmpMap.put(tCaseDrugSQL, "DELETE");
	        tmpMap.put(tFeeItemSQL, "DELETE");
	        tmpMap.put(tCaseCureSQL, "DELETE");
	        tmpMap.put(tOperationSQL, "DELETE");
	        tmpMap.put(tAccidentSQL, "DELETE");
	        tmpMap.put(tSubReportSQL, "DELETE");
	        tmpMap.put(tCaseRelaSQL, "DELETE");
	        tmpMap.put(tCaseSQL, "DELETE");
	        tmpMap.put(tRegisterSQL, "DELETE");
	        tmpMap.put(tHospCaseSQL, "DELETE");
	      //  tmpMap.put(tLLReceiptSQL, "DELETE");

    	}
        try {
            this.mInputData.clear();
            this.mInputData.add(tmpMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            buildError("cancelCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("cancelCase", "撤销数据提交失败");
            return false;
        }

        return false;
    }
    /**
     * 社保补充账单列表
     * @return boolean
     */
    /*private boolean getLLSecurityReceipt(String mainFeeNo){
    	System.out.println("=====社保补充账单列表（多条）SECURITYLIST 隶属于案件信息 start=====");
    	
    	List securityList = new ArrayList();
    	securityList=(List)mSecurityList.getChildren();
    	
    	for(int i=0;i<securityList.size();i++){
    		Element securityData = (Element)securityList.get(i);
    		String tMainFeeNo = securityData.getChildText("SBBC_MAINFEENO");
    		if(mainFeeNo.equals(tMainFeeNo)||mainFeeNo==tMainFeeNo){
    			
    		}else{
    			 buildError("getLLSecurityReceipt", "社保补充账单不能为空");
	             return false;
    		}
    	}
    	
    	for(int j=0;j<securityList.size();j++){
    		Element securityData = (Element)securityList.get(j);
    		
    		String tFeetype = securityData.getChildText("SBBC_FEETYPE");
    		String tFeeAtti = securityData.getChildText("FEEATTI");
    		String tMainFeeNo = securityData.getChildText("SBBC_MAINFEENO");
    		String tApplyAmnt = securityData.getChildText("APPLYAMNT");
    		String tPlanFee = securityData.getChildText("PLANFEE");
    		String tSupInHosFee = securityData.getChildText("SUPINHOSFEE");
    		String tOfficialSubsidy = securityData.getChildText("OFFICIALSUBSIDY");
    		String tSelfAmnt = securityData.getChildText("SELFAMNT");
    		String tSelfPay1 = securityData.getChildText("SELFPAY1");
    		String tSelfPay2 = securityData.getChildText("SELFPAY2");
    		String tGetLimit = securityData.getChildText("GETLIMIT");
    		String tMidAmnt = securityData.getChildText("MIDAMNT");
    		String tHighAmnt1 = securityData.getChildText("HIGHAMNT1");
    		String tHighAmnt2 = securityData.getChildText("HIGHAMNT2");
    		String tSuperAmnt = securityData.getChildText("SUPERAMNT");
    		String tRetireAddFee = securityData.getChildText("RETIREADDFEE");
    		String tSmallDoorPay = securityData.getChildText("SMALLDOORPAY");
    		String tHighDoorAmnt = securityData.getChildText("HIGHDOORAMNT");
    		String tFeeInSecu = securityData.getChildText("FEEINSECU");
    		String tYearSupInHosFee = securityData.getChildText("YEARSUPINHOSFEE");
    		
    		if (tFeetype == null || "".equals(tFeetype)) {
                buildError("insertLLSecurityReceipt", "【账单类型】的值不能为空");
                return false;
            }  
	   		 if (tFeeAtti == null || "".equals(tFeeAtti)) {
	                buildError("insertLLSecurityReceipt", "【账单属性】的值不能为空");
	                return false;
	            }  
	   		 if (tMainFeeNo == null || "".equals(tMainFeeNo)) {
	                buildError("insertLLSecurityReceipt", "【账单号码】的值不能为空");
	                return false;
	            }  
	   		 if (tApplyAmnt == null || "".equals(tApplyAmnt)) {
	                buildError("insertLLSecurityReceipt", "【申报金额】的值不能为空");
	                return false;
	            }   
	       	 
    		tLLSecurityReceiptSchema.setMainFeeNo(mainFeeNo);
    		tLLSecurityReceiptSchema.setFeeDetailNo(mainFeeNo);
	       	tLLSecurityReceiptSchema.setCaseNo(mLLCaseSchema.getCaseNo());
	       	tLLSecurityReceiptSchema.setRgtNo(mLLCaseSchema.getRgtNo());
    		tLLSecurityReceiptSchema.setApplyAmnt(tApplyAmnt);
    		tLLSecurityReceiptSchema.setPlanFee(tPlanFee);
    		tLLSecurityReceiptSchema.setSupInHosFee(tSupInHosFee);
    		tLLSecurityReceiptSchema.setOfficialSubsidy(tOfficialSubsidy);
    		tLLSecurityReceiptSchema.setSelfAmnt(tSelfAmnt);
    		tLLSecurityReceiptSchema.setSelfPay1(tSelfPay1);
    		tLLSecurityReceiptSchema.setSelfPay2(tSelfPay2);
    		tLLSecurityReceiptSchema.setGetLimit(tGetLimit);
    		tLLSecurityReceiptSchema.setMidAmnt(tMidAmnt);
    		tLLSecurityReceiptSchema.setHighAmnt1(tHighAmnt1);
    		tLLSecurityReceiptSchema.setHighAmnt2(tHighAmnt2);
    		tLLSecurityReceiptSchema.setSuperAmnt(tSuperAmnt);
    		tLLSecurityReceiptSchema.setRetireAddFee(tRetireAddFee);
    		tLLSecurityReceiptSchema.setSmallDoorPay(tSmallDoorPay);
    		tLLSecurityReceiptSchema.setHighDoorAmnt(tHighDoorAmnt);
    		tLLSecurityReceiptSchema.setHighDoorAmnt(tHighDoorAmnt);
    		tLLSecurityReceiptSchema.setFeeInSecu(tFeeInSecu);
    		tLLSecurityReceiptSchema.setYearSupInHosFee(tYearSupInHosFee);
    		tLLSecurityReceiptSchema.setOperator(mOperator);
    		tLLSecurityReceiptSchema.setMakeDate(mCurrentDate);
    		tLLSecurityReceiptSchema.setMakeTime(mCurrentTime);
    		tLLSecurityReceiptSchema.setModifyDate(mCurrentDate);
    		tLLSecurityReceiptSchema.setModifyTime(mCurrentTime);
    	}
    	
        System.out.println("=====社保补充账单列表（多条）SECURITYLIST 隶属于案件信息 end=====");
    	return true;
    }*/
    
    public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("D://TJ0186120000201802241000130725_122717.xml"), "GBK");
            LLTJAccidentAutoRegister tBusinessDeal = new LLTJAccidentAutoRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
