package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;
import java.io.FileInputStream;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author not attributable
 * @version 1.0
 */
public class LLTJAccidentAccModify implements ServiceInterface {
    public LLTJAccidentAccModify() {
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMMap = new MMap();
    private String mErrorMessage = "";	//错误描述    
   
    private Document mInXmlDoc;	 			//传入报文    
    private String mDocType = "";			//报文类型    
    private boolean mDealState = true;		//处理标志    
    private String mResponseCode = "1";		//返回类型代码    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码
    private String mDealType = "1";    		//处理类型 1 实时 2非实时       
   
    private Element tBodyData;			//报文BASE_PART被保险人信息部分        
    private String mManageCom = "";				//机构编码   
    private String mOperator  = "";				//操作员
      
    private String mClaimNo = "";    
    private String mBnfNo = "";
    private String mBnfName = ""; 
    private String mModifyCount = "";    
    private String mBankCode = ""; 
    private String mAccNo = ""; 
    private String mAccName = "";     
     
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public Document service(Document pInXmlDoc) {
        //兄弟别怨我，我也是被逼的，时间太紧，都写到一个类里了
        System.out.println("LLTJAccidentAccModify--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else {
                    	PubSubmit tPubSubmit = new PubSubmit();
                    	mResult.clear();
                    	mResult.add(mMMap);
                        if (!tPubSubmit.submitData(mResult, "")) {
                        	 mDealState = false;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }
    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState && "".equals(mClaimNo)) {
            return createFalseXML();
        } else {
        	try {
        	if (!"".equals(mClaimNo)) {
        		return createResultXML(mClaimNo);
        	} else {
        		return createResultXML(mClaimNo);
        	}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }     

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJAccidentAccModify--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
        System.out.println("LLTJAccidentAccModify--TransactionNum:" + mTransactionNum);

        //获取BODY部分内容
        tBodyData = tRootData.getChild("BODY");               

        return true;
    }
    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLTJAccidentAccModify--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"TJ03".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||mTransactionNum.equals("null")||
            mTransactionNum.length() < 16) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if (!mManageCom.equals("86120000")) {
            buildError("checkData()", "天津城乡居民意外项目的管理机构传入错误");
            return false;
        }
        
        return true;
    }
    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
       System.out.println("LLTJAccidentAccModify--dealData");  
        
        mClaimNo = tBodyData.getChildText("CLAIM_NO");
        mBnfNo = tBodyData.getChildText("BNFNO");    
        mBnfName = tBodyData.getChildText("BNFNAME");  
        mModifyCount = tBodyData.getChildText("MODIFYCOUNT");  
        mBankCode = tBodyData.getChildText("BANKCODE");  
        mAccNo = tBodyData.getChildText("ACCNO");  
        mAccName = tBodyData.getChildText("ACCNAME");  

        if (mClaimNo == null || "".equals(mClaimNo)|| mClaimNo.equals("null")) {
            buildError("dealData", "【平台理赔号】的值不能为空");
            return false;
        }
        if (mBnfNo == null || "".equals(mBnfNo)|| mBnfNo.equals("null")) {
            buildError("dealData", "【受益人编号】的值不能为空");
            return false;
        }
        if (mBnfName == null || "".equals(mBnfName)|| mBnfName.equals("null")) {
            buildError("dealData", "【受益人姓名】的值不能为空");
            return false;
        }
        if (mModifyCount == null || "".equals(mModifyCount)|| mModifyCount.equals("null")) {
            buildError("dealData", "【修改次数】的值不能为空");
            return false;
        }
        if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
            buildError("dealData", "【银行编码】的值不能为空");
            return false;
        }
        if (mAccNo == null || "".equals(mAccNo)|| mAccNo.equals("null")) {
            buildError("dealData", "【银行账号】的值不能为空或账号长度超过16位");
            return false;
        }
        if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
            buildError("dealData", "【银行账户名】的值不能为空");
            return false;
        }
        
          
        LLHospCaseDB mLLHospCaseDB = new LLHospCaseDB();
        LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();
        //mLLHospCaseDB.setClaimNo(mClaimNo);
        mLLHospCaseDB.setCaseNo(mClaimNo);
        mLLHospCaseDB.setBnfNo(mBnfNo);
        mLLHospCaseSet = mLLHospCaseDB.query();
        ExeSQL tllExeSQL = new ExeSQL();
        SSRS tllSSRS = new SSRS();
        if(mLLHospCaseSet.size()>=1)
        {        	        
        	for(int i = 1; i<=mLLHospCaseSet.size(); i++)
        	{
        		LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
        		mLLHospCaseSchema = mLLHospCaseSet.get(i);
        		String Sqlll ="select a.actugetno ,a.otherno,a.sumgetmoney,a.Drawer,"
      	          +"a.DrawerID,a.BankCode,a.BankAccNo,a.AccName,bankonthewayflag,confdate,cansendbank"
      	          +" from ljaget a where otherno = '"+mLLHospCaseSchema.getCaseNo()+"' "
      	          +" and confdate is null and paymode='4' and othernotype in ('5','D') and (BankAccNo is not null)"
      	          +" and (bankonthewayflag = '0' or bankonthewayflag is null) and managecom like '"+mManageCom+"%'"
      	          +" and (a.cansendbank = '0' or a.cansendbank is null or a.cansendbank = '1')";        		
                tllSSRS = tllExeSQL.execSQL(Sqlll);
                System.out.println(Sqlll);
               if(tllSSRS.getMaxRow()>=1)
               {
            	   LJAGetDB mLJAGetDB = new LJAGetDB(); 
                   mLJAGetDB.setActuGetNo(tllSSRS.GetText(i, 1));
                   if(mLJAGetDB.getInfo())
                   {
	                   	LJAGetSchema mLJAGetSchema = new LJAGetSchema();  
	                   	mLJAGetSchema = mLJAGetDB.getSchema();
                    	mLJAGetSchema.setBankCode(mBankCode);            	
                    	mLJAGetSchema.setBankAccNo(mAccNo);
                    	mLJAGetSchema.setAccName(mAccName);
                    	mLJAGetSchema.setDrawer(mAccName);
                    	mLJAGetSchema.setCanSendBank("0");
                    	mLJAGetSchema.setModifyDate(mCurrentDate);
                    	mLJAGetSchema.setModifyTime(mCurrentTime);
                    	mLJAGetSchema.setShouldDate(mCurrentDate);
                    	mMMap.put(mLJAGetSchema, "DELETE&INSERT");
                   }             
               }
                 
        	}              	        		      
        }
        	LLCasePayAdvancedTraceDB mLLCasePayAdvancedTraceDB = new LLCasePayAdvancedTraceDB();
        	LLCasePayAdvancedTraceSet mLLCasePayAdvancedTraceSet = new LLCasePayAdvancedTraceSet();
        	mLLCasePayAdvancedTraceDB.setClaimNo(mClaimNo);
        	mLLCasePayAdvancedTraceDB.setBnfNo(mBnfNo);
        	mLLCasePayAdvancedTraceSet = mLLCasePayAdvancedTraceDB.query();
             if(mLLCasePayAdvancedTraceSet.size()>=1)
             {        	        
             	for(int i = 1; i<=mLLCasePayAdvancedTraceSet.size(); i++)
             	{
             		LLCasePayAdvancedTraceSchema mLLCasePayAdvancedTraceSchema = new LLCasePayAdvancedTraceSchema();
             		mLLCasePayAdvancedTraceSchema = mLLCasePayAdvancedTraceSet.get(i);
             		String Sqlll ="select a.actugetno ,a.otherno,a.sumgetmoney,a.Drawer,"
           	          +"a.DrawerID,a.BankCode,a.BankAccNo,a.AccName,bankonthewayflag,confdate,cansendbank"
           	          +" from ljaget a where actugetno = '"+mLLCasePayAdvancedTraceSchema.getActugetNo()+"' "
           	          +" and confdate is null and paymode='4' and othernotype in ('5','D') and (BankAccNo is not null)"
           	          +" and (bankonthewayflag = '0' or bankonthewayflag is null) and managecom like '"+mManageCom+"%'"
           	          +" and (a.cansendbank = '0' or a.cansendbank is null or a.cansendbank = '1')";        		
                     tllSSRS = tllExeSQL.execSQL(Sqlll);
                     System.out.println(Sqlll);
                    if(tllSSRS.getMaxRow()>=1)
                    {
                 	   LJAGetDB mLJAGetDB = new LJAGetDB(); 
                        mLJAGetDB.setActuGetNo(tllSSRS.GetText(i, 1));
                        if(mLJAGetDB.getInfo())
                        {
     	                   	LJAGetSchema mLJAGetSchema = new LJAGetSchema();  
     	                   	mLJAGetSchema = mLJAGetDB.getSchema();
                         	mLJAGetSchema.setBankCode(mBankCode);            	
                         	mLJAGetSchema.setBankAccNo(mAccNo);
                         	mLJAGetSchema.setAccName(mAccName);
                         	mLJAGetSchema.setDrawer(mAccName);
                         	mLJAGetSchema.setCanSendBank("0");
                         	mLJAGetSchema.setModifyDate(mCurrentDate);
                         	mLJAGetSchema.setModifyTime(mCurrentTime);
                         	mLJAGetSchema.setShouldDate(mCurrentDate);
                         	mMMap.put(mLJAGetSchema, "DELETE&INSERT");
                        }             
                    }
                      
             	}              	        		      
        	
        }
        if( tllSSRS.getMaxRow()<= 0)
        {
        	 buildError("dealData", "没有可以修改的给付信息");
             return false;
        }
        mResult.add(mMMap);      
        return true;
    }
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
    	 System.out.println("LLTJAccidentAccModify--createResultXML(正常返回报文)");
    	 Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "REQUEST");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);

         Element tResponseCode = new Element("RESPONSE_CODE");
         tResponseCode.setText("1");
         tHeadData.addContent(tResponseCode);

         Element tErrorMessage = new Element("ERROR_MESSAGE");
         tErrorMessage.setText(mErrors.getFirstError());
         tHeadData.addContent(tErrorMessage);

         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
    	System.out.println("LLTJAccidentAccModify--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "REQUEST");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
   
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJAccidentAccModify";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }  
    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/LLHosp03.xml"), "GBK");
            LLTJAccidentAccModify tBusinessDeal = new LLTJAccidentAccModify();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
