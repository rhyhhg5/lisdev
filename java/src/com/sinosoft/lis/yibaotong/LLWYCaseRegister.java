package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDDiseaseDB;
import com.sinosoft.lis.db.LLAccidentTypeDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LLAccidentSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLOtherFactorSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LLAccidentSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * <p>Title: 微医项目分案接口</p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLWYCaseRegister implements ServiceInterface {
	public LLWYCaseRegister(){
		
	}
	 public CErrors mErrors = new CErrors();
	 private VData mInputData = new VData();
	 public VData mResult = new VData();
	 private GlobalInput mGlobalInput = new GlobalInput();
	 private MMap mMMap = new MMap();
	 private Document mInXmlDoc;  			//传入报文
	 private String mDocType="";			//报文类型
	 private boolean mDealState= true;		//处理标记
	 private String mResponseCode = "1";	//返回类型
	 private String mRequestType="";		//请求类型
	 private String mErrorMessage="";		//错误描述
	 private String mTransactionNum="";		//交易编码
	 
	 private Element mVersionData;			//报文VERSION_DATA部分
	 private Element mPictureData;       	//报文PICTURE_DATA部分
	 private Element mUserData;				//报文USER_DATA部分
	 private Element mCustomerData;			//报文CUSTOMER_DATA部分
	 private Element mReceiptList;			//报文RECEIPTLIST部分
	 private Element mDiseaseList;			//报文DISEASELIST部分
	 private Element mAccidentList;			//报文ACCIDENTLIST部分
	 
	 private String mCaseNo="";				//案件号
	 private String mVersionCode="";		//版本编码
	 private String mPicPatch="";			//影件路径
	 private String mInsuredNo="";			//客户号
	 private String mManageCom="";			//管理机构
	 private String mHandler="";			//案件处理人
	 private String mCaseRelaNo="";			//事件关联号
	 private String mAccDate = "";			//出险日期
	 
	 private String mLimit = "";
	 private String mMakeDate =PubFun.getCurrentDate();
	 private String mMakeTime =PubFun.getCurrentTime();
	 
//	 private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();	//处理人信息
	 private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();			//被保险人信息
	 private LLCaseSchema mLLCaseSchema = new LLCaseSchema();					//案件信息
	 private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();		//申请信息
	 private LLHospCaseSchema mLLHospCaseSchema = new  LLHospCaseSchema();		//医保通案件信息
	 private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();					//账单信息
	 private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet(); 		//分案收据明细
	 private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();	//事件信息
	 private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();				//疾病信息
	 private LLAccidentSet mLLAccidentSet = new LLAccidentSet();				//意外信息
	 private LLOtherFactorSchema mLLOtherFactorSchema = new LLOtherFactorSchema(); //其他要素录入信息 
	 private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();		//事件关联表
	 
	 public Document service(Document pInXmlDoc) {
		 System.out.println("LLWYCaseRegister====微医接口开始--service");
		 
		 mInXmlDoc=pInXmlDoc;
		 
		 try{
			 //获取报文
			 if(!getInputData(mInXmlDoc)){
				 mDealState = false;
			 }else{
				 if(!checkData()){
					mDealState = false; 
				 }else{
					 if(!dealData()){
						mDealState = false; 
					 }
				 }
			 }
			 
		 }catch(Exception e){
			 System.out.println(e.getMessage());
             mDealState = false;
             mResponseCode = "E";
             buildError("service()","系统未知错误"); 
		 }finally{
			 mInXmlDoc =createXML();
		 }
		 
		return  mInXmlDoc;
	 }
	 
	 /***
	  * 解析报文主要部分
	  * @param args
	  */
	 
	 private boolean getInputData(Document aXmlDoc) throws Exception{
		 System.out.println("LLWYCaseRegister--开始解析报文--getInputData");
		 if(aXmlDoc==null){
    		 buildError("getInPutData()","未获取报文");
    		 return false;
    	 }
    	 Element tRootData=aXmlDoc.getRootElement();
    	 mDocType=tRootData.getAttributeValue("type");
    	 
    	 //获取Head部分
    	 Element tHeadData=tRootData.getChild("HEAD");
    	 mRequestType =tHeadData.getChildText("REQUEST_TYPE");
    	 mTransactionNum =tHeadData.getChildText("TRANSACTION_NUM");
    	 System.out.println("TransactionNum========"+mTransactionNum);
    	 
    	 //获取Body部分
    	 Element tBodyData=tRootData.getChild("BODY");
    	 mVersionData =tBodyData.getChild("VERSION_DATA");
    	 mPictureData =tBodyData.getChild("PICTURE_DATA");
    	 mUserData =tBodyData.getChild("USER_DATA");
    	 mCustomerData =tBodyData.getChild("CUSTOMER_DATA");
    	 mReceiptList =mCustomerData.getChild("RECEIPTLIST");
    	 mDiseaseList =mCustomerData.getChild("DISEASELIST");
    	 mAccidentList =mCustomerData.getChild("ACCIDENTLIST");
		 return true;
	 }
	 
	 /***
	  * 校验报文信息
	  * 
	  */
	 private boolean checkData() throws Exception{
		 System.out.println("LLWYCaseRegister----checkData");
    	 if(!"REQUEST".equals(mDocType)){
    		 buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
             return false;
    	 }
    	 if(!"WY01".equals(mRequestType)){
    		 buildError("checkData()","【请求类型】的值不存在或匹配错误");
    	 }
    	 if(mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
    	            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误");
            return false;
    	 }
    	 if(!mRequestType.equals(mTransactionNum.substring(0, 4))){
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
    	 }
    	 
		 return true;
	 }
	 
	 /***
	  * 处理报文
	 * @throws Exception 
	  * 
	  */
	 private boolean dealData() throws Exception{
		 System.out.println("LLWYCaseRegister--dealData");
		 //校验客户信息和版本信息
		 if(!dealInsured()){
			 return false;
		 }
		 
		//案件受理 处理案件 事件等信息， 申请材料申请原因等表 暂省略了
	      if (!caseRegister()) {
	          return false;
	        }
		 
	    //处理账单信息 类似账单录入功能 选择 医院 类型的情况
	      if (!dealFee()) {
	          return false;
	        } 
		
	    //记录案件信息
	      if(!dealHospCase()){
	    	  return false;
	      }
	       
	    //影像资料 暂不进行存储以后再议
	     if(!LLWYScan()){
	          return false;
	        }   
	      
	   //生成案件信息   就是提交数据
	      if (!dealCase()) {
	          return false;
	        }
	  
	    
		 return true;
	 }
	 
	 /***
	  * 校验用户信息和版本信息
	  * 
	  */
	 private boolean dealInsured() throws Exception{
		 System.out.println("dealInsured----校验用户信息");
	     
		  //版本类型取传递的
	       mVersionCode=mVersionData.getChildText("VERSIONCODE");
	       if(mVersionCode ==null || "".equals(mVersionCode)){
	    	   buildError("dealInsured","版本类型不能为空");
	    	   return false;
	       }
	       
		  //校验交易编码，防止重复
		 String aSQSL ="select 1 from llhospcase where apptranno='"+mTransactionNum+"'  and hospitcode='"+mRequestType+"'  " 
		 		+ "and casetype in(select code from ldcode where codetype='casetype' and comcode='"+mVersionCode+"') with ur";
		 ExeSQL tExeSQL =new ExeSQL();
		 String aCount =tExeSQL.getOneValue(aSQSL);
		 if("1".equals(aCount)){
			 buildError("dealInsured","交互编码："+mTransactionNum+"重复使用");
	    	  return false;
		 }
	
		 //用客户号校验该客户是否有正在处理的保全项目,返回的是工单号，而且校验的是全部保单，考虑以后是否按照保单校验
		 mInsuredNo =mCustomerData.getChildText("INSUREDNO");
         if (!"".equals(LLCaseCommon.checkPerson(mInsuredNo))) {
             buildError("dealInsured", "该客户正在进行保全处理，不能进行理赔");
             return false;
         }
		 return true;
	 }
	 
	 /**
	  * 解析受理信息
	 * @throws Exception 
	  * 
	  */
	 private boolean caseRegister() throws Exception{
		 System.out.println("LLWYCaseRegister--caseRegister");
		 
	        mAccDate = mCustomerData.getChildText("ACCDATE");
	        
	        if (mAccDate == null || "".equals(mAccDate)) {
	            buildError("caseRegister", "【发生日期】的值不能为空");
	            return false;
	        }
	        if (!PubFun.checkDateForm(mAccDate)) {
	            buildError("caseRegister", "【发生日期】格式错误(正确格式yyyy-MM-dd)");
	            return false;
	        }
	      	
	        
	        //处理人取WY传过来的
	        mHandler = mUserData.getChildText("USERCODE");
	        
	        if (mHandler == null || "".equals(mHandler)) {
	            buildError("caseRegister", "【理赔员】的值不能为空");
	            return false;
	        }
	    
	        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
	        tLLClaimUserDB.setUserCode(mHandler);
	        tLLClaimUserDB.setClaimDeal("1");//理赔员权限
	        tLLClaimUserDB.setHandleFlag("1");//案件分配权限
	        tLLClaimUserDB.setStateFlag("1");

	        System.out.println("--------------------------------"+tLLClaimUserDB.getComCode()+"-------------------------------");
	        LLClaimUserSet tLLClaimUserSet = tLLClaimUserDB.query();

	        if (tLLClaimUserSet.size() <= 0) {
	            buildError("caseRegister", "案件处理人查询失败或没有权限");
	            return false;
	        }
	        //案件机构取理赔用户机构
	        mManageCom = tLLClaimUserSet.get(1).getComCode();
	        
	        LLCaseDB tLLCaseDB = new LLCaseDB();
	        String tCaseSQl = "select * from llcase where rgtstate not in ('11','12','14') and customerno='" +
	                          mInsuredNo + "'";
	        LLCaseSet tLLCaseSet = tLLCaseDB.executeQuery(tCaseSQl);
	        if (tLLCaseSet.size() > 0) {
	            buildError("dealCont", "客户" + mInsuredNo + "有未处理完的理赔案件，不能进行理赔。");
	            return false;
	        }

	        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
	        tLCInsuredDB.setInsuredNo(mInsuredNo);
	        LCInsuredSet tLCinsuredSet = tLCInsuredDB.query();
	        if (tLCinsuredSet.size() < 1) {
	            buildError("caseRegister", "被保险人查询失败");
	            return false;
	        }
	        //只需要取一个就可以，只要基本信息
	        mLCInsuredSchema = tLCinsuredSet.get(1);

	        mLimit = PubFun.getNoLimit(mManageCom);
	        mCaseNo = PubFun1.CreateMaxNo("CaseNo", mLimit);

	        if (!getCaseInfo()) {
	            return false;
	        }

	        mCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", mLimit);

	        if (!getSubInfo()) {
	            return false;
	        }
		 
		 return true;
	 }
	 
	 /***
	  * 生成案件信息
	 * @throws Exception 
	  * 
	  */
	 private boolean getCaseInfo() throws Exception{
		 System.out.println("LLWYCaseRegister--getCaseInfo");
		 
		 	mLLCaseSchema.setCaseNo(mCaseNo);
	        mLLCaseSchema.setRgtNo(mCaseNo);
	        //申请类案件
	        mLLCaseSchema.setRgtType("1");
	        //案件处理到理算确认
	        mLLCaseSchema.setRgtState("01");
	        mLLCaseSchema.setCustomerNo(mInsuredNo);
	        mLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
	        //账单状态录入
	        mLLCaseSchema.setReceiptFlag("1");
	        //未调查
	        mLLCaseSchema.setSurveyFlag("0");
	        mLLCaseSchema.setRgtDate(mMakeDate);

	        mLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
	        mLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());

	        int tAge = LLCaseCommon.calAge(mLCInsuredSchema.getBirthday());
	        mLLCaseSchema.setCustomerAge(tAge);

	        mLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
	        mLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
	        mLLCaseSchema.setHandler(mHandler);
	        mLLCaseSchema.setDealer(mHandler);
	        //银行信息
	        String mCaseGetMode = mCustomerData.getChildText("CASEGETMODE");
	        String mBankCode = mCustomerData.getChildText("BANKNO");
	        String mAccNo = mCustomerData.getChildText("ACCNO");
	        String mAccName = mCustomerData.getChildText("ACCNAME");
	        
	        if(!mCaseGetMode.equals("1") &&!mCaseGetMode.equals("2") )
	    	{
	            if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
	                buildError("dealData", "【银行编码】的值不能为空");
	                return false;
	            }
	            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
	                buildError("dealData", "【银行名称】的值不能为空");
	                return false;
	            }
	            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
	                buildError("dealData", "【账户名】的值不能为空");
	                return false;
	            }
	            if (mAccNo == null || "".equals(mAccNo)|| mAccNo.equals("null")) {
	                buildError("dealData", "【账号】的值不能为空");
	                return false;
	            }
	            Pattern tPattern = Pattern.compile("(\\d)+");
	    		Matcher isNum = tPattern.matcher(mAccNo);

	    		if (!isNum.matches()) {
	    			buildError("dealData", "【账号】只能是数字组成");
	    			return false;
	    		}
	    	}
	        mLLCaseSchema.setCaseGetMode(mCaseGetMode); 
	        mLLCaseSchema.setBankCode(mBankCode);
	        mLLCaseSchema.setBankAccNo(mAccNo);
	        mLLCaseSchema.setAccName(mAccName);
	        
	        //事件类型
	        String mAccidentType = mCustomerData.getChildText("ACCDESCTYPE");
	        mLLCaseSchema.setAccidentType(mAccidentType);
	        
	        //案件机构补齐8位
	        if (mManageCom.length() == 4) {
	            mLLCaseSchema.setMngCom(mManageCom + "0000");
	        }else if(mManageCom.length() == 6){
	        	mLLCaseSchema.setMngCom(mManageCom + "00");
	        } 
	        else if(mManageCom.length() == 2){//两位机构估计有问题一般不会有的，除非业务设置pad理赔用户机构为86 我就不信你会这样设...
	        	mLLCaseSchema.setMngCom(mManageCom + "000000");
	        } 
	        else {
	            mLLCaseSchema.setMngCom(mManageCom);
	        }
	        //申请人电话
	        String aTelPhone =mCustomerData.getChildText("TELPHONE");
	        mLLCaseSchema.setMobilePhone(aTelPhone);
	        
	        mLLCaseSchema.setOperator(mHandler);
	        mLLCaseSchema.setMakeDate(mMakeDate);
	        mLLCaseSchema.setMakeTime(mMakeTime);
	        mLLCaseSchema.setModifyDate(mMakeDate);
	        mLLCaseSchema.setModifyTime(mMakeTime);

	        //新增一个WY理赔标识--14  SH理赔标识暂定--15
	        if("WY01".equals(mVersionCode)){
	        	mLLCaseSchema.setCaseProp("14");
	        }else{
	        	mLLCaseSchema.setCaseProp("15");
	        }
	        mLLCaseSchema.setRigister(mHandler);
	        mLLCaseSchema.setClaimer(mHandler);
	        mLLCaseSchema.setOtherIDType(mLCInsuredSchema.getOthIDType());
	        mLLCaseSchema.setOtherIDNo(mLCInsuredSchema.getOthIDNo());

	        mLLCaseSchema.setAccidentDate(mAccDate);

	        //申请表信息
	        mLLRegisterSchema.setRgtNo(mCaseNo);
	        mLLRegisterSchema.setRgtState("13");
	        //客户号
	        mLLRegisterSchema.setRgtObj("1");
	        mLLRegisterSchema.setRgtObjNo(mInsuredNo);
	        //受理方式微医项目暂定为 --11 微医平台，其他项目暂定为 9--其他   #3425 
	        if("WY01".equals(mVersionCode)){
	        	mLLRegisterSchema.setRgtType("11");
	        }else{
	        	mLLRegisterSchema.setRgtType("9");
	        }
	        //个案受理
	        mLLRegisterSchema.setRgtClass("0");
	        mLLRegisterSchema.setRgtantName(mLLCaseSchema.getCustomerName());
	        //银行信息
	    	mLLRegisterSchema.setBankCode(mBankCode);
	    	mLLRegisterSchema.setAccName(mAccName); 
	      	mLLRegisterSchema.setBankAccNo(mAccNo); 
	      	mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
	        //默认本人申请
	        mLLRegisterSchema.setRelation("00");
	        //申请日期
	        mLLRegisterSchema.setAppDate(mMakeDate);
	        mLLRegisterSchema.setCustomerNo(mInsuredNo);
	        mLLRegisterSchema.setRgtDate(mMakeDate);
	        mLLRegisterSchema.setHandler(mHandler);
	        mLLRegisterSchema.setMngCom(mLLCaseSchema.getMngCom());
	        mLLRegisterSchema.setIDType(mLLCaseSchema.getIDType());
	        mLLRegisterSchema.setIDNo(mLLCaseSchema.getIDNo());
	        mLLRegisterSchema.setRgtantMobile(aTelPhone);
	        mLLRegisterSchema.setOperator(mHandler);
	        mLLRegisterSchema.setMakeDate(mMakeDate);
	        mLLRegisterSchema.setMakeTime(mMakeTime);
	        mLLRegisterSchema.setModifyDate(mMakeDate);
	        mLLRegisterSchema.setModifyTime(mMakeTime);
	        
		 return true;
	 }
	 
	 /***
	  * 
	  * 生成事件信息、疾病信息、意外信息、其他信息
	  * 
	  */
	 private boolean getSubInfo() throws Exception{
		 System.out.println("LLWYCaseRegister------getSubInfo()");
		 
		 	List tReceiptList = new ArrayList();
	        tReceiptList = (List) mReceiptList.getChildren();
	        //只取第一个账单医院信息如有不同医院就需要调整报文格式
	        Element mAccReceiptData = (Element) tReceiptList.get(0);
	        String mHospitCode = mAccReceiptData.getChildText("HOSIPITALNO");
	        String mHospitName = mAccReceiptData.getChildText("HOSIPITALNAME");
	        
	        List tDiseaseList = new ArrayList();
	        tDiseaseList = (List) mDiseaseList.getChildren();
	      //疾病信息
	        for (int i = 0; i < tDiseaseList.size(); i++) {
	        Element	tDiseaseData = (Element) tDiseaseList.get(i);
	        
	        String tDiseaseCode = tDiseaseData.getChildText("DISEASECODE");
	        
	        if ("".equals(tDiseaseCode) || "null".equals(tDiseaseCode)) {
	            buildError("getSubInfo", "【疾病编码】的值不能为空");
	            return false;
	        }
	        LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
	        tLDDiseaseDB.setICDCode(tDiseaseCode);
	        if (!tLDDiseaseDB.getInfo()) {
	            buildError("getSubInfo", "【疾病编码】不存在");
	            return false;
	        }
	        String tDiseaseName = tDiseaseData.getChildText("DISEASENAME");//没用
	        
	        LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
	        String tCureNo = PubFun1.CreateMaxNo("CURENO", mLimit);
	        mLLCaseCureSchema.setSerialNo(tCureNo);
	        mLLCaseCureSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
	        mLLCaseCureSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	        mLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
	        mLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());
	        mLLCaseCureSchema.setHospitalCode(mHospitCode);
	        mLLCaseCureSchema.setHospitalName(mHospitName);
	        mLLCaseCureSchema.setCaseNo(mCaseNo);
	        mLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo);
	        mLLCaseCureSchema.setSeriousFlag("0");
	        mLLCaseCureSchema.setOperator(mHandler);
	        mLLCaseCureSchema.setMngCom(mLLCaseSchema.getMngCom());
	        mLLCaseCureSchema.setMakeDate(mMakeDate);
	        mLLCaseCureSchema.setMakeTime(mMakeTime);
	        mLLCaseCureSchema.setModifyDate(mMakeDate);
	        mLLCaseCureSchema.setModifyTime(mMakeTime);
	        
	        mLLCaseCureSet.add(mLLCaseCureSchema);
	        }
	        
	        //事件信息
	        String tInHospitalDate = mAccReceiptData.getChildText("HOSPSTARTDATE");
	        String tOutHospitalDate = mAccReceiptData.getChildText("HOSPENDDATE");
	        if ("".equals(tInHospitalDate) || "null".equals(tInHospitalDate)) {
	            buildError("getSubInfo", "【入院日期】的值不能为空");
	            return false;
	        }
	        if (!PubFun.checkDateForm(tInHospitalDate)) {
	            buildError("getSubInfo", "【入院日期】格式错误");
	            return false;
	        }

	        if ("".equals(tOutHospitalDate) || "null".equals(tOutHospitalDate)) {
	            buildError("getSubInfo", "【出院日期】的值不能为空");
	            return false;
	        }
	        if (!PubFun.checkDateForm(tOutHospitalDate)) {
	            buildError("getSubInfo", "【出院日期】格式错误");
	            return false;
	        }

	        String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", mLimit);
	        mLLSubReportSchema.setSubRptNo(tSubRptNo);
	        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

	        //事件类型只能是疾病、意外或者空
	        String tAccDescType = mCustomerData.getChildText("ACCDESCTYPE");
	        if ("1".equals(tAccDescType) || "2".equals(tAccDescType)) {
	            mLLSubReportSchema.setAccidentType(tAccDescType);
	        }

	        mLLSubReportSchema.setAccDate(mAccDate);
	        mLLSubReportSchema.setInHospitalDate(tInHospitalDate);
	        mLLSubReportSchema.setOutHospitalDate(tOutHospitalDate);
//	        mLLSubReportSchema.setAccDesc(mLLCaseCureSchema.getDiseaseName());
//	        mLLSubReportSchema.setAccPlace(mLLCaseCureSchema.getHospitalName());
	        mLLSubReportSchema.setOperator(mHandler);
	        mLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());
	        mLLSubReportSchema.setMakeDate(mMakeDate);
	        mLLSubReportSchema.setMakeTime(mMakeTime);
	        mLLSubReportSchema.setModifyDate(mMakeDate);
	        mLLSubReportSchema.setModifyTime(mMakeTime);
	        
	        // 3544 发生地点省、市、县 **start**
	        String tAccProvinceCode = mCustomerData.getChildText("ACCPROVINCECODE"); // 发生地点省
	        String tAccCityCode = mCustomerData.getChildText("ACCCITYCODE") ; // 发生地点市
	        String tAccCountyCode = mCustomerData.getChildText("ACCCOUNTYCODE") ; //发生地点县
	        String tResult = LLCaseCommon.checkAccPlace(tAccProvinceCode, tAccCityCode, tAccCountyCode);
	        if(!"".equals(tResult)){
	        	buildError("getSubInfo",tResult);
	            return false;
	        }else{
	        	mLLSubReportSchema.setAccProvinceCode(tAccProvinceCode);
	        	mLLSubReportSchema.setAccCityCode(tAccCityCode);
	        	mLLSubReportSchema.setAccCountyCode(tAccCountyCode);
	        }
	        //意外信息

	        if ("2".equals(tAccDescType)) {
	        	
	            List tAccidentList = new ArrayList();
	            tAccidentList = (List) mAccidentList.getChildren();
	            
	            if(tAccidentList.size()<1){
	            	buildError("getSubInfo", "意外事件，【意外信息】不能为空");
	                return false;
	            }

	            for (int i = 0; i < tAccidentList.size(); i++) {
	            Element	tAccidentData = (Element) tAccidentList.get(i);
	            
	            String tAccidentCode = tAccidentData.getChildText("ACCIDENTCODE");
	            
	            if ("".equals(tAccidentCode) || "null".equals(tAccidentCode)) {
	                buildError("getSubInfo", "【意外代码】的值不能为空");
	                return false;
	            }
	            LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
	            tLLAccidentTypeDB.setAccidentNo(tAccidentCode);
	            if (!tLLAccidentTypeDB.getInfo()) {
	                buildError("getSubInfo", "【意外代码】不存在");
	                return false;
	            }
	            String tAccidentName = tAccidentData.getChildText("ACCIDENTNAME");
	            
	         	LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
	         	String AccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mLimit);
	         	
	         	tLLAccidentSchema.setAccidentNo(AccidentNo);
	    		tLLAccidentSchema.setCaseNo(mCaseNo);
	    		tLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
	    		tLLAccidentSchema.setCode(tAccidentCode);
	    		tLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());

	    		mLLAccidentSet.add(tLLAccidentSchema);
	            }
	        	
	        }

	        //事件关联表
	        mLLCaseRelaSchema.setCaseNo(mCaseNo);
	        mLLCaseRelaSchema.setSubRptNo(tSubRptNo);
	        mLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
	        
	        //其他信息
	        String tSocailMedical = mCustomerData.getChildText("SOCAILMEDICAL");
	        if("1".equals(tSocailMedical)||"0".equals(tSocailMedical)){
	   		 
	        	mLLOtherFactorSchema.setFactorType("9");//9-补充编码
	        	mLLOtherFactorSchema.setFactorCode("9921");//固定编码
	        	mLLOtherFactorSchema.setCaseNo(mCaseNo);
	        	mLLOtherFactorSchema.setCaseRelaNo(mCaseRelaNo);
	        	mLLOtherFactorSchema.setSubRptNo(tSubRptNo);
	        	mLLOtherFactorSchema.setValue(tSocailMedical);
	        	mLLOtherFactorSchema.setFactorName("享有社会医疗保险和公费医疗(0-是，1-否)");
	        }

		 return true;
	 }
	 
	 /**
	  * 解析账单信息
	  * 
	  */
	 private boolean dealFee() throws Exception{
		System.out.println("LLWYCaseRegister----dealFee()");

	   //账单信息
       List tReceiptList = new ArrayList();
       tReceiptList = (List) mReceiptList.getChildren();
       //校验医院编码+账单号码的唯一性
       for(int k = 0; k < tReceiptList.size(); k++) {
    	   Element aReceiptData=  (Element) tReceiptList.get(k);
    	   String aReceiptNo= aReceiptData.getChildText("MAINFEENO");
           String aHospitalNo = aReceiptData.getChildText("HOSIPITALNO");
           for (int j =tReceiptList.size()-1; j>k; j--) {
        	   Element cReceiptData=  (Element) tReceiptList.get(j);
        	   String cReceiptNo= cReceiptData.getChildText("MAINFEENO");
               String cHospitalNo = cReceiptData.getChildText("HOSIPITALNO");
               String cHospitalName= cReceiptData.getChildText("HOSIPITALNAME");
               if (aReceiptNo.equals(cReceiptNo)
                   && aHospitalNo.equals(cHospitalNo)) {
            	   buildError("dealFee", cHospitalName+"已经存在"+cReceiptNo+"的账单号码，请重新确认录入");
    	           return false;
               }
           }
       }
   
       System.out.println("List长度:"+tReceiptList.size());

       for(int i=0; i<tReceiptList.size(); i++){
	       Element mReceiptData = (Element) tReceiptList.get(i);
	       String mMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
	       String tFeeAtti = mReceiptData.getChildText("FEEATTI");
	       String tFeeType = mReceiptData.getChildText("FEETYPE");
	       String tReceiptNo = mReceiptData.getChildText("MAINFEENO");
	       String tHospitalNo = mReceiptData.getChildText("HOSIPITALNO");
	       String tHospitalName = mReceiptData.getChildText("HOSIPITALNAME");	       
	       String tHospStartDate = mReceiptData.getChildText("HOSPSTARTDATE");
	       String tHospEndDate = mReceiptData.getChildText("HOSPENDDATE");
	       String tMedicareType = mReceiptData.getChildText("MEDICARETYPE");  // 3544 医保类型
	       //账单表存的汇总值 存账单表时使用
	       double mSumFee = 0.0;
	       double mPlanFee = 0.0;
	       double mSelfAmnt = 0.0;
	       double mRefuseAmnt = 0.0;
	       
	       if (!"1".equals(tFeeType) && !"2".equals(tFeeType) && !"3".equals(tFeeType)) {
	           buildError("dealFee", "【账单类型】的值不存在");
	           return false;
	       }
	       //校验医院编码+账单号码的唯一性
	       String aSQL ="select 1 from llfeemain where ReceiptNo='"+tReceiptNo+"' and  HospitalCode='"+tHospitalNo+"' with ur ";
	       ExeSQL tExeSQL = new ExeSQL();
	       String aCount =tExeSQL.getOneValue(aSQL);
	       if("1".equals(aCount)){
	    	   buildError("dealFee", tHospitalName+"已经存在"+tReceiptNo+"的账单号码，请重新确认录入");
	           return false;
	       }
	       //费用项目信息
	       Element mFeeitemList = mReceiptData.getChild("FEEITEMLIST");
	       
	       List tFeeitemList = new ArrayList();
	       tFeeitemList = (List) mFeeitemList.getChildren();
	       for(int y=0; y<tFeeitemList.size(); y++){
	       	
	       Element	mFeeitemData = (Element) tFeeitemList.get(y);
	
	       String tFeeitemCode = mFeeitemData.getChildText("FEEITEMCODE");
	       String tSumFee = mFeeitemData.getChildText("SUMFEE");
	       String tPlanFee = mFeeitemData.getChildText("PLANFEE");
	       String tSelfAmnt = mFeeitemData.getChildText("SELFAMNT");
	       String tRefuseAmnt = mFeeitemData.getChildText("REFUSEAMNT");
	       
	       //账单金额
	       if (tSumFee == null || "".equals(tSumFee)) {
	           buildError("dealFee", "【账单金额】的值不能为空");
	           return false;
	       }
	       double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee), 2);
	
	       if (tSumFeeValue <= 0) {
	           buildError("dealFee", "【账单金额】的值必须大于0");
	           return false;
	       }
	       
	       //找到费用项目对应名称  
	       String tSQL = "select codename   from ldcode where codetype ='llfeeitemtype' and code='"+tFeeitemCode+"' ";
	       
	       String tFeeitemName = tExeSQL.getOneValue(tSQL);
	       if("".equals(tFeeitemName)||null==tFeeitemName||"null".equals(tFeeitemName)){
	       	 buildError("dealFee", "【费用项目代码】的值不存在");
	            return false;
	       }
	       //费用明细信息
	       LLCaseReceiptSchema mLLCaseReceiptSchema = new LLCaseReceiptSchema();
	       String tLimit1 = PubFun.getNoLimit("86");
	       mLLCaseReceiptSchema.setFeeDetailNo(
	               PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
	       mLLCaseReceiptSchema.setMainFeeNo(mMainFeeNo);
	       mLLCaseReceiptSchema.setRgtNo(mCaseNo);
	       mLLCaseReceiptSchema.setCaseNo(mCaseNo);
	       mLLCaseReceiptSchema.setFeeItemCode(tFeeitemCode);
	       mLLCaseReceiptSchema.setFeeItemName(tFeeitemName);
	       mLLCaseReceiptSchema.setFee(tSumFee);
	       mLLCaseReceiptSchema.setSelfAmnt(tSelfAmnt);
	       mLLCaseReceiptSchema.setPreAmnt(tPlanFee);
	       mLLCaseReceiptSchema.setRefuseAmnt(tRefuseAmnt);
	       mLLCaseReceiptSchema.setAvaliFlag("0");
	       mLLCaseReceiptSchema.setMngCom(mLLCaseSchema.getMngCom());
	       mLLCaseReceiptSchema.setOperator(mHandler);
	       mLLCaseReceiptSchema.setMakeDate(mMakeDate);
	       mLLCaseReceiptSchema.setMakeTime(mMakeTime);
	       mLLCaseReceiptSchema.setModifyDate(mMakeDate);
	       mLLCaseReceiptSchema.setModifyTime(mMakeTime);
	       
	       mSumFee += mLLCaseReceiptSchema.getFee();
	       mPlanFee += mLLCaseReceiptSchema.getPreAmnt();
	       mSelfAmnt += mLLCaseReceiptSchema.getSelfAmnt();
	       mRefuseAmnt += mLLCaseReceiptSchema.getRefuseAmnt();
	       mLLCaseReceiptSet.add(mLLCaseReceiptSchema);
	       }
	
	       //账单信息
	       LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();	
	       // 3544 医保类型
	       String tMedResult = LLCaseCommon.checkMedicareType(tMedicareType);
	       if(!"".equals(tMedResult)){
	    	   buildError("dealFee", tMedResult);
	           return false;
	       }else{
	    	   mLLFeeMainSchema.setMedicareType(tMedicareType);
	       }
	       mLLFeeMainSchema.setMainFeeNo(mMainFeeNo);
	       mLLFeeMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
	       mLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
	       mLLFeeMainSchema.setRgtNo(mLLCaseSchema.getRgtNo());
	       mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	       mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
	       mLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
	       mLLFeeMainSchema.setHospitalCode(tHospitalNo);
	       mLLFeeMainSchema.setHospitalName(tHospitalName);
	       mLLFeeMainSchema.setReceiptNo(tReceiptNo);
	
	       mLLFeeMainSchema.setFeeType(tFeeType);
	       mLLFeeMainSchema.setFeeAtti(tFeeAtti);
	       mLLFeeMainSchema.setFeeDate(tHospEndDate);
	       mLLFeeMainSchema.setHospStartDate(tHospStartDate);
	       mLLFeeMainSchema.setHospEndDate(tHospEndDate);
	       //计算实际住院天数
	       int tRealHospDay = PubFun.calInterval(mLLFeeMainSchema.getHospStartDate(),
	                                             mLLFeeMainSchema.getHospEndDate(),
	                                             "D") + 1;
	       mLLFeeMainSchema.setRealHospDate(tRealHospDay);
	       mLLFeeMainSchema.setAge((int) mLLCaseSchema.getCustomerAge());
	       mLLFeeMainSchema.setSecurityNo(mLCInsuredSchema.getOthIDNo());
	       mLLFeeMainSchema.setInsuredStat(mLCInsuredSchema.getInsuredStat());
	
	       mLLFeeMainSchema.setSumFee(mSumFee);
	       mLLFeeMainSchema.setPreAmnt(mPlanFee);
	       mLLFeeMainSchema.setSelfAmnt(mSelfAmnt);
	       mLLFeeMainSchema.setRefuseAmnt(mRefuseAmnt);
	
	       mLLFeeMainSchema.setOperator(mHandler);
	       mLLFeeMainSchema.setMngCom(mLLCaseSchema.getMngCom());
	       mLLFeeMainSchema.setMakeDate(mMakeDate);
	       mLLFeeMainSchema.setMakeTime(mMakeTime);
	       mLLFeeMainSchema.setModifyDate(mMakeDate);
	       mLLFeeMainSchema.setModifyTime(mMakeTime);
	       
	       mLLFeeMainSet.add(mLLFeeMainSchema);
       }

		 return true;
	 }
	 
	 /**
	  * 记录接口案件信息
	  * 
	  */
	 private boolean dealHospCase() throws Exception{
		 System.out.println("LLWYCaseRegister---dealHospCase()");
		 String tSQL="select code from ldcode where codetype='casetype' and comcode='"+mVersionCode+"'with ur ";
		 ExeSQL tExe = new ExeSQL();
		 String tCasetype= tExe.getOneValue(tSQL);
		 
		 mLLHospCaseSchema.setCaseNo(mCaseNo);
		 mLLHospCaseSchema.setHospitCode(mRequestType);
		 mLLHospCaseSchema.setHCNo(mVersionCode);		//版本信息
		 mLLHospCaseSchema.setHandler(mLLCaseSchema.getHandler());
		 mLLHospCaseSchema.setAppTranNo(mTransactionNum);
		 mLLHospCaseSchema.setAppDate(mMakeDate);
		 mLLHospCaseSchema.setAppTime(mMakeTime);
		 mLLHospCaseSchema.setCaseType(tCasetype);    //案件类型 
		 mLLHospCaseSchema.setConfirmState("1");
    	 mLLHospCaseSchema.setDealType("2");
    	 mLLHospCaseSchema.setBnfNo(mLLCaseSchema.getCustomerNo());
    	 mLLHospCaseSchema.setClaimNo(mCaseNo);  // JH03接口增加诊疗明细时使用
    	 mLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
    	 mLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
		 
		 return true;
	 }
	 
	 
	 /***
	  * 生成案件信息
	  * 
	  */
	 private boolean dealCase() throws Exception{
		 System.out.println("LLYWCaseRegister----dealCase()");
		 
		 mMMap.put(mLLRegisterSchema, "DELETE&INSERT");
		 mMMap.put(mLLCaseSchema, "DELETE&INSERT");
		 mMMap.put(mLLSubReportSchema, "DELETE&INSERT");
		 mMMap.put(mLLCaseRelaSchema, "DELETE&INSERT");
		 mMMap.put(mLLFeeMainSet, "DELETE&INSERT");
		 mMMap.put(mLLCaseCureSet, "DELETE&INSERT");
		 mMMap.put(mLLHospCaseSchema, "DELETE&INSERT");
	     if (mLLOtherFactorSchema.getCaseNo() != null &&
	            !"".equals(mLLOtherFactorSchema.getCaseNo()) &&
	            !"null".equals(mLLOtherFactorSchema.getCaseNo())) {
	    	 mMMap.put(mLLOtherFactorSchema, "DELETE&INSERT");
	        }
	      
		 if (mLLAccidentSet.size()>0) {
	            mMMap.put(mLLAccidentSet, "DELETE&INSERT");
	        }
		  if (mLLCaseReceiptSet.size() > 0) {
	            mMMap.put(mLLCaseReceiptSet, "DELETE&INSERT");
	        }
		 
		  try {
	            this.mInputData.clear();
	            this.mInputData.add(mMMap);
//	            mResult.clear();
//	            mResult.add(this.mLLCaseSchema);
	        } catch (Exception ex) {
	            // @@错误处理
	            buildError("dealCase", "准备提交数据失败");
	            return false;
	        }

	        PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(mInputData, "")) {
	            buildError("dealCase", "数据提交失败");
	            return false;
	        }

		 return true;
	 }
	 
	 /***
	  * 存储影像件信息
	  * 
	  */
	 private boolean LLWYScan() throws  Exception{
		 System.out.println("LLWYCaseRegister---LLWYScan()");
		 
		 mPicPatch =mPictureData.getChildText("PICPATH"); //无用 暂取 不进行存储，方便以后。
		 //版本号为SH01时，进行影像件的存储
		 if("SH01".equals(mVersionCode)){
			 mGlobalInput.ManageCom= mLLCaseSchema.getMngCom();
			 mGlobalInput.Operator = mLLCaseSchema.getHandler();
			 
			 TransferData transferData = new TransferData();
		    	transferData.setNameAndValue("PicPath", mPicPatch);
		    	VData tVData = new VData();
				  tVData.add(mLLCaseSchema);
				  tVData.add(mGlobalInput);	
				  tVData.add(transferData);	
				  LLSHGetScanImageBL tLLSHGetScanImageBL = new LLSHGetScanImageBL();
				  if(!tLLSHGetScanImageBL.submitData(tVData,"")) {
					  CErrors tError=tLLSHGetScanImageBL.mErrors;
					  String Content = "<br>案件"+mCaseNo+",操作失败 "+ tError.getFirstError();
					  buildError("LLSHScan()", "扫描件上传报错："+Content);
			          mDealState = false; 
			          return false;
				  }
			 
		 }else{

		 }
		 return true;
	 }
	 
	 /***
	  * 生成返回报文信息
	 * @throws Exception 
	  * 
	  */
	 private Document createXML() {
		 // 处理失败报文
    	 if(!mDealState || "".equals(mCaseNo)){
    		  return createFalseXML();
    	 }else{
    		 try{
    			return createResultXML(mCaseNo) ;
    		 }catch(Exception e){
    			 mResponseCode = "0";
         		buildError("createXML()", "生成返回报文错误!");
         		return createFalseXML();
    		 }
    	 }
		 
	 }
	 
	 /***
	  * 生成错误信息报文
	  * 
	  */
	 private Document createFalseXML() {
		System.out.println("LLWYCaseRegister---createFalseXML()");
		
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");
		//Head部分信息
		Element tHead = new Element("HEAD");
		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		Element tTransactionNum =new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		
		tHead.addContent(tRequestType);
		tHead.addContent(tTransactionNum);
		
		//Body部分信息
		Element tBody = new Element("BODY");
		
		if(!"E".equals(mResponseCode)){
			mResponseCode="0";
		}
		mErrorMessage=mErrors.getFirstError();
		Element tResonseCode= new Element("RESPONSE_CODE");
		tResonseCode.setText(mResponseCode);
		tBody.addContent(tResonseCode);
		Element tErrorMessage = new Element("ERROR_MESSAGE");
		tErrorMessage.setText(mErrorMessage);
		tBody.addContent(tErrorMessage);
		
		tRootData.addContent(tHead);
		tRootData.addContent(tBody);
		
		Document tDocument = new Document(tRootData);
		
		return tDocument; 
	 }
	 
	 /***
	  * 生成正确的返回报文
	  * 
	  */
	 private Document createResultXML(String aCaseNo) throws Exception{
		 System.out.println("LLWYCaseRegister---createResultXML()");
		 
		 Element tRootData = new Element("PACKET");
		 tRootData.addAttribute("type", "RESPONSE");
		 tRootData.addAttribute("version", "1.0");
		 
		 //Head部分信息
		 Element tHead = new Element("HEAD");
		 Element tRequestType = new Element("REQUEST_TYPE");
		 tRequestType.setText(mRequestType);
		 Element tTransactionNum =new Element("TRANSACTION_NUM");
		 tTransactionNum.setText(mTransactionNum);
			
	   	 tHead.addContent(tRequestType);
		 tHead.addContent(tTransactionNum);
		 
		 //Body部分信息
		 Element tBody =new Element("BODY");
		 Element tBaseData = new Element("BASE_DATA");
		 
		 Element tCaseNo = new Element("CASENO");
		 tCaseNo.setText(aCaseNo);
		 tBaseData.addContent(tCaseNo);
		 
		 String tSQL = "select rgtdate,rgtstate from llcase where caseno='"+aCaseNo+"' with ur";
		 ExeSQL tExeSQL = new ExeSQL();
	     SSRS tSSRS = tExeSQL.execSQL(tSQL);
	        
		 Element tRgtData = new Element("RGTDATE");
		 tRgtData.setText(tSSRS.GetText(1, 1));
	     tBaseData.addContent(tRgtData);
		 
	     Element tGiveType = new Element("GIVETYPE");
	     tGiveType.setText("");
	     tBaseData.addContent(tGiveType);
	     
	     Element tRealPay = new Element("REALPAY");
	     tRealPay.setText("");
	     tBaseData.addContent(tRealPay);
	     
	     Element tRgtState = new Element("RGTSTATE");
	     tRgtState.setText(tSSRS.GetText(1, 2));
	     tBaseData.addContent(tRgtState);
	     
	     tBody.addContent(tBaseData);
	     
	     tRootData.addContent(tHead);
	     tRootData.addContent(tBody);
	     
	     Document tDocument  = new Document(tRootData);
		 return tDocument;
	 }
	 
	 /***
	  * 追加错误信息
	  * @param args
	  */
	 private void buildError(String szFunc, String szErrMsg){
		 CError cError = new CError();
         cError.moduleName = "LLWYCaseRegister";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         System.out.println(szFunc + "--" + szErrMsg);
         this.mErrors.addOneError(cError);
		 
	 }

	public static void main(String[] args) {
		Document tInXmlDoc;
		
		try{
			 tInXmlDoc = JdomUtil.build(new FileInputStream("E:\\理赔报文\\WY01\\WY01.xml"), "GBK");
			 LLWYCaseRegister tLLWYCaseRegister = new LLWYCaseRegister();
			 Document outXmlDoc =tLLWYCaseRegister.service(tInXmlDoc);
			 System.out.println("打印传入报文============");
	         JdomUtil.print(tInXmlDoc.getRootElement());
	         System.out.println("打印传出报文============");
			 JdomUtil.print(outXmlDoc);
			 
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}
