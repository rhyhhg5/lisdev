package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 
 * @author XHW
 * 
 * 2018-03-29
 */
public class LLTYIDGetDutyInfo implements ServiceInterface{
	
	public LLTYIDGetDutyInfo() {
		
	}

	//
	public CErrors mErrors = new CErrors();
	//
	public VData mResult = new VData();
	//传入报文
	private Document mInXmlDoc;
	//报文类型
	private String mDocType = "";
	//处理标志
	private boolean mDealState = true;
	//返回类型代码
	private String mResponseCode = "1";
	//请求类型
	private String mRequestType = "";
	//错误描述
	private String mErrorMessage = "";
	//交互编码
	private String mTransactionNum = "";
	//机构编码
	private String mManageCom="";
	public SSRS mSSRS = new SSRS();
	//查询的证件号
	private String mIDNO = "";
	//报文body部分
	private Element tBODY = new Element("BODY");
	//保单险种责任信息集合
	private Element tPOLCIYLIST = new Element("POLCIYLIST");
	//保单险种责任信息（多条）
	private Element tPOLICY_DATA = new Element("POLICY_DATA");
	
	/**
	 * @see
	 * com.sinosoft.lis.yibaotong.ServiceInterface#service(org.jdom.Document)
	 */
	public Document service(Document pInXmlDoc) {
		System.out.println("LLTYIDGetDutyInfo------------------service");
		mInXmlDoc = pInXmlDoc;
		try {
			if(!getInputData(mInXmlDoc)) {
				mDealState = false;
			}else{
				if(!checkData()) {
					mDealState = false;
				}else {
					if(!dealData()) {
						mDealState = false;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mDealState = false;
			mResponseCode = "E";
			buildError("service()", "系统未知错误");
		}finally {
			return createXML();
		}
	}

	/**
	 * 生成返回的报文信息
	 * 
	 * @return Document
	 */
	private Document createXML() {
		if(!mDealState) {
			return createFalseXML();
		}else {
			try {
				return createResultXML(mIDNO);
			}catch(Exception e) {
				buildError("createXML", "生成错误报文");
				return createFalseXML();
			}
		}
	}

	

	private Document createResultXML(String mIDNO2) {
		System.out.println("LLTYIDGetDutyInfo------------------createResultXML------返回正确报文");
		
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");
		//创建head部分
		Element tHeadData = new Element("HEAD");
		
		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);
		
		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);
		
		tRootData.addContent(tHeadData);
		
		//创建body部分
		Element tBodyData = new Element("BODY");
		//报文保单责任部分
		qryPolicyList();
		tBodyData.addContent(tPOLCIYLIST);
		
		tRootData.addContent(tBodyData);
		Document tDocument = new Document(tRootData);
		return tDocument;
	}

	/**
	 * 报文保单责任部分的生成
	 */
	private void qryPolicyList() {
		System.out.println("LLTYIDGetDutyInfo-------qryPolicyList-------start报文保单责任部分的生成");
		
		for(int i = 1 ; i <= mSSRS.getMaxRow() ; i++) {
			Element tPolicyData = new Element("POLICY_DATA");
			Element tContNo = new Element("CONTNO");
			Element tGrpContNo = new Element("GRPCONTNO");
			Element tPrem = new Element("PREM");
			Element tAmnt = new Element("AMNT");
			Element tInsuredName = new Element("INSUREDNAME");
			Element tInsuredNo = new Element("INSUREDNNO");
			Element tInsuredSex = new Element("INSUREDSEX");
			Element tInsuredIDType = new Element("INSUREDIDTYPE");
			Element tInsuredIDNo = new Element("INSUREDIDNO");
			Element tInsuredBirthday = new Element("INSUREDBIRTHDAY");
			Element tCvaliDate = new Element("CVALIDATE");
			Element tCinvaliDate = new Element("CINVALIDATE");
			Element tRiskCode = new Element("RISKCODE");
			Element tRiskName = new Element("RISKNAME");
			Element tDutyCode = new Element("DUTYCODE");
			Element tDutyName = new Element("DUTYNAME");
			Element tGetDutyCode = new Element("GETDUTYCODE");
			Element tGetDutyName = new Element("GETDUTYNAME");
			Element tGetDutyKind = new Element("GETDUTYKIND");
			Element tGetDutyKindName = new Element("GETDUTYKINDNAME");
			Element tDutyPrem = new Element("DUTYPREM");
			Element tDutyAmnt = new Element("DUTYAMNT");
			Element tGetRate = new Element("GETRATE");
			Element tGetLimit = new Element("GETLIMIT");
			Element tStateFlag = new Element("STATEFLAG");
			Element tSpecContent = new Element("SPECCONTENT");
			Element tBankNo = new Element("BANKNO");
			Element tBankName = new Element("BANKNAME");
			Element tAccno = new Element("ACCNO");
			Element tAccnName = new Element("ACCNAME");
			Element tRelationTomainInsured = new Element("RELATIONTOMAININSURED");
			Element tName = new Element("NAME");
			Element tIDType = new Element("IDTYPE");
			Element tIDNo = new Element("IDNO");
			Element tCustomerNo = new Element("CUSTOMERNO");
			Element tSex = new Element("SEX");
			
			tContNo.setText(mSSRS.GetText(i, 1));
			tGrpContNo.setText(mSSRS.GetText(i, 2));
			tPrem.setText(mSSRS.GetText(i, 3));
			tAmnt.setText(mSSRS.GetText(i, 4));
			tInsuredName.setText(mSSRS.GetText(i, 5));
			tInsuredNo.setText(mSSRS.GetText(i, 6));
			tInsuredSex.setText(mSSRS.GetText(i, 7));
			tInsuredIDType.setText(mSSRS.GetText(i, 8));
			tInsuredIDNo.setText(mSSRS.GetText(i, 9));
			tInsuredBirthday.setText(mSSRS.GetText(i, 10));
			tCvaliDate.setText(mSSRS.GetText(i, 11));
			tCinvaliDate.setText(mSSRS.GetText(i, 12));
			tRiskCode.setText(mSSRS.GetText(i, 13));
			tRiskName.setText(mSSRS.GetText(i, 14));
			tDutyCode.setText(mSSRS.GetText(i, 15));
			tDutyName.setText(mSSRS.GetText(i, 16));
			tGetDutyCode.setText(mSSRS.GetText(i, 17));
			tGetDutyName.setText(mSSRS.GetText(i, 18));
			tGetDutyKind.setText(mSSRS.GetText(i, 19));
			tGetDutyKindName.setText(mSSRS.GetText(i, 20));
			tDutyPrem.setText(mSSRS.GetText(i, 21));
			tDutyAmnt.setText(mSSRS.GetText(i, 22));
			tGetRate.setText(mSSRS.GetText(i, 23));
			tGetLimit.setText(mSSRS.GetText(i, 24));
			tStateFlag.setText(mSSRS.GetText(i, 25));
			tSpecContent.setText(mSSRS.GetText(i, 26));
			tBankNo.setText(mSSRS.GetText(i, 27));
			tBankName.setText(mSSRS.GetText(i, 28));
			tAccno.setText(mSSRS.GetText(i, 29));
			tAccnName.setText(mSSRS.GetText(i, 30));
			tRelationTomainInsured.setText(mSSRS.GetText(i, 31));
			tName.setText(mSSRS.GetText(i, 32));
			tIDType.setText(mSSRS.GetText(i, 33));
			tIDNo.setText(mSSRS.GetText(i, 34));
			tCustomerNo.setText(mSSRS.GetText(i, 35));
			tSex.setText(mSSRS.GetText(i, 36));
			
			tPolicyData.addContent(tContNo);
			tPolicyData.addContent(tGrpContNo);
			tPolicyData.addContent(tPrem);
			tPolicyData.addContent(tAmnt);
			tPolicyData.addContent(tInsuredName);
			tPolicyData.addContent(tInsuredNo);
			tPolicyData.addContent(tInsuredSex);
			tPolicyData.addContent(tInsuredIDType);
			tPolicyData.addContent(tInsuredIDNo);
			tPolicyData.addContent(tInsuredBirthday);
			tPolicyData.addContent(tCvaliDate);
			tPolicyData.addContent(tCinvaliDate);
			tPolicyData.addContent(tRiskCode);
			tPolicyData.addContent(tRiskName);
			tPolicyData.addContent(tDutyCode);
			tPolicyData.addContent(tDutyName);
			tPolicyData.addContent(tGetDutyCode);
			tPolicyData.addContent(tGetDutyName);
			tPolicyData.addContent(tGetDutyKind);
			tPolicyData.addContent(tGetDutyKindName);
			tPolicyData.addContent(tDutyPrem);
			tPolicyData.addContent(tDutyAmnt);
			tPolicyData.addContent(tGetRate);
			tPolicyData.addContent(tGetLimit);
			tPolicyData.addContent(tStateFlag);
			tPolicyData.addContent(tSpecContent);
			tPolicyData.addContent(tBankNo);
			tPolicyData.addContent(tBankName);
			tPolicyData.addContent(tAccno);
			tPolicyData.addContent(tAccnName);
			tPolicyData.addContent(tRelationTomainInsured);
			tPolicyData.addContent(tName);
			tPolicyData.addContent(tIDType);
			tPolicyData.addContent(tIDNo);
			tPolicyData.addContent(tCustomerNo);
			tPolicyData.addContent(tSex);
			
			tPOLCIYLIST.addContent(tPolicyData);
		}
		System.out.println("LLTYIDGetDutyInfo-------qryPolicyList-------end报文保单责任部分的生成");	
	}

	private Document createFalseXML() {
		System.out.println("LLTYIDGetDutyInfo------------------createFalseXML-------返回错误报文");
		
		//创建根节点以及添加根节点属性值
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");
		//创建head部分以及节点和内容
		Element tHeadData = new Element("HEAD");
		
		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);
		
		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);
		
		tRootData.addContent(tHeadData);
		//创建body部分
		Element tBodyData = new Element("BODY");
		//返回的案件处理机构
		Element tResponseCode = new Element("RESPONSE_CODE");
		tResponseCode.setText("1");
		tBodyData.addContent(tResponseCode);
		
		mErrorMessage = mErrors.getFirstError();
		Element tErrorMessage = new Element("ERROR_MESSAGE");
		tErrorMessage.setText(mErrorMessage);
		
		tBodyData.addContent(tErrorMessage);
		
		tRootData.addContent(tBodyData);
		
		Document tDocument = new Document(tRootData);
		return tDocument;
	}

	/**
	 * 处理报文信息
	 * 
	 * @return  boolean
	 */
	private boolean dealData() {
		System.out.println("LLTYIDGetDutyInfo------------------dealData");
		
		ExeSQL tExeSQL = new ExeSQL();	
		String dealSQL = "select "+
				"lcp.contno,"+
				"lcp.grpcontno, " +
				"lcp.prem,"+
				"lcp.amnt,"+
				"lcp.insuredname,"+
				"lcp.insuredno,"+
				"lci.sex,"+
				"lci.IDType,"+
				"lci.idno,"+
				"lci.Birthday,"+
				"lcp.cvalidate,"+
				"lcp.enddate,"+
				"lcp.riskcode,"+
				"(select riskname from lmriskapp where riskcode = lcp.riskcode fetch first row only),"+
				"lcd.dutycode,"+
				"(select dutyname from lmduty where dutycode= lcd.dutycode),"+
				"lcg.getdutycode,"+
				"(select getdutyname from lmdutyget where getdutycode = lcg.getdutycode),"+
				" lmd.getdutykind,"+
				" (SELECT codename FROM ldcode WHERE codetype = 'getdutykind' AND code = lmd.getdutykind),"+
				"lcd.prem,"+
				"lcd.amnt,"+
				"lcd.getrate,"+
				"lcd.getlimit,"+
				"(select codename from ldcode where codetype = 'stateflag' and code = lcp.stateflag),"+
				"( case when lcp.grpcontno='00000000000000000000' then (select remark from lccont  where contno=lcp.contno ) " + 
				 "else  (select remark  from lcgrpcont where grpcontno= lcp.grpcontno) end), " +
				"lci.BankCode,"+
				"(select bankname from ldbank where bankcode = lci.BankAccNo),"+
				"lci.BankAccNo,"+
				"lci.AccName,"+
				"(case when lci.RelationToMainInsured = '00' then '本人' "+
				"when lci.RelationToMainInsured = '01' then '妻子' "+
				"when lci.RelationToMainInsured = '02' then '丈夫' "+
				"when lci.RelationToMainInsured = '03' then '儿女' "+
				"when lci.RelationToMainInsured = '04' then '父亲' "+
				"when lci.RelationToMainInsured = '05' then '母亲' "+
				"when lci.RelationToMainInsured = '06' then '岳父' "+
				"when lci.RelationToMainInsured = '07' then '岳母' "+
				"else '' end),"+
				"(select name from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select idtype from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select idno from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select insuredno from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select sex from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only)"+
				"from lcpol lcp,lcinsured lci,lcget lcg,lcduty lcd,lmdutygetclm lmd "+
				"where 1=1 "+
				"and lcp.appflag ='1' "+
				"and lcp.stateflag <>'4' "+
				"and lmd.getdutycode=lcg.getdutycode "+
				"and lcp.prtno = lci.prtno "+
				"and lcg.contno = lcp.contno "+
				"and lcd.contno = lcp.contno "+
				"and lcd.contno = lcg.contno "+
				"and lcp.insuredno = lci.insuredno "+
				"and lci.contno = lcp.contno "+
				"and lcp.polno = lcg.polno "+
				"and lcg.polno = lcd.polno "+
				"and lcg.DutyCode = lcd.DutyCode "+
				"and lci.idno='"+mIDNO+"' "+
				"union "+
				"select "+
				"lcp.contno,"+
				"lcp.grpcontno, " +
				"lcp.prem,"+
				"lcp.amnt,"+
				"lcp.insuredname,"+
				"lcp.insuredno,"+
				"lci.sex,"+
				"lci.IDType,"+
				"lci.idno,"+
				"lci.Birthday,"+
				"lcp.cvalidate ,"+
				//修改保单失效日期
				"(case when (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lp.EdorNo and lp.EdorNo = lcp.EdorNo and lpdiskimport.insuredno = lcp.insuredno)  is not null then (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lp.EdorNo and lp.EdorNo = lcp.EdorNo and lpdiskimport.insuredno = lcp.insuredno) when (select edorvalidate from lpedoritem where edorno = lcp.edorno fetch first 1 rows only) is not null then (select edorvalidate from lpedoritem where edorno = lcp.edorno fetch first 1 rows only) when (select edorvalidate from lpgrpedoritem where edorno = lcp.edorno fetch first 1 rows only) is not null then (select edorvalidate from lpgrpedoritem where edorno = lcp.edorno fetch first 1 rows only) else lcp.paytodate end) as CInValiDate,"+
				"lcp.riskcode,"+
				"(select riskname from lmriskapp where riskcode = lcp.riskcode fetch first row only),"+
				"lcd.dutycode,"+
				"(select dutyname from lmduty where dutycode= lcd.dutycode),"+
				"lcg.getdutycode,"+
				"(select getdutyname from lmdutyget where getdutycode = lcg.getdutycode),"+
				"lmd.getdutykind,"+
				"(SELECT codename FROM ldcode WHERE codetype = 'getdutykind' AND code = lmd.getdutykind),"+
				"lcd.prem,"+
				"lcd.amnt,"+
				"lcd.getrate,"+
				"lcd.getlimit,"+
				"(select codename from ldcode where codetype = 'stateflag' and code = lcp.stateflag),"+
				"( case when lcp.grpcontno='00000000000000000000' then (select remark from lbcont  where contno=lcp.contno ) " + 
				 "else  (select remark  from lbgrpcont where grpcontno= lcp.grpcontno) end), " +
				"lci.BankCode,"+
				"(select bankname from ldbank where bankcode = lci.BankAccNo),"+
				"lci.BankAccNo,"+
				"lci.AccName,"+
				"(case when lci.RelationToMainInsured = '00' then '本人' "+
							"when lci.RelationToMainInsured = '01' then '妻子' "+
							"when lci.RelationToMainInsured = '02' then '丈夫' "+
							"when lci.RelationToMainInsured = '03' then '儿女' "+
							"when lci.RelationToMainInsured = '04' then '父亲' "+
							"when lci.RelationToMainInsured = '05' then '母亲' "+
							"when lci.RelationToMainInsured = '06' then '岳父' "+
							"when lci.RelationToMainInsured = '07' then '岳母' "+
							"else '' end),"+
				"(select name from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select idtype from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select idno from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select insuredno from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
				"(select sex from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only) "+
				"from lbpol lcp,lbinsured lci,lbget lcg,lbduty lcd,lmdutygetclm lmd "+
				"where 1=1 "+
				"and lcp.appflag='1' "+
				"and lcp.stateflag <>'4' "+
				"and lmd.getdutycode=lcg.getdutycode "+
				"and lcp.prtno = lci.prtno "+
				"and lcg.contno = lcp.contno "+
				"and lcd.contno = lcp.contno "+
				"and lcd.contno = lcg.contno "+
				"and lcp.insuredno = lci.insuredno "+
				"and lci.contno = lcp.contno "+
				"and lcp.polno = lcg.polno "+
				"and lcg.polno = lcd.polno "+
				"and lcg.DutyCode = lcd.DutyCode "+
				"and lci.idno='"+mIDNO+"' with ur";
		
		mSSRS = tExeSQL.execSQL(dealSQL);
		if(mSSRS!=null&&mSSRS.getMaxRow()>0) {
			
		}else {
			buildError("dealData()", "本次理赔申请不在外包理赔范围内，请您到公司柜面申请理赔");
			return false;
		}
		return true;
	}

	/**
	 * 校验基本数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		System.out.println("LLTYIDGetDutyInfo------------------checkData");
		
		if(!"REQUEST".equals(mDocType)) {
			buildError("checkData()", "报文类型【type=" + mDocType + "】错误！");
			return false;
		}
		if(!"TY01".equals(mRequestType)) {
			buildError("checkData()", "【请求类型】的值不存在或者匹配错误！");
			return false;
		}
		if(mTransactionNum==null || "".equals(mTransactionNum) || "null".equals(mTransactionNum) 
				|| mTransactionNum.length() != 30) {
			buildError("checkData()", "【交互编码】有问题！");
			return false;
		}
		if(!"TY01".equals(mTransactionNum.substring(0, 4))) {
			buildError("checkData()", "【交互编码】的请求类型编码错误！");
			return false;
		}
		
		return true;
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param mInXml
	 * 				Document
	 * @return boolean
	 */
	private boolean getInputData(Document mInXml) {
		System.out.println("LLTYIDGetDutyInfo------------------getInputData");
		if(mInXml==null) {
			buildError("getInputData()", "未获取报文");
			return false;
		}
		//获取根节点
		Element tRootData = mInXml.getRootElement();
		//获取根节点type属性的值
		mDocType = tRootData.getAttributeValue("type");
		//获取head内容
		Element tHeadData = tRootData.getChild("HEAD");
		//获取head节点的value
		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
		
		System.out.println("LLTYIDGetDutyInfo---------------mTransactionNum=" + mTransactionNum);
		
		//获取body部分内容
		Element tInBody = tRootData.getChild("BODY");
		Element tInInSuredData = tInBody.getChild("INSURED_DATA");
		mIDNO = tInInSuredData.getChildTextTrim("IDNO");
		return true;
	}
	
	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TestInfo";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		Document tInXmlDoc;
		try {
			tInXmlDoc = JdomUtil.build(new FileInputStream(
					"E:\\理赔报文\\TY接口\\TY01.xml"), "GBK");
			LLTYIDGetDutyInfo ttt = new LLTYIDGetDutyInfo();
			Document tOutXmlDoc = ttt.service(tInXmlDoc);
			JdomUtil.print(tOutXmlDoc);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
