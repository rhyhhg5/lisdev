package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLSocialTransFtpXmlCase {

	 /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();

    public LLSocialTransFtpXmlCase()
    {
    }

    public boolean submitData()
    {
        if (getSubmitMap() == null)
        {
            return false;
        }


        return true;
    }



    public MMap getSubmitMap()
    {


        if (!dealData())
        {
            return null;
        }

        return map;
    }


    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }
    boolean getXls(){
    	 String tUIRoot = CommonBL.getUIRoot();

         if (tUIRoot == null)
         {
             mErrors.addOneError("没有查找到应用目录");
             return false;
         }

         int xmlCount = 0;

         ExeSQL tExeSql = new ExeSQL();

         String tServerIP = tExeSql
                 .getOneValue("select codename from ldcode where codetype='SocialClaim' and code='IP/Port'");
         String tPort = tExeSql
                 .getOneValue("select codealias from ldcode where codetype='SocialClaim' and code='IP/Port'");
         String tUsername = tExeSql
                 .getOneValue("select codename from ldcode where codetype='SocialClaim' and code='User/Pass'");
         String tPassword = tExeSql
                 .getOneValue("select codealias from ldcode where codetype='SocialClaim' and code='User/Pass'");

         

//       服务器存放路径		
	       String tFileXmlPath = tExeSql
			.getOneValue("select codename from ldcode where codetype='SocialClaim' and code='XmlPath'");
   
 		String tFilePathLocalCore = tUIRoot + tFileXmlPath;
 		//String tFilePathLocalCore = tFileXmlPath;

//      服务器存放批次报文的路径  
//        String tFileXmlPathRgt = tFileBackXmlPath  + PubFun.getCurrentDate()+"\\";
//      服务器存放批次返回报文的路径         
//       String tFileBackXmlPathRgt = tFileBackXmlPath  + PubFun.getCurrentDate()+"\\Back\\";

    	
    	try
        {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "ActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "LLSocialTransFtpXmlCase";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
           
//            String tFilePathLocalCore = tFileXmlPath;
//            tFilePathLocalCore="E:\\ceshi\\case\\";//本地测试路径 代替服务器存放xml路径
            tFilePathLocalCore = tFilePathLocalCore+PubFun.getCurrentDate()+"/";//服务器存放案件报文文件的路径 每天生成一个文件夹,服务器上要改为"/"本地测试用"\\"
//            String tFileImportBackPath = tFilePath;
            String tFileImportPath = "/sbftp/SBClaimCase";//存放社保向核心发送的案件信息XML文件 的目录
            String tFileImportBackPath = "/sbftp/SBClaimCaseBack";//ftp上存放 核心向社保返回的案件信息处理结果的XML文件 的目录
            
            String tFileImportPathBackUp = "/sbftp/SBClaimBackup/" +PubFun.getCurrentDate()+"/";//存放核心理赔相关备份文件，每天归档
            
            String tFilePathout = tFilePathLocalCore+"Back/";//发布到服务器上需要改为"/"本地测试用"\\"
//            tFTPTool.changeWorkingDirectory(tFileImportBackPath);
//            tFTPTool.makeDirectory("WBClaimBack");
            
//            if(!newFolder(tUIRoot+"temp_lp/WBClaim/")){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tUIRoot+"temp_lp/WBClaim/8695/")){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFileCasePath)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFilePathLocalCore)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFilePathout)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
            
            File mFileDir = new File(tFilePathLocalCore);
    		if (!mFileDir.exists()) {
    			if (!mFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
    			}
    		}
    		
            File tFileDir = new File(tFilePathout);
    		if (!tFileDir.exists()) {
    			if (!tFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
    			}
    		}
    		
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
            System.out.println("OK");
//            tFTPTool.removeDirectory(tFileImportBackPath);
//            System.out.println("OK");
//            tFTPTool.uploadAll(tFileImportPath, tFilePathLocal);
            System.out.println("tFileImportPath="+tFileImportPath);
//            File tIMFileDir = new File(tFileImportPath);
//            String[] fileNames=tIMFileDir.list(); 
//            if(fileNames==null||0==fileNames.length){
//            	tFTPTool.logoutFTP();
//            	return false;
//            }
//            tFTPTool.tFileImportPath
            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            System.out.println(errContent);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                tFTPTool.logoutFTP();
                return false;
            }
            System.out.println("tFilePathLocalCore="+tFilePathLocalCore);
           
            if(tPath.length==0){
            	System.out.println("目录中为找到XML文件");
                return false;
            }
            System.out.println("tFileImportPathBackUp=="+tFileImportPathBackUp);
            for (int k = 0; k < tPath.length; k++)
            {
                if (tPath[k] == null)
                {
                    continue;
                }
                if(tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[k])){
                	tFTPTool.changeWorkingDirectory(tFileImportPath);
                    tFTPTool.deleteFile(tPath[k]);
                }
                	
                
            }
            tFTPTool.logoutFTP();//获取报文后先退出连接做业务处理
            
           
            
            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }


                xmlCount++;
                 
                 
                try
                {
                    VData tVData = new VData();

                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("FileName", tPath[j]);
                    tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);

                    tVData.add(tTransferData);
                    tVData.add(mGI);

//                    AddCertifyListUI tAddCertifyListUI = new AddCertifyListUI();

//                    if (!tAddCertifyListUI.submitData(tVData, "ActiveImport"))
//                    {
//                        mErrors.copyAllErrors(tAddCertifyListUI.mErrors);
//                        try
//                        {
//                            tFTPTool.deleteFile(tPath[j]);
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
                    Document tInXmlDoc;   
                    FileInputStream fis = new FileInputStream(tFilePathLocalCore+tPath[j]);
                    tInXmlDoc = JdomUtil.build(fis, "GBK");
                    fis.close();
                    LLSocialAutoXML tLLSocialAutoXML = new LLSocialAutoXML();
                    Document tOutXmlDoc = tLLSocialAutoXML.service(tInXmlDoc);
                    System.out.println("打印传入报文============");
                    JdomUtil.print(tInXmlDoc.getRootElement());
                    System.out.println("打印传出报文============");
                    JdomUtil.print(tOutXmlDoc);
                    
                    
                    FileOutputStream fos = new FileOutputStream(tFilePathout+tPath[j]);
                    JdomUtil.output(tOutXmlDoc,fos );
                    fos.close();

                    


                }
                catch (Exception ex)
                {
                    System.out.println("批次导入失败: " + tPath[j]);
                    mErrors.addOneError("批次导入失败" + tPath[j]);
                    ex.printStackTrace();
                  
                }
            }
//          待业务处理完成上传返回报文时再连接FTP
            FTPTool tFTPTool2 = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "ActiveMode");
            if (!tFTPTool2.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "LLSocialTransFtpXmlCase";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool2.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
            //返回报文
            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }
	            
	            
	            System.out.println("tFilePathout="+tFilePathout);
	            tFTPTool2.upload(tFileImportBackPath, tFilePathout+tPath[j]);
	//            tFTPTool.deleteFile(tPath[j]);
	            
            }
            tFTPTool2.logoutFTP();
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LLSocialTransFtpXmlCase";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LLSocialTransFtpXmlCase";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

    	return true;
    }
    
    private String getDealInfo()
    {
        return mDealInfo;
    }
    
//  建文件夹
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
	
    public static void main(String[] args)
    {
    	LLSocialTransFtpXmlCase a =new LLSocialTransFtpXmlCase();
    	a.getXls();
    }
}
