public class ClientTest {

	public static String callService(String wsUrl, String method, Object... arg)
			throws Exception {
		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf.createClient(wsUrl);
		Object[] res = client.invoke(method, arg);
		return (String) res[0];
	}

	public static void main(String[] args) throws Exception {
                //1.2.2	案件给付信息反馈 xml格式字符串示例
                String xmlstr = "<?xml version=\"1.0\" encoding=\"GBK\"?>" +
		"<PACKET type=\"RESPONSE\" version=\"1.0\" >" +
		"<HEAD>"+
		  "<REQUEST_TYPE>TJ02</REQUEST_TYPE>"+
		  "<TRANSACTION_NUM>TJ0286120000XXXX</TRANSACTION_NUM>"+
		"</HEAD>"+
		"<BODY>"+
		"<CLAIM_NO>2011052602010002</CLAIM_NO>"+
		"<CASENO>C1200010050100001</CASENO>"+
		"<BNFNO>1000000000000880</BNFNO>"+
		"<BNFNAME>微Ⅰ四</BNFNAME>"+
		"<SUMPAY>702.88</SUMPAY>"+
		"<BANKCODE>010203</BANKCODE>"+
		"<ACCNO>1341641316576131</ACCNO>"+
		"<ACCNAME>张三</ACCNAME>"+
		"<DEALFLAG>1</DEALFLAG>"+
		"<CONFDATE>2011-05-01</CONFDATE>"+
		"<FALSEREASON></FALSEREASON>"+
		"</BODY>"+
		"</PACKET>";
		 String retStr=ClientTest.callService("http://IP未确定:7001/tjsx/services/PICC_CALL_YH?wsdl",
		 "call_YH_Hp", new Object[]{xmlstr});
		 System.out.println(retStr);
	}
}