package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom.Document;

public class SaveMessage {
    /**
     * 依据预订规则，生成报文文件名
     * @param sTransCode
     * @param sSuffix
     * @return
     */
    public static String generateFileName(String sTransCode, String sSuffix){
        return (sTransCode + "_" + DateUtil.getCurrentDate("HHmmss") + "." + sSuffix);
    }
    
    /**
     * 依据预订规则生成报文存放路径
     * @param pType
     * @return
     */
    public static String generateFilePath(String FilePath,String pType)
    {
        StringBuffer sFilePath = new StringBuffer();
        sFilePath.append(FilePath).append('/')
                    .append("FileContent").append('/')
                    .append(DateUtil.getCurrentDate("yyyy")).append('/')
                    .append(DateUtil.getCurrentDate("yyyyMM")).append('/')
                    .append(DateUtil.getCurrentDate("yyyyMMdd")).append('/')
                    .append(pType).append('/');
        return sFilePath.toString();
    }
    
    public static void save(Document pXmlDoc, String pName, String pType,String FilePath) {
        StringBuffer mFilePath = new StringBuffer();
        mFilePath.append(generateFilePath(FilePath,pType));
        File mFileDir = new File(mFilePath.toString());
        if (!mFileDir.exists()) {
            if (!mFileDir.mkdirs()) {
                System.err.println("创建目录失败！" + mFileDir.getPath());
            }
        }
        
        try {
            FileOutputStream tFos = new FileOutputStream(mFilePath.toString() + pName);
            JdomUtil.output(pXmlDoc, tFos);
            tFos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void save(byte[] pBytes, String pName, String pType,String FilePath) {
        StringBuffer mFilePath = new StringBuffer();
        mFilePath.append(generateFilePath(FilePath,pType));
        File mFileDir = new File(mFilePath.toString());
        if (!mFileDir.exists()) {
            if (!mFileDir.mkdirs()) {
                System.err.println("创建目录失败！" + mFileDir.getPath());
            }
        }
        
        try {
            FileOutputStream tFos = new FileOutputStream(mFilePath.toString() + pName);
            tFos.write(pBytes);
            tFos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
