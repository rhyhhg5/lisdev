package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class LLPadGetScanImageBL {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mPrtNo = "";

	private TransferData mTransferData;

	private FTPTool tFTPTool;

	private String mPicPath;
	
	private String mFTPLocation; //本地存放ZIP地址

//	private String mZipName;	//本地存放ZIP名字
	
	private String mCaseNo;
	
	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
	
	private String mZipLocalPath;

	private String tFileName;

	private String mFTPPath;

	private VData mVData;

	public boolean submitData(VData cInputData, String operate) {

		mVData = cInputData;

		if (!getInputData(mVData)) {
			return false;
		}

		if (!getConn()) {
			return false;
		}

		if (!getFile()) {
			try {
				tFTPTool.logout();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		//deleteFile();

		if (!dealFile()) {
			return false;
		}

		return true;
	}

	private void deleteFile() {
		try {
			//tFTPTool.deleteFile(mFTPPath + tFileName);
			tFTPTool.logout();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean dealFile() {
		LLPadDealScanImages tLLPadDealScanImages = new LLPadDealScanImages();
		VData tVData = new VData();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("CaseNo", mCaseNo);
		transferData.setNameAndValue("ZipLocalPath", mZipLocalPath);
		transferData.setNameAndValue("ZipName", tFileName);
		tVData.add(mGI);
		tVData.add(transferData);

		try {
			if (!tLLPadDealScanImages.submitData(tVData, "")) {
				CError tError = new CError();
				tError.moduleName = "PadGetScanFileBL";
				tError.functionName = "dealFile";
				tError.errorMessage = tLLPadDealScanImages.mErrors.getFirstError();
				mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	private boolean getFile() {
		String getPath = "select codename ,codealias from ldcode where codetype='LLPadScan' and code='FTPPath/LocalPath' ";//若有约定好目录保存FTPPath
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "PadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp服务器路径未配置";
			mErrors.addOneError(tError);
			return false;
		}
		mFTPPath = tPathSSRS.GetText(1, 1);
		mFTPPath = mFTPLocation;
		String tUIRoot = CommonBL.getUIRoot();
		mZipLocalPath = tUIRoot+tPathSSRS.GetText(1, 2)+PubFun.getCurrentDate()+ "/";
		boolean downflag = true;
		downflag = tFTPTool.downloadFileExists(mFTPPath, mZipLocalPath, tFileName);
		System.out.println("下载成功标识为：" + downflag);
		if (!downflag) {
			System.out.println(tFTPTool.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "PadGetScanFileBL";
			tError.functionName = "getFile";
			tError.errorMessage = tFTPTool.mErrors.getFirstError();
			mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	private boolean getInputData(VData inputData) {
		mGI = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) inputData.getObjectByObjectName(
				"TransferData", 0);
		mLLCaseSchema = (LLCaseSchema)inputData.getObjectByObjectName("LLCaseSchema", 0);
		mCaseNo = mLLCaseSchema.getCaseNo();
		mPicPath = (String) mTransferData.getValueByName("PicPath");//穿过来的zip包完整路径
		//得到传过来的FTP上的路径和文件名
		File FTPzip = new File(mPicPath);
		tFileName = FTPzip.getName();
		mFTPLocation =mPicPath.substring(0,mPicPath.lastIndexOf(tFileName));
		System.out.println("上传扫描件案件号为：" + mCaseNo);
		return true;
	}

	private boolean getConn() {
		String getIPPort = "select codename ,codealias from ldcode where codetype='LLPadScan' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='LLPadScan' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "PadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp配置有误";
			mErrors.addOneError(tError);
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 1),
				tUPSSRS.GetText(1, 2), Integer.parseInt(tIPSSRS.GetText(1, 2)));
		try {
			if (!tFTPTool.loginFTP()) {
				CError tError = new CError();
				tError.moduleName = "PadGetScanFileBL";
				tError.functionName = "getConn";
				tError.errorMessage = tFTPTool
						.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				mErrors.addOneError(tError);
				return false;
			}
		} catch (SocketException e) {
			CError tError = new CError();
			tError.moduleName = "PadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			CError tError = new CError();
			tError.moduleName = "PadGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		// 160000000001
		LLCaseSchema mLLCaseSchema = new LLCaseSchema();
		mLLCaseSchema.setCaseNo("C4400151117000131");
		LLPadGetScanImageBL tPadGetFile = new LLPadGetScanImageBL();
		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("PicPath", "/llpadscan/test.zip");
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.Operator = "LLPadScan";
		mGlobalInput.AgentCom = "";
		mGlobalInput.ManageCom = "86110000";
		mGlobalInput.ComCode = "86110000";

		VData tVData = new VData();
		tVData.add(mLLCaseSchema);
		tVData.add(transferData1);
		tVData.add(mGlobalInput);
		tPadGetFile.submitData(tVData, "");
//		
//		File root = new File("E:\\PADTEST\\iMAGES\\zip\\test.zip");
//		String name = root.getName();
//		System.out.println(root.getName());
//		String path =root.getPath();
//		System.out.println(path.substring(0,path.lastIndexOf(name)));
	}

}
