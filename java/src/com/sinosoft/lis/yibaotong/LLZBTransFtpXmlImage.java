package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public class LLZBTransFtpXmlImage {
	/**
	 * 错误容器
	 * @param args
	 */
	public CErrors tErrors = new CErrors();
    private String mDealInfo = "";
    private GlobalInput mGI = null; //用户信息
    private MMap map = new MMap();
    
    public LLZBTransFtpXmlImage(){
    	
    }
    
    public boolean submitData(){
    	
    	if(getSubmitMap()==null){
    		return false;
    	}
    	
    	return true;
    }
    
    public MMap getSubmitMap(){
    	
    	if (!dealData())
        {
            return null;
        }
    	return map;
    }
    
    private boolean dealData(){
    	
    	if(!getXls()){
    		
    		return false; 
    	}

    	return true;
    }
    
    /***
     * 开始逻辑的处理
     * @return
     */
    private boolean getXls(){
    	
    	String tUIRoot= CommonBL.getUIRoot();
    	if(tUIRoot== null){
    		tErrors.addOneError("没有查找到应用目录");
    		return false;
    	}
    	//本地测试
//    	tUIRoot="D:\\picc\\ui\\"; 
    	
    	int xmlCount=0;
    	
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	String tServerIP = tExeSQL
                .getOneValue("select codename from ldcode where codetype='ZBClaim' and code='IP/Port'");
    	String tPort = tExeSQL
                .getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='IP/Port'");
    	String tUsername = tExeSQL
                .getOneValue("select codename from ldcode where codetype='ZBClaim' and code='User/Pass'");
    	String tPassword = tExeSQL
                .getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='User/Pass'");
    	
    	String tFileBackPath = tExeSQL
                 .getOneValue("select codename from ldcode where codetype='ZBClaim' and code='UrlPicPath'");
    	
    	tFileBackPath= tUIRoot + tFileBackPath;  //核心影像件存放位置
    	
    	//本地测试
//    	tFileBackPath =tUIRoot +"xerox\\EasyScan2016\\WBClaim\\8633\\";
    	
    	
    	try {
    	FTPTool tFTPTool = new FTPTool(tServerIP,tUsername,tPassword,Integer.parseInt(tPort),"aaActiveMode");
			if(!tFTPTool.loginFTP()){
				CError tCError = new CError();
				tCError.moduleName="LLZBTransFtpXmlImage";
				tCError.functionName="getXls";
				tCError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                tErrors.addOneError(tCError);
                System.out.println(tCError.errorMessage);
                return false;
			}
			
			 String tFilePathLocalCore = tFileBackPath+PubFun.getCurrentDate()+"/";//核心影像件的地址
			 //本地测试
//			 String tFilePathLocalCore = tFileBackPath+PubFun.getCurrentDate()+"\\";
			 
			 String tFileImportPath = "/01PH/8633/ZWBClaimImage"; //ftp文件存放目录
	         String tFileImportPathBackUp = "/01PH/8633/ZWBClaimBackUp/"+PubFun.getCurrentDate()+"/";//FTP备份目录
	         
	         //创建核心文件
	         File tFile = new File(tFilePathLocalCore);
	         if(!tFile.exists()){
	        	 if(!tFile.mkdirs()){
	        		 System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
	    						+ tFile.getPath());
	        	 }
	         }
	         
	         //新建FTP备份文件夹
	         if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
	        	 System.out.println("新建目录已存在");
	         }
			 
	         System.out.println("===========浙江外包影像件开始操作======");
	         // FTP文件获取至核心
	         String[] tPath= tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
	         String tErrCount = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
	         
	         if( tPath==null && tErrCount!=null){
	        	 tErrors.addOneError(tErrCount);
	        	 return false;
	         }
			
	         for(int i=0;i<tPath.length;i++){
	        	 
	        	 if(tPath[i]==null){
	        		 continue;
	        	 }
	        	 
	        	 xmlCount++;
	        	 // 备份影像件
	        	 tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[i]);
	        	 tFTPTool.changeWorkingDirectory(tFileImportPath);
	        	 // 删除
	        	 tFTPTool.deleteFile(tPath[i]);
	        	 System.out.println("浙江外包影像件第"+tPath[i]+"结束上载");
	        	 
	         }
	         tFTPTool.logoutFTP();

		} catch (SocketException e) {
				e.printStackTrace();
			 	CError tError = new CError();
	            tError.moduleName = "LLZBTransFtpXmlImage";
	            tError.functionName = "getXls";
	            tError.errorMessage = "获取连接失败";
	            tErrors.addOneError(tError);
	            System.out.println(tError.errorMessage);
	            return false;
		} catch (IOException e) {
				e.printStackTrace();
				CError tError = new CError();
	            tError.moduleName = "LLZBTransFtpXmlImage";
	            tError.functionName = "getXls";
	            tError.errorMessage = "获取连接失败";
	            tErrors.addOneError(tError);
	            System.out.println(tError.errorMessage);
	            return false;
		}
    	
    	if (xmlCount == 0)
        {
            tErrors.addOneError("没有需要导入的数据");
        }
    	
    	return true;
    }
    
    
    
	public String getmDealInfo() {
		return mDealInfo;
	}

	public void setmDealInfo(String mDealInfo) {
		this.mDealInfo = mDealInfo;
	}

	public static void main(String[] args) {
		LLZBTransFtpXmlImage tLLZBTransFtpXmlImage = new LLZBTransFtpXmlImage();
		tLLZBTransFtpXmlImage.getXls();

	}

}
