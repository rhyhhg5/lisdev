package com.sinosoft.lis.yibaotong;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;


import java.io.*;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.*;

/**
 * 保单号是否已经配置官网理赔查询功能
 * @author Alfred Xu
 * @time 2016-08-24
 *
 */
public class LLGWCheckContNo implements ServiceInterface{
	public CErrors mErrors = new CErrors();
	private Document tInXmlDoc;
	private Element body;
	private Element head;
	private String point;
	private String requestType;
	private String mErrorMessage;
	private String mTransactionNum;
	
	public Document service(Document pInXmlDoc) {
		try {
            if (!getInputData(pInXmlDoc)) {
            	
            } else {
                if (!checkData()) {
                	
                } else {
                    if (!dealData()) {
                    	
                    }
                }
            }
        } catch (Exception ex) {
			point = "E";

        } finally {
        	return CreatResponseXml();
        }
		
		
		
	}

	private Document CreatResponseXml() {
		Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(requestType);
        

        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(point);
        tBody.addContent(tResponseCode);
	
	        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        tHeadData.addContent(tRequestType);
        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
	}

	

	private boolean dealData() {
		try{
			
			String grpContNo = body.getChildText("GRPCONTNO");
			ExeSQL tExeSQl = new ExeSQL();
			String com = "select ManageCom from lccont where GrpContNo='"+grpContNo+"' union select ManageCom from lbcont where GrpContNo='"+grpContNo+"' fetch first 1 row only";		
			String ManageCom = tExeSQl.getOneValue(com);
			String checkSql = "SELECT count(RGTNO) FROM LLAppClaimReason l  where '1471933210000'='1471933210000' and  reasontype='9' and reasoncode='99'  and l.mngcom like '"+ManageCom+"%' and l.RGTNO='"+grpContNo+"'  fetch first 3000 rows only with ur";
			String checkOk = tExeSQl.getOneValue(checkSql);
			int a = Integer.parseInt(checkOk);
			if(a>0){
				point = "1";
			}else{
				point = "0";
			}
		}catch(Exception e){
			mErrors.addOneError("进行团单是否配置查询时出错!");
			point = "E";
			return false;
		}
		return true;
	}

	private boolean checkData() {
		Element root = tInXmlDoc.getRootElement();
		head = root.getChild("HEAD");
		body = root.getChild("BODY");
		requestType = head.getChildText("REQUEST_TYPE");
		mTransactionNum = head.getChildText("TRANSACTION_NUM");
		if(!"GW01".equals(requestType)){
			mErrors.addOneError("请求服务器类型出现错误!");
			point = "E";
			return false;
		}
		if(null==head||null==body){
			mErrors.addOneError("没有得到需要处理的相关信息!");
			point = "E";
			return false;
		}
		
		return true;
	}

	private boolean getInputData(Document pInXmlDoc) {
		this.tInXmlDoc = pInXmlDoc;
		return true;
	}

	public static void main(String args[]){
		try{
			
			LLGWCheckContNo gwbl = new LLGWCheckContNo();
		/*	File file = new File("e:/xuyp.xml");
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
			
			SAX reader = new SAXReader();           
			Document   document = reader.read(in);
			*/
			 SAXBuilder sb=new SAXBuilder(); 
			 Document doc=sb.build("e:/LLGW/llgw.xml");
			 Document response =  gwbl.service(doc);
			 doc2XML(response);
		}catch(Exception e){
			e.getStackTrace();
		}
	}
	
	
	public static void doc2XML(Document doc) throws Exception{
		
		XMLOutputter xmloutputter = new XMLOutputter();
	    OutputStream outputStream;
	    try {
	      outputStream = new FileOutputStream("E:\\LLGW\\document.xml");
	      xmloutputter.output(doc, outputStream);
	      System.out.println("xml文档生成成功！");
	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	}
	
}
