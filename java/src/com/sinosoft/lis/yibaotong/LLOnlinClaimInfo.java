package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 
 * @author XHW
 * 
 * 2018-03-29
 */
public class LLOnlinClaimInfo implements ServiceInterface{
	
	public LLOnlinClaimInfo() {
		
	}

	//
	public CErrors mErrors = new CErrors();
	//
	public VData mResult = new VData();
	//传入报文
	private Document mInXmlDoc;
	//报文类型
	private String mDocType = "";
	//处理标志
	private boolean mDealState = true;
	//返回类型代码
	private String mResponseCode = "1";
	//请求类型
	private String mRequestType = "";
	//错误描述
	private String mErrorMessage = "";
	//交互编码
	private String mTransactionNum = "";
	public SSRS mSSRS = new SSRS();
	//客户姓名
	private String mCUSTOMERNAME="";
	//证件类型
	private String mIDTYPE="";
	//查询的证件号
	private String mIDNO = "";
	//案件集合
	private Element tCASE_LIST = new Element("CASE_LIST");
	
	
	
	/**
	 * @see
	 * com.sinosoft.lis.yibaotong.ServiceInterface#service(org.jdom.Document)
	 */
	public Document service(Document pInXmlDoc) {
		System.out.println("LLOnlinClaimInfo------------------service");
		mInXmlDoc = pInXmlDoc;
		try {
			if(!getInputData(mInXmlDoc)) {
				mDealState = false;
			}else{
				if(!checkData()) {
					mDealState = false;
				}else {
					if(!dealData()) {
						mDealState = false;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mDealState = false;
			mResponseCode = "E";
			buildError("service()", "系统未知错误");
		}finally {
			return createXML();
		}
	}

	/**
	 * 生成返回的报文信息
	 * 
	 * @return Document
	 */
	private Document createXML() {
		if(!mDealState) {
			return createFalseXML();
		}else {
			try {
				return createResultXML();
			}catch(Exception e) {
				buildError("createXML", "生成错误报文");
				return createFalseXML();
			}
		}
	}

	

	private Document createResultXML() {
		System.out.println("LLOnlinClaimInfo------------------createResultXML------返回正确报文");
		
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");
		//创建head部分
		Element tHeadData = new Element("HEAD");
		
		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);
		
		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);
		
		tRootData.addContent(tHeadData);
		
		//创建body部分
		Element tBodyData = new Element("BODY");
		tBodyData.addContent(tCASE_LIST);
		
		tRootData.addContent(tBodyData);
		Document tDocument = new Document(tRootData);
		return tDocument;
	}
	

	private Document createFalseXML() {
		System.out.println("LLOnlinClaimInfo------------------createFalseXML-------返回错误报文");
		
		//创建根节点以及添加根节点属性值
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");
		//创建head部分以及节点和内容
		Element tHeadData = new Element("HEAD");
		
		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);
		//返回的案件处理机构
		Element tResponseCode = new Element("RESPONSE_CODE");
		tResponseCode.setText("1");
		tHeadData.addContent(tResponseCode);
		mErrorMessage = mErrors.getFirstError();
		Element tErrorMessage = new Element("ERROR_MESSAGE");
		tErrorMessage.setText(mErrorMessage);
		tHeadData.addContent(tErrorMessage);
		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);	
		tRootData.addContent(tHeadData);
		Document tDocument = new Document(tRootData);
		return tDocument;
	}

	/**
	 * 处理报文信息
	 * 
	 * @return  boolean
	 */
	private boolean dealData() {
		System.out.println("LLOnlinClaimInfo------------------dealData");
		String sqlname="";
		if(!"".equals(mCUSTOMERNAME)&&mCUSTOMERNAME!=null){
			sqlname = "and llc.customername='"+mCUSTOMERNAME+"' ";
		}
		ExeSQL tExeSQL = new ExeSQL();	
		String dealSQL = "select llc.caseno,llc.rgtstate,llc.rgtdate from llcase llc "
					   + "where llc.idno='"+mIDNO+"' "
					   + "and llc.idtype='"+mIDTYPE+"' "
					   + sqlname 
					   + "order by rgtdate desc with ur";
		
		mSSRS = tExeSQL.execSQL(dealSQL);
		if(mSSRS!=null&&mSSRS.getMaxRow()>0) {
			for(int m = 1 ; m <= mSSRS.getMaxRow(); m++) {
				String mRgtState = mSSRS.GetText(m, 2);
				Element tCaseData = new Element("CASE_DATA");
				if("04".equals(mRgtState) || "05".equals(mRgtState) || "06".equals(mRgtState) || "09".equals(mRgtState)
				|| "10".equals(mRgtState) || "11".equals(mRgtState) || "12".equals(mRgtState) || "16".equals(mRgtState)) {
					 String mmSQL = "select 1 from llclaimdetail lcd " +
							 		"where 1=1  " +
							 		"and not exists (select 1 from ldcode where  codetype = 'ShieldRisk' and code = lcd.riskcode)" +
							 		"and lcd.caseno = '" + mSSRS.GetText(m, 1) + "' " +
							 		"with ur";
					 SSRS mmSSRS = tExeSQL.execSQL(mmSQL);
					 //没有屏蔽的险种进行显示
					 if(mmSSRS.getMaxRow() > 0) {						 
						 Element tCaseNo = new Element("CASENO");
						 Element tRgtState = new Element("RGTSTATE");
						 Element tRgtDate = new Element("RGTDATE");
						 tCaseNo.setText(mSSRS.GetText(m, 1));
						 tRgtState.setText(mSSRS.GetText(m, 2));
						 tRgtDate.setText(mSSRS.GetText(m, 3));
						 tCaseData.addContent(tCaseNo);
						 tCaseData.addContent(tRgtState);
						 tCaseData.addContent(tRgtDate);
						 Element tRemarkDate = new Element("RESERVE_DATA");
						 Element tISProblem = new Element("ISPROBLEM");
						 Element ttState = new Element("STATE");
						 String problemSQL = "select state from LLCASEPROBLEM where 1=1 and caseno = '" + mSSRS.GetText(m, 1) +
									         "' order by makedate desc ,maketime desc with ur ";
						 SSRS problemSSRS = tExeSQL.execSQL(problemSQL);
						 int problemNo = problemSSRS.getMaxRow();
						 if(problemNo>0) {
							tISProblem.setText("1");
							ttState.setText(problemSSRS.GetText(1, 1));
						 }else {
							tISProblem.setText("0");
						 }
						 tCaseData.addContent(tRemarkDate);
						 tCaseData.addContent(tISProblem);
						 tCaseData.addContent(ttState);
						 tCASE_LIST.addContent(tCaseData);
					 }
				}else if("01".equals(mRgtState) || "02".equals(mRgtState) || "03".equals(mRgtState)
						|| "13".equals(mRgtState) || "14".equals(mRgtState)){
					//理算之前的显示线上受理的案件
					String XSSQL = "select 1 from llhospcase where 1=1 "+
									"and hcno in ('WXCLAIM','APPWXCLAIM') " +
								   "and caseno = '" + mSSRS.GetText(m, 1) + "' with ur";
					SSRS XSSSRS = tExeSQL.execSQL(XSSQL);
					if(XSSSRS.getMaxRow()>0) {						
						Element tCaseNo = new Element("CASENO");
						Element tRgtState = new Element("RGTSTATE");
						Element tRgtDate = new Element("RGTDATE");
						tCaseNo.setText(mSSRS.GetText(m, 1));
						tRgtState.setText(mSSRS.GetText(m, 2));
						tRgtDate.setText(mSSRS.GetText(m, 3));
						tCaseData.addContent(tCaseNo);
						tCaseData.addContent(tRgtState);
						tCaseData.addContent(tRgtDate);
						Element tRemarkDate = new Element("RESERVE_DATA");
						Element tISProblem = new Element("ISPROBLEM");
						Element ttState = new Element("STATE");
						String problemSQL = "select state from LLCASEPROBLEM where 1=1 and caseno = '" + mSSRS.GetText(m, 1) +
								"' order by makedate desc ,maketime desc with ur ";
						SSRS problemSSRS = tExeSQL.execSQL(problemSQL);
						int problemNo = problemSSRS.getMaxRow();
						if(problemNo>0) {
							tISProblem.setText("1");
							ttState.setText(problemSSRS.GetText(1, 1));
						}else {
							tISProblem.setText("0");
						}
						tCaseData.addContent(tRemarkDate);
						tCaseData.addContent(tISProblem);
						tCaseData.addContent(ttState);
						tCASE_LIST.addContent(tCaseData);
					}
				}
			}
		}else {
			buildError("dealData()", "查询失败");
			return false;
		}
		return true;
	}

	/**
	 * 校验基本数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		System.out.println("LLOnlinClaimInfo------------------checkData");
		
		if(!"REQUEST".equals(mDocType)) {
			buildError("checkData()", "报文类型【type=" + mDocType + "】错误！");
			return false;
		}
		if(!"OC04".equals(mRequestType)) {
			buildError("checkData()", "【请求类型】的值不存在或者匹配错误！");
			return false;
		}
		if(mTransactionNum==null || "".equals(mTransactionNum) || "null".equals(mTransactionNum) 
				|| mTransactionNum.length() != 30) {
			buildError("checkData()", "【交互编码】有问题！");
			return false;
		}
		if(!"OC04".equals(mTransactionNum.substring(0, 4))) {
			buildError("checkData()", "【交互编码】的请求类型编码错误！");
			return false;
		}
		
		if("".equals(mCUSTOMERNAME) || "null".equals(mCUSTOMERNAME) || mCUSTOMERNAME==null) {
			buildError("checkData()", "被保险人姓名不能为空");
			return false;
		}
		if("".equals(mIDTYPE) || "null".equals(mIDTYPE) || mIDTYPE==null) {
			buildError("checkData()", "证件类型不能为空");
			return false;
		}
		if("".equals(mIDNO) || "null".equals(mIDNO) || mIDNO==null) {
			buildError("checkData()", "证件号码不能为空");
			return false;
		}
		
		return true;
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param mInXml
	 * 				Document
	 * @return boolean
	 */
	private boolean getInputData(Document mInXml) {
		System.out.println("LLOnlinClaimInfo------------------getInputData");
		if(mInXml==null) {
			buildError("getInputData()", "未获取报文");
			return false;
		}
		//获取根节点
		Element tRootData = mInXml.getRootElement();
		//获取根节点type属性的值
		mDocType = tRootData.getAttributeValue("type");
		//获取head内容
		Element tHeadData = tRootData.getChild("HEAD");
		//获取head节点的value
		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
		
		System.out.println("LLOnlinClaimInfo---------------mTransactionNum=" + mTransactionNum);
		
		//获取body部分内容
		Element tInBody = tRootData.getChild("BODY");
		Element tInInSuredData = tInBody.getChild("INSURED_DATA");
		mCUSTOMERNAME = tInInSuredData.getChildTextTrim("CUSTOMERNAME");
		mIDTYPE = tInInSuredData.getChildTextTrim("IDTYPE");
		mIDNO = tInInSuredData.getChildTextTrim("IDNO");
		return true;
	}
	
	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TestInfo";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		Document tInXmlDoc;
		try {
			tInXmlDoc = JdomUtil.build(new FileInputStream(
					"D:/Java/test/weixin/OC04/wx04.xml"), "GBK");
			LLOnlinClaimInfo ttt = new LLOnlinClaimInfo();
			Document tOutXmlDoc = ttt.service(tInXmlDoc);
			JdomUtil.print(tOutXmlDoc);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
