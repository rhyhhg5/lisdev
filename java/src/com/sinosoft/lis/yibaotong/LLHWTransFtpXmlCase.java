package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLHWTransFtpXmlCase {
	/**错误的容器 **/
	public CErrors mErrors = new CErrors();
	
	private String mDealInfo = "";

	private GlobalInput mGI = null; //用户信息

	private MMap map = new MMap();
	
	public LLHWTransFtpXmlCase(){
		
	}
	
	public boolean submitData(){
		
		if(getSubmitMap()==null){
			
			return false;
		}
		
		return true;
	}
	
	
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;

    }
	
	public MMap getSubmitMap(){
		
		if (!dealData())
        {
            return null;
        }

		return map;
	}
	
	private boolean dealData(){
	
        if (!getXls())
        {
            return false;
        }

		return true;
	}
	
	private boolean getXls(){
		//获取根目录
		String tUIRoot= CommonBL.getUIRoot();
		if(tUIRoot==null){
			mErrors.addOneError("没有查到应用目录");
			return false;
		}
		System.out.println(tUIRoot);
		//本地测试
//		tUIRoot="D:\\picc\\ui\\";
		
		 int xmlCount = 0;
		
		ExeSQL tExeSQL = new ExeSQL();
		
		 String tServerIP = tExeSQL
	                .getOneValue("select codename from ldcode where codetype='HWClaim' and code='IP/Port'");
	     String tPort = tExeSQL
	                .getOneValue("select codealias from ldcode where codetype='HWClaim' and code='IP/Port'");
	     String tUsername = tExeSQL
	                .getOneValue("select codename from ldcode where codetype='HWClaim' and code='User/Pass'");
	     String tPassword = tExeSQL
	                .getOneValue("select codealias from ldcode where codetype='HWClaim' and code='User/Pass'");

	     String tFileRgtkPath = tExeSQL
	        		.getOneValue("select codename from ldcode where codetype='HWClaim' and code='XmlPath'");

	     String tFileCasePath = tExeSQL
					.getOneValue("select codealias from ldcode where codetype='HWClaim' and code='XmlPath'");
	     
	      //本地测试
//	     tFileRgtkPath ="temp_lp\\WBClaim\\8631\\RgtXml\\";
//	     tFileCasePath="temp_lp\\WBClaim\\8631\\CaseXml\\";

		 tFileRgtkPath = tUIRoot + tFileRgtkPath;   //核心系统批次存储路径
		 tFileCasePath = tUIRoot + tFileCasePath;   //核心系统案件存储路径
		 
		 FTPTool tFTPTool = new FTPTool(tServerIP,tUsername,tPassword,Integer.parseInt(tPort),"aaActiveMode");
		 try {
			if(!tFTPTool.loginFTP()){
				 CError tError = new CError();
				 tError.moduleName="LLHWTransFtpXmlCase";
				 tError.functionName="getXls";
				 tError.errorMessage=tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				 mErrors.addOneError(tError);
				 System.out.println(tError.errorMessage);	 
				 return false;
			 }
			
			System.out.println("====FTP上海外包案件信息登录成功===");
			
			String tFilePathLocalCore = tFileCasePath+PubFun.getCurrentDate()+"/";//服务器存放案件报文文件的路径 每天生成一个文件夹
			//本地测试
//			 tFilePathLocalCore = tFileCasePath+PubFun.getCurrentDate()+"\\";
			// 本地D盘测试
			//tFilePathLocalCore = "D:\\Java\\test\\HWClaimCase\\";//本地测试使用
			String tFileImportPath = "/01PH/8631/GrpCont/HWClaimCase";//ftp上存放文件的目录
            String tFileImportBackPath = "/01PH/8631/GrpCont/HWClaimCaseBack";//ftp上存放核心返回报文文件的目录
            String tFileImportPathBackUp = "/01PH/8631/GrpCont/HWClaimBackUp/" +PubFun.getCurrentDate()+"/";//备份目录
            String tFilePathout = tFilePathLocalCore+"Back/";   // 核心返回报文存储路径
            //本地测试
//             tFilePathout = tFilePathLocalCore+"Back\\";  
            
            String tDate = PubFun.getCurrentDate();
            tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                  + tDate.substring(8, 10);
            String tComtop="HW02"+tDate;
            String ResultMax = tFilePathout+"upload/" +(tComtop + PubFun1.CreateMaxNo(tComtop, 5))+"/";

            
            File mFileDir = new File(tFilePathLocalCore);
            if(!mFileDir.exists()){
            	if(!mFileDir.mkdirs()){
            		System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
            	}
            }
            
            File tFileDir = new File(tFilePathout);
            if(!tFileDir.exists()){
            	if(!tFileDir.mkdirs()){
            		System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
            	}
            }
            
    		File tResultMaxDir = new File(ResultMax);
            if(!tResultMaxDir.exists()){
            	if(!tResultMaxDir.mkdirs()){
            		System.err.println("创建目录[" + ResultMax.toString() + "]失败！"
    						+ tResultMaxDir.getPath());
            	}
            }

            
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
           String[] tPath= tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
           String tErrorContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
           
           if (tPath == null && null != tErrorContent)
           {
               mErrors.addOneError(tErrorContent);
               return false;
           }
           // 报文存储
           for(int i=0;i<tPath.length;i++){
        	   if(tPath[i]==null){
        		   continue;
        	   }
        	   tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[i]);
        	   tFTPTool.changeWorkingDirectory(tFileImportPath);
        	   tFTPTool.deleteFile(tPath[i]);
           }
           tFTPTool.logoutFTP();
          
           // 报文解析
	           for(int j=0;j<tPath.length;j++){
	        	   if(tPath[j]==null){
	        		   continue;
	        	   }
	        	   xmlCount++;
	        	   FileInputStream fis = null;
	        	   FileOutputStream fos = null;
	        	   FileOutputStream fos2 = null;
	        	   try
	               {
		        	   VData tVData = new VData();
		        	   TransferData tTransferData = new TransferData(); 
		        	   tTransferData.setNameAndValue("FileName", tPath[j]);
		        	   tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);
		        	   
		        	   tVData.add(tTransferData);
		        	   tVData.add(mGI);
		        	   
		        	   Document tInXmlDoc ;
		        	   fis = new FileInputStream(tFilePathLocalCore+tPath[j]);
		        	   tInXmlDoc =JdomUtil.build(fis,"GBK"); 
		        	   LLHWCaseRegister tLLHWCaseRegister = new LLHWCaseRegister();
		        	   Document tOutXmlDoc = tLLHWCaseRegister.service(tInXmlDoc);
		        	   
		        	   System.out.println("打印传入报文============");
		               JdomUtil.print(tInXmlDoc.getRootElement());
		               System.out.println("打印传出报文============");
		               JdomUtil.print(tOutXmlDoc);
		               
		               //创建输出流进行写出报文
		               fos = new FileOutputStream(tFilePathout+tPath[j]);		               
			           JdomUtil.output(tOutXmlDoc,fos);
			           fos2 = new FileOutputStream(ResultMax+tPath[j]);
			           JdomUtil.output(tOutXmlDoc,fos2);			        
	           } catch (Exception ex)
               {
                   System.out.println("批次导入失败: " + tPath[j]);
                   mErrors.addOneError("批次导入失败" + tPath[j]);
                   ex.printStackTrace();
                }finally {
                	try {
                		if(fis != null) {
                			fis.close();
                		}
                	}finally {
                		if(fos != null) {
                			fos.close();
                		}
                		if(fos2 != null){
                			fos2.close();
                		}
                	}
                }
               }
	         //再次连接ftp进行上报
	            try {
	            	FTPTool tFTPTool2 = new FTPTool(tServerIP,tUsername,tPassword,Integer.parseInt(tPort),"aaActiveMode");
	            	if(!tFTPTool2.loginFTP()){
	            	 CError tError = new CError();
	   				 tError.moduleName="LLHWTransFtpXmlCase";
	   				 tError.functionName="getXls";
	   				 tError.errorMessage=tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
	   				 mErrors.addOneError(tError);
	   				 System.out.println(tError.errorMessage);	 
	   				 return false;
		  			 } 
	            	System.out.println("====FTP上海信息，二次登录成功,准备上载返回报文===");
		            if(tFTPTool2.uploadAll(tFileImportBackPath, ResultMax)){
		            	System.out.println("报文上传失败");
		            }
		            tFTPTool2.logoutFTP();
	            }catch(Exception e) {
	            	e.printStackTrace();
					CError tError = new CError();
					tError.moduleName="LLHWTransFtpXmlCase";
			        tError.functionName = "getXls";
			        tError.errorMessage = "回传返回报文过程中出现异常";
			        mErrors.addOneError(tError);
			        System.out.println(tError.errorMessage);
			        return false;
	            }
	           

		} catch (SocketException e) {
	
			e.printStackTrace();
			 CError tError = new CError();
	         tError.moduleName = "LLHWTransFtpXmlCase";
	         tError.functionName = "getXls";
	         tError.errorMessage = "获取连接失败";
	         mErrors.addOneError(tError);
	         System.out.println(tError.errorMessage);
	         return false;
		} catch (IOException e) {
			
			e.printStackTrace();
			CError tError = new CError();
            tError.moduleName = "LLHWTransFtpXmlCase";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
		}
		 
		 
		   if (xmlCount == 0)
	        {
	            mErrors.addOneError("没有需要导入的数据");
	        }
		return true;
	}
	

    public String getDealInfo()
    {
        return mDealInfo;
    }
	
//  建文件夹
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }

	
	public static void main(String[] args) {
		LLHWTransFtpXmlCase t =new LLHWTransFtpXmlCase();
		t.getXls();
	}

}
