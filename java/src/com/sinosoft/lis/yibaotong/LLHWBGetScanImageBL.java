package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.db.ES_DOC_HANDLERDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_HANDLERSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.vschema.ES_DOC_HANDLERSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

/**
 * <p>Title: 理赔上海个险外包影像件</p>
 *
 * <p>Description: 影像件存储</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Zhang
 * @version 1.0
 */
public class LLHWBGetScanImageBL {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mPrtNo = "";

	private TransferData mTransferData;

	private FTPTool tFTPTool;

	private String mPicPath;
	
	private String mFTPLocation; //本地存放ZIP地址

//	private String mZipName;	//本地存放ZIP名字
	
	private String mCaseNo;
	
	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
	private ES_DOC_HANDLERSet SuccES_DOC_HANDLERSet = new ES_DOC_HANDLERSet();
	private PubFun1 pf1 = new PubFun1();
	private PubFun pf =new PubFun();
	private VData mInputData = new VData();
    private MMap map = new MMap();
	
	private String mZipLocalPath;

	private String tFileName;

	private String mFTPPath;

	private VData mVData;
	
	private String mSerialNo;
	
	public boolean submitData(){
		
		if(!getInputData(mVData)){
			return false;
		}
		
		return true;
	}
	
	/***
	 * 提交解压影像件资料
	 * @param args
	 */
	private boolean dealFile() {
		LLHWBDealScanImages tLLHWBDealScanImages = new LLHWBDealScanImages();
		VData tVData = new VData();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("ZipLocalPath", mZipLocalPath);
		transferData.setNameAndValue("SuccES_DOC_HANDLERSet", SuccES_DOC_HANDLERSet);
		tVData.add(transferData);

		try {
			if (!tLLHWBDealScanImages.submitData(tVData)) {
				CError tError = new CError();
				tError.moduleName = "LLHWBGetScanImageBL";
				tError.functionName = "dealFile";
				tError.errorMessage = tLLHWBDealScanImages.mErrors.getFirstError();
				mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	/***
	 * 下载影像件资料
	 * @param args
	 */
	private boolean getFile() {
		String getPath = "select codename ,codealias from ldcode where codetype='LLSHWBScan' and code='FTPPath/LocalPath' ";//若有约定好目录保存FTPPath
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "SHGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp服务器路径未配置";
			mErrors.addOneError(tError);
			return false;
		}
//		mFTPPath = tPathSSRS.GetText(1, 1);
		mFTPPath = mFTPLocation;
		String tUIRoot = CommonBL.getUIRoot();
//		String tUIRoot ="E:\\FTP文件\\";
//		mZipLocalPath = "E:\\FTP文件\\LLSHWBScanzip\\"+PubFun.getCurrentDate()+ "\\";
		mZipLocalPath = tUIRoot+tPathSSRS.GetText(1, 2)+PubFun.getCurrentDate()+ "/";
		boolean downflag = true;
		File mFileDir = new File(mZipLocalPath);
		if(!mFileDir.exists()) {
			if(!mFileDir.mkdirs()) {
				System.out.println("创建目录[" + mZipLocalPath.toString() + "]失败！" + mFileDir.getPath());
			}
		}
		downflag = tFTPTool.downloadFileExists(mFTPPath, mZipLocalPath, tFileName);
		System.out.println("下载成功标识为：" + downflag);
		if (!downflag) {
			System.out.println(tFTPTool.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "SHGetScanFileBL";
			tError.functionName = "getFile";
			tError.errorMessage = tFTPTool.mErrors.getFirstError();
			mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/***
	 * 连接FTP
	 * 
	 * @param args
	 */
	private boolean getConn() {
		String getIPPort = "select codename ,codealias from ldcode where codetype='LLSHWBScan' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='LLSHWBScan' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "LLHWBGetScanImageBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp配置有误";
			mErrors.addOneError(tError);
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 1),
				tUPSSRS.GetText(1, 2), Integer.parseInt(tIPSSRS.GetText(1, 2)));
		try {
			if (!tFTPTool.loginFTP()) {
				CError tError = new CError();
				tError.moduleName = "LLHWBGetScanImageBL";
				tError.functionName = "getConn";
				tError.errorMessage = tFTPTool
						.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				mErrors.addOneError(tError);
				return false;
			}
		} catch (SocketException e) {
			CError tError = new CError();
			tError.moduleName = "LLHWBGetScanImageBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			CError tError = new CError();
			tError.moduleName = "LLHWBGetScanImageBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	
	
	/**
	 * 校验基本数据信息
	 * @param args
	 */
	private boolean getInputData(VData aVData){
		String StrPath = "select CaseNo,ZipPath,FileName,MngCom,Operater,SerialNo from ES_DOC_HANDLER " + 
				 " where 1=1 and state = '2' and pictype = 'HW' " +  
				 " fetch first 25 rows only with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(StrPath);
		int tNo = tSSRS.getMaxRow();
		if(tNo>0) {
			for (int i = 1; i <= tNo; i++) {	
				if (!getConn()) {
					return false;
				}
				mSerialNo = tSSRS.GetText(i, 6);
				mCaseNo = tSSRS.GetText(i, 1);
				tFileName = tSSRS.GetText(i, 3);
				mPicPath = tSSRS.GetText(i, 2);
				mFTPLocation =mPicPath.substring(0,mPicPath.lastIndexOf(tFileName));
				if(!getFile()){
					continue;
				}
				ES_DOC_HANDLERSchema SuccES_DOC_HANDLERSchema = new ES_DOC_HANDLERSchema();
				ES_DOC_HANDLERDB SuccES_DOC_HANDLERDB = new ES_DOC_HANDLERDB();
				SuccES_DOC_HANDLERDB.setCaseNo(mCaseNo);
				SuccES_DOC_HANDLERDB.setSerialNo(mSerialNo);
				if(!SuccES_DOC_HANDLERDB.getInfo()) {
					System.out.println("数据查询失败");
					continue;
				}
				SuccES_DOC_HANDLERSchema = SuccES_DOC_HANDLERDB.getSchema();
				SuccES_DOC_HANDLERSet.add(SuccES_DOC_HANDLERSchema);
			}
			if(!tFTPTool.logoutFTP()) {
				return false;
			}
			if(!dealFile()){
				return false;
			}
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		/*LLCaseSchema mLLCaseSchema = new LLCaseSchema();
		mLLCaseSchema.setCaseNo("C3100180104000013");
		LLHWBGetScanImageBL tSHWBGetFile = new LLHWBGetScanImageBL();
		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("PicPath", "8631/Cont/Image/2018-01-04/15189625.zip");
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.Operator = "cm1102";
		mGlobalInput.AgentCom = "";
		mGlobalInput.ManageCom = "86110000";
		mGlobalInput.ComCode = "86110000";

		VData tVData = new VData();
		tVData.add(mLLCaseSchema);
		tVData.add(transferData1);
		tVData.add(mGlobalInput);
		tSHWBGetFile.submitData(tVData, "");*/
		LLHWBGetScanImageBL ll = new LLHWBGetScanImageBL();
		ll.submitData();
	}

}
