package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 自动立案</p>
 *
 * <p>Description: 生成案件至受理状态</p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zjd
 * @version 1.0
 */
public class LLPadIDGetdutyContStateFlagInfo implements ServiceInterface
{
    
    public LLPadIDGetdutyContStateFlagInfo(){}
    
    public CErrors mErrors = new CErrors();
//    private VData mInputData = new VData();
    public VData mResult = new VData();
//    private MMap mMMap = new MMap();
    private String mErrorMessage = "";  
    
    private String mManageCom="";   //管理机构
//    private String mOperator="";   //操作员
//    /** 社保保单标志 0：非社保；1：社保*/
//    private String mSocialSecurity = "0";
    
//    private List tList = new ArrayList();//返回信息合集
    //报文头信息
//    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private String mResponseCode = "1";     //返回类型代码    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    private String mDealType = "1";         //处理类型 1 实时 2非实时 
    
    //基本信息
    private String mIDNo = "";			

    
    /** 菜单组、菜单组到菜单的相关信息*/
//    private List mInsurednoList = new ArrayList();
    public SSRS mSSRS = new SSRS();
    
//    private String mGRPCONTNO = "";           //团体保单号    
//    private String mPPPEOPLES = "";     //申请人数    
//    private String mTOGETHERFLAG = "";       //给付方式    
//    private String mCASEGETMODE = "";    //赔款领取方式
//    private String mAPPAMNT = "";         //申报金额
//    
//    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();//医保通案件处理表信息
//    
//    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
//    
//    private LLCaseSet mLLCaseSet = new LLCaseSet();
//    private LLCaseSet mbackCaseSet = new LLCaseSet();//案件回退集合
//    
//      private GlobalInput tG=new GlobalInput();
//    
//    private String mReturnMessage = "";//错误提示集合
//    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
//    //批次号
//    private String mRgtNo = "";
//    private String mCurrentDate = PubFun.getCurrentDate();
//    private String mCurrentTime = PubFun.getCurrentTime();
 
    //处理结果标记
    private boolean mDealState=true;
    
    
    private Element mInsuredData;
//    private Element mBaseData;

    public Document service(Document pInXmlDoc)
    {
        
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{
                        //保存信息到LLHospCase
                        /*PubSubmit tPubSubmit = new PubSubmit();
                        VData tVData = new VData();
                        MMap tMMap= new MMap();
                        tMMap.put(mLLHospCaseSet, "INSERT");
                        tVData.add(tMMap);
                        tPubSubmit.submitData(tVData, "INSERT");*/
                    }
                   
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealDatacase(){
    	//TODO Auto-generated method stub
    	//1.受理到检录
//    	if(!dealCaseRegister()){
//    		return false;
//    	}else{
//    		//受理出错,跳出处理
//        	if(!mDealState){
//        		return false;
//        	}else{
//                //保存信息到LLHospCase
//                PubSubmit tPubSubmit = new PubSubmit();
//                VData mVData = new VData();
//                MMap tMMap= new MMap();
//                tMMap.put(mLLHospCaseSet, "INSERT");
//                mVData.add(tMMap);
//                if(!tPubSubmit.submitData(mVData, "")){
//                	 mDealState = false;
//                	 buildError("service()", "数据提交报错");
//                }
//        	}
//    	}
//        //2.检录到理算,包含核赔
//    	if(!batchClaimCal(mRgtNo)){
//    		return false;
//    	}else{
//    		//受理出错,跳出处理
//        	if(!mDealState){
//        		return false;
//        	}
//    	}
//    	//3.审批审定
//    	if(!simpleClaimAudit()){
//    		return false;
//    	}else{
//    		//受理出错,跳出处理
//        	if(!mDealState){
//        		return false;
//        	}
//    	}
//    	
         
        return true;
    }
    
    
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJAccidentAutoRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
    
        
        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mInsuredData = tBodyData.getChild("INSURED_DATA");         
        return true;

    }
    
    
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
            return false;
        }

        if (!"SH01".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误!");
            return false;
        }

        if (!"SH01".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"00000000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的固定值错误!");
            return false;
        }
        
        
        
        return true;

    }
    
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
        
    	mIDNo= mInsuredData.getChildTextTrim("IDNO");
    	if (mIDNo == null || "".equals(mIDNo)) {
            buildError("dealData()", "【被保险人证件号码】为空!");
            return false;
        }


    	ExeSQL tExeSQL = new ExeSQL();
    	String checksql = "select insuredno from lcinsured where idno='" +
    			mIDNo +
    			"' " 
    			;
    	SSRS tSSRS=tExeSQL.execSQL(checksql);
    	if(tSSRS!=null&&tSSRS.getMaxRow()>0){
    		
	    }else{
            buildError("dealData()", "未找到ID为"+mIDNo+"的有效客户信息");
            mDealState = false; 
        }  
//    	System.out.println("查出来的客户号列表==="+mInsurednoList.toString());
    	//对被保人客户号进行校验
    	
			
		String chksql1 ="select distinct cp.GRPCONTNO,cp.CONTNO,cp.INSUREDNAME ,cp.INSUREDNO ,cp.CVALIDATE,cp.ENDDATE ,cp.riskcode,"+
		"(select riskname from lmriskapp where riskcode=cp.riskcode fetch first row only)," +
		"c.getdutycode,c.getdutyname,c.getdutykind," +
		"(select codename from ldcode where codetype='getdutykind' and code=c.getdutykind fetch first row only)," +
		
		"( case when cp.stateflag in('1') then '1' else '0' end )" +
		
		" from lcpol cp,lcget cg,lmdutygetclm c" +
		" where cp.polno=cg.polno" +
		" and cg.getdutycode=c.getdutycode" +
		" and cp.appflag='1' " +
		" and cp.stateflag='1'" +
		" and cg.contno in (select contno from lcinsured where idno='" +
		mIDNo +
		"')";
//		ExeSQL tExeSQL = new ExeSQL();
		mSSRS=tExeSQL.execSQL(chksql1);
		if(mSSRS!=null&&mSSRS.getMaxRow()>0){
    		
		}else{
			buildError("dealData()", "本次理赔申请不在移动理赔范围内，请您到公司柜面申请理赔");
            mDealState = false; 
		}
    	//密码正确性验证
//    	String sqlStr = "select * from lduser where usercode = '" + mUserCode + "' ";  //查询该用户所有信息
    	
//    	 LDUserDB tLDUserDB = new LDUserDB();
//    	 tLDUserDB.setUserCode(mUserCode);
//    	 tLDUserDB.setPassword(encryptPwd);
//         mLDUserSet = tLDUserDB.query();
//         if(mLDUserSet.size()<=0){
//             buildError("dealData()", "用户名或密码不正确!");
//             mDealState = false; 
//             return false;
//         }

 
        return true;
    }
    
    

    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
           try
        {
            return createResultXML();
        }
        catch (Exception e)
        {
            
            e.printStackTrace();
            buildError("createXML()", "生成返回报文错误!");
            return createFalseXML();
        }
        }
    }  
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        System.out.println("LLPADUserLogin--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        

//        Element tResponseCode = new Element("RESPONSE_CODE");
//        tResponseCode.setText(mResponseCode);
//        tHeadData.addContent(tResponseCode);



        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        
      
        Element tBody = new Element("BODY");
        Element tBackData = new Element("BACK_DATA");
        
        
        Element tLoginState = new Element("RESPONSE_CODE");
        
        tLoginState.setText("0");
        tBackData.addContent(tLoginState);
        
        mErrorMessage = mErrors.getFirstError();
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBackData.addContent(tErrorMessage);
        
        
        tBody.addContent(tBackData);
        
        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() throws Exception {
         System.out.println("LLPADUserLogin--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");


         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);


         Element tBodyData = new Element("BODY");
         

          for(int i = 1;i<=mSSRS.getMaxRow();i++)    {   
        	  Element tPolicyData = new Element("POLICY_DATA");
        	  Element tGrpContNo = new Element("GRPCONTNO");
        	  Element tContNo = new Element("CONTNO");
              Element tInsuredName = new Element("INSUREDNAME");
              Element tInsuredNo = new Element("INSUREDNNO");
              Element tCvalidate = new Element("CVALIDATE");
              Element tCInvalidate = new Element("CINVALIDATE");
              Element tRiskCode = new Element("RISKCODE");
              Element tRiskName = new Element("RISKNAME");
              Element tGetDutyCode = new Element("GETDUTYCODE");
              Element tGetdutyname = new Element("GETDUTYNAME");
              Element tGetdutykind = new Element("GETDUTYKIND");
              Element tGetdutykindname = new Element("GETDUTYKINDNAME");
              Element tSTATEFLAG = new Element("STATEFLAG");
             
             tGrpContNo.setText(mSSRS.GetText(i, 1));
	         tContNo.setText(mSSRS.GetText(i, 2));
	         tInsuredName.setText(mSSRS.GetText(i, 3));
	         tInsuredNo.setText(mSSRS.GetText(i, 4));
	         tCvalidate.setText(mSSRS.GetText(i, 5));
	         tCInvalidate.setText(mSSRS.GetText(i, 6));
	         tRiskCode.setText(mSSRS.GetText(i, 7));
	         tRiskName.setText(mSSRS.GetText(i, 8));
	         tGetDutyCode.setText(mSSRS.GetText(i, 9));
	         tGetdutyname.setText(mSSRS.GetText(i, 10));
	         tGetdutykind.setText(mSSRS.GetText(i, 11));
	         tGetdutykindname.setText(mSSRS.GetText(i, 12));
	         tSTATEFLAG.setText(mSSRS.GetText(i, 13));
	         
	         tPolicyData.addContent(tGrpContNo);
	         tPolicyData.addContent(tContNo);
	         tPolicyData.addContent(tInsuredName);
	         tPolicyData.addContent(tInsuredNo);
	         tPolicyData.addContent(tCvalidate);
	         tPolicyData.addContent(tCInvalidate);
	         tPolicyData.addContent(tRiskCode);
	         tPolicyData.addContent(tRiskName);
	         tPolicyData.addContent(tGetDutyCode);
	         tPolicyData.addContent(tGetdutyname);
	         tPolicyData.addContent(tGetdutykind);
	         tPolicyData.addContent(tGetdutykindname);
	         tPolicyData.addContent(tSTATEFLAG);
	         
	         tBodyData.addContent(tPolicyData);
          }
          
         tRootData.addContent(tBodyData);
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLPADUserLogin";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    
    public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
//            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/PADTEST/SH01.xml"), "GBK");
        	tInXmlDoc = JdomUtil.build(new FileInputStream("C:/Users/Administrator/Desktop/工作记录/开发/2017/3400 上海项目/SH01/SH01.xml"), "GBK");
        	LLPadIDGetdutyContStateFlagInfo tBusinessDeal = new LLPadIDGetdutyContStateFlagInfo();
//            LLPadIDGetdutyInfo tBusinessDeal = new LLPadIDGetdutyInfo();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
