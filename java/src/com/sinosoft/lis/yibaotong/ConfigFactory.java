package com.sinosoft.lis.yibaotong;

/**
 * 生成配置文件对象。
 * 提供一种缓存机制，只在第一次调用时加载配置文件生成对应的配置文件实例，
 * 然后缓存该实例，后续调用直接返回该实例。
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

public class ConfigFactory {
	private static Logger cLogger = Logger.getLogger(ConfigFactory.class);
	
	private static Document cServiceConfig = null;	//各交易业务处理类配置
	private static ResourceBundle cPreConfig = null;	//前置机通用配置
	
	/**
	 * 返回serviceConfig.xml(各银行、各交易业务处理类配置)对应实例。
	 * 加载出错，返回null。
	 */
	public static Document getServiceConfig() {
		if (null == cServiceConfig) {
			cLogger.debug("Start load serviceConfig.xml...");
//			InputStream tIs = ConfigFactory.class.getResourceAsStream("/com/sinosoft/lis/yibaotong/serviceConfig.xml");
			InputStream tIs = ConfigFactory.class.getResourceAsStream("/serviceConfig.xml");
			cServiceConfig = JdomUtil.build(tIs);
			try {
				tIs.close();
			} catch (IOException ex) {
				cLogger.error("关闭文件流失败(serviceConfig.xml)！", ex);
			}
			cLogger.debug("End load serviceConfig.xml!");
		}

		return cServiceConfig;
	}
	
	/**
	 * 返回PreConfig.properties(前置机通用配置)对应实例。
	 * 加载出错，返回null。
	 */
	public static ResourceBundle getPreConfig() {
		if (null == cPreConfig) {
			cLogger.debug("Start load PreConfig.properties...");
			cPreConfig = ResourceBundle.getBundle("PreConfig");
			cLogger.debug("End load PreConfig.properties!");
		}

		return cPreConfig;
	}
	
	public static void main(String[] args){
//		String mPostServletURL = ConfigFactory.getPreConfig().getString("PostServletURL");
//		System.out.println("mPostServletURL == " + mPostServletURL);
		
		List tServiceList = ConfigFactory.getServiceConfig().getRootElement().getChildren();
		for (int i=0; i < tServiceList.size(); i++) {
			Element ttService = (Element) tServiceList.get(i);
			System.out.println(ttService.getChildText("class"));
		}
	}
}
