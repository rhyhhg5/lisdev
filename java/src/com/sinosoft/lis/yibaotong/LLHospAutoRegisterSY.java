package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.utility.ExeSQL;

import java.text.DecimalFormat;
import java.io.FileInputStream;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLHospAutoRegisterSY implements ServiceInterface {
    public LLHospAutoRegisterSY() {
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMMap = new MMap();

    //传入报文
    private Document mInXmlDoc;
    //报文类型
    private String mDocType = "";
    //处理标志
    private boolean mDealState = true;

    //返回类型代码
    private String mResponseCode = "1";
    //请求类型
    private String mRequestType = "";
    //错误描述
    private String mErrorMessage = "";
    //交互编码
    private String mTransactionNum = "";
    //交互编码中医院代码
    private String mTranHospCode = "";
    //报文BASE_PART部分
    private Element mBaseData;
    //报文保单部分
    private Element mContList;
    //报文处方部分
    private Element mPrescriptionList;
    //报文费用明细
    private Element mFeeItemList;

    //机构编码
    private String mManageCom = "";
    private String newManageCom="86210100";//#3470 增加支公司
    //客户号
    private String mInsuredNo = "";
    //保单信息
    private LCPolSet mLCPolSet = new LCPolSet();

    //医院编码
    private String mHospitalCode = "";
    //出险日期
    private String mAccDate = "";
    //案件号
    private String mCaseNo = "";
    //案件处理人
    private String mHandler = "";
    //医院信息
    private LDHospitalDB mLDHospitalDB = new LDHospitalDB();
    //案件信息
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    //申请信息
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    //被保险人信息
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    //事件信息
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    //事件关联
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    //疾病信息
    private LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
    //手术信息
    private LLOperationSchema mLLOperationSchema = new LLOperationSchema();
    //意外信息
    private LLAccidentSchema mLLAccidentSchema = new LLAccidentSchema();
    //申请原因
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new
            LLAppClaimReasonSchema();
    //申请材料
    private LLAffixSet mLLAffixSet = new LLAffixSet();
    //账单表
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    //社保账单
    private LLSecurityReceiptSchema mLLSecurityReceiptSchema = new
            LLSecurityReceiptSchema();
    //社保明细
    private LLFeeOtherItemSet mLLFeeOtherItemSet = new LLFeeOtherItemSet();
    //药品
    private LLCaseDrugSet mLLCaseDrugSet = new LLCaseDrugSet();
    //费用明细
    private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();
    //理算险种表
    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    //自动理算表
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();

    //理算数据
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    //理算确认数据
    private LLClaimDetailSet mConfirmLLClaimDetailSet = new LLClaimDetailSet();
    //险种理算数据
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    //案件理算表
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    //应付明细
    private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
    //应付
    private LJSGetSchema mLJSGetSchema = new LJSGetSchema();
    //医保通案件号
    private LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
    //事件关联号
    private String mCaseRelaNo = "";
    //账单号码
    private String mReceiptNo = "";
    
    //已存在账单的案件号
    private String mOldCaseNo = "";

    private String mLimit = "";

    private String mMakeDate = PubFun.getCurrentDate();
    private String mMakeTime = PubFun.getCurrentTime();

    //处理类型 1 实时 2非实时
    private String mDealType = "1";
    private String mGetRate = "";

    public Document service(Document pInXmlDoc) {
        //兄弟别怨我，我也是被逼的，时间太紧，都写到一个类里了
        System.out.println("LLHospAutoRegister--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLHospAutoRegister--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"13".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        mTranHospCode = mTransactionNum.substring(2, 9);

        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        mLDHospitalDB.setHospitCode(mTranHospCode);
        if (!mLDHospitalDB.getInfo()) {
            buildError("checkData()", "医院查询失败");
            return false;
        }
        
        LDCode1DB tLDCode1DB = new LDCode1DB();
        tLDCode1DB.setCodeType("ybt_service");
        tLDCode1DB.setCode("13");
        tLDCode1DB.setCode1(mTranHospCode);
        
        if (!tLDCode1DB.getInfo()) {
        	buildError("checkData()", "医院配置信息查询失败");
            return false;
        }
        
        mManageCom = tLDCode1DB.getComCode();
        if ("".equals(mManageCom) || mManageCom.length() < 4) {
            buildError("checkData()", "医院所属机构错误");
            return false;
        }
        
        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLHospAutoRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        System.out.println("LLHospAutoRegister--TransactionNum:" +
                           mTransactionNum);

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mBaseData = tBodyData.getChild("BASE_PART");
        mContList = tBodyData.getChild("INSURANCE_POLICY_LIST");
        mPrescriptionList = tBodyData.getChild("PRESCRIPTION_LIST");
        mFeeItemList = tBodyData.getChild("FEEITEM_LIST");
        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLHospAutoRegister--dealData");
        //处理保单信息
        if (!dealCont()) {
            return false;
        }

        //案件受理
        if (!caseRegister()) {
            return false;
        }

        //处理账单信息
        if (!dealFee()) {
            return false;
        }

        //准备计算数据
        if (!getCalInfo()) {
            return false;
        }

        //生成案件信息
        if (!dealCase()) {
            return false;
        }

        //理算金额
        if (!claimCal()) {
            return false;
        }

        if (!endCal()) {
            cancelCase();
            return false;
        }

        return true;
    }


    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState && "".equals(mOldCaseNo)) {
            return createFalseXML();
        } else {
        	try {
        	if (!"".equals(mOldCaseNo)) {
        		return createResultXML(mOldCaseNo);
        	} else {
        		return createResultXML(mCaseNo);
        	}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
        DecimalFormat tDF = new DecimalFormat("0.##");

        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        //Head部分
        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText("1");
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText("");
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        //Body部分
        Element tBodyData = new Element("BODY");
        Element tBasePartData = new Element("BASE_PART");

        Element tInsuranceList = new Element("INSURANCE_POLICY_LIST");
        
        double tSumPay = 0.0;

        String tPaySQL =
                "select a.caseno,a.grpcontno,a.contno,a.riskcode,coalesce(sum(a.realpay),0), "
                + " (select riskname from lmrisk where riskcode=a.riskcode)"
                + " From llclaimdetail a where a.caseno='" + aCaseNo +
                "' group by a.caseno,a.grpcontno,a.contno,a.riskcode";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tPaySQL);

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            Element tInsuranceData = new Element("INSURANCE_POLICY_DATA");

            Element tInsuranceNo = new Element("INSURANCE_POLICY_NUMBER");
            if ("00000000000000000000".equals(tSSRS.GetText(i, 2))) {
            	tInsuranceNo.addContent(tSSRS.GetText(i, 3));
            } else {
            	tInsuranceNo.addContent(tSSRS.GetText(i, 2));
            }
            tInsuranceData.addContent(tInsuranceNo);

            Element tRiskCode = new Element("RISK_CODE");
            tRiskCode.addContent(tSSRS.GetText(i, 4));
            tInsuranceData.addContent(tRiskCode);

            Element tRiskName = new Element("RISK_NAME");
            tRiskName.addContent(tSSRS.GetText(i, 6));
            tInsuranceData.addContent(tRiskName);
            
            tSumPay += Double.parseDouble(tSSRS.GetText(i, 5));
            
            Element tRealPay = new Element("TOTAL_PAYMENT_BY_INSURANCE");
            if ("1".equals(mDealType)) {
                tRealPay.addContent(tSSRS.GetText(i, 5));
            } else {
                tRealPay.addContent("0.00");
            }
            tInsuranceData.addContent(tRealPay);

            Element tCompensateInfo = new Element("COMPENSATE_INFORMATION");
            String tPayInfo = "***************** 险种" + tSSRS.GetText(i, 4) +
                              " *****************<![CDATA[\\n]]>"
                              + "理赔号：" + aCaseNo + "<![CDATA[\\n]]>"
                              + "单位:元" + "<![CDATA[\\n]]>"
                              + "总费用(扣减前):" + mLLFeeMainSchema.getSumFee() +
                              "<![CDATA[\\n]]>";
            String tDetailSQL = " select b.polno,b.dutycode,a.getdutycode,a.getdutykind,"
            	              + " b.caserelano,a.getdutyname,a.calcode,b.realpay "
            	              + " from lmdutygetclm a,llclaimdetail b "
            	              + " where a.getdutycode=b.getdutycode and a.getdutykind=b.getdutykind "
            	              + " and b.caseno='"+aCaseNo+"' and b.riskcode='"+tSSRS.GetText(i, 4)
            	              + "' and b.realpay<>0 "
            	              + " order by a.getdutycode,a.getdutykind "
            	              + " with ur";
            System.out.println(tDetailSQL);
            SSRS tDetailSSRS = tExeSQL.execSQL(tDetailSQL);
            for (int j = 1; j<=tDetailSSRS.getMaxRow(); j++) {
            	String tPolNo = tDetailSSRS.GetText(j,1);
            	String tDutyCode = tDetailSSRS.GetText(j,2);
            	String tGetDutyCode = tDetailSSRS.GetText(j,3);
            	String tGetDutyKind = tDetailSSRS.GetText(j,4);
            	String tCaseRelaNo = tDetailSSRS.GetText(j,5);
            	String tCalCode = tDetailSSRS.GetText(j,7);
            	
            	LLElementDetailDB tLLElementDetailDB = new LLElementDetailDB();
            	tLLElementDetailDB.setCaseNo(aCaseNo);
            	tLLElementDetailDB.setCaseRelaNo(tCaseRelaNo);
            	tLLElementDetailDB.setPolNo(tPolNo);
            	tLLElementDetailDB.setDutyCode(tDutyCode);
            	tLLElementDetailDB.setGetDutyCode(tGetDutyCode);
            	tLLElementDetailDB.setGetDutyKind(tGetDutyKind);
            	tLLElementDetailDB.setCalCode(tCalCode);
            	
            	LLElementDetailSet tLLElementDetailSet = tLLElementDetailDB.query();
            	
            	//160307特殊处理
            	if ("160307".equals(tSSRS.GetText(i, 4))) {
            		
            		LLSecurityReceiptDB tLLSecurityReceiptDB = new LLSecurityReceiptDB();
            		tLLSecurityReceiptDB.setCaseNo(aCaseNo);
            		
            		LLSecurityReceiptSet tLLSecurityReceiptSet = tLLSecurityReceiptDB.query();
            		
            		LLSecurityReceiptSchema tLLSecurityReceiptSchema = tLLSecurityReceiptSet.get(1);           		
            		
            		LLElementDetailSchema tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("ApplyAmnt");
            		tLLElementDetailSchema.setElementValue(String.valueOf(tLLSecurityReceiptSchema.getApplyAmnt()));
            		tLLElementDetailSet.add(tLLElementDetailSchema);
            		
            		tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("PlanAmnt");
            		tLLElementDetailSchema.setElementValue(String.valueOf(tLLSecurityReceiptSchema.getPlanFee()));
            		tLLElementDetailSet.add(tLLElementDetailSchema);
            		
            		tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("SelfAmnt");
            		tLLElementDetailSchema.setElementValue(String.valueOf(tLLSecurityReceiptSchema.getSelfAmnt()));
            		tLLElementDetailSet.add(tLLElementDetailSchema);
            		
            		tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("SelfPay2");
            		tLLElementDetailSchema.setElementValue(String.valueOf(tLLSecurityReceiptSchema.getSelfPay2()));
            		tLLElementDetailSet.add(tLLElementDetailSchema);
            		
            		tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("GetLimitLine");
            		tLLElementDetailSchema.setElementValue(String.valueOf(tLLSecurityReceiptSchema.getGetLimit()));
            		tLLElementDetailSet.add(tLLElementDetailSchema);
            		
            		tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("PayRate");
            		tLLElementDetailSchema.setElementValue(String.valueOf(mGetRate));
            		tLLElementDetailSet.add(tLLElementDetailSchema);
            		
            		tLLElementDetailSchema = new LLElementDetailSchema();
            		tLLElementDetailSchema.setCalCode(tCalCode);
            		tLLElementDetailSchema.setElementCode("MidAmnt");
            		tLLElementDetailSchema.setElementValue(String.valueOf(tLLSecurityReceiptSchema.getMidAmnt()));
            		tLLElementDetailSet.add(tLLElementDetailSchema);            		
            		            		
            	}
            	
            	tPayInfo = tPayInfo + tDetailSSRS.GetText(j,6) + ":"
            	         + parseClaimResult(tLLElementDetailSet) + "<![CDATA[\\n]]>";
            }
            
//                              + "基本医疗统筹支付:" +
//                              mLLSecurityReceiptSchema.getPlanFee()
//                              + "<![CDATA[\\n]]>"
//                              + "自费费用：" + mLLSecurityReceiptSchema.getSelfAmnt()
//                              + "<![CDATA[\\n]]>"
//                              + "乙类药、部分支付需个人承担部分：" +
//                              mLLSecurityReceiptSchema.getSelfPay2()
//                              + "<![CDATA[\\n]]>"
//                              + "起付线：" + mLLSecurityReceiptSchema.getGetLimit()
//                              + "<![CDATA[\\n]]>"
//                              + "报销比例：" + mGetRate
//                              + "<![CDATA[\\n]]>"
//                              + "中段金额：基本医疗统筹支付（" +
//                              mLLSecurityReceiptSchema.getPlanFee() +
//                              "）/报销比例（" + mGetRate + "）*（1-报销比例（" + mGetRate +
//                              "））="
//                              + mLLSecurityReceiptSchema.getMidAmnt()
//                              + "<![CDATA[\\n]]>";

//            String tMidPay =
//                    "select coalesce(sum(a.realpay),0),a.outdutyamnt,a.outdutyrate"
//                    + " from llclaimdetail a where a.caseno = '" + mCaseNo
//                    +
//                    "' and a.dutycode='629002' group by a.outdutyamnt,a.outdutyrate with ur";
//            SSRS tMidSSRS = tExeSQL.execSQL(tMidPay);
//            if (tMidSSRS.getMaxRow() > 0) {
//                double tMidRealPay = Arith.round(Double.parseDouble(tMidSSRS.
//                        GetText(1, 1)), 2);
//                if (tMidRealPay > 0) {
//                    tPayInfo += "中段赔付金额：（中段金额（" +
//                            mLLSecurityReceiptSchema.getMidAmnt() +
//                            "）-免赔额（" + tMidSSRS.GetText(1, 2) + "））*赔付比例（" +
//                            tMidSSRS.GetText(1, 3) + "）=" +
//                            tMidSSRS.GetText(1, 1) + "<![CDATA[\\n]]>";
//
//                }
//            }
//            if (mLLSecurityReceiptSchema.getHighAmnt1() > 0) {
//                tPayInfo += "高段金额：总费用（" + mLLSecurityReceiptSchema.getApplyAmnt() +
//                        "）-基本医疗统筹支付(" + mLLSecurityReceiptSchema.getPlanFee() +
//                        ")-起付线（" + mLLSecurityReceiptSchema.getGetLimit() +
//                        "）-中段金额（" + mLLSecurityReceiptSchema.getMidAmnt() +
//                        "）-自费费用（" + mLLSecurityReceiptSchema.getSelfAmnt() +
//                        "）-乙类药、部分支付需个人承担部分(" +
//                        mLLSecurityReceiptSchema.getSelfPay2() + ")=" +
//                        mLLSecurityReceiptSchema.getHighAmnt1() +
//                        "<![CDATA[\\n]]>";
//                String tHighAmnt1Pay =
//                        "select coalesce(sum(a.realpay),0),a.outdutyamnt,a.outdutyrate"
//                        + " from llclaimdetail a where a.caseno = '" + mCaseNo
//                        +
//                        "' and a.dutycode='629003' group by a.outdutyamnt,a.outdutyrate with ur";
//                SSRS tHighAmnt1SSRS = tExeSQL.execSQL(tHighAmnt1Pay);
//                if (tHighAmnt1SSRS.getMaxRow() > 0) {
//                    double tHighRealPay = Arith.round(Double.parseDouble(
//                            tHighAmnt1SSRS.
//                            GetText(1, 1)), 2);
//                    if (tHighRealPay > 0) {
//                        tPayInfo += "高段赔付金额：（高段金额（" +
//                                mLLSecurityReceiptSchema.getHighAmnt2() +
//                                "）-免赔额（" + tHighAmnt1SSRS.GetText(1, 2) +
//                                "））*赔付比例（" +
//                                tHighAmnt1SSRS.GetText(1, 3) + "）=" +
//                                tHighAmnt1SSRS.GetText(1, 1) +
//                                "<![CDATA[\\n]]>";
//                    }
//                }
//            }
//            tPayInfo += " 本险种理赔金额:" + tSSRS.GetText(i, 5);
            if ("1".equals(mDealType)) {
                tCompensateInfo.addContent(tPayInfo);
            } else {
                tCompensateInfo.addContent("");
            }
            tInsuranceData.addContent(tCompensateInfo);
            tInsuranceList.addContent(tInsuranceData);
        }
        
        Element tTotalPayMent = new Element("TOTAL_PAYMENT");
        
        String tSumRealPay = tDF.format(Arith.round(tSumPay, 2));
        if ("1".equals(mDealType)) {
            tTotalPayMent.setText(tSumRealPay);
        } else {
            tTotalPayMent.setText("0.00");
        }
        tBasePartData.addContent(tTotalPayMent);

        Element tCaseNo = new Element("CASE_NUMBER");
        tCaseNo.setText(aCaseNo);
        tBasePartData.addContent(tCaseNo);

        tBodyData.addContent(tBasePartData);

        tBodyData.addContent(tInsuranceList);

        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospAutoRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 解析校验保单部分
     * @return boolean
     */
    private boolean dealCont() throws Exception {
        System.out.println("LLHospAutoRegister--dealCont");
        List tContList = new ArrayList();
        tContList = (List) mContList.getChildren();

        //循环校验每个保单的数据,将正常保单计入mLCPolSet
        out:
        for (int i = 0; i < tContList.size(); i++) {
            Element tContData = (Element) tContList.get(i);

            String tInsuredNo = tContData.getChildText("INSUREDNO");
            if (!"".equals(mInsuredNo)) {
                if (!mInsuredNo.equals(tInsuredNo)) {
                    buildError("dealCont", "一次只能处理一个客户的信息");
                    return false;
                }
            } else {
                mInsuredNo = tInsuredNo;
            }

            if (mInsuredNo == null || "".equals(mInsuredNo)) {
                buildError("dealCont", "【客户号】的值不能为空");
                return false;
            }

            //用客户号校验该客户是否有正在处理的保全项目,返回的是工单号，而且校验的是全部保单，考虑以后是否按照保单校验
            if (!"".equals(LLCaseCommon.checkPerson(mInsuredNo))) {
                buildError("dealCont", "该客户正在进行保全处理，不能进行理赔");
                return false;
            }

            String tGrpContNo = tContData.getChildText(
                    "INSURANCE_POLICY_NUMBER");
            if (tGrpContNo == null || "".equals(tGrpContNo)) {
                buildError("dealCont", "【保单号】的值不能为空");
                return false;
            }

            String tContNo = tContData.getChildText(
                    "SUB_INSURANCE_POLICY_NUMBER");
            if (tContNo == null || "".equals(tContNo)) {
                buildError("dealCont", "【分单号】的值不能为空");
                return false;
            }
            
            String tRiskCode = tContData.getChildText("RISK_CODE");
            if (tRiskCode == null || "".equals(tRiskCode)) {
                buildError("dealCont", "【险种编码】的值不能为空");
                return false;
            }

            String tPolSQL = "select a.polno from lcpol a where (a.managecom like '" +
		                    mManageCom + "%' or a.managecom like'"+newManageCom+"%') and a.contno='" + tContNo +
		                    "' and a.riskcode='" + tRiskCode +
		                    "' and a.insuredno='" +
		                    mInsuredNo +
		                    "' and a.appflag='1' " //and a.stateflag='1' "
		                    +
		                    " and exists (select 1 from llhospcomright where riskcode=a.riskcode and hospitcode='" +
		                    mTranHospCode +
		                    "' and requesttype='13' and state='0')"
		                    ;
            
            System.out.println(tPolSQL);
            
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tPolSSRS = tExeSQL.execSQL(tPolSQL);
            
            if (tPolSSRS.getMaxRow() !=1 ) {
            	buildError("dealCont", "分单" + tContNo + "下险种"+ tRiskCode +"查询失败");
                return false;
            }
            
//            LCPolBL tLCPolDB = new LCPolBL();
//            LCPolSet tLCPolSet = tLCPolDB.executeQuery(tPolSQL);
//            if (tLCPolSet.size() <= 0) {
//                buildError("dealCont", "分单" + tContNo + "查询失败");
//                return false;
//            }
            
            String tPolNo = tPolSSRS.GetText(1, 1);
            
            if ("".equals(tPolNo)) {
            	buildError("dealCont", "分单" + tContNo + "下险种"+ tRiskCode +"查询失败");
                return false;
            }
            
            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(tPolNo);
            
            if (!tLCPolBL.getInfo()) {
            	buildError("dealCont", "分单" + tContNo + "下险种"+ tRiskCode +"查询失败");
                return false;
            }
            
            LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
            
            String tPolNoNew = tLCPolSchema.getPolNo();

            //剔除重复的保单
           	for (int j = 1; j <= mLCPolSet.size(); j++) {
           		String tPolNoOld = mLCPolSet.get(j).getPolNo();
                if (tPolNoNew.equals(tPolNoOld)) {
                	continue out;
                }
            }
           	mLCPolSet.add(tLCPolSchema);
        }

        return true;
    }


    /**
     * 解析受理信息
     * @return boolean
     */
    private boolean caseRegister() throws Exception {
        System.out.println("LLHospAutoRegister--caseRegister");
        mHospitalCode = mBaseData.getChildText("HOSPITAL_NUMBER");
        if (!mTranHospCode.equals(mHospitalCode)) {
            buildError("caseRegister", "【医院编码】的值与【交互编码】不符");
            return false;
        }

        mAccDate = mBaseData.getChildText("ACCDATE");
        if (mAccDate == null || "".equals(mAccDate)) {
            buildError("caseRegister", "【出险日期】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(mAccDate)) {
            buildError("caseRegister", "【出险日期】格式错误");
            return false;
        }

        mDealType = mBaseData.getChildText("DEALTYPE");

        if (mDealType == null || "".equals(mDealType)) {
            buildError("caseRegister", "【处理类型】的值不能为空");
            return false;
        }

        if (!"1".equals(mDealType) && !"2".equals(mDealType)) {
            buildError("caseRegister", "【处理类型】的值错误");
            return false;
        }

//        mGetRate = mBaseData.getChildText("GETRATE");
//        if (mGetRate == null || "".equals(mGetRate)) {
//            buildError("caseRegister", "【医保报销比例】的值不能为空");
//            return false;
//        } 
        
        mReceiptNo = mBaseData.getChildText("RECEIPTNO");
        if (mReceiptNo == null || "".equals(mReceiptNo)) {
            buildError("dealFee", "【账单号码】的值不能为空");
            return false;
        }
        
        String tReceiptNoSQL = "select a.caseno from llhospcase a,llfeemain b "
        	                 + " where a.caseno=b.caseno and a.confirmstate='0' and a.dealtype='1' "
        	                 + " and a.hospitcode='"+ mTranHospCode 
        	                 + "' and b.receiptno='"+ mReceiptNo +"'";
        System.out.println(tReceiptNoSQL);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tReceiptSSRS = tExeSQL.execSQL(tReceiptNoSQL);
        
        if (tReceiptSSRS.getMaxRow() > 0) {
        	mOldCaseNo = tReceiptSSRS.GetText(1, 1);
        	return false;
        }
        
        LLCaseDB tLLCaseDB = new LLCaseDB();
        String tCaseSQl = "select * from llcase where rgtstate not in ('09','11','12','14') and customerno='" +
                          mInsuredNo + "'";
        LLCaseSet tLLCaseSet = tLLCaseDB.executeQuery(tCaseSQl);
        if (tLLCaseSet.size() > 0) {
            buildError("dealCont", "客户" + mInsuredNo + "有未处理完的理赔案件，不能进行实时结算。");
            return false;
        }

        //不取传送过来的处理人了，取核心自己配置的处理人  #1557  2013-10-14
//        mHandler = mBaseData.getChildText("STAFF_NUMBER");
        String mHandlerSQl = "select Code from ldcode2 where codetype = 'SYybt_Handler'";
        ExeSQL exesql = new ExeSQL();
        mHandler = exesql.getOneValue(mHandlerSQl);
        
        
        if (mHandler == null || "".equals(mHandler)) {
            buildError("caseRegister", "【案件处理人】的值不能为空");
            return false;
        }

        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(mHandler);
        tLLClaimUserDB.setClaimDeal("1");
        tLLClaimUserDB.setHandleFlag("1");
        tLLClaimUserDB.setStateFlag("1");
       // tLLClaimUserDB.setComCode(mManageCom);
       // tLLClaimUserDB.setComCode("86");
        System.out.println("理赔自动理算--------------------------------"+tLLClaimUserDB.getComCode()+"-------------------------------");
        LLClaimUserSet tLLClaimUserSet = tLLClaimUserDB.query();

        if (tLLClaimUserSet.size() <= 0) {
            buildError("caseRegister", "案件处理人查询失败或没有权限");
            return false;
        }

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(mInsuredNo);
        LCInsuredSet tLCinsuredSet = tLCInsuredDB.query();
        if (tLCinsuredSet.size() < 1) {
            buildError("caseRegister", "被保险人查询失败");
            return false;
        }
        //只需要取一个就可以，只要基本信息
        mLCInsuredSchema = tLCinsuredSet.get(1);

        mLimit = PubFun.getNoLimit(mManageCom);
        mCaseNo = PubFun1.CreateMaxNo("CaseNo", mLimit);

        if (!getCaseInfo()) {
            return false;
        }

        mCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", mLimit);

        if (!getSubInfo()) {
            return false;
        }

        if (!getAppReasonInfo()) {
            return false;
        }

        return true;
    }


    /**
     * 解析账单信息
     * @return boolean
     */
    private boolean dealFee() throws Exception {
        System.out.println("LLHospAutoRegister--dealFee");
        String tFeeType = mBaseData.getChildText("FEETYPE");
        if (!"1".equals(tFeeType) && !"2".equals(tFeeType)) {
            buildError("dealFee", "【账单类型】的值不存在");
            return false;
        }

        String tInHosNo = "";
        if ("2".equals(tFeeType)) {
            tInHosNo = mBaseData.getChildText("PATIENT_NUMBER");
        }

        //总费用
        String tSumFee = mBaseData.getChildText("TOTAL_MEDICAL_COST");
        if (tSumFee == null || "".equals(tSumFee)) {
            buildError("dealFee", "【总费用】的值不能为空");
            return false;
        }
        double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee), 2);

        if (tSumFeeValue <= 0) {
            buildError("dealFee", "【总费用】的值必须大于0");
            return false;
        }
        
        String tMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
        mLLFeeMainSchema.setMainFeeNo(tMainFeeNo);
        mLLFeeMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
        mLLFeeMainSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
        mLLFeeMainSchema.setHospitalCode(mLDHospitalDB.getHospitCode());
        mLLFeeMainSchema.setHospitalName(mLDHospitalDB.getHospitName());
        mLLFeeMainSchema.setHosGrade(mLDHospitalDB.getLevelCode());
        mLLFeeMainSchema.setReceiptNo(mReceiptNo);

        if (!"".equals(tInHosNo) && tInHosNo != null) {
            mLLFeeMainSchema.setInHosNo(tInHosNo);
        }

        mLLFeeMainSchema.setFeeType(tFeeType);
        //账单类型默认简易社保结算
        mLLFeeMainSchema.setFeeAtti("4");
        mLLFeeMainSchema.setFeeDate(mMakeDate);
        mLLFeeMainSchema.setHospStartDate(mLLSubReportSchema.getInHospitalDate());
        mLLFeeMainSchema.setHospEndDate(mLLSubReportSchema.getOutHospitalDate());
        //计算实际住院天数
        int tRealHospDay = PubFun.calInterval(mLLFeeMainSchema.getHospStartDate(),
                                              mLLFeeMainSchema.getHospEndDate(),
                                              "D") + 1;
        mLLFeeMainSchema.setRealHospDate(tRealHospDay);
        mLLFeeMainSchema.setAge((int) mLLCaseSchema.getCustomerAge());
        mLLFeeMainSchema.setSecurityNo(mLCInsuredSchema.getOthIDNo());
        mLLFeeMainSchema.setInsuredStat(mLCInsuredSchema.getInsuredStat());

        mLLFeeMainSchema.setSumFee(tSumFeeValue);

        mLLFeeMainSchema.setOperator(mHandler);
        mLLFeeMainSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLFeeMainSchema.setMakeDate(mMakeDate);
        mLLFeeMainSchema.setMakeTime(mMakeTime);
        mLLFeeMainSchema.setModifyDate(mMakeDate);
        mLLFeeMainSchema.setModifyTime(mMakeTime);
        
        if ("4".equals(mLLFeeMainSchema.getFeeAtti())) {
            if (!getSecurityInfo()) {
                return false;
            }

            mLLFeeMainSchema.setRefuseAmnt(mLLSecurityReceiptSchema.getSelfPay2());
            mLLFeeMainSchema.setSelfAmnt(mLLSecurityReceiptSchema.getSelfAmnt());
        }
        // #3544  医保类型  计算被保人年龄 
        int tAge= getAgeDate(mLCInsuredSchema.getBirthday(),mAccDate);
        if(tAge<18){ 
        	mLLFeeMainSchema.setMedicareType("1"); //城镇居民
        }else{
        	mLLFeeMainSchema.setMedicareType("2");  //城镇职工
        }

        if (!getCaseDrug()) {
            return false;
        }

        if (!getCaseReceipt()) {
            return false;
        }

        return true;
    }

    /**
     * 准备理算数据
     * @return boolean
     * @throws Exception
     */
    private boolean getCalInfo() throws Exception {
        System.out.println("LLHospAutoRegister--getCalInfo");
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        if (mLCPolSet.size() < 1) {
            buildError("getCalInfo", "没有可以赔付的保单");
            return false;
        }

        for (int i = 1; i <= mLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = mLCPolSet.get(i);

            //出险日期、入院日期、出院日期必须都在保单年度里
            if (!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(), mAccDate)) {
                continue;
            }

//            if (!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(),
//                                             mLLFeeMainSchema.getHospStartDate())) {
//                continue;
//            }
//
//            if (!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(),
//                                             mLLFeeMainSchema.getHospEndDate())) {
//                continue;
//            }

            LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();

            Reflections tReflections = new Reflections();
            tReflections.transFields(tLLCasePolicySchema, tLCPolSchema);

            tLLCasePolicySchema.setCaseNo(mLLCaseSchema.getCaseNo());
            tLLCasePolicySchema.setRgtNo(mLLCaseSchema.getRgtNo());
            tLLCasePolicySchema.setCaseRelaNo(mCaseRelaNo);
            tLLCasePolicySchema.setPolType("1");
            tLLCasePolicySchema.setCasePolType("0");
            tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
            tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion());
            tLLCasePolicySchema.setOperator(mLLCaseSchema.getHandler());
            tLLCasePolicySchema.setMngCom(mLLCaseSchema.getMngCom());
            tLLCasePolicySchema.setMakeDate(mMakeDate);
            tLLCasePolicySchema.setMakeTime(mMakeTime);
            tLLCasePolicySchema.setModifyDate(mMakeDate);
            tLLCasePolicySchema.setModifyTime(mMakeTime);

            LLToClaimDutySet tLLToClaimDutySet = new LLToClaimDutySet();

            //获取可以赔付的责任
            LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
            tLLHospComRightDB.setRiskCode(tLCPolSchema.getRiskCode());
            tLLHospComRightDB.setHospitCode(mTranHospCode);
            tLLHospComRightDB.setRequestType(mRequestType);
            tLLHospComRightDB.setState("0");
            LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
            tLLHospComRightSet = tLLHospComRightDB.query();

            if (tLLHospComRightSet.size() < 1) {
                buildError("getCalInfo()",
                           "该分单" + tLCPolSchema.getContNo() + "险种不能进行实时赔付");
                return false;
            }

            for (int j = 1; j <= tLLHospComRightSet.size(); j++) {
                LLHospComRightSchema tLLHospComRightSchema = tLLHospComRightSet.
                        get(j);
                LCGetDB tLCGetDB = new LCGetDB();
                tLCGetDB.setPolNo(tLCPolSchema.getPolNo());
                tLCGetDB.setDutyCode(tLLHospComRightSchema.getDutyCode());
                LCGetSet tLCGetSet = tLCGetDB.query();

                if (tLCGetSet.size() < 1) {
                    continue;
                }

                for (int m = 1; m <= tLCGetSet.size(); m++) {
                    LCGetSchema tLCGetSchema = tLCGetSet.get(m);
                    String tGetDutyKind = "100";
                    String tClaimReason = "02";
                    if (mLLFeeMainSchema.getFeeType().equals("1")) {
                    	tClaimReason = "01";
                    }
                    
                    
                    String tGetDutyKindSQL = "select a.getdutykind from LMDutyGetClm a,LDGetDutyKind b "
                    	                   + " where a.getdutykind=b.getdutykind and a.getdutycode='"
                    	                   + tLCGetSchema.getGetDutyCode()
                    	                   + "' and b.causecode='"+ mLLSubReportSchema.getAccidentType()
                    	                   + "' and b.claimreason='"+ tClaimReason +"'";
                    
                    ExeSQL tExeSQl = new ExeSQL();
                    System.out.println(tGetDutyKindSQL);
                    SSRS tGetDutyKindSSRS = tExeSQl.execSQL(tGetDutyKindSQL);
                    
                    if (tGetDutyKindSSRS.getMaxRow() <= 0) {
                    	continue;                    	
                    }
                    
                    tGetDutyKind = tGetDutyKindSSRS.GetText(1, 1);
//                    LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
//                    tLMDutyGetClmDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
//                    tLMDutyGetClmDB.setGetDutyKind(tGetDutyKind);
//
//                    if (!tLMDutyGetClmDB.getInfo()) {
//                        continue;
//                    }

                    LLToClaimDutySchema tLLToClaimDutySchema = new
                            LLToClaimDutySchema();

                    Reflections tReflection = new Reflections();
                    tReflection.transFields(tLLToClaimDutySchema, tLCPolSchema);

                    tLLToClaimDutySchema.setCaseNo(mLLCaseSchema.getCaseNo());
                    tLLToClaimDutySchema.setCaseRelaNo(mCaseRelaNo);
                    tLLToClaimDutySchema.setSubRptNo(mLLCaseRelaSchema.
                            getSubRptNo());
                    tLLToClaimDutySchema.setGetDutyCode(tLCGetSchema.
                            getGetDutyCode());
                    tLLToClaimDutySchema.setDutyCode(tLCGetSchema.getDutyCode());
                    tLLToClaimDutySchema.setGetDutyKind(tGetDutyKind);
                    //理算次数
                    tLLToClaimDutySchema.setClaimCount(0);

                    tLLToClaimDutySchema.setPolMngCom(tLCPolSchema.getManageCom());
                    tLLToClaimDutySchema.setRiskVer(tLCPolSchema.getRiskVersion());
                    tLLToClaimDutySet.add(tLLToClaimDutySchema);
                }
            }

            if (tLLToClaimDutySet.size() < 1) {
                buildError("getCalInfo", "没有可以匹配的责任");
                return false;
            }

            mLLToClaimDutySet.add(tLLToClaimDutySet);
            mLLCasePolicySet.add(tLLCasePolicySchema);
        }

        if (mLLToClaimDutySet.size() < 1 || mLLCasePolicySet.size() < 1) {
            buildError("getCalInfo", "没有可以理算的保单");
            return false;
        }

        return true;
    }

    /**
     * 生成案件信息
     * @return boolean
     */
    private boolean getCaseInfo() throws Exception {
        System.out.println("LLHospAutoRegister--getCaseInfo");
        mLLCaseSchema.setCaseNo(mCaseNo);
        mLLCaseSchema.setRgtNo(mCaseNo);
        //申请类案件
        mLLCaseSchema.setRgtType("1");
        //案件处理到理算确认
        mLLCaseSchema.setRgtState("04");
        mLLCaseSchema.setCustomerNo(mInsuredNo);
        mLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
        //账单状态未录入
        mLLCaseSchema.setReceiptFlag("3");
        //未调查
        mLLCaseSchema.setSurveyFlag("0");
        mLLCaseSchema.setRgtDate(mMakeDate);

        String tCustState = mBaseData.getChildText("CUSTOMERSTATE");
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llcuststatus");
        tLDCodeDB.setCode(tCustState);
        if (!tLDCodeDB.getInfo()) {
            buildError("getSubInfo", "【离院状态】的值不存在");
            return false;
        }
        mLLCaseSchema.setCustState(tCustState);

        mLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());

        int tAge = LLCaseCommon.calAge(mLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerAge(tAge);

        mLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
        mLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
        mLLCaseSchema.setHandler(mHandler);
        mLLCaseSchema.setDealer(mHandler);

        if (mManageCom.length() == 4) {
            mLLCaseSchema.setMngCom(mManageCom + "0000");
        } else {
            mLLCaseSchema.setMngCom(mManageCom);
        }

        mLLCaseSchema.setOperator(mHandler);
        mLLCaseSchema.setMakeDate(mMakeDate);
        mLLCaseSchema.setMakeTime(mMakeTime);
        mLLCaseSchema.setModifyDate(mMakeDate);
        mLLCaseSchema.setModifyTime(mMakeTime);

        //医保通受理方式
        mLLCaseSchema.setCaseProp("10");

        mLLCaseSchema.setRigister(mHandler);
        mLLCaseSchema.setClaimer(mHandler);
        mLLCaseSchema.setOtherIDType(mLCInsuredSchema.getOthIDType());
        mLLCaseSchema.setOtherIDNo(mLCInsuredSchema.getOthIDNo());

        mLLCaseSchema.setAccidentDate(mAccDate);

        //申请表信息
        mLLRegisterSchema.setRgtNo(mCaseNo);
        mLLRegisterSchema.setRgtState("13");
        //客户号
        mLLRegisterSchema.setRgtObj("1");
        mLLRegisterSchema.setRgtObjNo(mInsuredNo);
        //默认受理方式医保通
        mLLRegisterSchema.setRgtType("6");
        //个案受理
        mLLRegisterSchema.setRgtClass("0");
        mLLRegisterSchema.setRgtantName(mLLCaseSchema.getCustomerName());
        //默认本人申请
        mLLRegisterSchema.setRelation("00");
        mLLRegisterSchema.setCustomerNo(mInsuredNo);
        mLLRegisterSchema.setRgtDate(mMakeDate);
        mLLRegisterSchema.setHandler(mHandler);
        mLLRegisterSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLRegisterSchema.setIDType(mLLCaseSchema.getIDType());
        mLLRegisterSchema.setIDNo(mLLCaseSchema.getIDNo());
        mLLRegisterSchema.setOperator(mHandler);
        mLLRegisterSchema.setMakeDate(mMakeDate);
        mLLRegisterSchema.setMakeTime(mMakeTime);
        mLLRegisterSchema.setModifyDate(mMakeDate);
        mLLRegisterSchema.setModifyTime(mMakeTime);

        return true;
    }

    /**
     * 生成事件信息、疾病信息、手术信息
     * @return boolean
     * @throws Exception
     */
    private boolean getSubInfo() throws Exception {
        System.out.println("LLHospAutoRegister--getSubInfo");
        //疾病信息
        String tDiseaseCode = mBaseData.getChildText("DISEASECODE");
        if ("".equals(tDiseaseCode) || "null".equals(tDiseaseCode)) {
            buildError("getSubInfo", "【疾病编码】的值不能为空");
            return false;
        }

        LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
        tLDDiseaseDB.setICDCode(tDiseaseCode);
        if (!tLDDiseaseDB.getInfo()) {
            buildError("getSubInfo", "【疾病编码】不存在");
            return false;
        }

        String tCureNo = PubFun1.CreateMaxNo("CURENO", mLimit);
        mLLCaseCureSchema.setSerialNo(tCureNo);
        mLLCaseCureSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLCaseCureSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
        mLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());
        mLLCaseCureSchema.setHospitalCode(mLDHospitalDB.getHospitCode());
        mLLCaseCureSchema.setHospitalName(mLDHospitalDB.getHospitName());
        mLLCaseCureSchema.setCaseNo(mCaseNo);
        mLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo);
        mLLCaseCureSchema.setSeriousFlag("0");
        mLLCaseCureSchema.setOperator(mHandler);
        mLLCaseCureSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLCaseCureSchema.setMakeDate(mMakeDate);
        mLLCaseCureSchema.setMakeTime(mMakeTime);
        mLLCaseCureSchema.setModifyDate(mMakeDate);
        mLLCaseCureSchema.setModifyTime(mMakeTime);

        //手术信息
        String tOperationCode = mBaseData.getChildText("OPERATIONCODE");
        if (!"".equals(tOperationCode) && !"null".equals(tOperationCode)) {
            LDICDOPSDB tLDICDOPSDB = new LDICDOPSDB();
            tLDICDOPSDB.setICDOPSCode(tOperationCode);
            if (!tLDICDOPSDB.getInfo()) {
                buildError("getSubInfo", "【手术编码】不存在");
                return false;
            }

            String tOperationNo = PubFun1.CreateMaxNo("OPERATION", mLimit);
            mLLOperationSchema.setSerialNo(tOperationNo);
            mLLOperationSchema.setCaseNo(mCaseNo);
            mLLOperationSchema.setCaseRelaNo(mCaseRelaNo);
            mLLOperationSchema.setOperationCode(tLDICDOPSDB.getICDOPSCode());
            mLLOperationSchema.setOperationName(tLDICDOPSDB.getICDOPSName());
            mLLOperationSchema.setOpLevel(tLDICDOPSDB.getFrequencyFlag());
            mLLOperationSchema.setOperator(mHandler);
            mLLOperationSchema.setMngCom(mLLCaseSchema.getMngCom());
            mLLOperationSchema.setMakeDate(mMakeDate);
            mLLOperationSchema.setMakeTime(mMakeTime);
            mLLOperationSchema.setModifyDate(mMakeDate);
            mLLOperationSchema.setModifyTime(mMakeTime);

        }

        //事件信息
        String tInHospitalDate = mBaseData.getChildText("HOSPSTARTDATE");
        if ("".equals(tInHospitalDate) || "null".equals(tInHospitalDate)) {
            buildError("getSubInfo", "【入院日期】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(tInHospitalDate)) {
            buildError("getSubInfo", "【入院日期】格式错误");
            return false;
        }

        String tOutHospitalDate = mBaseData.getChildText("HOSPENDDATE");

        if ("".equals(tOutHospitalDate) || "null".equals(tOutHospitalDate)) {
            buildError("getSubInfo", "【出院日期】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(tOutHospitalDate)) {
            buildError("getSubInfo", "【出院日期】格式错误");
            return false;
        }

        String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", mLimit);
        mLLSubReportSchema.setSubRptNo(tSubRptNo);
        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

        //事件类型只能是疾病、意外或者空
        String tAccidentType = mBaseData.getChildText("ACCIDENTTYPE");
        if ("1".equals(tAccidentType) || "2".equals(tAccidentType)) {
            mLLSubReportSchema.setAccidentType(tAccidentType);
        }
        // #3544  录入发生地点省、市、县。程序默认
        mLLSubReportSchema.setAccProvinceCode("210000"); //辽宁省
        mLLSubReportSchema.setAccCityCode("210100");   //沈阳市
        mLLSubReportSchema.setAccCountyCode("210102"); //和平区
        
        mLLSubReportSchema.setAccDate(mAccDate);
        mLLSubReportSchema.setInHospitalDate(tInHospitalDate);
        mLLSubReportSchema.setOutHospitalDate(tOutHospitalDate);
//        mLLSubReportSchema.setAccDesc(mLLCaseCureSchema.getDiseaseName());
//        mLLSubReportSchema.setAccPlace(mLLCaseCureSchema.getHospitalName());
        mLLSubReportSchema.setOperator(mHandler);
        mLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLSubReportSchema.setMakeDate(mMakeDate);
        mLLSubReportSchema.setMakeTime(mMakeTime);
        mLLSubReportSchema.setModifyDate(mMakeDate);
        mLLSubReportSchema.setModifyTime(mMakeTime);
        
        //意外信息
        String tAccidentCode = mBaseData.getChildText("ACCIDENTNO");
        if ("2".equals(tAccidentType)) {
        	if ("".equals(tAccidentCode) || "null".equals(tAccidentCode)) {
        		buildError("getSubInfo", "【意外编码】不能为空");
                return false;      		
        	}
        	        	
        	LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
        	tLLAccidentTypeDB.setAccidentNo(tAccidentCode);
        	if (!tLLAccidentTypeDB.getInfo()) {
                buildError("getSubInfo", "【意外编码】不存在");
                return false;
            }
        	
        	String tAccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mLimit);
        	mLLAccidentSchema.setAccidentNo(tAccidentNo);
        	mLLAccidentSchema.setCaseNo(mCaseNo);
        	mLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
        	mLLAccidentSchema.setCode(tAccidentCode);
        	mLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());
        }

        //事件关联表
        mLLCaseRelaSchema.setCaseNo(mCaseNo);
        mLLCaseRelaSchema.setSubRptNo(tSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);

        return true;
    }

    /**
     * 生成申请原因、申请材料
     * @return boolean
     * @throws Exception
     */
    private boolean getAppReasonInfo() throws Exception {
        System.out.println("LLHospAutoRegister--getAppReasonInfo");
        //申请原因默认住院
        String tReasonCode = "02";
        String tReasonName = "住院费用";
//        if ("".equals(tReasonCode)) {
//            buildError("getAppReasonInfo", "【申请原因】的值不能为空");
//            return false;
//        }
//        LDCodeDB tLDCodeDB = new LDCodeDB();
//        tLDCodeDB.setCodeType("llrgtreason");
//        tLDCodeDB.setCode(tRgtReason);
//        if (!tLDCodeDB.getInfo()) {
//            buildError("getAppReasonInfo", "【申请原因】的值不存在");
//            return false;
//        }

        mLLAppClaimReasonSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLAppClaimReasonSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLAppClaimReasonSchema.setCustomerNo(mInsuredNo);
        mLLAppClaimReasonSchema.setReasonCode(tReasonCode);
        mLLAppClaimReasonSchema.setReason(tReasonName);
        mLLAppClaimReasonSchema.setReasonType("0");
        mLLAppClaimReasonSchema.setOperator(mHandler);
        mLLAppClaimReasonSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLAppClaimReasonSchema.setMakeDate(mMakeDate);
        mLLAppClaimReasonSchema.setMakeTime(mMakeTime);
        mLLAppClaimReasonSchema.setModifyDate(mMakeDate);
        mLLAppClaimReasonSchema.setModifyTime(mMakeTime);

        //选择申请材料
        //默认选中项，稍后开发配置功能
        String tAffixSQL = "select * from llmaffix where affixcode in ('7000','4500','4200','4300','4400','4900')";

        LLMAffixDB tLLMAffixDB = new LLMAffixDB();
        LLMAffixSet tLLMAffixSet = tLLMAffixDB.executeQuery(tAffixSQL);
        if (tLLMAffixSet.size() < 1) {
            buildError("getAppReasonInfo", "申请材料查询失败");
            return false;
        }

        for (int i = 1; i <= tLLMAffixSet.size(); i++) {
            LLMAffixSchema tLLMAffixSchema = tLLMAffixSet.get(i);
            String tAffixNo = PubFun1.CreateMaxNo("rgtaffixno", mLimit);
            LLAffixSchema tLLAffixSchema = new LLAffixSchema();
            tLLAffixSchema.setAffixNo(tAffixNo);
            tLLAffixSchema.setSerialNo(i);
            tLLAffixSchema.setRgtNo(mLLCaseSchema.getRgtNo());
            tLLAffixSchema.setCaseNo(mLLCaseSchema.getCaseNo());
            tLLAffixSchema.setReasonCode(tReasonCode);
            tLLAffixSchema.setAffixType(tLLMAffixSchema.getAffixTypeCode());
            tLLAffixSchema.setAffixCode(tLLMAffixSchema.getAffixCode());
            tLLAffixSchema.setAffixName(tLLMAffixSchema.getAffixName());
            tLLAffixSchema.setCount(1);
            tLLAffixSchema.setShortCount(0);
            tLLAffixSchema.setSupplyDate(mMakeDate);
            tLLAffixSchema.setOperator(mHandler);
            tLLAffixSchema.setMngCom(mLLCaseSchema.getMngCom());
            tLLAffixSchema.setMakeDate(mMakeDate);
            tLLAffixSchema.setMakeTime(mMakeTime);
            tLLAffixSchema.setModifyDate(mMakeDate);
            tLLAffixSchema.setModifyTime(mMakeTime);

            mLLAffixSet.add(tLLAffixSchema);
        }

        //申请材料提交日期
        mLLCaseSchema.setAffixGetDate(mMakeDate);

        return true;
    }

    /**
     * 获取社保账单
     * @return boolean
     * @throws Exception
     */
    private boolean getSecurityInfo() throws Exception {
        System.out.println("LLHospAutoRegister--getSecurityInfo");
        //社保起付线
        String tGetLimit = mBaseData.getChildText("UNDERWAY_CRITERION");
        if (tGetLimit == null || "".equals(tGetLimit)) {
            buildError("getSecurityInfo", "【社保起付线】的值不能为空");
            return false;
        }
        double tGetLimitValue = Arith.round(Double.parseDouble(tGetLimit), 2);

//      统筹支付金额
        String tLowAmnt = mBaseData.getChildText("LOWAMNT");
        if (tLowAmnt == null || "".equals(tLowAmnt)) {
            buildError("getSecurityInfo", "【低段金额】的值不能为空");
            return false;
        }
        double tLowAmntValue = Arith.round(Double.parseDouble(tLowAmnt), 2);
                
        //统筹支付金额
        String tPlanFee = mBaseData.getChildText("BASE_INSURANCE");
        if (tPlanFee == null || "".equals(tPlanFee)) {
            buildError("getSecurityInfo", "【统筹支付金额】的值不能为空");
            return false;
        }
        double tPlanFeeValue = Arith.round(Double.parseDouble(tPlanFee), 2);

        //统筹自付金额
        String tMidAmnt = mBaseData.getChildText("MIDAMNT");
        if (tMidAmnt == null || "".equals(tMidAmnt)) {
            buildError("getSecurityInfo", "【统筹自付金额】的值不能为空");
            return false;
        }
        double tMidAmntValue = Arith.round(Double.parseDouble(tMidAmnt), 2);

        //大额救助支付金额
        String tHighAmnt2 = mBaseData.getChildText("HIGHAMNT2");
        if (tHighAmnt2 == null || "".equals(tHighAmnt2)) {
            buildError("getSecurityInfo", "【大额救助支付金额】的值不能为空");
            return false;
        }
        double tHighAmnt2Value = Arith.round(Double.parseDouble(tHighAmnt2), 2);

        //大额救助自付金额
        String tHighAmnt1 = mBaseData.getChildText("HIGHAMNT1");
        if (tHighAmnt1 == null || "".equals(tHighAmnt1)) {
            buildError("getSecurityInfo", "【大额救助自付金额】的值不能为空");
            return false;
        }
        double tHighAmnt1Value = Arith.round(Double.parseDouble(tHighAmnt1), 2);

        //自费金额
        String tSelfAmnt = mBaseData.getChildText("SELFAMNT");
        if (tSelfAmnt == null || "".equals(tSelfAmnt)) {
            buildError("getSecurityInfo", "【自费金额】的值不能为空");
            return false;
        }
        double tSelfAmntValue = Arith.round(Double.parseDouble(tSelfAmnt), 2);

        //年度累计统筹基金医疗费用
        String tYearPlayFee = mBaseData.getChildText("YEAR_PLAY_FEE");
        if (tYearPlayFee == null || "".equals(tYearPlayFee)) {
            buildError("getSecurityInfo", "【年度累计统筹基金医疗费用】的值不能为空");
            return false;
        }
        double tYearPlayFeeValue = Arith.round(Double.parseDouble(tYearPlayFee),
                                               2);

        //补充医疗保险赔付金额(其他公司赔付的 没什么用)
//        String tSelfPay1 = mBaseData.getChildText("COMPLEMENTARITY_INSURANCE");
//        if ("".equals(tSelfPay1) || "null".equals(tSelfPay1)) {
//            buildError("getSecurityInfo", "【补充医疗保险赔付金额】的值不能为空");
//            return false;
//        }
//        double tSelfPay1Value = Arith.round(Double.parseDouble(tSelfPay1), 2);

        //其他保险赔付金额
        String tOtherPay = mBaseData.getChildText("ELSE_INSURANCE_MONEY");
        if (tOtherPay == null || "".equals(tOtherPay)) {
            buildError("getSecurityInfo", "【其他保险赔付金额】的值不能为空");
            return false;
        }
        double tOtherPayValue = Arith.round(Double.parseDouble(tOtherPay),
                                            2);

        //总自费（社保外：全自费+部分自付）
        String tFeeOutSecu = mBaseData.getChildText("TOTAL_SELF_PAYMENT");
        if (tFeeOutSecu == null || "".equals(tFeeOutSecu)) {
            buildError("getSecurityInfo", "【总自费】的值不能为空");
            return false;
        }
        double tFeeOutSecuValue = Arith.round(Double.parseDouble(tFeeOutSecu),
                                              2);

        //自付2(部分自付)
        double tSelfPay2Value = tFeeOutSecuValue - tSelfAmntValue;

        // 和张克运确认了，各部分之和可以大于总金额2分
        if (Arith.round(tFeeOutSecuValue + tLowAmntValue + tMidAmntValue
				+ tHighAmnt1Value + tPlanFeeValue + tOtherPayValue, 2) > Arith
				.round(mLLFeeMainSchema.getSumFee() + 0.02, 2)) {
			buildError("getSecurityInfo", "账单各部分金额合计不能大于账单总费用");
			return false;
		}

// if (tMidAmntValue + tHighAmnt1Value + tGetLimitValue >
//            tSelfPay1Value) {
//            buildError("getSecurityInfo",
//                       "统筹自付金额+大额救助自付金额+社保起付线不能大于补充医疗保险赔付金额");
//            return false;
//        }

        mLLSecurityReceiptSchema.setFeeDetailNo(mLLFeeMainSchema.getMainFeeNo());
        mLLSecurityReceiptSchema.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());
        mLLSecurityReceiptSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLSecurityReceiptSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLSecurityReceiptSchema.setApplyAmnt(mLLFeeMainSchema.getSumFee());
        mLLSecurityReceiptSchema.setFeeInSecu(mLLSecurityReceiptSchema.
                                              getApplyAmnt() - tFeeOutSecuValue);
        mLLSecurityReceiptSchema.setFeeOutSecu(tFeeOutSecuValue);
        mLLSecurityReceiptSchema.setPlanFee(tPlanFeeValue);
        mLLSecurityReceiptSchema.setLowAmnt(tLowAmntValue);
        mLLSecurityReceiptSchema.setYearPlayFee(tYearPlayFeeValue);
//        mLLSecurityReceiptSchema.setSelfPay1(tSelfPay1Value);
        mLLSecurityReceiptSchema.setSelfPay2(tSelfPay2Value);
        mLLSecurityReceiptSchema.setSelfAmnt(tSelfAmntValue);
        mLLSecurityReceiptSchema.setGetLimit(tGetLimitValue);
        mLLSecurityReceiptSchema.setMidAmnt(tMidAmntValue);
        mLLSecurityReceiptSchema.setHighAmnt1(tHighAmnt1Value);
        mLLSecurityReceiptSchema.setHighAmnt2(tHighAmnt2Value);

        mLLSecurityReceiptSchema.setOperator(mHandler);
        mLLSecurityReceiptSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLSecurityReceiptSchema.setMakeDate(mMakeDate);
        mLLSecurityReceiptSchema.setMakeTime(mMakeTime);
        mLLSecurityReceiptSchema.setModifyDate(mMakeDate);
        mLLSecurityReceiptSchema.setModifyTime(mMakeTime);

        //保存社保明细
        if (!saveFeeItem()) {
            return false;
        }

        return true;
    }

    /**
     * 生成案件信息
     * @return boolean
     * @throws Exception
     */
    private boolean dealCase() throws Exception {
        System.out.println("LLHospAutoRegister--dealCase");
        mMMap.put(mLLRegisterSchema, "DELETE&INSERT");
        mMMap.put(mLLCaseSchema, "DELETE&INSERT");
        mMMap.put(mLLSubReportSchema, "DELETE&INSERT");
        mMMap.put(mLLCaseRelaSchema, "DELETE&INSERT");
        if (mLLOperationSchema.getCaseNo() != null &&
            !"".equals(mLLOperationSchema.getCaseNo()) &&
            !"null".equals(mLLOperationSchema.getCaseNo())) {
            mMMap.put(mLLOperationSchema, "DELETE&INSERT");
        }
        
        if (mLLAccidentSchema.getCaseNo() != null &&
        	!"".equals(mLLAccidentSchema.getCaseNo()) &&
            !"null".equals(mLLAccidentSchema.getCaseNo())) {
            mMMap.put(mLLAccidentSchema, "DELETE&INSERT");
        }
        
        mMMap.put(mLLCaseCureSchema, "DELETE&INSERT");
        mMMap.put(mLLAppClaimReasonSchema, "DELETE&INSERT");
        mMMap.put(mLLAffixSet, "DELETE&INSERT");
        mMMap.put(mLLFeeMainSchema, "DELETE&INSERT");
        mMMap.put(mLLSecurityReceiptSchema, "DELETE&INSERT");
        if (mLLFeeOtherItemSet.size() > 0) {
            mMMap.put(mLLFeeOtherItemSet, "DELETE&INSERT");
        }
        if (mLLCaseDrugSet.size() > 0) {
            mMMap.put(mLLCaseDrugSet, "DELETE&INSERT");
        }
        if (mLLCaseReceiptSet.size() > 0) {
            mMMap.put(mLLCaseReceiptSet, "DELETE&INSERT");
        }

        mMMap.put(mLLCasePolicySet, "DELETE&INSERT");
        mMMap.put(mLLToClaimDutySet, "DELETE&INSERT");

        try {
            this.mInputData.clear();
            this.mInputData.add(mMMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            buildError("dealCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("dealCase", "数据提交失败");
            return false;
        }

        return true;
    }

    /**
     * 调用理算类计算
     * @return boolean
     * @throws Exception
     */
    private boolean claimCal() throws Exception {
        System.out.println("LLHospAutoRegister--claimCal");
        ClaimCalBL tClaimCalAutoBL = new ClaimCalBL();

        mGlobalInput.Operator = mHandler;
        mGlobalInput.ManageCom = mManageCom;

        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(mLLCaseSchema);
        if (!tClaimCalAutoBL.submitData(aVData, "autoCal")) {
            buildError("dealCase", "数据提交失败");
        }
        return true;
    }

    /**
     * 校验保额
     * @return boolean
     * @throws Exception
     */
    private boolean checkAmnt() throws Exception {
        System.out.println("LLHospAutoRegister--checkAmnt");
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        tLLClaimDetailDB.setCaseNo(mCaseNo);
        LLClaimDetailSet tGetLLClaimDetailSet = tLLClaimDetailDB.query();

        if (tGetLLClaimDetailSet.size() < 1) {
            buildError("checkAmnt", "理算失败，请联系核心运维人员");
            return false;
        }

        for (int i = 1; i <= tGetLLClaimDetailSet.size(); i++) {
            LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
            LLClaimDetailSchema tLLClaimDetailSchema = tGetLLClaimDetailSet.get(
                    i);
            tLLClaimDetailSet.add(tLLClaimDetailSchema);

            LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
            VData tVData = new VData();

            tVData.addElement(tGetLLClaimDetailSet);
            tVData.addElement(mLLCaseSchema);
            tVData.addElement(mGlobalInput);

            if (!tLLUWCheckBL.submitData(tVData, "")) {
                double tRealPay = getRealPay(tLLClaimDetailSchema);
                tLLClaimDetailSchema.setRealPay(Arith.round(tRealPay, 2));
                tLLClaimDetailSchema.setStandPay(Arith.round(tRealPay, 2));
            }
            mLLClaimDetailSet.add(tLLClaimDetailSchema);
        }
        return true;
    }

    /**
     * 校验保额失败后，返回可以赔付的金额
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getRealPay(LLClaimDetailSchema aLLClaimDetailSchema) {
        return 0.0;
    }

    /**
     * 增加赔付结论
     * @return boolean
     * @throws Exception
     */
    private boolean claimSave() throws Exception {
        System.out.println("LLHospAutoRegister--claimSave");
        if (mLLClaimDetailSet.size() < 1) {
            buildError("claimSave", "理算数据获取失败");
            return false;
        }

        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setCaseNo(mCaseNo);

        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        if (tLLClaimPolicySet.size() < 1) {
            buildError("claimSave", "理算数据获取失败");
            return false;
        }

        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(mCaseNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() < 1) {
            buildError("claimSave", "理算数据获取失败");
            return false;
        }

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            buildError("claimSave", "案件查询失败");
            return false;
        } else {
            mLLCaseSchema = tLLCaseDB.getSchema();
        }

        double tSumRealPay = 0;
        double tSumStandPay = 0;
        String tGiveType = "1";

        for (int j = 1; j <= tLLClaimPolicySet.size(); j++) {
            LLClaimPolicySchema tLLClaimPolicySchema = tLLClaimPolicySet.get(j);

            double tRealPay = 0;
            double tStandPay = 0;

            for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
                LLClaimDetailSchema tLLClaimDetailSchema =
                        mLLClaimDetailSet.get(i);

                if (tLLClaimPolicySchema.getPolNo().equals(tLLClaimDetailSchema.
                        getPolNo()) &&
                    tLLClaimPolicySchema.getGetDutyKind().equals(
                            tLLClaimDetailSchema.getGetDutyKind())) {

                    if (tLLClaimDetailSchema.getRealPay() > 0) {
                        tLLClaimDetailSchema.setGiveType("1");
                        tLLClaimDetailSchema.setGiveTypeDesc("正常给付");
                        tLLClaimDetailSchema.setGiveReason("01");
                        tLLClaimDetailSchema.setGiveReasonDesc("条款原因");
                    } else {
                        tLLClaimDetailSchema.setGiveType("3");
                        tLLClaimDetailSchema.setGiveTypeDesc("全额拒付");
                        tLLClaimDetailSchema.setGiveReason("99");
                        tLLClaimDetailSchema.setGiveReasonDesc("其他");
                    }

                    mConfirmLLClaimDetailSet.add(tLLClaimDetailSchema);

                    if ("3".equals(tGiveType) &&
                        "3".equals(tLLClaimDetailSchema.getGiveType())) {
                        tGiveType = "3";
                    } else if ("3".equals(tGiveType) ||
                               "3".equals(tLLClaimDetailSchema.getGiveType())) {
                        tGiveType = "2";
                    } else if ("2".equals(tGiveType) ||
                               "2".equals(tLLClaimDetailSchema.getGiveType())) {
                        tGiveType = "2";
                    } else {
                        tGiveType = "1";
                    }

                    tRealPay += tLLClaimDetailSchema.getRealPay();
                    tStandPay += tLLClaimDetailSchema.getStandPay();
                }
            }

            tLLClaimPolicySchema.setRealPay(Arith.round(tRealPay, 2));
            tLLClaimPolicySchema.setStandPay(Arith.round(tStandPay, 2));
            tLLClaimPolicySchema.setClmState("1"); //已结算
            tLLClaimPolicySchema.setClmUWer(mLLCaseSchema.getHandler());
            tLLClaimPolicySchema.setGiveType(tGiveType);
            tLLClaimPolicySchema.setGiveTypeDesc(LLCaseCommon.getGiveDesc(
                    tGiveType));

            mLLClaimPolicySet.add(tLLClaimPolicySchema);

            tSumRealPay += tLLClaimPolicySchema.getRealPay();
            tSumStandPay += tLLClaimPolicySchema.getStandPay();
        }

        mLLClaimSchema = tLLClaimSet.get(1);
        mLLClaimSchema.setRealPay(Arith.round(tSumRealPay, 2));
        mLLClaimSchema.setStandPay(Arith.round(tSumStandPay, 2));
        mLLClaimSchema.setGiveType(tGiveType);
        if ("3".equals(tGiveType)) {
            mLLCaseSchema.setDeclineFlag("1");
        } else {
            mLLCaseSchema.setDeclineFlag("");
        }
        mLLClaimSchema.setGiveTypeDesc(LLCaseCommon.getGiveDesc(tGiveType));

        return true;
    }

    private boolean dealClaimSave() throws Exception {
        System.out.println("LLHospAutoRegister--dealClaimSave");
        String delsql1 = "delete from llclaimdetail where caseno='" +
                         mCaseNo + "'";
        String delsql2 = "delete from llclaimPolicy where caseno='" +
                         mCaseNo + "'";
        String delsql3 = "delete from llclaim where caseno='" +
                         mCaseNo + "'";
        String delLJSGetClaim =
                "delete from ljsgetclaim where othernotype='5' and otherno='"
                + mCaseNo + "'";
        String delLJSGet =
                "delete from ljsget where othernotype='5' and otherno='"
                + mCaseNo + "'";

        MMap tMMap = new MMap();
        tMMap.put(delsql1, "DELETE");
        tMMap.put(delsql2, "DELETE");
        tMMap.put(delsql3, "DELETE");
        tMMap.put(delLJSGetClaim, "DELETE");
        tMMap.put(delLJSGet, "DELETE");

        tMMap.put(mLLCaseSchema, "UPDATE");
        tMMap.put(mConfirmLLClaimDetailSet, "DELETE&INSERT");
        tMMap.put(mLLClaimPolicySet, "DELETE&INSERT");
        tMMap.put(mLLClaimSchema, "DELETE&INSERT");
        tMMap.put(mLJSGetClaimSet, "DELETE&INSERT");
        tMMap.put(mLJSGetSchema, "DELETE&INSERT");
        tMMap.put(mLLHospCaseSchema, "DELETE&INSERT");

        try {
            this.mInputData.clear();
            this.mInputData.add(tMMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            buildError("dealClaimSave", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("dealClaimSave", "数据提交失败");
            return false;
        }

        return true;
    }

    /**
     * 生成应付
     * @return boolean
     * @throws Exception
     */
    private boolean genLJSGet() throws Exception {
        System.out.println("LLHospAutoRegister--genLJSGet");
        String sql = "select sum(realpay),stattype,GetDutyKind,polno,kindcode," +
                     "riskcode,riskver from llclaimdetail where caseno='" +
                     mCaseNo +
                     "' group by stattype,getdutykind,polno,kindcode,riskcode,riskver ";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(sql);
        if (exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||
            ssrs.getMaxRow() <= 0) {
            buildError("genLJSGet", "生成应付统计错误");
            return false;
        }
        mLJSGetClaimSet = new LJSGetClaimSet();
        String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", mLimit);
        double tSumRealPay = 0;
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
            tLJSGetClaimSchema.setFeeFinaType(ssrs.GetText(i, 2));
            tLJSGetClaimSchema.setGetDutyKind(ssrs.GetText(i, 3));
            tLJSGetClaimSchema.setFeeOperationType(ssrs.GetText(i, 3));
            tLJSGetClaimSchema.setPay(0);
            for (int k = 1; k <= mLLClaimDetailSet.size(); k++) {
                LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.
                        get(k);
                if (ssrs.GetText(i, 2).equals(aLLClaimDetailSchema.getStatType()) &&
                    ssrs.GetText(i,
                                 3).equals(aLLClaimDetailSchema.getGetDutyKind()) &&
                    ssrs.GetText(i, 4).equals(aLLClaimDetailSchema.getPolNo()) &&
                    ssrs.GetText(i, 5).equals(aLLClaimDetailSchema.getKindCode()) &&
                    ssrs.GetText(i, 6).equals(aLLClaimDetailSchema.getRiskCode()) &&
                    ssrs.GetText(i, 7).equals(aLLClaimDetailSchema.getRiskVer())) {
                    double apaymoney = tLJSGetClaimSchema.getPay() +
                                       aLLClaimDetailSchema.getRealPay();
                    apaymoney = Arith.round(apaymoney, 2);
                    tLJSGetClaimSchema.setPay(apaymoney);
                }
            }
            tLJSGetClaimSchema.setPolNo(ssrs.GetText(i, 4));
            tLJSGetClaimSchema.setKindCode(ssrs.GetText(i, 5));
            tLJSGetClaimSchema.setRiskCode(ssrs.GetText(i, 6));
            tLJSGetClaimSchema.setRiskVersion(ssrs.GetText(i, 7));
            //添加续保查询
            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(ssrs.GetText(i, 4));
            if (!tLCPolBL.getInfo()) {
                buildError("genLJSGet", "保单查询失败[" + ssrs.GetText(i, 4) + "]");
                return false;
            }
            tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
            tLJSGetClaimSchema.setContNo(tLCPolBL.getContNo());
            tLJSGetClaimSchema.setGrpContNo(tLCPolBL.getGrpContNo());
            tLJSGetClaimSchema.setGrpPolNo(tLCPolBL.getGrpPolNo());
            tLJSGetClaimSchema.setAgentCode(tLCPolBL.getAgentCode());
            tLJSGetClaimSchema.setAgentCom(tLCPolBL.getAgentCom());
            tLJSGetClaimSchema.setAgentGroup(tLCPolBL.getAgentGroup());
            tLJSGetClaimSchema.setAgentType(tLCPolBL.getAgentType());
            tLJSGetClaimSchema.setOperator(mLLCaseSchema.getHandler());
            tLJSGetClaimSchema.setManageCom(tLCPolBL.getManageCom());
            tLJSGetClaimSchema.setMakeDate(mMakeDate);
            tLJSGetClaimSchema.setMakeTime(mMakeTime);
            tLJSGetClaimSchema.setModifyDate(mMakeDate);
            tLJSGetClaimSchema.setModifyTime(mMakeTime);
            tLJSGetClaimSchema.setOtherNo(mCaseNo);
            tLJSGetClaimSchema.setOtherNoType("5");

            mLJSGetClaimSet.add(tLJSGetClaimSchema);
            tSumRealPay += tLJSGetClaimSchema.getPay();
        }
        Reflections tref = new Reflections();
        tref.transFields(mLJSGetSchema, mLJSGetClaimSet.get(1));

        tSumRealPay = Arith.round(tSumRealPay, 2);
        mLJSGetSchema.setSumGetMoney(tSumRealPay);
        return true;
    }

    /**
     * 计算完成后，完成数据处理
     * @return boolean
     * @throws Exception
     */
    private boolean endCal() throws Exception {
        System.out.println("LLHospAutoRegister--endCal");
        try {
            //校验保额
            if (!checkAmnt()) {
                return false;
            }

            //确定赔付结论
            if (!claimSave()) {
                return false;
            }

            //生成应付
            if (!genLJSGet()) {
                return false;
            }

            //记录交易轨迹
            if (!makeHospCase()) {
                return false;
            }

            //提交理算确认数据
            if (!dealClaimSave()) {
                return false;
            }
        } catch (Exception ex) {
            buildError("endCal", "处理理算数据失败");
            return false;
        }
        return true;
    }

    private boolean cancelCase() throws Exception {
        System.out.println("LLHospAutoRegister--cancelCase");
        //理算数据
        String tClaimSQL = "delete from LLClaim where CaseNo='" + mCaseNo + "'";
        String tClaimPolicySQL = "delete from LLClaimPolicy where CaseNo='" +
                                 mCaseNo + "'";
        String tClaimDetailSQL = "delete from LLClaimDetail where CaseNo='" +
                                 mCaseNo + "'";
        String tInsureAccTrace = "delete from LCInsureAccTrace where OtherNo='" +
                                 mCaseNo + "'";
        String tToClaimDutySQL = "delete from LLToClaimDuty where CaseNo ='" +
                                 mCaseNo + "'";
        String tCasePolicySQL = "delete from LLCasePolicy where CaseNo ='"
                                + mCaseNo + "'";

        String tFeeMainSQL = "delete from LLFeeMain where CaseNo='" + mCaseNo +
                             "'";
        String tSecuritySQL = "delete from LLSecurityReceipt where CaseNo = '" +
                              mCaseNo + "'";
        String tFeeItemSQL = "delete from LLFeeOtherItem where CaseNo = '" +
                             mCaseNo + "'";
        String tCaseReceiptSQL = "delete from LLCaseReceipt where CaseNo = '" +
                                 mCaseNo + "'";
        String tCaseDrugSQL = "delete from LLCaseDrug where CaseNo = '" +
                              mCaseNo + "'";
        String tCaseCureSQL = "delete from LLCaseCure where CaseNo = '" +
                              mCaseNo + "'";
        String tOperationSQL = "delete from LLOperation where CaseNo = '" +
                               mCaseNo + "'";
        String tAccidentSQL = "delete from LLAccident where CaseNo = '" +
                               mCaseNo + "'";

        String tSubReportSQL = "delete from LLSubReport a where exists (select 1 from llcaserela where a.subrptno=subrptno and CaseNo='" +
                               mCaseNo + "')";
        String tCaseRelaSQL = "delete from LLCaseRela where CaseNo='" + mCaseNo +
                              "'";
        String tCaseSQL = "delete from LLCase where CaseNo='" + mCaseNo + "'";

        String tRegisterSQL = "delete from LLRegister where RgtNo ='" + mCaseNo +
                              "'";

        String tHospCaseSQL = "delete from LLHospCase where CaseNo ='" +
                              mCaseNo + "'";

        MMap tmpMap = new MMap();
        tmpMap.put(tClaimSQL, "DELETE");
        tmpMap.put(tClaimPolicySQL, "DELETE");
        tmpMap.put(tClaimDetailSQL, "DELETE");
        tmpMap.put(tInsureAccTrace, "DELETE");
        tmpMap.put(tToClaimDutySQL, "DELETE");
        tmpMap.put(tCasePolicySQL, "DELETE");
        tmpMap.put(tFeeMainSQL, "DELETE");
        tmpMap.put(tSecuritySQL, "DELETE");
        tmpMap.put(tCaseReceiptSQL, "DELETE");
        tmpMap.put(tCaseDrugSQL, "DELETE");
        tmpMap.put(tFeeItemSQL, "DELETE");
        tmpMap.put(tCaseCureSQL, "DELETE");
        tmpMap.put(tOperationSQL, "DELETE");
        tmpMap.put(tAccidentSQL, "DELETE");
        tmpMap.put(tSubReportSQL, "DELETE");
        tmpMap.put(tCaseRelaSQL, "DELETE");
        tmpMap.put(tCaseSQL, "DELETE");
        tmpMap.put(tRegisterSQL, "DELETE");
        tmpMap.put(tHospCaseSQL, "DELETE");

        try {
            this.mInputData.clear();
            this.mInputData.add(tmpMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            buildError("cancelCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("cancelCase", "撤销数据提交失败");
            return false;
        }

        return false;
    }

    /**
     * 生成医保通案件表
     * @return boolean
     * @throws Exception
     */
    private boolean makeHospCase() throws Exception {
        System.out.println("LLHospAutoRegister--makeHospCase");
        mLLHospCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLHospCaseSchema.setHospitCode(mTranHospCode);
        mLLHospCaseSchema.setHandler(mLLCaseSchema.getHandler());
        mLLHospCaseSchema.setAppTranNo(mTransactionNum);
        mLLHospCaseSchema.setAppDate(mMakeDate);
        mLLHospCaseSchema.setAppTime(mMakeTime);
        if ("1".equals(mDealType)) {
            mLLHospCaseSchema.setRealpay(mLLClaimSchema.getRealPay());
        } else {
            mLLHospCaseSchema.setRealpay(0);
        }
        //处理类型
        mLLHospCaseSchema.setDealType(mDealType);
        //未确认
        mLLHospCaseSchema.setConfirmState("0");
        mLLHospCaseSchema.setMakeDate(mMakeDate);
        mLLHospCaseSchema.setMakeTime(mMakeTime);
        mLLHospCaseSchema.setModifyDate(mMakeDate);
        mLLHospCaseSchema.setModifyTime(mMakeTime);
        return true;
    }

    /**
     * 保存社保明细
     * @return boolean
     * @throws Exception
     */
    private boolean saveFeeItem() throws Exception {
        System.out.println("LLHospAutoRegister--saveFeeItem");
        //合计金额
        if (mLLSecurityReceiptSchema.getApplyAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getApplyAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("301");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("301");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }

        }

        //统筹基金支付
        if (mLLSecurityReceiptSchema.getPlanFee() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getPlanFee());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("302");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("302");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //全自费
        if (mLLSecurityReceiptSchema.getSelfAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("305");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("305");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //自付二
        if (mLLSecurityReceiptSchema.getSelfPay2() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfPay2());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("306");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("306");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //自付一
        if (mLLSecurityReceiptSchema.getSelfPay1() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getSelfPay1());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("307");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("307");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //起付线
        if (mLLSecurityReceiptSchema.getGetLimit() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getGetLimit());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("308");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("308");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //中段
        if (mLLSecurityReceiptSchema.getMidAmnt() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getMidAmnt());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("309");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("309");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        //高段1
        if (mLLSecurityReceiptSchema.getHighAmnt1() > 0) {
            LLFeeOtherItemSchema tLLFeeOtherItemSchema = new
                    LLFeeOtherItemSchema();
            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mManageCom);
            Reflections tReflection = new Reflections();
            tLLFeeOtherItemSchema.setCDSerialNo(tFeeDetailNo);
            tReflection.transFields(tLLFeeOtherItemSchema,
                                    mLLSecurityReceiptSchema);
            tLLFeeOtherItemSchema.setFeeMoney(mLLSecurityReceiptSchema.
                                              getHighAmnt1());
            tLLFeeOtherItemSchema.setItemClass("S");
            tLLFeeOtherItemSchema.setAvliFlag("0");
            tLLFeeOtherItemSchema.setItemCode("310");

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llsecufeeitem");
            tLDCodeDB.setCode("310");

            if (tLDCodeDB.getInfo()) {
                tLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
                tLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
                mLLFeeOtherItemSet.add(tLLFeeOtherItemSchema);
            }
        }

        return true;
    }

    /**
     * 保存处方明细
     * @return boolean
     * @throws Exception
     */
    private boolean getCaseDrug() throws Exception {
        if (mPrescriptionList == null) {
            return true;
        }

        List tPrescriptionList = new ArrayList();
        tPrescriptionList = (List) mPrescriptionList.getChildren();

        for (int i = 0; i < tPrescriptionList.size(); i++) {
            Element tPrescriptionData = (Element) tPrescriptionList.get(i);
            Element tPrescriptionListData = tPrescriptionData.getChild(
                    "PRESCRIPTION_DETAIL_LIST");
            List tCaseDrugList = new ArrayList();
            tCaseDrugList = tPrescriptionListData.getChildren();

            String tPrescriptionNo = tPrescriptionData.getChildText(
                    "PRESCRIPTION_NUMBER");
            if (tPrescriptionNo == null || "".equals(tPrescriptionNo)) {
                buildError("getCaseDrug", "【处方编号】不能为空");
                return false;
            }

            for (int j = 0; j < tCaseDrugList.size(); j++) {
                Element tCaseDrugData = (Element) tCaseDrugList.get(j);

                String tDrugName = tCaseDrugData.getChildText(
                        "HOSPITAL_LIST_NAME");
                if (tDrugName == null || "".equals(tDrugName)) {
                    buildError("getCaseDrug", "【医疗项目名称】不能为空");
                    return false;
                }

                String tDrugCode = tCaseDrugData.getChildText(
                        "HOSPITAL_LIST_NUMBER");
                if (tDrugCode == null || "".equals(tDrugCode)) {
                    buildError("getCaseDrug", "【医院医疗目录编号】不能为空");
                    return false;
                }

                String tFee = tCaseDrugData.getChildText("AGGREGATE_MONEY");
                if (tFee == null || "".equals(tFee)) {
                    buildError("getCaseDrug", "【总金额】不能为空");
                    return false;
                }
                double tSumFee = Arith.round(Double.parseDouble(tFee), 2);

                String tLimitFee = tCaseDrugData.getChildText("LIMITED_MONEY");
                double tLimitFeeValue = 0.0;
                if (!"".equals(tLimitFee) && tLimitFee != null) {
                    tLimitFeeValue = Double.parseDouble(tLimitFee);
                }

                String tDeductionFee = tCaseDrugData.getChildText(
                        "DEDUCTION_MONEY");
                double tDeductionFeeValue = 0.0;
                if (!"".equals(tDeductionFee) && tDeductionFee != null) {
                    tDeductionFeeValue = Double.parseDouble(tDeductionFee);
                }

                double tUnReasonableFee = Arith.round(tLimitFeeValue +
                        tDeductionFeeValue, 2);

                String tRate = tCaseDrugData.getChildText("LIABLE_EXPENSE");
                if (tRate == null || "".equals(tRate)) {
                    buildError("getCaseDrug", "【自费比例】不能为空");
                    return false;
                }

                double tPayRate = Double.parseDouble(tRate);
                double tSelfAmnt = 0.0;
                double tSelfPay2 = 0.0;

                if (tPayRate < 0 || tPayRate > 1) {
                    buildError("getCaseDrug", "【自费比例】范围错误");
                    return false;
                }

                if (tPayRate == 1) {
                    tSelfAmnt = Arith.round(tSumFee - tUnReasonableFee, 2);
                } else {
                    tSelfPay2 = Arith.round((tSumFee - tUnReasonableFee) *
                                            tPayRate, 2);
                }

                double tSecuFee = Arith.round(tSumFee - tUnReasonableFee -
                                              tSelfAmnt - tSelfPay2, 2);

                String tDrugDetailNo = PubFun1.CreateMaxNo("DRUGDETAILNO",
                        mLimit);
                LLCaseDrugSchema tLLCaseDrugSchema = new LLCaseDrugSchema();
                tLLCaseDrugSchema.setDrugDetailNo(tDrugDetailNo);
                tLLCaseDrugSchema.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());
                tLLCaseDrugSchema.setRgtNo(mLLFeeMainSchema.getRgtNo());
                tLLCaseDrugSchema.setCaseNo(mLLFeeMainSchema.getCaseNo());
                if (tDrugName.length() > 60) {
                    tDrugName = tDrugName.substring(0, 60);
                }
                tLLCaseDrugSchema.setDrugName(tDrugName);
                if (tDrugCode.length() > 20) {
                    tDrugCode = tDrugCode.substring(0, 20);
                }
                tLLCaseDrugSchema.setDrugCode(tDrugCode);
                tLLCaseDrugSchema.setFee(tSumFee);
                tLLCaseDrugSchema.setSecuFee(tSecuFee);
                tLLCaseDrugSchema.setSelfFee(tSelfAmnt);
                tLLCaseDrugSchema.setSelfPay2(tSelfPay2);
                tLLCaseDrugSchema.setUnReasonableFee(tUnReasonableFee);
                tLLCaseDrugSchema.setPrescriptionNo(tPrescriptionNo);
                //录入方式为1-医保通
                tLLCaseDrugSchema.setInputType("1");

                tLLCaseDrugSchema.setMngCom(mLLCaseSchema.getMngCom());
                tLLCaseDrugSchema.setOperator(mLLCaseSchema.getHandler());
                tLLCaseDrugSchema.setMakeDate(mMakeDate);
                tLLCaseDrugSchema.setMakeTime(mMakeTime);
                tLLCaseDrugSchema.setModifyDate(mMakeDate);
                tLLCaseDrugSchema.setModifyTime(mMakeTime);

                mLLCaseDrugSet.add(tLLCaseDrugSchema);

            }
        }

        return true;
    }

    /**
     * 处理费用明细部分（住院编码可以直接匹配，门诊有问题）
     * @return boolean
     * @throws Exception
     */
    private boolean getCaseReceipt() throws Exception {
        if (mFeeItemList == null) {
            return true;
        }
        List tFeeItemList = new ArrayList();
        tFeeItemList = (List) mFeeItemList.getChildren();

        for (int i = 0; i < tFeeItemList.size(); i++) {
            Element tFeeItemData = (Element) tFeeItemList.get(i);

            String tFeeCode = tFeeItemData.getChildText("FEEITEM_CODE");
            if (tFeeCode == null || "".equals(tFeeCode)) {
                buildError("getCaseReceipt", "【账单分类编号】不能为空");
                return false;
            }

            String tSumFee = tFeeItemData.getChildText("SUMFEE");
            if (tSumFee == null || "".equals(tSumFee)) {
                buildError("getCaseReceipt", "【总金额】不能为空");
                return false;
            }
            double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee), 2);

            String tSelfAmnt = tFeeItemData.getChildText("SELFAMNT");
            if (tSelfAmnt == null || "".equals(tSelfAmnt)) {
                buildError("getCaseReceipt", "【全自费费用】不能为空");
                return false;
            }
            double tSelfAmntValue = Arith.round(Double.parseDouble(tSelfAmnt),
                                                2);

            String tPreAmnt = tFeeItemData.getChildText("PREAMNT");
            if (tPreAmnt == null || "".equals(tPreAmnt)) {
                buildError("getCaseReceipt", "【部分自付费用】不能为空");
                return false;
            }
            double tPreAmntValue = Arith.round(Double.parseDouble(tPreAmnt), 2);

            String tRefuseAmnt = tFeeItemData.getChildText("REFUSEAMNT");
            if (tRefuseAmnt == null || "".equals(tRefuseAmnt)) {
                buildError("getCaseReceipt", "【不合理费用】不能为空");
                return false;
            }
            double tRefuseAmntValue = Arith.round(Double.parseDouble(
                    tRefuseAmnt), 2);

            if ("2".equals(mLLFeeMainSchema.getFeeType())) {
                tFeeCode = "2" + tFeeCode;
            }

            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("llfeeitemtype");
            tLDCodeDB.setCode(tFeeCode);
            if (!tLDCodeDB.getInfo()) {
                buildError("getCaseReceipt", "分类编号查询失败");
                return false;
            }

            String tFeeDetailNo = PubFun1.CreateMaxNo("FEEDETAILNO", mLimit);

            LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();
            tLLCaseReceiptSchema.setFeeDetailNo(tFeeDetailNo);
            tLLCaseReceiptSchema.setMainFeeNo(mLLFeeMainSchema.getMainFeeNo());
            tLLCaseReceiptSchema.setRgtNo(mLLFeeMainSchema.getRgtNo());
            tLLCaseReceiptSchema.setCaseNo(mLLFeeMainSchema.getCaseNo());
            tLLCaseReceiptSchema.setFeeItemCode(tFeeCode);
            tLLCaseReceiptSchema.setFeeItemName(tLDCodeDB.getCodeName());
            tLLCaseReceiptSchema.setFee(tSumFeeValue);
            tLLCaseReceiptSchema.setAvaliFlag("0");
            tLLCaseReceiptSchema.setMngCom(mLLFeeMainSchema.getMngCom());
            tLLCaseReceiptSchema.setOperator(mLLFeeMainSchema.getOperator());
            tLLCaseReceiptSchema.setMakeDate(mMakeDate);
            tLLCaseReceiptSchema.setMakeTime(mMakeTime);
            tLLCaseReceiptSchema.setModifyDate(mMakeDate);
            tLLCaseReceiptSchema.setModifyTime(mMakeTime);
            tLLCaseReceiptSchema.setPreAmnt(tPreAmntValue);
            tLLCaseReceiptSchema.setSelfAmnt(tSelfAmntValue);
            tLLCaseReceiptSchema.setRefuseAmnt(tRefuseAmntValue);

            mLLCaseReceiptSet.add(tLLCaseReceiptSchema);
        }
        return true;
    }
    
    /**
     * 解析理算过程
     * @param aLLElementDetailSet
     * @return
     * @throws Exception
     */
    private String parseClaimResult(LLElementDetailSet aLLElementDetailSet) {
    	if (aLLElementDetailSet.size() <= 0) {
    		buildError("parseClaimResult", "查询理算要素出错。");
            return "";
    	}
    	    	
        PubCalculator tPubCalculator = new PubCalculator();
        String tCalCode = "";
        
        for (int i = 1 ; i <= aLLElementDetailSet.size(); i++) {
        	LLElementDetailSchema tLLElementDetailSchema = aLLElementDetailSet.get(i);
        	if (i==1) {
        		tCalCode = tLLElementDetailSchema.getCalCode();
        	} else {
        		if (!tCalCode.equals(tLLElementDetailSchema.getCalCode())) {
        			buildError("parseClaimResult", "理算要素计算编码出错。");
                    return "";
        		}
        	}
        	
        	tPubCalculator.addBasicFactor(tLLElementDetailSchema.getElementCode(), tLLElementDetailSchema.getElementValue());
        }
        
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setCalCode(tCalCode);
        if (!tLMCalModeDB.getInfo()) {
        	buildError("parseClaimResult", "理算算法查询失败。");
            return "";
        }

        String tSql = tLMCalModeDB.getRemark().trim();
        String tStr = "";
        String tStr1 = "";
        try {
            while (true) {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals("")) {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        } catch (Exception ex) {
            // @@错误处理
            buildError("parseClaimResult", "解释" + tSql + "的变量:" + tStr + "时出错。");
            return "";
        }
        if ("0".equals(tSql.trim())) {
        	tSql = "解析"+tLMCalModeDB.getCalCode()+"提示信息出错。";
        }
        if (tSql.trim().equals("")) {
            return tLMCalModeDB.getRemark();
        }
        return tSql;
    }

    /**
     * 计算被保人周岁年龄 （出身日期--发生日期）
     * @param args
     * 
     */
    private int getAgeDate(String tCustomerBirhday, String tAccDate){
    	FDate tFDate = new FDate();
    	Date tBirthday= tFDate.getDate(tCustomerBirhday);
    	Date tAccday =tFDate.getDate(tAccDate);
    	int CustomerAge = PubFun.calInterval(tBirthday, tAccday, "Y");
        System.out.println("年龄计算函数。。。" + CustomerAge);
        return CustomerAge;
    }
    
    
    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:\\理赔报文\\沈阳报文/13.xml"), "GBK");

            LLHospAutoRegisterSY tBusinessDeal = new LLHospAutoRegisterSY();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
