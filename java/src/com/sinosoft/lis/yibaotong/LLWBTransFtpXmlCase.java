package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLWBTransFtpXmlCase
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();

    public LLWBTransFtpXmlCase()
    {
    }

    public boolean submitData()
    {
        if (getSubmitMap() == null)
        {
            return false;
        }


        return true;
    }



    public MMap getSubmitMap()
    {


        if (!dealData())
        {
            return null;
        }

        return map;
    }


    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }


    private boolean getXls()
    {
        String tUIRoot = CommonBL.getUIRoot();

        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }
        
        int xmlCount = 0;

        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='IP/Port'");
        String tPort = tExeSql
                .getOneValue("select codealias from ldcode where codetype='WBClaim' and code='IP/Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='User/Pass'");
        String tPassword = tExeSql
                .getOneValue("select codealias from ldcode where codetype='WBClaim' and code='User/Pass'");

        String tFileRgtkPath = tExeSql
        		.getOneValue("select codename from ldcode where codetype='WBClaim' and code='XmlPath'");

        String tFileCasePath = tExeSql
				.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='XmlPath'");

		tFileRgtkPath = tUIRoot + tFileRgtkPath;
		tFileCasePath = tUIRoot + tFileCasePath;
		
        try
        {
        	FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "CardActiveBatchImportBL";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
           
//            String tFilePathLocal = tFileBackPath;
//            String tFilePathLocal = "E:\\8695\\WBClaimCase\\";
            String tFilePathLocalCore = tFileCasePath+PubFun.getCurrentDate()+"/";//服务器存放案件报文文件的路径 每天生成一个文件夹
//            String tFileImportBackPath = tFilePath;
//            tFilePathLocalCore = "D:\\Java\\test\\LLWB\\";//本地测试使用
            String tFileImportPath = "/01PH/8695/WBClaimCase";//ftp上存放文件的目录
            String tFileImportBackPath = "/01PH/8695/WBClaimCaseBack";//ftp上存放核心返回报文文件的目录

            String tFileImportPathBackUp = "/01PH/8695/WBClaimBackUp/" +PubFun.getCurrentDate()+"/";//备份目录
            
            String tFilePathout = tFilePathLocalCore+"Back/";          
            
            String tComtop="WBResult";
            String ResultMax = tFilePathout+"upload/" +(tComtop + PubFun1.CreateMaxNo(tComtop, 5))+"/";
            
            File mFileDir = new File(tFilePathLocalCore);
    		if (!mFileDir.exists()) {
    			if (!mFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
    			}
    		}
    		
            File tFileDir = new File(tFilePathout);
    		if (!tFileDir.exists()) {
    			if (!tFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
    			}
    		}
    		
    		File tResultMaxDir = new File(ResultMax);
            if(!tResultMaxDir.exists()){
            	if(!tResultMaxDir.mkdirs()){
            		System.err.println("创建目录[" + ResultMax.toString() + "]失败！"
    						+ tResultMaxDir.getPath());
            	}
            }
    		
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
            System.out.println("OK");

            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                return false;
            }
            // 报文先做存储
            for(int i=0;i<tPath.length;i++){
         	   if(tPath[i]==null){
         		   continue;
         	   }
         	   tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[i]);
         	   tFTPTool.changeWorkingDirectory(tFileImportPath);
         	   tFTPTool.deleteFile(tPath[i]);
            }

            tFTPTool.logoutFTP();
            
            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }
                System.out.println("Hello !!");  

                xmlCount++;

                FileInputStream fis = null;
                FileOutputStream fos = null;
                FileOutputStream fos2 = null;
                try
                {
                    VData tVData = new VData();

                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("FileName", tPath[j]);
                    tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);

                    tVData.add(tTransferData);
                    tVData.add(mGI);

                    Document tInXmlDoc;  
                    fis = new FileInputStream(tFilePathLocalCore+tPath[j]);
                    tInXmlDoc = JdomUtil.build(fis, "GBK");
                    LLWBCaseRegister tBusinessDeal = new LLWBCaseRegister();
                    Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
                    System.out.println("打印传入报文============");
                    JdomUtil.print(tInXmlDoc.getRootElement());
                    System.out.println("打印传出报文============");
                    JdomUtil.print(tOutXmlDoc);
                    fos = new FileOutputStream(tFilePathout+tPath[j]);
                    JdomUtil.output(tOutXmlDoc, fos);
                    
                    fos2 = new FileOutputStream(ResultMax+tPath[j]);
			        JdomUtil.output(tOutXmlDoc,fos2);
                }
                catch (Exception ex)
                {
                    System.out.println("批次导入失败: " + tPath[j]);
                    mErrors.addOneError("批次导入失败" + tPath[j]);
                    ex.printStackTrace();
                  
                }finally {
                	try {
                		if(fis!=null) {
                			fis.close();
                		}
                	}finally {
                		if(fos != null) {
                			fos.close();
                		}
                		if(fos2 != null) {
                			fos2.close();
                		}
                	}
                }
            }
            
            //再次连接ftp进行上报
            try {
            	FTPTool tFTPTool2 = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
                if (!tFTPTool2.loginFTP()){
            		CError tError = new CError();
                    tError.moduleName = "CardActiveBatchImportBL";
                    tError.functionName = "getXls";
                    tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);
                    return false;
	  			 } 
	            if(tFTPTool2.uploadAll(tFileImportBackPath, ResultMax)){
	            	System.out.println("报文上传失败");
	            }
	            tFTPTool2.logoutFTP();
            }catch(Exception e) {
            	e.printStackTrace();
				CError tError = new CError();
		        tError.moduleName = "LLTJJHCaseTransFtpXml";
		        tError.functionName = "getXls";
		        tError.errorMessage = "回传返回报文过程中出现异常";
		        mErrors.addOneError(tError);
		        System.out.println(tError.errorMessage);
		        return false;
            }

        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }



    public String getDealInfo()
    {
        return mDealInfo;
    }

//  建文件夹
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
	
    public static void main(String[] args)
    {
    	LLWBTransFtpXmlCase a =new LLWBTransFtpXmlCase();
    	a.getXls();
    }
}
