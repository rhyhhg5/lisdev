package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;

//test用
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLHospCustomerWS implements ServiceInterface {
    public LLHospCustomerWS() {
    }

    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();
    private MMap tMap = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();

    private Document mInXmlDoc; //传入报文
    private String mDocType = ""; //报文类型
    private boolean mDealState = true; //处理标志
    private String mResponseCode = "1"; //返回类型代码
    private String mRequestType = ""; //请求类型
    private String mErrorMessage = ""; //错误描述
    private String mTransactionNum = ""; //交互编码
    private String mTranHospCode = ""; //交互编码中医院代码
    private Element mBodyData; //报文Body部分

    //医保通导入号
    private String mHWNo = "";

    //客户姓名
    private String mInsuredName = "";
    //客户性别
    private String mInsuredSex = "";
    //客户出生日期
    private String mBirthDate = "";
    //证件类型
    private String mIDType = "";
    //证件号码
    private String mIDNo = "";
    //用户状态
    private String mInsuredState = "";
    //其他号码
    private String mOtherID = "";
    //保单号码
    private String mGrpContNo = "";
    //保障计划
    private String mContPlan = "";
    //生效日期
    private String mCValiDate = "";
    //医院编码
    private String mHospitalCode = "";
    //操作人
    private String mOperatorCode = "";

    private String mManageCom = "";

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LPDiskImportSchema mLPDiskImportSchema = new LPDiskImportSchema();

    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    private VData mVData = new VData();

    public Document service(Document pInXmlDoc) {
        mInXmlDoc = pInXmlDoc;
        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"06".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        if (mInsuredName == null || "".equals(mInsuredName)) {
            buildError("checkData()", "获取报文的【客户姓名】为空!");
            return false;
        }

        if (mInsuredSex == null || "".equals(mInsuredSex)) {
            buildError("checkData()", "获取报文的【客户性别】为空!");
            return false;
        }

        if (!"0".equals(mInsuredSex) && !"1".equals(mInsuredSex) &&
            !"2".equals(mInsuredSex)) {
            buildError("checkData()", "获取报文的【客户性别】错误!");
            return false;
        }

        if (mBirthDate == null || "".equals(mBirthDate)) {
            buildError("checkData()", "获取报文的【客户出生日期】为空!");
            return false;
        }

        if (!PubFun.checkDateForm(mBirthDate)) {
            buildError("checkData()", "【出生日期】格式错误");
            return false;
        }

        if (mIDType == null || "".equals(mIDType)) {
            buildError("checkData()", "获取报文的【客户证件类型】为空!");
            return false;
        }

        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("idtype");
        tLDCodeDB.setCode(mIDType);
        if (!tLDCodeDB.getInfo()) {
            buildError("checkData()", "【证件类型】查询失败");
            return false;
        }

        if (mIDNo == null || "".equals(mIDNo)) {
            buildError("checkData()", "获取报文的【客户证件号码】为空!");
            return false;
        }

        if ("0".equals(mIDType)) {
            if (!PubFun.checkIDNo(mIDNo)) {
                buildError("checkData()", "身份证号错误!");
                return false;
            }
            String tBirtyday = PubFun.getBirthdayFromId(mIDNo);
            if (!tBirtyday.equals(mBirthDate)) {
                buildError("checkData()", "身份证号与生日不符!");
                return false;
            }

            String tSex = PubFun.getSexFromId(mIDNo);
            if (!tSex.equals(mInsuredSex)) {
                buildError("checkData()", "身份证号与性别不符!");
                return false;
            }
        }

        if (mCValiDate != null && !"".equals(mCValiDate)) {
            if (!PubFun.checkDateForm(mCValiDate)) {
                buildError("checkData()", "【生效日期】格式错误");
                return false;
            }
        }

        if (mInsuredState == null || "".equals(mInsuredState)) {
            buildError("checkData()", "获取报文的【人员状态】为空!");
            return false;
        }

        tLDCodeDB.setCodeType("insustat");
        tLDCodeDB.setCode(mInsuredState);
        if (!tLDCodeDB.getInfo()) {
            buildError("checkData()", "【人员状态】查询失败");
            return false;
        }

        if (mGrpContNo == null || "".equals(mGrpContNo)) {
            buildError("checkData()", "获取报文的【保单号码】为空!");
            return false;
        }

        if (mContPlan == null || "".equals(mContPlan)) {
            buildError("checkData()", "获取报文的【保障计划】为空!");
            return false;
        }

        if (mHospitalCode == null || "".equals(mHospitalCode)) {
            buildError("checkData()", "获取报文的【医院编码】为空!");
            return false;
        }

        mTranHospCode = mTransactionNum.substring(2, 9);
        if (!mHospitalCode.equals(mTranHospCode)) {
            buildError("checkData()", "获取报文的【医院编码】错误!");
            return false;
        }

        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        tLDHospitalDB.setHospitCode(mTranHospCode);
        if (!tLDHospitalDB.getInfo()) {
            buildError("checkData()", "该医院查询失败");
            return false;
        }

        mManageCom = tLDHospitalDB.getManageCom();
        if ("".equals(mManageCom)) {
            buildError("checkData()", "医院所属机构错误");
            return false;
        }

        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setSerialNo(mTranHospCode);
        tLPDiskImportDB.setEdorType("WS");
        tLPDiskImportDB.setState("1");
        if (tLPDiskImportDB.query().size() > 0) {
            buildError("checkData()", "该医院上次操作异常，请与核心系统运维人员联系！");
            return false;
        }

        if (mOperatorCode == null || "".equals(mOperatorCode)) {
            buildError("checkData()", "获取报文的【操作人】为空!");
            return false;
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        String tGrpContSQL = "select * from lcgrpcont where grpcontno='" +
                             mGrpContNo + "' and managecom like '" +
                             mManageCom +
                             "%' and appflag = '1' and (stateflag is null or stateflag not in('0'))";
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tGrpContSQL);
        if (tLCGrpContSet.size() <= 0) {
            buildError("checkData()", "保单查询失败！");
            return false;
        }
        mLCGrpContSchema = tLCGrpContSet.get(1);

        if ("2".equals(mLCGrpContSchema.getCardFlag())) {
            buildError("checkData()", "该保单为卡折业务，不能进行实名化！");
            return false;
        }

        if (0 == mLCGrpContSchema.getPrintCount()) {
            buildError("checkData()", "保单未打印，不能进行实名化操作！");
            return false;
        }

        String sql = "select * from LJSPay " +
                     "where OtherNoType = '1' " + //1是团单续期
                     "and OtherNo = '" + mGrpContNo + "' ";
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql);
        if (tLJSPaySet.size() > 0) {
            buildError("checkData()", "该保单处于续期待收费状态，续期核销之后才能进行实名化！");
            return false;
        }

        String tBQSQL = "select b.* from LPEdorApp a, LPGrpEdorItem b " +
                        "where  a.EdorAcceptNo = b.EdorAcceptNo " +
                        "and a.EdorState != '0' " +
//                        "and b.EdorType = 'WS' " +
                        "and b.GrpContNo = '" + mGrpContNo +
                        "' ";
        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(
                tBQSQL);
        if (tLPGrpEdorItemSet.size() > 0) {
            buildError("checkData()", "保单" + mGrpContNo +
                       "下有未结案的" + tLPGrpEdorItemSet.get(1).getEdorType() +
                       "项目受理，受理号为" +
                       tLPGrpEdorItemSet.get(1).getEdorNo() +
                       "，该工单结案后才能继续受理。");

            return false;
        }

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mGrpContNo);
        tLCPolDB.setPolTypeFlag("1");
        tLCPolDB.setContPlanCode(mContPlan);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if (tLCPolSet.size() <= 0) {
            buildError("checkData()", "保单险种查询失败！");
            return false;
        }

        boolean tRiskRightFlag = false;
        for (int i = 1; i <= tLLHospComRightSet.size(); i++) {
            for (int j = 1; j <= tLCPolSet.size(); j++) {
                if (tLLHospComRightSet.get(i).getRiskCode().equals(
                        tLCPolSet.
                        get(j).getRiskCode())) {
                    tRiskRightFlag = true;
                    break;
                }
            }
        }
        if (!tRiskRightFlag) {
            buildError("checkData()", "该保单险种无实名化权限！");
            return false;
        }

        String tDQSQL =
                "select state from lcgrpbalplan where grpcontno = '" +
                mGrpContNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tDQSSRS = tExeSQL.execSQL(tDQSQL);
        if (tDQSSRS.getMaxRow() > 0) {
            if (!"0".equals(tDQSSRS.GetText(1, 1))) {
                buildError("checkData()", "该保单正在进行定期结算，不能进行客户实名化！");
                return false;
            }
        }

        String tWSSQL = "select 1 from LMRiskEdoritem  a, LMEdorItem b "
                        +
                        " where a.edorCode = b.edorCode and b.edorcode != 'XB'"
                        + " and a.riskCode in"
                        +
                        " (select riskCode from LCGrpPol where grpContNo = '" +
                        mGrpContNo +
                        "') and (b.edorTypeFlag != 'N'or b.edorTypeFlag is null)"
                        + " and b.edorcode = 'WS'";
        SSRS tWSSSRS = tExeSQL.execSQL(tWSSQL);
        if (tWSSSRS.getMaxRow() <= 0) {
            if (!"0".equals(tDQSSRS.GetText(1, 1))) {
                buildError("checkData()", "该保单下没有可以进行实名化的保单！");
                return false;
            }
        }
        mGlobalInput.ManageCom = mManageCom;
        mGlobalInput.Operator = mOperatorCode;

        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setGrpContNo(mGrpContNo);
        tLPGrpEdorItemSchema.setEdorNo(mGrpContNo);
        tLPGrpEdorItemSchema.setEdorAcceptNo(mGrpContNo);
        tLPGrpEdorItemSchema.setEdorType("WS");

        CheckEdorItem tCheckEdorItem = new CheckEdorItem();
        tCheckEdorItem.setContType(BQ.CONTTYPE_G);
        tCheckEdorItem.setManageCom(mGlobalInput.ManageCom);
        tCheckEdorItem.setGrpContNo(mGrpContNo);
        tCheckEdorItem.setEdorType("WS");
        tCheckEdorItem.setLPGrpEdorItem(tLPGrpEdorItemSchema);
        if (!tCheckEdorItem.submitData()) {
            buildError("checkData()", tCheckEdorItem.mErrors.getFirstError());
            return false;
        }

        VData inputCheckData = new VData();
        inputCheckData.add(mGlobalInput);
        inputCheckData.add(tLPGrpEdorItemSchema);
        inputCheckData.add("VERIFY||BEGIN");
        inputCheckData.add("GEDORINPUT#EDORTYPE");

        CheckFieldBL tCheckFieldBL = new CheckFieldBL();
        if (!tCheckFieldBL.submitData(inputCheckData, "")) {
            buildError("checkData()", tCheckFieldBL.mErrors.getFirstError());
            return false;
        }

        DisabledManageBL tDisabledManageBL = new DisabledManageBL();
        if (!tDisabledManageBL.dealDisabledcont(tLPGrpEdorItemSchema.
                                                getGrpContNo(),
                                                tLPGrpEdorItemSchema.
                                                getEdorType(),
                                                2)) {
            buildError("checkData()",
                       "保单" + tLPGrpEdorItemSchema.getGrpContNo() +
                       "下" + tLPGrpEdorItemSchema.getEdorType() +
                       "项目在" + CommonBL.getCodeName("stateflag",
                    tDisabledManageBL.getState()) + "状态下不能添加!");
            return false;
        }

        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        tLCInsuredSchema.setGrpContNo(mGrpContNo);
        tLCInsuredSchema.setName(mInsuredName);
        tLCInsuredSchema.setSex(mInsuredSex);
        tLCInsuredSchema.setBirthday(mBirthDate);
        tLCInsuredSchema.setIDType(mIDType);
        tLCInsuredSchema.setIDNo(mIDNo);
        OldCustomerCheck tOldCustomerCheck = new OldCustomerCheck(
                tLCInsuredSchema);

        if (tOldCustomerCheck.checkInsured() == OldCustomerCheck.OLD) {
            buildError("checkData()", "该保单下已存在该被保人！");
            return false;
        }

        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        Element tBasepart = tBodyData.getChild("BASE_PART");

        mInsuredName = tBasepart.getChildTextTrim("PERSON_NAME").trim();
        mInsuredSex = tBasepart.getChildTextTrim("SEX").trim();
        mBirthDate = tBasepart.getChildTextTrim("BIRTH_DATE").trim();
        mIDType = tBasepart.getChildTextTrim("ID_TYPE").trim();
        mIDNo = tBasepart.getChildTextTrim("ID_NUMBER").trim();
        mInsuredState = tBasepart.getChildTextTrim("BEING_STATE_CODE").trim();
        mOtherID = tBasepart.getChildTextTrim("OTHER_ID").trim();
        mGrpContNo = tBasepart.getChildTextTrim("INSURANCE_POLICY_NUMBER").
                     trim();
        mContPlan = tBasepart.getChildTextTrim("CONT_PLAN").trim();
        mCValiDate = tBasepart.getChildTextTrim("CVALIDATE").trim();
        mHospitalCode = tBasepart.getChildTextTrim("HOSPITAL_NUMBER").trim();
        mOperatorCode = tBasepart.getChildTextTrim("OPERATOR").trim();

        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        if (!importData()) {
            return false;
        }

        if (!claim()) {
            updateState();
            return false;
        }

        if (!confirm()) {
            return false;
        }

        return true;
    }

    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
            return createResultXML();
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "1";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);

        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospCustomerWS";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 保存数据
     * @return boolean
     * @throws Exception
     */
    private boolean importData() throws Exception {
        System.out.println("LLHospCustomerWS--importData");
        String tLimit = PubFun.getNoLimit(mManageCom);
        System.out.println("管理机构代码是 : " + tLimit);
        mHWNo = PubFun1.CreateMaxNo("HWNO", tLimit);
        System.out.println("HWNo:" + mHWNo);
        mLPDiskImportSchema.setEdorNo(mHWNo);
        mLPDiskImportSchema.setEdorType("WS");
        mLPDiskImportSchema.setGrpContNo(mGrpContNo);
        mLPDiskImportSchema.setSerialNo(mTranHospCode);
        mLPDiskImportSchema.setState("1");
        mLPDiskImportSchema.setEmployeeName(mInsuredName);
        mLPDiskImportSchema.setInsuredName(mInsuredName);
        //默认本人
        mLPDiskImportSchema.setRelation("00");
        mLPDiskImportSchema.setSex(mInsuredSex);
        mLPDiskImportSchema.setBirthday(mBirthDate);
        mLPDiskImportSchema.setIDType(mIDType);
        mLPDiskImportSchema.setIDNo(mIDNo);
        mLPDiskImportSchema.setRetire(mInsuredState);
        mLPDiskImportSchema.setContPlanCode(mContPlan);

        if (mCValiDate == null || "".equals(mCValiDate)) {
            mLPDiskImportSchema.setEdorValiDate(mLCGrpContSchema.
                                                getCValiDate());
        } else {
            mLPDiskImportSchema.setEdorValiDate(mCValiDate);
        }

        mLPDiskImportSchema.setOperator(mOperatorCode);
        mLPDiskImportSchema.setMakeDate(mCurrentDate);
        mLPDiskImportSchema.setMakeTime(mCurrentTime);
        mLPDiskImportSchema.setModifyDate(mCurrentDate);
        mLPDiskImportSchema.setModifyTime(mCurrentTime);
        mLPDiskImportSchema.setOthIDNo(mOtherID);
        mLPDiskImportSchema.setImportFileName(mTransactionNum);

        tMap.put(mLPDiskImportSchema, "INSERT");

        mResult.clear();
        mResult.add(tMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            buildError("dealData()", "数据库保存失败！");
            return false;
        }
        return true;
    }

    /**
     * 调用保全理算类
     * @return boolean
     * @throws Exception
     */
    private boolean claim() throws Exception {
        System.out.println("LLHospCustomerWS--claim");
        LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
        tLPGrpEdorItemSchema.setEdorNo(mLPDiskImportSchema.getEdorNo());
        tLPGrpEdorItemSchema.setEdorType(mLPDiskImportSchema.getEdorType());
        tLPGrpEdorItemSchema.setGrpContNo(mLPDiskImportSchema.getGrpContNo());

        mVData.add(mGlobalInput);
        mVData.add(tLPGrpEdorItemSchema);

        GrpEdorWSAppConfirmBL tGrpEdorWSAppConfimBL = new
                GrpEdorWSAppConfirmBL();
        if (!tGrpEdorWSAppConfimBL.submitData(mVData, "")) {
            buildError("claim()",
                       tGrpEdorWSAppConfimBL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * 调用保全确认
     * @return boolean
     * @throws Exception
     */
    private boolean confirm() throws Exception {
        System.out.println("LLHospCustomerWS--confirm");
        GrpEdorWSConfirmBL tGrpEdorWSConfirmBL = new GrpEdorWSConfirmBL();
        if (!tGrpEdorWSConfirmBL.submitData(mVData, "")) {
            buildError("confirm()",
                       tGrpEdorWSConfirmBL.mErrors.getFirstError());
            return false;
        }

        return true;
    }

    /**
     * 理算失败维护
     * @return boolean
     * @throws Exception
     */
    private boolean updateState() throws Exception {
        System.out.println("LLHospCustomerWS--updateState");
        String tUpdateSQL = "update LPDiskImport " +
                            "set State = '0', " +
                            " ErrorReason = '" + mErrors.getFirstError() +
                            "' " +
                            "where EdorNo = '" + mHWNo + "' " +
                            "and EdorType = 'WS' " +
                            "and GrpContNo = '" + mGrpContNo + "' " +
                            "and SerialNo = '" + mTranHospCode +
                            "' ";
        MMap tMap = new MMap();
        tMap.put(tUpdateSQL, "UPDATE");
        mResult.clear();
        mResult.add(tMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            buildError("dealData()", "数据库保存失败！");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "D:/hosp06.xml"), "GBK");

            LLHospCustomerWS tBusinessDeal = new LLHospCustomerWS();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String tIDNo = "110108740404495";
//        System.out.println(PubFun.checkIDNo(tIDNo));
    }
}
