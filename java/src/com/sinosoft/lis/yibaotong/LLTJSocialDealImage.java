package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLTJSocialDealImage implements ServiceInterface{

	public LLTJSocialDealImage() {
		super();
	}
	
	public CErrors mErrors = new CErrors();
	public VData mResult = new VData();
	private MMap map = new MMap();
	private String mErrorMessage = "";	//错误描述 
	private PubFun pf =new PubFun();
	private VData mInputData = new VData();
	
	private Document mInXmlDoc;	 			//传入报文  
	private Document mOutXmlDoc;			//输出报文
    private String mDocType = "";			//报文类型    
    private String mDocVersion = "";		//报文版本
    private boolean mDealState = true;		//处理标志    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码 
    
    private Element mLLCaseList;
    private Element mLLCaseData;
    private Element mVersionData;
    
    private String mVersionCode = "";	//案件类型--版本号
    private String mCaseno = "";		//案件号
    private String mPicPath = "";		//影像件路径

	public Document service(Document pInXmlDoc) {
		//程序入口
		System.out.println("LLSocialDealImageSup-----------service()------");
		mInXmlDoc = pInXmlDoc;
		try {
			if(!getInputData(mInXmlDoc)) {
				mDealState = false;
			}else {
				if(!checkData()) {
					mDealState = false;
				}else {
					int length = mLLCaseList.getChildren("LLCASE_DATA").size();
					if(length >= 1) {
						for (int i = 0; i < length; i++) {
							mLLCaseData = (Element) mLLCaseList.getChildren("LLCASE_DATA").get(i);
							mCaseno = "";
							mPicPath = "";
							String[] str1 = new String[3];
							if(!dealData()) {
								str1[0] = mCaseno;
								str1[1] = "2";//处理案件错误
								str1[2] = mErrors.getFirstError();
								mResult.add(str1);
								continue;
							}else {
								str1[0] = mCaseno;
								str1[1] = "1";//处理案件成功S
								str1[2] = "";
								mResult.add(str1);
							}
						}
					}else {
						buildError("service()", "报文有问题");
						mDealState = false;
					}
				}
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
            mDealState = false;
            buildError("service()", "系统未知错误");
		}finally {
			mOutXmlDoc = createXML();
		}
		return mOutXmlDoc;
	}
	
	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
    	
    	System.out.println("LLSocialDealImageSup-----------createXML");
    	
    	return createResultXML();
    	
    }
    
    
    /**
     * 返回正确的报文
     */
    private Document createResultXML() {
    	System.out.println("LLSocialDealImageSup-----------createResultXML");
    	
    	Element tRootData = new Element("PACKET");
    	tRootData.addAttribute("type", "RESPONSE");
    	tRootData.addAttribute("version", "1.0");
    	//创建头部
    	Element tHeadData = new Element("HEAD");
    	Element tRequestType = new Element("REQUEST_TYPE");
    	tRequestType.setText(mRequestType);
    	tHeadData.addContent(tRequestType);
    	
    	Element tTransactionNum = new Element("TRANSACTION_NUM");
    	tTransactionNum.setText(mTransactionNum);
    	tHeadData.addContent(tTransactionNum);
    	
    	tRootData.addContent(tHeadData);
    	
    	Element tBodyData = new Element("BODY");
    	Element tLLCaseList = new Element("CASE_LIST");
    	int number = mResult.size();
    	if(number > 0) {
    		for (int i = 0; i < mResult.size(); i++) {
        		Element tLLCaseDATA = new Element("CASE_DATA");
        		String[] arr = (String[]) mResult.get(i);
        		String sCaseno = arr[0];
        		String sSuccess = arr[1];
        		String sError = arr[2];
        		Element tCaseElement = new Element("CASENO");
        		tCaseElement.setText(sCaseno);
        		tLLCaseDATA.addContent(tCaseElement);
        		
        		Element tSuccessElement = new Element("SUCCESS");
        		tSuccessElement.setText(sSuccess);
        		tLLCaseDATA.addContent(tSuccessElement);
        		
        		Element tMessageElement = new Element("MESSAGE");
        		tMessageElement.setText(sError);
        		tLLCaseDATA.addContent(tMessageElement);
        		
        		tLLCaseList.addContent(tLLCaseDATA);
    		}
    	}else {
    		
    		Element tLLCaseDATA = new Element("CASE_DATA");
    		Element tCaseElement = new Element("CASENO");
    		tCaseElement.setText("");
    		tLLCaseDATA.addContent(tCaseElement);
    		
    		Element tSuccessElement = new Element("SUCCESS");
    		tSuccessElement.setText("2");
    		tLLCaseDATA.addContent(tSuccessElement);
    		
    		Element tMessageElement = new Element("MESSAGE");
    		tMessageElement.setText(mErrors.getFirstError());
    		tLLCaseDATA.addContent(tMessageElement);
    		
    		tLLCaseList.addContent(tLLCaseDATA);
    		
    	}
    	
    	tBodyData.addContent(tLLCaseList);
    	
    	tRootData.addContent(tBodyData);
    		
    	Document tDocument = new Document(tRootData);
    	return tDocument;
    }
	
	/**
	 * 处理数据
	 * @return
	 */
	private boolean dealData() {
		
		System.out.println("LLSocialDealImageSup-----------dealData()------");
		mCaseno = mLLCaseData.getChildTextTrim("CASENO");
		mPicPath = mLLCaseData.getChildTextTrim("PICPATH");
		if("".equals(mCaseno) || "null".equals(mCaseno) || mCaseno==null) {
			buildError("dealData()", "案件号不能为空");
			return false;
		}
		if("".equals(mPicPath) || "null".equals(mPicPath) || mPicPath==null) {
			buildError("dealData()", "影像件路径不能为空");
			return false;
		}
		if(!mPicPath.endsWith(".zip")) {
    		buildError("dealTYScan()", "影像件路径不完整");  
    		return false;
    	}
		String SNo = LLCaseCommon.checkSocialSecurity(mCaseno, "null");
		if(!"1".equals(SNo)) {
			buildError("dealData()", "该"+mCaseno+"案件不是社保案件");
            //return false;
		}
		String mCaseSQL = "select mngcom,operator from llcase where 1=1 and caseno = '" 
				  + mCaseno + "' with ur";
		ExeSQL CaseExeSQL = new ExeSQL();
		SSRS CaseSSRS = CaseExeSQL.execSQL(mCaseSQL);
		int size = CaseSSRS.getMaxRow();
		if(size<1) {
			buildError("dealData()", "该"+mCaseno+"案件有问题");
            return false;
		}
		String tManageCom = CaseSSRS.GetText(1, 1);
		String tOperator = CaseSSRS.GetText(1, 2);
		
		if(mPicPath!=null && !"".equals(mPicPath) && !"null".equals(mPicPath) && mPicPath!="") {			
			//影像件数据处理
			ImageHandler tImageHandler = new ImageHandler(mPicPath, mCaseno, mVersionCode,tManageCom, tOperator);
			if(!tImageHandler.dealImage()) {
				buildError("dealTYScan()", "影像件处理有问题"); 
				return false;
			}
		}

		return true;
	}
	
	/**
	 * 校验报文信息
	 * @return
	 */
	private boolean checkData() {
		
		System.out.println("LLSocialDealImageSup-----------checkData()------");
		if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }
		if (!"TJ06".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }
		if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
	            buildError("checkData()", "【交互编码】的编码个数错误");
	            return false;
	        }
		if (!"TJ06".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }
		String str = mTransactionNum.substring(4, 12);        
        if ("".equals(str) || str == null || str.equals("null")|| !"86120000".equals(str)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
        if("".equals(mVersionCode) || "null".equals(mVersionCode) || mVersionCode == null) {
        	buildError("checkData()", "【版本信息】的值有误");
            return false;
        }
		return true;
	}
	
	/**
	 * 解析报文主要部分
	 * @param cInXml
	 * @return
	 */
	private boolean getInputData(Document cInXml) {
		System.out.println("LLSocialDealImageSup-----------getInputData()------");
		if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }
		Element tRootData = cInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");
		mDocVersion = tRootData.getAttributeValue("version");
		//获取head部分
		Element tHeadData = tRootData.getChild("HEAD");
		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
		//获取body部分
		Element tBodyData = tRootData.getChild("BODY");
		mVersionData = tBodyData.getChild("VERSION_DATA");
		mVersionCode = mVersionData.getChildTextTrim("VERSIONCODE");
		mLLCaseList = tBodyData.getChild("LLCASE_LIST");
		
		return true;
	}
    
	 /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLWBRgtRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args) {
    	 Document tInXmlDoc;    
         try {
             tInXmlDoc = JdomUtil.build(new FileInputStream("D:/Java/test/TJ06/TJ06.xml"), "GBK");
             LLTJSocialDealImage tLLOnlinClaimGetImage = new LLTJSocialDealImage();
             Document tOutXmlDoc = tLLOnlinClaimGetImage.service(tInXmlDoc);
             System.out.println("打印传入报文============");
             JdomUtil.print(tInXmlDoc.getRootElement());
             System.out.println("打印传出报文============");
             JdomUtil.print(tOutXmlDoc);
         	        	
         } catch (Exception e) {
             e.printStackTrace();
         }
	}


	
}
