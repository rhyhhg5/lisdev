package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLOnlinGiveInfoQry implements ServiceInterface{

	public LLOnlinGiveInfoQry() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CErrors mErrors = new CErrors();

	public VData mResult = new VData();
	// 传入报文
	private Document mInXmlDoc;
	// 报文类型
	private String mDocType = "";
	// 处理标志
	private boolean mDealState = true;
	// 返回类型代码
	private String mResponseCode = "1";
	// 请求类型
	private String mRequestType = "";
	// 错误描述
	private String mErrorMessage = "";
	// 交互编码
	private String mTransactionNum = "";
	// 查询的案件号
	private String mCaseno = "";
	//参数使用
	private SSRS ssSSRS = new SSRS();
	private SSRS SSRS1 = new SSRS();
	private SSRS SSRS2 = new SSRS();
	private SSRS SSRS3 = new SSRS();
	private SSRS SSRS4 = new SSRS();
	private SSRS SSRS5 = new SSRS();
	private SSRS SSRS6 = new SSRS();
	private SSRS SSRS7 = new SSRS();
	private SSRS SSRS8 = new SSRS();
	private SSRS SSRS9 = new SSRS();
	private SSRS SSRS10 = new SSRS();
	private SSRS SSRS11 = new SSRS();
	
	
	public Document service(Document pInXmlDoc) {
		
		System.out.println("LLOnlinGiveInfoQry-----------------service");
		mInXmlDoc = pInXmlDoc;
		
		try {
			if(!getInputData(mInXmlDoc)) {
				mDealState = false;
			}else {
				if(!checkData()) {
					mDealState = false;
				}else {
					if(!dealData()) {
						mDealState = false;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mDealState = false;
			mResponseCode = "E";
			buildError("service()", "系统未知错误");
		}finally {
			return createXML();
		}
	}
	
	/**
	 * 生成返回的报文信息
	 * 
	 * @return Document
	 */
	private Document createXML() {
		if (!mDealState) {
			return createFalseXML();
		}else{
			try {
				return createResultXML();
			} catch (Exception ex) {
				buildError("createXML()", "生成返回报文错误");
				return createFalseXML();
			}
		}
	}
	
	/**
	 * 生成错误信息报文
	 * 
	 * @return Document
	 */
	private Document createFalseXML() {
		System.out.println("LLOnlinClaimInfoQry--createResultXML(返回错误报文)");
	    Element tRootData = new Element("PACKET");
	    tRootData.addAttribute("type", "RESPONSE");
	    tRootData.addAttribute("version", "1.0");

	    //Head部分
	    Element tHeadData = new Element("HEAD");

	    Element tRequestType = new Element("REQUEST_TYPE");
	    tRequestType.setText(mRequestType);
	    tHeadData.addContent(tRequestType);


	    Element tTransactionNum = new Element("TRANSACTION_NUM");
	    tTransactionNum.setText(mTransactionNum);
	    tHeadData.addContent(tTransactionNum);	    
	    System.out.println("开始返回错误报文生成!-----");
        Element response = new Element("RESPONSE_CODE");
        mErrorMessage = mErrors.getFirstError();
        response.setText("0");
        tHeadData.addContent(response);
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);
        tRootData.addContent(tHeadData);
        Document tDocument = new Document(tRootData);
        return tDocument;
	}
	
	/**
	 * 处理需要返回正确的报文
	 * @return
	 */
	private Document createResultXML() {
		
		System.out.println("LLOnlinGiveInfoQry----------------dealData");
		
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");

		// Head部分
		Element tHeadData = new Element("HEAD");

		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);

		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);
		
		tRootData.addContent(tHeadData);
		
		Element tBodyData = new Element("BODY");
		Element llcaseData = new Element("LLCASE");
		Element insuredName = new Element("INSUREDNAME");
		insuredName.setText(SSRS1.GetText(1, 1));
		llcaseData.addContent(insuredName);
		Element insuredNo = new Element("INSUREDNO");
		insuredNo.setText(SSRS1.GetText(1, 2));
		llcaseData.addContent(insuredNo);
		Element idType = new Element("IDTYPE");
		idType.setText(SSRS1.GetText(1, 3));
		llcaseData.addContent(idType);
		Element idNo = new Element("IDNO");
		idNo.setText(SSRS1.GetText(1, 4));
		llcaseData.addContent(idNo);
		Element rgtantName = new Element("RGTANTNAME");
		rgtantName.setText(SSRS2.GetText(1, 1));
		llcaseData.addContent(rgtantName);
		Element rgtantRelation = new Element("RGTANTRELATIOIN");
		rgtantRelation.setText(SSRS2.GetText(1, 2));
		llcaseData.addContent(rgtantRelation);
		Element rgtantIdNo = new Element("RGTANTIDNO");
		rgtantIdNo.setText(SSRS2.GetText(1, 3));
		llcaseData.addContent(rgtantIdNo);
		Element rgtantIdType = new Element("RGTANTIDTYPE");
		rgtantIdType.setText(SSRS2.GetText(1, 4));
		llcaseData.addContent(rgtantIdType);
		Element rgtantPhone = new Element("RGTANTPHONE");
		rgtantPhone.setText(SSRS2.GetText(1, 5));
		llcaseData.addContent(rgtantPhone);
		Element caseGetMode = new Element("CASEGETMODE");
		caseGetMode.setText(SSRS1.GetText(1, 5));
		llcaseData.addContent(caseGetMode);
		Element bankNo = new Element("BANKNO");
		bankNo.setText(SSRS1.GetText(1, 6));
		llcaseData.addContent(bankNo);
		Element bankName = new Element("BANKNAME");
		bankName.setText(SSRS1.GetText(1, 7));
		llcaseData.addContent(bankName);
		Element accList = new Element("ACCLIST");
		ExeSQL tExeSQL = new ExeSQL();
		
		//查询案件相关的事件号
		String mmSQL2 = "select caserelano,subrptno from llcaserela where caseno = '" + mCaseno + "' with ur";
		ssSSRS = tExeSQL.execSQL(mmSQL2);
		
		if(ssSSRS.getMaxRow()>0) {			
			for (int i = 1; i <= ssSSRS.getMaxRow(); i++) {				
				String tCaserelano = ssSSRS.GetText(i, 1);
				String tSubrptno = ssSSRS.GetText(i, 2);
				//查询事件
				String str3 = "select " + 
							  " lls.accdate 发生日期," + 
							  "(case when lls.accidenttype = '1' then '疾病' when  lls.accidenttype = '2' then '意外' end) 事件类型, " + 
							  "(select codename from ldcode1 where codetype = 'province1' and code = lls.accprovincecode) 省," + 
							  "(select codename from ldcode1 where codetype = 'city1' and code = lls.acccitycode) 市," + 
							  "(select codename from ldcode1 where codetype = 'county1' and code = lls.acccountycode) 县 " + 
							  "from llsubreport lls where lls.subrptno = '" + tSubrptno + "' with ur";
				SSRS3 = tExeSQL.execSQL(str3);
				Element accData = new Element("ACCDATA");
				Element accDate = new Element("ACCDATE");
				accDate.setText(SSRS3.GetText(1, 1));
				accData.addContent(accDate);
				Element accDescType = new Element("ACCDESCTYPE");
				accDescType.setText(SSRS3.GetText(1, 2));
				accData.addContent(accDescType);
				Element accProvinceCode = new Element("ACCPROVINCECODE");
				accProvinceCode.setText(SSRS3.GetText(1, 3));
				accData.addContent(accProvinceCode);
				Element accCityCode = new Element("ACCCITYCODE");
				accCityCode.setText(SSRS3.GetText(1, 4));
				accData.addContent(accCityCode);
				Element accCountyCode = new Element("ACCCOUNTYCODE");
				accCountyCode.setText(SSRS3.GetText(1, 5));
				accData.addContent(accCountyCode);
				//查询账单信息
				Element receiptList = new Element("RECEIPTLIST");
				String str4 = "select llf.feeatti 账单属性, llf.feetype 账单类型, llf.receiptno 账单号, llf.hosgrade 医院级别,llf.hospitalcode 医院编码,llf.hospitalname 医院名称, " + 
							  "llf.hospstartdate 入院日期,llf.hospenddate 出院日期,llf.feedate 账单日期,llf.realhospdate 治疗天数,llf.mainfeeno,llf.age 年龄  from llfeemain llf where " + 
							  "llf.caseno = '" + mCaseno + "' and llf.caserelano = '" + tCaserelano + "' and llf.feeatti = '0' with ur";

				SSRS4 = tExeSQL.execSQL(str4);
				if(SSRS4.getMaxRow()>0) {
					for (int j = 1; j<=SSRS4.getMaxRow(); j++) {					
						Element receiptData = new Element("RECEIPTDATA");
						receiptData.addAttribute("FEEATTIC", "0");
						Element feeatti = new Element("FEEATTI");
						feeatti.setText(SSRS4.GetText(j, 1));
						receiptData.addContent(feeatti);
						Element feetype = new Element("FEETYPE");
						feetype.setText(SSRS4.GetText(j, 2));
						receiptData.addContent(feetype);
						Element mainfeeno = new Element("MAINFEENO");
						mainfeeno.setText(SSRS4.GetText(j, 3));
						receiptData.addContent(mainfeeno);
						Element age = new Element("AGE");
						age.setText(SSRS4.GetText(j, 12));
						receiptData.addContent(age);
						Element hosgrade = new Element("HOSGRADE");
						hosgrade.setText(SSRS4.GetText(j, 4));
						receiptData.addContent(hosgrade);
						Element hosipitalno = new Element("HOSPITALNO");
						hosipitalno.setText(SSRS4.GetText(j, 5));
						receiptData.addContent(hosipitalno);
						Element hospitalName = new Element("HOSPITALNAME");
						hospitalName.setText(SSRS4.GetText(j, 6));
						receiptData.addContent(hospitalName);
						Element hospStartDate = new Element("HOSPSTARTDATE");
						hospStartDate.setText(SSRS4.GetText(j, 7));
						receiptData.addContent(hospStartDate);
						Element hospEndDate = new Element("HOSPENDDATE");
						hospEndDate.setText(SSRS4.GetText(j, 8));
						receiptData.addContent(hospEndDate);
						Element feeDate = new Element("FEEDATE");
						feeDate.setText(SSRS4.GetText(j, 9));
						receiptData.addContent(feeDate);
						Element realHospDate = new Element("REALHOSPDATE");
						realHospDate.setText(SSRS4.GetText(j, 10));
						receiptData.addContent(realHospDate);
						Element feeList = new Element("FEELIST");
						String str5 = "select feeitemcode 费用项目代码,feeitemname 费用项目名称,fee 费用总金额,selfamnt 社保外费用,preamnt 先期给付,"
								+ "refuseamnt 不合理费用 from llcasereceipt where mainfeeno = '" + SSRS4.GetText(j, 11) + "' order by feeitemcode with ur";
						SSRS5 = tExeSQL.execSQL(str5);
						if(SSRS5.getMaxRow()>0) {
							for (int k = 1; k <= SSRS5.getMaxRow(); k++) {
								Element feeData = new Element("FEEDATA");
								Element feeItemCode = new Element("FEEITEMCODE");
								feeItemCode.setText(SSRS5.GetText(k, 1));
								feeData.addContent(feeItemCode);
								Element feeItemName = new Element("FEEITEMNAME");
								feeItemName.setText(SSRS5.GetText(k, 2));
								feeData.addContent(feeItemName);
								Element sumFee = new Element("FEE");
								sumFee.setText(SSRS5.GetText(k, 3));
								feeData.addContent(sumFee);
								Element selfAmnt = new Element("SELFAMNT");
								selfAmnt.setText(SSRS5.GetText(k, 4));
								feeData.addContent(selfAmnt);
								Element preAmnt = new Element("PREAMNT");
								preAmnt.setText(SSRS5.GetText(k, 5));
								feeData.addContent(preAmnt);
								Element refuseAmnt = new Element("REFUSEAMNT");
								refuseAmnt.setText(SSRS5.GetText(k, 6));
								feeData.addContent(refuseAmnt);
								feeList.addContent(feeData);
							}
						}
						receiptData.addContent(feeList);
						receiptList.addContent(receiptData);
					}
				}
				
				String str6 = "select llf.feeatti 账单属性, llf.feetype 账单类型, llf.receiptno 账单号,llf.insuredstat 人员类别, llf.hosgrade 医院级别,llf.hospitalcode 医院编码,llf.hospitalname 医院名称, " + 
							  "llf.hospstartdate 入院日期,llf.hospenddate 出院日期,llf.feedate,llf.realhospdate 治疗天数 ,llf.mainfeeno,llf.age 年龄 " + 
							  "from llfeemain llf where llf.caseno = '" + mCaseno + "' and llf.caserelano = '" + tCaserelano + "' and llf.feeatti = '4' with ur";
				SSRS6 = tExeSQL.execSQL(str6);
				if(SSRS6.getMaxRow()>0) {
					for (int k = 1; k <= SSRS6.getMaxRow(); k++) {
						Element receiptData = new Element("RECEIPTDATA");
						receiptData.addAttribute("FEEATTIC", "4");
						Element feeatti = new Element("FEEATTI");
						feeatti.setText(SSRS6.GetText(k, 1));
						receiptData.addContent(feeatti);
						Element feeType = new Element("FEETYPE");
						feeType.setText(SSRS6.GetText(k, 2));
						receiptData.addContent(feeType);
						Element mainFeeNo = new Element("MAINFEENO");
						mainFeeNo.setText(SSRS6.GetText(k, 3));
						receiptData.addContent(mainFeeNo);
						Element insuredStat = new Element("INSUREDSTAT");
						insuredStat.setText(SSRS6.GetText(k, 4));
						receiptData.addContent(insuredStat);
						Element age = new Element("AGE");
						age.setText(SSRS6.GetText(k, 13));
						receiptData.addContent(age);
						Element hosGrade = new Element("HOSGRADE");
						hosGrade.setText(SSRS6.GetText(k, 5));
						receiptData.addContent(hosGrade);
						Element hosipitalNo = new Element("HOSPITALNO");
						hosipitalNo.setText(SSRS6.GetText(k, 6));
						receiptData.addContent(hosipitalNo);
						Element hospitalName = new Element("HOSPITALNAME");
						hospitalName.setText(SSRS6.GetText(k, 7));
						receiptData.addContent(hospitalName);
						Element hospStartDate = new Element("HOSPSTARTDATE");
						hospStartDate.setText(SSRS6.GetText(k, 8));
						receiptData.addContent(hospStartDate);
						Element hospEndDate = new Element("HOSPENDDATE");
						hospEndDate.setText(SSRS6.GetText(k, 9));
						receiptData.addContent(hospEndDate);
						Element feeDate = new Element("FEEDATE");
						feeDate.setText(SSRS6.GetText(k, 10));
						receiptData.addContent(feeDate);
						Element realHospDate = new Element("REALHOSPDATE");
						realHospDate.setText(SSRS6.GetText(k, 11));
						receiptData.addContent(realHospDate);
						Element feeList = new Element("FEELIST");
						String str7 = "select feeitemcode 费用项目代码,feeitemname 费用项目名称,fee 费用总金额  from llcasereceipt where mainfeeno = '" + SSRS6.GetText(k, 12) + "' order by feeitemcode with ur";
						SSRS7 = tExeSQL.execSQL(str7);
						if(SSRS7.getMaxRow()>0) {
							for (int j = 1; j <= SSRS7.getMaxRow(); j++) {
								Element feeData = new Element("FEEDATA");
								Element feeItemCode = new Element("FEEITEMCODE");
								feeItemCode.setText(SSRS7.GetText(j, 1));
								feeData.addContent(feeItemCode);
								Element feeItemName = new Element("FEEITEMNAME");
								feeItemName.setText(SSRS7.GetText(j, 2));
								feeData.addContent(feeItemName);
								Element fee = new Element("FEE");
								fee.setText(SSRS7.GetText(j, 3));
								feeData.addContent(fee);
								feeList.addContent(feeData);
							}
						}
						receiptData.addContent(feeList);
						Element itemList = new Element("ITEMLIST");
						String str8 = "select itemcode 费用代码,drugname 费用名称,feemoney 费用金额 from llfeeotheritem where 1=1 and mainfeeno = '" + 
									  SSRS6.GetText(k, 12) + "' order by itemcode with ur";
						SSRS8 = tExeSQL.execSQL(str8);
						if(SSRS8.getMaxRow()>0) {
							for (int j = 1; j <= SSRS8.getMaxRow(); j++) {								
								Element itemData = new Element("ITEMDATA");
								Element itemCode = new Element("ITEMCODE");
								itemCode.setText(SSRS8.GetText(j, 1));
								itemData.addContent(itemCode);
								Element itemName = new Element("ITEMNAME");
								itemName.setText(SSRS8.GetText(j, 2));
								itemData.addContent(itemName);
								Element feeMoney = new Element("FEEMONEY");
								feeMoney.setText(SSRS8.GetText(j, 3));
								itemData.addContent(feeMoney);
								itemList.addContent(itemData);
							}
						}
						receiptData.addContent(itemList);
						receiptList.addContent(receiptData);
					}
				}
				accData.addContent(receiptList);
				//查询理赔明细
				Element policyList = new Element("POLICYLIST");
				String str9 = "select  llc.contno 保单号,llc.riskcode 险种编码,(select riskname from lmriskapp where riskcode = llc.riskcode) 险种名称,llc.getdutycode 给付责任编码," + 
							  "(select getdutyname from LMDutyGetClm where getdutycode = llc.getdutycode and getdutykind = llc.getdutykind)给付责任名称,llc.getdutykind 给付责任类型编码," + 
							  "(select codename from ldcode where codetype = 'getdutykind' and code = llc.getdutykind) 给付责任类型名称," + 
							  "llc.claimmoney 理算金额,llc.outdutyamnt 免赔额,llc.outdutyrate 给付比例,llc.realpay 实赔金额,llc.givetype 赔付结论,llc.givereason 赔付结论依据 " +
							  "from llclaimdetail llc where llc.caseno = '" + mCaseno + "' and llc.caserelano = '" + tCaserelano + "' with ur";
				SSRS9 = tExeSQL.execSQL(str9);
				if(SSRS9.getMaxRow()>0) {
					for (int j = 1; j <= SSRS9.getMaxRow(); j++) {
						Element policyData = new Element("POLICYDATA");
						Element contno = new Element("CONTNO");
						contno.setText(SSRS9.GetText(j, 1));
						policyData.addContent(contno);
						Element riskcode = new Element("RISKCODE");
						riskcode.setText(SSRS9.GetText(j, 2));
						policyData.addContent(riskcode);
						Element riskName = new Element("RISKNAME");
						riskName.setText(SSRS9.GetText(j, 3));
						policyData.addContent(riskName);
						Element getDutyCode = new Element("GETDUTYCODE");
						getDutyCode.setText(SSRS9.GetText(j, 4));
						policyData.addContent(getDutyCode);
						Element getDutyName = new Element("GETDUTYNAME");
						getDutyName.setText(SSRS9.GetText(j, 5));
						policyData.addContent(getDutyName);
						Element getDutyKind = new Element("GETDUTYKIND");
						getDutyKind.setText(SSRS9.GetText(j, 6));
						policyData.addContent(getDutyKind);
						Element getDutyKindName = new Element("GETDUTYKINDNAME");
						getDutyKindName.setText(SSRS9.GetText(j, 7));
						policyData.addContent(getDutyKindName);
						Element claimMoney = new Element("CLAIMMONEY");
						claimMoney.setText(SSRS9.GetText(j, 8));
						policyData.addContent(claimMoney);
						Element outDutyAmnt = new Element("OUTDUTYAMNT");
						outDutyAmnt.setText(SSRS9.GetText(j, 9));
						policyData.addContent(outDutyAmnt);
						Element giveRate = new Element("GIVERATE");
						giveRate.setText(SSRS9.GetText(j, 10));
						policyData.addContent(giveRate);
						Element realPay = new Element("REALPAY");
						realPay.setText(SSRS9.GetText(j, 11));
						policyData.addContent(realPay);
						Element giveType = new Element("GIVETYPE");
						giveType.setText(SSRS9.GetText(j, 12));
						policyData.addContent(giveType);
						Element giveReason = new Element("GIVEREASON");
						giveReason.setText(SSRS9.GetText(j, 13));
						policyData.addContent(giveReason);
						policyList.addContent(policyData);
					}
				}
				accData.addContent(policyList);
				accList.addContent(accData);
				
			}
		}
		//理赔总金额
		String str10 = "select givetype,givetypedesc,realpay from llclaim where caseno = '" + mCaseno + "' with ur";
		SSRS10 = tExeSQL.execSQL(str10);
		Element realAllPay = new Element("REALALLPAY");
		if(SSRS10.getMaxRow()>0) {	
			Element allGiveType =new Element("AllGIVETYPE");
			allGiveType.setText(SSRS10.GetText(1, 1));
			realAllPay.addContent(allGiveType);
			Element allGiveTypeDesc =new Element("AllGIVETYPEDESC");
			allGiveTypeDesc.setText(SSRS10.GetText(1, 2));
			realAllPay.addContent(allGiveTypeDesc);
			Element allPay =new Element("ALLPAY");
			allPay.setText(SSRS10.GetText(1, 3));
			realAllPay.addContent(allPay);
		}
		accList.addContent(realAllPay);
		llcaseData.addContent(accList);
		//给付明细
		String str11 = "select lja.drawer 领款人姓名, " + 
					   "(select idtype from llcase where caseno = ljc.otherno) 领款人证件类型, " + 
					   "lja.drawerid 领款人证件号, " + 
					   "lja.bankcode 领款银行编码, " + 
					   "(select bankname from ldbank where bankcode = lja.bankcode) 领款银行名称, " + 
					   "lja.paymode 领取方式, " +
					   "lja.bankaccno 领款账户号, " + 
					   "coalesce(sum(ljc.pay),0) " + 
					   "from ljaget lja,ljagetclaim ljc where  lja.actugetno = ljc.actugetno " +
					   "and ljc.otherno ='" + mCaseno + "' " +
					   "group by lja.drawer,ljc.otherno,lja.drawerid,lja.bankcode,lja.bankaccno,lja.paymode with ur";
		SSRS11 = tExeSQL.execSQL(str11);
		Element payData = new Element("PAYDATA");
		if(SSRS11.getMaxRow()>0) {					
			Element drawer = new Element("DRAWNAME");
			drawer.setText(SSRS11.GetText(1, 1));
			payData.addContent(drawer);
			Element draweIdType = new Element("DRAWIDTYPE");
			draweIdType.setText(SSRS11.GetText(1, 2));
			payData.addContent(draweIdType);
			Element drawerIdNo = new Element("DRAWIDNO");
			drawerIdNo.setText(SSRS11.GetText(1, 3));
			payData.addContent(drawerIdNo);
			Element bankcode = new Element("BANKCODE");
			bankcode.setText(SSRS11.GetText(1, 4));
			payData.addContent(bankcode);
			Element bankname = new Element("BANKNAME");
			bankname.setText(SSRS11.GetText(1, 5));
			payData.addContent(bankname);
			Element CaseGetMode = new Element("CASEGETMODE");
			CaseGetMode.setText(SSRS11.GetText(1, 6));
			payData.addContent(CaseGetMode);
			Element accno = new Element("ACCNO");
			accno.setText(SSRS11.GetText(1, 7));
			payData.addContent(accno);
			Element PAY = new Element("PAY");
			PAY.setText(SSRS11.GetText(1, 8));
			payData.addContent(PAY);
		}
		llcaseData.addContent(payData);
		tBodyData.addContent(llcaseData);
		tRootData.addContent(tBodyData);
		
		Document doc = new Document(tRootData);
		
		return doc;
	}
	/**
	 * 查询报文中的数据
	 */
	private boolean dealData() {
		
		System.out.println("LLOnlinGiveInfoQry----------------detaData");
		ExeSQL tExeSQL = new ExeSQL();
		//查询被保人信息
		String str1 = "select llc.customername 被保险人姓名,llc.customerno 被保险人客户号,llc.idtype 证件类型,llc.idno 证件号码,(select codename from ldcode where code = llc.casegetmode and codetype = 'paymode') 受益领取方式,llc.bankcode 银行编码,(select bankname from ldbank where bankcode = llc.bankcode),llc.rgtno from llcase llc where llc.caseno = '" + mCaseno + "' with ur";
		SSRS1 = tExeSQL.execSQL(str1);
		if(SSRS1.getMaxRow()<1) {
			buildError("dealData()", "案件未做处理");
			return false;
		}
		
		//查询申请人信息
		String str2 = "select llr.rgtantname 申请人姓名,(select codename from ldcode where codetype = 'llrelation' and code = llr.relation) 申请人与被保险人关系,llr.idno 申请人证件号码,llr.idtype 申请人证件类型,llr.rgtantmobile 申请人手机号 from llregister llr where llr.rgtno = '" + SSRS1.GetText(1, 8) + "' with ur";
		SSRS2 = tExeSQL.execSQL(str2);
		if(SSRS2.getMaxRow()<1) {
			buildError("dealData()", "未查询到立案信息");
			return false;
		}
		return true;
	}
	/**
	 * 校验请求报文节点值
	 * @return
	 */
	private boolean checkData() {
		
		System.out.println("LLOnlinGiveInfoQry----------------checkData");
		
		if(!"REQUEST".equals(mDocType)) {
			buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
			return false;
		}
		if(!"OC06".equals(mRequestType)) {
			buildError("checkData", "【请求类型】的值匹配有误或者不存在");
			return false;
		}
		if(mTransactionNum == null || "".equals(mTransactionNum) || "null".equals(mTransactionNum) || mTransactionNum.length() != 30) {
			buildError("checkData", "【交互编码】编码的个数有误");
			return false;
		}
		if(!"OC06".equals(mTransactionNum.substring(0,4))) {
			buildError("checkData", "【交互编码】请求类型有误");
			return false;
		}
		String mManagecom = mTransactionNum.substring(4, 12);
		if(mManagecom == null || "".equals(mManagecom) || "null".equals(mManagecom) || !"00000000".equals(mManagecom)) {
			buildError("checkData", "【交互编码】固定值有问题");
			return false;
		}
		
		return true;
	}
	/**
	 * 解析报文
	 * @param mInXmlDoc
	 * @return
	 */
	private boolean getInputData(Document mInXmlDoc) {
		
		System.out.println("LLOnlinGiveInfoQry----------------getInputData");
		
		if(mInXmlDoc == null) {
			buildError("getInputData()","未获取报文");
			return false;
		}
		//获取报文根节点
		Element tRootElement = mInXmlDoc.getRootElement();
		//获取根节点type属性
		mDocType = tRootElement.getAttributeValue("type");
		//获取HEAD节点
		Element tHeadElement = tRootElement.getChild("HEAD");
		//获取消息头节点
		mRequestType = tHeadElement.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadElement.getChildTextTrim("TRANSACTION_NUM");
		//获取BODY节点
		Element tBodyElement = tRootElement.getChild("BODY");
		Element tinCASE_DATA = tBodyElement.getChild("CASE_DATA");
		mCaseno = tinCASE_DATA.getChildTextTrim("CASENO");
		
		return true;
	}
	
	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLOnlinGiveInfoQry";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}
	
	public static void main(String[] args) {
		Document tInXmlDoc;
		try {
			tInXmlDoc = JdomUtil.build(new FileInputStream(
					"D:/Java/test/weixin/OC06/wx06.xml"), "GBK");
			
			LLOnlinGiveInfoQry tBusinessDeal = new LLOnlinGiveInfoQry();
			long tStartMillis0 = System.currentTimeMillis();
			Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
			// JdomUtil.print(tInXmlDoc.getRootElement());
			JdomUtil.print(tOutXmlDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
