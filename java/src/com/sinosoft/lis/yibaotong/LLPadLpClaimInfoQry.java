/**
 * 
 */
package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * @author it
 * 
 */
public class LLPadLpClaimInfoQry implements ServiceInterface {

	/**
	 * 
	 */
	public LLPadLpClaimInfoQry() {
		// TODO Auto-generated constructor stub
	}

	public CErrors mErrors = new CErrors();

	public VData mResult = new VData();
	
	// 传入报文
	private Document mInXmlDoc;

	// 报文类型
	private String mDocType = "";

	// 处理标志
	private boolean mDealState = true;

	// 返回类型代码
	private String mResponseCode = "1";

	// 请求类型
	private String mRequestType = "";

	// 错误描述
	private String mErrorMessage = "";

	// 交互编码
	private String mTransactionNum = "";
    private String mManageCom="";   //
	public SSRS mSSRS1 = new SSRS();
	public SSRS mSSRS2 = new SSRS();
	public SSRS mSSRS3 = new SSRS();

	// 查询的案件号
	private String mCASENO = "";

	// 报文BODY部分
	private Element tBODY=new Element("BODY");

	// 报文保单责任部分
	private Element tPOLICYLIST=new Element("POLICYLIST");
	
	// 报文给付明细部分
	private Element tPAYDETAILLIST=new Element("PAYDETAILLIST");

	// 报文赔付信息
	private Element tPAY_DATA=new Element("PAY_DATA");

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sinosoft.lis.yibaotong.ServiceInterface#service(org.jdom.Document)
	 */
	public Document service(Document pInXmlDoc) {
		System.out.println("LLPadLpClaimInfoQry--service");
		mInXmlDoc = pInXmlDoc;

		try {
			if (!getInputData(mInXmlDoc)) {
				mDealState = false;
			} else {
				if (!checkData()) {
					mDealState = false;
				}else{
					if (!dealData()) {
                    mDealState = false;
					}
					
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			mDealState = false;
			mResponseCode = "E";
			buildError("service()", "系统未知错误");
		} finally {
			return createXML();
		}
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param cInXml
	 *            Document
	 * @return boolean
	 */
	private boolean getInputData(Document cInXml) throws Exception {
		System.out.println("LLPadLpClaimInfoQry--getInputData");
		if (cInXml == null) {
			buildError("getInputData()", "未获得报文");
			return false;
		}

		Element tRootData = cInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");

		// 获取HEAD部分内容
		Element tHeadData = tRootData.getChild("HEAD");

		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

		System.out.println("LLPadLpClaimInfoQry--TransactionNum:"
				+ mTransactionNum);

		// 获取BODY部分内容
		Element tinBODY = tRootData.getChild("BODY");
		Element tinCASE_DATA = tinBODY.getChild("CASE_DATA");
		mCASENO = tinCASE_DATA.getChildTextTrim("CASENO");

		return true;
	}

	/**
	 * 校验基本数据
	 * 
	 * @return boolean
	 */
	 private boolean checkData(){
	        
	        if (!"REQUEST".equals(mDocType)) {
	            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
	            return false;
	        }

	        if (!"YD06".equals(mRequestType)) {
	            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
	            return false;
	        }

	        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
	            buildError("checkData()", "【交互编码】的编码个数错误!");
	            return false;
	        }

	        if (!"YD06".equals(mTransactionNum.substring(0, 4))) {
	            buildError("checkData()", "【交互编码】的请求类型编码错误!");
	            return false;
	        }

	        mManageCom = mTransactionNum.substring(4, 12);        
	        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"00000000".equals(mManageCom)) {
	            buildError("checkData()", "【交互编码】的固定值错误!");
	            return false;
	        }
	        
	        
	        
	        return true;

	    }
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	
    	String chksql1= " select "+
    	" cld.contno ,"+
    	" cld.RiskCode ,"+
    	" (select riskname from lmriskapp where riskcode=cld.riskcode fetch first row only),"+
    	" cld.getdutycode,"+
    	" (select getdutyname from lmdutygetclm where getdutycode=cld.getdutycode fetch first row only) ,"+
    	" cld.getdutykind ,"+
    	" (select codename from ldcode where codetype='getdutykind' and code=cld.getdutykind fetch first row only),"+
    	" sum(cld.ClaimMoney) ,"+
    	" sum(cld.OUTDUTYAMNT) ,"+
    	" cld.OutDutyRate,"+
    	" sum(cld.RealPay), "+
    	" cld.GIVETYPE "+
    	" from llcase c,llclaimdetail cld"+
    	" where "+
    	" cld.caseno = c.caseno"+
    	" and c.caseno = '"+mCASENO+"'"+
    	" group by cld.contno ,cld.RiskCode,cld.getdutycode,cld.getdutykind,cld.OutDutyRate,cld.GIVETYPE";
    	
    	mSSRS1=tExeSQL.execSQL(chksql1);
		if(mSSRS1!=null&&mSSRS1.getMaxRow()>0){
    		
		}else{
			buildError("dealData()", "未找到案件保单责任信息");
            mDealState = false; 
		}
    	
    	String chksql2= " select "+
		" lf.FeeType ,"+
		" lf.feedate ,"+
		" sum(lf.sumfee) ,"+
		" sum(lf.sumfee-lf.SELFAMNT) ,"+
		" sum(lf.SELFAMNT) ,"+
		" sum(lf.PreAmnt) ,"+
		" sum(lf.RefuseAmnt) "+
		" from llfeemain lf"+
		" where "+
		" lf.caseno='"+mCASENO+"'"+
		" group by lf.FeeType,lf.feedate";
		
    	
    	mSSRS2=tExeSQL.execSQL(chksql2);
		if(mSSRS2!=null&&mSSRS2.getMaxRow()>0){
    		
		}else{
			buildError("dealData()", "未找到案件给付信息");
            mDealState = false; 
		}
    	
    	
    	String chksql3= " select "+
    	" lja.drawer ,"+
		" lja.drawerid ,"+
		" lja.paymode ,"+
		" lja.bankaccno ,"+
		" lja.confdate ,"+
		" sum(lja.sumgetmoney) "+
		" from ljaget lja"+
		" where lja.othernotype ='5'"+
		" and lja.otherno='"+mCASENO +"'"+
		" group by lja.drawer,lja.drawerid,lja.paymode,lja.bankaccno,lja.confdate ";

    	mSSRS3=tExeSQL.execSQL(chksql3);
		if(mSSRS3!=null&&mSSRS3.getMaxRow()>0){
    		
		}else{
			buildError("dealData()", "未找到案件赔付信息");
            mDealState = false; 
		}
		
    	
    	return true;
    }

	/**
	 * 
	 * 
	 * @return boolean
	 */
	private boolean qryPolicyList() throws Exception {
		System.out.println("LLPadLpClaimInfoQry--qryPolicyList");

		Element mPOLICY_DATA = new Element("POLICY_DATA");

				
		for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
			Element tPOLICY_DATA = new Element("POLICY_DATA");

			Element tCONTNO = new Element("CONTNO");
			Element tRISKCODE = new Element("RISKCODE");
			Element tRISKNAME = new Element("RISKNAME");
			Element tGETDUTYCODE = new Element("GETDUTYCODE");
			Element tGETDUTYNAME = new Element("GETDUTYNAME");
			Element tGETDUTYKIND = new Element("GETDUTYKIND");
			Element tGETDUTYKINDNAME = new Element("GETDUTYKINDNAME");
			Element tCLAIMMONEY = new Element("CLAIMMONEY");
			Element tOUTDUTYAMNT = new Element("OUTDUTYAMNT");
			Element tGETRATE = new Element("GETRATE");
			Element tREALPAY = new Element("REALPAY");
			Element tGIVETYPE = new Element("GIVETYPE");

			tCONTNO.setText(mSSRS1.GetText(i, 1));
			tRISKCODE.setText(mSSRS1.GetText(i, 2));
			tRISKNAME.setText(mSSRS1.GetText(i, 3));
			tGETDUTYCODE.setText(mSSRS1.GetText(i, 4));
			tGETDUTYNAME.setText(mSSRS1.GetText(i, 5));
			tGETDUTYKIND.setText(mSSRS1.GetText(i, 6));
			tGETDUTYKINDNAME.setText(mSSRS1.GetText(i, 7));
			tCLAIMMONEY.setText(mSSRS1.GetText(i, 8));
			tOUTDUTYAMNT.setText(mSSRS1.GetText(i, 9));
			tGETRATE.setText(mSSRS1.GetText(i, 10));
			tREALPAY.setText(mSSRS1.GetText(i, 11));
			tGIVETYPE.setText(mSSRS1.GetText(i, 12));

			
			tPOLICY_DATA.addContent(tCONTNO);
			tPOLICY_DATA.addContent(tRISKCODE);
			tPOLICY_DATA.addContent(tRISKNAME);
			tPOLICY_DATA.addContent(tGETDUTYCODE);
			tPOLICY_DATA.addContent(tGETDUTYNAME);
			tPOLICY_DATA.addContent(tGETDUTYKIND);
			tPOLICY_DATA.addContent(tGETDUTYKINDNAME);
			tPOLICY_DATA.addContent(tCLAIMMONEY);
			tPOLICY_DATA.addContent(tOUTDUTYAMNT);
			tPOLICY_DATA.addContent(tGETRATE);
			tPOLICY_DATA.addContent(tREALPAY);
			tPOLICY_DATA.addContent(tGIVETYPE);

			tPOLICYLIST.addContent(tPOLICY_DATA);
		}

		return true;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	private boolean qryPayDetalList() throws Exception {
		System.out.println("LLPadLpClaimInfoQry--qryPayDetalList");
		Element mPAYDETAIL_DATA = new Element("PAYDETAIL_DATA");



		for (int i = 1; i <= mSSRS2.getMaxRow(); i++) {
			Element tPAYDETAIL_DATA = new Element("PAYDETAIL_DATA");

			Element tFEETYPE = new Element("FEETYPE");
			Element tFEEDATE = new Element("FEEDATE");
			Element tSUMFEE = new Element("SUMFEE");
			Element tINSOCIALAMNT = new Element("INSOCIALAMNT");
			Element tSELFAMNT = new Element("SELFAMNT");
			Element tPLANFEE = new Element("PLANFEE");
			Element tREFUSEAMNT = new Element("REFUSEAMNT");

			tFEETYPE.setText (mSSRS2.GetText(i, 1));
			tFEEDATE.setText (mSSRS2.GetText(i, 2));
			tSUMFEE.setText (mSSRS2.GetText(i, 3));
			tINSOCIALAMNT.setText (mSSRS2.GetText(i, 4));
			tSELFAMNT.setText (mSSRS2.GetText(i, 5));
			tPLANFEE.setText (mSSRS2.GetText(i, 6));
			tREFUSEAMNT.setText (mSSRS2.GetText(i, 7));

			tPAYDETAIL_DATA.addContent(tFEETYPE);
			tPAYDETAIL_DATA.addContent(tFEEDATE);
			tPAYDETAIL_DATA.addContent(tSUMFEE);
			tPAYDETAIL_DATA.addContent(tINSOCIALAMNT);
			tPAYDETAIL_DATA.addContent(tSELFAMNT);
			tPAYDETAIL_DATA.addContent(tPLANFEE);
			tPAYDETAIL_DATA.addContent(tREFUSEAMNT);

			tPAYDETAILLIST.addContent(tPAYDETAIL_DATA);
		}

		return true;
	}

	/**
	 * 
	 * 
	 * @return boolean
	 */
	private boolean qryPay_data() throws Exception {
		System.out.println("LLPadLpClaimInfoQry--qryPay_data");
		

		

		if (mSSRS3.getMaxRow() > 0) {
			for (int i = 1; i <= mSSRS3.getMaxRow(); i++) {
			Element tDRAWNAME = new Element("DRAWNAME");
			Element tDRAWIDNO = new Element("DRAWIDNO");
			Element tCASEGETMODE = new Element("CASEGETMODE");
			Element tACCNO = new Element("ACCNO");
			Element tCONFDATE = new Element("CONFDATE");
			Element tPAY = new Element("PAY");

			tDRAWNAME.setText (mSSRS3.GetText(1, 1));
			tDRAWIDNO.setText (mSSRS3.GetText(1, 2));
			tCASEGETMODE.setText (mSSRS3.GetText(1, 3));
			tACCNO.setText (mSSRS3.GetText(1, 4));
			tCONFDATE.setText (mSSRS3.GetText(1, 5));
			tPAY.setText (mSSRS3.GetText(1, 6));


			tPAY_DATA.addContent(tDRAWNAME);
			tPAY_DATA.addContent(tDRAWIDNO);
			tPAY_DATA.addContent(tCASEGETMODE);
			tPAY_DATA.addContent(tACCNO);
			tPAY_DATA.addContent(tCONFDATE);
			tPAY_DATA.addContent(tPAY);
			
			}
		}

		return true;
	}

	/**
	 * 生成返回的报文信息
	 * 
	 * @return Document
	 */
	private Document createXML() {
		if (!mDealState) {
			return createFalseXML();
		}else{
			try {
				return createResultXML(mCASENO);
			} catch (Exception ex) {
				buildError("createXML()", "生成返回报文错误");
				return createFalseXML();
			}
		}
	}

	/**
	 * 生成正常返回报文
	 * 
	 * @return Document
	 */
	private Document createResultXML(String aCaseNo) throws Exception {
		
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");

		// Head部分
		Element tHeadData = new Element("HEAD");

		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);

		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);

		tRootData.addContent(tHeadData);

		// Body部分
		// 报文保单责任部分
		qryPolicyList();
		// 报文给付明细部分
		qryPayDetalList();
		// 报文赔付信息
		qryPay_data();

		// 报文信息
		tBODY.addContent(tPOLICYLIST);
		tBODY.addContent(tPAYDETAILLIST);
		tBODY.addContent(tPAY_DATA);

		tRootData.addContent(tBODY);

		Document tDocument = new Document(tRootData);
		return tDocument;
	}

	/**
	 * 生成错误信息报文
	 * 
	 * @return Document
	 */
	private Document createFalseXML() {
		System.out.println("LLPadLpClaimInfoQry--createResultXML(返回错误报文)");
	    Element tRootData = new Element("PACKET");
	    tRootData.addAttribute("type", "RESPONSE");
	    tRootData.addAttribute("version", "1.0");

	    //Head部分
	    Element tHeadData = new Element("HEAD");

	    Element tRequestType = new Element("REQUEST_TYPE");
	    tRequestType.setText(mRequestType);
	    tHeadData.addContent(tRequestType);


	    Element tTransactionNum = new Element("TRANSACTION_NUM");
	    tTransactionNum.setText(mTransactionNum);
	    tHeadData.addContent(tTransactionNum);

	    tRootData.addContent(tHeadData);

	    //Body部分
	    Element tBodyData = new Element("BODY");
	    //返回的案件处理结构
	    System.out.println("开始返回错误报文生成!-----");
        Element response = new Element("RESPONSE_CODE");
        mErrorMessage = mErrors.getFirstError();
        response.setText("0");
        tBodyData.addContent(response);
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);
        tRootData.addContent(tBodyData);
        Document tDocument = new Document(tRootData);
        return tDocument;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLPadLpClaimInfoQry";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Document tInXmlDoc;
		try {
			tInXmlDoc = JdomUtil.build(new FileInputStream(
					"E:/PADTEST/YD06.xml"), "GBK");

			LLPadLpClaimInfoQry tBusinessDeal = new LLPadLpClaimInfoQry();
			long tStartMillis0 = System.currentTimeMillis();
			Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
			// JdomUtil.print(tInXmlDoc.getRootElement());
			JdomUtil.print(tOutXmlDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
