package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 自动立案</p>
 *
 * <p>Description: 生成案件至受理状态</p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zjd
 * @version 1.0
 */
public class LLPadUserLogin implements ServiceInterface
{
    
    public LLPadUserLogin(){}
    
    public CErrors mErrors = new CErrors();
//    private VData mInputData = new VData();
    public VData mResult = new VData();
//    private MMap mMMap = new MMap();
    private String mErrorMessage = "";  //错误描述    
    
    private String mManageCom="";   //管理机构
//    private String mOperator="";   //操作员
//    /** 社保保单标志 0：非社保；1：社保*/
//    private String mSocialSecurity = "0";
    
//    private List tList = new ArrayList();//返回信息合集
    //报文头信息
//    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private String mResponseCode = "1";     //返回类型代码    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    private String mDealType = "1";         //处理类型 1 实时 2非实时 
    
    //基本信息
    private String mUserCode = "";			//用户编码
    private String mPassWord = "";			//密码
    
    /** 菜单组、菜单组到菜单的相关信息*/
    LDUserSchema mLDUserSchema = new LDUserSchema();
    LDUserSet mLDUserSet = new LDUserSet();
    
//    private String mGRPCONTNO = "";           //团体保单号    
//    private String mPPPEOPLES = "";     //申请人数    
//    private String mTOGETHERFLAG = "";       //给付方式    
//    private String mCASEGETMODE = "";    //赔款领取方式
//    private String mAPPAMNT = "";         //申报金额
//    
//    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();//医保通案件处理表信息
//    
//    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
//    
//    private LLCaseSet mLLCaseSet = new LLCaseSet();
//    private LLCaseSet mbackCaseSet = new LLCaseSet();//案件回退集合
//    
      private GlobalInput tG=new GlobalInput();
//    
//    private String mReturnMessage = "";//错误提示集合
//    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
//    //批次号
//    private String mRgtNo = "";
//    private String mCurrentDate = PubFun.getCurrentDate();
//    private String mCurrentTime = PubFun.getCurrentTime();
 
    //处理结果标记
    private boolean mDealState=true;
    
    
    private Element mUserData;
//    private Element mBaseData;

    public Document service(Document pInXmlDoc)
    {
        
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{
                        //保存信息到LLHospCase
                        /*PubSubmit tPubSubmit = new PubSubmit();
                        VData tVData = new VData();
                        MMap tMMap= new MMap();
                        tMMap.put(mLLHospCaseSet, "INSERT");
                        tVData.add(tMMap);
                        tPubSubmit.submitData(tVData, "INSERT");*/
                    }
                   
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealDatacase(){
    	//TODO Auto-generated method stub
    	//1.受理到检录
//    	if(!dealCaseRegister()){
//    		return false;
//    	}else{
//    		//受理出错,跳出处理
//        	if(!mDealState){
//        		return false;
//        	}else{
//                //保存信息到LLHospCase
//                PubSubmit tPubSubmit = new PubSubmit();
//                VData mVData = new VData();
//                MMap tMMap= new MMap();
//                tMMap.put(mLLHospCaseSet, "INSERT");
//                mVData.add(tMMap);
//                if(!tPubSubmit.submitData(mVData, "")){
//                	 mDealState = false;
//                	 buildError("service()", "数据提交报错");
//                }
//        	}
//    	}
//        //2.检录到理算,包含核赔
//    	if(!batchClaimCal(mRgtNo)){
//    		return false;
//    	}else{
//    		//受理出错,跳出处理
//        	if(!mDealState){
//        		return false;
//        	}
//    	}
//    	//3.审批审定
//    	if(!simpleClaimAudit()){
//    		return false;
//    	}else{
//    		//受理出错,跳出处理
//        	if(!mDealState){
//        		return false;
//        	}
//    	}
//    	
         
        return true;
    }
    
    
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJAccidentAutoRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

 
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
    
        

        Element tBodyData = tRootData.getChild("BODY");
        mUserData = tBodyData.getChild("USER_DATA");          //登陆用户信息   
        return true;

    }
    
    
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
            return false;
        }

        if (!"YD01".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误!");
            return false;
        }

        if (!"YD01".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }

        if (!"YD01".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }
        
        return true;

    }
    
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
        
    	mUserCode= mUserData.getChildTextTrim("USERCODE");
    	if (mUserCode == null || "".equals(mUserCode)) {
            buildError("dealData()", "【用户编码】为空!");
            return false;
        }
    	mPassWord= mUserData.getChildTextTrim("PASSWORD");
    	if (mPassWord == null || "".equals(mPassWord)) {
            buildError("dealData()", "【密码】为空!");
            return false;
        }
    	LisIDEA tIdea = new LisIDEA();
    	String encryptPwd = tIdea.encryptString(mPassWord);
    
    	//用户编码验证
    	String userstate= "";
    	ExeSQL tExeSQL = new ExeSQL();
    	String checksql = "select userstate from lduser where usercode= '"+mUserCode+"' ";
    	SSRS tSSRS=tExeSQL.execSQL(checksql);
    	if(tSSRS!=null&&tSSRS.getMaxRow()>0){
	    		userstate  = tSSRS.GetText(1, 1);
	        
    	}else{
            buildError("dealData()", "用户编码不存在!");//sc5302测试用这个试一下
            mDealState = false; 
            
        }  
    	System.out.println("userstate==="+userstate);
    	if("1".equals(userstate)){
    		 buildError("dealData()", "该用户已经失效！");
             mDealState = false; 
    	}else if("L".equals(userstate)){
   		 	buildError("dealData()", "该用户已被锁定！");
            mDealState = false; 
		}else if("".equals(userstate)){
			buildError("dealData()", "该用户状态异常！");
            mDealState = false; 
		}
			
		String chksql1 ="select current_date-pmodifydate from lduser where usercode= '"+mUserCode+"'";
//		ExeSQL tExeSQL = new ExeSQL();
		SSRS chk1SSRS=tExeSQL.execSQL(chksql1);
		if(chk1SSRS!=null){
	    	if(chk1SSRS.getMaxRow()>0){
	    		if("".equals(chk1SSRS.GetText(1, 1).trim())){
	    			int overTime = Integer.parseInt(chk1SSRS.GetText(1, 1));
	    			System.out.println("已经"+overTime+"天没改密码了!");
				    if (overTime>300) {
				        buildError("dealData()", "该用户已超过3个月未修改密码,请重置密码");
			             mDealState = false; 
				    }
	    		}
	    		
	    	}
		}
  
//    	String sqlStr = "select * from lduser where usercode = '" + mUserCode + "' ";  //查询该用户所有信息
    	
    	 LDUserDB tLDUserDB = new LDUserDB();
    	 tLDUserDB.setUserCode(mUserCode);
    	 tLDUserDB.setPassword(encryptPwd);
         mLDUserSet = tLDUserDB.query();
         if(mLDUserSet.size()<=0){
             buildError("dealData()", "用户名或密码不正确!");
             mDealState = false; 
             return false;
         }
         mManageCom= mLDUserSet.get(1).getComCode();
         System.out.println("该用户的机构编码是:"+mManageCom);
	    //机构
//	    String tLimit = PubFun.getNoLimit(mManageCom);
//	    System.out.println("整理后的管理机构代码是 : "+tLimit);
 
        return true;
    }
    
    

    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
           try
        {
            return createResultXML();
        }
        catch (Exception e)
        {
            
            e.printStackTrace();
            buildError("createXML()", "生成返回报文错误!");
            return createFalseXML();
        }
        }
    }  
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        System.out.println("LLPADUserLogin--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        

//        Element tResponseCode = new Element("RESPONSE_CODE");
//        tResponseCode.setText(mResponseCode);
//        tHeadData.addContent(tResponseCode);



        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        
   
        Element tBody = new Element("BODY");
        Element tBackData = new Element("BACK_DATA");
        
        
        Element tLoginState = new Element("LOGIN_STATE");
        
        tLoginState.setText("1");
        tBackData.addContent(tLoginState);
        
        mErrorMessage = mErrors.getFirstError();
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBackData.addContent(tErrorMessage);
        
        
        tBody.addContent(tBackData);
        
        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() throws Exception {
         System.out.println("LLPADUserLogin--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

 
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);


         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BACK_DATA");
         tBodyData.addContent(tBackData);
         

         Element tLoginState = new Element("LOGIN_STATE");
                 
         tLoginState.setText("0");
         tBackData.addContent(tLoginState);
         Element tErrorMessage = new Element("ERROR_MESSAGE");
         tErrorMessage.setText("");
         tBackData.addContent(tErrorMessage);
         tRootData.addContent(tBodyData);
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLPADUserLogin";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    
    public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/PADTEST/YD01.xml"), "GBK");
            LLPadUserLogin tBusinessDeal = new LLPadUserLogin();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
