package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.ftp.FTPFile;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bl.LCGetBL;
import com.sinosoft.lis.bl.LCInsuredBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDDiseaseDB;
import com.sinosoft.lis.db.LDICDOPSDB;
import com.sinosoft.lis.db.LLAccidentTypeDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.db.LMDutyGetClmDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetClaimSchema;
import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.schema.LLAccidentSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLOperationSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.schema.LLToClaimDutySchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetClaimSet;
import com.sinosoft.lis.vschema.LLAccidentSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCasePolicySet;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLOperationSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.lis.vschema.LLToClaimDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;
 
/**
 * <p>Title: 上海个险自动理赔</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author ZH
 * @version 1.0
 */
public class LLHWAutoCaseRegister implements ServiceInterface {

	//错误容器
	public CErrors mErrors = new CErrors();
	public VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();

    
    public LLHWAutoCaseRegister(){
    	
    }
    
    //传入报文
    private Document mInXmlDoc;
    //报文类型
    private String mDocType;
    //处理标志
    private boolean mDealState = true;
    //返回类型代码
    private String mResponseCode = "1";
    //请求类型
    private String mRequestType = "";
    //错误描述
    private String mErrorMessage = "";
    //交互编码
    private String mTransactionNum = "";
    //扫描件路径
    private String mPicPath;
    //报文PICTURE_DATA部分
    private Element mPictureData;
    //报文USER_DATA部分
    private Element mUserData;
    //报文CUSTOMER_DATA部分
    private Element mCustomerData;
    //报文RECEIPTLIST部分
    private Element mReceiptList;
    //报文PAYLIST部分
    private Element mPayList;
    //报文DISEASELIST部分
    private Element mDiseaseList;
    //报文ACCIDENTLIST部分
    private Element mAccidentList;
    //报文OPERATIONLIST部分
    private Element mOperationList;
    //管理机构
    private String mManageCom="86310000";
    private String mAccDate = "";			//出险日期
    private String mCaseNo = "";			//案件号
    private String mUserCode = "";			//案件处理人
    private String mInsuredNo = "";			//客户号
    private String mCaseRelaNo="";			//事件关联号
    private String mClaimNo ="";			//平台理赔号
    private String mCaseGetMode = "";		//收益金领取方式
    private String mBankNo = "";			//银行编码
    private String mAccNo = "";				//账户号码
    private String mAccName ="" ;			//账户名称
    private String mRelaDrawerInsured ="";	//领款人与被保人关系
    private String mAccidentType ="";       //事件类型
    private String mAffixNo="";				//领款总金额
    private double mTableSumFee=0;			//账单金额
	
    private String mLimit = "";
	private String mMakeDate =PubFun.getCurrentDate();
	private String mMakeTime =PubFun.getCurrentTime();
    
	private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();			//被保险人信息
	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();					//案件信息
	private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();		//申请信息
	private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema();			//案件扩展表
	private LLHospCaseSchema mLLHospCaseSchema = new  LLHospCaseSchema();		//医保通案件信息
	private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet();					//账单信息
	private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet(); 		//分案收据明细
	private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();	//事件信息
	private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();				//疾病信息
	private LLAccidentSet mLLAccidentSet = new LLAccidentSet();				//意外信息
	private LLOperationSet mLLOperationSet = new LLOperationSet(); 			//手术信息 
	private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema(); //申请原因
	private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();		//事件关联表
    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();			 //理算险种表
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();        //自动理算表
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();	    //理算数据
    private LLClaimDetailSet mConfirmLLClaimDetailSet = new LLClaimDetailSet();     //理算确认数据
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();	    //险种理算数据
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();		      //案件理算表
    private LJSGetSchema mLJSGetSchema = new LJSGetSchema();  			  //应付总表
    private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();        //赔付应付表
    private ExeSQL mExeSQL = new ExeSQL();
    
    private FTPTool tFTPTool;
    
    public Document service(Document pInXmlDoc){
    	System.out.println("LLHWAutoCaseRegister---上海外包个险----service()");
    	
    	mInXmlDoc=pInXmlDoc;
    	
    	try{
	    	if(!getInputData(mInXmlDoc)){
	    		mDealState = false;
	    	}else{
	    		if(!checkData()){
	    			mDealState =false;
	    		}else{
	    			if(!dealData()){
	    				mDealState =false;
	    				cancelCase();
	    			}
	    		}
	    	}
    	}catch(Exception e){
    		 System.out.println(e.getMessage());
             mDealState = false;
             mResponseCode = "E";
             buildError("service()","系统未知错误");
             cancelCase();
    	}finally{
    		
    		return createXML();
    		
    	}

    }
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBody.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo,String aClaimNo) throws Exception {
         System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BASE_DATA");
         tBodyData.addContent(tBackData);
         
         //Base_Data部分
         Element tCaseno = new Element("CASENO");      
         tCaseno.setText(aCaseNo);
         tBackData.addContent(tCaseno);
         
         Element tClaimno = new Element("CLAIMNO");
         tClaimno.setText(aClaimNo);
         tBackData.addContent(tClaimno);
         
         Element tRgtDate = new Element("RGTDATE");
         tRgtDate.setText(this.mLLRegisterSchema.getRgtDate());
         tBackData.addContent(tRgtDate);
         
         tRootData.addContent(tBodyData);
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    
    /**
     * 生成返回报文处理
     * 
     */
    
    private Document createXML(){
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
        	try {
        		return createResultXML(mCaseNo,mClaimNo);
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }
    
    /**
     * 删除数据处理
     * 
     */
    private boolean cancelCase() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险---cancelCase()");
    	  //理算数据
        String tClaimSQL = "delete from LLClaim where CaseNo='" + mCaseNo + "'";
        String tClaimPolicySQL = "delete from LLClaimPolicy where CaseNo='" +
                                 mCaseNo + "'";
        String tClaimDetailSQL = "delete from LLClaimDetail where CaseNo='" +
                                 mCaseNo + "'";
        String tToClaimDutySQL = "delete from LLToClaimDuty where CaseNo ='" +
                                 mCaseNo + "'";
        String tCasePolicySQL = "delete from LLCasePolicy where CaseNo ='"
                                + mCaseNo + "'";

        String tFeeMainSQL = "delete from LLFeeMain where CaseNo='" + mCaseNo +
                             "'";
        String tCaseReceiptSQL = "delete from LLCaseReceipt where CaseNo = '" +
                                 mCaseNo + "'";
        String tCaseCureSQL = "delete from LLCaseCure where CaseNo = '" +
                              mCaseNo + "'";
        String tOperationSQL = "delete from LLOperation where CaseNo = '" +
                               mCaseNo + "'";
        String tAccidentSQL = "delete from LLAccident where CaseNo = '" +
                               mCaseNo + "'";    
        String tLJSGet = "delete from LJSGet where othernotype='5' and otherno = '" +
        						mCaseNo + "'";
        String tLJSGetClaim = "delete from LJSGetClaim where othernotype='5' and otherno = '" +
								mCaseNo + "'";
        String tLLHospCase ="delete from llhospcase where caseno='"+mCaseNo+"' and claimno='"+mClaimNo+"'";

//        String tSubReportSQL = "delete from LLSubReport a where exists (select 1 from llcaserela where a.subrptno=subrptno and CaseNo='" +
//                               mCaseNo + "')";
//        String tCaseRelaSQL = "delete from LLCaseRela where CaseNo='" + mCaseNo +
//                              "'";
        String tCaseSQL = "update  LLCase set rgtstate='14' ,CancleReason='9',CancleRemark='上海外包理赔自动撤件',CancleDate=current date where CaseNo='" + mCaseNo + "'";

//        String tRegisterSQL = "delete from LLRegister where RgtNo ='" + mCaseNo +
//                              "'";

//        String tHospCaseSQL = "delete from LLHospCase where CaseNo ='" +
//                              mCaseNo + "'";

        MMap tmpMap = new MMap();
        tmpMap.put(tClaimSQL, "DELETE");
        tmpMap.put(tClaimPolicySQL, "DELETE");
        tmpMap.put(tClaimDetailSQL, "DELETE");
        tmpMap.put(tToClaimDutySQL, "DELETE");
        tmpMap.put(tCasePolicySQL, "DELETE");
        tmpMap.put(tFeeMainSQL, "DELETE");
        tmpMap.put(tCaseReceiptSQL, "DELETE");
        tmpMap.put(tCaseCureSQL, "DELETE");
        tmpMap.put(tOperationSQL, "DELETE");
        tmpMap.put(tAccidentSQL, "DELETE");
        tmpMap.put(tLLHospCase,"DELETE");
        tmpMap.put(tLJSGet, "DELETE");
        tmpMap.put(tLJSGetClaim, "DELETE");
//        tmpMap.put(tSubReportSQL, "DELETE");
//        tmpMap.put(tCaseRelaSQL, "DELETE");
        tmpMap.put(tCaseSQL, "UPDATE");
//        tmpMap.put(tRegisterSQL, "DELETE");
//        tmpMap.put(tHospCaseSQL, "DELETE");

        try {
          
            this.mInputData.clear();
            this.mInputData.add(tmpMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            buildError("cancelCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("cancelCase", "撤销数据提交失败");
            return false;
        }
    	
    	return false;
    }
    
    /***
     * 报文信息处理
     * 
     */
    private boolean dealData() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险---dealData()");
    	
    	//校验基本信息
    	if(!dealInsured()){
    		return false;
    	}
    	//生成案件信息
    	if(!caseRegister()){
    		return false;
    	}
    	
    	//账单信息
    	if(!dealFee()){
    		return false;
    	}
    	//理算处理
    	if(!caseClaim()){
    		return false;
    	}
    	//数据进行存储处理
    	if(!dealCase()){
    		return false;
    	}
    	
    	//理算核赔规则走一圈，暂无参考，借鉴慎重
    	if(!uwCheck()){
    		return false;
    	}
    	
    	//对外接口表
    	if(!dealHospCase()){
    		return false;
    	}
    	
    	// 影像件资料进行存储
    	if(!dealHWScan()){
    		return false;
    	}
    	
    	
    	return  true;
    }
    
    /**
     * 影像件接口解析
     * 
     */
    private boolean dealHWScan() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--dealHWScan()--数据处理开始");
    	
    	mPicPath =mPictureData.getChildText("PICPATH");
    	
    	if(mPicPath==null || "".equals(mPicPath)) {
    		buildError("dealTYScan()", "影像件路径不能为空"); 
    		mDealState = false; 
    		return false;
    	}
  
    	ImageHandler tImageHandler = new ImageHandler(mPicPath, mCaseNo, "HW",mManageCom, mUserCode);
    	if(!tImageHandler.dealImage()) {
    		buildError("dealTYScan()", "影像件处理有问题"); 
    		mDealState = false; 
    		return false;
    	}
    	
    	return true;
    }
    
    /***
     * 对外接口数据
     * 
     */
    private boolean dealHospCase() throws Exception{
    	 System.out.println("LLHWAutoCaseRegister--上海外包个险--dealHospCase()--数据处理开始");
    	 mLLHospCaseSchema.setCaseNo(mCaseNo);
		 mLLHospCaseSchema.setHospitCode(mRequestType);
		 mLLHospCaseSchema.setHandler(mLLCaseSchema.getHandler());
		 mLLHospCaseSchema.setAppTranNo(mTransactionNum);
		 mLLHospCaseSchema.setAppDate(mMakeDate);
		 mLLHospCaseSchema.setAppTime(mMakeTime);
		 mLLHospCaseSchema.setCaseType("14");    //案件类型 
		 mLLHospCaseSchema.setConfirmState("1");
    	 mLLHospCaseSchema.setDealType("2");
    	 mLLHospCaseSchema.setBnfNo(mLLCaseSchema.getCustomerNo());
    	 mLLHospCaseSchema.setClaimNo(mClaimNo); 
    	 mLLHospCaseSchema.setRealpay(mAffixNo);
    	 mLLHospCaseSchema.setHCNo(mLLCaseSchema.getMngCom());
    	 mLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
    	 mLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
    	 
    	 MMap mMMap = new MMap();
    	 mMMap.put(mLLHospCaseSchema, "DELETE&INSERT");

         //提交数据库
           PubSubmit tPubSubmit = new PubSubmit();
           try{
  	         this.mInputData.clear();       
  	         this.mInputData.add(mMMap);
  	         if(!tPubSubmit.submitData(mInputData, "")){
  	        	 buildError("dealCase", "数据提交失败");
  	             return false;
  	         }
           } catch (Exception o){
  		   		 System.out.println(o.getMessage());
  		   		 System.out.println("案件环节生成数据信息出错...");
  		         mDealState = false;
  		         mResponseCode = "E";
  		         buildError("service()","系统未知错误"); 
      }
    	
    	return true;
    }
    
    
    /***
     * 理赔金额过核赔
     * 
     */
    private boolean uwCheck() throws Exception{
    	
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--uwCheck()--数据处理开始");
    	LLCaseCommon tLLCaseCommon  = new LLCaseCommon();
    	MMap mMMap = new MMap();
    	SSRS tCusSSRS = new SSRS();
    	ExeSQL oExeSQL = new ExeSQL();
    	VData tVData = new VData();
    	String tErr= null;
    	if(mLLRegisterSchema.getRgtNo()!=null && !"".equals(mLLRegisterSchema.getRgtNo())){
    		String tCusSql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'with ur";
    		tCusSSRS = oExeSQL.execSQL(tCusSql);
    		System.out.println(tCusSSRS.getMaxRow());
    		
    	}
    	for(int a=1;a<=tCusSSRS.getMaxRow();a++){
    		LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
    		LLCaseSet tLLCaseSet = new LLCaseSet();
    		LLClaimDetailSet tLLClaimDetaillSet = new LLClaimDetailSet();
    		LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
    		LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    		
    		LLCaseDB tLLCaseDB = new LLCaseDB();
    		tLLCaseDB.setCustomerNo(tCusSSRS.GetText(a, 1));
    		tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
    		tLLCaseDB.setRgtState("01");
    		tLLCaseSet.add(tLLCaseDB.query());
    		LLCaseSchema tLLCaseSchema=tLLCaseSet.get(1);
    		
    		if(tLLCaseSet.size()>0 && tLLCaseSet!=null){
    			for(int b=1;b<=tLLCaseSet.size();b++){
    				LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB(); 
    				tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(b).getCaseNo());
    				tLLClaimDetaillSet.set(tLLClaimDetailDB.query());
    				LLCaseRelaDB  tLLCaseRelaDB = new LLCaseRelaDB();
    				tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(b).getCaseNo());
    				tLLCaseRelaSet.set(tLLCaseRelaDB.query());
    			}
    			
    			for(int c=1;c<=tLLCaseRelaSet.size();c++){
    				LLSubReportDB  tLLSubReportDB = new LLSubReportDB();
    				tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(c).getSubRptNo());
    				tLLSubReportSet.set(tLLSubReportDB.query());
    			}
    		}
    		
    		System.out.println("上海个险外包 开始进行核赔规则.....");
    		
    		tVData.add(tLLCaseSchema);
    		tVData.add(tLLSubReportSet);
    		tVData.add(tLLCaseRelaSet);
    		tVData.add(tLLClaimDetaillSet);
    		tVData.add(mGlobalInput);
    		
    		if(!tLLUWCheckBL.submitData(tVData, "")){
    			if(tErr==null){
            		  tErr="客户号为"+tCusSSRS.GetText(a, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+tLLCaseSchema.getCaseNo()+");";  
            	  }
    		    if(tErr!=null){
               	 CError.buildErr(this,tErr);
                	 return false;
    		    }
             }else{
            	 //变更案件信息
            	 tLLCaseSchema.setRgtState("04"); //理算状态
            	 tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
            	 tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
            	 mMMap.put(tLLCaseSchema, "UPDATE");
            	 //轨迹记录表
  	             LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
  	             mLLCaseOpTimeSchema.setCaseNo(mLLCaseSchema.getCaseNo());
  	             mLLCaseOpTimeSchema.setRgtState("04");
  	             mLLCaseOpTimeSchema.setOperator(mUserCode);
  	             mLLCaseOpTimeSchema.setManageCom(mLLCaseSchema.getMngCom()); 
  	            try {
  	               LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
  	                       mLLCaseOpTimeSchema);
  	               mMMap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
  	           } catch (Exception ex) {

  	           }
  	            
             }
	
    	}
    	//提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        try{
	         this.mInputData.clear();       
	         this.mInputData.add(mMMap);
	         if(!tPubSubmit.submitData(mInputData, "")){
	        	 buildError("dealCase", "数据提交失败");
	             return false;
	         }
        } catch (Exception o){
		   		 System.out.println(o.getMessage());
		   		 System.out.println("核赔环节生成数据信息出错...");
		         mDealState = false;
		         mResponseCode = "E";
		         buildError("service()","系统未知错误"); 
      }

    	return true;
    }
    
    
    /***
     * 数据存储处理
     * 
     */
    private boolean dealCase() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--dealCase()--数据处理开始");
    	MMap mMMap = new MMap();
    	//提交数据信息
        mMMap.put(mLLCaseSchema,"DELETE&INSERT");
        mMMap.put(mLLRegisterSchema, "DELETE&INSERT");
        if(mLLCaseExtSchema.getCaseNo()!=null && !"".equals(mLLCaseExtSchema.getCaseNo())){
        	mMMap.put(mLLCaseExtSchema, "DELETE&INSERT");
        }
        mMMap.put(mLLSubReportSchema, "DELETE&INSERT"); 
        mMMap.put(mLLCaseRelaSchema, "DELETE&INSERT");
        mMMap.put(mLLCaseCureSet, "DELETE&INSERT");
        if(mLLAccidentSet!=null && mLLAccidentSet.size()>0 ){
        	mMMap.put(mLLAccidentSet, "DELETE&INSERT");
        }
        if(mLLOperationSet!=null && mLLOperationSet.size()>0){
        	 mMMap.put(mLLOperationSet, "DELETE&INSERT");
        }
       if(mLLAppClaimReasonSchema.getCaseNo()!=null && !"".equals(mLLAppClaimReasonSchema.getCaseNo())){
    	   mMMap.put(mLLAppClaimReasonSchema, "DELETE&INSERT");
       }
       
       mMMap.put(mLLCaseReceiptSet, "DELETE&INSERT");
       mMMap.put(mLLFeeMainSet, "DELETE&INSERT");
       
       //理算数据
       mMMap.put(mLLToClaimDutySet, "DELETE&INSERT");
       mMMap.put(mLLClaimDetailSet, "DELETE&INSERT");
       mMMap.put(mLJSGetClaimSet, "DELETE&INSERT");
       mMMap.put(mLLClaimPolicySet, "DELETE&INSERT");
       mMMap.put(mLLCasePolicySet, "DELETE&INSERT");
       mMMap.put(mLLClaimSchema, "DELETE&INSERT");
       mMMap.put(mLJSGetSchema, "DELETE&INSERT");

       //提交数据库
         PubSubmit tPubSubmit = new PubSubmit();
         
         try{
	         this.mInputData.clear();       
	         this.mInputData.add(mMMap);
	         if(!tPubSubmit.submitData(mInputData, "")){
	        	 buildError("dealCase", "数据提交失败");
	             return false;
	         }
         } catch (Exception o){
		   		 System.out.println(o.getMessage());
		   		 System.out.println("案件环节生成数据信息出错...");
		         mDealState = false;
		         mResponseCode = "E";
		         buildError("service()","系统未知错误"); 
    }
    	
    	return true;
    }
    
    /**
     * 解析理算信息
     * 
     */
    private boolean caseClaim() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--caseClaim()--计算开始");
    	 double mSum =0; 
    	 String totalGiveType="";
    	 String strLimit = PubFun.getNoLimit(mManageCom);
	     String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit); 	     
	     System.out.println("tGetNoticeNo=="+tGetNoticeNo);
	     
	     String limit = PubFun.getNoLimit(this.mManageCom);
	     String mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
	     System.out.println("mClmNo======"+mClmNo);
	     LLCaseCommon tLLCaseCommon = new LLCaseCommon();
	     List tPayList = new ArrayList();
	     tPayList = mPayList.getChildren();
	     
	     out:
	     for(int i=0;i<tPayList.size();i++){
	    	 Element tPayData = (Element) tPayList.get(i);
	    	 String tContNo = tPayData.getChildText("CONTNO");
	    	 String tRiskCode = tPayData.getChildText("RISKCODE");
	    	 String tGetDutyCode = tPayData.getChildText("GETDUTYCODE");
	    	 String tGetDutyKind = tPayData.getChildText("GETDUTYKIND");
	    	 double mRealPay = Double.parseDouble(tPayData.getChildText("RELAPAY"));
	    	 double tDeclineAmnt = Double.parseDouble(tPayData.getChildText("DECLINEAMNT"));
	    	 String tGiveType = tPayData.getChildText("GIVETYPE");
	    	 String tGiveReason = tPayData.getChildText("GIVEREASON");
	    	 boolean mRiskFlag =true;
	    	 if(mRealPay==0 && tGiveType.equals("1")){
	    		 CError.buildErr(this, "实赔金额为0，不能选择正常给付");
	             return false;     
	    	 }
	    	 //校验保单信息。第一次逆推理，多担待，有问题及早发现。
	    	 System.out.println("保单信息验证----开始---慎重---");
	    	 
	    	 String tPolSQL ="select polno from lcpol where riskcode='"+tRiskCode+"' and contno='"+tContNo+"' "
	    	 		+ "and insuredno='"+mInsuredNo+"' union select polno from lbpol where riskcode='"+tRiskCode+"' "
	    	 		+ "and contno='"+tContNo+"' and insuredno='"+mInsuredNo+"' with ur";
	    	 String aPolNo = mExeSQL.getOneValue(tPolSQL);
	    	 LCPolSchema tLCPolSchema  = new  LCPolSchema();
	    	 if("".equals(aPolNo) || aPolNo==null){
	    		 buildError("caseClaim", "保单险种信息查询失败");
	    	 }else{
		    	 LCPolBL tLCPolBL = new LCPolBL();
		    	 tLCPolBL.setRiskCode(tRiskCode);
		    	 tLCPolBL.setContNo(tContNo);
		    	 tLCPolBL.setInsuredNo(mInsuredNo);
		    	 tLCPolBL.setPolNo(aPolNo);
		    	 if(!tLCPolBL.getInfo()){
		    		 buildError("caseClaim", "保单险种信息查询失败");
	                 return false;
		    	 }
		    	 tLCPolSchema =tLCPolBL.getSchema();
	    	 }
	    	 
	    	 String tGetsql="select dutycode from lcget where polno='"+tLCPolSchema.getPolNo()+"' and "
	    	 		+ "getdutycode='"+tGetDutyCode+"' union select dutycode from lbget where polno='"+tLCPolSchema.getPolNo()+"' and "
	    	 		+ "getdutycode='"+tGetDutyCode+"' with ur";
	    	 String tDutycode =mExeSQL.getOneValue(tGetsql);
	    	 LCGetSchema tLCGetSchema  = new LCGetSchema();
	    	 if("".equals(tDutycode) || tDutycode==null){
	    		 buildError("caseClaim", "保单给付责任信息查询失败");
                 return false;
	    	 }else{
		    	 LCGetBL tLCGetBL =new LCGetBL();
		    	 tLCGetBL.setPolNo(tLCPolSchema.getPolNo());
		    	 tLCGetBL.setGetDutyCode(tGetDutyCode);
		    	 tLCGetBL.setDutyCode(tDutycode);
		    	 if(!tLCGetBL.getInfo()){
		    		 buildError("caseClaim", "保单给付责任信息查询失败");
	                 return false;
		    	 }
		    	 
		    	 tLCGetSchema = tLCGetBL.getSchema();
	    	 }
	            //出险日期、入院日期、出院日期必须都在保单年度里
	         if (!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(), mAccDate)) {
	            	buildError("caseClaim", "出险日期不在保单有效期内");
	                return false;
            }
	         
	       //得到赔付结论和赔付结论依据(name)
             String giveTypeSql = "select CodeName from ldcode where 1 = 1 and codetype = 'llclaimdecision'  and code='"+tGiveType+"' order by Code";
             String giveReasonSql ="select a.CodeName from ldcode a where  trim(a.codetype)=(select trim(b.codeaLias) from ldcode b where b.codetype='llclaimdecision' and b.code='"+tGiveType+"')  and a.code='"+tGiveReason+"' order by a.code";
             ExeSQL giveTypeSQL = new ExeSQL();
             String mGiveTypeDesc = giveTypeSQL.getOneValue(giveTypeSql);
             String mGiveReasonDesc =giveTypeSQL.getOneValue(giveReasonSql);
             
             System.out.println("上海个险 先进行赔付明细的描述-----");
             
             //待理算责任
             LLToClaimDutySchema tLLToClaimDutySchema = new LLToClaimDutySchema();
             tLLToClaimDutySchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
             
             LMDutyGetClmDB tLMDutyGetClmDB = new LMDutyGetClmDB();
             tLMDutyGetClmDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
//             if(tLMDutyGetClmDB.query().size()!=1){
//            	 buildError("caseClaim", "查询出多条给付责任类型");
//                 return false;
//             }
             tLLToClaimDutySchema.setPolNo(tLCPolSchema.getPolNo());
             tLLToClaimDutySchema.setCaseNo(mCaseNo);
             tLLToClaimDutySchema.setGetDutyKind(tGetDutyKind);
             tLLToClaimDutySchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
             tLLToClaimDutySchema.setSubRptNo(mLLCaseRelaSchema.getSubRptNo());
             tLLToClaimDutySchema.setClaimCount("0");
             tLLToClaimDutySchema.setEstClaimMoney("0");
             tLLToClaimDutySchema.setDutyCode(tLCGetSchema.getDutyCode());	
             tLLToClaimDutySchema.setPolMngCom(tLCPolSchema.getManageCom());
             tLLToClaimDutySchema.setRiskVer(tLCPolSchema.getRiskVersion());
             tLLToClaimDutySchema.setRiskCode(tRiskCode);
             tLLToClaimDutySchema.setContNo(tContNo);
             tLLToClaimDutySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
             tLLToClaimDutySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
             tLLToClaimDutySchema.setKindCode(tLCPolSchema.getKindCode());

             mLLToClaimDutySet.add(tLLToClaimDutySchema);
             
           //分案保单明细
             LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
             Reflections LLClaimDetailtref = new Reflections();
             LLClaimDetailtref.transFields(tLLCasePolicySchema, tLLToClaimDutySchema);
             tLLCasePolicySchema.setCaseNo(mLLCaseSchema.getCaseNo());
             tLLCasePolicySchema.setRgtNo(mLLCaseSchema.getRgtNo());
             tLLCasePolicySchema.setCasePolType("0");  //保单类型 0-被保人案件
             tLLCasePolicySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
             tLLCasePolicySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
             tLLCasePolicySchema.setContNo(tLCPolSchema.getContNo());
             tLLCasePolicySchema.setPolNo(tLCPolSchema.getPolNo());
             tLLCasePolicySchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
             tLLCasePolicySchema.setKindCode(tLCPolSchema.getKindCode());  //险类代码
             tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion()); //险种版本号
             tLLCasePolicySchema.setRiskCode(tLCPolSchema.getRiskCode());
             tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
             tLLCasePolicySchema.setSaleChnl(tLCPolSchema.getSaleChnl()); //销售渠道
             tLLCasePolicySchema.setAgentCode(tLCPolSchema.getAgentCode());//代理人代码
             tLLCasePolicySchema.setAgentGroup(tLCPolSchema.getAgentGroup());//代理人组别
             tLLCasePolicySchema.setInsuredNo(tLCPolSchema.getInsuredNo());
             tLLCasePolicySchema.setInsuredName(tLCPolSchema.getInsuredName());
             tLLCasePolicySchema.setInsuredSex(tLCPolSchema.getInsuredSex());
             tLLCasePolicySchema.setInsuredBirthday(tLCPolSchema.getInsuredBirthday());
             tLLCasePolicySchema.setAppntNo(tLCPolSchema.getAppntNo());
             tLLCasePolicySchema.setAppntName(tLCPolSchema.getAppntName());
             tLLCasePolicySchema.setCValiDate(tLCPolSchema.getCValiDate());//生效日期
             tLLCasePolicySchema.setPolType("1");  //保单性质状态 1-正式 保单
             tLLCasePolicySchema.setMngCom(mManageCom);
             tLLCasePolicySchema.setOperator(mUserCode);
             tLLCasePolicySchema.setMakeDate(mMakeDate);
             tLLCasePolicySchema.setMakeTime(mMakeTime);
             tLLCasePolicySchema.setModifyDate(mMakeDate);
             tLLCasePolicySchema.setModifyTime(mMakeTime);

             if(mLLCasePolicySet.size()>0 && mLLCasePolicySet!=null){
            	 for(int c=1;c<=mLLCasePolicySet.size();c++){
            		 String cPolNo = mLLCasePolicySet.get(c).getPolNo();
            		 String cContNo = mLLCasePolicySet.get(c).getContNo();
            		 String cRiskCode = mLLCasePolicySet.get(c).getRiskCode();
            		 if(cRiskCode.equals(tLLCasePolicySchema.getRiskCode())&&
            				 cContNo.equals(tLLCasePolicySchema.getContNo())&&
            				 cPolNo.equals(tLLCasePolicySchema.getPolNo())){
            			 	 mRiskFlag= false; 
            		 }
            	 }
            	  if(mRiskFlag){
                 	 mLLCasePolicySet.add(tLLCasePolicySchema);
                  }
             }else{
            	   mLLCasePolicySet.add(tLLCasePolicySchema);
             }
             
             //赔付明细
             LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema(); 
             Reflections tReflections = new Reflections();
             tReflections.transFields(tLLClaimDetailSchema, tLLToClaimDutySchema); 
             tLLClaimDetailSchema.setCaseNo(mLLCaseSchema.getCaseNo());
             tLLClaimDetailSchema.setRgtNo(mLLCaseSchema.getRgtNo());
             tLLClaimDetailSchema.setClmNo(mClmNo);
             tLLClaimDetailSchema.setPolNo(tLCPolSchema.getPolNo());
             tLLClaimDetailSchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
             tLLClaimDetailSchema.setGetDutyCode(tGetDutyCode);
             tLLClaimDetailSchema.setGetDutyKind(tGetDutyKind);
             tLLClaimDetailSchema.setStatType(tLMDutyGetClmDB.query().get(1).getStatType()); //统计类别
             tLLClaimDetailSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
             tLLClaimDetailSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
             tLLClaimDetailSchema.setContNo(tLCPolSchema.getContNo());
             tLLClaimDetailSchema.setKindCode(tLCPolSchema.getKindCode());  //险类代码
             tLLClaimDetailSchema.setRiskVer(tLCPolSchema.getRiskVersion()); //险种版本号
             tLLClaimDetailSchema.setRiskCode(tLCPolSchema.getRiskCode());
             tLLClaimDetailSchema.setPolMngCom(tLCPolSchema.getManageCom());
             tLLClaimDetailSchema.setSaleChnl(tLCPolSchema.getSaleChnl()); //销售渠道
             tLLClaimDetailSchema.setAgentCode(tLCPolSchema.getAgentCode());//代理人代码
             tLLClaimDetailSchema.setAgentGroup(tLCPolSchema.getAgentGroup());//代理人组别
             tLLClaimDetailSchema.setTabFeeMoney(mTableSumFee);
             tLLClaimDetailSchema.setClaimMoney(mRealPay);
             tLLClaimDetailSchema.setDeclineAmnt(tDeclineAmnt);
             tLLClaimDetailSchema.setOverAmnt("0");  //溢额
             tLLClaimDetailSchema.setRealPay(mRealPay);
             tLLClaimDetailSchema.setStandPay(mRealPay);
             tLLClaimDetailSchema.setPreGiveAmnt("0");  //先期给费
             tLLClaimDetailSchema.setSelfGiveAmnt("0");  //自费
             tLLClaimDetailSchema.setRefuseAmnt("0");	 //不合理费用
             tLLClaimDetailSchema.setOtherAmnt("0");	 //其他已给付
             tLLClaimDetailSchema.setOutDutyAmnt("0");	 //免赔额
             tLLClaimDetailSchema.setOutDutyRate("1");		//免赔比例
             tLLClaimDetailSchema.setApproveAmnt("0");		//通融给付
             tLLClaimDetailSchema.setAgreeAmnt("0");	  //协议给付
             tLLClaimDetailSchema.setDutyCode(tLCGetSchema.getDutyCode());
             tLLClaimDetailSchema.setGiveType(tGiveType);
             tLLClaimDetailSchema.setGiveTypeDesc(mGiveTypeDesc);
             tLLClaimDetailSchema.setGiveReason(tGiveReason);
             tLLClaimDetailSchema.setGiveReasonDesc(mGiveReasonDesc);
             tLLClaimDetailSchema.setMngCom(mLLCaseSchema.getMngCom());
             tLLClaimDetailSchema.setOperator(mUserCode);
             tLLClaimDetailSchema.setMakeDate(mMakeDate);
             tLLClaimDetailSchema.setMakeTime(mMakeTime);
             tLLClaimDetailSchema.setModifyDate(mMakeDate);
             tLLClaimDetailSchema.setModifyTime(mMakeTime);
        	 
        	 totalGiveType = sumGiveType(totalGiveType,tGiveType);
             
             //总金额
             mSum += mRealPay;
             
             mLLClaimDetailSet.add(tLLClaimDetailSchema);             
             
             //踢重保单赔案明细
             if(mLLClaimPolicySet!=null && mLLClaimPolicySet.size()>0){
            	 for(int b=1;b<=mLLClaimPolicySet.size();b++){
            		 LLClaimPolicySchema bLLClaimPolicySchema= mLLClaimPolicySet.get(b);
            		 LJSGetClaimSchema  bLJSGetClaimSchema=mLJSGetClaimSet.get(b);
            		 String bOldContNo = bLLClaimPolicySchema.getContNo();
            		 String bOldRiskCode = bLLClaimPolicySchema.getRiskCode();
            		 String bOldGetDutyKind =bLLClaimPolicySchema.getGetDutyKind();
            		 String bOldPolNo = bLLClaimPolicySchema.getPolNo();
            		 if(bOldContNo.equals(tLLClaimDetailSchema.getContNo())
            				 && bOldRiskCode.equals(tLLClaimDetailSchema.getRiskCode())
            				 && bOldGetDutyKind.equals(tLLClaimDetailSchema.getGetDutyKind())
            				 && bOldPolNo.equals(tLLClaimDetailSchema.getPolNo())){
            			
            			     double aRealPay= bLLClaimPolicySchema.getRealPay();
            			     double aPay = bLJSGetClaimSchema.getPay();
            			     aRealPay += mRealPay;
            			     aPay += mRealPay;
            			 	 bLLClaimPolicySchema.setRealPay(aRealPay);
            			 	 bLLClaimPolicySchema.setStandPay(aRealPay);
            			 	 bLJSGetClaimSchema.setPay(aPay);
            			 	 continue out;
            		 } 
            	 }
             }
             //分案保单明细
             LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
             tReflections.transFields(tLLClaimPolicySchema, tLLToClaimDutySchema);
             tLLClaimPolicySchema.setClmNo(mClmNo);
             tLLClaimPolicySchema.setCaseNo(mLLCaseSchema.getCaseNo());
             tLLClaimPolicySchema.setRgtNo(mLLCaseSchema.getRgtNo());
             tLLClaimPolicySchema.setCaseRelaNo(mLLCaseRelaSchema.getCaseRelaNo());
             tLLClaimPolicySchema.setGrpContNo(tLCPolSchema.getGrpContNo());
             tLLClaimPolicySchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
             tLLClaimPolicySchema.setContNo(tLCPolSchema.getContNo());
             tLLClaimPolicySchema.setPolNo(tLCPolSchema.getPolNo());
             tLLClaimPolicySchema.setGetDutyKind(tLLClaimDetailSchema.getGetDutyKind());
             tLLClaimPolicySchema.setRiskCode(tLCPolSchema.getRiskCode());
             tLLClaimPolicySchema.setKindCode(tLCPolSchema.getKindCode());  //险类代码
             tLLClaimPolicySchema.setRiskVer(tLCPolSchema.getRiskVersion()); //险种版本号
             tLLClaimPolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
             tLLClaimPolicySchema.setSaleChnl(tLCPolSchema.getSaleChnl()); //销售渠道
             tLLClaimPolicySchema.setAgentCode(tLCPolSchema.getAgentCode());//代理人代码
             tLLClaimPolicySchema.setAgentGroup(tLCPolSchema.getAgentGroup());//代理人组别
             tLLClaimPolicySchema.setInsuredNo(tLCPolSchema.getInsuredNo());
             tLLClaimPolicySchema.setInsuredName(tLCPolSchema.getInsuredName());
             tLLClaimPolicySchema.setAppntNo(tLCPolSchema.getAppntNo());
             tLLClaimPolicySchema.setAppntName(tLCPolSchema.getAppntName());
             tLLClaimPolicySchema.setCValiDate(tLCPolSchema.getCValiDate());//生效日期
             tLLClaimPolicySchema.setClmState("1");   //理赔状态 1-已理算
             tLLClaimPolicySchema.setStandPay(mRealPay);
             tLLClaimPolicySchema.setRealPay(mRealPay);
             tLLClaimPolicySchema.setPreGiveAmnt("0");
             tLLClaimPolicySchema.setSelfGiveAmnt("0");
             tLLClaimPolicySchema.setRefuseAmnt("0");
             tLLClaimPolicySchema.setApproveAmnt("0");
             tLLClaimPolicySchema.setAgreeAmnt("0");
             tLLClaimPolicySchema.setGiveType(tGiveType);
             tLLClaimPolicySchema.setGiveTypeDesc(mGiveTypeDesc);
             tLLClaimPolicySchema.setGiveReason(tGiveReason);
             tLLClaimPolicySchema.setGiveReasonDesc(mGiveReasonDesc);
             tLLClaimPolicySchema.setClmUWer(mUserCode);
             tLLClaimPolicySchema.setMngCom(mLLCaseSchema.getMngCom());
             tLLClaimPolicySchema.setOperator(mUserCode);
             tLLClaimPolicySchema.setMakeDate(mMakeDate);
             tLLClaimPolicySchema.setMakeTime(mMakeTime);
             tLLClaimPolicySchema.setModifyDate(mMakeDate);
             tLLClaimPolicySchema.setModifyTime(mMakeTime);
             
             mLLClaimPolicySet.add(tLLClaimPolicySchema);
             
             
           //赔付应付表
             LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
             Reflections aLJSGet = new Reflections();
             aLJSGet.transFields(tLJSGetClaimSchema, tLLClaimDetailSchema); 
             tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
             tLJSGetClaimSchema.setFeeOperationType(tLLClaimDetailSchema.getGetDutyKind());
             tLJSGetClaimSchema.setFeeFinaType(tLLClaimDetailSchema.getStatType());
             tLJSGetClaimSchema.setOtherNo(mLLCaseSchema.getCaseNo());
             tLJSGetClaimSchema.setOtherNoType("5");
             tLJSGetClaimSchema.setGetDutyKind(tLLClaimDetailSchema.getGetDutyKind());
             tLJSGetClaimSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
             tLJSGetClaimSchema.setPay(mRealPay);
             tLJSGetClaimSchema.setManageCom(tLCPolSchema.getManageCom());
             tLJSGetClaimSchema.setAgentCom(tLCPolSchema.getAgentCom());
             
             mLJSGetClaimSet.add(tLJSGetClaimSchema);
             

	     }
         if(tPayList.size()==mLLClaimDetailSet.size()){
        	 //理赔金额比较
        	 if(Arith.round(Double.parseDouble(mAffixNo),2)- Arith.round(mSum,2) != 0)
             {
            	  buildError("caseClaim", "每个给付责任的理赔金额不等于赔付金额");
            	  return false;
             } 
        	 
        	 //赔案信息
        	 //获取赔付结论。。愁死。。。 	 
        	 mLLClaimSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        	 mLLClaimSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        	 mLLClaimSchema.setClmNo(mClmNo);
        	 mLLClaimSchema.setGetDutyKind("000000");
        	 mLLClaimSchema.setClmState("2");
        	 mLLClaimSchema.setStandPay(mSum);
        	 mLLClaimSchema.setRealPay(mSum);
        	 mLLClaimSchema.setGiveType(totalGiveType);
        	 mLLClaimSchema.setGiveTypeDesc(getGiveDesc(totalGiveType));
             mLLClaimSchema.setClmUWer(mUserCode);
             mLLClaimSchema.setCheckType("0");
             mLLClaimSchema.setMngCom(mLLCaseSchema.getMngCom());
             mLLClaimSchema.setOperator(mUserCode);
             mLLClaimSchema.setMakeDate(mMakeDate);
             mLLClaimSchema.setMakeTime(mMakeTime);
             mLLClaimSchema.setModifyDate(mMakeDate);
             mLLClaimSchema.setModifyTime(mMakeTime);
             
             //应付总表
             Reflections LJSGettref = new Reflections();
	     	 LJSGettref.transFields(mLJSGetSchema, mLJSGetClaimSet.get(1)); 
	         mLJSGetSchema.setSumGetMoney(mSum);  

         }else{
        	 buildError("caseClaim", "理赔给付责任总数不匹配。");
       	  	 return false;
         }

    	return true;
    }
    /***
     * 赔付结论
     * 
     */
    private String sumGiveType(String fGiveType, String sGiveType) {
        if (fGiveType.equals("")) {
            return sGiveType;
        }
        if (sGiveType.equals("")) {
            return fGiveType;
        }
        if (fGiveType.equals("4") || fGiveType.equals("5")) {
            return fGiveType;
        }
        if (sGiveType.equals("4") || sGiveType.equals("5")) {
            return sGiveType;
        }
        boolean fRefuse = fGiveType.equals("3");
        boolean sRefuse = sGiveType.equals("3");
        if (fRefuse && sRefuse) {
            return "3"; //全拒
        }
        if (fRefuse || sRefuse) {
            return "2"; //拒一个
        }
        if (fGiveType.equals("2") || sGiveType.equals("2")) {
            return "2";
        } else {
            return "1";
        }
    }
    
    /***
     * 赔付结论原因
     * 
     */
    private String getGiveDesc(String agivetype) {
        String agivedesc = "";
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llclaimdecision");
        tLDCodeDB.setCode(agivetype);
        if (tLDCodeDB.getInfo()) {
            agivedesc = tLDCodeDB.getCodeName();
        }
        return agivedesc;
    }
    
    /***
     * 解析账单信息
     * 
     */
    private boolean dealFee() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--dealFee()");
    	
    	List aReceiptList =  new ArrayList();
    	aReceiptList = (List)mReceiptList.getChildren();
    	for(int i=0;i<aReceiptList.size();i++){
    		Element tReceiptData = (Element) aReceiptList.get(i);
        	String mMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
    		String tFeeType = tReceiptData.getChildTextTrim("FEETYPE");  // 账单种类
    		String tReceiptNo = tReceiptData.getChildTextTrim("MAINFEENO"); //账单号码
    		String tHospitalCode = tReceiptData.getChildTextTrim("HOSIPITALNO"); //医院编码
    		String tHosipitalName = tReceiptData.getChildTextTrim("HOSIPITALNAME"); // 医院名称
    		String tHospStartDate = tReceiptData.getChildTextTrim("HOSPSTARTDATE"); // 住院日期
    		String tHospEndDate = tReceiptData.getChildTextTrim("HOSPENDDATE"); //出院日期
    		String tFeeDate = tReceiptData.getChildTextTrim("FEEDATE"); //结算日期
    		String tRealHospDate = tReceiptData.getChildTextTrim("REALHOSPDATE"); // 住院天数
    		String tMedicareType = tReceiptData.getChildTextTrim("MEDICARETYPE"); // 医保类型
    		String tFeeAffixType = tReceiptData.getChildTextTrim("FEEAFFIXTYPE"); // 账单类型
    		String tInpatientNo = tReceiptData.getChildTextTrim("INPATIENTNO"); //  住院号
    		//账单表存的汇总值 存账单表时使用
 	       	double mSumFee = 0.0;
 	       	double mPlanFee = 0.0;
 	       	double mSelfAmnt = 0.0;
 	       	double mRefuseAmnt = 0.0;
 	       
    		
    		//校验账单种类
            if (!"1".equals(tFeeType) && !"2".equals(tFeeType) && !"3".equals(tFeeType)) {
                buildError("dealFee", "【账单类型】的值不存在");
                return false;
            }
            
            //校验医保类型
            String tMedResult =LLCaseCommon.checkMedicareType(tMedicareType);
            if(!"".equals(tMedResult)){
            	buildError("dealFee", tMedResult);
                return false;
            }
            
            //费用项目明细
            Element tFeeitemList = tReceiptData.getChild("FEEITEMLIST");
            List aFeeitemData =  new ArrayList();
            aFeeitemData = tFeeitemList.getChildren();
            for(int a=0;a<aFeeitemData.size();a++){
            	Element tFeeitemData = (Element) aFeeitemData.get(a);
            	String tFeeitemCode = tFeeitemData.getChildText("FEEITEMCODE");
            	String tSumFee = tFeeitemData.getChildText("SUMFEE");
            	String tSelfAmnt = tFeeitemData.getChildText("SELFAMNT");
            	String tPreAmnt = tFeeitemData.getChildText("PREAMNT");
            	String tRefuseAmnt = tFeeitemData.getChildText("REFUSEAMNT");
            	double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee),2) ;
            	
            	if(tSumFeeValue<=0){
            	   buildError("dealFee", "【账单金额】的值必须大于0");
      	           return false;
            	}
            	//找到费用项目对应名称  
     	       String tSQL = "select codename   from ldcode where codetype ='llfeeitemtype' and code='"+tFeeitemCode+"' ";
     	       
     	       String tFeeitemName = mExeSQL.getOneValue(tSQL);
     	       if("".equals(tFeeitemName)||null==tFeeitemName||"null".equals(tFeeitemName)){
     	       	 buildError("dealFee", "【费用项目代码】的值不存在");
     	            return false;
     	       }
            	
     	       //费用明细信息
     	       LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();
     	       String tLimit1 = PubFun.getNoLimit("86");
     	       tLLCaseReceiptSchema.setFeeDetailNo(
     	                PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
     	       tLLCaseReceiptSchema.setMainFeeNo(mMainFeeNo);
     	       tLLCaseReceiptSchema.setRgtNo(mCaseNo);
     	       tLLCaseReceiptSchema.setCaseNo(mCaseNo);
     	       tLLCaseReceiptSchema.setFeeItemCode(tFeeitemCode);
     	       tLLCaseReceiptSchema.setFeeItemName(tFeeitemName);
     	       tLLCaseReceiptSchema.setFee(tSumFee);
     	       tLLCaseReceiptSchema.setSelfAmnt(tSelfAmnt);
     	       tLLCaseReceiptSchema.setPreAmnt(tPreAmnt);
     	       tLLCaseReceiptSchema.setRefuseAmnt(tRefuseAmnt);
     	       tLLCaseReceiptSchema.setAvaliFlag("0");
    	       tLLCaseReceiptSchema.setMngCom(mLLCaseSchema.getMngCom());
    	       tLLCaseReceiptSchema.setOperator(mUserCode);
    	       tLLCaseReceiptSchema.setMakeDate(mMakeDate);
    	       tLLCaseReceiptSchema.setMakeTime(mMakeTime);
    	       tLLCaseReceiptSchema.setModifyDate(mMakeDate);
    	       tLLCaseReceiptSchema.setModifyTime(mMakeTime);
    	       
    	       mSumFee += tLLCaseReceiptSchema.getFee();
    	       mPlanFee += tLLCaseReceiptSchema.getPreAmnt();
    	       mSelfAmnt += tLLCaseReceiptSchema.getSelfAmnt();
    	       mRefuseAmnt += tLLCaseReceiptSchema.getRefuseAmnt();
    	       mLLCaseReceiptSet.add(tLLCaseReceiptSchema);

            }
            
    		// 账单信息
    		LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
    		tLLFeeMainSchema.setMainFeeNo(mMainFeeNo);
    		tLLFeeMainSchema.setCaseNo(mCaseNo);
    		tLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
    		tLLFeeMainSchema.setRgtNo(mCaseNo);
    		tLLFeeMainSchema.setFeeType(tFeeType);
    		tLLFeeMainSchema.setFeeAtti("0"); //账单属性为 0 -医院
    		tLLFeeMainSchema.setFeeAffixType(tFeeAffixType);  //账单类型
    		tLLFeeMainSchema.setInHosNo(tInpatientNo); //住院号
//    		tLLFeeMainSchema.setAffixNo(mAffixNo); //总实赔金额
    		tLLFeeMainSchema.setCustomerNo(mInsuredNo);
    		tLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
    		tLLFeeMainSchema.setMedicareType(tMedicareType);
    		tLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
    		tLLFeeMainSchema.setHospitalCode(tHospitalCode);
    		tLLFeeMainSchema.setHospitalName(tHosipitalName);
    		tLLFeeMainSchema.setReceiptNo(tReceiptNo);
    		tLLFeeMainSchema.setFeeDate(tFeeDate);
    		tLLFeeMainSchema.setHospStartDate(tHospStartDate);
    		tLLFeeMainSchema.setHospEndDate(tHospEndDate);
    		tLLFeeMainSchema.setRealHospDate(tRealHospDate);
    		tLLFeeMainSchema.setAge((int) mLLCaseSchema.getCustomerAge());
  	        tLLFeeMainSchema.setSecurityNo(mLCInsuredSchema.getOthIDNo());
  	        tLLFeeMainSchema.setInsuredStat(mLCInsuredSchema.getInsuredStat());
  	        tLLFeeMainSchema.setSumFee(mSumFee);
  	        tLLFeeMainSchema.setPreAmnt(mPlanFee);
  	        tLLFeeMainSchema.setSelfAmnt(mSelfAmnt);
  	        tLLFeeMainSchema.setRefuseAmnt(mRefuseAmnt);
  	        
  	        tLLFeeMainSchema.setOperator(mUserCode);
  	        tLLFeeMainSchema.setMngCom(mLLCaseSchema.getMngCom());
  	        tLLFeeMainSchema.setMakeDate(mMakeDate);
  	        tLLFeeMainSchema.setMakeTime(mMakeTime);
  	        tLLFeeMainSchema.setModifyDate(mMakeDate);
  	        tLLFeeMainSchema.setModifyTime(mMakeTime);
  	        //账单总额
  	        mTableSumFee += mSumFee;
  	        
  	        mLLFeeMainSet.add(tLLFeeMainSchema);
  	        
    	}
        
    	return true;
    }
    
    /**
     * 解析案件信息
     */
    private boolean caseRegister() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--caseRegister()");
    	
    	//校验发生日期格式
    	mAccDate= mCustomerData.getChildText("ACCDATE");
    	if(!PubFun.checkDateForm(mAccDate)){
    		buildError("caseRegister","【发生日期】格式错误(正确格式yyyy-MM-dd)");
    		return false;
    	}
    	
    	//领款总金额
    	mAffixNo = mCustomerData.getChildText("AFFIXNO");
    
    	//银行信息
        mCaseGetMode = mCustomerData.getChildText("CASEGETMODE");
        mBankNo = mCustomerData.getChildText("BANKNO");
    	mAccNo = mCustomerData.getChildText("ACCNO");
    	mAccName = mCustomerData.getChildText("ACCNAME");
    	mRelaDrawerInsured = mCustomerData.getChildText("RELADRAWERINSURED"); 
    	
    	if(!"1".equals(mCaseGetMode) && !"2".equals(mCaseGetMode)){
    		//领取方式非现金时，领款人与被保人关系非空
    		if("".equals(mRelaDrawerInsured)||mRelaDrawerInsured==null||mRelaDrawerInsured=="null" ){
    			buildError("caseRegister","当收益金领取方式非现金时领款人与被保人关系必录");
        		return false;
    		}
    		Pattern tPattern = Pattern.compile("(\\d)+");
    		Matcher tNum= tPattern.matcher(mAccNo);
    		if(!tNum.matches()){
    			buildError("caseRegister","【银行账号】只能是数字组成");
        		return false;
    		}
    	}
    	
        mLimit = PubFun.getNoLimit(mManageCom);
        mCaseNo = PubFun1.CreateMaxNo("CaseNo", mLimit);
        
        mCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", mLimit);
        
        if(!getCaseInfo()){
        	return false;
        }
        
        if(!getSubInfo()){
        	return false;
        }
        
    	return true;
    }
    
    /***
     * 生成事件信息、疾病信息、意外信息、手术信息、申请原因
     * 
     */
    private boolean getSubInfo(){
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--getSubInfo()");
    	
    	String tAccProvinceCode = mCustomerData.getChildText("ACCPROVINCECODE");
    	String tAccCityCode = mCustomerData.getChildText("ACCCITYCODE");
    	String tAccCountyCode = mCustomerData.getChildText("ACCCOUNTYCODE");
    	String tAccPlace = mCustomerData.getChildText("ACCPLACE");
    	String tAccDesc = mCustomerData.getChildText("ACCDESC");
    	String tAccDescType = mCustomerData.getChildText("ACCDESCTYPE");
    	
    	String tAccReason =LLCaseCommon.checkAccPlace(tAccProvinceCode, tAccCityCode, tAccCountyCode);
    	if(!"".equals(tAccReason)){
    		buildError("getSubInfo", tAccReason);
    		return false;
    	}
    	
    	List tReceiptList =  new ArrayList();
    	tReceiptList = (List)mReceiptList.getChildren();
    	Element tReceiptData = (Element) tReceiptList.get(0);
    	String tHospitCode = tReceiptData.getChildText("HOSIPITALNO");
    	String tHospStartDate = tReceiptData.getChildText("HOSPSTARTDATE");
    	String tHospEndDate = tReceiptData.getChildText("HOSPENDDATE");
    	String tHospitalName = tReceiptData.getChildText("HOSIPITALNAME");
    	
    	if(!PubFun.checkDateForm(tHospStartDate)){
    		buildError("getSubInfo","【住院日期】格式错误(正确格式yyyy-MM-dd)");
    		return false;
    	}
    	if(!PubFun.checkDateForm(tHospEndDate)){
    		buildError("getSubInfo","【出院日期】格式错误(正确格式yyyy-MM-dd)");
    		return false;
    	}
    	
    	// 事件信息
    	String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", mLimit);
    	mLLSubReportSchema.setSubRptNo(tSubRptNo);
    	mLLSubReportSchema.setAccDate(mAccDate);
    	mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
    	mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
    	mLLSubReportSchema.setInHospitalDate(tHospStartDate);
    	mLLSubReportSchema.setOutHospitalDate(tHospEndDate);
    	mLLSubReportSchema.setAccPlace(tAccPlace);
    	mLLSubReportSchema.setAccProvinceCode(tAccProvinceCode);
    	mLLSubReportSchema.setAccCityCode(tAccCityCode);
    	mLLSubReportSchema.setAccCountyCode(tAccCountyCode);
    	mLLSubReportSchema.setAccDesc(tAccDesc);
    	mLLSubReportSchema.setAccidentType(tAccDescType);
        mLLSubReportSchema.setOperator(mUserCode);
        mLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLSubReportSchema.setMakeDate(mMakeDate);
        mLLSubReportSchema.setMakeTime(mMakeTime);
        mLLSubReportSchema.setModifyDate(mMakeDate);
        mLLSubReportSchema.setModifyTime(mMakeTime);
        
        System.out.println("客户事件已录入成功，待输入数据库。。。");
        //事件关联信息
        mLLCaseRelaSchema.setCaseNo(mCaseNo);
        mLLCaseRelaSchema.setSubRptNo(tSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
        
        // 疾病信息
        List tDiseaseList = new ArrayList();
        tDiseaseList =(List) mDiseaseList.getChildren();
        for(int i=0;i<tDiseaseList.size();i++){
        	Element tDiseaseData = (Element) tDiseaseList.get(i);
        	LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
        	String tDiseaseCode = tDiseaseData.getChildText("DISEASECODE");
        	String tDiseaseName = tDiseaseData.getChildText("DISEASENAME");
        	//校验录入的疾病信息
        	LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
            tLDDiseaseDB.setICDCode(tDiseaseCode);
            if (!tLDDiseaseDB.getInfo()) {
                 buildError("getSubInfo", "【疾病编码】不存在");
                 return false;
             }
            
            String tCureNo = PubFun1.CreateMaxNo("CURENO", mLimit);
            tLLCaseCureSchema.setSerialNo(tCureNo);
            tLLCaseCureSchema.setCaseNo(mCaseNo);
            tLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo); 
            tLLCaseCureSchema.setHospitalCode(tHospitCode);
            tLLCaseCureSchema.setHospitalName(tHospitalName);
            tLLCaseCureSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
            tLLCaseCureSchema.setCustomerName(mLLCaseSchema.getCustomerName());
            tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
            tLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());
            tLLCaseCureSchema.setSeriousFlag("0");
            tLLCaseCureSchema.setOperator(mUserCode);
            tLLCaseCureSchema.setMngCom(mLLCaseSchema.getMngCom());
            tLLCaseCureSchema.setMakeDate(mMakeDate);
            tLLCaseCureSchema.setMakeTime(mMakeTime);
            tLLCaseCureSchema.setModifyDate(mMakeDate);
            tLLCaseCureSchema.setModifyTime(mMakeTime);
            
            mLLCaseCureSet.add(tLLCaseCureSchema);

        }
        
    	//意外信息
        
        List tAccidentList = new ArrayList();  
        tAccidentList = (List)mAccidentList.getChildren();
        if(tAccidentList.size()>0){
        	for(int a=0;a<tAccidentList.size();a++){
        		Element tAccidentData =  (Element) tAccidentList.get(a);
        		LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
        		String tAccidentCode =tAccidentData.getChildText("ACCIDENTCODE");
        		String tAccidentName = tAccidentData.getChildText("ACCIDENTNAME");
        		//事件类型为意外时，必录
        		
        		if("2".equals(mAccidentType) && ("".equals(tAccidentCode) ||tAccidentCode==null)){
        			 buildError("getSubInfo", "【意外代码】不能为空");
                     return false;
        		}
        		
        		if(!"".equals(tAccidentCode)&& tAccidentCode!=null){
        			System.out.println("上海个险外包--意外信息已录入--看进程走哪。。");
        			LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
                    tLLAccidentTypeDB.setAccidentNo(tAccidentCode);
                    if (!tLLAccidentTypeDB.getInfo()) {
                        buildError("getSubInfo", "【意外代码】不存在");
                        return false;
                    }
                    
                	String AccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mLimit);
                 	tLLAccidentSchema.setAccidentNo(AccidentNo);
                 	tLLAccidentSchema.setCaseNo(mCaseNo);
                 	tLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
                 	tLLAccidentSchema.setCode(tAccidentCode);
                 	tLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());
                 	
                 	mLLAccidentSet.add(tLLAccidentSchema);
        		}else{
        			System.out.println("上海个险外包--意外信息未录入");
        		}
        	}
        	
        }
    	
    	//手术信息
       List tOperationList = new ArrayList();
       tOperationList = mOperationList.getChildren();
       if(tOperationList.size()>0){
    	   for(int o=0;o<tOperationList.size();o++){
    		   Element tOperationData = (Element) tOperationList.get(o);
    		   LLOperationSchema tLLOperationSchema = new LLOperationSchema();
    		   String tOperationCode = tOperationData.getChildText("OPERATIONCODE");
    		   String tOperationName = tOperationData.getChildText("OPERATIONNAME");
    		   if(!"".equals(tOperationCode) && tOperationCode!=null){
    			   System.out.println("上海个险外包--手术信息已录入");
    			   LDICDOPSDB tLDICDOpsDB = new LDICDOPSDB();
        		   tLDICDOpsDB.setICDOPSCode(tOperationCode);
        		   if(!tLDICDOpsDB.getInfo()){
        			   buildError("getSubInfo", "【手术代码】不存在");
                       return false;
        	        }
        		   
        		   String tCureNo = PubFun1.CreateMaxNo("OPERATION", mLimit);
        		   
        		   tLLOperationSchema.setSerialNo(tCureNo);
        		   tLLOperationSchema.setCaseNo(mCaseNo);
        		   tLLOperationSchema.setCaseRelaNo(mCaseRelaNo);
        		   tLLOperationSchema.setOperationCode(tOperationCode);
        		   tLLOperationSchema.setOperationName(tLDICDOpsDB.getICDOPSName());
        		   tLLOperationSchema.setMngCom(mLLCaseSchema.getMngCom());
                   tLLOperationSchema.setOperator(mUserCode);
                   tLLOperationSchema.setMakeDate(mMakeDate);
                   tLLOperationSchema.setMakeTime(mMakeTime);
                   tLLOperationSchema.setModifyDate(mMakeDate);
                   tLLOperationSchema.setModifyTime(mMakeTime);
        		   
                   mLLOperationSet.add(tLLOperationSchema);
    		   }else{
    			   System.out.println("上海个险外包--手术信息未录入");
    		   }
    	   }
       }

       //申请原因
       String tReasonCode = mCustomerData.getChildText("REASONCODE");
       if(!"".equals(tReasonCode)){
    	   mLLAppClaimReasonSchema.setCaseNo(mCaseNo);
    	   mLLAppClaimReasonSchema.setRgtNo(mCaseNo);
    	   mLLAppClaimReasonSchema.setReasonCode(tReasonCode);
    	   mLLAppClaimReasonSchema.setReasonType("0"); // 原因类型
    	   mLLAppClaimReasonSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
    	   mLLAppClaimReasonSchema.setMngCom(mLLCaseSchema.getMngCom());
    	   mLLAppClaimReasonSchema.setOperator(mLLCaseSchema.getOperator());
    	   mLLAppClaimReasonSchema.setMakeDate(mMakeDate);
    	   mLLAppClaimReasonSchema.setMakeTime(mMakeTime);
    	   mLLAppClaimReasonSchema.setModifyDate(mMakeDate);
    	   mLLAppClaimReasonSchema.setModifyTime(mMakeTime);

       }
       
    	return true;
    }
    
    
    
    
    /**
     * 生成案件信息
     * 
     */
    private boolean getCaseInfo() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--getCaseInfo()");
    	
    	mLLCaseSchema.setCaseNo(mCaseNo);
    	mLLCaseSchema.setRgtNo(mCaseNo);
    	mLLCaseSchema.setAccidentDate(mAccDate);
        //申请类案件
        mLLCaseSchema.setRgtType("1");
        //案件状态处理到理算确认
        mLLCaseSchema.setRgtState("01");
        mLLCaseSchema.setCustomerNo(mInsuredNo);
        mLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
        //账单状态录入
        mLLCaseSchema.setReceiptFlag("1");
        //未调查
        mLLCaseSchema.setSurveyFlag("0");
        mLLCaseSchema.setRgtDate(mMakeDate);
        mLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());

        int tAge = LLCaseCommon.calAge(mLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerAge(tAge);
        
        mLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
        mLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
        mLLCaseSchema.setHandler(mUserCode);
        mLLCaseSchema.setDealer(mUserCode); 
        //银行信息
        mLLCaseSchema.setCaseGetMode(mCaseGetMode);
        mLLCaseSchema.setBankCode(mBankNo);
        mLLCaseSchema.setBankAccNo(mAccNo);
        mLLCaseSchema.setAccName(mAccName);
        
        //事件类型
        mAccidentType = mCustomerData.getChildText("ACCDESCTYPE");
        mLLCaseSchema.setAccidentType(mAccidentType);
        
        mLLCaseSchema.setMngCom(mManageCom);
        String tMobilePhone = mCustomerData.getChildText("MOBILEPHONE");
        mLLCaseSchema.setMobilePhone(tMobilePhone);
        //新增一个 上海外包 暂时定位HW--16
        mLLCaseSchema.setCaseProp("16"); 
        mLLCaseSchema.setOperator(mUserCode);
        mLLCaseSchema.setMakeDate(mMakeDate);
        mLLCaseSchema.setMakeTime(mMakeTime);
        mLLCaseSchema.setModifyDate(mMakeDate);
        mLLCaseSchema.setModifyTime(mMakeTime);
        
        mLLCaseSchema.setRigister(mUserCode);
        mLLCaseSchema.setClaimer(mUserCode);
        mLLCaseSchema.setOtherIDType(mLCInsuredSchema.getOthIDType());
        mLLCaseSchema.setOtherIDNo(mLCInsuredSchema.getOthIDNo());

        //申请信息
    	String tRgtantName = mCustomerData.getChildText("RGTANTNAME");
    	String tRgtantIdNo = mCustomerData.getChildText("RGTANTIDNO");
    	String tRgtantIdType = mCustomerData.getChildText("RGTANTIDTYPE");
    	String tRgtantPhone = mCustomerData.getChildText("RGTANTPHONE");
        String tRgtantEmail = mCustomerData.getChildText("RGTANTEMAIL");
        String tRgtantAddress = mCustomerData.getChildText("RGTANTADDRESS");
        String tRgtantRelation = mCustomerData.getChildText("RGTANTRELATION");
        
        if(tRgtantIdType.equals("0")||tRgtantIdType.equals("5")){
        	if(!"".equals(PubFun.CheckIDNo(tRgtantIdType, tRgtantIdNo, "", ""))){
        		buildError("getCaseInfo","【申请人身份证号】不符合规则");
        		return false;
        	}
        }
        
        mLLRegisterSchema.setRgtNo(mCaseNo);
        mLLRegisterSchema.setRgtState("13");
        //客户号
        mLLRegisterSchema.setRgtObj("1");
        mLLRegisterSchema.setRgtObjNo(mInsuredNo);
        //受理方式为 8-社保平台
        mLLRegisterSchema.setRgtType("8");
        //个案受理
        mLLRegisterSchema.setRgtClass("0");
        mLLRegisterSchema.setRgtantName(tRgtantName);
        mLLRegisterSchema.setRelation(tRgtantRelation);
        //银行信息
    	mLLRegisterSchema.setBankCode(mBankNo);
    	mLLRegisterSchema.setAccName(mAccName); 
      	mLLRegisterSchema.setBankAccNo(mAccNo); 
      	mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
      	//申请日期
        mLLRegisterSchema.setAppDate(mMakeDate);
        mLLRegisterSchema.setCustomerNo(mInsuredNo);
        mLLRegisterSchema.setRgtDate(mMakeDate);
        mLLRegisterSchema.setHandler(mUserCode);
        mLLRegisterSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLRegisterSchema.setIDType(tRgtantIdType);
        mLLRegisterSchema.setIDNo(tRgtantIdNo);
        mLLRegisterSchema.setRgtantAddress(tRgtantAddress);
        mLLRegisterSchema.setRgtantMobile(tRgtantPhone);
        mLLRegisterSchema.setEmail(tRgtantEmail);
        mLLRegisterSchema.setOperator(mUserCode);
        mLLRegisterSchema.setMakeDate(mMakeDate);
        mLLRegisterSchema.setMakeTime(mMakeTime);
        mLLRegisterSchema.setModifyDate(mMakeDate);
        mLLRegisterSchema.setModifyTime(mMakeTime);
        
        //案件扩展表
        if(!"".equals(mRelaDrawerInsured) && mRelaDrawerInsured!=null){
        	mLLCaseExtSchema.setCaseNo(mCaseNo);
        	mLLCaseExtSchema.setRelaDrawerInsured(mRelaDrawerInsured);
        	mLLCaseExtSchema.setMngCom(mLLCaseSchema.getMngCom());
        	mLLCaseExtSchema.setOperator(mUserCode);
        	mLLCaseExtSchema.setMakeDate(mMakeDate);
        	mLLCaseExtSchema.setMakeTime(mMakeTime);
        	mLLCaseExtSchema.setModifyDate(mMakeDate);
        	mLLCaseExtSchema.setModifyTime(mMakeTime);
        	
        }
    	return true;
    }
    
    /***
     * 校验客户信息、业务员信息、保单信息、平台唯一性校验
     * 
     */
    private boolean dealInsured() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海外包个险--dealInsured()");
    	
    	//校验业务员信息
    	mUserCode = mUserData.getChildText("USERCODE");
    	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(mUserCode);
    	tLLClaimUserDB.setClaimDeal("1");  //理赔权限
    	tLLClaimUserDB.setHandleFlag("1"); //案件分配权限
    	tLLClaimUserDB.setStateFlag("1"); //理赔员状态
    	
    	LLClaimUserSet tLLClaimUserSet = tLLClaimUserDB.query();
    	if(tLLClaimUserSet.size()<=0){
    		buildError("dealInsured", "案件处理人查询失败或没有权限");
    		return false;
    	}
    	
    	mGlobalInput.Operator=mUserCode;
    	
    	//平台理赔号唯一性校验
    	mClaimNo = mCustomerData.getChildText("CLAIMNO");
    	String tClaimSql = "select * from llhospcase where claimno='"+mClaimNo+"' and HospitCode='"+mRequestType+"' with ur";
    	SSRS tSClaim =mExeSQL.execSQL(tClaimSql);
    	if(tSClaim.getMaxRow()>0){
    		buildError("dealData()", "平台理赔号为："+mClaimNo+"的数据信息已重复使用。");
        	System.out.println("===平台理赔号出现重复，表中已存在，交易编码："+mTransactionNum+"，平台理赔号："+mClaimNo+"===");
        	return false;
    	}
    	
    	//客户信息校验
    	mInsuredNo = mCustomerData.getChildText("INSUREDNO");
    	String tIdNo =mCustomerData.getChildText("IDNO");
    	String tInsuredName =mCustomerData.getChildText("INSUREDNAME");
    	List aContList = new ArrayList();
    	aContList =(List)mPayList.getChildren();
    	Element aPayData =(Element) aContList.get(0);
		String aContNo = aPayData.getChildText("CONTNO");
    	//如果客户号不为空，根据客户号和保单号去被保人表取值，否则根据身份证号和保单号去取值
    	if(!"".equals(mInsuredNo) && mInsuredNo!=null){
	    	LCInsuredBL tLCInsuredBL = new LCInsuredBL();
	    	tLCInsuredBL.setInsuredNo(mInsuredNo);
	    	tLCInsuredBL.setName(tInsuredName);
	    	tLCInsuredBL.setContNo(aContNo);
	    	
	    	if(!tLCInsuredBL.getInfo()){
	    		buildError("dealInsured", "被保险人查询失败");
	    		return false;
	    	}
	    	
	    	mLCInsuredSchema =tLCInsuredBL.getSchema();

    	}else{
    		String InsureSQL ="select insuredno from lcinsured where name='"+tInsuredName+"' and contno='"+aContNo+" '"
    				+ "and idno='"+tIdNo+"' union select insuredno from lbinsured where name='"+tInsuredName+"' and contno='"+aContNo+" '"
    				+ "and idno='"+tIdNo+"' with ur";
    		String tInsuredno = mExeSQL.getOneValue(InsureSQL);
    		if("".equals(tInsuredno) ||tInsuredno==null){
    			buildError("dealInsured", "被保险人查询失败");
	    		return false;
    		}else{
	    		LCInsuredBL tLCInsuredBL = new LCInsuredBL();
		    	tLCInsuredBL.setInsuredNo(tInsuredno);
		    	tLCInsuredBL.setName(tInsuredName);
		    	tLCInsuredBL.setContNo(aContNo);
		    	
		    	if(!tLCInsuredBL.getInfo()){
		    		buildError("dealInsured", "被保险人查询失败");
		    		return false;
		    	}
		    	
		    	mLCInsuredSchema =tLCInsuredBL.getSchema();
		    	mInsuredNo=mLCInsuredSchema.getInsuredNo();  //获取被保人客户号
    	 }
    	}	
    	String tCaseSql ="select * from llcase where customerno='"+mInsuredNo+"' and "
    			+ "rgtstate not in('11','12','14') with ur ";
    	SSRS tSCase =mExeSQL.execSQL(tCaseSql);
    	if(tSCase.getMaxRow()>0){
    		buildError("dealInsured", "客户" + mInsuredNo + "有未处理完的理赔案件，不能进行理赔。");
            return false;
    	}
    	
    	if(!"".equals(LLCaseCommon.checkPerson(mInsuredNo))){
    		buildError("dealInsured", "该客户正在进行保全处理，不能进行理赔");
            return false;
    	}
    	
    	//保单信息校验
    	List tContList = new ArrayList();
    	tContList =(List)mPayList.getChildren();
    	for(int i=0;i<tContList.size();i++){
    		Element tPayData = (Element) tContList.get(i);
    		String tContNo = tPayData.getChildText("CONTNO");
    		String tRiskCode =tPayData.getChildText("RISKCODE");
    		String tGetDutyCode =tPayData.getChildText("GETDUTYCODE");
    		String tGetDutyKind =tPayData.getChildText("GETDUTYKIND");
    		
    		 for (int j =tContList.size()-1; j>i; j--) {
    			    Element pPayData = (Element) tContList.get(j);
    	    		String pContNo = pPayData.getChildText("CONTNO");
    	    		String pRiskCode =pPayData.getChildText("RISKCODE");
    	    		String pGetDutyCode =pPayData.getChildText("GETDUTYCODE");
    	    		String pGetDutyKind =pPayData.getChildText("GETDUTYKIND");
                 if (pContNo.equals(tContNo) && tRiskCode.equals(pRiskCode)
                     && tGetDutyCode.equals(pGetDutyCode) &&tGetDutyKind.equals(pGetDutyKind)) {
              	   buildError("dealFee", "保单"+tContNo+"下的给付责任"+pGetDutyCode+"和给付类型"+pGetDutyKind+"重复录入，请重新确认录入。");
      	           return false;
                 }
             }
    		//校验赔付险种责任信息
    		String tRiskSql="select 1 from ldcode where codetype='shwbrisk' and codename='"+tRiskCode+"'"
    				+ "and comcode='"+tGetDutyCode+"' and  othersign='"+tGetDutyKind+"' with ur";
    		String tNumber = mExeSQL.getOneValue(tRiskSql);
    		if(!"1".equals(tNumber)){
    			System.out.println("上海个险保单:" + tContNo + ",险种:"+tRiskCode+"下的给付责任编码"+ tGetDutyCode +"和给付责任类型"+tGetDutyKind+"不在理赔范围内");
    			buildError("dealCont", "保单:" + tContNo + ",险种:"+tRiskCode+"下的给付责任编码"+ tGetDutyCode +"和给付责任类型"+tGetDutyKind+"不在理赔范围内");
                return false;
    		
    		}
    		
    		//校验保单信息
    		String tContSql="select polno from lcpol where contno='"+tContNo+"' and riskcode='"+tRiskCode+"' "
    				+ "and insuredno='"+mInsuredNo+"' and appflag='1' and stateflag<>'4'  union "
    				+ "select polno from lbpol where contno='"+tContNo+"' and riskcode='"+tRiskCode+"'"
    				+ "and insuredno='"+mInsuredNo+"' and appflag='1' and stateflag<>'4'  with ur";
    		SSRS tSPolNo =mExeSQL.execSQL(tContSql);
    		if(tSPolNo.getMaxRow()!=1){
    			buildError("dealCont", "保单" + tContNo + "下险种"+ tRiskCode +"查询失败");
                return false;
    		}
    		String tPolNo = tSPolNo.GetText(1, 1);
    		
    		if ("".equals(tPolNo)) {
            	buildError("dealCont", "保单" + tContNo + "下险种"+ tRiskCode +"查询失败了");
                return false;
            }
    		
    	}
    	return true;
    }
    
    
    /***
     * 校验报文基本数据
     * 
     */
    private boolean checkData() throws Exception{
    	System.out.println("LLHWAutoCaseRegister--上海个险外包--checkData()");
    	if(!"REQUEST".equals(mDocType)){
    		buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
    	}
    	if(!"HW06".equals(mRequestType)){
    		 buildError("checkData()", "【请求类型】的值不存在或匹配错误");
             return false;
    	}
    	if(mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
        buildError("checkData()", "【交互编码】的编码个数错误");
        return false;
    	}
    	if(!"HW06".equals(mTransactionNum.substring(0, 4))){
    		buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
    	}
    	
    	mManageCom= mTransactionNum.substring(4, 12);
    	if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86310000".equals(mManageCom)) {
	            buildError("checkData()", "【交互编码】的管理机构编码错误!");
	            return false;
	    }
    	
    	mGlobalInput.ManageCom=mManageCom;
    	return true;
    }
    

    /***
     * 解析报文主要部分
     * @param args
     */
    private boolean getInputData(Document cInXml) throws Exception {
    	System.out.println("LLHWAutoCaseRegister--上海个险外包--getInputData()");
    	
    	if(cInXml==null){
    		buildError("getInputData()","未获取到报文");
    		return false;
    	}
    	
    	Element tRootData =cInXml.getRootElement();
    	//获取节点信息
    	mDocType=tRootData.getAttributeValue("type");
    	
    	//获取报文Head部分
    	Element tHeadData= tRootData.getChild("HEAD");
    	mRequestType = tHeadData.getChildText("REQUEST_TYPE");
    	mTransactionNum = tHeadData.getChildText("TRANSACTION_NUM");
    	
    	//获取报文Body部分
    	Element tBodyData = tRootData.getChild("BODY");
    	mPictureData = tBodyData.getChild("PICTURE_DATA");
    	mUserData = tBodyData.getChild("USER_DATA");
    	mCustomerData = tBodyData.getChild("CUSTOMER_DATA");
    	mReceiptList = mCustomerData.getChild("RECEIPTLIST");
    	mPayList = mCustomerData.getChild("PAYLIST");
    	mDiseaseList = mCustomerData.getChild("DISEASELIST");
    	mAccidentList = mCustomerData.getChild("ACCIDENTLIST");
    	mOperationList = mCustomerData.getChild("OPERATIONLIST");

    	return true;
    }
    
    
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHWAutoCaseRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
	
	
	
	public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("D:/Java/test/HWRequest/SHWB_003_2发送报文1.xml"), "GBK");
            LLHWAutoCaseRegister tBusinessDeal = new LLHWAutoCaseRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }

	}

}
