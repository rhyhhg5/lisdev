package com.sinosoft.lis.yibaotong;

/**
 * 此类为支持银保通及核心使用的老版本JDOM(应该是0.9以下)
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

public class JdomUtil {
	/**
	 * 采用GBK编码构建一个Document对象；构建失败，返回null。
	 */
	public static Document build(byte[] pBytes) {
		return build(pBytes, "GBK");
	}
	
	/**
	 * 采用GBK编码构建一个Document对象；构建失败，返回null。
	 */
	public static Document build(InputStream pIs) {
		return build(pIs, "GBK");
	}
	
	/**
	 * 采用指定字符集编码构建一个Document对象；构建失败，返回null。
	 */
	public static Document build(byte[] pBytes, String pCharset) { 
		return build(new ByteArrayInputStream(pBytes), pCharset);
	}
	
	/**
	 * 采用指定字符集编码构建一个Document对象；构建失败，返回null。
	 */
	public static Document build(InputStream pIs, String pCharset) {
		try {
			InputStreamReader mReader = new InputStreamReader(pIs, pCharset);
		
			SAXBuilder mSAXBuilder = new SAXBuilder();
			return mSAXBuilder.build(mReader);
		} catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 将Document输出到指定的输出流。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进3空格。
	 * 注意：此方法不自动关闭流，如有需要，请在调用后手动关闭。
	 */
	public static void output(Document pXmlDoc, OutputStream pOs) throws IOException {
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(pXmlDoc, pOs);
	}
	
	/**
	 * 将Element输出到指定的输出流。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进3空格。
	 * 注意：此方法不自动关闭流，如有需要，请在调用后手动关闭。
	 */
	public static void output(Element pElement, OutputStream pOs) throws IOException {
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(pElement, pOs);
	}
	
	/**
	 * 将Document打印到控制台。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进3空格。
	 */
	public static void print(Document pXmlDoc) {
		try {
			output(pXmlDoc, System.out);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * 将Element打印到控制台。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进3空格。
	 */
	public static void print(Element pElement) {
		try {
			output(pElement, System.out);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * 将Document转换为GBK编码的字节数组，去首尾空格。
	 */
	public static byte[] toBytes(Document pXmlDoc) throws IOException {
		return toBytes(pXmlDoc, "GBK");
	}
	
	/**
	 * 将Element转换为GBK编码的字节数组，去首尾空格。
	 */
	public static byte[] toBytes(Element pElement) throws IOException {
		return toBytes(pElement, "GBK");
	}
	
	/**
	 * 将Document转换为指定字符集编码的字节数组，去首尾空格。
	 */
	public static byte[] toBytes(Document pXmlDoc, String pCharset) throws IOException {
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding(pCharset);
		mXMLOutputter.setTrimText(true);
		ByteArrayOutputStream mBaos = new ByteArrayOutputStream();
		mXMLOutputter.output(pXmlDoc, mBaos);
		return mBaos.toByteArray();
	}
	
	/**
	 * 将Element转换为指定字符集编码的字节数组，去首尾空格。
	 */
	public static byte[] toBytes(Element pElement, String pCharset) throws IOException {
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding(pCharset);
		mXMLOutputter.setTrimText(true);
		ByteArrayOutputStream mBaos = new ByteArrayOutputStream();
		mXMLOutputter.output(pElement, mBaos);
		return mBaos.toByteArray();
	}
	
	/**
     * 根据XML 字符串 建立JDom对象
     * @param xmlString XML格式的字符串
     * @return 返回建立的JDom对象，建立不成功返回null 。
     */
    public static Document buildFromXMLString(String xmlString) {
        try {
            SAXBuilder builder = new SAXBuilder();
            Document anotherDocument = builder.build(new StringReader(xmlString));
            return anotherDocument;
        } catch(JDOMException e) {
            e.printStackTrace();
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }
	
	/**
	 * 这个方法将JDom对象转换字符串.
	 * @param document 将要被转换的JDom对象
	 */
	public static String outputToString(Document document) {
		return outputToString(document, "GBK");
	}

	/**
	 * 这个方法将JDom对象转换字符串.
	 * @param document 将要被转换的JDom对象
	 * @param encoding 输出字符串使用的编码
	 */
	public static String outputToString(Document document, String encoding) {
		ByteArrayOutputStream byteRep = new ByteArrayOutputStream();
		XMLOutputter docWriter = new XMLOutputter("    ", true, encoding);
		try {
			docWriter.output(document, byteRep);
		} catch (Exception e) {

		}

		return byteRep.toString();
	}

	
}

