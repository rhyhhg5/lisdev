package com.sinosoft.lis.yibaotong;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.llcase.CachedLPInfo;

public class YibaotongServlet extends HttpServlet {
	private static final long serialVersionUID = 4567L;
	private static final Logger cLogger = Logger.getLogger(YibaotongServlet.class);

	protected void doGet(HttpServletRequest pRequest, HttpServletResponse pResponse)
		throws ServletException, IOException {
		cLogger.debug("Into YibaotongServlet.doGet()...");
		pResponse.getOutputStream().println(
				"Now in YibaotongServlet, and YibaotongServlet.doGet() work well!");
	}

	protected void doPost(HttpServletRequest pRequest, HttpServletResponse pResponse) {
		cLogger.info("Into YibaotongServlet.doPost()...");
		Document tOutXmlDoc = null;
		
		try {
			//1 获取输入流，转化为org.jdom.Document文档
			Document tInXmlDoc = getInStdXml(pRequest);
			
			//2 调用功能实现，输出org.jdom.Document文档
			tOutXmlDoc = service(tInXmlDoc);
			
			//3 将org.jdom.Document转化为byte[]
			convertByte(pResponse, tOutXmlDoc);
			
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
//			mOutXmlDoc = YBTPostUtil.getSimpleOutStdXml("0", ex.toString());
		}
		
		cLogger.info("Out YibaotongServlet.doPost()!");
	}

	/**
	 * @param pResponse
	 * @param mOutXmlDoc
	 */
	private void convertByte(HttpServletResponse pResponse, Document mOutXmlDoc) {
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("GBK");
		try {
			OutputStream tResponseOs = pResponse.getOutputStream();
			mXMLOutputter.output(mOutXmlDoc, tResponseOs);
			tResponseOs.close();
		} catch (IOException ex) {
			cLogger.error("xml转换失败！", ex);
		}
	}

	/**
	 * @param pRequest
	 * @return
	 * @throws IOException
	 */
	private Document getInStdXml(HttpServletRequest pRequest)
			throws IOException {
		InputStream tRequestIs = pRequest.getInputStream();
		Document tInXmlDoc = JdomUtil.build(tRequestIs);
		tRequestIs.close();
		return tInXmlDoc;
	}
	
	/**
	 * 业务功能类需要提供此方法
	 * 输入: org.jdom.Document
	 * 输出: org.jdom.Document
	 * @param pInXmlDoc
	 * @return
	 */
	public Document service(Document pInXmlDoc) {
		System.out.println("In  Concrete Service.");
		Document tOutStdXml = null;
		
		try {
			Element eHead = pInXmlDoc.getRootElement().getChild("HEAD");
			
//			List tServiceList = ConfigFactory.getServiceConfig().getRootElement().getChildren();
//			String tClassName = null;
//			for (int i=0; i < tServiceList.size(); i++) {
//				Element ttService = (Element) tServiceList.get(i);
//				if (ttService.getChildText("requesttype").equals(eHead.getChildText("REQUEST_TYPE")))
//				{
//					tClassName = ttService.getChildText("class");
//					break;
//				}
//			}
			
//			LDCode1DB tLDCode1DB = new LDCode1DB();
//			tLDCode1DB.setCodeType("ybt_service");
//			tLDCode1DB.setCode(eHead.getChildText("REQUEST_TYPE"));
//			
			String tHospitalCode = eHead.getChildText("TRANSACTION_NUM").substring(2,9);
//			
//			tLDCode1DB.setCode1(tHospitalCode);
//			
//			if (!tLDCode1DB.getInfo()) {
//				tOutStdXml = getSimpleOutStdXml("0", "服务类查询失败", pInXmlDoc);
//			}
			
			CachedLPInfo tCachedLPInfo = CachedLPInfo.getInstance();
			LDCode1Schema tLDCode1Schema = tCachedLPInfo
					.findYBTClassTypeByCode(eHead.getChildText("REQUEST_TYPE"),
							tHospitalCode);
			
			if (tLDCode1Schema == null) {
				tOutStdXml = getSimpleOutStdXml("0", "服务类查询失败", pInXmlDoc);
			}
			
			String tClassName = tLDCode1Schema.getCodeAlias();
			
			if (tClassName==null ||"".equals(tClassName)) {
				tOutStdXml = getSimpleOutStdXml("0", "服务类查询失败", pInXmlDoc);
			}
			
			System.out.println("服务类："+ tClassName + ","+ tLDCode1Schema.getCodeName());
			cLogger.info("服务类：" + tClassName + ","+ tLDCode1Schema.getCodeName());
			Class tServiceClass = Class.forName(tClassName);
			ServiceInterface tService = (ServiceInterface) tServiceClass.newInstance();
			tOutStdXml = tService.service(pInXmlDoc);
		} catch (Throwable ex) {
			cLogger.error("交易出错！", ex);
			tOutStdXml = getSimpleOutStdXml("E", ex.toString(), pInXmlDoc);
		}
		System.out.println("Out  Concrete Service.");
		return tOutStdXml;
	}
	
	/**
	 * 接收报文发生错误
	 * @param pErrorMsg
	 * @return
	 */
	public Document getSimpleOutStdXml(String pResponseCode,String pErrorMsg, Document pInXmlDoc) {
		XMLPubTool tXMLPubTool = new XMLPubTool();
		Element eOldHead = pInXmlDoc.getRootElement().getChild("HEAD");
		
		Element eRequestType = tXMLPubTool.makeElement("REQUEST_TYPE", eOldHead.getChildText("REQUEST_TYPE"));
		Element eResponseCode = tXMLPubTool.makeElement("RESPONSE_CODE", pResponseCode);
		Element eErrorMessage = tXMLPubTool.makeElement("ERROR_MESSAGE", pErrorMsg);
		Element eTransactionNum = tXMLPubTool.makeElement("TRANSACTION_NUM", eOldHead.getChildText("TRANSACTION_NUM"));
		
		Element eHead = tXMLPubTool.makeEmptyElement("HEAD");
		Element ePacket = tXMLPubTool.makeComplexElement("PACKET", "type", "RESPONSE");
		tXMLPubTool.AddAttribute(ePacket, "version", "1.0");
		
		tXMLPubTool.LinkElement(ePacket, eHead);
		tXMLPubTool.LinkElement(eHead,eRequestType);
		tXMLPubTool.LinkElement(eHead,eResponseCode);
		tXMLPubTool.LinkElement(eHead,eErrorMessage);
		tXMLPubTool.LinkElement(eHead,eTransactionNum);
		
		Document tNoStdXml = new Document(ePacket);
		return tNoStdXml;
	}
}
