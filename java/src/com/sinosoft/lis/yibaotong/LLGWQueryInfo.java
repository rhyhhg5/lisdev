package com.sinosoft.lis.yibaotong;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 被保人五要素信息查询返回
 * @author Alfred Xu
 * @time 2016-08-24
 *
 */
public class LLGWQueryInfo implements ServiceInterface{
	
	public CErrors mErrors = new CErrors();
	private Document tInXmlDoc;
	private Element body;
	private Element head;
	private String point;
	private String requestType;
	private String mErrorMessage;//错误信息
	private String mTransactionNum;//交互编码
	private String mresqonseType;

	
	//基本信息
	private String infoMessage[][];
	private String name;
	private String insuredNo;
	private String idNo;
	private String idType;
	private String birthday;

	public Document service(Document pInXmlDoc) {
		try {
            if (!getInputData(pInXmlDoc)) {
            	
            } else {
                if (!checkData()) {
                	
                } else {
                    if (!dealData()) {
                    	
                    }
                }
            }
        } catch (Exception ex) {
			point = "E";

        } finally {
        	if("E".equals(point)||"0".equals(point)){
        		mresqonseType = "0";
        		return CreatErrorXml();
        	}else{
        		mresqonseType = "1";
        		return CreatResponseXml();
        	}
        }
	}

	private Document CreatErrorXml() {
		Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(requestType);
        tHeadData.addContent(tRequestType);

        Element tResponseType = new Element("RESPONSE_TYPE");
        tResponseType.setText(mresqonseType);
        tHeadData.addContent(tResponseType);
        
        //System.out.println(tResponseType.getText());
        
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(point);
        tBody.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        
        return tDocument;
	}

	private Document CreatResponseXml() {
		Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");
        Element tinsuredList = new Element("INSURED_LIST");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(requestType);
        tHeadData.addContent(tRequestType);

        Element tResponseType = new Element("RESPONSE_TYPE");
        tResponseType.setText(mresqonseType);
        tHeadData.addContent(tResponseType);


        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        for(int i =0;i<infoMessage.length;i++){
        	
        	name = infoMessage[i][1];
        	insuredNo = infoMessage[i][0];
        	this.idNo = infoMessage[i][3];
        	idType = infoMessage[i][4];
        	birthday = infoMessage[i][2];
        	
        	Element insuredData = new Element("INSURED_DATA");  
        	
        	Element tname = new Element("NAME");
        	tname.setText(name);
        	insuredData.addContent(tname);
        	
        	Element tinsuredNo = new Element("INSUREDNO");
        	tinsuredNo.setText(insuredNo);
        	insuredData.addContent(tinsuredNo);
        	
        	Element tidNo = new Element("IDNO");
        	tidNo.setText(idNo);
        	insuredData.addContent(tidNo);
        	
        	Element tidType = new Element("IDTYPE");
        	tidType.setText(idType);
        	insuredData.addContent(tidType);
        	
        	Element tbirthday = new Element("BIRTHDAY");
        	tbirthday.setText(birthday);
        	insuredData.addContent(tbirthday);
        	
        	tinsuredList.addContent(insuredData);
        }
        	
        
        tBody.addContent(tinsuredList);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
	}

	private boolean dealData() {
		String grpContNo = body.getChildText("GRPCONTNO");
		String idNo = body.getChildText("IDNO");
		
		ExeSQL tExeSQl = new ExeSQL();
		String findInfo = "select distinct a.insuredno,a.name,a.birthday,a.idno,a.IDType from lcinsured a,lccont b where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4'  and a.IDNo='"+idNo+"' and b.grpcontno='"+grpContNo+"'  union select distinct a.insuredno,a.name,a.birthday,a.idno,a.IDType from lbinsured a,lbcont b where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4'  and a.IDNo='"+idNo+"' and b.grpcontno='"+grpContNo+"'  fetch first 3000 rows only with ur";
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQl.execSQL(findInfo);
		if(tSSRS == null||tSSRS.MaxRow<=0){
			mErrors.addOneError("根据身份证号没有查出相关数据!");
			point = "0";
			return false;
		}else{			
			infoMessage = tSSRS.getAllData();
		}
		return true;
	}

	private boolean checkData() {
		Element root = tInXmlDoc.getRootElement();
		head = root.getChild("HEAD");
		body = root.getChild("BODY");
		requestType = head.getChildText("REQUEST_TYPE");
		mTransactionNum = head.getChildText("TRANSACTION_NUM");
		if(!"GW04".equals(requestType)){
			mErrors.addOneError("请求服务器类型出现错误!");
			point = "0";
			return false;
		}
		if(null==head||null==body){
			mErrors.addOneError("没有得到需要处理的相关信息!");
			point = "0";
			return false;
		}
		
		return true;
	}

	private boolean getInputData(Document pInXmlDoc) {
		this.tInXmlDoc = pInXmlDoc;
		return true;
	}
	
	//以下方法为测试专用方法
	public static void main(String args[]){
		try{
			
			LLGWQueryInfo gwbl = new LLGWQueryInfo();
		/*	File file = new File("e:/xuyp.xml");
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
			
			SAX reader = new SAXReader();           
			Document   document = reader.read(in);
			*/
			 SAXBuilder sb=new SAXBuilder(); 
			 Document doc=sb.build("E:\\LLGW\\GW04.xml");
			 Document response =  gwbl.service(doc);
			 doc2XML(response);
		}catch(Exception e){
			e.getStackTrace();
		}
	}
	
	
	public static void doc2XML(Document doc) throws Exception{
		
		XMLOutputter xmloutputter = new XMLOutputter();
	    OutputStream outputStream;
	    try {
	      outputStream = new FileOutputStream("E:\\LLGW\\outputGW04.xml");
	      xmloutputter.output(doc, outputStream);
	      System.out.println("xml文档生成成功！");
	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	}

}
