package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LLCaseBackDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimPolicyDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.llcase.LLCaseAutoCommonUnderWrite;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.lis.llcase.UnitClaimSaveBL;
import com.sinosoft.lis.llcase.genCaseInfoBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseBackSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLClaimUWMainSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.lis.vschema.LLCaseBackSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseOpTimeSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 自动立案并理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author lyc
 * @version 1.0
 */
public class LLWBCaseRegister implements ServiceInterface
{
    
    public LLWBCaseRegister(){}
    
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private String mErrorMessage = "";  //错误描述    
    

    private String mManageCom="86950000";   //管理机构
    private String mOperator="";   //人员
    
    private List tList = new ArrayList();//返回信息合集
    //报文头信息
    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private String mResponseCode = "1";     //返回类型代码    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    private String mDealType = "2";         //处理类型 1 实时 2非实时 
    private boolean tCancelFlag = true;		//每次运行时将前一批案件撤件
    
    //基本信息
    private int mCaseNum = 0;
    private String mGRPCONTNO = "";       //团体保单号    
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";
    private String mReturnMessage = "";//错误提示集合
    
    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();//医保通案件处理表信息
    
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
    private LLCaseSet mbackCaseSet = new LLCaseSet();//案件回退集合
    private LLCaseSet mCliamCaseSet = new LLCaseSet();//案件回退集合
    


	private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();
	
	private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();


	
	private ES_DOC_RELATIONSet mES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();
	
    private GlobalInput tG=new GlobalInput();
    
   
    //批次号
    private String mRgtNo = "";
    
    private String mTogetherflag="";	// 给付方式
    private String mCasegetmode="";  //赔款领取方式
    
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
    //处理结果标记
    private boolean mDealState=true;  
    private Element mLLCaseList;

    
    public Document service(Document pInXmlDoc)
    {
    	//#2979
    	String sql="select code from ldcode where codetype ='SZWB'";
    	System.out.println(sql);
    	ExeSQL tExeSQL = new ExeSQL();
    	mOperator = tExeSQL.getOneValue(sql);
		System.out.println("mOperator="+mOperator);
		//#2979

    	//TODO 开始
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{
//                        //保存信息到LLHospCase
//                        PubSubmit tPubSubmit = new PubSubmit();
//                        VData tVData = new VData();
//                        MMap tMMap= new MMap();
//                        tMMap.put(mLLHospCaseSet, "INSERT");
//                        tVData.add(tMMap);
//                        tPubSubmit.submitData(tVData, "INSERT");
                    }
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	mInXmlDoc = createXML();
        }
        return mInXmlDoc;
    }
    
    
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLWBCaseRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
    
        
        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mLLCaseList = tBodyData.getChild("LLCASELIST");          //理赔信息    
        tG.ManageCom=mManageCom;
        tG.Operator=mOperator;
       
        return true;

    }
    
    
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
            return false;
        }

        if (!"WB02".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误!");
            return false;
        }

        if (!"WB02".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86950000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误!");
            return false;
        }
                     
        return true;

    }
    
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
    	//TODO Auto-generated method stub
    	//1.受理到检录
    	if(!dealCaseRegister()){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}else{
                //保存信息到LLHospCase
                PubSubmit tPubSubmit = new PubSubmit();
                VData mVData = new VData();
                MMap tMMap= new MMap();
                tMMap.put(mLLHospCaseSet, "INSERT");
                tMMap.put(mES_DOC_RELATIONSet, "INSERT");
                tMMap.put(mES_DOC_MAINSet, "INSERT");
                tMMap.put(mES_DOC_PAGESSet, "INSERT");
                mVData.add(tMMap);
                if(!tPubSubmit.submitData(mVData, "")){
                	 mDealState = false;
                	 buildError("service()", "数据提交报错");
                }
        	}
    	}
        //2.检录到理算,包含核赔
    	if(!batchClaimCal(mRgtNo)){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}
    	}
    	//3.理算确认
    	if(!simpleClaimAudit()){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}
    	}
    	
         
        return true;
    }
    
    /**
     * 直接对整个批次进行理算确认
     * @return
     */
    private boolean simpleClaimAudit() {
    	//TODO Auto-generated method stub
		//1.数据校验
    	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(tG.Operator);
        if(!tLLClaimUserDB.getInfo()){
            // @@错误处理
            buildError("simpleClaimAudit()", "您不是理赔人，无权作确认操作");
            mDealState = false; 
            return false;
        }
        if(tLLClaimUserDB.getClaimPopedom()==null){
            // @@错误处理
            buildError("simpleClaimAudit()", "您没有理赔权限，无权作确认操作");
            mDealState = false; 
            return false;
        }
        
        //2.准备数据
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        mLLCaseSet = tLLCaseDB.query();
        if(mLLCaseSet.size()<=0){
            buildError("simpleClaimAudit()", "没有待确认的案件!");
            mDealState = false; 
            return false;
        }
        for(int i=1;i<=mLLCaseSet.size();i++){
            LLCaseSchema tcase = mLLCaseSet.get(i);               
            if (!dealClaim(tcase)) {
                continue;
            }
//            tcase.setRgtState("04");
//            tcase.setHandler(tG.Operator);
//            if(!UWCase(tcase))
//                continue;    
        }
        
        //3.对错误案件进行回退
        CaseBack(mbackCaseSet);
        
        //4.更新LLregister
        mInputData.clear();
        MMap map = new MMap();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        String tRgtNo = mLLRegisterSchema.getRgtNo();
        tLLRegisterDB.setRgtNo(tRgtNo);
        if (tLLRegisterDB.getInfo()) {
            LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
            tLLRegisterSchema = tLLRegisterDB.getSchema();

                    tLLRegisterSchema.setRgtState("02");
   
            map.put(tLLRegisterSchema, "UPDATE");
            map.put(mCliamCaseSet, "UPDATE");
            mInputData.add(map);
            mResult.add(mReturnMessage);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, null)) {
                CError.buildErr(this,"数据提交失败!");
                return false;
            }
        }
    	
		return true;
	}
    
    /**
     * 对批次下个案进行简单的理算确认
     * @param aCaseSchema
     * @return
     */
    private boolean dealClaim(LLCaseSchema aCaseSchema) {
        VData tvdata = new VData();
        UnitClaimSaveBL tUnitClaimSaveBL = new UnitClaimSaveBL();
        tvdata.add(aCaseSchema);
        tvdata.add(tG);
        if(!(tUnitClaimSaveBL.submitData(tvdata,"BATCH"))){
            CErrors tError = tUnitClaimSaveBL.mErrors;
            String ErrMessage = tError.getFirstError();
            aCaseSchema.setCancleRemark(ErrMessage);
            aCaseSchema.setCancleReason("4");
            mbackCaseSet.add(aCaseSchema);
            //@@错误处理
            mReturnMessage += "<br>案件"+aCaseSchema.getCaseNo()
                    +"信息不全，被自动回退到批改信箱。";
            return false;
        }else{
//        	确认成功的置上状态
        	aCaseSchema.setRgtState("04");
        	aCaseSchema.setHandler(tG.Operator);
        	mCliamCaseSet.add(aCaseSchema);
        }
        return true;
    }

    /**
     * 批次下个案循环审批审定
     * @param aCaseSchema
     * @return
     */
    private boolean UWCase(LLCaseSchema aCaseSchema){
//        VData tvdata = new VData();
//        //此处为循环调用核赔类的入口，在此处判断此批次下案件是否为社保案件，若为社保案件，传入社保批次标志 
//        //由于默认操作人为cm****,此处标记传入"商团"
//        mSocialSecurity = "0";
//        ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
//        LLCaseAutoCommonUnderWrite tLLCaseAutoCommonUnderWrite = new LLCaseAutoCommonUnderWrite();
//        tvdata.clear();
//        tvdata.add(aCaseSchema);
//        tvdata.add(tG);
//        tvdata.add(mLLClaimUserSchema);
//        //将社保标记传入ClaimUnderwriteBL
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("mSocialSecurity", mSocialSecurity);
//        tvdata.add(tTransferData);
//        System.out.println("社保标记为："+mSocialSecurity);
//        if(!tClaimUnderwriteBL.submitData(tvdata,"BATCH||UW")){
//            CErrors tError = tClaimUnderwriteBL.mErrors;
//            String ErrMessage = tError.getFirstError();
//            mReturnMessage += "<br>案件" + aCaseSchema.getCaseNo()
//                    + "审批失败,原因是:"+ErrMessage;
//            return false;
//        }
//        VData tResultData = tClaimUnderwriteBL.getResult();
//        String strResult = (String) tResultData.getObjectByObjectName("String", 0);
//        mReturnMessage+="<br>"+aCaseSchema.getCaseNo()+strResult;
//        return true;
//        
    	LLClaimDB mLLClaimDB = new LLClaimDB();
    	mLLClaimDB.setCaseNo(aCaseSchema.getCaseNo());
    	LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    	mLLClaimSchema = mLLClaimDB.query().get(1);
    	
    	LLClaimPolicyDB mLLClaimPolicyDB = new LLClaimPolicyDB();
    	mLLClaimPolicyDB.setCaseNo(aCaseSchema.getCaseNo());
    	LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    	mLLClaimPolicySet.set(mLLClaimPolicyDB.query());
    	
  	 	LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();

  		  mLLClaimUWMainSchema.setRemark1("外包平台前置审核");

  	  VData tVData = new VData();
        tVData.add(tG);
        tVData.add(aCaseSchema);
        tVData.add(mLLClaimSchema);
        tVData.add(mLLClaimPolicySet);
        tVData.add(mLLClaimUWMainSchema);

        LLCaseAutoCommonUnderWrite tLLCaseAutoCommonUnderWrite = new LLCaseAutoCommonUnderWrite();
        try
        {
      	/* if(mClaimType == null || "".equals(mClaimType)){
      		 if(!tLLCaseUnderWrite.submitData(tVData, "UWCASE"))
               {
                   CError.buildErr(this, tLLCaseUnderWrite.mErrors.getErrContent());
                   return false;
               }
      	 }
      	 else{*/
	             if(!tLLCaseAutoCommonUnderWrite.submitData(tVData, "ENDCASE"))
	             {
	            	 String ErrMessage = tLLCaseAutoCommonUnderWrite.mErrors.getErrContent();
	                 buildError("simpleClaimAudit()", "审批报错："+ErrMessage);
	                 return false;
	             }
           //}

        }catch(Exception ex)
        {
            ex.printStackTrace();
            buildError("simpleClaimAudit()", "结算出现未知异常!");
            return false;
        }         
        

  	  return true;
  
    }
    
    /**
     * 案件回退
     * 1。审定人发现案件错误，做案件回退
     * 2。系统审定操作不能通过，做自动回退
     * @return boolean
     */
    private boolean CaseBack(LLCaseSet aLLCaseSet) {
        System.out.println("外包项目-回退案件数:"+aLLCaseSet.size());
        MMap map = new MMap();
        LLCaseBackSet tLLCaseBackSet = new LLCaseBackSet();
        LLCaseBackSet oLLCaseBackSet = new LLCaseBackSet();
        LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
        boolean tflag= false;
        for(int i=1;i<=aLLCaseSet.size();i++){
            LLCaseSchema tLLCaseSchema = aLLCaseSet.get(i);
            LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
            String oRgtState = ""+tLLCaseSchema.getRgtState();
            String oRgtStateName = "";
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCode(oRgtState);
            tLDCodeDB.setCodeType("llrgtstate");
            if(tLDCodeDB.getInfo())
                oRgtStateName = tLDCodeDB.getCodeName();
            if(!oRgtState.equals("03")&&!oRgtState.equals("04")&&
               !oRgtState.equals("05")&&!oRgtState.equals("06")&&
               !oRgtState.equals("09")&&!oRgtState.equals("10")){
                //写报错信息返回
                mReturnMessage+="案件"+tLLCaseSchema.getCaseNo()+"当前为"
                        +oRgtStateName+",不能做回退操作<br>";
                continue;
            }
            String tLimit = PubFun.getNoLimit(this.tG.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);

            tLLCaseBackSchema.setBeforState(oRgtState);
            tLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            tLLCaseBackSchema.setAviFlag("Y");
            tLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            tLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setNHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setAfterState("01");
            tLLCaseBackSchema.setReason(tLLCaseSchema.getCancleReason());
            tLLCaseBackSchema.setRemark(tLLCaseSchema.getCancleRemark());
            tLLCaseBackSchema.setMngCom(tG.ManageCom);
            tLLCaseBackSchema.setOperator(tG.Operator);
            tLLCaseBackSchema.setMakeDate(mCurrentDate);
            tLLCaseBackSchema.setMakeTime(mCurrentTime);
            tLLCaseBackSchema.setModifyDate(mCurrentDate);
            tLLCaseBackSchema.setModifyTime(mCurrentTime);

            tLLCaseBackSet.add(tLLCaseBackSchema);
            
            //#740 批次导入后、案件回退时增加轨迹表数据
            //add by GY 2013-1-14
            String tSQL= "select max(Sequance) from LLCaseOpTime where caseno = '"+tLLCaseSchema.getCaseNo()+"' and RgtState='01' with ur ";
            ExeSQL mExeSQL = new ExeSQL();
            String tSequance = mExeSQL.getOneValue(tSQL);
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            tLLCaseOpTimeSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseOpTimeSchema.setRgtState("01");
            if("".equals(tSequance)|| tSequance==null ||"null".equals(tSequance) ){
            tLLCaseOpTimeSchema.setSequance("1");//导致案件的第一次回退必须发生在这里
            }else{
            	tSequance = Integer.toString(Integer.parseInt(tSequance)+ 1);
            	 tLLCaseOpTimeSchema.setSequance(tSequance);
            }
            tLLCaseOpTimeSchema.setManageCom(tG.ManageCom);
            tLLCaseOpTimeSchema.setOperator(tG.Operator);
            tLLCaseOpTimeSchema.setStartDate(mCurrentDate);
            tLLCaseOpTimeSchema.setStartTime(mCurrentTime);
            tLLCaseOpTimeSchema.setOpTime("0:00:00");
            
            tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
            
            //保存后当前回退记录生效，需要把过去的回退记录设为过期
            LLCaseBackDB tLLCaseBackDB = new LLCaseBackDB();
            tLLCaseBackDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackDB.setAviFlag("Y");
            LLCaseBackSet tempLLCaseBackSet = new LLCaseBackSet();
            tempLLCaseBackSet = tLLCaseBackDB.query();
            for(int x=1;x<=tempLLCaseBackSet.size();x++){
                tempLLCaseBackSet.get(x).setAviFlag("");
                tempLLCaseBackSet.get(x).setModifyDate(mCurrentDate);
                tempLLCaseBackSet.get(x).setModifyTime(mCurrentTime);
            }
            oLLCaseBackSet.add(tempLLCaseBackSet);
            aLLCaseSet.get(i).setRgtState("01");
            aLLCaseSet.get(i).setCancleReason("");
            aLLCaseSet.get(i).setCancleRemark("");
            aLLCaseSet.get(i).setUWer(tG.Operator);
            aLLCaseSet.get(i).setDealer(tG.Operator);
            aLLCaseSet.get(i).setModifyDate(mCurrentDate);
            aLLCaseSet.get(i).setModifyTime(mCurrentTime);
            tflag = true;
        }
        if(tflag){
            map.put(aLLCaseSet, "UPDATE");
            map.put(tLLCaseBackSet, "INSERT");
            map.put(oLLCaseBackSet, "UPDATE");
            map.put(tLLCaseOpTimeSet, "INSERT");
        }
        this.mInputData.clear();
        mInputData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        return true;
    }

	/**
     * 分案受理
     * @return
     */
    private boolean dealCaseRegister() {
		// TODO Auto-generated method stub
    	System.out.println(tG.Operator+"===="+tG.ManageCom);
    	ExeSQL tExeSQL = new ExeSQL();
    	
        if(mLLCaseList.getChildren("LLCASE_DATA").size()>0){
           
            for(int i=0;i<mLLCaseList.getChildren("LLCASE_DATA").size();i++){
                Element mLLCaseInfo=(Element)mLLCaseList.getChildren("LLCASE_DATA").get(i);
                if(mLLCaseInfo!=null){
                    //调用报文解析类将数据封装到类中
                	LLWBMajorDiseasesCaseParser tLLWBDiseasesCaseParser=new LLWBMajorDiseasesCaseParser(mLLCaseInfo);
                    
                    LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                    LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();////3842 tmm 2018-5-17
                    LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
                    LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                            LLSecurityReceiptSchema();
                    LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
                    
                    VData tVData = new VData();

                    genCaseInfoBL tgenCaseInfoBL = new genCaseInfoBL();

                    //1.通过rgtno查询该批次的团单号,开始处理分案数据
                    mRgtNo = tLLWBDiseasesCaseParser.getRGTNO();
                    String tGrpNoSql = "select rgtobjno,togetherflag,casegetmode  from llregister where rgtno='"+mRgtNo+"' with ur";
                    SSRS tGrpNoSSRS=tExeSQL.execSQL(tGrpNoSql);
                    
                    if(tGrpNoSSRS.getMaxRow() <= 0){
                        buildError("dealData()", "未提供批次申请号,查询信息失败");
                        mDealState = false;                   	
                    }
                    mGRPCONTNO = tGrpNoSSRS.GetText(1, 1);
                    mTogetherflag =tGrpNoSSRS.GetText(1, 2);
	                mCasegetmode =tGrpNoSSRS.GetText(1, 3);
	                 
	                 //给付方式为 1-个人给付时 或者 2-部分给付时 
	                 if("1".equals(mTogetherflag)||"2".equals(mTogetherflag)){
	                	 tLLCaseSchema.setCaseGetMode(mCasegetmode);
	                 }
                    
                    tLLCaseSchema.setCaseNo("");
                    tLLCaseSchema.setRgtNo(mRgtNo);
                    tLLCaseSchema.setAccidentDate(tLLWBDiseasesCaseParser.getACCDATE());
                    tLLCaseSchema.setRgtType("1");//1-申请类
                    tLLCaseSchema.setRgtState("03");//检录状态
                    
                    tLLCaseSchema.setPhone(tLLWBDiseasesCaseParser.getGETDUTYCODE());//给付责任编码,暂存在Phone字段中
                    //3501 如果传客户号，根据客户号，从被保人表里查询，如果不传客户号，则根据证件号码在被保人表中查询 star.
                    if(!"".equals(tLLWBDiseasesCaseParser.getCUSTOMERNO()) && tLLWBDiseasesCaseParser.getCUSTOMERNO()!=null){
                    	String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGRPCONTNO+"' and a.insuredno='"+tLLWBDiseasesCaseParser.getCUSTOMERNO()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGRPCONTNO+"' and b.insuredno='"+tLLWBDiseasesCaseParser.getCUSTOMERNO()+"' ";
                        SSRS tSSRS=tExeSQL.execSQL(tSqlIn);
                        if(tSSRS.getMaxRow()>0){
                            tLLCaseSchema.setCustomerNo(tSSRS.GetText(1, 1));
                            tLLFeeMainSchema.setCustomerNo(tSSRS.GetText(1, 1));
                            tLLCaseSchema.setCustBirthday(tSSRS.GetText(1, 4));  
                            tLLCaseSchema.setIDNo(tSSRS.GetText(1, 5)); 
                            tLLCaseSchema.setIDType(tSSRS.GetText(1, 8));
                            if("".equals(tSSRS.GetText(1, 8))||tSSRS.GetText(1, 8)==null||tSSRS.GetText(1, 8)=="null"){
                           	 tLLCaseSchema.setIDType("4");
                            }
                        }else{
                            buildError("dealData()", "客户查询失败!");
                            mDealState = false; 
                            
                        }  
                    }else{
                    	String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGRPCONTNO+"' and a.idno='"+tLLWBDiseasesCaseParser.getIDNO()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGRPCONTNO+"' and b.idno='"+tLLWBDiseasesCaseParser.getIDNO()+"' ";
                        SSRS tSSRS=tExeSQL.execSQL(tSqlIn);
                        if(tSSRS.getMaxRow()>0){
                            tLLCaseSchema.setCustomerNo(tSSRS.GetText(1, 1));
//                            tLLFeeMainSchema.setCustomerNo(tSSRS.GetText(1, 1));
                            tLLCaseSchema.setCustBirthday(tSSRS.GetText(1, 4));  
                            tLLCaseSchema.setIDNo(tSSRS.GetText(1, 5)); 
                            tLLCaseSchema.setIDType(tSSRS.GetText(1, 8));
                            if("".equals(tSSRS.GetText(1, 8))||tSSRS.GetText(1, 8)==null||tSSRS.GetText(1, 8)=="null"){
                              	 tLLCaseSchema.setIDType("4");
                               }
                        }else{
                            buildError("dealData()", "未提供客户号，社保号或身份证号等信息，无法确认客户身份!");
                            mDealState = false; 
                            
                        }  
                    }
                    //3501 先从被保人表中查询,如果有从被保人表中获取 end.
    
                    tLLCaseSchema.setCustomerName(tLLWBDiseasesCaseParser.getCUSTOMERNAME());                       
                    
                    if(!"".equals(tLLWBDiseasesCaseParser.getMOBILEPHONE()) && tLLWBDiseasesCaseParser.getMOBILEPHONE()!=null){
                        tLLCaseSchema.setMobilePhone(tLLWBDiseasesCaseParser.getMOBILEPHONE());
                    }
                                      
                    tLLCaseSchema.setCustomerSex(tLLWBDiseasesCaseParser.getSEX());
                    
                    if(!"".equals(tLLWBDiseasesCaseParser.getREMARK()) && tLLWBDiseasesCaseParser.getREMARK()!=null){
                    	tLLCaseExtSchema.setRemark(tLLWBDiseasesCaseParser.getREMARK());////3842 tmm 2018-5-17
                    }                   
                    
                    tLLCaseSchema.setRgtState("03"); //案件状态
                    tLLCaseSchema.setSurveyFlag("0");
                    tLLCaseSchema.setAccdentDesc("理赔外包");
                    tLLCaseSchema.setSurveyFlag("0");
                    
                    tLLCaseSchema.setInHospitalDays(tLLWBDiseasesCaseParser.getREALHOSPDATE());
                    
//                    tLLCaseSchema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
                    
                    String tbankSql=" select li.BankCode,li.BankAccNo,li.AccName  from lcinsured li where li.insuredno ='"+tLLCaseSchema.getCustomerNo()+"'  and li.grpcontno ='"+mGRPCONTNO+"'  ";
                    SSRS bankSSRS=tExeSQL.execSQL(tbankSql);
	                 if(!"".equals(tLLWBDiseasesCaseParser.getBANKCODE()) && tLLWBDiseasesCaseParser.getBANKCODE()!=null){
	                	 tLLCaseSchema.setBankCode(tLLWBDiseasesCaseParser.getBANKCODE()); 
	                 }else{
	                	 if(bankSSRS.getMaxRow()>0){
	                		 if(!"".equals(bankSSRS.GetText(1,1))){
	                			 tLLCaseSchema.setBankCode(bankSSRS.GetText(1,1));
	                		 }else{
		                		 //如果批次为个人给付，赔款领取方式为银行，则银行信息不能为空,阻断
		                		 String sebankc="  select count(1) from llregister where 1=1 and rgtno='"+mRgtNo+"'"
		                		 				+" and TogetherFlag in ('1','2') and CaseGetMode not in ('1','2') ";
		                		 String nums=tExeSQL.getOneValue(sebankc);
		                		 if(!"0".equals(nums)){
		                			 buildError("dealData()","【银行编码】的值不能为空");
			                    	 return false;
		                		 }
		                	 }
	                		 
	                	 }
	                 }
	                 
	                 if(!"".equals(tLLWBDiseasesCaseParser.getBANKACCNO()) && tLLWBDiseasesCaseParser.getBANKACCNO() !=null){
	                	 tLLCaseSchema.setBankAccNo(tLLWBDiseasesCaseParser.getBANKACCNO());
	                 }else{
	                	 if(bankSSRS.getMaxRow()>0){
	                		 if(!"".equals(bankSSRS.GetText(1,2))){
	                			 tLLCaseSchema.setBankAccNo(bankSSRS.GetText(1,2));
	                		 }else{
		                		 //如果批次为个人给付，赔款领取方式为银行，则银行信息不能为空,阻断
		                		 String sebankc="  select count(1) from llregister where 1=1 and rgtno='"+mRgtNo+"'"
		                		 				+" and TogetherFlag in ('1','2') and CaseGetMode not in ('1','2') ";
		                		 String nums=tExeSQL.getOneValue(sebankc);
		                		 if(!"0".equals(nums)){
		                			 buildError("dealData()","【账号】的值不能为空");
			                    	 return false;
		                		 }
		                	 }
	                		 
	                	 }
	                 }
	                 
	                 if(!"".equals(tLLWBDiseasesCaseParser.getACCNAME()) && tLLWBDiseasesCaseParser.getACCNAME()!=null){
	                	 tLLCaseSchema.setAccName(tLLWBDiseasesCaseParser.getACCNAME());
	                 }else{
	                	 if(bankSSRS.getMaxRow()>0){
	                		 if(!"".equals(bankSSRS.GetText(1,3))){
	                			 tLLCaseSchema.setAccName(bankSSRS.GetText(1,3));
	                		 }else{
		                		 //如果批次为个人给付，赔款领取方式为银行，则银行信息不能为空,阻断
		                		 String sebankc="  select count(1) from llregister where 1=1 and rgtno='"+mRgtNo+"'"
		                		 				+" and TogetherFlag in ('1','2') and CaseGetMode not in ('1','2') ";
		                		 String nums=tExeSQL.getOneValue(sebankc);
		                		 if(!"0".equals(nums)){
		                			 buildError("dealData()","【账户名】的值不能为空");
			                    	 return false;
		                		 }
		                	 }
	                		 
	                	 }
	                 }
	                 
	                 
	                 
                    //开始处理账单信息
                                   
                    tLLFeeMainSchema.setRgtNo(mRgtNo);
                    tLLFeeMainSchema.setCaseNo("");
                    
                    //赋值可以使用导入的getdutykind
                    tLLFeeMainSchema.setOldMainFeeNo(tLLWBDiseasesCaseParser.getGETDUTYKIND());
                    
                    tLLFeeMainSchema.setCustomerName(tLLWBDiseasesCaseParser.getCUSTOMERNAME());
                    tLLFeeMainSchema.setCustomerSex(tLLWBDiseasesCaseParser.getSEX());
                    tLLFeeMainSchema.setInsuredStat(tLLWBDiseasesCaseParser.getINSUREDSTAT());
                    tLLFeeMainSchema.setHospitalCode(tLLWBDiseasesCaseParser.getHOSPITALNAMECODE());
                    tLLFeeMainSchema.setHospitalName(tLLWBDiseasesCaseParser.getHOSPITALNAME());
                    tLLFeeMainSchema.setSecurityNo(tLLWBDiseasesCaseParser.getSECURYTINO());
                   
                    tLLFeeMainSchema.setFeeAtti("4");
                    tLLFeeMainSchema.setSelfAmnt(tLLWBDiseasesCaseParser.getSELFAMNT());
                    tLLFeeMainSchema.setFeeType(tLLWBDiseasesCaseParser.getFEETYPE());
                    tLLFeeMainSchema.setReceiptNo(tLLWBDiseasesCaseParser.getRECEIPTNO());
                    tLLFeeMainSchema.setFeeDate(tLLWBDiseasesCaseParser.getFEEDATE());
                    
                    tLLFeeMainSchema.setHospStartDate(tLLWBDiseasesCaseParser.getHOSPSTARTDATE());
                    tLLFeeMainSchema.setHospEndDate(tLLWBDiseasesCaseParser.getHOSPENDDATE());
                    tLLFeeMainSchema.setRealHospDate(tLLWBDiseasesCaseParser.getREALHOSPDATE());
                    
                    if(!"".equals(tLLWBDiseasesCaseParser.getINPATIENTNO()) && tLLWBDiseasesCaseParser.getINPATIENTNO()!=null){
                        tLLFeeMainSchema.setInHosNo(tLLWBDiseasesCaseParser.getINPATIENTNO());
                    }
                    
                    //第三方
                    if(!"".equals(tLLWBDiseasesCaseParser.getOTHERORGANAMNT()) && tLLWBDiseasesCaseParser.getOTHERORGANAMNT()!=null){
                        tLLFeeMainSchema.setOtherOrganAmnt(tLLWBDiseasesCaseParser.getOTHERORGANAMNT());
                    }
                     
                    if(!"".equals(tLLWBDiseasesCaseParser.getREFUSEAMNT()) && tLLWBDiseasesCaseParser.getREFUSEAMNT()!=null){
                        tLLFeeMainSchema.setRefuseAmnt(tLLWBDiseasesCaseParser.getREFUSEAMNT());
                    }
                    
                    tLLFeeMainSchema.setSumFee(tLLWBDiseasesCaseParser.getAPPLYAMNT());
                    
                    
                    //可报销范围内金额
                    
                    if(!"".equals(tLLWBDiseasesCaseParser.getREIMBBURSEMENT()) && tLLWBDiseasesCaseParser.getREIMBBURSEMENT()!=null){
                        tLLFeeMainSchema.setReimbursement(tLLWBDiseasesCaseParser.getREIMBBURSEMENT());
                    }
                    
                    tLLFeeMainSchema.setAffixNo(tLLWBDiseasesCaseParser.getAFFIXNO());
                    tLLFeeMainSchema.setOriginFlag(tLLWBDiseasesCaseParser.getORIGINFLAG());
                    
                    // #3544 出险地点 、医保类型**start**
                    // 校验医保类型
                    String tMedicaretype =tLLWBDiseasesCaseParser.getMEDICARETYPE();
                    String tMedicareCheck =LLCaseCommon.checkMedicareType(tMedicaretype);
                    if(!"".equals(tMedicareCheck)){
                    	buildError("dealData()",tMedicareCheck+"。");
                        mDealState = false; 
                        return false;
                    }else{
                    	tLLFeeMainSchema.setMedicareType(tMedicaretype);
                    }
                    // 校验发生地点省、市、 县
                    LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
                    String tAccProvinceCode= tLLWBDiseasesCaseParser.getACCPROVINCECODE();
                    String tAccCityCode = tLLWBDiseasesCaseParser.getACCCITYCODE();
                    String tAccCountyCode = tLLWBDiseasesCaseParser.getACCCOUNTYCODE();
                    String tAccCheck =LLCaseCommon.checkAccPlace(tAccProvinceCode, tAccCityCode, tAccCountyCode);
                    if(!"".equals(tAccCheck)){
                    	buildError("dealData()",tAccCheck+"。");
                        mDealState = false; 
                        return false;
                    }else{
                    	tLLSubReportSchema.setAccProvinceCode(tAccProvinceCode);
                    	tLLSubReportSchema.setAccCityCode(tAccCityCode);
                    	tLLSubReportSchema.setAccCountyCode(tAccCountyCode);
                    }
                    // # 3544 出险地点、医保类型 **end**
                    
                    //社保账单填充
                 
                    tLLSecurityReceiptSchema.setFeeDetailNo("");
                    tLLSecurityReceiptSchema.setMainFeeNo("");
                    tLLSecurityReceiptSchema.setRgtNo(mRgtNo);
                    tLLSecurityReceiptSchema.setCaseNo("");
                    tLLSecurityReceiptSchema.setApplyAmnt(tLLWBDiseasesCaseParser.getAPPLYAMNT());
                    tLLSecurityReceiptSchema.setFeeInSecu("");
                   
                    tLLSecurityReceiptSchema.setYearPlayFee("");//年付统筹支付
                    
                    if(!"".equals(tLLWBDiseasesCaseParser.getSUPINHOSFEE()) && tLLWBDiseasesCaseParser.getSUPINHOSFEE()!=null){
                        tLLSecurityReceiptSchema.setSupInHosFee(tLLWBDiseasesCaseParser.getSUPINHOSFEE());//大额医疗费
                    }
                   
                    //高段一责任金额
                    tLLSecurityReceiptSchema.setHighAmnt1(tLLWBDiseasesCaseParser.getHIGHAMNT1());//大额救助支付 
                    
                    //个人自付
                    if(!"".equals(tLLWBDiseasesCaseParser.getSELFPAY1()) && tLLWBDiseasesCaseParser.getSELFPAY1()!=null){
                        tLLSecurityReceiptSchema.setSelfPay1(tLLWBDiseasesCaseParser.getSELFPAY1());//SELFPAY1个人自付
                    }
                    //部分自付
                    if(!"".equals(tLLWBDiseasesCaseParser.getSELFPAY2()) && tLLWBDiseasesCaseParser.getSELFPAY2()!=null){
                        tLLSecurityReceiptSchema.setSelfPay2(tLLWBDiseasesCaseParser.getSELFPAY2());//SELFPAY1个人自付
                    }
                    
                   
                    tLLSecurityReceiptSchema.setSelfAmnt(tLLWBDiseasesCaseParser.getSELFAMNT());//全部自费
                    
                    tLLSecurityReceiptSchema.setGetLimit(tLLWBDiseasesCaseParser.getGETLIMIT());
//                  //账户支付
                    if(!"".equals(tLLWBDiseasesCaseParser.getACCOUNTPAY()) && tLLWBDiseasesCaseParser.getACCOUNTPAY()!=null){
                        tLLSecurityReceiptSchema.setAccountPay(tLLWBDiseasesCaseParser.getACCOUNTPAY());
                    }
                    //统筹支付
                    if(!"".equals(tLLWBDiseasesCaseParser.getPLANFEE()) && tLLWBDiseasesCaseParser.getPLANFEE()!=null){
                        tLLSecurityReceiptSchema.setPlanFee(tLLWBDiseasesCaseParser.getPLANFEE());//统筹支付
                    }
                    tLLSecurityReceiptSchema.setMidAmnt(tLLWBDiseasesCaseParser.getMIDAMNT());
                    
                   
                    //分案诊疗明细
//                    tLLCaseCureSchema.setCustomerName(tLLWBDiseasesCaseParser.getCUSTOMERNAME());
//                    tLLCaseCureSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
//                    tLLCaseCureSchema.setHospitalName(tLLWBDiseasesCaseParser.getHOSPITALNAME());
//                    tLLCaseCureSchema.setHospitalCode(tLLWBDiseasesCaseParser.getHOSPITALNAMECODE());
//                    tLLCaseCureSchema.setReceiptNo(tLLWBDiseasesCaseParser.getRECEIPTNO());
//                    tLLCaseCureSchema.setDiseaseName(tLLWBDiseasesCaseParser.getDISEASENAME());
//                    tLLCaseCureSchema.setDiseaseCode(tLLWBDiseasesCaseParser.getDISEASECODE());
//                    tLLCaseCureSet.add(tLLCaseCureSchema);
//                    诊疗信息
//                    Element mDiseaseList = mLLCaseInfo.getChild("DISEASELIST");
                	List tDiseaseList = new ArrayList();
//                	tDiseaseList = (List) mDiseaseList.getChildren();
                	tDiseaseList = (List)tLLWBDiseasesCaseParser.getDISEASELIST().getChildren();

                	LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet(); 
                	if(tDiseaseList.size()>0){
                    for (int k = 0; k < tDiseaseList.size(); k++) 
                    {
                        Element tDiseaseData = (Element) tDiseaseList.get(k);
                        String mDiseaseCode = tDiseaseData.getChildText("DISEASECODE");
                        String mDiseaseName = tDiseaseData.getChildText("DISEASENAME");

                        if (mDiseaseCode == null || "".equals(mDiseaseCode)|| "null".equals(mDiseaseName)) {
                            buildError("CustomerWS", "【疾病代码】的值不能为空");
                            return false;
                        }
                        if (mDiseaseName == null || "".equals(mDiseaseName)|| "null".equals(mDiseaseName)) {
                            buildError("CustomerWS", "【疾病名称】的值不能为空");
                            return false;
                        }           
                        LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
                        mLLCaseCureSchema.setReceiptNo(tLLWBDiseasesCaseParser.getRECEIPTNO());
                        mLLCaseCureSchema.setDiseaseCode(mDiseaseCode);
                        mLLCaseCureSchema.setDiseaseName(mDiseaseName);
                        mLLCaseCureSet.add(mLLCaseCureSchema);                       
                    }
                	}
////                	处理扫描件信息
//                    Element tImageInfos = tLLWBDiseasesCaseParser.getIMAGEINFOS();
//                	List tPageInfo = new ArrayList();
//                	tPageInfo = (List) tImageInfos.getChildren("PAGEINFO");
//                	String mSunType = tImageInfos.getChildTextTrim("SUBTYPE");
//                	String mPages = tImageInfos.getChildTextTrim("NUMPAGES");
//
//        			mES_DOC_MAINSchema.setDocCode(mRgtNo);
//        			mES_DOC_MAINSchema.setInputStartDate(mCurrentDate);
//        			mES_DOC_MAINSchema.setInputEndDate(mCurrentDate);
//        			mES_DOC_MAINSchema.setSubType("LP02");
//        			mES_DOC_MAINSchema.setBussType("LP");
//        			mES_DOC_MAINSchema.setManageCom(mManageCom);
//        			mES_DOC_MAINSchema.setVersion("01");
//        			mES_DOC_MAINSchema.setScanNo("0");
//        			mES_DOC_MAINSchema.setState("01");
//        			mES_DOC_MAINSchema.setDocFlag("1");
//        			mES_DOC_MAINSchema.setOperator(mOperator);
//        			mES_DOC_MAINSchema.setScanOperator(mOperator);
//        			mES_DOC_MAINSchema.setNumPages(mPages);
//        			String strDocID = getMaxNo("DocID");
//        			mES_DOC_MAINSchema.setDocID(strDocID);
//        			mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
//        			mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
//        			mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
//        			mES_DOC_MAINSchema.setModifyTime(mCurrentTime);
//        			
//                	if(tPageInfo.size()>0){
//                    for (int k = 0; k < tPageInfo.size(); k++) 
//                    {
//                        Element tPageInfoDate = (Element) tPageInfo.get(k);
//                        String mPageCode = tPageInfoDate.getChildText("PAGECODE");
//                        String mPageName = tPageInfoDate.getChildText("PAGENAME");
//                        String mPageSuffix = tPageInfoDate.getChildText("PAGESUFFIX");
//                        String mPicPath = tPageInfoDate.getChildText("PICPATH");
//                        
//                        ExeSQL tExeSql = new ExeSQL();
//                        String tFilePathIp = tExeSql
//                        	.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='UrlPicPath'");
//                        
//                        String tFilePathUrl = tExeSql
//                    	.getOneValue("select codename from ldcode where codetype='WBClaim' and code='ServerIp'");
//                        
//                        String tFilePath = tExeSql
//                    	.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='ServerIp'");
//
//    					ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
//    					String strPageID = getMaxNo("PageID");
//    					tES_DOC_PAGESSchema.setHostName(tFilePathIp);
//    					tES_DOC_PAGESSchema.setPageCode(mPageCode);
//    					tES_DOC_PAGESSchema.setPageName(mPageName);
//    					tES_DOC_PAGESSchema.setPageSuffix(".gif");
//    					tES_DOC_PAGESSchema.setPageFlag("1");
//    					tES_DOC_PAGESSchema.setPageType("0");
//    					tES_DOC_PAGESSchema.setPicPath(tFilePath);
//    					tES_DOC_PAGESSchema.setPicPathFTP(tFilePathUrl);
//    					tES_DOC_PAGESSchema.setManageCom(mOperator);
//    					tES_DOC_PAGESSchema.setOperator(mOperator);
//    					tES_DOC_PAGESSchema.setPageID(strPageID);
//    					tES_DOC_PAGESSchema.setDocID(strDocID);
//    					tES_DOC_PAGESSchema.setMakeDate(mCurrentDate);
//    					tES_DOC_PAGESSchema.setModifyDate(mCurrentDate);
//    					tES_DOC_PAGESSchema.setMakeTime(mCurrentTime);
//    					tES_DOC_PAGESSchema.setModifyTime(mCurrentTime);
//    					mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
//    				                   
//                    }
//                    mES_DOC_RELATIONSchema.setBussNo(mRgtNo);
//            		mES_DOC_RELATIONSchema.setDocCode(mRgtNo);
//            		mES_DOC_RELATIONSchema.setBussNoType("21");
//            		mES_DOC_RELATIONSchema.setDocID(strDocID);
//            		mES_DOC_RELATIONSchema.setBussType(mES_DOC_MAINSchema.getBussType());
//            		mES_DOC_RELATIONSchema.setSubType(mES_DOC_MAINSchema.getSubType());
//            		mES_DOC_RELATIONSchema.setRelaFlag("0");
//            		
//                	}  
//                	
                	
                    //案件实效
                    mLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
                    mLLCaseOpTimeSchema.setRgtState("01");
                    mLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
                    mLLCaseOpTimeSchema.setOperator(mOperator);
                    mLLCaseOpTimeSchema.setManageCom(mManageCom);
                                       
                    //申请原因
                    LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
                    if (tLLWBDiseasesCaseParser.getFEETYPE().equals("1")) {
                        tLLAppClaimReasonSchema.setReasonCode("01");
                        tLLAppClaimReasonSchema.setReason("门诊"); //申请原因
                    } else if(tLLWBDiseasesCaseParser.getFEETYPE().equals("2")){
                        tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
                        tLLAppClaimReasonSchema.setReason("住院"); //申请原因
                    }else if(tLLWBDiseasesCaseParser.getFEETYPE().equals("3")){
                        tLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
                        tLLAppClaimReasonSchema.setReason("特种病"); //申请原因
                    }else{
                        //#531_如果不是以上三种类型，则不存
                        tLLAppClaimReasonSchema.setReasonCode("00"); //原因代码
                        tLLAppClaimReasonSchema.setReason("无"); //申请原因
                    }
                    tLLAppClaimReasonSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
                    tLLAppClaimReasonSchema.setReasonType("0");
                    
                    if(!"00".equals(tLLAppClaimReasonSchema.getReasonCode())){
                        tVData.add(tLLAppClaimReasonSchema);    
                    }
                        
                    //开始提交案件信息
                    LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
                    tLLRegisterSchema.setRgtNo(mRgtNo);
                    tLLRegisterSchema.setRgtObjNo(mGRPCONTNO);
                    
                    tVData.add(tLLRegisterSchema);
                    tVData.add(tLLCaseSchema);
                    tVData.add(tLLFeeMainSchema);
                    tVData.add(tLLSecurityReceiptSchema);
                    tVData.add(mLLCaseCureSet);
                    tVData.add(mLLCaseOpTimeSchema);
                    tVData.add(tG);
                    if(tLLSubReportSchema!=null){ // 3544 发生地点、医保类型
                    	tVData.add(tLLSubReportSchema);
                    }
                    if(tLLCaseExtSchema !=null){
                    	tVData.add(tLLCaseExtSchema);//3842 tmm 2018-5-17
	                 }
                    if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")) {
                        CErrors tError = tgenCaseInfoBL.mErrors;
                        tgenCaseInfoBL.getResult();
                        String ErrMessage = tError.getFirstError();
                        buildError("dealData()", "生成理赔信息出错!客户号:"+tLLCaseSchema.getCustomerNo()+"客户姓名:"+tLLCaseSchema.getCustomerName()+"错误信息:"+ErrMessage);
                        return false;
	                }else{
	                	
	                	mCaseNum = mCaseNum+1;
	                    tLLCaseSchema=(LLCaseSchema)tgenCaseInfoBL.getResult().getObjectByObjectName("LLCaseSchema", 0);
	                    //tLLCaseSchema=(LLCaseSchema)tLLSSgenCaseInfoBL.getResult().getObjectByObjectName("LLCaseSchema", 0);                      
	                    tList.add(mCaseNum+","+tLLCaseSchema.getCaseNo()+","+tLLWBDiseasesCaseParser.getCLAIMNO());	                      
	                    System.out.println("tList:"+tList.size());	  
	                    
	                    String tCaseNo=tLLCaseSchema.getCaseNo();
	                	
//	                	处理扫描件信息
	                    Element tImageInfos = tLLWBDiseasesCaseParser.getIMAGEINFOS();
	                	List tPageInfo = new ArrayList();
	                	tPageInfo = (List) tImageInfos.getChildren("pageinfo");
	                	String mSunType = tImageInfos.getChildTextTrim("subtype");
	                	String mPages = tImageInfos.getChildTextTrim("NUMPAGES");
	                	if(!"".equals(mPages)&& mPages!=null && !"null".equals(mPages)){
	                		ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();
	        			mES_DOC_MAINSchema.setDocCode(tCaseNo);
	        			mES_DOC_MAINSchema.setInputStartDate(mCurrentDate);
	        			mES_DOC_MAINSchema.setInputEndDate(mCurrentDate);
	        			mES_DOC_MAINSchema.setSubType("LP01");
	        			mES_DOC_MAINSchema.setBussType("LP");
	        			mES_DOC_MAINSchema.setManageCom(mManageCom);
	        			mES_DOC_MAINSchema.setVersion("01");
	        			mES_DOC_MAINSchema.setScanNo("0");
	        			mES_DOC_MAINSchema.setState("01");
	        			mES_DOC_MAINSchema.setDocFlag("1");
	        			mES_DOC_MAINSchema.setOperator(mOperator);
	        			mES_DOC_MAINSchema.setScanOperator(mOperator);
	        			mES_DOC_MAINSchema.setNumPages(mPages);
	        			String strDocID = getMaxNo("DocID");
	        			mES_DOC_MAINSchema.setDocID(strDocID);
	        			mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
	        			mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
	        			mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
	        			mES_DOC_MAINSchema.setModifyTime(mCurrentTime);
	        			mES_DOC_MAINSet.add(mES_DOC_MAINSchema);
	        			
	                	if(tPageInfo.size()>0){
	                    for (int k = 0; k < tPageInfo.size(); k++) 
	                    {
	                    	
	                        Element tPageInfoDate = (Element) tPageInfo.get(k);
	                        String mPageCode = tPageInfoDate.getChildText("pagecode");
	                        String mPageName = tPageInfoDate.getChildText("PAGENAME");
	                        String mPageSuffix = tPageInfoDate.getChildText("PAGESUFFIX");
	                        String mPicPath = tPageInfoDate.getChildText("PICPATH");
	                        if(!"".equals(mPageCode)&& mPageCode!=null && !"null".equals(mPageCode)&&!"".equals(mPageName)&& mPageName!=null && !"null".equals(mPageName)){
	                        ExeSQL tExeSql = new ExeSQL();
	                        String tFilePathIp = tExeSql
	                        	.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='UrlPicPath'");
	                        
	                        String tFilePathUrl = tExeSql
	                    	.getOneValue("select codename from ldcode where codetype='WBClaim' and code='ServerIp'");
	                        tFilePathUrl = tFilePathUrl + PubFun.getCurrentDate()+"/";
	                        String tFilePath = tExeSql
	                    	.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='ServerIp'");
	                        tFilePath = tFilePath + PubFun.getCurrentDate()+"/";
	    					ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
	    					String strPageID = getMaxNo("PageID");
	    					tES_DOC_PAGESSchema.setHostName(tFilePathIp);
	    					tES_DOC_PAGESSchema.setPageCode(mPageCode);
	    					tES_DOC_PAGESSchema.setPageName(mPageName);
	    					tES_DOC_PAGESSchema.setPageSuffix(".jpg");
	    					tES_DOC_PAGESSchema.setPageFlag("1");
	    					tES_DOC_PAGESSchema.setPageType("0");
	    					tES_DOC_PAGESSchema.setPicPath(tFilePath);
	    					tES_DOC_PAGESSchema.setPicPathFTP(tFilePathUrl);
	    					tES_DOC_PAGESSchema.setManageCom(mManageCom);
	    					tES_DOC_PAGESSchema.setOperator(mOperator);
	    					tES_DOC_PAGESSchema.setPageID(strPageID);
	    					tES_DOC_PAGESSchema.setDocID(strDocID);
	    					tES_DOC_PAGESSchema.setMakeDate(mCurrentDate);
	    					tES_DOC_PAGESSchema.setModifyDate(mCurrentDate);
	    					tES_DOC_PAGESSchema.setMakeTime(mCurrentTime);
	    					tES_DOC_PAGESSchema.setModifyTime(mCurrentTime);
	    					mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
	                    	}               
	                    }
	                	 ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
	                    mES_DOC_RELATIONSchema.setBussNo(tCaseNo);
	            		mES_DOC_RELATIONSchema.setDocCode(tCaseNo);
	            		mES_DOC_RELATIONSchema.setBussNoType("21");
	            		mES_DOC_RELATIONSchema.setDocID(strDocID);
	            		mES_DOC_RELATIONSchema.setBussType(mES_DOC_MAINSchema.getBussType());
	            		mES_DOC_RELATIONSchema.setSubType(mES_DOC_MAINSchema.getSubType());
	            		mES_DOC_RELATIONSchema.setRelaFlag("0");
	            		mES_DOC_RELATIONSet.add(mES_DOC_RELATIONSchema);
	                	}  
	  
	                	}

	                	                  
	                    
	                    //信息到LLHospCase
	                    LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
	                    mLLHospCaseSchema.setCaseNo(tLLCaseSchema.getCaseNo());             
	                    mLLHospCaseSchema.setDealType(mDealType);
	                    mLLHospCaseSchema.setHospitCode(mRequestType);
	                    mLLHospCaseSchema.setHandler(mOperator);
	                    mLLHospCaseSchema.setAppTranNo(mTransactionNum);
	                    mLLHospCaseSchema.setAppDate(mCurrentDate);
	                    mLLHospCaseSchema.setAppTime(mCurrentTime);                         
	                    mLLHospCaseSchema.setConfirmState("1");
	                    if(mRequestType.equals("WB02")){
	                    	mLLHospCaseSchema.setCaseType("04");//外包项目-04
	                    }
	                      
	                    mLLHospCaseSchema.setClaimNo(tLLWBDiseasesCaseParser.getCLAIMNO());
	                    mLLHospCaseSchema.setBnfNo(tLLCaseSchema.getCustomerNo());   
	                    mLLHospCaseSchema.setRealpay(tLLWBDiseasesCaseParser.getAFFIXNO());//申报金额
	                    mLLHospCaseSchema.setMakeDate(mCurrentDate);
	                    mLLHospCaseSchema.setMakeTime(mCurrentTime);
	                    mLLHospCaseSchema.setModifyDate(mCurrentDate);
	                    mLLHospCaseSchema.setModifyTime(mCurrentTime);
	                      
	                    mLLHospCaseSet.add(mLLHospCaseSchema);
	                

	                                           
	                }                    
                }else{
                    buildError("dealData()", "获取理赔信息出错!");
                    mDealState = false;
                    return false;
                }
            }
        }else{
            buildError("dealData()", "报文中没有理赔信息!");
            mDealState = false; 
            return false;
        }
         
        return true;
    
	}

    /**
     * 理算及核赔
     * @return
     */
	private boolean batchClaimCal(String aRgtNo) {
		// TODO Auto-generated method stub
		//1.理算前校验
        MMap tmap = new MMap();
        VData tdata = new VData();
		LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(aRgtNo);
        if (!tLLRegisterDB.getInfo()) {
            // @@错误处理
            buildError("dealData()", "团体立案信息查询失败!");
            mDealState = false; 
            return false;
        }
        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();//撤件标志
        if ("1".equals(declineflag)) {
            // @@错误处理
            buildError("dealData()", "该团体申请已撤件，不能再录入个人客户!");
            mDealState = false; 
            return false;
        }
        if ("03".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            buildError("dealData()", "团体申请下所有个人案件已经结案完毕，" +
                    "不能再录入个人客户!");
            mDealState = false; 
            return false;
        }
        if ("04".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
            buildError("dealData()", "团体申请下所有个人案件已经给付确认，" +
                    "不能再录入个人客户!");
            mDealState = false; 
            return false;
        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseDB.setRgtState("03");
        mLLCaseSet = tLLCaseDB.query();
        if(mLLCaseSet.size()<=0){
            buildError("simpleClaimAudit()", "没有待理算的案件!");
            mDealState = false; 
            return false;
        }
        
        //2.理算
        for(int i=1;i<=mLLCaseSet.size();i++){
            if(!calClaim(mLLCaseSet.get(i).getCaseNo())){
                continue;
            }else{
                LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
                mLLCaseOpTimeSchema.setCaseNo(mLLCaseSet.get(i).getCaseNo());
                mLLCaseOpTimeSchema.setRgtState("04");
                mLLCaseOpTimeSchema.setOperator(tG.Operator);
                mLLCaseOpTimeSchema.setManageCom(tG.ManageCom);
                LLCaseCommon tLLCaseCommon = new LLCaseCommon();
                try {
                    LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.
                            CalTimeSpan(mLLCaseOpTimeSchema);
                    tmap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
                } catch (Exception ex) {
                    System.out.println("没有时效记录");
                }
            }
        }
        
        if (!uwCheck()) {
            return false;
        }
        
        tdata.add(tmap);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(tdata, null)) {
            buildError("dealData()", "数据库保存失败");
            mDealState = false; 
            return false;
        }
        
		return true;
	}

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
	private boolean calClaim(String caseNo) {

        ClaimCalBL aClaimCalAutoBL = new ClaimCalBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(caseNo);
        VData aVData = new VData();
        aVData.addElement(tG);
        aVData.addElement(tLLCaseSchema);
        if(!aClaimCalAutoBL.submitData(aVData, "autoCal")){
             buildError("calClaim()", "理算失败");
             mDealState = false; 
             return false;
        }
        return true;
    
	}
	
    /**
     * 调用核赔规则
     * @return boolean
     */
	private boolean uwCheck() {

        String tsql="";
        int tErrNO=0;
        String tErr=null;
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = new SSRS();
        System.out.println(this.mLLRegisterSchema.getRgtNo());
        if(this.mLLRegisterSchema.getRgtNo()!=null&&!("".equals(this.mLLRegisterSchema.getRgtNo()))){
	        tsql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'  with ur";
	        System.out.println("BatchClaimCalBL-中通过批量号获取客户号："+tsql);
	        ssrs = exesql.execSQL(tsql);
        }
               
        System.out.println(ssrs.getMaxRow());
        for(int i=1;i<=ssrs.getMaxRow();i++){
      	  VData tVData = new VData();
      	  LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
      	  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
      	  LLCaseSet tLLCaseSet=new LLCaseSet();
      	  LLCaseRelaSet tLLCaseRelaSet=new LLCaseRelaSet();
      	  LLSubReportSet tLLSubReportSet=new LLSubReportSet();
      	  LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
      	  LLCaseDB tLLCaseDB=new LLCaseDB();
      	  tLLCaseDB.setCustomerNo(ssrs.GetText(i, 1));
      	  tLLCaseDB.setRgtState("03");
      	  tLLCaseDB.setRgtNo(this.mLLRegisterSchema.getRgtNo());
      	  tLLCaseSet=tLLCaseDB.query();
      	  String casenos="";
      	  if(tLLCaseSet!=null&&tLLCaseSet.size()>0)
      	  {
      		  for(int a=1;a<=tLLCaseSet.size();a++){
	      		  System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息Start！");
	      		  casenos=casenos+","+tLLCaseSet.get(a).getCaseNo();
	      		  LLClaimDetailDB tLLClaimDetailDB=new LLClaimDetailDB();
	      		  tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
	      		  tLLClaimDetailSet.add(tLLClaimDetailDB.query());
	      		  LLCaseRelaDB tLLCaseRelaDB=new LLCaseRelaDB();
	      		  tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
	      		  tLLCaseRelaSet.add(tLLCaseRelaDB.query());
	      		  System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息END！");
      		  }
      		  if(tLLCaseRelaSet!=null&&tLLCaseRelaSet.size()>0){
	      		  for(int b=1;b<=tLLCaseRelaSet.size();b++){
	      			  LLSubReportDB tLLSubReportDB=new LLSubReportDB();
	      			  tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(b).getSubRptNo());
	      			  tLLSubReportSet.add(tLLSubReportDB.query());
	      		  }
	      		  System.out.println("BatchClaimCalBL-中获取理赔LLSubReport表信息END！");
      		  }
      	  }
        
      	  tVData.addElement(tLLCaseSchema);
      	  tVData.addElement(tLLCaseRelaSet);
      	  tVData.addElement(tLLSubReportSet);
      	  tVData.addElement(tLLClaimDetailSet);
      	  tVData.addElement(tG);
          if (!tLLUWCheckBL.submitData(tVData,"")) {
          	  tErrNO=tErrNO+1;
          	  if(tErr==null){
          		  tErr=String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";  
          	  }else{ 
          		  tErr=tErr+String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";
               // CError.buildErr(this,"客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+")");
          	  }
            }          
         }
         if(tErr!=null){
        	 CError.buildErr(this,tErr);
        	 return false;
         }
         return true;
    
	}

	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState || "".equals(mRgtNo)) {
            return createFalseXML();
        } else {
        	try
        	{
        		return createResultXML(mRgtNo);
        	}
        	catch (Exception e)
        	{          
        		e.printStackTrace();
        		buildError("createXML()", "生成返回报文错误!");
        		return createFalseXML();
        	}
        }
    }  
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        Element tBodyData = new Element("BODY");
        
        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBodyData.addContent(tResponseCode);

        mErrorMessage = mErrors.getFirstError();
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aRgtNo) throws Exception {
         System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BACK_DATA");
         tBodyData.addContent(tBackData);
         
         //Back_Data部分
         Element tRgtNO = new Element("RGTNO");
                 
         tRgtNO.setText(aRgtNo);
         tBackData.addContent(tRgtNO);
         
         
         //返回的案件处理结构
         Element tLLCaseList = new Element("LLCASELIST");
         
         System.out.println("开始返回报文生成--list的长度"+tList.size());
         if(tList!=null && tList.size()>0){
             
             for(int i=0;i< tList.size();i++){
                 System.out.println("开始返回报文生成:"+i);
                 String[] tresult=tList.get(i).toString().split(",");
                 
                 System.out.println(tresult[0]+"==="+tresult[1]);
                 Element tLLCaseData = new Element("LLCASE_DATA");
                 Element tCASENUM = new Element("CASENUM");
                 tCASENUM.setText(tresult[0]);
                 Element tCASENO = new Element("CASENO");
                 
                 Element tClaimno = new Element("CLAIMNO");
                 
                 tCASENO.setText(tresult[1]);
                 tClaimno.setText(tresult[2]);
                 tLLCaseData.addContent(tCASENUM);
                 tLLCaseData.addContent(tCASENO);
                 tLLCaseData.addContent(tClaimno);
                 tLLCaseList.addContent(tLLCaseData);
             }
         }
         tBodyData.addContent(tLLCaseList);
         tRootData.addContent(tBodyData);
         
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    /**
     * 给团体保单做撤件
     * @return
     */
        private boolean CancelRgt()
        {
        	if(tCancelFlag){           	
                LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                System.out.println("立案号码是" + mRgtNo);
                tLLRegisterDB.setRgtNo(mRgtNo);
                if(tLLRegisterDB.getInfo()){
//                    if (!(tLLRegisterDB.getDeclineFlag() == null ||
//                          tLLRegisterDB.getDeclineFlag().equals(""))){
//                        if (tLLRegisterDB.getDeclineFlag().equals("1")){
//                            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
//                            CError tError = new CError();
//                            tError.moduleName = "LLRegisterBL";
//                            tError.functionName = "CancelRgt";
//                            tError.errorMessage = "该批次已经进行了撤销，不能对此进行撤销！";
//                            this.mErrors.addOneError(tError);
//                            return false;
//                        }
//
//                    }
                    boolean flag = false;
                    String scase = "select 1 from llcase where rgtno='"+tLLRegisterDB.getRgtNo()+"' and rgtstate<>'14' fetch first rows only with ur";
                    ExeSQL tExe = new ExeSQL();
                    String result  =  tExe.getOneValue(scase);
                    if("1".equals(result)){
                    	flag = true;
                    }
                    System.out.println("tLLRegisterDB.getRgtState()  && !flag" + !(tLLRegisterDB.getRgtState().equals("03") && !flag));
                     
                     
                    if (!tLLRegisterDB.getRgtState().equals("01") && 
                    		!tLLRegisterDB.getRgtState().equals("02") && 
                    		!(tLLRegisterDB.getRgtState().equals("03") && !flag) && 
                    		!"07".equals(tLLRegisterDB.getRgtState())){
                    	    this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "LLRegisterBL";
                            tError.functionName = "CancelRgt";
                            tError.errorMessage = "该批次下所有个人结案完毕，不能对此进行撤销！";
                            this.mErrors.addOneError(tError);
                            return false;
                    }
                    
                    if (!tLLRegisterDB.getHandler1().equals(tG.Operator)
                        && !LLCaseCommon.checkUPUpUser(tG.Operator,tLLRegisterDB.getHandler1())){
                        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LLRegisterBL";
                        tError.functionName = "CancelRgt";
                        tError.errorMessage = "您不是该批次处理人，不能撤销该批次！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    
                }else{
                    this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLRegisterBL";
                    tError.functionName = "CancelRgt";
                    tError.errorMessage = "批次查询失败！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

         
                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                String tCaseSQL = "SELECT * FROM llcase WHERE rgtno='"+mRgtNo+"' AND rgtstate IN ('05','06','09','10','11','12')";
                tSSRS = tExeSQL.execSQL(tCaseSQL);
                if (tSSRS.getMaxRow() > 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "LLRegisterBL";
                    tError.functionName = "updateData";
                    tError.errorMessage = "该批次下案件状态不允许批次撤件！";
                    System.out.println("不能进行撤销数据");
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                
                
                //删除已产生的赔案信息
                String sql0 = "delete from LLClaim where rgtno='"+mRgtNo+"'";
                String sql_p = "delete from LLClaimPolicy where rgtno='"+mRgtNo+"'";
                String sql_d = "delete from LLClaimDetail where rgtno='"+mRgtNo+"'";
                String sql_acc = "delete from LCInsureAccTrace where OtherNo in (select caseno from llcase where rgtno='"+mRgtNo+"')";
                //删除赔案信息增加ljsget,ljsgetclaim的数据	#2368 cbs00071160关于撤件案件造成的宽限期过后保单状态的问题
                String sql_ljsget_p = "delete from ljsget where otherno='"+mRgtNo+"'";
                String sql_ljsget_c = "delete from ljsget where otherno in (select caseno from llcase where rgtno='"+mRgtNo+"') ";
                String sql_ljsgetclaim_p = "delete from ljsgetclaim where otherno='"+mRgtNo+"'";
                String sql_ljsgetclaim_c = "delete from ljsgetclaim where otherno in (select caseno from llcase where rgtno='"+mRgtNo+"') ";

                
                //删除审批相关的信息
                String sql_uwm = "delete from LLClaimUWmain where rgtno='"+mRgtNo+"'";
                String sql_ud = "delete from LLClaimUWDetail where caseno in (select caseno from llcase where rgtno='"+mRgtNo+"')";
                String sql_uw = "delete from LLClaimUnderwrite where rgtno='"+mRgtNo+"'";
                
                tLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
                //tLLRegisterSchema.setDeclineFlag("1"); //撤销标准
                tLLRegisterSchema.setRgtReason("外包项目批次导入错误");
                tLLRegisterSchema.setCanceler(tG.Operator);
                tLLRegisterSchema.setCancelReason("");
                tLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
                tLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());

                
                MMap map = new MMap();
                /*
                 * 判断是否是天津社保案件，TRUE 更改状态  false 不做处理
                 * */
                ExeSQL tExe = new ExeSQL();
                String querySQL = "select RgtType from llregister where rgtno='"+mRgtNo+"'";
                
                String rgtType_result  =  tExe.getOneValue(querySQL);
                if("8".equals(rgtType_result)){
                	String squerySQL = "select caseno from llcase where rgtno='"+mRgtNo+"'";
                	SSRS rgtType_resultt  =  tExe.execSQL(squerySQL);//下标从1开始
                	if(rgtType_resultt!=null&&rgtType_resultt.MaxRow>0){
                		for(int i=1;i<=rgtType_resultt.MaxRow;i++){
                			String ConfirmState_update = "update LLHospCase set ConfirmState = '2' where caseno = '"+rgtType_resultt.GetText(i, 1)+"'";
                			map.put(ConfirmState_update,"UPDATE");
                		}
                	}
                }
                
                map.put(tLLRegisterSchema, "UPDATE");
                map.put(sql0, "DELETE");
                map.put(sql_p, "DELETE");
                map.put(sql_d, "DELETE");
                map.put(sql_acc, "DELETE");
                map.put(sql_uwm, "DELETE");
                map.put(sql_ud, "DELETE");
                map.put(sql_uw, "DELETE");
                map.put(sql_ljsget_p, "DELETE");
                map.put(sql_ljsget_c, "DELETE");
                map.put(sql_ljsgetclaim_p, "DELETE");
                map.put(sql_ljsgetclaim_c, "DELETE");

                LLCaseDB tLLCaseDB = new LLCaseDB();
                LLCaseSet tLLCaseSet = new LLCaseSet();
                LLCaseSet saveLLCaseSet = new LLCaseSet();
                tLLCaseDB.setRgtNo(mRgtNo);
                tLLCaseSet = tLLCaseDB.query();
                if(tLLCaseSet.size()>0){
                    for (int i=1 ; i<=tLLCaseSet.size();i++){
                        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                        tLLCaseSchema = tLLCaseSet.get(i);
                        tLLCaseSchema.setCancleReason("");
                        tLLCaseSchema.setCancleRemark("团体批次被撤销");
                        tLLCaseSchema.setRgtState("14");
                        tLLCaseSchema.setHandler(tG.Operator);
                        tLLCaseSchema.setCancleDate(PubFun.getCurrentDate());
                        tLLCaseSchema.setCancler(tG.Operator);
                        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
                        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
                        saveLLCaseSet.add(tLLCaseSchema);
                    }
                    map.put(saveLLCaseSet,"UPDATE");
               }


                    this.mResult.clear();
                    this.mResult.add(map);
                PubSubmit ps = new PubSubmit();
                if (!ps.submitData(this.mResult, ""))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(ps.mErrors);
                    return false;
                }else{
                	tCancelFlag = false;
                }
                return true;           
        	}else{
        		return true;  
        	}
        }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJSocialSecurityRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
	// 生成流水号，包含错误处理
	private String getMaxNo(String cNoType) {
		String strNo = PubFun1.CreateMaxNo(cNoType, 1);

		if (strNo.equals("") || strNo.equals("0")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UploadPrepareBL";
			tError.functionName = "getReturnData";
			tError.errorNo = "-90";
			tError.errorMessage = "生成流水号失败!";
			this.mErrors.addOneError(tError);
			strNo = "";
		}
		return strNo;
	}
    
    public static void main(String[] args) {
    	Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("C:\\Users\\admin\\Desktop\\新建文件夹 (2)\\新建文件夹\\分案件信息导入\\LCSQ_018\\LCSQ_018.xml"), "GBK");
            LLWBCaseRegister tBusinessDeal = new LLWBCaseRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
