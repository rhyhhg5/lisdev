package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLTJJHCaseTransFtpXml
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();

    public LLTJJHCaseTransFtpXml()
    {
    }

    public boolean submitData()
    {
        if (getSubmitMap() == null)
        {
            return false;
        }

     

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;

    }

    public MMap getSubmitMap()
    {

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
   

        return true;
    }

    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }


    private boolean getXls()
    {
        String tUIRoot = CommonBL.getUIRoot();

        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        int xmlCount = 0;

        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype='TJJHClaim' and code='IP/Port'");
        String tPort = tExeSql
                .getOneValue("select codealias from ldcode where codetype='TJJHClaim' and code='IP/Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype='TJJHClaim' and code='User/Pass'");
        String tPassword = tExeSql
                .getOneValue("select codealias from ldcode where codetype='TJJHClaim' and code='User/Pass'");

        String tFileRgtkPath = tExeSql
                .getOneValue("select codename from ldcode where codetype='TJJHClaim' and code='XmlPath'");
        
        String tFileCasePath = tExeSql
        		.getOneValue("select codealias from ldcode where codetype='TJJHClaim' and code='XmlPath'");
        
        tFileRgtkPath = tUIRoot + tFileRgtkPath;
        tFileCasePath = tUIRoot + tFileCasePath;
       
   

        try
        {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "LLTJJHCaseTransFtpXml";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
           

            String tFilePathLocalCore = tFileCasePath+PubFun.getCurrentDate()+"/";//服务器存放文件的路径 每天生成一个文件夹
//            tFilePathLocalCore = "D:\\Java\\test\\TJJH2\\";//本地测试使用
            String tFileImportPath = "/01PH/TJJH/8612/TJJHClaimCase";//ftp上存放文件的目录
            String tFileImportBackPath = "/01PH/TJJH/8612/TJJHClaimCaseBack";//ftp上返回文件目录
            String tFileImportPathBackUp = "/01PH/TJJH/8612/TJJHClaimCaseBackUp/"+PubFun.getCurrentDate()+"/";//FTP备份目录
            String tFilePathout = tFilePathLocalCore+"Back/";
            
            String tDate = PubFun.getCurrentDate();
            tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                  + tDate.substring(8, 10);
            String tComtop="TJJH1"+tDate;
            String ResultMax = tFilePathout+"upload/" +(tComtop + PubFun1.CreateMaxNo(tComtop, 5))+"/";

            File mFileDir = new File(tFilePathLocalCore);
    		if (!mFileDir.exists()) {
    			if (!mFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
    			}
    		}
    		
            File tFileDir = new File(tFilePathout);
    		if (!tFileDir.exists()) {
    			if (!tFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
    			}
    		}
    		
    		File tResultMaxDir = new File(ResultMax);
            if(!tResultMaxDir.exists()){
            	if(!tResultMaxDir.mkdirs()){
            		System.err.println("创建目录[" + ResultMax.toString() + "]失败！"
    						+ tResultMaxDir.getPath());
            	}
            }
    		
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
            System.out.println("OK");

            
            	
            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                return false;
            }
            // 报文先做存储
            for(int i=0;i<tPath.length;i++){
         	   if(tPath[i]==null){
         		   continue;
         	   }
         	   tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[i]);
         	   tFTPTool.changeWorkingDirectory(tFileImportPath);
         	   tFTPTool.deleteFile(tPath[i]);
            }
            tFTPTool.logoutFTP();
            
            //报文进行解析
            for(int j=0;j<tPath.length;j++){
            	if(tPath[j]==null){
	        		   continue;
	        	   }
            	System.out.println("Hello !!");
            	xmlCount++;
	        	   FileInputStream fis = null;
	        	   FileOutputStream fos = null;
	        	   FileOutputStream fos2 = null;
	        	   try
	               {
	        		   VData tVData = new VData();
		        	   TransferData tTransferData = new TransferData(); 
		        	   tTransferData.setNameAndValue("FileName", tPath[j]);
		        	   tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);
		        	   
		        	   tVData.add(tTransferData);
		        	   tVData.add(mGI);
		        	   
		        	   Document tInXmlDoc ;
		        	   fis = new FileInputStream(tFilePathLocalCore+tPath[j]);
		        	   tInXmlDoc =JdomUtil.build(fis,"GBK");
		        	   LLTJJHCaseRegister tBusinessDeal = new LLTJJHCaseRegister();
	                   Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
	                   System.out.println("打印传入报文============");
	                   JdomUtil.print(tInXmlDoc.getRootElement());
	                   System.out.println("打印传出报文============");
	                   JdomUtil.print(tOutXmlDoc);
	                   //创建输出流进行写出报文
	                   
	                   fos = new FileOutputStream(tFilePathout+tPath[j]);
			           JdomUtil.output(tOutXmlDoc,fos);
			           
			           fos2 = new FileOutputStream(ResultMax+tPath[j]);
			           JdomUtil.output(tOutXmlDoc,fos2);
	               }catch(Exception e) {
	            	   System.out.println("静海分案导入失败: " + tPath[j]);
	                   mErrors.addOneError("静海分案导入失败" + tPath[j]);
	                   e.printStackTrace();
	               }finally {
	                	try {
	                		if(fis != null) {
	                			fis.close();
	                		}
	                	}finally {
	                		if(fos != null) {
	                			fos.close();
	                		}
	                		if(fos2 != null) {
	                			fos2.close();
	                		}
	                	}
	                }
            }
            //再次连接ftp进行上报
            try {
            	FTPTool tFTPTool2 = new FTPTool(tServerIP,tUsername,tPassword,Integer.parseInt(tPort),"aaActiveMode");
            	if(!tFTPTool2.loginFTP()){
	  				 CError tError = new CError();
	  				 tError.moduleName="LLTJJHCaseTransFtpXml";
	  				 tError.functionName="getXls";
	  				 tError.errorMessage=tFTPTool2.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
	  				 mErrors.addOneError(tError);
	  				 System.out.println(tError.errorMessage);	 
	  				 return false;
	  			 } 
            	System.out.println("====FTP天津静海信息，二次登录成功,准备上载返回报文===");
	            if(tFTPTool2.uploadAll(tFileImportBackPath, ResultMax)){
	            	System.out.println("报文上传失败");
	            }
	            tFTPTool2.logoutFTP();
            }catch(Exception e) {
            	e.printStackTrace();
				CError tError = new CError();
		        tError.moduleName = "LLTJJHCaseTransFtpXml";
		        tError.functionName = "getXls";
		        tError.errorMessage = "回传返回报文过程中出现异常";
		        mErrors.addOneError(tError);
		        System.out.println(tError.errorMessage);
		        return false;
            }

        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }


    public String getDealInfo()
    {
        return mDealInfo;
    }
    
//  建文件夹
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args)
    {
    	LLTJJHCaseTransFtpXml a =new LLTJJHCaseTransFtpXml();
    	a.getXls();
    }
}
