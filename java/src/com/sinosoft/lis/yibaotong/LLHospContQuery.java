package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

//test用
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLHospContQuery implements ServiceInterface {
    public LLHospContQuery() {
    }

    private VData mInputData = new VData();
    private MMap mMMap = new MMap();
    
    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();
    public ExeSQL tesql = new ExeSQL();
    public SSRS tSSRS = new SSRS();

    private Document mInXmlDoc; //传入报文
    private String mDocType = ""; //报文类型
    private boolean mDealState = true; //处理标志
    private String mResponseCode = "1"; //返回类型代码
    private String mRequestType = ""; //请求类型
    private String mErrorMessage = ""; //错误描述
    private String mTransactionNum = ""; //交互编码
    private String mTranHospCode = ""; //交互编码中医院代码
    private Element mBodyData; //报文Body部分

    //提取客户保单信息
    private String mPerson_Name = ""; //姓名
    private String mSex = ""; //性别
    private String mID_Type = ""; //证件类型
    private String mID_Number = ""; //证件号码
    private String mBirth_Date = ""; //出生日期
    private String mHospital_Number = ""; //人保医院编码
    private String mManageCom = ""; //机构编码


    public Document service(Document pInXmlDoc) {
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"02".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || ("").equals(mTransactionNum) ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        if (!mHospital_Number.equals(mTransactionNum.substring(2, 9))) {
            buildError("checkData()", "交互编码与人保医院编码不匹配");
            return false;
        }
        //校验获取报文的姓名、性别、证件类型、证件号码、出生日期、医院编码 信息非空
        if ("".equals(mPerson_Name) || mPerson_Name == null) {
            buildError("checkData()", "获取报文的姓名为空!");
            return false;
        }
        if ("".equals(mSex) || mSex == null) {
            buildError("checkData()", "获取报文的性别为空!");
            return false;
        }
        if (!mSex.equals("0") && !mSex.equals("1") && !mSex.equals("2")) {
            buildError("checkData()", "获取报文的性别代码错误!");
            return false;
        }
        if ("".equals(mID_Type) || mID_Type == null) {
            buildError("checkData()", "获取报文的证件类型为空!");
            return false;
        }
        if (!mID_Type.equals("0") && !mID_Type.equals("1") &&
            !mID_Type.equals("2") && !mID_Type.equals("4")) {
            buildError("checkData()", "获取报文的证件类型代码错误!");
            return false;
        }
        if ("".equals(mID_Number) || mID_Number == null) {
            buildError("checkData()", "获取报文的证件号码为空!");
            return false;
        }
        if ("".equals(mBirth_Date) || mBirth_Date == null) {
            buildError("checkData()", "获取报文的出生日期为空!");
            return false;
        }
        if (!PubFun.checkDateForm(mBirth_Date)) {
            buildError("checkData()", "【出生日期】格式错误");
            return false;
        }
        if ("".equals(mHospital_Number) || mHospital_Number == null) {
            buildError("checkData()", "获取报文的医院编码为空!");
            return false;
        }
        mTranHospCode = mTransactionNum.substring(2, 9);

        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        tLDHospitalDB.setHospitCode(mTranHospCode);
        if (!tLDHospitalDB.getInfo()) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }
        mManageCom = tLDHospitalDB.getManageCom();
        if ("".equals(mManageCom)) {
            buildError("checkData()", "医院所属机构错误");
            return false;
        }

        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        Element tBasepart = tBodyData.getChild("BASE_PART");

        mPerson_Name = tBasepart.getChildTextTrim("PERSON_NAME");
        mSex = tBasepart.getChildTextTrim("SEX");
        mID_Type = tBasepart.getChildTextTrim("ID_TYPE");
        mID_Number = tBasepart.getChildTextTrim("ID_NUMBER");
        mBirth_Date = tBasepart.getChildTextTrim("BIRTH_DATE");
        mHospital_Number = tBasepart.getChildTextTrim("HOSPITAL_NUMBER");
        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {

        //根据客户的五要素查询保单信息
        String sqlStr =
                "select b.ManageCom,b.grpcontno,b.contno,b.InsuredNo,b.SignDate,b.cvalidate, "
                + " b.enddate,b.prem,b.AgentCode,(select Name from laagent where AgentCode=b.AgentCode),"
                + " b.AppntName,(select Name from lcbnf where PolNo=b.polno order by BnfLot desc fetch first 1 rows only ),"
                +
                " b.riskcode,(select riskname from lmrisk where riskcode=b.riskcode),b.conttype,"                              
                + " (case when (select double(calfactorvalue) from lccontplandutyparam where calfactor = 'ShareAmnt' "
                + " and calfactorvalue is not null and grppolno = b.grppolno and contplancode = b.contplancode "
                + " and grpcontno = b.grpcontno fetch first 1 rows only) is null "
                + " and b.amnt >0 then b.amnt-nvl((select sum(realpay) from llcase m,llclaimdetail n "
                + " where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.polno=b.polno),0) else -1 end), "
                + " b.polno,b.ContPlanCode "
                + " from lcinsured a,lcpol b "
                +
                " where 1=1 and a.contno=b.contno and a.InsuredNo=b.InsuredNo"
                + " and b.appflag='1' and b.Stateflag='1'"
                + " and exists (select 1 from LLHospComRight where riskcode=b.riskcode and requesttype='02' and hospitcode='" +
                mTranHospCode + "' )"
                + " and a.Name='" + mPerson_Name + "' and a.Sex='" + mSex +
                "' and a.Birthday='" + mBirth_Date + "'"
                + " and a.IDType='" + mID_Type + "' and a.IDNo='" + mID_Number +
                "' and b.ManageCom like'" + mManageCom + "%'"
                + " union all "
                + " select b.ManageCom,b.grpcontno,b.contno,b.InsuredNo,b.SignDate,b.cvalidate, "
                + " b.enddate,b.prem,b.AgentCode,(select Name from laagent where AgentCode=b.AgentCode),"
                + " b.AppntName,(select Name from lcbnf where PolNo=b.polno order by BnfLot desc fetch first 1 rows only ),"
                +
                " b.riskcode,(select riskname from lmrisk where riskcode=b.riskcode),b.conttype, "
                + " (case when (select double(calfactorvalue) from lccontplandutyparam where calfactor = 'ShareAmnt' "
                + " and calfactorvalue is not null and grppolno = b.grppolno and contplancode = b.contplancode "
                + " and grpcontno = b.grpcontno fetch first 1 rows only) is null "
                + " and b.amnt >0 then b.amnt-nvl((select sum(realpay) from llcase m,llclaimdetail n "
                + " where m.caseno=n.caseno and m.rgtstate in ('09','11','12') and n.polno=b.polno),0) else -1 end), "
                + " b.polno,b.ContPlanCode "
                + " from lbinsured a,lbpol b,lcrnewstatelog c "
                +
                " where 1=1 and a.contno=b.contno and a.InsuredNo=b.InsuredNo"
                + " and b.appflag='1' and b.Stateflag='3' and c.newpolno=b.polno and c.state='6'"
                + " and exists (select 1 from LLHospComRight where riskcode=b.riskcode and requesttype='02' and hospitcode='" +
                mTranHospCode + "' )"
                + " and a.Name='" + mPerson_Name + "' and a.Sex='" + mSex +
                "' and a.Birthday='" + mBirth_Date + "'"
                + " and a.IDType='" + mID_Type + "' and a.IDNo='" + mID_Number +
                "' and b.ManageCom like'" + mManageCom + "%'"
                ;
        System.out.println(sqlStr);
        tSSRS = tesql.execSQL(sqlStr);

        if (tSSRS.getMaxRow() <= 0) {
            buildError("dealData()", "查询客户保单信息失败!");
            return false;
        }  
        System.out.println("保单客户数:"+tSSRS.getMaxRow());
//        #2582 存储所有查询出来的客户，支持理算时的校验  by-lyc
        for(int i=1; i<= tSSRS.getMaxRow(); i++){
        String mCustomerNo = tSSRS.GetText(i, 4);
        
        String mLimit = PubFun.getNoLimit(mManageCom);
        String mSerialNo = PubFun1.CreateMaxNo("HOSP", mLimit);
        LLHospQueryTraceSet	tLLHospQueryTraceSet = new LLHospQueryTraceSet();	
        LLHospQueryTraceSchema tLLHospQueryTraceSchema = new LLHospQueryTraceSchema();
        tLLHospQueryTraceSchema.setSerialNo(mSerialNo);
        tLLHospQueryTraceSchema.setCustomerNo(mCustomerNo);
        tLLHospQueryTraceSchema.setQueryDate(PubFun.getCurrentDate());
        tLLHospQueryTraceSchema.setQueryTime(PubFun.getCurrentTime());        
        tLLHospQueryTraceSchema.setAppTranNo(mTransactionNum);
        tLLHospQueryTraceSchema.setAppTranType(mRequestType);
        tLLHospQueryTraceSchema.setDealer("SCYB");
        tLLHospQueryTraceSchema.setCaseType("00");
        tLLHospQueryTraceSchema.setDealType("0");
        tLLHospQueryTraceSchema.setOtherNo(mCustomerNo);
        tLLHospQueryTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLLHospQueryTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLLHospQueryTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLLHospQueryTraceSchema.setModityTime(PubFun.getCurrentTime());
        tLLHospQueryTraceSchema.setManageCom(mManageCom);
        tLLHospQueryTraceSchema.setOperator("SCYB");
        tLLHospQueryTraceSet.add(tLLHospQueryTraceSchema);
        mMMap.put(tLLHospQueryTraceSet, "INSERT");
        }
        try {
            this.mInputData.clear();
            this.mInputData.add(mMMap);         
        } catch (Exception ex) {
            // @@错误处理
            buildError("dealCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("dealCase", "数据提交失败");
            return false;
        }
        return true;
    }


    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
            return createResultXML();
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText("02");
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "1";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        
        Element tBody = new Element("BODY");
        Element tListData = new Element("INSURANCE_POLICY_LIST");

        for (int m = 1; m <= tSSRS.getMaxRow(); m++) {

            String tManageCom = tSSRS.GetText(m, 1);
            
            String tContNo = tSSRS.GetText(m, 3);
            String tInsuredNo = tSSRS.GetText(m, 4);
            String tSignDate = tSSRS.GetText(m, 5);
            String tCValiDate = tSSRS.GetText(m, 6);
            String tEndDate = tSSRS.GetText(m, 7);
            String tPrem = tSSRS.GetText(m, 8);
            String tAgentCode = tSSRS.GetText(m, 9);
            String tAgentName = tSSRS.GetText(m, 10);
            String tAppntName = tSSRS.GetText(m, 11);
            String tBnfPerson = tSSRS.GetText(m, 12);
            String tCreate_Time = tSSRS.GetText(m, 5);
            String cState = "1";
            String cLook_Bound = "0";
            String cOutpation_Look_Extend = "0";
            String cInpation_Look_Extend = "0";
            String cBill_Type = tSSRS.GetText(m, 15);
            String cSurpAmnt = null;
            if(null != tSSRS.GetText(m, 16) && !"".equals(tSSRS.GetText(m, 16)) 
            	&& Double.parseDouble(tSSRS.GetText(m, 16))>= 0)                  
            {
            	 cSurpAmnt = tSSRS.GetText(m, 16) ;   
            }    
            String cPolNo = tSSRS.GetText(m, 17);
            String cContPlanCode = tSSRS.GetText(m, 18);
            String tGrpContNo = tSSRS.GetText(m, 2);
            if ("1".equals(cBill_Type)) {
            	tGrpContNo = tSSRS.GetText(m, 3);
            }
            
            String cRemark = "";
            String tRiskCode = tSSRS.GetText(m, 13);
            String tRiskName = tSSRS.GetText(m, 14);

            Element aManageCom = new Element("MANAGEMENT_ORGANIZATION_CODE");
            aManageCom.setText(tManageCom);

            Element aGrpContNo = new Element("INSURANCE_POLICY_NUMBER");
            aGrpContNo.setText(tGrpContNo);

            Element aContNo = new Element("SUB_INSURANCE_POLICY_NUMBER");
            aContNo.setText(tContNo);

            Element aRiskCode = new Element("RISK_CODE");
            aRiskCode.setText(tRiskCode);

            Element aRiskName = new Element("RISK_NAME");
            aRiskName.setText(tRiskName);

            Element aInsuredNo = new Element("INSUREDNO");
            aInsuredNo.setText(tInsuredNo);

            Element aSignDate = new Element("PAYMENT_DATE");
            aSignDate.setText(tSignDate);

            Element aCValiDate = new Element("INSURE_DATE");
            aCValiDate.setText(tCValiDate);

            Element aEndDate = new Element("AVILD_DATE");
            aEndDate.setText(tEndDate);

            Element aPrem = new Element("PAY_MONEY");
            aPrem.setText(tPrem);

            Element aAgentCode = new Element("EMPLOYEE_NUMBER");
            aAgentCode.setText(tAgentCode);

            Element aAgentName = new Element("EMPLOYEE_NAME");
            aAgentName.setText(tAgentName);

            Element aAppntName = new Element("PAY_PERSON");
            aAppntName.setText(tAppntName);

            Element aBnfPerson = new Element("BENIFT_PERSON");
            aBnfPerson.setText(tBnfPerson);

            Element aState = new Element("BILL_STATE");
            aState.setText(cState);

            Element aLook_Bound = new Element("LOOK_BOUND");
            aLook_Bound.setText(cLook_Bound);

            Element aOutpation_Look_Extend = new Element(
                    "OUTPATION_LOOK_EXTEND");
            aOutpation_Look_Extend.setText(cOutpation_Look_Extend);

            Element aInpation_Look_Extend = new Element("INPATION_LOOK_EXTEND");
            aInpation_Look_Extend.setText(cInpation_Look_Extend);

            Element aCreate_Time = new Element("CREATE_TIME");
            aCreate_Time.setText(tCreate_Time);

            Element aBill_Type = new Element("BILL_TYPE");
            aBill_Type.setText(cBill_Type);

            Element aRemark = new Element("REMARK");
            aRemark.setText(cRemark);
            
            Element aSurpAmnt = new Element("RISK_AMNT");
            aSurpAmnt.setText(cSurpAmnt);
            
            Element aContPlanCode = new Element("CONT_PLAN");
            aContPlanCode.setText(cContPlanCode);

			String tDutySql = "select X.dutycode,X.getlimit - X.outdutyamnt,"
					+ " (case when X.shareamnt = 0 then (case when X.amnt = 0 then -1 else X.amnt - X.realpay end) "
					+ " when X.shareamnt > 0 then(case when X.amnt = 0 then X.shareamnt - X.sharerealpay else "
					+ " (case when X.shareamnt - X.sharerealpay > X.amnt - X.realpay then X.amnt - X.realpay else X.shareamnt - X.sharerealpay end) "
					+ " end) else(case when X.amnt = 0 then (case when X.riskamnt = 0 then -1 else X.riskamnt - X.riskrealpay "
					+ " end) else(case when X.riskamnt-X.riskrealpay > X.amnt-X.realpay then X.amnt-X.realpay else X.riskamnt-X.riskrealpay end)"
					+ " end) end) "
					+ " from (select a.dutycode dutycode,nvl(a.getlimit, 0) getlimit,a.amnt amnt,d.amnt riskamnt, "
					+ " (select double(calfactorvalue) from lccontplandutyparam where calfactor = 'ShareAmnt' and calfactorvalue is not null "
					+ " and grppolno = d.grppolno and dutycode = a.dutycode and contplancode = d.contplancode and grpcontno = d.grpcontno) shareamnt, "
					+ " nvl((select sum(OutDutyAmnt) from llcase b, llclaimdetail c where b.caseno = c.caseno and b.rgtstate in ('09', '11', '12') "
					+ " and c.polno = a.polno and c.dutycode = a.dutycode), 0) outdutyamnt, "
					+ " nvl((select sum(c.realpay) from llcase b, llclaimdetail c where b.caseno = c.caseno and b.rgtstate in ('09', '11', '12') "
					+ " and c.polno = a.polno and c.dutycode = a.dutycode),0) realpay, "
					+ " nvl((select sum(c.realpay) from llcase b, llclaimdetail c, lccontplandutyparam e where b.caseno = c.caseno "
					+ " and b.rgtstate in ('09', '11', '12') and e.grpcontno = d.grpcontno and e.contplancode = d.contplancode "
					+ " and e.dutycode = c.dutycode and e.calfactor = 'ShareAmnt' and e.calfactorvalue is not null and c.polno = d.polno),0) sharerealpay, "
					+ " nvl((select sum(c.realpay) from llcase b, llclaimdetail c where b.caseno = c.caseno and b.rgtstate in ('09', '11', '12') "
					+ " and c.polno = a.polno),0) riskrealpay "
					+ " from lcduty a, lcpol d where a.polno = d.polno and d.polno = '"
					+ cPolNo + "') AS X ";
            System.out.println(tDutySql);
            ExeSQL aesql = new ExeSQL();
            SSRS aSSRS = new SSRS();
            aSSRS = aesql.execSQL(tDutySql);
            Element tDutyList = new Element("DUTYLIST");
            if(aSSRS.getMaxRow()>=1 )
            {
            	for( int n = 1; n <= aSSRS.getMaxRow() ; n ++)
            	{
            		Element tDutyData = new Element("DUTYDATA");
            		
            		String tDudyCode = aSSRS.GetText(n, 1);
            		Element aDudyCode = new Element("DUTYCODE");
            		aDudyCode.setText(tDudyCode);
                     
            		String tSurpOutAmnt = "";
            		Element aSurpOutAmnt = new Element("GETLIMIT");
            		if(Double.parseDouble(aSSRS.GetText(n, 2))>=0 )
            		{         
            			tSurpOutAmnt = aSSRS.GetText(n, 2);
                		aSurpOutAmnt.setText(tSurpOutAmnt);
            		}
            		    
            		String tSurpDudtyAmnt ="";
            		Element aSurpDudtyAmnt = new Element("AMNT");
            		if(Double.parseDouble(aSSRS.GetText(n, 3))>=0 )
            		{         
            			tSurpDudtyAmnt = aSSRS.GetText(n, 3);
                        aSurpDudtyAmnt.setText(tSurpDudtyAmnt);
            		}
                                                            
                    tDutyData.addContent(aDudyCode);    
                    tDutyData.addContent(aSurpOutAmnt);  
                    tDutyData.addContent(aSurpDudtyAmnt);  
                    
                    tDutyList.addContent(tDutyData);
            	}
            }
            
            Element tContInfo = new Element("INSURANCE_POLICY_DATA");
            tContInfo.addContent(aManageCom);
            tContInfo.addContent(aGrpContNo);
            tContInfo.addContent(aContNo);
            tContInfo.addContent(aRiskCode);
            tContInfo.addContent(aRiskName);
            tContInfo.addContent(aInsuredNo);
            tContInfo.addContent(aSignDate);
            tContInfo.addContent(aCValiDate);
            tContInfo.addContent(aEndDate);
            tContInfo.addContent(aPrem);
            tContInfo.addContent(aAgentCode);
            tContInfo.addContent(aAgentName);
            tContInfo.addContent(aAppntName);
            tContInfo.addContent(aBnfPerson);
            tContInfo.addContent(aCreate_Time);
            tContInfo.addContent(aState);
            tContInfo.addContent(aLook_Bound);
            tContInfo.addContent(aOutpation_Look_Extend);
            tContInfo.addContent(aInpation_Look_Extend);
            tContInfo.addContent(aBill_Type);
            tContInfo.addContent(aRemark);
            tContInfo.addContent(aSurpAmnt);
            tContInfo.addContent(aContPlanCode);
            tContInfo.addContent(tDutyList);
            
            tListData.addContent(tContInfo);                       
            
        }

        tBody.addContent(tListData);        
        tRootData.addContent(tBody);
        
        Document tDocument = new Document(tRootData);

        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospContQuery";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args) { 
//    	String mManageCom = "86110000";
//    	String mLimit = PubFun.getNoLimit(mManageCom);
//        String mSerialNo = PubFun1.CreateMaxNo("HOSP", mLimit);
//          System.out.println(mSerialNo);
//
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:/LLHosp02.xml"), "GBK");
            JdomUtil.print(tInXmlDoc);
            LLHospContQuery tBusinessDeal = new LLHospContQuery();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
