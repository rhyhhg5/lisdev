package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.llcase.ClientRegisterBL;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseAmountSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLCaseAmountSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 团体管理式补充医疗保险(A款)报案处理-申请，分案</p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLMARgtRegister implements ServiceInterface {
	
	public void LLMARgtRegister(){	
	}
	
	  public CErrors mErrors = new CErrors();
	  public VData mResult = new VData();
	  private MMap mMMap = new MMap();
	  private GlobalInput mG = new GlobalInput();
	  private List mList= new ArrayList();
	  private String mErrorMessage = "";	//错误描述    
	 
	  private Document mInXmlDoc;	 			//传入报文    
	  private String mDocType = "";			//报文类型    
	  private boolean mDealState = true;		//处理标志    
	  private String mResponseCode = "1";		//返回类型代码    
	  private String mRequestType = "";		//请求类型    
	  private String mTransactionNum = "";	//交互编码

	  private Element mUserData;			 //报文USER_DATA部分
	  private Element mBatchData;			//报文BATCH _DATA批次立案信息部分    
	  private Element mLLCaseList;			//报文LLCASELIST部分
	  private Element mCustomerData;        //报文CUSTOMER_DATA部分
	  private Element mPolicyList;			 //报文POLICYLIST部分
	  private Element mSubReportList;		 //报文SUBREPORTLIST部分
	  
	  /**
	   * 传入报文信息
	   */
	  private String mManageCom = "";		//机构编码   
	  private String mOperator  = "";			//操作员      且行且用
	 
	  private String mGrpContNo= ""; 				//团单号
	  private String mApPeoples="";					//申请人数
	  private String mTogetherFlag = "";			//给付方式
	  private String mCaseGetMode="";				//赔款领取方式
	  private String mBankCode="";					//银行编码
	  private String mBankName="";					//银行名称
	  private String mAccName="";					//账户名
	  private String mBankAccNo="";					//账号
	  
	  /**
	   * 生成批次必要信息
	   */
     private String mRgtNo = "" ;					//批次号
     private String mCustomerNo = "";				//团体客户号
     private String mGrpName = "";					//单位名称
     private String mRgtantPostCode = "";			//邮政编码，对应表中立案人/申请人邮政编码“申请人邮政编码”
     private String mRgtantAddress = "";			//联系地址，对应表中“申请人地址”
     private String mRgtantPhone = "";				//联系电话，对应表中是“申请人电话”
     private String mRgtantName = "";				//申请人姓名
     private String mIdNo="";						//申请人证件号
     private String mIdType="";						//申请人证件类型
     private String mIdStartDate ="";				//申请人证件生效日期
     private String mIdEndDate="";					//申请人证件失效日期
     
     /**
      * 生成个案必要信息
      */
     private String mContNo ="";					//分单号
     private String mRgtMobile = "";				//移动电话
     private String mInsuredNo ="";        		   //被保人客户号  
     private String mRiskCode ="";
     private String mGetDutyCode ="";
     private String mGetDutyKind ="";
     private String mCaseNo="";					  //案件号
     
     
     private int g;
     private int mCaseNum = 0;
     private ExeSQL mExeSQL = new ExeSQL();
     private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();		//申请信息
  //   private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();	//处理人信息
     private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();				//医保通案件处理表信息

//     private LLCaseRelaSet mLLCaseRelaSchema = new LLCaseRelaSet();				//事件关联
     private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();			//被保人信息
     private LLCaseAmountSet mLLCaseAmountSet = new LLCaseAmountSet();			//事件赔付信息
     
     
     public Document service(Document pInXmlDoc) {
    	 //接口处理开始,第一次写，请误模仿.....
    	 System.out.println("LLMARgtRegister-----------service");
    	 mInXmlDoc=pInXmlDoc;
    	 
    	 try{
    		//获取报文
    		 if(!getInPutData(mInXmlDoc)){
    			 mDealState=false;
    		 }else{
    			 if(!checkData()){
    				 mDealState=false;
    			 }else{
    				 if(!dealData()){
    					 mDealState=false; 
    					 cancelCase();
    				 }
    			 }
    		 }
    		 
    	 }catch(Exception ex){
    		 System.out.println(ex.getMessage());
             mDealState = false;
             mResponseCode = "E";
             buildError("service()","系统未知错误"); 
             cancelCase();
    	 }finally{
    		 mInXmlDoc =createXML();
    	 }
    	  
		return mInXmlDoc;
     }

     /**
      * 解析报文主要部分
      */
     private boolean getInPutData(Document cXmlDoc) throws Exception {
    	 System.out.println("开始解析报文----getInPutData");
    	 if(cXmlDoc==null){
    		 buildError("getInPutData()","未获取报文");
    		 return false;
    	 }
    	 Element tRootData=cXmlDoc.getRootElement();
    	 mDocType=tRootData.getAttributeValue("type");
    	 
    	 //获取Head部分
    	 Element tHeadData=tRootData.getChild("HEAD");
    	 mRequestType =tHeadData.getChildText("REQUEST_TYPE");
    	 mTransactionNum =tHeadData.getChildText("TRANSACTION_NUM");
    	 System.out.println("TransactionNum========"+mTransactionNum);
    	 
    	 //获取Body部分
    	 Element tBodyData =tRootData.getChild("BODY");
    	 mUserData = tBodyData.getChild("USER_DATA");
    	 mBatchData = tBodyData.getChild("BATCH_DATA");
    	 mLLCaseList = tBodyData.getChild("LLCASELIST");
    	 mCustomerData = mLLCaseList.getChild("CUSTOMER_DATA");
    	 	
    	 return true;
     }
     /**
      * 校验报文信息
      * 
      */
     private boolean checkData() throws Exception{
    	 System.out.println("LLMARgtRegister----checkData");
    	 if(!"REQUEST".equals(mDocType)){
    		 buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
             return false;
    	 }
    	 if(!"MA01".equals(mRequestType)){
    		 buildError("checkData()","【请求类型】的值不存在或匹配错误");
    	 }
    	 if(mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
    	            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误");
            return false;
    	 }
    	 if(!mRequestType.equals(mTransactionNum.substring(0, 4))){
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
    	 }
    	 
    	 return true;
     }
     
     /**
      * 处理报文
      * 
      */
     private boolean dealData() throws Exception{
    	 System.out.println("LLMARgtRegister----dealData");
    	 //校验用户机构
    	 if(!dealMngCom()){
    		 return false;
    	 }
    	 
    	 //生成批次信息
    	 if(!dealRegister()){
    		 return false;
    	 }
    	 //生成案件信息,到受理完成
    	 if(!dealCaseRegister()){
    		 return false;
    	 }else{
    		 if(!mDealState){
    			 return false;
    		 }else{
                 //保存信息到LLHospCase  和 LLCaseAmount
                 PubSubmit tPubSubmit = new PubSubmit();
                 VData mVData = new VData();
                 MMap tMMap= new MMap();
                 tMMap.put(mLLHospCaseSet, "INSERT");
                 tMMap.put(mLLCaseAmountSet, "INSERT");
                 mVData.add(tMMap);
                 if(!tPubSubmit.submitData(mVData, "")){
                 	 mDealState = false;
                 	 buildError("service()", "案件信息数据提交报错");
                 	 return false;
                 }
         	}
    	 }    	 
    	 
    	 return true ;
     }
     
     /**
      * 解析管理机构
      * 
      */
     private boolean dealMngCom() throws Exception{
    	 System.out.println("LLMARgtRegister---dealMngCom");
    	 
    	 mManageCom=mUserData.getChildText("MNGCOM");
    	
    	 String tDealerSql="select code from ldcode where codetype='MA01Type' and codename='"+mManageCom+"' with ur";					
    	 String tDealer=mExeSQL.getOneValue(tDealerSql);
    	 if(tDealer==null ||"".equals(tDealer)){
    		 buildError("dealMngCom", "该管理机构下没有案件处理人");
             return false;
    	 }

    	 LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB(); 
    	 tLLClaimUserDB.setUserCode(tDealer);
    	 tLLClaimUserDB.setManageCom(mManageCom);
    	 tLLClaimUserDB.setHandleFlag("1");
    	 tLLClaimUserDB.setClaimDeal("1");
    	 tLLClaimUserDB.setStateFlag("1");
    	 
    	 LLClaimUserSet tLLClaimUserSet =tLLClaimUserDB.query();
    	 if(tLLClaimUserSet.size() <=0){
    		 buildError("dealMngCom", "案件处理人查询失败或没有权限");
             return false;
    	 }
    	 
    	 mG.Operator=tDealer;
    	 mG.ManageCom=mManageCom;
    	 
    	 return true;
     }
     
     
     /**
      * 解析批次信息
      * 
      */
     private boolean dealRegister() throws Exception{
    	 System.out.println("LLMARgtRegister--deaRegister");  
    	 
    	 //校验交易号是否存在，存在则返回
    	 String existsSQL="select 1 from llhospcase where hospitcode='"+mRequestType+"' and "
    	 		+ "apptranno='"+mTransactionNum+"' and casetype='08' with ur";
    	 String aExists= mExeSQL.getOneValue(existsSQL);
    	 if("1".equals(aExists)){
    		 buildError("dealRegister","该批次交易号:"+mTransactionNum+"已存在");
    		 return false;
    	 }
    	 
    	 mGrpContNo =mBatchData.getChildText("GRPCONTNO");       //团单号
    	 mApPeoples =mBatchData.getChildText("APPEOPLES");		 //申请人数
    	 mTogetherFlag =mBatchData.getChildText("TOGETHERFLAG"); //给付方式
    	 mCaseGetMode =mBatchData.getChildText("CASEGETMODE");	 //赔款领取方式
    	 mBankCode =mBatchData.getChildText("BANKCODE");		 //银行编码
    	 mBankName =mBatchData.getChildText("BANKNAME");		 //银行名称
    	 mAccName =mBatchData.getChildText("ACCNAME");			 //账户名
    	 mBankAccNo =mBatchData.getChildText("BANKACCNO");		 //账号
    	 
    	 /*
    	  *简单校验
    	  */
    	 if(mGrpContNo==null ||"".equals(mGrpContNo) || mGrpContNo.equals("null")){
    		 buildError("dealRegister","【团单号码】的值不能为空");
    		 return false;
    	 }
    	 if (mApPeoples == null || "".equals(mApPeoples) || mApPeoples.equals("null")) {
            buildError("dealRegister", "【申请人数】的值不能为空");
            return false;
         }
    	 if (mTogetherFlag == null || "".equals(mTogetherFlag)|| mTogetherFlag.equals("null")) {
        	buildError("dealRegister","【团单给付方式】的值不能为空");
         }
    	 if (!mTogetherFlag.equals("1") && !mTogetherFlag.equals("2")  && !mTogetherFlag.equals("3")  && !mTogetherFlag.equals("4") ) {
        	buildError("dealRegister", "【团单给付方式】的代码不在指定代码范围内");
        	return false;
         }
    	 if (mCaseGetMode == null || "".equals(mCaseGetMode)|| mCaseGetMode.equals("null")) {
         	buildError("dealRegister", "【团单赔款领取方式】的值不能为空");
         	return false;
         }
         if (!mCaseGetMode.equals("1") &&!mCaseGetMode.equals("2") &&  !mCaseGetMode.equals("3") && !mCaseGetMode.equals("4") && !mCaseGetMode.equals("11")) {
         	buildError("dealRegister", "【团单赔款领取方式】的代码不在指定代码范围内");
         	return false;
         }
 
         if(!"1".equals(mCaseGetMode) && !"2".equals(mCaseGetMode) && !"1".equals(mTogetherFlag) &&!"2".equals(mTogetherFlag)){
        	  if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
                  buildError("dealRegister", "【团单银行编码】的值不能为空");
                  return false;
              }
              if (mBankName == null || "".equals(mBankName)|| mBankName.equals("null")) {
                  buildError("dealRegister", "【团单银行名称】的值不能为空");
                  return false;
              }
              if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
                  buildError("dealRegister", "【团单银行账户名】的值不能为空");
                  return false;
              }
              if (mBankAccNo == null || "".equals(mBankAccNo)|| mBankAccNo.equals("null")) {
                  buildError("dealRegister", "【团单银行账号】的值不能为空");
                  return false;
              }
              
              Pattern tPattern = Pattern.compile("(\\d)+");
      		  Matcher isNum = tPattern.matcher(mBankAccNo);
              if(!isNum.matches()){
            	  buildError("dealRegister","【团单银行账号】只能是数字组成");
            	  return false;
              }
         }
         
         /*
          * 生成批次信息
          */
    	 if(!RgtRegister()){
    		 return false;
    	 }else{
    		 mResult.clear();
    		 mResult.add(mMMap);
    		 try{
             	PubSubmit tPubSubmit = new PubSubmit();
                 if (!tPubSubmit.submitData(mResult, "")) {
                 	 mDealState = false;
                 	 buildError("service()", "团单批次数据提交报错");
                 	 return false;
                  }
         		} catch (Exception ex) {
                  mDealState = false;
                  mResponseCode = "E";
                  buildError("service()", "团单批次系统未知错误");
         	}
    	 }
         
    	 return true;
     }
     
     /**
      * 解析批次信息
      * 
      */
     private boolean RgtRegister() throws Exception{
    	 System.out.println("LLMARgtRegister----RgtRegister");
 
    	 String aAddressNo="";   //客户地址号码 

    	//用团单号校验该客户是否有正在处理的保全项目,返回的是工单号，而且校验的是全部保单，考虑以后是否按照保单校验
    	 String result =LLCaseCommon.checkGrp(mGrpContNo);
    	 if (!"".equals(result)) {
             buildError("RgtRegister", "工单号为"+result+"的保单管理现正对该团单进行保全操作，请先通知保全撤销相关操作，再进行理赔!");
             mDealState=false;
             return false;
         }
    	 //查询团单信息
    	 String tGrpContsql= "select a.customerno, a.name,a.AddressNo " +
         		"from lcgrpcont g,LCGrpAppnt a " +
         		"where a.grpcontno=g.grpcontno and g.appflag = '1' " +
         		"and g.GrpContNo='"+mGrpContNo+"' with ur ";
         SSRS tGrpContSSRS = new SSRS();
         tGrpContSSRS =mExeSQL.execSQL(tGrpContsql);
         if(tGrpContSSRS==null || tGrpContSSRS.getMaxRow()<=0){
        	 buildError("RgtRegister","该团单号："+mGrpContNo+"，在核心不存在");
        	 return false;
         }else{
        	 for (int i=1;i<=tGrpContSSRS.getMaxRow();i++){
        		  mCustomerNo=tGrpContSSRS.GetText(i, 1); 
        		  mGrpName =tGrpContSSRS.GetText(i, 2);  
        		  aAddressNo=tGrpContSSRS.GetText(i, 3); 

        		 if(mGrpName.length()>19){
        			 mGrpName =mGrpName.substring(0, 19);
        		 }
        	 }
         }
    	
    	 //查询投保人信息
         String tGrpCussql="select linkman1,phone1,GrpAddress,Grpzipcode from LCGrpAddress " +
		    		"where customerno = '"+mCustomerNo+"' and addressno = '" + aAddressNo + "' with ur";
         SSRS tGrpCusSSRS =mExeSQL.execSQL(tGrpCussql);
         if(tGrpCusSSRS!=null && tGrpCusSSRS.getMaxRow()>0){
        	 for(int j=1;j<=tGrpCusSSRS.getMaxRow();j++){
        		 mRgtantName =tGrpCusSSRS.GetText(j, 1);
        		 mRgtantPhone =tGrpCusSSRS.GetText(j, 2);
        		 mRgtantAddress =tGrpCusSSRS.GetText(j, 3);
        		 mRgtantPostCode =tGrpCusSSRS.GetText(j, 4);
        		 
        		 if(mRgtantAddress.length()>25){
        			 mRgtantAddress =mRgtantAddress.substring(0,25);
        		 }
        	 }
         }
         //查询证件信息
         String tGrpIdsql="select idno,idtype,idstartdate,idenddate from LCGrpAppnt"+
        		 " where GrpContNo = '" +mGrpContNo+ "'";
         SSRS tGrpIdSSRS=mExeSQL.execSQL(tGrpIdsql);
         if(tGrpIdSSRS!=null && tGrpIdSSRS.getMaxRow()>0){
        	 for(int a=1;a<=tGrpIdSSRS.getMaxRow();a++){
        		 mIdNo=tGrpIdSSRS.GetText(a, 1);
        		 mIdType=tGrpIdSSRS.GetText(a, 2);
        		 mIdStartDate=tGrpIdSSRS.GetText(a, 3);
        		 mIdEndDate=tGrpIdSSRS.GetText(a, 4);
        	 }
         }
         
         //生成批次
         String tLimit = PubFun.getNoLimit(mG.ManageCom);
         System.out.println("管理机构代码是 : "+mG.ManageCom);
         String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
         mRgtNo= RGTNO;
         System.out.println("getInputData start......");
         
         mLLRegisterSchema.setRgtNo(mRgtNo);
         mLLRegisterSchema.setRgtState("01"); // 检录状态
         mLLRegisterSchema.setRgtObj("0");	  //号码类型  0-总单 不懂啥意思
         mLLRegisterSchema.setRgtObjNo(mGrpContNo);
         mLLRegisterSchema.setRgtType("10");  // 受理方式 10-健管平台新增
         mLLRegisterSchema.setRgtClass("1");  // 申请类型 1-团体
         mLLRegisterSchema.setApplyerType("0");   
         mLLRegisterSchema.setRgtantName(mRgtantName);  //申请姓名
         mLLRegisterSchema.setRelation("05"); 			//申请人与被保人关系 其他
         mLLRegisterSchema.setRgtantAddress(mRgtantAddress);    //申请人地址
         mLLRegisterSchema.setRgtantPhone(mRgtantPhone);    	  //申请人电话
         mLLRegisterSchema.setPostCode(mRgtantPostCode);  //邮编
         mLLRegisterSchema.setCustomerNo(mCustomerNo);
         mLLRegisterSchema.setGrpName(mGrpName);     	  //单位名称
         mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate()); //立案日期
         mLLRegisterSchema.setAppDate(PubFun.getCurrentDate());	//申请日期
         mLLRegisterSchema.setAppPeoples(mApPeoples); //申请人数
         mLLRegisterSchema.setInputPeoples(mApPeoples);//申请人数
         mLLRegisterSchema.setCaseGetMode(mCaseGetMode);  //赔款领取方式   
         mLLRegisterSchema.setTogetherFlag(mTogetherFlag);//给付方式
         mLLRegisterSchema.setMngCom(mG.ManageCom);		//暂定 
         mLLRegisterSchema.setHandler1(mG.Operator);		//暂定以后修改
         mLLRegisterSchema.setOperator(mG.Operator);
//         if(mTogetherFlag.equals("3") || mTogetherFlag.equals("4")){
        	 mLLRegisterSchema.setBankCode(mBankCode);
        	 mLLRegisterSchema.setAccName(mAccName);
        	 mLLRegisterSchema.setBankAccNo(mBankAccNo);
//         }
         mLLRegisterSchema.setIDNo(mIdNo);
         mLLRegisterSchema.setIDType(mIdType);
         mLLRegisterSchema.setIDStartDate(mIdStartDate);
         mLLRegisterSchema.setIDEndDate(mIdEndDate);
         mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
         mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
         mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
         mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        
         LLHospCaseSchema aLLHospCaseSchema = new LLHospCaseSchema();
         aLLHospCaseSchema.setCaseNo(mRgtNo);
         aLLHospCaseSchema.setHospitCode(mRequestType);
         aLLHospCaseSchema.setHandler(mG.Operator);  //暂存以后修改
         aLLHospCaseSchema.setAppTranNo(mTransactionNum);
         aLLHospCaseSchema.setAppDate(PubFun.getCurrentDate());
         aLLHospCaseSchema.setAppTime(PubFun.getCurrentTime());
         aLLHospCaseSchema.setDealType("2");
         aLLHospCaseSchema.setCaseType("08");  //管A 批次
         aLLHospCaseSchema.setConfirmState("1");
         aLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
         aLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
         aLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
         aLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
         
         mMMap.put(mLLRegisterSchema, "DELETE&INSERT");
         mMMap.put(aLLHospCaseSchema, "DELETE&INSERT");
    	 return true;
     }
     
     /**
      * 解析案件信息
      * 
      */
     private boolean dealCaseRegister() throws Exception{
    	 System.out.println("LLMARegister----dealCaseRegister");
    	 
    	 if(mLLCaseList.getChildren("CUSTOMER_DATA").size()>0){
    		 for(g=0;g<mLLCaseList.getChildren("CUSTOMER_DATA").size();g++){
    			 mCustomerData=  (Element) mLLCaseList.getChildren("CUSTOMER_DATA").get(g);
    			 if(mCustomerData!=null){
    				 LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    			     LLSubReportSet mLLSubReportSet = new LLSubReportSet();     		//事件信息
    			     LLAppClaimReasonSet mLLAppClaimReasonSet = new LLAppClaimReasonSet(); //申请原因
    				 ClientRegisterBL tClientRegisterBL =new ClientRegisterBL();
    				 mPolicyList = mCustomerData.getChild("POLICYLIST");
    		    	 mSubReportList = mCustomerData.getChild("SUBREPORTLIST");
    				 VData tVData = new VData();
    				 // 校验分单信息 
    				 if(!dealCont()){
    					 return false;
    				 }
    				 //获取分单被保人5要素
                     String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGrpContNo+"' and a.idno='"+mCustomerData.getChildText("IDNO")+"' and a.insuredno='"+mCustomerData.getChildText("INSUREDNO")+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGrpContNo+"' and b.idno='"+mCustomerData.getChildText("IDNO")+"'and b.insuredno='"+mCustomerData.getChildText("INSUREDNO")+"' ";
                     ExeSQL tExeSQL = new ExeSQL();
                     SSRS tSSRS=tExeSQL.execSQL(tSqlIn);
                     if(tSSRS.getMaxRow()<0){
                    	buildError("dealCaseRegister", "第"+(g+1)+"位客户的被保险人查询失败");
 			            return false;
                     }
    				 
    				  LCInsuredDB tLCInsuredDB = new LCInsuredDB();
			          tLCInsuredDB.setInsuredNo(mInsuredNo);
			          LCInsuredSet tLCinsuredSet = tLCInsuredDB.query();
			          if (tLCinsuredSet.size() < 1) {
			            buildError("dealCaseRegister", "第"+(g+1)+"位客户的被保险人查询失败");
			            return false;
			          }
			          mLCInsuredSchema=tLCinsuredSet.get(1); 		//获取基本信息
			          
			          //生成案件信息
			          System.out.println("LLMARgtRegister----CaseRegister");
			 		 LLCaseSchema tLLCaseSchema = new LLCaseSchema();
			 		 
			 		 tLLCaseSchema.setCaseNo("");
			 		 tLLCaseSchema.setRgtNo(mLLRegisterSchema.getRgtNo());
			 		 tLLCaseSchema.setRgtState("01");    //受理状态
			 		
			          if(!"".equals(mCustomerData.getChildText("INSUREDNAME")) && mCustomerData.getChildText("INSUREDNAME")!=null){
			         	 tLLCaseSchema.setCustomerNo(mInsuredNo); 
			 			 tLLCaseSchema.setCustomerName(mCustomerData.getChildText("INSUREDNAME"));
			          }else{
			         		tLLCaseSchema.setCustomerNo(mInsuredNo);
			         		tLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
			         	}
			          // 3544 证件类型校验 如果为 0-身份证 5-户口本则进行校验 否则不校验从被保人表取值
			          String tIdType = mCustomerData.getChildText("IDTYPE");
			          String tIdNo = mCustomerData.getChildText("IDNO");
			          String tSxe = mCustomerData.getChildText("SEX");
			          if("0".equals(tIdType)|| "5".equals(tIdType)){
			        	 String  tIdResult =PubFun.CheckIDNo(tIdType,tIdNo,"", tSxe);
			        	 if(!"".equals(tIdResult)){
			        		 buildError("dealCaseRegister", "第"+(g+1)+"位客户的"+tIdResult);
			 	             return false;
			        	 }else{
			        		 tLLCaseSchema.setIDNo(tIdNo);
					 		 tLLCaseSchema.setIDType(tIdType);
					 		 tLLCaseSchema.setCustomerSex(tSxe);
			        	 }
			          }else{
			        	  tLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
					 	  tLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
					 	  tLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());
			          }
			         
			 		 tLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
			 		 
			 		 if(!"".equals(mCustomerData.getChildText("MOBILEPHONE")) && mCustomerData.getChildText("MOBILEPHONE")!=null){
			 			 tLLCaseSchema.setMobilePhone(mCustomerData.getChildText("MOBILEPHONE"));
			 		 }else{
			 			 String tPhoneSql="select b.mobile from lcaddress b where customerno='"+mInsuredNo+"' and b.AddressNo='" + mLCInsuredSchema.getAddressNo() +"'";
			 			 String tPhone =mExeSQL.getOneValue(tPhoneSql);
			 			 if(!"".equals(tPhone)&& tPhone!=null){
			 				 tLLCaseSchema.setMobilePhone(tPhone);
			 			 }
			 		 }
			 		 tLLCaseSchema.setSurveyFlag("0");    	//未调查
			 		 tLLCaseSchema.setRgtType("1");	   		//申请类
			 		 tLLCaseSchema.setAccdentDesc("管A项目");
			 		 tLLCaseSchema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
			 		 tLLCaseSchema.setCaseProp("09");       //简易案件
			 		 
			 		 String aBankCode =mCustomerData.getChildText("BANKCODE");
			 		 String aBankName =mCustomerData.getChildText("BANKNAME");
			 		 String aAccName =mCustomerData.getChildText("ACCNAME");
			 		 String aBankAccNo =mCustomerData.getChildText("BANKACCNO");
			 		 
			 		 if(!"1".equals(mLLRegisterSchema.getCaseGetMode()) && !"2".equals(mLLRegisterSchema.getCaseGetMode())){
			 	            if (aBankCode == null || "".equals(aBankCode)|| aBankCode.equals("null")) {
			 	                buildError("dealCaseRegister", "第"+(g+1)+"位客户的【银行编码】的值不能为空");
			 	                return false;
			 	            }
			 	            if (aBankName == null || "".equals(aBankName)|| aBankName.equals("null")) {
			 	                buildError("dealCaseRegister", "第"+(g+1)+"位客户的【银行名称】的值不能为空");
			 	                return false;
			 	            }
			 	            if (aAccName == null || "".equals(aAccName)|| aAccName.equals("null")) {
			 	                buildError("dealCaseRegister", "第"+(g+1)+"位客户的【账户名】的值不能为空");
			 	                return false;
			 	            }
			 	            if (aBankAccNo == null || "".equals(aBankAccNo)|| aBankAccNo.equals("null")) {
			 	                buildError("dealCaseRegister", "第"+(g+1)+"位客户的【账号】的值不能为空");
			 	                return false;
			 	            }
			 	            Pattern tPattern = Pattern.compile("(\\d)+");
			 	    		Matcher isNum = tPattern.matcher(aBankAccNo);

			 	    		if (!isNum.matches()) {
			 	    			buildError("dealCaseRegister", "第"+(g+1)+"位客户的【账号】只能是数字组成");
			 	    			return false;
			 	    		}
			 		 }
			 		 tLLCaseSchema.setBankCode(aBankCode);
			 		 tLLCaseSchema.setBankAccNo(aBankAccNo);
			 		 tLLCaseSchema.setAccName(aAccName);
					 
					 //生成事件信息
					 List tSubReportList = new ArrayList();
					 tSubReportList =mSubReportList.getChildren();
					 if(tSubReportList.size()>0){
						 for(int j=0;j<tSubReportList.size();j++){
							LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
							Element tSubReportData=  (Element) tSubReportList.get(j);
							String aAccDate =tSubReportData.getChildText("ACCDATE");
							if(aAccDate == null ||"".equals(aAccDate) ||aAccDate.equals("null")){
								buildError("dealCaseRegister","第"+(g+1)+"位客户的【发生日期】值不能为空");
								return false;
							}
						 if (!PubFun.checkDateForm(aAccDate)) {
					            buildError("dealCaseRegister", "第"+(g+1)+"位客户的【发生日期】格式错误(正确格式yyyy-MM-dd)");
					            return false;
					        }
							String aAccPlace =tSubReportData.getChildText("ACCPLACE");
							String aHopStartDate =tSubReportData.getChildText("HOSPSTARTDATE");
							String aHopEndDate =tSubReportData.getChildText("HOSPENDDATE");
							if(!"".equals(aHopStartDate) && aHopStartDate!= null){
								if (!PubFun.checkDateForm(aHopStartDate)) {
						            buildError("dealCaseRegister", "第"+(g+1)+"位客户的【入院日期】格式错误(正确格式yyyy-MM-dd)");
						            return false;
						        }
							}
							if(!"".equals(aHopEndDate) && aHopEndDate!= null){
								if(!PubFun.checkDateForm(aHopEndDate)){
									buildError("dealCaseRegister", "第"+(g+1)+"位客户的【出院日期】格式错误(正确格式yyyy-MM-dd)");
						            return false;
								}
							}
							String aAccDesc =tSubReportData.getChildText("ACCDESC");
							String aAccType =tSubReportData.getChildText("ACCTYPE");
							if(aAccType==null ||"".equals(aAccType)|| aAccType.equals("null")){
								buildError("dealCaseRegister","第"+(g+1)+"位客户的【事件类型】值不能为空");
								return false;
							}
							// 3544 校验发生地点省、市、县信息
							String aAccProvinceCode= tSubReportData.getChildText("ACCPROVINCECODE"); // 发生地点省
							String aAccCityCode= tSubReportData.getChildText("ACCCITYCODE"); // 发生地点市
							String aAccCountyCode= tSubReportData.getChildText("ACCCOUNTYCODE"); // 发生地点县
							String aResult = LLCaseCommon.checkAccPlace(aAccProvinceCode, aAccCityCode, aAccCountyCode);
							if(!"".equals(aResult)){
								buildError("dealCaseRegister","第"+(g+1)+"位客户的"+aResult);
								return false;
							}else{
								tLLSubReportSchema.setAccProvinceCode(aAccProvinceCode);
								tLLSubReportSchema.setAccCityCode(aAccCityCode);
								tLLSubReportSchema.setAccCountyCode(aAccCountyCode);
							}
							tLLSubReportSchema.setAccDate(aAccDate);
							tLLSubReportSchema.setSubRptNo("");
							tLLSubReportSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
							tLLSubReportSchema.setCustomerName(tLLCaseSchema.getCustomerName());
							tLLSubReportSchema.setAccDesc(aAccDesc);
							tLLSubReportSchema.setInHospitalDate(aHopStartDate);
							tLLSubReportSchema.setOutHospitalDate(aHopEndDate);
							tLLSubReportSchema.setAccPlace(aAccPlace);
							tLLSubReportSchema.setAccidentType(aAccType);
							
							mLLSubReportSet.add(tLLSubReportSchema);
							
						    }
					 }
					 
					 //案件的时效性
					 tLLCaseOpTimeSchema.setCaseNo("");
					 tLLCaseOpTimeSchema.setRgtState("01");
					 tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
					 tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
					 
					 // 赔付事件信息(目前仅针对于管A存储)  以及申请原因 
					 List aPolicyList = new ArrayList();
					 aPolicyList =mPolicyList.getChildren();
					 if(aPolicyList.size()>0){
						 for(int b=0;b<aPolicyList.size();b++){
							LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema(); 
							Element aPolicyData= (Element) aPolicyList.get(b);
							String aReasonCode= aPolicyData.getChildText("REASONCODE");
							if(!"".equals(aReasonCode) && aReasonCode!=null){
								tLLAppClaimReasonSchema.setReasonCode(aReasonCode);
								tLLAppClaimReasonSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
								tLLAppClaimReasonSchema.setReasonType("0");
								mLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
							}
						 }
					 }
					 if(mLLAppClaimReasonSet.size()>0){
						 tVData.add(mLLAppClaimReasonSet);
					 }
					 
					 //开始提交案件信息
					 LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
	                 tLLRegisterSchema.setRgtNo(mRgtNo);
	                 tLLRegisterSchema.setRgtObjNo(mGrpContNo);
	                 
	                 tVData.add(tLLRegisterSchema);
	                 tVData.add(mLLSubReportSet);
	                 tVData.add(tLLCaseSchema);
	                 tVData.add(tLLCaseOpTimeSchema);
	                 tVData.add(mG);
	                 
	                 if(!tClientRegisterBL.submitData(tVData, "UPDATE||MAIN")){
	                	 CErrors tError = tClientRegisterBL.mErrors;
	                	 tClientRegisterBL.getResult();
                         String ErrMessage = tError.getFirstError();
                         buildError("dealData()", "生成理赔信息出错!客户号:"+tLLCaseSchema.getCustomerNo()+"客户姓名:"+tLLCaseSchema.getCustomerName()+"错误信息:"+ErrMessage);
                         mDealState=false;
                         return false;
	                 }else{
	                	 mCaseNum =mCaseNum+1;
	                	 tLLCaseSchema =(LLCaseSchema) tClientRegisterBL.getResult().getObjectByObjectName("LLCaseSchema", 0);
	                	 mList.add(mCaseNum+","+tLLCaseSchema.getCaseNo());
	                	 System.out.println(mList.size());
	                	 
	                	 //信息到llcaseamount
	                	 for(int x=0;x<aPolicyList.size();x++){
	                		LLCaseAmountSchema tLLCaseAmountSchema = new LLCaseAmountSchema();
	                		Element xPolicyData =(Element) aPolicyList.get(x);
	                		String aBatchNo =PubFun1.CreateMaxNo("MACNO"+PubFun.getCurrentDate2(),8);
							tLLCaseAmountSchema.setCaseNo(tLLCaseSchema.getCaseNo());
							tLLCaseAmountSchema.setBatchNo(aBatchNo);
							tLLCaseAmountSchema.setTypeCode("MA01");    // 管A类型
							tLLCaseAmountSchema.setRiskCode(xPolicyData.getChildText("RISKCODE"));
							tLLCaseAmountSchema.setGetDutyCode(xPolicyData.getChildText("GETDUTYCODE"));
							tLLCaseAmountSchema.setRealAmount(xPolicyData.getChildText("REALAMOUNT"));
	                		tLLCaseAmountSchema.setMngCom(mG.ManageCom);
							tLLCaseAmountSchema.setOperator(mG.Operator);
							tLLCaseAmountSchema.setMakeDate(PubFun.getCurrentDate());
							tLLCaseAmountSchema.setMakeTime(PubFun.getCurrentTime());
							tLLCaseAmountSchema.setModifyDate(PubFun.getCurrentDate());
							tLLCaseAmountSchema.setModifyTime(PubFun.getCurrentTime());
							mLLCaseAmountSet.add(tLLCaseAmountSchema);
	                	 }
	                		
	                	 //信息存储到LLHOSPCASE
	                	 LLHospCaseSchema tLLHospCaseSchema = new LLHospCaseSchema();
	                	 tLLHospCaseSchema.setCaseNo(tLLCaseSchema.getCaseNo());
	                	 tLLHospCaseSchema.setClaimNo(mLLRegisterSchema.getRgtNo());
	                	 tLLHospCaseSchema.setHospitCode(mRequestType);
	                	 tLLHospCaseSchema.setHandler(tLLCaseSchema.getHandler());
	                	 tLLHospCaseSchema.setAppTranNo(mTransactionNum);
	                	 tLLHospCaseSchema.setAppDate(PubFun.getCurrentDate());
	                	 tLLHospCaseSchema.setAppTime(PubFun.getCurrentTime());
	                	 tLLHospCaseSchema.setCaseType("09");   //管A 案件
	                	 tLLHospCaseSchema.setConfirmState("1");
	                	 tLLHospCaseSchema.setDealType("2");
	                	 tLLHospCaseSchema.setBnfNo(tLLCaseSchema.getCustomerNo());
	                	 tLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
	                	 tLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
	                	 tLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
	                	 tLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
	                	 
	                	 mLLHospCaseSet.add(tLLHospCaseSchema);
	                 }
    			 }else{
    				 buildError("dealCaseRegister()", "获取第"+(g+1)+"位理赔信息出错!");
                     mDealState = false;
                     return false;
    			 }
    		 }
    		 
    	 }else{
    		 buildError("dealCaseRegister","报文中没有理赔信息");
    		 mDealState=false;
    		 return false;
    	 }
    	 
    	 
    	 
    	 return true;
     }
     
     /**
      * 解析分单信息
      * 
      */
     private boolean dealCont() throws Exception{
    	  mContNo = mCustomerData.getChildText("COUNTNO");
    	 if(mContNo ==null ||"".equals(mContNo)){
    		 buildError("dealCont","第"+(g+1)+"位客户的【保单号】的值不能为空");
    		 return false;
    	 }
    	  mInsuredNo =mCustomerData.getChildText("INSUREDNO");
    	 if (mInsuredNo == null || "".equals(mInsuredNo)) {
             buildError("dealCont","第"+(g+1)+"位客户的 【客户号】的值不能为空");
             return false;
         }

         //用客户号校验该客户是否有正在处理的保全项目,返回的是工单号，而且校验的是全部保单，考虑以后是否按照保单校验
         if (!"".equals(LLCaseCommon.checkPerson(mInsuredNo))) {
             buildError("dealCont", "该客户"+mInsuredNo+"正在进行保全处理，不能进行理赔");
             return false;
         }
         
         List tPolicyList = new ArrayList();
    	 tPolicyList =mPolicyList.getChildren();
    	 for(int i=0; i<tPolicyList.size();i++){
    		 Element tPolicyData= (Element) tPolicyList.get(i);
    		 mRiskCode =tPolicyData.getChildText("RISKCODE");
    		 if(mRiskCode ==null || "".equals(mRiskCode)){
    			  buildError("dealCont", "第"+(g+1)+"位客户的【险种编码】的值不能为空");
    	          return false;
    		 }
    		 mGetDutyCode =tPolicyData.getChildText("GETDUTYCODE");
    		 if (mGetDutyCode == null || "".equals(mGetDutyCode)) {
                 buildError("dealCont", "第"+(g+1)+"位客户的【给付责任编码】的值不能为空");
                 return false;
               }
    		 mGetDutyKind =tPolicyData.getChildText("REALAMOUNT");
    		 if (mGetDutyKind == null || "".equals(mGetDutyKind)) {
                 buildError("dealCont", "第"+(g+1)+"位客户的【给付责任类型】的值不能为空");
                 return false;
             }
    	
    		 if(!"162401".equals(mRiskCode)){
    			 buildError("dealCont","第"+(g+1)+"位客户的【险种编码】未符合批次信息");
    			 return false;
    		 }
    		 if(!"741201".equals(mGetDutyCode) &&!"741202".equals(mGetDutyCode)
     				&& !"741203".equals(mGetDutyCode)){
    			 buildError("dealCont","第"+(g+1)+"位客户的【给付责任类型】未符合批次信息");
    			 return false;
    		 }
    		   String tPolSQL = "select a.polno from lcpol a where  a.contno='" + mContNo +
                       "' and a.riskcode='" + mRiskCode +"' and a.insuredno='" +mInsuredNo +
                       "' and a.appflag='1' and a.stateflag='1' with ur ";
    		 System.out.println(tPolSQL);
    		 SSRS tPolSSRS =mExeSQL.execSQL(tPolSQL);
    		 if(tPolSSRS.getMaxRow()!=1){
    			 buildError("dealCont", "保单" + mContNo + "下险种"+ mRiskCode +"查询失败");
                 return false;
    		 }
    		 String tPolNo = tPolSSRS.GetText(1, 1);
             if ("".equals(tPolNo)) {
             	buildError("dealCont", "保单" + mContNo + "下险种"+ mRiskCode +"查询失败了");
                 return false;
             }
    	 }
    	 return true;
     }
     
     /**
      * 删除案件信息
      */
     private boolean cancelCase() {
    	System.out.println("LLMARgtRegister-----cancelCase");

    	if(!mDealState){
    		if(!"".equals(mLLRegisterSchema.getRgtNo()) && mLLRegisterSchema.getRgtNo()!=null){
    	
			//申请信息
			String xRgtSql ="update  llregister set declineflag='1',cancelreason='09',canceler='"+mG.Operator+"' "
					+ ",modifydate=current date,modifytime=current time  where rgtno ='"+mLLRegisterSchema.getRgtNo()+"' with ur";
			//案件信息
			String xCaseSql ="update llcase set rgtstate='14',CancleRemark='管A团体自动撤件',canclereason='09',cancledate=current date "
					+ " ,cancler='"+mG.Operator+"',modifydate=current date,modifytime=current time  where rgtno='"+mLLRegisterSchema.getRgtNo()+"' with ur";
			
			
			
			MMap tMMap =new MMap();
			tMMap.put(xRgtSql, "UPDATE");
			tMMap.put(xCaseSql, "UPDATE");
			
			try{
				mResult.clear();
				mResult.add(tMMap);
			 }catch(Exception e){
				buildError("cancelCase","准备提交撤件数据失败");
				return false;
			}
			 PubSubmit tPubSubmit = new PubSubmit();
		     if (!tPubSubmit.submitData(mResult, "")) {
		         buildError("cancelCase", "撤销数据提交失败");
		         return false;
		     }
    		}
			}else{
				return true;
			}
    	
    	 return false;
     }
     /**
      * 生成返回的报文信息
      * 
      */
     private Document createXML(){
    	 // 处理失败报文
    	 if(!mDealState || "".equals(mRgtNo)){
    		  return createFalseXML();
    	 }else{
    		 try{
    			return createResultXML(mRgtNo) ;
    		 }catch(Exception e){
    			 mResponseCode = "0";
         		buildError("createXML()", "生成返回报文错误!");
         		return createFalseXML();
    		 }
    	 }
     }
     
     /**
      * 
      * 生成错误信息报文
      * 
      */
     private Document createFalseXML(){
    	 System.out.print("LLMARgtRegister----createFalseXML");
    	 Element tRootData = new Element("PACKET");
    	 tRootData.addAttribute("type", "RESPONSE");
    	 tRootData.addAttribute("version", "1.0");
    	 // HEAD部分信息
    	 Element tHead = new Element("HEAD");
    	 Element tRequestType = new Element("REQUEST_TYPE");
    	 tRequestType.setText(mRequestType);
    	 Element tTransactionNum = new Element("TRANSACTION_NUM");
    	 tTransactionNum.setText(mTransactionNum);
    	 
    	 tHead.addContent(tRequestType);
    	 tHead.addContent(tTransactionNum);
    	 
    	 //BODY部分信息
    	 Element tBody = new Element("BODY");
    	 
    	 if(!"E".equals(mResponseCode)){
    		 mResponseCode="0";
    	 }
    	 
    	 Element tResponseCode = new Element("RESPONSE_CODE");
    	 tResponseCode.addContent(mResponseCode);
    	 tBody.addContent(tResponseCode);
    	 
    	 mErrorMessage =mErrors.getFirstError();
    	 Element tErrorMessage =new Element("ERROR_MESSAGE");
    	 tErrorMessage.addContent(mErrorMessage);
    	 tBody.addContent(tErrorMessage);
    	 
    	 //生成错误报文
    	 tRootData.addContent(tHead);
    	 tRootData.addContent(tBody);
    	 
    	 Document tDocument = new Document(tRootData);
    	 
    	 return tDocument;
     }
     
     /**
      * 生成正确的报文信息  
      * 
      */
     
     private Document createResultXML(String aRgtNo) throws Exception{
    	 System.out.println("LLMARgtRegister-----createResultXML");
    	 
    	 Element tRootData = new Element("PACKET");
    	 tRootData.addAttribute("type", "RESPONSE");
    	 tRootData.addAttribute("version","1.0");
    	 
    	 //HEAD 部分
    	 Element tHead = new Element("HEAD");
    	 Element tRequestType =new Element("REQUEST_TYPE");
    	 tRequestType.setText(mRequestType);
    	 tHead.addContent(tRequestType);
    	 
    	 Element tTransactionNum = new Element("TRANSACTION_NUM"); 
    	 tTransactionNum.setText(mTransactionNum);
    	 tHead.addContent(tTransactionNum);
    	 
    	 //BODY 部分
    	 
    	 Element tBody =new Element("BODY");
    	 Element tBackData = new Element("BACK_DATA");
    	 tBody.addContent(tBackData);
    	 
    	 //BACK_DATA部分
    	 Element  tRgtNo = new Element("RGTNO");
    	 tRgtNo.setText(aRgtNo);
    	 tBackData.addContent(tRgtNo);
    	 
    	 //案件处理部分
    	 Element  tCaseList =new Element("LLCASELIST");
    	 
    	 System.out.println("开始生成返回报文---mList"+mList.size());
    	 
    	 if(mList.size()>0 && mList!=null){
    		 for(int i=0; i<mList.size();i++){
    			 System.out.println("xml---"+i);
    			 
    			 String tList[] =mList.get(i).toString().split(",");
    			 
    			 System.out.println(tList[0]+"==="+tList[1]);
    			 Element tCaseData = new Element("LLCase_Data");
    			 Element tCaseNum = new Element("CASENUM") ;
    			 tCaseNum.setText(tList[0]);
    			 tCaseData.addContent(tCaseNum);
    			 
    			 Element tCaseNo = new Element("CASENO");
    			 tCaseNo.setText(tList[1]);
    			 tCaseData.addContent(tCaseNo);
    			 
    			 tCaseList.addContent(tCaseData);
    		 }
    	 
    	 }
    	 tBody.addContent(tCaseList);
    	 
    	 tRootData.addContent(tHead);
    	 tRootData.addContent(tBody);
    	 
    	 Document tDocument = new Document(tRootData);
    	 
    	 return tDocument;
     }
     
     /**
      * 追加错误信息
      *
      * @param szFunc
      *            String
      * @param szErrMsg
      *            String
      */
     private void buildError(String szFunc, String szErrMsg) {
         CError cError = new CError();
         cError.moduleName = "LLMARgtRegister";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         System.out.println(szFunc + "--" + szErrMsg);
         this.mErrors.addOneError(cError);
     }
     
	public static void main(String[] args) {
	  	Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/理赔报文/管A/MA00.xml"), "GBK");
            LLMARgtRegister tBusinessDeal = new LLMARgtRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}

}
