package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPFile;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_HANDLERSchema;
import com.sinosoft.lis.vschema.ES_DOC_HANDLERSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class ImageHandler {

	//影像件路径
	private String mPicPath;
	public CErrors mCErrors = new CErrors();
	private FTPTool tFTPTool;
	private ES_DOC_HANDLERSet mES_DOC_HANDLERSet = new ES_DOC_HANDLERSet();
	private PubFun1 pf1 = new PubFun1();
	private PubFun pf =new PubFun();
	private VData mInputData = new VData();
    private MMap map = new MMap();
    private String Caseno;
    private String PicType;
    private String ManageCom;
    private String Operator;

	public String getCaseno() {
		return Caseno;
	}

	public void setCaseno(String caseno) {
		Caseno = caseno;
	}

	public String getPicType() {
		return PicType;
	}

	public void setPicType(String picType) {
		PicType = picType;
	}

	public String getManageCom() {
		return ManageCom;
	}

	public void setManageCom(String manageCom) {
		ManageCom = manageCom;
	}

	public String getOperator() {
		return Operator;
	}

	public void setOperator(String operator) {
		Operator = operator;
	}

	public ImageHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImageHandler(String mPicPath) {
		super();
		this.mPicPath = mPicPath;
	}
	/**
	 * 影像件统一点处理
	 */
	public boolean dealImage() {
		if(!getConn()){
			return false;
		}
		if(!isExist()) {
			return false;
		}
		if(!SavePath()) {
			return false;
		}
		return true;
	}
	/**
	 * 一键处理影像件
	 * @param mPicPath  影像件路径
	 * @param caseno   案件号
	 * @param picType	处理类型
	 * @param manageCom	机构
	 * @param operator	操作人员
	 */
	 public ImageHandler(String mPicPath, String caseno, String picType,
			String manageCom, String operator) {
		super();
		this.mPicPath = mPicPath;
		Caseno = caseno;
		PicType = picType;
		ManageCom = manageCom;
		Operator = operator;
	}

	/**
		 * 影像件异步处理，进行对数据单独的处理
		 * @params args
		 */
		public boolean SavePath() {
			//影像件异步处理，开始
			ES_DOC_HANDLERSchema tES_DOC_HANDLERSchema = new ES_DOC_HANDLERSchema();
			String strES = "ES"+pf.getCurrentDate2();
			String serialno = strES + pf1.CreateMaxNo(strES, 8);
			System.out.println("serialno:"+serialno);
			tES_DOC_HANDLERSchema.setSerialNo(serialno);
			tES_DOC_HANDLERSchema.setCaseNo(Caseno);
			tES_DOC_HANDLERSchema.setZipPath(mPicPath);
			tES_DOC_HANDLERSchema.setFileName(new File(mPicPath).getName());
			tES_DOC_HANDLERSchema.setState("2");
			tES_DOC_HANDLERSchema.setPicType(PicType);
			tES_DOC_HANDLERSchema.setMngCom(ManageCom);
			tES_DOC_HANDLERSchema.setOperater(Operator);
			tES_DOC_HANDLERSchema.setMakeDate(pf.getCurrentDate());
			tES_DOC_HANDLERSchema.setMakeTime(pf.getCurrentTime());
			tES_DOC_HANDLERSchema.setModifyDate(pf.getCurrentDate());
			tES_DOC_HANDLERSchema.setModifyTime(pf.getCurrentTime());
			mES_DOC_HANDLERSet.add(tES_DOC_HANDLERSchema);
			if(mES_DOC_HANDLERSet.size()>0) {
				map.put(mES_DOC_HANDLERSet, "INSERT");
		    	this.mInputData.clear();
		        mInputData.add(map);
		    	PubSubmit tPubSubmit = new PubSubmit();
		        if (!tPubSubmit.submitData(mInputData, null)) {
		        	  buildError("SavePath()", "影像件数据处理有误");
			          return false;
		        }
			}
			return true;
		}
	/**
	 * 判断FTP文件是否存在
	 */
	public boolean isExist() {
		
		try {
			if(!mPicPath.endsWith(".zip")) {
	    		buildError("dealTYScan()", "影像件路径不完整");  
	    		return false;
	    	}
			FTPFile[] files = tFTPTool.listFiles(mPicPath);
			if(files==null || files.length<1) {
				buildError("isExist()", "影像件文件不存在"); 
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
	 * 连接FTP
	 * @param mPicPath
	 * @return
	 */
	public boolean getConn() {
		
    		String getIPPort = "select codename ,codealias from ldcode where codetype='LLTYScan' and code='IP/Port' ";
    		String getUserPs = "select codename ,codealias from ldcode where codetype='LLTYScan' and code='User/Pass' ";
    		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
    		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
    		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
    			buildError("getConn()", "ftp配置有误"); 
    			return false;
    		}
    		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 1),
    				tUPSSRS.GetText(1, 2), Integer.parseInt(tIPSSRS.GetText(1, 2)));
    		try {
    			if (!tFTPTool.loginFTP()) {
    				buildError("getConn()", tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE));
    				return false;
    			}
    		} catch (SocketException e) {
    			buildError("getConn()", "FTP连接异常");
    			e.printStackTrace();
    			return false;
    		} catch (IOException e) {
    			buildError("getConn()", "FTP连接异常");
    			e.printStackTrace();
    			return false;
    		}
		return true;
}
	/***
	 * 错误信息
	 * @param args
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError tCError = new CError();
		tCError.moduleName="LLTYAutoCaseRegister";
		tCError.functionName=szFunc;
		tCError.errorMessage=szErrMsg;
		System.out.println(szFunc+"--"+szErrMsg);
		this.mCErrors.addOneError(tCError);
	}
}