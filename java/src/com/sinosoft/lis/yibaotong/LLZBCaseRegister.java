package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LLCaseBackDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.lis.llcase.UnitClaimSaveBL;
import com.sinosoft.lis.llcase.genCaseInfoBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseBackSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.lis.vschema.LLCaseBackSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseOpTimeSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 自动立案并理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhang
 * @version 1.0
 */
public class LLZBCaseRegister {
	
	 public CErrors mErrors = new CErrors();
	 public VData mResult = new VData();
	 private VData mInputData = new VData();
	 private String mErrorMessage="";  //错误描述
	 
	 private String mManageCom="86330000";   //管理机构
	 private String mOperator="";   //人员
	 
	 private List mList = new ArrayList();//返回信息合集
	 //报文头信息
     private Document mInXmlDoc;             //传入报文    
     private String mDocType = "";           //报文类型    
     private String mResponseCode = "1";     //返回类型代码    
     private String mRequestType = "";       //请求类型    
     private String mTransactionNum = "";    //交互编码
     private String mDealType = "2";         //处理类型 1 实时 2非实时 
     private boolean tCancelFlag = true;		//每次运行时将前一批案件撤件
     //报文内容
     private int mCaseNum = 0;
     private String mGRPCONTNO = "";       //团体保单号    
     /** 社保保单标志 0：非社保；1：社保*/
     private String mSocialSecurity = "0";
     private String mReturnMessage = "";//错误提示集合
     
     private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();  //医保通案件处理表信息
     private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); // 批次信息
     private LLCaseSet mLLCaseSet = new LLCaseSet();
     private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
     private LLCaseSet mbackCaseSet = new LLCaseSet();//案件回退集合
     private LLCaseSet mCliamCaseSet = new LLCaseSet();//案件确认集合
     private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();
 	 private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();
 	 private ES_DOC_RELATIONSet mES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();
     private GlobalInput mG=new GlobalInput();
     
     //批次号
     private String mRgtNo = "";
     private String mTogetherflag="";	// 给付方式
     private String mCasegetmode="";  //赔款领取方式
     private String mCurrentDate = PubFun.getCurrentDate();
     private String mCurrentTime = PubFun.getCurrentTime();
     
     //处理结果标记
     private boolean mDealState=true;  
     private Element mLLCaseList;

	 public LLZBCaseRegister(){
		 
	 }
	 
	 public Document service(Document pInXmlDoc){
		 ExeSQL tExeSQL = new ExeSQL();
		 //获取案件操作员
		 String sql ="select code from ldcode where codetype ='ZJWB'";
		 String tOperator =tExeSQL.getOneValue(sql);
		 mOperator =tOperator;
		 System.out.println("mOperator==="+mOperator);
		 try{
			 //获取报文信息
			 if(!getInputData(pInXmlDoc)){
				 mDealState=false;
			 }
			 //校验基本信息
			 if(!checkData()){
				 mDealState=false;
			 }else{
				 // 数据处理
				 if(!dealData()){
					 mDealState = false;
				 }
			 }

		 }catch(Exception ex){
		    System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
		 }finally{
			 mInXmlDoc = createXML();
		 }

		 return mInXmlDoc;
	 }
	 
	 /***
	  * 理赔返回报文
	  * @param tInXmlDoc
	  * @return
	  * @throws Exception
	  */
	 private Document createXML(){
		  //处理失败，返回错误信息报文
		 if(!mDealState || "".equals(mRgtNo)){
			   return createFalseXML();
		 }else{
			 try{
				 return createResultXml(mRgtNo);
			 }catch(Exception e){
				 e.printStackTrace();
	        	 buildError("createXML()", "生成返回报文错误!");
	        	 return createFalseXML();
			 }
		 }

	 }
	 
	 /**
	  * 理赔返回错误报文
	  * @param tInXmlDoc
	  * @return
	  * @throws Exception
	  */
	 private Document createFalseXML(){
		 System.out.println("LLZBCaseRegister--createFalseXML(错误返回报文)");
		 Element tRootData = new Element("PACKET");
		 tRootData.addAttribute("type", "RESPONSE");
		 tRootData.addAttribute("version", "1.0");
		 // HEAD部分数据
		 Element tHeadData = new Element("HEAD");
		 Element tRequestType = new Element("REQUEST_TYPE");
		 tRequestType.setText(mRequestType);
		 tHeadData.addContent(tRequestType);
		 Element tTransactionNum = new Element("TRANSACTION_NUM");
		 tTransactionNum.setText(mTransactionNum);
		 tHeadData.addContent(tTransactionNum);
		 
		 //Body部分数据
		 Element tBodyData = new Element("BODY");
		 if (!"E".equals(mResponseCode)) {
	            mResponseCode = "0";
	        }
		 Element tResponseCode = new Element("RESPONSE_CODE");
		 tResponseCode.setText(mResponseCode);
	     tBodyData.addContent(tResponseCode);
	     
	     String tError= mErrors.getFirstError();
	     if(tError.indexOf("@@")!=-1){//说明有多个为空的节点
		     Element tErrorList = new Element("ERROR_LIST");
	    	 String [] listErr=tError.split("@@");
	    	 for (int a = 0; a < listErr.length; a++) {
	    		 String tErrors=listErr[a];
	    		 Element tErrorMessage = new Element("ERROR_MESSAGE");
	    	     tErrorMessage.setText(tErrors);
	    	     tErrorList.addContent(tErrorMessage);
			}
	    	 tBodyData.addContent(tErrorList);
	     }else{
	     Element tErrorMessage = new Element("ERROR_MESSAGE");
	     tErrorMessage.setText(tError);
	     tBodyData.addContent(tErrorMessage);
	     }
	     tRootData.addContent(tHeadData);
	     tRootData.addContent(tBodyData);
	    
	     Document tDocument = new Document(tRootData);
		 return tDocument;
 
	 }
	 
	 /***
	  * 理赔生成正确的报文
	  * 
	  * @param tInXmlDoc
	  * @return
	  * @throws Exception
	  */
	 private Document createResultXml(String aRgtNo){
		 System.out.println("LLZBCaseRegister--createResultXml(返回正确报文)");
		 
		 Element tRootData = new Element("PACKET");
		 tRootData.addAttribute("type", "RESPONSE");
		 tRootData.addAttribute("version", "1.0");
		 // HEAD部分数据
		 Element tHeadData = new Element("HEAD");
		 Element tRequestType = new Element("REQUEST_TYPE");
		 tRequestType.setText(mRequestType);
		 tHeadData.addContent(tRequestType);
		 Element tTransactionNum = new Element("TRANSACTION_NUM");
		 tTransactionNum.setText(mTransactionNum);
		 tHeadData.addContent(tTransactionNum);
		 
		 tRootData.addContent(tHeadData);
		 
		 //Body 部分
		 Element tBodyData = new Element("BODY");
		 Element tBackData = new Element("BACK_DATA");
		 tBodyData.addContent(tBackData);
		 
		 //Back_Data部分
		 Element tRgtData = new Element("RGTNO");
		 tRgtData.setText(aRgtNo);
		 tBackData.addContent(tRgtData);
		 
		 //LLCaseList部分
		 Element tLLcaseList = new Element("LLCASELIST");
		 if(mList.size()>0 && mList!=null){
			 for(int i=0;i<mList.size();i++){
				  System.out.println("开始返回报文生成:"+i);
				  String[] tResult = mList.get(i).toString().split(",");
				  System.out.println("tResult[0]"+tResult[0]+"  "+"tResult[1]"+tResult[1]);
				  
				  Element tListData = new Element("LLCASE_DATA");
				  Element tCaseNum =new Element("CASENUM");
				  tCaseNum.setText(tResult[0]);
				  Element tCaseNo = new Element("CASENO");
				  tCaseNo.setText(tResult[1]);
				  Element tClaimNo = new Element("CLAIMNO");
				  tClaimNo.setText(tResult[2]);
				  
				  tListData.addContent(tCaseNum);
				  tListData.addContent(tCaseNo);
				  tListData.addContent(tClaimNo);
				  
				  tLLcaseList.addContent(tListData);
			 }
		 }
		 
		 tBodyData.addContent(tLLcaseList);
		 tRootData.addContent(tBodyData);
		 
		 Document tDocument = new Document(tRootData);
		 return tDocument;
		 
	 }
	 
	 
	 // 获取报文信息
	 private boolean getInputData(Document tInXmlDoc) throws Exception{
		   System.out.println("LLZBCaseRegister--getInputData--浙江分案信息");
		   
		   if (tInXmlDoc == null) {
	            buildError("getInputData()", "未获得报文");
	            return false;
	        }
		 //获取根元素
		 Element tElement =tInXmlDoc.getRootElement();
		 mDocType = tElement.getAttributeValue("type");
		 
		 //获取Head部分内容
		 Element tHead =tElement.getChild("HEAD");
		 mRequestType = tHead.getChildText("REQUEST_TYPE");
		 mTransactionNum = tHead.getChildText("TRANSACTION_NUM");
		 
		 //获取Body部分内容
		 Element tBody =tElement.getChild("BODY");
		 mLLCaseList =tBody.getChild("LLCASELIST");
		
		 mG.ManageCom=mManageCom;
	     mG.Operator=mOperator;
		 return true;
	 }
	 
	 /***
	  * 校验报文基本信息
	  * @return
	  */
	 private boolean checkData() throws Exception{
		 System.out.println("LLZBCaseRegister--checkData--浙江分案信息");
		   if (!"REQUEST".equals(mDocType)) {
	            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
	            return false;
	        }

	        if (!"ZW02".equals(mRequestType)) {
	            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
	            return false;
	        }

	        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
	            buildError("checkData()", "【交互编码】的编码个数错误!");
	            return false;
	        }

	        if (!"ZW02".equals(mTransactionNum.substring(0, 4))) {
	            buildError("checkData()", "【交互编码】的请求类型编码错误!");
	            return false;
	        }

	        mManageCom = mTransactionNum.substring(4, 12);        
	        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86330000".equals(mManageCom)) {
	            buildError("checkData()", "【交互编码】的管理机构编码错误!");
	            return false;
	        }
		 
		 return true;
	 }
	 
	 /***
	  * 处理报文基本信息
	  * 
	  */
	 private boolean dealData() throws Exception{
		 System.out.println("LLZBCaseRegister--dealData--浙江分案信息");
		 
		 //1.受理到检录
		 if(!dealCaseRegister()){
			 return false;
		 }else{
			 //受理错误
			 if(!mDealState){
				 return false;
			 }else{
			    //保存信息到LLHospCase
                PubSubmit tPubSubmit = new PubSubmit();
                VData mVData = new VData();
                MMap tMMap= new MMap();
                tMMap.put(mLLHospCaseSet, "INSERT");
                tMMap.put(mES_DOC_RELATIONSet, "INSERT");
                tMMap.put(mES_DOC_MAINSet, "INSERT");
                tMMap.put(mES_DOC_PAGESSet, "INSERT");
                mVData.add(tMMap);
                if(!tPubSubmit.submitData(mVData, "")){
                	 mDealState = false;
                	 buildError("service()", "数据提交报错");
	                }
			 }
		 }
		 //2.检录到理算，包含核赔
		 if(!batchClaimCal(mRgtNo)){
			 return false;
		 }else{
			 //理算出错
			 if(!mDealState){
				 return false;
			 }
		 }
		 
		 //3.理算确认
		 if(!simpleClaimAudit()){
			 
		 }else{
			 //确认出错
			 if(!mDealState){
				 return false;
			 }
		 }

		 return true ;
	 }
	 
	 /***
	  * 理算确认
	  * 
	  */
	 private boolean simpleClaimAudit(){
		 System.out.println("浙江外包--理算确认simpleClaimAudit()--胜利在望");
		 //1.数据校验
		 LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
		 tLLClaimUserDB.setUserCode(mG.Operator);
		 if(!tLLClaimUserDB.getInfo()){
			 buildError("simpleClaimAudit()", "您不是理赔人，无权作确认操作");
	         mDealState = false; 
	         return false;
		 }
		 if(tLLClaimUserDB.getClaimPopedom()==null){
			 buildError("simpleClaimAudit()", "您没有理赔权限，无权作确认操作");
	         mDealState = false; 
	         return false;
		 }
		 
		 //2.准备数据确认
		 LLCaseDB tLLCaseDB = new LLCaseDB();
		 tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
		 mLLCaseSet = tLLCaseDB.query();
		 if(mLLCaseSet.size()<=0){
			 buildError("simpleClaimAudit()", "没有待确认的案件!");
	         mDealState = false; 
	         return false;
		 }
		   for(int i=1;i<=mLLCaseSet.size();i++){
	            LLCaseSchema tcase = mLLCaseSet.get(i);               
	            if (!dealClaim(tcase)) {
	                continue;
	        }
		  }
		 
		 //3.对错误案件进行回退
		  CaseBack(mbackCaseSet);
		  
		 //4.更新llregister
		 mInputData.clear();
		 MMap map = new MMap();
		 LLRegisterDB tLLRegisterDB = new LLRegisterDB();
	     String tRgtNo = mLLRegisterSchema.getRgtNo();
	     tLLRegisterDB.setRgtNo(tRgtNo);
	     if(tLLRegisterDB.getInfo()){
	    	 LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
	    	 tLLRegisterSchema = tLLRegisterDB.getSchema();
	    	 tLLRegisterSchema.setRgtState("02");  //团体受理申请结束
	    	 // 数据处理
	    	    map.put(tLLRegisterSchema, "UPDATE");
	            map.put(mCliamCaseSet, "UPDATE");
	            mInputData.add(map);
	            mResult.add(mReturnMessage);
	            PubSubmit tPubSubmit = new PubSubmit();
	            if (!tPubSubmit.submitData(mInputData, null)) {
	                CError.buildErr(this,"数据提交失败!");
	                return false;
	            }
	     }else{
	    	 buildError("simpleClaimAudit()", "批次操作查询失败");
	         mDealState = false; 
	         return false; 
	     }

		 return true;
	 }
	 
	 /**
	     * 案件回退
	     * 1。审定人发现案件错误，做案件回退
	     * 2。系统审定操作不能通过，做自动回退
	     * @return boolean
	  */
	 private boolean CaseBack(LLCaseSet tbackCaseSet){
		 System.out.println("浙江外包--CaseBack()----案件撤件功能");
		 System.out.println("撤件的个数--"+tbackCaseSet.size());
		 MMap tMap = new MMap();
		 LLCaseBackSet tLLCaseBackSet = new LLCaseBackSet();
		 LLCaseBackSet oldLLCaseBackSet = new LLCaseBackSet();
		 LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
		 boolean tFlag=false;
		 if(tbackCaseSet.size()>0){
			 for(int a=1;a<=tbackCaseSet.size();a++){
				 LLCaseSchema tLLCaseSchema = tbackCaseSet.get(a);
				 LLCaseBackSchema aLLCaseBackSchema = new LLCaseBackSchema();
				 String tRgtState= tLLCaseSchema.getRgtState();
				 String tRgtStateName="";
				 LDCodeDB tLDCodeDB = new LDCodeDB();
				 tLDCodeDB.setCode(tRgtState);
				 tLDCodeDB.setCodeType("llrgtstate");
				 if(tLDCodeDB.getInfo()){ // 获取状态名称
					 tRgtStateName =tLDCodeDB.getCodeName();
				 }
			     if(!tRgtState.equals("03")&&!tRgtState.equals("04")&&
			        !tRgtState.equals("05")&&!tRgtState.equals("06")&&
			        !tRgtState.equals("09")&&!tRgtState.equals("10")){
			                //写报错信息返回
			                mReturnMessage+="案件"+tLLCaseSchema.getCaseNo()+"当前为"
			                   +tRgtStateName+",不能做回退操作<br>";
			                continue;
			      }
			     
			     String tLimit= PubFun.getNoLimit(this.mG.ManageCom);
			     String tCaseBackNo= PubFun1.CreateMaxNo("CaseBack", tLimit);
			     aLLCaseBackSchema.setBeforState(tRgtState); //旧案件状态
			     aLLCaseBackSchema.setCaseBackNo(tCaseBackNo);
			     aLLCaseBackSchema.setAviFlag("Y"); //标记
			     aLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
			     aLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
			     aLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
			     aLLCaseBackSchema.setNHandler(tLLCaseSchema.getHandler());
			     aLLCaseBackSchema.setAfterState("01"); //新案件状态
			     aLLCaseBackSchema.setRemark(tLLCaseSchema.getCancleRemark());
			     aLLCaseBackSchema.setReason(tLLCaseSchema.getCancleReason());
			     aLLCaseBackSchema.setMngCom(this.mG.ManageCom);
			     aLLCaseBackSchema.setOperator(this.mG.Operator);
			     aLLCaseBackSchema.setMakeDate(mCurrentDate);
			     aLLCaseBackSchema.setMakeTime(mCurrentTime);
			     aLLCaseBackSchema.setModifyDate(mCurrentDate);
			     aLLCaseBackSchema.setModifyTime(mCurrentTime);
			     
			     tLLCaseBackSet.add(aLLCaseBackSchema);
			     //操作轨迹
			     String tSQL= "select max(Sequance) from LLCaseOpTime where caseno = '"+tLLCaseSchema.getCaseNo()+"' and RgtState='01' with ur ";
			     ExeSQL aExeSql = new ExeSQL();
			     String tSequance= aExeSql.getOneValue(tSQL);
			     LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
			     tLLCaseOpTimeSchema.setRgtState("01");
			     tLLCaseOpTimeSchema.setCaseNo(tLLCaseSchema.getCaseNo());
			     if("".equals(tSequance) ||tSequance ==null ||"null".equals(tSequance)){
			    	 tLLCaseOpTimeSchema.setSequance("1");
			     }else{
			    	 tSequance =Integer.toString(Integer.parseInt(tSequance)+1);
			    	 tLLCaseOpTimeSchema.setSequance(tSequance);
			     }
		         tLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
		         tLLCaseOpTimeSchema.setOperator(mG.Operator);
		         tLLCaseOpTimeSchema.setStartDate(mCurrentDate);
		         tLLCaseOpTimeSchema.setStartTime(mCurrentTime);
		         tLLCaseOpTimeSchema.setOpTime("0:00:00");
		         
		         tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
		         
		         //保存后当前回退记录生效，需要把过去的回退记录设为过期
	             LLCaseBackDB tLLCaseBackDB = new LLCaseBackDB();
	             tLLCaseBackDB.setCaseNo(tLLCaseSchema.getCaseNo());
	             tLLCaseBackDB.setAviFlag("Y");
	             LLCaseBackSet tempLLCaseBackSet = new LLCaseBackSet();
	             tempLLCaseBackSet = tLLCaseBackDB.query();
	             for(int x=1;x<=tempLLCaseBackSet.size();x++){
	                tempLLCaseBackSet.get(x).setAviFlag("");
	                tempLLCaseBackSet.get(x).setModifyDate(mCurrentDate);
	                tempLLCaseBackSet.get(x).setModifyTime(mCurrentTime);
	             }
	             oldLLCaseBackSet.add(tempLLCaseBackSet);  //旧记录修改日期
	             tbackCaseSet.get(a).setRgtState("01");  //案件状态
	             tbackCaseSet.get(a).setCancleReason("");
	             tbackCaseSet.get(a).setCancleRemark("");
	             tbackCaseSet.get(a).setUWer(mG.Operator);
	             tbackCaseSet.get(a).setDealer(mG.Operator);
	             tbackCaseSet.get(a).setModifyDate(mCurrentDate);
	             tbackCaseSet.get(a).setModifyTime(mCurrentTime);
	             tFlag=true;
  
			 }
			 
		 }
		 if(tFlag){
			 tMap.put(tbackCaseSet, "UPDATE");
			 tMap.put(tLLCaseBackSet, "INSERT");
			 tMap.put(oldLLCaseBackSet, "UPDATE");
			 tMap.put(tLLCaseOpTimeSet, "INSERT");
	     }
		 
		 this.mInputData.clear();
         mInputData.add(tMap);
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
		 return true;
	 }
	 
	/***
	 * 对批次案件理算确认
	 * 
	 */
	private boolean dealClaim(LLCaseSchema aLLCaseSchema){
		System.out.println("浙江外包开始确认---不容易--");
		VData tvdata = new VData();
	    UnitClaimSaveBL tUnitClaimSaveBL = new UnitClaimSaveBL();
	    tvdata.add(aLLCaseSchema);
	    tvdata.add(mG);
	    if(!tUnitClaimSaveBL.submitData(tvdata, "BATCH")){
	    	CErrors tError = tUnitClaimSaveBL.mErrors;
	    	String tErrMesssage = tError.getFirstError();
	    	aLLCaseSchema.setCancleRemark(tErrMesssage); //撤件备注
	    	aLLCaseSchema.setCancleReason("4");
	    	mbackCaseSet.add(aLLCaseSchema);
	    	mReturnMessage += "<br>案件"+aLLCaseSchema.getCaseNo()
	                    +"信息不全，被自动回退到批改信箱。";
	        return false;  
	    }else{
	    	aLLCaseSchema.setRgtState("04");
	    	aLLCaseSchema.setHandler(mG.Operator);
	    	mCliamCaseSet.add(aLLCaseSchema);
	    }
		
		return true;
	}
		   
	 /***
	  * 检录信息到理算（核赔规则）
	  * 
	  */
	 private boolean batchClaimCal(String aRgtNo){
		 //1.理算前批次数据校验
		 MMap tMap =new MMap();
		 VData tData = new VData();
		 LLRegisterDB tLLRegisterDB = new LLRegisterDB();
		 tLLRegisterDB.setRgtNo(aRgtNo);
		 if(!tLLRegisterDB.getInfo()){
			 buildError("dealData()", "团体立案信息查询失败!");
	         mDealState = false; 
	         return false;
		 }
		 mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
		 String tDecFlag = mLLRegisterSchema.getDeclineFlag(); // 撤件标志
		 if("1".equals(tDecFlag)){
			 buildError("dealData()", "该团体申请已撤件，不能再进行理赔!");
			 mDealState = false;
			 return false;
		 }
		 if ("03".equals(mLLRegisterSchema.getRgtState())) {
	            // @@错误处理
			 buildError("dealData()", "团体申请下所有个人案件已经结案完毕，" +
                    "不能再进行理赔!");
			 mDealState = false; 
			 return false;
	        }
         if ("04".equals(mLLRegisterSchema.getRgtState())) {
            // @@错误处理
        	buildError("dealData()", "团体申请下所有个人案件已经给付确认，" +
                "不能再进行理赔!");
        	mDealState = false; 
        	return false;
         }
         //理算钱案件数据进行校验
         LLCaseDB tLLCaseDB = new LLCaseDB();
         tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
         tLLCaseDB.setRgtState("03");
         mLLCaseSet =tLLCaseDB.query();
         if(mLLCaseSet.size()<=0){
        	 buildError("batchClaimCal()", "没有待理算的案件!");
             mDealState = false; 
             return false;
         }
         
         //2.理算
         for(int i=1;i<=mLLCaseSet.size();i++){
        	 if(!calClaim(mLLCaseSet.get(i).getCaseNo())){
        		 continue;
        	 }else{
        		 LLCaseOpTimeSchema  tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
        		 tLLCaseOpTimeSchema.setCaseNo(mLLCaseSet.get(i).getCaseNo());
        		 tLLCaseOpTimeSchema.setRgtState("04");
        		 tLLCaseOpTimeSchema.setOperator(mG.Operator);
                 tLLCaseOpTimeSchema.setManageCom(mG.ManageCom);
                 LLCaseCommon tLLCaseCommon = new  LLCaseCommon();
                 try{
	                 LLCaseOpTimeSchema mLLCaseOpTimeSchema =tLLCaseCommon.CalTimeSpan(tLLCaseOpTimeSchema);
	                 tMap.put(mLLCaseOpTimeSchema, "DELETE&INSERT");
                 }catch(Exception ex){
                	 System.out.println("没有时效记录");
                 }
        	 }
         }
         
         //3.核赔校验
         if(!uwCheck()){
        	 return false;
         }
         
         //4.提交数据
         tData.add(tMap);
         PubSubmit ps = new PubSubmit();
         if (!ps.submitData(tData, null)) {
             buildError("dealData()", "数据库保存失败");
             mDealState = false; 
             return false;
         }
         
		 
		 return true;
	 }
	 
	 /***
	  * 
	  * 核赔信息
	  */
	 private boolean uwCheck(){
		 System.out.println("浙江外包案件信息---uwCheck--核赔环节");
		 String tSql="";
		 ExeSQL tExesql = new ExeSQL();
		 int tErrNO=0;
	     String tErr=null;
	     SSRS ssrs = new SSRS();
	     if(!"".equals(this.mLLRegisterSchema.getRgtNo()) && this.mLLRegisterSchema.getRgtNo()!=null){
	    	 tSql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'  with ur";
		     System.out.println("BatchClaimCalBL-中通过批量号获取客户号："+tSql);
		     ssrs = tExesql.execSQL(tSql);
	     }
	     System.out.println(ssrs.getMaxRow());
	     for(int j=1;j<=ssrs.getMaxRow();j++){
	    	  VData tVData = new VData();
	      	  LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
	      	  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
	      	  LLCaseSet tLLCaseSet=new LLCaseSet();
	      	  LLCaseRelaSet tLLCaseRelaSet=new LLCaseRelaSet();
	      	  LLSubReportSet tLLSubReportSet=new LLSubReportSet();
	      	  LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
	      	  LLCaseDB tLLCaseDB = new LLCaseDB();
	      	  tLLCaseDB.setCustomerNo(ssrs.GetText(j,1));
	      	  tLLCaseDB.setRgtState("03");
	      	  tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
	      	  tLLCaseSet =tLLCaseDB.query();
	      	  String tCasenos="";
	      	  if(tLLCaseSet.size()>0 && tLLCaseSet!=null){
	      		 for(int i=1;i<=tLLCaseSet.size();i++){
	      			System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息Start！");
	      			tCasenos= tCasenos+","+tLLCaseSet.get(i).getCaseNo();
	      			LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
	      			tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(i).getCaseNo());
	      			tLLClaimDetailSet.add(tLLClaimDetailDB.query());
	      			LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
	      			tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(i).getCaseNo());
	      			tLLCaseRelaSet.add(tLLCaseRelaDB.query());
	      			System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息End！"); 
	      		 }
	      		 
	      		 if(tLLCaseRelaSet!=null && tLLCaseRelaSet.size()>0){
	      			 for(int a=1;a<=tLLCaseRelaSet.size();a++){
	      				 LLSubReportDB tLLSubReportDB = new LLSubReportDB();
	      				 tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(a).getSubRptNo());
	      				 tLLSubReportSet.add(tLLSubReportDB.query()); 
	      			 }
	      			System.out.println("BatchClaimCalBL-中获取理赔LLSubReport表信息END！");
	      		 }
	      	  }
	      	  tVData.addElement(tLLCaseSchema);
	      	  tVData.addElement(tLLCaseRelaSet);
	      	  tVData.addElement(tLLSubReportSet);
	      	  tVData.addElement(tLLClaimDetailSet);
	      	  tVData.addElement(mG);
	      	  if(!tLLUWCheckBL.submitData(tVData, "")){
	      		tErrNO = tErrNO +1;
	      		if(tErr==null){
	      			tErr=String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(j, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+tCasenos+");";  
	      		}else{
	      			tErr=tErr+String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(j, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+tCasenos+");";
	      		}
	      	  }
	     }
	     if(tErr!=null){
        	 CError.buildErr(this,tErr);
        	 return false;
         }
		 
		 return true;
	 }
	 
	 /***
	  * 自动理算，生成赔案信息
	  * 
	  */
	 private boolean calClaim(String aCaseNo){
		 System.out.println("浙江外包案件信息---calClaim--理算环节");
		 ClaimCalBL aClaimCalAutoBL = new ClaimCalBL();
	     LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	     tLLCaseSchema.setCaseNo(aCaseNo);
	     VData tData = new VData();
	     tData.add(tLLCaseSchema);
	     tData.add(mG);
	     if(!aClaimCalAutoBL.submitData(tData, "autoCal")){
	    	 buildError("calClaim()", "理算失败");
             mDealState = false; 
             return false; 
	     } 
		 return true;
	 }
	 
	 
	 /***
	  *  理赔分案信息处理
	  * 
	  */
	 private boolean dealCaseRegister(){
		 System.out.println("dealCaseRegister--浙江分案处理");
		 
		 ExeSQL tExeSQL = new ExeSQL();
		 //获取LLCASE_DATA信息
		 if(mLLCaseList.getChildren("LLCASE_DATA").size()>0){
			  for(int i=0;i<mLLCaseList.getChildren("LLCASE_DATA").size();i++){
				  Element mLLCaseInfo= (Element) mLLCaseList.getChildren("LLCASE_DATA").get(i);
				  if(mLLCaseInfo!=null){
					//调用报文解析类将数据封装到类中
					 LLZBMajorDiseasesCaseParser tLLZBMajorDiseasesCaseParser=new LLZBMajorDiseasesCaseParser(mLLCaseInfo);
	                 LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	                 LLCaseExtSchema tLLCaseExtSchema = new LLCaseExtSchema();//3842 tmm 2018-5-17
	                 LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
	                 LLSecurityReceiptSchema tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
	                 LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema(); 
	                 VData tVData = new VData();
	                 
	                 genCaseInfoBL tgenCaseInfoBL = new genCaseInfoBL(); 
	                 
	                 //校验数据的唯一性
	                 String pk=" select count(1) from LLHospCase where 1 =1 "
//	                    		+ " and AppTranNo='"+mTransactionNum+"' "
	                    		+ " and ClaimNo='"+tLLZBMajorDiseasesCaseParser.getCLAIMNO()+"' with ur "	;
	                    String number=tExeSQL.getOneValue(pk);
	                	System.out.println("================SQL=======【"+pk+"】===========");
	                    if(!"0".equals(number)){
	                    	buildError("dealData()", "交易编码为："+mTransactionNum+"，平台理赔号为："+tLLZBMajorDiseasesCaseParser.getCLAIMNO()+"的数据信息已重复使用。");
	                    	System.out.println("================交易编码+平台理赔号出现重复，表中已存在，交易编码："+mTransactionNum+"，平台理赔号："+tLLZBMajorDiseasesCaseParser.getCLAIMNO()+"=========");
	                    	return false;
	                    }
	                    //加非空校验，总共29个字段。
	                    String errs=checkNode(tLLZBMajorDiseasesCaseParser,i);
	                    if(!"".equals(errs)&&errs!=null){
	                    	buildError("dealCaseRegister", errs);
	                    	return false;
	                    }
	                    if(mLLHospCaseSet.size()>0){
		                    for(int a=1;a<=mLLHospCaseSet.size();a++){
		                    	LLHospCaseSchema aLLHospCaseSchema = mLLHospCaseSet.get(a);
		                    	String aClaimNo= aLLHospCaseSchema.getClaimNo();
		                    	if(aClaimNo.equals(tLLZBMajorDiseasesCaseParser.getCLAIMNO())){
		                    		buildError("dealData()", "平台理赔号为："+tLLZBMajorDiseasesCaseParser.getCLAIMNO()+"的数据信息已重复使用。");
			                    	System.out.println("================交易编码+平台理赔号出现重复，表中已存在，交易编码："+mTransactionNum+"，平台理赔号："+tLLZBMajorDiseasesCaseParser.getCLAIMNO()+"=========");
			                    	return false;
		                    	}
		                    }
	                    }
	                      
	                 //通过rgtno 获取到团单号 进行分案处理
	                 mRgtNo = tLLZBMajorDiseasesCaseParser.getRGTNO();
	                 String tRgtSql ="select rgtobjno,togetherflag,casegetmode from llregister where rgtno='"+mRgtNo+"' with ur";
	                 SSRS tGrpCont =tExeSQL.execSQL(tRgtSql);
	                 if(tGrpCont.getMaxRow()<0){
	                	 buildError("dealData()", "未提供批次申请号,查询信息失败");
	                     mDealState = false;  
	                 }
	                 mGRPCONTNO =tGrpCont.GetText(1,1);
	                 mTogetherflag =tGrpCont.GetText(1, 2);
	                 mCasegetmode =tGrpCont.GetText(1, 3);
	                 
	                 //给付方式为 1-个人给付时 或者 2-部分给付时 
	                 if("1".equals(mTogetherflag)||"2".equals(mTogetherflag)){
	                	 tLLCaseSchema.setCaseGetMode(mCasegetmode);
	                 }
	                 
	            	 String tAccDate = tLLZBMajorDiseasesCaseParser.getACCDATE();
	            	 if(!"".equals(tAccDate) && tAccDate!=null){
	            		 if (!PubFun.checkDateForm(tAccDate)) {
					            buildError("dealCaseRegister", "第"+(i+1)+"位客户的【发生日期】格式错误(正确格式yyyy-MM-dd)");
					            return false;
					        }
	            	 }else{
	            		 buildError("dealCaseRegister", "第"+(i+1)+"位客户的【发生日期】为空。");
				            return false;
	            	 }
					
	                 tLLCaseSchema .setCaseNo("");
	                 tLLCaseSchema.setRgtState("03");  //检录状态
	                 tLLCaseSchema.setRgtType("1");   //申请类案件
	                 tLLCaseSchema.setRgtNo(mRgtNo); 
	                 tLLCaseSchema.setAccidentDate(tAccDate); // 发生日期
	                 tLLCaseSchema.setPhone(tLLZBMajorDiseasesCaseParser.getGETDUTYCODE()); // 给付责任暂存
	                 //  根据客户号团单号获取被保人信息
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getCUSTOMERNO()) && tLLZBMajorDiseasesCaseParser.getCUSTOMERNO()!=null){
	                	 String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGRPCONTNO+"' and a.insuredno='"+tLLZBMajorDiseasesCaseParser.getCUSTOMERNO()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGRPCONTNO+"' and b.insuredno='"+tLLZBMajorDiseasesCaseParser.getCUSTOMERNO()+"' with ur ";
	                     SSRS tSSRS =tExeSQL.execSQL(tSqlIn);
	                     if(tSSRS.getMaxRow()>0){
	                    	tLLCaseSchema.setCustomerNo(tSSRS.GetText(1,1)); 
	                    	tLLCaseSchema.setCustBirthday(tSSRS.GetText(1,4));
	                    	tLLCaseSchema.setIDNo(tSSRS.GetText(1,5));
	                    	tLLCaseSchema.setIDType(tSSRS.GetText(1,8));
	                    	if("".equals(tSSRS.GetText(1,8)) || tSSRS.GetText(1,8)==null || tSSRS.GetText(1,8)=="null"){
	                    		tLLCaseSchema.setIDType("4"); 
	                    	}
	                     }else{
	                    	 buildError("dealData()","客户信息查询失败");
	                    	 return false;
	                     }
	                 }else{
	                	 // 根据团单号和证件号获取被保人信息
	                	 String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGRPCONTNO+"' and a.idno='"+tLLZBMajorDiseasesCaseParser.getIDNO()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGRPCONTNO+"' and b.idno='"+tLLZBMajorDiseasesCaseParser.getIDNO()+"' with ur ";
		                 SSRS tSSRS = tExeSQL.execSQL(tSqlIn);
		                 if(tSSRS.getMaxRow()>0){
		                	 tLLCaseSchema.setCustomerNo(tSSRS.GetText(1,1)); 
		                     tLLCaseSchema.setCustBirthday(tSSRS.GetText(1,4));
		                     tLLCaseSchema.setIDNo(tSSRS.GetText(1,5));
		                     tLLCaseSchema.setIDType(tSSRS.GetText(1,8));
		                     if("".equals(tSSRS.GetText(1,8)) || tSSRS.GetText(1,8)==null || tSSRS.GetText(1,8)=="null"){
		                    		tLLCaseSchema.setIDType("4"); 
		                    	}
		                 }else{
		                	 buildError("dealData()","客户信息查询失败");
	                    	 return false;
		                 }
	                 }
	                 
	                 tLLCaseSchema.setCustomerName(tLLZBMajorDiseasesCaseParser.getCUSTOMERNAME());
	                 tLLCaseSchema.setCustomerSex(tLLZBMajorDiseasesCaseParser.getSEX());
	                 
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getMOBILEPHONE()) && tLLZBMajorDiseasesCaseParser.getMOBILEPHONE()!=null){
	                	 tLLCaseSchema.setMobilePhone(tLLZBMajorDiseasesCaseParser.getMOBILEPHONE());
	                 }
	                 
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getREMARK()) && tLLZBMajorDiseasesCaseParser.getREMARK() !=null){
	                	 tLLCaseExtSchema.setRemark(tLLZBMajorDiseasesCaseParser.getREMARK());//3842 tmm 2018-5-17
	                 }
	                 
	                 tLLCaseSchema.setSurveyFlag("0"); 
	                 tLLCaseSchema.setAccdentDesc("浙江外包");
	                 tLLCaseSchema.setInHospitalDays(tLLZBMajorDiseasesCaseParser.getREALHOSPDATE()); //实际住院天数
//	                 tLLCaseSchema.setCaseGetMode("");
	                 // 获取银行信息
	                 String tbankSql=" select li.BankCode,li.BankAccNo,li.AccName  from lcinsured li where li.insuredno ='"+tLLCaseSchema.getCustomerNo()+"'  and li.grpcontno ='"+mGRPCONTNO+"'  ";
	                 SSRS bankSSRS=tExeSQL.execSQL(tbankSql);
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getBANKCODE()) && tLLZBMajorDiseasesCaseParser.getBANKCODE()!=null){
	                	 tLLCaseSchema.setBankCode(tLLZBMajorDiseasesCaseParser.getBANKCODE()); 
	                 }else{
	                	 if(bankSSRS.getMaxRow()>0){
	                		 if(!"".equals(bankSSRS.GetText(1,1))){
	                			 tLLCaseSchema.setBankCode(bankSSRS.GetText(1,1));
	                		 }else{
		                		 //如果批次为个人给付，赔款领取方式为银行，则银行信息不能为空,阻断
		                		 String sebankc="  select count(1) from llregister where 1=1 and rgtno='"+mRgtNo+"'"
		                		 				+" and TogetherFlag in ('1','2') and CaseGetMode not in ('1','2') ";
		                		 String nums=tExeSQL.getOneValue(sebankc);
		                		 if(!"0".equals(nums)){
		                			 buildError("dealData()","【银行编码】的值不能为空");
			                    	 return false;
		                		 }
		                	 }
	                		 
	                	 }
	                 }
	                 
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getBANKACCNO()) && tLLZBMajorDiseasesCaseParser.getBANKACCNO() !=null){
	                	 tLLCaseSchema.setBankAccNo(tLLZBMajorDiseasesCaseParser.getBANKACCNO());
	                 }else{
	                	 if(bankSSRS.getMaxRow()>0){
	                		 if(!"".equals(bankSSRS.GetText(1,2))){
	                			 tLLCaseSchema.setBankAccNo(bankSSRS.GetText(1,2));
	                		 }else{
		                		 //如果批次为个人给付，赔款领取方式为银行，则银行信息不能为空,阻断
		                		 String sebankc="  select count(1) from llregister where 1=1 and rgtno='"+mRgtNo+"'"
		                		 				+" and TogetherFlag in ('1','2') and CaseGetMode not in ('1','2') ";
		                		 String nums=tExeSQL.getOneValue(sebankc);
		                		 if(!"0".equals(nums)){
		                			 buildError("dealData()","【账号】的值不能为空");
			                    	 return false;
		                		 }
		                	 }
	                		 
	                	 }
	                 }
	                 
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getACCNAME()) && tLLZBMajorDiseasesCaseParser.getACCNAME()!=null){
	                	 tLLCaseSchema.setAccName(tLLZBMajorDiseasesCaseParser.getACCNAME());
	                 }else{
	                	 if(bankSSRS.getMaxRow()>0){
	                		 if(!"".equals(bankSSRS.GetText(1,3))){
	                			 tLLCaseSchema.setAccName(bankSSRS.GetText(1,3));
	                		 }else{
		                		 //如果批次为个人给付，赔款领取方式为银行，则银行信息不能为空,阻断
		                		 String sebankc="  select count(1) from llregister where 1=1 and rgtno='"+mRgtNo+"'"
		                		 				+" and TogetherFlag in ('1','2') and CaseGetMode not in ('1','2') ";
		                		 String nums=tExeSQL.getOneValue(sebankc);
		                		 if(!"0".equals(nums)){
		                			 buildError("dealData()","【账户名】的值不能为空");
			                    	 return false;
		                		 }
		                	 }
	                		 
	                	 }
	                 }
	                 
	                 
	                 
	                 //开始处理账单信息
	                 tLLFeeMainSchema.setRgtNo(mRgtNo);
	                 tLLFeeMainSchema.setCaseNo("");
	                 tLLFeeMainSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
	                 tLLFeeMainSchema.setOldMainFeeNo(tLLZBMajorDiseasesCaseParser.getGETDUTYKIND()); // 暂存此处
	                 tLLFeeMainSchema.setCustomerName(tLLCaseSchema.getCustomerName());
	                 tLLFeeMainSchema.setCustomerSex(tLLCaseSchema.getCustomerSex());
	                 tLLFeeMainSchema.setInsuredStat(tLLZBMajorDiseasesCaseParser.getINSUREDSTAT());  //人员类型
	                 tLLFeeMainSchema.setHospitalCode(tLLZBMajorDiseasesCaseParser.getHOSPITALNAMECODE()); //医院编码
	                 tLLFeeMainSchema.setHospitalName(tLLZBMajorDiseasesCaseParser.getHOSPITALNAME()); //医院名称
	                 tLLFeeMainSchema.setSecurityNo(tLLZBMajorDiseasesCaseParser.getSECURYTINO()); //社保号
	                 tLLFeeMainSchema.setFeeAtti("4");  //账单属性
	                 tLLFeeMainSchema.setFeeType(tLLZBMajorDiseasesCaseParser.getFEETYPE()); //账单种类
	                 tLLFeeMainSchema.setSelfAmnt(tLLZBMajorDiseasesCaseParser.getSELFAMNT()); // 自费金额
	                 tLLFeeMainSchema.setReceiptNo(tLLZBMajorDiseasesCaseParser.getRECEIPTNO());  //账单号码
	                 tLLFeeMainSchema.setFeeDate(tLLZBMajorDiseasesCaseParser.getFEEDATE());  //账单日期
	                 tLLFeeMainSchema.setHospStartDate(tLLZBMajorDiseasesCaseParser.getHOSPSTARTDATE());
	                 tLLFeeMainSchema.setHospEndDate(tLLZBMajorDiseasesCaseParser.getHOSPENDDATE());
	                 tLLFeeMainSchema.setRealHospDate(tLLCaseSchema.getInHospitalDays()); //实际住院天数
	                 
	                 //住院号
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getINPATIENTNO()) && tLLZBMajorDiseasesCaseParser.getINPATIENTNO()!=null){
	                	 tLLFeeMainSchema.setInHosNo(tLLZBMajorDiseasesCaseParser.getINPATIENTNO());
	                 }
	                 //第三方
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getOTHERORGANAMNT()) && tLLZBMajorDiseasesCaseParser.getOTHERORGANAMNT()!=null){
	                	  tLLFeeMainSchema.setOtherOrganAmnt(tLLZBMajorDiseasesCaseParser.getOTHERORGANAMNT());
	                 }
	                 // 不合理费用
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getREFUSEAMNT()) && tLLZBMajorDiseasesCaseParser.getREFUSEAMNT()!=null){
	                	 tLLFeeMainSchema.setRefuseAmnt(tLLZBMajorDiseasesCaseParser.getREFUSEAMNT());
	                 }
	                 //费用总额
	                 tLLFeeMainSchema.setSumFee(tLLZBMajorDiseasesCaseParser.getAPPLYAMNT());
	                 
	                 //可报销范围内金额
	                 if(!"".equals(tLLZBMajorDiseasesCaseParser.getREIMBBURSEMENT()) && tLLZBMajorDiseasesCaseParser.getREIMBBURSEMENT()!=null){
	                	 tLLFeeMainSchema.setReimbursement(tLLZBMajorDiseasesCaseParser.getREIMBBURSEMENT());
	                 }
	                 //实赔金额
	                 tLLFeeMainSchema.setAffixNo(tLLZBMajorDiseasesCaseParser.getAFFIXNO());
	                 //账单标记
	                 tLLFeeMainSchema.setOriginFlag(tLLZBMajorDiseasesCaseParser.getORIGINFLAG());
	                 // 校验医保类型
	                 String tMedicaretype =tLLZBMajorDiseasesCaseParser.getMEDICARETYPE();
	                 String tMedicareCheck =LLCaseCommon.checkMedicareType(tMedicaretype);
                     if(!"".equals(tMedicareCheck)){
                    	buildError("dealData()",tMedicareCheck+"。");
                        mDealState = false; 
                        return false;
                     }else{
                    	tLLFeeMainSchema.setMedicareType(tMedicaretype);
                     }
                 
                     // 事件信息
                     LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
                     
                     // 校验发生地点省、市、 县
                     String tAccProvince=tLLZBMajorDiseasesCaseParser.getACCPROVINCECODE(); //省
                     String tAccCity = tLLZBMajorDiseasesCaseParser.getACCCITYCODE();    //市
                     String tAccCounty = tLLZBMajorDiseasesCaseParser.getACCCOUNTYCODE();  //县
                     String tAccCheck =LLCaseCommon.checkAccPlace(tAccProvince, tAccCity, tAccCounty);
                     if(!"".equals(tAccCheck)){
                     	buildError("dealData()",tAccCheck+"。");
                         mDealState = false; 
                         return false;
                     }else{
                     	tLLSubReportSchema.setAccProvinceCode(tAccProvince);
                     	tLLSubReportSchema.setAccCityCode(tAccCity);
                     	tLLSubReportSchema.setAccCountyCode(tAccCounty);
                     }
                     
                     //社保账单录入
                     tLLSecurityReceiptSchema.setFeeDetailNo(""); // 账单费用明细
                     tLLSecurityReceiptSchema.setMainFeeNo(""); //账单号码
                     tLLSecurityReceiptSchema.setRgtNo(mRgtNo);
                     tLLSecurityReceiptSchema.setCaseNo("");
                     //申报金额
                     tLLSecurityReceiptSchema.setApplyAmnt(tLLZBMajorDiseasesCaseParser.getAPPLYAMNT());
                     //医保目录内金额
                     tLLSecurityReceiptSchema.setFeeInSecu("");
                     //年付统筹支付
                     tLLSecurityReceiptSchema.setYearPlayFee("");
                     //住院大额医疗
                     if(!"".equals(tLLZBMajorDiseasesCaseParser.getSUPINHOSFEE()) && tLLZBMajorDiseasesCaseParser.getSUPINHOSFEE()!=null){
                    	 tLLSecurityReceiptSchema.setSupInHosFee(tLLZBMajorDiseasesCaseParser.getSUPINHOSFEE());
                     }
                     //高段一责任金额
                     tLLSecurityReceiptSchema.setHighAmnt1(tLLZBMajorDiseasesCaseParser.getHIGHAMNT1());
                     //自付一金额
                     if(!"".equals(tLLZBMajorDiseasesCaseParser.getSELFPAY1()) && tLLZBMajorDiseasesCaseParser.getSELFPAY1()!=null){
                    	 tLLSecurityReceiptSchema.setSelfPay1(tLLZBMajorDiseasesCaseParser.getSELFPAY1());
                     }
                     //自付二金额
                     if(!"".equals(tLLZBMajorDiseasesCaseParser.getSELFPAY2()) && tLLZBMajorDiseasesCaseParser.getSELFPAY2()!=null){
                    	 tLLSecurityReceiptSchema.setSelfPay2(tLLZBMajorDiseasesCaseParser.getSELFPAY2());
                     }
                     //自费金额
                     tLLSecurityReceiptSchema.setSelfAmnt(tLLZBMajorDiseasesCaseParser.getSELFAMNT());
                     //起付限
                     tLLSecurityReceiptSchema.setGetLimit(tLLZBMajorDiseasesCaseParser.getGETLIMIT());
                     //账户支付
                     if(!"".equals(tLLZBMajorDiseasesCaseParser.getACCOUNTPAY()) && tLLZBMajorDiseasesCaseParser.getACCOUNTPAY()!=null){
                    	 tLLSecurityReceiptSchema.setAccountPay(tLLZBMajorDiseasesCaseParser.getACCOUNTPAY());
                     }
                     //统筹支付
                     if(!"".equals(tLLZBMajorDiseasesCaseParser.getPLANFEE()) && tLLZBMajorDiseasesCaseParser.getPLANFEE()!=null){
                    	 tLLSecurityReceiptSchema.setPlanFee(tLLZBMajorDiseasesCaseParser.getPLANFEE());
                     }
                     //中段责任金
                     if(!"".equals(tLLZBMajorDiseasesCaseParser.getMIDAMNT()) && tLLZBMajorDiseasesCaseParser.getMIDAMNT()!=null){
                    	 tLLSecurityReceiptSchema.setMidAmnt(tLLZBMajorDiseasesCaseParser.getMIDAMNT());
                     }
                     
                     //诊疗明细
                     List tDiseaseList = new ArrayList();
                     Element tDisease=tLLZBMajorDiseasesCaseParser.getDISEASELIST();
                     tDiseaseList =tDisease.getChildren();
                     LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
                     if(tDiseaseList.size()>0){
	                     for(int j=0;j<tDiseaseList.size();j++){
	                    	Element tDiseaseData= (Element) tDiseaseList.get(j);
	                    	String tDiseaseCode = tDiseaseData.getChildText("DISEASECODE");
	                    	String tDiseaseName = tDiseaseData.getChildText("DISEASENAME");
	                    	// 校验疾病信息
	                    	if("".equals(tDiseaseCode) || tDiseaseCode==null || tDiseaseCode=="null"){
	                    		 buildError("dealData()", "【疾病代码】的值不能为空");
	                    		 return false;
	                    	}
	                    	if("".equals(tDiseaseName) || tDiseaseName==null || tDiseaseName=="null"){
	                    		buildError("dealData()", "【疾病名称】的值不能为空");
	                    		 return false;
	                    	}
	                    	LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
	                    	//账单号
	                    	tLLCaseCureSchema.setReceiptNo(tLLFeeMainSchema.getReceiptNo()); 
	                    	tLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
	                    	tLLCaseCureSchema.setDiseaseName(tDiseaseName);
	                    	tLLCaseCureSet.add(tLLCaseCureSchema);
	                     }
                     }
                     
                     // 案件的时效性 
                     tLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
                     tLLCaseOpTimeSchema.setRgtState("01");
                     tLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
                     tLLCaseOpTimeSchema.setOperator(mOperator);
                     tLLCaseOpTimeSchema.setManageCom(mManageCom);
                     
                     //申请原因
                     LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
                     if(tLLZBMajorDiseasesCaseParser.getFEETYPE().equals("1")){
                    	 tLLAppClaimReasonSchema.setReasonCode("01");
                         tLLAppClaimReasonSchema.setReason("门诊"); //申请原因
                     }else if(tLLZBMajorDiseasesCaseParser.getFEETYPE().equals("2")){
                    	 tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
                         tLLAppClaimReasonSchema.setReason("住院"); //申请原因
                     }else if(tLLZBMajorDiseasesCaseParser.getFEETYPE().equals("3")){
                    	 tLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
                         tLLAppClaimReasonSchema.setReason("特种病"); //申请原因
                     }else{
                    	 tLLAppClaimReasonSchema.setReasonCode("00"); //原因代码
                         tLLAppClaimReasonSchema.setReason("无"); //申请原
                     }
                     tLLAppClaimReasonSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
                     tLLAppClaimReasonSchema.setReasonType("0");
                     
                     if(!"00".equals(tLLAppClaimReasonSchema.getReasonCode())){
                    	 tVData.add(tLLAppClaimReasonSchema);
                     }
                     
                     //开始提交数据
                     LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
                     tLLRegisterSchema.setRgtNo(mRgtNo);
                     tLLRegisterSchema.setRgtObjNo(mGRPCONTNO);
                     tVData.add(tLLRegisterSchema);
                     tVData.add(tLLCaseCureSet);
                     tVData.add(tLLCaseSchema);
                     tVData.add(tLLSubReportSchema);
                     tVData.add(tLLCaseOpTimeSchema);
                     tVData.add(tLLFeeMainSchema);
                     tVData.add(tLLSecurityReceiptSchema);
                     tVData.add(mG);
                     tVData.add(mG);
	                 if(tLLCaseExtSchema !=null){
	                	 tVData.add(tLLCaseExtSchema);//3842 tmm 2018-5-17
	                 }

                     
                     if(!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")){
                    	CErrors tError= tgenCaseInfoBL.mErrors;
                    	tgenCaseInfoBL.getResult();
                    	String tErrMessage= tError.getFirstError();
                    	buildError("dealData()", "生成理赔信息出错!客户号:"+tLLCaseSchema.getCustomerNo()+"客户姓名:"+tLLCaseSchema.getCustomerName()+"错误信息:"+tErrMessage);
                        return false;
                     }else{
                    	 mCaseNum=mCaseNum+1;
                    	 tLLCaseSchema =  (LLCaseSchema) tgenCaseInfoBL.getResult().getObjectByObjectName("LLCaseSchema", 0);
                    	 mList.add(mCaseNum+","+tLLCaseSchema.getCaseNo()+","+tLLZBMajorDiseasesCaseParser.getCLAIMNO());
                    	 System.out.println("=====浙江外包案件个数："+mList.size());
                    	 String tCaseNo= tLLCaseSchema.getCaseNo();
                    	 
                    	 //LLHospCase数据报存
                    	 LLHospCaseSchema tLLHospCaseSchema = new LLHospCaseSchema();
                    	 tLLHospCaseSchema.setCaseNo(tCaseNo);
                    	 tLLHospCaseSchema.setDealType(mDealType);
 	                     tLLHospCaseSchema.setHospitCode(mRequestType);
 	                     tLLHospCaseSchema.setHandler(mOperator);
 	                     tLLHospCaseSchema.setAppTranNo(mTransactionNum);
 	                     tLLHospCaseSchema.setAppDate(mCurrentDate);
 	                     tLLHospCaseSchema.setAppTime(mCurrentTime);                         
 	                     tLLHospCaseSchema.setConfirmState("1");
 	                     if(mRequestType.equals("ZW02")){
 	                    	tLLHospCaseSchema.setCaseType("12");//浙江外包项目-12
 	                     }
 	                      
 	                    tLLHospCaseSchema.setClaimNo(tLLZBMajorDiseasesCaseParser.getCLAIMNO());
 	                    tLLHospCaseSchema.setBnfNo(tLLCaseSchema.getCustomerNo());   
 	                    tLLHospCaseSchema.setRealpay(tLLZBMajorDiseasesCaseParser.getAFFIXNO());//申报金额
 	                    tLLHospCaseSchema.setHCNo(tLLCaseSchema.getMngCom()); //保障个人信箱查询功能
 	                    tLLHospCaseSchema.setMakeDate(mCurrentDate);
 	                    tLLHospCaseSchema.setMakeTime(mCurrentTime);
 	                    tLLHospCaseSchema.setModifyDate(mCurrentDate);
 	                    tLLHospCaseSchema.setModifyTime(mCurrentTime);
 	                      
 	                    mLLHospCaseSet.add(tLLHospCaseSchema);
 	                    
 	                    // 影像件信息
 	                   Element tImageInfos= tLLZBMajorDiseasesCaseParser.getIMAGEINFOS();
 	                   List tPageInfo = new ArrayList();
 	                   tPageInfo=(List)tImageInfos.getChildren("pageinfo");
 	                   String tSubType = tImageInfos.getChildText("subtype");
 	                  if("".equals(tSubType)||tSubType==null){buildError("dealCaseRegister","第"+(i+1)+"位客户的影像类型subtype不能为空"); return false;}
 	                   String mPages = tImageInfos.getChildText("NUMPAGES");
 	                  if("".equals(mPages)||mPages==null){buildError("dealCaseRegister","第"+(i+1)+"位客户的影像页数NUMPAGES不能为空"); return false;}
 	                   if(!"".equals(mPages) && mPages!=null && !"null".equals(mPages)){
 	                	   //影像件主表
 	                	   ES_DOC_MAINSchema tES_DOC_MainSchema = new ES_DOC_MAINSchema();
 	                	   tES_DOC_MainSchema.setDocCode(tCaseNo);
 	                	   tES_DOC_MainSchema.setInputStartDate(mCurrentDate);
 	                	   tES_DOC_MainSchema.setInputEndDate(mCurrentDate);
 	                	   tES_DOC_MainSchema.setSubType("LP01");
 	                	   tES_DOC_MainSchema.setBussType("LP");
 	                	   tES_DOC_MainSchema.setManageCom(mManageCom);
 	                	   tES_DOC_MainSchema.setVersion("01");
 	                	   tES_DOC_MainSchema.setScanNo("0");
 	                	   tES_DOC_MainSchema.setState("01");
 	                	   tES_DOC_MainSchema.setDocFlag("1");
 	                	   tES_DOC_MainSchema.setOperator(mOperator);
 	                	   tES_DOC_MainSchema.setScanOperator(mOperator);
 	                	   tES_DOC_MainSchema.setNumPages(mPages);
 		        		   String strDocID = getMaxNo("DocID");
 		        		   tES_DOC_MainSchema.setDocID(strDocID);
 		        		   tES_DOC_MainSchema.setMakeDate(mCurrentDate);
 		        		   tES_DOC_MainSchema.setModifyDate(mCurrentDate);
 		        		   tES_DOC_MainSchema.setMakeTime(mCurrentTime);
 		        		   tES_DOC_MainSchema.setModifyTime(mCurrentTime);
 		        		   mES_DOC_MAINSet.add(tES_DOC_MainSchema);
 	                	   
 	                	   if(tPageInfo.size()>0){
 	                		   for(int j=0;j<tPageInfo.size();j++){
 	                			 Element tPageInfoData = (Element) tPageInfo.get(j);
 	                			 String tPageCode = tPageInfoData.getChildText("pagecode");
 	                			if("".equals(tPageCode)||tPageCode==null){buildError("dealCaseRegister","第"+(i+1)+"位客户的影像序号pagecode不能为空"); return false;}
 	                			 String tPageName = tPageInfoData.getChildText("PAGENAME");
 	                			if("".equals(tPageName)||tPageName==null){buildError("dealCaseRegister","第"+(i+1)+"位客户的影像名PAGENAME不能为空"); return false;}
 	                			 String tPageSuffix = tPageInfoData.getChildText("PAGESUFFIX");
 	                			if("".equals(tPageSuffix)||tPageSuffix==null){buildError("dealCaseRegister","第"+(i+1)+"位客户的影像扩展名PAGESUFFIX不能为空"); return false;}            
 	   	                    
 	                			 String tPicPath = tPageInfoData.getChildText("PICPATH");
 	                			 
 	                			 if(!"".equals(tPageCode) && tPageCode!=null &&!"null".equals(tPageCode) 
 	                					 &&!"".equals(tPageName)&&tPageName!=null&&!"null".equals(tPageName)){
 	                				 ExeSQL aExeSQL = new ExeSQL();
 	                				 String tFilePathUrl=aExeSQL.
 	                						 getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='UrlPicPath'");
 	                				 String tFilePathIp=aExeSQL.
 	                						 getOneValue("select codename from ldcode where codetype='ZBClaim' and code='ServerIp'");
 	                				 tFilePathIp=tFilePathIp + PubFun.getCurrentDate()+"/";
 	                				 String tFilePath=aExeSQL.
 	                						 getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='ServerIp'");
 	                				 tFilePath =tFilePath +  PubFun.getCurrentDate()+"/";
 	                				 
 	                				 //分页信息
 	                				 ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
 	                				 String strPageID = getMaxNo("PageID");
	 	   	    					 tES_DOC_PAGESSchema.setHostName(tFilePathUrl);
	 	   	    					 tES_DOC_PAGESSchema.setPageCode(tPageCode);
	 	   	    					 tES_DOC_PAGESSchema.setPageName(tPageName);
	 	   	    					 tES_DOC_PAGESSchema.setPageSuffix(".jpg");
	 	   	    					 tES_DOC_PAGESSchema.setPageFlag("1");
	 	   	    					 tES_DOC_PAGESSchema.setPageType("0");
	 	   	    					 tES_DOC_PAGESSchema.setPicPath(tFilePath);
	 	   	    					 tES_DOC_PAGESSchema.setPicPathFTP(tFilePathIp);
	 	   	    					 tES_DOC_PAGESSchema.setManageCom(mManageCom);
	 	   	    					 tES_DOC_PAGESSchema.setOperator(mOperator);
	 	   	    					 tES_DOC_PAGESSchema.setPageID(strPageID);
	 	   	    					 tES_DOC_PAGESSchema.setDocID(strDocID);
	 	   	    					 tES_DOC_PAGESSchema.setMakeDate(mCurrentDate);
	 	   	    					 tES_DOC_PAGESSchema.setModifyDate(mCurrentDate);
	 	   	    					 tES_DOC_PAGESSchema.setMakeTime(mCurrentTime);
	 	   	    					 tES_DOC_PAGESSchema.setModifyTime(mCurrentTime);
	 	   	    					 mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
 
 	                			 }
 	                			 
 	                		   }
 	                		   ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
 	                		   tES_DOC_RELATIONSchema.setBussNo(tCaseNo);
 	  	            		   tES_DOC_RELATIONSchema.setDocCode(tCaseNo);
 	  	            		   tES_DOC_RELATIONSchema.setBussNoType("21");
 	  	            		   tES_DOC_RELATIONSchema.setDocID(strDocID);
 	  	            		   tES_DOC_RELATIONSchema.setBussType(tES_DOC_MainSchema.getBussType());
 	  	            		   tES_DOC_RELATIONSchema.setSubType(tES_DOC_MainSchema.getSubType());
 	  	            		   tES_DOC_RELATIONSchema.setRelaFlag("0");
 	  	            		   mES_DOC_RELATIONSet.add(tES_DOC_RELATIONSchema);
 	                		   
 	                	   }
 	                   } 
                     }
				  }else{
                 	 buildError("dealData()","获取理赔信息出错!");
                     mDealState = false;
                     return false;
                  }
			  }
		  }else{
	            buildError("dealData()", "报文中没有理赔信息!");
	            mDealState = false; 
	            return false;
	        }
		 return true;
	 }
	 
	 private String checkNode(
			LLZBMajorDiseasesCaseParser tLLZBMajorDiseasesCaseParser, int i) {
		// TODO Auto-generated method stub
		String err="";
 //不为空校验
		 if("".equals(tLLZBMajorDiseasesCaseParser.getIDNO())||tLLZBMajorDiseasesCaseParser.getIDNO()==null){err=err+"@@"+ "第"+(i+1)+"位客户的身份证号IDNO不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getRGTNO())||tLLZBMajorDiseasesCaseParser.getRGTNO()==null){err=err+"@@"+ "第"+(i+1)+"位客户的批次号RGTNO不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getREALHOSPDATE())||tLLZBMajorDiseasesCaseParser.getREALHOSPDATE()==null){err=err+"@@"+"第"+(i+1)+"位客户的住院天数REALHOSPDATE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getSEX())||tLLZBMajorDiseasesCaseParser.getSEX()==null){err=err+"@@"+ "第"+(i+1)+"位客户的性别Sex不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getCUSTOMERNAME())||tLLZBMajorDiseasesCaseParser.getCUSTOMERNAME()==null){err=err+"@@"+ "第"+(i+1)+"客户的姓名CUSTOMERNAME不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getHOSPITALNAME())||tLLZBMajorDiseasesCaseParser.getHOSPITALNAME()==null){err=err+"@@"+"第"+(i+1)+"位客户的医院名称HOSPITALNAME不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getHOSPITALNAMECODE())||tLLZBMajorDiseasesCaseParser.getHOSPITALNAMECODE()==null){err=err+"@@"+"第"+(i+1)+"位客户的医院编码HOSPITALNAMECODE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getFEETYPE())||tLLZBMajorDiseasesCaseParser.getFEETYPE()==null){err=err+"@@"+"第"+(i+1)+"位客户的就诊类别FEETYPE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getRECEIPTNO())||tLLZBMajorDiseasesCaseParser.getRECEIPTNO()==null){err=err+"@@"+"第"+(i+1)+"位客户的账单号码RECEIPTNO不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getFEEDATE())||tLLZBMajorDiseasesCaseParser.getFEEDATE()==null){err=err+"@@"+"第"+(i+1)+"位客户的账单日期FEEDATE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getHOSPSTARTDATE())||tLLZBMajorDiseasesCaseParser.getHOSPSTARTDATE()==null){err=err+"@@"+"第"+(i+1)+"位客户的入院日期HOSPSTARTDATE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getHOSPENDDATE())||tLLZBMajorDiseasesCaseParser.getHOSPENDDATE()==null){err=err+"@@"+"第"+(i+1)+"位客户的出院日期HOSPENDDATE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getAPPLYAMNT())||tLLZBMajorDiseasesCaseParser.getAPPLYAMNT()==null){err=err+"@@"+"第"+(i+1)+"位客户的费用总额APPLYAMNT不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getGETLIMIT())||tLLZBMajorDiseasesCaseParser.getGETLIMIT()==null){err=err+"@@"+"第"+(i+1)+"位客户的起付钱GETLIMIT不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getORIGINFLAG())||tLLZBMajorDiseasesCaseParser.getORIGINFLAG()==null){err=err+"@@"+"第"+(i+1)+"位客户的医疗标志ORIGINFLAG不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getGETDUTYCODE())||tLLZBMajorDiseasesCaseParser.getGETDUTYCODE()==null){err=err+"@@"+"第"+(i+1)+"位客户的给付责任编码GETDUTYCODE不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getGETDUTYKIND())||tLLZBMajorDiseasesCaseParser.getGETDUTYKIND()==null){err=err+"@@"+"第"+(i+1)+"位客户的给负责任类型GETDUTYKIND不能为空";}
		 if("".equals(tLLZBMajorDiseasesCaseParser.getCLAIMNO())||tLLZBMajorDiseasesCaseParser.getCLAIMNO()==null){err=err+"@@"+"第"+(i+1)+"位客户的平台理赔号CLAIMNO不能为空";}
	//日期格式校验，应为YYYY-MM-DD	 
		PubFun pf=new PubFun();
		 if(!pf.checkDateForm(tLLZBMajorDiseasesCaseParser.getFEEDATE())&&!"".equals(tLLZBMajorDiseasesCaseParser.getFEEDATE())&&tLLZBMajorDiseasesCaseParser.getFEEDATE()!=null){err=err+"@@"+"第"+(i+1)+"位客户的账单日期FEEDATE为DATE日期类型格式应为YYYY-MM-DD";}
		 if(!pf.checkDateForm(tLLZBMajorDiseasesCaseParser.getHOSPSTARTDATE())&&!"".equals(tLLZBMajorDiseasesCaseParser.getHOSPSTARTDATE())&&tLLZBMajorDiseasesCaseParser.getHOSPSTARTDATE()!=null){err=err+"@@"+"第"+(i+1)+"位客户的入院日期HOSPSTARTDATE为DATE日期类型格式应为YYYY-MM-DD";}
		 if(!pf.checkDateForm(tLLZBMajorDiseasesCaseParser.getHOSPENDDATE())&&!"".equals(tLLZBMajorDiseasesCaseParser.getHOSPENDDATE())&&tLLZBMajorDiseasesCaseParser.getHOSPENDDATE()!=null){err=err+"@@"+"第"+(i+1)+"位客户的出院日期HOSPENDDATE为DATE日期类型格式应为YYYY-MM-DD";}
		 if(!pf.checkDateForm(tLLZBMajorDiseasesCaseParser.getACCDATE())&&!"".equals(tLLZBMajorDiseasesCaseParser.getACCDATE())&&tLLZBMajorDiseasesCaseParser.getACCDATE()!=null){err=err+"@@"+"第"+(i+1)+"位客户的发生日期ACCDATE为DATE日期类型格式应为YYYY-MM-DD";}
		 if(!pf.checkDateForm(tLLZBMajorDiseasesCaseParser.getMAKEDATE())&&!"".equals(tLLZBMajorDiseasesCaseParser.getMAKEDATE())&&tLLZBMajorDiseasesCaseParser.getMAKEDATE()!=null){err=err+"@@"+"第"+(i+1)+"位客户的经办时间MAKEDATE为DATE日期类型格式应为YYYY-MM-DD";}

		 
         return "".equals(err)||err==null ? "" : err.substring(2);
	}

	/***
	  *生成流水号信息
	  * 
	  */
	 
	 private String getMaxNo(String cNoType){
		 String strNo =PubFun1.CreateMaxNo(cNoType, 1);
		 if (strNo.equals("") || strNo.equals("0")) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "LLZBCaseRegister";
				tError.functionName = "getReturnData";
				tError.errorNo = "-90";
				tError.errorMessage = "生成流水号失败!";
				this.mErrors.addOneError(tError);
				strNo = "";
		 }
		 return strNo; 	 
	 }
	 
	 
	 /***
	  * 追加错误信息
	  * @param args
	  */
	 private void buildError(String szFunc,String szErrMsg){
		    CError cError = new CError();
	        cError.moduleName = "LLZBCaseRegister";
	        cError.functionName = szFunc;
	        cError.errorMessage = szErrMsg;
	        System.out.println(szFunc + "--" + szErrMsg);
	        this.mErrors.addOneError(cError);
		 
	 }
	 
	public static void main(String[] args) {
	 	Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/理赔报文/浙江外包/ZJLP_007.xml"), "GBK");
            LLZBCaseRegister tBusinessDeal = new LLZBCaseRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

}
