package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LLCaseBackDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.db.LLSocialClaimUserDB;
import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.llcase.ClaimUnderwriteBL;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.lis.llcase.UnitClaimSaveBL;
import com.sinosoft.lis.llcase.genCaseInfoBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseBackSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSocialClaimUserSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLCaseBackSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseOpTimeSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 自动立案并结案</p>
 *
 * <p>Description: 生成案件至结案状态</p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zjd
 * @version 1.0
 */
public class LLTJMajorDiseasesCaseRegister implements ServiceInterface
{
    
    public LLTJMajorDiseasesCaseRegister(){}
    
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private String mErrorMessage = "";  //错误描述    
    
    /**
     * 天津大病默认配置
     */
    private String mManageCom="86120000";   //管理机构
    private String mOperator="ss1201";   //管理机构
    
    private List tList = new ArrayList();//返回信息合集
    //报文头信息
    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private String mResponseCode = "1";     //返回类型代码    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    private String mDealType = "2";         //处理类型 1 实时 2非实时 
    private boolean tCancelFlag = true;		//每次运行时将前一批案件撤件
    
    //基本信息
    private int mCaseNum = 0;
    private String mGRPCONTNO = "";       //团体保单号    
    private String mAPPAMNT = "";         //申报金额
    /** 社保保单标志 0：非社保；1：社保*/
    private String mSocialSecurity = "0";
    private String mReturnMessage = "";//错误提示集合
    
    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();//医保通案件处理表信息
    
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    private LLCaseSet mLLCaseSet = new LLCaseSet();
    private LLSocialClaimUserSchema mLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
    private LLCaseSet mbackCaseSet = new LLCaseSet();//案件回退集合
    
    private GlobalInput tG=new GlobalInput();
    
   
    //批次号
    private String mRgtNo = "";
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
    //处理结果标记
    private boolean mDealState=true;  
    private Element mLLCaseList;

    public Document service(Document pInXmlDoc)
    {
    	//TODO 开始webservice 
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{
                        //保存信息到LLHospCase
                        /*PubSubmit tPubSubmit = new PubSubmit();
                        VData tVData = new VData();
                        MMap tMMap= new MMap();
                        tMMap.put(mLLHospCaseSet, "INSERT");
                        tVData.add(tMMap);
                        tPubSubmit.submitData(tVData, "INSERT");*/
                    }
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	mInXmlDoc = createXML();
        }
        return mInXmlDoc;
    }
    
    
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJMajorDiseasesCaseRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
    
        
        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mLLCaseList = tBodyData.getChild("LLCASELIST");          //理赔信息    
        tG.ManageCom=mManageCom;
        tG.Operator=mOperator;
       
        return true;

    }
    
    
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
            return false;
        }

        if (!"DB02".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误!");
            return false;
        }

        if (!"DB02".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86120000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误!");
            return false;
        }
                     
        return true;

    }
    
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
    	//TODO Auto-generated method stub
    	//1.受理到检录
    	if(!dealCaseRegister()){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}else {
        		 //保存信息到LLHospCase
                PubSubmit tPubSubmit = new PubSubmit();
                VData tVData = new VData();
                MMap tMMap= new MMap();
                tMMap.put(mLLHospCaseSet, "INSERT");
                tVData.add(tMMap);
                tPubSubmit.submitData(tVData, "INSERT");
        	}
    	}
        //2.检录到理算,包含核赔
    	if(!batchClaimCal(mRgtNo)){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}
    	}
    	//3.审批审定
    	if(!simpleClaimAudit()){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}
    	}
    	
         
        return true;
    }
    
    /**
     * 直接对整个批次进行审批审定
     * @return
     */
    private boolean simpleClaimAudit() {
    	//TODO Auto-generated method stub
		//1.数据校验
    	LLSocialClaimUserDB tLLSocialClaimUserDB = new LLSocialClaimUserDB();
    	tLLSocialClaimUserDB.setUserCode(tG.Operator);
        if(!tLLSocialClaimUserDB.getInfo()){
            // @@错误处理
            buildError("simpleClaimAudit()", "您不是理赔人，无权作审定操作");
            mDealState = false; 
            return false;
        }

        
        //2.准备数据
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        mLLCaseSet = tLLCaseDB.query();
        if(mLLCaseSet.size()<=0){
            buildError("simpleClaimAudit()", "没有待审批的案件!");
            mDealState = false; 
            return false;
        }
        for(int i=1;i<=mLLCaseSet.size();i++){
            LLCaseSchema tcase = mLLCaseSet.get(i);               
            if (!dealClaim(tcase)) {
                continue;
            }
            tcase.setRgtState("04");
            tcase.setHandler(tG.Operator);
            if(!UWCase(tcase))
                continue;
        }
        
        //3.对错误案件进行回退
        CaseBack(mbackCaseSet);
        
        //4.更新LLregister
        mInputData.clear();
        MMap map = new MMap();
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        String tRgtNo = mLLRegisterSchema.getRgtNo();
        tLLRegisterDB.setRgtNo(tRgtNo);
        if (tLLRegisterDB.getInfo()) {
            LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
            tLLRegisterSchema = tLLRegisterDB.getSchema();
            LLCaseDB aLLCaseDB = new LLCaseDB();
            LLCaseSet tLLCaseSet = new LLCaseSet();
            String tsql =
                    "select * from llcase where rgtstate not in('09','14') and rgtno='"
                    + tRgtNo + "'";
            System.out.println(tsql);
            tLLCaseSet = aLLCaseDB.executeQuery(tsql);
            System.out.println(tLLRegisterSchema.getRgtState());
            if (tLLCaseSet != null && tLLCaseSet.size() > 0) {
                if (tLLRegisterSchema.getRgtState().equals("03"))
                    tLLRegisterSchema.setRgtState("02");
            } else {
                if (tLLRegisterSchema.getRgtState().equals("02"))
                    tLLRegisterSchema.setRgtState("03");
            }
            map.put(tLLRegisterSchema, "UPDATE");
            mInputData.add(map);
            mResult.add(mReturnMessage);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, null)) {
                CError.buildErr(this,"数据提交失败!");
                return false;
            }
        }
    	
		return true;
	}
    
    /**
     * 对批次下个案进行简单的理算确认
     * @param aCaseSchema
     * @return
     */
    private boolean dealClaim(LLCaseSchema aCaseSchema) {
        VData tvdata = new VData();
        UnitClaimSaveBL tUnitClaimSaveBL = new UnitClaimSaveBL();
        tvdata.add(aCaseSchema);
        tvdata.add(tG);
        if(!(tUnitClaimSaveBL.submitData(tvdata,"BATCH"))){
            CErrors tError = tUnitClaimSaveBL.mErrors;
            String ErrMessage = tError.getFirstError();
            aCaseSchema.setCancleRemark(ErrMessage);
            aCaseSchema.setCancleReason("4");
            mbackCaseSet.add(aCaseSchema);
            //@@错误处理
            mReturnMessage += "<br>案件"+aCaseSchema.getCaseNo()
                    +"信息不全，被自动回退到批改信箱。";
            return false;
        }
        return true;
    }

    /**
     * 批次下个案循环审批审定
     * @param aCaseSchema
     * @return
     */
    private boolean UWCase(LLCaseSchema aCaseSchema){
        VData tvdata = new VData();
        //此处为循环调用核赔类的入口，在此处判断此批次下案件是否为社保案件，若为社保案件，传入社保批次标志 
        //此处虽为社保保单,但是由于默认操作人为cm****,此处标记传入"商团"
        mSocialSecurity = "1";
        ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
        tvdata.clear();
        tvdata.add(aCaseSchema);
        tvdata.add(tG);
        //将社保标记传入ClaimUnderwriteBL
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("mSocialSecurity", mSocialSecurity);
        tvdata.add(tTransferData);
        System.out.println("社保标记为："+mSocialSecurity);
        if(!tClaimUnderwriteBL.submitData(tvdata,"BATCH||UW")){
            CErrors tError = tClaimUnderwriteBL.mErrors;
            String ErrMessage = tError.getFirstError();
            mReturnMessage += "<br>案件" + aCaseSchema.getCaseNo()
                    + "审批失败,原因是:"+ErrMessage;
            return false;
        }
        VData tResultData = tClaimUnderwriteBL.getResult();
        String strResult = (String) tResultData.getObjectByObjectName("String", 0);
        mReturnMessage+="<br>"+aCaseSchema.getCaseNo()+strResult;
        return true;
    }
    
    /**
     * 案件回退
     * 1。审定人发现案件错误，做案件回退
     * 2。系统审定操作不能通过，做自动回退
     * @return boolean
     */
    private boolean CaseBack(LLCaseSet aLLCaseSet) {
        System.out.println("天津大病-回退案件数:"+aLLCaseSet.size());
        MMap map = new MMap();
        LLCaseBackSet tLLCaseBackSet = new LLCaseBackSet();
        LLCaseBackSet oLLCaseBackSet = new LLCaseBackSet();
        LLCaseOpTimeSet tLLCaseOpTimeSet = new LLCaseOpTimeSet();
        boolean tflag= false;
        for(int i=1;i<=aLLCaseSet.size();i++){
            LLCaseSchema tLLCaseSchema = aLLCaseSet.get(i);
            LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
            String oRgtState = ""+tLLCaseSchema.getRgtState();
            String oRgtStateName = "";
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCode(oRgtState);
            tLDCodeDB.setCodeType("llrgtstate");
            if(tLDCodeDB.getInfo())
                oRgtStateName = tLDCodeDB.getCodeName();
            if(!oRgtState.equals("03")&&!oRgtState.equals("04")&&
               !oRgtState.equals("05")&&!oRgtState.equals("06")&&
               !oRgtState.equals("09")&&!oRgtState.equals("10")){
                //写报错信息返回
                mReturnMessage+="案件"+tLLCaseSchema.getCaseNo()+"当前为"
                        +oRgtStateName+",不能做回退操作<br>";
                continue;
            }
            String tLimit = PubFun.getNoLimit(this.tG.ManageCom);
            String CaseBackNo = PubFun1.CreateMaxNo("CaseBack", tLimit);

            tLLCaseBackSchema.setBeforState(oRgtState);
            tLLCaseBackSchema.setCaseBackNo(CaseBackNo);
            tLLCaseBackSchema.setAviFlag("Y");
            tLLCaseBackSchema.setRgtNo(tLLCaseSchema.getRgtNo());
            tLLCaseBackSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackSchema.setOHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setNHandler(tLLCaseSchema.getHandler());
            tLLCaseBackSchema.setAfterState("01");
            tLLCaseBackSchema.setReason(tLLCaseSchema.getCancleReason());
            tLLCaseBackSchema.setRemark(tLLCaseSchema.getCancleRemark());
            tLLCaseBackSchema.setMngCom(tG.ManageCom);
            tLLCaseBackSchema.setOperator(tG.Operator);
            tLLCaseBackSchema.setMakeDate(mCurrentDate);
            tLLCaseBackSchema.setMakeTime(mCurrentTime);
            tLLCaseBackSchema.setModifyDate(mCurrentDate);
            tLLCaseBackSchema.setModifyTime(mCurrentTime);

            tLLCaseBackSet.add(tLLCaseBackSchema);
            
            //#740 批次导入后、案件回退时增加轨迹表数据
            //add by GY 2013-1-14
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
            tLLCaseOpTimeSchema.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseOpTimeSchema.setRgtState("01");
            tLLCaseOpTimeSchema.setSequance("1");//导致案件的第一次回退必须发生在这里
            tLLCaseOpTimeSchema.setManageCom(tG.ManageCom);
            tLLCaseOpTimeSchema.setOperator(tG.Operator);
            tLLCaseOpTimeSchema.setStartDate(mCurrentDate);
            tLLCaseOpTimeSchema.setStartTime(mCurrentTime);
            tLLCaseOpTimeSchema.setOpTime("0:00:00");
            
            tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
            
            //保存后当前回退记录生效，需要把过去的回退记录设为过期
            LLCaseBackDB tLLCaseBackDB = new LLCaseBackDB();
            tLLCaseBackDB.setCaseNo(tLLCaseSchema.getCaseNo());
            tLLCaseBackDB.setAviFlag("Y");
            LLCaseBackSet tempLLCaseBackSet = new LLCaseBackSet();
            tempLLCaseBackSet = tLLCaseBackDB.query();
            for(int x=1;x<=tempLLCaseBackSet.size();x++){
                tempLLCaseBackSet.get(x).setAviFlag("");
                tempLLCaseBackSet.get(x).setModifyDate(mCurrentDate);
                tempLLCaseBackSet.get(x).setModifyTime(mCurrentTime);
            }
            oLLCaseBackSet.add(tempLLCaseBackSet);
            aLLCaseSet.get(i).setRgtState("01");
            aLLCaseSet.get(i).setCancleReason("");
            aLLCaseSet.get(i).setCancleRemark("");
            aLLCaseSet.get(i).setUWer(tG.Operator);
            aLLCaseSet.get(i).setDealer(tG.Operator);
            aLLCaseSet.get(i).setModifyDate(mCurrentDate);
            aLLCaseSet.get(i).setModifyTime(mCurrentTime);
            tflag = true;
        }
        if(tflag){
            map.put(aLLCaseSet, "UPDATE");
            map.put(tLLCaseBackSet, "INSERT");
            map.put(oLLCaseBackSet, "UPDATE");
            map.put(tLLCaseOpTimeSet, "INSERT");
        }
        this.mInputData.clear();
        mInputData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, null)) {
            CError.buildErr(this,"数据提交失败!");
            return false;
        }
        return true;
    }

	/**
     * 分案受理
     * @return
     */
    private boolean dealCaseRegister() {
		// TODO Auto-generated method stub
    	System.out.println(tG.Operator+"===="+tG.ManageCom);
    	ExeSQL tExeSQL = new ExeSQL();
    	String claimno=""; //记录曾经跑过的平台理赔号
        if(mLLCaseList.getChildren("LLCASE_DATA").size()>0){
           
            for(int i=0;i<mLLCaseList.getChildren("LLCASE_DATA").size();i++){
                Element mLLCaseInfo=(Element)mLLCaseList.getChildren("LLCASE_DATA").get(i);
                if(mLLCaseInfo!=null){
                    //调用报文解析类将数据封装到类中
                	LLTJMajorDiseasesCaseParser tLLTJMajorDiseasesCaseParser=new LLTJMajorDiseasesCaseParser(mLLCaseInfo);
                	 //交易编码mTransactionNum+平台理赔号CLAIMNO校验唯一
                	//1.获取以上提到的两字段
                	//2.找到以上两字段存储的位置
                	//3.查询后校验表中是否存在，如果存在就循环下一个理赔案件
                    String [] claimnos=claimno.trim().split("   ");
                    System.out.println("=================跑过的平台理赔号："+claimno+"长度为："+claimnos.length);
                    boolean boolFlag=false;
                    for (int j = 0; j < claimnos.length; j++) {
						
                    	if(tLLTJMajorDiseasesCaseParser.getCLAIMNO().equals(claimnos[j])){
                    		System.out.println("这个平台理赔号已经跑过了："+tLLTJMajorDiseasesCaseParser.getCLAIMNO());
                    		boolFlag=true;
                    	}
					}
                    if(boolFlag){
                    	buildError("dealData()", "同一报文中平台理赔号出现重复，交易编码："+mTransactionNum+"，平台理赔号："+tLLTJMajorDiseasesCaseParser.getCLAIMNO());
                    	System.out.println("================同一报文中平台理赔号出现重复，交易编码："+mTransactionNum+"，平台理赔号："+tLLTJMajorDiseasesCaseParser.getCLAIMNO()+"========");
                    	return false;
                    }
                    String pk=" select count(1) from LLHospCase llh,llcase llc where 1 =1 "
//                    		+ " and AppTranNo='"+mTransactionNum+"' "
                    		+ " and llh.caseno=llc.caseno "
                    		+ " and llc.rgtstate!='14' "
                    		+ " and llh.ClaimNo='"+tLLTJMajorDiseasesCaseParser.getCLAIMNO()+"' and casetype='13' with ur "	;
                    String number=tExeSQL.getOneValue(pk);
                	System.out.println("================SQL=======【"+pk+"】===========");
                	System.out.println("================数量："+number);
                    if(!"0".equals(number)){
                    	buildError("dealData()", "交易编码+平台理赔号出现重复，表中已存在，交易编码："+mTransactionNum+"，平台理赔号："+tLLTJMajorDiseasesCaseParser.getCLAIMNO());
                    	System.out.println("================交易编码+平台理赔号出现重复，表中已存在，交易编码："+mTransactionNum+"，平台理赔号："+tLLTJMajorDiseasesCaseParser.getCLAIMNO()+"=========");
                    	return false;
                    }
                	
                    LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                    LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
                    LLSecurityReceiptSchema tLLSecurityReceiptSchema = new
                            LLSecurityReceiptSchema();
                    LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
                    LLCaseCureSchema tLLCaseCureSchema=new LLCaseCureSchema();
                    LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
                    
                    VData tVData = new VData();

                    genCaseInfoBL tgenCaseInfoBL = new genCaseInfoBL();

                    //1.通过rgtno查询该批次的团单号,开始处理分案数据
                    mRgtNo = tLLTJMajorDiseasesCaseParser.getRGTNO();
                    
                    String tGrpNoSql = "select rgtobjno from llregister where rgtno='"+mRgtNo+"' with ur";
                    SSRS tGrpNoSSRS=tExeSQL.execSQL(tGrpNoSql);
                    if(tGrpNoSSRS.getMaxRow() <= 0){
                        buildError("dealData()", "未提供批次申请号,查询信息失败");
                        mDealState = false;                   	
                    }
                    LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                    tLLRegisterDB.setRgtNo(mRgtNo);
                   if (!tLLRegisterDB.getInfo()) {
                  // @@错误处理
                	  buildError("dealData()", "团体立案信息查询失败!");
                	  mDealState = false; 
                	  return false;
                   }
                    mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
                    String declineflag = "" + mLLRegisterSchema.getDeclineFlag();//撤件标志
//                    String checkRgtstate = "select codename from ldcode where codetype='' and code='' with ur";
//                    SSRS checkRgtSSRS =  tExeSQL.execSQL(checkRgtstate);
//                    if("1".equals(checkRgtSSRS.GetText(1, 1))){
	                    if ("1".equals(declineflag)) {
	                        // @@错误处理
	                        buildError("dealData()", "该团体申请已撤件，不能再录入个人客户!");
	                        mDealState = false; 
	                        return false;
	                    }
	                    if ("03".equals(mLLRegisterSchema.getRgtState())) {
	                        // @@错误处理
	                        buildError("dealData()", "团体申请下所有个人案件已经结案完毕，" +
	                                "不能再录入个人客户!");
	                        mDealState = false; 
	                        return false;
	                    }
	                    if ("04".equals(mLLRegisterSchema.getRgtState())) {
	                        // @@错误处理
	                        buildError("dealData()", "团体申请下所有个人案件已经给付确认，" +
	                                "不能再录入个人客户!");
	                        mDealState = false; 
	                        return false;
	                    }
//                    }
                    mGRPCONTNO = tGrpNoSSRS.GetText(1, 1);
                    tLLCaseSchema.setCaseNo("");
                    tLLCaseSchema.setRgtNo(mRgtNo);
                    tLLCaseSchema.setAccidentDate(tLLTJMajorDiseasesCaseParser.getACCDATE());
                    tLLCaseSchema.setRgtType("1");//1-申请类
                    tLLCaseSchema.setRgtState("03");//检录状态
                    
                    tLLCaseSchema.setPhone(tLLTJMajorDiseasesCaseParser.getGETDUTYCODE());//给付责任编码,暂存在Phone字段中
                    //获取分单被保人5要素
                  //#3501 报文传入客户号，先从被保人表中查， 证件号码与证件类型均从被保人表获取，报文未传入客户号，根据传入的证件号去查 star
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getCUSTOMERNO()) && tLLTJMajorDiseasesCaseParser.getCUSTOMERNO()!=null){
                    	String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGRPCONTNO+"' and a.insuredno='"+tLLTJMajorDiseasesCaseParser.getCUSTOMERNO()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGRPCONTNO+"' and b.insuredno='"+tLLTJMajorDiseasesCaseParser.getCUSTOMERNO()+"' ";
                        SSRS tSSRS=tExeSQL.execSQL(tSqlIn);
                        if(tSSRS.getMaxRow()>0){
                            tLLCaseSchema.setCustomerNo(tSSRS.GetText(1, 1));
                            tLLFeeMainSchema.setCustomerNo(tSSRS.GetText(1, 1));
                            tLLCaseSchema.setCustBirthday(tSSRS.GetText(1, 4));
                            tLLCaseSchema.setIDNo(tSSRS.GetText(1, 5));
                            tLLCaseSchema.setIDType(tSSRS.GetText(1, 8));//#3501 保单登记平台客户证件类型，被保人表获取
                            if("".equals(tSSRS.GetText(1, 8))||tSSRS.GetText(1, 8)==null||tSSRS.GetText(1, 8)=="null"){
                              	 tLLCaseSchema.setIDType("4");
                              }
                        }else{
                            buildError("dealData()", "客户查询失败!");
                            mDealState = false; 
                        } 
                    }else{
                    	String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGRPCONTNO+"' and a.idno='"+tLLTJMajorDiseasesCaseParser.getIDNO()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGRPCONTNO+"' and b.idno='"+tLLTJMajorDiseasesCaseParser.getIDNO()+"' ";
                        SSRS tSSRS=tExeSQL.execSQL(tSqlIn);
                        if(tSSRS.getMaxRow()>0){
                            tLLCaseSchema.setCustomerNo(tSSRS.GetText(1, 1));
                            tLLCaseSchema.setCustBirthday(tSSRS.GetText(1, 4));
                            tLLCaseSchema.setIDNo(tSSRS.GetText(1, 5));
                            tLLCaseSchema.setIDType(tSSRS.GetText(1, 8));//#3501 保单登记平台客户证件类型，被保人表获取
                            if("".equals(tSSRS.GetText(1, 8))||tSSRS.GetText(1, 8)==null||tSSRS.GetText(1, 8)=="null"){
                              	 tLLCaseSchema.setIDType("4");
                               }
                        }else{
                            buildError("dealData()", "未提供客户号，社保号或身份证号等信息，无法确认客户身份!");
                            mDealState = false; 
                        }  
                    }
                    //#3501 报文传入客户号，先从被保人表中查， 证件号码与证件类型均从被保人表获取，报文未传入客户号，根据传入的证件号去查  end

                    
                    tLLCaseSchema.setCustomerName(tLLTJMajorDiseasesCaseParser.getCUSTOMERNAME());
        
//                    tLLCaseSchema.setIDType("0");
//                    tLLCaseSchema.setIDNo(tLLTJMajorDiseasesCaseParser.getIDNO());                  
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getMOBILEPHONE()) && tLLTJMajorDiseasesCaseParser.getMOBILEPHONE()!=null){
                        tLLCaseSchema.setMobilePhone(tLLTJMajorDiseasesCaseParser.getMOBILEPHONE());
                    }
                                      
                    tLLCaseSchema.setCustomerSex(tLLTJMajorDiseasesCaseParser.getSEX());
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getREMARK()) && tLLTJMajorDiseasesCaseParser.getREMARK()!=null){
                        tLLCaseSchema.setRemark(tLLTJMajorDiseasesCaseParser.getREMARK());
                    }                   
                    
                    tLLCaseSchema.setRgtState("03"); //案件状态
                    tLLCaseSchema.setSurveyFlag("0");
                    tLLCaseSchema.setAccdentDesc("天津大病");
                    tLLCaseSchema.setSurveyFlag("0");
                    
                    tLLCaseSchema.setInHospitalDays(tLLTJMajorDiseasesCaseParser.getREALHOSPDATE());
                    
                    tLLCaseSchema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
                    
                    String tbankSql=" select li.BankCode,li.BankAccNo,li.AccName  from lcinsured li where li.insuredno ='"+tLLCaseSchema.getCustomerNo()+"'  and li.grpcontno ='"+mGRPCONTNO+"'  ";
                    SSRS bankSSRS=tExeSQL.execSQL(tbankSql);
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getBANKCODE()) && tLLTJMajorDiseasesCaseParser.getBANKCODE()!=null){
                        tLLCaseSchema.setBankCode(tLLTJMajorDiseasesCaseParser.getBANKCODE());
                    }else{
                        if(bankSSRS.getMaxRow()>0){
                            tLLCaseSchema.setBankCode(bankSSRS.GetText(1, 1));
                        }
                    }
                   
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getBANKACCNO()) && tLLTJMajorDiseasesCaseParser.getBANKACCNO()!=null){
                        tLLCaseSchema.setBankAccNo(tLLTJMajorDiseasesCaseParser.getBANKACCNO());
                    }else{
                        if(bankSSRS.getMaxRow()>0){
                            tLLCaseSchema.setBankAccNo(bankSSRS.GetText(1, 2));
                        } 
                    }
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getACCNAME()) && tLLTJMajorDiseasesCaseParser.getACCNAME()!=null){
                        tLLCaseSchema.setAccName(tLLTJMajorDiseasesCaseParser.getACCNAME());
                    }else{
                        if(bankSSRS.getMaxRow()>0){
                            tLLCaseSchema.setAccName(bankSSRS.GetText(1, 3));
                        }
                    }                   
                    
                    //开始处理账单信息
                                   
                    tLLFeeMainSchema.setRgtNo(mRgtNo);
                    tLLFeeMainSchema.setCaseNo("");
                    
                    //赋值可以使用导入的getdutykind
                    tLLFeeMainSchema.setOldMainFeeNo(tLLTJMajorDiseasesCaseParser.getGETDUTYKIND());
                    
                    tLLFeeMainSchema.setCustomerName(tLLTJMajorDiseasesCaseParser.getCUSTOMERNAME());
                    tLLFeeMainSchema.setCustomerSex(tLLTJMajorDiseasesCaseParser.getSEX());
                    tLLFeeMainSchema.setInsuredStat(tLLTJMajorDiseasesCaseParser.getINSUREDSTAT());
                    tLLFeeMainSchema.setHospitalCode(tLLTJMajorDiseasesCaseParser.getHOSPITALNAMECODE());
                    tLLFeeMainSchema.setHospitalName(tLLTJMajorDiseasesCaseParser.getHOSPITALNAME());
                    tLLFeeMainSchema.setSecurityNo(tLLTJMajorDiseasesCaseParser.getSECURYTINO());
                   
                    tLLFeeMainSchema.setFeeAtti("4");
                    tLLFeeMainSchema.setSelfAmnt(tLLTJMajorDiseasesCaseParser.getSELFAMNT());
                    tLLFeeMainSchema.setFeeType(tLLTJMajorDiseasesCaseParser.getFEETYPE());
                    tLLFeeMainSchema.setReceiptNo(tLLTJMajorDiseasesCaseParser.getRECEIPTNO());
                    tLLFeeMainSchema.setFeeDate(tLLTJMajorDiseasesCaseParser.getFEEDATE());
                    
                    tLLFeeMainSchema.setHospStartDate(tLLTJMajorDiseasesCaseParser.getHOSPSTARTDATE());
                    tLLFeeMainSchema.setHospEndDate(tLLTJMajorDiseasesCaseParser.getHOSPENDDATE());
                    tLLFeeMainSchema.setRealHospDate(tLLTJMajorDiseasesCaseParser.getREALHOSPDATE());
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getINPATIENTNO()) && tLLTJMajorDiseasesCaseParser.getINPATIENTNO()!=null){
                        tLLFeeMainSchema.setInHosNo(tLLTJMajorDiseasesCaseParser.getINPATIENTNO());
                    }
                    
                    //第三方
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getOTHERORGANAMNT()) && tLLTJMajorDiseasesCaseParser.getOTHERORGANAMNT()!=null){
                        tLLFeeMainSchema.setOtherOrganAmnt(tLLTJMajorDiseasesCaseParser.getOTHERORGANAMNT());
                    }
                     
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getREFUSEAMNT()) && tLLTJMajorDiseasesCaseParser.getREFUSEAMNT()!=null){
                        tLLFeeMainSchema.setRefuseAmnt(tLLTJMajorDiseasesCaseParser.getREFUSEAMNT());
                    }
                    
                    tLLFeeMainSchema.setSumFee(tLLTJMajorDiseasesCaseParser.getAPPLYAMNT());
                    
                    
                    //可报销范围内金额
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getREIMBBURSEMENT()) && tLLTJMajorDiseasesCaseParser.getREIMBBURSEMENT()!=null){
                        tLLFeeMainSchema.setReimbursement(tLLTJMajorDiseasesCaseParser.getREIMBBURSEMENT());
                    }
                    
                    tLLFeeMainSchema.setAffixNo(tLLTJMajorDiseasesCaseParser.getAFFIXNO());
                    tLLFeeMainSchema.setOriginFlag(tLLTJMajorDiseasesCaseParser.getORIGINFLAG());
                    
                    // 3500 医保类型**start**
                  		String tMedicareType =tLLTJMajorDiseasesCaseParser.getMEDICARETYPE();			//医保类型
                  		
                  		//校验录入的医保类型信息
                  		String tMedicareCode =LLCaseCommon.checkMedicareType(tMedicareType);
                      		if(!"".equals(tMedicareCode)){
                      			buildError("dealData()", tMedicareCode);
                               mDealState = false; 
                               return false;
                      		}else{
                      			tLLFeeMainSchema.setMedicareType(tMedicareType);
                      		}
                   // 3500 医保类型**end**
                    //社保账单填充
                 
                    tLLSecurityReceiptSchema.setFeeDetailNo("");
                    tLLSecurityReceiptSchema.setMainFeeNo("");
                    tLLSecurityReceiptSchema.setRgtNo(mRgtNo);
                    tLLSecurityReceiptSchema.setCaseNo("");
                    tLLSecurityReceiptSchema.setApplyAmnt(tLLTJMajorDiseasesCaseParser.getAPPLYAMNT());
                    tLLSecurityReceiptSchema.setFeeInSecu("");
                   
                    tLLSecurityReceiptSchema.setYearPlayFee("");//年付统筹支付
                    
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getSUPINHOSFEE()) && tLLTJMajorDiseasesCaseParser.getSUPINHOSFEE()!=null){
                        tLLSecurityReceiptSchema.setSupInHosFee(tLLTJMajorDiseasesCaseParser.getSUPINHOSFEE());//大额医疗费
                    }
                   
                    //高段一责任金额
                    tLLSecurityReceiptSchema.setHighAmnt1(tLLTJMajorDiseasesCaseParser.getHIGHAMNT1());//大额救助支付 
                    
                    //个人自付
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getSELFPAY1()) && tLLTJMajorDiseasesCaseParser.getSELFPAY1()!=null){
                        tLLSecurityReceiptSchema.setSelfPay1(tLLTJMajorDiseasesCaseParser.getSELFPAY1());//SELFPAY1个人自付
                    }
                    //部分自付
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getSELFPAY2()) && tLLTJMajorDiseasesCaseParser.getSELFPAY2()!=null){
                        tLLSecurityReceiptSchema.setSelfPay2(tLLTJMajorDiseasesCaseParser.getSELFPAY2());//SELFPAY1个人自付
                    }
                    
                   
                    tLLSecurityReceiptSchema.setSelfAmnt(tLLTJMajorDiseasesCaseParser.getSELFAMNT());//全部自费
                    
                    tLLSecurityReceiptSchema.setGetLimit(tLLTJMajorDiseasesCaseParser.getGETLIMIT());
//                  //账户支付
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getACCOUNTPAY()) && tLLTJMajorDiseasesCaseParser.getACCOUNTPAY()!=null){
                        tLLSecurityReceiptSchema.setAccountPay(tLLTJMajorDiseasesCaseParser.getACCOUNTPAY());
                    }
                    //统筹支付
                    if(!"".equals(tLLTJMajorDiseasesCaseParser.getPLANFEE()) && tLLTJMajorDiseasesCaseParser.getPLANFEE()!=null){
                        tLLSecurityReceiptSchema.setPlanFee(tLLTJMajorDiseasesCaseParser.getPLANFEE());//统筹支付
                    }
                    tLLSecurityReceiptSchema.setMidAmnt(tLLTJMajorDiseasesCaseParser.getMIDAMNT());
                    
                   
                    //分案诊疗明细
                    tLLCaseCureSchema.setCustomerName(tLLTJMajorDiseasesCaseParser.getCUSTOMERNAME());
                    tLLCaseCureSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
                    tLLCaseCureSchema.setHospitalName(tLLTJMajorDiseasesCaseParser.getHOSPITALNAME());
                    tLLCaseCureSchema.setHospitalCode(tLLTJMajorDiseasesCaseParser.getHOSPITALNAMECODE());
                    tLLCaseCureSchema.setReceiptNo(tLLTJMajorDiseasesCaseParser.getRECEIPTNO());
                    tLLCaseCureSchema.setDiseaseName(tLLTJMajorDiseasesCaseParser.getDISEASENAME());
                    tLLCaseCureSchema.setDiseaseCode(tLLTJMajorDiseasesCaseParser.getDISEASECODE());
                    tLLCaseCureSet.add(tLLCaseCureSchema);
                    
                    //案件实效
                    mLLCaseOpTimeSchema.setStartDate(PubFun.getCurrentDate());
                    mLLCaseOpTimeSchema.setRgtState("01");
                    mLLCaseOpTimeSchema.setStartTime(PubFun.getCurrentTime());
                    mLLCaseOpTimeSchema.setOperator(mOperator);
                    mLLCaseOpTimeSchema.setManageCom(mManageCom);
                                       
                    //申请原因
                    LLAppClaimReasonSchema tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
                    if (tLLTJMajorDiseasesCaseParser.getFEETYPE().equals("1")) {
                        tLLAppClaimReasonSchema.setReasonCode("01");
                        tLLAppClaimReasonSchema.setReason("门诊"); //申请原因
                    } else if(tLLTJMajorDiseasesCaseParser.getFEETYPE().equals("2")){
                        tLLAppClaimReasonSchema.setReasonCode("02"); //原因代码
                        tLLAppClaimReasonSchema.setReason("住院"); //申请原因
                    }else if(tLLTJMajorDiseasesCaseParser.getFEETYPE().equals("3")){
                        tLLAppClaimReasonSchema.setReasonCode("03"); //原因代码
                        tLLAppClaimReasonSchema.setReason("特种病"); //申请原因
                    }else{
                        //#531_如果不是以上三种类型，则不存
                        tLLAppClaimReasonSchema.setReasonCode("00"); //原因代码
                        tLLAppClaimReasonSchema.setReason("无"); //申请原因
                    }
                    tLLAppClaimReasonSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
                    tLLAppClaimReasonSchema.setReasonType("0");
                    
                    if(!"00".equals(tLLAppClaimReasonSchema.getReasonCode())){
                        tVData.add(tLLAppClaimReasonSchema);    
                    }
                    
                    //3500
                    LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
              		String tProvinceAccCode=tLLTJMajorDiseasesCaseParser.getACCPROVINCECODE();	//发生地点省
              		String tCityAccCode =tLLTJMajorDiseasesCaseParser.getACCCITYCODE();			//发生地点市     
              		String tCountyAccCode =tLLTJMajorDiseasesCaseParser.getACCCOUNTYCODE();     //发生地点县
              		
                    String tAccCode=LLCaseCommon.checkAccPlace(tProvinceAccCode,tCityAccCode,tCountyAccCode); 
              		if(!"".equals(tAccCode)){
              			buildError("dealData()", tAccCode);
              			mDealState = false; 
                        return false;
              		}else{
              			tLLSubReportSchema.setAccProvinceCode(tProvinceAccCode);
              			tLLSubReportSchema.setAccCityCode(tCityAccCode);
              			tLLSubReportSchema.setAccCountyCode(tCountyAccCode);
              		}

                    //3500
                        
                    //开始提交案件信息
                    LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
                    tLLRegisterSchema.setRgtNo(mRgtNo);
                    tLLRegisterSchema.setRgtObjNo(mGRPCONTNO);
                    
                    tVData.add(tLLRegisterSchema);
                    tVData.add(tLLCaseSchema);
                    tVData.add(tLLFeeMainSchema);
                    tVData.add(tLLSecurityReceiptSchema);
                    tVData.add(tLLCaseCureSet);
                    tVData.add(mLLCaseOpTimeSchema);
                    tVData.add(tLLSubReportSchema);
                    tVData.add(tG);

                	//在此处额外添加下批次撤件功能,不包含在本次开发
//                	if(!CancelRgt()){
//                        buildError("dealData()", "批次预处理失败");
//                        mDealState = false; 
//                        return false;
//                	}
                	
                    if (!tgenCaseInfoBL.submitData(tVData, "INSERT||MAIN")) {
                        CErrors tError = tgenCaseInfoBL.mErrors;
                        tgenCaseInfoBL.getResult();
                        String ErrMessage = tError.getFirstError();
                        buildError("dealData()", "生成理赔信息出错!客户号:"+tLLCaseSchema.getCustomerNo()+"客户姓名:"+tLLCaseSchema.getCustomerName()+"错误信息:"+ErrMessage);
                        return false;
	                }else{

	                	mCaseNum = mCaseNum+1;
	                    tLLCaseSchema=(LLCaseSchema)tgenCaseInfoBL.getResult().getObjectByObjectName("LLCaseSchema", 0);
	                    //tLLCaseSchema=(LLCaseSchema)tLLSSgenCaseInfoBL.getResult().getObjectByObjectName("LLCaseSchema", 0);                      
	                    tList.add(mCaseNum+","+tLLCaseSchema.getCaseNo()+","+tLLTJMajorDiseasesCaseParser.getCLAIMNO());	                      
	                    System.out.println("tList:"+tList.size());	                    
	                    
	                    //信息到LLHospCase
	                    LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
	                    mLLHospCaseSchema.setCaseNo(tLLCaseSchema.getCaseNo());             
	                    mLLHospCaseSchema.setDealType(mDealType);
	                    mLLHospCaseSchema.setHospitCode(mRequestType);
	                    mLLHospCaseSchema.setHandler(mOperator);
	                    mLLHospCaseSchema.setAppTranNo(mTransactionNum);
	                    mLLHospCaseSchema.setAppDate(mCurrentDate);
	                    mLLHospCaseSchema.setAppTime(mCurrentTime);                         
	                    mLLHospCaseSchema.setConfirmState("1");
	                    if(mRequestType.equals("DB02")){
	                    	mLLHospCaseSchema.setCaseType("13");
	                    }
	                      
	                    mLLHospCaseSchema.setClaimNo(tLLTJMajorDiseasesCaseParser.getCLAIMNO());
	                    mLLHospCaseSchema.setBnfNo(tLLCaseSchema.getCustomerNo());   
	                    mLLHospCaseSchema.setRealpay(mAPPAMNT);//申报金额
	                    mLLHospCaseSchema.setMakeDate(mCurrentDate);
	                    mLLHospCaseSchema.setMakeTime(mCurrentTime);
	                    mLLHospCaseSchema.setModifyDate(mCurrentDate);
	                    mLLHospCaseSchema.setModifyTime(mCurrentTime);
	                      
	                    mLLHospCaseSet.add(mLLHospCaseSchema);
	                                           
	                }   
                    claimno=tLLTJMajorDiseasesCaseParser.getCLAIMNO()+"   "+claimno;
                }else{
                    buildError("dealData()", "获取理赔信息出错!");
                    mDealState = false;
                    return false;
                }
            }
        }else{
            buildError("dealData()", "报文中没有理赔信息!");
            mDealState = false; 
            return false;
        }
         
        return true;
    
	}

    /**
     * 理算及核赔
     * @return
     */
	private boolean batchClaimCal(String aRgtNo) {
		// TODO Auto-generated method stub
		//1.理算前校验
        MMap tmap = new MMap();
        VData tdata = new VData();
		LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(aRgtNo);
        //调整至受理时
//        if (!tLLRegisterDB.getInfo()) {
//            // @@错误处理
//            buildError("dealData()", "团体立案信息查询失败!");
//            mDealState = false; 
//            return false;
//        }
//        mLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
//        String declineflag = "" + mLLRegisterSchema.getDeclineFlag();//撤件标志
//        if ("1".equals(declineflag)) {
//            // @@错误处理
//            buildError("dealData()", "该团体申请已撤件，不能再录入个人客户!");
//            mDealState = false; 
//            return false;
//        }
//        if ("03".equals(mLLRegisterSchema.getRgtState())) {
//            // @@错误处理
//            buildError("dealData()", "团体申请下所有个人案件已经结案完毕，" +
//                    "不能再录入个人客户!");
//            mDealState = false; 
//            return false;
//        }
//        if ("04".equals(mLLRegisterSchema.getRgtState())) {
//            // @@错误处理
//            buildError("dealData()", "团体申请下所有个人案件已经给付确认，" +
//                    "不能再录入个人客户!");
//            mDealState = false; 
//            return false;
//        }
        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
        tLLCaseDB.setRgtState("03");
        mLLCaseSet = tLLCaseDB.query();
        if(mLLCaseSet.size()<=0){
            buildError("simpleClaimAudit()", "没有待理算的案件!");
            mDealState = false; 
            return false;
        }
        
        //2.理算
        for(int i=1;i<=mLLCaseSet.size();i++){
            if(!calClaim(mLLCaseSet.get(i).getCaseNo())){
                continue;
            }else{
                LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
                mLLCaseOpTimeSchema.setCaseNo(mLLCaseSet.get(i).getCaseNo());
                mLLCaseOpTimeSchema.setRgtState("04");
                mLLCaseOpTimeSchema.setOperator(tG.Operator);
                mLLCaseOpTimeSchema.setManageCom(tG.ManageCom);
                LLCaseCommon tLLCaseCommon = new LLCaseCommon();
                try {
                    LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.
                            CalTimeSpan(mLLCaseOpTimeSchema);
                    tmap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
                } catch (Exception ex) {
                    System.out.println("没有时效记录");
                }
            }
        }
        
        if (!uwCheck()) {
            return false;
        }
        
        tdata.add(tmap);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(tdata, null)) {
            buildError("dealData()", "数据库保存失败");
            mDealState = false; 
            return false;
        }
        
		return true;
	}

    /**
     * 自动理算，生成赔案信息表。
     * @return boolean
     */
	private boolean calClaim(String caseNo) {

        ClaimCalBL aClaimCalAutoBL = new ClaimCalBL();
        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        tLLCaseSchema.setCaseNo(caseNo);
        VData aVData = new VData();
        aVData.addElement(tG);
        aVData.addElement(tLLCaseSchema);
        if(!aClaimCalAutoBL.submitData(aVData, "autoCal")){
             buildError("calClaim()", "理算失败");
             mDealState = false; 
             return false;
        }
        return true;
    
	}
	
    /**
     * 调用核赔规则
     * @return boolean
     */
	private boolean uwCheck() {

        String tsql="";
        int tErrNO=0;
        String tErr=null;
        ExeSQL exesql = new ExeSQL();
        SSRS ssrs = new SSRS();
        System.out.println(this.mLLRegisterSchema.getRgtNo());
        if(this.mLLRegisterSchema.getRgtNo()!=null&&!("".equals(this.mLLRegisterSchema.getRgtNo()))){
	        tsql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'  with ur";
	        System.out.println("BatchClaimCalBL-中通过批量号获取客户号："+tsql);
	        ssrs = exesql.execSQL(tsql);
        }
               
        System.out.println(ssrs.getMaxRow());
        for(int i=1;i<=ssrs.getMaxRow();i++){
      	  VData tVData = new VData();
      	  LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
      	  LLCaseSchema tLLCaseSchema=new LLCaseSchema();
      	  LLCaseSet tLLCaseSet=new LLCaseSet();
      	  LLCaseRelaSet tLLCaseRelaSet=new LLCaseRelaSet();
      	  LLSubReportSet tLLSubReportSet=new LLSubReportSet();
      	  LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
      	  LLCaseDB tLLCaseDB=new LLCaseDB();
      	  tLLCaseDB.setCustomerNo(ssrs.GetText(i, 1));
      	  tLLCaseDB.setRgtState("03");
      	  tLLCaseDB.setRgtNo(this.mLLRegisterSchema.getRgtNo());
      	  tLLCaseSet=tLLCaseDB.query();
      	  String casenos="";
      	  if(tLLCaseSet!=null&&tLLCaseSet.size()>0)
      	  {
      		  for(int a=1;a<=tLLCaseSet.size();a++){
	      		  System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息Start！");
	      		  casenos=casenos+","+tLLCaseSet.get(a).getCaseNo();
	      		  LLClaimDetailDB tLLClaimDetailDB=new LLClaimDetailDB();
	      		  tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
	      		  tLLClaimDetailSet.add(tLLClaimDetailDB.query());
	      		  LLCaseRelaDB tLLCaseRelaDB=new LLCaseRelaDB();
	      		  tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(a).getCaseNo());
	      		  tLLCaseRelaSet.add(tLLCaseRelaDB.query());
	      		  System.out.println("BatchClaimCalBL-中通过客户号获取理赔业务表信息END！");
      		  }
      		  if(tLLCaseRelaSet!=null&&tLLCaseRelaSet.size()>0){
	      		  for(int b=1;b<=tLLCaseRelaSet.size();b++){
	      			  LLSubReportDB tLLSubReportDB=new LLSubReportDB();
	      			  tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(b).getSubRptNo());
	      			  tLLSubReportSet.add(tLLSubReportDB.query());
	      		  }
	      		  System.out.println("BatchClaimCalBL-中获取理赔LLSubReport表信息END！");
      		  }
      	  }
        
      	  tVData.addElement(tLLCaseSchema);
      	  tVData.addElement(tLLCaseRelaSet);
      	  tVData.addElement(tLLSubReportSet);
      	  tVData.addElement(tLLClaimDetailSet);
      	  tVData.addElement(tG);
          if (!tLLUWCheckBL.submitData(tVData,"")) {
          	  tErrNO=tErrNO+1;
          	  if(tErr==null){
          		  tErr=String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";  
          	  }else{ 
          		  tErr=tErr+String.valueOf(tErrNO)+"、客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+");";
               // CError.buildErr(this,"客户号为"+ssrs.GetText(i, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+casenos+")");
          	  }
            }          
         }
         if(tErr!=null){
        	 CError.buildErr(this,tErr);
        	 return false;
         }
         return true;
    
	}

	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState || "".equals(mRgtNo)) {
            return createFalseXML();
        } else {
        	try
        	{
        		return createResultXML(mRgtNo);
        	}
        	catch (Exception e)
        	{          
        		e.printStackTrace();
        		buildError("createXML()", "生成返回报文错误!");
        		return createFalseXML();
        	}
        }
    }  
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        Element tBodyData = new Element("BODY");
        
        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }else{
        	mResponseCode = "1";
        }
        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBodyData.addContent(tResponseCode);

        mErrorMessage = mErrors.getFirstError();
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aRgtNo) throws Exception {
         System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BACK_DATA");
         tBodyData.addContent(tBackData);
         
         //Back_Data部分
         Element tRgtNO = new Element("RGTNO");
                 
         tRgtNO.setText(aRgtNo);
         tBackData.addContent(tRgtNO);
         
         
         //返回的案件处理结构
         Element tLLCaseList = new Element("LLCASELIST");
         
         System.out.println("开始返回报文生成--list的长度"+tList.size());
         if(tList!=null && tList.size()>0){
             
             for(int i=0;i< tList.size();i++){
                 System.out.println("开始返回报文生成:"+i);
                 String[] tresult=tList.get(i).toString().split(",");
                 
                 System.out.println(tresult[0]+"==="+tresult[1]);
                 Element tLLCaseData = new Element("LLCASE_DATA");
                 Element tCASENUM = new Element("CASENUM");
                 tCASENUM.setText(tresult[0]);
                 Element tCASENO = new Element("CASENO");
                 
                 Element tClaimno = new Element("CLAIMNO");
                 
                 tCASENO.setText(tresult[1]);
                 tClaimno.setText(tresult[2]);
                 tLLCaseData.addContent(tCASENUM);
                 tLLCaseData.addContent(tCASENO);
                 tLLCaseData.addContent(tClaimno);
                 tLLCaseList.addContent(tLLCaseData);
             }
         }
         tBodyData.addContent(tLLCaseList);
         tRootData.addContent(tBodyData);
         
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    /**
     * 给团体保单做撤件
     * @return
     */
        private boolean CancelRgt()
        {
        	if(tCancelFlag){           	
                LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
                LLRegisterDB tLLRegisterDB = new LLRegisterDB();
                System.out.println("立案号码是" + mRgtNo);
                tLLRegisterDB.setRgtNo(mRgtNo);
                if(tLLRegisterDB.getInfo()){
//                    if (!(tLLRegisterDB.getDeclineFlag() == null ||
//                          tLLRegisterDB.getDeclineFlag().equals(""))){
//                        if (tLLRegisterDB.getDeclineFlag().equals("1")){
//                            this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
//                            CError tError = new CError();
//                            tError.moduleName = "LLRegisterBL";
//                            tError.functionName = "CancelRgt";
//                            tError.errorMessage = "该批次已经进行了撤销，不能对此进行撤销！";
//                            this.mErrors.addOneError(tError);
//                            return false;
//                        }
//
//                    }
                    boolean flag = false;
                    String scase = "select 1 from llcase where rgtno='"+tLLRegisterDB.getRgtNo()+"' and rgtstate<>'14' fetch first rows only with ur";
                    ExeSQL tExe = new ExeSQL();
                    String result  =  tExe.getOneValue(scase);
                    if("1".equals(result)){
                    	flag = true;
                    }
                    System.out.println("tLLRegisterDB.getRgtState()  && !flag" + !(tLLRegisterDB.getRgtState().equals("03") && !flag));
                     
                     
                    if (!tLLRegisterDB.getRgtState().equals("01") && 
                    		!tLLRegisterDB.getRgtState().equals("02") && 
                    		!(tLLRegisterDB.getRgtState().equals("03") && !flag) && 
                    		!"07".equals(tLLRegisterDB.getRgtState())){
                    	    this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "LLRegisterBL";
                            tError.functionName = "CancelRgt";
                            tError.errorMessage = "该批次下所有个人结案完毕，不能对此进行撤销！";
                            this.mErrors.addOneError(tError);
                            return false;
                    }
                    
                    if (!tLLRegisterDB.getHandler1().equals(tG.Operator)
                        && !LLCaseCommon.checkUPUpUser(tG.Operator,tLLRegisterDB.getHandler1())){
                        this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LLRegisterBL";
                        tError.functionName = "CancelRgt";
                        tError.errorMessage = "您不是该批次处理人，不能撤销该批次！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    
                }else{
                    this.mErrors.copyAllErrors(tLLRegisterDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LLRegisterBL";
                    tError.functionName = "CancelRgt";
                    tError.errorMessage = "批次查询失败！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

         
                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                String tCaseSQL = "SELECT * FROM llcase WHERE rgtno='"+mRgtNo+"' AND rgtstate IN ('05','06','09','10','11','12')";
                tSSRS = tExeSQL.execSQL(tCaseSQL);
                if (tSSRS.getMaxRow() > 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "LLRegisterBL";
                    tError.functionName = "updateData";
                    tError.errorMessage = "该批次下案件状态不允许批次撤件！";
                    System.out.println("不能进行撤销数据");
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                
                
                //删除已产生的赔案信息
                String sql0 = "delete from LLClaim where rgtno='"+mRgtNo+"'";
                String sql_p = "delete from LLClaimPolicy where rgtno='"+mRgtNo+"'";
                String sql_d = "delete from LLClaimDetail where rgtno='"+mRgtNo+"'";
                String sql_acc = "delete from LCInsureAccTrace where OtherNo in (select caseno from llcase where rgtno='"+mRgtNo+"')";
                //删除赔案信息增加ljsget,ljsgetclaim的数据	#2368 cbs00071160关于撤件案件造成的宽限期过后保单状态的问题
                String sql_ljsget_p = "delete from ljsget where otherno='"+mRgtNo+"'";
                String sql_ljsget_c = "delete from ljsget where otherno in (select caseno from llcase where rgtno='"+mRgtNo+"') ";
                String sql_ljsgetclaim_p = "delete from ljsgetclaim where otherno='"+mRgtNo+"'";
                String sql_ljsgetclaim_c = "delete from ljsgetclaim where otherno in (select caseno from llcase where rgtno='"+mRgtNo+"') ";

                
                //删除审批相关的信息
                String sql_uwm = "delete from LLClaimUWmain where rgtno='"+mRgtNo+"'";
                String sql_ud = "delete from LLClaimUWDetail where caseno in (select caseno from llcase where rgtno='"+mRgtNo+"')";
                String sql_uw = "delete from LLClaimUnderwrite where rgtno='"+mRgtNo+"'";
                
                tLLRegisterSchema.setSchema(tLLRegisterDB.getSchema());
                //tLLRegisterSchema.setDeclineFlag("1"); //撤销标准
                tLLRegisterSchema.setRgtReason("天津大病批次导入错误");
                tLLRegisterSchema.setCanceler(tG.Operator);
                tLLRegisterSchema.setCancelReason("");
                tLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
                tLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());

                
                MMap map = new MMap();
                /*
                 * 判断是否是天津社保案件，TRUE 更改状态  false 不做处理
                 * */
                ExeSQL tExe = new ExeSQL();
                String querySQL = "select RgtType from llregister where rgtno='"+mRgtNo+"'";
                
                String rgtType_result  =  tExe.getOneValue(querySQL);
                if("8".equals(rgtType_result)){
                	String squerySQL = "select caseno from llcase where rgtno='"+mRgtNo+"'";
                	SSRS rgtType_resultt  =  tExe.execSQL(squerySQL);//下标从1开始
                	if(rgtType_resultt!=null&&rgtType_resultt.MaxRow>0){
                		for(int i=1;i<=rgtType_resultt.MaxRow;i++){
                			String ConfirmState_update = "update LLHospCase set ConfirmState = '2' where caseno = '"+rgtType_resultt.GetText(i, 1)+"'";
                			map.put(ConfirmState_update,"UPDATE");
                		}
                	}
                }
                
                map.put(tLLRegisterSchema, "UPDATE");
                map.put(sql0, "DELETE");
                map.put(sql_p, "DELETE");
                map.put(sql_d, "DELETE");
                map.put(sql_acc, "DELETE");
                map.put(sql_uwm, "DELETE");
                map.put(sql_ud, "DELETE");
                map.put(sql_uw, "DELETE");
                map.put(sql_ljsget_p, "DELETE");
                map.put(sql_ljsget_c, "DELETE");
                map.put(sql_ljsgetclaim_p, "DELETE");
                map.put(sql_ljsgetclaim_c, "DELETE");

                LLCaseDB tLLCaseDB = new LLCaseDB();
                LLCaseSet tLLCaseSet = new LLCaseSet();
                LLCaseSet saveLLCaseSet = new LLCaseSet();
                tLLCaseDB.setRgtNo(mRgtNo);
                tLLCaseSet = tLLCaseDB.query();
                if(tLLCaseSet.size()>0){
                    for (int i=1 ; i<=tLLCaseSet.size();i++){
                        LLCaseSchema tLLCaseSchema = new LLCaseSchema();
                        tLLCaseSchema = tLLCaseSet.get(i);
                        tLLCaseSchema.setCancleReason("");
                        tLLCaseSchema.setCancleRemark("团体批次被撤销");
                        tLLCaseSchema.setRgtState("14");
                        tLLCaseSchema.setHandler(tG.Operator);
                        tLLCaseSchema.setCancleDate(PubFun.getCurrentDate());
                        tLLCaseSchema.setCancler(tG.Operator);
                        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
                        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
                        saveLLCaseSet.add(tLLCaseSchema);
                    }
                    map.put(saveLLCaseSet,"UPDATE");
               }


                    this.mResult.clear();
                    this.mResult.add(map);
                PubSubmit ps = new PubSubmit();
                if (!ps.submitData(this.mResult, ""))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(ps.mErrors);
                    return false;
                }else{
                	tCancelFlag = false;
                }
                return true;           
        	}else{
        		return true;  
        	}
        }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJSocialSecurityRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    
    public static void main(String[] args) {
    	Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("D:/理赔返回成功/发送报文/TJDB-010-3.xml"), "GBK");
            LLTJMajorDiseasesCaseRegister tBusinessDeal = new LLTJMajorDiseasesCaseRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
