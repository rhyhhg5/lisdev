/**
 * 2014-5-20
 */
package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


/**保存天津社保平台传过来的报文
 * @author zjd
 *
 */
public class LLTJSocialSecSave implements ServiceInterface
{
    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();
    private String mErrorMessage = "";  //错误描述  
    
    private String mManageCom="86120000";   //管理机构
    private String mOperator="cm1209";   //管理机构
    
    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private boolean mDealState = true;      //处理标志    
    private String mResponseCode = "1";     //返回类型代码    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    private String mDealType = "1";         //处理类型 1 实时 2非实时   
    
    private Element mBody; //
    private String mGrpContNo=""; //保单号
    private String mAppeaples=""; //人数
    private String mTogetherFlag=""; //给付方式
    private String mAppAMnt=""; //申报金额
    
    private ExeSQL tExeSQL = new ExeSQL();
    
    private List mCaseDateList;
    private Document pInXmlDoc;
    public Document service(Document mInXmlDoc) {
  
           pInXmlDoc=mInXmlDoc;
            try
            {
                if(!getInputData(pInXmlDoc)){
                    mDealState=false;
                }else{
                    if(!checkData()){
                        mDealState=false;
                    }else{
                        if(!save(pInXmlDoc)){
                            buildError("save()", "保存请求报文出错!");
                            mDealState=false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // TODO 自动生成 catch 块
                e.printStackTrace();
            }
            return createXML();
        
        
    }
    
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJSocialSecSave--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文!!!!!");
           
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        mBody=tRootData.getChild("BODY");
        
        
        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
        
        Element tBaseDate=mBody.getChild("BASE_DATA");
        mGrpContNo=tBaseDate.getChildTextTrim("GRPCONTNO"); //保单号
        mAppeaples=tBaseDate.getChildTextTrim("APPPEOPLES"); //人数
        mTogetherFlag=tBaseDate.getChildTextTrim("TOGETHERFLAG"); //给付方式
        mAppAMnt=tBaseDate.getChildTextTrim("APPAMNT"); //申报金额
        
        Element tCaseList=mBody.getChild("LLCASELIST");
        mCaseDateList=tCaseList.getChildren("LLCASE_DATA");
        
        System.out.println("导入的案件数:"+mCaseDateList.size());
        System.out.println("LLTJSocialSecSave--TransactionNum:" + mTransactionNum);
        
       
        return true;

    }
    
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
            return false;
        }

        if (!"TJ05".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误!");
            return false;
        }

        if (!"TJ05".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86120000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误!");
            return false;
        }
        
        if(mCaseDateList!=null){
            
            System.out.println("开始校验人数:"+mAppeaples.trim()+"===="+String.valueOf(mCaseDateList.size()).trim());
            if(!mAppeaples.trim().equals(String.valueOf(mCaseDateList.size()).trim())){
                buildError("checkData()", "【申请人数】与实际案件数不符!");
                return false;
            }
        }
        
        if(mCaseDateList!=null){
            if(mCaseDateList!=null){
                for(int i=0;i<mCaseDateList.size();i++){
                    Element mllcaseinfo=(Element)mCaseDateList.get(i);
                    String mbankcode=mllcaseinfo.getChildText("BANKCODE");
                    String mhospitalcode=mllcaseinfo.getChildText("HOSPITALNAMECODE");
                    String mclaimno=mllcaseinfo.getChildText("CLAIMNO");
                    System.out.println("前----------已经通过了平台理赔号的校验-------"+mclaimno);
                    String mDISEASECODE=mllcaseinfo.getChildText("DISEASECODE");
                    if(mbankcode!=null && !"".equals(mbankcode)){
                        String tSql=" select BankCode from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1')  and BankCode='"+mbankcode+"'  ";
                        String tbankcode=tExeSQL.getOneValue(tSql);
                        if("".equals(tbankcode)){
                            buildError("checkData()", "【案件信息】的银行编码不存在!");
                            return false;
                        }
                       
                    }
                    
                    if(mhospitalcode!=null && !"".equals(mhospitalcode)){
                        String tSql=" select a.hospitcode from  ldhospital a where HospitCode ='"+mhospitalcode+"' ";
                        String thospitalcode=tExeSQL.getOneValue(tSql);
                        if("".equals(thospitalcode)){
                            buildError("checkData()", "【案件信息】的医院编码不存在!");
                            return false;
                        }
                        
                    }
                    
                    if(mDISEASECODE!=null && !"".equals(mDISEASECODE)){
                        String tSql=" select icdcode from lddisease where icdcode='"+mDISEASECODE+"'";
                        String thospitalcode=tExeSQL.getOneValue(tSql);
                        if("".equals(thospitalcode)){
                            buildError("checkData()", "【案件信息】的疾病编码不存在!");
                            return false;
                        }
                        
                    }
                    
                    //平台理赔号
                    
                    if(mclaimno!=null && !"".equals(mclaimno)){
                        String tSql=" select claimno from LLHospCase where claimno='"+mclaimno+"' and hospitcode='TJ05'";
                        String tclaimno=tExeSQL.getOneValue(tSql);
                        String ttSql=" select ConfirmState from LLHospCase where claimno='"+mclaimno+"' and hospitcode='TJ05'";
                        String tConfirmState=tExeSQL.getOneValue(ttSql);
                        if(!"".equals(tclaimno)&&"1".equals(tConfirmState)){
                            buildError("checkData()", "【案件信息】的平台理赔号"+tclaimno+"系统中已存在!");
                            return false;
                        }
                    }
                }
            }

        }
        
        return true;

    }
    
    /**
     * 生成返回的报文信息
     * @return Document
     */
   
    private Document createXML() {
        System.out.println("LLTJSocialSecSave--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Element tbody=new Element("BODY");
        Element tresponsecode=new Element("RESPONSE_CODE");
        if (!mDealState) {
            tresponsecode.setText("E");
        }else{
            tresponsecode.setText("1");
        }
        
        mErrorMessage = mErrors.getFirstError();


        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tbody.addContent(tresponsecode);
        tbody.addContent(tErrorMessage);
        tRootData.addContent(tbody);
        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    
    
    /**
     * 保存报文到系统中指定的路径
     * @return
     */
    private boolean save(Document pInXmlDoc){
        ExeSQL tExeSQL=new ExeSQL();
//      获取配置信息
        String tSql="select DECIMAL(CURRENT DATE),codealias from ldcode where codetype='TJSocialSec' and code='2014TJSS' ";
//        String tSql="select codename,codealias from ldcode where codetype='TJSocialSec' and code='2014TJSS' ";
        
        SSRS tSSRS=tExeSQL.execSQL(tSql);
        
        if(tSSRS!=null){
            String mFilePath=tSSRS.GetText(1, 2);
            String mTaskDate=tSSRS.GetText(1, 1);
            String mDtype="InNoStd";
            String mFileInPath=generateFilePath(mFilePath,mDtype,mTaskDate);
            String mFilename=generateFileName();
            
            File mFileDir = new File(mFileInPath);
            if (!mFileDir.exists()) {
                if (!mFileDir.mkdirs()) {
                    System.err.println("创建目录失败！" + mFileDir.getPath());
                }
            }
            
            try {
                FileOutputStream tFos = new FileOutputStream(mFileInPath + mFilename);
                JdomUtil.output(pInXmlDoc, tFos);
                tFos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        }else{
            buildError("save()", "ldcode查询配置信息出错!");
            return false;
        }
        return true;
    }
    
    /**
     * 获取保存路径
     */
    public static String generateFilePath(String FilePath,String pType,String TaskDate)
    {
        StringBuffer sFilePath = new StringBuffer();
        sFilePath.append(FilePath).append('/')
                    .append("FileContent").append('/')
                    .append(DateUtil.getCurrentDate("yyyy")).append('/')
                    .append(DateUtil.getCurrentDate("yyyyMM")).append('/')
                    .append(TaskDate).append('/')
                    .append(pType).append('/');
        return sFilePath.toString();
    }
    
    
    
    /**
     * 生成待保存报文的文件名
     * 
     * @param aXmlDoc
     * @return
     */
    private String generateFileName() {
        String tTransactionNum = mTransactionNum;
        return SaveMessage.generateFileName(tTransactionNum, "xml");
    }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJSocialSecSave";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    
    public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/TJSB/TJ0501.xml"), "GBK");
            LLTJSocialSecSave tBusinessDeal = new LLTJSocialSecSave();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc.getRootElement());
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   

}
