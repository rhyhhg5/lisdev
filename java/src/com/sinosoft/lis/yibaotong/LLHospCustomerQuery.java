package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 查询客户信息</p>
 *
 * <p>Description: 查询客户信息</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLHospCustomerQuery implements ServiceInterface{
    public LLHospCustomerQuery() {
    }

    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();

    //传入报文
    private Document mInXmlDoc;
    //报文类型
    private String mDocType = "";
    //处理标志
    private boolean mDealState = true;

    //返回类型代码
    private String mResponseCode = "1";
    //请求类型
    private String mRequestType = "";
    //错误描述
    private String mErrorMessage = "";
    //交互编码
    private String mTransactionNum = "";
    //交互编码中医院代码
    private String mTranHospCode = "";
    //客户姓名
    private String mPersonName = "";
    //客户性别
    private String mSex = "";
    //客户证件号码
    private String mIdNumber = "";
    //人保医院编码
    private String mHospitalNumber = "";
    //客户家庭地址
    private String mHomeAddress = "";
    //客户家庭电话
    private String mHomePhone = "";
    //客户手机号码
    private String mMobile = "";
    
    //机构编码
    private String mManageCom = "";
    
    SSRS tSSRSInsured = null;

    public Document service(Document pInXmlDoc) {
        mInXmlDoc = pInXmlDoc;

        try {
        	
        	if (!getInputData(mInXmlDoc)) {
                 mDealState = false;
             } else {
                 if (!checkData()) {
                     mDealState = false;
                 } else {
                     if (!dealData()) {
                         mDealState = false;
                     }
                 }
             }


        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
    	System.out.println("LLHospCustomerQuery--checkData");
    	
    	if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型的值不存在");
            return false;
        }
    	
        if (!"01".equals(mRequestType)) {
            buildError("checkData()", "报文请求类型的值不存在");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "交互编码违反非空约束或长度错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "交互编码与报文请求类型不匹配");
            return false;
        }
        
        if (mPersonName == null || mPersonName.equals(""))
        {
        	buildError("checkData()", "客户姓名违反非空约束");
            return false;
        }
        
        if (mSex == null || mSex.equals(""))
        {
        	buildError("checkData()", "客户性别违反非空约束");
            return false;
        }
        
        if (mIdNumber == null || mIdNumber.equals(""))
        {
        	buildError("checkData()", "客户证件号码违反非空约束");
            return false;
        }
        
        if (mHospitalNumber == null || mHospitalNumber == "") {
                buildError("checkData()", "人保医院编码违反非空约束");
                return false;
            }
        System.out.println("mTransactionNum.substring(2, 9):"+mTransactionNum.substring(2, 9));
        
        if (!mHospitalNumber.equals(mTransactionNum.substring(2, 9))) {
            buildError("checkData()", "交互编码与人保医院编码不匹配");
            return false;
        }

        mTranHospCode = mTransactionNum.substring(2, 9);

        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        tLDHospitalDB.setHospitCode(mTranHospCode);
        if (!tLDHospitalDB.getInfo()) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }
        mManageCom = tLDHospitalDB.getManageCom();
        if ("".equals(mManageCom)) {
            buildError("checkData()", "医院所属机构的值不存在");
            return false;
        }

        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
    	System.out.println("LLHospCustomerQuery--getInputData");
    	
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        Element tBasePart = tBodyData.getChild("BASE_PART");
        mPersonName = tBasePart.getChildTextTrim("PERSON_NAME");
        mSex = tBasePart.getChildTextTrim("SEX");       
        mIdNumber = tBasePart.getChildTextTrim("ID_NUMBER");
        mHospitalNumber = tBasePart.getChildTextTrim("HOSPITAL_NUMBER");
        
//        System.out.println("mRequestType:"+mRequestType);
//        System.out.println("mTransactionNum:"+mTransactionNum);
//        System.out.println("mPersonName:"+mPersonName);
//        System.out.println("mSex:"+mSex);
//        System.out.println("mIdNumber:"+mIdNumber);
//        System.out.println("mHospitalNumber:"+mHospitalNumber);
//        System.out.println("mTransactionNum.substring(2, 9):"+mTransactionNum.substring(2, 9));
//        System.out.println("mTransactionNum.substring(0, 2):"+mTransactionNum.substring(0, 2));
        
        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {   	
    	System.out.println("LLHospCustomerQuery--dealData");
    	
    	String tSql = "select distinct insuredno,name,sex,idtype,idno,birthday,InsuredStat from lcinsured"
			  + " where managecom like '" + mManageCom + "%'"
			  + " and name = '" + mPersonName + "'"
			  + " and sex = '" + mSex + "'"
			  + " and idno = '" + mIdNumber + "'"
			  + " with ur";
      ExeSQL tExeSQL = new ExeSQL();
      System.out.println(tSql);
      tSSRSInsured = tExeSQL.execSQL(tSql);
      if (tSSRSInsured.getMaxRow() < 1)
      {
    	  buildError("dealData()", "核心系统无该客户信息");
          return false;
      }
      
        
        return true;
    }


    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
            return createResultXML();
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() {
        Element tRootData = new Element("PACKET");
        
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText("01");
        tHeadData.addContent(tRequestType);

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText("1");
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText("");
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        
        Element tBodyData = new Element("BODY");
        
        Element tPersonList = new Element("PERSON_LIST");
        for (int i = 1; i <= tSSRSInsured.getMaxRow(); i++) {
        	
            String tCustomerNo=tSSRSInsured.GetText(i, 1);
            String tSql = "select homeaddress,homephone,mobile from lcaddress"
				  + " where CustomerNo = '" + tCustomerNo + "'"
				  + " order by int(addressno) desc fetch first 1 rows only with ur";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = null;
            System.out.println(tSql);
            tSSRS = tExeSQL.execSQL(tSql);
            if (tSSRS.getMaxRow() > 0)
            {
            	mHomeAddress = tSSRS.GetText(1, 1);
            	mHomePhone = tSSRS.GetText(1, 2);
            	mMobile = tSSRS.GetText(1, 3);
            }
            
        	Element tPersonName = new Element("PERSON_NAME");
        	tPersonName.setText(tSSRSInsured.GetText(i, 2));
        	
        	//将系统中性别不为0-男、1-女的都归为2-其他
        	String tSexTemp = tSSRSInsured.GetText(i, 3);
        	if (!tSexTemp.equals("0") && !tSexTemp.equals("1")) {
        		tSexTemp = "2";
        	}
        	Element tSex = new Element("SEX");
        	tSex.setText(tSexTemp);
        	
        	Element tIDType = new Element("ID_TYPE");
        	tIDType.setText(tSSRSInsured.GetText(i, 4));
        	
        	Element tIDNumber = new Element("ID_NUMBER");
        	tIDNumber.setText(tSSRSInsured.GetText(i, 5));
        	
        	Element tBirthDate = new Element("BIRTH_DATE");
        	tBirthDate.setText(tSSRSInsured.GetText(i, 6));
        	
        	Element tHomeAddress = new Element("ADDRESS");
        	tHomeAddress.setText(mHomeAddress);
        	
        	Element tHomePhone = new Element("TELEPHONE");
        	tHomePhone.setText(mHomePhone);
        	
        	Element tMobile = new Element("MOBILEPHONE");
        	tMobile.setText(mMobile);
        	
        	//默认返回3-参加其他商业保险
        	Element tJoinInsuranceCode = new Element("JOIN_INSURANCE_CODE");
        	tJoinInsuranceCode.setText("3");
        	
        	//将系统中被保险人状态不为1-在职、2-退休的都归为3-其他
        	String tInsuredStat = tSSRSInsured.GetText(i, 7);
        	if (!tInsuredStat.equals("1") && !tInsuredStat.equals("2")) {
        		tInsuredStat = "3";
        	}
        	Element tBeingStateCode = new Element("BEING_STATE_CODE");
        	tBeingStateCode.setText(tInsuredStat);

            Element tPersonData = new Element("PERSON_DATA");
            tPersonData.addContent(tPersonName);
            tPersonData.addContent(tSex);
            tPersonData.addContent(tIDType);
            tPersonData.addContent(tIDNumber);
            tPersonData.addContent(tBirthDate);
            tPersonData.addContent(tHomeAddress);
            tPersonData.addContent(tHomePhone);
            tPersonData.addContent(tMobile);
            tPersonData.addContent(tJoinInsuranceCode);
            tPersonData.addContent(tBeingStateCode);
            tPersonList.addContent(tPersonData);
		}
        
        tBodyData.addContent(tPersonList);
        tRootData.addContent(tBodyData);
        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospCustomerQuery";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args) {
//    	System.out.println("程序开始…");
//    	
//		XMLPubTool tXMLPubTool = new XMLPubTool();
//		Element eRequestType = tXMLPubTool.makeElement("REQUEST_TYPE", "01");
//		Element eTransactionNum = tXMLPubTool.makeElement("TRANSACTION_NUM", "0151010120091209");
//		
//		Element eHead = tXMLPubTool.makeEmptyElement("HEAD");
//		Element ePacket = tXMLPubTool.makeComplexElement("PACKET", "type", "REQUEST");
//		tXMLPubTool.AddAttribute(ePacket, "version", "1.0");
//		
//		tXMLPubTool.LinkElement(ePacket, eHead);
//		tXMLPubTool.LinkElement(eHead,eRequestType);
//		tXMLPubTool.LinkElement(eHead,eTransactionNum);
//		
//		Document tNoStdXml = new Document(ePacket);
//		JdomUtil.print(tNoStdXml);
//		return tNoStdXml;
//
//
//        LLHospCustomerQuery tTest = new LLHospCustomerQuery();
////        System.out.println(tTest.mRequestType);
//        tTest.mDealState = true;
//
//        tTest.mResponseCode = "1";
//        //请求类型
//        tTest.mRequestType = "03";
//        //错误描述
//        tTest.mErrorMessage = "错了吧";
//        //交互编码
//        tTest.mTransactionNum = "123456789";
//
//        Element tRootData = new Element("PACKET");
//        Document tDocument = new Document(tRootData);
////        tTest.service(tDocument);

//        return null;
    	
    	Document tInXmlDoc;
        try
        {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "d:/customer.xml"), "GBK");

            LLHospCustomerQuery tBusinessDeal = new LLHospCustomerQuery();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        }
        catch (Exception e) {
			e.printStackTrace();
		}
    }
}
