package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;
import org.jdom.JDOMException;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLTPTransFtpXmlCase {

	/**错误的容器 **/
	public CErrors mErrors = new CErrors();
	
	private String mDealInfo = "";

	private GlobalInput mGI = null; //用户信息

	private MMap map = new MMap();
	
	public LLTPTransFtpXmlCase(){
		
	}
	
	public boolean submitData(String code,String codealias){
		
		if(getSubmitMap(code,codealias)==null){
			
			return false;
		}
		
		return true;
	}
	
	public MMap getSubmitMap(String code,String codealias){
		
		if (!dealData(code,codealias))
        {
            return null;
        }

		return map;
	}
	
	private boolean dealData(String code,String codealias){
	
        if (!getXls(code,codealias))
        {
            return false;
        }

		return true;
	}

	private boolean getXls(String code,String codealias){
		//获取根目录
		String tUIRoot= CommonBL.getUIRoot();
		if(tUIRoot==null){
			mErrors.addOneError("没有查到应用目录");
			return false;
		}
		System.out.println(tUIRoot);
		
//		tUIRoot="E:\\wdx\\CASE\\";//本地测试
		
		ExeSQL tExeSQL = new ExeSQL();
		
		 int xmlCount = 0;
		 String MngComs = "select code,codealias from ldcode where codetype = '"+code+"ComCode'";
			SSRS mngcom = tExeSQL.execSQL(MngComs);
			int MngComsNo = mngcom.getMaxRow();
			
			if(mngcom.MaxRow<=0){
				return false;
			}
			for (int is = 1; is <= MngComsNo; is++) {//具有多个机构
				
				String mng = mngcom.GetText(is, 1);
				System.out.println("开始外包方"+code+"机构："+mng);
		String tServerIP = tExeSQL
                .getOneValue("select codename from ldcode where codetype='"+codealias+"TPClaim' and code='IP/Port'");
		String tPort = tExeSQL
                .getOneValue("select codealias from ldcode where codetype='"+codealias+"TPClaim' and code='IP/Port'");
		String tUsername = tExeSQL
                .getOneValue("select codename from ldcode where codetype='"+codealias+"TPClaim' and code='User/Pass'");
		String tPassword = tExeSQL
                .getOneValue("select codealias from ldcode where codetype='"+codealias+"TPClaim' and code='User/Pass'");

		String tFileRgtkPath = tExeSQL
        		.getOneValue("select codename from ldcode where codetype='"+codealias+"TPClaim' and code='XmlPath'").replace("8631", code);
		String tFileCasePath = tExeSQL
				.getOneValue("select codealias from ldcode where codetype='"+codealias+"TPClaim' and code='XmlPath'").replace("8631", code);

		String thridcompany = tExeSQL.getOneValue("select comcode from ldcode where codetype='"+codealias+"TPClaim' and code='IP/Port'");
        
		 tFileRgtkPath = tUIRoot + tFileRgtkPath;   //核心系统批次存储路径
		 tFileCasePath = tUIRoot + tFileCasePath;   //核心系统案件存储路径
		 
		 
		 FTPTool tFTPTool = new FTPTool(tServerIP,tUsername,tPassword,Integer.parseInt(tPort),"aaActiveMode");
		 try {
			if(!tFTPTool.loginFTP()){
				 CError tError = new CError();
				 tError.moduleName="LLTPTransFtpXmlCase";
				 tError.functionName="getXls";
				 tError.errorMessage=tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				 mErrors.addOneError(tError);
				 System.out.println(tError.errorMessage);	 
				 return false;
			 }
			
			System.out.println("====FTP统一外包案件信息登录成功===");
			
			String tFilePathLocalCore = tFileCasePath+PubFun.getCurrentDate()+"/";//服务器存放案件报文文件的路径 每天生成一个文件夹
			
			String tFileImportPath = "/"+thridcompany+"/TPJK/" + mng + "/TPClaimCase";//ftp上存放文件的目录
            String tFileImportBackPath = "/"+thridcompany+"/TPJK/" + mng + "/TPClaimCaseBack";//ftp上存放核心返回报文文件的目录
            String tFileImportPathBackUp = "/"+thridcompany+"/TPJK/" + mng + "/TPClaimBackUp/" +PubFun.getCurrentDate()+"/";//备份目录
            String tFilePathout = tFilePathLocalCore+"Back/";   // 核心返回报文存储路径
            
//             tFilePathout = tFilePathLocalCore+"Back\\";  //本地测试
            
             //一天可以进入多次，每次进入便新建一个文件夹
             
            String tDate = PubFun.getCurrentDate();
            tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                  + tDate.substring(8, 10);
            String tComtop="TP"+tDate;
            String ResultMax = tFilePathout+"upload/" +(tComtop + PubFun1.CreateMaxNo(tComtop, 5))+"/";
                         
            
             
            File mFileDir = new File(tFilePathLocalCore);
            if(!mFileDir.exists()){
            	if(!mFileDir.mkdirs()){
            		System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
            	}
            }
            
            File tFileDir = new File(tFilePathout);
            if(!tFileDir.exists()){
            	if(!tFileDir.mkdirs()){
            		System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
            	}
            }
            
            
            File tResultMaxDir = new File(ResultMax);
            if(!tResultMaxDir.exists()){
            	if(!tResultMaxDir.mkdirs()){
            		System.err.println("创建目录[" + ResultMax.toString() + "]失败！"
    						+ tResultMaxDir.getPath());
            	}
            }
            
            
            
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
           tFTPTool.setBufferSize(1024*1024*10);
           String[] tPath= tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
           
           tFTPTool.cutDirectiory(tFileImportPath, tFileImportPathBackUp);
           
           String tErrorContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
           
           if (tPath == null && null != tErrorContent)
           {
               mErrors.addOneError(tErrorContent);
               return false;
           }
           // 报文存储
           for(int i=0;i<tPath.length;i++){
        	   if(tPath[i]==null){
        		   continue;
        	   }
           }
           tFTPTool.logoutFTP();//
           // 报文解析
	           for(int j=0;j<tPath.length;j++){
	        	   if(tPath[j]==null){
	        		   continue;
	        	   }
	        	   xmlCount++;
	        	   FileInputStream fis = null;
	        	   FileOutputStream fos = null;
	        	   FileOutputStream fos2 = null;
	        	   try
	               {
		        	   VData tVData = new VData();
		        	   TransferData tTransferData = new TransferData(); 
		        	   tTransferData.setNameAndValue("FileName", tPath[j]);
		        	   tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);
		        	   
		        	   tVData.add(tTransferData);
		        	   tVData.add(mGI);
		        	   
		        	   fis = new FileInputStream(tFilePathLocalCore+tPath[j]);
		        		Document tInXmlDoc =JdomUtil.build(fis,"GBK");
		        	   LLTPAutoCaseRegister tLLTPAutoCaseRegister = new LLTPAutoCaseRegister();
		        	   Document tOutXmlDoc = tLLTPAutoCaseRegister.service(tInXmlDoc,code,codealias,mng);
		        	   
		        	   System.out.println("打印传入报文============");
		               JdomUtil.print(tInXmlDoc.getRootElement());
		               System.out.println("打印传出报文============");
		               JdomUtil.print(tOutXmlDoc);
		               
		               //创建文件流
		              
		               fos = new FileOutputStream(tFilePathout+tPath[j]);
			           JdomUtil.output(tOutXmlDoc,fos);
			           
			           fos2 = new FileOutputStream(ResultMax+tPath[j]);
			           JdomUtil.output(tOutXmlDoc,fos2);
			           
	           } catch (Exception ex)
               {
                   System.out.println("批次导入失败: " + tPath[j]);
                   mErrors.addOneError("批次导入失败" + tPath[j]);
                   ex.printStackTrace();
                }finally {
                	try {
                		if(fis != null) {
                			fis.close();
                		}
                	}finally {
                		if(fos != null) {
                			fos.close();
                		}
                		if(fos2 != null) {
                			fos2.close();
                		}
                	}
                }
               }
	           FTPTool tFTPTool2 = new FTPTool(tServerIP,tUsername,tPassword,Integer.parseInt(tPort),"aaActiveMode");
	           try {
			  			if(!tFTPTool2.loginFTP()){
			  				 CError tError = new CError();
			  				 tError.moduleName="LLTPTransFtpXmlCase";
			  				 tError.functionName="getXls";
			  				 tError.errorMessage=tFTPTool2.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
			  				 mErrors.addOneError(tError);
			  				 System.out.println(tError.errorMessage);	 
			  				 return false;
			  			 } 
		  				System.out.println("====FTP统一外包案件信息，二次登录成功,准备上载返回报文===");
		  				tFTPTool2.setBufferSize(1024*1024*10);
		  				if(tFTPTool2.uploadAll(tFileImportBackPath, ResultMax)){
			            
			            }
			            tFTPTool2.logoutFTP();
				} catch (Exception e) {
					e.printStackTrace();
					 CError tError = new CError();
			         tError.moduleName = "LLTPTransFtpXmlCase";
			         tError.functionName = "getXls";
			         tError.errorMessage = "回传返回报文过程中出现异常";
			         mErrors.addOneError(tError);
			         System.out.println(tError.errorMessage);
			         tFTPTool2.logoutFTP();
			         continue;
				}
	           

		} catch (SocketException e) {
	
			e.printStackTrace();
			 CError tError = new CError();
	         tError.moduleName = "LLTPTransFtpXmlCase";
	         tError.functionName = "getXls";
	         tError.errorMessage = "获取连接失败";
	         mErrors.addOneError(tError);
	         System.out.println(tError.errorMessage);
	         tFTPTool.logoutFTP();
	         continue;
		} catch (IOException e) {
			
			e.printStackTrace();
			CError tError = new CError();
            tError.moduleName = "LLTPTransFtpXmlCase";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            tFTPTool.logoutFTP();
            continue;
		}
		System.out.println("结束外包方"+code+"机构："+mng);
			}
		   if (xmlCount == 0)
	        {
	            mErrors.addOneError("没有需要导入的数据");
	        }
		return true;
	}
	
	public void deleteFile(File file){ 
		   if(file.exists()){                    //判断文件是否存在
		    if(file.isFile()){                    //判断是否是文件
		     file.delete();                       //delete()方法 你应该知道 是删除的意思;
		    }else if(file.isDirectory()){              //否则如果它是一个目录
		     File files[] = file.listFiles();               //声明目录下所有的文件 files[];
		     for(int i=0;i<files.length;i++){            //遍历目录下所有的文件
		      this.deleteFile(files[i]);             //把每个文件 用这个方法进行迭代
		     } 
		    } 
		    file.delete(); 
		   }else{ 
		    System.out.println("所删除的文件不存在！"+'\n'); 
		   } 
		}
	
	public static void main(String[] args) {
		LLTPTransFtpXmlCase t =new LLTPTransFtpXmlCase();
//		ExeSQL tExeSQL = new ExeSQL();
//		String MngComs = "select code,codealias from ldcode where codetype = 'PHOutSourceComCode'";
//		SSRS MngComsSSRS = tExeSQL.execSQL(MngComs);
//		int MngComsNo = MngComsSSRS.getMaxRow();
//		if(MngComsNo>0) {	
//			for (int i = 1; i < MngComsNo; i++) {				
				t.getXls("8695","TPSZ09");
//			}
//		}
	}
}
