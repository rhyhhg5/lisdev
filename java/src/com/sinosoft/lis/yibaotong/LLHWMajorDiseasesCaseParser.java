/**
 * 2015-9-1
 */
package com.sinosoft.lis.yibaotong;

import org.jdom.Element;


/**
 * @author  lyc
 *
 */
public class LLHWMajorDiseasesCaseParser
{
    
    
   
    //节点名称
	private String RGTNO="";//批次号
    private String CLAIMNO="";//平台理赔号
    private String CASENUM="";     //案件顺序号-为返回报文准备
    private String CUSTOMERNAME="";//姓名
    private String IDNO="";     //身份证号
    private String SECURYTINO="";     //医保号
    private String INSUREDSTAT="";     //人员类别
    private String SEX="";     //性别
    private String HOSPITALNAME="";     //医院名称
    private String HOSPITALNAMECODE="";     //医院编码
    private String FEETYPE="";     //就诊类别
    private String RECEIPTNO="";   //账单号码  
    private String FEEDATE="";     //账单日期
    private String HOSPSTARTDATE="";     //入院日期
    private String HOSPENDDATE="";     //出院日期
    private String REALHOSPDATE="";     //住院天数
    private String APPLYAMNT="";     //费用总额
    private String GETLIMIT="";     //起付钱
    private String ACCOUNTPAY="";     //账户支付
    private String PLANFEE="";     //统筹支付   
    private String MIDAMNT="";     //统筹自付
    private String HIGHAMNT1="";     //大额救助支付
    private String SUPINHOSFEE="";     //大额救助自付   
    private String SELFPAY1="";     //个人自付
    private String SELFPAY2="";     //部分自付
    private String SELFAMNT="";     //全自费
    private String OTHERORGANAMNT="";     //第三方支付
    private String ACCDATE="";     //发生日期
    private String AFFIXNO="";     //实赔金额   
    private String ORIGINFLAG="";     //医疗标志
    private String GETDUTYCODE="";     //给付责任编码
    private String BANKCODE="";     //银行编码
    private String BANKNAME="";     //银行名称
    private String ACCNAME="";     //账户名
    private String BANKACCNO="";     //账号
    private String GETDUTYKIND="";     //给负责任类型
    private String CUSTOMERNO="";     //客户号   
    private String INPATIENTNO="";    //住院号   
    private String MAKEDATE="";     //经办时间
    
    private String MOBILEPHONE="";     //手机号
    private String REFUSEAMNT="";     //不合理费用
    private String REIMBBURSEMENT="";     //可报销范围内金额
    private String REMARK="";     //扣除明细
    private String ACCPROVINCECODE="" ; //发生地点(省)
    private String ACCCITYCODE ="";     //发生地点(市)
    private String ACCCOUNTYCODE ="";   //发生地点(县)
    private String MEDICARETYPE ="";    //医保类型
    private String FeeAffixType = "";//账单类型
    
    private boolean mDealState=true;
    
    Element DISEASELIST; //诊疗信息
    
    Element IMAGEINFOS; // 扫描件信息
    
    

	/**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO 自动生成方法存根

    }
    
    /**解析报文返回理赔信息
     * @author lyc
     */
    public LLHWMajorDiseasesCaseParser(Element mLLcase){
    	
    	RGTNO=mLLcase.getChildTextTrim("RGTNO");
        CLAIMNO=mLLcase.getChildTextTrim("CLAIMNO");
        CASENUM=mLLcase.getChildTextTrim("CASENUM");
        CUSTOMERNAME=mLLcase.getChildTextTrim("CUSTOMERNAME");
        IDNO=mLLcase.getChildTextTrim("IDNO");
        SECURYTINO=mLLcase.getChildTextTrim("SECURYTINO");
        INSUREDSTAT=mLLcase.getChildTextTrim("INSUREDSTAT");
        SEX=mLLcase.getChildTextTrim("SEX");
        HOSPITALNAME=mLLcase.getChildTextTrim("HOSPITALNAME");
        HOSPITALNAMECODE=mLLcase.getChildTextTrim("HOSPITALNAMECODE");
        FEETYPE=mLLcase.getChildTextTrim("FEETYPE");
        RECEIPTNO=mLLcase.getChildTextTrim("RECEIPTNO");
        FEEDATE=mLLcase.getChildTextTrim("FEEDATE");
        HOSPSTARTDATE=mLLcase.getChildTextTrim("HOSPSTARTDATE");
        HOSPENDDATE=mLLcase.getChildTextTrim("HOSPENDDATE");
        REALHOSPDATE=mLLcase.getChildTextTrim("REALHOSPDATE");
        APPLYAMNT=mLLcase.getChildTextTrim("APPLYAMNT");
        GETLIMIT=mLLcase.getChildTextTrim("GETLIMIT");
        ACCOUNTPAY=mLLcase.getChildTextTrim("ACCOUNTPAY");
        PLANFEE=mLLcase.getChildTextTrim("PLANFEE");
        MIDAMNT=mLLcase.getChildTextTrim("MIDAMNT");
        HIGHAMNT1=mLLcase.getChildTextTrim("HIGHAMNT1");
        SUPINHOSFEE=mLLcase.getChildTextTrim("SUPINHOSFEE");
        SELFPAY1=mLLcase.getChildTextTrim("SELFPAY1");
        SELFPAY2=mLLcase.getChildTextTrim("SELFPAY2");
        SELFAMNT=mLLcase.getChildTextTrim("SELFAMNT");
        OTHERORGANAMNT=mLLcase.getChildTextTrim("OTHERORGANAMNT");     
        ACCDATE=mLLcase.getChildTextTrim("ACCDATE");
        AFFIXNO=mLLcase.getChildTextTrim("AFFIXNO");
        ORIGINFLAG=mLLcase.getChildTextTrim("ORIGINFLAG");
        GETDUTYCODE=mLLcase.getChildTextTrim("GETDUTYCODE");
        BANKCODE=mLLcase.getChildTextTrim("BANKCODE");
        BANKNAME=mLLcase.getChildTextTrim("BANKNAME");
        ACCNAME=mLLcase.getChildTextTrim("ACCNAME");
        BANKACCNO=mLLcase.getChildTextTrim("BANKACCNO");
        GETDUTYKIND=mLLcase.getChildTextTrim("GETDUTYKIND");
        CUSTOMERNO=mLLcase.getChildTextTrim("CUSTOMERNO");
        INPATIENTNO=mLLcase.getChildTextTrim("INPATIENTNO");
        MAKEDATE=mLLcase.getChildTextTrim("MAKEDATE");
        MOBILEPHONE=mLLcase.getChildTextTrim("MOBILEPHONE");
        REFUSEAMNT=mLLcase.getChildTextTrim("REFUSEAMNT");
        REIMBBURSEMENT=mLLcase.getChildTextTrim("REIMBURSEMENT");
        REMARK=mLLcase.getChildTextTrim("REMARK");
        ACCPROVINCECODE=mLLcase.getChildTextTrim("ACCPROVINCECODE");
        ACCCITYCODE=mLLcase.getChildTextTrim("ACCCITYCODE");
        ACCCOUNTYCODE =mLLcase.getChildTextTrim("ACCCOUNTYCODE");
        MEDICARETYPE= mLLcase.getChildTextTrim("MEDICARETYPE");
        //账单类型
        FeeAffixType = mLLcase.getChildTextTrim("FEEAFFIXTYPE");
//        诊疗信息
         DISEASELIST = mLLcase.getChild("DISEASELIST");
// 		扫描件        
         IMAGEINFOS = mLLcase.getChild("ImageInfos");
         
   
    }


 
    
	public String getFeeAffixType() {
		return FeeAffixType;
	}

	public void setFeeAffixType(String feeAffixType) {
		FeeAffixType = feeAffixType;
	}

	public boolean ismDealState() {
		return mDealState;
	}

	public Element getDISEASELIST() {
		return DISEASELIST;
	}

	public void setDISEASELIST(Element dISEASELIST) {
		DISEASELIST = dISEASELIST;
	}

	public Element getIMAGEINFOS() {
		return IMAGEINFOS;
	}

	public void setIMAGEINFOS(Element iMAGEINFOS) {
		IMAGEINFOS = iMAGEINFOS;
	}

	public void setmDealState(boolean mDealState) {
		this.mDealState = mDealState;
	}

	

	public String getRGTNO() {
		return RGTNO;
	}

	public void setRGTNO(String rGTNO) {
		RGTNO = rGTNO;
	}

	public String getCLAIMNO() {
		return CLAIMNO;
	}

	public void setCLAIMNO(String cLAIMNO) {
		CLAIMNO = cLAIMNO;
	}

	public String getCASENUM() {
		return CASENUM;
	}

	public void setCASENUM(String cASENUM) {
		CASENUM = cASENUM;
	}

	public String getCUSTOMERNAME() {
		return CUSTOMERNAME;
	}

	public void setCUSTOMERNAME(String cUSTOMERNAME) {
		CUSTOMERNAME = cUSTOMERNAME;
	}

	public String getIDNO() {
		return IDNO;
	}

	public void setIDNO(String iDNO) {
		IDNO = iDNO;
	}

	public String getSECURYTINO() {
		return SECURYTINO;
	}

	public void setSECURYTINO(String sECURYTINO) {
		SECURYTINO = sECURYTINO;
	}

	public String getINSUREDSTAT() {
		return INSUREDSTAT;
	}

	public void setINSUREDSTAT(String iNSUREDSTAT) {
		INSUREDSTAT = iNSUREDSTAT;
	}

	public String getSEX() {
		return SEX;
	}

	public void setSEX(String sEX) {
		SEX = sEX;
	}

	public String getHOSPITALNAME() {
		return HOSPITALNAME;
	}

	public void setHOSPITALNAME(String hOSPITALNAME) {
		HOSPITALNAME = hOSPITALNAME;
	}

	public String getHOSPITALNAMECODE() {
		return HOSPITALNAMECODE;
	}

	public void setHOSPITALNAMECODE(String hOSPITALNAMECODE) {
		HOSPITALNAMECODE = hOSPITALNAMECODE;
	}

	public String getFEETYPE() {
		return FEETYPE;
	}

	public void setFEETYPE(String fEETYPE) {
		FEETYPE = fEETYPE;
	}

	public String getRECEIPTNO() {
		return RECEIPTNO;
	}

	public void setRECEIPTNO(String rECEIPTNO) {
		RECEIPTNO = rECEIPTNO;
	}

	public String getFEEDATE() {
		return FEEDATE;
	}

	public void setFEEDATE(String fEEDATE) {
		FEEDATE = fEEDATE;
	}

	public String getHOSPSTARTDATE() {
		return HOSPSTARTDATE;
	}

	public void setHOSPSTARTDATE(String hOSPSTARTDATE) {
		HOSPSTARTDATE = hOSPSTARTDATE;
	}

	public String getHOSPENDDATE() {
		return HOSPENDDATE;
	}

	public void setHOSPENDDATE(String hOSPENDDATE) {
		HOSPENDDATE = hOSPENDDATE;
	}

	public String getREALHOSPDATE() {
		return REALHOSPDATE;
	}

	public void setREALHOSPDATE(String rEALHOSPDATE) {
		REALHOSPDATE = rEALHOSPDATE;
	}

	public String getAPPLYAMNT() {
		return APPLYAMNT;
	}

	public void setAPPLYAMNT(String aPPLYAMNT) {
		APPLYAMNT = aPPLYAMNT;
	}

	public String getGETLIMIT() {
		return GETLIMIT;
	}

	public void setGETLIMIT(String gETLIMIT) {
		GETLIMIT = gETLIMIT;
	}

	public String getACCOUNTPAY() {
		return ACCOUNTPAY;
	}

	public void setACCOUNTPAY(String aCCOUNTPAY) {
		ACCOUNTPAY = aCCOUNTPAY;
	}

	public String getPLANFEE() {
		return PLANFEE;
	}

	public void setPLANFEE(String pLANFEE) {
		PLANFEE = pLANFEE;
	}

	public String getMIDAMNT() {
		return MIDAMNT;
	}

	public void setMIDAMNT(String mIDAMNT) {
		MIDAMNT = mIDAMNT;
	}

	public String getHIGHAMNT1() {
		return HIGHAMNT1;
	}

	public void setHIGHAMNT1(String hIGHAMNT1) {
		HIGHAMNT1 = hIGHAMNT1;
	}

	public String getSUPINHOSFEE() {
		return SUPINHOSFEE;
	}

	public void setSUPINHOSFEE(String sUPINHOSFEE) {
		SUPINHOSFEE = sUPINHOSFEE;
	}

	public String getSELFPAY1() {
		return SELFPAY1;
	}

	public void setSELFPAY1(String sELFPAY1) {
		SELFPAY1 = sELFPAY1;
	}

	public String getSELFPAY2() {
		return SELFPAY2;
	}

	public void setSELFPAY2(String sELFPAY2) {
		SELFPAY2 = sELFPAY2;
	}

	public String getSELFAMNT() {
		return SELFAMNT;
	}

	public void setSELFAMNT(String sELFAMNT) {
		SELFAMNT = sELFAMNT;
	}

	public String getOTHERORGANAMNT() {
		return OTHERORGANAMNT;
	}

	public void setOTHERORGANAMNT(String oTHERORGANAMNT) {
		OTHERORGANAMNT = oTHERORGANAMNT;
	}


	public String getACCDATE() {
		return ACCDATE;
	}

	public void setACCDATE(String aCCDATE) {
		ACCDATE = aCCDATE;
	}

	public String getAFFIXNO() {
		return AFFIXNO;
	}

	public void setAFFIXNO(String aFFIXNO) {
		AFFIXNO = aFFIXNO;
	}

	public String getORIGINFLAG() {
		return ORIGINFLAG;
	}

	public void setORIGINFLAG(String oRIGINFLAG) {
		ORIGINFLAG = oRIGINFLAG;
	}

	public String getGETDUTYCODE() {
		return GETDUTYCODE;
	}

	public void setGETDUTYCODE(String gETDUTYCODE) {
		GETDUTYCODE = gETDUTYCODE;
	}

	public String getBANKCODE() {
		return BANKCODE;
	}

	public void setBANKCODE(String bANKCODE) {
		BANKCODE = bANKCODE;
	}

	public String getBANKNAME() {
		return BANKNAME;
	}

	public void setBANKNAME(String bANKNAME) {
		BANKNAME = bANKNAME;
	}

	public String getACCNAME() {
		return ACCNAME;
	}

	public void setACCNAME(String aCCNAME) {
		ACCNAME = aCCNAME;
	}

	public String getBANKACCNO() {
		return BANKACCNO;
	}

	public void setBANKACCNO(String bANKACCNO) {
		BANKACCNO = bANKACCNO;
	}

	public String getGETDUTYKIND() {
		return GETDUTYKIND;
	}

	public void setGETDUTYKIND(String gETDUTYKIND) {
		GETDUTYKIND = gETDUTYKIND;
	}

	public String getCUSTOMERNO() {
		return CUSTOMERNO;
	}

	public void setCUSTOMERNO(String cUSTOMERNO) {
		CUSTOMERNO = cUSTOMERNO;
	}

	public String getINPATIENTNO() {
		return INPATIENTNO;
	}

	public void setINPATIENTNO(String iNPATIENTNO) {
		INPATIENTNO = iNPATIENTNO;
	}

	public String getMAKEDATE() {
		return MAKEDATE;
	}

	public void setMAKEDATE(String mAKEDATE) {
		MAKEDATE = mAKEDATE;
	}

	public String getMOBILEPHONE() {
		return MOBILEPHONE;
	}

	public void setMOBILEPHONE(String mOBILEPHONE) {
		MOBILEPHONE = mOBILEPHONE;
	}

	public String getREFUSEAMNT() {
		return REFUSEAMNT;
	}

	public void setREFUSEAMNT(String rEFUSEAMNT) {
		REFUSEAMNT = rEFUSEAMNT;
	}

	public String getREIMBBURSEMENT() {
		return REIMBBURSEMENT;
	}

	public void setREIMBBURSEMENT(String rEIMBBURSEMENT) {
		REIMBBURSEMENT = rEIMBBURSEMENT;
	}

	public String getREMARK() {
		return REMARK;
	}

	public void setREMARK(String rEMARK) {
		REMARK = rEMARK;
	}

	public String getACCPROVINCECODE() {
		return ACCPROVINCECODE;
	}

	public void setACCPROVINCECODE(String aCCPROVINCECODE) {
		ACCPROVINCECODE = aCCPROVINCECODE;
	}

	public String getACCCITYCODE() {
		return ACCCITYCODE;
	}

	public void setACCCITYCODE(String aCCCITYCODE) {
		ACCCITYCODE = aCCCITYCODE;
	}

	public String getACCCOUNTYCODE() {
		return ACCCOUNTYCODE;
	}

	public void setACCCOUNTYCODE(String aCCCOUNTYCODE) {
		ACCCOUNTYCODE = aCCCOUNTYCODE;
	}

	public String getMEDICARETYPE() {
		return MEDICARETYPE;
	}

	public void setMEDICARETYPE(String mEDICARETYPE) {
		MEDICARETYPE = mEDICARETYPE;
	}
    
   


}
