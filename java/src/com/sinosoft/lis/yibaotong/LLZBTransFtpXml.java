package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLZBTransFtpXml {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();
    
    public void LLZBTransFtpXml(){
    	
    }
    
    public boolean submitData(){
    	if (getSubmitMap() == null)
        {
            return false;
        }
    	
    	return true;
    }
    
    public MMap getSubmitMap()
    {

        if (!dealData())
        {
            return null;
        }

        return map;
    }
    
    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }
    
    private boolean getXls()
    {
        String tUIRoot = CommonBL.getUIRoot();

        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }
         // 本地测试用
//        tUIRoot ="D:\\picc\\ui\\";
        
        int xmlCount = 0;

        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype='ZBClaim' and code='IP/Port'");
        String tPort = tExeSql
                .getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='IP/Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype='ZBClaim' and code='User/Pass'");
        String tPassword = tExeSql
                .getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='User/Pass'");

        String tFileRgtkPath = tExeSql
                .getOneValue("select codename from ldcode where codetype='ZBClaim' and code='XmlPath'");
        
        String tFileCasePath = tExeSql
        		.getOneValue("select codealias from ldcode where codetype='ZBClaim' and code='XmlPath'");

        //本地测试
//        tFileRgtkPath ="temp_lp\\WBClaim\\8633\\RgtXml\\";
//        tFileCasePath="temp_lp\\WBClaim\\8633\\CaseXml\\";
        
        tFileRgtkPath = tUIRoot + tFileRgtkPath;    //批次核心存储路径
        tFileCasePath = tUIRoot + tFileCasePath;    //案件核心存储路径

        try
        {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "LLZBTransFtpXml";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
           System.out.println("FTP=====浙江外包批次信息登录成功。");
            
            String tFilePathLocalCore = tFileRgtkPath+PubFun.getCurrentDate()+"/";//服务器存放文件的路径 每天生成一个文件夹
            //本地测试
//            tFilePathLocalCore = tFileRgtkPath+PubFun.getCurrentDate()+"\\";//服务器存放文件的路径 每天生成一个文件夹
            
            String tFileImportPath = "/01PH/8633/ZWBClaimRgt";//ftp上存放文件的目录
            String tFileImportBackPath = "/01PH/8633/ZWBClaimRgtBack";//ftp上返回文件目录
            String tFileImportPathBackUp = "/01PH/8633/ZWBClaimBackUp/"+PubFun.getCurrentDate()+"/";//FTP备份目录
            String tFilePathout = tFilePathLocalCore+"Back/";  //服务器存放返回报文路径
            //本地测试
//            tFilePathout = tFilePathLocalCore+"Back\\";  //服务器存放返回报文路径
            
            File mFileDir = new File(tFilePathLocalCore);
    		if (!mFileDir.exists()) {
    			if (!mFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
    			}
    		}
    		
            File tFileDir = new File(tFilePathout);
    		if (!tFileDir.exists()) {
    			if (!tFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
    			}
    		}
    		
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
//           if(!tFTPTool.makeDirectory(tFileImportBackPath)){
//        	   System.out.println("FTP ===返回文件夹已存在");
//           }
            
            System.out.println("OK");
            	
            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                return false;
            }

            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }


                xmlCount++;

                try
                {
                    VData tVData = new VData();

                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("FileName", tPath[j]);
                    tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);

                    tVData.add(tTransferData);
                    tVData.add(mGI);

                    Document tInXmlDoc;   
                    tInXmlDoc = JdomUtil.build(new FileInputStream(tFilePathLocalCore+tPath[j]), "GBK");
                    LLZBRgtRegister tBusinessDeal = new LLZBRgtRegister();
                    Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
                    System.out.println("打印传入报文============");
                    JdomUtil.print(tInXmlDoc.getRootElement());
                    System.out.println("打印传出报文============");
                    JdomUtil.print(tOutXmlDoc);
                    
                    JdomUtil.output(tOutXmlDoc, new FileOutputStream(tFilePathout+tPath[j]));
 
                    tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[j]);
                    
                    tFTPTool.changeWorkingDirectory(tFileImportPath);
                    tFTPTool.deleteFile(tPath[j]);
                    
                    tFTPTool.upload(tFileImportBackPath, tFilePathout+tPath[j]);
//                    tFTPTool.deleteFile(tPath[j]);

                }
                catch (Exception ex)
                {
                    System.out.println("批次导入失败: " + tPath[j]);
                    mErrors.addOneError("批次导入失败" + tPath[j]);
                    ex.printStackTrace();
                  
                }
            }
            tFTPTool.logoutFTP();

        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LLZBTransFtpXml";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LLZBTransFtpXml";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }
    
    
	
	public static void main(String[] args) {
		LLZBTransFtpXml a =new LLZBTransFtpXml();
    	a.getXls();
		
	}

}
