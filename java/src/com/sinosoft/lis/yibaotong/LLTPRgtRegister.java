package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LLClaimUserSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;

/**
 * <p>Title: 外包批次处理-申请批次</p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author lyc
 * @version 1.0
 */
public class LLTPRgtRegister{
    public LLTPRgtRegister() {
    }

    public CErrors mErrors = new CErrors();
//    private VData mInputData = new VData();
    public VData mResult = new VData();
//    private GlobalInput mGlobalInput = new GlobalInput(); 
    private MMap mMMap = new MMap();
    private String mErrorMessage = "";	//错误描述    
   
    private Document mInXmlDoc;	 			//传入报文    
    private String mngs;
    private String mDocType = "";			//报文类型    
    private boolean mDealState = true;		//处理标志    
    private String mResponseCode = "1";		//返回类型代码    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码
//    private String mDealType = "1";    		//处理类型 1 实时 2非实时   
    
    private Element mBaseData;			//报文BATCH _DATA批次立案信息部分    
    private String mComCode="";     //报文中的用户机构节点的值。传递过来的用户机构
    private String mUserCode="";   //报文中的用户 节点的值。传递过来的用户 
    private String mManageCom = "";				//交易编码中的8位机构编码   
    private String mOperator  = " ";				//操作员    
    
    /**
     * 对方报文传入信息
     */
    private String mGrpContNo = "";//团体保单号
    private String mRgtantname = "";//联系人
    private String mIDtype = "";//证件类型
    private String mIdno = "";//证件号码
    private String mIdStartDate = "";//证件生效日期
    private String mIdEndDate = "";//证件失效日期
    private String mRgtantphone = "";//联系电话
    private String mRgtantadress = "";//联系地址
    private String mAppPeoples = "";//申请人数(包含录入申请人数)
    private String mTogetherFlag = "";//给付方式
    private String mCaseGetMode = "";//赔款领取方式
    private String mAppAmnt = "";//申报金额
    private String mBankCode = "";//银行编码
    private String mBankName = "";//银行名称
    private String mAccName = "";//账户名
    private String mBankAccNo = "";//账号
    
    /**
     * 生成批次必要信息
     */
    private String mRgtNo = "" ;
    private String mCustomerNo = "";//团体客户号
    private String mGrpName = "";//单位名称
    private String mRgtantPostCode = "";//邮政编码，对应表中立案人/申请人邮政编码“申请人邮政编码”
    
    
    private ExeSQL mExeSQL = new ExeSQL();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();

    public Document service(Document pInXmlDoc,String code,String codealias,String mng) {
        //程序处理接口
        System.out.println("LLTPRgtRegister--service");
        mInXmlDoc = pInXmlDoc;
        mngs=mng; 
        try {
            if (!getInputData(mInXmlDoc,mngs)) {
                mDealState = false;
            } 

            if (!checkData()) {
                mDealState = false;
            } else {
                if (!dealData()) {
                    mDealState = false;
                }else {                                 	                    	
                	mResult.clear();
                	mResult.add(mMMap);
                	PubSubmit tPubSubmit = new PubSubmit();
					if (!tPubSubmit.submitData(mResult, "")) {
						 mDealState = false;
						 buildError("service()", "数据提交报错");
					}
                }
            }
        
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	mInXmlDoc = createXML();
        }
    	return mInXmlDoc;
    }
    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
        	try {
        		if("TP01".equals(mRequestType)){
        			return createResultXML(mRgtNo);
        		}else{
        			return createFalseXML();
        		}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }     

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml,String mngs) throws Exception {
        System.out.println("LLTPRgtRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }
        if (mngs == null) {
            buildError("getInputData()", "未获得要处理的机构编码,请检查机构与版本号之间的映射关系是否配置");
            return false;
        }
        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");//此字段无实际意义

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");//报文类型-主要信息
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");//会进行唯一性校验
        System.out.println("LLTPRgtRegister--TransactionNum:" + mTransactionNum);
        
        if("TP01".equals(mRequestType))
        {       	
        	//获取BODY部分内容
            Element tBodyData = tRootData.getChild("BODY");
            mBaseData = tBodyData.getChild("BATCH_DATA");		//批次申请基本信息 
            mUserCode = tBodyData.getChild("USER_DATA").getChildText("USERCODE");//用户编码
            mComCode = tBodyData.getChild("USER_DATA").getChildText("MANAGECOM");//机构
//            List tBaseData = new ArrayList();
//            tBaseData = (List)tBodyData.getChildren();
//            if(tBaseData.size()!=1){
//            	buildError("dealData", "【团体客户信息】为空或存在多条,请核对");
//                return false;
//            }
        }
        
        return true;

    }
    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLTPRgtRegister--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"TP01".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误");
            return false;
        }

        if (!"TP01".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }
//111111交易编码，格式校验
        mManageCom = mTransactionNum.substring(4, 12);   //交易编码的 8位机构       
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null") ) { //这个判断，没啥意义，
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
        String Manage2  = mManageCom.substring(0, 2);   
        if (!"86".equals(Manage2) ) {
            buildError("checkData()", "【交互编码】的管理机构编码错误,需为86开头");
            return false;
        }
        String Manage1  = mManageCom.substring(2, 4);   
        if ("00".equals(Manage1) ) {
            buildError("checkData()", "【交互编码】的管理机构编码错误,不能为总公司机构编码");
            return false;
        }
       
        String Manage4  = mManageCom.substring(4, 8);   
        if (!"0000".equals(Manage4) ) {
            buildError("checkData()", "【交互编码】的管理机构编码后四位应为0000");
            return false;
        }
      //校验理赔用户
		LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(mUserCode);
    	tLLClaimUserDB.setClaimDeal("1");  //理赔权限
    	//不对用户进行分配权限制
    	//tLLClaimUserDB.setHandleFlag("1"); //案件分配权限
    	tLLClaimUserDB.setStateFlag("1"); //理赔员状态
    	
    	LLClaimUserSet tLLClaimUserSet =tLLClaimUserDB.query();
    	if(tLLClaimUserSet.size()<=0){
    		buildError("dealBasicData", "【用户编码】查询失败或没有权限");
    		return false;
    	}
    	
//22222报文中，管理机构格式校验
    	if ("".equals(mComCode) || mComCode == null || mComCode.equals("null") ) {  
            buildError("checkData()", "【管理机构】不能为空。");
            return false;
        }
    	
    	String tMngCom ="";//报文中管理机构的4位用户机构
    	if(mComCode.length()==4|| mComCode.length()==8 ||mComCode.length()==6){ //报文中的用户机构8600
    		 tMngCom =mComCode.substring(0, 4);
    	}else{
    		buildError("dealBasicData", "【管理机构】应为4位，6位或8位机构。");
    		return false;	
    	}
    	String Manage86  = tMngCom.substring(0, 2);   
        if (!"86".equals(Manage86) ) {
            buildError("checkData()", "【管理机构】编码错误,需为86开头");
            return false;
        }
    	
        String Manage00  = tMngCom.substring(2, 4);   
        if ("00".equals(Manage00) ) {
            buildError("checkData()", "【管理机构】编码错误,不能为8600");
            return false;
        }
        
    	String aComCode = tLLClaimUserSet.get(1).getComCode(); //表中的用户机构
//    	if(!"".equals(aComCode) && aComCode!=null){
//    		if(aComCode.length()==4){
//    			if(!aComCode.equals(tMngCom)){
//    				buildError("dealBasicData", "案件处理人机构和报文中理赔用户信息的管理机构不匹配。");
//    	    		return false;	
//    			}
//    		}else if(aComCode.length()==8 ||aComCode.length()==6){
//    			String newComCode = aComCode.substring(0, 4);
//    			if(!newComCode.equals(tMngCom)){
//    				buildError("dealBasicData", "案件处理人机构和报文中理赔用户信息的管理机构不匹配。");
//    	    		return false;	
//    			}
//    		}else{
//    			buildError("dealBasicData", "案件处理人机构不能为俩位机构。");
//	    		return false;	
//    		}
//    	}else{
//    		buildError("dealBasicData", "案件处理人机构不能为空。");
//    		return false;
//    	}
    	//表中用户的机构校验
    	if ("".equals(aComCode) || aComCode == null || aComCode.equals("null") ) {  
            buildError("checkData()", "核心系统理赔用户"+mUserCode+"的管理机构为空，请联系核心人员更换有效用户。");
            return false;
        }
    	
    	if(aComCode.length()==2){ //报文中的用户机构8600
    		buildError("checkData()", "核心系统理赔用户"+mUserCode+"的管理机构不能为两位机构，请联系核心人员更换用户编码。");
            return false;
    	}
    	String usercom  = "";   ///核心表中用户四位机构
    	if(aComCode.length()==4|| aComCode.length()==8 ||aComCode.length()==6){  
    		usercom =aComCode.substring(0, 4);
    	}else{
    		buildError("dealBasicData", "核心系统理赔用户"+mUserCode+"的管理机构不为4位，6位或8位机构，请联系核心人员更换用户编码。");
    		return false;	
    	}
    	
    	String Manage3  = mManageCom.substring(0, 4);   //交易编码4位机构      
    	
    	if(!Manage3.equals(mngs)){
    		buildError("checkData()", "请求报文推送的FTP路径错误，应推送至FTP的"+Manage3+"机构文件夹下");
            return false;
    	}
    	
    	
    	//报文中交易编码4位机构与报文中管理机构4位机构比较
    	if(!Manage3.equals(tMngCom)){
    		buildError("checkData()", "【交互编码】前4位机构与【管理机构】前4位机构必须相同");
            return false;
    	}
    	
    	//报文中交易编码4位机构与表中用户的4位机构比较
    	if(!Manage3.equals(usercom)){
    		buildError("checkData()", "【交互编码】前4位机构与核心系统理赔用户"+mUserCode+"的前4位机构必须相同");
            return false;
    	}
    	//报文中管理机构编码4位机构与表中用户的4位机构比较
        if (!tMngCom.equals(usercom) ) {
        	buildError("checkData()", "【管理机构】前4位机构与核心系统理赔用户"+mUserCode+"的前4位机构必须相同");
            return false;
        }
    	//表中的用户机构 必须大于 等于报文中的机构
        
//        aComCode 表中机构
//        mComCode 报文中机构
        String aCom="";
        for (int i = 1; i <= aComCode.length(); i++) {
        	String val= aComCode.substring((aComCode.length()-i),aComCode.length()-i+1 ) ;
			System.out.println(aComCode.length()-i);
			System.out.println(aComCode.length()-i+1);
        	System.out.println(val);
        	if(!"0".equals(val)){
				aCom=aComCode.substring(0,aComCode.length()-i+1);
				System.out.println(aCom);
				break;
			}
		}
        String mCom="";
        for (int i = 1; i <= mComCode.length(); i++) {
        	String val= mComCode.substring((mComCode.length()-i),mComCode.length()-i+1 ) ;
        	System.out.println(val);
        	if(!"0".equals(val)){
				mCom=mComCode.substring(0,mComCode.length()-i+1);
				System.out.println(mCom);
				break;
			}
		}
        System.out.println(aCom.length());
        System.out.println(mCom.length());
        if(!"".equals(aCom) && !"".equals(mCom)){
        if( aCom.length()<=mCom.length()){
        	
        	if(mCom.indexOf(aCom)==-1){
        		buildError("checkData()", "核心系统理赔用户机构必须是报文中用户机构的上级或者同级");
        		return false;
        	}
        	
        }else{
        	buildError("checkData()", "核心系统理赔用户机构必须是报文中用户机构的上级或者同级");
            return false;
        }
        }else{
        	buildError("checkData()", "报文中的用户机构为空或者核心系统理赔用户机构为空");
            return false;
        }
        return true;
    }
    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLTPRgtRegister--dealData");  
              
        mGrpContNo = mBaseData.getChildText("GRPCONTNO");//保单号码
        mRgtantname = mBaseData.getChildText("RGTANTNAME");//联系人
        mIDtype = mBaseData.getChildText("IDTYPE");//证件类型
        mIdno = mBaseData.getChildText("IDNO");//证件号码
        mIdStartDate = mBaseData.getChildText("IDSTARTDATE");//证件生效日期
        mIdEndDate = mBaseData.getChildText("IDENDDATE");//证件失效日期
        mRgtantphone = mBaseData.getChildText("RGTANTPHONE");//联系电话
        mRgtantadress = mBaseData.getChildText("RGTANTADDRESS");//联系地址
        mAppPeoples = mBaseData.getChildText("APPPEOPLES");//申请人数
        mTogetherFlag = mBaseData.getChildText("TOGETHERFLAG");//给付方式
        mCaseGetMode = mBaseData.getChildText("CASEGETMODE");//赔款领取方式
        mAppAmnt = mBaseData.getChildText("APPAMNT");//申报金额
        mBankCode = mBaseData.getChildText("BANKCODE");//银行编码
        mBankName = mBaseData.getChildText("BANKNAME");//银行名称
        mAccName = mBaseData.getChildText("ACCNAME");//账户名
        mBankAccNo = mBaseData.getChildText("BANKACCNO");//账号
        
        /*
         * 基本校验
         */      
        if (mGrpContNo == null || "".equals(mGrpContNo) || mGrpContNo.equals("null")) {
            buildError("dealData", "【保单号码】的值不能为空");
            return false;
        }
        
//        if(mIDtype == null || "".equals(mIDtype) || "null" .equals(mIDtype)) {
//        	buildError("dealData", "【证件类型】的值不能为空");
//            return false;
//        }
        if(mIdStartDate == null || "".equals(mIdStartDate)){
        	buildError("dealData", "【证件生效日期】有误");
            return false;
        }
        SimpleDateFormat mFormat = new SimpleDateFormat ("yyyy-MM-dd");
        mFormat.setLenient(false);
        boolean bool = false;
        Date mDate;
        try{
            mDate = mFormat.parse(mIdStartDate);
            bool = true;
        } catch (Exception e) {
		}
        
        if(!bool){
        	buildError("dealData", "【证件生效日期】有误");
            return false;
        }
       
        if(mIdEndDate != null && !"".equals(mIdEndDate)){
        	bool = false;
        	 try{
                 mDate = mFormat.parse(mIdEndDate);
                 bool = true;
             } catch (Exception e) {
            	 
     		 }
             if(!bool){
             	buildError("dealData", "【证件失效日期】有误");
                 return false;
             }
        }
       
        if (mAppPeoples == null || "".equals(mAppPeoples) || mAppPeoples.equals("null")) {
            buildError("dealData", "【申请人数】的值不能为空");
            return false;
        }
        if (mTogetherFlag == null || "".equals(mTogetherFlag)|| mTogetherFlag.equals("null")) {
        	mTogetherFlag = "3";//默认为全部统一给付
        }
        if (!mTogetherFlag.equals("1") && !mTogetherFlag.equals("2")  && !mTogetherFlag.equals("3")  && !mTogetherFlag.equals("4") ) {
        	buildError("dealData", "【给付方式】的代码不在指定代码范围内");
        	return false;
        }
        if (mCaseGetMode == null || "".equals(mCaseGetMode)|| mCaseGetMode.equals("null")) {
        	buildError("dealData", "【赔款领取方式】的值不能为空");
        	return false;
        }
        if (!mCaseGetMode.equals("1") &&!mCaseGetMode.equals("2") &&  !mCaseGetMode.equals("3") && !mCaseGetMode.equals("4") && !mCaseGetMode.equals("11")) {
        	buildError("dealData", "【赔款领取方式】的代码不在指定代码范围内");
        	return false;
        }
        if (mAppAmnt == null || "".equals(mAppAmnt) || mAppAmnt.equals("null")) {
            buildError("dealData", "【申报金额】的值不能为空");
            return false;
        }
        
        //对以下校验进行优化，具体需求，详见3817，上海
        if(!"1".equals(mTogetherFlag) && !"2".equals(mTogetherFlag)&&!mCaseGetMode.equals("1") &&!mCaseGetMode.equals("2") )
    	{
            if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
                buildError("dealData", "【银行编码】的值不能为空");
                return false;
            }
            if (mBankName == null || "".equals(mBankName)|| mBankName.equals("null")) {
                buildError("dealData", "【银行名称】的值不能为空");
                return false;
            }
            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
                buildError("dealData", "【账户名】的值不能为空");
                return false;
            }
            if (mBankAccNo == null || "".equals(mBankAccNo)|| mBankAccNo.equals("null")) {
                buildError("dealData", "【账号】的值不能为空");
                return false;
            }
            Pattern tPattern = Pattern.compile("(\\d)+");
    		Matcher isNum = tPattern.matcher(mBankAccNo);

    		if (!isNum.matches()) {
    			buildError("dealData", "【账号】只能是数字组成");
    			return false;
    		}
    	}
      
       
                  
        /*
         * 批次受理
         */
    	if (!RgtRegister()) {                        	
    		return false;
    	}             		              
          

        return true;
    }
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String mRgtNo) throws Exception {
    	 System.out.println("LLTPRgtRegister--createResultXML(正常返回报文)");
    	 Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);

         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BACK_DATA");
         tBodyData.addContent(tBackData);        
         Element tRgtNo = new Element("RGTNO");
         tRgtNo.setText(mRgtNo);
         tBackData.addContent(tRgtNo);                
         tRootData.addContent(tBodyData); 

         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
    	System.out.println("LLTPRgtRegister--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        //Head部分
        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        tRootData.addContent(tHeadData);

        //错误编码转换
        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        //Body部分
        Element tBodyData = new Element("BODY");
        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBodyData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);
        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
   
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTPRgtRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
   
    /**
     * 解析批次受理信息
     * @return boolean
     * 
     */
    private boolean RgtRegister() throws Exception {
        System.out.println("LLTPRgtRegister--caseRegister");     
        
        //1.通过团体保单号查询相关基础信息
        String tGrpContSql = "select a.customerno, a.name " +
        		"from lcgrpcont g, LCGrpAppnt a " +
        		"where a.grpcontno = g.grpcontno and g.appflag = '1' " +
        		"and g.GrpContNo='"+mGrpContNo+"' with ur ";
        SSRS tGrpContSSRS = new SSRS();
        tGrpContSSRS = mExeSQL.execSQL(tGrpContSql);
        if (tGrpContSSRS == null || tGrpContSSRS.getMaxRow() <= 0) {
            buildError("RgtRegister", "【保单号码】在核心不存在");
            return false;
        }else{
            for(int i=1;i <=tGrpContSSRS.getMaxRow();i++){
                mCustomerNo = tGrpContSSRS.GetText(i, 1);
                mGrpName = tGrpContSSRS.GetText(i, 2);                
                //判断单位名称过长的话要截取
                if(mGrpName.length() >19){
                	mGrpName=mGrpName.substring(0, 19);
                }
            }
        }
        
        //2.生成批次信息
        String result = LLCaseCommon.checkGrp(mGrpContNo);
        if(!"".equals(result)){
            buildError("dealData()","工单号为"+result+"的保单管理现正对该团单进行保全操作，请先通知保全撤销相关操作，再进行理赔!");
            mDealState = false; 
            return false;
        }        
        String tLimit = PubFun.getNoLimit(mComCode);
        System.out.println("管理机构代码是 : "+mManageCom);
        String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
        
        mRgtNo=RGTNO;
        System.out.println("getInputData start......");
        
        mLLRegisterSchema.setRgtNo(mRgtNo);
        mLLRegisterSchema.setRgtState("01");//
        mLLRegisterSchema.setRgtObj("0");
        mLLRegisterSchema.setRgtType("12");//默认为12-外包平台
        mLLRegisterSchema.setRgtClass("1");
        mLLRegisterSchema.setRgtObjNo(mGrpContNo);
        mLLRegisterSchema.setAppPeoples(mAppPeoples);
        mLLRegisterSchema.setTogetherFlag(mTogetherFlag);
        mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
        mLLRegisterSchema.setAppAmnt(mAppAmnt);
    	mLLRegisterSchema.setBankCode(mBankCode);
    	mLLRegisterSchema.setAccName(mAccName); 
      	mLLRegisterSchema.setBankAccNo(mBankAccNo);
      	mLLRegisterSchema.setRgtantAddress(mRgtantadress);	
      	mLLRegisterSchema.setRgtantPhone(mRgtantphone);	
      	mLLRegisterSchema.setRgtantName(mRgtantname);
      	mLLRegisterSchema.setIDType(mIDtype);
      	mLLRegisterSchema.setIDNo(mIdno);
      	mLLRegisterSchema.setIDStartDate(mIdStartDate);
      	mLLRegisterSchema.setIDEndDate(mIdEndDate);
      	System.out.println("对方报文未处理字段:[mBankName]"+mBankName);
      		
      	mLLRegisterSchema.setCustomerNo(mCustomerNo);  
      	mLLRegisterSchema.setGrpName(mGrpName);  	
//      	System.out.println("本次基本信息未处理字段:[mRgtantPostCode]"+mRgtantPostCode);
  
        mLLRegisterSchema.setHandler1(mUserCode);
        mLLRegisterSchema.setApplyerType("5");
        mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setMngCom(mComCode);
        mLLRegisterSchema.setOperator(mUserCode);
        mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        mLLRegisterSchema.setInputPeoples(mAppPeoples);
        
        mMMap.put(mLLRegisterSchema, "INSERT");      
        return true;
    }
    
    public static void main(String[] args) {
        Document tInXmlDoc = null;    
        String code=null;
        String codealias=null;
        String mng=null;
            try {
				tInXmlDoc = JdomUtil.build(new FileInputStream("D:/TP01.xml"), "GBK");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            LLTPRgtRegister tBusinessDeal = new LLTPRgtRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc,code,codealias,mng);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
    }
}

