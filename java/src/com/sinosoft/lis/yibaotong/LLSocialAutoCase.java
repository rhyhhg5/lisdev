package com.sinosoft.lis.yibaotong;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sinosoft.lis.bl.LCGetBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDHospitalDB;
import com.sinosoft.lis.db.LLCaseSocialDB;
import com.sinosoft.lis.db.LLClaimDetailSocialDB;
import com.sinosoft.lis.db.LLRgtSocialDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLCaseSocialSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimDetailSocialSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLRgtSocialSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLCaseSocialSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimDetailSocialSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 自动理赔</p>
 *
 * <p>Description: 生成案件至通知状态</p>
 *
 * <p>Copyright: Copyright (c) 2015</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLSocialAutoCase  {
    public LLSocialAutoCase() {
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private MMap mMMap = new MMap();

    //处理标志
    private boolean mDealState = true;

    //机构编码
    private String mManageCom = "";
    //客户号
    private String mInsuredNo = "";
    
    private String mRiskCode = "";

    //申请信息
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  
    //社保批次信息
    private LLRgtSocialSchema mLLRgtSocialSchema = new LLRgtSocialSchema();
 
    //社保案件信息
    private LLCaseSocialSet mLLCaseSocialSet = new LLCaseSocialSet();
//    理赔明细
    private LLClaimDetailSocialSet mLLClaimDetailSocialSet = new LLClaimDetailSocialSet();
     
    private TransferData mTransferData = new TransferData();
//    
    private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();
    private LLCaseSet   mLLCaseSet = new LLCaseSet();
    private LLSubReportSet   mLLSubReportSet = new LLSubReportSet();
    private LLCaseRelaSet   mLLCaseRelaSet = new LLCaseRelaSet();
    private LLClaimSet   mLLClaimSet = new LLClaimSet();
    private LLClaimPolicySet   mLLClaimPolicySet = new LLClaimPolicySet();
    private LLClaimDetailSet   mLLClaimDetailSet = new LLClaimDetailSet();
    private LJAGetClaimSet   mLJAGetClaimSet = new LJAGetClaimSet();

    private LJAGetSet tLJAGetSet = new LJAGetSet();
    
    private List tList = new ArrayList();//返回信息合集
    //事件关联号
    private String mCaseRelaNo = "";

    private String mMakeDate = PubFun.getCurrentDate();
    private String mMakeTime = PubFun.getCurrentTime();
    private FDate fDate = new FDate();

    //处理类型 1 实时 2非实时
    private String mDealType = "2";
 
    

    
    private String mGrpContNo = "";
    private String mOperate = "";
    private ExeSQL mExeSQL = new ExeSQL();


    /**
     * 生成批次必要信息
     */
    private String mRgtNo = "" ;
    private String mCustomerNo = "";//团体客户号
    private String mGrpName = "";//单位名称
    
    private String mRgtantAddress = "";//联系地址，对应表中“申请人地址”
    private String mRgtantPhone = "";//联系电话，对应表中是“申请人电话”
    private String mRgtantName = "";//申请人姓名
    
    private String mTogetherFlag ="";
//    案件必要信息
    private String mCaseNo = "";
    
    private String mOperator = "";
    private String mClmNo = "";
    private String tGetNoticeNo = ""; 
    private String mActuGetNo = ""; 
    
    double mSumGetMoney = 0;
    
    
//    社保临时表批次号
    private String mSocialRgtNo = "";
    
    

    public boolean submitData(VData cInputData, String cOperate) {

        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
        	mDealState = false;
          return false;
        }
        else
        {
            System.out.println("getinputdata success!");
        }

        if(!checkData())
        {
        	mDealState = false;
            return false;
        }
        //进行业务处理
        if (!dealData()) {
          // @@错误处理
        	mActuGetNo = "";
//        	cancelCase(); //暂时不用
        	mDealState = false;
          CError tError = new CError();
          tError.moduleName = "dealData";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败-->dealData!";
          this.mErrors.addOneError(tError);
          return false;
        }
   

        else {
        	mMMap.put(mLLRegisterSchema, "INSERT"); 
        	mMMap.put(mLLCaseSet, "INSERT"); 
        	mMMap.put(mLLCaseRelaSet, "INSERT"); 
        	mMMap.put(mLLSubReportSet, "INSERT"); 
        	mMMap.put(mLLHospCaseSet, "INSERT"); 
        	mMMap.put(mLLClaimDetailSet, "INSERT");
        	mMMap.put(mLLClaimPolicySet, "INSERT");
        	mMMap.put(mLLClaimSet, "INSERT");
        	mMMap.put(mLJAGetClaimSet, "INSERT");
        	mMMap.put(tLJAGetSet, "INSERT");
        	mInputData.clear();
        	mInputData.add(mMMap);
          System.out.println("Start  Submit...");
          PubSubmit tPubSubmit = new PubSubmit();
          if (!tPubSubmit.submitData(mInputData, mOperate))
          {
            // @@错误处理
        	  mActuGetNo = "";
//        	  cancelCase();
        	  mDealState = false;
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpRegisterBL";
            tError.functionName = "GrpRegisterBL";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
          }
          System.out.println("End  Submit...");
        }
        mInputData = null;
        return true;
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData()  {
       
        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(VData cInputData)  {
    	 System.out.println("getInputData start......");

    	 mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
    	 mSocialRgtNo = (String) mTransferData.getValueByName("SocialRgtNo");
    	 
    	 LLRgtSocialDB mLLRgtSocialDB = new LLRgtSocialDB();
    	 mLLRgtSocialSchema.setSocialRgtNo(mSocialRgtNo);
    	 mLLRgtSocialDB.setSchema(mLLRgtSocialSchema);
    	 mLLRgtSocialDB.getInfo();
    	 mLLRgtSocialSchema = mLLRgtSocialDB.getSchema();
   	    
    	 mOperator = "sicjk";
       
        return true;
    }

    /**
     * 业务报文
     * @return boolean
     */
    private boolean dealData()  {
        System.out.println("LLSocialAutoCase--dealData");
        //处理批次信息
        if (!dealRgt()) {
            return false;
        }

      //生成实付号码
          mTogetherFlag = mLLRegisterSchema.getTogetherFlag();
         if("3".equals(mTogetherFlag)||"4".equals(mTogetherFlag)){
	     String sNoLimit = PubFun.getNoLimit(mManageCom);
	     mActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
	     tList.add(mActuGetNo);
         }
//        处理案件
        LLCaseSocialDB mLLCaseSocialDB = new LLCaseSocialDB();
        mLLCaseSocialDB.setSocialRgtNo(mSocialRgtNo);
        mLLCaseSocialSet = mLLCaseSocialDB.query();
        if(mLLCaseSocialSet.size()<=0){
        	buildError("dealData()", "没有待处理的案件!");
        	mActuGetNo = ""; 
            mDealState = false; 
            return false;
        }
//        案件层级循环
        for(int i=1; i<=mLLCaseSocialSet.size();i++){
        LLCaseSocialSchema mLLCaseSocialSchema = mLLCaseSocialSet.get(i);
        if (!caseRegister(mLLCaseSocialSchema)) {
            return false;
        }

         String limit = PubFun.getNoLimit(mManageCom);
	     mClmNo = PubFun1.CreateMaxNo("CLMNO", limit);
	     String strLimit = PubFun.getNoLimit(mManageCom);
	     tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", strLimit);
	     
	   
	     
	     String mSqlIn4="select sum(realpay), SocialCaseNo from LLClaimDetailSocial  where SocialCaseNo='"+mLLCaseSocialSchema.getSocialCaseNo()+"' group by SocialCaseNo ";
	      SSRS tSSRS4=mExeSQL.execSQL(mSqlIn4);
	     
	      mSumGetMoney = Double.parseDouble(tSSRS4.GetText(1, 1));
	      
	     LLClaimSchema mLLClaimSchema = new LLClaimSchema();
         mLLClaimSchema.setClmNo(this.mClmNo);
         String tGetDutyKind = "000000";
         mLLClaimSchema.setGetDutyKind(tGetDutyKind);
         mLLClaimSchema.setCaseNo(mCaseNo);
         mLLClaimSchema.setClmState("3"); //结算
         mLLClaimSchema.setRgtNo(mRgtNo);
         mLLClaimSchema.setClmUWer(mOperator);
         mLLClaimSchema.setCheckType("0");
         mLLClaimSchema.setRealPay(tSSRS4.GetText(1, 1));
         mLLClaimSchema.setGiveType("1");
         mLLClaimSchema.setGiveTypeDesc("正常给付");
         mLLClaimSchema.setMngCom(mManageCom);
         mLLClaimSchema.setOperator(mOperator);
         mLLClaimSchema.setMakeDate(PubFun.getCurrentDate());
         mLLClaimSchema.setMakeTime(PubFun.getCurrentTime());
         mLLClaimSchema.setModifyDate(PubFun.getCurrentDate());
         mLLClaimSchema.setModifyTime(PubFun.getCurrentTime());
         
         mLLClaimSet.add(mLLClaimSchema);
         if("1".equals(mTogetherFlag)||"2".equals(mTogetherFlag)){
        	 String sNoLimit = PubFun.getNoLimit(mManageCom);
    	     mActuGetNo = PubFun1.CreateMaxNo("ACTUGETNO", sNoLimit);
    	     tList.add(mActuGetNo);
         }
	     
        LLClaimDetailSocialDB mLLClaimDetailSocialDB = new LLClaimDetailSocialDB();
        mLLClaimDetailSocialDB.setSocialRgtNo(mSocialRgtNo);
        mLLClaimDetailSocialDB.setSocialCaseNo(mLLCaseSocialSchema.getSocialCaseNo());
        mLLClaimDetailSocialSet = mLLClaimDetailSocialDB.query();
        mInsuredNo = mLLCaseSocialSchema.getCustomerNo();
//        给付责任层级循环
        for(int k=1; k<=mLLClaimDetailSocialSet.size();k++){    
         LLClaimDetailSocialSchema mLLClaimDetailSocialSchema =  mLLClaimDetailSocialSet.get(k);
         
         mRiskCode = mLLClaimDetailSocialSchema.getRiskCode();

         if (!getCalInfo(mLLCaseSocialSchema,mLLClaimDetailSocialSchema)) {
             return false;
         }
        }
//        llclaimpolicy险种给付责任类型层级循环
        String mSqlIn0="select sum(realpay), SocialCaseNo,riskcode,getdutykind from LLClaimDetailSocial  where SocialCaseNo='"+mLLCaseSocialSchema.getSocialCaseNo()+"' group by SocialCaseNo,riskcode,getdutykind ";
        SSRS tSSRS0=mExeSQL.execSQL(mSqlIn0);
        if(tSSRS0!=null && tSSRS0.getMaxRow()>0){
        	for(int m=1;m<=tSSRS0.getMaxRow();m++){
        		String mSqlIn3="select polno from lcpol where grpcontno='"+mGrpContNo+"' and insuredno='"+mInsuredNo+"' and riskcode ='"+tSSRS0.GetText(m, 3)+"' " +
    			" union all select polno from lbpol where grpcontno='"+mGrpContNo+"' and insuredno='"+mInsuredNo+"' and riskcode ='"+tSSRS0.GetText(m, 3)+"' fetch first row only ";
		        SSRS tSSRS3=mExeSQL.execSQL(mSqlIn3);
		        
		        if(tSSRS3.getMaxRow()<=0){
		        	mActuGetNo = ""; 
		        	buildError("getCalInfo()", "保单：'"+mGrpContNo+"'客户号：'"+mInsuredNo+"'险种：'"+tSSRS0.GetText(m, 3)+"'有问题，请核实。");
		            mDealState = false; 
		            return false;
		        }
		        	Reflections rf = new Reflections();
		            LCPolBL tlcpolbl = new LCPolBL();
		            tlcpolbl.setPolNo(tSSRS3.GetText(1, 1));
		            if(!tlcpolbl.getInfo()){
		            	mActuGetNo = ""; 
		                CError.buildErr(this,"保单信息查询失败");
		                return false;
		            }
                LLClaimPolicySchema mLLClaimPolicySchema = new LLClaimPolicySchema();
                LCPolSchema mLCPolSchema = tlcpolbl.getSchema();
        		rf.transFields(mLLClaimPolicySchema, mLCPolSchema);
     	        mLLClaimPolicySchema.setMakeDate(PubFun.getCurrentDate());
     	        mLLClaimPolicySchema.setMakeTime(PubFun.getCurrentTime());
     	        mLLClaimPolicySchema.setOperator(mOperator);
     	        mLLClaimPolicySchema.setMngCom(mManageCom);
     	        mLLClaimPolicySchema.setRiskVer(mLCPolSchema.getRiskVersion());
     	        mLLClaimPolicySchema.setPolMngCom(mLCPolSchema.getManageCom());
     		    mLLClaimPolicySchema.setClmNo(mClmNo);
     		    mLLClaimPolicySchema.setRgtNo(mRgtNo);
     		    mLLClaimPolicySchema.setCaseNo(mCaseNo);
     		    mLLClaimPolicySchema.setCaseRelaNo(mCaseRelaNo);
     		    mLLClaimPolicySchema.setGetDutyKind(tSSRS0.GetText(m, 4));
     		    mLLClaimPolicySchema.setRealPay(tSSRS0.GetText(m, 1));
     		    mLLClaimPolicySchema.setPolNo(mLCPolSchema.getPolNo());
     		    mLLClaimPolicySchema.setGrpContNo(mLCPolSchema.getGrpContNo());
     		    mLLClaimPolicySchema.setGiveType("1");
     		    mLLClaimPolicySchema.setGiveTypeDesc("正常给付");
     		    mLLClaimPolicySchema.setModifyDate(PubFun.getCurrentDate());
     		    mLLClaimPolicySchema.setModifyTime(PubFun.getCurrentTime());
     		    
     		    mLLClaimPolicySet.add(mLLClaimPolicySchema);
        	}       		    
        }
        //处理应付信息 个人
        if("1".equals(mTogetherFlag)||"2".equals(mTogetherFlag)){
        	if(!dealLjagetGe(mLLCaseSocialSchema)){
        		return false;
        	}
        }
        }

      //处理应付信息 统一
        if("3".equals(mTogetherFlag)||"4".equals(mTogetherFlag)){
        if (!dealLjaget()) {
            return false;
        }
        }
        return true;
    }



    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospAutoRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 解析校验保单部分
     * @return boolean
     */
    private boolean dealRgt()  {
    	
    	 //1.通过团体保单号查询相关基础信息
    	mGrpContNo = mLLRgtSocialSchema.getGrpContNo();
        String tAddressNo = "";
        String tGrpContSql = "select a.customerno, a.name, " +
        		"a.AddressNo " +
        		"from lcgrpcont g, LCGrpAppnt a " +
        		"where a.grpcontno = g.grpcontno and g.appflag = '1' " +
        		"and g.GrpContNo='"+mGrpContNo+"' with ur ";
        SSRS tGrpContSSRS = new SSRS();
        tGrpContSSRS = mExeSQL.execSQL(tGrpContSql);
        if (tGrpContSSRS == null || tGrpContSSRS.getMaxRow() <= 0) {
            buildError("RgtRegister", "【保单号码】在核心不存在");
            return false;
        }else{
            for(int i=1;i <=tGrpContSSRS.getMaxRow();i++){
                mCustomerNo = tGrpContSSRS.GetText(i, 1);
                mGrpName = tGrpContSSRS.GetText(i, 2);
                tAddressNo = tGrpContSSRS.GetText(i, 3);
                
                //判断单位名称过长的话要截取
                System.out.println(mGrpName.length());
                if(mGrpName.length() >19){
                	mGrpName=mGrpName.substring(0, 19);
                }
            }
        }
        
        String tGrpMessSql = "select linkman1,phone1,GrpAddress from LCGrpAddress " +
			"where customerno = '" + mCustomerNo +
			"' and addressno = '" + tAddressNo + "'";
        SSRS tGrpMessSSRS = new SSRS();
        tGrpMessSSRS = mExeSQL.execSQL(tGrpMessSql);
        if (tGrpMessSSRS != null && tGrpMessSSRS.getMaxRow() > 0) {
        	for(int i=1;i <=tGrpMessSSRS.getMaxRow();i++){
                mRgtantName = tGrpMessSSRS.GetText(i, 1);
                mRgtantPhone = tGrpMessSSRS.GetText(i, 2);
                mRgtantAddress = tGrpMessSSRS.GetText(i, 3);
            }
        	//判断单位地址过长的话要截取
            if(mRgtantAddress.length() >25){
            	mRgtantAddress=mRgtantAddress.substring(0, 25);
            }
        }
        
        //2.生成批次信息
        String result = LLCaseCommon.checkGrp(mGrpContNo);
        if(!"".equals(result)){
            buildError("dealData()","工单号为"+result+"的保单管理现正对该团单进行保全操作，请先通知保全撤销相关操作，再进行理赔!");
            mDealState = false; 
            return false;
        }    
        mManageCom = mLLRgtSocialSchema.getMngCom();
        String tLimit = PubFun.getNoLimit(mManageCom);
        System.out.println("管理机构代码是 : "+mManageCom);
        String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
        
        mRgtNo=RGTNO;
        System.out.println("getInputData start......");
        
        mLLRegisterSchema.setRgtNo(mRgtNo);
        mLLRegisterSchema.setRgtState("05");//
        mLLRegisterSchema.setRgtObj("0");
        mLLRegisterSchema.setRgtType("8");//默认为8-对外平台
        mLLRegisterSchema.setRgtClass("1");
        mLLRegisterSchema.setRgtObjNo(mGrpContNo);
        mLLRegisterSchema.setAppPeoples(mLLRgtSocialSchema.getAppPeoples());
        mLLRegisterSchema.setTogetherFlag(mLLRgtSocialSchema.getTogetherFlag());
        mLLRegisterSchema.setCaseGetMode(mLLRgtSocialSchema.getCaseGetMode());
        mLLRegisterSchema.setAppAmnt(mLLRgtSocialSchema.getAppAmnt());
    	mLLRegisterSchema.setBankCode(mLLRgtSocialSchema.getBankCode());
    	mLLRegisterSchema.setAccName(mLLRgtSocialSchema.getAccName()); 
      	mLLRegisterSchema.setBankAccNo(mLLRgtSocialSchema.getBankAccNo());    	
  
      		
      	mLLRegisterSchema.setCustomerNo(mCustomerNo);  
      	mLLRegisterSchema.setGrpName(mGrpName);  	
      	mLLRegisterSchema.setRgtantAddress(mRgtantAddress);	
      	mLLRegisterSchema.setRgtantPhone(mRgtantPhone);	
      	mLLRegisterSchema.setRgtantName(mRgtantName);
      	
  
        mLLRegisterSchema.setHandler1(mOperator);
        mLLRegisterSchema.setApplyerType("5");
        mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setMngCom(mManageCom);
        mLLRegisterSchema.setOperator(mOperator);
        mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        
             
        return true;
  
    }


    /**
     * 解析受理信息
     * @return boolean
     */
    private boolean caseRegister(LLCaseSocialSchema mLLCaseSocialSchema)  {
    	
    	LLCaseSchema tLLCaseSchema = new LLCaseSchema();

        String tLimit = PubFun.getNoLimit(mManageCom);
        mCaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
        tLLCaseSchema.setCaseNo(mCaseNo);
        tLLCaseSchema.setRgtNo(mRgtNo);
       
        tLLCaseSchema.setRgtType("1");//1-申请类
        tLLCaseSchema.setRgtState("11");//通知状态
        tLLCaseSchema.setSurveyFlag("0");
        tLLCaseSchema.setAccdentDesc("社保对接项目");
        
        //获取分单被保人5要素
        String tSqlIn="select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lcinsured a where a.GrpContNo ='"+mGrpContNo+"' and a.idno='"+mLLCaseSocialSchema.getIDNo()+"'  union select distinct insuredno,name,sex,birthday,idno,othidno,insuredstat,idtype from lbinsured b where b.GrpContNo ='"+mGrpContNo+"' and b.idno='"+mLLCaseSocialSchema.getIDNo()+"' ";
        SSRS tSSRS=mExeSQL.execSQL(tSqlIn);
        if(!"".equals(mLLCaseSocialSchema.getCustomerNo()) && mLLCaseSocialSchema.getCustomerNo()!=null){
            tLLCaseSchema.setCustomerNo(mLLCaseSocialSchema.getCustomerNo());
        }else{
            
            if(tSSRS.getMaxRow()>0){
                tLLCaseSchema.setCustomerNo(tSSRS.GetText(1, 1));
                
            }else{
            	mActuGetNo = ""; 
                buildError("dealData()", "保单：'"+mGrpContNo+"'证件号码：'"+mLLCaseSocialSchema.getIDNo()+"'有问题，无法确认客户身份!");
                mDealState = false; 
                return false;
                
            }  
        }
        if(!tSSRS.GetText(1, 1).equals(mLLCaseSocialSchema.getCustomerNo())||!tSSRS.GetText(1, 2).equals(mLLCaseSocialSchema.getCustomerName())||!tSSRS.GetText(1, 3).equals(mLLCaseSocialSchema.getSex())||!tSSRS.GetText(1, 4).equals(mLLCaseSocialSchema.getCustBirthday())){
        	mActuGetNo = ""; 
            buildError("dealData()", "保单：'"+mGrpContNo+"'证件号码：'"+mLLCaseSocialSchema.getIDNo()+"'的客户信息与导入的不符，无法确认客户身份!");
            mDealState = false; 
        	return false;
        }
        tLLCaseSchema.setCustomerName(mLLCaseSocialSchema.getCustomerName());
        tLLCaseSchema.setIDType("0");
        tLLCaseSchema.setIDNo(mLLCaseSocialSchema.getIDNo());
        
        tLLCaseSchema.setCustomerSex(mLLCaseSocialSchema.getSex());
        tLLCaseSchema.setCustBirthday(mLLCaseSocialSchema.getCustBirthday());
        tLLCaseSchema.setAccidentDate(mLLCaseSocialSchema.getAccDate());
        tLLCaseSchema.setInHospitalDate(mLLCaseSocialSchema.getInHospitalDate());
        tLLCaseSchema.setOutHospitalDate(mLLCaseSocialSchema.getOutHospitalDate());

        
        if(!"".equals(mLLCaseSocialSchema.getCaseGetMode()) && mLLCaseSocialSchema.getCaseGetMode()!=null){
        tLLCaseSchema.setCaseGetMode(mLLCaseSocialSchema.getCaseGetMode());
        }else{
        	tLLCaseSchema.setCaseGetMode(mLLRegisterSchema.getCaseGetMode());
        }
        String tbankSql=" select li.BankCode,li.BankAccNo,li.AccName  from lcinsured li where li.insuredno ='"+tLLCaseSchema.getCustomerNo()+"'  and li.grpcontno ='"+mGrpContNo+"'  ";
        SSRS bankSSRS=mExeSQL.execSQL(tbankSql);
        
        if(!"".equals(mLLCaseSocialSchema.getBankCode()) && mLLCaseSocialSchema.getBankCode()!=null){
            tLLCaseSchema.setBankCode(mLLCaseSocialSchema.getBankCode());
        }else{
            if(bankSSRS.getMaxRow()>0){
                tLLCaseSchema.setBankCode(bankSSRS.GetText(1, 1));
            }
        }
       
        if(!"".equals(mLLCaseSocialSchema.getBankAccNo()) && mLLCaseSocialSchema.getBankAccNo()!=null){
            tLLCaseSchema.setBankAccNo(mLLCaseSocialSchema.getBankAccNo());
        }else{
            if(bankSSRS.getMaxRow()>0){
                tLLCaseSchema.setBankAccNo(bankSSRS.GetText(1, 2));
            } 
        }
        
        if(!"".equals(mLLCaseSocialSchema.getAccName()) && mLLCaseSocialSchema.getAccName()!=null){
            tLLCaseSchema.setAccName(mLLCaseSocialSchema.getAccName());
        }else{
            if(bankSSRS.getMaxRow()>0){
                tLLCaseSchema.setAccName(bankSSRS.GetText(1, 3));
            }
        } 
        if(!"".equals(mLLCaseSocialSchema.getMobilePhone()) && mLLCaseSocialSchema.getMobilePhone()!=null){
            tLLCaseSchema.setMobilePhone(mLLCaseSocialSchema.getMobilePhone());
        }
        
        int CustAge = 0;
        String mCustBirthday = tLLCaseSchema.getCustBirthday();
        try {
            CustAge = calAge(mCustBirthday);
        } catch (Exception ex) {
        	mActuGetNo = ""; 
            System.out.println("计算年龄错误");
        }
        tLLCaseSchema.setCustomerAge(CustAge);
        tLLCaseSchema.setHandler(mOperator);
        tLLCaseSchema.setDealer(mOperator);
        tLLCaseSchema.setRigister(mOperator);
        tLLCaseSchema.setRgtDate(PubFun.getCurrentDate());
        tLLCaseSchema.setOperator(mOperator);
        tLLCaseSchema.setMngCom(mManageCom);
        tLLCaseSchema.setMakeDate(PubFun.getCurrentDate());
        tLLCaseSchema.setMakeTime(PubFun.getCurrentTime());
        tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
        tLLCaseSchema.setCaseProp("12");//社保系统对接案件
        tLLCaseSchema.setRigister(mOperator);
        tLLCaseSchema.setClaimer(mOperator);
        tLLCaseSchema.setDealer(mOperator);
        tLLCaseSchema.setUWer(mOperator);
        tLLCaseSchema.setUWDate(PubFun.getCurrentDate());
        tLLCaseSchema.setSigner(mOperator);
		tLLCaseSchema.setSignerDate(PubFun.getCurrentDate());
		tLLCaseSchema.setEndCaseDate(PubFun.getCurrentDate());
		
        
        mLLCaseSet.add(tLLCaseSchema);

        //事件信息
        LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
       
        String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
        mLLSubReportSchema.setSubRptNo(tSubRptNo);
        mLLSubReportSchema.setCustomerName(tLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());

        //事件类型只能是疾病、意外或者空

        mLLSubReportSchema.setAccidentType(mLLCaseSocialSchema.getAccidentType());

        mLLSubReportSchema.setAccDate(mLLCaseSocialSchema.getAccDate());
        mLLSubReportSchema.setInHospitalDate(mLLCaseSocialSchema.getInHospitalDate());
        mLLSubReportSchema.setOutHospitalDate(mLLCaseSocialSchema.getOutHospitalDate());
        mLLSubReportSchema.setAccDesc(mLLCaseSocialSchema.getAccDesc());
        mLLSubReportSchema.setAccPlace(mLLCaseSocialSchema.getAccPlace());
        mLLSubReportSchema.setOperator(mOperator);
        mLLSubReportSchema.setMngCom(mManageCom);
        mLLSubReportSchema.setMakeDate(mMakeDate);
        mLLSubReportSchema.setMakeTime(mMakeTime);
        mLLSubReportSchema.setModifyDate(mMakeDate);
        mLLSubReportSchema.setModifyTime(mMakeTime);
        
        mLLSubReportSet.add(mLLSubReportSchema);
        
      //事件关联表
        //事件关联
        LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
        mCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", tLimit);
        mLLCaseRelaSchema.setCaseNo(mCaseNo);
        mLLCaseRelaSchema.setSubRptNo(tSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
        mLLCaseRelaSet.add(mLLCaseRelaSchema);
        
        //信息到LLHospCase
        LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
        mLLHospCaseSchema.setCaseNo(tLLCaseSchema.getCaseNo());             
        mLLHospCaseSchema.setDealType(mDealType);
        mLLHospCaseSchema.setHospitCode("SB01");
        mLLHospCaseSchema.setHandler(mOperator);
        mLLHospCaseSchema.setAppTranNo(mLLCaseSocialSchema.getTransacTionNo());
        mLLHospCaseSchema.setAppDate(PubFun.getCurrentDate());
        mLLHospCaseSchema.setAppTime(PubFun.getCurrentTime());                         
        mLLHospCaseSchema.setConfirmState("1");
        mLLHospCaseSchema.setCaseType("05");//社保对接项目-05
        mLLHospCaseSchema.setBnfNo(tLLCaseSchema.getCustomerNo());   
        mLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
        mLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
        mLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
        mLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
          
        mLLHospCaseSet.add(mLLHospCaseSchema);


        return true;
    }
    /**
     * 个人给付应付信息生成
     * @return boolean
     * @throws Exception
     */
    private boolean dealLjagetGe(LLCaseSocialSchema mLLCaseSocialSchema)  {
    	
    	
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    	Reflections tref = new Reflections();
        tref.transFields(tLJAGetSchema,mLJAGetClaimSet.get(1));
        
        tLJAGetSchema.setActuGetNo(mActuGetNo);
	    tLJAGetSchema.setSumGetMoney(Arith.round(mSumGetMoney, 2));
	    System.out.println("LJAGet里的钱：" + mSumGetMoney);
	    tLJAGetSchema.setOtherNo(mCaseNo);
        if ("".equals(StrTool.cTrim(mLLCaseSocialSchema.getCaseGetMode()))) {
        	mActuGetNo = ""; 
            buildError("dealData()", "客户：'"+mLLCaseSocialSchema.getCustomerName()+"'导入的是个人给付但没有导入个人赔款领取方式!");
            mDealState = false; 
        	return false;
        }
        if ("4".equals(mLLCaseSocialSchema.getCaseGetMode()) ||
            "11".equals(mLLCaseSocialSchema.getCaseGetMode())) {
            //保险金领取方式为银行转帐
        	if(!"".equals(mLLCaseSocialSchema.getBankCode()) && mLLCaseSocialSchema.getBankCode()!=null && !"".equals(mLLCaseSocialSchema.getBankAccNo()) && mLLCaseSocialSchema.getBankAccNo()!=null && !"".equals(mLLCaseSocialSchema.getAccName()) && mLLCaseSocialSchema.getAccName()!=null){
            tLJAGetSchema.setBankAccNo(mLLCaseSocialSchema.getBankAccNo());
            tLJAGetSchema.setBankCode(mLLCaseSocialSchema.getBankCode());
            tLJAGetSchema.setAccName(mLLCaseSocialSchema.getAccName());
        	}
        	else{
        		mActuGetNo = ""; 
                buildError("dealData()", "客户：'"+mLLCaseSocialSchema.getCustomerName()+"'没有导入银行信息，但选择的赔款领取方式和银行相关!");
                mDealState = false; 
            	return false;
        	}
        }
        tLJAGetSchema.setPayMode(mLLCaseSocialSchema.getCaseGetMode());
        tLJAGetSchema.setDrawer(mLLCaseSocialSchema.getCustomerName());
        tLJAGetSchema.setDrawerID(mLLCaseSocialSchema.getIDNo());
        tLJAGetSchema.setOperator(mOperator);
        tLJAGetSchema.setManageCom(mLLRegisterSchema.getMngCom());
        tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
    	
        tLJAGetSet.add(tLJAGetSchema);
    	
    	return true;
    	
    }
    /**
     * 统一给付应付信息生成
     * @return boolean
     * @throws Exception
     */
    private boolean dealLjaget()  {
    	
    	LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    	Reflections tref = new Reflections();
        tref.transFields(tLJAGetSchema,mLJAGetClaimSet.get(1));
        
        String mSqlIn4="select sum(realpay)  from LLClaimDetailSocial  where SocialRgtNo='"+mSocialRgtNo+"' group by SocialRgtNo ";
	    SSRS tSSRS4=mExeSQL.execSQL(mSqlIn4);
	    
	    double sumRealpay = Double.parseDouble(tSSRS4.GetText(1, 1));


	    tLJAGetSchema.setSumGetMoney(Arith.round(sumRealpay, 2));
	    System.out.println("LJAGet里的钱：" + sumRealpay);
	    tLJAGetSchema.setOtherNo(mLLRegisterSchema.getRgtNo());
        if ("".equals(StrTool.cTrim(mLLRegisterSchema.getCaseGetMode()))) {
            mLLRegisterSchema.setCaseGetMode("1"); //默认现金
        }
        if ("4".equals(mLLRegisterSchema.getCaseGetMode()) ||
            "11".equals(mLLRegisterSchema.getCaseGetMode())) {
            //保险金领取方式为银行转帐
            tLJAGetSchema.setBankAccNo(mLLRegisterSchema.getBankAccNo());
            tLJAGetSchema.setBankCode(mLLRegisterSchema.getBankCode());
            tLJAGetSchema.setAccName(mLLRegisterSchema.getAccName());
        }
        tLJAGetSchema.setPayMode(mLLRegisterSchema.getCaseGetMode());
        tLJAGetSchema.setDrawer(mLLRegisterSchema.getRgtantName());
        tLJAGetSchema.setDrawerID(mLLRegisterSchema.getIDNo());
        tLJAGetSchema.setOperator(mOperator);
        tLJAGetSchema.setManageCom(mLLRegisterSchema.getMngCom());
        tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
    	
        tLJAGetSet.add(tLJAGetSchema);
    	
    	return true;
    	
    }

    /**
     * 准备理赔明细数据
     * @return boolean
     * @throws Exception
     */
    private boolean getCalInfo(LLCaseSocialSchema mLLCaseSocialSchema,LLClaimDetailSocialSchema mLLClaimDetailSocialSchema)   {
    	System.out.println("AUTO--getCalInfo");
    	String mSqlIn="select polno from lcpol where grpcontno='"+mGrpContNo+"' and insuredno='"+mInsuredNo+"' and riskcode ='"+mRiskCode+"' " +
    			" union all select polno from lbpol where grpcontno='"+mGrpContNo+"' and insuredno='"+mInsuredNo+"' and riskcode ='"+mRiskCode+"' fetch first row only ";
        SSRS tSSRS=mExeSQL.execSQL(mSqlIn);
        
        

       
        LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
        
        if(tSSRS.getMaxRow()<=0){
        	mActuGetNo = ""; 
        	buildError("getCalInfo()", "保单：'"+mGrpContNo+"'客户号：'"+mInsuredNo+"'险种：'"+mRiskCode+"'有问题，请核实!");
            mDealState = false; 
            return false;
        }

            LCPolBL tlcpolbl = new LCPolBL();
            tlcpolbl.setPolNo(tSSRS.GetText(1, 1));
            if(!tlcpolbl.getInfo()){
                mActuGetNo = ""; 
            	buildError("getCalInfo()", "保单信息查询失败");
                mDealState = false; 
                return false;
            }
            
            
            LCGetBL tLCGetBL = new LCGetBL();
            tLCGetBL.setPolNo(tSSRS.GetText(1, 1));
            tLCGetBL.setDutyCode(mLLClaimDetailSocialSchema.getDutyCode());
            tLCGetBL.setGetDutyCode(mLLClaimDetailSocialSchema.getGetDutyCode());
            if(!tLCGetBL.getInfo()){
            	  mActuGetNo = ""; 
              	  buildError("getCalInfo()", "给付责任信息查询失败");
                  mDealState = false; 
                  return false;
            }
            
            String mSqlIn1="select StatType from LMDutyGetClm where getdutycode='"+mLLClaimDetailSocialSchema.getGetDutyCode()+"' and getdutykind='"+mLLClaimDetailSocialSchema.getGetDutyKind()+"' ";
            SSRS tSSRS1=mExeSQL.execSQL(mSqlIn1);
            
            LCPolSchema mLCPolSchema = tlcpolbl.getSchema();
            
            tLLClaimDetailSchema.setGetDutyCode(mLLClaimDetailSocialSchema.getGetDutyCode());
            tLLClaimDetailSchema.setClmNo(mClmNo);
            tLLClaimDetailSchema.setRgtNo(mRgtNo);
            tLLClaimDetailSchema.setGetDutyKind(mLLClaimDetailSocialSchema.getGetDutyKind());
            tLLClaimDetailSchema.setCaseNo(mCaseNo);
            tLLClaimDetailSchema.setStatType(tSSRS1.GetText(1, 1));
            tLLClaimDetailSchema.setContNo(mLCPolSchema.getContNo());
            tLLClaimDetailSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
            tLLClaimDetailSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
            tLLClaimDetailSchema.setPolNo(mLCPolSchema.getPolNo());
            tLLClaimDetailSchema.setKindCode(mLCPolSchema.getKindCode());
            tLLClaimDetailSchema.setRiskCode(mLCPolSchema.getRiskCode());
            tLLClaimDetailSchema.setRiskVer(mLCPolSchema.getRiskVersion());
            tLLClaimDetailSchema.setPolMngCom(mLCPolSchema.getManageCom());
            tLLClaimDetailSchema.setSaleChnl(mLCPolSchema.getSaleChnl());
            tLLClaimDetailSchema.setAgentCode(mLCPolSchema.getAgentCode());
            tLLClaimDetailSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
            tLLClaimDetailSchema.setRealPay(mLLClaimDetailSocialSchema.getRealPay());
            tLLClaimDetailSchema.setDutyCode(mLLClaimDetailSocialSchema.getDutyCode());
            tLLClaimDetailSchema.setMakeDate(PubFun.getCurrentDate());
            tLLClaimDetailSchema.setModifyDate(PubFun.getCurrentDate());
            tLLClaimDetailSchema.setMakeTime(PubFun.getCurrentTime());
            tLLClaimDetailSchema.setModifyTime(PubFun.getCurrentTime());
            tLLClaimDetailSchema.setOperator(mOperator);
            tLLClaimDetailSchema.setMngCom(mManageCom);
            tLLClaimDetailSchema.setCaseRelaNo(mCaseRelaNo);
            tLLClaimDetailSchema.setGiveType("1");
            tLLClaimDetailSchema.setGiveTypeDesc("正常给付");
            
            mLLClaimDetailSet.add(tLLClaimDetailSchema);
//            应付信息
            Reflections tref = new Reflections();
            LJAGetClaimSchema mLJAGetClaimSchema = new LJAGetClaimSchema();
            tref.transFields(mLJAGetClaimSchema,tLLClaimDetailSchema);
            mLJAGetClaimSchema.setFeeFinaType(tLLClaimDetailSchema.getStatType());
            mLJAGetClaimSchema.setFeeOperationType(tLLClaimDetailSchema.getGetDutyKind());
            mLJAGetClaimSchema.setRiskVersion(mLCPolSchema.getRiskVersion());
            mLJAGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
            mLJAGetClaimSchema.setAgentCode(mLCPolSchema.getAgentCode());
            mLJAGetClaimSchema.setAgentCom(mLCPolSchema.getAgentCom());
            mLJAGetClaimSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
            mLJAGetClaimSchema.setAgentType(mLCPolSchema.getAgentType());
            mLJAGetClaimSchema.setMakeDate(PubFun.getCurrentDate());
            mLJAGetClaimSchema.setMakeTime(PubFun.getCurrentTime());
            mLJAGetClaimSchema.setPay(tLLClaimDetailSchema.getRealPay());
            mLJAGetClaimSchema.setOtherNo(mCaseNo);
            mLJAGetClaimSchema.setOtherNoType("5");
            mLJAGetClaimSchema.setManageCom(tLLClaimDetailSchema.getPolMngCom());

           
            mLJAGetClaimSchema.setOPConfirmDate(PubFun.getCurrentDate());
            mLJAGetClaimSchema.setOPConfirmCode(mOperator);
            mLJAGetClaimSchema.setOPConfirmTime(PubFun.getCurrentTime());
            mLJAGetClaimSchema.setOperator(mOperator);
            

            mLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
            mLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());
            mLJAGetClaimSchema.setActuGetNo(mActuGetNo);

            mLJAGetClaimSet.add(mLJAGetClaimSchema);
            


        return true;
    }

    
//    目前此操作没有用，将来若有分步提交可能会用到
    private boolean cancelCase()  {
        System.out.println("--cancelCase");
        //理算数据
        String tClaimSQL = "delete from LLClaim where RgtNo='" + mRgtNo + "'";
        String tClaimPolicySQL = "delete from LLClaimPolicy where RgtNo='" + mRgtNo + "'";
        String tClaimDetailSQL = "delete from LLClaimDetail where RgtNo='" + mRgtNo + "'";
//        String tInsureAccTrace = "delete from LCInsureAccTrace where OtherNo='" +
//                                 mCaseNo + "'";
//        String tToClaimDutySQL = "delete from LLToClaimDuty where CaseNo ='" +
//                                 mCaseNo + "'";
//        String tCasePolicySQL = "delete from LLCasePolicy where CaseNo ='"
//                                + mCaseNo + "'";
//
//        String tFeeMainSQL = "delete from LLFeeMain where CaseNo='" + mCaseNo +
//                             "'";
//        String tSecuritySQL = "delete from LLSecurityReceipt where CaseNo = '" +
//                              mCaseNo + "'";
//        String tFeeItemSQL = "delete from LLFeeOtherItem where CaseNo = '" +
//                             mCaseNo + "'";
//        String tCaseReceiptSQL = "delete from LLCaseReceipt where CaseNo = '" +
//                                 mCaseNo + "'";
//        String tCaseDrugSQL = "delete from LLCaseDrug where CaseNo = '" +
//                              mCaseNo + "'";
//        String tCaseCureSQL = "delete from LLCaseCure where CaseNo = '" +
//                              mCaseNo + "'";
//        String tOperationSQL = "delete from LLOperation where CaseNo = '" +
//                               mCaseNo + "'";
//        String tAccidentSQL = "delete from LLAccident where CaseNo = '" +
//                               mCaseNo + "'";

        String tSubReportSQL = "delete from LLSubReport a where exists (select 1 from llcaserela where a.subrptno=subrptno and " +
        		" CaseNo in(select caseno from llcase where rgtno = '"+mRgtNo+"' ))";
        String tCaseRelaSQL = "delete from LLCaseRela where CaseNo in(select caseno from llcase where rgtno = '"+mRgtNo+"' )";
        
        String tCaseSQL = "delete from LLCase where RgtNo='" + mRgtNo + "'";

        String tRegisterSQL = "delete from LLRegister where RgtNo ='" + mRgtNo +
                              "'";

        String tHospCaseSQL = "delete from LLHospCase where CaseNo in(select caseno from llcase where rgtno = '"+mRgtNo+"' )";
        
        String tLjagetSQL = "delete from Ljaget where actugetno ='" +
        mActuGetNo + "'";

        String tLjagetClaimSQL = "delete from LjagetClaim where actugetno ='" +
        mActuGetNo + "'";
        MMap tmpMap = new MMap();
        tmpMap.put(tClaimSQL, "DELETE");
        tmpMap.put(tClaimPolicySQL, "DELETE");
        tmpMap.put(tClaimDetailSQL, "DELETE");
//        tmpMap.put(tInsureAccTrace, "DELETE");
//        tmpMap.put(tToClaimDutySQL, "DELETE");
//        tmpMap.put(tCasePolicySQL, "DELETE");
//        tmpMap.put(tFeeMainSQL, "DELETE");
//        tmpMap.put(tSecuritySQL, "DELETE");
//        tmpMap.put(tCaseReceiptSQL, "DELETE");
//        tmpMap.put(tCaseDrugSQL, "DELETE");
//        tmpMap.put(tFeeItemSQL, "DELETE");
//        tmpMap.put(tCaseCureSQL, "DELETE");
//        tmpMap.put(tOperationSQL, "DELETE");
//        tmpMap.put(tAccidentSQL, "DELETE");
        tmpMap.put(tSubReportSQL, "DELETE");
        tmpMap.put(tCaseRelaSQL, "DELETE");
        tmpMap.put(tCaseSQL, "DELETE");
        tmpMap.put(tRegisterSQL, "DELETE");
        tmpMap.put(tHospCaseSQL, "DELETE");
        tmpMap.put(tLjagetSQL, "DELETE");
        tmpMap.put(tLjagetClaimSQL, "DELETE");

        try {
            this.mInputData.clear();
            this.mInputData.add(tmpMap);
            mResult.clear();
//            mResult.add(this.mLLSubReportSchema);
        } catch (Exception ex) {
            buildError("cancelCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("cancelCase", "撤销数据提交失败");
            return false;
        }

        return false;
    }
    
    /**
     * 计算年龄的函数
     * @return int
     */
    private int calAge(String mBirthday) {
        Date Birthday = fDate.getDate(mBirthday);
        String strNow = PubFun.getCurrentDate();
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。" + CustomerAge);
        return CustomerAge;
    }

    public String getActuGetNo() {
		return mActuGetNo;
	}

	public void setActuGetNo(String mActuGetNo) {
		this.mActuGetNo = mActuGetNo;
	}
	
    public List gettList() {
		return tList;
	}

	public void settList(List tList) {
		this.tList = tList;
	}

	public static void main(String[] args) {
    	LLSocialAutoCase tLLSocialAutoCase = new LLSocialAutoCase();
    	
    	TransferData mTransferData = new TransferData();
    	mTransferData.setNameAndValue("SocialRgtNo", "86950000000021");
    	 VData aVData = new VData(); 
    	 aVData.add(mTransferData);
//    	 LLRgtSocialSchema aa = new LLRgtSocialSchema();
//    	 LLRgtSocialDB db = new LLRgtSocialDB();
//    	 aa.setSocialRgtNo("86950000000019");
//    	 db.setSchema(aa);
//    	 db.getInfo();
//    	 aa = db.getSchema();
//    	 aVData.add(aa);
    	tLLSocialAutoCase.submitData(aVData, "SB");
    	
    }
}
