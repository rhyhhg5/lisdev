package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPReplyCodeName;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class LLTPGetScanImageBL {


	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mPrtNo = "";

	private TransferData mTransferData;
		
	private String mPathManageCom;  //根据管理机构存放路径

	private FTPTool tFTPTool;

	private String mPicPath;
	
	private String mFTPLocation; //本地存放ZIP地址

//	private String mZipName;	//本地存放ZIP名字
	
	private String mCaseNo;
	
	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
	
	private String mZipLocalPath;

	private String tFileName;

	private String mFTPPath;

	private VData mVData;
	
	public boolean submitData(VData cInputData, String operate){
		mVData = cInputData;
		
		if(!getInputData(mVData)){
			return false;
		}
		
		if (!getConn(operate)) {
			return false;
		}
		
		if(!getFile(operate)){
			try {
				tFTPTool.logout();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		if(!dealFile(operate)){
			return false;
		}
		
		return true;
	}
	
	/***
	 * 提交解压影像件资料
	 * @param args
	 */
	private boolean dealFile(String operate) {
		LLTPDealScanImages tLLTPDealScanImages = new LLTPDealScanImages();
		VData tVData = new VData();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("CaseNo", mCaseNo);
		transferData.setNameAndValue("ZipLocalPath", mZipLocalPath);
		transferData.setNameAndValue("ZipName", tFileName);
		tVData.add(mGI);
		tVData.add(transferData);

			if (!tLLTPDealScanImages.submitData(tVData, operate)) {
				CError tError = new CError();
				tError.moduleName = "LLTPGetScanImageBL";
				tError.functionName = "dealFile";
				tError.errorMessage = tLLTPDealScanImages.mErrors.getFirstError();
				mErrors.addOneError(tError);
				return false;
			}
		return true;
	}
	
	/***
	 * 下载影像件资料
	 * @param args
	 */
	private boolean getFile(String operate) {
		String getPath = "select codename ,codealias from ldcode where codetype='"+operate+"LLTPScan' and code='FTPPath/LocalPath' ";//若有约定好目录保存FTPPath
		SSRS tPathSSRS = new ExeSQL().execSQL(getPath);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "TYGetScanFileBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp服务器路径未配置";
			mErrors.addOneError(tError);
			return false;
		}
//		mFTPPath = tPathSSRS.GetText(1, 1);
//		mFTPPath = mGI.ManageCom.substring(0, 4)+"/GrpConts/"+mFTPLocation;
		mFTPPath =  mFTPLocation;
		
		String tUIRoot = CommonBL.getUIRoot();
		//本地测试
//		tUIRoot ="d:/ftp/";
//		mZipLocalPath = tUIRoot+mPathManageCom+"/"+PubFun.getCurrentDate()+ "/";
		mZipLocalPath = tUIRoot+tPathSSRS.GetText(1, 2)+"/"+mPathManageCom+"/"+PubFun.getCurrentDate()+ "/";  //统一存放至LLTPScanzip/机构编码前四位/当天/文件夹下
		boolean downflag = true;
		File mFileDir = new File(mZipLocalPath);
		if(!mFileDir.exists()) {
			if(!mFileDir.mkdirs()) {
				System.out.println("创建目录[" + mZipLocalPath.toString() + "]失败！" + mFileDir.getPath());
			}
		}
		downflag = tFTPTool.downloadFileExists(mFTPPath, mZipLocalPath, tFileName);
		System.out.println("下载成功标识为：" + downflag);
		if (!downflag) {
			System.out.println(tFTPTool.mErrors.getFirstError());
			CError tError = new CError();
			tError.moduleName = "TYGetScanFileBL";
			tError.functionName = "getFile";
			tError.errorMessage = tFTPTool.mErrors.getFirstError();
			mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
	/***
	 * 连接FTP
	 * 
	 * @param args
	 */
	private boolean getConn(String operate) {
		String getIPPort = "select codename ,codealias from ldcode where codetype='"+operate+"LLTPScan' and code='IP/Port' ";
		String getUserPs = "select codename ,codealias from ldcode where codetype='"+operate+"LLTPScan' and code='User/Pass' ";
		SSRS tIPSSRS = new ExeSQL().execSQL(getIPPort);
		SSRS tUPSSRS = new ExeSQL().execSQL(getUserPs);
		if (tIPSSRS.getMaxRow() < 1 || tUPSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "LLTPGetScanImageBL";
			tError.functionName = "getConn";
			tError.errorMessage = "ftp配置有误";
			mErrors.addOneError(tError);
			return false;
		}
		tFTPTool = new FTPTool(tIPSSRS.GetText(1, 1), tUPSSRS.GetText(1, 1),
				tUPSSRS.GetText(1, 2), Integer.parseInt(tIPSSRS.GetText(1, 2)));
		try {
			if (!tFTPTool.loginFTP()) {
				CError tError = new CError();
				tError.moduleName = "LLTPGetScanImageBL";
				tError.functionName = "getConn";
				tError.errorMessage = tFTPTool
						.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
				mErrors.addOneError(tError);
				return false;
			}
		} catch (SocketException e) {
			CError tError = new CError();
			tError.moduleName = "LLTPGetScanImageBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			CError tError = new CError();
			tError.moduleName = "LLTPGetScanImageBL";
			tError.functionName = "getConn";
			tError.errorMessage = "FTP连接异常";
			mErrors.addOneError(tError);
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	
	
	/**
	 * 校验基本数据信息
	 * @param args
	 */
	private boolean getInputData(VData aVData){
		mGI = (GlobalInput) aVData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) aVData.getObjectByObjectName(
				"TransferData", 0);
		mLLCaseSchema = (LLCaseSchema)aVData.getObjectByObjectName("LLCaseSchema", 0);
		mCaseNo = mLLCaseSchema.getCaseNo();
		mPicPath = (String) mTransferData.getValueByName("PicPath");//传过来的zip包完整路径
		mPathManageCom = mGI.ManageCom.substring(0, 4);
		//得到传过来的FTP上的路径和文件名
		File FTPzip = new File(mPicPath);
		tFileName = FTPzip.getName();
		mFTPLocation =mPicPath.substring(0,mPicPath.lastIndexOf(tFileName));
		System.out.println("上传扫描件案件号为：" + mCaseNo);
		
		return true;
	}
	
	public static void main(String[] args) {
		LLCaseSchema mLLCaseSchema = new LLCaseSchema();
		mLLCaseSchema.setCaseNo("C1100180327000001");
		LLTPGetScanImageBL tTYGetFile = new LLTPGetScanImageBL();
		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("PicPath", "8631/Cont/Image/2018-01-04/15189625.zip");
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.Operator = "cm1102";
		mGlobalInput.AgentCom = "";
		mGlobalInput.ManageCom = "86120000";
		mGlobalInput.ComCode = "86120000";

		VData tVData = new VData();
		tVData.add(mLLCaseSchema);
		tVData.add(transferData1);
		tVData.add(mGlobalInput);
		tTYGetFile.submitData(tVData, "");
	}



}
