package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.jdom.Document;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LLWBTransFtpXml
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();

    public LLWBTransFtpXml()
    {
    }

    public boolean submitData()
    {
        if (getSubmitMap() == null)
        {
            return false;
        }

     

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;

    }

    public MMap getSubmitMap()
    {

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
   

        return true;
    }

    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }


    private boolean getXls()
    {
        String tUIRoot = CommonBL.getUIRoot();

        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        int xmlCount = 0;

        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='IP/Port'");
        String tPort = tExeSql
                .getOneValue("select codealias from ldcode where codetype='WBClaim' and code='IP/Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='User/Pass'");
        String tPassword = tExeSql
                .getOneValue("select codealias from ldcode where codetype='WBClaim' and code='User/Pass'");

        String tFileRgtkPath = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='XmlPath'");
        
        String tFileCasePath = tExeSql
        		.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='XmlPath'");
/*      本地测试使用  
        String tFileBackXmlPath = tExeSql
			.getOneValue("select codename from ldcode where codetype='WBClaim' and code='BackXmlPath'");
        
        String tFileBackXmlPathRgt = tExeSql
			.getOneValue("select codename from ldcode where codetype='WBClaim' and code='BackXmlPathDesc'");
        
        String tFileBackXmlPathCase = tExeSql
			.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='BackXmlPathDesc'");*/
        
        tFileRgtkPath = tUIRoot + tFileRgtkPath;
        tFileCasePath = tUIRoot + tFileCasePath;
       
//        本地测试用
/*//        服务器存放批次报文的路径
        String tFileXmlPathRgt = tFileBackXmlPath + tFileBackXmlPathRgt + PubFun.getCurrentDate()+"\\";
//      服务器存放批次返回报文的路径  
        tFileBackXmlPathRgt = tFileBackXmlPath + tFileBackXmlPathRgt + PubFun.getCurrentDate()+"\\Back\\";
//      服务器存放案件报文的路径 
        String tFileXmlPathCase = tFileBackXmlPath + tFileBackXmlPathCase + PubFun.getCurrentDate()+"\\";
//      服务器存放案件返回报文的路径 
        tFileBackXmlPathCase = tFileBackXmlPath + tFileBackXmlPathCase + PubFun.getCurrentDate()+"\\Back\\";*/
        
   

        try
        {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "CardActiveBatchImportBL";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
           
//            String tFilePathLocal = tFileBackPath;
//            String tFilePathLocal = "E:/ceshi/rgt/";//本地测试使用
            String tFilePathLocalCore = tFileRgtkPath+PubFun.getCurrentDate()+"/";//服务器存放文件的路径 每天生成一个文件夹
//            String tFileImportBackPath = tFilePath;
            String tFileImportPath = "/01PH/8695/WBClaimRgt";//ftp上存放文件的目录
            String tFileImportBackPath = "/01PH/8695/WBClaimRgtBack";//ftp上返回文件目录
            String tFileImportPathBackUp = "/01PH/8695/WBClaimBackUp/"+PubFun.getCurrentDate()+"/";//FTP备份目录
            String tFilePathout = tFilePathLocalCore+"Back/";
            
//            tFTPTool.upload(tFileImportPathBackUp,tFileRgtkPath+"18201506083.xml");
//            tFTPTool.upload(tFileImportPathBackUp, "\\wasfs\\wasappserv\\profiles\\AppSrv01\\installedApps\\p55a1Node01Cell\\lis_war.ear\\taobaoPort.war\\temp_lp\\18201506083.xml");
//            tFTPTool.changeWorkingDirectory("/8695/WBClaimBackUp/");
//            tFTPTool.makeDirectory(PubFun.getCurrentDate());
//            temp_lp/WBClaim/8695/RgtXml
//            if(!newFolder(tUIRoot+"temp_lp/WBClaim/")){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tUIRoot+"temp_lp/WBClaim/8695/")){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFileRgtkPath)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFilePathLocalCore)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFilePathout)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
            File mFileDir = new File(tFilePathLocalCore);
    		if (!mFileDir.exists()) {
    			if (!mFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ mFileDir.getPath());
    			}
    		}
    		
            File tFileDir = new File(tFilePathout);
    		if (!tFileDir.exists()) {
    			if (!tFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathout.toString() + "]失败！"
    						+ tFileDir.getPath());
    			}
    		}
    		
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
            System.out.println("OK");

//            tFTPTool.uploadAll(tFileImportPath, tFilePathLocal);//本地测试使用
            
            	
            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                return false;
            }

            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }
                System.out.println("Hello !!");  
              
                // ------- ends here 
        		/*long a = System.currentTimeMillis();
        		while(System.currentTimeMillis()-a<=600000){
//        			System.out.println("5分钟内");
        		}*/
                	
                		
                
                

                xmlCount++;

                try
                {
                    VData tVData = new VData();

                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("FileName", tPath[j]);
                    tTransferData.setNameAndValue("FilePath", tFilePathLocalCore);

                    tVData.add(tTransferData);
                    tVData.add(mGI);

//                    AddCertifyListUI tAddCertifyListUI = new AddCertifyListUI();

//                    if (!tAddCertifyListUI.submitData(tVData, "ActiveImport"))
//                    {
//                        mErrors.copyAllErrors(tAddCertifyListUI.mErrors);
//                        try
//                        {
//                            tFTPTool.deleteFile(tPath[j]);
//                        }
//                        catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//                    }
                    Document tInXmlDoc;   
                    tInXmlDoc = JdomUtil.build(new FileInputStream(tFilePathLocalCore+tPath[j]), "GBK");
                    LLWBRgtRegister tBusinessDeal = new LLWBRgtRegister();
                    Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
                    System.out.println("打印传入报文============");
                    JdomUtil.print(tInXmlDoc.getRootElement());
                    System.out.println("打印传出报文============");
                    JdomUtil.print(tOutXmlDoc);
                    
                    JdomUtil.output(tOutXmlDoc, new FileOutputStream(tFilePathout+tPath[j]));
                    
                   
                    
                    tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[j]);
                    
                    tFTPTool.changeWorkingDirectory(tFileImportPath);
                    tFTPTool.deleteFile(tPath[j]);
                    
                    
                    
                    tFTPTool.upload(tFileImportBackPath, tFilePathout+tPath[j]);
//                    tFTPTool.deleteFile(tPath[j]);

                }
                catch (Exception ex)
                {
                    System.out.println("批次导入失败: " + tPath[j]);
                    mErrors.addOneError("批次导入失败" + tPath[j]);
                    ex.printStackTrace();
                  
                }
            }
            tFTPTool.logoutFTP();

        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }

    private boolean getInputData()
    {


        return true;
    }

    public String getDealInfo()
    {
        return mDealInfo;
    }
    
//  建文件夹
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args)
    {
    	LLWBTransFtpXml a =new LLWBTransFtpXml();
    	a.getXls();
    }
}
