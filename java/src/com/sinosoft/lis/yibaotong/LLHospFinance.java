package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

//test用
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLHospFinance implements ServiceInterface {
    public LLHospFinance() {
    }

    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();

    private Document mInXmlDoc; //传入报文
    private String mDocType = ""; //报文类型
    private boolean mDealState = true; //处理标志
    private String mResponseCode = "1"; //返回类型代码
    private String mRequestType = ""; //请求类型
    private String mErrorMessage = ""; //错误描述
    private String mTransactionNum = ""; //交互编码
    private String mTranHospCode = ""; //交互编码中医院代码

    private Element tCase_Number_List;
    private Element tListData = new Element("BALANCE_LIST");

    //提取客户保单信息
    private String mBalance_Flag = ""; //财务结算标志
    private String mManageCom = "";

    public Document service(Document pInXmlDoc) {
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"05".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || ("").equals(mTransactionNum) ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        mTranHospCode = mTransactionNum.substring(2, 9);

        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        tLDHospitalDB.setHospitCode(mTranHospCode);
        if (!tLDHospitalDB.getInfo()) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }
        mManageCom = tLDHospitalDB.getManageCom();
        if ("".equals(mManageCom) || mManageCom == null) {
            buildError("checkData()", "医院所属机构错误");
            return false;
        }
        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        tCase_Number_List = tBodyData.getChild("CASE_NUMBER_LIST");

        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {

        System.out.println("LLHospFinance--dealData()-------------");
        List tCaseList = new ArrayList();
        tCaseList = (List) tCase_Number_List.getChildren();

        for (int i = 0; i < tCaseList.size(); i++) {
            Element tContData = (Element) tCaseList.get(i);
            String tCaseNo = tContData.getChildText("CASE_NUMBER");
            //校验案件为医保通案件
            LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
            tLLHospCaseDB.setCaseNo(tCaseNo);
            tLLHospCaseDB.setConfirmState("1");
//            tLLHospCaseDB.setAppTranNo("05");
            tLLHospCaseDB.setHospitCode(mTranHospCode);
            if (tLLHospCaseDB.query().size() <= 0) {
                buildError("dealData", "查询医保通案件处理表信息失败!");
                return false;
            }

            if (tCaseNo == null || ("").equals(tCaseNo)) {
                buildError("dealData", "报文传入的赔案号为空!");
                return false;
            }

            //财务不转帐
            String sqlStr2 = "select count(*) from ljaget where otherno ='" +
                             tCaseNo + "' and othernotype='5'"
                             + " and SumGetMoney=0 and (finstate is null or finstate != 'FC')";

            ExeSQL ExeSQL2 = new ExeSQL();
            String tCount2 = ExeSQL2.getOneValue(sqlStr2);
            if (Double.parseDouble(tCount2) >= 1) {
                mBalance_Flag = "2";
            } else {
                //财务已转帐
                String sqlStr1 = "select count(*) from ljaget where otherno ='" +
                                 tCaseNo + "' and othernotype='5'"
                                 +
                                 " and confdate is not null and SumGetMoney>0"
                                 +" and ( finstate is null or finstate != 'FC')";
                ExeSQL ExeSQL1 = new ExeSQL();
                String tCount1 = ExeSQL1.getOneValue(sqlStr1);
                if (Double.parseDouble(tCount1) >= 1) {
                    mBalance_Flag = "1";
                } else {
                    mBalance_Flag = "0";
                }
            }
            Element aCaseNo = new Element("CASE_NUMBER");
            aCaseNo.setText(tCaseNo);

            Element aBalance_Flag = new Element("BALANCE_FLAG");
            aBalance_Flag.setText(mBalance_Flag);

            Element tContInfo = new Element("BALANCE_DATA");
            tContInfo.addContent(aCaseNo);
            tContInfo.addContent(aBalance_Flag);

            tListData.addContent(tContInfo);

        }
        return true;
    }


    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
            return createResultXML();
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "1";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Element tBody = new Element("BODY");
        tBody.addContent(tListData);
        tRootData.addContent(tBody);
        Document tDocument = new Document(tRootData);

        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospFinance";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args) {
//    	System.out.println("程序开始…");
//
//		XMLPubTool tXMLPubTool = new XMLPubTool();
//		Element eRequestType = tXMLPubTool.makeElement("REQUEST_TYPE", "01");
//		Element eTransactionNum = tXMLPubTool.makeElement("TRANSACTION_NUM", "0151010120091209");
//
//		Element eHead = tXMLPubTool.makeEmptyElement("HEAD");
//		Element ePacket = tXMLPubTool.makeComplexElement("PACKET", "type", "REQUEST");
//		tXMLPubTool.AddAttribute(ePacket, "version", "1.0");
//
//		tXMLPubTool.LinkElement(ePacket, eHead);
//		tXMLPubTool.LinkElement(eHead,eRequestType);
//		tXMLPubTool.LinkElement(eHead,eTransactionNum);
//
//		Document tNoStdXml = new Document(ePacket);
//		JdomUtil.print(tNoStdXml);
//		return tNoStdXml;
//
//
//        LLHospCustomerQuery tTest = new LLHospCustomerQuery();
////        System.out.println(tTest.mRequestType);
//        tTest.mDealState = true;
//
//        tTest.mResponseCode = "1";
//        //请求类型
//        tTest.mRequestType = "03";
//        //错误描述
//        tTest.mErrorMessage = "错了吧";
//        //交互编码
//        tTest.mTransactionNum = "123456789";
//
//        Element tRootData = new Element("PACKET");
//        Document tDocument = new Document(tRootData);
////        tTest.service(tDocument);

//        return null;
//    	LLHospFinance tTest = new LLHospFinance();
////        System.out.println(tTest.mRequestType);
//        tTest.mDealState = true;
//
//        tTest.mResponseCode = "1";
//        //请求类型
//        tTest.mRequestType = "02";
//        //错误描述
//        tTest.mErrorMessage = "错了吧";
//        //交互编码
//        tTest.mTransactionNum = "123456789";
//
//        Element tRootData = new Element("PACKET");
//        Document tDocument = new Document(tRootData);
//        tTest.service(tDocument);
//
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "D:/customer3.xml"), "GBK");

            LLHospFinance tBusinessDeal = new LLHospFinance();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
