package com.sinosoft.lis.yibaotong;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.midplat.kernel.util.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 保单银行账户信息查询
 * @author Alfred Xu
 * @time 2016-08-24
 *
 */
public class LLGWZHInfoQuery implements ServiceInterface{
	
	public LLGWZHInfoQuery(){
		
	}

	public CErrors mErrors = new CErrors();
	private Document tInXmlDoc; 		//传入报文
	private String mDocType = "";	    //报文类型    
	private String mRequestType ="";        //请求类型
	private String mErrorMessage ="";      //错误描述
	private String mTransactionNum ="";    //交互编码
	
	/**
	 * 对方报文传入信息
	 */
	private String mGrpContNo = "";//团体保单号
	
	/**
	 * 生成返回信息
	 */
	private String mResponseCode = "1"; //返回信息
	
	public Document service(Document pInXmlDoc) {
		tInXmlDoc = pInXmlDoc;
		try {
            if (!getInputData(tInXmlDoc)) {
            	
            } else {
                if (!checkData()) {
                	
                } else {
                    if (!dealData()) {
                    	
                    }
                }
            }
        } catch (Exception ex) {
        	mResponseCode = "E";

        } finally {
        	
        	return CreatResponseXml();
        }
			
	}

	private Document CreatResponseXml() {
		Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");
        //Head 部分
        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);


        mErrorMessage = mErrors.getFirstError();
        //Body 部分
        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBody.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        
        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
	}

	

	private boolean dealData() {
		try{
			
			String tGrpContNo = mGrpContNo;
			String tSqlc="select ClaimBankCode,ClaimBankAccNo,ClaimAccName from LCGrpAppnt where GrpContNo='"+tGrpContNo+"' union select ClaimBankCode,ClaimBankAccNo,ClaimAccName from LbGrpAppnt where GrpContNo='"+tGrpContNo+"'";
			    
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tBankInfo=tExeSQL.execSQL(tSqlc);
			
			if(tBankInfo!=null){
				String[][] bankInfos = tBankInfo.getAllData();
				if("".equals(bankInfos[0][0])||"".equals(bankInfos[0][1])||"".equals(bankInfos[0][2])||bankInfos[0][0]==null||bankInfos[0][1]==null||bankInfos[0][2]==null){
					mResponseCode = "0";
					buildError("service","受理失败，请联系公司客服人员，办理团体单位赔款领取银行账户设置");
					return false;
				}
			}else{
				buildError("service","受理失败，请联系公司客服人员，办理团体单位赔款领取银行账户设置!");
				mResponseCode = "0";
				return false;
			}
						
		}catch(Exception e){
			buildError("service", "进行账户信息查询时出错!");
			mResponseCode = "E";
			return false;
		}
		return true;
	}

	/**
	 * 校验基本的数据信息
	 * @return
	 */
	private boolean checkData() {
		System.out.println("LLGWZHInfoQuery.--checkData");
		if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            mResponseCode= "E";
            return false;
        }

        if (!"GW02".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            mResponseCode="E";
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误");
            mResponseCode="E";
            return false;
        }

        if (!"GW02".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            mResponseCode="E";
            return false;
        }
		return true;
	}
	/**
	 * 解析报文主要部分
	 * @param nInXml
	 * @return
	 */
	private boolean getInputData(Document nInXml) {
	System.out.println("LLGWZHInfoQuery--------Start");
	if(nInXml == null){
		buildError("getInputData()","未获取到报文");
		return false;
	}
	Element tRootData = nInXml.getRootElement();
	mDocType = tRootData.getAttributeValue("type");
	//获取到Head信息
	Element tHead = tRootData.getChild("HEAD");
	mRequestType = tHead.getChildTextTrim("REQUEST_TYPE");
	mTransactionNum = tHead.getChildTextTrim("TRANSACTION_NUM");
	
	if("GW02".equals(mRequestType)){
		//获取到Body信息
		Element tBody= tRootData.getChild("BODY");
		mGrpContNo = tBody.getChildTextTrim("GRPCONTNO");
		if(mGrpContNo == null){
			buildError("dealData", "团体客户信息为空");
			return false;
		}

	}
		return true;
	}
	 /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLGWZHInfoQuery";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
	
	//以下方法为测试专用方法
	public static void main(String args[]){
		 Document mInXmlDoc;
		 try{
			 mInXmlDoc = JdomUtil.build(new FileInputStream("E:/LLGW/GW02.xml"),"GBK"); 
			 LLGWZHInfoQuery tLLGWZHInfoQuery = new LLGWZHInfoQuery();
			 Document tOutXmlDoc = tLLGWZHInfoQuery.service(mInXmlDoc);
			 System.out.println("打印传入报文============");
			 JdomUtil.print(mInXmlDoc.getRootElement());
			 System.out.println("打印传出报文============");
			 JdomUtil.print(tOutXmlDoc);
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	}
}

