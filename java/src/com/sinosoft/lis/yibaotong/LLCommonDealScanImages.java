package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileNotFoundException;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.db.ES_DOC_HANDLERDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_HANDLERSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.vschema.ES_DOC_HANDLERSet;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.DeCompressZip;
import com.sinosoft.wasforwardxml.project.pad.util.IndexMap;

public class LLCommonDealScanImages {

	
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mCaseNo = "";

	private TransferData mTransferData;

	private String mZipLocalPath;

	private String mZipName;
	
//	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
	
 	private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();
	
	private ES_DOC_RELATIONSet mES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();

	private String mManageCom = "";
	
	private String pathManageCom = "";
	
	private String mOperator = "";

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private VData mResult = new VData();

	private static final long serialVersionUID = 1L;

	private ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();

	private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();

	private ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
	
	private ES_DOC_HANDLERSet mES_DOC_HANDLERSet = new ES_DOC_HANDLERSet();
	
	private ES_DOC_HANDLERSet SuccES_DOC_HANDLERSet = new ES_DOC_HANDLERSet();
	
	private ES_DOC_HANDLERSet tmES_DOC_HANDLERSet = new ES_DOC_HANDLERSet();

	private MMap map = new MMap();
	
	private IndexMap iMap = new IndexMap();
	
	private String HostName = "";
	
	private String mSerialNo = "";
	
	public boolean submitData(VData cInputData) {

		if (!getInputData(cInputData)) {
			return false;
		}

		try {
			if (!deal(mZipLocalPath)) {
				return false;
			}else{
                //保存信息到LLHospCase
                PubSubmit tPubSubmit = new PubSubmit();
                VData mVData = new VData();
                MMap tMMap= new MMap();
                if(tmES_DOC_HANDLERSet.size()>0){
                	tMMap.put(tmES_DOC_HANDLERSet, "UPDATE");
                }
                tMMap.put(mES_DOC_RELATIONSet, "INSERT");
                tMMap.put(mES_DOC_MAINSet, "INSERT");
                tMMap.put(mES_DOC_PAGESSet, "INSERT");
                tMMap.put(mES_DOC_HANDLERSet, "UPDATE");
                mVData.add(tMMap);
                if(!tPubSubmit.submitData(mVData, "")){	 
                	 mErrors.addOneError("数据提交报错");
                }
        	}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
	
	/***
	 * 处理影像件资料
	 * @param args
	 */
	private boolean deal(String path) throws Exception {

		if(SuccES_DOC_HANDLERSet.size()<0) {
			System.out.println("影像件数据有问题");
		}
		for(int i = 1 ; i <= SuccES_DOC_HANDLERSet.size() ; i++) {
			ES_DOC_HANDLERSchema SuccES_DOC_HANDLERSchema = new ES_DOC_HANDLERSchema();
			SuccES_DOC_HANDLERSchema = SuccES_DOC_HANDLERSet.get(i);
			mCaseNo = SuccES_DOC_HANDLERSchema.getCaseNo();
			mZipName = SuccES_DOC_HANDLERSchema.getFileName();
			mSerialNo = SuccES_DOC_HANDLERSchema.getSerialNo();
			mOperator = SuccES_DOC_HANDLERSchema.getOperater();
			mManageCom = SuccES_DOC_HANDLERSchema.getMngCom();
			System.out.println("path:" + path + " ,file:" + mZipName + ",CaseNo:"+ mCaseNo);
			String dir = path + mZipName;// 完整路径
			boolean flag = true;
			
			
			// 1、解压zip包
			String unRar="select codename,codealias from ldcode where codetype='LLTYScan' and code='UrlPicPath' ";
			
			SSRS tPathSSRS = new ExeSQL().execSQL(unRar);
			if (tPathSSRS.getMaxRow() < 1) {
				CError tError = new CError();
				tError.moduleName = "LLTYBDealScanImages";
				tError.functionName = "getConn";
				tError.errorMessage = "未设置文件解压路径";
				mErrors.addOneError(tError);
				continue;
			}
			String tUIRoot = CommonBL.getUIRoot();
			String unpath = tUIRoot+tPathSSRS.GetText(1, 1);
			pathManageCom = mManageCom.substring(0, 4);
			unpath = unpath+"/"+pathManageCom+"/"+PubFun.getCurrentDate()+"/"+mCaseNo+"/"; //同一解压到机构前四位/案件号/下
			HostName = tPathSSRS.GetText(1, 2);
//		unpath = "E:\\FTP文件\\LLSHScan\\"+PubFun.getCurrentDate()+"/"+mCaseNo+"/";
//		HostName = "本地";//本地调试
			if (!newFolder(unpath))
			{
				mErrors.addOneError("新建保存扫描件目录失败");
				continue;
			}		
			flag = unZip(dir, unpath);
			if (!flag) {
				ES_DOC_HANDLERSchema ttES_DOC_HANDLERSchema = new ES_DOC_HANDLERSchema();
		    	ES_DOC_HANDLERDB ttES_DOC_HANDLERDB = new ES_DOC_HANDLERDB();
		    	
		    	ttES_DOC_HANDLERDB.setSerialNo(mSerialNo);
		    	ttES_DOC_HANDLERDB.setCaseNo(mCaseNo);
		    	if(!ttES_DOC_HANDLERDB.getInfo()) {
		    		mErrors.addOneError("影像件异步处理数据查询失败!");
		    		continue;
		    	}
		    	ttES_DOC_HANDLERSchema = ttES_DOC_HANDLERDB.getSchema();
		    	ttES_DOC_HANDLERSchema.setState("3");
		    	ttES_DOC_HANDLERSchema.setRemark("文件解压异常");
		    	ttES_DOC_HANDLERSchema.setModifyDate(mCurrentDate);
				ttES_DOC_HANDLERSchema.setModifyTime(mCurrentTime);
				tmES_DOC_HANDLERSet.add(ttES_DOC_HANDLERSchema);
				continue;
			}
			System.out.println("**************** ③解压zip包完毕 ****************");
			
			
			// 4、处理扫描一套表
			flag = sendtoCore(unpath);
			if (!flag) {
				CError tError = new CError();
				tError.moduleName = "LLTYDealScanImages";
				tError.functionName = "sendtoCore";
				//tError.errorMessage = "扫描数据上传核心错误";
				this.mErrors.addOneError(tError);
				//deletefile(unpath);
				//deleteFiles(path, mZipName);
				ES_DOC_HANDLERSchema ttES_DOC_HANDLERSchema = new ES_DOC_HANDLERSchema();
		    	ES_DOC_HANDLERDB ttES_DOC_HANDLERDB = new ES_DOC_HANDLERDB();
		    	
		    	ttES_DOC_HANDLERDB.setSerialNo(mSerialNo);
		    	ttES_DOC_HANDLERDB.setCaseNo(mCaseNo);
		    	if(!ttES_DOC_HANDLERDB.getInfo()) {
		    		mErrors.addOneError("影像件异步处理数据异常");
		    		continue;
		    	}
		    	ttES_DOC_HANDLERSchema = ttES_DOC_HANDLERDB.getSchema();
		    	ttES_DOC_HANDLERSchema.setState("3");
		    	ttES_DOC_HANDLERSchema.setRemark("影像件数据异常");
		    	ttES_DOC_HANDLERSchema.setModifyDate(mCurrentDate);
				ttES_DOC_HANDLERSchema.setModifyTime(mCurrentTime);
				tmES_DOC_HANDLERSet.add(ttES_DOC_HANDLERSchema);
				continue;
			}
		}

		System.out.println("**************** ⑥理赔核心添加数据处理完毕 ****************");
		return true;

	}
	
	public boolean sendtoCore(String path){
		File root = new File(path);
		File[] tImages = root.listFiles();
		
		
//    	String mSunType = mImageInfos.getChildTextTrim("subtype");
//    	String mPages = mImageInfos.getChildTextTrim("NUMPAGES");
    	String mPages = String.valueOf(tImages.length);
    	System.out.println("mPages=" + mPages);
    	if(!"".equals(mPages)&& mPages!=null && !"null".equals(mPages)){  		
    		int k = 0;
    		int m = 0;
    		String strDocID = getMaxNo("DocID");
    		System.out.println("strDocID=" + strDocID);
        	if(tImages.length>0){
        	out:
            for (k = 0; k < tImages.length; ) 
            {
                String mPageCode = String.valueOf(k+1);
                String tPageNamejpg = tImages[k].getName();
                System.out.println("========+++++++++++tPageNamejpg = " + tPageNamejpg);
                String ImageSQL = "select  a.picpath,a.pagename,a.pagesuffix  from ES_DOC_PAGES a,ES_DOC_MAIN b where 1=1 "
            			+ "and a.docid = b.docid and b.doccode = '" + mCaseNo + "' order by a.makedate with ur";
                ExeSQL imageExeSQL = new ExeSQL();
                SSRS imageSSRS = imageExeSQL.execSQL(ImageSQL);
                int lengths = imageSSRS.getMaxRow();
                if(lengths>0) {
                	for (int i = 1; i <= lengths; i++) {
    					String imageName = imageSSRS.GetText(i, 2) + imageSSRS.GetText(i, 3);
    					if(tPageNamejpg.equals(imageName)) {
    						m++;
    						k++;
    						continue out;
    					}
    				}
                	k++;
                }
                if(lengths < 1) {
                	k++;
                }
                String mPageName = "";
                String mPageFormat = "";
                System.out.println("m=" + m + ",K=" + k);
           try{
        	   System.out.println("tPageNamejpg=" + tPageNamejpg);
        	   mPageName = tPageNamejpg.substring(0, tPageNamejpg.lastIndexOf("."));
               mPageFormat = tPageNamejpg.substring(tPageNamejpg.lastIndexOf("."));
               System.out.println("++++++++++++++" + mPageName);
               System.out.println("-------------" + mPageFormat);
           }catch(Exception e) {
        	   e.printStackTrace();
        	   mErrors.addOneError("影像件文件压缩异常");
        	   System.out.println("截取异常================");
        	   return false;
           }
            
//            String mPageSuffix = tPageInfoDate.getChildText("PAGESUFFIX");
//            String mPicPath = tPageInfoDate.getChildText("PICPATH");
           System.out.println("2222222222");
            if(!"".equals(mPageCode)&& mPageCode!=null && !"null".equals(mPageCode)&&!"".equals(mPageName)&& mPageName!=null && !"null".equals(mPageName)){
	           System.out.println("11111111111111+mPageCode=" + mPageCode);
            	ExeSQL tExeSql = new ExeSQL();
	            String tFilePathIp = tExeSql
	            	.getOneValue("select codealias from ldcode where codetype='LLTYScan' and code='UrlPicPath'");
	            
	            String tFilePathUrl = tExeSql
	        	.getOneValue("select codealias from ldcode where codetype='LLTYScan' and code='UrlPicPath'");
	            tFilePathUrl = "FTPPATH";
	            String tFilePath = tExeSql
	        	.getOneValue("select codename from ldcode where codetype='LLTYScan' and code='UrlPicPath'");
	            tFilePath = tFilePath +pathManageCom+"/"+PubFun.getCurrentDate() + "/"+ mCaseNo+"/"; //解压文件存放路径
				ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
				String strPageID = getMaxNo("PageID");
				tES_DOC_PAGESSchema.setHostName(tFilePathIp);
				tES_DOC_PAGESSchema.setPageCode(mPageCode);
				tES_DOC_PAGESSchema.setPageName(mPageName);
				tES_DOC_PAGESSchema.setPageSuffix(mPageFormat);
				tES_DOC_PAGESSchema.setPageFlag("1");
				tES_DOC_PAGESSchema.setPageType("0");
				tES_DOC_PAGESSchema.setPicPath(tFilePath);
				tES_DOC_PAGESSchema.setPicPathFTP(tFilePathUrl);
				tES_DOC_PAGESSchema.setManageCom(mManageCom);
				tES_DOC_PAGESSchema.setOperator(mOperator);
				tES_DOC_PAGESSchema.setPageID(strPageID);
				tES_DOC_PAGESSchema.setDocID(strDocID);
				tES_DOC_PAGESSchema.setMakeDate(mCurrentDate);
				tES_DOC_PAGESSchema.setModifyDate(mCurrentDate);
				tES_DOC_PAGESSchema.setMakeTime(mCurrentTime);
				tES_DOC_PAGESSchema.setModifyTime(mCurrentTime);
				mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
        	}               
        }
        if(m == k) {
             	mErrors.addOneError("影像件异步处理数据异常");
         		return false;
       	}
    	
    	ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
        mES_DOC_RELATIONSchema.setBussNo(mCaseNo);
		mES_DOC_RELATIONSchema.setDocCode(mCaseNo);
		mES_DOC_RELATIONSchema.setBussNoType("21");
		mES_DOC_RELATIONSchema.setDocID(strDocID);
		mES_DOC_RELATIONSchema.setBussType("LP");
		mES_DOC_RELATIONSchema.setSubType("LP01");
		mES_DOC_RELATIONSchema.setRelaFlag("0");
		mES_DOC_RELATIONSet.add(mES_DOC_RELATIONSchema);
    	}
        	
        	ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();
    		mES_DOC_MAINSchema.setDocCode(mCaseNo);
    		mES_DOC_MAINSchema.setInputStartDate(mCurrentDate);
    		mES_DOC_MAINSchema.setInputEndDate(mCurrentDate);
    		mES_DOC_MAINSchema.setSubType("LP01");
    		mES_DOC_MAINSchema.setBussType("LP");
    		mES_DOC_MAINSchema.setManageCom(mManageCom);
    		mES_DOC_MAINSchema.setVersion("01");
    		mES_DOC_MAINSchema.setScanNo("0");
    		mES_DOC_MAINSchema.setState("01");
    		mES_DOC_MAINSchema.setDocFlag("1");
    		mES_DOC_MAINSchema.setOperator(mOperator);
    		mES_DOC_MAINSchema.setScanOperator(mOperator);
    		mES_DOC_MAINSchema.setNumPages(mPages);
    		mES_DOC_MAINSchema.setDocID(strDocID);
    		mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
    		mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
    		mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
    		mES_DOC_MAINSchema.setModifyTime(mCurrentTime);
    		mES_DOC_MAINSet.add(mES_DOC_MAINSchema);
    	
    	ES_DOC_HANDLERSchema tES_DOC_HANDLERSchema = new ES_DOC_HANDLERSchema();
    	ES_DOC_HANDLERDB tES_DOC_HANDLERDB = new ES_DOC_HANDLERDB();
    	tES_DOC_HANDLERDB.setSerialNo(mSerialNo);
    	tES_DOC_HANDLERDB.setCaseNo(mCaseNo);
    	if(!tES_DOC_HANDLERDB.getInfo()) {
    		mErrors.addOneError("影像件异步处理数据查询失败!");
			return false;
    	}
    	tES_DOC_HANDLERSchema = tES_DOC_HANDLERDB.getSchema();
    	tES_DOC_HANDLERSchema.setState("1");
    	tES_DOC_HANDLERSchema.setModifyDate(mCurrentDate);
		tES_DOC_HANDLERSchema.setModifyTime(mCurrentTime);
		mES_DOC_HANDLERSet.add(tES_DOC_HANDLERSchema);
		
    	}
    	return true;
	}
	
	/***
	 * 获取参数
	 * @param args
	 */
	
	private boolean getInputData (VData inputData){
		mTransferData = (TransferData) inputData.getObjectByObjectName(
				"TransferData", 0);
		mZipLocalPath = (String) mTransferData.getValueByName("ZipLocalPath");
		SuccES_DOC_HANDLERSet = (ES_DOC_HANDLERSet) mTransferData.getValueByName("SuccES_DOC_HANDLERSet");
		
		return true;
	}
	
	/***
	 * 创建文件夹
	 * @param args
	 */
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdirs();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
	
	/**
	 * 解压zip包
	 */
	public boolean unZip(String file, String unpath) {
		DeCompressZip dec = new DeCompressZip();
		if (!dec.DeCompress(file, unpath)) {
			return false;
		}
		return true;
	}
	
	// 生成流水号，包含错误处理
	private String getMaxNo(String cNoType) {
		String strNo = PubFun1.CreateMaxNo(cNoType, 1);

		if (strNo.equals("") || strNo.equals("0")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UploadPrepareBL";
			tError.functionName = "getReturnData";
			tError.errorNo = "-90";
			tError.errorMessage = "生成流水号失败!";
			this.mErrors.addOneError(tError);
			strNo = "";
		}
		return strNo;
	}
			
	/*
	 * 各处理过程操作失败都需要删除本地缓存文件
	 */
	public boolean deleteFiles(String path, String file) {

		String folder = path + "/" + file;

		File zipFile = new File(folder);

		if (zipFile.exists()) {
			if (!zipFile.delete()) {
				System.out.println("111111");
				return false;
			}
		}

		return true;
	}
	
	// 删除目录下所有文件
			public boolean deletefile(String delpath) throws Exception {
				try {

					File file = new File(delpath);
					// 当且仅当此抽象路径名表示的文件存在且 是一个目录时，返回 true
					if (!file.isDirectory()) {
						file.delete();
					} else if (file.isDirectory()) {
						String[] filelist = file.list();
						for (int i = 0; i < filelist.length; i++) {
							File delfile = new File(delpath + "/" + filelist[i]);
							if (!delfile.isDirectory()) {
								delfile.delete();
								System.out
										.println(delfile.getAbsolutePath() + "删除文件成功");
							} else if (delfile.isDirectory()) {
								deletefile(delpath + "/" + filelist[i]);
							}
						}
						System.out.println(file.getAbsolutePath() + "删除成功");
						file.delete();
					}

				} catch (FileNotFoundException e) {
					System.out.println("deletefile() Exception:" + e.getMessage());
				}
				return true;
			}


	public static void main(String[] args) {
		LLCommonDealScanImages tLLTYDealScanImages = new LLCommonDealScanImages();
		 VData tVData = new VData();
		 TransferData transferData = new TransferData();
		 transferData.setNameAndValue("ZipLocalPath", "E:\\PADTEST\\iMAGES\\zip\\");
		 transferData.setNameAndValue("ZipName", "test.zip");
		 transferData.setNameAndValue("CaseNo", "C4400151117000029");
		 GlobalInput mGlobalInput = new GlobalInput();
		 mGlobalInput.Operator = "001";
		 mGlobalInput.ManageCom = "8601";
		 tVData.add(mGlobalInput);
		 tVData.add(transferData);

		 try {
			 tLLTYDealScanImages.submitData(tVData);
		 } catch (Exception e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }

	}


}
