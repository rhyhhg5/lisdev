package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;

public class LLHospServiceTest implements ServiceInterface {
	public Document service(Document pInXmlDoc) {
		return createFalseXML();
	}
	
	/**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() {
        Element tRootData = new Element("PACKET");
        tRootData.setText("normal");
        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText("01");
        tHeadData.addContent(tRequestType);

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText("00");
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText("Error");
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText("12312312");
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
}
