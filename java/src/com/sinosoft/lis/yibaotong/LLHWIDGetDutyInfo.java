package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * @author lishen
 * 
 * 2017-12-18
 */
public class LLHWIDGetDutyInfo implements ServiceInterface {

	public LLHWIDGetDutyInfo() {
	}
	
	//
	public CErrors mErrors = new CErrors();
	
	//
	public VData mResult = new VData();
	
	// 传入报文
	private Document mInXmlDoc;

	// 报文类型
	private String mDocType = "";

	// 处理标志
	private boolean mDealState = true;

	// 返回类型代码
	private String mResponseCode = "1";

	// 请求类型
	private String mRequestType = "";

	// 错误描述
	private String mErrorMessage = "";

	// 交互编码
	private String mTransactionNum = "";
    private String mManageCom="";   //
	public SSRS mSSRS1 = new SSRS();
	
	// 查询的证件号
	private String mIDNO = "";
	
	// 报文BODY部分
	private Element tBODY=new Element("BODY");
	//保单险种责任信息
	private Element tPOLCIYLIST=new Element("POLCIYLIST");
	//保单险种责任信息（多条）
	private Element tPOLICY_DATA=new Element("POLICY_DATA");
	

	/*
	 * 
	 * @see
	 * com.sinosoft.lis.yibaotong.ServiceInterface#service(org.jdom.Document)
	 */
	public Document service(Document pInXmlDoc) {
		System.out.println("LLHWIDGetDutyInfo--service");
		mInXmlDoc = pInXmlDoc;

		try {
			if (!getInputData(mInXmlDoc)) {
				mDealState = false;
			} else {
				if (!checkData()) {
					mDealState = false;
				}else{
					if (!dealData()) {
                    mDealState = false;
					}
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			mDealState = false;
			mResponseCode = "E";
			buildError("service()", "系统未知错误");
		} finally {
			return createXML();
		}
	}

	/**
	 * 解析报文主要部分
	 * 
	 * @param cInXml
	 *            Document
	 * @return boolean
	 */
	private boolean getInputData(Document cInXml) throws Exception {
		System.out.println("LLHWIDGetDutyInfo--getInputData");
		if (cInXml == null) {
			buildError("getInputData()", "未获得报文");
			return false;
		}

		Element tRootData = cInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");

		// 获取HEAD部分内容
		Element tHeadData = tRootData.getChild("HEAD");

		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

		System.out.println("LLHWIDGetDutyInfo--TransactionNum:"+ mTransactionNum);

		// 获取BODY部分内容
		Element tinBODY = tRootData.getChild("BODY");
		Element tinINSURED_DATA = tinBODY.getChild("INSURED_DATA");
		mIDNO = tinINSURED_DATA.getChildTextTrim("IDNO");

		return true;
	}

	/**
	 * 校验基本数据
	 * 
	 * @return boolean
	 */
	 private boolean checkData(){
	        
	        if (!"REQUEST".equals(mDocType)) {
	            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
	            return false;
	        }

	        if (!"HW05".equals(mRequestType)) {
	            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
	            return false;
	        }
	        
	        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
	            buildError("checkData()", "【交互编码】的编码个数错误!");
	            return false;
	        }

	        if (!"HW05".equals(mTransactionNum.substring(0, 4))) {
	            buildError("checkData()", "【交互编码】的请求类型编码错误!");
	            return false;
	        }

	        mManageCom = mTransactionNum.substring(4, 12);        
	        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86310000".equals(mManageCom)) {
	            buildError("checkData()", "【交互编码】的固定值错误!");
	            return false;
	        }   
	        return true;
	    }
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	String chksql1= "select "+
    					"lcp.contno,"+
    					"lcp.prem,"+
    					"lcp.amnt,"+
    					"lcp.insuredname,"+
    					"lcp.insuredno,"+
    					"lci.sex,"+
    					"lci.IDType,"+
    					"lci.idno,"+
    					"lci.Birthday,"+
    					"lcp.cvalidate,"+
    					"lcp.enddate,"+
    					"lcp.riskcode,"+
    					"(select riskname from lmriskapp where riskcode = lcp.riskcode fetch first row only),"+
    					"lcd.dutycode,"+
    					"(select dutyname from lmduty where dutycode= lcd.dutycode),"+
    					"lcg.getdutycode,"+
    					"(select getdutyname from lmdutyget where getdutycode = lcg.getdutycode),"+
    					" lmd.getdutykind,"+
    					" (SELECT codename FROM ldcode WHERE codetype = 'getdutykind' AND code = lmd.getdutykind),"+
    					"lcd.prem,"+
    					"lcd.amnt,"+
    					"lcd.getrate,"+
    					"lcd.getlimit,"+
    					"(select codename from ldcode where codetype = 'stateflag' and code = lcp.stateflag),"+
    					"lci.BankCode,"+
    					"(select bankname from ldbank where bankcode = lci.BankAccNo),"+
    					"lci.BankAccNo,"+
    					"lci.AccName,"+
    					"(case when lci.RelationToMainInsured = '00' then '本人' "+
    					"when lci.RelationToMainInsured = '01' then '妻子' "+
    					"when lci.RelationToMainInsured = '02' then '丈夫' "+
    					"when lci.RelationToMainInsured = '03' then '儿女' "+
    					"when lci.RelationToMainInsured = '04' then '父亲' "+
    					"when lci.RelationToMainInsured = '05' then '母亲' "+
    					"when lci.RelationToMainInsured = '06' then '岳父' "+
    					"when lci.RelationToMainInsured = '07' then '岳母' "+
						"else '' end),"+
						"(select name from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select idtype from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select idno from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select insuredno from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select sex from lcinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only)"+
						"from lcpol lcp,lcinsured lci,lcget lcg,lcduty lcd,lmdutygetclm lmd "+
						"where 1=1 "+
						"and lcp.appflag ='1' "+
						"and lcp.stateflag <>'4' "+
						"and lmd.getdutycode=lcg.getdutycode "+
						"and lcp.prtno = lci.prtno "+
						"and lcg.contno = lcp.contno "+
						"and lcd.contno = lcp.contno "+
						"and lcd.contno = lcg.contno "+
						"and lcp.insuredno = lci.insuredno "+
						"and lci.contno = lcp.contno "+
						"and lcp.polno = lcg.polno "+
						"and lcg.polno = lcd.polno "+
						"and lcg.DutyCode = lcd.DutyCode "+
						"and lci.idno='"+mIDNO+"' "+
						"union "+
						"select "+
						"lcp.contno,"+
						"lcp.prem,"+
						"lcp.amnt,"+
						"lcp.insuredname,"+
						"lcp.insuredno,"+
						"lci.sex,"+
    					"lci.IDType,"+
    					"lci.idno,"+
    					"lci.Birthday,"+
						"lcp.cvalidate ,"+
    					//修改保单失效日期
    					"(case when (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lp.EdorNo and lp.EdorNo = lcp.EdorNo and lpdiskimport.insuredno = lcp.insuredno)  is not null then (select (case when max(lpdiskimport.EdorValiDate) is null then  max(lp.EdorValiDate) else max(lpdiskimport.EdorValiDate)   end) from LPEdorItem lp, lpdiskimport where lpdiskimport.EdorNo = lp.EdorNo and lp.EdorNo = lcp.EdorNo and lpdiskimport.insuredno = lcp.insuredno) when (select edorvalidate from lpedoritem where edorno = lcp.edorno fetch first 1 rows only) is not null then (select edorvalidate from lpedoritem where edorno = lcp.edorno fetch first 1 rows only) when (select edorvalidate from lpgrpedoritem where edorno = lcp.edorno fetch first 1 rows only) is not null then (select edorvalidate from lpgrpedoritem where edorno = lcp.edorno fetch first 1 rows only) else lcp.paytodate end) as CInValiDate,"+
						"lcp.riskcode,"+
						"(select riskname from lmriskapp where riskcode = lcp.riskcode fetch first row only),"+
						"lcd.dutycode,"+
						"(select dutyname from lmduty where dutycode= lcd.dutycode),"+
						"lcg.getdutycode,"+
						"(select getdutyname from lmdutyget where getdutycode = lcg.getdutycode),"+
						"lmd.getdutykind,"+
    					"(SELECT codename FROM ldcode WHERE codetype = 'getdutykind' AND code = lmd.getdutykind),"+
    					"lcp.prem,"+
    					"lcd.amnt,"+
						"lcd.getrate,"+
						"lcd.getlimit,"+
						"(select codename from ldcode where codetype = 'stateflag' and code = lcp.stateflag),"+
						"lci.BankCode,"+
						"(select bankname from ldbank where bankcode = lci.BankAccNo),"+
						"lci.BankAccNo,"+
						"lci.AccName,"+
						"(case when lci.RelationToMainInsured = '00' then '本人' "+
									"when lci.RelationToMainInsured = '01' then '妻子' "+
									"when lci.RelationToMainInsured = '02' then '丈夫' "+
									"when lci.RelationToMainInsured = '03' then '儿女' "+
									"when lci.RelationToMainInsured = '04' then '父亲' "+
									"when lci.RelationToMainInsured = '05' then '母亲' "+
									"when lci.RelationToMainInsured = '06' then '岳父' "+
									"when lci.RelationToMainInsured = '07' then '岳母' "+
									"else '' end),"+
						"(select name from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select idtype from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select idno from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select insuredno from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only),"+
						"(select sex from lbinsured where contno=lci.contno and RelationToMainInsured='00' fetch first 1 rows only) "+
						"from lbpol lcp,lbinsured lci,lbget lcg,lbduty lcd,lmdutygetclm lmd "+
						"where 1=1 "+
						"and lcp.appflag='1' "+
						"and lcp.stateflag <>'4' "+
						"and lmd.getdutycode=lcg.getdutycode "+
						"and lcp.prtno = lci.prtno "+
						"and lcg.contno = lcp.contno "+
						"and lcd.contno = lcp.contno "+
						"and lcd.contno = lcg.contno "+
						"and lcp.insuredno = lci.insuredno "+
						"and lci.contno = lcp.contno "+
						"and lcp.polno = lcg.polno "+
						"and lcg.polno = lcd.polno "+
						"and lcg.DutyCode = lcd.DutyCode "+
						"and lci.idno='"+mIDNO+"' with ur";
    	mSSRS1=tExeSQL.execSQL(chksql1);
		if(mSSRS1!=null&&mSSRS1.getMaxRow()>0){
    		
		}else{
			buildError("dealData()", "本次理赔申请不在外包理赔范围内，请您到公司柜面申请理赔");
            mDealState = false; 
		}  	
    	return true;
    }

	/**
	 * 
	 * 
	 * @return boolean
	 */
	private boolean qryPolicyList() throws Exception {
		System.out.println("TestInfo--qryPolicyList");
		for (int i = 1; i <= mSSRS1.getMaxRow();i++) {
			Element tPOLICY_DATA = new Element("POLICY_DATA");
			Element tCONTNO = new Element("CONTNO");
			Element tPREM = new Element("PREM");
			Element tAMNT = new Element("AMNT");
			Element tINSUREDNAME = new Element("INSUREDNAME");
			Element tINSUREDNNO = new Element("INSUREDNNO");
			Element tINSUREDSEX = new Element("INSUREDSEX");
			Element tINSUREDIDTYPE = new Element("INSUREDIDTYPE");
			Element tINSUREDIDIDNO = new Element("INSUREDIDIDNO");
			Element tINSUREDBIRTHDAY = new Element("INSUREDBIRTHDAY");
			Element tCVALIDATE = new Element("CVALIDATE");
			Element tCINVALIDATE = new Element("CINVALIDATE");
			Element tRISKCODE  = new Element("RISKCODE");
			Element tRISKNAME = new Element("RISKNAME");
			Element tDUTYCODE = new Element("DUTYCODE");		
			Element tDUTYNAME = new Element("DUTYNAME");
			Element tGETDUTYCODE  = new Element("GETDUTYCODE");
			Element tGETDUTYNAME  = new Element("GETDUTYNAME");
			Element tGETDUTYKIND = new Element("GETDUTYKIND");
			Element tGETDUTYKINDNAME = new Element("GETDUTYKINDNAME");
			Element tDUTYPREM = new Element("DUTYPREM");
			Element tDUTYAMNT = new Element("DUTYAMNT");
			Element tGETRATE = new Element("GETRATE");
			Element tGETLIMIT = new Element("GETLIMIT");
			Element tSTATEFLAG = new Element("STATEFLAG");
			Element tBANKNO = new Element("BANKNO");
			Element tBANKNAME = new Element("BANKNAME");
			Element tACCNO = new Element("ACCNO");
			Element tACCNAME = new Element("ACCNAME");
			Element tRELATIONTOMAININSURED = new Element("RELATIONTOMAININSURED");
			Element tNAME = new Element("NAME");
			Element tIDTYPE = new Element("IDTYPE");
			Element tIDNO = new Element("IDNO");
			Element tCUSTOMERNO = new Element("CUSTOMERNO");
			Element tSEX = new Element("SEX");
			
			tCONTNO.setText(mSSRS1.GetText(i, 1));
			tPREM.setText(mSSRS1.GetText(i, 2));
			tAMNT.setText(mSSRS1.GetText(i, 3));
			tINSUREDNAME.setText(mSSRS1.GetText(i, 4));
			tINSUREDNNO.setText(mSSRS1.GetText(i, 5));
			tINSUREDSEX.setText(mSSRS1.GetText(i, 6));
			tINSUREDIDTYPE.setText(mSSRS1.GetText(i, 7));
			tINSUREDIDIDNO.setText(mSSRS1.GetText(i, 8));
			tINSUREDBIRTHDAY.setText(mSSRS1.GetText(i, 9));
			tCVALIDATE.setText(mSSRS1.GetText(i, 10));
			tCINVALIDATE.setText(mSSRS1.GetText(i, 11));
			tRISKCODE.setText(mSSRS1.GetText(i, 12));
			tRISKNAME.setText(mSSRS1.GetText(i, 13));
			tDUTYCODE.setText(mSSRS1.GetText(i, 14));			
			tDUTYNAME.setText(mSSRS1.GetText(i, 15));
			tGETDUTYCODE.setText(mSSRS1.GetText(i, 16));
			tGETDUTYNAME.setText(mSSRS1.GetText(i, 17));
			tGETDUTYKIND.setText(mSSRS1.GetText(i, 18));
			tGETDUTYKINDNAME.setText(mSSRS1.GetText(i, 19));
			tDUTYPREM.setText(mSSRS1.GetText(i, 20));
			tDUTYAMNT.setText(mSSRS1.GetText(i, 21));
			tGETRATE.setText(mSSRS1.GetText(i, 22));
			tGETLIMIT.setText(mSSRS1.GetText(i, 23));
			tSTATEFLAG.setText(mSSRS1.GetText(i, 24));
			tBANKNO.setText(mSSRS1.GetText(i, 25));
			tBANKNAME.setText(mSSRS1.GetText(i, 26));
			tACCNO.setText(mSSRS1.GetText(i, 27));			
			tACCNAME.setText(mSSRS1.GetText(i, 28));
			tRELATIONTOMAININSURED.setText(mSSRS1.GetText(i, 29));
			tNAME.setText(mSSRS1.GetText(i, 30));
			tIDTYPE.setText(mSSRS1.GetText(i, 31));
			tIDNO.setText(mSSRS1.GetText(i, 32));
			tCUSTOMERNO.setText(mSSRS1.GetText(i, 33));
			tSEX.setText(mSSRS1.GetText(i, 34));

			tPOLICY_DATA.addContent(tCONTNO);
			tPOLICY_DATA.addContent(tPREM);
			tPOLICY_DATA.addContent(tAMNT);
			tPOLICY_DATA.addContent(tINSUREDNAME);
			tPOLICY_DATA.addContent(tINSUREDNNO);
			tPOLICY_DATA.addContent(tINSUREDSEX);
			tPOLICY_DATA.addContent(tINSUREDIDTYPE);
			tPOLICY_DATA.addContent(tINSUREDIDIDNO);
			tPOLICY_DATA.addContent(tINSUREDBIRTHDAY);
			tPOLICY_DATA.addContent(tCVALIDATE);
			tPOLICY_DATA.addContent(tCINVALIDATE);
			tPOLICY_DATA.addContent(tRISKCODE);
			tPOLICY_DATA.addContent(tRISKNAME);
			tPOLICY_DATA.addContent(tDUTYCODE);
			tPOLICY_DATA.addContent(tDUTYNAME);			
			tPOLICY_DATA.addContent(tGETDUTYCODE);
			tPOLICY_DATA.addContent(tGETDUTYNAME);
			tPOLICY_DATA.addContent(tGETDUTYKIND);
			tPOLICY_DATA.addContent(tGETDUTYKINDNAME);
			tPOLICY_DATA.addContent(tDUTYPREM);
			tPOLICY_DATA.addContent(tDUTYAMNT);
			tPOLICY_DATA.addContent(tGETRATE);
			tPOLICY_DATA.addContent(tGETLIMIT);
			tPOLICY_DATA.addContent(tSTATEFLAG);
			tPOLICY_DATA.addContent(tBANKNO);			
			tPOLICY_DATA.addContent(tBANKNAME);
			tPOLICY_DATA.addContent(tACCNO);
			tPOLICY_DATA.addContent(tACCNAME);
			tPOLICY_DATA.addContent(tRELATIONTOMAININSURED);
			tPOLICY_DATA.addContent(tNAME);
			tPOLICY_DATA.addContent(tIDTYPE);
			tPOLICY_DATA.addContent(tIDNO);
			tPOLICY_DATA.addContent(tCUSTOMERNO);
			tPOLICY_DATA.addContent(tSEX);	
			tPOLCIYLIST.addContent(tPOLICY_DATA);
		}
		System.out.println("LLHWIDGetDutyInfo--qryPolicyListover");
		return true;
	}

	/**
	 * 生成返回的报文信息
	 * 
	 * @return Document
	 */
	private Document createXML() {
		if (!mDealState) {
			return createFalseXML();
		}else{
			try {
				return createResultXML(mIDNO);
			} catch (Exception ex) {
				buildError("createXML()", "生成返回报文错误");
				return createFalseXML();
			}
		}
	}

	/**
	 * 生成正常返回报文
	 * 
	 * @return Document
	 */
	private Document createResultXML(String aCaseNo) throws Exception {
		System.out.println("TestInfo--createResultXML(返回正确报文)");
		Element tRootData = new Element("PACKET");
		tRootData.addAttribute("type", "RESPONSE");
		tRootData.addAttribute("version", "1.0");

		// Head部分
		Element tHeadData = new Element("HEAD");

		Element tRequestType = new Element("REQUEST_TYPE");
		tRequestType.setText(mRequestType);
		tHeadData.addContent(tRequestType);

		Element tTransactionNum = new Element("TRANSACTION_NUM");
		tTransactionNum.setText(mTransactionNum);
		tHeadData.addContent(tTransactionNum);

		tRootData.addContent(tHeadData);

		// Body部分
		// 报文保单责任部分
		qryPolicyList();
		// 报文信息
		tBODY.addContent(tPOLCIYLIST);

		tRootData.addContent(tBODY);

		Document tDocument = new Document(tRootData);
		return tDocument;
	}

	/**
	 * 生成错误信息报文
	 * 
	 * @return Document
	 */
	private Document createFalseXML() {
		
	    Element tRootData = new Element("PACKET");
	    tRootData.addAttribute("type", "RESPONSE");
	    tRootData.addAttribute("version", "1.0");

	    //Head部分
	    Element tHeadData = new Element("HEAD");

	    Element tRequestType = new Element("REQUEST_TYPE");
	    tRequestType.setText(mRequestType);
	    tHeadData.addContent(tRequestType);


	    Element tTransactionNum = new Element("TRANSACTION_NUM");
	    tTransactionNum.setText(mTransactionNum);
	    tHeadData.addContent(tTransactionNum);

	    tRootData.addContent(tHeadData);

	    //Body部分
	    Element tBodyData = new Element("BODY");
	    //返回的案件处理结构
	    System.out.println("开始返回错误报文生成!-----");
        Element response = new Element("RESPONSE_CODE");
        mErrorMessage = mErrors.getFirstError();
        response.setText("0");
        tBodyData.addContent(response);
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);
        tRootData.addContent(tBodyData);
        Document tDocument = new Document(tRootData);
        return tDocument;
	}

	/**
	 * 追加错误信息
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "TestInfo";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Document tInXmlDoc;
		try {
			tInXmlDoc = JdomUtil.build(new FileInputStream(
					"E:/peihe/发送报文-DSJK_001-2.xml"), "GBK");
			LLHWIDGetDutyInfo tBusinessDeal = new LLHWIDGetDutyInfo();
//			long tStartMillis0 = System.currentTimeMillis();
			Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
			//JdomUtil.print(tInXmlDoc);
			JdomUtil.print(tOutXmlDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
