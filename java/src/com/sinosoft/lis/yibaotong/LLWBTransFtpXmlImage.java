package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LLWBTransFtpXmlImage
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();

    public LLWBTransFtpXmlImage()
    {
    }

    public boolean submitData()
    {
        if (getSubmitMap() == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;

    }

    public MMap getSubmitMap()
    {

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
       
        return true;
    }

    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }


    private boolean getXls()
    {
        String tUIRoot = CommonBL.getUIRoot();

        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        int xmlCount = 0;

        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='IP/Port'");
        String tPort = tExeSql
                .getOneValue("select codealias from ldcode where codetype='WBClaim' and code='IP/Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='User/Pass'");
        String tPassword = tExeSql
                .getOneValue("select codealias from ldcode where codetype='WBClaim' and code='User/Pass'");

  
        String tFileBackPath = tExeSql
                .getOneValue("select codename from ldcode where codetype='WBClaim' and code='UrlPicPath'");
        tFileBackPath = tUIRoot + tFileBackPath;
        
/*//        本地测试用
        String tFileBackPathP = tExeSql
        	.getOneValue("select codename from ldcode where codetype='WBClaim' and code='BackXmlPath'");

        String tFileBackPathPic = tExeSql
        	.getOneValue("select codealias from ldcode where codetype='WBClaim' and code='BackXmlPath'");
        tFileBackPathPic = tFileBackPathP + tFileBackPathPic + PubFun.getCurrentDate()+"\\";	//服务器存放文件的路径
*/
        try
        {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "CardActiveBatchImportBL";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
           
//            String tFilePathLocal = tFileBackPath;
//            String tFilePathLocal = "E:\\ceshi\\image\\";//本地测试
            String tFilePathLocalCore = tFileBackPath+PubFun.getCurrentDate()+"/";//ftp上的文件拿到本地的地址
//            String tFileImportBackPath = tFilePath;
            String tFileImportPath = "/01PH/8695/WBClaimImage";//ftp文件存放目录
            String tFileImportPathBackUp = "/01PH/8695/WBClaimBackUp/"+PubFun.getCurrentDate()+"/";//FTP备份目录
//            if(!newFolder(tUIRoot+"xerox/EasyScan2012/WBClaim/")){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tUIRoot+"xerox/EasyScan2012/WBClaim/8695/")){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
//            if(!newFolder(tFilePathLocalCore)){
//            	mErrors.addOneError("新建目录失败");
//                return false;
//            }
            File tFileDir = new File(tFilePathLocalCore);
    		if (!tFileDir.exists()) {
    			if (!tFileDir.mkdirs()) {
    				System.err.println("创建目录[" + tFilePathLocalCore.toString() + "]失败！"
    						+ tFileDir.getPath());
    			}
    		}
//            tFTPTool.changeWorkingDirectory("/8695/WBClaimBackUp/");
//            tFTPTool.makeDirectory(PubFun.getCurrentDate());
            if(!tFTPTool.makeDirectory(tFileImportPathBackUp)){
            	System.out.println("新建目录已存在");
            }
            
            System.out.println("OK");
//            tFTPTool.changeWorkingDirectory(tFileImportPathBackUp);
//            tFTPTool.deleteFile("18201506083.xml");
//            tFTPTool.removeDirectory(tFileImportPathBackUp);
//            System.out.println("OK");
//            tFTPTool.uploadAll(tFileImportPath, tFilePathLocal);
            
            tFTPTool.setFileType(FTP.BINARY_FILE_TYPE);
            String[] tPath = tFTPTool.downloadAllDirectory(tFileImportPath, tFilePathLocalCore);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                return false;
            }

            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }


                xmlCount++;
                                
                    tFTPTool.upload(tFileImportPathBackUp, tFilePathLocalCore+tPath[j]);
                    
                    tFTPTool.changeWorkingDirectory(tFileImportPath);
                    tFTPTool.deleteFile(tPath[j]);
                    
                    
                    
//                    tFTPTool.upload(tFileImportBackPath, tFilePathout+tPath[j]);
//                    tFTPTool.deleteFile(tPath[j]);

                
                
            }
            tFTPTool.logoutFTP();

        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }

    private boolean getInputData()
    {
      

        return true;
    }

    public String getDealInfo()
    {
        return mDealInfo;
    }
    
//  建文件夹
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args)
    {
    	LLWBTransFtpXmlImage a =new LLWBTransFtpXmlImage();
    	a.getXls();
    }
}
