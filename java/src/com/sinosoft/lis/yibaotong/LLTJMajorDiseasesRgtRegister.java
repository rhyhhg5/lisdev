package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.FileInputStream;

/**
 * <p>Title: 天津大病批次报案处理-申请批次</p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLTJMajorDiseasesRgtRegister implements ServiceInterface {
    public LLTJMajorDiseasesRgtRegister() {
    }

    public CErrors mErrors = new CErrors();
//    private VData mInputData = new VData();
    public VData mResult = new VData();
//    private GlobalInput mGlobalInput = new GlobalInput(); 
    private MMap mMMap = new MMap();
    private String mErrorMessage = "";	//错误描述    
   
    private Document mInXmlDoc;	 			//传入报文    
    private String mDocType = "";			//报文类型    
    private boolean mDealState = true;		//处理标志    
    private String mResponseCode = "1";		//返回类型代码    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码
//    private String mDealType = "1";    		//处理类型 1 实时 2非实时   
    
    private Element mBaseData;			//报文BATCH _DATA批次立案信息部分    
    		
    private String mManageCom = "";				//机构编码   
    private String mOperator  = "ss1201";				//操作员    
    
    /**
     * 对方报文传入信息
     */
    private String mGrpContNo = "";//团体保单号
    private String mAppPeoples = "";//申请人数(包含录入申请人数)
    private String mTogetherFlag = "";//给付方式
    private String mCaseGetMode = "";//赔款领取方式
    private String mAppAmnt = "";//申报金额
    private String mBankCode = "";//银行编码
    private String mBankName = "";//银行名称
    private String mAccName = "";//账户名
    private String mBankAccNo = "";//账号
    
    /**
     * 生成批次必要信息
     */
    private String mRgtNo = "" ;
    private String mCustomerNo = "";//团体客户号
    private String mGrpName = "";//单位名称
    private String mRgtantPostCode = "";//邮政编码，对应表中立案人/申请人邮政编码“申请人邮政编码”
    private String mRgtantAddress = "";//联系地址，对应表中“申请人地址”
    private String mRgtantPhone = "";//联系电话，对应表中是“申请人电话”
    private String mRgtMobile = "";//移动电话
    private String mRgtantName = "";//申请人姓名
    
    private ExeSQL mExeSQL = new ExeSQL();
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();

    public Document service(Document pInXmlDoc) {
        //大病程序处理接口
        System.out.println("LLTJMajorDiseasesRgtRegister--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } 

            if (!checkData()) {
                mDealState = false;
            } else {
                if (!dealData()) {
                    mDealState = false;
                }else {                                 	                    	
                	mResult.clear();
                	mResult.add(mMMap);
                	try{
                    	PubSubmit tPubSubmit = new PubSubmit();
                        if (!tPubSubmit.submitData(mResult, "")) {
                        	 mDealState = false;
                        }
                	} catch (Exception ex) {
                         mDealState = false;
                         mResponseCode = "E";
                         buildError("service()", "系统未知错误");
                	}
                }
            }
        
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	mInXmlDoc = createXML();
        }
    	return mInXmlDoc;
    }
    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
        	try {
        		if("DB01".equals(mRequestType)){
        			return createResultXML(mRgtNo);
        		}else{
        			return createFalseXML();
        		}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }     

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJMajorDiseasesRgtRegister--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");//此字段无实际意义

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");//报文类型-主要信息
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");//会进行唯一性校验
        System.out.println("LLTJMajorDiseasesRgtRegister--TransactionNum:" + mTransactionNum);
        
        if("DB01".equals(mRequestType))
        {       	
        	//获取BODY部分内容
            Element tBodyData = tRootData.getChild("BODY");
            mBaseData = tBodyData.getChild("BATCH_DATA");		//批次申请基本信息 
            
            List tBaseData = new ArrayList();
            tBaseData = (List)tBodyData.getChildren();
            if(tBaseData.size()!=1){
            	buildError("dealData", "【团体客户信息】为空或存在多条,请核对");
                return false;
            }
        }
        
        return true;

    }
    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLTJMajorDiseasesRgtRegister--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"DB01".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误");
            return false;
        }

        if (!"DB01".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86120000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
        
        return true;
    }
    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLTJMajorDiseasesRgtRegister--dealData");  
              
        mGrpContNo = mBaseData.getChildText("GRPCONTNO");//保单号码
        mAppPeoples = mBaseData.getChildText("APPPEOPLES");//申请人数
        mTogetherFlag = mBaseData.getChildText("TOGETHERFLAG");//给付方式
        mCaseGetMode = mBaseData.getChildText("CASEGETMODE");//赔款领取方式
        mAppAmnt = mBaseData.getChildText("APPAMNT");//申报金额
        mBankCode = mBaseData.getChildText("BANKCODE");//银行编码
        mBankName = mBaseData.getChildText("BANKNAME");//银行名称
        mAccName = mBaseData.getChildText("ACCNAME");//账户名
        mBankAccNo = mBaseData.getChildText("BANKACCNO");//账号
        
        /*
         * 基本校验
         */      
        if (mGrpContNo == null || "".equals(mGrpContNo) || mGrpContNo.equals("null")) {
            buildError("dealData", "【保单号码】的值不能为空");
            return false;
        }
        if (mAppPeoples == null || "".equals(mAppPeoples) || mAppPeoples.equals("null")) {
            buildError("dealData", "【申请人数】的值不能为空");
            return false;
        }
        if (mTogetherFlag == null || "".equals(mTogetherFlag)|| mTogetherFlag.equals("null")) {
        	mTogetherFlag = "3";//默认为全部统一给付
        }
        if (mCaseGetMode == null || "".equals(mCaseGetMode)|| mCaseGetMode.equals("null")) {
        	mCaseGetMode = "4";//默认为银行转账
        }
        if (mAppAmnt == null || "".equals(mAppAmnt) || mAppAmnt.equals("null")) {
            buildError("dealData", "【申报金额】的值不能为空");
            return false;
        }            
        if(mCaseGetMode != "1" && mCaseGetMode != "2" 
        	&& (mTogetherFlag == "3" || mTogetherFlag == "2"))
    	{
            if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
                buildError("dealData", "【银行编码】的值不能为空");
                return false;
            }
            if (mBankName == null || "".equals(mBankName)|| mBankName.equals("null")) {
                buildError("dealData", "【银行名称】的值不能为空");
                return false;
            }
            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
                buildError("dealData", "【账户名】的值不能为空");
                return false;
            }
            if (mBankAccNo == null || "".equals(mBankAccNo)|| mBankAccNo.equals("null")) {
                buildError("dealData", "【账号】的值不能为空或账号长度超过16位");
                return false;
            }
    	}
      
        Pattern tPattern = Pattern.compile("(\\d)+");
		Matcher isNum = tPattern.matcher(mBankAccNo);

		if (!isNum.matches()) {
			buildError("dealData", "【账号】只能是数字组成");
			return false;
		}
                  
        /*
         * 批次受理
         */
    	if (!RgtRegister()) {                        	
    		return false;
    	}             		              
          

        return true;
    }
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
    	 System.out.println("LLTJMajorDiseasesRgtRegister--createResultXML(正常返回报文)");
    	 Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);

//         Element tResponseCode = new Element("RESPONSE_CODE");
//         tResponseCode.setText("1");
//         tHeadData.addContent(tResponseCode);
//
//         Element tErrorMessage = new Element("ERROR_MESSAGE");
//         tErrorMessage.setText(mErrors.getFirstError());
//         tHeadData.addContent(tErrorMessage);

         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BACK_DATA");
         tBodyData.addContent(tBackData);        
         Element tRgtNo = new Element("RGTNO");
         tRgtNo.setText(aCaseNo);
         tBackData.addContent(tRgtNo);                
         tRootData.addContent(tBodyData); 

         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
    	System.out.println("LLTJAccidentAutoRegister--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        //Head部分
        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        tRootData.addContent(tHeadData);

        //错误编码转换
        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        //Body部分
        Element tBodyData = new Element("BODY");
        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBodyData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);
        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
   
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJAccidentAutoRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
   
    /**
     * 解析批次受理信息
     * @return boolean
     */
    private boolean RgtRegister() throws Exception {
        System.out.println("LLTJMajorDiseasesRgtRegister--caseRegister");     
        
        //1.通过团体保单号查询相关基础信息
        String tAddressNo = "";
        String tGrpContSql = "select a.customerno, a.name, " +
        		"a.AddressNo " +
        		"from lcgrpcont g, LCGrpAppnt a " +
        		"where a.grpcontno = g.grpcontno and g.appflag = '1' " +
        		"and g.GrpContNo='"+mGrpContNo+"' with ur ";
        SSRS tGrpContSSRS = new SSRS();
        tGrpContSSRS = mExeSQL.execSQL(tGrpContSql);
        if (tGrpContSSRS == null || tGrpContSSRS.getMaxRow() <= 0) {
            buildError("RgtRegister", "【保单号码】在核心不存在");
            return false;
        }else{
            for(int i=1;i <=tGrpContSSRS.getMaxRow();i++){
                mCustomerNo = tGrpContSSRS.GetText(i, 1);
                mGrpName = tGrpContSSRS.GetText(i, 2);
                tAddressNo = tGrpContSSRS.GetText(i, 3);
                
                //判断单位名称过长的话要截取
                if(mGrpName.length() >19){
                	mGrpName=mGrpName.substring(0, 19);
                }
            }
        }
        String tGrpPolSql = "select 1 from lcgrppol " +
        		" where grpcontno = '"+mGrpContNo+"' and riskcode='162201'";
        SSRS tGrpPolSSRS = new SSRS();
        tGrpPolSSRS = mExeSQL.execSQL(tGrpPolSql);
        if (tGrpPolSSRS == null || tGrpPolSSRS.getMaxRow() <= 0) {
            buildError("RgtRegister", "该保单的险种不为162201大病险种");
            return false;
        }
        
        String tGrpMessSql = "select linkman1,phone1,GrpAddress from LCGrpAddress " +
			"where customerno = '" + mCustomerNo +
			"' and addressno = '" + tAddressNo + "'";
        SSRS tGrpMessSSRS = new SSRS();
        tGrpMessSSRS = mExeSQL.execSQL(tGrpMessSql);
        if (tGrpMessSSRS != null && tGrpMessSSRS.getMaxRow() > 0) {
        	for(int i=1;i <=tGrpMessSSRS.getMaxRow();i++){
                mRgtantName = tGrpMessSSRS.GetText(i, 1);
                mRgtantPhone = tGrpMessSSRS.GetText(i, 2);
                mRgtantAddress = tGrpMessSSRS.GetText(i, 3);
            }
        	//判断单位地址过长的话要截取
            if(mRgtantAddress.length() >25){
            	mRgtantAddress=mRgtantAddress.substring(0, 25);
            }
        }
        
        //2.生成批次信息
        String result = LLCaseCommon.checkGrp(mGrpContNo);
        if(!"".equals(result)){
            buildError("dealData()","工单号为"+result+"的保单管理现正对该团单进行保全操作，请先通知保全撤销相关操作，再进行理赔!");
            mDealState = false; 
            return false;
        }        
        String tLimit = PubFun.getNoLimit(mManageCom);
        System.out.println("管理机构代码是 : "+mManageCom);
        String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
        
        mRgtNo=RGTNO;
        System.out.println("getInputData start......");
        
        mLLRegisterSchema.setRgtNo(mRgtNo);
        mLLRegisterSchema.setRgtState("01");//检录状态
        mLLRegisterSchema.setRgtObj("0");
        mLLRegisterSchema.setRgtType("8");//默认为8-社保平台
        mLLRegisterSchema.setRgtClass("1");
        mLLRegisterSchema.setRgtObjNo(mGrpContNo);
        mLLRegisterSchema.setAppPeoples(mAppPeoples);
        mLLRegisterSchema.setTogetherFlag(mTogetherFlag);
        mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
        mLLRegisterSchema.setAppAmnt(mAppAmnt);
    	mLLRegisterSchema.setBankCode(mBankCode);
    	mLLRegisterSchema.setAccName(mAccName); 
      	mLLRegisterSchema.setBankAccNo(mBankAccNo);    	
      	System.out.println("对方报文未处理字段:[mBankName]"+mBankName);
      		
      	mLLRegisterSchema.setCustomerNo(mCustomerNo);  
      	mLLRegisterSchema.setGrpName(mGrpName);  	
      	mLLRegisterSchema.setRgtantAddress(mRgtantAddress);	
      	mLLRegisterSchema.setRgtantPhone(mRgtantPhone);	
      	mLLRegisterSchema.setRgtantName(mRgtantName);
      	System.out.println("本次基本信息未处理字段:[mRgtantPostCode]"+mRgtantPostCode+";[mRgtMobile]"+mRgtMobile);
  
        mLLRegisterSchema.setHandler1(mOperator);
        mLLRegisterSchema.setApplyerType("5");
        mLLRegisterSchema.setRgtDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setMngCom(mManageCom);
        mLLRegisterSchema.setOperator(mOperator);
        mLLRegisterSchema.setMakeDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setMakeTime(PubFun.getCurrentTime());
        mLLRegisterSchema.setModifyDate(PubFun.getCurrentDate());
        mLLRegisterSchema.setModifyTime(PubFun.getCurrentTime());
        
        mMMap.put(mLLRegisterSchema, "INSERT");      
        return true;
    }
    
    public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/RgtTest.xml"), "GBK");
            LLTJMajorDiseasesRgtRegister tBusinessDeal = new LLTJMajorDiseasesRgtRegister();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
