package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.Document;
import org.jdom.Element;
import com.sinosoft.lis.db.LBInsuredDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLOnlinClaimCase implements ServiceInterface{
	//错误容器
    public CErrors mCErrors = new CErrors();
    public VData mInData = new VData();
    private GlobalInput mG = new GlobalInput();
	
    public LLOnlinClaimCase (){
    	
    }
	//传入报文
	private Document mInXmlDoc ;
	//处理标志
    private boolean mDealState = true;
    //报文类型
    private String mDocType="";
    //报文请求类型
    private String mRequestType="";
    //报文交互编码
    private String mTransactionNum="";  
	//版本号部分
    private Element mVersionData; 
    //影像件路径部分
    private Element mPictureData;
    //被保人部分
    private Element mCustomerData;  
    //事件部分
    private Element mAccList;


    //管理机构
    private String mMngCom="";
    //理赔用户
    private String mUserCode="";
    //被保人号
    private String mInsuredNo="";
    //给付方式
    private String mCaseGetMode="";
    //案件号
    private String mCaseNo="";
    //银行编码
    private String mBankNo="";
    //银行账户
    private String mAccNo ="";
    //银行账户名
    private String mAccName="";
    //事件关联号
    private String mCaseRelaNo="";
    //申请原因
//    private String mReasonCode="";
    //返回类型代码
    private String mResponseCode = "1";
    //版本号
    private String mVersionCode="";
    //影像件
    private String mPicPath="";
    //错误描述
    private String mErrorMessage = "";
    private String mLimit = "";

    
    String aMakeDate = PubFun.getCurrentDate();
    String aMakeTime = PubFun.getCurrentTime();
    
    private LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema(); //被保人客户表
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //申请信息
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema(); // 案件信息
//    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema(); //申请原因信息表
    private LLAppClaimReasonSet mLLAppClaimReasonSet= new LLAppClaimReasonSet();
    private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema(); //案件信息扩展表
    private LLSubReportSet mLLSubReportSet = new LLSubReportSet();//事件信息表
    private LLCaseRelaSet mLLCaseRelaSet = new LLCaseRelaSet();//事件关联信息表
    private LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
    private LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
    private ExeSQL mExeSQL = new ExeSQL();

	public Document service(Document pInXmlDoc) {
		// 个案统一接口开始执行，第一次，勿扰。
		System.out.println("LLOnlinClaimCase---线上理赔申请----service()");
		
		mInXmlDoc = pInXmlDoc;
		
		try{
			if(!getInputData(mInXmlDoc)){
				mDealState=false;
			}else{
				if(!checkData()){
					mDealState= false;
				}else{
					if(!dealData()){
						mDealState = false;
						cancelCase();
					}
				}
				
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()","系统未知错误");
            cancelCase();
		}finally{
			return createXML();
		}
	}
	
	  /**
     * 生成返回报文处理
     * 
     */
    
    private Document createXML(){
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
        	try {
        		return createResultXML(mCaseNo);
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
         System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BASE_DATA");
         tBodyData.addContent(tBackData);
         
         //Base_Data部分
         Element tCaseno = new Element("CASENO");      
         tCaseno.setText(aCaseNo);
         tBackData.addContent(tCaseno);
         tRootData.addContent(tBodyData);
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mCErrors.getFirstError();
        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

	/**
	 * 理赔报文数据处理
	 * 
	 */
	private boolean dealData() throws Exception{
		System.out.println("LLOnlinClaimCase--线上理赔接口--dealData()");
		
		//报文基本数据校验
		if(!dealBasicData()){
			return false;
		}
		//理赔案件信息
		if(!CaseRegister()){
			return false;
		}
		
		//理赔事件信息
		if(!getSubReport()){
			return false;
		}

		//对外接口表
    	if(!dealHospCase()){
    		return false;
    	}
    	//影像件资料进行存储
    	//
    	if(!dealTYScan()){
    		return false;
    	}

		return true;
	}
	
	 /**
     * 影像件接口解析
     * 
    /**
     * 影像件接口解析
     * 
     */
    private boolean dealTYScan() throws Exception{
    	System.out.println("LLOnlinClaimCase--统一接口--dealTYScan()--数据处理开始");
    	
    	mPicPath =mPictureData.getChildText("PICPATH");
    	
    	ImageHandler tImageHandler = new ImageHandler(mPicPath,mCaseNo,mVersionCode,mMngCom,mUserCode);
    	
    	if(!tImageHandler.dealImage()) {
    		buildError("dealTYScan()", "影像件异常"); 
			mDealState = false; 
    		return false;
		}

    	return true;
    }
  
	 
    /***
     * 对外接口数据
     * 
     */
    private boolean dealHospCase() throws Exception{
    	 System.out.println("LLOnlinClaimCase--线上理赔--dealHospCase()--数据处理开始");
    	 mLLHospCaseSchema.setCaseNo(mCaseNo);
		 mLLHospCaseSchema.setHospitCode(mRequestType);
		 mLLHospCaseSchema.setHandler(mLLCaseSchema.getHandler());
		 mLLHospCaseSchema.setAppTranNo(mTransactionNum);
		 mLLHospCaseSchema.setAppDate(aMakeDate);
		 mLLHospCaseSchema.setAppTime(aMakeTime);
		 mLLHospCaseSchema.setCaseType("18");    //案件类型 
		 mLLHospCaseSchema.setConfirmState("1");
    	 mLLHospCaseSchema.setDealType("2");
    	 mLLHospCaseSchema.setBnfNo(mLLCaseSchema.getCustomerNo());
    	 mLLHospCaseSchema.setHCNo(mVersionCode);
    	 mLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
    	 mLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
    	 
    	 MMap mMMap = new MMap();
    	 mMMap.put(mLLHospCaseSchema, "INSERT");

         //提交数据库
     	//提交数据信息
         mMMap.put(mLLCaseSchema,"INSERT");
         mMMap.put(mLLRegisterSchema, "INSERT");
         if(mLLCaseExtSchema.getCaseNo()!=null && !"".equals(mLLCaseExtSchema.getCaseNo())){
         	mMMap.put(mLLCaseExtSchema, "INSERT");
         }
         mMMap.put(mLLSubReportSet, "INSERT"); 
         mMMap.put(mLLCaseRelaSet, "INSERT");
// 	     if(mLLAppClaimReasonSchema.getCaseNo()!=null && !"".equals(mLLAppClaimReasonSchema.getCaseNo())){
// 	    	mMMap.put(mLLAppClaimReasonSchema, "INSERT");
// 	     }
 	     mMMap.put(mLLCaseOpTimeSchema, "INSERT");
 		  
         PubSubmit tPubSubmit = new PubSubmit();
         try{
  	       this.mInData.clear();       
  	       this.mInData.add(mMMap);
  	       if(!tPubSubmit.submitData(mInData, "")){
  	        	 buildError("dealCase", "数据提交失败");
  	             return false;
  	       }
         } catch (Exception o){
  		   		 System.out.println(o.getMessage());
  		   		 System.out.println("案件环节生成数据信息出错...");
  		         mDealState = false;
  		         mResponseCode = "E";
  		         buildError("service()","系统未知错误"); 
         }
    	
    	return true;
    }
	
	/**
	 * 理赔事件信息
	 * 
	 */
	private boolean getSubReport() throws Exception{
		//开始了多事件的循环，谨慎操刀。。。
		System.out.println("LLOnlinClaimCase--线上理赔接口--getSubReport()");
		if(mAccList.getChildren("ACC_DATA").size()>0){
			for(int i=0;i<mAccList.getChildren("ACC_DATA").size();i++){
				Element aAccData = (Element) mAccList.getChildren("ACC_DATA").get(i);
				//事件信息表
				if(!getAccRegister(aAccData)){
					return false;
				}
   	 
			}
		}else{
			buildError("getSubReport()", "报文中没有理赔信息!");
	        mDealState = false; 
	        return false;
		}

		return true;
	}	
	/**
	 * 解析事件信息
	 * 
	 */
	private boolean getAccRegister(Element oAccData) throws Exception{
		 System.out.println("LLTYAutoCaseRegister--统一接口--getAccRegister()");
		 
		 //深水摸鱼，开始生成事件
		 String aAccDate = oAccData.getChildText("ACCDATE");   //发生日期
		 String aAccDescType = oAccData.getChildText("ACCDESCTYPE");  //事件类型
		 String aAccProvinceCode = oAccData.getChildText("ACCPROVINCECODE"); //省
	     String aAccCityCode = oAccData.getChildText("ACCCITYCODE");  //市
	     String aAccCountyCode = oAccData.getChildText("ACCCOUNTYCODE"); //县
	     String aAccPlace = oAccData.getChildText("ACCPLACE");  //发生地点
	     String aAccDesc = oAccData.getChildText("ACCDESC");    //事件信息
	     String aInhospitalDate = oAccData.getChildText("INHOSPITALDATE");  //入院日期
	     String aOuthospitalDate = oAccData.getChildText("OUTHOSPITALDATE"); //出院日期
	    
	     	
	       if(!PubFun.checkDateForm(aAccDate)){
	    		buildError("getAccRegister","【发生日期】格式错误(正确格式yyyy-MM-dd)");
	    		return false;
	    	}
	     
		   if(!"".equals(aInhospitalDate) && aInhospitalDate!=null){
		        if(!PubFun.checkDateForm(aInhospitalDate)){
		    		buildError("getAccRegister","【住院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }
		    if(!"".equals(aOuthospitalDate) && aOuthospitalDate !=null){
		    	if(!PubFun.checkDateForm(aOuthospitalDate)){
		    		buildError("getAccRegister","【出院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }
	    	
	    	String tAccReason =LLCaseCommon.checkAccPlace(aAccProvinceCode, aAccCityCode, aAccCountyCode);
	    	if(!"".equals(tAccReason)){
	    		buildError("getAccRegister", tAccReason);
	    		return false;
	    	}
		
	    	LLSubReportSchema aLLSubReportSchema = new LLSubReportSchema();
			// 事件信息
	    	String aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", mLimit);
	    	aLLSubReportSchema.setSubRptNo(aSubRptNo);
	    	aLLSubReportSchema.setAccDate(aAccDate);
	    	aLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	    	aLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
	    	aLLSubReportSchema.setInHospitalDate(aInhospitalDate);
	    	aLLSubReportSchema.setOutHospitalDate(aOuthospitalDate);
	    	aLLSubReportSchema.setAccPlace(aAccPlace);
	    	aLLSubReportSchema.setAccProvinceCode(aAccProvinceCode);
	    	aLLSubReportSchema.setAccCityCode(aAccCityCode);
	    	aLLSubReportSchema.setAccCountyCode(aAccCountyCode);
	    	aLLSubReportSchema.setAccDesc(aAccDesc);
	    	aLLSubReportSchema.setAccidentType(aAccDescType);
	        aLLSubReportSchema.setOperator(mUserCode);
	        aLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());
	        aLLSubReportSchema.setMakeDate(aMakeDate);
	        aLLSubReportSchema.setMakeTime(aMakeTime);
	        aLLSubReportSchema.setModifyDate(aMakeDate);
	        aLLSubReportSchema.setModifyTime(aMakeTime);
	        
	        mLLSubReportSet.add(aLLSubReportSchema);
	        
	        System.out.println("客户事件已录入成功，待输入数据库。。。");
	        //事件关联信息
	        LLCaseRelaSchema aLLCaseRelaSchema = new LLCaseRelaSchema();
	        
	        mCaseRelaNo= PubFun1.CreateMaxNo("CASERELANO", mLimit);
	        aLLCaseRelaSchema.setCaseNo(mCaseNo);
	        aLLCaseRelaSchema.setSubRptNo(aSubRptNo);
	        aLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
	        
	    	mLLCaseRelaSet.add(aLLCaseRelaSchema);
		 
		return true;
	}
	
	/**
	 * 生成案件理赔信息
	 * 
	 */
	private boolean CaseRegister() throws Exception{
		System.out.println("LLOnlinClaimCase--线上理赔接口--CaseRegister()");
		
      //银行信息
        mCaseGetMode = mCustomerData.getChildText("CASEGETMODE");
        mBankNo = mCustomerData.getChildText("BANKNO");
    	mAccNo = mCustomerData.getChildText("ACCNO");
    	mAccName = mCustomerData.getChildText("ACCNAME");
    	


		if((!"".equals(mAccNo) && mAccNo!=null) &&(!"".equals(mBankNo) && mBankNo!=null)){
			Pattern tPattern = Pattern.compile("(\\d)+");
			Matcher tNum= tPattern.matcher(mAccNo);
			if(!tNum.matches()){
			buildError("caseRegister","【银行账号】只能是数字组成。");
    		return false;
			}
		}else{
			buildError("caseRegister","银行信息不能为空。");
    		return false;
		}
		LDBankSchema mLDBankSchema = new LDBankSchema();
		LDBankDB mLDBankDB = new LDBankDB();
		mLDBankSchema.setBankCode(mBankNo);
		mLDBankDB.setSchema(mLDBankSchema);
		if(!mLDBankDB.getInfo()){
			
			buildError("caseRegister","银行编码在核心系统不存在！");
			return false;
		}
		
    	

    	
        mLimit = PubFun.getNoLimit(mMngCom);
        mCaseNo = PubFun1.CreateMaxNo("CaseNo", mLimit);
        
        //申请信息
        if(!getRgtInfo()){
        	return  false;
        }
        
        if(!getCaseInfo()){
        	return false;
        }
        //受理申请原因
        
//        if(!getReasonInfo()){
//        	return false;
//        }
        
		return true;
	}
	
	/**
	 * 理赔受理申请原因
	 * 
	 */
//	private boolean getReasonInfo() throws Exception{
//		System.out.println("LLOnlinClaimCase--接口--getReasonInfo()");
//		
//		 //申请原因
//	       
//	       if(!"".equals(mReasonCode) && mReasonCode!=null){
//	    	   mLLAppClaimReasonSchema.setCaseNo(mCaseNo);
//	    	   mLLAppClaimReasonSchema.setRgtNo(mCaseNo);
//	    	   mLLAppClaimReasonSchema.setReasonCode(mReasonCode);
//	    	   mLLAppClaimReasonSchema.setReasonType("0"); // 原因类型
//	    	   mLLAppClaimReasonSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
//	    	   mLLAppClaimReasonSchema.setMngCom(mLLCaseSchema.getMngCom());
//	    	   mLLAppClaimReasonSchema.setOperator(mLLCaseSchema.getOperator());
//	    	   mLLAppClaimReasonSchema.setMakeDate(aMakeDate);
//	    	   mLLAppClaimReasonSchema.setMakeTime(aMakeTime);
//	    	   mLLAppClaimReasonSchema.setModifyDate(aMakeDate);
//	    	   mLLAppClaimReasonSchema.setModifyTime(aMakeTime);
//	       }
//	       mLLAppClaimReasonSet.add(mLLAppClaimReasonSchema);
//		
//		return true;
//	}
	
	/**
	 * 理赔案件信息
	 * 
	 */
	private boolean getCaseInfo() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getCaseInfo()");
		
		mLLCaseSchema.setCaseNo(mCaseNo);
		mLLCaseSchema.setRgtNo(mCaseNo);
        //申请类案件
        mLLCaseSchema.setRgtType("1");
        //案件状态处理到理算确认
        mLLCaseSchema.setRgtState("13");
        mLLCaseSchema.setCustomerNo(mInsuredNo);
        mLLCaseSchema.setCustomerName(tLCInsuredSchema.getName());
        //账单状态录入
        mLLCaseSchema.setReceiptFlag("0");
        //未调查
        mLLCaseSchema.setSurveyFlag("0");
        mLLCaseSchema.setRgtDate(aMakeDate);
        mLLCaseSchema.setCustBirthday(tLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerSex(tLCInsuredSchema.getSex());

        int tAge = LLCaseCommon.calAge(tLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerAge(tAge);
        
        mLLCaseSchema.setIDType(tLCInsuredSchema.getIDType());
        mLLCaseSchema.setIDNo(tLCInsuredSchema.getIDNo());
        mLLCaseSchema.setHandler(mUserCode);
        //银行信息
        mLLCaseSchema.setCaseGetMode(mCaseGetMode);
        mLLCaseSchema.setBankCode(mBankNo);
        mLLCaseSchema.setBankAccNo(mAccNo);
        mLLCaseSchema.setAccName(mAccName);        
        mLLCaseSchema.setMngCom(mMngCom);
        String tMobilePhone = mCustomerData.getChildText("MOBILEPHONE");
        mLLCaseSchema.setMobilePhone(tMobilePhone);
        mLLCaseSchema.setOperator(mUserCode);
        mLLCaseSchema.setMakeDate(aMakeDate);
        mLLCaseSchema.setMakeTime(aMakeTime);
        mLLCaseSchema.setModifyDate(aMakeDate);
        mLLCaseSchema.setModifyTime(aMakeTime);
        mLLCaseSchema.setCaseProp("09");
        mLLCaseSchema.setRigister(mUserCode);
        mLLCaseSchema.setClaimer(mUserCode);
        mLLCaseSchema.setOtherIDType(tLCInsuredSchema.getOthIDType());
        mLLCaseSchema.setOtherIDNo(tLCInsuredSchema.getOthIDNo());
//        //申请原因：死亡，死亡日期必录
//        if("05".equals(mReasonCode)){
//        	String DeathDate = mCustomerData.getChildText("DEATHDATE");
//        	if(DeathDate==null||"".equals(DeathDate)){
//        		buildError("getCaseInfo", "申请原因为身故时，死亡日期必录！");
//        		return false;
//        	}
//        	mLLCaseSchema.setDeathDate(DeathDate);
//        }
        

        mLLCaseOpTimeSchema.setCaseNo(mCaseNo);
        mLLCaseOpTimeSchema.setRgtState("13");
        mLLCaseOpTimeSchema.setSequance("1");
        mLLCaseOpTimeSchema.setManageCom(mMngCom);
        mLLCaseOpTimeSchema.setOpTime("0:0:0");
        mLLCaseOpTimeSchema.setOperator(mUserCode);
        mLLCaseOpTimeSchema.setStartDate(aMakeDate);
        mLLCaseOpTimeSchema.setStartTime(aMakeTime);
		
		return true;
		
	}
	
	/**
	 * 理赔受理申请信息
	 * 
	 */
	private boolean getRgtInfo() throws Exception{
		System.out.println("LLOnlinClaimCase--线上理赔接口--getRgtInfo()");
		
		//解析申请人信息
		String tRgtantName = mCustomerData.getChildText("RGTANTNAME"); //申请人姓名  
		String tRgtantIdNo = mCustomerData.getChildText("RGTANTIDNO"); //申请人证件号
		String tRgtantIdType = mCustomerData.getChildText("RGTANTIDTYPE"); //申请人证件类型
		String tRgtantPhone = mCustomerData.getChildText("TELPHONE"); //申请人手机号
        String tRelaTion = mCustomerData.getChildText("RELATION"); //申请人与被保人关系
        String tRgtType = mCustomerData.getChildText("RGTTYPE"); //受理类型
        
        if("".equals(tRgtType) || "null".equals(tRgtType) || tRgtType==null){
    		buildError("getRgtInfo", "案件受理类型不能为空");
    		return false;
    	}
        if(!"13".equals(tRgtType) && !"15".equals(tRgtType)) {
        	buildError("getRgtInfo", "案件受理类型有误");
    		return false;
        }
		
		mLLRegisterSchema.setRgtNo(mCaseNo);
        mLLRegisterSchema.setRgtState("13");
        //客户号
        mLLRegisterSchema.setRgtObj("1");
        mLLRegisterSchema.setRgtObjNo(mInsuredNo);
        //受理方式
        mLLRegisterSchema.setRgtType(tRgtType);
        //个案受理
        mLLRegisterSchema.setRgtClass("0");
        mLLRegisterSchema.setRgtantName(tRgtantName);
        mLLRegisterSchema.setRelation(tRelaTion);
        //银行信息
    	mLLRegisterSchema.setBankCode(mBankNo);
    	mLLRegisterSchema.setAccName(mAccName); 
      	mLLRegisterSchema.setBankAccNo(mAccNo); 
      	mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
      	//申请日期
        mLLRegisterSchema.setAppDate(aMakeDate);
        mLLRegisterSchema.setCustomerNo(mInsuredNo);
        mLLRegisterSchema.setRgtDate(aMakeDate);
        mLLRegisterSchema.setHandler(mUserCode);
        mLLRegisterSchema.setMngCom(mMngCom);
        mLLRegisterSchema.setIDType(tRgtantIdType);
        mLLRegisterSchema.setIDNo(tRgtantIdNo);
        mLLRegisterSchema.setRgtantMobile(tRgtantPhone);
        mLLRegisterSchema.setOperator(mUserCode);
        mLLRegisterSchema.setMakeDate(aMakeDate);
        mLLRegisterSchema.setMakeTime(aMakeTime);
        mLLRegisterSchema.setModifyDate(aMakeDate);
        mLLRegisterSchema.setModifyTime(aMakeTime);
		
		return true;
	}
	
	
	/**
	 * 校验案件基本信息
	 * 
	 */
	private boolean dealBasicData() throws Exception{
		System.out.println("LLOnlinClaimCase--线上理赔接口--dealBasicData()");
		
		
    	
    	
    	//版本号
    	mVersionCode = mVersionData.getChildText("VERSIONCODE");
    	//申请原因
//    	mReasonCode = mCustomerData.getChildText("REASONCODE");
    	//被保人校验
    	String aInsuredName = mCustomerData.getChildText("INSUREDNAME");
    	String aIdNo = mCustomerData.getChildText("IDNO");
    	mInsuredNo = mCustomerData.getChildText("INSUREDNO");
    	String sql = "select * from lcinsured where insuredno='"+mInsuredNo+"' and name='"+aInsuredName+"' and idno='"+aIdNo+"' with ur";
    	if(!"".equals(mInsuredNo)&& mInsuredNo!=null){
    		Reflections tR=new Reflections();
    		LCInsuredDB tLCInsuredBD = new LCInsuredDB();
    		LCInsuredSet tLCInsuredSet = new LCInsuredSet();
    		tLCInsuredSet = tLCInsuredBD.executeQuery(sql);
    		if(tLCInsuredSet.size()==0){
    			String LBsql = "select * from lbinsured where insuredno='"+mInsuredNo+"' and name='"+aInsuredName+"' and idno='"+aIdNo+"' with ur";
        		LBInsuredDB tLBInsuredBD = new LBInsuredDB();
        		LBInsuredSchema tLBInsuredSchema = new LBInsuredSchema();
        		LBInsuredSet tLBInsuredSet = new LBInsuredSet();
        		tLBInsuredSet = tLBInsuredBD.executeQuery(LBsql);
        		tLBInsuredSchema = tLBInsuredSet.get(1);
        		if(tLBInsuredSchema==null){
        			buildError("dealBasicData", "被保险人查询失败");
    	    		return false;
        		}else{
        			tR.transFields(tLCInsuredSchema,tLBInsuredSchema);
        		}
    		}else{
    			tLCInsuredSchema =tLCInsuredSet.get(1);
    		}

    		

    	}   	
    	//校验是否有未完成理赔案件
    	String tCaseSql ="select * from llcase where customerno='"+mInsuredNo+"' and "
    			+ "rgtstate not in('11','12','14') with ur ";
    	SSRS tSCase =mExeSQL.execSQL(tCaseSql);
    	if(tSCase.getMaxRow()>0){
    		buildError("dealBasicData", "客户" + mInsuredNo + "有未处理完的理赔案件，不能进行理赔。");
            return false;
    	}
    	if(!"".equals(LLCaseCommon.checkPerson(mInsuredNo))){
    		buildError("dealBasicData", "该客户正在进行保全处理，不能进行理赔");
            return false;
    	}
    	
    	//获取发生日期下的保单
    	if(!getMngCom()){
    		buildError("getMngCom", "出险日期内无有效保单");
            return false;
    	}
    	
    	
    	//查询用户理赔用户
    	String claimuserSQL = "";
    	if("WXCLAIM".equals(mVersionCode)) {
    		claimuserSQL = "select code from ldcode where codetype='OCclaimuser' and comcode='"+mMngCom+"' with ur";
    	}else if("APPWXCLAIM".equals(mVersionCode)) {
    		claimuserSQL = "select code from ldcode where codetype='APPclaimuser' and comcode='"+mMngCom+"' with ur";
    	}else {
    		buildError("dealBasicData", "版本信息有误");
			return false;
    	}
    	mUserCode = mExeSQL.getOneValue(claimuserSQL);
		if(mMngCom!=null&&mMngCom!=""&&mUserCode!=null&&mUserCode!=""){
			mG.Operator = mUserCode;
			mG.ManageCom = mMngCom;
		}else{
			buildError("dealBasicData", "未配置理赔人");
			return false;
		}
		
		//校验理赔用户
		LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(mUserCode);
    	tLLClaimUserDB.setClaimDeal("1");  //理赔权限
//    	tLLClaimUserDB.setHandleFlag("1"); //案件分配权限
    	tLLClaimUserDB.setStateFlag("1"); //理赔员状态
    	
    	LLClaimUserSet tLLClaimUserSet =tLLClaimUserDB.query();
    	if(tLLClaimUserSet.size()<=0){
    		buildError("dealBasicData", "案件处理人查询失败或没有权限");
    		return false;
    	}
    	
		return true;
	}
	
	
	/**
	 * 校验报文数据
	 * 
	 */
	private boolean checkData() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--checkData()");
		
		if(!"REQUEST".equals(mDocType)){
			buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
		}
		if(!"OC02".equals(mRequestType)){
			 buildError("checkData()", "【请求类型】的值不存在或匹配错误");
             return false;
		}
    	if(mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
        buildError("checkData()", "【交互编码】的编码个数错误");
        return false;
    	}
    	if(!"OC02".equals(mTransactionNum.substring(0, 4))){
    		buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
    	}
    	
		return true;
	}
	
	
	
	/**
	 * 解析报文
	 */
	private boolean getInputData(Document aInXml) throws Exception{
		System.out.println("LLOnlinClaimCase--线上理赔接口--getInputData()");
		
		if(aInXml==null){
			buildError("getInputData()","未获取到报文信息");
			return false;
		}
		
		Element tRootData = aInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");
		//解析HEAD部分
		Element tHeadData = tRootData.getChild("HEAD");
		mRequestType = tHeadData.getChildText("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildText("TRANSACTION_NUM");
		
		//解析BODY部分
		Element tBodyData = tRootData.getChild("BODY");
		mVersionData = tBodyData.getChild("VERSION_DATA");  //版本号
		mPictureData = tBodyData.getChild("PICTURE_DATA");  //影像件路径
		mCustomerData = tBodyData.getChild("CUSTOMER_DATA"); //被保人信息
		mAccList = mCustomerData.getChild("ACC_LIST");        //事件信息
		
		//理赔机构、用户需要根据事件发生日期内，生效日期最早的保单机构确定
		System.out.println("LLOnlinClaimCase--报文解析完毕。。");
		return true;
	}
	
	/***
	 * 错误信息
	 * @param args
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError tCError = new CError();
		tCError.moduleName="LLOnlinClaimCase";
		tCError.functionName=szFunc;
		tCError.errorMessage=szErrMsg;
		System.out.println(szFunc+"--"+szErrMsg);
		this.mCErrors.addOneError(tCError);
	}

    private boolean getMngCom(){
    	if(mAccList.getChildren("ACC_DATA").size()>0){
			for(int i=0;i<mAccList.getChildren("ACC_DATA").size();i++){
				
				Element aAccData = (Element) mAccList.getChildren("ACC_DATA").get(i);
				String accdate1 = aAccData.getChildText("ACCDATE");
				String SQL = "select managecom from lccont where insuredno='"+mInsuredNo+"' and '"+accdate1+"'>=cvalidate and '"+ accdate1 +"'<=cinvalidate and appflag='1' order by cvalidate fetch first 1 rows only with ur";
		    	mMngCom = mExeSQL.getOneValue(SQL);
		    	//如果C表没有数据，那么查询B表
		    	if(mMngCom==null||"".equals(mMngCom)){
		    		String LBSQL = "select managecom from lbcont where insuredno='"+mInsuredNo+"' and '"+accdate1+"'>=cvalidate and '"+ accdate1 +"'<=cinvalidate and stateflag!='4' order by cvalidate fetch first 1 rows only with ur";
		    		mMngCom= mExeSQL.getOneValue(LBSQL);
		    		if(mMngCom==null||"".equals(mMngCom)){
		    			continue;
		    		}else{
		    			break;
		    		}
		    	}else{
		    		break;
		    	}
			}
			if(mMngCom==null||mMngCom==""){
				return false;
			}
			System.out.println("机构处理");
			if(mMngCom.length()>=4) {
				System.out.println("机构截取前：" + mMngCom);
				mMngCom = mMngCom.substring(0, 4);
				System.out.println("机构截取后：" + mMngCom);
			}
    	}
    	return true;
    }
	  /**
     * 删除数据处理
     * 
     */
    private boolean cancelCase() throws Exception{
    	
        //提交数据库
    	//提交数据信息
    	 MMap mMMap = new MMap();
//    	mMMap.put(mLLHospCaseSchema, "DELETE");

        mMMap.put(mLLCaseSchema,"DELETE");
        mMMap.put(mLLRegisterSchema, "DELETE");
        if(mLLCaseExtSchema.getCaseNo()!=null && !"".equals(mLLCaseExtSchema.getCaseNo())){
        	mMMap.put(mLLCaseExtSchema, "DELETE");
        }
        mMMap.put(mLLSubReportSet, "DELETE"); 
        mMMap.put(mLLCaseRelaSet, "DELETE");
//	     if(mLLAppClaimReasonSchema.getCaseNo()!=null && !"".equals(mLLAppClaimReasonSchema.getCaseNo())){
//	    	mMMap.put(mLLAppClaimReasonSchema, "DELETE");
//	     }
	     mMMap.put(mLLCaseOpTimeSchema, "DELETE");
         PubSubmit tPubSubmit = new PubSubmit();
         try{
  	       this.mInData.clear();       
  	       this.mInData.add(mMMap);
  	       if(!tPubSubmit.submitData(mInData, "")){
  	        	 buildError("dealCase", "数据提交失败");
  	             return false;
  	       }
         } catch (Exception o){
  		   		 System.out.println(o.getMessage());
  		   		 System.out.println("撤销数据提交失败");
  		         mDealState = false;
  		         mResponseCode = "E";
  		         buildError("service()","系统未知错误"); 
         }
    	return false;

    }
    
	
	public static void main(String[] args) {
	     Document tInXmlDoc;    
	        try {
	            tInXmlDoc = JdomUtil.build(new FileInputStream("D:\\Java\\test\\weixin\\OC02\\OC02-1.xml"), "GBK");
	            LLOnlinClaimCase tBusinessDeal = new LLOnlinClaimCase();
	            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
	            System.out.println("打印传入报文============");
	            JdomUtil.print(tInXmlDoc.getRootElement());
	            System.out.println("打印传出报文============");
	            JdomUtil.print(tOutXmlDoc);
	        	        	
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	}

}
