package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LLCASEPROBLEMDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLCASEPROBLEMSchema;
import com.sinosoft.lis.vschema.LLCASEPROBLEMSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class LLOnlinClaimGetImage implements ServiceInterface{

	public LLOnlinClaimGetImage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CErrors mErrors = new CErrors();
	public VData mResult = new VData();
	private MMap map = new MMap();
	private String mErrorMessage = "";	//错误描述 
	private PubFun pf =new PubFun();
	private VData mInputData = new VData();
	private LLCASEPROBLEMSet mLLCASEPROBLEMSet = new LLCASEPROBLEMSet();
	
	private Document mInXmlDoc;	 			//传入报文    
    private String mDocType = "";			//报文类型    
    private boolean mDealState = true;		//处理标志    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码 
	
    private String mManageCom = "";				//机构编码   
    private String mOperator  = " ";				//操作员    
    
    private Element mVersionData;
    private Element mCaseData;
    private Element mPictureData;
    private String mVersionCode = "";
    private String mCaseNo = "";
    private String mRemark = "";
    private String mPicPath = "";
    
    private FTPTool tFTPTool;
    //状态   1--已下发      2--已回复
  	private String imageState = "1";
    
	public Document service(Document pInXmlDoc) {
		//程序入口
		System.out.println("LLOnlinClaimGetImage-----------service");
		mInXmlDoc = pInXmlDoc;
		try {
			if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
            	if (!checkData()) {
                    mDealState = false;
                }else {
                	 if (!dealData()) {
                         mDealState = false;
                     }else {
                    	 if(mLLCASEPROBLEMSet.size()>0) {
              				map.put(mLLCASEPROBLEMSet, "UPDATE");
              		    	this.mInputData.clear();
              		        mInputData.add(map);
              		    	PubSubmit tPubSubmit = new PubSubmit();
              		        if (!tPubSubmit.submitData(mInputData, null)) {
              		        	buildError("service()", "数据更新失败");
              		        	mDealState = false;
              		        }
                     }
                }
            }
           }
		}catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            buildError("service()", "系统未知错误");
        } finally {
        	mInXmlDoc = createXML();
        }
		return mInXmlDoc;
	}
	
	/**
	 * 处理数据
	 */
	private boolean dealData() {
		
		System.out.println("LLOnlinClaimGetImage-----------dealData");
		mVersionCode = mVersionData.getChildText("VERSIONCODE");
		mCaseNo = mCaseData.getChildText("CASENO"); 
		mRemark = mCaseData.getChildText("REMARK");
		mPicPath = mPictureData.getChildText("PICPATH");
		if(mVersionCode==null || "".equals(mVersionCode) || "null".equals(mVersionCode) || mVersionCode=="") {
			buildError("dealData", "【版本信息】的值不能为空");
            return false;
		}
		if(mCaseNo==null || "".equals(mCaseNo) || "null".equals(mCaseNo) || mCaseNo=="") {
			buildError("dealData", "【理赔案件号】的值不能为空");
            return false;
		}
		if(mPicPath==null && "".equals(mPicPath) && "null".equals(mPicPath) && mPicPath==""
				&& mRemark==null && "".equals(mRemark) && "null".equals(mRemark) && mRemark=="") {
			buildError("dealData", "影像件地址或备注信息二者不能同时为空");
            return false;
		}
		String mCaseSQL = "select mngcom,operator from llcase where 1=1 and caseno = '" 
						  + mCaseNo + "' with ur";
		ExeSQL CaseExeSQL = new ExeSQL();
		SSRS CaseSSRS = CaseExeSQL.execSQL(mCaseSQL);
		int size = CaseSSRS.getMaxRow();
		if(size<1) {
			buildError("dealData()", "该"+mCaseNo+"案件有问题");
            return false;
		}
		mManageCom = CaseSSRS.GetText(1, 1);
		mOperator = CaseSSRS.GetText(1, 2);
    	
		if(mPicPath!=null && !"".equals(mPicPath) && !"null".equals(mPicPath) && mPicPath!="") {			
			//影像件数据处理
			ImageHandler tImageHandler = new ImageHandler(mPicPath, mCaseNo, mVersionCode,mManageCom, mOperator);
			if(!tImageHandler.dealImage()) {
				buildError("dealTYScan()", "影像件处理有问题"); 
				return false;
			}else {
				imageState = "2";
			}
		}

		if(mRemark!=null && !"".equals(mRemark) && !"null".equals(mRemark) && mRemark!="") {			
			imageState = "2";
		}
		
		LLCASEPROBLEMSchema tLLCASEPROBLEMSchema = new LLCASEPROBLEMSchema();
		LLCASEPROBLEMSet tmLLCASEPROBLEMSet = new LLCASEPROBLEMSet();
		LLCASEPROBLEMDB tLLCASEPROBLEDB = new LLCASEPROBLEMDB();
		String CaseSql = "select * from LLCASEPROBLEM where 1=1 and caseno = '" + mCaseNo + "' and state='1' with ur ";
		tmLLCASEPROBLEMSet = tLLCASEPROBLEDB.executeQuery(CaseSql);
		tLLCASEPROBLEMSchema=tmLLCASEPROBLEMSet.get(1);
		if(tLLCASEPROBLEMSchema == null) {
			buildError("dealTYScan()", "影像件数据有问题"); 
			return false;
		}
		tLLCASEPROBLEMSchema.setREPLY(mRemark);
		tLLCASEPROBLEMSchema.setSTATE(imageState);
		tLLCASEPROBLEMSchema.setMODIFYDATE(pf.getCurrentDate());
		tLLCASEPROBLEMSchema.setMODIFYTIME(pf.getCurrentTime());
		
		mLLCASEPROBLEMSet.add(tLLCASEPROBLEMSchema);
		
		return true;
	}
	
	/**
	 * 校验报文信息
	 * @return
	 */
	private boolean checkData() {
		
		System.out.println("LLOnlinClaimGetImage-----------checkData");
		
		if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }
		if (!"OC03".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }
		if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
	            buildError("checkData()", "【交互编码】的编码个数错误");
	            return false;
	        }
		if (!"OC03".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }
		String str = mTransactionNum.substring(4, 12);        
        if ("".equals(str) || str == null || str.equals("null")|| !"00000000".equals(str)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
		return true;
	}
	
	/**
	 * 解析报文主要部分
	 * @param cInXml
	 * @return
	 */
	 private boolean getInputData(Document cInXml) {
		 
		 System.out.println("LLOnlinClaimGetImage-----------getInputData");
		 if (cInXml == null) {
	            buildError("getInputData()", "未获得报文");
	            return false;
	        }
		 Element tRootData = cInXml.getRootElement();
		 mDocType = tRootData.getAttributeValue("type");
		 
		 //获取HEAD部分内容
		 Element tHeadData = tRootData.getChild("HEAD");
	     mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");//报文类型-主要信息
	     mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");//会进行唯一性校验
	     System.out.println("LLOnlinClaimGetImage--TransactionNum:" + mTransactionNum);
	     
	     //获取BODY部分内容
	     Element tBodyData = tRootData.getChild("BODY");
	     mVersionData = tBodyData.getChild("VERSION_DATA");
	     mCaseData = tBodyData.getChild("CASE_DATA");
	     mPictureData = tBodyData.getChild("PICTURE_DATA");
	     
		 return true;
	}

	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
    	
    	System.out.println("LLOnlinClaimGetImage-----------createXML");
    	if(!mDealState) {
    		return createFalseXML();
    	}else {
    		try {
    			return createResultXML();
			} catch (Exception e) {
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
			}
    	}
    }
    /**
     * 返回错误的报文
     */
    private Document createFalseXML() {
    	
    	System.out.println("LLOnlinClaimGetImage-----------createFalseXML");
    	Element tRootData = new Element("PACKET");
    	tRootData.addAttribute("type", "RESPONSE");
    	tRootData.addAttribute("version", "1.0");
    	//创建头部
    	Element tHeadData = new Element("HEAD");
    	Element tRequestType = new Element("REQUEST_TYPE");
    	tRequestType.setText(mRequestType);
    	tHeadData.addContent(tRequestType);
    	
    	
    	Element tResponseCode = new Element("RESPONSE_CODE");
    	tResponseCode.setText("1");
    	tHeadData.addContent(tResponseCode);
    	
    	Element tErrorMessage = new Element("ERROR_MESSAGE");
    	mErrorMessage = mErrors.getFirstError();
    	tErrorMessage.setText(mErrorMessage);
    	tHeadData.addContent(tErrorMessage);
    	
    	Element tTransactionNum = new Element("TRANSACTION_NUM");
    	tTransactionNum.setText(mTransactionNum);
    	tHeadData.addContent(tTransactionNum);
    	
    	tRootData.addContent(tHeadData);
    	
    	Document tDocument = new Document(tRootData);
    	return tDocument;
    }
    /**
     * 返回正确的报文
     */
    private Document createResultXML() {
    	
    	System.out.println("LLOnlinClaimGetImage-----------createResultXML");
    	
    	Element tRootData = new Element("PACKET");
    	tRootData.addAttribute("type", "RESPONSE");
    	tRootData.addAttribute("version", "1.0");
    	//创建头部
    	Element tHeadData = new Element("HEAD");
    	
    	Element tRequestType = new Element("REQUEST_TYPE");
    	tRequestType.setText(mRequestType);
    	tHeadData.addContent(tRequestType);
    	
    	Element tTransactionNum = new Element("TRANSACTION_NUM");
    	tTransactionNum.setText(mTransactionNum);
    	tHeadData.addContent(tTransactionNum);
    	
    	tRootData.addContent(tHeadData);
    	
    	//创建body
    	Element tBodyData = new Element("BODY");
    	
    	Element tBaseData = new Element("BASE_DATA");
    	
    	Element tCaseNo = new Element("CASENO");
    	tCaseNo.setText(mCaseNo);
    	tBaseData.addContent(tCaseNo);
    	
    	Element tSuccess = new Element("SUCCESS");
    	tSuccess.setText("1");
    	tBaseData.addContent(tSuccess);
    	
    	tBodyData.addContent(tBaseData);
    	
    	tRootData.addContent(tBodyData);
    	
    	Document tDocument = new Document(tRootData);
    	return tDocument;
    }
	 /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLWBRgtRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    public static void main(String[] args) {
    	 Document tInXmlDoc;    
         try {
             tInXmlDoc = JdomUtil.build(new FileInputStream("D:/Java/test/TYRequest/ceshi.xml"), "GBK");
             LLOnlinClaimGetImage tLLOnlinClaimGetImage = new LLOnlinClaimGetImage();
             Document tOutXmlDoc = tLLOnlinClaimGetImage.service(tInXmlDoc);
             System.out.println("打印传入报文============");
             JdomUtil.print(tInXmlDoc.getRootElement());
             System.out.println("打印传出报文============");
             JdomUtil.print(tOutXmlDoc);
         	        	
         } catch (Exception e) {
             e.printStackTrace();
         }
	}
    
}
