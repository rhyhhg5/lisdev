package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLTJSocialImageSelect implements ServiceInterface{

	public LLTJSocialImageSelect() {
		super();
	}
	
	public CErrors mErrors = new CErrors();
	public VData mResult = new VData();
	private MMap map = new MMap();
	private String mErrorMessage = "";	//错误描述 
	private PubFun pf =new PubFun();
	private VData mInputData = new VData();
	
	private Document mInXmlDoc;	 			//传入报文  
	private Document mOutXmlDoc;			//输出报文
    private String mDocType = "";			//报文类型    
    private String mDocVersion = "";		//报文版本
    private boolean mDealState = true;		//处理标志    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码 
    private String mResponseCode = "1";		//返回类型代码 
    
    private Element mLLCaseData;
    
    private String mCaseno = "";		//案件号
    private String mPicPath = "";		//影像件路径
    
    private SSRS ImageSSRS = new SSRS();
    private ExeSQL ImageExeSQL = new ExeSQL();

	public Document service(Document pInXmlDoc) {
		//程序入口
		System.out.println("LLSocialDealImageSup-----------service()------");
		mInXmlDoc = pInXmlDoc;
		try {
			if(!getInputData(mInXmlDoc)) {
				mDealState = false;
			}else {
				if(!checkData()) {
					mDealState = false;
				}else {
					if(!dealData()) {
						mDealState = false;
					}
				}
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
		}finally {
			mOutXmlDoc = createXML();
		}
		return mOutXmlDoc;
	}
	
	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
    	
    	System.out.println("LLSocialDealImageSup-----------createXML");
    	
    	if(!mDealState) {
    		return createFalseXML();
    	}else {
    		try {
    			return createResultXML();
    		}catch(Exception e) {
    			buildError("createXML()", "生成返回报文错误");
    			return createFalseXML();
    		}
    	}
    	
    }
    
    /**
     * 返回错误的报文
     */
    private Document createFalseXML() {
    	System.out.println("LLSocialDealImageSup-----------createFalseXML");
    	
    	Element tRootData = new Element("PACKET");
    	tRootData.addAttribute("type", "RESPONSE");
    	tRootData.addAttribute("version", "1.0");
    	//创建头部
    	Element tHeadData = new Element("HEAD");
    	
    	Element tReuestType = new Element("REQUEST_TYPE");
    	tReuestType.setText(mRequestType);
    	tHeadData.addContent(tReuestType);  
    	
    	Element tResponseCode = new Element("RESPONSE_CODE"); 	
    	if(!"E".equals(mResponseCode)) {
    		mResponseCode = "0";
    	}
    	tResponseCode.setText(mResponseCode);
    	tHeadData.addContent(tResponseCode);  
    	
    	Element tErrorMessage = new Element("ERROR_MESSAGE");
    	tErrorMessage.setText(mErrors.getFirstError());
    	tHeadData.addContent(tErrorMessage);  
    	
    	Element tTransActionNum = new Element("TRANSACTION_NUM");
    	tTransActionNum.setText(mTransactionNum);
    	tHeadData.addContent(tTransActionNum); 
    	
    	tRootData.addContent(tHeadData);	
    	
    	Document tDocument = new Document(tRootData);
    	return tDocument;
    }
    
    /**
     * 返回正确的报文
     */
    private Document createResultXML() {
    	System.out.println("LLSocialDealImageSup-----------createResultXML");
    	
    	Element tRootData = new Element("PACKET");
    	tRootData.addAttribute("type", "RESPONSE");
    	tRootData.addAttribute("version", "1.0");
    	//创建头部
    	Element tHeadData = new Element("HEAD");
    	Element tRequestType = new Element("REQUEST_TYPE");
    	tRequestType.setText(mRequestType);
    	tHeadData.addContent(tRequestType);
    	
    	Element tTransactionNum = new Element("TRANSACTION_NUM");
    	tTransactionNum.setText(mTransactionNum);
    	tHeadData.addContent(tTransactionNum);
    	
    	tRootData.addContent(tHeadData);
    	
    	Element tBodyData = new Element("BODY");
    	Element tLLCaseData = new Element("LLCASE_DATA");
    	Element tCaseNo = new Element("CASENO");
    	tCaseNo.setText(mCaseno);
    	tLLCaseData.addContent(tCaseNo);
    	Element tPicPathList = new Element("PICPATH_LIST");
    	String IPSQL = "select codename from ldcode where  code='ServerIp' and codetype='WBClaim'";
    	SSRS IPSSRS = ImageExeSQL.execSQL(IPSQL);
    	String IP = "";
    	if(IPSSRS.getMaxRow()>0) {    		
    		String IPName = IPSSRS.GetText(1, 1);
    		IP = IPName.substring(0, IPName.indexOf("/"));
    	}
    	int length = ImageSSRS.getMaxRow();
    	if(length > 0) {
    		for (int i = 1; i <= length; i++) {
				Element tPicPathData = new Element("PICPATH_DATA");
				String ImageStr = "http://" + IP + "/" + ImageSSRS.GetText(i, 1) + ImageSSRS.GetText(i, 2) + ImageSSRS.GetText(i, 3);
				tPicPathData.setText(ImageStr);
				tPicPathList.addContent(tPicPathData);
			}
    	}
    	tLLCaseData.addContent(tPicPathList);
    	tBodyData.addContent(tLLCaseData);
    	tRootData.addContent(tBodyData);
    		
    	Document tDocument = new Document(tRootData);
    	return tDocument;
    }
	
	/**
	 * 处理数据
	 * @return
	 */
	private boolean dealData() {
		
		System.out.println("LLSocialDealImageSup-----------dealData()------");
		mCaseno = mLLCaseData.getChildTextTrim("CASENO");
		if("".equals(mCaseno) || "null".equals(mCaseno) || mCaseno==null) {
			buildError("dealData()", "案件号不能为空");
			return false;
		}
		String SNo = LLCaseCommon.checkSocialSecurity(mCaseno, "null");
		if(!"1".equals(SNo)) {
			buildError("dealData()", "该"+mCaseno+"案件不是社保案件");
            //return false;
		}
		String ImageSQL = "select  picpath,pagename,pagesuffix  from ES_DOC_PAGES a,ES_DOC_MAIN b where 1=1 "
    			+ "and a.docid = b.docid and b.doccode = '" + mCaseno + "' order by a.makedate with ur";
    	ImageSSRS = ImageExeSQL.execSQL(ImageSQL);
    	if(ImageSSRS.getMaxRow() < 1) {
    		buildError("dealData()", "未查找到影像件");
            return false;
    	}
    	int length = ImageSSRS.getMaxRow();
        if(length > 0) {
        	for (int i = 1; i <= length; i++) {
        		String ImageFormat = ImageSSRS.GetText(i, 3);
        		if(".jpg".equals(ImageFormat) && ".gif".equals(ImageFormat) && ".tif".equals(ImageFormat)) {
        			buildError("dealData()", "影像件的格式有问题");
        			return false;
        		}
        	}
        }
    	
    	
		return true;
	}
	
	/**
	 * 校验报文信息
	 * @return
	 */
	private boolean checkData() {
		
		System.out.println("LLSocialDealImageSup-----------checkData()------");
		if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }
		if (!"TJ07".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }
		if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
	            buildError("checkData()", "【交互编码】的编码个数错误");
	            return false;
	        }
		if (!"TJ07".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }
		String str = mTransactionNum.substring(4, 12);        
        if ("".equals(str) || str == null || str.equals("null")|| !"86120000".equals(str)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
		return true;
	}
	
	/**
	 * 解析报文主要部分
	 * @param cInXml
	 * @return
	 */
	private boolean getInputData(Document cInXml) {
		System.out.println("LLSocialDealImageSup-----------getInputData()------");
		if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }
		Element tRootData = cInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");
		mDocVersion = tRootData.getAttributeValue("version");
		//获取head部分
		Element tHeadData = tRootData.getChild("HEAD");
		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
		//获取body部分
		Element tBodyData = tRootData.getChild("BODY");
		mLLCaseData = tBodyData.getChild("LLCASE_DATA");
		
		return true;
	}
    
	 /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLWBRgtRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args) {
    	 Document tInXmlDoc;    
         try {
             tInXmlDoc = JdomUtil.build(new FileInputStream("D:/Java/test/TJ06/TJ007.xml"), "utf-8");
             LLTJSocialImageSelect tLLOnlinClaimGetImage = new LLTJSocialImageSelect();
             Document tOutXmlDoc = tLLOnlinClaimGetImage.service(tInXmlDoc);
             System.out.println("打印传入报文============");
             JdomUtil.print(tInXmlDoc.getRootElement());
             System.out.println("打印传出报文============");
             JdomUtil.print(tOutXmlDoc);
         	        	
         } catch (Exception e) {
             e.printStackTrace();
         }
	}


	
}
