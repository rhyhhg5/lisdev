package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.ftp.FTPFile;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bl.LCGetBL;
import com.sinosoft.lis.bl.LCInsuredBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDDiseaseDB;
import com.sinosoft.lis.db.LDICDOPSDB;
import com.sinosoft.lis.db.LLAccidentTypeDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLCaseRelaDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LLSubReportDB;
import com.sinosoft.lis.db.LMDutyGetClmDB;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJSGetClaimSchema;
import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.schema.LLAccidentSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseDrugSchema;
import com.sinosoft.lis.schema.LLCaseExtSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDeclineSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLFeeOtherItemSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLOperationSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.schema.LLToClaimDutySchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LJSGetClaimSet;
import com.sinosoft.lis.vschema.LJSGetSet;
import com.sinosoft.lis.vschema.LLAccidentSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCaseDrugSet;
import com.sinosoft.lis.vschema.LLCasePolicySet;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDeclineSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLFeeOtherItemSet;
import com.sinosoft.lis.vschema.LLOperationSet;
import com.sinosoft.lis.vschema.LLSecurityReceiptSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.lis.vschema.LLToClaimDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.util.FTPTool;

public class LLTYAutoCaseRegister implements ServiceInterface{
	//错误容器
    public CErrors mCErrors = new CErrors();
    public VData mVData = new VData();
    public VData mInData = new VData();
    private GlobalInput mG = new GlobalInput();
	
    public LLTYAutoCaseRegister (){
    	
    }
	//传入报文
	private Document mInXmlDoc ;
	//处理标志
    private boolean mDealState = true;
    //报文类型
    private String mDocType="";
    //报文请求类型
    private String mRequestType="";
    //报文交互编码
    private String mTransactionNum="";  
	//版本号部分
    private Element mVersionData; 
    //影像件路径部分
    private Element mPictureData;
    //理赔用户部分
    private Element mUserData;
    //被保人部分
    private Element mCustomerData;  
    //事件部分
    private Element mAccList;
    //账单部分
    private Element mReceiptList;
    //理算部分
    private Element mPayList;
    //疾病部分
//    private Element mDiseaseList;
//    //意外部分
//    private Element mAccidentList;
//    //手术部分
//    private Element mOperationList;
    
    private String mLimit = "";
    private String mCaseRelaNo="";

    //应付号
    private String tGetNoticeNo="";
    //累计实赔金额
    private double mGrossAmount=0; 
    //累计理算金额
    private double mSumStandPay=0;
    //管理机构
    private String mMngCom="";
    //理赔用户
    private String mUserCode="";
    //平台理赔号
    private String mClaimNo="";
    //被保人号
    private String mInsuredNo="";
    //赔款总额
    private String mTotalAmount;
    //拒付原因
    private String mDeclineReason;
    //给付方式
    private String mCaseGetMode="";
    //领款人与被保人关系
    private String mRelaDrawerInsured="";
    //案件号
    private String mCaseNo="";
    //银行编码
    private String mBankNo="";
    //银行账户
    private String mAccNo ="";
    //银行账户名
    private String mAccName="";
    //医院名称
    private String mHospitalCode="";
    //医院编码
    private String mHospitalName="";
    //事件类型
    private String mAccDescType="";
    //发生日期
    private String mAccDate="";
    //事件信息号
    private String mSubRptNo="";
    //返回类型代码
    private String mResponseCode = "1";
    //版本号
    private String mVersionCode="";
    //影像件
    private String mPicPath="";
    //错误描述
    private String mErrorMessage = "";
    //赔案号
    private  String mClmNo ="";
    //赔付结论
    private  String totalGiveType="";
    
    private FTPTool tFTPTool;
    
    String aMakeDate = PubFun.getCurrentDate();
    String aMakeTime = PubFun.getCurrentTime();
    
    private LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema(); //被保人客户表
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema(); //申请信息
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema(); // 案件信息
    private LLAppClaimReasonSchema mLLAppClaimReasonSchema = new LLAppClaimReasonSchema(); //申请原因信息表
    private LLCaseExtSchema mLLCaseExtSchema = new LLCaseExtSchema(); //案件信息扩展表
    private LLFeeMainSet mLLFeeMainSet = new LLFeeMainSet(); //账单信息
    private LLSubReportSet mLLSubReportSet = new LLSubReportSet();//事件信息表
    private LLCaseRelaSet mLLCaseRelaSet = new LLCaseRelaSet();//事件关联信息表
    private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();  // 费用项明细
    private LLCaseDrugSet mLLCaseDrugSet=new LLCaseDrugSet(); //扣除明细表
    private LLFeeOtherItemSet  mLLFeeOtherItemSet = new LLFeeOtherItemSet();// 社保账单明细
    private LLSecurityReceiptSet mSeReceiptSet = new LLSecurityReceiptSet(); //社保账单
    private LLCaseCureSet  mLLCaseCureSet = new LLCaseCureSet(); //疾病信息表
    private LLAccidentSet  mLLAccidentSet = new LLAccidentSet(); //意外信息表
    private LLOperationSet mLLOperationSet = new LLOperationSet();//手术信息表
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();//待理算责任信息表
    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();//分案明细表
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();//赔案明细表
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();//保单险种层明细
    private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet(); //赔付应付表
    private LLClaimSet mLLClaimSet = new LLClaimSet(); //赔案信息表
    private LJSGetSet mLJSGetSet = new LJSGetSet(); //应付总表
    private LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();//对外接口表
    private LJSGetClaimSet mNewLJSGetClaimSet = new LJSGetClaimSet(); //新赔付应付表
    private LLClaimDeclineSet mLLClaimDeclineSet = new LLClaimDeclineSet();
    private ExeSQL mExeSQL = new ExeSQL();

	public Document service(Document pInXmlDoc) {
		// 个案统一接口开始执行，第一次，勿扰。
		System.out.println("LLTYAutoCaseRegister---个险统一外包----service()");
		
		mInXmlDoc = pInXmlDoc;
		
		try{
			if(!getInputData(mInXmlDoc)){
				mDealState=false;
			}else{
				if(!checkData()){
					mDealState= false;
				}else{
					if(!dealData()){
						mDealState = false;
						cancelCase();
					}
				}
				
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()","系统未知错误");
            cancelCase();
		}finally{
			return createXML();
		}
	}
	
	  /**
     * 生成返回报文处理
     * 
     */
    
    private Document createXML(){
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
        	try {
        		return createResultXML(mCaseNo,mClaimNo);
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo,String aClaimNo) throws Exception {
         System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BASE_DATA");
         tBodyData.addContent(tBackData);
         
         //Base_Data部分
         Element tCaseno = new Element("CASENO");      
         tCaseno.setText(aCaseNo);
         tBackData.addContent(tCaseno);
         
         Element tClaimno = new Element("CLAIMNO");
         tClaimno.setText(aClaimNo);
         tBackData.addContent(tClaimno);
         
         Element tRgtDate = new Element("RGTDATE");
         tRgtDate.setText(this.mLLRegisterSchema.getRgtDate());
         tBackData.addContent(tRgtDate);
         
         tRootData.addContent(tBodyData);
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mCErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBody.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
	
	  /**
     * 删除数据处理
     * 
     */
    private boolean cancelCase() throws Exception{
    	System.out.println("LLTYAutoCaseRegister--统一接口---cancelCase()");
    	  //理算数据
        String tClaimSQL = "delete from LLClaim where CaseNo='" + mCaseNo + "'";
        String tClaimPolicySQL = "delete from LLClaimPolicy where CaseNo='" +
                                 mCaseNo + "'";
        String tClaimDetailSQL = "delete from LLClaimDetail where CaseNo='" +
                                 mCaseNo + "'";
        String tToClaimDutySQL = "delete from LLToClaimDuty where CaseNo ='" +
                                 mCaseNo + "'";
        String tCasePolicySQL = "delete from LLCasePolicy where CaseNo ='"
                                + mCaseNo + "'";

        String tFeeMainSQL = "delete from LLFeeMain where CaseNo='" + mCaseNo +
                             "'";
        String tCaseReceiptSQL = "delete from LLCaseReceipt where CaseNo = '" +
                                 mCaseNo + "'";
        String tCaseItemSQL = "delete from LLFeeOtherItem where caseno='"+mCaseNo+"'";
        String tCaseDrugSQL = "delete from LLCaseDrug where caseno='"+mCaseNo+"'";
        String tSecurySQL = "delete from llSecurityReceipt where  caseno='"+mCaseNo+"'";
        		
        String tCaseCureSQL = "delete from LLCaseCure where CaseNo = '" +
                              mCaseNo + "'";
        String tOperationSQL = "delete from LLOperation where CaseNo = '" +
                               mCaseNo + "'";
        String tAccidentSQL = "delete from LLAccident where CaseNo = '" +
                               mCaseNo + "'";    
        String tLJSGet = "delete from LJSGet where othernotype='5' and otherno = '" +
        						mCaseNo + "'";
        String tLJSGetClaim = "delete from LJSGetClaim where othernotype='5' and otherno = '" +
								mCaseNo + "'";
        String tLLHospCase ="delete from llhospcase where caseno='"+mCaseNo+"' and claimno='"+mClaimNo+"'";

//        String tSubReportSQL = "delete from LLSubReport a where exists (select 1 from llcaserela where a.subrptno=subrptno and CaseNo='" +
//                               mCaseNo + "')";
//        String tCaseRelaSQL = "delete from LLCaseRela where CaseNo='" + mCaseNo +
//                              "'";
        String tCaseSQL = "update  LLCase set rgtstate='14' ,CancleReason='9',CancleRemark='统一外包理赔自动撤件',CancleDate=current date where CaseNo='" + mCaseNo + "'";

//        String tRegisterSQL = "delete from LLRegister where RgtNo ='" + mCaseNo +
//                              "'";

//        String tHospCaseSQL = "delete from LLHospCase where CaseNo ='" +
//                              mCaseNo + "'";

        MMap tmpMap = new MMap();
        tmpMap.put(tClaimSQL, "DELETE");
        tmpMap.put(tClaimPolicySQL, "DELETE");
        tmpMap.put(tClaimDetailSQL, "DELETE");
        tmpMap.put(tToClaimDutySQL, "DELETE");
        tmpMap.put(tCasePolicySQL, "DELETE");
        tmpMap.put(tFeeMainSQL, "DELETE");
        tmpMap.put(tCaseReceiptSQL, "DELETE");
        tmpMap.put(tCaseCureSQL, "DELETE");
        tmpMap.put(tOperationSQL, "DELETE");
        tmpMap.put(tAccidentSQL, "DELETE");
        tmpMap.put(tLLHospCase,"DELETE");
        tmpMap.put(tLJSGet, "DELETE");
        tmpMap.put(tLJSGetClaim, "DELETE");
//        tmpMap.put(tSubReportSQL, "DELETE");
//        tmpMap.put(tCaseRelaSQL, "DELETE");
        tmpMap.put(tCaseSQL, "UPDATE");
//        tmpMap.put(tRegisterSQL, "DELETE");
//        tmpMap.put(tHospCaseSQL, "DELETE");

        try {
          
            this.mInData.clear();
            this.mInData.add(tmpMap);
        } catch (Exception ex) {
            buildError("cancelCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInData, "")) {
            buildError("cancelCase", "撤销数据提交失败");
            return false;
        }
    	
    	return false;
    }
	
	/**
	 * 理赔报文数据处理
	 * 
	 */
	private boolean dealData() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--dealData()");
		
		//报文基本数据校验
		if(!dealBasicData()){
			return false;
		}
		//理赔案件信息
		if(!CaseRegister()){
			return false;
		}
		
		//理赔事件信息
		if(!getSubReport()){
			return false;
		}

		//数据进行存储处理
    	if(!dealCase()){
    		return false;
    	}
    	//理算核赔规则走一圈，暂无参考，借鉴慎重
    	if(!uwCheck()){
    		return false;
    	}

    	//对外接口表
    	if(!dealHospCase()){
    		return false;
    	}
    	
    	// 影像件资料进行存储
    	if(!dealTYScan()){
    		return false;
    	}

		return true;
	}
	
	 /**
     * 影像件接口解析
     * 
     */
    private boolean dealTYScan() throws Exception{
    	System.out.println("LLTYAutoCaseRegister--统一接口--dealTYScan()--数据处理开始");
    	
    	mPicPath =mPictureData.getChildText("PICPATH");
    	
    	if(mPicPath==null || "".equals(mPicPath)) {
    		buildError("dealTYScan()", "影像件路径不能为空"); 
    		mDealState = false; 
    		return false;
    	}
    	
    	ImageHandler tImageHandler = new ImageHandler(mPicPath, mCaseNo, "TY",mG.ManageCom, mG.Operator);
    	if(!tImageHandler.dealImage()) {
    		buildError("dealTYScan()", "影像件处理有问题"); 
    		mDealState = false; 
    		return false;
    	}

    	return true;
    }
	 
    /***
     * 对外接口数据
     * 
     */
    private boolean dealHospCase() throws Exception{
    	 System.out.println("LLTYAutoCaseRegister--统一接口--dealHospCase()--数据处理开始");
    	 mLLHospCaseSchema.setCaseNo(mCaseNo);
		 mLLHospCaseSchema.setHospitCode(mRequestType);
		 mLLHospCaseSchema.setHandler(mLLCaseSchema.getHandler());
		 mLLHospCaseSchema.setAppTranNo(mTransactionNum);
		 mLLHospCaseSchema.setAppDate(aMakeDate);
		 mLLHospCaseSchema.setAppTime(aMakeTime);
		 mLLHospCaseSchema.setCaseType("15");    //案件类型 
		 mLLHospCaseSchema.setConfirmState("1");
    	 mLLHospCaseSchema.setDealType("2");
    	 mLLHospCaseSchema.setBnfNo(mLLCaseSchema.getCustomerNo());
    	 mLLHospCaseSchema.setClaimNo(mClaimNo); 
    	 mLLHospCaseSchema.setRealpay(mTotalAmount);
    	 mLLHospCaseSchema.setHCNo(mVersionCode);
    	 mLLHospCaseSchema.setMakeDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setMakeTime(PubFun.getCurrentTime());
    	 mLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
    	 mLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());
    	 
    	 MMap mMMap = new MMap();
    	 mMMap.put(mLLHospCaseSchema, "DELETE&INSERT");

         //提交数据库
           PubSubmit tPubSubmit = new PubSubmit();
           try{
  	         this.mInData.clear();       
  	         this.mInData.add(mMMap);
  	         if(!tPubSubmit.submitData(mInData, "")){
  	        	 buildError("dealCase", "数据提交失败");
  	             return false;
  	         }
           } catch (Exception o){
  		   		 System.out.println(o.getMessage());
  		   		 System.out.println("案件环节生成数据信息出错...");
  		         mDealState = false;
  		         mResponseCode = "E";
  		         buildError("service()","系统未知错误"); 
      }
    	
    	return true;
    }
    
	
	 /***
     * 理赔金额过核赔
     * 
     */
    private boolean uwCheck() throws Exception{
    	
    	System.out.println("LLTYAutoCaseRegister--统一接口--uwCheck()--数据处理开始");
    	LLCaseCommon tLLCaseCommon  = new LLCaseCommon();
    	MMap mMMap = new MMap();
    	SSRS tCusSSRS = new SSRS();
    	ExeSQL oExeSQL = new ExeSQL();
    	VData tVData = new VData();
    	String tErr= null;
    	if(mLLRegisterSchema.getRgtNo()!=null && !"".equals(mLLRegisterSchema.getRgtNo())){
    		String tCusSql="select distinct customerno from llcase where rgtno='"+this.mLLRegisterSchema.getRgtNo()+"'with ur";
    		tCusSSRS = oExeSQL.execSQL(tCusSql);
    		System.out.println(tCusSSRS.getMaxRow());
    		
    	}
    	for(int a=1;a<=tCusSSRS.getMaxRow();a++){
    		LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
    		LLCaseSet tLLCaseSet = new LLCaseSet();
    		LLClaimDetailSet tLLClaimDetaillSet = new LLClaimDetailSet();
    		LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
    		LLSubReportSet tLLSubReportSet = new LLSubReportSet();
    		
    		LLCaseDB tLLCaseDB = new LLCaseDB();
    		tLLCaseDB.setCustomerNo(tCusSSRS.GetText(a, 1));
    		tLLCaseDB.setRgtNo(mLLRegisterSchema.getRgtNo());
    		tLLCaseDB.setRgtState("01");
    		tLLCaseSet.add(tLLCaseDB.query());
    		LLCaseSchema tLLCaseSchema=tLLCaseSet.get(1);
    		
    		if(tLLCaseSet.size()>0 && tLLCaseSet!=null){
    			for(int b=1;b<=tLLCaseSet.size();b++){
    				LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB(); 
    				tLLClaimDetailDB.setCaseNo(tLLCaseSet.get(b).getCaseNo());
    				tLLClaimDetaillSet.set(tLLClaimDetailDB.query());
    				LLCaseRelaDB  tLLCaseRelaDB = new LLCaseRelaDB();
    				tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(b).getCaseNo());
    				tLLCaseRelaSet.set(tLLCaseRelaDB.query());
    			}
    			
    			for(int c=1;c<=tLLCaseRelaSet.size();c++){
    				LLSubReportDB  tLLSubReportDB = new LLSubReportDB();
    				LLSubReportSet aLLSubReportSet = new LLSubReportSet();
    				tLLSubReportDB.setSubRptNo(tLLCaseRelaSet.get(c).getSubRptNo());
    				aLLSubReportSet.set(tLLSubReportDB.query());
    				tLLSubReportSet.add(aLLSubReportSet);

    			}
    		}
    		
    		System.out.println("统一接口个险外包 开始进行核赔规则.....");
    		
    		tVData.add(tLLCaseSchema);
    		tVData.add(tLLSubReportSet);
    		tVData.add(tLLCaseRelaSet);
    		tVData.add(tLLClaimDetaillSet);
    		tVData.add(mG);
    		
    		if(!tLLUWCheckBL.submitData(tVData, "")){
    			if(tErr==null){
            		  tErr="客户号为"+tCusSSRS.GetText(a, 1)+"的客户，"+tLLUWCheckBL.getErrors().getFirstError()+"(本次批量导入该客户所属案件号"+tLLCaseSchema.getCaseNo()+");";  
            	  }
    		    if(tErr!=null){
                	 buildError("uwCheck",tErr);
                	 return false;
    		    }
             }else{
            	 //变更案件信息
            	 tLLCaseSchema.setRgtState("04"); //理算状态
            	 tLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
            	 tLLCaseSchema.setModifyTime(PubFun.getCurrentTime());
            	 mMMap.put(tLLCaseSchema, "UPDATE");
            	 //轨迹记录表
  	             LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
  	             mLLCaseOpTimeSchema.setCaseNo(mLLCaseSchema.getCaseNo());
  	             mLLCaseOpTimeSchema.setRgtState("04");
  	             mLLCaseOpTimeSchema.setOperator(mUserCode);
  	             mLLCaseOpTimeSchema.setManageCom(mLLCaseSchema.getMngCom()); 
  	            try {
  	               LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
  	                       mLLCaseOpTimeSchema);
  	               mMMap.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
  	           } catch (Exception ex) {

  	           }
  	            
             }
	
    	}
    	//提交数据库
        PubSubmit tPubSubmit = new PubSubmit();
        try{
	         this.mInData.clear();       
	         this.mInData.add(mMMap);
	         if(!tPubSubmit.submitData(mInData, "")){
	        	 buildError("dealCase", "数据提交失败");
	             return false;
	         }
        } catch (Exception o){
		   		 System.out.println(o.getMessage());
		   		 System.out.println("核赔环节生成数据信息出错...");
		         mDealState = false;
		         mResponseCode = "E";
		         buildError("service()","系统未知错误"); 
      }

    	return true;
    }
	
	/**
	 * 数据存储处理
	 * 
	 */
	private boolean dealCase() throws Exception{
    	System.out.println("LLTYAutoCaseRegister--统一接口--dealCase()--数据处理开始");
    	
    	
		    	MMap mMMap = new MMap();
		    	//提交数据信息
		        mMMap.put(mLLCaseSchema,"DELETE&INSERT");
		        mMMap.put(mLLRegisterSchema, "DELETE&INSERT");
		        if(mLLCaseExtSchema.getCaseNo()!=null && !"".equals(mLLCaseExtSchema.getCaseNo())){
		        	mMMap.put(mLLCaseExtSchema, "DELETE&INSERT");
		        }
		        mMMap.put(mLLSubReportSet, "DELETE&INSERT"); 
		        mMMap.put(mLLCaseRelaSet, "DELETE&INSERT");
		        mMMap.put(mLLCaseCureSet, "DELETE&INSERT");
		        if(mLLAccidentSet!=null && mLLAccidentSet.size()>0 ){
		        	mMMap.put(mLLAccidentSet, "DELETE&INSERT");
		        }
		        if(mLLOperationSet!=null && mLLOperationSet.size()>0){
		        	 mMMap.put(mLLOperationSet, "DELETE&INSERT");
		        }
		       if(mLLAppClaimReasonSchema.getCaseNo()!=null && !"".equals(mLLAppClaimReasonSchema.getCaseNo())){
		    	   mMMap.put(mLLAppClaimReasonSchema, "DELETE&INSERT");
		       }
		       if(mLLCaseDrugSet!=null && mLLCaseDrugSet.size()>0){
		    	   mMMap.put(mLLCaseDrugSet, "DELETE&INSERT");
		       }
		       if(mSeReceiptSet!=null && mSeReceiptSet.size()>0){
		    	   mMMap.put(mSeReceiptSet, "DELETE&INSERT");
		       }
		       if(mLLFeeOtherItemSet!=null && mLLFeeOtherItemSet.size()>0){
		    	   mMMap.put(mLLFeeOtherItemSet, "DELETE&INSERT");
		       }
		       if(mLLCaseReceiptSet!=null && mLLCaseReceiptSet.size()>0){
		    	   mMMap.put(mLLCaseReceiptSet, "DELETE&INSERT");
		       }
		       mMMap.put(mLLFeeMainSet, "DELETE&INSERT");
		       //增加拒付原因
		       if(mLLClaimDeclineSet!=null && mLLClaimDeclineSet.size()>0){
		    	   mMMap.put(mLLClaimDeclineSet, "DELETE&INSERT");
		       }
		       
		      
		       
		       //理算数据
		       mMMap.put(mLLToClaimDutySet, "DELETE&INSERT");
		       mMMap.put(mLLClaimDetailSet, "DELETE&INSERT");
		       mMMap.put(mNewLJSGetClaimSet, "DELETE&INSERT");
		       mMMap.put(mLLClaimPolicySet, "DELETE&INSERT");
		       mMMap.put(mLLCasePolicySet, "DELETE&INSERT");
		       mMMap.put(mLLClaimSet, "DELETE&INSERT");
		       mMMap.put(mLJSGetSet, "DELETE&INSERT");
		       
		
		       //提交数据库
		         PubSubmit tPubSubmit = new PubSubmit();
		         
		         try{
			         this.mInData.clear();       
			         this.mInData.add(mMMap);
			         if(!tPubSubmit.submitData(mInData, "")){
			        	 buildError("dealCase", "数据提交失败");
			             return false;
			         }
		         } catch (Exception o){
				   		 System.out.println(o.getMessage());
				   		 System.out.println("案件环节生成数据信息出错...");
				         mDealState = false;
				         mResponseCode = "E";
				         buildError("service()","系统未知错误"); 
		    }
         
    	return true;
    }
	
	/**
	 * 理赔事件信息
	 * 
	 */
	private boolean getSubReport() throws Exception{
		//开始了多事件的循环，谨慎操刀。。。
		System.out.println("LLTYAutoCaseRegister--统一接口--getSubReport()");
		
	     mClmNo = PubFun1.CreateMaxNo("CLMNO", mLimit);
	     System.out.println("mClmNo======"+mClmNo);
	     tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", mLimit); 	     
	     System.out.println("tGetNoticeNo=="+tGetNoticeNo);
		
		if(mAccList.getChildren("ACC_DATA").size()>0){
			for(int i=0;i<mAccList.getChildren("ACC_DATA").size();i++){
				Element aAccData = (Element) mAccList.getChildren("ACC_DATA").get(i);
				
				//事件信息表
				if(!getAccRegister(aAccData)){
					return false;
				}
				
				//账单信息 0-医院   4-简易社保
				if(!getDealFee(aAccData)){
					return false;
				}
				
				//检录信息
				if(!getRegisterCheck(aAccData)){
					return false;
				}
				
				//理算信息
				if(!getCaseClaim(aAccData)){
					return false;
				}
   	 
			}
			   //理算确认
				if(!getLastCase()){
					return false;
				}
		 	//获取赔付结论。。愁死。。。

		}else{
			buildError("getSubReport()", "报文中没有理赔信息!");
	        mDealState = false; 
	        return false;
		}

		return true;
	}
	/**
	 * 案件理算确认
	 * 
	 */
	private boolean getLastCase() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getLastCase()");
		 
	     for(int j=1;j<=mLJSGetClaimSet.size();j++){
	    	 boolean badFlag =true;
	    	 LJSGetClaimSchema oldLJSGetClaimSchema = mLJSGetClaimSet.get(j);
	    	 
	    	 if(mNewLJSGetClaimSet.size()<=0 || mNewLJSGetClaimSet==null ){
	    		 LJSGetClaimSchema newLJSClaimSchema = new LJSGetClaimSchema();
	             Reflections eLJSGetRef = new Reflections();
	             eLJSGetRef.transFields(newLJSClaimSchema, oldLJSGetClaimSchema);
	             mNewLJSGetClaimSet.add(newLJSClaimSchema);
	    	 }else {
	    		 String jContNo = oldLJSGetClaimSchema.getContNo();
	    		 String jRiskCode = oldLJSGetClaimSchema.getRiskCode();
	    		 String jPolNo = oldLJSGetClaimSchema.getPolNo();
	    		 String jGetDutyKind = oldLJSGetClaimSchema.getGetDutyKind();
	    		 double oPayMoney=0;
	    		 for(int o=1;o<=mNewLJSGetClaimSet.size();o++){
	    			LJSGetClaimSchema newNLjsGetSchema = mNewLJSGetClaimSet.get(o);
	    			if(jContNo.equals(newNLjsGetSchema.getContNo())&&
	    					jRiskCode.equals(newNLjsGetSchema.getRiskCode())&&
	    					jPolNo.equals(newNLjsGetSchema.getPolNo())&&
	    					jGetDutyKind.equals(newNLjsGetSchema.getGetDutyKind())  
	    					){
	    						
	    					oPayMoney =newNLjsGetSchema.getPay()+oldLJSGetClaimSchema.getPay();
	    					System.out.println(Arith.round(oPayMoney,2));
	    					newNLjsGetSchema.setPay(Arith.round(oPayMoney,2));
	    					badFlag =false;
	    			}
	    		 }
	    		 	if(badFlag){
	    		 	
	    				Reflections eLJSGetRef = new Reflections();
	    				LJSGetClaimSchema newaLJSClaimSchema = new LJSGetClaimSchema();
	   	             	eLJSGetRef.transFields(newaLJSClaimSchema, oldLJSGetClaimSchema);
	   	             	mNewLJSGetClaimSet.add(newaLJSClaimSchema);
		    			
	    		   }

	    	 }
	    	 
	     }
		
		
			
		//理赔金额比较
		 if(Arith.round(Double.parseDouble(mTotalAmount),2)- Arith.round(mGrossAmount,2) != 0)
         {
       	  buildError("getLastCase", "每个给付责任的理赔金额不等于赔付金额");
       	  return false;
         } else{
	    	
           LLClaimSchema cLLClaimSchema = new LLClaimSchema();
           cLLClaimSchema.setCaseNo(mLLCaseSchema.getCaseNo());
           cLLClaimSchema.setRgtNo(mLLCaseSchema.getRgtNo());
           cLLClaimSchema.setClmNo(mClmNo);
           cLLClaimSchema.setGetDutyKind("000000");
           cLLClaimSchema.setClmState("2");
           cLLClaimSchema.setStandPay(mSumStandPay);
           System.out.println(Arith.round(mGrossAmount,2));
           cLLClaimSchema.setRealPay(Arith.round(mGrossAmount,2));
           cLLClaimSchema.setGiveType(totalGiveType);
           cLLClaimSchema.setGiveTypeDesc(getGiveDesc(totalGiveType));
           cLLClaimSchema.setClmUWer(mUserCode);
           cLLClaimSchema.setCheckType("0");
           cLLClaimSchema.setMngCom(mLLCaseSchema.getMngCom());
           cLLClaimSchema.setOperator(mUserCode);
           cLLClaimSchema.setMakeDate(aMakeDate);
           cLLClaimSchema.setMakeTime(aMakeTime);
           cLLClaimSchema.setModifyDate(aMakeDate);
           cLLClaimSchema.setModifyTime(aMakeTime);
           
           mLLClaimSet.add(cLLClaimSchema);
           
           boolean mtFlag = false;
           for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
        	   LLClaimDetailSchema mtLLClaimDetailSchema = mLLClaimDetailSet.get(i);
			   if("3".equals(mtLLClaimDetailSchema.getGiveType())) {
				   if("".equals(mDeclineReason) || "null".equals(mDeclineReason) || mDeclineReason==null) {
	              		 buildError("getCaseClaim", "当赔付结论为全额拒付时，拒付原因必录。");
	                       return false;
	              	 }
				   mtFlag = true;
				   break;
			   }
		   }
           
          if(mtFlag) {
        	 LLClaimDeclineSchema tLLClaimDeclineSchema = new LLClaimDeclineSchema();
      		 tLLClaimDeclineSchema.setDeclineNo(cLLClaimSchema.getClmNo());
      		 tLLClaimDeclineSchema.setClmNo(cLLClaimSchema.getClmNo());
      		 tLLClaimDeclineSchema.setRgtNo(cLLClaimSchema.getRgtNo());
      		 tLLClaimDeclineSchema.setCaseNo(cLLClaimSchema.getCaseNo());
      		 tLLClaimDeclineSchema.setReason(mDeclineReason);
      		 tLLClaimDeclineSchema.setDeclineDate(aMakeDate);
      		 tLLClaimDeclineSchema.setMngCom(cLLClaimSchema.getMngCom());
      		 tLLClaimDeclineSchema.setOperator(cLLClaimSchema.getOperator());
      		 tLLClaimDeclineSchema.setMakeDate(aMakeDate);
      		 tLLClaimDeclineSchema.setMakeTime(aMakeTime);
      		 tLLClaimDeclineSchema.setModifyDate(aMakeDate);
      		 tLLClaimDeclineSchema.setModifyTime(aMakeTime);
      		 
      		mLLClaimDeclineSet.add(tLLClaimDeclineSchema);
          }else {
        	  if(!"".equals(mDeclineReason) && !"null".equals(mDeclineReason) && mDeclineReason!=null) {
         		 buildError("getCaseClaim", "当赔付结论不为全额拒付时，拒付原因不录。");
                  return false;
         	 }
          }
           
          
           //应付总表
           LJSGetSchema cLJSGetSchema = new LJSGetSchema();
           Reflections LJSGettref = new Reflections();
	     	LJSGettref.transFields(cLJSGetSchema, mLJSGetClaimSet.get(1)); 
	        cLJSGetSchema.setSumGetMoney(Arith.round(mGrossAmount,2)); 
	        mLJSGetSet.add(cLJSGetSchema);
	        mLJSGetClaimSet.clear();
        }
		
		
		return true ;
	}
	
	/**
	 * 案件理算信息处理
	 * 
	 */
	private boolean getCaseClaim(Element cAccData) throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getCaseClaim()");

	     LLCaseCommon cLLCaseCommon = new LLCaseCommon();
	     Element cPayList = cAccData.getChild("PAYLIST");
	     List cPayData = new ArrayList();
	     cPayData = (List) cPayList.getChildren("PAYDATA");

	     out:
	     for(int c=0;c<cPayData.size();c++){
	    	Element cPayInfo = (Element) cPayData.get(c);
	    	String cContNo = cPayInfo.getChildText("CONTNO");
	    	String cRiskCode = cPayInfo.getChildText("RISKCODE");
	    	String cGetDutyCode = cPayInfo.getChildText("GETDUTYCODE");
	    	String cGetDutyKind = cPayInfo.getChildText("GETDUTYKIND");
	    	double cTabFeeMoney = Double.parseDouble(cPayInfo.getChildText("TABFEEMONEY"));
	    	double cOutDutyAmnt = Double.parseDouble(cPayInfo.getChildText("OUTDUTYAMNT"));
	    	double cStandPay = Double.parseDouble(cPayInfo.getChildText("STANDPAY"));
	    	double cRelaPay = Double.parseDouble(cPayInfo.getChildText("RELAPAY"));
	    	double cDeclineAmnt = Double.parseDouble(cPayInfo.getChildText("DECLINEAMNT"));
	    	String cGiveType = cPayInfo.getChildText("GIVETYPE");
	    	String cGiveReason = cPayInfo.getChildText("GIVEREASON");
		     //险种判断标记
		    boolean mRiskFlag=true;

	    	if(cRelaPay==0 && "1".equals(cGiveType)){
	    		 buildError("getCaseClaim", "实赔金额为0，不能选择正常给付");
	             return false; 
	    	}
	    	
	    	//校验保单信息。第一次逆推理，多担待，有问题及早发现。
	    	 System.out.println("保单信息验证----开始---慎重---");
	     	//校验保单信息	
	     	for(int j=cPayData.size()-1;j>c;j--){
     			Element oContData =(Element) cPayData.get(j);
         		String oContNo = oContData.getChildText("CONTNO");
         		String oRiskCode = oContData.getChildText("RISKCODE");
         		String oGetDutyCode = oContData.getChildText("GETDUTYCODE");
         		String oGetDutyKind = oContData.getChildText("GETDUTYKIND");
         		
         		if(oContNo.equals(cContNo) && oRiskCode.equals(cRiskCode) && 
         				oGetDutyCode.equals(cGetDutyCode) && oGetDutyKind.equals(cGetDutyKind)){
         			buildError("dealFee", "保单"+cContNo+"下的给付责任"+cGetDutyCode+"和给付类型"+cGetDutyKind+"重复录入，请重新确认录入。");
        	           return false;
         		}
     		}
	     	
	    	 String tPolSQL ="select polno from lcpol where riskcode='"+cRiskCode+"' and contno='"+cContNo+"' "
	    	 		+ "and insuredno='"+mInsuredNo+"' union select polno from lbpol where riskcode='"+cRiskCode+"' "
	    	 		+ "and contno='"+cContNo+"' and insuredno='"+mInsuredNo+"' with ur";
	    	 String cPolNo = mExeSQL.getOneValue(tPolSQL);
	    	 LCPolSchema cLCPolSchema  = new  LCPolSchema();
	    	 if("".equals(cPolNo) || cPolNo==null){
	    		 buildError("caseClaim", "保单险种信息查询失败");
	    	 }else{
		    	 LCPolBL tLCPolBL = new LCPolBL();
		    	 tLCPolBL.setRiskCode(cRiskCode);
		    	 tLCPolBL.setContNo(cContNo);
		    	 tLCPolBL.setInsuredNo(mInsuredNo);
		    	 tLCPolBL.setPolNo(cPolNo);
		    	 if(!tLCPolBL.getInfo()){
		    		 buildError("caseClaim", "保单险种信息查询失败");
	                 return false;
		    	 }
		    	 cLCPolSchema =tLCPolBL.getSchema();
	    	 }
	    	 
	    	 String tGetsql="select dutycode from lcget where polno='"+cLCPolSchema.getPolNo()+"' and "
	    	 		+ "getdutycode='"+cGetDutyCode+"' union select dutycode from lbget where polno='"+cLCPolSchema.getPolNo()+"' and "
	    	 		+ "getdutycode='"+cGetDutyCode+"' with ur";
	    	 String cDutycode =mExeSQL.getOneValue(tGetsql);
	    	 LCGetSchema cLCGetSchema  = new LCGetSchema();
	    	 if("".equals(cDutycode) || cDutycode==null){
	    		 buildError("caseClaim", "保单给付责任信息查询失败");
                return false;
	    	 }else{
		    	 LCGetBL tLCGetBL =new LCGetBL();
		    	 tLCGetBL.setPolNo(cLCPolSchema.getPolNo());
		    	 tLCGetBL.setGetDutyCode(cGetDutyCode);
		    	 tLCGetBL.setDutyCode(cDutycode);
		    	 if(!tLCGetBL.getInfo()){
		    		 buildError("caseClaim", "保单给付责任信息查询失败");
	                 return false;
		    	 }
		    	 
		    	 cLCGetSchema = tLCGetBL.getSchema();
	    	 }
	    	 
	    	   //出险日期、入院日期、出院日期必须都在保单年度里
	         if (!cLLCaseCommon.checkPolValid(cLCPolSchema.getPolNo(), mAccDate)) {
	            	buildError("caseClaim", "出险日期不在保单有效期内");
	                return false;
            }

	         //得到赔付结论和赔付结论依据(name)
             String giveTypeSql = "select CodeName from ldcode where 1 = 1 and codetype = 'llclaimdecision'  and code='"+cGiveType+"' order by Code";
             String giveReasonSql ="select a.CodeName from ldcode a where  trim(a.codetype)=(select trim(b.codeaLias) from ldcode b where b.codetype='llclaimdecision' and b.code='"+cGiveType+"')  and a.code='"+cGiveReason+"' order by a.code";
             ExeSQL giveTypeSQL = new ExeSQL();
             String mGiveTypeDesc = giveTypeSQL.getOneValue(giveTypeSql);
             String mGiveReasonDesc =giveTypeSQL.getOneValue(giveReasonSql);
             
             if(mGiveReasonDesc.equals("")||mGiveReasonDesc==null||mGiveReasonDesc=="null"){
            	 mGiveReasonDesc = cGiveReason;
            	 cGiveReason="";
             }
             
             System.out.println("统一接口个险 先进行赔付明细的描述-----");
             
             LMDutyGetClmDB cLMDutyGetClmDB = new LMDutyGetClmDB();
             cLMDutyGetClmDB.setGetDutyCode(cLCGetSchema.getGetDutyCode());
             
             //代理算责任
             LLToClaimDutySchema cLLToClaimDutySchema = new LLToClaimDutySchema();
             cLLToClaimDutySchema.setCaseNo(mCaseNo);
             cLLToClaimDutySchema.setGetDutyCode(cLCGetSchema.getGetDutyCode());
             cLLToClaimDutySchema.setPolNo(cLCPolSchema.getPolNo());
             cLLToClaimDutySchema.setGetDutyKind(cGetDutyKind);
             cLLToClaimDutySchema.setCaseRelaNo(mCaseRelaNo);
             cLLToClaimDutySchema.setSubRptNo(mSubRptNo);
             cLLToClaimDutySchema.setGrpContNo(cLCPolSchema.getGrpContNo());
             cLLToClaimDutySchema.setGrpPolNo(cLCPolSchema.getGrpPolNo());
             cLLToClaimDutySchema.setContNo(cContNo);
             cLLToClaimDutySchema.setRiskCode(cRiskCode);
             cLLToClaimDutySchema.setKindCode(cLCPolSchema.getKindCode()); //险类代码
             cLLToClaimDutySchema.setRiskVer(cLCPolSchema.getRiskVersion()); //险种版本号
             cLLToClaimDutySchema.setPolMngCom(cLCPolSchema.getManageCom());
             cLLToClaimDutySchema.setClaimCount("0");
             cLLToClaimDutySchema.setEstClaimMoney("0");
             cLLToClaimDutySchema.setDutyCode(cLCGetSchema.getDutyCode());
             
             mLLToClaimDutySet.add(cLLToClaimDutySchema);
             
             //分案保单明细
             LLCasePolicySchema cLLCasePolicySchema = new LLCasePolicySchema();
             Reflections cLLClaimPolicyref = new Reflections();
             cLLClaimPolicyref.transFields(cLLCasePolicySchema, cLLToClaimDutySchema);
             cLLCasePolicySchema.setRgtNo(mCaseNo);
             cLLCasePolicySchema.setCasePolType("0");  //保单类型 0-被保人案件
             cLLCasePolicySchema.setSaleChnl(cLCPolSchema.getSaleChnl()); //销售渠道
             cLLCasePolicySchema.setAgentCode(cLCPolSchema.getAgentCode());//代理人代码
             cLLCasePolicySchema.setAgentGroup(cLCPolSchema.getAgentGroup());//代理人组别
             cLLCasePolicySchema.setInsuredNo(cLCPolSchema.getInsuredNo());
             cLLCasePolicySchema.setInsuredName(cLCPolSchema.getInsuredName());
             cLLCasePolicySchema.setInsuredSex(cLCPolSchema.getInsuredSex());
             cLLCasePolicySchema.setInsuredBirthday(cLCPolSchema.getInsuredBirthday());
             cLLCasePolicySchema.setAppntNo(cLCPolSchema.getAppntNo());
             cLLCasePolicySchema.setAppntName(cLCPolSchema.getAppntName());
             cLLCasePolicySchema.setCValiDate(cLCPolSchema.getCValiDate());//生效日期
             cLLCasePolicySchema.setPolType("1");  //保单性质状态 1-正式 保单
             cLLCasePolicySchema.setMngCom(mMngCom);
             cLLCasePolicySchema.setOperator(mUserCode);
             cLLCasePolicySchema.setMakeDate(aMakeDate);
             cLLCasePolicySchema.setMakeTime(aMakeTime);
             cLLCasePolicySchema.setModifyDate(aMakeDate);
             cLLCasePolicySchema.setModifyTime(aMakeTime);
             
             if(mLLCasePolicySet.size()>0 && mLLCasePolicySet!=null){
            	 for(int r=1;r<=mLLCasePolicySet.size();r++){
            		 String rPolNo = mLLCasePolicySet.get(r).getPolNo();
            		 String rContNo = mLLCasePolicySet.get(r).getContNo();
            		 String rRiskCode = mLLCasePolicySet.get(r).getRiskCode();
            		 if(rRiskCode.equals(cLLCasePolicySchema.getRiskCode())&&
            				 rContNo.equals(cLLCasePolicySchema.getContNo())&&
            				 rPolNo.equals(cLLCasePolicySchema.getPolNo())){
            			 	 mRiskFlag=false; 
            		 }
            	 }
            	 //第二次添加llcasepolicy
                 if(mRiskFlag){
                	 mLLCasePolicySet.add(cLLCasePolicySchema);
                 }
              }else{
            	   mLLCasePolicySet.add(cLLCasePolicySchema);
             }
             
            
                 //赔案明细
             LLClaimDetailSchema cLLClaimDetailSchema = new LLClaimDetailSchema();
             cLLClaimDetailSchema.setCaseNo(mCaseNo);
             cLLClaimDetailSchema.setRgtNo(mCaseNo);
             cLLClaimDetailSchema.setClmNo(mClmNo);
             cLLClaimDetailSchema.setPolNo(cLCPolSchema.getPolNo());
             cLLClaimDetailSchema.setGetDutyCode(cGetDutyCode);
             cLLClaimDetailSchema.setGetDutyKind(cGetDutyKind);
             cLLClaimDetailSchema.setCaseRelaNo(mCaseRelaNo);
             cLLClaimDetailSchema.setStatType(cLMDutyGetClmDB.query().get(1).getStatType()); //统计类别
             cLLClaimDetailSchema.setGrpContNo(cLCPolSchema.getGrpContNo());
             cLLClaimDetailSchema.setGrpPolNo(cLCPolSchema.getGrpPolNo());
             cLLClaimDetailSchema.setContNo(cLCPolSchema.getContNo());
             cLLClaimDetailSchema.setKindCode(cLCPolSchema.getKindCode()); //险类代码
             cLLClaimDetailSchema.setRiskCode(cRiskCode);
             cLLClaimDetailSchema.setRiskVer(cLCPolSchema.getRiskVersion()); //险种版本号
             cLLClaimDetailSchema.setPolMngCom(cLCPolSchema.getManageCom());
             cLLClaimDetailSchema.setSaleChnl(cLCPolSchema.getSaleChnl());//销售渠道
             cLLClaimDetailSchema.setAgentCode(cLCPolSchema.getAgentCode());//代理人编码
             cLLClaimDetailSchema.setAgentGroup(cLCPolSchema.getAgentGroup());//代理人组别
             cLLClaimDetailSchema.setTabFeeMoney(cTabFeeMoney); //账单金额
             cLLClaimDetailSchema.setClaimMoney(cStandPay); //理算金额
             cLLClaimDetailSchema.setDeclineAmnt(cDeclineAmnt); //拒付金额
             cLLClaimDetailSchema.setOverAmnt("0");  //溢额
             cLLClaimDetailSchema.setRealPay(cRelaPay);
             cLLClaimDetailSchema.setStandPay(cRelaPay); 
             cLLClaimDetailSchema.setPreGiveAmnt("0");  //先期给费
             cLLClaimDetailSchema.setSelfGiveAmnt("0");  //自费
             cLLClaimDetailSchema.setRefuseAmnt("0");	 //不合理费用
             cLLClaimDetailSchema.setOtherAmnt("0");	 //其他已给付
             cLLClaimDetailSchema.setOutDutyAmnt(cOutDutyAmnt);	 //免赔额
             cLLClaimDetailSchema.setOutDutyRate("1");		//免赔比例
             cLLClaimDetailSchema.setApproveAmnt("0");		//通融给付
             cLLClaimDetailSchema.setAgreeAmnt("0");	  //协议给付
             cLLClaimDetailSchema.setDutyCode(cLCGetSchema.getDutyCode());
             cLLClaimDetailSchema.setGiveType(cGiveType);
             cLLClaimDetailSchema.setGiveTypeDesc(mGiveTypeDesc);
             cLLClaimDetailSchema.setGiveReason(cGiveReason);
             cLLClaimDetailSchema.setGiveReasonDesc(mGiveReasonDesc);
             cLLClaimDetailSchema.setMngCom(mLLCaseSchema.getMngCom());
             cLLClaimDetailSchema.setOperator(mUserCode);
             cLLClaimDetailSchema.setMakeDate(aMakeDate);
             cLLClaimDetailSchema.setMakeTime(aMakeTime);
             cLLClaimDetailSchema.setModifyDate(aMakeDate);
             cLLClaimDetailSchema.setModifyTime(aMakeTime);
             
             
             totalGiveType = sumGiveType(totalGiveType,cGiveType);
                   
             //总金额
             mGrossAmount += cRelaPay;
             mSumStandPay += cStandPay;
             
             mLLClaimDetailSet.add(cLLClaimDetailSchema);
             
             if(mLLClaimPolicySet.size()>0 && mLLClaimPolicySet!=null){
            	 for(int b=1;b<=mLLClaimPolicySet.size();b++){
            		 LLClaimPolicySchema bLLClaimPolicySchema= mLLClaimPolicySet.get(b);
            		 LJSGetClaimSchema  bLJSGetClaimSchema=mLJSGetClaimSet.get(b);
            		 String bOldContNo = bLLClaimPolicySchema.getContNo();
            		 String bOldRiskCode = bLLClaimPolicySchema.getRiskCode();
            		 String bOldGetDutyKind =bLLClaimPolicySchema.getGetDutyKind();
            		 String bOldPolNo = bLLClaimPolicySchema.getPolNo();
            		 String bCaseRelaNo = bLLClaimPolicySchema.getCaseRelaNo();
            		 if(bOldContNo.equals(cLLClaimDetailSchema.getContNo())
            				 && bOldRiskCode.equals(cLLClaimDetailSchema.getRiskCode())
            				 && bOldGetDutyKind.equals(cLLClaimDetailSchema.getGetDutyKind())
            				 && bOldPolNo.equals(cLLClaimDetailSchema.getPolNo())
            				 && bCaseRelaNo.equals(cLLClaimDetailSchema.getCaseRelaNo())){
            			
            			     double aRealPay= bLLClaimPolicySchema.getRealPay();
            			     double aStandPay= bLLClaimPolicySchema.getStandPay();
            			     double aPay = bLJSGetClaimSchema.getPay();
            			     aRealPay += cRelaPay;
            			     aPay += cRelaPay;
            			     aStandPay += cStandPay;
            			 	 bLLClaimPolicySchema.setRealPay(aRealPay);
            			 	 bLLClaimPolicySchema.setStandPay(aStandPay);
            			 	 System.out.println(Arith.round(aPay,2));
            			 	 bLJSGetClaimSchema.setPay(Arith.round(aPay,2));
            			 	 continue out;
            		 } 
            	 }
             }
             
             //分案保单明细
             LLClaimPolicySchema cLLClaimPolicySchema = new LLClaimPolicySchema();
             Reflections tReflections = new Reflections();
             tReflections.transFields(cLLClaimPolicySchema, cLLClaimDetailSchema);
             cLLClaimPolicySchema.setInsuredNo(cLCPolSchema.getInsuredNo());
             cLLClaimPolicySchema.setInsuredName(cLCPolSchema.getInsuredName());
             cLLClaimPolicySchema.setAppntNo(cLCPolSchema.getAppntNo());
             cLLClaimPolicySchema.setAppntName(cLCPolSchema.getAppntName());
             cLLClaimPolicySchema.setCValiDate(cLCPolSchema.getCValiDate());//生效日期
             cLLClaimPolicySchema.setClmState("1");   //理赔状态 1-已理算
             cLLClaimPolicySchema.setStandPay(cRelaPay);
             cLLClaimPolicySchema.setRealPay(cRelaPay);
             cLLClaimPolicySchema.setPreGiveAmnt("0");
             cLLClaimPolicySchema.setSelfGiveAmnt("0");
             cLLClaimPolicySchema.setRefuseAmnt("0");
             cLLClaimPolicySchema.setApproveAmnt("0");
             cLLClaimPolicySchema.setAgreeAmnt("0");
             cLLClaimPolicySchema.setGiveType(cGiveType);
             cLLClaimPolicySchema.setGiveTypeDesc(mGiveTypeDesc);
             cLLClaimPolicySchema.setGiveReason(cGiveReason);
             cLLClaimPolicySchema.setGiveReasonDesc(mGiveReasonDesc);
             cLLClaimPolicySchema.setClmUWer(mUserCode);

             mLLClaimPolicySet.add(cLLClaimPolicySchema);
             
             //赔付应付表
             LJSGetClaimSchema cLJSGetClaimSchema = new LJSGetClaimSchema();
             Reflections aLJSGet = new Reflections();
             aLJSGet.transFields(cLJSGetClaimSchema, cLLClaimDetailSchema); 
             cLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
             cLJSGetClaimSchema.setFeeOperationType(cLLClaimDetailSchema.getGetDutyKind());
             cLJSGetClaimSchema.setFeeFinaType(cLLClaimDetailSchema.getStatType());
             cLJSGetClaimSchema.setOtherNo(mLLCaseSchema.getCaseNo());
             cLJSGetClaimSchema.setOtherNoType("5");
             cLJSGetClaimSchema.setGetDutyKind(cLLClaimDetailSchema.getGetDutyKind());
             cLJSGetClaimSchema.setRiskVersion(cLCPolSchema.getRiskVersion());
             cLJSGetClaimSchema.setPay(Arith.round(cRelaPay,2));
             cLJSGetClaimSchema.setManageCom(cLCPolSchema.getManageCom());
             cLJSGetClaimSchema.setAgentCom(cLCPolSchema.getAgentCom());
             cLJSGetClaimSchema.setGetDutyCode("");
             
             mLJSGetClaimSet.add(cLJSGetClaimSchema);
             
	     }

		return true;
	}
	
	/**
	 * 案件检录信息
	 * 
	 */
	private boolean getRegisterCheck(Element wAccData) throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getRegisterCheck()");
		
		//疾病信息
		Element wDiseaseList = wAccData.getChild("DISEASELIST");
		List wDiseaseData = new ArrayList();
		wDiseaseData =(List) wDiseaseList.getChildren("DISEASEDATA");
		for(int w=0;w<wDiseaseData.size();w++){
			Element wDiseaseInfo = (Element) wDiseaseData.get(w);
			String wDiseaseCode = wDiseaseInfo.getChildText("DISEASECODE");
			String wDiseaseName = wDiseaseInfo.getChildText("DISEASENAME");
			
        	//校验录入的疾病信息
        	LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
            tLDDiseaseDB.setICDCode(wDiseaseCode);
            if (!tLDDiseaseDB.getInfo()) {
                 buildError("getRegisterCheck", "【疾病编码】不存在");
                 return false;
             }
            LLCaseCureSchema wLLCaseCureSchema = new LLCaseCureSchema();
            String tCureNo = PubFun1.CreateMaxNo("CURENO", mLimit);
            wLLCaseCureSchema.setSerialNo(tCureNo);
            wLLCaseCureSchema.setCaseNo(mCaseNo);
            wLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo); 
            wLLCaseCureSchema.setHospitalCode(mHospitalCode);
            wLLCaseCureSchema.setHospitalName(mHospitalName);
            wLLCaseCureSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
            wLLCaseCureSchema.setCustomerName(mLLCaseSchema.getCustomerName());
            wLLCaseCureSchema.setDiseaseCode(wDiseaseCode);
            wLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());
            wLLCaseCureSchema.setSeriousFlag("0");
            wLLCaseCureSchema.setOperator(mUserCode);
            wLLCaseCureSchema.setMngCom(mLLCaseSchema.getMngCom());
            wLLCaseCureSchema.setMakeDate(aMakeDate);
            wLLCaseCureSchema.setMakeTime(aMakeTime);
            wLLCaseCureSchema.setModifyDate(aMakeDate);
            wLLCaseCureSchema.setModifyTime(aMakeTime);
            
            mLLCaseCureSet.add(wLLCaseCureSchema);
	
		}
		
		//意外信息
		Element wAccidentList = wAccData.getChild("ACCIDENTLIST");
		List wAccidentData= new ArrayList();
		wAccidentData =(List) wAccidentList.getChildren("ACCIDENTDATA");
		
		for(int a=0;a<wAccidentData.size();a++){
    		Element wAccidentInfo =  (Element) wAccidentData.get(a);
    		LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
    		String wAccidentCode =wAccidentInfo.getChildText("ACCIDENTCODE");
    		String wAccidentName = wAccidentInfo.getChildText("ACCIDENTNAME");
    		//事件类型为意外时，必录
    		
    		if("2".equals(mAccDescType) && ("".equals(wAccidentCode) ||wAccidentCode==null)){
    			 buildError("getRegisterCheck", "【意外代码】不能为空");
                 return false;
    		}
    		
    		if(!"".equals(wAccidentCode)&& wAccidentCode!=null){
    			System.out.println("TY02--意外信息已录入--看进程走哪。。");
    			LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
                tLLAccidentTypeDB.setAccidentNo(wAccidentCode);
                if (!tLLAccidentTypeDB.getInfo()) {
                    buildError("getRegisterCheck", "【意外代码】不存在");
                    return false;
                }
                
            	String AccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mLimit);
             	tLLAccidentSchema.setAccidentNo(AccidentNo);
             	tLLAccidentSchema.setCaseNo(mCaseNo);
             	tLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
             	tLLAccidentSchema.setCode(wAccidentCode);
             	tLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());
             	
             	mLLAccidentSet.add(tLLAccidentSchema);
    		}
		}
		
		//手术信息
		
    	//手术信息
	   Element wOperationList = wAccData.getChild("OPERATIONLIST");
       List wOperationData = new ArrayList();
       wOperationData = wOperationList.getChildren();
       if(wOperationData.size()>0){
    	   for(int o=0;o<wOperationData.size();o++){
    		   Element wOperationInfo = (Element) wOperationData.get(o);
    		   LLOperationSchema tLLOperationSchema = new LLOperationSchema();
    		   String wOperationCode = wOperationInfo.getChildText("OPERATIONCODE");
    		   String wOperationName = wOperationInfo.getChildText("OPERATIONNAME");
    		   if(!"".equals(wOperationCode) && wOperationCode!=null){
    			   System.out.println("TY02--手术信息已录入");
    			   LDICDOPSDB tLDICDOpsDB = new LDICDOPSDB();
        		   tLDICDOpsDB.setICDOPSCode(wOperationCode);
        		   if(!tLDICDOpsDB.getInfo()){
        			   buildError("getRegisterCheck", "【手术代码】不存在");
                       return false;
        	        }
        		   
        		   String tCureNo = PubFun1.CreateMaxNo("OPERATION", mLimit);
        		   
        		   tLLOperationSchema.setSerialNo(tCureNo);
        		   tLLOperationSchema.setCaseNo(mCaseNo);
        		   tLLOperationSchema.setCaseRelaNo(mCaseRelaNo);
        		   tLLOperationSchema.setOperationCode(wOperationCode);
        		   tLLOperationSchema.setOperationName(tLDICDOpsDB.getICDOPSName());
        		   tLLOperationSchema.setMngCom(mLLCaseSchema.getMngCom());
                   tLLOperationSchema.setOperator(mUserCode);
                   tLLOperationSchema.setMakeDate(aMakeDate);
                   tLLOperationSchema.setMakeTime(aMakeTime);
                   tLLOperationSchema.setModifyDate(aMakeDate);
                   tLLOperationSchema.setModifyTime(aMakeTime);
        		   
                   mLLOperationSet.add(tLLOperationSchema);
    		   }else{
    			   System.out.println("TY02--手术信息未录入");
    		   }
    	   }
       }

		return true;
	}
	
	
	/**
	 * 解析账单信息
	 * 账单分为 属性分为 0-医院 4-简易社保
	 */
	private boolean getDealFee(Element tAccData) throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getDealFee()");
		
		mReceiptList = tAccData.getChild("RECEIPTLIST");
		List aReceiptDemo = new ArrayList();
		aReceiptDemo = (List)mReceiptList.getChildren();
		for(int j=0;j<aReceiptDemo.size();j++){
			Element aRceiptData = (Element) aReceiptDemo.get(j);
			String aFeeValue = aRceiptData.getAttributeValue("FEEATTIC");
			
			if(!aFeeValue.equals("0") && !aFeeValue.equals("4")){
				buildError("getDealFee","账单属性为医院或简易社保结算，请重新录入。");
				return false;
			}
			
			if("0".equals(aFeeValue)){
				if(!dealHospFee(aRceiptData)){
					mDealState=false;
					return false;
				}
			}else if("4".equals(aFeeValue)){
					if(!dealSecurityFee(aRceiptData)){
						mDealState=false;
						return false;
					}
			}else{
				
				buildError("getDealFee","账单属性值非空，请重新录入");
				return false;
			}

		}

		return true;
	}
	
	/**
	 * 账单属性为4-简易社保时
	 * 
	 */
	
	private boolean dealSecurityFee(Element nRceiptData) throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--dealSecurityFee()");
		
		System.out.println("账单属性为 4-简易社保");
		if(nRceiptData!=null){
				
				String mMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
				String tFeeAttic = nRceiptData.getChildTextTrim("FEEATTIC"); //账单属性
				String tFeeType = nRceiptData.getChildTextTrim("FEETYPE");  // 账单种类
				String tReceiptNo = nRceiptData.getChildTextTrim("MAINFEENO"); //账单号码
				String tHospitalCode = nRceiptData.getChildTextTrim("HOSIPITALNO"); //医院编码
				String tHosipitalName = nRceiptData.getChildTextTrim("HOSIPITALNAME"); // 医院名称
				String tHospStartDate = nRceiptData.getChildTextTrim("HOSPSTARTDATE"); // 住院日期
				String tHospEndDate = nRceiptData.getChildTextTrim("HOSPENDDATE"); //出院日期
				String tFeeDate = nRceiptData.getChildTextTrim("FEEDATE"); //结算日期
				String tRealHospDate = nRceiptData.getChildTextTrim("REALHOSPDATE"); // 住院天数
				String tMedicareType = nRceiptData.getChildTextTrim("MEDICARETYPE"); // 医保类型
				String tFeeAffixType = nRceiptData.getChildTextTrim("FEEAFFIXTYPE"); // 账单类型
				String tInpatientNo = nRceiptData.getChildTextTrim("INPATIENTNO"); //  住院号
				
			    mHospitalCode = tHospitalCode;
			    mHospitalName = tHosipitalName;
			    
				//校验账单种类
	            if (!"1".equals(tFeeType) && !"2".equals(tFeeType) && !"3".equals(tFeeType)) {
	                buildError("dealSecurityFee", "【账单类型】的值不存在");
	                return false;
	            }
	            
	            //校验医保类型
	            String tMedResult =LLCaseCommon.checkMedicareType(tMedicareType);
	            if(!"".equals(tMedResult)){
	            	buildError("dealSecurityFee", tMedResult);
	                return false;
	            }
	            
	          //住院日期校验
			    if(!"".equals(tHospStartDate) && tHospStartDate!=null){
			        if(!PubFun.checkDateForm(tHospStartDate)){
			    		buildError("dealHospFee","【住院日期】格式错误(正确格式yyyy-MM-dd)");
			    		return false;
			    	}
			     }else{
			    	 buildError("dealHospFee","【住院日期】不能为空");
			    	 return false;
			     }
			    //出院日期校验
			    if(!"".equals(tHospEndDate) && tHospEndDate !=null){
			    	if(!PubFun.checkDateForm(tHospEndDate)){
			    		buildError("dealHospFee","【出院日期】格式错误(正确格式yyyy-MM-dd)");
			    		return false;
			    	}
			     }else{
			    	 buildError("dealHospFee","【出院日期】不能为空");
			    	 return false; 
			     }
			    //账单日期校验
			    if(!"".equals(tFeeDate) && tFeeDate !=null){
			    	if(!PubFun.checkDateForm(tFeeDate)){
			    		buildError("dealHospFee","【出院日期】格式错误(正确格式yyyy-MM-dd)");
			    		return false;
			    	}
			     }else{
			    	 buildError("dealHospFee","【账单日期】不能为空");
			    	 return false; 
			     }
	            
	            //费用明细项
	            Element aFeeItemList = nRceiptData.getChild("FEEITEMLIST");
	            List tFeeItemData = new ArrayList();
	            tFeeItemData =(List)aFeeItemList.getChildren();
	            if(tFeeItemData.size()>0){
	            	for(int e=0;e<tFeeItemData.size();e++){
	            		Element tFeeItemInfo = (Element) tFeeItemData.get(e);
	            		
	            		String eFeeCode = tFeeItemInfo.getChildText("FEECODE");
	            		String eFeeName = tFeeItemInfo.getChildText("FEENAME");
	            		String eFee = tFeeItemInfo.getChildText("FEE");
       		
	            		if(!"".equals(eFeeCode) && eFeeCode!=null){
		            		double eMoney =Arith.round(Double.parseDouble(eFee),2);
		            		if(eMoney<=0){
		            			buildError("dealSecurityFee", "【费用项金额】的值必须大于0");
					   	        return false;
		            		}
		            		
		            		//找到费用项目对应名称  
				  	       	String tSQL = "select codename from ldcode where codetype ='llfeeitemtype' and code='"+eFeeCode+"' with ur ";
				  	       
				  	       	String tFeeitemName = mExeSQL.getOneValue(tSQL);
				  	       	if(!tFeeitemName.equals(eFeeName)){
				  	       		buildError("dealSecurityFee", "【费用项目代码】的值不存在");
				  	            return false;
				  	       	}
				  	       	
	
		            		LLCaseReceiptSchema eLLCaseReceiptSchema = new LLCaseReceiptSchema();
					 	    String tLimit1 = PubFun.getNoLimit("86");
					 	    eLLCaseReceiptSchema.setFeeDetailNo(
					 	             PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
					 	    eLLCaseReceiptSchema.setMainFeeNo(mMainFeeNo);
					 	    eLLCaseReceiptSchema.setRgtNo(mCaseNo);
					 	    eLLCaseReceiptSchema.setCaseNo(mCaseNo);
					 	    eLLCaseReceiptSchema.setFeeItemCode(eFeeCode);
					 	    eLLCaseReceiptSchema.setFeeItemName(eFeeName);
					 	    eLLCaseReceiptSchema.setFee(eFee);
				 	        eLLCaseReceiptSchema.setAvaliFlag("0");
				 	        eLLCaseReceiptSchema.setMngCom(mLLCaseSchema.getMngCom());
				 	        eLLCaseReceiptSchema.setOperator(mUserCode);
				 	        eLLCaseReceiptSchema.setMakeDate(aMakeDate);
				 	        eLLCaseReceiptSchema.setMakeTime(aMakeTime);
				 	        eLLCaseReceiptSchema.setModifyDate(aMakeDate);
				 	        eLLCaseReceiptSchema.setModifyTime(aMakeTime);
				 	        
				 	        mLLCaseReceiptSet.add(eLLCaseReceiptSchema);
	            		}
	            	}
	            }
	            
	            //社保明细
	            Element eSecuitemInfo = nRceiptData.getChild("SECUITEMDATA");   
	            String eApplyAmnt = eSecuitemInfo.getChildText("APPLYAMNT");  // 合计金额
	            String ePlanFee = eSecuitemInfo.getChildText("PLANFEE"); 	   //统筹基金支付
	            String eSupInhosFee = eSecuitemInfo.getChildText("SUPINHOSFEE");  //大额医疗支付  
	            String eOfficialSubsidy = eSecuitemInfo.getChildText("OFFICIALSUBSIDY");  //公务员医疗补助
	            String eSelfAmnt = eSecuitemInfo.getChildText("SELFAMNT");      //自费
	            String eSelfPay2 = eSecuitemInfo.getChildText("SELFPAY2");		//自付二
	            String eSelfPay1 = eSecuitemInfo.getChildText("SELFPAY1");		//自付一
	            String eGetLimit = eSecuitemInfo.getChildText("GETLIMIT");		//起付线
	            String eMidAmnt = eSecuitemInfo.getChildText("MIDAMNT");		//中段
	            String eHighAmnt1 = eSecuitemInfo.getChildText("HIGHAMNT1");	//高段1
	            String eHighAmnt2 = eSecuitemInfo.getChildText("HIGHAMNT2");	//高段2
	            String eSuperAmnt = eSecuitemInfo.getChildText("SUPERAMNT");	//超高段
	            String eRetireAddFee = eSecuitemInfo.getChildText("RETIREADDFEE");	//退休补充医疗
	            String eSmallDoorPay = eSecuitemInfo.getChildText("SMALLDOORPAY");	//小额门急诊
	            String eHighDoorAmnt = eSecuitemInfo.getChildText("HIGHDOORAMNT");	//大额门急诊
	            String eReimbursement = eSecuitemInfo.getChildText("REIMBURSEMENT");	//可报销范围内金额
	            String eStandbyAmnt = eSecuitemInfo.getChildText("STANDBYAMNT");		//不合理费用
	            String eOtherPay = eSecuitemInfo.getChildText("OTHERPAY");				//第三方支付金额
	            String eRemPay = eSecuitemInfo.getChildText("REMPAY");					//剩余赔付金额
	            
	            //查找费用项代码
	            if(!"".equals(eApplyAmnt)&& eApplyAmnt!=null && eApplyAmnt!="null" ){  //合计金额
        	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("301");
	            	eLLFeeOtherItemSchema.setFeeMoney(eApplyAmnt); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("301");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }

	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(ePlanFee)&& ePlanFee!=null && ePlanFee!="null" ){  //统筹基金支付
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("302");
	            	eLLFeeOtherItemSchema.setFeeMoney(ePlanFee); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("302");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            
	            if(!"".equals(eSupInhosFee)&& eSupInhosFee!=null && eSupInhosFee!="null" ){  //大额医疗支付
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("303");
	            	eLLFeeOtherItemSchema.setFeeMoney(eSupInhosFee); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("303");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eOfficialSubsidy)&& eOfficialSubsidy!=null && eOfficialSubsidy!="null" ){  //公务员医疗补助
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("304");
	            	eLLFeeOtherItemSchema.setFeeMoney(eOfficialSubsidy); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("304");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eSelfAmnt)&& eSelfAmnt!=null && eSelfAmnt!="null" ){  //自费
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("305");
	            	eLLFeeOtherItemSchema.setFeeMoney(eSelfAmnt); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("305");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eSelfPay2)&& eSelfPay2!=null && eSelfPay2!="null" ){  //自付二
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("306");
	            	eLLFeeOtherItemSchema.setFeeMoney(eSelfPay2); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("306");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eSelfPay1)&& eSelfPay1!=null && eSelfPay1!="null" ){  //自付一
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("307");
	            	eLLFeeOtherItemSchema.setFeeMoney(eSelfPay1); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("307");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eGetLimit)&& eGetLimit!=null && eGetLimit!="null" ){  //起付线
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("308");
	            	eLLFeeOtherItemSchema.setFeeMoney(eGetLimit); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("308");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eMidAmnt)&& eMidAmnt!=null && eMidAmnt!="null" ){  //中段
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("309");
	            	eLLFeeOtherItemSchema.setFeeMoney(eMidAmnt); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("309");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eHighAmnt1)&& eHighAmnt1!=null && eHighAmnt1!="null" ){  //高段1
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("310");
	            	eLLFeeOtherItemSchema.setFeeMoney(eHighAmnt1); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("310");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	
	            if(!"".equals(eHighAmnt2)&& eHighAmnt2!=null && eHighAmnt2!="null" ){  //高段2
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("311");
	            	eLLFeeOtherItemSchema.setFeeMoney(eHighAmnt2); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("311");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eSuperAmnt)&& eSuperAmnt!=null && eSuperAmnt!="null" ){  //超高段
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("312");
	            	eLLFeeOtherItemSchema.setFeeMoney(eSuperAmnt); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("312");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eRetireAddFee)&& eRetireAddFee!=null && eRetireAddFee!="null" ){  //退休补充医疗
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("313");
	            	eLLFeeOtherItemSchema.setFeeMoney(eRetireAddFee); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("313");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eSmallDoorPay)&& eSmallDoorPay!=null && eSmallDoorPay!="null" ){  //小额门急诊
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("314");
	            	eLLFeeOtherItemSchema.setFeeMoney(eSmallDoorPay); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("314");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eHighDoorAmnt)&& eHighDoorAmnt!=null && eHighDoorAmnt!="null" ){  //大额门急诊
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("315");
	            	eLLFeeOtherItemSchema.setFeeMoney(eHighDoorAmnt); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("315");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eReimbursement)&& eReimbursement!=null && eReimbursement!="null" ){  //可报销范围内金额
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("316");
	            	eLLFeeOtherItemSchema.setFeeMoney(eReimbursement); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("316");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eStandbyAmnt)&& eStandbyAmnt!=null && eStandbyAmnt!="null" ){  //不合理费用
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("317");
	            	eLLFeeOtherItemSchema.setFeeMoney(eStandbyAmnt); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("317");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eOtherPay)&& eOtherPay!=null && eOtherPay!="null" ){  //第三方支付金额
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("318");
	            	eLLFeeOtherItemSchema.setFeeMoney(eOtherPay); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("318");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            if(!"".equals(eRemPay)&& eRemPay!=null && eRemPay!="null" ){  //剩余赔付金额
	            	
	            	LLFeeOtherItemSchema eLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
	            	eLLFeeOtherItemSchema.setCDSerialNo(PubFun1.CreateMaxNo("FEEDETAILNO",mLimit));
	            	eLLFeeOtherItemSchema.setCaseNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setMainFeeNo(mMainFeeNo);
	            	eLLFeeOtherItemSchema.setRgtNo(mCaseNo);
	            	eLLFeeOtherItemSchema.setItemClass("S");
	            	eLLFeeOtherItemSchema.setAvliFlag("0");
	            	eLLFeeOtherItemSchema.setItemCode("319");
	            	eLLFeeOtherItemSchema.setFeeMoney(eRemPay); 
	            	eLLFeeOtherItemSchema.setMngCom(mLLCaseSchema.getMngCom());
	            	eLLFeeOtherItemSchema.setOperator(mUserCode);
	            	eLLFeeOtherItemSchema.setMakeDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setMakeTime(aMakeTime);
	            	eLLFeeOtherItemSchema.setModifyDate(aMakeDate);
	            	eLLFeeOtherItemSchema.setModifyTime(aMakeTime);
	            	
	            	LDCodeDB tLDCodeDB = new LDCodeDB();
		            tLDCodeDB.setCodeType("llsecufeeitem");
		            tLDCodeDB.setCode("319");

	                if (tLDCodeDB.getInfo()) {
	                	eLLFeeOtherItemSchema.setDrugName(tLDCodeDB.getCodeName());
	                	eLLFeeOtherItemSchema.setAvliReason(tLDCodeDB.getCodeAlias());
	                }
	            	
	            	mLLFeeOtherItemSet.add(eLLFeeOtherItemSchema);
	            }
	            
	            //社保账单
	            
	            LLSecurityReceiptSchema mSeReceiptSchema = new LLSecurityReceiptSchema();
	            mSeReceiptSchema.setFeeDetailNo(mMainFeeNo);  
	            mSeReceiptSchema.setMainFeeNo(mMainFeeNo);
	            mSeReceiptSchema.setRgtNo(mCaseNo);
	            mSeReceiptSchema.setCaseNo(mCaseNo);
	            mSeReceiptSchema.setApplyAmnt(eApplyAmnt);
	            mSeReceiptSchema.setPlanFee(ePlanFee);
	            mSeReceiptSchema.setSupInHosFee(eSupInhosFee);
	            mSeReceiptSchema.setOfficialSubsidy(eOfficialSubsidy);
	            mSeReceiptSchema.setSelfAmnt(eSelfAmnt);
	            mSeReceiptSchema.setSelfPay2(eSelfPay2);
	            mSeReceiptSchema.setSelfPay1(eSelfPay1);
	            mSeReceiptSchema.setGetLimit(eGetLimit);
	            mSeReceiptSchema.setLowAmnt(eGetLimit);
	            mSeReceiptSchema.setMidAmnt(eMidAmnt);
	            mSeReceiptSchema.setHighAmnt1(eHighAmnt1);
	            mSeReceiptSchema.setHighAmnt2(eHighAmnt2);
	            mSeReceiptSchema.setSuperAmnt(eSuperAmnt);
	            mSeReceiptSchema.setRetireAddFee(eRetireAddFee);
	            mSeReceiptSchema.setYearRetireAddFee(eRetireAddFee);
	            mSeReceiptSchema.setSmallDoorPay(eSmallDoorPay);
	            mSeReceiptSchema.setHighDoorAmnt(eHighDoorAmnt);
	            mSeReceiptSchema.setStandbyAmnt(eStandbyAmnt);
	            mSeReceiptSchema.setOtherPay(eOtherPay);
	            mSeReceiptSchema.setRemPay(eRemPay);
	            mSeReceiptSchema.setMakeDate(aMakeDate);
	            mSeReceiptSchema.setMakeTime(aMakeTime);
	            mSeReceiptSchema.setModifyDate(aMakeDate);
	            mSeReceiptSchema.setModifyTime(aMakeTime);
	            mSeReceiptSchema.setMngCom(mMngCom);
	            mSeReceiptSchema.setOperator(mUserCode);

	            mSeReceiptSet.add(mSeReceiptSchema);
	            
			    //扣除明细信息
			    Element tDrugList = nRceiptData.getChild("DRUGLIST");
			    List tDrugInfo = new ArrayList();
			    tDrugInfo =tDrugList.getChildren();
			    if(tDrugInfo.size()>0){
				    for(int b=0;b<tDrugInfo.size();b++){
				    	Element tDrugData = (Element) tDrugInfo.get(b);
				    	String aDrugCode = tDrugData.getChildText("DRUGCODE");
				    	String aDrugName = tDrugData.getChildText("DRUGNAME");
				    	String aDrugFee = tDrugData.getChildText("DRUGFEE");
				    	String aDrugSecuFee = tDrugData.getChildText("DRUGSECUFEE");
				    	String aDrugSelfPay2 = tDrugData.getChildText("DRUGSELFPAY2");
				    	String aDrugSelfFee = tDrugData.getChildText("DRUGSELFFEE");
				    	String aDrugUnReasonFee = tDrugData.getChildText("DRUGUNREASONFEE");
				    	String aDrugRemark = tDrugData.getChildText("DRUGREMARK");
				    	
				    	if((!"".equals(aDrugName) && aDrugName!=null) && (!"".equals(aDrugFee)&&aDrugFee!=null)){
						    	LLCaseDrugSchema aLLCaseDrugSchema = new LLCaseDrugSchema();
						    	String aDrugDetailNo = PubFun1.CreateMaxNo("DRUGDETAILNO", mLimit);
						    	aLLCaseDrugSchema.setDrugDetailNo(aDrugDetailNo);
						    	aLLCaseDrugSchema.setCaseNo(mCaseNo);
						    	aLLCaseDrugSchema.setRgtNo(mCaseNo);
						    	aLLCaseDrugSchema.setMainFeeNo(mMainFeeNo);
						    	aLLCaseDrugSchema.setDrugCode(aDrugCode);
						    	aLLCaseDrugSchema.setDrugName(aDrugName);
						    	aLLCaseDrugSchema.setFee(aDrugFee);
						    	aLLCaseDrugSchema.setSecuFee(aDrugSecuFee);
						    	aLLCaseDrugSchema.setSelfPay2(aDrugSelfPay2);
						    	aLLCaseDrugSchema.setSelfFee(aDrugSelfFee);
						    	aLLCaseDrugSchema.setUnReasonableFee(aDrugUnReasonFee);
						    	aLLCaseDrugSchema.setRemark(aDrugRemark);
						    	aLLCaseDrugSchema.setMngCom(mMngCom);
						    	aLLCaseDrugSchema.setOperator(mUserCode);
						    	aLLCaseDrugSchema.setMakeDate(aMakeDate);
						    	aLLCaseDrugSchema.setMakeTime(aMakeTime);
						    	aLLCaseDrugSchema.setModifyDate(aMakeDate);
						    	aLLCaseDrugSchema.setModifyTime(aMakeTime);
						    	
						    	mLLCaseDrugSet.add(aLLCaseDrugSchema);
				    	}
				    }
				    
			    }
				
			    //账单主表
			    LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
	    		tLLFeeMainSchema.setMainFeeNo(mMainFeeNo);
	    		tLLFeeMainSchema.setCaseNo(mCaseNo);
	    		tLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
	    		tLLFeeMainSchema.setRgtNo(mCaseNo);
	    		tLLFeeMainSchema.setFeeType(tFeeType);
	    		tLLFeeMainSchema.setFeeAtti(tFeeAttic); //账单属性为
	    		tLLFeeMainSchema.setFeeAffixType(tFeeAffixType);  //账单类型
	    		tLLFeeMainSchema.setInHosNo(tInpatientNo); //住院号
//	    		tLLFeeMainSchema.setAffixNo(mAffixNo); //总实赔金额
	    		tLLFeeMainSchema.setCustomerNo(mInsuredNo);
	    		tLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	    		tLLFeeMainSchema.setMedicareType(tMedicareType);
	    		tLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
	    		tLLFeeMainSchema.setHospitalCode(tHospitalCode);
	    		tLLFeeMainSchema.setHospitalName(tHosipitalName);
	    		tLLFeeMainSchema.setReceiptNo(tReceiptNo);
	    		tLLFeeMainSchema.setFeeDate(tFeeDate);
	    		tLLFeeMainSchema.setHospStartDate(tHospStartDate);
	    		tLLFeeMainSchema.setHospEndDate(tHospEndDate);
	    		tLLFeeMainSchema.setRealHospDate(tRealHospDate);
	    		tLLFeeMainSchema.setAge((int) mLLCaseSchema.getCustomerAge());
	  	        tLLFeeMainSchema.setSecurityNo(tLCInsuredSchema.getOthIDNo());
	  	        tLLFeeMainSchema.setInsuredStat(tLCInsuredSchema.getInsuredStat());
	  	        tLLFeeMainSchema.setSumFee(eApplyAmnt);       
	  	        tLLFeeMainSchema.setOperator(mUserCode);
	  	        tLLFeeMainSchema.setMngCom(mMngCom);
	  	        tLLFeeMainSchema.setMakeDate(aMakeDate);
	  	        tLLFeeMainSchema.setMakeTime(aMakeTime);
	  	        tLLFeeMainSchema.setModifyDate(aMakeDate);
	  	        tLLFeeMainSchema.setModifyTime(aMakeTime);
	  	        tLLFeeMainSchema.setReimbursement(eReimbursement);//可报销范围内金额

	  	        mLLFeeMainSet.add(tLLFeeMainSchema);
	  	        
		}else{
			 buildError("dealSecurityFee()", "获取理赔账单信息时出错!");
             mDealState = false;
             return false;
		}
		
		return true;
	}
	
	/**
	 * 账单属性为0-医院时
	 */
	
	private boolean dealHospFee(Element tReceiptData) throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--dealHospFee()");
    	
		System.out.println("账单属性为 0--医院时");
		if(tReceiptData!=null){
			
			String mMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
			String tFeeAttic = tReceiptData.getChildTextTrim("FEEATTIC"); //账单属性
			String tFeeType = tReceiptData.getChildTextTrim("FEETYPE");  // 账单种类
			String tReceiptNo = tReceiptData.getChildTextTrim("MAINFEENO"); //账单号码
			String tHospitalCode = tReceiptData.getChildTextTrim("HOSIPITALNO"); //医院编码
			String tHosipitalName = tReceiptData.getChildTextTrim("HOSIPITALNAME"); // 医院名称
			String tHospStartDate = tReceiptData.getChildTextTrim("HOSPSTARTDATE"); // 住院日期
			String tHospEndDate = tReceiptData.getChildTextTrim("HOSPENDDATE"); //出院日期
			String tFeeDate = tReceiptData.getChildTextTrim("FEEDATE"); //结算日期
			String tRealHospDate = tReceiptData.getChildTextTrim("REALHOSPDATE"); // 住院天数
			String tMedicareType = tReceiptData.getChildTextTrim("MEDICARETYPE"); // 医保类型
			String tFeeAffixType = tReceiptData.getChildTextTrim("FEEAFFIXTYPE"); // 账单类型
			String tInpatientNo = tReceiptData.getChildTextTrim("INPATIENTNO"); //  住院号
			//账单表存的汇总值 存账单表时使用
		    double mSumFee = 0.0;
		    double mPlanFee = 0.0;
		    double mSelfAmnt = 0.0;
		    double mRefuseAmnt = 0.0;
		    
		    mHospitalCode = tHospitalCode;
		    mHospitalName = tHosipitalName;
		    //校验账单种类
		    if (!"1".equals(tFeeType) && !"2".equals(tFeeType) && !"3".equals(tFeeType)) {
		        buildError("dealHospFee", "【账单类型】的值不存在");
		        return false;
		    }
		    
		    //校验医保类型
		    String tMedResult =LLCaseCommon.checkMedicareType(tMedicareType);
		    if(!"".equals(tMedResult)){
		    	buildError("dealHospFee", tMedResult);
		        return false;
		    }
		    
		    //住院日期校验
		    if(!"".equals(tHospStartDate) && tHospStartDate!=null){
		        if(!PubFun.checkDateForm(tHospStartDate)){
		    		buildError("dealHospFee","【住院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }else{
		    	 buildError("dealHospFee","【住院日期】不能为空");
		    	 return false;
		     }
		    //出院日期校验
		    if(!"".equals(tHospEndDate) && tHospEndDate !=null){
		    	if(!PubFun.checkDateForm(tHospEndDate)){
		    		buildError("dealHospFee","【出院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }else{
		    	 buildError("dealHospFee","【出院日期】不能为空");
		    	 return false; 
		     }
		    //账单日期校验
		    if(!"".equals(tFeeDate) && tFeeDate !=null){
		    	if(!PubFun.checkDateForm(tFeeDate)){
		    		buildError("dealHospFee","【出院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }else{
		    	 buildError("dealHospFee","【账单日期】不能为空");
		    	 return false; 
		     }
		    
		    //费用项目明细
		    Element aFeeitemList = tReceiptData.getChild("FEEITEMLIST");
		    List aFeeitemData =  new ArrayList();
		    aFeeitemData = aFeeitemList.getChildren();
		    for(int a=0;a<aFeeitemData.size();a++){
		    		Element tFeeitemData = (Element) aFeeitemData.get(a);
		    		if(tFeeitemData!=null){
			        	String tFeeitemCode = tFeeitemData.getChildText("FEEITEMCODE");
			        	String tFeeName = tFeeitemData.getChildText("FEEITEMNAME");
			        	String tSumFee = tFeeitemData.getChildText("SUMFEE");
			        	String tSelfAmnt = tFeeitemData.getChildText("SELFAMNT");
			        	String tPreAmnt = tFeeitemData.getChildText("PREAMNT");
			        	String tRefuseAmnt = tFeeitemData.getChildText("REFUSEAMNT");

			        	if(!"".equals(tFeeitemCode) && tFeeitemCode!=null){
			        		double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee),2) ;
				        	if(tSumFeeValue<=0){
				         	   buildError("dealHospFee", "【账单金额】的值必须大于0");
				   	           return false;
				         	}
				        	
				        	//找到费用项目对应名称  
				  	       String tSQL = "select codename   from ldcode where codetype ='llfeeitemtype' and code='"+tFeeitemCode+"' ";
				  	       
				  	       String tFeeitemName = mExeSQL.getOneValue(tSQL);
				  	       if("".equals(tFeeitemName)||null==tFeeitemName||"null".equals(tFeeitemName)||!tFeeName.equals(tFeeitemName)){
				  	       	 buildError("dealHospFee", "【费用项目代码】的值不存在");
				  	            return false;
				  	       }
				  	       
				  	     //费用明细信息
				 	       LLCaseReceiptSchema tLLCaseReceiptSchema = new LLCaseReceiptSchema();
				 	       String tLimit1 = PubFun.getNoLimit("86");
				 	       tLLCaseReceiptSchema.setFeeDetailNo(
				 	                PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
				 	       tLLCaseReceiptSchema.setMainFeeNo(mMainFeeNo);
				 	       tLLCaseReceiptSchema.setRgtNo(mCaseNo);
				 	       tLLCaseReceiptSchema.setCaseNo(mCaseNo);
				 	       tLLCaseReceiptSchema.setFeeItemCode(tFeeitemCode);
				 	       tLLCaseReceiptSchema.setFeeItemName(tFeeitemName);
				 	       tLLCaseReceiptSchema.setFee(tSumFee);
				 	       tLLCaseReceiptSchema.setSelfAmnt(tSelfAmnt);
				 	       tLLCaseReceiptSchema.setPreAmnt(tPreAmnt);
				 	       tLLCaseReceiptSchema.setRefuseAmnt(tRefuseAmnt);
				 	       tLLCaseReceiptSchema.setAvaliFlag("0");
					       tLLCaseReceiptSchema.setMngCom(mLLCaseSchema.getMngCom());
					       tLLCaseReceiptSchema.setOperator(mUserCode);
					       tLLCaseReceiptSchema.setMakeDate(aMakeDate);
					       tLLCaseReceiptSchema.setMakeTime(aMakeTime);
					       tLLCaseReceiptSchema.setModifyDate(aMakeDate);
					       tLLCaseReceiptSchema.setModifyTime(aMakeTime);
					       
					       mSumFee += tLLCaseReceiptSchema.getFee();
					       mPlanFee += tLLCaseReceiptSchema.getPreAmnt();
					       mSelfAmnt += tLLCaseReceiptSchema.getSelfAmnt();
					       mRefuseAmnt += tLLCaseReceiptSchema.getRefuseAmnt();
					       mLLCaseReceiptSet.add(tLLCaseReceiptSchema);
			    		}else{
			    			buildError("dealHospFee()", "账单属性为医院时，账单信息必录!");
			                mDealState = false;
			                return false;
		    		}
		        }
		    }	
		    //扣除明细信息
		    Element tDrugList = tReceiptData.getChild("DRUGLIST");
		    List tDrugInfo = new ArrayList();
		    tDrugInfo =tDrugList.getChildren();
		    if(tDrugInfo.size()>0){
			    for(int b=0;b<tDrugInfo.size();b++){
			    	Element tDrugData = (Element) tDrugInfo.get(b);
			    	String aDrugCode = tDrugData.getChildText("DRUGCODE");
			    	String aDrugName = tDrugData.getChildText("DRUGNAME");
			    	String aDrugFee = tDrugData.getChildText("DRUGFEE");
			    	String aDrugSecuFee = tDrugData.getChildText("DRUGSECUFEE");
			    	String aDrugSelfPay2 = tDrugData.getChildText("DRUGSELFPAY2");
			    	String aDrugSelfFee = tDrugData.getChildText("DRUGSELFFEE");
			    	String aDrugUnReasonFee = tDrugData.getChildText("DRUGUNREASONFEE");
			    	String aDrugRemark = tDrugData.getChildText("DRUGREMARK");
			    	
			    	if((!"".equals(aDrugName)&& aDrugName!=null) && (!"".equals(aDrugFee)&& aDrugFee!=null)){
			    	
					    	LLCaseDrugSchema aLLCaseDrugSchema = new LLCaseDrugSchema();
					    	String aDrugDetailNo = PubFun1.CreateMaxNo("DRUGDETAILNO", mLimit);
					    	aLLCaseDrugSchema.setDrugDetailNo(aDrugDetailNo);
					    	aLLCaseDrugSchema.setCaseNo(mCaseNo);
					    	aLLCaseDrugSchema.setRgtNo(mCaseNo);
					    	aLLCaseDrugSchema.setMainFeeNo(mMainFeeNo);
					    	aLLCaseDrugSchema.setDrugCode(aDrugCode);
					    	aLLCaseDrugSchema.setDrugName(aDrugName);
					    	aLLCaseDrugSchema.setFee(aDrugFee);
					    	aLLCaseDrugSchema.setSecuFee(aDrugSecuFee);
					    	aLLCaseDrugSchema.setSelfPay2(aDrugSelfPay2);
					    	aLLCaseDrugSchema.setSelfFee(aDrugSelfFee);
					    	aLLCaseDrugSchema.setUnReasonableFee(aDrugUnReasonFee);
					    	aLLCaseDrugSchema.setRemark(aDrugRemark);
					    	aLLCaseDrugSchema.setMngCom(mMngCom);
					    	aLLCaseDrugSchema.setOperator(mUserCode);
					    	aLLCaseDrugSchema.setMakeDate(aMakeDate);
					    	aLLCaseDrugSchema.setMakeTime(aMakeTime);
					    	aLLCaseDrugSchema.setModifyDate(aMakeDate);
					    	aLLCaseDrugSchema.setModifyTime(aMakeTime);
					    	
					    	mLLCaseDrugSet.add(aLLCaseDrugSchema);
			    	}
			    }
			    
		    }
		    
		 // 账单信息
    		LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
    		tLLFeeMainSchema.setMainFeeNo(mMainFeeNo);
    		tLLFeeMainSchema.setCaseNo(mCaseNo);
    		tLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
    		tLLFeeMainSchema.setRgtNo(mCaseNo);
    		tLLFeeMainSchema.setFeeType(tFeeType);
    		tLLFeeMainSchema.setFeeAtti(tFeeAttic); //账单属性为
    		tLLFeeMainSchema.setFeeAffixType(tFeeAffixType);  //账单类型
    		tLLFeeMainSchema.setInHosNo(tInpatientNo); //住院号
//    		tLLFeeMainSchema.setAffixNo(mAffixNo); //总实赔金额
    		tLLFeeMainSchema.setCustomerNo(mInsuredNo);
    		tLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
    		tLLFeeMainSchema.setMedicareType(tMedicareType);
    		tLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
    		tLLFeeMainSchema.setHospitalCode(tHospitalCode);
    		tLLFeeMainSchema.setHospitalName(tHosipitalName);
    		tLLFeeMainSchema.setReceiptNo(tReceiptNo);
    		tLLFeeMainSchema.setFeeDate(tFeeDate);
    		tLLFeeMainSchema.setHospStartDate(tHospStartDate);
    		tLLFeeMainSchema.setHospEndDate(tHospEndDate);
    		tLLFeeMainSchema.setRealHospDate(tRealHospDate);
    		tLLFeeMainSchema.setAge((int) mLLCaseSchema.getCustomerAge());
  	        tLLFeeMainSchema.setSecurityNo(tLCInsuredSchema.getOthIDNo());
  	        tLLFeeMainSchema.setInsuredStat(tLCInsuredSchema.getInsuredStat());
  	        tLLFeeMainSchema.setSumFee(mSumFee);
  	        tLLFeeMainSchema.setPreAmnt(mPlanFee);
  	        tLLFeeMainSchema.setSelfAmnt(mSelfAmnt);
  	        tLLFeeMainSchema.setRefuseAmnt(mRefuseAmnt);
  	        
  	        tLLFeeMainSchema.setOperator(mUserCode);
  	        tLLFeeMainSchema.setMngCom(mMngCom);
  	        tLLFeeMainSchema.setMakeDate(aMakeDate);
  	        tLLFeeMainSchema.setMakeTime(aMakeTime);
  	        tLLFeeMainSchema.setModifyDate(aMakeDate);
  	        tLLFeeMainSchema.setModifyTime(aMakeTime);

  	        mLLFeeMainSet.add(tLLFeeMainSchema);
		     
        }else{
        	 buildError("dealHospFee()", "获取理赔账单信息时出错!");
             mDealState = false;
             return false;
        }
	    
		return true;
	}
	
	
	/**
	 * 解析事件信息
	 * 
	 */
	private boolean getAccRegister(Element oAccData) throws Exception{
		 System.out.println("LLTYAutoCaseRegister--统一接口--getAccRegister()");
		 
		 //深水摸鱼，开始生成事件
		 String aAccDate = oAccData.getChildText("ACCDATE");   //发生日期
		 String aAccDescType = oAccData.getChildText("ACCDESCTYPE");  //事件类型
		 String aAccProvinceCode = oAccData.getChildText("ACCPROVINCECODE"); //省
	     String aAccCityCode = oAccData.getChildText("ACCCITYCODE");  //市
	     String aAccCountyCode = oAccData.getChildText("ACCCOUNTYCODE"); //县
	     String aAccPlace = oAccData.getChildText("ACCPLACE");  //发生地点
	     String aAccDesc = oAccData.getChildText("ACCDESC");    //事件信息
	     String aInhospitalDate = oAccData.getChildText("INHOSPITALDATE");  //入院日期
	     String aOuthospitalDate = oAccData.getChildText("OUTHOSPITALDATE"); //出院日期
	    
	     mAccDate = aAccDate;
	     mAccDescType = aAccDescType;
	     	
	       if(!PubFun.checkDateForm(aAccDate)){
	    		buildError("getAccRegister","【发生日期】格式错误(正确格式yyyy-MM-dd)");
	    		return false;
	    	}
	     
		   if(!"".equals(aInhospitalDate) && aInhospitalDate!=null){
		        if(!PubFun.checkDateForm(aInhospitalDate)){
		    		buildError("getAccRegister","【住院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }
		    if(!"".equals(aOuthospitalDate) && aOuthospitalDate !=null){
		    	if(!PubFun.checkDateForm(aOuthospitalDate)){
		    		buildError("getAccRegister","【出院日期】格式错误(正确格式yyyy-MM-dd)");
		    		return false;
		    	}
		     }
	    	
	    	String tAccReason =LLCaseCommon.checkAccPlace(aAccProvinceCode, aAccCityCode, aAccCountyCode);
	    	if(!"".equals(tAccReason)){
	    		buildError("getAccRegister", tAccReason);
	    		return false;
	    	}
		
	    	LLSubReportSchema aLLSubReportSchema = new LLSubReportSchema();
			// 事件信息
	    	String aSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", mLimit);
	    	mSubRptNo = aSubRptNo;
	    	aLLSubReportSchema.setSubRptNo(aSubRptNo);
	    	aLLSubReportSchema.setAccDate(aAccDate);
	    	aLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
	    	aLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
	    	aLLSubReportSchema.setInHospitalDate(aInhospitalDate);
	    	aLLSubReportSchema.setOutHospitalDate(aOuthospitalDate);
	    	aLLSubReportSchema.setAccPlace(aAccPlace);
	    	aLLSubReportSchema.setAccProvinceCode(aAccProvinceCode);
	    	aLLSubReportSchema.setAccCityCode(aAccCityCode);
	    	aLLSubReportSchema.setAccCountyCode(aAccCountyCode);
	    	aLLSubReportSchema.setAccDesc(aAccDesc);
	    	aLLSubReportSchema.setAccidentType(aAccDescType);
	        aLLSubReportSchema.setOperator(mUserCode);
	        aLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());
	        aLLSubReportSchema.setMakeDate(aMakeDate);
	        aLLSubReportSchema.setMakeTime(aMakeTime);
	        aLLSubReportSchema.setModifyDate(aMakeDate);
	        aLLSubReportSchema.setModifyTime(aMakeTime);
	        
	        mLLSubReportSet.add(aLLSubReportSchema);
	        
	        System.out.println("客户事件已录入成功，待输入数据库。。。");
	        //事件关联信息
	        LLCaseRelaSchema aLLCaseRelaSchema = new LLCaseRelaSchema();
	        
	        mCaseRelaNo= PubFun1.CreateMaxNo("CASERELANO", mLimit);
	        aLLCaseRelaSchema.setCaseNo(mCaseNo);
	        aLLCaseRelaSchema.setSubRptNo(aSubRptNo);
	        aLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
	        
	    	mLLCaseRelaSet.add(aLLCaseRelaSchema);
		 
		return true;
	}
	
	/**
	 * 生成案件理赔信息
	 * 
	 */
	private boolean CaseRegister() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--CaseRegister()");
		
      //银行信息
        mCaseGetMode = mCustomerData.getChildText("CASEGETMODE");
        mBankNo = mCustomerData.getChildText("BANKNO");
    	mAccNo = mCustomerData.getChildText("ACCNO");
    	mAccName = mCustomerData.getChildText("ACCNAME");
    	mRelaDrawerInsured = mCustomerData.getChildText("RELADRAWERINSURED"); 
    	mTotalAmount = mCustomerData.getChildText("TOTALAMOUNT");
    	mDeclineReason = mCustomerData.getChildText("DECLINEREASON");

    	
    	if(!"1".equals(mCaseGetMode) && !"2".equals(mCaseGetMode)){
    		//领取方式非现金时，领款人与被保人关系非空
    		if("".equals(mRelaDrawerInsured)||mRelaDrawerInsured==null||mRelaDrawerInsured=="null" ){
    			buildError("caseRegister","当收益金领取方式非现金时领款人与被保人关系必录");
        		return false;
    		}
    		if((!"".equals(mAccNo) && mAccNo!=null) &&(!"".equals(mBankNo) && mBankNo!=null)){
    			Pattern tPattern = Pattern.compile("(\\d)+");
    			Matcher tNum= tPattern.matcher(mAccNo);
    			if(!tNum.matches()){
    			buildError("caseRegister","【银行账号】只能是数字组成。");
        		return false;
    			}
    		}else{
    			buildError("caseRegister","银行信息不能为空。");
        		return false;
    		}
    	}
    	
        mLimit = PubFun.getNoLimit(mMngCom);
        mCaseNo = PubFun1.CreateMaxNo("CaseNo", mLimit);
        
        //申请信息
        if(!getRgtInfo()){
        	return  false;
        }
        
        if(!getCaseInfo()){
        	return false;
        }
        //受理申请原因
        
        if(!getReasonInfo()){
        	return false;
        }
        
		return true;
	}
	
	/**
	 * 理赔受理申请原因
	 * 
	 */
	private boolean getReasonInfo() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getReasonInfo()");
		
		 //申请原因
	       String tReasonCode = mCustomerData.getChildText("REASONCODE");
	       if(!"".equals(tReasonCode) && tReasonCode!=null){
	    	   mLLAppClaimReasonSchema.setCaseNo(mCaseNo);
	    	   mLLAppClaimReasonSchema.setRgtNo(mCaseNo);
	    	   mLLAppClaimReasonSchema.setReasonCode(tReasonCode);
	    	   mLLAppClaimReasonSchema.setReasonType("0"); // 原因类型
	    	   mLLAppClaimReasonSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
	    	   mLLAppClaimReasonSchema.setMngCom(mLLCaseSchema.getMngCom());
	    	   mLLAppClaimReasonSchema.setOperator(mLLCaseSchema.getOperator());
	    	   mLLAppClaimReasonSchema.setMakeDate(aMakeDate);
	    	   mLLAppClaimReasonSchema.setMakeTime(aMakeTime);
	    	   mLLAppClaimReasonSchema.setModifyDate(aMakeDate);
	    	   mLLAppClaimReasonSchema.setModifyTime(aMakeTime);

	       }
		
		return true;
	}
	
	/**
	 * 理赔案件信息
	 * 
	 */
	private boolean getCaseInfo() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getCaseInfo()");
		
		mLLCaseSchema.setCaseNo(mCaseNo);
		mLLCaseSchema.setRgtNo(mCaseNo);
        //申请类案件
        mLLCaseSchema.setRgtType("1");
        //案件状态处理到理算确认
        mLLCaseSchema.setRgtState("01");
        mLLCaseSchema.setCustomerNo(mInsuredNo);
        mLLCaseSchema.setCustomerName(tLCInsuredSchema.getName());
        //账单状态录入
        mLLCaseSchema.setReceiptFlag("1");
        //未调查
        mLLCaseSchema.setSurveyFlag("0");
        mLLCaseSchema.setRgtDate(aMakeDate);
        mLLCaseSchema.setCustBirthday(tLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerSex(tLCInsuredSchema.getSex());

        int tAge = LLCaseCommon.calAge(tLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerAge(tAge);
        
        mLLCaseSchema.setIDType(tLCInsuredSchema.getIDType());
        mLLCaseSchema.setIDNo(tLCInsuredSchema.getIDNo());
        mLLCaseSchema.setHandler(mUserCode);
        mLLCaseSchema.setDealer(mUserCode); 
        //银行信息
        mLLCaseSchema.setCaseGetMode(mCaseGetMode);
        mLLCaseSchema.setBankCode(mBankNo);
        mLLCaseSchema.setBankAccNo(mAccNo);
        mLLCaseSchema.setAccName(mAccName);        
        mLLCaseSchema.setMngCom(mMngCom);
        String tMobilePhone = mCustomerData.getChildText("MOBILEPHONE");
        mLLCaseSchema.setMobilePhone(tMobilePhone);
        mLLCaseSchema.setOperator(mUserCode);
        mLLCaseSchema.setMakeDate(aMakeDate);
        mLLCaseSchema.setMakeTime(aMakeTime);
        mLLCaseSchema.setModifyDate(aMakeDate);
        mLLCaseSchema.setModifyTime(aMakeTime);
        
        mLLCaseSchema.setRigister(mUserCode);
        mLLCaseSchema.setClaimer(mUserCode);
        mLLCaseSchema.setOtherIDType(tLCInsuredSchema.getOthIDType());
        mLLCaseSchema.setOtherIDNo(tLCInsuredSchema.getOthIDNo());
        
        //案件扩展表
        if(!"".equals(mRelaDrawerInsured) && mRelaDrawerInsured!=null){
        	mLLCaseExtSchema.setCaseNo(mCaseNo);
        	mLLCaseExtSchema.setRelaDrawerInsured(mRelaDrawerInsured);
        	mLLCaseExtSchema.setMngCom(mLLCaseSchema.getMngCom());
        	mLLCaseExtSchema.setOperator(mUserCode);
        	mLLCaseExtSchema.setMakeDate(aMakeDate);
        	mLLCaseExtSchema.setMakeTime(aMakeTime);
        	mLLCaseExtSchema.setModifyDate(aMakeDate);
        	mLLCaseExtSchema.setModifyTime(aMakeTime);     	
        }
		
		return true;
		
	}
	
	/**
	 * 理赔受理申请信息
	 * 
	 */
	private boolean getRgtInfo() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getRgtInfo()");
		
		//解析申请人信息
		String tRgtantName = mCustomerData.getChildText("RGTANTNAME");   
		String tRgtantIdNo = mCustomerData.getChildText("RGTANTIDNO");
		String tRgtantIdType = mCustomerData.getChildText("RGTANTIDTYPE");
		String tRgtantPhone = mCustomerData.getChildText("RGTANTPHONE");
        String tRgtantEmail = mCustomerData.getChildText("RGTANTEMAIL");
        String tRgtantAddress = mCustomerData.getChildText("RGTANTADDRESS");
        String tRgtantRelation = mCustomerData.getChildText("RGTANTRELATION");
        
        if(tRgtantIdType.equals("0")||tRgtantIdType.equals("5")){
        	if(!"".equals(PubFun.CheckIDNo(tRgtantIdType, tRgtantIdNo, "", ""))){
        		buildError("getRgtInfo","【申请人身份证号】不符合规则");
        		return false;
        	}
        }
		
		mLLRegisterSchema.setRgtNo(mCaseNo);
        mLLRegisterSchema.setRgtState("13");
        //客户号
        mLLRegisterSchema.setRgtObj("1");
        mLLRegisterSchema.setRgtObjNo(mInsuredNo);
        //受理方式为 12-外包平台
        mLLRegisterSchema.setRgtType("12");
        //个案受理
        mLLRegisterSchema.setRgtClass("0");
        mLLRegisterSchema.setRgtantName(tRgtantName);
        mLLRegisterSchema.setRelation(tRgtantRelation);
        //银行信息
    	mLLRegisterSchema.setBankCode(mBankNo);
    	mLLRegisterSchema.setAccName(mAccName); 
      	mLLRegisterSchema.setBankAccNo(mAccNo); 
      	mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
      	//申请日期
        mLLRegisterSchema.setAppDate(aMakeDate);
        mLLRegisterSchema.setCustomerNo(mInsuredNo);
        mLLRegisterSchema.setRgtDate(aMakeDate);
        mLLRegisterSchema.setHandler(mUserCode);
        mLLRegisterSchema.setMngCom(mMngCom);
        mLLRegisterSchema.setIDType(tRgtantIdType);
        mLLRegisterSchema.setIDNo(tRgtantIdNo);
        mLLRegisterSchema.setRgtantAddress(tRgtantAddress);
        mLLRegisterSchema.setRgtantMobile(tRgtantPhone);
        mLLRegisterSchema.setEmail(tRgtantEmail);
        mLLRegisterSchema.setOperator(mUserCode);
        mLLRegisterSchema.setMakeDate(aMakeDate);
        mLLRegisterSchema.setMakeTime(aMakeTime);
        mLLRegisterSchema.setModifyDate(aMakeDate);
        mLLRegisterSchema.setModifyTime(aMakeTime);
		
		return true;
	}
	
	
	/**
	 * 校验案件基本信息
	 * 
	 */
	private boolean dealBasicData() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--dealBasicData()");
		
		//校验理赔用户
		LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(mUserCode);
    	tLLClaimUserDB.setClaimDeal("1");  //理赔权限
    	//取消用户分配权限的校验
    	//tLLClaimUserDB.setHandleFlag("1"); //案件分配权限
    	tLLClaimUserDB.setStateFlag("1"); //理赔员状态
    	
    	LLClaimUserSet tLLClaimUserSet =tLLClaimUserDB.query();
    	if(tLLClaimUserSet.size()<=0){
    		buildError("dealBasicData", "案件处理人查询失败或没有权限");
    		return false;
    	}
    	
    	String aComCode = tLLClaimUserSet.get(1).getComCode();
    	String tMngCom ="";
    	if(mMngCom.length()==4|| mMngCom.length()==8 ||mMngCom.length()==6){
    		 tMngCom =mMngCom.substring(0, 4);
    	}else{
    		buildError("dealBasicData", "案件处理人机构不能为俩位机构。");
    		return false;	
    	}
    	
    	
    	if(!"".equals(aComCode) && aComCode!=null){
    		if(aComCode.length()==4){
    			if(!aComCode.equals(tMngCom)){
    				buildError("dealBasicData", "案件处理人机构和报文机构不匹配。");
    	    		return false;	
    			}
    		}else if(aComCode.length()==8 ||aComCode.length()==6){
    			String newComCode = aComCode.substring(0, 4);
    			if(!newComCode.equals(tMngCom)){
    				buildError("dealBasicData", "案件处理人机构和报文机构不匹配。");
    	    		return false;	
    			}
    		}else{
    			buildError("dealBasicData", "案件处理人机构不能为俩位机构。");
	    		return false;	
    		}
    	}else{
    		buildError("dealBasicData", "案件处理人机构不能为空。");
    		return false;
    	}
    	
    	
    	//平台理赔号校验
    	mVersionCode = mVersionData.getChildText("VERSIONCODE");
    	mClaimNo = mCustomerData.getChildText("CLAIMNO");
    	String tClaimSql = "select * from llhospcase where claimno='"+mClaimNo+"' and hcno='"+mVersionCode+"'  and hospitcode='"+mRequestType+"'  with ur";
    	SSRS tSClaim =mExeSQL.execSQL(tClaimSql);
    	if(tSClaim.getMaxRow()>0){
    		buildError("dealBasicData()", "平台理赔号为："+mClaimNo+"的数据信息已重复使用。");
        	System.out.println("===平台理赔号出现重复，表中已存在，交易编码："+mTransactionNum+"，平台理赔号："+mClaimNo+"===");
        	return false;
    	}
    	
    	//被保人校验
    	String aInsuredName = mCustomerData.getChildText("INSUREDNAME");
    	String aIdNo = mCustomerData.getChildText("IDNO");
    	mInsuredNo = mCustomerData.getChildText("INSUREDNO");
    	
    	// 获取保单号
    	Element aAccData = (Element) mAccList.getChildren("ACC_DATA").get(0);
    	mPayList =aAccData.getChild("PAYLIST");
    	List aContList = new ArrayList();
    	aContList = (List)mPayList.getChildren("PAYDATA");
    	Element aPayData = (Element) aContList.get(0); //任意获取一个
    	String aContNo = aPayData.getChildText("CONTNO");
    	//如果客户号不为空，根据客户号和保单号去被保人表取值，否则根据身份证号和保单号去取值
    	if(!"".equals(mInsuredNo)&& mInsuredNo!=null){
    		LCInsuredBL tLCInsuredBL = new LCInsuredBL();
    		tLCInsuredBL.setInsuredNo(mInsuredNo);
    		tLCInsuredBL.setContNo(aContNo);
    		tLCInsuredBL.setName(aInsuredName);
    		
    		if(!tLCInsuredBL.getInfo()){
    			buildError("dealBasicData", "被保险人查询失败");
	    		return false;
    		}
    		
    		tLCInsuredSchema =tLCInsuredBL.getSchema();

    	}else{
    		String tInsuredSQL = "select a.insuredno from lcinsured a where a.name='"+aInsuredName+"' and "
    				+ "a.idno='"+aIdNo+"' and a.contno='"+aContNo+"' union select a.insuredno from lbinsured "
    				+ "a where a.name='"+aInsuredName+"' and a.idno='"+aIdNo+"' and a.contno='"+aContNo+"' with ur";
    		String aInsuredNo = mExeSQL.getOneValue(tInsuredSQL);
    		if("".equals(aInsuredNo) ||aInsuredNo==null){
    			buildError("dealBasicData", "被保险人查询失败");
	    		return false;
    		}else{
	    		LCInsuredBL tLCInsuredBL = new LCInsuredBL();
		    	tLCInsuredBL.setInsuredNo(aInsuredNo);
		    	tLCInsuredBL.setName(aInsuredName);
		    	tLCInsuredBL.setContNo(aContNo);
		    	
		    	if(!tLCInsuredBL.getInfo()){
		    		buildError("dealBasicData", "被保险人查询失败");
		    		return false;
		    	}
		    	
		    	tLCInsuredSchema =tLCInsuredBL.getSchema();
		    	mInsuredNo=tLCInsuredSchema.getInsuredNo();  //获取被保人客户号
    	 }
    	}
    	
    	//校验是否有未完成理赔案件
    	String tCaseSql ="select * from llcase where customerno='"+mInsuredNo+"' and "
    			+ "rgtstate not in('11','12','14') with ur ";
    	SSRS tSCase =mExeSQL.execSQL(tCaseSql);
    	if(tSCase.getMaxRow()>0){
    		buildError("dealBasicData", "客户" + mInsuredNo + "有未处理完的理赔案件，不能进行理赔。");
            return false;
    	}
    	
    	if(!"".equals(LLCaseCommon.checkPerson(mInsuredNo))){
    		buildError("dealBasicData", "该客户正在进行保全处理，不能进行理赔");
            return false;
    	}
    	
    	
		return true;
	}
	
	
	/**
	 * 校验报文数据
	 * 
	 */
	private boolean checkData() throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--checkData()");
		
		if(!"REQUEST".equals(mDocType)){
			buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
		}
		if(!"TY02".equals(mRequestType)){
			 buildError("checkData()", "【请求类型】的值不存在或匹配错误");
             return false;
		}
    	if(mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
	            mTransactionNum.length() != 30) {
        buildError("checkData()", "【交互编码】的编码个数错误");
        return false;
    	}
    	if(!"TY02".equals(mTransactionNum.substring(0, 4))){
    		buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
    	}
    	
		return true;
	}
	
	
	
	/**
	 * 解析报文
	 */
	private boolean getInputData(Document aInXml) throws Exception{
		System.out.println("LLTYAutoCaseRegister--统一接口--getInputData()");
		
		if(aInXml==null){
			buildError("getInputData()","未获取到报文信息");
			return false;
		}
		
		Element tRootData = aInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");
		//解析HEAD部分
		Element tHeadData = tRootData.getChild("HEAD");
		mRequestType = tHeadData.getChildText("REQUEST_TYPE");
		mTransactionNum = tHeadData.getChildText("TRANSACTION_NUM");
		
		//解析BODY部分
		Element tBodyData = tRootData.getChild("BODY");
		mVersionData = tBodyData.getChild("VERSION_DATA");  //版本号
		mPictureData = tBodyData.getChild("PICTURE_DATA");  //影像件路径
		mUserData = tBodyData.getChild("USER_DATA");        //理赔用户
		mCustomerData = tBodyData.getChild("CUSTOMER_DATA"); //被保人信息
		mAccList = mCustomerData.getChild("ACCLIST");        //事件信息
		
		//解析理赔用户
		mUserCode = mUserData.getChildText("USERCODE");
		mMngCom = mUserData.getChildText("MANAGECOM").trim();
	
		
		mG.Operator = mUserCode;
		mG.ManageCom = mMngCom;
		
		System.out.println("LLTYAutoCaseRegister--报文解析完毕。。");
		return true;
	}
	
	/***
	 * 错误信息
	 * @param args
	 */
	private void buildError(String szFunc,String szErrMsg){
		CError tCError = new CError();
		tCError.moduleName="LLTYAutoCaseRegister";
		tCError.functionName=szFunc;
		tCError.errorMessage=szErrMsg;
		System.out.println(szFunc+"--"+szErrMsg);
		this.mCErrors.addOneError(tCError);
	}
	
	 /***
     * 赔付结论
     * 
     */
    private String sumGiveType(String fGiveType, String sGiveType) {
        if (fGiveType.equals("")) {
            return sGiveType;
        }
        if (sGiveType.equals("")) {
            return fGiveType;
        }
        if (fGiveType.equals("4") || fGiveType.equals("5")) {
            return fGiveType;
        }
        if (sGiveType.equals("4") || sGiveType.equals("5")) {
            return sGiveType;
        }
        boolean fRefuse = fGiveType.equals("3");
        boolean sRefuse = sGiveType.equals("3");
        if (fRefuse && sRefuse) {
            return "3"; //全拒
        }
        if (fRefuse || sRefuse) {
            return "2"; //拒一个
        }
        if (fGiveType.equals("2") || sGiveType.equals("2")) {
            return "2";
        } else {
            return "1";
        }
    }
    
    
    /***
     * 赔付结论原因
     * 
     */
    private String getGiveDesc(String agivetype) {
        String agivedesc = "";
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llclaimdecision");
        tLDCodeDB.setCode(agivetype);
        if (tLDCodeDB.getInfo()) {
            agivedesc = tLDCodeDB.getCodeName();
        }
        return agivedesc;
    }
	
	public static void main(String[] args) {
	     Document tInXmlDoc;    
	        try {
	            tInXmlDoc = JdomUtil.build(new FileInputStream("E:\\TEST\\发送报文-DSJK_003-1.2.xml"), "GBK");
	            LLTYAutoCaseRegister tBusinessDeal = new LLTYAutoCaseRegister();
	            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
	            System.out.println("打印传入报文============");
	            JdomUtil.print(tInXmlDoc.getRootElement());
	            System.out.println("打印传出报文============");
	            JdomUtil.print(tOutXmlDoc);
	        	        	
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	}

}
