package com.sinosoft.lis.yibaotong;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.jdom.Element;

import com.sinosoft.lis.certifybusiness.CommonBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.wasforwardxml.project.pad.obj.FileInfoForm;
import com.sinosoft.wasforwardxml.project.pad.util.AnimatedGifEncoder;
import com.sinosoft.wasforwardxml.project.pad.util.DeCompressZip;
import com.sinosoft.wasforwardxml.project.pad.util.IOTrans;
import com.sinosoft.wasforwardxml.project.pad.util.IndexMap;
import com.sinosoft.wasforwardxml.project.pad.util.PraseXmlUtil;
import com.sinosoft.xreport.dl.test;

public class LLPadDealScanImages {

	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null; // 用户信息

	private String mCaseNo = "";

	private TransferData mTransferData;

	private String mZipLocalPath;

	private String mZipName;
	
//	private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
	
 	private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();
	
	private ES_DOC_RELATIONSet mES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();

	private String mManageCom = "";
	
	private String mOperator = "";

	private String mCurrentDate = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private VData mResult = new VData();

	private static final long serialVersionUID = 1L;

	private ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();

	private ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();

	private ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

	private MMap map = new MMap();
	
	private IndexMap iMap = new IndexMap();
	
	private String HostName = "";

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		try {
			if (!deal(mZipLocalPath, mZipName)) {
				return false;
			}else{
                //保存信息到LLHospCase
                PubSubmit tPubSubmit = new PubSubmit();
                VData mVData = new VData();
                MMap tMMap= new MMap();
                tMMap.put(mES_DOC_RELATIONSet, "INSERT");
                tMMap.put(mES_DOC_MAINSet, "INSERT");
                tMMap.put(mES_DOC_PAGESSet, "INSERT");
                mVData.add(tMMap);
                if(!tPubSubmit.submitData(mVData, "")){	 
                	 mErrors.addOneError("数据提交报错");
                }
        	}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	private boolean getInputData(VData inputData) {
		mGI = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
		mTransferData = (TransferData) inputData.getObjectByObjectName(
				"TransferData", 0);
		mCaseNo = (String)mTransferData.getValueByName("CaseNo");
		mZipLocalPath = (String) mTransferData.getValueByName("ZipLocalPath");
		mZipName = (String) mTransferData.getValueByName("ZipName");
		mOperator = mGI.Operator;
		mManageCom = mGI.ManageCom;
		System.out.println("上传扫描件案件号为：" + mCaseNo);
		return true;
	}

	private boolean deal(String path, String file) throws Exception {

		System.out.println("path:" + path + " ,file:" + file + ",CaseNo:"
				+ mCaseNo);
		String dir = path + file;// 完整路径

		boolean flag = true;

		
		// 1、解压zip包
		String unRar="select codename,codealias from ldcode where codetype='LLPadScan' and code='UrlPicPath' ";
		
		SSRS tPathSSRS = new ExeSQL().execSQL(unRar);
		if (tPathSSRS.getMaxRow() < 1) {
			CError tError = new CError();
			tError.moduleName = "LLPadDealScanImages";
			tError.functionName = "getConn";
			tError.errorMessage = "未设置文件解压路径";
			mErrors.addOneError(tError);
			return false;
		}
		String tUIRoot = CommonBL.getUIRoot();
		String unpath = tUIRoot+tPathSSRS.GetText(1, 1);
		unpath = unpath+PubFun.getCurrentDate()+"/"+mCaseNo+"/";
		HostName = tPathSSRS.GetText(1, 2);
//		unpath = "E:\\FTP文件\\LLPADScan\\"+PubFun.getCurrentDate()+"/"+mCaseNo+"/";
//		HostName = "本地";//本地调试
		if (!newFolder(unpath))
        {
			mErrors.addOneError("新建保存扫描件目录失败");
            return false;
        }		
		flag = unZip(dir, unpath);
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "LLPadDealScanImages";
			tError.functionName = "unZip";
			tError.errorMessage = "文件解压失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("**************** ③解压zip包完毕 ****************");

		// 2、解析xml文件 生成扫描一套表
//		unpath = unpath + mcaseNo;
//		String fileDetail = unpath + "/" + mcaseNo + ".xml";
//		
//	    if(!parseXml(fileDetail)){
//	    	deletefile(unpath);
//			deleteFiles(path, file);
//	    	return false;
//	    }
//		String tPrtno = (String) iMap.get("PRTNO");
//		String tManageCom = (String) iMap.get("MANAGECOM");
//		if (tPrtno.equals("") || !tPrtno.equals(mcaseNo)) {
//			CError tError = new CError();
//			tError.moduleName = "DealFile";
//			tError.functionName = "deal";
//			tError.errorMessage = "解析xml文件失败 或 扫描件与印刷号不匹配";
//			this.mErrors.addOneError(tError);
//			deletefile(unpath);
//			deleteFiles(path, file);
//			return false;
//		}
//		System.out.println("**************** ④解析xml文件完毕 ****************");

//		// 3.1 将jpg图片转换为tif 文件 删除jpg文件
//		flag = convertImage(unpath);
//		if (!flag) {
//			CError tError = new CError();
//			tError.moduleName = "LLPadDealScanImages";
//			tError.functionName = "convertImage";
//			tError.errorMessage = "图片转换失败";
//			this.mErrors.addOneError(tError);
//			deletefile(unpath);
//			deleteFiles(path, file);
//			return false;
//		}
//		System.out.println("**************** ⑤图片转换完毕 ****************");

		// 4、处理扫描一套表
		flag = sendtoCore(unpath);
		if (!flag) {
			CError tError = new CError();
			tError.moduleName = "LLPadDealScanImages";
			tError.functionName = "sendtoCore";
			tError.errorMessage = "扫描数据上传核心错误";
			this.mErrors.addOneError(tError);
			deletefile(unpath);
			deleteFiles(path, file);
			return false;
		}
		System.out.println("**************** ⑥理赔核心添加数据处理完毕 ****************");
		return true;

	}

	/**
	 * 解压zip包
	 */
	public boolean unZip(String file, String unpath) {
		DeCompressZip dec = new DeCompressZip();
		if (!dec.DeCompress(file, unpath)) {
			return false;
		}
		return true;
	}

	

	public boolean sendtoCore(String prtno, String manageCom) {

		String strDocID = getMaxNo("DocID");
		mES_DOC_MAINSchema.setDocID(strDocID);
		mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
		mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
		mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
		mES_DOC_MAINSchema.setModifyTime(mCurrentTime);

		for (int i = 0; i < mES_DOC_PAGESSet.size(); i++) {
			String strPageID = getMaxNo("PageID");
			mES_DOC_PAGESSet.get(i + 1).setPageID(strPageID);
			mES_DOC_PAGESSet.get(i + 1).setDocID(strDocID);
			mES_DOC_PAGESSet.get(i + 1).setMakeDate(mCurrentDate);
			mES_DOC_PAGESSet.get(i + 1).setModifyDate(mCurrentDate);
			mES_DOC_PAGESSet.get(i + 1).setMakeTime(mCurrentTime);
			mES_DOC_PAGESSet.get(i + 1).setModifyTime(mCurrentTime);
		}
		mES_DOC_RELATIONSchema.setBussNo(prtno);
		mES_DOC_RELATIONSchema.setDocCode(prtno);
		mES_DOC_RELATIONSchema.setBussNoType("11");
		mES_DOC_RELATIONSchema.setDocID(strDocID);
		mES_DOC_RELATIONSchema.setBussType(mES_DOC_MAINSchema.getBussType());
		mES_DOC_RELATIONSchema.setSubType(mES_DOC_MAINSchema.getSubType());
		mES_DOC_RELATIONSchema.setRelaFlag("0");

		map.put(mES_DOC_RELATIONSchema, "INSERT");
		map.put(mES_DOC_MAINSchema, "INSERT");
		map.put(mES_DOC_PAGESSet, "INSERT");
		this.mResult.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(this.mResult, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

		return true;
	}

//	public boolean convertImage(String path) {
//		try {
//			File root = new File(path);
//			File[] tfile = root.listFiles();
//			for (int i=0;i<tfile.length;i++) {
//				File a =tfile[i];
//				if (a.getName().endsWith(".jpg")) {
//					// 转换成tif
////					String input2 = a.toString();
////					String output2 = path
////							+ "/"
////							+ a.getName().substring(0,
////									a.getName().lastIndexOf(".")) + ".tif";
//
////					RenderedOp src2 = JAI.create("fileload", input2);
////					OutputStream os2 = new FileOutputStream(output2);
////
////					TIFFEncodeParam param3 = new TIFFEncodeParam();
////
////					ImageEncoder enc2 = ImageCodec.createImageEncoder("TIFF",
////							os2, param3);
////
////					enc2.encode(src2);
////					os2.flush();
////					os2.close();
//					
//					// 转换成gif
//					String input3 = a.toString();
//					String output3 = path
//							+ "/"
//							+ a.getName().substring(0,
//									a.getName().lastIndexOf(".")) + ".gif";
//
//					File PNFFile = new File(input3);
//					BufferedImage src1 = ImageIO.read(PNFFile);
//					AnimatedGifEncoder e = new AnimatedGifEncoder();
//					e.setRepeat(0);
//					e.start(output3);
//					e.setDelay(100); 
//					e.addFrame(src1);
//					e.finish();
//					//ImageIO.write(src1, "gif", new File(output3)); 
//					
//					String output2 = path
//					+ "/"
//					+ a.getName().substring(0,
//							a.getName().lastIndexOf(".")) + ".tif";
//					a.renameTo(new File(output2));
//
//				}
//			}
//
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return true;
//	}

	// 删除指定文件
	/*
	 * 各处理过程操作失败都需要删除本地缓存文件
	 */
	public boolean deleteFiles(String path, String file) {

		String folder = path + "/" + file;

		File zipFile = new File(folder);

		if (zipFile.exists()) {
			if (!zipFile.delete()) {
				System.out.println("111111");
				return false;
			}
		}

		return true;
	}

	// 删除目录下所有文件
	public boolean deletefile(String delpath) throws Exception {
		try {

			File file = new File(delpath);
			// 当且仅当此抽象路径名表示的文件存在且 是一个目录时，返回 true
			if (!file.isDirectory()) {
				file.delete();
			} else if (file.isDirectory()) {
				String[] filelist = file.list();
				for (int i = 0; i < filelist.length; i++) {
					File delfile = new File(delpath + "/" + filelist[i]);
					if (!delfile.isDirectory()) {
						delfile.delete();
						System.out
								.println(delfile.getAbsolutePath() + "删除文件成功");
					} else if (delfile.isDirectory()) {
						deletefile(delpath + "/" + filelist[i]);
					}
				}
				System.out.println(file.getAbsolutePath() + "删除成功");
				file.delete();
			}

		} catch (FileNotFoundException e) {
			System.out.println("deletefile() Exception:" + e.getMessage());
		}
		return true;
	}

	// 生成流水号，包含错误处理
	private String getMaxNo(String cNoType) {
		String strNo = PubFun1.CreateMaxNo(cNoType, 1);

		if (strNo.equals("") || strNo.equals("0")) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "UploadPrepareBL";
			tError.functionName = "getReturnData";
			tError.errorNo = "-90";
			tError.errorMessage = "生成流水号失败!";
			this.mErrors.addOneError(tError);
			strNo = "";
		}
		return strNo;
	}
	
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
	
	public boolean sendtoCore(String path){
		File root = new File(path);
		File[] tImages = root.listFiles();
		
//    	String mSunType = mImageInfos.getChildTextTrim("subtype");
//    	String mPages = mImageInfos.getChildTextTrim("NUMPAGES");
    	String mPages = String.valueOf(tImages.length);
    	if(!"".equals(mPages)&& mPages!=null && !"null".equals(mPages)){
    		ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();
		mES_DOC_MAINSchema.setDocCode(mCaseNo);
		mES_DOC_MAINSchema.setInputStartDate(mCurrentDate);
		mES_DOC_MAINSchema.setInputEndDate(mCurrentDate);
		mES_DOC_MAINSchema.setSubType("LP01");
		mES_DOC_MAINSchema.setBussType("LP");
		mES_DOC_MAINSchema.setManageCom(mManageCom);
		mES_DOC_MAINSchema.setVersion("01");
		mES_DOC_MAINSchema.setScanNo("0");
		mES_DOC_MAINSchema.setState("01");
		mES_DOC_MAINSchema.setDocFlag("1");
		mES_DOC_MAINSchema.setOperator(mOperator);
		mES_DOC_MAINSchema.setScanOperator(mOperator);
		mES_DOC_MAINSchema.setNumPages(mPages);
		String strDocID = getMaxNo("DocID");
		mES_DOC_MAINSchema.setDocID(strDocID);
		mES_DOC_MAINSchema.setMakeDate(mCurrentDate);
		mES_DOC_MAINSchema.setModifyDate(mCurrentDate);
		mES_DOC_MAINSchema.setMakeTime(mCurrentTime);
		mES_DOC_MAINSchema.setModifyTime(mCurrentTime);
		mES_DOC_MAINSet.add(mES_DOC_MAINSchema);
		
    	if(tImages.length>0){
        for (int k = 0; k < tImages.length; k++) 
        {
        	
            String mPageCode = String.valueOf(k+1);
            String tPageNamejpg = tImages[k].getName();
            String mPageName = tPageNamejpg.substring(0, tPageNamejpg.lastIndexOf("."));
            System.out.println(mPageName);
//            String mPageSuffix = tPageInfoDate.getChildText("PAGESUFFIX");
//            String mPicPath = tPageInfoDate.getChildText("PICPATH");
            if(!"".equals(mPageCode)&& mPageCode!=null && !"null".equals(mPageCode)&&!"".equals(mPageName)&& mPageName!=null && !"null".equals(mPageName)){
	            ExeSQL tExeSql = new ExeSQL();
	            String tFilePathIp = tExeSql
	            	.getOneValue("select codealias from ldcode where codetype='LLPadScan' and code='UrlPicPath'");
	            
	            String tFilePathUrl = tExeSql
	        	.getOneValue("select codealias from ldcode where codetype='LLPadScan' and code='UrlPicPath'");
	            tFilePathUrl = "FTPPATH";
	            String tFilePath = tExeSql
	        	.getOneValue("select codename from ldcode where codetype='LLPadScan' and code='UrlPicPath'");
	            tFilePath = tFilePath +PubFun.getCurrentDate() + "/"+ mCaseNo+"/";
				ES_DOC_PAGESSchema tES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
				String strPageID = getMaxNo("PageID");
				tES_DOC_PAGESSchema.setHostName(tFilePathIp);
				tES_DOC_PAGESSchema.setPageCode(mPageCode);
				tES_DOC_PAGESSchema.setPageName(mPageName);
				tES_DOC_PAGESSchema.setPageSuffix(".jpg");
				tES_DOC_PAGESSchema.setPageFlag("1");
				tES_DOC_PAGESSchema.setPageType("0");
				tES_DOC_PAGESSchema.setPicPath(tFilePath);
				tES_DOC_PAGESSchema.setPicPathFTP(tFilePathUrl);
				tES_DOC_PAGESSchema.setManageCom(mManageCom);
				tES_DOC_PAGESSchema.setOperator(mOperator);
				tES_DOC_PAGESSchema.setPageID(strPageID);
				tES_DOC_PAGESSchema.setDocID(strDocID);
				tES_DOC_PAGESSchema.setMakeDate(mCurrentDate);
				tES_DOC_PAGESSchema.setModifyDate(mCurrentDate);
				tES_DOC_PAGESSchema.setMakeTime(mCurrentTime);
				tES_DOC_PAGESSchema.setModifyTime(mCurrentTime);
				mES_DOC_PAGESSet.add(tES_DOC_PAGESSchema);
        	}               
        }
    	ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
        mES_DOC_RELATIONSchema.setBussNo(mCaseNo);
		mES_DOC_RELATIONSchema.setDocCode(mCaseNo);
		mES_DOC_RELATIONSchema.setBussNoType("21");
		mES_DOC_RELATIONSchema.setDocID(strDocID);
		mES_DOC_RELATIONSchema.setBussType(mES_DOC_MAINSchema.getBussType());
		mES_DOC_RELATIONSchema.setSubType(mES_DOC_MAINSchema.getSubType());
		mES_DOC_RELATIONSchema.setRelaFlag("0");
		mES_DOC_RELATIONSet.add(mES_DOC_RELATIONSchema);
    	}
    	}
    	return true;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		LLPadDealScanImages tLLPadDealScanImages = new LLPadDealScanImages();
		 VData tVData = new VData();
		 TransferData transferData = new TransferData();
		 transferData.setNameAndValue("ZipLocalPath", "E:\\PADTEST\\iMAGES\\zip\\");
		 transferData.setNameAndValue("ZipName", "test.zip");
		 transferData.setNameAndValue("CaseNo", "C4400151117000029");
		 GlobalInput mGlobalInput = new GlobalInput();
		 mGlobalInput.Operator = "001";
		 mGlobalInput.ManageCom = "8601";
		 tVData.add(mGlobalInput);
		 tVData.add(transferData);

		 try {
			 tLLPadDealScanImages.submitData(tVData, "");
		 ;
		 } catch (Exception e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }

		// String ab = "F:\\printdata\\160000000001.zip";
		// a.ceshi(ab);
		// File m=new File("F:\\printdata\\160000000001\\");
		// try {
		// a.deletefile("F:\\printdata\\160000000001.zip");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
//		String path = "F:\\12345\\PD00000000043";
//		try {
//			//a.convertImage(path);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}