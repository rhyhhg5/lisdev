package com.sinosoft.lis.yibaotong;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	public static Date parseDate(String pDateString, String pDateFormat) {
		try {
			return new SimpleDateFormat(pDateFormat).parse(pDateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String formatTrans(String pDate, String pOldFormat, String pNewFormat) {
		return new SimpleDateFormat(pNewFormat).format(parseDate(pDate, pOldFormat));
	}
	
	public static String getCurrentDate(String pDateFormat) {
		return new SimpleDateFormat(pDateFormat).format(new Date());
	}
	
	public static String getDateStr(Calendar pCalendar, String pFormat) {
		return getDateStr(pCalendar.getTime(), pFormat);
	}
	
	public static String getDateStr(Date pDate, String pFormat) {
		return new SimpleDateFormat(pFormat).format(pDate);
	}
	
	public static int getAge(Date pBirthday) {
		GregorianCalendar mBirthdayCalendar = new GregorianCalendar();
		mBirthdayCalendar.setTime(pBirthday);
		
		GregorianCalendar mNowCalendar = new GregorianCalendar();
		
		int mAge = mNowCalendar.get(Calendar.YEAR) - mBirthdayCalendar.get(Calendar.YEAR);
		
		mNowCalendar.add(Calendar.YEAR, -mAge);
		if (mNowCalendar.before(mBirthdayCalendar)) {
			mAge--;
		}
		
		return mAge;
	}
	
	public static int calInterval(String pStartDate, String pEndDate, String unit) {
		Date startDate = parseDate(pStartDate, "yyyy-MM-dd");
		Date endDate = parseDate(pEndDate, "yyyy-MM-dd");
		
        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);
        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);
        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        if (unit.equals("Y")) {
            interval = eYears - sYears;
            if (eMonths < sMonths) {
                interval--;
            } else {
                if (eMonths == sMonths && eDays < sDays) {
                    interval--;
                    if (eMonths == 1) { //如果同是2月，校验润年问题
                        if ((sYears % 4) == 0 && (eYears % 4) != 0) { //如果起始年是润年，终止年不是润年
                            if (eDays == 28) { //如果终止年不是润年，且2月的最后一天28日，那么补一
                                interval++;
                            }
                        }
                    }
                }
            }
        }
        if (unit.equals("M")) {
            interval = eYears - sYears;
            interval = interval * 12;

            interval = eMonths - sMonths + interval;
            if (eDays < sDays) {
                interval--;
                //eDays如果是月末，则认为是满一个月
                int maxDate = eCalendar.getActualMaximum(Calendar.DATE);
                if (eDays == maxDate) {
                    interval++;
                }
            }
        }
        if (unit.equals("D")) {
            interval = eYears - sYears;
            interval = interval * 365;
            interval = eDaysOfYear - sDaysOfYear + interval;

            // 处理润年
            int n = 0;
            eYears--;
            if (eYears > sYears) {
                int i = sYears % 4;
                if (i == 0) {
                    sYears++;
                    n++;
                }
                int j = (eYears) % 4;
                if (j == 0) {
                    eYears--;
                    n++;
                }
                n += (eYears - sYears) / 4;
            }
            if (eYears == sYears) {
                int i = sYears % 4;
                if (i == 0) {
                    n++;
                }
            }
            interval += n;
        }
        return interval;
    }
	
	public static void main(String[] args) throws Exception {
//		SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMdd");
//		Date mBirthdayDate = mDateFormat.parse("19990228");
//		GregorianCalendar mBirthdayCalendar = new GregorianCalendar();
//		mBirthdayCalendar.setTime(mBirthdayDate);
//		System.out.println("Birthday = " + mDateFormat.format(mBirthdayCalendar.getTime()));
//		
//		GregorianCalendar mNowCalendar = new GregorianCalendar();
//		mNowCalendar.set(2000, Calendar.FEBRUARY, 29);
//		System.out.println("Now = " + mDateFormat.format(mNowCalendar.getTime()));
//		
//		int mAge = mNowCalendar.get(Calendar.YEAR) - mBirthdayCalendar.get(Calendar.YEAR);
//		System.out.println(mAge);
//		
//		mNowCalendar.add(Calendar.YEAR, -mAge);
//		System.out.println("NowBirthday = " + mDateFormat.format(mNowCalendar.getTime()));
//		
//		if (mNowCalendar.before(mBirthdayCalendar)) {
//			System.out.println("before");
//			mAge--;
//		}
//		System.out.println(mAge);
		
//		SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy.MM.dd");
//		GregorianCalendar mNowCalendar = new GregorianCalendar(2000, Calendar.JANUARY, 01);
//		System.out.println(mDateFormat.format(mNowCalendar.getTime()));
//		mNowCalendar.add(Calendar.DAY_OF_MONTH, -1);
//		System.out.println(mDateFormat.format(mNowCalendar.getTime()));
		
		GregorianCalendar mBirthdayCalendar = new GregorianCalendar(2000, Calendar.SEPTEMBER, 28);
		System.out.println(getAge(mBirthdayCalendar.getTime()));
	}
}
