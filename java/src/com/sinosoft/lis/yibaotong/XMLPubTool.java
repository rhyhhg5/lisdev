package com.sinosoft.lis.yibaotong;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.io.SAXReader;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;

public class XMLPubTool {
	private String XMLFILE = "";
	private org.dom4j.Document dom4jDoc;
	
	public void XMLPubTool(){
		
	}
	
	public void XMLPubTool(String aXMLInstance){
		XMLFILE = aXMLInstance;
		try {
			File f = new File(XMLFILE);
			SAXReader reader = new SAXReader();
			dom4jDoc = reader.read(f);
		}catch (Exception ex) {
			ex.printStackTrace();
			return;
		}
	}

	/**
	 * 解析xpath串，获取元素或属性值
	 * 
	 * @param xpathString
	 * @return
	 */
	public String parseDom4jXPath(String xpathString) {
		try {
			org.dom4j.Node tNode = dom4jDoc.selectSingleNode(xpathString);
			return tNode.getText();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 生成简单元素
	 * @param sTag
	 * @param sContent
	 */
	public Element makeElement(String sTag, String sContent) {
		Element tElement = new Element(sTag);
		tElement.setText(sContent);
		return tElement;
	}
	
	/**
	 * 生成空元素
	 * @param sTag
	 */
	public Element makeEmptyElement(String sTag) {
		return makeElement(sTag,"");
	}
	
	/**
	 * 生成带属性的复杂元素
	 * @param sTag
	 * @param sAttriName
	 * @param sAttriValue
	 */
	public Element makeComplexElement(String sTag, String sAttriName,
			String sAttriValue) {
		Element tElement = new Element(sTag);
		Attribute tAttri = new Attribute(sAttriName, sAttriValue);
		List AttrList = new ArrayList();
		AttrList.add(tAttri);
		tElement.setAttributes(AttrList);
		return tElement;
	}
	
	/**
	 * 给元素增加属性
	 * @param aElement
	 * @param sAttriName
	 * @param sAttriValue
	 */
	public Element AddAttribute(Element aElement, String sAttriName,
			String sAttriValue) {
		List AttrList = aElement.getAttributes();
		Attribute tAttri = new Attribute(sAttriName, sAttriValue);
		AttrList.add(tAttri);
		aElement.setAttributes(AttrList);
		return aElement;
	}
	
	/**
	 * 建立元素链接关系
	 * @param aParent
	 * @param aChild
	 * @return
	 */
	public Element LinkElement(Element aParent, Element aChild){
		aParent.addContent(aChild);
		return aParent;
	}
	
	public static void main(String[] args){
		XMLPubTool tXMLPubTool = new XMLPubTool();
		Element tElement = tXMLPubTool.makeElement("REQUEST_TYPE", "01");
		Element sElement = tXMLPubTool.makeElement("TRANSACTION_NUM","5500010");
		
//		Element mElement = tXMLPubTool.makeEmptyElement("PACKET");
		Element mElement = tXMLPubTool.makeComplexElement("PACKET", "type", "REQUEST");
		tXMLPubTool.AddAttribute(mElement,"version","1.0");
		
		tXMLPubTool.LinkElement(mElement, sElement);
		
		Document tDocument = new Document(mElement);
		JdomUtil.print(tElement);
		System.out.println("");
		JdomUtil.print(tDocument);
		return;
	}
	
}

