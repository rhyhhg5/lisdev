package com.sinosoft.lis.yibaotong;

import org.jdom.Document;

public interface ServiceInterface {
	public Document service(Document pInXmlDoc);
}
