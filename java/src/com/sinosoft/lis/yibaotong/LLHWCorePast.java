package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLHWCorePast implements ServiceInterface {
	
	public LLHWCorePast() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CErrors mErrors = new CErrors();
	public VData mResult = new VData();
	/*错误信息*/
	private String mErrorMessage = "";
	/*报文类型*/
	private String mDocType = "";
	/*返回类型代码*/
	private String mResponseCode = "1";    
	/*请求类型*/
	private String mRequestType = "";       
	/*交互编码*/
	private String mTransactionNum = "";    
	/*处理类型 1 实时 2非实时*/
	private String mDealType = "1";        
	/*基本信息*/
	private String mCONTNO = "";
	/*菜单组、菜单组到菜单的相关信息*/
	public SSRS mSSRS = new SSRS();
	/*处理结果标记的状态*/
	private boolean mDealState=true;
	/*插入的节点数据*/
	private Element mCONTNO_DATA;
	
	public Document service(Document pInXmlDoc) {
		 
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{
                        //保存信息到LLHospCase
                       /* PubSubmit tPubSubmit = new PubSubmit();
                        VData tVData = new VData();
                        MMap tMMap= new MMap();
                        tMMap.put(mLLHospCaseSet, "INSERT");
                        tVData.add(tMMap);
                        tPubSubmit.submitData(tVData, "INSERT");*/
                    }
                   
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
	}

	private Document createXML() {
		//处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
           try
        {
            return createResultXML();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("createXML()", "生成返回报文错误!");
            return createFalseXML();
        }
        }
	}

	private Document createResultXML() {
		 System.out.println("LLPADUserLogin--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");


         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);


         Element tBodyData = new Element("BODY");
         
         Element tLLCASELIST = new Element("LLCASELIST");

          for(int i = 1;i<=mSSRS.getMaxRow();i++)    {   
        	  Element tLLCASE_DATA = new Element("LLCASE_DATA");
        	  Element tContNo = new Element("Contno");
        	  Element tCaseno = new Element("Caseno");
        	  Element tCLAIMNO  = new Element("CLAIMNO");
        	  Element tGetdutykind = new Element("Getdutykind");
        	  Element tGetdutykindname = new Element("Getdutykindname");
        	  Element tGetdutycde = new Element("Getdutycde");
        	  Element tGetdutycdename = new Element("Getdutycdename");
        	  Element tInsuedno = new Element("Insuedno");
        	  Element tName = new Element("name");
        	  Element tIdtype = new Element("idtype");
        	  Element tIdno = new Element("idno");
        	  Element tAccdate = new Element("accdate");
        	  Element tRiskcode = new Element("riskcode");
        	  Element tRiskname = new Element("riskname");
        	  Element tDISEASECODE = new Element("DISEASECODE");
        	  Element tDISEASEname = new Element("DISEASEname");
        	  Element tRealpay = new Element("realpay");
        	  Element tGivetype = new Element("givetype");
        	  Element tGivetypename = new Element("givetypename");
        	  Element tOutDutyAmnt = new Element("OutDutyAmnt");
              Element tEndcasedate = new Element("endcasedate");
             
              tContNo.setText(mSSRS.GetText(i, 1));
              tCaseno.setText(mSSRS.GetText(i, 2));
              tCLAIMNO.setText(mSSRS.GetText(i, 3));
              tGetdutykind.setText(mSSRS.GetText(i, 4));
              tGetdutykindname.setText(mSSRS.GetText(i, 5));
              tGetdutycde.setText(mSSRS.GetText(i, 6));
              tGetdutycdename.setText(mSSRS.GetText(i, 7));
              tInsuedno.setText(mSSRS.GetText(i, 8));
              tName.setText(mSSRS.GetText(i, 9));
              tIdtype.setText(mSSRS.GetText(i, 10));
              tIdno.setText(mSSRS.GetText(i, 11));
              tAccdate.setText(mSSRS.GetText(i, 12));
              tRiskcode.setText(mSSRS.GetText(i, 13));
              tRiskname.setText(mSSRS.GetText(i, 14));
              tDISEASECODE.setText(mSSRS.GetText(i, 15));
              tDISEASEname.setText(mSSRS.GetText(i, 16));
              tRealpay.setText(mSSRS.GetText(i, 17));
              tGivetype.setText(mSSRS.GetText(i, 18));
              tGivetypename.setText(mSSRS.GetText(i, 19));
              tOutDutyAmnt.setText(mSSRS.GetText(i, 20));
              tEndcasedate.setText(mSSRS.GetText(i, 21));
              
              tLLCASE_DATA.addContent(tContNo);
              tLLCASE_DATA.addContent(tCaseno);
              tLLCASE_DATA.addContent(tCLAIMNO);
              tLLCASE_DATA.addContent(tGetdutykind);
              tLLCASE_DATA.addContent(tGetdutykindname);
              tLLCASE_DATA.addContent(tGetdutycde);
              tLLCASE_DATA.addContent(tGetdutycdename);
              tLLCASE_DATA.addContent(tInsuedno);
              tLLCASE_DATA.addContent(tName);
              tLLCASE_DATA.addContent(tIdtype);
              tLLCASE_DATA.addContent(tIdno);
              tLLCASE_DATA.addContent(tAccdate);
              tLLCASE_DATA.addContent(tRiskcode);
              tLLCASE_DATA.addContent(tRiskname);
              tLLCASE_DATA.addContent(tDISEASECODE);
              tLLCASE_DATA.addContent(tDISEASEname);
              tLLCASE_DATA.addContent(tRealpay);
              tLLCASE_DATA.addContent(tGivetype);
              tLLCASE_DATA.addContent(tGivetypename);
              tLLCASE_DATA.addContent(tOutDutyAmnt);
              tLLCASE_DATA.addContent(tEndcasedate);
              
              tLLCASELIST.addContent(tLLCASE_DATA);
              
          }
          tBodyData.addContent(tLLCASELIST);
          
         tRootData.addContent(tBodyData);
         Document tDocument = new Document(tRootData);
         return tDocument;
         
	}

	private Document createFalseXML() {
		 System.out.println("LLPADUserLogin--createResultXML(错误返回报文)");
	        Element tRootData = new Element("PACKET");
	        tRootData.addAttribute("type", "RESPONSE");
	        tRootData.addAttribute("version", "1.0");

	        Element tHeadData = new Element("HEAD");

	        Element tRequestType = new Element("REQUEST_TYPE");
	        tRequestType.setText(mRequestType);
	        tHeadData.addContent(tRequestType);

	        if (!"E".equals(mResponseCode)) {
	            mResponseCode = "0";
	        }
	        

//	        Element tResponseCode = new Element("RESPONSE_CODE");
//	        tResponseCode.setText(mResponseCode);
//	        tHeadData.addContent(tResponseCode);



	        Element tTransactionNum = new Element("TRANSACTION_NUM");
	        tTransactionNum.setText(mTransactionNum);
	        tHeadData.addContent(tTransactionNum);
	        
	      
	        Element tBody = new Element("BODY");
	        
	        Element tLoginState = new Element("RESPONSE_CODE");
	        
	        tLoginState.setText("1");
	        tBody.addContent(tLoginState);
	        
	        mErrorMessage = mErrors.getFirstError();
	        Element tErrorMessage = new Element("ERROR_MESSAGE");
	        tErrorMessage.setText(mErrorMessage);
	        tBody.addContent(tErrorMessage);
	        
	        tRootData.addContent(tHeadData);
	        tRootData.addContent(tBody);

	        Document tDocument = new Document(tRootData);
	        return tDocument;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LLPADUserLogin";
		cError.functionName=szFunc;
		cError.errorMessage=szErrMsg;
		System.out.println(szFunc + "--" + szErrMsg);
		this.mErrors.addOneError(cError);
	}

	/*处理报文信息*/
	private boolean dealData() {
		mCONTNO = mCONTNO_DATA.getChildTextTrim("CONTNO");
		if (mCONTNO == null || "".equals(mCONTNO)) {
            buildError("dealData()", "【被保险人保单号码】为空!");
            return false;
        }
		ExeSQL tExeSQL = new ExeSQL();
		String checksql = "select a.contno from  llclaimdetail a,llcase b  " +
				"where a.caseno=b.caseno  and a.contno ='" + mCONTNO + "'and  b.rgtstate in('11','12')"; 
		SSRS tSSRS = tExeSQL.execSQL(checksql);
		if(tSSRS!=null&&tSSRS.getMaxRow()>0){
    		
	    }else{
            buildError("dealData()", "未找到保单号为"+mCONTNO+"的有效客户信息");
            mDealState = false; 
        }  
		String chksql = "select a.contno, a.caseno , " +
				"(select claimno from llhospcase where caseno=a.caseno) , " + 
				"a.getdutykind , " +  
				"(select codename from ldcode where codetype='getdutykind' and code=a.getdutykind fetch first row only) , " +  
				"a.getdutycode , " + 
				"(select getdutyname from lmdutygetclm where getdutycode=a.getdutycode fetch first row only ) , " +  
				"b.customerno ,b.customername ,b.idtype ,b.idno , " +  
				"(select max(accdate) from LLSubReport where subrptno in( select subrptno from llcaserela where caseno=b.caseno)) ,  " + 
				"a.riskcode , " + 
				"(select riskname from lmriskapp where riskcode=a.riskcode),  " + 
				"(select DiseaseCode from llcasecure where caseno =b.caseno fetch first row only) , " +  
				"(select DiseaseName from llcasecure where caseno =b.caseno fetch first row only) , " + 
				"sum(a.realpay) ,a.GiveType ,a.GiveTypeDesc ,sum(a.OutDutyAmnt) ,b.endcasedate " + 
				"from  llclaimdetail a,llcase b  " +
				"where a.caseno=b.caseno  and a.contno ='" + mCONTNO + "'and  b.rgtstate in('11','12')  " + 
				"group by a.caseno,a.getdutykind, a.getdutycode,b.customerno,  " + 
				"a.contno,b.customername,b.idtype,b.idno,a.GiveType,a.GiveTypeDesc,b.endcasedate,b.caseno,a.riskcode  with ur ";
		mSSRS = tExeSQL.execSQL(chksql);
		if(mSSRS!=null&&mSSRS.getMaxRow()>0){
    		
		}else{
			buildError("dealData()", "该保单暂无既往理赔信息");
            mDealState = false; 
		}
		return true;
	}

	/*校验报文信息*/
	private boolean checkData() {
		if(!"REQUEST".equals(mDocType)) {
			buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
			return false;
		}
		 if (!"HW07".equals(mRequestType)) {
	            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
	            return false;
	        }
		 if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
		            mTransactionNum.length() != 30) {
		            buildError("checkData()", "【交互编码】的编码个数错误!");
		            return false;
		        }
		 if (!"HW07".equals(mTransactionNum.substring(0, 4))) {
	            buildError("checkData()", "【交互编码】的请求类型编码错误!");
	            return false;
	        }
		return true;
	}

	private boolean getInputData(Document cInXml) {
		System.out.println("getInputData()");
		/*对报文做出判断*/
		if(cInXml==null) {
			buildError("getInputData()","未获得报文");
			return false;
		}
		/*获取报文的类型*/
		Element tRootData = cInXml.getRootElement();
		mDocType = tRootData.getAttributeValue("type");
		/*获取head部分内容*/
		Element tHeadData = tRootData.getChild("HEAD");
		mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
		System.out.println("####" + mRequestType);
		mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
		/*获取部分body内容*/
		Element tBodyData = tRootData.getChild("BODY");
		mCONTNO_DATA = tBodyData.getChild("CONTNO_DATA");
		return true;
	}
	
	public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
//            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/PADTEST/SH01.xml"), "GBK");
        	tInXmlDoc = JdomUtil.build(new FileInputStream("D:/项目/HW07.xml"), "GBK");
        	LLHWCorePast tBusinessDeal = new LLHWCorePast();
//            LLPadIDGetdutyInfo tBusinessDeal = new LLPadIDGetdutyInfo();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
}
