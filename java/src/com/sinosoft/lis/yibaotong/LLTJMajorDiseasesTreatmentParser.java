/**
 * 2014-5-20
 */
package com.sinosoft.lis.yibaotong;

import org.jdom.Element;


/**
 * @author 
 *
 */
public class LLTJMajorDiseasesTreatmentParser
{
    
    
   
    //节点名称
	private String CASENUM="";     //案件顺序号-为返回报文准备
	private String RGTNO="";//批次号
	private String CASENO="";//案件号
    private String CURENO="";//就诊顺序号    
    private String CURETYPE="";//汇总类型
    private String HOSPITALCODE="";     //医院编码
    private String SERPRONO=""; //服务项目大类编码
    private String NOMEANNO=""; //无含义项目编码顺序号
    private String UNIFIEDPRO=""; //统一项目
    private String SERPRONAME=""; //服务项目名称
    private String HOSSERPRONAME=""; //医院服务项目名称
    private String SELFRATE=""; //本项目自付比例
    private String REAGENTTYPE=""; //剂型
    private String SPEC=""; //规格
    private String PRICE=""; //单价
    private String QUANTITY=""; //数量
    private String ACCMONEY=""; //发生金额
    private String SELFMONEY=""; //全自费金额
    private String ADDSELFMONEY=""; //增负金额
    private String APPAMNT=""; //申报金额
    private String HANDMONEY=""; //审批金额
    private String DECLINEAMNT=""; //拒付金额
    private String AMNTACCDATE=""; //费用发生时间
    private String NO=""; //序号
    private String BILLNO=""; //医保专用票据号
    private String USEMETHOD=""; //用法
    private String USELEVEL=""; //用量
    private String USEDAYS=""; //用药天数
    private String ACCDESC=""; //特殊情况说明
    private String REMARK=""; //备注
    
    private boolean mDealState=true;

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO 自动生成方法存根

    }
    
    /**解析报文返回理赔信息
     * @author zjd
     */
    public LLTJMajorDiseasesTreatmentParser(Element mLLcase){
    	
    	CASENUM=mLLcase.getChildTextTrim("CASENUM");
    	RGTNO=mLLcase.getChildTextTrim("RGTNO");
        CASENO=mLLcase.getChildTextTrim("CASENO");       
        CURENO=mLLcase.getChildTextTrim("CURENO");
        if (CURENO == null || "".equals(CURENO)|| CURENO.equals("null")) {
        	CURENO=mLLcase.getChildTextTrim("CLAIMNO");
        }
        CURETYPE=mLLcase.getChildTextTrim("CURETYPE");
        HOSPITALCODE=mLLcase.getChildTextTrim("HOSPITALCODE");
        SERPRONO=mLLcase.getChildTextTrim("SERPRONO");
        NOMEANNO=mLLcase.getChildTextTrim("NOMEANNO");
        UNIFIEDPRO=mLLcase.getChildTextTrim("UNIFIEDPRO");
        SERPRONAME=mLLcase.getChildTextTrim("SERPRONAME");
        HOSSERPRONAME=mLLcase.getChildTextTrim("HOSSERPRONAME");
        SELFRATE=mLLcase.getChildTextTrim("SELFRATE");
        REAGENTTYPE=mLLcase.getChildTextTrim("REAGENTTYPE");
        SPEC=mLLcase.getChildTextTrim("SPEC");
        PRICE=mLLcase.getChildTextTrim("PRICE");
        QUANTITY=mLLcase.getChildTextTrim("QUANTITY");
        ACCMONEY=mLLcase.getChildTextTrim("ACCMONEY");
        SELFMONEY=mLLcase.getChildTextTrim("SELFMONEY");
        ADDSELFMONEY=mLLcase.getChildTextTrim("ADDSELFMONEY");
        APPAMNT=mLLcase.getChildTextTrim("APPAMNT");
        HANDMONEY=mLLcase.getChildTextTrim("HANDMONEY");
        DECLINEAMNT=mLLcase.getChildTextTrim("DECLINEAMNT");
        AMNTACCDATE=mLLcase.getChildTextTrim("AMNTACCDATE");
        NO=mLLcase.getChildTextTrim("NO");
        BILLNO=mLLcase.getChildTextTrim("BILLNO");
        USEMETHOD=mLLcase.getChildTextTrim("USEMETHOD");
        USELEVEL=mLLcase.getChildTextTrim("USELEVEL");
        USEDAYS=mLLcase.getChildTextTrim("USEDAYS");
        ACCDESC=mLLcase.getChildTextTrim("ACCDESC");
        REMARK=mLLcase.getChildTextTrim("REMARK");
        
    }

	public String getACCDESC() {
		return ACCDESC;
	}

	public void setACCDESC(String accdesc) {
		ACCDESC = accdesc;
	}

	public String getACCMONEY() {
		return ACCMONEY;
	}

	public void setACCMONEY(String accmoney) {
		ACCMONEY = accmoney;
	}

	public String getADDSELFMONEY() {
		return ADDSELFMONEY;
	}

	public void setADDSELFMONEY(String addselfmoney) {
		ADDSELFMONEY = addselfmoney;
	}

	public String getAMNTACCDATE() {
		return AMNTACCDATE;
	}

	public void setAMNTACCDATE(String amntaccdate) {
		AMNTACCDATE = amntaccdate;
	}

	public String getAPPAMNT() {
		return APPAMNT;
	}

	public void setAPPAMNT(String appamnt) {
		APPAMNT = appamnt;
	}

	public String getBILLNO() {
		return BILLNO;
	}

	public void setBILLNO(String billno) {
		BILLNO = billno;
	}

	public String getCASENO() {
		return CASENO;
	}

	public void setCASENO(String caseno) {
		CASENO = caseno;
	}

	public String getCASENUM() {
		return CASENUM;
	}

	public void setCASENUM(String casenum) {
		CASENUM = casenum;
	}

	public String getCURENO() {
		return CURENO;
	}

	public void setCURENO(String cureno) {
		CURENO = cureno;
	}

	public String getCURETYPE() {
		return CURETYPE;
	}

	public void setCURETYPE(String curetype) {
		CURETYPE = curetype;
	}

	public String getDECLINEAMNT() {
		return DECLINEAMNT;
	}

	public void setDECLINEAMNT(String declineamnt) {
		DECLINEAMNT = declineamnt;
	}

	public String getHANDMONEY() {
		return HANDMONEY;
	}

	public void setHANDMONEY(String handmoney) {
		HANDMONEY = handmoney;
	}

	public String getHOSPITALCODE() {
		return HOSPITALCODE;
	}

	public void setHOSPITALCODE(String hospitalcode) {
		HOSPITALCODE = hospitalcode;
	}

	public String getHOSSERPRONAME() {
		return HOSSERPRONAME;
	}

	public void setHOSSERPRONAME(String hosserproname) {
		HOSSERPRONAME = hosserproname;
	}

	public boolean isMDealState() {
		return mDealState;
	}

	public void setMDealState(boolean dealState) {
		mDealState = dealState;
	}

	public String getNO() {
		return NO;
	}

	public void setNO(String no) {
		NO = no;
	}

	public String getNOMEANNO() {
		return NOMEANNO;
	}

	public void setNOMEANNO(String nomeanno) {
		NOMEANNO = nomeanno;
	}

	public String getPRICE() {
		return PRICE;
	}

	public void setPRICE(String price) {
		PRICE = price;
	}

	public String getQUANTITY() {
		return QUANTITY;
	}

	public void setQUANTITY(String quantity) {
		QUANTITY = quantity;
	}

	public String getREAGENTTYPE() {
		return REAGENTTYPE;
	}

	public void setREAGENTTYPE(String reagenttype) {
		REAGENTTYPE = reagenttype;
	}

	public String getREMARK() {
		return REMARK;
	}

	public void setREMARK(String remark) {
		REMARK = remark;
	}

	public String getRGTNO() {
		return RGTNO;
	}

	public void setRGTNO(String rgtno) {
		RGTNO = rgtno;
	}

	public String getSELFMONEY() {
		return SELFMONEY;
	}

	public void setSELFMONEY(String selfmoney) {
		SELFMONEY = selfmoney;
	}

	public String getSELFRATE() {
		return SELFRATE;
	}

	public void setSELFRATE(String selfrate) {
		SELFRATE = selfrate;
	}

	public String getSERPRONAME() {
		return SERPRONAME;
	}

	public void setSERPRONAME(String serproname) {
		SERPRONAME = serproname;
	}

	public String getSERPRONO() {
		return SERPRONO;
	}

	public void setSERPRONO(String serprono) {
		SERPRONO = serprono;
	}

	public String getSPEC() {
		return SPEC;
	}

	public void setSPEC(String spec) {
		SPEC = spec;
	}

	public String getUNIFIEDPRO() {
		return UNIFIEDPRO;
	}

	public void setUNIFIEDPRO(String unifiedpro) {
		UNIFIEDPRO = unifiedpro;
	}

	public String getUSEDAYS() {
		return USEDAYS;
	}

	public void setUSEDAYS(String usedays) {
		USEDAYS = usedays;
	}

	public String getUSELEVEL() {
		return USELEVEL;
	}

	public void setUSELEVEL(String uselevel) {
		USELEVEL = uselevel;
	}

	public String getUSEMETHOD() {
		return USEMETHOD;
	}

	public void setUSEMETHOD(String usemethod) {
		USEMETHOD = usemethod;
	}
    
   

   

}
