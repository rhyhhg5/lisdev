package com.sinosoft.lis.yibaotong;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import org.jdom.*;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;


import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLAffixSchema;
import com.sinosoft.lis.schema.LLAppClaimReasonSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSecurityReceiptSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.vschema.LLAffixSet;
import com.sinosoft.lis.vschema.LLAppClaimReasonSet;
import com.sinosoft.lis.vschema.LLCaseOpTimeSet;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLCaseRelaSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLFeeMainSet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.lis.vschema.LLSecurityReceiptSet;
import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.lis.yibaotong.LLPadGetScanImageBL;
import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
//import com.sun.xml.xsom.impl.scd.Iterators.Map;
/**
 * 这个类处理官网的申请理赔信息，到完成受理阶段
 * @author Alfred Xu
 * @time 2016-08-19
 */
public class LLGWCaseRegisterBL implements ServiceInterface{
	
    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();
    private MMap mMMap = new MMap();    
    private FDate fDate = new FDate();
	private Document THXmlDoc;
	
	private String point="1";//是否出错
	private String requestType;//请求类型
	private String mErrorMessage;//错误信息 
	private String mTransactionNum;//交互编码
	private String mresqonseType;//返回类型：正常信息、错误信息
	
	/** head部数据*/
	private Element head;
	/** 整体数据*/
	private Element casePre;
	/** 案件信息打包*/
	private Element caselist;	
	/**团单号 */
	private String grpContno;
	/** 操作员 */
	private String operator;
	/** 给付方式 */
	private String togetherFlag;
	/** 申请理赔人数 */
	private String peoNum;
	/** 理赔团体批次号 */
	private String tRgtNo;	
	/** 管理机构 */
	private String ManageCom;
	
	private String tLimit ;
	
	private String mPicPath;
	
	private String strHandler;//理赔分配 操作人员
	
	private boolean mSocialFlag = false;//是否社保单判断
	
	private Document doc;
	
	private LLCaseCommon tLLCaseCommon = new LLCaseCommon();
	
	private LLCaseSchema tLLCaseSchema;
	private LLCaseSet tLLCaseSet = new LLCaseSet();
	
	
	
	private LLFeeMainSchema tLLFeeMainSchema;
	private LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();
	
	private LLSecurityReceiptSchema tLLSecurityReceiptSchema ;
	private LLSecurityReceiptSet tLLSecurityReceiptSet = new LLSecurityReceiptSet();
	
	 
	private LLCaseCureSchema tLLCaseCureSchema;
	
	private LLCaseOpTimeSchema tLLCaseOpTimeSchema ;
	private LLCaseOpTimeSet tLLCaseOpTimeSet =new LLCaseOpTimeSet();
	
	private LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
	
	private LLAppClaimReasonSchema  tLLAppClaimReasonSchema ;
	private LLAppClaimReasonSet  tLLAppClaimReasonSet = new LLAppClaimReasonSet();

	
	private LLCaseRelaSchema tLLCaseRelaSchema;	
	private LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
	
	private LLSubReportSchema tLLSubReportSchema ;
	private LLSubReportSet tLLSubReportSet = new LLSubReportSet();
	
	
	private LLCaseReceiptSchema tLLCaseReceiptSchema;
	private LLCaseReceiptSet tLLCaseReceiptSet = new LLCaseReceiptSet();

	private LLAffixSchema tLLAffixSchema;
	private LLAffixSet tLLAffixSet = new LLAffixSet();

    //医保通案件号
    private LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
	private LLHospCaseSet mLLHospCaseSet = new LLHospCaseSet();
    
    private String mMakeDate = PubFun.getCurrentDate();
    private String mMakeTime = PubFun.getCurrentTime();
	public Document service(Document pInXmlDoc) {
		 System.out.println("LLGWCaseRegister--service");
		 THXmlDoc = pInXmlDoc;

	        try {
	            if (!getInputData(THXmlDoc)) {
	            	point = "0";
	            } else {
	                if (!checkData()) {
	                	point = "0";
	                } else {
	                    if (!dealData()) {
	                    	point = "0";
	                    }else{
	                    	if(!prepareOutputData()){
	                    		point = "0";
	                    	}else{
	                    		if(!submitAllData()){
	                    			point = "0";
	                    		}
	                    	}
	                    }
	                }
	            }
	        } catch (Exception ex) {
	        	point = "E";
	        } finally {
	        	if("1".equals(point)){
	        		
	        		mresqonseType = "1";
	        		doc =  CreatMessageXml();
	        		
	        	}else{
	        		mresqonseType = "0";
	        		doc =  CreatErrorXml();
	        	}
	        }
	        
	        return doc;
	        
	}


	private boolean submitAllData() {
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mResult, "")) {
            // @@错误处理
            CError.buildErr(this, "数据提交失败!");
            return false;
        }
		return true;
	}


	private Document CreatMessageXml() {

		Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");
        //头节点
        Element tHeadData = new Element("HEAD");
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        
        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(requestType);
        tHeadData.addContent(tRequestType);
        
        Element tResponseType = new Element("RESPONSE_TYPE");
        tResponseType.setText(mresqonseType);
        tHeadData.addContent(tResponseType);
        


        tRootData.addContent(tHeadData);
        
        //内容节点
        Element tBody = new Element("BODY");
        
        Element tBackData = new Element("BACK_DATA");
        
        Element mRgtNo = new Element("RGTNO");
        mRgtNo.setText(tRgtNo);
        tBackData.addContent(mRgtNo);
        
        Element mMessage = new Element("MESSAGE");
        mMessage.setText("理赔申请成功，理赔申请的批次号为"+tRgtNo+"。");
        tBackData.addContent(mMessage);
        
        tBody.addContent(tBackData);
        
        Element tLLCaseList = new Element("LLCASELIST");
        for(int j = 1;j<=tLLCaseSet.size();j++){
        	Element tLLcaseData = new Element("LLCASE_DATA");
        	
        	Element mCaseNo = new Element("CASENO");
        	mCaseNo.setText(tLLCaseSet.get(j).getCaseNo());
            tLLcaseData.addContent(mCaseNo);
            
            tLLCaseList.addContent(tLLcaseData);
        	
        }
        tBody.addContent(tLLCaseList); 
        tRootData.addContent(tBody);
        
        Document tDocument = new Document(tRootData);
        
		return tDocument;
	}


	private boolean getInputData(Document tHXmlDoc2) {
		try{
			
			Element root = tHXmlDoc2.getRootElement();
			head = root.getChild("HEAD");
			Element body = root.getChild("BODY");
			casePre = body.getChild("LLCASE_PRE");
			caselist = body.getChild("LLCASE_LIST");
			
			requestType = head.getChildText("REQUEST_TYPE");
			mTransactionNum = head.getChildText("TRANSACTION_NUM");
			
			grpContno = casePre.getChildText("GRPCONTNO");

			togetherFlag = casePre.getChildText("TOGETHERFLAG");
			peoNum = casePre.getChildText("PEONUM");
			
			if("".equals(grpContno)||grpContno==null){
				point = "0";
				mErrors.addOneError("没有获取到团体保单号！");
				return false;
			}
			if("".equals(togetherFlag)||togetherFlag==null){
				point = "0";
				mErrors.addOneError("没有指定给付方式！");
				return false;
			}
			//暂时不进行校验
			/*if("".equals(operator)||operator==null){
			mErrors.addOneError("没有操作员信息！");
			return false;
		}*/
			if("".equals(peoNum)||peoNum==null){
				point = "0";
				mErrors.addOneError("没有指定理赔人数！");
				return false;
			}
			
			ExeSQL tExeSQl = new ExeSQL();
			String com = "select distinct(ManageCom) from lccont where GrpContNo='"+grpContno+"' fetch first 1 row only";	
			ManageCom = tExeSQl.getOneValue(com);
			
			if("".equals(ManageCom)||ManageCom==null){
				point = "0";
				mErrors.addOneError("保单管理机构为空！");
				return false;
			}
			tLimit = PubFun.getNoLimit(ManageCom);
		}catch(Exception e){
			mErrors.addOneError("获得报文数据失败！");
			return false;
		}
		
		return true;
	}


	private boolean checkData() {
		
		String result = LLCaseCommon.checkGrp(grpContno);
        if(!"".equals(result)){
        	point = "0";
            this.mErrors.addOneError("工单号为"+result+"的保单管理现正对该团单进行保全操作，请先通知保全撤销相关操作，再进行理赔!");
            return false;
        }
		
		if(caselist==null){
			point = "0";
			mErrors.addOneError("没有需要理赔的案件信息！");
			return false;
		}
		
		if(!"GW03".equals(requestType)){
			mErrors.addOneError("请求服务器类型出现错误!");
			point = "0";
			return false;
		}
		return true;
	}


	private boolean dealData() {
		//由于申请人在系统对接中没有体现，默认申请人由团单号从核心业务系统中带出
		
		//处理申请理赔申请信息
		if(!dealRegister()){
			point = "0";
			mErrors.addOneError("处理申请信息错误！");
			return false;
		}
		//处理理赔案件信息
		if(!dealCaseData()){
			return false;
		}
		
		//批次中最后一个案件的理赔处理人作为批次的处理人
		tLLRegisterSchema.setHandler(strHandler);
  	  	tLLRegisterSchema.setHandler1(operator);
  	  	tLLRegisterSchema.setOperator(operator);
		
		
		if(!setAllData()){
			return false;
		}
		return true;
	}
	
	private boolean setAllData() {
		try{
			mMMap.put(tLLRegisterSchema, "INSERT");
			if(mLLHospCaseSet.size()>0){
				mMMap.put(mLLHospCaseSet, "INSERT");
			}
			if(tLLAppClaimReasonSet.size()>0){				
				mMMap.put(tLLAppClaimReasonSet, "INSERT");
			}
			if(tLLCaseSet.size()>0){
				mMMap.put(tLLCaseSet, "INSERT");
			}
			if(tLLCaseRelaSet.size()>0){
				mMMap.put(tLLCaseRelaSet, "INSERT");				
			}
			if(tLLSubReportSet.size()>0){				
				mMMap.put(tLLSubReportSet, "INSERT");
			}
			if(tLLSecurityReceiptSet.size()>0){				
				mMMap.put(tLLSecurityReceiptSet, "INSERT");
			}
			if(tLLFeeMainSet.size()>0){				
				mMMap.put(tLLFeeMainSet, "INSERT");				
			}
			if(tLLCaseReceiptSet.size()>0){
				mMMap.put(tLLCaseReceiptSet, "INSERT");				
			}
			if(tLLCaseOpTimeSet.size()>0){
				mMMap.put(tLLCaseOpTimeSet, "INSERT");
			}
			if(tLLAffixSet.size()>0){
				mMMap.put(tLLAffixSet, "INSERT");
			}
			
		}catch(Exception e){
			point = "E";
			mErrors.addOneError("提交数据库数据准备出错！");
			return false;
		}
		
		return true;
	}


	//处理案件信息
	private boolean dealCaseData() {
		System.out.println("开始处理案件信息！");
		List<Element> caseDatas = caselist.getChildren("LLCASE_DATA");
		for(Iterator<Element> it =  caseDatas.iterator();it.hasNext();){
			Element caseData = it.next();
			//理赔案件信息
			if(!getLLCase(caseData)){
				return false;
			}
			//获得事件信息
			if(!getCaseRelaInfo(caseData)){
				point = "0";
				mErrors.addOneError("处理事件信息错误！客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
				return false;
			}
			//获得账单信息
			if(!dealFeeInfo(caseData)){
				point = "0";
				return false;
			}
			//处理申请材料
			if(!dealCL(caseData)){
				point = "0";
				mErrors.addOneError("处理材料信息错误！客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
				return false;
			}
			
			if(!dealScan(caseData)){
				point = "0";
				mErrors.addOneError("影像件信息错误！客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
				return false;
			}
			
            //记录交易轨迹
            if (!makeHospCase()) {
            	point = "0";
				mErrors.addOneError("记录交易轨迹错误！客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
                return false;
            }
			
			if(null != tLLCaseSchema){				
				tLLCaseSet.add(tLLCaseSchema);
			}
			if(null != tLLAppClaimReasonSchema){				
				tLLAppClaimReasonSet.add(tLLAppClaimReasonSchema);
			}
			if(null != tLLCaseOpTimeSchema){
				tLLCaseOpTimeSet.add(tLLCaseOpTimeSchema);
			}
			System.out.println("处理案件信息--结束！");
		}
		
		return true;
	}
	private boolean dealScan(Element caseData) {
		Element pictureList = caseData.getChild("PICTURE_LIST");
		mPicPath = pictureList.getChildText("PICTURE_PATH");
		//处理人在受理申请的时候被分配的
    	TransferData transferData = new TransferData();
    	transferData.setNameAndValue("PicPath", mPicPath);
    	transferData.setNameAndValue("Operator", operator);
    	transferData.setNameAndValue("ManageCom", ManageCom);
    	VData tVData = new VData();
		tVData.add(tLLCaseSchema);
		tVData.add(transferData);	
		LLGWDealScan tLLGWDealScan = new LLGWDealScan();
		if(!tLLGWDealScan.submitData(tVData,"")) {
			point = "0";
			mErrors.addOneError("图像材料获取、处理失败！客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
			return false;
		}
		return true;
	}


	//TODO 处理申请材料
	private boolean dealCL(Element caseData) {
		try{
			
			
			Element affixList = caseData.getChild("AFFIX_LIST");
			List affixDatas =  affixList.getChildren("AFFIX_DATA");
			int i = 1;
			for(Iterator<Element> it = affixDatas.iterator();it.hasNext();){
				tLLAffixSchema = new LLAffixSchema();
				
				Element affixData = it.next();
				String affixTypeCode = affixData.getChildText("AFFIXTYPECODE");
				String affixCode = affixData.getChildText("AFFIXCODE");
				String supplyDate = affixData.getChildText("SUPPLYDATE");			
				String affixCount = affixData.getChildText("AFFIXCOUNT");
				String shortCount = affixData.getChildText("SHORTCOUNT");
				
				ExeSQL tExeSQl = new ExeSQL();
				String sqlStr = "";
//				select affixtypename from LLMAffix  where affixtypecode='"+affixTypeCode+"' ";	
//				String AffixType = tExeSQl.getOneValue(sqlStr);
//				
				sqlStr = "select affixname from LLMAffix  where affixcode='"+affixCode+"' ";
				String affixName = tExeSQl.getOneValue(sqlStr);
				
				tLLAffixSchema.setAffixType(affixTypeCode);
				tLLAffixSchema.setAffixCode(affixCode);
				tLLAffixSchema.setAffixName(affixName);
				tLLAffixSchema.setSupplyDate(supplyDate);
				tLLAffixSchema.setShortCount(shortCount);
				tLLAffixSchema.setCount(affixCount);
				String rgtaffixno = PubFun1.CreateMaxNo("rgtaffixno", tLimit);
				tLLAffixSchema.setAffixNo(rgtaffixno);
				tLLAffixSchema.setSerialNo(i);
				tLLAffixSchema.setReasonCode("13");
				tLLAffixSchema.setRgtNo(tRgtNo);
				tLLAffixSchema.setCaseNo(tLLCaseSchema.getCaseNo());
				tLLAffixSchema.setProperty("");
				
				tLLAffixSchema.setMakeDate(mMakeDate);
				tLLAffixSchema.setMakeTime(mMakeTime);
				tLLAffixSchema.setModifyDate(mMakeDate);
				tLLAffixSchema.setModifyTime(mMakeTime);
				tLLAffixSchema.setMngCom(tLimit);
				tLLAffixSchema.setOperator(operator);
				
				i++;
				
			}
			tLLAffixSet.add(tLLAffixSchema);
		}catch(Exception e){
			point = "E";
			mErrors.addOneError("材料信息处理错误！");
			return false;
		}
		
		return true;
	}


	//获得账单信息
	private boolean dealFeeInfo(Element caseData) {
		System.out.println("账单信息处理--start");
		Element feeList = caseData.getChild("FEE_LIST");
		if(null == feeList){
			point = "0";
			mErrors.addOneError("没有账单信息！客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
			return false;
		}
		List FeeDatas = feeList.getChildren("FEE_DATA");
		if(FeeDatas.size()>0){			
			for(Iterator<Element> it = FeeDatas.iterator();it.hasNext();){
				Element feeData = it.next();			
				//准备账单信息
				if(!Feeinfo(feeData)){
					point = "0";
					return false;
				}
				if(null!=tLLSecurityReceiptSchema){					
					tLLSecurityReceiptSet.add(tLLSecurityReceiptSchema);
				}
				if(null != tLLFeeMainSchema){					
					tLLFeeMainSet.add(tLLFeeMainSchema);
				}
			}
		}
		
		return true;
	}


	private boolean Feeinfo(Element feeData) {
		tLLFeeMainSchema = new LLFeeMainSchema();
		tLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
		Element sbData = feeData.getChild("SBDATA");
		Element feetypeDatas = feeData.getChild("FEETYPE_LIST");	
		
		String mMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", tLimit);
		String feeDate = feeData.getChildText("FEEDATE");//计算日期 
		String hospitalName = feeData.getChildText("HOSPITALNAME");//医院名称
		String hospitalCode = feeData.getChildText("HOSPITALCODE");//医院代码
		String receiptNo = feeData.getChildText("RECEIPTNO");//账单号码
		String inHosNo = feeData.getChildText("INHOSNO");//住院号
		String hospStartDate = feeData.getChildText("HOSPSTARTDATE");//住院开始时间
		String hospEndDate = feeData.getChildText("HOSPENDDATE");//住院结束时间
		ExeSQL tExeSQl = new ExeSQL();
		String realHospDatesql = "select days('"+hospEndDate+"') - days('"+hospStartDate+"') from dual";	
		String realHospDate = tExeSQl.getOneValue(realHospDatesql);//住院天数  根据出院日期和入院日期得出的住院天数
//		String realHospDate = feeData.getChildText("REALHOSPDATE");
		String feeType = feeData.getChildText("FEETYPE");//账单种类
		if (!"1".equals(feeType) && !"2".equals(feeType) && !"3".equals(feeType)) {
			point = "0";
			mErrors.addOneError("【账单类型】的值不存在!客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
            return false;
        }

        

		tLLFeeMainSchema.setMainFeeNo(mMainFeeNo);
		tLLFeeMainSchema.setFeeDate(feeDate);
		tLLFeeMainSchema.setHospitalName(hospitalName);
		tLLFeeMainSchema.setHospitalCode(hospitalCode);
		tLLFeeMainSchema.setReceiptNo(receiptNo);
		tLLFeeMainSchema.setInHosNo(inHosNo);
		tLLFeeMainSchema.setHospStartDate(hospStartDate);
		tLLFeeMainSchema.setHospEndDate(hospEndDate);
		tLLFeeMainSchema.setRealHospDate(realHospDate);
		tLLFeeMainSchema.setFeeType(feeType);
		tLLFeeMainSchema.setCaseNo(tLLCaseSchema.getCaseNo());
		tLLFeeMainSchema.setCaseRelaNo(tLLCaseRelaSchema.getCaseRelaNo());
		tLLFeeMainSchema.setRgtNo(tRgtNo);
		tLLFeeMainSchema.setCustomerNo(tLLCaseSchema.getCustomerNo());
		tLLFeeMainSchema.setCustomerName(tLLCaseSchema.getCustomerName());
		//基本信息
		tLLFeeMainSchema.setOperator(operator);		
		tLLFeeMainSchema.setMakeDate(mMakeDate);
		tLLFeeMainSchema.setMakeTime(mMakeTime);
		tLLFeeMainSchema.setModifyDate(mMakeDate);
		tLLFeeMainSchema.setModifyTime(mMakeTime);
		tLLFeeMainSchema.setMngCom(ManageCom);
		
		if(null != sbData){
			
			//TODO 
			String SelfAmnt = feeData.getChildText("SELFAMNT");//自费
			String SelfPay2 = feeData.getChildText("SELFPAY2");//乙类自理
			String SelfPay1 = feeData.getChildText("SELFPAY1");//自负部分
			String GetLimit = feeData.getChildText("GETLIMIT");//起付线
			String SmallDoorPay = feeData.getChildText("SMALLDOORPAY");//小额门急诊
			String HighDoorAmnt = feeData.getChildText("HIGHDOORAMNT");//大额门急诊
			
			String applyAmnt = feeData.getChildText("APPLYAMNT");//合计金额
			String planFee = feeData.getChildText("PLANFEE");//统筹基金支付
			String supInhosFee = feeData.getChildText("SUPINHOSFEE");//大额医疗支付
			String officialSubsidy = feeData.getChildText("OFFICIALSUBSIDY");//公务员医疗补助
			String retireAddFee = feeData.getChildText("RETIREADDFEE");//退休补充医疗
			String reimburSement = feeData.getChildText("REIMBURSEMENT");//可报销范围内金额
			
			
			tLLSecurityReceiptSchema.setSelfAmnt(SelfAmnt);
			tLLSecurityReceiptSchema.setSelfPay2(SelfPay2);
			tLLSecurityReceiptSchema.setSelfPay1(SelfPay1);
			tLLSecurityReceiptSchema.setGetLimit(GetLimit);
			tLLSecurityReceiptSchema.setSmallDoorPay(SmallDoorPay);
			tLLSecurityReceiptSchema.setHighDoorAmnt(HighDoorAmnt);
			
			
			tLLSecurityReceiptSchema.setApplyAmnt(applyAmnt);
			tLLSecurityReceiptSchema.setPlanFee(planFee);
			tLLSecurityReceiptSchema.setSupInHosFee(supInhosFee);
			tLLSecurityReceiptSchema.setOfficialSubsidy(officialSubsidy);
			tLLSecurityReceiptSchema.setRetireAddFee(retireAddFee);
			tLLFeeMainSchema.setReimbursement(reimburSement);	
			
			String tLimit1 = PubFun.getNoLimit("86");
			tLLSecurityReceiptSchema.setFeeDetailNo(
	                PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
			tLLSecurityReceiptSchema.setMainFeeNo(mMainFeeNo);
			tLLSecurityReceiptSchema.setRgtNo(tRgtNo);
			tLLSecurityReceiptSchema.setCaseNo(tLLCaseSchema.getCaseNo());
			
			//基本信息
			tLLSecurityReceiptSchema.setOperator(operator);		
			tLLSecurityReceiptSchema.setMakeDate(mMakeDate);
			tLLSecurityReceiptSchema.setMakeTime(mMakeTime);
			tLLSecurityReceiptSchema.setModifyDate(mMakeDate);
			tLLSecurityReceiptSchema.setModifyTime(mMakeTime);
			tLLSecurityReceiptSchema.setMngCom(ManageCom);
			
		}
		
		if(null!=feetypeDatas){
			double feeAll = 0.0;
			List<Element> feetypedataMap = feetypeDatas.getChildren("FEETYPE_DATA");
			for(Iterator<Element> it = feetypedataMap.iterator();it.hasNext();){
				tLLCaseReceiptSchema = new LLCaseReceiptSchema();
				Element feeTypeData = it.next();
				String feeItemCode = feeTypeData.getChildText("FEEITEMCODE");
				String fee = feeTypeData.getChildText("FEE");
				String feeItemName = feeTypeData.getChildText("FEEITEMNAME");	
//		        找到费用项目对应名称
		        String tSQL = "select codename   from ldcode where codetype ='llfeeitemtype' and code='"+feeItemCode+"' ";
		        
		        ExeSQL tExeSQL = new ExeSQL();
		        String tFeeitemName = tExeSQL.getOneValue(tSQL);
		        if("".equals(tFeeitemName)||null==tFeeitemName||"null".equals(tFeeitemName)){
		        	point = "0";
		        	mErrors.addOneError("【费用项目代码】的值不存在,客户姓名:"+tLLCaseSchema.getCustomerName()+",客户号"+tLLCaseSchema.getCustomerNo()+".");
		             return false;
		        }
				String tLimit1 = PubFun.getNoLimit("86");
				tLLCaseReceiptSchema.setFeeDetailNo(
		                PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
				tLLCaseReceiptSchema.setFeeItemCode(feeItemCode);
				tLLCaseReceiptSchema.setFee(fee);
				tLLCaseReceiptSchema.setFeeItemName(feeItemName);
				tLLCaseReceiptSchema.setMainFeeNo(mMainFeeNo);
				tLLCaseReceiptSchema.setRgtNo(tRgtNo);
				tLLCaseReceiptSchema.setCaseNo(tLLCaseSchema.getCaseNo());
				//基本信息
				tLLCaseReceiptSchema.setOperator(operator);		
				tLLCaseReceiptSchema.setMakeDate(mMakeDate);
				tLLCaseReceiptSchema.setMakeTime(mMakeTime);
				tLLCaseReceiptSchema.setModifyDate(mMakeDate);
				tLLCaseReceiptSchema.setModifyTime(mMakeTime);
				tLLCaseReceiptSchema.setMngCom(ManageCom);
				feeAll += Double.parseDouble(fee);
				
				tLLCaseReceiptSet.add(tLLCaseReceiptSchema);
				
			}
			tLLFeeMainSchema.setSumFee(feeAll);
		}
		
		return true;
	}


	//处理事件表示信息
	private boolean getCaseRelaInfo(Element caseData) {
		System.out.println("事件信息处理---Start");
		Element eventList = caseData.getChild("EVENT_LIST");
		List<Element> eventDatas = eventList.getChildren("EVENT_DATA");
		for(Iterator<Element> it = eventDatas.iterator();it.hasNext();){
			tLLCaseRelaSchema = new LLCaseRelaSchema();
			tLLSubReportSchema = new LLSubReportSchema();
			Element eventData = it.next();
			//事件信息
			String SubRptNo = eventData.getChildText("SUBRPTNO");
			String AccDate = eventData.getChildText("ACCDATE");
			String AccPlace = eventData.getChildText("ACCPLACE");
			String inHospitalDate = eventData.getChildText("INHOSPITALDATE");
			String outHospitalDate = eventData.getChildText("OUTHOSPITALDATE");
			String accDesc = eventData.getChildText("ACCDESC");
			String accIdentType = eventData.getChildText("ACCIDENTTYPE");
			//关联表流水号
			String caserela = PubFun1.CreateMaxNo("CASERELANO", tLimit);
			
			tLLCaseRelaSchema.setCaseRelaNo(caserela);
			tLLCaseRelaSchema.setCaseNo(tLLCaseSchema.getCaseNo());
			tLLCaseRelaSchema.setSubRptNo(SubRptNo);
			//打包
			tLLCaseRelaSet.add(tLLCaseRelaSchema);
		
			tLLSubReportSchema.setAccDate(AccDate);
			tLLSubReportSchema.setAccPlace(AccPlace);
			tLLSubReportSchema.setInHospitalDate(inHospitalDate);
			tLLSubReportSchema.setOutHospitalDate(outHospitalDate);
			tLLSubReportSchema.setAccDesc(accDesc);
			tLLSubReportSchema.setAccidentType(accIdentType);			
			tLLSubReportSchema.setMakeDate(mMakeDate);
            tLLSubReportSchema.setMakeTime(mMakeTime);
            tLLSubReportSchema.setOperator(operator);
            tLLSubReportSchema.setMngCom(ManageCom);        
            tLLSubReportSchema.setModifyDate(mMakeDate);
            tLLSubReportSchema.setModifyTime(mMakeTime);			
			//打包
            tLLSubReportSet.add(tLLSubReportSchema);
            
            
		}
		return true;
	}

	//准备LLcase表数据
	private boolean getLLCase(Element caseData) {
		try{
			tLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
			tLLCaseSchema = new LLCaseSchema();
			tLLAppClaimReasonSchema = new LLAppClaimReasonSchema();
			
			String reasonCode = caseData.getChildText("REASONCODE");
			String reason = caseData.getChildText("REASON");
			String custState = caseData.getChildText("CUSTSTATE");
			String insuredNo = caseData.getChildText("INSUREDNO");
			String name = caseData.getChildText("NAME");
			String idType = caseData.getChildText("IDTYPE");
			String idNo = caseData.getChildText("IDNO");
			String birthday = caseData.getChildText("BIRTHDAY");
			String bankCode = caseData.getChildText("BANKCODE");
			String bandAccNo = caseData.getChildText("BANKACCNO");
			String AccName = caseData.getChildText("ACCNAME");
			if(!name.equals(AccName)){
				point = "0";
				mErrors.addOneError("被保人"+name+"与其银行账户名不相同！");
				return false;
			}
			
			
			String CaseNo = PubFun1.CreateMaxNo("CaseNo", tLimit);
			
			//核心体统处理完成后，案件处于受理状态
			tLLCaseSchema.setRgtState("01");
			tLLCaseSchema.setRgtType("1");
			//tLLCaseSchema.setCustomerAge(CustAge);
			//tLLCaseSchema.setReceiptFlag(ReceiptFlag);
			tLLCaseSchema.setRgtNo(tRgtNo);
			tLLCaseSchema.setCaseNo(CaseNo);
			//tLLCaseSchema.setHandler(strHandler);
			//tLLCaseSchema.setDealer(strHandler);
			//出险人基本信息
			tLLCaseSchema.setCustomerNo(insuredNo);
			tLLCaseSchema.setCustomerName(name);
			tLLCaseSchema.setCustBirthday(birthday);
			tLLCaseSchema.setIDType(idType);
			tLLCaseSchema.setIDNo(idNo);
			
			int CustAge = calAge(); 
			tLLCaseSchema.setCustomerAge(CustAge);
			//银行账户信息
			tLLCaseSchema.setBankCode(bankCode);
			tLLCaseSchema.setBankAccNo(bandAccNo);
			tLLCaseSchema.setAccName(AccName);
			tLLCaseSchema.setCustState(custState);
			tLLCaseSchema.setCaseProp("14");//新添加一种CaseProp 14 移动官网
			//社保人员操作支持？？
			
			//由于申请人在系统对接中没有体现，默认申请人由团单号从核心业务系统中带出
			//理赔人员配置
			//该团体保单的管理机构，在该管理机构查询得到合适的理赔人员
			strHandler = tLLCaseCommon.ChooseAssessor(ManageCom,insuredNo, "01");
			//如果没有，则从该机构的上级机构查询得到合适的理赔人员。管理机构默认为团单所在的管理机构
	  	  	if("".equals(strHandler)||strHandler==null){
	  	  		strHandler = tLLCaseCommon.ChooseAssessor(ManageCom,insuredNo, "04");
	  	  	}
	  	  	if("".equals(strHandler)||strHandler==null){
				mErrors.addOneError("机构"+ManageCom+"没有适合的理赔人员!");
	  	  		return false;
	  	  	}
	  	  	System.out.println("分配理赔人员："+strHandler);
			
	  	  	operator=strHandler;
	  	  	
			//分配理赔人员
			tLLCaseSchema.setClaimer(operator);
			tLLCaseSchema.setHandler(strHandler);
            tLLCaseSchema.setDealer(strHandler);
			
			tLLCaseSchema.setGrpNo(tLLRegisterSchema.getCustomerNo());//团体客户号
			tLLCaseSchema.setGrpName(tLLRegisterSchema.getGrpName());//单位名称
			
			tLLCaseSchema.setRigister(operator);
			tLLCaseSchema.setOperator(operator);
			tLLCaseSchema.setMngCom(ManageCom);//管理机构
			tLLCaseSchema.setRgtDate(mMakeDate);//申请日期
			tLLCaseSchema.setMakeDate(mMakeDate);
			tLLCaseSchema.setMakeTime(mMakeTime);
			tLLCaseSchema.setModifyDate(mMakeDate);
			tLLCaseSchema.setModifyTime(mMakeTime);
			
			LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
			
			mLLCaseOpTimeSchema.setStartDate(mMakeDate);
			mLLCaseOpTimeSchema.setStartTime(mMakeTime);
			mLLCaseOpTimeSchema.setCaseNo(CaseNo);
            mLLCaseOpTimeSchema.setOperator(operator);
            mLLCaseOpTimeSchema.setManageCom(ManageCom);
            
            tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(mLLCaseOpTimeSchema);
            
			//申请原因表
			tLLAppClaimReasonSchema.setReason(reason);
			tLLAppClaimReasonSchema.setRgtNo(tRgtNo);
			tLLAppClaimReasonSchema.setCaseNo(CaseNo);
			tLLAppClaimReasonSchema.setReasonType("0");
			tLLAppClaimReasonSchema.setReasonCode(reasonCode);
			tLLAppClaimReasonSchema.setMngCom(ManageCom);
			tLLAppClaimReasonSchema.setOperator(operator);
			tLLAppClaimReasonSchema.setCustomerNo(insuredNo);
			tLLAppClaimReasonSchema.setMakeDate(mMakeDate);
			tLLAppClaimReasonSchema.setMakeTime(mMakeTime);
			tLLAppClaimReasonSchema.setModifyDate(mMakeDate);
			tLLAppClaimReasonSchema.setModifyTime(mMakeTime);
			
			
			
			
			
		}catch(Exception e){
			point = "E";
			mErrors.addOneError("llcase准备错误！");
			return false;
		}
         return true;
	}


	//处理申请信息
	private boolean dealRegister() {
		
		try{
			
			
		
			System.out.println("理赔受理团体申请！--start");
			String registerInfo = "select  a.customerno, a.name, g.grpcontno,g.Peoples2, a.claimbankcode, a.claimbankaccno, a.claimaccname, a.AddressNo,'', b.riskcode from lcgrpcont g, LCGrpAppnt a,lcgrppol b where '1471829074000'='1471829074000' and  a.grpcontno = g.grpcontno and g.appflag = '1' and b.grpcontno=g.grpcontno  and g.GrpContNo='"+grpContno+"'  fetch first 3000 rows only with ur ";
			ExeSQL tExeSQl = new ExeSQL();
			SSRS tSSRS = new SSRS();
			tSSRS = tExeSQl.execSQL(registerInfo);
			String[][] data1 = tSSRS.getAllData();
			String registerInfo1 = "select linkman1,phone1,GrpAddress from LCGrpAddress where '1471837248000'='1471837248000' and  customerno = '"+data1[0][0]+"' and addressno = '"+data1[0][7]+"' fetch first 1 rows only with ur";
			SSRS tSSRS2 = new SSRS();
			tSSRS2 = tExeSQl.execSQL(registerInfo1);
			String[][] data2 = tSSRS2.getAllData();
			System.out.println(data1[0][0]);
			tLLRegisterSchema.setCustomerNo(data1[0][0]);//团体客户号
			tLLRegisterSchema.setGrpName(data1[0][1]);//单位名称
			tLLRegisterSchema.setAppPeoples(peoNum);//申请理赔人数
			tLLRegisterSchema.setRgtantName(data2[0][0]);//联系人
			tLLRegisterSchema.setRgtantPhone(data2[0][1]);//联系电话
			tLLRegisterSchema.setRgtantAddress(data2[0][2]);//联系地址
			tLLRegisterSchema.setTogetherFlag(togetherFlag);//给付类型
			tLLRegisterSchema.setRgtType("9");//受理方式  需要改成10  目前未扩展字段先用9
			tLLRegisterSchema.setCaseGetMode("4");
			tLLRegisterSchema.setRgtClass("1");
			tLLRegisterSchema.setRgtObj("0");
			tLLRegisterSchema.setRgtObjNo(grpContno);
			
			
			
			System.out.println("管理机构代码是 : "+tLimit);
			String RGTNO = PubFun1.CreateMaxNo("RGTNO", tLimit);
			tRgtNo = RGTNO;
			VData tno = new VData();
			tno.add(0,tLLRegisterSchema.getAppPeoples()+"");
			tno.add(1,RGTNO);
			tno.add(2,"MAX");
			PubFun1.CreateMaxNo("CaseNo",tLimit,tno);
			System.out.println("getInputData start......");
			tLLRegisterSchema.setRgtNo(RGTNO);
			tLLRegisterSchema.setRgtState("01");
			
			tLLRegisterSchema.setApplyerType("0");
			tLLRegisterSchema.setRgtDate(mMakeDate);
			tLLRegisterSchema.setMngCom(ManageCom);
			
			tLLRegisterSchema.setMakeDate(mMakeDate);
			tLLRegisterSchema.setMakeTime(mMakeTime);
			tLLRegisterSchema.setModifyDate(mMakeDate);
			tLLRegisterSchema.setModifyTime(mMakeTime);
			//银行账户
			String tSqlc="select ClaimBankCode,ClaimBankAccNo,ClaimAccName from LCGrpAppnt where GrpContNo='"+grpContno+"' union select ClaimBankCode,ClaimBankAccNo,ClaimAccName from LbGrpAppnt where GrpContNo='"+grpContno+"'";
			    
			ExeSQL tExeSQL = new ExeSQL();
			SSRS bankInfo=tExeSQL.execSQL(tSqlc);
			String[][] bankInfos = bankInfo.getAllData();
			//选择全部统一给付，若核心业务系统中未定义团体账户信息,官网提示“受理失败，请联系公司客服人员，办理团体单位赔款领取银行账户设置”
			if("3".equals(togetherFlag)){
				if("".equals(bankInfos[0][0])||"".equals(bankInfos[0][1])||"".equals(bankInfos[0][2])||bankInfos[0][0]==null||bankInfos[0][1]==null||bankInfos[0][2]==null){
					point = "0";
					mErrors.addOneError("受理失败，请联系公司客服人员，办理团体单位赔款领取银行账户设置");
					return false;
				}
			}
			
			if(bankInfo!=null){
				tLLRegisterSchema.setBankCode(bankInfos[0][0]);
				tLLRegisterSchema.setBankAccNo(bankInfos[0][1]);
				tLLRegisterSchema.setAccName(bankInfos[0][2]);
			}
			

			System.out.println("理赔受理团体申请！--end");
		}catch(Exception e){
			e.getStackTrace();
			point = "E";
			return false;
		}
	
		return true;
	}
	 /**
     * 生成对外接口案件表
     * @return boolean
     * @throws Exception
     */
    private boolean makeHospCase()  {
        System.out.println("LLPadAutoClaim--makeHospCase");
        mLLHospCaseSchema = new LLHospCaseSchema();
        mLLHospCaseSchema.setCaseNo(tLLCaseSchema.getCaseNo());
        mLLHospCaseSchema.setHospitCode(requestType);
        mLLHospCaseSchema.setHandler(tLLCaseSchema.getHandler());
        mLLHospCaseSchema.setAppTranNo(mTransactionNum);
        mLLHospCaseSchema.setAppDate(mMakeDate);
        mLLHospCaseSchema.setAppTime(mMakeTime);
        mLLHospCaseSchema.setDealType("2"); 
        mLLHospCaseSchema.setCaseType("07");//官网理赔接口
        mLLHospCaseSchema.setConfirmState("1");
        mLLHospCaseSchema.setMakeDate(mMakeDate);
        mLLHospCaseSchema.setMakeTime(mMakeTime);
        mLLHospCaseSchema.setModifyDate(mMakeDate);
        mLLHospCaseSchema.setModifyTime(mMakeTime);
        mLLHospCaseSet.add(mLLHospCaseSchema);
        return true;
    }
	private int calAge() {
        Date Birthday = fDate.getDate(tLLCaseSchema.getCustBirthday());
        String strNow = mMakeDate;
        Date aNow = fDate.getDate(strNow);
        int CustomerAge = PubFun.calInterval(Birthday, aNow, "Y");
        System.out.println("年龄计算函数。。。"+CustomerAge);
        return CustomerAge;
    }

	
	private boolean prepareOutputData() {
	    try {
	      
	      mResult.clear();
	      mResult.add(mMMap);
	    }
	    catch (Exception ex) {
	      // @@错误处理
	      CError tError = new CError();
	      tError.moduleName = "LLGWCaseRegisterBL";
	      tError.functionName = "prepareData";
	      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    return true;
	 }
	
	
	private Document CreatErrorXml() {
		Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        
        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(requestType);
        tHeadData.addContent(tRequestType);

        Element tResponseType = new Element("RESPONSE_TYPE");
        tResponseType.setText(mresqonseType);
        tHeadData.addContent(tResponseType);
        
        //System.out.println(tResponseType.getText());
        
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(point);
        tBody.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        
        return tDocument;
	}

	
	//以下方法为测试专用方法
	public static void main(String args[]){
		try{
			
			LLGWCaseRegisterBL gwbl = new LLGWCaseRegisterBL();
		/*	File file = new File("e:/xuyp.xml");
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
			
			SAX reader = new SAXReader();           
			Document   document = reader.read(in);
			*/
			SAXBuilder sb=new SAXBuilder();
			 Document doc=sb.build("D:/testing/LLGW/GW03.xml");
			 Document response =  gwbl.service(doc);
			 doc2XML(response);
		}catch(Exception e){
			e.getStackTrace();
		}
	}
	
	public static void doc2XML(Document doc) throws Exception{
		
		XMLOutputter xmloutputter = new XMLOutputter();
	    OutputStream outputStream;
	    try {
	      outputStream = new FileOutputStream("D:\\testing\\1111outputGW03.xml");
	      xmloutputter.output(doc, outputStream);
	      System.out.println("xml文档生成成功！");
	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	}
	
	
}
