package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLTreatmentDetailsSchema;
import com.sinosoft.lis.vschema.LLTreatmentDetailsSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 接受案件分账单信息</p>
 *
 * <p>Description: 接受案件分账单信息</p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author 
 * @version 1.0
 */
public class LLTJMajorDiseasesTreatmentDetails implements ServiceInterface
{
    
    public LLTJMajorDiseasesTreatmentDetails(){}
    
    public CErrors mErrors = new CErrors();
    private MMap mMMap = new MMap();
    public VData mResult = new VData();
    private String mErrorMessage = "";  //错误描述    
    
    /**
     * 天津大病默认配置
     */
    private String mManageCom="86120000";   //管理机构
    private String mOperator="ss1201";   //管理机构
    
    private List tList = new ArrayList();//返回信息合集
    //报文头信息
    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private String mResponseCode = "1";     //返回类型代码    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    
    //基本信息
    private int mCaseNum = 0;
    private GlobalInput tG=new GlobalInput();
    
   
    //批次号
    private String mRgtNo = "";
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    
    //处理结果标记
    private boolean mDealState=true;  
    private Element mLLCaseList;

    public Document service(Document pInXmlDoc)
    {
    	//TODO 开始webservice 
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{
                    	mResult.clear();
                    	mResult.add(mMMap);
                    	try{
                        	PubSubmit tPubSubmit = new PubSubmit();
                            if (!tPubSubmit.submitData(mResult, "")) {
                            	 mDealState = false;
                            }
                    	} catch (Exception ex) {
                             mDealState = false;
                             mResponseCode = "E";
                             buildError("service()", "系统未知错误");
                    	}
                    }
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	mInXmlDoc = createXML();
        }
        return mInXmlDoc;
    }
    
    
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJMajorDiseasesTreatmentDetails--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
    
        
        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mLLCaseList = tBodyData.getChild("LLCASELIST");          //理赔信息    
        tG.ManageCom=mManageCom;
        tG.Operator=mOperator;
       
        return true;

    }
    
    
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误!");
            return false;
        }

        if (!"DB03".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码个数错误!");
            return false;
        }

        if (!"DB03".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86120000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误!");
            return false;
        }
                     
        return true;

    }
    
    
    /**
     * 处理报文信息
     *
     */
    private boolean dealData(){
    	//TODO 业务逻辑处理
    	//1.单表存储
    	if(!dealTreatmentDetails()){
    		return false;
    	}else{
    		//受理出错,跳出处理
        	if(!mDealState){
        		return false;
        	}
    	}
         
        return true;
    }
    


	/**
     * 诊疗明细导入
     * @return
     */
    private boolean dealTreatmentDetails() {
		// TODO 诊疗明细导入
    	System.out.println(tG.Operator+"===="+tG.ManageCom);
    	
    	//1.首先解析传入报文
        if(mLLCaseList.getChildren("LLCASE_DATA").size()>0){
        	LLTreatmentDetailsSet tLLTreatmentDetailsSet = new LLTreatmentDetailsSet(); 
           
            for(int i=0;i<mLLCaseList.getChildren("LLCASE_DATA").size();i++){
                Element mLLCaseInfo=(Element)mLLCaseList.getChildren("LLCASE_DATA").get(i);
                if(mLLCaseInfo!=null){
                    //1.调用报文解析类将数据封装到类中
                	LLTJMajorDiseasesTreatmentParser tLLTJMajorDiseasesTreatmentParser=new LLTJMajorDiseasesTreatmentParser(mLLCaseInfo);                
                    LLTreatmentDetailsSchema tLLTreatmentDetailsSchema = new LLTreatmentDetailsSchema();
                                      
                    String tRgtNo = tLLTJMajorDiseasesTreatmentParser.getRGTNO();
                    String tCaseNo = tLLTJMajorDiseasesTreatmentParser.getCASENO();
                    String tCureNo = tLLTJMajorDiseasesTreatmentParser.getCURENO();
                    String tCureType = tLLTJMajorDiseasesTreatmentParser.getCURETYPE();
                    String tHospitalCode = tLLTJMajorDiseasesTreatmentParser.getHOSPITALCODE();
                    String tSerProNo = tLLTJMajorDiseasesTreatmentParser.getSERPRONO();
                    String tNoMeanNo = tLLTJMajorDiseasesTreatmentParser.getNOMEANNO();
                    String tUnifiedPro = tLLTJMajorDiseasesTreatmentParser.getUNIFIEDPRO();
                    String tSerProName = tLLTJMajorDiseasesTreatmentParser.getSERPRONAME();
                    String tHosSerProName = tLLTJMajorDiseasesTreatmentParser.getHOSSERPRONAME();
                    String tSelfRate = tLLTJMajorDiseasesTreatmentParser.getSELFRATE();
                    String tReagentType = tLLTJMajorDiseasesTreatmentParser.getREAGENTTYPE();
                    String tSpec = tLLTJMajorDiseasesTreatmentParser.getSPEC();
                    String tPrice = tLLTJMajorDiseasesTreatmentParser.getPRICE();
                    String tQuantity = tLLTJMajorDiseasesTreatmentParser.getQUANTITY();
                    String tAccMoney = tLLTJMajorDiseasesTreatmentParser.getACCMONEY();
                    String tSelfMoney = tLLTJMajorDiseasesTreatmentParser.getSELFMONEY();
                    String tAddSelfMoney = tLLTJMajorDiseasesTreatmentParser.getADDSELFMONEY();
                    String tAppAmnt = tLLTJMajorDiseasesTreatmentParser.getAPPAMNT();
                    String tHandMoney = tLLTJMajorDiseasesTreatmentParser.getHANDMONEY();
                    String tDeclineAmnt = tLLTJMajorDiseasesTreatmentParser.getDECLINEAMNT();
                    String tAmntAccdate = tLLTJMajorDiseasesTreatmentParser.getAMNTACCDATE();
                    String tNo = tLLTJMajorDiseasesTreatmentParser.getNO();
                    String tBillNo = tLLTJMajorDiseasesTreatmentParser.getBILLNO();
                    String tUseMethod = tLLTJMajorDiseasesTreatmentParser.getUSEMETHOD();
                    String tUseLevel = tLLTJMajorDiseasesTreatmentParser.getUSELEVEL();
                    String tUseDays = tLLTJMajorDiseasesTreatmentParser.getUSEDAYS();
                    String tAccDesc = tLLTJMajorDiseasesTreatmentParser.getACCDESC();
                    String tRemark = tLLTJMajorDiseasesTreatmentParser.getREMARK();
                    String tMngCom = tG.ManageCom;
                    String tOperator = tG.Operator;
                    String tMakeDate = mCurrentDate;
                    String tMakeTime = mCurrentTime;
                    String tModifyDate = mCurrentDate;
                    String tModifyTime = mCurrentTime;
                    
                    /*
                     * 基本校验
                     */      
                    if (tRgtNo == null || "".equals(tRgtNo) || tRgtNo.equals("null")) {
                        buildError("dealData", "【批次号】的值不能为空");
                        return false;
                    }else{
                    	mRgtNo = tRgtNo;
                    }
                    if (tCaseNo == null || "".equals(tCaseNo) || tCaseNo.equals("null")) {
                        buildError("dealData", "【案件号】的值不能为空");
                        return false;
                    }
                    if (tCureNo == null || "".equals(tCureNo)|| tCureNo.equals("null")) {
                        buildError("dealData", "【就诊顺序号】的值不能为空");
                        return false;
                    }
                    if (tCureType == null || "".equals(tCureType)|| tCureType.equals("null")) {
                        buildError("dealData", "【汇总类型】的值不能为空");
                        return false;
                    }
                    if (tHospitalCode == null || "".equals(tHospitalCode) || tHospitalCode.equals("null")) {
                        buildError("dealData", "【医院编码】的值不能为空");
                        return false;
                    }
                    if (tSerProNo == null || "".equals(tSerProNo) || tSerProNo.equals("null")) {
                        buildError("dealData", "【服务项目大类编码】的值不能为空");
                        return false;
                    }
                    if (tNoMeanNo == null || "".equals(tNoMeanNo) || tNoMeanNo.equals("null")) {
                        buildError("dealData", "【无含义项目编码顺序号】的值不能为空");
                        return false;
                    }
                    if (tUnifiedPro == null || "".equals(tUnifiedPro) || tUnifiedPro.equals("null")) {
                        buildError("dealData", "【统一项目】的值不能为空");
                        return false;
                    }
                    if (tSerProName == null || "".equals(tSerProName) || tSerProName.equals("null")) {
                        buildError("dealData", "【服务项目名称】的值不能为空");
                        return false;
                    }
                    if (tHosSerProName == null || "".equals(tHosSerProName) || tHosSerProName.equals("null")) {
                        buildError("dealData", "【医院服务项目名称】的值不能为空");
                        return false;
                    }
                    if (tSelfRate == null || "".equals(tSelfRate) || tSelfRate.equals("null")) {
                        buildError("dealData", "【本项目自付比例】的值不能为空");
                        return false;
                    }
                    if (tReagentType == null || "".equals(tReagentType) || tReagentType.equals("null")) {
                        buildError("dealData", "【剂型】的值不能为空");
                        return false;
                    }
                    if (tSpec == null || "".equals(tSpec) || tSpec.equals("null")) {
                        buildError("dealData", "【规格】的值不能为空");
                        return false;
                    }
                    
                    //2.数据存储
                    String tDetailNo = "";
                    tDetailNo = PubFun1.CreateMaxNo("LLTREATMENT",
                			mManageCom);
                    tLLTreatmentDetailsSchema.setCaseNo(tCaseNo);
                    tLLTreatmentDetailsSchema.setDetailNo(tDetailNo);
                    tLLTreatmentDetailsSchema.setRgtNo(tRgtNo);
                    tLLTreatmentDetailsSchema.setCureNo(tCureNo);
                    tLLTreatmentDetailsSchema.setCureType(tCureType);
                    tLLTreatmentDetailsSchema.setHospitalCode(tHospitalCode);
                    tLLTreatmentDetailsSchema.setSerProNo(tSerProNo);
                    tLLTreatmentDetailsSchema.setNoMeanNo(tNoMeanNo);
                    tLLTreatmentDetailsSchema.setUnifiedPro(tUnifiedPro);
                    tLLTreatmentDetailsSchema.setSerProName(tSerProName);
                    tLLTreatmentDetailsSchema.setHosSerProName(tHosSerProName);
                    tLLTreatmentDetailsSchema.setSelfRate(tSelfRate);
                    tLLTreatmentDetailsSchema.setReagentType(tReagentType);
                    tLLTreatmentDetailsSchema.setSpec(tSpec);
                    tLLTreatmentDetailsSchema.setPrice(tPrice);
                    tLLTreatmentDetailsSchema.setQuantity(tQuantity);
                    tLLTreatmentDetailsSchema.setAccMoney(tAccMoney);
                    tLLTreatmentDetailsSchema.setSelfMoney(tSelfMoney);
                    tLLTreatmentDetailsSchema.setAddSelfMoney(tAddSelfMoney);
                    tLLTreatmentDetailsSchema.setAppAmnt(tAppAmnt);
                    tLLTreatmentDetailsSchema.setHandMoney(tHandMoney);
                    tLLTreatmentDetailsSchema.setDeclineAmnt(tDeclineAmnt);
                    tLLTreatmentDetailsSchema.setAmntAccdate(tAmntAccdate);
                    tLLTreatmentDetailsSchema.setNo(tNo);
                    tLLTreatmentDetailsSchema.setBillNo(tBillNo);
                    tLLTreatmentDetailsSchema.setUseMethod(tUseMethod);
                    tLLTreatmentDetailsSchema.setUseLevel(tUseLevel);
                    tLLTreatmentDetailsSchema.setUseDays(tUseDays);
                    tLLTreatmentDetailsSchema.setAccDesc(tAccDesc);
                    tLLTreatmentDetailsSchema.setRemark(tRemark);
                    tLLTreatmentDetailsSchema.setMngCom(tMngCom);
                    tLLTreatmentDetailsSchema.setOperator(tOperator);
                    tLLTreatmentDetailsSchema.setMakeDate(tMakeDate);
                    tLLTreatmentDetailsSchema.setMakeTime(tMakeTime);
                    tLLTreatmentDetailsSchema.setModifyDate(tModifyDate);
                    tLLTreatmentDetailsSchema.setModifyTime(tModifyTime);
                   

                    mCaseNum = mCaseNum+1;                                        
                	tList.add(mCaseNum+","+tCaseNo+","+tCureNo);	                      
                    System.out.println("tList:"+tList.size());	  
                    
                    tLLTreatmentDetailsSet.add(tLLTreatmentDetailsSchema);
                    
                }else{
                    buildError("dealData()", "获取诊疗明细信息出错!");
                    mDealState = false;
                    return false;
                }
            }
            mMMap.put(tLLTreatmentDetailsSet, "DELETE&INSERT");
        }else{
            buildError("dealData()", "报文中没有诊疗明细信息!");
            mDealState = false; 
            return false;
        }
         
        return true;
    
	}

	/**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState || "".equals(mRgtNo)) {
            return createFalseXML();
        } else {
        	try
        	{
        		return createResultXML(mRgtNo);
        	}
        	catch (Exception e)
        	{          
        		e.printStackTrace();
        		buildError("createXML()", "生成返回报文错误!");
        		return createFalseXML();
        	}
        }
    }  
    
    
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        System.out.println("LLTJMajorDiseasesTreatmentDetails--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        Element tBodyData = new Element("BODY");
        
        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }else{
        	mResponseCode = "1";
        }
        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBodyData.addContent(tResponseCode);

        mErrorMessage = mErrors.getFirstError();
        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBodyData.addContent(tErrorMessage);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBodyData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
    
    
    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aRgtNo) throws Exception {
         System.out.println("LLTJMajorDiseasesCaseRegister--createResultXML(正常返回报文)");
         Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);


         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tBackData = new Element("BACK_DATA");
         tBodyData.addContent(tBackData);
         
         //Back_Data部分
         Element tRgtNO = new Element("RGTNO");
                 
         tRgtNO.setText(aRgtNo);
         tBackData.addContent(tRgtNO);
         
         
         //返回的案件处理结构
         Element tLLCaseList = new Element("LLCASELIST");
         
         System.out.println("开始返回报文生成--list的长度"+tList.size());
         if(tList!=null && tList.size()>0){
             
             for(int i=0;i< tList.size();i++){
                 System.out.println("开始返回报文生成:"+i);
                 String[] tresult=tList.get(i).toString().split(",");
                 
                 System.out.println(tresult[0]+"==="+tresult[1]);
                 Element tLLCaseData = new Element("LLCASE_DATA");
                 Element tCASENUM = new Element("CASENUM");
                 tCASENUM.setText(tresult[0]);
                 Element tCASENO = new Element("CASENO");
                 
                 Element tClaimno = new Element("CLAIMNO");
                 
                 tCASENO.setText(tresult[1]);
                 tClaimno.setText(tresult[2]);
                 tLLCaseData.addContent(tCASENUM);
                 tLLCaseData.addContent(tCASENO);
                 tLLCaseData.addContent(tClaimno);
                 tLLCaseList.addContent(tLLCaseData);
             }
         }
         tBodyData.addContent(tLLCaseList);
         tRootData.addContent(tBodyData);
         
         
         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJSocialSecurityRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
    
    
    public static void main(String[] args) {
    	Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/TreatmentTest.xml"), "GBK");
            LLTJMajorDiseasesTreatmentDetails tBusinessDeal = new LLTJMajorDiseasesTreatmentDetails();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
