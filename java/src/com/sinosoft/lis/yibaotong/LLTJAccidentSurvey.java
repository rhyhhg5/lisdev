package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.utility.ExeSQL;
import java.text.DecimalFormat;
import java.io.FileInputStream;

import javax.print.attribute.standard.MediaSize.Other;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLTJAccidentSurvey implements ServiceInterface {
    public LLTJAccidentSurvey() {  
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput(); 
    private MMap mMMap = new MMap();
    private String mErrorMessage = "";	//错误描述    
   
    private Document mInXmlDoc;	 			//传入报文    
    private String mDocType = "";			//报文类型    
    private boolean mDealState = true;		//处理标志    
    private String mResponseCode = "1";		//返回类型代码    
    private String mRequestType = "";		//请求类型    
    private String mTransactionNum = "";	//交互编码
    private String mDealType = "1";    		//处理类型 1 实时 2非实时   
    
    private Element mBaseData;			//报文BASE_PART被保险人信息部分    
    private Element mSurveylist;		//报文RECEIPTLIST账单信息部分    
    		
    private String mManageCom = "";				//机构编码   
    private String mOperator  = "cm1201";				//操作员
    private String mInsuredNo = "";			 	//客户号    
    private String mContNo = "";				//保单号     
    
    String mClaimNo = "";
    String mCaseNo = "";
    private String mSurveyNo="";
    private String mSURVEYNUMBER = "" ;
    private String mCONTENT = "" ;
    private String mTESULT = "" ;
    private String mSURVEYSTARTDATE = "" ;
    private String mSURVEYENDDATE = "" ;
    private String mPAYEE = "";
    private String mINQFEE = "";
    private String mINDIRECTFEE = "";
    private String mSURVEYFEE = "";
    private LLSurveySet tLLSurveySet=null;
    private LLInqFeeStaSet tLLInqFeeStaSet=null;
    private LLInqFeeSet tLLInqFeeSet=null;
    private LLIndirectFeeSet tLLIndirectFeeSet=null;
    private LLCaseIndFeeSet tLLCaseIndFeeSet=null;
    private LLInqApplySet tLLInqApplySet=null;
    
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();

    public Document service(Document pInXmlDoc) {
        System.out.println("LLTJAccidentSurvey--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else {                                 	                    	
                    	mMMap.put(tLLSurveySet, "INSERT");
                    	mMMap.put(tLLInqFeeStaSet, "INSERT");
                    	mMMap.put(tLLInqFeeSet, "INSERT");
                    	mMMap.put(tLLIndirectFeeSet, "INSERT");
                    	mMMap.put(tLLCaseIndFeeSet, "INSERT");      
                    	mMMap.put(tLLInqApplySet, "INSERT");
                    	mResult.clear();
                    	mResult.add(mMMap);
                    	try{
	                    	PubSubmit tPubSubmit = new PubSubmit();
	                        if (!tPubSubmit.submitData(mResult, "")) {
	                        	 mDealState = false;
	                        }
                    	} catch (Exception ex) {
                             mDealState = false;
                             mResponseCode = "E";
                             buildError("service()", "系统未知错误");
                    	}
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
        	return createXML();
        }
    }
    /**
     * 生成返回的报文信息
     * @return Document
     */
    public Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState || "".equals(mClaimNo)) {
            return createFalseXML();
        } else {
        	try {
        	if (!"".equals(mClaimNo)) {
        		return createResultXML(mClaimNo,mCaseNo);
        	} else {
        		return createResultXML(mClaimNo,mCaseNo);
        	}
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }     

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLTJAccidentSurvey--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");
        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
        System.out.println("LLTJAccidentSurvey--TransactionNum:" + mTransactionNum);

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mBaseData = tBodyData.getChild("BASE_PART");		//出险人基本信息        
        mSurveylist = tBodyData.getChild("SURVEYLIST");	//账单信息
        return true;
    }
    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLTJAccidentSurvey--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"TJ04".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
            mTransactionNum.length() != 30) {
            buildError("checkData()", "【交互编码】的编码错误");
            return false;
        }

        if (!"TJ04".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误");
            return false;
        }

        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"86120000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的管理机构编码错误");
            return false;
        }
        
        mClaimNo = mBaseData.getChildText("CLAIM_NO");
        mCaseNo = mBaseData.getChildText("CASENO");
        
        return true;
    }
    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLTJAccidentSurvey--dealData");  
        
        List tSurveylist = new ArrayList();
    	tSurveylist = (List)mSurveylist.getChildren();
    	tLLSurveySet=new LLSurveySet();
    	tLLInqFeeStaSet=new LLInqFeeStaSet();
    	tLLInqFeeSet=new LLInqFeeSet();
    	tLLIndirectFeeSet=new LLIndirectFeeSet();
    	tLLCaseIndFeeSet=new LLCaseIndFeeSet();
    	tLLInqApplySet =new LLInqApplySet();
    	String[] FeeListStrs=new String[10];
    	String[] IndirectFeeListStrs=new String[6];
    	double indirectFeeSum=0;
    	for (int i = 0; i < tSurveylist.size(); i++) 
        {
            Element tSurveyData = (Element) tSurveylist.get(i);
           
            mSURVEYNUMBER = tSurveyData.getChildText("SURVEYNUMBER");
            mCONTENT = tSurveyData.getChildText("CONTENT");
            mTESULT = tSurveyData.getChildText("TESULT");
            mSURVEYSTARTDATE = tSurveyData.getChildText("SURVEYSTARTDATE");
            mSURVEYENDDATE = tSurveyData.getChildText("SURVEYENDDATE");
            mPAYEE = tSurveyData.getChildText("PAYEE");
            mINQFEE = tSurveyData.getChildText("INQFEE");
            mINDIRECTFEE = tSurveyData.getChildText("INDIRECTFEE");
            mSURVEYFEE = tSurveyData.getChildText("SURVEYFEE");
            if (mSURVEYNUMBER == null || "".equals(mSURVEYNUMBER) || "".equals(mSURVEYNUMBER)) {
            	buildError("dealData", "【调查序号】的值不能为空");
            	return false;
            }
            mSurveyNo=mCaseNo+mSURVEYNUMBER;            
            String OperaSql="select * from LLSurvey where SurveyNo='"+mSurveyNo+"'";
            ExeSQL OperaExeSQL = new ExeSQL();
            if((OperaExeSQL.getOneValue(OperaSql)).length()>0)
            {
           	 buildError("SurveyNo", "【调查序号】在系统中已存在");
                return false;
            }
            if (mCaseNo == null || "".equals(mCaseNo)|| mCaseNo.equals("null")) {
                buildError("mCaseNo", "【案件号】的值不能为空");
                return false;
            }
            if (mPAYEE == null || "".equals(mPAYEE)|| mPAYEE.equals("null")) {
                buildError("mPAYEE", "【领款人】的值不能为空");
                return false;
            }
            if (mSURVEYENDDATE == null || "".equals(mSURVEYENDDATE)|| mSURVEYENDDATE.equals("null")) {
                buildError("mPAYEE", "【调查结束日期】的值不能为空");
                return false;
            }
            if (mINDIRECTFEE == null || "".equals(mINDIRECTFEE)|| mINDIRECTFEE.equals("null")) {
                buildError("mPAYEE", "【间接调查费】的值不能为空");
                return false;
            }
            if (mINQFEE == null || "".equals(mINQFEE)|| mINQFEE.equals("null")) {
                buildError("mPAYEE", "【直接调查费】的值不能为空");
                return false;
            }
            if (mSURVEYFEE == null || "".equals(mSURVEYFEE)|| mSURVEYFEE.equals("null")) {
                buildError("mPAYEE", "【案件调查费合计】的值不能为空");
                return false;
            }
            LLSurveySchema tLLSurveySchema=new LLSurveySchema();
	         //查找客户号及客户名称
	       	 String tSQL = "SELECT Customerno,Customername "
	             + "FROM LLCASE WHERE "
	             + "caseno = '"+mCaseNo+"' WITH UR";
			 ExeSQL tExeSQl = new ExeSQL();
			 SSRS tSSRS = new SSRS();
			 tSSRS = tExeSQl.execSQL(tSQL);
			 if (tSSRS.getMaxRow()<=0){
	             buildError("mCaseNo", "【案件号】不存在");
	             return false;
			 }
			 else
			 {
				 tLLSurveySchema.setCustomerNo(tSSRS.GetText(1, 1));
				 tLLSurveySchema.setCustomerName(tSSRS.GetText(1, 2));
			 } 
			 
			 
			//调查主表信息存入
			tLLSurveySchema.setSurveyNo(mSurveyNo);
            tLLSurveySchema.setOtherNo(mCaseNo);
            tLLSurveySchema.setOtherNoType("1");
            tLLSurveySchema.setSubRptNo("0000");
            tLLSurveySchema.setStartPhase("0");
            tLLSurveySchema.setContent(mCONTENT);
            tLLSurveySchema.setresult(mTESULT);
            tLLSurveySchema.setSurveyStartDate(mSURVEYSTARTDATE);
            tLLSurveySchema.setSurveyEndDate(mSURVEYENDDATE);
            tLLSurveySchema.setOperator(mOperator);
            tLLSurveySchema.setMakeDate(mCurrentDate);
            tLLSurveySchema.setMakeTime(mCurrentTime);
            tLLSurveySchema.setModifyDate(mCurrentDate);
            tLLSurveySchema.setModifyTime(mCurrentTime);
            tLLSurveySchema.setMngCom(mManageCom);
            tLLSurveySchema.setSurveyClass("1");
            tLLSurveySchema.setSurveyType("1"); 
            tLLSurveySchema.setSurveyFlag("3");
            tLLSurveySchema.setSurveyOperator(mOperator);
            tLLSurveySchema.setStartMan(mOperator);
            tLLSurveySchema.setConfer(mOperator);
            tLLSurveySchema.setConfNote("同意");              
            tLLSurveySchema.setSurveySite(mSURVEYFEE);
            
            tLLSurveySet.add(tLLSurveySchema);
            
            //调查费用统计表
            LLInqFeeStaSchema tLLInqFeeStaSchema=new LLInqFeeStaSchema();
            tLLInqFeeStaSchema.setSurveyNo(mSurveyNo);           
            tLLInqFeeStaSchema.setPayeeType("1");           
            tLLInqFeeStaSchema.setPayee(mPAYEE);
            tLLInqFeeStaSchema.setInqFee(mINQFEE);
            tLLInqFeeStaSchema.setIndirectFee(mINDIRECTFEE);
            tLLInqFeeStaSchema.setSurveyFee(mSURVEYFEE);
            tLLInqFeeStaSchema.setOtherNo(mCaseNo);
            tLLInqFeeStaSchema.setOtherNoType("1");
            tLLInqFeeStaSchema.setInqDept(mManageCom);
            tLLInqFeeStaSchema.setContSN(1);
            tLLInqFeeStaSchema.setOperator(mOperator);
            tLLInqFeeStaSchema.setMakeDate(mCurrentDate);
            tLLInqFeeStaSchema.setMakeTime(mCurrentTime);
            tLLInqFeeStaSchema.setModifyDate(mCurrentDate);
            tLLInqFeeStaSchema.setModifyTime(mCurrentTime);
            tLLInqFeeStaSchema.setInputer(mOperator);
            tLLInqFeeStaSchema.setConfer(mOperator);
            tLLInqFeeStaSchema.setInputDate(mCurrentDate);
            tLLInqFeeStaSchema.setConfDate(mCurrentDate);
            tLLInqFeeStaSet.add(tLLInqFeeStaSchema);
            
            //调查项目表
            LLInqApplySchema tLLInqApplySchema = new LLInqApplySchema();
            tLLInqApplySchema.setOtherNo(mCaseNo);
            tLLInqApplySchema.setOtherNoType("1");
            tLLInqApplySchema.setSurveyNo(mSurveyNo);
            tLLInqApplySchema.setInqNo(mSurveyNo);
            tLLInqApplySchema.setInqDesc(mCONTENT);
            tLLInqApplySchema.setInqDept(mManageCom);
            tLLInqApplySchema.setInqState("0");
            tLLInqApplySchema.setLocFlag("0");
            tLLInqApplySchema.setInqPer(mOperator);
            tLLInqApplySchema.setInqStartDate(mCurrentDate);
            tLLInqApplySchema.setConPer(mOperator);
            tLLInqApplySchema.setDipatcher(mOperator);
            tLLInqApplySchema.setOperator(mOperator);
            tLLInqApplySchema.setMakeDate(mCurrentDate);
            tLLInqApplySchema.setMakeTime(mCurrentTime);
            tLLInqApplySchema.setModifyDate(mCurrentDate);
            tLLInqApplySchema.setModifyTime(mCurrentTime); 
            tLLInqApplySet.add(tLLInqApplySchema);

            Element mINQFEELIST = (Element) tSurveyData.getChild("INQFEELIST");
            List tINQFEELIST = new ArrayList();
            tINQFEELIST = mINQFEELIST.getChildren();
               
            for (int j = 0; j < tINQFEELIST.size(); j++) 
            {
                Element tINQFEEData = (Element) tINQFEELIST.get(j);
                String tRECORD = tINQFEEData.getChildText("RECORD");
                String tAPPRAISAL = tINQFEEData.getChildText("APPRAISAL");
                String tEXPERTS = tINQFEEData.getChildText("EXPERTS");
                String tPOUNDAGE = tINQFEEData.getChildText("POUNDAGE");
                String tNOTARIZATION = tINQFEEData.getChildText("NOTARIZATION");
                String tASSESSMENT = tINQFEEData.getChildText("ASSESSMENT");
                String tLITIGATION = tINQFEEData.getChildText("LITIGATION");
                String tCERTIFICATION = tINQFEEData.getChildText("CERTIFICATION");
                String tTRAFFIC = tINQFEEData.getChildText("TRAFFIC");
                String tMATERIALS = tINQFEEData.getChildText("MATERIALS");
                FeeListStrs[0]=tRECORD;
                FeeListStrs[1]=tAPPRAISAL;
                FeeListStrs[2]=tEXPERTS;
                FeeListStrs[3]=tPOUNDAGE;
                FeeListStrs[4]=tNOTARIZATION;
                FeeListStrs[5]=tASSESSMENT;
                FeeListStrs[6]=tLITIGATION;
                FeeListStrs[7]=tCERTIFICATION;
                FeeListStrs[8]=tTRAFFIC;
                FeeListStrs[9]=tMATERIALS;
                for(int jj=0;jj<FeeListStrs.length;jj++){
	                if(FeeListStrs[jj].length()>0){
		                LLInqFeeSchema tLLInqFeeSchema=new LLInqFeeSchema();
		                tLLInqFeeSchema.setSurveyNo(mSurveyNo);
		                tLLInqFeeSchema.setOtherNo(mCaseNo);
		                tLLInqFeeSchema.setOtherNoType("1");
		                tLLInqFeeSchema.setInqDept(mManageCom);
		                tLLInqFeeSchema.setContSN(1);
		                tLLInqFeeSchema.setFeeItem(jj<9?"0"+(jj+1):""+(jj+1));
		                tLLInqFeeSchema.setFeeSum(FeeListStrs[jj]);
		                tLLInqFeeSchema.setOperator(mOperator);
		                tLLInqFeeSchema.setMakeDate(mCurrentDate);
		                tLLInqFeeSchema.setMakeTime(mCurrentTime);
		                tLLInqFeeSchema.setModifyDate(mCurrentDate);
		                tLLInqFeeSchema.setModifyTime(mCurrentTime);
		                tLLInqFeeSchema.setConfer(mOperator);
		                tLLInqFeeSchema.setUWState("1");
		                tLLInqFeeSet.add(tLLInqFeeSchema);
	                }  
                }
            }
            
            Element mINDIRECTFEELIST = (Element) tSurveyData.getChild("INDIRECTFEELIST");
            List tINDIRECTFEELIST = new ArrayList();
            tINDIRECTFEELIST = mINDIRECTFEELIST.getChildren();
            for (int k = 0; k < tINDIRECTFEELIST.size(); k++) 
            {
                Element tINDIRECTFEEData = (Element) tINDIRECTFEELIST.get(k);
                String tSERVICE = tINDIRECTFEEData.getChildText("SERVICE");
                String tTRAVEL = tINDIRECTFEEData.getChildText("TRAVEL");
                String tLABOUR = tINDIRECTFEEData.getChildText("LABOUR");
                String tQUZHENG = tINDIRECTFEEData.getChildText("QUZHENG");
                String tOIL = tINDIRECTFEEData.getChildText("OIL");
                String tOTHER = tINDIRECTFEEData.getChildText("OTHER");
                IndirectFeeListStrs[0]=tSERVICE;
                IndirectFeeListStrs[1]=tTRAVEL;
                IndirectFeeListStrs[2]=tLABOUR;
                IndirectFeeListStrs[3]=tQUZHENG;
                IndirectFeeListStrs[4]=tOIL;
                IndirectFeeListStrs[5]=tOTHER;
                String tLimit=null;
                String IndirectSN=null;
                indirectFeeSum=0;
                
            	//计算本月的最后一天
                String LastDaySql="select LAST_DAY(current date) from dual with ur";
                ExeSQL LastDaySQL = new ExeSQL();
                String tFeeDate = LastDaySQL.getOneValue(LastDaySql);
                
                for(int kk=0;kk<IndirectFeeListStrs.length;kk++){
                	if(IndirectFeeListStrs[kk].length()>0){
                		LLIndirectFeeSchema tLLIndirectFeeSchema=new LLIndirectFeeSchema();
                		if(tLimit==null){
	                		tLimit = PubFun.getNoLimit("86");
	                        IndirectSN = PubFun1.CreateMaxNo("FEEDETAILNO", tLimit);
                		}
                		tLLIndirectFeeSchema.setIndirectSN(IndirectSN);
                		tLLIndirectFeeSchema.setFeeItem("0"+(kk+1));
                		tLLIndirectFeeSchema.setFeeSum(IndirectFeeListStrs[kk]);
                		indirectFeeSum+=Double.parseDouble(IndirectFeeListStrs[kk]);
                		tLLIndirectFeeSchema.setFeeDate(tFeeDate);
                		tLLIndirectFeeSchema.setOperator(mOperator);
                		tLLIndirectFeeSchema.setMakeDate(mCurrentDate);
                		tLLIndirectFeeSchema.setMakeTime(mCurrentTime);
                		tLLIndirectFeeSchema.setModifyDate(mCurrentDate);
                		tLLIndirectFeeSchema.setModifyTime(mCurrentTime);
                		tLLIndirectFeeSchema.setUWState("1");
                		tLLIndirectFeeSchema.setFinInspector(mOperator);
                		tLLIndirectFeeSchema.setMngCom(mManageCom);
                		tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
                	}
                }
                if(indirectFeeSum>0){

	                
	                LLCaseIndFeeSchema tLLCaseIndFeeSchema=new LLCaseIndFeeSchema();
	                tLLCaseIndFeeSchema.setOtherNo(mCaseNo);
	                tLLCaseIndFeeSchema.setOtherNoType("1");
	                tLLCaseIndFeeSchema.setIndirectSN(IndirectSN);
	                tLLCaseIndFeeSchema.setFeeSum(indirectFeeSum);
	                tLLCaseIndFeeSchema.setFeeDate(tFeeDate);
	                tLLCaseIndFeeSchema.setMngCom(mManageCom);
	                tLLCaseIndFeeSchema.setOperator(mOperator);
	                tLLCaseIndFeeSchema.setMakeDate(mCurrentDate);
	                tLLCaseIndFeeSchema.setMakeTime(mCurrentTime);
	                tLLCaseIndFeeSchema.setModifyDate(mCurrentDate);
	                tLLCaseIndFeeSchema.setModifyTime(mCurrentTime);
	                tLLCaseIndFeeSchema.setPayed("0");
	                tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);
                }
            }
        }   

        return true;
    }
    /**
     * 生成正常返回报文
     * @return Document
     */
    public Document createResultXML(String aClaimNo,String aCaseNo) throws Exception {
    	 System.out.println("LLTJAccidentSurvey--createResultXML(正常返回报文)");
    	 Element tRootData = new Element("PACKET");
         tRootData.addAttribute("type", "RESPONSE");
         tRootData.addAttribute("version", "1.0");

         //Head部分
         Element tHeadData = new Element("HEAD");

         Element tRequestType = new Element("REQUEST_TYPE");
         tRequestType.setText(mRequestType);
         tHeadData.addContent(tRequestType);

         Element tResponseCode = new Element("RESPONSE_CODE");
         tResponseCode.setText("1");
         tHeadData.addContent(tResponseCode);

         Element tErrorMessage = new Element("ERROR_MESSAGE");
         tErrorMessage.setText(mErrors.getFirstError());
         tHeadData.addContent(tErrorMessage);

         Element tTransactionNum = new Element("TRANSACTION_NUM");
         tTransactionNum.setText(mTransactionNum);
         tHeadData.addContent(tTransactionNum);

         tRootData.addContent(tHeadData);

         //Body部分
         Element tBodyData = new Element("BODY");
         Element tClaimNo = new Element("CLAIM_NO");
         tClaimNo.setText(aClaimNo);
         tBodyData.addContent(tClaimNo);
         
         Element tCaseNo = new Element("CASENO");
         tCaseNo.setText(aCaseNo);
         tBodyData.addContent(tCaseNo);
         
         Element tSURVEYGETLIST = new Element("SURVEYGETLIST");
                 
         List tSurveylist = new ArrayList();
     	 tSurveylist = (List)mSurveylist.getChildren();

         for (int i = 0; i < tSurveylist.size(); i++) 
         {             
        	 Element tSurveyData = (Element) tSurveylist.get(i);
        	 
        	 Element tSURVEYGETDATA = new Element("SURVEYGETDATA");  
        	 
        	 Element tSURVEYNUMBER = new Element("SURVEYNUMBER");        	
        	 tSURVEYNUMBER.setText(tSurveyData.getChildText("SURVEYNUMBER"));
        	 tSURVEYGETDATA.addContent(tSURVEYNUMBER);
        	 
        	 Element tSURVEYNO = new Element("SURVEYNO");        	
        	 tSURVEYNO.setText(mSurveyNo);
        	 tSURVEYGETDATA.addContent(tSURVEYNO);
        	 
        	 Element tPAYEE = new Element("PAYEE");        	
        	 tPAYEE.setText(tSurveyData.getChildText("PAYEE"));
        	 tSURVEYGETDATA.addContent(tPAYEE);
        	 
        	 tSURVEYGETLIST.addContent(tSURVEYGETDATA);          	
         }   
         tBodyData.addContent(tSURVEYGETLIST);
         tRootData.addContent(tBodyData); 

         Document tDocument = new Document(tRootData);
         return tDocument;
         
    }
    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
    	System.out.println("LLTJAccidentSurvey--createResultXML(错误返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }
   
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLTJAccidentSurvey";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
   
    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("D:/LLTJSurvey01.xml"), "GBK");
            LLTJAccidentSurvey tBusinessDeal = new LLTJAccidentSurvey();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
        	        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
