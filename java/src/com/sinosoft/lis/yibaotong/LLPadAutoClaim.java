package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.bl.LCGetBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDDiseaseDB;
import com.sinosoft.lis.db.LLAccidentTypeDB;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.db.LLClaimDB;
import com.sinosoft.lis.db.LLClaimDetailDB;
import com.sinosoft.lis.db.LLClaimPolicyDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.db.LMCalModeDB;
import com.sinosoft.lis.llcase.ClaimCalBL;
import com.sinosoft.lis.llcase.ClaimUnderwriteBL;
import com.sinosoft.lis.llcase.LJAGetInsertBL;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.llcase.LLUWCheckBL;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSGetClaimSchema;
import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.schema.LLAccidentSchema;
import com.sinosoft.lis.schema.LLCaseCureSchema;
import com.sinosoft.lis.schema.LLCasePolicySchema;
import com.sinosoft.lis.schema.LLCaseReceiptSchema;
import com.sinosoft.lis.schema.LLCaseRelaSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.schema.LLClaimDetailSchema;
import com.sinosoft.lis.schema.LLClaimPolicySchema;
import com.sinosoft.lis.schema.LLClaimSchema;
import com.sinosoft.lis.schema.LLClaimUWMainSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLElementDetailSchema;
import com.sinosoft.lis.schema.LLFeeMainSchema;
import com.sinosoft.lis.schema.LLHospCaseSchema;
import com.sinosoft.lis.schema.LLOtherFactorSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.schema.LLSubReportSchema;
import com.sinosoft.lis.schema.LLToClaimDutySchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetClaimSet;
import com.sinosoft.lis.vschema.LLAccidentSet;
import com.sinosoft.lis.vschema.LLCaseCureSet;
import com.sinosoft.lis.vschema.LLCasePolicySet;
import com.sinosoft.lis.vschema.LLCaseReceiptSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLClaimDetailSet;
import com.sinosoft.lis.vschema.LLClaimPolicySet;
import com.sinosoft.lis.vschema.LLClaimSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.lis.vschema.LLElementDetailSet;
import com.sinosoft.lis.vschema.LLToClaimDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: PAD自动理赔</p>
 *
 * <p>Description: 生成案件至通知给付状态</p>
 *
 * <p>Copyright: Copyright (c) 2016</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Alex
 * @version 1.0
 */
public class LLPadAutoClaim implements ServiceInterface {
    public LLPadAutoClaim() {
    }

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    public VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMMap = new MMap();

    //传入报文
    private Document mInXmlDoc;
    //报文类型
    private String mDocType = "";
    //处理标志
    private boolean mDealState = true;

    //返回类型代码
    private String mResponseCode = "1";
    //请求类型
    private String mRequestType = "";
    //错误描述
    private String mErrorMessage = "";
    //交互编码
    private String mTransactionNum = "";
    //扫描件完整路径
    private String  mPicPath;
    //报文PICTURE_DATA部分
    private Element mPictureData;
    //报文USER_DATA部分
    private Element mUserData;
    //报文CUSTOMER_DATA部分
    private Element mCustomerData;
    //报文POLICYLIST部分
    private Element mPolicyList;
    //报文RECEIPTLIST部分
    private Element mReceiptList;
    //报文DISEASELIST部分
    private Element mDiseaseList;
    //报文ACCIDENTLIST部分
    private Element mAccidentList;

    //机构编码
    private String mManageCom = "";
    //客户号
    private String mInsuredNo = "";
    //保单信息
    private LCPolSet mLCPolSet = new LCPolSet();
    //给付责任信息
    private LCGetSet mLCGetSet = new LCGetSet();

    //出险日期
    private String mAccDate = "";
    //案件号
    private String mCaseNo = "";
    //案件处理人
    private String mHandler = "";
//    处理人信息
    private LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
    //案件信息
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    //申请信息
    private LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
    //被保险人信息
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    //事件信息
    private LLSubReportSchema mLLSubReportSchema = new LLSubReportSchema();
    //事件关联
    private LLCaseRelaSchema mLLCaseRelaSchema = new LLCaseRelaSchema();
    //疾病信息
    private LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
    //疾病信息
    private LLAccidentSet mLLAccidentSet = new LLAccidentSet();
//    其他编码
    private LLOtherFactorSchema tLLOtherFactorSchema = new LLOtherFactorSchema();
    //账单表
    private LLFeeMainSchema mLLFeeMainSchema = new LLFeeMainSchema();
    //费用明细
    private LLCaseReceiptSet mLLCaseReceiptSet = new LLCaseReceiptSet();
    //理算险种表
    private LLCasePolicySet mLLCasePolicySet = new LLCasePolicySet();
    //自动理算表
    private LLToClaimDutySet mLLToClaimDutySet = new LLToClaimDutySet();

    //理算数据
    private LLClaimDetailSet mLLClaimDetailSet = new LLClaimDetailSet();
    //理算确认数据
    private LLClaimDetailSet mConfirmLLClaimDetailSet = new LLClaimDetailSet();
    //险种理算数据
    private LLClaimPolicySet mLLClaimPolicySet = new LLClaimPolicySet();
    //案件理算表
    private LLClaimSchema mLLClaimSchema = new LLClaimSchema();
    //应付明细
    private LJSGetClaimSet mLJSGetClaimSet = new LJSGetClaimSet();
    //应付
    private LJSGetSchema mLJSGetSchema = new LJSGetSchema();
    //医保通案件号
    private LLHospCaseSchema mLLHospCaseSchema = new LLHospCaseSchema();
    //事件关联号
    private String mCaseRelaNo = "";

    private String mLimit = "";

    private String mMakeDate = PubFun.getCurrentDate();
    private String mMakeTime = PubFun.getCurrentTime();


    public Document service(Document pInXmlDoc) {
        //一个人买醉尝试放纵的滋味，时间紧先实现功能再说,写的烂的地方勿喷
        System.out.println("LLPadAutoClaim--service");
        mInXmlDoc = pInXmlDoc;

        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                        cancelCase();
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
            cancelCase();
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLPadAutoClaim--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"YD03".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }

        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLPadAutoClaim--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        System.out.println("LLPadAutoClaim--TransactionNum:" +
                           mTransactionNum);

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mPictureData = tBodyData.getChild("PICTURE_DATA");
        mUserData = tBodyData.getChild("USER_DATA");
        mCustomerData = tBodyData.getChild("CUSTOMER_DATA");
        mPolicyList = mCustomerData.getChild("POLICYLIST");
        mReceiptList = mCustomerData.getChild("RECEIPTLIST");
        mDiseaseList = mCustomerData.getChild("DISEASELIST");
        mAccidentList = mCustomerData.getChild("ACCIDENTLIST");

        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLPadAutoClaim--dealData");
        //处理保单信息 为后续生成理算你数据做准备
        if (!dealCont()) {
            return false;
        }

        //案件受理 处理案件 事件等信息， 申请材料申请原因等表 暂省略了
        if (!caseRegister()) {
            return false;
        }

        //处理账单信息 类似账单录入功能 选择 医院 类型的情况
        if (!dealFee()) {
            return false;
        }

        //准备计算数据   自动理算的关键所在   一般人都不告诉他
        if (!getCalInfo()) {
            return false;
        }

        //生成案件信息   就是提交数据
        if (!dealCase()) {
            return false;
        }

        //理算金额  没啥好说的 就是使用自动理算 关键在准备数据上
        if (!claimCal()) {
            return false;
        }
//理算确认  为了置上赔付结论 改状态等等
        if (!endCal()) {
            cancelCase();
            return false;
        }
//        审批审定  需要注意 传输的数据完整即可
        if(!simpleClaimAudit()){
        	cancelCase();
            return false;
        }
//        通知给付..........终于到最后一步了。生无可恋
        if(!LJaget()){
        	cancelCase();
            return false;
        }
        if(!LLPadScan()){
        	cancelCase();
            return false;
        }

        return true;
    }


    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
        	try {
        		return createResultXML(mCaseNo);
        	} catch (Exception ex) {
        		mResponseCode = "0";
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tBody.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tBody.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML(String aCaseNo) throws Exception {
        DecimalFormat tDF = new DecimalFormat("0.##");

        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        //Head部分
        Element tHeadData = new Element("HEAD");
        Element tBody = new Element("BODY");
        
        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);
        
        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        Element tBaseData = new Element("BASEDATA");
        
        Element tCaseNo = new Element("CASENO");
        tCaseNo.setText(aCaseNo);
        tBaseData.addContent(tCaseNo);

      
        String tSQL =" select a.rgtdate,b.givetype,b.REALPAY ,a.rgtstate from llcase a,llclaim b  " +
        		" where a.caseno=b.caseno and a.caseno='"+aCaseNo+"' with ur";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSQL);
        
        Element tRgtdate = new Element("RGTDATE");
        tRgtdate.setText(tSSRS.GetText(1, 1));
        tBaseData.addContent(tRgtdate);
        
        Element tGivetype = new Element("GIVETYPE");
        tGivetype.setText(tSSRS.GetText(1, 2));
        tBaseData.addContent(tGivetype);
        
        Element tRealpay = new Element("REALPAY");
        tRealpay.setText(tSSRS.GetText(1, 3));
        tBaseData.addContent(tRealpay);
        
        Element tRgtstate = new Element("RGTSTATE");
        tRgtstate.setText(tSSRS.GetText(1, 4));
        tBaseData.addContent(tRgtstate);
        
        tBody.addContent(tBaseData);

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBody);
        

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospAutoRegister";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    /**
     * 解析校验保单部分
     * @return boolean
     */
    private boolean dealCont() throws Exception {
        System.out.println("LLPadAutoClaim--dealCont");
        List tPolicyList = new ArrayList();
        tPolicyList = (List) mPolicyList.getChildren();

         mInsuredNo = mCustomerData.getChildText("INSUREDNO");
        //循环校验每个保单的数据,将正常保单计入mLCPolSet
        out:
        for (int i = 0; i < tPolicyList.size(); i++) {
            Element tContData = (Element) tPolicyList.get(i);

            if (mInsuredNo == null || "".equals(mInsuredNo)) {
                buildError("dealCont", "【客户号】的值不能为空");
                return false;
            }

            //用客户号校验该客户是否有正在处理的保全项目,返回的是工单号，而且校验的是全部保单，考虑以后是否按照保单校验
            if (!"".equals(LLCaseCommon.checkPerson(mInsuredNo))) {
                buildError("dealCont", "该客户正在进行保全处理，不能进行理赔");
                return false;
            }

            String tContNo = tContData.getChildText(
                    "CONTNO");
            if (tContNo == null || "".equals(tContNo)) {
                buildError("dealCont", "【保单号】的值不能为空");
                return false;
            }
            
            String tRiskCode = tContData.getChildText("RISKCODE");
            if (tRiskCode == null || "".equals(tRiskCode)) {
                buildError("dealCont", "【险种编码】的值不能为空");
                return false;
            }
            String tGetDutyCode = tContData.getChildText("GETDUTYCODE");
            if (tGetDutyCode == null || "".equals(tGetDutyCode)) {
                buildError("dealCont", "【给付责任编码】的值不能为空");
                return false;
            }
            String tGetDutyKind = tContData.getChildText("GETDUTYKIND");
            if (tGetDutyKind == null || "".equals(tGetDutyKind)) {
                buildError("dealCont", "【给付责任类型】的值不能为空");
                return false;
            }

            String tPolSQL = "select a.polno from lcpol a where  a.contno='" + tContNo +
                             "' and a.riskcode='" + tRiskCode +
                             "' and a.insuredno='" +
                             mInsuredNo +
                             "' and a.appflag='1' and a.stateflag='1' "
                             ;
            
            System.out.println(tPolSQL);
            
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tPolSSRS = tExeSQL.execSQL(tPolSQL);
//          
            if (tPolSSRS.getMaxRow() !=1 ) {
            	buildError("dealCont", "保单" + tContNo + "下险种"+ tRiskCode +"查询失败");
                return false;
            }
            
            String tPolNo = tPolSSRS.GetText(1, 1);
            
            if ("".equals(tPolNo)) {
            	buildError("dealCont", "保单" + tContNo + "下险种"+ tRiskCode +"查询失败了");
                return false;
            }
            
            LCGetBL tLCGetBL = new LCGetBL();
            String tGetSQL = "select * from lcget where polno='"+tPolNo+"' and getdutycode='"+tGetDutyCode+"' ";
            LCGetSet tLCGetSet = tLCGetBL.executeQuery(tGetSQL);
            if (tLCGetSet.size()==0) {
            	buildError("dealCont", "查询保单" + tContNo + "下险种"+ tRiskCode +"的给付责任失败");
                return false;
            }
//           一般情况set里只有一条数据.....为什么总是一般情况 特殊怎么办     再说！
            for(int y=1; y <=tLCGetSet.size();y++){
	            LCGetSchema tLCGetSchema = new LCGetSchema();
	            tLCGetSchema=tLCGetSet.get(y);
	            tLCGetSchema.setGetDutyKind(tGetDutyKind);
	            mLCGetSet.add(tLCGetSchema);
            }
            
            
            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(tPolNo);
            
            if (!tLCPolBL.getInfo()) {
            	buildError("dealCont", "查询保单" + tContNo + "下险种"+ tRiskCode +"失败");
                return false;
            }
            
            LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
            
            String tPolNoNew = tLCPolSchema.getPolNo();

            //剔除重复的保单
           	for (int j = 1; j <= mLCPolSet.size(); j++) {
           		String tPolNoOld = mLCPolSet.get(j).getPolNo();
                if (tPolNoNew.equals(tPolNoOld)) {
                	continue out;
                }
            }
           	mLCPolSet.add(tLCPolSchema);
        }

        return true;
    }


    /**
     * 解析受理信息
     * @return boolean
     */
    private boolean caseRegister() throws Exception {
        System.out.println("LLPadAutoClaim--caseRegister");
       
        List tReceiptList = new ArrayList();
        tReceiptList = (List) mReceiptList.getChildren();
        
        System.out.println("List长度:"+tReceiptList.size());
  
//      出险日期只取第一个账单的入院日期
        Element mAccReceiptData = (Element) tReceiptList.get(0);
        mAccDate = mAccReceiptData.getChildText("HOSPSTARTDATE");
        
        if (mAccDate == null || "".equals(mAccDate)) {
            buildError("caseRegister", "【入院日期】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(mAccDate)) {
            buildError("caseRegister", "【入院日期】格式错误(正确格式yyyy-MM-dd)");
            return false;
        }
//处理人取pad传过来的
        mHandler = mUserData.getChildText("USERCODE");
        
        if (mHandler == null || "".equals(mHandler)) {
            buildError("caseRegister", "【用户编码】的值不能为空");
            return false;
        }
    
        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        tLLClaimUserDB.setUserCode(mHandler);
        tLLClaimUserDB.setClaimDeal("1");//理赔员权限
        tLLClaimUserDB.setHandleFlag("1");//案件分配权限
        tLLClaimUserDB.setStateFlag("1");

        System.out.println("PAD理赔机构--------------------------------"+tLLClaimUserDB.getComCode()+"-------------------------------");
        LLClaimUserSet tLLClaimUserSet = tLLClaimUserDB.query();

        if (tLLClaimUserSet.size() <= 0) {
            buildError("caseRegister", "案件处理人查询失败或没有权限");
            return false;
        }
//      案件机构取理赔用户机构
        mManageCom = tLLClaimUserSet.get(1).getComCode();
        
        LLCaseDB tLLCaseDB = new LLCaseDB();
        String tCaseSQl = "select * from llcase where rgtstate not in ('11','12','14') and customerno='" +
                          mInsuredNo + "'";
        LLCaseSet tLLCaseSet = tLLCaseDB.executeQuery(tCaseSQl);
        if (tLLCaseSet.size() > 0) {
            buildError("dealCont", "客户" + mInsuredNo + "有未处理完的理赔案件，不能进行理赔。");
            return false;
        }

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(mInsuredNo);
        LCInsuredSet tLCinsuredSet = tLCInsuredDB.query();
        if (tLCinsuredSet.size() < 1) {
            buildError("caseRegister", "被保险人查询失败");
            return false;
        }
        //只需要取一个就可以，只要基本信息
        mLCInsuredSchema = tLCinsuredSet.get(1);

        mLimit = PubFun.getNoLimit(mManageCom);
        mCaseNo = PubFun1.CreateMaxNo("CaseNo", mLimit);

        if (!getCaseInfo()) {
            return false;
        }

        mCaseRelaNo = PubFun1.CreateMaxNo("CASERELANO", mLimit);

        if (!getSubInfo()) {
            return false;
        }

        return true;
    }


    /**
     * 解析账单信息
     * @return boolean
     */
    private boolean dealFee() throws Exception {
        System.out.println("LLPadAutoClaim--dealFee");
//        账单信息
        List tReceiptList = new ArrayList();
        tReceiptList = (List) mReceiptList.getChildren();
        
        System.out.println("List长度:"+tReceiptList.size());
        
        for(int i=0; i<tReceiptList.size(); i++){
        Element mReceiptData = (Element) tReceiptList.get(i);
        String mMainFeeNo = PubFun1.CreateMaxNo("MAINFEENO", mLimit);
        String tFeeAtti = mReceiptData.getChildText("FEEATTI");
        String tFeeType = mReceiptData.getChildText("FEETYPE");
        String tReceiptNo = mReceiptData.getChildText("MAINFEENO");
        String tHospitalNo = mReceiptData.getChildText("HOSIPITALNO");
        String tHospitalName = mReceiptData.getChildText("HOSIPITALNAME");
        String tHospStartDate = mReceiptData.getChildText("HOSPSTARTDATE");
        String tHospEndDate = mReceiptData.getChildText("HOSPENDDATE");
        String tMedicareType = mReceiptData.getChildText("MEDICARETYPE");  // # 3544 医保类型
//        账单表存的汇总值 存账单表时使用
        double mSumFee = 0.0;
        double mPlanFee = 0.0;
        double mSelfAmnt = 0.0;
        double mRefuseAmnt = 0.0;
        
        if (!"1".equals(tFeeType) && !"2".equals(tFeeType) && !"3".equals(tFeeType)) {
            buildError("dealFee", "【账单类型】的值不存在");
            return false;
        }
        // 校验医保类型
        String tMedResult = LLCaseCommon.checkMedicareType(tMedicareType);
        if(!"".equals(tMedResult)){
        	buildError("dealFee", tMedResult);
            return false;
        }else{
        	mLLFeeMainSchema.setMedicareType(tMedicareType);
        }
//        费用项目信息
        Element mFeeitemList = mReceiptData.getChild("FEEITEMLIST");
        
        List tFeeitemList = new ArrayList();
        tFeeitemList = (List) mFeeitemList.getChildren();
        for(int y=0; y<tFeeitemList.size(); y++){
        	
        Element	mFeeitemData = (Element) tFeeitemList.get(y);

        String tFeeitemCode = mFeeitemData.getChildText("FEEITEMCODE");
        String tSumFee = mFeeitemData.getChildText("SUMFEE");
        String tPlanFee = mFeeitemData.getChildText("PLANFEE");
        String tSelfAmnt = mFeeitemData.getChildText("SELFAMNT");
        String tRefuseAmnt = mFeeitemData.getChildText("REFUSEAMNT");
        
        //账单金额
        if (tSumFee == null || "".equals(tSumFee)) {
            buildError("dealFee", "【账单金额】的值不能为空");
            return false;
        }
        double tSumFeeValue = Arith.round(Double.parseDouble(tSumFee), 2);

        if (tSumFeeValue <= 0) {
            buildError("dealFee", "【账单金额】的值必须大于0");
            return false;
        }
        
//        找到费用项目对应名称
        String tSQL = "select codename   from ldcode where codetype ='llfeeitemtype' and code='"+tFeeitemCode+"' ";
        
        ExeSQL tExeSQL = new ExeSQL();
        String tFeeitemName = tExeSQL.getOneValue(tSQL);
        if("".equals(tFeeitemName)||null==tFeeitemName||"null".equals(tFeeitemName)){
        	 buildError("dealFee", "【费用项目代码】的值不存在");
             return false;
        }
        
        LLCaseReceiptSchema mLLCaseReceiptSchema = new LLCaseReceiptSchema();
        String tLimit1 = PubFun.getNoLimit("86");
        mLLCaseReceiptSchema.setFeeDetailNo(
                PubFun1.CreateMaxNo("FEEDETAILNO", tLimit1));
        mLLCaseReceiptSchema.setMainFeeNo(mMainFeeNo);
        mLLCaseReceiptSchema.setRgtNo(mCaseNo);
        mLLCaseReceiptSchema.setCaseNo(mCaseNo);
        mLLCaseReceiptSchema.setFeeItemCode(tFeeitemCode);
        mLLCaseReceiptSchema.setFeeItemName(tFeeitemName);
        mLLCaseReceiptSchema.setFee(tSumFee);
        mLLCaseReceiptSchema.setSelfAmnt(tSelfAmnt);
        mLLCaseReceiptSchema.setPreAmnt(tPlanFee);
        mLLCaseReceiptSchema.setRefuseAmnt(tRefuseAmnt);
        mLLCaseReceiptSchema.setAvaliFlag("0");
        mLLCaseReceiptSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLCaseReceiptSchema.setOperator(mHandler);
        mLLCaseReceiptSchema.setMakeDate(mMakeDate);
        mLLCaseReceiptSchema.setMakeTime(mMakeTime);
        mLLCaseReceiptSchema.setModifyDate(mMakeDate);
        mLLCaseReceiptSchema.setModifyTime(mMakeTime);
        
         mSumFee += mLLCaseReceiptSchema.getFee();
         mPlanFee += mLLCaseReceiptSchema.getPreAmnt();
         mSelfAmnt += mLLCaseReceiptSchema.getSelfAmnt();
         mRefuseAmnt += mLLCaseReceiptSchema.getRefuseAmnt();
        mLLCaseReceiptSet.add(mLLCaseReceiptSchema);
        }
        
      
        mLLFeeMainSchema.setMainFeeNo(mMainFeeNo);
        mLLFeeMainSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLFeeMainSchema.setCaseRelaNo(mCaseRelaNo);
        mLLFeeMainSchema.setRgtNo(mLLCaseSchema.getRgtNo());
        mLLFeeMainSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLFeeMainSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLFeeMainSchema.setCustomerSex(mLLCaseSchema.getCustomerSex());
        mLLFeeMainSchema.setHospitalCode(tHospitalNo);
        mLLFeeMainSchema.setHospitalName(tHospitalName);
        mLLFeeMainSchema.setReceiptNo(tReceiptNo);

        mLLFeeMainSchema.setFeeType(tFeeType);
        //账单类型默认简易社保结算
        mLLFeeMainSchema.setFeeAtti(tFeeAtti);
        mLLFeeMainSchema.setFeeDate(tHospEndDate);
        mLLFeeMainSchema.setHospStartDate(tHospStartDate);
        mLLFeeMainSchema.setHospEndDate(tHospEndDate);
        //计算实际住院天数
        int tRealHospDay = PubFun.calInterval(mLLFeeMainSchema.getHospStartDate(),
                                              mLLFeeMainSchema.getHospEndDate(),
                                              "D") + 1;
        mLLFeeMainSchema.setRealHospDate(tRealHospDay);
        mLLFeeMainSchema.setAge((int) mLLCaseSchema.getCustomerAge());
        mLLFeeMainSchema.setSecurityNo(mLCInsuredSchema.getOthIDNo());
        mLLFeeMainSchema.setInsuredStat(mLCInsuredSchema.getInsuredStat());

        mLLFeeMainSchema.setSumFee(mSumFee);
        mLLFeeMainSchema.setPreAmnt(mPlanFee);
        mLLFeeMainSchema.setSelfAmnt(mSelfAmnt);
        mLLFeeMainSchema.setRefuseAmnt(mRefuseAmnt);

        mLLFeeMainSchema.setOperator(mHandler);
        mLLFeeMainSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLFeeMainSchema.setMakeDate(mMakeDate);
        mLLFeeMainSchema.setMakeTime(mMakeTime);
        mLLFeeMainSchema.setModifyDate(mMakeDate);
        mLLFeeMainSchema.setModifyTime(mMakeTime);

        }

        return true;
    }

    /**
     * 准备理算数据
     * @return boolean
     * @throws Exception
     */
    private boolean getCalInfo() throws Exception {
        System.out.println("LLPadAutoClaim--getCalInfo");
        LLCaseCommon tLLCaseCommon = new LLCaseCommon();
        if (mLCPolSet.size() < 1) {
            buildError("getCalInfo", "没有可以赔付的保单");
            return false;
        }
//准备险种层理算数据
        for (int i = 1; i <= mLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = mLCPolSet.get(i);

            //出险日期、入院日期、出院日期必须都在保单年度里
            if (!tLLCaseCommon.checkPolValid(tLCPolSchema.getPolNo(), mAccDate)) {
            	buildError("getCalInfo", "出险日期不在保单有效期内");
                continue;
            }

            LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();

            Reflections tReflections = new Reflections();
            tReflections.transFields(tLLCasePolicySchema, tLCPolSchema);

            tLLCasePolicySchema.setCaseNo(mLLCaseSchema.getCaseNo());
            tLLCasePolicySchema.setRgtNo(mLLCaseSchema.getRgtNo());
            tLLCasePolicySchema.setCaseRelaNo(mCaseRelaNo);
            tLLCasePolicySchema.setPolType("1");
            tLLCasePolicySchema.setCasePolType("0");
            tLLCasePolicySchema.setPolMngCom(tLCPolSchema.getManageCom());
            tLLCasePolicySchema.setRiskVer(tLCPolSchema.getRiskVersion());
            tLLCasePolicySchema.setOperator(mLLCaseSchema.getHandler());
            tLLCasePolicySchema.setMngCom(mLLCaseSchema.getMngCom());
            tLLCasePolicySchema.setMakeDate(mMakeDate);
            tLLCasePolicySchema.setMakeTime(mMakeTime);
            tLLCasePolicySchema.setModifyDate(mMakeDate);
            tLLCasePolicySchema.setModifyTime(mMakeTime);

            mLLCasePolicySet.add(tLLCasePolicySchema);
        }
        
//准备给付责任层理算数据
        if(mLCGetSet.size()<1){
        	buildError("getCalInfo", "没有可以赔付的责任");
            return false;
        }
        LLToClaimDutySet tLLToClaimDutySet = new LLToClaimDutySet();
        
        for(int n=1;n<=mLCGetSet.size();n++){
        	LCGetSchema tLCGetSchema=mLCGetSet.get(n);
        	String tPolno = tLCGetSchema.getPolNo();
        	LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(tPolno);
            
            if (!tLCPolBL.getInfo()) {
            	buildError("dealCont", "查询保单" + tLCGetSchema.getContNo() + "失败");
                return false;
            }
            
            LCPolSchema tLCPolSchema = tLCPolBL.getSchema();
        	
        	LLToClaimDutySchema tLLToClaimDutySchema = new
            LLToClaimDutySchema();

		    Reflections tReflection = new Reflections();
		    tReflection.transFields(tLLToClaimDutySchema, tLCPolSchema);
		
		    tLLToClaimDutySchema.setCaseNo(mLLCaseSchema.getCaseNo());
		    tLLToClaimDutySchema.setCaseRelaNo(mCaseRelaNo);
		    tLLToClaimDutySchema.setSubRptNo(mLLCaseRelaSchema.
		            getSubRptNo());
		    tLLToClaimDutySchema.setGetDutyCode(tLCGetSchema.
		            getGetDutyCode());
		    tLLToClaimDutySchema.setDutyCode(tLCGetSchema.getDutyCode());
		    tLLToClaimDutySchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
		    //理算次数
		    tLLToClaimDutySchema.setClaimCount(0);
		
		    tLLToClaimDutySchema.setPolMngCom(tLCPolSchema.getManageCom());
		    tLLToClaimDutySchema.setRiskVer(tLCPolSchema.getRiskVersion());
		    tLLToClaimDutySet.add(tLLToClaimDutySchema);
        }
        if (tLLToClaimDutySet.size() < 1) {
            buildError("getCalInfo", "没有可以匹配的责任");
            return false;
        }
        
        mLLToClaimDutySet.add(tLLToClaimDutySet);
        
        if (mLLToClaimDutySet.size() < 1 || mLLCasePolicySet.size() < 1) {
            buildError("getCalInfo", "没有可以理算的保单");
            return false;
        }

        return true;
    }

    /**
     * 生成案件信息
     * @return boolean
     */
    private boolean getCaseInfo() throws Exception {
        System.out.println("LLPadAutoClaim--getCaseInfo");
        mLLCaseSchema.setCaseNo(mCaseNo);
        mLLCaseSchema.setRgtNo(mCaseNo);
        //申请类案件
        mLLCaseSchema.setRgtType("1");
        //案件处理到理算确认
        mLLCaseSchema.setRgtState("04");
        mLLCaseSchema.setCustomerNo(mInsuredNo);
        mLLCaseSchema.setCustomerName(mLCInsuredSchema.getName());
        //账单状态录入
        mLLCaseSchema.setReceiptFlag("1");
        //未调查
        mLLCaseSchema.setSurveyFlag("0");
        mLLCaseSchema.setRgtDate(mMakeDate);

        mLLCaseSchema.setCustBirthday(mLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerSex(mLCInsuredSchema.getSex());

        int tAge = LLCaseCommon.calAge(mLCInsuredSchema.getBirthday());
        mLLCaseSchema.setCustomerAge(tAge);

        mLLCaseSchema.setIDType(mLCInsuredSchema.getIDType());
        mLLCaseSchema.setIDNo(mLCInsuredSchema.getIDNo());
        mLLCaseSchema.setHandler(mHandler);
        mLLCaseSchema.setDealer(mHandler);
//        银行信息
        String mCaseGetMode = mCustomerData.getChildText("CASEGETMODE");
        String mBankCode = mCustomerData.getChildText("BANKNO");
        String mAccNo = mCustomerData.getChildText("ACCNO");
        String mAccName = mCustomerData.getChildText("ACCNAME");
        
        if(!mCaseGetMode.equals("1") &&!mCaseGetMode.equals("2") )
    	{
            if (mBankCode == null || "".equals(mBankCode)|| mBankCode.equals("null")) {
                buildError("dealData", "【银行编码】的值不能为空");
                return false;
            }
            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
                buildError("dealData", "【银行名称】的值不能为空");
                return false;
            }
            if (mAccName == null || "".equals(mAccName)|| mAccName.equals("null")) {
                buildError("dealData", "【账户名】的值不能为空");
                return false;
            }
            if (mAccNo == null || "".equals(mAccNo)|| mAccNo.equals("null")) {
                buildError("dealData", "【账号】的值不能为空");
                return false;
            }
            Pattern tPattern = Pattern.compile("(\\d)+");
    		Matcher isNum = tPattern.matcher(mAccNo);

    		if (!isNum.matches()) {
    			buildError("dealData", "【账号】只能是数字组成");
    			return false;
    		}
    	}
        mLLCaseSchema.setCaseGetMode(mCaseGetMode); 
        mLLCaseSchema.setBankCode(mBankCode);
        mLLCaseSchema.setBankAccNo(mAccNo);
        mLLCaseSchema.setAccName(mAccName);
        
//        事件类型
        String mAccidentType = mCustomerData.getChildText("ACCDESCTYPE");
        mLLCaseSchema.setAccidentType(mAccidentType);
        
        //案件机构补齐8位
        if (mManageCom.length() == 4) {
            mLLCaseSchema.setMngCom(mManageCom + "0000");
        }else if(mManageCom.length() == 6){
        	mLLCaseSchema.setMngCom(mManageCom + "00");
        } 
        else if(mManageCom.length() == 2){//两位机构估计有问题一般不会有的，除非业务设置pad理赔用户机构为86 我就不信你会这样设...
        	mLLCaseSchema.setMngCom(mManageCom + "000000");
        } 
        else {
            mLLCaseSchema.setMngCom(mManageCom);
        }

        mLLCaseSchema.setOperator(mHandler);
        mLLCaseSchema.setMakeDate(mMakeDate);
        mLLCaseSchema.setMakeTime(mMakeTime);
        mLLCaseSchema.setModifyDate(mMakeDate);
        mLLCaseSchema.setModifyTime(mMakeTime);

        //新增一个PAD理赔标识--13
        mLLCaseSchema.setCaseProp("13");

        mLLCaseSchema.setRigister(mHandler);
        mLLCaseSchema.setClaimer(mHandler);
        mLLCaseSchema.setOtherIDType(mLCInsuredSchema.getOthIDType());
        mLLCaseSchema.setOtherIDNo(mLCInsuredSchema.getOthIDNo());

        mLLCaseSchema.setAccidentDate(mAccDate);

        //申请表信息
        mLLRegisterSchema.setRgtNo(mCaseNo);
        mLLRegisterSchema.setRgtState("13");
        //客户号
        mLLRegisterSchema.setRgtObj("1");
        mLLRegisterSchema.setRgtObjNo(mInsuredNo);
        //受理方式暂定为4-上门 吧，恩  pad理赔应该算上门服务，不对之后再改吧快赶不上二路汽车了
        mLLRegisterSchema.setRgtType("4");
        //个案受理
        mLLRegisterSchema.setRgtClass("0");
        mLLRegisterSchema.setRgtantName(mLLCaseSchema.getCustomerName());
//        银行信息
    	mLLRegisterSchema.setBankCode(mBankCode);
    	mLLRegisterSchema.setAccName(mAccName); 
      	mLLRegisterSchema.setBankAccNo(mAccNo); 
      	mLLRegisterSchema.setCaseGetMode(mCaseGetMode);
        //默认本人申请
        mLLRegisterSchema.setRelation("00");
//      申请日期
        mLLRegisterSchema.setAppDate(mMakeDate);
        mLLRegisterSchema.setCustomerNo(mInsuredNo);
        mLLRegisterSchema.setRgtDate(mMakeDate);
        mLLRegisterSchema.setHandler(mHandler);
        mLLRegisterSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLRegisterSchema.setIDType(mLLCaseSchema.getIDType());
        mLLRegisterSchema.setIDNo(mLLCaseSchema.getIDNo());
        mLLRegisterSchema.setOperator(mHandler);
        mLLRegisterSchema.setMakeDate(mMakeDate);
        mLLRegisterSchema.setMakeTime(mMakeTime);
        mLLRegisterSchema.setModifyDate(mMakeDate);
        mLLRegisterSchema.setModifyTime(mMakeTime);

        return true;
    }

    /**
     * 生成事件信息、疾病信息、意外信息、其他信息
     * @return boolean
     * @throws Exception
     */
    private boolean getSubInfo() throws Exception {
        System.out.println("LLPadAutoClaim--getSubInfo");
        
        List tReceiptList = new ArrayList();
        tReceiptList = (List) mReceiptList.getChildren();
//      只取第一个账单医院信息如有不同医院就需要调整报文格式
        Element mAccReceiptData = (Element) tReceiptList.get(0);
        String mHospitCode = mAccReceiptData.getChildText("HOSIPITALNO");
        String mHospitName = mAccReceiptData.getChildText("HOSIPITALNAME");
        
        List tDiseaseList = new ArrayList();
        tDiseaseList = (List) mDiseaseList.getChildren();
      //疾病信息
        for (int i = 0; i < tDiseaseList.size(); i++) {
        Element	tDiseaseData = (Element) tDiseaseList.get(i);
        
        String tDiseaseCode = tDiseaseData.getChildText("DISEASECODE");
        
        if ("".equals(tDiseaseCode) || "null".equals(tDiseaseCode)) {
            buildError("getSubInfo", "【疾病编码】的值不能为空");
            return false;
        }
        LDDiseaseDB tLDDiseaseDB = new LDDiseaseDB();
        tLDDiseaseDB.setICDCode(tDiseaseCode);
        if (!tLDDiseaseDB.getInfo()) {
            buildError("getSubInfo", "【疾病编码】不存在");
            return false;
        }
        String tDiseaseName = tDiseaseData.getChildText("DISEASENAME");//没用
        

        LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
        String tCureNo = PubFun1.CreateMaxNo("CURENO", mLimit);
        mLLCaseCureSchema.setSerialNo(tCureNo);
        mLLCaseCureSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());
        mLLCaseCureSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLCaseCureSchema.setDiseaseCode(tDiseaseCode);
        mLLCaseCureSchema.setDiseaseName(tLDDiseaseDB.getICDName());
        mLLCaseCureSchema.setHospitalCode(mHospitCode);
        mLLCaseCureSchema.setHospitalName(mHospitName);
        mLLCaseCureSchema.setCaseNo(mCaseNo);
        mLLCaseCureSchema.setCaseRelaNo(mCaseRelaNo);
        mLLCaseCureSchema.setSeriousFlag("0");
        mLLCaseCureSchema.setOperator(mHandler);
        mLLCaseCureSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLCaseCureSchema.setMakeDate(mMakeDate);
        mLLCaseCureSchema.setMakeTime(mMakeTime);
        mLLCaseCureSchema.setModifyDate(mMakeDate);
        mLLCaseCureSchema.setModifyTime(mMakeTime);
        
        mLLCaseCureSet.add(mLLCaseCureSchema);
        }



     

        //事件信息
        String tInHospitalDate = mAccReceiptData.getChildText("HOSPSTARTDATE");
        String tOutHospitalDate = mAccReceiptData.getChildText("HOSPENDDATE");
        if ("".equals(tInHospitalDate) || "null".equals(tInHospitalDate)) {
            buildError("getSubInfo", "【入院日期】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(tInHospitalDate)) {
            buildError("getSubInfo", "【入院日期】格式错误");
            return false;
        }

        if ("".equals(tOutHospitalDate) || "null".equals(tOutHospitalDate)) {
            buildError("getSubInfo", "【出院日期】的值不能为空");
            return false;
        }
        if (!PubFun.checkDateForm(tOutHospitalDate)) {
            buildError("getSubInfo", "【出院日期】格式错误");
            return false;
        }

        String tSubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", mLimit);
        mLLSubReportSchema.setSubRptNo(tSubRptNo);
        mLLSubReportSchema.setCustomerName(mLLCaseSchema.getCustomerName());
        mLLSubReportSchema.setCustomerNo(mLLCaseSchema.getCustomerNo());

        //事件类型只能是疾病、意外或者空
        String tAccDescType = mCustomerData.getChildText("ACCDESCTYPE");
        if ("1".equals(tAccDescType) || "2".equals(tAccDescType)) {
            mLLSubReportSchema.setAccidentType(tAccDescType);
        }
        
        // #3544 发生地点省、市、县
        String tAccProvinceCode = mCustomerData.getChildText("ACCPROVINCECODE");
        String tAccCityCode = mCustomerData.getChildText("ACCCITYCODE");
        String tAccCountyCode = mCustomerData.getChildText("ACCCOUNTYCODE");
        
        String tResult =LLCaseCommon.checkAccPlace(tAccProvinceCode, tAccCityCode, tAccCountyCode);
        if(!"".equals(tResult)){
        	buildError("getSubInfo", tResult);
            return false;
        }else{
        	mLLSubReportSchema.setAccProvinceCode(tAccProvinceCode);
        	mLLSubReportSchema.setAccCityCode(tAccCityCode);
        	mLLSubReportSchema.setAccCountyCode(tAccCountyCode);
        }
    
        mLLSubReportSchema.setAccDate(mAccDate);
        mLLSubReportSchema.setInHospitalDate(tInHospitalDate);
        mLLSubReportSchema.setOutHospitalDate(tOutHospitalDate);
//        mLLSubReportSchema.setAccDesc(mLLCaseCureSchema.getDiseaseName());
//        mLLSubReportSchema.setAccPlace(mLLCaseCureSchema.getHospitalName());
        mLLSubReportSchema.setOperator(mHandler);
        mLLSubReportSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLSubReportSchema.setMakeDate(mMakeDate);
        mLLSubReportSchema.setMakeTime(mMakeTime);
        mLLSubReportSchema.setModifyDate(mMakeDate);
        mLLSubReportSchema.setModifyTime(mMakeTime);
        
        //意外信息

        if ("2".equals(tAccDescType)) {
        	
            List tAccidentList = new ArrayList();
            tAccidentList = (List) mAccidentList.getChildren();

            for (int i = 0; i < tAccidentList.size(); i++) {
            Element	tAccidentData = (Element) tAccidentList.get(i);
            
            String tAccidentCode = tAccidentData.getChildText("ACCIDENTCODE");
            
            if ("".equals(tAccidentCode) || "null".equals(tAccidentCode)) {
                buildError("getSubInfo", "【意外代码】的值不能为空");
                return false;
            }
            LLAccidentTypeDB tLLAccidentTypeDB = new LLAccidentTypeDB();
            tLLAccidentTypeDB.setAccidentNo(tAccidentCode);
            if (!tLLAccidentTypeDB.getInfo()) {
                buildError("getSubInfo", "【意外代码】不存在");
                return false;
            }
            String tAccidentName = tAccidentData.getChildText("ACCIDENTNAME");
            
         	LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
         	String AccidentNo = PubFun1.CreateMaxNo("ACCIDENTNO", mLimit);
         	
         	tLLAccidentSchema.setAccidentNo(AccidentNo);
    		tLLAccidentSchema.setCaseNo(mCaseNo);
    		tLLAccidentSchema.setCaseRelaNo(mCaseRelaNo);
    		tLLAccidentSchema.setCode(tAccidentCode);
    		tLLAccidentSchema.setName(tLLAccidentTypeDB.getAccName());

    		mLLAccidentSet.add(tLLAccidentSchema);
            }
        	
        }

        //事件关联表
        mLLCaseRelaSchema.setCaseNo(mCaseNo);
        mLLCaseRelaSchema.setSubRptNo(tSubRptNo);
        mLLCaseRelaSchema.setCaseRelaNo(mCaseRelaNo);
        
//       其他信息
        String tSocailMedical = mCustomerData.getChildText("SOCAILMEDICAL");
        if("1".equals(tSocailMedical)||"0".equals(tSocailMedical)){
   		 
 			tLLOtherFactorSchema.setFactorType("9");//9-补充编码
 			tLLOtherFactorSchema.setFactorCode("9921");//固定编码
 			tLLOtherFactorSchema.setCaseNo(mCaseNo);
 			tLLOtherFactorSchema.setCaseRelaNo(mCaseRelaNo);
 			tLLOtherFactorSchema.setSubRptNo(tSubRptNo);
 			tLLOtherFactorSchema.setValue(tSocailMedical);
 			tLLOtherFactorSchema.setFactorName("享有社会医疗保险和公费医疗(0-是，1-否)");
 			
        }

        return true;
    }


    /**
     * 生成案件信息
     * @return boolean
     * @throws Exception
     */
    private boolean dealCase() throws Exception {
        System.out.println("LLPadAutoClaim--dealCase");
        mMMap.put(mLLRegisterSchema, "DELETE&INSERT");
        mMMap.put(mLLCaseSchema, "DELETE&INSERT");
        mMMap.put(mLLSubReportSchema, "DELETE&INSERT");
        mMMap.put(mLLCaseRelaSchema, "DELETE&INSERT");
        if (tLLOtherFactorSchema.getCaseNo() != null &&
            !"".equals(tLLOtherFactorSchema.getCaseNo()) &&
            !"null".equals(tLLOtherFactorSchema.getCaseNo())) {
            mMMap.put(tLLOtherFactorSchema, "DELETE&INSERT");
        }
//        
        if (mLLAccidentSet.size()>0) {
            mMMap.put(mLLAccidentSet, "DELETE&INSERT");
        }
//        
        mMMap.put(mLLCaseCureSet, "DELETE&INSERT");
        
        mMMap.put(mLLFeeMainSchema, "DELETE&INSERT");
        if (mLLCaseReceiptSet.size() > 0) {
            mMMap.put(mLLCaseReceiptSet, "DELETE&INSERT");
        }
//
        mMMap.put(mLLCasePolicySet, "DELETE&INSERT");
        mMMap.put(mLLToClaimDutySet, "DELETE&INSERT");

        try {
            this.mInputData.clear();
            this.mInputData.add(mMMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            // @@错误处理
            buildError("dealCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("dealCase", "数据提交失败");
            return false;
        }

        return true;
    }

    /**
     * 调用理算类计算
     * @return boolean
     * @throws Exception
     */
    private boolean claimCal() throws Exception {
        System.out.println("LLPadAutoClaim--claimCal");
        ClaimCalBL tClaimCalAutoBL = new ClaimCalBL();

        mGlobalInput.Operator = mHandler;
        mGlobalInput.ManageCom = mManageCom;

        VData aVData = new VData();
        aVData.addElement(mGlobalInput);
        aVData.addElement(mLLCaseSchema);
        if (!tClaimCalAutoBL.submitData(aVData, "autoCal")) {
        	CErrors tError=tClaimCalAutoBL.mErrors;
  			String Content = "<br>案件"+mCaseNo+",操作失败 "+ tError.getFirstError();
  			buildError("LJaget()", "理算报错："+Content);
            return false;
        }
        return true;
    }

    /**
     * 校验保额
     * @return boolean
     * @throws Exception
     */
    private boolean checkAmnt() throws Exception {
        System.out.println("LLPadAutoClaim--checkAmnt");
        LLClaimDetailDB tLLClaimDetailDB = new LLClaimDetailDB();
        tLLClaimDetailDB.setCaseNo(mCaseNo);
        LLClaimDetailSet tGetLLClaimDetailSet = tLLClaimDetailDB.query();

        if (tGetLLClaimDetailSet.size() < 1) {
            buildError("checkAmnt", "理算失败，请联系核心运维人员");
            return false;
        }

        for (int i = 1; i <= tGetLLClaimDetailSet.size(); i++) {
            LLClaimDetailSet tLLClaimDetailSet = new LLClaimDetailSet();
            LLClaimDetailSchema tLLClaimDetailSchema = tGetLLClaimDetailSet.get(
                    i);
            tLLClaimDetailSet.add(tLLClaimDetailSchema);

            LLUWCheckBL tLLUWCheckBL = new LLUWCheckBL();
            VData tVData = new VData();

            tVData.addElement(tGetLLClaimDetailSet);
            tVData.addElement(mLLCaseSchema);
            tVData.addElement(mGlobalInput);

            if (!tLLUWCheckBL.submitData(tVData, "")) {
            	CErrors tError=tLLUWCheckBL.mErrors;
	  			String Content = "<br>案件"+mCaseNo+",操作失败 "+ tError.getFirstError();
	  			buildError("LJaget()", "核赔报错："+Content);
                return false;
            }
            mLLClaimDetailSet.add(tLLClaimDetailSchema);
        }
        return true;
    }

    /**
     * 校验保额失败后，返回可以赔付的金额
     * @param tLLClaimDetailSchema LLClaimDetailSchema
     * @return double
     */
    private double getRealPay(LLClaimDetailSchema aLLClaimDetailSchema) {
        return 0.0;
    }

    /**
     * 增加赔付结论
     * @return boolean
     * @throws Exception
     */
    private boolean claimSave() throws Exception {
        System.out.println("LLPadAutoClaim--claimSave");
        if (mLLClaimDetailSet.size() < 1) {
            buildError("claimSave", "理算数据获取失败");
            return false;
        }

        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();
        tLLClaimPolicyDB.setCaseNo(mCaseNo);

        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        if (tLLClaimPolicySet.size() < 1) {
            buildError("claimSave", "理算数据获取失败");
            return false;
        }

        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(mCaseNo);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() < 1) {
            buildError("claimSave", "理算数据获取失败");
            return false;
        }

        LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCaseNo);
        if (!tLLCaseDB.getInfo()) {
            buildError("claimSave", "案件查询失败");
            return false;
        } else {
            mLLCaseSchema = tLLCaseDB.getSchema();
        }

        double tSumRealPay = 0;
        double tSumStandPay = 0;
        String tGiveType = "1";

        for (int j = 1; j <= tLLClaimPolicySet.size(); j++) {
            LLClaimPolicySchema tLLClaimPolicySchema = tLLClaimPolicySet.get(j);

            double tRealPay = 0;
            double tStandPay = 0;
            int have1 = 0;
            int have3 = 0;

            for (int i = 1; i <= mLLClaimDetailSet.size(); i++) {
                LLClaimDetailSchema tLLClaimDetailSchema =
                        mLLClaimDetailSet.get(i);

                if (tLLClaimPolicySchema.getPolNo().equals(tLLClaimDetailSchema.
                        getPolNo()) &&
                    tLLClaimPolicySchema.getGetDutyKind().equals(
                            tLLClaimDetailSchema.getGetDutyKind())) {

                    if (tLLClaimDetailSchema.getRealPay() > 0) {
                        tLLClaimDetailSchema.setGiveType("1");
                        tLLClaimDetailSchema.setGiveTypeDesc("正常给付");
                        tLLClaimDetailSchema.setGiveReason("01");
                        tLLClaimDetailSchema.setGiveReasonDesc("条款原因");
                    } else {
                        tLLClaimDetailSchema.setGiveType("3");
                        tLLClaimDetailSchema.setGiveTypeDesc("全额拒付");
                        tLLClaimDetailSchema.setGiveReason("99");
                        tLLClaimDetailSchema.setGiveReasonDesc("其他");
                    }

                    mConfirmLLClaimDetailSet.add(tLLClaimDetailSchema);

                    if ("3".equals(tLLClaimDetailSchema.getGiveType())) {
                    	have3 = 1;
                    } else {
                    	have1 = 1;
                    }
                    tRealPay += tLLClaimDetailSchema.getRealPay();
                    tStandPay += tLLClaimDetailSchema.getStandPay();
                }

            }
            if(have1==0 && have3==1){
            	tGiveType="3";
            }else if(have1==1 && have3==0){
            	tGiveType="1";
            }else {
            	tGiveType="2";
            }
            tLLClaimPolicySchema.setRealPay(Arith.round(tRealPay, 2));
            tLLClaimPolicySchema.setStandPay(Arith.round(tStandPay, 2));
            tLLClaimPolicySchema.setClmState("1"); //已结算
            tLLClaimPolicySchema.setClmUWer(mLLCaseSchema.getHandler());
            tLLClaimPolicySchema.setGiveType(tGiveType);
            tLLClaimPolicySchema.setGiveTypeDesc(LLCaseCommon.getGiveDesc(
                    tGiveType));

            mLLClaimPolicySet.add(tLLClaimPolicySchema);

            tSumRealPay += tLLClaimPolicySchema.getRealPay();
            tSumStandPay += tLLClaimPolicySchema.getStandPay();
        }

        mLLClaimSchema = tLLClaimSet.get(1);
        mLLClaimSchema.setRealPay(Arith.round(tSumRealPay, 2));
        mLLClaimSchema.setStandPay(Arith.round(tSumStandPay, 2));
        mLLClaimSchema.setGiveType(tGiveType);
        if ("3".equals(tGiveType)) {
            mLLCaseSchema.setDeclineFlag("1");
        } else {
            mLLCaseSchema.setDeclineFlag("");
        }
        mLLClaimSchema.setGiveTypeDesc(LLCaseCommon.getGiveDesc(tGiveType));

        return true;
    }

    private boolean dealClaimSave() throws Exception {
        System.out.println("LLPadAutoClaim--dealClaimSave");
        String delsql1 = "delete from llclaimdetail where caseno='" +
                         mCaseNo + "'";
        String delsql2 = "delete from llclaimPolicy where caseno='" +
                         mCaseNo + "'";
        String delsql3 = "delete from llclaim where caseno='" +
                         mCaseNo + "'";
        String delLJSGetClaim =
                "delete from ljsgetclaim where othernotype='5' and otherno='"
                + mCaseNo + "'";
        String delLJSGet =
                "delete from ljsget where othernotype='5' and otherno='"
                + mCaseNo + "'";

        MMap tMMap = new MMap();
        tMMap.put(delsql1, "DELETE");
        tMMap.put(delsql2, "DELETE");
        tMMap.put(delsql3, "DELETE");
        tMMap.put(delLJSGetClaim, "DELETE");
        tMMap.put(delLJSGet, "DELETE");

        tMMap.put(mLLCaseSchema, "UPDATE");
        tMMap.put(mConfirmLLClaimDetailSet, "DELETE&INSERT");
        tMMap.put(mLLClaimPolicySet, "DELETE&INSERT");
        tMMap.put(mLLClaimSchema, "DELETE&INSERT");
        tMMap.put(mLJSGetClaimSet, "DELETE&INSERT");
        tMMap.put(mLJSGetSchema, "DELETE&INSERT");
        tMMap.put(mLLHospCaseSchema, "DELETE&INSERT");

        try {
            this.mInputData.clear();
            this.mInputData.add(tMMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            buildError("dealClaimSave", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("dealClaimSave", "数据提交失败");
            return false;
        }

        return true;
    }

    /**
     * 生成应付
     * @return boolean
     * @throws Exception
     */
    private boolean genLJSGet() throws Exception {
        System.out.println("LLPadAutoClaim--genLJSGet");
        String sql = "select sum(realpay),stattype,GetDutyKind,polno,kindcode," +
                     "riskcode,riskver from llclaimdetail where caseno='" +
                     mCaseNo +
                     "' group by stattype,getdutykind,polno,kindcode,riskcode,riskver ";
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(sql);
        if (exeSQL.mErrors.getErrorCount() > 0 || ssrs == null ||
            ssrs.getMaxRow() <= 0) {
            buildError("genLJSGet", "生成应付统计错误");
            return false;
        }
        mLJSGetClaimSet = new LJSGetClaimSet();
        String tGetNoticeNo = PubFun1.CreateMaxNo("GetNoticeNo", mLimit);
        double tSumRealPay = 0;
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
            tLJSGetClaimSchema.setFeeFinaType(ssrs.GetText(i, 2));
            tLJSGetClaimSchema.setGetDutyKind(ssrs.GetText(i, 3));
            tLJSGetClaimSchema.setFeeOperationType(ssrs.GetText(i, 3));
            tLJSGetClaimSchema.setPay(0);
            for (int k = 1; k <= mLLClaimDetailSet.size(); k++) {
                LLClaimDetailSchema aLLClaimDetailSchema = mLLClaimDetailSet.
                        get(k);
                if (ssrs.GetText(i, 2).equals(aLLClaimDetailSchema.getStatType()) &&
                    ssrs.GetText(i,
                                 3).equals(aLLClaimDetailSchema.getGetDutyKind()) &&
                    ssrs.GetText(i, 4).equals(aLLClaimDetailSchema.getPolNo()) &&
                    ssrs.GetText(i, 5).equals(aLLClaimDetailSchema.getKindCode()) &&
                    ssrs.GetText(i, 6).equals(aLLClaimDetailSchema.getRiskCode()) &&
                    ssrs.GetText(i, 7).equals(aLLClaimDetailSchema.getRiskVer())) {
                    double apaymoney = tLJSGetClaimSchema.getPay() +
                                       aLLClaimDetailSchema.getRealPay();
                    apaymoney = Arith.round(apaymoney, 2);
                    tLJSGetClaimSchema.setPay(apaymoney);
                }
            }
            tLJSGetClaimSchema.setPolNo(ssrs.GetText(i, 4));
            tLJSGetClaimSchema.setKindCode(ssrs.GetText(i, 5));
            tLJSGetClaimSchema.setRiskCode(ssrs.GetText(i, 6));
            tLJSGetClaimSchema.setRiskVersion(ssrs.GetText(i, 7));
            //添加续保查询
            LCPolBL tLCPolBL = new LCPolBL();
            tLCPolBL.setPolNo(ssrs.GetText(i, 4));
            if (!tLCPolBL.getInfo()) {
                buildError("genLJSGet", "保单查询失败[" + ssrs.GetText(i, 4) + "]");
                return false;
            }
            tLJSGetClaimSchema.setGetNoticeNo(tGetNoticeNo);
            tLJSGetClaimSchema.setContNo(tLCPolBL.getContNo());
            tLJSGetClaimSchema.setGrpContNo(tLCPolBL.getGrpContNo());
            tLJSGetClaimSchema.setGrpPolNo(tLCPolBL.getGrpPolNo());
            tLJSGetClaimSchema.setAgentCode(tLCPolBL.getAgentCode());
            tLJSGetClaimSchema.setAgentCom(tLCPolBL.getAgentCom());
            tLJSGetClaimSchema.setAgentGroup(tLCPolBL.getAgentGroup());
            tLJSGetClaimSchema.setAgentType(tLCPolBL.getAgentType());
            tLJSGetClaimSchema.setOperator(mLLCaseSchema.getHandler());
            tLJSGetClaimSchema.setManageCom(tLCPolBL.getManageCom());
            tLJSGetClaimSchema.setMakeDate(mMakeDate);
            tLJSGetClaimSchema.setMakeTime(mMakeTime);
            tLJSGetClaimSchema.setModifyDate(mMakeDate);
            tLJSGetClaimSchema.setModifyTime(mMakeTime);
            tLJSGetClaimSchema.setOtherNo(mCaseNo);
            tLJSGetClaimSchema.setOtherNoType("5");

            mLJSGetClaimSet.add(tLJSGetClaimSchema);
            tSumRealPay += tLJSGetClaimSchema.getPay();
        }
        Reflections tref = new Reflections();
        tref.transFields(mLJSGetSchema, mLJSGetClaimSet.get(1));

        tSumRealPay = Arith.round(tSumRealPay, 2);
        mLJSGetSchema.setSumGetMoney(tSumRealPay);
        return true;
    }

    /**
     * 计算完成后，完成数据处理
     * @return boolean
     * @throws Exception
     */
    private boolean endCal() throws Exception {
        System.out.println("LLPadAutoClaim--endCal");
        try {
            //校验保额
            if (!checkAmnt()) {
                return false;
            }

            //确定赔付结论
            if (!claimSave()) {
                return false;
            }

            //生成应付
            if (!genLJSGet()) {
                return false;
            }

            //记录交易轨迹
            if (!makeHospCase()) {
                return false;
            }

            //提交理算确认数据
            if (!dealClaimSave()) {
                return false;
            }
        } catch (Exception ex) {
            buildError("endCal", "处理理算数据失败");
            return false;
        }
        return true;
    }

    /**
     * 审批审定准备
     * @return
     */
    private boolean simpleClaimAudit() {
    	//TODO Auto-generated method stub
		//1.数据校验
    	LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    	tLLClaimUserDB.setUserCode(mHandler);
        if(!tLLClaimUserDB.getInfo()){
            // @@错误处理
            buildError("simpleClaimAudit()", "您不是理赔人，无权作审定操作");
            mDealState = false; 
            return false;
        }
        if(tLLClaimUserDB.getClaimPopedom()==null){
            // @@错误处理
            buildError("simpleClaimAudit()", "您没有理赔权限，无权作审定操作");
            mDealState = false; 
            return false;
        }
        mLLClaimUserSchema = tLLClaimUserDB.getSchema();
        
        //2.准备数据
            if(!UWCase(mLLCaseSchema)){
            buildError("simpleClaimAudit()", "您没有理赔权限，无权作审定操作");
            mDealState = false; 
            return false;   
            }
        
    	
		return true;
	}
    
    /**
     * 审批审定
     * @param aCaseSchema
     * @return
     */
    private boolean UWCase(LLCaseSchema aCaseSchema){
    	
    	LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();
    	LLClaimSchema tLLClaimSchema = new LLClaimSchema();
    	LLClaimDB tLLClaimDB = new LLClaimDB();
    	tLLClaimDB.setCaseNo(mCaseNo);
    	LLClaimSet tLLClaimSet = tLLClaimDB.query();
    	 if (tLLClaimSet.size()<=0) {
             buildError("simpleClaimAudit", "案件查询失败");
             return false;
         } else {
        	 tLLClaimSchema = tLLClaimSet.get(1);
         }
//    	 按照个案流程准备审批数据，这么点应该就行了
    	tLLClaimUWMainSchema.setRgtNo(mCaseNo);
    	tLLClaimUWMainSchema.setCaseNo(mCaseNo);
    	tLLClaimUWMainSchema.setClmNo(tLLClaimSchema.getClmNo());

    	tLLClaimUWMainSchema.setRemark1("PAD理赔审核");
    	
    	VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aCaseSchema);
        tVData.add(tLLClaimUWMainSchema);
        
        ClaimUnderwriteBL tClaimUnderwriteBL = new ClaimUnderwriteBL();
        
        if(!tClaimUnderwriteBL.submitData(tVData,"")){
    	  CErrors tError = tClaimUnderwriteBL.mErrors;
    	  String ErrMessage = tError.getFirstError();
    	  String mReturnMessage = "<br>案件" + aCaseSchema.getCaseNo()
              + "审批失败,原因是:"+ErrMessage;
    	  buildError("simpleClaimAudit()", "审批审定报错："+mReturnMessage);
          mDealState = false; 
    	  return false;
        }
        
  	  return true;
  
    }
    
    /**
     * 通知给付
     * @param aCaseSchema
     * @return
     */
    private boolean LJaget(){
    	LLCaseDB tLLCaseDB = new LLCaseDB();
   	 	tLLCaseDB.setCaseNo(mCaseNo);
	   	 if (!tLLCaseDB.getInfo()) {
			 CError.buildErr(this, "理赔案件信息查询失败！");
			 return false;
		 }else{
			 if("05".equals(tLLCaseDB.getRgtState())){
				 CError.buildErr(this, "账单金额已超出您的理赔权限，不能继续处理");
				 return false;
			 }
		 }
    	LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    	tLJAGetSchema.setOtherNo(mLLCaseSchema.getCaseNo());
		tLJAGetSchema.setOtherNoType("5");
		tLJAGetSchema.setAccName(mLLCaseSchema.getAccName());
		tLJAGetSchema.setBankAccNo(mLLCaseSchema.getBankAccNo());
		tLJAGetSchema.setPayMode(mLLCaseSchema.getCaseGetMode());
		tLJAGetSchema.setBankCode(mLLCaseSchema.getBankCode());
		tLJAGetSchema.setDrawer(mLLCaseSchema.getAccName());//领取人为账户名 和个案通知给付默认查询逻辑保持一致
		if(mLLCaseSchema.getCustomerName().equals(mLLCaseSchema.getAccName())){
			tLJAGetSchema.setDrawerID(mLLCaseSchema.getIDNo());//和个案通知给付默认查询逻辑保持一致,账户名和被保险人名字一致则使用被保险人证件号，后续有变化再调整
		}
		
		VData tVData = new VData();
		  tVData.add(mLLCaseSchema);
		  tVData.add(tLJAGetSchema);  
		  tVData.add(mGlobalInput);	
		  
		  LJAGetInsertBL  tLJAGetInsertBL = new LJAGetInsertBL();
		  if(!tLJAGetInsertBL.submitData(tVData,"")) {
			  CErrors tError=tLJAGetInsertBL.mErrors;
			  String Content = "<br>案件"+mCaseNo+",操作失败 "+ tError.getFirstError();
			  buildError("LJaget()", "通知给付报错："+Content);
	          mDealState = false; 
	          return false;
		  }
	    	
	    return true;
    }
    private boolean cancelCase() throws Exception {
        System.out.println("LLPadAutoClaim--cancelCase");
        //理算数据
        String tClaimSQL = "delete from LLClaim where CaseNo='" + mCaseNo + "'";
        String tClaimPolicySQL = "delete from LLClaimPolicy where CaseNo='" +
                                 mCaseNo + "'";
        String tClaimDetailSQL = "delete from LLClaimDetail where CaseNo='" +
                                 mCaseNo + "'";
        String tInsureAccTrace = "delete from LCInsureAccTrace where OtherNo='" +
                                 mCaseNo + "'";
        String tToClaimDutySQL = "delete from LLToClaimDuty where CaseNo ='" +
                                 mCaseNo + "'";
        String tCasePolicySQL = "delete from LLCasePolicy where CaseNo ='"
                                + mCaseNo + "'";

        String tFeeMainSQL = "delete from LLFeeMain where CaseNo='" + mCaseNo +
                             "'";
        String tSecuritySQL = "delete from LLSecurityReceipt where CaseNo = '" +
                              mCaseNo + "'";
        String tFeeItemSQL = "delete from LLFeeOtherItem where CaseNo = '" +
                             mCaseNo + "'";
        String tCaseReceiptSQL = "delete from LLCaseReceipt where CaseNo = '" +
                                 mCaseNo + "'";
        String tCaseDrugSQL = "delete from LLCaseDrug where CaseNo = '" +
                              mCaseNo + "'";
        String tCaseCureSQL = "delete from LLCaseCure where CaseNo = '" +
                              mCaseNo + "'";
        String tOperationSQL = "delete from LLOperation where CaseNo = '" +
                               mCaseNo + "'";
        String tAccidentSQL = "delete from LLAccident where CaseNo = '" +
                               mCaseNo + "'";
        String tLLClaimUnderwrite = "delete from LLClaimUnderwrite where CaseNo = '" +
        						mCaseNo + "'";
        String tLLClaimUWMDetail = "delete from LLClaimUWMDetail where CaseNo = '" +
        						mCaseNo + "'";
        String tLLClaimUWDetail = "delete from LLClaimUWDetail where CaseNo = '" +
								mCaseNo + "'";
        
        String LLClaimUWMain = "delete from LLClaimUWMain where CaseNo = '" +
								mCaseNo + "'";
        
        String tLJAGet = "delete from LJAGet where othernotype='5' and otherno = '" +
        						mCaseNo + "'";
        String tLJAGetClaim = "delete from LJAGetClaim where othernotype='5' and otherno = '" +
								mCaseNo + "'";
        

//        String tSubReportSQL = "delete from LLSubReport a where exists (select 1 from llcaserela where a.subrptno=subrptno and CaseNo='" +
//                               mCaseNo + "')";
//        String tCaseRelaSQL = "delete from LLCaseRela where CaseNo='" + mCaseNo +
//                              "'";
        String tCaseSQL = "update  LLCase set rgtstate='14' ,CancleReason='9',CancleRemark='PAD理赔自动撤件',CancleDate=current date where CaseNo='" + mCaseNo + "'";

//        String tRegisterSQL = "delete from LLRegister where RgtNo ='" + mCaseNo +
//                              "'";

//        String tHospCaseSQL = "delete from LLHospCase where CaseNo ='" +
//                              mCaseNo + "'";

        MMap tmpMap = new MMap();
        tmpMap.put(tClaimSQL, "DELETE");
        tmpMap.put(tClaimPolicySQL, "DELETE");
        tmpMap.put(tClaimDetailSQL, "DELETE");
        tmpMap.put(tInsureAccTrace, "DELETE");
        tmpMap.put(tToClaimDutySQL, "DELETE");
        tmpMap.put(tCasePolicySQL, "DELETE");
        tmpMap.put(tFeeMainSQL, "DELETE");
        tmpMap.put(tSecuritySQL, "DELETE");
        tmpMap.put(tCaseReceiptSQL, "DELETE");
        tmpMap.put(tCaseDrugSQL, "DELETE");
        tmpMap.put(tFeeItemSQL, "DELETE");
        tmpMap.put(tCaseCureSQL, "DELETE");
        tmpMap.put(tOperationSQL, "DELETE");
        tmpMap.put(tAccidentSQL, "DELETE");
        tmpMap.put(tLLClaimUnderwrite, "DELETE");
        tmpMap.put(tLLClaimUWMDetail, "DELETE");
        tmpMap.put(tLLClaimUWDetail, "DELETE");
        tmpMap.put(LLClaimUWMain, "DELETE");
        
        tmpMap.put(tLJAGet, "DELETE");
        tmpMap.put(tLJAGetClaim, "DELETE");
//        tmpMap.put(tSubReportSQL, "DELETE");
//        tmpMap.put(tCaseRelaSQL, "DELETE");
        tmpMap.put(tCaseSQL, "UPDATE");
//        tmpMap.put(tRegisterSQL, "DELETE");
//        tmpMap.put(tHospCaseSQL, "DELETE");

        try {
            this.mInputData.clear();
            this.mInputData.add(tmpMap);
            mResult.clear();
            mResult.add(this.mLLCaseSchema);
        } catch (Exception ex) {
            buildError("cancelCase", "准备提交数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            buildError("cancelCase", "撤销数据提交失败");
            return false;
        }

        return false;
    }

    /**
     * 生成对外接口案件表
     * @return boolean
     * @throws Exception
     */
    private boolean makeHospCase() throws Exception {
        System.out.println("LLPadAutoClaim--makeHospCase");
        mLLHospCaseSchema.setCaseNo(mLLCaseSchema.getCaseNo());
        mLLHospCaseSchema.setHospitCode(mRequestType);
        mLLHospCaseSchema.setHandler(mLLCaseSchema.getHandler());
        mLLHospCaseSchema.setAppTranNo(mTransactionNum);
        mLLHospCaseSchema.setAppDate(mMakeDate);
        mLLHospCaseSchema.setAppTime(mMakeTime);
        mLLHospCaseSchema.setRealpay(mLLClaimSchema.getRealPay());
        mLLHospCaseSchema.setDealType("2");
        mLLHospCaseSchema.setCaseType("06");//pad理赔接口
        mLLHospCaseSchema.setConfirmState("1");
        mLLHospCaseSchema.setMakeDate(mMakeDate);
        mLLHospCaseSchema.setMakeTime(mMakeTime);
        mLLHospCaseSchema.setModifyDate(mMakeDate);
        mLLHospCaseSchema.setModifyTime(mMakeTime);
        return true;
    }


    
    /**
     * 解析理算过程
     * @param aLLElementDetailSet
     * @return
     * @throws Exception
     */
    private String parseClaimResult(LLElementDetailSet aLLElementDetailSet) {
    	if (aLLElementDetailSet.size() <= 0) {
    		buildError("parseClaimResult", "查询理算要素出错。");
            return "";
    	}
    	    	
        PubCalculator tPubCalculator = new PubCalculator();
        String tCalCode = "";
        
        for (int i = 1 ; i <= aLLElementDetailSet.size(); i++) {
        	LLElementDetailSchema tLLElementDetailSchema = aLLElementDetailSet.get(i);
        	if (i==1) {
        		tCalCode = tLLElementDetailSchema.getCalCode();
        	} else {
        		if (!tCalCode.equals(tLLElementDetailSchema.getCalCode())) {
        			buildError("parseClaimResult", "理算要素计算编码出错。");
                    return "";
        		}
        	}
        	
        	tPubCalculator.addBasicFactor(tLLElementDetailSchema.getElementCode(), tLLElementDetailSchema.getElementValue());
        }
        
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setCalCode(tCalCode);
        if (!tLMCalModeDB.getInfo()) {
        	buildError("parseClaimResult", "理算算法查询失败。");
            return "";
        }

        String tSql = tLMCalModeDB.getRemark().trim();
        String tStr = "";
        String tStr1 = "";
        try {
            while (true) {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals("")) {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        } catch (Exception ex) {
            // @@错误处理
            buildError("parseClaimResult", "解释" + tSql + "的变量:" + tStr + "时出错。");
            return "";
        }
        if ("0".equals(tSql.trim())) {
        	tSql = "解析"+tLMCalModeDB.getCalCode()+"提示信息出错。";
        }
        if (tSql.trim().equals("")) {
            return tLMCalModeDB.getRemark();
        }
        return tSql;
    }
    private boolean LLPadScan(){
    	//处理人取pad传过来的
    	mPicPath = mPictureData.getChildText("PICPATH");
    	TransferData transferData = new TransferData();
    	transferData.setNameAndValue("PicPath", mPicPath);
    	VData tVData = new VData();
		  tVData.add(mLLCaseSchema);
		  tVData.add(mGlobalInput);	
		  tVData.add(transferData);	
		  LLPadGetScanImageBL tLLPadGetScanImageBL = new LLPadGetScanImageBL();
		  if(!tLLPadGetScanImageBL.submitData(tVData,"")) {
			  CErrors tError=tLLPadGetScanImageBL.mErrors;
			  String Content = "<br>案件"+mCaseNo+",操作失败 "+ tError.getFirstError();
			  buildError("LLPadScan()", "扫描件上传报错："+Content);
	          mDealState = false; 
	          return false;
		  }
		  return true;
    }

    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:/理赔报文/PAD/YD03.xml"), "GBK");

            LLPadAutoClaim tBusinessDeal = new LLPadAutoClaim();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
//            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
