/**
 * 2014-5-20
 */
package com.sinosoft.lis.yibaotong;

import org.jdom.Element;


/**
 * @author 
 *
 */
public class LLTJMajorDiseasesCaseParser
{
    
    
   
    //节点名称
	private String RGTNO="";//批次号
    private String CLAIMNO="";//平台理赔号
    private String CASENUM="";     //案件顺序号-为返回报文准备
    private String CUSTOMERNAME="";//姓名
    private String IDNO="";     //身份证号
    private String SECURYTINO="";     //医保号
    private String INSUREDSTAT="";     //人员类别
    private String SEX="";     //性别
    private String HOSPITALNAME="";     //医院名称
    private String HOSPITALNAMECODE="";     //医院编码
    private String FEETYPE="";     //就诊类别
    private String RECEIPTNO="";   //账单号码  
    private String FEEDATE="";     //账单日期
    private String HOSPSTARTDATE="";     //入院日期
    private String HOSPENDDATE="";     //出院日期
    private String REALHOSPDATE="";     //住院天数
    private String APPLYAMNT="";     //费用总额
    private String GETLIMIT="";     //起付钱
    private String ACCOUNTPAY="";     //账户支付
    private String PLANFEE="";     //统筹支付   
    private String MIDAMNT="";     //统筹自付
    private String HIGHAMNT1="";     //大额救助支付
    private String SUPINHOSFEE="";     //大额救助自付   
    private String SELFPAY1="";     //个人自付
    private String SELFPAY2="";     //部分自付
    private String SELFAMNT="";     //全自费
    private String OTHERORGANAMNT="";     //第三方支付
    private String DISEASENAME="";     //疾病诊断   
    private String DISEASECODE="";     //疾病编码
    private String ACCDATE="";     //发生日期
    private String AFFIXNO="";     //实赔金额   
    private String ORIGINFLAG="";     //医疗标志
    private String GETDUTYCODE="";     //给付责任编码
    private String BANKCODE="";     //银行编码
    private String BANKNAME="";     //银行名称
    private String ACCNAME="";     //账户名
    private String BANKACCNO="";     //账号
    private String GETDUTYKIND="";     //给负责任类型
    private String CUSTOMERNO="";     //客户号   
    private String INPATIENTNO="";    //住院号   
    private String MAKEDATE="";     //经办时间
    
    private String MOBILEPHONE="";     //手机号
    private String REFUSEAMNT="";     //不合理费用
    private String REIMBBURSEMENT="";     //可报销范围内金额
    private String REMARK="";     //扣除明细
    
    //3500
    private String ACCPROVINCECODE=""; //发生地点(省)
    private String ACCCITYCODE=""; //发生地点(市)
    private String ACCCOUNTYCODE=""; //发生地点(县)
    private String MEDICARETYPE=""; //医保类型
    
    private boolean mDealState=true;

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO 自动生成方法存根

    }
    
    /**解析报文返回理赔信息
     * @author zjd
     */
    public LLTJMajorDiseasesCaseParser(Element mLLcase){
    	
    	RGTNO=mLLcase.getChildTextTrim("RGTNO");
        CLAIMNO=mLLcase.getChildTextTrim("CLAIMNO");
        CASENUM=mLLcase.getChildTextTrim("CASENUM");
        CUSTOMERNAME=mLLcase.getChildTextTrim("CUSTOMERNAME");
        IDNO=mLLcase.getChildTextTrim("IDNO");
        SECURYTINO=mLLcase.getChildTextTrim("SECURYTINO");
        INSUREDSTAT=mLLcase.getChildTextTrim("INSUREDSTAT");
        SEX=mLLcase.getChildTextTrim("SEX");
        HOSPITALNAME=mLLcase.getChildTextTrim("HOSPITALNAME");
        HOSPITALNAMECODE=mLLcase.getChildTextTrim("HOSPITALNAMECODE");
        FEETYPE=mLLcase.getChildTextTrim("FEETYPE");
        RECEIPTNO=mLLcase.getChildTextTrim("RECEIPTNO");
        FEEDATE=mLLcase.getChildTextTrim("FEEDATE");
        HOSPSTARTDATE=mLLcase.getChildTextTrim("HOSPSTARTDATE");
        HOSPENDDATE=mLLcase.getChildTextTrim("HOSPENDDATE");
        REALHOSPDATE=mLLcase.getChildTextTrim("REALHOSPDATE");
        APPLYAMNT=mLLcase.getChildTextTrim("APPLYAMNT");
        GETLIMIT=mLLcase.getChildTextTrim("GETLIMIT");
        ACCOUNTPAY=mLLcase.getChildTextTrim("ACCOUNTPAY");
        PLANFEE=mLLcase.getChildTextTrim("PLANFEE");
        MIDAMNT=mLLcase.getChildTextTrim("MIDAMNT");
        HIGHAMNT1=mLLcase.getChildTextTrim("HIGHAMNT1");
        SUPINHOSFEE=mLLcase.getChildTextTrim("SUPINHOSFEE");
        SELFPAY1=mLLcase.getChildTextTrim("SELFPAY1");
        SELFPAY2=mLLcase.getChildTextTrim("SELFPAY2");
        SELFAMNT=mLLcase.getChildTextTrim("SELFAMNT");
        OTHERORGANAMNT=mLLcase.getChildTextTrim("OTHERORGANAMNT");
        DISEASENAME=mLLcase.getChildTextTrim("DISEASENAME");
        DISEASECODE=mLLcase.getChildTextTrim("DISEASECODE");
        ACCDATE=mLLcase.getChildTextTrim("ACCDATE");
        AFFIXNO=mLLcase.getChildTextTrim("AFFIXNO");
        ORIGINFLAG=mLLcase.getChildTextTrim("ORIGINFLAG");
        GETDUTYCODE=mLLcase.getChildTextTrim("GETDUTYCODE");
        BANKCODE=mLLcase.getChildTextTrim("BANKCODE");
        BANKNAME=mLLcase.getChildTextTrim("BANKNAME");
        ACCNAME=mLLcase.getChildTextTrim("ACCNAME");
        BANKACCNO=mLLcase.getChildTextTrim("BANKACCNO");
        GETDUTYKIND=mLLcase.getChildTextTrim("GETDUTYKIND");
        CUSTOMERNO=mLLcase.getChildTextTrim("CUSTOMERNO");
        INPATIENTNO=mLLcase.getChildTextTrim("INPATIENTNO");
        MAKEDATE=mLLcase.getChildTextTrim("MAKEDATE");
        MOBILEPHONE=mLLcase.getChildTextTrim("MOBILEPHONE");
        REFUSEAMNT=mLLcase.getChildTextTrim("REFUSEAMNT");
        REIMBBURSEMENT=mLLcase.getChildTextTrim("REIMBURSEMENT");
        REMARK=mLLcase.getChildTextTrim("REMARK");
        
        //3500
        ACCPROVINCECODE=mLLcase.getChildTextTrim("ACCPROVINCECODE");
        ACCCITYCODE=mLLcase.getChildTextTrim("ACCCITYCODE");
        ACCCOUNTYCODE=mLLcase.getChildTextTrim("ACCCOUNTYCODE");
        MEDICARETYPE=mLLcase.getChildTextTrim("MEDICARETYPE");
    }
    
   

    /**
     * @return aCCDATE
     */
    public String getACCDATE()
    {
        return ACCDATE;
    }

    /**
     * @param accdate 要设置的 aCCDATE
     */
    public void setACCDATE(String accdate)
    {
        ACCDATE = accdate;
    }

    /**
     * @return aCCNAME
     */
    public String getACCNAME()
    {
        return ACCNAME;
    }

    /**
     * @param accname 要设置的 aCCNAME
     */
    public void setACCNAME(String accname)
    {
        ACCNAME = accname;
    }

    /**
     * @return aCCOUNTPAY
     */
    public String getACCOUNTPAY()
    {
        return ACCOUNTPAY;
    }

    /**
     * @param accountpay 要设置的 aCCOUNTPAY
     */
    public void setACCOUNTPAY(String accountpay)
    {
        ACCOUNTPAY = accountpay;
    }

    /**
     * @return aFFIXNO
     */
    public String getAFFIXNO()
    {
        return AFFIXNO;
    }

    /**
     * @param affixno 要设置的 aFFIXNO
     */
    public void setAFFIXNO(String affixno)
    {
        AFFIXNO = affixno;
    }

    /**
     * @return aPPLYAMNT
     */
    public String getAPPLYAMNT()
    {
        return APPLYAMNT;
    }

    /**
     * @param applyamnt 要设置的 aPPLYAMNT
     */
    public void setAPPLYAMNT(String applyamnt)
    {
        APPLYAMNT = applyamnt;
    }

    /**
     * @return bANKACCNO
     */
    public String getBANKACCNO()
    {
        return BANKACCNO;
    }

    /**
     * @param bankaccno 要设置的 bANKACCNO
     */
    public void setBANKACCNO(String bankaccno)
    {
        BANKACCNO = bankaccno;
    }

    /**
     * @return bANKCODE
     */
    public String getBANKCODE()
    {
        return BANKCODE;
    }

    /**
     * @param bankcode 要设置的 bANKCODE
     */
    public void setBANKCODE(String bankcode)
    {
        BANKCODE = bankcode;
    }

    /**
     * @return bANKNAME
     */
    public String getBANKNAME()
    {
        return BANKNAME;
    }

    /**
     * @param bankname 要设置的 bANKNAME
     */
    public void setBANKNAME(String bankname)
    {
        BANKNAME = bankname;
    }

    /**
     * @return cUSTOMERNAME
     */
    public String getCUSTOMERNAME()
    {
        return CUSTOMERNAME;
    }

    /**
     * @param customername 要设置的 cUSTOMERNAME
     */
    public void setCUSTOMERNAME(String customername)
    {
        CUSTOMERNAME = customername;
    }

    /**
     * @return cUSTOMERNO
     */
    public String getCUSTOMERNO()
    {
        return CUSTOMERNO;
    }

    /**
     * @param customerno 要设置的 cUSTOMERNO
     */
    public void setCUSTOMERNO(String customerno)
    {
        CUSTOMERNO = customerno;
    }

    /**
     * @return dISEASECODE
     */
    public String getDISEASECODE()
    {
        return DISEASECODE;
    }

    /**
     * @param diseasecode 要设置的 dISEASECODE
     */
    public void setDISEASECODE(String diseasecode)
    {
        DISEASECODE = diseasecode;
    }

    /**
     * @return dISEASENAME
     */
    public String getDISEASENAME()
    {
        return DISEASENAME;
    }

    /**
     * @param diseasename 要设置的 dISEASENAME
     */
    public void setDISEASENAME(String diseasename)
    {
        DISEASENAME = diseasename;
    }

    /**
     * @return fEEDATE
     */
    public String getFEEDATE()
    {
        return FEEDATE;
    }

    /**
     * @param feedate 要设置的 fEEDATE
     */
    public void setFEEDATE(String feedate)
    {
        FEEDATE = feedate;
    }

    /**
     * @return fEETYPE
     */
    public String getFEETYPE()
    {
        return FEETYPE;
    }

    /**
     * @param feetype 要设置的 fEETYPE
     */
    public void setFEETYPE(String feetype)
    {
        FEETYPE = feetype;
    }

    /**
     * @return gETDUTYCODE
     */
    public String getGETDUTYCODE()
    {
        return GETDUTYCODE;
    }

    /**
     * @param getdutycode 要设置的 gETDUTYCODE
     */
    public void setGETDUTYCODE(String getdutycode)
    {
        GETDUTYCODE = getdutycode;
    }

    /**
     * @return gETDUTYKIND
     */
    public String getGETDUTYKIND()
    {
        return GETDUTYKIND;
    }

    /**
     * @param getdutykind 要设置的 gETDUTYKIND
     */
    public void setGETDUTYKIND(String getdutykind)
    {
        GETDUTYKIND = getdutykind;
    }

    /**
     * @return gETLIMIT
     */
    public String getGETLIMIT()
    {
        return GETLIMIT;
    }

    /**
     * @param getlimit 要设置的 gETLIMIT
     */
    public void setGETLIMIT(String getlimit)
    {
        GETLIMIT = getlimit;
    }

    /**
     * @return hIGHAMNT1
     */
    public String getHIGHAMNT1()
    {
        return HIGHAMNT1;
    }

    /**
     * @param highamnt1 要设置的 hIGHAMNT1
     */
    public void setHIGHAMNT1(String highamnt1)
    {
        HIGHAMNT1 = highamnt1;
    }

    /**
     * @return hOSPENDDATE
     */
    public String getHOSPENDDATE()
    {
        return HOSPENDDATE;
    }

    /**
     * @param hospenddate 要设置的 hOSPENDDATE
     */
    public void setHOSPENDDATE(String hospenddate)
    {
        HOSPENDDATE = hospenddate;
    }

    /**
     * @return hOSPITALNAME
     */
    public String getHOSPITALNAME()
    {
        return HOSPITALNAME;
    }

    /**
     * @param hospitalname 要设置的 hOSPITALNAME
     */
    public void setHOSPITALNAME(String hospitalname)
    {
        HOSPITALNAME = hospitalname;
    }

    /**
     * @return hOSPITALNAMECODE
     */
    public String getHOSPITALNAMECODE()
    {
        return HOSPITALNAMECODE;
    }

    /**
     * @param hospitalnamecode 要设置的 hOSPITALNAMECODE
     */
    public void setHOSPITALNAMECODE(String hospitalnamecode)
    {
        HOSPITALNAMECODE = hospitalnamecode;
    }

    /**
     * @return hOSPSTARTDATE
     */
    public String getHOSPSTARTDATE()
    {
        return HOSPSTARTDATE;
    }

    /**
     * @param hospstartdate 要设置的 hOSPSTARTDATE
     */
    public void setHOSPSTARTDATE(String hospstartdate)
    {
        HOSPSTARTDATE = hospstartdate;
    }

    /**
     * @return iDNO
     */
    public String getIDNO()
    {
        return IDNO;
    }

    /**
     * @param idno 要设置的 iDNO
     */
    public void setIDNO(String idno)
    {
        IDNO = idno;
    }

    /**
     * @return iNPATIENTNO
     */
    public String getINPATIENTNO()
    {
        return INPATIENTNO;
    }

    /**
     * @param inpatientno 要设置的 iNPATIENTNO
     */
    public void setINPATIENTNO(String inpatientno)
    {
        INPATIENTNO = inpatientno;
    }

    /**
     * @return iNSUREDSTAT
     */
    public String getINSUREDSTAT()
    {
        return INSUREDSTAT;
    }

    /**
     * @param insuredstat 要设置的 iNSUREDSTAT
     */
    public void setINSUREDSTAT(String insuredstat)
    {
        INSUREDSTAT = insuredstat;
    }

    /**
     * @return mAKEDATE
     */
    public String getMAKEDATE()
    {
        return MAKEDATE;
    }

    /**
     * @param makedate 要设置的 mAKEDATE
     */
    public void setMAKEDATE(String makedate)
    {
        MAKEDATE = makedate;
    }

    /**
     * @return mDealState
     */
    public boolean isMDealState()
    {
        return mDealState;
    }

    /**
     * @param dealState 要设置的 mDealState
     */
    public void setMDealState(boolean dealState)
    {
        mDealState = dealState;
    }

   

    /**
     * @return mIDAMNT
     */
    public String getMIDAMNT()
    {
        return MIDAMNT;
    }

    /**
     * @param midamnt 要设置的 mIDAMNT
     */
    public void setMIDAMNT(String midamnt)
    {
        MIDAMNT = midamnt;
    }

   

    /**
     * @return mOBILEPHONE
     */
    public String getMOBILEPHONE()
    {
        return MOBILEPHONE;
    }

    /**
     * @param mobilephone 要设置的 mOBILEPHONE
     */
    public void setMOBILEPHONE(String mobilephone)
    {
        MOBILEPHONE = mobilephone;
    }


    /**
     * @return oRIGINFLAG
     */
    public String getORIGINFLAG()
    {
        return ORIGINFLAG;
    }

    /**
     * @param originflag 要设置的 oRIGINFLAG
     */
    public void setORIGINFLAG(String originflag)
    {
        ORIGINFLAG = originflag;
    }

    /**
     * @return oTHERORGANAMNT
     */
    public String getOTHERORGANAMNT()
    {
        return OTHERORGANAMNT;
    }

    /**
     * @param otherorganamnt 要设置的 oTHERORGANAMNT
     */
    public void setOTHERORGANAMNT(String otherorganamnt)
    {
        OTHERORGANAMNT = otherorganamnt;
    }

    /**
     * @return pLANFEE
     */
    public String getPLANFEE()
    {
        return PLANFEE;
    }

    /**
     * @param planfee 要设置的 pLANFEE
     */
    public void setPLANFEE(String planfee)
    {
        PLANFEE = planfee;
    }

    /**
     * @return rEALHOSPDATE
     */
    public String getREALHOSPDATE()
    {
        return REALHOSPDATE;
    }

    /**
     * @param realhospdate 要设置的 rEALHOSPDATE
     */
    public void setREALHOSPDATE(String realhospdate)
    {
        REALHOSPDATE = realhospdate;
    }

    /**
     * @return rECEIPTNO
     */
    public String getRECEIPTNO()
    {
        return RECEIPTNO;
    }

    /**
     * @param receiptno 要设置的 rECEIPTNO
     */
    public void setRECEIPTNO(String receiptno)
    {
        RECEIPTNO = receiptno;
    }

    /**
     * @return rEFUSEAMNT
     */
    public String getREFUSEAMNT()
    {
        return REFUSEAMNT;
    }

    /**
     * @param refuseamnt 要设置的 rEFUSEAMNT
     */
    public void setREFUSEAMNT(String refuseamnt)
    {
        REFUSEAMNT = refuseamnt;
    }

    /**
     * @return rEIMBBURSEMENT
     */
    public String getREIMBBURSEMENT()
    {
        return REIMBBURSEMENT;
    }

    /**
     * @param reimbbursement 要设置的 rEIMBBURSEMENT
     */
    public void setREIMBBURSEMENT(String reimbbursement)
    {
        REIMBBURSEMENT = reimbbursement;
    }

    /**
     * @return rEMARK
     */
    public String getREMARK()
    {
        return REMARK;
    }

    /**
     * @param remark 要设置的 rEMARK
     */
    public void setREMARK(String remark)
    {
        REMARK = remark;
    }

    /**
     * @return sECURYTINO
     */
    public String getSECURYTINO()
    {
        return SECURYTINO;
    }

    /**
     * @param securytino 要设置的 sECURYTINO
     */
    public void setSECURYTINO(String securytino)
    {
        SECURYTINO = securytino;
    }

    /**
     * @return sELFAMNT
     */
    public String getSELFAMNT()
    {
        return SELFAMNT;
    }

    /**
     * @param selfamnt 要设置的 sELFAMNT
     */
    public void setSELFAMNT(String selfamnt)
    {
        SELFAMNT = selfamnt;
    }

    /**
     * @return sELFPAY1
     */
    public String getSELFPAY1()
    {
        return SELFPAY1;
    }

    /**
     * @param selfpay1 要设置的 sELFPAY1
     */
    public void setSELFPAY1(String selfpay1)
    {
        SELFPAY1 = selfpay1;
    }

    /**
     * @return sELFPAY2
     */
    public String getSELFPAY2()
    {
        return SELFPAY2;
    }

    /**
     * @param selfpay2 要设置的 sELFPAY2
     */
    public void setSELFPAY2(String selfpay2)
    {
        SELFPAY2 = selfpay2;
    }

    /**
     * @return sEX
     */
    public String getSEX()
    {
        return SEX;
    }

    /**
     * @param sex 要设置的 sEX
     */
    public void setSEX(String sex)
    {
        SEX = sex;
    }

    /**
     * @return sUPINHOSFEE
     */
    public String getSUPINHOSFEE()
    {
        return SUPINHOSFEE;
    }

    /**
     * @param supinhosfee 要设置的 sUPINHOSFEE
     */
    public void setSUPINHOSFEE(String supinhosfee)
    {
        SUPINHOSFEE = supinhosfee;
    }

    /**
     * @return cASENUM
     */
    public String getCASENUM()
    {
        return CASENUM;
    }

    /**
     * @param casenum 要设置的 cASENUM
     */
    public void setCASENUM(String casenum)
    {
        CASENUM = casenum;
    }

    /**
     * @return cLAIMNO
     */
    public String getCLAIMNO()
    {
        return CLAIMNO;
    }

    /**
     * @param claimno 要设置的 cLAIMNO
     */
    public void setCLAIMNO(String claimno)
    {
        CLAIMNO = claimno;
    }

	public String getRGTNO() {
		return RGTNO;
	}

	public void setRGTNO(String rgtno) {
		RGTNO = rgtno;
	}
//3500
	public String getACCPROVINCECODE() {
		return ACCPROVINCECODE;
	}

	public void setACCPROVINCECODE(String accprovincecode) {
		ACCPROVINCECODE = accprovincecode;
	}
	public String getACCCITYCODE() {
		return ACCCITYCODE;
	}

	public void setACCCITYCODE(String acccitycode) {
		ACCCITYCODE = acccitycode;
	}
	public String getACCCOUNTYCODE() {
		return ACCCOUNTYCODE;
	}

	public void setACCCOUNTYCODE(String acccountycode) {
		ACCCOUNTYCODE = acccountycode;
	}
	public String getMEDICARETYPE() {
		return MEDICARETYPE;
	}

	public void setMEDICARETYPE(String medicaretype) {
		MEDICARETYPE = medicaretype;
	}
	
}
