package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

//test用
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLHospEndCaseSY implements ServiceInterface {
    public LLHospEndCaseSY() {
    }

    public CErrors mErrors = new CErrors();
    public VData mResult = new VData();
    private MMap tMap = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();

    private Document mInXmlDoc; //传入报文
    private String mDocType = ""; //报文类型
    private boolean mDealState = true; //处理标志
    private String mResponseCode = "1"; //返回类型代码
    private String mRequestType = ""; //请求类型
    private String mErrorMessage = ""; //错误描述
    private String mTransactionNum = ""; //交互编码
    private String mTranHospCode = ""; //交互编码中医院代码
    private Element mBodyData; //报文Body部分

    //提取客户保单信息
    private String mCase_Number = ""; //赔案号
    private String mAccept_Flag = ""; //客户确认标记 0-签字、1-不签字
    private String mManageCom = ""; //机构编码
    private LLCaseSchema mLLCaseSchema = new LLCaseSchema();
    private String mClmNo = ""; //赔案号

    public Document service(Document pInXmlDoc) {
        mInXmlDoc = pInXmlDoc;
        try {
            if (!getInputData(mInXmlDoc)) {
                mDealState = false;
            } else {
                if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;
            mResponseCode = "E";
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"14".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransactionNum == null || mTransactionNum == "" ||
            mTransactionNum.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        if (!mRequestType.equals(mTransactionNum.substring(0, 2))) {
            buildError("checkData()", "【交互编码】与【请求类型】匹配错误");
            return false;
        }
        //校验获取报文的赔案号、是否确认 信息非空
        if (mCase_Number == null || "".equals(mCase_Number)) {
            buildError("checkData()", "获取报文的赔案号为空!");
            return false;
        }

        if (mAccept_Flag == null || "".equals(mAccept_Flag)) {
            buildError("checkData()", "获取报文的确认信息为空!");
            return false;
        }

        if (!"1".equals(mAccept_Flag) && !"2".equals(mAccept_Flag)) {
            buildError("checkData()", "【确认信息】的值不存在");
            return false;
        }

        mTranHospCode = mTransactionNum.substring(2, 9);

        LLHospComRightDB tLLHospComRightDB = new LLHospComRightDB();
        tLLHospComRightDB.setHospitCode(mTranHospCode);
        tLLHospComRightDB.setRequestType(mRequestType);
        tLLHospComRightDB.setState("0");
        LLHospComRightSet tLLHospComRightSet = new LLHospComRightSet();
        tLLHospComRightSet = tLLHospComRightDB.query();

        if (tLLHospComRightSet.size() < 1) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }

        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        tLDHospitalDB.setHospitCode(mTranHospCode);
        if (!tLDHospitalDB.getInfo()) {
            buildError("checkData()", "该医院无通信权限");
            return false;
        }
        mManageCom = tLDHospitalDB.getManageCom();
        if ("".equals(mManageCom)) {
            buildError("checkData()", "医院所属机构错误");
            return false;
        }

        return true;
    }

    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        Element tBasepart = tBodyData.getChild("BASE_PART");

        mCase_Number = tBasepart.getChildTextTrim("CASE_NUMBER");
        mAccept_Flag = tBasepart.getChildTextTrim("ACCEPT_FLAG");
        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
    	
        /*******************************
         * * 1、校验LLCase案件信息
         * **************/
    if(mCase_Number.startsWith("C")){
    	LLCaseDB tLLCaseDB = new LLCaseDB();
        tLLCaseDB.setCaseNo(mCase_Number);

        if (!tLLCaseDB.getInfo()) {
            buildError("dealData()", "查询理赔案件信息失败!");
            return false;
        }
        mLLCaseSchema = tLLCaseDB.getSchema();

        if (!mLLCaseSchema.getRgtState().equals("04")) {
            buildError("dealData()", "案件目前处于非理算状态!");
            return false;
        }

        /*******************************
         * * 2、校验并处理LLHospCase医保通案件信息
         * **************/
        LLHospCaseDB tLLHospCaseDB = new LLHospCaseDB();
        LLHospCaseSchema tLLHospCaseSchema = new LLHospCaseSchema();
        tLLHospCaseDB.setCaseNo(mCase_Number);
//        tLLHospCaseDB.setConfirmState("0");
        if (!tLLHospCaseDB.getInfo()) {
            buildError("dealData()", "查询医保通案件信息失败!");
            return false;
        }

        tLLHospCaseSchema = tLLHospCaseDB.getSchema();

        if (!"1".equals(tLLHospCaseSchema.getDealType())
            || !"0".equals(tLLHospCaseSchema.getConfirmState())) {
            buildError("dealData()", "医保通案件状态错误!");
            return false;
        }

        if (!mTranHospCode.equals(tLLHospCaseSchema.getHospitCode())) {
            buildError("dealData()", "医院编码与交互编码不符!");
            return false;
        }

        tLLHospCaseSchema.setConfirmTranNo(mTransactionNum);
        tLLHospCaseSchema.setConfirmDate(PubFun.getCurrentDate());
        tLLHospCaseSchema.setConfirmTime(PubFun.getCurrentTime());
        tLLHospCaseSchema.setModifyDate(PubFun.getCurrentDate());
        tLLHospCaseSchema.setModifyTime(PubFun.getCurrentTime());

        if (mAccept_Flag.equals("1")) {
            tLLHospCaseSchema.setConfirmState("1");
            if (!endCase()) {
                return false;
            }
        } else if (mAccept_Flag.equals("2")) {
            tLLHospCaseSchema.setConfirmState("2");
        }
        tMap.put(tLLHospCaseSchema, "UPDATE");

        mResult.clear();
        mResult.add(tMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            buildError("dealData()", "数据库保存失败！");
            return false;
        }
        }
     else{
        	LLHospAccTraceSchema tLLHospAccTraceSchema =new LLHospAccTraceSchema();
        	LLHospAccTraceDB tLLHospAccTraceDB =new LLHospAccTraceDB();
        	tLLHospAccTraceDB.setTCaseNo(mCase_Number);
        if(!tLLHospAccTraceDB.getInfo()){
        	buildError("dealData()", "查询医保通案件信息失败!");
            return false;
        }
        tLLHospAccTraceSchema=tLLHospAccTraceDB.getSchema();
        if (!"0".equals(tLLHospAccTraceSchema.getConfirmState())) {
                buildError("dealData()", "医保通案件状态错误!");
                return false;
        }
        if (!mTranHospCode.equals(tLLHospAccTraceSchema.getHospitCode())) {
                buildError("dealData()", "医院编码与交互编码不符!");
                return false;
        }
        tLLHospAccTraceSchema.setConfirmTranNo(mTransactionNum);
        tLLHospAccTraceSchema.setConfirmDate(PubFun.getCurrentDate());
        tLLHospAccTraceSchema.setConfirmTime(PubFun.getCurrentTime());
        tLLHospAccTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLLHospAccTraceSchema.setModifyTime(PubFun.getCurrentTime());
        if (mAccept_Flag.equals("1")) {
        	tLLHospAccTraceSchema.setConfirmState("1");
        	
        } else if (mAccept_Flag.equals("2")) {
        	tLLHospAccTraceSchema.setConfirmState("2");
        }
        tMap.put(tLLHospAccTraceSchema, "UPDATE");
        mResult.clear();
        mResult.add(tMap);

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            buildError("dealData()", "数据库保存失败！");
            return false;
        }
    }
       return true;
    }
    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
        //处理失败，返回错误信息报文
        if (!mDealState) {
            return createFalseXML();
        } else {
            return createResultXML();
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "0";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() {
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);

        if (!"E".equals(mResponseCode)) {
            mResponseCode = "1";
        }
        mErrorMessage = mErrors.getFirstError();

        Element tResponseCode = new Element("RESPONSE_CODE");
        tResponseCode.setText(mResponseCode);
        tHeadData.addContent(tResponseCode);

        Element tErrorMessage = new Element("ERROR_MESSAGE");
        tErrorMessage.setText(mErrorMessage);
        tHeadData.addContent(tErrorMessage);

        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);

        tRootData.addContent(tHeadData);

        Document tDocument = new Document(tRootData);

        return tDocument;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLHospEndCase";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

    private boolean endCase() throws Exception {
        /*******************************
         * * 3、校验并处理核赔信息
         * 3.1、校验赔案信息llclaim，获取赔案号clmno
         * **************/
        LLClaimDB tLLClaimDB = new LLClaimDB();
        tLLClaimDB.setCaseNo(mCase_Number);
        LLClaimSet tLLClaimSet = tLLClaimDB.query();
        if (tLLClaimSet.size() <= 0) {
            buildError("dealData()", "赔案信息查询失败，该案件尚未理算！");
            return false;
        }
        mClmNo = tLLClaimSet.get(1).getClmNo(); //赔案号

        /*******************************
         * * 3、校验并处理核赔信息
         * 3.2、校验赔案明细表LLClaimPolicy
         * **************/
        LLClaimPolicyDB tLLClaimPolicyDB = new LLClaimPolicyDB();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
        tLLClaimPolicyDB.setClmNo(mClmNo);
        LLClaimPolicySet tLLClaimPolicySet = tLLClaimPolicyDB.query();
        if (tLLClaimPolicySet.size() <= 0) {
            buildError("dealData()", "赔案明细表查询失败，没有理算结果！");
            return false;
        }

        /*******************************
         * * 3、校验并处理核赔信息
         * 3.3、处理核赔表LLClaimUnderwrite、LLClaimUWDetail、LLClaimUWMain
         * **************/
        LLClaimUnderwriteSet tLLClaimUnderwriteSet = new LLClaimUnderwriteSet();
        LLClaimUWDetailSet tLLClaimUWDetailSet = new LLClaimUWDetailSet();

        LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
        LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
//        //当前操作员的核赔权限
//        String sqlUser =
//                "select ClaimPopedom from llclaimuser where UserCode='" +
//                mLLCaseSchema.getHandler() + "'";
        tLLClaimUserDB.setUserCode(mLLCaseSchema.getHandler());
        if (!tLLClaimUserDB.getInfo()) {
            buildError("dealData()", "理赔人无权限信息");
            return false;
        }
        tLLClaimUserSchema = tLLClaimUserDB.getSchema();
        LLClaimUserDB tUpLLClaimUserDB = new LLClaimUserDB();
        tUpLLClaimUserDB.setUserCode(tLLClaimUserSchema.getUpUserCode());
        if (!tUpLLClaimUserDB.getInfo()) {
        	buildError("dealData()", "上级理赔人无权限信息");
            return false;
        }
        
        if (!"1".equals(tUpLLClaimUserDB.getStateFlag())) {
        	buildError("dealData()", "理赔人上级状态非有效");
            return false;
        }
        for (int i = 1; i <= tLLClaimPolicySet.size(); i++) {
            LLClaimPolicySchema tLLClaimPolicySchema = tLLClaimPolicySet.get(i);
            if (tLLClaimPolicySchema.getGiveType() == null) {
                buildError("dealData()", "保单无赔付结论,请先做理算确认！");
                return false;
            }
            Reflections trf = new Reflections();
            LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new
                    LLClaimUnderwriteSchema();
            trf.transFields(tLLClaimUnderwriteSchema, tLLClaimPolicySchema);

            //准备核赔记录数据LLClaimUnderwrite
            tLLClaimUnderwriteSchema.setClmDecision(tLLClaimPolicySchema.
                    getGiveType()); //核赔结论
            tLLClaimUnderwriteSchema.setClmDepend(tLLClaimPolicySchema.
                                                  getGiveReason()); //核赔依据
            tLLClaimUnderwriteSchema.setClmUWer(mLLCaseSchema.getHandler()); //核赔员
            tLLClaimUnderwriteSchema.setClmUWGrade(tLLClaimUserSchema.
                    getClaimPopedom());
            tLLClaimUnderwriteSchema.setCheckType("0"); //审批审定类型
            tLLClaimUnderwriteSchema.setAppGrade(tUpLLClaimUserDB.getClaimPopedom());
            tLLClaimUnderwriteSchema.setAppClmUWer(tUpLLClaimUserDB.getUserCode());
            tLLClaimUnderwriteSchema.setAppActionType("3");
            tLLClaimUnderwriteSchema.setOperator(mLLCaseSchema.getHandler());
            tLLClaimUnderwriteSchema.setMngCom(mLLCaseSchema.getMngCom());
            tLLClaimUnderwriteSchema.setMakeDate(PubFun.getCurrentDate());
            tLLClaimUnderwriteSchema.setMakeTime(PubFun.getCurrentTime());
            tLLClaimUnderwriteSchema.setModifyDate(PubFun.getCurrentDate());
            tLLClaimUnderwriteSchema.setModifyTime(PubFun.getCurrentTime());
            tLLClaimUnderwriteSet.add(tLLClaimUnderwriteSchema);

            //准备核赔履历记录数据LLClaimUWDetail
            LLClaimUWDetailSchema tLLClaimUWDetailSchema = new
                    LLClaimUWDetailSchema();
            LLClaimUWDetailDB tmLLClaimUWDetailDB = new LLClaimUWDetailDB();

            tmLLClaimUWDetailDB.setClmNo(mClmNo);
            tmLLClaimUWDetailDB.setPolNo(tLLClaimPolicySchema.getPolNo());
            int tCount;
            tCount = tmLLClaimUWDetailDB.getCount();
            tCount++;
            trf.transFields(tLLClaimUWDetailSchema, tLLClaimUnderwriteSchema);
            tLLClaimUWDetailSchema.setGetDutyKind(tLLClaimPolicySchema.
                                                  getGetDutyKind());
            tLLClaimUWDetailSchema.setGetDutyCode(tLLClaimPolicySchema.
                                                  getGetDutyKind());
            tLLClaimUWDetailSchema.setClmUWNo(String.valueOf(tCount)); //核赔次数
            tLLClaimUWDetailSchema.setGetDutyCode(tLLClaimPolicySchema.
                                                  getGetDutyKind());
            tLLClaimUWDetailSet.add(tLLClaimUWDetailSchema);
        }

        //准备核赔履历记录数据LLClaimUWMain
        LLClaimUWMainSchema mLLClaimUWMainSchema = new LLClaimUWMainSchema();
        mLLClaimUWMainSchema.setMakeDate(PubFun.getCurrentDate());
        mLLClaimUWMainSchema.setMakeTime(PubFun.getCurrentTime());

        mLLClaimUWMainSchema.setClmNo(tLLClaimUWDetailSet.get(1).getClmNo());
        mLLClaimUWMainSchema.setRgtNo(mCase_Number);
        mLLClaimUWMainSchema.setCaseNo(mCase_Number);
        mLLClaimUWMainSchema.setOperator(mLLCaseSchema.getHandler());
        mLLClaimUWMainSchema.setMngCom(mLLCaseSchema.getMngCom());
        mLLClaimUWMainSchema.setModifyDate(PubFun.getCurrentDate());
        mLLClaimUWMainSchema.setModifyTime(PubFun.getCurrentTime());
        mLLClaimUWMainSchema.setAppPhase("0"); //审核
        mLLClaimUWMainSchema.setClmUWer(mLLCaseSchema.getHandler());
        mLLClaimUWMainSchema.setClmUWGrade(tLLClaimUserSchema.getClaimPopedom());
        mLLClaimUWMainSchema.setClmDecision(tLLClaimSet.get(1).getGiveType());
        mLLClaimUWMainSchema.setcheckDecision1("1");
        mLLClaimUWMainSchema.setRemark1("医保通前置审核");
        mLLClaimUWMainSchema.setAppClmUWer(tUpLLClaimUserDB.getUserCode());
        mLLClaimUWMainSchema.setAppGrade(tUpLLClaimUserDB.getClaimPopedom());
        mLLClaimUWMainSchema.setAppActionType("3");
        
        tMap.put(tLLClaimUnderwriteSet.get(1), "DELETE&INSERT");
        tMap.put(tLLClaimUWDetailSet.get(1), "INSERT");
        tMap.put(mLLClaimUWMainSchema, "INSERT");

        /*******************************
         * * 3、校验并处理核赔信息
         * 3.4、处理案件llcase信息
         * **************/
        mLLCaseSchema.setRgtState("05");
        mLLCaseSchema.setUWer(mLLCaseSchema.getHandler());
        mLLCaseSchema.setUWDate(PubFun.getCurrentDate());
        mLLCaseSchema.setSigner(mLLClaimUWMainSchema.getAppClmUWer());
        mLLCaseSchema.setSignerDate(PubFun.getCurrentDate());
        mLLCaseSchema.setDealer(mLLClaimUWMainSchema.getAppClmUWer());
        mLLCaseSchema.setModifyDate(PubFun.getCurrentDate());
        mLLCaseSchema.setModifyTime(PubFun.getCurrentTime());        
        tMap.put(mLLCaseSchema, "DELETE&INSERT");

        //准备案件核赔履历信息LLClaimUWMDetail
        LLClaimUWMDetailDB tLLClaimUWMDetailDB = new LLClaimUWMDetailDB();
        tLLClaimUWMDetailDB.setClmNo(mClmNo);
        int tCount = tLLClaimUWMDetailDB.getCount();
        tCount++;
        Reflections ref = new Reflections();
        LLClaimUWMDetailSchema tClmUWMDlSchema = new LLClaimUWMDetailSchema();
        ref.transFields(tClmUWMDlSchema, mLLClaimUWMainSchema);
        tClmUWMDlSchema.setClmUWNo(String.valueOf(tCount));
        tClmUWMDlSchema.setRemark(mLLClaimUWMainSchema.getRemark2());
        tClmUWMDlSchema.setOperator(mLLCaseSchema.getHandler());
        tClmUWMDlSchema.setMngCom(mLLCaseSchema.getMngCom());
        tClmUWMDlSchema.setMakeDate(PubFun.getCurrentDate());
        tClmUWMDlSchema.setMakeTime(PubFun.getCurrentTime());
        tClmUWMDlSchema.setModifyDate(PubFun.getCurrentDate());
        tClmUWMDlSchema.setModifyTime(PubFun.getCurrentTime());

        tMap.put(tClmUWMDlSchema, "DELETE&INSERT");
        return true;

    }


    public static void main(String[] args) {
//    	System.out.println("程序开始…");
//
//		XMLPubTool tXMLPubTool = new XMLPubTool();
//		Element eRequestType = tXMLPubTool.makeElement("REQUEST_TYPE", "01");
//		Element eTransactionNum = tXMLPubTool.makeElement("TRANSACTION_NUM", "0151010120091209");
//
//		Element eHead = tXMLPubTool.makeEmptyElement("HEAD");
//		Element ePacket = tXMLPubTool.makeComplexElement("PACKET", "type", "REQUEST");
//		tXMLPubTool.AddAttribute(ePacket, "version", "1.0");
//
//		tXMLPubTool.LinkElement(ePacket, eHead);
//		tXMLPubTool.LinkElement(eHead,eRequestType);
//		tXMLPubTool.LinkElement(eHead,eTransactionNum);
//
//		Document tNoStdXml = new Document(ePacket);
//		JdomUtil.print(tNoStdXml);
//		return tNoStdXml;
//
//
//        LLHospCustomerQuery tTest = new LLHospCustomerQuery();
////        System.out.println(tTest.mRequestType);
//        tTest.mDealState = true;
//
//        tTest.mResponseCode = "1";
//        //请求类型
//        tTest.mRequestType = "03";
//        //错误描述
//        tTest.mErrorMessage = "错了吧";
//        //交互编码
//        tTest.mTransactionNum = "123456789";
//
//        Element tRootData = new Element("PACKET");
//        Document tDocument = new Document(tRootData);
////        tTest.service(tDocument);

//        return null;
//    	LLHospContQuery tTest = new LLHospContQuery();
////        System.out.println(tTest.mRequestType);
//        tTest.mDealState = true;
//
//        tTest.mResponseCode = "1";
//        //请求类型
//        tTest.mRequestType = "02";
//        //错误描述
//        tTest.mErrorMessage = "错了吧";
//        //交互编码
//        tTest.mTransactionNum = "123456789";
//
//        Element tRootData = new Element("PACKET");
//        Document tDocument = new Document(tRootData);
//        tTest.service(tDocument);
//
        Document tInXmlDoc;
        try { 
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:/04_A.xml"), "GBK");

            LLHospEndCaseSY tBusinessDeal = new LLHospEndCaseSY();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
