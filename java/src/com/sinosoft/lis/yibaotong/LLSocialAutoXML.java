package com.sinosoft.lis.yibaotong;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.lis.yibaotong.ServiceInterface;
import com.sinosoft.lis.schema.*;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.io.FileInputStream;

/**
 * <p>Title: 自动立案理算</p>
 *
 * <p>Description: 生成案件至理算状态</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLSocialAutoXML implements ServiceInterface {
    public LLSocialAutoXML() {
    }

    public CErrors mErrors = new CErrors();
    //private VData mInputData = new VData();
    public VData mResult = new VData();
    private MMap mMMap = new MMap();

    //传入报文
    private Document mInXmlDoc;
    //报文类型
    private String mDocType = "";
    //处理标志
    private boolean mDealState = true;


    //请求类型
    private String mRequestType = "";
    //错误描述
    private String mErrorMessage = "";
    //处理报文返回标志
    private String mActuGetNo;
    
    //理赔信息序号
    private String mTransacTionNo="";
    //批次流水号
    private String mSocialRgtNo= "";
    //保单号
    private String mGrpContNo= "";
    //案件流水号
    private String mSocialCaseNo= "";
    //账单项目流水号
    private String mSocialFeeNo="";
    
    //管理机构
    private String mMngCom="";
    
    //操作员
    private String mOperator="sicjk";
    
    private String mMakeDate = PubFun.getCurrentDate();

	private String mMakeTime = PubFun.getCurrentTime();
    //报文批次部分
    private Element mRGTList;
    //报文案件部分
    private Element mLLCaseList;
    //报文案件-账单费用部分
    private Element mLLFeeMainList;
    //报文案件-疾病部分
    private Element mDiseaseList;
    //报文案件-重疾部分
    private Element mSeriousList;
    //报文案件-手术部分
    private Element mOperationList;
    //报文案件-理赔明细部分
    private Element mPayList;
    //报文案件-账单费用-账单项目部分
    private Element mPrescriptionList;
    //报文案件-账单费用-费用明细部分
    private Element mFeeItemList;
    
    //社保批次信息
    private LLRgtSocialSchema mLLRgtSocialSchema = new LLRgtSocialSchema();
    
    private LLRgtSocialSet mLLRgtSocialSet = new LLRgtSocialSet();
    //社保案件信息
    private LLCaseSocialSchema mLLCaseSocialSchema = new LLCaseSocialSchema();
    private LLCaseSocialSet mLLCaseSocialSet = new LLCaseSocialSet();
    
    private LLPrescripSocialSchema  mLLPrescripSocialSchema =new LLPrescripSocialSchema(); 
    private LLPrescripSocialSet mLLPrescripSocialSet = new LLPrescripSocialSet();
    
    private LLAccDetailSocialSchema  mLLAccDetailSocialSchema = new LLAccDetailSocialSchema();
    private LLAccDetailSocialSet mLLAccDetailSocialSet = new LLAccDetailSocialSet();

    private LLFeeItemSocialSchema  mLLFeeItemSocialSchema = new LLFeeItemSocialSchema();
    private LLFeeItemSocialSet  mLLFeeItemSocialSet =new LLFeeItemSocialSet();

    private LLClaimDetailSocialSchema mLLClaimDetailSocialSchema = new LLClaimDetailSocialSchema();
    private LLClaimDetailSocialSet mLLClaimDetailSocialSet = new LLClaimDetailSocialSet();
    
    //报文处理
    LLSocialAutoCase mLLSocialAutoCase = new LLSocialAutoCase();
	VData aVData = new VData(); 

    
    public Document service(Document pInXmlDoc) {
        
        System.out.println("LLSocialAutoXML--service");
        mInXmlDoc = pInXmlDoc;

        try {
        	//报文获取
            if (!getInputData(mInXmlDoc)) {
            	mDealState = false;
            } else {
                if (!checkData()) {
                	mDealState = false;
                } else {
                    if (!dealData()) {
                    	//cancelCase();
                    	mDealState = false;
                    }
                }
            }
            //报文数据保存
            if(!submit()){
            	mDealState = false;
            }
            
            //报文处理
            if(mDealState){
            	 if(!AutoCase()){
                 	mDealState = false;
                 }
            }
           
        } catch (Exception ex) {

            System.out.println(ex.getMessage());
            
            mDealState = false;
        } finally {
        		//用于返回XML
        	return createXML();
        }
    }

    /**
     * 校验基本数据
     * @return boolean
     */
    private boolean checkData() throws Exception {
        System.out.println("LLSocialAutoXML--checkData");
        if (!"REQUEST".equals(mDocType)) {
            buildError("checkData()", "报文类型【type='" + mDocType + "'】错误");
            return false;
        }

        if (!"PICCSOCIAL".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误");
            return false;
        }

        if (mTransacTionNo == null || mTransacTionNo == "" ||
        		mTransacTionNo.length() < 9) {
            buildError("checkData()", "【交互编码】编码错误");
            return false;
        }

        return true;
    }
    private boolean submit(){
        mMMap.put(mLLRgtSocialSet, "INSERT");
        mMMap.put(mLLCaseSocialSet, "INSERT");
        mMMap.put(mLLPrescripSocialSet, "INSERT");
        mMMap.put(mLLFeeItemSocialSet, "INSERT");
        mMMap.put(mLLClaimDetailSocialSet, "INSERT");
        mMMap.put(mLLAccDetailSocialSet, "INSERT");
    	mResult.add(mMMap);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, "INSERT"))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSocialAutoXML";
            tError.functionName = "submit";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //mInputData = null;
        return true;
    }
    /**
     * 解析报文主要部分
     * @param cInXml Document
     * @return boolean
     */
    private boolean getInputData(Document cInXml) throws Exception {
        System.out.println("LLSocialAutoXML--getInputData");
        if (cInXml == null) {
            buildError("getInputData()", "未获得报文");
            return false;
        }

        Element tRootData = cInXml.getRootElement();
        mDocType = tRootData.getAttributeValue("type");

        //获取HEAD部分内容
        Element tHeadData = tRootData.getChild("HEAD");

        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
        mTransacTionNo = tHeadData.getChildTextTrim("TRANSACTION_NUM");

        System.out.println("LLSocialAutoXML--mTransacTionNo:" +mTransacTionNo);

        //获取BODY部分内容
        Element tBodyData = tRootData.getChild("BODY");
        mRGTList = tBodyData.getChild("RGTLIST");
        mLLCaseList = tBodyData.getChild("LLCASELIST");
        return true;
    }

    /**
     * 处理报文
     * @return boolean
     */
    private boolean dealData() throws Exception {
        System.out.println("LLSocialAutoXML--dealData");
        //获取并存储社保批次信息	LLRgtSocial相关		
        if (!RgtSocial()) {
            return false;
        }
        //获取并存储社保案件信息	LLCaseSocial相关
        if (!CaseSocial()) {
            return false;
        }

        
        return true;
    }

    private boolean AutoCase(){
		System.out.println("tSocialRgtNo====="+mSocialRgtNo);
    	TransferData mTransferData = new TransferData();
    	mTransferData.setNameAndValue("SocialRgtNo", mSocialRgtNo);
   	 	aVData.add(mTransferData);
   	 	if(!mLLSocialAutoCase.submitData(aVData, "SB")){
	   	 	CErrors tError = mLLSocialAutoCase.mErrors;
	        String ErrMessage = tError.getFirstError();
	        buildError("错误信息",ErrMessage);
	        return false;
   	 	}
    
   	 	return true;
    }
    
    
    /**
     * 生成返回的报文信息
     * @return Document
     */
    private Document createXML() {
    	
        //返回信息报文
        if (!mDealState||mLLSocialAutoCase.getActuGetNo()=="") {
            return createFalseXML();
        } else {
        	try {
        		return createResultXML();
        	} catch (Exception ex) {
        		
                buildError("createXML()", "生成返回报文错误");
        		return createFalseXML();
        	}
        }
    }

    /**
     * 生成错误信息报文
     * @return Document
     */
    private Document createFalseXML() {
        //Request部分
        Element tRootData = new Element("Request");
        
        //Head部分
        Element tHeadData = new Element("head");
        
        
        Element tTransactionNum = new Element("Transaction_NUM");
        tTransactionNum.addContent(mTransacTionNo);
        tHeadData.addContent(tTransactionNum);
        //body部分
        Element tBodyData = new Element("body");
        
   	
        Element tPayStatus = new Element("PayStatus");
        tPayStatus.addContent("0");
        tBodyData.addContent(tPayStatus);
        Element tPayEason = new Element("PayEason");
        tPayEason.addContent(mErrors.getFirstError());
        tBodyData.addContent(tPayEason);
        Element tGetNoticeNoData = new Element("GetNoticeNo");
        tGetNoticeNoData.addContent("");
        tBodyData.addContent(tGetNoticeNoData);
        

        tRootData.addContent(tHeadData);
        tRootData.addContent(tBodyData);
        Document tDocument = new Document(tRootData);
        
        return tDocument;
    }

    /**
     * 生成正常返回报文
     * @return Document
     */
    private Document createResultXML() throws Exception {
 
         //Request部分
         Element tRootData = new Element("Request");
         
         
         //Head部分
         Element tHeadData = new Element("head");
         
         
         Element tTransactionNum = new Element("Transaction_NUM");
         tTransactionNum.addContent(mTransacTionNo);
         tHeadData.addContent(tTransactionNum);
         //body部分
         Element tBodyData = new Element("body");
         
         List mList = new ArrayList();
         mList = mLLSocialAutoCase.gettList();
         
         System.out.println("开始返回报文生成--list的长度"+mList.size());
         if(mList!=null && mList.size()>0){
             
         for(int i=0;i< mList.size();i++){
        	 
         String tresult=mList.get(i).toString();
         Element tPayStatus = new Element("PayStatus");
         tPayStatus.addContent("1");
         tBodyData.addContent(tPayStatus);
         Element tPayEason = new Element("PayEason");
         tPayEason.addContent("");
         tBodyData.addContent(tPayEason);
         Element tGetNoticeNoData = new Element("GetNoticeNo");
         tGetNoticeNoData.addContent(tresult);
         tBodyData.addContent(tGetNoticeNoData);
             }
         }

         tRootData.addContent(tHeadData);
         tRootData.addContent(tBodyData);
    	Document tDocument = new Document(tRootData);
    	return tDocument;
    }
    
    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLSocialAutoXML";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    /**
     * 解析校验社保批次部分
     * @return boolean
     */
//  社保批次信息	LLRgtSocial		
    private boolean RgtSocial() {
    	System.out.println("RgtSocial--start");
    	List tRGTList = new ArrayList();
    	tRGTList = (List) mRGTList.getChildren();
    	for (int i = 0; i < tRGTList.size(); i++) {
            Element tRGTdata = (Element) tRGTList.get(i);
            
            mGrpContNo = tRGTdata.getChildText("GRPCONTNO");
            if (mGrpContNo == null || "".equals(mGrpContNo)) {
                buildError("RgtSocial", "【保单号】的值不能为空");
                return false;
            }
            
            String mAppPeoples = tRGTdata.getChildText("APPPEOPLES");
            if (mAppPeoples == null || "".equals(mAppPeoples)) {
                buildError("RgtSocial", "【申请人数】的值不能为空");
                return false;
            }
            
            String mTogetherFlag = tRGTdata.getChildText("TOGETHERFLAG");
            if (mTogetherFlag == null || "".equals(mTogetherFlag)) {
                buildError("RgtSocial", "【给付方式】的值不能为空");
                return false;
            }
            
            String mCaseGetMode = tRGTdata.getChildText("CASEGETMODE");
            if (mCaseGetMode == null || "".equals(mCaseGetMode)) {
                buildError("RgtSocial", "【保险金领取方式】的值不能为空");
                return false;
            }
            
            String mAppAmnt = tRGTdata.getChildText("APPAMNT");
            if (mAppAmnt == null || "".equals(mAppAmnt)) {
                buildError("RgtSocial", "【申请金额】的值不能为空");
                return false;
            }
            String mBankCode = tRGTdata.getChildText("BANKCODE");
            
            String mAccName = tRGTdata.getChildText("ACCNAME");
            
            String mBankAccNo = tRGTdata.getChildText("BANKACCNO");
            
            String tGetMngComSQL="select managecom from lcgrpcont where grpcontno= '" +mGrpContNo +
								"' union select managecom from lBgrpcont where grpcontno= '" + mGrpContNo + "' with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tGetMngCom = tExeSQL.execSQL(tGetMngComSQL);
			if (tGetMngCom.getMaxRow() != 1) {
				buildError("RgtSocial", "【管理机构】获取不正确");
				return false;
			}
			mMngCom = tGetMngCom.GetText(1,1);
			if (mMngCom == null || "".equals(mMngCom)) {
			    buildError("RgtSocial", "【管理机构】的值不能为空");
			    return false;
			}
            

            mSocialRgtNo = PubFun1.CreateMaxNo("SocialRgtNo",mMngCom);
            mLLRgtSocialSchema.setSocialRgtNo(mSocialRgtNo);
            mLLRgtSocialSchema.setTransacTionNo(mTransacTionNo);
            mLLRgtSocialSchema.setGrpContNo(mGrpContNo);
            mLLRgtSocialSchema.setAppPeoples(mAppPeoples);
            mLLRgtSocialSchema.setTogetherFlag(mTogetherFlag);
            mLLRgtSocialSchema.setCaseGetMode(mCaseGetMode);
            mLLRgtSocialSchema.setAppAmnt(mAppAmnt);
            mLLRgtSocialSchema.setBankCode(mBankCode);
            mLLRgtSocialSchema.setBankAccNo(mBankAccNo);
            mLLRgtSocialSchema.setAccName(mAccName);
            mLLRgtSocialSchema.setMngCom(mMngCom);
            mLLRgtSocialSchema.setOperator(mOperator);
            mLLRgtSocialSchema.setMakeDate(mMakeDate);
            mLLRgtSocialSchema.setMakeTime(mMakeTime);
            mLLRgtSocialSchema.setModifyDate(mMakeDate);
            mLLRgtSocialSchema.setModifyTime(mMakeTime);
            System.out.println("账户名称="+mLLRgtSocialSchema.getAccName());
            LLRgtSocialSchema tLLRgtSocialSchema = new LLRgtSocialSchema();
            tLLRgtSocialSchema.setSchema(mLLRgtSocialSchema);
            mLLRgtSocialSet.add(tLLRgtSocialSchema);

            
    	}
    	return true;
    }
    
    //社保案件信息相关	LLCaseSocial
    private boolean CaseSocial() {
    	System.out.println("CaseSocial--start");
    	List tLLCaseList = new ArrayList();
    	tLLCaseList = (List) mLLCaseList.getChildren();
    	for (int i = 0; i < tLLCaseList.size(); i++) {
            Element tLLCasedata = (Element) tLLCaseList.get(i);
            
            
            String tCustomerNo = tLLCasedata.getChildText("CUSTOMERNO");
            if (tCustomerNo == null || "".equals(tCustomerNo)) {
                buildError("CaseSocial", "【出险人客户号】的值不能为空");
                return false;
            }
            System.out.println(tCustomerNo);
            String tCustomerName = tLLCasedata.getChildText("CUSTOMERNAME");
            if (tCustomerName == null || "".equals(tCustomerName)) {
            	buildError("CaseSocial", "【出险人名称】的值不能为空");
            	return false;
            }
            //证件类型固定为0-身份证
            String tIDType ="0";
            
            String tIDNo = tLLCasedata.getChildText("IDNO");
            if (tIDNo == null || "".equals(tIDNo)) {
            	buildError("CaseSocial", "【证件号码】的值不能为空");
            	return false;
            }
            
            String tSex = tLLCasedata.getChildText("SEX");
            if (tSex == null || "".equals(tSex)) {
            	buildError("CaseSocial", "【出险人性别】的值不能为空");
            	return false;
            }
            
            String tCustBirthday = tLLCasedata.getChildText("CUSTBIRTHDAY");
            if (tCustBirthday == null || "".equals(tCustBirthday)) {
            	buildError("CaseSocial", "【出险人生日】的值不能为空");
            	return false;
            }
            
            String tReasonCode = tLLCasedata.getChildText("REASONCODE");
            
            
            String tAccDate = tLLCasedata.getChildText("ACCDATE");
            if (tAccDate == null || "".equals(tAccDate)) {
            	buildError("CaseSocial", "【发生日期】的值不能为空");
            	return false;
            }
            
            String tAccPlace = tLLCasedata.getChildText("ACCPLACE");
            
            
            String tInHospitalDate = tLLCasedata.getChildText("HOSPSTARTDATE");
            if (tInHospitalDate == null || "".equals(tInHospitalDate)) {
            	buildError("CaseSocial", "【入院日期】的值不能为空");
            	return false;
            }
            
            String tOutHospitalDate = tLLCasedata.getChildText("HOSPENDDATE");
            if (tOutHospitalDate == null || "".equals(tOutHospitalDate)) {
            	buildError("CaseSocial", "【出院日期】的值不能为空");
            	return false;
            }
            
            //事件信息
            String tAccDesc = tLLCasedata.getChildText("ACCDESC");
            
            //事件类型
            String tAccidentType = tLLCasedata.getChildText("ACCIDENTTYPE");
            
            //受益金领取方式
            String tCaseGetMode = tLLCasedata.getChildText("CASEGETMODE");
            
            //银行编码
            String tBankCode = tLLCasedata.getChildText("BANKCODE");
            
            //银行帐户名
            String tAccName = tLLCasedata.getChildText("ACCNAME");
            
            //银行帐号
            String tBankAccNo = tLLCasedata.getChildText("BANKACCNO");
            
            //被保人手机号
            String tMobilePhone = tLLCasedata.getChildText("MOBILEPHONE");
            
            mSocialCaseNo = PubFun1.CreateMaxNo("SocialCaseNo",mMngCom);
            mLLCaseSocialSchema.setSocialCaseNo(mSocialCaseNo);
            mLLCaseSocialSchema.setSocialRgtNo(mSocialRgtNo);
            mLLCaseSocialSchema.setTransacTionNo(mTransacTionNo);
            mLLCaseSocialSchema.setCustomerNo(tCustomerNo);
            mLLCaseSocialSchema.setCustomerName(tCustomerName);
            mLLCaseSocialSchema.setIDType(tIDType);
            mLLCaseSocialSchema.setIDNo(tIDNo);
            mLLCaseSocialSchema.setSex(tSex);
            mLLCaseSocialSchema.setCustBirthday(tCustBirthday);
            mLLCaseSocialSchema.setReasonCode(tReasonCode);
            mLLCaseSocialSchema.setAccDate(tAccDate);
            mLLCaseSocialSchema.setOutHospitalDate(tOutHospitalDate);
            mLLCaseSocialSchema.setInHospitalDate(tInHospitalDate);
            mLLCaseSocialSchema.setAccPlace(tAccPlace);
            mLLCaseSocialSchema.setAccDesc(tAccDesc);
            mLLCaseSocialSchema.setAccidentType(tAccidentType);
            mLLCaseSocialSchema.setAccName(tAccName);
            mLLCaseSocialSchema.setBankCode(tBankCode);
            mLLCaseSocialSchema.setBankAccNo(tBankAccNo);
            mLLCaseSocialSchema.setCaseGetMode(tCaseGetMode);
            mLLCaseSocialSchema.setMobilePhone(tMobilePhone);
            mLLCaseSocialSchema.setMngCom(mMngCom);
            mLLCaseSocialSchema.setOperator(mOperator);
            mLLCaseSocialSchema.setMakeDate(mMakeDate);
            mLLCaseSocialSchema.setMakeTime(mMakeTime);
            mLLCaseSocialSchema.setModifyDate(mMakeDate);
            mLLCaseSocialSchema.setModifyTime(mMakeTime);
            
            LLCaseSocialSchema tLLCaseSocialSchema = new LLCaseSocialSchema();
            tLLCaseSocialSchema.setSchema(mLLCaseSocialSchema);
            mLLCaseSocialSet.add(tLLCaseSocialSchema);

            mLLFeeMainList = tLLCasedata.getChild("LLFEEMAINLIST");
            if (!FeeMain()) {
                return false;
            }
            
            //出险内容
            
            mDiseaseList = tLLCasedata.getChild("DISEASELIST");
            mSeriousList= tLLCasedata.getChild("SERIOUSLIST");
            mOperationList = tLLCasedata.getChild("OPERATIONLIST");
            
            if (!AccDetailSocial()) {
                return false;
            }

            //理赔明细
            mPayList = tLLCasedata.getChild("PAYLIST");
            if (!ClaimDetailSocial()) {
                return false;
            }

            
            
            
    	}
    	return true;
    }
   
    private boolean FeeMain() {
    	System.out.println("FeeMain--start");
    	List tLLFeeMainList = new ArrayList();
    	tLLFeeMainList = (List)mLLFeeMainList.getChildren();
    	for (int i = 0; i < tLLFeeMainList.size(); i++) {
            Element tLLFeeMainData = (Element) tLLFeeMainList.get(i);
            
//          医院名称	HospitalName
            String HospitalName = tLLFeeMainData.getChildText("HOSPITALNAME");
            if (HospitalName == null || "".equals(HospitalName)) {
            	buildError("FeeMain", "【医院名称】的值不能为空");
            	return false;
            }
            
//          医院代码	HospitalCode
            String tHospitalCode = tLLFeeMainData.getChildText("HOSPITALNAMECODE");
            if (tHospitalCode == null || "".equals(tHospitalCode)) {
            	buildError("FeeMain", "【医院代码】的值不能为空");
            	return false;
            }
            


//          实际住院天数	RealHospDate
            String RealHospDate = tLLFeeMainData.getChildText("REALHOSPDATE");
            if (RealHospDate == null || "".equals(RealHospDate)) {
            	buildError("FeeMain", "【实际住院天数】的值不能为空");
            	return false;
            }
         
//          账单号码	ReceiptNo
            String ReceiptNo = tLLFeeMainData.getChildText("RECEIPTNO");
            
//          账单日期	FeeDate
            String FeeDate = tLLFeeMainData.getChildText("FEEDATE");
            if (FeeDate == null || "".equals(FeeDate)) {
            	buildError("FeeMain", "【账单日期】的值不能为空");
            	return false;
            }
//          住院号	InHosNo
            String InHosNo = tLLFeeMainData.getChildText("INHOSNO");
            

            mLLPrescripSocialSchema.setSocialCaseNo(mSocialCaseNo);
            mLLPrescripSocialSchema.setSocialRgtNo(mSocialRgtNo);
            mLLPrescripSocialSchema.setTransacTionNo(mTransacTionNo);
            mLLPrescripSocialSchema.setHospitalCode(tHospitalCode);
            mLLPrescripSocialSchema.setHospitalName(HospitalName);
            mLLPrescripSocialSchema.setRealHospDate(RealHospDate);
            mLLPrescripSocialSchema.setReceiptNo(ReceiptNo);
            mLLPrescripSocialSchema.setFeeDate(FeeDate);
            mLLPrescripSocialSchema.setInHosNo(InHosNo);
            
            mPrescriptionList = tLLFeeMainData.getChild("PRESCRIPTIONLIST");
            if (!PrescriptionList()) {
                return false;
            }
            mFeeItemList = tLLFeeMainData.getChild("FEEITEMLIST");
            if (!FeeItemList()) {
                return false;
            }
            

    	}
        return true;
    }
    
//  社保账单费用信息	LLPerscriptionSocial
    private boolean PrescriptionList() {
    	System.out.println("PrescriptionList--start");
    	List tPrescriptionList = new ArrayList();
    	tPrescriptionList = (List) mPrescriptionList.getChildren();
    	for (int i = 0; i < tPrescriptionList.size(); i++) {
            Element tPrescriptionData = (Element) tPrescriptionList.get(i);
            
//          合计金额	ApplyAmnt
            String ApplyAmnt = tPrescriptionData.getChildText("APPLYAMNT");
            
//          统筹基金支付	PlanFee
            String PlanFee = tPrescriptionData.getChildText("PLANFEE");
            
//          大额医疗支付	SupInHosFee
            String SupInHosFee = tPrescriptionData.getChildText("SUPINHOSFEE");
            
//          公务员医疗补助	OfficialSubsidy
            String OfficialSubsidy = tPrescriptionData.getChildText("OFFICIALSUBSIDY");
           
//          自费	SelfAmnt
            String SelfAmnt = tPrescriptionData.getChildText("SELFAMNT");
            
//          自付二	SelfPay2
            String SelfPay2 = tPrescriptionData.getChildText("SELFPAY2");
            
//          自付一	SelfPay1
            String SelfPay1 = tPrescriptionData.getChildText("SELFPAY1");
            
//          起付线	GetLimit
            String GetLimit = tPrescriptionData.getChildText("GETLIMIT");
            
//          中段	MidAmnt
            String MidAmnt = tPrescriptionData.getChildText("MIDAMNT");
            
//          高段1	HighAmnt1
            String HighAmnt1 = tPrescriptionData.getChildText("HIGHAMNT1");
            
//          高段2	HighAmnt2
            String HighAmnt2 = tPrescriptionData.getChildText("HIGHAMNT2");
            
//          超高段	SuperAmnt
            String SuperAmnt = tPrescriptionData.getChildText("SUPERAMNT");
            
//          退休补充医疗	RetireAddFee
            String RetireAddFee = tPrescriptionData.getChildText("RETIREADDFEE");
            
//          小额门急诊	SmallDoorPay
            String SmallDoorPay = tPrescriptionData.getChildText("SMALLDOORPAY");
            
//          大额门急诊	HighDoorAmnt
            String HighDoorAmnt = tPrescriptionData.getChildText("HIGHDOORAMNT");
            
//          可报销范围内金额	Reimbursement
            String Reimbursement = tPrescriptionData.getChildText("REIMBURSEMENT");
            
            
            
            
            mSocialFeeNo = PubFun1.CreateMaxNo("SocialFeeNo",mMngCom);
            mLLPrescripSocialSchema.setSocialFeeNo(mSocialFeeNo);

            mLLPrescripSocialSchema.setApplyAmnt(ApplyAmnt);
            mLLPrescripSocialSchema.setPlanFee(PlanFee);
            mLLPrescripSocialSchema.setSupInHosFee(SupInHosFee);
            mLLPrescripSocialSchema.setOfficialSubsidy(OfficialSubsidy);
            mLLPrescripSocialSchema.setSelfAmnt(SelfAmnt);
            mLLPrescripSocialSchema.setSelfPay2(SelfPay2);
            mLLPrescripSocialSchema.setSelfPay1(SelfPay1);
            mLLPrescripSocialSchema.setGetLimit(GetLimit);
            mLLPrescripSocialSchema.setMidAmnt(MidAmnt);
            mLLPrescripSocialSchema.setHighAmnt1(HighAmnt1);
            mLLPrescripSocialSchema.setHighAmnt2(HighAmnt2);
            mLLPrescripSocialSchema.setSuperAmnt(SuperAmnt);
            mLLPrescripSocialSchema.setRetireAddFee(RetireAddFee);
            mLLPrescripSocialSchema.setSmallDoorPay(SmallDoorPay);
            mLLPrescripSocialSchema.setHighDoorAmnt(HighDoorAmnt);
            mLLPrescripSocialSchema.setReimbursement(Reimbursement);
            mLLPrescripSocialSchema.setMngCom(mMngCom);
            mLLPrescripSocialSchema.setOperator(mOperator);
            mLLPrescripSocialSchema.setMakeDate(mMakeDate);
            mLLPrescripSocialSchema.setMakeTime(mMakeTime);
            mLLPrescripSocialSchema.setModifyDate(mMakeDate);
            mLLPrescripSocialSchema.setModifyTime(mMakeTime);
            LLPrescripSocialSchema  tLLPrescripSocialSchema =new LLPrescripSocialSchema(); 
            tLLPrescripSocialSchema.setSchema(mLLPrescripSocialSchema);
            mLLPrescripSocialSet.add(tLLPrescripSocialSchema);


    	}
        return true;
    }

  
    //社保费用明细相关 	LLFeeItemSocial
    private boolean FeeItemList() {
    	System.out.println("FeeItemList--start");
    	List tFeeItemList = new ArrayList();
    	tFeeItemList = (List) mFeeItemList.getChildren();
    	for (int i = 0; i < tFeeItemList.size(); i++) {
            Element tFeeItemData = (Element) tFeeItemList.get(i);
    	
           
            //费用项目代码 FeeItemCode	
            String FeeItemCode = tFeeItemData.getChildText("FEEITEMCODE");
            
            //费用金额	Fee	
            String Fee = tFeeItemData.getChildText("FEE");
            
            String tSocialFeeDetailNo = PubFun1.CreateMaxNo("SocialFeeDetail",mMngCom);
            mLLFeeItemSocialSchema.setSocialFeeDetailNo(tSocialFeeDetailNo);
            mLLFeeItemSocialSchema.setSocialFeeNo(mSocialFeeNo);
            mLLFeeItemSocialSchema.setSocialCaseNo(mSocialCaseNo);
            mLLFeeItemSocialSchema.setSocialRgtNo(mSocialRgtNo);
            mLLFeeItemSocialSchema.setTransacTionNo(mTransacTionNo);
            mLLFeeItemSocialSchema.setFeeItemCode(FeeItemCode);
            mLLFeeItemSocialSchema.setFee(Fee);
            mLLFeeItemSocialSchema.setMngCom(mMngCom);
            mLLFeeItemSocialSchema.setOperator(mOperator);
            mLLFeeItemSocialSchema.setMakeDate(mMakeDate);
            mLLFeeItemSocialSchema.setMakeTime(mMakeTime);
            mLLFeeItemSocialSchema.setModifyDate(mMakeDate);
            mLLFeeItemSocialSchema.setModifyTime(mMakeTime);
            LLFeeItemSocialSchema  tLLFeeItemSocialSchema = new LLFeeItemSocialSchema();
            tLLFeeItemSocialSchema.setSchema(mLLFeeItemSocialSchema);
            mLLFeeItemSocialSet.add(tLLFeeItemSocialSchema);
    	}
    	
        return true;
    }
    
//  疾病信息类型AccType为01
//  重疾信息类型AccType为02 
//  手术信息类型AccType为03
    //社保出险内容信息相关	LLAccDetailSocial
    private boolean AccDetailSocial() {
    	System.out.println("AccDetailSocial--start");

    	
    	List tDiseaseList = new ArrayList();
    	tDiseaseList = (List) mDiseaseList.getChildren();
    	for (int i = 0; i < tDiseaseList.size(); i++) {
            Element tDiseaseData = (Element) tDiseaseList.get(i);
            
            String DiseaseCode = tDiseaseData.getChildText("DISEASECODE");
            if (DiseaseCode == null || "".equals(DiseaseCode)) {
            	buildError("FeeMain", "【疾病代码】的值不能为空");
            	return false;
            }
            String DiseaseName = tDiseaseData.getChildText("DISEASENAME");
            if (DiseaseName == null || "".equals(DiseaseName)) {
            	buildError("FeeMain", "【疾病名称】的值不能为空");
            	return false;
            }
//	        	出险信息流水号	SocialAccNo
        	String tSocialAccNo = PubFun1.CreateMaxNo("SocialAccNo",mMngCom);
        	mLLAccDetailSocialSchema.setSocialAccNo(tSocialAccNo);
        	mLLAccDetailSocialSchema.setSocialCaseNo(mSocialCaseNo);
        	mLLAccDetailSocialSchema.setSocialRgtNo(mSocialRgtNo);
        	mLLAccDetailSocialSchema.setTransacTionNo(mTransacTionNo);
        	//疾病的信息类型为01
        	mLLAccDetailSocialSchema.setAccType("01");
        	mLLAccDetailSocialSchema.setAccCode(DiseaseCode);
        	mLLAccDetailSocialSchema.setAccName(DiseaseName);
        	mLLAccDetailSocialSchema.setMngCom(mMngCom);
        	mLLAccDetailSocialSchema.setOperator(mOperator);
        	mLLAccDetailSocialSchema.setMakeDate(mMakeDate);
        	mLLAccDetailSocialSchema.setMakeTime(mMakeTime);
        	mLLAccDetailSocialSchema.setModifyDate(mMakeDate);
        	mLLAccDetailSocialSchema.setModifyTime(mMakeTime);
        	LLAccDetailSocialSchema  tLLAccDetailSocialSchema = new LLAccDetailSocialSchema();
        	tLLAccDetailSocialSchema.setSchema(mLLAccDetailSocialSchema);     	
        	mLLAccDetailSocialSet.add(tLLAccDetailSocialSchema);

    	}
    	
    	
    	List tSeriousList = new ArrayList();
    	tSeriousList = (List) mSeriousList.getChildren();
    	for (int i = 0; i < tSeriousList.size(); i++) {
            Element tSeriousData = (Element) tSeriousList.get(i);
            
            String SeriousCode = tSeriousData.getChildText("SERIOUSCODE");
            String SeriousName = tSeriousData.getChildText("SERIOUSNAME");
            if(!(SeriousCode == null || "".equals(SeriousCode))||!(SeriousName == null || "".equals(SeriousName))){
//	        	出险信息流水号	SocialAccNo
            	String tSocialAccNo = PubFun1.CreateMaxNo("SocialAccNo",mMngCom);
            	mLLAccDetailSocialSchema.setSocialAccNo(tSocialAccNo);
            	mLLAccDetailSocialSchema.setSocialCaseNo(mSocialCaseNo);
            	mLLAccDetailSocialSchema.setSocialRgtNo(mSocialRgtNo);
            	mLLAccDetailSocialSchema.setTransacTionNo(mTransacTionNo);
            	//重疾信息类型为02
            	mLLAccDetailSocialSchema.setAccType("02");
            	mLLAccDetailSocialSchema.setAccCode(SeriousCode);
            	mLLAccDetailSocialSchema.setAccName(SeriousName);
            	mLLAccDetailSocialSchema.setMngCom(mMngCom);
            	mLLAccDetailSocialSchema.setOperator(mOperator);
            	mLLAccDetailSocialSchema.setMakeDate(mMakeDate);
            	mLLAccDetailSocialSchema.setMakeTime(mMakeTime);
            	mLLAccDetailSocialSchema.setModifyDate(mMakeDate);
            	mLLAccDetailSocialSchema.setModifyTime(mMakeTime);
            	LLAccDetailSocialSchema  tLLAccDetailSocialSchema = new LLAccDetailSocialSchema();
            	tLLAccDetailSocialSchema.setSchema(mLLAccDetailSocialSchema);
            	mLLAccDetailSocialSet.add(tLLAccDetailSocialSchema);
            }
            
    	}
    	
    	
    	
    	List tOperationList = new ArrayList();
    	tOperationList = (List) mOperationList.getChildren();
    	for (int i = 0; i < tOperationList.size(); i++) {
            Element tOperationData = (Element) tOperationList.get(i);
            
            String OperationCode = tOperationData.getChildText("OPERATIONCODE");
            String OperationName = tOperationData.getChildText("OPERATIONNAME");
            
            if(!(OperationCode == null || "".equals(OperationCode))||!(OperationCode == null || "".equals(OperationCode))){
//	        	出险信息流水号	SocialAccNo
            	String tSocialAccNo = PubFun1.CreateMaxNo("SocialAccNo",mMngCom);
            	mLLAccDetailSocialSchema.setSocialAccNo(tSocialAccNo);
            	mLLAccDetailSocialSchema.setSocialCaseNo(mSocialCaseNo);
            	mLLAccDetailSocialSchema.setSocialRgtNo(mSocialRgtNo);
            	mLLAccDetailSocialSchema.setTransacTionNo(mTransacTionNo);
            	//手术信息类型为03
            	mLLAccDetailSocialSchema.setAccType("03");
            	mLLAccDetailSocialSchema.setAccCode(OperationCode);
            	mLLAccDetailSocialSchema.setAccName(OperationName);
            	mLLAccDetailSocialSchema.setMngCom(mMngCom);
            	mLLAccDetailSocialSchema.setOperator(mOperator);
            	mLLAccDetailSocialSchema.setMakeDate(mMakeDate);
            	mLLAccDetailSocialSchema.setMakeTime(mMakeTime);
            	mLLAccDetailSocialSchema.setModifyDate(mMakeDate);
            	mLLAccDetailSocialSchema.setModifyTime(mMakeTime);
            	LLAccDetailSocialSchema  tLLAccDetailSocialSchema = new LLAccDetailSocialSchema();
            	tLLAccDetailSocialSchema.setSchema(mLLAccDetailSocialSchema);
            	mLLAccDetailSocialSet.add(tLLAccDetailSocialSchema);


            }
    	}

    	
        return true;
    }
    
    //社保理赔明细相关	LLClaimDetailSocial
    private boolean ClaimDetailSocial() {
    	List tPayList = new ArrayList();
    	tPayList = (List) mPayList.getChildren();
    	for (int i = 0; i < tPayList.size(); i++) {
            Element tPayData = (Element) tPayList.get(i);
    	
            
//            险种代码
            String RiskCode = tPayData.getChildText("RISKCODE");
            if (RiskCode == null || "".equals(RiskCode)) {
            	buildError("FeeMain", "【险种代码】的值不能为空");
            	return false;
            }
//            给付责任类型
            String GetDutyKind = tPayData.getChildText("GETDUTYKIND");
            if (GetDutyKind == null || "".equals(GetDutyKind)) {
            	buildError("FeeMain", "【给付责任类型】的值不能为空");
            	return false;
            }
//            责任编码
            String DutyCode = tPayData.getChildText("DUTYCODE");
            if (DutyCode == null || "".equals(DutyCode)) {
            	buildError("FeeMain", "【责任编码】的值不能为空");
            	return false;
            }
//            给付责任编码
            String GetDutyCode = tPayData.getChildText("GETDUTYCODE");
            if (GetDutyCode == null || "".equals(GetDutyCode)) {
            	buildError("FeeMain", "【给付责任编码】的值不能为空");
            	return false;
            }
//            实赔金额
            String RealPay  = tPayData.getChildText("REALPAY");
            if (RealPay == null || "".equals(RealPay)) {
            	buildError("FeeMain", "【实赔金额】的值不能为空");
            	return false;
            }
            
            //理赔明细流水号
            String tSocialDetailNo = PubFun1.CreateMaxNo("SocialDetailNo",mMngCom);
        	mLLClaimDetailSocialSchema.setSocialDetailNo(tSocialDetailNo);
        	mLLClaimDetailSocialSchema.setSocialCaseNo(mSocialCaseNo);
        	mLLClaimDetailSocialSchema.setSocialRgtNo(mSocialRgtNo);
        	mLLClaimDetailSocialSchema.setTransacTionNo(mTransacTionNo);
        	mLLClaimDetailSocialSchema.setRiskCode(RiskCode);
        	mLLClaimDetailSocialSchema.setGetDutyKind(GetDutyKind);
        	mLLClaimDetailSocialSchema.setDutyCode(DutyCode);
        	mLLClaimDetailSocialSchema.setGetDutyCode(GetDutyCode);
        	mLLClaimDetailSocialSchema.setRealPay(RealPay);
        	mLLClaimDetailSocialSchema.setMngCom(mMngCom);
        	mLLClaimDetailSocialSchema.setOperator(mOperator);
        	mLLClaimDetailSocialSchema.setMakeDate(mMakeDate);
        	mLLClaimDetailSocialSchema.setMakeTime(mMakeTime);
        	mLLClaimDetailSocialSchema.setModifyDate(mMakeDate);
        	mLLClaimDetailSocialSchema.setModifyTime(mMakeTime);
        	LLClaimDetailSocialSchema tLLClaimDetailSocialSchema = new LLClaimDetailSocialSchema();
        	tLLClaimDetailSocialSchema.setSchema(mLLClaimDetailSocialSchema);
        	mLLClaimDetailSocialSet.add(tLLClaimDetailSocialSchema);

            
    	}
        return true;
    }
    


    public static void main(String[] args) {
        Document tInXmlDoc;
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream(
                    "E:/ceshi.xml"), "GBK");

            LLSocialAutoXML tBusinessDeal = new LLSocialAutoXML();
            //tBusinessDeal.service(tInXmlDoc);
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            //JdomUtil.print(tInXmlDoc.getRootElement());
            JdomUtil.print(tOutXmlDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    }
}

