package com.sinosoft.lis.yibaotong;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.schema.LLRegisterSchema;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLHospCaseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LLPadStateQuery implements ServiceInterface{
    public LLPadStateQuery(){}
    
    public CErrors mErrors = new CErrors();
    private String mErrorMessage = "";  //错误描述    
    private List tList = new ArrayList();//返回信息合集
    private Document mInXmlDoc;             //传入报文    
    private String mDocType = "";           //报文类型    
    private String mRequestType = "";       //请求类型    
    private String mTransactionNum = "";    //交互编码
    private String mManageCom;
    private boolean mDealState=true;
    private Element mCasedate;
    private String mCaseno;
    private String mrgtstate;
    private Element rgtState;

	public Document service(Document pInXmlDoc) {

        
        try {
            //获取报文
            if (!getInputData(pInXmlDoc)) {
                mDealState = false;
            } 
            
            //校验报文
            if (!checkData()) {
                    mDealState = false;
                } else {
                    if (!dealData()) {
                        mDealState = false;
                    }else{

                    }
                   
                }
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            mDealState = false;            
            buildError("service()", "系统未知错误");
        } finally {
            return createXML();
        }
    
	}

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */

	private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LLPadStateQuery";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);

		
	}


	private boolean getInputData(Document cInXml) {
	       System.out.println("LLPadStateQuery--getInputData");
	        if (cInXml == null) {
	            buildError("getInputData()", "未获得报文");
	            return false;
	        }
	        Element tRootData = cInXml.getRootElement();
	        mDocType = tRootData.getAttributeValue("type");

	        //获取HEAD部分内容
	        Element tHeadData = tRootData.getChild("HEAD");
	        mRequestType = tHeadData.getChildTextTrim("REQUEST_TYPE");
	        mTransactionNum = tHeadData.getChildTextTrim("TRANSACTION_NUM");
	    
	        
	        //获取BODY部分内容
	        Element tBodyData = tRootData.getChild("BODY");
	        mCasedate = tBodyData.getChild("CASE_DATA");
	        mCaseno = mCasedate.getChildTextTrim("CASENO");          //理赔信息    
			return true;

	}
	
    /**
     * 校验报文信息
     *
     */
    private boolean checkData(){
        System.out.println("mCaseno==="+mCaseno);
    	if (!"YD05".equals(mRequestType)) {
            buildError("checkData()", "【请求类型】的值不存在或匹配错误!");
            return false;
        }
    	if (mCaseno == null  || mCaseno.equals("null")) {
    		buildError("checkData()", "传入的案件号为空！");
            return false;
    	}
        if (mTransactionNum == null || mTransactionNum == "" || mTransactionNum.equals("null")||
                mTransactionNum.length() != 30) {
                buildError("checkData()", "【交互编码】的编码个数错误!");
                return false;
            }
        if (!"YD05".equals(mTransactionNum.substring(0, 4))) {
            buildError("checkData()", "【交互编码】的请求类型编码错误!");
            return false;
        }
        mManageCom = mTransactionNum.substring(4, 12);        
        if ("".equals(mManageCom) || mManageCom == null || mManageCom.equals("null")|| !"00000000".equals(mManageCom)) {
            buildError("checkData()", "【交互编码】的固定值错误!");
            return false;
        }

        return true;

    }

	private boolean dealData() {
		if(mCasedate!=null){		
				ExeSQL tExeSQL = new ExeSQL();
				String sql="select a.rgtstate from LLcase a where a.caseno='"+mCaseno+"'";
				SSRS tSSRS=tExeSQL.execSQL(sql);
	            if(tSSRS!=null){
	    	    	if(tSSRS.getMaxRow()>0){
	    	    		mrgtstate=tSSRS.GetText(1, 1);
	    	    	}
	    	    	else{
	    	    		 buildError("dealData()", "未查询到"+mCaseno+"的相关数据！");
			             mDealState = false; 
	    	    	}
	            }
		}
		return true;
	}
	
    /**
     * 生成返回的报文信息
     * @return Document
     */
	private Document createXML() {
	      //处理失败，返回错误信息报文
        if (mDealState == false) {
            return createFalseXML();
        } else {
           try
        {
            return createResultXML();
        }
        catch (Exception e)
        {
            
            e.printStackTrace();
            buildError("createXML()", "生成返回报文错误 !");
            return createFalseXML();
        }
        }
	}
	
    /**
     * 生成正常返回报文
     * @return Document
     */
	private Document createResultXML() {
        System.out.println("LLPadStateQuery--createResultXML(正常返回报文)");
        Element tRootData = new Element("PACKET");
        tRootData.addAttribute("type", "RESPONSE");
        tRootData.addAttribute("version", "1.0");

        //Head部分
        Element tHeadData = new Element("HEAD");

        Element tRequestType = new Element("REQUEST_TYPE");
        tRequestType.setText(mRequestType);
        tHeadData.addContent(tRequestType);


        Element tTransactionNum = new Element("TRANSACTION_NUM");
        tTransactionNum.setText(mTransactionNum);
        tHeadData.addContent(tTransactionNum);
        //Body部分
        Element tBodyData = new Element("BODY");
        //返回的案件处理结构
        System.out.println("开始返回报文生成!-----");
        Element basedata = new Element("BASE_DATA");
        Element tState = new Element("RGTSTATE");
        tState.setText(mrgtstate);
        basedata.addContent(tState);
        tBodyData.addContent(basedata);
        tRootData.addContent(tHeadData);
        tRootData.addContent(tBodyData);
        Document tDocument = new Document(tRootData);
		return tDocument;
	}
 	
	/**
	 * 返回错误报文
	 * @return
	 */
	private Document createFalseXML() {   
		System.out.println("LLPadStateQuery--createResultXML(返回错误报文)");
    Element tRootData = new Element("PACKET");
    tRootData.addAttribute("type", "RESPONSE");
    tRootData.addAttribute("version", "1.0");

    //Head部分
    Element tHeadData = new Element("HEAD");

    Element tRequestType = new Element("REQUEST_TYPE");
    tRequestType.setText(mRequestType);
    tHeadData.addContent(tRequestType);


    Element tTransactionNum = new Element("TRANSACTION_NUM");
    tTransactionNum.setText(mTransactionNum);
    tHeadData.addContent(tTransactionNum);

    tRootData.addContent(tHeadData);

    //Body部分
    Element tBodyData = new Element("BODY");
    //返回的案件处理结构
    System.out.println("开始返回错误报文生成!-----");
            Element response = new Element("RESPONSE_CODE");
            mErrorMessage = mErrors.getFirstError();
            response.setText("0");
            tBodyData.addContent(response);
            Element tErrorMessage = new Element("ERROR_MESSAGE");
            tErrorMessage.setText(mErrorMessage);
            tBodyData.addContent(tErrorMessage);
            tRootData.addContent(tBodyData);
            Document tDocument = new Document(tRootData);
            return tDocument;
            
}

    public static void main(String[] args) {
        Document tInXmlDoc;    
        try {
            tInXmlDoc = JdomUtil.build(new FileInputStream("E:/PADTEST/SBXM-005.xml"), "GBK");
            System.out.println(tInXmlDoc+"===");
            LLPadStateQuery tBusinessDeal = new LLPadStateQuery();
            Document tOutXmlDoc = tBusinessDeal.service(tInXmlDoc);
            System.out.println("打印传入报文============");
            JdomUtil.print(tInXmlDoc.getRootElement());
            System.out.println("打印传出报文============");
            JdomUtil.print(tOutXmlDoc);
                        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
