/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;

import com.sinosoft.lis.schema.ES_DOC_QC_MAINSchema;
import com.sinosoft.lis.vschema.ES_DOC_QC_MAINSet;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_QC_MAINDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class ES_DOC_QC_MAINDB extends ES_DOC_QC_MAINSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public ES_DOC_QC_MAINDB(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "ES_DOC_QC_MAIN");
        mflag = true;
    }

    public ES_DOC_QC_MAINDB() {
        con = null;
        db = new DBOper("ES_DOC_QC_MAIN");
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        ES_DOC_QC_MAINSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        ES_DOC_QC_MAINSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement(
                    "DELETE FROM ES_DOC_QC_MAIN WHERE  DocID = ?");
            pstmt.setDouble(1, this.getDocID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE ES_DOC_QC_MAIN SET  DocID = ? , DocCode = ? , BussType = ? , SubType = ? , ScanOperator = ? , ScanManageCom = ? , ScanLastApplyDate = ? , ScanLastApplyTime = ? , QCOperator = ? , QCManageCom = ? , QCLastApplyDate = ? , QCLastApplyTime = ? , QCFlag = ? , QCSuggest = ? , UnpassReason = ? , ScanOpeState = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  DocID = ?");
            pstmt.setDouble(1, this.getDocID());
            if (this.getDocCode() == null || this.getDocCode().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, StrTool.GBKToUnicode(this.getDocCode()));
            }
            if (this.getBussType() == null || this.getBussType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, StrTool.GBKToUnicode(this.getBussType()));
            }
            if (this.getSubType() == null || this.getSubType().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, StrTool.GBKToUnicode(this.getSubType()));
            }
            if (this.getScanOperator() == null ||
                this.getScanOperator().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, StrTool.GBKToUnicode(this.getScanOperator()));
            }
            if (this.getScanManageCom() == null ||
                this.getScanManageCom().equals("null")) {
                pstmt.setNull(6, 1);
            } else {
                pstmt.setString(6, StrTool.GBKToUnicode(this.getScanManageCom()));
            }
            if (this.getScanLastApplyDate() == null ||
                this.getScanLastApplyDate().equals("null")) {
                pstmt.setNull(7, 91);
            } else {
                pstmt.setDate(7, Date.valueOf(this.getScanLastApplyDate()));
            }
            if (this.getScanLastApplyTime() == null ||
                this.getScanLastApplyTime().equals("null")) {
                pstmt.setNull(8, 1);
            } else {
                pstmt.setString(8,
                                StrTool.GBKToUnicode(this.getScanLastApplyTime()));
            }
            if (this.getQCOperator() == null ||
                this.getQCOperator().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, StrTool.GBKToUnicode(this.getQCOperator()));
            }
            if (this.getQCManageCom() == null ||
                this.getQCManageCom().equals("null")) {
                pstmt.setNull(10, 1);
            } else {
                pstmt.setString(10, StrTool.GBKToUnicode(this.getQCManageCom()));
            }
            if (this.getQCLastApplyDate() == null ||
                this.getQCLastApplyDate().equals("null")) {
                pstmt.setNull(11, 91);
            } else {
                pstmt.setDate(11, Date.valueOf(this.getQCLastApplyDate()));
            }
            if (this.getQCLastApplyTime() == null ||
                this.getQCLastApplyTime().equals("null")) {
                pstmt.setNull(12, 1);
            } else {
                pstmt.setString(12,
                                StrTool.GBKToUnicode(this.getQCLastApplyTime()));
            }
            if (this.getQCFlag() == null || this.getQCFlag().equals("null")) {
                pstmt.setNull(13, 1);
            } else {
                pstmt.setString(13, StrTool.GBKToUnicode(this.getQCFlag()));
            }
            if (this.getQCSuggest() == null ||
                this.getQCSuggest().equals("null")) {
                pstmt.setNull(14, 1);
            } else {
                pstmt.setString(14, StrTool.GBKToUnicode(this.getQCSuggest()));
            }
            if (this.getUnpassReason() == null ||
                this.getUnpassReason().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, StrTool.GBKToUnicode(this.getUnpassReason()));
            }
            if (this.getScanOpeState() == null ||
                this.getScanOpeState().equals("null")) {
                pstmt.setNull(16, 1);
            } else {
                pstmt.setString(16, StrTool.GBKToUnicode(this.getScanOpeState()));
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(17, 91);
            } else {
                pstmt.setDate(17, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(18, 1);
            } else {
                pstmt.setString(18, StrTool.GBKToUnicode(this.getMakeTime()));
            }
            if (this.getModifyDate() == null ||
                this.getModifyDate().equals("null")) {
                pstmt.setNull(19, 91);
            } else {
                pstmt.setDate(19, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null ||
                this.getModifyTime().equals("null")) {
                pstmt.setNull(20, 1);
            } else {
                pstmt.setString(20, StrTool.GBKToUnicode(this.getModifyTime()));
            }
            // set where condition
            pstmt.setDouble(21, this.getDocID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO ES_DOC_QC_MAIN VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setDouble(1, this.getDocID());
            if (this.getDocCode() == null || this.getDocCode().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, StrTool.GBKToUnicode(this.getDocCode()));
            }
            if (this.getBussType() == null || this.getBussType().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, StrTool.GBKToUnicode(this.getBussType()));
            }
            if (this.getSubType() == null || this.getSubType().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, StrTool.GBKToUnicode(this.getSubType()));
            }
            if (this.getScanOperator() == null ||
                this.getScanOperator().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, StrTool.GBKToUnicode(this.getScanOperator()));
            }
            if (this.getScanManageCom() == null ||
                this.getScanManageCom().equals("null")) {
                pstmt.setNull(6, 1);
            } else {
                pstmt.setString(6, StrTool.GBKToUnicode(this.getScanManageCom()));
            }
            if (this.getScanLastApplyDate() == null ||
                this.getScanLastApplyDate().equals("null")) {
                pstmt.setNull(7, 91);
            } else {
                pstmt.setDate(7, Date.valueOf(this.getScanLastApplyDate()));
            }
            if (this.getScanLastApplyTime() == null ||
                this.getScanLastApplyTime().equals("null")) {
                pstmt.setNull(8, 1);
            } else {
                pstmt.setString(8,
                                StrTool.GBKToUnicode(this.getScanLastApplyTime()));
            }
            if (this.getQCOperator() == null ||
                this.getQCOperator().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, StrTool.GBKToUnicode(this.getQCOperator()));
            }
            if (this.getQCManageCom() == null ||
                this.getQCManageCom().equals("null")) {
                pstmt.setNull(10, 1);
            } else {
                pstmt.setString(10, StrTool.GBKToUnicode(this.getQCManageCom()));
            }
            if (this.getQCLastApplyDate() == null ||
                this.getQCLastApplyDate().equals("null")) {
                pstmt.setNull(11, 91);
            } else {
                pstmt.setDate(11, Date.valueOf(this.getQCLastApplyDate()));
            }
            if (this.getQCLastApplyTime() == null ||
                this.getQCLastApplyTime().equals("null")) {
                pstmt.setNull(12, 1);
            } else {
                pstmt.setString(12,
                                StrTool.GBKToUnicode(this.getQCLastApplyTime()));
            }
            if (this.getQCFlag() == null || this.getQCFlag().equals("null")) {
                pstmt.setNull(13, 1);
            } else {
                pstmt.setString(13, StrTool.GBKToUnicode(this.getQCFlag()));
            }
            if (this.getQCSuggest() == null ||
                this.getQCSuggest().equals("null")) {
                pstmt.setNull(14, 1);
            } else {
                pstmt.setString(14, StrTool.GBKToUnicode(this.getQCSuggest()));
            }
            if (this.getUnpassReason() == null ||
                this.getUnpassReason().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, StrTool.GBKToUnicode(this.getUnpassReason()));
            }
            if (this.getScanOpeState() == null ||
                this.getScanOpeState().equals("null")) {
                pstmt.setNull(16, 1);
            } else {
                pstmt.setString(16, StrTool.GBKToUnicode(this.getScanOpeState()));
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(17, 91);
            } else {
                pstmt.setDate(17, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(18, 1);
            } else {
                pstmt.setString(18, StrTool.GBKToUnicode(this.getMakeTime()));
            }
            if (this.getModifyDate() == null ||
                this.getModifyDate().equals("null")) {
                pstmt.setNull(19, 91);
            } else {
                pstmt.setDate(19, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null ||
                this.getModifyTime().equals("null")) {
                pstmt.setNull(20, 1);
            } else {
                pstmt.setString(20, StrTool.GBKToUnicode(this.getModifyTime()));
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement(
                    "SELECT * FROM ES_DOC_QC_MAIN WHERE  DocID = ?",
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setDouble(1, this.getDocID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_DOC_QC_MAINDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try {
                        rs.close();
                    } catch (Exception ex) {}
                    try {
                        pstmt.close();
                    } catch (Exception ex1) {}

                    if (!mflag) {
                        try {
                            con.close();
                        } catch (Exception et) {}
                    }
                    return false;
                }
                break;
            }
            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                pstmt.close();
            } catch (Exception ex3) {}

            if (i == 0) {
                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                pstmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public ES_DOC_QC_MAINSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_QC_MAINSet aES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_DOC_QC_MAIN");
            ES_DOC_QC_MAINSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                ES_DOC_QC_MAINSchema s1 = new ES_DOC_QC_MAINSchema();
                s1.setSchema(rs, i);
                aES_DOC_QC_MAINSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_QC_MAINSet;
    }

    public ES_DOC_QC_MAINSet executeQuery(String sql) {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_QC_MAINSet aES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                ES_DOC_QC_MAINSchema s1 = new ES_DOC_QC_MAINSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_DOC_QC_MAINDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aES_DOC_QC_MAINSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_QC_MAINSet;
    }

    public ES_DOC_QC_MAINSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_QC_MAINSet aES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_DOC_QC_MAIN");
            ES_DOC_QC_MAINSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                ES_DOC_QC_MAINSchema s1 = new ES_DOC_QC_MAINSchema();
                s1.setSchema(rs, i);
                aES_DOC_QC_MAINSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_QC_MAINSet;
    }

    public ES_DOC_QC_MAINSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_QC_MAINSet aES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                ES_DOC_QC_MAINSchema s1 = new ES_DOC_QC_MAINSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_DOC_QC_MAINDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aES_DOC_QC_MAINSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_QC_MAINSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_DOC_QC_MAIN");
            ES_DOC_QC_MAINSchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update ES_DOC_QC_MAIN " + sqlObj.getUpdPart() +
                         " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ES_DOC_QC_MAINDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                stmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                             ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "prepareData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            } catch (Exception ex2) {}
            try {
                mStatement.close();
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     * @return ES_DOC_QC_MAINSet
     */
    public ES_DOC_QC_MAINSet getData() {
        int tCount = 0;
        ES_DOC_QC_MAINSet tES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();
        ES_DOC_QC_MAINSchema tES_DOC_QC_MAINSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tES_DOC_QC_MAINSchema = new ES_DOC_QC_MAINSchema();
            tES_DOC_QC_MAINSchema.setSchema(mResultSet, 1);
            tES_DOC_QC_MAINSet.add(tES_DOC_QC_MAINSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tES_DOC_QC_MAINSchema = new ES_DOC_QC_MAINSchema();
                    tES_DOC_QC_MAINSchema.setSchema(mResultSet, 1);
                    tES_DOC_QC_MAINSet.add(tES_DOC_QC_MAINSchema);
                }
            }
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "getData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return null;
        }
        return tES_DOC_QC_MAINSet;
    }

    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "ES_DOC_QC_MAINDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        } catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex2.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "ES_DOC_QC_MAINDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        } catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex3.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
