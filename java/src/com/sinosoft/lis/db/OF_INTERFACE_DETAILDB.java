/**
 * Copyright (c) 2006 Sinosoft Co.,LTD.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.io.StringReader;
import java.sql.*;
import com.sinosoft.lis.schema.OF_INTERFACE_DETAILSchema;
import com.sinosoft.lis.vschema.OF_INTERFACE_DETAILSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: OF_INTERFACE_DETAILDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Company: Sinosoft Co.,LTD </p>
 * @Database: PhysicalDataModel_1
 * @author：Makerx
 * @CreateDate：2007-12-19
 */
public class OF_INTERFACE_DETAILDB extends OF_INTERFACE_DETAILSchema
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;
    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;

    // @Constructor
    public OF_INTERFACE_DETAILDB(Connection cConnection)
    {
        con = cConnection;
        db = new DBOper(con, "OF_INTERFACE_DETAIL");
        mflag = true;
    }

    public OF_INTERFACE_DETAILDB()
    {
        con = null;
        db = new DBOper("OF_INTERFACE_DETAIL");
        mflag = false;
    }

    // @Method
    public boolean deleteSQL()
    {
        OF_INTERFACE_DETAILSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema))
        {
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount()
    {
        OF_INTERFACE_DETAILSchema tSchema = this.getSchema();
        int tCount = db.getCount(tSchema);
        if (tCount < 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return -1;
        }
        return tCount;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert()
    {
        PreparedStatement pstmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            pstmt = con.prepareStatement("INSERT INTO OF_INTERFACE_DETAIL VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setDouble(1, this.getROW_ID());
            if (this.getCURRENCY_CODE() == null || this.getCURRENCY_CODE().equals("null"))
            {
                pstmt.setNull(2, 12);
            }
            else
            {
                pstmt.setString(2, this.getCURRENCY_CODE());
            }
            if (this.getUSER_JE_CATEGORY_NAME() == null || this.getUSER_JE_CATEGORY_NAME().equals("null"))
            {
                pstmt.setNull(3, 12);
            }
            else
            {
                pstmt.setString(3, this.getUSER_JE_CATEGORY_NAME());
            }
            if (this.getSEGMENT1() == null || this.getSEGMENT1().equals("null"))
            {
                pstmt.setNull(4, 12);
            }
            else
            {
                pstmt.setString(4, this.getSEGMENT1());
            }
            if (this.getSEGMENT2() == null || this.getSEGMENT2().equals("null"))
            {
                pstmt.setNull(5, 12);
            }
            else
            {
                pstmt.setString(5, this.getSEGMENT2());
            }
            if (this.getSEGMENT3() == null || this.getSEGMENT3().equals("null"))
            {
                pstmt.setNull(6, 12);
            }
            else
            {
                pstmt.setString(6, this.getSEGMENT3());
            }
            if (this.getSEGMENT4() == null || this.getSEGMENT4().equals("null"))
            {
                pstmt.setNull(7, 12);
            }
            else
            {
                pstmt.setString(7, this.getSEGMENT4());
            }
            if (this.getSEGMENT5() == null || this.getSEGMENT5().equals("null"))
            {
                pstmt.setNull(8, 12);
            }
            else
            {
                pstmt.setString(8, this.getSEGMENT5());
            }
            if (this.getSEGMENT6() == null || this.getSEGMENT6().equals("null"))
            {
                pstmt.setNull(9, 12);
            }
            else
            {
                pstmt.setString(9, this.getSEGMENT6());
            }
            if (this.getSEGMENT7() == null || this.getSEGMENT7().equals("null"))
            {
                pstmt.setNull(10, 12);
            }
            else
            {
                pstmt.setString(10, this.getSEGMENT7());
            }
            if (this.getSEGMENT8() == null || this.getSEGMENT8().equals("null"))
            {
                pstmt.setNull(11, 12);
            }
            else
            {
                pstmt.setString(11, this.getSEGMENT8());
            }
            if (this.getTRANSACTION_DATE() == null || this.getTRANSACTION_DATE().equals("null"))
            {
                pstmt.setNull(12, 91);
            }
            else
            {
                pstmt.setDate(12, Date.valueOf(this.getTRANSACTION_DATE()));
            }
            if (this.getACCOUNTING_DATE() == null || this.getACCOUNTING_DATE().equals("null"))
            {
                pstmt.setNull(13, 91);
            }
            else
            {
                pstmt.setDate(13, Date.valueOf(this.getACCOUNTING_DATE()));
            }
            pstmt.setDouble(14, this.getPOSTING_ID());
            pstmt.setDouble(15, this.getENTERED_DR());
            pstmt.setDouble(16, this.getENTERED_CR());
            if (this.getREFERENCE5() == null || this.getREFERENCE5().equals("null"))
            {
                pstmt.setNull(17, 12);
            }
            else
            {
                pstmt.setString(17, this.getREFERENCE5());
            }
            if (this.getREFERENCE10() == null || this.getREFERENCE10().equals("null"))
            {
                pstmt.setNull(18, 12);
            }
            else
            {
                pstmt.setString(18, this.getREFERENCE10());
            }
            if (this.getCURRENCY_CONVERSION_DATE() == null || this.getCURRENCY_CONVERSION_DATE().equals("null"))
            {
                pstmt.setNull(19, 91);
            }
            else
            {
                pstmt.setDate(19, Date.valueOf(this.getCURRENCY_CONVERSION_DATE()));
            }
            pstmt.setDouble(20, this.getCURRENCY_CONVERSION_RATE());
            pstmt.setDouble(21, this.getACCOUNTED_DR());
            pstmt.setDouble(22, this.getACCOUNTED_CR());
            if (this.getATTRIBUTE1() == null || this.getATTRIBUTE1().equals("null"))
            {
                pstmt.setNull(23, 12);
            }
            else
            {
                pstmt.setString(23, this.getATTRIBUTE1());
            }
            if (this.getATTRIBUTE2() == null || this.getATTRIBUTE2().equals("null"))
            {
                pstmt.setNull(24, 12);
            }
            else
            {
                pstmt.setString(24, this.getATTRIBUTE2());
            }
            if (this.getATTRIBUTE3() == null || this.getATTRIBUTE3().equals("null"))
            {
                pstmt.setNull(25, 12);
            }
            else
            {
                pstmt.setString(25, this.getATTRIBUTE3());
            }
            if (this.getATTRIBUTE4() == null || this.getATTRIBUTE4().equals("null"))
            {
                pstmt.setNull(26, 12);
            }
            else
            {
                pstmt.setString(26, this.getATTRIBUTE4());
            }
            if (this.getATTRIBUTE5() == null || this.getATTRIBUTE5().equals("null"))
            {
                pstmt.setNull(27, 12);
            }
            else
            {
                pstmt.setString(27, this.getATTRIBUTE5());
            }
            if (this.getATTRIBUTE6() == null || this.getATTRIBUTE6().equals("null"))
            {
                pstmt.setNull(28, 12);
            }
            else
            {
                pstmt.setString(28, this.getATTRIBUTE6());
            }
            if (this.getATTRIBUTE7() == null || this.getATTRIBUTE7().equals("null"))
            {
                pstmt.setNull(29, 12);
            }
            else
            {
                pstmt.setString(29, this.getATTRIBUTE7());
            }
            if (this.getATTRIBUTE8() == null || this.getATTRIBUTE8().equals("null"))
            {
                pstmt.setNull(30, 12);
            }
            else
            {
                pstmt.setString(30, this.getATTRIBUTE8());
            }
            if (this.getATTRIBUTE9() == null || this.getATTRIBUTE9().equals("null"))
            {
                pstmt.setNull(31, 12);
            }
            else
            {
                pstmt.setString(31, this.getATTRIBUTE9());
            }
            if (this.getATTRIBUTE10() == null || this.getATTRIBUTE10().equals("null"))
            {
                pstmt.setNull(32, 12);
            }
            else
            {
                pstmt.setString(32, this.getATTRIBUTE10());
            }
            if (this.getATTRIBUTE11() == null || this.getATTRIBUTE11().equals("null"))
            {
                pstmt.setNull(33, 12);
            }
            else
            {
                pstmt.setString(33, this.getATTRIBUTE11());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            // 输出出错Sql语句
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            sqlObj.setSQL(1, this.getSchema());
            System.out.println("出错Sql为：" + sqlObj.getSQL());
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     **/
    public boolean delete()
    {
        PreparedStatement pstmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            pstmt = con.prepareStatement("DELETE FROM OF_INTERFACE_DETAIL WHERE  ROW_ID = ?");
            pstmt.setDouble(1, this.getROW_ID());
            pstmt.executeUpdate();
            pstmt.close();
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            // 输出出错Sql语句
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            sqlObj.setSQL(4, this.getSchema());
            System.out.println("出错Sql为：" + sqlObj.getSQL());
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update()
    {
        PreparedStatement pstmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            pstmt = con.prepareStatement("UPDATE OF_INTERFACE_DETAIL SET  ROW_ID = ? , CURRENCY_CODE = ? , USER_JE_CATEGORY_NAME = ? , SEGMENT1 = ? , SEGMENT2 = ? , SEGMENT3 = ? , SEGMENT4 = ? , SEGMENT5 = ? , SEGMENT6 = ? , SEGMENT7 = ? , SEGMENT8 = ? , TRANSACTION_DATE = ? , ACCOUNTING_DATE = ? , POSTING_ID = ? , ENTERED_DR = ? , ENTERED_CR = ? , REFERENCE5 = ? , REFERENCE10 = ? , CURRENCY_CONVERSION_DATE = ? , CURRENCY_CONVERSION_RATE = ? , ACCOUNTED_DR = ? , ACCOUNTED_CR = ? , ATTRIBUTE1 = ? , ATTRIBUTE2 = ? , ATTRIBUTE3 = ? , ATTRIBUTE4 = ? , ATTRIBUTE5 = ? , ATTRIBUTE6 = ? , ATTRIBUTE7 = ? , ATTRIBUTE8 = ? , ATTRIBUTE9 = ? , ATTRIBUTE10 = ? , ATTRIBUTE11 = ? WHERE  ROW_ID = ?");
            pstmt.setDouble(1, this.getROW_ID());
            if (this.getCURRENCY_CODE() == null || this.getCURRENCY_CODE().equals("null"))
            {
                pstmt.setNull(2, 12);
            }
            else
            {
                pstmt.setString(2, this.getCURRENCY_CODE());
            }
            if (this.getUSER_JE_CATEGORY_NAME() == null || this.getUSER_JE_CATEGORY_NAME().equals("null"))
            {
                pstmt.setNull(3, 12);
            }
            else
            {
                pstmt.setString(3, this.getUSER_JE_CATEGORY_NAME());
            }
            if (this.getSEGMENT1() == null || this.getSEGMENT1().equals("null"))
            {
                pstmt.setNull(4, 12);
            }
            else
            {
                pstmt.setString(4, this.getSEGMENT1());
            }
            if (this.getSEGMENT2() == null || this.getSEGMENT2().equals("null"))
            {
                pstmt.setNull(5, 12);
            }
            else
            {
                pstmt.setString(5, this.getSEGMENT2());
            }
            if (this.getSEGMENT3() == null || this.getSEGMENT3().equals("null"))
            {
                pstmt.setNull(6, 12);
            }
            else
            {
                pstmt.setString(6, this.getSEGMENT3());
            }
            if (this.getSEGMENT4() == null || this.getSEGMENT4().equals("null"))
            {
                pstmt.setNull(7, 12);
            }
            else
            {
                pstmt.setString(7, this.getSEGMENT4());
            }
            if (this.getSEGMENT5() == null || this.getSEGMENT5().equals("null"))
            {
                pstmt.setNull(8, 12);
            }
            else
            {
                pstmt.setString(8, this.getSEGMENT5());
            }
            if (this.getSEGMENT6() == null || this.getSEGMENT6().equals("null"))
            {
                pstmt.setNull(9, 12);
            }
            else
            {
                pstmt.setString(9, this.getSEGMENT6());
            }
            if (this.getSEGMENT7() == null || this.getSEGMENT7().equals("null"))
            {
                pstmt.setNull(10, 12);
            }
            else
            {
                pstmt.setString(10, this.getSEGMENT7());
            }
            if (this.getSEGMENT8() == null || this.getSEGMENT8().equals("null"))
            {
                pstmt.setNull(11, 12);
            }
            else
            {
                pstmt.setString(11, this.getSEGMENT8());
            }
            if (this.getTRANSACTION_DATE() == null || this.getTRANSACTION_DATE().equals("null"))
            {
                pstmt.setNull(12, 91);
            }
            else
            {
                pstmt.setDate(12, Date.valueOf(this.getTRANSACTION_DATE()));
            }
            if (this.getACCOUNTING_DATE() == null || this.getACCOUNTING_DATE().equals("null"))
            {
                pstmt.setNull(13, 91);
            }
            else
            {
                pstmt.setDate(13, Date.valueOf(this.getACCOUNTING_DATE()));
            }
            pstmt.setDouble(14, this.getPOSTING_ID());
            pstmt.setDouble(15, this.getENTERED_DR());
            pstmt.setDouble(16, this.getENTERED_CR());
            if (this.getREFERENCE5() == null || this.getREFERENCE5().equals("null"))
            {
                pstmt.setNull(17, 12);
            }
            else
            {
                pstmt.setString(17, this.getREFERENCE5());
            }
            if (this.getREFERENCE10() == null || this.getREFERENCE10().equals("null"))
            {
                pstmt.setNull(18, 12);
            }
            else
            {
                pstmt.setString(18, this.getREFERENCE10());
            }
            if (this.getCURRENCY_CONVERSION_DATE() == null || this.getCURRENCY_CONVERSION_DATE().equals("null"))
            {
                pstmt.setNull(19, 91);
            }
            else
            {
                pstmt.setDate(19, Date.valueOf(this.getCURRENCY_CONVERSION_DATE()));
            }
            pstmt.setDouble(20, this.getCURRENCY_CONVERSION_RATE());
            pstmt.setDouble(21, this.getACCOUNTED_DR());
            pstmt.setDouble(22, this.getACCOUNTED_CR());
            if (this.getATTRIBUTE1() == null || this.getATTRIBUTE1().equals("null"))
            {
                pstmt.setNull(23, 12);
            }
            else
            {
                pstmt.setString(23, this.getATTRIBUTE1());
            }
            if (this.getATTRIBUTE2() == null || this.getATTRIBUTE2().equals("null"))
            {
                pstmt.setNull(24, 12);
            }
            else
            {
                pstmt.setString(24, this.getATTRIBUTE2());
            }
            if (this.getATTRIBUTE3() == null || this.getATTRIBUTE3().equals("null"))
            {
                pstmt.setNull(25, 12);
            }
            else
            {
                pstmt.setString(25, this.getATTRIBUTE3());
            }
            if (this.getATTRIBUTE4() == null || this.getATTRIBUTE4().equals("null"))
            {
                pstmt.setNull(26, 12);
            }
            else
            {
                pstmt.setString(26, this.getATTRIBUTE4());
            }
            if (this.getATTRIBUTE5() == null || this.getATTRIBUTE5().equals("null"))
            {
                pstmt.setNull(27, 12);
            }
            else
            {
                pstmt.setString(27, this.getATTRIBUTE5());
            }
            if (this.getATTRIBUTE6() == null || this.getATTRIBUTE6().equals("null"))
            {
                pstmt.setNull(28, 12);
            }
            else
            {
                pstmt.setString(28, this.getATTRIBUTE6());
            }
            if (this.getATTRIBUTE7() == null || this.getATTRIBUTE7().equals("null"))
            {
                pstmt.setNull(29, 12);
            }
            else
            {
                pstmt.setString(29, this.getATTRIBUTE7());
            }
            if (this.getATTRIBUTE8() == null || this.getATTRIBUTE8().equals("null"))
            {
                pstmt.setNull(30, 12);
            }
            else
            {
                pstmt.setString(30, this.getATTRIBUTE8());
            }
            if (this.getATTRIBUTE9() == null || this.getATTRIBUTE9().equals("null"))
            {
                pstmt.setNull(31, 12);
            }
            else
            {
                pstmt.setString(31, this.getATTRIBUTE9());
            }
            if (this.getATTRIBUTE10() == null || this.getATTRIBUTE10().equals("null"))
            {
                pstmt.setNull(32, 12);
            }
            else
            {
                pstmt.setString(32, this.getATTRIBUTE10());
            }
            if (this.getATTRIBUTE11() == null || this.getATTRIBUTE11().equals("null"))
            {
                pstmt.setNull(33, 12);
            }
            else
            {
                pstmt.setString(33, this.getATTRIBUTE11());
            }
            pstmt.setDouble(34, this.getROW_ID());
            pstmt.executeUpdate();
            pstmt.close();
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            // 输出出错Sql语句
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            sqlObj.setSQL(2, this.getSchema());
            System.out.println("出错Sql为：" + sqlObj.getSQL());
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 查询操作
     * 查询条件：主键
     * @return boolean
     */
    public boolean getInfo()
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            pstmt = con.prepareStatement("SELECT * FROM OF_INTERFACE_DETAIL WHERE  ROW_ID = ?", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setDouble(1, this.getROW_ID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next())
            {
                i++;
                if (!this.setSchema(rs, i))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "OF_INTERFACE_DETAILDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);
                    try
                    {
                        rs.close();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try
                    {
                        pstmt.close();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    if (!mflag)
                    {
                        try
                        {
                            con.close();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    return false;
                }
                break;
            }
            try
            {
                rs.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            if (i == 0)
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            // 输出出错Sql语句
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            sqlObj.setSQL(6, this.getSchema());
            System.out.println("出错Sql为：" + sqlObj.getSQL());
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            try
            {
                pstmt.close();
            }
            catch (Exception ex1)
            {
                ex1.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            // 断开数据库连接
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 查询操作
     * 查询条件：传入的schema中有值的字段
     * @return boolean
     */
    public OF_INTERFACE_DETAILSet query()
    {
        Statement stmt = null;
        ResultSet rs = null;
        OF_INTERFACE_DETAILSet aOF_INTERFACE_DETAILSet = new OF_INTERFACE_DETAILSet();
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
        sqlObj.setSQL(5, this.getSchema());
        String sql = sqlObj.getSQL();
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                if(i>10000)      
                 { 
                 CError tError = new CError();
                 tError.moduleName = "OF_INTERFACE_DETAILDB";
                 tError.functionName = "query";
                 tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式";
                 this.mErrors .addOneError(tError);
                 break;//结束当前循环             
                 }
                OF_INTERFACE_DETAILSchema s1 = new OF_INTERFACE_DETAILSchema();
                s1.setSchema(rs, i);
                aOF_INTERFACE_DETAILSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {
                ex1.printStackTrace();
            }
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("##### Error Sql in OF_INTERFACE_DETAILDB at query(): " + sql);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return aOF_INTERFACE_DETAILSet;
    }

    public OF_INTERFACE_DETAILSet executeQuery(String sql)
    {
        Statement stmt = null;
        ResultSet rs = null;
        OF_INTERFACE_DETAILSet aOF_INTERFACE_DETAILSet = new OF_INTERFACE_DETAILSet();
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next())
            {
                i++;
                if(i>10000)      
                 { 
                 CError tError = new CError();
                 tError.moduleName = "OF_INTERFACE_DETAILDB";
                 tError.functionName = "executeQuery";
                 tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式";
                 this.mErrors .addOneError(tError);
                 break;//结束当前循环             
                 }
                OF_INTERFACE_DETAILSchema s1 = new OF_INTERFACE_DETAILSchema();
                if (!s1.setSchema(rs, i))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "OF_INTERFACE_DETAILDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aOF_INTERFACE_DETAILSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {
                ex1.printStackTrace();
            }
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("##### Error Sql in OF_INTERFACE_DETAILDB at query(): " + sql);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return aOF_INTERFACE_DETAILSet;
    }

    public OF_INTERFACE_DETAILSet query(int nStart, int nCount)
    {
        Statement stmt = null;
        ResultSet rs = null;
        OF_INTERFACE_DETAILSet aOF_INTERFACE_DETAILSet = new OF_INTERFACE_DETAILSet();
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
        sqlObj.setSQL(5, this.getSchema());
        String sql = sqlObj.getSQL();
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                if (i < nStart)
                {
                    continue;
                }
                if (i >= nStart + nCount)
                {
                    break;
                }
                OF_INTERFACE_DETAILSchema s1 = new OF_INTERFACE_DETAILSchema();
                s1.setSchema(rs, i);
                aOF_INTERFACE_DETAILSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {
                ex1.printStackTrace();
            }
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("##### Error Sql in OF_INTERFACE_DETAILDB at query(): " + sql);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return aOF_INTERFACE_DETAILSet;
    }

    public OF_INTERFACE_DETAILSet executeQuery(String sql, int nStart, int nCount)
    {
        Statement stmt = null;
        ResultSet rs = null;
        OF_INTERFACE_DETAILSet aOF_INTERFACE_DETAILSet = new OF_INTERFACE_DETAILSet();
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                if (i < nStart)
                {
                    continue;
                }
                if (i >= nStart + nCount)
                {
                    break;
                }
                OF_INTERFACE_DETAILSchema s1 = new OF_INTERFACE_DETAILSchema();
                if (!s1.setSchema(rs, i))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "OF_INTERFACE_DETAILDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aOF_INTERFACE_DETAILSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {
                ex1.printStackTrace();
            }
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("##### Error Sql in OF_INTERFACE_DETAILDB at query(): " + sql);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return aOF_INTERFACE_DETAILSet;
    }

    public boolean update(String strWherePart)
    {
        Statement stmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
        sqlObj.setSQL(2, this.getSchema());
        String sql = "update OF_INTERFACE_DETAIL " + sqlObj.getUpdPart() + " where " + strWherePart;
        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "OF_INTERFACE_DETAILDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);
                if (!mflag)
                {
                    try
                    {
                        con.close();
                    }
                    catch (Exception et)
                    {
                        et.printStackTrace();
                    }
                }
                return false;
            }
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            System.out.println("##### Error Sql in OF_INTERFACE_DETAILDB at update(String strWherePart): " + sql);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {
                ex1.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            // 断开数据库连接
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL)
    {
        if (mResultSet != null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
            if (!mflag)
            {
                con.close();
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "prepareData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try
            {
                mResultSet.close();
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                mStatement.close();
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData()
    {
        boolean flag = true;
        if (null == mResultSet)
        {
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            flag = mResultSet.next();
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     * @return OF_INTERFACE_DETAILSet
     */
    public OF_INTERFACE_DETAILSet getData()
    {
        int tCount = 0;
        OF_INTERFACE_DETAILSet tOF_INTERFACE_DETAILSet = new OF_INTERFACE_DETAILSet();
        OF_INTERFACE_DETAILSchema tOF_INTERFACE_DETAILSchema = null;
        if (null == mResultSet)
        {
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try
        {
            tCount = 1;
            tOF_INTERFACE_DETAILSchema = new OF_INTERFACE_DETAILSchema();
            tOF_INTERFACE_DETAILSchema.setSchema(mResultSet, 1);
            tOF_INTERFACE_DETAILSet.add(tOF_INTERFACE_DETAILSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT)
            {
                if (mResultSet.next())
                {
                    tOF_INTERFACE_DETAILSchema = new OF_INTERFACE_DETAILSchema();
                    tOF_INTERFACE_DETAILSchema.setSchema(mResultSet, 1);
                    tOF_INTERFACE_DETAILSet.add(tOF_INTERFACE_DETAILSchema);
                }
            }
        }
        catch (Exception ex)
        {
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "getData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2)
            {
                ex2.printStackTrace();
            }
            try
            {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3)
            {
                ex3.printStackTrace();
            }
            if (!mflag)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {
                    et.printStackTrace();
                }
            }
            return null;
        }
        return tOF_INTERFACE_DETAILSet;
    }

    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData()
    {
        boolean flag = true;
        try
        {
            if (null == mResultSet)
            {
                CError tError = new CError();
                tError.moduleName = "OF_INTERFACE_DETAILDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            }
            else
            {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2)
        {
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex2.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try
        {
            if (null == mStatement)
            {
                CError tError = new CError();
                tError.moduleName = "OF_INTERFACE_DETAILDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            }
            else
            {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3)
        {
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex3.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
