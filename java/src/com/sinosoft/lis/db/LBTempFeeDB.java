/*
 * <p>ClassName: LBTempFeeDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sinosoft.lis.schema.LBTempFeeSchema;
import com.sinosoft.lis.vschema.LBTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.SQLString;
import com.sinosoft.utility.StrTool;

public class LBTempFeeDB extends LBTempFeeSchema
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    // @Constructor
    public LBTempFeeDB(Connection tConnection)
    {
        con = tConnection;
        db = new DBOper(con, "LBTempFee");
        mflag = true;
    }

    public LBTempFeeDB()
    {
        con = null;
        db = new DBOper("LBTempFee");
        mflag = false;
    }

    // @Method
    public boolean insert()
    {
        LBTempFeeSchema tSchema = this.getSchema();
        if (!db.insert(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "insert";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public boolean update()
    {
        LBTempFeeSchema tSchema = this.getSchema();
        if (!db.update(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "update";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public boolean deleteSQL()
    {
        LBTempFeeSchema tSchema = this.getSchema();
        if (!db.deleteSQL(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public boolean delete()
    {
        LBTempFeeSchema tSchema = this.getSchema();
        if (!db.delete(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "delete";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public int getCount()
    {
        LBTempFeeSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean getInfo()
    {
        Statement stmt = null;
        ResultSet rs = null;

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBTempFee");
            LBTempFeeSchema aSchema = this.getSchema();
            sqlObj.setSQL(6, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                if (this.setSchema(rs, i) == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBTempFeeDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try
                    {
                        rs.close();
                    }
                    catch (Exception ex)
                    {}
                    try
                    {
                        stmt.close();
                    }
                    catch (Exception ex1)
                    {}

                    if (mflag == false)
                    {
                        try
                        {
                            con.close();
                        }
                        catch (Exception et)
                        {}
                    }
                    return false;
                }
                break;
            }
            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (i == 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LBTempFeeDB";
                tError.functionName = "getInfo";
                tError.errorMessage = "未找到相关数据!";
                this.mErrors.addOneError(tError);

                if (mflag == false)
                {
                    try
                    {
                        con.close();
                    }
                    catch (Exception et)
                    {}
                }
                return false;
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
            return false;
        }
        // 断开数据库连接
        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return true;
    }

    public LBTempFeeSet query()
    {
        Statement stmt = null;
        ResultSet rs = null;
        LBTempFeeSet aLBTempFeeSet = new LBTempFeeSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBTempFee");
            LBTempFeeSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                LBTempFeeSchema s1 = new LBTempFeeSchema();
                s1.setSchema(rs, i);
                aLBTempFeeSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aLBTempFeeSet;
    }

    public LBTempFeeSet executeQuery(String sql)
    {
        Statement stmt = null;
        ResultSet rs = null;
        LBTempFeeSet aLBTempFeeSet = new LBTempFeeSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next())
            {
                i++;
                LBTempFeeSchema s1 = new LBTempFeeSchema();
                if (s1.setSchema(rs, i) == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBTempFeeDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLBTempFeeSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aLBTempFeeSet;
    }

    public LBTempFeeSet query(int nStart, int nCount)
    {
        Statement stmt = null;
        ResultSet rs = null;
        LBTempFeeSet aLBTempFeeSet = new LBTempFeeSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBTempFee");
            LBTempFeeSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;

                if (i < nStart)
                {
                    continue;
                }

                if (i >= nStart + nCount)
                {
                    break;
                }

                LBTempFeeSchema s1 = new LBTempFeeSchema();
                s1.setSchema(rs, i);
                aLBTempFeeSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aLBTempFeeSet;
    }

    public LBTempFeeSet executeQuery(String sql, int nStart, int nCount)
    {
        Statement stmt = null;
        ResultSet rs = null;
        LBTempFeeSet aLBTempFeeSet = new LBTempFeeSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next())
            {
                i++;

                if (i < nStart)
                {
                    continue;
                }

                if (i >= nStart + nCount)
                {
                    break;
                }

                LBTempFeeSchema s1 = new LBTempFeeSchema();
                if (s1.setSchema(rs, i) == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBTempFeeDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLBTempFeeSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aLBTempFeeSet;
    }

}
