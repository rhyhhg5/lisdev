/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.sinosoft.lis.schema.ES_PROCESS_DEFSchema;
import com.sinosoft.lis.vschema.ES_PROCESS_DEFSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.SQLString;
import com.sinosoft.utility.StrTool;

/*
 * <p>ClassName: ES_PROCESS_DEFDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-19
 */
public class ES_PROCESS_DEFDB extends ES_PROCESS_DEFSchema
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    // @Constructor
    public ES_PROCESS_DEFDB(Connection tConnection)
    {
        con = tConnection;
        db = new DBOper(con, "ES_PROCESS_DEF");
        mflag = true;
    }

    public ES_PROCESS_DEFDB()
    {
        con = null;
        db = new DBOper("ES_PROCESS_DEF");
        mflag = false;
    }

    // @Method
    public boolean insert()
    {
        ES_PROCESS_DEFSchema tSchema = this.getSchema();
        if (!db.insert(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "insert";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public boolean update()
    {
        ES_PROCESS_DEFSchema tSchema = this.getSchema();
        if (!db.update(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "update";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public boolean deleteSQL()
    {
        ES_PROCESS_DEFSchema tSchema = this.getSchema();
        if (!db.deleteSQL(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public boolean delete()
    {
        ES_PROCESS_DEFSchema tSchema = this.getSchema();
        if (!db.delete(tSchema))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "delete";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    public int getCount()
    {
        ES_PROCESS_DEFSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean getInfo()
    {
        Statement stmt = null;
        ResultSet rs = null;

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_PROCESS_DEF");
            ES_PROCESS_DEFSchema aSchema = this.getSchema();
            sqlObj.setSQL(6, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                if (this.setSchema(rs, i) == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_PROCESS_DEFDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try
                    {
                        rs.close();
                    }
                    catch (Exception ex)
                    {}
                    try
                    {
                        stmt.close();
                    }
                    catch (Exception ex1)
                    {}

                    if (mflag == false)
                    {
                        try
                        {
                            con.close();
                        }
                        catch (Exception et)
                        {}
                    }
                    return false;
                }
                break;
            }
            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (i == 0)
            {
                if (mflag == false)
                {
                    try
                    {
                        con.close();
                    }
                    catch (Exception et)
                    {}
                }
                return false;
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
            return false;
        }
        // 断开数据库连接
        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return true;
    }

    public ES_PROCESS_DEFSet query()
    {
        Statement stmt = null;
        ResultSet rs = null;
        ES_PROCESS_DEFSet aES_PROCESS_DEFSet = new ES_PROCESS_DEFSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_PROCESS_DEF");
            ES_PROCESS_DEFSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;
                ES_PROCESS_DEFSchema s1 = new ES_PROCESS_DEFSchema();
                s1.setSchema(rs, i);
                aES_PROCESS_DEFSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aES_PROCESS_DEFSet;
    }

    public ES_PROCESS_DEFSet executeQuery(String sql)
    {
        Statement stmt = null;
        ResultSet rs = null;
        ES_PROCESS_DEFSet aES_PROCESS_DEFSet = new ES_PROCESS_DEFSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next())
            {
                i++;
                ES_PROCESS_DEFSchema s1 = new ES_PROCESS_DEFSchema();
                if (s1.setSchema(rs, i) == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_PROCESS_DEFDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aES_PROCESS_DEFSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aES_PROCESS_DEFSet;
    }

    public ES_PROCESS_DEFSet query(int nStart, int nCount)
    {
        Statement stmt = null;
        ResultSet rs = null;
        ES_PROCESS_DEFSet aES_PROCESS_DEFSet = new ES_PROCESS_DEFSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_PROCESS_DEF");
            ES_PROCESS_DEFSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next())
            {
                i++;

                if (i < nStart)
                {
                    continue;
                }

                if (i >= nStart + nCount)
                {
                    break;
                }

                ES_PROCESS_DEFSchema s1 = new ES_PROCESS_DEFSchema();
                s1.setSchema(rs, i);
                aES_PROCESS_DEFSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aES_PROCESS_DEFSet;
    }

    public ES_PROCESS_DEFSet executeQuery(String sql, int nStart, int nCount)
    {
        Statement stmt = null;
        ResultSet rs = null;
        ES_PROCESS_DEFSet aES_PROCESS_DEFSet = new ES_PROCESS_DEFSet();

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next())
            {
                i++;

                if (i < nStart)
                {
                    continue;
                }

                if (i >= nStart + nCount)
                {
                    break;
                }

                ES_PROCESS_DEFSchema s1 = new ES_PROCESS_DEFSchema();
                if (s1.setSchema(rs, i) == false)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_PROCESS_DEFDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aES_PROCESS_DEFSet.add(s1);
            }
            try
            {
                rs.close();
            }
            catch (Exception ex)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                rs.close();
            }
            catch (Exception ex2)
            {}
            try
            {
                stmt.close();
            }
            catch (Exception ex3)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
        }

        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return aES_PROCESS_DEFSet;
    }

    public boolean update(String strWherePart)
    {
        Statement stmt = null;

        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }

        try
        {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_PROCESS_DEF");
            ES_PROCESS_DEFSchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update ES_PROCESS_DEF " + sqlObj.getUpdPart() +
                         " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ES_PROCESS_DEFDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (mflag == false)
                {
                    try
                    {
                        con.close();
                    }
                    catch (Exception et)
                    {}
                }
                return false;
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_PROCESS_DEFDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try
            {
                stmt.close();
            }
            catch (Exception ex1)
            {}

            if (mflag == false)
            {
                try
                {
                    con.close();
                }
                catch (Exception et)
                {}
            }
            return false;
        }
        // 断开数据库连接
        if (mflag == false)
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {}
        }

        return true;
    }

}
