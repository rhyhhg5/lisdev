/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAssessMainDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class LAAssessMainDB extends LAAssessMainSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LAAssessMainDB(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LAAssessMain");
        mflag = true;
    }

    public LAAssessMainDB() {
        con = null;
        db = new DBOper("LAAssessMain");
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LAAssessMainSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LAAssessMainSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LAAssessMain WHERE  IndexCalNo = ? AND AgentGrade = ? AND BranchType = ? AND ManageCom = ? AND AssessType = ?");
            if (this.getIndexCalNo() == null ||
                this.getIndexCalNo().equals("null")) {
                pstmt.setNull(1, 1);
            } else {
                pstmt.setString(1, StrTool.space(this.getIndexCalNo(), 8));
            }
            if (this.getAgentGrade() == null ||
                this.getAgentGrade().equals("null")) {
                pstmt.setNull(2, 1);
            } else {
                pstmt.setString(2, StrTool.space(this.getAgentGrade(), 3));
            }
            if (this.getBranchType() == null ||
                this.getBranchType().equals("null")) {
                pstmt.setNull(3, 1);
            } else {
                pstmt.setString(3, StrTool.space(this.getBranchType(), 2));
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(4, 1);
            } else {
                pstmt.setString(4, StrTool.space(this.getManageCom(), 10));
            }
            if (this.getAssessType() == null ||
                this.getAssessType().equals("null")) {
                pstmt.setNull(5, 1);
            } else {
                pstmt.setString(5, StrTool.space(this.getAssessType(), 2));
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LAAssessMain SET  IndexCalNo = ? , AgentGrade = ? , BranchType = ? , ManageCom = ? , State = ? , AssessCount = ? , ConfirmCount = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BranchAttr = ? , AssessType = ? , BranchType2 = ? WHERE  IndexCalNo = ? AND AgentGrade = ? AND BranchType = ? AND ManageCom = ? AND AssessType = ?");
            if (this.getIndexCalNo() == null ||
                this.getIndexCalNo().equals("null")) {
                pstmt.setNull(1, 1);
            } else {
                pstmt.setString(1, this.getIndexCalNo());
            }
            if (this.getAgentGrade() == null ||
                this.getAgentGrade().equals("null")) {
                pstmt.setNull(2, 1);
            } else {
                pstmt.setString(2, this.getAgentGrade());
            }
            if (this.getBranchType() == null ||
                this.getBranchType().equals("null")) {
                pstmt.setNull(3, 1);
            } else {
                pstmt.setString(3, this.getBranchType());
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(4, 1);
            } else {
                pstmt.setString(4, this.getManageCom());
            }
            if (this.getState() == null || this.getState().equals("null")) {
                pstmt.setNull(5, 1);
            } else {
                pstmt.setString(5, this.getState());
            }
            pstmt.setInt(6, this.getAssessCount());
            pstmt.setInt(7, this.getConfirmCount());
            if (this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(8, 1);
            } else {
                pstmt.setString(8, this.getOperator());
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(9, 91);
            } else {
                pstmt.setDate(9, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(10, 1);
            } else {
                pstmt.setString(10, this.getMakeTime());
            }
            if (this.getModifyDate() == null ||
                this.getModifyDate().equals("null")) {
                pstmt.setNull(11, 91);
            } else {
                pstmt.setDate(11, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null ||
                this.getModifyTime().equals("null")) {
                pstmt.setNull(12, 1);
            } else {
                pstmt.setString(12, this.getModifyTime());
            }
            if (this.getBranchAttr() == null ||
                this.getBranchAttr().equals("null")) {
                pstmt.setNull(13, 1);
            } else {
                pstmt.setString(13, this.getBranchAttr());
            }
            if (this.getAssessType() == null ||
                this.getAssessType().equals("null")) {
                pstmt.setNull(14, 1);
            } else {
                pstmt.setString(14, this.getAssessType());
            }
            if (this.getBranchType2() == null ||
                this.getBranchType2().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getBranchType2());
            }
            // set where condition
            if (this.getIndexCalNo() == null ||
                this.getIndexCalNo().equals("null")) {
                pstmt.setNull(16, 1);
            } else {
                pstmt.setString(16, StrTool.space(this.getIndexCalNo(), 8));
            }
            if (this.getAgentGrade() == null ||
                this.getAgentGrade().equals("null")) {
                pstmt.setNull(17, 1);
            } else {
                pstmt.setString(17, StrTool.space(this.getAgentGrade(), 3));
            }
            if (this.getBranchType() == null ||
                this.getBranchType().equals("null")) {
                pstmt.setNull(18, 1);
            } else {
                pstmt.setString(18, StrTool.space(this.getBranchType(), 2));
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(19, 1);
            } else {
                pstmt.setString(19, StrTool.space(this.getManageCom(), 10));
            }
            if (this.getAssessType() == null ||
                this.getAssessType().equals("null")) {
                pstmt.setNull(20, 1);
            } else {
                pstmt.setString(20, StrTool.space(this.getAssessType(), 2));
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LAAssessMain VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if (this.getIndexCalNo() == null ||
                this.getIndexCalNo().equals("null")) {
                pstmt.setNull(1, 1);
            } else {
                pstmt.setString(1, this.getIndexCalNo());
            }
            if (this.getAgentGrade() == null ||
                this.getAgentGrade().equals("null")) {
                pstmt.setNull(2, 1);
            } else {
                pstmt.setString(2, this.getAgentGrade());
            }
            if (this.getBranchType() == null ||
                this.getBranchType().equals("null")) {
                pstmt.setNull(3, 1);
            } else {
                pstmt.setString(3, this.getBranchType());
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(4, 1);
            } else {
                pstmt.setString(4, this.getManageCom());
            }
            if (this.getState() == null || this.getState().equals("null")) {
                pstmt.setNull(5, 1);
            } else {
                pstmt.setString(5, this.getState());
            }
            pstmt.setInt(6, this.getAssessCount());
            pstmt.setInt(7, this.getConfirmCount());
            if (this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(8, 1);
            } else {
                pstmt.setString(8, this.getOperator());
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(9, 91);
            } else {
                pstmt.setDate(9, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(10, 1);
            } else {
                pstmt.setString(10, this.getMakeTime());
            }
            if (this.getModifyDate() == null ||
                this.getModifyDate().equals("null")) {
                pstmt.setNull(11, 91);
            } else {
                pstmt.setDate(11, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null ||
                this.getModifyTime().equals("null")) {
                pstmt.setNull(12, 1);
            } else {
                pstmt.setString(12, this.getModifyTime());
            }
            if (this.getBranchAttr() == null ||
                this.getBranchAttr().equals("null")) {
                pstmt.setNull(13, 1);
            } else {
                pstmt.setString(13, this.getBranchAttr());
            }
            if (this.getAssessType() == null ||
                this.getAssessType().equals("null")) {
                pstmt.setNull(14, 1);
            } else {
                pstmt.setString(14, this.getAssessType());
            }
            if (this.getBranchType2() == null ||
                this.getBranchType2().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getBranchType2());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LAAssessMain WHERE  IndexCalNo = ? AND AgentGrade = ? AND BranchType = ? AND ManageCom = ? AND AssessType = ?",
                                         ResultSet.TYPE_FORWARD_ONLY,
                                         ResultSet.CONCUR_READ_ONLY);
            if (this.getIndexCalNo() == null ||
                this.getIndexCalNo().equals("null")) {
                pstmt.setNull(1, 1);
            } else {
                pstmt.setString(1, StrTool.space(this.getIndexCalNo(), 8));
            }
            if (this.getAgentGrade() == null ||
                this.getAgentGrade().equals("null")) {
                pstmt.setNull(2, 1);
            } else {
                pstmt.setString(2, StrTool.space(this.getAgentGrade(), 3));
            }
            if (this.getBranchType() == null ||
                this.getBranchType().equals("null")) {
                pstmt.setNull(3, 1);
            } else {
                pstmt.setString(3, StrTool.space(this.getBranchType(), 2));
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(4, 1);
            } else {
                pstmt.setString(4, StrTool.space(this.getManageCom(), 10));
            }
            if (this.getAssessType() == null ||
                this.getAssessType().equals("null")) {
                pstmt.setNull(5, 1);
            } else {
                pstmt.setString(5, StrTool.space(this.getAssessType(), 2));
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAAssessMainDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try {
                        rs.close();
                    } catch (Exception ex) {}
                    try {
                        pstmt.close();
                    } catch (Exception ex1) {}

                    if (!mflag) {
                        try {
                            con.close();
                        } catch (Exception et) {}
                    }
                    return false;
                }
                break;
            }
            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                pstmt.close();
            } catch (Exception ex3) {}

            if (i == 0) {
                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                pstmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public LAAssessMainSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LAAssessMainSet aLAAssessMainSet = new LAAssessMainSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LAAssessMain");
            LAAssessMainSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                LAAssessMainSchema s1 = new LAAssessMainSchema();
                s1.setSchema(rs, i);
                aLAAssessMainSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLAAssessMainSet;
    }

    public LAAssessMainSet executeQuery(String sql) {
        Statement stmt = null;
        ResultSet rs = null;
        LAAssessMainSet aLAAssessMainSet = new LAAssessMainSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                LAAssessMainSchema s1 = new LAAssessMainSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAAssessMainDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLAAssessMainSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLAAssessMainSet;
    }

    public LAAssessMainSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LAAssessMainSet aLAAssessMainSet = new LAAssessMainSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LAAssessMain");
            LAAssessMainSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LAAssessMainSchema s1 = new LAAssessMainSchema();
                s1.setSchema(rs, i);
                aLAAssessMainSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLAAssessMainSet;
    }

    public LAAssessMainSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LAAssessMainSet aLAAssessMainSet = new LAAssessMainSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LAAssessMainSchema s1 = new LAAssessMainSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAAssessMainDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLAAssessMainSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLAAssessMainSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LAAssessMain");
            LAAssessMainSchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update LAAssessMain " + sqlObj.getUpdPart() +
                         " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAssessMainDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                stmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                             ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "prepareData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            } catch (Exception ex2) {}
            try {
                mStatement.close();
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     * @return LAAssessMainSet
     */
    public LAAssessMainSet getData() {
        int tCount = 0;
        LAAssessMainSet tLAAssessMainSet = new LAAssessMainSet();
        LAAssessMainSchema tLAAssessMainSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLAAssessMainSchema = new LAAssessMainSchema();
            tLAAssessMainSchema.setSchema(mResultSet, 1);
            tLAAssessMainSet.add(tLAAssessMainSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLAAssessMainSchema = new LAAssessMainSchema();
                    tLAAssessMainSchema.setSchema(mResultSet, 1);
                    tLAAssessMainSet.add(tLAAssessMainSchema);
                }
            }
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "getData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return null;
        }
        return tLAAssessMainSet;
    }

    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LAAssessMainDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        } catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex2.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LAAssessMainDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        } catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex3.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
