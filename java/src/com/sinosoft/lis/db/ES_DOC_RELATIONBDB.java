/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;

import com.sinosoft.lis.schema.ES_DOC_RELATIONBSchema;
import com.sinosoft.lis.vschema.ES_DOC_RELATIONBSet;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_RELATIONBDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class ES_DOC_RELATIONBDB extends ES_DOC_RELATIONBSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public ES_DOC_RELATIONBDB(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "ES_DOC_RELATIONB");
        mflag = true;
    }

    public ES_DOC_RELATIONBDB() {
        con = null;
        db = new DBOper("ES_DOC_RELATIONB");
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        ES_DOC_RELATIONBSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        ES_DOC_RELATIONBSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM ES_DOC_RELATIONB WHERE  DocID = ? AND BussNoType = ? AND BussNo = ?");
            pstmt.setDouble(1, this.getDocID());
            if (this.getBussNoType() == null ||
                this.getBussNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, StrTool.GBKToUnicode(this.getBussNoType()));
            }
            if (this.getBussNo() == null || this.getBussNo().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, StrTool.GBKToUnicode(this.getBussNo()));
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE ES_DOC_RELATIONB SET  DocID = ? , BussNoType = ? , BussNo = ? , DocCode = ? , BussType = ? , SubType = ? , RelaFlag = ? WHERE  DocID = ? AND BussNoType = ? AND BussNo = ?");
            pstmt.setDouble(1, this.getDocID());
            if (this.getBussNoType() == null ||
                this.getBussNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, StrTool.GBKToUnicode(this.getBussNoType()));
            }
            if (this.getBussNo() == null || this.getBussNo().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, StrTool.GBKToUnicode(this.getBussNo()));
            }
            if (this.getDocCode() == null || this.getDocCode().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, StrTool.GBKToUnicode(this.getDocCode()));
            }
            if (this.getBussType() == null || this.getBussType().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, StrTool.GBKToUnicode(this.getBussType()));
            }
            if (this.getSubType() == null || this.getSubType().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, StrTool.GBKToUnicode(this.getSubType()));
            }
            if (this.getRelaFlag() == null || this.getRelaFlag().equals("null")) {
                pstmt.setNull(7, 12);
            } else {
                pstmt.setString(7, StrTool.GBKToUnicode(this.getRelaFlag()));
            }
            // set where condition
            pstmt.setDouble(8, this.getDocID());
            if (this.getBussNoType() == null ||
                this.getBussNoType().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, StrTool.GBKToUnicode(this.getBussNoType()));
            }
            if (this.getBussNo() == null || this.getBussNo().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, StrTool.GBKToUnicode(this.getBussNo()));
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement(
                    "INSERT INTO ES_DOC_RELATIONB VALUES( ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setDouble(1, this.getDocID());
            if (this.getBussNoType() == null ||
                this.getBussNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, StrTool.GBKToUnicode(this.getBussNoType()));
            }
            if (this.getBussNo() == null || this.getBussNo().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, StrTool.GBKToUnicode(this.getBussNo()));
            }
            if (this.getDocCode() == null || this.getDocCode().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, StrTool.GBKToUnicode(this.getDocCode()));
            }
            if (this.getBussType() == null || this.getBussType().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, StrTool.GBKToUnicode(this.getBussType()));
            }
            if (this.getSubType() == null || this.getSubType().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, StrTool.GBKToUnicode(this.getSubType()));
            }
            if (this.getRelaFlag() == null || this.getRelaFlag().equals("null")) {
                pstmt.setNull(7, 12);
            } else {
                pstmt.setString(7, StrTool.GBKToUnicode(this.getRelaFlag()));
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM ES_DOC_RELATIONB WHERE  DocID = ? AND BussNoType = ? AND BussNo = ?",
                                         ResultSet.TYPE_FORWARD_ONLY,
                                         ResultSet.CONCUR_READ_ONLY);
            pstmt.setDouble(1, this.getDocID());
            if (this.getBussNoType() == null ||
                this.getBussNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, StrTool.GBKToUnicode(this.getBussNoType()));
            }
            if (this.getBussNo() == null || this.getBussNo().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, StrTool.GBKToUnicode(this.getBussNo()));
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_DOC_RELATIONBDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try {
                        rs.close();
                    } catch (Exception ex) {}
                    try {
                        pstmt.close();
                    } catch (Exception ex1) {}

                    if (!mflag) {
                        try {
                            con.close();
                        } catch (Exception et) {}
                    }
                    return false;
                }
                break;
            }
            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                pstmt.close();
            } catch (Exception ex3) {}

            if (i == 0) {
                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                pstmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public ES_DOC_RELATIONBSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_RELATIONBSet aES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_DOC_RELATIONB");
            ES_DOC_RELATIONBSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                ES_DOC_RELATIONBSchema s1 = new ES_DOC_RELATIONBSchema();
                s1.setSchema(rs, i);
                aES_DOC_RELATIONBSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_RELATIONBSet;
    }

    public ES_DOC_RELATIONBSet executeQuery(String sql) {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_RELATIONBSet aES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                ES_DOC_RELATIONBSchema s1 = new ES_DOC_RELATIONBSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_DOC_RELATIONBDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aES_DOC_RELATIONBSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_RELATIONBSet;
    }

    public ES_DOC_RELATIONBSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_RELATIONBSet aES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_DOC_RELATIONB");
            ES_DOC_RELATIONBSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                ES_DOC_RELATIONBSchema s1 = new ES_DOC_RELATIONBSchema();
                s1.setSchema(rs, i);
                aES_DOC_RELATIONBSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_RELATIONBSet;
    }

    public ES_DOC_RELATIONBSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        ES_DOC_RELATIONBSet aES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                ES_DOC_RELATIONBSchema s1 = new ES_DOC_RELATIONBSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ES_DOC_RELATIONBDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aES_DOC_RELATIONBSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aES_DOC_RELATIONBSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("ES_DOC_RELATIONB");
            ES_DOC_RELATIONBSchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update ES_DOC_RELATIONB " + sqlObj.getUpdPart() +
                         " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ES_DOC_RELATIONBDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                stmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                             ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "prepareData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            } catch (Exception ex2) {}
            try {
                mStatement.close();
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     * @return ES_DOC_RELATIONBSet
     */
    public ES_DOC_RELATIONBSet getData() {
        int tCount = 0;
        ES_DOC_RELATIONBSet tES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();
        ES_DOC_RELATIONBSchema tES_DOC_RELATIONBSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tES_DOC_RELATIONBSchema = new ES_DOC_RELATIONBSchema();
            tES_DOC_RELATIONBSchema.setSchema(mResultSet, 1);
            tES_DOC_RELATIONBSet.add(tES_DOC_RELATIONBSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tES_DOC_RELATIONBSchema = new ES_DOC_RELATIONBSchema();
                    tES_DOC_RELATIONBSchema.setSchema(mResultSet, 1);
                    tES_DOC_RELATIONBSet.add(tES_DOC_RELATIONBSchema);
                }
            }
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "getData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return null;
        }
        return tES_DOC_RELATIONBSet;
    }

    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "ES_DOC_RELATIONBDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        } catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex2.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "ES_DOC_RELATIONBDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        } catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "ES_DOC_RELATIONBDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex3.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
