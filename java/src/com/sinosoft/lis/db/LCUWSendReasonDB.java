/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LCUWSendReasonSchema;
import com.sinosoft.lis.vschema.LCUWSendReasonSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCUWSendReasonDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2006-01-04
 */
public class LCUWSendReasonDB extends LCUWSendReasonSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LCUWSendReasonDB(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LCUWSendReason");
        mflag = true;
    }

    public LCUWSendReasonDB() {
        con = null;
        db = new DBOper("LCUWSendReason");
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LCUWSendReasonSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LCUWSendReasonSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LCUWSendReason WHERE  OtherNo = ? AND OtherNoType = ? AND UWNo = ? AND SerialNo = ?");
            if (this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getOtherNo());
            }
            if (this.getOtherNoType() == null ||
                this.getOtherNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNoType());
            }
            pstmt.setInt(3, this.getUWNo());
            if (this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getSerialNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LCUWSendReason SET  OtherNo = ? , OtherNoType = ? , UWNo = ? , SerialNo = ? , SendType = ? , UpUserCode = ? , Reason = ? , Operator = ? , ManageCom = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  OtherNo = ? AND OtherNoType = ? AND UWNo = ? AND SerialNo = ?");
            if (this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getOtherNo());
            }
            if (this.getOtherNoType() == null ||
                this.getOtherNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNoType());
            }
            pstmt.setInt(3, this.getUWNo());
            if (this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getSerialNo());
            }
            if (this.getSendType() == null || this.getSendType().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getSendType());
            }
            if (this.getUpUserCode() == null ||
                this.getUpUserCode().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getUpUserCode());
            }
            if (this.getReason() == null || this.getReason().equals("null")) {
                pstmt.setNull(7, 12);
            } else {
                pstmt.setString(7, this.getReason());
            }
            if (this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getOperator());
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getManageCom());
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(10, 91);
            } else {
                pstmt.setDate(10, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getMakeTime());
            }
            if (this.getModifyDate() == null ||
                this.getModifyDate().equals("null")) {
                pstmt.setNull(12, 91);
            } else {
                pstmt.setDate(12, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null ||
                this.getModifyTime().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getModifyTime());
            }
            // set where condition
            if (this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getOtherNo());
            }
            if (this.getOtherNoType() == null ||
                this.getOtherNoType().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getOtherNoType());
            }
            pstmt.setInt(16, this.getUWNo());
            if (this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getSerialNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LCUWSendReason VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if (this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getOtherNo());
            }
            if (this.getOtherNoType() == null ||
                this.getOtherNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNoType());
            }
            pstmt.setInt(3, this.getUWNo());
            if (this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getSerialNo());
            }
            if (this.getSendType() == null || this.getSendType().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getSendType());
            }
            if (this.getUpUserCode() == null ||
                this.getUpUserCode().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getUpUserCode());
            }
            if (this.getReason() == null || this.getReason().equals("null")) {
                pstmt.setNull(7, 12);
            } else {
                pstmt.setString(7, this.getReason());
            }
            if (this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getOperator());
            }
            if (this.getManageCom() == null ||
                this.getManageCom().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getManageCom());
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(10, 91);
            } else {
                pstmt.setDate(10, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getMakeTime());
            }
            if (this.getModifyDate() == null ||
                this.getModifyDate().equals("null")) {
                pstmt.setNull(12, 91);
            } else {
                pstmt.setDate(12, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null ||
                this.getModifyTime().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getModifyTime());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {}
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LCUWSendReason WHERE  OtherNo = ? AND OtherNoType = ? AND UWNo = ? AND SerialNo = ?",
                                         ResultSet.TYPE_FORWARD_ONLY,
                                         ResultSet.CONCUR_READ_ONLY);
            if (this.getOtherNo() == null || this.getOtherNo().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getOtherNo());
            }
            if (this.getOtherNoType() == null ||
                this.getOtherNoType().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getOtherNoType());
            }
            pstmt.setInt(3, this.getUWNo());
            if (this.getSerialNo() == null || this.getSerialNo().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getSerialNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCUWSendReasonDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try {
                        rs.close();
                    } catch (Exception ex) {}
                    try {
                        pstmt.close();
                    } catch (Exception ex1) {}

                    if (!mflag) {
                        try {
                            con.close();
                        } catch (Exception et) {}
                    }
                    return false;
                }
                break;
            }
            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                pstmt.close();
            } catch (Exception ex3) {}

            if (i == 0) {
                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                pstmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public LCUWSendReasonSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LCUWSendReasonSet aLCUWSendReasonSet = new LCUWSendReasonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCUWSendReason");
            LCUWSendReasonSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                LCUWSendReasonSchema s1 = new LCUWSendReasonSchema();
                s1.setSchema(rs, i);
                aLCUWSendReasonSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLCUWSendReasonSet;
    }

    public LCUWSendReasonSet executeQuery(String sql) {
        Statement stmt = null;
        ResultSet rs = null;
        LCUWSendReasonSet aLCUWSendReasonSet = new LCUWSendReasonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                LCUWSendReasonSchema s1 = new LCUWSendReasonSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCUWSendReasonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLCUWSendReasonSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLCUWSendReasonSet;
    }

    public LCUWSendReasonSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCUWSendReasonSet aLCUWSendReasonSet = new LCUWSendReasonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCUWSendReason");
            LCUWSendReasonSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LCUWSendReasonSchema s1 = new LCUWSendReasonSchema();
                s1.setSchema(rs, i);
                aLCUWSendReasonSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLCUWSendReasonSet;
    }

    public LCUWSendReasonSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCUWSendReasonSet aLCUWSendReasonSet = new LCUWSendReasonSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LCUWSendReasonSchema s1 = new LCUWSendReasonSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCUWSendReasonDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLCUWSendReasonSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLCUWSendReasonSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCUWSendReason");
            LCUWSendReasonSchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update LCUWSendReason " + sqlObj.getUpdPart() +
                         " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCUWSendReasonDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                stmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                             ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "prepareData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            } catch (Exception ex2) {}
            try {
                mStatement.close();
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     * @return LCUWSendReasonSet
     */
    public LCUWSendReasonSet getData() {
        int tCount = 0;
        LCUWSendReasonSet tLCUWSendReasonSet = new LCUWSendReasonSet();
        LCUWSendReasonSchema tLCUWSendReasonSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLCUWSendReasonSchema = new LCUWSendReasonSchema();
            tLCUWSendReasonSchema.setSchema(mResultSet, 1);
            tLCUWSendReasonSet.add(tLCUWSendReasonSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLCUWSendReasonSchema = new LCUWSendReasonSchema();
                    tLCUWSendReasonSchema.setSchema(mResultSet, 1);
                    tLCUWSendReasonSet.add(tLCUWSendReasonSchema);
                }
            }
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "getData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return null;
        }
        return tLCUWSendReasonSet;
    }

    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LCUWSendReasonDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        } catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex2.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LCUWSendReasonDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        } catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LCUWSendReasonDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex3.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
