/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LHServTaskRelaSchema;
import com.sinosoft.lis.vschema.LHServTaskRelaSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LHServTaskRelaDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-服务实施管理-20060731
 * @CreateDate：2006-08-01
 */
public class LHServTaskRelaDB extends LHServTaskRelaSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LHServTaskRelaDB(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LHServTaskRela");
        mflag = true;
    }

    public LHServTaskRelaDB() {
        con = null;
        db = new DBOper("LHServTaskRela");
        mflag = false;
    }

    // @Method
    public boolean insert() {
        LHServTaskRelaSchema tSchema = this.getSchema();
        if (db.insert(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "insert";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public boolean update() {
        LHServTaskRelaSchema tSchema = this.getSchema();
        if (db.update(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "update";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public boolean deleteSQL() {
        LHServTaskRelaSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public boolean delete() {
        LHServTaskRelaSchema tSchema = this.getSchema();
        if (db.delete(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "delete";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LHServTaskRelaSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean getInfo() {
        Statement stmt = null;
        ResultSet rs = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LHServTaskRela");
            LHServTaskRelaSchema aSchema = this.getSchema();
            sqlObj.setSQL(6, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LHServTaskRelaDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try {
                        rs.close();
                    } catch (Exception ex) {}
                    try {
                        stmt.close();
                    } catch (Exception ex1) {}

                    if (!mflag) {
                        try {
                            con.close();
                        } catch (Exception et) {}
                    }
                    return false;
                }
                break;
            }
            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (i == 0) {
                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "getInfo";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    public LHServTaskRelaSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LHServTaskRelaSet aLHServTaskRelaSet = new LHServTaskRelaSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LHServTaskRela");
            LHServTaskRelaSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                LHServTaskRelaSchema s1 = new LHServTaskRelaSchema();
                s1.setSchema(rs, i);
                aLHServTaskRelaSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLHServTaskRelaSet;
    }

    public LHServTaskRelaSet executeQuery(String sql) {
        Statement stmt = null;
        ResultSet rs = null;
        LHServTaskRelaSet aLHServTaskRelaSet = new LHServTaskRelaSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                LHServTaskRelaSchema s1 = new LHServTaskRelaSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LHServTaskRelaDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLHServTaskRelaSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLHServTaskRelaSet;
    }

    public LHServTaskRelaSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LHServTaskRelaSet aLHServTaskRelaSet = new LHServTaskRelaSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LHServTaskRela");
            LHServTaskRelaSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LHServTaskRelaSchema s1 = new LHServTaskRelaSchema();
                s1.setSchema(rs, i);
                aLHServTaskRelaSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "query";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLHServTaskRelaSet;
    }

    public LHServTaskRelaSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LHServTaskRelaSet aLHServTaskRelaSet = new LHServTaskRelaSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LHServTaskRelaSchema s1 = new LHServTaskRelaSchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LHServTaskRelaDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLHServTaskRelaSet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {}
            try {
                stmt.close();
            } catch (Exception ex1) {}
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {}
            try {
                stmt.close();
            } catch (Exception ex3) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return aLHServTaskRelaSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                       ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LHServTaskRela");
            LHServTaskRelaSchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update LHServTaskRela " + sqlObj.getUpdPart() +
                         " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LHServTaskRelaDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {}
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "update";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);

            try {
                stmt.close();
            } catch (Exception ex1) {}

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                             ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "prepareData";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            } catch (Exception ex2) {}
            try {
                mStatement.close();
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     * @return LHServTaskRelaSet
     */
    public LHServTaskRelaSet getData() {
        int tCount = 0;
        LHServTaskRelaSet tLHServTaskRelaSet = new LHServTaskRelaSet();
        LHServTaskRelaSchema tLHServTaskRelaSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLHServTaskRelaSchema = new LHServTaskRelaSchema();
            tLHServTaskRelaSchema.setSchema(mResultSet, 1);
            tLHServTaskRelaSet.add(tLHServTaskRelaSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLHServTaskRelaSchema = new LHServTaskRelaSchema();
                    tLHServTaskRelaSchema.setSchema(mResultSet, 1);
                    tLHServTaskRelaSet.add(tLHServTaskRelaSchema);
                }
            }
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "getData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {}
            }
            return null;
        }
        return tLHServTaskRelaSet;
    }

    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LHServTaskRelaDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        } catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex2.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LHServTaskRelaDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        } catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaDB";
            tError.functionName = "closeData";
            tError.errorMessage = ex3.toString();
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
