package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.db.LMGrpChargeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LMGrpChargeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class GrpChargeBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LMGrpChargeSchema mLMGrpChargeSchema = new LMGrpChargeSchema();
    //private LMGrpChargeSet mLMGrpChargeSet=new LMGrpChargeSet();

    public GrpChargeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        //进行插入数据
        if (mOperate.equals("INSERT||CHARGE"))
        {
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败GrpChargeBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //对数据进行修改操作
        if (mOperate.equals("UPDATE||CHARGE"))
        {
            if (!updateData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败GrpChargeBL-->updateData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("---updateData---");
        }
        //对数据进行删除操作
        if (mOperate.equals("DELETE||CHARGE"))
        {
            if (!deleteData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败GrpChargeBL-->deleteData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("----deleteData---");
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start GrpChargeBL Submit...");
            GrpChargeBLS tGrpChargeBLS = new GrpChargeBLS();
            tGrpChargeBLS.submitData(mInputData, mOperate);
            System.out.println("End GrpChargeBL Submit...");
            //如果有需要处理的错误，则返回
            if (tGrpChargeBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tGrpChargeBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpChargeBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        mLMGrpChargeSchema.setOperator(this.mGlobalInput.Operator);
        mLMGrpChargeSchema.setMakeDate(PubFun.getCurrentDate());
        mLMGrpChargeSchema.setMakeTime(PubFun.getCurrentTime());
        mLMGrpChargeSchema.setModifyDate(PubFun.getCurrentDate());
        mLMGrpChargeSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        LMGrpChargeDB tLMGrpChargeDB = new LMGrpChargeDB();
        tLMGrpChargeDB.setGrpPolNo(mLMGrpChargeSchema.getGrpPolNo());
        tLMGrpChargeDB.setPayCountFrom(mLMGrpChargeSchema.getPayCountFrom());
        tLMGrpChargeDB.setPayCountTo(mLMGrpChargeSchema.getPayCountTo());
        tLMGrpChargeDB.setChargeType(mLMGrpChargeSchema.getChargeType());
        if (!tLMGrpChargeDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMGrpChargeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpChargeBL";
            tError.functionName = "updateData";
            tError.errorMessage = "数据查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLMGrpChargeSchema.setOperator(this.mGlobalInput.Operator);
        mLMGrpChargeSchema.setMakeDate(tLMGrpChargeDB.getMakeDate());
        mLMGrpChargeSchema.setMakeTime(tLMGrpChargeDB.getMakeTime());
        mLMGrpChargeSchema.setModifyDate(PubFun.getCurrentDate());
        mLMGrpChargeSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLMGrpChargeSchema.setSchema((LMGrpChargeSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LMGrpChargeSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start GrpChargeBLQuery Submit...");
        LMGrpChargeDB tLMGrpChargeDB = new LMGrpChargeDB();
        tLMGrpChargeDB.setSchema(this.mLMGrpChargeSchema);
        //this.mLMGrpChargeSet=tLMGrpChargeDB.query();
        //this.mResult.add(this.mLMGrpChargeSet);
        System.out.println("End GrpChargeBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLMGrpChargeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMGrpChargeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpChargeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.clear();
            this.mInputData.add(this.mLMGrpChargeSchema);
            mResult.clear();
            mResult.add(this.mLMGrpChargeSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpChargeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        GrpChargeBL GrpChargeBL1 = new GrpChargeBL();
    }
}
