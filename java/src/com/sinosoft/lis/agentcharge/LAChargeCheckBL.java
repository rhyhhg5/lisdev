package com.sinosoft.lis.agentcharge;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.schema.LAChargePayBatchLogSchema;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LAChargeCheckBL {
	private Logger cLogger = Logger.getLogger(getClass());

	/** 往工作流引擎中传输数据的容器 */

	private VData mInputData = new VData();
	// 传输数据类
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/**
	 * 发送报文的保单数据封装类
	 */
	private TransferData mOutxml = new TransferData();
	/**
	 * 前台传来的mChargetype,其中：ZX：手续费校验，HX:手续费结算
	 */
	private String mChargetype = "";
	/**
	 * 批次号
	 */
	private String mBatchNo = "";
	/**
	 * 操作人
	 */
	private String mOperator ="";
	/**
	 * 团个标记
	 */
	private String mContType = "";
	/**
	 * 中介机构
	 */
	private String mAgentCom = "";
	/**
	 * 中介机构与批次号一一对应
	 * 为了以后支持结算时可以多个批次号
	 */
//	private Map<String,String> mAgentComToBatchNo = new HashMap();
	private ExeSQL mExeSQL = new ExeSQL();
	private LAChargeSet mLAChargeSet = new LAChargeSet();

	public boolean submitData(VData cInputData, String cOperate) {
		// 首先将数据在本类中做一个备份
		mInputData = cInputData;
		System.out.println("Start ContInputChargecomChkBL Submit...");
		if (!getInputData(mInputData, cOperate)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		try {
			if (!dealData()) {
				return false;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println("End ContInputChargecomChkBL Submit...");

		mInputData = null;
		return true;
	}

	private boolean checkData() {

//		for(int i =0;i<this.mLAChargeSet.size();i++)
//		{
//			LAChargeSchema tLAChargeSchema = new LAChargeSchema();
//			tLAChargeSchema = this.mLAChargeSet.get(i);
//			if(this.mAgentComToBatchNo.containsKey(tLAChargeSchema.getAgentCom()))
//			{
//				continue;
//			}
//			else
//			{
//				this.mAgentComToBatchNo.put(tLAChargeSchema.getAgentCom(), tLAChargeSchema.getBatchNo());
//			}
//		}
		return true;
	}

	private boolean dealData() throws FileNotFoundException {
		String tresult = queryChargeInfo();
		if (!"".equals(tresult) && tresult != null) {
			VData cVData = new VData();
			TransferData cTransferData = new TransferData();
			cTransferData.setNameAndValue("BatchNo", this.mBatchNo);
			cTransferData.setNameAndValue("ChargeType", this.mChargetype);
			cTransferData.setNameAndValue("XML", tresult);
			cTransferData.setNameAndValue("Operator", this.mOperator);
			cVData.add(cTransferData);
			DealLAChargeBackXML tDealLAChargeBackXML = new DealLAChargeBackXML();
			boolean tFlag = tDealLAChargeBackXML.submit(cVData, "");
			if(false==tFlag)
			{
				this.mErrors.copyAllErrors(tDealLAChargeBackXML.mErrors);
			}
			else
			{
				LAChargePayBatchLogSchema tLAChargePayBatchLogSchema = new LAChargePayBatchLogSchema();
				tLAChargePayBatchLogSchema=tDealLAChargeBackXML.getLAChargePayBatchLogSchemaByBatchNo(this.mBatchNo);
				tFlag = getResult(tLAChargePayBatchLogSchema);
			}
			return tFlag;
		} else {
			System.out.println("获取返回报文失败！");
			CError tError = new CError();
			tError.moduleName = "ContInputChargecomChkBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询返回报文获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

	}


	private boolean getInputData(VData cInputData, String cOperate) {
		System.out.println("+==============");
		// 从输入数据中得到所有对象

		this.mLAChargeSet = (LAChargeSet) mInputData.get(0);		
		mChargetype = (String) mInputData.get(1);
		this.mBatchNo =(String)mInputData.get(2);
		this.mOperator =(String)mInputData.get(3);
		this.mContType =(String)mInputData.get(4);
		LAChargeSchema checkLAChargeSchema = new LAChargeSchema();
		checkLAChargeSchema =  this.mLAChargeSet.get(1);
		this.mAgentCom   = checkLAChargeSchema.getAgentCom();
		if(checkLAChargeSchema.getBranchType().equals("3")){
			this.mAgentCom = (String)mInputData.get(5);
			System.out.println(this.mAgentCom);
		}
		return true;
	}

	public String queryChargeInfo() {
		// 根据统一工号调用webservice查询业务员信息
		String tResponseXml = "";
		long mOldTimeMillis = System.currentTimeMillis();
		long mCurTimeMillis = mOldTimeMillis;
		try {
			RPCServiceClient tRPCServiceClient = new RPCServiceClient();
			//把以下的修改成了配置，所以需要查询
//			String url = "http://10.252.4.69:8080/jszjpt/services/SXGX_WS_SERVER?wsdl";
//			String qName = "http://ws.sinosoft.com";
//			String method = "ecService";
			String tSQL ="select code,codename from ldcode where codetype='JSAgentComChk' with ur";
			SSRS tSSRS = mExeSQL.execSQL(tSQL);
			String url = "";
			String qName = "";
			String method = "";
			if(0<tSSRS.getMaxRow())
			{
				for(int i=1;i<=tSSRS.getMaxRow();i++)
				{
					if("url".equals(tSSRS.GetText(i, 1)))
					{
						url = tSSRS.GetText(i, 2);
					}
					else if("qName".equals(tSSRS.GetText(i, 1)))
					{
						qName = tSSRS.GetText(i, 2);
					}
					else if("method".equals(tSSRS.GetText(i, 1)))
					{
						method = tSSRS.GetText(i, 2);
					}
					
				}
			}
			Options options = tRPCServiceClient.getOptions();
			EndpointReference targetEPR = new EndpointReference(url);
			options.setTo(targetEPR);
			options.setTimeOutInMilliSeconds(120000);//
			QName opAddEntry = new QName(qName, method);
			String inputXml = getinputXml(); // 这里是中介机构校验接口请求报文
			// String inputXml
			// ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Packet type=\"REQUEST\" version=\"1.0\" channel=\"0\"><Head><RequestType>C01</RequestType></Head><Body><Policy><SerialNo>00000000000000000007</SerialNo><prtno>18150206002</prtno><AgencyCode>PC3200000592</AgencyCode><AgencyType>01</AgencyType><ComCode>PICC</ComCode><Coverage><CoverageType>H</CoverageType></Coverage><Coverage><CoverageType>H</CoverageType></Coverage><PolicyApplyDate>2015-02-06 15:23:11</PolicyApplyDate><PolicyFlag>01</PolicyFlag></Policy></Body></Packet>";
			String serialNo = "PICCjs_86320000";// 这里是外部交易流水号，可为空
			Object[] opAddEntryArgs = new Object[] { inputXml, this.mChargetype,
					"C07", serialNo };
			System.out.println("################" + this.mChargetype);
			Class[] classes = new Class[] { String.class, String.class,
					String.class, String.class };
			mCurTimeMillis = System.currentTimeMillis();
			cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis)
					/ 1000.0 + "s");

			cLogger.info("开始调用WebService服务！" + url);
			tResponseXml = (String) tRPCServiceClient.invokeBlocking(
					opAddEntry, opAddEntryArgs, classes)[0];
			mCurTimeMillis = System.currentTimeMillis();
			cLogger.info("调用WebService服务成功！耗时："
					+ (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
			System.out.println("返回的报文：" + tResponseXml);
		} catch (Exception e) {
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "ContInputChargecomChkBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "发送校验报文失败!";
			this.mErrors.addOneError(e.getMessage());
		}
		return tResponseXml;
	}

	private String getinputXml() {
		LAChargeSchema cLAChargeSchema = new LAChargeSchema();
		cLAChargeSchema =  this.mLAChargeSet.get(1);
		Element tRootData = new Element("Packet");
		tRootData.addAttribute("type", "REQUEST");
		tRootData.addAttribute("version", "1.0");
		tRootData.addAttribute("channel", this.mContType);
		Element tHeadData = new Element("Head");
		Element tRequestType = new Element("RequestType");
		tRequestType.setText("C07");
		tHeadData.addContent(tRequestType);
		tRootData.addContent(tHeadData);
		String contsql = "select d.BankAccOpen,d.bankaccno,d.bankaccname,d.AgentOrganCode from lacom d where d.agentcom='"
			+ this.mAgentCom + "'";
		SSRS tres = mExeSQL.execSQL(contsql);
		if (tres != null && tres.MaxRow > 0) {
			mOutxml.setNameAndValue("BankName", tres.GetText(1, 1));
			mOutxml.setNameAndValue("AccountNumber", tres.GetText(1, 2));
			mOutxml.setNameAndValue("AccountName", tres.GetText(1, 3));
			mOutxml.setNameAndValue("AgentOrganCode", tres.GetText(1, 4));
		} else {
			mOutxml.setNameAndValue("BankName", "");
			mOutxml.setNameAndValue("AccountNumber", "");
			mOutxml.setNameAndValue("AccountName", "");
			mOutxml.setNameAndValue("AgentOrganCode", "");
		}
		Element tbody = new Element("Body");
		Element tCommission = new Element("Commission");
		Element tAgencyCode = new Element("AgencyCode");
		tAgencyCode.setText((String) mOutxml.getValueByName("AgentOrganCode"));
		tCommission.addContent(tAgencyCode);
		Element tBatchNo = new Element("BatchNo");
		tBatchNo.setText(this.mBatchNo);
		tCommission.addContent(tBatchNo);
		Element tStatementsComCode = new Element("StatementsComCode");
		tStatementsComCode.setText(cLAChargeSchema.getManageCom());
		tCommission.addContent(tStatementsComCode);
		Element tPayComCode = new Element("PayComCode");
		tPayComCode.setText(cLAChargeSchema.getManageCom().substring(0, 4));
		tCommission.addContent(tPayComCode);
		Element tCurrencyCode = new Element("CurrencyCode");
		tCurrencyCode.setText("CNY");
		tCommission.addContent(tCurrencyCode);
		
		Element tBankName = new Element("BankName");
		tBankName.setText((String) mOutxml.getValueByName("BankName"));
		tCommission.addContent(tBankName);
		Element tAccountNumber = new Element("AccountNumber");
		tAccountNumber
				.setText((String) mOutxml.getValueByName("AccountNumber"));
		tCommission.addContent(tAccountNumber);
		Element tAccountName = new Element("AccountName");
		tAccountName.setText((String) mOutxml.getValueByName("AccountName"));
		tCommission.addContent(tAccountName);
		HashMap tMmap = new HashMap();
		tMmap = dealDataOfLACharge(mLAChargeSet, mContType);
		Set tSet = tMmap.keySet();
		if(null!=tSet)
		{
			HashMap aMmap = new HashMap();
			Iterator iterator = tSet.iterator();
		    while(iterator.hasNext()) 
		    {
			    Object key = iterator.next();
			    Object value = tMmap.get(key);
				String[] prems = value.toString().split(",");
				double prem = Double.parseDouble(prems[0]);
				double charge = Double.parseDouble(prems[1]);
			    //保/批单类型+保/批单号+险种+缴费方式 +手续费比例
			    String[] arrays = key.toString().split(",");
			    String tPolicyTypeValue = arrays[0].substring(0,2);
			    String tPolicyEndorsementNoValue =  arrays[0].substring(2);
			    String tCoverageCodeValue = arrays[1];
			    String tPayTypeValue = arrays[2];
			    String tCommissionRateValue = arrays[3];
			    String tCommisionSN  = arrays[4];
			    mOutxml = new TransferData();
			    mOutxml.setNameAndValue("PolicyType", tPolicyTypeValue);
			    mOutxml.setNameAndValue("PolicyEndorsementNo", tPolicyEndorsementNoValue);
			    mOutxml.setNameAndValue("CostType", "01");
				mOutxml.setNameAndValue("PayType", tPayTypeValue);
				mOutxml.setNameAndValue("CoverageCode", tCoverageCodeValue);
				mOutxml.setNameAndValue("Premium", CommonBL.bigDoubleToCommonString(prem, "0.00"));
				mOutxml.setNameAndValue("CommissionRate", tCommissionRateValue);
				mOutxml.setNameAndValue("CommissionCharge", CommonBL.bigDoubleToCommonString(charge,"0.00"));
				System.out.println("+++++++++++++++++++++++++");
				
				String contsql1 = "";
//					
				if("0".equals(tPayTypeValue))
				{
					mOutxml.setNameAndValue("CurryPayPeriod","1");
				}
				else
				{
					String tPoliceNo ="";
					tPoliceNo=tPolicyEndorsementNoValue+tCoverageCodeValue+tPayTypeValue;
					if(aMmap.containsKey(tPoliceNo))
					{
						Object count = aMmap.get(tPoliceNo);
						int tvalue = Integer.parseInt(count.toString());
						aMmap.put(tPoliceNo, String.valueOf(tvalue+1));
					}	
					else
					{
						aMmap.put(tPoliceNo, "1");
					}
					int  count = Integer.parseInt(aMmap.get(tPoliceNo).toString());
					if("01".equals(tPolicyTypeValue))
					{
						if("0".equals(this.mContType))
						{
							contsql1 ="select count(distinct substr(commisionsn,1,9))+"+count+" from lacharge where riskcode ='"+tCoverageCodeValue+"' and grpcontno ='"+tPolicyEndorsementNoValue+"' and payintv ='"+tPayTypeValue+"' and chargestate ='1' and substr(commisionsn,1,9) <> '"+tCommisionSN+"'  with ur";
						}
						else if("1".equals(this.mContType))
						{
							contsql1 ="select count(distinct substr(commisionsn,1,9))+"+count+" from lacharge where riskcode ='"+tCoverageCodeValue+"' and contno ='"+tPolicyEndorsementNoValue+"' and payintv ='"+tPayTypeValue+"' and chargestate ='1' and substr(commisionsn,1,9) <> '"+tCommisionSN+"'  with ur";
						}
						SSRS tre = mExeSQL.execSQL(contsql1);
						if (tre != null && tre.MaxRow > 0) {
							mOutxml.setNameAndValue("CurryPayPeriod",tre.GetText(1, 1));
						}
						
					}
					else if("02".equals(tPolicyTypeValue))
					{
						mOutxml.setNameAndValue("CurryPayPeriod",count+"");
					}
				}
				String sql1 = "select c.risktype,c.riskname from lmriskapp c where c.riskcode='"+tCoverageCodeValue + "'";
				SSRS tr = mExeSQL.execSQL(sql1);
				if (tr != null && tr.MaxRow > 0) {
					String tOldSQL = "select codename from ldcode1 where codetype = 'jszjold' and code = '"+tPolicyEndorsementNoValue+"' and code1 = '"+tCoverageCodeValue+"' with ur ";
					String tOldType = new ExeSQL().getOneValue(tOldSQL);
					if(tOldType != null && !"".equals(tOldType)){
						mOutxml.setNameAndValue("CoverageType", tOldType);
					}else{
						mOutxml.setNameAndValue("CoverageType", tr.GetText(1, 1));
					}
					mOutxml.setNameAndValue("CoverageName", tr.GetText(1, 2));
				} 
				else
				{
					mOutxml.setNameAndValue("CoverageType", null);
					mOutxml.setNameAndValue("CoverageName", null);
				}
				Element tCommissionItem = new Element("CommissionItem");
				Element tPolicyType = new Element("PolicyType");
				tPolicyType.setText((String) mOutxml.getValueByName("PolicyType"));
				tCommissionItem.addContent(tPolicyType);
				Element tPolicyEndorsementNo = new Element("PolicyEndorsementNo");
				tPolicyEndorsementNo.setText((String) mOutxml
						.getValueByName("PolicyEndorsementNo"));
				tCommissionItem.addContent(tPolicyEndorsementNo);
				Element tCostType = new Element("CostType");
				tCostType.setText((String) mOutxml.getValueByName("CostType"));
				tCommissionItem.addContent(tCostType);
				Element tPayType = new Element("PayType");
				tPayType.setText((String) mOutxml.getValueByName("PayType").toString());
				tCommissionItem.addContent(tPayType);
				Element tCurryPayPeriod = new Element("CurryPayPeriod");
				tCurryPayPeriod.setText((String) mOutxml
						.getValueByName("CurryPayPeriod"));
				tCommissionItem.addContent(tCurryPayPeriod);
				
				Element tCoverage = new Element("Coverage");
				Element tCoverageType = new Element("CoverageType");
				tCoverageType.setText((String) mOutxml.getValueByName("CoverageType"));
				tCoverage.addContent(tCoverageType);
				Element tCoverageCode = new Element("CoverageCode");
				tCoverageCode.setText((String) mOutxml.getValueByName("CoverageCode"));
				tCoverage.addContent(tCoverageCode);
				Element tCoverageName = new Element("CoverageName");
				tCoverageName.setText((String) mOutxml.getValueByName("CoverageName"));
				tCoverage.addContent(tCoverageName);
				Element tPremium = new Element("Premium");
				tPremium.setText((String) mOutxml.getValueByName("Premium").toString());
				tCoverage.addContent(tPremium);
				Element tCommissionRate = new Element("CommissionRate");
				tCommissionRate.setText((String) mOutxml.getValueByName(
				"CommissionRate").toString());
				tCoverage.addContent(tCommissionRate);
				Element tCommissionCharge = new Element("CommissionCharge");
				tCommissionCharge.setText((String) mOutxml.getValueByName(
				"CommissionCharge").toString());
				tCoverage.addContent(tCommissionCharge);
				tCommissionItem.addContent(tCoverage);
				
				Element tPolicyCharge = new Element("PolicyCharge");
				String sql="select actype from lacom where agentcom='"+this.mAgentCom+"'";
				ExeSQL aExeSQL = new ExeSQL();
	            String ACTYPE = aExeSQL.getOneValue(sql);
				if("3".equals(cLAChargeSchema.getBranchType())&&!ACTYPE.equals("11")&&ACTYPE!=null&&!ACTYPE.equals(""))
				{
					tPolicyCharge.setText("10");
				}
				else
				{
					tPolicyCharge.setText("");
				}
				tCommissionItem.addContent(tPolicyCharge);
				tCommission.addContent(tCommissionItem);
		    }
		}
		tbody.addContent(tCommission);
		tRootData.addContent(tbody);
		Document tDocument = new Document(tRootData);
		JdomUtil.print(tDocument.getRootElement());
		return DomtoString(tDocument);
	}

	private String DomtoString(Document doc) {
		XMLOutputter xmlout = new XMLOutputter();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		try {
			xmlout.setEncoding("GBK");
			xmlout.output(doc, bo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String xmlStr = bo.toString().replace("GBK", "UTF-8");
		System.out.println("发送报文：" + xmlStr);
		return xmlStr;
	}

	public boolean getResult(LAChargePayBatchLogSchema cLAChargePayBatchLogSchema)
	{
		String tResponseCode ="";
		if("ZX".equals(this.mChargetype))
		{
			tResponseCode = cLAChargePayBatchLogSchema.getZXResponseCode();
		}
		else if("HX".equals(this.mChargetype))
		{
			tResponseCode = cLAChargePayBatchLogSchema.getHXResponseCode();
		}
		if("1".equals(tResponseCode))
		{
			return true;
		}
		return false;
	}
	
	public  HashMap dealDataOfLACharge(LAChargeSet cLAChargeSet,String cContType)
	{
		HashMap tMmap = new HashMap();
		for(int i =1;i<=cLAChargeSet.size();i++)
		{
			String tPoliceNo ="";
			LAChargeSchema tLAChargeSchema = new LAChargeSchema();
			tLAChargeSchema = cLAChargeSet.get(i);
			LACommisionSchema  tLACommisionSchema = new LACommisionSchema();
			LACommisionDB tLACommisionDB = new LACommisionDB();
			tLACommisionDB.setCommisionSN(tLAChargeSchema.getCommisionSN());
			tLACommisionDB.getInfo();
			tLACommisionSchema = tLACommisionDB.getSchema();
			String tContno = tLAChargeSchema.getContNo();
			System.out.println(tLAChargeSchema.getContNo());
			String tgrpcontno = tLAChargeSchema.getGrpContNo();
			System.out.println(tLAChargeSchema.getGrpContNo());
			if ("1".equals(cContType))
			{
				String sql = "select endorsementno,FeeOperationType from LJAGetEndorse b "
					+ " where b.actugetno='"+tLACommisionSchema.getReceiptNo()+"' " 
					+" and b.ManageCom='"+tLACommisionSchema.getManageCom()+"' " 
					+" and b.RiskCode='"+tLACommisionSchema.getRiskCode()+"' "
					+" and b.makedate='"+tLACommisionSchema.getTMakeDate()+"'"
					+" and b.polno='"+tLACommisionSchema.getPolNo()+"' fetch first 1 rows only with ur";
				SSRS tSSRS = new SSRS(); 
				tSSRS =mExeSQL.execSQL(sql);
				if (tSSRS!=null&&tSSRS.getMaxRow()>0) {
					String endorsementno = tSSRS.GetText(1, 1);
					String FeeOperationType = tSSRS.GetText(1, 2);
					mOutxml.setNameAndValue("PolicyType", "02");
					mOutxml.setNameAndValue("PolicyEndorsementNo", (endorsementno+FeeOperationType).replace(" ", ""));
					tPoliceNo="02"+(endorsementno+FeeOperationType).replace(" ", "");
				} 
				else  
				{
					mOutxml.setNameAndValue("PolicyEndorsementNo", tContno);
					tPoliceNo="01"+tContno;
				}
			}
			else if ("0".equals(cContType))
			{
				String sql = "select b.endorsementno,FeeOperationType,sum(getmoney) from LJAGetEndorse b "
					+ " where b.actugetno='"+tLACommisionSchema.getReceiptNo()+"' " 
					+" and b.ManageCom='"+tLACommisionSchema.getManageCom()+"' " 
					+" and b.RiskCode='"+tLACommisionSchema.getRiskCode()+"' "
					+" and b.makedate='"+tLACommisionSchema.getTMakeDate()+"'";
				if(!"ZC".equals(tLACommisionSchema.getTransType()))
				{
					sql+=" and b.FeeOperationType='"+tLACommisionSchema.getTransType()+"' ";
				}
				sql+=" and b.grppolno='"+tLACommisionSchema.getGrpPolNo()+"' ";
				sql+=" group by b.endorsementno,FeeOperationType with ur";
				SSRS tSSRS = new SSRS(); 
				tSSRS =mExeSQL.execSQL(sql);
				if (tSSRS!=null&&tSSRS.getMaxRow()>0) {
					String endorsementno = "";
					String FeeOperationType = "";
					for(int k=1;k<=tSSRS.getMaxRow();k++)
					{
						if(null!=tSSRS.GetText(k, 3)&&!"".equals(tSSRS.GetText(k, 3)))
						{
							double  transmoney = Double.parseDouble(tSSRS.GetText(k, 3));
							if(tLACommisionSchema.getTransMoney()==transmoney)
							{
								endorsementno = tSSRS.GetText(k, 1);
								FeeOperationType = tSSRS.GetText(k, 2);
							}
						}
					}
					if("".equals(endorsementno))
					{
						for(int j=1;j<=tSSRS.getMaxRow();j++)
						{
							if(null!=tSSRS.GetText(j, 3)&&!"".equals(tSSRS.GetText(j, 3)))
							{
								String tSumSQL ="select sum(transmoney) from lacommision b where receiptno ='"+tLACommisionSchema.getReceiptNo()+"' " 
								+" and b.RiskCode='"+tLACommisionSchema.getRiskCode()+"' "
								+" and b.TransType='"+tLACommisionSchema.getTransType()+"' "
								+" and b.tmakedate='"+tLACommisionSchema.getTMakeDate()+"'";
								double sumTransmoney = Double.parseDouble(mExeSQL.getOneValue(tSumSQL));
								double  transmoney = Double.parseDouble(tSSRS.GetText(j, 3));
								if(sumTransmoney==transmoney)
								{
									endorsementno = tSSRS.GetText(j, 1);
									FeeOperationType = tSSRS.GetText(j, 2);
								}
							}
						}
					}
					if("".equals(endorsementno))
					{
						System.out.println("+++++SDFDSDFDFSDFSD");
					}
					mOutxml.setNameAndValue("PolicyType", "02");
					mOutxml.setNameAndValue("PolicyEndorsementNo", (endorsementno+FeeOperationType).replace(" ", ""));
					tPoliceNo="02"+(endorsementno+FeeOperationType).replace(" ", "");
				} 
				else  
				{
					mOutxml.setNameAndValue("PolicyEndorsementNo", tgrpcontno);
					tPoliceNo="01"+tgrpcontno;
				}
			}
			double tprem=tLAChargeSchema.getTransMoney();
			double tCharge =tLAChargeSchema.getCharge();
			//保/批单类型+保/批单号+险种+缴费方式 +手续费比例
			tPoliceNo=tPoliceNo+","+tLAChargeSchema.getRiskCode()+","+tLAChargeSchema.getPayIntv()+","+tLAChargeSchema.getChargeRate()+","+tLAChargeSchema.getCommisionSN().substring(0, 9);
			System.out.println(tPoliceNo);
			if(tMmap.containsKey(tPoliceNo))
			{
				Object count = tMmap.get(tPoliceNo);
				String[] prems = count.toString().split(",");
				double prem = Double.parseDouble(prems[0]);
				double charge = Double.parseDouble(prems[1]);
				tMmap.put(tPoliceNo, (prem+tprem)+","+(tCharge+charge));
			}
			else
			{
				tMmap.put(tPoliceNo, tprem+","+tCharge);
			}
		}
			
		return tMmap;
	}
	public static void main(String[] args) {
		LAChargeCheckBL tLAChargeCheckBL = new LAChargeCheckBL();
		LAChargeDB tLAChargeDB = new LAChargeDB();
		LAChargeSet tLAChargeSet = new LAChargeSet();
		String tStr = "select *  from lacharge where chargetype='51' and branchtype='2' and branchtype2='02' and chargestate='0' and  charge<>0  and managecom='86320000' and tmakedate between '2015-07-16' and '2015-07-30' and agentcom='PI3200000134' with ur";
 		tLAChargeSet = tLAChargeDB.executeQuery(tStr);
		 System.out.println("aaaaaaaaaaaa:"+tStr);
		String chargetype = "HX";
		VData tVData = new VData();
//		tVData.add(tLAChargeSet);
//		tVData.add(chargetype);
		tVData.add(tLAChargeSet);
		tVData.add(chargetype);
		tVData.add("2015080486321200001");
		tVData.add("001");
		tVData.add("0");
		System.out.println("wwwwwww");
		System.out.print(tLAChargeSet.size());
		tLAChargeCheckBL.submitData(tVData, "check");
//		System.out.println(Integer.parseInt("8"));
		// System.out.println("ggggggggggggggg:"+tVData.size());
		
		
//		System.out.println("20150507863200001004".substring(16, 17));
	}
}
