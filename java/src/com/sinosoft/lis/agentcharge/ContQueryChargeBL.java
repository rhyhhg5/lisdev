package com.sinosoft.lis.agentcharge;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.schema.LAChargePayBatchLogSchema;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ContQueryChargeBL {
	private Logger cLogger = Logger.getLogger(getClass());
	   
	/** 往工作流引擎中传输数据的容器 */

    //private LAChargeSchema tLAChargeSchema=new LAChargeSchema();
    private VData mInputData=new VData();
	//传输数据类
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /**
     * 发送报文的保单数据封装类
     */
    private TransferData mTransferData=new TransferData();
    /**
     * 前台传来的LAChargeSchema
     */
    private String mBatchNo ="";
    private String mVoucherRegNo="";
    private String mOperator ="";
    //private ExeSQL mExeSQL=new ExeSQL();
    //private LAChargeSet mLAChargeSet = new LAChargeSet();
	public boolean submitData(VData cInputData, String cOperate) {
	        //首先将数据在本类中做一个备份
	        mInputData = (VData) cInputData.clone();
	        System.out.println("Start ContQueryChargeBL Submit...");
	        if (!getInputData(mInputData, cOperate))
	        {
	            return false;
	        }

	        if (!checkData())
	        {
	            return false;
	        }

	        try {
				if (!dealData())
				{
				    return false;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	       
	        System.out.println("End ContQueryChargeBL Submit...");

	        mInputData = null;
	        return true;
	    }
     private boolean checkData(){
		
		return true;
	}
     private boolean dealData() throws FileNotFoundException{
  		String tresult=queryChargeInfo();	
  		if (!"".equals(tresult) && tresult != null) {
			VData cVData = new VData();
			TransferData cTransferData = new TransferData();
			cTransferData.setNameAndValue("BatchNo", this.mBatchNo);
			cTransferData.setNameAndValue("Operator", this.mOperator);
			cTransferData.setNameAndValue("XML", tresult);
			cVData.add(cTransferData);
			DealLAChargeBackXML tDealLAChargeBackXML = new DealLAChargeBackXML();
			boolean tFlag = tDealLAChargeBackXML.submit(cVData, "");
			if(false==tFlag)
			{
				this.mErrors.copyAllErrors(tDealLAChargeBackXML.mErrors);
			}
			else
			{
				LAChargePayBatchLogSchema tLAChargePayBatchLogSchema = new LAChargePayBatchLogSchema();
				tLAChargePayBatchLogSchema = tDealLAChargeBackXML.getLAChargePayBatchLogSchemaByVoucherRegNo(this.mVoucherRegNo);
				tFlag = getResult(tLAChargePayBatchLogSchema);
			}
			return tFlag;
		} else {
			System.out.println("获取返回报文失败！");
			CError tError = new CError();
			tError.moduleName = "ContInputChargecomChkBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询返回报文获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
  }
     private Document getDocument(String tstr){
  		java.io.Reader in = new StringReader(tstr);  
          Document doc=null;
  		try {
  			doc = (new SAXBuilder()).build(in);
  		} catch (JDOMException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}         
          return doc; 
  	}
     private boolean getInputData(VData cInputData,String cOperate){
  		System.out.println("+==============");
  		//从输入数据中得到所有对象
          mTransferData = (TransferData) cInputData.getObjectByObjectName(
                  "TransferData", 0);
//  		tLAChargeSchema=(LAChargeSchema)cInputData.getObjectByObjectName("LAChargeSchema",0);
          mBatchNo = (String)mTransferData.getValueByName("BatchNo");
          mOperator = (String)mTransferData.getValueByName("Operator");
          String sql ="select VoucherRegNo from LAChargePayBatchLog where batchno ='"+mBatchNo+"' with ur";
          mVoucherRegNo= new ExeSQL().getOneValue(sql);
          System.out.println("eeeeeeeeeeee:"+mVoucherRegNo);
          return true;
  	}
     public String queryChargeInfo(){
  		//根据统一工号调用webservice查询业务员信息
  		String tResponseXml="";
  		long mOldTimeMillis = System.currentTimeMillis();
  		long mCurTimeMillis = mOldTimeMillis;
  	     try
  	        {
  	            RPCServiceClient tRPCServiceClient = new RPCServiceClient();
//  	            String url = "http://10.252.4.69:8080/jszjpt/services/SXGX_WS_SERVER?wsdl";
//  	            String qName = "http://ws.sinosoft.com";
//  	            String method = "ecService";
	  	        String tSQL ="select code,codename from ldcode where codetype='JSAgentComChk' with ur";
  	        	ExeSQL tExeSQL = new ExeSQL();
	  			SSRS tSSRS = tExeSQL.execSQL(tSQL);
	  			String url = "";
	  			String qName = "";
	  			String method = "";
	  			if(0<tSSRS.getMaxRow())
	  			{
	  				for(int i=1;i<=tSSRS.getMaxRow();i++)
	  				{
	  					if("url".equals(tSSRS.GetText(i, 1)))
	  					{
	  						url = tSSRS.GetText(i, 2);
	  					}
	  					else if("qName".equals(tSSRS.GetText(i, 1)))
	  					{
	  						qName = tSSRS.GetText(i, 2);
	  					}
	  					else if("method".equals(tSSRS.GetText(i, 1)))
	  					{
	  						method = tSSRS.GetText(i, 2);
	  					}
	  					
	  				}
	  			}
  	            Options options = tRPCServiceClient.getOptions();
  	            EndpointReference targetEPR = new EndpointReference(url);
  	            options.setTo(targetEPR);
  	            options.setTimeOutInMilliSeconds(120000);//
  	            QName opAddEntry = new QName(qName, method);
  	            String inputXml = getinputXml(); //这里是中介机构校验接口请求报文
  	            //String inputXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Packet type=\"REQUEST\" version=\"1.0\" channel=\"0\"><Head><RequestType>C01</RequestType></Head><Body><Policy><SerialNo>00000000000000000007</SerialNo><prtno>18150206002</prtno><AgencyCode>PC3200000592</AgencyCode><AgencyType>01</AgencyType><ComCode>PICC</ComCode><Coverage><CoverageType>H</CoverageType></Coverage><Coverage><CoverageType>H</CoverageType></Coverage><PolicyApplyDate>2015-02-06 15:23:11</PolicyApplyDate><PolicyFlag>01</PolicyFlag></Policy></Body></Packet>";
  	            String serialNo = "PICCjs_86320000";//这里是外部交易流水号，可为空
  	            Object[] opAddEntryArgs = new Object[] { inputXml,"HX","C13",serialNo };
  	            Class[] classes = new Class[] { String.class,String.class,String.class,String.class };
  	            mCurTimeMillis = System.currentTimeMillis();
  	    		cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

  	    		cLogger.info("开始调用WebService服务！" + url);
  	            tResponseXml = 
  	            (String) tRPCServiceClient.invokeBlocking(opAddEntry, opAddEntryArgs, classes)[0];
  	            mCurTimeMillis = System.currentTimeMillis();
  	    		cLogger.info("调用WebService服务成功！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
  	    		System.out.println("返回的报文："+tResponseXml);
  	        }
  	        catch (Exception e)
  	        {
  	            e.printStackTrace();
  	            CError tError = new CError();
  	            tError.moduleName = "ContQueryChargeBL";
  	            tError.functionName = "getInputData";
  	            tError.errorMessage = "发送校验报文失败!";
  	            this.mErrors.addOneError(e.getMessage());
  	        }
  		return tResponseXml;
  	}
  	
      private String getinputXml(){ 
      	Element tRootData = new Element("Packet");
          tRootData.addAttribute("type", "REQUEST");
          tRootData.addAttribute("version", "1.0");
          Element tHeadData = new Element("Head");
          Element tRequestType = new Element("RequestType");
          tRequestType.setText("C13");
          tHeadData.addContent(tRequestType);
          tRootData.addContent(tHeadData);

          Element tbody=new Element("Body");
          Element tVoucher = new Element("Voucher");
          Element tVoucherRegNo = new Element("VoucherRegNo");
          tVoucherRegNo.setText(mVoucherRegNo);
          //tVoucherRegNo.setText("V0201330009001234567890126");
          tVoucher.addContent(tVoucherRegNo);
          tbody.addContent(tVoucher);
          tRootData.addContent(tbody);
          Document tDocument = new Document(tRootData);
          JdomUtil.print(tDocument.getRootElement());
          return DomtoString(tDocument);   
      }  
 	private String DomtoString(Document doc){
      	XMLOutputter xmlout = new XMLOutputter();
      	ByteArrayOutputStream bo = new ByteArrayOutputStream();
      	try {
      		xmlout.setEncoding("GBK");
  			xmlout.output(doc,bo);
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
      	String xmlStr = bo.toString().replace("GBK", "UTF-8");
      	System.out.println("发送报文："+xmlStr);
      return xmlStr;	
      }
 	
 	public boolean getResult(LAChargePayBatchLogSchema cLAChargePayBatchLogSchema)
	{
 		String  tResponseCode = cLAChargePayBatchLogSchema.getPayResponseCode();
		if("1".equals(tResponseCode))
		{
			return true;
		}
		return false;
	}
 	
      public static void main(String[] args) {
    	  ContQueryChargeBL tDealQueryChargeInfoBL = new ContQueryChargeBL();
      	 //LAChargeDB tLAChargeDB = new LAChargeDB();
      	//String tStr="select * from lacharge where managecom='86320000' and agentcom='PC3200000384' fetch first  rows only with ur";
      	//tDealQueryChargeInfoBL.mLAChargeSet=tLAChargeDB.executeQuery(tStr);
     	//System.out.println("aaaaaaaaaaaa:"+tStr);
    	  String tBatchNo="20150702863200000001";
    	  String mPayStatus="03";
      	VData tVData = new VData();
      	TransferData tTransferData = new TransferData();
      	tTransferData.setNameAndValue("BatchNo",tBatchNo);
      	tTransferData.setNameAndValue("Operator", "001");
      	tVData.add(tTransferData);
      	System.out.println("ffffffffffffff:"+mPayStatus);
//      	for (int i = 1; i <= tDealQueryChargeInfoBL.mLAChargeSet.size(); i++)
//         { 
//      		tDealQueryChargeInfoBL.tLAChargeSchema=tDealQueryChargeInfoBL.mLAChargeSet.get(i);
//      		System.out.println("eeeeeeeeeee"+tDealQueryChargeInfoBL.mLAChargeSet.size());
//      		//tTransferData.setNameAndValue("contno","2200682780");
//        	  tVData.add(tDealQueryChargeInfoBL.tLAChargeSchema);
//        	  System.out.println("ffffffffffffff"+tVData.size());
//         }
     	 tDealQueryChargeInfoBL.submitData(tVData, "check");
      	//System.out.println("ggggggggggggggg:"+tVData.size());
  	}
}
