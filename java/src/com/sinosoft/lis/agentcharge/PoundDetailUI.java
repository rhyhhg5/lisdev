package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

public class PoundDetailUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String AgentCom = null;
    private String ChargeType = null;
    private String BeginDate = null;
    private String EndDate = null;
    private String BranchType = null;

    public PoundDetailUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            // 准备传往后台的数据
            VData vData = new VData();
            if (!prepareOutputData(vData))
            {
                return false;
            }

            PoundDetailBL tPoundDetailBL = new PoundDetailBL();
            System.out.println("Start PoundDetailUI Submit ...");

            if (!tPoundDetailBL.submitData(vData, cOperate))
            {
                if (tPoundDetailBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPoundDetailBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "LACommisionF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tPoundDetailBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "LACommisionF1PUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.addElement(AgentCom);
            vData.addElement(ChargeType);
            vData.addElement(BeginDate);
            vData.addElement(EndDate);
            vData.addElement(BranchType);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        AgentCom = ((String) cInputData.get(0));
        ChargeType = ((String) cInputData.get(1));
        BeginDate = ((String) cInputData.get(2));
        EndDate = ((String) cInputData.get(3));
        BranchType = ((String) cInputData.get(4));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 5));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PoundDetailUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        //LACommisionF1PUI LACommisionF1PUI1 = new LACommisionF1PUI();
    }
}
