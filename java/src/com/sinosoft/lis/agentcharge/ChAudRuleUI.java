/*
 * <p>ClassName: ChAudFewlUI </p>
 * <p>Description: ChAudFewlUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-12-05 09:58:25
 */
package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ChAudRuleUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] agrs)
    {
        String cOperate = "INSERT||MAIN";
        String tStartDate = "2005-07-01";
        String tEndDate = "2005-10-31";
        String tManameCom = "86530000";
        String tChargeType = "11";
        String tBranchType = "2";
        String tBranchType2 = "02";
        String tChargeType2 = "RULE";

        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak002";
        tGlobalInput.ManageCom = "86530000";
        tGlobalInput.ComCode = "mak002";

        cInputData.add(tStartDate);
        cInputData.add(tEndDate);
        cInputData.add(tManameCom);
        cInputData.add(tChargeType);
        cInputData.add(tBranchType);
        cInputData.add(tBranchType2);
        cInputData.add(tChargeType2);
        cInputData.add(tGlobalInput);

        ChAudRuleUI tChAudRuleUI = new ChAudRuleUI();
        boolean actualReturn = tChAudRuleUI.submitData(cInputData, cOperate);
        if(actualReturn)
        {
            System.out.println("处理成功！");
        }
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        ChAudRuleBL tChAudRuleBL= new ChAudRuleBL();
        tChAudRuleBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tChAudRuleBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tChAudRuleBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAGroupUniteUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        return true;
    }
}
