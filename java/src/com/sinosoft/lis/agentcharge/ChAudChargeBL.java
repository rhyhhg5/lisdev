package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LMGrpChargeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LARateChargeSet;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class ChAudChargeBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mChargeType;
    private String mOperate;
    /** 业务处理相关变量 */
//  private LJAPayGrpSchema mLJAPayGrpSchema=new LJAPayGrpSchema();
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();

    public ChAudChargeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        //进行插入数据
        if (mOperate.equals("INSERT||CHARGE"))
        {
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChAudChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ChAudChargeBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //对数据进行修改操作
        if (mOperate.equals("UPDATE||CHARGE"))
        {
            if (!updateData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChAudChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ChAudChargeBL-->updateData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("---updateData---");
        }
        //对数据进行删除操作
        if (mOperate.equals("DELETE||CHARGE"))
        {
            if (!deleteData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChAudChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ChAudChargeBL-->deleteData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("----deleteData---");
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ChAudChargeBL Submit...");
            ChAudChargeBLS tChAudChargeBLS = new ChAudChargeBLS();
            tChAudChargeBLS.submitData(mInputData, mOperate);
            System.out.println("End ChAudChargeBL Submit...");
            //如果有需要处理的错误，则返回
            if (tChAudChargeBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tChAudChargeBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ChAudChargeBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        LAChargeSchema tLAChargeSchema;
        LJAPayGrpDB tLJAPayGrpDB;
     //   LMGrpChargeDB tLMGrpChargeDB;
        for (int i = 1; i <= mLJAPayGrpSet.size(); i++)
        {
            tLJAPayGrpDB = new LJAPayGrpDB();
            tLJAPayGrpDB.setSchema(mLJAPayGrpSet.get(i));
            if (!tLJAPayGrpDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ChAudChargeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "数据查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LACommisionSet tLACommisionSet=new LACommisionSet();
            LACommisionDB tLACommisionDB=new LACommisionDB();
            tLACommisionDB.setGrpPolNo(tLJAPayGrpDB.getGrpPolNo());
            tLACommisionSet=tLACommisionDB.query();
            if(tLACommisionSet.size()<=0)
            {
                CError tError = new CError();
                tError.moduleName = "ChAudChargeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "此保单还没有提数,不能进行计算!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LACommisionSchema tLACommisionSchema=new LACommisionSchema();
            tLACommisionSchema=tLACommisionSet.get(1);
            String tCommisionsn=tLACommisionSchema.getCommisionSN();
//            tLMGrpChargeDB = new LMGrpChargeDB();
//            String tSQL = "select * from LMGrpCharge where PayCountFrom<=" +
//                          tLJAPayGrpDB.getPayCount() + " and PayCountTo>=" +
//                          tLJAPayGrpDB.getPayCount() + " and GrpPolNo='" +
//                          tLJAPayGrpDB.getGrpContNo() + "'";//原来是GrpPolNo()2005-9-22修改
//            LMGrpChargeSet tLMGrpChargeSet = tLMGrpChargeDB.executeQuery(tSQL);
//            System.out.println(tSQL);
            LARateChargeDB tLARateChargeDB=new LARateChargeDB();
            String tSQL = "select * from laratecharge where otherno=" +
            tLJAPayGrpDB.getGrpContNo()+ " and othernotype='3' " ;
            LARateChargeSet tLARateChargeSet=new LARateChargeSet();
            tLARateChargeSet=tLARateChargeDB.executeQuery(tSQL);
            if (tLARateChargeSet.size() <= 0)
            {
                tLAChargeSchema = new LAChargeSchema();
                tLAChargeSchema.setAgentCom(tLJAPayGrpDB.getAgentCom());
                String tType=tLAChargeSchema.getAgentCom().substring(0,2);
                if(tType.equals("PI")||tType.equals("PC")||tType.equals("PN"))
                {
                  tLAChargeSchema.setChargeType("11");
                }
                else if(tType.equals("PY"))
                {
                  tLAChargeSchema.setChargeType("21");
                }
                else if(tType.equals("PJ"))
                {
                  tLAChargeSchema.setChargeType("61");
                }

                tLAChargeSchema.setCommisionSN(tCommisionsn);
                tLAChargeSchema.setBranchType("2");
                tLAChargeSchema.setBranchType2("02");
                tLAChargeSchema.setGrpContNo(tLJAPayGrpDB.getGrpContNo());//原来是GrpPolNo()2005-9-22修改
                tLAChargeSchema.setContNo(tLJAPayGrpDB.getGrpContNo());//原来是GrpPolNo()2005-9-22修改
                tLAChargeSchema.setCalDate(tLJAPayGrpDB.getMakeDate());
                tLAChargeSchema.setWageNo(tLJAPayGrpDB.getMakeDate().substring(
                        0, 4) + tLJAPayGrpDB.getMakeDate().substring(5, 7));
//        tLAChargeSchema.setContNo(tLJAPayGrpDB.getContNo());
                tLAChargeSchema.setGrpPolNo(tLJAPayGrpDB.getGrpPolNo());
                tLAChargeSchema.setMainPolNo(tLJAPayGrpDB.getGrpPolNo());
                tLAChargeSchema.setPolNo(tLJAPayGrpDB.getGrpPolNo());
                tLAChargeSchema.setManageCom(tLJAPayGrpDB.getManageCom());
                tLAChargeSchema.setRiskCode(tLJAPayGrpDB.getRiskCode());
                tLAChargeSchema.setReceiptNo(tLJAPayGrpDB.getPayNo());
                tLAChargeSchema.setTransType(tLJAPayGrpDB.getPayType());
                tLAChargeSchema.setPayCount(tLJAPayGrpDB.getPayCount());
                tLAChargeSchema.setTransMoney(tLJAPayGrpDB.getSumActuPayMoney());
                tLAChargeSchema.setChargeRate(0);
                tLAChargeSchema.setCharge(0);
                tLAChargeSchema.setOperator(mGlobalInput.Operator);
                tLAChargeSchema.setMakeDate(PubFun.getCurrentDate());
                tLAChargeSchema.setMakeTime(PubFun.getCurrentTime());
                tLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
                tLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
                tLAChargeSchema.setChargeState("0");
                mLAChargeSet.add(tLAChargeSchema);
            }
            else
            {
                for (int j = 1; j <= tLARateChargeSet.size(); j++)
                {
                    tLAChargeSchema = new LAChargeSchema();
                    tLAChargeSchema.setAgentCom(tLJAPayGrpDB.getAgentCom());
                    String tType=tLAChargeSchema.getAgentCom().substring(0,2);
                    if(tType.equals("PI")||tType.equals("PC")||tType.equals("PN"))
                    {
                        tLAChargeSchema.setChargeType("11");
                    }
                    else if(tType.equals("PY"))
                    {
                        tLAChargeSchema.setChargeType("21");
                    }
                    else if(tType.equals("PJ"))
                    {
                        tLAChargeSchema.setChargeType("61");
                    }

                    tLAChargeSchema.setBranchType("2");
                    tLAChargeSchema.setBranchType2("02");
                    tLAChargeSchema.setCommisionSN(tCommisionsn);
                    tLAChargeSchema.setCalDate(tLJAPayGrpDB.getMakeDate());
                    tLAChargeSchema.setWageNo(tLJAPayGrpDB.getMakeDate().
                                              substring(0, 4) +
                                              tLJAPayGrpDB.getMakeDate().
                                              substring(5, 7));
//          tLAChargeSchema.setContNo(tLJAPayGrpDB.getContNo());
                    tLAChargeSchema.setGrpContNo(tLJAPayGrpDB.getGrpContNo());
                    tLAChargeSchema.setContNo(tLJAPayGrpDB.getGrpContNo());//原来是GrpPolNo()2005-9-22修改
                    tLAChargeSchema.setGrpPolNo(tLJAPayGrpDB.getGrpPolNo());//原来是GrpPolNo()2005-9-22修改
                    tLAChargeSchema.setMainPolNo(tLJAPayGrpDB.getGrpPolNo());
                    tLAChargeSchema.setPolNo(tLJAPayGrpDB.getGrpPolNo());
                    tLAChargeSchema.setManageCom(tLJAPayGrpDB.getManageCom());
                    tLAChargeSchema.setRiskCode(tLJAPayGrpDB.getRiskCode());
                    tLAChargeSchema.setReceiptNo(tLJAPayGrpDB.getPayNo());
                    tLAChargeSchema.setTransType(tLJAPayGrpDB.getPayType());
                    tLAChargeSchema.setPayCount(tLJAPayGrpDB.getPayCount());
                    tLAChargeSchema.setTransMoney(tLJAPayGrpDB.
                                                  getSumActuPayMoney());
                    tLAChargeSchema.setChargeRate(tLARateChargeSet.get(j).
                                                  getRate());
                    tLAChargeSchema.setCharge(tLJAPayGrpDB.getSumActuPayMoney() *
                                              tLARateChargeSet.get(j).
                                              getRate());
                    tLAChargeSchema.setOperator(mGlobalInput.Operator);
                    tLAChargeSchema.setMakeDate(PubFun.getCurrentDate());
                    tLAChargeSchema.setMakeTime(PubFun.getCurrentTime());
                    tLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
                    tLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
                    tLAChargeSchema.setChargeState("0");
                    mLAChargeSet.add(tLAChargeSchema);
                }
            }
        }
//    mLJAPayGrpSchema.setOperator(this.mGlobalInput.Operator);
//    mLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
//    mLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
//    mLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
//    mLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
//    LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
//    tLJAPayGrpDB.setGrpPolNo(mLJAPayGrpSchema.getGrpPolNo());
//    tLJAPayGrpDB.setPayCountFrom(mLJAPayGrpSchema.getPayCountFrom());
//    tLJAPayGrpDB.setPayCountTo(mLJAPayGrpSchema.getPayCountTo());
//    tLJAPayGrpDB.setChargeType(mLJAPayGrpSchema.getChargeType());
//    if (!tLJAPayGrpDB.getInfo())
//    {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "ChAudChargeBL";
//      tError.functionName = "updateData";
//      tError.errorMessage ="数据查询失败!";
//      this.mErrors.addOneError(tError) ;
//      return false;
//    }
//    mLJAPayGrpSchema.setOperator(this.mGlobalInput.Operator);
//    mLJAPayGrpSchema.setMakeDate(tLJAPayGrpDB.getMakeDate());
//    mLJAPayGrpSchema.setMakeTime(tLJAPayGrpDB.getMakeTime());
//    mLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
//    mLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLJAPayGrpSet.set((LJAPayGrpSet) cInputData.getObjectByObjectName(
                "LJAPayGrpSet", 0));
//    this.mLJAPayGrpSchema.setSchema((LJAPayGrpSchema)cInputData.getObjectByObjectName("LJAPayGrpSchema",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
      //  mChargeType=cInputData.get(2).toString();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ChAudChargeBLQuery Submit...");
        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
//    tLJAPayGrpDB.setSchema(this.mLJAPayGrpSchema);
        //this.mLJAPayGrpSet=tLJAPayGrpDB.query();
        //this.mResult.add(this.mLJAPayGrpSet);
        System.out.println("End ChAudChargeBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLJAPayGrpDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChAudChargeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.clear();
//      this.mInputData.add(this.mLJAPayGrpSchema);
            this.mInputData.add(this.mLAChargeSet);
            mResult.clear();
//      mResult.add(this.mLJAPayGrpSchema);
            mResult.add(this.mLJAPayGrpSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChAudChargeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    public static void main(String[] args)
    {
        ChAudChargeBL chAudChargeBL1 = new ChAudChargeBL();
        VData tVData = new VData();
        LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        tLJAPayGrpSchema.setAgentCom("11091100000108");
        tLJAPayGrpSchema.setGrpPolNo("86110020030220000145");
        tLJAPayGrpSchema.setPayNo("86110020030320000340");
        tLJAPayGrpSchema.setPayType("ZC");
        tLJAPayGrpSchema.setPayCount(1);
        tLJAPayGrpSet.add(tLJAPayGrpSchema);
        tVData.addElement(tLJAPayGrpSet);
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86110000";
        tGI.Operator = "aa";
        tVData.addElement(tGI);
        chAudChargeBL1.submitData(tVData, "INSERT||CHARGE");
    }
}
