/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-19 09:58:25
 */
package com.sinosoft.lis.agentcharge;

import java.sql.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class ChAudFewBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionSet mUpdateLACommisionSet = new LACommisionSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
    private String mChargeType = "";     // 手续费结算类型
    private String mChargeType2 = "";    // 验证是否是特殊业务
    private String mMark = "1";          // 结算过的保单设置标记
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理前进行验证
        if (!check()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"") ;
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            buildError("submitData","保存手续费数据失败！");
            return false;
        }

        return true;
    }

    /**
     * 进行后台检验
     * @return boolean
     */
    private boolean check()
    {
        /* ↓ *** Delete *** 2005-12-06 *** LiuHao **********************
        // 处理特殊业务时，进行此次验证
        if("FEW".equals(this.mChargeType2))
        {
            // 验证是否结算过
            for (int i = 1; i <= this.mLACommisionSet.size(); i++)
            {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = mLACommisionSet.get(i);

                String tSQL = "";
                tSQL = "select count(*) from lacharge where grpcontno='" +
                       tLACommisionSchema.getGrpContNo() + "'";

                int tDataCount = execQuery(tSQL);
                if (tDataCount < 0) {
                    buildError("check", "查询数据库报错！");
                    return false;
                }
                if (tDataCount != 0) {
                    buildError("check", "合同号为["
                               + tLACommisionSchema.getGrpContNo()
                               + "]的保单已经结算过了！请确认！");
                    return false;
                }
            }
        }
        * ↑ *** Delete *** 2005-12-06 *** LiuHao **********************/

       // 进行保单是否完全结算的验证
       if("FEW".equals(this.mChargeType2))
       {
           for(int i = 1; i <= this.mLACommisionSet.size(); i++)
           {
               LACommisionSchema tLACommisionSchema = new LACommisionSchema();
               tLACommisionSchema = mLACommisionSet.get(i);

               String tSQL = "";
               tSQL = "select count(*) from lacommision where grpcontno='"
                    + tLACommisionSchema.getGrpContNo() + "' and P7 <> "
                    + this.mMark;
               System.out.println(tSQL);
               int tDataCount = execQuery(tSQL);
               if (tDataCount < 0)
               {
                  buildError("check", "查询数据库报错！");
                  return false;
               }
               if(tDataCount == 0)
               {
                   buildError("check", "合同号为["
                               + tLACommisionSchema.getGrpContNo()
                               + "]的保单已经完全结算过了！请确认！");
                   return false;
               }
           }
       }

        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        // 循环处理每个特殊业务
        for(int i=1;i<=this.mLACommisionSet.size();i++)
        {
            String tGrpContNo = this.mLACommisionSet.get(i).getGrpContNo();
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setGrpContNo(tGrpContNo);
            // 取得提数表里的数据
            tLACommisionSet = tLACommisionDB.query();
            if (tLACommisionSet.size() == 0)
            {
                buildError("dealData","查询保单数据失败！");
                return false;
            }

//            // 设置基础数据
//            tLAChargeSchema = setLAChargeData(tLACommisionSet);
//            if(null == tLAChargeSchema)
//            {
//                return false;
//            }
            // 进行手续费计算  （多险种）
            if(!dealAccountCharge(tLACommisionSet,tLAChargeSchema))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 设置手续费基础数据
     * @param pmLACommisionSet LACommisionSet
     * @return LAChargeSchema
     */
    private LAChargeSchema setLAChargeData(LACommisionSchema pmLACommisionSchema)
    {
        LAChargeSchema tLAChargeSchema = new LAChargeSchema();

        tLAChargeSchema.setAgentCom(pmLACommisionSchema.getAgentCom());
        tLAChargeSchema.setChargeType(this.mChargeType);
        tLAChargeSchema.setPolNo(pmLACommisionSchema.getPolNo());
        if(null == pmLACommisionSchema.getReceiptNo() || "".equals(pmLACommisionSchema.getReceiptNo()))
        {
            buildError("setLAChargeData",
                       "保单号:[" + pmLACommisionSchema.getPolNo() +
                       "],查不到此保单的保单收据号，请确认！");
            return null;
        }
        tLAChargeSchema.setReceiptNo(pmLACommisionSchema.getReceiptNo());
        tLAChargeSchema.setCommisionSN(pmLACommisionSchema.getCommisionSN());
        tLAChargeSchema.setWageNo(pmLACommisionSchema.getWageNo());
        tLAChargeSchema.setGrpContNo(pmLACommisionSchema.getGrpContNo());
        tLAChargeSchema.setGrpPolNo(pmLACommisionSchema.getGrpPolNo());
        tLAChargeSchema.setContNo(pmLACommisionSchema.getContNo());
        tLAChargeSchema.setBranchType(pmLACommisionSchema.getBranchType());
        tLAChargeSchema.setBranchType2(pmLACommisionSchema.getBranchType2());
        tLAChargeSchema.setCalDate(pmLACommisionSchema.getCalDate());
        tLAChargeSchema.setMainPolNo(pmLACommisionSchema.getMainPolNo());
        tLAChargeSchema.setManageCom(pmLACommisionSchema.getManageCom());
        tLAChargeSchema.setRiskCode(pmLACommisionSchema.getRiskCode());
        tLAChargeSchema.setTransType(pmLACommisionSchema.getTransType());
        tLAChargeSchema.setPayCount(pmLACommisionSchema.getPayCount());
        tLAChargeSchema.setChargeState("0");
        tLAChargeSchema.setOperator(this.mGlobalInput.Operator);
        tLAChargeSchema.setMakeDate(currentDate);
        tLAChargeSchema.setMakeTime(currentTime);
        tLAChargeSchema.setModifyDate(currentDate);
        tLAChargeSchema.setModifyTime(currentTime);

        return tLAChargeSchema;
    }

    /**
     * 手续费计算
     * @param pmLACommisionSet LACommisionSet
     * @return boolean
     */
    private boolean dealAccountCharge(LACommisionSet pmLACommisionSet,
                                      LAChargeSchema pmLAChargeSchema)
    {
        double tRageCharge = 0.00;
        double tCharge = 0.00;
        double tTransMoney = 0.00;
        String tPrtNo = "";
        LAChargeSet tLAChargeSet = new LAChargeSet();

        // 1、先判断新契约是否录入手续费比例
        // 取得保单的印刷号
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContDB.setGrpContNo(pmLACommisionSet.get(1).getGrpContNo());
        // 查询新契约保单信息
        if(!tLCGrpContDB.getInfo())
        {
            buildError("dealAccountCharge","查询保单印刷号失败！");
            return false;
        }
        tPrtNo = tLCGrpContDB.getPrtNo();  // 取得保单印刷号

        // 取得新单录入的手续费比例
        tRageCharge = getRateCharge(tPrtNo,"0","0000");
        if(tRageCharge == -100)
        {
            buildError("dealAccountCharge","查询新契约手续费比例出错！");
            return false;
        }

        for (int i = 1; i <= pmLACommisionSet.size(); i++)
        {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();

            tLACommisionSchema = pmLACommisionSet.get(i);

            // ↓ *** add *** LiuHao *** 2005-12-06 ***************
            // 进行LACommision里当前数据已结算的验证
            if(tLACommisionSchema.getP7() == 1)
            {
                // 当前数据已经结算过不再作结算操作
                System.out.println("["+tLACommisionSchema.getCommisionSN()+"]计算过了！");
                continue;
            }
            // ↑ *** add *** LiuHao *** 2005-12-06 ***************

            // 保存基础数据
            tLAChargeSchema = setLAChargeData(tLACommisionSchema);
            if(null == tLAChargeSchema)
            {
                return false;
            }

            // 如果新契约进行了手续费录入 记录手续费相关信息
            if(tRageCharge >= 0)
            {
                tLAChargeSchema.setTransMoney(tLACommisionSchema.getTransMoney());
                tLAChargeSchema.setChargeRate(tRageCharge);
                tLAChargeSchema.setCharge(tLACommisionSchema.getTransMoney() * tRageCharge);
            }

            // 如果新契约没有进行手续费记录  查询框架协议手续费比例进行计算  并记录手续费信息
            if (tRageCharge == -1)
            {
                // 取得符合条件的协议号
                String tProtoColNo = getProtoColNo(tLACommisionSchema.
                                                   getAgentCom(),
                                                   tLACommisionSchema.getTMakeDate());
                System.out.println("手续费遵循的协议的协议号码是：[" + tProtoColNo + "]");
                double tRiskRate = getRateCharge(tProtoColNo, "3",
                                                 tLACommisionSchema.getRiskCode());
                if (tRiskRate < 0)
                {
                    buildError("dealAccountCharge",
                               "查询框架协议手续费比例失败！请确认保单缴费时间是否在协议期间内。");
                    return false;
                }

                // 记录手续费信息
                tLAChargeSchema.setTransMoney(tLACommisionSchema.getTransMoney());
                tLAChargeSchema.setChargeRate(tRiskRate);
                tLAChargeSchema.setCharge(tLACommisionSchema.getTransMoney() * tRiskRate);
            }
            tLAChargeSet.add(tLAChargeSchema);

            // 为参加结算的保单设置标记
            tLACommisionSchema.setP7(this.mMark);
            this.mUpdateLACommisionSet.add(tLACommisionSchema);
        }

        this.mLAChargeSet.add(tLAChargeSet);
        /*
        // 正常取得新契约手续费比例  进行手续费计算并保存
        if(tRageCharge >= 0)
        {
            // 把保费全加起来  统一计算手续费
            for (int i = 1; i <= pmLACommisionSet.size(); i++)
            {
                tTransMoney = tTransMoney +
                              pmLACommisionSet.get(i).getTransMoney();
            }
            // 计算手续费
            tCharge = tTransMoney * tRageCharge;
            // 保存手续费计算结果
            pmLAChargeSchema.setCharge(tCharge);
            // 保存手续费数据
            mLAChargeSet.add(pmLAChargeSchema);
        }
        // 2、新契约没有录入保单手续费比例  以框架协议中的手续费比例计算
        if(tRageCharge == -1)
        {// 新契约没有录入手续费比例
            for(int j=1;j<=pmLACommisionSet.size();j++)
            {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = pmLACommisionSet.get(j);
                String tRiskCode = tLACommisionSchema.getRiskCode();
                // 取得符合条件的协议号
                String tProtoColNo = getProtoColNo(tLACommisionSchema.
                        getAgentCom(), tLACommisionSchema.getTMakeDate());
                System.out.println("手续费遵循的协议的协议号码是：["+tProtoColNo+"]");
                double tRiskRate = getRateCharge(tProtoColNo,"3", tRiskCode);
                if(tRiskRate < 0)
                {
                    buildError("dealAccountCharge","查询框架协议手续费比例失败！");
                    return false;
                }
                tCharge = tCharge + tLACommisionSchema.getTransMoney() * tRiskRate;
            }
            // 保存手续费计算结果
            pmLAChargeSchema.setCharge(tCharge);
            // 保存手续费数据
            mLAChargeSet.add(pmLAChargeSchema);
        }*/

        return true;
    }

    /**
     * 取得协议号码
     * @param pmAgentCom String
     * @param pmTMakeDate String
     * @return String
     */
    private String getProtoColNo(String pmAgentCom,String pmTMakeDate)
    {
        String tSQL = "";

        tSQL  = "select * from LACont";
        tSQL += " where AgentCom = '" + pmAgentCom + "'";
        tSQL += "   and StartDate <= '" + pmTMakeDate + "'";
        tSQL += "   and EndDate >= '" + pmTMakeDate + "'";

        LAContDB tLAContDB = new LAContDB();
        LAContSet tLAContSet = new LAContSet();
        tLAContSet = tLAContDB.executeQuery(tSQL);

        if(tLAContSet.size() == 0)
        {
            buildError("dealAccountCharge","查询框架协议号码失败！");
            return "";
        }
        System.out.println("检索到协议的条目数："+tLAContSet.size());

        return tLAContSet.get(1).getProtocolNo();
    }

    /**
     * 取得手续费比例
     * @param pmObjNo String
     * @param pmObjType String   0：按保单查询  3：按框架协议查询
     * @param pmRiskCode String
     * @return int   -100 表示访问数据库失败  -1 表示新契约没有录入手续费比例
     */
    private double getRateCharge(String pmObjNo,String pmObjType,String pmRiskCode)
    {
        double tRtValue = 0.00;
        LARateChargeDB tLARateChargeDB = new LARateChargeDB();
        LARateChargeSet tLARateChargeSet = new LARateChargeSet();

        tLARateChargeDB.setOtherNo(pmObjNo);
        tLARateChargeDB.setOtherNoType(pmObjType);
        tLARateChargeDB.setRiskCode(pmRiskCode);

        tLARateChargeSet = tLARateChargeDB.query();

        if(tLARateChargeDB.mErrors.needDealError())
        {// 访问数据库出错
            return -100;
        }

        if(tLARateChargeSet.size() == 0)
        {// 没有找到符合条件的数据
            return -1;
        }

        tRtValue = tLARateChargeSet.get(1).getRate();

        return tRtValue;
    }

    /**
     * 准备提交后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try{
            mMap.put(this.mLAChargeSet, "INSERT");
            mMap.put(this.mUpdateLACommisionSet, "UPDATE");
            this.mOutputDate.add(mMap);
        }catch (Exception ex)
        {
            buildError("prepareOutputData","准备提交后台的数据时出错！");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentEvaluateBL.getInputData.........");
        this.mChargeType = (String) cInputData.getObject(0);
        this.mChargeType2 = (String) cInputData.getObject(1);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLACommisionSet.set((LACommisionSet) cInputData.getObjectByObjectName(
                "LACommisionSet", 0));

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ChAudFewBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
}
