package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class GrpAgentComBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private String[] mGrpPolNo;
    private String mAgentCom;

    public GrpAgentComBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        //进行插入数据
        if (mOperate.equals("INSERT||CHARGE"))
        {
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpAgentComBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败GrpAgentComBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start GrpAgentComBL Submit...");
        GrpAgentComBLS tGrpAgentComBLS = new GrpAgentComBLS();
        tGrpAgentComBLS.submitData(mInputData, mOperate);
        System.out.println("End GrpAgentComBL Submit...");
        //如果有需要处理的错误，则返回
        if (tGrpAgentComBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpAgentComBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGrpPolNo = (String[]) cInputData.getObject(0);
        this.mAgentCom = (String) cInputData.getObjectByObjectName("String", 0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start GrpAgentComBLQuery Submit...");
        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
//    tLJAPayGrpDB.setSchema(this.mLJAPayGrpSchema);
        //this.mLJAPayGrpSet=tLJAPayGrpDB.query();
        //this.mResult.add(this.mLJAPayGrpSet);
        System.out.println("End GrpAgentComBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLJAPayGrpDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGrpPolNo);
            mInputData.add(this.mAgentCom);
            mInputData.add(this.mGlobalInput);
            mResult.clear();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    public static void main(String[] args)
    {
        GrpAgentComBL GrpAgentComBL1 = new GrpAgentComBL();
        VData tVData = new VData();
        LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        tLJAPayGrpSchema.setAgentCom("11091100000108");
        tLJAPayGrpSchema.setGrpPolNo("86110020030220000145");
        tLJAPayGrpSchema.setPayNo("86110020030320000340");
        tLJAPayGrpSchema.setPayType("ZC");
        tLJAPayGrpSchema.setPayCount(1);
        tLJAPayGrpSet.add(tLJAPayGrpSchema);
        tVData.addElement(tLJAPayGrpSet);
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86110000";
        tGI.Operator = "aa";
        tVData.addElement(tGI);
        GrpAgentComBL1.submitData(tVData, "INSERT||CHARGE");
    }
}
