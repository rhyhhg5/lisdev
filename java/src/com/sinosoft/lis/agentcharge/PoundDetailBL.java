package com.sinosoft.lis.agentcharge;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PoundDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

//取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String AgentCom = null;
    private String ChargeType = null;
    private String BeginDate = null;
    private String EndDate = null;
    private String BranchType = null;
//输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
//业务处理相关变量
    /** 全局数据 */

    public PoundDetailBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        AgentCom = ((String) cInputData.get(0));
        ChargeType = ((String) cInputData.get(1));
        BeginDate = ((String) cInputData.get(2));
        EndDate = ((String) cInputData.get(3));
        BranchType = ((String) cInputData.get(4));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 5));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();
        if (ChargeType.equals("3"))
        {
            msql = "select a.AgentCode,a.Name,b.PolNo,b.P11,b.RiskCode,e.Transmoney,b.PayYears,b.paycount,c.agentcom,c.name,e.Charge"
                   + " from LAAgent a,lacommision b,lacom c,lacharge e where a.AgentCode=b.AgentCode and b.branchtype='3' "
                   + " and b.agentcom=c.agentcom and b.CommisionSN=e.CommisionSN and b.managecom like '" +
                   mGlobalInput.ManageCom + "%'"
                   + " and b.AgentCom like '" + AgentCom +
                   "%' and e.caldate>='" + BeginDate + "' and e.caldate<='" +
                   EndDate + "'";
        }
        else if (ChargeType.equals("0") || ChargeType.equals("1") ||
                 ChargeType.equals("2"))
        {
            int Type = Integer.parseInt(ChargeType) + 2;
            msql = "select a.AgentCode,a.Name,b.PolNo,b.P11,b.RiskCode,e.Transmoney,b.PayYears,b.paycount,c.agentcom,c.name,e.Charge"
                   + " from LAAgent a,lacommision b,lacom c,lacharge e where a.AgentCode=b.AgentCode and b.branchtype='3' "
                   + " and b.agentcom=c.agentcom and b.CommisionSN=e.CommisionSN and b.managecom like '" +
                   mGlobalInput.ManageCom + "%'"
                   + " and e.ChargeType like '" + Type
                   + "%' and b.AgentCom like '" + AgentCom +
                   "%' and e.caldate>='" + BeginDate + "' and e.caldate<='" +
                   EndDate + "'";
        }
        else if (ChargeType.equals("55"))
        {
            msql = "select a.AgentCode,a.Name,b.PolNo,b.P11,b.RiskCode,e.Transmoney,b.PayYears,b.paycount,c.agentcom,c.name,e.Charge"
                   + " from LAAgent a,lacommision b,lacom c,lacharge e where a.AgentCode=b.AgentCode and b.branchtype='3' "
                   + " and b.agentcom=c.agentcom and e.agentcom=b.agentcom and b.CommisionSN=e.CommisionSN and e.ChargeType like '5%%'"
                   + " and e.agentcom ='" + AgentCom + "'"
                   + " and e.caldate>='" + BeginDate + "' and e.caldate<='" +
                   EndDate + "'"
                   + " and b.managecom like '" + mGlobalInput.ManageCom + "%'"
                   + " order by e.agentcom,b.polno,e.chargetype";
        }
        else if (ChargeType.equals("51") || ChargeType.equals("52") ||
                 ChargeType.equals("53") || ChargeType.equals("54"))
        {
            msql = "select a.AgentCode,a.Name,b.PolNo,b.P11,b.RiskCode,e.Transmoney,b.PayYears,b.paycount,c.agentcom,c.name,e.Charge"
                   + " from LAAgent a,lacommision b,lacom c,lacharge e where a.AgentCode=b.AgentCode and b.branchtype='3' "
                   + " and b.agentcom=c.agentcom and e.agentcom=b.agentcom and b.CommisionSN=e.CommisionSN and e.ChargeType ='" +
                   ChargeType + "'"
                   + " and e.agentcom ='" + AgentCom + "'"
                   + " and e.caldate>='" + BeginDate + "' and e.caldate<='" +
                   EndDate + "'"
                   + " and b.managecom like '" + mGlobalInput.ManageCom + "%'"
                   + " order by e.agentcom,b.polno,e.chargetype";
        }
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("BANK");
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            strArr = new String[11];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        strArr = new String[13];
        strArr[0] = "保单号";
        strArr[1] = "印刷号";
        strArr[2] = "投保人";
        strArr[3] = "签单日期";
        strArr[4] = "生效日期";
        strArr[5] = "回单日期";
        strArr[6] = "保费";
        strArr[7] = "代理人编码";
        strArr[8] = "姓名";
        strArr[9] = "网点名称";
        strArr[10] = "个人提佣金额";
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PoundDetailPrt.vts", "printer"); //最好紧接着就初始化xml文档
        xmlexport.addListTable(tlistTable, strArr);
        mResult.addElement(xmlexport);
        return true;
    }

    public static void main(String[] args)
    {
        //LACommisionF1PBL LACommisionF1PBL1 = new LACommisionF1PBL();
    }
}
