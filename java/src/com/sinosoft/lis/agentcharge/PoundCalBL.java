package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAComChargeSchema;
import com.sinosoft.lis.vschema.LAComChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

public class PoundCalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAComChargeSchema mLAComChargeSchema = new LAComChargeSchema();
    private LAComChargeSet mLAComChargeSet = new LAComChargeSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
    public PoundCalBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PoundCalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败PoundCalBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start PoundCalBL Submit...");
        PoundCalBLS tPoundCalBLS = new PoundCalBLS();
        tPoundCalBLS.submitData(mInputData, cOperate);
        System.out.println("End PoundCalBL Submit...");
        //如果有需要处理的错误，则返回
        if (tPoundCalBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPoundCalBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "PoundCalBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        if (mLAComChargeSchema.getChargeType().equals("0"))
        {
            cal0();
        }
        else if (mLAComChargeSchema.getChargeType().equals("1"))
        {
            cal1();
        }
        else if (mLAComChargeSchema.getChargeType().equals("2"))
        {
            cal2();
        }
        else if (mLAComChargeSchema.getChargeType().equals("3"))
        {
            cal0();
            cal1();
            cal2();
        }
        else if (mLAComChargeSchema.getChargeType().equals("55"))
        {
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();

            String aSQL = "select ManageCom,sum(charge),decode(ChargeType,'51','0','52','1','53','2','54','3') from LACharge where "
                          + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                          "'"
                          + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                          "' and CalDate<='"
                          + mLAComChargeSchema.getStartEnd() +
                          "' and AgentCom like '"
                          + mLAComChargeSchema.getAgentCom() +
                          "%' and ChargeType like '5%' group by managecom,ChargeType";

            ExeSQL aExeSQL = new ExeSQL();
            SSRS aSSRS = aExeSQL.execSQL(aSQL);
            LAComChargeSchema tLAComChargeSchema;
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                tLAComChargeSchema = new LAComChargeSchema();
                tLAComChargeSchema.setManageCom(aSSRS.GetText(a, 1));
                tLAComChargeSchema.setSumMoney(aSSRS.GetText(a, 2));
                tLAComChargeSchema.setCurrMoney(aSSRS.GetText(a, 2));
                tLAComChargeSchema.setChargeType(aSSRS.GetText(a, 3));
                if (this.mOperate.equals("PRINT"))
                {
                    tLAComChargeSchema.setState("1");
                }
                else
                {
                    tLAComChargeSchema.setState("2");
                }
                tLAComChargeSchema.setMakeDate(currentDate);
                tLAComChargeSchema.setMakeTime(currentTime);
                tLAComChargeSchema.setModifyDate(currentDate);
                tLAComChargeSchema.setModifyTime(currentTime);
                tLAComChargeSchema.setGetDate(currentDate);
                tLAComChargeSchema.setOperator(mGlobalInput.Operator);
                tLAComChargeSchema.setOperator2(mGlobalInput.Operator);
                tLAComChargeSchema.setBranchType(mLAComChargeSchema.
                                                 getBranchType());
                tLAComChargeSchema.setStartDate(mLAComChargeSchema.getStartDate());
                tLAComChargeSchema.setStartEnd(mLAComChargeSchema.getStartEnd());
                tLAComChargeSchema.setAgentCom(mLAComChargeSchema.getAgentCom());
                mLAComChargeSet.add(tLAComChargeSchema);
            }

        }
        else if (mLAComChargeSchema.getChargeType().substring(0, 1).equals("5") &&
                 !mLAComChargeSchema.getChargeType().equals("55"))
        {
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();

            String aSQL = "select ManageCom,sum(charge),decode(ChargeType,'51','0','52','1','53','2','54','3') from LACharge where "
                          + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                          "'"
                          + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                          "' and CalDate<='"
                          + mLAComChargeSchema.getStartEnd() +
                          "' and AgentCom like '"
                          + mLAComChargeSchema.getAgentCom() +
                          "%' and ChargeType = '" +
                          mLAComChargeSchema.getChargeType() +
                          "' group by managecom,ChargeType";

            ExeSQL aExeSQL = new ExeSQL();
            SSRS aSSRS = aExeSQL.execSQL(aSQL);
            LAComChargeSchema tLAComChargeSchema;
            tLAComChargeSchema = new LAComChargeSchema();
            tLAComChargeSchema.setManageCom(aSSRS.GetText(1, 1));
            tLAComChargeSchema.setSumMoney(aSSRS.GetText(1, 2));
            tLAComChargeSchema.setCurrMoney(aSSRS.GetText(1, 2));
            tLAComChargeSchema.setChargeType(aSSRS.GetText(1, 3));
            if (this.mOperate.equals("PRINT"))
            {
                tLAComChargeSchema.setState("1");
            }
            else
            {
                tLAComChargeSchema.setState("2");
            }
            tLAComChargeSchema.setMakeDate(currentDate);
            tLAComChargeSchema.setMakeTime(currentTime);
            tLAComChargeSchema.setModifyDate(currentDate);
            tLAComChargeSchema.setModifyTime(currentTime);
            tLAComChargeSchema.setOperator(mGlobalInput.Operator);
            tLAComChargeSchema.setOperator2(mGlobalInput.Operator);
            tLAComChargeSchema.setBranchType(mLAComChargeSchema.getBranchType());
            tLAComChargeSchema.setStartDate(mLAComChargeSchema.getStartDate());
            tLAComChargeSchema.setStartEnd(mLAComChargeSchema.getStartEnd());
            tLAComChargeSchema.setAgentCom(mLAComChargeSchema.getAgentCom());
            mLAComChargeSet.add(tLAComChargeSchema);

        }
        tReturn = true;
        return tReturn;
    }

    private boolean cal0()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        String aSQL = "select ManageCom,sum(charge) from LACharge where "
                      + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                      "'"
                      + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                      "' and CalDate<='"
                      + mLAComChargeSchema.getStartEnd() +
                      "' and AgentCom like '"
                      + mLAComChargeSchema.getAgentCom() +
                      "%' and ChargeType='21'  and chargestate='0'  group by managecom";

        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = aExeSQL.execSQL(aSQL);
        LAComChargeSchema tLAComChargeSchema;
        tLAComChargeSchema = new LAComChargeSchema();
        tLAComChargeSchema.setManageCom(aSSRS.GetText(1, 1));
        tLAComChargeSchema.setSumMoney(aSSRS.GetText(1, 2));
        tLAComChargeSchema.setCurrMoney(aSSRS.GetText(1, 2));
        tLAComChargeSchema.setChargeType("0");
        if (this.mOperate.equals("PRINT"))
        {
            tLAComChargeSchema.setState("1");
        }
        else
        {
            tLAComChargeSchema.setState("2");
        }
        tLAComChargeSchema.setMakeDate(currentDate);
        tLAComChargeSchema.setMakeTime(currentTime);
        tLAComChargeSchema.setModifyDate(currentDate);
        tLAComChargeSchema.setModifyTime(currentTime);
        tLAComChargeSchema.setGetDate(currentDate);
        tLAComChargeSchema.setOperator(mGlobalInput.Operator);
        tLAComChargeSchema.setOperator2(mGlobalInput.Operator);
        tLAComChargeSchema.setBranchType(mLAComChargeSchema.getBranchType());
        tLAComChargeSchema.setStartDate(mLAComChargeSchema.getStartDate());
        tLAComChargeSchema.setStartEnd(mLAComChargeSchema.getStartEnd());
        tLAComChargeSchema.setAgentCom(mLAComChargeSchema.getAgentCom());
        mLAComChargeSet.add(tLAComChargeSchema);
        String tSQL = "select * from LACharge where "
                      + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                      "'"
                      + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                      "' and CalDate<='"
                      + mLAComChargeSchema.getStartEnd() +
                      "' and AgentCom like '"
                      + mLAComChargeSchema.getAgentCom() +
                      "%' and ChargeType='21' and chargestate='0' group by managecom";
        LAChargeDB tLAChargeDB=new LAChargeDB();
        LAChargeSet tLAChargeSet=new LAChargeSet();
        tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
//        if(tLAChargeSet==null||tLAChargeSet.size()<=0)
//        {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLAChargeSet.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "PoundCalBL";
//                tError.functionName = "cal0";
//                tError.errorMessage = "手续费查询失败!";
//                this.mErrors.addOneError(tError);
//                return false;
//        }
        for(int i=1;i<=tLAChargeSet.size();i++)
        {
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema=tLAChargeSet.get(i);
            tLAChargeSchema.setOperator(mGlobalInput.Operator);
            tLAChargeSchema.setMakeDate(PubFun.getCurrentDate());
            tLAChargeSchema.setMakeTime(PubFun.getCurrentTime());
            tLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
            tLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
            tLAChargeSchema.setChargeState("1");
            mLAChargeSet.add(tLAChargeSchema);


        }

        return true;
    }

    private boolean cal1()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        String bSQL = "select ManageCom,sum(charge) from LACharge where "
                      + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                      "' "
                      + "and CalDate>='" + mLAComChargeSchema.getStartDate() +
                      "' and CalDate<='"
                      + mLAComChargeSchema.getStartEnd() +
                      "' and AgentCom like '"
                      + mLAComChargeSchema.getAgentCom() +
                      "%' and ChargeType like '3%' group by managecom";

        ExeSQL bExeSQL = new ExeSQL();
        SSRS bSSRS = bExeSQL.execSQL(bSQL);
        LAComChargeSchema tLAComChargeSchema;
        tLAComChargeSchema = new LAComChargeSchema();
        tLAComChargeSchema.setManageCom(bSSRS.GetText(1, 1));
        tLAComChargeSchema.setSumMoney(bSSRS.GetText(1, 2));
        tLAComChargeSchema.setCurrMoney(bSSRS.GetText(1, 2));
        tLAComChargeSchema.setChargeType("1");
        if (this.mOperate.equals("PRINT"))
        {
            tLAComChargeSchema.setState("1");
        }
        else
        {
            tLAComChargeSchema.setState("2");
        }
        tLAComChargeSchema.setMakeDate(currentDate);
        tLAComChargeSchema.setMakeTime(currentTime);
        tLAComChargeSchema.setModifyDate(currentDate);
        tLAComChargeSchema.setModifyTime(currentTime);
        tLAComChargeSchema.setGetDate(currentDate);
        tLAComChargeSchema.setOperator(mGlobalInput.Operator);
        tLAComChargeSchema.setOperator2(mGlobalInput.Operator);
        tLAComChargeSchema.setBranchType(mLAComChargeSchema.getBranchType());
        tLAComChargeSchema.setStartDate(mLAComChargeSchema.getStartDate());
        tLAComChargeSchema.setStartEnd(mLAComChargeSchema.getStartEnd());
        tLAComChargeSchema.setAgentCom(mLAComChargeSchema.getAgentCom());
        mLAComChargeSet.add(tLAComChargeSchema);
        return true;
    }


    private boolean cal2()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        String cSQL = "select ManageCom,sum(charge) from LACharge where "
                      + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                      "'"
                      + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                      "' and CalDate<='"
                      + mLAComChargeSchema.getStartEnd() +
                      "' and AgentCom like '"
                      + mLAComChargeSchema.getAgentCom() +
                      "%' and ChargeType like '4%' group by managecom";

        ExeSQL cExeSQL = new ExeSQL();
        SSRS cSSRS = cExeSQL.execSQL(cSQL);
        LAComChargeSchema tLAComChargeSchema;
        tLAComChargeSchema = new LAComChargeSchema();
        tLAComChargeSchema.setManageCom(cSSRS.GetText(1, 1));
        tLAComChargeSchema.setSumMoney(cSSRS.GetText(1, 2));
        tLAComChargeSchema.setCurrMoney(cSSRS.GetText(1, 2));
        tLAComChargeSchema.setChargeType("2");
        if (this.mOperate.equals("PRINT"))
        {
            tLAComChargeSchema.setState("1");
        }
        else
        {
            tLAComChargeSchema.setState("2");
        }
        tLAComChargeSchema.setMakeDate(currentDate);
        tLAComChargeSchema.setMakeTime(currentTime);
        tLAComChargeSchema.setModifyDate(currentDate);
        tLAComChargeSchema.setModifyTime(currentTime);
        tLAComChargeSchema.setGetDate(currentDate);
        tLAComChargeSchema.setOperator(mGlobalInput.Operator);
        tLAComChargeSchema.setOperator2(mGlobalInput.Operator);
        tLAComChargeSchema.setBranchType(mLAComChargeSchema.getBranchType());
        tLAComChargeSchema.setStartDate(mLAComChargeSchema.getStartDate());
        tLAComChargeSchema.setStartEnd(mLAComChargeSchema.getStartEnd());
        tLAComChargeSchema.setAgentCom(mLAComChargeSchema.getAgentCom());
        mLAComChargeSet.add(tLAComChargeSchema);

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAComChargeSchema.setSchema((LAComChargeSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LAComChargeSchema", 1));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAComChargeSet);
            this.mInputData.add(this.mLAChargeSet);
            System.out.println("size:" + mLAComChargeSet.size());
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PoundCalBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
    }
}
