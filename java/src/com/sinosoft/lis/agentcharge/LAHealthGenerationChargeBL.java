package com.sinosoft.lis.agentcharge;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2016</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LAHealthGenerationChargeBL
{ /**错误信息容器*/
	public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    SSRS tSSRS=new SSRS();
    public LAHealthGenerationChargeBL()
    {
      }
   
	public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }
        
        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);
        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());
            
            CError tError = new CError();
            tError.moduleName = "LAWageCrossChargeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        System.out.println("========================================"+tSSRS.getMaxRow());
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        mToExcel[0][0] = "健代产实付手续费查询报表";
        mToExcel[1][0] = "保单号";
        mToExcel[1][1] = "管理机构编码";
        mToExcel[1][2] = "管理机构名称";
        mToExcel[1][3] = "销售人员工号";
        mToExcel[1][4] = "销售人员姓名";
        mToExcel[1][5] = "销售人员归属省级机构";
        mToExcel[1][6] = "保单归属省级机构代码";        
        mToExcel[1][7] = "保单互动专员工号";
        mToExcel[1][8] = "互动专员归属机构代码";
        mToExcel[1][9] = "互动专员归属渠道代码";
        mToExcel[1][10] = "互动专员有效性";
        mToExcel[1][11] = "财险保单经办人代码";
        mToExcel[1][12] = "财险保单经办人归属机构";
        mToExcel[1][13] = "财险保单归属人代码";
        mToExcel[1][14] = "财险保单归属人归属机构";
        mToExcel[1][15] = "财险保单出单人代码";
        mToExcel[1][16] = "财险保单出单人归属机构";
        mToExcel[1][17] = "是否可支付佣金保单状态";
        mToExcel[1][18] = "佣金不可支付原因";
        mToExcel[1][19] = "公对公支付单号";
        mToExcel[1][20] = "出单系统标识";
        mToExcel[1][21] = "公对公佣金";
        mToExcel[1][22] = "实付佣金";
        mToExcel[1][23] = "应付业务佣金合计";
        mToExcel[1][24] = "80%手续费";
        mToExcel[1][25] = "剩余手续费";
        mToExcel[1][26] = "80%手续费实付状态";
        mToExcel[1][27] = "公对个人佣金比例";
        mToExcel[1][28] = "承保日期";
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            { 
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
                //System.out.println(row +1);
                //System.out.println("---------"+mToExcel[row +1][col - 1]);
                //System.out.println( tSSRS.GetText(row, col));
            }
            //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+tSSRS.getMaxCol());
        }
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

   
   
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData  cInputData)
    {
    	mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) cInputData
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAWageCrossChargeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAWageCrossChargeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


}



