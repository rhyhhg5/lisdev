package com.sinosoft.lis.agentcharge;

import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LAChargePayBatchLogDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargePayBatchLogSchema;
import com.sinosoft.lis.vschema.LAChargePayBatchLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 用来处理江苏中介平台手续费相关接口返回报文的信息保存
 * 返回信息保存到表LAChargePayBatchLog中
 * 
 * @author yangyang
 * 2015-5-4
 */
public class DealLAChargeBackXML {

	/**
	 * 批次号
	 */
	private String mBatchNo ="";
	/**
	 * 返回报文 
	 */
	private String mXml ="";
	/**
	 * 手续费类型
	 */
	private String mChargeType="";
	/**
	 * 操作人
	 */
	private String mOperator ="";
	
	/**
	 * 对于数据库的操作（新增或修改)
	 */
	private String mOperate="";
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	
	 /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    
    private MMap mMap = new MMap();
	private LAChargePayBatchLogSchema mLAChargePayBatchLogSchema = new LAChargePayBatchLogSchema();
	
	/**
	 * 对于返回报文 进行处理
	 * @return
	 */
	public boolean submit(VData cVdata,String cOperator)
	{
		if (!getInputData(cVdata, cOperator)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		
		if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start ActiveAgentChargeCalNewBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ActiveAgentChargeCalNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

		return true;
		
	}
	private Document getDocument(String tstr) {
		java.io.Reader in = new StringReader(tstr);
		Document doc = null;
		try {
			doc = (new SAXBuilder()).build(in);
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return doc;
	}
	private boolean getInputData(VData cVData, String cOpeartor)
	{
		TransferData cTransferData = new TransferData();
		cTransferData = (TransferData)cVData.get(0);
		this.mBatchNo = (String)cTransferData.getValueByName("BatchNo");
		this.mXml = (String)cTransferData.getValueByName("XML");
		this.mChargeType = (String)cTransferData.getValueByName("ChargeType");
		this.mOperator = (String)cTransferData.getValueByName("Operator");;
		return true;
	}
	
	private boolean checkData()
	{
		return true; 
	}
	
	private boolean dealData()
	{
		String tSerialno = "";
		String sql ="select * from LAChargePayBatchLog where batchno ='"+this.mBatchNo+"'";
		LAChargePayBatchLogDB tLAChargePayBatchLogDB = new LAChargePayBatchLogDB();
		LAChargePayBatchLogSet tLAChargePayBatchLogSet = new LAChargePayBatchLogSet();
		tLAChargePayBatchLogSet = tLAChargePayBatchLogDB.executeQuery(sql);
		if(0==tLAChargePayBatchLogSet.size())
		{
			this.mOperate = "INSERT";
			String tOtherNo = PubFun.getCurrentDate2();
			tSerialno = tOtherNo+PubFun1.CreateMaxNo(tOtherNo, 2);
			this.mLAChargePayBatchLogSchema.setSerialNo(tSerialno);
			this.mLAChargePayBatchLogSchema.setBatchNo(this.mBatchNo);
			this.mLAChargePayBatchLogSchema.setMakeDate(PubFun.getCurrentDate());
			this.mLAChargePayBatchLogSchema.setMakeTime(PubFun.getCurrentTime());
			this.mLAChargePayBatchLogSchema.setModifyDate(PubFun.getCurrentDate());
			this.mLAChargePayBatchLogSchema.setModifyTime(PubFun.getCurrentTime());
		}
		else
		{
			this.mOperate = "UPDATE";
			this.mLAChargePayBatchLogSchema = tLAChargePayBatchLogSet.get(1);
			this.mLAChargePayBatchLogSchema.setModifyDate(PubFun.getCurrentDate());
			this.mLAChargePayBatchLogSchema.setModifyTime(PubFun.getCurrentTime());
		}
		this.mLAChargePayBatchLogSchema.setOperator(this.mOperator);
		Document tresDocument = getDocument(this.mXml.toString());
		if (tresDocument != null) {
			Element tall = tresDocument.getRootElement();
			Element thead = tall.getChild("Head");
			System.out.println("tal:"
					+ tall.getChild("Head").getChildText("ErrorCode"));
			Element tRequestType = thead.getChild("RequestType");
			Element tResponseCode = thead.getChild("ResponseCode");
			Element tHeadErrorCode = thead.getChild("ErrorCode");
			Element tHeadErrorMessage = thead.getChild("ErrorMessage");
			
			String tRealErrorCode = tHeadErrorCode.getText();
			String tRealErrorMessage  =tHeadErrorMessage.getText();
			
			//手续费校验和手续费结算接口类型
			if("C07".equals(tRequestType.getText()))
			{
				if("ZX".equals(this.mChargeType))
				{
					this.mLAChargePayBatchLogSchema.setZXRequestType(tRequestType.getText());
					this.mLAChargePayBatchLogSchema.setZXResponseCode(tResponseCode.getText());
					this.mLAChargePayBatchLogSchema.setZXErrorCode(tRealErrorCode);
					this.mLAChargePayBatchLogSchema.setZXErrorMessage(tRealErrorMessage);
				}
				if("HX".equals(this.mChargeType))
				{
					this.mLAChargePayBatchLogSchema.setHXRequestType(tRequestType.getText());
					this.mLAChargePayBatchLogSchema.setHXResponseCode(tResponseCode.getText());
					this.mLAChargePayBatchLogSchema.setHXErrorCode(tRealErrorCode);
					this.mLAChargePayBatchLogSchema.setHXErrorMessage(tRealErrorMessage);
				}
			}
			else if("C09".equals(tRequestType.getText()))
			{
				if("HX".equals(this.mChargeType))
				{
					this.mLAChargePayBatchLogSchema.setHXRequestType(tRequestType.getText());
					this.mLAChargePayBatchLogSchema.setHXResponseCode(tResponseCode.getText());
					this.mLAChargePayBatchLogSchema.setHXErrorCode(tRealErrorCode);
					this.mLAChargePayBatchLogSchema.setHXErrorMessage(tRealErrorMessage);
				}
				if("1".equals(tResponseCode.getText())&&"HX".equals(this.mChargeType))
				{
					Element tBody = tall.getChild("Body");
					Element tBasePart = tBody.getChild("BasePart");
					Element tVoucherRegNo = tBasePart.getChild("VoucherRegNo");
					this.mLAChargePayBatchLogSchema.setVoucherRegNo(tVoucherRegNo.getText());
				}
			}
			else if("C12".equals(tRequestType.getText()))
			{
				if(!"1".equals(tResponseCode.getText())&&!"0000".equals(tRealErrorCode))
				{
					Element tBody = tall.getChild("Body");
					if(null!=tBody)
					{
						Element tError = tBody.getChild("Error");
						Element tBodyErrorCode = tError.getChild("ErrorCode");
						Element tBodyErrorMessage = tError.getChild("ErrorMessage");	
						tRealErrorCode = tBodyErrorCode.getText();
						tRealErrorMessage = tBodyErrorMessage.getText();
					}
				}
				this.mLAChargePayBatchLogSchema.setRegisteRequestType(tRequestType.getText());
				this.mLAChargePayBatchLogSchema.setRegisterResponseCode(tResponseCode.getText());
				this.mLAChargePayBatchLogSchema.setRegisteErrorCode(tRealErrorCode);
				this.mLAChargePayBatchLogSchema.setRegisteErrorMessage(tRealErrorMessage);
			}
			else if("C13".equals(tRequestType.getText()))
			{
				this.mLAChargePayBatchLogSchema.setPayRequestType(tRequestType.getText());
				this.mLAChargePayBatchLogSchema.setPayResponseCode(tResponseCode.getText());
				this.mLAChargePayBatchLogSchema.setPayErrorCode(tRealErrorCode);
				this.mLAChargePayBatchLogSchema.setPayErrorMessage(tRealErrorMessage);
				if("1".equals(tResponseCode.getText()))
				{
					Element tBody = tall.getChild("Body");
					Element tVoucher = tBody.getChild("Voucher");
					Element tStatementsPayStatusInfo = tVoucher.getChild("StatementsPayStatusInfo");
					Element tBankName = tStatementsPayStatusInfo.getChild("BankName");
					Element tAccountName = tStatementsPayStatusInfo.getChild("AccountName");
					Element tAccountNumber = tStatementsPayStatusInfo.getChild("AccountNumber");
					Element tAmount = tStatementsPayStatusInfo.getChild("Amount");
					Element tPayTime = tStatementsPayStatusInfo.getChild("PayTime");
					Element tPayStatus = tStatementsPayStatusInfo.getChild("PayStatus");
					this.mLAChargePayBatchLogSchema.setBankName(tBankName.getText());
					this.mLAChargePayBatchLogSchema.setAccountName(tAccountName.getText());
					this.mLAChargePayBatchLogSchema.setAccountNumber(tAccountNumber.getText());
					this.mLAChargePayBatchLogSchema.setAmount(tAmount.getText());
					this.mLAChargePayBatchLogSchema.setPayTime(tPayTime.getText());
					this.mLAChargePayBatchLogSchema.setPayStatus(tPayStatus.getText());
				}
			}
		}
		else
		{
			System.out.println("获取返回报文失败！");
			CError tError = new CError();
			tError.moduleName = "ContInputChargecomChkBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询返回报文获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	 private boolean prepareOutputData() {
	        mMap.put(this.mLAChargePayBatchLogSchema, this.mOperate);
	        this.mInputData.add(mMap);
	        return true;
	  }
	 public LAChargePayBatchLogSchema getLAChargePayBatchLogSchemaByBatchNo(String cBatchNo)
	 {
		String sql ="select * from LAChargePayBatchLog where batchno ='"+cBatchNo+"'";
		LAChargePayBatchLogDB tLAChargePayBatchLogDB = new LAChargePayBatchLogDB();
		LAChargePayBatchLogSet tLAChargePayBatchLogSet = new LAChargePayBatchLogSet();
		tLAChargePayBatchLogSet = tLAChargePayBatchLogDB.executeQuery(sql);
		if(0!=tLAChargePayBatchLogSet.size())
		{
			return tLAChargePayBatchLogSet.get(1);
		}
		return null;
	 }
	 public LAChargePayBatchLogSchema getLAChargePayBatchLogSchemaByVoucherRegNo(String cVoucherRegNo)
	 {
		String sql ="select * from LAChargePayBatchLog where VoucherRegNo ='"+cVoucherRegNo+"'";
		LAChargePayBatchLogDB tLAChargePayBatchLogDB = new LAChargePayBatchLogDB();
		LAChargePayBatchLogSet tLAChargePayBatchLogSet = new LAChargePayBatchLogSet();
		tLAChargePayBatchLogSet = tLAChargePayBatchLogDB.executeQuery(sql);
		if(0!=tLAChargePayBatchLogSet.size())
		{
			return tLAChargePayBatchLogSet.get(1);
		}
		return null;
	 }
	
}
