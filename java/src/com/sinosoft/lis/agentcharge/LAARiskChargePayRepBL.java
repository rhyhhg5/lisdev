package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
public class LAARiskChargePayRepBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    public GlobalInput mGlobalInput = new GlobalInput();
    //private String mDownloadType = null;


    public LAARiskChargePayRepBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	
	      if(!lockOporator(this.mGI.Operator))
	      {
	    	  buildError("submitData", "为防止系统压力过大，用户"+this.mGI.Operator+"已被锁定，请在2分钟后再进行打印操作！");
	    	  return false;
	      }
    	System.out.println("................LAARiskChargePayRepBL.java begin dealData()");
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);
//      防止同一用户多次点击下载加入锁
//        MMap tCekMap1 = null;
//		  tCekMap1 = lockOporator(this.mGI.Operator);
//	      if (tCekMap1 == null)
//	      {
//	        return false;
//	      }
//	      map.add(tCekMap1);


        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAARiskChargePayRepBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        String[][] mToExcel = null;
	   mToExcel= new String[tSSRS.getMaxRow() + 2][23];
	   mToExcel[0][3] = "个险中介手续费查询报表";
	   mToExcel[1][0] = "年月";
	   mToExcel[1][1] = "管理机构编码";
	   mToExcel[1][2] = "中介机构编码";
	   mToExcel[1][3] = "中介机构名称";
	   mToExcel[1][4] = "首期手续费";
	   mToExcel[1][5] = "续年手续费";
	   mToExcel[1][6] = "继续率奖(加款)";
	   mToExcel[1][7] = "月度奖(加款)";
	   mToExcel[1][8] = "季度奖(加款)"; 
	   mToExcel[1][9] = "年度奖(加款)";
	   mToExcel[1][10] = "其他加款";
	   mToExcel[1][11] = "其他扣款";
	   mToExcel[1][12] = "合计金额";
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row+1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
            
//           System.out.println(t);
            
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
    	System.out.println("...............begin inputdate");
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAARiskChargePayRepBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        
        mSql = (String) tf.getValueByName("querySql");
        mSql=StrTool.GBKToUnicode(mSql);
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        //this.mDownloadType = (String) tf.getValueByName("downloadType");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null )
        {
            CError tError = new CError();
            tError.moduleName = "LAARiskChargePayRepBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private boolean lockOporator(String s)
    {
    	
    	
        /**锁定标志"XSLACharge"*/
        String tLockNoType = "XS";
        /**锁定时间*/
        String tAIS = "120";
        System.out.println("设置了2分的解锁时间");
        System.out.println("参数---->>"+this.mGI.Operator);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", this.mGI.Operator);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
       
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(mGlobalInput);
        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
       if(!tLockTableActionBL.submitData(tVData, null))
       {
    	   return false;
       }

        return true;
    }
  
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAARiskChargePayRepBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    
}


