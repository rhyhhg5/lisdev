/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-12-07 09:58:25
 */
package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;


public class ChAutGrantBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mAgentCom = "";
    private String mManageCom = "";
    private String mACType = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String mChargeType = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();
    private LAChargeSet mLAChargeSet = new LAChargeSet();

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理前进行验证
        if (!check()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            buildError("submitData","进行手续费审核发放后台提交失败！");
            return false;
        }

        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try{
            mMap.put(this.mLAChargeSet, "UPDATE");
            this.mOutputDate.add(mMap);
        }catch (Exception ex)
        {
            buildError("prepareOutputData","准备提交后台的数据时出错！");
            return false;
        }

        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        // 验证没有符合条件的，未审核发放的手续费数据
        String tSQL = "";
        LAChargeSet tLAChargeSet = new LAChargeSet();
        LAChargeDB tLAChargeDB = new LAChargeDB();

        tSQL  = "select a.* from lacharge a,lacom b";
        tSQL += " where a.agentcom = b.agentcom";
        tSQL += "   and a.branchtype = '" + this.mBranchType + "'";
        tSQL += "   and a.branchtype2 = '" + this.mBranchType2 + "'";
        tSQL += "   and a.chargestate = '0'";
        if(!"".equals(this.mAgentCom))
        {
            tSQL += " and a.agentcom = '" + this.mAgentCom + "'";
        }
        if(!"".equals(this.mManageCom))
        {
            tSQL += " and a.managecom = '" + this.mManageCom + "'";
        }
        if(!"".equals(this.mACType))
        {
            tSQL += " and b.actype = '" + this.mACType + "'";
        }
        if(!"".equals(this.mStartDate))
        {
            tSQL += " and a.caldate >= '" + this.mStartDate + "'";
        }
        if(!"".equals(this.mEndDate))
        {
            tSQL += " and a.caldate <= '" + this.mEndDate + "'";
        }
        if(!"".equals(this.mChargeType))
        {
            tSQL += " and a.chargetype = '" + this.mChargeType + "'";
        }
        System.out.println(tSQL);
        tLAChargeSet = tLAChargeDB.executeQuery(tSQL);

        if(tLAChargeDB.mErrors.needDealError())
        {
            buildError("dealData","访问数据库查询指定条件的未审核手续费信息时出错！");
            return false;
        }
        if(tLAChargeSet.size() == 0)
        {
            buildError("dealData","没有找到符合条件的未审核手续费信息！");
            return false;
        }

        // 更改审核方法状态
        if(!dealChAutGrant(tLAChargeSet))
        {
            return false;
        }

        return true;
    }

    /**
     * 更改审核发放状态
     * @param pmLAChargeSet LAChargeSet
     * @return boolean
     */
    private boolean dealChAutGrant(LAChargeSet pmLAChargeSet)
    {
        for(int i=1;i<=pmLAChargeSet.size();i++)
        {
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema = pmLAChargeSet.get(i);

            tLAChargeSchema.setChargeState("2");

            this.mLAChargeSet.add(tLAChargeSchema);
        }

        return true;
    }

    /**
     * 处理前的验证
     * @return boolean
     */
    private boolean check()
    {
        // 对于几个必填项目的非空验证
        if("".equals(this.mBranchType))
        {
            buildError("check","必要参数[BranchType]不能为空！");
            return false;
        }
        if("".equals(this.mBranchType2))
        {
            buildError("check","必要参数[BranchType2]不能为空！");
            return false;
        }
        if("".equals(this.mManageCom))
        {
            buildError("check","必要参数[管理机构]不能为空！");
            return false;
        }
        if("".equals(this.mStartDate))
        {
            buildError("check","必要参数[审核起期]不能为空！");
            return false;
        }
        if("".equals(this.mEndDate))
        {
            buildError("check","必要参数[审核止期]不能为空！");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentEvaluateBL.getInputData.........");
        this.mAgentCom = ((String) cInputData.getObject(0)).trim();
        this.mManageCom = ((String) cInputData.getObject(1)).trim();
        this.mACType = ((String) cInputData.getObject(2)).trim();
        this.mStartDate = ((String) cInputData.getObject(3)).trim();
        this.mEndDate = ((String) cInputData.getObject(4)).trim();
        this.mChargeType = ((String) cInputData.getObject(5)).trim();
        this.mBranchType = ((String) cInputData.getObject(6)).trim();
        this.mBranchType2 = ((String) cInputData.getObject(7)).trim();
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println(mAgentCom + " / "+mManageCom+" / "+mACType+" / "
                           +mStartDate+" / "+mEndDate+" / "+mChargeType+" / "
                           +mBranchType+" / "+mBranchType2);

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ChAudFewBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

}
