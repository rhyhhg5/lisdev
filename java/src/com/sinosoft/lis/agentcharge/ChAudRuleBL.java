/*
 * <p>ClassName: ChAudFewlBL </p>
 * <p>Description: ChAudFewlBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-12-05 09:58:25
 */
package com.sinosoft.lis.agentcharge;

import java.sql.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class ChAudRuleBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mManameCom = "";
    private String mChargeType = "";
    private String mBranchType = "";
    private String mBranchType2= "";
    private String mChargeType2 = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理前进行验证
        if (!check()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        return true;
    }

    /**
     * 进行后台检验
     * @return boolean
     */
    private boolean check()
    {
        // 四个参数缺一不可
        if(this.mStartDate == null || "".equals(this.mStartDate))
        {
            buildError("check","[计算起期]不能为空！");
            return false;
        }
        if(this.mEndDate == null || "".equals(this.mEndDate))
        {
            buildError("check","[计算止期]不能为空！");
            return false;
        }
        if(this.mManameCom == null || "".equals(this.mManameCom))
        {
            buildError("check","[管理机构]不能为空！");
            return false;
        }
        if(this.mChargeType == null || "".equals(this.mChargeType))
        {
            buildError("check","[结算类型]不能为空！");
            return false;
        }

        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        String tSQL = "";
        LACommisionSet tLACommisionSet = new LACommisionSet();

        // 取得符合条件的未计算的保单信息
        tLACommisionSet = getLACommisionData();
        if(null == tLACommisionSet)
        {
            return false;
        }
        if(tLACommisionSet.size() == 0)
        {
            buildError("dealCharge","没有找到符合条件的保单数据！");
            return false;
        }

        // 对未结算的保单进行结算处理
        if(!dealCharge(tLACommisionSet))
        {
            return false;
        }

        return true;
    }

    /**
     * 进行手续费结算
     * @param <any> LACommisionSet
     * @return boolean
     */
    private boolean dealCharge(LACommisionSet pmLACommisionSet)
    {
        LACommisionSet tLACommisionSet = new LACommisionSet();

        for(int i=1;i<=pmLACommisionSet.size();i++)
        {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            String tGrpContNo = pmLACommisionSet.get(i).getGrpContNo();

            if(tLACommisionSet.size() > 0)
            {
                boolean tFlag = true;
                for(int j=1;j<=tLACommisionSet.size();j++)
                {
                    if(tLACommisionSet.get(j).getGrpContNo().equals(tGrpContNo))
                    {
                        tFlag = false;
                        break;
                    }
                }
                if(tFlag)
                {
                    tLACommisionSchema.setGrpContNo(tGrpContNo);
                    tLACommisionSet.add(tLACommisionSchema);
                }
            }else
            {
                tLACommisionSchema.setGrpContNo(tGrpContNo);
                tLACommisionSet.add(tLACommisionSchema);
            }
        }

        // 判断是否取数成功
        if(tLACommisionSet.size() == 0)
        {
            buildError("dealCharge","没有需要结算的数据！");
            return false;
        }
        System.out.println("需要处理的保单个数："+tLACommisionSet.size());
        VData cInputData = new VData();
        cInputData.add(this.mChargeType);
        cInputData.add(this.mChargeType2);
        cInputData.add(this.mGlobalInput);
        cInputData.add(tLACommisionSet);
        ChAudFewUI tChAudFewUI = new ChAudFewUI();
        boolean actualReturn = tChAudFewUI.submitData(cInputData, mOperate);
        //如果有需要处理的错误，则返回
        if (tChAudFewUI.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tChAudFewUI.mErrors);
            buildError("dealCharge","数据提交失败！");

            return false;
        }

        return true;
    }

    /**
     * 查询符合条件的未结算的保单费用信息
     * @return LACommisionSet
     */
    private LACommisionSet getLACommisionData()
    {
        String tSQL = "";
        LACommisionSet tRtLACommisionSet = new LACommisionSet();
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();

        tSQL  = "select * from LACommision where ManageCom ='";
        tSQL += this.mManameCom + "' and BranchType='" + this.mBranchType;
        tSQL += "' and BranchType2='" + this.mBranchType2 + "'";
        tSQL += " and CalDate >= '" + this.mStartDate + "'";
        tSQL += " and CalDate <= '" + this.mEndDate + "'";
        tSQL += " and P7 <> 1";    // 没有进行结算的数据
        System.out.println("检索符合条件的保单费用信息：" + tSQL);

        tLACommisionSet = tLACommisionDB.executeQuery(tSQL);
        if(tLACommisionDB.mErrors.needDealError())
        {
            buildError("dealData","查询符合条件的保单费用信息时出错！");
            return null;
        }
        if(tLACommisionSet.size() == 0)
        {
            buildError("dealData","没有找到符合条件的保单！");
            return null;
        }

        tRtLACommisionSet = tLACommisionSet;

        return tRtLACommisionSet;
    }

    /**
     * 准备提交后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try{
        }catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentEvaluateBL.getInputData.........");
        this.mStartDate = (String) cInputData.getObject(0);
        this.mEndDate = (String) cInputData.getObject(1);
        this.mManameCom = (String) cInputData.getObject(2);
        this.mChargeType = (String) cInputData.getObject(3);
        this.mBranchType = (String) cInputData.getObject(4);
        this.mBranchType2 = (String) cInputData.getObject(5);
        this.mChargeType2 = (String) cInputData.getObject(6);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        System.out.println(mStartDate+"  "+mEndDate+"  "+mManameCom+"  "+mChargeType
                +"  "+mBranchType+"  "+mBranchType2+"  "+mChargeType2);

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ChAudFewBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
}
