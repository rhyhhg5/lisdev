package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAComChargeSchema;
import com.sinosoft.lis.vschema.LAComChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.FDate;
import java.util.Date;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.schema.LAChargeSchema;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */


public class ChDstComCharge
{

 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
 /** 往后面传输数据的容器 */
    private VData mInputData;
 /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
 /** 数据操作字符串 */
    private String mOperate;
 /** 业务处理相关变量 */
    private LAComChargeSchema mLAComChargeSchema = new LAComChargeSchema();
    private LAComChargeSet mLAComChargeSet = new LAComChargeSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
    private MMap map = new MMap();
           /** 业务处理相关变量 */
//  private LAChargeSchema mLAChargeSchema=new LAChargeSchema();
    public ChDstComCharge()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("1123435555");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //校验时间
        System.out.println("112343");
        if (!check(cInputData))
       {
           return false;
       }
        System.out.println("11234355555555555555555");
        //进行业务处理
        //进行插入数据
        if (!dealData())
        {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "PoundCalBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败PoundCalBL-->dealData!";
          this.mErrors.addOneError(tError);
          return false;
         }
       //准备往后台的数据
         if (!prepareOutputData())
         {
          return false;
         }
         else
         {
         System.out.println("Start ChDstComCharge Submit...");
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, mOperate))
         {
           // @@错误处理
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "ChDstComCharge";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           this.mErrors.addOneError(tError);
           return false;
         }
         System.out.println("End ChDstComCharge Submit...");
       }
       mInputData = null;
       return true;
    }
    //校验时间，判断是否计算过
    private boolean check(VData mInputData)
    {
        //LAComChargeSet tLAComChargeSet = new LAComChargeSet();
        //tLAComChargeSet.set((LAComChargeSet) mInputData.getObjectByObjectName(
        //        "LAComChargeSet", 1));
        //LAComChargeSchema tLAComChargeSchema = new LAComChargeSchema();
        //tLAComChargeSchema.setSchema(tLAComChargeSet.get(1));
        String asql = "";
        String max = "";
        String min = "";
        FDate fd = new FDate();
        String currentDate = PubFun.getCurrentDate();
        String strSQL="select agentcom from lacom where  agentcom='"+mLAComChargeSchema.getAgentCom()+"'";
        ExeSQL strExeSQL = new ExeSQL();
        String tAgentCom=strExeSQL.getOneValue(strSQL);
        if(tAgentCom.equals("")||tAgentCom==null)
        {
            CError tError = new CError();
            tError.moduleName = "ChDstComCharge";
            tError.functionName = "check()";
            tError.errorMessage = "没有代码为"+mLAComChargeSchema.getAgentCom()+"的代理机构";
            this.mErrors.addOneError(tError);
            return false;

        }

        if((fd.getDate(mLAComChargeSchema.getStartEnd())).compareTo(fd.getDate(currentDate))>0)
        {
            CError tError = new CError();
            tError.moduleName = "ChDstComCharge";
            tError.functionName = "check()";
            tError.errorMessage = "统计止期不能大于今天";
            this.mErrors.addOneError(tError);
                return false;
        }
            max =
                    "select char(max(StartEnd)) from lacomcharge where agentcom='" +
                    mLAComChargeSchema.getAgentCom() + "' and chargetype ='" +
                    mLAComChargeSchema.getChargeType() + "' ";
            min =
                    "select char(min(StartDate)) from lacomcharge where agentcom='" +
                    mLAComChargeSchema.getAgentCom() + "' and chargetype ='" +
                    mLAComChargeSchema.getChargeType() + "' ";
            ExeSQL bExeSQL = new ExeSQL();
            ExeSQL cExeSQL = new ExeSQL();
            max = bExeSQL.getOneValue(max);
            min = cExeSQL.getOneValue(min);
            if (max == null || max.equals("")|| min == null||min.equals(""))
            {
                return true;
            }

            asql = "select nvl(count(*),0) from lacomcharge where agentcom='" +
                   mLAComChargeSchema.getAgentCom() + "' and  chargetype ='" +
                   mLAComChargeSchema.getChargeType() + "'"
                   + " and '" + max + "'>'" + mLAComChargeSchema.getStartDate() +
                   "' or '" + min + "'>'" + mLAComChargeSchema.getStartEnd() +
                   "' ";
            //确定银代审核日期
            ExeSQL aExeSQL = new ExeSQL();
            if (aExeSQL.getOneValue(asql).equals("0"))
            {
                FDate fDate = new FDate();
                Date MaxDate = fDate.getDate(max);
                System.out.println("MaxDate" + MaxDate);

                Date BeginDate = PubFun.calDate(MaxDate, 1, "D", null);
                String StartDay = fDate.getString(BeginDate);
                System.out.println("应该起期" + StartDay);
                if (StartDay.equals(mLAComChargeSchema.getStartDate()))
                {
                    return true;
                }
                else
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ChDstComCharge";
                    tError.functionName = "check()";
                    tError.errorMessage = "上次提数时间" + max + ",请选择其第二天提数";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            else
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChDstComCharge";
                tError.functionName = "check()";
                tError.errorMessage = "上次审核日期：" + max + "不能重复结算";
                this.mErrors.addOneError(tError);
                return false;
            }

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        String aSQL = "select ManageCom,sum(charge) from LACharge where "
                      + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                      "'"
                      + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                      "' and CalDate<='"
                      + mLAComChargeSchema.getStartEnd() +
                      "' and AgentCom like '"
                      + mLAComChargeSchema.getAgentCom() +
                      "%' and ChargeType='"+mLAComChargeSchema.getChargeType()+"'   AND chargestate='0' group by managecom";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = aExeSQL.execSQL(aSQL);
        LAComChargeSchema tLAComChargeSchema;
        tLAComChargeSchema = new LAComChargeSchema();
        if(aSSRS.getMaxRow()>0)
        {
            tLAComChargeSchema.setManageCom(aSSRS.GetText(1, 1));
            tLAComChargeSchema.setSumMoney(aSSRS.GetText(1, 2));
            tLAComChargeSchema.setCurrMoney(aSSRS.GetText(1, 2));
        }
        else
        {
            tLAComChargeSchema.setSumMoney(0);
            tLAComChargeSchema.setCurrMoney(0);
        }
        tLAComChargeSchema.setChargeType(mLAComChargeSchema.getChargeType());
//        if (this.mOperate.equals("PRINT"))
//       {
            tLAComChargeSchema.setState("0");
//        }
//        else
//       {
//            tLAComChargeSchema.setState("2");
//       }
        tLAComChargeSchema.setMakeDate(currentDate);
        tLAComChargeSchema.setMakeTime(currentTime);
        tLAComChargeSchema.setModifyDate(currentDate);
        tLAComChargeSchema.setModifyTime(currentTime);
        tLAComChargeSchema.setGetDate(currentDate);
        tLAComChargeSchema.setOperator(mGlobalInput.Operator);
        tLAComChargeSchema.setOperator2(mGlobalInput.Operator);
        tLAComChargeSchema.setBranchType(mLAComChargeSchema.getBranchType());
        tLAComChargeSchema.setBranchType2(mLAComChargeSchema.getBranchType2());
        tLAComChargeSchema.setStartDate(mLAComChargeSchema.getStartDate());
        tLAComChargeSchema.setStartEnd(mLAComChargeSchema.getStartEnd());
        tLAComChargeSchema.setAgentCom(mLAComChargeSchema.getAgentCom());
        mLAComChargeSet.add(tLAComChargeSchema);
        map.put(mLAComChargeSet, "INSERT");
//修改lacharge中保单手续费状态
        String tSQL = "select * from LACharge where "
                      + "branchtype = '" + mLAComChargeSchema.getBranchType() +
                      "'"
                      + " and CalDate>='" + mLAComChargeSchema.getStartDate() +
                      "' and CalDate<='"
                      + mLAComChargeSchema.getStartEnd() +
                      "' and AgentCom like '"
                      + mLAComChargeSchema.getAgentCom() +
                      "%' and ChargeType='"+mLAComChargeSchema.getChargeType()+"'  AND chargestate='0'  group by managecom";
        LAChargeDB tLAChargeDB=new LAChargeDB();
        LAChargeSet tLAChargeSet=new LAChargeSet();
        tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
//        if(tLAChargeSet==null||tLAChargeSet.size()<=0)
//        {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLAChargeSet.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "PoundCalBL";
//                tError.functionName = "cal0";
//                tError.errorMessage = "手续费查询失败!";
//                this.mErrors.addOneError(tError);
//                return false;
//        }
        for(int i=1;i<=tLAChargeSet.size();i++)
        {
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema=tLAChargeSet.get(i);
            tLAChargeSchema.setOperator(mGlobalInput.Operator);
            tLAChargeSchema.setMakeDate(PubFun.getCurrentDate());
            tLAChargeSchema.setMakeTime(PubFun.getCurrentTime());
            tLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
            tLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
            tLAChargeSchema.setChargeState("1");
            mLAChargeSet.add(tLAChargeSchema);
        }
        map.put(mLAChargeSet, "UPDATE");
        return true;

    }
    private boolean getInputData(VData cInputData)
    {
        this.mLAComChargeSchema.setSchema((LAComChargeSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LAComChargeSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
          this.mInputData = new VData();
          mInputData.add(map);
        }
        catch (Exception ex)
        {
              // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ChDstComCharge";
          tError.functionName = "prepareData";
          tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
          this.mErrors.addOneError(tError);
          return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
    }
}
