package com.sinosoft.lis.agentcharge;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

import java.sql.Connection;

import com.sinosoft.lis.vdb.LAChargeDBSet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class GrpAgentComBLS
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

//传输数据类
    private VData mInputData;

    /** 数据操作字符串 */
    private String mOperate;

    public GrpAgentComBLS()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        System.out.println("----bls----");
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        System.out.println("Start GrpAgentComBLS Submit...");
        if (this.mOperate.equals("INSERT||CHARGE"))
        {
            if (!saveLACharge())
            {
                System.out.println("Insert failed");
            }
            System.out.println("End GrpAgentComBLS Submit...");
            return false;
        }
        System.out.println(" sucessful");

        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLACharge()
    {
        String tAgentCom = (String) mInputData.getObjectByObjectName("String",
                0);
        String[] tGrpPolNo = (String[]) mInputData.getObject(0);
        System.out.println("Start Save...");
        Connection conn;

        System.out.println("Start 保存...");
        for (int i = 0; i < tGrpPolNo.length; i++)
        {
            conn = null;
            try
            {
                conn = DBConnPool.getConnection();
                if (conn == null)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpAgentComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据库连接失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                conn.setAutoCommit(false);
                ExeSQL tExeSQL = new ExeSQL(conn);
                String aSQL = "update LCGrpPol set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(aSQL);
                String bSQL = "update LBGrpPol set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(bSQL);
                String cSQL = "update LCPol set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(cSQL);
                String dSQL = "update LBPol set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(dSQL);
                String eSQL = "update LACommision set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(eSQL);
                String fSQL = "update LJSPay set AgentCom='" + tAgentCom +
                              "' where otherNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(fSQL);
                String gSQL = "update LJSPayGrp set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(gSQL);
                String hSQL = "update LJSPayPerson set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(hSQL);
                String iSQL = "update LJAPay set AgentCom='" + tAgentCom +
                              "' where IncomeNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(iSQL);
                String jSQL = "update LJAPayGrp set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(jSQL);
                String kSQL = "update LJAPayPerson set AgentCom='" + tAgentCom +
                              "' where GrpPolNo='" + tGrpPolNo[i] + "'";
                tExeSQL.execUpdateSQL(kSQL);
                conn.commit();
                conn.close();
            }
            catch (Exception ex)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpAgentComBLS";
                tError.functionName = "submitData";
                tError.errorMessage = ex.toString();
                this.mErrors.addOneError(tError);
                try
                {
                    conn.rollback();
                    conn.close();
                }
                catch (Exception e)
                {}
                return false;
            }

        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean deleteLACharge()
    {
        LAChargeSet tLAChargeSet = new LAChargeSet();
        tLAChargeSet.set((LAChargeSet) mInputData.getObjectByObjectName(
                "LAChargeSet", 0));
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAChargeDBSet tLAChargeDBSet = new LAChargeDBSet(conn);
            tLAChargeDBSet.set(tLAChargeSet);
            if (!tLAChargeDBSet.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAChargeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpAgentComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLACharge()
    {
        LAChargeSet tLAChargeSet = new LAChargeSet();
        tLAChargeSet.set((LAChargeSet) mInputData.getObjectByObjectName(
                "LAChargeSet", 0));
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAChargeDBSet tLAChargeDBSet = new LAChargeDBSet(conn);
            tLAChargeDBSet.set(tLAChargeSet);
            if (!tLAChargeDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAChargeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpAgentComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpAgentComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        GrpAgentComBLS GrpAgentComBLS1 = new GrpAgentComBLS();
    }
}
