package com.sinosoft.lis.agentcharge;


import java.sql.Connection;
import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAComChargeSchema;
import com.sinosoft.lis.vdb.LAComChargeDBSet;
import com.sinosoft.lis.vschema.LAComChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vdb.LAChargeDBSet;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

public class PoundCalBLS
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public PoundCalBLS()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start PoundCalBLS Submit...");
        System.out.println("Operator" + this.mOperate);
        if (this.mOperate.equals("CAL") || this.mOperate.equals("PRINT") ||
            this.mOperate.equals("CONFIRM"))
        {
            tReturn = saveLAPresence(cInputData);
        }

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End PoundCalBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAPresence(VData mInputData)
    {
        //校验时间
        boolean tReturn = check(mInputData);
        if (tReturn == true)
        {
            System.out.println("Start Save...");
            Connection conn;
            conn = null;
            conn = DBConnPool.getConnection();
            if (conn == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PoundCalBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据库连接失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            try
            {
                conn.setAutoCommit(false);
                System.out.println("Start 保存...");
                LAComChargeDBSet tLAComChargeDBSet = new LAComChargeDBSet(conn);
                tLAComChargeDBSet.set((LAComChargeSet) mInputData.
                                      getObjectByObjectName("LAComChargeSet", 1));
                System.out.println("size:" + tLAComChargeDBSet.size());
                if (this.mOperate.equals("PRINT"))
                {
                    if (!tLAComChargeDBSet.insert())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAComChargeDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "PoundCalBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
                else
                {
                    if (!tLAComChargeDBSet.update())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAComChargeDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "PoundCalBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
                //修改lacharge表中的手续费状态
                LAChargeSet tLAChargeSet = new LAChargeSet();
                tLAChargeSet.set((LAChargeSet) mInputData.getObjectByObjectName(
                        "LAChargeSet", 0));
                LAChargeDBSet tLAChargeDBSet = new LAChargeDBSet(conn);
                tLAChargeDBSet.set(tLAChargeSet);
                if (!tLAChargeDBSet.update())
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAChargeDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "PoundCalBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                 }
                conn.commit();
                conn.close();
            }
            catch (Exception ex)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PoundCalBLS";
                tError.functionName = "submitData";
                tError.errorMessage = ex.toString();
                this.mErrors.addOneError(tError);
                tReturn = false;
                try
                {
                    conn.rollback();
                    conn.close();
                }
                catch (Exception e)
                {}
            }
        }
        return tReturn;
    }

    private boolean check(VData mInputData)
    {
        LAComChargeSet tLAComChargeSet = new LAComChargeSet();
        tLAComChargeSet.set((LAComChargeSet) mInputData.getObjectByObjectName(
                "LAComChargeSet", 1));
        LAComChargeSchema tLAComChargeSchema = new LAComChargeSchema();
        tLAComChargeSchema.setSchema(tLAComChargeSet.get(1));
        String asql = "";
        String max = "";
        String min = "";
        //银代部审核
        String strSQL="select agentcom from lacom where  agentcom='"+tLAComChargeSchema.getAgentCom()+"'";
        ExeSQL strExeSQL = new ExeSQL();
        String tAgentCom=strExeSQL.getOneValue(strSQL);
        if(tAgentCom.equals("")||tAgentCom==null)
        {
                CError tError = new CError();
                tError.moduleName = "ChDstComCharge";
                tError.functionName = "check()";
                tError.errorMessage = "没有代码为"+tLAComChargeSchema.getAgentCom()+"的代理机构";
                this.mErrors.addOneError(tError);
                return false;

        }
        if (this.mOperate.equals("PRINT"))
        {
            max =
                    "select char(max(StartEnd)) from lacomcharge where agentcom='" +
                    tLAComChargeSchema.getAgentCom() + "' and chargetype ='" +
                    tLAComChargeSchema.getChargeType() + "' ";
            min =
                    "select char(min(StartDate)) from lacomcharge where agentcom='" +
                    tLAComChargeSchema.getAgentCom() + "' and chargetype ='" +
                    tLAComChargeSchema.getChargeType() + "' ";
            ExeSQL bExeSQL = new ExeSQL();
            ExeSQL cExeSQL = new ExeSQL();
            max = bExeSQL.getOneValue(max);
            min = cExeSQL.getOneValue(min);
            if (max == null || max.equals("")|| min == null||min.equals(""))
            {
                return true;
            }

            asql = "select nvl(count(*),0) from lacomcharge where agentcom='" +
                   tLAComChargeSchema.getAgentCom() + "' and  chargetype ='" +
                   tLAComChargeSchema.getChargeType() + "'"
                   + " and '" + max + "'>'" + tLAComChargeSchema.getStartDate() +
                   "' or '" + min + "'>'" + tLAComChargeSchema.getStartEnd() +
                   "' ";
            //确定银代审核日期
            ExeSQL aExeSQL = new ExeSQL();
            String tcount=aExeSQL.getOneValue(asql).substring(0,1);
            System.out.println("213233333333333333||||"+tcount);
            if (tcount.equals("0"))
            {
                FDate fDate = new FDate();
                Date MaxDate = fDate.getDate(max);
                System.out.println("MaxDate" + MaxDate);

                Date BeginDate = PubFun.calDate(MaxDate, 1, "D", null);
                String StartDay = fDate.getString(BeginDate);
                System.out.println("应该起期" + StartDay);
                if (StartDay.equals(tLAComChargeSchema.getStartDate()))
                {
                    return true;
                }
                else
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LARateChargeBLS";
                    tError.functionName = "check()";
                    tError.errorMessage = "上次提数时间" + max + ",请选择其第二天提数";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            else
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LARateChargeBLS";
                tError.functionName = "check()";
                tError.errorMessage = "上次审核日期：" + max + "不能重复结算";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //财务审核日期判定
        else
        {
            String bsql =
                    "select StartDate,StartEnd from lacomcharge where agentcom='" +
                    tLAComChargeSchema.getAgentCom() + "' and chargetype ='" +
                    tLAComChargeSchema.getChargeType() + "' and state='1'";
            ExeSQL bExeSQL = new ExeSQL();
            SSRS bSSRS = new SSRS();
            bSSRS = bExeSQL.execSQL(bsql);
            if (bSSRS.GetText(1, 1).equals(tLAComChargeSchema.getStartDate()) &&
                bSSRS.GetText(1, 2).equals(tLAComChargeSchema.getStartEnd()))
            {
                return true;
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "LARateChargeBLS";
                tError.functionName = "check()";
                tError.errorMessage = "审核时间与银代部审核时间不一致，银代审核时间为" +
                                      bSSRS.GetText(1, 1) + "--" +
                                      bSSRS.GetText(1, 2);
                this.mErrors.addOneError(tError);
                return false;
            }
        }
    }


    public static void main(String[] args)
    {
    }
}
