package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LAComChargeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LAComChargeSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class ChFinChargeBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;

    private String mAgentCom = null;
    private String mChargeType = null;
    private String mStartDate = null;
    private String mEndDate = null;
    /** 业务处理相关变量 */
//  private LAChargeSchema mLAChargeSchema=new LAChargeSchema();
//  private LAChargeSet aLAChargeSet = new LAChargeSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
    private LAComChargeSchema mLAComChargeSchema = new LAComChargeSchema();

    public ChFinChargeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        //进行插入数据
        if (mOperate.equals("INSERT||CHARGE"))
        {
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChFinChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ChFinChargeBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //对数据进行修改操作
        if (mOperate.equals("UPDATE||CHARGE"))
        {
            if (!updateData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChFinChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ChFinChargeBL-->updateData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("---updateData---");
        }
        //对数据进行删除操作
        if (mOperate.equals("DELETE||CHARGE"))
        {
            if (!deleteData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ChFinChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ChFinChargeBL-->deleteData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("----deleteData---");
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ChFinChargeBL Submit...");
            ChFinChargeBLS tChFinChargeBLS = new ChFinChargeBLS();
            tChFinChargeBLS.submitData(mInputData, mOperate);
            System.out.println("End ChFinChargeBL Submit...");
            //如果有需要处理的错误，则返回
            if (tChFinChargeBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tChFinChargeBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ChFinChargeBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String tSQL =
                "select * from LACharge where ChargeState='1' and BranchType='2' and AgentCom='" +
                mAgentCom + "' and ChargeType='" + mChargeType +
                "' and CalDate>='" + mStartDate + "' and CalDate<='" + mEndDate +
                "'";
        System.out.println(tSQL);
        LAChargeSet tLAChargeSet = new LAChargeSet();
        LAChargeDB tLAChargeDB = new LAChargeDB();
//    tLAChargeDB.setAgentCom(this.mAgentCom);
//    tLAChargeDB.setChargeType(this.mLAChargeSet);
        tLAChargeSet = tLAChargeDB.executeQuery(tSQL);
        LAChargeSchema tLAChargeSchema = null;
        for (int i = 1; i <= tLAChargeSet.size(); i++)
        {
            tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema.setSchema(tLAChargeSet.get(i));
            tLAChargeSchema.setOperator(mGlobalInput.Operator);
            tLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
            tLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
            tLAChargeSchema.setChargeState("2");
            mLAChargeSet.add(tLAChargeSchema);
        }
        LAComChargeDB tLAComChargeDB = new LAComChargeDB();
        tLAComChargeDB.setAgentCom(mAgentCom);
        tLAComChargeDB.setChargeType(mChargeType);
        tLAComChargeDB.setStartDate(mStartDate);
        tLAComChargeDB.setStartEnd(mEndDate);
        if (!tLAComChargeDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAComChargeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChFinChargeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "数据查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLAComChargeSchema.setSchema(tLAComChargeDB);
        mLAComChargeSchema.setGetDate(PubFun.getCurrentDate());
        mLAComChargeSchema.setState("2");
        mLAComChargeSchema.setOperator2(mGlobalInput.Operator);
        mLAComChargeSchema.setModifyDate(PubFun.getCurrentDate());
        mLAComChargeSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
//    LAChargeDB tLAChargeDB = new LAChargeDB();
//    tLAChargeDB.setGrpPolNo(mLAChargeSchema.getGrpPolNo());
//    tLAChargeDB.setPayCountFrom(mLAChargeSchema.getPayCountFrom());
//    tLAChargeDB.setPayCountTo(mLAChargeSchema.getPayCountTo());
//    tLAChargeDB.setChargeType(mLAChargeSchema.getChargeType());
//    if (!tLAChargeDB.getInfo())
//    {
//      // @@错误处理
//      this.mErrors.copyAllErrors(tLAChargeDB.mErrors);
//      CError tError = new CError();
//      tError.moduleName = "ChFinChargeBL";
//      tError.functionName = "updateData";
//      tError.errorMessage ="数据查询失败!";
//      this.mErrors.addOneError(tError) ;
//      return false;
//    }
//    mLAChargeSchema.setOperator(this.mGlobalInput.Operator);
//    mLAChargeSchema.setMakeDate(tLAChargeDB.getMakeDate());
//    mLAChargeSchema.setMakeTime(tLAChargeDB.getMakeTime());
//    mLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
//    mLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//    this.mLAChargeSet.set((LAChargeSet)cInputData.getObjectByObjectName("LAChargeSet",0));
//    this.mLAChargeSchema.setSchema((LAChargeSchema)cInputData.getObjectByObjectName("LAChargeSchema",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mAgentCom = (String) cInputData.get(0);
        mChargeType = (String) cInputData.get(1);
        mStartDate = (String) cInputData.get(2);
        mEndDate = (String) cInputData.get(3);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ChFinChargeBLQuery Submit...");
        LAChargeDB tLAChargeDB = new LAChargeDB();
//    tLAChargeDB.setSchema(this.mLAChargeSchema);
        //this.mLAChargeSet=tLAChargeDB.query();
        //this.mResult.add(this.mLAChargeSet);
        System.out.println("End ChFinChargeBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAChargeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAChargeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChFinChargeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            //this.mInputData=new VData();
            //this.mInputData.add(this.mGlobalInput);
            this.mInputData.clear();
//      this.mInputData.add(this.mLAChargeSchema);
            this.mInputData.add(this.mLAChargeSet);
            this.mInputData.add(this.mLAComChargeSchema);
            mResult.clear();
//      mResult.add(this.mLAChargeSchema);
            mResult.add(this.mLAChargeSet);
            mResult.add(this.mLAComChargeSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChFinChargeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        ChFinChargeBL chFinChargeBL1 = new ChFinChargeBL();
    }
}
