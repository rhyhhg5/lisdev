package com.sinosoft.lis.agentcharge;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */


import java.sql.Connection;

import com.sinosoft.lis.db.LMGrpChargeDB;
import com.sinosoft.lis.schema.LMGrpChargeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class GrpChargeBLS
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
//传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;

    public GrpChargeBLS()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        System.out.println("Start GrpChargeBLS Submit...");
        if (this.mOperate.equals("INSERT||CHARGE"))
        {
            if (!saveLMGrpCharge())
            {
                System.out.println("Insert failed");
            }
            System.out.println("End GrpChargeBLS Submit...");
            return false;
        }
        if (this.mOperate.equals("DELETE||CHARGE"))
        {
            if (!deleteLMGrpCharge())
            {
                System.out.println("delete failed");
            }
            System.out.println("End GrpChargeBLS Submit...");
            return false;
        }
        if (this.mOperate.equals("UPDATE||CHARGE"))
        {
            if (!updateLMGrpCharge())
            {
                System.out.println("update failed");
            }
            System.out.println("End GrpChargeBLS Submit...");
            return false;
        }
        System.out.println(" sucessful");

        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLMGrpCharge()
    {
        LMGrpChargeSchema tLMGrpChargeSchema = new LMGrpChargeSchema();
        tLMGrpChargeSchema = (LMGrpChargeSchema) mInputData.
                             getObjectByObjectName("LMGrpChargeSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpChargeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LMGrpChargeDB tLMGrpChargeDB = new LMGrpChargeDB(conn);
            tLMGrpChargeDB.setSchema(tLMGrpChargeSchema);
            if (!tLMGrpChargeDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMGrpChargeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpChargeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpChargeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean deleteLMGrpCharge()
    {
        LMGrpChargeSchema tLMGrpChargeSchema = new LMGrpChargeSchema();
        tLMGrpChargeSchema = (LMGrpChargeSchema) mInputData.
                             getObjectByObjectName("LMGrpChargeSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpChargeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LMGrpChargeDB tLMGrpChargeDB = new LMGrpChargeDB(conn);
            tLMGrpChargeDB.setSchema(tLMGrpChargeSchema);
            if (!tLMGrpChargeDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMGrpChargeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpChargeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpChargeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLMGrpCharge()
    {
        LMGrpChargeSchema tLMGrpChargeSchema = new LMGrpChargeSchema();
        tLMGrpChargeSchema = (LMGrpChargeSchema) mInputData.
                             getObjectByObjectName("LMGrpChargeSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "GrpChargeBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LMGrpChargeDB tLMGrpChargeDB = new LMGrpChargeDB(conn);
            tLMGrpChargeDB.setSchema(tLMGrpChargeSchema);
            if (!tLMGrpChargeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMGrpChargeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpChargeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpChargeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        GrpChargeBLS grpChargeBLS1 = new GrpChargeBLS();
    }
}
