package com.sinosoft.lis.agentcharge;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.schema.LAChargePayBatchLogSchema;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ChargePayInfoRegister {
	private Logger cLogger = Logger.getLogger(getClass());

	/** 往工作流引擎中传输数据的容器 */

	private VData mInputData = new VData();
	// 传输数据类
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/**
	 * 发送报文的保单数据封装类
	 */
	private TransferData mOutxml = new TransferData();
	/**
	 * 凭证登记码
	 */
	private String mVoucherRegNo = "";
	/**
	 * 批次号
	 */
	private String mBatchNo = "";
	/**
	 * 本次支付总金额
	 */
	private double mTotalAmount = 0;
	/**
	 * 币种类型
	 */
	private String mCurrencyCode ="CNY";
	/**
	 * 操作人
	 */
	private String mOperator ="";
	/**
	 * 中介机构与批次号一一对应
	 * 为了以后支持结算时可以多个批次号
	 */
//	private Map<String,String> mAgentComToBatchNo = new HashMap();
	private ExeSQL mExeSQL = new ExeSQL();
	private LAChargeSet mLAChargeSet = new LAChargeSet();

	public boolean submitData(VData cInputData, String cOperate) {
		// 首先将数据在本类中做一个备份
		mInputData = cInputData;
		System.out.println("Start ContInputChargecomChkBL Submit...");
		if (!getInputData(mInputData, cOperate)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		try {
			if (!dealData()) {
				return false;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println("End ContInputChargecomChkBL Submit...");

		mInputData = null;
		return true;
	}

	private boolean checkData() {

		String mOutxmlSql ="select d.BankAccOpen,d.bankaccname,d.bankaccno from lacom d where d.agentcom in (select distinct agentcom from lacharge where batchno='"+mBatchNo+"')";
		SSRS tssrs = new SSRS();
		tssrs = mExeSQL.execSQL(mOutxmlSql);
		String tBankName = tssrs.GetText(1, 1);
		String tAccountName = tssrs.GetText(1, 2);
		String tAccountNumber = tssrs.GetText(1, 3);
		String mOutxmlSql1 ="select sum(SumGetMoney),modifydate,max(modifytime) from ljaget a where " +
		"a.getnoticeno in (select commisionsn from lacharge where batchno='"+mBatchNo+"' ) group by bankcode,drawer,bankaccno,modifydate ";
		SSRS tssrs1 = new SSRS();
		tssrs1 = mExeSQL.execSQL(mOutxmlSql1);
		String tAmount = tssrs1.GetText(1, 1);
		String tMakeDate = tssrs1.GetText(1, 2);
		String tMakeTime = tssrs1.GetText(1, 3);
//		String tPayTime =tMakeDate.replaceAll("-", "")+ tMakeTi me.substring(0,tMakeTime.lastIndexOf(":")).replace(":", "");
		String tPayTime =tMakeDate+" "+ tMakeTime;
		mOutxml.setNameAndValue("BankName", tBankName);
		mOutxml.setNameAndValue("AccountName", tAccountName);
		mOutxml.setNameAndValue("Amount", tAmount);
		mOutxml.setNameAndValue("AccountNumber", tAccountNumber);
		mOutxml.setNameAndValue("PayTime", tPayTime);
//		mOutxml.setNameAndValue("PayTime", "2015-07-03 10:59:00");
		mOutxml.setNameAndValue("TotalAmount", tAmount);
		return true;
	}

	private boolean dealData() throws FileNotFoundException {
		String tresult = queryChargeInfo();
		if (!"".equals(tresult) && tresult != null) {
			VData cVData = new VData();
			TransferData cTransferData = new TransferData();
			cTransferData.setNameAndValue("XML", tresult);
			cTransferData.setNameAndValue("BatchNo", mBatchNo);
			cTransferData.setNameAndValue("Operator", this.mOperator);
			cVData.add(cTransferData);
			DealLAChargeBackXML tDealLAChargeBackXML = new DealLAChargeBackXML();
			boolean tFlag = tDealLAChargeBackXML.submit(cVData, "");
			if(false==tFlag)
			{
				this.mErrors.copyAllErrors(tDealLAChargeBackXML.mErrors);
			}
			else
			{
				LAChargePayBatchLogSchema tLAChargePayBatchLogSchema = new LAChargePayBatchLogSchema();
				tLAChargePayBatchLogSchema = tDealLAChargeBackXML.getLAChargePayBatchLogSchemaByVoucherRegNo(this.mVoucherRegNo);
				tFlag = getResult(tLAChargePayBatchLogSchema);
			}
			return tFlag;
		} else {
			System.out.println("获取返回报文失败！");
			CError tError = new CError();
			tError.moduleName = "ContInputChargecomChkBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询返回报文获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

	}


	private boolean getInputData(VData cInputData, String cOperate) {
		System.out.println("+==============");
		// 从输入数据中得到所有对象
		mOutxml = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mBatchNo = (String) mOutxml.getValueByName("BatchNo");
		String sql = "select VoucherRegNo from lachargepaybatchlog where batchno ='"+mBatchNo+"' with ur";
		mVoucherRegNo = mExeSQL.getOneValue(sql);
		this.mOperator  = (String) mOutxml.getValueByName("Operator");
		return true;
	}

	public String queryChargeInfo() {
		// 根据统一工号调用webservice查询业务员信息
		String tResponseXml = "";
		long mOldTimeMillis = System.currentTimeMillis();
		long mCurTimeMillis = mOldTimeMillis;
		try {
			RPCServiceClient tRPCServiceClient = new RPCServiceClient();
			//把以下的修改成了配置，所以需要查询
//			String url = "http://10.252.4.69:8080/jszjpt/services/SXGX_WS_SERVER?wsdl";
//			String qName = "http://ws.sinosoft.com";
//			String method = "ecService";
			String tSQL ="select code,codename from ldcode where codetype='JSAgentComChk' with ur";
			SSRS tSSRS = mExeSQL.execSQL(tSQL);
			String url = "";
			String qName = "";
			String method = "";
			if(0<tSSRS.getMaxRow())
			{
				for(int i=1;i<=tSSRS.getMaxRow();i++)
				{
					if("url".equals(tSSRS.GetText(i, 1)))
					{
						url = tSSRS.GetText(i, 2);
					}
					else if("qName".equals(tSSRS.GetText(i, 1)))
					{
						qName = tSSRS.GetText(i, 2);
					}
					else if("method".equals(tSSRS.GetText(i, 1)))
					{
						method = tSSRS.GetText(i, 2);
					}
					
				}
			}
			Options options = tRPCServiceClient.getOptions();
			EndpointReference targetEPR = new EndpointReference(url);
			options.setTo(targetEPR);
			options.setTimeOutInMilliSeconds(120000);//
			QName opAddEntry = new QName(qName, method);
			String inputXml = getinputXml(); // 这里是中介机构校验接口请求报文
			// String inputXml
			// ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Packet type=\"REQUEST\" version=\"1.0\" channel=\"0\"><Head><RequestType>C01</RequestType></Head><Body><Policy><SerialNo>00000000000000000007</SerialNo><prtno>18150206002</prtno><AgencyCode>PC3200000592</AgencyCode><AgencyType>01</AgencyType><ComCode>PICC</ComCode><Coverage><CoverageType>H</CoverageType></Coverage><Coverage><CoverageType>H</CoverageType></Coverage><PolicyApplyDate>2015-02-06 15:23:11</PolicyApplyDate><PolicyFlag>01</PolicyFlag></Policy></Body></Packet>";
			String serialNo = "PICCjs_86320000";// 这里是外部交易流水号，可为空
			Object[] opAddEntryArgs = new Object[] { inputXml, "HX",
					"C12", serialNo };
			Class[] classes = new Class[] { String.class, String.class,
					String.class, String.class };
			mCurTimeMillis = System.currentTimeMillis();
			cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis)
					/ 1000.0 + "s");

			cLogger.info("开始调用WebService服务！" + url);
			tResponseXml = (String) tRPCServiceClient.invokeBlocking(
					opAddEntry, opAddEntryArgs, classes)[0];
			mCurTimeMillis = System.currentTimeMillis();
			cLogger.info("调用WebService服务成功！耗时："
					+ (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
			System.out.println("返回的报文：" + tResponseXml);
		} catch (Exception e) {
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "ContInputChargecomChkBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "发送校验报文失败!";
			this.mErrors.addOneError(e.getMessage());
		}
		return tResponseXml;
	}

	private String getinputXml() {
		Element tRootData = new Element("Packet");
		tRootData.addAttribute("type", "REQUEST");
		tRootData.addAttribute("version", "1.0");
		Element tHeadData = new Element("Head");
		Element tRequestType = new Element("RequestType");
		tRequestType.setText("C12");
		tHeadData.addContent(tRequestType);
		tRootData.addContent(tHeadData);
		Element tbody = new Element("Body");
		Element tVoucherPaymentInfo = new Element("VoucherPaymentInfo");
		Element VoucherRegNo = new Element("VoucherRegNo");
		VoucherRegNo.setText(this.mVoucherRegNo);
		tVoucherPaymentInfo.addContent(VoucherRegNo);
		Element tTotalAmount = new Element("TotalAmount");
		tTotalAmount.setText((String) mOutxml.getValueByName("TotalAmount"));
		tVoucherPaymentInfo.addContent(tTotalAmount);
		Element tCurrencyCode = new Element("CurrencyCode");
		tCurrencyCode.setText(mCurrencyCode);
		tVoucherPaymentInfo.addContent(tCurrencyCode);
		Element tStatementsItem = new Element("StatementsItem");
		Element tBatchNo = new Element("BatchNo");
		tBatchNo.setText(this.mBatchNo);
		tStatementsItem.addContent(tBatchNo);
		Element tBankName = new Element("BankName");
		tBankName.setText((String) mOutxml.getValueByName("BankName"));
		tStatementsItem.addContent(tBankName);
		Element tAccountName = new Element("AccountName");
		tAccountName.setText((String) mOutxml.getValueByName("AccountName"));
		tStatementsItem.addContent(tAccountName);
		Element tAccountNumber = new Element("AccountNumber");
		tAccountNumber.setText((String) mOutxml.getValueByName("AccountNumber"));
		tStatementsItem.addContent(tAccountNumber);
		Element tAmount = new Element("Amount");
		tAmount.setText((String) mOutxml.getValueByName("Amount"));
		tStatementsItem.addContent(tAmount);
		Element tCurrencyCode1 = new Element("CurrencyCode");
		tCurrencyCode1.setText(mCurrencyCode);
		tStatementsItem.addContent(tCurrencyCode1);
		Element tPayTime = new Element("PayTime");
		tPayTime.setText((String) mOutxml.getValueByName("PayTime"));
		tStatementsItem.addContent(tPayTime);
		Element tPayStatus = new Element("PayStatus");
		tPayStatus.setText("02");
		tStatementsItem.addContent(tPayStatus);
		tVoucherPaymentInfo.addContent(tStatementsItem);
		tbody.addContent(tVoucherPaymentInfo);
		tRootData.addContent(tbody);
		Document tDocument = new Document(tRootData);
		JdomUtil.print(tDocument.getRootElement());
		return DomtoString(tDocument);
	}

	private String DomtoString(Document doc) {
		XMLOutputter xmlout = new XMLOutputter();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		try {
			xmlout.setEncoding("GBK");
			xmlout.output(doc, bo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String xmlStr = bo.toString().replace("GBK", "UTF-8");
		System.out.println("发送报文：" + xmlStr);
		return xmlStr;
	}

	public boolean getResult(LAChargePayBatchLogSchema cLAChargePayBatchLogSchema)
	{
		String  tResponseCode = cLAChargePayBatchLogSchema.getRegisterResponseCode();
		if("1".equals(tResponseCode))
		{
			return true;
		}
		return false;
	}
	public static void main(String[] args) {
		ChargePayInfoRegister tLAChargeCheckBL = new ChargePayInfoRegister();
		VData tVData = new VData();
		TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("BatchNo", "20150702863200000001");
		tTransferData.setNameAndValue("Operator", "001");
		tVData.add(tTransferData);
		tLAChargeCheckBL.submitData(tVData, "");
	}
}

