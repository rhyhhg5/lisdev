/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-19 09:58:25
 */
package com.sinosoft.lis.agentcharge;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ChAudFewUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] agrs)
    {
        String cOperate = "INSERT||MAIN";
        String tChargeType = "11";
        String tChargeType2 = "FEW";
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
        tLACommisionSchema.setGrpContNo("0000008701");
        tLACommisionSet.add(tLACommisionSchema);

        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak002";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "mak002";

        cInputData.add(tChargeType);
        cInputData.add(tChargeType2);
        cInputData.add(tGlobalInput);
        cInputData.add(tLACommisionSet);

        ChAudFewUI tChAudFewUI = new ChAudFewUI();
        boolean actualReturn = tChAudFewUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        ChAudFewBL tChAudFewBL= new ChAudFewBL();
        tChAudFewBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tChAudFewBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tChAudFewBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAGroupUniteUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        return true;
    }
}
