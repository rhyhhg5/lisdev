package com.sinosoft.lis.colltb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CollectContPlanRiskUI
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public CollectContPlanRiskUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CollectContPlanRiskBL tCollectContPlanRiskBL = new CollectContPlanRiskBL();
            if (!tCollectContPlanRiskBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCollectContPlanRiskBL.mErrors);
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }

    /**
     * 调试主函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
    }
}
