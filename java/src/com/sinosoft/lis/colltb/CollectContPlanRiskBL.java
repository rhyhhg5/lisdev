package com.sinosoft.lis.colltb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 生成默认保障计划下的险种信息。
 * <p>条件：</p>
 * <ul>
 * <li>只有一个保障计划。</li>
 * <li>每次只针对一个险种进行处理。</li>
 * </ul>
 * 
 * @author LY
 *
 */
public class CollectContPlanRiskBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mGrpContNo = "";

    private String mGrpPolNo = null;

    /** 保险计划要素信息　*/
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();

    public CollectContPlanRiskBL()
    {
    }

    /**
     * ＵＩ接口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubsubmit = new PubSubmit();
        if (!tPubsubmit.submitData(this.mResult, ""))
        {
            this.mErrors.copyAllErrors(tPubsubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 从前台获取数据
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet) cInputData
                .getObjectByObjectName("LCContPlanDutyParamSet", 0);

        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            buildError("checkData", "处理超时，请重新登录。");
            return false;
        }

        if (mTransferData == null)
        {
            buildError("checkData", "所需参数不完整。");
            return false;
        }

        if (mLCContPlanDutyParamSet == null
                || mLCContPlanDutyParamSet.size() <= 0)
        {
            System.out.println("参数LCContPlanDutyParamSet数据集为空。");
            buildError("checkData", "要素信息传入失败！");
            return false;
        }

        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            System.out.println("查询团体合同信息失败！");
            buildError("checkData", "查询团体合同信息失败！");
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());

        if (CollectContCommonBL.getCountOfSubCont(mGrpContNo) > 0)
        {
            System.out.println("已生成分单信息，不能进行修改！");
            buildError("checkData", "已生成分单信息，不能进行修改！");
            return false;
        }

        return true;
    }

    /**
     * 数据处理
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Operate : " + this.mOperate);
        if (this.mOperate.equals("INSERT||CONTPLAN"))
        {
            if (!insertData())
                return false;
        }
        else if (this.mOperate.equals("DELETE||CONTPLAN"))
        {
            if (!deleteData())
                return false;
        }

        return true;
    }

    /**
     * 准备向后台传输
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        if (this.mOperate.equals("INSERT||CONTPLAN"))
        {
            //            this.map.put(this.mLCContPlanDutyParamSet, SysConst.INSERT);
            //            this.map.put(this.mLCContPlanRiskSet, SysConst.INSERT);
            //            this.map.put(this.mLCContPlanSchema, SysConst.INSERT);
            //            this.map.put(this.mLCGrpPolSet, SysConst.INSERT);
        }
        System.out.println("map : " + map.size());
        if (map.size() > 0)
        {
            this.mResult.add(this.map);
        }
        return true;
    }

    /**
     * 执行插入操作
     * @return boolean
     */
    private boolean insertData()
    {
        MMap tTmpMMap = null;

        // 不允许重复录入险种。
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        if (findRiskPol(tRiskCode))
        {
            System.out.println("该险种已存在。");
            buildError("insertData", "该险种已存在！");
            return false;
        }
        // -------------------------

        // 获取要增加险种的描述信息。
        LMRiskSchema tLMRiskSchema = loadRiskInfo(tRiskCode);
        if (null == tLMRiskSchema)
        {
            System.out.println("查询险种失败！");
            buildError("insertData", "查询险种失败！");
            return false;
        }
        // -------------------------

        // 创建默认保障计划
        tTmpMMap = createContPlan();
        if (null != tTmpMMap)
        {
            map.add(tTmpMMap);
            tTmpMMap = null;
        }
        // -------------------------

        // 创建团体险种信息
        tTmpMMap = createGrpPol(tLMRiskSchema);
        if (null != tTmpMMap)
        {
            map.add(tTmpMMap);
            tTmpMMap = null;
        }
        // -------------------------

        // 创建默认保障计划下险种信息。
        tTmpMMap = createContPlanRisk(tLMRiskSchema);
        if (null != tTmpMMap)
        {
            map.add(tTmpMMap);
            tTmpMMap = null;
        }
        // -------------------------

        // 创建险种要素信息。
        tTmpMMap = createContPlanRiskDutyFactor();
        if (null != tTmpMMap)
        {
            map.add(tTmpMMap);
            tTmpMMap = null;
        }
        // -------------------------

        return true;
    }

    /**
     * 删除操作<br>
     * 1、先删除要素信息<br>
     * 2、删除险种计划<br>
     * 3、删除计划<br>
     * 4、删除险种
     * @return boolean
     */
    private boolean deleteData()
    {
        String tRiskCode = (String) mTransferData.getValueByName("RiskCode");
        if (!findRiskPol(tRiskCode))
        {
            System.out.println("该险种信息不存在。");
            buildError("deleteData", "该险种信息不存在！");
            return false;
        }

        if (getContCount() > 0)
        {
            System.out.println("已生成分单合同信息。");
            buildError("deleteData", "已生成分单合同信息！");
            return false;
        }

        String tDelDutyFactorSql = " delete from LCContPlanDutyParam "
                + " where GrpContNo = '" + this.mGrpContNo + "' "
                + " and RiskCode = '" + tRiskCode + "' ";
        map.put(tDelDutyFactorSql, SysConst.DELETE);

        String tDelPlanRiskSql = " delete from LCContPlanRisk "
                + " where GrpContNo = '" + this.mGrpContNo + "' "
                + " and RiskCode = '" + tRiskCode + "' ";
        map.put(tDelPlanRiskSql, SysConst.DELETE);

        String tDelGrpPolSql = " delete from LCGrpPol "
                + " where GrpContNo = '" + this.mGrpContNo + "' "
                + " and RiskCode = '" + tRiskCode + "' ";
        map.put(tDelGrpPolSql, SysConst.DELETE);

        if (1 == getGrpPolCount())
        {
            String tDelContPlanSql = " delete from LCContPlan "
                    + " where GrpContNo = '" + this.mGrpContNo + "' ";
            map.put(tDelContPlanSql, SysConst.DELETE);
        }

        return true;
    }

    /**
     * 查找险种是否已经存在。
     * @param cRiskCode 险种代码
     * @return
     */
    private boolean findRiskPol(String cRiskCode)
    {
        String tStrSql = "select count(1) from LCGrpPol "
                + " where GrpContNo = '" + mGrpContNo + "' "
                + " and RiskCode = '" + cRiskCode + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);

        return !"0".equals(tResult);
    }

    /**
     * 获取保单下已导入保单数量。
     * @return  导入保单份数
     */
    private int getContCount()
    {
        String tStrSql = " select count(1) from LCCont "
                + " where GrpContNo = '" + mGrpContNo + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        return Integer.parseInt(tResult);
    }

    /**
     * 获取险种描述信息。
     * @param tRiskCode 险种代码
     * @return 险种描述信息，如该种描述不存在，则返回 null 。
     */
    private LMRiskSchema loadRiskInfo(String tRiskCode)
    {
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(tRiskCode);
        if (!tLMRiskDB.getInfo())
        {
            return null;
        }

        return tLMRiskDB.getSchema();
    }

    /**
     * 创建默认保障计划。如果保障计划存在，则会覆盖。
     * @return 结果集事务包
     */
    private MMap createContPlan()
    {
        MMap tMMap = new MMap();

        LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();

        tLCContPlanSchema.setGrpContNo(mGrpContNo);
        tLCContPlanSchema.setProposalGrpContNo(mLCGrpContSchema
                .getProposalGrpContNo());
        tLCContPlanSchema.setContPlanCode("00");
        tLCContPlanSchema.setContPlanName("默认计划");
        tLCContPlanSchema.setPlanType("0");
        tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCContPlanSchema);

        tMMap.put(tLCContPlanSchema, SysConst.DELETE_AND_INSERT);

        return tMMap;
    }

    /**
     * 创建团体险种信息。
     * @param tLMRiskSchema 新增的险种信息。
     * @return 结果集事务包
     */
    private MMap createGrpPol(LMRiskSchema tLMRiskSchema)
    {
        MMap tMMap = new MMap();

        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();

        // 生成险种流水号
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        mGrpPolNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
        // --------------------

        tLCGrpPolSchema.setRiskCode(tLMRiskSchema.getRiskCode());

        tLCGrpPolSchema.setGrpPolNo(mGrpPolNo);
        tLCGrpPolSchema.setGrpProposalNo(mGrpPolNo);

        tLCGrpPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCGrpPolSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCGrpPolSchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
        tLCGrpPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCGrpPolSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLCGrpPolSchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLCGrpPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCGrpPolSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLCGrpPolSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLCGrpPolSchema.setAddressNo(mLCGrpContSchema.getAddressNo());
        tLCGrpPolSchema.setGrpName(mLCGrpContSchema.getGrpName());
        tLCGrpPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
        tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());

        tLCGrpPolSchema.setAppFlag("0");
        tLCGrpPolSchema.setUWFlag("0");
        tLCGrpPolSchema.setApproveFlag("0");
        tLCGrpPolSchema.setOperator(mGlobalInput.Operator);

        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++)
        {
            if (tLCGrpPolSchema.getRiskCode().equals(
                    mLCContPlanDutyParamSet.get(i).getRiskCode()))
            {
                if (mLCContPlanDutyParamSet.get(i).getCalFactor()
                        .equals("Mult"))
                {
                    tLCGrpPolSchema.setMult(mLCContPlanDutyParamSet.get(i)
                            .getCalFactorValue());
                    break;
                }
                //                else if (mLCContPlanDutyParamSet.get(i).getCalFactor().equals(
                //                        "Amnt"))
                //                {
                //                    tLCGrpPolSchema.setAmnt(mLCContPlanDutyParamSet.get(i)
                //                            .getCalFactorValue());
                //                    break;
                //                }
            }
        }
        PubFun.fillDefaultField(tLCGrpPolSchema);

        tMMap.put(tLCGrpPolSchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 创建计划下险种信息。
     * @param tLMRiskSchema 新增的险种信息。
     * @return 结果集事务包
     */
    private MMap createContPlanRisk(LMRiskSchema tLMRiskSchema)
    {
        MMap tMMap = new MMap();

        String tMainRiskCode = (String) mTransferData
                .getValueByName("MainRiskCode");

        String tProposalGrpContNo = mLCGrpContSchema.getProposalGrpContNo();

        LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();

        tLCContPlanRiskSchema.setGrpContNo(this.mGrpContNo);
        tLCContPlanRiskSchema.setProposalGrpContNo(tProposalGrpContNo);
        tLCContPlanRiskSchema.setMainRiskCode(tMainRiskCode);
        //tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskDB.getRiskVer());
        tLCContPlanRiskSchema.setRiskCode(tLMRiskSchema.getRiskCode());
        tLCContPlanRiskSchema.setRiskVersion(tLMRiskSchema.getRiskVer());

        tLCContPlanRiskSchema.setContPlanCode("00");
        tLCContPlanRiskSchema.setContPlanName("默认计划");
        tLCContPlanRiskSchema.setPlanType("0");
        tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);

        PubFun.fillDefaultField(tLCContPlanRiskSchema);

        tMMap.put(tLCContPlanRiskSchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 创建险种要素信息。
     * @return 结果集事务包
     */
    private MMap createContPlanRiskDutyFactor()
    {
        MMap tMMap = new MMap();

        if (null == mGrpPolNo || "".equals(mGrpPolNo))
        {
            return null;
        }

        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = null;
        String tProposalGrpContNo = mLCGrpContSchema.getProposalGrpContNo();

        for (int i = 1; i <= mLCContPlanDutyParamSet.size(); i++)
        {
            tLCContPlanDutyParamSchema = mLCContPlanDutyParamSet.get(i);

            tLCContPlanDutyParamSchema.setGrpPolNo(mGrpPolNo);

            tLCContPlanDutyParamSchema.setGrpContNo(mGrpContNo);
            tLCContPlanDutyParamSchema.setProposalGrpContNo(tProposalGrpContNo);

            tLCContPlanDutyParamSchema.setContPlanCode("00");
            tLCContPlanDutyParamSchema.setContPlanName("默认计划");
            tLCContPlanDutyParamSchema.setPlanType("0");

            tLCContPlanDutyParamSchema = null;
        }

        PubFun.fillDefaultField(mLCContPlanDutyParamSet);

        tMMap.put(mLCContPlanDutyParamSet, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 获取保障计划下险种个数。
     * @return 已存在险种的个数。
     */
    private int getGrpPolCount()
    {
        String tStrSql = "select count(1) from LCGrpPol "
                + " where GrpContNo = '" + mGrpContNo + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);

        return Integer.parseInt(tResult);
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CollectContPlanRiskBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
