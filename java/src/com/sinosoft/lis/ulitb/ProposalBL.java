/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.ulitb;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.HashMap;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.bq.BqCalBL;
import com.sinosoft.lis.certify.PubCertifyTakeBack;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */
/**************************************************************************************************** 
 * Fang 2009-01-16 ASR20081922 界面调整 
 * 修改一、人员上载如果被保险人年龄不在投保范围，系统提示不明确，不能确定是哪个被保险人：提示信息中增加姓名的显示
 * **************************************************************************************************
 **/
public class ProposalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /**专享连接*/
	private Connection con;
	private boolean isConCanUse = false;
    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();
    /** 投保单类型 */
    private String mAppntType = "1"; // 默认为个人投保人
    private String mPolType = "1"; // 默认为个人投保单类型
    private boolean TakeBackCertifyFalg = false; //是否回收单证标记
    private String OriginOperator = ""; //更新时保存原先保单的操作员
//    private double OriginPrem = 0; //更新时保存原先保单的实际保费
//    private double OriginStandPrem = 0; //更新时保存原先保单的标准保费
    /* 处理浮动费率的变量 */
    private double[] FloatRate = null; //浮动费率 用于记录多责任不同的统一费率
    private double InputPrem = 0; //保存界面录入的保费
    private double InputAmnt = 0; //保存界面录入的保额
//    private final boolean autoCalFloatRateFlag = false; //是否自动计算浮动费率,如果界面传入浮动费率=ConstRate，自动计算
//    private final double ConstRate = -1; //如果标明需要从输入的保费保额计算浮动费率，那么界面传入的浮动费率值为常量
    /*转换精确位数的对象   */
//    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
//    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象
    private final int mSCALE=2;//保费保额计算出来后的精确位数
//    private String FRateFORMATMODOL = "0.000000000000"; //浮动费率计算出来后的精确位数
//    private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象
    /* 不需要保费保额计算的标记，用于特殊的险种。譬如：众悦年金分红 */
    private boolean noCalFlag = false;
    
    private ExeSQL mExeSQL;
//    private boolean deleteAccNo = false;
    //保存投保单对应的操作类型,默认是投保单标记(其他可能是保全类型)
    private String mSavePolType = "0";
    //是否文件投保标记
    boolean mGrpImportFlag = false;
    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();
    /** 业务处理相关变量 */
    /** 接受前台传输数据的容器 */
    private TransferData mTransferData = new TransferData();
    
    
    private Reflections mref = new Reflections();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /**合同表*/
    private LCContSchema mLCContSchema = new LCContSchema();
    /** 个人客户表 */
    private LDPersonSet mLDPersonSet = new LDPersonSet();
    /** 银行账户表 */
//    private LCBankAccBL mLCBankAccBL = new LCBankAccBL();
    /** 银行授权书表 */
//    private LCBankAuthBL mLCBankAuthBL = new LCBankAuthBL();
    /** 保单 */
    private LCPolBL mLCPolBL = new LCPolBL();
    //主险保单
    private LCPolBL mainLCPolBL = new LCPolBL();
    //private LCGrpPolBL mLCGrpPolBL = new LCGrpPolBL();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); //磁盘投保时，该集体信息从外部传入，否则内部查询
    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema(); //磁盘投保时，该集体信息从外部传入，否则内部查询
    /**险种承保描述表 */
    private LMRiskAppSchema mLMRiskAppSchema = new LMRiskAppSchema();
    /**险种描述表 */
    private LMRiskSchema mLMRiskSchema = new LMRiskSchema();
//    private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
    /**被保人联系地址*/
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    
    private LCAccountSchema mLCAccountSchema = new LCAccountSchema();
    
    /** 投保人 */
    private LCAppntBL mLCAppntBL = new LCAppntBL();
    private LCGrpAppntBL mLCGrpAppntBL = new LCGrpAppntBL();
    /** 被保人 */
    private LCInsuredBL mLCInsuredBL = new LCInsuredBL();
    private LCInsuredBLSet mLCInsuredBLSet = new LCInsuredBLSet();
//    private LCInsuredBLSet mLCInsuredBLSetNew = new LCInsuredBLSet();
    /** 连带，连生被保人 */
    private LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();
//    private LCInsuredRelatedSet newLCInsuredRelatedSet = new            LCInsuredRelatedSet();
    /** 受益人 */
    private LCBnfBLSet mLCBnfBLSet = new LCBnfBLSet();
    private LCBnfBLSet mLCBnfBLSetNew = new LCBnfBLSet();
    /** 告知信息 */
    private LCCustomerImpartBLSet mLCCustomerImpartBLSet = new LCCustomerImpartBLSet();
    private LCCustomerImpartBLSet mLCCustomerImpartBLSetNew = new LCCustomerImpartBLSet();
    /** 特别约定 */
    private LCSpecBLSet mLCSpecBLSet = new LCSpecBLSet();
    private LCSpecSet mLCSpecBLSetNew = new LCSpecBLSet();
    /** 保费项表*/
    private LCPremBLSet mLCPremBLSet = new LCPremBLSet();
    private LCPremBLSet mDiskLCPremBLSet = new LCPremBLSet(); //保存特殊的保费项数据(目前针对磁盘投保，不用计算保费保额类型)
    /** 给付项表*/
    private LCGetBLSet mLCGetBLSet = new LCGetBLSet();
    //一般的责任信息
    private LCDutyBL mLCDutyBL = new LCDutyBL();
    /** 责任表*/
    private LCDutyBLSet mLCDutyBLSet = new LCDutyBLSet();
    /** 可选责任信息 */
    private LCDutyBLSet mLCDutyBLSet1 = new LCDutyBLSet();
    private LCDutySchema ttLCDutySchema1=new LCDutySchema();
    private boolean mNeedDuty = false;
    /** 应收总表 **/
    LJSPaySet mLJSPaySet = new LJSPaySet();
    private String mChangePlanFlag = "1";
    private String EdorType = "";
    private String mEdorNo = "";
    private String preEdorType = "";
	private LMCalModeSchema mLMCalMode;
    private int aflag1=0;

	
	
    public ProposalBL()
    {
    	 mExeSQL = new ExeSQL();
    }
    
    public ProposalBL(Connection tConnection)
    {
    con = tConnection;
    isConCanUse = true;
    mExeSQL = new ExeSQL(con);

    }


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据操作校验
        if (!checkData())
        {
            return false;
        }

        //特殊校验处理针对险种－对民生用，可单独屏蔽不用,不影响后面
        if (!preSpecCheck())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (!this.mOperate.equals("DELETE||PROPOSAL"))
        {
            if (this.mOperate.equals("INSERT||PROPOSAL"))
            {
                if (!CheckTBField("INSERT"))
                {
                    return false;
                }
            }
            else
            {
                if (!CheckTBField("UPDATE"))
                {
                    return false;
                }
            }
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        
      if ("GA".equals(this.EdorType)||"AP".equals(this.EdorType)) {
			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mInputData, "")) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError.buildErr(this, "数据提交失败!");
				return false;
			}

		} else {

			ProposalBLS tProposalBLS = new ProposalBLS();
			tProposalBLS.submitData(mInputData, cOperate);

			// 如果有需要处理的错误，则返回
			if (tProposalBLS.mErrors.needDealError()) {
				// @@错误处理
				this.mErrors.copyAllErrors(tProposalBLS.mErrors);
				CError.buildErr(this, "数据提交失败!");
				return false;
			}

		}
        	
        // System.out.println("TakeBackCertifyFalg:"+TakeBackCertifyFalg);
        if (TakeBackCertifyFalg) //如果回收单证标记为真(针对个人投保单)
        {
            try
            {
                //单证回收
                PubCertifyTakeBack tPubCertifyTakeBack = new PubCertifyTakeBack();
                if (tPubCertifyTakeBack.CheckNewType(tPubCertifyTakeBack.
                        CERTIFY_CheckNo2)) //如果需要单证回收
                {
                    String operator = mGlobalInput.Operator;
                    String CertifyType = "";

                    //System.out.println("getPrtNo:"+mLCPolBL.getPrtNo());
                    if (mLCPolBL.getPrtNo().substring(2, 4).equals("15")) //银行险的编码15，个险11，团险12
                    {
                        CertifyType = tPubCertifyTakeBack.CERTIFY_ProposalBank;
                    }
                    else
                    {
                        if (mLCPolBL.getPrtNo().substring(2, 4).equals("14"))
                        {
                            CertifyType = tPubCertifyTakeBack.CERTIFY_ProSpec;
                        }
                        else if (mLCPolBL.getPrtNo().substring(2,
                                4).equals("16"))
                        {
                            CertifyType = tPubCertifyTakeBack.CERTIFY_ProSimple;
                        }
                        else
                        {
                            CertifyType = tPubCertifyTakeBack.CERTIFY_Proposal;
                        }
                    }

                    //System.out.println("CertifyType:"+CertifyType);
                    if (!tPubCertifyTakeBack.CertifyTakeBack_A(mLCPolBL.getPrtNo()
                            , mLCPolBL.getPrtNo(), CertifyType, operator))
                    {
                        System.out.println("单证回收错误（个人投保单）:"
                                + mLCPolBL.getPrtNo());
                        System.out.println("错误原因："
                                +
                                tPubCertifyTakeBack.mErrors.getFirstError()
                                .toString());

                        //保存错误信息
                    }
                }
            }
            catch (Exception e)
            {
                mErrors.addOneError(e.toString());
            }
        }
        mInputData = null;

        return true;
    }

    /**
     * 不包含校验的承保数据处理，不保存数据
     * @param cInputData VData
     * @param cOperate String
     * @return VData
     */
    public VData appNoChk(VData cInputData, String cOperate)
    {
        VData tReturn = null;

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return null;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败!";
            this.mErrors.addOneError(tError);

            return null;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return null;
        }

        // 检查是否出错
        if (!this.mErrors.needDealError())
        {
            tReturn = mInputData;
        }

        return tReturn;
    }

    /**
     * 数据处理校验
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean checkData()
    {

        // 判断投保单的类型 1--个人 2--集体 3--合同 //PQ 应该没有mPolType='3'的情况
        if (!mLCPolBL.getContType().equals("1"))
        {
            mPolType = "2";
            mAppntType = "2";
        }

        if (mOperate.equals("INSERT||PROPOSAL")
                || mOperate.equals("UPDATE||PROPOSAL"))
        {
            if (mLCPolBL.getPolTypeFlag() == null) //保单类型标记0--个人单，1--无名单，2 ---公共帐户)
            {
                mLCPolBL.setPolTypeFlag("0"); //缺省设为个人单
            }
            if (mLCPolBL.getAppFlag() == null ||"".equals(mLCPolBL.getAppFlag())||
                    mLCGrpPolSchema.getAppFlag().equals("")) //签单标记为空时，补为0
            {
                String tSavePolType = (String) mTransferData.getValueByName(
                        "SavePolType");
                if (tSavePolType != null)
                {
                    if (tSavePolType.trim().equals(""))
                    {
                        mLCPolBL.setAppFlag("0");
                    }
                    else
                    {
                        mLCPolBL.setAppFlag(tSavePolType);
                    }
                }
                else
                {
                    mLCPolBL.setAppFlag("0");
                }

            }
            if (mPolType.equals("2"))
            {
                if (mLCGrpContSchema == null)
                {
                    //如果是团单，从外界传入的险种承保描述信息和险种描述信息为空，那么从数据库查询
                    //LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
                    LCGrpContDB tLCGrpContDB = null;
                    if(this.isConCanUse)
                    	tLCGrpContDB = new LCGrpContDB(this.con);
                    else
                    	tLCGrpContDB = new LCGrpContDB();
                    tLCGrpContDB.setGrpContNo(mLCPolBL.getGrpContNo());

                    if (tLCGrpContDB.getInfo())
                    {
                        mLCGrpContSchema = tLCGrpContDB.getSchema();
                        // mLCGrpContSchema.setSchema(tLCGrpContDB);
                    }
                    else
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                        CError.buildErr(this,"集体(投)合同表取数失败");
                        return false;
                    }
                }
                if (mLCGrpPolSchema == null)
                {
                    //如果是团单，从外界传入的险种承保描述信息和险种描述信息为空，那么从数据库查询
                    //LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
                    LCGrpPolDB tLCGrpPolDB = null;
                    if(this.isConCanUse)
                    	tLCGrpPolDB = new LCGrpPolDB(this.con);
                    else
                    	tLCGrpPolDB = new LCGrpPolDB();
                    tLCGrpPolDB.setGrpContNo(mLCPolBL.getGrpContNo());
                    tLCGrpPolDB.setRiskCode(mLCPolBL.getRiskCode());

                    LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
                    if (tLCGrpPolDB.mErrors.needDealError()) {
						// @@错误处理
						this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
						CError.buildErr(this, "集体险种表取数失败");
						return false;
					}
                    if (tLCGrpPolSet.size() == 0 && !"GA".equals(this.EdorType)) {
						CError.buildErr(this, "集体险种表取数失败");
						return false;
					}
                    if ("AP".equals(this.EdorType)) {
						String tEdorNo = (String) mTransferData
								.getValueByName("SavePolType");
						if (StrTool.cTrim(tEdorNo).equals("")) {
							CError.buildErr(this, "未传入保全标志！");
							return false;
						}
						mEdorNo = tEdorNo;
						this.mSavePolType = "2";
						mLCPolBL.setAppFlag(mSavePolType);
					}
                    if (tLCGrpPolSet.size() == 0 && "GA".equals(this.EdorType)) {
						String tEdorNo = (String) mTransferData
								.getValueByName("SavePolType");
						if (StrTool.cTrim(tEdorNo).equals("")) {
							CError.buildErr(this, "未传入保全标志！");
							return false;
						}
						LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
						tLPEdorItemDB.setEdorNo(tEdorNo);
						tLPEdorItemDB.setEdorType(EdorType);
						tLPEdorItemDB.setContNo(mLCPolBL.getContNo());
						tLPEdorItemDB.setSchema(tLPEdorItemDB.query().get(1));
						LCContDB tLCContDB = new LCContDB();
						tLCContDB.setContNo(mLCPolBL.getContNo());
						tLCContDB.getInfo();
						
						mLCPolBL.setCValiDate(tLPEdorItemDB.getEdorValiDate());
						mLCPolBL.setManageCom(tLCContDB.getManageCom());
						mLCPolBL.setAgentType(tLCContDB.getAgentType());
						mLCPolBL.setAgentCode1(tLCContDB.getAgentCode1());
						mLCPolBL.setSaleChnl(tLCContDB.getSaleChnl());
						mEdorNo = tEdorNo;
						this.mSavePolType = "2";
						mLCPolBL.setAppFlag(mSavePolType);
						LPPolDB tLPPolDB = new LPPolDB();
						LPPolSchema tLPPolSchema = new LPPolSchema();
						mref
								.transFields(tLPPolSchema, mLCPolBL
										.getSchema());
						tLPPolDB.setSchema(tLPPolSchema);
						tLPPolDB.setEdorNo(mEdorNo);
						tLPPolDB.setEdorType(EdorType);
						String tsql;/* = "select * from LPPol where edorno = '"
								+ tEdorNo + "' and edortype = '" + EdorType
								+ "' and grpcontno = '"
								+ mLCGrpContSchema.getGrpContNo()
								+ "' and riskcode = '" + mLCPolBL.getRiskCode()
								+ "' and rownum = 1";*/
						LPGrpPolDB tLPGrpPolDB = new LPGrpPolDB();
						tLPGrpPolDB.setEdorNo(mEdorNo);
						tLPGrpPolDB.setEdorType(EdorType);
						tLPGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
						tLPGrpPolDB.setRiskCode(mLCPolBL.getRiskCode());
						
						LPGrpPolSet tLPGrpPolSet = tLPGrpPolDB.query();
						if (tLPGrpPolSet.size() > 0)
							tLPPolDB.setGrpPolNo(tLPGrpPolSet.get(1).getGrpPolNo());
						else {
							
							LCPolDB tLCPolDB = new LCPolDB();
							tsql = "select * from LCPol where   grpcontno = '"
									+ mLCGrpContSchema.getGrpContNo()
									+ "' and riskcode = '"
									+ mLCPolBL.getRiskCode()
									+ "' and rownum = 1";
							LCPolSet tLCPolSet = tLCPolDB.executeQuery(tsql);
							if (tLCPolSet.size() > 0) {
								tLPPolDB.setGrpPolNo(tLCPolSet.get(1)
										.getGrpPolNo());

							} else {
								String tGrpPolNo = PubFun1.CreateMaxNo(
										"GRPPOLNO", PubFun
												.getNoLimit(mLCGrpContSchema
														.getManageCom()));
								tLPPolDB.setGrpPolNo(tGrpPolNo);
							}
						}
						LCGrpPolSchema aLCGrpPolSchema = new LCGrpPolSchema();
						mref.transFields(aLCGrpPolSchema, mLCGrpContSchema);
						aLCGrpPolSchema.setGrpPolNo(tLPPolDB.getGrpPolNo());
						aLCGrpPolSchema.setGrpProposalNo(tLPPolDB.getGrpPolNo());
						aLCGrpPolSchema.setRiskCode(mLCPolBL.getRiskCode());
						tLCGrpPolSet.add(aLCGrpPolSchema);

					}
					mLCGrpPolSchema = tLCGrpPolSet.get(1).getSchema();
                }
                mLCPolBL.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());
                mLCPolBL.setAgentCode(mLCContSchema.getAgentCode());
                mLCPolBL.setAgentGroup(mLCContSchema.getAgentGroup());
                mLCPolBL.setAgentCom(mLCContSchema.getAgentCom());
            }

            //如果从外界传入的险种承保描述信息和险种描述信息为空，那么从数据库查询
            if (mLMRiskAppSchema == null)
            {
                LMRiskAppDB tLMRiskAppDB = null;
                if(this.isConCanUse)
                	tLMRiskAppDB = new LMRiskAppDB(this.con);
                else
                	tLMRiskAppDB = new LMRiskAppDB();
                tLMRiskAppDB.setRiskCode(mLCPolBL.getRiskCode());
                if (tLMRiskAppDB.getInfo())
                {
                    mLMRiskAppSchema = tLMRiskAppDB.getSchema();
                }
                else
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "LMRiskApp表查询失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            if (mLMRiskSchema == null)
            {
                LMRiskDB tLMRiskDB =null;
                if(this.isConCanUse)
                	tLMRiskDB = new LMRiskDB(this.con);
                else
                	tLMRiskDB = new LMRiskDB();
                tLMRiskDB.setRiskCode(mLCPolBL.getRiskCode());
                if (tLMRiskDB.getInfo())
                {
                    mLMRiskSchema = tLMRiskDB.getSchema();
                }
                else
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLMRiskDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "LMRiskApp表查询失败!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }

            //如果是个单校验代理人编码并置上代理人组别
            if (mPolType.equals("1"))
            {
                if ((mLCPolBL.getSaleChnl() == null)
                        || mLCPolBL.getSaleChnl().equals(""))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "在投保数据接受时没有得到足够的数据，请您确认有：销售渠道编码!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                if ((mLCPolBL.getAgentCode() == null)
                        || mLCPolBL.getAgentCode().equals(""))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "在投保数据接受时没有得到足够的数据，请您确认有：代理人编码!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                else
                {
                    LAAgentDB tLAAgentDB = null;
                    if(this.isConCanUse)
                    	tLAAgentDB = new LAAgentDB(this.con);
                    else
                    	tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(mLCPolBL.getAgentCode());
                    if (!tLAAgentDB.getInfo())
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage =
                                "在投保数据接受时没有得到正确的数据，请您确认：代理人编码没有输入错误!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if ((tLAAgentDB.getManageCom() == null)
                            || !tLAAgentDB.getManageCom().equals(mLCPolBL
                            .getManageCom()))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage =
                                "代理人编码对应数据库中的管理机构为空或者和您录入的管理机构不符合！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    mLCPolBL.setAgentGroup(tLAAgentDB.getBranchCode());
                }
            }
        }

        if (mOperate.equals("INSERT||PROPOSAL"))
        {

            // 只校验主险

            if (mLMRiskAppSchema.getSubRiskFlag().equals("M"))
            {
                if (!"LC".equals(EdorType)&&!"CS".equals(EdorType))
                { //保全保障变更不需校验 Alex 20050902 调整月薪也不需要校验 zhangkuo 20070517
                    //如果是个人投保单且是主险，则将单证回收的标记为真，用于保存投保单信息后的单证回收模块
                    TakeBackCertifyFalg = true;
                    StringBuffer tSBql = new StringBuffer(128);
                    tSBql.append("select * from LCPol where PrtNo = '");
                    tSBql.append(mLCPolBL.getPrtNo());
                    tSBql.append("' and RiskCode='");
                    tSBql.append(mLMRiskAppSchema.getRiskCode());
                    tSBql.append("' and InsuredNo='");
                    tSBql.append(mLCInsuredBL.getInsuredNo());
                    tSBql.append("'");
                    tSBql.append(" and riskcode <> 'NAK02' ");

//                String sql = "";
//                sql = "select * from LCPol where PrtNo = '" + mLCPolBL.getPrtNo()
//                        + "' and RiskCode='" + mLMRiskAppSchema.getRiskCode() + "' and InsuredNo='"
//                        + mLCInsuredBL.getInsuredNo() + "'";

                    //PQ 印刷号可以有多个主险
                    //一个合同下一个被保险人同一个险种只能录入一个
//                System.out.println("adsfasdfasd" + tSBql.toString());
                    LCPolDB tLCPolDB = null;
                    if(this.isConCanUse)
                    	tLCPolDB = new LCPolDB(this.con);
                    else
                    	tLCPolDB = new LCPolDB();
                    LCPolSet tLCPolSet = tLCPolDB.executeQuery(tSBql.toString());
                    if (tLCPolDB.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "LCPol表查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (tLCPolSet.size() > 0)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        //modify by winnie ASR20093075 错误信息提示不明确
                        tError.errorMessage = "该投保单下被保险人"+mLCInsuredBL.getName()+"已经录入过该险种"+mLMRiskAppSchema.getRiskCode()+"！";
//                        tError.errorMessage = "该投保单下该被保险人已经录入过该险种！";
                        //end 
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    
                    String polStr = "select * "
								  +" from LCPol "
								  +" where PrtNo = '"+mLCPolBL.getPrtNo()+"' "
								  +" and RiskCode = 'NAK02' "
								  +" and InsuredNo = '"+mLCInsuredBL.getInsuredNo()+"' ";
                    
                    LCPolDB aLCPolDB = new LCPolDB();
                    
                    LCPolSet aLCPolSet = aLCPolDB.executeQuery(polStr);
                    
                    if(aLCPolSet.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "LCPol表查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    
                    if (aLCPolSet.size() > 0 && !"NAK02".equals(mLMRiskAppSchema.getRiskCode()))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        
                        tError.errorMessage = "该团体合同下已经存在该被保险人"+mLCInsuredBL.getName()+"，不能重复添加！";

                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    
                }
            }
            // end of if
            else
            {
                //如果是附加险，校验
                //  (1)如果当前要保存的附加险对应的主险下已经存在相同附加险的纪录，那么提示：已经存在不许录入。
                //  (2)如果要保存的附加险的被保人和主险不同，那么不许保存--暂时不做
                //因为保全要做特殊处理
                if (mSavePolType.equals("0"))
                {
/*                  LCPolDB tempLCpolDB = null;
                    if(this.isConCanUse)
                    	tempLCpolDB = new LCPolDB(this.con);
                    else
                    	tempLCpolDB = new LCPolDB();
                    tempLCpolDB.setMainPolNo(mLCPolBL.getMainPolNo());
                    tempLCpolDB.setRiskCode(mLCPolBL.getRiskCode());
                    
                    LCPolSet tempLCPolSet = new LCPolSet();
                    tempLCPolSet = tempLCpolDB.query();
                    if ((tempLCPolSet != null) && (tempLCPolSet.size() > 0))
					{
*/                        
                	//chenwm0102 采用DB查询时可能会因为mainpolno为空,查出很多条数据,导致当机.
                    StringBuffer tSBql = new StringBuffer(128);
                    tSBql.append("select count(*) from  lcpol where mainpolno='");
                    tSBql.append(mLCPolBL.getMainPolNo());
                    tSBql.append("' and riskcode='");
                    tSBql.append(mLCPolBL.getRiskCode());
                    tSBql.append("'");
                    int tCount = Integer.parseInt(mExeSQL.getOneValue(tSBql.toString()));
                    if (tCount>0){
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "该投保单的主险下已经存在相同附加险的投保单！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }
                //保全增加附加险校验，不能重复添加相同的附加险,保障变更不需校验 Alex 20050902
                if (mSavePolType.equals("2") && !"LC".equals(EdorType)&&!"CS".equals(EdorType))
                {
                    LCPolDB tempLCpolDB = null;
                    if(this.isConCanUse)
                    	tempLCpolDB = new LCPolDB(this.con);
                    else
                    	tempLCpolDB = new LCPolDB();
                    if(mLCPolBL.getMainPolNo()==null||mLCPolBL.getMainPolNo().equals("")){
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "没有找到该投保单附加险对应的主险！";
                        this.mErrors.addOneError(tError);
                        return false;
                    	
                    }
                    tempLCpolDB.setMainPolNo(mLCPolBL.getMainPolNo());
                    tempLCpolDB.setRiskCode(mLCPolBL.getRiskCode());

                    LCPolSet tempLCPolSet = new LCPolSet();
                    tempLCPolSet = tempLCpolDB.query();
                    if ((tempLCPolSet != null) && (tempLCPolSet.size() > 0))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "该投保单的主险下已经存在相同附加险的投保单！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }

            }

            //检验生效日期是否小于开办日期
            if (mLMRiskAppSchema.getStartDate() == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该投保单的险种对应的开办日期在险种描述中不存在！";
                this.mErrors.addOneError(tError);

                return false;
            }
            else
            {
                if (PubFun.calInterval(mLMRiskAppSchema.getStartDate(),
                        mLCPolBL.getCValiDate(), "D") < 0)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "该投保单录入的生效日期小于该险种的开办日期，请确认！";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }

            //检验生效日期是否大于停办日期
            if ((EdorType == null) || EdorType.trim().equals(""))
            {
	            if (mLMRiskAppSchema.getEndDate() != null)
	            {
	                if (!mLMRiskAppSchema.getEndDate().trim().equals(""))
	                {
	                    if (PubFun.calInterval(mLMRiskAppSchema.getEndDate(),
	                            mLCPolBL.getCValiDate(), "D") >
	                            0)
	                    {
	                        // @@错误处理
	                        CError tError = new CError();
	                        tError.moduleName = "ProposalBL";
	                        tError.functionName = "checkData";
	                        tError.errorMessage = "该投保单录入的生效日期超过该险种的停办日期，请确认！";
	                        this.mErrors.addOneError(tError);
	
	                        return false;
	                    }
	                }
	            }
            }

            //检查投保人录入的数据是否正确
            //                if (checkLCAppnt() == false)
            //                {
            //                    return false;
            //                }
        }

        //检查被保人录入的数据是否正确
        if (!checkLCInsured())
        {
            return false;
        }
        // end of if

        if (mOperate.equals("UPDATE||PROPOSAL"))
        {
            String tProposalNo = mLCPolBL.getProposalNo();
//            LCPolSchema tLCPolSchema = new LCPolSchema();
            LCPolDB tLCPolDB = null;
            if(this.isConCanUse)
            	tLCPolDB = new LCPolDB(this.con);
            else
            	tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tProposalNo);
            if (!tLCPolDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "LCPol表查询失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
            OriginOperator = tLCPolDB.getOperator();
//            OriginPrem = tLCPolDB.getPrem();
//            OriginStandPrem = tLCPolDB.getStandPrem(); //保存更新前的标准保费

            mLCPolBL.setMakeDate(tLCPolDB.getMakeDate());
            mLCPolBL.setMakeTime(tLCPolDB.getMakeTime());
            mLCPolBL.setModifyDate(theCurrentDate);
            mLCPolBL.setModifyTime(theCurrentTime);

            // 校验
            if (StrTool.cTrim(tLCPolDB.getAppFlag()).equals("1"))
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "此投保单已经签发保单，不能进行此项操作!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //如果是主险复核或核保成功，不能做修改
            if (tLCPolDB.getPolNo().equals(tLCPolDB.getMainPolNo()))
            {
//                if (StrTool.cTrim(tLCPolDB.getApproveFlag()).equals("9"))
//                {
//                    CError tError = new CError();
//                    tError.moduleName = "ProposalBL";
//                    tError.functionName = "checkData";
//                    tError.errorMessage = "此投保单已经复核成功，不能进行此项操作!";
//                    this.mErrors.addOneError(tError);
//
//                    return false;
//                }
                if (StrTool.cTrim(tLCPolDB.getUWFlag()).equals("9"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "此投保单已经核保成功，不能进行此项操作!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
            else
            {
                //如果是附加险，不管复核或核保状态。容许修改,将其状态置为初始状态
                mLCPolBL.setApproveFlag("0");
                mLCPolBL.setUWFlag("0");
            }

            if (mChangePlanFlag.equals("1"))
            {
                //如果该投保单还有未回复的问题件,则不容许进行修改
                StringBuffer tSBql = new StringBuffer(128);
                tSBql.append("select count(*) from  lcissuepol where ProposalContNo='");
                tSBql.append(tLCPolDB.getProposalNo());
                tSBql.append("' and BackObjType in('1') and ReplyMan is null");
//                String tStr =
//                        "select count(*) from  lcissuepol where ProposalContNo='"
//                        + tLCPolDB.getProposalNo()
//                        + "' and BackObjType in('1') and ReplyMan is null";

                //LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
//                ExeSQL tExeSQL = new ExeSQL();
                int tCount = Integer.parseInt(mExeSQL.getOneValue(tSBql.toString()));

//                System.out.println("======tCount=======" + tCount);
                if (tCount == 0)
                {
//                    System.out.println("======tCount=======" + tCount);
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "此投保单有操作员的问题件未回复，不能进行此项修改操作,请先回复!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }
        // end of if

        if (mOperate.equals("DELETE||PROPOSAL")) // sun要求修改校验规则，只限制是否签单！
        {
            String tProposalNo = mLCPolBL.getProposalNo();
//            LCPolSchema tLCPolSchema = new LCPolSchema();
            LCPolDB tLCPolDB = null;
            if(this.isConCanUse)
            	tLCPolDB = new LCPolDB(this.con);
            else
            	tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tProposalNo);
            if (!tLCPolDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "LCPol表查询失败!";
                this.mErrors.addOneError(tError);

                return false;
            }

            // 校验
            if (StrTool.cTrim(tLCPolDB.getAppFlag()).equals("1"))
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "checkData";
                tError.errorMessage = "此投保单已经签发保单保单，不能进行此项操作!";
                this.mErrors.addOneError(tError);

                return false;
            }
            if (tLCPolDB.getPolNo().equals(tLCPolDB.getMainPolNo()))
            {
                LCPolDB tLCPolDB1 = null;
                if(this.isConCanUse)
                	tLCPolDB1 = new LCPolDB(this.con);
                else
                	tLCPolDB1 = new LCPolDB();
                tLCPolDB1.setMainPolNo(tLCPolDB.getMainPolNo());
                int tPolCount = tLCPolDB1.getCount();
                if (tLCPolDB1.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "查询投保单失败";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tPolCount > 1)
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "该主险下还有附加险，请先删除附加险！";
                    this.mErrors.addOneError(tError);
                    return false;
                }

            }
        }

        
        if(!"".equals(mLCInsuredBL.getRelationToMainInsured())&&!"00".equals(mLCInsuredBL.getRelationToMainInsured()))
        {
             if("NAK02".equals(mLCPolBL.getRiskCode()))
             {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "ProposalBL";
                  tError.functionName = "checkData";
                  tError.errorMessage ="中意借贷宝团体定期寿险，不能增加连带被保险人！";
                  this.mErrors.addOneError(tError);
                  return false;
             }
             
//             String insureStr = "select count(1) from lcinsured "
//            	              +" where grpcontno = '"+mLCInsuredBL.getGrpContNo()+"'"
//            	              +" and insuredno = '"+mLCInsuredBL.getMainInsuredNo()+"'";
//             
//             SSRS insuSSRS = new SSRS();
//             
//             insuSSRS = mExeSQL.execSQL(insureStr);
//             
//             if(insuSSRS!=null&&insuSSRS.getMaxRow()>0&&Integer.parseInt(insuSSRS.GetText(1, 1))>0)
//             {
//                 // @@错误处理
//                 CError tError = new CError();
//                 tError.moduleName = "ProposalBL";
//                 tError.functionName = "checkData";
//                 tError.errorMessage ="主被保险人在同一张保单投保多次，请查证！";
//                 this.mErrors.addOneError(tError);
//                 return false;
//            }
             
         }

        
        return true;
    }

    /**
     * 检查被保人录入的数据是否正确(适用于个人投保单和集体下的个单)
     * @return boolean
     */
    private boolean checkLCInsured()
    {
        //如果是无名单或者公共帐户的个人，不校验返回
        if (mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4")
                || mLCPolBL.getPolTypeFlag().equals("2")||mLCPolBL.getPolTypeFlag().equals("5"))
        {
            return true;
        }

        // 被保人
        if (mLCInsuredBLSet != null)
        {
            /*Lis5.3 upgrade set
              for (int i = 1; i <= mLCInsuredBLSet.size(); i++)
              {
                  LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
                  tLCInsuredSchema = mLCInsuredBLSet.get(i);
                  if (tLCInsuredSchema != null)
                  {
                      if (tLCInsuredSchema.getName() != null)
                      { //去空格
                          tLCInsuredSchema.setName(tLCInsuredSchema.getName()
                                                                   .trim());
                      }
                      if (tLCInsuredSchema.getSex() != null)
                      {
             tLCInsuredSchema.setSex(tLCInsuredSchema.getSex().trim());
                      }
                      if (tLCInsuredSchema.getIDNo() != null)
                      {
                          tLCInsuredSchema.setIDNo(tLCInsuredSchema.getIDNo()
                                                                   .trim());
                      }
                      if (tLCInsuredSchema.getIDType() != null)
                      {
             tLCInsuredSchema.setIDType(tLCInsuredSchema.getIDType()
                                                                     .trim());
                      }
                      if (tLCInsuredSchema.getBirthday() != null)
                      {
             tLCInsuredSchema.setBirthday(tLCInsuredSchema.getBirthday()
             .trim());
                      }

                      //如果没有录入被保人标记是主被保人还是从被保人，则默认是主被保人
                      if ((tLCInsuredSchema.getInsuredGrade() == null)
             || tLCInsuredSchema.getInsuredGrade().trim().equals(""))
                      {
                          tLCInsuredSchema.setInsuredGrade("M");
                      }

                      //有客户号
                      if ((tLCInsuredSchema.getCustomerNo() != null)
             && !tLCInsuredSchema.getCustomerNo().trim().equals(""))
                      {
                          LDPersonDB tLDPersonDB = new LDPersonDB();
                          tLDPersonDB.setCustomerNo(tLCInsuredSchema
                                                    .getCustomerNo());
                          if (tLDPersonDB.getInfo() == false)
                          {
                              CError tError = new CError();
                              tError.moduleName = "ProposalBL";
                              tError.functionName = "checkPerson";
                              tError.errorMessage = "数据库查询失败!";
                              this.mErrors.addOneError(tError);

                              return false;
                          }
                          if (tLCInsuredSchema.getName() != null)
                          {
             String Name = StrTool.GBKToUnicode(tLDPersonDB.getName()
             .trim());
             String NewName = StrTool.GBKToUnicode(tLCInsuredSchema.getName()
             .trim());

                              if (!Name.equals(NewName))
                              {
                                  CError tError = new CError();
                                  tError.moduleName = "ProposalBL";
                                  tError.functionName = "checkPerson";
             tError.errorMessage = "您输入的被保人客户号对应在数据库中的客户姓名("
                                                        + Name + ")与您录入的客户姓名("
                                                        + NewName + ")不匹配！";
                                  this.mErrors.addOneError(tError);

                                  return false;
                              }
                          }
                          if (tLCInsuredSchema.getSex() != null)
                          {
                              if (!tLDPersonDB.getSex().equals(tLCInsuredSchema
                                                                   .getSex()))
                              {
                                  CError tError = new CError();
                                  tError.moduleName = "ProposalBL";
                                  tError.functionName = "checkPerson";
             tError.errorMessage = "您输入的被保人客户号对应在数据库中的客户性别("
                                                        + tLDPersonDB.getSex()
                                                        + ")与您录入的客户性别("
             + tLCInsuredSchema.getSex()
                                                        + ")不匹配！";
                                  this.mErrors.addOneError(tError);

                                  return false;
                              }
                          }
                      }
                      else //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
                      {
                          if ((tLCInsuredSchema.getName() != null)
                                  && (tLCInsuredSchema.getIDNo() != null)
                                  && (tLCInsuredSchema.getSex() != null))
                          {
                              if (tLCInsuredSchema.getName().trim().equals("")
             && tLCInsuredSchema.getIDNo().trim().equals(""))
                              {
                                  CError tError = new CError();
                                  tError.moduleName = "ProposalBL";
                                  tError.functionName = "checkPerson";
                                  tError.errorMessage = "请至少输入的被保人的姓名或ID号！";
                                  this.mErrors.addOneError(tError);

                                  return false;
                              }

                              LDPersonDB tLDPersonDB = new LDPersonDB();
                              tLDPersonDB.setName(tLCInsuredSchema.getName());
                              tLDPersonDB.setSex(tLCInsuredSchema.getSex());
                              if (tLCInsuredSchema.getBirthday() != null)
                              {
                                  tLDPersonDB.setBirthday(tLCInsuredSchema
                                                          .getBirthday());
                              }

                              //tLDPersonDB.setIDType("0");
                              tLDPersonDB.setIDNo(tLCInsuredSchema.getIDNo());

                              LDPersonSet tLDPersonSet = tLDPersonDB.query();
                              if (tLDPersonSet != null)
                              {
                                  if (tLDPersonSet.size() > 0)
                                  {
             tLCInsuredSchema.setCustomerNo(tLDPersonSet.get(1)
             .getCustomerNo());
                                      mLCInsuredBLSet.set(i, tLCInsuredSchema);
                                  }
                              }
                          }
                      }
                  }
              }
             */
        }

        return true;
    }

    /**
     * 校验个人投保单的投保人
     * @return
     */
//    private boolean checkLCAppnt()
//    {
//        //如果是无名单或者公共帐户的个人，不校验返回
//        if (mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4")
//                || mLCPolBL.getPolTypeFlag().equals("2"))
//        {
//            return true;
//        }
//
//        // 投保人-个人客户--如果是集体下个人，该投保人为空,所以个人时校验个人投保人
//        if (mPolType.equals("1"))
//        {
//            if (mLCAppntBL != null)
//            {
//                if (mLCAppntBL.getAppntName() != null)
//                { //去空格
//                    mLCAppntBL.setAppntName(mLCAppntBL.getAppntName().trim());
//                }
//                if (mLCAppntBL.getAppntSex() != null)
//                {
//                    mLCAppntBL.setSex(mLCAppntBL.getAppntSex().trim());
//                }
//                if (mLCAppntBL.getAppntIDNo() != null)
//                {
//                    mLCAppntBL.setIDNo(mLCAppntBL.getidgetAppntIDNo().trim());
//                }
//                if (mLCAppntBL.getIDType() != null)
//                {
//                    mLCAppntBL.setIDType(mLCAppntBL.getIDType().trim());
//                }
//                if (mLCAppntBL.getBirthday() != null)
//                {
//                    mLCAppntBL.setBirthday(mLCAppntBL.getBirthday().trim());
//                }
//
//                if (mLCAppntBL.getCustomerNo() != null)
//                {
//                    //如果有客户号
//                    if (!mLCAppntBL.getCustomerNo().equals(""))
//                    {
//                        LDPersonDB tLDPersonDB = new LDPersonDB();
//
//                        tLDPersonDB.setCustomerNo(mLCAppntBL.getCustomerNo());
//                        if (tLDPersonDB.getInfo() == false)
//                        {
//                            CError tError = new CError();
//                            tError.moduleName = "ProposalBL";
//                            tError.functionName = "checkPerson";
//                            tError.errorMessage = "数据库查询失败!";
//                            this.mErrors.addOneError(tError);
//
//                            return false;
//                        }
//                        if (mLCAppntBL.getName() != null)
//                        {
//                            String Name = StrTool.GBKToUnicode(tLDPersonDB.getName()
//                                                                          .trim());
//                            String NewName = StrTool.GBKToUnicode(mLCAppntBL.getName()
//                                                                               .trim());
//
//                            if (!Name.equals(NewName))
//                            {
//                                CError tError = new CError();
//                                tError.moduleName = "ProposalBL";
//                                tError.functionName = "checkPerson";
//                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户姓名("
//                                                      + Name + ")与您录入的客户姓名("
//                                                      + NewName + ")不匹配！";
//                                this.mErrors.addOneError(tError);
//
//                                return false;
//                            }
//                        }
//                        if (mLCAppntBL.getSex() != null)
//                        {
//                            if (!tLDPersonDB.getSex().equals(mLCAppntBL
//                                                                 .getSex()))
//                            {
//                                CError tError = new CError();
//                                tError.moduleName = "ProposalBL";
//                                tError.functionName = "checkPerson";
//                                tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户性别("
//                                                      + tLDPersonDB.getSex()
//                                                      + ")与您录入的客户性别("
//                                                      + mLCAppntBL.getSex()
//                                                      + ")不匹配！";
//                                this.mErrors.addOneError(tError);
//
//                                return false;
//                            }
//                        }
//                    }
//                    else //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
//                    {
//                        if ((mLCAppntBL.getName() != null)
//                                && (mLCAppntBL.getSex() != null)
//                                && (mLCAppntBL.getIDNo() != null))
//                        {
//                            LDPersonDB tLDPersonDB = new LDPersonDB();
//                            tLDPersonDB.setName(mLCAppntBL.getName());
//                            tLDPersonDB.setSex(mLCAppntBL.getSex());
//                            tLDPersonDB.setBirthday(mLCAppntBL.getBirthday());
//
//                            //tLDPersonDB.setIDType("0");
//                            tLDPersonDB.setIDNo(mLCAppntBL.getIDNo());
//
//                            LDPersonSet tLDPersonSet = tLDPersonDB.query();
//                            if (tLDPersonSet != null)
//                            {
//                                if (tLDPersonSet.size() > 0)
//                                {
//                                    mLCAppntBL.setCustomerNo(tLDPersonSet.get(1)
//                                                                            .getCustomerNo());
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return true;
//    }

//    /**
//     * 校验集体投保人信息（暂不用）
//     * @return
//     */
//    private boolean checkLCAppntGrp()
//    {
//        //集体客户;
//        if (mLCGrpAppntBL != null)
//        {
//            if (mLCGrpAppntBL.getCustomerNo() != null) //如果集体客户号不为空
//            {
//                if (!mLCGrpAppntBL.getCustomerNo().equals(""))
//                {
//                    LDGrpDB tLDGrpDB = new LDGrpDB();
//                    //tLDGrpDB.setGrpNo(mLCGrpAppntBL.getGrpNo());
//                    if (!tLDGrpDB.getInfo())
//                    {
//                        CError tError = new CError();
//                        tError.moduleName = "ProposalBL";
//                        tError.functionName = "checkPerson";
//                        tError.errorMessage = "数据库查询失败!";
//                        this.mErrors.addOneError(tError);
//
//                        return false;
//                    }
//                    if (mLCGrpAppntBL.getName() != null)
//                    {
//                        String Name = StrTool.GBKToUnicode(tLDGrpDB.getGrpName()
//                                .trim());
//                        String NewName = StrTool.GBKToUnicode(mLCGrpAppntBL.
//                                getName()
//                                .trim());
//                        if (!Name.equals(NewName))
//                        {
//                            CError tError = new CError();
//                            tError.moduleName = "ProposalBL";
//                            tError.functionName = "checkPerson";
//                            tError.errorMessage = "您输入的集体客户号对应在数据库中的客户姓名("
//                                    + Name + ")与您录入的集体客户姓名("
//                                    + NewName + ")不匹配！";
//                            this.mErrors.addOneError(tError);
//
//                            return false;
//                        }
//                    }
//                }
//            }
//            else //如果集体客户号为空。待补充
//            {
//            }
//        }
//
//        return true;
//    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
        //处理个人新增业务
        if (this.mOperate.equals("INSERT||PROPOSAL")
                || this.mOperate.equals("UPDATE||PROPOSAL"))
        {
            if (!dealDataPerson())
            {
                return false;
            }

            //银行实时出单的特殊处理--可以去掉
            if (!dealDataForBank())
            {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||PROPOSAL"))
        {
            if (!dealDataDel())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行个人保单删除的逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealDataDel()
    {
        LCPolDB tLCPolDB = null;
        if(this.isConCanUse)
        	tLCPolDB = new LCPolDB(this.con);
        else
        	tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLCPolBL.getProposalNo());
        if (!tLCPolDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);

            return false;
        }
        LCContDB tLCContDB = null;
        if(this.isConCanUse)
        	tLCContDB = new LCContDB(this.con);
        else
        	tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLCPolDB.getContNo());
        if (!tLCContDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);

            return false;
        }

        //由于精度问题注释该段代码 modify by sunxy 2005-01-16
//        tLCContDB.setPrem(tLCContDB.getPrem()
//                          - tLCPolDB.getPrem());
//        tLCContDB.setAmnt(tLCContDB.getAmnt()
//                          - tLCPolDB.getAmnt());


        //由于精度问题添加该段代码 modify by sunxy 2005-01-16
        if(tLCContDB.getPolType().equals("5"))
        {
        	LCPolDB tLCPolDBli=null;
        	if(this.isConCanUse)
        		tLCPolDBli = new LCPolDB(this.con);
            else
            	tLCPolDBli = new LCPolDB();
        	LCPolSet tLCPolSetli = new LCPolSet();
        	tLCPolDBli.setContNo(tLCContDB.getContNo());
        	tLCPolSetli=tLCPolDBli.query();
        	if(tLCPolSetli.size()==1)
        	{
        		tLCContDB.setAmnt(0);
        	}
        }
        else
        {

        String strCalContAmnt = String.valueOf(Arith.round(tLCContDB.getAmnt() -
                tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额

        double calPolAmnt = Double.parseDouble(strCalContAmnt);

        tLCContDB.setAmnt(calPolAmnt);
        }

        String strCalContPrem = String.valueOf(Arith.round(tLCContDB.getPrem() -
                tLCPolDB.getPrem(),mSCALE)); //转换计算后的保额
        double calPolPrem = Double.parseDouble(strCalContPrem);        
        tLCContDB.setPrem(calPolPrem);
        
        
        mLCContSchema.setSchema(tLCContDB.getSchema());
        if (tLCPolDB.getContType().equals("2"))
        {
            mPolType = "2";
        }

        if (mPolType.equals("2")) // 集体下的个人
        {
        	LCGrpPolDB tLCGrpPolDB = null;
            if(this.isConCanUse)
            	tLCGrpPolDB = new LCGrpPolDB(this.con);
            else
            	tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(tLCPolDB.getGrpContNo());
            tLCGrpPolDB.setRiskCode(tLCPolDB.getRiskCode());

            LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
            if (tLCGrpPolSet.size() == 0)
            {

                this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);

                return false;

            }
            else
            {
                mLCGrpPolSchema = tLCGrpPolSet.get(1);
            }
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyTime(theCurrentTime);

            LCGrpContDB tLCGrpContDB = null;
            if(this.isConCanUse)
            	tLCGrpContDB = new LCGrpContDB(this.con);
            else
            	tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(tLCPolDB.getGrpContNo());
            if (!tLCGrpContDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                return false;
            }
            tLCGrpContDB.setModifyDate(theCurrentDate);
            tLCGrpContDB.setModifyTime(theCurrentTime);

            int NONAMENUM = 1;

            if ((tLCPolDB.getPolTypeFlag() == null)
                    || tLCPolDB.getPolTypeFlag().equals(""))
            {
                tLCPolDB.setPolTypeFlag("0");
            }

            //如果不是集体下的无名单加人--类型是4
            if (!mSavePolType.equals("4"))
            {
                //保单类型为无名单（投保人数>0）的或者为公共帐户（投保人数为0）的
                if (tLCPolDB.getPolTypeFlag().equals("1") || tLCPolDB.getPolTypeFlag().equals("3") || tLCPolDB.getPolTypeFlag().equals("4")
                        || tLCPolDB.getPolTypeFlag().equals("2")||tLCPolDB.getPolTypeFlag().equals("5"))
                {
                    mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2()
                            - tLCPolDB.getInsuredPeoples());
                    //tLCGrpContDB.setPeoples2(tLCGrpContDB.getPeoples2()
                    //                            - tLCPolDB.getInsuredPeoples());
                    NONAMENUM = tLCPolDB.getInsuredPeoples();
                }
                else
                {
                    mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2()
                            - 1);
                    //tLCGrpContDB.setPeoples2(tLCGrpContDB.getPeoples2()
                    //                            - 1);
                }

                //由于精度问题添加该段代码 modify by sunxy 2005-01-16
                String strCalGrpPolPrem = String.valueOf(Arith.round(mLCGrpPolSchema.
                        getPrem() - tLCPolDB.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                String strCalGrpPolAmnt = String.valueOf(Arith.round(mLCGrpPolSchema.
                        getAmnt() - tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额

                String strCalGrpContPrem = String.valueOf(Arith.round(tLCGrpContDB.
                        getPrem() - tLCPolDB.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                String strCalGrpContAmnt = String.valueOf(Arith.round(tLCGrpContDB.
                        getAmnt() - tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额

                double calGrpPolPrem = Double.parseDouble(strCalGrpPolPrem);
                double calGrpPolAmnt = Double.parseDouble(strCalGrpPolAmnt);

                double calGrpContPrem = Double.parseDouble(strCalGrpContPrem);
                double calGrpContAmnt = Double.parseDouble(strCalGrpContAmnt);

                mLCGrpPolSchema.setPrem(calGrpPolPrem);
                mLCGrpPolSchema.setAmnt(calGrpPolAmnt);
                tLCGrpContDB.setPrem(calGrpContPrem);
                tLCGrpContDB.setAmnt(calGrpContAmnt);

            }
            mLCGrpContSchema = tLCGrpContDB.getSchema();
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行个人保单录入的逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealDataPerson()
    {
    	long t = System.currentTimeMillis();
        int i;
        int iMax;
        String tStr = "";
        String tNo = "";
        String tLimit = "";
        Reflections tReflections = new Reflections();
//        System.out.println("第一步");
        //产生保单号码
        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
            //判定是团体保单下的个人单的险种信息，还是普通个单下的险种信息
            if (mLCContSchema.getContType().equals("2"))
            {
                tNo = PubFun1.CreateMaxNo("GProposalNo", tLimit);
            }
            else
            {
                tNo = PubFun1.CreateMaxNo("ProposalNo", tLimit);
            }
            mLCPolBL.setProposalNo(tNo);
        }
        else
        {
            //投保申请日期处理:1 没有该投保单，属于新增，取录入值；
            //2 数据库中有该投保单，若数据库的指定生效日期值为N，取录入值；若数据库的指定生效日期值为Y，保持申请日期。
            if (mOperate.equals("UPDATE||PROPOSAL"))
            {
            	LCPolDB xLCPolDB = null;
                 if(this.isConCanUse)
                	 xLCPolDB = new LCPolDB(this.con);
                 else
                	 xLCPolDB = new LCPolDB();
                xLCPolDB.setPolNo(mLCPolBL.getPolNo());
                if (xLCPolDB.getInfo())
                {
                    if ((xLCPolDB.getSpecifyValiDate() != null)
                            && xLCPolDB.getSpecifyValiDate().equals("Y"))
                    {
                        mLCPolBL.setPolApplyDate(xLCPolDB.getPolApplyDate());
                    }
                }
            }
        }
        mLCPolBL.setPolNo(mLCPolBL.getProposalNo());

        //如果是否指定生效日期在界面上没有传入，那么默认是否
        if ((mLCPolBL.getSpecifyValiDate() == null)
                || mLCPolBL.getSpecifyValiDate().equals(""))
        {
            mLCPolBL.setSpecifyValiDate("N");
        }

        //删除应收总表数据
//        if (deleteAccNo)
//        {
//            LJSPayDB tLJSPayDB = new LJSPayDB();
//            tLJSPayDB.setOtherNo(mLCPolBL.getPolNo());
//            mLJSPaySet = tLJSPayDB.query();
//
//            for (int k = 0; k < mLJSPaySet.size(); k++)
//            {
//                LJSPaySchema tLJSPaySchema = mLJSPaySet.get(k + 1);
//                if (tLJSPaySchema.getBankOnTheWayFlag().equals("1"))
//                {
//                    // @@错误处理
//                    CError tError = new CError();
//                    tError.moduleName = "ProposalBL";
//                    tError.functionName = "dealDataPerson";
//                    tError.errorMessage = "应收表有数据在发往银行，不能删除应收总表数据!";
//                    this.mErrors.addOneError(tError);
//
//                    return false;
//                }
//            }
//        }

        if ((mLCPolBL.getMainPolNo() == null)
                || mLCPolBL.getMainPolNo().trim().equals(""))
        {
            mLCPolBL.setMainPolNo(mLCPolBL.getProposalNo());
            mainLCPolBL.setSchema(mLCPolBL);
        }
        else
        {
            if (!mGrpImportFlag)
            {
                mainLCPolBL.setPolNo(mLCPolBL.getMainPolNo());
                mainLCPolBL.getInfo();
                if (mainLCPolBL.mErrors.needDealError())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "查询该附险的主险保单时失败！";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }

        //mLCPolBL.setMasterPolNo( SysConst.ZERONO );
        //为了中英打印需求，在保单不是按分数或档次卖的时候，mult存0
//        if (mLCPolBL.getMult() == 0)
//        {
//            mLCPolBL.setMult(1);
//        }

//        System.out.println("第2步");

        //查询险种描述表
        //查询险种承保描述表
        // 投保人
        String tAppntCustomerNo = "";
        String tAppntName = "";
        //个人投保人
        if (mAppntType.equals("1"))
        {
            if (StrTool.cTrim(mLCAppntBL.getAppntNo()).equals(""))
            {
                tLimit = "SN";

                String tAppntNo = PubFun1.CreateMaxNo("CustomerNo", tLimit);
                mLCAppntBL.setAppntNo(tAppntNo);

                // 客户表同步数据
                LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                tReflections.transFields(tLDPersonSchema, mLCAppntBL.getSchema());
                tLDPersonSchema.setOperator(mGlobalInput.Operator);
                tLDPersonSchema.setMakeDate(theCurrentDate);
                tLDPersonSchema.setMakeTime(theCurrentTime);
                tLDPersonSchema.setModifyDate(theCurrentDate);
                tLDPersonSchema.setModifyTime(theCurrentTime);
                mLDPersonSet.add(tLDPersonSchema);
            }
            else
            {
            	LCAppntDB mLCAppntDB = null;
                if(this.isConCanUse)
                	mLCAppntDB = new LCAppntDB(this.con);
                else
                	mLCAppntDB = new LCAppntDB();
                mLCAppntDB.setSchema(mLCAppntBL);
                mLCAppntDB.getInfo();
                mLCAppntBL.setSchema(mLCAppntDB);
            }

            // 校验投保人投保年龄
            int tAppntAge = 0;
            tAppntAge = PubFun.calInterval(mLCAppntBL.getAppntBirthday(), mLCPolBL.getCValiDate()
                    , "Y");
//            System.out.println("投保年龄:" + tAppntAge);
//            System.out.println("投保年龄描述:" + mLMRiskAppSchema.getMinAppntAge());
            if (tAppntAge < mLMRiskAppSchema.getMinAppntAge())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson";
                tError.errorMessage = "投保人投保年龄"+tAppntAge+"岁不符合险种最低"+mLMRiskAppSchema.getMinAppntAge()+"岁投保要求!";
                this.mErrors.addOneError(tError);

                return false;
            }
            tAppntCustomerNo = mLCAppntBL.getAppntNo();
            tAppntName = mLCAppntBL.getAppntName();
        }
        //集体投保人
        else if (mAppntType.equals("2"))
        {
//            mLCGrpAppntBL.setPolNo(mLCPolBL.getPolNo());
//            mLCAppntGrpBL.setAppntGrade("M");
//            mLCAppntGrpBL.setOperator(mGlobalInput.Operator);
//            mLCAppntGrpBL.setMakeDate(theCurrentDate);
//            mLCAppntGrpBL.setMakeTime(theCurrentTime);
//            mLCAppntGrpBL.setModifyDate(theCurrentDate);
//            mLCAppntGrpBL.setModifyTime(theCurrentTime);

            //如果从外部接收的数据为空，那么从数据库查询
//            if (mLDGrpSchema == null)
//            {
//                LDGrpDB tLDGrpDB = new LDGrpDB();
//                //PQ 需要恢复
//                tLDGrpDB.setCustomerNo(mLCGrpAppntBL.getCustomerNo());
//                if (tLDGrpDB.getInfo() == false)
//                {
//                    // @@错误处理
//                    this.mErrors.copyAllErrors(tLDGrpDB.mErrors);
//
//                    CError tError = new CError();
//                    tError.moduleName = "ProposalBL";
//                    tError.functionName = "dealDataPerson";
//                    tError.errorMessage = "LDGrp表查询失败!";
//                    this.mErrors.addOneError(tError);
//
//                    return false;
//                }
//                mLDGrpSchema = tLDGrpDB.getSchema();
//            }
            tAppntCustomerNo = mLCGrpAppntBL.getCustomerNo();
            tAppntName = mLCGrpAppntBL.getName();
        }
        mLCPolBL.setAppntNo(tAppntCustomerNo);
        mLCPolBL.setAppntName(tAppntName);
        //被保人-如果有多条，即有连带被保人，则将主被保人放在第一位
//        LCInsuredBL tLCInsuredBL1 = new LCInsuredBL();
//        if (mLCInsuredBLSet.size() > 1)
//        {
//            LCInsuredBL tempLCInsuredBL = new LCInsuredBL();
//            for (int n = 1; n <= mLCInsuredBLSet.size(); n++)
//            {
//                /*Lis5.3 upgrade get
//              String InsuredGrade = mLCInsuredBLSet.get(n).getInsuredGrade();
//              */
//             //PQ 被保险人处理，连带和主被保险人要分开
//             String InsuredGrade ="";
//                if ((InsuredGrade != null) && InsuredGrade.equals("M"))
//                {
//                    if (n == 1)
//                    {
//                        break;
//                    }
//
//                    //得到排序第n位的数据和第1位互换
//                    tempLCInsuredBL.setSchema(mLCInsuredBLSet.get(n).getSchema());
//                    mLCInsuredBLSet.set(n, mLCInsuredBLSet.get(1));
//                    mLCInsuredBLSet.set(1, tempLCInsuredBL);
//
//                    break;
//                }
//            }
//        }
//        tLCInsuredBL1.setSchema((LCInsuredSchema) mLCInsuredBLSet.get(1));

        //判断投保人是否和主被保人相同，集体投保人不判断
        // 0 表示投保人和主被保人不相同  1 表示投保人和主被保人相同

        //被保险人处理
        if (StrTool.cTrim(mLCInsuredBL.getInsuredNo()).equals(""))
        {
            tLimit = "SN";

            String tInsuredNo = PubFun1.CreateMaxNo("CustomerNo", tLimit);
            mLCInsuredBL.setInsuredNo(tInsuredNo);

            //如果投保单类型是无名单或者公共帐户，不添加客户信息，补充投保人信息
            if (mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4")
                    || mLCPolBL.getPolTypeFlag().equals("2")||mLCPolBL.getPolTypeFlag().equals("5"))
            {
                //如果是附加险，查出主险的被保人号码赋给附加险
                if (mLMRiskAppSchema.getSubRiskFlag().equals("S"))
                {
                	LCPolDB tempLCpolDB = null;
                    if(this.isConCanUse)
                    	tempLCpolDB = new LCPolDB(this.con);
                    else
                    	tempLCpolDB = new LCPolDB();
                    tempLCpolDB.setPolNo(mLCPolBL.getMainPolNo());
                    if (!tempLCpolDB.getInfo())
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkLCInsured";
                        tError.errorMessage = "未查询到该投保单的主险！";
                        this.mErrors.addOneError(tError);

                        return false;
                    }

                    //如果主险也是无名单（因该是），那么取主险的被保人信息
                    if (tempLCpolDB.getPolTypeFlag().equals("1") || tempLCpolDB.getPolTypeFlag().equals("3") || tempLCpolDB.getPolTypeFlag().equals("4")
                            || tempLCpolDB.getPolTypeFlag().equals("2")||tempLCpolDB.getPolTypeFlag().equals("5"))
                    {
                        mLCInsuredBL.setInsuredNo(tempLCpolDB.getInsuredNo());
                        mLCInsuredBL.setName(tempLCpolDB.getInsuredName());
                        mLCInsuredBL.setBirthday(tempLCpolDB.getInsuredBirthday());
                        mLCInsuredBL.setSex(tempLCpolDB.getInsuredSex());
                    }
                }
                if (mLCPolBL.getPolTypeFlag().equals("2"))
                {
                    mLCInsuredBL.setName("公共账户");
                }else if(mLCPolBL.getPolTypeFlag().equals("5")) {
                	 mLCInsuredBL.setName("公共保额");
                }
                else
                {
                    mLCInsuredBL.setName("无名单");
                }
                if (mLCInsuredBL.getSex() == null)
                {
                    mLCInsuredBL.setSex("0");
                }
                //如果是无名单，没有录入年龄，默认是30
                if (mLCInsuredBL.getBirthday() == null)
                {
                    int tInsuredAge = 0;
                    //如果投保年龄没有录入
                    if (mLCPolBL.getInsuredAppAge() <= 0)
                    {
                        tInsuredAge = 30;
                        mLCPolBL.setInsuredAppAge(tInsuredAge);
                    }
                    else
                    {
                        tInsuredAge = mLCPolBL.getInsuredAppAge(); //前台录入年龄
                    }

                    //年龄所在年=系统年-年龄
                    String year = Integer.toString(Integer.parseInt(StrTool.getYear())
                            - tInsuredAge);
                    String brithday = StrTool.getDate(year, StrTool.getMonth(), StrTool.getDay());
                    brithday = StrTool.replace(brithday, "/", "-");
                    mLCInsuredBL.setBirthday(brithday);
                }
            }
            else
            {
                // 客户表同步数据
                LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                tReflections.transFields(tLDPersonSchema, mLCInsuredBL.getSchema());
                tLDPersonSchema.setOperator(mGlobalInput.Operator);
                tLDPersonSchema.setMakeDate(theCurrentDate);
                tLDPersonSchema.setMakeTime(theCurrentTime);
                tLDPersonSchema.setModifyDate(theCurrentDate);
                tLDPersonSchema.setModifyTime(theCurrentTime);
                mLDPersonSet.add(tLDPersonSchema);
            }
            mLCInsuredBLSet.set(1, mLCInsuredBL);
        }
        else
        {
        	if(!EdorType.equals("LC")&&!EdorType.equals("CS")){
        		LCInsuredDB mLCInsuredDB = null;
                if(this.isConCanUse)
                	mLCInsuredDB = new LCInsuredDB(this.con);
                else
                	mLCInsuredDB = new LCInsuredDB();
            mLCInsuredDB.setSchema(mLCInsuredBL);
            mLCInsuredDB.getInfo();
            mLCInsuredBL.setSchema(mLCInsuredDB);
        	}
        }

        //mLCInsuredBL.setPolNo(mLCPolBL.getPolNo());
//        System.out.println("第4步");
        mLCInsuredBL.setDefaultFields();
        //mLCInsuredBLSetNew.add(mLCInsuredBL);

//        System.out.println("第5步");
        //PQ 连带或连生被保险人处理
        //校验连带被保人投保年龄
        if (mLCInsuredRelatedSet.size() >= 1)
        {
            if (!checkSLCInsured(mLCInsuredRelatedSet))
            {
                return false;
            }

            for (i = 1; i <= mLCInsuredRelatedSet.size(); i++)
            {
                LCInsuredRelatedSchema tLCInsuredRelatedSchema = new LCInsuredRelatedSchema();
                tLCInsuredRelatedSchema = mLCInsuredRelatedSet.get(i);

                tLCInsuredRelatedSchema.setPolNo(mLCPolBL.getPolNo());
//                tLCInsuredRelatedBL.setPrtNo(mLCPolBL.getPrtNo());
                tLCInsuredRelatedSchema.setMainCustomerNo(mLCInsuredBL.getInsuredNo());
                tLCInsuredRelatedSchema.setModifyDate(theCurrentDate);
                tLCInsuredRelatedSchema.setModifyTime(theCurrentTime);

                if (StrTool.cTrim(tLCInsuredRelatedSchema.getCustomerNo()).equals(""))
                {
                    tLimit = "SN";

                    String tInsuredNo = PubFun1.CreateMaxNo("CustomerNo", tLimit);
                    tLCInsuredRelatedSchema.setCustomerNo(tInsuredNo);

                    // 客户表同步数据
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tReflections.transFields(tLDPersonSchema, tLCInsuredRelatedSchema);
                    tLDPersonSchema.setOperator(mGlobalInput.Operator);
                    tLDPersonSchema.setMakeDate(theCurrentDate);
                    tLDPersonSchema.setMakeTime(theCurrentTime);
                    tLDPersonSchema.setModifyDate(theCurrentDate);
                    tLDPersonSchema.setModifyTime(theCurrentTime);
                    mLDPersonSet.add(tLDPersonSchema);
                }
                //LCInsuredRelatedSet.set(i,tLCInsuredRelatedBL.getSchema());
                //newLCInsuredRelatedSet.add(tLCInsuredRelatedBL);
            }
        }
//        System.out.println("第6步");
        // 校验被保人投保年龄
        int tInsuredAge = 0;
        if (mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4"))
        {
            tInsuredAge = mLCPolBL.getInsuredAppAge(); //前台录入年龄
        }
        else
        {
            //通过前台录入出生日期及计算
            tInsuredAge = PubFun.calInterval(mLCInsuredBL.getBirthday(), mLCPolBL.getCValiDate()
                    , "Y");
        }
//        System.out.println("第7步");
//        System.out.println("被保投保年龄:" + tInsuredAge);
//            System.out.println("被保投保年龄描述:" + mLMRiskAppSchema.getMinInsuredAge()+"|"+mLMRiskAppSchema.getMaxInsuredAge());
        if ((tInsuredAge < mLMRiskAppSchema.getMinInsuredAge())
                || ((tInsuredAge > mLMRiskAppSchema.getMaxInsuredAge())
                && (mLMRiskAppSchema.getMaxInsuredAge() > 0)))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "dealDataPerson";
//Amended By Fang for ASR20081922
            //tError.errorMessage = "被保人投保年龄"+tInsuredAge+"岁不符合("+mLMRiskAppSchema.getRiskName()+")该险种投保范围("+mLMRiskAppSchema.getMinInsuredAge()+"-"+mLMRiskAppSchema.getMaxInsuredAge()+")要求!";
            tError.errorMessage = "被保人("+mLCInsuredBL.getName()+")投保年龄"+tInsuredAge+"岁不符合("+mLMRiskAppSchema.getRiskName()+")该险种投保范围("+mLMRiskAppSchema.getMinInsuredAge()+"-"+mLMRiskAppSchema.getMaxInsuredAge()+")要求!";
//End ASR20081922        
            this.mErrors.addOneError(tError);

            return false;
        }
        
		int tPayEndYear=mLCPolBL.getPayEndYear();
		String tPayEndYearFlag=mLCPolBL.getPayEndYearFlag()==null?"":mLCPolBL.getPayEndYearFlag();
		int tGetYear=mLCPolBL.getGetYear();
		if(tPayEndYearFlag.equals("A")){
			if(tInsuredAge>tPayEndYear){
	            CError tError = new CError();
	            tError.moduleName = "ProposalBL";
	            tError.functionName = "dealDataPerson";
	            tError.errorMessage = "被保险人:"+mLCInsuredBL.getName()+",年龄:"+tInsuredAge
	            						+"岁超过了期交的交费期间"+tPayEndYear+"岁.";
				this.mErrors.addOneError(tError);
				return false;
			}
			if(mLCPolBL.getRiskCode().equals("NEK2")){
				if(mLCInsuredBL.getSex()!=null&&mLCInsuredBL.getSex().equals("1")){
					if(tPayEndYear!=54){
			            CError tError = new CError();
			            tError.moduleName = "ProposalBL";
			            tError.functionName = "dealDataPerson";
			            tError.errorMessage = "被保险人:"+mLCInsuredBL.getName()+",关联的交费期间:"+tPayEndYear
			            	+"岁与性别应该对应的交费期间:交至54岁(男)不符.";
						this.mErrors.addOneError(tError);
						return false;
					}
					if(tGetYear!=55){
			            CError tError = new CError();
			            tError.moduleName = "ProposalBL";
			            tError.functionName = "dealDataPerson";
			            tError.errorMessage = "被保险人:"+mLCInsuredBL.getName()+",关联的起领期间:"+tGetYear
			            	+"岁与性别应该对应的起领期间:从55岁开始领取(女)不符.";
						this.mErrors.addOneError(tError);
						return false;
					}
				}else	if(mLCInsuredBL.getSex()!=null&&mLCInsuredBL.getSex().equals("0")){
					if(tPayEndYear!=59){
			            CError tError = new CError();
			            tError.moduleName = "ProposalBL";
			            tError.functionName = "dealDataPerson";
			            tError.errorMessage = "被保险人:"+mLCInsuredBL.getName()+",关联的交费期间:"+tPayEndYear
			            	+"岁与性别应该对应的交费期间:交至59岁(女)不符.";
						this.mErrors.addOneError(tError);
						return false;
					}
					if(tGetYear!=60){
			            CError tError = new CError();
			            tError.moduleName = "ProposalBL";
			            tError.functionName = "dealDataPerson";
			            tError.errorMessage = "被保险人:"+mLCInsuredBL.getName()+",关联的起领期间:"+tGetYear
			            	+"岁与性别应该对应的起领期间:从60岁开始领取(男)不符.";
						this.mErrors.addOneError(tError);
						return false;
					}
				}

			}
		}
		
        mLCPolBL.setInsuredNo(mLCInsuredBL.getInsuredNo());
//        System.out.println("mLCInsuredBL.getInsuredNo()"+mLCInsuredBL.getInsuredNo());
        mLCPolBL.setInsuredName(mLCInsuredBL.getName());
        mLCPolBL.setInsuredSex(mLCInsuredBL.getSex());
        mLCPolBL.setInsuredBirthday(mLCInsuredBL.getBirthday());
        mLCPolBL.setInsuredAppAge(tInsuredAge);
        mLCPolBL.setOccupationType(mLCInsuredBL.getOccupationType());
        //处理__保费逾期未付选择__的处理方式.
        //如果操作人员从界面输入了相关的方法,而且系统lmriskapp里面定义的
        //autopayflag 方式为 1 (自动垫交)
        //我们可以按照客户的输入进行记录.
        if (mLMRiskAppSchema.getAutoPayFlag() != null
                && mLMRiskAppSchema.getAutoPayFlag().equalsIgnoreCase("0"))
        {
            mLCPolBL.setAutoPayFlag(mLMRiskAppSchema.getAutoPayFlag());
        }
        else if (mLCPolBL.getAutoPayFlag() == null)
        {
            mLCPolBL.setAutoPayFlag(mLMRiskAppSchema.getAutoPayFlag());
        }
        mLCPolBL.setStandPrem(mLCPolBL.getPrem()); //投保时总保费和每期保费相同

        //如果不是无名单或者不是公共帐户，那么存放主被保人
        if (!(mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4")
                || mLCPolBL.getPolTypeFlag().equals("2")||mLCPolBL.getPolTypeFlag().equals("5")))
        {
            mLCPolBL.setInsuredPeoples(1);
        }

        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            //houzm添加：如果是集体下的个人录入保存，判断
            //如果姓名，性别，出生日期相同的，并且在同一印刷号，同一主附险中的个人信息，不能保存
            //if( !mLCPolBL.getGrpPolNo().equals( SysConst.ZERONO )&&mLCPolBL.getContNo().equals( SysConst.ZERONO ))
//delete by lilei        	
//ASR20093813-中意借贷宝团体定期寿险      	
//            if (mPolType.equals("2") && !"LC".equals(mTransferData.getValueByName("EdorType"))&&!"CS".equals(mTransferData.getValueByName("EdorType")))
//            {
//                StringBuffer tSBql = new StringBuffer(256);
//                tSBql.append("select * from LCPol where InsuredName='");
//                tSBql.append(mLCPolBL.getInsuredName());
//                tSBql.append("' and InsuredSex='");
//                tSBql.append(mLCPolBL.getInsuredSex());
//                tSBql.append("' and InsuredBirthday='");
//                tSBql.append(mLCPolBL.getInsuredBirthday());
//                tSBql.append("' and RiskCode='");
//                tSBql.append(mLCPolBL.getRiskCode());
//                tSBql.append("' and PrtNo='");
//                tSBql.append(mLCPolBL.getPrtNo());
//                tSBql.append("' and InsuredNo='");
//                tSBql.append(mLCPolBL.getInsuredNo());
//                tSBql.append("'");
//                LCPolDB tempLCPolDB = null;
//                if(this.isConCanUse)
//                	tempLCPolDB = new LCPolDB(this.con);
//                else
//                	tempLCPolDB = new LCPolDB();
//                LCPolSet tempLCPolSet = tempLCPolDB.executeQuery(tSBql.toString());
//                if (tempLCPolDB.mErrors.needDealError())
//                {
//                    // @@错误处理
//                    this.mErrors.copyAllErrors(tempLCPolDB.mErrors);
//
//                    CError tError = new CError();
//                    tError.moduleName = "ProposalBL";
//                    tError.functionName = "checkData";
//                    tError.errorMessage = "LCPol表查询失败!";
//                    this.mErrors.addOneError(tError);
//
//                    return false;
//                }
//                if (tempLCPolSet.size() > 0)
//                {
//                    // @@错误处理
//                    CError tError = new CError();
//                    tError.moduleName = "ProposalBL";
//                    tError.functionName = "checkData";
//                    tError.errorMessage =
//                            "该个人的姓名，性别，出生日期及保单的印刷号和险种编码和已存在的保单数据重复！不能录入";
//                    this.mErrors.addOneError(tError);
//
//                    return false;
//                }
//            }
            //以上是houzm添加

            //保全新增附加险时，设置险种和责任的保险期间为生效日期到主险交至日期的未满期天数 Alex 2005-12-14
            if ("NS".equals(mTransferData.getValueByName("EdorType")))
            {
                if(mainLCPolBL.getPayIntv()!=0){
                    int insurDays = 0;
                    insurDays = PubFun.calInterval(mLCPolBL.
                            getCValiDate(), mainLCPolBL.getPaytoDate(), "D");
                    if (insurDays > 0)
                    {
                        mLCPolBL.setInsuYear(insurDays);
                        mLCPolBL.setInsuYearFlag("D");
                    }
                    for (int dutyIndex = 1; dutyIndex <= mLCDutyBLSet1.size();
                                         dutyIndex++)
                    {
                        if (insurDays > 0)
                        {
                            mLCDutyBLSet1.get(dutyIndex).setInsuYear(insurDays);
                            mLCDutyBLSet1.get(dutyIndex).setInsuYearFlag("D");
                        }
                    }
                }
            }
        }
        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            mLCPolBL.setOperator(mGlobalInput.Operator);
        }
        if (mOperate.equals("UPDATE||PROPOSAL"))
        {
            mLCPolBL.setOperator(OriginOperator);
        }

//        System.out.println("保费保额计算调用部分");
        System.out.println("ProposalBL:"+(System.currentTimeMillis()-t));
        if (!pubCal())
        {
            return false;
        }
        System.out.println("ProposalBL:"+(System.currentTimeMillis()-t));
        //System.out.println("保费保额计算结束");
        //保单表
        mLCPolBL.setRiskVersion(mLMRiskAppSchema.getRiskVer().trim());
        mLCPolBL.setKindCode(mLMRiskAppSchema.getKindCode().trim());

        if (mSavePolType.equals("0"))
        {
            mLCPolBL.setAppFlag("0"); //投保单/保单标志
        }
        else
        {
            mLCPolBL.setAppFlag(mSavePolType); //投保单/保单标志
        }
        mLCPolBL.setUWFlag("0"); //核保状态，0为未核保
        mLCPolBL.setApproveFlag("0"); //复核保状态，0为未复核

        if (mLMRiskSchema.getRnewFlag().equals("N"))
        {
            mLCPolBL.setRnewFlag( -2); // 续保标记，－2非续保
        }
        if (this.mLCCustomerImpartBLSet.size() > 0) //是否有告知信息
        {
            mLCPolBL.setImpartFlag("1");
        }
        else
        {
            mLCPolBL.setImpartFlag("0");
        }
        if (this.mLCBnfBLSet.size() > 0) //是否有受益人信息
        {
            mLCPolBL.setBnfFlag("1");
        }
        else
        {
            mLCPolBL.setBnfFlag("0");
        }
        // System.out.println("承保描述表完成！");
//        boolean bankFlag = true;

        // 银行授权书表
//        if (mLCPolBL.getPayLocation().trim().equals("0") // 银行自动转账
//                || mLCPolBL.getPayLocation().trim().equals("8") // 首期交费：银行转账
//                || (mLCPolBL.getPayLocation().trim().equals("9")
//                && (bankFlag == true)) //首期交费：现金交纳并且填写了银行编码和银行账号
//        )
//        {
//            if (bankFlag == false)
//            {
//                CError tError = new CError();
//                tError.moduleName = "ProposalBL";
//                tError.functionName = "dealDataPerson";
//                tError.errorMessage = "开户银行和银行账号不能为空";
//                this.mErrors.addOneError(tError);
//
//                return false;
//            }
//            mLCBankAuthBL.setPolNo(mLCPolBL.getPolNo());
//            mLCBankAuthBL.setPayGetFlag("0");
//
//            mLCBankAuthBL.setBankCode(mLCContSchema.getBankCode());
//            mLCBankAuthBL.setBankAccNo(mLCContSchema.getBankAccNo());
//
//            mLCBankAuthBL.setManageCom(mLCPolBL.getManageCom());
//            mLCBankAuthBL.setPayValidFlag("1"); // 开通银行代收
//            mLCBankAuthBL.setState("1"); // 正常
//            mLCBankAuthBL.setOperator(mLCPolBL.getOperator());
//            mLCBankAuthBL.setMakeDate(theCurrentDate);
//            mLCBankAuthBL.setMakeTime(theCurrentTime);
//            mLCBankAuthBL.setModifyDate(theCurrentDate);
//            mLCBankAuthBL.setModifyTime(theCurrentTime);
//
//            // 银行账户表
//            mLCBankAccBL = new LCBankAccBL();
//
//            mLCBankAccBL.setBankCode(mLCContSchema.getBankCode());
//            mLCBankAccBL.setBankAccNo(mLCContSchema.getBankAccNo());
//            mLCBankAccBL.setAccName(mLCContSchema.getAccName());
//
//            mLCBankAccBL.setModifyDate(theCurrentDate);
//            mLCBankAccBL.setModifyTime(theCurrentTime);
//        }
//        else
//        {
//            mLCBankAuthBL = null; // 银行授权书表
//            mLCBankAccBL = null; // 银行账户表
//        }

        // 受益人
        iMax = mLCBnfBLSet.size();
        for (i = 1; i <= iMax; i++)
        {
            LCBnfBL tLCBnfBL = new LCBnfBL();
            tLCBnfBL.setSchema(mLCBnfBLSet.get(i));
            tLCBnfBL.setInsuredNo(mLCInsuredBL.getInsuredNo());
            tLCBnfBL.setContNo(mLCContSchema.getContNo());
            tLCBnfBL.setPolNo(mLCPolBL.getPolNo());
            tLCBnfBL.setOperator(mGlobalInput.Operator);
            tLCBnfBL.setMakeDate(theCurrentDate);
            tLCBnfBL.setMakeTime(theCurrentTime);
//            tStr = String.valueOf(i);
            //2007.5.24  guoxq 在此将号码用流水号自动生成 
            tStr = PubFun1.CreateMaxNo("BnfNo", 20);
            tLCBnfBL.setBnfNo(tStr);
            tLCBnfBL.setDefaultFields();
            mLCBnfBLSetNew.add(tLCBnfBL);
        }

        //处理告知信息190
        LCCustomerImpartBL tLCCustomerImpartBL;
        iMax = mLCCustomerImpartBLSet.size();
        for (i = 1; i <= iMax; i++)
        {
            tLCCustomerImpartBL = new LCCustomerImpartBL();
            tLCCustomerImpartBL.setSchema(mLCCustomerImpartBLSet.get(i).getSchema());
            if ((tLCCustomerImpartBL.getCustomerNoType() != null)
                    && tLCCustomerImpartBL.getCustomerNoType().equals("A")) // 投保人告知
            {
                tLCCustomerImpartBL.setCustomerNo(mLCPolBL.getInsuredNo());
            }
            if ((tLCCustomerImpartBL.getCustomerNoType() != null)
                    && tLCCustomerImpartBL.getCustomerNoType().equals("I")) // 被保人告知
            {
                tLCCustomerImpartBL.setCustomerNo(mLCPolBL.getAppntNo());
            }
            tLCCustomerImpartBL.setDefaultFields();
            mLCCustomerImpartBLSetNew.add(tLCCustomerImpartBL);
        }

        // 特别约定
        LCSpecBL tLCSpecBL;
        iMax = mLCSpecBLSet.size();
        for (i = 1; i <= iMax; i++)
        {
            tLCSpecBL = new LCSpecBL();
            tLCSpecBL.setSchema(mLCSpecBLSet.get(i).getSchema());
            tStr = String.valueOf(i);
            tLCSpecBL.setSerialNo(PubFun1.CreateMaxNo("SpecNo"
                    , PubFun.getNoLimit(mGlobalInput.ComCode)));
            tLCSpecBL.setContNo(mLCPolBL.getContNo());
            tLCSpecBL.setGrpContNo(mLCPolBL.getGrpContNo());
            tLCSpecBL.setPolNo(mLCPolBL.getPolNo());
            tLCSpecBL.setProposalNo(mLCPolBL.getProposalNo());

            tLCSpecBL.setOperator(mGlobalInput.Operator);
            tLCSpecBL.setDefaultFields();
            tLCSpecBL.setMakeDate(tLCSpecBL.getModifyDate());
            tLCSpecBL.setMakeTime(tLCSpecBL.getModifyTime());
            mLCSpecBLSetNew.add(tLCSpecBL);
        }
        //更新合同表相关信息
        mLCContSchema.setModifyDate(theCurrentDate);
        mLCContSchema.setModifyDate(theCurrentDate);
        //mLCContSchema.PayIntv
        //mLCContSchema.setPayMode
        //mLCContSchema.setPayLocation
        //mLCContSchema.setDisputedFlag
        //mLCContSchema.setOutPayFlag
        //mLCContSchema.setGetPolMode
        //mLCContSchema.setPeoples(mLCContSchema.getPeoples()+mLCPolBL.getInsuredPeoples());

//        System.out.println("sunxy:mLCContSchema.getPrem()"+mLCContSchema.getPrem()) ;
//        System.out.println("sunxy:mLCPolBL.getPrem()"+mLCPolBL.getPrem()) ;

        mLCContSchema.setMult(mLCContSchema.getMult() + mLCPolBL.getMult());
        mLCContSchema.setPrem(mLCContSchema.getPrem() + mLCPolBL.getPrem());
        if(mLCContSchema.getPolType().equals("5"))
        {
        	if(mLCContSchema.getAmnt()==0)
        	{
        		mLCContSchema.setAmnt(mLCPolBL.getAmnt());
        	}
        	else if(mLCContSchema.getAmnt()!=0&&mLCContSchema.getAmnt()!=mLCPolBL.getAmnt())
        	{
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson";
                tError.errorMessage = "多险种共享的公共保额两次报额应该相同，请检查!";
                this.mErrors.addOneError(tError);

                return false;
        	}
        }
        else
        {
        mLCContSchema.setAmnt(mLCContSchema.getAmnt() + mLCPolBL.getAmnt());
        }
        mLCContSchema.setSumPrem(mLCContSchema.getSumPrem() +
                mLCPolBL.getSumPrem());

//         if (mLCPolBL.getPaytoDate().compareTo(mLCContSchema.getPaytoDate())>0)
//         mLCContSchema.setPaytoDate (mLCPolBL.getPaytoDate()); //最大交至日期
//         if (mLCPolBL.getFirstPayDate().compareTo(mLCContSchema.getFirstPayDate())<0)
//         mLCContSchema.setFirstPayDate(mLCPolBL.getFirstPayDate()); //最小首期缴费日期
//         if (mLCPolBL.getCValiDate().compareTo(mLCContSchema.getCValiDate())<0)
//         mLCContSchema.setCValiDate(mLCPolBL.getCValiDate()); //最小生效日期

        //设置险种的交费方式
        mLCPolBL.setPayMode(mLCContSchema.getPayMode());
        //设置险种的申请日，默认从合同上取
        mLCPolBL.setPolApplyDate(mLCContSchema.getPolApplyDate());
        //设置销售取道
        mLCPolBL.setSaleChnlDetail(mLCContSchema.getSaleChnlDetail());
        //集体下的个人投保单信息
        if (mPolType.equals("2"))
        {
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCGrpPolSchema.setModifyDate(theCurrentDate);
            mLCPolBL.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());

            if (mOperate.equals("INSERT||PROPOSAL"))
            {
                //如果添加个人，则更新集体状态
                mLCGrpContSchema.setApproveFlag("0"); //复核状态
                mLCGrpContSchema.setUWFlag("0"); //核保状态
                mLCGrpPolSchema.setApproveFlag("0"); //复核状态
                mLCGrpPolSchema.setUWFlag("0"); //核保状态

                //普通的集体下个人被保人数为1
                int NONAMENUM = 1;
                //如果保单类型是无名单：被保人人数不定，根据录入决定;如果保单类型是公共帐户：被保人人数为0，因为只是为生成集体帐户而用
                if (mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4")
                        || mLCPolBL.getPolTypeFlag().equals("2")||mLCPolBL.getPolTypeFlag().equals("5"))
                {
                    NONAMENUM = mLCPolBL.getInsuredPeoples();
                }

                //如果不是集体下无名单加人--如果是--是否也应该加人，加费
                if (!mSavePolType.equals("4"))
                {
                    //mLCGrpContSchema.setPeoples(mLCGrpContSchema.getPeoples2()
                    //                              + (1 * NONAMENUM));

                    //由于精度问题添加该段代码 modify by sunxy 2005-01-16
//                    mLCGrpContSchema.setPrem(mLCGrpContSchema.getPrem()
//                                              + mLCPolBL.getPrem());
//                    mLCGrpContSchema.setAmnt(mLCGrpContSchema.getAmnt()
//                                              + mLCPolBL.getAmnt());
//                    mLCGrpContSchema.setMult(mLCGrpContSchema.getMult()
//                                              + mLCPolBL.getMult());
//
//                    mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2()
//                                                + (1 * NONAMENUM));
//                    mLCGrpPolSchema.setPrem(mLCGrpPolSchema.getPrem()
//                                            + mLCPolBL.getPrem());
//                    mLCGrpPolSchema.setAmnt(mLCGrpPolSchema.getAmnt()
//                                            + mLCPolBL.getAmnt());


                    //由于精度问题添加该段代码 add by sunxy 2005-01-16
                    String strCalGrpPolPrem = String.valueOf(Arith.round(
                            mLCGrpPolSchema.getPrem() + mLCPolBL.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                    String strCalGrpPolAmnt = String.valueOf(Arith.round(
                            mLCGrpPolSchema.getAmnt() + mLCPolBL.getAmnt(),mSCALE)); //转换计算后的保额
                    String strCalGrpContPrem = String.valueOf(Arith.round(
                            mLCGrpContSchema.getPrem() + mLCPolBL.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                    String strCalGrpContAmnt = String.valueOf(Arith.round(
                            mLCGrpContSchema.getAmnt() + mLCPolBL.getAmnt(),mSCALE)); //转换计算后的保额
                    double calGrpPolPrem = Double.parseDouble(strCalGrpPolPrem);
                    double calGrpPolAmnt = Double.parseDouble(strCalGrpPolAmnt);
                    double calGrpContPrem = Double.parseDouble(strCalGrpContPrem);
                    double calGrpContAmnt = Double.parseDouble(strCalGrpContAmnt);

                    mLCGrpPolSchema.setPrem(calGrpPolPrem);
                    mLCGrpPolSchema.setAmnt(calGrpPolAmnt);
                    mLCGrpContSchema.setPrem(calGrpContPrem);
                    mLCGrpContSchema.setAmnt(calGrpContAmnt);

                    mLCGrpContSchema.setMult(mLCGrpContSchema.getMult() + mLCPolBL.getMult());
                    if(mLCPolBL.getPolTypeFlag().equals("2")||mLCPolBL.getPolTypeFlag().equals("5")){
                    }else{
                    	 mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2() + (1 * NONAMENUM));                    	
                    }
                   
                }
            }
            if (mOperate.equals("UPDATE||PROPOSAL"))
            {
            	LCPolDB tLCPolDB = null;
                if(this.isConCanUse)
                	tLCPolDB = new LCPolDB(this.con);
                else
                	tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(mLCPolBL.getPolNo());
                if (!tLCPolDB.getInfo())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "个人投保单表取数失败";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                if ((tLCPolDB.getApproveFlag() == null)
                        || (tLCPolDB.getUWFlag() == null))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "该个人投保单的复核状态或者核保状态存在问题！请告知信息技术部核实";
                    this.mErrors.addOneError(tError);

                    return false;
                }

                //判断如果更新的个人的复核状态或核保状态发生变化，更新集体状态
                if (!(tLCPolDB.getApproveFlag().equals(mLCPolBL.getApproveFlag())
                        && tLCPolDB.getApproveFlag().equals(mLCPolBL
                        .getApproveFlag())))
                {

                    mLCGrpContSchema.setApproveFlag("0"); //复核状态
                    mLCGrpContSchema.setUWFlag("0"); //核保状态

                    mLCGrpPolSchema.setApproveFlag("0"); //复核状态
                    mLCGrpPolSchema.setUWFlag("0"); //核保状态

                }

                int NONAMENUM = 1;
                //如果不是集体下无名单加人
                if (!mSavePolType.equals("4"))
                {
                    //上边已经加过mLCPolBL的prem,amnt,mult,sumprem,所以在这里不再加了
                    String strCalContPrem = String.valueOf(Arith.round(mLCContSchema.getPrem()
                            - tLCPolDB.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                    String strCalContAmnt = String.valueOf(Arith.round(mLCContSchema.getAmnt()
                            - tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额
                    String strCalContSumPrem = String.valueOf(Arith.round(mLCContSchema.getSumPrem()
                            - tLCPolDB.getSumPrem(),mSCALE));
                    double calContPrem = Double.parseDouble(strCalContPrem);
                    double calContAmnt = Double.parseDouble(strCalContAmnt);
                    double calContSumPrem = Double.parseDouble(strCalContSumPrem);
                    mLCContSchema.setPrem(calContPrem);
                    mLCContSchema.setAmnt(calContAmnt);
                    mLCContSchema.setMult(mLCContSchema.getMult() - tLCPolDB.getMult());
                    mLCContSchema.setSumPrem(calContSumPrem);

                    //修改集体下个人时，只有无名单可能对被保人数变化
                    if (mLCPolBL.getPolTypeFlag().equals("1") || mLCPolBL.getPolTypeFlag().equals("3") || mLCPolBL.getPolTypeFlag().equals("4"))
                    {
                        mLCGrpPolSchema.setPeoples2((mLCGrpPolSchema.getPeoples2()
                                + mLCPolBL.getInsuredPeoples())
                                - tLCPolDB.getInsuredPeoples());
                        NONAMENUM = mLCPolBL.getInsuredPeoples();
                    }
                    else
                    {
//                        mLCGrpContSchema.setPeoples2( mLCGrpContSchema.getPeoples2() + 1 );
                        mLCGrpPolSchema.setPeoples2(mLCGrpPolSchema.getPeoples2() + 1);
                    }

                    //由于精度问题注释调该段代码 add by sunxy 2005-01-16
//                    mLCGrpContSchema.setPrem((mLCGrpContSchema.getPrem()
//                                                               + mLCPolBL.getPrem())
//                                                               - tLCPolDB.getPrem());
//                                       mLCGrpContSchema.setAmnt((mLCGrpContSchema.getAmnt()
//                                                               + mLCPolBL.getAmnt())
//                                                               - tLCPolDB.getAmnt());
//                                      mLCGrpContSchema.setMult((mLCGrpContSchema.getMult()
//                                                               + mLCPolBL.getMult())
//                                                               - tLCPolDB.getMult());
//
//
//                                       mLCGrpPolSchema.setPrem((mLCGrpPolSchema.getPrem()
//                                                               + mLCPolBL.getPrem())
//                                                               - tLCPolDB.getPrem());
//                                       mLCGrpPolSchema.setAmnt((mLCGrpPolSchema.getAmnt()
//                                                               + mLCPolBL.getAmnt())
//                                            - tLCPolDB.getAmnt());

                    //由于精度问题添加该段代码 add by sunxy 2005-01-16
                    String strCalGrpPolPrem = String.valueOf(Arith.round((mLCGrpPolSchema.getPrem()
                            + mLCPolBL.getPrem()) - tLCPolDB.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                    String strCalGrpPolAmnt = String.valueOf(Arith.round((mLCGrpPolSchema.getAmnt()
                            + mLCPolBL.getAmnt()) - tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额

                    String strCalGrpContPrem = String.valueOf(Arith.round((mLCGrpContSchema.getPrem()
                            + mLCPolBL.getPrem()) - tLCPolDB.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                    String strCalGrpContAmnt = String.valueOf(Arith.round((mLCGrpContSchema.getAmnt()
                            + mLCPolBL.getAmnt()) - tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额

                    double calGrpPolPrem = Double.parseDouble(strCalGrpPolPrem);
                    double calGrpPolAmnt = Double.parseDouble(strCalGrpPolAmnt);
                    double calGrpContPrem = Double.parseDouble(strCalGrpContPrem);
                    double calGrpContAmnt = Double.parseDouble(strCalGrpContAmnt);

                    mLCGrpContSchema.setPrem(calGrpContPrem);
                    mLCGrpContSchema.setAmnt(calGrpContAmnt);
                    mLCGrpContSchema.setMult((mLCGrpContSchema.getMult() + mLCPolBL.getMult())
                            - tLCPolDB.getMult());
                    mLCGrpPolSchema.setPrem(calGrpPolPrem);
                    mLCGrpPolSchema.setAmnt(calGrpContAmnt);
                }
            }
        }
        else if (mPolType.equals("1"))
        {
            if (mOperate.equals("UPDATE||PROPOSAL"))
            {
            	LCPolDB tLCPolDB = null;
                if(this.isConCanUse)
                	tLCPolDB = new LCPolDB(this.con);
                else
                	tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(mLCPolBL.getPolNo());
                if (!tLCPolDB.getInfo())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPolDB.mErrors);

                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "个人投保单表取数失败";
                    this.mErrors.addOneError(tError);

                    return false;
                }
                String strCalContPrem = String.valueOf(Arith.round(mLCContSchema.getPrem()
                        - tLCPolDB.getPrem(),mSCALE)); //转换计算后的保费(规定的精度)
                String strCalContAmnt = String.valueOf(Arith.round(mLCContSchema.getAmnt()
                        - tLCPolDB.getAmnt(),mSCALE)); //转换计算后的保额
                String strCalContSumPrem = String.valueOf(Arith.round(mLCContSchema.getSumPrem()
                        - tLCPolDB.getSumPrem(),mSCALE));
                double calContPrem = Double.parseDouble(strCalContPrem);
                double calContAmnt = Double.parseDouble(strCalContAmnt);
                double calContSumPrem = Double.parseDouble(strCalContSumPrem);
                mLCContSchema.setPrem(calContPrem);
                mLCContSchema.setAmnt(calContAmnt);
                mLCContSchema.setMult(mLCContSchema.getMult() - tLCPolDB.getMult());
                mLCContSchema.setSumPrem(calContSumPrem);
            }
        }
        //更新 投资表的数据
/*        if (mOperate.equals("INSERT||PROPOSAL")&&mLCPerInvestPlanSet.size()>0){
        	for(int n=0;n<mLCPerInvestPlanSet.size();n++){
        		mLCPerInvestPlanSet.get(n).setPolNo(mLCPolBL.getPolNo());
        		mLCPerInvestPlanSet.get(n).setGrpPolNo(mLCPolBL.getGrpPolNo());
        		mLCPerInvestPlanSet.get(n).setGrpContNo(mLCPolBL.getGrpContNo());
        		mLCPerInvestPlanSet.get(n).setInsuredNo(mLCPolBL.getInsuredNo());
        		String mRiskCode=mLCPerInvestPlanSet.get(n).getRiskCode();
        		String mPayPlanCode = mLCPerInvestPlanSet.get(n).getPayPlanCode();
        		String mSQL = "select insuaccno from lmriskaccpay where riskcode='"+mRiskCode
        			+"' and payplancode='"+mPayPlanCode;
        		mLCPerInvestPlanSet.get(n).setInsuAccNo(mExeSQL.execSQL(mSQL).GetText(1, 1));
        		mLCPerInvestPlanSet.get(n).setOperator(mGlobalInput.Operator);
        		mLCPerInvestPlanSet.get(n).setMakeDate(theCurrentDate);
        		mLCPerInvestPlanSet.get(n).setMakeTime(theCurrentTime);
        		mLCPerInvestPlanSet.get(n).setModifyOperator(mGlobalInput.Operator);
        		mLCPerInvestPlanSet.get(n).setModifyDate(theCurrentDate);
        		mLCPerInvestPlanSet.get(n).setModifyTime(theCurrentTime);
        	}
        }
*/        
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        if (mOperate.equals("DELETE||PROPOSAL"))
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));

            // 保单
            mLCPolBL.setSchema((LCPolSchema) cInputData.getObjectByObjectName("LCPolSchema", 0));
            LCPolDB tLCPolDB = null;
            if(this.isConCanUse)
            	tLCPolDB = new LCPolDB(this.con);
            else
            	tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCPolBL.getProposalNo());
            if (!tLCPolDB.getInfo())
            {
                CError.buildErr(this, "查询险种保单失败！");
                return false;
            }
            mLCPolBL.setSchema(tLCPolDB.getSchema());
        }
        else
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));

            // 前台向后台传递的数据容器：放置一些不能通过Schema传递的变量
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
            if (mTransferData == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
                this.mErrors.addOneError(tError);

                return false;
            }
            
			

            if (mTransferData.getValueByName("GrpImport") != null)
            {
                mGrpImportFlag = true;
                //传入主险保单,因为前面有LCPolSchema,为区别索引，特从第10个为止开始查找
                LCPolBL tMainPolBL = (LCPolBL) cInputData.getObjectByObjectName("LCPolBL", 10);
                if (tMainPolBL == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "传入主险保单不能为空!";
                    this.mErrors.addOneError(tError);

                    return false;

                }
                mainLCPolBL = tMainPolBL;
                //mainLCPolBL .setSchema( tMainPol  );
            }
            //合同
            mLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName("LCContSchema"
                    , 0));
            if (mLCContSchema.getContNo() == null)
            {
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到合同的数据!";
                this.mErrors.addOneError(tError);

                return false;

            }
            else
            {
                if (!mGrpImportFlag)
                {
                	LCContDB mLCContDB = null;
                    if(this.isConCanUse)
                    	mLCContDB = new LCContDB(this.con);
                    else
                    	mLCContDB = new LCContDB();
                    mLCContDB.setSchema(mLCContSchema);
                    mLCContDB.getInfo();
                    if (mLCContDB.mErrors.needDealError())
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "查询合同的数据失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    mLCContSchema.setSchema(mLCContDB);
                }
            }
            // 保单
            mLCPolBL.setSchema((LCPolSchema) cInputData.getObjectByObjectName("LCPolSchema", 0));

            if (mTransferData == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
                this.mErrors.addOneError(tError);

                return false;
            }
            //PQ
//            if (mTransferData.getValueByName("samePersonFlag") == null)
//            {
//                // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "ProposalBL";
//                tError.functionName = "prepareData";
//                tError.errorMessage = "没有接收到samePersonFlag标志。";
//                this.mErrors.addOneError(tError);
//
//                return false;
//            }
            if (("1").equals(mLCContSchema.getContType()))
            {
                //投保人信息，被保险人信息从数据库里查询
                if (!mGrpImportFlag)
                {
                    mLCAppntBL.setSchema((LCAppntSchema) cInputData.getObjectByObjectName(
                            "LCAppntSchema", 0));
                    LCAppntDB tLCAppntDB = null;
                    if(this.isConCanUse)
                    	tLCAppntDB = new LCAppntDB(this.con);
                    else
                    	tLCAppntDB = new LCAppntDB();
                    tLCAppntDB.setContNo(mLCAppntBL.getContNo());
                    tLCAppntDB.setAppntNo(mLCAppntBL.getAppntNo());
                    if (tLCAppntDB.getInfo())
                    {
                        mLCAppntBL.setSchema(tLCAppntDB);
                    }
                    else
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "没有查到传入的投保人客户信息!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }

            }
            else
            {

                mLCGrpAppntBL.setSchema((LCGrpAppntSchema) cInputData.getObjectByObjectName(
                        "LCGrpAppntSchema", 0));
                LCGrpAppntDB tLCGrpAppntDB = null;
                if(this.isConCanUse)
                	tLCGrpAppntDB = new LCGrpAppntDB(this.con);
                else
                	tLCGrpAppntDB = new LCGrpAppntDB();
                tLCGrpAppntDB.setGrpContNo(mLCGrpAppntBL.getGrpContNo());
                tLCGrpAppntDB.setCustomerNo(mLCGrpAppntBL.getCustomerNo());
                if (tLCGrpAppntDB.getInfo())
                {
                    mLCGrpAppntBL.setSchema(tLCGrpAppntDB);
                    // tLCGrpAppntDB.setSchema(tLCGrpAppntDB);
                }
                else
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "没有查到传入的集体投保人客户信息!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }

            //被保险人
            //mLCInsuredBLSet.set((LCInsuredSet) cInputData.getObjectByObjectName("LCInsuredSet",
            //                                                                     0));
            mLCInsuredBL.setSchema((LCInsuredSchema) cInputData.getObjectByObjectName(
                    "LCInsuredSchema", 0));
            if (!mGrpImportFlag)
            {
            	LCInsuredDB tLCInsuredDB = null;
                if(this.isConCanUse)
                	tLCInsuredDB = new LCInsuredDB(this.con);
                else
                	tLCInsuredDB = new LCInsuredDB();
                tLCInsuredDB.setContNo(mLCInsuredBL.getContNo());
                tLCInsuredDB.setInsuredNo(mLCInsuredBL.getInsuredNo());
                if (tLCInsuredDB.getInfo())
                {
                    mLCInsuredBL.setSchema(tLCInsuredDB);
                }
                else
                {

                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "没有查到传入的被保险人客户信息!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }

            //连带被保险人
            mLCInsuredRelatedSet.set((LCInsuredRelatedSet) cInputData.getObjectByObjectName(
                    "LCInsuredRelatedSet", 0));
            // 受益人
            mLCBnfBLSet.set((LCBnfSet) cInputData.getObjectByObjectName("LCBnfSet", 0));

            // 告知信息
            mLCCustomerImpartBLSet.set((LCCustomerImpartSet) cInputData.getObjectByObjectName(
                    "LCCustomerImpartSet", 0));

            int impartCount = mLCCustomerImpartBLSet.size();
            for (int i = 1; i <= impartCount; i++)
            {
                LCCustomerImpartSchema tImpart = mLCCustomerImpartBLSet.get(i);
                String impartCode = tImpart.getImpartCode();
                String impartVer = tImpart.getImpartVer();
                String customerType = tImpart.getCustomerNoType();
                for (int j = i + 1; j <= impartCount; j++)
                {
                    LCCustomerImpartSchema tImpart1 = mLCCustomerImpartBLSet.get(j);
                    String impartCode1 = tImpart1.getImpartCode();
                    String impartVer1 = tImpart1.getImpartVer();
                    String customerType1 = tImpart1.getCustomerNoType();
                    if (impartCode.equals(impartCode1)
                            && impartVer.equals(impartVer1)
                            && customerType.equals(customerType1))
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "prepareData";
                        tError.errorMessage = "客户告知录入重复。";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    // end of if
                }
                // end of for
            }
            // end of for

            // 特别约定
            mLCSpecBLSet.set((LCSpecSet) cInputData.getObjectByObjectName("LCSpecSet", 0));

            //地址信息
            if(cInputData.getObjectByObjectName("LCAddressSchema",0)!= null)
            {
              mLCAddressSchema.setSchema( (LCAddressSchema) cInputData.
                                         getObjectByObjectName("LCAddressSchema", 0));
            }
            //账户信息
            if(cInputData.getObjectByObjectName("LCAccountSchema",0)!= null)
            {
              mLCAccountSchema.setSchema( (LCAccountSchema) cInputData.
                                         getObjectByObjectName("LCAccountSchema", 0));
            }else{
            	mLCAccountSchema = null;
            }
            
            // 一般责任信息(接收传入的单个责任信息)
            if (cInputData.getObjectByObjectName("LCDutySchema", 0) != null)
            {
                mLCDutyBL.setSchema((LCDutySchema) cInputData.getObjectByObjectName("LCDutySchema"
                        , 0));
                if (mLCDutyBL.getPayEndYear() > 0)
                {
                    if ((mLCDutyBL.getPayEndYearFlag() == null)
                            || mLCDutyBL.getPayEndYearFlag().equals(""))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "责任项的缴费期间单位PayEndYearFlag不能为空!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                }
                if(mLCPolBL!=null){
                	mLCPolBL.setPayEndYear(mLCDutyBL.getPayEndYear());
                	mLCPolBL.setPayEndYearFlag(mLCDutyBL.getPayEndYearFlag());
                	mLCPolBL.setGetYear(mLCDutyBL.getGetYear());
                	mLCPolBL.setGetYearFlag(mLCDutyBL.getGetYearFlag());
                }
            }

            // 可选责任(接收传入的多个责任信息)
            LCDutySet tLCDutySet = (LCDutySet) cInputData.getObjectByObjectName("LCDutySet", 0);

            //初始化不需要责任的标记为假-该标记针对传入的单个责任做判断
            boolean isNullDuty = false;
            if ((mLCDutyBL.getPayEndDate() == null)
                    && (mLCDutyBL.getPayEndYear() == 0)
                    && (mLCDutyBL.getPayEndYearFlag() == null)
                    && (mLCDutyBL.getGetYearFlag() == null)
                    && (mLCDutyBL.getGetStartType() == null)
                    && (mLCDutyBL.getGetYear() == 0)
                    && (mLCDutyBL.getEndDate() == null)
                    && (mLCDutyBL.getInsuYear() == 0)
                    && (mLCDutyBL.getInsuYearFlag() == null)
                    && (mLCDutyBL.getPayIntv() < 0))
            {
                isNullDuty = true;
            }

            //如果传入的多个责任信息和传入的单个责任信息为空，则需要责任标记为假。
            if ((tLCDutySet == null) && isNullDuty)
            {
                mNeedDuty = false;
            }
            else
            {
                //如果传入的可选责任集合为空，保存录入的单个责任
                if (tLCDutySet == null)
                {
                    mLCDutyBLSet1.add(mLCDutyBL.getSchema());
                    
                }
                else
                {
                    //如果有可选责任，保存可选责任
                    mLCDutyBLSet1.set(tLCDutySet);
                    ttLCDutySchema1=tLCDutySet.get(1);
                    
                }
                mNeedDuty = true;
            }
//            if (mTransferData.getValueByName("samePersonFlag").equals("0")
//                    && (mLCAppntBL == null))
//            {
//                // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "ProposalBL";
//                tError.functionName = "getInputData";
//                tError.errorMessage = "投保人与被保人不是同一个人时，投保人数据不能为空!";
//                this.mErrors.addOneError(tError);
//
//                return false;
//            }
            if ((mLCPolBL == null) || (mLCInsuredBLSet == null) || (mLCBnfBLSet == null)
                    || (mLCCustomerImpartBLSet == null) || (mLCSpecBLSet == null) || (mLCContSchema == null))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage =
                        "在投保数据接受时没有得到足够的数据，请您确认有：合同，险种,投保人,被保人,受益人,告知信息,特别约定的信息!";
                this.mErrors.addOneError(tError);

                return false;
            }
            //PQ 现在根据LCCont里的账户信息判断
            //Lis5.3 upgrade get
            if (StrTool.compareString(mLCPolBL.getPayLocation(), "0")
                    && (StrTool.cTrim(mLCContSchema.getBankCode()).equals("")
                    || StrTool.cTrim(mLCContSchema.getBankAccNo()).equals("")))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "收费方式选择了银行自动转账，但是合同信息中没有录入开户行和银行账号信息!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //判断是否删除应收总表信息
//            if (mTransferData.getValueByName("samePersonFlag").equals("1"))
//            {
//                deleteAccNo = true;
//            }

            //判断保存的类型是
            if (mTransferData.getValueByName("SavePolType") != null)
            {
                mSavePolType = (String) mTransferData.getValueByName("SavePolType");
                if ((mSavePolType == null) || mSavePolType.trim().equals(""))
                {
                    mSavePolType = "0";
                }
            }
            if (mTransferData.getValueByName("EdorType") != null)
            {
                EdorType = (String) mTransferData.getValueByName("EdorType");
                if ((EdorType == null) || EdorType.trim().equals(""))
                {
                    EdorType = "";
                }
                else
                {
                	if("PB".equals(EdorType)||"PE".equals(EdorType))
                	{
                		preEdorType = EdorType;
                		mSavePolType = "2";
                		EdorType="NI";
                	}
                }
            }

            mLCPolBL.setContNo(mLCContSchema.getContNo());
            mLCPolBL.setProposalContNo(mLCContSchema.getProposalContNo());
            mLCPolBL.setPrtNo(mLCContSchema.getPrtNo());
//            mLCPolBL.setproposal(mLCContSchema.getGrpPolNo());
            mLCPolBL.setAgentCode(mLCContSchema.getAgentCode());
            mLCPolBL.setAgentGroup(mLCContSchema.getAgentGroup());
            mLCPolBL.setGrpContNo(mLCContSchema.getGrpContNo());
            mLCPolBL.setContType(mLCContSchema.getContType());
            mLCPolBL.setPolTypeFlag(mLCContSchema.getPolType());
            //判断个单时，置团体险种保单号全0
            if (mLCPolBL.getContType().equals("1"))
            {
                mLCPolBL.setGrpPolNo(SysConst.ZERONO);
            }

        }
        

        //得到磁盘投保传来的集体投保单信息，注意，这里是引用，后面对该对象的修改会保留下来
        mLCGrpPolSchema = (LCGrpPolSchema) cInputData.getObjectByObjectName("LCGrpPolSchema", 0);
        mLCGrpContSchema = (LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0);
        //得到需要特殊处理的保费项纪录，如果存在这样的纪录(譬如：众悦年金分红)
        mDiskLCPremBLSet.set((LCPremSet) cInputData.getObjectByObjectName("LCPremSet", 0));
        if (mDiskLCPremBLSet != null)
        {
            if (mDiskLCPremBLSet.size() > 0)
            {
                noCalFlag = true;

                //保单传入的保费项置为0--否则在修改的时候会翻倍
                mLCPolBL.setPrem(0);
                mLCPolBL.setStandPrem(0);
            }
        }

        //当磁盘投保时，会从外部传入该值-后面会判断
        mLMRiskAppSchema = (LMRiskAppSchema) cInputData.getObjectByObjectName("LMRiskAppSchema", 0);
        mLMRiskSchema = (LMRiskSchema) cInputData.getObjectByObjectName("LMRiskSchema", 0);
//        mLDGrpSchema = (LDGrpSchema) cInputData.getObjectByObjectName(
//                "LDGrpSchema",
//                0);

//        //如果界面没有录入浮动费率，此时mLCPolBL的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
//        //保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
//        //PQ 浮动费率改变到责任级，需要改变
//        FloatRate = mLCPolBL.getFloatRate();
//
//        //如果界面录入浮动费率=-1，则计算浮动费率的标记为真
//        if (FloatRate == ConstRate)
//        {
//            autoCalFloatRateFlag = true;
//
//            //将保单中的该子段设为1，后面描述算法中计算
//            mLCPolBL.setFloatRate(1);
//        }
//        InputPrem = mLCPolBL.getPrem();
//        InputAmnt = mLCPolBL.getAmnt();
//        //浮动费率处理END
        //获得是否为保单计划变更标志:0 表示是:1 表示否
        if (mTransferData.getValueByName("ChangePlanFlag") != null)
        {
//            System.out.println("----ChangePlanFlag------"
//                               + mTransferData.getValueByName("ChangePlanFlag"));
            mChangePlanFlag = (String) mTransferData.getValueByName("ChangePlanFlag");
        }
       
        //投资比例表
//        mLCPerInvestPlanSet = (LCPerInvestPlanSet)cInputData.getObjectByObjectName("LCPerInvertPlanSet", 0);
        
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean prepareOutputData()
    {
		// int i;
		// int iMax;

		// 为增加公共保额添加的，不能产生保费
		if (preEdorType != null && "PE".equals(preEdorType)
				&& mLCPolBL.getSchema().getPrem() > 0) {
			CError.buildErr(this, "保全项目：增加公共账户不能产生保费，请添加完账户后在追加保费中增加保费。谢谢合作！");
			return false;
		}

		mInputData = new VData();
		if ("GA".equals(this.EdorType)) {
			MMap tMMap = new MMap();
			mInputData.add(tMMap);
			try {
				LPPolSchema tLPPolSchema = new LPPolSchema();
				this.mref.transFields(tLPPolSchema, mLCPolBL.getSchema());
				tLPPolSchema.setEdorNo(this.mEdorNo);
				tLPPolSchema.setEdorType(this.EdorType);
				tMMap.put("DELETE from LPPol where edorno = '" + mEdorNo
						+ "' and edortype = '" + EdorType + "' and contno = '"
						+ mLCPolBL.getContNo() + "'", "DELETE");
				tMMap.put(tLPPolSchema, "DELETE&INSERT");
				
				LPPremSet tLPPremSet = new LPPremSet();
				for (int i = 1; i <= mLCPremBLSet.size(); i++) {
					LPPremSchema tLPPremSchema = new LPPremSchema();
					this.mref.transFields(tLPPremSchema, mLCPremBLSet.get(i)
							.getSchema());
					tLPPremSchema.setEdorNo(this.mEdorNo);
					tLPPremSchema.setEdorType(this.EdorType);
					tLPPremSet.add(tLPPremSchema);
				}
				tMMap.put("DELETE from LPPrem where edorno = '" + mEdorNo
						+ "' and edortype = '" + EdorType + "' and contno = '"
						+ mLCPolBL.getContNo() + "'", "DELETE");
				tMMap.put(tLPPremSet, "DELETE&INSERT");
				
				LPGetSet tLPGetSet = new LPGetSet();
				for (int i = 1; i <= mLCGetBLSet.size(); i++) {
					LPGetSchema tLPGetSchema = new LPGetSchema();
					this.mref.transFields(tLPGetSchema, mLCGetBLSet.get(i)
							.getSchema());
					tLPGetSchema.setEdorNo(this.mEdorNo);
					tLPGetSchema.setEdorType(this.EdorType);
					tLPGetSchema.setNeedAcc("1");// 万能/投连转年金，账户相关
					tLPGetSet.add(tLPGetSchema);
				}
				tMMap.put("DELETE from LPGet where edorno = '" + mEdorNo
						+ "' and edortype = '" + EdorType + "' and contno = '"
						+ mLCPolBL.getContNo() + "'", "DELETE");
				tMMap.put(tLPGetSet, "DELETE&INSERT");

				LPDutySet tLPDutySet = new LPDutySet();
				for (int i = 1; i <= mLCDutyBLSet.size(); i++) {
					LPDutySchema tLPGetSchema = new LPDutySchema();
					this.mref.transFields(tLPGetSchema, mLCDutyBLSet.get(i)
							.getSchema());
					tLPGetSchema.setEdorNo(this.mEdorNo);
					tLPGetSchema.setEdorType(this.EdorType);
					tLPGetSchema.setPolNo(tLPPolSchema.getPolNo());
					tLPDutySet.add(tLPGetSchema);
				}
				tMMap.put("DELETE from LPDuty where edorno = '" + mEdorNo
						+ "' and edortype = '" + EdorType + "' and contno = '"
						+ mLCPolBL.getContNo() + "'", "DELETE");
				tMMap.put(tLPDutySet, "DELETE&INSERT");

				if (mPolType.equals("2")) {
					LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
					this.mref.transFields(tLPGrpPolSchema, mLCGrpPolSchema);
					tLPGrpPolSchema.setEdorNo(this.mEdorNo);
					tLPGrpPolSchema.setEdorType(this.EdorType);
					tMMap.put("DELETE from LPGrpPol where edorno = '" + mEdorNo
							+ "' and edortype = '" + EdorType
							+ "' and Grpcontno = '" + mLCPolBL.getGrpContNo()
							+ "'", "DELETE");
					tMMap.put(tLPGrpPolSchema, "DELETE&INSERT");
					if (!mOperate.equals("DELETE||PROPOSAL")) {
						// 如果个人标记不为0-即保全标记,则此时保证团单的状态不变
						if (!mLCPolBL.getAppFlag().equals("0")) {
							LCGrpPolDB tempLCGrpPolDB = null;
							if (this.isConCanUse)
								tempLCGrpPolDB = new LCGrpPolDB(this.con);
							else
								tempLCGrpPolDB = new LCGrpPolDB();
							tempLCGrpPolDB.setGrpPolNo(mLCGrpPolSchema
									.getGrpPolNo());
							if (tempLCGrpPolDB.getInfo()) {
								mLCGrpPolSchema.setApproveFlag(tempLCGrpPolDB
										.getApproveFlag());
								mLCGrpPolSchema.setUWFlag(tempLCGrpPolDB
										.getUWFlag());
							} else {
								mLCGrpPolSchema.setApproveFlag("9");
								mLCGrpPolSchema.setUWFlag("9");
							}
						}
					}
				}

			} catch (Exception ex) {
				// @@错误处理
				CError.buildErr(this, "在准备往后层处理所需要的数据时出错。");
				return false;
			}

		} else if ("AP".equals(this.EdorType)) {
			MMap tMMap = new MMap();
			mInputData.add(tMMap);
			try {
				tMMap.put(mLCPolBL.getSchema(), "INSERT");

				tMMap.put("INSERT into lppol select '" + this.mEdorNo + "','"
						+ this.EdorType
						+ "',a.* from lcpol a where a.polno = '"
						+ mLCPolBL.getPolNo() + "'", "INSERT");
				tMMap.put(
						"UPDATE LCPol set prem = 0,standprem = 0,sumprem = 0 where polno = '"
								+ mLCPolBL.getPolNo() + "'", "UPDATE");
				LCPremSet tLPPremSet = new LCPremSet();
				tLPPremSet.add(mLCPremBLSet);
				tMMap.put(tLPPremSet, "INSERT");

				tMMap.put("INSERT into lpprem select '" + this.mEdorNo + "','"
						+ this.EdorType
						+ "',a.* from lcprem a where a.polno = '"
						+ mLCPolBL.getPolNo() + "'", "INSERT");
				tMMap.put(
						"UPDATE lcprem set prem = 0,standprem = 0,sumprem = 0 where polno = '"
								+ mLCPolBL.getPolNo() + "'", "UPDATE");
				LCGetSet tLPGetSet = new LCGetSet();
				tLPGetSet.set(mLCGetBLSet);
				tMMap.put(tLPGetSet, "INSERT");
				tMMap.put("INSERT into lpget select '" + this.mEdorNo + "','"
						+ this.EdorType
						+ "',a.* from lcget a where a.polno = '"
						+ mLCPolBL.getPolNo() + "'", "INSERT");

				LCDutySet tLPDutySet = new LCDutySet();
				tLPDutySet.set(mLCDutyBLSet);
				tMMap.put(tLPDutySet, "INSERT");
				tMMap.put("INSERT into lpduty select '" + this.mEdorNo + "','"
						+ this.EdorType
						+ "',a.* from LCDuty a where a.polno = '"
						+ mLCPolBL.getPolNo() + "'", "INSERT");
				tMMap.put(
						"UPDATE lcduty set prem = 0,standprem = 0,sumprem = 0 where polno = '"
								+ mLCPolBL.getPolNo() + "'", "UPDATE");

			} catch (Exception ex) {
				// @@错误处理
				CError.buildErr(this, "在准备往后层处理所需要的数据时出错。");
				return false;
			}
			
		} else {
			try {
				// 不能改变顺序!!

				mInputData.add(mAppntType);
				mInputData.add(mPolType);
				mInputData.add(mLCContSchema);
				mInputData.add(mLCPolBL.getSchema());
				mInputData.add(mLCAppntBL.getSchema());
				mInputData.add(mLCGrpAppntBL.getSchema());

				// if (mLCBankAuthBL != null)
				// {
				// mInputData.add(mLCBankAuthBL.getSchema());
				// }
				//
				// if (mLCBankAccBL != null)
				// {
				// mInputData.add(mLCBankAccBL.getSchema());
				// }

				// LCInsuredSet tLCInsuredSet = new LCInsuredSet();
				// tLCInsuredSet = (LCInsuredSet) mLCInsuredBLSetNew;
				// mInputData.add(tLCInsuredSet);

				LCBnfSet tLCBnfSet = new LCBnfSet();
				tLCBnfSet = mLCBnfBLSetNew;
				mInputData.add(tLCBnfSet);

				LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
				tLCCustomerImpartSet.set(mLCCustomerImpartBLSetNew);
				mInputData.add(mLCCustomerImpartBLSetNew);

				LCSpecSet tLCSpecSet = new LCSpecSet();
				tLCSpecSet = mLCSpecBLSetNew;
				mInputData.add(tLCSpecSet);

				LCPremSet tLCPremSet = new LCPremSet();
				tLCPremSet = mLCPremBLSet;
				mInputData.add(tLCPremSet);

				LCGetSet tLCGetSet = new LCGetSet();
				tLCGetSet = mLCGetBLSet;
				mInputData.add(tLCGetSet);

				mInputData.add(mLCDutyBLSet);

				if (mPolType.equals("2")) {
					mInputData.add(mLCGrpPolSchema);
					mInputData.add(mLCGrpContSchema);
					if (!mOperate.equals("DELETE||PROPOSAL")) {
						// 如果个人标记不为0-即保全标记,则此时保证团单的状态不变
						if (!mLCPolBL.getAppFlag().equals("0")) {
							LCGrpPolDB tempLCGrpPolDB = null;
							if (this.isConCanUse)
								tempLCGrpPolDB = new LCGrpPolDB(this.con);
							else
								tempLCGrpPolDB = new LCGrpPolDB();
							tempLCGrpPolDB.setGrpPolNo(mLCGrpPolSchema
									.getGrpPolNo());
							if (tempLCGrpPolDB.getInfo()) {
								mLCGrpPolSchema.setApproveFlag(tempLCGrpPolDB
										.getApproveFlag());
								mLCGrpPolSchema.setUWFlag(tempLCGrpPolDB
										.getUWFlag());
							} else {
								mLCGrpPolSchema.setApproveFlag("9");
								mLCGrpPolSchema.setUWFlag("9");
							}
						}
					}
				}
				mInputData.add(mLCInsuredRelatedSet);
				mInputData.add(mLDPersonSet);
				mInputData.add(mLJSPaySet);
				mInputData.add(mGlobalInput);
				mInputData.add(mLCAddressSchema);
				mInputData.add(mLCAccountSchema);
				// mInputData.add(mLCPerInvestPlanSet);
		
				//add by winnie ASR20093075 用作缓存计划产品使用
				mInputData.add(mLMRiskAppSchema);
				//end add
			} catch (Exception ex) {
				// @@错误处理
				CError.buildErr(this, "在准备往后层处理所需要的数据时出错。");
				return false;
			}
		}
		// 准备往前面传输的数据
		mResult.clear();
		mResult.add(mLCPolBL.getSchema());
		mResult.add(mLCDutyBLSet);
	
		return true;
	}

    public VData getResult()
    {
        return mResult;
    }



    /**
	 * 校验字段
	 * 
	 * @param operType
	 *            String
	 * @return boolean
	 */
    private boolean CheckTBField(String operType)
    {
        // 保单 mLCPolBL mLCGrpPolBL
        // 投保人 mLCAppntBL mLCAppntGrpBL
        // 被保人 mLCInsuredBLSet mLCInsuredBLSetNew
        // 受益人 mLCBnfBLSet mLCBnfBLSetNew
        // 告知信息 mLCCustomerImpartBLSet mLCCustomerImpartBLSetNew
        // 特别约定 mLCSpecBLSet mLCSpecBLSetNew
        // 保费项表 mLCPremBLSet 保存特殊的保费项数据(目前针对磁盘投保，不用计算保费保额类型)
        // 给付项表 mLCGetBLSet
        // 一般的责任信息 mLCDutyBL
        // 责任表 mLCDutyBLSet
        String strMsg = "";
        boolean MsgFlag = false;
// LCInsuredSchema tLCInsuredSchema = mLCInsuredBLSetNew.get(1);
// LCDutySchema tLCDutySchema = mLCDutyBLSet.get(1);

        String RiskCode = mLCPolBL.getRiskCode();

        try
        {
            VData tVData = new VData();
            VData ttVData = new VData();
            CheckFieldCom tCheckFieldCom = new CheckFieldCom();
            CheckFieldCom ttCheckFieldCom = new CheckFieldCom();
            // 计算要素
            FieldCarrier tFieldCarrier = new FieldCarrier();
            tFieldCarrier.setInsuredNo(mLCPolBL.getInsuredNo()); // 被保人号码
            tFieldCarrier.setAppAge(mLCPolBL.getInsuredAppAge()); // 被保人年龄
            tFieldCarrier.setInsuredName(mLCPolBL.getInsuredName()); // 被保人姓名
            tFieldCarrier.setSex(mLCPolBL.getInsuredSex()); // 被保人性别
            tFieldCarrier.setMult(mLCPolBL.getMult()); // 投保份数
            tFieldCarrier.setPolNo(mLCPolBL.getPolNo()); // 投保单号码
            tFieldCarrier.setMainPolNo(mLCPolBL.getMainPolNo()); // 主险号码
            tFieldCarrier.setRiskCode(mLCPolBL.getRiskCode()); // 险种编码
            tFieldCarrier.setCValiDate(mLCPolBL.getCValiDate()); // 生效日期
            tFieldCarrier.setAmnt(mLCPolBL.getAmnt()); // 保额
            tFieldCarrier.setPrem(mLCPolBL.getPrem());
            tFieldCarrier.setInsuredBirthday(mLCPolBL.getInsuredBirthday()); //被保人出生日期
            tFieldCarrier.setInsuYear(mLCPolBL.getInsuYear()); //保险期间
            tFieldCarrier.setInsuYearFlag(mLCPolBL.getInsuYearFlag()); //保险期间单位
            tFieldCarrier.setPayEndYear(mLCPolBL.getPayEndYear()); //交费期间
            tFieldCarrier.setPayEndYearFlag(mLCPolBL.getPayEndYearFlag()); //交费期间单位
            tFieldCarrier.setPayIntv(mLCPolBL.getPayIntv()); //交费方式
            tFieldCarrier.setPayYears(mLCPolBL.getPayYears()); //交费年期
            tFieldCarrier.setOccupationType(mLCPolBL.getOccupationType()); //被保人职业类别
            tFieldCarrier.setGrpPolNo(mLCPolBL.getGrpPolNo());
            tFieldCarrier.setContNo(mLCPolBL.getContNo());
            tFieldCarrier.setEndDate(mLCPolBL.getEndDate());
//            System.out.println("保单类型为："+mLCPolBL.getPolTypeFlag());
            tFieldCarrier.setPolTypeFlag(mLCPolBL.getPolTypeFlag());
            LCPremSchema tLCPremSchema = null;
            for (int i = 1; i <= mLCPremBLSet.size(); i++)
            {
                tLCPremSchema = mLCPremBLSet.get(i);
            }
            /*Lis5.3 upgrade get
             System.out.println("进入算法表的管理费比例为"+tLCPremSchema.getManageFeeRate());
             tFieldCarrier.setManageFeeRate(tLCPremSchema.getManageFeeRate());
             */
//            System.out.println("进入算法表的责任编码为"+tLCPremSchema.getDutyCode());
            tFieldCarrier.setDutyCode(tLCPremSchema.getDutyCode());

            if (mLCPolBL.getStandbyFlag1() != null)
            {
                tFieldCarrier.setStandbyFlag1(mLCPolBL.getStandbyFlag1());
            }
            if (mLCPolBL.getStandbyFlag2() != null)
            {
                tFieldCarrier.setStandbyFlag2(mLCPolBL.getStandbyFlag2());
            }
            if (mLCPolBL.getStandbyFlag3() != null)
            {
                tFieldCarrier.setStandbyFlag3(mLCPolBL.getStandbyFlag3());
            }
        
            tVData.add(tFieldCarrier);

            LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
            tLMCheckFieldSchema.setRiskCode(RiskCode);
            tLMCheckFieldSchema.setFieldName("TB" + operType); //投保
            tVData.add(tLMCheckFieldSchema);

            ttVData.add(tFieldCarrier);
            LMCheckFieldSchema ttLMCheckFieldSchema = new LMCheckFieldSchema();
            ttLMCheckFieldSchema.setRiskCode("000000");
            ttLMCheckFieldSchema.setFieldName("TB" + operType); //投保
            ttVData.add(ttLMCheckFieldSchema);
            //step1
            if (ttCheckFieldCom.CheckField(ttVData))
            {
                LMCheckFieldSet mLMCheckFieldSet = ttCheckFieldCom.GetCheckFieldSet();
                for (int n = 1; n <= mLMCheckFieldSet.size(); n++)
                {
                    LMCheckFieldSchema tField = mLMCheckFieldSet.get(n);
                    if ((tField.getReturnValiFlag() != null)
                            && tField.getReturnValiFlag().equals("N"))
                    {
                        if ((tField.getMsgFlag() != null)
                                && tField.getMsgFlag().equals("Y"))
                        {
                            MsgFlag = true;
                            strMsg = strMsg + tField.getMsg() + " ; ";

                            break;
                        }
                    }
                }
                if (MsgFlag)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "CheckTBField";
                    tError.errorMessage = "数据有误：" + strMsg;
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
            else
            {
                this.mErrors.copyAllErrors(tCheckFieldCom.mErrors);
                return false;
            }
            //step2
            if (tCheckFieldCom.CheckField(tVData))
            {
                LMCheckFieldSet mLMCheckFieldSet = tCheckFieldCom.GetCheckFieldSet();
                for (int n = 1; n <= mLMCheckFieldSet.size(); n++)
                {
                    LMCheckFieldSchema tField = mLMCheckFieldSet.get(n);
                    if ((tField.getReturnValiFlag() != null)
                            && tField.getReturnValiFlag().equals("N"))
                    {
                        if ((tField.getMsgFlag() != null)
                                && tField.getMsgFlag().equals("Y"))
                        {
                            MsgFlag = true;
                            strMsg = strMsg + tField.getMsg() + " ; ";
                            break;
                        }
                    }
                }
                if (MsgFlag)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "CheckTBField";
                    tError.errorMessage = "数据有误：" + strMsg;
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
            else
            {
                this.mErrors.copyAllErrors(tCheckFieldCom.mErrors);
                return false;
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "CheckTBField";
            tError.errorMessage = "发生错误，请检验CheckField模块:" + ex;
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 准备处理数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean PrepareSubmitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据操作校验
        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (!this.mOperate.equals("DELETE||PROPOSAL"))
        {
            if (this.mOperate.equals("INSERT||PROPOSAL"))
            {
//                if (!CheckTBField("INSERT"))
//                {
//                    return false;
//                }
            }
            else
            {
                if (!CheckTBField("UPDATE"))
                {
                    return false;
                }
            }
        }

        //准备往后台的数据
        if (prepareOutputData())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 和PrepareSubmitData配合使用，返回准备数据
     * @return VData
     */
    public VData getSubmitResult()
    {
        return mInputData;
    }

    /**
     * 保费保额计算调用部分
     * @return boolean
     */
    public boolean pubCal()
    { 	
        //是否传入领取项的标记getDutykind
        String getDutykind = (String) mTransferData.getValueByName("GetDutyKind");
        LCGetBL tLCGetBL = new LCGetBL();
        CalBL tCalBL;
        if (mNeedDuty)
        {
            LMRiskDutySet tLMRiskDutySet = mCRI.findRiskDutyByRiskCode(mLCPolBL.getRiskCode());
            if (tLMRiskDutySet == null)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(mCRI.mErrors);
                mCRI.mErrors.clearErrors();

                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "pubCal";
                tError.errorMessage = "LMRiskDuty表查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            //初始化FloatRate
            FloatRate=new double[mLCDutyBLSet1.size()];
            //统一费率处理
            for (int i = 1; i <= mLCDutyBLSet1.size(); i++)
            {
                LCDutySchema tLCDutySchema = mLCDutyBLSet1.get(i);
                //chenwm 0128 重置ttLCDutySchema1,初始化的时候置的set第一个
                ttLCDutySchema1 = mLCDutyBLSet1.get(i);
                String dutyCode = mLCDutyBLSet1.get(i).getDutyCode();
                // String calMode = "";

                //如果界面没有录入浮动费率，此时mLCDutySchema的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
                //保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
                //FloatRate = tLCDutySchema.getFloatRate();

                InputPrem = tLCDutySchema.getPrem();
                InputAmnt = tLCDutySchema.getAmnt();
                if (mLCDutyBLSet1.size() == 1 && InputPrem == 0.0 && InputAmnt == 0.0)
                {
                    InputPrem = mLCPolBL.getPrem();
                    InputAmnt = mLCPolBL.getAmnt();
                }
              
                //保存计算方向
                String calRule = tLCDutySchema.getCalRule();
                if (calRule == null)
                {
                    //默认为表定费率
                    tLCDutySchema.setCalRule("0");
                }
                //如果是统一费率，需要去默认值或者保险计划里取出统一费率的值,算出保费保额
                else if (calRule.equals("1"))
                {
                	String tContPlanCode = "";
                	LCContPlanDutyParamDB tLCContPlanDutyParamDB = null;
                    if(this.isConCanUse)
                    	tLCContPlanDutyParamDB = new LCContPlanDutyParamDB(this.con);
                    else
                    	tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
                    LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
                    tLCContPlanDutyParamDB.setGrpContNo(mLCPolBL.getGrpContNo());
                    tLCContPlanDutyParamDB.setDutyCode(dutyCode);
                    tLCContPlanDutyParamDB.setRiskCode(mLCPolBL.getRiskCode());
                    tLCContPlanDutyParamDB.setMainRiskCode(mainLCPolBL.getRiskCode());
                    tLCContPlanDutyParamDB.setCalFactor("FloatRate");


                    if (StrTool.compareString(mLCInsuredBL.getContPlanCode(), ""))
                    {
                        tLCContPlanDutyParamDB.setContPlanCode("00");
                    }
                    else
                    {
                        tLCContPlanDutyParamDB.setContPlanCode(mLCInsuredBL.getContPlanCode());
                    }
                   
                    tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
                    if (tLCContPlanDutyParamDB.mErrors.needDealError())
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "pubCal";
                        tError.errorMessage = "LCContPlanDutyParam查询失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }                                
                   
                    //如果在计划中没有取到,需要去默认值里去
                    if (tLCContPlanDutyParamSet.size() <= 0
                            || (!StrTool.compareString(mLCInsuredBL.getContPlanCode()
                            , "")
                            && StrTool.compareString(tLCContPlanDutyParamSet.get(1).
                            getCalFactorValue()
                            , "")))
                    {
                        tLCContPlanDutyParamDB.setContPlanCode("00");
                        tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
                        if (tLCContPlanDutyParamDB.mErrors.needDealError())
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "pubCal";
                            tError.errorMessage = "LCContPlanDutyParam查询失败!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                    }
                    
                    System.out.println(ttLCDutySchema1.getFloatRate());
                    if (tLCContPlanDutyParamSet.size() <= 0
                            || StrTool.compareString(tLCContPlanDutyParamSet.get(1).
                            getCalFactorValue(), ""))
                    {
                        if(ttLCDutySchema1.getFloatRate()!=0)
                        {
                        	aflag1=1;
                        }
                        else
                        {
                        // @@错误处理
                    	aflag1=0;
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "pubCal";
                        tError.errorMessage =
                                "计划为："+mLCInsuredBL.getContPlanCode()+"计算方向为统一费率，但是在保险计划于默认值中均未查到统一费率值!";
                        this.mErrors.addOneError(tError);
                        return false;
                        }

                    }
                   
                    //中意新加的功能,统一费率计算.如过有保额的话,回去先按保额计算保费.如果没有保额的话,回去找工资算保额,然后
                    //再按统一费率去算保费的
                    if(InputAmnt == 0) {
	                    tContPlanCode = tLCContPlanDutyParamDB.getContPlanCode();                    
	                    //重新去取StandbyFlag1,StandbyFlag2,StandbyFlag3
	                    LCContPlanDutyParamDB mLCContPlanDutyParamDB = null;
	                    if(this.isConCanUse)
	                    	mLCContPlanDutyParamDB = new LCContPlanDutyParamDB(this.con);
	                    else
	                    	mLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
	                    LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
	                    mLCContPlanDutyParamDB.setGrpContNo(mLCPolBL.getGrpContNo());
	                    mLCContPlanDutyParamDB.setDutyCode(dutyCode);
	                    mLCContPlanDutyParamDB.setRiskCode(mLCPolBL.getRiskCode());
	                    mLCContPlanDutyParamDB.setMainRiskCode(mainLCPolBL.getRiskCode());
	                    mLCContPlanDutyParamDB.setContPlanCode(tContPlanCode);
	                    
	                    mLCContPlanDutyParamSet = mLCContPlanDutyParamDB.query();
	                    if (mLCContPlanDutyParamDB.mErrors.needDealError())
	                    {
	                        // @@错误处理
	                        CError tError = new CError();
	                        tError.moduleName = "ProposalBL";
	                        tError.functionName = "pubCal";
	                        tError.errorMessage = "LCContPlanDutyParam查询失败!";
	                        this.mErrors.addOneError(tError);
	                        return false;
	                    }       
	                    
	                    Calculator mCalculator = null;
						if (isConCanUse)
							mCalculator = new Calculator(this.con);
						else
							mCalculator = new Calculator();
					
	                    mCalculator.setCalCode("YXSBE1");  // 用月薪算保额
	                    
	                    CalBase mCalBase = new CalBase();
	                 
	                    
	                    for(int j =1;j<=mLCContPlanDutyParamSet.size();j++) {
	                    	if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactor(), "StandbyFlag1")){
	                    		if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactorValue(), "")) {
	                    			mCalBase.setStandbyFlag1("");
	                    		}else {
	                    			mCalBase.setStandbyFlag1(mLCContPlanDutyParamSet.get(j).getCalFactorValue());
	                    		}
	                    		continue ;
	                    	}else if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactor(), "StandbyFlag2")) {
	                    		if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactorValue(), "")) {
	                    			mCalBase.setStandbyFlag2("");
	                    		}else {
	                    			mCalBase.setStandbyFlag2(mLCContPlanDutyParamSet.get(j).getCalFactorValue());
	                    		}
	                    		continue;
	                    	}else if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactor(), "StandbyFlag3")) {
	                    		if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactorValue(), "")) {
	                    			mCalBase.setStandbyFlag3("");
	                    		}else {
	                    			mCalBase.setStandbyFlag3(mLCContPlanDutyParamSet.get(j).getCalFactorValue());
	                    		}
	                    		continue;
	                    	}else if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactor(), "StandbyFlag4")) {
	                    		if(StrTool.compareString(mLCContPlanDutyParamSet.get(j).getCalFactorValue(), "")) {
	                    		}else {
	                    		}
	                    		continue;
	                    	}
	                    }
	                    
	                    mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
	                    mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
	                    mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
	                    mCalculator.addBasicFactor("CalType", EdorType);
	                    mCalculator.addBasicFactor("GrpPolNo", mLCPolBL.getGrpPolNo());
	                 	                    
	                    String tStr = "";
	                    tStr = mCalculator.calculate();
	                    if (tStr.trim().equals(""))
	                    {
//	                    	 @@错误处理
	                        CError tError = new CError();
	                        tError.moduleName = "ProposalBL";
	                        tError.functionName = "pubCal";
	                        tError.errorMessage =
	                                "计划为："+mLCInsuredBL.getContPlanCode()+"计算方向为统一费率的时候，算不出保额,请检查保额或月薪有没有录入!";
	                        this.mErrors.addOneError(tError);
	                        return false ;
	                    }
	                    else
	                    {
	                    	InputAmnt = Double.parseDouble(tStr);
	                    }
                    }
                    //测试加入GIL 算法 中意 张阔 20070518
                    if(EdorType.equals("NI")){
                    	LCGrpPolDB tLCGrpPolDB = null;
                         if(this.isConCanUse)
                        	 tLCGrpPolDB = new LCGrpPolDB(this.con);
                         else
                        	 tLCGrpPolDB = new LCGrpPolDB();
                    	 tLCGrpPolDB.setGrpPolNo(mLCPolBL.getGrpPolNo());
                    	 if(!tLCGrpPolDB.getInfo()){
                    		 CError tError = new CError();
                             tError.moduleName = "ProposalBL";
                             tError.functionName = "dealDataPerson";
                             tError.errorMessage = "没有查到相应的团单险种!";
                             this.mErrors.addOneError(tError);
                             return false;
                    	 }
                    	 LMRiskAppDB tLMRiskAppDB = null;
                         if(this.isConCanUse)
                        	 tLMRiskAppDB = new LMRiskAppDB(this.con);
                         else
                        	 tLMRiskAppDB = new LMRiskAppDB();
                    	 tLMRiskAppDB.setRiskType8("1");
                    	LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.query();
                    	
                    		 for(int risk=1;risk<=tLMRiskAppSet.size();risk++){} 
                    }
                    
                    //月薪变更的GIL
                    if(EdorType.equals("CS")){
                    	LCGrpPolDB tLCGrpPolDB = null;
                         if(this.isConCanUse)
                        	 tLCGrpPolDB = new LCGrpPolDB(this.con);
                         else
                        	 tLCGrpPolDB = new LCGrpPolDB();
                    	 tLCGrpPolDB.setGrpPolNo(mLCPolBL.getGrpPolNo());
                    	 if(!tLCGrpPolDB.getInfo()){
                    		 CError tError = new CError();
                             tError.moduleName = "ProposalBL";
                             tError.functionName = "dealDataPerson";
                             tError.errorMessage = "没有查到相应的团单险种!";
                             this.mErrors.addOneError(tError);
                             return false;
                    	 }
                    	 LMRiskAppDB tLMRiskAppDB = null;
                         if(this.isConCanUse)
                        	 tLMRiskAppDB = new LMRiskAppDB(this.con);
                         else
                        	 tLMRiskAppDB = new LMRiskAppDB();
                    	 tLMRiskAppDB.setRiskType8("1");
                    	LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.query();
                    	
                    	
                    	 ExeSQL exesql = new ExeSQL();
        				 String sqlli1="select polno from lcpol where contno = '"+mLCInsuredBL.getContNo()+"' and riskcode = '"+mLCPolBL.getRiskCode()+"'";
        				 SSRS InsuRSSRSli1 = new SSRS();
        				 InsuRSSRSli1 = exesql.execSQL(sqlli1);
        				 if(InsuRSSRSli1!=null&&InsuRSSRSli1.MaxRow>0)
        				 {
        					 mLCPolBL.setPolNo(InsuRSSRSli1.GetText(1, 1));
        				 }
        				 else
        				 {
                    		 CError tError = new CError();
                             tError.moduleName = "ProposalBL";
                             tError.functionName = "dealDataPerson";
                             tError.errorMessage = "查询原险种信息失败!";
                             this.mErrors.addOneError(tError);
                             return false;
        				 }                    	
                    	
                    	
                    	
                    		 for(int risk=1;risk<=tLMRiskAppSet.size();risk++){
                    			 if(tLMRiskAppSet.get(risk).getRiskCode().equals(tLCGrpPolDB.getRiskCode()))
                    			 {
                    				 double amntli = 0.0;
                    				 String sqlli3="select amnt from lcduty where polno = '"+InsuRSSRSli1.GetText(1, 1)+"' and dutycode = '"+mLCDutyBLSet1.get(i).getDutyCode()+"'";
                    				 SSRS InsuRSSRSli3 = new SSRS();
                    				 InsuRSSRSli3 = exesql.execSQL(sqlli3);
                    				 if(InsuRSSRSli3.MaxRow>0 && InsuRSSRSli3!=null&&!("".equals(InsuRSSRSli3.GetText(1, 1)))) //Amended By Fang for PIR20102780
                    				 {
                    				 }
                    				 else
                    				 {
                    				 }
                    				
                    		  }
                    		 } 
                    }
                    
                    //保障变更的GIL
                    if(EdorType.equals("LC")){
                    	LCGrpPolDB tLCGrpPolDB = null;
                        if(this.isConCanUse)
                       	 tLCGrpPolDB = new LCGrpPolDB(this.con);
                        else
                       	 tLCGrpPolDB = new LCGrpPolDB();
                   	 tLCGrpPolDB.setGrpPolNo(mLCPolBL.getGrpPolNo());
                   	 if(!tLCGrpPolDB.getInfo()){
                   		 CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "dealDataPerson";
                            tError.errorMessage = "没有查到相应的团单险种!";
                            this.mErrors.addOneError(tError);
                            return false;
                   	 }
                   	 LMRiskAppDB tLMRiskAppDB = null;
                        if(this.isConCanUse)
                       	 tLMRiskAppDB = new LMRiskAppDB(this.con);
                        else
                       	 tLMRiskAppDB = new LMRiskAppDB();
                   	 tLMRiskAppDB.setRiskType8("1");
                   	LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.query();
                   	
                   	
                   	 ExeSQL exesql = new ExeSQL();
       				 String sqlli1="select polno from lcpol where contno = '"+mLCInsuredBL.getContNo()+"' and riskcode = '"+mLCPolBL.getRiskCode()+"'";
       				 SSRS InsuRSSRSli1 = new SSRS();
       				 InsuRSSRSli1 = exesql.execSQL(sqlli1);
       				 if(InsuRSSRSli1!=null&&InsuRSSRSli1.MaxRow>0)
       				 {
       					 mLCPolBL.setPolNo(InsuRSSRSli1.GetText(1, 1));
       				 }
       				 else
       				 {

       				 }                    	
                   	
                   	
                   	
                   		 for(int risk=1;risk<=tLMRiskAppSet.size();risk++){
                   			 if(tLMRiskAppSet.get(risk).getRiskCode().equals(tLCGrpPolDB.getRiskCode()))
                   			 {
                			  double amntli = 0.0;
                			  if(InsuRSSRSli1!=null&&InsuRSSRSli1.MaxRow>0)
                				 {
                				 String sqlli3="select amnt from lcduty where polno = '"+InsuRSSRSli1.GetText(1, 1)+"' and dutycode = '"+mLCDutyBLSet1.get(i).getDutyCode()+"'";
                				 SSRS InsuRSSRSli3 = new SSRS();
                				 InsuRSSRSli3 = exesql.execSQL(sqlli3);
                				 if(InsuRSSRSli3!=null&&InsuRSSRSli3.getMaxRow()>0&&!("".equals(InsuRSSRSli3.GetText(1, 1))))
                				 {
                				 }
                				 else
                				 {
         				 }
                			  }
                			  else
                			  {
                			  }
                			 
        		  }
                   		 } 
                   }
                    
                    
                    
                    double oneFloatRate;
                    if(aflag1==1)
                    {
                    	oneFloatRate=ttLCDutySchema1.getFloatRate();
                    }
                    else
                    {
                    oneFloatRate = Double.parseDouble(tLCContPlanDutyParamSet.get(1).getCalFactorValue());
                    }
                    
                    //如果界面上录入保费：
                    if ((InputPrem > 0) && (InputAmnt > 0))
                    {
                        double Prem1 = Arith.mul(InputAmnt,oneFloatRate);
                    	if (InputPrem
                                != Arith.round(Prem1,mSCALE))
                        {
                            CError tError = new CError();
                            tError.moduleName = "CalBL";
                            tError.functionName = "calOneDuty";
                            tError.errorMessage = "您录入的保费保额（" + InputPrem + "," +
                                    InputAmnt
                                    + "）与统一费率（" + oneFloatRate +
                                    "）不符合！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    else if ((InputPrem > 0))
                    {
                        InputAmnt = Arith.div(InputPrem,oneFloatRate,mSCALE);
                    }
                    else if ((InputAmnt > 0))
                    {
                    	double Prem1 = Arith.mul(InputAmnt,oneFloatRate);
                        InputPrem = Arith.round(Prem1,mSCALE);
                    }
                    //autoCalFloatRateFlag = true;
                    
//                    ExeSQL mExeSQL = new ExeSQL();
                    SSRS bSSRS = new SSRS();
                  String  strSQL = "select rateintv from ratepayintv where payintv='"+tLCDutySchema.getPayIntv()+"'";
    				
                  bSSRS = mExeSQL.execSQL(strSQL);
                   
                  if(bSSRS.getMaxRow()>0) {
                	  InputPrem = Arith.mul(InputPrem,Double.parseDouble(bSSRS.GetText(1,1))); //保费在乘以比例
                  }
                	//统一费率需要保存计划录入的费率 
                  	FloatRate[i-1]=tLCDutySchema.getFloatRate();
                	//将保单中的该子段设为1，后面描述算法中计算
                    tLCDutySchema.setFloatRate(1);
                    tLCDutySchema.setPrem(InputPrem);
                    tLCDutySchema.setStandPrem(InputPrem);
                    tLCDutySchema.setAmnt(InputAmnt);
                    if (mLCDutyBLSet1.size() == 1)
                    {
                        mLCPolBL.setPrem(InputPrem);
                        mLCPolBL.setStandPrem(InputPrem);
                        mLCPolBL.setAmnt(InputAmnt);
                    }
                }
                else if(calRule.equals("3"))
                {
                	//对期交保费的处理，InputPrem录入的必须是一年期保费
//                	ExeSQL mExeSQL = new ExeSQL();
                	String strSQL = "select rateintv*"+InputPrem+" from ratepayintv where payintv=decode("+tLCDutySchema.getPayIntv()+",-1,0,12,0,"+tLCDutySchema.getPayIntv()+")";
                	String tPrem = mExeSQL.getOneValue(strSQL);
                	if(tPrem==null||"".equals(tPrem))
                	{
                		CError.buildErr(this,"执行SQL失败！");
                		return false;
                	}
                	InputPrem = Double.parseDouble(tPrem);
                	 tLCDutySchema.setPrem(InputPrem);
                     tLCDutySchema.setStandPrem(InputPrem);
                     if (mLCDutyBLSet1.size() == 1)
                     {
                         mLCPolBL.setPrem(InputPrem);
                         mLCPolBL.setStandPrem(InputPrem);
                         //mLCPolBL.setAmnt(InputAmnt);
                     }
                     //noCalFlag=true;
                }
            }
            //测试加入GIL 算法 中意 张阔 20070518
            if(EdorType.equals("NI")){
            	LCGrpPolDB tLCGrpPolDB = null;
                 if(this.isConCanUse)
                	 tLCGrpPolDB = new LCGrpPolDB(this.con);
                 else
                	 tLCGrpPolDB = new LCGrpPolDB();
            	 tLCGrpPolDB.setGrpPolNo(mLCPolBL.getGrpPolNo());
            	 if(!tLCGrpPolDB.getInfo()){
            		 CError tError = new CError();
                     tError.moduleName = "ProposalBL";
                     tError.functionName = "dealDataPerson";
                     tError.errorMessage = "没有查到相应的团单险种!";
                     this.mErrors.addOneError(tError);
                     return false;
            	 }
            	 LMRiskAppDB tLMRiskAppDB = null;
                 if(this.isConCanUse)
                	 tLMRiskAppDB = new LMRiskAppDB(this.con);
                 else
                	 tLMRiskAppDB = new LMRiskAppDB();
            	 tLMRiskAppDB.setRiskType8("1");
            	LMRiskAppSet tLMRiskAppSet = tLMRiskAppDB.query();
            	
            	for(int duty=1;duty<=mLCDutyBLSet1.size();duty++){
            		 for(int risk=1;risk<=tLMRiskAppSet.size();risk++){} 
            	 }
            }
           
            
            
            //需要传入责任计算
            if (StrTool.cTrim(getDutykind).equals(""))
            {
                // 传入LCPol的和页面录入的责任信息（单个或可选责任）数据,保存在tCalBL对象的内部成员中，后续将用到
                if(!this.isConCanUse)
                	tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet1, EdorType);
                else
                	tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet1, EdorType,this.con);
            }
            else
            {
                // 传入投保单和领取项和责任项的数据
                tLCGetBL.setGetDutyKind(getDutykind);
                LCGetBLSet tLCGetBLSet = new LCGetBLSet();
                tLCGetBLSet.add(tLCGetBL);
                //mLCPolBL-传入的投保单信息 ， mLCDutyBLSet1-传入的责任信息 ， tLCGetBLSet-保存传入的getDutykind字段，其它为空。
                if(!this.isConCanUse)
                	tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet1, tLCGetBLSet, EdorType);
                else
                	tCalBL = new CalBL(mLCPolBL, mLCDutyBLSet1, tLCGetBLSet, EdorType,this.con);

            }
        }
        else
        {
            //不传入责任计算
            if (StrTool.cTrim(getDutykind).equals(""))
            {
                // 只传入LCPol的数据
            	if(!this.isConCanUse)
            		tCalBL = new CalBL(mLCPolBL, EdorType);
            	else
            		tCalBL = new CalBL(mLCPolBL, EdorType,this.con);
            }
            else
            {
                // 传入LCPol和LCGet的数据
                tLCGetBL.setGetDutyKind(getDutykind);
                LCGetBLSet tLCGetBLSet = new LCGetBLSet();
                tLCGetBLSet.add(tLCGetBL);
                if(!this.isConCanUse)
                	tCalBL = new CalBL(mLCPolBL, tLCGetBLSet, EdorType);
                else
                	tCalBL = new CalBL(mLCPolBL, tLCGetBLSet, EdorType,this.con);
            }
        }
        //将被保人信息传入 Alex 20051028
        tCalBL.setLCInsuredSchema(mLCInsuredBL.getSchema());

        
        tCalBL.setNoCalFalg(noCalFlag); //将是否需要计算的标记传入计算类中
       
        long t=System.currentTimeMillis();
        if (!noCalFlag) //如果需要计算
        {
//            System.out.println("before tCalBL.calPol()");
        	
            if (!tCalBL.calPol()) //前面tCalBL的成员变量保存了传入的数据后，这里计算将用到这些成员变量
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson-cal1";
                tError.errorMessage = tCalBL.mErrors.getFirstError()
                        +
                        "(如果没有具体错误原因，请依次检查：1 交费方式和交费期间（或保险期间）是否匹配 2 保费，保额是否输入 3 相关要素（譬如年龄）是否超过费率计算范围 4 职业代码是否和计算相关) ";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("tCalBL.calPol:"+(System.currentTimeMillis()-t));
        }
        else
        {
            if (!tCalBL.calPol2(mDiskLCPremBLSet)) //前面tCalBL的成员变量保存了传入的数据后，这里计算将用到这些成员变量
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson-cal2";
                tError.errorMessage = tCalBL.mErrors.getFirstError()
                        +
                        "(如果没有具体错误原因，请依次检查：1 交费方式和交费期间（或保险期间）是否匹配 2 保费，保额是否输入 3 相关要素（譬如年龄）是否超过费率计算范围 4 职业代码是否和计算相关) ";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //取出计算的结果
        mLCPolBL.setSchema(tCalBL.getLCPol().getSchema());
        LCContDB tLCContDB1 = null;
        if(this.isConCanUse)
        	tLCContDB1 = new LCContDB(this.con);
        else
        	tLCContDB1 = new LCContDB();
        LCContSet tLCContSet1 = new LCContSet();
        LCContSchema tLCContSchema1 = new LCContSchema();
        tLCContDB1.setContNo(mLCPolBL.getContNo());
        tLCContSet1=tLCContDB1.query();
        if(tLCContSet1.size()!=0&&mLCPolBL.getAmnt()!=0&&tLCContSchema1.getPeoples()!=0)
        {
        tLCContSchema1=tLCContSet1.get(1);
        mLCPolBL.setAmnt(tLCContSchema1.getPeoples()*mLCPolBL.getAmnt());
        }
        
        mLCPremBLSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
        mLCGetBLSet = tCalBL.getLCGet();
        mLCDutyBLSet = tCalBL.getLCDuty();
//        mLCPremBLSet.setPolNo(mLCPolBL.getPolNo());//不在这处理
//        mLCGetBLSet.setPolNo(mLCPolBL.getPolNo());//不在这处理
        if (mOperate.equals("INSERT||PROPOSAL"))
        {
            mLCPolBL.setDefaultFields();
        }
//不在这处理
//        for (int i = 1; i <= mLCPremBLSet.size(); i++)
//        {
//            mLCPremBLSet.get(i).setOperator(mGlobalInput.Operator);
//            mLCPremBLSet.get(i).setMakeDate(theCurrentDate);
//            mLCPremBLSet.get(i).setMakeTime(theCurrentTime);
//            mLCPremBLSet.get(i).setModifyDate(theCurrentDate);
//            mLCPremBLSet.get(i).setModifyTime(theCurrentTime);
//        }
//      不在这处理
//        for (int i = 1; i <= mLCGetBLSet.size(); i++)
//        {
//            mLCGetBLSet.get(i).setOperator(mGlobalInput.Operator);
//            mLCGetBLSet.get(i).setMakeDate(theCurrentDate);
//            mLCGetBLSet.get(i).setMakeTime(theCurrentTime);
//            mLCGetBLSet.get(i).setModifyDate(theCurrentDate);
//            mLCGetBLSet.get(i).setModifyTime(theCurrentTime);
//        }

        //将加费的保费项查询出来,并加入到保费项集合中
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append("select * from lcprem where polno='");
        tSBql.append(mLCPolBL.getPolNo());
        tSBql.append("' and PayPlanCode like '000000%'");
        LCPremDB tempLCPremDB = null;
        if(this.isConCanUse)
        	tempLCPremDB = new LCPremDB(this.con);
        else
        	tempLCPremDB = new LCPremDB();
        LCPremSet tempLCPremSet = new LCPremSet();
        tempLCPremSet = tempLCPremDB.executeQuery(tSBql.toString());
        mLCPremBLSet.add(tempLCPremSet);

//        //处理浮动费率--houzm add
//        if (!dealFloatRate(mLCPolBL, mLCPremBLSet, mLCGetBLSet, mLCDutyBLSet))
//        {
//            return false;
//        }
//        System.out.println("step1:"+(System.currentTimeMillis()-ttt));
        HashMap tHMPrem  = new HashMap();
        // 保费项
        for (int j = 1; j <= mLCPremBLSet.size(); j++)
        {
            LCPremSchema tLCPremSchema = mLCPremBLSet.get(j);
            tLCPremSchema.setPolNo(mLCPolBL.getPolNo());
            if (!tLCPremSchema.getPayPlanCode().startsWith("000000"))
            {
            	tLCPremSchema.setOperator(mGlobalInput.Operator);
            	tLCPremSchema.setMakeDate(theCurrentDate);
            	tLCPremSchema.setMakeTime(theCurrentTime);
            	tLCPremSchema.setModifyDate(theCurrentDate);
            	tLCPremSchema.setModifyTime(theCurrentTime);
            	//add by 张阔, 计算期交未满期
            	if((EdorType.equals("CS")||EdorType.equals("LC")||EdorType.equals("NI"))&&(mLCPolBL.getPayIntv()!=0&&mLCPolBL.getPayIntv()!=-1)){
            		
            		String tGrpCValiDate = mLCGrpContSchema.getCValiDate();//保单生效日
            		LCGrpPolDB tLCGrpPolDB = null;
                    if(this.isConCanUse)
                  	  tLCGrpPolDB = new LCGrpPolDB(this.con);
                    else
                  	  tLCGrpPolDB = new LCGrpPolDB();
                    tLCGrpPolDB.setGrpPolNo(mLCPolBL.getGrpPolNo());
                    if (!tLCGrpPolDB.getInfo())
                    {
                        mErrors.addOneError(new CError("查询LCGrpPol数据失败！"));
                        return false;
                    }
                    String tGrpPaytoDate = tLCGrpPolDB.getPaytoDate(); //保单交至日
                    String tGrpPayEndDate = tLCGrpPolDB.getPayEndDate(); //保单终交日
                    int tPayIntv = mLCPolBL.getPayIntv(); //交费期间单位
	               	 //对生效日期根据保单的CalType计算方式进行处理
	               	String tEdorValiDate = mLCPolBL.getCValiDate(); //个人生效日期
	               	if(EdorType.equals("NI")||EdorType.equals("LC")||EdorType.equals("CS"))
	               	{
	   	            	 String sqlStr = " select GrpContNo_cvalidate('"+mLCPolBL.getGrpContNo()+"','"+mLCPolBL.getGrpPolNo()+"','"+tEdorValiDate+"') From dual "; //如果是指定的保单，当月1号，返回当天，否则返回下月1号
	   	            	 tEdorValiDate = mExeSQL.getOneValue(sqlStr);
	   	            	 System.out.println("生效日期处理后为："+mLCPolBL.getRiskCode()+":"+tEdorValiDate+" SQL:"+sqlStr);
	               	}
	               	double tStandPrem = tLCPremSchema.getStandPrem(); //标准保费

            		//calSumPrem(String GrpCvalidate,String GrpPaytodate,String GrpPayenddate,int GrpPayintv,String EdorValiDate,double Standprem)
            		double[] result = calSumPrem( tGrpCValiDate, tGrpPaytoDate, tGrpPayEndDate,tPayIntv,tEdorValiDate,tStandPrem);
            		System.out.println(result[0]+"  "+result[1]);
        			tLCPremSchema.setPrem(result[0]);
        			tLCPremSchema.setSumPrem(result[1]);
//            		double getMoney=0;
//            	 BqCalBase tBqCalBase = new BqCalBase();
//            	 //对生效日期根据保单的CalType计算方式进行处理
//            	 String tCValiDate = mLCPolBL.getCValiDate();
//            	 if(EdorType.equals("NI")||EdorType.equals("LC")||EdorType.equals("CS"))
//            	 {
//	            	 String sqlStr = " select GrpContNo_cvalidate('"+mLCPolBL.getGrpContNo()+"','"+mLCPolBL.getGrpPolNo()+"','"+tCValiDate+"') From dual "; //如果是指定的保单，当月1号，返回当天，否则返回下月1号
//	            	 tCValiDate = mExeSQL.getOneValue(sqlStr);
//	            	 System.out.println("生效日期处理后为："+mLCPolBL.getRiskCode()+":"+tCValiDate+" SQL:"+sqlStr);
//            	 }
//                  tBqCalBase.setCValiDate(tCValiDate);
//                  tBqCalBase.setEdorValiDate(tCValiDate);
//                  tBqCalBase.setPayIntv(mLCPolBL.getPayIntv());
//                  LCGrpPolDB tLCGrpPolDB = null;
//                  if(this.isConCanUse)
//                	  tLCGrpPolDB = new LCGrpPolDB(this.con);
//                  else
//                	  tLCGrpPolDB = new LCGrpPolDB();
//                  tLCGrpPolDB.setGrpPolNo(mLCPolBL.getGrpPolNo());
//                  if (!tLCGrpPolDB.getInfo())
//                  {
//                      mErrors.addOneError(new CError("查询LCGrpPol数据失败！"));
//                      return false;
//                  }
//                  tBqCalBase.setPayToDate(tLCGrpPolDB.getPaytoDate());
//                  int paycont=0;
//                	//添加跨期处理
//                	//计算退保的所在区间
//                	String tEdorValiDate=tBqCalBase.getEdorValiDate();//保全生效日
//                	String tPayToDate=tBqCalBase.getPayToDate();
//                	
//                	int nowpaycount = 0; //目前为止已经过了多少期
//                	int EdorPaycout = 0; //保全生效日时，已经过了多少期
//                	double lastStandprem = 0; //末期的标准保费（计算未满期比例之后的）
//                	double PremStandPrem = tLCPremSchema.getStandPrem(); //用于保存整期的standprem,tLCPremSchema.setStandPrem的被赋其它值用于计算后，最终要改回来
//                	String tGrpCValiDate = mLCGrpContSchema.getCValiDate();//得到保单生效日，用于推算目前已经过了多少期 
//                	String tEdorPaytodate1 = "";                           //保全生效日所在期间的起始日
//                	String tEdorPaytodate2 = mLCGrpContSchema.getCValiDate();//保全生效日所在期间的终止日 
//                	for(nowpaycount=0 ; tLCGrpPolDB.getPaytoDate().compareTo(tGrpCValiDate) > 0 ;nowpaycount++)
//                	{
//                		tGrpCValiDate = PubFun.calDate(tGrpCValiDate, Integer.parseInt(tBqCalBase.getPayIntv()), "M", null);
//                	}
//                	System.out.println("nowpaycount==" + nowpaycount+" tGrpCValiDate="+tGrpCValiDate); //得到目前过了多少期
//                	                	
//                	for(EdorPaycout=0 ; tEdorValiDate.compareTo(tEdorPaytodate2) > 0 ;EdorPaycout++)
//                	{
//                		tEdorPaytodate2 = PubFun.calDate(tEdorPaytodate2, Integer.parseInt(tBqCalBase.getPayIntv()), "M", null);
//                	}
//                	tEdorPaytodate1 = PubFun.calDate(tEdorPaytodate2, -Integer.parseInt(tBqCalBase.getPayIntv()), "M", null);
//                	System.out.println("EdorPaycout==" + EdorPaycout); //保全生效日时，已经过的期数
//                	System.out.println("tEdorPaytodate1="+tEdorPaytodate1+" tEdorPaytodate2="+tEdorPaytodate2);
//                	
//                	paycont = nowpaycount- EdorPaycout;  // 不包括保全所在期，还有多少期
//                	
//                	if(tGrpCValiDate.compareTo(tLCGrpPolDB.getPayEndDate()) > 0) //说明已经是末期，且最后一期不是整期
//                	{
//                		//防止交费期间不为payintv整数倍的情况，算出的tGrpCValiDate会超过payenddate,，不是整齐的情况；如11个月包年交保单                	
//	 		            BqCalBL tBqCalBL = new BqCalBL();
//						BqCalBase ttBqCalBase = new BqCalBase();
//						ttBqCalBase.setPayIntv(0);//交费间隔为0.为了取算法中的第一步
//						ttBqCalBase.setPayToDate(tGrpCValiDate);//通过paytintv算出的交至日						
//						ttBqCalBase.setEdorValiDate(tLCGrpPolDB.getPayEndDate());//实际终交日
//						tGrpCValiDate = PubFun.calDate(tGrpCValiDate, -Integer.parseInt(tBqCalBase.getPayIntv()), "M", null);
//						ttBqCalBase.setCValiDate(tGrpCValiDate);//末期的起始日
//						double WMQ = tBqCalBL.calChgMoney("QJWMQ0", ttBqCalBase);
//						lastStandprem = Arith.round(Arith.mul(tLCPremSchema.getStandPrem(), Arith.sub(1,WMQ)),2);						
//                	}else
//                	{
//                		lastStandprem = tLCPremSchema.getStandPrem();
//                	}
//                	if(tEdorPaytodate2.compareTo(tLCGrpPolDB.getPayEndDate()) > 0)//若保全生效日所在期间为最后一期,特殊处理
//                	{                		
//                		tEdorPaytodate2 = tLCGrpPolDB.getPayEndDate();
//                		tLCPremSchema.setStandPrem(lastStandprem);
//                	}
//                	
//                	tBqCalBase.setPayToDate(tEdorPaytodate2);
//                	                		                		
//                        int bakPayIntv = Integer.parseInt(tBqCalBase.getPayIntv());
//                        String bakCvaliDate = tBqCalBase.getCValiDate();
//                		                   
//                    	if(tBqCalBase.getPayToDate()!=null&&tBqCalBase.getPayIntv()!=null
//                    	&&!("".equals(tBqCalBase.getPayToDate()))&&!("".equals(tBqCalBase.getPayIntv())))
//                    	{
//                    		tBqCalBase.setCValiDate(tEdorPaytodate1);
//                    		tBqCalBase.setPayIntv(0);
//                    	}
//                    	System.out.println("tBqCalBase.getCValiDate()="+tBqCalBase.getCValiDate());                    	
//                		
//                		System.out.println("paycont="+paycont);
//                		
//                			BqCalBL tBqCalBL = new BqCalBL();
//                			getMoney = tLCPremSchema.getStandPrem()*tBqCalBL.calChgMoney("QJWMQ0", tBqCalBase); // 计算保费补退总额
//                			if (tBqCalBL.mErrors.needDealError())
//                			{
//                				mErrors.copyAllErrors(tBqCalBL.mErrors);
//                				mErrors.addOneError(new CError("计算补退费金额失败！"));
//                				return false;
//                			}
//                			
//                    	tBqCalBase.setPayIntv(bakPayIntv);
//                    	tBqCalBase.setCValiDate(bakCvaliDate);		
//                		if(0==paycont){  // 不跨期
//                			tLCPremSchema.setPrem(Arith.round(getMoney,2));//保费保留两位精度
//                			tLCPremSchema.setSumPrem(Arith.round(getMoney,2));
//                		}else{           // 跨期 
//                			tLCPremSchema.setPrem(Arith.round(lastStandprem,2));
//                			tLCPremSchema.setSumPrem(Arith.round(getMoney+lastStandprem+tLCPremSchema.getStandPrem()*(paycont-1),2));
//                		}
//                		tLCPremSchema.setStandPrem(PremStandPrem); // 把standprem赋原值
            	}else{
            			tLCPremSchema.setPrem(Arith.round(tLCPremSchema.getStandPrem(),2));
            	}
            }
//          保费项按dutycode分组
            if(tHMPrem.containsKey(tLCPremSchema.getDutyCode())){
            	LCPremBLSet tLCPremBLSet = (LCPremBLSet)tHMPrem.get(tLCPremSchema.getDutyCode());
            	tLCPremBLSet.add(tLCPremSchema);
            }else{
            	LCPremBLSet tLCPremBLSet = new LCPremBLSet();
            	tLCPremBLSet.add(tLCPremSchema);
            	tHMPrem.put(tLCPremSchema.getDutyCode(),tLCPremBLSet);
            }
            	
	
            mLCPremBLSet.set(j, tLCPremSchema);
        }
//        System.out.println("step2:"+(System.currentTimeMillis()-ttt));
        // 领取项
        for (int j = 1; j <= mLCGetBLSet.size(); j++)
        {
            LCGetSchema tLCGetSchema = mLCGetBLSet.get(j);
            tLCGetSchema.setPolNo(mLCPolBL.getPolNo());
            tLCGetSchema.setOperator(mGlobalInput.Operator);
            tLCGetSchema.setMakeDate(theCurrentDate);
            tLCGetSchema.setMakeTime(theCurrentTime);
            tLCGetSchema.setModifyDate(theCurrentDate);
            tLCGetSchema.setModifyTime(theCurrentTime);
            mLCGetBLSet.set(j, tLCGetSchema);
        }
//        System.out.println("step3:"+(System.currentTimeMillis()-ttt));
        double sumPremLCPol = 0; //保单中的实际保费和

        // 责任项
        for (int j = 1; j <= mLCDutyBLSet.size(); j++)
        {
            LCDutySchema tLCDutySchema = mLCDutyBLSet.get(j);

            double sumPremDuty = 0;
//            for (int m = 1; m <= mLCPremBLSet.size(); m++)
//            {
//                if (mLCPremBLSet.get(m).getDutyCode().equals(tLCDutySchema.getDutyCode()))
//                {
//                    //将该责任下的所有保费项累加
//                	sumPremDuty += mLCPremBLSet.get(m).getPrem();
//                }
//            }
            LCPremBLSet tLCPremBLSet = (LCPremBLSet)tHMPrem.get(tLCDutySchema.getDutyCode());
            if(tLCPremBLSet!=null)
            {
            	 for (int m = 1; m <= tLCPremBLSet.size(); m++) {
                  //将该责任下的所有保费项累加
            		 sumPremDuty += Double.parseDouble(PubFun.format(tLCPremBLSet.get(m).getPrem()));
                       
                   }
            }
            tLCDutySchema.setPrem(sumPremDuty);
            tLCDutySchema.setFirstPayDate(mLCPolBL.getCValiDate());
            tLCDutySchema.setModifyDate(theCurrentDate);
            tLCDutySchema.setModifyTime(theCurrentTime);
            if (tLCDutySchema.getCalRule() == null || tLCDutySchema.getCalRule().equals(""))
            {
                //设置默认的费率信息
                tLCDutySchema.setCalRule("0");
            }
            //统一费率 保存计划中录入的费率 不使用算出的费率
            if(tLCDutySchema.getCalRule().equals("1")){
            	//下面这种置法没有考虑到多责任,2个及2个以上都置成一样了.
            	//tLCDutySchema.setFloatRate(FloatRate);
            	tLCDutySchema.setFloatRate(FloatRate[j-1]);
            }
            if (tLCDutySchema.getPremToAmnt() == null || tLCDutySchema.getPremToAmnt().equals(""))
            {
                //设置算法方向
            	LMDutyDB tLMDutyDB = null;
                 if(this.isConCanUse)
                	 tLMDutyDB = new LMDutyDB(this.con);
                 else
                	 tLMDutyDB = new LMDutyDB();
                tLMDutyDB.setDutyCode(tLCDutySchema.getDutyCode());
                tLMDutyDB.getInfo();
                tLCDutySchema.setPremToAmnt(tLMDutyDB.getCalMode());
            }
            //chenwm20071207 复制被保险人的时候duty的polno为空,所以需要用lcpol中新生成的polno.
/*            if(tLCDutySchema.getPolNo()==null||tLCDutySchema.getPolNo()==""){
            	tLCDutySchema.setPolNo(mLCPolBL.getPolNo());
            }
*/            mLCDutyBLSet.set(j, tLCDutySchema);
            sumPremLCPol += sumPremDuty;
        }
//        System.out.println("step4:"+(System.currentTimeMillis()-ttt));
        mLCPolBL.setPrem(sumPremLCPol); //保存实际保费

        // 从责任表中取出第一条数据填充LCPol中的部分字段
        LCDutySchema tLCDutySchema1 = new LCDutySchema();
//        System.out.println("mLCDutyBLSet"+mLCDutyBLSet.get(1));
        tLCDutySchema1.setSchema((LCDutySchema) mLCDutyBLSet.get(1));
//        System.out.println("lcpol payintv:" + mLCPolBL.getPayIntv());
//        System.out.println("lcduty payintv:" + tLCDutySchema1.getPayIntv());

        if (mLCPolBL.getPayIntv() == 0)
        {
            mLCPolBL.setPayIntv(tLCDutySchema1.getPayIntv());
        }
        if (mLCPolBL.getInsuYear() == 0)
        {
            mLCPolBL.setInsuYear(tLCDutySchema1.getInsuYear());
        }
        if (mLCPolBL.getPayEndYear() == 0)
        {
            mLCPolBL.setPayEndYear(tLCDutySchema1.getPayEndYear());
        }
        if (mLCPolBL.getGetYear() == 0)
        {
            mLCPolBL.setGetYear(tLCDutySchema1.getGetYear());
        }
        if (mLCPolBL.getAcciYear() == 0)
        {
            mLCPolBL.setAcciYear(tLCDutySchema1.getAcciYear());
        }
        //chenwm0114 当pol中的flag为""不为null时有问题. 
        if ((mLCPolBL.getInsuYearFlag() == null||mLCPolBL.getInsuYearFlag().equals(""))
                && (tLCDutySchema1.getInsuYearFlag() != null))
        {
            mLCPolBL.setInsuYearFlag(tLCDutySchema1.getInsuYearFlag());
        }
        if ((mLCPolBL.getGetStartType() == null||mLCPolBL.getGetStartType().equals(""))
                && (tLCDutySchema1.getGetStartType() != null))
        {
            mLCPolBL.setGetStartType(tLCDutySchema1.getGetStartType());
        }
        if ((mLCPolBL.getGetYearFlag() == null||mLCPolBL.getGetYearFlag().equals(""))
                && (tLCDutySchema1.getGetYearFlag() != null))
        {
            mLCPolBL.setGetYearFlag(tLCDutySchema1.getGetYearFlag());
        }
        if ((mLCPolBL.getPayEndYearFlag() == null||mLCPolBL.getPayEndYearFlag().equals(""))
                && (tLCDutySchema1.getPayEndYearFlag() != null))
        {
            mLCPolBL.setPayEndYearFlag(tLCDutySchema1.getPayEndYearFlag());
        }
        if ((mLCPolBL.getAcciYearFlag() == null||mLCPolBL.getAcciYearFlag().equals(""))
                && (tLCDutySchema1.getAcciYearFlag() != null))
        {
            mLCPolBL.setAcciYearFlag(tLCDutySchema1.getAcciYearFlag());
        }
        return true;
    }

    /**
     * 校验连带被保险人年龄
     * @param mLCInsuredRelatedSet LCInsuredRelatedSet
     * @return boolean
     */
    private boolean checkSLCInsured(LCInsuredRelatedSet mLCInsuredRelatedSet)
    {
        int tInsuredAge = 0;
        LCInsuredRelatedBL tLCInsuredRelatedBL = new LCInsuredRelatedBL();
        for (int i = 1; i <= mLCInsuredRelatedSet.size(); i++)
        {
            tLCInsuredRelatedBL.setSchema(mLCInsuredRelatedSet.get(i));
            tInsuredAge = PubFun.calInterval(tLCInsuredRelatedBL.getBirthday()
                    , mLCPolBL.getCValiDate(), "Y");
            if ((tInsuredAge < mLMRiskAppSchema.getMinInsuredAge())
                    || ((tInsuredAge > mLMRiskAppSchema.getMaxInsuredAge())
                    && (mLMRiskAppSchema.getMaxInsuredAge() > 0)))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "dealDataPerson";
                tError.errorMessage = "连带被保人投保年龄不符合投保要求!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 银行出单特殊处理
     * @return boolean
     */
    private static boolean dealDataForBank()
    {
        /*Lis5.3 upgrade get
          if ((mLCPolBL.getStandbyFlag3() != null)
                    && (mLCPolBL.getStandbyFlag4() != null))
            {
                if (mLCPolBL.getStandbyFlag3().equals("Y"))
                {
                    if (mLCPolBL.getStandbyFlag4().trim().length() != 20)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "dealDataForBank";
                        tError.errorMessage = "银行出单标记为Y时必须录入长度为20位的银行出单保单号";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    else
                    {
                        LCPolDB tLCPolDB = new LCPolDB();
                        tLCPolDB.setStandbyFlag4(mLCPolBL.getStandbyFlag4());

                        LCPolSet tLCPolSet = new LCPolSet();
                        tLCPolSet = tLCPolDB.query();
                        if (tLCPolSet.size() > 0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "dealDataForBank";
                            tError.errorMessage = "录入的银行出单保单号已经存在,不能保存!";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                }

                if (mLCPolBL.getStandbyFlag3().equals("N")
                        && (mLCPolBL.getStandbyFlag4().trim().length() > 0))
                {
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataForBank";
                    tError.errorMessage = "银行出单标记为N时不能录入银行出单的保单号!";
                    this.mErrors.addOneError(tError);

                    return false;
                }

                //如果银行出单标记为真且银行出单保单号录入，在不打印保单
                if (mLCPolBL.getStandbyFlag3().equals("Y")
                        && (mLCPolBL.getStandbyFlag4().trim().length() > 0))
                {
                    mLCPolBL.setPrintCount(1);
                    mLCPolBL.setApproveFlag("9");
                    mLCPolBL.setApproveCode("000");
                    mLCPolBL.setApproveDate(theCurrentDate);
                }
            }
         */
        return true;
    }

    /**
     * 特殊处理，可屏蔽
     * @return boolean
     */
    private boolean preSpecCheck()
    {
        if (mOperate.equals("INSERT||PROPOSAL")
                || mOperate.equals("UPDATE||PROPOSAL"))
        {
            if ((mLCPolBL.getRiskCode() != null)
                    && mLCPolBL.getRiskCode().equals("241801"))
            {
                if (mLCPolBL.getStandbyFlag2() == null)
                {
                    mLCPolBL.setStandbyFlag2("0");
                }

                int InsuNum = Integer.parseInt(mLCPolBL.getStandbyFlag2());
                if ((InsuNum + 1) != mLCInsuredBLSet.size())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ProposalBL";
                    tError.functionName = "dealDataPerson";
                    tError.errorMessage = "连带被保人数目和填写在保单中的数目不一致!";
                    this.mErrors.addOneError(tError);

                    return false;
                }
            }
        }

        return true;
    }
    
  
	/**
	 * 保全计算保费方法 (前提:EdorValiDate>=GrpCvalidate)
	 * @param GrpCvalidate String 保单生效日期
	 * @param GrpPaytodate String 保单交至日期
	 * @param GrpPayenddate String 保单终交日期
	 * @param GrpPayintv int 保单交费间隔 0,1,3,6   
	 * @param EdorValiDate String 个人生效日期   
	 * @param Standprem double 个人标准保费      
	 * @return double[0]=prem,double[1]=sumprem
	 * @add by hm
	 */
	public double[] calSumPrem(String GrpCvalidate,String GrpPaytodate,String GrpPayenddate,int GrpPayintv,String EdorValiDate,double Standprem)
	{
		System.out.println("calSumPrem():GrpCvalidate="+GrpCvalidate+"  GrpPaytodate="+GrpPaytodate+"  GrpPayenddate="+GrpPayenddate+"  GrpPayintv="+GrpPayintv+"  EdorValiDate="+EdorValiDate+"  Standprem="+Standprem);
		double[] result = new double[2];
		double sumprem=0;
		double prem =0;
		FDate tD = new FDate();
		if(tD.getDate(GrpPaytodate).compareTo(tD.getDate(EdorValiDate)) <= 0 || tD.getDate(EdorValiDate).compareTo(tD.getDate(GrpCvalidate)) < 0) //1保单的交至日期小于等于个人生效日期 2个人生效日期小于保单的生效日期
		{
			sumprem=0;
			prem=0;
		}
		else
		{
			String tGrpCvalidate = GrpCvalidate;
			String tGrpEndDate = GrpPayenddate;
			String tEdorValiDate =EdorValiDate;
			int tGrpPayintv = GrpPayintv;
			String tGrpPaytodate =GrpPaytodate;
			if(tD.getDate(GrpPaytodate).compareTo(tD.getDate(GrpPayenddate))<0) //保单未交至末期
			{
				String tMonDate[] = new String[2];
				tMonDate = PubFun.getIntervalDate(tGrpCvalidate,tGrpEndDate,tEdorValiDate,tGrpPayintv);
				String Edv_PayToDate_B = tMonDate[0];  //保全生效日所在交费期间的起期
				String Edv_PayToDate_E = tMonDate[1];  //保全生效日所在交费期间的止期
				
				System.out.println("公式1 "+Edv_PayToDate_B+" "+Edv_PayToDate_E);
				
				//tEdorValiDate所属的标准交费期间之后至paytodate之前还有几期标准交费期间
				int afterNum = PubFun.getPayIntvNum(tGrpCvalidate,tGrpEndDate,tEdorValiDate,tGrpPayintv,tGrpPaytodate); 
				
				//代入计算公式 sumprem = (Edv_PayToDate_E - tEdorValiDate)/(Edv_PayToDate_E - Edv_PayToDate_B) * Standprem + afterNum * Standprem
				sumprem = Arith.round(
					      	Arith.add(
					      		Arith.mul(
					      			Arith.div( PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(Edv_PayToDate_E), "D"),
					      		    PubFun.calInterval(tD.getDate(Edv_PayToDate_B), tD.getDate(Edv_PayToDate_E), "D")),Standprem),
					      		Arith.mul(afterNum, Standprem)
				            ),
				          2);
				
				//计算prem
				if(afterNum==0)
				{
					//prem = (Edv_PayToDate_E - tEdorValiDate)/(Edv_PayToDate_E - Edv_PayToDate_B) * Standprem
					prem = Arith.round(
					      		Arith.mul(
					      			Arith.div( PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(Edv_PayToDate_E), "D"),
					      		    PubFun.calInterval(tD.getDate(Edv_PayToDate_B), tD.getDate(Edv_PayToDate_E), "D")),Standprem),
				          2);
				}
				else
				{
					//prem = Standprem
					prem = Arith.round(Standprem,2);
				}
			}
			else  //保单已交至末期
			{
				//判断末期是否为整期
				String tGrp_MonDate[] = new String[2];
				//函数getIntervalDate要求CValiDate<=EdorValiDate<EndDate.系统中时间是从0时算起,tGrpPaytodate有效时间是前一天的24时
				String tGrpPaytodate_1 = PubFun.calDate(GrpPaytodate,-1,"D",null); 
				tGrp_MonDate = PubFun.getIntervalDate(tGrpCvalidate,tGrpEndDate,tGrpPaytodate_1,tGrpPayintv);
				String GrpEnd_PayToDate_B = tGrp_MonDate[0];  //终交日所在交费期间的起期
				String GrpEnd_PayToDate_E = tGrp_MonDate[1];  //终交日所在交费期间的止期
				
				System.out.println("公式2 "+GrpEnd_PayToDate_B+" "+GrpEnd_PayToDate_E);
				
				if(tD.getDate(GrpEnd_PayToDate_E).compareTo(tD.getDate(GrpPayenddate))==0) //末期是整期(趸交走此分支)
				{
					//计算方式同上(未交至末期)相同
					String tMonDate[] = new String[2];
					tMonDate = PubFun.getIntervalDate(tGrpCvalidate,tGrpEndDate,tEdorValiDate,tGrpPayintv);
					String Edv_PayToDate_B = tMonDate[0];  //保全生效日所在交费期间的起期
					String Edv_PayToDate_E = tMonDate[1];  //保全生效日所在交费期间的止期
					
					System.out.println("公式2_1 "+Edv_PayToDate_B+" "+Edv_PayToDate_E);
					//tEdorValiDate所属的标准交费期间之后至paytodate之前还有几期标准交费期间
					int afterNum = PubFun.getPayIntvNum(tGrpCvalidate,tGrpEndDate,tEdorValiDate,tGrpPayintv,tGrpPaytodate); 
					
					//代入计算公式 sumprem = (Edv_PayToDate_E - tEdorValiDate)/(Edv_PayToDate_E - Edv_PayToDate_B) * Standprem + afterNum * Standprem
					sumprem = Arith.round(
						      	Arith.add(
						      		Arith.mul(
						      			Arith.div( PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(Edv_PayToDate_E), "D"),
						      		    PubFun.calInterval(tD.getDate(Edv_PayToDate_B), tD.getDate(Edv_PayToDate_E), "D")),Standprem),
						      		Arith.mul(afterNum, Standprem)
					            ),
					          2);
					
					//计算prem
					if(afterNum==0)
					{
						//prem = (Edv_PayToDate_E - tEdorValiDate)/(Edv_PayToDate_E - Edv_PayToDate_B) * Standprem
						prem = Arith.round(
						      		Arith.mul(
						      			Arith.div( PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(Edv_PayToDate_E), "D"),
						      		    PubFun.calInterval(tD.getDate(Edv_PayToDate_B), tD.getDate(Edv_PayToDate_E), "D")),Standprem),
					          2);
					}
					else
					{
						//prem = Standprem
						prem = Arith.round(Standprem,2);
					}
				}
				else  //末期不是整期,如11月半保单
				{
					//判断个人cvalidate当期的paytodate是否小于保单的paytodate
					String tMonDate[] = new String[2];
					tMonDate = PubFun.getIntervalDate(tGrpCvalidate,tGrpEndDate,tEdorValiDate,tGrpPayintv);
					String Edv_PayToDate_B = tMonDate[0];  //保全生效日所在交费期间的起期
					String Edv_PayToDate_E = tMonDate[1];  //保全生效日所在交费期间的止期
					
					System.out.println("公式2_2 "+Edv_PayToDate_B+" "+Edv_PayToDate_E);
					//tEdorValiDate所属的标准交费期间之后至paytodate之前还有几期标准交费期间
					int afterNum = PubFun.getPayIntvNum(tGrpCvalidate,tGrpEndDate,tEdorValiDate,tGrpPayintv,tGrpPaytodate); 
					if(tD.getDate(Edv_PayToDate_E).compareTo(tD.getDate(GrpPaytodate))<0) //EdorValiDate当前期不是最后一期
					{
						System.out.println("公式2_2_1 EdorValiDate不是末期");
						//代入计算公式 sumprem = (Edv_PayToDate_E - tEdorValiDate)/(Edv_PayToDate_E - Edv_PayToDate_B )* Standprem
						//                   + (afterNum - 1) * Standprem
						//                   + (tGrpPaytodate - GrpEnd_PayToDate_B)/(GrpEnd_PayToDate_E - GrpEnd_PayToDate_B)* Standprem
						sumprem = Arith.round(
									Arith.add(
										Arith.add(
											Arith.mul(
												Arith.div(PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(Edv_PayToDate_E), "D"),
													PubFun.calInterval(tD.getDate(Edv_PayToDate_B), tD.getDate(Edv_PayToDate_E), "D")),
												Standprem),
											Arith.mul((afterNum-1), Standprem)
										),
										Arith.mul(
											Arith.div(PubFun.calInterval(tD.getDate(GrpEnd_PayToDate_B), tD.getDate(tGrpPaytodate), "D"),
												PubFun.calInterval(tD.getDate(GrpEnd_PayToDate_B), tD.getDate(GrpEnd_PayToDate_E), "D")),
											Standprem)
									),
								2);
						//计算prem
						//prem = (tGrpPaytodate - GrpEnd_PayToDate_B)/(GrpEnd_PayToDate_E - GrpEnd_PayToDate_B)* Standprem
						prem = 	Arith.round(
									Arith.mul(
										Arith.div(PubFun.calInterval(tD.getDate(GrpEnd_PayToDate_B), tD.getDate(tGrpPaytodate), "D"),
											PubFun.calInterval(tD.getDate(GrpEnd_PayToDate_B), tD.getDate(GrpEnd_PayToDate_E), "D")),
									Standprem),
								2);
								 
					}
					else //EdorValiDate当前期是最后一期
					{
						System.out.println("公式2_2_1 EdorValiDate是末期");
						//代入计算公式 sumprem = (tGrpPaytodate - tEdorValiDate)/ (GrpEnd_PayToDate_E - GrpEnd_PayToDate_B) * Standprem
						sumprem = Arith.round(
									Arith.mul(
										Arith.div(PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(tGrpPaytodate), "D"),
											PubFun.calInterval(tD.getDate(GrpEnd_PayToDate_B), tD.getDate(GrpEnd_PayToDate_E), "D")),
										Standprem),
									2);
						//计算prem
						//prem = (tGrpPaytodate - tEdorValiDate)/ (GrpEnd_PayToDate_E - GrpEnd_PayToDate_B) * Standprem
						prem = Arith.round(
								Arith.mul(
									Arith.div(PubFun.calInterval(tD.getDate(tEdorValiDate), tD.getDate(tGrpPaytodate), "D"),
										PubFun.calInterval(tD.getDate(GrpEnd_PayToDate_B), tD.getDate(GrpEnd_PayToDate_E), "D")),
									Standprem),
							    2);
					}
				}
			}
		}
		result[0]=prem;
		result[1]=sumprem;
		return result;
	}
    public static void main(String[] args)
    {
        ProposalBL ProposalBL1 = new ProposalBL();

        //        LCPolSchema mLCPolSchema =new LCPolSchema();
        //        LCSpecSchema mLCSpecSchema=new LCSpecSchema();
        GlobalInput mGlobalInput = new GlobalInput();

        //        TransferData mTransferData = new TransferData();
        //        /** 被保人 */
        //        LCInsuredSet mLCInsuredSet = new LCInsuredSet();
        //        LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
        //        mLCInsuredSchema.setPolNo("11");
        //        mLCInsuredSchema.setName("hzm");
        //        mLCInsuredSchema.setSex("0");
        //        mLCInsuredSchema.setBirthday("1978-1-1");
        //        mLCInsuredSchema.setModifyDate("2002-1-1");
        //        mLCInsuredSchema.setModifyTime("10:10:10");
        //        mLCInsuredSet.add(mLCInsuredSchema);
        //        /** 投保人 */
        //        LCAppntSchema mLCAppntSchema = new LCAppntSchema();
        //        LCAppntGrpSchema mLCAppntGrpSchema = new LCAppntGrpSchema();
        //        /** 保险责任表 */
        //        LCDutySchema mLCDutySchema = new LCDutySchema();
        //        mLCDutySchema.setPolNo("11");
        //        mLCDutySchema.setDutyCode("001");
        //        mLCDutySchema.setMult("1");
        //        mLCDutySchema.setStandPrem(100);
        //        mLCDutySchema.setPrem(100);
        //        mLCDutySchema.setAmnt(100);
        //        mLCDutySchema.setFirstPayDate("2002-1-1");
        //        mLCDutySchema.setPaytoDate("2002-2-1");
        //        mLCDutySchema.setPayIntv(12);
        //
        //        mLCDutySchema.setGetYearFlag("Y");
        //        mLCDutySchema.setGetYear(1);
        //        mLCDutySchema.setInsuYearFlag("M");
        //        mLCDutySchema.setInsuYear(1);
        //        mLCDutySchema.setGetStartDate("1");
        //        mLCDutySchema.setPayEndYearFlag("Y");
        //        mLCDutySchema.setPayEndDate("2008-1-1");
        //        mLCDutySchema.setGetStartDate("2003-1-1");
        //
        //        mLCDutySchema.setModifyDate("2002-1-1");
        //        mLCDutySchema.setModifyTime("10:10:10");

        /** 全局变量 */
        mGlobalInput.Operator = "Admin";
        mGlobalInput.ComCode = "asd";
        mGlobalInput.ManageCom = "sdd";

        //        /** 传递变量 */
        //        mTransferData.setNameAndValue("getIntv","1");
        //        mTransferData.setNameAndValue("samePersonFlag","1");
        //        /** 客户告知 */
        //        LCCustomerImpartSet mLCCustomerImpartSet=new LCCustomerImpartSet();
        //        LCCustomerImpartSchema mLCCustomerImpartSchema=new LCCustomerImpartSchema();
        //        mLCCustomerImpartSchema.setPolNo("11");
        //        mLCCustomerImpartSchema.setImpartCode("11");
        //        mLCCustomerImpartSchema.setImpartVer("001") ;
        //        mLCCustomerImpartSchema.setCustomerNo("11");
        //        mLCCustomerImpartSchema.setCustomerNoType("001") ;
        //        mLCCustomerImpartSchema.setUWClaimFlg("1") ;
        //        mLCCustomerImpartSchema.setPrtFlag("2") ;
        //        mLCCustomerImpartSchema.setModifyDate("01/01/01") ;
        //        mLCCustomerImpartSchema.setModifyTime("01:01:01") ;
        //        mLCCustomerImpartSet.add(mLCCustomerImpartSchema) ;
        //        /** 个人保单表 */
        //        mLCPolSchema.setPolNo("86110020030110023595");
        //        mLCPolSchema.setProposalNo("86110020030110023595");
        //        mLCPolSchema.setGrpPolNo("");
        //        mLCPolSchema.setPrtNo("");
        //        mLCPolSchema.setPolNo("1111") ;
        //        mLCPolSchema.setAgentCode("01-01-01");
        //        mLCPolSchema.setManageCom("1012");
        //        mLCPolSchema.setPayLocation("1");
        //        mLCPolSchema.setRiskCode("211601");
        //        mLCSpecSchema.setMakeDate("01-01-01") ;
        //
        //        VData tv=new VData();
        //        tv.add(mLCPolSchema);
        //        tv.add(mLCSpecSchema);
        //        tv.add(mLCCustomerImpartSet);
        //        tv.add(mGlobalInput);
        //        tv.add(mTransferData);
        //        tv.add(mLCDutySchema);
        //        tv.add(mLCAppntSchema);
        //        tv.add(mLCAppntGrpSchema);
        //        tv.add(mLCInsuredSet);
        //ProposalBL1.submitData(tv,"INSERT||PROPOSAL");
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setProposalNo("86320020040110020172");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.addElement(tLCPolSchema);
        tVData.addElement(mGlobalInput);
        ProposalBL1.submitData(tVData, "DELETE||PROPOSAL");
    }
}
