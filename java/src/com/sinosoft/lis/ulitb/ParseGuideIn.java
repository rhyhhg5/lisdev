/*
 * @(#)ParseGuideIn.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.ulitb;

//import java.io.*;
//import java.util.*;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.FindReplaceInfo;
import com.f1j.util.F1Exception;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInsuredListDB;
import com.sinosoft.lis.db.LCInsuredListPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCGrpPositionDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LMDutyDB;
import com.sinosoft.lis.db.LMRiskDutyDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.schema.LCGrpIssuePolSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.schema.LCInsuredListPolSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.schema.LDRiskWrapSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.schema.LMRiskSchema;
import com.sinosoft.lis.vbl.LCBnfBLSet;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LCGrpIssuePolSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LCInsuredListPolSet;
import com.sinosoft.lis.vschema.LCInsuredRelatedSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCPremToAccSet;
import com.sinosoft.lis.vschema.LMRiskDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLPathTool;
import com.sinosoft.lis.pubfun.Arith;

/*
 * <p>Title: Web业务系统</p>
 * <p>ClassName:ParserGuidIn </p>
 * <p>Description: 磁盘投保入口类文件 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft </p>
 * @author：WUJS
 * @version：6.0
 * @CreateDate：2004-12-13
 */

public class ParseGuideIn
{
    //  @Fields
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public CErrors logErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /**内存文件暂存*/
    private org.jdom.Document myDocument;

    private org.jdom.Element myElement;

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LCGrpImportLogSet mLCGrpImportLogSet = new LCGrpImportLogSet();

    //团体自动下发问题件
    private LCGrpIssuePolSet mLCGrpIssuePolSet = new LCGrpIssuePolSet();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private String FileName;

    private String XmlFileName;

    private String mPreFilePath;

    private String mFlag;

    private String mGrpContNo;

    //数据Xml解析节点描述
    private String FilePath = "E:/temp/";

    private String ParseRootPath = "/DATASET/BATCHID";

    private String PATH_GRPCONTNO = "/DATASET/GRPCONTNO";

    private String ParseLCPolPath = "/DATASET/LCPOLTABLE/ROW";

    private String ParseInsuredPath = "/DATASET/INSUREDTABLE";

    private String ParseBnfPath = "/DATASET/BNFTABLE";

    private String parseInsuredRelaPath = "/DATASET/INSURED_RELA_TABLE";

    private String ParsePath = "/DATASET/CONTTABLE/ROW";

    //配置文件Xml节点描述
    private String ImportFileName;

    private String ConfigFileName;

    private String ConfigRootPath = "/CONFIG";

    private String mBatchNo = "";

    private String mPrtNo = "";

    private String mContNo = "";

    private String mainRiskCode = "";

    private LCPolBL mainPolBL = new LCPolBL();

    private org.w3c.dom.Document m_doc = null; // 临时的Document对象

    private GrpPolImpInfo m_GrpPolImpInfo = new GrpPolImpInfo();

    private String[] m_strDataFiles = null;

    private int TestFlag = 0;

    //保全传入参数
    private String mEdorType = null;

    private String mEdorNo = null;

    //保全计算结果
    private LCPolSet mEdorLCPolSet = new LCPolSet();

    private LCContSet mEdorLCContSet = new LCContSet();

    private MMap mMap = new MMap();

    private double InputPrem = 0.0;

    private double InputAmnt = 0.0;

    /** 领取频次，目前主要用于失能险。 */
    //private String mGrpGetIntv = "";
    private String mRiskWrapCode = null;

    /**
     * 定义保单类型,PolTypeFlag : 1 --无名单：
     *                          2 --（团单）公共帐户
     *                          C -- 理赔帐户
     *                          G -- 固定帐户
     * @author : Yangming
     * */
    private String PolTypeFlag = "";

    /**
     * 定义账户金额
     */
    private double PublicAcc = 0.0;

    /**
     * 计算方向
     */
    private String CalRule = "";

    //     @Constructors
    private String retire;

    private String avgage;

    private String InsuredNum;

    private String InsuredState;

    private String currentContPlan_retire;

    private String currentContPlan_avgage;

    private String currentContPlan_InsuredNum;

    /** 卡单标记 */
    private String mCardFalg;

    private double sumSub = 0.0;

    /** 套餐险种标记 */
    private HashMap mRiskWrapCache = new HashMap();

    //    private HashMap riskCached;
    
    //by gzh 
    private ExeSQL mExeSQL = new ExeSQL();// 用于SQL查询的
	private String getYear=""; // 从告示中获取保险金领取年龄
	private String getDutyKind=""; // 从告示中获取保险金领取年龄
//	private HashMap dutyMap = new HashMap();//用于保存Lcinsuredlist新增字段传值

    public ParseGuideIn()
    {
        //bulidDocument();
    }

    public ParseGuideIn(String fileName)
    {
        bulidDocument();
        FileName = fileName;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        boolean re = true;
        this.getInputData();

        if (!cOperate.equals("INSERT||DATABASE"))
        {
            if (!this.checkData())
            {
                return false;
            }
            System.out.println("开始时间:" + PubFun.getCurrentTime());
            try
            {
                if (!this.parseVts())
                {
                    return false;
                }

                for (int nIndex = 0; nIndex < m_strDataFiles.length; nIndex++)
                {
                    XmlFileName = m_strDataFiles[nIndex];
                    boolean res = this.ParseXml();

                    if (!res)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ParseGuideIn";
                        tError.functionName = "checkData";
                        tError.errorMessage = this.mErrors.getFirstError();
                        this.logErrors.addOneError(tError);
                        continue;
                    }

                }

                //            mLCGrpImportLogSet = m_GrpPolImpInfo.getErrors();
                //
                //            //错误日志
                //            if (mLCGrpImportLogSet.size() > 0)
                //            {
                //                String tReturn = mLCGrpImportLogSet.encode();
                //                tReturn = "0|" + mLCGrpImportLogSet.size() + "^" + tReturn;
                //                mResult.clear();
                //                mResult.addElement(tReturn);
                //            }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "导入文件格式有误!";
                this.logErrors.addOneError(tError);
                //  return false;
            }
        }
        /**
         * 目前磁盘导入分为两步：
         * 1、上传将Xml文件解析完，数据直接导入表中
         * 2、如果内存中还存在被保人清单信息则取出被保险人信息
         *    如果没有则从数据库查询出信息
         */
        else
        {
            VData mInputData = new VData();
            LCInsuredListSet tLCInsuredListSet = (LCInsuredListSet) mInputData
                    .getObjectByObjectName("LCInsuredListSet", 0);
            LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
            String strSql = "";
            if (tLCInsuredListSet == null && StrTool.cTrim(mEdorNo).equals(""))
            {
                //                tLCInsuredListDB.setGrpContNo(mGrpContNo);
                //                tLCInsuredListDB.setState("0");
                strSql = "select * from LCInsuredList where GrpContNo='"
                        + mGrpContNo + "' and State='0' with ur ";

                // 广东被保人人数规则校验
//                if (!chkInsuNums(mGrpContNo))
//                {
//                    buildError("checkData", "被保人数少于6人，请核实。");
//                    return false;
//                }
                // --------------------

                //                tLCInsuredListDB.prepareData(strSql);
            }
            if (tLCInsuredListSet == null && !StrTool.cTrim(mEdorNo).equals(""))
            {
                //                tLCInsuredListDB.setGrpContNo(mGrpContNo);
                //                tLCInsuredListDB.setEdorNo(mEdorNo);
                //                tLCInsuredListDB.setState("0"); ;
                String tContNo = (String) mTransferData
                        .getValueByName("ContNo");
                String whereContNo = "";
                if (null != tContNo && !"".equals(tContNo)
                        && !"null".equals(tContNo))
                {
                    whereContNo = " and contno = '" + tContNo + "' ";
                }
                strSql = "select * from LCInsuredList where GrpContNo='"
                        + mGrpContNo + "' and State='0' and EdorNo='" + mEdorNo
                        + "' " + whereContNo + " with ur ";
                //                tLCInsuredListDB.prepareData(strSql);
            }
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(mGrpContNo);
            if (!tLCGrpContDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = this.mErrors.getFirstError();
                this.logErrors.addOneError(tError);
            }
            this.mLCGrpContSchema = tLCGrpContDB.getSchema();
            //            if (tLCInsuredListDB.hasMoreData()) {
            //                do {
            //                    tLCInsuredListSet = tLCInsuredListDB.getData();
            //                    if (tLCInsuredListSet != null) {
            //                        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
            //                            LCInsuredListSchema tLCInsuredListSchema =
            //                                    tLCInsuredListSet.get(i);
            //                            if (tLCInsuredListSchema == null) {
            //                                CError tError = new CError();
            //                                tError.moduleName = "ParseGuideIn";
            //                                tError.functionName = "checkData";
            //                                tError.errorMessage = this.mErrors.
            //                                        getFirstError();
            //                                this.logErrors.addOneError(tError);
            //                            }
            //                            boolean bl = DiskContImport(tLCInsuredListSchema,
            //                                    tLCInsuredListSchema.getContNo());
            //                        }
            //                    }
            //                } while (tLCInsuredListSet != null &&
            //                         tLCInsuredListSet.size() > 0);
            //            }
            /** 新增方法:
             * 游标查询方法 */
            RSWrapper rswrapper = new RSWrapper();
            tLCInsuredListSet = new LCInsuredListSet();
            rswrapper.prepareData(tLCInsuredListSet, strSql);
            try
            {
                do
                {
                    rswrapper.getData();
                    for (int i = 1; i <= tLCInsuredListSet.size(); i++)
                    {
                        LCInsuredListSchema tLCInsuredListSchema = tLCInsuredListSet
                                .get(i);
                        if (tLCInsuredListSchema == null)
                        {
                            CError tError = new CError();
                            tError.moduleName = "ParseGuideIn";
                            tError.functionName = "checkData";
                            tError.errorMessage = this.mErrors.getFirstError();
                            this.logErrors.addOneError(tError);
                        }
                        /** 原来的程序生成家庭关系存在问题,目前改进,但影响效率 */
                        LCInsuredListSet oneCont = new LCInsuredListSet();
                        if ((tLCInsuredListSchema.getRelation() == null || tLCInsuredListSchema
                                .getRelation().equals("00")))
                        {

                            if (tLCInsuredListSchema.getInsuredID().equals("C")
                                    || tLCInsuredListSchema.getInsuredID()
                                            .equals("G"))
                            {
                                oneCont.add(tLCInsuredListSchema);
                            }
                            else
                            {
                                tLCInsuredListDB.setGrpContNo(this.mGrpContNo);
                                tLCInsuredListDB.setContNo(tLCInsuredListSchema
                                        .getContNo());
                                tLCInsuredListDB.setEdorNo(tLCInsuredListSchema
                                        .getEdorNo());
                                String sql;
                                if (StrTool.cTrim(mEdorNo).equals(""))
                                {
                                    sql = "select * from lcinsuredlist where grpcontno='"
                                            + this.mGrpContNo
                                            + "' and contno='"
                                            + tLCInsuredListSchema.getContNo()
                                            + "' and insuredid not in ('C','G') order by Relation";
                                }
                                else
                                {
                                    sql = "select * from lcinsuredlist where grpcontno='"
                                            + this.mGrpContNo
                                            + "' and contno='"
                                            + tLCInsuredListSchema.getContNo()
                                            + "' and edorno = '"
                                            + tLCInsuredListSchema.getEdorNo()
                                            + "' and insuredid not in ('C','G') order by Relation";
                                }
                                oneCont = tLCInsuredListDB.executeQuery(sql);
                            }

                            boolean bl = DiskContImport(oneCont,
                                    tLCInsuredListSchema.getContNo());

                        }
                    }
                }
                while (tLCInsuredListSet.size() > 0);
                rswrapper.close();
                if ((mEdorNo == null) || (mEdorNo.equals("")))
                {
                    dealPremError();
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                rswrapper.close();
            }
        }
        this.mErrors = this.logErrors;
        System.out.println("结束时间:" + PubFun.getCurrentTime());
        if (this.mErrors.getErrorCount() > 0)
        {
            return false;
        }
        /** 处理总人数问题 */
        if (!dealSumPeople())
        {
            return false;
        }
        return true;
    }

    /**
     * dealSumPeople
     *
     * @return boolean
     */
    private boolean dealSumPeople()
    {

        /**
         * 保全增人和公共账户都不记为被保险人数
         * @author:yangming
         */
        if (StrTool.cTrim(mEdorType).equals("")
                && !StrTool.cTrim(this.mFlag).equals("N"))
        {
            MMap tMap = new MMap();
            StringBuffer sql_LCGrpPol = new StringBuffer();
            sql_LCGrpPol.append("select a,sum(b) from ( ");
            sql_LCGrpPol.append("SELECT GRPPOLNO a,INSUREDPEOPLES b ");
            sql_LCGrpPol.append(" FROM LCPOL WHERE ");
            sql_LCGrpPol.append("GRPCONTNO='");
            sql_LCGrpPol.append(this.mGrpContNo);
            sql_LCGrpPol.append("' AND POLTYPEFLAG IN ('0','1')");
            sql_LCGrpPol.append(" union all ");
            sql_LCGrpPol.append(" SELECT GRPPOLNO a,0 b ");
            sql_LCGrpPol.append(" FROM LCPOL WHERE  ");
            sql_LCGrpPol.append("GRPCONTNO='");
            sql_LCGrpPol.append(this.mGrpContNo);
            sql_LCGrpPol.append("' AND POLTYPEFLAG IN ('2')");
            sql_LCGrpPol.append(" ) as x ");
            sql_LCGrpPol.append(" GROUP BY A ");

            SSRS ssrs = (new ExeSQL()).execSQL(sql_LCGrpPol.toString());
            System.out.println("查询人数错误：" + sql_LCGrpPol.toString());
            for (int i = 1; i <= ssrs.getMaxRow(); i++)
            {
                String tGrpPolNo = ssrs.GetText(i, 1);
                String tSumPeople = ssrs.GetText(i, 2);
                StringBuffer update_LCGrpPol = new StringBuffer();
                update_LCGrpPol.append("UPDATE LCGRPPOL SET PEOPLES2=");
                update_LCGrpPol.append(tSumPeople);
                update_LCGrpPol
                        .append(",PREM=(SELECT SUM(PREM) FROM LCPOL WHERE GRPPOLNO=LCGRPPOL.GRPPOLNO)");
                update_LCGrpPol.append(" WHERE GRPPOLNO ='");
                update_LCGrpPol.append(tGrpPolNo);
                update_LCGrpPol.append("'");
                tMap.put(update_LCGrpPol.toString(), "UPDATE");
            }

            StringBuffer sql_LCGrpCont = new StringBuffer();
            sql_LCGrpCont.append("SELECT nvl(SUM(PEOPLES), 0) ");
            sql_LCGrpCont.append(" FROM LCCONT WHERE ");
            sql_LCGrpCont.append(" GRPCONTNO='");
            sql_LCGrpCont.append(this.mGrpContNo);
            sql_LCGrpCont.append("' AND POLTYPE IN ('0','1')");
            String sum_People = (new ExeSQL()).getOneValue(sql_LCGrpCont
                    .toString());

            StringBuffer update_LCGrpCont = new StringBuffer();
            update_LCGrpCont.append("UPDATE LCGRPCONT SET PEOPLES2=");
            update_LCGrpCont.append(sum_People);
            update_LCGrpCont
                    .append(",PREM=(SELECT SUM(PREM) FROM LCCONT WHERE GRPCONTNO=LCGRPCONT.GRPCONTNO)");
            update_LCGrpCont.append(" WHERE GRPCONTNO='");
            update_LCGrpCont.append(this.mGrpContNo);
            update_LCGrpCont.append("'");
            tMap.put(update_LCGrpCont.toString(), "UPDATE");
            tMap.add(m_GrpPolImpInfo.logSucc(mBatchNo, mGrpContNo, "0", mPrtNo,
                    null, mGlobalInput));
            VData tVData = new VData();
            tVData.add(tMap);
            PubSubmit ps = new PubSubmit();
            //        logMap =

            if (!ps.submitData(tVData, null))
            {
                this.mErrors.copyAllErrors(ps.mErrors);
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo, "0",
                        "0", "0", null, null, null, ps.mErrors,
                        this.mGlobalInput);
            }
        }
        return true;
    }

    /**
     * 返回保全磁盘增人所需的计算结果
     * @return LCPolSet
     */
    public LCPolSet getEdorNiResult()
    {
        return mEdorLCPolSet;
    }

    /**
     * 返回保全磁盘增人所需的计算结果
     * @return LCPolSet
     */
    public LCContSet getEdorNiContResult()
    {
        return mEdorLCContSet;
    }

    /**
     * 得到传入数据
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        mPreFilePath = (String) mTransferData.getValueByName("FilePath");
        this.FileName = (String) mTransferData.getValueByName("FileName");
        System.out.println("mPreFilePath : " + mPreFilePath);
        mFlag = (String) mTransferData.getValueByName("Flag");
        if (mTransferData != null)
        {
            this.mEdorType = (String) mTransferData.getValueByName("EdorType");
        }
        this.mEdorNo = (String) mTransferData.getValueByName("EdorNo");
        this.mGrpContNo = (String) this.mTransferData
                .getValueByName("GrpContNo");
        this.mBatchNo = (String) this.mTransferData.getValueByName("FileName");
        this.mCardFalg = (String) this.mTransferData.getValueByName("CardFlag");
        if (mBatchNo == null)
        {
            mBatchNo = PubFun1.CreateMaxNo("BATCHNO", 10);
        }
        //        mBatchNo = mBatchNo.substring(0, mBatchNo.indexOf("."));
    }

    /**
     * 校验传输数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无操作员信息，请重新登录!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无导入文件信息，请重新导入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FileName = (String) mTransferData.getValueByName("FileName");
        }
        return true;
    }

    /**
     * 得到生成文件路径
     *
     * @return boolean
     */
    private boolean getFilePath()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("XmlPath");
        if (!tLDSysVarDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "缺少文件导入路径!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FilePath = tLDSysVarDB.getSysVarValue();
        }

        return true;
    }

    /**
     * 检验文件是否存在
     *
     * @return boolean
     */
    private boolean checkXmlFileName()
    {
        //    XmlFileName = (String)mTransferData.getValueByName("FileName");
        File tFile = new File(XmlFileName);
        if (!tFile.exists())
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("XmlPath");
            if (!tLDSysVarDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "缺少文件导入路径!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                FilePath = tLDSysVarDB.getSysVarValue();
            }

            File tFile1 = new File(FilePath);
            if (!tFile1.exists())
            {
                tFile1.mkdirs();
            }
            XmlFileName = FilePath + XmlFileName;
            File tFile2 = new File(XmlFileName);
            if (!tFile2.exists())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "请上传相应的数据文件到指定路径" + FilePath + "!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 检查导入配置文件是否存在
     *
     * @return boolean
     */
    private boolean checkImportConfig()
    {
        this.getFilePath();
        //FilePath="E:/temp/";

        String filePath = mPreFilePath + FilePath;
        System.out.println(filePath);
        File tFile1 = new File(filePath);
        if (!tFile1.exists())
        {
            tFile1.mkdirs();
        }

        ConfigFileName = filePath + "Import.xml";
        File tFile2 = new File(ConfigFileName);
        if (!tFile2.exists())
        {
            System.out.println("检查导入配置文件是否存在");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 初始化上传文件
     *
     * @return boolean
     */
    private boolean initImportFile()
    {
        this.getFilePath();
        ImportFileName = mPreFilePath + FilePath + FileName;
        //ImportFileName = "E:/temp/" + FileName;
        System.out.println(ImportFileName);
        File tFile = new File(ImportFileName);
        if (!tFile.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "未上传文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----导入文件");
        return true;
    }

    /**
     * 解析excel并转换成xml文件
     *
     * @return boolean
     * @throws Exception
     */
    private boolean parseVts() throws Exception
    {
        //初始化导入文件
        if (!this.initImportFile())
        {
            return false;
        }
        if (!this.checkImportConfig())
        {
            return false;
        }

        GrpPolVTSParser gpvp = new GrpPolVTSParser();

        if (!gpvp.setFileName(ImportFileName))
        {
            mErrors.copyAllErrors(gpvp.mErrors);
            return false;
        }
        if (!gpvp.setConfigFileName(ConfigFileName))
        {
            mErrors.copyAllErrors(gpvp.mErrors);
            return false;
        }
        //转换excel到xml
        if (!gpvp.transform())
        {
            mErrors.copyAllErrors(gpvp.mErrors);
            return false;
        }

        // 得到生成的XML数据文件名
        m_strDataFiles = gpvp.getDataFiles();

        return true;
    }

    /**
     * 提交第一层内的循环体到第一层循环内的指定结点下
     * ???目前不知道有啥用 -wujs
     * @param aVT Vector
     * @param aElt Element
     */
    public void AddMuliElement(Vector aVT, org.jdom.Element aElt)
    {
        int NodeCount = aVT.size();
        //org.jdom.Element tElt1 = (org.jdom.Element)myElement.clone();
        org.jdom.Element tElt = myElement;
        int i = 0;
        while (NodeCount > 0)
        {
            i++;
            String NodeName = (String) aVT.get(i - 1);
            //      tElt = tElt.getChild((String)aVT.get(i-1));
            tElt = (org.jdom.Element) tElt.getChildren((String) aVT.get(i - 1))
                    .get(tElt.getChildren((String) aVT.get(i - 1)).size() - 1);
            //myElement.addContent(t.Elt);
            NodeCount--;
        }
        tElt.addContent(aElt);
        //myDocument = new org.jdom.Document(tElt1);
    }

    /**
     *
     * @return boolean
     * @param tBatchNo String
     * @param tPrtNo String
     */
    private boolean checkGrpCont(String tBatchNo, String tPrtNo)
    {
        LCGrpContDB grpContDB = new LCGrpContDB();
        grpContDB.setPrtNo(tPrtNo);
        LCGrpContSet tLCGrpContSet = grpContDB.query();
        if (tLCGrpContSet == null || tLCGrpContSet.size() <= 0)
        {
            CError.buildErr(this, "没有找到团体合同[" + tPrtNo + "]的信息");
            m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(), "",
                    "", "", "", "", null, this.mErrors, mGlobalInput);
            this.mErrors.clearErrors();
            return false;
        }
        else
        {
            this.mLCGrpContSchema.setSchema(tLCGrpContSet.get(1).getSchema());
        }

        /**
         * Yangming：为提高报错信息准确性将限制一批次号只能导入一次不能重复导入。
         */
        //       LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
        //       LCGrpImportLogSet tLCGrpImportLogSet = new LCGrpImportLogSet();
        //       tLCGrpImportLogDB.setBatchNo(mBatchNo);
        //       tLCGrpImportLogDB.setGrpContNo(mGrpContNo);
        //       tLCGrpImportLogSet = tLCGrpImportLogDB.query();
        //       if(tLCGrpImportLogSet.size()>0)
        //       {
        //           CError.buildErr(this, "批次号[" + mBatchNo + "]不能重复导入！");
        //           m_GrpPolImpInfo.logError(mBatchNo, mGrpContNo, "1", "",
        //                                    "", "", "", null, this.mErrors,
        //                                    mGlobalInput);
        //           this.logErrors.addOneError(mErrors.getFirstError());
        //           this.mErrors.clearErrors();
        //           return false;
        //       }
        /**
         * YangMing:
         * 添加对保险生效日期与实效日期的校验,
         * picch的保险规则是保险期间必须为整
         * 月数,且有生效日期当天0时到实效日期
         * 24时结束.
         * 此校验以保证保费计算时日期的正确性
         */
        if (!checkValiDate(mLCGrpContSchema.getCValiDate(), mLCGrpContSchema
                .getCInValiDate()))
        {
            CError.buildErr(this, "团体合同[" + mGrpContNo + "]保险生效日期为"
                    + mLCGrpContSchema.getCValiDate() + ",失效日期为"
                    + mLCGrpContSchema.getCInValiDate() + ",不是整月,请检查！");
            m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(), "1",
                    "", "", "", "", null, this.mErrors, mGlobalInput);
            GrpAutoQuest(mLCGrpContSchema, this.mErrors, "1", "401", "团体合同",
                    "CValiDate", "生效日期", mLCGrpContSchema.getCValiDate());
            this.logErrors.addOneError(mErrors.getFirstError());
            this.mErrors.clearErrors();
            return false;
        }

        if ("1".equals(StrTool.cTrim(mLCGrpContSchema.getAppFlag())))
        {
            CError.buildErr(this, "团体合同[" + mGrpContNo + "]已经签单，不再接受投保");
            m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(), "",
                    "", "", "", "", null, this.mErrors, mGlobalInput);
            this.mErrors.clearErrors();

            return false;
        }
        //团单合同的录单完成日期不为空则表示已经录入完成
        if (!"".equals(StrTool.cTrim(mLCGrpContSchema.getInputDate())))
        {
            CError.buildErr(this, "团体合同[" + mGrpContNo + "]已录入完成，不再接受投保");
            m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(), "",
                    "", "", "", "", null, this.mErrors, mGlobalInput);
            this.mErrors.clearErrors();
            return false;
        }
        return true;
    }

    /**
     * 解析xml,
     *
     * @return boolean
     */
    private boolean ParseXml()
    {
        // System.out.println("-----开始读取Xml文件");
        if (!checkXmlFileName())
        {
            return false;
        }
        File tFile = new File(XmlFileName);
        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        this.mErrors.clearErrors();

        String tBatchNo = "";
        String tPrtNo = "";
        try
        {
            //批次号
            tBatchNo = tXPT.parseX(ParseRootPath).getFirstChild()
                    .getNodeValue();
            //印刷号
            tPrtNo = tXPT.parseX(PATH_GRPCONTNO).getLastChild().getNodeValue();
            mBatchNo = tBatchNo;
            System.out.println("批次号为   : " + mBatchNo);
            mPrtNo = tPrtNo;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        //根据flag 调用不同的check
        if ((mFlag != null) && (mFlag.equals("BQ")))
        {
        }
        else
        {
            checkGrpCont(tBatchNo, tPrtNo);
        }

        LCGrpContDB grpContDB = new LCGrpContDB();
        grpContDB.setPrtNo(tPrtNo);
        LCGrpContSet tLCGrpContSet = grpContDB.query();
        mLCGrpContSchema = tLCGrpContSet.get(1);
        mGrpContNo = mLCGrpContSchema.getGrpContNo();

        /*******************************/
        //得到保单的传入信息
        NodeList nodeList = tXPT.parseN(ParsePath);
        int nNodeCount = 0;

        if (nodeList != null)
        {
            nNodeCount = nodeList.getLength();
        }

        //循环中，每一次都调用一次承包后台程序，首先挑出一条保单纪录，再通过ID号找到保费项，责任，给付等，组合成Set集合
        //此时新单是没有投保单号的，因此在保单，保费项，责任等纪录中投保单号为空,后台自动生成投保单号
        //最后将数据放入VData中，传给GrpPollmport模块。一次循环完毕
        GrpPolParser tGrpPolParser = new GrpPolParser();

        for (int i = 0; i < nNodeCount; i++)
        {
            boolean tSucc = true;
            Node node = nodeList.item(i);

            try
            {
                node = transformNode(node);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                node = null;
            }

            NodeList contNodeList = node.getChildNodes();
            if (contNodeList.getLength() <= 0)
            {
                continue;
            }
            Node contNode = null;
            String nodeName = "";
            LCInsuredRelatedSet tLCInsuredRelatedSet = null;
            LCInsuredSet tLCInsuredSet = null;
            LCBnfSet tLCBnfSet = null;
            String contId = "";
            for (int j = 0; j < contNodeList.getLength(); j++)
            {

                contNode = contNodeList.item(j);
                nodeName = contNode.getNodeName();
                if (nodeName.equals("#text"))
                {
                    continue;
                }
                if (nodeName.equals("CONTID"))
                {
                    contId = contNode.getFirstChild().getNodeValue();
                    continue;
                }

                if (nodeName.equals("INSUREDTABLE"))
                {
                    //解析被保险人xml
                    tLCInsuredSet = (LCInsuredSet) tGrpPolParser
                            .getLCInsuredSet(contNode);

                    //将客户身份证号码中的x转换成大写（被保险人） 2009-02-15 liuyp
                    if (tLCInsuredSet != null && tLCInsuredSet.size() != 0)
                    {
                        for (int k = 1; k <= tLCInsuredSet.size(); k++)
                        {
                            if (tLCInsuredSet.get(k).getIDType() != null
                                    && tLCInsuredSet.get(k).getIDNo() != null)
                            {
                                if (tLCInsuredSet.get(k).getIDType()
                                        .equals("0"))
                                {
                                    String tLCInsuredIdNo = tLCInsuredSet
                                            .get(k).getIDNo().toUpperCase();
                                    tLCInsuredSet.get(k)
                                            .setIDNo(tLCInsuredIdNo);
                                }
                            }
                        }

                    }

                    continue;
                }
                if (nodeName.equals("LCPOLTABLE"))
                {
                    //保单信息
                    VData tVData = null;

                    // 解析一个保单节点
                    tVData = tGrpPolParser.parseLCPolNode(contNode);
                    //                if ( tVData==null  || tVData.size()<=0)
                    //                {
                    //                    this.mErrors.copyAllErrors(tGrpPolParser.mErrors);
                    //                    return false;
                    //                }

                    //                TransferData tTransferData =
                    //                        (TransferData) tVData.getObjectByObjectName(
                    //                        "TransferData",
                    //                        0);
                    //                String ContId = (String) tTransferData.getValueByName(
                    //                        "ContId");
                    //缓存险种保单信息
                    m_GrpPolImpInfo.addContPolData(contId, tVData);
                    continue;
                }
                if (nodeName.equals("BNFTABLE"))
                {
                    //受益人解析
                    tLCBnfSet = (LCBnfSet) tGrpPolParser.getLCBnfSet(contNode);
                    continue;
                }
                if (nodeName.equals("INSURED_RELA_TABLE"))
                {
                    //连身被保人表
                    tLCInsuredRelatedSet = (LCInsuredRelatedSet) tGrpPolParser
                            .getLCInsuredRelatedSet(contNode);
                    continue;
                }
            }

            if (tLCInsuredSet == null)
            {
                CError.buildErr(this, "被保险人节点解析失败,被保险人不能为空!");
                m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(),
                        "", "", "", "", "", null, this.mErrors, mGlobalInput);
                this.mErrors.clearErrors();
                return false;
            }

            if (!m_GrpPolImpInfo.init(tBatchNo, this.mGlobalInput,
                    tLCInsuredSet, tLCBnfSet, tLCInsuredRelatedSet,
                    mLCGrpContSchema))
            {

                m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(),
                        "", "", "", "", "", null, this.mErrors, mGlobalInput);
                this.mErrors.clearErrors();
                return false;
            }

            //按合同导入
            boolean bl = DiskContImport(contId);

            if (!bl)
            {
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = mErrors.getFirstError();
                this.logErrors.addOneError(tError);
                System.out.println(logErrors.getErrorCount());
                System.out.println("导入失败");

                continue;
            }
            else
            {
                System.out.println("导入成功");
                TestFlag = TestFlag + 1;
            }
            //杨明添加用于校验每个人的保费误差
            System.out.println("有这么多这个合同导入成功" + TestFlag);
            System.out.println("有这么多这个磁盘投保文件" + nNodeCount);

            if (nNodeCount == TestFlag)
            {
                LCPolDB mLCPolDB = new LCPolDB();
                LCPolSet mLCPolSet = new LCPolSet();
                mLCPolDB.setContNo(mContNo);

                mLCPolSet = mLCPolDB.query();
                if (mLCPolSet.size() == 0)
                {
                    CError.buildErr(this, "查询保单信息失败！原因是在磁盘投保失败，为生成保单。");
                    return false;
                }
                ExeSQL mExeSQL = new ExeSQL();
                //校验保险计划中的总人数与磁盘投保文件中的被保险人数必须相同否则算钱会出现误差
                String ChkPeoplesSql = "select sum(peoples3) from lccontplan where grpcontno='"
                        + mGrpContNo + "'";
                ExeSQL ChkPeoplesExe = new ExeSQL();
                SSRS ChkPeoplesSSRS = ChkPeoplesExe.execSQL(ChkPeoplesSql);
                String SumPeoples = "";
                if (ChkPeoplesSSRS != null)
                {
                    SumPeoples = ChkPeoplesSSRS.GetText(1, 1);
                }

                //校验被保险人数与磁盘投保文件个数相同
                String strSQL = "select count(insuredno) from lcinsured where grpcontno='"
                        + mLCPolSet.get(1).getGrpContNo() + "'";
                SSRS CheckPeoples = mExeSQL.execSQL(strSQL);
                if (CheckPeoples == null)
                {
                    CError.buildErr(this, "团体合同下没有被保险人,或磁盘投保失败！");
                    return false;
                }
                String Chk = CheckPeoples.GetText(1, 1);
                //(String)TestFlag
                System.out.println("计划中的被保险人数!"
                        + Integer.valueOf(SumPeoples).intValue());
                System.out.println("被保险人数!" + Integer.valueOf(Chk).intValue());
                if (Chk.equals(SumPeoples))
                {
                    //此句SQL语句用于查询出在保险计划中录入的各险种的总保费。如果是标定费率的则无总保费
                    String sql = "select a.grpcontno,b.riskprem,a.peoples3,b.riskcode,a.contplancode "
                            + "from lccontplan a,lccontplanrisk b where"
                            + " a.grpcontno='"
                            + mLCPolSet.get(1).getGrpContNo()
                            + "' and a.contplancode=b.contplancode "
                            + "  and riskprem <>0"
                            + " and a.grpcontno=b.grpcontno";
                    System.out.println("用于查询出在保险计划中录入的各险种的总保费。如果是表定费率的则无总保费:"
                            + sql);
                    //                String RiskPrem[] = null; //用于存放查询处的总保费
                    //                String RiskCode[] = null; //用于存放查询出的保险计划中的险种代码。
                    String GrpContNo = "";
                    //                String PeoPles3[] = null; //用于存放总人数
                    //                String ContPlanCode[] = null; //用于存放保险计划代码；
                    String arr[][] = null;
                    String sum = "";
                    String beforePrem = "";
                    String tRiskPrem = "";
                    VData tempVata = new VData();
                    MMap map = new MMap();

                    SSRS ssrs = mExeSQL.execSQL(sql);
                    System.out.println(ssrs);
                    if (ssrs != null)
                    {
                        //将查询出的结果放入数组中
                        System.out.println(arr);
                        arr = ssrs.getAllData();
                    }
                    if (arr != null)
                    {
                        for (int t = 0; t < arr.length; t++)
                        {
                            GrpContNo = arr[t][0];
                            System.out.println(GrpContNo);
                            String RiskPrem = arr[t][1];
                            System.out.println(RiskPrem);
                            String PeoPles3 = arr[t][2];
                            System.out.println(PeoPles3);
                            String RiskCode = arr[t][3];
                            System.out.println(RiskCode);
                            String ContPlanCode = arr[t][4];
                            System.out.println(ContPlanCode);
                            String sql2 = "select sum(prem) from lcpol where "
                                    + " GrpContNo='" + GrpContNo
                                    + "' and insuredno in "
                                    + "(select insuredno from "
                                    + " lcinsured where grpcontno='"
                                    + GrpContNo + "' and contplancode='"
                                    + ContPlanCode + "') " + "and riskcode='"
                                    + RiskCode + "'";
                            String sql5 = "select riskprem from lccontplanrisk where "
                                    + " riskcode ='"
                                    + RiskCode
                                    + "' and contplancode='"
                                    + ContPlanCode
                                    + "' and grpcontno ='" + GrpContNo + "'";

                            System.out.println("用于查询lcpol下的总保费 : " + sql2);
                            SSRS ssrs2 = mExeSQL.execSQL(sql2);

                            if (ssrs2 != null)
                            {
                                sum = ssrs2.GetText(1, 1);
                                System.out.println("查询出lcpol的总保费为 ： " + sum);
                            }
                            SSRS ssrs5 = mExeSQL.execSQL(sql5);
                            if (ssrs5 != null)
                            {
                                tRiskPrem = ssrs5.GetText(1, 1);
                                System.out.println("录入总保费 : " + tRiskPrem);
                            }

                            double sumprem = Double.valueOf(sum).doubleValue();
                            new DecimalFormat("0.00").format(sumprem);
                            double a = Double.valueOf(tRiskPrem).doubleValue();
                            new DecimalFormat("0.00").format(a);
                            double sub = sumprem - a;
                            new DecimalFormat("0.00").format(sub);
                            System.out.println("计算出的误差是 : " + sub);
                            if (sub != 0)
                            {
                                String sql3 = "select prem,insuredno from lcpol where "
                                        + "grpcontno='"
                                        + GrpContNo
                                        + "' "
                                        + "and riskcode='"
                                        + RiskCode
                                        + "' and insuredno in (select insuredno from lcinsured where "
                                        + " grpcontno='"
                                        + GrpContNo
                                        + "' and contplancode='"
                                        + ContPlanCode
                                        + "')";
                                System.out.println("查询出每个人的保费 ：" + sql3);
                                SSRS ssrs3 = mExeSQL.execSQL(sql3);
                                if (ssrs3 != null)
                                {
                                    beforePrem = ssrs3.GetText(1, 1);
                                    String beforeInsuredno = ssrs3
                                            .GetText(1, 2);
                                    System.out.println("被保险人号 : "
                                            + beforeInsuredno);
                                    double afterQueryPrem = Double.valueOf(
                                            beforePrem).doubleValue();
                                    new DecimalFormat("0.00")
                                            .format(afterQueryPrem);
                                    System.out.println("afterQueryPrem  : "
                                            + afterQueryPrem);
                                    System.out.println("sub  : " + sub);

                                    afterQueryPrem = afterQueryPrem - sub;
                                    System.out.println("sdsdfsd afterQueryPrem"
                                            + afterQueryPrem);
                                    new DecimalFormat("0.00")
                                            .format(afterQueryPrem);
                                    String sql4 = "update lcpol set prem="
                                            + afterQueryPrem
                                            + " where grpcontno = '"
                                            + GrpContNo + "' and insuredno='"
                                            + beforeInsuredno + "'";
                                    map.put(sql4, "UPDATE");
                                    System.out.println("执行的更新语句 ： " + sql4);
                                }
                            }
                        }

                        tempVata.add(map);
                    }
                    PubSubmit tPubSubmit = new PubSubmit();
                    if (!tPubSubmit.submitData(tempVata, "UPDATE"))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ParseGuideIn";
                        tError.functionName = "checkData";
                        tError.errorMessage = "在解决保费误差过程中，执行更新语句失败！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                else
                {
                    System.out.println("录入的人数与计划中的人数不符！");
                }
            }
        }

        /*、××××××××、*/

        //    if (tBatchNo == null || tBatchNo.trim().equals("")) {
        //      // @@错误处理
        //      CError tError = new CError();
        //      tError.moduleName = "ParseGuideIn";
        //      tError.functionName = "checkData";
        //      tError.errorMessage = "批次号不能为空!";
        //      this.mErrors.addOneError(tError);
        //      return false;
        //    }
        //    BatchNo = tBatchNo;
        //initLDGrp 保存了集体信息
        //        //解析被保险人xml
        //        LCInsuredSet tLCInsuredSet = (LCInsuredSet)this.ParseOneNode(tXPT,
        //                this.ParseInsuredPath);
        //        if (tLCInsuredSet == null)
        //        {
        //            CError.buildErr(this, "被保险人节点解析失败,被保险人不能为空!");
        //            m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(), "", "",
        //                                     "", "", "", null, this.mErrors,
        //                                     mGlobalInput);
        //            this.mErrors.clearErrors();
        //            return false;
        //        }
        //        //受益人解析
        //        LCBnfSet tLCBnfSet = (LCBnfSet)this.ParseOneNode(tXPT,
        //                this.ParseBnfPath);
        //        //连身被保人表
        //        LCInsuredRelatedSet tLCInsuredRelatedSet = (LCInsuredRelatedSet)this.
        //                ParseOneNode(tXPT, this.parseInsuredRelaPath);
        //
        //
        //        //保单信息
        //        m_GrpPolImpInfo.intiContPolData();
        //        boolean pd = parsePolData(tXPT);
        //        if (!pd)
        //        {
        //            System.out.println("解析保单节点出错!");
        //            return false;
        //        }
        //        if (!m_GrpPolImpInfo.init(tBatchNo, this.mGlobalInput,
        //                                  tLCInsuredSet, tLCBnfSet,
        //                                  tLCInsuredRelatedSet,
        //                                  tLCGrpContSchema))
        //        {
        //
        //            m_GrpPolImpInfo.logError(tBatchNo, grpContDB.getGrpContNo(), "", "",
        //                                     "", "", "", null, this.mErrors,
        //                                     mGlobalInput);
        //            this.mErrors.clearErrors();
        //            return false;
        //        }
        //
        //        //导入
        //        boolean bl = DiskContImport();
        if (!tFile.delete())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "Xml文件删除失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //    }
        if (this.mErrors.needDealError())
        {
            System.out.println(this.mErrors.getErrorCount() + "error:"
                    + this.mErrors.getFirstError());
        }

        return true;
    }

    /**
     * 提交到proposalBL ,做数据生成和准备
     *
     * @param tNewVData VData
     * @param tLCInsuredListSet LCInsuredListSchema
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema
     * @return boolean
     */
    private MMap submiDatattoProposalBL(VData tNewVData,
            LCInsuredListSet tLCInsuredListSet,
            LCGrpPolSchema tLCGrpPolSchema)
    {
        /** 首先获取保障计划的险种套餐标志 */
        //    mLDRiskWrapSchema
        if (tLCGrpPolSchema.getRiskWrapFlag() != null
                && tLCGrpPolSchema.getRiskWrapFlag().equals("Y"))
        {
            /** 进行处理套餐投保年龄的校验 */
        	for(int i = 1; i <= tLCInsuredListSet.size(); i++)
        	{
        		if(tLCInsuredListSet.get(i).getNoNamePeoples() <= 0)
        		{
        			if (!dealWrapAppage(tLCInsuredListSet, tLCGrpPolSchema))
                    {
                        return null;
                    }
        		}	
        	}	
        }
        //tNewVData 里面还有一些要创建的信息，因此需要重新获得
        MMap map = (MMap) tNewVData.getObjectByObjectName("MMap", 0);
        if (map == null)
        {
            map = new MMap();
        }
        String strID = (String) ((TransferData) tNewVData
                .getObjectByObjectName("TransferData", 0)).getValueByName("ID");
        String PolKey = (String) ((TransferData) tNewVData
                .getObjectByObjectName("TransferData", 0))
                .getValueByName("PolKey");
        ProposalBL tProposalBL = new ProposalBL();
        if (!tProposalBL.PrepareSubmitData(tNewVData, "INSERT||PROPOSAL"))
        {
            this.mErrors.copyAllErrors(tProposalBL.mErrors);
            //      m_GrpPolImpInfo.logError(strID, tLCPolSchema, false, this.mErrors,
            //                               mGlobalInput);
            return null;
        }
        else
        {
            VData pData = tProposalBL.getSubmitResult();
            if (pData == null)
            {
                CError.buildErr(this, "保单提交计算失败!");
                return null;
            }

            //LCContSchema rcontSchema = (LCContSchema)pData.getObjectByObjectName("LCContSchema",2);
            LCPolSchema rPolSchema = (LCPolSchema) pData.getObjectByObjectName(
                    "LCPolSchema", 0);
//            if (tLCContPlanRiskSchema != null
//                    && tLCContPlanRiskSchema.getRiskAmnt() > 0)
//            {
//                rPolSchema.setAmnt(tLCContPlanRiskSchema.getRiskAmnt());
//            }
            mEdorLCPolSet.add(rPolSchema);
            LCGrpPolSchema rGrpPol = (LCGrpPolSchema) pData
                    .getObjectByObjectName("LCGrpPolSchema", 0);
            LCGrpContSchema rGrpCont = (LCGrpContSchema) pData
                    .getObjectByObjectName("LCGrpContSchema", 0);
            LCPremBLSet rPremBLSet = (LCPremBLSet) pData.getObjectByObjectName(
                    "LCPremBLSet", 0);

            if (rPremBLSet == null)
            {
                CError.buildErr(this, "保单提交计算保费项数据准备有误!");
                return null;
            }
            LCPremSet rPremSet = new LCPremSet();
            for (int i = 1; i <= rPremBLSet.size(); i++)
            {
                rPremSet.add(rPremBLSet.get(i));
            }

            LCDutyBLSet rDutyBLSet = (LCDutyBLSet) pData.getObjectByObjectName(
                    "LCDutyBLSet", 0);
            if (rDutyBLSet == null)
            {
                CError.buildErr(this, "保单提交计算责任项数据准备有误!");
                return null;
            }
            LCDutySet rDutySet = new LCDutySet();
            for (int i = 1; i <= rDutyBLSet.size(); i++)
            {
                rDutySet.add(rDutyBLSet.get(i));
            }

            LCGetBLSet rGetBLSet = (LCGetBLSet) pData.getObjectByObjectName(
                    "LCGetBLSet", 0);
            if (rGetBLSet == null)
            {
                CError.buildErr(this, "保单提交计算给付项数据准备有误!");
                return null;
            }
            LCGetSet rGetSet = new LCGetSet();
            for (int i = 1; i <= rGetBLSet.size(); i++)
            {
                rGetSet.add(rGetBLSet.get(i));
            }

            LCBnfBLSet rBnfBLSet = (LCBnfBLSet) pData.getObjectByObjectName(
                    "LCBnfBLSet", 0);
            LCBnfSet rBnfSet = new LCBnfSet();
            if (rBnfBLSet != null)
            {
                for (int i = 1; i <= rBnfBLSet.size(); i++)
                {
                    rBnfSet.add(rBnfBLSet.get(i));
                }
            }
            LCInsuredRelatedSet tLCInsuredRelatedSet = (LCInsuredRelatedSet) pData
                    .getObjectByObjectName("LCInsuredRelatedSet", 0);
            // if ( tLCInsuredRelatedSet!=null && LCInsuredRelatedSet.size()>0)

            //更新个单合同金额相关，更新的时候同时更新缓存中的数据
            String contId = m_GrpPolImpInfo.getContKey(PolKey);
            LCContSchema tLCContSchema = m_GrpPolImpInfo
                    .findLCContfromCache(contId);
            if (tLCContSchema == null)
            {
                CError.buildErr(this, "查找合同[" + contId + "]失败");
                return null;
            }
            mContNo = tLCContSchema.getContNo();
            tLCContSchema.setPrem(PubFun.setPrecision(tLCContSchema.getPrem()
                    + rPolSchema.getPrem(), "0.00"));
            tLCContSchema.setAmnt(tLCContSchema.getAmnt()
                    + rPolSchema.getAmnt());
            tLCContSchema.setSumPrem(tLCContSchema.getSumPrem()
                    + rPolSchema.getSumPrem());
            tLCContSchema.setMult(tLCContSchema.getMult()
                    + rPolSchema.getMult());
            tLCContSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
            tLCContSchema.setSaleChnlDetail("01");
            if (StrTool.cTrim(mCardFalg).equals("1"))
            {
                tLCContSchema.setCardFlag("1");
            }
            tLCContSchema.setCInValiDate(rPolSchema.getEndDate());
            LCContSchema tContSchema = new LCContSchema();
            tContSchema.setSchema(tLCContSchema);
            LCContSet tContSet = new LCContSet();

            //如果是保全磁盘导入则把AppFlag置为2
            if ((mFlag != null) && (mFlag.equals("BQ")))
            {
                tContSchema.setAppFlag("2");
                rPolSchema.setAppFlag("2");
            }

            //更新集体险种金额相关
            String riskCode = rPolSchema.getRiskCode();
            LCGrpPolSchema mLCGrpPolSchema = m_GrpPolImpInfo
                    .findLCGrpPolSchema(riskCode);
            if (!StrTool.cTrim(rPolSchema.getAppFlag()).equals("2"))
            {
                tLCGrpPolSchema.setPrem(PubFun.setPrecision(tLCGrpPolSchema
                        .getPrem()
                        + rPolSchema.getPrem(), "0.00"));
                tLCGrpPolSchema.setAmnt(tLCGrpPolSchema.getAmnt()
                        + rPolSchema.getAmnt());
            }
            tLCGrpPolSchema.setPayEndDate(PubFun.getLaterDate(tLCGrpPolSchema
                    .getPayEndDate(), rPolSchema.getPayEndDate()));
            tLCGrpPolSchema.setPaytoDate(PubFun.getLaterDate(tLCGrpPolSchema
                    .getPaytoDate(), rPolSchema.getPaytoDate()));
            tLCGrpPolSchema.setFirstPayDate(PubFun.getBeforeDate(
                    tLCGrpPolSchema.getFirstPayDate(), rPolSchema
                            .getFirstPayDate()));
            rGrpPol.setPrem(tLCGrpPolSchema.getPrem());
            rGrpPol.setAmnt(tLCGrpPolSchema.getAmnt());
            rGrpPol.setPayEndDate(tLCGrpPolSchema.getPayEndDate());
            rGrpPol.setPaytoDate(tLCGrpPolSchema.getPaytoDate());
            rGrpPol.setFirstPayDate(tLCGrpPolSchema.getFirstPayDate());
            rGrpPol.setCValiDate(rPolSchema.getCValiDate());
            //更新团单合同金额相关
            LCGrpContSchema sGrpContSchema = m_GrpPolImpInfo
                    .getLCGrpContSchema();
            //            if (!StrTool.cTrim(rPolSchema.getAppFlag()).equals("2")) {
            //                sGrpContSchema.setPrem(PubFun.setPrecision(sGrpContSchema.
            //                        getPrem() +
            //                        rPolSchema.getPrem(), "0.00"));
            //                sGrpContSchema.setAmnt(sGrpContSchema.getAmnt() +
            //                                       rPolSchema.getAmnt());
            //            }
            rGrpCont.setPrem(sGrpContSchema.getPrem());
            rGrpCont.setSaleChnlDetail("01");
            rGrpCont.setAmnt(sGrpContSchema.getAmnt());
            tContSet.add(tContSchema);
            mEdorLCContSet.add(tContSchema);
            LCInsuredListSchema tLCInsuredListSchema = null;
            for (int i = 1; i <= tLCInsuredListSet.size(); i++)
            {
                tLCInsuredListSchema = tLCInsuredListSet.get(i);
                tLCInsuredListSchema.setContNo(tContSchema.getContNo());
                tLCInsuredListSchema.setState("1");
                //                tLCInsuredListSet.get(i).setInsuredNo(tContSchema.getInsuredNo());
                tLCInsuredListSchema.setBatchNo(this.mBatchNo);
                if (StrTool.cTrim(tLCInsuredListSet.get(i).getInsuredNo())
                        .equals(rPolSchema.getInsuredNo()))
                {
                    tLCInsuredListSchema = tLCInsuredListSet.get(i);
                }
                /**　处理完成被保人清单，校验处理对应险种信息 */
                String strSqlListPol = "update LCInsuredListPol set ContNo='"
                        + tLCInsuredListSchema.getContNo() + "',PolNo='"
                        + rPolSchema.getPolNo() + "' where GrpContNo='"
                        + this.mGrpContNo + "' and InsuredID='"
                        + tLCInsuredListSchema.getInsuredID()
                        + "' and RiskCode='" + rPolSchema.getRiskCode() + "'";
                LCInsuredListPolDB tLCInsuredListPolDB = new LCInsuredListPolDB();
                tLCInsuredListPolDB.setGrpContNo(this.mGrpContNo);
                tLCInsuredListPolDB.setInsuredID(tLCInsuredListSchema
                        .getInsuredID());
                tLCInsuredListPolDB.setRiskCode(rPolSchema.getRiskCode());
                if (tLCInsuredListPolDB.getCount() > 0)
                {
                    map.put(strSqlListPol, "UPDATE");
                }
            }
            LCInsuredListSet tmpLCInsuredListSet = new LCInsuredListSet();
            tmpLCInsuredListSet.add(tLCInsuredListSet);

            rPolSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
            map.put(tmpLCInsuredListSet, "UPDATE");
            map.put(tContSet, "UPDATE");
            map.put(rPolSchema.getSchema(), "INSERT");
            if (StrTool.cTrim(mEdorNo).equals("")
                    & StrTool.cTrim(mEdorType).equals(""))
            {
                map.put(rGrpPol.getSchema(), "UPDATE");
                map.put(rGrpCont.getSchema(), "UPDATE");
            }
            map.put(rPremSet, "INSERT");
            map.put(rDutySet, "INSERT");
            map.put(rGetSet, "INSERT");
            map.put(rBnfSet, "INSERT");
            map.put(tLCInsuredRelatedSet, "INSERT");
            Date date = new Date();
            Random rd = new Random(date.getTime());
            long u = rd.nextLong();
            //个人单需要重新统计个单合同人数
            if ("0".equals(tContSchema.getPolType()))
            {
                String s0 = " update lccont set peoples=(select count(distinct insuredno) from lcpol where '"
                        + u
                        + "'='"
                        + u
                        + "' and contno='"
                        + tContSchema.getContNo()
                        + "' and PolTypeFlag in ('0','1')),prem=(select sum(prem) from lcpol where '"
                        + u
                        + "'='"
                        + u
                        + "' and contno='"
                        + tContSchema.getContNo()
                        + "') "
                        + " where contno='"
                        + tContSchema.getContNo() + "'" + " and PolType = '0' ";
                map.put(s0, "UPDATE");
            }
            String s1 = "update lcgrpcont  set peoples2=(select sum(peoples) from lccont where '"
                    + u
                    + "'='"
                    + u
                    + "' and GrpContNo='"
                    + tContSchema.getGrpContNo()
                    + "' and PolType in ('0','1'))"
                    + " where grpcontno='"
                    + rGrpCont.getGrpContNo() + "'";

            String s2 = "update lcgrppol set peoples2=(select sum(insuredpeoples) from lcpol where '"
                    + u
                    + "'='"
                    + u
                    + "' and grppolno='"
                    + rGrpPol.getGrpPolNo()
                    + "' and PolTypeFlag in ('0','1'))"
                    + " where grppolno='" + rGrpPol.getGrpPolNo() + "'";
            if (StrTool.cTrim(rPolSchema.getPolTypeFlag()).equals("0")
                    || StrTool.cTrim(rPolSchema.getPolTypeFlag()).equals("1"))
            {
                s2 = "update lcgrppol set peoples2=to_zero(peoples2) + 1 where grppolno='"
                        + rPolSchema.getGrpPolNo()
                        + "' and '"
                        + u
                        + "'='"
                        + u
                        + "'";
            }
            if (StrTool.cTrim(tContSchema.getPolType()).equals("0")
                    || StrTool.cTrim(tContSchema.getPolType()).equals("1"))
            {
                s2 = "update lcgrpcont set peoples2=to_zero(peoples2) + to_zero("
                        + tContSchema.getPeoples()
                        + ") where GrpContNo='"
                        + tContSchema.getGrpContNo()
                        + "' and '"
                        + u
                        + "'='"
                        + u + "'";
            }

            //            String s0 =
            //                    "update lccont set peoples=(select count(distinct insuredno) from lcpol where "
            //                    + u + "=" + u + " and contno='"
            //                    + tContSchema.getContNo() + "')";
            //更新人数
            //            String s1 =
            //                    "update lcgrpcont  set peoples2=(select count(*) from lcinsured where "
            //                    + u + "=" + u + " and GrpContNo='" +
            //                    tContSchema.getGrpContNo() + "')"
            //                    + " where grpcontno='" + rGrpCont.getGrpContNo() + "'";
            //            String s2 = "update lcgrppol set peoples2=(select count(distinct insuredno) from lcpol where "
            //                        + u + "=" + u + " and grppolno='"
            //                        + rGrpPol.getGrpPolNo()
            //                        + "' and riskcode='" + tLCPolSchema.getRiskCode() +
            //                        "')"
            //                        + " where grppolno='" + rGrpPol.getGrpPolNo() + "'";
            //            map.put(s0, "UPDATE");
            /**
             * 保全增人和公共账户都不记为被保险人数
             * @author:yangming
             */
            if (StrTool.cTrim(mEdorType).equals("")
                    && !StrTool.cTrim(this.PolTypeFlag).equals("C")
                    && !StrTool.cTrim(this.PolTypeFlag).equals("G")
                    && !StrTool
                            .cTrim(this.mLCGrpContSchema.getBigProjectFlag())
                            .equals("1"))
            {
                map.put(s1, "UPDATE");
                map.put(s2, "UPDATE");
            }
            //缓存保存的主险保单信息
            m_GrpPolImpInfo.cachePolInfo(strID, rPolSchema);
        }

        return map;
    }

    /**
     * 得到MMap，保全调用
     * @return MMap
     */
    public MMap getSubmitData()
    {
        return mMap;
    }

    private boolean batchSave(MMap map)
    {

        if ((mFlag != null) && ((mFlag.equals("BQ")) || (mFlag.equals("N"))))
        {
            System.out.println("添加一个被保人信息，准备保全递交");
            try
            {
                mMap.add(map);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                System.out.println("内存益出");
            }
            return true;
        }
        PubSubmit pubSubmit = new PubSubmit();
        VData sData = new VData();
        sData.add(map);
        boolean tr = pubSubmit.submitData(sData, "");

        if (!tr)
        {
            if (pubSubmit.mErrors.getErrorCount() > 0)
            {
                //错误回退
                mErrors.copyAllErrors(pubSubmit.mErrors);
                pubSubmit.mErrors.clearErrors();
            }
            else
            {
                CError.buildErr(this, "保存数据库的时候失败！");
            }
            return false;
        }
        return true;
    }

    /**
     * 以合同为单位做磁盘投保,对一个合同所有保单，一旦有一个险种保单导入失败，则要么失败
     *
     * @return boolean
     * @param contIndex String
     */
    private boolean DiskContImport(String contIndex)
    {
        LCInsuredSchema insuredSchema = null;
        LCInsuredSet tInsuredSet = m_GrpPolImpInfo.getLCInsuredSet();
        MMap subMap = null; //提交结果集缓存
        boolean state = true; //导入状态，
        boolean saveState = true;
        String logRiskCode = "";
        String logContPlanCode = "";
        String logPolId = "";
        String insuredIndex = "";

        //取出缓存的合同表，并对序号排序，按合同序号从小到大导入
        //    HashMap contMap = m_GrpPolImpInfo.getContCache();

        //       Set keySet = contMap.keySet() ;
        //      int[] ContKey = this.getOrderKeySetInt(keySet);

        //      for (int uu = 0; uu < ContKey.length; uu++)

        //    {
        //    Object key = contMapp.getKeyByOrder(String.valueOf(g + 1));
        subMap = null;
        //   String contIndex = String.valueOf( ContKey[uu] ) ;
        mContNo = "";
        LCGrpImportLogSchema logSchema = m_GrpPolImpInfo.getLogInfo(mBatchNo,
                contIndex);
        if (logSchema != null && "0".equals(logSchema.getErrorState()))
        {
            //该批次号该合同已经导入成功
            //   continue;
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "第" + contIndex + "个被保险人已导入过！     ";
            this.logErrors.addOneError(tError);
            CError.buildErr(this, "该批次号合同ID为[" + contIndex + "]的合同已经导入过！");
            m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo, contIndex,
                    "", "", "", "", null, this.mErrors, mGlobalInput);
            this.mErrors.clearErrors();
            return false;
        }
        /**
         * 校验被保险人的各种信息
         * 目前的校验信息较少暂时添加日期生日校验\
         * 身份证校验\性别\姓名\
         */
        if (!checkInsured(tInsuredSet,null))
        {
            return false;
        }

        state = true;
        //先查找有保障计划的导入
        for (int i = 1; i <= tInsuredSet.size(); i++)
        {
            //初始化，避免重复使用
            logRiskCode = "";
            logContPlanCode = "";
            logPolId = "";
            if (state == false)
            {
                break;
            }
            insuredSchema = tInsuredSet.get(i);
            insuredSchema.setSequenceNo(String.valueOf(i));
            //不是同一个合同的，跳出
            if (!insuredSchema.getContNo().equals(contIndex))
            {
                continue;
            }
            //不是保障计划的，跳出
            if ("".equals(StrTool.cTrim(insuredSchema.getContPlanCode())))
            {
                continue;
            }

            //解析XML时借用PrtNo保存被保人序号
            insuredIndex = insuredSchema.getPrtNo();
            if ("".equals(StrTool.cTrim(insuredIndex)))
            {
                CError.buildErr(this, "第[" + i + "]个被保人ID没有填写");
                //continue;
                state = false;
                break; //break for insured
            }
            //保障计划代码不空才导入
            logContPlanCode = insuredSchema.getContPlanCode();
            LCContPlanSchema tContPlanSchema = m_GrpPolImpInfo
                    .findLCContPlan(insuredSchema.getContPlanCode());
            if (tContPlanSchema == null)
            {
                CError.buildErr(this, "被保险人[" + i + "]保单保险计划["
                        + insuredSchema.getContPlanCode() + "]查找失败");
                //continue;
                state = false;
                break; //break for insured
            }

            //险种保险计划
            LCContPlanRiskSet tLCContPlanRiskSet = m_GrpPolImpInfo
                    .findLCContPlanRiskSet(insuredSchema.getContPlanCode());

            if (tLCContPlanRiskSet == null || tLCContPlanRiskSet.size() <= 0)
            {
                CError.buildErr(this, "被保险人[" + i + "]保单险种保险计划["
                        + insuredSchema.getContPlanCode() + "]查找失败");
                //continue;
                state = false;
                break; //break for insured
            }

//            //对保险险种计划排序，确保主险在前面
//            LCContPlanRiskSet mainPlanRiskSet = new LCContPlanRiskSet();
//            LCContPlanRiskSet subPlanRiskSet = new LCContPlanRiskSet();
//            LCContPlanRiskSchema contPlanRiskSchema = null;
//            for (int t = 1; t <= tLCContPlanRiskSet.size(); t++)
//            {
//                contPlanRiskSchema = tLCContPlanRiskSet.get(t);
//                if (contPlanRiskSchema.getRiskCode().equals(
//                        contPlanRiskSchema.getMainRiskCode()))
//                {
//                    mainPlanRiskSet.add(contPlanRiskSchema);
//                }
//                else
//                {
//                    subPlanRiskSet.add(contPlanRiskSchema);
//                }
//
//            }
//            mainPlanRiskSet.add(subPlanRiskSet);
            //根据险种计划生成险种保单相关信息
//          根据险种信息生成险种保单相关信息
            // BY GZH DATE 20101215
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo(mGrpContNo);
            LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
            for (int j = 1; j <= tLCGrpPolSet.size(); j++)
            {
                logRiskCode = tLCGrpPolSet.get(j).getRiskCode();
                //生成保单，责任等信息
                VData tData = prepareContPlanData(logRiskCode,contIndex, insuredIndex,
                        insuredSchema, tLCGrpPolSet.get(j), null);
                if (tData == null)
                {
                    state = false;
                    break; //break for conplarisk
                }
                //提交生成数据
                MMap oneRisk = submiDatattoProposalBL(tData, null,
                		tLCGrpPolSet.get(j));
                if (oneRisk == null)
                {
                    //失败
                    state = false;
                    break; //break for insured
                }
                else
                {
                    //成功
                    if (subMap == null)
                    {
                        subMap = oneRisk;
                    }
                    else
                    {
                        subMap.add(oneRisk);
                    }
                }
            }
        }
        //计划导入准备期间发生错误
        if (state == false)
        {
            logError(contIndex, insuredIndex, logPolId, logContPlanCode,
                    logRiskCode, insuredSchema);
            saveState = false;
            //continue;
            return false;
        }
        else
        {
            //无计划导入
            //导入险种保单，先根据合同ID查找到所有相同合同id下的所有保单数据，再一张一张保单信息导入
            VData tContPolData = (VData) m_GrpPolImpInfo
                    .getContPolData(contIndex);
            if (tContPolData != null)
            {

                LCPolSchema tLCPolSchema = null;
                VData tPolData = null;
                for (int u = 0; u < tContPolData.size(); u++)
                {
                    tPolData = (VData) tContPolData.get(u);
                    if (state == false)
                    {
                        break;
                    }
                    for (int i = 0; i < tPolData.size(); i++)
                    {

                        //初始化，避免重复使用
                        logRiskCode = "";
                        logContPlanCode = "";
                        logPolId = "";
                        if (state == false)
                        {
                            break;
                        }

                        VData onePolData = (VData) tPolData.get(i);
                        if (onePolData == null)
                        {
                            continue;
                        }
                        //备份原ID号
                        tLCPolSchema = (LCPolSchema) onePolData
                                .getObjectByObjectName("LCPolSchema", 0);
                        logRiskCode = tLCPolSchema.getRiskCode();
                        insuredIndex = tLCPolSchema.getInsuredNo();
                        contIndex = tLCPolSchema.getContNo();

                        VData tprepareData = this
                                .prepareProposalBLData(onePolData);
                        if (tprepareData == null)
                        {
                            System.out.println("准备数据的时候出错！");
                            state = false;
                        }
                        else
                        {
                            MMap tMap = this.submiDatattoProposalBL(
                                    tprepareData, null, null);
                            if (tMap == null)
                            {

                                state = false;
                            }
                            if (subMap == null)
                            {
                                subMap = tMap;
                            }
                            else
                            {
                                subMap.add(tMap);
                            }
                        }
                    }
                }
            }
        }
        if (state == false)
        {

            logError(contIndex, insuredIndex, logPolId, "", logRiskCode,
                    insuredSchema);
            saveState = false;
            return false;
            //continue;
        }

        boolean bs = true;

        if (state && subMap != null && subMap.keySet().size() > 0)
        {
            MMap logMap = m_GrpPolImpInfo.logSucc(mBatchNo, mGrpContNo,
                    contIndex, mPrtNo, mContNo, mGlobalInput);
            subMap.add(logMap);
            //            subMap.put(mLCGrpIssuePolSet, "DELETE&INSERT");
            bs = batchSave(subMap);
        }

        if (!bs || !state)
        {
            //合同导入没有成功，需要回退缓存
            m_GrpPolImpInfo.removeCaceh(contIndex);
            System.out.println("合同[" + contIndex + "]导入失败！！"
                    + this.mErrors.getErrContent());

            //导入失败日志，数据库保存，只能定位到合同上的失败
            logError(contIndex, "", "", "", "", null);

            //保存导入信息
            saveState = false;
        }
        else
        {
            System.out.println("合同[" + contIndex + "]导入成功！！");
            //保存导入信息
        }

        //   } //end contKeyId

        return saveState;
    }

    private boolean DiskContImport(LCInsuredListSet tLCInsuredListSet,
            String contIndex)
    {
        LCInsuredSet tInsuredSet = new LCInsuredSet();
        for (int i = 1; i <= tLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tLCInsuredListSchema = tLCInsuredListSet.get(i)
                    .getSchema();
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            this.PolTypeFlag = tLCInsuredListSchema.getPublicAccType();
            this.PublicAcc = tLCInsuredListSchema.getPublicAcc();
            tLCInsuredSchema.setContNo(contIndex);
            //tLCInsuredSchema.setInsuredNo(tLCInsuredListSchema.getInsuredID());
            tLCInsuredSchema.setPrtNo(tLCInsuredListSchema.getInsuredID());
            tLCInsuredSchema.setName(tLCInsuredListSchema.getInsuredName());
            tLCInsuredSchema.setSex(tLCInsuredListSchema.getSex());
            tLCInsuredSchema.setInsuredStat(tLCInsuredListSchema.getRetire());
            tLCInsuredSchema.setBirthday(tLCInsuredListSchema.getBirthday());
            System.out.println("tLCInsuredSchema : "
                    + tLCInsuredSchema.getBirthday());
            tLCInsuredSchema.setIDType(tLCInsuredListSchema.getIDType());
            tLCInsuredSchema.setIDNo(tLCInsuredListSchema.getIDNo());
            tLCInsuredSchema.setContPlanCode(tLCInsuredListSchema
                    .getContPlanCode());
            tLCInsuredSchema.setOccupationType(tLCInsuredListSchema
                    .getOccupationType());
            tLCInsuredSchema.setBankAccNo(tLCInsuredListSchema.getBankAccNo());
            tLCInsuredSchema.setBankCode(tLCInsuredListSchema.getBankCode());
            tLCInsuredSchema.setAccName(tLCInsuredListSchema.getAccName());
            tLCInsuredSchema.setRelationToMainInsured(tLCInsuredListSchema
                    .getRelation());
           //tLCInsuredSchema.setPosition(tLCInsuredListSchema
           //         .getPosition());
          //  tLCInsuredSchema.setGrpContNo(tLCInsuredListSchema.getGrpContNo());
            tLCInsuredSchema.setOthIDNo(tLCInsuredListSchema.getOthIDNo());
            tLCInsuredSchema.setOthIDType(tLCInsuredListSchema.getOthIDType());
            tLCInsuredSchema.setEnglishName(tLCInsuredListSchema
                    .getEnglishName());
            tLCInsuredSchema
                    .setGrpInsuredPhone(tLCInsuredListSchema.getPhone());
            tLCInsuredSchema.setNativePlace(tLCInsuredListSchema.getNativePlace());
            tLCInsuredSchema.setNativeCity(tLCInsuredListSchema.getNativeCity());
            if (StrTool.cTrim(tLCInsuredListSchema.getPublicAccType()).equals(
                    "1"))
            {
                tLCInsuredSchema
                        .setMakeTime(String.valueOf((int) tLCInsuredListSchema
                                .getNoNamePeoples()));
                //            LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
                //            tLCContPlanRiskDB.setGrpContNo(this.mGrpContNo);
                //            tLCContPlanRiskDB.setContPlanCode(tLCInsuredSchema.getContPlanCode());
                //            LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
                //            if (tLCContPlanRiskSet.size() > 1) {
                //                buildError("DiskContImport", "无名单暂不支持单计划多险种情况！");
                //                return false;
                //            }
                //            if (tLCContPlanRiskSet.size() <= 0) {
                //                buildError("DiskContImport", "没有录入保险计划！");
                //                return false;
                //            }
                //            if (this.InputPrem <= 0) {
                //                buildError("DiskContImport", "无名单录入保费为0,请重新确认保险计划是否录入正确！");
                //                return false;
                //            }
                //            this.InputPrem = tLCContPlanRiskSet.get(1).getRiskPrem();
            }
            CalRule = StrTool.cTrim(tLCInsuredListSchema.getCalRule()).equals(
                    "") ? "" : tLCInsuredListSchema.getCalRule();
            if (!StrTool.cTrim(tLCInsuredListSchema.getRiskCode()).equals(""))
            {
                LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
                tLCContPlanRiskDB.setRiskCode(tLCInsuredListSchema
                        .getRiskCode());
                tLCContPlanRiskDB.setGrpContNo(this.mGrpContNo);
                LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB
                        .query();
                if (tLCContPlanRiskSet.size() > 0)
                {
                    tLCInsuredSchema.setContPlanCode(tLCContPlanRiskSet.get(1)
                            .getContPlanCode());
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "ParseGuideIn";
                    tError.functionName = "checkData";
                    tError.errorMessage = "生成账户错误！";
                    this.logErrors.addOneError(tError);
                }
            }
            String DiskNo = "";
            try
            {
                int x = Integer.parseInt(tLCInsuredListSchema.getInsuredID());
                System.out.println(x);
                DiskNo = String.valueOf(x);
            }
            catch (Exception ex)
            {
                String strSql = "select count(1) from lcinsuredlist where grpcontno='"
                        + tLCInsuredListSchema.getGrpContNo() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                DiskNo = String.valueOf(Integer.parseInt(tExeSQL
                        .getOneValue(strSql)) + 1);
            }
            tLCInsuredSchema.setDiskImportNo(DiskNo);
            if (StrTool.cTrim(PolTypeFlag).equals("C")
                    || StrTool.cTrim(PolTypeFlag).equals("G"))
            {
                tLCInsuredSchema.setCreditGrade("2");
            }
            else if (StrTool.cTrim(PolTypeFlag).equals("1"))
            {
                tLCInsuredSchema.setCreditGrade("1");
            }
            else
            {
                tLCInsuredSchema.setCreditGrade("0");
            }

            if (tLCInsuredListSchema.getPublicAcc() > 0
                    && !StrTool.cTrim(PolTypeFlag).equals("1")
                    && !StrTool.cTrim(this.mEdorType).equals(""))
            {
                this.InputPrem = tLCInsuredListSchema.getPublicAcc();
            }
            tInsuredSet.add(tLCInsuredSchema);
        }

        /**
         * 初始化缓存
         */
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();
        m_GrpPolImpInfo.init(this.mBatchNo, this.mGlobalInput, tInsuredSet,
                tLCBnfSet, tLCInsuredRelatedSet, mLCGrpContSchema);
        /** 检验内存泄漏问题 */
        m_GrpPolImpInfo.deBug(contIndex);

        LCInsuredSchema insuredSchema = null;

        MMap subMap = null; //提交结果集缓存
        boolean state = true; //导入状态，
        boolean saveState = true;
        String logRiskCode = "";
        String logContPlanCode = "";
        String logPolId = "";
        String insuredIndex = "";

        /**
         * 导入前的相关校验
         */
        /*
         LCGrpImportLogSchema logSchema = m_GrpPolImpInfo.getLogInfo(
         mBatchNo, contIndex);
         if (logSchema != null && "0".equals(logSchema.getErrorState())) {
         //该批次号该合同已经导入成功
         //   continue;
         CError tError = new CError();
         tError.moduleName = "ParseGuideIn";
         tError.functionName = "checkData";
         tError.errorMessage = "第" + contIndex + "个被保险人已导入过！     ";
         this.logErrors.addOneError(tError);
         CError.buildErr(this,
         "该批次号合同ID为[" + contIndex +
         "]的合同已经导入过！");
         m_GrpPolImpInfo.logError(this.mBatchNo,
         this.mGrpContNo,
         contIndex,
         "",
         "", "", "", null, this.mErrors,
         mGlobalInput);
         this.mErrors.clearErrors();
         return false;
         }
         */

        /**
         * 如果是公共账户则不判断被保险人信息
         * @author yangming
         */
        if (!StrTool.cTrim(PolTypeFlag).equals("C")
                && !StrTool.cTrim(PolTypeFlag).equals("G")
                && !StrTool.cTrim(PolTypeFlag).equals("1"))
        {
            /**
             * 校验被保险人的各种信息
             * 目前的校验信息较少暂时添加日期生日校验\
             * 身份证校验\性别\姓名\职业类别\职业代码
             */
            if (!checkInsured(tInsuredSet,tLCInsuredListSet))
            {
                return false;
            }
        }
        //        return false;

        for (int i = 1; i <= tInsuredSet.size(); i++)
        {
            /** 首先判断是否为保全已导入信息 */
//            if (StrTool.cTrim(tInsuredSet.get(i).getContPlanCode())
//                    .equals("FM"))
//            {
//                LCContDB tLCContDB = new LCContDB();
//                if (tInsuredSet.get(i).getName() == null
//                        || tInsuredSet.get(i).getName().equalsIgnoreCase(""))
//                {
//                    buildError("DiskContImport", "保全增人没有录入主被保人姓名！");
//                    return false;
//                }
//                if (tInsuredSet.get(i).getIDNo() == null
//                        || tInsuredSet.get(i).getIDNo().equalsIgnoreCase(""))
//                {
//                    buildError("DiskContImport", "保全增人没有录入主被保人证件号码！");
//                    return false;
//                }
//                if (tInsuredSet.get(i).getIDType() == null
//                        || tInsuredSet.get(i).getIDType().equalsIgnoreCase(""))
//                {
//                    buildError("DiskContImport", "保全增人没有录入主被保人证件类型！");
//                    return false;
//                }
//                tLCContDB.setGrpContNo(this.mGrpContNo);
//                tLCContDB.setInsuredName(tInsuredSet.get(i).getName());
//                tLCContDB.setInsuredIDNo(tInsuredSet.get(i).getIDNo());
//                tLCContDB.setInsuredIDType(tInsuredSet.get(i).getIDType());
//                LCContSet tLCContSet = tLCContDB.query();
//                if (tLCContSet == null || tLCContSet.size() <= 0)
//                {
//                    buildError("DiskContImport",
//                            "保全增人增加子女,在查找对应主被保人时查询失败,请确认系统中存在("
//                                    + tInsuredSet.get(i).getName() + ")此被保人！");
//                    return false;
//                }
//                if (tLCContSet.size() > 1)
//                {
//                    buildError("DiskContImport",
//                            "保全增人增加子女,根据被保人姓名,证件号码,证件类型,在系统中不能确定唯一一个主被保人,请确认系统中存在唯一一个("
//                                    + tInsuredSet.get(i).getName() + ")此被保人！");
//                    return false;
//                }
//                this.m_GrpPolImpInfo.cacheLCContSchema(contIndex, tLCContSet
//                        .get(i));
//                continue;
//            }
            //初始化，避免重复使用
            logRiskCode = "";
            logContPlanCode = "";
            logPolId = "";
            if (state == false)
            {
                break;
            }
            insuredSchema = tInsuredSet.get(i);
            insuredSchema.setSequenceNo(String.valueOf(i));
            //不是同一个合同的，跳出
            if (!insuredSchema.getContNo().equals(contIndex))
            {
                continue;
            }
            //不是保障计划的，跳出
//            if ("".equals(StrTool.cTrim(insuredSchema.getContPlanCode())))
//            {
//                continue;
//            }

            //解析XML时借用PrtNo保存被保人序号
            insuredIndex = insuredSchema.getPrtNo();
            if ("".equals(StrTool.cTrim(insuredIndex)))
            {
                CError.buildErr(this, "第[" + i + "]个被保人ID没有填写");
                //continue;
                state = false;
                break; //break for insured
            }
//            //保障计划代码不空才导入
//            logContPlanCode = insuredSchema.getContPlanCode();
//            LCContPlanSchema tContPlanSchema = m_GrpPolImpInfo
//                    .findLCContPlan(insuredSchema.getContPlanCode());
//            if (tContPlanSchema == null
//                    && !StrTool.cTrim(logContPlanCode).equals("FM"))
//            {
//                CError.buildErr(this, "被保险人[" + i + "]保单保险计划["
//                        + insuredSchema.getContPlanCode() + "]查找失败");
//                //continue;
//                state = false;
//                break; //break for insured
//            }
//
//            //险种保险计划
//            LCContPlanRiskSet tLCContPlanRiskSet = m_GrpPolImpInfo
//                    .findLCContPlanRiskSet(insuredSchema.getContPlanCode());
//
//            if ((tLCContPlanRiskSet == null || tLCContPlanRiskSet.size() <= 0)
//                    && !StrTool.cTrim(logContPlanCode).equals("FM"))
//            {
//                CError.buildErr(this, "被保险人[" + i + "]保单险种保险计划["
//                        + insuredSchema.getContPlanCode() + "]查找失败");
//                //continue;
//                state = false;
//                break; //break for insured
//            }
//
//            //对保险险种计划排序，确保主险在前面
//            LCContPlanRiskSet mainPlanRiskSet = new LCContPlanRiskSet();
//            LCContPlanRiskSet subPlanRiskSet = new LCContPlanRiskSet();
//            LCContPlanRiskSchema contPlanRiskSchema = null;
//            for (int t = 1; t <= tLCGrpPolSet.size(); t++)
//            {
//                contPlanRiskSchema = tLCGrpPolSet.get(t);
//                if (contPlanRiskSchema.getRiskCode().equals(
//                        contPlanRiskSchema.getMainRiskCode()))
//                {
//                    mainPlanRiskSet.add(contPlanRiskSchema);
//                }
//                else
//                {
//                    subPlanRiskSet.add(contPlanRiskSchema);
//                }
//
//            }
//            mainPlanRiskSet.add(subPlanRiskSet);
//            /** 处理一下保险计划信息,目的是将非账户计划信息剔除 */
//            for (int n = 1; n <= mainPlanRiskSet.size(); n++)
//            {
//                for (int m = 1; m <= tLCInsuredListSet.size(); m++)
//                {
//                    String tRiskCode = tLCInsuredListSet.get(m).getRiskCode();
//                    if (tRiskCode != null && !tRiskCode.equals(""))
//                    {
//                        if (!mainPlanRiskSet.get(n).getRiskCode().equals(
//                                tRiskCode))
//                        {
//                            mainPlanRiskSet.removeRange(n, n);
//                            n--;
//                        }
//                    }
//                }
//            }
            //根据险种信息生成险种保单相关信息
            // BY GZH DATE 20101215
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpContNo(mGrpContNo);
            LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
            for (int j = 1; j <= tLCGrpPolSet.size(); j++)
            {
                logRiskCode = tLCGrpPolSet.get(j).getRiskCode();
                //生成保单，责任等信息
                /** 添加在职退休比,平均年龄等计算要素 */
//                retire = getRetire(mainPlanRiskSet.get(j));
//                avgage = getInsuredAvgAge(mainPlanRiskSet.get(j));
//                InsuredNum = getInsuredNum(mainPlanRiskSet.get(j));

                // 增加“领取频次：GrpGetIntv”计算要素（失能险）。
                //mGrpGetIntv = getGrpGetIntv(mainPlanRiskSet.get(j));
                // ---------------------------------------------

                VData tData = prepareContPlanData(logRiskCode,contIndex, insuredIndex,
                        insuredSchema, tLCGrpPolSet.get(j),
                        tLCInsuredListSet);
                if (tData == null)
                {
                    state = false;
                    break; //break for conplarisk
                }

                //提交生成数据
                MMap oneRisk = submiDatattoProposalBL(tData, tLCInsuredListSet,
                		tLCGrpPolSet.get(j));
                if (oneRisk == null)
                {
                    //失败
                    state = false;
                    break; //break for insured
                }
                else
                {
                    //成功
                    if (subMap == null)
                    {
                        subMap = oneRisk;
                    }
                    else
                    {
                        subMap.add(oneRisk);
                    }
                }
            }
        }
        //计划导入准备期间发生错误
        if (state == false)
        {
            logError(contIndex, insuredIndex, logPolId, logContPlanCode,
                    logRiskCode, insuredSchema);
            saveState = false;
            //continue;
            return false;
        }
        else
        {
            //无计划导入
            //导入险种保单，先根据合同ID查找到所有相同合同id下的所有保单数据，再一张一张保单信息导入
            VData tContPolData = (VData) m_GrpPolImpInfo
                    .getContPolData(contIndex);
            if (tContPolData != null)
            {

                LCPolSchema tLCPolSchema = null;
                VData tPolData = null;
                for (int u = 0; u < tContPolData.size(); u++)
                {
                    tPolData = (VData) tContPolData.get(u);
                    if (state == false)
                    {
                        break;
                    }
                    for (int i = 0; i < tPolData.size(); i++)
                    {

                        //初始化，避免重复使用
                        logRiskCode = "";
                        logContPlanCode = "";
                        logPolId = "";
                        if (state == false)
                        {
                            break;
                        }

                        VData onePolData = (VData) tPolData.get(i);
                        if (onePolData == null)
                        {
                            continue;
                        }
                        //备份原ID号
                        tLCPolSchema = (LCPolSchema) onePolData
                                .getObjectByObjectName("LCPolSchema", 0);
                        logRiskCode = tLCPolSchema.getRiskCode();
                        insuredIndex = tLCPolSchema.getInsuredNo();
                        contIndex = tLCPolSchema.getContNo();

                        VData tprepareData = this
                                .prepareProposalBLData(onePolData);
                        if (tprepareData == null)
                        {
                            System.out.println("准备数据的时候出错！");
                            state = false;
                        }
                        else
                        {
                            MMap tMap = this.submiDatattoProposalBL(
                                    tprepareData, tLCInsuredListSet, null);
                            if (tMap == null)
                            {

                                state = false;
                            }
                            if (subMap == null)
                            {
                                subMap = tMap;
                            }
                            else
                            {
                                subMap.add(tMap);
                            }
                        }
                    }
                }
            }
        }
        if (state == false)
        {

            logError(contIndex, insuredIndex, logPolId, "", logRiskCode,
                    insuredSchema);
            saveState = false;
            return false;
        }

        boolean bs = true;

        if (state && subMap != null && subMap.keySet().size() > 0)
        {
            MMap logMap = m_GrpPolImpInfo.logSucc(mBatchNo, mGrpContNo,
                    contIndex, mPrtNo, mContNo, mGlobalInput);
            subMap.add(logMap);
            //            subMap.put(mLCGrpIssuePolSet, "DELETE&INSERT");
            bs = batchSave(subMap);
        }

        if (!bs || !state)
        {
            //合同导入没有成功，需要回退缓存
            m_GrpPolImpInfo.removeCaceh(contIndex);
            System.out.println("合同[" + contIndex + "]导入失败！！"
                    + this.mErrors.getErrContent());

            //导入失败日志，数据库保存，只能定位到合同上的失败
            logError(contIndex, "", "", "", "", null);

            //保存导入信息
            saveState = false;
        }
        else
        {
            this.InputPrem = 0.0;
            this.InputAmnt = 0.0;
            System.out.println("合同[" + contIndex + "]导入成功！！");
            //保存导入信息
        }

        //   } //end contKeyId

        return saveState;
    }

    /**
     * 按保险计划导入的数据准备
     *
     * @param contIndex String 合同Id
     * @param insuredIndex String 被保险人Id
     * @param insuredSchema LCInsuredSchema 主被保险人信息
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema 保险险种计划
     * @param tLCInsuredListSet LCInsuredListSet
     * @return VData
     */
    private VData prepareContPlanData(String Riskcode ,String contIndex, String insuredIndex,
            LCInsuredSchema insuredSchema,
            LCGrpPolSchema tLCGrpPolSchema,
            LCInsuredListSet tLCInsuredListSet)
    {
        // 增加套餐编码要素
        mRiskWrapCode = null;

        if (tLCGrpPolSchema.getRiskWrapFlag() != null
                && tLCGrpPolSchema.getRiskWrapFlag().equals("Y"))
        {
//            mRiskWrapCode = tLCGrpPolSchema.getRiskWrapCode();
        }
        // ---------------------------------------------

        VData tNewVData = new VData();
        MMap tmpMap = new MMap();
        String strRiskCode = tLCGrpPolSchema.getRiskCode();
//        LCGrpPolSchema sLCGrpPolSchema = m_GrpPolImpInfo
//                .findLCGrpPolSchema(strRiskCode);
//        if (sLCGrpPolSchema == null)
//        {
//            buildError("prepareContPlanData", strRiskCode + "险种对应的集体投保单没有找到!");
//            return null;
//        }

        //拷贝一份，避免修改本地数据
        LCGrpPolSchema sLCGrpPolSchema = new LCGrpPolSchema();
        sLCGrpPolSchema.setSchema(tLCGrpPolSchema);

        String PolKey = m_GrpPolImpInfo.getPolKey(contIndex, insuredIndex,
                strRiskCode);
        //======UPD===zhangtao======2005-03-28=========BGN=========================
        boolean contFlag = false;
        LCContSchema contSchema = new LCContSchema();
        contSchema = m_GrpPolImpInfo.findLCContfromCache(contIndex);
        if (contSchema != null
                && !"".equals(StrTool.cTrim(contSchema.getContNo())))
        { //合同已经创建过
            contFlag = true;
        }

        //提交生成被保险人号,创建合同的sql
        VData tContData = m_GrpPolImpInfo.getContData(contIndex, insuredIndex,
        		sLCGrpPolSchema, tLCInsuredListSet,mLCGrpContSchema);
        if (tContData == null)
        {
            System.out.println("创建合同信息时出错!");
            return null;
        }
        MMap contMap = (MMap) tContData.getObjectByObjectName("MMap", 0);
        tmpMap.add(contMap);
        insuredSchema = (LCInsuredSchema) tContData.getObjectByObjectName(
                "LCInsuredSchema", 0);

        tNewVData.add(insuredSchema);
        contSchema = (LCContSchema) tContData.getObjectByObjectName(
                "LCContSchema", 0);
        if (!contFlag)
        {
            //合同第一次创建
            //设置主被保险人信息
            contSchema.setInsuredBirthday(insuredSchema.getBirthday());
            contSchema.setInsuredNo(insuredSchema.getInsuredNo());
            contSchema.setInsuredName(insuredSchema.getName());
            contSchema.setInsuredIDNo(insuredSchema.getIDNo());
            contSchema.setInsuredIDType(insuredSchema.getIDType());
            contSchema.setInsuredSex(insuredSchema.getSex());
        }
        contSchema.setPayIntv(this.mLCGrpContSchema.getPayIntv());
        contSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
        tNewVData.add(contSchema);
        //======UPD===zhangtao======2005-03-28=========END=========================
        //生成保单个人险种信息
//                LCPolSchema tLCPolSchema = formLCPolSchema(insuredSchema,
//                        tLCContPlanRiskSchema);
        LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
		for(int i=1;i<=tLCInsuredListSet.size();i++){
			if(insuredSchema.getInsuredNo().equals(tLCInsuredListSet.get(i).getInsuredNo())){
				tLCInsuredListSchema = tLCInsuredListSet.get(i);
			}
		}
        LCPolSchema tLCPolSchema = formLCPolSchema(insuredSchema,
        		sLCGrpPolSchema, contSchema,tLCInsuredListSchema);
//        //主险保单信息
//        String mainRiskCode = m_GrpPolImpInfo.getMainRiskCode(
//                tLCContPlanRiskSchema.getContPlanCode(), strRiskCode);

//        if (mainRiskCode.equals(""))
//        {
//            mainRiskCode = strRiskCode;
//            mainPolBL.setRiskCode(mainRiskCode);
//        }
        mainRiskCode = strRiskCode;
        String mainPolKey = m_GrpPolImpInfo.getPolKey(contIndex, insuredIndex,
                mainRiskCode);
        tLCPolSchema.setMainPolNo(mainPolKey);
        tLCPolSchema.setInsuredNo(insuredIndex);
        //填充所有保单信息
        VData polData = m_GrpPolImpInfo.formatLCPol(tLCPolSchema, PolKey);
        if (polData == null)
        {
            CError.buildErr(this, "出错了:", m_GrpPolImpInfo.mErrors);
            return null;
        }
        MMap polRelaMap = (MMap) polData.getObjectByObjectName("MMap", 0);
        tLCPolSchema = (LCPolSchema) polData.getObjectByObjectName(
                "LCPolSchema", 0);
        tLCPolSchema.setInsuredNo(insuredSchema.getInsuredNo());
        if (polRelaMap != null && polRelaMap.keySet().size() > 0)
        {
            tmpMap.add(polRelaMap);
        }

        //责任信息查询和生成
        /**
         * 将分成两种形式
         * 1\不是公共账户将走保险计划
         * 2\如果是公共账户将从险种描述中取出相应的描述
         */
        if (!StrTool.cTrim(this.PolTypeFlag).equals("G")
                && !StrTool.cTrim(this.PolTypeFlag).equals("C"))
        {
//            ContPlanQryBL contPlanQryBL = new ContPlanQryBL();
            VData tVData = new VData();
            tVData.add(sLCGrpPolSchema);
//            boolean b = contPlanQryBL.submitData(tVData, "");
//            if (!b || contPlanQryBL.mErrors.needDealError())
//            {
//                CError.buildErr(this, "查询计划要约出错:", contPlanQryBL.mErrors);
//                return null;
//            }
//            tVData = contPlanQryBL.getResult();
//            LCDutySet tDutySet = (LCDutySet) tVData.getObjectByObjectName(
//                    "LCDutySet", 0);
//            LCGetSet tGetSet = (LCGetSet) tVData.getObjectByObjectName(
//                    "LCGetSet", 0);
//            tNewVData.add(tGetSet);
//            by gzh 20101211 获取界面录入的年金开始领取年龄
			String parasql="select impartparammodle from lccustomerimpart where grpcontno='"+mGrpContNo+"' and impartcode = '010' and impartver = '008'";
			SSRS paraSSRS =mExeSQL.execSQL(parasql);
			if(paraSSRS.MaxRow>0){
				String[] paras = paraSSRS.GetText(1, 1).split(",");
				if("0".equals(insuredSchema.getSex())){
					getYear = paras[1];
				}else{
					getYear = paras[2];
				}
			}
			String typesql = "select impartparammodle from lccustomerimpart where grpcontno='"+mGrpContNo+"' and impartcode = '010' and impartver = '009'";
			SSRS typeSSRS =mExeSQL.execSQL(typesql);
			if(typeSSRS.MaxRow>0){
				String[] getDutyKinds = {"A","B","C","D"};
				String[] typeparas = typeSSRS.GetText(1, 1).split(",");
				for(int typecount=0;typecount<(typeparas.length-1);typecount++){
					if("Y".equals(typeparas[typecount])){
						getDutyKind = getDutyKinds[typecount];
					}
				}
			}
			
			LCDutySet tLCDutySet = getPerLCDutySetData(Riskcode,insuredSchema,tLCInsuredListSchema);
//            if (tLCDutySet == null)
//            {
//                LCDutySchema tDutySchema = (LCDutySchema) tVData
//                        .getObjectByObjectName("LCDutySchema", 0);
//                if (tDutySchema == null)
//                {
//                    CError.buildErr(this, "查询计划要约出错:责任信息不能为空");
//                    return null;
//                }
//                setPolInfoByDuty(tLCPolSchema, tDutySchema);
//                tDutySet = new LCDutySet();
//                tDutySet.add(tDutySchema);
//            }
            if (tLCDutySet == null || tLCDutySet.size() <= 0)
            {
                CError.buildErr(this, strRiskCode + "要约信息生成错误出错");
                return null;
            }
            /**
             * 将险种的交费频次存入责任中
             */
            if (tLCPolSchema.getPayIntv() > 0)
            {
                for (int i = 1; i <= tLCDutySet.size(); i++)
                {
                	tLCDutySet.get(i).setPayIntv(
                            this.mLCGrpContSchema.getPayIntv());
                }
            }
//            //2006-10-19算费修改
//            String tSqlCalValue = " select calfactorvalue from lccontplandutyparam"
//                    + " where grpcontno = '"
//                    + tLCContPlanRiskSchema.getGrpContNo()
//                    + "'"
//                    + " and contplancode = '"
//                    + tLCContPlanRiskSchema.getContPlanCode()
//                    + "'"
//                    + " and riskcode = '"
//                    + tLCContPlanRiskSchema.getRiskCode()
//                    + "'" + " and calfactor = 'CalRule' with ur";
//
//            if (new ExeSQL().getOneValue(tSqlCalValue).equals("4"))
//            {
//                LCInsuredListPolDB tLCInsuredListPolDB = new LCInsuredListPolDB();
//                tLCInsuredListPolDB.setRiskCode(tLCContPlanRiskSchema
//                        .getRiskCode());
//                tLCInsuredListPolDB.setInsuredID(insuredIndex);
//                tLCInsuredListPolDB.setGrpContNo(this.mGrpContNo);
//                LCInsuredListPolSet tLCInsuredListPolSet = tLCInsuredListPolDB
//                        .query();
//                if (tLCInsuredListPolSet != null
//                        && tLCInsuredListPolSet.size() > 0)
//                {
//                    InputPrem = Arith.round(
//                            Double.parseDouble(tLCInsuredListPolSet.get(1)
//                                    .getPrem()), 2);
//                    InputAmnt = Arith.round(
//                            Double.parseDouble(tLCInsuredListPolSet.get(1)
//                                    .getAmnt()), 2);
//                }
//            }
//            else
//            {
//                InputPrem = 0.00;
//                InputAmnt = 0.00;
//            }
            /**
             * 如果保险计划中录入了保费就将保费存入
             * @author yangming
             */
//            if (this.InputPrem > 0)
//            {
//                int dutyCount = tDutySet.size();
//                if (dutyCount > 1)
//                {
//                    CError.buildErr(this, strRiskCode + "约定差异费率不支持复杂险种导入(多责任)");
//                    return null;
//                }
//                tDutySet.get(1).setPrem(this.InputPrem);
//            }
//            if (this.InputAmnt > 0)
//            {
//                int dutyCount = tDutySet.size();
//                if (dutyCount > 1)
//                {
//                    CError.buildErr(this, strRiskCode + "约定差异费率不支持复杂险种导入(多责任)");
//                    return null;
//                }
//                tDutySet.get(1).setAmnt(this.InputAmnt);
//            }
            /** 无名单保费计算,保费将去保险计划中的保费 */
//            if (this.PolTypeFlag != null && this.PolTypeFlag.equals("1"))
//            {
//                InputPrem = tLCContPlanRiskSchema.getRiskPrem();
//                if (this.InputPrem > 0)
//                {
//                    int dutyCount = tDutySet.size();
//                    double calPrem = 0.0;
//                    double prePrem = this.InputPrem / dutyCount;
//                    BigDecimal prem = new BigDecimal(prePrem);
//                    double doublePrePrem = prem.setScale(2,
//                            BigDecimal.ROUND_DOWN).doubleValue();
//                    for (int i = 1; i <= tDutySet.size(); i++)
//                    {
//                        if (!CalRule.equals(""))
//                        {
//                            tDutySet.get(i).setCalRule(this.CalRule);
//                        }
//                        if (i == tDutySet.size())
//                        {
//                            doublePrePrem = InputPrem - calPrem;
//                        }
//                        tDutySet.get(i).setPrem(doublePrePrem);
//                        calPrem += doublePrePrem;
//                    }
//                }
//            }
            /**
             * 将险种的交费频次存入责任
             */
            tNewVData.add(tLCDutySet);
            //保费
            LCPremSet tPremSet = new LCPremSet();
            tPremSet = (LCPremSet) tVData.getObjectByObjectName("LCPremSet", 0);
            tNewVData.add(tPremSet);
        }
        else
        {
            LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
            tLMRiskDutyDB.setRiskCode(Riskcode);
            tLMRiskDutyDB.setSpecFlag(this.PolTypeFlag);
            LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
            if (tLMRiskDutySet.size() <= 0)
            {
                buildError("prepareContPlanData", strRiskCode
                        + "险种对应账户的描述没有找到!");
                return null;
            }
            LCDutySet tLCDutySet = new LCDutyBLSet();
            for (int i = 1; i <= tLMRiskDutySet.size(); i++)
            {
                LMDutyDB tLMDutyDB = new LMDutyDB();
                tLMDutyDB.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
                if (!tLMDutyDB.getInfo())
                {
                    System.out
                            .println("ParseGuideIn.prepareContPlanData(contIndex, insuredIndex, insuredSchema, tLCContPlanRiskSchema)  \n--Line:1951  --Author:YangMing");
                    buildError("prepareContPlanData", strRiskCode
                            + "险种对应账户的描述没有找到!");
                    return null;
                }
                Reflections ref = new Reflections();
                LCDutySchema tLCDutySchema = new LCDutySchema();
                ref.transFields(tLCDutySchema, tLMDutyDB.getSchema());
                tLCDutySchema.setDutyCode(tLMDutyDB.getDutyCode());
                tLCDutySchema.setPrem(this.PublicAcc);
                tLCDutySchema.setCalRule("3");
                if (tLMRiskDutySet.size() == 1)
                {
                    tNewVData.add(tLCDutySchema);
                }
                tLCDutySet.add(tLCDutySchema);
            }
            tNewVData.add(tLCDutySet);
        }
        //连身被保险人信息
        //处理连身被保险人
        String[] relaIns = null;
        relaIns = m_GrpPolImpInfo.getInsuredRela(contIndex, insuredIndex,
                strRiskCode);
        if (relaIns != null)
        {
            LCInsuredRelatedSet tRelaInsSet = m_GrpPolImpInfo
                    .getInsuredRelaData(tLCPolSchema.getInsuredNo(), relaIns);
            if (tRelaInsSet == null)
            {
                //创建连带被保险人出错
                mErrors.copyAllErrors(m_GrpPolImpInfo.mErrors);
                return null;
            }
            tNewVData.add(tRelaInsSet);
        }

        //受益人信息
        LCBnfSet tLCBnfSet = new LCBnfSet();
        //主被保险人受益人
        LCBnfSet lLCBnfSet = m_GrpPolImpInfo.getBnfSet(PolKey);
        if (lLCBnfSet != null && lLCBnfSet.size() > 0)
        {
            tLCBnfSet.add(lLCBnfSet);
        }
        //连带被保险人受益人
        if (relaIns != null)
        {
            for (int i = 0; i < relaIns.length; i++)
            {
                String tmpPolKey = m_GrpPolImpInfo.getPolKey(contIndex,
                        relaIns[i], strRiskCode);
                LCBnfSet tmpLCBnfSet = m_GrpPolImpInfo.getBnfSet(tmpPolKey);
                if (tmpLCBnfSet != null && tmpLCBnfSet.size() > 0)
                {
                    tLCBnfSet.add(tmpLCBnfSet);
                }
            }
        }
        if (tLCBnfSet != null && tLCBnfSet.size() > 0)
        {
            VData tr = m_GrpPolImpInfo.perareBnf(tLCBnfSet, insuredSchema,
                    tLCGrpPolSchema, insuredIndex);

            MMap tmap = (MMap) tr.getObjectByObjectName("MMap", 0);
            if (tmap != null)
            {
                tmpMap.add(tmap);
            }
            tLCBnfSet = (LCBnfSet) tr.getObjectByObjectName("LCBnfSet", 0);
            tNewVData.add(tLCBnfSet);

        }

        //加入对应险种的集体投保单信息,险种承保描述信息
        LMRiskAppSchema tLMRiskAppSchema = m_GrpPolImpInfo
                .findLMRiskAppSchema(strRiskCode);
        if (tLMRiskAppSchema == null)
        {
            buildError("prepareContPlanData", strRiskCode + "险种对应的险种承保描述没有找到!");
            return null;
        }
        LMRiskSchema tLMRiskSchema = m_GrpPolImpInfo
                .findLMRiskSchema(strRiskCode);
        if (tLMRiskSchema == null)
        {
            buildError("prepareContPlanData", strRiskCode + "险种对应的险种承保描述没有找到!");
            return null;
        }

        LDGrpSchema ttLDGrpSchema = m_GrpPolImpInfo
                .findLDGrpSchema(tLCGrpPolSchema.getCustomerNo());
        if (ttLDGrpSchema == null)
        {
            buildError("prepareContPlanData", tLCGrpPolSchema.getCustomerNo()
                    + "对应的集体客户信息没有找到!");
            return null;
        }

        String Industry = getIndustry(this.mLCGrpContSchema.getBusinessType());
        String InsuredState = insuredSchema.getInsuredStat();
        //其他信息
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("samePersonFlag", "0");
        tTransferData.setNameAndValue("GrpImport", 1);
        tTransferData.setNameAndValue("ID", PolKey);
        tTransferData.setNameAndValue("PolKey", PolKey);
        tTransferData.setNameAndValue("EdorNo", mEdorNo);
        tTransferData.setNameAndValue("EdorType", mEdorType);
        tTransferData.setNameAndValue("RetireRate", retire);
        tTransferData.setNameAndValue("AvgAge", avgage);
        tTransferData.setNameAndValue("InsuredNum", InsuredNum);
        tTransferData.setNameAndValue("Industry", Industry);
        tTransferData.setNameAndValue("InsuredState", InsuredState);
        tTransferData.setNameAndValue("GetDutyKind", getDutyKind);
        // 增加“领取频次”要素
        //tTransferData.setNameAndValue("GrpGetIntv", mGrpGetIntv);
        // -----------------------

        // 增加“套单编码”要素
        if (mRiskWrapCode != null)
        {
            tTransferData.setNameAndValue("RiskWrapCode", mRiskWrapCode);
        }
        // -----------------------

        tNewVData.add(mGlobalInput);
        tNewVData.add(tLCGrpPolSchema);
        tNewVData.add(tLMRiskAppSchema);
        tNewVData.add(tLMRiskSchema);
        tNewVData.add(ttLDGrpSchema);
        tNewVData.add(m_GrpPolImpInfo.getLCGrpAppntSchema());
        tNewVData.add(tLCPolSchema);
        tNewVData.add(tTransferData);
        tNewVData.add(this.mainPolBL);
        tNewVData.add(this.mLCGrpContSchema);
        tNewVData.add(tmpMap);

        mRiskWrapCode = null;

        return tNewVData;
    }

    /**
     * getIndustry
     *
     * @param tIndustry String
     * @return String
     */
    private String getIndustry(String tIndustry)
    {
        String sql = "select codealias from ldcode where codetype='businesstype' and code='"
                + tIndustry + "'";
        System.out.println("+++++++" + (new ExeSQL()).getOneValue(sql));
        if (StrTool.cTrim((new ExeSQL()).getOneValue(sql)).equals(""))
        {
            return "";
        }
        else
        {
            return (new ExeSQL()).getOneValue(sql);
        }
    }

    /**
     * getInsuredNum
     *
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema
     * @return String
     */
    private String getInsuredNum(LCContPlanRiskSchema tLCContPlanRiskSchema)
    {
        boolean needQuery = false;
        if (this.currentContPlan_InsuredNum == null
                || !currentContPlan_InsuredNum.equals(tLCContPlanRiskSchema
                        .getContPlanCode()))
        {
            currentContPlan_InsuredNum = tLCContPlanRiskSchema
                    .getContPlanCode();
            needQuery = true;
        }
        if (!needQuery)
        {
            return this.InsuredNum;
        }
        String strSql = "select count(1) from lcinsuredlist where grpcontno='"
                + this.mGrpContNo + "' and contplancode='"
                + tLCContPlanRiskSchema.getContPlanCode() + "'";

        return (new ExeSQL()).getOneValue(strSql);
    }

    /**
     * getInsuredAvgAge
     *
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema
     * @return String
     */
    private String getInsuredAvgAge(LCContPlanRiskSchema tLCContPlanRiskSchema)
    {
        boolean needQuery = false;
        if (this.currentContPlan_avgage == null
                || !currentContPlan_avgage.equals(tLCContPlanRiskSchema
                        .getContPlanCode()))
        {
            currentContPlan_avgage = tLCContPlanRiskSchema.getContPlanCode();
            needQuery = true;
        }
        if (!needQuery)
        {
            return this.avgage;
        }

        String strSql = "select round(avg((to_date(a.CValiDate)-to_date(b.birthday))/365),2) from "
                + " lcinsuredlist b,lcgrpcont a where a.grpcontno=b.grpcontno and a.grpcontno='"
                + this.mGrpContNo
                + "'and b.contplancode='"
                + tLCContPlanRiskSchema.getContPlanCode() + "'";
        String AvgAge = (new ExeSQL()).getOneValue(strSql);
        return AvgAge;
    }

    /**
     * getRetire
     *
     * @return Double
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema
     */
    private String getRetire(LCContPlanRiskSchema tLCContPlanRiskSchema)
    {
        boolean needQuery = false;
        if (this.currentContPlan_retire == null
                || !currentContPlan_retire.equals(tLCContPlanRiskSchema
                        .getContPlanCode()))
        {
            currentContPlan_retire = tLCContPlanRiskSchema.getContPlanCode();
            needQuery = true;
        }
        if (!needQuery)
        {
            return retire;
        }
        if (tLCContPlanRiskSchema.getContPlanCode().equals("11")
                || tLCContPlanRiskSchema.getContPlanCode().equals("00"))
        {
            return retire;
        }
        String insured = "select count(1) from lcinsuredlist where grpcontno='"
                + this.mGrpContNo + "' and contplancode = '"
                + tLCContPlanRiskSchema.getContPlanCode() + "'";
        String no = (new ExeSQL()).getOneValue(insured);
        System.out.println("ExeSQL : " + insured);
        if (StrTool.cTrim(no).equals("") || StrTool.cTrim(no).equals("null")
                || StrTool.cTrim(no).equals("0"))
        {
            buildError("getRetire", "参保人数为空！");
            return "0";
        }
        String sql = "select DECIMAL((select count(1) from lcinsuredlist where retire='2' and grpcontno='"
                + this.mGrpContNo
                + "' and contplancode='"
                + tLCContPlanRiskSchema.getContPlanCode()
                + "'),12,2)/"
                + no
                + " from dual";
        String retire = (new ExeSQL()).getOneValue(sql);
        return retire;
    }

    /**
     * 生成保单信息
     *
     * @param insuredSchema LCInsuredSchema
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema
     * @param contSchema LCContSchema
     * @return LCPolSchema
     */
    private LCPolSchema formLCPolSchema(LCInsuredSchema insuredSchema,
    		LCGrpPolSchema tLCGrpPolSchema, LCContSchema contSchema,LCInsuredListSchema tLCInsuredListSchema)
    {

        LCPolSchema tLCPolSchema = new LCPolSchema();
        String strRiskCode = tLCGrpPolSchema.getRiskCode();
//        String Contplancode = tLCGrpPolSchema.getContPlanCode();
//        LCGrpPolSchema tLCGrpPolSchema = m_GrpPolImpInfo
//                .findLCGrpPolSchema(strRiskCode);

        if (tLCGrpPolSchema == null)
        {
            buildError("formatLCPol", "找不到指定险种所对应的集体保单，险种为：" + strRiskCode);
            return null;
        }

        tLCPolSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
        tLCPolSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        if ("Y".equals(tLCPolSchema.getSpecifyValiDate())
                && tLCPolSchema.getCValiDate() != null)
        {
            //如果磁盘倒入指定生效日期，且生效日期字段有值,那么就用 生效日期字段的值
        }
        else
        {
            //否则一律用集体单的生效日期
            tLCPolSchema.setCValiDate(contSchema.getCValiDate());
        }

        tLCPolSchema.setManageCom(tLCGrpPolSchema.getManageCom());
        tLCPolSchema.setSaleChnl(tLCGrpPolSchema.getSaleChnl());
        tLCPolSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
        tLCPolSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
        tLCPolSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
        tLCPolSchema.setAgentCode1(tLCGrpPolSchema.getAgentCode1());
        tLCPolSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
        tLCPolSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
        //tLCPolSchema.set
        tLCPolSchema.setContType("2");
        //        tLCPolSchema.setPolTypeFlag("2");
        tLCPolSchema.setPolTypeFlag(contSchema.getPolType());

        // 根据合同表帐户类型标志，对应险种帐户类型标志。
        tLCPolSchema.setAccType(contSchema.getAccType());
        // --------------------------------------

        tLCPolSchema.setInsuredPeoples(contSchema.getPeoples());
        tLCPolSchema.setRiskCode(strRiskCode);
        tLCPolSchema.setContNo(insuredSchema.getContNo());
        tLCPolSchema.setInsuredSex(insuredSchema.getSex());
        tLCPolSchema.setInsuredBirthday(insuredSchema.getBirthday());
        tLCPolSchema.setInsuredName(insuredSchema.getName());
//        tLCPolSchema.setContPlanCode(Contplancode);
        tLCPolSchema.setInsuredNo(insuredSchema.getInsuredNo());
        
//       添加lcpolshema归属规则信息
        if(!StrTool.cTrim(tLCInsuredListSchema.getAscriptionRuleCode()).equals("")){
        	tLCPolSchema.setAscriptionRuleCode(tLCInsuredListSchema.getAscriptionRuleCode());
        }else{
        	String AscriptionRuleCodeSql = "select Ascriptionrulecode from lcAscriptionrulefactory where grpcontno='"+tLCGrpPolSchema.getGrpContNo()+"'";
            ExeSQL tExeSQL  = new ExeSQL();
            SSRS AscriptionRuleCodeSSRS = tExeSQL.execSQL(AscriptionRuleCodeSql);
            if(AscriptionRuleCodeSSRS.getMaxRow()>0){
            	tLCPolSchema.setAscriptionRuleCode(AscriptionRuleCodeSSRS.GetText(1, 1));
            }else{
            	buildError("formatLCPol", "归属规则为空!");
                return null;
            }
        }
        return tLCPolSchema;

    }

    public String getExtendFileName(String aFileName)
    {
        File tFile = new File(aFileName);
        String aExtendFileName = "";
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (i < 1)
            {
                i = 1;
            }
            if (name.substring(i - 1, i).equals("."))
            {
                aExtendFileName = name.substring(i, name.length());
                System.out.println("ExtendFileName;" + aExtendFileName);
                return aExtendFileName;
            }
        }
        return aExtendFileName;
    }

    /**
     * 字符串替换
     *
     * @param s1 String
     * @param OriginStr String
     * @param DestStr String
     * @return java.lang.String
     */
    public static String replace(String s1, String OriginStr, String DestStr)
    {
        s1 = s1.trim();
        int mLenOriginStr = OriginStr.length();
        for (int j = 0; j < s1.length(); j++)
        {
            int befLen = s1.length();
            if (s1.substring(j, j + 1) == null
                    || s1.substring(j, j + 1).trim().equals(""))
            {
                continue;
            }
            else
            {
                if (OriginStr != null && DestStr != null)
                {
                    if (j + mLenOriginStr <= s1.length())
                    {

                        if (s1.substring(j, j + mLenOriginStr)
                                .equals(OriginStr))
                        {

                            OriginStr = s1.substring(j, j + mLenOriginStr);

                            String startStr = s1.substring(0, j);
                            String endStr = s1.substring(j + mLenOriginStr, s1
                                    .length());

                            s1 = startStr + DestStr + endStr;

                            j = j + s1.length() - befLen;
                            if (j < 0)
                            {
                                j = 0;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return s1;
    }

    /**
     * 得到日志显示结果
     *
     * @return com.sinosoft.utility.VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        ParseGuideInUI tPGI = new ParseGuideInUI();

        //    try
        //    {
        //      tPGI.parseVts();
        //    }
        //    catch(Exception e)
        //    {
        //      e.printStackTrace();
        //
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        //    tTransferData.setNameAndValue("Flag", "BQ");
        //    tTransferData.setNameAndValue("EdorType", "NI");
        tTransferData.setNameAndValue("GrpContNo", "1400003828");
        //    tTransferData.setNameAndValue("EdorNo", "20051017000003");
        //    tTransferData.setNameAndValue("EdorValiDate", "2005-10-17");
        //    tTransferData.setNameAndValue("FileName", "10人A计划.xls");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "UW0001";
        tG.ManageCom = "86110000";

        tVData.add(tTransferData);
        tVData.add(tG);
        // tPGI.outputDocumentToFile("E:/","ProcedureXml1");
        boolean ress = tPGI.submitData(tVData, "INSERT||DATABASE");
        if (ress)
        {
            System.out.println(tPGI.mErrors.getFirstError());
        }
        else
        {
            System.out.println(tPGI.mErrors.getFirstError());
        }

    }

    /**
     * Build a instance document object for function transfromNode()
     */
    private void bulidDocument()
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);

        try
        {
            m_doc = dfactory.newDocumentBuilder().newDocument();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Detach node from original document, fast XPathAPI process.
     *
     * @param node Node
     * @return org.w3c.dom.Node
     * @throws Exception
     */
    private org.w3c.dom.Node transformNode(org.w3c.dom.Node node)
            throws Exception
    {
        Node nodeNew = m_doc.importNode(node, true);

        return nodeNew;
    }

    /**
     * 保单数据准备(按险种单导入的时候使用)
     * @param aVData VData
     * @return VData 返回数据集，包含提交给ProposalBL需要用到的信息。
     */
    private VData prepareProposalBLData(VData aVData)
    {
        VData tNewVData = new VData();
        MMap tmpMap = new MMap();
        TransferData tTransferData = (TransferData) aVData
                .getObjectByObjectName("TransferData", 0);
        LCPolSchema tLCPolSchema = (LCPolSchema) aVData.getObjectByObjectName(
                "LCPolSchema", 0);
        LCPremSet tLCPremSet = (LCPremSet) aVData.getObjectByObjectName(
                "LCPremSet", 0);
        LCPremToAccSet tLCPremToAccSet = (LCPremToAccSet) aVData
                .getObjectByObjectName("LCPremToAccSet", 0);
        LCDutySet tLCDutySet = (LCDutySet) aVData.getObjectByObjectName(
                "LCDutySet", 0);
        //险种ID
        String strID = (String) tTransferData.getValueByName("ID");

        //备份原ID号
        String strRiskCode = tLCPolSchema.getRiskCode();
        String InsuredId = tLCPolSchema.getInsuredNo();
        String contId = tLCPolSchema.getContNo();
        //导入的时候使用appflag字段缓存连带被保险人ID信息
        String relaInsId = tLCPolSchema.getAppFlag();

        //查询集体保单信息
        LCGrpPolSchema sLCGrpPolSchema = m_GrpPolImpInfo
                .findLCGrpPolSchema(strRiskCode);
        if (sLCGrpPolSchema == null)
        {
            buildError("prepareProposalBLData", strRiskCode + "险种对应的集体投保单没有找到!");
            return null;
        }
        //拷贝一份，避免修改本地数据
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema.setSchema(sLCGrpPolSchema);
        boolean mainPolFlag = false;
        if (strID.equals(tLCPolSchema.getMainPolNo()))
        {
            //  mainRiskCode = tLCPolSchema.getRiskCode() ;
            mainPolFlag = true;
        }
        /*
         mainRiskCode = tLCPolSchema.getRiskCode();
         if (!tLCPolSchema.getMainPolNo().equals("")
         && !tLCPolSchema.getMainPolNo().equals( strID ))
         {
         LCPolSchema tMainLCPolSchema = m_GrpPolImpInfo.findMainLCPolSchema(
         tLCPolSchema.getMainPolNo());
         if (tMainLCPolSchema == null)
         {
         CError.buildErr(this,
         "查找主险保单[" + tLCPolSchema.getMainPolNo() + "]失败");
         return null;
         }
         mainRiskCode = tMainLCPolSchema.getRiskCode();
         }
         */
        //格式化保单信息
        VData tPolData = m_GrpPolImpInfo.formatLCPol(tLCPolSchema, strID);
        if (tPolData == null)
        {
            mErrors.copyAllErrors(m_GrpPolImpInfo.mErrors);
            return null;
        }
        tLCPolSchema = (LCPolSchema) tPolData.getObjectByObjectName(
                "LCPolSchema", 0);
        if (!mainPolFlag)
        {
            LCPolSchema mainPolSchema = m_GrpPolImpInfo
                    .findMainPolSchema(tLCPolSchema.getMainPolNo());
            if (mainPolSchema == null)
            {
                CError.buildErr(this, "查找主险保单[" + strID + "]失败");
                return null;
            }
            mainPolBL.setSchema(mainPolSchema);
        }
        else
        {
            mainPolBL.setSchema(tLCPolSchema);
        }

        MMap poldataMap = (MMap) tPolData.getObjectByObjectName("MMap", 0);
        if (poldataMap != null)
        {
            tmpMap.add(poldataMap);
        }

        //被保险人、合同信息
        LCInsuredSchema insuredSchema = m_GrpPolImpInfo
                .findInsuredfromCache(InsuredId);
        if (insuredSchema == null)
        {
            CError.buildErr(this, "未找到被保险人[" + InsuredId + "]");
            return null;
        }
        LCContSchema contSchema = m_GrpPolImpInfo.findLCContfromCache(contId);
        if (contSchema == null)
        {
            CError.buildErr(this, "未找到合同[" + contId + "]");
            return null;
        }
        //设置主被保险人信息
        contSchema.setInsuredBirthday(insuredSchema.getBirthday());
        contSchema.setInsuredNo(insuredSchema.getInsuredNo());
        contSchema.setInsuredName(insuredSchema.getName());
        contSchema.setInsuredIDNo(insuredSchema.getIDNo());
        contSchema.setInsuredIDType(insuredSchema.getIDType());
        contSchema.setInsuredSex(insuredSchema.getSex());

        //责任信息查询和生成

        LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setMainRiskCode(mainPolBL.getRiskCode());
        tLCContPlanRiskSchema.setGrpContNo(this.mGrpContNo);
        tLCContPlanRiskSchema.setRiskCode(tLCPolSchema.getRiskCode());
        //00-默认值
        tLCContPlanRiskSchema.setContPlanCode("00");
        tLCContPlanRiskSchema.setProposalGrpContNo(this.mGrpContNo);

        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        tLCContPlanRiskDB.setSchema(tLCContPlanRiskSchema);

        LCDutySet tempDutySet = null;
        //如果没有默认值，则取excel中提交的数据
        if (!tLCContPlanRiskDB.getInfo())
        {
            if (tLCDutySet != null && tLCDutySet.size() > 0)
            {
                //责任项页签中有数据
                for (int j = 1; j <= tLCDutySet.size(); j++)
                {
                    if ("0".equals(tLCDutySet.get(j).getCalRule())
                            || "1".equals(tLCDutySet.get(j).getCalRule()))
                    {

                        CError.buildErr(this, "责任项对应计算规则为表定费率或统一费率，但没有查到默认值");
                        return null;
                    }
                }
                tempDutySet = tLCDutySet;
            }
            else
            {
                //责任项页签没有数据，则用险种保单页签的数据
                LCDutySchema ntempDutySchema = new LCDutySchema();
                setDutyByPolInfo(ntempDutySchema, tLCPolSchema);
                tempDutySet = new LCDutySet();
                tempDutySet.add(ntempDutySchema);
            }
        }
        else
        {
            //如果有默认值，则需要查询默认值
            ContPlanQryBL contPlanQryBL = new ContPlanQryBL();
            VData tVData = new VData();
            tVData.add(tLCContPlanRiskSchema);
            boolean b = contPlanQryBL.submitData(tVData, "");
            if (!b || contPlanQryBL.mErrors.needDealError())
            {
                CError.buildErr(this, "查询计划要约出错:", contPlanQryBL.mErrors);
                return null;
            }
            tVData = contPlanQryBL.getResult();
            tempDutySet = (LCDutySet) tVData.getObjectByObjectName("LCDutySet",
                    0);
            if (tempDutySet == null)
            {
                //只有一条责任项，以险种保单页签的数据为准
                LCDutySchema ttempDutySchema = (LCDutySchema) tVData
                        .getObjectByObjectName("LCDutySchema", 0);

                ///setPolInfoByDuty(tLCPolSchema, ttempDutySchema);
                setDutyByPolInfo(ttempDutySchema, tLCPolSchema);
                tempDutySet = new LCDutySet();
                tempDutySet.add(ttempDutySchema);

            }
            //有多条责任项，以责任项页签的数据为准
            for (int i = 1; i <= tLCDutySet.size(); i++)
            {
                for (int j = 1; j <= tempDutySet.size(); j++)
                {
                    if (tempDutySet.get(j).getDutyCode().equals(
                            tLCDutySet.get(i).getDutyCode()))
                    {
                        if (tLCDutySet.get(i).getMult() > 0)
                        {
                            tempDutySet.get(j).setMult(
                                    tLCDutySet.get(i).getMult());
                        }
                        if (tLCDutySet.get(i).getPrem() > 0)
                        {
                            tempDutySet.get(j).setPrem(
                                    tLCDutySet.get(i).getPrem());
                        }
                        if (tLCDutySet.get(i).getAmnt() > 0)
                        {
                            tempDutySet.get(j).setAmnt(
                                    tLCDutySet.get(i).getAmnt());
                        }
                        if (tLCDutySet.get(i).getPayIntv() > 0)
                        {
                            tempDutySet.get(j).setPayIntv(
                                    tLCDutySet.get(i).getPayIntv());
                        }
                        if (tLCDutySet.get(i).getInsuYear() > 0)
                        {
                            tempDutySet.get(j).setInsuYear(
                                    tLCDutySet.get(i).getInsuYear());
                        }

                        if (!"".equals(StrTool.cTrim(tLCDutySet.get(i)
                                .getInsuYearFlag())))
                        {
                            tempDutySet.get(j).setInsuYearFlag(
                                    tLCDutySet.get(i).getInsuYearFlag());
                        }

                        if (tLCDutySet.get(i).getPayEndYear() > 0)
                        {
                            tempDutySet.get(j).setPayEndYear(
                                    tLCDutySet.get(i).getPayEndYear());
                        }
                        if (!"".equals(StrTool.cTrim(tLCDutySet.get(i)
                                .getPayEndYearFlag())))
                        {
                            tempDutySet.get(j).setPayEndYearFlag(
                                    tLCDutySet.get(i).getPayEndYearFlag());
                        }
                        if (tLCDutySet.get(i).getGetYear() > 0)
                        {
                            tempDutySet.get(j).setGetYear(
                                    tLCDutySet.get(i).getGetYear());
                        }

                        if (!"".equals(StrTool.cTrim(tLCDutySet.get(i)
                                .getGetYearFlag())))
                        {
                            tempDutySet.get(j).setGetYearFlag(
                                    tLCDutySet.get(i).getGetYearFlag());
                        }

                        if (!"".equals(StrTool.cTrim(tLCDutySet.get(i)
                                .getGetStartType())))
                        {
                            tempDutySet.get(j).setGetStartType(
                                    tLCDutySet.get(i).getGetStartType());
                        }

                        //计算方向
                        if (!"".equals(StrTool.cTrim(tLCDutySet.get(i)
                                .getPremToAmnt())))
                        {
                            tempDutySet.get(j).setPremToAmnt(
                                    tLCDutySet.get(i).getPremToAmnt());
                        }
                        //计算规则 被缓存在 最终核保人编码 字段
                        if (!"".equals(StrTool.cTrim(tLCDutySet.get(i)
                                .getCalRule())))
                        {
                            tempDutySet.get(j).setCalRule(
                                    tLCDutySet.get(i).getCalRule());
                        }
                        //费率
                        if (tLCDutySet.get(i).getFloatRate() > 0)
                        {
                            if ("0".equals(tempDutySet.get(j).getCalRule())
                                    || "1".equals(tempDutySet.get(j)
                                            .getCalRule()))
                            {
                                //计算规则为 0-表定费率 或 1-统一费率 时
                                //费率必须是从默认值中取得
                            }
                            else
                            {
                                tempDutySet.get(j).setFloatRate(
                                        tLCDutySet.get(i).getFloatRate());
                            }
                        }
                        //免赔额
                        if (tLCDutySet.get(i).getGetLimit() > 0)
                        {
                            tempDutySet.get(j).setGetLimit(
                                    tLCDutySet.get(i).getGetLimit());
                        }
                        //赔付比例
                        if (tLCDutySet.get(i).getGetRate() > 0)
                        {
                            tempDutySet.get(j).setGetRate(
                                    tLCDutySet.get(i).getGetRate());
                        }

                        break;
                    }
                }
            }
        }
        //清空借用字段
        tLCPolSchema.setAppFlag("");
        tLCPolSchema.setUWCode("");
        tLCPolSchema.setApproveFlag("");
        tLCPolSchema.setUWFlag("");

        //处理连身被保险人
        LCInsuredRelatedSet tRelaInsSet = null;
        String[] relaIns = null;
        if (!"".equals(StrTool.cTrim(relaInsId)))
        {
            relaIns = relaInsId.split(",");
            if (relaIns == null)
            {
                relaInsId.split(";");
            }
            if (relaIns != null)
            {
                tRelaInsSet = m_GrpPolImpInfo.getInsuredRelaData(tLCPolSchema
                        .getInsuredNo(), relaIns);
                if (tRelaInsSet == null)
                {
                    //创建连带被保险人出错
                    mErrors.copyAllErrors(m_GrpPolImpInfo.mErrors);
                    return null;
                }

            }
        }

        String PolKey = m_GrpPolImpInfo.getPolKey(contId, InsuredId,
                strRiskCode);
        tTransferData.setNameAndValue("PolKey", PolKey);

        //受益人信息
        LCBnfSet tLCBnfSet = new LCBnfSet();
        tLCBnfSet = m_GrpPolImpInfo.getBnfSetByPolId(strID);
        if (tLCBnfSet != null && tLCBnfSet.size() > 0)
        {
            VData tr = m_GrpPolImpInfo.perareBnf(tLCBnfSet, insuredSchema,
                    tLCGrpPolSchema, InsuredId);

            MMap tmap = (MMap) tr.getObjectByObjectName("MMap", 0);
            if (tmap != null)
            {
                tmpMap.add(tmap);
            }
            tLCBnfSet = (LCBnfSet) tr.getObjectByObjectName("LCBnfSet", 0);
        }

        tTransferData.setNameAndValue("samePersonFlag", 0);
        tTransferData.setNameAndValue("GrpImport", 1); //磁盘投保标志
        //险种描述信息
        LMRiskAppSchema tLMRiskAppSchema = m_GrpPolImpInfo
                .findLMRiskAppSchema(strRiskCode);
        if (tLMRiskAppSchema == null)
        {
            buildError("prepareData", strRiskCode + "险种对应的险种承保描述没有找到!");
            return null;
        }
        LMRiskSchema tLMRiskSchema = m_GrpPolImpInfo
                .findLMRiskSchema(strRiskCode);
        if (tLMRiskSchema == null)
        {
            buildError("prepareData", strRiskCode + "险种对应的险种承保描述没有找到!");
            return null;
        }

        LDGrpSchema ttLDGrpSchema = m_GrpPolImpInfo
                .findLDGrpSchema(tLCGrpPolSchema.getCustomerNo());
        if (ttLDGrpSchema == null)
        {
            buildError("prepareData", tLCGrpPolSchema.getCustomerNo()
                    + "对应的集体信息没有找到!");
            return null;
        }

        tNewVData.add(tTransferData);
        tNewVData.add(mGlobalInput);

        //        if (tLCDutySet.size() > 1)
        //        {
        //            tNewVData.add(tLCDutySet);
        //        }
        //        else if (tLCDutySet.size() == 1)
        //        {
        //            setPolInfoByDuty(tLCPolSchema, tLCDutySet.get(1));
        //            tNewVData.add(tLCDutySet.get(1));
        //        }
        //        else
        //        {
        //            tNewVData.add(new LCDutySchema());
        //        }
        if (tempDutySet == null || tempDutySet.size() <= 0)
        {
            CError.buildErr(this, "险种[" + strRiskCode + "]责任不能为空");
            return null;
        }
        tNewVData.add(tempDutySet);
        if (tRelaInsSet != null)
        {
            tNewVData.add(tRelaInsSet);
        }
        tNewVData.add(tLCPolSchema);
        tNewVData.add(tLCPremSet);
        tNewVData.add(tLCPremToAccSet);
        tNewVData.add(contSchema);
        tNewVData.add(insuredSchema);
        tNewVData.add(tLCBnfSet);

        tNewVData.add(tLCGrpPolSchema);
        tNewVData.add(m_GrpPolImpInfo.getLCGrpAppntSchema());
        tNewVData.add(tLMRiskAppSchema);
        tNewVData.add(tLMRiskSchema);
        tNewVData.add(ttLDGrpSchema);
        tNewVData.add(mainPolBL);
        tNewVData.add(tmpMap);

        return tNewVData;
    }

    /**
     * 创建错误日志
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ParseGuideIn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        this.logErrors.addOneError(cError);
    }

    /**
     * 记录日志信息
     * @param contId String
     * @param insuredId String
     * @param polId String
     * @param contplancode String
     * @param riskcode String
     * @param insuredSchema LCInsuredSchema
     */
    private void logError(String contId, String insuredId, String polId,
            String contplancode, String riskcode, LCInsuredSchema insuredSchema)
    {
        if (m_GrpPolImpInfo.mErrors.getErrorCount() >= 0)
        {
            this.mErrors.copyAllErrors(m_GrpPolImpInfo.mErrors);
        }
        m_GrpPolImpInfo.logError(mBatchNo, mGrpContNo, contId, insuredId,
                polId, contplancode, riskcode, insuredSchema, this.mErrors,
                mGlobalInput);
        logErrors.copyAllErrors(this.mErrors);
        this.mErrors.clearErrors();
        m_GrpPolImpInfo.mErrors.clearErrors();

    }

    /**
     * 获取排序的keyset
     * @param set Set
     * @return Object[]
     */

    private int[] getOrderKeySetInt(Set set)
    {
        if (set == null)
        {
            return null;
        }
        Object[] oSet = set.toArray();
        int dest[] = new int[oSet.length];
        for (int i = 0; i < oSet.length; i++)
        {
            dest[i] = Integer.parseInt((String) oSet[i]);
        }
        Arrays.sort(dest);
        return dest;
    }

    /**
     * 通过duty设置一些lcpol的元素
     * @param tLCPolSchema LCPolSchema
     * @param dutySchema LCDutySchema
     */
    private void setPolInfoByDuty(LCPolSchema tLCPolSchema,
            LCDutySchema dutySchema)
    {

        tLCPolSchema.setInsuYear(dutySchema.getInsuYear());
        tLCPolSchema.setInsuYearFlag(dutySchema.getInsuYearFlag());
        tLCPolSchema.setPrem(dutySchema.getPrem());
        tLCPolSchema.setAmnt(dutySchema.getAmnt());
        tLCPolSchema.setPayEndYear(dutySchema.getPayEndYear());
        tLCPolSchema.setPayEndYearFlag(dutySchema.getPayEndYearFlag());
        tLCPolSchema.setGetYear(dutySchema.getGetYear());
        tLCPolSchema.setGetYearFlag(dutySchema.getGetYearFlag());
        tLCPolSchema.setAcciYear(dutySchema.getAcciYear());
        tLCPolSchema.setAcciYearFlag(dutySchema.getAcciYearFlag());
        tLCPolSchema.setMult(dutySchema.getMult());
        //计算方向,在按分数卖的保单，切记算方向为O的时候
        if (dutySchema.getMult() > 0
                && "O".equals(StrTool.cTrim(dutySchema.getPremToAmnt())))
        {
            tLCPolSchema.setPremToAmnt(dutySchema.getPremToAmnt());
        }
        tLCPolSchema.setStandbyFlag1(dutySchema.getStandbyFlag1());
        tLCPolSchema.setStandbyFlag2(dutySchema.getStandbyFlag2());
        tLCPolSchema.setStandbyFlag3(dutySchema.getStandbyFlag3());
    }

    private void setDutyByPolInfo(LCDutySchema dutySchema,
            LCPolSchema tLCPolSchema)
    {
        if (tLCPolSchema.getMult() > 0)
        {
            dutySchema.setMult(tLCPolSchema.getMult());
        }
        if (tLCPolSchema.getPrem() > 0)
        {
            dutySchema.setPrem(tLCPolSchema.getPrem());
        }
        if (tLCPolSchema.getAmnt() > 0)
        {
            dutySchema.setAmnt(tLCPolSchema.getAmnt());
        }
        if (tLCPolSchema.getPayIntv() > 0)
        {
            dutySchema.setPayIntv(tLCPolSchema.getPayIntv());
        }
        if (tLCPolSchema.getInsuYear() > 0)
        {
            dutySchema.setInsuYear(tLCPolSchema.getInsuYear());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getInsuYearFlag())))
        {
            dutySchema.setInsuYearFlag(tLCPolSchema.getInsuYearFlag());
        }
        if (tLCPolSchema.getPayEndYear() > 0)
        {
            dutySchema.setPayEndYear(tLCPolSchema.getPayEndYear());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getPayEndYearFlag())))
        {
            dutySchema.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag());
        }
        if (tLCPolSchema.getGetYear() > 0)
        {
            dutySchema.setGetYear(tLCPolSchema.getGetYear());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getGetYearFlag())))
        {
            dutySchema.setGetYearFlag(tLCPolSchema.getGetYearFlag());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getGetStartType())))
        {
            dutySchema.setGetStartType(tLCPolSchema.getGetStartType());
        }
        //计算方向
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getPremToAmnt())))
        {
            dutySchema.setPremToAmnt(tLCPolSchema.getPremToAmnt());
        }
        //计算规则被缓存在 最终核保人编码 UWCode 字段
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getUWCode())))
        {
            dutySchema.setCalRule(tLCPolSchema.getUWCode());
        }
        //费率
        if (tLCPolSchema.getFloatRate() > 0)
        {
            if ("0".equals(dutySchema.getCalRule())
                    || "1".equals(dutySchema.getCalRule()))
            {
                //计算规则为 0-表定费率 或 1-统一费率 时
                //费率必须是从默认值中取得
            }
            else
            {
                dutySchema.setFloatRate(tLCPolSchema.getFloatRate());
            }
        }

        if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag1())))
        {
            dutySchema.setStandbyFlag1(tLCPolSchema.getStandbyFlag1());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag2())))
        {
            dutySchema.setStandbyFlag2(tLCPolSchema.getStandbyFlag2());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getStandbyFlag3())))
        {
            dutySchema.setStandbyFlag3(tLCPolSchema.getStandbyFlag3());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getApproveFlag())))
        {
            dutySchema.setGetLimit(tLCPolSchema.getApproveFlag());
        }
        if (!"".equals(StrTool.cTrim(tLCPolSchema.getUWFlag())))
        {
            dutySchema.setGetRate(tLCPolSchema.getUWFlag());
        }

    }

    /**
     * 返回批次号,用于报错信息反显时数据库查询的方便
     * @return String
     */
    public String getBatchNo()
    {
        return this.mBatchNo;
    }

    /**
     * 被保险人的信息校验包括:
     * 生日校验
     * 姓名校验
     * 身份证位数校验
     * 性别校验
     * 职业类别校验
     * 职业代码校验
     * @param chkLCInsuredSet LCInsuredSet
     * @return boolean
     * @author YangMing
     */
    private boolean checkInsured(LCInsuredSet chkLCInsuredSet,LCInsuredListSet tLCInsuredListSet)
    {
        int maxInsuredNo = chkLCInsuredSet.size();
        for (int n = 1; n <= maxInsuredNo; n++)
        {
            LCInsuredSchema chkLCInsuredSchema = new LCInsuredSchema();
            chkLCInsuredSchema = chkLCInsuredSet.get(n);
            //by gzh 20101222
            String insuereid = "";
            String Position ="";
            String GrpContNo="";
            for(int m= 1;m<=tLCInsuredListSet.size();m++){
            	if(chkLCInsuredSchema.getName().equals(tLCInsuredListSet.get(m).getInsuredName())){
            		insuereid = tLCInsuredListSet.get(m).getInsuredID();
            		Position = tLCInsuredListSet.get(m).getPosition();
            		GrpContNo=tLCInsuredListSet.get(m).getGrpContNo();
            		break;
            	}
            }
            String Birthday = chkLCInsuredSchema.getBirthday();
            if (Birthday == null || Birthday.equals("null")
                    || Birthday.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "在获得被保险人的出生日期时发生错误!";
                this.mErrors.addOneError(tError);
                return false;

            }
            String[] birthday = Birthday.split("-");

            if (birthday.length != 3)
            {
                CError.buildErr(this, "被保险人[" + chkLCInsuredSchema.getName()
                        + "]的生日信息有误");
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                        chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                        "被保险人", "Birthday", "生日", chkLCInsuredSchema
                                .getBirthday());
                this.mErrors.clearErrors();
                return false;

            }
            String Year = birthday[0];
            String Month = birthday[1];
            String Day = birthday[2];
            if (Integer.parseInt(Year) < 1900 && Integer.parseInt(Year) > 2500)
            {
                CError.buildErr(this, "请检查被保险人[" + chkLCInsuredSchema.getName()
                        + "]的出生年份是否有误");
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                        chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                        "被保险人", "Birthday", "生日", chkLCInsuredSchema
                                .getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            if (Integer.parseInt(Month) < 0 && Integer.parseInt(Month) > 12)
            {
                CError.buildErr(this, "请检查被保险人[" + chkLCInsuredSchema.getName()
                        + "]的出生月份是否有误");
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                        chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                        "被保险人", "Birthday", "生日", chkLCInsuredSchema
                                .getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            if (Integer.parseInt(Day) < 0 && Integer.parseInt(Day) > 32)
            {
                CError.buildErr(this, "请检查被保险人[" + chkLCInsuredSchema.getName()
                        + "]的出生日期份是否有误");
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                        chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                        "被保险人", "Birthday", "生日", chkLCInsuredSchema
                                .getBirthday());
                this.mErrors.clearErrors();
                return false;
            }
            if (chkLCInsuredSchema.getName() == null
                    || "".equals(chkLCInsuredSchema.getName()))
            {
                CError.buildErr(this, "请检查被保险人[" + chkLCInsuredSchema.getName()
                        + "]的姓名是否有误 ");
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                        chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                        "被保险人", "Name", "姓名", chkLCInsuredSchema.getName());
                this.mErrors.clearErrors();
                return false;
            }
            if (chkLCInsuredSchema.getSex() == null
                    || "".equals(chkLCInsuredSchema.getSex())
                    || (!"1".equals(chkLCInsuredSchema.getSex()) && !"0"
                            .equals(chkLCInsuredSchema.getSex())))
            {
                CError.buildErr(this, "请检查被保险人[" + chkLCInsuredSchema.getName()
                        + "]的性别是否有误 ");
                m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                        chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                        "被保险人", "Sex", "性别", chkLCInsuredSchema.getSex());
                this.mErrors.clearErrors();
                return false;
            }
            String IDNo = chkLCInsuredSchema.getIDNo();
            int IDNoBit = IDNo == null ? 0 : IDNo.length();
            if (!StrTool.cTrim(chkLCInsuredSchema.getIDNo()).equals(""))
            {
                if (chkLCInsuredSchema.getIDType() == null
                        || "".equals(chkLCInsuredSchema.getIDType()))
                {
                    CError.buildErr(this, "请检查被保险人["
                            + chkLCInsuredSchema.getName() + "]的证件类型否有误 ");
                    m_GrpPolImpInfo.logError(this.mBatchNo, this.mGrpContNo,
                            chkLCInsuredSchema.getContNo(), insuereid, "", "", "", null,
                            this.mErrors, mGlobalInput);
                    GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3", "999",
                            "被保险人", "IDType", "证件类型", chkLCInsuredSchema
                                    .getIDType());
                    this.mErrors.clearErrors();
                    return false;
                }
                if ("0".equals(chkLCInsuredSchema.getIDType()))
                {
                    if (IDNoBit != 15 && IDNoBit != 18)
                    {
                        CError.buildErr(this, "请检查被保险人["
                                + chkLCInsuredSchema.getName()
                                + "]的身份证号码位数是否有误！");
                        m_GrpPolImpInfo.logError(this.mBatchNo,
                                this.mGrpContNo,
                                chkLCInsuredSchema.getContNo(),
                                insuereid, "", "", "",
                                null, this.mErrors, mGlobalInput);
                        GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                "999", "被保险人", "IDNo", "证件号码",
                                chkLCInsuredSchema.getIDNo());
                        this.mErrors.clearErrors();
                        return false;
                    }
                    if (IDNoBit == 15)
                    {

                        //最好是用正则表达式
                        for (int i = 0; i < IDNo.length(); i++)
                        {
                            if ("0123456789".indexOf(IDNo.charAt(i)) < 0)
                            {
                                CError.buildErr(this, "请检查被保险人["
                                        + chkLCInsuredSchema.getName()
                                        + "]15位身份证号码是否是纯数字");
                                m_GrpPolImpInfo.logError(this.mBatchNo,
                                        this.mGrpContNo, chkLCInsuredSchema
                                                .getContNo(),
                                                insuereid, "",
                                        "", "", null, this.mErrors,
                                        mGlobalInput);
                                System.out.println("请检查被保险人["
                                        + chkLCInsuredSchema.getName()
                                        + "]15位身份证号码是否是纯数字");
                                return false;
                            }
                        }

                        String subYear = IDNo.substring(6, 8);
                        String subMonth = IDNo.substring(8, 10);
                        String subDay = IDNo.substring(10, 12);
                        String subSex = IDNo.substring(13);
                        String Choice = "";
                        String Sex = chkLCInsuredSchema.getSex();
                        if (Integer.parseInt(subSex) % 2 == 1)
                        {
                            Choice = "0";
                        }
                        if (Integer.parseInt(subSex) % 2 == 0)
                        {
                            Choice = "1";
                        }
                        if (!Choice.equals(Sex))
                        {
                            CError.buildErr(this, "请检查被保险人["
                                    + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人性别是否对应！");
                            m_GrpPolImpInfo.logError(this.mBatchNo,
                                    this.mGrpContNo, chkLCInsuredSchema
                                            .getContNo(), insuereid, "", "", "", null,
                                    this.mErrors, mGlobalInput);
                            GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                    "999", "被保险人", "IDNo", "证件号码",
                                    chkLCInsuredSchema.getIDNo());
                            this.mErrors.clearErrors();
                            return false;
                        }
                        if (!Year.substring(2).equals(subYear)
                                || !subMonth.equals(Month)
                                || !subDay.equals(Day))
                        {
                            CError.buildErr(this, "请检查被保险人["
                                    + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人生日是否对应！");
                            m_GrpPolImpInfo.logError(this.mBatchNo,
                                    this.mGrpContNo, chkLCInsuredSchema
                                            .getContNo(), insuereid, "", "", "", null,
                                    this.mErrors, mGlobalInput);
                            GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                    "00010", "被保险人", "IDNo", "证件号码",
                                    chkLCInsuredSchema.getIDNo());

                            this.mErrors.clearErrors();
                            return false;
                        }
                    }
                    if (IDNoBit == 18)
                    {
                        String subYear = IDNo.substring(6, 10);
                        String subMonth = IDNo.substring(10, 12);
                        String subDay = IDNo.substring(12, 14);
                        String subSex = IDNo.substring(16, 17);
                        String Sex = chkLCInsuredSchema.getSex();
                        String Choice = "";
                        if (Integer.parseInt(subSex) % 2 == 1)
                        {
                            Choice = "0";
                        }
                        if (Integer.parseInt(subSex) % 2 == 0)
                        {
                            Choice = "1";
                        }
                        if (!Choice.equals(Sex))
                        {
                            CError.buildErr(this, "请检查被保险人["
                                    + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人性别是否对应！");
                            m_GrpPolImpInfo.logError(this.mBatchNo,
                                    this.mGrpContNo, chkLCInsuredSchema
                                            .getContNo(), insuereid, "", "", "", null,
                                    this.mErrors, mGlobalInput);
                            GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                    "999", "被保险人", "IDNo", "证件号码",
                                    chkLCInsuredSchema.getIDNo());
                            this.mErrors.clearErrors();
                            return false;
                        }

                        if (!Year.equals(subYear) || !subMonth.equals(Month)
                                || !subDay.equals(Day))
                        {
                            CError.buildErr(this, "请检查被保险人["
                                    + chkLCInsuredSchema.getName()
                                    + "]的身份证号码与被保险人生日是否对应！");
                            m_GrpPolImpInfo.logError(this.mBatchNo,
                                    this.mGrpContNo, chkLCInsuredSchema
                                            .getContNo(), insuereid, "", "", "", null,
                                    this.mErrors, mGlobalInput);
                            GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                                    "999", "被保险人", "IDNo", "证件号码",
                                    chkLCInsuredSchema.getIDNo());
                            this.mErrors.clearErrors();
                            return false;
                        }
                    }
                }
            }
            
            //职业类别与职业代码的校验
            String OccupationType = chkLCInsuredSchema.getOccupationType();
            String OccupationCode = chkLCInsuredSchema.getOccupationCode();
            if(!CheckOccupationType(OccupationType))
            {
            	CError.buildErr(this, "请检查被保险人["
                        + chkLCInsuredSchema.getName()
                        + "]的职业类别是否符合描述！");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                        this.mGrpContNo, chkLCInsuredSchema
                                .getContNo(), insuereid, "", "", "", chkLCInsuredSchema,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                        "999", "被保险人", "OccupationType", "职业类别",chkLCInsuredSchema.getOccupationType());
                this.mErrors.clearErrors();
//                buildError("DiskContImport", "请检查被保险人["+chkLCInsuredSchema.getName()+"]的职业类别是否符合描述！");
                return false;
            }
            if(!CheckOccupationCode(OccupationCode,OccupationType))
            {
            	CError.buildErr(this, "请检查被保险人["
                        + chkLCInsuredSchema.getName()
                        + "]的职业类别与职业代码是否符合描述！");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                        this.mGrpContNo, chkLCInsuredSchema
                                .getContNo(), insuereid, "", "", "", chkLCInsuredSchema,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                        "999", "被保险人", "OccupationCode", "职业代码",chkLCInsuredSchema.getOccupationCode());
                this.mErrors.clearErrors();
//                buildError("DiskContImport", "请检查被保险人["+chkLCInsuredSchema.getName()+"的职业类别与职业代码是否符合描述！");
                return false;
            }
            
            
            //职业类别与职业代码的校验    
            if(!CheckPosition(Position,GrpContNo))
            {
            	CError.buildErr(this, "请检查被保险人["
                        + chkLCInsuredSchema.getName()
                        + "]的级别录入有误");
                m_GrpPolImpInfo.logError(this.mBatchNo,
                        this.mGrpContNo, chkLCInsuredSchema
                                .getContNo(), insuereid, "", "", "", chkLCInsuredSchema,
                        this.mErrors, mGlobalInput);
                GrpAutoQuest(mLCGrpContSchema, this.mErrors, "3",
                        "999", "被保险人", "OccupationType", "职业类别",chkLCInsuredSchema.getPosition());
                this.mErrors.clearErrors();
//                buildError("DiskContImport", "请检查被保险人["+chkLCInsuredSchema.getName()+"]的职业类别是否符合描述！");
                return false;
            }
         
            
        }
        return true;
    }

    private boolean checkValiDate(String StartDate, String EndDate)
    {
        int monthNum = PubFun.calInterval2(StartDate, EndDate, "M");
        String calSql = "select 1 from dual where date('" + StartDate + "') + "
                + monthNum + " month - 1 day = date('" + EndDate + "')";
        ExeSQL tExeSQL = new ExeSQL();
        String chk = tExeSQL.getOneValue(calSql);
        if ("".equals(chk) || chk == null)
        {
            return false;
        }
        return true;
    }

    /**
     * Yangming:用于自动下发问题件
     *
     * @param tempLCGrpContSchema LCGrpContSchema
     * @param tCError CErrors
     * @param BackObjType 返回对象
     * @param IssueType 用于返回问题代码
     * @param QuestionObj 问题对象(投保人,被保人,合同,业务员)
     * @param ErrField 错误字段
     * @param ErrFieldName 错误字段名
     * @param ErrContent 原填写内容
     * @return boolean
     */
    private boolean GrpAutoQuest(LCGrpContSchema tempLCGrpContSchema,
            CErrors tCError, String BackObjType, String IssueType,
            String QuestionObj, String ErrField, String ErrFieldName,
            String ErrContent)
    {
        String GroContNo = tempLCGrpContSchema.getGrpContNo();
        String BackObj = "";
        for (int n = 1; n < mLCGrpIssuePolSet.size(); n++)
        {
            if (BackObjType.equals(mLCGrpIssuePolSet.get(n).getBackObjType())
                    && QuestionObj.equals(mLCGrpIssuePolSet.get(n)
                            .getQuestionObj())
                    && ErrField.equals(mLCGrpIssuePolSet.get(n).getErrField())
                    && ErrFieldName.equals(mLCGrpIssuePolSet.get(n)
                            .getErrFieldName())
                    && ErrContent.equals(mLCGrpIssuePolSet.get(n)
                            .getErrContent())
                    && IssueType
                            .equals(mLCGrpIssuePolSet.get(n).getIssueType()))
            {
                return false;
            }
        }
        if (GroContNo == null || "".equals(GroContNo))
        {
            return false;
        }
        if (tCError == null)
        {
            return false;
        }
        /**
         * BackObjType退回对象类型,
         * 1 －－ 操作员
         * 2 －－ 业务员
         * 3 －－ 保户
         * 4 －－ 机构
         */
        if ("1".equals(BackObjType))
        {
            BackObj = tempLCGrpContSchema.getOperator();
        }
        if ("2".equals(BackObjType))
        {
            BackObj = tempLCGrpContSchema.getAgentCode();
        }
        if ("3".equals(BackObjType))
        {
            BackObj = tempLCGrpContSchema.getAppntNo();
        }
        if ("4".equals(BackObjType))
        {
            BackObj = tempLCGrpContSchema.getManageCom();
        }
        String errMess = "";
        errMess = tCError.getError(tCError.getErrorCount() - 1).errorMessage
                + ";";
        LCGrpIssuePolSchema tLCGrpIssuePolSchema = new LCGrpIssuePolSchema();
        tLCGrpIssuePolSchema.setGrpContNo(GroContNo);
        tLCGrpIssuePolSchema.setProposalGrpContNo(GroContNo);
        tLCGrpIssuePolSchema.setSerialNo(PubFun1.CreateMaxNo("GIPAYNOTICENO",
                tempLCGrpContSchema.getPrtNo()));
        tLCGrpIssuePolSchema.setIssueType(IssueType);
        tLCGrpIssuePolSchema.setOperatePos("10");
        tLCGrpIssuePolSchema.setBackObjType(BackObjType);
        tLCGrpIssuePolSchema.setBackObj(BackObj);
        tLCGrpIssuePolSchema.setIssueCont(errMess);
        tLCGrpIssuePolSchema.setNeedPrint("N");
        tLCGrpIssuePolSchema.setOperator(this.mGlobalInput.Operator);
        tLCGrpIssuePolSchema.setManageCom(this.mGlobalInput.ManageCom);
        tLCGrpIssuePolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpIssuePolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpIssuePolSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpIssuePolSchema.setQuestionObj(QuestionObj);
        tLCGrpIssuePolSchema.setErrField(ErrField);
        tLCGrpIssuePolSchema.setErrFieldName(ErrFieldName);
        tLCGrpIssuePolSchema.setErrContent(ErrContent);
        MMap map = new MMap();
        map.put(tLCGrpIssuePolSchema, "DELETE&INSERT");
        VData cInputData = new VData();
        cInputData.add(map);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(cInputData, null))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        //mLCGrpIssuePolSet.add(tLCGrpIssuePolSchema);
        return true;
    }

    private boolean dealPremError()
    {
        /** 实际误差应该只关心计划的总保费和该计划下的被保险人的总保费是否相同,如果不相同
         * 则让一个人,一个险种,一个责任,一个缴费承担其误差 */
        /** 首先教研人数是否相同 */
        String strSql = "select sum(peoples3),a.contplancode,a.riskcode from lccontplanrisk a,lccontplan b where a.grpcontno='"
                + this.mGrpContNo
                + "' and a.contplancode not in ('00','11') and a.grpcontno=b.grpcontno and a.contplancode=b.contplancode group by a.contplancode,a.riskcode ";
        SSRS ssrs = (new ExeSQL()).execSQL(strSql);
        /** 无论何时此处都不应该抱错 */
        if (ssrs.mErrors.needDealError())
        {
            buildError("dealPremError", "查询保险计划信息失败！");
            return false;
        }
        VData tVData = null;
        MMap map = new MMap();
        for (int i = 1; i <= ssrs.getMaxRow(); i++)
        {
            /** 首先保存保险计划 */
            String tContPlanCode = StrTool.cTrim(ssrs.GetText(i, 2));
            String tPeoples = StrTool.cTrim(ssrs.GetText(i, 1));
            String tRiskCode = StrTool.cTrim(ssrs.GetText(i, 3));
            if (diffPeoples(tContPlanCode, tPeoples))
            {
                continue;
            }
            if (tVData == null)
            {
                tVData = new VData();
            }
            /** 人数相同,开始处理误差 */
            VData tmpVData = getUpdate(tContPlanCode, tPeoples, tRiskCode);
            if (tmpVData != null)
            {
                map.add((MMap) tmpVData.getObjectByObjectName("MMap", 0));
            }
        }
        if (tVData != null)
        {
            tVData.add(map);
            PubSubmit ps = new PubSubmit();
            if (!ps.submitData(tVData, null))
            {
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * getUpdate
     *
     * @param tContPlanCode String
     * @param tPeoples String
     * @param tRiskCode String
     * @return VData
     */
    private VData getUpdate(String tContPlanCode, String tPeoples,
            String tRiskCode)
    {
        String str_Insured_Sum_Prem = (new ExeSQL())
                .getOneValue(getInsuredSumPrem(tContPlanCode, tRiskCode));
        System.out.println("保险计划 : " + tContPlanCode + " 人员下的总保费: "
                + str_Insured_Sum_Prem + " 险种:" + tPeoples);
        if (StrTool.cTrim(str_Insured_Sum_Prem).equals(""))
        {
            str_Insured_Sum_Prem = "0";
        }

        /** 被保险人的保费 */
        double num_Insured_Sum_Prem = Double.parseDouble(str_Insured_Sum_Prem);
        String str_ContPlan_Sum_Prem = (new ExeSQL())
                .getOneValue(getContPlanSumPrem(tContPlanCode, tRiskCode));
        System.out.println("+++++++++1"
                + Double.parseDouble(str_ContPlan_Sum_Prem));
        if (StrTool.cTrim(str_ContPlan_Sum_Prem).equals(""))
        {
            str_ContPlan_Sum_Prem = "0";
        }
        if (Double.parseDouble(str_ContPlan_Sum_Prem) == 0.0)
        {
            System.out.println("babababab!!!");
            return null;
        }

        /** 保险计划总保费 */
        double num_ContPlan_Sum_Prem = Double
                .parseDouble(str_ContPlan_Sum_Prem);
        if (num_Insured_Sum_Prem - num_ContPlan_Sum_Prem >= 0.009
                || num_ContPlan_Sum_Prem - num_Insured_Sum_Prem >= 0.009)
        {
            System.out.println("当前保障计划下的保费和录入的保费不同需要解决误差问题!");
            /** 误差 */
            double sub = num_ContPlan_Sum_Prem - num_Insured_Sum_Prem;
            sumSub += sub;
            /** 误差太大显然为错误 */
            if (Math.abs(sub) > 100)
            {
                buildError("getUpdate", "保障计划录入保费："
                        + PubFun.setPrecision(num_ContPlan_Sum_Prem, "0.00")
                        + "被保人共" + tPeoples + "人，保费：" + num_Insured_Sum_Prem);
                return null;
            }
            /** 查询处当前计划下的一个被保险人 */
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = tLCPolDB.executeQuery(getPolSet(tContPlanCode,
                    tRiskCode));
            /** 让最后一个险种信息承担计划下的误差,维护误差过程中需要维护保费相关表 */
            VData tVData = perparePremErrorData(
                    tLCPolSet.get(tLCPolSet.size()), sub);
            if (tVData == null)
            {
                return null;
            }
            else
            {
                return tVData;
            }
        }
        return null;
    }

    /**
     * perparePremErrorData
     *
     * @param lCPolSchema LCPolSchema
     * @param sub double
     * @return VData
     */
    private VData perparePremErrorData(LCPolSchema lCPolSchema, double sub)
    {
        LCContSchema tLCContSchema = getContSet(lCPolSchema);
        LCGrpContSchema tLCGrpContSchema = getGrpContSet(lCPolSchema);
        LCGrpPolSchema tLCGrpPolSchema = getGrpPolSet(lCPolSchema);
        LCDutySchema tLCDutySchema = getDutySet(lCPolSchema);
        LCPremSchema tLCPremSchema = getPremSet(lCPolSchema, tLCDutySchema);
        tLCContSchema.setPrem(PubFun.setPrecision(tLCContSchema.getPrem()
                + sumSub, "0.00"));
        tLCGrpContSchema.setPrem(PubFun.setPrecision(tLCGrpContSchema.getPrem()
                + sumSub, "0.00"));
        tLCDutySchema.setPrem(PubFun.setPrecision(
                tLCDutySchema.getPrem() + sub, "0.00"));
        //        tLCDutySchema.setStandPrem(
        //                PubFun.setPrecision(tLCDutySchema.getStandPrem() + sub, "0.00"));
        tLCPremSchema.setPrem(PubFun.setPrecision(
                tLCPremSchema.getPrem() + sub, "0.00"));
        //        tLCPremSchema.setStandPrem(
        //                PubFun.setPrecision(tLCPremSchema.getStandPrem() + sub, "0.00"));
        //        lCPolSchema.setStandPrem(
        //                PubFun.setPrecision(lCPolSchema.getStandPrem() + sub, "0.00"));
        lCPolSchema.setPrem(PubFun.setPrecision(lCPolSchema.getPrem() + sub,
                "0.00"));
        tLCGrpPolSchema.setPrem(PubFun.setPrecision(tLCGrpPolSchema.getPrem()
                + sub, "0.00"));
        MMap map = new MMap();
        map.put(tLCContSchema, "UPDATE");
        map.put(tLCGrpContSchema, "UPDATE");
        map.put(tLCDutySchema, "UPDATE");
        map.put(tLCPremSchema, "UPDATE");
        map.put(lCPolSchema, "UPDATE");
        map.put(tLCGrpPolSchema, "UPDATE");
        VData tVData = new VData();
        tVData.add(map);
        return tVData;
    }

    /**
     * getGrpPolSet
     *
     * @param lCPolSchema LCPolSchema
     * @return LCGrpPolSchema
     */
    private LCGrpPolSchema getGrpPolSet(LCPolSchema lCPolSchema)
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(lCPolSchema.getGrpPolNo());
        if (!tLCGrpPolDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
            return null;
        }
        return tLCGrpPolDB.getSchema();
    }

    private LCGrpContSchema getGrpContSet(LCPolSchema lCPolSchema)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(lCPolSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo())
        {
            buildError("perparePremErrorData", "解决误差时查旬团体险种信息失败！");
            return null;
        }
        LCGrpContSchema tLCGrpContSchema = tLCGrpContDB.getSchema();
        return tLCGrpContSchema;
    }

    private LCContSchema getContSet(LCPolSchema lCPolSchema)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(lCPolSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            buildError("perparePremErrorData", tLCContDB.mErrors
                    .getFirstError());
            return null;
        }
        LCContSchema tLCContSchema = tLCContDB.getSchema();
        return tLCContSchema;
    }

    private LCPremSchema getPremSet(LCPolSchema lCPolSchema,
            LCDutySchema tLCDutySchema)
    {
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(lCPolSchema.getPolNo());
        tLCPremDB.setDutyCode(tLCDutySchema.getDutyCode());
        LCPremSet tLCPremSet = tLCPremDB.query();
        if (tLCPremSet.size() <= 0)
        {
            buildError("perparePremErrorData", "在解决误差问题时查询缴费失败！");
            return null;
        }
        LCPremSchema tLCPremSchema = tLCPremSet.get(tLCPremSet.size());
        return tLCPremSchema;
    }

    private LCDutySchema getDutySet(LCPolSchema lCPolSchema)
    {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(lCPolSchema.getPolNo());
        LCDutySet tLCDutySet = tLCDutyDB.query();
        if (tLCDutySet.size() <= 0)
        {
            buildError("perparePremErrorData", "解决误差时查旬责任信息失败！");
            return null;
        }
        return tLCDutySet.get(tLCDutySet.size());
    }

    /**
     *
     * @param tContPlanCode String
     * @param tRiskCode String
     * @return String
     */
    private String getPolSet(String tContPlanCode, String tRiskCode)
    {
        return "select * from lcpol where insuredno in (select distinct insuredno from lcinsured where contplancode='"
                + tContPlanCode
                + "' and grpcontno='"
                + this.mGrpContNo
                + "') and grpcontno='"
                + this.mGrpContNo
                + "' and riskcode='"
                + tRiskCode + "' fetch first 1 rows only";
    }

    /**
     *
     * @param tContPlanCode String
     * @param tRiskCode String
     * @return String
     */
    private String getContPlanSumPrem(String tContPlanCode, String tRiskCode)
    {
        String str_ContPlan_Sum_Prem = "select sum(RiskPrem) from lccontplanrisk where contplancode='"
                + tContPlanCode
                + "' and grpcontno='"
                + this.mGrpContNo
                + "' and riskcode='" + tRiskCode + "'";
        return str_ContPlan_Sum_Prem;
    }

    /**
     *
     * @param tContPlanCode String
     * @param tRiskCode String
     * @return String
     */
    private String getInsuredSumPrem(String tContPlanCode, String tRiskCode)
    {
        String insured_Sum_Prem = "select sum(prem) from lcpol where insuredno in "
                + "(select distinct insuredno from lcinsured where contplancode='"
                + tContPlanCode
                + "' and grpcontno='"
                + this.mGrpContNo
                + "') and grpcontno='"
                + this.mGrpContNo
                + "' and riskcode='"
                + tRiskCode + "'";
        return insured_Sum_Prem;
    }

    /**
     * diffPeoples
     *
     * @param tContPlanCode String
     * @param tPeoples String
     * @return boolean
     */
    private boolean diffPeoples(String tContPlanCode, String tPeoples)
    {
        String peoples = (new ExeSQL())
                .getOneValue("select count(1) from lcinsured where grpcontno='"
                        + this.mGrpContNo + "' and contplancode='"
                        + tContPlanCode + "'");
        if (StrTool.cTrim(tPeoples).equals(StrTool.cTrim(peoples)))
        {
            System.out.println("人数相同");
            return false;
        }
        else
        {
            System.out.println("人数不同");
            return true;
        }
    }

    /**
     * dealWrapAppage
     *
     * @return boolean
     */
    private boolean dealWrapAppage(LCInsuredListSet tLCInsuredListSet,
            LCGrpPolSchema tLCGrpPolSchema)
    {
//        String tRiskCode = tLCGrpPolSchema.getRiskCode();
//        String tWrapCode = tLCContPlanRiskSchema.getRiskWrapCode();

//        String key = tRiskCode + "-" + tWrapCode;
//        LDRiskWrapSchema tLDRiskWrapSchema = (LDRiskWrapSchema) mRiskWrapCache
//                .get(key);
//        if (tLDRiskWrapSchema == null)
//        {
//            LDRiskWrapDB tLDRiskWrapDB = new LDRiskWrapDB();
//            tLDRiskWrapDB.setRiskCode(tRiskCode);
//            tLDRiskWrapDB.setRiskWrapCode(tWrapCode);
//            if (!tLDRiskWrapDB.getInfo())
//            {
//                String str = "查询套餐描述信息失败!";
//                buildError("dealWrapAppage", str);
//                System.out.println("在程序ParseGuideIn.dealWrapAppage() - 4142 : "
//                        + str);
//                return false;
//            }
//            tLDRiskWrapSchema = tLDRiskWrapDB.getSchema();
//            mRiskWrapCache.put(key, tLDRiskWrapSchema);
//        }
//        /** 最大投保年龄 */
//        int tMaxInsuredAge = tLDRiskWrapSchema.getMaxInsuredAge();
//        int tMinInsuredAge = tLDRiskWrapSchema.getMinInsuredAge();
//        for (int i = 1; i <= tLCInsuredListSet.size(); i++)
//        {
//            /** 取生效日期，首选LCInsuredList中的生效日期 */
//            String tCValiDate = null;
//            if (tLCInsuredListSet.get(i).getEdorValiDate() != null)
//            {
//                tCValiDate = tLCInsuredListSet.get(i).getEdorValiDate();
//            }
//            else
//            {
//                tCValiDate = this.mLCGrpContSchema.getCValiDate();
//            }
//            int age = PubFun.getInsuredAppAge(tCValiDate, tLCInsuredListSet
//                    .get(i).getBirthday());
//            if (tMaxInsuredAge > 0 && age > tMaxInsuredAge)
//            {
//                String str = "被保人：" + tLCInsuredListSet.get(i).getInsuredName()
//                        + "年龄：" + age + "；不满足套餐最大投保年龄限制!";
//                buildError("dealWrapAppage", str);
//                System.out.println("在程序ParseGuideIn.dealWrapAppage() - 4162 : "
//                        + str);
//                return false;
//            }
//            if (tMinInsuredAge > 0 && age < tMinInsuredAge)
//            {
//                String str = "被保人：" + tLCInsuredListSet.get(i).getInsuredName()
//                        + "年龄：" + age + "；不满足套餐投最小保年龄限制!";
//                buildError("dealWrapAppage", str);
//                System.out.println("在程序ParseGuideIn.dealWrapAppage() - 4162 : "
//                        + str);
//                return false;
//            }
//        }
        return true;
    }

    /**
     * 获取“领取频次”要素值，如没有该要素，返回空串。
     * @param tLCContPlanRiskSchema 险种计划信息
     * @return
     */
    private String getGrpGetIntv(LCContPlanRiskSchema tLCContPlanRiskSchema)
    {
        String tStrSql = " select CalFactorValue from LCContPlanDutyParam "
                + " where GrpContNo='" + mGrpContNo + "' "
                + " and RiskCode = '" + tLCContPlanRiskSchema.getRiskCode()
                + "' " + " and CalFactor = 'GrpGetIntv' ";
        return new ExeSQL().getOneValue(tStrSql);
    }

    /**
     * 校验被保人人数。
     * @param cGrpContNo
     * @return
     */
    private boolean chkInsuNums(String cGrpContNo)
    {
        String tStrSql = " select 1 "
                + " from "
                + " ( "
                + " select count(lcil.InsuredId) InsuCount "
                + " from LCInsuredList lcil "
                + " where 1 = 1 "
                + " and lcil.GrpContNo = '"
                + cGrpContNo
                + "' "
                + " and lcil.InsuredId not in ('C', 'G') "
                + " and lcil.PublicAccType is null "
                + " ) as tmp "
                + " where 1 = 1 "
                + " and tmp.InsuCount < 6 "
                + " and not exists (select 1 from LCInsuredList lcil where lcil.GrpContNo = '"
                + cGrpContNo
                + "' and PublicAccType = '1') "
                + " and exists (select 1 from LCGrpCont lgc where lgc.GrpContNo = '"
                + cGrpContNo + "' and lgc.ManageCom like '8644%') ";
        String tResult = new ExeSQL().getOneValue(tStrSql);

        if ("1".equals(tResult))
        {
            System.out.println("广东团单被保人规则校验不符，被保人数少于6人。");
            return false;
        }

        return true;
    }
    
    /**
     * 校验被保人职业类别。
     * @param OccupationType
     * @return
     */
    private boolean CheckOccupationType(String OccupationType)
    {
    	if(OccupationType != null && OccupationType != ""){
        	String sql = "select 1 from dual where '" + OccupationType + "' in (select distinct occupationtype from ldoccupation)";
        	ExeSQL tExeSQL = new ExeSQL();
        	String chk = tExeSQL.getOneValue(sql);
        	if ("".equals(chk) || chk == null) 
            {
        		System.out.println("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
                return false;
            }
        }
    	return true;
    }
    
    /**
     * 校验被保人职业类别与职业代码是否匹配。
     * @param OccupationType
     * @return
     */
    private boolean CheckOccupationCode(String OccupationCode, String OccupationType)
    {
    	if(OccupationCode != null && OccupationCode != ""){
        	String sql = "select 1 from ldoccupation where OccupationCode = '" + OccupationCode +"' and OccupationType = '" + OccupationType + "'";
        	ExeSQL tExeSQL = new ExeSQL();
        	String chk = tExeSQL.getOneValue(sql);
        	if ("".equals(chk) || chk == null) 
            {
        		System.out.println("被保人有职业类别和职业代码与系统描述不一致，请查看！");
                return false;
            }
        }
    	return true;
    }
    
    //by gzh 20101215
    /**
	 * 得到保单责任计算信息
	 * 
	 * @param cSheet
	 * @param cBMI
	 * @param cPolId
	 * @param strIDColIndex
	 * @return
	 */
	private LCDutySet getPerLCDutySetData(String RiskCode,LCInsuredSchema insuredSchema ,LCInsuredListSchema tLCInsuredListSchema) {
		boolean isErrFlag = false;
		LCDutySet tLCDutySet = new LCDutySet();
		// 选中这个LCDuty的sheet
		try {
			String dutycodeSql = "select lmr.dutycode,ldp.accpayclass " +
					"from lmriskduty lmr,lmdutypayrela lmd,lmdutypay ldp " +
					"where riskcode='"+RiskCode+"' " +
					"and lmr.dutycode = lmd.dutycode " +
					"and lmd.payplancode = ldp.payplancode " +
					"and ldp.accpayclass in ('4','5','6')";
			SSRS dutycodeSSRS = mExeSQL.execSQL(dutycodeSql);
			if(dutycodeSSRS.MaxRow<=0){
				buildError("getPerLCDutySetData", "险种描述获取失败!");
				return null;
			}
			
			for(int dutyCount=1;dutyCount<=dutycodeSSRS.MaxRow;dutyCount++){
//				 数据的处理
				LCDutySchema tLCDutySchema = new LCDutySchema();
				// 险种ID --0
				// 被保险人ID --1
				// 客户姓名--2
				// 险种代码 --3
				tLCDutySchema.setDutyCode(dutycodeSSRS.GetText(dutyCount, 1));// 责任代码（参考录入界面）
				tLCDutySchema.setPayIntv("");// 交费间隔
				tLCDutySchema.setInsuYear("");// 保险期间
				tLCDutySchema.setInsuYearFlag("");// 保险期间标志
				tLCDutySchema.setPayEndYear("");// 交费年期
				tLCDutySchema.setPayEndYearFlag("");// 交费年期标志
				//年金开始领取年龄/年期
				if(!getYear.equals("N")){
					tLCDutySchema.setGetYear(getYear);
				}else{
					tLCDutySchema.setGetYear(tLCInsuredListSchema.getGetYear());// 年金开始领取年龄/年期--10
				}
//				 年金开始领取标志
				if(StrTool.cTrim(tLCInsuredListSchema.getGetYearFlag()).equals("")){
					tLCDutySchema.setGetYearFlag("A");
				}else{
					tLCDutySchema.setGetYearFlag(tLCInsuredListSchema.getGetYearFlag());
				}
				
				tLCDutySchema.setGetStartType("");// 起领日期计算类型 --12
				String tGetDutyKind = tLCInsuredListSchema.getGetDutyKind();// 年金领取类型
				if(!StrTool.cTrim(tGetDutyKind).equals("")){
					getDutyKind = tGetDutyKind;
				}// --13

				tLCDutySchema.setPremToAmnt("");// 计算方向
				tLCDutySchema.setMult("");// 份数
				String tPrem = "";
				if("4".equals(dutycodeSSRS.GetText(dutyCount,2))){
					tPrem = String.valueOf(tLCInsuredListSchema.getAppntPrem());
				}else if("5".equals(dutycodeSSRS.GetText(dutyCount,2))){
					tPrem = String.valueOf(tLCInsuredListSchema.getPersonPrem());
				}else{
					tPrem = String.valueOf(tLCInsuredListSchema.getPersonOwnPrem());
				}
				if (tPrem != null && !tPrem.equals("")
						&& !PubFun.isNumeric(tPrem)) {
					buildError("getPerLCDutySetData", "险种ID为" + RiskCode
							+ "的保费填写为非数字,请修改后再上传!");
					isErrFlag = true;
					return null;
				}
				
				tLCDutySchema.setPrem(tPrem);// 保费
				tLCDutySchema.setAmnt("");// 保额
				tLCDutySchema.setCalRule("");// 计算规则
				tLCDutySchema.setFloatRate("");// 费率
				tLCDutySchema.setGetLimit("");// 免赔额
				tLCDutySchema.setGetRate("");// 赔付比例
				tLCDutySchema.setStandbyFlag1("");// 备用字段1
				tLCDutySchema.setStandbyFlag2("");// 备用字段2
				tLCDutySchema.setStandbyFlag3("");// 备用字段3
//				tLCDutySchema.setStandbyFlag4(cBMI.getText(cSheet, tfr
//						.getRow(), 25));// 备用字段4--25

				tLCDutySet.add(tLCDutySchema);
			}
					
		} catch (Exception e) {
			buildError("getPerLCDutySetData", "处理责任信息时发生异常：" + e.getMessage());
			System.out.println("解析LCPol发生异常：" + e.getMessage());
			e.printStackTrace();
		}
		return tLCDutySet;
	}
	public boolean CheckPosition(String Position,String Grpcontno){

		LCGrpPositionDB tLCGrpPositionDB = new LCGrpPositionDB();
		tLCGrpPositionDB.setGrpContNo(Grpcontno);
		tLCGrpPositionDB.setGradeCode(Position);
        if (tLCGrpPositionDB.query().size()==0)
        {	System.out.println("被保人有职级别与系统描述不一致，请查看！");
            return false;
        }
		return true;
	}
	
	
}
