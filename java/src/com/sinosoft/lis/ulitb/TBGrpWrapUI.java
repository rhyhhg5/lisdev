package com.sinosoft.lis.ulitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 1.0
 */
public class TBGrpWrapUI {
  public TBGrpWrapUI() {
  }

  public static void main(String[] args) {
    TBGrpWrapUI tbgrpwrapui = new TBGrpWrapUI();
  }

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();
  /**
   * 向BL传递的接口
   *
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData, String cOperate) {
    TBGrpWrapBL tTBGrpWrapBL = new TBGrpWrapBL();
    if (!tTBGrpWrapBL.submitData(cInputData, cOperate)) {
      this.mErrors.copyAllErrors(tTBGrpWrapBL.mErrors);
      return false;
    }
    return true;
  }

}
