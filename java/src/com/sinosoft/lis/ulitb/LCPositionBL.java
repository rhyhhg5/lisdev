/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.ulitb;


import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


/**
 * 保障计划要素数据准备类
 * <p>Title: </p>
 * <p>Description: 根据操作类型，进行数据校验、准备处理 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author ZHUXF
 * @version 1.0
 */
public class LCPositionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData() ;
    private MMap map =  new MMap();  
    /** 往后面传输数据的容器 */



    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String mOperate;


    /** 业务处理相关变量 */
    private LCGrpPositionSet mLCGrpPositionSet = new LCGrpPositionSet();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private TransferData mTransferData = new TransferData();
    private String GrpContNo = "";
   /**用于提交数据*/

    /** 时间信息*/
    String mCurrentDate = PubFun.getCurrentDate(); //当前值
    String mCurrentTime = PubFun.getCurrentTime();
    

    
    public LCPositionBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError.buildErr(this, "数据处理失败LCPositionBL-->dealData!");
            return false;
        }

        
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LCPositionBL Submit...");
       
        PubSubmit tPubSubmit = new PubSubmit();
        
        //如果有需要处理的错误，则返回
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            CError.buildErr(this, "数据提交失败LCPositionBL-->PubSubmit");
            return false;
        }
        mInputData = null;
        return true;
    }
 
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
    	System.out.println("**********dealData Begin***************");
    	boolean tReturn = true;
    	System.out.println(mLCGrpPositionSet!=null);
    	System.out.println(mLCGrpPositionSet.size());
        if(mLCGrpPositionSet!=null&&mLCGrpPositionSet.size()>0)
        {  	
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	tLCGrpContDB.setGrpContNo(mLCGrpPositionSet.get(1).getGrpContNo());
         if(tLCGrpContDB.getInfo())
         {
        	 mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
         	int n = mLCGrpPositionSet.size();
         	for(int i=1;i<=n;i++)
         	{       	
         		mLCGrpPositionSet.get(i).setGrpContNo(mLCGrpContSchema.getGrpContNo());
         		mLCGrpPositionSet.get(i).setPrtNo(mLCGrpContSchema.getPrtNo());
         		mLCGrpPositionSet.get(i).setMakeDate(mCurrentDate);
         		mLCGrpPositionSet.get(i).setMakeTime(mCurrentTime);
         		mLCGrpPositionSet.get(i).setModifyDate(mCurrentDate);
         		mLCGrpPositionSet.get(i).setModifyTime(mCurrentTime);
         		mLCGrpPositionSet.get(i).setOperator(mGlobalInput.Operator);
         	}
         	
         }
        }
        map.put("delete from lcgrpposition where grpcontno='" 
        		+ GrpContNo +
                "'" , "DELETE");
        
        map.put(mLCGrpPositionSet, "DELETE&INSERT");
        System.out.println("**********dealData End***************");    
        return tReturn; 
    }


    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	this.mLCGrpPositionSet.set((LCGrpPositionSet) cInputData.
                                       getObjectByObjectName(
                "LCGrpPositionSet",
                0));
        	
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                "GlobalInput", 0));
        
		this.mTransferData = (TransferData) cInputData
		  .getObjectByObjectName("TransferData", 0);
		
		if (mTransferData == null) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "未得到传送数据!";
			this.mErrors.addOneError(tError);
			return false;

		}
		
		GrpContNo = (String) mTransferData.getValueByName("GrpContNo");
		
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
         
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层提交所需要的数据时出错LCGrpPositionSet-->prepareOutputData().");
            return false;
        }
        return true;
    }


    /**
     * 返回集
     * @return VData
     */
    public VData getResult()
    {
        return this.mResult;
    }   
}
