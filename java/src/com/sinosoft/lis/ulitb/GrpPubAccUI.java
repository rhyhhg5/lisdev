package com.sinosoft.lis.ulitb;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.GrpPubAccBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.utility.TransferData;

/**
 *
 * <p>Title: 处理团体公共账户页面接口</p>
 *
 * <p>Description: 页面将传入公共账户类型和公共账户金额</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author YangMing
 * @version 1.0
 */
public class GrpPubAccUI {
    private VData mInputData = new VData();
    private String mOperate = "";
    public CErrors mErrors = new CErrors();
    public GrpPubAccUI() {
    }

    public static void main(String[] args) {
        GrpPubAccUI grppubaccui = new GrpPubAccUI();
        VData data=new VData();
        LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
        LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
        TransferData tTransferData = new TransferData();
        GlobalInput tG = new GlobalInput();
        tG.ComCode="86";
        tG.Operator="picch";
        tG.ManageCom="86";
        tLCInsuredListSchema.setGrpContNo("B14000000000002175");
        tLCInsuredListSchema.setInsuredName("团体理赔帐户");
        tLCInsuredListSchema.setEmployeeName("团体理赔帐户");
        tLCInsuredListSchema.setContPlanCode("A");
        tLCInsuredListSchema.setPublicAcc("50000.00");
        tLCInsuredListSchema.setPublicAccType("C");
        tLCInsuredListSet.add(tLCInsuredListSchema);

        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo("B14000000000002175");
        tLCContPlanDutyParamSchema.setProposalGrpContNo("B14000000000002175");
        tLCContPlanDutyParamSchema.setGrpPolNo("B23000000000003368");

        tLCContPlanDutyParamSchema.setMainRiskCode("1605");
        tLCContPlanDutyParamSchema.setRiskCode("1605");
        tLCContPlanDutyParamSchema.setContPlanCode("11");
        tLCContPlanDutyParamSchema.setDutyCode("000000");
        tLCContPlanDutyParamSchema.setCalFactor("AccType2");
        tLCContPlanDutyParamSchema.setCalFactorValue("02");
        tLCContPlanDutyParamSchema.setPlanType("0");
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("100001");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
        tTransferData.setNameAndValue("GrpContNo","B14000000000002175");
        tTransferData.setNameAndValue("RiskCode","1605");
        tTransferData.setNameAndValue("GrpPolNo","B23000000000003368");
  	tTransferData.setNameAndValue("InsuAccNo","100001");

        data.add(tLCInsuredListSet);
        data.add(tLCContPlanDutyParamSet);
        data.add(tG);
        data.add(tTransferData);
        grppubaccui.submitData(data, "INSERT||MAIN");
    }

    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        GrpPubAccBL tGrpPubAccBL = new GrpPubAccBL();
        try {
            if (!tGrpPubAccBL.submitData(mInputData, mOperate)) {
                this.mErrors.copyAllErrors(tGrpPubAccBL.mErrors);
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(
                    "GrpPubAccUI.submitData(cInputData, cOperate)  \n--Line:47  --Author:YangMing");
            CError tError = new CError();
            tError.moduleName = "OLCInuredListUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
