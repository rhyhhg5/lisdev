/*
 * <p>ClassName: ContInsuredUI </p>
 * <p>Description: ContInsuredUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2004-11-18 10:09:44
 */
package com.sinosoft.lis.ulitb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class ContInsuredUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();


    /** 数据操作字符串 */
    private String mOperate;


//业务处理相关变量
    /** 全局数据 */
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    public ContInsuredUI() {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
//    if (!getInputData(cInputData))
//      return false;
//
//    //进行业务处理
//    if (!dealData())
//      return false;
//
//    //准备往后台的数据
//    if (!prepareOutputData())
//      return false;

        ContInsuredBL tContInsuredBL = new ContInsuredBL();

        System.out.println("Start OLCInsured UI Submit...");
        tContInsuredBL.submitData(cInputData, mOperate);
        //System.out.println("End OLCInsured UI Submit...");
        //如果有需要处理的错误，则返回
        if (tContInsuredBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tContInsuredBL.mErrors);
            return false;
        }
        if (!mOperate.equals("DELETE||CONTINSURED")) {
            this.mResult.clear();
            this.mResult = tContInsuredBL.getResult();
        }
//        if(!tContInsuredBL.dealProposal())
//        {
//            tContInsuredBL.submitData(mResult, "DELETE||CONTINSURED");
//            this.mErrors.copyAllErrors(tContInsuredBL.mErrors);
//            return false;
//        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args) {


//        String birthday = "1982-06-20";
//        String year = birthday.substring(0, birthday.indexOf("-"));
//        String month = birthday.substring(birthday.indexOf("-") + 1,
//                                          birthday.indexOf("-") + 3);
//        String day = birthday.substring(birthday.lastIndexOf("-")+1);
//
//        String today = PubFun.getCurrentDate();
//        String tyear = today.substring(0, today.indexOf("-"));
//        String tmonth = today.substring(today.indexOf("-") + 1,
//                                          today.indexOf("-") + 3);
//        String tday = today.substring(today.lastIndexOf("-")+1);
//
//        int age = Integer.parseInt(tyear)-Integer.parseInt(year);
//        if(Integer.parseInt(tmonth) == Integer.parseInt(month))
//        {
//            if(Integer.parseInt(tday) < Integer.parseInt(day))
//            {
//                age = age -1;
//            }
//        }
//        else if(Integer.parseInt(tmonth) < Integer.parseInt(month))
//        {
//            age = age -1;
//        }


        LCInsuredDB tOLDLCInsuredDB=new LCInsuredDB();

        LCContSchema tLCContSchema = new LCContSchema();
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSet tLDPersonSet = new LDPersonSet();
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = new LCAddressSet();
        //LCInsuredSchema tmLCInsuredSchema = new LCInsuredSchema();

        LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
        LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new
                LCCustomerImpartDetailSet();
        TransferData tTransferData = new TransferData();
        tLCContDB.setPrtNo("16000013178");
        tLCContSet=tLCContDB.query();
        tLCContSchema=tLCContSet.get(1);
        tLDPersonDB.setName("吴敏11");
        tLDPersonSet=tLDPersonDB.query();
        tLDPersonSchema=tLDPersonSet.get(1);
/*        tLCInsuredDB.setName("吴敏11");
        tLCInsuredSet=tLCInsuredDB.query();
        tLCInsuredSchema=tLCInsuredSet.get(1);
        tOLDLCInsuredDB.setSchema(tLCInsuredSchema);
 */
        tLCAddressDB.setCustomerNo("000148459");
        tLCAddressSet=tLCAddressDB.query();
        tLCAddressSchema=tLCAddressSet.get(1);

/*
        tLCContSchema.setGrpContNo("00000000000000000000");
        tLCContSchema.setContNo("13000227446");
        tLCContSchema.setPrtNo("16000013178");
        tLCContSchema.setManageCom("86");
*/
        tLCInsuredSchema.setInsuredNo("000148323");
        tLCInsuredSchema.setRelationToMainInsured(
                "00");
        tLCInsuredSchema.setRelationToAppnt(
                "00");
        tLCInsuredSchema.setContNo("13000227446");
        tLCInsuredSchema.setGrpContNo("00000000000000000000");
        tLCInsuredSchema.setContPlanCode("");
        tLCInsuredSchema.setExecuteCom("");
        tLCInsuredSchema.setJoinCompanyDate(
                "");
        tLCInsuredSchema.setSalary("");
        tLCInsuredSchema.setBankCode("");
        tLCInsuredSchema.setBankAccNo("");
        tLCInsuredSchema.setAccName("");
        tOLDLCInsuredDB.setSchema(tLCInsuredSchema);

/*
        tLDPersonSchema.setCustomerNo("000016114");
        tLDPersonSchema.setName("陈搏8");
        tLDPersonSchema.setSex("0");
        tLDPersonSchema.setBirthday("1955-01-02");
        tLDPersonSchema.setIDType("0");
        tLDPersonSchema.setIDNo("220602195501022010");
        tLDPersonSchema.setPassword("");
        tLDPersonSchema.setNativePlace("");
        tLDPersonSchema.setNationality("ML");
        tLDPersonSchema.setRgtAddress("");
        tLDPersonSchema.setMarriage("0");
        tLDPersonSchema.setMarriageDate("");
        tLDPersonSchema.setHealth("");
        tLDPersonSchema.setStature("");
        tLDPersonSchema.setAvoirdupois("");
        tLDPersonSchema.setDegree("");
        tLDPersonSchema.setCreditGrade("");
        tLDPersonSchema.setOthIDType("");
        tLDPersonSchema.setOthIDNo("");
        tLDPersonSchema.setICNo("");
        tLDPersonSchema.setGrpNo("");
        tLDPersonSchema.setJoinCompanyDate(
                "");
        tLDPersonSchema.setStartWorkDate("");
        tLDPersonSchema.setPosition("");
        tLDPersonSchema.setSalary("");
        tLDPersonSchema.setOccupationType("");
        tLDPersonSchema.setOccupationCode("");
        tLDPersonSchema.setWorkType("");
        tLDPersonSchema.setPluralityType("");
        tLDPersonSchema.setDeathDate("");
        tLDPersonSchema.setSmokeFlag("");
        tLDPersonSchema.setBlacklistFlag("");
        tLDPersonSchema.setProterty("");
        tLDPersonSchema.setRemark("");
        tLDPersonSchema.setState("");
        tLDPersonSchema.setGrpName("");

        tLCAddressSchema.setCustomerNo("");
        tLCAddressSchema.setAddressNo("");
        tLCAddressSchema.setPostalAddress("");
        tLCAddressSchema.setZipCode("");
        tLCAddressSchema.setPhone("");
        tLCAddressSchema.setFax("");
        tLCAddressSchema.setMobile("");
        tLCAddressSchema.setEMail("");

        tLCAddressSchema.setHomeAddress("werwerwerwer");
        tLCAddressSchema.setHomeZipCode("");
        tLCAddressSchema.setHomePhone("");
        tLCAddressSchema.setHomeFax("");
        tLCAddressSchema.setGrpName("");

        tLCAddressSchema.setCompanyPhone("");
        tLCAddressSchema.setCompanyAddress("werwerwerwerwer");
        tLCAddressSchema.setCompanyZipCode("");
        tLCAddressSchema.setCompanyFax("");
 */
        String SavePolType = "";
        String BQFlag = "";
        if (BQFlag == null)
            SavePolType = "0";
        else if (BQFlag.equals(""))
            SavePolType = "0";
        else
            SavePolType = BQFlag;
        tTransferData.setNameAndValue("SavePolType", SavePolType); //保全保存标记，默认为0，标识非保全
        tTransferData.setNameAndValue("ContNo", "");
        tTransferData.setNameAndValue("FamilyType",
                                      "0"); //家庭单标记
        tTransferData.setNameAndValue("ContType",
                                      "1"); //团单，个人单标记
        tTransferData.setNameAndValue("PolTypeFlag",
                                      ""); //无名单标记
        tTransferData.setNameAndValue("InsuredPeoples",
                                      ""); //被保险人人数
        tTransferData.setNameAndValue("InsuredAppAge",
                                      ""); //被保险人年龄
        tTransferData.setNameAndValue("SequenceNo",
                                      "1"); //
//        tTransferData.setNameAndValue("OldContNo","2300002392");
        System.out.println("ContType" + "ContType");

        // 准备传输数据 VData
        System.out.println("tLDPersonSchema2" + tLDPersonSchema);
        VData tVData = new VData();
        tVData.add(tLCContSchema);
        tVData.add(tLDPersonSchema);
        tVData.add(tLCCustomerImpartSet);
        tVData.add(tLCCustomerImpartDetailSet);
        tVData.add(tLCInsuredSchema);
        tVData.add(tLCAddressSchema);
        tVData.add(tOLDLCInsuredDB);
        System.out.println("sadf12312" + tOLDLCInsuredDB.getInsuredNo());

        tVData.add(tTransferData);
        GlobalInput tGI = new GlobalInput();
        tGI.Operator="picch";
        tGI.ManageCom="86";
        tGI.ComCode="86";
        tVData.add(tGI);

        ContInsuredUI tContInsuredUI = new ContInsuredUI();
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        if (tContInsuredUI.submitData(tVData, "UPDATE||CONTINSURED")) {
            CErrors mErrors = new CErrors();
            mErrors.copyAllErrors(tContInsuredUI.mErrors);
            System.out.println(mErrors.getFirstError());
            VData tempVData = tContInsuredUI.getResult();
        }

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        return true;
    }

    /**
     * 得到处理后的结果集
     * @return 结果集
     */

    public VData getResult() {
        return this.mResult;
    }
}
