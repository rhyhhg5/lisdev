/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.ulitb;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Date;

import com.sinosoft.lis.bl.LCDutyBL;
import com.sinosoft.lis.bl.LCGetBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.bl.LCPremBL;
import com.sinosoft.lis.db.LMDutyGetDB;
import com.sinosoft.lis.db.LMDutyPayDB;
import com.sinosoft.lis.db.LMPolDutyEdorCalDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.CalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LMDutyCtrlSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LMDutyGetSchema;
import com.sinosoft.lis.schema.LMDutyPaySchema;
import com.sinosoft.lis.schema.LMDutySchema;
import com.sinosoft.lis.schema.LMRiskDutySchema;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vbl.LCGetBLSet;
import com.sinosoft.lis.vbl.LCPremBLSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LMDutyCtrlSet;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import com.sinosoft.lis.vschema.LMDutyGetRelaSet;
import com.sinosoft.lis.vschema.LMDutyPayRelaSet;
import com.sinosoft.lis.vschema.LMPolDutyEdorCalSet;
import com.sinosoft.lis.vschema.LMRiskDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

/*
 * <p>Title: 保费计算类 </p> <p>Description: 通过传入的保单信息和责任信息构建出保费信息和领取信息 </p> <p>Copyright:
 * Copyright (c) 2002</p> <p>Company: sinosoft</p> @author HST
 * 
 * @version 1.0 @date 2002-07-01
 */

public class CalBL {
	// @Field
	private FDate fDate = new FDate();

	public CErrors mErrors = new CErrors(); // 错误信息

	private boolean mFlag = false; // 记录Duty的传入方式，true传入没有DutyCode的DutySet

	private boolean mGetFlag = false; // 记录Get的传入方式，true传入没有GetDutyCode的GetSet

	private boolean noCalFlag = false; // 不需要计算保费保额的标记

	private Connection con;

	private boolean isConCanUse = false;

	private ExeSQL mExeSQL;

	private LCPolBL mLCPolBL = new LCPolBL(); // 保单信息

	private LCDutySchema mLCDutyBLIn; // 录入的与责任相关的信息

	private LCDutyBLSet mLCDutyBLSet; // 责任信息

	private LCGetSchema mLCGetBLIn; // 录入的与领取相关的信息

	private LCGetBLSet mLCGetSetIn; // 传入的领取信息

	private LCPremBLSet mLCPremBLSet = new LCPremBLSet(); // 保费项信息

	private LCGetBLSet mLCGetBLSet = new LCGetBLSet(); // 领取项信息

	private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();// 被保人信息

	private LMDutyCtrlSet mLMDutyCtrlSet; // 录入控制信息

	private CalBase mCalBase; // 计算基础数据

	private String mOperator = "";

	private String calType;

	private String currCalMode = "";

	private boolean tag = false;

	// private final double OriginPrem = 0; //更新时保存原先保单的实际保费
	// private final double OriginStandPrem = 0; //更新时保存原先保单的标准保费

	/* 转换精确位数的对象 */
	// private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数
	// private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL);
	// //数字转换对象
	private final int mSCALE = 2;// 保费保额计算出来后的精确位数

	private String FRateFORMATMODOL = "0.000000000000"; // 浮动费率计算出来后的精确位数

	private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); // 数字转换对象

	/* 处理浮动费率的变量 */
	private double FloatRate = 0; // 浮动费率

	private double InputPrem = 0; // 保存界面录入的保费

	private double InputAmnt = 0; // 保存界面录入的保额

	private boolean autoCalFloatRateFlag = false; // 是否自动计算浮动费率,如果界面传入浮动费率=ConstRate，自动计算

	private final double ConstRate = -1; // 如果标明需要从输入的保费保额计算浮动费率，那么界面传入的浮动费率值为常量

	private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();
	
	public boolean returnflag1 = true;
	
	public boolean getreturnflag() {  		
		return returnflag1 ;
		}


	// @Constructor
	public CalBL(LCPolBL aLCPolBL, String calFlag) {
		mExeSQL = new ExeSQL();
		mLCPolBL.setSchema(aLCPolBL);
		mLCDutyBLIn = new LCDutySchema();
		mLCGetBLIn = new LCGetSchema();
		calType = calFlag;
	}

	public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet, String calFlag) {
		mExeSQL = new ExeSQL();
		mLCPolBL.setSchema(tLCPolBL); // 保存界面录入的投保单信息
		mLCGetBLIn = new LCGetSchema();

		if (tLCDutyBLSet.size() == 1
				&& (tLCDutyBLSet.get(1).getDutyCode() == null || tLCDutyBLSet
						.get(1).getDutyCode().equals(""))) {
			mLCDutyBLIn = tLCDutyBLSet.get(1); // 保存界面录入的责任信息-单条
		} else {
			mLCDutyBLIn = new LCDutySchema(); // 无数据
			mLCDutyBLSet = new LCDutyBLSet();
			mLCDutyBLSet.set(tLCDutyBLSet); // 保存界面录入的责任信息-多条
			mFlag = true;
		}
		calType = calFlag;
	}

	// 2007.3.30新增加的 对于GIL处理的特殊
	public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet, String calFlag,
			boolean tag) {
		mExeSQL = new ExeSQL();
		mLCPolBL.setSchema(tLCPolBL); // 保存界面录入的投保单信息
		mLCGetBLIn = new LCGetSchema();

		if (tLCDutyBLSet.size() == 1
				&& (tLCDutyBLSet.get(1).getDutyCode() == null || tLCDutyBLSet
						.get(1).getDutyCode().equals(""))) {
			mLCDutyBLIn = tLCDutyBLSet.get(1); // 保存界面录入的责任信息-单条
		} else {
			mLCDutyBLIn = new LCDutySchema(); // 无数据
			mLCDutyBLSet = new LCDutyBLSet();
			mLCDutyBLSet.set(tLCDutyBLSet); // 保存界面录入的责任信息-多条
			mFlag = true;
		}
		this.tag = tag;
		if (tag) {
			setNoCalFalg(false); // 将是否需要计算的标记传入计算类中
//			boolean flag = calPol();
			returnflag1 = calPol(); // 将保费计算状态存入returnflag
		}
		calType = calFlag;
	}

	public CalBL(LCPolBL tLCPolBL, LCGetBLSet tLCGetBLSet, String calFlag) {
		mExeSQL = new ExeSQL();
		mLCPolBL.setSchema(tLCPolBL);
		mLCDutyBLIn = new LCDutySchema();

		if (tLCGetBLSet.size() == 1
				&& StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode())
						.equals("")) {
			mLCGetBLIn = tLCGetBLSet.get(1);
		} else {
			mLCGetBLIn = new LCGetSchema();
			mLCGetSetIn = new LCGetBLSet();
			mLCGetSetIn.set(tLCGetBLSet);
			mGetFlag = true;
		}
		calType = calFlag;
	}

	public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet,
			LCGetBLSet tLCGetBLSet, String calFlag) {
		mExeSQL = new ExeSQL();
		mLCPolBL.setSchema(tLCPolBL); // 保存界面录入的投保单信息

		// 责任
		if (tLCDutyBLSet.size() == 1
				&& (tLCDutyBLSet.get(1).getDutyCode() == null || tLCDutyBLSet
						.get(1).getDutyCode().equals(""))) {
			mLCDutyBLIn = tLCDutyBLSet.get(1); // 保存界面录入的责任信息-单条
		} else {
			mLCDutyBLIn = new LCDutySchema(); // 无数据
			mLCDutyBLSet = new LCDutyBLSet();
			mLCDutyBLSet.set(tLCDutyBLSet); // 保存界面录入的责任信息-多条
			mFlag = true; // 可选责任为真
		}

		// 领取项
		if (tLCGetBLSet.size() == 1
				&& StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode())
						.equals("")) {
			mLCGetBLIn = tLCGetBLSet.get(1); // 保存界面录入的领取项信息-其它字段为空，GetDutyKind字段有值
		} else {
			mLCGetBLIn = new LCGetSchema();
			mLCGetSetIn = new LCGetBLSet();
			mLCGetSetIn.set(tLCGetBLSet);
			mGetFlag = true; // 领取项为真
		}

		calType = calFlag;
	}

	public CalBL(LCPolBL aLCPolBL, String calFlag, Connection aConnection) {
		this.con = aConnection;
		this.isConCanUse = true;
		mExeSQL = new ExeSQL(this.con);
		mLCPolBL.setSchema(aLCPolBL);
		mLCDutyBLIn = new LCDutySchema();
		mLCGetBLIn = new LCGetSchema();
		calType = calFlag;
	}

	public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet, String calFlag,
			Connection aConnection) {
		this.con = aConnection;
		this.isConCanUse = true;
		mExeSQL = new ExeSQL(this.con);
		mLCPolBL.setSchema(tLCPolBL); // 保存界面录入的投保单信息
		mLCGetBLIn = new LCGetSchema();

		if (tLCDutyBLSet.size() == 1
				&& (tLCDutyBLSet.get(1).getDutyCode() == null || tLCDutyBLSet
						.get(1).getDutyCode().equals(""))) {
			mLCDutyBLIn = tLCDutyBLSet.get(1); // 保存界面录入的责任信息-单条
		} else {
			mLCDutyBLIn = new LCDutySchema(); // 无数据
			mLCDutyBLSet = new LCDutyBLSet();
			mLCDutyBLSet.set(tLCDutyBLSet); // 保存界面录入的责任信息-多条
			mFlag = true;
		}
		calType = calFlag;
	}

	// 2007.3.30新增加的 对于GIL处理的特殊
	public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet, String calFlag,
			boolean tag, Connection aConnection) {
		this.con = aConnection;
		this.isConCanUse = true;
		mExeSQL = new ExeSQL(this.con);
		mLCPolBL.setSchema(tLCPolBL); // 保存界面录入的投保单信息
		mLCGetBLIn = new LCGetSchema();

		if (tLCDutyBLSet.size() == 1
				&& (tLCDutyBLSet.get(1).getDutyCode() == null || tLCDutyBLSet
						.get(1).getDutyCode().equals(""))) {
			mLCDutyBLIn = tLCDutyBLSet.get(1); // 保存界面录入的责任信息-单条
		} else {
			mLCDutyBLIn = new LCDutySchema(); // 无数据
			mLCDutyBLSet = new LCDutyBLSet();
			mLCDutyBLSet.set(tLCDutyBLSet); // 保存界面录入的责任信息-多条
			mFlag = true;
		}
		this.tag = tag;
		if (tag) {
			setNoCalFalg(false); // 将是否需要计算的标记传入计算类中
			boolean flag = calPol();
		}
		calType = calFlag;
	}

	public CalBL(LCPolBL tLCPolBL, LCGetBLSet tLCGetBLSet, String calFlag,
			Connection aConnection) {
		this.con = aConnection;
		this.isConCanUse = true;
		mExeSQL = new ExeSQL(this.con);
		mLCPolBL.setSchema(tLCPolBL);
		mLCDutyBLIn = new LCDutySchema();

		if (tLCGetBLSet.size() == 1
				&& StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode())
						.equals("")) {
			mLCGetBLIn = tLCGetBLSet.get(1);
		} else {
			mLCGetBLIn = new LCGetSchema();
			mLCGetSetIn = new LCGetBLSet();
			mLCGetSetIn.set(tLCGetBLSet);
			mGetFlag = true;
		}
		calType = calFlag;
	}

	public CalBL(LCPolBL tLCPolBL, LCDutyBLSet tLCDutyBLSet,
			LCGetBLSet tLCGetBLSet, String calFlag, Connection aConnection) {
		this.con = aConnection;
		this.isConCanUse = true;
		mExeSQL = new ExeSQL(this.con);
		mLCPolBL.setSchema(tLCPolBL); // 保存界面录入的投保单信息

		// 责任
		if (tLCDutyBLSet.size() == 1
				&& (tLCDutyBLSet.get(1).getDutyCode() == null || tLCDutyBLSet
						.get(1).getDutyCode().equals(""))) {
			mLCDutyBLIn = tLCDutyBLSet.get(1); // 保存界面录入的责任信息-单条
		} else {
			mLCDutyBLIn = new LCDutySchema(); // 无数据
			mLCDutyBLSet = new LCDutyBLSet();
			mLCDutyBLSet.set(tLCDutyBLSet); // 保存界面录入的责任信息-多条
			mFlag = true; // 可选责任为真
		}

		// 领取项
		if (tLCGetBLSet.size() == 1
				&& StrTool.cTrim(tLCGetBLSet.get(1).getGetDutyCode())
						.equals("")) {
			mLCGetBLIn = tLCGetBLSet.get(1); // 保存界面录入的领取项信息-其它字段为空，GetDutyKind字段有值
		} else {
			mLCGetBLIn = new LCGetSchema();
			mLCGetSetIn = new LCGetBLSet();
			mLCGetSetIn.set(tLCGetBLSet);
			mGetFlag = true; // 领取项为真
		}

		calType = calFlag;
	}

	// @Method
	/**
	 * 计算函数
	 * 
	 * @return boolean
	 */
	public boolean calPol() {
		// 准备描述的相关信息(查询责任录入控制信息-mLMDutyCtrlSet)
		if (!this.preDefineInfo()) {
			return false;
		}

		// System.out.println("after preDefineInfo()");
		if (mLCDutyBLSet == null) {
			// 取得必选的责任-通过描述取得责任信息的结构,并将界面录入的责任信息存入-mLCDutyBLSet
			if (!this.getDutyStructure()) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calPol";
				tError.errorMessage = "getDutyStructure方法返回失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		// 从传入的保单信息中获取责任表中需要计算的以外的信息
		this.getDutyOtherData();

		// System.out.println("after getDutyOtherData()");

		int n = mLCDutyBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCDutySchema tLCDutySchema = mLCDutyBLSet.get(i);

			// 准备录入部分的信息
			if (mFlag) {
				mLCDutyBLIn.setSchema(tLCDutySchema);
			}
			// 计算责任表中的部分信息(保险期间，交费期间，领取期间等)
			tLCDutySchema = this.calDutyOther(tLCDutySchema);
			if (tLCDutySchema == null) {
				return false;
			}
			// System.out.println("计算责任表下的保费项和领取项信息");
			// 计算责任表下的保费项和领取项信息
			tLCDutySchema = this.calOneDuty(tLCDutySchema);
			if (this.mErrors.needDealError()) {
				return false;
			}

			// 增加浮动费率处理
			mLCDutyBLSet.set(i, tLCDutySchema);
		}

		// 通过责任信息计算保单信息
		this.calPolFromDuty();

		return true;
	}

	/**
	 * 当不需要计算保费保额时调用该函数，增加了对保费保额的处理
	 * 
	 * @param pLCPremSet
	 *            LCPremSet
	 * @return boolean
	 */
	public boolean calPol2(LCPremSet pLCPremSet) {
		if (!calPol()) {
			return false;
		}

		double sumPrem = 0;
		for (int i = 1; i <= pLCPremSet.size(); i++) {
			sumPrem += pLCPremSet.get(i).getStandPrem();
			for (int n = 1; n <= mLCPremBLSet.size(); n++) // 找到对应的保费项修改金额
			{
				if (pLCPremSet.get(i).getDutyCode().equals(
						mLCPremBLSet.get(n).getDutyCode())
						&& pLCPremSet.get(i).getPayPlanCode().equals(
								mLCPremBLSet.get(n).getPayPlanCode())) {

					mLCPremBLSet.get(n).setPrem(pLCPremSet.get(i).getPrem());
					mLCPremBLSet.get(n).setSumPrem(
							mLCPremBLSet.get(n).getSumPrem()
									+ mLCPremBLSet.get(n).getPrem());
					mLCPremBLSet.get(n).setRate(pLCPremSet.get(i).getRate());
					// 对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
					// 但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
					mLCPremBLSet.get(n).setStandPrem(
							mLCPremBLSet.get(n).getPrem());

					// 保全人工核保处数据的需要add by sxy 2004-03-16
					// mLCPremBLSet.get(n).setState("0") ;

					mLCPremBLSet.get(n).setOperator(
							pLCPremSet.get(i).getOperator());
					mLCPremBLSet.get(n).setMakeDate(
							pLCPremSet.get(i).getMakeDate());
					mLCPremBLSet.get(n).setMakeTime(
							pLCPremSet.get(i).getMakeTime());

					break;
				}

			}
			for (int m = 1; m <= mLCDutyBLSet.size(); m++) // 找到对应的责任项累积金额
			{
				mLCDutyBLSet.get(m).setAmnt(0); // 保额置0
				mLCDutyBLSet.get(m).setRiskAmnt(0); // 风险保额
				if (pLCPremSet.get(i).getDutyCode().equals(
						mLCDutyBLSet.get(m).getDutyCode())) {
					mLCDutyBLSet.get(m).setPrem(
							mLCDutyBLSet.get(m).getPrem()
									+ pLCPremSet.get(i).getPrem());
					mLCDutyBLSet.get(m).setSumPrem(
							mLCDutyBLSet.get(m).getPrem());
					// 对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
					// 但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
					mLCDutyBLSet.get(m).setStandPrem(
							mLCDutyBLSet.get(m).getPrem());

					break;
				}
			}
		}
		mLCPolBL.setPrem(sumPrem);
		// 对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
		// 但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
		mLCPolBL.setStandPrem(sumPrem);
		mLCPolBL.setSumPrem(sumPrem);
		mLCPolBL.setAmnt(0); // 保额置0
		mLCPolBL.setRiskAmnt(0); // 风险保额

		for (int x = 1; x <= mLCGetBLSet.size(); x++) {
			mLCGetBLSet.get(x).setStandMoney(0); // 保额置0
			mLCGetBLSet.get(x).setActuGet(0); // 保额置0
		}

		return true;
	}

	/**
	 * 可用输入的保费项作为参数进行计算
	 * 
	 * @return boolean
	 */
	public boolean calPol3() {

		// 准备描述的相关信息(查询责任录入控制信息-mLMDutyCtrlSet)
		if (!this.preDefineInfo()) {
			return false;
		}

		// System.out.println("after preDefineInfo()");

		if (mLCDutyBLSet == null) {
			// 取得必选的责任-通过描述取得责任信息的结构,并将界面录入的责任信息存入-mLCDutyBLSet
			if (!this.getDutyStructure()) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calPol";
				tError.errorMessage = "getDutyStructure方法返回失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		// 从传入的保单信息中获取责任表中需要计算的以外的信息
		this.getDutyOtherData();

		// System.out.println("after getDutyOtherData()");

		int n = mLCDutyBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCDutySchema tLCDutySchema = mLCDutyBLSet.get(i);

			// 准备录入部分的信息
			if (mFlag) {
				mLCDutyBLIn.setSchema(tLCDutySchema);
			}
			// 计算责任表中的部分信息(保险期间，交费期间，领取期间等)
			tLCDutySchema = this.calDutyOther(tLCDutySchema);
			if (tLCDutySchema == null) {
				return false;
			}
			// System.out.println("计算责任表下的保费项和领取项信息");
			// 计算责任表下的保费项和领取项信息
			tLCDutySchema = this.calOneDuty2(tLCDutySchema);
			if (this.mErrors.needDealError()) {
				return false;
			}

			// 增加浮动费率处理
			mLCDutyBLSet.set(i, tLCDutySchema);
		}

		// 通过责任信息计算保单信息
		this.calPolFromDuty();

		return true;
	}

	/**
	 * 准备描述数据--准备mLMDutyCtrlSet数据
	 * 
	 * @return boolean
	 */
	private boolean preDefineInfo() {
		// 责任录入控制信息
		mLMDutyCtrlSet = new LMDutyCtrlSet();

		LMRiskDutySet tLMRiskDutySet = mCRI
				.findRiskDutyByRiskCodeClone(mLCPolBL.getRiskCode());

		if (tLMRiskDutySet == null) {
			// @@错误处理
			// this.mErrors.copyAllErrors(tLMRiskDutyDB.mErrors);
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "preDefineInfo";
			tError.errorMessage = "LMRiskDuty表查询失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// System.out.println("in preDefineInfo()");

		int n = tLMRiskDutySet.size();
		for (int i = 1; i <= n; i++) {
			LMRiskDutySchema tLMRiskDutySchema = tLMRiskDutySet.get(i);

			LMDutyCtrlSet tLMDutyCtrlSet = mCRI
					.findDutyCtrlByDutyCodeClone(tLMRiskDutySchema
							.getDutyCode());

			if (tLMDutyCtrlSet == null) {
				// @@错误处理
				this.mErrors.copyAllErrors(mCRI.mErrors);
				mCRI.mErrors.clearErrors();

				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "preDefineInfo";
				tError.errorMessage = "LMDutyCtrl表查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			mLMDutyCtrlSet.add(tLMDutyCtrlSet);
		}

		return true;
	}

	/**
	 * 判断该字段的内容的取值位置是否为录入值 录入项代码、录入位置（责任、领取项、保费项）、字段名称
	 * 
	 * @param code
	 *            String
	 * @param ctrl
	 *            String
	 * @param fieldName
	 *            String
	 * @return boolean
	 */
	private boolean chkInput(String code, String ctrl, String fieldName) {
		int n = mLMDutyCtrlSet.size();
		for (int i = 1; i <= n; i++) {
			LMDutyCtrlSchema tLMDutyCtrlSchema = mLMDutyCtrlSet.get(i);
			if (StrTool.cTrim(ctrl).equals("D")) {
				if (StrTool.cTrim(code).equals(
						StrTool.cTrim(tLMDutyCtrlSchema.getDutyCode()))
						&& StrTool.cTrim(fieldName).toLowerCase().equals(
								StrTool.cTrim(tLMDutyCtrlSchema.getFieldName())
										.toLowerCase())
						&& StrTool.cTrim(tLMDutyCtrlSchema.getCtrlType())
								.equals("D")) {
					if (StrTool.cTrim(tLMDutyCtrlSchema.getInpFlag()).equals(
							"Y")) {
						return true;
					} else {
						return false;
					}
				}
			} else {
				if (StrTool.cTrim(code).equals(
						StrTool.cTrim(tLMDutyCtrlSchema.getOtherCode()))
						&& StrTool.cTrim(fieldName).toLowerCase().equals(
								StrTool.cTrim(tLMDutyCtrlSchema.getFieldName())
										.toLowerCase())
						&& StrTool.cTrim(ctrl).equals(
								StrTool.cTrim(tLMDutyCtrlSchema.getCtrlType()))) {
					if (StrTool.cTrim(tLMDutyCtrlSchema.getInpFlag()).equals(
							"Y")) {
						return true;
					} else {
						return false;
					}
				}
			}
			// end of if
		}
		// end of for
		return false;
	}

	public LCPolBL getLCPol() {
		return mLCPolBL;
	}

	public LCDutyBLSet getLCDuty() {
		return mLCDutyBLSet;
	}

	public LCPremBLSet getLCPrem() {
		return mLCPremBLSet;
	}

	public LCGetBLSet getLCGet() {
		return mLCGetBLSet;
	}

	// add by winnie ASR20093075
	public LCDutySchema getOneLCDuty() {
		return mLCDutyBLIn;
	}

	// end add by
	/**
	 * 通过描述取得保费项信息的结构
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @return boolean
	 */
	private boolean getPremStructure(LCDutySchema mLCDutySchema) {
		LMDutyPayRelaSet tLMDutyPayRelaSet = mCRI
				.findDutyPayRelaByDutyCode(mLCDutySchema.getDutyCode());

		if (tLMDutyPayRelaSet == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "getPremStructure";
			tError.errorMessage = "未找到相关数据!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 把数据装入mLCPremBLSet中
		int n = tLMDutyPayRelaSet.size();
		for (int i = 1; i <= n; i++) {
			// 若当前为公共账户，则不生成个人交费对应的lcprem项
			if (mLCPolBL.getPolTypeFlag().equals("2")) {
				String temCode = tLMDutyPayRelaSet.get(i).getPayPlanCode();
				LMDutyPayDB temLMDutyPayDB = null;
				if (this.isConCanUse)
					temLMDutyPayDB = new LMDutyPayDB(this.con);
				else
					temLMDutyPayDB = new LMDutyPayDB();
				temLMDutyPayDB.setPayPlanCode(temCode);
				if (!temLMDutyPayDB.getInfo()) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "getPremStructure";
					tError.errorMessage = "LMDutyPay表查询失败!";
					this.mErrors.addOneError(tError);
					return false;
				} else if (temLMDutyPayDB.getPayAimClass().equals("1")) {
					System.out.println("公共账户不生成个人交费项！");
					continue;
				}
			} else if (mLCPolBL.getPolTypeFlag().equals("5")) {// 若当前为公共保额，则不生成个人交费对应的lcprem项
				String temCode = tLMDutyPayRelaSet.get(i).getPayPlanCode();
				LMDutyPayDB temLMDutyPayDB = null;
				if (this.isConCanUse)
					temLMDutyPayDB = new LMDutyPayDB(this.con);
				else
					temLMDutyPayDB = new LMDutyPayDB();
				temLMDutyPayDB.setPayPlanCode(temCode);
				if (!temLMDutyPayDB.getInfo()) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "getPremStructure";
					tError.errorMessage = "LMDutyPay表查询失败!";
					this.mErrors.addOneError(tError);
					return false;
				} else if (temLMDutyPayDB.getPayAimClass().equals("1")) {
					System.out.println("公共保额不生成个人交费项！");
					continue;
				}
			}
			LCPremBL tLCPremBL = new LCPremBL();
			tLCPremBL.setDutyCode(mLCDutySchema.getDutyCode());
			tLCPremBL.setPayPlanCode(tLMDutyPayRelaSet.get(i).getPayPlanCode());

			mLCPremBLSet.add(tLCPremBL);
		}
		return true;
	}

	/**
	 * 从保单信息或责任信息中获取保费项表中需要计算的以外的信息
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 */
	private void getPremOtherData(LCDutySchema mLCDutySchema) {
		int n = mLCPremBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);
			// PQ1 设置从LCCont中获得的值
			// 从保单获得的信息
			if (tLCPremSchema.getPolNo() == null) {
				tLCPremSchema.setPolNo(mLCPolBL.getPolNo());
			}
			if (tLCPremSchema.getContNo() == null) {
				tLCPremSchema.setContNo(mLCPolBL.getContNo());
			}
			if (tLCPremSchema.getGrpContNo() == null) {
				tLCPremSchema.setGrpContNo(mLCPolBL.getGrpContNo());
			}

			if (tLCPremSchema.getAppntNo() == null) {
				tLCPremSchema.setAppntNo(mLCPolBL.getAppntNo());
			}
			if (tLCPremSchema.getAppntType() == null) {
				tLCPremSchema.setAppntType(mLCPolBL.getContType());
			}

			if (tLCPremSchema.getPayStartDate() == null) {
				tLCPremSchema.setPayStartDate(mLCPolBL.getCValiDate());
			}
			if (tLCPremSchema.getManageCom() == null) {
				tLCPremSchema.setManageCom(mLCPolBL.getManageCom());
			}
			// ////////////////////////////////////////////////////////////////////////////////////////////
			// 保费项表暂时处理管理费比例的方法是：从保单表的管理费比例（暂时存储）中取出
			// 1。民生常青基业的处理方式是保单的管理费比例和保费项上的管理费比例是一样的，并且保单表和保费表是一一对应的
			// 2。众悦团体年金的处理方式是个单的管理费比例取于团单的管理费比例，故每一张个单的管理费比例相同
			// add by guoxiang at 2004-9-7
			// ///////////////////////////////////////////////////////////////////////////////////////////
			// 从责任表获得的信息
			/*
			 * if (tLCPremSchema.getStandPrem() == 0.0) {
			 * tLCPremSchema.setStandPrem(mLCDutySchema.getStandPrem()); }
			 */

			// 保全保全人工核保的需要add by sxy 2004-03-16
			tLCPremSchema.setState("0");
			mLCPremBLSet.set(i, tLCPremSchema);
		}
	}

	/**
	 * 计算保费项表中保费的值
	 * 
	 * @param mLCPremSchema
	 *            LCPremSchema
	 * @param tLCDutySchema
	 *            LCDutySchema
	 * @param tLMDutyPaySchema
	 *            LMDutyPaySchema
	 * @param calFlag
	 *            String
	 * @return double
	 */
	private double calPremValue(LCPremSchema mLCPremSchema,
			LCDutySchema tLCDutySchema, LMDutyPaySchema tLMDutyPaySchema,
			String calFlag) {
		double mValue = -1;
		currCalMode = calFlag;
		String mCalCode = null;
		if (StrTool.cTrim(calFlag).equals("P")) {
			mCalCode = tLMDutyPaySchema.getCnterCalCode(); // 保费算保额
		}
		if (StrTool.cTrim(calFlag).equals("G")) {
			mCalCode = tLMDutyPaySchema.getCalCode(); // 保额算保费
		}
		if (StrTool.cTrim(calFlag).equals("O")) {
			mCalCode = tLMDutyPaySchema.getOthCalCode(); // 其他算保费
		}
		// 根据实际保费算保额情况记录保费保额计算方向，在保单重算时使用
		if (StrTool.cTrim(calFlag).equals("A")
				&& tLCDutySchema.getStandPrem() != 0.0) {
			mCalCode = tLMDutyPaySchema.getCnterCalCode(); // 保费、保额互算{
			currCalMode = "P";
		}
		if (StrTool.cTrim(calFlag).equals("A")
				&& tLCDutySchema.getAmnt() != 0.0) {
			mCalCode = tLMDutyPaySchema.getCalCode(); // 保费、保额互算{
			currCalMode = "G";
		}
		if (StrTool.cTrim(calFlag).equals("I")) {
			mCalCode = tLMDutyPaySchema.getCnterCalCode(); // 录入保费保额
		}

		// 取默认值
		if (StrTool.cTrim(mCalCode).equals("")) {
			mValue = tLMDutyPaySchema.getDefaultVal();
			return mValue;
		}
	
		// 对于有GIL值,回来重算的特殊处理.强行按保额算保费
		if (tag) {
			mCalCode = tLMDutyPaySchema.getCalCode(); // 保额算保费
		}

		// 计算
		Calculator mCalculator = null;
		if (this.isConCanUse)
			mCalculator = new Calculator(this.con);
		else
			mCalculator = new Calculator();
		mCalculator.setCalCode(mCalCode);
		// 增加基本要素
		mCalculator.addBasicFactor("Get", mCalBase.getGet());
		mCalculator.addBasicFactor("Mult", mCalBase.getMult());
		mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
		mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
		mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
		mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
		mCalculator.addBasicFactor("Sex", mCalBase.getSex());
		mCalculator.addBasicFactor("Job", mCalBase.getJob());
		mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
		mCalculator.addBasicFactor("PayEndYearFlag", mCalBase
				.getPayEndYearFlag());
		mCalculator.addBasicFactor("GetStartDate", "");
		mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
		mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
		mCalculator.addBasicFactor("Years", mCalBase.getYears());
		mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
		mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
		mCalculator.addBasicFactor("Grp", "");
		mCalculator.addBasicFactor("GetFlag", "");
		
		if(mCalBase.getCalType()!=null && !mCalBase.getCalType().equals("") && (mCalBase.getCalType().equals("NI")||mCalBase.getCalType().equals("LC")||mCalBase.getCalType().equals("CS")))
		{
       	 	String sqlStr = " select GrpContNo_cvalidate('"+mCalBase.getGrpContNo()+"','"+mCalBase.getGrpPolNo()+"','"+mCalBase.getCValiDate()+"') From dual "; //如果是指定的保单，当月1号，返回当天，否则返回下月1号
       	 	String tCValiDate = mExeSQL.getOneValue(sqlStr);
			mCalculator.addBasicFactor("CValiDate", tCValiDate);
			System.out.println(mCalBase.getGrpContNo()+":"+mCalBase.getRiskCode()+"生效日期处理后为："+tCValiDate);
		}
		else
		{
			mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
		}
		mCalculator.addBasicFactor("Count", mCalBase.getCount());
		mCalculator.addBasicFactor("FirstPayDate", "");
		mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
		mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
		mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
		mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
		mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
		mCalculator.addBasicFactor("GetDutyKind", mCalBase.getGetDutyKind());
		mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
		mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
		mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
//		mCalculator.addBasicFactor("StandbyFlag4", mCalBase.getStandbyFlag4());
		// zhangjq 2008-4-18
		// 加setStandbyFlag5、setStandbyFlag6（NMK04险种用StandbyFlag1~StandbyFlag6保存赔付倍数）
//		mCalculator.addBasicFactor("StandbyFlag5", mCalBase.getStandbyFlag5());
//		mCalculator.addBasicFactor("StandbyFlag6", mCalBase.getStandbyFlag6());
		mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
		mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
		mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
		mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
		mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
		mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
		mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
//		mCalculator.addBasicFactor("ContPlanCode", mCalBase.getContPlanCode());
//		mCalculator.addBasicFactor("InsuredNo", mCalBase.getInsuredNo());
		mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
		mCalculator.addBasicFactor("RiskCode", mCalBase.getRiskCode());
//		mCalculator.addBasicFactor("DutyCode", mCalBase.getDutyCode());
		mCalculator.addBasicFactor("CalRule", mCalBase.getCalRule());
//		mCalculator.addBasicFactor("Salary", mCalBase.getSalary());
		// hm 2008-10-28 加SysContPlanCode保险套餐编码. 只有表定费率的套餐，此项才有值
		// add by hanming
//		mCalculator.addBasicFactor("PolTypeFlag", mCalBase.getPolTypeFlag());

		String tStr = "";
		tStr = mCalculator.calculate();
		if (tStr == null || tStr.trim().equals("")) {
			mValue = 0;
		} else {
			mValue = Double.parseDouble(tStr);
		}
		return mValue;
	}

	/**
	 * 计算保费项表中的其他值
	 * 
	 * @param mLCPremSchema
	 *            LCPremSchema
	 * @param tLMDutyPaySchema
	 *            LMDutyPaySchema
	 * @param tLCDutySchema
	 *            LCDutySchema
	 * @return LCPremSchema
	 */
	private LCPremSchema calPremOther(LCPremSchema mLCPremSchema,
			LMDutyPaySchema tLMDutyPaySchema, LCDutySchema tLCDutySchema) {
		mLCPremSchema.setPayPlanType(tLMDutyPaySchema.getType());
		mLCPremSchema.setUrgePayFlag(tLMDutyPaySchema.getUrgePayFlag());
		mLCPremSchema.setNeedAcc(tLMDutyPaySchema.getNeedAcc());
		mLCPremSchema.setState("0");

		// 判断 PayIntv 的取值位置
		if (chkInput(mLCPremSchema.getPayPlanCode(), "P", "PayIntv")) {
			mLCPremSchema.setPayIntv(mLCDutyBLIn.getPayIntv());
		} else {
			/** 保持lcpol与lcprem的PayIntv一致 */
			mLCPremSchema.setPayIntv(mLCDutyBLIn.getPayIntv());
			// mLCPremSchema.setPayIntv(tLMDutyPaySchema.getPayIntv());
		}

		// 计算缴费终止日期
		Date baseDate = null;
		Date compDate = null;
		int interval = 0;
		String unit = null;
		Date payEndDate = null;
		int payEndYear = 0;
		String payEndYearFlag = null;

		// 判断 payEndYear 的取值位置
		if (chkInput(mLCPremSchema.getPayPlanCode(), "P", "PayEndYear")) {
			if (mLCDutyBLIn.getPayEndYear() == 0) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calPremOther";
				tError.errorMessage = "必须录入缴费终止年期!";
				this.mErrors.addOneError(tError);
			}
			payEndYear = mLCDutyBLIn.getPayEndYear();
		} else {
			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
					"3")) {
				// 取保险终止日期
				payEndYear = tLCDutySchema.getInsuYear();
			}

			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
					"4")) {
				// 取领取开始日期
				payEndYear = tLCDutySchema.getGetYear();
			}

			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
					"0")) {
				// 以计算为准
				payEndYear = tLMDutyPaySchema.getPayEndYear();
			}
		}

		// 判断 payEndYearFlag 的取值位置
		if (chkInput(mLCPremSchema.getPayPlanCode(), "P", "PayEndYearFlag")) {
			if (mLCDutyBLIn.getPayEndYearFlag() == null) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calPremOther";
				tError.errorMessage = "必须录入缴费终止年期标志!";
				System.out.println("必须录入缴费终止年期标志:请检查责任项的payendyearflag标记");
				this.mErrors.addOneError(tError);
			}
			payEndYearFlag = mLCDutyBLIn.getPayEndYearFlag();
		} else {
			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
					"3")) {
				// 取保险终止日期
				payEndYearFlag = tLCDutySchema.getInsuYearFlag();
			}

			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
					"4")) {
				// 取领取开始日期
				payEndYearFlag = tLCDutySchema.getGetYearFlag();
			}

			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals(
					"0")) {
				// 以计算为准
				payEndYearFlag = tLMDutyPaySchema.getPayEndYearFlag();
			}
		}

		// 计算
		if (StrTool.cTrim(payEndYearFlag).equals("A")) {
			baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
			unit = "Y";

			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalRef()).equals(
					"S")) {
				// 起保日期对应日
				compDate = fDate.getDate(mLCPolBL.getCValiDate());
			}
			if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalRef()).equals(
					"B")) {
				// 生日对应日
				compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
			}
		} else {
			baseDate = fDate.getDate(mLCPolBL.getCValiDate());
			unit = payEndYearFlag;
		}
		interval = payEndYear;

		if (StrTool.cTrim(tLMDutyPaySchema.getPayEndDateCalMode()).equals("4")) {
			// 取领取开始日期
			if (StrTool.cTrim(tLCDutySchema.getGetStartType()).equals("B")) {
				compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
			}

			if (StrTool.cTrim(tLCDutySchema.getGetStartType()).equals("S")) {
				compDate = fDate.getDate(mLCPolBL.getCValiDate());
			}
		}
		// 中意要求止期减一天 张阔
		payEndDate = PubFun.calDate(baseDate, interval, unit, compDate);
		mLCPremSchema.setPayEndDate(fDate.getString(payEndDate));

		// 准备计算基础数据部分
		mCalBase.setPayEndYear(payEndYear);
		mCalBase.setPayEndYearFlag(payEndYearFlag);

		return mLCPremSchema;
	}

	/**
	 * 通过描述取得保费项信息的结构
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @return boolean
	 */
	private boolean getGetStructure(LCDutySchema mLCDutySchema) {
		LMDutyGetRelaSet tLMDutyGetRelaSet = mCRI
				.findDutyGetRelaByDutyCode(mLCDutySchema.getDutyCode());

		if (tLMDutyGetRelaSet == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "getGetStructure";
			tError.errorMessage = "未找到相关数据!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 把数据装入mLCGetBLSet中
		int n = tLMDutyGetRelaSet.size();
		for (int i = 1; i <= n; i++) {
			LCGetBL tLCGetBL = new LCGetBL();
			tLCGetBL.setDutyCode(mLCDutySchema.getDutyCode());
			tLCGetBL.setGetDutyCode(tLMDutyGetRelaSet.get(i).getGetDutyCode());

			mLCGetBLSet.add(tLCGetBL);
		}
		return true;
	}

	/**
	 * 从保单信息或责任信息中获取领取项表中需要计算的以外的信息
	 */
	private void getGetOtherData() {
		int n = mLCGetBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);
			// PQ1 需要设一些从LCCont获得的值
			// 从保单获得的信息
			tLCGetSchema.setPolNo(mLCPolBL.getPolNo());
			tLCGetSchema.setContNo(mLCPolBL.getContNo());
			tLCGetSchema.setGrpContNo(mLCPolBL.getGrpContNo());
			// tLCGetSchema.setAppntNo( mLCPolBL.getAppntNo() );
			// tLCGetSchema.setAppntType( mLCPolBL.getContType() );

			tLCGetSchema.setInsuredNo(mLCPolBL.getInsuredNo());

			tLCGetSchema.setGetMode(mLCPolBL.getLiveGetMode());
			tLCGetSchema.setManageCom(mLCPolBL.getManageCom());

			// 中意，设置发放标准及发放机构
//			tLCGetSchema.setSendDate(mLCPolBL.getSendDate());
//			tLCGetSchema.setAnnuityDate(mLCPolBL.getAnnuityDate());
//			tLCGetSchema.setSendComCode(mLCPolBL.getSendComCode());
//			tLCGetSchema.setSendInnerCode(mLCPolBL.getSendInnerCode());
//			tLCGetSchema.setSendStandard(mLCPolBL.getSendStandard());
//			tLCGetSchema.setRetireType(mLCPolBL.getRetireType());
//			tLCGetSchema.setContNoSub(mLCPolBL.getContNoSub());
//			tLCGetSchema.setInsuredStat(mLCPolBL.getInsuredStat());
//			tLCGetSchema.setPrtNo(mLCPolBL.getPrtNo());
//			tLCGetSchema.setStandardStartDate(mLCPolBL.getStandardStartDate());
//			tLCGetSchema.setStockFlag(mLCPolBL.getStockFlag());

			mLCGetBLSet.set(i, tLCGetSchema);
		}
	}

	/**
	 * 计算领取项表中实际领取的值
	 * 
	 * @param mLCGetSchema
	 *            LCGetSchema
	 * @param tLCDutySchema
	 *            LCDutySchema
	 * @param tLMDutyGetSchema
	 *            LMDutyGetSchema
	 * @param calFlag
	 *            String
	 * @return double
	 */
	private double calGetValue(LCGetSchema mLCGetSchema,
			LCDutySchema tLCDutySchema, LMDutyGetSchema tLMDutyGetSchema,
			String calFlag) {
		double mValue = -1;

		String mCalCode = null;
		if (StrTool.cTrim(calFlag).equals("P")) {
			mCalCode = tLMDutyGetSchema.getCalCode(); // 保费算保额
		}
		if (StrTool.cTrim(calFlag).equals("G")) {
			mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 保额算保费
		}
		if (StrTool.cTrim(calFlag).equals("O")) {
			mCalCode = tLMDutyGetSchema.getOthCalCode(); // 其他算保费
		}
		if (StrTool.cTrim(calFlag).equals("A")
				&& tLCDutySchema.getStandPrem() != 0.0) {
			mCalCode = tLMDutyGetSchema.getCalCode(); // 保费、保额互算
		}
		if (StrTool.cTrim(calFlag).equals("A")
				&& tLCDutySchema.getAmnt() != 0.0) {
			mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 保费、保额互算
		}
		if (StrTool.cTrim(calFlag).equals("I")) {
			mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 录入保费保额
		}

		// 取默认值
		if (StrTool.cTrim(mCalCode).equals("")) {
			mValue = tLMDutyGetSchema.getDefaultVal();
			return mValue;
		}
		// 对于有GIL值,回来重算的特殊处理.强行按保额算保费
		if (tag) {
			mCalCode = tLMDutyGetSchema.getCnterCalCode(); // 保额算保费
		}

		// 计算
		Calculator mCalculator = null;
		if (this.isConCanUse)
			mCalculator = new Calculator(this.con);
		else
			mCalculator = new Calculator();
		mCalculator.setCalCode(mCalCode);
		// 增加基本要素
		mCalculator.addBasicFactor("Get", mCalBase.getGet());
		mCalculator.addBasicFactor("Mult", mCalBase.getMult());
		mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
		mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
		mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
		mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
		mCalculator.addBasicFactor("Sex", mCalBase.getSex());
		mCalculator.addBasicFactor("Job", mCalBase.getJob());
		mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
		mCalculator.addBasicFactor("PayEndYearFlag", mCalBase
				.getPayEndYearFlag());
		mCalculator.addBasicFactor("GetStartDate", "");
		mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
		mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
		mCalculator.addBasicFactor("Years", mCalBase.getYears());
		mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
		mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
		mCalculator.addBasicFactor("Grp", "");
		mCalculator.addBasicFactor("GetFlag", "");
		mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
		mCalculator.addBasicFactor("Count", mCalBase.getCount());
		mCalculator.addBasicFactor("FirstPayDate", "");
		mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
		mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
		mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
		mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
		mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
		mCalculator.addBasicFactor("GetDutyKind", mCalBase.getGetDutyKind());
		mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
		mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
		mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
//		mCalculator.addBasicFactor("StandbyFlag4", mCalBase.getStandbyFlag4());
		mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
		mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
		mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
		mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
		mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
		mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
		mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
//		mCalculator.addBasicFactor("Salary", mCalBase.getSalary());
//		mCalculator.addBasicFactor("DutyCode", mCalBase.getDutyCode());
		// add by hanming
//		mCalculator.addBasicFactor("PolTypeFlag", mCalBase.getPolTypeFlag());

		// 中意年金
//		mCalculator.addBasicFactor("SendMoney", "" + mLCPolBL.getSendMoney());

		String tStr = "";
		tStr = mCalculator.calculate();
		if (tStr.trim().equals("")) {
			mValue = 0;
		} else {
			mValue = Double.parseDouble(tStr);
		}
		return mValue;
	}

	/**
	 * 计算领取项表中的其他值
	 * 
	 * @param mLCGetSchema
	 *            LCGetSchema
	 * @param tLMDutyGetSchema
	 *            LMDutyGetSchema
	 * @param tLCDutySchema
	 *            LCDutySchema
	 * @return LCGetSchema
	 */
	private LCGetSchema calGetOther(LCGetSchema mLCGetSchema,
			LMDutyGetSchema tLMDutyGetSchema, LCDutySchema tLCDutySchema) {
		mLCGetSchema.setLiveGetType(tLMDutyGetSchema.getType());
		mLCGetSchema.setNeedAcc(tLMDutyGetSchema.getNeedAcc());
		mLCGetSchema.setCanGet(tLMDutyGetSchema.getCanGet());
		mLCGetSchema.setNeedCancelAcc(tLMDutyGetSchema.getNeedCancelAcc());

		// DiscntFlag 1-只能现金领取, 0-可以选择。
		if (tLMDutyGetSchema.getDiscntFlag() == null) // 如果是空，默认是0
		{
			tLMDutyGetSchema.setDiscntFlag("0");
		}
		if (tLMDutyGetSchema.getDiscntFlag().equals("1")) // 如果是1，则领取项必须是现金
		{
			// 可以查找该行，前面已使用,如果用于接收界面输入的领取方式，若描述表中是1，则将该值覆盖。
			mLCGetSchema.setGetMode("1");
		} else {
			mLCGetSchema.setGetMode(mLCPolBL.getLiveGetMode());
		}

		LMDutyGetAliveSchema tLMDutyGetAliveSchema = new LMDutyGetAliveSchema();
		if (StrTool.cTrim(tLMDutyGetSchema.getType()).equals("0")) // 生存领取类型
		{
			// 给付责任类型GetDutyKind
			if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetDutyKind")
					&& mLCGetBLIn.getGetDutyKind() != null
					&& !"".equals(mLCGetBLIn.getGetDutyKind())) {
				mLCGetSchema.setGetDutyKind(mLCGetBLIn.getGetDutyKind());
				LMDutyGetAliveSchema tempLMDutyGetAliveSchema = mCRI
						.findDutyGetAlive(mLCGetSchema.getGetDutyCode(),
								mLCGetSchema.getGetDutyKind());

				if (tempLMDutyGetAliveSchema == null) {
					// @@错误处理
					this.mErrors.copyAllErrors(mCRI.mErrors);
					mCRI.mErrors.clearErrors();

					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calGetOther";
					tError.errorMessage = "LMDutyGetAlive表查询失败!";
					this.mErrors.addOneError(tError);
					return null;
				}
				tLMDutyGetAliveSchema.setSchema(tempLMDutyGetAliveSchema);
			} else if (!chkInput(mLCGetSchema.getGetDutyCode(), "G",
					"GetDutyKind")) {
				LMDutyGetAliveSet tLMDutyGetAliveSet = mCRI
						.findDutyGetAliveByGetDutyCodeClone(mLCGetSchema
								.getGetDutyCode());

				if (tLMDutyGetAliveSet == null) {
					// @@错误处理
					this.mErrors.copyAllErrors(mCRI.mErrors);
					mCRI.mErrors.clearErrors();

					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calGetOther";
					tError.errorMessage = "LMDutyGetAlive表查询失败!";
					this.mErrors.addOneError(tError);
					return null;
				}
				tLMDutyGetAliveSchema = tLMDutyGetAliveSet.get(1);
				mLCGetSchema.setGetDutyKind(tLMDutyGetAliveSchema
						.getGetDutyKind());
			} // end of if
			if (mLCGetSchema.getGetDutyKind() != null
					&& !"".equals(mLCGetSchema.getGetDutyKind())) {
				// 复制描述数据
				tLMDutyGetSchema.setGetIntv(tLMDutyGetAliveSchema.getGetIntv());
				tLMDutyGetSchema.setDefaultVal(tLMDutyGetAliveSchema
						.getDefaultVal());
				tLMDutyGetSchema.setCalCode(tLMDutyGetAliveSchema.getCalCode());
				tLMDutyGetSchema.setCnterCalCode(tLMDutyGetAliveSchema
						.getCnterCalCode());
				tLMDutyGetSchema.setOthCalCode(tLMDutyGetAliveSchema
						.getOthCalCode());
				tLMDutyGetSchema.setGetYear(tLMDutyGetAliveSchema
						.getGetStartPeriod());
				tLMDutyGetSchema.setGetYearFlag(tLMDutyGetAliveSchema
						.getGetStartUnit());
				tLMDutyGetSchema.setStartDateCalRef(tLMDutyGetAliveSchema
						.getStartDateCalRef());
				tLMDutyGetSchema.setStartDateCalMode(tLMDutyGetAliveSchema
						.getStartDateCalMode());
				tLMDutyGetSchema.setMinGetStartPeriod(tLMDutyGetAliveSchema
						.getMinGetStartPeriod());
				tLMDutyGetSchema.setGetEndPeriod(tLMDutyGetAliveSchema
						.getGetEndPeriod());
				tLMDutyGetSchema.setGetEndUnit(tLMDutyGetAliveSchema
						.getGetEndUnit());
				tLMDutyGetSchema.setEndDateCalRef(tLMDutyGetAliveSchema
						.getEndDateCalRef());
				tLMDutyGetSchema.setEndDateCalMode(tLMDutyGetAliveSchema
						.getEndDateCalMode());
				tLMDutyGetSchema.setMaxGetEndPeriod(tLMDutyGetAliveSchema
						.getMaxGetEndPeriod());
				tLMDutyGetSchema.setAddFlag(tLMDutyGetAliveSchema.getAddFlag());
				tLMDutyGetSchema.setUrgeGetFlag(tLMDutyGetAliveSchema
						.getUrgeGetFlag());
				// tLMDutyGetSchema.setDiscntFlag(
				// tLMDutyGetAliveSchema.getDiscntFlag() );
			}
		} // end of if

		mLCGetSchema.setUrgeGetFlag(tLMDutyGetSchema.getUrgeGetFlag());
		if (mLCGetSchema.getGetIntv() == 0) {
			mLCGetSchema.setGetIntv(tLMDutyGetSchema.getGetIntv());
		}
		if (mLCGetSchema.getGetLimit() == 0.0) {
			mLCGetSchema.setGetLimit(tLMDutyGetSchema.getGetLimit());
		}
		if (mLCGetSchema.getGetRate() == 0.0) {
			mLCGetSchema.setGetRate(tLMDutyGetSchema.getGetRate());
		}
		String s = mLCGetSchema.getState();
		if (s != null) {
			s = tLMDutyGetSchema.getAddAmntFlag() + s.substring(1, s.length());
		} else {
			s = tLMDutyGetSchema.getAddAmntFlag();
		}

		mLCGetSchema.setState(s);

		// 递增率
		if (StrTool.cTrim(tLMDutyGetSchema.getAddFlag()).equals("Y")) {
			if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "AddRate")) {
				mLCGetSchema.setAddRate(mLCGetBLIn.getAddRate());
			} else {
//				mLCGetSchema.setAddRate(tLMDutyGetAliveSchema.getAddValue());
			}
		}

		// 准备计算基础数据部分
		mCalBase.setGetDutyKind(mLCGetSchema.getGetDutyKind());
		mCalBase.setAddRate(mLCGetSchema.getAddRate());

		// 计算日期
		Date baseDate = null;
		Date compDate = null;
		int interval = 0;
		String unit = null;
		Date getStartDate = null;
		Date getEndDate = null;

		// 计算领取开始日期
		String getStartType = null;
		String getYearFlag = null;
		int getYear = 0;

		// 判断 getYear 的取值位置
		if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetYear")) {
			if (mLCDutyBLIn.getGetYear() == 0) {
				// 错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calGetOther";
				tError.errorMessage = "必须录入开始领取年期!";
				this.mErrors.addOneError(tError);
				return null;
			}
			// 获取领取年龄年期
			getYear = mLCDutyBLIn.getGetYear();
		} else {
			// 如果校验失败，获取默认的领取年龄年期
			getYear = tLMDutyGetSchema.getGetYear();
		}

		// 判断 getYearFlag 的取值位置
		if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetYearFlag")) {
			if (mLCDutyBLIn.getGetYearFlag() == null) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calGetOther";
				tError.errorMessage = "必须录入开始领取年期标志!";
				this.mErrors.addOneError(tError);
				return null;
			}
			// 获取领取年龄年期标志
			getYearFlag = mLCDutyBLIn.getGetYearFlag();
		} else {
			// 如果校验失败，获取默认的领取年龄年期标志
			getYearFlag = tLMDutyGetSchema.getGetYearFlag();
		}

		// 判断 getStartType 的取值位置
		if (chkInput(mLCGetSchema.getGetDutyCode(), "G", "GetStartType")) {
			if (mLCDutyBLIn.getGetStartType() == null) {
				// 错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calGetOther";
				tError.errorMessage = "必须录入领取日期计算类型!";
				this.mErrors.addOneError(tError);
				return null;
			}
			// 获取起领日期计算类型
			getStartType = mLCDutyBLIn.getGetStartType();
		} else {
			// 如果校验失败，获取默认的起领日期计算类型
			getStartType = tLMDutyGetSchema.getStartDateCalRef();
		}

		// 如果领取年龄年期标志为按年龄计算，起始日期特殊设置
		if (StrTool.cTrim(getYearFlag).equals("A")) {
			baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
			unit = "Y";
			// 生日对应日
			if (StrTool.cTrim(getStartType).equals("B")) {
				compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
			}
			// 起保日期对应日
			else if (StrTool.cTrim(getStartType).equals("S")) {
				compDate = fDate.getDate(mLCPolBL.getCValiDate());
			}
		} else {
			baseDate = fDate.getDate(mLCPolBL.getCValiDate());
			unit = getYearFlag;
		}
		interval = getYear;

		// 取保险终止日期
		if (StrTool.cTrim(tLMDutyGetSchema.getStartDateCalMode()).equals("3")) {
			mLCGetSchema.setGetStartDate(tLCDutySchema.getEndDate());
		}
		// 取计算结果
		else if (StrTool.cTrim(tLMDutyGetSchema.getStartDateCalMode()).equals(
				"0")) {
			getStartDate = PubFun.calDate(baseDate, interval, unit, compDate);
			mLCGetSchema.setGetStartDate(fDate.getString(getStartDate));
		}

		mLCGetSchema.setGettoDate(mLCGetSchema.getGetStartDate());

		// 准备计算基础数据部分
		mCalBase.setGetYear(getYear);
		mCalBase.setGetYearFlag(getYearFlag);

		// 计算领取终止日期
		if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalMode()).equals("3")) // 取保险终止日期
		{
			mLCGetSchema.setGetEndDate(tLCDutySchema.getEndDate());
		}

		if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalMode()).equals("0")) // 取计算的值
		{
			if (StrTool.cTrim(tLMDutyGetSchema.getGetEndUnit()).equals("A")) {
				baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
				unit = "Y";
				if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalRef()).equals(
						"B")) {
					compDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
				}
				if (StrTool.cTrim(tLMDutyGetSchema.getEndDateCalRef()).equals(
						"S")) {
					compDate = fDate.getDate(mLCPolBL.getCValiDate());
				}
			} else {
				baseDate = fDate.getDate(mLCPolBL.getCValiDate());
				unit = tLMDutyGetSchema.getGetEndUnit();
			}
			// 得到领取期限
			interval = tLMDutyGetSchema.getGetEndPeriod();
			// 中意要求止期减一天 张阔
			getEndDate = PubFun.calDate(baseDate, interval, unit, compDate);
			mLCGetSchema.setGetEndDate(fDate.getString(getEndDate));
		}
		return mLCGetSchema;
	}

	/**
	 * 通过描述取得责任信息的结构
	 * 
	 * @return boolean
	 */
	private boolean getDutyStructure() {
		// 从险种责任表中取出责任的结构
		LMRiskDutySet tLMRiskDutySet = mCRI.findRiskDutyByRiskCode(mLCPolBL
				.getRiskCode());

		if (tLMRiskDutySet == null) {
			// @@错误处理
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "getDutyStructure";
			tError.errorMessage = "LMRiskDuty表查询失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 把数据装入mLCDutyBLSet中
		mLCDutyBLSet = new LCDutyBLSet();
		int n = tLMRiskDutySet.size();
		for (int i = 1; i <= n; i++) {
			if (!StrTool.cTrim(tLMRiskDutySet.get(i).getChoFlag()).equals("M")) {
				continue;
			}

			LCDutyBL tLCDutyBL = new LCDutyBL();
			tLCDutyBL.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());

			if (mLCDutyBLIn != null) // 录入优先
			{
				tLCDutyBL.setPayIntv(mLCDutyBLIn.getPayIntv());
				tLCDutyBL.setInsuYearFlag(mLCDutyBLIn.getInsuYearFlag());
				tLCDutyBL.setInsuYear(mLCDutyBLIn.getInsuYear());
				tLCDutyBL.setAcciYearFlag(mLCDutyBLIn.getAcciYearFlag());
				tLCDutyBL.setAcciYear(mLCDutyBLIn.getAcciYear());
				tLCDutyBL.setPayEndYear(mLCDutyBLIn.getPayEndYear());
				tLCDutyBL.setPayEndYearFlag(mLCDutyBLIn.getPayEndYearFlag());
				tLCDutyBL.setGetYear(mLCDutyBLIn.getGetYear());
				tLCDutyBL.setGetYearFlag(mLCDutyBLIn.getGetYearFlag());
				tLCDutyBL.setStandbyFlag1(mLCDutyBLIn.getStandbyFlag1());
				tLCDutyBL.setStandbyFlag2(mLCDutyBLIn.getStandbyFlag2());
				tLCDutyBL.setStandbyFlag3(mLCDutyBLIn.getStandbyFlag3());
//				tLCDutyBL.setStandbyFlag4(mLCDutyBLIn.getStandbyFlag4());
				tLCDutyBL.setMult(mLCDutyBLIn.getMult());
				tLCDutyBL.setCalRule(mLCDutyBLIn.getCalRule());
				tLCDutyBL.setFloatRate(mLCDutyBLIn.getFloatRate());
				tLCDutyBL.setPremToAmnt(mLCDutyBLIn.getPremToAmnt());
				tLCDutyBL.setGetLimit(mLCDutyBLIn.getGetLimit());
				tLCDutyBL.setGetRate(mLCDutyBLIn.getGetRate());
				tLCDutyBL.setSSFlag(mLCDutyBLIn.getSSFlag());
				tLCDutyBL.setPeakLine(mLCDutyBLIn.getPeakLine());
			}

			mLCDutyBLSet.add(tLCDutyBL);
		}

		return true;
	}

	/**
	 * 从保单信息中获取责任表中需要计算的以外的信息
	 */
	private void getDutyOtherData() {
		int n = mLCDutyBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCDutySchema tLCDutySchema = mLCDutyBLSet.get(i);

			// 从保单带过来的信息
			// PQ1 需要设一些从LCCont带过来的值
			if (tLCDutySchema.getPolNo() == null) {
				tLCDutySchema.setPolNo(mLCPolBL.getPolNo());
			}
			if (tLCDutySchema.getContNo() == null) {
				tLCDutySchema.setContNo(mLCPolBL.getContNo());
			}
			if (tLCDutySchema.getMult() == 0.0) {
				tLCDutySchema.setMult(mLCPolBL.getMult());
			}
			if (tLCDutySchema.getFirstPayDate() == null) {
				tLCDutySchema.setFirstPayDate(mLCPolBL.getCValiDate());
			}
			if(tag == false){
				if (tLCDutySchema.getStandPrem() == 0.0) {
					if (tLCDutySchema.getPrem() == 0.0) {
						tLCDutySchema.setPrem(mLCPolBL.getPrem());
						tLCDutySchema.setStandPrem(mLCPolBL.getStandPrem());
					} else {
						tLCDutySchema.setStandPrem(tLCDutySchema.getPrem());
					}
				}
			}
			if (tLCDutySchema.getAmnt() == 0.0) {
				tLCDutySchema.setAmnt(mLCPolBL.getAmnt());
			}
			if (tLCDutySchema.getRiskAmnt() == 0.0) {
				tLCDutySchema.setRiskAmnt(mLCPolBL.getRiskAmnt());
			}
			if (tLCDutySchema.getLiveGetMode() == null) {
				tLCDutySchema.setLiveGetMode(mLCPolBL.getLiveGetMode());
			}
			if (tLCDutySchema.getDeadGetMode() == null) {
				tLCDutySchema.setDeadGetMode(mLCPolBL.getDeadGetMode());
			}
			if (tLCDutySchema.getBonusGetMode() == null) {
				tLCDutySchema.setBonusGetMode(mLCPolBL.getBonusGetMode());
			}
			tLCDutySchema.setContNo(mLCPolBL.getContNo());
			tLCDutySchema.setOperator(mLCPolBL.getOperator());
			tLCDutySchema.setMakeDate(PubFun.getCurrentDate());
			tLCDutySchema.setMakeTime(PubFun.getCurrentTime());
			// 中意新增
//			tLCDutySchema.setSendDate(mLCPolBL.getSendDate());
//			tLCDutySchema.setAnnuityDate(mLCPolBL.getAnnuityDate());
//			tLCDutySchema.setSendComCode(mLCPolBL.getSendComCode());
//			tLCDutySchema.setSendInnerCode(mLCPolBL.getSendInnerCode());
//			tLCDutySchema.setSendStandard(mLCPolBL.getSendStandard());
//			tLCDutySchema.setSendMoney(mLCPolBL.getSendMoney());
//			tLCDutySchema.setRetireType(mLCPolBL.getRetireType());
//			tLCDutySchema.setContNoSub(mLCPolBL.getContNoSub());
//			tLCDutySchema.setInsuredStat(mLCPolBL.getInsuredStat());
//			tLCDutySchema.setContNoSub(mLCPolBL.getContNoSub());
//			tLCDutySchema.setPrtNo(mLCPolBL.getPrtNo());
//			tLCDutySchema.setStandardStartDate(mLCPolBL.getStandardStartDate());
			mLCDutyBLSet.set(i, tLCDutySchema);
		}
	}

	/**
	 * 计算责任表中的其他相关信息
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @return LCDutySchema
	 */
	private LCDutySchema calDutyOther(LCDutySchema mLCDutySchema) {
		LMDutySchema tLMDutySchema = mCRI.findDutyByDutyCodeClone(mLCDutySchema
				.getDutyCode());

		if (tLMDutySchema == null) {
			// @@错误处理
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calDutyOther";
			tError.errorMessage = "LMDuty表查询失败!";
			this.mErrors.addOneError(tError);
			return null;
		}

		if (mLCDutyBLIn.getGetStartType() != null) {
			mLCDutySchema.setGetStartType(mLCDutyBLIn.getGetStartType());
		}

		Date baseDate = null;
		Date compDate = null;
		int interval = 0;
		String unit = null;

		int insuYearIn = mLCDutyBLIn.getInsuYear();
		String insuYearFlagIn = mLCDutyBLIn.getInsuYearFlag();
		int payEndYearIn = mLCDutyBLIn.getPayEndYear();
		String payEndYearFlagIn = mLCDutyBLIn.getPayEndYearFlag();
		int getYearIn = mLCDutyBLIn.getGetYear();
		String getYearFlagIn = mLCDutyBLIn.getGetYearFlag();

		// 对保险期间的判断,如果是短期险的话,保险期间不应该超过一年
		// ExeSQL mExeSQL = new ExeSQL();
		// chenwm20070723 短期险中医疗产品有大于1年的情况,和frost讨论 先注释掉
		/*
		 * SSRS bSSRS = new SSRS(); String strSQL = "select * from lmriskapp
		 * where riskperiod in ('S','M') AND
		 * riskcode='"+mLCPolBL.getRiskCode()+"'"; //这种方法判断是否为短期险 bSSRS =
		 * mExeSQL.execSQL(strSQL);
		 * //如果保险期间单位是M,这保险期间不能大于12,保险期间单位是D,保险期间不能大于366 if(bSSRS.getMaxRow()>0) {
		 * if( (insuYearFlagIn!=
		 * null&&insuYearFlagIn.equals("M")&&insuYearIn>12) || (insuYearFlagIn!=
		 * null&&insuYearFlagIn.equals("D")&& insuYearIn>366)||
		 * (insuYearFlagIn!= null&&insuYearFlagIn.equals("Y")&& insuYearIn>1)) { //
		 * @@错误处理 CError tError = new CError(); tError.moduleName = "CalBL";
		 * tError.functionName = "calPremValue"; tError.errorMessage =
		 * "保障计划或输入险种中的保险期间录入错误,不应该超过1年!"; this.mErrors.addOneError(tError);
		 * return null; } }
		 */// 保险期限
		Date endDate = null;
		int insuYear = 0;
		int years = 0;
		String insuYearFlag = null;

		// 判断 InsuYear 的取值位置
		if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("0"))

		{
			if (mLCDutyBLIn.getInsuYear() == 0
					|| mLCDutyBLIn.getInsuYearFlag() == null) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入保险终止年期及年期单位!";
				this.mErrors.addOneError(tError);
				return null;
			}
			if (mLCDutyBLIn.getInsuYearFlag().trim().equals("")) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入保险终止年期及年期单位!";
				this.mErrors.addOneError(tError);
				return null;
			}
			insuYear = insuYearIn;
			insuYearFlag = insuYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("1")) // 和缴费终止期间的值相同
		{
			insuYear = payEndYearIn;
			insuYearFlag = payEndYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("2")) // 和起领期间的值相同
		{
			insuYear = getYearIn;
			insuYearFlag = getYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("4")) // 从描述表取其默认值
		{
			insuYear = tLMDutySchema.getInsuYear();
			insuYearFlag = tLMDutySchema.getInsuYearFlag();
		}
		if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("5")) // 更据算法，将缴费终止期间和默认值相加
		{
			insuYear = tLMDutySchema.getInsuYear() + payEndYearIn;
			insuYearFlag = payEndYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getInsuYearRela()).equals("6")) // 更据算法，将起领期间和默认值相加
		{
			insuYear = tLMDutySchema.getInsuYear() + getYearIn;
			insuYearFlag = getYearFlagIn;
		}

		// 计算
		if (mLCDutySchema.getEndDate() == null
				|| mLCDutySchema.getEndDate().equals("")) {
			baseDate = fDate.getDate(mLCPolBL.getCValiDate());
			unit = insuYearFlag;
			interval = insuYear;
			compDate = null;
			if (unit == null && interval == 0) {
				endDate = fDate.getDate("3000-01-01");
			} else {
				if (StrTool.cTrim(unit).equals("A")) {
					baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
					unit = "Y";
					compDate = fDate.getDate(mLCPolBL.getCValiDate());
				} else {
					baseDate = fDate.getDate(mLCPolBL.getCValiDate());
				}
				// 中意要求止期减一天 张阔
				endDate = PubFun.calDate(baseDate, interval, unit, compDate);

				if (StrTool.cTrim(insuYearFlag).equals("A")) {
					years = PubFun.calInterval(fDate.getDate(mLCPolBL
							.getCValiDate()), endDate, "Y");
				} else {
					years = insuYear;
				}
				mLCDutySchema.setYears(years);
			}

			mLCDutySchema.setEndDate(fDate.getString(endDate));

		}
		mLCDutySchema.setInsuYearFlag(insuYearFlag.trim());
		mLCDutySchema.setInsuYear(insuYear);

		// 意外责任期限
		Date acciEndDate = null;
		int acciYear = 0;
		String acciYearFlag = null;

		// 判断 acciYear 的取值位置
		if (chkInput(mLCDutySchema.getDutyCode(), "D", "AcciYear")) {
			if (mLCDutyBLIn.getAcciYear() == 0) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入意外责任终止年期及单位!";
				this.mErrors.addOneError(tError);
				return null;
			}
			acciYear = mLCDutyBLIn.getAcciYear();
		} else {
			acciYear = tLMDutySchema.getAcciYear();
		}

		// 判断 AcciYearFlag 的取值位置
		if (chkInput(mLCDutySchema.getDutyCode(), "D", "AcciYearFlag")) {
			if (mLCDutyBLIn.getAcciYearFlag() == null) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入意外责任终止年期标志!";
				this.mErrors.addOneError(tError);
				return null;
			}
			// if (mLCDutyBLIn.getAcciYearFlag().trim().equals(""))
			// {
			// // @@错误处理
			// CError tError = new CError();
			// tError.moduleName = "CalBL";
			// tError.functionName = "calDutyOther";
			// tError.errorMessage = "必须录入意外责任终止年期标志!" ;
			// this.mErrors.addOneError(tError) ;
			// return null;
			// }
			acciYearFlag = mLCDutyBLIn.getAcciYearFlag();
		} else {
			acciYearFlag = tLMDutySchema.getAcciYearFlag();
		}

		// 计算
		if (StrTool.cTrim(mLCDutySchema.getAcciEndDate()).equals("")
				&& acciYear != 0) {
			baseDate = fDate.getDate(mLCPolBL.getCValiDate());
			unit = acciYearFlag;
			interval = acciYear;
			compDate = null;
			if (!(unit == null && interval == 0)) {
				if (StrTool.cTrim(unit).equals("A")) {
					baseDate = fDate.getDate(mLCPolBL.getInsuredBirthday());
					unit = "Y";
					compDate = fDate.getDate(mLCPolBL.getCValiDate());
				} else {
					baseDate = fDate.getDate(mLCPolBL.getCValiDate());
				}
				// 中意要求止期减一天 张阔
				acciEndDate = PubFun
						.calDate(baseDate, interval, unit, compDate);

				mLCDutySchema.setAcciEndDate(fDate.getString(acciEndDate));
			}
		}
		mLCDutySchema.setAcciYearFlag(acciYearFlag);
		mLCDutySchema.setAcciYear(acciYear);

		// 判断 PayEndYear 的取值位置
		int payEndYear = 0;
		String payEndYearFlag = null;
		if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("0")) // 取录入值
		{
			if (mLCDutyBLIn.getPayEndYear() == 0
					|| mLCDutyBLIn.getPayEndYearFlag() == null) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入保险终止年期和保险终止年期单位!";
				this.mErrors.addOneError(tError);
				return null;
			}
			if (mLCDutyBLIn.getPayEndYearFlag().trim().equals("")) {
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入保险终止年期和保险终止年期单位!";
				this.mErrors.addOneError(tError);
				return null;
			}

			payEndYear = payEndYearIn;
			payEndYearFlag = payEndYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("3")) // 和保险期间的值相同
		{
			payEndYear = insuYearIn;
			payEndYearFlag = insuYearFlagIn;
			// 如果保险期间的值不从界面录入，则取描述值 frost于2005-9-25修改
			if (payEndYear == 0
					|| (payEndYearFlag == null || payEndYearFlag.equals(""))) {
				payEndYear = tLMDutySchema.getInsuYear();
				payEndYearFlag = tLMDutySchema.getInsuYearFlag();
			}
		}
		if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("2")) // 和起领期间的值相同
		{
			payEndYear = getYearIn;
			payEndYearFlag = getYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("4")) // 从描述表取其默认值
		{
			payEndYear = tLMDutySchema.getPayEndYear();
			payEndYearFlag = tLMDutySchema.getPayEndYearFlag();
		}
		if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("7")) // 更据算法，将保险期间和默认值相加
		{
			payEndYear = tLMDutySchema.getPayEndYear() + insuYearIn;
			payEndYearFlag = insuYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getPayEndYearRela()).equals("6")) // 更据算法，将起领期间和默认值相加
		{
			payEndYear = tLMDutySchema.getPayEndYear() + getYearIn;
			payEndYearFlag = getYearFlagIn;
		}
		mLCDutySchema.setPayEndYearFlag(payEndYearFlag);
		mLCDutySchema.setPayEndYear(payEndYear);

		// 判断 GetYear 的取值位置
		int getYear = 0;
		String getYearFlag = null;
		if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("0")) // 取录入值
		{

			if (mLCDutyBLIn.getGetYear() == 0
					|| mLCDutyBLIn.getGetYearFlag() == null) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入起领年期及单位!";
				this.mErrors.addOneError(tError);
				return null;
			}
			if (mLCDutyBLIn.getGetYearFlag().trim().equals("")) {
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calDutyOther";
				tError.errorMessage = "必须录入起领年期及单位!";
				this.mErrors.addOneError(tError);
				return null;
			}

			getYear = getYearIn;
			getYearFlag = getYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("3")) // 和保险期间的值相同
		{
			getYear = insuYearIn;
			getYearFlag = insuYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("1")) // 和交费终止期间的值相同
		{
			getYear = payEndYearIn;
			getYearFlag = payEndYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("4")) // 从描述表取其默认值
		{
			getYear = tLMDutySchema.getGetYear();
			getYearFlag = tLMDutySchema.getGetYearFlag();
		}
		if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("7")) // 更据算法，将保险期间和默认值相加
		{
			getYear = tLMDutySchema.getGetYear() + insuYearIn;
			getYearFlag = insuYearFlagIn;
		}
		if (StrTool.cTrim(tLMDutySchema.getGetYearRela()).equals("5")) // 更据算法，将交费终止期间和默认值相加
		{
			getYear = tLMDutySchema.getGetYear() + payEndYearIn;
			getYearFlag = payEndYearFlagIn;
		}
		mLCDutySchema.setGetYearFlag(getYearFlag);
		mLCDutySchema.setGetYear(getYear);

		return mLCDutySchema;
	}

	/**
	 * 从保费项表中取得责任表中部分信息
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @param tLMDutySchema
	 *            LMDutySchema
	 * @return LCDutySchema
	 */
	private LCDutySchema calDutyFromPrem(LCDutySchema mLCDutySchema,
			LMDutySchema tLMDutySchema) {
		String dutyCode = mLCDutySchema.getDutyCode();

		double sumPrem = 0;
		Date maxPayEndDate = fDate.getDate("1900-01-01");
		int payIntv = -8;
		int n = mLCPremBLSet.size();
		int premDutyNum = 0; // 默认没有缴费项
		for (int i = 1; i <= n; i++) {
			LCPremBL mLCPremBL = (LCPremBL) mLCPremBLSet.get(i);

			if (StrTool.cTrim(mLCPremBL.getDutyCode()).equals(dutyCode)) {
				payIntv = mLCPremBL.getPayIntv();
				sumPrem += mLCPremBL.getStandPrem();

				Date payEndDate = fDate.getDate(mLCPremBL.getPayEndDate());
				// 获取最大的交费终止日期
				if (maxPayEndDate.before(payEndDate)) {
					maxPayEndDate = payEndDate;
				}
				premDutyNum++;
			}
		}
		// 当此责任没有设置缴费项时，取有缴费项的PayIntv,PayEndDate
		if (premDutyNum < 1 && n > 0) {
			payIntv = mLCPremBLSet.get(1).getPayIntv();
			maxPayEndDate = fDate.getDate(mLCPremBLSet.get(1).getPayEndDate());
		}

		Date tStartDate = null;

		String tPayEndYearFlag = mLCPolBL.getPayEndYearFlag(); // 终交年期标志
		// 如果终交年期标志为空，或者为A的时候，按年份计算交费年期
		if (tPayEndYearFlag == null || tPayEndYearFlag.equals("")
				|| tPayEndYearFlag.equals("A") || tPayEndYearFlag.equals("Y")) {
			tPayEndYearFlag = "Y";
			// 之所以这么修改的原因，在计算年份的时候，计算的结果为年差值
			tStartDate = fDate.getDate(mLCPolBL.getCValiDate().substring(0, 5)
					+ "01-01"); // 保单生效日所在年的第一天
		} else {
			tStartDate = fDate.getDate(mLCPolBL.getCValiDate()); // 保单生效日
		}

		if (n == 0) {// 个别险种没有保费项的描述所以以duty为准
			payIntv = mLCDutySchema.getPayIntv();
			int PayEndYear = mLCDutySchema.getPayEndYear();
			maxPayEndDate = fDate.getDate(PubFun.calDate(mLCPolBL
					.getCValiDate(), PayEndYear, tPayEndYearFlag, null));
			sumPrem = 0;
		}
		// 计算交费年期
		int payYears = PubFun.calInterval(tStartDate, maxPayEndDate,
				tPayEndYearFlag);

		mLCDutySchema.setPayYears(payYears);
		mLCDutySchema.setPayIntv(payIntv);
		mLCDutySchema.setStandPrem(sumPrem);
		mLCDutySchema.setPrem(sumPrem);
		mLCDutySchema.setPayEndDate(fDate.getString(maxPayEndDate));

		return mLCDutySchema;
	}

	/**
	 * 从领取项表中取得责任表中部分信息
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @param tLMDutySchema
	 *            LMDutySchema
	 * @return LCDutySchema
	 */
	private LCDutySchema calDutyFromGet(LCDutySchema mLCDutySchema,
			LMDutySchema tLMDutySchema) {
		String dutyCode = mLCDutySchema.getDutyCode();

		double sumAmnt = 0;
		Date minGetStartDate = fDate.getDate("3000-01-01");
		int n = mLCGetBLSet.size();
		for (int i = 1; i <= n; i++) {
			if (mLCGetBLSet.get(i).getDutyCode().equals(dutyCode)) {

				LCGetBL mLCGetBL = (LCGetBL) mLCGetBLSet.get(i);
				if (mLCGetBL.getState().substring(0, 1).equals("Y")) {
					sumAmnt += mLCGetBL.getStandMoney();
				}

				Date getStartDate = fDate.getDate(mLCGetBL.getGetStartDate());
				if (minGetStartDate.after(getStartDate)) {
					minGetStartDate = getStartDate;
				}
			}
		}
		mCalBase.setAmnt(sumAmnt);

		// 计算基本保额和风险保额
		mLCDutySchema = this.calAmnt(mLCDutySchema, tLMDutySchema);

		mLCDutySchema.setGetStartDate(fDate.getString(minGetStartDate));

		return mLCDutySchema;
	}

	/**
	 * 计算一个责任下的所有信息
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @return LCDutySchema
	 */
	private LCDutySchema calOneDuty(LCDutySchema mLCDutySchema) {
		String dutyCode = mLCDutySchema.getDutyCode();
		String calMode = "";

		// 如果界面没有录入浮动费率，此时mLCDutySchema的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
		// 保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
		FloatRate = mLCDutySchema.getFloatRate();

		InputPrem = mLCDutySchema.getPrem();
		InputAmnt = mLCDutySchema.getAmnt();

		// String strCalPrem = mDecimalFormat.format(InputPrem);
		// //转换计算后的保费(规定的精度)
		// String strCalAmnt = mDecimalFormat.format(InputAmnt); //转换计算后的保额
		// InputPrem = Double.parseDouble(strCalPrem);
		// InputAmnt = Double.parseDouble(strCalAmnt);

		// 保存计算方向
		String calRule = mLCDutySchema.getCalRule();
		if (calRule == null) {
			// 默认为表定费率
			mLCDutySchema.setCalRule("0");
		}
		// 如果是统一费率，需要去默认值里取出统一费率的值,算出保费保额
		else if (calRule.equals("1")) {
			autoCalFloatRateFlag = true;
		} else if (calRule.equals("3")) {
			autoCalFloatRateFlag = true;
			FloatRate = -1;
		}

		// 如果界面录入浮动费率=-1，则计算浮动费率的标记为真
		if (FloatRate == ConstRate) {
			autoCalFloatRateFlag = true;

			// 将保单中的该子段设为1，后面描述算法中计算
			mLCDutySchema.setFloatRate(1);
		}

		// 浮动费率处理END

		// 准备计算基础数据部分
		mCalBase = new CalBase();
		mCalBase.setRiskCode(mLCPolBL.getRiskCode());
//		mCalBase.setDutyCode(mLCDutySchema.getDutyCode());
		mCalBase.setCalRule(mLCDutySchema.getCalRule());
		mCalBase.setCalType(calType);
		mCalBase.setPrem(mLCDutySchema.getStandPrem());
		// mCalBase.setPrem( mLCDutySchema.getPrem() );
		mCalBase.setGet(mLCDutySchema.getAmnt());
		mCalBase.setMult(mLCDutySchema.getMult());
		// 份数：按份卖的险种保存份数，其他险种存0。为了保证000301、000302算法的正确性，当份数为0时，该字段赋值为1。
		if (mLCDutySchema.getMult() == 0) {
			mCalBase.setMult(1);
		}
		mCalBase.setYears(mLCDutySchema.getYears());
		mCalBase.setInsuYear(mLCDutySchema.getInsuYear());
		mCalBase.setInsuYearFlag(mLCDutySchema.getInsuYearFlag());
		mCalBase.setAppAge(mLCPolBL.getInsuredAppAge());
		mCalBase.setSex(mLCPolBL.getInsuredSex());
		mCalBase.setJob(mLCPolBL.getOccupationType());
		mCalBase.setCount(mLCPolBL.getInsuredPeoples());
		mCalBase.setPolNo(mLCPolBL.getPolNo());
		mCalBase.setContNo(mLCPolBL.getContNo());

		// 对stadnflag 这3个字段的特殊处理,是防止后面算保费的时候出错
		// zhangjq 2008-4-18 加.trim()去处空格
		if (StrTool.compareString(mLCDutySchema.getStandbyFlag1(), "")) {
			mCalBase.setStandbyFlag1("");
		} else {
			mCalBase.setStandbyFlag1(mLCDutySchema.getStandbyFlag1().trim());
		}

		if (StrTool.compareString(mLCDutySchema.getStandbyFlag2(), "")) {
			mCalBase.setStandbyFlag2("");
		} else {
			mCalBase.setStandbyFlag2(mLCDutySchema.getStandbyFlag2().trim());
		}

		if (StrTool.compareString(mLCDutySchema.getStandbyFlag3(), "")) {
			mCalBase.setStandbyFlag3("");
		} else {
			mCalBase.setStandbyFlag3(mLCDutySchema.getStandbyFlag3().trim());
		}

//		if (StrTool.compareString(mLCDutySchema.getStandbyFlag4(), "")) {
//			mCalBase.setStandbyFlag4("");
//		} else {
//			mCalBase.setStandbyFlag4(mLCDutySchema.getStandbyFlag4().trim());
//		}
//		// zhangjq 2008-4-18
//		// 加setStandbyFlag5、setStandbyFlag6（NMK04险种用StandbyFlag1~StandbyFlag6保存赔付倍数）
//		if (StrTool.compareString(mLCDutySchema.getStandbyFlag5(), "")) {
//			mCalBase.setStandbyFlag5("");
//		} else {
//			mCalBase.setStandbyFlag5(mLCDutySchema.getStandbyFlag5().trim());
//		}
//		// zhangjq 2008-4-18
//		// 加setStandbyFlag5、setStandbyFlag6（NMK04险种用StandbyFlag1~StandbyFlag6保存赔付倍数）
//		if (StrTool.compareString(mLCDutySchema.getStandbyFlag6(), "")) {
//			mCalBase.setStandbyFlag6("");
//		} else {
//			mCalBase.setStandbyFlag6(mLCDutySchema.getStandbyFlag6().trim());
//		}

		// mCalBase.setStandbyFlag4(mLCDutySchema.getStandbyFlag4());
		mCalBase.setGrpPolNo(mLCPolBL.getGrpPolNo());
		mCalBase.setGetLimit(mLCDutySchema.getGetLimit());
		mCalBase.setGetRate(mLCDutySchema.getGetRate());
		mCalBase.setSSFlag(mLCDutySchema.getSSFlag());
		mCalBase.setPeakLine(mLCDutySchema.getPeakLine());
		mCalBase.setCValiDate(mLCPolBL.getCValiDate());
//		mCalBase.setContPlanCode(mLCInsuredSchema.getContPlanCode());
//		mCalBase.setInsuredNo(mLCInsuredSchema.getInsuredNo());
		mCalBase.setGrpContNo(mLCInsuredSchema.getGrpContNo());
//		mCalBase.setSalary(mLCInsuredSchema.getSalary());
		// add by hanming
//		mCalBase.setPolTypeFlag(mLCPolBL.getPolTypeFlag());

		if (mLCDutySchema.getFloatRate() == 0.0) {
			mLCDutySchema.setFloatRate(1);
		}
		mCalBase.setFloatRate(mLCDutySchema.getFloatRate());

		int tRnewFlag = -8;
		if (mLCPolBL.getRenewCount() == 0) {
			tRnewFlag = 0;
		}
		if (mLCPolBL.getRenewCount() > 0) {
			tRnewFlag = 1;
		}
		mCalBase.setRnewFlag(tRnewFlag);

		LMDutySchema tLMDutySchema = mCRI.findDutyByDutyCode(dutyCode);

		if (tLMDutySchema == null) {
			// @@错误处理
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "LMDuty表查询失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}

		if (mLCDutySchema.getPayEndYearFlag() == null) {
			mLCDutySchema.setPayEndYearFlag(tLMDutySchema.getPayEndYearFlag());
		}
		if (mLCDutySchema.getGetYearFlag() == null) {
			mLCDutySchema.setGetYearFlag(tLMDutySchema.getGetYearFlag());
		}
		if (mLCDutySchema.getInsuYearFlag() == null) {
			mLCDutySchema.setInsuYearFlag(tLMDutySchema.getInsuYearFlag());
		}
		if (mLCDutySchema.getAcciYearFlag() == null) {
			mLCDutySchema.setAcciYearFlag(tLMDutySchema.getAcciYearFlag());
		}

		if (calType == null || calType.equals("")) {
			if (mLCDutySchema.getPremToAmnt() == null
					|| mLCDutySchema.getPremToAmnt().equals("")) {
				calMode = tLMDutySchema.getCalMode();
			} else {
				calMode = mLCDutySchema.getPremToAmnt();
			}
		} else {
			if (mLCDutySchema.getPremToAmnt() != null
					&& !("".equals(mLCDutySchema.getPremToAmnt()))) {
				calMode = mLCDutySchema.getPremToAmnt();
			} else if (tLMDutySchema.getCalMode() != null
					&& !("".equals(tLMDutySchema.getCalMode()))) {
				calMode = tLMDutySchema.getCalMode();
			} else {
				LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = null;
				if (this.isConCanUse)
					tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB(this.con);
				else
					tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
				tLMPolDutyEdorCalDB.setRiskCode(mLCPolBL.getRiskCode());
				tLMPolDutyEdorCalDB.setEdorType(calType);
				LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB
						.query();
				if (tLMPolDutyEdorCalDB.mErrors.needDealError()) {
					mErrors.copyAllErrors(tLMPolDutyEdorCalDB.mErrors);
					mErrors.addOneError(new CError("查询险种责任保全计算信息不成功！"));
					return null;
				}
				if (tLMPolDutyEdorCalSet.size() > 0) {
					calMode = tLMPolDutyEdorCalSet.get(1).getCalMode(); // 获取保全重算的计算方式
				} else {
					mErrors.copyAllErrors(tLMPolDutyEdorCalDB.mErrors);
					mErrors.addOneError(new CError("查询险种责任保全计算信息不成功！"));
					return null;
				}
			}
		}

		// 保费项结构部分
		if (!getPremStructure(mLCDutySchema)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "getPremStructure方法返回失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}
		// 从保单信息或责任信息中获取保费项表中需要计算的以外的信息
		this.getPremOtherData(mLCDutySchema);

		int n = mLCPremBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);

			if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
				LMDutyPaySchema tLMDutyPaySchema = mCRI
						.findDutyPayByPayPlanCode(tLCPremSchema
								.getPayPlanCode());

				if (tLMDutyPaySchema == null) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calOneDuty";
					tError.errorMessage = "LMDutyPay表查询失败!";
					this.mErrors.addOneError(tError);
					return mLCDutySchema;
				}

				// 计算保费项中除保费外的其他信息
				tLCPremSchema = this.calPremOther(tLCPremSchema,
						tLMDutyPaySchema, mLCDutySchema);
				if (mOperator != null && !mOperator.equals("")) {
					tLCPremSchema.setOperator(mOperator);
					tLCPremSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPremSchema.setMakeTime(PubFun.getCurrentTime());
				}
				mLCPremBLSet.set(i, tLCPremSchema);
			} // end of if
		} // end of for

		// 领取项结构部分
		if (!getGetStructure(mLCDutySchema)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "getGetStructure方法返回失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}
		this.getGetOtherData();

		int m = mLCGetBLSet.size();
		for (int i = 1; i <= m; i++) {
			LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

			if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
				LMDutyGetSchema tLMDutyGetSchema = mCRI
						.findDutyGetByGetDutyCode(tLCGetSchema.getGetDutyCode());

				if (tLMDutyGetSchema == null) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calOneDuty";
					tError.errorMessage = "LMDutyGet表查询失败!";
					this.mErrors.addOneError(tError);
					return mLCDutySchema;
				}

				// 取得相对应的传入的领取项的信息
				if (mGetFlag) {
					// 传入的是多条的GetSet
					int inpGetCount = mLCGetSetIn.size();
					for (int j = 1; j <= inpGetCount; j++) {
						LCGetSchema tLCGetSchema1 = mLCGetSetIn.get(j);
						if (tLCGetSchema1.getGetDutyCode().equals(
								tLCGetSchema.getGetDutyCode())) {
							mLCGetBLIn = tLCGetSchema1.getSchema();
							break;
						}
						// end of if
					}
					// end of for
				}
				// end of if

				// 计算领取项中除保费外的其他信息
				tLCGetSchema = this.calGetOther(tLCGetSchema, tLMDutyGetSchema,
						mLCDutySchema);
				if (this.mErrors.needDealError()) {
					return null;
				}
				tLCGetSchema.setOperator(mLCGetBLIn.getOperator());
				tLCGetSchema.setMakeDate(mLCGetBLIn.getMakeDate());
				tLCGetSchema.setMakeTime(mLCGetBLIn.getMakeTime());
				mLCGetBLSet.set(i, tLCGetSchema);
			} // end of if
		} // end of for

		if (!noCalFlag) // 如果需要计算保费,该标记由承保程序传入
		{
			// 计算保费
			int j = mLCPremBLSet.size();
			for (int i = 1; i <= j; i++) {
				LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);

				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					LMDutyPaySchema tLMDutyPaySchema = mCRI
							.findDutyPayByPayPlanCode(tLCPremSchema
									.getPayPlanCode());

					if (tLMDutyPaySchema == null) {
						// @@错误处理
						CError tError = new CError();
						tError.moduleName = "CalBL";
						tError.functionName = "calOneDuty";
						tError.errorMessage = "LMDutyPay表查询失败!";
						this.mErrors.addOneError(tError);
						return mLCDutySchema;
					}

					// 准备计算基础数据部分
					mCalBase.setPayIntv(tLCPremSchema.getPayIntv());

					if (mLCGetBLIn.getGetDutyKind() == null) {
						// 此时保全会从后台查询出领取项，可能是多条，循环找出GetDutyKind不为空的一条赋值
						for (int t = 1; t <= mLCGetBLSet.size(); t++) {
							if (mLCGetBLSet.get(t).getGetDutyKind() != null) {
								mCalBase.setGetDutyKind(mLCGetBLSet.get(t)
										.getGetDutyKind());
							}
						}

					} else {
						// houzm add(将界面录入的GetDutyKind保存)
						mCalBase.setGetDutyKind(mLCGetBLIn.getGetDutyKind());
					}
					// 可以添加从界面传入的要素，该要素只是用于计算

					// 计算保费值
					double calResult = 0;
					if (!("3".equals(mLCDutySchema.getCalRule()) && ""
							.equals(calType))
							&& !("3".equals(mLCDutySchema.getCalRule()) && "IC"
									.equals(calType))
							&& !("3".equals(mLCDutySchema.getCalRule()) && "IO"
									.equals(calType))) {
						calResult = calPremValue(tLCPremSchema, mLCDutySchema,
								tLMDutyPaySchema, calMode);
					} else {
						calResult = mLCDutySchema.getPrem();
					}
					mLCDutySchema.setPremToAmnt(currCalMode);
					if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
							"N")) {
						if (calResult <= 0.0 && (!autoCalFloatRateFlag && InputPrem > 0 || !autoCalFloatRateFlag && InputAmnt > 0)) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保费计算失败";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						} else {
							tLCPremSchema.setStandPrem(calResult);
						}
					}
					if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
							"Y")) {
						if (calResult < 0.0) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保费计算失败";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						} else {
							tLCPremSchema.setStandPrem(calResult);
						}
					}
					tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
					mLCPremBLSet.set(i, tLCPremSchema);
				} // end of if
			} // end of for

			// 对于GIL重算的特殊处理.如果是GIL重算的话,则不重新算保额
			if (!tag) {
				// 计算保额
				int k = mLCGetBLSet.size();
				for (int i = 1; i <= k; i++) {
					LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

					if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
						LMDutyGetDB tLMDutyGetDB = null;
						if (this.isConCanUse)
							tLMDutyGetDB = new LMDutyGetDB(this.con);
						else
							tLMDutyGetDB = new LMDutyGetDB();
						tLMDutyGetDB.setGetDutyCode(tLCGetSchema
								.getGetDutyCode());
						if (!tLMDutyGetDB.getInfo()) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "LMDutyGet表查询失败!";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						}

						// 准备计算基础数据部分
						mCalBase.setGetIntv(tLCGetSchema.getGetIntv());
						mCalBase.setGDuty(tLCGetSchema.getGetDutyCode());
						mCalBase.setAddRate(tLCGetSchema.getAddRate());
						// 计算保额值
						double calResult = 0;
						calResult = calGetValue(tLCGetSchema, mLCDutySchema,
								tLMDutyGetDB, calMode);
						// null认为是N
						if (tLMDutyGetDB.getZeroFlag() == null
								|| tLMDutyGetDB.getZeroFlag().equals("")) {
							tLMDutyGetDB.setZeroFlag("N");
						}
						if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals(
								"N")) {
							if (calResult <= 0.0) {
								// @@错误处理
								CError tError = new CError();
								tError.moduleName = "CalBL";
								tError.functionName = "calOneDuty";
								tError.errorMessage = "保额计算失败!";
								this.mErrors.addOneError(tError);
								return mLCDutySchema;
							} else {
								tLCGetSchema.setStandMoney(calResult);
								tLCGetSchema.setActuGet(calResult);
							}
						}
						if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals(
								"Y")) {
							if (calResult < 0.0) {
								// @@错误处理
								CError tError = new CError();
								tError.moduleName = "CalBL";
								tError.functionName = "calOneDuty";
								tError.errorMessage = "保额计算失败!";
								this.mErrors.addOneError(tError);
								return mLCDutySchema;
							} else {
								tLCGetSchema.setStandMoney(calResult);
								tLCGetSchema.setActuGet(calResult);
							}
						}
						// mLCGetBLSet.set(i, tLCGetSchema);
					}
					// end of if
				}
			}
			// end of for
		}

		// 责任总体计算,计算lcduty表PayYears,PayIntv,StandPrem,Prem,PayEndDate
		mLCDutySchema = this.calDutyFromPrem(mLCDutySchema, tLMDutySchema);
		// 对于GIL重算的特殊处理,如果是GIL重算的话,则不进行重新计LCDuty表的保额和风险保额
		// （GIL处理原lcudty.amnt,riskamnt计算有问题）通过算法处理,兼容原来处理方法
		// if(!tag) {
		// 计算lcduty表Amnt,RiskAmnt,GetStartDate
		mLCDutySchema = this.calDutyFromGet(mLCDutySchema, tLMDutySchema);
		// }

		if (this.mErrors.needDealError()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "责任总体计算失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}
		if (!dealFloatRate(mLCPremBLSet, mLCGetBLSet, mLCDutySchema)) {
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "处理浮动费率失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;

		}

		return mLCDutySchema;
	}

	/**
	 * 计算一个责任下的所有信息 add by winnie ASR20093075
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @param mLCPremSet 全部的责任
	 *            LCPremSet
	 * @param mLCGetSet
	 *            LCGetSet
	 * @return LCDutySchema
	 */
	public boolean calOneCacheDuty(LCDutyBL mLCDutyBL,
			LCPremBLSet cLCPremSetBL, LCGetBLSet cLCGetSetBL) {
		LCDutySchema cLCDutySchema = mLCDutyBL.getSchema();
		//modify by winnie ASR20093075  20090416
		// 准备描述的相关信息(查询责任录入控制信息-mLMDutyCtrlSet)
		if (!this.preDefineInfo()) {
			return false;
		}
		if (mLCDutyBLSet == null) {
			// 取得必选的责任-通过描述取得责任信息的结构,并将界面录入的责任信息存入-mLCDutyBLSet
			if (!this.getDutyStructure()) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "calPol";
				tError.errorMessage = "getDutyStructure方法返回失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		// 从传入的保单信息中获取责任表中需要计算的以外的信息
		this.getDutyOtherData();
		//modify by winnie ASR20093075 20090713
		if(mLCDutyBLSet!=null&&mLCDutyBLSet.size()>0){
			for(int i=1;i<=mLCDutyBLSet.size();i++){
				if(cLCDutySchema.getDutyCode().equals(mLCDutyBLSet.get(i).getDutyCode())){
					cLCDutySchema=mLCDutyBLSet.get(i);
				}
			}
		}
		mLCDutyBLIn.setSchema(cLCDutySchema);
		LCPremSet pLCPremSet=new LCPremSet();
		//end modify 20090714
		// 计算责任表中的部分信息(保险期间，交费期间，领取期间等)
		cLCDutySchema = this.calDutyOther(cLCDutySchema);
		if (cLCDutySchema == null) {
			return false;
		}
		//end modify 20090416
		String dutyCode = cLCDutySchema.getDutyCode();
		String calMode = "";
		// 如果界面没有录入浮动费率，此时mLCDutySchema的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
		// 保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
		FloatRate = cLCDutySchema.getFloatRate();
		InputPrem = cLCDutySchema.getPrem();
		InputAmnt = cLCDutySchema.getAmnt();

		// 保存计算方向
		String calRule = cLCDutySchema.getCalRule();
		if (calRule == null) {
			// 默认为表定费率
			cLCDutySchema.setCalRule("0");
		}
		// 如果是统一费率，需要去默认值里取出统一费率的值,算出保费保额
		else if (calRule.equals("1")) {
			autoCalFloatRateFlag = true;
		} else if (calRule.equals("3")) {
			autoCalFloatRateFlag = true;
			FloatRate = -1;
		}

		// 如果界面录入浮动费率=-1，则计算浮动费率的标记为真
		if (FloatRate == ConstRate) {
			autoCalFloatRateFlag = true;
			// 将保单中的该子段设为1，后面描述算法中计算
			cLCDutySchema.setFloatRate(1);
		}
		
		// 浮动费率处理END

		// 准备计算基础数据部分
		mCalBase = new CalBase();
		mCalBase.setRiskCode(mLCPolBL.getRiskCode());
//		mCalBase.setDutyCode(cLCDutySchema.getDutyCode());
		mCalBase.setCalRule(cLCDutySchema.getCalRule());
		mCalBase.setCalType(calType);
		mCalBase.setPrem(cLCDutySchema.getStandPrem());
		mCalBase.setGet(cLCDutySchema.getAmnt());
		mCalBase.setMult(cLCDutySchema.getMult());
		// 份数：按份卖的险种保存份数，其他险种存0。为了保证000301、000302算法的正确性，当份数为0时，该字段赋值为1。
		if (cLCDutySchema.getMult() == 0) {
			mCalBase.setMult(1);
		}
		mCalBase.setYears(cLCDutySchema.getYears());
		mCalBase.setInsuYear(cLCDutySchema.getInsuYear());
		mCalBase.setInsuYearFlag(cLCDutySchema.getInsuYearFlag());
		mCalBase.setAppAge(mLCPolBL.getInsuredAppAge());
		mCalBase.setSex(mLCPolBL.getInsuredSex());
		mCalBase.setJob(mLCPolBL.getOccupationType());
		mCalBase.setCount(mLCPolBL.getInsuredPeoples());
		mCalBase.setPolNo(mLCPolBL.getPolNo());
		mCalBase.setContNo(mLCPolBL.getContNo());
		if (StrTool.compareString(cLCDutySchema.getStandbyFlag1(), "")) {
			mCalBase.setStandbyFlag1("");
		} else {
			mCalBase.setStandbyFlag1(cLCDutySchema.getStandbyFlag1().trim());
		}

		if (StrTool.compareString(cLCDutySchema.getStandbyFlag2(), "")) {
			mCalBase.setStandbyFlag2("");
		} else {
			mCalBase.setStandbyFlag2(cLCDutySchema.getStandbyFlag2().trim());
		}

		if (StrTool.compareString(cLCDutySchema.getStandbyFlag3(), "")) {
			mCalBase.setStandbyFlag3("");
		} else {
			mCalBase.setStandbyFlag3(cLCDutySchema.getStandbyFlag3().trim());
		}

//		if (StrTool.compareString(cLCDutySchema.getStandbyFlag4(), "")) {
//			mCalBase.setStandbyFlag4("");
//		} else {
//			mCalBase.setStandbyFlag4(cLCDutySchema.getStandbyFlag4().trim());
//		}
//		// zhangjq 2008-4-18
//		// 加setStandbyFlag5、setStandbyFlag6（NMK04险种用StandbyFlag1~StandbyFlag6保存赔付倍数）
//		if (StrTool.compareString(cLCDutySchema.getStandbyFlag5(), "")) {
//			mCalBase.setStandbyFlag5("");
//		} else {
//			mCalBase.setStandbyFlag5(cLCDutySchema.getStandbyFlag5().trim());
//		}
//		// zhangjq 2008-4-18
//		// 加setStandbyFlag5、setStandbyFlag6（NMK04险种用StandbyFlag1~StandbyFlag6保存赔付倍数）
//		if (StrTool.compareString(cLCDutySchema.getStandbyFlag6(), "")) {
//			mCalBase.setStandbyFlag6("");
//		} else {
//			mCalBase.setStandbyFlag6(cLCDutySchema.getStandbyFlag6().trim());
//		}

		mCalBase.setGrpPolNo(mLCPolBL.getGrpPolNo());
		mCalBase.setGetLimit(cLCDutySchema.getGetLimit());
		mCalBase.setGetRate(cLCDutySchema.getGetRate());
		mCalBase.setSSFlag(cLCDutySchema.getSSFlag());
		mCalBase.setPeakLine(cLCDutySchema.getPeakLine());
		mCalBase.setCValiDate(mLCPolBL.getCValiDate());
//		mCalBase.setContPlanCode(mLCInsuredSchema.getContPlanCode());
//		mCalBase.setInsuredNo(mLCInsuredSchema.getInsuredNo());
		mCalBase.setGrpContNo(mLCInsuredSchema.getGrpContNo());
//		mCalBase.setSalary(mLCInsuredSchema.getSalary());
		// add by hanming
//		mCalBase.setPolTypeFlag(mLCPolBL.getPolTypeFlag());

		if (cLCDutySchema.getFloatRate() == 0.0) {
			cLCDutySchema.setFloatRate(1);
		}
		mCalBase.setFloatRate(cLCDutySchema.getFloatRate());

		int tRnewFlag = -8;
		if (mLCPolBL.getRenewCount() == 0) {
			tRnewFlag = 0;
		}
		if (mLCPolBL.getRenewCount() > 0) {
			tRnewFlag = 1;
		}
		mCalBase.setRnewFlag(tRnewFlag);

		LMDutySchema tLMDutySchema = mCRI.findDutyByDutyCode(dutyCode);

		if (tLMDutySchema == null) {
			// @@错误处理
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "LMDuty表查询失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (cLCDutySchema.getPayEndYearFlag() == null) {
			cLCDutySchema.setPayEndYearFlag(tLMDutySchema.getPayEndYearFlag());
		}
		if (cLCDutySchema.getGetYearFlag() == null) {
			cLCDutySchema.setGetYearFlag(tLMDutySchema.getGetYearFlag());
		}
		if (cLCDutySchema.getInsuYearFlag() == null) {
			cLCDutySchema.setInsuYearFlag(tLMDutySchema.getInsuYearFlag());
		}
		if (cLCDutySchema.getAcciYearFlag() == null) {
			cLCDutySchema.setAcciYearFlag(tLMDutySchema.getAcciYearFlag());
		}

		if (calType == null || calType.equals("")) {
			if (cLCDutySchema.getPremToAmnt() == null
					|| cLCDutySchema.getPremToAmnt().equals("")) {
				calMode = tLMDutySchema.getCalMode();
			} else {
				calMode = cLCDutySchema.getPremToAmnt();
			}
		} else {
			if (cLCDutySchema.getPremToAmnt() != null
					&& !("".equals(cLCDutySchema.getPremToAmnt()))) {
				calMode = cLCDutySchema.getPremToAmnt();
			} else if (tLMDutySchema.getCalMode() != null
					&& !("".equals(tLMDutySchema.getCalMode()))) {
				calMode = tLMDutySchema.getCalMode();
			} else {
				LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = null;
				if (this.isConCanUse)
					tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB(this.con);
				else
					tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
				tLMPolDutyEdorCalDB.setRiskCode(mLCPolBL.getRiskCode());
				tLMPolDutyEdorCalDB.setEdorType(calType);
				LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB
						.query();
				if (tLMPolDutyEdorCalDB.mErrors.needDealError()) {
					mErrors.copyAllErrors(tLMPolDutyEdorCalDB.mErrors);
					mErrors.addOneError(new CError("查询险种责任保全计算信息不成功！"));
					return false;
				}
				if (tLMPolDutyEdorCalSet.size() > 0) {
					calMode = tLMPolDutyEdorCalSet.get(1).getCalMode(); // 获取保全重算的计算方式
				} else {
					mErrors.copyAllErrors(tLMPolDutyEdorCalDB.mErrors);
					mErrors.addOneError(new CError("查询险种责任保全计算信息不成功！"));
					return false;
				}
			}
		}
		// 准备描述的相关信息(查询责任录入控制信息-mLMDutyCtrlSet)
		if (!this.preDefineInfo()) {
			return false;
		}
		int n = cLCPremSetBL.size();
		for (int i = 1; i <= n; i++) {
			LCPremSchema tLCPremSchema = cLCPremSetBL.get(i);
			if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
				pLCPremSet.add(tLCPremSchema);
				LMDutyPaySchema tLMDutyPaySchema = mCRI
						.findDutyPayByPayPlanCode(tLCPremSchema
								.getPayPlanCode());
				if (tLMDutyPaySchema == null) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calOneDuty";
					tError.errorMessage = "LMDutyPay表查询失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				//add by winnie ASR20093075 20090421
				tLCPremSchema.setPayStartDate(mLCPolBL.getCValiDate());
				//end add
				// 计算保费项中除保费外的其他信息
				tLCPremSchema = this.calPremOther(tLCPremSchema,
						tLMDutyPaySchema, cLCDutySchema);
				if (mOperator != null && !mOperator.equals("")) {
					tLCPremSchema.setOperator(mOperator);
					tLCPremSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPremSchema.setMakeTime(PubFun.getCurrentTime());
					tLCPremSchema.setModifyDate(PubFun.getCurrentDate());
					tLCPremSchema.setModifyTime(PubFun.getCurrentTime());
				}
			//	mLCPremBLSet.add(tLCPremSchema);
			} // end of if
			//modif by winnie ASR20093075 接收其他的责任对应的缴费信息
			mLCPremBLSet.add(tLCPremSchema);
			//end modify 20090715 
		} // end of for

		int m = cLCGetSetBL.size();
		for (int i = 1; i <= m; i++) {
			LCGetSchema tLCGetSchema = cLCGetSetBL.get(i);
			if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
				LMDutyGetSchema tLMDutyGetSchema = mCRI
						.findDutyGetByGetDutyCode(tLCGetSchema.getGetDutyCode());

				if (tLMDutyGetSchema == null) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calOneDuty";
					tError.errorMessage = "LMDutyGet表查询失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				// 取得相对应的传入的领取项的信息
				if (mGetFlag) {
					// 传入的是多条的GetSet
					int inpGetCount = mLCGetSetIn.size();
					for (int j = 1; j <= inpGetCount; j++) {
						LCGetSchema tLCGetSchema1 = mLCGetSetIn.get(j);
						if (tLCGetSchema1.getGetDutyCode().equals(
								tLCGetSchema.getGetDutyCode())) {
							mLCGetBLIn = tLCGetSchema1.getSchema();
							break;
						}
						// end of if
					}
					// end of for
				}
				// end of if
				// 计算领取项中除保费外的其他信息
				tLCGetSchema = this.calGetOther(tLCGetSchema, tLMDutyGetSchema,
						cLCDutySchema);
				if (this.mErrors.needDealError()) {
					return false;
				}
				if (mOperator != null && !mOperator.equals("")) {
					tLCGetSchema.setOperator(mLCGetBLIn.getOperator());
					tLCGetSchema.setMakeDate(mLCGetBLIn.getMakeDate());
					tLCGetSchema.setMakeTime(mLCGetBLIn.getMakeTime());
					tLCGetSchema.setModifyDate(mLCGetBLIn.getMakeDate());
					tLCGetSchema.setModifyTime(mLCGetBLIn.getMakeTime());
				}
				mLCGetBLSet.add(tLCGetSchema);
			} // end of if
		} // end of for

		if (!noCalFlag) // 如果需要计算保费,该标记由承保程序传入
		{
			// 计算保费
			int j = mLCPremBLSet.size();
			for (int i = 1; i <= j; i++) {
				LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);
				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					LMDutyPaySchema tLMDutyPaySchema = mCRI
							.findDutyPayByPayPlanCode(tLCPremSchema
									.getPayPlanCode());

					if (tLMDutyPaySchema == null) {
						// @@错误处理
						CError tError = new CError();
						tError.moduleName = "CalBL";
						tError.functionName = "calOneDuty";
						tError.errorMessage = "LMDutyPay表查询失败!";
						this.mErrors.addOneError(tError);
						return false;
					}

					// 准备计算基础数据部分
					mCalBase.setPayIntv(tLCPremSchema.getPayIntv());

					if (mLCGetBLIn.getGetDutyKind() == null) {
						// 此时保全会从后台查询出领取项，可能是多条，循环找出GetDutyKind不为空的一条赋值
						for (int t = 1; t <= mLCGetBLSet.size(); t++) {
							if (mLCGetBLSet.get(t).getGetDutyKind() != null) {
								mCalBase.setGetDutyKind(mLCGetBLSet.get(t)
										.getGetDutyKind());
							}
						}

					} else {
						// houzm add(将界面录入的GetDutyKind保存)
						mCalBase.setGetDutyKind(mLCGetBLIn.getGetDutyKind());
					}
					// 可以添加从界面传入的要素，该要素只是用于计算

					// 计算保费值
					double calResult = 0;
					if (!("3".equals(cLCDutySchema.getCalRule()) && ""
							.equals(calType))
							&& !("3".equals(cLCDutySchema.getCalRule()) && "IC"
									.equals(calType))
							&& !("3".equals(cLCDutySchema.getCalRule()) && "IO"
									.equals(calType))) {
						calResult = calPremValue(tLCPremSchema, cLCDutySchema,
								tLMDutyPaySchema, calMode);
					} else {
						calResult = cLCDutySchema.getPrem();
					}
					cLCDutySchema.setPremToAmnt(currCalMode);
					if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
							"N")) {
						if (calResult <= 0.0 && (!autoCalFloatRateFlag && InputPrem > 0 || !autoCalFloatRateFlag && InputAmnt > 0)) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保费计算失败";
							this.mErrors.addOneError(tError);
							return false;
						} else {
							tLCPremSchema.setStandPrem(calResult);
						}
					}
					if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
							"Y")) {
						if (calResult < 0.0) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保费计算失败";
							this.mErrors.addOneError(tError);
							return false;
						} else {
							tLCPremSchema.setStandPrem(calResult);
						}
					}
					tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
					//mLCPremBLSet.set(i, tLCPremSchema);
				} // end of if
				//modify by winnie ASR20090375接收其他的责任对应的缴费信息
				mLCPremBLSet.set(i, tLCPremSchema);
				//end modify 20090715
			} // end of for

			// 对于GIL重算的特殊处理.如果是GIL重算的话,则不重新算保额
			if (!tag) {
				// 计算保额
				int k = mLCGetBLSet.size();
				for (int i = 1; i <= k; i++) {
					LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

					if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
						LMDutyGetDB tLMDutyGetDB = null;
						if (this.isConCanUse)
							tLMDutyGetDB = new LMDutyGetDB(this.con);
						else
							tLMDutyGetDB = new LMDutyGetDB();
						tLMDutyGetDB.setGetDutyCode(tLCGetSchema
								.getGetDutyCode());
						if (!tLMDutyGetDB.getInfo()) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "LMDutyGet表查询失败!";
							this.mErrors.addOneError(tError);
							return false;
						}

						// 准备计算基础数据部分
						mCalBase.setGetIntv(tLCGetSchema.getGetIntv());
						mCalBase.setGDuty(tLCGetSchema.getGetDutyCode());
						mCalBase.setAddRate(tLCGetSchema.getAddRate());

						// 计算保额值
						double calResult = 0;
						calResult = calGetValue(tLCGetSchema, cLCDutySchema,
								tLMDutyGetDB, calMode);
						// null认为是N
						if (tLMDutyGetDB.getZeroFlag() == null
								|| tLMDutyGetDB.getZeroFlag().equals("")) {
							tLMDutyGetDB.setZeroFlag("N");
						}
						if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals(
								"N")) {
							if (calResult <= 0.0) {
								// @@错误处理
								CError tError = new CError();
								tError.moduleName = "CalBL";
								tError.functionName = "calOneDuty";
								tError.errorMessage = "保额计算失败!";
								this.mErrors.addOneError(tError);
								return false;
							} else {
								tLCGetSchema.setStandMoney(calResult);
								tLCGetSchema.setActuGet(calResult);
							}
						}
						if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals(
								"Y")) {
							if (calResult < 0.0) {
								// @@错误处理
								CError tError = new CError();
								tError.moduleName = "CalBL";
								tError.functionName = "calOneDuty";
								tError.errorMessage = "保额计算失败!";
								this.mErrors.addOneError(tError);
								return false;
							} else {
								tLCGetSchema.setStandMoney(calResult);
								tLCGetSchema.setActuGet(calResult);
							}
						}
					}
					// end of if
				}
			}
			// end of for
		}

		// 责任总体计算,计算lcduty表PayYears,PayIntv,StandPrem,Prem,PayEndDate
		cLCDutySchema = this.calDutyFromPrem(cLCDutySchema, tLMDutySchema);
		cLCDutySchema = this.calDutyFromGet(cLCDutySchema, tLMDutySchema);

		if (this.mErrors.needDealError()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "责任总体计算失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (!dealFloatRate(mLCPremBLSet, mLCGetBLSet, cLCDutySchema)) {
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "处理浮动费率失败!";
			this.mErrors.addOneError(tError);
			return false;

		}
		//modify by winnie ASR20093075 start
		if(noCalFlag){
			double sumPrem = 0;
			for (int i = 1; i <= pLCPremSet.size(); i++) {
				sumPrem += pLCPremSet.get(i).getStandPrem();
				for (int y=1; y<= mLCPremBLSet.size(); y++) // 找到对应的保费项修改金额
				{
					if (pLCPremSet.get(i).getDutyCode().equals(
							mLCPremBLSet.get(y).getDutyCode())
							&& pLCPremSet.get(i).getPayPlanCode().equals(
									mLCPremBLSet.get(y).getPayPlanCode())) {

						mLCPremBLSet.get(y).setPrem(pLCPremSet.get(i).getPrem());
						mLCPremBLSet.get(y).setSumPrem(
								mLCPremBLSet.get(y).getSumPrem()
										+ mLCPremBLSet.get(y).getPrem());
						mLCPremBLSet.get(y).setRate(pLCPremSet.get(i).getRate());
						// 对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
						// 但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
						mLCPremBLSet.get(y).setStandPrem(
								mLCPremBLSet.get(y).getPrem());

						// 保全人工核保处数据的需要add by sxy 2004-03-16
						// mLCPremBLSet.get(n).setState("0") ;

						mLCPremBLSet.get(y).setOperator(
								pLCPremSet.get(i).getOperator());
						mLCPremBLSet.get(y).setMakeDate(
								pLCPremSet.get(i).getMakeDate());
						mLCPremBLSet.get(y).setMakeTime(
								pLCPremSet.get(i).getMakeTime());

						break;
					}

				}
				cLCDutySchema.setAmnt(0); // 保额置0
				cLCDutySchema.setRiskAmnt(0); // 风险保额
				if (pLCPremSet.get(i).getDutyCode().equals(
						cLCDutySchema.getDutyCode())) {
						cLCDutySchema.setPrem(
								cLCDutySchema.getPrem()
										+ pLCPremSet.get(i).getPrem());
						cLCDutySchema.setSumPrem(
								cLCDutySchema.getPrem());
						// 对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
						// 但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
						cLCDutySchema.setStandPrem(
								cLCDutySchema.getPrem());
						break;
				}
			}
			mLCPolBL.setPrem(sumPrem);
			// 对于不定期交费，其标准保费置为0，实际保费置为实际缴纳金额
			// 但是后续处理需要标准保费，所以--后改为在承包描述完毕后，将这种类型的相关表的其标准保费置为0
			mLCPolBL.setStandPrem(sumPrem);
			mLCPolBL.setSumPrem(sumPrem);
			mLCPolBL.setAmnt(0); // 保额置0
			mLCPolBL.setRiskAmnt(0); // 风险保额

			for (int x = 1; x <= mLCGetBLSet.size(); x++) {
				mLCGetBLSet.get(x).setStandMoney(0); // 保额置0
				mLCGetBLSet.get(x).setActuGet(0); // 保额置0
			}
		}
		mLCDutyBLIn.setSchema(cLCDutySchema);
		//modify by winnie ASR20093075 20090713
		return true;
	}

	/**
	 * 计算一个责任下的所有信息,用于已输入保费项的情况
	 * 
	 * @param mLCDutySchema
	 *            LCDutySchema
	 * @return LCDutySchema
	 */
	private LCDutySchema calOneDuty2(LCDutySchema mLCDutySchema) {
		String dutyCode = mLCDutySchema.getDutyCode();
		String calMode = "";

		// 如果界面没有录入浮动费率，此时mLCDutySchema的FloatRate=0，那么在后面的程序中，会将mLCPolBL的FloatRate字段置为1
		// 保存浮动费率,录入的保费，保额:用于后面的浮动费率的计算
		FloatRate = mLCDutySchema.getFloatRate();

		InputPrem = mLCDutySchema.getPrem();
		InputAmnt = mLCDutySchema.getAmnt();

		// String strCalPrem = mDecimalFormat.format(InputPrem);
		// //转换计算后的保费(规定的精度)
		// String strCalAmnt = mDecimalFormat.format(InputAmnt); //转换计算后的保额
		// InputPrem = Double.parseDouble(strCalPrem);
		// InputAmnt = Double.parseDouble(strCalAmnt);

		// 保存计算方向
		String calRule = mLCDutySchema.getCalRule();
		if (calRule == null) {
			// 默认为表定费率
			mLCDutySchema.setCalRule("0");
		}
		// 如果是统一费率，需要去默认值里取出统一费率的值,算出保费保额
		else if (calRule.equals("1")) {
			autoCalFloatRateFlag = true;
		} else if (calRule.equals("3")) {
			autoCalFloatRateFlag = true;
			FloatRate = -1;
		}

		// 如果界面录入浮动费率=-1，则计算浮动费率的标记为真
		if (FloatRate == ConstRate) {
			autoCalFloatRateFlag = true;

			// 将保单中的该子段设为1，后面描述算法中计算
			mLCDutySchema.setFloatRate(1);
		}

		// 浮动费率处理END

		// 准备计算基础数据部分
		mCalBase = new CalBase();
		mCalBase.setCalType(calType);
		mCalBase.setPrem(InputPrem);
		// mCalBase.setPrem(mLCDutySchema.getStandPrem());
		// mCalBase.setPrem( mLCDutySchema.getPrem() );
		mCalBase.setGet(mLCDutySchema.getAmnt());
		mCalBase.setMult(mLCDutySchema.getMult());
		// 份数：按份卖的险种保存份数，其他险种存0。为了保证000301、000302算法的正确性，当份数为0时，该字段赋值为1。
		if (mLCDutySchema.getMult() == 0) {
			mCalBase.setMult(1);
		}
		mCalBase.setYears(mLCDutySchema.getYears());
		mCalBase.setInsuYear(mLCDutySchema.getInsuYear());
		mCalBase.setInsuYearFlag(mLCDutySchema.getInsuYearFlag());
		mCalBase.setAppAge(mLCPolBL.getInsuredAppAge());
		mCalBase.setSex(mLCPolBL.getInsuredSex());
		mCalBase.setJob(mLCPolBL.getOccupationType());
		mCalBase.setCount(mLCPolBL.getInsuredPeoples());
		mCalBase.setPolNo(mLCPolBL.getPolNo());
		mCalBase.setContNo(mLCPolBL.getContNo());
		mCalBase.setStandbyFlag1(mLCDutySchema.getStandbyFlag1());
		mCalBase.setStandbyFlag2(mLCDutySchema.getStandbyFlag2());
		mCalBase.setStandbyFlag3(mLCDutySchema.getStandbyFlag3());
//		mCalBase.setStandbyFlag4(mLCDutySchema.getStandbyFlag4());
		mCalBase.setGrpPolNo(mLCPolBL.getGrpPolNo());
		mCalBase.setGetLimit(mLCDutySchema.getGetLimit());
		mCalBase.setGetRate(mLCDutySchema.getGetRate());
		mCalBase.setSSFlag(mLCDutySchema.getSSFlag());
		mCalBase.setPeakLine(mLCDutySchema.getPeakLine());
		mCalBase.setCValiDate(mLCPolBL.getCValiDate());
//		mCalBase.setSalary(mLCInsuredSchema.getSalary());
		// add by hanming
//		mCalBase.setPolTypeFlag(mLCPolBL.getPolTypeFlag());

		// System.out.println("---------getlimit:" + mLCDutySchema.getGetLimit()
		// +
		// " getrate:" + mLCDutySchema.getGetRate());

		if (mLCDutySchema.getFloatRate() == 0.0) {
			mLCDutySchema.setFloatRate(1);
		}
		mCalBase.setFloatRate(mLCDutySchema.getFloatRate());

		int tRnewFlag = -8;
		if (mLCPolBL.getRenewCount() == 0) {
			tRnewFlag = 0;
		}
		if (mLCPolBL.getRenewCount() > 0) {
			tRnewFlag = 1;
		}
		mCalBase.setRnewFlag(tRnewFlag);

		LMDutySchema tLMDutySchema = mCRI.findDutyByDutyCode(dutyCode);

		if (tLMDutySchema == null) {
			// @@错误处理
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "LMDuty表查询失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}

		if (mLCDutySchema.getPayEndYearFlag() == null) {
			mLCDutySchema.setPayEndYearFlag(tLMDutySchema.getPayEndYearFlag());
		}
		if (mLCDutySchema.getGetYearFlag() == null) {
			mLCDutySchema.setGetYearFlag(tLMDutySchema.getGetYearFlag());
		}
		if (mLCDutySchema.getInsuYearFlag() == null) {
			mLCDutySchema.setInsuYearFlag(tLMDutySchema.getInsuYearFlag());
		}
		if (mLCDutySchema.getAcciYearFlag() == null) {
			mLCDutySchema.setAcciYearFlag(tLMDutySchema.getAcciYearFlag());
		}

		if (calType == null || calType.equals("")) {
			calMode = tLMDutySchema.getCalMode();
		} else {
			LMPolDutyEdorCalDB tLMPolDutyEdorCalDB = null;
			if (this.isConCanUse)
				tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB(this.con);
			else
				tLMPolDutyEdorCalDB = new LMPolDutyEdorCalDB();
			tLMPolDutyEdorCalDB.setRiskCode(mLCPolBL.getRiskCode());
			tLMPolDutyEdorCalDB.setEdorType(calType);
			LMPolDutyEdorCalSet tLMPolDutyEdorCalSet = tLMPolDutyEdorCalDB
					.query();
			if (tLMPolDutyEdorCalDB.mErrors.needDealError()) {
				mErrors.copyAllErrors(tLMPolDutyEdorCalDB.mErrors);
				mErrors.addOneError(new CError("查询险种责任保全计算信息失败！"));
				return null;
			}
			if (tLMPolDutyEdorCalSet.size() > 0) {
				calMode = tLMPolDutyEdorCalSet.get(1).getCalMode(); // 获取保全重算的计算方式
			} else {
				calMode = tLMDutySchema.getCalMode(); // 若不指定保全重算的计算方式，则默认承保的计算方式
			}
		}

		// 保费项结构部分
		if (!getPremStructure(mLCDutySchema)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "getPremStructure方法返回失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}
		// 从保单信息或责任信息中获取保费项表中需要计算的以外的信息
		this.getPremOtherData(mLCDutySchema);

		int n = mLCPremBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);

			if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
				LMDutyPaySchema tLMDutyPaySchema = mCRI
						.findDutyPayByPayPlanCode(tLCPremSchema
								.getPayPlanCode());

				if (tLMDutyPaySchema == null) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calOneDuty";
					tError.errorMessage = "LMDutyPay表查询失败!";
					this.mErrors.addOneError(tError);
					return mLCDutySchema;
				}

				// 计算保费项中除保费外的其他信息
				tLCPremSchema = this.calPremOther(tLCPremSchema,
						tLMDutyPaySchema, mLCDutySchema);
				if (mOperator != null && !mOperator.equals("")) {
					tLCPremSchema.setOperator(mOperator);
					tLCPremSchema.setMakeDate(PubFun.getCurrentDate());
					tLCPremSchema.setMakeTime(PubFun.getCurrentTime());
				}
				mLCPremBLSet.set(i, tLCPremSchema);
			} // end of if
		} // end of for

		// 领取项结构部分
		if (!getGetStructure(mLCDutySchema)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "getGetStructure方法返回失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}
		this.getGetOtherData();

		int m = mLCGetBLSet.size();
		for (int i = 1; i <= m; i++) {
			LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

			if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
				LMDutyGetSchema tLMDutyGetSchema = mCRI
						.findDutyGetByGetDutyCode(tLCGetSchema.getGetDutyCode());

				if (tLMDutyGetSchema == null) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "CalBL";
					tError.functionName = "calOneDuty";
					tError.errorMessage = "LMDutyGet表查询失败!";
					this.mErrors.addOneError(tError);
					return mLCDutySchema;
				}

				// 取得相对应的传入的领取项的信息
				if (mGetFlag) {
					// 传入的是多条的GetSet
					int inpGetCount = mLCGetSetIn.size();
					for (int j = 1; j <= inpGetCount; j++) {
						LCGetSchema tLCGetSchema1 = mLCGetSetIn.get(j);
						if (tLCGetSchema1.getGetDutyCode().equals(
								tLCGetSchema.getGetDutyCode())) {
							mLCGetBLIn = tLCGetSchema1.getSchema();
							break;
						}
						// end of if
					}
					// end of for
				}
				// end of if

				// 计算领取项中除保费外的其他信息
				tLCGetSchema = this.calGetOther(tLCGetSchema, tLMDutyGetSchema,
						mLCDutySchema);
				if (this.mErrors.needDealError()) {
					return null;
				}
				tLCGetSchema.setOperator(mLCGetBLIn.getOperator());
				tLCGetSchema.setMakeDate(mLCGetBLIn.getMakeDate());
				tLCGetSchema.setMakeTime(mLCGetBLIn.getMakeTime());
				mLCGetBLSet.set(i, tLCGetSchema);
			} // end of if
		} // end of for

		if (!noCalFlag) // 如果需要计算保费,该标记由承保程序传入
		{
			// 计算保费
			int j = mLCPremBLSet.size();
			for (int i = 1; i <= j; i++) {
				LCPremSchema tLCPremSchema = mLCPremBLSet.get(i);

				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					LMDutyPaySchema tLMDutyPaySchema = mCRI
							.findDutyPayByPayPlanCode(tLCPremSchema
									.getPayPlanCode());

					if (tLMDutyPaySchema == null) {
						// @@错误处理
						CError tError = new CError();
						tError.moduleName = "CalBL";
						tError.functionName = "calOneDuty";
						tError.errorMessage = "LMDutyPay表查询失败!";
						this.mErrors.addOneError(tError);
						return mLCDutySchema;
					}

					// 准备计算基础数据部分
					mCalBase.setPayIntv(tLCPremSchema.getPayIntv());

					// 可以添加从界面传入的要素，该要素只是用于计算

					// 计算保费值
					double calResult = 0;
					calResult = calPremValue(tLCPremSchema, mLCDutySchema,
							tLMDutyPaySchema, calMode);
					mLCDutySchema.setPremToAmnt(currCalMode);
					if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
							"N")) {
						if (calResult <= 0.0 && (!autoCalFloatRateFlag && InputPrem > 0 || !autoCalFloatRateFlag && InputAmnt > 0)) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保费计算失败";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						} else {
							tLCPremSchema.setStandPrem(calResult);
						}
					}
					if (StrTool.cTrim(tLMDutyPaySchema.getZeroFlag()).equals(
							"Y")) {
						if (calResult < 0.0) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保费计算失败";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						} else {
							tLCPremSchema.setStandPrem(calResult);
						}
					}
					tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
					mLCPremBLSet.set(i, tLCPremSchema);
				} // end of if
			} // end of for

			// 计算保额
			int k = mLCGetBLSet.size();
			for (int i = 1; i <= k; i++) {
				LCGetSchema tLCGetSchema = mLCGetBLSet.get(i);

				if (tLCGetSchema.getDutyCode().equals(dutyCode)) {
					LMDutyGetDB tLMDutyGetDB = null;
					if (this.isConCanUse)
						tLMDutyGetDB = new LMDutyGetDB(this.con);
					else
						tLMDutyGetDB = new LMDutyGetDB();
					tLMDutyGetDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
					if (!tLMDutyGetDB.getInfo()) {
						// @@错误处理
						CError tError = new CError();
						tError.moduleName = "CalBL";
						tError.functionName = "calOneDuty";
						tError.errorMessage = "LMDutyGet表查询失败!";
						this.mErrors.addOneError(tError);
						return mLCDutySchema;
					}

					// 准备计算基础数据部分
					mCalBase.setGetIntv(tLCGetSchema.getGetIntv());
					mCalBase.setGDuty(tLCGetSchema.getGetDutyCode());
					mCalBase.setAddRate(tLCGetSchema.getAddRate());

					if (mLCGetBLIn.getGetDutyKind() == null) {
						// 此时保全会从后台查询出领取项，可能是多条，循环找出GetDutyKind不为空的一条赋值
						for (int t = 1; t <= mLCGetBLSet.size(); t++) {
							if (mLCGetBLSet.get(t).getGetDutyKind() != null) {
								mCalBase.setGetDutyKind(mLCGetBLSet.get(t)
										.getGetDutyKind());
								break;
							}
						}

					} else {
						// houzm add(将界面录入的GetDutyKind保存)
						mCalBase.setGetDutyKind(mLCGetBLIn.getGetDutyKind());
					}

					// 计算保额值
					double calResult = 0;
					calResult = calGetValue(tLCGetSchema, mLCDutySchema,
							tLMDutyGetDB, calMode);
					// null认为是N
					if (tLMDutyGetDB.getZeroFlag() == null
							|| tLMDutyGetDB.getZeroFlag().equals("")) {
						tLMDutyGetDB.setZeroFlag("N");
					}
					if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals("N")) {
						if (calResult <= 0.0) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保额计算失败!";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						} else {
							tLCGetSchema.setStandMoney(calResult);
							tLCGetSchema.setActuGet(calResult);
						}
					}
					if (StrTool.cTrim(tLMDutyGetDB.getZeroFlag()).equals("Y")) {
						if (calResult < 0.0) {
							// @@错误处理
							CError tError = new CError();
							tError.moduleName = "CalBL";
							tError.functionName = "calOneDuty";
							tError.errorMessage = "保额计算失败!";
							this.mErrors.addOneError(tError);
							return mLCDutySchema;
						} else {
							tLCGetSchema.setStandMoney(calResult);
							tLCGetSchema.setActuGet(calResult);
						}
					}
					mLCGetBLSet.set(i, tLCGetSchema);
				}
				// end of if
			}
			// end of for
		}

		// 责任总体计算
		mLCDutySchema = this.calDutyFromPrem(mLCDutySchema, tLMDutySchema);
		mLCDutySchema = this.calDutyFromGet(mLCDutySchema, tLMDutySchema);
		if (this.mErrors.needDealError()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "责任总体计算失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;
		}
		if (!dealFloatRate(mLCPremBLSet, mLCGetBLSet, mLCDutySchema)) {
			this.mErrors.copyAllErrors(mCRI.mErrors);
			mCRI.mErrors.clearErrors();

			CError tError = new CError();
			tError.moduleName = "CalBL";
			tError.functionName = "calOneDuty";
			tError.errorMessage = "处理浮动费率失败!";
			this.mErrors.addOneError(tError);
			return mLCDutySchema;

		}

		return mLCDutySchema;
	}

	/**
	 * 从责任表中取得保单表中部分信息
	 * 
	 * @return boolean
	 */
	private boolean calPolFromDuty() {

		double sumAmnt = 0;
		double sumRiskAmnt = 0;
		double sumPrem = 0;
		int payYears = 0;
		int years = 0;
		Date minPayEndDate = fDate.getDate("3000-01-01");
		Date minGetStartDate = fDate.getDate("3000-01-01");
		Date maxEndDate = fDate.getDate("1900-01-01");
		Date maxAcciEndDate = fDate.getDate("1900-01-01");
		// ExeSQL mExeSQL = new ExeSQL();
		SSRS aSSRS = new SSRS();
		boolean tag = false;

		int n = mLCDutyBLSet.size();
		for (int i = 1; i <= n; i++) {
			LCDutySchema mLCDutySchema = mLCDutyBLSet.get(i);
			// guoxq 2007.3.16
			// 中意特殊处理，对于GIL的特殊处理，如果是GIL的话，取责任的最大保额为险种保额
			String SQL = "select dutycode from lmriskduty where dutycode ='"
					+ mLCDutySchema.getDutyCode()
					+ "' and "
					+ "riskcode in (select riskcode from lmriskapp where risktype8 = '1')";
			// System.out.println("判断是否为GIL的责任SQL :"+SQL);
			aSSRS = mExeSQL.execSQL(SQL);
			if (aSSRS.getMaxRow() > 0) {
				tag = true;
				sumAmnt = sumAmnt > mLCDutySchema.getAmnt() ? sumAmnt
						: mLCDutySchema.getAmnt();
				sumRiskAmnt = sumRiskAmnt > mLCDutySchema.getRiskAmnt() ? sumRiskAmnt
						: mLCDutySchema.getRiskAmnt();
			} else {
				sumAmnt += mLCDutySchema.getAmnt();
				sumRiskAmnt += mLCDutySchema.getRiskAmnt();
			}

			sumPrem += mLCDutySchema.getStandPrem();
			Date payEndDate = fDate.getDate(mLCDutySchema.getPayEndDate());
			if (payEndDate != null) {
				if (minPayEndDate.after(payEndDate)) {
					minPayEndDate = payEndDate;
				}
			}

			Date getStartDate = fDate.getDate(mLCDutySchema.getGetStartDate());
			if (getStartDate != null) {
				if (minGetStartDate.after(getStartDate)) {
					minGetStartDate = getStartDate;
				}
			}

			Date endDate = fDate.getDate(mLCDutySchema.getEndDate());
			if (endDate != null) {
				if (maxEndDate.before(endDate)) {
					maxEndDate = endDate;
				}
			}

			Date acciEndDate = fDate.getDate(mLCDutySchema.getAcciEndDate());
			if (acciEndDate != null) {
				if (maxAcciEndDate.before(acciEndDate)) {
					maxAcciEndDate = acciEndDate;
				}
			} else {
				maxAcciEndDate = null;
			}

			payYears = mLCDutySchema.getPayYears();
			years = mLCDutySchema.getYears();
		}

		// 如果最大保额大于GIL值的话,则将保额改为GIl的值
		// if(tag) {
		// String sql = "select GIL from lcgrppol where
		// grppolno='"+mLCPolBL.getGrpPolNo()+"'";
		// aSSRS = mExeSQL.execSQL(sql);
		// if(aSSRS.getMaxRow()>0 ) {
		// System.out.println("+++++++"+aSSRS.GetText(1, 1));
		// if(Double.parseDouble(aSSRS.GetText(1, 1))<sumAmnt) {
		// mLCPolBL.setAmnt(aSSRS.GetText(1, 1));
		// mLCPolBL.setRiskAmnt(aSSRS.GetText(1, 1));
		// }
		// }
		// }else{
		mLCPolBL.setAmnt(sumAmnt);
		mLCPolBL.setRiskAmnt(sumRiskAmnt);
		// }

		mLCPolBL.setStandPrem(sumPrem);
		mLCPolBL.setPrem(sumPrem);
		// mLCPolBL.setAmnt(sumAmnt);
		// mLCPolBL.setRiskAmnt(sumRiskAmnt);
		mLCPolBL.setPayEndDate(fDate.getString(minPayEndDate));
		mLCPolBL.setGetStartDate(fDate.getString(minGetStartDate));
		mLCPolBL.setEndDate(fDate.getString(maxEndDate));
		mLCPolBL.setAcciEndDate(fDate.getString(maxAcciEndDate));
		mLCPolBL.setPayYears(payYears);
		mLCPolBL.setYears(years);

		return true;
	}

	/**
	 * 计算风险保额和基本保额
	 * 
	 * @param tLCDutySchema
	 *            LCDutySchema
	 * @param tLMDutySchema
	 *            LMDutySchema
	 * @return LCDutySchema
	 */
	private LCDutySchema calAmnt(LCDutySchema tLCDutySchema,
			LMDutySchema tLMDutySchema) {
		double tAmnt = -1;
		double tRiskAmnt = -1;
		String tBasicCalCode = tLMDutySchema.getBasicCalCode();
		String tRiskCalCode = tLMDutySchema.getRiskCalCode();
		// add by hm begin
		// 20081215为兼容原来产品设置（原来的情况：如果是GIL重算的话,则不进行重新计LCDuty表的保额和风险保额），以后所有情况都重算，通过算法实现。
		if (tBasicCalCode.equals("000003")) {
			tBasicCalCode = "000031";
		}
		if (tRiskCalCode.equals("000003")) {
			tRiskCalCode = "000032";
		}
		// add by hm end
		String tStr = "";

		// 计算
		Calculator mCalculator = null;
		if (this.isConCanUse)
			mCalculator = new Calculator(this.con);
		else
			mCalculator = new Calculator();
		// 增加基本要素
		mCalculator.addBasicFactor("Get", mCalBase.getGet());
		mCalculator.addBasicFactor("Mult", mCalBase.getMult());
		mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
		mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv());
		mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv());
		mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
		mCalculator.addBasicFactor("Sex", mCalBase.getSex());
		mCalculator.addBasicFactor("Job", mCalBase.getJob());
		mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
		mCalculator.addBasicFactor("PayEndYearFlag", mCalBase
				.getPayEndYearFlag());
		mCalculator.addBasicFactor("GetStartDate", "");
		mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear());
		mCalculator.addBasicFactor("GetYearFlag", mCalBase.getGetYearFlag());
		mCalculator.addBasicFactor("Years", mCalBase.getYears());
		mCalculator.addBasicFactor("InsuYear", mCalBase.getInsuYear());
		mCalculator.addBasicFactor("InsuYearFlag", mCalBase.getInsuYearFlag());
		mCalculator.addBasicFactor("Grp", "");
		mCalculator.addBasicFactor("GetFlag", "");
		mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
		mCalculator.addBasicFactor("Count", mCalBase.getCount());
		mCalculator.addBasicFactor("FirstPayDate", "");
		mCalculator.addBasicFactor("RnewFlag", mCalBase.getRnewFlag());
		mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate());
		mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty());
		mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
		mCalculator.addBasicFactor("Amnt", mCalBase.getAmnt()); // 计算出来的保额
		mCalculator.addBasicFactor("FRate", mCalBase.getFloatRate());
		mCalculator.addBasicFactor("StandbyFlag1", mCalBase.getStandbyFlag1());
		mCalculator.addBasicFactor("StandbyFlag2", mCalBase.getStandbyFlag2());
		mCalculator.addBasicFactor("StandbyFlag3", mCalBase.getStandbyFlag3());
//		mCalculator.addBasicFactor("StandbyFlag4", mCalBase.getStandbyFlag4());
		mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
		mCalculator.addBasicFactor("GetLimit", mCalBase.getGetLimit());
		mCalculator.addBasicFactor("GetRate", mCalBase.getGetRate());
		mCalculator.addBasicFactor("SSFlag", mCalBase.getSSFlag());
		mCalculator.addBasicFactor("PeakLine", mCalBase.getPeakLine());
		mCalculator.addBasicFactor("CalType", mCalBase.getCalType());
		mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
//		mCalculator.addBasicFactor("Salary", mCalBase.getSalary());
		// add by hanming
//		mCalculator.addBasicFactor("DutyCode", mCalBase.getDutyCode());
		if (tag) // 是GIL处理
		{
			mCalculator.addBasicFactor("GILFlag", "1");
		} else {
			mCalculator.addBasicFactor("GILFlag", "0");
		}
		// 计算基本保额
		mCalculator.setCalCode(tBasicCalCode);

		tStr = mCalculator.calculate();
		if (tStr.trim().equals("")) {
			tAmnt = 0;
		} else {
			tAmnt = Double.parseDouble(tStr);
		}

		// 计算风险保额
		mCalculator.setCalCode(tRiskCalCode);

		tStr = mCalculator.calculate();
		if (tStr.trim().equals("")) {
			tRiskAmnt = 0;
		} else {
			tRiskAmnt = Double.parseDouble(tStr);
		}

		tLCDutySchema.setAmnt(tAmnt);
		tLCDutySchema.setRiskAmnt(tRiskAmnt);

		return tLCDutySchema;
	}

	/**
	 * 从外部给标志赋值
	 * 
	 * @param parmFlag
	 *            boolean
	 */
	public void setNoCalFalg(boolean parmFlag) {
		noCalFlag = parmFlag;
	}

	/**
	 * 判断是否需要处理浮动附率：如果需要，将相关数据变更
	 * 只处理页面录入保费，保额而没有录入浮动费率的情况：录入浮动费率的情况已经在险种计算公式描述中完成
	 * 在此之前，如果传入浮动费率，计算公式中已经计算过浮动费率
	 * 
	 * @param pLCPremBLSet
	 *            LCPremBLSet 保费项
	 * @param pLCGetBLSet
	 *            LCGetBLSet 领取项
	 * @param pLCDutyShema
	 *            LCDutySchema 责任项
	 * @return boolean
	 */
	private boolean dealFloatRate(LCPremBLSet pLCPremBLSet,
			LCGetBLSet pLCGetBLSet, LCDutySchema pLCDutyShema) {
		String dutyCode = pLCDutyShema.getDutyCode();
		if ("NI".equals(calType) || "LC".equals(calType)
				|| "CS".equals(calType)) // 上一个版本由于约定费率时浮动费率为空的情况下做ＩＣ时做不过去,所以加上了IC的校验,
		// 现在由于一般的IC做不过去,并且在生产和34环境都查不到浮动费率为空的数据,故去掉限制.
		{ // 保全增人和保障变更不计算浮动费率
			autoCalFloatRateFlag = false;
		}

		// 如果界面上录入保费和保额：且浮动费率==0(不计算浮动费率,校验录入的保费保额是否和计算得到的数据匹配
		if ((FloatRate == 0)
				&& (InputPrem > 0)
				&& (InputAmnt > 0)
				&& !("NI".equals(calType) || "LC".equals(calType) || "CS"
						.equals(calType))) {
			dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema); // 转换精度
			if ((InputPrem != pLCDutyShema.getPrem())
					|| (InputAmnt != pLCDutyShema.getAmnt())) {
				CError tError = new CError();
				tError.moduleName = "ProposalBL";
				tError.functionName = "dealFloatRate";
				tError.errorMessage = "您录入的保费保额（" + InputPrem + "," + InputAmnt
						+ "）与计算得到的保费保额（" + pLCDutyShema.getPrem() + ","
						+ pLCDutyShema.getAmnt() + "）不符合！";
				this.mErrors.addOneError(tError);

				return false;
			}

			return true;
		}

		// 如果界面上录入保费和保额：且浮动费率==规定的常量,那么认为该浮动费率需要自动计算得到
		// 如果界面上录入保费和保额：且浮动费率<>规定的常量,那么认为浮动费率已经给出，且在前面的保费保额计算中用到。此处不用处理
		// 该种险种的计算公式需要描述
		// 临时方案，先处理IC约定费率录入保费为0的情况
		if ((autoCalFloatRateFlag && InputPrem >= 0 && InputAmnt > 0)
				|| (autoCalFloatRateFlag && InputPrem == 0
						&& "IC".equals(calType)
						&& "3".equals(pLCDutyShema.getCalRule()) && InputAmnt > 0)) {
			String strCalPrem = String.valueOf(Arith.round(pLCDutyShema
					.getPrem(), mSCALE)); // 转换计算后的保费(规定的精度)
			String strCalAmnt = String.valueOf(Arith.round(pLCDutyShema
					.getAmnt(), mSCALE)); // 转换计算后的保额
			double calPrem = Double.parseDouble(strCalPrem);
			double calAmnt = Double.parseDouble(strCalAmnt);

			// 如果界面录入的保费保额与计算出来的保费保额相等，则认为浮动费率=1,不做任何处理返回
			if ((calPrem == InputPrem) && (calAmnt == InputAmnt)) {
				return true;
			}

			if (pLCDutyShema.getPrem() == 0) {
//				CError tError = new CError();
//				tError.moduleName = "ProposalBL";
//				tError.functionName = "dealFloatRate";
//				tError.errorMessage = "计算得到的保费为0,不能做浮动费率计算!";
//				this.mErrors.addOneError(tError);
//
//				return false;
				// 更新责任项
				pLCDutyShema.setPrem(InputPrem);
				pLCDutyShema.setStandPrem(InputAmnt);
//				 更新保费项
				for (int j = 1; j <= pLCPremBLSet.size(); j++) {

					LCPremSchema tLCPremSchema = pLCPremBLSet.get(j);
					if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
						if (!tLCPremSchema.getPayPlanCode().substring(0, 6).equals(
								"000000")) {
							tLCPremSchema.setStandPrem(InputPrem);
							// chenwm20070816 人工核保 调整GIL后 lcprem表没有存上新的保费
							tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
						}
						pLCPremBLSet.set(j, tLCPremSchema);
					}
				}
			}
			else {
			// 浮动费率的计算公式为：P(现)*A(原)/(A(现)*P(原))
				double formatFloatRate = 0;
				if (pLCDutyShema.getDutyCode().length() == 10
						&& pLCDutyShema.getPremToAmnt().equals("O"))// 针对CS解决要素变动后IC出现保费
				{// fengyan 2008-12-26
					String tdutycode = pLCDutyShema.getDutyCode().substring(0, 6);
					String sql = "select Amnt from lcduty where polno = '"
							+ pLCDutyShema.getPolNo() + "' and dutycode = '"
							+ tdutycode + "'";
					ExeSQL tExeSQL = new ExeSQL();
					if (tExeSQL.getOneValue(sql).equals("")) {
					} else {
						double Amnt = Double.valueOf(tExeSQL.getOneValue(sql))
								.doubleValue();
						InputAmnt = Amnt;
					}
				}
				FloatRate = (InputPrem * pLCDutyShema.getAmnt())
						/ (InputAmnt * pLCDutyShema.getPrem());
				// 长城团体意外伤害保险要求可选责任保额不算入总保额，此时该责任的amnt为0，
				// 采用约定费率计算时走到该处无法正确计算floatrate并无法正确保存保费，故增加以下部分：
				if (FloatRate == 0) {
					FloatRate = InputPrem / pLCDutyShema.getPrem();
				}
				// 以上由yeshu添加于2005-8-12
				formatFloatRate = Double.parseDouble(mFRDecimalFormat
						.format(FloatRate));
	
				// 更新责任项
				pLCDutyShema.setPrem(pLCDutyShema.getPrem() * FloatRate);
				pLCDutyShema.setStandPrem(pLCDutyShema.getStandPrem() * FloatRate);
	
				pLCDutyShema.setFloatRate(formatFloatRate); // 存储转换后的浮动费率精度
			
			// 更新保费项
			for (int j = 1; j <= pLCPremBLSet.size(); j++) {

				LCPremSchema tLCPremSchema = pLCPremBLSet.get(j);
				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					if (!tLCPremSchema.getPayPlanCode().substring(0, 6).equals(
							"000000")) {

						tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem()
								* FloatRate);
						// chenwm20070816 人工核保 调整GIL后 lcprem表没有存上新的保费
						tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());

					}

					pLCPremBLSet.set(j, tLCPremSchema);
				}
			}
			}
			// //更新责任项
			// for (int j = 1; j <= pLCDutyBLSet.size(); j++)
			// {
			// LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
			// tLCDutySchema.setStandPrem(tLCDutySchema.getStandPrem() *
			// FloatRate);
			// pLCDutyBLSet.set(j, tLCDutySchema);
			// }

			dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema); // 转换精度

			// 因为累计和可能存在进位误差（可能会有0.01元的），所以需要调整
			double sumDutyPrem = 0.0;
			for (int j = 1; j <= pLCPremBLSet.size(); j++) {
				LCPremSchema tLCPremSchema = pLCPremBLSet.get(j);
				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					sumDutyPrem += tLCPremSchema.getStandPrem();
				}
			}

			if (sumDutyPrem != InputPrem) {
				double diffMoney = sumDutyPrem - InputPrem;

				// //调整责任表
				// for (int j = 1; j <= pLCDutyBLSet.size(); j++)
				// {
				// if (pLCDutyBLSet.get(j).getStandPrem() > 0.0)
				// {
				// pLCDutyBLSet.get(j).setStandPrem(pLCDutyBLSet.get(j)
				// .getStandPrem()
				// - diffMoney);
				//
				// break;
				// }
				// }

				// 调整保费表
				for (int j = 1; j <= pLCPremBLSet.size(); j++) {
					if (pLCPremBLSet.get(j).getDutyCode().equals(dutyCode)) {
						if (pLCPremBLSet.get(j).getStandPrem() > 0) {
							pLCPremBLSet.get(j).setStandPrem(
									pLCPremBLSet.get(j).getStandPrem()
											- diffMoney);

							break;
						}
					}
				}
			}

			return true;
		}

		// 如果界面上录入保费.而保额为0：且浮动费率==规定的常量,那么认为录入的保费即为经过计算后得到的保费，
		// 此时，需要自动计算得到该浮动费率
		if ((autoCalFloatRateFlag) && (InputPrem > 0) && (InputAmnt == 0)) {
			String strCalPrem = String.valueOf(Arith.round(pLCDutyShema
					.getPrem(), mSCALE)); // 转换计算后的保费(规定的精度)
			double calPrem = Double.parseDouble(strCalPrem);

			// 如果界面录入的保费与计算出来的保费相等，则认为浮动费率=1,不做任何处理返回
			if (calPrem == InputPrem) {
				return true;
			}

			if (pLCDutyShema.getPrem() == 0) {
				CError tError = new CError();
				tError.moduleName = "CalBL";
				tError.functionName = "dealFloatRate";
				tError.errorMessage = "计算得到的保费为0,不能做浮动费率计算!";
				this.mErrors.addOneError(tError);

				return false;
			}

			// 浮动费率的计算公式为：P(现)/P(原)
			double formatFloatRate = 0;
			FloatRate = InputPrem / (pLCDutyShema.getPrem());
			formatFloatRate = Double.parseDouble(mFRDecimalFormat
					.format(FloatRate));

			// 更新责任项
			pLCDutyShema.setPrem(InputPrem);
			pLCDutyShema.setStandPrem(InputPrem);
			pLCDutyShema.setFloatRate(formatFloatRate); // 存储转换后的浮动费率精度

			// 更新保费项
			for (int j = 1; j <= pLCPremBLSet.size(); j++) {
				LCPremSchema tLCPremSchema = pLCPremBLSet.get(j);
				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					if (!tLCPremSchema.getPayPlanCode().substring(0, 6).equals(
							"000000")) {
						tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem()
								* FloatRate);
						tLCPremSchema.setPrem(tLCPremSchema.getStandPrem());
					}

					pLCPremBLSet.set(j, tLCPremSchema);
				}
			}

			// //更新责任项
			// for (int j = 1; j <= pLCDutyBLSet.size(); j++)
			// {
			// LCDutySchema tLCDutySchema = (LCDutySchema) pLCDutyBLSet.get(j);
			// tLCDutySchema.setStandPrem(tLCDutySchema.getStandPrem() *
			// FloatRate);
			// pLCDutyBLSet.set(j, tLCDutySchema);
			//
			// if ((pLCDutyBLSet.get(j).getStandPrem() == 0)
			// && (pLCPremBLSet.get(j).getStandPrem() != 0))
			// {
			// pLCPremBLSet.get(j).setStandPrem(0);
			// }
			// }

			dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema); // 转换精度

			// 因为累计和可能存在进位误差（可能会有0.01元的），所以需要调整
			double sumDutyPrem = 0.0;
			for (int j = 1; j <= pLCPremBLSet.size(); j++) {
				LCPremSchema tLCPremSchema = pLCPremBLSet.get(j);
				if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
					sumDutyPrem += tLCPremSchema.getStandPrem();
				}
			}

			if (sumDutyPrem != InputPrem) {
				double diffMoney = sumDutyPrem - InputPrem;

				// //调整责任表
				// for (int j = 1; j <= pLCDutyBLSet.size(); j++)
				// {
				// if (pLCDutyBLSet.get(j).getStandPrem() > 0.0)
				// {
				// pLCDutyBLSet.get(j).setStandPrem(pLCDutyBLSet.get(j)
				// .getStandPrem()
				// - diffMoney);
				//
				// break;
				// }
				// }

				// 调整保费表
				for (int j = 1; j <= pLCPremBLSet.size(); j++) {
					if (pLCPremBLSet.get(j).getDutyCode().equals(dutyCode)) {
						if (pLCPremBLSet.get(j).getStandPrem() > 0) {
							pLCPremBLSet.get(j).setStandPrem(
									pLCPremBLSet.get(j).getStandPrem()
											- diffMoney);

							break;
						}
					}
				}
			}

			return true;
		}

		// 否则，直接转换精度
		dealFormatModol(pLCPremBLSet, pLCGetBLSet, pLCDutyShema);

		return true;
	}

	/**
	 * 处理传入的数据，转换合适的精度
	 * 
	 * @param pLCPremBLSet
	 *            LCPremBLSet
	 * @param pLCGetBLSet
	 *            LCGetBLSet
	 * @param pLCDutyShema
	 *            LCDutySchema
	 * @return boolean
	 */
	private boolean dealFormatModol(LCPremBLSet pLCPremBLSet,
			LCGetBLSet pLCGetBLSet, LCDutySchema pLCDutyShema) {
		String dutyCode = pLCDutyShema.getDutyCode();
		String strCalPrem = String.valueOf(Arith.round(pLCDutyShema.getPrem(),
				mSCALE)); // 转换计算后的保费(规定的精度)
		String strCalAmnt = String.valueOf(Arith.round(pLCDutyShema.getAmnt(),
				mSCALE)); // 转换计算后的保额
		double calPrem = Double.parseDouble(strCalPrem);
		double calAmnt = Double.parseDouble(strCalAmnt);

		// 更新个人保单
		pLCDutyShema.setPrem(calPrem);
		pLCDutyShema.setStandPrem(calPrem);
		pLCDutyShema.setAmnt(calAmnt);

		// 更新保费项
		double tempPrem = 0;

		// double tempAmnt=0;
		for (int j = 1; j <= pLCPremBLSet.size(); j++) {
			LCPremSchema tLCPremSchema = pLCPremBLSet.get(j);
			if (tLCPremSchema.getDutyCode().equals(dutyCode)) {
				tempPrem = Arith.round(tLCPremSchema.getStandPrem(), mSCALE);
				tLCPremSchema.setStandPrem(tempPrem);
				pLCPremBLSet.set(j, tLCPremSchema);
			}
		}

		// 更新责任项

		tempPrem = Arith.round(pLCDutyShema.getStandPrem(), mSCALE);
		pLCDutyShema.setStandPrem(tempPrem);

		return true;
	}

	public void setOperator(String tOperator) {
		mOperator = tOperator;
	}

	public void setLCInsuredSchema(LCInsuredSchema tLCInsuredSchema) {
		this.mLCInsuredSchema.setSchema(tLCInsuredSchema);
	}

	public static void main(String[] args) {
		String a = "2004-03-01";
		System.out.println(a.substring(0, 5) + "01-01");
		// 001险种测试
		// LCPolBL tLCPolBL = new LCPolBL();
		// tLCPolBL.setPolNo("001");
		// tLCPolBL.setRiskCode("001");
		// tLCPolBL.setCValiDate("2002-07-20");
		// tLCPolBL.setRiskVersion("2002");
		// tLCPolBL.setOccupationType("3");
		// tLCPolBL.setMult(2);
		//
		// CalBL tC = new CalBL(tLCPolBL, "");

		// 002险种测试
		// LCPolBL tLCPolBL = new LCPolBL();
		// tLCPolBL.setPolNo("001");
		// tLCPolBL.setRiskCode("211601");
		// tLCPolBL.setCValiDate("2005-02-20");
		// tLCPolBL.setRiskVersion("2002");
		// tLCPolBL.setAmnt(1000);
		// tLCPolBL.setInsuredSex("1");
		// tLCPolBL.setInsuredBirthday("1977-05-03");
		// tLCPolBL.setInsuredAppAge(22);
		//
		// LCDutyBL tLCDutyBL = new LCDutyBL();
		// tLCDutyBL.setInsuYear(12);
		// tLCDutyBL.setInsuYearFlag("M");
		// tLCDutyBL.setPremToAmnt("G");
		// LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
		// tLCDutyBLSet.add(tLCDutyBL);
		//
		// CalBL tC = new CalBL(tLCPolBL, tLCDutyBLSet, "");

		// 003险种测试
		// LCPolBL tLCPolBL = new LCPolBL();
		// tLCPolBL.setPolNo("001");
		// tLCPolBL.setRiskCode("003");
		// tLCPolBL.setCValiDate("2002-07-20");
		// tLCPolBL.setRiskVersion("2002");
		// tLCPolBL.setPrem("3000");
		// tLCPolBL.setAmnt("1000000");
		//
		// CalBL tC = new CalBL(tLCPolBL, "");

		// 004险种测试
		// LCPolBL tLCPolBL = new LCPolBL();
		// tLCPolBL.setPolNo("001");
		// tLCPolBL.setRiskCode("004");
		// tLCPolBL.setCValiDate("2002-07-20");
		// tLCPolBL.setRiskVersion("2002");
		// tLCPolBL.setInsuredSex("1");
		// tLCPolBL.setMult("6");
		// tLCPolBL.setOccupationType("3");
		// tLCPolBL.setInsuredBirthday("1977-05-03");
		// tLCPolBL.setInsuredAppAge(22);
		//
		// LCDutyBL tLCDutyBL1 = new LCDutyBL();
		// tLCDutyBL1.setDutyCode("001001");
		//
		// LCDutyBL tLCDutyBL2 = new LCDutyBL();
		// tLCDutyBL2.setDutyCode("002001");
		// tLCDutyBL2.setPayEndYear(10);
		// tLCDutyBL2.setGetYear(50);
		// tLCDutyBL2.setPrem("2000");
		//
		// LCDutyBL tLCDutyBL3 = new LCDutyBL();
		// tLCDutyBL3.setDutyCode("003001");
		// tLCDutyBL3.setPrem("3000");
		// tLCDutyBL3.setAmnt("100000");
		//
		// LCDutyBLSet tLCDutyBLSet = new LCDutyBLSet();
		// tLCDutyBLSet.add(tLCDutyBL1);
		// tLCDutyBLSet.add(tLCDutyBL2);
		// tLCDutyBLSet.add(tLCDutyBL3);
		//
		// CalBL tC = new CalBL(tLCPolBL, tLCDutyBLSet, "");

		// boolean flag = tC.calPol();
		// tLCPolBL = tC.getLCPol();
	}
}
