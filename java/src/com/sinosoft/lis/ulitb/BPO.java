package com.sinosoft.lis.ulitb;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class BPO
{
    //外包批次状态
    public static final String BATCH_STATE_SENT = new String("01");  //发送前置机
    public static final String BATCH_STATE_GET = new String("02");  //外包返回
    public static final String BATCH_STATE_IMPORT = new String("03");  //已导入

    //外包任务状态
    public static final String MISSION_STATE_SEND_SUCC = new String("01"); //发送成功
    public static final String MISSION_STATE_DATA_ERR = new String("02"); //数据错误
    public static final String MISSION_STATE_CREATE_CONT = new String("03"); //生成保单
    public static final String MISSION_STATE_INPUT_CONT = new String("04"); //已录入
    public static final String MISSION_STATE_NO_DATA = new String("05"); //不录入
    public static final String MISSION_STATE_DELETE = new String("06"); //已删除

    //外包任务操作类型
    public static final String OPERATE_TYPE_SEND = new String("11"); //任务发送；
    public static final String OPERATE_TYPE_GET = new String("12"); //接收任务；
    public static final String OPERATE_TYPE_IMPORT = new String("13"); //数据导入；
    public static final String OPERATE_TYPE_UPDATE = new String("14"); //修改数据(修改不能进入系统的外包数据)
    public static final String OPERATE_TYPE_DELETE = new String("15"); //修改数据(修改不能进入系统的外包数据)

    public BPO()
    {
    }

    public static void main(String[] args)
    {
        BPO bpo = new BPO();
    }
}
