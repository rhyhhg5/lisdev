/*
 * <p>ClassName: OLCInuredListBL </p>
 * <p>Description: OLCInuredListBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft </p>
 * @Database:
 * @CreateDate：2005-07-27 17:39:01
 */
package com.sinosoft.lis.ulitb;

import java.io.DataOutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCInsuredListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCInsuredListSchema;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class OLCInuredListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LCInsuredListSchema mLCInsuredListSchema = new LCInsuredListSchema();
    private LCInsuredListSet mLCInsuredListSet = new LCInsuredListSet();
    private MMap map = new MMap();
//private LCInuredListSet mLCInuredListSet=new LCInuredListSet();
    /** 是否需要重算 */
    private boolean mNeedCalInsured = false;
    public OLCInuredListBL() {
    }

    public static void main(String[] args) {
        URL u = null;
        try {
            u = new URL(";");
        } catch (Exception ex) {

        }
        try {
            URLConnection Con = u.openConnection();
            DataOutputStream t = new DataOutputStream(Con.getOutputStream());
            t.writeBytes("");
        } catch (Exception ex) {

        }

    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("## " + mInputData);
        if (!tPubSubmit.submitData(this.mInputData, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLCInuredListBL Submit...");
        }
        mInputData = null;
        System.out.println("是否需要重新计算被保险人");
        if (mNeedCalInsured) {
            if (!calInsured()) {
                return false;
            }
        }
        return true;
    }

    /**
     * calInsured
     *
     * @return boolean
     */
    private boolean calInsured() {
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo",
                                      mLCInsuredListSchema.getGrpContNo());
        tTransferData.setNameAndValue("Flag", "N");
        tVData.add(tTransferData);
        tVData.add(this.mGlobalInput);
        ParseGuideIn tPGI = new ParseGuideIn();
        boolean ress = true;
        try {
            ress = tPGI.submitData(tVData, "INSERT||DATABASE");
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("calInsured", "发生异常错误 原因是: " + ex + "！");
            return false;
        }
        if (!ress) {
            this.mErrors.copyAllErrors(tPGI.mErrors);
            return false;
        }
        map = new MMap();
        map.add(tPGI.getSubmitData());
        map.put("update lccont set UWDate='" + PubFun.getCurrentDate() +
                "',UWTime='" + PubFun.getCurrentTime() + "',ApproveTime='" +
                PubFun.getCurrentTime() + "',ApproveDate='" +
                PubFun.getCurrentDate() + "',UWFlag='9',UWOperator='" +
                mGlobalInput.Operator + "',ApproveFlag='9' where grpcontno='" +
                this.mLCInsuredListSchema.getGrpContNo() + "'",
                "UPDATE");
        map.put("update lcpol set UWDate='" + PubFun.getCurrentDate() +
                "',UWTime='" + PubFun.getCurrentTime() + "',ApproveTime='" +
                PubFun.getCurrentTime() + "',ApproveDate='" +
                PubFun.getCurrentDate() + "',UWFlag='9',UWCode='" +
                mGlobalInput.Operator + "',ApproveFlag='9' where grpcontno='" +
                this.mLCInsuredListSchema.getGrpContNo() + "'",
                "UPDATE");
        VData vd = new VData();
        vd.add(map);
        PubSubmit tPubsubmit = new PubSubmit();
        if (!tPubsubmit.submitData(vd, null)) {
            buildError("delInsured", "递交失败！" + tPubsubmit.mErrors.getContent());
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (this.mLCInsuredListSet.size() > 0) {
                for (int i = 1; i <= mLCInsuredListSet.size(); i++) {
                    if (!mLCInsuredListSet.get(i).getPublicAccType().equals(""))
                    //如果不为空则表示为公共账户
                    {
                        mLCInsuredListSet.get(i).setInsuredName("公共账户");
                        mLCInsuredListSet.get(i).setSex("0");
                        String year = Integer.toString(Integer.parseInt(StrTool
                                .getYear())
                                - 30);
                        String brithday = StrTool.getDate(year,
                                StrTool.getMonth(),
                                StrTool.getDay());
                        brithday = StrTool.replace(brithday, "/", "-");
                        mLCInsuredListSet.get(i).setBirthday(brithday);
                        mLCInsuredListSet.get(i).setMakeDate(PubFun.
                                getCurrentDate());
                        mLCInsuredListSet.get(i).setModifyDate(PubFun.
                                getCurrentDate());
                        mLCInsuredListSet.get(i).setMakeTime(PubFun.
                                getCurrentTime());
                        mLCInsuredListSet.get(i).setModifyTime(PubFun.
                                getCurrentTime());
                    }
                }
                if (mLCInsuredListSet.size() > 0) {
                    map.put(mLCInsuredListSet, "DELETE&INSERT");
                }
            }
        }
        if (this.mOperate.equals("INSERT||INSURED")) {
            insertInsured();
        }
        //被保险人清单的修改
        if (this.mOperate.equals("UPDATE||INSURED")) {
            updateData();
        }
        if (this.mOperate.equals("DELETE||INSURED")) {
            deleteData();
        }
        /** 进行保费重算的校验,需要 */
        if (!needCalInsured()) {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean updateData() {
        System.out.println("## 测试 : ");
        if (mLCInsuredListSchema != null) {
            System.out.println(mLCInsuredListSchema.encode());

//            if (mLCInsuredListSchema.getGrpContNo() != null) {
//                System.out.println("@@@");
//                map.put(mLCInsuredListSchema, "UPDATE");
//            }
            if (StrTool.cTrim(mLCInsuredListSchema.getIDType()).equals("") &&
                !StrTool.cTrim(mLCInsuredListSchema.getIDNo()).equals("")) {
                mLCInsuredListSchema.setIDType("0"); //证件类型默认为身份证
            } else if (StrTool.cTrim(mLCInsuredListSchema.getIDType()).equals("") &&
                    StrTool.cTrim(mLCInsuredListSchema.getIDNo()).equals("")){
                mLCInsuredListSchema.setIDType(null); //证件类型默认为身份证
            }
            System.out.println("测试证件类型~~~~ ：" + mLCInsuredListSchema.getIDType());
            if (StrTool.cTrim(mLCInsuredListSchema.getOthIDType()).equals("") &&
                !StrTool.cTrim(mLCInsuredListSchema.getOthIDNo()).equals("")) {
                mLCInsuredListSchema.setOthIDType("1");
            } else {
                mLCInsuredListSchema.setOthIDType(null); //证件类型默认为身份证
            }

            /** 如果已经生成被保险人先进行删除动作 */
            if (mLCInsuredListSchema.getState().equals("1")) {
                System.out.println("已经生成了被保险人,需要进行保费重算");
                this.mNeedCalInsured = true;
                MMap tMap = deleteCont();
                if (tMap == null) {
                    return false;
                }
                this.map.add(tMap);
                /** 将被保人清单状态置为待保费计算状态 */
                mLCInsuredListSchema.setState("0");
            }
            mLCInsuredListSchema.setModifyDate(PubFun.getCurrentDate());
            mLCInsuredListSchema.setModifyTime(PubFun.getCurrentTime());
            this.map.put(mLCInsuredListSchema, "UPDATE");

        }
        return true;
    }

    private MMap deleteCont() {
        String tContNo = mLCInsuredListSchema.getContNo();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo()) {
            return null;
        }
        VData cInputData = new VData();
        TransferData mTransferData = new TransferData();
        cInputData.add(mGlobalInput);
        cInputData.add(tLCContDB.getSchema());
        cInputData.add(mTransferData);
        ContDeleteUI tContDeleteUI = new ContDeleteUI();
        if (tContDeleteUI.submitData(cInputData, "GETMAP") == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tContDeleteUI.mErrors);
            return null;
        }
        VData tResult = tContDeleteUI.getResult();
        MMap tMap = (MMap) tResult.getObjectByObjectName("MMap", 0);
        if (tMap == null || tMap.size() <= 0) {
            buildError("updateData", "生成删除数据失败！");
            return null;
        }
        return tMap;
    }

    /**
     * needCalInsured
     *
     * @return boolean
     */
    private boolean needCalInsured() {
        String mGrpContNo = this.mLCInsuredListSchema.getGrpContNo();
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setGrpContNo(mGrpContNo);
        if("".equals(mGrpContNo) || "null".equals(mGrpContNo) || mGrpContNo == null){
        	buildError("needCalInsured", "传入的grpcontno 为空!");
            return false;
        }
        LCContPlanSet tLCContPlanSet = tLCContPlanDB.query();
        /** 如果存在保障计划 */
        if (tLCContPlanSet.size() > 0) {
            this.mNeedCalInsured = true;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean deleteData() {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setSchema(mLCInsuredListSchema);
        if (!tLCInsuredListDB.getInfo()) {
            this.mErrors.copyAllErrors(tLCInsuredListDB.mErrors);
            return false;
        }
        MMap tMap = deleteCont();
        if (tMap != null) {
            map.add(tMap);
        }
        this.map.put(tLCInsuredListDB.getSchema(), "DELETE");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mLCInsuredListSchema = (LCInsuredListSchema) cInputData.
                                    getObjectByObjectName(
                                            "LCInsuredListSchema", 0);
        this.mLCInsuredListSet = (LCInsuredListSet) cInputData.
                                 getObjectByObjectName(
                                         "LCInsuredListSet", 0);
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean submitquery() {
        this.mResult.clear();
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setSchema(this.mLCInsuredListSchema);
        //如果有需要处理的错误，则返回
        if (tLCInsuredListDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCInsuredListDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInuredListBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLCInsuredListSchema);
            this.mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLCInsuredListSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInuredListBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 添加被保险人
     * @return boolean
     */
    private boolean insertInsured() {
        if (mLCInsuredListSchema == null) {
            System.out.println(
                    "程序第225行出错，请检查OLCInuredListBL.java中的insertInsured方法！");
            CError tError = new CError();
            tError.moduleName = "OLCInuredListBL.java";
            tError.functionName = "insertInsured";
            tError.errorMessage = "传入的背保险人信息为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql =
                "select count(insuredid)+1 from lcinsuredlist where grpcontno='" +
                mLCInsuredListSchema.getGrpContNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String InsuredID = "";
        try {
            InsuredID = tExeSQL.getOneValue(sql);
        } catch (Exception ex) {
            InsuredID = "1";
        }
        mLCInsuredListSchema.setState("0");
        mLCInsuredListSchema.setContNo(InsuredID);
        mLCInsuredListSchema.setInsuredID(InsuredID);
        mLCInsuredListSchema.setBatchNo(PubFun1.CreateMaxNo("BatchNo", 10));
        if (StrTool.cTrim(mLCInsuredListSchema.getEmployeeName()).equals("")) {
            mLCInsuredListSchema.setEmployeeName(mLCInsuredListSchema.
                                                 getInsuredName()); //证件类型默认为身份证
        }
        if (StrTool.cTrim(mLCInsuredListSchema.getRetire()).equals("")) {
            mLCInsuredListSchema.setRetire("0"); //证件类型默认为身份证
        }
        if (StrTool.cTrim(mLCInsuredListSchema.getIDType()).equals("") &&
            !StrTool.cTrim(mLCInsuredListSchema.getIDNo()).equals("")) {
            mLCInsuredListSchema.setIDType("0"); //证件类型默认为身份证
        } else {
            mLCInsuredListSchema.setIDType(null); //证件类型默认为身份证
        }
        System.out.println("测试证件类型：" + mLCInsuredListSchema.getIDType());
        if (StrTool.cTrim(mLCInsuredListSchema.getRelation()).equals("")) {
            mLCInsuredListSchema.setRelation("00"); //证件类型默认为身份证
        }
        if (StrTool.cTrim(mLCInsuredListSchema.getOthIDType()).equals("") &&
            !StrTool.cTrim(mLCInsuredListSchema.getOthIDNo()).equals("")) {
            mLCInsuredListSchema.setOthIDType("1");
        } else {
            mLCInsuredListSchema.setOthIDType(null); //证件类型默认为身份证
        }
        if (StrTool.cTrim(mLCInsuredListSchema.getContPlanCode()).equals("")) {
            mLCInsuredListSchema.setContPlanCode("00");
        }

        mLCInsuredListSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCInsuredListSchema);
        this.map.put(this.mLCInsuredListSchema, "INSERT");
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OLCInuredListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
