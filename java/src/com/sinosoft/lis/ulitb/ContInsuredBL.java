/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.ulitb;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.lang.reflect.*;
import java.lang.Class;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: ContInsuredBL </p> <p>Description: ContInsuredBL类文件 </p> <p>Copyright:
 * Copyright (c) 2002</p> <p>Company: testcompany </p> @Database:
 * @CreateDate：2004-11-18 10:09:44
 */
public class ContInsuredBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private SSRS tSSRS = new SSRS(); // 备用

	private ExeSQL tExeSQL;

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private Connection con;

	private boolean isConCanUse = false;

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private Reflections ref = new Reflections();

	private TransferData mTransferData = new TransferData();

	private MMap map = new MMap();

	// 统一更新日期，时间
	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	/** 数据操作字符串 */
	private String mOperate;

	private boolean FirstTrialFlag = false;

	/** 业务处理相关变量 */
	private LCGrpContDB mLCGrpContDB = new LCGrpContDB();

	private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();

	private LCInsuredDB mOLDLCInsuredDB = new LCInsuredDB(); // 记录需要删除或修改的客户

	private LDPersonSchema mLDPersonSchema = new LDPersonSchema(); // 从界面传入的客户数据

	private LDPersonSchema tLDPersonSchema; // tLDPersonSet包含需要新建的客户

	// private ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
	// ES_DOC_RELATIONSchema(); //添加到关联表被保险人的数据

	// 存放主被保险人的InsuredNo,用于连带被保险人时 去寻找主被保险人的InsuredNo
	private HashMap otmInsuredNo = new HashMap();

	private HashMap otmInsuredNoCvalidate = new HashMap();
	private HashMap otmInsuredNoOrange = new HashMap();//PIR20092076
	private HashMap otmInsuredIdCvalidate = new HashMap();

	private HashMap otmInsuredId = new HashMap();

	private LCContSchema mLCContSchema = new LCContSchema();

	private LCAddressSchema mLCAddressSchema = new LCAddressSchema();

	private LCAddressSchema tLCAddressSchema;

	// private LCAccountSchema mLCAccountSchema = new LCAccountSchema();
	private LCCustomerImpartSet mLCCustomerImpartSet;

	private LCCustomerImpartParamsSet mLCCustomerImpartParamsSet;

	private LCCustomerImpartDetailSet mLCCustomerImpartDetailSet;

	private LCPolSet preLCPolSet = new LCPolSet();

	private String ContPostalAddress;

	private String ContZipCode;

	private String ContNo;

	private String FamilyType;

	private String ContType;

	private String EdorType;

	private String EdorNo;

	private String EdorAcceptNo;

	private String tPolTypeFlag;

	private String tInsuredPeoples;

	private String tInsuredAppAge;

	private String tSequenceNo; // 客户顺序号
	
	private String tNoticeWay =""; //理赔通知方式

	private LCContPlanDB tLCContPlanDB1 = new LCContPlanDB();

	private LCContPlanSchema tLCContPlanSchema1 = new LCContPlanSchema();

	private LCContPlanSet tLCContPlanSet1 = new LCContPlanSet();

	private LCContDB tLCContDB1 = new LCContDB();

	private LCContSchema tLCContSchema1 = new LCContSchema();

	private LCContSet tLCContSet1 = new LCContSet();

	private final int mSCALE = 2;// 保费保额计算出来后的精确位数

	// add by winnie ASR20093075 上载使用
	private LPGrpEdorItemSchema mLPGrpEdorItemSchema = new LPGrpEdorItemSchema();// 团险保全项目表

//	private LPGrpEdorMainSchema mLPGrpEdorMainSchema = new LPGrpEdorMainSchema();// 团险保全批改表

	private LPEdorItemSchema mLPEdorItemSchema = new LPEdorItemSchema();// 个险保全项目表

	private LPEdorMainSchema mLPEdorMainSchema = new LPEdorMainSchema();// 个险保全批改表

	// end add
	
	//add by winnie ASR20093243 被保人对应的分支机构的上下级关系
	private Map mInsuredComMap = new HashMap();
	
	LCGeneralToRiskSet mLCGeneralToRiskSet ;//统括保单的比例配制信息
	//end add
	public ContInsuredBL() {
		tExeSQL = new ExeSQL();
	}

	public ContInsuredBL(Connection tConnection) {
		this.con = tConnection;
		isConCanUse = true;
		tExeSQL = new ExeSQL(this.con);
		mLCGrpContDB = new LCGrpContDB(this.con);
		tLCContDB1 = new LCContDB(this.con);
		tLCContPlanDB1 = new LCContPlanDB(this.con);
		mOLDLCInsuredDB = new LCInsuredDB(this.con);
	}

	/**
	 * 处理数据，不提交后台执行
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean preparesubmitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!mOperate.equals("DELETE||CONTINSURED")) {
			if (!this.checkData()) {
				return false;
			}
			// System.out.println("---checkData---");
			// 进行业务处理
			if (!dealData()) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据处理失败!";
				// chenwm0114 导人的时候一个人会出现多条错误信息,这条没用, 界面添加人也不需要,有详细的错误信息..
				// this.mErrors.addOneError(tError);
				return false;
			}
		} else {
			if (!deleteData()) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "deleteData";
				tError.errorMessage = "删除数据时失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (mOperate.equals("DELETE||INSUREDRISK")) {
			// System.out.println("asfdsfsfafasf");
			if (!delrisk(cInputData)) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据处理失败ContInsuredBL-->delrisk!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} else {
			if (!getInputData(cInputData)) {
				return false;
			}
			if (!mOperate.equals("DELETE||CONTINSURED")) {
				if (!this.checkData()) {
					return false;
				}
				// System.out.println("---checkData---");
				// 进行业务处理
				if (!dealData()) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "submitData";
					tError.errorMessage = "数据处理失败";
					this.mErrors.addOneError(tError);
					return false;
				}
			} else {
				if (!deleteData()) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "deleteData";
					tError.errorMessage = "删除数据时失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			// 在进行被保险人修改的之后,如果有个人险种要进行重算
			if (mOperate.equals("UPDATE||CONTINSURED")
					&& preLCPolSet.size() > 0) {
				LCContSchema aftLCContSchema = new LCContSchema();
				// LCPolSet aftLCPolSet = new LCPolSet();
				VData tInputData = new VData();
				ReCalInsuredBL tReCalInsuredBl = new ReCalInsuredBL(
						mLCInsuredSchema, preLCPolSet, mLCContSchema,
						mGlobalInput);
				if (!tReCalInsuredBl.reCalInsured()) {
					this.mErrors.copyAllErrors(tReCalInsuredBl.mErrors);
					return false;
				}
				tInputData = tReCalInsuredBl.getResult();
				map.add((MMap) tInputData.getObjectByObjectName("MMap", 0));
				// 团单下，单更新了个人合同时，也需要更新团体合同和集体险种表
				if (ContType.equals("2")) {
					if ((LCContSchema) tInputData.getObjectByObjectName(
							"LCContSchema", 0) != null) {
						aftLCContSchema = (LCContSchema) tInputData
								.getObjectByObjectName("LCContSchema", 0);
						// aftLCPolSet = (LCPolSet)
						// tInputData.getObjectByObjectName("LCPolSet", 0);
						mLCGrpContDB.setGrpContNo(aftLCContSchema
								.getGrpContNo());

						// 集体险种信息
						StringBuffer tSBWherePart = new StringBuffer(64);
						tSBWherePart.append("from LCPol where GrpContNo='");
						tSBWherePart.append(mLCGrpContDB.getGrpContNo());
						tSBWherePart.append("')");

						StringBuffer tSBql = new StringBuffer(256);
						tSBql
								.append("update LCGrpCont set Prem=(select SUM(Prem) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",Amnt=(select SUM(Amnt) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",SumPrem=(select SUM(SumPrem) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",Mult=(select SUM(Mult) ");
						tSBql.append(tSBWherePart);
						tSBql
								.append(",Peoples2=(select SUM(Peoples) from lccont where grpcontno='");
						tSBql.append(mLCGrpContDB.getGrpContNo());
						// tSBql.append("') where grpcontno='");
						tSBql
								.append("' and PolType<>'2' and PolType<>'5') where grpcontno='"); // Alex
						// 不统计公共帐户虚拟人
						// 20050924
						tSBql.append(mLCGrpContDB.getGrpContNo());
						tSBql.append("'");
						map.put(tSBql.toString(), "UPDATE");
						// 更新集体险种表
						for (int i = 1; i <= preLCPolSet.size(); i++) {
							// String fromPart = "from LCPol where GrpContNo='"
							// +
							// aftLCContSchema.getGrpContNo()
							// + "' and riskcode ='" +
							// preLCPolSet.get(i).getRiskCode() +
							// "')";
							tSBWherePart = new StringBuffer(64);
							tSBWherePart.append("from LCPol where GrpContNo='");
							tSBWherePart.append(aftLCContSchema.getGrpContNo());
							tSBWherePart.append("' and riskcode ='");
							tSBWherePart.append(preLCPolSet.get(i)
									.getRiskCode());
							tSBWherePart.append("')");

							// Alex 不统计公共帐户虚拟人 20050924
							StringBuffer tSBWherePart1 = new StringBuffer(64);
							tSBWherePart1
									.append("from LCPol where GrpContNo='");
							tSBWherePart1
									.append(aftLCContSchema.getGrpContNo());
							tSBWherePart1.append("' and riskcode ='");
							tSBWherePart1.append(preLCPolSet.get(i)
									.getRiskCode());
							tSBWherePart1
									.append("' and PolTypeFlag<>'2' and PolTypeFlag<>'5')");

							tSBql = new StringBuffer(256);
							tSBql
									.append("update LCGrpPol set Prem=(select SUM(Prem) ");
							tSBql.append(tSBWherePart);
							tSBql.append(", Amnt=(select SUM(Amnt) ");
							tSBql.append(tSBWherePart);
							tSBql.append(", SumPrem=(select SUM(SumPrem) ");
							tSBql.append(tSBWherePart);
							tSBql.append(", Mult=(select SUM(Mult) ");
							tSBql.append(tSBWherePart);
							tSBql
									.append(", Peoples2=(select SUM(InsuredPeoples) ");
							tSBql.append(tSBWherePart1);
							tSBql.append(" where grppolno='");
							tSBql.append(preLCPolSet.get(i).getGrpPolNo());
							tSBql.append("'");
							// 这个地方不知道对错，需要实际测试
							map.put(tSBql.toString(), "UPDATE");
							// map.put("update LCGrpPol set Prem=(select
							// SUM(Prem) " + fromPart
							// + ", Amnt=(select SUM(Amnt) " + fromPart
							// + ", SumPrem=(select SUM(SumPrem) " +
							// fromPart
							// + ", Mult=(select SUM(Mult) " + fromPart
							// + ", Peoples2=(select SUM(InsuredPeoples) " +
							// fromPart + " where grppolno='" +
							// preLCPolSet.get(i).getGrpPolNo()
							// + "'", "UPDATE");
						}
					}

				}
			}
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}

		// System.out.println("Start tPRnewManualDunBLS Submit...");
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, mOperate)) {
			mInputData = null;
			return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}
	}
	
	/**
	 * 取老被保人信息的方法 Added By xbing for ASR20106330_团体保单主子合同保单号设置需求
	 * 
	 * @param tLCInsuredSchema
	 *            LCInsuredSchema
	 */
	private LCInsuredSchema getOldInsured(LCInsuredSchema tLCInsuredSchema,
			String tOldGrpContno) {
		StringBuffer tSBql = new StringBuffer(128);
		tSBql.append("Select b.* From insured_view b Where b.GrpContNo='");
		tSBql.append(tOldGrpContno);
		tSBql.append("' and b.name='");
		tSBql.append(tLCInsuredSchema.getName());
		tSBql.append("' and b.IDType='");
		tSBql.append(tLCInsuredSchema.getIDType());
		tSBql.append("' and upper(b.IDNo)=upper('");
		tSBql.append(tLCInsuredSchema.getIDNo());
		tSBql.append("') and b.Sex='");
		tSBql.append(tLCInsuredSchema.getSex());
		tSBql.append("' and b.Birthday=to_date('");
		tSBql.append(tLCInsuredSchema.getBirthday());
		tSBql.append("','YYYY-MM-DD')");

		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		LCInsuredSet orgLCInsuredSet = tLCInsuredDB.executeQuery(tSBql
				.toString());
		if (orgLCInsuredSet == null || orgLCInsuredSet.size() <= 0) {
			if (!"0".equals(tLCInsuredSchema.getIDType()))
				return null;
			else {// 15<-->18转换后再查一次
				tSBql = new StringBuffer(128);
				tSBql
						.append("Select b.* From insured_view b Where b.GrpContNo='");
				tSBql.append(tOldGrpContno);
				tSBql.append("' and b.name='");
				tSBql.append(tLCInsuredSchema.getName());
				tSBql.append("' and b.IDType='");
				tSBql.append(tLCInsuredSchema.getIDType());
				tSBql.append("' and b.Sex='");
				tSBql.append(tLCInsuredSchema.getSex());
				tSBql.append("' and b.Birthday=to_date('");
				tSBql.append(tLCInsuredSchema.getBirthday());
				tSBql.append("','YYYY-MM-DD')");
				orgLCInsuredSet = tLCInsuredDB.executeQuery(tSBql.toString());
				for (int i = 1; i <= orgLCInsuredSet.size(); i++) {
					if (PubFun.CompareId(orgLCInsuredSet.get(i).getIDNo(),
							tLCInsuredSchema.getIDNo()))
						return orgLCInsuredSet.get(i);
				}
				return null;
			}
		}
		return orgLCInsuredSet.get(1).getSchema();
	}

	/**
	 * 校验传入的数据
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
//		Added By xbing for ASR20106330_团体保单主子合同保单号设置需求
		//如果保单号和主合同号不相同，则该保单为子保单，需对上载的人员与主保单进行对比，五项基本信息不一致的要报错。
//		if (null != mLCInsuredSchema.getGrpContNo() && !"".equals(mLCInsuredSchema.getGrpContNo())){//区分批量导入和单个被保人添加，只有单个添加时才走此处
//			LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
//			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
//			tLCGrpContDB.setGrpContNo(mLCInsuredSchema.getGrpContNo());
//			tLCGrpContSchema = tLCGrpContDB.query().get(1).getSchema();
//			if (!tLCGrpContSchema.getGrpContNo().equals(tLCGrpContSchema.getMainGrpContNo())){
//				LCInsuredSchema oldInsuredSchema = new LCInsuredSchema();
//				oldInsuredSchema = getOldInsured(mLCInsuredSchema,tLCGrpContSchema.getMainGrpContNo());
//				
//				if(oldInsuredSchema!=null){//通过投保单号从lcgrpcont中取得起保日期，然后在LCCONT表中判断子保单的起保日期是否在主保单该被保险人的保障期间内。
//					String strSql = "select to_char(cvalidate) from lcgrpcont where prtno = '"+tLCGrpContSchema.getPrtNo()+"'";
//					String cvalidate = "";
//					cvalidate = tExeSQL.getOneValue(strSql);
//					strSql = "select contno from lccont where insuredno = '"+oldInsuredSchema.getInsuredNo()+"' and grpcontno = '"+tLCGrpContSchema.getMainGrpContNo()+"' " +
//							" and cvalidate <= '"+cvalidate+"' and paytodate >= '"+cvalidate+"' ";
//					String flag = "";
//					flag = tExeSQL.getOneValue(strSql);
//					if(null == flag || "".equals(flag)){
//						CError tError = new CError();
//						tError.moduleName = "ContInsuredBL";
//						tError.functionName = "checkData";
//						tError.errorMessage = "被保人"+mLCInsuredSchema.getName()+"证件号"+mLCInsuredSchema.getIDNo()+"，起保日期不在主合同该被保险人有效保险期间内，无法上传！";
//						this.mErrors.addOneError(tError);
//						return false;
//					}
//					
//				}else{
//					CError tError = new CError();
//					tError.moduleName = "ContInsuredBL";
//					tError.functionName = "checkData";
//					tError.errorMessage = "被保人"+mLCInsuredSchema.getName()+"证件号"+mLCInsuredSchema.getIDNo()+"，在主合同保单中没有该被保人，无法上传！";
//					this.mErrors.addOneError(tError);
//
//					return false;
//				}
//			}
//		}		
		if (!this.checkLDPerson()) {
			return false;
		}
		if (!this.checkLCAddress()) {
			return false;
		}
		if (ContType.equals("2") && mOperate.equals("INSERT||CONTINSURED")) {
			if (mLDPersonSchema.getCustomerNo() != null
					&& !("").equals(mLDPersonSchema.getCustomerNo())) {
				LCInsuredDB tLCInsuredDB = null;
				if (this.isConCanUse)
					tLCInsuredDB = new LCInsuredDB(this.con);
				else
					tLCInsuredDB = new LCInsuredDB();
				tLCInsuredDB.setGrpContNo(mLCContSchema.getGrpContNo());
				tLCInsuredDB.setInsuredNo(mLDPersonSchema.getCustomerNo());
				int insuredCount = tLCInsuredDB.getCount();
				if (tLCInsuredDB.mErrors.needDealError()) {
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "checkData";
					tError.errorMessage = "查询团体合同下被保险人失败!";

					this.mErrors.addOneError(tError);
					return false;
				}
				if (insuredCount > 0) {
					
					String lcpolStr = "select count(1) "
								    +" from lcpol "
								    +" where grpcontno = '"+mLCContSchema.getGrpContNo()+"' "
								    +" and insuredno = '"+mLDPersonSchema.getCustomerNo()+"' "
								    +" and riskcode <> 'NAK02'";
					
					SSRS polSSRS = new SSRS();
					
					polSSRS = tExeSQL.execSQL(lcpolStr);

					if(polSSRS!=null&&polSSRS.getMaxRow()>0&&Integer.parseInt(polSSRS.GetText(1, 1))>0)
					{
						CError tError = new CError();
						tError.moduleName = "ContInsuredBL";
						tError.functionName = "checkData";
						tError.errorMessage = "该团体合同下已经存在该被保险人"+mLDPersonSchema.getName()+"，不能重复添加!";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
			}
		}
		// if (mOperate.equals("UPDATE||CONTINSURED")) {
		// LCPolDB tLCPolDB = new LCPolDB();
		// tLCPolDB.setContNo(mLCContSchema.getContNo());
		// tLCPolDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
		// int polCOunt = tLCPolDB.getCount();
		// if (tLCPolDB.mErrors.needDealError()) {
		// CError.buildErr(this, "查询被保险人险种保单时失败！");
		// return false;
		// }
		// if (polCOunt > 0) {
		// if (balance(mLDPersonSchema, mOLDLCInsuredDB)) {
		// CError.buildErr(this,
		// "该被保险人已经录入险种不能更新性别，生日，职业类别等关键信息,请先删除险种！");
		// }
		// return false;
		// }
		// }

		// 删除时要判断如果时主被保险人，要确认没有其他被保险人??放到录入完成时统一校验有没有主被保险人
		if (mOperate.equals("DELETE||CONTINSURED")) {
			if (("00").equals(mOLDLCInsuredDB.getRelationToMainInsured())) {
				LCInsuredDB tempLCInsuredDB = null;
				if (this.isConCanUse)
					tempLCInsuredDB = new LCInsuredDB(this.con);
				else
					tempLCInsuredDB = new LCInsuredDB();
				LCInsuredSet tempLCInsuredSet = new LCInsuredSet();
				tempLCInsuredDB.setContNo(mOLDLCInsuredDB.getContNo());
				tempLCInsuredSet = tempLCInsuredDB.query();
				if (tempLCInsuredDB.mErrors.needDealError()) {
					this.mErrors.copyAllErrors(mOLDLCInsuredDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "deleteData";
					tError.errorMessage = "获取合同下被保险人信息时失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				if (tempLCInsuredSet.size() > 1) {
					this.mErrors.copyAllErrors(mOLDLCInsuredDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "deleteData";
					tError.errorMessage = "请先删除其他被保险人再删除主被保险人!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}

		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean dealData() {
		String tNo;
		String tLimit;
		// 当是个人时，由界面上取得合同号，当是团体下个人的时候，需要在保存个人合同下第一个被保险人的时候，生成LCCont
		if (ContType.equals("2")) {
			if (mLCContSchema.getContNo() == null
					|| mLCContSchema.getContNo().equals("")) {
				mLCGrpContDB.setGrpContNo(mLCContSchema.getGrpContNo());
				if (!mLCGrpContDB.getInfo()) {
					this.mErrors.copyAllErrors(mLCGrpContDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "deleteData";
					tError.errorMessage = "获取团体合同数据时失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLCContSchema.setManageCom(mLCGrpContDB.getManageCom());
				mLCContSchema.setAgentCode(mLCGrpContDB.getAgentCode());
				mLCContSchema.setAgentGroup(mLCGrpContDB.getAgentGroup());
				mLCContSchema.setAgentCom(mLCGrpContDB.getAgentCom());
				mLCContSchema.setAgentType(mLCGrpContDB.getAgentType());
				mLCContSchema.setSaleChnl(mLCGrpContDB.getSaleChnl());
				mLCContSchema.setSignCom(mGlobalInput.ManageCom);
				mLCContSchema.setAppntNo(mLCGrpContDB.getAppntNo());
				mLCContSchema.setAppntName(mLCGrpContDB.getGrpName());
				// 设置销售取道
				mLCContSchema.setSaleChnlDetail(mLCGrpContDB
						.getSaleChnlDetail());
				// 设置申请日
				mLCContSchema.setPolApplyDate(mLCGrpContDB.getPolApplyDate());
				// 设置交费方式
				mLCContSchema.setPayMode(mLCGrpContDB.getPayMode());

				tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
				// 判定是团体保单下的个人单，还是普通个单
				if (ContType.equals("2")) {
					tNo = PubFun1.CreateMaxNo("GProposalContNo", tLimit);
				} else {
					tNo = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
				}
				// 合同上去掉处理机构
				// if (mLCInsuredSchema.getExecuteCom() == null ||
				// mLCInsuredSchema.getExecuteCom().equals("")) {
				// mLCContSchema.setExecuteCom(mGlobalInput.ManageCom);
				// }
				// else {
				// mLCContSchema.setExecuteCom(mLCInsuredSchema.getExecuteCom());
				// }

				mLCContSchema.setContNo(tNo);
				mLCContSchema.setProposalContNo(tNo);
				mLCContSchema.setContType(ContType);
				mLCContSchema.setBankCode(mLCContSchema.getBankCode());
				mLCContSchema.setBankAccNo(mLCContSchema.getBankAccNo());
				mLCContSchema.setAccName(mLCContSchema.getAccName());

				// mLCContSchema.setInsuredNo("0"); // 暂设
				mLCContSchema.setPolType(tPolTypeFlag);
				if (tInsuredPeoples == null || tInsuredPeoples.equals("")) {
					mLCContSchema.setPeoples(1);
				} else {
					mLCContSchema.setPeoples(tInsuredPeoples);
				}
				mLCContSchema.setCardFlag("0");
				String mSavePolType = (String) mTransferData
						.getValueByName("SavePolType");

				if (mLCContSchema.getAppFlag() == null
						|| mLCContSchema.getAppFlag().equals("")) {
					if (mSavePolType != null && !mSavePolType.equals("")
							&& !mSavePolType.equals("null")) {
						if (mSavePolType.trim().equals("")) {
							if (EdorType != null
									&& ("PB".equals(EdorType) || "PE"
											.equals(EdorType))) {
								mLCContSchema.setAppFlag("2");
							} else {
								mLCContSchema.setAppFlag("0");
							}
						} else {
							mLCContSchema.setAppFlag(mSavePolType);
						}
					} else {
						if (EdorType != null
								&& ("PB".equals(EdorType) || "PE"
										.equals(EdorType))) {
							mLCContSchema.setAppFlag("2");
						} else {
							mLCContSchema.setAppFlag("0");
						}
					}
				}

				mLCContSchema.setApproveFlag("0");
				mLCContSchema.setUWFlag("0");

				mLCContSchema.setOperator(mGlobalInput.Operator);
				mLCContSchema.setMakeDate(theCurrentDate);
				mLCContSchema.setMakeTime(theCurrentTime);
			}
		} else // 针对个人合同单处理被保人数
		{
			// System.out.println("人数是----***：" + mLCContSchema.getPeoples());
			if (mLCContSchema.getPeoples() == 0) {
				mLCContSchema.setPeoples(1);
			} else {
				mLCContSchema.setPeoples(mLCContSchema.getPeoples() + 1);
			}
		}
		tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
		// 判断是否需要新建客户
		if (mLDPersonSchema.getCustomerNo() == null
				|| ("").equals(mLDPersonSchema.getCustomerNo())) {
			tNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
			mLDPersonSchema.setCustomerNo(tNo);
			if (mLCContSchema.getPolType().equals("0")) {
				// 个人客户
				mLDPersonSchema.setMakeDate(theCurrentDate);
				mLDPersonSchema.setMakeTime(theCurrentTime);
				mLDPersonSchema.setModifyDate(theCurrentDate);
				mLDPersonSchema.setModifyTime(theCurrentTime);
				mLDPersonSchema.setOperator(mGlobalInput.Operator);
				tLDPersonSchema = new LDPersonSchema();
				tLDPersonSchema.setSchema(mLDPersonSchema); // tLDPersonSet包含需要新建的客户

				if (!"".equals(mLCInsuredSchema.getContPlanCode())) {
					tLCContPlanDB1.setGrpContNo(mLCContSchema.getGrpContNo());
					tLCContPlanDB1.setContPlanCode(mLCInsuredSchema
							.getContPlanCode());
					tLCContPlanSet1 = tLCContPlanDB1.query();
					if (tLCContPlanSet1 != null && tLCContPlanSet1.size() > 0) {
						tLCContPlanSchema1 = tLCContPlanSet1.get(1);
						// tLCContPlanSchema1.setPeoples3(tLCContPlanSchema1.getPeoples3()+1);
						// map.put(tLCContPlanSchema1, "UPDATE");
						
					}

				}

			} else {
				if (mLCContSchema.getPolType().equals("2")) {
					mLCInsuredSchema.setName("公共账户");
				} else if ((mLCContSchema.getPolType().equals("5"))) {
					mLCInsuredSchema.setName("公共保额");
				} else {
					mLCInsuredSchema.setName("无名单");
				}

				if (mLCInsuredSchema.getSex() == null
						|| mLCInsuredSchema.getSex().equals("")) {
					mLCInsuredSchema.setSex("0");
				}
				if (mLCInsuredSchema.getBirthday() == null
						|| mLCInsuredSchema.getBirthday().equals("")) { // 如果是无名单，没有录入年龄，默认是30
					int tInsuredAge = 0;
					if (tInsuredAppAge == null || tInsuredAppAge.equals("")) { // 如果投保年龄没有录入
						tInsuredAge = 30;

					} else {
						tInsuredAge = Integer.parseInt(tInsuredAppAge); // 前台录入年龄
					}

					// 年龄所在年=系统年-年龄
					String year = Integer.toString(Integer.parseInt(StrTool
							.getYear())
							- tInsuredAge);
					String brithday = StrTool.getDate(year, StrTool.getMonth(),
							StrTool.getDay());
					brithday = StrTool.replace(brithday, "/", "-");
					mLCInsuredSchema.setBirthday(brithday);
				}
			}
		}
		if (mLCContSchema.getPolType().equals("2")) {
			mLCInsuredSchema.setOccupationCode(""); 
		}
		String tInsuredNo = mLCInsuredSchema.getContNo();

		if (mLCContSchema.getPolType().equals("0")) {
			// 把主被保险人的ID 和InsuredNo放入hashMap中
			this.otmInsuredNo.put(mLCInsuredSchema.getContNo(), mLDPersonSchema
					.getCustomerNo());//			
	
//			this.otmInsuredNoOrange.put(mLCInsuredSchema.getContNo(),
//					mLCInsuredSchema.getOrganComCode());
//			if (mLCInsuredSchema.getInsuredID() != null
//					&& !("".equals(mLCInsuredSchema.getInsuredID()))) {
//				this.otmInsuredId.put(mLCInsuredSchema.getInsuredID(),
//						mLDPersonSchema.getCustomerNo());
//				this.otmInsuredIdCvalidate.put(mLCInsuredSchema.getInsuredID(),
//						mLCInsuredSchema.getIValiDate());
//			}

			mLCInsuredSchema.setInsuredNo(mLDPersonSchema.getCustomerNo());
		} else { // 虚拟客户的单独编码规则,虚拟客户不存入ldperson
			tNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
			if (mLCInsuredSchema == null
					|| "".equals(mLCInsuredSchema.getInsuredNo())) {
				otmInsuredNo.put(mLCInsuredSchema.getContNo(), tNo); //
				mLCInsuredSchema.setInsuredNo(tNo);
			} else {
				otmInsuredNo.put(mLCInsuredSchema.getContNo(), mLCInsuredSchema
						.getInsuredNo()); //
				mLCInsuredSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());
			}
		}
		// chenwm20071015 将后面的设置grpcontno提前. 因为在导人的时候,如果录入的主被保险人的长度大于8,是从数据库中找的,
		// 如果没有grpcontno,则在下面的验证查询中有问题,即不在本保单下,其它保单有也可能会通过验证.
		mLCInsuredSchema.setGrpContNo(mLCContSchema.getGrpContNo());

		// 查询是否有主被保险人的InsuredNo,如果有被保险人,但是没有找到主被保险人的InsuredNo,则报错 2007.3.26
			// 获取主被保险人的InsuredNo
			// 这样判断是为了避免在界面上录入和重磁盘导入相冲突,因在界面上的MainInsuredNo是数据库中有的,
			// 而磁盘导入的时候是生成的

			ExeSQL exesql = new ExeSQL();

	

		// 如果职业类别为空，则直接根据职业代码解析职业类别
		if (mLCInsuredSchema.getOccupationType() == null
				|| mLCInsuredSchema.getOccupationType().equals("")) {
			if ((mLCInsuredSchema.getOccupationCode() != null)
					&& (!mLCInsuredSchema.getOccupationCode().equals(""))) {
				LDOccupationDB tLDOccupationDB = null;
				if (!this.isConCanUse)
					tLDOccupationDB = new LDOccupationDB();
				else
					tLDOccupationDB = new LDOccupationDB(this.con);
				tLDOccupationDB.setOccupationCode(mLCInsuredSchema
						.getOccupationCode());
				if (tLDOccupationDB.getInfo()) {
					mLCInsuredSchema.setOccupationType(tLDOccupationDB
							.getOccupationType());
				} else {
					System.out.println("没有查询到相应的职业类别");
				}
			}
		}

		// System.out.println("mLCInsuredSchema.getInsuredNo:" +
		// mLCInsuredSchema.getInsuredNo());

		// 产生客户地址
		if (mLCAddressSchema != null
				&& ((mLCAddressSchema.getPostalAddress() != null && !mLCAddressSchema
						.getPostalAddress().equals(""))
						|| (mLCAddressSchema.getZipCode() != null && !mLCAddressSchema
								.getZipCode().equals(""))
						|| (mLCAddressSchema.getPhone() != null && !mLCAddressSchema
								.getPhone().equals(""))
						|| (mLCAddressSchema.getMobile() != null && !mLCAddressSchema
								.getMobile().equals(""))
						|| (mLCAddressSchema.getEMail() != null && !mLCAddressSchema
								.getEMail().equals("")) || (mLCAddressSchema
						.getGrpName() != null && !mLCAddressSchema.getGrpName()
						.equals("")))) {

			try {
				// tSSRS = new SSRS();
				if ((StrTool.compareString(mLCAddressSchema.getAddressNo(), ""))) {
					StringBuffer tSBql = new StringBuffer(256);
					tSBql
							.append("Select Case When to_char(max(to_number(AddressNo))) Is Null Then '0' Else to_char(max(to_number(AddressNo))) End from LCAddress where CustomerNo='");
					tSBql.append(mLDPersonSchema.getCustomerNo());
					tSBql.append("'");
					// ExeSQL tExeSQL = new ExeSQL();
					tSSRS = tExeSQL.execSQL(tSBql.toString());

					Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
					int ttNo = firstinteger.intValue() + 1;
					Integer integer = new Integer(ttNo);
					tNo = integer.toString();
					// System.out.println("得到的地址码是：" + tNo);
					mLCAddressSchema.setAddressNo(tNo);
				}

			} catch (Exception e) {
				CError tError = new CError();
				tError.moduleName = "ContIsuredBL";
				tError.functionName = "createAddressNo";
				tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
				this.mErrors.addOneError(tError);
				mLCAddressSchema.setAddressNo("");
			}
			mLCAddressSchema.setCustomerNo(mLDPersonSchema.getCustomerNo());
			mLCAddressSchema.setMakeDate(theCurrentDate);
			mLCAddressSchema.setMakeTime(theCurrentTime);
			mLCAddressSchema.setModifyDate(theCurrentDate);
			mLCAddressSchema.setModifyTime(theCurrentTime);
			mLCAddressSchema.setOperator(mGlobalInput.Operator);
			tLCAddressSchema = new LCAddressSchema();
			tLCAddressSchema.setSchema(mLCAddressSchema); // tLCAddressSchema包含需要新建的客户地址

		}

		// 产生客户账户
		// if (mLCAccountSchema != null) {
		// if (mLCAccountSchema.getBankCode() != null &&
		// mLCAccountSchema.getBankAccNo() != null) {
		// mLCAccountSchema.setCustomerNo(mLDPersonSchema.getCustomerNo());
		// if (mLCAccountSchema.getAccName() == null &&
		// ("").equals(mLCAccountSchema.getAccName()))
		// mLCAccountSchema.setAccName(mLDPersonSchema.getName());
		// mLCAccountSchema.setMakeDate(theCurrentDate);
		// mLCAccountSchema.setMakeTime(theCurrentTime);
		// mLCAccountSchema.setModifyDate(theCurrentDate);
		// mLCAccountSchema.setModifyTime(theCurrentTime);
		// mLCAccountSchema.setOperator(mGlobalInput.Operator);
		//
		// map.put(mLCAccountSchema, "INSERT");
		// }
		// }

		// Start TD280 gaoliang 2008-09-23
		// 注销原因:只要在无扫描录入和新单复核时以"添加被保人"方式添加连带被保人后，承保处理-->团体保单-->人工核保,个人核保信息就不显示姓名
		// // 如果是主被保险人，设置合同表相关信息
		// if (("00").equals(mLCInsuredSchema.getRelationToMainInsured())
		// || mLCContSchema.getAppntNo().equalsIgnoreCase(
		// mLCInsuredSchema.getInsuredNo())) {
		// End TD280 gaoliang 2008-09-23
		// 注销原因:只要在无扫描录入和新单复核时以"添加被保人"方式添加连带被保人后，承保处理-->团体保单-->人工核保,个人核保信息就不显示姓名
		mLCInsuredSchema.setIDNo(mLCInsuredSchema.getIDNo().toUpperCase()); //证件号全部变更为大写
		mLCContSchema.setInsuredBirthday(mLCInsuredSchema.getBirthday());
		mLCContSchema.setInsuredIDNo(mLCInsuredSchema.getIDNo());
		mLCContSchema.setInsuredIDType(mLCInsuredSchema.getIDType());
		mLCContSchema.setInsuredName(mLCInsuredSchema.getName());
		mLCContSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());
		mLCContSchema.setInsuredSex(mLCInsuredSchema.getSex());
		// 如果是家庭单，在录入主被保险人时产生家庭保障号（在前台控制必须首先录入主被保险人）
		if (("1").equals(mLCContSchema.getFamilyType())) { // 家庭单
			if (mLCContSchema.getFamilyID() == null
					|| mLCContSchema.getFamilyID().equals("")) {
				tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
				tNo = PubFun1.CreateMaxNo("FamilyID", 10);
				// mLCInsuredSchema.setFamilyID(tNo);
				mLCContSchema.setFamilyID(tNo);
			}
		}
		// Start TD280 gaoliang 2008-09-23
		// 注销原因:只要在无扫描录入和新单复核时以"添加被保人"方式添加连带被保人后，承保处理-->团体保单-->人工核保,个人核保信息就不显示姓名
		// }else {
		// mLCContSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());
		// //添加公共保额到insuredname字段
		// if("5".equals(mLCContSchema.getPolType())){
		// mLCContSchema.setInsuredName(mLCInsuredSchema.getName());
		// }
		// }
		// End TD280 gaoliang 2008-09-23
		// 注销原因:只要在无扫描录入和新单复核时以"添加被保人"方式添加连带被保人后，承保处理-->团体保单-->人工核保,个人核保信息就不显示姓名

		String row = mLCInsuredSchema.getPrtNo(); // 得到行号
		mLCInsuredSchema.setAddressNo(mLCAddressSchema.getAddressNo());
		mLCInsuredSchema.setContNo(mLCContSchema.getContNo());
		mLCInsuredSchema.setPrtNo(mLCContSchema.getPrtNo());
		mLCInsuredSchema.setAppntNo(mLCContSchema.getAppntNo());
		mLCInsuredSchema.setGrpContNo(mLCContSchema.getGrpContNo());

		mLCInsuredSchema.setManageCom(mLCContSchema.getManageCom()); // 管理理机构
		if (mLCInsuredSchema.getExecuteCom() == null
				|| mLCInsuredSchema.getExecuteCom().equals("")) {
			mLCInsuredSchema.setExecuteCom(mLCInsuredSchema.getManageCom()); // 处理机构
		}

		mLCInsuredSchema.setSequenceNo(tSequenceNo);
		mLCInsuredSchema.setFamilyID(mLCContSchema.getFamilyID());
		mLCInsuredSchema.setModifyDate(theCurrentDate);
		mLCInsuredSchema.setModifyTime(theCurrentTime);
		mLCInsuredSchema.setOperator(mGlobalInput.Operator);

		// 处理告知信息
		if (mLCCustomerImpartSet != null && mLCCustomerImpartSet.size() > 0) {
			// 设置所有告知信息得客户号码
			for (int i = 1; i <= mLCCustomerImpartSet.size(); i++) {
				mLCCustomerImpartSet.get(i)
						.setContNo(mLCContSchema.getContNo());
				mLCCustomerImpartSet.get(i).setGrpContNo(
						mLCContSchema.getGrpContNo());
				mLCCustomerImpartSet.get(i).setPrtNo(mLCContSchema.getPrtNo());
				mLCCustomerImpartSet.get(i).setProposalContNo(
						mLCContSchema.getProposalContNo());
				if (mLCCustomerImpartSet.get(i).getCustomerNoType().equals("I")) {
					mLCCustomerImpartSet.get(i).setCustomerNo(
							mLCInsuredSchema.getInsuredNo());
				}
			}
			CustomerImpartBL mCustomerImpartBL = new CustomerImpartBL();
			VData tempVData = new VData();
			tempVData.add(mLCCustomerImpartSet);
			tempVData.add(mGlobalInput);
			mCustomerImpartBL.submitData(tempVData, "IMPART||DEAL");
			if (mCustomerImpartBL.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "dealData";
				tError.errorMessage = mCustomerImpartBL.mErrors.getFirstError()
						.toString();
				this.mErrors.addOneError(tError);
				return false;
			}
			tempVData.clear();
			tempVData = mCustomerImpartBL.getResult();
			if (null != (LCCustomerImpartSet) tempVData.getObjectByObjectName(
					"LCCustomerImpartSet", 0)) {
				mLCCustomerImpartSet = (LCCustomerImpartSet) tempVData
						.getObjectByObjectName("LCCustomerImpartSet", 0);
				// System.out.println("告知条数" + mLCCustomerImpartSet.size());
			} else {
				// System.out.println("告知条数为空");
			}
			if (null != (LCCustomerImpartParamsSet) tempVData
					.getObjectByObjectName("LCCustomerImpartParamsSet", 0)) {
				mLCCustomerImpartParamsSet = (LCCustomerImpartParamsSet) tempVData
						.getObjectByObjectName("LCCustomerImpartParamsSet", 0);
			}
		}
		// 处理告知明细信息
		if (mLCCustomerImpartDetailSet != null
				&& mLCCustomerImpartDetailSet.size() > 0) {
			// 设置所有告知明细信息得客户号码
			for (int i = 1; i <= mLCCustomerImpartDetailSet.size(); i++) {
				StringBuffer tSBql = new StringBuffer(256);
				tSBql
						.append("Select Case When max(SubSerialNo) Is Null Then '0' Else max(SubSerialNo) End from LCCustomerImpartDetail where GrpContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and ProposalContNo='");
				tSBql.append(mLCContSchema.getProposalContNo());
				tSBql.append("' and ImpartVer='");
				tSBql.append(mLCCustomerImpartDetailSet.get(i).getImpartVer());
				tSBql.append("' and  ImpartCode='");
				tSBql.append(mLCCustomerImpartDetailSet.get(i).getImpartCode());
				tSBql.append("' and CustomerNo='");
				tSBql.append(mLCInsuredSchema.getInsuredNo());
				tSBql.append("' and CustomerNoType='1'");
				// ExeSQL tExeSQL = new ExeSQL();
				tSSRS = tExeSQL.execSQL(tSBql.toString());

				Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
				int ttNo = firstinteger.intValue() + 1;
				Integer integer = new Integer(ttNo);
				tNo = integer.toString();
				// System.out.println("得到的告知码是：" + tNo);

				mLCCustomerImpartDetailSet.get(i).setSubSerialNo(tNo);
				mLCCustomerImpartDetailSet.get(i).setContNo(
						mLCContSchema.getContNo());
				mLCCustomerImpartDetailSet.get(i).setGrpContNo(
						mLCContSchema.getGrpContNo());
				mLCCustomerImpartDetailSet.get(i).setPrtNo(
						mLCContSchema.getPrtNo());
				mLCCustomerImpartDetailSet.get(i).setProposalContNo(
						mLCContSchema.getProposalContNo());
				mLCCustomerImpartDetailSet.get(i).setCustomerNo(
						mLCInsuredSchema.getInsuredNo());
				mLCCustomerImpartDetailSet.get(i).setOperator(
						mGlobalInput.Operator);
				mLCCustomerImpartDetailSet.get(i).setMakeDate(theCurrentDate);
				mLCCustomerImpartDetailSet.get(i).setMakeTime(theCurrentTime);
				mLCCustomerImpartDetailSet.get(i).setModifyDate(theCurrentDate);
				mLCCustomerImpartDetailSet.get(i).setModifyTime(theCurrentTime);
			}
		}

		if (EdorType != null
				&& (("NI".equals(EdorType))
						|| ("PE".equals(EdorType) && "2".equals(mLCContSchema
								.getPolType())) || ("PB".equals(EdorType) && "5"
						.equals(mLCContSchema.getPolType())))) {
			if (!BQdealdata()) {
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "dealdata";
				tError.errorMessage = "调用保全的函数发生错误!";
				this.mErrors.addOneError(tError);

				return false;
			}
		}

		if (mOperate.equals("INSERT||CONTINSURED")) {
			insertData();
		}
		if (mOperate.equals("UPDATE||CONTINSURED")) {
			updateData();
		}
		// 设置初审时的交费间隔
		if (FirstTrialFlag) {
			map.put("update LCCont set PayIntv='' where contno='"
					+ mLCContSchema.getContNo() + "'", "UPDATE");
		}
		return true;
	}

	/**
	 * 处理保存逻辑，保存处理过的数据 如果在处理过程中出错，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean insertData() {
		// 团单时需要更新团体合同表
		mLCContSchema.setModifyDate(theCurrentDate);
		mLCContSchema.setModifyTime(theCurrentTime);
		mLCInsuredSchema.setMakeDate(theCurrentDate);
		mLCInsuredSchema.setMakeTime(theCurrentTime);
		/*
		 * //关联表数据的添加 add by tuqiang ES_DOC_MAINDB tES_DOC_MAINDB = new
		 * ES_DOC_MAINDB(); tES_DOC_MAINDB.setDocCode(mLCContSchema.getPrtNo());
		 * ES_DOC_MAINSet tES_DOC_MAINDBSet = tES_DOC_MAINDB.query();
		 * if(tES_DOC_MAINDBSet!=null&&tES_DOC_MAINDBSet.size()!=0) { for(int
		 * i=0;i<tES_DOC_MAINDBSet.size();i++) {
		 * tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINDBSet.get(i+1).getDocID());
		 * tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINDBSet.get(i+1).getBussType());
		 * tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINDBSet.get(i+1).getSubType());
		 * tES_DOC_RELATIONSchema.setBussNo(mLCInsuredSchema.getInsuredNo());
		 * tES_DOC_RELATIONSchema.setBussNoType("41");
		 * tES_DOC_RELATIONSchema.setRelaFlag("0");
		 * map.put(tES_DOC_RELATIONSchema, "INSERT"); } }else {
		 * System.out.println("写关联表出错！！"); }
		 */
		// map.put(mLCContSchema, "DELETE&INSERT");
		map.put(mLCInsuredSchema, "INSERT");
		if (tLDPersonSchema != null) {
			// 添加被保险人的密码字段,为身份证后六位的加密.如果最后一位为x,转化为X,在加密.如果是不身份证号码取号码的后六位加密.
			// 如果字段长度少于6.取整个字段加密为密码 2007.4.27 guoxq
			int tLength = tLDPersonSchema.getIDNo().length();
			String tPassword = "";
			// 首先判断长度是否大于或等于6
			if (tLength >= 6) {
				// 判断是身份证且最后一位为x
				if (tLDPersonSchema.getIDType().equals("0")
						&& tLDPersonSchema.getIDNo().substring(tLength - 1)
								.equals("x")) {
					tPassword = tLDPersonSchema.getIDNo()
							.substring(tLength - 6);
					tPassword = tPassword.toUpperCase();
				} else {
					tPassword = tLDPersonSchema.getIDNo()
							.substring(tLength - 6);
				}
			} else {
				tPassword = tLDPersonSchema.getIDNo();
			}
			// 进行加密
			LisIDEA tIdea = new LisIDEA();
			String Password = tIdea.encryptString(tPassword);

			tLDPersonSchema.setPassword(Password);
			map.put(tLDPersonSchema, "INSERT");

		}
		map.put(mLCContSchema, "DELETE&INSERT");
		if (tLCAddressSchema != null) {
			StringBuffer tSBql = new StringBuffer(256);
			tSBql
					.append("select varchar(max(int(AddressNo))) from LCAddress where CustomerNo='");
			tSBql.append(mLDPersonSchema.getCustomerNo());
			tSBql.append("'");
			// ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(tSBql.toString());
			if (!tSSRS.GetText(1, 1).equals("")) {
				int a = Integer.parseInt(tSSRS.GetText(1, 1));
				if (a != Integer.parseInt(mLCAddressSchema.getAddressNo())) {
					map.put(tLCAddressSchema, "INSERT");
				}
			} else {
				map.put(tLCAddressSchema, "INSERT");
			}
		}
		if (mLCCustomerImpartParamsSet != null) {
			map.put(mLCCustomerImpartParamsSet, "INSERT");
		}
		if (mLCCustomerImpartSet != null) {
			map.put(mLCCustomerImpartSet, "INSERT");
		}
		if (mLCCustomerImpartDetailSet != null) {
			map.put(mLCCustomerImpartDetailSet, "INSERT");
		}
		if ((ContType.equals("2") && !"2".equals(mLCContSchema.getPolType()))
				&& !"5".equals(mLCContSchema.getPolType())) // 非公共帐户虚拟人才累计人数
		// Alex
		// 20050924
		{
			mLCGrpContDB.setGrpContNo(mLCContSchema.getGrpContNo());
			mLCGrpContDB.getInfo();
			if (mOperate.equals("INSERT||CONTINSURED")) {
				mLCGrpContDB.setPeoples2(mLCGrpContDB.getPeoples2()
						+ mLCContSchema.getPeoples());
			}
			mLCGrpContDB.setModifyDate(theCurrentDate);
			mLCGrpContDB.setModifyTime(theCurrentTime);
			map.put(mLCGrpContDB.getSchema(), "UPDATE");
		}

		if (ContType.equals("1")) {
			map.put(mLCContSchema, "UPDATE");
		}

		return true;
	}

	/**
	 * 更新逻辑，更新处理过的数据 如果在处理过程中出错，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean updateData() {
		LCPolSet tLCPolSet = new LCPolSet();
		LCPolDB tLCPolDB = null;
		if (this.isConCanUse)
			tLCPolDB = new LCPolDB(this.con);
		else
			tLCPolDB = new LCPolDB();
		tLCPolDB.setContNo(mLCContSchema.getContNo());
		tLCPolDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
		tLCPolSet = tLCPolDB.query();
		if (tLCPolDB.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询该客户的险种保单失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (tLCPolSet.size() > 0
				&& !StrTool.compareString(mOLDLCInsuredDB.getContPlanCode(),
						mLCInsuredSchema.getContPlanCode())) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "dealData";
			tError.errorMessage = "该被保险人下还有险种保单，不能更新保险计划！";
			this.mErrors.addOneError(tError);
			return false;
		}
		deleteData();
		insertData();

		// 当客户号码，姓名发生变更时，要更新险种表，领取项表
		if (!mOLDLCInsuredDB.getName().equals(mLCInsuredSchema.getName())
				|| !mOLDLCInsuredDB.getInsuredNo().equals(
						mLCInsuredSchema.getInsuredNo())) {
			LCGetSet tLCGetSet = new LCGetSet();
			LCGetDB tLCGetDB = null;
			if (this.isConCanUse)
				tLCGetDB = new LCGetDB(this.con);
			else
				tLCGetDB = new LCGetDB();

			tLCGetDB.setContNo(mLCContSchema.getContNo());
			tLCGetDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
			tLCGetSet = tLCGetDB.query();
			if (tLCGetDB.mErrors.needDealError()) {
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "dealData";
				tError.errorMessage = "查询该客户的领取项失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			for (int i = 1; i <= tLCPolSet.size(); i++) {
				tLCPolSet.get(i).setInsuredNo(mLCInsuredSchema.getInsuredNo());
				tLCPolSet.get(i).setInsuredName(mLCInsuredSchema.getName());
			}
			for (int i = 1; i <= tLCGetSet.size(); i++) {
				tLCGetSet.get(i).setInsuredNo(mLCInsuredSchema.getInsuredNo());
			}
			// 更新查出来的保单集合，为重算作准备
			for (int i = 1; i <= preLCPolSet.size(); i++) {
				preLCPolSet.get(i)
						.setInsuredNo(mLCInsuredSchema.getInsuredNo());
				preLCPolSet.get(i).setInsuredName(mLCInsuredSchema.getName());
			}
			if (tLCPolSet.size() > 0) {
				map.put(tLCPolSet, "UPDATE");
			}
			if (tLCGetSet.size() > 0) {
				map.put(tLCGetSet, "UPDATE");
			}

		}
		if (mLDPersonSchema != null && mLDPersonSchema.getCustomerNo() != null
				&& !"".equals(mLDPersonSchema.getCustomerNo())
				&& "0".equals(mLCContSchema.getPolType())) {
			LDPersonDB tLDPersonDB = new LDPersonDB();
			LDPersonSet tLDPersonSet = new LDPersonSet();
			tLDPersonDB.setCustomerNo(mLDPersonSchema.getCustomerNo());
			tLDPersonSet = tLDPersonDB.query();
			if (tLDPersonSet != null && tLDPersonSet.size() > 0) {
				LDPersonSchema ttLDPersonSchema = new LDPersonSchema();
				ttLDPersonSchema = tLDPersonSet.get(1);
				ttLDPersonSchema.setEnglishName(mLDPersonSchema
						.getEnglishName());
				if(mLDPersonSchema.getNativePlace() != null && !"".equals(mLDPersonSchema.getNativePlace()))
				{  //新国籍不等于空和不等于空字符串时，才更新ldperson
					ttLDPersonSchema.setNativePlace(mLDPersonSchema.getNativePlace()); // guoxh TASK000033-上传国籍处理
				}				
				map.put(ttLDPersonSchema, "DELETE&INSERT");
			} else {
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "updateData";
				tError.errorMessage = "查询客户失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		// 修改主被保人的noticeway同时修改其连带被保人的  guoxh 

		return true;
	}

	/**
	 * 删除处理，删除处理后的数据，根据操作类型的不同采取不同的处理 如果在处理过程中出错，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean deleteData() {
		// LCGrpContDB mLCGrpContDB = new LCGrpContDB();

		String updateContFlag = "INSERT";
		boolean updateGrpContFlag = false;
		// 对于删除的情况，首先需要的是备份数据
		if (mOperate.equals("DELETE||CONTINSURED")) {
			StringBuffer tSBql = new StringBuffer(256);
			System.out.println(mLCContSchema.getContNo());

			// 用于批量删除被保险人下 的保单责任之间其他控制实例. 中意特殊新增加的2007.4.26 guoxq
			// tSBql.append("delete from lcdutyotherctrl where polno in(select
			// polno from lcpol where grpcontno ='");
		

			// 用于批量删除被保险人下的 保单责任其他控制责任关联表. 中意特殊新增加的2007.4.26 guoxq
		

			// 为了解决多次__新增__删除被保险人.在备份之前执行删除命令,保证数据库当中紧紧存入一遍,避免数据插入失败
			tSBql = new StringBuffer(256);
			tSBql.append("delete from LOBInsured where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' and InsuredNo = '");
			tSBql.append(mOLDLCInsuredDB.getInsuredNo());
			tSBql.append("' ");
			map.put(tSBql.toString(), "DELETE");

			tSBql = new StringBuffer(256);
			tSBql.append("delete from lccont where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' ");
			map.put(tSBql.toString(), "DELETE");

			// 开始进行__备份插入数据得操作.
			tSBql = new StringBuffer(256);
			tSBql
					.append("insert into LOBInsured (select * from LCInsured where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' and InsuredNo = '");
			tSBql.append(mOLDLCInsuredDB.getInsuredNo());
			tSBql.append("')");
			// map.put(
			// "insert into LOBInsured (select * from LCInsured where ContNo='"
			// + mLCContSchema.getContNo() + "' and InsuredNo = '"
			// + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
			map.put(tSBql.toString(), "INSERT");

			// 为了解决多次__新增__删除告知参数表.在备份之前执行删除命令,保证数据库当中紧紧存入一遍,避免数据插入失败
			tSBql = new StringBuffer(256);
			tSBql.append("delete from LOBCustomerImpartParams where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' and CustomerNo = '");
			tSBql.append(mOLDLCInsuredDB.getInsuredNo());
			tSBql.append("' ");
			map.put(tSBql.toString(), "DELETE");

			// 备份告知参数表和告知表
			tSBql = new StringBuffer(256);
			tSBql
					.append("insert into LOBCustomerImpartParams (select * from LCCustomerImpartParams where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' and CustomerNo = '");
			tSBql.append(mOLDLCInsuredDB.getInsuredNo());
			tSBql.append("')");
			// map.put(
			// "insert into LOBCustomerImpartParams (select *from
			// LCCustomerImpartParams where ContNo='"
			// + mLCContSchema.getContNo() + "' and CustomerNo = '"
			// + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
			map.put(tSBql.toString(), "INSERT");

			// 为了解决多次__新增__删除告知参数表.在备份之前执行删除命令,保证数据库当中紧紧存入一遍,避免数据插入失败
			tSBql = new StringBuffer(256);
			tSBql.append("delete from LOBCustomerImpart where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' and CustomerNo = '");
			tSBql.append(mOLDLCInsuredDB.getInsuredNo());
			tSBql.append("' ");
			map.put(tSBql.toString(), "DELETE");

			tSBql = new StringBuffer(256);
			tSBql
					.append("insert into LOBCustomerImpart (select * from LCCustomerImpart where ContNo='");
			tSBql.append(mLCContSchema.getContNo());
			tSBql.append("' and CustomerNo = '");
			tSBql.append(mOLDLCInsuredDB.getInsuredNo());
			tSBql.append("')");
			// map.put("insert into LOBCustomerImpart (select *from
			// LCCustomerImpart where ContNo='"
			// + mLCContSchema.getContNo() + "' and CustomerNo = '"
			// + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
			map.put(tSBql.toString(), "INSERT");

			// 为了解决__初审录入过程当中, 如果删除被保人, 那么把数据库当中相对应的__初审险种信息表__删除
		

		}

		map.put(mOLDLCInsuredDB.getSchema(), "DELETE");

		// 删除告知参数表_____针对被保人的.
		StringBuffer tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpartParams where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mOLDLCInsuredDB.getInsuredNo());
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		// 删除告知参数表____针对投保人的
		tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpartParams where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mOLDLCInsuredDB.getAppntNo());
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		// 删除代理人报告书
		tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpartParams where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mTransferData.getValueByName("AgentCode"));
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		// 删除告知表____针对被保人的
		tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpart where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mOLDLCInsuredDB.getInsuredNo());
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		// 删除告知表___针对投保人的
		tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpart where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mOLDLCInsuredDB.getAppntNo());
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		// 删除告知表__代理人报告书告知表
		tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpart where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mTransferData.getValueByName("AgentCode"));
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		// 删除告知明细表
		tSBql = new StringBuffer(256);
		tSBql.append("delete from LCCustomerImpartDetail where ContNo='");
		tSBql.append(mLCContSchema.getContNo());
		tSBql.append("' and CustomerNo = '");
		tSBql.append(mOLDLCInsuredDB.getInsuredNo());
		tSBql.append("'");
		map.put(tSBql.toString(), "DELETE");

		LCPolDB tLCPolDB = null;
		if (this.isConCanUse)
			tLCPolDB = new LCPolDB(this.con);
		else
			tLCPolDB = new LCPolDB();

		tLCPolDB.setContNo(mLCContSchema.getContNo());
		tLCPolDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
		preLCPolSet = tLCPolDB.query();
		if (tLCPolDB.mErrors.needDealError()) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "查询该客户下险种保单时失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		StringBuffer updateLCGrpContStr = new StringBuffer();
		// 如果删除被保险人时，需要删除被保险人下所有险种，在更新时不需要
		if (mOperate.equals("DELETE||CONTINSURED")) {
			if (preLCPolSet.size() > 0) {
				LCDelPolLogSet mLCDelPolLogSet = new LCDelPolLogSet();
				// 备份险种数据
				tSBql = new StringBuffer(256);
				tSBql
						.append("insert into LOBDuty (select * from LCDuty a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and b.InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("'))");
				// map.put("insert into LOBDuty (select * from LCDuty a where
				// a.PolNo in (select b.polno from lcpol b where b.ContNo='" +
				// mLCContSchema.getContNo() + "' and b.InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo()
				// + "'))", "INSERT");
				map.put(tSBql.toString(), "INSERT");

				tSBql = new StringBuffer(256);
				tSBql
						.append("select * from LCDuty a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and b.InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("')");
				LCDutyDB tLCDutyDB = null;
				if (this.isConCanUse)
					tLCDutyDB = new LCDutyDB(this.con);
				else
					tLCDutyDB = new LCDutyDB();
				LCDutySet tLCDutySet = new LCDutySet();
				// tLCDutySet = tLCDutyDB.executeQuery(
				// "select * from LCDuty a where a.PolNo in (select b.polno from
				// lcpol b where b.ContNo='"
				// + mLCContSchema.getContNo() + "' and b.InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo() + "')");
				tLCDutySet = tLCDutyDB.executeQuery(tSBql.toString());
				if (tLCDutyDB.mErrors.needDealError()) {
					CError.buildErr(this, "查询该被保险人责任信息时失败；");
				}
				if (tLCDutySet.size() > 0) {
					map.put(tLCDutySet, "DELETE"); // 被保人下的责任删除
				}

				tSBql = new StringBuffer(256);
				tSBql
						.append("insert into LOBPrem (select * from LCPrem a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and b.InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("'))");
				// map.put("insert into LOBPrem (select * from LCPrem a where
				// a.PolNo in (select b.polno from lcpol b where b.ContNo='" +
				// mLCContSchema.getContNo() + "' and b.InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo()
				// + "'))", "INSERT");
				map.put(tSBql.toString(), "INSERT");

				tSBql = new StringBuffer(256);
				tSBql
						.append("select * from LCPrem a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and b.InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("')");
				LCPremDB tLCPremDB = null;
				if (this.isConCanUse)
					tLCPremDB = new LCPremDB(this.con);
				else
					tLCPremDB = new LCPremDB();
				LCPremSet tLCPremSet = new LCPremSet();
				// tLCPremSet = tLCPremDB.executeQuery(
				// "select * from LCPrem a where a.PolNo in (select b.polno from
				// lcpol b where b.ContNo='"
				// + mLCContSchema.getContNo() + "' and b.InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo()
				// + "')");
				tLCPremSet = tLCPremDB.executeQuery(tSBql.toString());
				if (tLCPremDB.mErrors.needDealError()) {
					CError.buildErr(this, "查询该被保险人保费项信息时失败；");
				}
				if (tLCPremSet.size() > 0) {
					map.put(tLCPremSet, "DELETE"); // 被保人下的险种删除
				}

				tSBql = new StringBuffer(256);
				tSBql
						.append("insert into LOBGet (select * from LCGet a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and b.InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("'))");
				// map.put("insert into LOBGet (select * from LCGet a where
				// a.PolNo in (select b.polno from lcpol b where b.ContNo='" +
				// mLCContSchema.getContNo() + "' and b.InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo()
				// + "'))", "INSERT");
				map.put(tSBql.toString(), "INSERT");

				tSBql = new StringBuffer(256);
				tSBql
						.append("select * from LCGet a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and b.InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("')");
				LCGetDB tLCGetDB = null;
				if (this.isConCanUse)
					tLCGetDB = new LCGetDB(this.con);
				else
					tLCGetDB = new LCGetDB();
				LCGetSet tLCGetSet = new LCGetSet();
				// tLCGetSet = tLCGetDB.executeQuery(
				// "select * from LCGet a where a.PolNo in (select b.polno from
				// lcpol b where b.ContNo='"
				// + mLCContSchema.getContNo() + "' and b.InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo()
				// + "')");
				tLCGetSet = tLCGetDB.executeQuery(tSBql.toString());
				if (tLCGetDB.mErrors.needDealError()) {
					CError.buildErr(this, "查询该被保险人给付项信息时失败；");
				}
				if (tLCGetSet.size() > 0) {
					map.put(tLCGetSet, "DELETE"); // 被保人下的给付删除
				}

				tSBql = new StringBuffer(256);
				tSBql
						.append("insert into LOBPol (select * from LCPol where ContNo='");
				tSBql.append(mLCContSchema.getContNo());
				tSBql.append("' and InsuredNo = '");
				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
				tSBql.append("')");
				// map.put("insert into LOBPol (select * from LCPol where
				// ContNo='"
				// + mLCContSchema.getContNo() + "' and InsuredNo = '"
				// + mOLDLCInsuredDB.getInsuredNo() + "')", "INSERT");
				map.put(tSBql.toString(), "INSERT");

				map.put(preLCPolSet, "DELETE"); // 被保人下的险种删除

				// 删除个人险种下的投资规则 chenwm20070914
//				tSBql = new StringBuffer(256);
//				tSBql
//						.append("insert into Lobperinvestplan (select * from lcperinvestplan a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
//				tSBql.append(mLCContSchema.getContNo());
//				tSBql.append("' and b.InsuredNo = '");
//				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
//				tSBql.append("'))");
//				map.put(tSBql.toString(), "INSERT");

//				tSBql = new StringBuffer(256);
//				tSBql
//						.append("select * from lcperinvestplan a where a.PolNo in (select b.polno from lcpol b where b.ContNo='");
//				tSBql.append(mLCContSchema.getContNo());
//				tSBql.append("' and b.InsuredNo = '");
//				tSBql.append(mOLDLCInsuredDB.getInsuredNo());
//				tSBql.append("')");
			
		
			

				for (int polcount = 1; polcount <= preLCPolSet.size(); polcount++) {
					// 填写险种删除操作的日志
					LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();
					LCPolSchema tLCPolSchema = new LCPolSchema();
					tLCPolSchema.setSchema(preLCPolSet.get(polcount));
					mLCDelPolLog.setOtherNo(tLCPolSchema.getPolNo());
					if (ContType.equals("2")) {
						mLCDelPolLog.setOtherNoType("4");
					} else {
						mLCDelPolLog.setOtherNoType("14");
					}
					mLCDelPolLog.setPrtNo(tLCPolSchema.getPrtNo());
					if (tLCPolSchema.getAppFlag().equals("1")) {
						mLCDelPolLog.setIsPolFlag("1");
					} else {
						mLCDelPolLog.setIsPolFlag("0");
					}
					mLCDelPolLog.setOperator(mGlobalInput.Operator);
					mLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
					mLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
					mLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
					mLCDelPolLog.setModifyDate(PubFun.getCurrentDate());
					mLCDelPolLog.setModifyTime(PubFun.getCurrentTime());
					mLCDelPolLogSet.add(mLCDelPolLog);
					if (polcount == preLCPolSet.size()) {
						map.put(mLCDelPolLogSet, "INSERT");
					}

					// 更新合同表，这个地方用java做数字的加减，可能会导致误差
					mLCContSchema.setPrem(mLCContSchema.getPrem()
							- preLCPolSet.get(polcount).getPrem());
					mLCContSchema.setSumPrem(mLCContSchema.getSumPrem()
							- preLCPolSet.get(polcount).getSumPrem());
					mLCContSchema.setAmnt(mLCContSchema.getAmnt()
							- preLCPolSet.get(polcount).getAmnt());
					mLCContSchema.setMult(mLCContSchema.getMult()
							- preLCPolSet.get(polcount).getMult());
					mLCContSchema.setPeoples(mLCContSchema.getPeoples()
							- preLCPolSet.get(polcount).getInsuredPeoples());
			
					updateContFlag = "UPDATE";
					// 更新团体表
					if (ContType.equals("2")) {

						// 集体险种信息
						StringBuffer tSBWherePart = new StringBuffer(64);
						tSBWherePart.append("from LCPol where GrpContNo='");
						tSBWherePart.append(mLCContSchema.getGrpContNo());
						tSBWherePart.append("' and riskcode ='");
						tSBWherePart.append(preLCPolSet.get(polcount)
								.getRiskCode());
						tSBWherePart.append("')");

						// Alex 不统计公共帐户虚拟人 20050924
						StringBuffer tSBWherePart1 = new StringBuffer(64);
						tSBWherePart1.append("from LCPol where GrpContNo='");
						tSBWherePart1.append(mLCContSchema.getGrpContNo());
						tSBWherePart1.append("' and riskcode ='");
						tSBWherePart1.append(preLCPolSet.get(polcount)
								.getRiskCode());
						tSBWherePart1
								.append("' and PolTypeFlag<>'2' and PolTypeFlag<>'5')");

						// String fromPart = "from LCPol where GrpContNo='" +
						// mLCContSchema.getGrpContNo()
						// + "' and riskcode ='" +
						// preLCPolSet.get(polcount).getRiskCode() +
						// "')";

						tSBql = new StringBuffer(256);
						tSBql
								.append("update LCGrpPol set Prem=(select SUM(Prem) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",Amnt=(select SUM(Amnt) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",SumPrem=(select SUM(SumPrem) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",Mult=(select SUM(Mult) ");
						tSBql.append(tSBWherePart);
						tSBql.append(",Peoples2=(select SUM(InsuredPeoples) ");
						tSBql.append(tSBWherePart1);
						tSBql.append(" where grppolno='");
						tSBql.append(preLCPolSet.get(polcount).getGrpPolNo());
						tSBql.append("'");
						// map.put("update LCGrpPol set Prem=(select SUM(Prem) "
						// + fromPart
						// + ",Amnt=(select SUM(Amnt) " + fromPart
						// + ",SumPrem=(select SUM(SumPrem) " + fromPart
						// + ",Mult=(select SUM(Mult) " + fromPart
						// + ",Peoples2=(select SUM(InsuredPeoples) "
						// + fromPart
						// + " where grppolno='"
						// + preLCPolSet.get(polcount).getGrpPolNo() + "'"
						// , "UPDATE");
						map.put(tSBql.toString(), "UPDATE");
					}
				}

			}
			if (("1").equals(mLCContSchema.getPolType())) { // 无名单
				mLCContSchema.setPeoples(0);

			} else {
				if (!("2").equals(mLCContSchema.getPolType())
						&& !("5").equals(mLCContSchema.getPolType())) { // 减少公共帐户虚拟被保人不更新人数
					// Alex
					// 20050924
					mLCContSchema.setPeoples(mLCContSchema.getPeoples() - 1);
				}

			}
			// 更新 计划与删除公共保额 的关联 js中已经控制 注释掉
			/*
			 * if(("5").equals(mLCContSchema.getPolType())){ StringBuffer
			 * updateLCContStr = new StringBuffer(64);
			 * updateLCContStr.append("UPDATE lccontplan SET pubamncontno=''
			 * WHERE pubamncontno='");
			 * updateLCContStr.append(mLCContSchema.getContNo());
			 * updateLCContStr.append("'"); map.put(updateLCContStr.toString(),
			 * "UPDATE"); }
			 */
			if (ContType.equals("2")) {
				// 集体险种信息
				StringBuffer tSBWherePart = new StringBuffer(64);
				tSBWherePart.append("from LCPol where GrpContNo='");
				tSBWherePart.append(mLCContSchema.getGrpContNo());
				tSBWherePart.append("')");
				// String fromPart = "from LCPol where GrpContNo='" +
				// mLCContSchema.getGrpContNo()
				// + "')";

				updateLCGrpContStr = new StringBuffer(256);
				updateLCGrpContStr
						.append("update LCGrpCont set Prem=(select SUM(Prem) ");
				updateLCGrpContStr.append(tSBWherePart);
				updateLCGrpContStr.append(",Amnt=(select SUM(Amnt) ");
				updateLCGrpContStr.append(tSBWherePart);
				updateLCGrpContStr.append(",SumPrem=(select SUM(SumPrem) ");
				updateLCGrpContStr.append(tSBWherePart);
				updateLCGrpContStr.append(",Mult=(select SUM(Mult) ");
				updateLCGrpContStr.append(tSBWherePart);
				updateLCGrpContStr
						.append(",Peoples2=(select SUM(Peoples) from lccont where grpcontno='");
				updateLCGrpContStr.append(mLCContSchema.getGrpContNo());
				updateLCGrpContStr
						.append("' and PolType<>'2' and PolType<>'5') where grpcontno='");
				updateLCGrpContStr.append(mLCContSchema.getGrpContNo());
				updateLCGrpContStr.append("'");
				// updateLCGrpContStr = "update LCGrpCont set Prem=(select
				// SUM(Prem) " +
				// fromPart
				// + ",Amnt=(select SUM(Amnt) " +
				// fromPart
				// + ",SumPrem=(select SUM(SumPrem) " +
				// fromPart
				// + ",Mult=(select SUM(Mult) " +
				// fromPart
				// + ",Peoples2=(select SUM(Peoples) from lccont where
				// grpcontno='"
				// + mLCContSchema.getGrpContNo() +
				// "') where grpcontno='"
				// + mLCContSchema.getGrpContNo() +
				// "'";
				updateGrpContFlag = true;

			}

			// 主被保险人时删除合同上主被保险人信息,在点录入完成，复核修改完成时，需要检查是否有主被保险人
			if (("00").equals(mOLDLCInsuredDB.getRelationToMainInsured())) {
				// //////////////////在此首先填写日志////////////////////////////////////////
				LCDelPolLogSchema InsuredLCDelPolLog = new LCDelPolLogSchema();
				InsuredLCDelPolLog.setOtherNo(mOLDLCInsuredDB.getSchema()
						.getInsuredNo());
				InsuredLCDelPolLog.setOtherNoType("13");
				InsuredLCDelPolLog.setPrtNo(mOLDLCInsuredDB.getSchema()
						.getPrtNo());
				if (mOLDLCInsuredDB.getSchema().equals("1")) { // no
					// appflag,only
					// give it
					InsuredLCDelPolLog.setIsPolFlag("1");
				} else {
					InsuredLCDelPolLog.setIsPolFlag("0");
				}
				InsuredLCDelPolLog.setOperator(mGlobalInput.Operator);
				InsuredLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
				InsuredLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
				InsuredLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
				InsuredLCDelPolLog.setModifyDate(PubFun.getCurrentDate());
				InsuredLCDelPolLog.setModifyTime(PubFun.getCurrentTime());
				// 日志填写完毕

				mLCContSchema.setInsuredBirthday("");
				mLCContSchema.setInsuredIDNo("");
				mLCContSchema.setInsuredIDType("");
				mLCContSchema.setInsuredName("");
				mLCContSchema.setInsuredNo("0");
				mLCContSchema.setInsuredSex("");
				mLCContSchema.setModifyDate(theCurrentDate);
				mLCContSchema.setModifyTime(theCurrentTime);
				updateContFlag = "UPDATE";
				// 团单时删除合同
				if (ContType.equals("2")) {
					// 首先将被保人的删除操作的日志填写,团单与个单的不同仅仅在于Type
					InsuredLCDelPolLog.setOtherNoType("3");
					// 填写日志完毕
					LCInsuredDB tLCInsuredDB = null;
					if (this.isConCanUse)
						tLCInsuredDB = new LCInsuredDB(this.con);
					else
						tLCInsuredDB = new LCInsuredDB();
					tLCInsuredDB.setContNo(mOLDLCInsuredDB.getContNo());
					int insuredCount = tLCInsuredDB.getCount();
					if (tLCInsuredDB.mErrors.needDealError()) {
						CError tError = new CError();
						tError.moduleName = "ContInsuredBL";
						tError.functionName = "getInputData";
						tError.errorMessage = "查询客户时失败!";
						this.mErrors.addOneError(tError);
						return false;
					}
					// 最后一个客户时需要删除个人合同表
					if (insuredCount == 1) {
						updateContFlag = "DELETE";
						// 在此首先填写个单合同日志
						LCDelPolLogSchema LCContLCDelPolLog = new LCDelPolLogSchema();
						LCContLCDelPolLog.setOtherNo(mLCContSchema.getContNo());
						LCContLCDelPolLog.setOtherNoType("2");
						LCContLCDelPolLog.setPrtNo(mLCContSchema.getPrtNo());
						if (mLCContSchema.getAppFlag().equals("1")) {
							LCContLCDelPolLog.setIsPolFlag("1");
						} else {
							LCContLCDelPolLog.setIsPolFlag("0");
						}
						LCContLCDelPolLog.setOperator(mGlobalInput.Operator);
						LCContLCDelPolLog.setManageCom(mGlobalInput.ManageCom);
						LCContLCDelPolLog.setMakeDate(PubFun.getCurrentDate());
						LCContLCDelPolLog.setMakeTime(PubFun.getCurrentTime());
						LCContLCDelPolLog
								.setModifyDate(PubFun.getCurrentDate());
						LCContLCDelPolLog
								.setModifyTime(PubFun.getCurrentTime());
						map.put(LCContLCDelPolLog, "INSERT");
						// 日志填写完毕
						// 数据的备份
						tSBql = new StringBuffer(128);
						tSBql
								.append("insert into LOBCont (select * from LCCont where ContNo='");
						tSBql.append(mLCContSchema.getContNo());
						tSBql.append("')");
						// map.put("insert into LOBCont (select * from LCCont
						// where ContNo='"
						// + mLCContSchema.getContNo() + "')", "INSERT");
						map.put(tSBql.toString(), "INSERT");
					}
				}
				// map.put(mLCContSchema, "UPDATE");
				// 删除主被保险人时填写日志
				map.put(InsuredLCDelPolLog, "INSERT");
			}else{
				updateContFlag = "DELETE";
			}

		}
		
		//删除GIL处理信息
		MMap tMMap = new MMap();
		map.add(tMMap);
		
		if (!updateContFlag.equals("INSERT")) {
			map.put(mLCContSchema, updateContFlag);
		}
		if (updateGrpContFlag) {
			map.put(updateLCGrpContStr.toString(), "UPDATE");
		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 * @param cInputData
	 *            VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			this.mLCContSchema.setSchema((LCContSchema) cInputData
					.getObjectByObjectName("LCContSchema", 0));
			this.mLCInsuredSchema.setSchema((LCInsuredSchema) cInputData
					.getObjectByObjectName("LCInsuredSchema", 0));
			this.mLCAddressSchema.setSchema((LCAddressSchema) cInputData
					.getObjectByObjectName("LCAddressSchema", 0));
			this.mOLDLCInsuredDB.setSchema((LCInsuredDB) cInputData
					.getObjectByObjectName("LCInsuredDB", 0));
			// System.out.println("asdfasdf" + mOLDLCInsuredDB.getInsuredNo());
			this.mLDPersonSchema.setSchema((LDPersonSchema) cInputData
					.getObjectByObjectName("LDPersonSchema", 0));
			this.mLCCustomerImpartSet = (LCCustomerImpartSet) cInputData
					.getObjectByObjectName("LCCustomerImpartSet", 0);
			this.mLCCustomerImpartDetailSet = (LCCustomerImpartDetailSet) cInputData
					.getObjectByObjectName("LCCustomerImpartDetailSet", 0);
			this.mTransferData = (TransferData) cInputData
					.getObjectByObjectName("TransferData", 0);
			this.otmInsuredNo = (HashMap) cInputData.getObjectByObjectName(
					"HashMap", 0);
			this.otmInsuredId = (HashMap) cInputData.getObjectByObjectName(
					"HashMap", 9);
			this.otmInsuredNoCvalidate = (HashMap) cInputData
					.getObjectByObjectName("HashMap", 10);
			this.otmInsuredIdCvalidate = (HashMap) cInputData
					.getObjectByObjectName("HashMap", 11);
			//add by winnie ASR20093243 20090504
			this.mInsuredComMap=(HashMap)cInputData.getObjectByObjectName("HashMap",12);
            this.otmInsuredNoOrange = (HashMap) cInputData
			        .getObjectByObjectName("HashMap", 13);
			if(this.mInsuredComMap==null){
				this.mInsuredComMap=new HashMap();
				 //add by winnie ASR20093243 统括保单增加了被保人分支机构的校验，故需要提供对应的分支机构的对应关系
	          
	            //end add 20090504
			}
			//end add
			// chenwm071113 防止界面录入时报空指针错误.
			if (this.otmInsuredNo == null) {
				this.otmInsuredNo = new HashMap();
			}
			if (this.otmInsuredId == null) {
				this.otmInsuredId = new HashMap();
			}
			if (this.otmInsuredNoCvalidate == null) {
				this.otmInsuredNoCvalidate = new HashMap();
			}
			if (this.otmInsuredNoOrange == null) {
				this.otmInsuredNoOrange = new HashMap();
			}
			if (this.otmInsuredIdCvalidate == null) {
				this.otmInsuredIdCvalidate = new HashMap();
			}

			if (mTransferData == null) {
				CError tError = new CError();
				tError.moduleName = "ContInsuredBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "未得到传送数据!";
				this.mErrors.addOneError(tError);
				return false;

			}
			if (mLCContSchema.getPayIntv() == 88) {
				FirstTrialFlag = true;
			}
			FamilyType = (String) mTransferData.getValueByName("FamilyType");
			tPolTypeFlag = (String) mTransferData.getValueByName("PolTypeFlag");
			tInsuredPeoples = (String) mTransferData
					.getValueByName("InsuredPeoples");
			tInsuredAppAge = (String) mTransferData
					.getValueByName("InsuredAppAge");
			FamilyType = (String) mTransferData.getValueByName("FamilyType");
			tSequenceNo = (String) mTransferData.getValueByName("SequenceNo");
			ContType = (String) mTransferData.getValueByName("ContType");
			EdorType = (String) mTransferData.getValueByName("EdorType");
			tNoticeWay = (String) mTransferData.getValueByName("NoticeWay");  //理赔通知方式
			
			EdorAcceptNo = (String) mTransferData
					.getValueByName("EdorAcceptNo");
			EdorNo = (String) mTransferData.getValueByName("EdorNo");
			System.out.println(EdorType);
			String DiskImportFlag = (String) mTransferData
					.getValueByName("DiskImportFlag");
			String OccupaytionType = mLCInsuredSchema.getOccupationType();
			ref.transFields(mLCInsuredSchema, mLDPersonSchema); // 客户表
			mLCInsuredSchema.setOccupationType(OccupaytionType);
			mLCInsuredSchema.setInsuredNo(mLDPersonSchema.getCustomerNo());
	

			mLCContSchema.setContType(ContType);
			ContNo = mLCContSchema.getContNo();
			// 当是个人时，由界面上取得合同号，当是团体下个人的时候，需要在保存个人合同下第一个的时候，生成LCCont
			if (ContNo == null || ("").equals(ContNo)) {
				// @@错误处理
				if (ContType.equals("1")) {
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "未得到合同号!";
					this.mErrors.addOneError(tError);
					return false;
				}
			} else {
				if (DiskImportFlag != null && DiskImportFlag.equals("1")) {
					// DoNothing
				} else {
					LCContDB tLCContDB = null;
					if (this.isConCanUse)
						tLCContDB = new LCContDB(this.con);
					else
						tLCContDB = new LCContDB();
					tLCContDB.setContNo(ContNo);
					tLCContDB.getInfo();
					if (tLCContDB.mErrors.needDealError()) {
						// @@错误处理
						this.mErrors.copyAllErrors(tLCContDB.mErrors);
						CError tError = new CError();
						tError.moduleName = "ContInsuredBL";
						tError.functionName = "getInputData";
						tError.errorMessage = "未查到相关合同信息!";
						this.mErrors.addOneError(tError);
						return false;
					}

					tLCContDB.setBankCode(mLCContSchema.getBankCode());
					tLCContDB.setAccName(mLCContSchema.getAccName());
					tLCContDB.setBankAccNo(mLCContSchema.getBankAccNo());
					
					mLCContSchema.setSchema(tLCContDB);
					if ("5".equals(mLCContSchema.getPolType())
							|| "2".equals(mLCContSchema.getPolType())
							|| "1".equals(mLCContSchema.getPolType())
							|| "3".equals(mLCContSchema.getPolType())
							|| "4".equals(mLCContSchema.getPolType())) {
					}
				}
			}
			mLCContSchema.setFamilyType(FamilyType);
			if (!mOperate.equals("INSERT||CONTINSURED")) {
				// System.out.println("InsuredNo:" +
				// mOLDLCInsuredDB.getInsuredNo());
				mOLDLCInsuredDB.setContNo(mLCContSchema.getContNo());
				mOLDLCInsuredDB.setInsuredNo(mOLDLCInsuredDB.getInsuredNo());
				if (!mOLDLCInsuredDB.getInfo()) {
					// @@错误处理
					this.mErrors.copyAllErrors(mOLDLCInsuredDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "ContInsuredBL";
					tError.functionName = "deleteData";
					tError.errorMessage = "获取待删除被保险人信息时失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}

		} catch (Exception ex) {

			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "getInputData";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);
			return false;

		}

		// this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
		return true;
	}

	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
			mResult.clear();
			LCInsuredSchema tempLCInsuredSchema = new LCInsuredSchema();
			tempLCInsuredSchema.setSchema(mLCInsuredSchema);
			mResult.add(tempLCInsuredSchema);
			mResult.add(this.tLCAddressSchema);
			mResult.add(this.mLDPersonSchema);
			mResult.add(this.mLCContSchema);
			mResult.add(map);
			// add by winnie ASR20093075
			mResult.add(mLPEdorItemSchema);
			mResult.add(mLPEdorMainSchema);
			mResult.add(mLPGrpEdorItemSchema);
			// end
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCInsuredBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private boolean delrisk(VData cInputData) {
		// System.out.println("asfdsfsfafasf0");
		this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		// System.out.println("asfdsfsfafasf2");
		if (mTransferData == null) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "未得到传送数据!";
			this.mErrors.addOneError(tError);
			return false;

		}
		// System.out.println("asfdsfsfafasf3");
		// String InsuredNo = (String)
		// mTransferData.getValueByName("InsuredNo");
		String PolNo = (String) mTransferData.getValueByName("PolNo");
		// /////////////////////准备更新合同表的保费数据//////////////////////////////////////////
		LCPolDB tLCPolDB = null;
		if (this.isConCanUse)
			tLCPolDB = new LCPolDB(this.con);
		else
			tLCPolDB = new LCPolDB();
		tLCPolDB.setPolNo(PolNo);
		if (!tLCPolDB.getInfo()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCPolDB.mErrors);

			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "delrisk";
			tError.errorMessage = "险种信息查询失败";
			this.mErrors.addOneError(tError);
			return false;
		}

		LCContDB tLCContDB = null;
		if (this.isConCanUse)
			tLCContDB = new LCContDB(this.con);
		else
			tLCContDB = new LCContDB();
		tLCContDB.setContNo(tLCPolDB.getContNo());
		if (!tLCContDB.getInfo()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCContDB.mErrors);

			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "delrisk";
			tError.errorMessage = "险种合同信息查询失败";
			this.mErrors.addOneError(tError);
			return false;
		}
		// String FORMATMODOL = "0.00";
		// DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL);
		String strCalContPrem = String.valueOf(Arith.round(tLCContDB.getPrem()
				- tLCPolDB.getPrem(), mSCALE)); // 转换计算后的保费(规定的精度)
		String strCalContAmnt = String.valueOf(Arith.round(tLCContDB.getAmnt()
				- tLCPolDB.getAmnt(), mSCALE)); // 转换计算后的保额
		String strCalContSumPrem = String.valueOf(Arith.round(tLCContDB
				.getSumPrem()
				- tLCPolDB.getSumPrem(), mSCALE));
		double calContPrem = Double.parseDouble(strCalContPrem);
		double calContAmnt = Double.parseDouble(strCalContAmnt);
		double calContSumPrem = Double.parseDouble(strCalContSumPrem);
		tLCContDB.setPrem(calContPrem);
		tLCContDB.setAmnt(calContAmnt);
		tLCContDB.setMult(mLCContSchema.getMult() - tLCPolDB.getMult());
		tLCContDB.setSumPrem(calContSumPrem);
		// ///////////////////准备更新合同表的保费数据完毕///////////////////////////////////
		StringBuffer tSBql = new StringBuffer(128);
		tSBql.append("delete from lcpol where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from lcpol where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCInsuredRelated where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCInsuredRelated where PolNo='" + PolNo + "'",
		// "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCDuty where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCDuty where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCBnf where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCBnf where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCPrem where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCPrem where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCGet where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCGet where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCSpec where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCSpec where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		tSBql = new StringBuffer(128);
		tSBql.append("delete from LCSpec where PolNo='");
		tSBql.append(PolNo);
		tSBql.append("'");
		// map.put("delete from LCSpec where PolNo='" + PolNo + "'", "DELETE");
		map.put(tSBql.toString(), "DELETE");

		map.put(tLCContDB.getSchema(), "UPDATE");

		return true;
	}

	/**
	 * 检查被保人录入的数据是否正确(适用于个人投保单和集体下的个单) 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean checkLDPerson() {
		// 如果是无名单或者公共帐户的个人，不校验返回
		if (StrTool.compareString(tPolTypeFlag, "1")
				|| StrTool.compareString(tPolTypeFlag, "2")
				|| StrTool.compareString(tPolTypeFlag, "5")
				|| StrTool.compareString(tPolTypeFlag, "3")
				|| StrTool.compareString(tPolTypeFlag, "4")) {
			return true;
		}

		if (mLDPersonSchema != null) {
			if (mLDPersonSchema.getName() != null) {
				// 去空格
				mLDPersonSchema.setName(mLDPersonSchema.getName().trim());
				if (mLDPersonSchema.getSex() != null) {
					mLDPersonSchema.setSex(mLDPersonSchema.getSex().trim());
				}
				if (mLDPersonSchema.getIDNo() != null) {
					mLDPersonSchema.setIDNo(mLDPersonSchema.getIDNo().trim().toUpperCase());
				}
				if (mLDPersonSchema.getIDType() != null) {
					mLDPersonSchema.setIDType(mLDPersonSchema.getIDType()
							.trim());
				}
				if (mLDPersonSchema.getBirthday() != null) {
					mLDPersonSchema.setBirthday(mLDPersonSchema.getBirthday()
							.trim());
				}

			}
			if (mLDPersonSchema.getCustomerNo() != null) {
				// 如果有客户号
				if (!mLDPersonSchema.getCustomerNo().equals("")) {
					LDPersonDB tLDPersonDB = null;
					if (this.isConCanUse)
						tLDPersonDB = new LDPersonDB(this.con);
					else
						tLDPersonDB = new LDPersonDB();

					tLDPersonDB.setCustomerNo(mLDPersonSchema.getCustomerNo());
					if (!tLDPersonDB.getInfo()) {
						CError tError = new CError();
						tError.moduleName = "ContInsuredBL";
						tError.functionName = "checkPerson";
						tError.errorMessage = "查询客户失败!请确认您录入的客户号码";
						this.mErrors.addOneError(tError);

						return false;
					}
					if (mLDPersonSchema.getName() != null) {
						String Name = StrTool.GBKToUnicode(tLDPersonDB
								.getName().trim());
						String NewName = StrTool.GBKToUnicode(mLDPersonSchema
								.getName().trim());

						if (!Name.equals(NewName)) {
							CError tError = new CError();
							tError.moduleName = "ContInsuredBL";
							tError.functionName = "checkPerson";
							tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户姓名("
									+ Name + ")与您录入的客户姓名(" + NewName + ")不匹配！";
							this.mErrors.addOneError(tError);

							return false;
						}
					}
					// if (mLDPersonSchema.getEnglishName() != null) {
					// String Name = StrTool.GBKToUnicode(tLDPersonDB
					// .getEnglishName().trim());
					// String NewName = StrTool.GBKToUnicode(mLDPersonSchema
					// .getEnglishName().trim());
					//
					// if (!Name.equals(NewName)) {
					// CError tError = new CError();
					// tError.moduleName = "ContInsuredBL";
					// tError.functionName = "checkPerson";
					// tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户的英语姓名("
					// + Name + ")与您录入的客户英语姓名(" + NewName
					// + ")不匹配！";
					// this.mErrors.addOneError(tError);
					//
					// return false;
					// }
					// }

					if (mLDPersonSchema.getSex() != null) {
						if (!tLDPersonDB.getSex().equals(
								mLDPersonSchema.getSex())) {
							CError tError = new CError();
							tError.moduleName = "ContInsuredBL";
							tError.functionName = "checkPerson";
							tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户性别("
									+ tLDPersonDB.getSex() + ")与您录入的客户性别("
									+ mLDPersonSchema.getSex() + ")不匹配！";
							this.mErrors.addOneError(tError);

							return false;
						}
					}
					if (mLDPersonSchema.getBirthday() != null) {
						if (!tLDPersonDB.getBirthday().equals(
								mLDPersonSchema.getBirthday())) {
							CError tError = new CError();
							tError.moduleName = "ContInsuredBL";
							tError.functionName = "checkPerson";
							tError.errorMessage = "您输入的被保险人客户号对应在数据库中的客户生日("
									+ tLDPersonDB.getBirthday() + ")与您录入的客户生日("
									+ mLDPersonSchema.getBirthday() + ")不匹配！";
							this.mErrors.addOneError(tError);

							return false;
						}
					}

				} else {
					// 如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
					/*
					 * 2008-5-16 modified by zhangjq:
					 * 修改判断条件，修改成只对身份证类型进行继续校验，对于非身份证类型证件在GrpPolImpInfo中的getContData()
					 * 中已经进行判断，无需在这里进行判断
					 */
					if ((mLDPersonSchema.getName() != null)
							&& (mLDPersonSchema.getSex() != null)
							&& (mLDPersonSchema.getIDNo() != null)
							&& (mLDPersonSchema.getIDType() != null)) {
						LDPersonDB tLDPersonDB = null;
						if (this.isConCanUse)
							tLDPersonDB = new LDPersonDB(this.con);
						else
							tLDPersonDB = new LDPersonDB();
//						tLDPersonDB.setName(mLDPersonSchema.getName());
//						tLDPersonDB.setSex(mLDPersonSchema.getSex());
//						tLDPersonDB.setBirthday(mLDPersonSchema.getBirthday());
//
//						tLDPersonDB.setIDType(mLDPersonSchema.getIDType());
//						tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo()
//								.toLowerCase());// 先查带小写的x
//
//						LDPersonSet tLDPersonSet = tLDPersonDB.query();
//						/*
//						 * 2008-5-16 modified by zhangjq:
//						 * 修改判断条件，对于DB类的query()方法，始终会返回一个非空对象，故这里原来使用tLDPersonSet==null进行判断显然是不对的
//						 */
//						if (tLDPersonSet != null && tLDPersonSet.size() == 0) {
//							tLDPersonDB.setIDNo(mLDPersonSchema.getIDNo()
//									.toUpperCase());// 如果为空,再查带大写的X.
//							tLDPersonSet = tLDPersonDB.query();
//						}
						LDPersonSet tLDPersonSet = new LDPersonSet();
						tLDPersonSet = tLDPersonDB.executeQuery("select * from ldperson where name='" //fengyan
							+ mLDPersonSchema.getName()
							+ "' and sex='"
							+ mLDPersonSchema.getSex()
							+ "' and birthday=date'"
							+ mLDPersonSchema.getBirthday()
							+ "' and idtype='"
							+ mLDPersonSchema.getIDType()
							+ "' and upper(idno)=upper('"
							+ mLDPersonSchema.getIDNo() + "')");
						
						if (tLDPersonSet != null) {
							if (tLDPersonSet.size() > 0) {
								mLDPersonSchema.setCustomerNo(tLDPersonSet.get(
										1).getCustomerNo());
							} else // 针对身份证做校验
							{
								//modify by winnie PIR20092271  身份证15位和18位自动识别 20090826
								if (mLDPersonSchema.getIDType() != null
										&& !mLDPersonSchema.getIDType().equals("")
										&& mLDPersonSchema.getIDType().equals("0")) {
//								if (tLDPersonDB.getIDType() != null
//										&& !tLDPersonDB.getIDType().equals("")
//										&& tLDPersonDB.getIDType().equals("0")) {
								//end modify PIR20092271
									LDPersonDB ttLDPersonDB = null;
									if (this.isConCanUse)
										ttLDPersonDB = new LDPersonDB(this.con);
									else
										ttLDPersonDB = new LDPersonDB();
									ttLDPersonDB.setName(mLDPersonSchema
											.getName());
									ttLDPersonDB.setSex(mLDPersonSchema
											.getSex());
									ttLDPersonDB.setBirthday(mLDPersonSchema
											.getBirthday());
									ttLDPersonDB.setIDType(mLDPersonSchema
											.getIDType());
									LDPersonSet ttLDPersonSet = ttLDPersonDB
											.query();
									if (ttLDPersonSet.size() > 0) {
										// PubFun tPubFun=new PubFun();
										for (int ccount = 1; ccount <= ttLDPersonSet
												.size(); ccount++) {
											if (PubFun.CompareId(ttLDPersonSet
													.get(ccount).getIDNo(),
													mLDPersonSchema.getIDNo())) {
												mLDPersonSchema
														.setCustomerNo(ttLDPersonSet
																.get(ccount)
																.getCustomerNo());
												break;
											}
										}
									}
								}
							}

						}
					}
				}
			}

		}
		return true;
	}

	/**
	 * 检查地址数据是否正确 如果在处理过程中出错或者数据有错误，则返回false,否则返回true
	 * 
	 * @return boolean
	 */
	private boolean checkLCAddress() {
		if (mLCAddressSchema != null) {
			if (mLCAddressSchema.getPostalAddress() != null) { // 去空格
				mLCAddressSchema.setPostalAddress(mLCAddressSchema
						.getPostalAddress().trim());
				if (mLCAddressSchema.getZipCode() != null) {
					mLCAddressSchema.setZipCode(mLCAddressSchema.getZipCode()
							.trim());
				}
				if (mLCAddressSchema.getPhone() != null) {
					mLCAddressSchema.setPhone(mLCAddressSchema.getPhone()
							.trim());
				}

			}
			// if (mLCAddressSchema.getAddressNo() != null)
			// {
			// //如果有地址号
			// if (!mLCAddressSchema.getAddressNo().equals(""))
			// {
			// LCAddressDB tLCAddressDB = new LCAddressDB();
			//
			// tLCAddressDB.setAddressNo(mLCAddressSchema.getAddressNo());
			// tLCAddressDB.setCustomerNo(mLCAddressSchema.getCustomerNo());
			// if (!tLCAddressDB.getInfo())
			// {
			// CError tError = new CError();
			// tError.moduleName = "ContInsuredBL";
			// tError.functionName = "checkAddress";
			// tError.errorMessage = "数据库查询失败!";
			// this.mErrors.addOneError(tError);
			//
			// return false;
			// }
			// if (!StrTool.compareString(mLCAddressSchema.getCustomerNo(),
			// tLCAddressDB.getCustomerNo())
			// || !StrTool.compareString(mLCAddressSchema.getAddressNo(),
			// tLCAddressDB.getAddressNo())
			// || !StrTool.compareString(mLCAddressSchema.
			// getPostalAddress(),
			// tLCAddressDB.getPostalAddress())
			// || !StrTool.compareString(mLCAddressSchema.getZipCode(),
			// tLCAddressDB.getZipCode())
			// || !StrTool.compareString(mLCAddressSchema.getPhone(),
			// tLCAddressDB.getPhone())
			// || !StrTool.compareString(mLCAddressSchema.getFax(),
			// tLCAddressDB.getFax())
			// || !StrTool.compareString(mLCAddressSchema.
			// getHomeAddress(),
			// tLCAddressDB.getHomeAddress())
			// || !StrTool.compareString(mLCAddressSchema.
			// getHomeZipCode(),
			// tLCAddressDB.getHomeZipCode())
			// || !StrTool.compareString(mLCAddressSchema.getHomePhone(),
			// tLCAddressDB.getHomePhone())
			// || !StrTool.compareString(mLCAddressSchema.getHomeFax(),
			// tLCAddressDB.getHomeFax())
			// || !StrTool.compareString(mLCAddressSchema.
			// getCompanyAddress(),
			// tLCAddressDB.
			// getCompanyAddress())
			// || !StrTool.compareString(mLCAddressSchema.
			// getCompanyZipCode(),
			// tLCAddressDB.
			// getCompanyZipCode())
			// || !StrTool.compareString(mLCAddressSchema.
			// getCompanyPhone(),
			// tLCAddressDB.getCompanyPhone())
			// || !StrTool.compareString(mLCAddressSchema.
			// getCompanyFax(),
			// tLCAddressDB.getCompanyFax())
			// || !StrTool.compareString(mLCAddressSchema.getMobile(),
			// tLCAddressDB.getMobile())
			// || !StrTool.compareString(mLCAddressSchema.getMobileChs(),
			// tLCAddressDB.getMobileChs())
			// || !StrTool.compareString(mLCAddressSchema.getEMail(),
			// tLCAddressDB.getEMail())
			// || !StrTool.compareString(mLCAddressSchema.getBP(),
			// tLCAddressDB.getBP())
			// || !StrTool.compareString(mLCAddressSchema.getMobile2(),
			// tLCAddressDB.getMobile2())
			// || !StrTool.compareString(mLCAddressSchema.
			// getMobileChs2(),
			// tLCAddressDB.getMobileChs2())
			// || !StrTool.compareString(mLCAddressSchema.getEMail2(),
			// tLCAddressDB.getEMail2())
			// || !StrTool.compareString(mLCAddressSchema.getBP2(),
			// tLCAddressDB.getBP2())
			// )
			// {
			// CError tError = new CError();
			// tError.moduleName = "ContInsuredBL";
			// tError.functionName = "checkAddress";
			// tError.errorMessage =
			// "您输入的地址信息与数据库里对应的信息不符，请去掉地址号，重新生成!";
			// this.mErrors.addOneError(tError);
			//
			// return false;
			//
			// }
			//
			// }
			// }

		}

		return true;
	}

	/**
	 * 对比险种中关系到保额保费计算的被保人信息是否有更改
	 * 
	 * @param vLDPersonSchema
	 *            LDPersonSchema
	 * @param vLCInsuredSchema
	 *            LCInsuredSchema
	 * @return boolean
	 */
	private static boolean balance(LDPersonSchema vLDPersonSchema,
			LCInsuredSchema vLCInsuredSchema) {
		// 性别
		if (!(vLDPersonSchema.getSex().trim()).equals(vLCInsuredSchema.getSex()
				.trim())) {
			return true;
		}
		// 生日
		if (!(vLDPersonSchema.getBirthday().trim()).equals(vLCInsuredSchema
				.getBirthday().trim())) {
			return true;
		}
		// 职业类别
		if (vLDPersonSchema.getOccupationType() != null) {
			if (!StrTool.compareString(vLDPersonSchema.getOccupationType(),
					vLCInsuredSchema.getOccupationType())) {
				return true;
			}
		}
		// 职业类别
		if (!StrTool.compareString(vLDPersonSchema.getIDType(),
				vLCInsuredSchema.getIDType())) {
			return true;
		}
		if (!StrTool.compareString(vLDPersonSchema.getIDNo(), vLCInsuredSchema
				.getIDNo())) {
			return true;
		} else {
			if (vLCInsuredSchema.getOccupationType() != null) {
				return true;
			}
		}
		return false;
	}

	private boolean PBdealdata() {
		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
		tLPGrpEdorItemDB.setEdorAcceptNo(EdorAcceptNo);
		tLPGrpEdorItemDB.setEdorNo(EdorNo);
		tLPGrpEdorItemDB.setEdorType("PB");
		tLPGrpEdorItemDB.setGrpContNo(mLCContSchema.getGrpContNo());
		tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
		if (tLPGrpEdorItemSet != null && tLPGrpEdorItemSet.size() > 0) {
			tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
		} else {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "checkAddress";
			tError.errorMessage = "查询LPGrpEdorItem数据不成功!";
			this.mErrors.addOneError(tError);

			return false;
		}

		tLPGrpEdorItemSchema.setEdorState("3");
		this.map.put(tLPGrpEdorItemSchema, "DELETE&INSERT");

		return true;
	}

	private boolean PEdealdata() {
		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
		tLPGrpEdorItemDB.setEdorAcceptNo(EdorAcceptNo);
		tLPGrpEdorItemDB.setEdorNo(EdorNo);
		tLPGrpEdorItemDB.setEdorType("PE");
		tLPGrpEdorItemDB.setGrpContNo(mLCContSchema.getGrpContNo());
		tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
		if (tLPGrpEdorItemSet != null && tLPGrpEdorItemSet.size() > 0) {
			tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
		} else {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "checkAddress";
			tError.errorMessage = "查询LPGrpEdorItem数据不成功!";
			this.mErrors.addOneError(tError);

			return false;
		}

		tLPGrpEdorItemSchema.setEdorState("3");
		this.map.put(tLPGrpEdorItemSchema, "DELETE&INSERT");

		return true;
	}

	private boolean BQdealdata() {

		if (EdorNo == null || "".equals(EdorNo)) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "checkAddress";
			tError.errorMessage = "传入的数据为空EDORNO";
			this.mErrors.addOneError(tError);

			return false;
		}
		if (EdorType == null || "".equals(EdorType)) {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "checkAddress";
			tError.errorMessage = "传入的数据为空EDORNO";
			this.mErrors.addOneError(tError);

			return false;
		}
		LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
		LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		LPGrpEdorItemSet tLPGrpEdorItemSet = new LPGrpEdorItemSet();
		tLPGrpEdorItemDB.setEdorNo(EdorNo);
		tLPGrpEdorItemDB.setEdorType(EdorType);
		tLPGrpEdorItemDB.setGrpContNo(mLCContSchema.getGrpContNo());
		tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
		if (tLPGrpEdorItemSet != null && tLPGrpEdorItemSet.size() > 0) {
			tLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
		} else {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "checkAddress";
			tError.errorMessage = "查询LPGrpEdorItem数据不成功!";
			this.mErrors.addOneError(tError);

			return false;
		}

		LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
		LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
		LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
		tLPGrpEdorMainDB.setEdorNo(EdorNo);
		tLPGrpEdorMainDB.setGrpContNo(mLCContSchema.getGrpContNo());
		tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
		if (tLPGrpEdorMainSet != null && tLPGrpEdorMainSet.size() > 0) {
			tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);
		} else {
			CError tError = new CError();
			tError.moduleName = "ContInsuredBL";
			tError.functionName = "checkAddress";
			tError.errorMessage = "查询LPGrpEdorMain数据不成功!";
			this.mErrors.addOneError(tError);

			return false;
		}

		LPEdorItemSchema pLPEdorItemSchema = new LPEdorItemSchema();
		pLPEdorItemSchema.setEdorAcceptNo(tLPGrpEdorMainSchema
				.getEdorAcceptNo());
		pLPEdorItemSchema.setEdorNo(EdorNo);
		pLPEdorItemSchema.setEdorType(tLPGrpEdorItemSchema.getEdorType());
		pLPEdorItemSchema.setGrpContNo(tLPGrpEdorItemSchema.getGrpContNo());
		pLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
		pLPEdorItemSchema.setPolNo("000000");
		pLPEdorItemSchema.setEdorState("3");
		pLPEdorItemSchema.setEdorAppDate(tLPGrpEdorItemSchema.getEdorAppDate());
		pLPEdorItemSchema.setEdorAppNo(tLPGrpEdorItemSchema.getEdorAppNo());
		pLPEdorItemSchema.setEdorValiDate(mLCContSchema.getCValiDate());
		pLPEdorItemSchema.setGetMoney(mLCContSchema.getPrem());
		pLPEdorItemSchema.setUWFlag("0");
		pLPEdorItemSchema.setInsuredNo(mLCContSchema.getInsuredNo());
		pLPEdorItemSchema.setOperator(mGlobalInput.Operator);
		pLPEdorItemSchema.setManageCom(mLCContSchema.getManageCom());
		pLPEdorItemSchema.setModifyDate(PubFun.getCurrentDate());
		pLPEdorItemSchema.setModifyTime(PubFun.getCurrentTime());
		pLPEdorItemSchema.setMakeDate(PubFun.getCurrentDate());
		pLPEdorItemSchema.setMakeTime(PubFun.getCurrentTime());

		LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
		tLPEdorMainSchema.setEdorAcceptNo(tLPGrpEdorItemSchema
				.getEdorAcceptNo());
		tLPEdorMainSchema.setEdorNo(tLPGrpEdorItemSchema.getEdorNo());
		tLPEdorMainSchema.setContNo(pLPEdorItemSchema.getContNo());
		tLPEdorMainSchema.setEdorAppNo(tLPGrpEdorItemSchema.getEdorAppNo());
		tLPEdorMainSchema.setEdorAppName(tLPGrpEdorMainSchema.getEdorAppName());
		tLPEdorMainSchema.setManageCom(tLPGrpEdorMainSchema.getManageCom());
		tLPEdorMainSchema.setEdorAppDate(tLPGrpEdorItemSchema.getEdorAppDate());
		tLPEdorMainSchema.setEdorValiDate(mLCContSchema.getCValiDate());
		tLPEdorMainSchema.setEdorState("3");
		tLPEdorMainSchema.setBankCode(tLPGrpEdorMainSchema.getBankCode());
		tLPEdorMainSchema.setBankAccNo(tLPGrpEdorMainSchema.getBankAccNo());
		tLPEdorMainSchema.setAccName(tLPGrpEdorMainSchema.getAccName());
		tLPEdorMainSchema.setPostalAddress(tLPGrpEdorMainSchema
				.getPostalAddress());
		tLPEdorMainSchema.setZipCode(tLPGrpEdorMainSchema.getZipCode());
		tLPEdorMainSchema.setPhone(tLPGrpEdorMainSchema.getPhone());
		tLPEdorMainSchema.setPrintFlag(tLPGrpEdorMainSchema.getPrintFlag());
		tLPEdorMainSchema.setUWState("0");
		tLPEdorMainSchema.setOperator(mGlobalInput.Operator);
		tLPEdorMainSchema.setManageCom(mLCContSchema.getManageCom());
		tLPEdorMainSchema.setGetMoney(mLCContSchema.getPrem());
		tLPEdorMainSchema.setModifyDate(PubFun.getCurrentDate());
		tLPEdorMainSchema.setModifyTime(PubFun.getCurrentTime());
		tLPEdorMainSchema.setMakeDate(PubFun.getCurrentDate());
		tLPEdorMainSchema.setMakeTime(PubFun.getCurrentTime());
		this.map.put(pLPEdorItemSchema, "DELETE&INSERT");
		this.map.put(tLPEdorMainSchema, "DELETE&INSERT");

		tLPGrpEdorItemSchema.setEdorState("3");
		map.put(tLPGrpEdorItemSchema, "DELETE&INSERT");
		// add by winnie ASR20093075 保全增人应用
		try {
			mLPEdorItemSchema = (LPEdorItemSchema) pLPEdorItemSchema.clone();
			Reflections rf = new Reflections();
		rf.transFields(mLPEdorMainSchema,tLPEdorMainSchema);
			
			mLPGrpEdorItemSchema = (LPGrpEdorItemSchema) tLPGrpEdorItemSchema
					.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		// end add
		return true;
	}

	/**
	 * 主函数
	 * 
	 * @param args
	 *            String[]
	 */
	public static void main(String[] args) {
	}
}
