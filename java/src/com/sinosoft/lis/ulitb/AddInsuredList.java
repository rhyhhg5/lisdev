package com.sinosoft.lis.ulitb;

import java.util.Arrays;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.ulitb.NIDiskImport;
import com.sinosoft.lis.ulitb.GrpDiskImport;

/**
 * <p>Title: 增加被保人磁盘导入类</p>
 * <p>Description: 把从磁盘导入的被保人清单添加到数据库 </p>
 * <p>增加功能，处理从磁盘导入的多Sheet文档</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft </p>
 * @author QiuYang
 * @rewrite by Wulg
 * @version 1.0
 */

public class AddInsuredList {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 全局变量 */
    private GlobalInput mGlobalInput;
    private VData mResult = new VData();
    public String error;
    /** 团体合同号 */
    private String mGrpContNo = null;

    /** 保全号 */
    private String mEdorNo = null;

    /** 批次号 */
    private String mBatchNo = null;

    /** 团体合同号信息 */
    private LCGrpContSchema mLCGrpContSchema = null;

    private LPGrpEdorItemSchema mLPGrpEdorItemSchema = null;

    /** 当前日期 */
    private String mCurrentDate = PubFun.getCurrentDate();

    /** 当前时间 */
    private String mCurrentTime = PubFun.getCurrentTime();

    /** 节点名 */
    private String[] sheetName = {"InsuredInfo", "Ver", "RiskInfo"};
    /** 配置文件名 */
    private String configName = "UliGrpDiskImport.xml";
    
    private final static String[] OSNATIVECITY =  {"GHA","SOM","TZA","TUN","UGA","ZMB","TCD","CAF","AFG","ARE","OMN","AZE","PAK","PSE","BHR","BTN","PRK","TMP","PHL","GEO","KAZ","KOR","KGZ","KHM","QAT","KWT","LAO","LBN","MDV","MYS","MNG","BGD","MMR","NPL","JPN","SAU","LKA","TJK","THA","TUR","TKM","BRN","UZB","SGP","SYR","ARM","YEM","IRQ","IRN","ISR","IND","IDN","JOR","VNM","MAC","CHN","TWN","HKG","PLW","DZA","EGY","ETH","AGO","BEN","BWA","BFA","BDI","GNQ","TGO","ERI","CPV","GMB","COG","COD","DJI","GIN","GNB","GAB","ZWE","CMR","COM","CIV","KEN","LSO","LBR","LBY","RWA","MDG","MLI","MUS","MRT","MAR","MOZ","NAM","ZAF","SSD","NER","NGA","SLE","SEN","SYC","STP","SDN","SWZ","ESH","MWI","SHN","ALB","IRL","EST","AND","AUT","BEL","ISL","CYP","BIH","POL","BGR","BLR","DNK","DEU","RUS","FRA","FIN","NLD","MNE","CZE","HRV","LVA","LTU","LIE","LUX","ROM","MLT","MKD","MDA","MCO","NOR","PRT","SWE","CHE","SRB","SVK","SVN","SMR","UKR","ESP","GRC","HUN","ITA","GBR","VAT","FRO","GIB","REU","PYF","ATF","MTQ","GLP","MYT","SPM","WLF","NCL","VGB","IOT","SJM","ARG","ATG","BRB","BOL","BRA","DMA","ECU","COL","CUB","GRD","GUY","CAN","PER","USA","MEX","SUR","LCA","TTO","VEN","URY","JAM","CHL","BHS","ABW","AIA","PRY","PAN","BMU","PRI","BLZ","DOM","GUF","CRI","GRL","HTI","ANT","HND","CYM","VIR","MSR","NIC","SLV","VCT","TCA","GTM","UMI","MNP","MHL","FLK","SGS","GUM","KNA","AUS","PNG","FJI","COK","ASM","FSM","NRU","WSM","TON","VUT","NZL","HMD","CCK","NFK","SLB","PCN","NIU","TKL","TUV","KIR","BVT","CXR","ATA"};
    private final static String[] HKNATIVECITY =  {"1","2","3"};

    /**
     * 构造函数，保全入口
     * @param GrpContNo String
     * @param gi GlobalInput
     * @param edorNo StringD:\wuligang\workspace\doc
     */
    public AddInsuredList(String GrpContNo, GlobalInput gi, String edorNo) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
        this.mEdorNo = edorNo;
        if ((edorNo != null) && (!edorNo.equals(""))) {
            LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
            tLPGrpEdorItemDB.setEdorNo(mEdorNo);
            tLPGrpEdorItemDB.setEdorType("TZ");
            tLPGrpEdorItemDB.setGrpContNo(GrpContNo);
            LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
            if (tLPGrpEdorItemSet.size() > 0) {
                mLPGrpEdorItemSchema = tLPGrpEdorItemSet.get(1);
            }
        }
    }

    /**
     * 构造函数，新契约入口
     * @param GrpContNo String
     * @param gi GlobalInput
     */
    public AddInsuredList(String GrpContNo, GlobalInput gi) {
        this.mGlobalInput = gi;
        this.mGrpContNo = GrpContNo;
    }

    /**
     * 添加传入的多个Sheet数据
     * @param path String
     * @param fileName String
     */
    public boolean doAdd(String path, String fileName) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        GrpDiskImport importFile = new GrpDiskImport(path + fileName,
                path + configName,
                sheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LCInsuredListSet tLCInsuredListSet = (LCInsuredListSet) importFile.
                                             getSchemaSet();
        LCInsuredListPolSet tLCInsuredListPolSet = (LCInsuredListPolSet)
                importFile.
                getLCInsuredListPolSet();
        for(int i=1;i<=tLCInsuredListSet.size();i++){
         	String name=tLCInsuredListSet.get(i).getInsuredName();
            String IDType=tLCInsuredListSet.get(i).getIDType();
         	 String IDNo=tLCInsuredListSet.get(i).getIDNo();
         	 String birthday=tLCInsuredListSet.get(i).getBirthday();
         	 String sex=tLCInsuredListSet.get(i).getSex();
         	 String insuredid=tLCInsuredListSet.get(i).getInsuredID();
         	String nativeplace = tLCInsuredListSet.get(i).getNativePlace();
         	String nativecity = tLCInsuredListSet.get(i).getNativeCity();
         	String chk = chkOccupation(nativeplace,nativecity,IDType,IDNo);
         	if(!"".equals(chk)&&chk!=null){
         		mErrors.addOneError("导入被保人信息列表中，被保人:"+name+" ,"+chk+"!");
         		this.mErrors.setContent("导入被保人信息列表中，被保人:"+name+" ,"+chk+"!");
         		return false;
         	 }else{
         		 if("ML".equals(nativeplace)){
         			tLCInsuredListSet.get(i).setNativeCity("");
         		 }
         	 }
         	 if(IDNo!=null&&!"".equals(IDNo)){
         		String chkIDNo=PubFun.CheckIDNo(IDType, IDNo, birthday, sex);
            	 if(!"".equals(chkIDNo)&&chkIDNo!=null){
            		 mErrors.addOneError("导入被保人信息列表中,被保人:"+name+" ,"+chkIDNo+"!");
            		 this.mErrors.setContent("导入被保人信息列表中,被保人:"+name+","+chkIDNo+"!");
            		 return false;
            	 }
         	 }
         	String err="";
         	if(name==null||name.equals("")){          			
         			err+=",被保险人姓名缺失";
         	 }
         	if(IDType==null||IDType.equals("")){
         		err+=",被保险人证件类型缺失";
         	 }
         	if(IDNo==null||IDNo.equals("")){
         		err+=",被保险人证件号码缺失";
         	 }
         	if(birthday==null||birthday.equals("")){
         		err+=",被保险人出生日期缺失";
         	 }
         	if(sex==null||sex.equals("")){
         		err+=",被保险人性别缺失";
         	 }
         		if(err!=null&&!"".equals(err)){
         			mErrors.addOneError("导入被保人信息列表中,被保人号:"+insuredid+","+"被保险人姓名："+name+err+"!");
         		 return false;
         		}
         }
        for(int i=1;i<=tLCInsuredListSet.size();i++){
        	double PersonOwnPrem=tLCInsuredListSet.get(i).getPersonOwnPrem();
        	if(PersonOwnPrem==0.0||(int)PersonOwnPrem==0){
        		
        	}else{
        		error="个人账户直接交费金额不得录入！";
        		 this.mErrors.setContent("个人账户直接交费金额不得录入！");
                return false;
        		
        	}
        	
        }
        this.mResult.add(tLCInsuredListSet);
        this.mResult.add(tLCInsuredListPolSet);

        //存放Insert Into语句的容器
        MMap map = new MMap();
        if ((mEdorNo != null) && (!mEdorNo.equals(""))) {
            deleteInsuredList(map);
        }
        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
            //添加一个被保人
            addOneInsured(map, tLCInsuredListSet.get(i), i);
        }
        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
            deleteInsuredListPol(map, tLCInsuredListPolSet.get(i).getInsuredID(),
                                 tLCInsuredListPolSet.get(i).getRiskCode());
            addOneInsuredPol(map, tLCInsuredListPolSet.get(i), i);
        }

        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }

    /**
     * 保全增人导入。单sheet added by huxl @ 2006-12-8
     * 保全差异增人。多sheet 导入 modify by qulq 2007-7-24
     * @param path String
     * @param fileName String
     * @param edortype String
     * @return boolean
     */
    public boolean doAdd(String path, String fileName, String edorType) {
        //处理批次号：批次号码为文件名（不含扩展名）
        mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

        //从磁盘导入数据
        NIDiskImport importFile = new NIDiskImport(path + fileName,path + configName,
                sheetName);

        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrrors);
            return false;
        }

        String localVersion = importFile.getVersion();
        String strSql = "select 1 from lccontplandutyparam where calfactor ='CalRule' and calfactorvalue ='4' "
											+ " and contplancode not in ('00','11') and grpcontno ='"+mGrpContNo+"'";
        ExeSQL exeSql = new ExeSQL();
        SSRS ssrs = new SSRS();
        ssrs = exeSql.execSQL(strSql);
        if(ssrs.getMaxRow()!=0)
        {
            String tstrSql = "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer'";
            SSRS tssrs = exeSql.execSQL(tstrSql);
            if(!localVersion.equals(tssrs.GetText(1, 1)))
            {
                mErrors.addOneError("该保单下有险种为差异算费，请使用差异增人模板"+tssrs.GetText(1, 1));
                return false;
            }
        }
        else
        {
        	  String tstrSql = "select SysVarValue from LDSysvar where SysVar='NiDiskImportVer2.2'";
            SSRS tssrs = exeSql.execSQL(tstrSql);
            if(!localVersion.equals(tssrs.GetText(1, 1)))
            {
                mErrors.addOneError("导入模板错误请使用普通增人模板"+tssrs.GetText(1, 1));
                return false;
            }
        }

        LCInsuredListSet tLCInsuredListSet = (LCInsuredListSet) importFile.
                                             getSchemaSet();
        LCInsuredListPolSet tLCInsuredListPolSet = (LCInsuredListPolSet)
                importFile.getLCInsuredListPolSet();
        this.mResult.add(tLCInsuredListSet);
				this.mResult.add(tLCInsuredListPolSet);
        //存放Insert Into语句的容器
        MMap map = new MMap();
        if ((mEdorNo != null) && (!mEdorNo.equals(""))) {
            deleteInsuredList(map);
        }
        for (int i = 1; i <= tLCInsuredListSet.size(); i++) {
            //添加一个被保人
            addOneInsured(map, tLCInsuredListSet.get(i), i);
        }
        map.put("delete from LCInsuredListPol where grpcontno ='"+mGrpContNo+"'","DELETE");
        for (int i = 1; i <= tLCInsuredListPolSet.size(); i++) {
            deleteInsuredListPol(map, tLCInsuredListPolSet.get(i).getInsuredID(),
                                 tLCInsuredListPolSet.get(i).getRiskCode());
            addOneInsuredPol(map, tLCInsuredListPolSet.get(i), i);
        }
        //提交数据到数据库
        if (!submitData(map)) {
            return false;
        }
        return true;
    }


    /**
     * 先删除相关的数据
     * @param map MMap
     */
    private void deleteInsuredList(MMap map) {
        String sql = "delete from LCInsuredList " +
                     "where GrpContNo = '" + mGrpContNo + "' " +
                     "and EdorNo = '" + mEdorNo + "' ";
        map.put(sql, "DELETE");
    }

    private void deleteInsuredListPol(MMap map, String InsuredID,
                                      String RiskCode) {
        String sql = "delete from LCInsuredListPol where GrpContNo = '" + mGrpContNo + "' "
                     + " and InsuredID = '" + InsuredID + "' "
                     +	" and RiskCode = '" + RiskCode + "'"
                     ;
        map.put(sql, "DELETE");
    }

    /**
     * 校验导入数据
     * @param cLCInsuredListSet LCInsuredListSet
     * @return boolean
     */
    private boolean checkData(LCInsuredListSet cLCInsuredListSet) {
        for (int i = 1; i <= cLCInsuredListSet.size(); i++) {
            LCInsuredListSchema schema = cLCInsuredListSet.get(i);
//            String occupationType = schema.getOccupationType();
//            if ((occupationType == null) || (occupationType.equals(""))) {
//                mErrors.addOneError("导入错误，缺少职业类别！");
//                return false;
//            }

            String contPlanCode = schema.getContPlanCode();
            if ((contPlanCode == null) || (contPlanCode.equals(""))) {
                mErrors.addOneError("导入错误，缺少保险计划！");
                return false;
            }
        }
        return true;
    }

    /**
     * 添加一个被保人
     * @param map MMap
     * @param aLCInsuredSchema LCInsuredSchema
     */
    private void addOneInsured(MMap map,
                               LCInsuredListSchema cLCInsuredListSchema, int i) {
        LCInsuredListSchema tLCInsuredListSchema = cLCInsuredListSchema;
        if (tLCInsuredListSchema.getContNo() == null ||
            tLCInsuredListSchema.getContNo().equals("")) {
            return;
        }
        tLCInsuredListSchema.setGrpContNo(mGrpContNo);
        tLCInsuredListSchema.setState("0"); //0为未生效，1为有效
        tLCInsuredListSchema.setBatchNo(this.mBatchNo);
        //在磁盘投保时合同号码存储合同id
        if (StrTool.cTrim(tLCInsuredListSchema.getContNo()).equals("")) {
            tLCInsuredListSchema.setContNo(String.valueOf(i));
        }
        String edorValiDate = tLCInsuredListSchema.getEdorValiDate();
        if ((edorValiDate == null) || (edorValiDate.equals(""))) {
            if (mLPGrpEdorItemSchema != null) {
                tLCInsuredListSchema.setEdorValiDate(
                        mLPGrpEdorItemSchema.getEdorValiDate());
            }
        }
        tLCInsuredListSchema.setEdorNo(mEdorNo);
        tLCInsuredListSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListSchema.setMakeDate(mCurrentDate);
        tLCInsuredListSchema.setMakeTime(mCurrentTime);
        tLCInsuredListSchema.setModifyDate(mCurrentDate);
        tLCInsuredListSchema.setModifyTime(mCurrentTime);
        String prem = tLCInsuredListSchema.getPrem();
        if ((prem != null) && (!prem.equals("")) &&
            (Double.parseDouble(prem) > 0)) {
            tLCInsuredListSchema.setPublicAcc(Double.parseDouble(prem));
        }
        //设置计算方向
        if (tLCInsuredListSchema.getPublicAcc() > 0) {
            tLCInsuredListSchema.setCalRule("4");
        }
        map.put(tLCInsuredListSchema, "DELETE&INSERT");
    }

    private void addOneInsuredPol(MMap map,
                                  LCInsuredListPolSchema
                                  cLCInsuredListPolSchema, int i) {
        LCInsuredListPolSchema tLCInsuredListPolSchema =
                cLCInsuredListPolSchema;
        if (tLCInsuredListPolSchema.getContNo() == null ||
            tLCInsuredListPolSchema.getContNo().equals("")) {
            return;
        }
        if (tLCInsuredListPolSchema.getPrem() == null ||
            tLCInsuredListPolSchema.getPrem().equals("")) {
            tLCInsuredListPolSchema.setPrem("0");
        }
        if (tLCInsuredListPolSchema.getAmnt() == null ||
            tLCInsuredListPolSchema.getAmnt().equals("")) {
            tLCInsuredListPolSchema.setAmnt("0");
        }
        String strPrem =
                String.valueOf(Arith.round(Double.parseDouble(
                        tLCInsuredListPolSchema.getPrem()), 2));
        String strAmnt =
                String.valueOf(Arith.round(Double.parseDouble(
                        tLCInsuredListPolSchema.getAmnt()), 2));

        tLCInsuredListPolSchema.setPrem(strPrem);
        tLCInsuredListPolSchema.setAmnt(strAmnt);
        tLCInsuredListPolSchema.setGrpContNo(
                mGrpContNo);
        tLCInsuredListPolSchema.setMakeDate(mCurrentDate);
        tLCInsuredListPolSchema.setMakeTime(mCurrentTime);
        tLCInsuredListPolSchema.setModifyDate(mCurrentDate);
        tLCInsuredListPolSchema.setModifyTime(mCurrentTime);
        tLCInsuredListPolSchema.setOperator(mGlobalInput.Operator);
        tLCInsuredListPolSchema.setPolNo(i + "");
        map.put(tLCInsuredListPolSchema, "DELETE&INSERT");
    }
    
    private String chkOccupation(String nativePlace,String nativecity,String idtype,String idno){
    	String res = "";
    	if(nativePlace==null || "".equals(nativePlace)){
    		res = "国籍不能为空";
    		return res;
    	}
    	if(!"HK".equals(nativePlace)&&!"ML".equals(nativePlace)&&!"OS".equals(nativePlace)){
    		res = "国籍录入错误";
    		return res;
    	}
    	if("HK".equals(nativePlace) || "OS".equals(nativePlace)){
    		if(nativecity==null || "".equals(nativecity)){
    			res = "国家/地区不能为空";
    			return res;
    		}
    		if(!Arrays.asList(OSNATIVECITY).contains(nativecity) && !Arrays.asList(HKNATIVECITY).contains(nativecity)){
    			res = "国家/地区录入错误";
    			return res;
    		}
    		if("HK".equals(nativePlace)){
        		if(!Arrays.asList(HKNATIVECITY).contains(nativecity)){
        			res = "国籍与地区不匹配";
        			return res;
        		}
        	}else if("OS".equals(nativePlace)){
        		if(!Arrays.asList(OSNATIVECITY).contains(nativecity)){
        			res = "国籍与国家不匹配";
        			return res;
        		}
        	}
    	}
    	if("a".equals(idtype)||"b".equals(idtype)){
   		 if(!"HK".equals(nativePlace)){
   			res = "国籍与证件类型不匹配";
   			return res;
   		 }
   		 if("a".equals(idtype)){
				if(!"1".equals(nativecity)&&!"2".equals(nativecity)){
					res = "证件类型和地区不匹配";
					return res;
				}
			}
			if("b".equals(idtype)){
				if(!"3".equals(nativecity)){
					res = "证件类型和地区不匹配";
					return res;	
				}
			}
			if("1".equals(nativecity)){
				if(idno.indexOf("810000")!=0){
					res = "证件号码和地区不匹配";
					return res;
				}
			}
			if("2".equals(nativecity)){
				if(idno.indexOf("820000")!=0){
					res = "证件号码和地区不匹配";
					return res;
				}
			}
			if("3".equals(nativecity)){
				if(idno.indexOf("830000")!=0){
					res = "证件号码和地区不匹配！";
					return res;
				}
			}
   	 	}
    	return res;
    }

    /**
     * 提交数据到数据库
     * @param map MMap
     * @return boolean
     */
    private boolean submitData(MMap map) {
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")) {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 返回执行结果
     * @return VData
     */
    public VData getResult() {
        return this.mResult;
    }

    /**
     * 主函数，测试用
     * @param args String[]
     */
    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.Operator = "endor";
        String grpContNo = "1400003904";
        String path = "Y:\\ui\\temp\\";
        String fileName = "18061117009.xls";
        AddInsuredList tAddInsuredList = new AddInsuredList(grpContNo, tGI);
        if (!tAddInsuredList.doAdd(path, fileName)) {
            System.out.println(tAddInsuredList.mErrors.getFirstError());
            System.out.println("磁盘导入失败！");
        }
    }
}
