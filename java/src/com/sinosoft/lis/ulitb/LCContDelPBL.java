package com.sinosoft.lis.ulitb;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.certify.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LCContDelPBL{
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 往前面传输数据的容器 */
  private VData mResult = new VData();

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  /**业务处理相关变量*/
  private GlobalInput mGlobalInput = new GlobalInput();
  private TransferData mTransferData = new TransferData();
  private LCContSet mLCContSet = new LCContSet();
  LCContSchema mLCContSchema = new LCContSchema();


  public LCContDelPBL()
  {
  }

  public VData getResult()
  {
    return mResult;
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    System.out.println("Start LCContDelPBL  Submit ...");

    mInputData = (VData)cInputData.clone();
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLCContSet.set((LCContSet)cInputData.getObjectByObjectName("LCContSet",0));
    mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
    ContDeleteUI tContDeleteUI = new ContDeleteUI();

    int count =  mLCContSet.size();
    System.out.println(count);

    for(int i=1; i<=count; i++)
    {
      LCContSchema mLCContSchema = new LCContSchema();
      mLCContSchema.setSchema(mLCContSet.get(i));
      String sscontno = mLCContSchema.getContNo();
      System.out.println(sscontno);

      cInputData.clear();
      cInputData.add(mGlobalInput);
      cInputData.add(mLCContSchema);
      cInputData.add(mLCContSet);
      cInputData.add(mTransferData);

      if (tContDeleteUI.submitData(cInputData, cOperate) == false)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tContDeleteUI.mErrors);
          return false;

      }



    }



    return true;

  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "DerferAppF1PUI";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }


























}
