/**
 * 2008-10-25
 */
package com.sinosoft.lis.ulitb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCCoInsuranceParamSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCIGrpContSchema;
import com.sinosoft.lis.vschema.LCCoInsuranceParamSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CoInsuranceGrpContBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private LCGrpContSchema mLCGrpContSchema = null;

    private LCCoInsuranceParamSet mLCCoInsuranceParamSet = null;

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        try
        {
            mOperate = cOperate;

            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            if (mGlobalInput == null)
            {
                buildError("getInputData", "处理超时，请重新登录。");
                return false;
            }

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mTransferData == null)
            {
                buildError("getInputData", "所需参数不完整。");
                return false;
            }

            mLCCoInsuranceParamSet = (LCCoInsuranceParamSet) cInputData
                    .getObjectByObjectName("LCCoInsuranceParamSet", 0);
            //            if (mLCCoInsuranceParamSet == null)
            //            {
            //                buildError("getInputData", "所需共保机构信息不完整。");
            //                return false;
            //            }

            String tGrpContNo = (String) mTransferData
                    .getValueByName("GrpContNo");
            mLCGrpContSchema = getGrpContInfo(tGrpContNo);
            if (mLCGrpContSchema == null)
            {
                buildError("getInputData", "合同号为[" + tGrpContNo + "] 的保单信息不存在。");
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("getInputData", "未知异常：" + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
        MMap tTmpMap = null;

        // 防止并发，加锁。
        tTmpMap = null;
        tTmpMap = lockCont(mLCGrpContSchema);
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------------

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createCoInsuranceInfo();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        if ("Delete".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = delCoInsuranceInfo();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    /**
     * 获取保单信息。
     * @param cGrpContNo 保单合同号
     * @return 如果保单存在返回Schema，如不存在返回null。
     */
    private LCGrpContSchema getGrpContInfo(String cGrpContNo)
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(cGrpContNo);

        if (!tLCGrpContDB.getInfo())
        {
            return null;
        }

        return tLCGrpContDB.getSchema();
    }

    /**
     * 创建共保保单相关信息。
     * @return
     */
    private MMap createCoInsuranceInfo()
    {
        if (!"1".equals(mLCGrpContSchema.getCoInsuranceFlag()))
        {
            buildError("createCoInsuranceInfo", "印刷号为["
                    + mLCGrpContSchema.getPrtNo() + "]单不是共保保单。");
            return null;
        }

        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 首先清除该单全部共保相关信息。
        tTmpMap = delCoInsuranceInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // -------------------------------

        // 创建共保机构要素。
        tTmpMap = createCoInsuranceComParams();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // -------------------------------

        // 创建共保保单状态。
        tTmpMap = createCoInsuranceGrpCont();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // -------------------------------

        return tMMap;
    }

    /**
     * 删除共保保单相关信息。
     * @return
     */
    private MMap delCoInsuranceInfo()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 创建共保机构要素。
        tTmpMap = delCoInsuranceComParams();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // -------------------------------

        // 创建共保保单状态。
        tTmpMap = delCoInsuranceGrpCont();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // -------------------------------

        return tMMap;
    }

    /**
     * 创建共保机构信息。
     * @return
     */
    private MMap createCoInsuranceComParams()
    {
        if (!checkCoInsuranceComParams(mLCCoInsuranceParamSet))
        {
            return null;
        }

        MMap tMMap = new MMap();

        LCCoInsuranceParamSet tLCCoInsuranceParamSet = new LCCoInsuranceParamSet();

        for (int i = 1; i <= mLCCoInsuranceParamSet.size(); i++)
        {
            LCCoInsuranceParamSchema tTmpLCCoInsuranceParamSchema = null;
            tTmpLCCoInsuranceParamSchema = mLCCoInsuranceParamSet.get(i);

            LCCoInsuranceParamSchema tLCCoInsuranceParamSchema = new LCCoInsuranceParamSchema();
            tLCCoInsuranceParamSchema.setGrpContNo(tTmpLCCoInsuranceParamSchema
                    .getGrpContNo());
            tLCCoInsuranceParamSchema
                    .setProposalGrpContNo(tTmpLCCoInsuranceParamSchema
                            .getProposalGrpContNo());
            tLCCoInsuranceParamSchema.setPrtNo(tTmpLCCoInsuranceParamSchema
                    .getPrtNo());

            tLCCoInsuranceParamSchema.setAgentCom(tTmpLCCoInsuranceParamSchema
                    .getAgentCom());
            tLCCoInsuranceParamSchema
                    .setAgentComName(tTmpLCCoInsuranceParamSchema
                            .getAgentComName());
            tLCCoInsuranceParamSchema.setRate(tTmpLCCoInsuranceParamSchema
                    .getRate());

            tLCCoInsuranceParamSchema.setOperator(mGlobalInput.Operator);
            tLCCoInsuranceParamSchema.setMakeDate(mCurrentDate);
            tLCCoInsuranceParamSchema.setMakeTime(mCurrentTime);
            tLCCoInsuranceParamSchema.setModifyDate(mCurrentDate);
            tLCCoInsuranceParamSchema.setModifyTime(mCurrentTime);

            tLCCoInsuranceParamSet.add(tLCCoInsuranceParamSchema);
        }

        mResult.add(tLCCoInsuranceParamSet);
        tMMap.put(tLCCoInsuranceParamSet, SysConst.DELETE_AND_INSERT);

        return tMMap;
    }

    /**
     * 创建共保保单状态信息。
     * @return
     */
    private MMap createCoInsuranceGrpCont()
    {
        MMap tMMap = new MMap();
        String tState = "1";

        LCIGrpContSchema tLCIGrpContSchema = new LCIGrpContSchema();

        tLCIGrpContSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCIGrpContSchema.setProposalGrpContNo(mLCGrpContSchema
                .getProposalGrpContNo());
        tLCIGrpContSchema.setPrtNo(mLCGrpContSchema.getPrtNo());

        tLCIGrpContSchema.setState(tState);

        tLCIGrpContSchema.setOperator(mGlobalInput.Operator);
        tLCIGrpContSchema.setMakeDate(mCurrentDate);
        tLCIGrpContSchema.setMakeTime(mCurrentTime);
        tLCIGrpContSchema.setModifyDate(mCurrentDate);
        tLCIGrpContSchema.setModifyTime(mCurrentTime);

        mResult.add(tLCIGrpContSchema);
        tMMap.put(tLCIGrpContSchema, SysConst.DELETE_AND_INSERT);

        return tMMap;
    }

    /**
     * 清除共保机构信息。
     * @return
     */
    private MMap delCoInsuranceComParams()
    {
        MMap tMMap = new MMap();

        String tGrpContNo = mLCGrpContSchema.getGrpContNo();

        String tStrSql = "delete from LCCoInsuranceParam "
                + " where GrpContNo = '" + tGrpContNo + "'";
        tMMap.put(tStrSql, SysConst.DELETE);

        return tMMap;
    }

    private MMap delCoInsuranceGrpCont()
    {
        MMap tMMap = new MMap();

        String tGrpContNo = mLCGrpContSchema.getGrpContNo();

        String tStrSql = "delete from LCIGrpCont " + " where GrpContNo = '"
                + tGrpContNo + "'";
        tMMap.put(tStrSql, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 校验共保机构代码是否有效。
     * @param cLCCoInsuranceParamSet
     * @return
     */
    private boolean checkCoInsuranceComParams(
            LCCoInsuranceParamSet cLCCoInsuranceParamSet)
    {
        if (cLCCoInsuranceParamSet == null
                && cLCCoInsuranceParamSet.size() == 0)
        {
            buildError("checkCoInsuranceComParams", "所需共保机构信息未填写或不完整。");
            return false;
        }

        for (int i = 1; i <= cLCCoInsuranceParamSet.size(); i++)
        {
            LCCoInsuranceParamSchema tLCCoInsuranceParamSchema = null;
            tLCCoInsuranceParamSchema = cLCCoInsuranceParamSet.get(i);

            String tCIComCode = tLCCoInsuranceParamSchema.getAgentCom();
            String tCIComName = getCoInsuranceComName(tCIComCode);
            if (tCIComName.equals(""))
            {
                buildError("checkCoInsuranceComParams", "[" + tCIComName
                        + "]机构代码不存在");
                return false;
            }
            //            else if (!tCIComName.equals(tLCCoInsuranceParamSchema
            //                    .getAgentComName()))
            //            {
            //                buildError("checkCoInsuranceComParams", "机构代码与机构名称不一致。");
            //                return false;
            //            }

        }
        return true;
    }

    /**
     * 获取机构名称。
     * @param cAgentComCode
     * @return 如机构代码存在，返回机构名称；如不存在，返回“”。
     */
    private String getCoInsuranceComName(String cAgentComCode)
    {
        String tComName = null;

        String tStrSql = " select trim(lac.Name) from LACom lac "
                + " where lac.AgentCom = '" + cAgentComCode + "' ";
        tComName = new ExeSQL().getOneValue(tStrSql);

        return tComName;
    }

    /**
     * 锁定动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(LCGrpContSchema cLCGrpContSchema)
    {
        MMap tMMap = null;

        // 共保信息锁定标志为：“CI”
        String tLockNoType = "CI";
        // -----------------------

        // 锁定有效时间
        String tAIS = "10";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCGrpContSchema
                .getGrpContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ScanDeleBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
