/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.ulitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


/**
 * <p> Title: Web业务系统 </p>
 * <p> Description: UI功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author HST
 * @version 1.0
 * @date 2002-09-25
 */
public class LCRiskZTFeeUI
{
    /** 往前面传输数据的容器 */
   // private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    // @Constructor
    public LCRiskZTFeeUI()
    {}

    // @Method
    /**
     * 数据提交方法
     * @param: cInputData 传入的数据
     *		  cOperate 数据操作字符串
     * @return: boolean
     **/
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mOperate = cOperate;

        LCRiskZTFeeBL tLCRiskZTFeeBL = new LCRiskZTFeeBL();

        System.out.println("---UI BEGIN---");
        if (tLCRiskZTFeeBL.submitData(cInputData, mOperate))
        {
            //mResult = tLCRiskZTFeeBL.getResult();
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCRiskZTFeeBL.mErrors);
            return false;
        }
    }

//    public VData getResult()
//    {
//        return mResult;
//    }

    public static void main(String[] args)
    {
    }
}
