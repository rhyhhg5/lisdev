/*
 * @(#)GrpPolImpInfo.java	2004-12-13
 *
 * Copyright 2004 Sinosoft Co. Ltd. All rights reserved.
 *  All right reserved.
 */

package com.sinosoft.lis.ulitb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;

import java.util.HashMap;
import java.util.Iterator;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 保存及填充磁盘投保过程中需要的一些信息 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author Kevin
 * @version 6.0
 * @date 2003-07-09
 */
public class GrpPolImpInfo
{
    public CErrors mErrors = new CErrors();

    private boolean m_bInited = false;

    private GlobalInput mGlobalInput = null;

    // 同一个印刷号下的所有集体保单的信息
    private LCGrpPolSet m_LCGrpPolSet = new LCGrpPolSet();

    //同一个印刷号下的所有集体保单的险种承保描述信息
    private LMRiskAppSet m_LMRiskAppSet = new LMRiskAppSet();

    //同一个印刷号下的所有集体保单的险种描述信息
    private LMRiskSet m_LMRiskSet = new LMRiskSet();

    //集体信息表
    private LDGrpSet m_LDGrpSet = new LDGrpSet();

    private LCGrpAppntSchema mLCGrpAppntSchema = null;

    private String m_strBatchNo = "";

    private LCGrpContSchema mLCGrpContSchema = null;

    private LCContPlanSet mLCContPlanSet = new LCContPlanSet();

    private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();

    private LCBnfSet mLCBnfSet = new LCBnfSet();

    private LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();

    // 缓存的一些信息
    // 对于有主附险的情况，将主险记录的相关信息缓存，因为在处理附加险的时候
    // 需要主险的保单号等相关的信息
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet(); //保存原始信息

    //保存经过创建或系统查询出来后的被保险人信息
    private HashMap mInsuredMap = new HashMap();

    private HashMap mMainPolMap = new HashMap();

    //保存创建或系统中查询出来的合同信息
    private HashMap mContCache = new HashMap();

    private HashMap mBnfMap = new HashMap();

    private HashMap mContPolData = new HashMap(); //保存以合同为索引的保单信息

    public GrpPolImpInfo()
    {
        //  reset();
    }

    /**
     * 通过磁盘投保文件中的团体保单号来初始化一些信息
     * @param strBatchNo String
     * @param GlobalInput GlobalInput
     * @param tLCInsuredSet LCInsuredSet
     * @param tLCBnfSet LCBnfSet
     * @param tLCInsuredRelatedSet LCInsuredRelatedSet
     * @param tLCGrpContSchema LCGrpContSchema
     * @return boolean
     */
    public boolean init(String strBatchNo, GlobalInput GlobalInput,
            LCInsuredSet tLCInsuredSet, LCBnfSet tLCBnfSet,
            LCInsuredRelatedSet tLCInsuredRelatedSet,
            LCGrpContSchema tLCGrpContSchema)
    {
        mGlobalInput = GlobalInput;

        //清空
        mContCache.clear();
        //  mContCache = new MMap();

        mInsuredMap.clear();
        mMainPolMap.clear();
        //  mContPolData.clear();

        mLCBnfSet = tLCBnfSet;
        mLCInsuredRelatedSet = tLCInsuredRelatedSet;
        if (strBatchNo == null || strBatchNo.equals(""))
        {
            buildError("init", "传入非法的磁盘投保批次号");
            return false;
        }
        m_strBatchNo = strBatchNo;

        //初始化合同id信息
        mLCInsuredSet = tLCInsuredSet;
        String contId = "";

        for (int i = 1; i <= tLCInsuredSet.size(); i++)
        {
            contId = tLCInsuredSet.get(i).getContNo();
            if ("".equals(contId))
            {
                buildError("init", "被保人信息中有没有填写的合同号!");
                return false;
            }
            if (!mContCache.containsKey(contId))
            {
                mContCache.put(contId, null);
            }
        }

        mLCGrpContSchema = tLCGrpContSchema;
        String grpcontNo = mLCGrpContSchema.getGrpContNo();

        if (m_LDGrpSet == null || m_LDGrpSet.size() == 0)
        {
            if (initLDGrp(tLCGrpContSchema.getAppntNo()) == false)
            {
                return false;
            }
        }

        if (mLCGrpAppntSchema == null)
        {
            if (!this.initLDGrpAppnt(tLCGrpContSchema.getGrpContNo(),
                    tLCGrpContSchema.getAppntNo()))
            {
                return false;
            }
        }
        // 根据合同号查询集体保单
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(grpcontNo);
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

        LCGrpPolSchema tLCGrpPolSchema = null;

        //集体险种保单信息
        for (int nIndex = 0; nIndex < tLCGrpPolSet.size(); nIndex++)
        {
            tLCGrpPolSchema = tLCGrpPolSet.get(nIndex + 1);
            isCachedRisk(tLCGrpPolSchema.getRiskCode());

        }

        m_LCGrpPolSet = tLCGrpPolSet;

        //保障计划,保障计划险种计划初始化
        boolean needquery = true;
        for (int t = 1; t <= mLCContPlanSet.size(); t++)
        {
            if (mLCContPlanSet.get(t).getGrpContNo().equals(grpcontNo))
            {
                needquery = false;
            }
        }
        if (needquery)
        {
            queryLCContPlan(grpcontNo);
            for (int t = 1; t <= mLCContPlanSet.size(); t++)
            {
                this.queryLCContPlanRisk(grpcontNo, mLCContPlanSet.get(t)
                        .getContPlanCode());
            }
        }
        return true;
    }

    public LCInsuredSet getLCInsuredSet()
    {
        return this.mLCInsuredSet;
    }

    /**
     * 根据合同号移除已经缓存的合同 ， 被保险人 和 主险保单
     * @param contId String
     * @return boolean
     */
    public boolean removeCaceh(String contId)
    {
        //本部分代码没有测过
        //移除缓存合同

        this.mContCache.put(contId, null);
        String polKey = "";
        Iterator it = null;
        //移除被保险人,移除主险保单，记录下金额
        LCPolSchema tPol = null;
        for (int i = 1; i <= this.mLCInsuredSet.size(); i++)
        {
            //String insuredId = this.mLCInsuredSet.get(i).getInsuredNo() ;
            if (contId.equals(this.mLCInsuredSet.get(i).getContNo()))
            {
                polKey = contId + "-"
                        + this.mLCInsuredSet.get(i).getInsuredNo();
                this.mInsuredMap.remove(this.mLCInsuredSet.get(i)
                        .getInsuredNo());
                it = this.mMainPolMap.keySet().iterator();
                String key = "";

                while (it.hasNext())
                {
                    key = (String) it.next();
                    if (key.indexOf("-") > 0)
                    {
                        if (polKey.equals(key
                                .substring(0, key.lastIndexOf("-"))))
                        {
                            tPol = (LCPolSchema) this.mMainPolMap.get(key);
                            if (tPol != null)
                            {
                                this.substractMoney(tPol.getRiskCode(), tPol
                                        .getPrem(), tPol.getAmnt());
                                this.mMainPolMap.remove(key);
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * 根据险种代码查找出对应的集体险种保单，减去传入的保费保额
     * @param riskcode String
     * @param prem double
     * @param amnt double
     */
    private void substractMoney(String riskcode, double prem, double amnt)
    {
        LCGrpPolSchema grpPol = this.findLCGrpPolSchema(riskcode);
        double tPrem = grpPol.getPrem() - prem;
        if (tPrem < 0)
        {
            tPrem = 0;
        }
        double tAmnt = grpPol.getAmnt() - amnt;
        if (tAmnt < 0)
        {
            tAmnt = 0;
        }

        grpPol.setPrem(tPrem);
        grpPol.setAmnt(tAmnt);
        this.mLCGrpContSchema.setPrem(mLCGrpContSchema.getPrem() - tPrem);
        this.mLCGrpContSchema.setAmnt(mLCGrpContSchema.getAmnt() - tAmnt);
    }

    /**
     * 根据合同ID,主被保险人ID,险种代码查找连身被保险人信息
     * @param contId String
     * @param mainInsured String
     * @param riskcode String
     * @return String[]
     */
    public String[] getInsuredRela(String contId, String mainInsured,
            String riskcode)
    {
        StringBuffer sb = null;
        for (int i = 1; i <= mLCInsuredRelatedSet.size(); i++)
        {
            if (contId.equals(mLCInsuredRelatedSet.get(i).getOperator())
                    && mainInsured.equals(mLCInsuredRelatedSet.get(i)
                            .getMainCustomerNo()) //借用段
                    && riskcode.equals(mLCInsuredRelatedSet.get(i).getPolNo()))
            {
                if (sb == null)
                {
                    sb = new StringBuffer();

                }
                else
                {
                    sb.append(",");
                }
                sb.append(mLCInsuredRelatedSet.get(i).getCustomerNo());
            }

        }
        if (sb != null)
        {
            return sb.toString().split(",");
        }
        return null;
    }

    /**
     * 通过险种索引获取受益人信息，在有计划磁盘投保时使用
     * @param strPolId String
     * @return LCBnfSet
     */
    public LCBnfSet getBnfSet(String strPolId)
    {
        LCBnfSet tBnfSet = new LCBnfSet();
        LCBnfSchema tSchema = null;
        for (int i = 1; i <= this.mLCBnfSet.size(); i++)
        {
            tSchema = mLCBnfSet.get(i);
            if (getPolKey(tSchema.getContNo(), tSchema.getInsuredNo(),
                    tSchema.getPolNo()).equals(strPolId))
            {
                LCBnfSchema newSchema = new LCBnfSchema();
                newSchema.setSchema(tSchema);
                tBnfSet.add(newSchema);
            }
        }
        return tBnfSet;
    }

    /**
     * 通过险种唯一ID查找受益人信息。在以险种保单为线索导入的时候使用
     * @param strPolId String  险种ID
     * @return LCBnfSet
     */
    public LCBnfSet getBnfSetByPolId(String strPolId)
    {
        LCBnfSet tBnfSet = new LCBnfSet();
        LCBnfSchema tSchema = null;
        int polId = this.parseId(strPolId);
        for (int i = 1; i <= this.mLCBnfSet.size(); i++)
        {
            tSchema = mLCBnfSet.get(i);
            //险种ID 被缓存在 BnfNo
            if (tSchema.getBnfNo() == polId)
            {
                LCBnfSchema newSchema = new LCBnfSchema();
                newSchema.setSchema(tSchema);
                tBnfSet.add(newSchema);
            }
        }
        return tBnfSet;

    }

    public LCGrpContSchema getLCGrpContSchema()
    {
        return this.mLCGrpContSchema;
    }

    /**
     * 生成险种保单索引
     * @param contId String
     * @param insuredId String
     * @param riskcode String
     * @return String
     */
    public String getPolKey(String contId, String insuredId, String riskcode)
    {
        return contId + "-" + insuredId + "-" + riskcode;
    }

    /**
     * 查某一个险种保险计划
     * @param riskcode String
     * @param contPlanCode String
     * @return LCContPlanRiskSchema
     */
    public LCContPlanRiskSchema findLCContPlanRiskSchema(String riskcode,
            String contPlanCode)
    {
        for (int i = 1; i < mLCContPlanRiskSet.size(); i++)
        {
            if (mLCContPlanRiskSet.get(i).getRiskCode().equals(riskcode)
                    && mLCContPlanRiskSet.get(i).getContPlanCode().equals(
                            contPlanCode))
            {
                return mLCContPlanRiskSet.get(i);
            }
        }
        return null;
    }

    /**
     * 从缓存中查找保险计划代码对应的所有险种保险计划
     * @param contPlanCode String
     * @return LCContPlanRiskSet
     */
    public LCContPlanRiskSet findLCContPlanRiskSet(String contPlanCode)
    {
        LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
        for (int i = 1; i <= mLCContPlanRiskSet.size(); i++)
        {
            if (mLCContPlanRiskSet.get(i).getContPlanCode()
                    .equals(contPlanCode))
            {
                tLCContPlanRiskSet.add(mLCContPlanRiskSet.get(i));
            }
        }
        return tLCContPlanRiskSet;
    }

    /**
     * 查询保险计划下所有险种保险计划
     *
     * @param grpContNo String
     * @param contPlanCode String
     */
    private void queryLCContPlanRisk(String grpContNo, String contPlanCode)
    {
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        tLCContPlanRiskDB.setGrpContNo(grpContNo);
        tLCContPlanRiskDB.setContPlanCode(contPlanCode);
        mLCContPlanRiskSet.add(tLCContPlanRiskDB.query());

    }

    /**
     * 查找保险计划
     * @param contPlanCode String
     * @return LCContPlanSchema
     */
    public LCContPlanSchema findLCContPlan(String contPlanCode)
    {
        for (int i = 1; i <= mLCContPlanSet.size(); i++)
        {
            if (mLCContPlanSet.get(i).getContPlanCode().equals(contPlanCode))
            {
                return mLCContPlanSet.get(i);
            }
        }
        return null;

    }

    /**
     * 查询保险计划
     *
     * @param grpContNo String
     */
    public void queryLCContPlan(String grpContNo)
    {
        LCContPlanDB tContPlanDB = new LCContPlanDB();
        tContPlanDB.setGrpContNo(grpContNo);
        mLCContPlanSet.add(tContPlanDB.query());
    }

    /**
     * 格式化个人保单信息。从磁盘投保文件中得到的个人保单信息，
     * 没有主险保单号，集体保单号等信息，需要重新设置。
     *
     * @param tLCPolSchema 险种信息
     * @param strID 险种ID
     * @return com.sinosoft.utility.VData
     */
    public VData formatLCPol(LCPolSchema tLCPolSchema, String strID)
    {
        VData tReturnData = new VData();
        String strRiskCode = tLCPolSchema.getRiskCode();
        LCInsuredSchema tLCInsuredSchema = null;
        LCGrpPolSchema tLCGrpPolSchema = findLCGrpPolSchema(strRiskCode);

        if (tLCGrpPolSchema == null)
        {
            buildError("formatLCPol", "找不到指定险种所对应的集体保单，险种为：" + strRiskCode);
            return null;
        }

        //校验险种是否存在，能不能挂在其指定主险中
        tLCPolSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
        tLCPolSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        //        if ("1".equals(tLCPolSchema.getSpecifyValiDate()) &&
        //            tLCPolSchema.getCValiDate() != null) {
        //            //如果磁盘倒入指定生效日期，且生效日期字段有值,那么就用 生效日期字段的值
        //        } else {
        //            //否则一律用集体单的生效日期
        //            tLCPolSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
        //        }

        tLCPolSchema.setManageCom(tLCGrpPolSchema.getManageCom());
        tLCPolSchema.setSaleChnl(tLCGrpPolSchema.getSaleChnl());
        tLCPolSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
        tLCPolSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
        tLCPolSchema.setAgentType(tLCGrpPolSchema.getAgentType());
        tLCPolSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
        tLCPolSchema.setAgentCode1(tLCGrpPolSchema.getAgentCode1());
        tLCPolSchema.setAppntName(tLCGrpPolSchema.getGrpName());
        tLCPolSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
        tLCPolSchema.setPolApplyDate(this.mLCGrpContSchema.getPolApplyDate());
        String mainPolId = StrTool.cTrim(tLCPolSchema.getMainPolNo());
        // String mainRiskCode = tLCPolSchema.getRiskCode();
        if (!"".equals(mainPolId) && !strID.equals(mainPolId))
        {
            //如果不是主险保单，则查找
            LCPolSchema tMainLCPolSchema = findCacheLCPolSchema(mainPolId);
            //如果从导入轨迹中没有找到
            if (tMainLCPolSchema == null)
            {
                buildError("formatLCPol", "数据库和导入轨迹中找不到附加险被保人(" + mainPolId
                        + ")对应的个人主险保单纪录");

                return null;
            }
            // mainRiskCode= tMainLCPolSchema.getRiskCode();
            tLCPolSchema.setMainPolNo(tMainLCPolSchema.getPolNo());
            tLCPolSchema.setContNo(tMainLCPolSchema.getContNo().trim());
            //被保险人也应该存在
            String insuredIndex = tLCPolSchema.getInsuredNo();
            if (mainPolId.indexOf("-") > 0)
            {
                String[] arr = mainPolId.split("-");

                insuredIndex = arr[1];
            }
            //附加险的被保人即是主险的被保人
            tLCInsuredSchema = this.findInsuredfromCache(insuredIndex);
            if (tLCInsuredSchema == null)
            {
                mErrors.clearErrors();
                buildError("formatLCPol", "新增附加险:找不到对应主险的被保人信息,ID="
                        + insuredIndex);
                return null;
            }

        }
        else
        {

            //本身为主险保单,设置主险保单号空，查找或创建被保险人
            tLCPolSchema.setMainPolNo("");
            tLCInsuredSchema = this.findInsuredfromCache(tLCPolSchema
                    .getInsuredNo());
            if (tLCInsuredSchema == null)
            {
                //需要创建新被保险人
                VData tData = this.getContData(tLCPolSchema.getContNo(),
                        tLCPolSchema.getInsuredNo(), tLCGrpPolSchema, null,null);
                if (tData == null)
                {
                    return null;
                }

                MMap map = (MMap) tData.getObjectByObjectName("MMap", 0);
                if (map != null && map.keySet().size() > 0)
                {
                    tReturnData.add(map);
                }
                tLCInsuredSchema = (LCInsuredSchema) tData
                        .getObjectByObjectName("LCInsuredSchema", 0);

            }
        }

        tLCPolSchema.setContNo(tLCInsuredSchema.getContNo().trim());
        tLCPolSchema.setInsuredSex(tLCInsuredSchema.getSex());
        tLCPolSchema.setOccupationType(tLCInsuredSchema.getOccupationType()); //
        tLCPolSchema.setInsuredBirthday(tLCInsuredSchema.getBirthday());
        tLCPolSchema.setInsuredName(tLCInsuredSchema.getName());
        tLCPolSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());

        tReturnData.add(tLCPolSchema);

        return tReturnData;
    }

    /**
     * 通过计划附加险查找主险Id
     *
     * @param planId String
     * @param riskcode String
     * @return String
     */
    public String getMainRiskCode(String planId, String riskcode)
    {
        LCContPlanRiskSchema schema = null;
        String mainRiskCode = "";
        for (int i = 1; i <= this.mLCContPlanRiskSet.size(); i++)
        {
            schema = mLCContPlanRiskSet.get(i);
            if (schema.getContPlanCode().equals(planId)
                    && schema.getRiskCode().equals(riskcode))
            {
                // return this.mLCContPlanRiskSet.get(i).getMainRiskCode()
                if (schema.getMainRiskCode().equals("000000")
                        || "".equals(StrTool.cTrim(schema.getMainRiskCode())))
                {
                    mainRiskCode = riskcode;
                }
                else
                {
                    mainRiskCode = schema.getMainRiskCode();
                }
                break;
            }
        }
        return mainRiskCode;
    }

    /**
     * 从被保险人咧表中查找到id号一样的被保险人，创建连身被保险人信息。
     * @param mainInsuredNo String
     * @param relaIns String[]
     * @return LCInsuredRelatedSet
     */
    public LCInsuredRelatedSet getInsuredRelaData(String mainInsuredNo,
            String[] relaIns)
    {
        LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();
        //  LCInsuredSchema mainInsured = this.findInsuredfromCache(mainInsureId);
        //  if ( mainInsured ==null ){
        //    CError.buildErr(this, "查找主被保险人["+ mainInsured +"]信息出错!");
        //    return null;
        //  }
        for (int i = 0; i < relaIns.length; i++)
        {
            String tmpId = relaIns[i];
            LCInsuredSchema tSchema = this.findInsuredfromCache(tmpId);
            if (tSchema == null)
            {
                //需要创建新的LCInsuredSchema
                VData tInsData = this.getInsuredData(tmpId);
                if (tInsData == null)
                {
                    CError.buildErr(this, "在准备连身被保险人[" + tmpId + "]的时候出现错误");
                    return null;
                }

                tSchema = (LCInsuredSchema) tInsData.getObjectByObjectName(
                        "LCInsuredSchema", 0);
            }
            if (tSchema != null)
            {
                LCInsuredRelatedSchema tRelaSchema = new LCInsuredRelatedSchema();
                tRelaSchema.setCustomerNo(tSchema.getInsuredNo());
                tRelaSchema.setName(tSchema.getName());
                tRelaSchema.setIDNo(tSchema.getIDNo());
                tRelaSchema.setIDType(tSchema.getIDType());
                tRelaSchema.setMainCustomerNo(mainInsuredNo);
                tRelaSchema.setAddressNo(tSchema.getAddressNo());
                //  tRelaSchema.setPolNo();
                tRelaSchema.setSex(tSchema.getSex());
                tRelaSchema.setRelationToInsured(tSchema
                        .getRelationToMainInsured());
                tRelaSchema.setOperator(mGlobalInput.Operator);
                tRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tRelaSchema.setModifyDate(tRelaSchema.getMakeDate());
                tRelaSchema.setModifyTime(tRelaSchema.getMakeTime());
                tRelaSchema.setBirthday(tSchema.getBirthday());
                tLCInsuredRelatedSet.add(tRelaSchema);
            }

        }
        return tLCInsuredRelatedSet;
    }

    /**
     * 缓存数据准备成功的险种保单信息
     * @param strID String
     * @param aLCPolSchema LCPolSchema
     */
    public void cachePolInfo(String strID, LCPolSchema aLCPolSchema)
    {

        mMainPolMap.put(strID, aLCPolSchema);
    }

    /**
     * 通过主险保单号查询保单
     * @param mainPolNo String
     * @return LCPolSchema
     */
    public LCPolSchema findMainPolSchema(String mainPolNo)
    {
        Iterator it = mMainPolMap.keySet().iterator();
        LCPolSchema tmpSchema;
        while (it.hasNext())
        {
            tmpSchema = (LCPolSchema) mMainPolMap.get(it.next());
            if (tmpSchema.getPolNo().equals(mainPolNo))
            {
                return tmpSchema;
            }

        }
        return null;
    }

    /**
     * 缓存创建的合同信息
     * @param strID String
     * @param aLCContSchema LCContSchema
     */
    public void cacheLCContSchema(String strID, LCContSchema aLCContSchema)
    {
        mContCache.put(strID, aLCContSchema);
    }

    /**
     * 缓存被保险人信息，该被保险人被创建或查询出具体信息
     * @param strID String
     * @param schema LCInsuredSchema
     */
    private void cacheLCInsuredSchema(String strID, LCInsuredSchema schema)
    {
        mInsuredMap.put(strID, schema);
    }

    private LCPolSchema findCacheLCPolSchema(String strID)
    {
        return (LCPolSchema) this.mMainPolMap.get(strID);
    }

    /**
     * 根据strID获取被保险人信息，该被保险人被创建或查询出具体信息
     * @param strID String
     * @return LCInsuredSchema
     */
    public LCInsuredSchema findInsuredfromCache(String strID)
    {
        return (LCInsuredSchema) mInsuredMap.get(strID);
    }

    /**
     * 从缓存中获取合同信息
     * @param strID String
     * @return LCContSchema
     */
    public LCContSchema findLCContfromCache(String strID)
    {
        Object dest = mContCache.get(strID);
        if (dest == null)
        {
            return null;
        }
        if (dest.getClass().getName().equals("java.lang.String"))
        {
            return null;
        }
        return (LCContSchema) dest;
    }

    /**
     * 从缓存中根据id号获取未被修改的被保险人信息
     * @param index int
     * @return LCInsuredSchema
     */
    private LCInsuredSchema findInsured(String index)
    {
        //        if (index < 0 || index > mLCInsuredSet.size())return null;
        //        return mLCInsuredSet.get(index);
        for (int i = 1; i <= mLCInsuredSet.size(); i++)
        {
            if (mLCInsuredSet.get(i).getPrtNo().equals(index))
            {
                return mLCInsuredSet.get(i);
            }
        }
        return null;
    }

    /**
     * 记录失败导入的日志,待完成
     *
     * @param batchNo String
     * @param grpContNo String
     * @param contId String
     * @param insuredId String
     * @param polId String
     * @param contPlanCode String
     * @param riskCode String
     * @param insuredSchema LCInsuredSchema
     * @param errInfo CErrors
     * @param globalInput GlobalInput
     * @return boolean
     */
    public boolean logError(String batchNo, String grpContNo, String contId,
            String insuredId, String polId, String contPlanCode,
            String riskCode, LCInsuredSchema insuredSchema, CErrors errInfo,
            GlobalInput globalInput)
    {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
        LCGrpImportLogSchema delLCGrpImportLogSchema = new LCGrpImportLogSchema();
        // 设置主键
        delLCGrpImportLogSchema.setContID(contId);
        delLCGrpImportLogSchema.setBatchNo(batchNo);

        LCGrpImportLogSchema tLCGrpImportLogSchema = new LCGrpImportLogSchema();
        //避免报错
        if (insuredSchema == null)
        {
            insuredSchema = new LCInsuredSchema();
        }
        // 设置主键
        tLCGrpImportLogSchema.setIdNo(polId);
        tLCGrpImportLogSchema.setContID(contId);
        tLCGrpImportLogSchema.setBatchNo(batchNo);
        tLCGrpImportLogSchema.setRiskCode(riskCode);
        tLCGrpImportLogSchema.setGrpContNo(grpContNo);
        tLCGrpImportLogSchema.setInsuredID(insuredId);
        tLCGrpImportLogSchema.setContPlanCode(contPlanCode);
        tLCGrpImportLogSchema.setInsuredNo(insuredSchema.getInsuredNo());
        tLCGrpImportLogSchema.setInsuredName(insuredSchema.getName());
        tLCGrpImportLogSchema.setOperator(globalInput.Operator);
        tLCGrpImportLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpImportLogSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpImportLogSchema.setErrorType("1");
        tLCGrpImportLogSchema.setErrorState("1");
        String errMess = "";
        for (int i = 0; i < errInfo.getErrorCount(); i++)
        {
            errMess += errInfo.getError(i).errorMessage + ";";
        }

        if ("".equals(errMess))
        {
            errMess = "未捕捉到的错误。";
        }
        errMess.replaceAll("\n", "");
        tLCGrpImportLogSchema.setErrorInfo(errMess);

        tLCGrpImportLogDB.setSchema(delLCGrpImportLogSchema);
        boolean res = true;
        res = tLCGrpImportLogDB.delete();
        if (res)
        {
            tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
            res = tLCGrpImportLogDB.insert();
        }
        return res;
    }

    /**
     * 记录成功导入的信息,待完成
     *
     * @param batchNo String
     * @param grpContNo String
     * @param contId String
     * @param prtNo String
     * @param contNo String
     * @param globalInput GlobalInput
     * @return MMap
     */
    public MMap logSucc(String batchNo, String grpContNo, String contId,
            String prtNo, String contNo, GlobalInput globalInput)
    {
        MMap tmpMap = new MMap();
        LCGrpImportLogSchema delLCGrpImportLogSchema = new LCGrpImportLogSchema();

        // 设置主键
        delLCGrpImportLogSchema.setContID(contId);
        delLCGrpImportLogSchema.setBatchNo(batchNo);
        tmpMap.put(delLCGrpImportLogSchema, "DELETE");

        LCGrpImportLogSchema tLCGrpImportLogSchema = new LCGrpImportLogSchema();
        tLCGrpImportLogSchema.setContID(contId);
        tLCGrpImportLogSchema.setBatchNo(batchNo);
        tLCGrpImportLogSchema.setGrpContNo(grpContNo);
        tLCGrpImportLogSchema.setContNo(contNo);
        tLCGrpImportLogSchema.setOperator(globalInput.Operator);
        tLCGrpImportLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpImportLogSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpImportLogSchema.setErrorType("1");
        tLCGrpImportLogSchema.setErrorState("0");
        tLCGrpImportLogSchema.setPrtNo(prtNo);
        tLCGrpImportLogSchema.setErrorInfo("导入成功");
        tmpMap.put(tLCGrpImportLogSchema, "INSERT");

        return tmpMap;

    }

    /**
     * 查询系统中是否已经有改日志信息
     *
     * @param batchNo String
     * @param contId String
     * @return boolean
     */
    public LCGrpImportLogSchema getLogInfo(String batchNo, String contId)
    {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
        tLCGrpImportLogDB.setBatchNo(batchNo);
        tLCGrpImportLogDB.setContID(contId);
        boolean bExist = tLCGrpImportLogDB.getInfo();
        if (bExist)
        {
            return tLCGrpImportLogDB.getSchema();
        }
        return null;
    }

    /**
     * 写日志信息
     * @param strID String
     * @param aLCInsuredSchema LCInsuredSchema
     * @param bSucc boolean
     * @param errInfo CErrors
     * @param globalInput GlobalInput
     */
    //    public void logError(String strID, LCInsuredSchema aLCInsuredSchema,
    //                         boolean bSucc, CErrors errInfo,
    //                         GlobalInput globalInput)
    //    {
    //        LCGrpImportLogSchema tLCGrpImportLogSchema = new LCGrpImportLogSchema();
    //
    //        // 设置主键
    //        tLCGrpImportLogSchema.setIDNo(strID);
    //        tLCGrpImportLogSchema.setBatchNo(m_strBatchNo);
    //
    //        String strRiskCode = strID.substring(strID.lastIndexOf("-"));
    //        String plancode = aLCInsuredSchema.getContPlanCode();
    //        tLCGrpImportLogSchema.setRiskCode(plancode);
    //        LCGrpPolSchema tLCGrpPolSchema = findLCGrpPolSchema(strRiskCode);
    //        tLCGrpImportLogSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
    //
    //        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
    //        tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
    //
    //        boolean bExist = tLCGrpImportLogDB.getInfo();
    //        String strState = tLCGrpImportLogDB.getErrorState();
    //
    //        if (bExist)
    //        {
    //            tLCGrpImportLogSchema.setSchema(tLCGrpImportLogDB);
    //            tLCGrpImportLogDB.delete();
    //        }
    //
    //        tLCGrpImportLogDB = new LCGrpImportLogDB();
    //        tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
    //
    //        tLCGrpImportLogDB.setOperator(globalInput.Operator);
    //        tLCGrpImportLogDB.setMakeDate(PubFun.getCurrentDate());
    //        tLCGrpImportLogDB.setMakeTime(PubFun.getCurrentTime());
    //        tLCGrpImportLogDB.setErrorType("1");
    //
    //        if (bSucc)
    //        {
    //            tLCGrpImportLogDB.setErrorState("0");
    //            tLCGrpImportLogDB.setPolNo(aLCInsuredSchema.getContNo());
    //            tLCGrpImportLogDB.setInsuredNo(aLCInsuredSchema.getInsuredNo());
    //            tLCGrpImportLogDB.setPrtNo(tLCGrpPolSchema.getPrtNo());
    //        }
    //        else
    //        {
    //            if (bExist)
    //            {
    //                tLCGrpImportLogDB.setErrorState(strState);
    //            }
    //            else
    //            {
    //                tLCGrpImportLogDB.setErrorState("1");
    //            }
    //            tLCGrpImportLogDB.setErrorInfo(errInfo.getFirstError());
    //        }
    //
    //        tLCGrpImportLogDB.insert();
    //
    //    }
    /**
     * 写日志信息
     * @param strID String
     * @param aLCPolSchema LCPolSchema
     * @param bSucc boolean
     * @param errInfo CErrors
     * @param globalInput GlobalInput
     */
    //    public void logError(String strID,
    //                         LCPolSchema aLCPolSchema,
    //                         boolean bSucc,
    //                         CErrors errInfo,
    //                         GlobalInput globalInput)
    //    {
    //
    //        LCGrpImportLogSchema tLCGrpImportLogSchema = new LCGrpImportLogSchema();
    //
    //        // 设置主键
    //        tLCGrpImportLogSchema.setIDNo(strID);
    //        tLCGrpImportLogSchema.setBatchNo(m_strBatchNo);
    //
    //        String strRiskCode = aLCPolSchema.getRiskCode();
    //        tLCGrpImportLogSchema.setRiskCode(strRiskCode);
    //
    //        LCGrpPolSchema tLCGrpPolSchema = findLCGrpPolSchema(strRiskCode);
    //        tLCGrpImportLogSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
    //
    //        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();
    //        tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
    //
    //        boolean bExist = tLCGrpImportLogDB.getInfo();
    //        String strState = tLCGrpImportLogDB.getErrorState();
    //
    //        if (bExist)
    //        {
    //            tLCGrpImportLogSchema.setSchema(tLCGrpImportLogDB);
    //            tLCGrpImportLogDB.delete();
    //        }
    //
    //        tLCGrpImportLogDB = new LCGrpImportLogDB();
    //        tLCGrpImportLogDB.setSchema(tLCGrpImportLogSchema);
    //
    //        tLCGrpImportLogDB.setOperator(globalInput.Operator);
    //        tLCGrpImportLogDB.setMakeDate(PubFun.getCurrentDate());
    //        tLCGrpImportLogDB.setMakeTime(PubFun.getCurrentTime());
    //        tLCGrpImportLogDB.setErrorType("1");
    //
    //        if (bSucc)
    //        {
    //            tLCGrpImportLogDB.setErrorState("0");
    //            tLCGrpImportLogDB.setPolNo(aLCPolSchema.getPolNo());
    //            tLCGrpImportLogDB.setInsuredNo(aLCPolSchema.getInsuredNo());
    //            tLCGrpImportLogDB.setPrtNo(aLCPolSchema.getPrtNo());
    //        }
    //        else
    //        {
    //            if (bExist)
    //            {
    //                tLCGrpImportLogDB.setErrorState(strState);
    //            }
    //            else
    //            {
    //                tLCGrpImportLogDB.setErrorState("1");
    //            }
    //            tLCGrpImportLogDB.setErrorInfo(errInfo.getFirstError());
    //        }
    //
    //        tLCGrpImportLogDB.insert();
    //    }
    /**
     * 获取错误信息
     * @return LCGrpImportLogSet
     */
    public LCGrpImportLogSet getErrors()
    {
        LCGrpImportLogDB tLCGrpImportLogDB = new LCGrpImportLogDB();

        String strSQL = "SELECT * FROM LCGrpImportLog" + " WHERE BatchNo = '"
                + m_strBatchNo + "'"
                + " AND ( ErrorState = '1' OR ErrorInfo IS NOT NULL )"
                + " ORDER BY TO_NUMBER(IDNo)";

        return tLCGrpImportLogDB.executeQuery(strSQL);
    }

    /**
     * 创建错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "GrpPolImpInfo";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 判断险种描述是否已经在缓存中
     * 如果不在，从库中查找并缓存
     * 查找失败返回false
     * @param strRiskCode String
     * @return int
     */
    private boolean isCachedRisk(String strRiskCode)
    {

        LMRiskAppSchema tRiskAppSchema = this.findLMRiskAppSchema(strRiskCode);
        if (tRiskAppSchema == null)
        {
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(strRiskCode);
            if (!tLMRiskAppDB.getInfo())
            {
                mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
                return false;
            }
            tRiskAppSchema = tLMRiskAppDB.getSchema();
            m_LMRiskAppSet.add(tRiskAppSchema.getSchema());

            if (initLMRisk(strRiskCode) == false)
            {
                return false;
            }
        }

        return true;
        //    if (tRiskAppSchema.getSubRiskFlag() != null
        //        && tRiskAppSchema.getSubRiskFlag().equals("M")) {
        //      return 1;
        //    }
        //    else {
        //      return 0;
        //    }
    }

    /**
     * 初始化险种描述数据信息
     *
     * @param strRiskCode String
     * @return boolean
     */
    private boolean initLMRisk(String strRiskCode)
    {
        LMRiskDB tLMRiskDB = new LMRiskDB();

        tLMRiskDB.setRiskCode(strRiskCode);

        if (!tLMRiskDB.getInfo())
        {
            mErrors.copyAllErrors(tLMRiskDB.mErrors);
            return false;
        }

        m_LMRiskSet.add(tLMRiskDB.getSchema());
        return true;
    }

    /**
     * 初始化集体单位信息
     * @param custNo String
     * @return boolean
     */
    private boolean initLDGrp(String custNo)
    {
        LDGrpDB tLDGrpDB = new LDGrpDB();

        tLDGrpDB.setCustomerNo(custNo);

        if (tLDGrpDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDGrpDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "dealDataPerson";
            tError.errorMessage = "LDGrp表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        m_LDGrpSet.add(tLDGrpDB.getSchema());
        return true;
    }

    /**
     * 出始化集体投保人信息，并缓存
     * @param grpContNo String
     * @param custNo String
     * @return boolean
     */
    private boolean initLDGrpAppnt(String grpContNo, String custNo)
    {
        LCGrpAppntDB tLDGrpAppntDB = new LCGrpAppntDB();

        tLDGrpAppntDB.setCustomerNo(custNo);
        tLDGrpAppntDB.setGrpContNo(grpContNo);
        if (tLDGrpAppntDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDGrpAppntDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "dealDataPerson";
            tError.errorMessage = "LDGrp表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //  m_LDGrpSet.add(tLDGrpDB.getSchema());
        this.mLCGrpAppntSchema = tLDGrpAppntDB.getSchema();
        return true;
    }

    public LCGrpAppntSchema getLCGrpAppntSchema()
    {
        return this.mLCGrpAppntSchema;
    }

    /**
     * 根据个人保单的险种编码找到对应的集体保单
     *
     * @param strRiskCode String
     * @return com.sinosoft.lis.schema.LCGrpPolSchema
     */
    public LCGrpPolSchema findLCGrpPolSchema(String strRiskCode)
    {
        for (int nIndex = 0; nIndex < m_LCGrpPolSet.size(); nIndex++)
        {
            if (m_LCGrpPolSet.get(nIndex + 1).getRiskCode().equals(strRiskCode))
            {
                return m_LCGrpPolSet.get(nIndex + 1);
            }
        }

        return null;
    }

    //houzm add
    /**
     * 根据个人保单的险种编码找到对应的集体保单
     *
     * @param strRiskCode String
     * @return com.sinosoft.lis.schema.LMRiskAppSchema
     */
    public LMRiskAppSchema findLMRiskAppSchema(String strRiskCode)
    {
        LMRiskAppSchema tLMRiskAppSchema = null;

        for (int nIndex = 0; nIndex < m_LMRiskAppSet.size(); nIndex++)
        {
            tLMRiskAppSchema = m_LMRiskAppSet.get(nIndex + 1);

            if (tLMRiskAppSchema.getRiskCode().equals(strRiskCode))
            {
                return tLMRiskAppSchema;
            }
        }

        return null;
    }

    /**
     * 根据个人保单的险种编码找到对应的集体保单
     *
     * @param strRiskCode String
     * @return com.sinosoft.lis.schema.LMRiskSchema
     */
    public LMRiskSchema findLMRiskSchema(String strRiskCode)
    {
        LMRiskSchema tLMRiskSchema = null;

        for (int nIndex = 0; nIndex < m_LMRiskSet.size(); nIndex++)
        {
            tLMRiskSchema = m_LMRiskSet.get(nIndex + 1);

            if (tLMRiskSchema.getRiskCode().equals(strRiskCode))
            {
                return tLMRiskSchema;
            }
        }

        return null;
    }

    /**
     * 查找集体单位信息
     * @param GrpNo String
     * @return LDGrpSchema
     */
    public LDGrpSchema findLDGrpSchema(String GrpNo)
    {
        LDGrpSchema tLDGrpSchema = null;
        for (int nIndex = 0; nIndex < m_LDGrpSet.size(); nIndex++)
        {
            tLDGrpSchema = m_LDGrpSet.get(nIndex + 1);
            if (tLDGrpSchema.getCustomerNo().equals(GrpNo))
            {
                return tLDGrpSchema;
            }

        }
        return null;
    }

    /**
     *获取缓存的合同信息
     * @return HashMap
     */
    public HashMap getContCache()
    {
        return this.mContCache;
    }

    /**
     * 合同信息查找或创建，涉及到被保险人 当被保险人不存在的时候;创建被保险人，存在则取用
     * 合同不存在，创建;存在，取用
     *
     * @param contId LCPolSchema
     * @param insuredId LCInsuredSchema
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @param tLCInsuredListSchema LCInsuredListSchema
     * @return boolean
     */
    public VData getContData(String contId, String insuredId,
            LCGrpPolSchema tLCGrpPolSchema, LCInsuredListSet tLCInsuredListSet,LCGrpContSchema mLCGrpContSchema)
    {
        LCInsuredListSchema tLCInsuredListSchema = null;
        String position = "";
        String JoinCompanyDate = "";
        for (int i = 1; i <= tLCInsuredListSet.size(); i++)
        {
        	if(insuredId.equals(tLCInsuredListSet.get(i).getInsuredID())){
        		position = tLCInsuredListSet.get(i).getPosition();
            	JoinCompanyDate = tLCInsuredListSet.get(i).getJoinCompanyDate();
        	}
        	
        	if (tLCInsuredListSet.get(i).getRelation() == null
                    || tLCInsuredListSet.get(i).getRelation().equals("00"))
            {
                tLCInsuredListSchema = tLCInsuredListSet.get(i).getSchema();
            }
        }
        if (tLCInsuredListSchema == null)
        {
            CError
                    .buildErr(
                            this,
                            "程序发生异常错误,请程序员Yangming处理(GrpPolImpInfo:1208 getContData:tLCInsuredListSchema Error)");
            return null;
        }
        VData tInputData = new VData();
        VData tResult = new VData();
        LCContSchema contSchema = null;
        LCInsuredSchema tLCInsuerdSchema = null;
        if (contId != null)
        {
            contSchema = findLCContfromCache(contId);
        }
        if (insuredId != null)
        {
            tLCInsuerdSchema = this.findInsuredfromCache(insuredId);
        }
        if (tLCInsuerdSchema != null && contSchema != null
                && !"".equals(StrTool.cTrim(contSchema.getContNo())))
        { //已经创建，直接返回
            tResult.add(contSchema);
            tResult.add(tLCInsuerdSchema);
            return tResult;
        }

        Reflections ref = new Reflections();
        boolean needtocach = false;
        boolean createIns = false;
        boolean createCont = false;
        // LDPersonSchema tPersonSchema;
        if (contSchema == null)
        {
            createCont = true;
        }
        if (tLCInsuerdSchema == null)
        {
            //如果被保险人没有被创建过，则从原始解析的xml中获取
            //标识要创建被保险人
            createIns = true;
            tLCInsuerdSchema = this.findInsured(insuredId);
            if (tLCInsuerdSchema == null)
            {
                CError.buildErr(this, "导入文件中没有被保险人[" + insuredId + "]");
                return null;
            }
            if (!createCont)
            { //合同已经存在，
                tLCInsuerdSchema.setContNo(contSchema.getContNo());

            }
        }
        //
        LDPersonSchema tPersonSchema = new LDPersonSchema();
        ref.transFields(tPersonSchema, tLCInsuerdSchema);
        tPersonSchema.setSocialCenterNo(tLCInsuerdSchema.getOthIDNo());
        tPersonSchema.setState(tLCInsuerdSchema.getInsuredStat());
        tPersonSchema.setCustomerNo(tLCInsuerdSchema.getInsuredNo());
        LDPersonSchema nPersonSchema = checkInSystemPerson(tPersonSchema);
        LCInsuredSchema cacheInsuredSchema = new LCInsuredSchema();
        cacheInsuredSchema.setSchema(tLCInsuerdSchema);

        if (nPersonSchema != null)
        {
            tPersonSchema = nPersonSchema;
            if (!StrTool.cTrim(tPersonSchema.getEnglishName()).equals(
                    tLCInsuredListSchema.getEnglishName())
                    || !StrTool.cTrim(tPersonSchema.getOthIDNo()).equals(
                            tLCInsuredListSchema.getOthIDNo())
                    || !StrTool.cTrim(tPersonSchema.getOthIDType()).equals(
                            tLCInsuredListSchema.getOthIDType()))
            {
                tPersonSchema.setEnglishName(tLCInsuredListSchema
                        .getEnglishName());
                tPersonSchema.setOthIDNo(tLCInsuredListSchema.getOthIDNo());
                tPersonSchema.setOthIDType(tLCInsuredListSchema.getOthIDType());
            }
            ref.transFields(tLCInsuerdSchema, tPersonSchema);
            //tLCInsuerdSchema.set
            tLCInsuerdSchema.setInsuredNo(nPersonSchema.getCustomerNo());
        }
        //        tLCInsuerdSchema.setOccupationType(tLCInsuredListSchema
        //                .getOccupationType());

        // 修改连带被保人职业类别被主被保人职业类别覆盖的问题。
        String tTmpOccupationType = tLCInsuredListSchema.getOccupationType();
        // 修改被保人职业代码被LDPerson表中的值所覆盖的问题。
        // 由于被保人清单中没有填写职业代码，所以将职业代码置空。  by zhangyang
        String tTmpOccupationCode = null; 

        for (int i = 1; i <= tLCInsuredListSet.size(); i++)
        {
            LCInsuredListSchema tTmpLCInsuredListSchema = tLCInsuredListSet
                    .get(i);
            if (insuredId.equals(tTmpLCInsuredListSchema.getInsuredID()))
            {
                tTmpOccupationType = tTmpLCInsuredListSchema
                        .getOccupationType();
                break;
            }
        }
        tLCInsuerdSchema.setOccupationType(tTmpOccupationType); 
        tLCInsuerdSchema.setOccupationCode(tTmpOccupationCode);
        
        // --------------------------------------------------

        tPersonSchema.setGrpName(tLCInsuredListSchema.getGrpName());
        tPersonSchema.setGrpNo(tLCInsuredListSchema.getGrpNo());
        tPersonSchema.setSocialCenterName(tLCInsuredListSchema
                .getSocialCenterName());
        tPersonSchema.setSocialCenterNo(tLCInsuredListSchema
                .getSocialCenterNo());
        MMap map = new MMap();
        if (createCont)
        {
            contSchema = new LCContSchema();

            if (!createIns)
            {
                //被保险人已经创建
                contSchema.setInsuredNo(tPersonSchema.getCustomerNo());
            }
            else
            {
                contSchema.setInsuredNo("");
            }
            if(tLCInsuredListSchema.getEdorValiDate() == null || tLCInsuredListSchema.getEdorValiDate().equals("")){
            	 contSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
            }else{
            	contSchema.setCValiDate(tLCInsuredListSchema.getEdorValiDate());
            }
            

            contSchema.setInsuredSex(tPersonSchema.getSex());
            contSchema.setInsuredName(tPersonSchema.getName());
            contSchema.setInsuredBirthday(tPersonSchema.getBirthday());
            contSchema.setBankAccNo(tLCInsuerdSchema.getBankAccNo());
            contSchema.setBankCode(tLCInsuerdSchema.getBankCode());
            contSchema.setAccName(tLCInsuerdSchema.getAccName());
            //借用被保人信用等级字段保存 保单类型标记
            contSchema.setPolType(cacheInsuredSchema.getCreditGrade());
            if (StrTool.cTrim(contSchema.getPolType()).equals(""))
            {
                CError
                        .buildErr(this,
                                "程序发生异常错误,请程序员Yangming处理(GrpPolImpInfo:1362 LCCont:PolType Error)");
                return null;
            }
            if ("1".equals(cacheInsuredSchema.getCreditGrade()))
            {
                //借用入机时间字段保存 被保人数目
                contSchema.setPeoples(String.valueOf(tLCInsuerdSchema
                        .getMakeTime()));
            }
            else
            {
                contSchema.setPeoples("1");
            }

            // 如果是被保人是公共帐户虚人，判断帐户类型标志。公共帐户insuredId字段会存放帐户类型标志
            if ("2".equals(contSchema.getPolType()))
            {
                if ("C".equals(insuredId))
                {
                    contSchema.setAccType("1");
                }
                else if ("G".equals(insuredId))
                {
                    contSchema.setAccType("2");
                }
                else
                {
                    CError.buildErr(this, "未找到对应公共帐户类型。");
                    return null;
                }
            }
            // ----------------------------

            contSchema.setContType("2");
            //            contSchema.setPolType("0");

            contSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            contSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            contSchema.setAppntName(tLCGrpPolSchema.getGrpName());
            contSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
            contSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
            contSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            // contSchema.setInputOperator(tLCGrpPolSchema.getOperator());
            contSchema.setOperator(mGlobalInput.Operator);
            contSchema.setManageCom(mGlobalInput.ManageCom);
            contSchema.setMakeDate(tLCGrpPolSchema.getMakeDate());
            contSchema.setMakeTime(tLCGrpPolSchema.getMakeTime());
            contSchema.setModifyDate(tLCGrpPolSchema.getModifyDate());
            contSchema.setModifyTime(tLCGrpPolSchema.getModifyTime());
            contSchema.setHandlerDate(tLCInsuredListSchema.getHandlerDate());
            contSchema.setHandler(tLCInsuredListSchema.getHandler());
            contSchema.setHandlerPrint(tLCInsuredListSchema.getHandlerPrint());
            contSchema.setBankCode(tLCInsuredListSchema.getPerBankCode());
            contSchema.setBankAccNo(tLCInsuredListSchema.getPerBankAccNo());
            contSchema.setAccName(tLCInsuredListSchema.getPerAccName());
        }

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setSchema(tLCInsuerdSchema);
        tLCInsuredDB.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
        //       tLCInsuredDB.setInsuredNo(tPersonSchema.getCustomerNo());

        LCCustomerImpartSet impartSet = new LCCustomerImpartSet();

        TransferData transferData = new TransferData();
        transferData.setNameAndValue("FamilyType", "0");
        transferData.setNameAndValue("DiskImportFlag", "1");
        transferData.setNameAndValue("ContType", "2");
        transferData.setNameAndValue("PolTypeFlag", contSchema.getPolType());
        transferData.setNameAndValue("InsuredPeoples", String
                .valueOf(contSchema.getPeoples()));
        transferData.setNameAndValue("", tLCInsuredListSchema.getGrpNo());
        tInputData.add(mGlobalInput);
        tInputData.add(contSchema);
        tInputData.add(tLCInsuerdSchema);
        tInputData.add(tLCInsuredDB);
        LCAddressSchema inLCAddressSchema = new LCAddressSchema();
        tInputData.add(inLCAddressSchema);
        tInputData.add(tPersonSchema);
        tInputData.add(impartSet);
        tInputData.add(transferData);
        tInputData.add(tLCInsuredListSchema);

        //提交生成或得到团单下个单合同
        ContInsuredBL contInsuredBl = new ContInsuredBL();
        boolean cRes = contInsuredBl.preparesubmitData(tInputData,
                "INSERT||CONTINSURED");
        if (!cRes)
        {
            // CError.buildErr(this, contInsuredBl.mErrors.getErrContent());
            this.mErrors.copyAllErrors(contInsuredBl.mErrors);
            return null;
        }
        tResult = contInsuredBl.getResult();

        map.add((MMap) tResult.getObjectByObjectName("MMap", 0));

        LCAddressSchema tLCAddressSchema = (LCAddressSchema) tResult
                .getObjectByObjectName("LCAddressSchema", 0);
        if (tLCAddressSchema != null && tLCAddressSchema.getAddressNo() != null)
        {
            //    tLCAddressSet.add( tLCAddressSchema );
            map.put(tLCAddressSchema, "INSERT");
        }
        LCInsuredSchema rInsureSchema = (LCInsuredSchema) tResult
                .getObjectByObjectName("LCInsuredSchema", 0);
        LCContSchema rContSchema = (LCContSchema) tResult
                .getObjectByObjectName("LCContSchema", 0);
        if (tLCAddressSchema != null)
        {
            rInsureSchema.setAddressNo(tLCAddressSchema.getAddressNo());

        }

        //如果被保险人页签里面的信息不为空，则以填写的为准.
        if (!StrTool.cTrim(cacheInsuredSchema.getOccupationType()).equals(""))
        {
            rInsureSchema.setOccupationType(cacheInsuredSchema
                    .getOccupationType());
        }
        if (!StrTool.cTrim(cacheInsuredSchema.getOccupationCode()).equals(""))
        {
            rInsureSchema.setOccupationCode(cacheInsuredSchema
                    .getOccupationCode());
        }
        if (!StrTool.cTrim(cacheInsuredSchema.getPluralityType()).equals(""))
        {
            rInsureSchema.setPluralityType(cacheInsuredSchema
                    .getPluralityType());
        }
        

        // 修改被保人职业代码被LDPerson表中的值所覆盖的问题。
        // 由于被保人清单中没有填写职业代码，所以将职业代码置空。  by zhangyang
        String tTmpOccupationCode2 = null; 
        ((LCInsuredSchema)((MMap)tResult.getObjectByObjectName("MMap", 0))
        		.getObjectByObjectName("LCInsuredSchema", 0))
        		.setOccupationCode(tTmpOccupationCode2);
        //by gzh
        ((LCInsuredSchema)((MMap)tResult.getObjectByObjectName("MMap", 0))
        		.getObjectByObjectName("LCInsuredSchema", 0))
        		.setPosition(position);
        ((LCInsuredSchema)((MMap)tResult.getObjectByObjectName("MMap", 0))
        		.getObjectByObjectName("LCInsuredSchema", 0))
        		.setJoinCompanyDate(JoinCompanyDate);
        String tNativePlace = cacheInsuredSchema.getNativePlace();
        ((LCInsuredSchema)((MMap)tResult.getObjectByObjectName("MMap", 0))
        		.getObjectByObjectName("LCInsuredSchema", 0))
        		.setNativePlace(tNativePlace);
        String tNativeCity = cacheInsuredSchema.getNativeCity();
        ((LCInsuredSchema)((MMap)tResult.getObjectByObjectName("MMap", 0))
        		.getObjectByObjectName("LCInsuredSchema", 0))
        		.setNativeCity(tNativeCity);

        tLCInsuerdSchema.setInsuredStat("");
        //缓存创建了的信息
        if (createCont)
        {
            cacheLCContSchema(contId, rContSchema);
        }
        if (createIns)
        {
            //rContSchema.setPeoples(rContSchema.getPeoples() + 1);
            cacheLCInsuredSchema(insuredId, rInsureSchema);
        }
        for (int i = 1; i <= tLCInsuredListSet.size(); i++)
        {
            if (tLCInsuredListSet.get(i).getInsuredID().equals(insuredId))
            {
                tLCInsuredListSet.get(i).setInsuredNo(
                        rInsureSchema.getInsuredNo());
            }
            if (StrTool.cTrim(tLCInsuredListSet.get(i).getContPlanCode())
                    .equals("FM"))
            {
                tLCInsuredListSet.get(i).setInsuredNo(
                        rContSchema.getInsuredNo());
            }
        }
        tResult.add(rContSchema);
        tResult.add(rInsureSchema);
        if (map != null)
        {
            tResult.add(map);
        }
        return tResult;
    }

    /**
     * 受益人信息：查找被保险人，当受益人跟被保险人为同一人，则利用被保险人的信息
     * 如果不是同一人，查找被保险人是否已经创建，若没需要创建 ， 创建受益人信息
     *
     * @param tLCBnfSet LCBnfSet
     * @param insuredSchema LCInsuredSchema
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @param insureIndex String
     * @return VData
     */
    public VData perareBnf(LCBnfSet tLCBnfSet, LCInsuredSchema insuredSchema,
            LCGrpPolSchema tLCGrpPolSchema, String insureIndex)
    {
        MMap tmpMap = null;
        //校验处理受益人的被保险人信息
        LCBnfSchema tBnfSchema = null;
        for (int t = 1; t <= tLCBnfSet.size(); t++)
        {
            tBnfSchema = tLCBnfSet.get(t);
            //主被保险人的合同号一致
            tBnfSchema.setContNo(insuredSchema.getContNo());
            //            if ("".equals(StrTool.cTrim(tBnfSchema.getInsuredNo()))
            //                || tBnfSchema.getInsuredNo().equals(insureIndex))
            //            {

            if ("1".equals(StrTool.cTrim(tBnfSchema.getBnfType()))
                    && "00".equals(StrTool.cTrim(tBnfSchema
                            .getRelationToInsured())))
            {
                CError.buildErr(this, "合同[" + tBnfSchema.getContNo()
                        + "]有受益人类型为'死亡受益人'，不能指定为被保人本人");
                return null;
            }
            //受益人为被保险人本人
            else if ("00".equals(StrTool.cTrim(tBnfSchema
                    .getRelationToInsured())))
            {
                tBnfSchema.setInsuredNo(insuredSchema.getInsuredNo());
                tBnfSchema.setCustomerNo(insuredSchema.getInsuredNo());
                tBnfSchema.setBirthday(insuredSchema.getBirthday());
                tBnfSchema.setName(insuredSchema.getName());
                tBnfSchema.setSex(insuredSchema.getSex());
                tBnfSchema.setIDType(insuredSchema.getIDType());
                tBnfSchema.setIDNo(insuredSchema.getIDNo());

            }
            else
            {
                //查询被保险人
                LCInsuredSchema tInsuredSchema = findInsuredfromCache(tBnfSchema
                        .getInsuredNo());
                if (tInsuredSchema == null)
                {
                    CError.buildErr(this, "受益人信息没有查找到被保险人["
                            + tBnfSchema.getInsuredNo() + "]信息");
                    return null;
                }
                tBnfSchema.setInsuredNo(tInsuredSchema.getInsuredNo());
                //根据被保险人，判断是否需要新创建被保险人合同和表
                // tBnfSchema.setContNo(tInsuredSchema.getContNo());
                //察看系统中看看不需要创建新的客户
                LDPersonSchema person = checkInSystem_Bnf(tBnfSchema);
                if (person == null)
                {
                    CError.buildErr(this, "受益人信息创建失败");

                    return null;
                }
                tBnfSchema.setCustomerNo(person.getCustomerNo());
            }
            if ("".equals(tBnfSchema.getBnfGrade()))
            {
                tBnfSchema.setBnfGrade("1");
            }

            tBnfSchema.setOperator(mGlobalInput.Operator);
            tBnfSchema.setMakeDate(PubFun.getCurrentDate());
            tBnfSchema.setMakeTime(PubFun.getCurrentTime());
            tBnfSchema.setModifyDate(tBnfSchema.getMakeDate());
            tBnfSchema.setModifyTime(tBnfSchema.getMakeTime());
        }

        VData tData = new VData();
        if (tmpMap != null && tmpMap.keySet().size() > 0)
        {
            tData.add(tmpMap);
        }
        tData.add(tLCBnfSet);
        return tData;
    }

    /**
     *  查询Person是否已经存在于系统,不存在则创建LDPerson
     * @param schema LDPersonSchema
     * @return LDPersonSchema
     */
    private LDPersonSchema checkInSystemPerson(LDPersonSchema schema)
    {
        LDPersonDB tdb = new LDPersonDB();
        LDPersonSet iSet = null;
        if (!"".equals(StrTool.cTrim(schema.getCustomerNo())))
        {
            tdb = new LDPersonDB();
            tdb.setCustomerNo(schema.getCustomerNo());
            iSet = tdb.query();
        }
        if (iSet != null && iSet.size() >= 0)
        {
            return iSet.get(iSet.size());
        }

        boolean err = false;
        if ("".equals(StrTool.cTrim(schema.getName())))
        {
            CError.buildErr(this, "客户姓名不能为空！");
            err = true;
        }
        if (!"".equals(StrTool.cTrim(schema.getSex()))
                && !"".equals(StrTool.cTrim(schema.getBirthday()))
                && !"".equals(StrTool.cTrim(schema.getIDType()))
                && !"".equals(StrTool.cTrim(schema.getIDNo())))
        {
            /*
             if ("".equals(StrTool.cTrim(schema.getSex())))
             {
             CError.buildErr(this, "客户性别不能为空！");
             err = true;

             }
             if ("".equals(StrTool.cTrim(schema.getBirthday())))
             {
             CError.buildErr(this, "客户生日不能为空！");
             err = true;
             }
             if ("".equals(StrTool.cTrim(schema.getBirthday())))
             {
             CError.buildErr(this, "客户证件类型不能为空！");
             err = true;
             }

             if ("".equals(StrTool.cTrim(schema.getBirthday())))
             {
             CError.buildErr(this, "客户证件号码不能为空！");
             err = true;
             }
             */

            tdb.setIDNo(schema.getIDNo());
            tdb.setIDType(schema.getIDType());
            tdb.setBirthday(schema.getBirthday());
            tdb.setName(schema.getName());
            tdb.setSex(schema.getSex());
            tdb.setSocialCenterNo(schema.getSocialCenterNo());

            iSet = tdb.query();
            //查询到，返回
            if (!tdb.mErrors.needDealError() && iSet != null && iSet.size() > 0)
            {
                return iSet.get(iSet.size());
            }
        }

        return schema;
    }

    /**
     * 查询受益人是否已经存在于系统,不存在则创建LDPerson
     * 在系统中，则contNo不为空，否则contNo为空
     * @param schema LCInsuredSchema
     * @return LCInsuredSchema
     */

    private LDPersonSchema checkInSystem_Bnf(LCBnfSchema schema)
    {
        LDPersonDB tdb = new LDPersonDB();
        LDPersonSet iSet = null;
        if (!"".equals(StrTool.cTrim(schema.getCustomerNo())))
        {
            tdb = new LDPersonDB();
            tdb.setCustomerNo(schema.getCustomerNo());
            iSet = tdb.query();
        }
        if (iSet != null && iSet.size() >= 0)
        {
            return iSet.get(iSet.size());
        }

        boolean err = false;
        if ("".equals(StrTool.cTrim(schema.getName())))
        {
            CError.buildErr(this, "客户姓名不能为空！");
            err = true;
        }
        //        if ("".equals(StrTool.cTrim(schema.getSex())))
        //        {
        //            CError.buildErr(this, "客户性别不能为空！");
        //            err = true;
        //
        //        }
        //        if ("".equals(StrTool.cTrim(schema.getBirthday())))
        //        {
        //            CError.buildErr(this, "客户生日不能为空！");
        //            err = true;
        //        }
        if (!"".equals(StrTool.cTrim(schema.getSex()))
                && !"".equals(StrTool.cTrim(schema.getBirthday()))
                && !"".equals(StrTool.cTrim(schema.getIDType()))
                && !"".equals(StrTool.cTrim(schema.getIDNo())))
        {

            tdb.setIDNo(schema.getIDNo());
            tdb.setIDType(schema.getIDType());
            tdb.setBirthday(schema.getBirthday());
            tdb.setName(schema.getName());
            tdb.setSex(schema.getSex());

            iSet = tdb.query();

            //查询到，返回
            if (!tdb.mErrors.needDealError() && iSet != null && iSet.size() > 0)
            {
                return iSet.get(iSet.size());
            }
        }
        //需要创建客户

        String nolimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
        String customerno = PubFun1.CreateMaxNo("CustomerNo", nolimit);
        LDPersonSchema tPerson = new LDPersonSchema();
        tPerson.setCustomerNo(customerno);
        tPerson.setBirthday(schema.getBirthday());
        tPerson.setIDNo(schema.getIDNo());
        tPerson.setIDType(schema.getIDType());
        tPerson.setOperator(mGlobalInput.Operator);
        tPerson.setMakeDate(PubFun.getCurrentDate());
        tPerson.setMakeTime(PubFun.getCurrentTime());
        tPerson.setModifyDate(tPerson.getMakeDate());
        tPerson.setModifyTime(tPerson.getMakeTime());
        tPerson.setName(schema.getName());
        tPerson.setSex(schema.getSex());

        tdb.setSchema(tPerson);
        if (!tdb.insert())
        {
            CError.buildErr(this, "创建客户时出错:", tdb.mErrors);
            return null;
        }
        return tdb.getSchema();

    }

    /**
     * 查询或创建被保人信息，但是与合同无关；创建完毕以后缓存被保险人信息
     * @param insuredId String
     * @return VData
     */
    private VData getInsuredData(String insuredId)
    {

        VData tData = new VData();
        LDPersonDB tdb = new LDPersonDB();
        Reflections ref = new Reflections();
        //   int insuredIndex = this.parseId(insuredId);
        LCInsuredSchema tSchema = this.findInsured(insuredId);

        LCInsuredSchema schema = new LCInsuredSchema();
        //拷贝一份缓存
        schema.setSchema(tSchema);
        schema.setContNo("");
        if (schema == null)
        {
            CError.buildErr(this, "创建被保险人时未从导入数据中查找到被保险人[" + insuredId + "]");
            return null;
        }
        //需要创建客户
        boolean err = false;
        if ("".equals(StrTool.cTrim(schema.getName())))
        {
            CError.buildErr(this, "客户姓名不能为空！");
            err = true;
        }
        if ("".equals(StrTool.cTrim(schema.getSex())))
        {
            CError.buildErr(this, "客户性别不能为空！");
            err = true;

        }
        if ("".equals(StrTool.cTrim(schema.getBirthday())))
        {
            CError.buildErr(this, "客户生日不能为空！");
            err = true;
        }
        LDPersonSet iSet = null;
        if (!"".equals(StrTool.cTrim(schema.getInsuredNo())))
        {

            tdb.setCustomerNo(schema.getInsuredNo());
            iSet = tdb.query();
        }
        if (iSet == null)
        {
            //  }
            //  else if ("0".equals(StrTool.cTrim(schema.getIDType())) && schema.getIDNo() != null) {
            //用身份证id查询
            tdb.setIDNo(schema.getIDNo());
            tdb.setIDType(schema.getIDType());
            tdb.setName(schema.getName());
            tdb.setSex(schema.getSex());
            tdb.setBirthday(schema.getBirthday());
            tdb.setSocialCenterNo(schema.getOthIDNo());
            iSet = tdb.query();
        }

        if (!tdb.mErrors.needDealError() && iSet != null && iSet.size() > 0)
        {

            LDPersonSchema tPersonSchema = iSet.get(1);
            ref.transFields(schema, tPersonSchema);
            schema.setInsuredNo(tPersonSchema.getCustomerNo());
        }
        else
        {

            String nolimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
            String customerno = PubFun1.CreateMaxNo("CustomerNo", nolimit);
            LDPersonSchema tPerson = new LDPersonSchema();
            tPerson.setCustomerNo(customerno);
            tPerson.setBirthday(schema.getBirthday());
            tPerson.setIDNo(schema.getIDNo());
            tPerson.setIDType(schema.getIDType());
            tPerson.setOperator(mGlobalInput.Operator);
            tPerson.setMakeDate(PubFun.getCurrentDate());
            tPerson.setMakeTime(PubFun.getCurrentTime());
            tPerson.setModifyDate(tPerson.getMakeDate());
            tPerson.setModifyTime(tPerson.getMakeTime());
            tPerson.setName(schema.getName());
            tPerson.setSex(schema.getSex());

            tdb.setSchema(tPerson);
            if (!tdb.insert())
            {
                CError.buildErr(this, "创建客户时出错:", tdb.mErrors);
                return null;
            }

            ref.transFields(schema, tPerson);
            schema.setInsuredNo(tPerson.getCustomerNo());
        }

        this.cacheLCInsuredSchema(insuredId, schema);
        tData.add(schema);
        return tData;
    }

    /**
     * 类型转换
     * @param id String
     * @return int
     */
    private int parseId(String id)
    {
        int newId = 0;
        id = StrTool.cTrim(id);
        try
        {
            newId = Integer.parseInt(id);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return -1;
        }
        return newId;
    }

    /**
     * 获取险种保单数据
     * @param contId String
     * @return Object
     */
    public Object getContPolData(String contId)
    {
        return this.mContPolData.get(contId);
    }

    public void intiContPolData()
    {
        this.mContPolData = new HashMap();

    }

    /**
     * 按合同号缓存险种保单数据
     * @param contId String
     * @param data Object
     */
    public void addContPolData(String contId, Object data)
    {

        if (mContPolData.containsKey(contId))
        {
            VData tVData = (VData) mContPolData.get(contId);
            tVData.add(data);
        }
        else
        {
            VData contpolData = new VData();
            contpolData.add(data);
            mContPolData.put(contId, contpolData);
        }
    }

    /**
     * 解析获取合同id ,
     *
     * @param contpolIns String like contid-polid-insuredid
     * @return String ，contid
     */
    public String getContKey(String contpolIns)
    {
        String rs = contpolIns;
        if (contpolIns.indexOf("-") > 0)
        {
            Object[] obj = contpolIns.split("-");
            rs = (String) obj[0];
        }
        return rs;
    }

    /**
     * 内存调试用,主要显示缓存中的各个对象是否存在没有释放,等内存泄露问题.
     *
     * @param contId String
     */
    public void deBug(String contId)
    {
        /** 首先处理比较大的HashMap对象 */
        System.out.println("****************内存泄漏检测******************");
        System.out.println("正在处理第" + contId + "号合同");
        System.out.println("检测 mInsuredMap 对象");
        System.out.println("mInsuredMap 大小为"
                + (mInsuredMap != null ? mInsuredMap.size() : 0));
        System.out.println("检测 mMainPolMap 对象");
        System.out.println("mMainPolMap 大小为"
                + (mMainPolMap != null ? mMainPolMap.size() : 0));
        System.out.println("检测 mContCache 对象");
        System.out.println("mContCache 大小为"
                + (mContCache != null ? mContCache.size() : 0));
        System.out.println("检测 mErrors 对象");
        System.out.println("mErrors 大小为"
                + (mErrors != null ? mErrors.getErrorCount() : 0));
        System.out.println("检测 m_LCGrpPolSet 对象");
        System.out.println("m_LCGrpPolSet 大小为"
                + (m_LCGrpPolSet != null ? m_LCGrpPolSet.size() : 0));
        System.out.println("检测 m_LMRiskAppSet 对象");
        System.out.println("m_LMRiskAppSet 大小为"
                + (m_LMRiskAppSet != null ? m_LMRiskAppSet.size() : 0));
        System.out.println("检测 m_LMRiskSet 对象");
        System.out.println("m_LMRiskSet 大小为"
                + (m_LMRiskSet != null ? m_LMRiskSet.size() : 0));
        System.out.println("****************内存泄漏检测完成**************");
    }

}
