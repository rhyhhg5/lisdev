/*
 * <p>ClassName: OLCInuredListUI </p>
 * <p>Description: OLCInuredListUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft </p>
 * @Database:
 * @CreateDate：2005-07-27 17:39:01
 */
package com.sinosoft.lis.ulitb;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class OLCInuredListUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private LCInsuredListSchema mLCInsuredListSchema = new LCInsuredListSchema();
    public OLCInuredListUI() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
//  //得到外部传入的数据,将数据备份到本类中
//  if (!getInputData(cInputData))
//    return false;
//
//  //进行业务处理
//  if (!dealData())
//    return false;
//
//  //准备往后台的数据
//  if (!prepareOutputData())
//    return false;

        OLCInuredListBL tOLCInuredListBL = new OLCInuredListBL();

        //System.out.println("Start OLCInuredList UI Submit...");
        tOLCInuredListBL.submitData(mInputData, mOperate);
        //System.out.println("End OLCInuredList UI Submit...");
        //如果有需要处理的错误，则返回
        if (tOLCInuredListBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tOLCInuredListBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLCInuredListUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN")) {
            this.mResult.clear();
            this.mResult = tOLCInuredListBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args) {
        LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
        OLCInuredListUI tOLCInuredListUI = new OLCInuredListUI();
        tLCInsuredListSchema.setGrpContNo("1");
        tLCInsuredListSchema.setInsuredID("1");
        tLCInsuredListSchema.setState("");
        tLCInsuredListSchema.setContNo("");
        tLCInsuredListSchema.setBatchNo("");
        tLCInsuredListSchema.setInsuredNo("");
        tLCInsuredListSchema.setRetire("");
        tLCInsuredListSchema.setEmployeeName("");
        tLCInsuredListSchema.setInsuredName("");
        tLCInsuredListSchema.setRelation("");
        tLCInsuredListSchema.setSex("");
        tLCInsuredListSchema.setBirthday("");
        tLCInsuredListSchema.setIDType("");
        tLCInsuredListSchema.setIDNo("");
        tLCInsuredListSchema.setContPlanCode("");
        tLCInsuredListSchema.setOccupationType("");
        tLCInsuredListSchema.setBankCode("");
        tLCInsuredListSchema.setBankAccNo("");
        tLCInsuredListSchema.setAccName("");
        tLCInsuredListSchema.setOperator("");
        tLCInsuredListSchema.setMakeDate("");
        tLCInsuredListSchema.setMakeTime("");
        tLCInsuredListSchema.setModifyDate("");
        tLCInsuredListSchema.setModifyTime("");

        VData tVData = new VData();
        tVData.add(tLCInsuredListSchema);
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86";
        tG.Operator = "group";
        tG.ManageCom = "86";
        tVData.add(tG);

        tOLCInuredListUI.submitData(tVData, "UPDATE||INSURED");
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mLCInsuredListSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInuredListUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLCInsuredListSchema.setSchema((LCInsuredListSchema) cInputData.getObjectByObjectName(
                "LCInuredListSchema", 0));
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
