/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.ulitb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * 管理费数据校验、准备类
 * <p>Title: </p>
 * <p>Description: 通过前台传入的数据信息，做一定的校验和处理</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class GrpFeeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
	public MMap mMMap = new MMap();


    /** 数据操作字符串 */
    private String mOperate;
    
    private String AddData;

    private Reflections mRef = new Reflections();
    /** 业务处理相关变量 */
    private LCGrpFeeSet mLCGrpFeeSet = new LCGrpFeeSet();
    private LCGrpFeeParamSet mLCGrpFeeParamSet = new LCGrpFeeParamSet();
    private LCGrpFeeToAccSet mLCGrpFeeToAccSet = new LCGrpFeeToAccSet();
//    private LCGrpPolSpecSet mLCGrpPolSpecSet = new LCGrpPolSpecSet();

    /** 时间信息*/
    String mCurrentDate = PubFun.getCurrentDate(); //当前值
    String mCurrentTime = PubFun.getCurrentTime();

    public GrpFeeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //数据校验
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError.buildErr(this,"数据处理失败GrpFeeBL-->dealData!");
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            /*System.out.println("Start GrpFeeBL Submit...");
            GrpFeeBLS tGrpFeeBLS = new GrpFeeBLS();
            tGrpFeeBLS.submitData(mInputData, cOperate);
            System.out.println("End GrpFeeBL Submit...");
            //如果有需要处理的错误，则返回
            if (tGrpFeeBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tGrpFeeBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpFeeBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }*/
            
    		//提交数据
    		PubSubmit tPubSubmit = new PubSubmit();
    		if (tPubSubmit.submitData(mInputData, "")) {
    			return true;
    		} else {
    			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
    			return false;
    		}
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据传入操作类型，进行数据准备
     * @return boolean
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;

        //查询合同单号，险种代码
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(this.mLCGrpFeeSet.get(1).getGrpPolNo());
        tLCGrpPolDB.getInfo();
        if("1".equals(this.AddData)&&!"NIK04".equals(tLCGrpPolDB.getRiskCode()))
        {
        	CError.buildErr(this,"只有险种NIK04可以补录账户初始管理费！");
        	return false;
        }
        for (int j= 1; j<= this.mLCGrpFeeSet.size(); j++)
        {
        	//放入管理费主表中，由于一次实际提交的管理费主表记录为1，所以可以如此处理
           	//---update by LiuLiang --2005-08-31--由于现在改为多选所以原来的处理方式
           	//已经不能满足要求,要循环的来取值,否则插入的时候会提示插入失败
           	this.mLCGrpFeeSet.get(j).setGrpContNo(tLCGrpPolDB.getGrpContNo());
           	this.mLCGrpFeeSet.get(j).setRiskCode(tLCGrpPolDB.getRiskCode());
           	this.mLCGrpFeeSet.get(j).setOperator(this.mGlobalInput.Operator); //operator
           	this.mLCGrpFeeSet.get(j).setMakeDate(mCurrentDate); //makedate
           	this.mLCGrpFeeSet.get(j).setMakeTime(mCurrentTime); //maketime
           	this.mLCGrpFeeSet.get(j).setModifyDate(mCurrentDate); //modifydate
           	this.mLCGrpFeeSet.get(j).setModifyTime(mCurrentTime); //modifytime
           	
           	if("1".equals(this.AddData))
           	{
           		MMap tMMap = new MMap();
           		Reflections tref = new Reflections();
           		LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
           		LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
           		LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
           		tLCInsureAccClassDB.setGrpPolNo(tLCGrpPolDB.getGrpPolNo());
           		tLCInsureAccClassDB.setInsuAccNo(this.mLCGrpFeeSet.get(j).getInsuAccNo());
           		tLCInsureAccClassDB.setPayPlanCode(this.mLCGrpFeeSet.get(j).getPayPlanCode());
           		LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.query();
           		if(tLCInsureAccClassSet.size()>0)
           		{
           			tLCInsureAccClassFeeSet.add(new LCInsureAccClassFeeSchema());
           			tref.transFields(tLCInsureAccClassFeeSet,tLCInsureAccClassSet);
           			
           			LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
               		tLCInsureAccDB.setGrpPolNo(tLCGrpPolDB.getGrpPolNo());
               		tLCInsureAccDB.setInsuAccNo(this.mLCGrpFeeSet.get(j).getInsuAccNo());
               		LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();
               		if(tLCInsureAccSet.size()>0)
               		{
               			tLCInsureAccFeeSet.add(new LCInsureAccFeeSchema());
               			tref.transFields(tLCInsureAccFeeSet,tLCInsureAccSet);
               		}
               		else
               		{
               			tLCInsureAccFeeSet.add(new LCInsureAccFeeSchema());
               			tLCInsureAccSet.add(new LCInsureAccSchema());
               			tref.transFields(tLCInsureAccFeeSet,tLCInsureAccClassSet);
               			tref.transFields(tLCInsureAccSet,tLCInsureAccClassSet);
               			for(int k = 1;k<=tLCInsureAccFeeSet.size();k++)
               			{
               				tLCInsureAccFeeSet.get(k).setPrtNo(tLCGrpPolDB.getPrtNo());
               				tLCInsureAccFeeSet.get(k).setRiskCode(tLCGrpPolDB.getRiskCode());
               				tLCInsureAccFeeSet.get(k).setInsuredNo("000000");
               				tLCInsureAccSet.get(k).setPrtNo(tLCGrpPolDB.getPrtNo());
               				tLCInsureAccSet.get(k).setRiskCode(tLCGrpPolDB.getRiskCode());
               				tLCInsureAccSet.get(k).setInsuredNo("000000");
               			}
               			
               			tMMap.put(tLCInsureAccSet,"DELETE&INSERT");
               			tMMap.put("UPDATE LCInsureAcc set InsuredNo = nvl((select INSUREDNO from lcpol where polno = LCInsureAcc.polno),InsuredNo) where grppolno = '"+tLCGrpPolDB.getGrpPolNo()+"'","UPDATE");

               		    
               		}
           		}
           		
           		
           		PubSubmit tPubSubmit = new PubSubmit();
           		
           		tMMap.put(tLCInsureAccFeeSet,"DELETE&INSERT");
           		tMMap.put("UPDATE LCInsureAccFee set InsuredNo = nvl((select INSUREDNO from lcpol where polno = LCInsureAccFee.polno),InsuredNo) where grppolno = '"+tLCGrpPolDB.getGrpPolNo()+"'","UPDATE");
           		tMMap.put("UPDATE lcprem set NeedAcc =  '1' where grpcontno = '"+tLCGrpPolDB.getGrpContNo()+"' and exists (select 1 from lcpol where polno = lcprem.polno and grppolno = '"+tLCGrpPolDB.getGrpPolNo()+"')","UPDATE");
           		tMMap.put(tLCInsureAccClassFeeSet,"DELETE&INSERT");
           		VData tVData = new VData();
           		tVData.addElement(tMMap);
           		tPubSubmit.submitData(tVData,"");
           	}
              
            //获取LMRiskFee表的一些信息
            LMRiskFeeSet tLMRiskFeeSet = new LMRiskFeeSet();
            LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
            tLMRiskFeeDB.setFeeCode(mLCGrpFeeSet.get(j).getFeeCode());
            tLMRiskFeeDB.setPayPlanCode(mLCGrpFeeSet.get(j).getPayPlanCode());
            tLMRiskFeeDB.setInsuAccNo(mLCGrpFeeSet.get(j).getInsuAccNo());
            tLMRiskFeeSet = tLMRiskFeeDB.query();
            if(tLMRiskFeeSet != null && tLMRiskFeeSet.size() > 0){
            	this.mLCGrpFeeSet.get(j).setFeeStartDate(tLMRiskFeeSet.get(1).getFeeStartDate());
            	this.mLCGrpFeeSet.get(j).setFeeTakePlace(tLMRiskFeeSet.get(1).getFeeTakePlace());
            	this.mLCGrpFeeSet.get(j).setFeeNum(tLMRiskFeeSet.get(1).getFeeNum());
            	this.mLCGrpFeeSet.get(j).setFeeBaseType(tLMRiskFeeSet.get(1).getFeeBaseType());
              	this.mLCGrpFeeSet.get(j).setInterfaceClassName(tLMRiskFeeSet.get(1).getInterfaceClassName());	
              	this.mLCGrpFeeSet.get(j).setFeeType(tLMRiskFeeSet.get(1).getFeeType());
            }
              	
            //处理管理费顺序表
            LMFeeToAccSet tLMFeeToAccSet = new LMFeeToAccSet();
            LMFeeToAccDB tLMFeeToAccDB = new LMFeeToAccDB();
            tLMFeeToAccDB.setFeeCode(mLCGrpFeeSet.get(j).getFeeCode());
            tLMFeeToAccDB.setInsuAccNo(mLCGrpFeeSet.get(j).getInsuAccNo());
            tLMFeeToAccDB.setPayPlanCode(mLCGrpFeeSet.get(j).getPayPlanCode());
            tLMFeeToAccSet = tLMFeeToAccDB.query();
            for(int m = 1; m <= tLMFeeToAccSet.size(); m++){
            	LMFeeToAccSchema tLMFeeToAccSchema = tLMFeeToAccSet.get(m);
              	LCGrpFeeToAccSchema tLCGrpFeeToAccSchema = new LCGrpFeeToAccSchema();
              	tLCGrpFeeToAccSchema.setGrpPolNo(tLCGrpPolDB.getGrpPolNo());
              	tLCGrpFeeToAccSchema.setFeeCode(tLMFeeToAccSchema.getFeeCode());
              	tLCGrpFeeToAccSchema.setPayPlanCode(tLMFeeToAccSchema.getPayPlanCode());
              	tLCGrpFeeToAccSchema.setInsuAccNo(tLMFeeToAccSchema.getInsuAccNo());
              	tLCGrpFeeToAccSchema.setPayPlanCode1(tLMFeeToAccSchema.getPayPlanCode1());
              	tLCGrpFeeToAccSchema.setInsuAccNo1(tLMFeeToAccSchema.getInsuAccNo1());
              	tLCGrpFeeToAccSchema.setFeeNum(tLMFeeToAccSchema.getFeeNum());
              	mLCGrpFeeToAccSet.add(tLCGrpFeeToAccSchema);
            }
        }

        //查看管理费子表中，是否有数据信息
        for (int i = 1; i <= this.mLCGrpFeeParamSet.size(); i++)
        {
            System.out.println("come in ...");
            this.mLCGrpFeeParamSet.get(i).setGrpContNo(tLCGrpPolDB.getGrpContNo());
            this.mLCGrpFeeParamSet.get(i).setRiskCode(tLCGrpPolDB.getRiskCode());
            this.mLCGrpFeeParamSet.get(i).setFeeCode(this.mLCGrpFeeSet.get(1).getFeeCode());
            this.mLCGrpFeeParamSet.get(i).setInsuAccNo(this.mLCGrpFeeSet.get(1).getInsuAccNo());
            this.mLCGrpFeeParamSet.get(i).setPayPlanCode(this.mLCGrpFeeSet.get(1).getPayPlanCode());

            this.mLCGrpFeeParamSet.get(i).setOperator(this.mGlobalInput.Operator); //operator
            this.mLCGrpFeeParamSet.get(i).setMakeDate(mCurrentDate); //makedate
            this.mLCGrpFeeParamSet.get(i).setMakeTime(mCurrentTime); //maketime
            this.mLCGrpFeeParamSet.get(i).setModifyDate(mCurrentDate); //modifydate
            this.mLCGrpFeeParamSet.get(i).setModifyTime(mCurrentTime); //modifytime
        }
//        if (mLCGrpPolSpecSet != null) {
//			for (int i = 1; i <= mLCGrpPolSpecSet.size(); i++) {
//				LCGrpPolSpecSchema aLCGrpPolSpecSchema = mLCGrpPolSpecSet.get(i);
//				if (tLCGrpPolDB.getGrpPolNo().equals(
//						aLCGrpPolSpecSchema.getGrpPolNo())) {
//					aLCGrpPolSpecSchema
//							.setGrpContNo(tLCGrpPolDB.getGrpContNo());
//					aLCGrpPolSpecSchema.setGrpPolNo(tLCGrpPolDB.getGrpPolNo());
//					aLCGrpPolSpecSchema.setPrtNo(tLCGrpPolDB.getPrtNo());
//					aLCGrpPolSpecSchema.setRiskCode(tLCGrpPolDB.getRiskCode());
//					aLCGrpPolSpecSchema.setOperator(mGlobalInput.Operator);
//					aLCGrpPolSpecSchema.setMakeDate(PubFun.getCurrentDate());
//					aLCGrpPolSpecSchema.setMakeTime(PubFun.getCurrentTime());
//					aLCGrpPolSpecSchema.setModifyDate(PubFun.getCurrentDate());
//					aLCGrpPolSpecSchema.setModifyTime(PubFun.getCurrentTime());
//				}
//			}
//		}
        
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                "GlobalInput", 0));
        this.mLCGrpFeeSet.set((LCGrpFeeSet) cInputData.
                              getObjectByObjectName("LCGrpFeeSet", 0));
        this.mLCGrpFeeParamSet.set((LCGrpFeeParamSet) cInputData.
                                   getObjectByObjectName(
                "LCGrpFeeParamSet", 0));
        TransferData tTransferData = (TransferData)cInputData.
                                   getObjectByObjectName(
                "TransferData", 0);
        if(tTransferData!=null)
        {
        	AddData = (String)tTransferData.getValueByName("AddData");
        }
        
//        mLCGrpPolSpecSet = ((LCGrpPolSpecSet) cInputData
//				.getObjectByObjectName("LCGrpPolSpecSet", 0));
        

        return true;
    }
    
    private boolean checkData()
    {
        ExeSQL exeSql = new ExeSQL();
        //NHK01产品特有校验,此人有NHK01这个险种,如果需要修改初始费用比例(包括 01，02情况)，必须先将被保险人全部删除
        String strNHK =" select count(1) from lcpol where grppolno='"+mLCGrpFeeSet.get(1).getGrpPolNo()+"' and riskcode='NHK01' ";
        String numNHK = exeSql.getOneValue(strNHK);
        if(Integer.parseInt(numNHK)>=1)
        {
        	for (int i= 1; i<= this.mLCGrpFeeSet.size(); i++)
        	{
        		String tPayPlanCode = mLCGrpFeeSet.get(i).getPayPlanCode();
        		String tFeeCalMode = mLCGrpFeeSet.get(i).getFeeCalMode();
        		if(tFeeCalMode!=null && tPayPlanCode!=null && tPayPlanCode.equals("635101"))
        		{
        			CError.buildErr(this,"如需修改初始费用信息，请先删除该险种下被保险人！");
        			return false;
        		}
        	}
        }
        return true;
    }
    /**
     * 查询操作
     * @return boolean
     */
    private boolean submitquery()
    {
        mInputData = null;
        return true;
    }

    /**
     * 放后台准备数据
     * @return boolean
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            for(int i = 1; i <= mLCGrpFeeSet.size(); i++){
                //删除管理费子表信息
                String tSql = "delete from LCGrpFeeParam Where GrpPolNo = '" +
                			  mLCGrpFeeSet.get(i).getGrpPolNo() +
                              "' and RiskCode = '" +
                              mLCGrpFeeSet.get(i).getRiskCode() +
                              "' and FeeCode = '" +
                              mLCGrpFeeSet.get(i).getFeeCode() +
                              "' and InsuAccNo = '" +
                              mLCGrpFeeSet.get(i).getInsuAccNo() +
                              "' and PayPlanCode = '" +
                              mLCGrpFeeSet.get(i).getPayPlanCode() + "'";
                
                mMMap.put(tSql, "DELETE");
            }
            if (this.mOperate.equals("INSERT||MAIN"))
            {
            	mMMap.put(mLCGrpFeeSet, "DELETE&INSERT");
            	mMMap.put(mLCGrpFeeParamSet, "DELETE&INSERT");
            	mMMap.put(mLCGrpFeeToAccSet, "DELETE&INSERT");
            }
            if (this.mOperate.equals("DELETE||MAIN"))
            {
            	mMMap.put(mLCGrpFeeSet, "DELETE");
            	mMMap.put(mLCGrpFeeParamSet, "DELETE");
            	mMMap.put(mLCGrpFeeToAccSet, "DELETE");
            }
            if (this.mOperate.equals("UPDATE||MAIN"))
            {
            	mMMap.put(mLCGrpFeeSet, "DELETE&INSERT");
            	mMMap.put(mLCGrpFeeParamSet, "DELETE&INSERT");
            	mMMap.put(mLCGrpFeeToAccSet, "DELETE&INSERT");
            }
//            if(this.mLCGrpPolSpecSet!=null){
//            	mMMap.put(mLCGrpPolSpecSet, "DELETE&INSERT");
//            }
            this.mInputData.add(this.mMMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
