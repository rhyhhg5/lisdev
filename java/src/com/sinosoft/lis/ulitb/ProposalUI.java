package com.sinosoft.lis.ulitb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.ulitb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import java.text.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:投保功能类（界面输入）
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YT
 * @version 1.0
 */
public class ProposalUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    //业务处理相关变量
    /** 接受前台传输数据的容器 */
    private TransferData mTransferData = new TransferData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 保单 */
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    /** 投保人 */
    private LCAppntIndSchema mLCAppntIndSchema = new LCAppntIndSchema();
    private LCAppntGrpSchema mLCAppntGrpSchema = new LCAppntGrpSchema();
    /** 被保人 */
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    /** 受益人 */
    private LCBnfSet mLCBnfSet = new LCBnfSet();
    /** 告知信息 */
    private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
    /** 特别约定 */
    private LCSpecSet mLCSpecSet = new LCSpecSet();
    //一般的责任信息
    private LCDutySchema mLCDutySchema = new LCDutySchema();

    /** 可选责任信息 */
    private LCDutySet mLCDutySet1 = new LCDutySet();
    private boolean mNeedDuty = false;

    public ProposalUI() {
    }

    public static void main(String[] args) {
        ProposalUI tProposalUI = null;
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCDutySchema tLCDutySchema = new LCDutySchema();
        LCPremToAccSet tLCPremToAccSet = new LCPremToAccSet();
        LCPremSet tLCPremSet = new LCPremSet();
        LCPremSchema tLCPremSchema1 = new LCPremSchema();
        LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        LCContSchema tLCContSchema = new LCContSchema();


        GlobalInput tG = new GlobalInput();
        String ContNo = "";
        String GrpContNo = "";
        String RiskCode = "1201";
        tG.ManageCom = "86310000";
        tG.Operator = "001";
        VData tVData = new VData();
        /**
         * 团体特需医疗,界面录入方式;
         */
        ContNo = "00086114701";
        GrpContNo = "00000000000000000000";
        LCPremToAccSchema tLCPremToAccSchema = new LCPremToAccSchema();
//        tLCPremSchema1.setDutyCode("204001");
//        tLCPremSchema1.setPayPlanCode("204101");
//        tLCPremSchema1.setStandPrem("500000"); //每个保费项上的保费在js页里算出
//        tLCPremSchema1.setPrem("500000");
//        tLCPremSchema1.setRate(0);
//        tLCPremSet.add(tLCPremSchema1);

        tLCPremToAccSchema.setDutyCode("205001");
        tLCPremToAccSchema.setPayPlanCode("205101");
        tLCPremToAccSchema.setInsuAccNo("");
        tLCPremToAccSchema.setRate("0");
        tLCPremToAccSet.add(tLCPremToAccSchema);

        tVData.add(tLCPremSet);
        tVData.add(tLCPremToAccSet);

        tLCPolSchema.setContNo(ContNo);
        tLCPolSchema.setProposalNo("11000049255");
        tLCPolSchema.setPrtNo("16000035243");
        tLCPolSchema.setManageCom("86310000");
        tLCPolSchema.setSaleChnl("02");
        tLCPolSchema.setAgentCode("3101000177");
        tLCPolSchema.setAgentGroup("000000000093");
        tLCPolSchema.setGrpContNo(GrpContNo);
        tLCPolSchema.setPolApplyDate("2006-10-20");
        tLCPolSchema.setRiskCode(RiskCode);
        tLCPolSchema.setPayIntv("12");
        tLCPolSchema.setPayMode("");
        tLCPolSchema.setRiskVersion("2002");
        tLCPolSchema.setCValiDate("2006-9-29");
        tLCPolSchema.setHealthCheckFlag("");
        tLCPolSchema.setPayLocation("");
        tLCPolSchema.setLiveGetMode("");

        tLCDutySchema.setPayIntv(tLCPolSchema.getPayIntv()); //交费方式
        tLCDutySchema.setInsuYear("1000"); //保险期间
        tLCDutySchema.setInsuYearFlag("A");
        tLCDutySchema.setPayEndYear("20"); //交费年期
        tLCDutySchema.setPayEndYearFlag("Y");
        tLCDutySchema.setAmnt(50000);
//        tLCDutySchema.setMult(1);

        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        tLCAppntSchema.setContNo(ContNo);
        tLCAppntSchema.setAppntNo("");
        tLCAppntSchema.setAppntName("");
        tLCAppntSchema.setAppntSex("");
        tLCAppntSchema.setAppntBirthday("");


        tLCGrpAppntSchema.setCustomerNo("000826417");
        tLCGrpAppntSchema.setGrpContNo(GrpContNo);

        tLCInsuredSchema.setContNo(ContNo);
        tLCInsuredSchema.setInsuredNo("000861147");  //客户号

        TransferData tTransferData = new TransferData();

        tTransferData.setNameAndValue("getIntv","1"); //领取间隔（方式）
        tTransferData.setNameAndValue("GetDutyKind", ""); //年金开始领取年龄
        tTransferData.setNameAndValue("samePersonFlag", "0"); //投保人同被保人标志
        tTransferData.setNameAndValue("deleteAccNo", "0");
        tTransferData.setNameAndValue("ChangePlanFlag", "0");
        tTransferData.setNameAndValue("EdorType", "NS");
        tTransferData.setNameAndValue("SavePolType", "2");

        /**********************************************************************/

        tLCContSchema.setContNo(ContNo);
        tVData.addElement(tLCDutySchema);
        tVData.addElement(tLCContSchema);
        tVData.addElement(tLCPolSchema);
        tVData.addElement(tLCAppntSchema);
        tVData.addElement(tLCGrpAppntSchema);
        tVData.addElement(tLCInsuredSchema);
        LCInsuredRelatedSet mLCInsuredRelatedSet = new LCInsuredRelatedSet();
        tVData.addElement(mLCInsuredRelatedSet);
        tVData.addElement(tTransferData);
        tVData.add(tG);
        tProposalUI = new ProposalUI();

        if (!tProposalUI.submitData(tVData, "INSERT||PROPOSAL"))
//                if (!tProposalUI.submitData(tVData,"UPDATE||PROPOSAL"))
//                if (!tProposalUI.submitData(tVData,"DELETE||PROPOSAL"))
        {
            System.out.println(tProposalUI.mErrors.getFirstError());
        }

    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        //this.mOperate =cOperate;

        //得到外部传入的数据,将数据备份到本类中
        //if (!getInputData(cInputData))
        //	return false;

        //进行业务处理
        //if (!dealData())
        //	return false;

        //准备往后台的数据
        //if (!prepareOutputData())
        //	return false;

        ProposalBL tProposalBL = new ProposalBL();
//System.out.println("Start Proposal UI Submit...");
        tProposalBL.submitData(cInputData, cOperate);
//System.out.println("End Proposal UI Submit...");

        //如果有需要处理的错误，则返回
        if (tProposalBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tProposalBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ProposalUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        mResult = tProposalBL.getResult();

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(mTransferData);
            mInputData.add(mLCPolSchema);
            mInputData.add(mLCAppntIndSchema);
            mInputData.add(mLCAppntGrpSchema);
            mInputData.add(mLCInsuredSet);
            mInputData.add(mLCBnfSet);
            mInputData.add(mLCCustomerImpartSet);
            mInputData.add(mLCSpecSet);
            mInputData.add(mLCDutySchema);

            if (this.mNeedDuty) {
                mInputData.add(mLCDutySet1);
            }
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        if (mOperate.equals("DELETE||PROPOSAL")) {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            // 保单
            mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName("LCPolSchema", 0));
        } else {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            // 前台向后台传递的数据容器：放置一些不能通过Schema传递的变量
            mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
            // 保单
            mLCPolSchema.setSchema((LCPolSchema) cInputData.getObjectByObjectName("LCPolSchema", 0));
            // 投保人
            mLCAppntIndSchema.setSchema((LCAppntIndSchema) cInputData.getObjectByObjectName(
                    "LCAppntIndSchema", 0));
            mLCAppntGrpSchema.setSchema((LCAppntGrpSchema) cInputData.getObjectByObjectName(
                    "LCAppntGrpSchema", 0));
            // 被保人
            mLCInsuredSet.set((LCInsuredSet) cInputData.getObjectByObjectName("LCInsuredSet", 0));
            // 受益人
            mLCBnfSet.set((LCBnfSet) cInputData.getObjectByObjectName("LCBnfSet", 0));
            // 告知信息
            mLCCustomerImpartSet.set((LCCustomerImpartSet) cInputData.getObjectByObjectName(
                    "LCCustomerImpartSet", 0));
            // 特别约定
            mLCSpecSet.set((LCSpecSet) cInputData.getObjectByObjectName("LCSpecSet", 0));

            // 一般责任信息
            if ((LCDutySchema) cInputData.getObjectByObjectName("LCDutySchema", 0) != null) {
                mLCDutySchema.setSchema((LCDutySchema) cInputData.getObjectByObjectName(
                        "LCDutySchema", 0));
            }

            // 可选责任

            LCDutySet tLCDutySet = (LCDutySet) cInputData.getObjectByObjectName(
                    "LCDutySet", 0);

            if (tLCDutySet == null) {
                mNeedDuty = false;
            } else {
                mNeedDuty = true;
            }
            mLCDutySet1.set(tLCDutySet);
            if (mTransferData == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalUI";
                tError.functionName = "getInputData";
                tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (mLCPolSchema == null || mLCAppntIndSchema == null || mLCInsuredSet == null ||
                mLCBnfSet == null || mLCCustomerImpartSet == null || mLCSpecSet == null) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ProposalBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "在投保数据接受时没有得到足够的数据，请您确认有：保单,投保人,被保人,受益人,告知信息,特别约定的信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
        } // end of if
        return true;
    }

    public VData getResult() {
        return mResult;
    }
}
