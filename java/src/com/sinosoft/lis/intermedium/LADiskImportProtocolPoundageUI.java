package com.sinosoft.lis.intermedium;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportProtocolPoundageUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportProtocolPoundageBL mLADiskImportProtocolPoundageBL = null;

    public LADiskImportProtocolPoundageUI()
    {
    	mLADiskImportProtocolPoundageBL = new LADiskImportProtocolPoundageBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportProtocolPoundageBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportProtocolPoundageBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportProtocolPoundageBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportProtocolPoundageBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportProtocolPoundageBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportProtocolPoundageBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportProtocolPoundageUI zLADiskImportProtocolPoundageUI = new LADiskImportProtocolPoundageUI();
    }
}
