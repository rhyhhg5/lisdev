package com.sinosoft.lis.intermedium;


import java.io.File;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LARateCommisionDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.lis.vschema.LMRiskSet;
import com.sinosoft.lis.agentconfig.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportProtocolPoundageBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "InterProtocolPoundageRate.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
	LAComSet mLAComSet =new LAComSet();
	LAComDB mLAComDB = new LAComDB();
	
    private LARateCommisionSet mLARateCommisionSetFinal=new LARateCommisionSet();
    
    private LARateCommisionSet mLARateCommisionSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private LMRiskAppSet mLMRiskAppSet = new LMRiskAppSet();
    private LMRiskAppDB mLMRiskAppDB = new LMRiskAppDB();
    private StringBuffer merrorInfo=new StringBuffer();
    public LADiskImportProtocolPoundageBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        //System.out.println("begin import");
        //从磁盘导入数据
        LAGetOtherChargeDiskImporter importer = new LAGetOtherChargeDiskImporter(fileName,
                configFileName,
                diskimporttype);
        //System.out.println("DiskImportTraRateCommisionBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        System.out.println("doimported");
        if (diskimporttype.equals("LARateCommision")) {
            mLARateCommisionSet = (LARateCommisionSet) importer
                                  .getSchemaSet();

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportTraRateCommisionBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"'  with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LARateCommision")) {
            if (mLARateCommisionSet == null) {
                mErrors.addOneError("导入手续费比率信息失败！没有得到要导入的数据。");
                return false;
            } else {
            	LMRiskDB tLMRiskDB=new LMRiskDB();
            	LMRiskSet tLMRiskSet=new LMRiskSet();
            	String tempSQL="select * from lmrisk where 1=1 with ur";
            	tLMRiskSet=tLMRiskDB.executeQuery(tempSQL);
            	tempSQL="select * from ldcom where length(trim(comcode))=8";
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LARateCommisionDB mTempLARateCommisionDB=new LARateCommisionDB();
            	//LARateCommisionSet mLARateCommisionSet=new LARateCommisionSet();
            	LARateCommisionSchema mTempLARateCommisionSchema=new LARateCommisionSchema();
            	LARateCommisionSchema mTempLARateCommisionSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
                for (int i = 1; i <= mLARateCommisionSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLARateCommisionSchema=mLARateCommisionSet.get(i);
                    if (mTempLARateCommisionSchema.getManageCom() == null ||
                    		mTempLARateCommisionSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构不能为空/");
                    }
                    if (mTempLARateCommisionSchema.getRiskCode() == null ||
                    		mTempLARateCommisionSchema.getRiskCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("险种编码不能为空/");
                    }
                    if (mTempLARateCommisionSchema.getF01() == null ||
                    		mTempLARateCommisionSchema.getF01().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("保费类型不能为空/");
                    }
                    if (mTempLARateCommisionSchema.getAgentCom() == null ||
                    		mTempLARateCommisionSchema.getAgentCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("代理机构不能为空/");
                    }
                    if (mTempLARateCommisionSchema.getCurYear() <= 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("保单年度不能为空/");
                    }
                    if (mTempLARateCommisionSchema.getPayIntv() == null ||
                    		mTempLARateCommisionSchema.getPayIntv().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("缴费方式不能为空/");
                    } 
                    if(!mTempLARateCommisionSchema.getPayIntv().equals("0")
                    		&&!mTempLARateCommisionSchema.getPayIntv().equals("12")&&!mTempLARateCommisionSchema.getPayIntv().equals("6")
                    		&&!mTempLARateCommisionSchema.getPayIntv().equals("3")&&!mTempLARateCommisionSchema.getPayIntv().equals("1")){
                    	this.merrorNum++;
                        this.merrorInfo.append("不能录入此缴费方式/");
                    }
                    if (mTempLARateCommisionSchema.getRate() < 0 
                    		||mTempLARateCommisionSchema.getRate()>1) {
                    	System.out.println("DiskImportTraRateCommisionBL.java: getRate:"+mLARateCommisionSet.get(i).getRate());
                        this.merrorNum++;
                        this.merrorInfo.append("佣金比率应介于０与１之间/");
                    } 
                    if (mTempLARateCommisionSchema.getF03()<= 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("缴费年期不能为空或0/");
                    }
                    if(mTempLARateCommisionSchema.getManageCom()!=null
                    		&&!mTempLARateCommisionSchema.getManageCom().equals("")
                    		&&!mTempLARateCommisionSchema.getManageCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLARateCommisionSchema.getManageCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构代码不存在/");
                        }
                    }
                    int tempFlag2=0;
                    if(mTempLARateCommisionSchema.getRiskCode()!=null
                    		&&!mTempLARateCommisionSchema.getRiskCode().equals("")){
                    	for(int n=1;n<=tLMRiskSet.size();n++){
                        	if(tLMRiskSet.get(n).getRiskCode()
                        			.equals(mTempLARateCommisionSchema.getRiskCode())){
                        		tempFlag2=1;
                        		break;
                        	}
                        }
                        if(tempFlag2==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("险种编码不存在/");
                        }
                    }
                    int tempFlag3=0;
                    if(mTempLARateCommisionSchema.getAgentCom()!=null
                    		&&!mTempLARateCommisionSchema.getAgentCom().equals("")){
                    	String tAgentComSQL = "select * from lacom where managecom ='"+mTempLARateCommisionSchema.getManageCom()+"' and branchtype=" +
                    			"'"+mTransferData.getValueByName("branchtype")+"' and branchtype2='"+mTransferData.getValueByName("branchtype2")+"' ";
                        mLAComSet = mLAComDB.executeQuery(tAgentComSQL);
                        for(int n=1;n<=mLAComSet.size();n++){
                        	if(mLAComSet.get(n).getAgentCom()
                        			.equals(mTempLARateCommisionSchema.getAgentCom())){
                        		tempFlag3=1;
                        		break;
                        	}
                        }
                    	if(tempFlag3==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("该代理机构"+mTempLARateCommisionSchema.getAgentCom()+"不在对应的"+mTempLARateCommisionSchema.getManageCom()+"管理机构下/");
                        }
                    }
                    //添加保费类型的校验
                    if(mTempLARateCommisionSchema.getF01()!=null
                    		&&!mTempLARateCommisionSchema.getF01().equals("")){
                    	String tRiskCode =mTempLARateCommisionSchema.getRiskCode();
                    	String tF01Code = mTempLARateCommisionSchema.getF01();
                    	String tsql ="select * from lmriskapp where 1=1 and risktype4<>'4' and riskcode ='"+tRiskCode+"' with ur";
                    mLMRiskAppSet=(LMRiskAppSet)mLMRiskAppDB.executeQuery(tsql);
                    if(mLMRiskAppSet.size()>0){
                    	if(!"10".equals(tF01Code)){
                    		this.merrorNum++;
                            this.merrorInfo.append("普通险种"+mTempLARateCommisionSchema.getRiskCode()+"保费类型为10!/");
                    	}
                    	
                    }
                    
                    }
                    //判断险种是否停售
                   if(mTempLARateCommisionSchema.getRiskCode()!=null &&
                		   !mTempLARateCommisionSchema.getRiskCode().equals("")){
                	   String tRiskCode =mTempLARateCommisionSchema.getRiskCode();
                	   String tsql ="select * from lmriskapp where 1=1  and riskcode ='"+tRiskCode+"' and enddate <'"+PubFun.getCurrentDate()+"' and enddate is not null with ur";
                	   mLMRiskAppSet=(LMRiskAppSet)mLMRiskAppDB.executeQuery(tsql);
                	   if(mLMRiskAppSet.size()>0){
                		   this.merrorNum++;
                           this.merrorInfo.append("险种"+mTempLARateCommisionSchema.getRiskCode()+"已经失效，不是在售险种!/");
                	   }
                   } 
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLARateCommisionSet.size(); j++){
                        	mTempLARateCommisionSchema2=mLARateCommisionSet.get(j);
                        	if(mTempLARateCommisionSchema.getManageCom()
                        			.equals(mTempLARateCommisionSchema2.getManageCom())
                        			&&mTempLARateCommisionSchema.getF01()
                        			.equals(mTempLARateCommisionSchema2.getF01())
                        			&&mTempLARateCommisionSchema.getRiskCode()
                        			.equals(mTempLARateCommisionSchema2.getRiskCode())
                        			&&mTempLARateCommisionSchema.getCurYear()
                        			==mTempLARateCommisionSchema2.getCurYear()
                        			&&mTempLARateCommisionSchema.getPayIntv()
                        			.equals(mTempLARateCommisionSchema2.getPayIntv())
                        			&&mTempLARateCommisionSchema.getF03()
                        			==mTempLARateCommisionSchema2.getF03()
                        			&&mTempLARateCommisionSchema.getAgentCom()
                        			.equals(mTempLARateCommisionSchema2.getAgentCom())){
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLARateCommisionSchema,mTempLARateCommisionDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LARateCommisionSchema mLARateCommisionSchema,LARateCommisionDB mLARateCommisionDB){

	LARateCommisionSet tempLARateCommisionSet=new LARateCommisionSet();
	String tempSql="select * from laratecommision where managecom='"
		+mLARateCommisionSchema.getManageCom()+"' and riskcode='"
		+mLARateCommisionSchema.getRiskCode()+"'and f01='"
		+mLARateCommisionSchema.getF01()+"'  and payintv='"
		+mLARateCommisionSchema.getPayIntv()+"' and curyear="
		+mLARateCommisionSchema.getCurYear()+" and f03="
		+mLARateCommisionSchema.getF03()+" and f06 = '10'"
		+" and agentcom='"
		+mLARateCommisionSchema.getAgentCom()+"' and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")+"'";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLARateCommisionSet=mLARateCommisionDB.executeQuery(tempSql);
	if(tempLARateCommisionSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LARateCommisionSchema tempLARateCommisionSchema=new LARateCommisionSchema();
		tempLARateCommisionSchema.setManageCom(mLARateCommisionSchema.getManageCom());
		tempLARateCommisionSchema.setRiskCode(mLARateCommisionSchema.getRiskCode());
		tempLARateCommisionSchema.setRate(mLARateCommisionSchema.getRate());
		tempLARateCommisionSchema.setCurYear(mLARateCommisionSchema.getCurYear());
		tempLARateCommisionSchema.setF03(mLARateCommisionSchema.getF03());
		tempLARateCommisionSchema.setAgentCom(mLARateCommisionSchema.getAgentCom());
		tempLARateCommisionSchema.setPayIntv(mLARateCommisionSchema.getPayIntv());
		tempLARateCommisionSchema.setF01(mLARateCommisionSchema.getF01());
		//tempLARateCommisionSchema=mLARateCommisionSchema;
		mLARateCommisionSetFinal.add(tempLARateCommisionSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	int idx=Integer.parseInt(idxTemp)+1;
    	String idxTempCompare = (String) mTransferData.getValueByName("idxCompare");
    	int idxCompare=0;
    	if(idxTempCompare!=null&&!idxTempCompare.equals("")){
    		idxCompare=Integer.parseInt(idxTempCompare)+1;
    	}else{
    		idxCompare=1;
    	}
    	System.out.println("prepareData:idxTempCompare"+idxCompare);
        if (diskimporttype.equals("LARateCommision")) {
        	System.out.println("prepareData:doing");
            importPersons=mLARateCommisionSetFinal.size();
            //System.out.println("prepareData:unImportPersons"+unImportPersons);
            if(mLARateCommisionSetFinal!=null&&mLARateCommisionSetFinal.size()>0){
             	//importPersons = mLARateCommisionSetFinal.size();
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLARateCommisionSetFinal.size(); i++) {
              	System.out.println("preparedata : managecom"+mLARateCommisionSetFinal.get(i).getManageCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	//System.out.println("BL:branchtype  "+branchtype);
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	//System.out.println("BL:branchtype2  "+branchtype2);
              	mLARateCommisionSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLARateCommisionSetFinal.get(i).setIdx(idx++);
              	mLARateCommisionSetFinal.get(i).setBranchType(branchtype);
              	mLARateCommisionSetFinal.get(i).setBranchType2(branchtype2); 
              	mLARateCommisionSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLARateCommisionSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLARateCommisionSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLARateCommisionSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	mLARateCommisionSetFinal.get(i).setYear("0");
              	mLARateCommisionSetFinal.get(i).setAppAge(0);
              	mLARateCommisionSetFinal.get(i).setF02("A");
              	mLARateCommisionSetFinal.get(i).setF05("0");
              	mLARateCommisionSetFinal.get(i).setF06("10");
              	mLARateCommisionSetFinal.get(i).setsex("A");
              	//mLARateCommisionSet.get(i).setRateType("11");
              	mLARateCommisionSetFinal.get(i).setVersionType("11");
              	mmap.put(mLARateCommisionSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

//        DiskImportTraRateCommisionBL d = new DiskImportTraRateCommisionBL();
//        d.submitData(v, "INSERT");
//
//        if (d.mErrors.needDealError()) {
//            System.out.println(d.mErrors.getErrContent());
//        }
    }

}
