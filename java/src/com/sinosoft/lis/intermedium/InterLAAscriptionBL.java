/*
 * <p>ClassName: HDLAAscriptionBL </p>
 * <p>Description: HDLAAscriptionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.intermedium;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAOrphanPolicyDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.schema.LAOrphanPolicyBSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAOrphanPolicyBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.pubfun.MMap;

public class InterLAAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private MMap mMap = new MMap();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();

    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();

    private String message="";
    private String AgentNew1 = "";

    private String tContNo1 = "";
    private String tGrpContNo1 = "";

    private String updateSQL = "";

    private LAAscriptionSchema preLAAscriptionSchema = new LAAscriptionSchema();

    private LAOrphanPolicyBSet tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();

    private LAOrphanPolicySet delLAOrphanPolicyBSet = new LAOrphanPolicySet();
    private  String currentDate = PubFun.getCurrentDate();//当前日期
    private  String currentTime = PubFun.getCurrentTime();//当期时间
    private String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);

    public InterLAAscriptionBL()
    {

    }

    public static void main(String[] args)
    {

    }
    
    public String getMessage()
    {
    	return this.message;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAAscriptionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean dealData()
    {
        boolean tReturn = true;
        int tCount = 0;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tSQL = "select a.agentgroup,a.branchattr from labranchgroup a,laagent b where a.agentgroup=b.agentgroup and b.agentcode='"
                + mLAAscriptionSchema.getAgentOld() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询原业务员相应的信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }

        int a = tSSRS.getMaxRow();
        if (a != 1)
        {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查到原业务员相应的信息";
            this.mErrors.addOneError(tError);
            return false;
        }
        String mAgentGroup = tSSRS.GetText(1, 1);
        String mBranchAttr = tSSRS.GetText(1, 2);
        String strsql = "select count(ascripno)+1 from laascription where 1=1 and "
                + "ascripstate='3' and contno='"
                + mLAAscriptionSchema.getContNo() + "'";

        ExeSQL tExeSQL1 = new ExeSQL();
        String mCount = tExeSQL1.getOneValue(strsql);
        if (tExeSQL1.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询归属表信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        int mCountNO = Integer.parseInt(mCount);//得到归属次数
          //互动直销
        if (mOperate.equals("DIRECT||MAIN"))
        {   
            String tAgentOld = mLAAscriptionSchema.getAgentOld();
            String tContNo = mLAAscriptionSchema.getContNo();
            String tGrpContNo = mLAAscriptionSchema.getGrpContNo();
            System.out.println("………………开始进行插入操作…………………");
            System.out.println("获取的个单号为：" + tContNo);
            System.out.println("获取的团单号为：" + tGrpContNo);
            String oldAgentGroupSQL = "select agentgroup from laagent where agentcode = '"+mLAAscriptionSchema.getAgentOld()+"'";
        	String newAgentGroupSQL =" select agentgroup from laagent where agentcode ='"+mLAAscriptionSchema.getAgentNew()+"'";
        	ExeSQL agentGroupExeSQL = new ExeSQL();
        	String oldAgentGroup = agentGroupExeSQL.getOneValue(oldAgentGroupSQL);
            String newAgentGroup = agentGroupExeSQL.getOneValue(newAgentGroupSQL);
            if(!oldAgentGroup.equals(newAgentGroup))
            {
            	  CError tError = new CError();
                  tError.moduleName = "HDLAAscriptionBL";
                  tError.functionName = "checkData";
                  tError.errorMessage = "原互动专员与新互动专员不在同一销售机构！不能归属" ;
                  this.mErrors.addOneError(tError);
                  return false;
            }
            // 判断是人对人的归属还是单个保单对人的归属****************** 
            if ((tContNo == null || tContNo.equals(""))&&(tGrpContNo == null || tGrpContNo.equals("")))
            {
                //可实现批量归属
                System.out.println("人对人进行批量归属！");
                String allSQL = "select distinct(contno) from lccont where agentcode='"
                        + tAgentOld + "' and uwflag<>'a' and grpcontno='00000000000000000000'";
                ExeSQL allExeSQL = new ExeSQL();
                SSRS allSSRS = new SSRS();
                allSSRS = allExeSQL.execSQL(allSQL);
                System.out.println("大小：" + allSSRS.getMaxRow());
                if (allSSRS.getMaxRow() > 0)
                {
                    for (int i = 1; i <= allSSRS.getMaxRow(); i++)
                    {
                        //对每个保单进行循环处理
                        //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                        String Ascrip = "select getUniteCode(agentnew) from LAAscription where agentold='"
                            + tAgentOld
                            + "' and ascripstate='2' and contno='"
                            + allSSRS.GetText(i, 1) + "' and maketype='01' and validflag='N'";
                        ExeSQL aExeSQL = new ExeSQL();
                        String flag = aExeSQL.getOneValue(Ascrip);
                        if(flag!=null&&!flag.equals(""))
                        {
                            CError tError = new CError();
                            tError.moduleName = "HDLAAscriptionBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = ""+allSSRS.GetText(i, 1)+" 保单已经归属给代理人"+flag+"，并未确认，请确认！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        //判断是否为孤儿单
                        tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
                        delLAOrphanPolicyBSet = new LAOrphanPolicySet();
                        LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                        LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                        //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                        tmLAOrphanPolicyDB.setContNo(allSSRS.GetText(i, 1));
                        if (!tmLAOrphanPolicyDB.getInfo())
                        {

                        }
                        else
                        {
                            tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                            //备份
                            LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                            Reflections tReflections = new Reflections();
                            tReflections.transFields(tmLAOrphanPolicyBSchema,tmLAOrphanPolicySchema);
                            tmLAOrphanPolicyBSchema.setEdorType("01");
                            tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                            tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                            tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                            tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                            tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                            tmLAOrphanPolicyBSchema.setOperator(this.mGlobalInput.Operator);
                            mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                            mMap.put(tmLAOrphanPolicySchema, "DELETE");
                        }
                        //判断是否为归属未确认的保单
                        String Ascripfail = "select 'Y' from LAAscription where agentold='"
                                + tAgentOld
                                + "' and ascripstate<>'3' and contno='"
                                + allSSRS.GetText(i, 1) + "' and maketype='02' and validflag='N'";
                        ExeSQL failExeSQL = new ExeSQL();
                        String failflag = failExeSQL.getOneValue(Ascripfail);
                        if (failflag != null && !failflag.equals(""))
                        {
                           updateSQL= "update LAAscription "
                                    +" set ValidFlag='Y'"
                                    +",modifydate='"+ currentDate+ "'"
                                    +",modifytime='"+ currentTime +  "'"
                                    +",operator='"+ this.mGlobalInput.Operator+ "' "
                                    +" where agentold='"+ mLAAscriptionSchema.getAgentOld() + "'"
                                    +" and ValidFlag='N'"
                                    +" and AscripState<>'3' "
                                    +" and  contno='"+ allSSRS.GetText(i, 1) + "' ";
                            mMap.put(updateSQL, "UPDATE");
                        }
                        //进行数据插入LAAscription的准备
                        String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
                        System.out.println("mAscripNo:" + mAscripNo);
                        System.out.println("Contno:" + allSSRS.GetText(i, 1));
                        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                        tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
                        tLAAscriptionSchema.setAscripNo(mAscripNo);
                        tLAAscriptionSchema.setValidFlag("N");
                        tLAAscriptionSchema.setAClass("01");
                        tLAAscriptionSchema.setContNo(allSSRS.GetText(i, 1));
                        tLAAscriptionSchema.setAgentGroup(mAgentGroup);
                        tLAAscriptionSchema.setBranchAttr(mBranchAttr);
                        tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                        tLAAscriptionSchema.setMakeDate(currentDate);
                        tLAAscriptionSchema.setMakeTime(currentTime);
                        tLAAscriptionSchema.setModifyDate(currentDate);
                        tLAAscriptionSchema.setModifyTime(currentTime);
                        tLAAscriptionSchema.setAscriptionCount(mCountNO);
                        tLAAscriptionSchema.setAscripState("2");
                        mLAAscriptionSet.add(tLAAscriptionSchema);
                        mMap.put(tLAAscriptionSchema, "INSERT");

                    }
                }
                else
                {
                	message = "没有要归属的保单!";
                }
                
                
                String allGrpSQL = "select distinct(grpcontno) from lcgrpcont where agentcode='"
                        + tAgentOld + "' and uwflag<>'a'";
                ExeSQL allGrpExeSQL = new ExeSQL();
                SSRS allGrpSSRS = new SSRS();
                allGrpSSRS = allGrpExeSQL.execSQL(allGrpSQL);
                System.out.println("大小：" + allGrpSSRS.getMaxRow());
                if (allGrpSSRS.getMaxRow() > 0)
                {
                    for (int i = 1; i <= allGrpSSRS.getMaxRow(); i++)
                    {
                        //对每个保单进行循环处理
                        //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                        String Ascrip = "select getUniteCode(agentnew) from LAAscription where agentold='"
                            + tAgentOld
                            + "' and ascripstate='2' and grpcontno='"
                            + allGrpSSRS.GetText(i, 1) + "' and maketype='01' and validflag='N'";
                        ExeSQL aExeSQL = new ExeSQL();
                        String flag = aExeSQL.getOneValue(Ascrip);
                        if(flag!=null&&!flag.equals(""))
                        {
                            CError tError = new CError();
                            tError.moduleName = "HDLAAscriptionBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = ""+allGrpSSRS.GetText(i, 1)+" 保单已经归属给代理人"+flag+"，并未确认，请确认！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        //判断是否为孤儿单
                        tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
                        delLAOrphanPolicyBSet = new LAOrphanPolicySet();
                        LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                        LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                        //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                        tmLAOrphanPolicyDB.setContNo(allGrpSSRS.GetText(i, 1));
                        if (!tmLAOrphanPolicyDB.getInfo())
                        {                
                        }
                        else
                        {
                            tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                            //备份
                            LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                            Reflections tReflections = new Reflections();
                            tReflections.transFields(tmLAOrphanPolicyBSchema,tmLAOrphanPolicySchema);
                            tmLAOrphanPolicyBSchema.setEdorType("01");
                            tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                            tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                            tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                            tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                            tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                            tmLAOrphanPolicyBSchema.setOperator(this.mGlobalInput.Operator);
                            mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                            mMap.put(tmLAOrphanPolicySchema, "DELETE");
                        }
                        //判断是否为归属未确认的保单
                        String Ascripfail = "select 'Y' from LAAscription where agentold='"
                                + tAgentOld
                                + "' and ascripstate<>'3' and grpcontno='"
                                + allGrpSSRS.GetText(i, 1) + "' and maketype='02' and validflag='N'";
                        ExeSQL failExeSQL = new ExeSQL();
                        String failflag = failExeSQL.getOneValue(Ascripfail);
                        if (failflag != null && !failflag.equals(""))
                        {
                           updateSQL= "update LAAscription "
                                    +" set ValidFlag='Y'"
                                    +",modifydate='"+ currentDate+ "'"
                                    +",modifytime='"+ currentTime + "'"
                                    +",operator='"+ this.mGlobalInput.Operator+ "' "
                                    +" where agentold='"+ mLAAscriptionSchema.getAgentOld() + "'"
                                    +" and ValidFlag='N'"
                                    +" and AscripState<>'3' "
                                    +" and  grpcontno='"+ allGrpSSRS.GetText(i, 1) + "' ";
                            mMap.put(updateSQL, "UPDATE");
                        }
                        //进行数据插入LAAscription的准备
                        String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
                        System.out.println("mAscripNo:" + mAscripNo);
                        System.out.println("Contno:" + allGrpSSRS.GetText(i, 1));
                        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                        tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
                        tLAAscriptionSchema.setAscripNo(mAscripNo);
                        tLAAscriptionSchema.setValidFlag("N");
                        tLAAscriptionSchema.setAClass("01");
                        tLAAscriptionSchema.setGrpContNo(allGrpSSRS.GetText(i, 1));
                        tLAAscriptionSchema.setAgentGroup(mAgentGroup);
                        tLAAscriptionSchema.setBranchAttr(mBranchAttr);
                        tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                        tLAAscriptionSchema.setMakeDate(currentDate);
                        tLAAscriptionSchema.setMakeTime(currentTime);
                        tLAAscriptionSchema.setModifyDate(currentDate);
                        tLAAscriptionSchema.setModifyTime(currentTime);
                        tLAAscriptionSchema.setAscriptionCount(mCountNO);
                        tLAAscriptionSchema.setAscripState("2");
                        mLAAscriptionSet.add(tLAAscriptionSchema);
                        mMap.put(tLAAscriptionSchema, "INSERT");
                    }
                }
                
            }
            else
            //单张保单归属*******
            {
                if (tContNo != null && !tContNo.equals("")){
                //判断是否为已撤销的保单*******个单
                String ttsql = "select 'X' from lccont where contno='"+ tContNo + "' and Agentcode='"
                               + tAgentOld+ "' and uwflag='a'";
                ExeSQL ttExeSQL = new ExeSQL();
                String ttFlag = ttExeSQL.getOneValue(ttsql);
                if (ttExeSQL.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询保单表信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (ttFlag != null && !ttFlag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = " 已做撤销的保单不做归属！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                String Ascrip = "select getUniteCode(agentnew) from LAAscription "
                                +" where agentold='"+ tAgentOld
                                +"' and ascripstate='2' and contno='"
                                + tContNo + "' and maketype='01' and validflag='N'";
                ExeSQL aExeSQL = new ExeSQL();
                String flag = aExeSQL.getOneValue(Ascrip);
                if(flag!=null&&!flag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = ""+tContNo+" 保单已经归属给代理人"+flag+",并未确认，请执行归属确认！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否为孤儿单
                LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                tmLAOrphanPolicyDB.setContNo(tContNo);
                if (!tmLAOrphanPolicyDB.getInfo())
                {
                }
                else
                {
                    tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                    //备份
                    LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tmLAOrphanPolicyBSchema,
                            tmLAOrphanPolicySchema);
                    tmLAOrphanPolicyBSchema.setEdorType("02");
                    tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                    tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                    tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                    tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                    tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                    tmLAOrphanPolicyBSchema
                            .setOperator(this.mGlobalInput.Operator);
                    mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                    mMap.put(tmLAOrphanPolicySchema, "DELETE");
                }
                //判断是否为自动归属失败的保单
                String Ascripfail = "select 'Y' from LAAscription where agentold='"
                        + tAgentOld
                        + "'and ascripstate<>'3' and contno='"
                        + tContNo + "' and maketype='02' and validflag='N'";
                ExeSQL failExeSQL = new ExeSQL();
                String failflag = failExeSQL.getOneValue(Ascripfail);
                if (failflag != null && !failflag.equals(""))
                {
                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                            + currentDate
                            + "',modifytime='"
                            + currentTime
                            + "',operator='"
                            + this.mGlobalInput.Operator
                            + "' where agentold='"
                            + mLAAscriptionSchema.getAgentOld()
                            + "'  and ValidFlag='N'"
                            + " and MakeType='02' and ascripstate<>'3' and contno='"
                            + tContNo + "'";
                    mMap.put(updateSQL, "UPDATE");
                }
                String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
                        "PerAscripNo", 20);
                mLAAscriptionSchema.setAscripNo(mAscripNo);
                mLAAscriptionSchema.setValidFlag("N");
                mLAAscriptionSchema.setAClass("01");
                mLAAscriptionSchema.setAgentGroup(mAgentGroup);
                mLAAscriptionSchema.setBranchAttr(mBranchAttr);
                mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                mLAAscriptionSchema.setMakeDate(currentDate);
                mLAAscriptionSchema.setMakeTime(currentTime);
                mLAAscriptionSchema.setModifyDate(currentDate);
                mLAAscriptionSchema.setModifyTime(currentTime);
                mLAAscriptionSchema.setAscriptionCount(mCountNO);
                mLAAscriptionSchema.setAscripState("2");
                mLAAscriptionSet.add(mLAAscriptionSchema);
                mMap.put(mLAAscriptionSchema, "INSERT");
                }
                else{
                //判断是否为已撤销的保单****团单
                String ttsql = "select 'X' from lcgrpcont where grpcontno='"+ tGrpContNo + "' and Agentcode='"
                               + tAgentOld+ "' and uwflag='a'";
                ExeSQL ttExeSQL = new ExeSQL();
                String ttFlag = ttExeSQL.getOneValue(ttsql);
                if (ttExeSQL.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询保单表信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (ttFlag != null && !ttFlag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = " 已做撤销的保单不做归属！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                String Ascrip = "select getUniteCode(agentnew) from LAAscription "
                                +" where agentold='"+ tAgentOld
                                +"' and ascripstate='2' and grpcontno='"
                                + tContNo + "' and maketype='01' and validflag='N'";
                ExeSQL aExeSQL = new ExeSQL();
                String flag = aExeSQL.getOneValue(Ascrip);
                if(flag!=null&&!flag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = ""+tContNo+" 保单已经归属给代理人"+flag+",并未确认，请确认！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否为孤儿单
                LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                tmLAOrphanPolicyDB.setContNo(tContNo);
                if (!tmLAOrphanPolicyDB.getInfo())
                {
                }
                else
                {
                    tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                    //备份
                    LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tmLAOrphanPolicyBSchema,
                            tmLAOrphanPolicySchema);
                    tmLAOrphanPolicyBSchema.setEdorType("01");
                    tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                    tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                    tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                    tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                    tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                    tmLAOrphanPolicyBSchema
                            .setOperator(this.mGlobalInput.Operator);
                    mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                    mMap.put(tmLAOrphanPolicySchema, "DELETE");
                }
                //判断是否为自动归属失败的保单
                String Ascripfail = "select 'Y' from LAAscription where agentold='"
                        + tAgentOld
                        + "'and ascripstate<>'3' and grpcontno='"
                        + tContNo + "' and maketype='02' and validflag='N'";
                ExeSQL failExeSQL = new ExeSQL();
                String failflag = failExeSQL.getOneValue(Ascripfail);
                if (failflag != null && !failflag.equals(""))
                {
                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                            + currentDate
                            + "',modifytime='"
                            + currentTime
                            + "',operator='"
                            + this.mGlobalInput.Operator
                            + "' where agentold='"
                            + mLAAscriptionSchema.getAgentOld()
                            + "'  and ValidFlag='N'"
                            + " and MakeType='02' and ascripstate<>'3' and grpcontno='"
                            + tContNo + "'";
                    mMap.put(updateSQL, "UPDATE");
                }
                String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
                        "PerAscripNo", 20);
                mLAAscriptionSchema.setAscripNo(mAscripNo);
                mLAAscriptionSchema.setValidFlag("N");
                mLAAscriptionSchema.setAClass("01");
                mLAAscriptionSchema.setAgentGroup(mAgentGroup);
                mLAAscriptionSchema.setBranchAttr(mBranchAttr);
                mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                mLAAscriptionSchema.setMakeDate(currentDate);
                mLAAscriptionSchema.setMakeTime(currentTime);
                mLAAscriptionSchema.setModifyDate(currentDate);
                mLAAscriptionSchema.setModifyTime(currentTime);
                mLAAscriptionSchema.setAscriptionCount(mCountNO);
                mLAAscriptionSchema.setAscripState("2");
                mLAAscriptionSet.add(mLAAscriptionSchema);
                mMap.put(mLAAscriptionSchema, "INSERT");
                
            }
                
            }
            LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
            tLAAscriptionSet.set(mLAAscriptionSet);
            mLAAscriptionSet.clear();
            System.out.println("需要处理数据数目"+tLAAscriptionSet.size());
            if(!dealCont(tLAAscriptionSet)){
            	return false;
        }
        }
        //互动中介
        if (mOperate.equals("INTER||MAIN"))
        {   
        	String AgentNew =  this.mLAAscriptionSchema.getAgentNew();//新的代理人
            String AgentOld = this.mLAAscriptionSchema.getAgentOld();//原代理人
            String AgentComNew =  this.mLAAscriptionSchema.getAgentComNew();//新的代理机构
            String AgentComOld = this.mLAAscriptionSchema.getAgentComOld();//原代理机构
            String ContNo = this.mLAAscriptionSchema.getContNo();
            String GrpContNo= this.mLAAscriptionSchema.getGrpContNo();
            String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
                    "PerAscripNo", 20);
            System.out.println("mAscripNo:"+mAscripNo);
            //插入归属表
            LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
            tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
            tLAAscriptionSchema.setAscripNo(mAscripNo);
            tLAAscriptionSchema.setValidFlag("N");
            tLAAscriptionSchema.setAClass("01");
            tLAAscriptionSchema.setContNo(ContNo);
            tLAAscriptionSchema.setGrpContNo(GrpContNo);
            tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
            tLAAscriptionSchema.setMakeDate(currentDate);
            tLAAscriptionSchema.setMakeTime(currentTime);
            tLAAscriptionSchema.setModifyDate(currentDate);
            tLAAscriptionSchema.setModifyTime(currentTime);
            tLAAscriptionSchema.setAscriptionCount(mCountNO);
            mMap.put(tLAAscriptionSchema, "INSERT");
            String agentSQL =" select a.agentgroup,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
                            +" (select branchseries from labranchgroup where agentgroup=a.agentgroup),a.branchcode,a.name from laagent a "
                            +" where agentcode='"+AgentNew+"' and branchtype='"+this.mLAAscriptionSchema.getBranchType()+"'"
                            +" and branchtype2='"+this.mLAAscriptionSchema.getBranchType2()+"'";
            tSSRS = tExeSQL.execSQL(agentSQL);
            if(tSSRS.getMaxRow()<0)
            {
                     // @@错误处理
              this.mErrors.copyAllErrors(tSSRS.mErrors);
              CError tError = new CError();
              tError.moduleName = "LABankContAgentBL";
              tError.functionName = "submitDat";
              tError.errorMessage = "人员新政信息查询出错!";
              this.mErrors.addOneError(tError);
              return false;
            }
            String AgentGroup = tSSRS.GetText(1, 1);
            String Name =  tSSRS.GetText(1, 5);

            String oldBranchSeries="";
            String newBranchSeries="";
            String newBranchAttr="";
            String oldAgentgroup="";
            String newAgentgroup="";

            String oldBranchSeriesSQL="select branchseries from labranchgroup where branchtype='"+this.mLAAscriptionSchema.getBranchType()+"' and agentgroup=(select agentgroup from laagent where agentcode='"+AgentOld+"')";
            SSRS mSSRS00 = new SSRS();
            ExeSQL mExeSQL00 = new ExeSQL();
            mSSRS00 = mExeSQL00.execSQL(oldBranchSeriesSQL);
            oldBranchSeries=mSSRS00.GetText(1, 1);

            String newBranchSeriesSQL="select branchseries,branchattr from labranchgroup where branchtype='"+this.mLAAscriptionSchema.getBranchType()+"' and agentgroup=(select agentgroup from laagent where agentcode='"+AgentNew+"')";
            SSRS mSSRS01 = new SSRS();
            ExeSQL mExeSQL01 = new ExeSQL();
            mSSRS01 = mExeSQL01.execSQL(newBranchSeriesSQL);
            newBranchSeries=mSSRS01.GetText(1, 1);
            newBranchAttr=mSSRS01.GetText(1, 2);

            String oldAgentgroupSQL="select agentgroup from laagent where agentcode='"+AgentOld+"'";
            SSRS mSSRS02 = new SSRS();
            ExeSQL mExeSQL02 = new ExeSQL();
            mSSRS02 = mExeSQL02.execSQL(oldAgentgroupSQL);
            oldAgentgroup=mSSRS02.GetText(1, 1);

            String newAgentgroupSQL="select agentgroup from laagent where agentcode='"+AgentNew+"'";
            SSRS mSSRS03 = new SSRS();
            ExeSQL mExeSQL03 = new ExeSQL();
            mSSRS03 = mExeSQL03.execSQL(newAgentgroupSQL);
            newAgentgroup=mSSRS03.GetText(1, 1);
            System.out.println("~~~~~~~~~~"+oldBranchSeries.substring(0,11));
            if(ContNo != null && !"".equals(ContNo))
            {
            //需要反冲
            if(!oldBranchSeries.substring(0,12).equals(newBranchSeries.substring(0,12)))
            {
                LJAPayDB tLJAPayDB=new LJAPayDB();
                LJAPayPersonDB tLJAPayPersonDB=new LJAPayPersonDB();
                LJTempFeeDB tLJTempFeeDB =new LJTempFeeDB();

          	  String tManageComSQL="select managecom from laagent where agentcode='"+AgentOld+"'";
          	  String ManageCom="";
          	  SSRS mSSRS0 = new SSRS();
                ExeSQL mExeSQL0 = new ExeSQL();
                mSSRS0 = mExeSQL0.execSQL(tManageComSQL);
                ManageCom=mSSRS0.GetText(1, 1);

                String CH1SQL=" select * from ljapay where payno in (select payno from ljapayperson where contno='"+ContNo +"'"
                             +" and agentcode='"+AgentOld+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"')";
          	  LJAPaySet tLJAPaySet=tLJAPayDB.executeQuery(CH1SQL);
          	  if(tLJAPaySet.size()>0)
      	  {
                   System.out.println("~~~~~~~"+tLJAPaySet.size());
      	     String tLimit = PubFun.getNoLimit(ManageCom);

                   for(int i=1;i<=tLJAPaySet.size();i++)
                   {
                       LJAPaySchema tLJAPaySchema  = new LJAPaySchema();
                       tLJAPaySchema = tLJAPaySet.get(i);
                       String tPayNo=PubFun1.CreateMaxNo("PAYNO", tLimit);
                       String sql1 = "insert into LJAPay (select '"+tPayNo+"', IncomeNo, IncomeType, AppntNo, SumActuPayMoney,"
                                   +"PAYDATE, ENTERACCDATE, '"+currentDate+"', ApproveCode, APPROVEDATE,'"+tPayNo+"', Operator,"
                                   +" '"+currentDate+"','"+currentTime+"', "
                                   +"GetNoticeNo, '"+currentDate+"', '"+currentTime+"', ManageCom, '"+mLAAscriptionSchema.getAgentComNew()+"', AgentType, BankCode,"
                                   +"BankAccNo, RiskCode, '"+AgentNew+"', '"+newAgentgroup+"', AccName, STARTPAYDATE, PayTypeFlag,"
                                   +"PrtStateFlag, PrintTimes,FinState,duefeetype,markettype,salechnl "
                                   +" from LJAPay where IncomeNo='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"')";
      		 String sql2 = "insert into LJAPayPerson select POLNO, PAYCOUNT, GRPCONTNO, GRPPOLNO, CONTNO, MANAGECOM, '"+mLAAscriptionSchema.getAgentComNew()+"',"
                                   +"AGENTTYPE, RISKCODE, '"+AgentNew+"', '"+newAgentgroup+"', PAYTYPEFLAG, APPNTNO, '"+tPayNo+"', "
                                   +"PAYAIMCLASS, DUTYCODE, PAYPLANCODE, SUMDUEPAYMONEY, SUMACTUPAYMONEY, PAYINTV"
                                   +", PAYDATE, PAYTYPE, ENTERACCDATE, '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE,"
                                   +"ININSUACCSTATE, APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO, OPERATOR,"
                                   +"'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', FINSTATE,MoneyNoTax,MoneyTax,BusiType,TaxRate from"
                                   +" LJAPAYPERSON where  CONTNO='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";

      		tPayNo=PubFun1.CreateMaxNo("PAYNO", tLimit);
                      String sql3 = "insert into LJAPay (select '"+tPayNo+"', IncomeNo, IncomeType, AppntNo, (-1)*SumActuPayMoney,"
                                  +"PAYDATE, ENTERACCDATE, '"+currentDate+"', ApproveCode, APPROVEDATE, '"+tPayNo+"', Operator,"
                                  +" '"+currentDate+"','"+currentTime+"', "
                                  +"GetNoticeNo, '"+currentDate+"', '"+currentTime+"', ManageCom, AgentCom, AgentType, BankCode,"
                                  +"BankAccNo, RiskCode, AgentCode, AgentGroup, AccName, STARTPAYDATE, PayTypeFlag,"
                                  +"PrtStateFlag, PrintTimes,FinState,duefeetype,markettype,salechnl "
                                  +" from LJAPay where IncomeNo='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"')";

                      String sql4 = "insert into LJAPayPerson select POLNO, PAYCOUNT, GRPCONTNO, GRPPOLNO, CONTNO, MANAGECOM, AGENTCOM,"
                                  +"AGENTTYPE, RISKCODE, AGENTCODE, AGENTGROUP, PAYTYPEFLAG, APPNTNO, '"+tPayNo+"', "
                                  +"PAYAIMCLASS, DUTYCODE, PAYPLANCODE, (-1)*SUMDUEPAYMONEY, (-1)*SUMACTUPAYMONEY, PAYINTV"
                                  +", PAYDATE, PAYTYPE, ENTERACCDATE, '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE,"
                                  +"ININSUACCSTATE, APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO, OPERATOR,"
                                  +"'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', FINSTATE,(-1)*MoneyNoTax,(-1)*MoneyTax,BusiType,TaxRate from"
                                  +" LJAPAYPERSON where CONTNO='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";

                      mMap.put(sql1, "INSERT");
                      mMap.put(sql2, "INSERT");
                      mMap.put(sql3, "INSERT");
                      mMap.put(sql4, "INSERT");
                      }
                    }
                    String CH3SQL="select * from ljagetclaim where contno='"+ContNo+"' and agentcode='"+AgentOld+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                 LJAGetClaimDB tLJAGetClaimDB=new LJAGetClaimDB();
                 LJAGetClaimSet tLJAGetClaimSet=tLJAGetClaimDB.executeQuery(CH3SQL);
                 if(tLJAGetClaimSet.size()>0)
                 {
                     String tLimit = PubFun.getNoLimit(ManageCom);
                     for(int i=1;i<=tLJAGetClaimSet.size();i++)
                     {
                          String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                          LJAGetClaimSchema tLJAGetClaimSchema  = new LJAGetClaimSchema();
                          tLJAGetClaimSchema = tLJAGetClaimSet.get(i);
                          String sql1 = "insert into LJAGetClaim (select '"+tACTUGETNONo+"', FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
                                               +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
                                               +"RISKCODE, RISKVERSION, SALECHNL, AGENTCODE, AGENTGROUP, GETDATE,"
                                               +"ENTERACCDATE, CONFDATE,(-1)*PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
                                               +"OPCONFIRMCODE, OPCONFIRMDATE, OPCONFIRMTIME, SERIALNO, OPERATOR, '"+currentDate+"',"
                                               +"'"+currentTime+"', '"+currentDate+"', '"+currentTime+"' from"
                                               +" LJAGetClaim where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetClaimSchema.getActuGetNo()+"')";
                           tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                           String sql2 = "insert into LJAGetClaim (select '"+tACTUGETNONo+"', FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
                                 +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
                                 +"RISKCODE, RISKVERSION, SALECHNL, '"+AgentNew+"', '"+AgentGroup+"', GETDATE,"
                                 +"ENTERACCDATE, CONFDATE,PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
                                 +"OPCONFIRMCODE, OPCONFIRMDATE, OPCONFIRMTIME, SERIALNO, OPERATOR, '"+currentDate+"',"
                                 +"'"+currentTime+"', '"+currentDate+"', '"+currentTime+"' from"
                                 +" LJAGetClaim where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetClaimSchema.getActuGetNo()+"')";
                            mMap.put(sql1, "INSERT");
                            mMap.put(sql2, "INSERT");
                           }

                 }


                 String CH4SQL="select * from ljagetendorse where contno='"+ContNo+"' and agentcode='"+AgentOld+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                 LJAGetEndorseDB tLJAGetEndorseDB=new LJAGetEndorseDB();
                 LJAGetEndorseSet tLJAGetEndorseSet=tLJAGetEndorseDB.executeQuery(CH4SQL);
                 if(tLJAGetEndorseSet.size()>0)
                 {
                    String tLimit = PubFun.getNoLimit(ManageCom);
                    for(int i=1;i<=tLJAGetEndorseSet.size();i++)
                    {
                        String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                        LJAGetEndorseSchema tLJAGetEndorseSchema   = new LJAGetEndorseSchema ();
                        tLJAGetEndorseSchema  = tLJAGetEndorseSet.get(i);
                        String sql1 = "insert into LJAGetEndorse (select '"+tACTUGETNONo+"', ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
                                              +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
                                              +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, ENTERACCDATE,GETCONFIRMDATE,"
                                              +"(-1)*GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
                                              +"AGENTCODE, AGENTGROUP, GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
                                              +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG,"
                                              +"'"+currentTime+"', FINSTATE,(-1)*MoneyNoTax,(-1)*MoneyTax,BusiType,TaxRate "
                                              +"from LJAGetEndorse where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetEndorseSchema.getActuGetNo()+"')";
                        tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                        String sql2 = "insert into LJAGetEndorse (select '"+tACTUGETNONo+"', ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
                                              +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
                                              +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, ENTERACCDATE,GETCONFIRMDATE,"
                                              +"GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
                                              +"'"+AgentNew+"', '"+AgentGroup+"', GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
                                              +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG,"
                                              +"'"+currentTime+"', FINSTATE,MoneyNoTax,MoneyTax,BusiType,TaxRate "
                                              +"from LJAGetEndorse where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetEndorseSchema.getActuGetNo()+"')";
                         mMap.put(sql1, "INSERT");
                         mMap.put(sql2, "INSERT");
                       }
                 }

                }
                //不需要反冲
                else{
                     String LJAPAYPERSONSQL   = " update LJAPAYPERSON set  agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' , AGENTCOM = '"+AgentComNew+"',modifydate = '"+currentDate+"', modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where contno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                     String ljapaySQL   =" update ljapay  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"', agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"', modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where incomeno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                     String ljagetdorseSQL=" update LJAGETENDORSE set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                     String ljtempfeeSQL=" update LJAGETCLAIM set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                     String lacommisionSQL   = "update lacommision  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',branchcode='"+AgentGroup+"',branchattr='"+newBranchAttr+"',branchseries=(select branchseries from labranchgroup where agentgroup='"+AgentGroup+"'),modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and tmakedate >='"+tLAAscriptionSchema.getAscriptionDate()+"' ";

                     mMap.put(LJAPAYPERSONSQL, "UPDATE");
                     mMap.put(ljapaySQL	, "UPDATE");
                     mMap.put(ljagetdorseSQL, "UPDATE");
                     mMap.put(ljtempfeeSQL	, "UPDATE");
                     mMap.put(lacommisionSQL	, "UPDATE");
                }
                //修改业务表
               String LCPolSQL = "update LCPol  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup = '"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"'";
               String LCContSQL  = "update LCCont set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LCContGetPolSQL   = "update LCContGetPol set agentcode = '"+AgentNew+"',agentname='"+Name+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"'where contno='"+ContNo+"' ";
               
//               String LJTEMPFEESQL   = "update LJTEMPFEE   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where otherno='"+ContNo+"' and othernotype='2'";
               String LCUWSubSQL = "update LCUWSub set agentcode='"+AgentNew+"',agentgroup = '"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LCUWMasterSQL = "update LCUWMaster set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno = '"+ContNo+"' ";
               String LCCUWSubSQL = "update LCCUWSub set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"'";
               String LCCUWMasterSQL = "update LCCUWMaster set agentcode = '"+AgentNew+"',agentgroup = '"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where contno='"+ContNo+"'";
               String LCContReceiveSQL   = "update LCContReceive set agentcode = '"+AgentNew+"',agentname='"+Name+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"' where contno='"+ContNo+"' ";

               String LJSGETCLAIMSQL   = "update LJSGETCLAIM set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LJSGETENDORSESQL   = "update LJSGETENDORSE set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
//               String LJSPAYBSQL   = "update LJSPAYB set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+ContNo+"' ";
               String LJAGETOTHERSQL   = "update LJAGETOTHER   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where othernotype='6' and otherno in (select polno from lcpol where contno='"+ContNo+"')";

               String LPUWSUBMAINSQL   = "update LPUWSUBMAIN set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LPUWSUBSQL   = "update LPUWSUB  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LPUWMASTERMAINSQL   = "update LPUWMASTERMAIN  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LPUWMASTERSQL   = "update LPUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";

               String LPPOLSQL   = "update LPPOL  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LPCUWMASTERSQL   = "update LPCUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LPCONTSQL   = "update LPCONT set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LLCLAIMUWDETAILSQL   = "update LLCLAIMUWDETAIL  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";

               String LLCLAIMUNDERWRITESQL   = "update LLCLAIMUNDERWRITE   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LLCLAIMPOLICYSQL   = "update LLCLAIMPOLICY   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LLCLAIMDETAILSQL   = "update LLCLAIMDETAIL   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LLCASEPOLICYLSQL   = "update LLCASEPOLICY   set agentcode ='"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LJSPAYPERSONBSQL   = "update LJSPAYPERSON   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
               String LJSPAYSQL   = "update LJSPAY  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+ContNo+"' ";
               String LJSPAYSQL1   = "update LJSPAY  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno in(select  edorno  from lpedoritem  where contno= '"+ContNo+"') ";
               
               String LBCONTSQL = "update lbcont set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date,modifytime=current time  where  contno='"+ContNo+"'";
               String LBPOLSQL= "update lbpol set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  contno='"+ContNo+"'";
                
               mMap.put(LBCONTSQL, "UPDATE");
               mMap.put(LBPOLSQL, "UPDATE");
               
               mMap.put(LCPolSQL, "UPDATE");
               mMap.put(LCContSQL, "UPDATE");
               mMap.put(LCContGetPolSQL, "UPDATE");
               mMap.put(LJSPAYSQL, "UPDATE");
               mMap.put(LJSPAYSQL1, "UPDATE");
//               mMap.put(LJTEMPFEESQL, "UPDATE");
               mMap.put(LCUWSubSQL, "UPDATE");
               mMap.put(LCUWMasterSQL, "UPDATE");
               mMap.put(LCCUWSubSQL, "UPDATE");
               mMap.put(LCCUWMasterSQL	, "UPDATE");
               mMap.put(LCContReceiveSQL, "UPDATE");

               mMap.put(LJSGETCLAIMSQL, "UPDATE");
               mMap.put(LJSGETENDORSESQL, "UPDATE");
       //        mMap.put(LJSPAYBSQL, "UPDATE");
               mMap.put(LJAGETOTHERSQL, "UPDATE");

               mMap.put(LPUWSUBMAINSQL, "UPDATE");
               mMap.put(LPUWSUBSQL, "UPDATE");
               mMap.put(LPUWMASTERMAINSQL, "UPDATE");
               mMap.put(LPUWMASTERSQL	, "UPDATE");

               mMap.put(LPPOLSQL, "UPDATE");
               mMap.put(LPCUWMASTERSQL, "UPDATE");
               mMap.put(LPCONTSQL, "UPDATE");
               mMap.put(LLCLAIMUWDETAILSQL, "UPDATE");

               mMap.put(LLCLAIMUNDERWRITESQL, "UPDATE");
               mMap.put(LLCLAIMPOLICYSQL, "UPDATE");
               mMap.put(LLCLAIMDETAILSQL, "UPDATE");
               mMap.put(LLCASEPOLICYLSQL, "UPDATE");
               mMap.put(LJSPAYPERSONBSQL, "UPDATE");
            }else
            	//互动中介团单
            {

                //需要反冲
                if(!oldBranchSeries.substring(0,12).equals(newBranchSeries.substring(0,12)))
                {
                    LJAPayDB tLJAPayDB=new LJAPayDB();
                    LJAPayPersonDB tLJAPayPersonDB=new LJAPayPersonDB();
                    LJTempFeeDB tLJTempFeeDB =new LJTempFeeDB();

              	  String tManageComSQL="select managecom from laagent where agentcode='"+AgentOld+"'";
              	  String ManageCom="";
              	  SSRS mSSRS0 = new SSRS();
                    ExeSQL mExeSQL0 = new ExeSQL();
                    mSSRS0 = mExeSQL0.execSQL(tManageComSQL);
                    ManageCom=mSSRS0.GetText(1, 1);

                    String CH1SQL=" select * from ljapay where payno in (select payno from ljapayperson where grpcontno='"+GrpContNo +"'"
                                 +" and agentcode='"+AgentOld+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"')";
              	  LJAPaySet tLJAPaySet=tLJAPayDB.executeQuery(CH1SQL);
              	  if(tLJAPaySet.size()>0)
          	  {
                       System.out.println("~~~~~~~"+tLJAPaySet.size());
          	     String tLimit = PubFun.getNoLimit(ManageCom);

                       for(int i=1;i<=tLJAPaySet.size();i++)
                       {
                           LJAPaySchema tLJAPaySchema  = new LJAPaySchema();
                           tLJAPaySchema = tLJAPaySet.get(i);
                           String tPayNo=PubFun1.CreateMaxNo("PAYNO", tLimit);
                           String sql1 = "insert into LJAPay (select '"+tPayNo+"', IncomeNo, IncomeType, AppntNo, SumActuPayMoney,"
                                       +"PAYDATE, ENTERACCDATE, '"+currentDate+"', ApproveCode, APPROVEDATE,'"+tPayNo+"', Operator,"
                                       +" '"+currentDate+"','"+currentTime+"', "
                                       +"GetNoticeNo, '"+currentDate+"', '"+currentTime+"', ManageCom, '"+mLAAscriptionSchema.getAgentComNew()+"', AgentType, BankCode,"
                                       +"BankAccNo, RiskCode, '"+AgentNew+"', '"+newAgentgroup+"', AccName, STARTPAYDATE, PayTypeFlag,"
                                       +"PrtStateFlag, PrintTimes,FinState,duefeetype,markettype,salechnl "
                                       +" from LJAPay where IncomeNo='"+GrpContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"')";
          		 String sql2 = "insert into LJAPayPerson select POLNO, PAYCOUNT, GRPCONTNO, GRPPOLNO, CONTNO, MANAGECOM, '"+mLAAscriptionSchema.getAgentComNew()+"',"
                                       +"AGENTTYPE, RISKCODE, '"+AgentNew+"', '"+newAgentgroup+"', PAYTYPEFLAG, APPNTNO, '"+tPayNo+"', "
                                       +"PAYAIMCLASS, DUTYCODE, PAYPLANCODE, SUMDUEPAYMONEY, SUMACTUPAYMONEY, PAYINTV"
                                       +", PAYDATE, PAYTYPE, ENTERACCDATE, '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE,"
                                       +"ININSUACCSTATE, APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO, OPERATOR,"
                                       +"'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', FINSTATE,MoneyNoTax,MoneyTax,BusiType,TaxRate from"
                                       +" LJAPAYPERSON where  GRPCONTNO='"+GrpContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";
          		 String sqlGrp1 =" insert into ljapaygrp select grppolno,paycount,grpcontno,managecom,"+mLAAscriptionSchema.getAgentComNew()+"," 
          		 					   +"agenttype,riskcode,'"+AgentNew+"', '"+newAgentgroup+"',paytypeflag,appntno,'"+tPayNo+"', "
          		 					   +"endorsementno,sumduepaymoney,sumactupaymoney,payintv,paydate,paytype,enteraccdate, '"+currentDate+"'," 
          		 					   +"lastpaytodate,curpaytodate,approvecode,approvedate,approvetime,serialno,getnoticeno,operator," 
          		 					   +"'"+currentDate+"', '"+currentTime+"','"+currentDate+"', '"+currentTime+"',finstate,MoneyNoTax,MoneyTax,BusiType,TaxRate " 
          		 					   +" from ljapaygrp where  GRPCONTNO='"+GrpContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";
          		 		tPayNo=PubFun1.CreateMaxNo("PAYNO", tLimit);
                          String sql3 = "insert into LJAPay (select '"+tPayNo+"', IncomeNo, IncomeType, AppntNo, (-1)*SumActuPayMoney,"
                                      +"PAYDATE, ENTERACCDATE, '"+currentDate+"', ApproveCode, APPROVEDATE, '"+tPayNo+"', Operator,"
                                      +" '"+currentDate+"','"+currentTime+"', "
                                      +"GetNoticeNo, '"+currentDate+"', '"+currentTime+"', ManageCom, AgentCom, AgentType, BankCode,"
                                      +"BankAccNo, RiskCode, AgentCode, AgentGroup, AccName, STARTPAYDATE, PayTypeFlag,"
                                      +"PrtStateFlag, PrintTimes,FinState,duefeetype,markettype,salechnl "
                                      +" from LJAPay where IncomeNo='"+GrpContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"')";

                          String sql4 = "insert into LJAPayPerson select POLNO, PAYCOUNT, GRPCONTNO, GRPPOLNO, CONTNO, MANAGECOM, AGENTCOM,"
                                      +"AGENTTYPE, RISKCODE, AGENTCODE, AGENTGROUP, PAYTYPEFLAG, APPNTNO, '"+tPayNo+"', "
                                      +"PAYAIMCLASS, DUTYCODE, PAYPLANCODE, (-1)*SUMDUEPAYMONEY, (-1)*SUMACTUPAYMONEY, PAYINTV"
                                      +", PAYDATE, PAYTYPE, ENTERACCDATE, '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE,"
                                      +"ININSUACCSTATE, APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO, OPERATOR,"
                                      +"'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', FINSTATE,(-1)*MoneyNoTax,(-1)*MoneyTax,BusiType,TaxRate from"
                                      +" LJAPAYPERSON where GRPCONTNO='"+GrpContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";
                          String sqlGrp2 =" inser tinto ljapaygrp select grppolno,paycount,grpcontno,managecom,"+mLAAscriptionSchema.getAgentComNew()+"," 
		 					   +"agenttype,riskcode,'"+AgentNew+"', '"+newAgentgroup+"',paytypeflag,appntno,'"+tPayNo+"', "
		 					   +"endorsementno,(-1)*sumduepaymoney,(-1)*sumactupaymoney,payintv,paydate,paytype,enteraccdate, '"+currentDate+"'," 
		 					   +"lastpaytodate,curpaytodate,approvecode,approvedate,approvetime,serialno,getnoticeno,operator," 
		 					   +"'"+currentDate+"', '"+currentTime+"','"+currentDate+"', '"+currentTime+"',finstate,(-1)*MoneyNoTax,(-1)*MoneyTax,BusiType,TaxRate " 
		 					   +" from ljapaygrp where  GRPCONTNO='"+GrpContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";
                          mMap.put(sql1, "INSERT");
                          mMap.put(sql2, "INSERT");
                          mMap.put(sql3, "INSERT");
                          mMap.put(sql4, "INSERT");
                          mMap.put(sqlGrp1, "INSERT");
                          mMap.put(sqlGrp2, "INSERT");
                          }
                        }
                        String CH3SQL="select * from ljagetclaim where grpcontno='"+GrpContNo+"' and agentcode='"+AgentOld+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                     LJAGetClaimDB tLJAGetClaimDB=new LJAGetClaimDB();
                     LJAGetClaimSet tLJAGetClaimSet=tLJAGetClaimDB.executeQuery(CH3SQL);
                     if(tLJAGetClaimSet.size()>0)
                     {
                         String tLimit = PubFun.getNoLimit(ManageCom);
                         for(int i=1;i<=tLJAGetClaimSet.size();i++)
                         {
                              String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                              LJAGetClaimSchema tLJAGetClaimSchema  = new LJAGetClaimSchema();
                              tLJAGetClaimSchema = tLJAGetClaimSet.get(i);
                              String sql1 = "insert into LJAGetClaim (select '"+tACTUGETNONo+"', FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
                                                   +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
                                                   +"RISKCODE, RISKVERSION, SALECHNL, AGENTCODE, AGENTGROUP, GETDATE,"
                                                   +"ENTERACCDATE, CONFDATE,(-1)*PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
                                                   +"OPCONFIRMCODE, OPCONFIRMDATE, OPCONFIRMTIME, SERIALNO, OPERATOR, '"+currentDate+"',"
                                                   +"'"+currentTime+"', '"+currentDate+"', '"+currentTime+"' from"
                                                   +" LJAGetClaim where agentcode='"+AgentOld+"' and GRPCONTNO='"+GrpContNo+"' and ActuGetNo = '"+tLJAGetClaimSchema.getActuGetNo()+"')";
                               tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                               String sql2 = "insert into LJAGetClaim (select '"+tACTUGETNONo+"', FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
                                     +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
                                     +"RISKCODE, RISKVERSION, SALECHNL, '"+AgentNew+"', '"+AgentGroup+"', GETDATE,"
                                     +"ENTERACCDATE, CONFDATE,PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
                                     +"OPCONFIRMCODE, OPCONFIRMDATE, OPCONFIRMTIME, SERIALNO, OPERATOR, '"+currentDate+"',"
                                     +"'"+currentTime+"', '"+currentDate+"', '"+currentTime+"' from"
                                     +" LJAGetClaim where agentcode='"+AgentOld+"' and GRPCONTNO='"+GrpContNo+"' and ActuGetNo = '"+tLJAGetClaimSchema.getActuGetNo()+"')";
                                mMap.put(sql1, "INSERT");
                                mMap.put(sql2, "INSERT");
                               }

                     }


                     String CH4SQL="select * from ljagetendorse where grpcontno='"+GrpContNo+"' and agentcode='"+AgentOld+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                     LJAGetEndorseDB tLJAGetEndorseDB=new LJAGetEndorseDB();
                     LJAGetEndorseSet tLJAGetEndorseSet=tLJAGetEndorseDB.executeQuery(CH4SQL);
                     if(tLJAGetEndorseSet.size()>0)
                     {
                        String tLimit = PubFun.getNoLimit(ManageCom);
                        for(int i=1;i<=tLJAGetEndorseSet.size();i++)
                        {
                            String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                            LJAGetEndorseSchema tLJAGetEndorseSchema   = new LJAGetEndorseSchema ();
                            tLJAGetEndorseSchema  = tLJAGetEndorseSet.get(i);
                            String sql1 = "insert into LJAGetEndorse (select '"+tACTUGETNONo+"', ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
                                                  +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
                                                  +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, ENTERACCDATE,GETCONFIRMDATE,"
                                                  +"(-1)*GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
                                                  +"AGENTCODE, AGENTGROUP, GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
                                                  +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG,"
                                                  +"'"+currentTime+"', FINSTATE,(-1)*MoneyNoTax,(-1)*MoneyTax,BusiType,TaxRate "
                                                  +"from LJAGetEndorse where agentcode='"+AgentOld+"' and GRPCONTNO='"+GrpContNo+"' and ActuGetNo = '"+tLJAGetEndorseSchema.getActuGetNo()+"')";
                            tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                            String sql2 = "insert into LJAGetEndorse (select '"+tACTUGETNONo+"', ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
                                                  +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
                                                  +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, ENTERACCDATE,GETCONFIRMDATE,"
                                                  +"GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
                                                  +"'"+AgentNew+"', '"+AgentGroup+"', GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
                                                  +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG,"
                                                  +"'"+currentTime+"', FINSTATE,MoneyNoTax,MoneyTax,BusiType,TaxRate "
                                                  +"from LJAGetEndorse where agentcode='"+AgentOld+"' and GRPCONTNO='"+GrpContNo+"' and ActuGetNo = '"+tLJAGetEndorseSchema.getActuGetNo()+"')";
                             mMap.put(sql1, "INSERT");
                             mMap.put(sql2, "INSERT");
                           }
                     }

                    }
                    //不需要反冲
                    else{
                         String LJAPAYPERSONSQL   = " update LJAPAYPERSON set  agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' , AGENTCOM = '"+AgentComNew+"',modifydate = '"+currentDate+"', modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where grpcontno='"+GrpContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                         String ljapaySQL   =" update ljapay  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"', agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"', modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where incomeno='"+GrpContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                         String ljagetdorseSQL=" update LJAGETENDORSE set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                         String ljtempfeeSQL=" update LJAGETCLAIM set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                         String lacommisionSQL   = "update lacommision  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',branchcode='"+AgentGroup+"',branchattr='"+newBranchAttr+"',branchseries=(select branchseries from labranchgroup where agentgroup='"+AgentGroup+"'),modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' and tmakedate >='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
                         String ljapaygrpsql =" update ljapaygrp set  agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' , AGENTCOM = '"+AgentComNew+"',modifydate = '"+currentDate+"', modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where grpcontno='"+GrpContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
                         mMap.put(LJAPAYPERSONSQL, "UPDATE");
                         mMap.put(ljapaySQL	, "UPDATE");
                         mMap.put(ljagetdorseSQL, "UPDATE");
                         mMap.put(ljtempfeeSQL	, "UPDATE");
                         mMap.put(lacommisionSQL	, "UPDATE");
                         mMap.put(ljapaygrpsql	, "UPDATE");
                    }
                    //修改业务表
                   String LCGRPPolSQL = "update LCGRPPol  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup = '"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"'";
                   String LCPolSQL = "update LCPol  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup = '"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"'";
                   String LCGRPContSQL  = "update LCGRPCont set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LCContSQL  = "update LCCont set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
 //                  String LCContGetPolSQL   = "update LCContGetPol set agentcode = '"+AgentNew+"',agentname='"+Name+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"'where contno='"+ContNo+"' ";
                   
//                   String LJTEMPFEESQL   = "update LJTEMPFEE   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where otherno='"+ContNo+"' and othernotype='2'";
                   String LCUWSubSQL = "update LCUWSub set agentcode='"+AgentNew+"',agentgroup = '"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LCUWMasterSQL = "update LCUWMaster set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno = '"+GrpContNo+"' ";
                   String LCCUWSubSQL = "update LCCUWSub set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"'";
                   String LCCUWMasterSQL = "update LCCUWMaster set agentcode = '"+AgentNew+"',agentgroup = '"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where grpcontno='"+GrpContNo+"'";
 //                  String LCContReceiveSQL   = "update LCContReceive set agentcode = '"+AgentNew+"',agentname='"+Name+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"' where contno='"+ContNo+"' ";
                   
                   String LJSGETCLAIMSQL   = "update LJSGETCLAIM set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LJSGETENDORSESQL   = "update LJSGETENDORSE set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
//                   String LJSPAYBSQL   = "update LJSPAYB set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+ContNo+"' ";
                   String LJAGETOTHERSQL   = "update LJAGETOTHER   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where othernotype='6' and otherno in (select polno from lcpol where grpcontno='"+GrpContNo+"')";

                   String LPUWSUBMAINSQL   = "update LPUWSUBMAIN set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPUWSUBSQL   = "update LPUWSUB  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPUWMASTERMAINSQL   = "update LPUWMASTERMAIN  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPUWMASTERSQL   = "update LPUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";

                   String LPGRPPOLSQL   = "update LPGRPPOL  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPPOLSQL   = "update LPPOL  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPCUWMASTERSQL   = "update LPCUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPGRPCONTSQL   = "update LPGRPCONT set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LPCONTSQL   = "update LPCONT set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LLCLAIMUWDETAILSQL   = "update LLCLAIMUWDETAIL  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";

                   String LLCLAIMUNDERWRITESQL   = "update LLCLAIMUNDERWRITE   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LLCLAIMPOLICYSQL   = "update LLCLAIMPOLICY   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LLCLAIMDETAILSQL   = "update LLCLAIMDETAIL   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LLCASEPOLICYLSQL   = "update LLCASEPOLICY   set agentcode ='"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   String LJSPAYPERSONBSQL   = "update LJSPAYPERSON   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
                   
                   String LJSPAYSQL1   = "update LJSPAY  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno in(select  edorno  from lpgrpedoritem  where grpcontno= '"+GrpContNo+"') ";
                   String LJSPAYSQL   = "update LJSPAY  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+GrpContNo+"' ";
                   String LBPOLSQL = "update lbpol set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
                   String LBGRPPOLSQL = "update lbgrppol set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
	               String LBCONTSQL = "update lbcont set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
	               String LBGRPCONTSQL = "update lbgrpcont set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
                   String LJSPAYGRPSQL= "update LJSPayGrp set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
                 
                   mMap.put(LBPOLSQL, "UPDATE");
                   mMap.put(LBGRPPOLSQL, "UPDATE");
                   mMap.put(LBCONTSQL, "UPDATE");
                   mMap.put(LBGRPCONTSQL, "UPDATE");
                   mMap.put(LJSPAYGRPSQL, "UPDATE");

                   
                   mMap.put(LCGRPPolSQL, "UPDATE");
                   mMap.put(LCGRPContSQL, "UPDATE");
                   mMap.put(LPGRPPOLSQL, "UPDATE");
                   mMap.put(LPGRPCONTSQL, "UPDATE");

                   mMap.put(LCPolSQL, "UPDATE");
                   mMap.put(LCContSQL, "UPDATE");
//                   mMap.put(LCContGetPolSQL, "UPDATE");
                   mMap.put(LJSPAYSQL, "UPDATE");
                   mMap.put(LJSPAYSQL1, "UPDATE");
//                   mMap.put(LJTEMPFEESQL, "UPDATE");
                   mMap.put(LCUWSubSQL, "UPDATE");
                   mMap.put(LCUWMasterSQL, "UPDATE");
                   mMap.put(LCCUWSubSQL, "UPDATE");
                   mMap.put(LCCUWMasterSQL	, "UPDATE");
//                   mMap.put(LCContReceiveSQL, "UPDATE");
                   
                   mMap.put(LJSGETCLAIMSQL, "UPDATE");
                   mMap.put(LJSGETENDORSESQL, "UPDATE");
           //        mMap.put(LJSPAYBSQL, "UPDATE");
                   mMap.put(LJAGETOTHERSQL, "UPDATE");

                   mMap.put(LPUWSUBMAINSQL, "UPDATE");
                   mMap.put(LPUWSUBSQL, "UPDATE");
                   mMap.put(LPUWMASTERMAINSQL, "UPDATE");
                   mMap.put(LPUWMASTERSQL	, "UPDATE");

                   mMap.put(LPPOLSQL, "UPDATE");
                   mMap.put(LPCUWMASTERSQL, "UPDATE");
                   mMap.put(LPCONTSQL, "UPDATE");
                   mMap.put(LLCLAIMUWDETAILSQL, "UPDATE");

                   mMap.put(LLCLAIMUNDERWRITESQL, "UPDATE");
                   mMap.put(LLCLAIMPOLICYSQL, "UPDATE");
                   mMap.put(LLCLAIMDETAILSQL, "UPDATE");
                   mMap.put(LLCASEPOLICYLSQL, "UPDATE");
                   mMap.put(LJSPAYPERSONBSQL, "UPDATE");
                	
            }
               String endSQL="update  LAAscription set AscripState='3' ,modifydate=current date,modifytime =current time"
            		      +" where AscripNo='"+mAscripNo+"'";
            		      mMap.put(endSQL, "UPDATE");
               
           //  准备往后台的数据
               if (!prepareOutputData())
               {
                   return false;
               }
               PubSubmit tPubSubmit = new PubSubmit();
               if (!tPubSubmit.submitData(mInputData, ""))
               {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "LABKAssess1BL";
                   tError.functionName = "submitData";
                   tError.errorMessage = "数据提交失败!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
          }
        	
        	
        
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        this.mLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData
                .getObjectByObjectName("LAAscriptionSchema", 0));
        System.out.println("Cont=" + mLAAscriptionSchema.getContNo());
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        AgentNew1 = (String) cInputData.get(2);
        tContNo1 = (String) cInputData.get(3);
        tGrpContNo1 = (String) cInputData.get(4);
        System.out.println("AgentNew1:" + AgentNew1);
        System.out.println("tContNo1:" + tContNo1);
        System.out.println("????????????????");
        return true;
    }

    //验证原代理人与保单号是否符合
    private boolean checkData()
    {
        if (mLAAscriptionSchema.getContNo() != null
                && !(mLAAscriptionSchema.getContNo().equals("")))
        {  
            String strsql1 = "select prtno from lccont  where agentcode='"
                    + mLAAscriptionSchema.getAgentOld() + "' and contno='"
                    + mLAAscriptionSchema.getContNo() + "'";
            ExeSQL tstrExeSQL = new ExeSQL();
            String tPrtno = tstrExeSQL.getOneValue(strsql1);
            if (tPrtno == null || tPrtno.equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "原代理人"
                        + mLAAscriptionSchema.getAgentOld() + "没有保单号为:"
                        + mLAAscriptionSchema.getContNo() + "的保单";
                this.mErrors.addOneError(tError);
                return false;
            }
            String   tSQL="select '1' from lccont b "
                + "where b.uwflag = 'a'  and b.PrtNo='"
                + tPrtno+"'";
            ExeSQL tExeSQL = new ExeSQL();
            String flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单已撤单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where b.uwflag = '1'  and b.PrtNo='"
                + tPrtno+"'";
            tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单已拒保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where (b.uwflag = '8' or b.uwflag='2')  and b.PrtNo='"
                + tPrtno+"'";
            //tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单为延期承保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tPrtno+"' and b.signdate is null and b.prtno not in (" +
                       		"select prtno from lcrnewstatelog where state!='6' and contno='" +
                       		mLAAscriptionSchema.getContNo()+"' )";
            //tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单没有签单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tPrtno+"' and b.signdate is not null "
                +" and (b.customgetpoldate is  null or b.getpoldate is null) with ur";
            //tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单未回执回销，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }   
            if (mLAAscriptionSchema.getGrpContNo() != null
                && !(mLAAscriptionSchema.getGrpContNo().equals("")))
          {
            String strsql2 = "select prtno from lcgrpcont  where agentcode='"
                    + mLAAscriptionSchema.getAgentOld() + "' and grpcontno='"
                    + mLAAscriptionSchema.getGrpContNo() + "'";
            ExeSQL tsExeSQL = new ExeSQL();
            String tsPrtno = tsExeSQL.getOneValue(strsql2);
            if (tsPrtno == null || tsPrtno.equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "原代理人"
                        + mLAAscriptionSchema.getAgentOld() + "没有保单号为:"
                        + mLAAscriptionSchema.getGrpContNo() + "的保单";
                this.mErrors.addOneError(tError);
                return false;
            }
            String   tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag = 'a'  and b.PrtNo='"
                + tsPrtno+"'";
            ExeSQL tExeSQL1 = new ExeSQL();
            String flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单已撤单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag = '1'  and b.PrtNo='"
                + tsPrtno+"'";
            tExeSQL1 = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单已拒保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where (b.uwflag = '8' or b.uwflag='2')  and b.PrtNo='"
                + tsPrtno+"'";
            //tExeSQL = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单为延期承保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tsPrtno+"' and b.signdate is null and b.prtno not in (" +
                       		"select prtno from lcrnewstatelog where state!='6' and grpcontno='" +
                       		mLAAscriptionSchema.getGrpContNo()+"' )";
            //tExeSQL = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单没有签单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tsPrtno+"' and b.signdate is not null "
                +" and (b.customgetpoldate is  null or b.getpoldate is null) with ur";
            //tExeSQL = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单未回执回销，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
          }
        //校验是否跨机构分配
        String sql = "select managecom from laagent where agentcode='"
                + this.mLAAscriptionSchema.getAgentNew()
                + "' and branchtype='5'";
        ExeSQL tExeSQL = new ExeSQL();
        String managecom = tExeSQL.getOneValue(sql);
        if (!this.mLAAscriptionSchema.getManageCom().equals(managecom))
        {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "现代理人与原代理人机构不一致！请在同一机构下做保单分配！";
            this.mErrors.addOneError(tError);
            return false;
        }
            

        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            System.out.println("|||||||||||||||"+ mLAAscriptionSchema.getBranchAttr());
           this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
    //针对直销下的保单
    private boolean dealCont(LAAscriptionSet tLAAscriptionSet)
    {
        for (int i = 1; i <= tLAAscriptionSet.size(); i++) {
            
            LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
            tLAAscriptionSchema = tLAAscriptionSet.get(i);

            String tAscripNo = tLAAscriptionSchema.getAscripNo();
            String tGrpContNo = tLAAscriptionSchema.getGrpContNo();
            String tContNo = tLAAscriptionSchema.getContNo();
            String tAgentNew = tLAAscriptionSchema.getAgentNew();
            String sql1 = "select a.agentgroup,a.branchcode,b.branchattr,b.branchseries,a.managecom,a.Name "
            	 +" from laagent a , labranchgroup b "
            	 +" where a.agentgroup=b.agentgroup and a.agentcode='" +tAgentNew + "' and a.agentstate<='02' ";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL2 = new ExeSQL();
            tSSRS = tExeSQL2.execSQL(sql1);
            if (tExeSQL2.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tExeSQL2.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAscriptionEnsureBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询新代理人信息出错或代理人为离职状态！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tAgentGroup = tSSRS.GetText(1, 1);
            String tBranchCode = tSSRS.GetText(1, 2);
            String tBranchAttr = tSSRS.GetText(1, 3);
            String tBranchSeries = tSSRS.GetText(1, 4);
            String tManageCom = tSSRS.GetText(1, 5);
            //String tName = tSSRS.GetText(1, 6);


//            tLAAscriptionSchema = tLAAscriptionDB.getSchema();
            // 原本程序是按照当前日期为保单归属日期，先按照保单分配中的归属日期为准
            // tLAAscriptionSchema.setAscriptionDate(currentDate);   
      //      String tCheckDate=getRightData(tLAAscriptionSchema.getAgentOld(),tLAAscriptionSchema.getManageCom());
     //       tLAAscriptionSchema.setAscriptionDate(tCheckDate);
              //团单
            if(tLAAscriptionSchema.getGrpContNo()!= null&&!tLAAscriptionSchema.getGrpContNo().trim().equals("")){
              String tSQL="select count('1') from lcgrpcont where prtno in (select distinct prtno from lcgrppol where grpcontno='"+tGrpContNo+"')";      
              SSRS tSSRS1 = new SSRS();
              ExeSQL tExeSQL1 = new ExeSQL();
              tSSRS1 = tExeSQL1.execSQL(tSQL);
              int tCount1 = tSSRS1.getMaxRow();
              if (tCount1>=1) 
              {
              	 String updateSQL="update lcgrpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where prtno = (select distinct prtno from lcgrppol where grpcontno='"+tGrpContNo+"')";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update lccont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                	+" ,modifytime=current time  where prtno = (select distinct prtno from lcgrppol where grpcontno='"+tGrpContNo+"')";
               	 this.mMap.put(updateSQL, "UPDATE");
               	updateSQL="update lcgrppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where prtno = (select distinct prtno from lcgrppol where grpcontno='"+tGrpContNo+"')";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update lcpol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                	+" ,modifytime=current time  where prtno = (select distinct prtno from lcgrppol where grpcontno='"+tGrpContNo+"')";
               	 this.mMap.put(updateSQL, "UPDATE");
              }
              
              String tSQL1="select count('1') from lbgrpcont where prtno in (select distinct prtno from lbgrppol where grpcontno='"+tGrpContNo+"')";      
              SSRS tSSRS2 = new SSRS();
              ExeSQL tExeSQL = new ExeSQL();
              tSSRS2 = tExeSQL.execSQL(tSQL1);
              int tCount = tSSRS2.getMaxRow();
              if (tCount>=1) 
              {
              	 String updateSQL="update lbgrpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where prtno = (select distinct prtno from lbgrppol where grpcontno='"+tGrpContNo+"')";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update lbcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                	+" ,modifytime=current time  where prtno = (select distinct prtno from lbgrppol where grpcontno='"+tGrpContNo+"')";
               	 this.mMap.put(updateSQL, "UPDATE");
               	updateSQL="update lbgrppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where prtno = (select distinct prtno from lbgrppol where grpcontno='"+tGrpContNo+"')";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update lbpol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                	+" ,modifytime=current time  where prtno = (select distinct prtno from lbgrppol where grpcontno='"+tGrpContNo+"')";
               	 this.mMap.put(updateSQL, "UPDATE");
              }
            
              String tSQL3="select count('1') from lpgrpcont where prtno in (select distinct prtno from lpgrpcont where grpcontno='"+tGrpContNo+"')";      
              SSRS tSSRS3 = new SSRS();
              ExeSQL tExeSQL3 = new ExeSQL();
              tSSRS3 = tExeSQL3.execSQL(tSQL3);
              int tCount3 = tSSRS3.getMaxRow();
              if (tCount3>=1) {
              	 String updateSQL="update lpgrpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where prtno = (select distinct prtno from lpgrpcont where grpcontno='"+tGrpContNo+"')";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update lpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                	+" ,modifytime=current time  where prtno = (select distinct prtno from lpgrpcont where grpcontno='"+tGrpContNo+"')";
               	 this.mMap.put(updateSQL, "UPDATE");
               	updateSQL="update lpgrppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where prtno = (select distinct prtno from lpgrpcont where grpcontno='"+tGrpContNo+"')";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update lppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                	+" ,modifytime=current time  where prtno = (select distinct prtno from lpgrpcont where grpcontno='"+tGrpContNo+"')";
               	 this.mMap.put(updateSQL, "UPDATE");
              }
              
              String tSQL4="select count('1') from LJSPayGrp where  grpcontno='"+tGrpContNo+"' ";      
              SSRS tSSRS4 = new SSRS();
              ExeSQL tExeSQL4 = new ExeSQL();
              tSSRS4 = tExeSQL4.execSQL(tSQL4);
              int tCount4 = tSSRS4.getMaxRow();
              if (tCount4>=1) {
              	 String updateSQL="update LJSPayGrp set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 updateSQL="update ljspayperson set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                   +" ,managecom='"+tManageCom +"' ,modifydate=current date"
               	+" ,modifytime=current time  where grpcontno='"+ tGrpContNo+"'";
               	 this.mMap.put(updateSQL, "UPDATE");
                 String LJSPAYSQL1   = "update LJSPAY  set agentcode = '"+tAgentNew+"',agentgroup='"+tAgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno in(select  edorno  from lpgrpedoritem  where grpcontno= '"+tGrpContNo+"') ";
                  this.mMap.put(LJSPAYSQL1, "UPDATE"); 
               	updateSQL="update LJSPay set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                  +" ,managecom='"+tManageCom +"' ,modifydate=current date"
              	+" ,modifytime=current time  where OtherNo='"+ tGrpContNo+"'";
              	 this.mMap.put(updateSQL, "UPDATE");
              	 
              }
              String upSQL="update LJSGetEndorse set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
              +" ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where grpcontno='"+ tGrpContNo+"'";
          	 this.mMap.put(upSQL, "UPDATE"); 
              //按照归属时间来更改
              LACommisionSet tLACommisionSet = new LACommisionSet();
              LACommisionDB tLACommisionDB = new LACommisionDB();
              System.out.println("getAscriptionDate:"+tLAAscriptionSchema.getAscriptionDate());
              LAWageSet tLAWageSet = new LAWageSet();
              LAWageDB tLAWageDB = new LAWageDB();
              String managecom=tLAAscriptionSchema.getManageCom();
              String wageno=tLAAscriptionSchema.getAscriptionDate().substring(0,4)+tLAAscriptionSchema.getAscriptionDate().substring(5,7);
              String mSQL="select * from lawage where indexcalno='"+wageno+"' and managecom='"+managecom
              +"' and branchtype='"+tLAAscriptionSchema.getBranchType()+"' and branchtype2='"+tLAAscriptionSchema.getBranchType2()+"' ";
              System.out.println(mSQL);
              tLAWageSet = tLAWageDB.executeQuery(mSQL);
              //如果没有算过薪资
              if(tLAWageSet.size()==0)
              {
              String pSQL="select * from lacommision where grpcontno='"+tGrpContNo+"'  and tmakedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
              System.out.println(pSQL);
              tLACommisionSet = tLACommisionDB.executeQuery(pSQL);
              if(tLACommisionSet.size()>=1)
              {
              	String ttSQL="update LACommision set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
              	+"',managecom='"+tManageCom+"',branchattr='"+tBranchAttr+"',branchseries='"+tBranchSeries+"',branchcode='"+tBranchCode+"'"
              		+",modifydate=current date,modifytime=current time "
              		+" where grpcontno='"+tGrpContNo+"'  and tmakedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
              	this.mMap.put(ttSQL, "UPDATE");
              }
             
              
              String tSQL6="select count(1) from ljapaygrp where grpcontno='"+tGrpContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";    
              SSRS tSSRS6 = new SSRS();
              ExeSQL tExeSQL6 = new ExeSQL();
              tSSRS6 = tExeSQL6.execSQL(tSQL6);
              int tCount6 = tSSRS6.getMaxRow();
              if (tCount6>=1) {
              //可以没有应收
              String updateSQL="update LJAPayGrp set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                  +" ,managecom='"+tManageCom +"' ,modifydate=current date"
              	+" ,modifytime=current time  where grpcontno='"+tGrpContNo+"'"
              	+" and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
              this.mMap.put(updateSQL, "UPDATE");
              String updateSQL1="update ljapayperson set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                  +" ,managecom='"+tManageCom +"' ,modifydate=current date"
              	+" ,modifytime=current time  where grpcontno='"+ tGrpContNo+"'"
              	+" and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
              this.mMap.put(updateSQL1, "UPDATE");
              String updateSQL2="update ljapay set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
              +" ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where incomeno='"+ tGrpContNo+"'"
          	+" and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
              this.mMap.put(updateSQL2, "UPDATE");
              }
              }        
            }

            //个单
            if(tLAAscriptionSchema.getContNo()!= null&&!tLAAscriptionSchema.getContNo().trim().equals("")){
            //修改LCCont表中的AgentGroup,AgentCode,manageCom
            	String tSQL1="select count('1') from LCCont where  prtno in (select prtno from lccont where contno='"+tContNo+"' ) ";
                SSRS tSSRS1 = new SSRS();
                ExeSQL tExeSQL1 = new ExeSQL();
                tSSRS1 = tExeSQL1.execSQL(tSQL1);
                int tCount1 = tSSRS1.getMaxRow();
                if (tCount1>=1) {
                	 String updateSQL="update LCCont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                 	 +" ,modifytime=current time  where   prtno in (select prtno from lccont where contno='"+tContNo+"' ) ";
                	 this.mMap.put(updateSQL, "UPDATE");
                	 updateSQL="update LCPol set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                     +" ,managecom='"+tManageCom +"' ,modifydate=current date"
                 	+" ,modifytime=current time  where  prtno in (select prtno from lccont where contno='"+tContNo+"' ) ";
                 	 this.mMap.put(updateSQL, "UPDATE");
                }
                
                String tSQL2="select count('1') from LBCont where  prtno in (select prtno from lbcont where contno='"+tContNo+"' ) ";
                SSRS tSSRS2 = new SSRS();
                ExeSQL tExeSQL21 = new ExeSQL();
                tSSRS2 = tExeSQL21.execSQL(tSQL2);
                int tCount2 = tSSRS2.getMaxRow();
                if (tCount2>=1) {
                	 String updateSQL="update LBCont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                 	 +" ,modifytime=current time  where   prtno in (select prtno from lbcont where contno='"+tContNo+"' ) ";
                	 this.mMap.put(updateSQL, "UPDATE");
                	 updateSQL="update LBPol set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                     +" ,managecom='"+tManageCom +"' ,modifydate=current date"
                 	+" ,modifytime=current time  where  prtno in (select prtno from lbcont where contno='"+tContNo+"' ) ";
                 	 this.mMap.put(updateSQL, "UPDATE");
                }
            
                
                String tSQL3="select count('1') from lpcont where prtno  in  (select prtno from lccont where contno = '"+tContNo+"'" +
                        " union" +
                        " select prtno from lbcont where contno = '"+tContNo+"')";
                SSRS tSSRS3 = new SSRS();
                ExeSQL tExeSQL3 = new ExeSQL();
                tSSRS3 = tExeSQL3.execSQL(tSQL3);
                int tCount3 = tSSRS3.getMaxRow();
                if (tCount3>=1) {
                	 String updateSQL="update lpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
                 	 +" ,modifytime=current time  where  prtno in (select prtno from lccont where contno = '"+tContNo+"'" +
                        " union" +
                        " select prtno from lbcont where contno = '"+tContNo+"')";
                	 this.mMap.put(updateSQL, "UPDATE");
                	 updateSQL="update LPPol set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                     +" ,managecom='"+tManageCom +"' ,modifydate=current date"
                 	+" ,modifytime=current time  where  prtno in (select prtno from lccont where contno='"+tContNo+"' )";
                 	 this.mMap.put(updateSQL, "UPDATE");
                }
                
                String tSQL4="select count('1') from LJSPayPerson  where contno = '"+tContNo+"' ";
                SSRS tSSRS4 = new SSRS();
                ExeSQL tExeSQL4 = new ExeSQL();
                tSSRS4 = tExeSQL4.execSQL(tSQL4);
                int tCount4 = tSSRS4.getMaxRow();
                if (tCount4>=1) {
          	      String updateSQL="update LJSPayPerson set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
           	      +" ,modifytime=current time  where contno = '"+tContNo+"' ";
          	      this.mMap.put(updateSQL, "UPDATE");
          	      updateSQL="update LJSPay set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                    +" ,managecom='"+tManageCom +"' ,modifydate=current date"
           	      +" ,modifytime=current time  where OtherNo = '"+tContNo+"' ";
           	      this.mMap.put(updateSQL, "UPDATE");
           	   String LJSPAYSQL1   = "update LJSPAY  set agentcode = '"+tAgentNew+"',agentgroup='"+tAgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno in(select  edorno  from lpedoritem  where contno= '"+tContNo+"') ";
               this.mMap.put(LJSPAYSQL1, "UPDATE"); 
                 }
                String updSQL="update LJSGetEndorse set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                +" ,managecom='"+tManageCom +"' ,modifydate=current date"
      	      +" ,modifytime=current time  where OtherNo = '"+tContNo+"' ";
      	      this.mMap.put(updSQL, "UPDATE");
                
      	       String nameSQL ="select name from laagent where agentcode ='"+tAgentNew+"'";
      	       ExeSQL nameExe = new ExeSQL();
      	       String nameNew = nameExe.getOneValue(nameSQL);
                String update="update LCContGetPol set agentcode='"+tAgentNew+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
      	      +" ,modifytime=current time,agentname ='"+nameNew+"'  where ContNo = '"+tContNo+"' ";
      	      this.mMap.put(update, "UPDATE");
            
            
//          按照归属时间来更改
            LACommisionSet tLACommisionSet = new LACommisionSet();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            System.out.println("getAscriptionDate:"+tLAAscriptionSchema.getAscriptionDate());
            LAWageSet tLAWageSet = new LAWageSet();
            LAWageDB tLAWageDB = new LAWageDB();
            String managecom=tLAAscriptionSchema.getManageCom();
            String wageno=tLAAscriptionSchema.getAscriptionDate().substring(0,4)+tLAAscriptionSchema.getAscriptionDate().substring(5,7);
            String mSQL="select * from lawage where indexcalno='"+wageno+"' and  managecom='"+managecom
            +"' and branchtype='"+tLAAscriptionSchema.getBranchType()+"' and branchtype2='"+tLAAscriptionSchema.getBranchType2()+"' ";
            System.out.println(mSQL);
            tLAWageSet = tLAWageDB.executeQuery(mSQL);
            //如果没有算过薪资
            if(tLAWageSet.size()==0)
            {
            String pSQL="select * from lacommision where contno='"+tContNo+"'  and tmakedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
            System.out.println(pSQL);
            tLACommisionSet = tLACommisionDB.executeQuery(pSQL);
            if(tLACommisionSet.size()>=1){
          	  String ttSQL="update LACommision set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
            	+"',managecom='"+tManageCom+"',branchattr='"+tBranchAttr+"',branchseries='"+tBranchSeries+"',branchcode='"+tBranchCode+"'"
            		+",modifydate=current date,modifytime=current time "
            		+" where contno='"+tContNo+"'  and tmakedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
            	this.mMap.put(ttSQL, "UPDATE");  
            }
           
            //按照归属时间来更改实收表
            //修改LJSPayPerson表中的AgentGroup,AgentCode,manageCom
//            LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
//            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
//            String makedate=tLAAscriptionSchema.getAscriptionDate().substring(0,7)+"-01";
//            String mSQL1="select * from ljapayperson where contno='"+tContNo+"'  and makedate>='"+makedate+"' ";
//            String mSQL2="select * from ljapayperson where contno='"+tContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
//            System.out.println(mSQL2);
//            tLJAPayPersonSet = tLJAPayPersonDB.executeQuery(mSQL2);
//            //可以没有应收
            
            String tSQL6="select count(1) from ljapayperson where contno='"+tContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
            SSRS tSSRS6 = new SSRS();
            ExeSQL tExeSQL6 = new ExeSQL();
            tSSRS6 = tExeSQL6.execSQL(tSQL6);
            int tCount6 = tSSRS6.getMaxRow();
            if (tCount6>=1) {
//            if (tLJAPayPersonSet.size()>=1) {
                String updateSQL1="update LJAPayPerson set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                +" ,managecom='"+tManageCom +"' ,modifydate=current date"
            	+" ,modifytime=current time  where contno='"+ tContNo+"'"
            	+" and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
                this.mMap.put(updateSQL1, "UPDATE");
                String updateSQL2="update ljapay set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
                +" ,managecom='"+tManageCom +"' ,modifydate=current date"
            	+" ,modifytime=current time  where incomeno='"+ tContNo+"'"
            	+" and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
                this.mMap.put(updateSQL2, "UPDATE");
            }        
            }   
            }
//          modify by zhuxt 20140923  
            //需要修改保单回执回销表lccontgetpol和合同接收表lccontreceive
            String pSQLl="select * from lccontgetpol where contno='"+tContNo+"' and getpolstate = '0' and conttype='1' ";
            SSRS tS5 = new SSRS();
            tS5 = tExeSQL2.execSQL(pSQLl);
            int tCou5 = tS5.getMaxRow();
            if (tCou5>=1) {
            String tNameSQL = "select name from laagent where agentcode = '" + tAgentNew +"'";
            String tAgentname = tExeSQL2.getOneValue(tNameSQL);
            if (tExeSQL2.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "LAGrpAscrptionBL";
                tError.functionName = "dealCont";
                tError.errorMessage = "查询保单原业务员姓名出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            String mSql_lccontgetpol = " update lccontgetpol set agentcode='" +
            tAgentNew +
            "',agentname='"
            + tAgentname + "',modifydate='" + PubFun.getCurrentDate() +
            "' ,modifytime='" + PubFun.getCurrentTime() + "' where contno='" +
            tContNo +
            "'";

      		String mSql_lccontreceive = " update lccontreceive set agentcode='" +
      		tAgentNew +
      		"',agentname='"
      		+ tAgentname + "',modifydate='" + PubFun.getCurrentDate() +
      		"' ,modifytime='" + PubFun.getCurrentTime() + "' where contno='" +
      		tContNo +
      		"'";
      		
      		this.mMap.put(mSql_lccontgetpol, "UPDATE");
      		this.mMap.put(mSql_lccontreceive, "UPDATE");  
            }
            String endSQL="update  LAAscription set AscripState='3' ,AscriptionDate='"+tLAAscriptionSchema.getAscriptionDate()+"',modifydate=current date,modifytime =current time"
            +" where AscripNo='"+tAscripNo+"'";
            this.mMap.put(endSQL, "UPDATE");
            
//          准备往后台的数据
            if (!prepareOutputData())
            {
                return false;
            }
            
            System.out.println("Start LABKAssess1BL Submit...");
            

            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABKAssess1BL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
            }    
           return true;
          }
//    public boolean checkBQ(String contno) {
//		  String bqSql = "select 'Y' from lpedoritem where contno = '"+contno+"' and exists (select 1 from lpedorapp where edoracceptno = lpedoritem.edoracceptno and edorstate != '0') with ur";
//  	  ExeSQL bqExeSQL = new ExeSQL();
//  	  String bqStr = bqExeSQL.getOneValue(bqSql);
//  	  if(bqStr != null && !bqStr.equals("")) {
//            return false;
//  	  }
//  	  System.out.println("保单号为：" + contno + "的保单不是未结案状态！");
//  	  return true;
//	}
//    


}
