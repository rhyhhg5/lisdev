package com.sinosoft.lis.intermedium;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportManageProtocolPoundageUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportManageProtocolPoundageBL mLADiskImportManageProtocolPoundageBL = null;

    public LADiskImportManageProtocolPoundageUI()
    {
    	mLADiskImportManageProtocolPoundageBL = new LADiskImportManageProtocolPoundageBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {

    	if(!mLADiskImportManageProtocolPoundageBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportManageProtocolPoundageBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportManageProtocolPoundageBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportManageProtocolPoundageBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportManageProtocolPoundageBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportManageProtocolPoundageBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportManageProtocolPoundageUI zLADiskImportProtocolPoundageUI = new LADiskImportManageProtocolPoundageUI();
    }
}
