package com.sinosoft.lis.intermedium;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class InterLADiskImportActiveUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private InterLADiskImportActiveBL mInterLADiskImportActiveBL = null;

    public InterLADiskImportActiveUI()
    {
    	mInterLADiskImportActiveBL = new InterLADiskImportActiveBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mInterLADiskImportActiveBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mInterLADiskImportActiveBL.mErrors);
            return false;
        }
        //
        if(mInterLADiskImportActiveBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mInterLADiskImportActiveBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mInterLADiskImportActiveBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mInterLADiskImportActiveBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	InterLADiskImportActiveUI zInterLADiskImportActiveUI = new InterLADiskImportActiveUI();
    }
}
