package com.sinosoft.lis.intermedium;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class LADiskImportTaxRateUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private LADiskImportTaxRateBL mLADiskImportTaxRateBL = null;

    public LADiskImportTaxRateUI()
    {
    	mLADiskImportTaxRateBL = new LADiskImportTaxRateBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mLADiskImportTaxRateBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mLADiskImportTaxRateBL.mErrors);
            return false;
        }
        //
        if(mLADiskImportTaxRateBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mLADiskImportTaxRateBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mLADiskImportTaxRateBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mLADiskImportTaxRateBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	LADiskImportTaxRateUI zLADiskImportTaxRateUI = new LADiskImportTaxRateUI();
    }
}
