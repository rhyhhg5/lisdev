package com.sinosoft.lis.intermedium;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentconfig.*;
import com.sinosoft.lis.agentquery.LAXBTQueryBL;
import com.sinosoft.lis.agentquery.LAXBTQueryUI;
import com.sinosoft.lis.vschema.LARateCommisionSet;
/**
 * <p>Title: LAAChargeWrapRateUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class InterProtocolPoundageUI
{
	 /**错误信息容器*/
    public CErrors mErrors = new CErrors();
    
    private VData mResult = new VData();

    public InterProtocolPoundageUI()
    {
        System.out.println("InterProtocolPoundageUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	InterProtocolPoundageBL bl = new InterProtocolPoundageBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }else{
        	 this.mResult = bl.getResult();
        }
        return true;
    }

    public static void main(String[] args)
    {
    	InterProtocolPoundageUI tInterProtocolPoundageUI = new   InterProtocolPoundageUI();
         System.out.println("LAXBTQueryUI"); 
    }
}
