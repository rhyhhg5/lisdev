package com.sinosoft.lis.intermedium;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LPrintTempSaveDownUI
{
    /**������Ϣ����*/
    public CErrors mErrors = new CErrors();

    public LPrintTempSaveDownUI()
    {
        System.out.println("LPrintTempSaveDownUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LPrintTempSaveDownBL bl = new LPrintTempSaveDownBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    	LPrintTempSaveDownUI tLPrintTempSaveDownUI = new   LPrintTempSaveDownUI();
         System.out.println("LPrintTempSaveDownUI");
    }
}
