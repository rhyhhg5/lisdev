package com.sinosoft.lis.intermedium;

import java.io.File;
import java.math.BigDecimal;

import com.sinosoft.lis.agentconfig.LAGetOtherChargeDiskImporter;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAGetOtherChargeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAGetOtherChargeSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAGetOtherChargeSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class InterLADiskImportActiveBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LAActiveChargeDiskImport.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
	
    private LAGetOtherChargeSet mLAGetOtherChargeSetFinal=new LAGetOtherChargeSet();
    
    private LAGetOtherChargeSet mLAGetOtherChargeSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public InterLADiskImportActiveBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        System.out.println("begin import");
        //从磁盘导入数据
        LAGetOtherChargeDiskImporter importer = new LAGetOtherChargeDiskImporter(fileName,
                configFileName,
                diskimporttype);
        //System.out.println("LADiskImportActiveBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        
        
        
        System.out.println("doimported");
        if (diskimporttype.equals("LAGetOtherCharge")) {
            mLAGetOtherChargeSet = (LAGetOtherChargeSet) importer
                                  .getSchemaSet();

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }
        this.mResult.add(mmap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("InterLADiskImportActiveBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
    	FDate chgdate = new FDate();
        //展业证信息校验
        if (diskimporttype.equals("LAGetOtherCharge")) {
            if (mLAGetOtherChargeSet == null) {
                mErrors.addOneError("导入佣金率信息失败！没有得到要导入的数据或者导入模版不是最新，请下载最新的模版。");
                return false;
            } else {
            	String tempSQL="select * from ldcom where (length(trim(comcode))=4 or length(trim(comcode))=8)";
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LAGetOtherChargeDB mTempLAGetOtherChargeDB=new LAGetOtherChargeDB();
            	LAAgentDB tLAAgentDB = new LAAgentDB();
            	LAAgentSet tLAAgentSet = new LAAgentSet();
            	//LAGetOtherChargeSet mLAGetOtherChargeSet=new LAGetOtherChargeSet();
            	LAGetOtherChargeSchema mTempLAGetOtherChargeSchema=new LAGetOtherChargeSchema();
            	LAGetOtherChargeSchema mTempLAGetOtherChargeSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
                for (int i = 1; i <= mLAGetOtherChargeSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLAGetOtherChargeSchema=mLAGetOtherChargeSet.get(i);
                    if (mTempLAGetOtherChargeSchema.getCalNo() == null ||
                    		mTempLAGetOtherChargeSchema.getCalNo().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("结算单号不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getMangeCom() == null ||
                    		mTempLAGetOtherChargeSchema.getMangeCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("销售机构不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getAgentName() == null ||
                    		mTempLAGetOtherChargeSchema.getAgentName().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("销售人员姓名不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getGroupagentCode() == null ||
                    		mTempLAGetOtherChargeSchema.getGroupagentCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("销售人员工号不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getContNo() == null ||
                    		mTempLAGetOtherChargeSchema.getContNo().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("保单号不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getAscripCom() == null ||
                    		mTempLAGetOtherChargeSchema.getAscripCom().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("归属机构代码不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getAscripName() == null ||
                    		mTempLAGetOtherChargeSchema.getAscripName().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("归属机构名称不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getRiskCode() == null ||
                    		mTempLAGetOtherChargeSchema.getRiskCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("险种代码不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getRiskName() == null ||
                    		mTempLAGetOtherChargeSchema.getRiskName().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("险种名称不能为空/");
                    }
                    
                    if (mTempLAGetOtherChargeSchema.getTransMoney() < 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("保费不能为空/");
                    } 
                    if (mTempLAGetOtherChargeSchema.getChargeRate() < 0 
                    		||mTempLAGetOtherChargeSchema.getChargeRate()>1) {
                    	System.out.println("InterLADiskImportActiveBL.java: getRate:"+mLAGetOtherChargeSet.get(i).getChargeRate());
                        this.merrorNum++;
                        this.merrorInfo.append("佣金比率应介于０与１之间/");
                    } 
                    if (mTempLAGetOtherChargeSchema.getSignDate()==null ||mTempLAGetOtherChargeSchema.getSignDate().equals("")  ) {
                        this.merrorNum++;
                        this.merrorInfo.append("签单日期不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getInterSalCom()==null ||mTempLAGetOtherChargeSchema.getInterSalCom().equals("")  ) {
                        this.merrorNum++;
                        this.merrorInfo.append("交叉销售机构不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getTmakeDate()==null ||mTempLAGetOtherChargeSchema.getTmakeDate().equals("")  ) {
                        this.merrorNum++;
                        this.merrorInfo.append("佣金生成日期不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getPayNum()< 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("佣金支付批次号不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getActNo()==null ||mTempLAGetOtherChargeSchema.getActNo().equals("")  ) {
                        this.merrorNum++;
                        this.merrorInfo.append("业务单证ID不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getCharge()< 0 ) {
                        this.merrorNum++;
                        this.merrorInfo.append("佣金不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getStartDate()==null ||mTempLAGetOtherChargeSchema.getStartDate().equals("")  ) {
                        this.merrorNum++;
                        this.merrorInfo.append("起保日期不能为空/");
                    }
                    if (mTempLAGetOtherChargeSchema.getEndDate()==null ||mTempLAGetOtherChargeSchema.getEndDate().equals("")  ) {
                        this.merrorNum++;
                        this.merrorInfo.append("终保日期不能为空/");
                    }
                    //金额较验
                    if (mTempLAGetOtherChargeSchema.getChargeRate() >=0 && mTempLAGetOtherChargeSchema.getTransMoney()>=0 && 
                    		mTempLAGetOtherChargeSchema.getCharge()>=0) {
                    	double rate = mTempLAGetOtherChargeSchema.getChargeRate();
                    	double charge = mTempLAGetOtherChargeSchema.getCharge();
                    	double charge1 = Arith.round(mulrate(rate,mTempLAGetOtherChargeSchema.getTransMoney()),2);
                    	if(charge != charge1){
                    		this.merrorNum++;
                            this.merrorInfo.append("录入的佣金，佣金比例，保费这三项中有误/");
                    	}
                        
                    }
                    if (mTempLAGetOtherChargeSchema.getStartDate() != null &&  !mTempLAGetOtherChargeSchema.getStartDate().equals("") &&
                    		mTempLAGetOtherChargeSchema.getEndDate() != null &&  !mTempLAGetOtherChargeSchema.getEndDate().equals("")	) {
                         String startDate = mTempLAGetOtherChargeSchema.getStartDate();
                         String endDate = mTempLAGetOtherChargeSchema.getEndDate();
                         if(endDate.compareTo(startDate) <= 0){
                        	 this.merrorNum++;
                             this.merrorInfo.append("录入终保日期必须大于起保日期/");
                         }
                    }
                    //销售机构较验
                    if(mTempLAGetOtherChargeSchema.getMangeCom()!=null
                    		&&!mTempLAGetOtherChargeSchema.getMangeCom().equals("")
                    		&&!mTempLAGetOtherChargeSchema.getMangeCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLAGetOtherChargeSchema.getMangeCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("销售机构代码不存在/");
                        }
                    }
                   //销售人员的较验
                    if (mTempLAGetOtherChargeSchema.getAgentName() != null &&
                    		!mTempLAGetOtherChargeSchema.getAgentName().equals(
                                       "")) {
                        String nameSql ="select * from laagent where managecom ='"+mTempLAGetOtherChargeSchema.getMangeCom()+"'";
                        tLAAgentSet =  tLAAgentDB.executeQuery(nameSql);
                        int tempFlag1=0;
                        for (int f = 1; f <= tLAAgentSet.size(); f++) {
							if(tLAAgentSet.get(f).getName().equals(mTempLAGetOtherChargeSchema.getAgentName())){
								tempFlag1 =1;
								break;
							}
						}
                        if(tempFlag1 == 0){
                        	this.merrorNum++;
                        	this.merrorInfo.append("在"+mTempLAGetOtherChargeSchema.getMangeCom()+"的销售机构下没" +
                        			"有这个名叫"+mTempLAGetOtherChargeSchema.getAgentName()+"的业务员/  ");
                        }
                    }
                    //销售人员工号的较验
                    if (mTempLAGetOtherChargeSchema.getGroupagentCode() != null &&
                    		!mTempLAGetOtherChargeSchema.getGroupagentCode().equals(
                                       "")) {
                        String codeSQL ="select * from laagent where name ='"+mTempLAGetOtherChargeSchema.getAgentName()+"'";
                        tLAAgentSet = tLAAgentDB.executeQuery(codeSQL);
                        int tempFlag1=0;
                        for (int g = 1; g <= tLAAgentSet.size(); g++) {
                        	if(tLAAgentSet.get(g).getGroupAgentCode()!= null &&!"".equals(tLAAgentSet.get(g).getGroupAgentCode())){
							  if(tLAAgentSet.get(g).getGroupAgentCode().equals(mTempLAGetOtherChargeSchema.getGroupagentCode())){
								tempFlag1 = 1;
								break;
							}
                         }
						}
                        if(tempFlag1 == 0){
                        	this.merrorNum++;
                        	this.merrorInfo.append("销售人员"+mTempLAGetOtherChargeSchema.getAgentName()+"的工号不为:"+mTempLAGetOtherChargeSchema.getGroupagentCode()+"!/ ");
                        }
                    }
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLAGetOtherChargeSet.size(); j++){
                        	mTempLAGetOtherChargeSchema2=mLAGetOtherChargeSet.get(j);
                        	if(mTempLAGetOtherChargeSchema.getMangeCom().equals(mTempLAGetOtherChargeSchema2.getMangeCom())
                        	&&mTempLAGetOtherChargeSchema.getRiskCode().equals(mTempLAGetOtherChargeSchema2.getRiskCode())
                        	&&mTempLAGetOtherChargeSchema.getCalNo().equals(mTempLAGetOtherChargeSchema2.getCalNo())
                        	&&mTempLAGetOtherChargeSchema.getAgentName().equals(mTempLAGetOtherChargeSchema2.getAgentName())
                        	&&mTempLAGetOtherChargeSchema.getGroupagentCode().equals(mTempLAGetOtherChargeSchema2.getGroupagentCode())
                        	&&mTempLAGetOtherChargeSchema.getContNo().equals(mTempLAGetOtherChargeSchema2.getContNo())
                        	&&mTempLAGetOtherChargeSchema.getAscripCom().equals(mTempLAGetOtherChargeSchema2.getAscripCom())
                			&&mTempLAGetOtherChargeSchema.getAscripName().equals(mTempLAGetOtherChargeSchema2.getAscripName())
                            &&mTempLAGetOtherChargeSchema.getRiskName().equals(mTempLAGetOtherChargeSchema2.getRiskName())
                        	&&mTempLAGetOtherChargeSchema.getTransMoney() == (mTempLAGetOtherChargeSchema2.getTransMoney())
                       		&&mTempLAGetOtherChargeSchema.getChargeRate() == (mTempLAGetOtherChargeSchema2.getChargeRate())
                       		&&mTempLAGetOtherChargeSchema.getSignDate().equals(mTempLAGetOtherChargeSchema2.getSignDate())
                       		&&mTempLAGetOtherChargeSchema.getInterSalCom().equals(mTempLAGetOtherChargeSchema2.getInterSalCom())
                       		&&mTempLAGetOtherChargeSchema.getTmakeDate().equals(mTempLAGetOtherChargeSchema2.getTmakeDate())
                       		&&mTempLAGetOtherChargeSchema.getPayNum() == (mTempLAGetOtherChargeSchema2.getPayNum())
                       		&&mTempLAGetOtherChargeSchema.getActNo().equals(mTempLAGetOtherChargeSchema2.getActNo())
                       		&&mTempLAGetOtherChargeSchema.getCharge() == (mTempLAGetOtherChargeSchema2.getCharge())
                        	&&mTempLAGetOtherChargeSchema.getStartDate().equals(mTempLAGetOtherChargeSchema2.getStartDate())
                       		&&mTempLAGetOtherChargeSchema.getEndDate().equals(mTempLAGetOtherChargeSchema2.getEndDate())
                       		&&mTempLAGetOtherChargeSchema.getAppntName().equals(mTempLAGetOtherChargeSchema2.getAppntName())
                       		&&mTempLAGetOtherChargeSchema.getCarNo().equals(mTempLAGetOtherChargeSchema2.getCarNo())
                       		&&mTempLAGetOtherChargeSchema.getGetMoneyName().equals(mTempLAGetOtherChargeSchema2.getGetMoneyName())
                       		&&mTempLAGetOtherChargeSchema.getBankCode().equals(mTempLAGetOtherChargeSchema2.getBankCode())
                       		&&mTempLAGetOtherChargeSchema.getOrgCrsPayVou().equals(mTempLAGetOtherChargeSchema2.getOrgCrsPayVou()))
                        	{
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLAGetOtherChargeSchema,mTempLAGetOtherChargeDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LAGetOtherChargeSchema mLAGetOtherChargeSchema,LAGetOtherChargeDB mLAPayOtherChargeDB){

	LAGetOtherChargeSet tempLAGetOtherChargeSet=new LAGetOtherChargeSet();
	String tempSql="select * from LAGetOtherCharge where mangecom='"
		+mLAGetOtherChargeSchema.getMangeCom()+"' and riskcode='"
		+mLAGetOtherChargeSchema.getRiskCode()+"' and calno='"
		+mLAGetOtherChargeSchema.getCalNo()+"' and agentname= '"
		+mLAGetOtherChargeSchema.getAgentName()+"' and groupagentcode='"
		+mLAGetOtherChargeSchema.getGroupagentCode()+"' and contno='"
		+mLAGetOtherChargeSchema.getContNo()+"' and ascripcom = '"
		+mLAGetOtherChargeSchema.getAscripCom()+"' and ascripname='"
		+mLAGetOtherChargeSchema.getAscripName()+"' and riskname='"
		+mLAGetOtherChargeSchema.getRiskName()+"' and transmoney="
		+mLAGetOtherChargeSchema.getTransMoney()+" and chargerate="
		+mLAGetOtherChargeSchema.getChargeRate()+" and signdate='"
		+mLAGetOtherChargeSchema.getSignDate()+"' and intersalcom='"
		+mLAGetOtherChargeSchema.getInterSalCom()+"' and tmakedate='"
		+mLAGetOtherChargeSchema.getTmakeDate()+"' and paynum="
		+mLAGetOtherChargeSchema.getPayNum()+" and actno='"
		+mLAGetOtherChargeSchema.getActNo()+"' and charge="
		+mLAGetOtherChargeSchema.getCharge()+" and startdate='"
		+mLAGetOtherChargeSchema.getStartDate()+"' and enddate='"
		+mLAGetOtherChargeSchema.getEndDate()+"'";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLAGetOtherChargeSet=mLAPayOtherChargeDB.executeQuery(tempSql);
	if(tempLAGetOtherChargeSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LAGetOtherChargeSchema tempLAGetOtherChargeSchema=new LAGetOtherChargeSchema();
		tempLAGetOtherChargeSchema.setMangeCom(mLAGetOtherChargeSchema.getMangeCom());
		tempLAGetOtherChargeSchema.setRiskCode(mLAGetOtherChargeSchema.getRiskCode());
		tempLAGetOtherChargeSchema.setCalNo(mLAGetOtherChargeSchema.getCalNo());
		tempLAGetOtherChargeSchema.setAgentName(mLAGetOtherChargeSchema.getAgentName());
		tempLAGetOtherChargeSchema.setGroupagentCode(mLAGetOtherChargeSchema.getGroupagentCode());
		tempLAGetOtherChargeSchema.setContNo(mLAGetOtherChargeSchema.getContNo());
		tempLAGetOtherChargeSchema.setAscripCom(mLAGetOtherChargeSchema.getAscripCom());
		tempLAGetOtherChargeSchema.setAscripName(mLAGetOtherChargeSchema.getAscripName());
		tempLAGetOtherChargeSchema.setRiskName(mLAGetOtherChargeSchema.getRiskName());
		tempLAGetOtherChargeSchema.setTransMoney(mLAGetOtherChargeSchema.getTransMoney());
		tempLAGetOtherChargeSchema.setChargeRate(mLAGetOtherChargeSchema.getChargeRate());
		tempLAGetOtherChargeSchema.setSignDate(mLAGetOtherChargeSchema.getSignDate());
		tempLAGetOtherChargeSchema.setInterSalCom(mLAGetOtherChargeSchema.getInterSalCom());
		tempLAGetOtherChargeSchema.setTmakeDate(mLAGetOtherChargeSchema.getTmakeDate());
		tempLAGetOtherChargeSchema.setPayNum(mLAGetOtherChargeSchema.getPayNum());
		tempLAGetOtherChargeSchema.setActNo(mLAGetOtherChargeSchema.getActNo());
		tempLAGetOtherChargeSchema.setCharge(mLAGetOtherChargeSchema.getCharge());
		tempLAGetOtherChargeSchema.setStartDate(mLAGetOtherChargeSchema.getStartDate());
		tempLAGetOtherChargeSchema.setEndDate(mLAGetOtherChargeSchema.getEndDate());
		tempLAGetOtherChargeSchema.setAppntName(mLAGetOtherChargeSchema.getAppntName());
		tempLAGetOtherChargeSchema.setCarNo(mLAGetOtherChargeSchema.getCarNo());
		tempLAGetOtherChargeSchema.setGetMoneyName(mLAGetOtherChargeSchema.getGetMoneyName());
		tempLAGetOtherChargeSchema.setBankCode(mLAGetOtherChargeSchema.getBankCode());
		tempLAGetOtherChargeSchema.setOrgCrsPayVou(mLAGetOtherChargeSchema.getOrgCrsPayVou());
		//tempLAGetOtherChargeSchema=mLAGetOtherChargeSchema;
		mLAGetOtherChargeSetFinal.add(tempLAGetOtherChargeSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	if(idxTemp == null || idxTemp.equals("")){
    		idxTemp = "0";
    	}
    	int idx=Integer.parseInt(idxTemp)+1;
        if (diskimporttype.equals("LAGetOtherCharge")) {
        	System.out.println("prepareData:doing");
            importPersons=mLAGetOtherChargeSetFinal.size();
            //System.out.println("prepareData:unImportPersons"+unImportPersons);
            if(mLAGetOtherChargeSetFinal!=null&&mLAGetOtherChargeSetFinal.size()>0){
             	//importPersons = mLAGetOtherChargeSetFinal.size();
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLAGetOtherChargeSetFinal.size(); i++) {
              	System.out.println("preparedata : managecom"+mLAGetOtherChargeSetFinal.get(i).getMangeCom());
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	System.out.println("BL:branchtype  "+branchtype);
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	System.out.println("BL:branchtype2  "+branchtype2);
              	mLAGetOtherChargeSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLAGetOtherChargeSetFinal.get(i).setIdx(idx++);
//              	mLAGetOtherChargeSetFinal.get(i).setBranchType(branchtype);
//              	mLAGetOtherChargeSetFinal.get(i).setBranchType2(branchtype2); 
              	mLAGetOtherChargeSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLAGetOtherChargeSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLAGetOtherChargeSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLAGetOtherChargeSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	mLAGetOtherChargeSetFinal.get(i).setActDate("00000000");
              	mLAGetOtherChargeSetFinal.get(i).setCompCode("00");
              	mLAGetOtherChargeSetFinal.get(i).setOrgCrsCode("0000");
              	mLAGetOtherChargeSetFinal.get(i).setOrgCrsCompCode("000");
              	//mLAGetOtherChargeSet.get(i).setRateType("11");
//              	mLAGetOtherChargeSetFinal.get(i).setVersionType("11");
              	mmap.put(mLAGetOtherChargeSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLAGetOtherChargeSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mLCGrpImportLogSet.get(i).setContNo("Active");
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");
        
        VData v = new VData();
        v.add(g);
        v.add(t);

        InterLADiskImportActiveBL d = new InterLADiskImportActiveBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }
    
    //double类型乘法计算
    public static double mulrate(double v1, double v2) {

        BigDecimal b1 = new BigDecimal(Double.toString(v1));

        BigDecimal b2 = new BigDecimal(Double.toString(v2));

        return b1.multiply(b2).doubleValue();

    }

}
