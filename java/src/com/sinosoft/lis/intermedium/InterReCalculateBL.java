package com.sinosoft.lis.intermedium;

import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LAACReCalculateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author 
 * @version 1.0----2009-1-13
 */
public class InterReCalculateBL {
  //错误处理类
	public CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */ 
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAChargeSet mLAChargeSet = new LAChargeSet();
  private LAChargeSet mUpLAChargeSet = new LAChargeSet();
  private LJAGetSet mLJAGetSet=new LJAGetSet();
  private String mManageCom="";
  private String mAgentCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mGrpContNo="";
  private String mCardNo="";
  private String mWrapCode="";
  private String mRiskCode="";
  private String mContFlag="";
  private String mChargeType="";
  
  public InterReCalculateBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionSetBL LaratecommisionSetbl = new LaratecommisionSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();

    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "InterReCalculateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      this.mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     if(this.mOperate.equals("SELECTPAY")){
    	 this.mLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
         System.out.println("LAChargeSet get"+mLAChargeSet.size());
     }
     else if(this.mOperate.equals("ALLPAY")){
    	 this.mManageCom=(String)cInputData.get(1);
    	 this.mAgentCom=(String)cInputData.get(2);
    	 this.mStartDate=(String)cInputData.get(3);
    	 this.mEndDate=(String)cInputData.get(4);
    	 this.mGrpContNo=(String)cInputData.get(5);
    	 this.mCardNo=(String)cInputData.get(6);
    	 this.mWrapCode=(String)cInputData.get(7);
    	 this.mRiskCode=(String)cInputData.get(8);
    	 this.mContFlag=(String)cInputData.get(9);
    	 this.mChargeType=(String)cInputData.get(10);
     }
     else{
    	 CError tError = new CError();
         tError.moduleName = "InterReCalculateBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "不支持的操作类型,请验证操作类型.";
         this.mErrors.addOneError(tError);
         return false;
     }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "InterReCalculateBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  //不需要数据校验
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin InterReCalculateBL.dealData........."+mOperate);
    try {
    	LAChargeDB tLAChargeDB=new LAChargeDB();
      if (mOperate.equals("SELECTPAY")) {
      	  String updateSql;
        for (int i = 1; i <= mLAChargeSet.size(); i++) {

              if(("55").equals(mLAChargeSet.get(i).getChargeType())){  
              	 updateSql=" update lacharge set "
              		+"chargerate=db2inst1.getactivechargerateqj(char(lacharge.branchtype),trim(char(lacharge.branchtype2)),"
              		+"char(lacharge.riskcode),char(lacharge.agentcom),char(lacharge.contno),char(lacharge.grpcontno),"
              		+"char((select payyear from lacommision where commisionsn=lacharge.commisionsn)),char((select payyears from lacommision where commisionsn=lacharge.commisionsn)),"
              		+"char('11'),char((select renewcount from lacommision where commisionsn=lacharge.commisionsn)),char((select transstate from lacommision where commisionsn=lacharge.commisionsn)),"
              		+"char(lacharge.payintv),char((select f3 from lacommision where commisionsn=lacharge.commisionsn)),char(lacharge.tmakedate),(case when lacharge.grpcontno ='00000000000000000000' then lacharge.polno else lacharge.grppolno end )),"
              		+"modifydate=current date,modifytime=current time   "
              		+" where commisionsn='"+mLAChargeSet.get(i).getCommisionSN()+"' and chargestate='0'";
              	map.put(updateSql, "UPDATE");
              	updateSql="update lacharge set charge=ROUND(transmoney * chargerate,2),PerCharge=round(transmoney * chargerate*0.8,2),RemainCharge=round(transmoney * chargerate*0.2,2),"
              		+"modifydate=current date,modifytime=current time "
              		+" where commisionsn='"+mLAChargeSet.get(i).getCommisionSN()+"'  and chargestate='0'";
              	map.put(updateSql, "UPDATE");
              }
              if(("56").equals(mLAChargeSet.get(i).getChargeType())){
                	 updateSql=" update lacharge set "
                  		+"chargerate=db2inst1.GETACTIVECHARGERATE(char(lacharge.branchtype),trim(char(lacharge.branchtype2)),"
                  		+"char(lacharge.riskcode),char(lacharge.managecom),char(lacharge.agentcom),"
                  		+"char('11'),char((select f3 from lacommision where commisionsn=lacharge.tcommisionsn)),(case when lacharge.grpcontno ='00000000000000000000' then lacharge.polno else lacharge.grppolno end )"
                  		+",(select char(transstate) from lacommision where commisionsn=lacharge.tcommisionsn))"
                  		+",modifydate=current date,modifytime=current time   "
                  		+" where commisionsn='"+mLAChargeSet.get(i).getCommisionSN()+"' and chargestate='0'";
                  	map.put(updateSql, "UPDATE");
                  	updateSql="update lacharge set charge=ROUND((select charge from lacharge b where b.commisionsn=lacharge.tcommisionsn) * chargerate,2),"
                  		+"modifydate=current date,modifytime=current time "
                  		+" where commisionsn='"+mLAChargeSet.get(i).getCommisionSN()+"'  and chargestate='0'";
                  	map.put(updateSql, "UPDATE");
              }
              
          }
        }
      
      else if(mOperate.equals("ALLPAY")){
    	  String tSQL="select * from lacharge where  branchtype='5' and branchtype2='01' and chargestate='0"
        	  +"' and managecom='"+this.mManageCom
        	  +"' and tmakedate between '"
        	  +this.mStartDate+"' and '"+this.mEndDate+"'";
    	  if("0".equals(this.mChargeType)){
    		  tSQL+=" and chargetype = '55' ";
    	  }
    	  if("1".equals(this.mChargeType)){
    		  tSQL+=" and chargetype = '56' ";
    	  }
    	  if(this.mAgentCom!=null&&!this.mAgentCom.equals("")){
    		  tSQL+=" and agentcom='"+this.mAgentCom+"'";
    	  }
    	  if(this.mGrpContNo!=null&&!this.mGrpContNo.equals("")){
    		  tSQL+=" and grpcontno='"+this.mGrpContNo+"'";
    	  }
    	  if(this.mCardNo!=null&&!this.mCardNo.equals("")){
    		  tSQL+=" and grpcontno in (select grpcontno from licertify where cardno ='"+mCardNo+"')";
    	  }
    	  if(this.mWrapCode!=null&&!this.mWrapCode.equals("")){
    		  tSQL+=" and exists  (select '1' from lacommision where f3='"+mWrapCode+"' and commisionsn=lacharge.commisionsn)";
    	  }
    	  if(this.mRiskCode!=null&&!this.mRiskCode.equals("")){
    		  tSQL+=" and riskcode='"+this.mRiskCode+"'";
    	  }
    	  if(this.mContFlag!=null&&!this.mContFlag.equals(""))
    	  {
    		  if("0".equals(this.mContFlag))
    		  {
    			  tSQL+=" and exists(select 1 from ljapay where payno = lacharge.receiptno and incomeno = (select contno from lacommision where commisionsn = lacharge.commisionsn) and duefeetype = '0' )";  
    		  }else if("1".equals(this.mContFlag))
    		  {
    			  tSQL+=" and not exists(select 1 from ljapay where payno = lacharge.receiptno and incomeno = (select contno from lacommision where commisionsn = lacharge.commisionsn) and duefeetype = '0' )";  
    		  }else{
    			  
    		  }
    	  }
    	  tSQL+=" with ur";
    	  System.out.println("打印sql如下："+tSQL);
    	  this.mUpLAChargeSet=tLAChargeDB.executeQuery(tSQL);
    	  String updateSql;
    	  for(int j=1;j<=mUpLAChargeSet.size();j++){
              if(("55").equals(mUpLAChargeSet.get(j).getChargeType())){  
                	 updateSql=" update lacharge set "
                		+"chargerate=db2inst1.getactivechargerateqj(char(lacharge.branchtype),trim(char(lacharge.branchtype2)),"
                		+"char(lacharge.riskcode),char(lacharge.agentcom),char(lacharge.contno),char(lacharge.grpcontno),"
                		+"char((select payyear from lacommision where commisionsn=lacharge.commisionsn)),char((select payyears from lacommision where commisionsn=lacharge.commisionsn)),"
                		+"char('11'),char((select renewcount from lacommision where commisionsn=lacharge.commisionsn)),char((select transstate from lacommision where commisionsn=lacharge.commisionsn)),"
                		+"char(lacharge.payintv),char((select f3 from lacommision where commisionsn=lacharge.commisionsn)),char(lacharge.tmakedate),(case when lacharge.grpcontno ='00000000000000000000' then lacharge.polno else lacharge.grppolno end )),"
                		+"modifydate=current date,modifytime=current time   "
                		+" where commisionsn='"+mUpLAChargeSet.get(j).getCommisionSN()+"' and chargestate='0'";
                	map.put(updateSql, "UPDATE");
                	updateSql="update lacharge set charge=ROUND(transmoney * chargerate,2),PerCharge=ROUND(transmoney * chargerate*0.8,2),RemainCharge =ROUND(transmoney * chargerate*0.2,2),"
                		+"modifydate=current date,modifytime=current time "
                		+" where commisionsn='"+mUpLAChargeSet.get(j).getCommisionSN()+"'  and chargestate='0'";
                	map.put(updateSql, "UPDATE");
                }
                if(("56").equals(mUpLAChargeSet.get(j).getChargeType())){
                  	 updateSql=" update lacharge set "
                    		+"chargerate=db2inst1.GETACTIVECHARGERATE(char(lacharge.branchtype),trim(char(lacharge.branchtype2)),"
                    		+"char(lacharge.riskcode),char(lacharge.managecom),char(lacharge.agentcom),"
                    		+"char('11'),char((select f3 from lacommision where commisionsn=lacharge.tcommisionsn)),(case when lacharge.grpcontno ='00000000000000000000' then lacharge.polno else lacharge.grppolno end )"
                    		+",(select char(transstate) from lacommision where commisionsn=lacharge.tcommisionsn))"
                    		+",modifydate=current date,modifytime=current time   "
                    		+" where commisionsn='"+mUpLAChargeSet.get(j).getCommisionSN()+"' and chargestate='0'";
                    	map.put(updateSql, "UPDATE");
                    	updateSql="update lacharge set charge=ROUND((select charge from lacharge b where b.commisionsn=lacharge.tcommisionsn)*chargerate,2),"
                    		+"modifydate=current date,modifytime=current time "
                    		+" where commisionsn='"+mUpLAChargeSet.get(j).getCommisionSN()+"'  and chargestate='0'";
                    	map.put(updateSql, "UPDATE");
                }
          }   	
      }     
      else{
    	  CError tError = new CError();
          tError.moduleName = "InterReCalculateBL";
          tError.functionName = "dealData";
          tError.errorMessage = "不支持的操作类型,请验证操作类型.";
          this.mErrors.addOneError(tError);
          return false;
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "InterReCalculateBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "InterReCalculateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private String getAgentComName(String tAgentCom){
	  String comName = "";
	  ExeSQL tExeSQL=new ExeSQL();
	  String sql=" select name from lacom where agentcom='"+tAgentCom+"' with ur ";
	  SSRS tSSRS = tExeSQL.execSQL(sql);	    	 
	  comName = tSSRS.GetText(1, 1);
	  return comName ;
  }
}
