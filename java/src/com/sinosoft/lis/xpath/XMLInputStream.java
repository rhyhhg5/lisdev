/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
//import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;

import org.jdom.input.SAXBuilder;

/**
 * XMLInputStream
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class XMLInputStream
{

    public static void main(String[] args) throws Exception
    {
//        String url = "http://192.168.70.156:8900/ui/HttpXml.jsp";
        String url = "http://219.239.36.20/lis/HttpXml.jsp";

        URL jspUrl = new URL(url);
        URLConnection uc = jspUrl.openConnection();
        uc.setDoOutput(true);
        OutputStream os = uc.getOutputStream(); //得到客户端的输出流对象
        System.out.println("URL===" + uc.getURL());
        String ts = "";

//        return String.valueOf(tCalendar.get(Calendar.YEAR)) + "-"
//                + String.valueOf(tCalendar.get(Calendar.MONTH) + 1) + "-"
//                + String.valueOf(tCalendar.get(Calendar.DATE));

//         ts = "<?xml version='1.0' encoding='UTF-8'?><DATASETS><DATASET><com>86110000</com>"
//                    +"<org>86110021</org><printtype>A</printtype></DATASET></DATASETS>";
//         ts =
//                "<?xml version='1.0' encoding='UTF-8'?><DATASETS><DATASET><com>86110000</com>"
//                +
//                "<org>86110001</org><printtype>A</printtype><filename>240110000000037.xml"
//                + "</filename></DATASET></DATASETS>";
        ts =
                "<?xml version='1.0' encoding='UTF-8'?><DATASETS><DATASET><usercode>actest"
                + "</usercode><pwd>actest</pwd></DATASET></DATASETS>";
//         ts = "<?xml version='1.0' encoding='UTF-8'?><DATASETS><DATASET><data><row><com>8611</com><org>86110100</org><printtype>A</printtype><filename>240110000000029.xml</filename><printflag>1</printflag></row><row><com>8611</com><org>86110100</org><printtype>A</printtype><filename>240110000000044.xml</filename><printflag>0</printflag></row></data></DATASET></DATASETS>";
//        StringBufferInputStream sbs = new StringBufferInputStream(ts);
//        int length = 0;
//        byte[] buffer = new byte[4096];
//        while ((length = sbs.read(buffer)) != -1)
//        {
//            os.write(buffer, 0, length);
//        }
//        os.flush(); //将输出流提交到服务器端
//        os.close(); //关闭输出流对象

        FileWriter fw = new FileWriter("e:/WriteData.xml"); //建立FileWriter对象，并实例化fw
//        StringBuffer contentBuf = new StringBuffer();
//        int size;
//        BufferedReader reader = request.getReader();
//        char[] charBuf = new char[2046];
//        while ((size = reader.read(charBuf)) > -1) {
//            contentBuf.append(charBuf, 0, size);
//        }
//        reader.close();

//        BufferedInputStream bufIs = null;
//        bufIs = new BufferedInputStream(ir);
//        String FilePath = "e:/WriteData.xml";
//        FileOutputStream fos = new FileOutputStream(FilePath);
//        int fn = 0;
//        while ((fn = ir.read()) != -1)
//        {
//            fos.write(fn);
//        }
//        fos.close();


//        BufferedReader bsr = new BufferedReader(isr);
//        char[] b = new char[2048];
//        String sf = "";
//        int i;
//        while ((i = bsr.read(b)) > -1)
//        {
//            sf += new String(b, 0, i - 1);
//        }
//        fw.write(sf);
//        fw.close();

        InputStream ir = uc.getInputStream();
//        byte[] bf=new byte[4096];
//        length = 0;
//        int lenshow =0;
//        ByteArrayOutputStream baos=new ByteArrayOutputStream();
//        while ( (length = ir.read(bf)) != -1) {
//                baos.write(bf,0,length);
//        }
//        baos.flush();
//        String returnValue=baos.toString("GB2312");
//        baos.close();
//        fw.write(returnValue);
//        fw.close();
//
        InputStreamReader isr = new InputStreamReader(ir);
        int n = 0;
        String s = "";
        char[] ic = new char[2046];
        StringBuffer stringBuffer = new StringBuffer();
        while ((n = isr.read(ic)) != -1)
        {
            stringBuffer.append(new String(ic, 0, n));
        }
//        fw.write(stringBuffer.toString());
//        fw.close();
        String t;
        t = s.substring(0, 1);
        if (t.compareTo("<") == 0)
        {
//            s = s;
        }
        else
        {
            s = s.substring(1);
        }
        System.out.println(s);

        //用org.jdom.Document解析数据流
        StringReader ls = new StringReader(s);
        SAXBuilder saxb = new SAXBuilder(false);
        org.jdom.Document aa = saxb.build(ls);
        System.out.println(aa.getRootElement().getChild("DATASET").getChild(
                "LCGrpCont.GrpName").
                           getText());
        System.out.println("os close");
    }
}
