package com.sinosoft.lis.xpath;

import java.io.InputStream;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ProReader
{

    //doc root  根元素
    private static final String XQLROOT = "/segItems";
    //segItm
    private static final String XQLSEGITEM = XQLROOT + "/segItem";
    //sourceItem  业务属性
    private static final String XQLSOURCEITEM = XQLSEGITEM + "/sourceItem";

    //transValues
    private static final String XQLTRANSVALUES = XQLSEGITEM + "/transValues";
    //transValue
    public static final String XQLTRANSVALUE = XQLTRANSVALUES + "/transValue";


    private String filePath = Conf.PROFILE;
    private Document m_document = null;
    private Node m_nodeRoot = null;

    // 以SegItem的Code属性为键值构建的树
    private TreeMap m_tmSegCode = new TreeMap();

    // 临时性的Document，用来将属于大的树的DOM节点转换成属于小的树的DOM节点
    private Document m_docTemp = null;


    public ProReader()
    {
    }

    public void load(String strFilePath) throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        m_docTemp = db.newDocument();
        m_document = db.parse(strFilePath);
        buildTree(); // 构建查找所需的树
    }

    public void load(InputStream ins) throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        m_docTemp = db.newDocument();
        m_document = db.parse(ins);
        buildTree(); // 构建查找所需的树
    }

    private void buildTree() throws Exception
    {
        m_nodeRoot = m_document.getDocumentElement();

        String str = "";
        Node node = null;
        NodeList nodelist = XPathAPI.selectNodeList(m_nodeRoot, XQLSEGITEM);

        m_tmSegCode.clear();

        for (int nIndex = 0; nIndex < nodelist.getLength(); nIndex++)
        {
            node = nodelist.item(nIndex);
            str = XPathAPI.eval(node, "@code").toString();

            if (m_tmSegCode.get(str) != null)
            {
                throw new Exception("SegItem 的编号重复了，Code = " + str);
            }

            // 保存的都是属于小的DOM树的节点
            node = transformNode(node);

            m_tmSegCode.put(str, node);
        }
    }

    public NodeList getAllSegItems() throws Exception
    {
        NodeList nl = XPathAPI.selectNodeList(m_nodeRoot, XQLSEGITEM);
        return nl;
    }

    /**
     * 根据SegItem节点的code属性来查找SegItem节点
     * @param strSegCode
     * @return
     */
    public Node findBySegCode(String strSegCode)
    {
        Node node = (Node) m_tmSegCode.get(strSegCode);
        return node;
    }

    /**
     * 将属于大的DOM树的节点转换成属于小的DOM树的节点
     * @param nodeSrc
     * @return
     */
    private Node transformNode(Node nodeSrc)
    {
        return m_docTemp.importNode(nodeSrc, true);
    }

    //main()
    public static void main(String[] args) throws Exception
    {
        ProReader proReader = new ProReader();

        proReader.load("property.xml");

        NodeList nl = proReader.getAllSegItems();
        for (int i = 0; i < nl.getLength(); i++)
        {
            Node node = nl.item(i);
            segItemPro sip = new segItemPro(node);
            System.out.println(sip.getSegItemProName());
            NodeList nls = sip.getAllTransValues();
            for (int j = 0; j < nls.getLength(); j++)
            {
                Node nodes = nls.item(j);
                transValue tv = new transValue(nodes);
                System.out.println("      " + tv.getTransValueSource() + "-->" +
                                   tv.getTransValueDetail());
            }
        }
    }

}


//*****************************************************************************

class segItemPro
{
    Node segItemProRoot;

    public segItemPro(Node segItemProRoot)
    {
        this.segItemProRoot = segItemProRoot;
    }

    private String getSegItemProChildValue(String relativeXQL) throws Exception
    {
        String result = XPathUsage.findValue(segItemProRoot, relativeXQL);
        return (result == null) ? "" : result;
    }

    public String getSegItemProName() throws Exception
    {
        return getSegItemProChildValue("@name");
    }

    public String getSegItemProCode() throws Exception
    {
        return getSegItemProChildValue("@code");
    }

    public String getSegItemProValueDef() throws Exception
    {
        return getSegItemProChildValue("@value_def");
    }

    public String getSegItemProFlag() throws Exception
    {
        return getSegItemProChildValue("@flag");
    }

    public String getSegItemProSourceItem() throws Exception
    {
        return getSegItemProChildValue("sourceItem");
    }

    public Node getTransValuesFollowing() throws Exception
    {
        return XPathAPI.selectSingleNode(segItemProRoot, "transValues");
    }

    public NodeList getAllTransValues() throws Exception
    {
        NodeList nl = XPathAPI.selectNodeList(segItemProRoot,
                                              "transValues/transValue");
        return nl;
    }

}


//*****************************************************************************

class transValue
{

    Node transValueRoot;

    public transValue(Node transValueRoot)
    {
        this.transValueRoot = transValueRoot;
    }

    private String getTransValueChildValue(String relativeXQL) throws Exception
    {
        String result = XPathUsage.findValue(transValueRoot, relativeXQL);
        return (result == null) ? "" : result;
    }

    public String getTransValueSource() throws Exception
    {
        return getTransValueChildValue("@value_sour");
    }

    public String getTransValueDetail() throws Exception
    {
        return getTransValueChildValue("@value_detail");
    }


}
