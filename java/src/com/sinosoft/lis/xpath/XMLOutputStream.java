/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.xpath;

import java.io.*;
import java.util.Hashtable;
import java.util.Map;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.utility.*;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * XMLOutputStream
 * <p>Title: xml流处理</p>
 * <p>Description: 保单接口http流处理</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft Inc.</p>
 * @author Yang Yalin
 * @version 1.0
 */
public class XMLOutputStream
{
    Map vouchers = new Hashtable(); //Xml生成使用对象
    private InputStream mInputStream; //输入流对象
    private XMLDatasets mXMLDatasets = null; //Xml对象
    private VData mResult = new VData(); //返回数据流容器
    private String mFileName = ""; //调试用输出文件名全局变量
    private FileInputStream mfi = null; //文件读入流对象
    private String mWebPath = ""; //文件路径


    /**
     * 通过传入数据流和文件路径构建类
     * @param in InputStream
     * @param WebPath String
     */
    public XMLOutputStream(InputStream in, String WebPath)
    {

        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                           "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.parsers.SAXParserFactory",
                           "org.apache.xerces.jaxp.SAXParserFactoryImpl");
        init(in);
        System.out.println("WebPah is " + WebPath);
        mWebPath = WebPath;
        System.out.println("new XMLOutPutStream over ......");
    }


    /**
     * 调试函数
     * @param args String[]
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
//        String ts = "";
//        ts = "<?xml version='1.0' encoding='UTF-8'?><DATASETS><DATASET><usercode>actest"
//                   +"</usercode><pwd>actest</pwd></DATASET></DATASETS>";
//        ts =
//               "<?xml version='1.0' encoding='UTF-8'?><DATASETS><DATASET><com>86110000</com>"
//               +
//               "<org>86110001</org><printtype>A</printtype><filename>240110000000037.xml"
//               + "</filename></DATASET></DATASETS>";
//        StringBufferInputStream sbs = new StringBufferInputStream(ts);
//        XMLOutputStream uff = new XMLOutputStream(sbs, "e:/work/");
//        uff.read(sbs);
//        uff.write();
    }


    /**
     * 获取输入流
     * @param in InputStream
     */
    private void init(InputStream in)
    {
        this.mInputStream = in;
    }


    /**
     * 解析传入xml数据信息
     * @param in InputStream
     * @throws Exception
     */
    public void read(InputStream in) throws Exception
    {
        System.out.println("begin parseXML");
        //传入流按UTF-8模式读取
//        byte[] b = new byte[4096];
//        String ss="";
//        int i;
//        //读取返回的输入流信息（由服务器端返回的流数据）
//        while ((i = in.read(b)) != -1)
//        {
//            ss += new String(b, 0, i - 1);
//        }
//        System.out.println("String is " + ss);
        //按照UTF－8的模式读取xml数据信息
        InputStreamReader isr = new InputStreamReader(in, "UTF-8");
        System.out.println("开始读入...");
        int n = 0;
        //使用StringBuffer读取数据，可以提高效率
        StringBuffer sb = new StringBuffer();
        while ((n = isr.read()) != -1)
        {
            sb.append((char) n);
        }
        String s = new String(sb);
//        System.out.println("");
//        System.out.println("String is " + s + "*******");

        StringReader lsr = new StringReader(s);
        SAXBuilder saxb = new SAXBuilder(false);
        Document doc = null;
        //将流转换为doc
        try
        {
            doc = saxb.build(lsr);
            //设置根节点标签对象
            Element eDataSet = doc.getRootElement().getChild("DATASET");
            String ls = "";
            System.out.println("输入流校验......");
            if (in != null)
            {
                //目前没有传入一个标签表明是何种操作，因此程序的友好性不够，希望以后能够添加该标签
                try
                {
                    //登陆信息处理
                    if (ls.compareTo("") == 0)
                    {
                        ls = getXMLValueLogin(eDataSet);
                    }
                    //打印完毕处理
                    if (ls.compareTo("") == 0)
                    {
                        ls = getXMLValueReturn(eDataSet);
                    }
                    //查询打印文件信息
                    if (ls.compareTo("") == 0)
                    {
                        ls = getXMLValueFile(eDataSet);
                    }
                    //查询打印条件下文件列表
                    if (ls.compareTo("") == 0)
                    {
                        ls = getXMLValueFileList(eDataSet);
                    }
                }
                catch (Exception ex)
                {
                    System.out.println("出错Http请求!");
                }
            }
            System.out.println("over");
        }
        catch (Exception ex)
        {
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("message",
                    "无法解析传入流"));
        }
    }


    /**
     * 登陆信息处理
     * @param eDataSet Element
     * @return String
     * @throws Exception
     */
    private String getXMLValueLogin(Element eDataSet) throws
            Exception
    {
        //判定是否有usercode标签
        if (eDataSet.getChild("usercode") == null)
        {
            return "";
        }
        //获取异常
        try
        {
            String tUserCode = eDataSet.getChild("usercode").getText();
            String tPwd = eDataSet.getChild("pwd").getText(); //用户密码
            String tPwdE = ""; //密码加密
            //XML对象
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            //加密程序
            LisIDEA tLisIDEA = new LisIDEA();
            tPwdE = tLisIDEA.encryptString(tPwd);
            //查询用户表
            LDUserDB tLDUserDB = new LDUserDB();
            tLDUserDB.setUserCode(tUserCode);
            tLDUserDB.setPassword(tPwdE);
            LDUserSet tLDUserSet = new LDUserSet();
            tLDUserSet = tLDUserDB.query();
            //判定用户是否存在
            if (tLDUserSet.size() == 1)
            {
                //存在处理
                XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
                xmlDatasetP.addDataObject(new XMLDataTag("flag", "1"));
                xmlDatasetP.addDataObject(new XMLDataTag("usercode", tUserCode));
                xmlDatasetP.addDataObject(new XMLDataTag("username",
                        tLDUserSet.get(1).getUserName()));
                //查询用户所在机构信息
                LDComDB tLDComDB = new LDComDB();
                tLDComDB.setComCode(tLDUserSet.get(1).getComCode().substring(0,
                        4));
                tLDComDB.getInfo();
                //用户的管理机构信息，截取机构信息的前4位为管理机构
                xmlDatasetP.addDataObject(new XMLDataTag("usercom",
                        tLDComDB.getName()));
                System.out.println("*************" +
                                   tLDUserSet.get(1).getComCode().length() +
                                   "*************");
                if (tLDUserSet.get(1).getComCode().length() > 4)
                {
                    //如果机构信息超过4位，先从LDCom表中查询数据，如果没有则查询LABranchGroup表
                    tLDComDB.setComCode(tLDUserSet.get(1).getComCode());
                    if (tLDComDB.getInfo())
                    {
                        xmlDatasetP.addDataObject(new XMLDataTag("userorg",
                                tLDComDB.getName()));
                    }
                    else
                    {
                        //分公司编码不一定要在4位
                        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                        tLABranchGroupDB.setBranchAttr(tLDUserSet.get(1).
                                getComCode());
                        xmlDatasetP.addDataObject(new XMLDataTag("userorg",
                                tLABranchGroupDB.query().get(1).getName()));
                    }
                }
                else
                {
                    //如果机构编码没有超过4位，则从LDcom中获取机构信息
                    tLDComDB.setComCode(tLDUserSet.get(1).getComCode());
                    tLDComDB.getInfo();
                    xmlDatasetP.addDataObject(new XMLDataTag("userorg",
                            tLDComDB.getName()));
                }

                //调用描述，处理该用户的打印权限信息
                getUserPrint(xmlDatasetP, "UserPrint.xml", "_USERCODE",
                             tUserCode);
                System.out.println("该用户存在！");
            }
            else
            {
                //不存在处理
                XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
                xmlDatasetP.addDataObject(new XMLDataTag("flag", "0"));
                xmlDatasetP.addDataObject(new XMLDataTag("message",
                        "用户密码不正确，或用户不存在！"));
                System.out.println("该用户不存在！");
            }
        }
        catch (Exception ex)
        {
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("flag", "0"));
            xmlDatasetP.addDataObject(new XMLDataTag("message", "获取登陆用户信息出错"));
        }
        mFileName = "Login.xml";
        return "1";
    }


    /**
     * 获取查询条件下的代打印文件列表
     * @param eDataSet Element
     * @return String
     * @throws Exception
     */
    private String getXMLValueFileList(Element eDataSet) throws
            Exception
    {
        //判定是否有com标签
        if (eDataSet.getChild("com") == null)
        {
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("message",
                    "传入XML流标签异常"));
            return "";
        }
        //获取异常
        try
        {
            String tCom = eDataSet.getChild("com").getText(); //分公司编码
            String tOrg = eDataSet.getChild("org").getText(); //机构编码
            String tPrintType = eDataSet.getChild("printtype").getText(); //打印类型
            //XML对象
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            //文件路径
            //查询一级目录是否存在
            String FilePath = mWebPath + tCom + "/";
            if (new File(FilePath).exists() == false)
            {
                xmlDatasetP.addDataObject(new XMLDataTag("message",
                        "服务器无此查询条件的目录结构"));
            }
            else
            {
                //查询二级目录是否存在
                FilePath = mWebPath + tCom + "/" + tOrg + "/";
                if (new File(FilePath).exists() == false)
                {
                    xmlDatasetP.addDataObject(new XMLDataTag("message",
                            "服务器无此查询条件的目录结构"));
                }
                else
                {
                    //查询三级目录是否存在
                    FilePath = mWebPath + tCom + "/" + tOrg + "/" + tPrintType +
                               "/";
                    if (new File(FilePath).exists() == false)
                    {
                        xmlDatasetP.addDataObject(new XMLDataTag("message",
                                "服务器无此查询条件的目录结构"));
                    }
                    else
                    {
                        //如果文件路径存在，查询路径下的所有文件信息
                        xmlDatasetP.createFileListName(FilePath);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("message",
                    "获取查询条件下保单文件列表出错"));
        }
        mFileName = "FileList.xml";
        return "1";
    }


    /**
     * 获取查询条件下的代打印文件数据
     * @param eDataSet Element
     * @return String
     * @throws Exception
     */
    private String getXMLValueFile(Element eDataSet) throws
            Exception
    {

        //判定是否有filename标签
        if (eDataSet.getChild("filename") == null)
        {
            return "";
        }

        //获取异常
        try
        {
            String tFileName = eDataSet.getChild("filename").getText(); //打印文件
            String tCom = eDataSet.getChild("com").getText(); ; //分公司编码
            String tOrg = eDataSet.getChild("org").getText(); ; //机构编码
            String tPrintType = eDataSet.getChild("printtype").getText(); ; //打印类型
            //文件路径，原则上这里不会发生文件不存在的事情，所以没有对文件的存在性做校验
            String FileName = mWebPath + tCom + "/" + tOrg + "/" + tPrintType +
                              "/" +
                              tFileName;
            //获取文件信息，放入倒一个流对象中
            mfi = new FileInputStream(FileName);
        }
        catch (Exception ex)
        {
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("message", "获取保单xml数据出错"));
        }
        mFileName = "File.xml";
        return "1";
    }


    /**
     * 打印结果返回处理
     * @param eDataSet Element
     * @return String
     * @throws Exception
     */
    private String getXMLValueReturn(Element eDataSet) throws
            Exception
    {
        //判定是否有printflag标签
        if (eDataSet.getChild("data") == null)
        {
            return "";
        }
        //获取异常
        try
        {
            String tPrintFlag = "";
            String tCom = ""; //分公司编码
            String tOrg = ""; //机构编码
            String tPrintType = ""; //打印类型
            String tFileName = ""; //xml文件名
            String FilePath = ""; //文件路径
            Element eRow = null;

            //循环查询需要处理的文件信息
            for (int i = 0;
                         i < eDataSet.getChild("data").getChildren("row").size();
                         i++)
            {
                eRow = (Element) eDataSet.getChild("data").getChildren("row").
                       get(i);
                tPrintFlag = eRow.getChild("printflag").getText();
                tCom = eRow.getChild("com").getText();
                tOrg = eRow.getChild("org").getText();
                tPrintType = eRow.getChild("printtype").getText();
                tFileName = eRow.getChild("filename").getText();
                //如果文件打印成功，则文件需要修改，或替换
                System.out.println("i     " + i + tCom + tOrg + tPrintType +
                                   tFileName);
                FilePath = mWebPath + tCom + "/" + tOrg + "/" + tPrintType +
                           "/";
                File f = new File(FilePath, tFileName);
                if (tPrintFlag.compareTo("1") == 0)
                {
                    //其实如果要处理的文件不存在，应该报一个错误
                    if (f.exists())
                    {
                        f.delete(); //文件删除
                    }
                }
            }

            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("dealflag", "1"));
        }
        catch (Exception ex)
        {
            mXMLDatasets = new XMLDatasets();
            mXMLDatasets.createDocument();
            XMLDatasetP xmlDatasetP = mXMLDatasets.createDatasetP();
            xmlDatasetP.addDataObject(new XMLDataTag("message", "处理打印完毕保单数据出错"));
        }
        mFileName = "Return.xml";
        return "1";
    }


    /**
     * 根据配置文件生成xml数据
     * @param xmlDatasetP XMLDatasetP  XML流
     * @param FileName String  配置文件名
     * @param SelectCodeType String  查询参数
     * @param SelectCode String  查询参数值
     * @throws Exception
     */
    private void getUserPrint(XMLDatasetP xmlDatasetP, String FileName,
                              String SelectCodeType, String SelectCode) throws
            Exception
    {
        //数据流
        XMLDataFormater innerFormater = null;
        String szTemplateFile = "";
        //获得配置文件路径
        szTemplateFile = mWebPath + FileName;
        //校验配置文件是否存在
        if (new File(szTemplateFile).exists() == false)
        {
            throw new Exception("缺少配置文件：" + szTemplateFile);
        }
        Hashtable hashData = new Hashtable();
        //将查询值值赋给xml文件
        hashData.put(SelectCodeType, SelectCode);
        //根据配置文件生成xml数据
        XMLDataMine xmlDataMine = new XMLDataMine(new FileInputStream(
                szTemplateFile), hashData);
        xmlDataMine.setDataFormater(innerFormater);
        xmlDatasetP.addDataObject(xmlDataMine);
    }


    /**
     * 调试用输出信息
     * @throws Exception
     */
    public void write() throws Exception
    {
        BufferedInputStream bufIs = null;
        if (mfi == null)
        {
            bufIs = new BufferedInputStream(mXMLDatasets.
                                            getInputStream());
        }
        else
        {
            bufIs = new BufferedInputStream(mfi);
        }
        String FilePath = mWebPath + mFileName;
        FileOutputStream fos = new FileOutputStream(FilePath);
        int n = 0;
        while ((n = bufIs.read()) != -1)
        {
            fos.write(n);
        }
        fos.close();
    }


    /**
     * 返回信息
     * @return VData
     * @throws Exception
     */
    public VData getResult() throws Exception
    {

        this.mResult.clear();
        //根据流对象的不同，返回方式不太一样
        if (mfi == null)
        {
            //xml对象流返回
            this.mResult.add(mXMLDatasets.getInputStream());
        }
        else
        {
            //文件流返回
            this.mResult.add(mfi);
        }
        return this.mResult;
    }

}
