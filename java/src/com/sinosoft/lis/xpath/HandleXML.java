/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import java.io.FileOutputStream;
import java.util.List;
import com.sinosoft.lis.pubfun.PubFun;
import org.jdom.*;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import java.io.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author 朱向峰
 * @version 1.0
 */
public class HandleXML {
    private Element myElement;
    public int intFileCount;
    public int intFileNum;
    public HandleXML() {
    }

    public static void main(String[] args) throws Exception {
        HandleXML handlexml = new HandleXML();
        handlexml.BuildXML("d:/", "OF3-2005-06-01-12-12-25-0.xml");
    }

    public int setFilecount(int intfile) {
        intFileCount = intfile;
        return intFileCount;
    }

    public int setfilenum(int intfilenum) {
        intFileNum = intfilenum;
        return intFileNum;
    }

    public String BuildXML(String cPath, String cFileName) throws Exception {
        String Fileposition = "";
        String FileWritename = "";
        String billtype = "";
        String message = "";
        //设置输出xml元素
        Element tOutput_root = new Element("ufinterface");
        Element tOutput_voucher = new Element("voucher");
        Element tOutput_voucher_header = new Element("voucher_header");
        Element tOutput_voucher_body = new Element("voucher_body");
        //将根元素植入文档Document中
        Document tOutput_Doc = new Document(tOutput_root);

        //设置解析xml对象
        SAXBuilder tbuilder = new SAXBuilder();
        //将文件信息读入到Document对象中
        Fileposition = cPath + "/toFinXml/" + cFileName;
        Document tInput_Doc = tbuilder.build(Fileposition);

        //获取读入xml的根节点
        Element tInput_root = tInput_Doc.getRootElement();
        //将读入xml的根节点信息写入到输出xml的根节点中
        AddEleAttr("billtype", tInput_root.getAttribute("billtype").getValue(),
                   tOutput_root);
        AddEleAttr("filename", tInput_root.getAttribute("filename").getValue(),
                   tOutput_root);
        AddEleAttr("isexchange",
                   tInput_root.getAttribute("isexchange").getValue(),
                   tOutput_root);
        AddEleAttr("operation", tInput_root.getAttribute("operation").getValue(),
                   tOutput_root);
        AddEleAttr("proc", tInput_root.getAttribute("proc").getValue(),
                   tOutput_root);
        AddEleAttr("receiver", tInput_root.getAttribute("receiver").getValue(),
                   tOutput_root);
        AddEleAttr("replace", tInput_root.getAttribute("replace").getValue(),
                   tOutput_root);
        AddEleAttr("roottag", tInput_root.getAttribute("roottag").getValue(),
                   tOutput_root);
        AddEleAttr("sender", tInput_root.getAttribute("sender").getValue(),
                   tOutput_root);
        AddEleAttr("subtype", tInput_root.getAttribute("subtype").getValue(),
                   tOutput_root);

        Attribute tta = new Attribute("xmlns",
                                      "http://www.sinosoft.com/picc/intf/ufinterface.xsd");
        Namespace ttn = Namespace.getNamespace("msdata",
                                               "urn:schemas-microsoft-com:xml-msdata");
        Attribute ttb = new Attribute("xmlns", "", ttn);

        Namespace ttm = Namespace.getNamespace("xsi",
                                               "http://www.w3.org/2001/XMLSchema-instance");
        Attribute ttd = new Attribute("schemaLocation",
                                      "http://www.sinosoft.com/picc/intf/ufinterface.xsd",
                                      ttm);
        tOutput_root.addAttribute(tta);
        tOutput_root.addAttribute(ttb);
        tOutput_root.addAttribute(ttd);

        //获取读入xml的根节点下的voucher节点
        Element tInput_voucher = tInput_Doc.getRootElement().getChild("voucher");
        AddEleAttr("id", tInput_voucher.getAttribute("id").getValue(),
                   tOutput_voucher);

        //获取读入xml的voucher_header节点
        Element tInput_voucher_header = tInput_Doc.getRootElement().getChild(
                "voucher").getChild("voucher_header");
        //获取voucher_header下的元素信息
        AddElement("company", tInput_voucher_header.getChild("company").getText(),
                   tOutput_voucher_header);
        AddElement("voucher_type",
                   tInput_voucher_header.getChild("voucher_type").getText(),
                   tOutput_voucher_header);
        AddElement("year",
                   tInput_voucher_header.getChild("year").getText(),
                   tOutput_voucher_header);
        AddElement("fiscal_year",
                   tInput_voucher_header.getChild("fiscal_year").getText(),
                   tOutput_voucher_header);
        AddElement("standby",
                   tInput_voucher_header.getChild("standby").getText(),
                   tOutput_voucher_header);
        AddElement("accounting_period",
                   tInput_voucher_header.getChild("accounting_period").getText(),
                   tOutput_voucher_header);
        AddElement("no",
                   tInput_voucher_header.getChild("no").getText(),
                   tOutput_voucher_header);
        billtype = tInput_voucher_header.getChild("voucher_id").getText();
        AddElement("voucher_id",
                   tInput_voucher_header.getChild("voucher_id").getText(),
                   tOutput_voucher_header);
        AddElement("attachment",
                   tInput_voucher_header.getChild("attachment").getText(),
                   tOutput_voucher_header);
        AddElement("attachment_number",
                   tInput_voucher_header.getChild("attachment_number").getText(),
                   tOutput_voucher_header);
        AddElement("date",
                   tInput_voucher_header.getChild("date").getText(),
                   tOutput_voucher_header);

        AddElement("enter",
                   tInput_voucher_header.getChild("enter").getText(),
                   tOutput_voucher_header);

        AddElement("cashier",
                   tInput_voucher_header.getChild("cashier").getText(),
                   tOutput_voucher_header);

        AddElement("signature",
                   tInput_voucher_header.getChild("signature").getText(),
                   tOutput_voucher_header);

        AddElement("checker",
                   tInput_voucher_header.getChild("checker").getText(),
                   tOutput_voucher_header);

        AddElement("tallydate",
                   tInput_voucher_header.getChild("tallydate").getText(),
                   tOutput_voucher_header);

        AddElement("manager",
                   tInput_voucher_header.getChild("manager").getText(),
                   tOutput_voucher_header);

        AddElement("voucher_making_system",
                   tInput_voucher_header.getChild("voucher_making_system").
                   getText(),
                   tOutput_voucher_header);
        AddElement("memo1",
                   tInput_voucher_header.getChild("memo1").getText(),
                   tOutput_voucher_header);

        AddElement("memo2",
                   tInput_voucher_header.getChild("memo2").getText(),
                   tOutput_voucher_header);

        AddElement("reserve1",
                   tInput_voucher_header.getChild("reserve1").getText(),
                   tOutput_voucher_header);

        AddElement("reserve2",
                   tInput_voucher_header.getChild("reserve2").getText(),
                   tOutput_voucher_header);

        //获取读入xml的voucher_body节点
        Element tInput_voucher_body = tInput_Doc.getRootElement().getChild(
                "voucher").getChild("voucher_body");
        //获取voucher_body节点下的entry列表
        List tList = tInput_voucher_body.getChildren("entry");
        System.out.print("headerend");
        //循环entry，获取其下的个元素信息
        for (int i = 0; i < tList.size(); i++) {
            //初始化一个输出xml的entry对象
            myElement = new Element("entry");
            //获得当前入xml的entry对象
            Element tInput_entry = (Element) tList.get(i);

            AddElement("entry_id", tInput_entry.getChild("entry_id").getText());
            if (tInput_entry.getChild("account_code").getText().equals("22030103"))
                System.out.println("err :"+tInput_entry.getChild("account_code").getText());
            AddElement("account_code",
                       tInput_entry.getChild("account_code").getText());

            AddElement("accbooktype",
                       tInput_entry.getChild("accbooktype").getText());
            AddElement("accbookcode",
                       tInput_entry.getChild("accbookcode").getText());
            AddElement("centercode",
                       tInput_entry.getChild("centercode").getText());
            AddElement("branchcode",
                       tInput_entry.getChild("branchcode").getText());
            AddElement("policytype", "");
            AddElement("policytakeway", "");
            AddElement("policytaketype",
                       tInput_entry.getChild("bankcode").getText());
            AddElement("policytakeflag", "");
            AddElement("receiptno",
                       tInput_entry.getChild("receiptno").getText());
            AddElement("policy_no",
                       tInput_entry.getChild("policy_no").getText());
            AddElement("Policynoother",
                       tInput_entry.getChild("Policynoother").getText());
            AddElement("policytype1",
                       tInput_entry.getChild("policytype1").getText());
            AddElement("policyno1",
                       tInput_entry.getChild("policyno1").getText());
            AddElement("Policystart",
                       tInput_entry.getChild("Policystart").getText());
            AddElement("Policyend",
                       tInput_entry.getChild("Policyend").getText());
            AddElement("operdate",
                       tInput_entry.getChild("operdate").getText());
            AddElement("policydate",
                       tInput_entry.getChild("policydate").getText());
            AddElement("policyresp",
                       tInput_entry.getChild("policyresp").getText());
            AddElement("itemdesc",
                       tInput_entry.getChild("itemdesc").getText());
            AddElement("relareason",
                       "");
            AddElement("agreementyear",
                       "");
            AddElement("avalibletype",
                       "");
            AddElement("avalibleterm",
                       "");
            AddElement("sellchannel",
                       transellChannel(tInput_entry.getChild("sellchannel").
                                       getText()));
            AddElement("takeway",
                       "");
            AddElement("accountno",
                       tInput_entry.getChild("accountno").getText());
            AddElement("check_no",
                       tInput_entry.getChild("check_no").getText());
            AddElement("F01",
                       tInput_entry.getChild("F01").getText());
            AddElement("F02",
                       tInput_entry.getChild("F02").getText());
            AddElement("F03",
                       tInput_entry.getChild("F03").getText());
            AddElement("F04",
                       tInput_entry.getChild("F04").getText());
            AddElement("F05",
                       tInput_entry.getChild("F05").getText());
            AddElement("F06",
                       tranf06(tInput_entry.getChild("F06").getText()));
            AddElement("F07",
                       tranf07(tInput_entry.getChild("F07").getText()));
            AddElement("F08",
                       tInput_entry.getChild("F08").getText());
            AddElement("F09",
                       tInput_entry.getChild("F09").getText());
            AddElement("F10",
                       tInput_entry.getChild("F10").getText());
            AddElement("F11",
                       tInput_entry.getChild("F11").getText());
            AddElement("F12",
                       tInput_entry.getChild("F12").getText());
            AddElement("F13",
                       "");
            AddElement("F14",
                       tInput_entry.getChild("F14").getText());
            AddElement("F15",
                       "");
            AddElement("F16",
                       "");
            AddElement("F17",
                       "");
            AddElement("F18",
                       tInput_entry.getChild("F18").getText());
            AddElement("F19",
                       "");
            AddElement("F20",
                       "");
            AddElement("F21",
                       "");
            AddElement("F22",
                       tInput_entry.getChild("F22").getText());
            AddElement("F23",
                       "");
            AddElement("F24",
                       "");
            AddElement("F25",
                       "");
            AddElement("F26",
                       "");
            AddElement("F27",
                       "");
            AddElement("F28",
                       "");
            if (tInput_entry.getChild("account_code").getText().equals("4101")) {
                AddElement("F29", "QD/");
                AddElement("F30",
                           transellChannel(tInput_entry.getChild("sellchannel").
                                           getText()) + "/");
            } else {
                AddElement("F29",
                           tInput_entry.getChild("F29").getText());
                AddElement("F30",
                           tInput_entry.getChild("F30").getText());
            }

            AddElement("rawunit",
                       tInput_entry.getChild("rawunit").getText());
            AddElement("rawoper",
                       tInput_entry.getChild("rawoper").getText());
            AddElement("policykey",
                       "");
            AddElement("temp",
                       "");
            AddElement("abstract",
                       tInput_entry.getChild("abstract").getText());
            AddElement("settlement",
                       tInput_entry.getChild("settlement").getText());
            AddElement("account_period",
                       tInput_entry.getChild("account_period").getText());
            AddElement("transaction_date",
                       tInput_entry.getChild("transaction_date").getText());
            AddElement("document_id",
                       tInput_entry.getChild("document_id").getText());
            AddElement("document_date",
                       tInput_entry.getChild("document_date").getText());
            AddElement("currency",
                       tInput_entry.getChild("currency").getText());
            AddElement("exchange_rate1",
                       tInput_entry.getChild("exchange_rate1").getText());
            AddElement("exchange_rate2",
                       tInput_entry.getChild("exchange_rate2").getText());
            AddElement("dc_marker",
                       tInput_entry.getChild("dc_marker").getText());
            AddElement("debit_quantity",
                       tInput_entry.getChild("debit_quantity").getText());
            AddElement("primary_debit_amount",
                       tInput_entry.getChild("primary_debit_amount").getText());
            AddElement("natural_debit_currency",
                       tInput_entry.getChild("natural_debit_currency").getText());
            AddElement("secondary_debit_amount",
                       tInput_entry.getChild("secondary_debit_amount").getText());
            AddElement("credit_quantity",
                       tInput_entry.getChild("credit_quantity").getText());
            AddElement("primary_credit_amount",
                       tInput_entry.getChild("primary_credit_amount").getText());
            AddElement("natural_credit_currency",
                       tInput_entry.getChild("natural_credit_currency").getText());
            AddElement("secondary_credit_amount",
                       tInput_entry.getChild("secondary_credit_amount").getText());
//             System.out.print("body");
            //将entry标签添加到voucher_body对象中
            tOutput_voucher_body.addContent(myElement);
        }
        //添加元素到输出xml中
        tOutput_voucher.addContent(tOutput_voucher_header);
        tOutput_voucher.addContent(tOutput_voucher_body);
        tOutput_root.addContent(tOutput_voucher);

        //输出xml到文件
        XMLOutputter XMLOut = new XMLOutputter("  ", true, "UTF-8");

        String tCt = PubFun.getCurrentTime();
        tCt = tCt.replace(':', '-');
        if (intFileCount > 0) {
            FileWritename = "PICC-" + billtype + "-" + PubFun.getCurrentDate() +
                            "-" + intFileCount + ".xml";
            Fileposition = cPath + "PiccFinxml/" + FileWritename;
        } else {
            String tTimeString = PubFun.getCurrentDate() + "-" + tCt;
            FileWritename = "PICC-" + billtype + "-" + tTimeString + ".xml";
            Fileposition = cPath + "PiccFinxml/" + FileWritename;
        }
        File FileXmlnew = new File(cPath + "PiccFinxml/");
        if (!FileXmlnew.exists()) {
            FileXmlnew.mkdir();
        }

        FileOutputStream tFos = new FileOutputStream(Fileposition);
        XMLOut.output(tOutput_Doc, tFos);
        System.out.print("writeend");
        BillXmlOutput tBillXmlOutput = new BillXmlOutput();
        if (!tBillXmlOutput.findfile(cPath + "PiccFinxml/", FileWritename)) {
            message = "没有生成出第三张凭证";
        } else {
//            String copypath = cPath + "PiccFinxml/" + "PICC-" +
//                              billtype +
//                              "-" + PubFun.getCurrentDate();
//
//            if ((intFileCount == intFileNum) && billtype.equals("3")) {
//                UniteXML isdone = new UniteXML();
//                for (int i = 2; i <= intFileNum; i++) {
//                    System.out.println(copypath + "-" + i + ".xml");
//                    if (isdone.isMerging(copypath + "-1.xml",
//                                         copypath + "-" + i + ".xml")) {
//                        System.out.println("XML files have been merged.");
//                    } else {
//                        System.out.println("XML files have NOTbeen merged.");
//                    }
//                    File foo = new File(copypath + "-" + i + ".xml");
//                    try {
//                        if (foo.exists()) {
//                            if (foo.delete()) {
//                                System.out.println("已经删除文件" + copypath + "-" +
//                                        i +
//                                        ".xml");
//
//                            }
//                        }
//                    } catch (Exception ex) {
//                        System.out.println(ex);
//                    }
//                }
//                Fileposition = copypath + "-1.xml";
//                System.out.println("开始执行数据传输......");
//                try {
//                    message = tBillXmlOutput.submitData(Fileposition);
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    System.out.println("url is error");
//                }
//                System.out.println(Fileposition);
//            } else {
//                message = "";
//            }
//            if (!billtype.equals("3")) {
                message = tBillXmlOutput.submitData(Fileposition);
//            }

        }
        return message;
    }


    /**
     * 向element下指定元素添加属性
     * @param attr String
     * @param value String
     * @param cElement Element
     * @return Element
     */
    public Element AddEleAttr(String attr, String value, Element cElement) {
        cElement.addAttribute(attr, value);
        return cElement;
    }

    /**
     * 向动态element添加内容
     * @param name String
     * @param value String
     * @return Element
     */
    public Element AddElement(String name, String value) {
        Element level = new Element(name);
        level.setText(value);
        this.myElement.addContent(level);

        return myElement;
    }

    /**
     * 向指定element添加内容
     * @param name String
     * @param value String
     * @param cElement Element
     * @return Element
     */
    public Element AddElement(String name, String value, Element cElement) {
        Element level = new Element(name);
        level.setText(value);
        cElement.addContent(level);

        return cElement;
    }

    private String transellChannel(String vt) {
        if (vt.equals("01")) {
            return "QD01"; //QD01	个人代理            02-个人营销
        }
        if (vt.equals("02")) {
            return "QD05"; //QD05	公司直销            01-团险直销
        }
        if (vt.equals("03")) {
            return "QD02"; //QD02	保险专业代理         05-专业代理
        }
        if (vt.equals("05")) {
            return "QD04"; //QD04	其他兼业代理         04-兼业代理
        }
        if (vt.equals("04")) {
            return "QD03"; //QD03	银行邮政代理         03-银行代理
        }
        if (vt.equals("06")) {
            return "QD06"; //QD06	保险经纪公司         06-经纪公司
        }
        if (vt.equals("07")) {
            return "QD99"; //QD99	其他                07-不计业绩销售渠道
        }
        if(vt.equals("08")){
            return "QD99"; //QD99	其他                08-其他
        }
        if (vt.equals("99")) {
            return "QD99"; //QD99	99其他                其他
        } else {
            return "";
        }

    }


    private String tranf07(String vt) {
        if (vt.equals("S")) {
            return "01";
        }
        if (vt.equals("M")) {
            return "02"; // L--长险(Long)、M--一年期险(Middle)、S--极短期险(Short)
        }
        if (vt.equals("L")) {
            return "03";
        } else {
            return ""; //01	一年期以内  02	一年期  03	一年期以上
        }

    }

    private String tranf06(String vt) {

        if (vt.equals("H")||vt.equals("M")) {
            return "01";
        }
        if (vt.equals("A")) {
            return "02";
        }
        if (vt.equals("O")) {
            return "01";
        }
        if (vt.equals("R")) {
            return "02";
        }
        if (vt.equals("L")) {
            return "01";
        }

        else {
            return "";
        }
        //01健康险02意外伤害险03其他
        //L--寿险(Life)、A--意外险(Accident)、H--健康险(Health)

    }


}
