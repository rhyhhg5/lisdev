/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class ReqParamWriter
{

    String template = "<?xml version=\"1.0\" encoding=\"GB2312\"?>"
                      + "\n" + "<!--DOCTYPE voucher SYSTEM \"vouchere.dtd\"-->"
                      + "\n" + "<ufinterface roottag='voucher' billtype='gl' subtype='run' replace='Y' receiver='1003' sender='1101' proc='add' operation='req' isexchange='Y' filename='pz.xml'>"
                      + "\n" + "<voucher id=\"112101\">"
                      + "\n" + "</voucher>"
                      + "\n" + "</ufinterface>";
    private String filePath = Conf.TEMPLATEFILE;
    //doc root
    private static final String XQLROOT = "/ufinterface";
    //bill
    private static final String XQLVOUCHER = XQLROOT + "/voucher";
    //bill_head
    private static final String XQLBILLHEAD = XQLVOUCHER + "/voucher_header";
    //bill_body
    private static final String XQLVOUCHERBODY = XQLVOUCHER + "/voucher_body";
    //entry
    private static final String XQLENTRY = XQLVOUCHERBODY + "/entry";
    private Document document;
    private Node root;
    private Node voucherRoot;
    private Node voucherHeader;
    private Node voucherBody;

    public ReqParamWriter()
    {
    }

    public ReqParamWriter(String filePath)
    {
        this.filePath = filePath;
    }

    public void parseXML() throws Exception
    {
        StringReader sr = new StringReader(this.template);
        InputSource inputSource = new InputSource(sr);
        // Parse the file
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
//    document = db.parse(filePath);
        document = db.parse(inputSource);
        root = document.getDocumentElement();
        voucherRoot = XPathAPI.selectSingleNode(root, XQLVOUCHER);

    }

    /**
     * 加一个新节点,并分行
     * @param start 开始节点
     * @param name 新节点名
     * @return 新节点
     */
    private Node addNodeNewLine(Node start, String name)
    {
        Document document = start.getOwnerDocument();
        Node newLine = document.createTextNode("\n");
//    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    public Node getRootNode() throws Exception
    {
        return this.root;
    }

    public Node getVoucherHeaderNode() throws Exception
    {
        if (this.voucherHeader == null)
        {
            voucherHeader = addNodeNewLine(voucherRoot, "voucher_header");
        }

        return voucherHeader;
    }

    public Node getVoucherBodyNode() throws Exception
    {
        if (this.voucherBody == null)
        {
            voucherBody = addNodeNewLine(voucherRoot, "voucher_body");
        }

        return voucherBody;
    }

    public Node getVoucherNode() throws Exception
    {
        return voucherRoot;
    }

    public void setCompany(String company) throws Exception
    {
        addSetHeadValue("company", company);
    }

    public void setVoucherType(String vt) throws Exception
    {
        addSetHeadValue("voucher_type", vt);
    }

    public void setYear(String year) throws Exception
    {
        addSetHeadValue("year", year);
    }

    //whn_add
    public void setfiscal_year(String year) throws Exception
    {
        addSetHeadValue("fiscal_year", year);
    }

    public void setstandby(String vt) throws Exception
    {
        //addSetHeadValue("period",vt);//accounting_period
        addSetHeadValue("standby", vt);

    }


    public void setlodebatch(String vt) throws Exception
        {
            //addSetHeadValue("period",vt);//accounting_period
            addSetHeadValue("lodebatch", vt);

        }

    public void setPeriod(String vt) throws Exception
    {
        //addSetHeadValue("period",vt);//accounting_period
        addSetHeadValue("accounting_period", vt);

    }

    public void setNo(String no) throws Exception
    {
        addSetHeadValue("no", no);

    }

    //whn_add
    public void setvoucher_id(String no) throws Exception
    {
        addSetHeadValue("voucher_id", no);
    }

    public void setAttachment(String vt) throws Exception
    {
        addSetHeadValue("attachment", vt);

    }

    //whn_add
    public void setattachment_number(String vt) throws Exception
    {
        addSetHeadValue("attachment_number", vt);

    }

    public void setDate(String date) throws Exception
    {
        addSetHeadValue("date", date);

    }

    public void setEnter(String vt) throws Exception
    {
        addSetHeadValue("enter", vt);

    }

    public void setCashier(String cashier) throws Exception
    {
        addSetHeadValue("cashier", cashier);

    }

    public void setSignature(String vt) throws Exception
    {
        addSetHeadValue("signature", vt);

    }

    public void setChecker(String checker) throws Exception
    {
        addSetHeadValue("checker", checker);

    }

    public void setTallydate(String vt) throws Exception
    {
        addSetHeadValue("tallydate", vt);

    }

    public void setManager(String manager) throws Exception
    {
        addSetHeadValue("manager", manager);

    }

    public void setSystem(String vt) throws Exception
    {
        addSetHeadValue("voucher_making_system", vt);
    }


    public void setMemo1(String memo1) throws Exception
    {
        addSetHeadValue("memo1", memo1);

    }

    public void setMemo2(String vt) throws Exception
    {
        addSetHeadValue("memo2", vt);

    }

    public void setReserve1(String vt) throws Exception
    {
        addSetHeadValue("reserve1", vt);

    }


    public void setReserve2(String vt) throws Exception
    {
        addSetHeadValue("reserve2", vt);

    }

    public void addSetHeadValue(String newNodeName, String value) throws
            Exception
    {
        Node headNode = getVoucherHeaderNode();
        Node newNode = addNodeNewLine(headNode, newNodeName);
        setNodeValue(newNode, value);

    }

    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }


    public static void p(String in)
    {
        System.out.println(in);
    }

    /**
     * clone a ReqParamWriter object, deep clone.
     * @return
     * @throws Exception
     */
    public ReqParamWriter rpwClone() throws Exception
    {

        /*
             ReqParamWriter newRPW=new ReqParamWriter();
             newRPW.parseXML();

             Node newRoot=root.cloneNode(true);

             newRPW.document.removeChild(newRPW.root);
             newRPW.document.adoptNode(newRoot);
             newRPW.document.appendChild(newRoot);

             newRPW.root=newRoot;
         newRPW.voucherHeader=XPathAPI.selectSingleNode(newRoot,XQLBILLHEAD);
         newRPW.voucherBody=XPathAPI.selectSingleNode(newRoot,XQLVOUCHERBODY);
             newRPW.voucherRoot=XPathAPI.selectSingleNode(newRoot,XQLVOUCHER);


             return newRPW;
         */
        ReqParamWriter newRPW = new ReqParamWriter();
        newRPW.parseXML();
        Node newRoot = root.cloneNode(true);
        //新建一个document
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        newRPW.document = db.newDocument();

        newRPW.root = newRPW.document.importNode(newRoot, true);
        newRPW.document.appendChild(newRPW.root);

        newRPW.voucherHeader = XPathAPI.selectSingleNode(newRPW.root,
                XQLBILLHEAD);
        newRPW.voucherBody = XPathAPI.selectSingleNode(newRPW.root,
                XQLVOUCHERBODY);
        newRPW.voucherRoot = XPathAPI.selectSingleNode(newRPW.root, XQLVOUCHER);

        return newRPW;
    }

    public static void main(String[] args) throws Exception
    {
        ReqParamWriter reqParamWriter1 = new ReqParamWriter();
        reqParamWriter1.parseXML();
        reqParamWriter1.setCompany("yang company");
        reqParamWriter1.setVoucherType("yang vouchertype");

        Node bn = reqParamWriter1.getVoucherBodyNode();
        RPEntry rp = new RPEntry(bn);

        rp.setEntryId("yang entry id");

        p(XPathUsage.toString(reqParamWriter1.root));
    }


}


class RPEntry
{

    private Node bodyNode;
    private Node entryNode;
    private Document document;

    public RPEntry(Node bodyNode) throws Exception
    {
        this.bodyNode = bodyNode;

//    document=bodyNode.getOwnerDocument();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        document = db.newDocument();

        Node nodeBody = document.createElement("temp_body");

        entryNode = addNodeNewLine(nodeBody, "entry");
    }

    /**
     * Kevin 2003-07-25
     * 在生成了所有的数据之后，再将Node添加到原来的DOM树中去。
     * 如果一开始就在原来的DOM树中添加Node，会使生成数据的速度越来越慢。
     * @throws Exception
     */
    public void attachToNode() throws Exception
    {
        Node nodeTemp = this.bodyNode.getOwnerDocument().createTextNode("\n");
        this.bodyNode.appendChild(nodeTemp);

        entryNode = this.bodyNode.getOwnerDocument().importNode(entryNode, true);
        this.bodyNode.appendChild(entryNode);
    }

    public void setEntryId(String v) throws Exception
    {
        addSetEntryValue("entry_id", v);

    }

    public void set(String v) throws Exception
    {
        addSetEntryValue("", v);

    }

    public void setAccountCode(String v) throws Exception
    {
        addSetEntryValue("account_code", v);

    }

    public void setAbstract(String v) throws Exception
    {
        addSetEntryValue("abstract", v);

    }

    public void setSettlement(String v) throws Exception
    {
        addSetEntryValue("settlement", v);

    }

    public void setaccountingperiod(String v) throws Exception
    {
        addSetEntryValue("account_period", v);

    }

    public void settransationdate(String v) throws Exception
    {
        addSetEntryValue("transaction_date", v);

    }


    public void setDocumentId(String v) throws Exception
    {
        addSetEntryValue("document_id", v);

    }

    public void setDocumentDate(String v) throws Exception
    {
        addSetEntryValue("document_date", v);

    }


    public void setCurrency(String v) throws Exception
    {
        addSetEntryValue("currency", v);

    }

    public void setUnitPrice(String v) throws Exception
    {
        addSetEntryValue("unit_price", v);

    }


    public void setExcrate1(String v) throws Exception
    {
        //addSetEntryValue("excrate1",v);
        addSetEntryValue("exchange_rate1", v);

    }

    public void setRenew(String v) throws Exception
    {
        addSetEntryValue("renew", v);

    }

    //grppol_no
    public void setGrppolNo(String v) throws Exception
    {
        addSetEntryValue("grppol_no", v);
    }

    public void setaccbooktype(String v) throws Exception
    {
        addSetEntryValue("accbooktype", v);
    }

    public void setaccbookcode(String v) throws Exception
    {
         addSetEntryValue("accbookcode", v);
    }

    public void setcentercode(String v) throws Exception
    {
         addSetEntryValue("centercode", v);
    }

    public void setbranchcode(String v) throws Exception
    {
         addSetEntryValue("branchcode", v);
    }

    public void setPolicynoother(String v) throws Exception
    {
         addSetEntryValue("Policynoother", v);
    }

    public void setpolicytype1(String v) throws Exception
    {
         addSetEntryValue("policytype1", v);
    }

    public void setpolicyno1(String v) throws Exception
    {
         addSetEntryValue("policyno1", v);
    }

    public void setPolicystart(String v) throws Exception
    {
         addSetEntryValue("Policystart", v);
    }

    public void setPolicyend(String v) throws Exception
    {
       addSetEntryValue("Policyend", v);
    }

    public void setoperdate(String v) throws Exception
    {
        addSetEntryValue("operdate", v);
    }

    public void setpolicydate(String v) throws Exception
    {
        addSetEntryValue("policydate", v);
    }

public void setpolicyresp(String v) throws Exception
{
  addSetEntryValue("policyresp", v);
}

public void setitemdesc(String v) throws Exception
{
  addSetEntryValue("itemdesc", v);
}

public void setrelareason(String v) throws Exception
{
  addSetEntryValue("relareason", v);
}

public void setagreementyear(String v) throws Exception
{
  addSetEntryValue("agreementyear", v);
}

public void setavalibletype(String v) throws Exception
{
  addSetEntryValue("avalibletype", v);
}

public void setavalibleterm(String v) throws Exception
{
  addSetEntryValue("avalibleterm", v);
}

public void setsellchannel(String v) throws Exception
{
  addSetEntryValue("sellchannel", v);
}

public void settakeway(String v) throws Exception
{
  addSetEntryValue("takeway", v);
}

public void setaccountcode(String v) throws Exception
{
  addSetEntryValue("accountcode", v);
}

public void setF01(String v) throws Exception
{
  addSetEntryValue("F01", v);
}

public void setF02(String v) throws Exception
{
  addSetEntryValue("F02", v);
}

public void setF03(String v) throws Exception
{
  addSetEntryValue("F03", v);
}


public void setF04(String v) throws Exception
{
  addSetEntryValue("F04", v);
}


public void setF05(String v) throws Exception
{
  addSetEntryValue("F05", v);
}


public void setF06(String v) throws Exception
{
  addSetEntryValue("F06", v);
}


public void setF07(String v) throws Exception
{
  addSetEntryValue("F07", v);
}


public void setF08(String v) throws Exception
{
  addSetEntryValue("F08", v);
}

public void setF09(String v) throws Exception
{
  addSetEntryValue("F09", v);
}

public void setF10(String v) throws Exception
{
  addSetEntryValue("F10", v);
}

public void setF11(String v) throws Exception
{
  addSetEntryValue("F11", v);
}

public void setF12(String v) throws Exception
{
  addSetEntryValue("F12", v);
}

public void setF13(String v) throws Exception
{
  addSetEntryValue("F13", v);
}


public void setF14(String v) throws Exception
{
  addSetEntryValue("F14", v);
}


public void setF15(String v) throws Exception
{
  addSetEntryValue("F15", v);
}


public void setF16(String v) throws Exception
{
  addSetEntryValue("F16", v);
}


public void setF17(String v) throws Exception
{
  addSetEntryValue("F17", v);
}


public void setF18(String v) throws Exception
{
  addSetEntryValue("F18", v);
}

public void setF19(String v) throws Exception
{
  addSetEntryValue("F19", v);
}

public void setF20(String v) throws Exception
{
  addSetEntryValue("F20", v);
}

public void setF21(String v) throws Exception
{
  addSetEntryValue("F21", v);
}

public void setF22(String v) throws Exception
{
  addSetEntryValue("F22", v);
}

public void setF23(String v) throws Exception
{
  addSetEntryValue("F23", v);
}


public void setF24(String v) throws Exception
{
  addSetEntryValue("F24", v);
}


public void setF25(String v) throws Exception
{
  addSetEntryValue("F25", v);
}


public void setF26(String v) throws Exception
{
  addSetEntryValue("F26", v);
}


public void setF27(String v) throws Exception
{
  addSetEntryValue("F27", v);
}


public void setF28(String v) throws Exception
{
  addSetEntryValue("F28", v);
}

public void setF29(String v) throws Exception
{
  addSetEntryValue("F29", v);
}

public void setF30(String v) throws Exception
{
  addSetEntryValue("F30", v);
}

public void setrawunit(String v) throws Exception
{
  addSetEntryValue("rawunit", v);
}

public void setrawoper(String v) throws Exception
{
  addSetEntryValue("rawoper", v);
}

public void setpolicykey(String v) throws Exception
{
  addSetEntryValue("policykey", v);
}

public void settemp(String v) throws Exception
{
  addSetEntryValue("temp", v);
}



    public void setExcrate2(String v) throws Exception
    {
        //addSetEntryValue("excrate2",v);
        addSetEntryValue("exchange_rate2", v);

    }


    public void setDebitQuantity(String v) throws Exception
    {
        addSetEntryValue("debit_quantity", v);

    }

    public void setdcmarker(String v) throws Exception
    {
        addSetEntryValue("dc_marker", v);

    }

    public void setDebitAmount(String v) throws Exception
    {
        //addSetEntryValue("debit_amount",v);
        addSetEntryValue("primary_debit_amount", v);

    }


    public void setFracDebitAmount(String v) throws Exception
    {
        //addSetEntryValue("frac_debit_amount",v);
        addSetEntryValue("secondary_debit_amount", v);

    }

    public void setLocalDebitCurrency(String v) throws Exception
    {
        //addSetEntryValue("local_debit_currency",v); //whn local -> natural
        addSetEntryValue("natural_debit_currency", v);

    }


    public void setCreditQuantity(String v) throws Exception
    {
        addSetEntryValue("credit_quantity", v);

    }

    public void setCreditAmount(String v) throws Exception
    {
        //addSetEntryValue("credit_amount",v);
        addSetEntryValue("primary_credit_amount", v);
    }


    public void setFracCreditAmount(String v) throws Exception
    {
        //addSetEntryValue("frac_credit_amount",v);
        addSetEntryValue("secondary_credit_amount", v);
    }

    public void setLocalCreditCurrency(String v) throws Exception
    {
        //addSetEntryValue("local_credit_currency",v); //whn local -> natural
        addSetEntryValue("natural_credit_currency", v);

    }

    public void setBillType(String v) throws Exception
    {
        addSetEntryValue("bill_type", v);
    }


    public void setBillId(String v) throws Exception
    {
        addSetEntryValue("bill_id", v);
    }


    public void setBillDate(String v) throws Exception
    {
        addSetEntryValue("bill_date", v);
    }

    public void appendAux(Node aux) throws Exception
    {
        Node newLine = entryNode.getOwnerDocument().createTextNode("\n");
        //entryNode.getOwnerDocument().adoptNode(aux);

        entryNode.appendChild(entryNode.getOwnerDocument().importNode(aux, true));
        entryNode.appendChild(newLine);
    }


    public void setDetail(String v) throws Exception
    {
        addSetEntryValue("detail", v);

    }

    public void setRiskInfo(String v) throws Exception
    {
        addSetEntryValue("risk_info", v);
    }

    public void setPayWayOfRiskInfo(String v) throws Exception
    {
        addSetEntryAttrValue("risk_info", "pay_way", v);
    }

    public void setPayPeriodOfRiskInfo(String v) throws Exception
    {
        addSetEntryAttrValue("risk_info", "pay_period", v);
    }

    public void setPayTimeOfRiskInfo(String v) throws Exception
    {
        addSetEntryAttrValue("risk_info", "pay_time", v);
    }

    public void setBussNo(String v) throws Exception
    {
        addSetEntryValue("buss_no", v);
    }

    public void setCheckNo(String v) throws Exception
    {
        addSetEntryValue("check_no", v);
    }

    public void setPolicyNo(String v) throws Exception
    {
        addSetEntryValue("policy_no", v);
    }

    public void setAccountDep(String v) throws Exception
    {
        addAccountValue("部门", v);
    }

    public void setAccountPerson(String v) throws Exception
    {
        addAccountValue("人员", v);
    }

    public void setAccountCus(String v) throws Exception
    {
        addAccountValue("客户", v);
    }

    public void setAccountSup(String v) throws Exception
    {
        addAccountValue("供应商", v);
    }

    public void setAccountPro(String v) throws Exception
    {
        addAccountValue("项目", v);
    }

    //modify by hou
    //添加节点及其Value
    public void addSetEntryValue(String newNodeName, String value) throws
            Exception
    {
        //System.out.println("得到 newNodeName ：" + newNodeName);
        if (XPathAPI.selectSingleNode(entryNode, newNodeName) != null)
        {
            //System.out.println("找到节点"+newNodeName);
            Node tempNode = XPathAPI.selectSingleNode(entryNode, newNodeName);
            setNodeValue(tempNode, value);
        }
        else
        {
            //System.out.println("测试添加新的节点" + newNodeName);
            Node newNode = addNodeNewLine(entryNode, newNodeName);
            setNodeValue(newNode, value);
        }
    }

    //add by hou
    //添加节点属性及其Value
    private void addSetEntryAttrValue(String NodeName, String AttrName,
                                      String value) throws Exception
    {

        Node targetNode = XPathAPI.selectSingleNode(entryNode, NodeName);
        if (targetNode != null)
        {
            //System.out.println("进入If");

            Element riskinfo = (Element) targetNode;
            riskinfo.setAttribute(AttrName, value);
            //entryNode.appendChild(riskinfo);
            //Node newLine=document.createTextNode("\n");
            //entryNode.appendChild(newLine);
        }
        else
        {

            //System.out.println("来到Else");
            //Node newNode=addNodeNewLine(entryNode,NodeName);
            //newNode.getChildNodes();
            Element riskinfo = document.createElement(NodeName);
            riskinfo.setAttribute(AttrName, value);
            //att.setNodeValue(value);

            entryNode.appendChild(riskinfo);
            Node newLine = document.createTextNode("\n");
            entryNode.appendChild(newLine);

            //    Element linkurl=document.createElement("haha");
            //Text textseg=document.createTextNode("url");
            //linkurl.appendChild(textseg);
            //entryNode.appendChild(linkurl);
        }
        //增加一个新节点，再增加属性
    }

    //("部门",value)
    private void addAccountValue(String AttrName, String NodeValue) throws
            Exception
    {
        String NodeName = "auxiliary_accounting";
        Node targetNode = XPathAPI.selectSingleNode(entryNode, NodeName);
        Node newLine = document.createTextNode("\n");
        //如果已经存在

        if (targetNode != null)
        {
            //auxAccount aa = new auxAccount(targetNode);
            //if(aa.getAccDep())
            System.out.println("进入If");
            Element account = (Element) targetNode;
            Element attr = document.createElement("item");
            attr.setAttribute("name", AttrName);
            //attr.setNodeValue(NodeValue);
            setNodeValue(attr, NodeValue);
            account.appendChild(newLine);
            account.appendChild((Node) attr);
            account.appendChild(newLine);
            //entryNode.appendChild();
            //riskinfo.setAttribute(AttrName,value);
            //entryNode.appendChild(riskinfo);
            //Node newLine=document.createTextNode("\n");
            //entryNode.appendChild(newLine);
        }
        else
        {

            System.out.println("来到Else");

            Element account = document.createElement(NodeName);
            Element attr = document.createElement("item");
            //account.appendChild(newLine);
            attr.setAttribute("name", AttrName);
            attr.setNodeValue(NodeValue);
            setNodeValue(attr, NodeValue);
            //attr.appendChild(newLine);
            account.appendChild(newLine);
            account.appendChild((Node) attr);
            account.appendChild(newLine);
            entryNode.appendChild((Node) account);
            entryNode.appendChild(newLine);
            //Node newLine=document.createTextNode("\n");
            //entryNode.appendChild(newLine);
            //Element riskinfo = document.createElement(NodeName);
            //riskinfo.setAttribute(AttrName,value);
            //att.setNodeValue(value);

            //entryNode.appendChild(riskinfo);
            //Node newLine=document.createTextNode("\n");
            //      entryNode.appendChild(newLine);

            //    Element linkurl=document.createElement("haha");
            //Text textseg=document.createTextNode("url");
            //linkurl.appendChild(textseg);
            //entryNode.appendChild(linkurl);
        }

    }


    //add by hou
    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }

    private Node addNodeNewLine(Node start, String name)
    {
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        //start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    private Node addNode(Node start, String name)
    {
        //Node newLine=document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);
        //    start.appendChild(newLine);
        start.appendChild(newNode);
        //start.appendChild(newLine);
        return newNode;
    }
}
