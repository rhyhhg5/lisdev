package com.sinosoft.lis.xpath;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public class BuildProXML
{

    String templateStr = "<?xml version=\"1.0\" encoding=\"GB2312\"?>"
                         + "\n" + "<segItems>"
                         + "\n" + "</segItems>";

    private String filePath = Conf.TEMPLATEFILE;

    //doc root
    private static final String XQLROOT = "/segItems";

    private static final String XQLSEGITEM = XQLROOT + "/segItem";

    private static final String XQLSOURCEITEM = XQLSEGITEM + "/sourceItem";

    private static final String XQLTRANSVALUES = XQLSEGITEM + "/transValues";

    private static final String XQLTRANSVALUE = XQLTRANSVALUES + "/transValue";

    private Document document;
    private Node root;
    private Node segItemRoot;
    private Node tranVsRoot;
    private Node tranVRoot;

    public BuildProXML()
    {
        //System.out.println(this.templateStr);
    }

    public BuildProXML(String filePath)
    {
        this.filePath = filePath;
    }

    public void parseXML() throws Exception
    {
        StringReader sr = new StringReader(this.templateStr);
        InputSource inputSource = new InputSource(sr);

        // Parse the file
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        //    document = db.parse(filePath);
        document = db.parse(inputSource);
        root = document.getDocumentElement();
        segItemRoot = XPathAPI.selectSingleNode(root, XQLSEGITEM);
    }

    /**
     * 加一个新节点,并分行
     * @param start 开始节点
     * @param name 新节点名
     * @return 新节点
     */
    private Node addNodeNewLine(Node start, String name)
    {
        Document document = start.getOwnerDocument();
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    public Node getRootNode() throws Exception
    {
        return this.root;
    }

    public Node getSegItemNode() throws Exception
    {
        if (this.segItemRoot == null)
        {
            segItemRoot = addNodeNewLine(this.root, "segItem");
        }

        return segItemRoot;
    }

    public Node getTransValuesNode() throws Exception
    {
        if (this.tranVsRoot == null)
        {
            tranVsRoot = addNodeNewLine(segItemRoot, "mainItem");
        }

        return tranVsRoot;
    }

    public void setSourceItem(String value) throws Exception
    {
        addSetSegValue("sourceItem", value);
    }

    public void addSetSegValue(String newNodeName, String value) throws
            Exception
    {
        Node headNode = this.getSegItemNode();

        Node newNode = addNodeNewLine(headNode, newNodeName);
        setNodeValue(newNode, value);

    }

    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }

    public static void main(String[] args) throws Exception
    {
        BuildProXML bp = new BuildProXML();
        bp.parseXML();
        Node bn = bp.getRootNode();

        File f = new File("d:/houzw/bkPro.txt");
        System.out.println("aa");
        FileInputStream fis = new FileInputStream(f);
        int len = fis.available();
        System.out.println("len is :" + len);
        byte b[] = new byte[len];
        int h = fis.read(b, 0, len);

        String content = new String(b);
        content = "数据组织生成xml:" + "\r\n" + content;
        char[] ch = content.toCharArray();
        //for()
        System.out.println(ch.length);
        int lenCh = ch.length;
        int hh = 0;

        for (int i = 0; i < ch.length; i++)
        {
            int end = 0;
            int start = 0;
            if (content.charAt(i) == '{')
            {
                segItemProSet btv = new segItemProSet(bn);

                end = i - 2;
                //System.out.println(""+end);
                for (int j = end - 1; j > 0; j--)
                {
                    //System.out.print(content.charAt(j));
                    if (content.charAt(j) == '\n')
                    {
                        start = j + 1;
                        String tempStr = content.substring(start, end);
                        StringTokenizer st = new StringTokenizer(tempStr, " ");

                        int count = 0;
                        while (st.hasMoreElements())
                        {
                            String str = st.nextToken();

                            if (count == 0)
                            {
                                btv.setSegItemProName(str);
                            }
                            if (count == 1)
                            {
                                btv.setSegItemProCode(str);
                            }
                            if (count == 2)
                            {
                                btv.setSegItemProValueDef(str);
                            }
                            if (count == 3)
                            {
                                btv.setSegItemProFlag(str);
                            }
                            if (count == 4)
                            {
                                btv.setSourceItem(str);
                            }

                            count++;
                        }

                        //System.out.println(content.substring(start,end));
                        break;
                    }
                }
                ///////////////
                Node main = btv.getTransVsNode();

                for (int k = i + 3; k < ch.length; k++)
                {
                    int ends = 0;
                    if (content.charAt(k) == '}')
                    {
                        ends = k - 2;
                        String tmpCon = content.substring(i + 3, ends);
                        //System.out.println(tmpCon);
                        StringTokenizer st = new StringTokenizer(tmpCon, "\r\n");

                        while (st.hasMoreElements())
                        {
                            String tmpStr = st.nextToken();

                            transValueSet sis = new transValueSet(main);
                            StringTokenizer sts = new StringTokenizer(tmpStr,
                                    " ");
                            //4个
                            int cc = 0;
                            while (sts.hasMoreElements())
                            {
                                String lastStr = sts.nextToken();
                                //System.out.println("get lastStr : " + lastStr);
                                if (cc == 0)
                                {
                                    sis.setTranValueSour(lastStr);
                                }
                                if (cc == 1)
                                {
                                    sis.setTranValueDetail(lastStr);
                                }
                                if (cc == 2)
                                {
                                    sis.setTranFlag(lastStr);
                                }

                                cc = cc + 1;
                                //System.out.println("hou : " + lastStr);
                            }
                        }
                        ////////////////////adsfa
                        break;
                    }
                }
                //System.out.println();
            }
        }

        FileWriter fw = new FileWriter("d:\\houtestPro.xml");
        fw.write(XPathUsage.toString(bp.getRootNode()));
        fw.close();
        System.out.println("输出到 d:/houtestPro.xml");

        /*
             BuildProXML bp = new BuildProXML();
             bp.parseXML();
             Node bn = bp.getRootNode();
             for(int i = 0 ; i < 3 ; i ++)
             {
          segItemProSet sip = new segItemProSet(bn);
          sip.setSegItemProName("a"+i);
          sip.setSegItemProCode("b" +i);
          sip.setSegItemProValueDef("c"+i);
          sip.setSegItemProFlag("1");
          sip.setSourceItem("d");
          Node tr = sip.getTransVsNode();
          for(int j = 0 ; j < 3 ; j++)
          {
            transValueSet tvs = new transValueSet(tr);
            tvs.setTranValueSour("i="+i+";j="+j);
            tvs.setTranValueDetail("i="+i+";j="+j);
            tvs.setTranFlag("i="+i+";j="+j);
          }
             }
             System.out.println(XPathUsage.toString(bp.getRootNode()));
         */
    }


}


//////////////////////////////////////////////////////////////////////////////

class segItemProSet
{
    private Node parentNode; //上级
    private Node segNode; //本级
    private Document document;

    public segItemProSet(Node Node) throws Exception
    {
        this.parentNode = Node;
        document = parentNode.getOwnerDocument();
        segNode = addNodeNewLine(parentNode, "segItem");
    }

    public void setSourceItem(String v) throws Exception
    {
        addSetValue("sourceItem", v);
    }

    public void setSegItemProName(String v) throws Exception
    {
        addSetProValue("segItem", "name", v);
    }

    public void setSegItemProCode(String v) throws Exception
    {
        addSetProValue("segItem", "code", v);
    }

    public void setSegItemProValueDef(String v) throws Exception
    {
        addSetProValue("segItem", "value_def", v);
    }

    public void setSegItemProFlag(String v) throws Exception
    {
        addSetProValue("segItem", "flag", v);
    }

    public Node getTransVsNode() throws Exception
    {
        Node res = XPathAPI.selectSingleNode(segNode, "transValues");
        if (res == null)
        {
            //segNode parentNode
            Element e = document.createElement("transValues");
            segNode.appendChild(e);
            res = (Node) e;
        }
        return res;
    }

    public void addSetProValue(String nodeName, String attrName, String value) throws
            Exception
    {
        Node target = XPathAPI.selectSingleNode(segNode, ".");
        // if(target != null)
        // {
        Element e = (Element) target;
        e.setAttribute(attrName, value);
        // }
        //else{
        //  Element e = document.createElement(nodeName);
        //  e.setAttribute(attrName,value);
        //  segNode.appendChild(e);
        //  Node newLine = document.createTextNode("\n");
        //  segNode.appendChild(newLine);

        //}
    }

    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }

    private Node addNodeNewLine(Node start, String name)
    {
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        //start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    private void addSetValue(String newNodeName, String value) throws Exception
    {
        //System.out.println("得到 newNodeName ：" + newNodeName);
        if (XPathAPI.selectSingleNode(segNode, newNodeName) != null)
        {
            //System.out.println("找到节点");
            Node tempNode = XPathAPI.selectSingleNode(segNode, newNodeName);
            setNodeValue(tempNode, value);
        }
        else
        {
            //System.out.println("测试添加新的节点" + newNodeName);
            Node newNode = addNodeNewLine(segNode, newNodeName);
            setNodeValue(newNode, value);
        }
    }

}


//////////////////////////////////////////////////////////////////////////////

class transValueSet
{
    Node transVs;
    Node transV;
    Document document;

    public transValueSet(Node no)
    {

        this.transVs = no;

        document = transVs.getOwnerDocument();

        transV = addNodeNewLine(transVs, "transValue");

    }

    private Node addNodeNewLine(Node start, String name)
    {
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        //start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }

    private void addSetValue(String newNodeName, String value) throws Exception
    {
        //System.out.println("得到 newNodeName ：" + newNodeName);
        if (XPathAPI.selectSingleNode(transVs, newNodeName) != null)
        {
            //System.out.println("找到节点");
            Node tempNode = XPathAPI.selectSingleNode(transVs, newNodeName);
            setNodeValue(tempNode, value);
        }
        else
        {
            //System.out.println("测试添加新的节点" + newNodeName);
            Node newNode = addNodeNewLine(transVs, newNodeName);
            setNodeValue(newNode, value);
        }
    }

    public void setTranValueSour(String v) throws Exception
    {
        addSetProValue("transValue", "value_sour", v);
    }

    public void setTranValueDetail(String v) throws Exception
    {
        addSetProValue("transValue", "value_detail", v);
    }

    public void setTranFlag(String v) throws Exception
    {
        addSetProValue("transValue", "flag", v);
    }

    private void addSetProValue(String NodeName, String attrName, String value) throws
            Exception
    {

        Element tr = (Element) transV;
        tr.setAttribute(attrName, value);
        transVs.appendChild(tr);

    }


}
