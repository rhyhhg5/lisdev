/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

//import java.io.File;
import java.io.StringWriter;

//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Yang Yalin
 * @version 1.0
 */
public class XPathUsage
{
    public static void main(String args[])
    {
//        XPathUsage xu = new XPathUsage();
//        String s = "D:/yang/yang/src/data_src.xml";
//        System.out.println(s);
//        File docFile = new File(s);
//        Document doc = null;
//        try
//        {
//            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//            DocumentBuilder db = dbf.newDocumentBuilder();
//            doc = db.parse(s);
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            System.out.print("Problem parsing the file [" + args[1] + "]\n");
//        }
//        Element root = doc.getDocumentElement();
//        try
//        {
//            System.out.println("find value //[@att]: " +
//                               findValue(root, "//qewr") + "/////////////");
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
    }

    /**
     * Returns the contents of all immediate child text nodes, can strip whitespace
     * <p>
     * Takes a node as input and merges all its immediate text nodes into a
     * string.  If the strip whitespace flag is set, whitespace at the beggining
     * and end of each merged text node will be removed
     * @param node Node
     * @return String
     */
    public static String getTextContents(Node node)
    {
        NodeList childNodes;
        StringBuffer contents = new StringBuffer();

        //yang 2002-11-21 added to avoid NullPointerException
        if (node == null)
        {
            return null;
        }

        childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++)
        {
            if (childNodes.item(i).getNodeType() == Node.TEXT_NODE)
            {
                contents.append(childNodes.item(i).getNodeValue());
            }
        }
        return contents.toString();
    }

    /**
     * Returns the text contents of the first node mathcing an XPath expression
     * <p>
     * Takes a context node and an xpath expression and finds a matching
     * node. The text contents of this node are returned as a string
     * @param node Node
     * @param xql String
     * @return String
     * @throws Exception
     */
    public static String findValue(Node node, String xql) throws Exception
    {

        if ((xql == null) || (xql.length() == 0))
        {
            throw new Exception("findValue called with empty xql statement");
        }

        if (node == null)
        {
            throw new Exception("findValue called with null node");
        }
        return getTextContents(XPathAPI.selectSingleNode(node, xql));
    }

    /**
     * finds a node based on an XPath expression and sets the text contents of it
     * <p>
     * Takes a context node and an XPath expression.  The matching node gets a
     * text node appending containing the contents of the value string.  The
     * node matching the XPath expression is returned
     * @param startNode Node
     * @param value String
     * @param xql String
     * @return Node
     * @throws Exception
     */
    public static Node setValue(Node startNode, String value, String xql) throws
            Exception
    {
        Node targetNode = XPathAPI.selectSingleNode(startNode, xql);

        NodeList children = targetNode.getChildNodes();
        int index = 0;
        int length = children.getLength();

        // Remove all of the current contents
        for (index = 0; index < length; index++)
        {
            targetNode.removeChild(children.item(index));
        }

        // Add in the new value
        Document doc = startNode.getOwnerDocument();
        targetNode.appendChild(doc.createTextNode(value));

        return targetNode;
    }

    /**
     * append a node with the given name at the specified location in the document
     * <p>
     * Takes a context node, the name of the new node, and an XPath expression.
     * The new node is appended to the document at the point specified by the
     * context node and the XPath statement
     * @param startNode Node
     * @param name String
     * @param xql String
     * @return Node
     * @throws Exception
     */
    public static Node appendNode(Node startNode, String name, String xql) throws
            Exception
    {
        Node targetNode = XPathAPI.selectSingleNode(startNode, xql);
        Document doc = startNode.getOwnerDocument();

        Element newElement = doc.createElement(name);
        targetNode.appendChild((Node) newElement);
        return ((Node) newElement);
    }

    /**
     * Converts an Element Node or Document into its text representation
     * <p>
     * Converts either the SubTree designated by an Element node or an
     * entire tree, specified by a Document object into an XML Text
     * representation.
     * @param node Node
     * @return String
     * @throws Exception
     */
    public static String toString(Node node) throws Exception
    {
        StringWriter writer = new StringWriter();
        XMLSerializer serial = new XMLSerializer();
        serial.setOutputCharStream(writer);
        OutputFormat format = new OutputFormat();

        format.setPreserveSpace(true);
        format.setEncoding("GB2312");
        serial.setOutputFormat(format);
        if (node.getNodeType() == Node.DOCUMENT_NODE)
        {
            serial.serialize((Document) node);
        }
        else
        {
            serial.serialize((Element) node);
        }
        return writer.toString();
    }
}
