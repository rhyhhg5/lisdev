/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import java.io.*;
import java.text.*;
import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import org.apache.log4j.*;
import org.jdom.input.*;
import org.jdom.output.*;
import org.w3c.dom.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft Inc.</p>
 * @author Yang Yalin
 * @version 1.0
 */

public class UFFecade
{
    BillReader billReader;
    ReqParamWriter rpw;
    Map vouchers = new Hashtable();
    private String iter;
    private Date m_dateLast = new Date();
    private String FileName = "";
    private int entrycount = 0;
    private CErrors mErrors = new CErrors();
    private String transportmessage ="";
    public int filecount;
    public int intFilenum;

    /**
     * 类初始化
     */
    public UFFecade()
    {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                           "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.parsers.SAXParserFactory",
                           "org.apache.xerces.jaxp.SAXParserFactoryImpl");
        init();
    }


    /**
     * 类初始化
     * @param in InputStream
     */
    public UFFecade(InputStream in)
    {
        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                           "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
        System.setProperty("javax.xml.parsers.SAXParserFactory",
                           "org.apache.xerces.jaxp.SAXParserFactoryImpl");
        init(in);
    }


    /**
     * 初始化xml流信息
     */
    private void init()
    {
        billReader = new BillReader();
        rpw = new ReqParamWriter();
    }


    /**
     * 初始化xml流信息
     * @param in InputStream
     */
    private void init(InputStream in)
    {
        billReader = new BillReader(in);
        rpw = new ReqParamWriter();
    }

    public int getentrycount ()
    {
         return entrycount;
    }
    public int setFilecount(int intFilecount)
    {
        filecount = intFilecount;
        return filecount;
    }
    public int setFilenum(int intSetfilenum)
    {
        intFilenum = intSetfilenum;
        return intFilenum;
    }
    /**
     * 解析xml流信息
     * @throws Exception
     */
    public void read() throws Exception
    {
        billReader.parseXML();
    }


    /**
     * 处理xml流信息
     * @param xml2path String
     * @throws Exception
     */
    public void convert(String xml2path) throws Exception
    {
        //构建日志对象
//        Logger mylog = Logger.getLogger("com.sinosoft.lis.xpath");
   try {


       ReqParamWriter rpwHeader = new ReqParamWriter(); //建立目标文件xml
       rpwHeader.parseXML();
       //构建新的xml头部信息
       newHeader(rpwHeader);

       // 读取sour.xml文件
       SourReader sourReader = new SourReader();
       sourReader.load(xml2path + "sour.xml");

       // 读取property.xml文件
       ProReader proReader = new ProReader();
       proReader.load(xml2path + "property.xml");

       //根据需求拆分xml流信息，放入Map对象
       Map unit2Entry = billReader.getUnitEntries();

       Iterator iter = unit2Entry.keySet().iterator();
//        int k = 0;
       while (iter.hasNext()) {
           Object province = iter.next(); //一个省
           Vector vecProEn = (Vector) unit2Entry.get(province); //读入源文件中的所有<entry>
           entrycount = entrycount + vecProEn.size();

           // 一个省使用一个ReqParamWriter对象
//            LDCodeDB tLDCodeDB = new LDCodeDB();
//            tLDCodeDB.setCodeType("finheader");
//            tLDCodeDB.setCode(province.toString());
//            if (!tLDCodeDB.getInfo())
//            {
//                mylog.error("LDCode表中以下公司XML文件头描述:" + province.toString());
//                continue;
//            }
           ReqParamWriter aRPW = rpwHeader.rpwClone();
//            Node M = aRPW.getRootNode();
//            NamedNodeMap NM = M.getAttributes();
//            Node RN = NM.getNamedItem("receiver");
//            Node SN = NM.getNamedItem("sender");
           //从LDCode中获取发送者和接收者的编码信息，替换掉ufinterface标签中的对应元素数据
//            SN.setNodeValue(tLDCodeDB.getCodeName());
//            RN.setNodeValue(tLDCodeDB.getCodeAlias());

           String strLimit = PubFun.getNoLimit(province.toString());
           //构建一个凭证号码
           String tvoucherid = PubFun1.CreateMaxNo("VOUCHERIDNO", strLimit);
           Node nn = aRPW.getVoucherNode();
           NamedNodeMap NM1 = nn.getAttributes();
           Node IN = NM1.getNamedItem("id");
           //将凭证号码写入到voucher标签下的id元素中
           IN.setNodeValue(tvoucherid);
           int tCount = vecProEn.size();

           for (int i = 0; i < tCount; i++) {
//                System.out.println(i);
               String way = "";
               BillEntry billEntry = (BillEntry) vecProEn.get(i); //读如原文件的一个<entry>
               RPEntry rpEntry = new RPEntry(aRPW.getVoucherBodyNode()); //建立目标文件的一个<entry>
               if (i == 15)
                   System.out.println("err");
               //增加目标文件的一个<entry>的数据EntryId
               if (filecount == 0)
                   rpEntry.setEntryId(String.valueOf(i + 1));
               else
                   rpEntry.setEntryId(String.valueOf(i + 1 +
                           (filecount - 1) * 400));
               //读出原文件的一个<entry>中的BillType的值
               String bill_type = billEntry.getBillType();
//                String riskInfo = billEntry.getRiskInfo();
               String mainItemCode = null;
               Node nodeBillToVoucher = sourReader.findByBillType(
                       bill_type);
               if (nodeBillToVoucher == null) {
                   System.out.println("Can't get needed node");
//                    mylog.error("描述文件中无对应的科目代码:" + bill_type);
                   continue;
               }
               billToVoucher btv = new billToVoucher(nodeBillToVoucher);
               mainItemCode = btv.getMainItemCode();
               way = btv.getWay();
               NodeList nls = btv.getAllSegItems();
               String code = "";
               int lalen = nls.getLength();

               // Deal with segitems belong to this billToVoucher node
               for (int b = 0; b < lalen; b++) {
                   segItem segitem = new segItem(nls.item(b));
                   if (segitem.getSegItemFlag().equals("0")) {
                       continue;
                   }
                   Node nodeSegItemPro = proReader.findBySegCode(segitem.
                           getSegItemCode());
                   if (nodeSegItemPro == null) {
                       System.out.println("Can't find SegItemPro code is " +
                                          segitem.getSegItemCode());
//                        mylog.error("无法确定对应的明细会计科目代码:" + segitem.getSegItemCode());
                   }
                   segItemPro sip = new segItemPro(nodeSegItemPro);
                   String strSourceItem = sip.getSegItemProSourceItem();
                   String strSourceValue = "";
                   if (strSourceItem.equals("risk_info")) {
                       strSourceValue = billEntry.getRiskInfo();

                   } else if (strSourceItem.equals("risk_sort")) {
                       strSourceValue = billEntry.getRiskSort();
                   } else if (strSourceItem.equals("bankkind")) {
                       Node tnodel = billEntry.getAuxAccounting();
                       auxAccount tauxAccountl = new auxAccount(tnodel);
                       NodeList nlnll = tauxAccountl.items;
                       int Auxlenl = nlnll.getLength();
                       //循环数据源中的辅助核算项
                       for (int c = 0; c < Auxlenl; c++) {
                           Item it = new Item(nlnll.item(c));
                           String tItemName = it.getProName();
                           if (tItemName.equals("银行分类")) {
                               strSourceValue = it.getValue(nlnll.item(c));
                               break;
                           }
                       }
                   } else if (strSourceItem.equals("趸期交")) {
                       Node tnode2 = billEntry.getAuxAccounting();
                       auxAccount tauxAccount2 = new auxAccount(tnode2);
                       NodeList nlnl2 = tauxAccount2.items;
                       int Auxlen2 = nlnl2.getLength();
                       //循环数据源中的辅助核算项
                       for (int cc = 0; cc < Auxlen2; cc++) {
                           Item it = new Item(nlnl2.item(cc));
                           String tItemName = it.getProName();
                           if (tItemName.equals("趸期交")) {
                               strSourceValue = it.getValue(nlnl2.item(cc));
                               break;
                           }
                       }
                   } else if (strSourceItem.equals("首续期")) {
                       Node tnode3 = billEntry.getAuxAccounting();
                       auxAccount tauxAccount3 = new auxAccount(tnode3);
                       NodeList nlnl3 = tauxAccount3.items;
                       int Auxlen3 = nlnl3.getLength();
                       //循环数据源中的辅助核算项
                       for (int h = 0; h < Auxlen3; h++) {
                           Item it = new Item(nlnl3.item(h));
                           String tItemName = it.getProName();
                           if (tItemName.equals("首续期")) {
                               strSourceValue = it.getValue(nlnl3.item(h));
                               break;
                           }
                       }
                   } else {
                       System.out.print("Unsupported type");
//                        mylog.error("Unsupported sourceitem type" +
//                                    strSourceItem);
                   }
                   NodeList nodelist = sip.getAllTransValues();
                   boolean bFound = false;
                   for (int nIndex = 0; nIndex < nodelist.getLength();
                                     nIndex++) {
                       transValue tv = new transValue(nodelist.item(nIndex));
                       if (tv.getTransValueSource().equals(strSourceValue)) {
                           code = tv.getTransValueDetail();
                           bFound = true;
                           break;
                       }
                   }
                   if (bFound) {
                       break;
                   }
               }
               //辅助核算项描述信息
               NodeList als = btv.getAllAuxiliary();
               int lentest = als.getLength();
               Node tnode = billEntry.getAuxAccounting();
               auxAccount tauxAccount = new auxAccount(tnode);
               NodeList nlnl = tauxAccount.items;
               int Auxlen = nlnl.getLength();
               String tFlag = "0";
               //循环数据源中的辅助核算项
               for (int b = 0; b < Auxlen; b++) {
                   Item it = new Item(nlnl.item(b));
                   String tItemName = it.getProName();
                   tFlag = "0";
                   for (int bb = 0; bb < lentest; bb++) {
                       Node nodes1 = als.item(bb);
                       segAuxiliary sa = new segAuxiliary(nodes1);
                       if (!sa.getSegAuxiliaryFlag().equals("0")) {
                           if (sa.getSegAuxiliaryName().trim().equals(
                                   tItemName.trim())) {
                               tFlag = "1";
                               break;
                           }
                       }
                   }
                   if (tFlag.equals("0")) {
                       tnode.removeChild(it.itemRoot);
                   }
               }
               String tComCode = mainItemCode + code;
               rpEntry.setAccountCode(tComCode);

               //重这里开始主要处理Entry下的元素信息
               String abstra = billEntry.getEntryChildValue("abstract");
               //用友摘要30字的限制
               if (abstra.getBytes().length > 60) {
                   byte[] tb = abstra.getBytes();
                   abstra = abstra.substring(0, 30);
               }
               rpEntry.addSetEntryValue("abstract", abstra);
               rpEntry.addSetEntryValue("settlement",
                                        billEntry.
                                        getEntryChildValue("settlement"));
//                String accountingperiod = billEntry.getaccountingperiod();
//                rpEntry.setaccountingperiod(accountingperiod);
               rpEntry.addSetEntryValue("account_period",
                                        billEntry.
                                        getEntryChildValue(
                       "accountingperiod"));
//                String transationdate = billEntry.gettransationdate();
//                rpEntry.settransationdate(transationdate);
               rpEntry.addSetEntryValue("transaction_date",
                                        billEntry.
                                        getEntryChildValue(
                       "transationdate"));
//                rpEntry.setDocumentId(billEntry.getCheckNo());
               rpEntry.addSetEntryValue("document_id",
                                        billEntry.
                                        getEntryChildValue("check_no"));
//                rpEntry.setDocumentDate(billEntry.getBillDate());

               rpEntry.addSetEntryValue("check_no",
                                        billEntry.
                                        getEntryChildValue("check_no"));

               rpEntry.addSetEntryValue("document_date",
                                        billEntry.
                                        getEntryChildValue("bill_date"));
//                String curr = billEntry.getcurrency();
//                rpEntry.setCurrency(curr);
               rpEntry.addSetEntryValue("currency",
                                        billEntry.
                                        getEntryChildValue("currency"));
//                String excrate1 = billEntry.getExcrate1();
//                rpEntry.setExcrate1(excrate1);
               rpEntry.addSetEntryValue("exchange_rate1",
                                        billEntry.
                                        getEntryChildValue("excrate1"));
//                String excrate2 = billEntry.getExcrate2();
//                rpEntry.setExcrate2(excrate2);
               rpEntry.addSetEntryValue("exchange_rate2",
                                        billEntry.
                                        getEntryChildValue("excrate2"));
               String money = billEntry.getEntryChildValue("money");
               String tmoney = new DecimalFormat("0.000").format(new
                       Double(
                               money));
               String tmoneyz = tmoney.substring(0, tmoney.indexOf("."));
               String tmoneyx = tmoney.substring(tmoney.indexOf(".") + 1,
                       tmoney.length());
               tmoney = "";
               for (int mz = 0; mz < 15 - tmoneyz.length(); mz++) {
                   tmoney = tmoney + "0";
               }
               tmoney = tmoney + tmoneyz + tmoneyx;

               if (way.equals("借")) {
                   rpEntry.setdcmarker("D");
                   rpEntry.setDebitQuantity("0");
//                    rpEntry.setDebitAmount(money);
                   rpEntry.addSetEntryValue("primary_debit_amount", money);
                   rpEntry.addSetEntryValue("debit_format", tmoney);
//                    rpEntry.setLocalDebitCurrency(money);
                   rpEntry.addSetEntryValue("natural_debit_currency",
                                            money);
                   rpEntry.setFracDebitAmount("0");
                   rpEntry.setCreditQuantity("0");
                   rpEntry.setCreditAmount("0");
                   rpEntry.addSetEntryValue("credit_format", "0");
                   rpEntry.setLocalCreditCurrency("0");
                   rpEntry.setFracCreditAmount("0");
               } else {
                   rpEntry.setdcmarker("C");
                   rpEntry.setDebitQuantity("0");
                   rpEntry.setDebitAmount("0");
                   rpEntry.addSetEntryValue("debit_format", "0");
                   rpEntry.setLocalDebitCurrency("0");
                   rpEntry.setFracDebitAmount("0");
                   rpEntry.setCreditQuantity("0");
//                    rpEntry.setCreditAmount(money);
                   rpEntry.addSetEntryValue("primary_credit_amount", money);
                   rpEntry.addSetEntryValue("credit_format", tmoney);
//                    rpEntry.setLocalCreditCurrency(money);
                   rpEntry.addSetEntryValue("natural_credit_currency",
                                            money);
                   rpEntry.setFracCreditAmount("0");

               }
//
//                  rpEntry.setaccbooktype(billEntry.getaccbooktype());

               rpEntry.addSetEntryValue("accbooktype",
                                        billEntry.
                                        getEntryChildValue("accbooktype"));
//                rpEntry.setaccbookcode(billEntry.getaccbookcode());
               rpEntry.addSetEntryValue("accbookcode",
                                        billEntry.
                                        getEntryChildValue("accbookcode"));
//                rpEntry.setcentercode(billEntry.getcentercode());
               rpEntry.addSetEntryValue("centercode",
                                        billEntry.
                                        getEntryChildValue("centercode"));
//                rpEntry.setbranchcode(billEntry.getbranchcode());
               rpEntry.addSetEntryValue("branchcode",
                                        billEntry.
                                        getEntryChildValue("branchcode"));
//                rpEntry.setPolicynoother(billEntry.getPolicynoother());

//                rpEntry.addSetEntryValue("InsuCode",
//                                         billEntry.
//                                         getEntryChildValue("InsuCode"));
//
//                rpEntry.addSetEntryValue("InsuRevPayWay",
//                                               billEntry.
//                                               getEntryChildValue("InsuRevPayWay"));
//
//                rpEntry.addSetEntryValue("InsuRevPayType",
//                                                     billEntry.
//                                                     getEntryChildValue("InsuRevPayType"));
//                rpEntry.addSetEntryValue("InsuFlag",
//                                             billEntry.
//                                             getEntryChildValue("InsuFlag"));





               rpEntry.addSetEntryValue("receiptno",
                                        billEntry.
                                        getEntryChildValue("receiptno"));
               rpEntry.addSetEntryValue("policy_no",
                                        billEntry.
                                        getEntryChildValue("policy_no"));
               rpEntry.addSetEntryValue("Policynoother",
                                        billEntry.
                                        getEntryChildValue("Policynoother"));

               rpEntry.addSetEntryValue("policytype1",
                                        billEntry.
                                        getEntryChildValue("policytype1"));

               rpEntry.addSetEntryValue("policyno1",
                                        billEntry.
                                        getEntryChildValue("policyno1"));
//                rpEntry.setPolicystart(billEntry.getPolicystart());
               rpEntry.addSetEntryValue("Policystart",
                                        billEntry.
                                        getEntryChildValue("Policystart"));
//                rpEntry.setPolicyend(billEntry.getPolicyend());
               rpEntry.addSetEntryValue("Policyend",
                                        billEntry.
                                        getEntryChildValue("Policyend"));
//                rpEntry.setoperdate(billEntry.getoperdate());
               rpEntry.addSetEntryValue("operdate",
                                        billEntry.
                                        getEntryChildValue("operdate"));
//                rpEntry.setpolicydate(billEntry.getpolicydate());
               rpEntry.addSetEntryValue("policydate",
                                        billEntry.
                                        getEntryChildValue("policydate"));
//                rpEntry.setpolicyresp(billEntry.getpolicyresp());
               rpEntry.addSetEntryValue("policyresp",
                                        billEntry.
                                        getEntryChildValue("policyresp"));
//                rpEntry.setitemdesc(billEntry.getitemdesc());
               rpEntry.addSetEntryValue("itemdesc",
                                        billEntry.
                                        getEntryChildValue("itemdesc"));

//                rpEntry.addSetEntryValue("relareason",
//                                         billEntry.
//                                         getEntryChildValue("relareason"));

//                rpEntry.addSetEntryValue("agreementyear",
//                                         billEntry.
//                                         getEntryChildValue("agreementyear"));

//                rpEntry.addSetEntryValue("avalibletype",
//                                         billEntry.
//                                         getEntryChildValue("avalibletype"));

//                rpEntry.addSetEntryValue("avalibleterm",
//                                         billEntry.
//                                         getEntryChildValue("avalibleterm"));

               rpEntry.addSetEntryValue("sellchannel",
                                        billEntry.
                                        getEntryChildValue("sellchannel"));

//                rpEntry.addSetEntryValue("takeway",
//                                         billEntry.getEntryChildValue("takeway"));
//
               rpEntry.addSetEntryValue("accountno",
                                        billEntry.
                                        getEntryChildValue("accountno"));
               rpEntry.addSetEntryValue("bankcode",
                                        billEntry.
                                        getEntryChildValue("bankcode"));

               rpEntry.addSetEntryValue("F01",
                                        billEntry.getEntryChildValue("F01"));

               rpEntry.addSetEntryValue("F02",
                                        billEntry.getEntryChildValue("F02"));

               rpEntry.addSetEntryValue("F03",
                                        billEntry.getEntryChildValue("F03"));

               rpEntry.addSetEntryValue("F04",
                                        billEntry.getEntryChildValue("F04"));

               rpEntry.addSetEntryValue("F05",
                                        billEntry.getEntryChildValue("F05"));

               rpEntry.addSetEntryValue("F06",
                                        billEntry.getEntryChildValue("F06"));

               rpEntry.addSetEntryValue("F07",
                                        billEntry.getEntryChildValue("F07"));

               rpEntry.addSetEntryValue("F08",
                                        billEntry.getEntryChildValue("F08"));

               rpEntry.addSetEntryValue("F09",
                                        billEntry.getEntryChildValue("F09"));

               rpEntry.addSetEntryValue("F10",
                                        billEntry.getEntryChildValue("F10"));

               rpEntry.addSetEntryValue("F11",
                                        billEntry.getEntryChildValue("F11"));

               rpEntry.addSetEntryValue("F12",
                                        billEntry.getEntryChildValue("F12"));

//                rpEntry.addSetEntryValue("F13",
//                                         billEntry.getEntryChildValue("F13"));
//
               rpEntry.addSetEntryValue("F14",
                                        billEntry.getEntryChildValue("F14"));
//
//                rpEntry.addSetEntryValue("F15",
//                                         billEntry.getEntryChildValue("F15"));
//
//                rpEntry.addSetEntryValue("F16",
//                                         billEntry.getEntryChildValue("F16"));
//
//                rpEntry.addSetEntryValue("F17",
//                                         billEntry.getEntryChildValue("F17"));

               rpEntry.addSetEntryValue("F18",
                                        billEntry.getEntryChildValue("F18"));

//                rpEntry.addSetEntryValue("F19",
//                                         billEntry.getEntryChildValue("F19"));
//
//                rpEntry.addSetEntryValue("F20",
//                                         billEntry.getEntryChildValue("F20"));
//
//                rpEntry.addSetEntryValue("F21",
//                                         billEntry.getEntryChildValue("F21"));

               rpEntry.addSetEntryValue("F22",
                                        billEntry.getEntryChildValue("F22"));

//                rpEntry.addSetEntryValue("F23",
//                                         billEntry.getEntryChildValue("F23"));
//
//                rpEntry.addSetEntryValue("F24",
//                                         billEntry.getEntryChildValue("F24"));
//
//                rpEntry.addSetEntryValue("F25",
//                                         billEntry.getEntryChildValue("F25"));
//
//                rpEntry.addSetEntryValue("F26",
//                                         billEntry.getEntryChildValue("F26"));
//
//                rpEntry.addSetEntryValue("F27",
//                                         billEntry.getEntryChildValue("F27"));
//
//                rpEntry.addSetEntryValue("F28",
//                                         billEntry.getEntryChildValue("F28"));
//
               rpEntry.addSetEntryValue("F29",
                                        billEntry.getEntryChildValue("F29"));

               rpEntry.addSetEntryValue("F30",
                                        billEntry.getEntryChildValue("F30"));

               rpEntry.addSetEntryValue("rawunit",
                                        billEntry.getEntryChildValue(
                       "rawunit"));

               rpEntry.addSetEntryValue("rawoper",
                                        billEntry.getEntryChildValue(
                       "rawoper"));

//                rpEntry.addSetEntryValue("policykey",
//                                         billEntry.
//                                         getEntryChildValue("policykey"));
//
//                rpEntry.addSetEntryValue("temp",
//                                         billEntry.getEntryChildValue("temp"));
//
//                rpEntry.addSetEntryValue("policytype",
//                                         billEntry.
//                                         getEntryChildValue("policytype"));
//                rpEntry.addSetEntryValue("policytakeway",
//                                         billEntry.
//                                         getEntryChildValue("policytakeway"));
//
//                rpEntry.addSetEntryValue("policytaketype",
//                                         billEntry.
//                                         getEntryChildValue("policytaketype"));
//
//                rpEntry.addSetEntryValue("policytakeflag",
//                                         billEntry.
//                                         getEntryChildValue("policytakeflag"));
//


               rpEntry.appendAux(tnode.cloneNode(true));
               rpEntry.setDetail("");
               // 在生成了所有的内容之后，将所有的内容加入到原来的DOM树中。
               rpEntry.attachToNode();
           }
           Node newRoot = aRPW.getRootNode();
           //设置省单位
           XPathUsage.setValue(newRoot, province.toString(), "//company");
           //设置附件数
           XPathUsage.setValue(newRoot, "" + vecProEn.size(),
                               "//attachment");
           vouchers.put(province, aRPW);
       }
   }
 catch (Exception ex) {
       System.out.println(ex.getMessage());
   }
    }


    /**
     * xml流头部信息处理
     * @param reqHeader ReqParamWriter
     * @throws Exception
     */
    public void newHeader(ReqParamWriter reqHeader) throws Exception
    {
        reqHeader.setCompany("001");
        //待重设
        reqHeader.setVoucherType("记"); //增加目标xml文件的<voucher_header>里的数据
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("Fiscal_year");
        if (tLDSysVarDB.getInfo())
        {
            reqHeader.setfiscal_year(tLDSysVarDB.getSysVarValue());
        }
        this.billReader.getBillHeadMemo1();
        String tVoucherDate = this.billReader.getBillVoucherDate(); //year
        //根据财务确定当前凭证的会计月度
        //String tPeriod = tVoucherDate.equals("") ? "" : tVoucherDate.substring(5,7);
        String tPeriod = getPeriod(tVoucherDate);
        String voucher_id = this.billReader.getBillID(); //voucher_id
        String standby = this.billReader.getBillStandBy();
        String lodebatch = this.billReader.getlodebatch();
        reqHeader.setlodebatch(lodebatch);
        reqHeader.setstandby(standby);
        reqHeader.setNo("");
        reqHeader.setPeriod(tPeriod);
        reqHeader.setvoucher_id(voucher_id);
        reqHeader.setYear(tVoucherDate);
        reqHeader.setAttachment("5");
        reqHeader.setattachment_number("9");

        String bussDate = billReader.getBillHeadDate();
        String tString = tVoucherDate;
        this.iter = billReader.getBillID();
//System.out.println("now get the time is :+================ " + tString);
        reqHeader.setDate(tString);

        reqHeader.setEnter("业务系统");
        reqHeader.setCashier("");
        reqHeader.setSignature("Y");
        reqHeader.setChecker("");
        reqHeader.setTallydate(bussDate);
        reqHeader.setManager("");
        String tGL = "GL";

        tLDSysVarDB.setSysVar("InterfaceGL");
        if (tLDSysVarDB.getInfo())
        {
            tGL = tLDSysVarDB.getSysVarValue();
        }
        reqHeader.setSystem(tGL);
        reqHeader.setMemo1("");
        reqHeader.setMemo2("");
        reqHeader.setReserve1("");
        reqHeader.setReserve2("");

    }

    /**
     * 写入后返回xml, 用"||"分隔
     * @param xml2path String
     * @return String
     * @throws Exception
     */
    public void writeReturnXML(String xml2path) throws Exception
    {

        String message = "";
        Iterator iter = vouchers.keySet().iterator();
//        StringBuffer sb = new StringBuffer();
        int i = 0;
//        String dirSys = System.getProperty("user.dir");

        String tPath = xml2path + "toFinXml" + File.separator;
//        String dirLog = xml2path + "toFinLog" + File.separator;
//        dirSys  + File.separator
        //设置xml输出目录，如果不存在则生成
        File tFileXml = new File(tPath);
        if (!tFileXml.exists())
        {
            tFileXml.mkdir();
        }

//        File tFileLog = new File(dirLog);
//        if (!tFileLog.exists())
//        {
//            tFileLog.mkdir();
//        }
        //设置生成xml文件名属性
        String tCt = PubFun.getCurrentTime();
        tCt = tCt.replace(':', '-');
        String tTimeString = PubFun.getCurrentDate() + "-" + tCt;
        String Xmlfilename = "OF" + this.iter + "-" + tTimeString;
//        String Logfilename = "Log" + this.titer + "-" + tTimeString;
//        FileName = Xmlfilename + "-0.xml";
        //循环生成xml文件
        if(entrycount >0)
        {
            while (iter.hasNext())
            {
                System.out.println("B1");
                String tFileName = tPath + Xmlfilename + "-" + i + ".xml";
                //if (filecount > 0)
                    //tFileName = tPath +Xmlfilename + "-" + filecount + ".xml";
                Object province = iter.next();
                ReqParamWriter rpw = (ReqParamWriter) vouchers.get(province);
                //构建一个文件输出流
                FileOutputStream fos = new FileOutputStream(tFileName);
                //设定输出格式

                XMLOutputter outputter = new XMLOutputter("  ", true, "UTF-8");
                System.out.println("B2");
                //将String转换成org.w3c.dom.Document对象
                Document doc = DocUse.parseXMLDocument(XPathUsage.toString(rpw.
                        getRootNode()));
                System.out.println("B2-1");
                //利用DOMBuilder方法将doc对象转换成jdom对象
                DOMBuilder builder = new DOMBuilder();
                System.out.println("B3");
                org.jdom.Document jdomDoc = builder.build(doc);
                //这样输出的时候UTF-8格式文件能够被正常处理
                //生成OF数据文件
                outputter.output(jdomDoc, fos);
                //fos.close();
                BillXmlOutput tBillXmlOutput = new BillXmlOutput();
                if(!tBillXmlOutput.findfile(tPath,Xmlfilename + "-" + i + ".xml"))
                {
                   message = "业务类型" + this.iter + "没有生成出第2张"+ String.valueOf(province) +"凭证";
                   buildError("writeReturnXML",message);
                }
                else
                {

                   System.out.println("B4");
                   HandleXML thx = new HandleXML();
                   thx.setFilecount(filecount);
                   thx.setfilenum(intFilenum);
                   try{
                       //生成PICC数据文件
                           message = thx.BuildXML(xml2path,
                                              Xmlfilename + "-" + i + ".xml");
                           if (!message.equals(""))
                           transportmessage = message.substring(2,message.length()-2);
                   }
                   catch(Exception e)
                   {
                        System.out.print(e.getMessage());
                   }
                   if (!message.equals(""))
                   {
                       if (!message.substring(0, 1).equals("0")) {
                           buildError("writeReturnXML",
                                      "业务类型" + this.iter +
                                      String.valueOf(province) + "凭证" + message);
                       }
                   }
                }
                i++;
            }
        }
        if(mErrors.getErrorCount()>0)
        {
            String logPath = xml2path + "PiccFinLog" + File.separator;
            String Logfilename = "Log" + this.iter + "-" + tTimeString;
            File FileXmlnew = new File(logPath);
            if (!FileXmlnew.exists())
            {
                FileXmlnew.mkdir();
            }

            FileWriter fw1 = new FileWriter(logPath + Logfilename + ".log");
            fw1.write(mErrors.getErrContent());
            fw1.close();

        }
    }


    /**
     * 拆分日期
     * @param tBussDate String
     * @return String
     */
    public CErrors geterrors()
    {
        return  mErrors;
    }
    public String gettransportmessage()
    {
        PubFun ss = new PubFun();
        return  ss.changForJavaScript(transportmessage);
    }


    private String getPeriod(String tBussDate)
    {
        String tPeriod = "";
        if (tBussDate != null)
        {
            if (tBussDate.substring(5, 7).equals("12"))
            {
                return "12";
            }
            String tDate = tBussDate.substring(8, 10);
            if (Integer.parseInt(tDate) >= 26)
            {
                tPeriod = String.valueOf(Integer.parseInt(tBussDate.substring(5,
                        7)) + 1);
            }
            else
            {
                tPeriod = String.valueOf(Integer.parseInt(tBussDate.substring(5,
                        7)));
            }
            if (tPeriod.length() == 1)
            {
                tPeriod = "0" + tPeriod;
            }
        }
        return tPeriod;
    }


    /**
     * 返回两数的乘积
     * @param in1 String
     * @param in2 String
     * @return String
     */
    public static String mul(String in1, String in2)
    {
        double x = Double.parseDouble(in1);
        double y = Double.parseDouble(in2);
        double r = x * y;
        return "" + r;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UFFecade";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 测试函数
     * @param args String[]
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        //for (int i=1;i<=10;i++)
       // {
//            FileInputStream fis = new FileInputStream(
//                    "D:\\bill6-"+PubFun.getCurrentDate()+"-"+"0.xml");

//           FileInputStream fis = new FileInputStream(
//                   "D:\\bill4-2007-04-30-0.xml");
//           UFFecade uff = new UFFecade(fis);
//           uff.read();
////            uff.setFilecount(1);
//           uff.convert("D:\\");
//           uff.writeReturnXML("D:\\");

      try {
          FileInputStream fis = new FileInputStream(
                  "D:\\bill4-2007-04-30-0.xml");
          UFFecade uff = new UFFecade(fis);
          uff.read();
//            uff.setFilecount(1);
          uff.convert("D:\\");
          uff.writeReturnXML("D:\\");

      } catch (Exception ex) {
          System.out.println(ex.getMessage());
      }
       // }
    }

}
