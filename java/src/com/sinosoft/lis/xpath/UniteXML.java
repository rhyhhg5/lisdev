package com.sinosoft.lis.xpath;

import java.io. *; //Java基础包，包含各种IO操作
import java.util. *; //Java基础包，包含各种标准数据结构操作
import javax.xml.parsers. *; //XML解析器接口
import org.w3c.dom. *; //XML的DOM实现
import org.apache.crimson.tree.XmlDocument;//写XML文件要用到
import javax.xml.transform. *;
import javax.xml.transform.dom. *;
import javax.xml.transform.stream. *;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class UniteXML {
    public UniteXML() {
    }
    public boolean isMerging(String mainFileName, String subFilename) throws Exception
    {
      boolean isOver = false;
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance() ;
      DocumentBuilder db = null;

      try
      {
        db = dbf.newDocumentBuilder ();
      }
      catch (ParserConfigurationException pce)
      {
        System.err.println(pce); //出现异常时，输出异常信息
      }
       Document doc_main = null,doc_vice= null;
 //获取两个XML文件的Document。
      try {
        doc_main = db.parse (mainFileName);
        doc_vice = db.parse (subFilename);
      }
      catch (DOMException dom) {
         System.err.println (dom.getMessage ());
      }
      catch (Exception ioe) {
         System.err.println (ioe);
      }
 //获取两个文件的根元素。
      Element root_main = doc_main.getDocumentElement ();
      Element root_vice = doc_vice.getDocumentElement ();
 //下面添加被合并文件根节点下的每个元素
 /*主文件子节点bill_body*/
      NodeList messageMain = root_main.getChildNodes();
      Element root_main1 = (Element)messageMain.item(1);
      NodeList messageMain1 = root_main1.getChildNodes();
      Element root_main2 = (Element)messageMain1.item(3);

 /*被合并文件子节点bill_body*/
      NodeList messageItems = root_vice.getChildNodes();
      Element root_vice1 = (Element)messageItems.item(1);
      NodeList messageItems1 = root_vice1.getChildNodes();
      Element root_vice2 = (Element)messageItems1.item(3);
      NodeList messageItems2 = root_vice2.getChildNodes();
      int item_number2 = messageItems2.getLength ();
      int item_number1 = messageItems1.getLength ();
      int item_number = messageItems.getLength ();
 //如果去掉根节点下的第一个元素，比如<所属管理系统>，那么i从3开始。否则i从1开始。
      for (int i=1; i < item_number2; i=i+2 ) {
 //调用dupliate()，依次复制被合并XML文档中根节点下的元素。
        Element messageItem = (Element) messageItems2.item (i);
        isOver = dupliate(doc_main, root_main2, messageItem);
      }
 //调用 write To()，将合并得到的Document写入目标XML文档。
    boolean isWritten = writeTo(doc_main, mainFileName);
    return  isOver && isWritten;
}
        private boolean dupliate (Document doc_dup, Element father, Element son) throws Exception
        {
            boolean isdone = false;
            String son_name = son.getNodeName();
            Element subITEM = doc_dup.createElement (son_name);
 //复制节点的属性
            if (son.hasAttributes())
            {
              NamedNodeMap attributes = son.getAttributes();
              for (int i=0; i < attributes.getLength () ; i ++)
              {
                   String attribute_name = attributes.item(i).getNodeName();
                   String attribute_value= attributes.item(i).getNodeValue();
                   subITEM.setAttribute (attribute_name, attribute_value);
              }
             }
              father.appendChild (subITEM);
 //复制节点的值
           Text value_son = (Text)son.getFirstChild ();
           String nodevalue_root = "";
           if (value_son != null && value_son.getLength () >0) nodevalue_root = (String) value_son.getNodeValue ();
           Text valuenode_root = null;
           if ((nodevalue_root != null)&&(nodevalue_root.length () >0)) valuenode_root = doc_dup.createTextNode (nodevalue_root);
           if (valuenode_root != null && valuenode_root.getLength () >0) subITEM.appendChild (valuenode_root);
 //复制子结点
           NodeList sub_messageItems = son.getChildNodes ();
           int sub_item_number = sub_messageItems.getLength();
           if (sub_item_number < 2)
           {
           //如果没有子节点,则返回
           isdone = true;
           }
          else
          {
            for (int j = 1; j < sub_item_number; j=j+2)
            {
 //如果有子节点,则递归调用本方法
              Element sub_messageItem = (Element) sub_messageItems.item (j);
              isdone = dupliate (doc_dup, subITEM, sub_messageItem);
            }
          }
         return isdone;
        }
    private boolean writeTo (Document doc, String fileName) throws Exception
    {
       boolean isOver = false;
       DOMSource doms = new DOMSource (doc);
       File f = new File (fileName);
       StreamResult sr = new StreamResult (f);
       try
      {
          TransformerFactory tf=TransformerFactory.newInstance ();
          Transformer t=tf.newTransformer ();
          Properties properties = t.getOutputProperties ();
          properties.setProperty (OutputKeys.ENCODING,"GB2312");
          t.setOutputProperties (properties);
          t.transform (doms, sr);
          isOver = true;
      }
      catch (TransformerConfigurationException tce)
     {
          tce.printStackTrace ();
     }
      catch (TransformerException te)
     {
          te.printStackTrace ();
      }
       return isOver;
     }
      public static void main(String[] args) throws Exception
     {

      //if ( isdone.isMerging("D:/a.xml","D:/b.xml")) System.out.println ("XML files have been merged.");
      //else System.out.println ("XML files have NOTbeen merged.");
      UniteXML isdone = new UniteXML();
      for(int i=2;i<=10;i++)
      {
           if ( isdone.isMerging("D:/PiccFinxml/PICC-3-2005-10-111.xml","D:/PiccFinxml/PICC-3-2005-10-11"+ i +".xml")) System.out.println ("XML files have been merged.");
           else System.out.println ("XML files have NOTbeen merged.");
      }
    }


}
