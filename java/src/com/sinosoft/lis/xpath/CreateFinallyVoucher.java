package com.sinosoft.lis.xpath;

import java.io.FileInputStream;

/**
 * <p>Title: </p>
 * 转换类
 * <p>Description: </p>
 * 通过此类实行各种最终凭证文件
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SINOSOFT</p>
 * @author 朱向峰
 * @version 1.0
 */
public class CreateFinallyVoucher
{
    public CreateFinallyVoucher()
    {
    }

    /**
     * 根据传入的xml文件名生成最终凭证
     * @param cFileName String
     * @param cOutputFolder String
     */
    public static void FinallyBuilder(String cFileName, String cOutputFolder) throws
            Exception
    {
        //生成第三张xml文件的方式
//        FileInputStream tFis = new FileInputStream(cFileName);
//        XMLRebirth tXMLRebirth = new XMLRebirth(tFis);
//        tXMLRebirth.read();
//        tXMLRebirth.convert();
//        tXMLRebirth.writeReturnXML(cOutputFolder);

        //生成txt文件方式
//        WriteToFile WriteToFile = new WriteToFile();
//        WriteToFile.xmlTransform(cFileName,cOutputFolder);
    }

    public static void main(String[] args)
    {
//        CreateFinallyVoucher createfinallyvoucher = new CreateFinallyVoucher();
    }
}
