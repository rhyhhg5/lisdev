/**
 * Copyright ? 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.xpath;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.db.LDSysVarDB;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;


/**
 * BillXmlOutput
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class BillXmlOutput
{

    public static void main(String[] args) throws Exception
    {   String  ss;
        String date ="2005-06-01";
        String danzhenghao="1";
        String baselocality = "D:/";
        BillXmlOutput tBillXmlOutput = new BillXmlOutput();
        ss = tBillXmlOutput.searchfile(date,danzhenghao,baselocality);
        tBillXmlOutput.submitData(baselocality+ss);

    }

   public  String  searchfile(String date, String danzhenghao,String baselocality)
   {   System.out.print("serachbegin");
       String filename ="";
       String filelocality = baselocality + "PiccFinxml/";
       String nowfilename ="";
       String returnfilename ="";
       String iter ="";
       String date1 = "00-00-00";
       String time1 = "00-00-00";
       String date2 = "";
       String time2 ="";
       FileViewer  tFileViewer = new FileViewer(filelocality);
       tFileViewer.refreshList();
       while (tFileViewer.nextFile())
       {   nowfilename = tFileViewer.getFileName();
           iter = nowfilename.substring(5,6);
           if(iter.equals(danzhenghao))
           {
               date2 = nowfilename.substring(7,17);
               if(date2.equals(date))
               {
                  time2 = nowfilename.substring(18,26);
                  if(time2.compareTo(time1)>0)
                  {
                      time1= time2;
                      returnfilename = nowfilename;
                  }

               }
           }


           System.out.print(nowfilename+"\n");
           System.out.print(iter+"\n");
           System.out.print(date2+"\n");
           System.out.print(time2+"\n");
       }
     return  returnfilename;
   }

   public  boolean  findfile(String path, String filename)
   {
       System.out.print("serachbegin");
       String nowfilename = "";
       FileViewer  tFileViewer = new FileViewer(path);
       tFileViewer.refreshList();
       while (tFileViewer.nextFile())
       {
           nowfilename = tFileViewer.getFileName();
           if(nowfilename.equals(filename)) return true;
       }
     System.out.print("serachend");
     return  false;
   }




   public  String  submitData(String filelocality) throws Exception
   {
       String s = "";
       LDSysVarSchema tLDSysVarSchema = new LDSysVarSchema();
       LDSysVarDB tLDSysVarDB = new LDSysVarDB();
       tLDSysVarDB.setSysVar("transportaddress");
       if(!tLDSysVarDB.getInfo())
       {
         s = "查询提交财务数据处理地址出错";
         return s;
       }
       tLDSysVarSchema.setSchema(tLDSysVarDB.getSchema());
       String url = tLDSysVarSchema.getSysVarValue();
       FileInputStream fi = new FileInputStream(filelocality);
       URL jspUrl = new URL(url);
       URLConnection uc = jspUrl.openConnection();
       uc.setDoOutput(true);
       OutputStream os = uc.getOutputStream(); //得到客户端的输出流对象
       System.out.println("URL===" + uc.getURL());
       String ts="";
       int length = 0;
       byte[] buffer = new byte[4096];
       while ((length = fi.read(buffer)) != -1)
       {
           os.write(buffer, 0, length);
       }
       os.flush(); //将输出流提交到服务器端
       os.close(); //关闭输出流对象

       InputStream ir = uc.getInputStream();
       InputStreamReader isr = new InputStreamReader(ir);
       int n = 0;


       while ((n = isr.read()) != -1)
       {
           System.out.print((char) n);
           s += (char) n;
       }
       System.out.println(s);
       System.out.println("os close");
       return  s ;
   }


}
