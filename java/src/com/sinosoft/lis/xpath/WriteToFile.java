/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 财务数据转换到文件模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class WriteToFile
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    //输出文件名
    private String TxtName = "";

    public WriteToFile()
    {
    }

    /**
     * 执行格式转化，将xml文件通过xsl样式设定，改为txt文件
     * xml文件名传入，xsl文件名写死，输出文件名动态
     */
    public boolean xmlTransform(String cPath, String cXmlName)
    {
        try
        {
            System.out.println("Prepare Begin ... !");
            //xml文件路径
            File fSource = new File(cPath + "/toFinXml/" + cXmlName);
            System.out.println("fSource is :" + fSource);
            //xsl文件路进
            File fStyle = new File(cPath + "/toFinXml/xmlstyle.xsl");
            System.out.println("fStyle is :" + fStyle);

            Source source = new StreamSource(fSource);
            //result输出文件路进
            TxtName = AgentPubFun.formatDate(PubFun.getCurrentDate(),
                                             "yyyyMMdd");
            System.out.println(cXmlName.indexOf(".xml"));
            TxtName = cXmlName.substring(0, cXmlName.indexOf(".xml"));

            //生成的txt文件可以用特定名＋日期来定义，默认存放到c根目录下
            Result result = new StreamResult(new FileOutputStream(cPath +
                    "/toFinXml/" + TxtName + ".txt"));
            System.out.println("result is :" + result);
            Source style = new StreamSource(fStyle);
            System.out.println("Prepare End ... !");

            //Create the Transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer(style);

            //Transform the Document
            transformer.transform(source, result);

            System.out.println("Transform Success!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            System.out.println("Error is " + e.toString());
            CError tError = new CError();
            tError.moduleName = "WriteToFile";
            tError.functionName = "SimpleTransform";
            tError.errorMessage = "Xml处理失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        WriteToFile WriteToFile = new WriteToFile();
        WriteToFile.xmlTransform("d:",
                                 "OF1-2005-03-24-16-50-43-0.xml");
    }
}
