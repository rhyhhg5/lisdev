package com.sinosoft.lis.xpath;

import java.io.InputStream;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SourReader
{
    //doc root  根元素
    private static final String XQLROOT = "/billToVouchers";
    //billToVoucher  对应关系
    private static final String XQLBILLTOVOUCHER = XQLROOT + "/billToVoucher";
    //bill_type  业务类型
    private static final String XQLBILLTYPE = XQLBILLTOVOUCHER + "/bill_type";

    //mainItem  财务一级科目
    private static final String XQLMAINITEM = XQLBILLTOVOUCHER + "/mainItem";
    //segItem  名细科目
    public static final String XQLSEGITEM = XQLMAINITEM + "/segItem";

    //auxiliary辅助核算标志
    private static final String XQLAUXILIARY = XQLBILLTOVOUCHER + "/auxiliary";
    //aux辅助核算明细
    public static final String XQLSEGAUX = XQLAUXILIARY + "/segAuxiliary";


    private String filePath = Conf.SOURFILE;
    private Document m_document = null;
    private Node m_nodeRoot = null;

    // 以BillType为键值的Map
    private TreeMap m_tmBillType = new TreeMap();

    public SourReader()
    {
    }

    public SourReader(String filePath)
    {
        this.filePath = filePath;
    }

    public void load(String strFilePath) throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        m_document = db.parse(strFilePath);
        buildTree(); // 构建查找所需的树
    }

    public void load(InputStream ins) throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        m_document = db.parse(ins);
        buildTree(); // 构建查找所需的树
    }

    private void buildTree() throws Exception
    {
        m_nodeRoot = m_document.getDocumentElement();

        String str = "";
        Node node = null;
        NodeList nodelist = XPathAPI.selectNodeList(m_nodeRoot,
                XQLBILLTOVOUCHER);

        m_tmBillType.clear();

        for (int nIndex = 0; nIndex < nodelist.getLength(); nIndex++)
        {
            node = nodelist.item(nIndex);
            str = XPathAPI.eval(node, "bill_type").toString();

            m_tmBillType.put(str, node);
        }
    }

//   public void parseXML() throws Exception
//   {
//     throw new Exception("parseXML has been ");
//     // Parse the file
//     DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//     DocumentBuilder db = dbf.newDocumentBuilder();
//
//     m_document = db.parse(filePath);
//     m_nodeRoot = m_document.getDocumentElement();
//   }

    public NodeList getAllBillToVouchers() throws Exception
    {
        NodeList nl = XPathAPI.selectNodeList(m_nodeRoot, XQLBILLTOVOUCHER);
        return nl;
    }

    public Node findByBillType(String strBillType)
    {
        Node node = (Node) m_tmBillType.get(strBillType);
        return node;
    }

    //main()
    public static void main(String[] args) throws Exception
    {
        System.out.println("======== Start the ParseFile ===========");

        SourReader sourReader1 = new SourReader();

        sourReader1.load("sour.xml");

        NodeList nl = sourReader1.getAllBillToVouchers();
        for (int i = 0; i < nl.getLength(); i++)
        {
            Node node = nl.item(i);
            //System.out.println(Node.ELEMENT_NODE);
            //System.out.println(XPathUsage.findValue(node,"mainItem/segItem/@code"));
            billToVoucher btv = new billToVoucher(node);
            System.out.println(btv.getBillType());
            //Node it = node.getOwnerDocument().getDocumentElement() ;
            NodeList nls = btv.getAllSegItems();

            //int k=XPathAPI.selectNodeList(node,"/mainItem/segItem").getLength();
            //System.out.println(XPathUsage.findValue(node,"/mainItem/@name"));

            for (int j = 0; j < nls.getLength(); j++)
            {
                Node nodes = nls.item(j);
                segItem si = new segItem(nodes);
                System.out.println("   segItemName is :" + si.getSegItemName());
            }
        }
        System.out.println("======== End the ParseFile ===========");
    }

}


//*****************************************************************************

class billToVoucher
{

    Node billToVoucherRoot;

    public billToVoucher(Node billToVoucherRoot)
    {
        this.billToVoucherRoot = billToVoucherRoot;
    }


    private String getBillToVoucherChildValue(String relativeXQL) throws
            Exception
    {
        String result = XPathUsage.findValue(billToVoucherRoot, relativeXQL);
        return (result == null) ? "" : result;
    }

    /**
     * 得到一个对应关系的停用标志
     * @return 停用标志
     */
    public String getBillToVoucherFlag() throws Exception
    {
        return getBillToVoucherChildValue("billToVoucher/@flag");
    }

    /**
     * 得到业务类型
     * @return 业务类型
     */
    public String getBillType() throws Exception
    {
        return getBillToVoucherChildValue("bill_type");
    }

    public String getWay() throws Exception
    {
        return getBillToVoucherChildValue("way");
    }

    /**
     * 得到一级财务科目名称
     * @return 一级财务科目名称
     */
    public String getMainItemName() throws Exception
    {
        return getBillToVoucherChildValue("mainItem/@name");
    }

    /**
     * 得到一级财务科目代码
     * @return 一级财务科目代码
     */
    public String getMainItemCode() throws Exception
    {
        return getBillToVoucherChildValue("mainItem/@code");
    }

    /**
     * 得到一级财务科目的级别
     * @return 一级财务科目的级别
     */
    public String getMainItemDirNum() throws Exception
    {
        return getBillToVoucherChildValue("mainItem/@dir_num");
    }


    /**
     * 得到一级财务科目的停用标志
     * @return 停用标志
     */
    public String getMainItemFlag() throws Exception
    {
        return getBillToVoucherChildValue("mainItem/@flag");
    }

    /**
     * 得到一级财务科目下的名细科目
     * @return 一级财务科目下的名细科目
     */
    public Node getMainItemFollowing() throws Exception
    {
        return XPathAPI.selectSingleNode(billToVoucherRoot, "mainItem");
    }

    /**
     * 得到一级财务科目下的名细科目
     * @return 一级财务科目下的名细科目
     */
    public NodeList getAllSegItems() throws Exception
    {
        //String tempPath = SourReader.
        NodeList nl = XPathAPI.selectNodeList(billToVoucherRoot,
                                              "mainItem/segItem");
        return nl;
    }

    public String getAuxiliaryDir_num() throws Exception
    {
        return getBillToVoucherChildValue("auxiliary/@dir_num");
    }

    public String getAuxiliaryFlag() throws Exception
    {
        return getBillToVoucherChildValue("auxiliary/@flag");
    }

    public Node getAuxiliaryFollowing() throws Exception
    {
        return XPathAPI.selectSingleNode(billToVoucherRoot, "auxiliary");
    }

    /**
     * 得到一级财务科目下的名细科目
     * @return 一级财务科目下的名细科目
     */
    public NodeList getAllAuxiliary() throws Exception
    {
        //String tempPath = SourReader.
        NodeList nl = XPathAPI.selectNodeList(billToVoucherRoot,
                                              "auxiliary/segAuxiliary");
        return nl;
    }

}


//******************************************************************************

class segItem
{
    Node segItemRoot;

    public segItem(Node segItemRoot)
    {
        this.segItemRoot = segItemRoot;
    }

    private String getSegItemChildValue(String relativeXQL) throws Exception
    {
        String result = XPathUsage.findValue(segItemRoot, relativeXQL);
        return (result == null) ? "" : result;
    }

    /**
     * 得到名细科目名称
     * @return 名细科目名称
     */
    public String getSegItemName() throws Exception
    {
        return getSegItemChildValue("@name");
    }

    /**
     * 得到名细科目代码
     * @return 名细科目代码
     */
    public String getSegItemCode() throws Exception
    {
        return getSegItemChildValue("@code");
    }

    /**
     * 得到名细科目所属级次
     * @return 名细科目所属级次
     */
    public String getSegItemDirLevel() throws Exception
    {
        return getSegItemChildValue("@dir_level");
    }

    /**
     * 得到名细科目停用标志
     * @return 名细科目停用标志
     */
    public String getSegItemFlag() throws Exception
    {
        return getSegItemChildValue("@flag");
    }

}


class segAuxiliary
{
    Node segAuxiliaryRoot;

    public segAuxiliary(Node segAuxiliaryRoot)
    {
        this.segAuxiliaryRoot = segAuxiliaryRoot;
    }

    private String getSegAuxiliaryChildValue(String relativeXQL) throws
            Exception
    {
        String result = XPathUsage.findValue(segAuxiliaryRoot, relativeXQL);
        return (result == null) ? "" : result;
    }

    /**
     * 得到名细科目名称
     * @return 名细科目名称
     */
    public String getSegAuxiliaryCode() throws Exception
    {
        return getSegAuxiliaryChildValue("@code");
    }

    /**
     * 得到名细科目所属级次
     * @return 名细科目所属级次
     */
    public String getSegAuxiliaryDirLevel() throws Exception
    {
        return getSegAuxiliaryChildValue("@dir_level");
    }

    /**
     * 得到名细科目停用标志
     * @return 名细科目停用标志
     */
    public String getSegAuxiliaryFlag() throws Exception
    {
        return getSegAuxiliaryChildValue("@flag");
    }

    public String getSegAuxiliaryName() throws Exception
    {
        return getSegAuxiliaryChildValue("@name");
    }


}
