/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class BillReader
{
    //doc root
    private static final String XQLROOT = "/ufinterface";
    //bill
    private static final String XQLBILL = XQLROOT + "/bill";
    //bill_head
    private static final String XQLBILLHEAD = XQLBILL + "/bill_head";

    //bill_body
    private static final String XQLBILLBODY = XQLBILL + "/bill_body";
    //entry
    private static final String XQLENTRY = XQLBILLBODY + "/entry";


    private String filePath = Conf.BILLFILE;
    private Document document;
    private Node root;

    private InputStream inputStream;

    private Document m_docTemp = null;


    public BillReader()
    {

    }

    public BillReader(String filePath)
    {

        this.filePath = filePath;

    }

    public BillReader(InputStream is)
    {
        this.inputStream = is;
    }

    public void parseXML() throws Exception
    {
        // Parse the file
        System.out.println("begin parseXML");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        m_docTemp = db.newDocument();

        System.out.println("end build documentbuilder");
        if (inputStream != null)
        {
            document = db.parse(inputStream);
        }
        else
        {
            document = db.parse(filePath);
        }
        root = document.getDocumentElement();
    }


    /**
     * 得到root下的voucher_header标签中的信息备注
     * @param ChildName String
     * @return String
     * @throws Exception
     */
    public String getHeaderChildValue(String ChildName) throws Exception
    {
        String result = XPathUsage.findValue(root,
                                             XQLBILLHEAD + "/" + ChildName);
        return (result == null) ? "" : result;
    }

    /**
     * 得到单证来源
     * @return 单证来源
     * @throws Exception
     */
    public String getBillHeadSour() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/bill_sour");
        return (result == null) ? "" : result;
    }

    /**
     * 得到业务公司
     * @return 业务公司
     * @throws Exception
     */
    public String getBillHeadCompany() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/company");
        return (result == null) ? "" : result;
    }

    /**
     * 得到业务日期
     * @return 业务日期
     * @throws Exception
     */
    public String getBillHeadDate() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/buss_date");
        return (result == null) ? "" : result;
    }

    /**
     * 得到头信息备注1
     * @return 得到头信息备注1
     * @throws Exception
     */
    public String getBillHeadMemo1() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/memo1");
        return (result == null) ? "" : result;
    }

    /**
     * 得到头信息备注2
     * @return 得到头信息备注2
     * @throws Exception
     */
    public String getBillHeadMemo2() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/memo2");
        return (result == null) ? "" : result;
    }

    public String getBillStandBy() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/standby");
        return (result == null) ? "" : result;
    }


    public String getBillVoucherDate() throws Exception
    {
        String result = XPathUsage.findValue(root,
                                             XQLBILLHEAD + "/voucher_date");
        return (result == null) ? "" : result;
    }

    public String getBillID() throws Exception
    {
        String result = XPathUsage.findValue(root, XQLBILLHEAD + "/voucher_id");
        return (result == null) ? "" : result;
    }

    public String getlodebatch() throws Exception
    {
            String result = XPathUsage.findValue(root, XQLBILLHEAD + "/lodebatch");
            return (result == null) ? "" : result;
    }

    /**
     * 得到所有业务数据
     * @return 所有业务数据
     * @throws Exception
     */
    public NodeList getAllEntries() throws Exception
    {
        NodeList nl = XPathAPI.selectNodeList(root, XQLENTRY);
        return nl;
    }


    /**
     * 将Entry按省归类
     * @return
     */
    public Map getUnitEntries() throws Exception
    {
        String strLastProvince = "";

        Map contentMap = new Hashtable();
        Vector vecProvince = null;

        NodeList nl = XPathAPI.selectNodeList(root, XQLENTRY);

        for (int i = 0; i < nl.getLength(); i++)
        {
            Node node = nl.item(i);

            node = transformNode(node);

            BillEntry be = new BillEntry(node);
            String unit = be.getUnitCode();

            if (null == unit || unit.length() < 2)
            {
                throw new Exception("单位" + unit + "编码错");
            }

            //按[0,6)分类
            String province = unit.substring(0, 2);

            if (!province.equals(strLastProvince))
            {
                strLastProvince = province;

                vecProvince = (Vector) contentMap.get(province);

                if (vecProvince == null)
                {
                    vecProvince = new Vector();
                    contentMap.put(province, vecProvince);
                }
            }

            vecProvince.add(be);
        }

        return contentMap;
    }

    /**
     * 得到主方法
     * @return 主方法
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        BillReader billReader1 = new BillReader();
        billReader1.parseXML();
        p("|" + billReader1.getBillHeadCompany() + "|");

        NodeList nl = billReader1.getAllEntries();

        for (int i = 0; i < nl.getLength(); i++)
        {

            Node node = nl.item(i);
            BillEntry be = new BillEntry(node);
            p(be.getAccountNo());
        }
    }


    public static void p(String in)
    {
        System.out.println(in);
    }

    /**
     * 将属于大的DOM树的节点转换成属于小的DOM树的节点
     * @param nodeSrc
     * @return
     */
    private Node transformNode(Node nodeSrc)
    {
        return m_docTemp.importNode(nodeSrc, true);
    }
}


class BillEntry
{
    //实体根
    Node entryRoot;

    public BillEntry(Node entryRoot)
    {
        this.entryRoot = entryRoot;
    }

    public String getEntryChildValue(String relativeXQL) throws Exception
    {
        String result = XPathUsage.findValue(entryRoot, relativeXQL);
        return (result == null) ? "" : result;

    }

    /**
     * 得到单证类别（号、名称）
     * @return 单证类别（号、名称）
     */
    public String getBillType() throws Exception
    {
        return getEntryChildValue("bill_type");
    }

    /**
     * 得到单证号
     * @return 单证单证号
     */
    public String getBillID() throws Exception
    {
        return getEntryChildValue("bill_id");
    }

    /**
     * 得到机构编码
     * @return 机构编码
     */
    public String getUnitCode() throws Exception
    {
        return getEntryChildValue("unit_code");
    }

    /**
     * 得到保单号
     * @return 保单号
     */
    public String getPolicyNo() throws Exception
    {
        return getEntryChildValue("policy_no");
    }

    /**
     * 得到机构编码
     * @return 机构编码
     */
    public String getBussNo() throws Exception
    {
        return getEntryChildValue("buss_no");
    }

    /**
     * 得到机构编码
     * @return 机构编码
     */
    public String getBillDate() throws Exception
    {
        return getEntryChildValue("bill_date");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */

    public String gettransationdate() throws Exception
    {
        return getEntryChildValue("transationdate");
    }

    public String getaccountingperiod() throws Exception
    {
        return getEntryChildValue("accountingperiod");
    }


    public String getSettlement() throws Exception
    {
        return getEntryChildValue("settlement");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getCheckNo() throws Exception
    {
        return getEntryChildValue("check_no");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getAccountNo() throws Exception
    {
        return getEntryChildValue("account_no");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getcurrency() throws Exception
    {
        return getEntryChildValue("currency");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getMoney() throws Exception
    {
        return getEntryChildValue("money");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getRenew() throws Exception
    {
        return getEntryChildValue("renew");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getAbstract() throws Exception
    {
        return getEntryChildValue("abstract");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */
    public String getExcrate1() throws Exception
    {
        return getEntryChildValue("excrate1");
    }

    /**
     * 得到结算方式
     * @return 结算方式
     */




    public String getaccbooktype() throws Exception
    {
        return getEntryChildValue("accbooktype");
    }

    public String getaccbookcode() throws Exception
    {
       return getEntryChildValue("accbookcode");
    }

   public String getcentercode() throws Exception
   {
      return getEntryChildValue("centercode");
   }

   public String getbranchcode() throws Exception
   {
      return getEntryChildValue("branchcode");
   }

   public String getPolicynoother() throws Exception
   {
      return getEntryChildValue("Policynoother");
   }

   public String getpolicytype1() throws Exception
   {
      return getEntryChildValue("policytype1");
   }

   public String getpolicyno1() throws Exception
   {
      return getEntryChildValue("policyno1");
   }

   public String getPolicystart() throws Exception
   {
      return getEntryChildValue("Policystart");
   }

   public String getPolicyend() throws Exception
   {
      return getEntryChildValue("Policyend");
   }

   public String getoperdate() throws Exception
   {
      return getEntryChildValue("operdate");
   }

   public String getpolicydate() throws Exception
   {
      return getEntryChildValue("policydate");
   }

   public String getpolicyresp() throws Exception
   {
      return getEntryChildValue("policyresp");
   }

   public String getitemdesc() throws Exception
   {
      return getEntryChildValue("itemdesc");
   }

   public String getrelareason() throws Exception
   {
      return getEntryChildValue("relareason");
   }

   public String getagreementyear() throws Exception
   {
      return getEntryChildValue("agreementyear");
   }

   public String getavalibletype() throws Exception
   {
      return getEntryChildValue("avalibletype");
   }

   public String getavalibleterm() throws Exception
   {
      return getEntryChildValue("avalibleterm");
   }

   public String getsellchannel() throws Exception
   {
      return getEntryChildValue("sellchannel");
   }

   public String gettakeway() throws Exception
   {
      return getEntryChildValue("takeway");
   }

   public String getaccountcode() throws Exception
   {
      return getEntryChildValue("accountcode");
   }

   public String getF01() throws Exception
   {
      return getEntryChildValue("F01");
   }

   public String getF02() throws Exception
   {
      return getEntryChildValue("F02");
   }

   public String getF03() throws Exception
   {
      return getEntryChildValue("F03");
   }


   public String getF04() throws Exception
   {
      return getEntryChildValue("F04");
   }


   public String getF05() throws Exception
   {
      return getEntryChildValue("F05");
   }


   public String getF06() throws Exception
   {
      return getEntryChildValue("F06");
   }


   public String getF07() throws Exception
   {
      return getEntryChildValue("F07");
   }


   public String getF08() throws Exception
   {
      return getEntryChildValue("F08");
   }

   public String getF09() throws Exception
   {
      return getEntryChildValue("F09");
   }

   public String getF10() throws Exception
   {
      return getEntryChildValue("F10");
   }

   public String getF11() throws Exception
   {
      return getEntryChildValue("F11");
   }

   public String getF12() throws Exception
   {
      return getEntryChildValue("F12");
   }

   public String getF13() throws Exception
   {
      return getEntryChildValue("F13");
   }


   public String getF14() throws Exception
   {
      return getEntryChildValue("F14");
   }


   public String getF15() throws Exception
   {
      return getEntryChildValue("F15");
   }


   public String getF16() throws Exception
   {
      return getEntryChildValue("F16");
   }


   public String getF17() throws Exception
   {
      return getEntryChildValue("F17");
   }


   public String getF18() throws Exception
   {
      return getEntryChildValue("F18");
   }

   public String getF19() throws Exception
   {
      return getEntryChildValue("F19");
   }

   public String getF20() throws Exception
   {
      return getEntryChildValue("F20");
   }

   public String getF21() throws Exception
   {
      return getEntryChildValue("F21");
   }

   public String getF22() throws Exception
   {
      return getEntryChildValue("F22");
   }

   public String getF23() throws Exception
   {
      return getEntryChildValue("F23");
   }


   public String getF24() throws Exception
   {
      return getEntryChildValue("F24");
   }


   public String getF25() throws Exception
   {
      return getEntryChildValue("F25");
   }


   public String getF26() throws Exception
   {
      return getEntryChildValue("F26");
   }


   public String getF27() throws Exception
   {
      return getEntryChildValue("F27");
   }


   public String getF28() throws Exception
   {
      return getEntryChildValue("F28");
   }

   public String getF29() throws Exception
   {
      return getEntryChildValue("F29");
   }

   public String getF30() throws Exception
   {
      return getEntryChildValue("F30");
   }

   public String getrawunit() throws Exception
   {
      return getEntryChildValue("rawunit");
   }

   public String getrawoper() throws Exception
   {
      return getEntryChildValue("rawoper");
   }

   public String getpolicykey() throws Exception
   {
      return getEntryChildValue("policykey");
   }

   public String gettemp() throws Exception
   {
      return getEntryChildValue("temp");
   }




    public String getExcrate2() throws Exception
    {
        return getEntryChildValue("excrate2");
    }

    //grppol_no
    public String getGrppolNo() throws Exception
    {
        return getEntryChildValue("grppol_no");
    }

    /**
     * 得到票据号
     * @return 票据号
     */
    public String getDocumentID() throws Exception
    {
        return getEntryChildValue("document_id");
    }

    /**
     * 得到票据日期
     * @return 票据日期
     */
    public String getDocumentDate() throws Exception
    {
        return getEntryChildValue("document_date");
    }

    /**
     * 得到险种信息
     * @return 险种信息
     */
    public String getRiskInfo() throws Exception
    {
        return getEntryChildValue("risk_info");
    }

    /**
     * 得到险类信息
     * @return 险类信息
     */
    public String getRiskSort() throws Exception
    {
        return getEntryChildValue("risk_sort");
    }


    /**
     * 得到险种缴费
     * @return 险种缴费方式
     */
    public String getRiskInfoPayWay() throws Exception
    {
        return getEntryChildValue("risk_info/@pay_way");
    }

    /**
     * 得到险种缴费年期
     * @return 险种缴费年期
     */
    public String getRiskInfoPayPeriod() throws Exception
    {
        return getEntryChildValue("risk_info/@pay_period");
    }

    /**
     * 得到险种缴费期次
     * @return 险种缴费期次
     */
    public String getRiskInfoPayTime() throws Exception
    {
        return getEntryChildValue("risk_info/@pay_time");
    }


    public Node getAuxAccounting() throws Exception
    {
        return XPathAPI.selectSingleNode(entryRoot, "auxiliary_accounting");
    }

    public NodeList getAuxAccountingItems() throws Exception
    {
        NodeList nodeList = XPathAPI.selectNodeList(entryRoot,
                "auxiliary_accounting");
        return nodeList;
    }
}


class auxAccount
{
    Node auxAccRoot;
    NodeList items;

    public auxAccount(Node auxAccRoot) throws Exception
    {
        this.auxAccRoot = auxAccRoot;
        this.items = XPathAPI.selectNodeList(auxAccRoot, "item");
    }

    public NodeList getAllItems(Node tempNode) throws Exception
    {
        NodeList nl = XPathAPI.selectNodeList(tempNode, "item");
        return nl;
    }

    //public String getItemValue() throws Exception
    //{
    //  return getEntryChildValue("risk_info/@pay_time");
    //}

    private String getAuxAccChildValue(String relativeXQL, Node no) throws
            Exception
    {
        String result = null;
        //System.out.println("length is " + items.getLength());
        NodeList nls = XPathAPI.selectNodeList(no, "item");
        //System.out.println("get length is : " + nls.getLength());
        for (int i = 0; i < nls.getLength(); i++)
        {

            Item it = new Item(nls.item(i));
            //System.out.println(it.getValue(nls.item(i)));

            if (it.getProName().equals(relativeXQL))
            {
                //System.out.println("get the Match : "+it.getProName() );
                result = it.getValue(nls.item(i));
                //System.out.println("getvalue ：" + result);
            }
        }
        //String result=XPathUsage.getTextContents();// findValue(auxAccRoot,relativeXQL);
        return (result == null) ? "" : result;
    }

    private String getEntryChildValue(String Str) throws Exception
    {
        String result = XPathUsage.findValue(auxAccRoot, Str);
        return (result == null) ? "" : result;
    }


    //部门
    public String getAccDep(Node no) throws Exception
    {
        return getAuxAccChildValue("部门", no);
    }

    //人员
    public String getAccPerson(Node no) throws Exception
    {
        return getAuxAccChildValue("人员", no);
    }

    //客户
    public String getAccCus(Node no) throws Exception
    {
        return getAuxAccChildValue("客户", no);
    }

    //供应商
    public String getAccSup(Node no) throws Exception
    {
        return getAuxAccChildValue("供应商", no);
    }

    //项目
    public String getAccPro(Node no) throws Exception
    {
        return getAuxAccChildValue("项目", no);
    }

    public String getItemName() throws Exception
    {
        return getEntryChildValue("@name");
    }


}


class Item
{
    Node itemRoot;
    public Item(Node itemRoot)
    {
        this.itemRoot = itemRoot;
    }

    public String getValue(Node no) throws Exception
    {
        String result = XPathUsage.findValue(no, ".");
        return (result == null) ? "" : result;
    }

    public String getProName() throws Exception
    {
        String result = XPathUsage.findValue(itemRoot, "@name");
        return (result == null) ? "" : result;
    }
}
