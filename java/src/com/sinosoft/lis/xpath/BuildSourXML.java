package com.sinosoft.lis.xpath;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;


public class BuildSourXML
{
    String templateStr = "<?xml version=\"1.0\" encoding=\"GB2312\"?>"
                         + "\n" + "<billToVouchers>"
                         + "\n" + "</billToVouchers>";

    private String filePath = Conf.TEMPLATEFILE;

    //doc root
    private static final String XQLROOT = "/billToVouchers";

    private static final String XQLVOUCHER = XQLROOT + "/billToVoucher";

    private static final String XQLBILLTYPE = XQLVOUCHER + "/bill_type";

    private static final String XQLBILLWAY = XQLVOUCHER + "/way";

    private static final String XQLMAINITEM = XQLVOUCHER + "/mainItem";

    private static final String XQLSEGITEM = XQLMAINITEM + "/segItem";

    private Document document;
    private Node root;
    private Node voucherRoot;
    private Node mainRoot;
    private Node segRoot;
    private Node wayRoot;


    public BuildSourXML()
    {
        //System.out.println(templateStr);
    }

    public BuildSourXML(String filePath)
    {
        this.filePath = filePath;
    }

    public void parseXML() throws Exception
    {
        StringReader sr = new StringReader(this.templateStr);
        InputSource inputSource = new InputSource(sr);

        // Parse the file
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        //    document = db.parse(filePath);
        document = db.parse(inputSource);
        root = document.getDocumentElement();
        voucherRoot = XPathAPI.selectSingleNode(root, XQLVOUCHER);
    }

    /**
     * 加一个新节点,并分行
     * @param start 开始节点
     * @param name 新节点名
     * @return 新节点
     */
    private Node addNodeNewLine(Node start, String name)
    {
        Document document = start.getOwnerDocument();
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    public Node getRootNode() throws Exception
    {
        return this.root;
    }

    public Node getVoucherNode() throws Exception
    {
        if (this.voucherRoot == null)
        {
            voucherRoot = addNodeNewLine(this.root, "billToVoucher");
        }

        return voucherRoot;
    }

    public Node getVoucherMainItemNode() throws Exception
    {
        if (this.mainRoot == null)
        {
            mainRoot = addNodeNewLine(voucherRoot, "mainItem");
        }
        return mainRoot;
    }

    public Node getVoucherWay() throws Exception
    {
        if (this.wayRoot == null)
        {
            wayRoot = addNodeNewLine(voucherRoot, "way");
        }
        return wayRoot;
    }

    public void setBillType(String value) throws Exception
    {
        addSetVoucherValue("bill_type", value);
    }

    public void setWay(String value) throws Exception
    {
        addSetVoucherValue("way", value);
    }

    public void addSetVoucherValue(String newNodeName, String value) throws
            Exception
    {
        Node headNode = this.getVoucherNode();
        Node newNode = addNodeNewLine(headNode, newNodeName);
        setNodeValue(newNode, value);
    }

    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }


    public static void main(String[] args) throws Exception
    {
        BuildSourXML bs = new BuildSourXML();
        bs.parseXML();
        Node bn = bs.getRootNode();

        File f = new File("d:/houzw/bkSour.txt");
        //System.out.println("aa");
        FileInputStream fis = new FileInputStream(f);
        int len = fis.available();
        //System.out.println("len is :"+len);
        byte b[] = new byte[len];
        int h = fis.read(b, 0, len);
        String content = new String(b);
        content = "数据组织生成xml:" + "\r\n" + content;
        char[] ch = content.toCharArray();
        //for()
        //System.out.println(ch.length);
        int lenCh = ch.length;
        int hh = 0;
        for (int i = 0; i < ch.length; i++)
        {
            int end = 0;
            int start = 0;
            if (content.charAt(i) == '{')
            {
                billToVoucherSet btv = new billToVoucherSet(bn);
                end = i - 2;
                //System.out.println(""+end);
                for (int j = end - 1; j > 0; j--)
                {
                    //System.out.print(content.charAt(j));
                    if (content.charAt(j) == '\n')
                    {
                        start = j + 1;
                        String tempStr = content.substring(start, end);
                        StringTokenizer st = new StringTokenizer(tempStr, " ");

                        int count = 0;
                        while (st.hasMoreElements())
                        {
                            String str = st.nextToken();

                            if (count == 0)
                            {
                                btv.setBillType(str);
                            }
                            if (count == 1)
                            {
                                btv.setWay(str);
                            }
                            if (count == 2)
                            {
                                btv.setMainName(str);
                            }
                            if (count == 3)
                            {
                                btv.setMainCode(str);
                            }
                            if (count == 4)
                            {
                                btv.setMainDirNum(str);
                            }
                            if (count == 5)
                            {
                                btv.setMainFlag(str);
                            }

                            count++;
                        }
                        //System.out.println(content.substring(start,end));
                        break;
                    }
                }
                ///////////////
                Node main = btv.getMainNode();

                for (int k = i + 3; k < ch.length; k++)
                {
                    int ends = 0;
                    if (content.charAt(k) == '}')
                    {
                        ends = k - 2;
                        String tmpCon = content.substring(i + 3, ends);
                        //System.out.println(tmpCon);
                        StringTokenizer st = new StringTokenizer(tmpCon, "\r\n");

                        while (st.hasMoreElements())
                        {
                            String tmpStr = st.nextToken();

                            segItemSet sis = new segItemSet(main);
                            StringTokenizer sts = new StringTokenizer(tmpStr,
                                    " ");
                            //4个
                            int cc = 0;
                            while (sts.hasMoreElements())
                            {
                                String lastStr = sts.nextToken();
                                //System.out.println("get lastStr : " + lastStr);
                                if (cc == 0)
                                {
                                    sis.setSegName(lastStr);
                                }
                                if (cc == 1)
                                {
                                    sis.setSegCode(lastStr);
                                }
                                if (cc == 2)
                                {
                                    sis.setSegLevel(lastStr);
                                }
                                if (cc == 3)
                                {
                                    sis.setSegFlag(lastStr);
                                }
                                cc = cc + 1;
                                //System.out.println("hou : " + lastStr);
                            }
                        }
                        ////////////////////adsfa
                        break;
                    }
                }
                //System.out.println();
            }
        }

        FileWriter fw = new FileWriter("d:\\houtestSour.xml");
        fw.write(XPathUsage.toString(bs.getRootNode()));
        fw.close();
        System.out.println("输出到 d:/houtestSour.xml");
        /* BuildSourXML bs = new BuildSourXML();
             bs.parseXML();
             Node bn = bs.getRootNode();

             for(int i = 0 ; i < 3;i++)
             {
             billToVoucherSet btv = new billToVoucherSet(bn);
             btv.setBillType("a"+i+"adf");
             btv.setMainName("na"+i+"me");
             btv.setMainCode("co"+i+"de");
             btv.setMainDirNum("dir"+i+"num");
             btv.setMainFlag("1");

             Node mainNode = btv.getMainNode();
             for(int j = 0 ; j < 3 ; j++)
             {
             segItemSet si = new segItemSet(mainNode);
             si.setSegName("测试a i="+ i + " ;j="+j);
             si.setSegCode("测试b i="+ i + " ;j="+j);
             si.setSegLevel("1");
             si.setSegFlag("1");
             }

             }
             System.out.println(XPathUsage.toString(bs.getRootNode()));
         */
    }
}


///////////////////////////////////////////////////////////////////////////////

class billToVoucherSet
{

    private Node parentNode; //上级
    private Node bTvNode; //本级
    private Document document;

    public billToVoucherSet(Node Node) throws Exception
    {
        this.parentNode = Node;
        document = parentNode.getOwnerDocument();
        bTvNode = addNodeNewLine(parentNode, "billToVoucher");
    }

    public void setBillType(String v) throws Exception
    {
        addSetValue("bill_type", v);
    }

    public void setWay(String v) throws Exception
    {
        addSetValue("way", v);
    }

    public void setMainName(String v) throws Exception
    {
        addSetProValue("mainItem", "name", v);
    }

    public void setMainCode(String v) throws Exception
    {
        addSetProValue("mainItem", "code", v);
    }

    public void setMainDirNum(String v) throws Exception
    {
        addSetProValue("mainItem", "dir_num", v);
    }

    public void setMainFlag(String v) throws Exception
    {
        addSetProValue("mainItem", "flag", v);
    }

    public Node getMainNode() throws Exception
    {
        Node res = XPathAPI.selectSingleNode(bTvNode, "mainItem");
        return res;
    }

    private void addSetValue(String newNodeName, String value) throws Exception
    {
        //System.out.println("得到 newNodeName ：" + newNodeName);
        if (XPathAPI.selectSingleNode(bTvNode, newNodeName) != null)
        {
            //System.out.println("找到节点");
            Node tempNode = XPathAPI.selectSingleNode(bTvNode, newNodeName);
            setNodeValue(tempNode, value);
        }
        else
        {
            //System.out.println("测试添加新的节点" + newNodeName);
            Node newNode = addNodeNewLine(bTvNode, newNodeName);
            setNodeValue(newNode, value);
        }
    }

    private void addSetProValue(String NodeName, String attrName, String value) throws
            Exception
    {
        Node targetNode = XPathAPI.selectSingleNode(bTvNode, NodeName);
        if (targetNode != null)
        {
            //System.out.println("进入If");

            Element main = (Element) targetNode;
            main.setAttribute(attrName, value);
            //entryNode.appendChild(riskinfo);
            //Node newLine=document.createTextNode("\n");
            //entryNode.appendChild(newLine);
        }
        else
        {

            //System.out.println("来到Else");
            //Node newNode=addNodeNewLine(entryNode,NodeName);
            //newNode.getChildNodes();
            Element main = document.createElement(NodeName);
            main.setAttribute(attrName, value);
            //att.setNodeValue(value);

            bTvNode.appendChild(main);
            Node newLine = document.createTextNode("\n");
            bTvNode.appendChild(newLine);

            //    Element linkurl=document.createElement("haha");
            //Text textseg=document.createTextNode("url");
            //linkurl.appendChild(textseg);
            //entryNode.appendChild(linkurl);
        }
        //增加一个新节点，再增加属性
    }


    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }

    private Node addNodeNewLine(Node start, String name)
    {
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        //start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

}


///////////////////////////////////////////////////////////////////////////////

class segItemSet
{
    Node mainNode;
    Node segNode;
    Document document;

    public segItemSet(Node no)
    {
        this.mainNode = no;
        document = mainNode.getOwnerDocument();
        segNode = addNodeNewLine(mainNode, "segItem");
    }

    private Node addNodeNewLine(Node start, String name)
    {
        Node newLine = document.createTextNode("\n");
        //    Node newLine2=document.createTextNode("\n");
        Node newNode = document.createElement(name);

        //start.appendChild(newLine);
        start.appendChild(newNode);
        start.appendChild(newLine);

        return newNode;
    }

    public void setNodeValue(Node node, String value) throws Exception
    {
        Node textNode = document.createTextNode(value);
        node.appendChild(textNode);
    }

    private void addSetValue(String newNodeName, String value) throws Exception
    {
        //System.out.println("得到 newNodeName ：" + newNodeName);
        if (XPathAPI.selectSingleNode(mainNode, newNodeName) != null)
        {
            System.out.println("找到节点");
            Node tempNode = XPathAPI.selectSingleNode(mainNode, newNodeName);
            setNodeValue(tempNode, value);
        }
        else
        {
            System.out.println("测试添加新的节点" + newNodeName);
            Node newNode = addNodeNewLine(mainNode, newNodeName);
            setNodeValue(newNode, value);
        }
    }

    public void setSegName(String v) throws Exception
    {
        addSetProValue("segItem", "name", v);
    }

    public void setSegCode(String v) throws Exception
    {
        addSetProValue("segItem", "code", v);
    }

    public void setSegLevel(String v) throws Exception
    {
        addSetProValue("segItem", "dir_level", v);
    }

    public void setSegFlag(String v) throws Exception
    {
        addSetProValue("segItem", "flag", v);
    }


    private void addSetProValue(String NodeName, String attrName, String value) throws
            Exception
    {
        /*Node targetNode = XPathAPI.selectSingleNode(segNode,NodeName);
             if(targetNode!=null)
             {
             //System.out.println("进入If");

             Element seg = (Element)targetNode;
             seg.setAttribute(attrName,value);
             //entryNode.appendChild(riskinfo);
             //Node newLine=document.createTextNode("\n");
             //entryNode.appendChild(newLine);
             }else
             {
         */
        //System.out.println("来到Else");
        //Node newNode=addNodeNewLine(entryNode,NodeName);
        //newNode.getChildNodes();
        //segNode.
        Element seg = (Element) segNode;
        //Element seg = document.createElement(NodeName);
        seg.setAttribute(attrName, value);
        //att.setNodeValue(value);

        mainNode.appendChild(seg);
        //Node newLine=document.createTextNode("\n");
        //mainNode.appendChild(newLine);

        //    Element linkurl=document.createElement("haha");
        //Text textseg=document.createTextNode("url");
        //linkurl.appendChild(textseg);
        //entryNode.appendChild(linkurl);
        /*    }*/
        //增加一个新节点，再增加属性
    }


}
