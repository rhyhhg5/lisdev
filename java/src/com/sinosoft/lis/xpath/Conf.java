/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.xpath;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.schema.LDSysVarSchema;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft Inc.</p>
 * @author Yang Yalin
 * @version 1.0
 */

public class Conf
{
    public static String SOURFILE = "sour.xml";
    public static String PROFILE = "property.xml";
    public static String BILLFILE = "D:/houzw/bill.xml";
    public static String TEMPLATEFILE = "D:/xml/RQTemplate.xml";
    //财务数据流接收URL地址
    public static String UFURL =
            "http://192.168.71.107:7001/picc/intf/InterfLoad.jsp";

    public Conf()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        LDSysVarSchema tLDSysVarSchema = new LDSysVarSchema();
        tLDSysVarDB.setSysVar("FinacialURL");
        if (tLDSysVarDB.getInfo())
        {
            tLDSysVarSchema.setSchema(tLDSysVarDB.getSchema());
            UFURL = tLDSysVarSchema.getSysVarValue().trim();
        }
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
//        Conf conf1 = new Conf();
    }
}
