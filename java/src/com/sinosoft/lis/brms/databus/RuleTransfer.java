package com.sinosoft.lis.brms.databus;

import java.io.*;
import java.rmi.RemoteException;
import java.util.*;

import javax.xml.rpc.ServiceException;


import org.jdom.*;
import org.jdom.input.DOMBuilder;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.ExeSQL;

import com.sinosoft.brms.databus.client.*;


/**
 * <p>ClassName:RuleTransfer</p>
 * <p>
 *  Description: 规则引擎调用及解析
 * </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: SinoSoft  </p>
 * @author 张阳
 * @version 1.0
 * @date: 2009-12-7
 */


public class RuleTransfer implements Serializable
{
    public RuleTransfer()
    {
    }

    /**********************************BASEINFO--基本信息元素描述定义******************************************************/
    private static final String XML_UWDTO = "uwDto";
    private static final String XML_BUSSNISSMODULE = "businessModule";
    private static final String XML_ISAPPROVED = "isApproved";
    private static final String XML_ISTEST = "isTest";
    private static final String XML_POLICYNUM = "policyNum";
    private static final String XML_SUBSYSTEM = "subSystem";
    private static final String XML_SYSTEM = "system";
    private static final String XML_VERIFYRESULTLIST = "verifyResultList";
    private static final String XML_VERIFYRESULT = "verifyResult";
    private static final String XML_RETURNINFO = "returnInfo";
    private static final String XML_RULENAME = "ruleName";
    private static final String XML_RISKCODE = "riskCode";
    private static final String XML_INSUREDNO = "insuredNo";
    private static final String XML_CONTNO = "contNo";
    private static final String XML_OCCUPCODEAFTEREDOR = "occupCodeAfterEdor";
    private String passFlag;


    //调用规则引擎,返回校验信息
    public String getXmlStr(String system,String subSystem,String businessModule,String prtno,boolean isTest){

    	//获取规则引擎服务地址
    	ExeSQL exeSql = new ExeSQL();
    	String sql = "select sysvarvalue from ldsysvar where sysvar = 'RuleAddress'";
    	String ruleAddress = exeSql.getOneValue(sql);
    	System.out.println(ruleAddress);
    	//ruleAddress = "http://10.10.164.4:8028/DataBus/RuleService";

    	String xmlStr = "";
		RuleService service = null;
		RuleServiceImplServiceLocator locator = new RuleServiceImplServiceLocator();
		System.out.println("生成RuleServiceImplServiceLocator完毕");
//		"http://10.10.164.4:8028/DataBus/RuleService"
		locator.setRuleServiceImplPortEndpointAddress(ruleAddress);
		System.out.println("设置地址完毕");
		try {
			service = locator.getRuleServiceImplPort();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		System.out.println("生成RuleService完毕");
		try {
			//获得规则引擎返回的校验信息
			xmlStr = service.fireRule(system, subSystem, businessModule, prtno, isTest);
			System.out.println("调用fireRule完毕");
			System.out.println(xmlStr);
		} catch (RemoteException e) {
			System.out.println("调用fireRule异常");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return xmlStr;
    }
    

	// 调用规则引擎,返回校验信息 modify by zxs
	public String getNewXmlStr(String system, String subSystem, String businessModule, String prtno, boolean isTest) {

		// 获取规则引擎服务地址
		ExeSQL exeSql = new ExeSQL();
		String sql = "select sysvarvalue from ldsysvar where sysvar = 'NewRuleAddress'";
		String ruleAddress = exeSql.getOneValue(sql);
		System.out.println(ruleAddress);
		// ruleAddress = "http://10.10.164.4:8028/DataBus/RuleService";

		String xmlStr = "";
		RuleService service = null;
		RuleServiceImplServiceLocator locator = new RuleServiceImplServiceLocator();
		System.out.println("生成RuleServiceImplServiceLocator完毕");
		// "http://10.10.164.4:8028/DataBus/RuleService"
		locator.setRuleServiceImplPortEndpointAddress(ruleAddress);
		System.out.println("设置地址完毕");
		try {
			service = locator.getRuleServiceImplPort();
		} catch (ServiceException e1) {
			e1.printStackTrace();
		}
		System.out.println("生成RuleService完毕");
		try {
			// 获得规则引擎返回的校验信息
			xmlStr = service.fireRule(system, subSystem, businessModule, prtno, isTest);
			System.out.println("调用fireRule完毕");
			System.out.println(xmlStr);
		} catch (RemoteException e) {
			System.out.println("调用fireRule异常");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return xmlStr;
	}


    //将字符串转为Document对象
    public Document stringToDoc(String xmlStr){

    	Document document = null;
    	ByteArrayInputStream bais = null;
		try {
			bais = new ByteArrayInputStream(xmlStr.getBytes("GBK"));
	 		DOMBuilder domB = new DOMBuilder();
	 		document = domB.build(bais);
	 		bais.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
                catch (JDOMException e) {
			e.printStackTrace();
		} finally {
			try {
				bais.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    	return document;
    }

    //解析校验结论
    public String getApproved(Document doc){

    	Element root = (Element) doc.getRootElement();
   	    Element isApproved = root.getChild(XML_ISAPPROVED);
   	    passFlag = isApproved.getTextTrim();

   	    return passFlag;
    }

    //解析校验信息，返回所有险种的核保校验信息LMUWSet
    public LMUWSet getAllPolUWMSG(Document doc){

    	LMUWSet tLMUWSet = new LMUWSet();
    	//如果校验不通过，获取校验信息
    	if(passFlag!=null && !"1".equals(passFlag)){

    		Element root = (Element) doc.getRootElement();
       	    Element verifyResultList = root.getChild(XML_VERIFYRESULTLIST);
       	    Element eleContNo = root.getChild(XML_CONTNO);
       	    String tContNo=eleContNo.getTextTrim();
       	    ExeSQL tExeSQL=new ExeSQL();
       	    String tSQL=null;
       	    if(verifyResultList!=null){
       	    	List rsList = verifyResultList.getChildren(XML_VERIFYRESULT);
       	    	String insuredName="";
       	    	String insuredNo="";
       	    	for(int i=0;i<rsList.size();i++){
       	    		insuredName="";
       	    		insuredNo="";
       	   	    	Element verifyResult = (Element) rsList.get(i);
       	   	    	String returnInfo = verifyResult.getChildTextTrim(XML_RETURNINFO);
       	   	    	String ruleCode = verifyResult.getChildTextTrim(XML_RULENAME);
       	   	        String riskCode = verifyResult.getChildTextTrim(XML_RISKCODE);
       	   	        insuredNo=verifyResult.getChildTextTrim(XML_INSUREDNO);
       	   	        if(insuredNo!=null && !insuredNo.equals("")){
       	   	        	tSQL="select name from lcinsured where contno='"+tContNo
       	   	        		+"' and insuredno='"+insuredNo+"'";
       	   	        	insuredName=tExeSQL.getOneValue(tSQL);
       	   	        	if(insuredName==null){
       	   	        		insuredName="";
       	   	        	}
       	   	    	}else{
       	   	    		insuredNo="";
       	   	    	}
                    String toccupCodeAfterEdor = verifyResult.getChildTextTrim(XML_OCCUPCODEAFTEREDOR);
       	   	        LMUWSchema tLMUWSChema = new LMUWSchema();
       	   	        tLMUWSChema.setRiskCode(riskCode);
       	   	        tLMUWSChema.setRiskName(insuredName);
       	   	        tLMUWSChema.setOthCalCode(insuredNo);
                        if(toccupCodeAfterEdor== null || toccupCodeAfterEdor.equals ("")){
                            tLMUWSChema.setRemark(returnInfo);
                         }else
                         {
                             tLMUWSChema.setRemark("被保人变更后职业代码为".concat(toccupCodeAfterEdor).concat("，").concat(returnInfo));
                         }
       	   	    	tLMUWSChema.setUWCode(ruleCode);
       	   	        tLMUWSChema.setPassFlag("5");
       	   	        tLMUWSChema.setUWGrade("A1");//系统只存在A1
       	   	    	tLMUWSet.add(tLMUWSChema);
       	   	    }
       	    }
    	}

   	   return tLMUWSet;
    }

    //获取某个险种的核保校验信息LMUWSet
    public LMUWSet getPolUWMSG(LMUWSet LMUWSetPol,String riskcode){

    	LMUWSet LMUWSetPolUnPass = new LMUWSet();

    	for(int i = 1;i <= LMUWSetPol.size();i++){
    		LMUWSchema tLMUWSchema = LMUWSetPol.get(i);
    		if(riskcode.equals(tLMUWSchema.getRiskCode()) || "000000".equals(tLMUWSchema.getRiskCode())){
    			LMUWSetPolUnPass.add(tLMUWSchema);
    		}
    	}

    	return LMUWSetPolUnPass;
    }
//  获取某个险种的核保校验信息LMUWSet
    public LMUWSet getSinglePolUWMSG(LMUWSet LMUWSetPol,String riskCode,String insuredNo){

    	LMUWSet LMUWSetPolUnPass = new LMUWSet();

    	if(LMUWSetPol.size()>0){
    		for(int i = 1;i <= LMUWSetPol.size();i++){
        		LMUWSchema tLMUWSchema = LMUWSetPol.get(i);
        		if(riskCode.equals(tLMUWSchema.getRiskCode())
        				&&insuredNo.equals(tLMUWSchema.getOthCalCode())){
        			LMUWSetPolUnPass.add(tLMUWSchema);
        		}
        	}
    	}
    	return LMUWSetPolUnPass;
    }

//  获取某个险种的核保校验信息LMUWSet
    public LMUWSet getInsuredUWMSG(LMUWSet LMUWSetPol){

    	LMUWSet LMUWSetPolUnPass = new LMUWSet();

    	if(LMUWSetPol.size()>0){
    		for(int i = 1;i <= LMUWSetPol.size();i++){
        		LMUWSchema tLMUWSchema = LMUWSetPol.get(i);
        		if(tLMUWSchema.getRiskCode().equals("000000")
        				&&tLMUWSchema.getOthCalCode()!=null
        				&&!tLMUWSchema.getOthCalCode().equals("")){
        			LMUWSetPolUnPass.add(tLMUWSchema);
        		}
        	}
    	}
    	return LMUWSetPolUnPass;
    }

    
//  获取某个险种的核保校验信息LMUWSet
    public LMUWSet getCommonPolUWMSG(LMUWSet LMUWSetPol){

    	LMUWSet LMUWSetPolUnPass = new LMUWSet();
    	if(LMUWSetPol.size()>0){
    		for(int i = 1;i <= LMUWSetPol.size();i++){
        		LMUWSchema tLMUWSchema = LMUWSetPol.get(i);
        		if("000000".equals(tLMUWSchema.getRiskCode())){
        			LMUWSetPolUnPass.add(tLMUWSchema);
        		}
        	}
    	}
    	return LMUWSetPolUnPass;
    }

//    public Document test(){
//
//    	BPOTest test = new BPOTest();
//    	test.setTestFileName("1.xml");
//    	String str = test.getFileAddress();
//    	File testFile = new File(str);
//    	InputStream bais = null;
//		try {
//			bais = new FileInputStream(testFile);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//
//		Document document = null;
//		try {
//	 		DOMBuilder domB = new DOMBuilder();
//	 		document = domB.build(bais);
//	 		bais.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (JDOMException e) {
//			e.printStackTrace();
//		}
//
//		return document;
//    }




    public static void main(String[] arg){

    	RuleService service = null;
	RuleServiceImplServiceLocator locator = new RuleServiceImplServiceLocator();

	locator.setRuleServiceImplPortEndpointAddress("http://10.252.4.31:9081/DataBus/RuleService");
	try {
		service = locator.getRuleServiceImplPort();
	} catch (ServiceException e1) {
		e1.printStackTrace();
	}

	Document doc = null;
	RuleTransfer ruleTransfer = new RuleTransfer();
	try {
//		System.out.println(service.fireRule("lis", "uw", "NU", "1005050500001685",
//				false));
		//获得规则引擎返回的校验信息
		String xmlStr = service.fireRule("lis", "uw", "SU", "20100128000008|TB",false);
		System.out.println(xmlStr);
		//将校验信息转换为Xml的document对象
		doc = ruleTransfer.stringToDoc(xmlStr);
		System.out.println("哈哈");
	} catch (RemoteException e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	}

    }
}
