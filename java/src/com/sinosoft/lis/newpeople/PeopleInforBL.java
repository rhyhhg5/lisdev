package com.sinosoft.lis.newpeople;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.NewPeopleTaskTableSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: lis
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003
 * </p>
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author zxs
 * @version 1.0
 */

public class PeopleInforBL {
	// @Field
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mOperate = "";

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private NewPeopleTaskTableSchema mNewPeopleSchema = new NewPeopleTaskTableSchema();

	private MMap mMap = new MMap();

	private NewPeopleTaskTableSchema peopleTaskTableSchema = new NewPeopleTaskTableSchema();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	public PeopleInforBL() {
	}

	// @Method
	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		this.mInputData = cInputData;
		// 全局变量
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		peopleTaskTableSchema = (NewPeopleTaskTableSchema) cInputData.getObjectByObjectName("NewPeopleTaskTableSchema",
				0);
		return true;
	}

	private boolean dealData() {

		if ("INSERT||MAIN".equals(mOperate)) {
			System.out.println(peopleTaskTableSchema.getName());
			mNewPeopleSchema.setSerialNo(PubFun1.CreateMaxNo("PeopleInforBL", 5));
			mNewPeopleSchema.setName(peopleTaskTableSchema.getName());
			mNewPeopleSchema.setSex(peopleTaskTableSchema.getSex());
			mNewPeopleSchema.setBirthday(peopleTaskTableSchema.getBirthday());
			mNewPeopleSchema.setIDType(peopleTaskTableSchema.getIDType());
			mNewPeopleSchema.setIDNo(peopleTaskTableSchema.getIDNo());
			mNewPeopleSchema.setRemark(peopleTaskTableSchema.getRemark());

			mNewPeopleSchema.setOperator(mGlobalInput.Operator);
			System.out.println(CurrentDate);
			mNewPeopleSchema.setMakeDate(CurrentDate);
			mNewPeopleSchema.setMakeTime(CurrentTime);
			mNewPeopleSchema.setModifyDate(CurrentDate);
			mNewPeopleSchema.setModifyTime(CurrentTime);
			mMap.put(this.mNewPeopleSchema, SysConst.INSERT);

		} else if ("UPDATE||MAIN".equals(mOperate)) {
			System.out.println(peopleTaskTableSchema.getName());
			mNewPeopleSchema.setSerialNo(peopleTaskTableSchema.getSerialNo());
			mNewPeopleSchema.setName(peopleTaskTableSchema.getName());
			mNewPeopleSchema.setSex(peopleTaskTableSchema.getSex());
			mNewPeopleSchema.setBirthday(peopleTaskTableSchema.getBirthday());
			mNewPeopleSchema.setIDType(peopleTaskTableSchema.getIDType());
			mNewPeopleSchema.setIDNo(peopleTaskTableSchema.getIDNo());
			mNewPeopleSchema.setRemark(peopleTaskTableSchema.getRemark());
			mNewPeopleSchema.setMakeDate(peopleTaskTableSchema.getMakeDate());
			mNewPeopleSchema.setMakeTime(peopleTaskTableSchema.getMakeTime());
			mNewPeopleSchema.setOperator(mGlobalInput.Operator);
			System.out.println(CurrentDate);

			mNewPeopleSchema.setModifyDate(CurrentDate);
			mNewPeopleSchema.setModifyTime(CurrentTime);
			mMap.put(this.mNewPeopleSchema, SysConst.UPDATE);

		} else if ("DELETE||MAIN".equals(mOperate)) {
			System.out.println(peopleTaskTableSchema.getName());
			mNewPeopleSchema.setSerialNo(peopleTaskTableSchema.getSerialNo());
			mNewPeopleSchema.setName(peopleTaskTableSchema.getName());
			mNewPeopleSchema.setSex(peopleTaskTableSchema.getSex());
			mNewPeopleSchema.setBirthday(peopleTaskTableSchema.getBirthday());
			mNewPeopleSchema.setIDType(peopleTaskTableSchema.getIDType());
			mNewPeopleSchema.setIDNo(peopleTaskTableSchema.getIDNo());
			mNewPeopleSchema.setRemark(peopleTaskTableSchema.getRemark());
			mNewPeopleSchema.setMakeDate(peopleTaskTableSchema.getMakeDate());
			mNewPeopleSchema.setMakeTime(peopleTaskTableSchema.getMakeTime());
			mNewPeopleSchema.setOperator(mGlobalInput.Operator);
			System.out.println(CurrentDate);
			mNewPeopleSchema.setModifyDate(CurrentDate);
			mNewPeopleSchema.setModifyTime(CurrentTime);
			mMap.put(this.mNewPeopleSchema, SysConst.DELETE);
		}
		return true;

	}

	// 提交
	public boolean submitData(VData cInputData, String tOperate) {
		this.mOperate = tOperate;
		System.err.println("cInputData=" + cInputData + ",tOperate=" + tOperate);
		try {
			if (!getInputData(cInputData)) {

				return false;
			}

			if (!dealData()) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "AdjustAssessBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据处理失败PeopleInforBL-->dealData!";
				this.mErrors.addOneError(tError);
				return false;
			}

			// 准备往后台的数据
			if (!prepareOutputData()) {
				return false;
			}

			PubSubmit tPubSubmit = new PubSubmit();
			System.out.println("Start PeopleInforBL Submit...");
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "PeopleInforBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			mInputData = null;

			System.out.println("End PeopleinforBL Submit...");
			// 如果有需要处理的错误，则返回
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			buildError("submitData", ex.toString());
			return false;
		}
	}

	/**
	 * 准备后台的数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PersonInforBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PeopleInforBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
}
