package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EsAssessBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private LDScanCheckUserSet mtLDScanCheckUserSet = new
            LDScanCheckUserSet();

    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();

    public EsAssessBL() {
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "EsAssessBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            this.mInputData = (VData) cInputData.clone();
            this.mOperate = cOperate;
            System.out.println("mOperate"+mOperate);
            if (!this.mOperate.equals("DEL") && !this.mOperate.equals("UPDATE")) {
                buildError("checkData",
                           "不支持的字符串"+mOperate+"");
                return false;
            }
            if (this.getInputData(cInputData) == false) {
                return false;
            }
            System.out.println("---End getInputData---");
            if (this.checkData() == false) {
                return false;
            }
            System.out.println("---End checkData---");
            if (this.dealData() == false) {
                return false;
            }
            System.out.println("---End dealData---");
            //统一递交数据
            mResult.add(map);
            PubSubmit pubsubmit = new PubSubmit();
            if (!pubsubmit.submitData(mResult, "")) {
                mErrors.copyAllErrors(pubsubmit.mErrors);
                return false;
            }
            System.out.println("---End pubsubmit---");
        } catch (Exception ex) {

        }
        return true;
    }

    private boolean getInputData(VData cInputData) {

        this.mtLDScanCheckUserSet.set((LDScanCheckUserSet) cInputData.
                                      getObjectByObjectName(
                                              "LDScanCheckUserSet", 0));
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (this.mtLDScanCheckUserSet == null) {
            buildError("getInputData",
                       "没有得到schema");
            return false;
        }
        System.out.println(mtLDScanCheckUserSet.get(1).getUserCode());
        System.out.println(mtLDScanCheckUserSet.get(1).getSubType());
        return true;
    }

    public boolean checkData() {

        if (this.mtLDScanCheckUserSet.size() < 1) {
            buildError("checkData",
                       "录入的信息有误!");
            return false;
        }
        for (int i = 1; i <= mtLDScanCheckUserSet.size(); i++) {
            String tSubType = mtLDScanCheckUserSet.get(i).getSubType();
            String cUserCode = mtLDScanCheckUserSet.get(i).getUserCode();
            if (cUserCode.equals("")) {
                buildError("checkData",
                           "UserCode传入错误！");
                return false;
            }
            for (int j = i + 1; j <= mtLDScanCheckUserSet.size(); j++) {
                String cSubType = mtLDScanCheckUserSet.get(j).getSubType();
                if (tSubType.equals(cSubType)) {
                    buildError("checkData",
                               "第" + i + "条和第" + j + "条录入重复！");
                    return false;
                }
            }
        }
        return true;
    }

    public boolean dealData() {
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        //删除原来的信息
        LDScanCheckUserDB dLDScanCheckUserDB = new
                                               LDScanCheckUserDB();
        dLDScanCheckUserDB.setUserCode(this.mtLDScanCheckUserSet.get(1).
                                       getUserCode());
        LDScanCheckUserSet dLDScanCheckUserSet = dLDScanCheckUserDB.query();
        //判断是插入还是删除
        if (this.mOperate.equals("UPDATE")) {
            LDScanCheckUserSet tLDScanCheckUserSet = new LDScanCheckUserSet();
            for (int i = 1; i <= this.mtLDScanCheckUserSet.size(); i++) {
                //先修改
                String olddate = "";
                String oldtime = "";
                LDScanCheckUserDB tLDScanCheckUserDB = new LDScanCheckUserDB();
                tLDScanCheckUserDB.setUserCode(this.mtLDScanCheckUserSet.get(i).
                                               getUserCode());
                tLDScanCheckUserDB.setSubType(this.mtLDScanCheckUserSet.get(i).
                                              getSubType());
                if (tLDScanCheckUserDB.getInfo()) {
                    olddate = tLDScanCheckUserDB.getMakeDate();
                    oldtime = tLDScanCheckUserDB.getMakeTime();
                }
                if (olddate.equals("")) {
                    olddate = cDate;
                }
                if (oldtime.equals("")) {
                    oldtime = cTime;
                }
                LDScanCheckUserSchema tLDScanCheckUserSchema =
                        mtLDScanCheckUserSet.
                        get(i);
                tLDScanCheckUserSchema.setOperator(mGlobalInput.Operator);
                tLDScanCheckUserSchema.setMakeDate(olddate);
                tLDScanCheckUserSchema.setMakeTime(oldtime);
                tLDScanCheckUserSchema.setModifyDate(cDate);
                tLDScanCheckUserSchema.setModifyTime(cTime);
                tLDScanCheckUserSet.add(tLDScanCheckUserSchema);
            }
            //加入map
            if (dLDScanCheckUserSet.size() > 0) {
                this.map.put(dLDScanCheckUserSet, "DELETE");
            }

            this.map.put(tLDScanCheckUserSet, "INSERT");
        }
        if (this.mOperate.equals("DEL")) {
            this.map.put(dLDScanCheckUserSet, "DELETE");
        }

        return true;
    }


}
