package com.sinosoft.lis.easyscan;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: EasyScan单证索引管理</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Liuqiang
 * @version 1.0
 */
public class EsDocManageBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();

    //输入参数私有变量
    private ES_DOC_MAINSchema tES_DOC_MAINSchema;
    private ES_DOC_PAGESSet tES_DOC_PAGESSet;

    public EsDocManageBLS()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = true;

        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        getInputData();

        //DELETE||MAIN  UPDATE||MAIN  UPDATE||PAGES  DELETE||PAGES
        tReturn = saveModify(cOperate);
        /*        if (cOperate.equals("DELETE||MAIN"))
                {
                    tReturn = deleteMain();
                }
                if (cOperate.equals("DELETE||PAGES"))
                {
                    tReturn = deletePages();
                }
                if (cOperate.equals("UPDATE||MAIN"))
                {
                    tReturn = updateMain();
                }
                if (cOperate.equals("UPDATE||PAGES"))
                {
                    tReturn = updatePages();
                }*/
        if (tReturn)
        {
            System.out.println("EsDocManageBLS:Save Successful!");
        }
        else
        {
            System.out.println("EsDocManageBLS:Save Failed!");
        }

        mResult.clear();
        mResult = mInputData;
        mInputData = null;

        return tReturn;
    }

    //获取返回数据
    public VData getResult()
    {
        return mResult;
    }

    //入参处理
    private boolean getInputData()
    {
        //获取入参
        tES_DOC_MAINSchema = (ES_DOC_MAINSchema) mInputData.get(0);
        tES_DOC_PAGESSet = (ES_DOC_PAGESSet) mInputData.get(1);

        return true;
    }

    //保存单证修改操作
    private boolean saveModify(String cOperate)
    {
        boolean blnReturn = false;
        System.out.println("Start saveModify ...");
        Connection conn = DBConnPool.getConnection();
        try
        {
            conn.setAutoCommit(false);

            if (conn == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "EsDocManageBLS";
                tError.functionName = "saveModify";
                tError.errorMessage = "数据库连接失败";
                this.mErrors.addOneError(tError);
                return false;
            }

            conn.setAutoCommit(false);

            //处理单证主表
            System.out.println("Dealing ES_DOC_MAIN ...");

            ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB(conn);
            tES_DOC_MAINDB.setSchema(tES_DOC_MAINSchema);
            blnReturn = true;
            if (cOperate.equals("DELETE||MAIN")) //删除单证主表数据
            {
                blnReturn = tES_DOC_MAINDB.delete();
            }
            if (cOperate.equals("UPDATE||MAIN")) //更新单证主表数据
            {
                blnReturn = tES_DOC_MAINDB.update();
            }
            if (blnReturn == false)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "EsDocManageBLS";
                tError.functionName = "saveModify";
                tError.errorMessage = "保存数据库ES_DOC_MAIN表的修改出现错误";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //处理单证页表数据
            System.out.println("Dealing ES_DOC_PAGES ...");
            ES_DOC_PAGESDBSet tES_DOC_PAGESDBSet = new ES_DOC_PAGESDBSet();
            tES_DOC_PAGESDBSet.set(tES_DOC_PAGESSet);
            blnReturn = true;
            if(cOperate.equals("DELETE||MAIN")) //删除单证同时删除所有页
            {
              ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
              tES_DOC_PAGESDB.setDocID(tES_DOC_MAINSchema.getDocID());
              blnReturn = tES_DOC_PAGESDB.deleteSQL();
            }
            if (cOperate.equals("DELETE||PAGES")) //删除单证页数据
            {
                blnReturn = tES_DOC_PAGESDBSet.delete();
            }
            if (cOperate.equals("UPDATE||PAGES")) //更新单证页数据
            {
                blnReturn = tES_DOC_PAGESDBSet.update();
            }
            if (blnReturn == false)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "EsDocManageBLS";
                tError.functionName = "saveModify";
                tError.errorMessage = "保存数据库ES_DOC_PAGES表的修改出现错误";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //提交事务
            //conn.rollback();//测试用
            conn.commit();

            conn.close();
        }
        catch (Exception ex)
        {
            System.out.println("Exception in BLS");
            System.out.println("Exception:" + ex.toString());

            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "EsDocManageBLS";
            tError.functionName = "saveModify";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        return true;
    }
    public static void main(String[] args)
    {
        EsDocManageBLS tEsDocManageBLS = new EsDocManageBLS();
    }
}
