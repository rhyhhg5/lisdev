package com.sinosoft.lis.easyscan;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ScanListPrintUI
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    public ScanListPrintUI()
    {
    }

    /**
     * 外部操作的提交方法，调用dealData()进行业务逻辑处理，
     * 并得到处理后的XmlExport对象，返回页面
     * @param cInputData VData：对象，需要：
     * a)	TransferData对象：存储页面查询清单的Sql语句。
     * b)	GlobalInput对象：操作员信息。
     * @param operate String：操作方式，此可为“”
     * @returnXmlExport：成功待打印的清单数据，否则null
     */
    public XmlExport getXmlExport(VData cInputData, String operate)
    {
        ScanListPrintBL bl = new ScanListPrintBL();

        XmlExport tXmlExport = bl.getXmlExport(cInputData, operate);
        if(tXmlExport == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return null;
        }

        return tXmlExport;
    }


    public static void main(String[] args)
    {
        ScanListPrintUI ui = new ScanListPrintUI();
    }
}
