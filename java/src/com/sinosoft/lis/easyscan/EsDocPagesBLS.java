package com.sinosoft.lis.easyscan;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: EasyScan单证索引管理</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Liuqiang
 * @version 1.0
 */
public class EsDocPagesBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
	private String mOperate;
    //输入参数私有变量
    private ES_DOC_PAGESSet tES_DOC_PAGESSet;
    private ES_DOC_PAGESDBSet tES_DOC_PAGESDBSet = new ES_DOC_PAGESDBSet();

    public EsDocPagesBLS()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
		boolean tReturn = false;
		mInputData = ( VData )cInputData.clone() ;
		this.mOperate = cOperate;
		//将操作数据拷贝到本类中
		System.out.println("Start EsDocPagesBLS Submit...");

        if (this.mOperate.equals("UPDATE||PAGES"))
        {
			tReturn = updatePages(cInputData);
        }
		if (tReturn)
		{
			System.out.println(" sucessful");
		}
		else
		{
			System.out.println("Save failed");
		}
		System.out.println("End EsDocPagesBLS Submit...");
        return tReturn;
    }

    //获取返回数据
    public VData getResult()
    {
        return mResult;
    }

    //入参处理
    private boolean getInputData()
    {
        //获取入参
         tES_DOC_PAGESSet = (ES_DOC_PAGESSet) mInputData.get(1);

        return true;
    }

    //保存单证修改操作
       public static void main(String[] args)
    {
        EsDocPagesBLS tEsDocPagesBLS = new EsDocPagesBLS();
    }
	private boolean updatePages(VData mInputData)
		{
			boolean tReturn = true;
			System.out.println("Start Save...");
			Connection conn = null;
		    conn = DBConnPool.getConnection();
			if (conn == null)
			{
				System.out.println("建立连接失败");
				CError tError = new CError();
				tError.moduleName = "EsDocPagesBLS";
				tError.functionName = "updateData";
				tError.errorMessage = "数据库连接失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			try
			{
				conn.setAutoCommit(false);
				System.out.println("Start 保存...");
				ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB(conn);
				tES_DOC_PAGESDBSet.set((ES_DOC_PAGESSet) mInputData.getObjectByObjectName(
						"ES_DOC_PAGESSet", 0));
				if (!tES_DOC_PAGESDB.update())
				{
					// @@错误处理
					this.mErrors.copyAllErrors(tES_DOC_PAGESDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "LAAddressBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "数据保存失败!";
					this.mErrors.addOneError(tError);
					conn.rollback();
					conn.close();
					return false;
				}
				conn.commit();
				conn.close();
			}
			catch (Exception ex)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "LAAddressBLS";
				tError.functionName = "submitData";
				tError.errorMessage = ex.toString();
				this.mErrors.addOneError(tError);
				tReturn = false;
				try
				{
					conn.rollback();
					conn.close();
				}
				catch (Exception e)
				{}
			}
			return tReturn;
    }
}
