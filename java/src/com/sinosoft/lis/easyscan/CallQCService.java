package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CallQCService {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    MMap map = new MMap();

    public CallQCService() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            System.out.println(
                    "----------------------CallQCService Begin----------------------");

            ES_DOC_QC_DEFDB tES_DOC_QC_DEFDB = new ES_DOC_QC_DEFDB();
            ES_DOC_QC_DEFSchema tES_DOC_QC_DEFSchema = new ES_DOC_QC_DEFSchema();

            ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
            if (tES_DOC_MAINSET.size() != 1) {
                System.out.println("2");
                CError tError = new CError();
                tError.moduleName = "CallQCService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "传入数据出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            ES_DOC_MAINSchema tES_DOC_MAINSchema = tES_DOC_MAINSET.get(1).
                    getSchema();
//            tES_DOC_QC_DEFDB.setManageCom(tES_DOC_MAINSchema.getManageCom());
//            tES_DOC_QC_DEFDB.setBussType(tES_DOC_MAINSchema.getBussType());
//            tES_DOC_QC_DEFDB.setSubType(tES_DOC_MAINSchema.getSubType());
//            tES_DOC_QC_DEFDB.setState("1");
//            ES_DOC_QC_DEFSet tES_DOC_QC_DEFSet = tES_DOC_QC_DEFDB.query();
//            if (tES_DOC_QC_DEFSet.size() < 1) {
//                System.out.println("不走质检");
//                return false;
//            }
            //这里是临时用的方法begin
            String cScanOperator = tES_DOC_MAINSchema.getScanOperator();
            String cc=cScanOperator.substring(0, 2);
            System.out.println("用户的前两位是:"+cc);
            if (!cScanOperator.substring(0, 2).equals("ms")) {
                System.out.println("不走质检");
                return false;
            }
            //这里是临时用的方法end
            else {
                System.out.println("需要走质检");
                ES_DOC_QC_MAINDB cES_DOC_QC_MAINDB = new ES_DOC_QC_MAINDB();
                cES_DOC_QC_MAINDB.setDocID(tES_DOC_MAINSchema.getDocID());
                ES_DOC_QC_MAINSet cES_DOC_QC_MAINSet = cES_DOC_QC_MAINDB.query();
                if (cES_DOC_QC_MAINSet.size() > 0) {
                    CallService tCallService = new CallService();
                    if (!tCallService.submitData(cInputData, cOperate, "2")) {
                        mErrors.copyAllErrors(tCallService.mErrors);
                        return false;
                    }
                    map.add((MMap) tCallService.getResult().
                            getObjectByObjectName(
                                    "MMap", 0));
                    mResult.add(map);

                } else {
                    ES_DOC_QC_MAINSchema tES_DOC_QC_MAINSchema = new
                            ES_DOC_QC_MAINSchema();
                    tES_DOC_QC_MAINSchema.setDocID(tES_DOC_MAINSchema.getDocID());
                    tES_DOC_QC_MAINSchema.setDocCode(tES_DOC_MAINSchema.
                            getDocCode());
                    tES_DOC_QC_MAINSchema.setBussType(tES_DOC_MAINSchema.
                            getBussType());
                    tES_DOC_QC_MAINSchema.setSubType(tES_DOC_MAINSchema.
                            getSubType());
                    tES_DOC_QC_MAINSchema.setScanOperator(tES_DOC_MAINSchema.
                            getScanOperator());
                    tES_DOC_QC_MAINSchema.setScanManageCom(tES_DOC_MAINSchema.
                            getManageCom());
                    tES_DOC_QC_MAINSchema.setScanLastApplyDate(PubFun.
                            getCurrentDate());
                    tES_DOC_QC_MAINSchema.setScanLastApplyTime(PubFun.
                            getCurrentTime());
                    tES_DOC_QC_MAINSchema.setQCFlag("0");
                    tES_DOC_QC_MAINSchema.setMakeDate(PubFun.getCurrentDate());
                    tES_DOC_QC_MAINSchema.setMakeTime(PubFun.getCurrentTime());
                    tES_DOC_QC_MAINSchema.setModifyDate(PubFun.getCurrentDate());
                    tES_DOC_QC_MAINSchema.setModifyTime(PubFun.getCurrentTime());

                    map.put(tES_DOC_QC_MAINSchema, "INSERT");
                    mResult.add(map);
                }
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }
}
