package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class QCManageUI {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public QCManageUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            // 数据操作字符串拷贝到本类中
            this.mInputData = (VData) cInputData.clone();
            this.mOperate = cOperate;

            System.out.println("---QCManage BL BEGIN---");
            QCManageBL tQCManageBL = new QCManageBL();
            if (tQCManageBL.submitData(cInputData, mOperate) == false) {
                // @@错误处理
                this.mErrors.copyAllErrors(tQCManageBL.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("---QCManage BL END---");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }
}


