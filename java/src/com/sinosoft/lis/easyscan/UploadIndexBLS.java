package com.sinosoft.lis.easyscan;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;

import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Liuqiang
 * @version 1.0
 */
public class UploadIndexBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private GlobalInput tGI = new GlobalInput();
    private String mRtnCode = "0";
    private String mRtnDesc = "";
    //工作流相关数据
    private ES_DOC_MAINSchema mES_DOC_MAINSchema = new ES_DOC_MAINSchema();

    public UploadIndexBLS()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = true;

        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        tReturn = save();

        if (tReturn)
        {
            System.out.println("UploadIndexBLS:Save Successful!");
        }
        else
        {
            System.out.println("UploadIndexBLS:Save Failed!");
        }

        mResult.clear();
        mResult = mInputData;
        mInputData = null;

        return tReturn;
    }
    public VData getResult()
    {
        return mResult;
    }
    private boolean save()
    {
        boolean tReturn = true;
        boolean bRet = false;
        ES_DOC_MAINSet tES_DOC_MAINSet = (ES_DOC_MAINSet) mInputData.get(0);
        ES_DOC_PAGESSet tES_DOC_PAGESSet = (ES_DOC_PAGESSet) mInputData.get(1);

        System.out.println("Start Update...");

        Connection conn = DBConnPool.getConnection();

        try
        {
            conn.setAutoCommit(false);

            if (conn == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UploadIndexBLS";
                tError.functionName = "Save";
                tError.errorNo = "-10";
                tError.errorMessage = "数据库连接失败";
                this.mErrors.addOneError(tError);

                return false;
            }

            conn.setAutoCommit(false);

            //保存单证主表数据
            System.out.println("Start ES_DOC_MAIN ...");

            if (tES_DOC_MAINSet.size() != 0)
            {
                ES_DOC_MAINSchema tES_DOC_MAINSchema = tES_DOC_MAINSet.get(1);
                //WF data prepare
                mES_DOC_MAINSchema = tES_DOC_MAINSchema;
                ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB(conn);
                tES_DOC_MAINDB.setSchema(tES_DOC_MAINSchema);

                if (tES_DOC_MAINSchema.getDocFlag().trim().equals("0"))
                {
                    tES_DOC_MAINDB.setDocFlag("1");
                    bRet = tES_DOC_MAINDB.insert();
                }
                else //flage="1"表示是中心存在的数据
                {
                    //设置es_doc_main的中心处理的数据以避免被客户端上载的数据覆盖
                    ES_DOC_MAINDB queryES_DOC_MAINDB = new ES_DOC_MAINDB();
                    queryES_DOC_MAINDB.setDocCode(tES_DOC_MAINSchema
                        .getDocCode());
                    ES_DOC_MAINSet queryES_DOC_MAINSet = queryES_DOC_MAINDB
                        .query();
                    if (queryES_DOC_MAINSet.size() != 1)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UploadIndexBLS";
                        tError.functionName = "save";
                        tError.errorNo = "-1";
                        tError.errorMessage = "查询数据库ES_DOC_MAIN表出现错误";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    tES_DOC_MAINDB.setDocFlag(queryES_DOC_MAINSet.get(1)
                                                                         .getDocFlag());
                    tES_DOC_MAINDB.setMakeDate(queryES_DOC_MAINSet.get(1)
                                                                        .getMakeDate());
                    tES_DOC_MAINDB.setScanOperator(queryES_DOC_MAINSet.get(1)
                                                                      .getScanOperator());
                    tES_DOC_MAINDB.setMakeDate(queryES_DOC_MAINSet.get(
                            1).getMakeDate());
                    tES_DOC_MAINDB.setMakeTime(queryES_DOC_MAINSet.get(
                            1).getMakeTime());

                    //Update数据库数据
                    bRet = tES_DOC_MAINDB.update();
                }

                if (!bRet)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "UploadIndexBLS";
                    tError.functionName = "Save";
                    tError.errorNo = "-2";
                    tError.errorMessage = "保存单证主表(ES_DOC_MAIN)数据失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();

                    return false;
                }
            }

            System.out.println("Start ES_DOC_Pages ...");

            if (tES_DOC_PAGESSet.size() != 0)
            {
                ES_DOC_PAGESDBSet tES_DOC_PAGESDBSet = new ES_DOC_PAGESDBSet(conn);

                for (int i = 1; i <= tES_DOC_PAGESSet.size(); i++)
                {
                    tES_DOC_PAGESSet.get(i).setPageFlag("1");
                }

                tES_DOC_PAGESDBSet.set(tES_DOC_PAGESSet);

                if (!tES_DOC_PAGESDBSet.insert())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "UploadIndexBLS";
                    tError.functionName = "Save";
                    tError.errorNo = "-2";
                    tError.errorMessage = "保存单证页表(ES_DOC_PAGES)数据失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();

                    return false;
                }
            }

            //提交事务
            conn.commit();

            //conn.rollback();//测试用
            conn.close();
        }
        catch (Exception ex)
        {
            System.out.println("Exception in BLS");
            System.out.println("Exception:" + ex.toString());

            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UploadIndexBLS";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {
            }

            tReturn = false;
        }
        //Lis Upgrade: add by HYQ 20041222
        //Make first WF activity
        if (tReturn)
        {
            System.out.println("----------------------WorkFlow Begin----------------------");
            TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
            VData tVData = new VData();
            TransferData tTransferData = new TransferData();

            //prepare data of lwfieldmap(table)
            tGI.Operator = mES_DOC_MAINSchema.getScanOperator();
            tGI.ManageCom = mES_DOC_MAINSchema.getManageCom();
            tTransferData.setNameAndValue("PrtNo",mES_DOC_MAINSchema.getDocCode());
            tTransferData.setNameAndValue("InputDate",mES_DOC_MAINSchema.getMakeDate());
            tTransferData.setNameAndValue("Operator",mES_DOC_MAINSchema.getScanOperator());
            tTransferData.setNameAndValue("ManageCom",mES_DOC_MAINSchema.getManageCom());
            System.out.println(mES_DOC_MAINSchema.getDocCode());
            System.out.println(mES_DOC_MAINSchema.getMakeDate());
            System.out.println(mES_DOC_MAINSchema.getScanOperator());
            System.out.println(mES_DOC_MAINSchema.getManageCom());

            tVData.add(tGI);
            tVData.add(tTransferData);
            try
            {
                if (tTbWorkFlowUI.submitData(tVData, "7899999999"))   //生成扫描件录入节点
                {
                    System.out.println("First WorkFlow Activity successed!");
                }
                else
                {

                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "UploadIndexBLS";
                    tError.functionName = "Save";
                    tError.errorNo = "-99";
                    tError.errorMessage = "扫描录入工作流节点生成失败!";
                    this.mErrors.addOneError(tError);

                    tReturn = false;
                }
            }
            catch (Exception ex)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UploadIndexBLS";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = ex.toString();
                this.mErrors.addOneError(tError);

                tReturn = false;
            }
            System.out.println("----------------------WorkFlow End----------------------");
        }
        return tReturn;
    }
    public static void main(String[] args)
    {
        UploadIndexBLS uploadIndexBLS1 = new UploadIndexBLS();
    }
}
