package com.sinosoft.lis.easyscan;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ProposalDownloadUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData;
    private String mOperate;

    public boolean submitData(VData cInputData, String cOperate) throws
            Exception {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if(!tProposalDownloadBL.submitData(mInputData, mOperate)){
            if (tProposalDownloadBL.mErrors.needDealError())
                {
            mErrors.copyAllErrors(tProposalDownloadBL.mErrors);
            System.out.println("mEsPicDownloadBL.mErrors"+tProposalDownloadBL.mErrors);
                }else{
                    buildError("sbumitData", "tZhuF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            return false;
        }
        return true;
    }
    private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "LCContF1PUI";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}

}
