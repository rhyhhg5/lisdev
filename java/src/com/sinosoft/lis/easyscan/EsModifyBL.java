package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EsModifyBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 数据操作字符串 */
    private String mIssueDocID;

    /** 数据操作字符串 */
    private boolean mIssueDocIDFlag = false;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private Es_IssueDocSchema mEs_IssueDocSchema = new Es_IssueDocSchema();

    MMap map = new MMap();

    private GlobalInput mGlobalInput = new GlobalInput();

    private boolean mbAutoPassFlag = false;

    public EsModifyBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start submitData");
        try
        {
            this.mInputData = (VData) cInputData.clone();
            this.mOperate = cOperate;
            if (!this.mOperate.equals("Apply")
                    && !this.mOperate.equals("ApplyPass")
                    && !this.mOperate.equals("ApplyNoPass")
                    && !this.mOperate.equals("ApplyPass|Auto"))
            {
                buildError("checkData", "不支持的字符串" + mOperate + "");
                return false;
            }
            if (this.getInputData(cInputData) == false)
            {
                return false;
            }
            System.out.println("---End getInputData---");
            if (this.checkData() == false)
            {
                return false;
            }
            System.out.println("---End checkData---");
            if (this.dealData() == false)
            {
                return false;
            }
            System.out.println("---End dealData---");
            mResult.add(map);
            PubSubmit pubsubmit = new PubSubmit();
            if (!pubsubmit.submitData(mResult, ""))
            {
                mErrors.copyAllErrors(pubsubmit.mErrors);
                return false;
            }
            System.out.println("---End pubsubmit---");
        }
        catch (Exception ex)
        {

        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "EsAssessBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getInputData(VData cInputData)
    {
        this.mEs_IssueDocSchema.setSchema((Es_IssueDocSchema) cInputData
                .getObjectByObjectName("Es_IssueDocSchema", 0));
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (this.mEs_IssueDocSchema == null)
        {
            buildError("getInputData", "没有得到schema");
            return false;
        }
        if ("ApplyPass|Auto".equals(this.mOperate))
        {
            this.mOperate = "ApplyPass";
            this.mbAutoPassFlag = true;
        }
        return true;
    }

    public boolean checkData()
    {
        //通用校验
        //是否有该类型的审核人this.mEs_IssueDocSchema;
        String tSubType = this.mEs_IssueDocSchema.getSubType();

        // 如果不是投保件（TB），不进行校验。
        if (tSubType != null && !"TB".equals(tSubType.substring(0, 2)))
        {
            String tStrSql = " select IssueDocId from Es_IssueDoc where BussNo = '"
                    + this.mEs_IssueDocSchema.getBussNo()
                    + "' and BussType = '"
                    + this.mEs_IssueDocSchema.getBussType()
                    + "' and SubType = '"
                    + this.mEs_IssueDocSchema.getSubType() + "' ";
            this.mIssueDocID = new ExeSQL().getOneValue(tStrSql);
            if (!"".equals(this.mIssueDocID))
            {
                this.mIssueDocIDFlag = true;
            }
            return true;
        }
        // ------------------------------------

        LDScanCheckUserDB tLDScanCheckUserDB = new LDScanCheckUserDB();
        tLDScanCheckUserDB.setSubType(tSubType);
        LDScanCheckUserSet tLDScanCheckUserSet = tLDScanCheckUserDB.query();
        if (tLDScanCheckUserSet.size() < 1)
        {
            buildError("getInputData", "还没有该类型的扫描件审核员,请先添加审核员!");
            return false;
        }
        if (this.mOperate.equals("Apply"))
        { //如果是申请
            //判断该审核员有没有权限
            Es_IssueDocDB cEs_IssueDocDB = new Es_IssueDocDB();
            cEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
            Es_IssueDocSet cEs_IssueDocSet = cEs_IssueDocDB
                    .executeQuery("select * from Es_IssueDoc where BussNo='"
                            + this.mEs_IssueDocSchema.getBussNo()
                            + "' and BussType='"
                            + this.mEs_IssueDocSchema.getBussType()
                            + "' and SubType='"
                            + this.mEs_IssueDocSchema.getSubType()
                            + "' and Status='1' and StateFlag ='1' with ur");
            if (cEs_IssueDocSet.size() > 1)
            {
                buildError("checkData", "在扫描修改池中,已经有号码为: "
                        + this.mEs_IssueDocSchema.getBussNo() + "的单证处于审核状态!");
                return false;
            }
            Es_IssueDocSet tEs_IssueDocSet = cEs_IssueDocDB
                    .executeQuery("select * from Es_IssueDoc where BussNo='"
                            + this.mEs_IssueDocSchema.getBussNo()
                            + "' and BussType='"
                            + this.mEs_IssueDocSchema.getBussType()
                            + "' and SubType='"
                            + this.mEs_IssueDocSchema.getSubType()
                            + "' and Status='1' and StateFlag ='3' with ur");
            if (tEs_IssueDocSet.size() > 0)
            {
                if (tEs_IssueDocSet.size() != 1)
                {
                    buildError("checkData", "在扫描修改池中,号码为: "
                            + this.mEs_IssueDocSchema.getBussNo() + "的单证的数目为: "
                            + tEs_IssueDocSet.size() + "而不是唯一!");
                    return false;
                }
                this.mIssueDocIDFlag = true;
                this.mIssueDocID = tEs_IssueDocSet.get(1).getIssueDocID();
            }

        }
        if (this.mOperate.equals("ApplyPass"))
        {
            // 如果是自动通过，不进行权限校验。
            if (!mbAutoPassFlag)
            {
                //如果是审核通过
                //判断该审核员有没有权限
                LDScanCheckUserDB cLDScanCheckUserDB = new LDScanCheckUserDB();
                cLDScanCheckUserDB.setSubType(this.mEs_IssueDocSchema
                        .getSubType());
                cLDScanCheckUserDB.setUserCode(mGlobalInput.Operator);
                if (!cLDScanCheckUserDB.getInfo())
                {
                    buildError("checkData", "您没有审核"
                            + this.mEs_IssueDocSchema.getSubType() + "类型的权限!");
                    return false;
                }
            }
            // -------------------------------

            Es_IssueDocDB cEs_IssueDocDB = new Es_IssueDocDB();
            cEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
            Es_IssueDocSet cEs_IssueDocSet = cEs_IssueDocDB
                    .executeQuery("select * from Es_IssueDoc where BussNo='"
                            + this.mEs_IssueDocSchema.getBussNo()
                            + "' and BussType='"
                            + this.mEs_IssueDocSchema.getBussType()
                            + "' and SubType='"
                            + this.mEs_IssueDocSchema.getSubType()
                            + "' and Status='1' and StateFlag in ('1','3') with ur");
            if (cEs_IssueDocSet.size() != 1)
            {
                buildError("checkData", "在扫描修改池中,号码为: "
                        + this.mEs_IssueDocSchema.getBussNo() + "的单证的数目为"
                        + cEs_IssueDocSet.size() + "而不是唯一!");
                return false;
            }
            this.mIssueDocID = cEs_IssueDocSet.get(1).getIssueDocID();
        }
        if (this.mOperate.equals("ApplyNoPass"))
        { //如果是审核不通过
            //判断该审核员有没有权限
            LDScanCheckUserDB cLDScanCheckUserDB = new LDScanCheckUserDB();
            cLDScanCheckUserDB.setSubType(this.mEs_IssueDocSchema.getSubType());
            cLDScanCheckUserDB.setUserCode(mGlobalInput.Operator);
            if (!cLDScanCheckUserDB.getInfo())
            {
                buildError("getInputData", "您没有审核"
                        + this.mEs_IssueDocSchema.getSubType() + "类型的权限!");
                return false;
            }
            Es_IssueDocDB cEs_IssueDocDB = new Es_IssueDocDB();
            cEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
            Es_IssueDocSet cEs_IssueDocSet = cEs_IssueDocDB
                    .executeQuery("select * from Es_IssueDoc where BussNo='"
                            + this.mEs_IssueDocSchema.getBussNo()
                            + "' and BussType='"
                            + this.mEs_IssueDocSchema.getBussType()
                            + "' and SubType='"
                            + this.mEs_IssueDocSchema.getSubType()
                            + "' and Status='1' and StateFlag='1' with ur");
            if (cEs_IssueDocSet.size() != 1)
            {
                buildError("checkData", "在扫描修改池中,号码为: "
                        + this.mEs_IssueDocSchema.getBussNo() + "的单证的数目为"
                        + cEs_IssueDocSet.size() + "而不是唯一!");
                return false;
            }
            this.mIssueDocID = cEs_IssueDocSet.get(1).getIssueDocID();
        }
        return true;
    }

    public boolean dealData()
    {
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        String tBussNoType = "";
        ES_DOC_RELATIONDB cCES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
        cCES_DOC_RELATIONDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
        cCES_DOC_RELATIONDB.setBussType(this.mEs_IssueDocSchema.getBussType());
        cCES_DOC_RELATIONDB.setSubType(this.mEs_IssueDocSchema.getSubType());
        ES_DOC_RELATIONSet cES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();
        cES_DOC_RELATIONSet = cCES_DOC_RELATIONDB.query();
        if (cES_DOC_RELATIONSet.size() > 0)
        {
            tBussNoType = cES_DOC_RELATIONSet.get(1).getBussNoType();
        }
        else
        {
            buildError("dealData", "在关联表中找不到号码为:"
                    + this.mEs_IssueDocSchema.getBussNo() + "的扫描件!");
            return false;
        }

        if (this.mOperate.equals("Apply"))
        { //如果是申请
            //
            if (mIssueDocIDFlag == false)
            {
                String tCount1;
                int i;
                Es_IssueDocSchema cEs_IssueDocSchema = new Es_IssueDocSchema();
                String tSQL = "select MAX(to_number(IssueDocID)) from Es_IssueDoc where BussNo is not null";
                ExeSQL tExeSQL = new ExeSQL();
                tCount1 = tExeSQL.getOneValue(tSQL);
                if (tCount1 != null && !tCount1.equals(""))
                {
                    Integer tInteger = new Integer(tCount1);
                    i = tInteger.intValue();
                    i = i + 1;
                }
                else
                {
                    i = 1;
                }
                cEs_IssueDocSchema.setIssueDocID(String.valueOf(i));
                cEs_IssueDocSchema.setMakeDate(cDate);
                cEs_IssueDocSchema.setMakeTime(cTime);
                cEs_IssueDocSchema.setModifyDate(cDate);
                cEs_IssueDocSchema.setModifyTime(cTime);
                cEs_IssueDocSchema
                        .setPromptOperator(this.mGlobalInput.Operator);
                cEs_IssueDocSchema.setBussNo(this.mEs_IssueDocSchema
                        .getBussNo());
                cEs_IssueDocSchema.setBussType(this.mEs_IssueDocSchema
                        .getBussType());
                cEs_IssueDocSchema.setBussNoType(tBussNoType);
                cEs_IssueDocSchema.setSubType(this.mEs_IssueDocSchema
                        .getSubType());
                cEs_IssueDocSchema.setIssueDesc(this.mEs_IssueDocSchema
                        .getIssueDesc());
                cEs_IssueDocSchema.setStatus("1");
                cEs_IssueDocSchema.setResult(null);
                cEs_IssueDocSchema.setStateFlag("1");
                cEs_IssueDocSchema.setIssueType(this.mEs_IssueDocSchema.getIssueType());
                this.map.put(cEs_IssueDocSchema, "INSERT");
            }
            else
            {
                Es_IssueDocDB tEs_IssueDocDB = new Es_IssueDocDB();
                tEs_IssueDocDB.setIssueDocID(this.mIssueDocID);
                tEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
                tEs_IssueDocDB.setBussNoType(tBussNoType);
                tEs_IssueDocDB.setSubType(this.mEs_IssueDocSchema.getSubType());
                if (!tEs_IssueDocDB.getInfo())
                {
                    buildError("dealData", "在Es_IssueDoc表中找不到号码为:"
                            + this.mIssueDocID + "的记录!");
                    return false;
                }
                Es_IssueDocSchema tEs_IssueDocSchema = tEs_IssueDocDB
                        .getSchema();
                tEs_IssueDocSchema.setStatus("1");
                tEs_IssueDocSchema.setStateFlag("1");
                tEs_IssueDocSchema.setResult(null);
                tEs_IssueDocSchema.setIssueDesc(this.mEs_IssueDocSchema
                        .getIssueDesc());
                tEs_IssueDocSchema
                        .setPromptOperator(this.mGlobalInput.Operator);
                tEs_IssueDocSchema.setModifyDate(cDate);
                tEs_IssueDocSchema.setModifyTime(cTime);
                this.map.put(tEs_IssueDocSchema, "UPDATE");
            }
        }
        if (this.mOperate.equals("ApplyPass"))
        { //如果是审核通过
            //
            Es_IssueDocDB tEs_IssueDocDB = new Es_IssueDocDB();
            tEs_IssueDocDB.setIssueDocID(this.mIssueDocID);
            tEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
            tEs_IssueDocDB.setBussNoType(tBussNoType);
            tEs_IssueDocDB.setSubType(this.mEs_IssueDocSchema.getSubType());

            if (!tEs_IssueDocDB.getInfo())
            {
                buildError("dealData", "在Es_IssueDoc表中找不到号码为:"
                        + this.mIssueDocID + "的记录!");
                return false;
            }
            Es_IssueDocSchema tEs_IssueDocSchema = tEs_IssueDocDB.getSchema();
            tEs_IssueDocSchema.setStatus("0");
            tEs_IssueDocSchema.setStateFlag("2");
            tEs_IssueDocSchema.setCheckContent(this.mEs_IssueDocSchema
                    .getCheckContent());
            tEs_IssueDocSchema.setCheckOperator(this.mGlobalInput.Operator);
            tEs_IssueDocSchema.setCheckDate(cDate);
            tEs_IssueDocSchema.setCheckTime(cTime);
            this.map.put(tEs_IssueDocSchema, "UPDATE");
        }
        if (this.mOperate.equals("ApplyNoPass"))
        { //如果是审核不通过
            //
            Es_IssueDocDB tEs_IssueDocDB = new Es_IssueDocDB();
            tEs_IssueDocDB.setIssueDocID(this.mIssueDocID);
            tEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
            tEs_IssueDocDB.setBussNoType(tBussNoType);
            tEs_IssueDocDB.setSubType(this.mEs_IssueDocSchema.getSubType());

            if (!tEs_IssueDocDB.getInfo())
            {
                buildError("dealData", "在Es_IssueDoc表中找不到号码为:"
                        + this.mIssueDocID + "的记录!");
                return false;
            }
            Es_IssueDocSchema tEs_IssueDocSchema = tEs_IssueDocDB.getSchema();
            tEs_IssueDocSchema.setStatus("1");
            tEs_IssueDocSchema.setStateFlag("3");
            tEs_IssueDocSchema.setCheckContent(this.mEs_IssueDocSchema
                    .getCheckContent());
            tEs_IssueDocSchema.setCheckOperator(this.mGlobalInput.Operator);
            tEs_IssueDocSchema.setCheckDate(cDate);
            tEs_IssueDocSchema.setCheckTime(cTime);
            this.map.put(tEs_IssueDocSchema, "UPDATE");
        }
        return true;
    }

}
