package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: EasyScan单证索引管理</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Liuqiang
 * @version 1.0
 */
public class EsDocManageBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private GlobalInput tGI = new GlobalInput();
    String mRtnCode = "0";
    String mRtnDesc = "";

    public EsDocManageBL()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = true;

        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        EsDocManageBLS tEsDocManageBLS = new EsDocManageBLS();
        tEsDocManageBLS.submitData(mInputData, cOperate);

        //如果有需要处理的错误，则返回
        if (tEsDocManageBLS.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tEsDocManageBLS.mErrors);
            tReturn = false;
        }

        mResult.clear();
        mResult = tEsDocManageBLS.getResult();

        mInputData = null;

        return tReturn;
    }

    public VData getResult()
    {
        return mResult;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate)
    {
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        return true;
    }

    public static void main(String[] args)
    {
        EsDocManageBL tEsDocManageBL = new EsDocManageBL();
    }
}
