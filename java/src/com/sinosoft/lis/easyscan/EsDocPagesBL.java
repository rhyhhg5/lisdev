package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: EasyScan单证索引管理</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Liuqiang
 * @version 1.0
 */
public class EsDocPagesBL
{
		/** 错误处理类，每个需要错误处理的类中都放置该类 */
		public CErrors mErrors = new CErrors();
		private VData mResult = new VData();
		/** 往后面传输数据的容器 */
		private VData mInputData = new VData();
		private MMap map = new MMap();
		/** 全局数据 */
		private GlobalInput mGlobalInput = new GlobalInput();
		/** 数据操作字符串 */
		private String mOperate;
		private String tNo;
		/** 业务处理相关变量 */
		private ES_DOC_PAGESSet mES_DOC_PAGESSet=new ES_DOC_PAGESSet();
public EsDocPagesBL()
    {
    }
	public static void main(String[] args) {
  }
  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
     */
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("******after get :bl***");
		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHUnitContraBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LHUnitContraBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("******after dealData :bl***");
		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		   else {
			System.out.println("Start EsDocPagesBL Submit...");
			EsDocPagesBLS tEsDocPagesBLS = new EsDocPagesBLS();
			//tOLHUnitContraBLS.submitData(mInputData,mOperate);
			System.out.println("End EsDocPagesBL Submit...");
			//如果有需要处理的错误，则返回
			if (tEsDocPagesBLS.mErrors.needDealError()) {
				// @@错误处理
				this.mErrors.copyAllErrors(tEsDocPagesBLS.mErrors);
				CError tError = new CError();
				tError.moduleName = "LHUnitContraBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		PubSubmit tPubSubmit = new PubSubmit();
		tPubSubmit.submitData(mInputData, mOperate);
		//this.mResult=mInputData;
		mInputData = null;
		return true;
    }

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (this.mOperate.equals("UPDATE||PAGES")) {

			if (mES_DOC_PAGESSet != null && mES_DOC_PAGESSet.size() > 0) {

			 for (int i = 1; i <= mES_DOC_PAGESSet.size(); i++) {

				 mES_DOC_PAGESSet.get(i).setOperator(mGlobalInput.
						 Operator);

				 mES_DOC_PAGESSet.get(i).setModifyDate(PubFun.
						 getCurrentDate());
				 mES_DOC_PAGESSet.get(i).setModifyTime(PubFun.
						 getCurrentTime());

			 }
			map.put(mES_DOC_PAGESSet, "UPDATE");
		 }

	}


		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean updateData() {
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean deleteData() {
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		this.mES_DOC_PAGESSet.set((ES_DOC_PAGESSet) cInputData.
										getObjectByObjectName(
												"ES_DOC_PAGESSet", 0));

		this.mGlobalInput.setSchema((GlobalInput) cInputData.
									getObjectByObjectName("GlobalInput", 0));
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			this.mInputData.clear();

			mInputData.add(map);
			mResult.clear();
			mResult.add(this.mES_DOC_PAGESSet);

		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHCustomTestBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}
}
