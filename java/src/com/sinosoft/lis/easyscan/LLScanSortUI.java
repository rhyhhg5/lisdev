package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLScanSortUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存放查询结果 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    //
    private LLScanSortSchema mLLScanSortSchema = new LLScanSortSchema();
    private LLScanSortSet mLLScanSortSet = new LLScanSortSet();

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
            return false;
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        LLScanSortBL tLLScanSortBL = new LLScanSortBL();
        System.out.println("Start LLScanSort UI Submit...");
        tLLScanSortBL.submitData(mInputData, mOperate);
        System.out.println("End LLScanSort UI Submit...");
        //如果有需要处理的错误，则返回
        if (tLLScanSortBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLScanSortBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLLScanSortUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLLScanSortSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLScanSortUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLLScanSortSet.set((LLScanSortSet) cInputData.
                                getObjectByObjectName(
                                        "LLScanSortSet", 0));
        if (mLLScanSortSet == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLScanSortUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "picch";
        tG.ManageCom = "86110000";
        LLScanSortSchema tLLScanSortSchema = new LLScanSortSchema();
        tLLScanSortSchema.setPageID("173");
        tLLScanSortSchema.setDocID("69");
        tLLScanSortSchema.setPageCode("1");
        tLLScanSortSchema.setPageName("F173");
        tLLScanSortSchema.setPageFlag("1");
        tLLScanSortSchema.setSortType("00");
        tLLScanSortSchema.setSortPage("01");
        tLLScanSortSchema.setSortNo("1");
        LLScanSortSet tLLScanSortSet = new LLScanSortSet();
        tLLScanSortSet.add(tLLScanSortSchema);

        VData tVData = new VData();
        tVData.add(tLLScanSortSet);
        tVData.add(tG);
        LLScanSortUI tLLScanSortUI = new LLScanSortUI();
        tLLScanSortUI.submitData(tVData, "INSERT");

    }
}
