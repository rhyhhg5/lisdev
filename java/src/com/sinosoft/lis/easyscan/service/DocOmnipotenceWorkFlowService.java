package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 投保单证上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;

public class DocOmnipotenceWorkFlowService implements EasyScanService
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 过程ID */
    private String mProcessID = null;

    /** 活动ID */
    private String mActivityID = null;

    public DocOmnipotenceWorkFlowService()
    {
    }

    /**
     * 生成新单录入工作流节点，扫描时自动调用。
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("-----DocOmnipotenceWorkFlowService Begin-------");
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
        GlobalInput tGlobalInput = new GlobalInput();
        /** 全局变量 */
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";

        /**得到具体信息**/

        ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
        if (tES_DOC_MAINSET.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "DocOmnipotenceWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = "传入数据出错!";
            return false;
        }

        ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema) tES_DOC_MAINSET
                .get(1);
        if (tES_DOC_MAINSchema.getDocCode() == null
                || (tES_DOC_MAINSchema.getDocCode().length() != 12 && tES_DOC_MAINSchema.getDocCode().length() != 13))
        {
            mErrors.addOneError("印刷号必须为12位");
            return false;
        }

        if (!checkData(tES_DOC_MAINSchema))
        {
            return false;
        }

        tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode());
        tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema
                .getMakeDate());
        tTransferData.setNameAndValue("ScanOperator", tES_DOC_MAINSchema
                .getScanOperator());
        tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema
                .getManageCom());

        tVData.add(tGlobalInput);
        tVData.add(tTransferData);
        try
        {
            //创建工作流
            ActivityOperator tActivityOpertor = new ActivityOperator();
            if (!createID(tES_DOC_MAINSchema.getSubType()))
            {
                CError tError = new CError();
                tError.moduleName = "DocOmnipotenceWorkFlowService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "获取单证细类失败!";
                return false;
            }
            if (tActivityOpertor.CreateStartMission(mProcessID, mActivityID,
                    tVData))
            {
                mResult = tActivityOpertor.getResult();
            }

            else
            {
                CError tError = new CError();
                tError.moduleName = "DocOmnipotenceWorkFlowService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "扫描录入工作流节点生成失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
            tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1)
                    .getBussType());
            tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1)
                    .getSubType());
            tES_DOC_RELATIONSchema.setRelaFlag("0");
            tES_DOC_RELATIONSchema.setBussNoType("11");
            tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1)
                    .getDocCode());
            tES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(1)
                    .getDocCode());
            if (tES_DOC_RELATIONSchema == null)
            {
                CError tError = new CError();
                tError.moduleName = "DocTBIntlUploadRelationServiceService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "数据生成出错!";
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "DocOmnipotenceWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
        }

        //DocTBWorkFlowService service = new DocTBWorkFlowService();
        //String archiveNo = service.createArchiveNo(tES_DOC_MAINSchema.getManageCom()); //归档号
        String archiveNo = createArchiveNo(tES_DOC_MAINSchema.getManageCom());
        if (archiveNo == null)
        {
            //mErrors.copyAllErrors(service.mErrors);
            return false;
        }

        // ES_DOC_MAINSchema aES_DOC_MAINSchema = tES_DOC_MAINSchema.getSchema();
        tES_DOC_MAINSchema.setArchiveNo(archiveNo);
        tES_DOC_MAINSchema.setState("01");

        MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        //map.put(aES_DOC_MAINSchema, SysConst.UPDATE);//
        map.put(tES_DOC_RELATIONSchema, SysConst.INSERT);

        //增加 DocCode 的锁定    2008-10-06
        MMap mMap = lockTable(tES_DOC_MAINSchema.getDocCode(), tGlobalInput);
        if (mMap != null)
            map.add(mMap);
        else
            return false;

        System.out.println("--------WorkFlow End-----------");
        return true;
    }

    /**
     * checkData
     *
     * @param tES_DOC_MAINSchema ES_DOC_MAINSchema
     * @return boolean
     */
    private boolean checkData(ES_DOC_MAINSchema tES_DOC_MAINSchema)
    {
        String sql = "select 1 from ES_DOC_MAIN "
                + "where DocCode = '"
                + tES_DOC_MAINSchema.getDocCode()
                + "' " //印刷号已扫描
                + "union all " + "select 1 from LWMission "
                + "where MissionProp1 = '" + tES_DOC_MAINSchema.getDocCode()
                + "' " + "   or MissionProp2 = '"
                + tES_DOC_MAINSchema.getDocCode()
                + "' " //印刷号已生成工作流
                + "union all " + "select 1 from LCCont " + "where PrtNo = '"
                + tES_DOC_MAINSchema.getDocCode() + "' "
                + " union all select 1 from lbcont where prtno = '"
                + tES_DOC_MAINSchema.getDocCode() + "' "; //印刷号已生成保单
        String temp = new ExeSQL().getOneValue(sql);
        if (temp != null && !temp.equals("") && !temp.equals("null"))
        {
            mErrors.addOneError("印刷号已扫描或已录入保单，不能再扫描");
            return false;
        }

        return true;
    }

    //获取单证细类，生成工作流 过程ID和活动ID
    private boolean createID(String aSubType)
    {
        if (aSubType.equals("TB04"))
        {
            mProcessID = "0000000009";
            mActivityID = "0000009001";
        }
        else if (aSubType.equals("TB05"))
        {
            mProcessID = "0000000007";
            mActivityID = "0000007999";
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "DocOmnipotenceWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = "获取单证细类失败!";
            return false;
        }
        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        String sql = "select max(DocID) from ES_DOC_MAIN ";
        String maxDocID = new ExeSQL().getOneValue(sql);

        ES_DOC_MAINDB db = new ES_DOC_MAINDB();
        db.setDocID(maxDocID);
        db.getInfo();
        ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
        tES_DOC_MAINSet.add(db.getSchema());

        VData d = new VData();
        d.add(tES_DOC_MAINSet);

        DocOmnipotenceWorkFlowService bl = new DocOmnipotenceWorkFlowService();
        if (!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }

    }

    /**
     * 锁定扫描上载
     * @param LockNoKey
     * @param tGlobalInput
     * @return
     */
    private MMap lockTable(String lockNoKey, GlobalInput globalInput)
    {
        MMap map = null;

        //扫描上载类型：“SI”
        String tLockNoType = "SI";

        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        map = tLockTableActionBL.getSubmitMap(tVData, null);
        if (map == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return map;
    }

    /**
     * 生成归档号es_doc_main.ArchiveNo
     * @return String
     */
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "Y" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

    /**
     * 获取处理后的数据集合
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
