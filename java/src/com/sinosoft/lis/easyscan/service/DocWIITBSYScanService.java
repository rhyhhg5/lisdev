/**
 * 2011-1-26
 */
package com.sinosoft.lis.easyscan.service;

import com.sinosoft.lis.easyscan.EasyScanService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;

/**
 * @author LY
 *
 */
public class DocWIITBSYScanService implements EasyScanService
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocWIITBSYScanService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            ES_DOC_MAINSchema tScanMainInfo = null;

            // 获取扫描件信息
            ES_DOC_MAINSet tEDMScanInfoSet = (ES_DOC_MAINSet) cInputData.getObjectByObjectName("ES_DOC_MAINSet", 0);
            if (tEDMScanInfoSet.size() != 1)
            {
                buildError("submitData", "影印件主表信息获取失败。");
                return false;
            }
            tScanMainInfo = tEDMScanInfoSet.get(1);
            // --------------------

            // 处理归档号
            String archiveNo = createArchiveNo(tScanMainInfo.getManageCom());
            if (archiveNo == null)
            {
                return false;
            }
            tScanMainInfo.setArchiveNo(archiveNo);

            System.out.println("设置归档号：" + tScanMainInfo.getArchiveNo());
            // --------------------

            String tPrtNo = tScanMainInfo.getDocCode();
            // 校验印刷号是否已使用            
            if (!chkCertCode(tPrtNo))
            {
                buildError("submitData", "该印刷号已扫描。");
                return false;
            }
            // --------------------

            // 创建工作流起始节点
            //            if (!createMissionFirstNode(tScanMainInfo))
            //            {
            //                return false;
            //            }
            // --------------------

            // 对补扫进行锁定，时效为5分钟。
            MMap tTmpMap = null;
            tTmpMap = lockCont(tPrtNo);
            if (tTmpMap == null)
            {
                return false;
            }
            mResult.add(tTmpMap);
            // -------------------------------

        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "团体工伤险扫描上传业务出现异常。");
            return false;
        }

        return true;
    }

    private boolean createMissionFirstNode(ES_DOC_MAINSchema cScanMainInfo)
    {
        VData tVData = new VData();

        // 参数要素
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PrtNo", cScanMainInfo.getDocCode());
        tTransferData.setNameAndValue("ManageCom", cScanMainInfo.getManageCom());
        tTransferData.setNameAndValue("ScanDate", cScanMainInfo.getMakeDate());
        tTransferData.setNameAndValue("Operator", cScanMainInfo.getScanOperator());

        tVData.add(tTransferData);
        // --------------------

        // 封装公共变量参数
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = cScanMainInfo.getManageCom();
        tGI.Operator = cScanMainInfo.getScanOperator();
        tVData.add(tGI);
        // --------------------

        try
        {
            ActivityOperator tActivityOpertor = new ActivityOperator();
            if (tActivityOpertor.CreateStartMission("0000000012", "0000012001", tVData))
            {
                mResult = tActivityOpertor.getResult();
            }
            else
            {
                buildError("createMissionFirstNode", "创建起始工作流节点失败。");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("createMissionFirstNode", "创建起始工作流节点出现异常错误。");
            return false;
        }

        return true;
    }

    private boolean chkCertCode(String cCertCode)
    {
        String tStrSql = " select 1 from Es_Doc_Main where DocCode = '" + cCertCode + "' and SubType = 'TB09' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
            return false;

        return true;
    }

    /**
     * 锁定动作。
     * @param 
     * @return
     */
    private MMap lockCont(String cPrtNo)
    {
        MMap tMMap = null;

        // 团体工伤险扫描标志：“G0”
        String tLockNoType = "G0";
        // -----------------------

        // 锁定有效时间
        String tAIS = "60";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cPrtNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(new GlobalInput());
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            buildError("lockCont", "保单锁定失败。");
            return null;
        }

        return tMMap;
    }

    /**
     * 
     * @return String
     */
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7) + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "J" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);
        CError cError = new CError();
        cError.moduleName = "DocWIITBScanService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

}
