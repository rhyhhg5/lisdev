package com.sinosoft.lis.easyscan.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.easyscan.EasyScanService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.LockTableSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;

public class DocSYWorkFlowService implements EasyScanService{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();


    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out
                .println("----------------------WorkFlow Begin----------------------");
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        GlobalInput tGlobalInput = new GlobalInput();
        /** 全局变量 */
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";

        /**得到具体信息**/

        ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
        if (tES_DOC_MAINSET.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = "传入数据出错!";
            return false;
        }

        ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema) tES_DOC_MAINSET
                .get(1);

        //prepare data of lwfieldmap(table)
        //tTransferData.setNameAndValue("PrtNo","1234");// ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
        //tTransferData.setNameAndValue("InputDate","2005-10-12");//((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
        //tTransferData.setNameAndValue("ScanOperator","001");//
        //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
        //tTransferData.setNameAndValue("ManageCom", "86"); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

        tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
        tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema
                .getMakeDate()); //((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
        tTransferData.setNameAndValue("ScanOperator", tES_DOC_MAINSchema
                .getScanOperator()); //
        //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
        tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema
                .getManageCom()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

        tVData.add(tGlobalInput);
        tVData.add(tTransferData);
        //080730 创建工作流之前先查询是否巳经存在
           String test  = "select * from lwmission where activityid='0000001099' and missionprop1='"+
                          tES_DOC_MAINSchema.getDocCode()+"' with ur";
           SSRS result = new ExeSQL().execSQL(test);
           if(result.getMaxRow()>0){
                CError tError = new CError();
                tError.moduleName = "DocTBWorkFlowService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "工作流表已经存在!";
                this.mErrors.addOneError(tError);
                return false;
           }
        try
        {
            ActivityOperator tActivityOpertor = new ActivityOperator();

            if (tActivityOpertor.CreateStartMission("0000000003", "0000001099",
                    tVData))
            {
                mResult = tActivityOpertor.getResult();
            }
            else
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "DocTBWorkFlowService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "扫描录入工作流节点生成失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DocTBWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
        }

        MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        //向LockTable表中置数据。
        
        /**
         * 增加并发查询设计，主要由于
         */
        LockTableDB tLockTableDB = new LockTableDB();
        tLockTableDB.setNoType("SI");
        tLockTableDB.setNoLimit(tES_DOC_MAINSchema.getDocCode());
        if(tLockTableDB.getInfo()){
        	String date = tLockTableDB.getMakeDate();
        	String time = tLockTableDB.getMakeTime();
        	
        	DateFormat  df  =  new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.SIMPLIFIED_CHINESE); 
        	Date bef = null;
			Date now = new Date();
			try {
				bef = df.parse(date+" "+time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			long between = (now.getTime() - bef.getTime()) / 1000;// 除以1000是为了转换成秒
			long day = between / (24 * 3600);
			long hour = between % (24 * 3600) / 3600;
			long minute = between % 3600 / 60;
			long second = between % 60 / 60;
			
			if(day>0 || hour>0 || minute > 0 || second > 30){
				map.put(tLockTableDB.getSchema(), SysConst.DELETE);
			}else{
				CError tError = new CError();
	            tError.moduleName = "DocTBWorkFlowService";
	            tError.functionName = "Save";
	            tError.errorNo = "-99";
	            tError.errorMessage = "为了保证扫描上载避免重复导入，现控制同一单证号码在30秒不得重复上载。目前还有："+(30-second)+"秒";
	            this.mErrors.addOneError(tError);
	            return false;
			}
        }
        LockTableSchema tLockTableSchema = new LockTableSchema();
        tLockTableSchema.setNoType("SI");
        tLockTableSchema.setNoLimit(tES_DOC_MAINSchema.getDocCode());
        tLockTableSchema.setMakeDate(PubFun.getCurrentDate());
        tLockTableSchema.setMakeTime(PubFun.getCurrentTime());
        map.put(tLockTableSchema, SysConst.INSERT);


        // 修改调用该段程序后，会覆盖原 es_doc_main 表中，某些字段的数据库默认值。
        // gongqun2007-4-12
        //ES_DOC_MAINSchema aES_DOC_MAINSchema = tES_DOC_MAINSchema.getSchema();
        String archiveNo = createArchiveNo(tES_DOC_MAINSchema.getManageCom());
        if (archiveNo == null)
        {
            return false;
        }
        //tES_DOC_MAINSchema.setArchiveNo(archiveNo);
        //tES_DOC_MAINSchema.setState("01");
        double tDocId = tES_DOC_MAINSchema.getDocID();
        String tStrSql = "update ES_Doc_Main " + " set ArchiveNo = '"
                + archiveNo + "', " + " State = '01', "
                + " ModifyDate = current date, ModifyTime = current time "
                + " where DocId = " + tDocId;
        //MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        map.put(tStrSql, SysConst.UPDATE);
        //map.put(aES_DOC_MAINSchema, SysConst.UPDATE);

        //System.out.println("\n\n\n\n设置归档号：" + tES_DOC_MAINSchema.getArchiveNo());
        System.out.println("\n\n\n\n设置归档号：" + tStrSql);

        System.out.println("----------------WorkFlow End-----------------");
        return true;
    }

    /** gongqun2007-4-12
     * 生成归档号es_doc_main.ArchiveNo
     * @return String
     */
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "SY" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        String sql = "select max(DocID) from ES_DOC_MAIN ";
        String maxDocID = new ExeSQL().getOneValue(sql);

        ES_DOC_MAINDB db = new ES_DOC_MAINDB();
        db.setDocID(maxDocID);
        db.getInfo();
        ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
        tES_DOC_MAINSet.add(db.getSchema());

        VData d = new VData();
        d.add(tES_DOC_MAINSet);

        DocTBWorkFlowService bl = new DocTBWorkFlowService();
        if (!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }

    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
