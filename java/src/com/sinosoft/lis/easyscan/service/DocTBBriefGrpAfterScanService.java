package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class DocTBBriefGrpAfterScanService implements EasyScanService {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public DocTBBriefGrpAfterScanService() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			System.out.println("----------------------DocTBBriefGrpServiece Begin----------------------");
			ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

			/**得到具体信息**/

			ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
			if (tES_DOC_MAINSET.size() != 1) {
				System.out.println("2");
				CError tError = new CError();
				tError.moduleName = "DocTBBriefGrpServiece";
				tError.functionName = "Save";
				tError.errorNo = "-99";
				tError.errorMessage = "传入数据出错!";
				return false;
			}

			tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
			tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1)
					.getBussType());
			tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1)
					.getSubType());
			tES_DOC_RELATIONSchema.setRelaFlag("0");
			tES_DOC_RELATIONSchema.setBussNoType("12");
			tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1)
					.getDocCode());
			if (tES_DOC_RELATIONSchema != null) {
				//返回数据map
				MMap map = new MMap();
				map.put(tES_DOC_RELATIONSchema, "INSERT");
				mResult.add(map);
			} else {
				CError tError = new CError();
				tError.moduleName = "DocTBWorkFlowService";
				tError.functionName = "Save";
				tError.errorNo = "-99";
				tError.errorMessage = "数据生成出错!";
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
		//test
		ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
		tES_DOC_MAINSchema.setDocCode("1234");
		tES_DOC_MAINSchema.setDocID("1234");
		ES_DOC_MAINSet tES_DOC_MAINSET = new ES_DOC_MAINSet();
		tES_DOC_MAINSET.add(tES_DOC_MAINSchema);
		VData tVData = new VData();
		tVData.add(tES_DOC_MAINSET);
		DocTBUploadRelationService tDocTBRelation1Service = new DocTBUploadRelationService();
		tDocTBRelation1Service.submitData(tVData, "");
		PubSubmit pubsubmit = new PubSubmit();
		/**准备提交数据**/
		if (pubsubmit.submitData(tDocTBRelation1Service.getResult(), "")) {
			System.out.println("OK");
		}
	}

	/** gongqun2007-4-12
	 * 生成归档号es_doc_main.ArchiveNo
	 * @return String
	 */
	public String createArchiveNo(String tManageCom) {
		String tDate = PubFun.getCurrentDate();
		tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
				+ tDate.substring(8, 10);

		String comPart = "";
		int length = tManageCom.length();
		if (length == 2) {
			CError tError = new CError();
			tError.moduleName = "DocTBGrpWorkFlowService";
			tError.functionName = "createGrpArchiveNo";
			tError.errorMessage = "两位机构代码不能进行投保单扫描";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return null;
		} else if (length == 4) {
			comPart = tManageCom.substring(2, 4) + "00";
		} else {
			comPart = tManageCom.substring(2, 6);
		}

		String tComtop = "YBT" + comPart + tDate;
		String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
		return tArchiveNo;
	}
}
