package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBUploadRelationService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBUploadRelationService() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    try
    {
      System.out.println(
          "----------------------DocTBRelation1 Begin----------------------");
      ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

      /**得到具体信息**/

      ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
      System.out.println("1");
      if (tES_DOC_MAINSET.size() != 1) {
        System.out.println("2");
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "传入数据出错!";
        return false;
      }

      System.out.println("3");
      tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
      tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
      tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
      tES_DOC_RELATIONSchema.setRelaFlag("0");
      tES_DOC_RELATIONSchema.setBussNoType("11");
      tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
      if (tES_DOC_RELATIONSchema != null) {
        //返回数据map
        System.out.println("tES_DOC_RELATIONSchema--INSERT");
        MMap map = new MMap();
        map.put(tES_DOC_RELATIONSchema, "INSERT");
        mResult.add(map);
      }
      else {
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "数据生成出错!";
        return false;
      }
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return true;
  }

  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    //test
    ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
    tES_DOC_MAINSchema.setDocCode("1234");
    tES_DOC_MAINSchema.setDocID("1234");
    ES_DOC_MAINSet tES_DOC_MAINSET = new ES_DOC_MAINSet();
    tES_DOC_MAINSET.add(tES_DOC_MAINSchema);
    VData tVData = new VData();
    tVData.add(tES_DOC_MAINSET);
    DocTBUploadRelationService tDocTBRelation1Service = new DocTBUploadRelationService();
    tDocTBRelation1Service.submitData(tVData, "");
    PubSubmit pubsubmit = new PubSubmit();
    /**准备提交数据**/
    if (pubsubmit.submitData(tDocTBRelation1Service.getResult(), "")) {
        System.out.println("OK");
    }
  }

    public CErrors getErrors() {
        return mErrors;
    }
}
