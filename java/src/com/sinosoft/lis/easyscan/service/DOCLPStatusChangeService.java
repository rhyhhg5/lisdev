package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 理赔申请书上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.easyscan.EasyScanService;
import com.sinosoft.lis.llcase.LLCaseCommon;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.LLCaseOpTimeSchema;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class DOCLPStatusChangeService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DOCLPStatusChangeService() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    try
    {
      MMap map = new MMap();
      System.out.println("----------DOCLPStatusChangeService Begin----------");
      ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
      LLCaseOpTimeSchema mLLCaseOpTimeSchema = new LLCaseOpTimeSchema();
      LLCaseSchema mLLCaseSchema = new LLCaseSchema();
      LLCaseDB mLLCaseDB = new LLCaseDB();
      /**得到具体信息**/

      ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
      if (tES_DOC_MAINSET.size() != 1) {
        System.out.println("2");
        CError tError = new CError();
        tError.moduleName = "DocLPStatusChangeService";
        tError.functionName = "submitData";
        tError.errorNo = "-99";
        tError.errorMessage = "传入数据出错!";
        return false;
      }
      mLLCaseDB.setCaseNo(tES_DOC_MAINSET.get(1).getDocCode());
      if(mLLCaseDB.getInfo())
      {
        mLLCaseSchema = mLLCaseDB.getSchema();
        if(mLLCaseSchema.getRgtState().equals("01")){
            mLLCaseSchema.setRgtState("02");
            mLLCaseSchema.setHospitalFlag("1");
            mLLCaseSchema.setOperator(tES_DOC_MAINSET.get(1).getScanOperator());
            mLLCaseOpTimeSchema.setCaseNo(mLLCaseSchema.getCaseNo());
            mLLCaseOpTimeSchema.setOperator(tES_DOC_MAINSET.get(1).getScanOperator());
            mLLCaseOpTimeSchema.setManageCom(tES_DOC_MAINSET.get(1).getManageCom());
            mLLCaseOpTimeSchema.setStartDate(mLLCaseSchema.getModifyDate());
            mLLCaseOpTimeSchema.setStartTime(mLLCaseSchema.getModifyTime());
            mLLCaseOpTimeSchema.setRgtState("02");
            LLCaseCommon tLLCaseCommon = new LLCaseCommon();
            LLCaseOpTimeSchema tLLCaseOpTimeSchema = tLLCaseCommon.CalTimeSpan(
                    mLLCaseOpTimeSchema);
            map.put(tLLCaseOpTimeSchema, "DELETE&INSERT");
        }
        map.put(mLLCaseSchema,"UPDATE");
        GlobalInput tGlobalInput = new GlobalInput();
        /** 全局变量 */
        tGlobalInput.Operator = "lp001";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        //增加 DocCode 的锁定    2008-10-06
        MMap mMap = lockTable(tES_DOC_MAINSET.get(1).getDocCode(), tGlobalInput);
        if(mMap != null)
            map.add(mMap);
        else
            return false;
      }else{
          CError tError = new CError();
          tError.moduleName = "DOCLPStatusChangeService";
          tError.functionName = "Save";
          tError.errorNo = "-99";
          tError.errorMessage = "上传失败！单证号"
                                +tES_DOC_MAINSET.get(1).getDocCode()+
                                "不是正确的理赔案件号!";
          System.out.println("return false\n"+tError.errorMessage);
          return false;
      }
      if (map != null) {
        //返回数据map
        mResult.add(map);
      }
      else {
        CError tError = new CError();
        tError.moduleName = "DOCLPStatusChangeService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "没有找到相应的数据!";
        return false;
      }
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return true;
  }
  public CErrors getErrors()
  {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    //test
    ES_DOC_MAINSet aES_DOC_MAINSet = new ES_DOC_MAINSet();
    ES_DOC_MAINSchema aES_DOC_MAINSchema = new ES_DOC_MAINSchema();
    aES_DOC_MAINSchema.setBussType("LP");
    aES_DOC_MAINSchema.setSubType("LP02");
    aES_DOC_MAINSchema.setDocCode("P9400060815000005");
    aES_DOC_MAINSchema.setDocFlag("1");
    aES_DOC_MAINSchema.setDocID("29018");
    aES_DOC_MAINSet.add(aES_DOC_MAINSchema);
    VData aVData = new VData();
    aVData.add(aES_DOC_MAINSet);
    DOCLPStatusChangeService tService = new DOCLPStatusChangeService();
    if (tService.submitData(aVData, "INSERT")) {
      System.out.println("成功！！");
    }

  }
  
  /**
   * 锁定扫描上载
   * @param LockNoKey
   * @param tGlobalInput
   * @return
   */
  private MMap lockTable(String lockNoKey, GlobalInput globalInput)
  {
      MMap map = null;
      
      //扫描上载类型：“SI”
      String tLockNoType = "SI";
      
      //锁定有效时间
      String tAIS = "60";

      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("LockNoKey", lockNoKey);
      tTransferData.setNameAndValue("LockNoType", tLockNoType);
      tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

      VData tVData = new VData();
      tVData.add(globalInput);
      tVData.add(tTransferData);
      
      LockTableActionBL tLockTableActionBL = new LockTableActionBL();
      map = tLockTableActionBL.getSubmitMap(tVData, null);
      if(map == null)
      {
          mErrors.copyAllErrors(tLockTableActionBL.mErrors);
          return null;
      }
      
      return map;
  }
}
