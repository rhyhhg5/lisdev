package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;

public class DocHMContPro implements EasyScanService
{
        /** 传入数据的容器 */
        private VData mInputData = new VData();

        /** 传出数据的容器 */
        private VData mResult = new VData();

        /** 数据操作字符串 */
        private String mOperate;

        /** 错误处理类 */
        public CErrors mErrors = new CErrors();

        public DocHMContPro() {}

        public boolean submitData(VData cInputData, String cOperate)
        {
            try
            {
                MMap map = new MMap();
                System.out.println("---------DocHMContPro Begin--------");
                ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

                /**得到具体信息**/
                ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
                if (tES_DOC_MAINSET.size() != 1)
                {
                    CError tError = new CError();
                    tError.moduleName = "DocHMContPro";
                    tError.functionName = "submitData";
                    tError.errorNo = "-99";
                    tError.errorMessage = "传入数据出错!";
                    return false;
                }

                //校验保单是否上传
                ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
                ES_DOC_MAINSet ttES_DOC_MAINSet = new ES_DOC_MAINSet();
                tES_DOC_MAINDB.setDocCode(tES_DOC_MAINSET.get(1).getDocCode());
                tES_DOC_MAINDB.setSubType("TB02");
                ttES_DOC_MAINSet = tES_DOC_MAINDB.query();

                if(ttES_DOC_MAINSet.size() < 1 || ttES_DOC_MAINSet == null)
                {
                    CError tError = new CError();
                    tError.moduleName = "DocHMContPro";
                    tError.functionName = "submitData";
                    tError.errorNo = "-99";
                    tError.errorMessage = "此协议对应的保单没有上传!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                //校验正上传的健康协议是否已上传过
                ES_DOC_RELATIONDB tES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
                ES_DOC_RELATIONSet ttES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();
                tES_DOC_RELATIONDB.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
                tES_DOC_RELATIONDB.setSubType("HM04");
                ttES_DOC_RELATIONSet = tES_DOC_RELATIONDB.query();

                if(ttES_DOC_RELATIONSet.size() > 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "DocHMContPro";
                    tError.functionName = "submitData";
                    tError.errorNo = "-99";
                    tError.errorMessage = "此保单的健康协议已上传，请勿重复上传!";
                    this.mErrors.addOneError(tError);
                    return false;
                }



                //关联关系
                tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
                tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
                tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
                tES_DOC_RELATIONSchema.setRelaFlag("0");
                tES_DOC_RELATIONSchema.setBussNoType("64");
                tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
                if (tES_DOC_RELATIONSchema != null)
                {
                    map.put(tES_DOC_RELATIONSchema, "INSERT");
                }

                if (map != null)
                {
                    //返回数据map
                    mResult.add(map);
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "DocHMContPro";
                    tError.functionName = "submitData";
                    tError.errorNo = "-99";
                    tError.errorMessage = "数据生成出错!";
                    return false;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return true;
        }

        public CErrors getErrors()
        {
            return mErrors;
        }

        public VData getResult()
        {
            return mResult;
        }

        public static void main(String[] args)
        {
            ES_DOC_MAINSchema es = new ES_DOC_MAINSchema();
            ES_DOC_MAINSet esSet = new ES_DOC_MAINDBSet();

            es.setDocID("11111111111111");
            es.setBussType("HM");
            es.setSubType("HM04");
            es.setDocCode("18000001903");
            es.setScanOperator("001");
            es.setManageCom("86");
            es.setMakeDate(PubFun.getCurrentDate());
            es.setMakeTime(PubFun.getCurrentTime());
            es.setModifyDate(PubFun.getCurrentDate());
            es.setModifyTime("15:07:07");
            esSet.add(es);
            VData v = new VData();
            v.add(esSet);
            DocHMContPro d = new DocHMContPro();
            d.submitData(v, "001");
        }
}
