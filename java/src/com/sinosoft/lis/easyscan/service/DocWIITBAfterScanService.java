/**
 * 
 */
package com.sinosoft.lis.easyscan.service;

import com.sinosoft.lis.easyscan.EasyScanService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;

/**
 * @author zhangyang
 *
 */
public class DocWIITBAfterScanService implements EasyScanService {

	/** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocWIITBAfterScanService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            ES_DOC_MAINSchema tScanMainInfo = null;

            // 获取扫描件信息
            ES_DOC_MAINSet tEDMScanInfoSet = (ES_DOC_MAINSet) cInputData.getObjectByObjectName("ES_DOC_MAINSet", 0);
            if (tEDMScanInfoSet.size() != 1)
            {
                buildError("submitData", "影印件主表信息获取失败。");
                return false;
            }
            tScanMainInfo = tEDMScanInfoSet.get(1);
            // --------------------

            String tPrtNo = tScanMainInfo.getDocCode();
            // 校验保单是否已录入            
            if (!chkContMessage(tPrtNo))
            {
                buildError("submitData", "该印刷号不存在。");
                return false;
            }
            // --------------------
            
            // 校验保单是否已经上载了扫描件
            if(!cheakESDocMain(tPrtNo))
            {
            	buildError("submitData","该保单的扫描件已经上载！");
            }
            // --------------------
            
            // 处理归档号
            String archiveNo = createArchiveNo(tScanMainInfo.getManageCom());
            if (archiveNo == null)
            {
                return false;
            }
            tScanMainInfo.setArchiveNo(archiveNo);

            System.out.println("设置归档号：" + tScanMainInfo.getArchiveNo());
            // --------------------

            // 对补扫进行锁定，时效为5分钟。
            MMap tTmpMap = null;
            tTmpMap = lockCont(tPrtNo);
            if (tTmpMap == null)
            {
                return false;
            }
            mResult.add(tTmpMap);
            // -------------------------------

        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "团体工伤险扫描上传业务出现异常。");
            return false;
        }

        return true;
    }

    //校验保单是否存在
    private boolean chkContMessage(String cPrtNo)
    {
        String tStrSql = " select 1 from LCGrpCont where PrtNo = '" + cPrtNo + "' " + " union all "
                + " select 1 from LBGrpCont where PrtNo = '" + cPrtNo + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if (tResult == null || "".equals(tResult))
        {
        	return false;
        }

        return true;
    }
    
    //校验保单的扫描件是否已经上载
    private boolean cheakESDocMain(String cPrtNo)
    {
    	String tStrSQL = "select 1 from ES_DOC_MAIN where doccode = '" + cPrtNo + "' and SubType = 'TB25'";
    	String tResult = new ExeSQL().getOneValue(tStrSQL);
    	if("1".equals(tResult))
    	{
    		return false;
    	}
    	
    	return true;
    }

    /**
     * 锁定动作。
     * @param 
     * @return
     */
    private MMap lockCont(String cPrtNo)
    {
        MMap tMMap = null;

        // 团体工伤险扫描标志：“G0”
        String tLockNoType = "G0";
        // -----------------------

        // 锁定有效时间
        String tAIS = "60";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cPrtNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(new GlobalInput());
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            buildError("lockCont", "保单锁定失败。");
            return null;
        }

        return tMMap;
    }

    /**
     * 
     * @return String
     */
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7) + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "J" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);
        CError cError = new CError();
        cError.moduleName = "DocWIITBScanService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

}
