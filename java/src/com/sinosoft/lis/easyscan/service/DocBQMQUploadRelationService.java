package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.TaskDeliverBL;
import com.sinosoft.utility.*;
import com.sinosoft.task.TaskInputBL;


public class DocBQMQUploadRelationService implements EasyScanService
{
    /** 传出数据的容器 */
    private VData mResult = new VData();

    private String workNoForDelever = "";
    private boolean deliverFlag = true;
    private String nodeNo = "0"; //在生成历史信息节点详细信息是使用，默认为生成新工单

    /** 数据操作字符串 */
    private String mOperate;

    //公共录入信息
    private GlobalInput gi = new GlobalInput();

    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocBQMQUploadRelationService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            MMap map = new MMap();
            System.out.println("------DocBQMQUploadRelationService Begin-----");
            ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
                    ES_DOC_RELATIONSchema();
            /**得到具体信息**/
            ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
            if (tES_DOC_MAINSET.size() < 1)
            {
                mErrors.addOneError("传入数据出错");
                return false;
            }
                     
                //公单关联关系
                tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
                tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).
                                                   getBussType());
                tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).
                                                  getSubType());
                tES_DOC_RELATIONSchema.setRelaFlag("0");
                tES_DOC_RELATIONSchema.setBussNoType("15");
                tES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(1).
                                                  getDocCode());
                tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
                if (tES_DOC_RELATIONSchema != null)
                {
                    map.put(tES_DOC_RELATIONSchema, "INSERT");
                }

                //设置归档号
                String archiveNo = createArchiveNo(tES_DOC_MAINSET.get(1).getManageCom());
                if (archiveNo == null)
                {
                    return false;
                }
                double tDocId = tES_DOC_MAINSET.get(1).getDocID();
                String tStrSql = "update ES_Doc_Main " + " set ArchiveNo = '"
                        + archiveNo + "', " + " State = '01', "
                        + " ModifyDate = current date, ModifyTime = current time "
                        + " where DocId = " + tDocId;
                map.put(tStrSql, SysConst.UPDATE);
                                
                if (map != null)
                {
                    mResult.add(map);
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "DocBQUploadRelationService";
                    tError.functionName = "Save";
                    tError.errorNo = "-99";
                    tError.errorMessage = "数据生成出错!";
                    return false;
                }
             
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return true;
    }
    
    //生成归档号es_doc_main.ArchiveNo
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "PD" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    } 
      
    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

    
}
