package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


public class DocHMCustomerTest implements EasyScanService
{
        /** 传入数据的容器 */
        private VData mInputData = new VData();


        /** 传出数据的容器 */
        private VData mResult = new VData();

        /** 数据操作字符串 */
        private String mOperate;

        /** 错误处理类 */
        public CErrors mErrors = new CErrors();

        public DocHMCustomerTest() {}

        public boolean submitData(VData cInputData, String cOperate)
        {

            try
            {
                MMap map = new MMap();
                ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
                        ES_DOC_RELATIONSchema();

                /**得到具体信息**/
                ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.
                                                 get(0);
                if (tES_DOC_MAINSET.size() != 1)
                {
                    CError tError = new CError();
                    tError.moduleName = "DocHMCustomerTest";
                    tError.functionName = "submitData";
                    tError.errorNo = "-99";
                    tError.errorMessage = "传入数据出错!";
                    return false;
                }

                tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
                System.out.println("OKKKKKKKKKKKKKKK"+tES_DOC_RELATIONSchema.getDocID());
                tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).
                        getBussType());
                tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).
                                                  getSubType());
                tES_DOC_RELATIONSchema.setRelaFlag("0");
                tES_DOC_RELATIONSchema.setBussNoType("10");
                tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).
                                                 getDocCode());
                if (tES_DOC_RELATIONSchema != null)
                {
                    map.put(tES_DOC_RELATIONSchema, "INSERT");
                }



                if (map != null)
                {
                        //返回数据map
                        mResult.add(map);
                }
                else
                {
                      CError tError = new CError();
                      tError.moduleName = "DocHMWorkFlowService";
                      tError.functionName = "submitData";
                      tError.errorNo = "-99";
                      tError.errorMessage = "数据生成出错!";
                      return false;
                }
                 System.out.println("OKKKKDDDDDDDDDdd"+tES_DOC_MAINSET.get(1).getDocCode());

                 LHScanInfoDB tLHScanInfoDB=new LHScanInfoDB();
                 tLHScanInfoDB.setSerialNo(tES_DOC_MAINSET.get(1).getDocCode());
                 tLHScanInfoDB.getInfo();
                 ExeSQL tExeSQL= new ExeSQL();

                if(tLHScanInfoDB.getInfo() == false)
               {
                 System.out.println("VVVVVVVVVVVVVVVVVVVVvv");
                String sql2 = " insert into  lhscaninfo (SerialNo, ScanState ,ScanDate, ScanTime, "
                            +" InsertState, ManageCom,Operator,MakeDate, "
                            +" MakeTime, ModifyDate ,ModifyTime )"
                            +" values ('"+tES_DOC_MAINSET.get(1).getDocCode()+"' ,'1',"
                            +" '"+PubFun.getCurrentDate()+"', "
                            +" '"+PubFun.getCurrentTime()+"', "
                            +" '3', "
                            +" '"+tES_DOC_MAINSET.get(1).getManageCom()+"', "
                            +" '"+tES_DOC_MAINSET.get(1).getScanOperator()+"', "
                            +" '"+PubFun.getCurrentDate()+"' , "
                            +" '"+PubFun.getCurrentTime() +"', "
                            +" '"+PubFun.getCurrentDate()+"', "
                            +" '"+PubFun.getCurrentTime()+"') ";
                System.out.println("VVBBBBBBBBBBBBBBBBBBBBB");
                      if (tExeSQL.execUpdateSQL(sql2) == false) {
                        CError tError = new CError();
                        tError.moduleName = "DocHMWorkFlowService";
                        tError.functionName = "submitData";
                        tError.errorNo = "-99";
                        tError.errorMessage = "将LHScanInfo->scanstate置为扫描状态时出错";
                        return false;
                      }

               }
               else
               {
                   if(tES_DOC_MAINSET.get(1).getInputState()=="null"||tES_DOC_MAINSET.get(1).getInputState()==""||tES_DOC_MAINSET.get(1).getInputState()=="null")
                   {
                       String sql = " update lhscaninfo set scanstate = '1', "
                                    + " InsertState = '3', "
                                    + " ManageCom = '" +
                                    tES_DOC_MAINSET.get(1).getManageCom() +
                                    "', "
                                    + " Operator = '" +
                                    tES_DOC_MAINSET.get(1).getScanOperator() +
                                    "', "
                                    + " ModifyDate = '" +
                                    tES_DOC_MAINSET.get(1).getModifyDate() +
                                    "', "
                                    + " ModifyTime = '" +
                                    tES_DOC_MAINSET.get(1).getModifyTime() +
                                    "' where serialno ='"
                                    + tES_DOC_MAINSET.get(1).getDocCode() + "'";

                       if (tExeSQL.execUpdateSQL(sql) == false) {
                           CError tError = new CError();
                           tError.moduleName = "DocHMWorkFlowService";
                           tError.functionName = "submitData";
                           tError.errorNo = "-99";
                           tError.errorMessage =
                                   "将LHScanInfo->scanstate置为扫描状态时出错";
                           return false;
                       }
                   }
               }


            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        return true;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

            public static void main(String[] args) {
      ES_DOC_MAINSchema es = new ES_DOC_MAINSchema();
      ES_DOC_MAINSet esSet = new ES_DOC_MAINDBSet();

      es.setDocID("1");
      es.setBussType("1");
      es.setSubType("1");
      es.setDocCode("12");
      es.setScanOperator("001");
      es.setManageCom("86");
      es.setMakeDate(PubFun.getCurrentDate());
      es.setMakeTime(PubFun.getCurrentTime());
      es.setModifyDate(PubFun.getCurrentDate());
      es.setModifyTime("15:07:07");
      esSet.add(es);

      VData v = new VData();
      v.add(esSet);

      DocHMCustomerTest d = new DocHMCustomerTest();
      d.submitData(v, "001");

      VData vData = d.getResult();
      PubSubmit p = new PubSubmit();

      if(p.submitData(vData, "001"))
      {
          System.out.println("OKKKKKKKKKKKKKKK");
      }
  }
}
