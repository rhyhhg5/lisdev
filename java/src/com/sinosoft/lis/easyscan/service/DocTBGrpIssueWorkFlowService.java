package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 体检单上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBGrpIssueWorkFlowService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBGrpIssueWorkFlowService() {
  }
  public boolean submitData(VData cInputData, String cOperate) {


    System.out.println("----------------------DocTBGrpIssueWorkFlowService Begin----------------------");
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    GlobalInput tGlobalInput = new GlobalInput();
    /** 全局变量 */
//    tGlobalInput.Operator = "001";
//    tGlobalInput.ComCode = "86";
//    tGlobalInput.ManageCom = "86";

    /**得到具体信息**/
    ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet)cInputData.get(0);
    if(tES_DOC_MAINSET.size()!=1)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "传入数据出错!";
      return false;
    }

    ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema)tES_DOC_MAINSET.get(1);


    //通过前台得到最后操作数据
    tGlobalInput.Operator = tES_DOC_MAINSchema.getScanOperator();
    tGlobalInput.ComCode = tES_DOC_MAINSchema.getManageCom();
    tGlobalInput.ManageCom = tES_DOC_MAINSchema.getManageCom();
    //校验是否有要扫描的问题件
    LCGrpIssuePolDB tLCGrpIssuePolDB=new LCGrpIssuePolDB();
    LCGrpIssuePolSet tLCGrpIssuePolSet=new LCGrpIssuePolSet();
    tLCGrpIssuePolSet=tLCGrpIssuePolDB.executeQuery("select * from LCGrpIssuePol where prtseq='"+tES_DOC_MAINSchema.getDocCode()+"' and state='3'");
    if(tLCGrpIssuePolSet.size()==0)
    {
        CError tError = new CError();
       tError.moduleName = "DocTBWorkFlowService";
       tError.functionName = "Save";
       tError.errorNo = "-99";
       tError.errorMessage = "没有打印团体问题件通知书，不允许进行回销操作!";
       return false;
    }
    //校验保单所属状态
    LCGrpContDB mLCGrpContDB = new LCGrpContDB();
    LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    mLCGrpContSet=mLCGrpContDB.executeQuery("select * from lcgrpcont where prtno='"+tES_DOC_MAINSchema.getDocCode().substring(0,11)+"' and uwflag='9'");
    System.out.println("爸爸爸爸"+tES_DOC_MAINSchema.getDocCode().substring(0,11));
    if (mLCGrpContSet.size()==1) {
   CError tError = new CError();
   tError.moduleName = "DocTBWorkFlowService";
   tError.functionName = "Save";
   tError.errorNo = "-99";
   tError.errorMessage = "此保单已经经过人工核保，不能进行回销！！";
   return false;
    }
    //prepare data of lwfieldmap(table)
    //tTransferData.setNameAndValue("PrtNo","1234");// ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
    //tTransferData.setNameAndValue("InputDate","2005-10-12");//((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
    //tTransferData.setNameAndValue("ScanOperator","001");//
    //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
    //tTransferData.setNameAndValue("ManageCom", "86"); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

//    tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
//    tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema.getMakeDate()); //((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
//    tTransferData.setNameAndValue("ScanOperator",tES_DOC_MAINSchema.getScanOperator()); //
//    ((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
//    tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema.getManageCom()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());


    System.out.println("----------------------WorkFlow End----------------------");
   return true;
  }
  public CErrors getErrors()
  {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }
}
