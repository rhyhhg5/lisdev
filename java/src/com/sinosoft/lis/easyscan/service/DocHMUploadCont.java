package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;


public class DocHMUploadCont
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocHMUploadCont() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    try
    {
      MMap map = new MMap();
      System.out.println(
          "----------------------DocHMUploadCont Begin----------------------");
      ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

      /**得到具体信息**/

      ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
      if (tES_DOC_MAINSET.size() != 1) {
        System.out.println("2");
        CError tError = new CError();
        tError.moduleName = "DocHMUploadCont";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "传入数据出错!";
        return false;
      }

      //公单关联关系
      tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
      tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
      tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
      tES_DOC_RELATIONSchema.setRelaFlag("0");
      tES_DOC_RELATIONSchema.setBussNoType("61");
      tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
      if(tES_DOC_RELATIONSchema!=null)
      {
        map.put(tES_DOC_RELATIONSchema,"INSERT");
      }

      //扫描业务号关联
//      tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
//      tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
//      tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
//      tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
//      tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
//      tES_DOC_RELATIONSchema.setBussNoType("31");
//      tES_DOC_RELATIONSchema.setRelaFlag("0");
//      if (tES_DOC_RELATIONSchema != null) {
//          map.put(tES_DOC_RELATIONSchema, "INSERT");
//      }

      if (map != null) {
        //返回数据map
        mResult.add(map);
      }
      else {
        CError tError = new CError();
        tError.moduleName = "DocHMWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "数据生成出错!";
        return false;
      }
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return true;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args)
  {
      ES_DOC_MAINSchema es = new ES_DOC_MAINSchema();
      ES_DOC_MAINSet esSet = new ES_DOC_MAINDBSet();

      es.setDocID("1");
      es.setBussType("1");
      es.setSubType("1");
      es.setDocCode("12");
      es.setScanOperator("001");
      es.setManageCom("86");
      es.setMakeDate(PubFun.getCurrentDate());
      es.setMakeTime(PubFun.getCurrentTime());
      es.setModifyDate(PubFun.getCurrentDate());
      es.setModifyTime("15:07:07");
      esSet.add(es);

      VData v = new VData();
      v.add(esSet);

      DocHMUploadCont d = new DocHMUploadCont();
      d.submitData(v, "001");

      VData vData = d.getResult();
      PubSubmit p = new PubSubmit();

      if(p.submitData(vData, "001"))
      {
          System.out.println("OKKKKKKKKKKKKKKK");
      }
  }
}
