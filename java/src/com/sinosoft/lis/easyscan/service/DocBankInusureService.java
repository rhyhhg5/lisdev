package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 银保投保书接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

public class DocBankInusureService implements EasyScanService
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocBankInusureService()
    {
    }

    /**
     * 生成新单录入工作流节点，扫描时自动调用。
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("-----DocBankInusureService Begin-------");
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
        GlobalInput tGlobalInput = new GlobalInput();
        
        /** 全局变量 */
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        
        /**得到具体信息**/
        ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
        if(tES_DOC_MAINSET.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "DocBankInusureService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = "传入数据出错!";
            return false;
        }

        ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema)tES_DOC_MAINSET.get(1);
        if(tES_DOC_MAINSchema.getDocCode() == null || tES_DOC_MAINSchema.getDocCode().length() != 12)
        {
            mErrors.addOneError("印刷号必须为12位");
            return false;
        }

        tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode());
        tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema.getMakeDate());
        tTransferData.setNameAndValue("ScanOperator", tES_DOC_MAINSchema.getScanOperator());
        tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema.getManageCom());

        tVData.add(tGlobalInput);
        tVData.add(tTransferData);
        try
        {
            //创建工作流
            ActivityOperator tActivityOpertor = new ActivityOperator();
            if (tActivityOpertor.CreateStartMission("0000000007", "0000007999", tVData))
            {
                mResult = tActivityOpertor.getResult();
            }

            else
            {
                CError tError = new CError();
                tError.moduleName = "DocBankInusureService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "扫描录入工作流节点生成失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
            tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).
                                               getBussType());
            tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
            tES_DOC_RELATIONSchema.setRelaFlag("0");
            tES_DOC_RELATIONSchema.setBussNoType("11");
            tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
            tES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(1).getDocCode());
            if (tES_DOC_RELATIONSchema == null) {
                CError tError = new CError();
                tError.moduleName = "DocBankInusureService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "数据生成出错!";
                return false;
            }

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "DocBankInusureService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
        }

        DocTBWorkFlowService service = new DocTBWorkFlowService();
        String archiveNo = service.createArchiveNo(
            tES_DOC_MAINSchema.getManageCom());  //归档号
        if(archiveNo == null)
        {
            mErrors.copyAllErrors(service.mErrors);
            return false;
        }

       // ES_DOC_MAINSchema aES_DOC_MAINSchema = tES_DOC_MAINSchema.getSchema();
        tES_DOC_MAINSchema.setArchiveNo(archiveNo);
        MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        //map.put(aES_DOC_MAINSchema, SysConst.UPDATE);//
        map.put(tES_DOC_RELATIONSchema, SysConst.INSERT);

        System.out.println("--------DocBankInusureService End-----------");
        return true;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {

    }

    /**
     * 获取处理后的数据集合
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
