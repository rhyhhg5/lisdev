package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 投保单证上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBAskWorkFlowService
    implements EasyScanService {
  /** 传入数据的容器 */


  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBAskWorkFlowService() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println(
        "----------------------WorkFlow Begin----------------------");
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    GlobalInput tGlobalInput = new GlobalInput();
    /** 全局变量 */
    tGlobalInput.Operator = "001";
    tGlobalInput.ComCode = "86";
    tGlobalInput.ManageCom = "86";

    /**得到具体信息**/

    ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet)cInputData.get(0);
    if(tES_DOC_MAINSET.size()!=1)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBAskWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "传入数据出错!";
      return false;
    }

    ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema)tES_DOC_MAINSET.get(1);

    //prepare data of lwfieldmap(table)
    //tTransferData.setNameAndValue("PrtNo","1234");// ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
    //tTransferData.setNameAndValue("InputDate","2005-10-12");//((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
    //tTransferData.setNameAndValue("ScanOperator","001");//
    //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
    //tTransferData.setNameAndValue("ManageCom", "86"); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

    tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
    tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema.getMakeDate()); //((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
    tTransferData.setNameAndValue("ScanOperator",tES_DOC_MAINSchema.getScanOperator()); //
    //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
    tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema.getManageCom()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

    tVData.add(tGlobalInput);
    tVData.add(tTransferData);
    try {
      ActivityOperator tActivityOpertor = new  ActivityOperator();
      if(tActivityOpertor.CreateStartMission("0000000006","0000006001",tVData))
      {
        mResult = tActivityOpertor.getResult();
      }
      else {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "DocTBAskWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "扫描录入工作流节点生成失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
// @@错误处理
      CError tError = new CError();
      tError.moduleName = "DocTBAskWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("----------------------WorkFlow End----------------------");
   return true;
  }

  public VData getResult() {
    return mResult;
  }

    public CErrors getErrors() {
        return mErrors;
    }

}
