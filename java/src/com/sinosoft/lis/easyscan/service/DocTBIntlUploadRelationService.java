package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description:
 * 投保单上传后对于关联表的的操作
 * 生成扫描件和业务的关联
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class DocTBIntlUploadRelationService implements EasyScanService
{
    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocTBIntlUploadRelationService()
    {
    }

    /**
     * 操作的提交方法，EasyScan调用的入口。
     * @param cInputData VData:
     * 包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	ES_DOC_MAINSet对象，完整的扫描件信息。
     * @param cOperate String
     * @return boolean:成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            System.out.println("----DocTBIntlUploadRelationService Begin----");
            ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
                ES_DOC_RELATIONSchema();
            ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
            System.out.println("1");
            if(tES_DOC_MAINSET.size() != 1)
            {
                CError tError = new CError();
                tError.moduleName = "DocTBIntlUploadRelationServiceService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "传入数据出错!";
                return false;
            }

            tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
            tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).
                                               getBussType());
            tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
            tES_DOC_RELATIONSchema.setRelaFlag("0");
            tES_DOC_RELATIONSchema.setBussNoType("11");
            tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
            tES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(1).getDocCode());
            if(tES_DOC_RELATIONSchema != null)
            {
                System.out.println("tES_DOC_RELATIONSchema--INSERT");
                MMap map = new MMap();
                map.put(tES_DOC_RELATIONSchema, "INSERT");
                mResult.add(map);
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "DocTBIntlUploadRelationServiceService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "数据生成出错!";
                return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 获取处理后的数据集合，包括工作流信息，可直接保存。
     * @return VData：成功为处理后的数据集合true，否则null
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        //test
        ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
        tES_DOC_MAINSchema.setDocCode("1234");
        tES_DOC_MAINSchema.setDocID("1234");
        ES_DOC_MAINSet tES_DOC_MAINSET = new ES_DOC_MAINSet();
        tES_DOC_MAINSET.add(tES_DOC_MAINSchema);
        VData tVData = new VData();
        tVData.add(tES_DOC_MAINSET);
        DocTBIntlUploadRelationService tDocTBRelation1Service = new
            DocTBIntlUploadRelationService();
        tDocTBRelation1Service.submitData(tVData, "");
        PubSubmit pubsubmit = new PubSubmit();
        /**准备提交数据**/
        if(pubsubmit.submitData(tDocTBRelation1Service.getResult(), ""))
        {
            System.out.println("OK");
        }
    }

    public CErrors getErrors()
    {
        return mErrors;
    }
}
