package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class DocAccidentWorkFlowService implements EasyScanService
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocAccidentWorkFlowService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            MMap map = new MMap();
            System.out.println("DocBriefTBScanServiece Begin");

            ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
            if (tES_DOC_MAINSET.size() != 1)
            {
                System.out.println("tES_DOC_MAINSET.size() != 1");
                buildError("submitData", "传入数据出错!");
                return false;
            }

            ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
            mES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1)
                    .getDocCode());
            mES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(1)
                    .getDocCode());
            mES_DOC_RELATIONSchema.setBussNoType("11");
            mES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
            mES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1)
                    .getBussType());
            mES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1)
                    .getSubType());
            mES_DOC_RELATIONSchema.setRelaFlag("0");
            if (mES_DOC_RELATIONSchema != null)
            {
                map.put(mES_DOC_RELATIONSchema, "INSERT");
            }
            if (map != null)
            {
                //返回数据map
                mResult.add(map);
            }
            else
            {
                buildError("submitData", "数据生成出错!");
                return false;
            }

            LCContDB mLCContDB = new LCContDB();
            mLCContDB.setPrtNo(tES_DOC_MAINSET.get(1).getDocCode());
            mLCContDB.setCardFlag("2");
            LCContSet tLCContSet = mLCContDB.query();

            LBContDB mLBContDB = new LBContDB();
            mLBContDB.setPrtNo(tES_DOC_MAINSET.get(1).getDocCode());
            mLBContDB.setCardFlag("2");
            LBContSet tLBContSet = mLBContDB.query();
            if (tLCContSet.size() == 0 && tLBContSet.size() == 0)
            {
                buildError("submitData",
                        "系统中未找到相关保单数据，请前进行录入后，再进行补扫操作。（注：该补扫功能仅支持简易平台-意外险类型的保单）");
                return false;
            }

            String sql = "select 1 from ES_DOC_MAIN " + "where DocCode = '"
                    + tES_DOC_MAINSET.get(1).getDocCode()
                    + "' and SubType = 'TB10' "; //印刷号已生成保单

            String temp = new ExeSQL().getOneValue(sql);
            if ("1".equals(temp))
            {
                mErrors.addOneError("印刷号已扫描，不能重复上传。");
                return false;
            }

            // 对补扫进行锁定，时效为5分钟。
            MMap tTmpMap = null;
            tTmpMap = lockCont(tES_DOC_MAINSET.get(1).getDocCode());
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
            // -------------------------------

            // 修改调用该段程序后，会覆盖原 es_doc_main 表中，某些字段的数据库默认值。
            String archiveNo = createArchiveNo(tES_DOC_MAINSET.get(1)
                    .getManageCom());
            if (archiveNo == null)
            {
                return false;
            }

            double tDocId = tES_DOC_MAINSET.get(1).getDocID();
            String tStrSql = "update ES_Doc_Main " + " set ArchiveNo = '"
                    + archiveNo + "', " + " State = '01', "
                    + " ModifyDate = current date, ModifyTime = current time "
                    + " where DocId = " + tDocId;
            map.put(tStrSql, SysConst.UPDATE);
            System.out.println("\n\n\n\n设置归档号：" + tStrSql);

            System.out.println("----------------WorkFlow End-----------------");

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

    /** 
     * 生成归档号es_doc_main.ArchiveNo
     * @return String
     */
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            buildError("createGrpArchiveNo", "两位机构代码不能进行投保单扫描");
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "G" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }

    /**
     * 锁定动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(String cPrtNo)
    {
        MMap tMMap = null;

        // 简易投保单扫描标志：“BI”
        String tLockNoType = "BI";
        // -----------------------

        // 锁定有效时间
        String tAIS = "60";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cPrtNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(new GlobalInput());
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);
        CError cError = new CError();
        cError.moduleName = "DocTBGrpWorkFlowService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
