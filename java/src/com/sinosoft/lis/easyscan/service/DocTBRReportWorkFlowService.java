package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 核保单证上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBRReportWorkFlowService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBRReportWorkFlowService() {
  }
  public boolean submitData(VData cInputData, String cOperate) {


    System.out.println("----------------------DocTBReportWorkFlowService Begin----------------------");
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    GlobalInput tGlobalInput = new GlobalInput();


    /**得到具体信息**/
    ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet)cInputData.get(0);
    if(tES_DOC_MAINSET.size()!=1)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBRReportWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "传入数据出错!";
      return false;
    }

    ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema)tES_DOC_MAINSET.get(1);

    //全局变量
    tGlobalInput.Operator = tES_DOC_MAINSchema.getScanOperator();
    tGlobalInput.ComCode = tES_DOC_MAINSchema.getManageCom();
    tGlobalInput.ManageCom = tES_DOC_MAINSchema.getManageCom();

    //对输入数据进行验证,得到生调工作流节点

    LWMissionDB mLWMissionDB = new LWMissionDB();
    LWMissionSet mLWMissionSet = new LWMissionSet();
    mLWMissionDB.setActivityID("0000001112");
    mLWMissionDB.setMissionProp3(tES_DOC_MAINSchema.getDocCode()); //流水号
    mLWMissionSet = mLWMissionDB.query();
    if(mLWMissionSet==null&&mLWMissionSet.size()!=1)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBRReportWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "无发放此扫描单！！";
      return false;
    }

    //prepare data of lwfieldmap(table)
    //tTransferData.setNameAndValue("PrtNo","1234");// ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
    //tTransferData.setNameAndValue("InputDate","2005-10-12");//((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
    //tTransferData.setNameAndValue("ScanOperator","001");//
    //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
    //tTransferData.setNameAndValue("ManageCom", "86"); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

//    tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
//    tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema.getMakeDate()); //((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
//    tTransferData.setNameAndValue("ScanOperator",tES_DOC_MAINSchema.getScanOperator()); //
//    ((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
//    tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema.getManageCom()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

    LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
    tLZSysCertifySchema.setCertifyCode("9999");
    tLZSysCertifySchema.setCertifyNo(tES_DOC_MAINSchema.getDocCode());
    tLZSysCertifySchema.setTakeBackOperator(tES_DOC_MAINSchema.getScanOperator());
    tLZSysCertifySchema.setTakeBackMakeDate(tES_DOC_MAINSchema.getMakeDate());
    tLZSysCertifySchema.setSendOutCom(tES_DOC_MAINSchema.getManageCom());//通过查询得到
    tLZSysCertifySchema.setReceiveCom(tES_DOC_MAINSchema.getManageCom());

// 准备传输数据 VData
    String tOperate = new String();
    tTransferData.setNameAndValue("CertifyNo", tES_DOC_MAINSchema.getDocCode());
    tTransferData.setNameAndValue("CertifyCode", "1112");
    tTransferData.setNameAndValue("ContNo",
                                  mLWMissionSet.get(1).getMissionProp2());
    tTransferData.setNameAndValue("MissionID",
                                  mLWMissionSet.get(1).getMissionID());
    tTransferData.setNameAndValue("SubMissionID",
                                  mLWMissionSet.get(1).getSubMissionID());
    tTransferData.setNameAndValue("LZSysCertifySchema", tLZSysCertifySchema);
    //tTransferData.setNameAndValue("flag", "N"); //add by tuqiang for tranction!!
    tVData.add(tGlobalInput);
    tVData.add(tTransferData);

    try {
      TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
      if (!tTbWorkFlowUI.submitData(tVData, "0000001112"))
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "DocTBRReportWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "扫描录入工作流节点生成失败!";
        this.mErrors.addOneError(tError);
        System.out.println(tTbWorkFlowUI.mErrors.getFirstError());
        return false;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
// @@错误处理
      CError tError = new CError();
      tError.moduleName = "DocTBRReportWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("----------------------WorkFlow End----------------------");
   return true;
  }
  public CErrors getErrors()
  {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }
}
