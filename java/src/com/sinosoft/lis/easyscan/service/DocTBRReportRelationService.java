package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 生调单证上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBRReportRelationService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBRReportRelationService() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    try
    {
      MMap map = new MMap();
      System.out.println( "----------------------DocTBRReportRelationService Begin----------------------");
      ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();

      /**得到具体信息**/
      ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
      ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
      if (tES_DOC_MAINSET.size() != 1) {
        System.out.println("2");
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "传入数据出错!";
        return false;
      }
      tES_DOC_MAINSchema = tES_DOC_MAINSET.get(1);
      LBMissionDB mLBMissionDB = new LBMissionDB();
      LBMissionSet mLBMissionSet = new LBMissionSet();
      mLBMissionDB.setActivityID("0000001107");
      mLBMissionDB.setMissionProp3(tES_DOC_MAINSchema.getDocCode()); //流水号
      mLBMissionSet = mLBMissionDB.query();
      if (mLBMissionSet == null && mLBMissionSet.size() != 1) {
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "无发放此体检通知书！！";
        return false;
      }
      ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
      mES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
      mES_DOC_RELATIONSchema.setBussNoType("15");
      mES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
      mES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
      mES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
      mES_DOC_RELATIONSchema.setRelaFlag("0");
      if (mES_DOC_RELATIONSchema != null) {
        map.put(mES_DOC_RELATIONSchema,"INSERT");
      }
      String sql = "select prtno from lccont where proposalcontno=(select otherno from loprtmanager where prtseq='"+tES_DOC_MAINSET.get(1).getDocCode()+"')";
      String prtNo = (new ExeSQL()).getOneValue(sql);
      mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
      mES_DOC_RELATIONSchema.setBussNo(prtNo);
      mES_DOC_RELATIONSchema.setBussNoType("13");
      mES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
      mES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
      mES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
      mES_DOC_RELATIONSchema.setRelaFlag("0");
      if (mES_DOC_RELATIONSchema != null) {
        map.put(mES_DOC_RELATIONSchema,"INSERT");
      }
      if (map != null) {
        //返回数据map
        mResult.add(map);
      }
      else {
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "数据生成出错!";
        return false;
      }
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return true;

  }
  public CErrors getErrors()
  {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }

}
