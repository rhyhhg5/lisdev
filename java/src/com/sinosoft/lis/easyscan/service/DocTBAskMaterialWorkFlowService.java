package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 体检单上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBAskMaterialWorkFlowService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBAskMaterialWorkFlowService() {
  }
  public boolean submitData(VData cInputData, String cOperate) {


    System.out.println("----------------------DocTBAskMaterialWorkFlowService Begin----------------------");
    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    GlobalInput tGlobalInput = new GlobalInput();
    /** 全局变量 */
//    tGlobalInput.Operator = "001";
//    tGlobalInput.ComCode = "86";
//    tGlobalInput.ManageCom = "86";

    /**得到具体信息**/
    ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet)cInputData.get(0);
    if(tES_DOC_MAINSET.size()!=1)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBAskMaterialWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "传入数据出错!";
      return false;
    }

    ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema)tES_DOC_MAINSET.get(1);


    //通过前台得到最后操作数据
    tGlobalInput.Operator = tES_DOC_MAINSchema.getScanOperator();
    tGlobalInput.ComCode = tES_DOC_MAINSchema.getManageCom();
    tGlobalInput.ManageCom = tES_DOC_MAINSchema.getManageCom();

    //对输入数据进行验证
    LWMissionDB mLWMissionDB = new LWMissionDB();
    LWMissionSet mLWMissionSet = new LWMissionSet();
    mLWMissionDB.setActivityID("0000006010");
    mLWMissionDB.setMissionProp7(tES_DOC_MAINSchema.getDocCode()); //流水号
    mLWMissionSet = mLWMissionDB.query();
    if(mLWMissionSet==null&&mLWMissionSet.size()!=1)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBAskMaterialWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "无发放此扫描单！！";
      return false;
    }

    if(mLWMissionSet.size()==0)
    {
      CError tError = new CError();
      tError.moduleName = "DocTBAskMaterialWorkFlowService";
      tError.functionName = "Save";
      tError.errorNo = "-99";
      tError.errorMessage = "无发放此扫描单！！";
      return false;
    }
    System.out.println("----------------------WorkFlow End----------------------");
   return true;
  }
  public CErrors getErrors()
  {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }
}
