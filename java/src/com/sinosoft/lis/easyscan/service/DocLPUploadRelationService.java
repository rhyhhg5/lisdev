package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class DocLPUploadRelationService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocLPUploadRelationService() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
      try {
          MMap map = new MMap();         
          System.out.println(
                  "-------DocLPUploadRelationService Begin--------");
          ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
                  ES_DOC_RELATIONSchema();
          LGWorkSchema mLGWorkSchema = new LGWorkSchema();
          /**得到具体信息**/

          ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
          if (tES_DOC_MAINSET.size() != 1) {
              System.out.println("tES_DOC_MAINSET.size() != 1");
              CError tError = new CError();
              tError.moduleName = "DocLPUploadRelationService";
              tError.functionName = "submitData";
              tError.errorNo = "-99";
              tError.errorMessage = "传入数据出错!";
              this.mErrors.addOneError(tError);
              return false;
          }
          if(!"".equals(tES_DOC_MAINSET.get(1).getSubType())&& null != tES_DOC_MAINSET.get(1).getSubType()
        		  && "LP01".equals(tES_DOC_MAINSET.get(1).getSubType()))
          {
        	  LLCaseDB mLLCaseDB = new LLCaseDB();
              mLLCaseDB.setCaseNo(tES_DOC_MAINSET.get(1).getDocCode());
              if(!mLLCaseDB.getInfo())
              {
            	  CError tError = new CError();
                  tError.moduleName = "DocLPUploadRelationService";
                  tError.functionName = "Save";
                  tError.errorNo = "-99";
                  tError.errorMessage = "上传失败！单证号"+tES_DOC_MAINSET.get(1).getDocCode()+"不是正确的理赔案件号!";
                  this.mErrors.addOneError(tError);
                  System.out.println("return false\n"+tError.errorMessage);
                  return false;
              }
          }else if(!"".equals(tES_DOC_MAINSET.get(1).getSubType())&& null != tES_DOC_MAINSET.get(1).getSubType()
        		  && "LP02".equals(tES_DOC_MAINSET.get(1).getSubType()))
          {
        	  LLRegisterDB mLLRegisterDB = new LLRegisterDB();
              mLLRegisterDB.setRgtNo(tES_DOC_MAINSET.get(1).getDocCode());
              if(!mLLRegisterDB.getInfo())
              {
            	  CError tError = new CError();
                  tError.moduleName = "DocLPUploadRelationService";
                  tError.functionName = "Save";
                  tError.errorNo = "-99";
                  tError.errorMessage = "上传失败！单证号"+tES_DOC_MAINSET.get(1).getDocCode()+"不是正确的理赔批次号!";
                  this.mErrors.addOneError(tError);
                  System.out.println("return false\n"+tError.errorMessage);
                  return false;
              }
          }else if(!"".equals(tES_DOC_MAINSET.get(1).getSubType())&& null != tES_DOC_MAINSET.get(1).getSubType()
        		  && "LP03".equals(tES_DOC_MAINSET.get(1).getSubType()))
          {
        	  LLPrepaidClaimDB mLLPrepaidClaimDB = new LLPrepaidClaimDB();
        	  mLLPrepaidClaimDB.setPrepaidNo(tES_DOC_MAINSET.get(1).getDocCode());
              if(!mLLPrepaidClaimDB.getInfo())
              {
            	  CError tError = new CError();
                  tError.moduleName = "DocLPUploadRelationService";
                  tError.functionName = "Save";
                  tError.errorNo = "-99";
                  tError.errorMessage = "上传失败！单证号"+tES_DOC_MAINSET.get(1).getDocCode()+"不是正确的预付赔款号!";
                  this.mErrors.addOneError(tError);
                  System.out.println("return false\n"+tError.errorMessage);
                  return false;
              }
          }else if(!"".equals(tES_DOC_MAINSET.get(1).getSubType())&& null != tES_DOC_MAINSET.get(1).getSubType()
        		  && "LP04".equals(tES_DOC_MAINSET.get(1).getSubType()))
          {
        	  LLSurveyDB mLLSurveyDB = new LLSurveyDB();
        	  mLLSurveyDB.setSurveyNo(tES_DOC_MAINSET.get(1).getDocCode());
              if(mLLSurveyDB.query().size()<=0)
              {
            	  CError tError = new CError();
                  tError.moduleName = "DocLPUploadRelationService";
                  tError.functionName = "Save";
                  tError.errorNo = "-99";
                  tError.errorMessage = "上传失败！单证号"+tES_DOC_MAINSET.get(1).getDocCode()+"不是正确的理赔调查号!";
                  this.mErrors.addOneError(tError);
                  System.out.println("return false\n"+tError.errorMessage);
                  return false;
              }
          }          
          
          ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new
                  ES_DOC_RELATIONSchema();
          mES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
          mES_DOC_RELATIONSchema.setBussNoType("21");
          mES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
          mES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).
                                             getBussType());
          mES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
          mES_DOC_RELATIONSchema.setRelaFlag("0");
          if (mES_DOC_RELATIONSchema != null) {
              map.put(mES_DOC_RELATIONSchema, "INSERT");
          }
          if (map != null) {
              //返回数据map
              mResult.add(map);
          } else {
              CError tError = new CError();
              tError.moduleName = "DocTBWorkFlowService";
              tError.functionName = "Save";
              tError.errorNo = "-99";
              tError.errorMessage = "数据生成出错!";
              this.mErrors.addOneError(tError);
              return false;
          }
      } catch (Exception e) {
          e.printStackTrace();
      }
      return true;
  }

  public CErrors getErrors() {
    return mErrors;
  }

  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    //test
    LGWorkSchema mLGWorkSchema = new LGWorkSchema();
    mLGWorkSchema.setAcceptNo("20050228000004");
    mLGWorkSchema.setWorkNo("20050228000004");
    mLGWorkSchema.setOperator("001");
    mLGWorkSchema.setMakeDate("2005-02-28");
    mLGWorkSchema.setMakeTime("15:07:07");
    mLGWorkSchema.setModifyDate("2005-02-28");
    mLGWorkSchema.setModifyTime("15:07:07");
    mLGWorkSchema.setAcceptCom("8600000");

    MMap map = new MMap();
    map.put(mLGWorkSchema, "INSERT");
    PubSubmit pubsubmit = new PubSubmit();

    VData mResult1 = new VData();
    mResult1.add(map);
    if (pubsubmit.submitData(mResult1, "")) {
      System.out.println("成功！！");
    }

  }
}
