package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LockTableDB;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class DocTBAdditionalResServiece implements EasyScanService
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocTBAdditionalResServiece()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            MMap map = new MMap();
            System.out.println("DocTBAdditionalResServiece Begin");

            ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
            if (tES_DOC_MAINSET.size() != 1)
            {
                System.out.println("tES_DOC_MAINSET.size() != 1");
                CError tError = new CError();
                tError.moduleName = "DocTBAdditionalResServiece";
                tError.functionName = "submitData";
                tError.errorNo = "-99";
                tError.errorMessage = "传入数据出错!";
                return false;
            }

            ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
            mES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1)
                    .getDocCode());
            mES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(1)
                    .getDocCode());
            mES_DOC_RELATIONSchema.setBussNoType("11");
            mES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
            mES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1)
                    .getBussType());
            mES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1)
                    .getSubType());
            mES_DOC_RELATIONSchema.setRelaFlag("0");
            if (mES_DOC_RELATIONSchema != null)
            {
                map.put(mES_DOC_RELATIONSchema, "INSERT");
            }
            if (map != null)
            {
                //返回数据map
                mResult.add(map);
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "DocTBAdditionalResServiece";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "数据生成出错!";
                return false;
            }
            
            LCContDB mLCContDB = new LCContDB();
            mLCContDB.setPrtNo(tES_DOC_MAINSET.get(1).getDocCode());
            mLCContDB.setCardFlag("9");
            LCContSet tLCContSet = mLCContDB.query();
            
            LBContDB mLBContDB = new LBContDB();
            mLBContDB.setPrtNo(tES_DOC_MAINSET.get(1).getDocCode());
            mLBContDB.setCardFlag("9");
            LBContSet tLBContSet = mLBContDB.query();
            if (tLCContSet.size() == 0 && tLBContSet.size() == 0) {
                CError tError = new CError();
                tError.moduleName = "DocTBAdditionalResServiece";
                tError.functionName = "submitData";
                tError.errorMessage = "数据传入有错误,不存在此数据!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            String sql = "select 1 from ES_DOC_MAIN "
                + "where DocCode = '"
                + tES_DOC_MAINSET.get(1).getDocCode()
                + "' " ; //印刷号已生成保单


	        String temp = new ExeSQL().getOneValue(sql);
	        if (temp != null && !temp.equals("") && !temp.equals("null"))
	        {
	            mErrors.addOneError("印刷号已扫描或已录入保单，不能再扫描");
	            return false;
	        }
            
            //向LockTable表中置数据。
            /**
             * 增加并发查询设计，主要由于
             */
            LockTableDB tLockTableDB = new LockTableDB();
            tLockTableDB.setNoType("SI");
            tLockTableDB.setNoLimit(tES_DOC_MAINSET.get(1).getDocCode());
            if(tLockTableDB.getInfo()){
            	String date = tLockTableDB.getMakeDate();
            	String time = tLockTableDB.getMakeTime();
            	
            	DateFormat  df  =  new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.SIMPLIFIED_CHINESE); 
            	Date bef = null;
    			Date now = new Date();
    			try {
    				bef = df.parse(date+" "+time);
    			} catch (ParseException e) {
    				e.printStackTrace();
    			}
    			
    			long between = (now.getTime() - bef.getTime()) / 1000;// 除以1000是为了转换成秒
    			long day = between / (24 * 3600);
    			long hour = between % (24 * 3600) / 3600;
    			long minute = between % 3600 / 60;
    			long second = between % 60 / 60;
    			
    			if(day>0 || hour>0 || minute > 0 || second > 30){
    				map.put(tLockTableDB.getSchema(), SysConst.DELETE);
    			}else{
    				CError tError = new CError();
    	            tError.moduleName = "DocTBWorkFlowService";
    	            tError.functionName = "Save";
    	            tError.errorNo = "-99";
    	            tError.errorMessage = "为了保证扫描上载避免重复导入，现控制同一单证号码在30秒不得重复上载。目前还有："+(30-second)+"秒";
    	            this.mErrors.addOneError(tError);
    	            return false;
    			}
            }
            LockTableSchema tLockTableSchema = new LockTableSchema();
            tLockTableSchema.setNoType("SI");
            tLockTableSchema.setNoLimit(tES_DOC_MAINSET.get(1).getDocCode());
            tLockTableSchema.setMakeDate(PubFun.getCurrentDate());
            tLockTableSchema.setMakeTime(PubFun.getCurrentTime());
            map.put(tLockTableSchema, SysConst.INSERT);


            // 修改调用该段程序后，会覆盖原 es_doc_main 表中，某些字段的数据库默认值。
            // gongqun2007-4-12
            //ES_DOC_MAINSchema aES_DOC_MAINSchema = tES_DOC_MAINSchema.getSchema();
            String archiveNo = createArchiveNo(tES_DOC_MAINSET.get(1).getManageCom());
            if (archiveNo == null)
            {
                return false;
            }
            //tES_DOC_MAINSchema.setArchiveNo(archiveNo);
            //tES_DOC_MAINSchema.setState("01");
            double tDocId = tES_DOC_MAINSET.get(1).getDocID();
            String tStrSql = "update ES_Doc_Main " + " set ArchiveNo = '"
                    + archiveNo + "', " + " State = '01', "
                    + " ModifyDate = current date, ModifyTime = current time "
                    + " where DocId = " + tDocId;
            //MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
            map.put(tStrSql, SysConst.UPDATE);
            //map.put(aES_DOC_MAINSchema, SysConst.UPDATE);

            //System.out.println("\n\n\n\n设置归档号：" + tES_DOC_MAINSchema.getArchiveNo());
            System.out.println("\n\n\n\n设置归档号：" + tStrSql);

            System.out.println("----------------WorkFlow End-----------------");
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }
    
    /** gongqun2007-4-12
     * 生成归档号es_doc_main.ArchiveNo
     * @return String
     */
    public String createArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "YBT" + comPart + tDate;
        String tArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tArchiveNo;
    }
    
    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        String sql = "select max(DocID) from ES_DOC_MAIN where doccode = '16000005397'";
        String maxDocID = new ExeSQL().getOneValue(sql);

        ES_DOC_MAINDB db = new ES_DOC_MAINDB();
        db.setDocID(maxDocID);
        db.getInfo();
        ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
        tES_DOC_MAINSet.add(db.getSchema());

        VData d = new VData();
        d.add(tES_DOC_MAINSet);

        DocTBAdditionalResServiece bl = new DocTBAdditionalResServiece();
        if (!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }

    }
    
}
