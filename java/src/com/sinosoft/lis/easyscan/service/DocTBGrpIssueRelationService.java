package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 体检单证上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocTBGrpIssueRelationService
    implements EasyScanService {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 传出数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public DocTBGrpIssueRelationService() {
  }

  public boolean submitData(VData cInputData, String cOperate) {
    try
    {
      MMap map = new MMap();
      System.out.println( "----------------------DocTBGrpIssueWorkRelationService Begin----------------------");

      /**得到具体信息**/
      ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
      if (tES_DOC_MAINSET.size() != 1) {
        System.out.println("2");
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "传入数据出错!";
        return false;
      }
      ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
      mES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(1).getDocCode());
      mES_DOC_RELATIONSchema.setBussNoType("23");
      mES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(1).getDocID());
      mES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(1).getBussType());
      mES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(1).getSubType());
      mES_DOC_RELATIONSchema.setRelaFlag("0");
      if (mES_DOC_RELATIONSchema != null) {
        map.put(mES_DOC_RELATIONSchema,"INSERT");
      }
      StringBuffer sql=new StringBuffer();
      sql.append("update LCGrpIssuePol set state='");
      sql.append(4);
      sql.append("',modifydate='");
      sql.append(PubFun.getCurrentDate());
      sql.append("',modifytime='");
      sql.append(PubFun.getCurrentTime());
      sql.append("' where prtseq='");
      sql.append(tES_DOC_MAINSET.get(1).getDocCode());
      sql.append("' and state='3'");
      map.put(sql.toString(), "UPDATE");
      System.out.println("aaa"+ tES_DOC_MAINSET.get(1).getDocCode().substring(0,11));
      map.put("update Lwmission set activitystatus='3',"
              + "ModifyDate='" +PubFun.getCurrentDate() +
              "',ModifyTime='" +PubFun.getCurrentTime() +
              "' where Missionprop2='" +
              tES_DOC_MAINSET.get(1).getDocCode().substring(0,11)+ "' and activityid='0000002004'  "
        , "UPDATE");

      if (map != null) {
        //返回数据map
        mResult.add(map);
      }
      else {
        CError tError = new CError();
        tError.moduleName = "DocTBWorkFlowService";
        tError.functionName = "Save";
        tError.errorNo = "-99";
        tError.errorMessage = "数据生成出错!";
        return false;
      }
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return true;

  }

  public VData getResult() {
    return mResult;
  }
  public CErrors getErrors()
  {
    return mErrors;
  }

}
