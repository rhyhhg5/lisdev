package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 *
 * <p>
 * Description: 保全续保待核体检通知书上传处理接口
 * 
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Houyd
 * @version 1.0
 */
import com.sinosoft.lis.cbcheck.UWIndStateUI;
import com.sinosoft.lis.db.LPIssuePolDB;
import com.sinosoft.lis.db.LPPENoticeDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.easyscan.EasyScanService;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.lis.schema.LPIssuePolSchema;
import com.sinosoft.lis.schema.LPPENoticeSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class DocBQUWReplyService
    implements EasyScanService {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 问题卷流水号 */
  private String mPrtSeq;
  /** 问题卷类型 BQ03-体检通知书；BQ04-问题件通知书*/
  private String mSubType;
  
  private GlobalInput mGlobalInput;

  private ES_DOC_MAINSet nES_DOC_MAINSet;
  private ES_DOC_PAGESSet nES_DOC_PAGESSet;
  private ES_DOC_MAINSchema nES_DOC_MAINSchema;
  private ES_DOC_PAGESSchema nES_DOC_PAGESSchema;
  private static final String CON_STATUES = "1"; //问题卷状态：1-已回销
  private static final String CON_RESULT = "y"; //回销结果

  public DocBQUWReplyService() {
    try {
      jbInit();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData(String cOperate) {
    try {
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData()) {
        return false;
      }
      //检查外部传入的数据
      if (!checkInputData()) {
        return false;
      }
      //
      if(!dealRelationService()){
    	  return false;
      }
      //进行业务处理,准备返回数据
      if (!getReturnData()) {
        return false;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return true;
  }

  /**
   * 关系处理，使其可以查询到影像
   * @return
   */
  private boolean dealRelationService(){

	    try
	    {
	      MMap map = new MMap();
	      System.out.println( "----------------------DocBQUWReplyService Begin----------------------");

	      /** 在relation 中存储数据，用于页面的回显*/
	      ES_DOC_RELATIONSchema mES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
	      mES_DOC_RELATIONSchema.setBussNo(nES_DOC_MAINSet.get(1).getDocCode());
	      mES_DOC_RELATIONSchema.setBussNoType("92");
	      mES_DOC_RELATIONSchema.setDocID(nES_DOC_MAINSet.get(1).getDocID());
	      mES_DOC_RELATIONSchema.setBussType(nES_DOC_MAINSet.get(1).getBussType());
	      mES_DOC_RELATIONSchema.setSubType(nES_DOC_MAINSet.get(1).getSubType());
	      mES_DOC_RELATIONSchema.setRelaFlag("0");
	      if (mES_DOC_RELATIONSchema != null) {
	        map.put(mES_DOC_RELATIONSchema,"INSERT");
	      }

	      if (map != null) {
	        //返回数据map
	        mResult.add(map);
	      }
	      else {
	        CError tError = new CError();
	        tError.moduleName = "DocBQUWReplyService";
	        tError.functionName = "Save";
	        tError.errorNo = "-99";
	        tError.errorMessage = "数据生成出错!";
	        return false;
	      }
	    }catch(Exception e)
	    {
	      e.printStackTrace();
	    }
	    return true;
  }
  
  /**
   * 从输入数据中得到所有对象
   * @param: cInputData 输入数据
   * @return:如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData() {
    System.out.println("DocBQUWReplyService: Getting Input Data ...");
    nES_DOC_MAINSet = (ES_DOC_MAINSet) mInputData.get(0);
    nES_DOC_PAGESSet = (ES_DOC_PAGESSet) mInputData.get(1);
    mGlobalInput = (GlobalInput) mInputData.get(5);
    mPrtSeq = nES_DOC_MAINSet.get(1).getDocCode();
    mSubType = nES_DOC_MAINSet.get(1).getSubType();
    return true;
  }

  /**
   * 校验传入的数据的合法性
   * @param: void
   * @return:如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean checkInputData() {
    try {
      System.out.println(
          "DocBQUWReplyService: Checking input data ...");
      if (nES_DOC_MAINSet.size() != 1) {
        CError tError = new CError();
        tError.moduleName = "DocBQUWReplyService";
        tError.functionName = "Save";
        tError.errorNo = "-9999";
        tError.errorMessage = "回销问题卷时,数据传输出错:单证个数不合法!";
        this.mErrors.addOneError(tError);
        return false;
      }
      nES_DOC_MAINSchema = nES_DOC_MAINSet.get(1);

      if (nES_DOC_PAGESSet.size() < 1) {
        CError tError = new CError();
        tError.moduleName = "DocBQUWReplyService";
        tError.functionName = "Save";
        tError.errorNo = "-500";
        tError.errorMessage = "回销问题卷时,数据传输出错:单证页数不合法!";
        this.mErrors.addOneError(tError);
        return false;
      }
      nES_DOC_PAGESSchema = nES_DOC_PAGESSet.get(1);
    }
    catch (Exception ex) {
      // @@错误处理
      System.out.println(
          "Exception in DocBQUWReplyService->checkInputData");
      System.out.println("Exception:" + ex.toString());
      CError tError = new CError();
      tError.moduleName = "DocBQUWReplyService";
      tError.functionName = "checkInputData";
      tError.errorNo = "-1500";
      tError.errorMessage = "回销问题卷时,出现意外错误!\n错误信息如下:\n" + ex.toString();
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 进行业务处理,准备返回数据
   * @param: void
   * @return:如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean getReturnData() {
    System.out.println(
        "DocBQUWReplyService: start get Return Data  ...");
    if("BQ03".equals(mSubType)){
    	System.out.println(
        "DocBQUWReplyService: start get Health Return Data  ...");
    	if(!dealHealth()){
        	
        	return false;
        }
    }else if("BQ04".equals(mSubType)){
    	System.out.println(
        "DocBQUWReplyService: start get Quest Return Data  ...");
    	if(!dealQuest()){
        	
        	return false;
        }	
    }else{
    	CError tError = new CError();
        tError.moduleName = "DocBQUWReplyService";
        tError.functionName = "getReturnData";
        tError.errorNo = "-500";
        tError.errorMessage = "回销体检问题卷,暂不支持此类型的操作";
        this.mErrors.addOneError(tError);
        return false;
    	
    }
    
    
    
    return true;
  }
  
  /**
   * 处理问题件通知书
   * @return
   */
  private boolean dealQuest(){
	    try {
	        MMap map = new MMap();
	        LPIssuePolDB tLPIssuePolDB = new LPIssuePolDB();
	        LPIssuePolSchema tLPIssuePolSchema = new LPIssuePolSchema();
	        tLPIssuePolDB.setPrtSeq(mPrtSeq);
	        tLPIssuePolSchema = tLPIssuePolDB.query().get(1);
	        String tEdorNo = tLPIssuePolSchema.getEdorNo();
	        if(tEdorNo != null && !"".equals(tEdorNo)){//modify by Houyd 只要上传，则改变状态
	          //一个工单下最新通知书
	      	  LWMissionDB tLWMissionDB = new LWMissionDB();
	      	  LWMissionSchema tLWMissionSchema = new LWMissionSchema();
	      	  UWIndStateUI tUWIndStateUI = new UWIndStateUI();
	      	  LWMissionSet tLWMissionSet = new LWMissionSet();

	          //变更工作流状态
	      	  tLWMissionDB.setActivityID("0000001181");
	      	  tLWMissionDB.setProcessID("0000000003");
	      	  tLWMissionDB.setMissionProp12(tEdorNo);
	      	  tLWMissionSet = tLWMissionDB.query();
	      	  if(tLWMissionSet.size()<1){
	      		  //查询是否为保全的核保件
	      		  LWMissionDB tLWMissionDB1 = new LWMissionDB();
	      		tLWMissionDB1.setActivityID("0000001180");
	      		tLWMissionDB1.setProcessID("0000000003");
	      		tLWMissionDB1.setMissionProp1(tEdorNo);
		      	  tLWMissionSet = tLWMissionDB1.query();
	      	  }
	      	  tLWMissionSchema = tLWMissionSet.get(1).getSchema();
	      	  tLWMissionSchema.setActivityStatus("1");
	      		  
	      	  String tOperate = "UPDATE||MAIN";
	          VData tVData = new VData();
	          tVData.add(tLWMissionSchema);
	          tVData.add(mGlobalInput);
	          tUWIndStateUI.submitData(tVData,tOperate);	
	          /**
	          String tSql = "select max(prtSeq) from LPIssuePol where edorno='"+tEdorNo+"'";
	          ExeSQL tExeSql = new ExeSQL();
	          SSRS tSSRS = new SSRS();
	          tSSRS = tExeSql.execSQL(tSql);
	          String tPrtSeq = tSSRS.GetText(1, 1);
	          if(!mPrtSeq.equals(tPrtSeq)){
	        	  CError tError = new CError();
	              tError.moduleName = "DocBQUWReplyService";
	              tError.functionName = "getReturnData";
	              tError.errorNo = "-500";
	              tError.errorMessage = "回销问题卷,请上载最新的问题件通知书,编号："+tPrtSeq+"";
	              this.mErrors.addOneError(tError);
	              return false;
	          }else{
	        	  //一个工单下最新通知书
		      	  LWMissionDB tLWMissionDB = new LWMissionDB();
		      	  LWMissionSchema tLWMissionSchema = new LWMissionSchema();
		      	  UWIndStateUI tUWIndStateUI = new UWIndStateUI();

		          //变更工作流状态
		      	  tLWMissionDB.setActivityID("0000001181");
		      	  tLWMissionDB.setProcessID("0000000003");
		      	  tLWMissionDB.setMissionProp12(tEdorNo);
		      	  tLWMissionSchema = tLWMissionDB.query().get(1).getSchema();
		      	  tLWMissionSchema.setActivityStatus("1");
		      		  
		      	  String tOperate = "UPDATE||MAIN";
		          VData tVData = new VData();
		          tVData.add(tLWMissionSchema);
		          tVData.add(mGlobalInput);
		          tUWIndStateUI.submitData(tVData,tOperate);		      	    
	          }
	          */
	          
	    	  //变更问题件状态
	          tLPIssuePolSchema.setOperator(mGlobalInput.Operator);
	          tLPIssuePolSchema.setModifyDate(PubFun.getCurrentDate());
	          tLPIssuePolSchema.setModifyTime(PubFun.getCurrentTime());
	    	  map.put(tLPIssuePolSchema, "UPDATE");
	    	  if (map != null) {
	              //返回数据map
	              mResult.add(map);
	          }	  
	        }else{
	      	  CError tError = new CError();
	          tError.moduleName = "DocBQUWReplyService";
	          tError.functionName = "getReturnData";
	          tError.errorNo = "-500";
	          tError.errorMessage = "回销问题卷,在提交数据时出错!";
	          this.mErrors.addOneError(tError);
	          return false;
	        }
	      }
	      catch (Exception ex) {
	        // @@错误处理
	        ex.printStackTrace();
	        CError tError = new CError();
	        tError.moduleName = "DocBQUWReplyService";
	        tError.functionName = "getReturnData";
	        tError.errorNo = "-1500";
	        tError.errorMessage = "回销问题卷时,出现意外错误!\n错误信息如下:\n" + ex.toString();
	        this.mErrors.addOneError(tError);
	        return false;
	      }
	      return true;
  }
  
  /**
   * 处理体检通知书
   * @return
   */
  private boolean dealHealth(){
	    try {
	        MMap map = new MMap();
	        LPPENoticeDB tLPPENoticeDB = new LPPENoticeDB();
	        LPPENoticeSchema tLPPENoticeSchema = new LPPENoticeSchema();
	        tLPPENoticeDB.setPrtSeq(mPrtSeq);
	        tLPPENoticeSchema = tLPPENoticeDB.query().get(1);
	        String tEdorNo = tLPPENoticeSchema.getEdorNo();
	        if(tEdorNo != null && !"".equals(tEdorNo)){ //modify by Houyd 只要上传，则改变状态
	        	
        	  LWMissionDB tLWMissionDB = new LWMissionDB();
	      	  LWMissionSchema tLWMissionSchema = new LWMissionSchema();
	      	  UWIndStateUI tUWIndStateUI = new UWIndStateUI();
	      	  
	      	  //变更工作流状态
    		  tLWMissionDB.setActivityID("0000001181");
    		  tLWMissionDB.setProcessID("0000000003");
    		  tLWMissionDB.setMissionProp12(tEdorNo);
    		  
    		  LWMissionSet tLWMissionSet = new LWMissionSet();
    		  tLWMissionSet = tLWMissionDB.query();
    		  if(tLWMissionSet==null||tLWMissionSet.size()<1){
    			  tLWMissionDB.setActivityID("0000001180");
    			  tLWMissionDB.setProcessID("0000000003");
    			  tLWMissionDB.setMissionProp1(tEdorNo);
    			  tLWMissionSet = tLWMissionDB.query();
    		  }
    		  if(tLWMissionSet==null||tLWMissionSet.size()<1){
	        	  CError tError = new CError();
		          tError.moduleName = "DocBQRReplyService";
		          tError.functionName = "getReturnData";
		          tError.errorNo = "-500";
		          tError.errorMessage = "回销体检问题卷,核保信息查询失败!";
		          this.mErrors.addOneError(tError);
		          return false;  
    		  }
    		  tLWMissionSchema = tLWMissionSet.get(1).getSchema();
    		  tLWMissionSchema.setActivityStatus("1");
			  
			  String tOperate = "UPDATE||MAIN";
	          VData tVData = new VData();
	  		  tVData.add(tLWMissionSchema);
	  		  tVData.add(mGlobalInput);
	  		  tUWIndStateUI.submitData(tVData,tOperate);
	        	
	          /**
	      	  LPPENoticeDB aLPPENoticeDB = new LPPENoticeDB();
	      	  LPPENoticeSet aLPPENoticeSet = new LPPENoticeSet();
	      	  aLPPENoticeDB.setEdorNo(tEdorNo);
	      	  aLPPENoticeSet = aLPPENoticeDB.query();
	      	  //一个工单下通知书的数目
	      	  LWMissionDB tLWMissionDB = new LWMissionDB();
	      	  LWMissionSchema tLWMissionSchema = new LWMissionSchema();
	      	  UWIndStateUI tUWIndStateUI = new UWIndStateUI();
	      	  if(aLPPENoticeSet.size() == 1){
	      		  //变更工作流状态
	      		  tLWMissionDB.setActivityID("0000001181");
	      		  tLWMissionDB.setProcessID("0000000003");
	      		  tLWMissionDB.setMissionProp12(tEdorNo);
	      		  tLWMissionSchema = tLWMissionDB.query().get(1).getSchema();
	      		  tLWMissionSchema.setActivityStatus("1");
	      		  
	      		  String tOperate = "UPDATE||MAIN";
	                VData tVData = new VData();
	        		  tVData.add(tLWMissionSchema);
	        		  tVData.add(mGlobalInput);
	        		  tUWIndStateUI.submitData(tVData,tOperate);
	      	  }else{
	      		  int tCount = 0;
	      		  for(int i=1 ;i<=aLPPENoticeSet.size(); i++){  
	      			  if("01".equals(aLPPENoticeSet.get(i).getPEState())){//存在已经上传的问题件
	      				  if(mPrtSeq.equals(aLPPENoticeSet.get(i).getPrtSeq())){
	      					  CError tError = new CError();
	      			          tError.moduleName = "DocBQUWReplyService";
	      			          tError.functionName = "getReturnData";
	      			          tError.errorNo = "-500";
	      			          tError.errorMessage = "回销体检问题卷,工单"+tEdorNo+"下此体检通知书已经回销!";
	      			          this.mErrors.addOneError(tError);
	      			          return false;
	      				  }
	      				  tCount++;
	      				  continue;
	      			  }
	      		  }
	      		  if(tCount == aLPPENoticeSet.size()-1){//此时上传的是最后一个问题件
	      			  //变更工作流状态
	          		  tLWMissionDB.setActivityID("0000001181");
	          		  tLWMissionDB.setProcessID("0000000003");
	          		  tLWMissionDB.setMissionProp12(tEdorNo);
	          		  tLWMissionSchema = tLWMissionDB.query().get(1).getSchema();
	          		  tLWMissionSchema.setActivityStatus("1");
	      			  
	      			  String tOperate = "UPDATE||MAIN";
	      	          VData tVData = new VData();
	      	  		  tVData.add(tLWMissionSchema);
	      	  		  tVData.add(mGlobalInput);
	      	  		  tUWIndStateUI.submitData(tVData,tOperate);
	      		  }
	      	  }
	      	  */
	    		  
	    		//变更体检问题件状态
	    		tLPPENoticeSchema.setPEState("01");//null:'体检通知书未录入';'01':'体检通知书已录入';'02':体检通知书已打印
	    		tLPPENoticeSchema.setOperator(mGlobalInput.Operator);
	    		tLPPENoticeSchema.setModifyDate(PubFun.getCurrentDate());
	    		tLPPENoticeSchema.setModifyTime(PubFun.getCurrentTime());
	    		map.put(tLPPENoticeSchema, "UPDATE");
	    		if (map != null) {
	              //返回数据map
	              mResult.add(map);
	            }
	        }else {
	        	  CError tError = new CError();
		          tError.moduleName = "DocBQRReplyService";
		          tError.functionName = "getReturnData";
		          tError.errorNo = "-500";
		          tError.errorMessage = "回销体检问题卷,在提交数据时出错!";
		          this.mErrors.addOneError(tError);
		          return false;        	  
	        }
	      }
	      catch (Exception ex) {
	        // @@错误处理
	        ex.printStackTrace();
	        CError tError = new CError();
	        tError.moduleName = "DocBQUWReplyService";
	        tError.functionName = "getReturnData";
	        tError.errorNo = "-1500";
	        tError.errorMessage = "回销问题卷时,出现意外错误!\n错误信息如下:\n" + ex.toString();
	        this.mErrors.addOneError(tError);
	        return false;
	      }
	      return true;
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return: 操作成功返回true，失败返回false
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println(
        "----------------------DocBQUWReplyService Begin----------------------");
    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    //业务处理
    if (!dealData(cOperate)) {
      return false;
    }
    return true;
  }

  /**
   * 获得操作结果
   * @param: void
   * @return:结果数据对象集合
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 获得错误集合
   * @param: void
   * @return:错误对象集合
   */
  public CErrors getErrors() {
    return mErrors;
  }

  public static void main(String[] args) {
    DocBQUWReplyService nDocIssueDocReplyWorkService = new
        DocBQUWReplyService();
    ES_DOC_MAINSet nES_DOC_MAINSet = new ES_DOC_MAINSet();
    ES_DOC_MAINSchema nES_DOC_MAINSchema = new ES_DOC_MAINSchema();
    //根据调试需要,为nES_DOC_MAINSchema各个字段赋上值,然后添加到nES_DOC_MAINSet
    nES_DOC_MAINSchema.setBussType("LP");
    nES_DOC_MAINSchema.setDocCode("88888888888888");
    nES_DOC_MAINSchema.setDocID("56");
    nES_DOC_MAINSet.add(nES_DOC_MAINSchema);

    ES_DOC_PAGESSet nES_DOC_PAGESSet = new ES_DOC_PAGESSet();
    ES_DOC_PAGESSchema nES_DOC_PAGESSchema = new ES_DOC_PAGESSchema();
    //根据调试需要,为nES_DOC_PAGESSchema各个字段赋上值,然后添加到nES_DOC_PAGESSet
    nES_DOC_PAGESSchema.setDocID("56");
    nES_DOC_PAGESSet.add(nES_DOC_PAGESSchema);

    VData vData = new VData();
    vData.add(nES_DOC_MAINSet);
    vData.add(nES_DOC_PAGESSet);
    if (nDocIssueDocReplyWorkService.submitData(vData, "")) {
      System.out.println("Congratulations!");
    }
    else {
      System.out.println("I am sorry,try again!");
      System.out.println(nDocIssueDocReplyWorkService.getErrors().getErrContent());
    }
  }

  private void jbInit() throws Exception {
  }
}
