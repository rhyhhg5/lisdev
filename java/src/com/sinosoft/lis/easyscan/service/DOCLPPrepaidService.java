package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 预付赔款上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author maning
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class DOCLPPrepaidService implements EasyScanService {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public DOCLPPrepaidService() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		try {
			MMap map = new MMap();
			System.out
					.println("----------DOCLPPrepaidService Begin----------");
			ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
			
			LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
			LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
			/** 得到具体信息* */

			ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
			if (tES_DOC_MAINSET.size() != 1) {
				System.out.println("2");
				CError tError = new CError();
				tError.moduleName = "DocLPStatusChangeService";
				tError.functionName = "submitData";
				tError.errorNo = "-99";
				tError.errorMessage = "传入数据出错!";
				return false;
			}
			tLLPrepaidClaimDB.setPrepaidNo(tES_DOC_MAINSET.get(1).getDocCode());
			if (tLLPrepaidClaimDB.getInfo()) {
				tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
				if (tLLPrepaidClaimSchema.getRgtState().equals("01")) {
					tLLPrepaidClaimSchema.setRgtState("02");
					tLLPrepaidClaimSchema.setOperator(tES_DOC_MAINSET.get(1)
							.getScanOperator());
					tLLPrepaidClaimSchema.setModifyDate(PubFun.getCurrentDate());
					tLLPrepaidClaimSchema.setModifyTime(PubFun.getCurrentTime());
					map.put(tLLPrepaidClaimSchema, "UPDATE");
				}
			} else {
				CError tError = new CError();
				tError.moduleName = "DOCLPPrepaidService";
				tError.functionName = "Save";
				tError.errorNo = "-99";
				tError.errorMessage = "上传失败！单证号"
						+ tES_DOC_MAINSET.get(1).getDocCode() + "不是正确的预付赔款号!";
				System.out.println("return false\n" + tError.errorMessage);
				return false;
			}
			if (map != null) {
				// 返回数据map
				mResult.add(map);
			} else {
				CError tError = new CError();
				tError.moduleName = "DOCLPStatusChangeService";
				tError.functionName = "Save";
				tError.errorNo = "-99";
				tError.errorMessage = "没有找到相应的数据!";
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
		// test
		ES_DOC_MAINSet aES_DOC_MAINSet = new ES_DOC_MAINSet();
		ES_DOC_MAINSchema aES_DOC_MAINSchema = new ES_DOC_MAINSchema();
		aES_DOC_MAINSchema.setBussType("LP");
		aES_DOC_MAINSchema.setSubType("LP02");
		aES_DOC_MAINSchema.setDocCode("P9400060815000005");
		aES_DOC_MAINSchema.setDocFlag("1");
		aES_DOC_MAINSchema.setDocID("29018");
		aES_DOC_MAINSet.add(aES_DOC_MAINSchema);
		VData aVData = new VData();
		aVData.add(aES_DOC_MAINSet);
		DOCLPStatusChangeService tService = new DOCLPStatusChangeService();
		if (tService.submitData(aVData, "INSERT")) {
			System.out.println("成功！！");
		}

	}
}
