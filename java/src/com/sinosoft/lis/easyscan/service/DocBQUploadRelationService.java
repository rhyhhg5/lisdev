package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单上传后对于关联表的的操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.TaskDeliverBL;
import com.sinosoft.utility.*;
import com.sinosoft.task.TaskInputBL;


public class DocBQUploadRelationService implements EasyScanService
{
    /** 传出数据的容器 */
    private VData mResult = new VData();

    private String workNoForDelever = "";
    private boolean deliverFlag = true;
    private String nodeNo = "0"; //在生成历史信息节点详细信息是使用，默认为生成新工单

    /** 数据操作字符串 */
    private String mOperate;

    //公共录入信息
    private GlobalInput gi = new GlobalInput();

    private LGWorkSchema mLGWorkSchema = new LGWorkSchema();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocBQUploadRelationService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            MMap map = new MMap();
            System.out.println("------DocBQUploadRelationService Begin-----");
            ES_DOC_RELATIONSchema tES_DOC_RELATIONSchema = new
                    ES_DOC_RELATIONSchema();

            LGWorkTraceSchema wt = new LGWorkTraceSchema();

            /**得到具体信息**/
            ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
            if (tES_DOC_MAINSET.size() < 1)
            {
                mErrors.addOneError("传入数据出错");
                return false;
            }
            String tno = "";

            for (int i = 1; i <= tES_DOC_MAINSET.size(); i++)
            {
                //获得扫描的第一页,检验是否是补扫描
                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                TaskInputBL tTaskInputBL = new TaskInputBL(); //需要生成新工单时使用
                String sql = "select acceptNo, typeNo, nodeNo " +
                             "from LGWork " +
                             "where acceptNo='" +
                             tES_DOC_MAINSET.get(i).getDocCode() + "' ";
                tSSRS = tExeSQL.execSQL(sql);

                if (tSSRS.getMaxRow() == 1)
                {
                    //是补扫描则不需要生成新工单
                    tno = tSSRS.GetText(1, 1);
                    deliverFlag = false;
                    nodeNo = tSSRS.GetText(1, 3);

                    //将工单标识为有新扫描件
                    LGWorkDB db = new LGWorkDB();
                    db.setWorkNo(tES_DOC_MAINSET.get(i).getDocCode());
                    db.getInfo();

                    db.setScanFlag("0");
//                    db.setOperator(tES_DOC_MAINSET.get(i).getScanOperator());
                    db.setModifyDate(tES_DOC_MAINSET.get(i).getMakeDate());
                    db.setModifyTime(tES_DOC_MAINSET.get(i).getMakeTime());
                    map.put(db.getSchema(), "DELETE&INSERT");

                    //若是函件回复扫描将保全申请状态置为函件回销待处理,函件状态亦为待处理,第一次循环
                    if (i == 1 && tES_DOC_MAINSET.get(i).getSubType().equals("BQ02"))
                    {
                    	LGWorkDB tLGWorkDB = new LGWorkDB();
                        String checkSQL ="select * from LGWork where workno ='" + tES_DOC_MAINSET.get(i).getDocCode()
                                         + "' and statusNo in ('4','5','6','7','8','99')"
                                         ;
                        LGWorkSet tLGWorkSet = tLGWorkDB.executeQuery(checkSQL);
                        if(tLGWorkSet==null||tLGWorkSet.size()==0)
                        {
                            //将保全业务状态改为函件回销待处理
                            LPEdorAppSchema schema = changeLPEdorAppState(
                                    tES_DOC_MAINSET.get(i));
                            if (schema == null) {
                                return false;
                            }
                            map.put(schema, "DELETE&INSERT");

                            //将函件状态改为回销待处理
                            LGLetterSchema s = changeLGLetterState(
                                    tES_DOC_MAINSET.get(i));
                            if (s == null) {
                                return false;
                            }
                            map.put(s, "DELETE&INSERT");
                        }
                    }
                }
                else
                {
                    //需要生成新工单
                    String typeNo;

                    //if_else 备用
                    if ((tES_DOC_MAINSET.get(i).getDocCode().substring(0, 2)).
                        equals("16"))
                    {
                        typeNo = "03";
                    }
                    else
                    {
                        typeNo = "03";
                    }

                    //生成工单信息
                    mLGWorkSchema.setOperator(
                        tES_DOC_MAINSET.get(i).getScanOperator());
                    mLGWorkSchema.setAcceptCom(
                        tES_DOC_MAINSET.get(i).getManageCom());
                    mLGWorkSchema.setMakeDate(
                        tES_DOC_MAINSET.get(i).getMakeDate());
                    mLGWorkSchema.setMakeTime(
                        tES_DOC_MAINSET.get(i).getMakeTime());
                    mLGWorkSchema.setModifyDate(
                        tES_DOC_MAINSET.get(i).getMakeDate());
                    mLGWorkSchema.setModifyTime(
                        tES_DOC_MAINSET.get(i).getMakeTime());
                    mLGWorkSchema.setAcceptDate(
                        tES_DOC_MAINSET.get(i).getMakeDate());
                    mLGWorkSchema.setNodeNo("0"); //当前是起始结点
                    mLGWorkSchema.setStatusNo("2"); //设置为未经办状态
                    mLGWorkSchema.setTypeNo(typeNo);
                    mLGWorkSchema.setCustomerNo("");
                    mLGWorkSchema.setScanFlag("0");

                    if (mLGWorkSchema != null)
                    {
                        VData tVData = new VData();
                        gi.Operator = tES_DOC_MAINSET.get(i).getScanOperator();
                        gi.ManageCom = tES_DOC_MAINSET.get(i).getManageCom();

                        tVData.add(mLGWorkSchema);
                        tVData.add(gi);

                        //调用工单录入BL类
                        if (!tTaskInputBL.submitData(tVData, "INSERT||MAIN"))
                        {
                            mErrors.addOneError("扫描时生成工单出错");
                            return false;
                        }

                        VData tRet = tTaskInputBL.getResult();
                        LGWorkSchema w = new LGWorkSchema();
                        w.setSchema((LGWorkSchema) tRet.
                                    getObjectByObjectName("LGWorkSchema", 0));
                        workNoForDelever = w.getWorkNo();
                        tno = w.getWorkNo();
                    }
                } //else

                //公单关联关系
                tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(i).getDocID());
                tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(i).
                                                   getBussType());
                tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(i).
                                                  getSubType());
                tES_DOC_RELATIONSchema.setRelaFlag("0");
                tES_DOC_RELATIONSchema.setBussNoType("91");
                tES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(i).
                                                  getDocCode());
                tES_DOC_RELATIONSchema.setBussNo(tno);
                if (tES_DOC_RELATIONSchema != null)
                {
                    map.put(tES_DOC_RELATIONSchema, "INSERT");
                }

                /*
                                 //扫描业务号关联
                                 tES_DOC_RELATIONSchema = new ES_DOC_RELATIONSchema();
                 tES_DOC_RELATIONSchema.setDocID(tES_DOC_MAINSET.get(i).getDocID());
                                 tES_DOC_RELATIONSchema.setBussNo(tES_DOC_MAINSET.get(i).
                                                 getDocCode());
                                 tES_DOC_RELATIONSchema.setBussType(tES_DOC_MAINSET.get(i).
                        getBussType());
                                 tES_DOC_RELATIONSchema.setSubType(tES_DOC_MAINSET.get(i).
                                                  getSubType());
                                 tES_DOC_RELATIONSchema.setDocCode(tES_DOC_MAINSET.get(i).
                                                  getDocCode());
                                 tES_DOC_RELATIONSchema.setBussNoType("31");
                                 tES_DOC_RELATIONSchema.setRelaFlag("0");
                                 if (tES_DOC_RELATIONSchema != null)
                                 {
                    map.put(tES_DOC_RELATIONSchema, "INSERT");
                                 }
                 */

                if (map != null)
                {
                    //返回数据map
                    mResult.add(map);
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "DocBQUploadRelationService";
                    tError.functionName = "Save";
                    tError.errorNo = "-99";
                    tError.errorMessage = "数据生成出错!";
                    return false;
                }

                if (deliverFlag)
                {
                    if (!delieverTask(tES_DOC_MAINSET.get(i).getDocCode(),
                                      tES_DOC_MAINSET.get(i).getManageCom()))
                    {
                        return false;
                    }
                }
            } //for
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return true;
    }

    //将保全业务状态改为函件回销待处理
    private LPEdorAppSchema changeLPEdorAppState(ES_DOC_MAINSchema schema)
    {
        LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();

        tLPEdorAppDB.setEdorAcceptNo(schema.getDocCode());
        if (!tLPEdorAppDB.getInfo())
        {
            mErrors.addOneError(
                    "没有查到受理号"
                    + tLPEdorAppDB.getEdorAcceptNo()
                    + "所对应的保全申请。");
            return null;
        }

        tLPEdorAppDB.setEdorState("6"); //函件回销待处理
        tLPEdorAppDB.setOperator(schema.getScanOperator());
        tLPEdorAppDB.setModifyDate(schema.getMakeDate());
        tLPEdorAppDB.setModifyTime(schema.getMakeTime());

        return tLPEdorAppDB.getSchema();
    }

    public LGLetterSchema changeLGLetterState(ES_DOC_MAINSchema s)
    {
        LGLetterDB db = new LGLetterDB();
//        db.setEdorAcceptNo(s.getDocCode());
//        LGLetterSet set = db.query();
//        String sql = "select * from "
//                     + " LGLetter where edorAcceptNo='" + s.getDocCode() + "' "
//                     + "    and serialNumber='" + set.size() + "' ";
        String sql="select * from LGLetter where edorAcceptNo='" +s.getDocCode()+ "' "
        		+" order by int(serialNumber) desc";
        LGLetterSet set2 = db.executeQuery(sql);
        if(set2 != null && set2.size() != 0)
        {
            set2.get(1).setState("2");
            set2.get(1).setOperator(s.getScanOperator());
            set2.get(1).setModifyDate(s.getMakeDate());
            set2.get(1).setModifyTime(s.getMakeTime());
            return  set2.get(1);
        }
        else
        {
//            mErrors.addOneError("没有查到受理号为" + s.getDocCode() + "序号为"
//                                + set.size() + "的函件");
        	mErrors.addOneError("没有查到受理号为" + s.getDocCode() + "的函件");
            return null;
        }
    }

    /**
     * 实现自动转交
     *参数condition: 扫描得到的条形码值，通过它可以查询到转交的目标心想
     * ES_DOC_MAINSchema 扫描件信息
     * */
    private boolean delieverTask(String condition, String comCode)
    {
        String tCopyFlag = "N";
        String tDeliverType = "0";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();

        //输入参数
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        LGWorkTraceSchema tLGWorkTraceSchema = new LGWorkTraceSchema();
        LGTraceNodeOpSchema mLGTraceNodeOpSchema = new LGTraceNodeOpSchema();

        //查询自动转交的信箱
        String sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where ruleContent='" + condition + "' "
                     + "    and goalType = '1' "  //扫描
                     + "    and sourceComCode = '" + comCode + "' "
                     + "    and defaultFlag='1'";
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() > 0)
        {
            tLGWorkTraceSchema.setWorkBoxNo(tSSRS.GetText(1, 1));
        }

        mLGTraceNodeOpSchema.setWorkNo(workNoForDelever);
        mLGTraceNodeOpSchema.setNodeNo(nodeNo);
        mLGTraceNodeOpSchema.setOperatorType("1");
        mLGTraceNodeOpSchema.setRemark("自动批注：保全扫描转交时自动产生");

        String workNo[] = new String[1];
        workNo[0] = workNoForDelever;
        VData tVData = new VData();

        tVData.add(gi);
        tVData.add(workNo);
        tVData.add(tCopyFlag);
        tVData.add(tDeliverType);
        tVData.add(tLGWorkSchema);
        tVData.add(tLGWorkTraceSchema);
        tVData.add(mLGTraceNodeOpSchema);

        TaskDeliverBL tTaskDeliverBL = new TaskDeliverBL();
        if (tTaskDeliverBL.submitData(tVData, "ok") == false)
        {
            CError tError = new CError();
            tError.moduleName = "DocBQUploadRelationService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = "数据生成出错!";
            return false;

        }

        return true;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        
                 ES_DOC_MAINSchema es = new ES_DOC_MAINSchema();
                 ES_DOC_MAINSet esSet = new ES_DOC_MAINSet();

                 es.setDocID("111");
                 es.setBussType("BQ");
                 es.setSubType("BQ02");
                 es.setDocCode("20141113000028");
                 es.setScanOperator("pa0001");
                 es.setManageCom("86");
                 es.setMakeDate(PubFun.getCurrentDate());
                 es.setMakeTime(PubFun.getCurrentTime());
                 es.setModifyDate(PubFun.getCurrentDate());
                 es.setModifyTime("15:07:07");
                 esSet.add(es);
         
//        ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
//        tES_DOC_MAINDB.setDocID("20141113000028");
//        tES_DOC_MAINDB.setDocCode("");
        VData v = new VData();
//        v.add(tES_DOC_MAINDB.query());
        v.add(esSet);
        DocBQUploadRelationService d = new DocBQUploadRelationService();
        d.submitData(v, "002");

//        VData vData = d.getResult();
//        DocBQUploadRelationService p = new DocBQUploadRelationService();

//        if (p.submitData(v, "001"))
//        {
//            System.out.println("OKKKKKKKKKKKKKKK");
//        }
    }
}
