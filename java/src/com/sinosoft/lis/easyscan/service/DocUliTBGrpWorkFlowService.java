package com.sinosoft.lis.easyscan.service;

/**
 * <p>Title: </p>
 * <p>Description: 团体投保单证上载工作流处理接口实现类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author tuqiang
 * @version 1.0
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.workflowengine.*;

import com.sinosoft.utility.*;

import java.sql.*;

public class DocUliTBGrpWorkFlowService implements EasyScanService
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public DocUliTBGrpWorkFlowService()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out
                .println("----------------------WorkFlow Begin----------------------");
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        GlobalInput tGlobalInput = new GlobalInput();
        /** 全局变量 */
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";

        /**得到具体信息**/

        ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) cInputData.get(0);
        if (tES_DOC_MAINSET.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = "传入数据出错!";
            return false;
        }

        ES_DOC_MAINSchema tES_DOC_MAINSchema = (ES_DOC_MAINSchema) tES_DOC_MAINSET
                .get(1);

        //prepare data of lwfieldmap(table)
        //tTransferData.setNameAndValue("PrtNo","1234");// ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
        //tTransferData.setNameAndValue("InputDate","2005-10-12");//((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
        //tTransferData.setNameAndValue("ScanOperator","001");//
        //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
        //tTransferData.setNameAndValue("ManageCom", "86"); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());

        tTransferData.setNameAndValue("PrtNo", tES_DOC_MAINSchema.getDocCode()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getDocCode());
        tTransferData.setNameAndValue("InputDate", tES_DOC_MAINSchema
                .getMakeDate()); //((ES_DOC_MAINSchema)cInputData.get(0)).getMakeDate());
        tTransferData.setNameAndValue("ScanOperator", tES_DOC_MAINSchema
                .getScanOperator()); //
        //((ES_DOC_MAINSchema)cInputData.get(0)).getScanOperator());
        tTransferData.setNameAndValue("ManageCom", tES_DOC_MAINSchema
                .getManageCom()); // ((ES_DOC_MAINSchema)cInputData.get(0)).getManageCom());
        tTransferData.setNameAndValue("ULIFlag","1"); //
        tVData.add(tGlobalInput);
        tVData.add(tTransferData);
        try
        {
            ActivityOperator tActivityOpertor = new ActivityOperator();
            if (tActivityOpertor.CreateStartMission("0000000004", "0000002099",
                    tVData))
            {
                mResult = tActivityOpertor.getResult();
            }
            else
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "DocTBWorkFlowService";
                tError.functionName = "Save";
                tError.errorNo = "-99";
                tError.errorMessage = "扫描录入工作流节点生成失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DocTBWorkFlowService";
            tError.functionName = "Save";
            tError.errorNo = "-99";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //增加 DocCode 的锁定    2008-10-06
        MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        MMap mMap = lockTable(tES_DOC_MAINSchema.getDocCode(), tGlobalInput);
        if(mMap != null)
            map.add(mMap);
        else
            return false;

        // 修改调用该段程序后，会覆盖原 es_doc_main 表中，某些字段的数据库默认值。
        // gongqun2007-4-12
        //ES_DOC_MAINSchema aES_DOC_MAINSchema = tES_DOC_MAINSchema.getSchema();
        String archiveNo = createGrpArchiveNo(tES_DOC_MAINSchema.getManageCom());
        if (archiveNo == null)
        {
            return false;
        }
        tES_DOC_MAINSchema.setArchiveNo(archiveNo);
        //MMap map = (MMap) mResult.getObjectByObjectName("MMap", 0);
        //map.put(aES_DOC_MAINSchema, SysConst.UPDATE);

        System.out.println("\n\n\n\n设置归档号：" + tES_DOC_MAINSchema.getArchiveNo());
        System.out.println("----------------------WorkFlow End----------------------");
        
        return true;
    }

    /** gongqun2007-4-12
     * 生成归档号es_doc_main.ArchiveNo
     * @return String
     */
    public String createGrpArchiveNo(String tManageCom)
    {
        String tDate = PubFun.getCurrentDate();
        tDate = tDate.substring(0, 4) + tDate.substring(5, 7)
                + tDate.substring(8, 10);

        String comPart = "";
        int length = tManageCom.length();
        if (length == 2)
        {
            CError tError = new CError();
            tError.moduleName = "DocTBGrpWorkFlowService";
            tError.functionName = "createGrpArchiveNo";
            tError.errorMessage = "两位机构代码不能进行投保单扫描";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        else if (length == 4)
        {
            comPart = tManageCom.substring(2, 4) + "00";
        }
        else
        {
            comPart = tManageCom.substring(2, 6);
        }

        String tComtop = "T" + comPart + tDate;
        String tGrpArchiveNo = tComtop + PubFun1.CreateMaxNo(tComtop, 5);
        return tGrpArchiveNo;
    }
    
    /**
     * 锁定扫描上载
     * @param LockNoKey
     * @param tGlobalInput
     * @return
     */
    private MMap lockTable(String lockNoKey, GlobalInput globalInput)
    {
        MMap map = null;
        
        //扫描上载类型：“SI”
        String tLockNoType = "SI";
        
        //锁定有效时间
        String tAIS = "60";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", lockNoKey);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(globalInput);
        tVData.add(tTransferData);
        
        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        map = tLockTableActionBL.getSubmitMap(tVData, null);
        if(map == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        
        return map;
    }

    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public VData getResult()
    {
        return mResult;
    }

}
