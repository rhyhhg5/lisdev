/**
 * 
 */
package com.sinosoft.lis.easyscan;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.ES_DOC_PAGESDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.ZipAndUnzip;
import com.sinosoft.lis.schema.ES_DOC_PAGESSchema;
import com.sinosoft.lis.vschema.ES_DOC_PAGESSet;
import com.sinosoft.lis.vschema.LDSysVarSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author Administrator
 *
 */
public class EsDocPagesDownload
{
    public CErrors mErrors = new CErrors();
    private VData result = new VData();
    private GlobalInput globalInput = new GlobalInput();
    
    private String docID;
    private String docCode;
    private String realPath;

    public boolean submitData(VData inputData, String operate)
    {
        if (!operate.equals("DOWNLOAD")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        
        if (!getInputData(inputData)) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }
    
    private boolean getInputData(VData inputData)
    {
        globalInput = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
        TransferData transferData = (TransferData) inputData.getObjectByObjectName("TransferData", 0);
        if(transferData == null)
        {
            buildError("getInputData", "传入数据缺失！");
            return false;
        }
        docID = (String) transferData.getValueByName("DocID");
        docCode = (String) transferData.getValueByName("DocCode");
        realPath = (String) transferData.getValueByName("RealPath");
        
        return true;
    }
    
    private boolean checkData()
    {
        
        return true;
    }
    
    private boolean dealData()
    {
        List inputFileList = new ArrayList();
        List zipFileList = new ArrayList();
        String outputFileName = realPath + docCode + ".zip";

        //查询内部服务URL
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("InnerServiceURL");
        LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
        if (tLDSysVarSet.size() != 1)
        {
            buildError("getScanPic", "缺少内部服务URL的描述数据！");
            return false;
        }
        String innerServiceURL = tLDSysVarSet.get(1).getSysVarValue() + "/";
        
        ES_DOC_PAGESDB pagesDB = new ES_DOC_PAGESDB();
        pagesDB.setDocID(docID);
        ES_DOC_PAGESSet pagesSet = pagesDB.query();
        for(int i = 0; i < pagesSet.size(); i++)
        {
            ES_DOC_PAGESSchema pagesSchema = pagesSet.get(i + 1);
            String picPath = pagesSchema.getPicPath();
            String pageName = pagesSchema.getPageName() + ".tif";
            String fileZipPath = picPath.substring(picPath.indexOf("EasyScan") + 9);
            
            inputFileList.add(innerServiceURL + picPath + pageName);
            zipFileList.add(fileZipPath + pageName);
        }
        
        ZipAndUnzip zip = new ZipAndUnzip();
        zip.htmlZip(inputFileList, zipFileList, outputFileName);
        
        return true;
    }

    private void buildError(String functionName, String errorMessage) {
        CError cError = new CError();
        cError.moduleName = "EsDocPagesDownload";
        cError.functionName = functionName;
        cError.errorMessage = errorMessage;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub
        EsDocPagesDownload download = new EsDocPagesDownload();
        //download.docID = "256601";
        //download.docCode = "160000826531";
        download.docID = "256602";
        download.docCode = "160000826294";
        download.realPath = "E://project/ui/";
        download.dealData();
    }

}
