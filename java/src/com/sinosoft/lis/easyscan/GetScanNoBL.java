/**
 * <p>Title: Web业务系统</p>
 * <p>Description: EasyScan从中心请求获取扫描批次号</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author wellhi
 * @version 1.0
 */

package com.sinosoft.lis.easyscan;

import org.jdom.*;
import org.jdom.output.XMLOutputter;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class GetScanNoBL {
  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();
  private VData mInputData;
  private VData mResult = new VData();
  private static final String CON_XML_ROOT = "DATA";
  private static final String CON_XML_SCANNO = "ScanNo";
  private static final String CON_XML_RETURN = "RETURN";
//  private String mManageCom = "";
//  private String mOperator = "";
//  private String mOperate = "";
//  private String strScanNo = "";
//  private String strBussNo = "";
//  private String strBussType = "";
//  private String strSubType = "";
//  private String strManageCom = "";
//  private String strOperator = "";
//  private String strStartDate = "";
//  private String strEndDate = "";
  private String mManageCom;
  private String mOperator;
  private String mOperate;
  private String strScanNo;
  private String strBussNo;
  private String strBussType;
  private String strSubType;
  private String strStartDate;
  private String strEndDate;

  public GetScanNoBL() {
  }

  //入参处理
  private boolean getInputData() {
    GlobalInput nGlobalInput = (GlobalInput) mInputData.get(0);
    mOperator = nGlobalInput.Operator;
    mManageCom = nGlobalInput.ManageCom;
    mOperate = (String) mInputData.get(1);
    strScanNo = (String) mInputData.get(2);
    strBussNo = (String) mInputData.get(3);
    strBussType = (String) mInputData.get(4);
    strSubType = (String) mInputData.get(5);
    strStartDate = (String) mInputData.get(6);
    strEndDate = (String) mInputData.get(7);

    return true;
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    boolean tReturn = true;
    //首先将数据在本类中做一个备份
    mInputData = (VData) cInputData.clone();
    //进行业务处理
    if (!getInputData()) {
      return false;
    }
    if (!dealData()) {
      tReturn = false;
    }
    return tReturn;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData() {
    try {
      //获取新的批次号
      if (mOperate.equals("0")) {
        if (!getNewScanNo()) {
          return false;
        }
      }
      else {
        //查询历史批次号
        if (mOperate.equals("1")) {
          if (!getHisScanNo()) {
            return false;
          }
        }
        else {
          //一般不会出现
          return false;
        }
      }

      return true;
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  public VData getResult() {
    return mResult;
  }

  private boolean getNewScanNo() {
    System.out.println("GetScanNoBL: Start getting new ScanNo  ...");

    Element root = new Element(CON_XML_ROOT);
    Element scanNo = new Element(CON_XML_SCANNO);
    Element docRet = new Element(CON_XML_RETURN);
    String strNumber = "0";
    String strMessage = "";

    //获取最大批次号
    String strScanNo = getMaxNo("ScanNo");
    if (!strScanNo.equals("")) {
      scanNo.setText(strScanNo);
      root.addContent(scanNo);
    }
    else {
      System.out.println("EasyScan获取批次号出错。");
      strNumber = "-500";
      strMessage = "EasyScan获取批次号出错!";
    }

    //添加返回的错误
    //错误代码
    Element column = new Element("NUMBER");
    column = column.setText(strNumber);
    docRet.addContent(column);

    column = new Element("MESSAGE");
    column = column.setText(strMessage);
    docRet.addContent(column);
    root.addContent(docRet);
    //错误信息
    try {
      Document doc = new Document(root);
      XMLOutputter out = new XMLOutputter();
      //把doc转换为OutputStream
      String strXML = out.outputString(doc);
      System.out.println(strXML);
      mResult.add(strXML);
    }
    catch (Exception e) {
      System.out.println("EasyScan获取批次号出错。Exception:" + e.toString());
    }
    System.out.println("GetScanNoBL: End of getting new ScanNo.");
    return true;
  }

  private boolean getHisScanNo() {
    System.out.println("GetScanNoBL: Start getting history ScanNo  ...");
//    ES_DOC_MAINSchema nES_DOC_MAINSchema=new ES_DOC_MAINSchema();

    Element root = new Element(CON_XML_ROOT);
    Element docRet = new Element(CON_XML_RETURN);
    String strNumber = "0";
    String strMessage = "";

//    String strSQL =
//        "SELECT DISTINCT ScanNo FROM es_doc_main WHERE trim(ManageCom)='" +
//        mManageCom + "' and trim(ScanOperator)='" + mOperator + "' order by ScanNo DESC";
    StringBuffer bufSQL = new StringBuffer(
        "SELECT DISTINCT ScanNo FROM es_doc_main");
    bufSQL.append(" WHERE ManageCom like '");
    bufSQL.append(mManageCom);
    bufSQL.append("%");
    if (mOperator != null && !mOperator.equals("")) {
      bufSQL.append("' AND ScanOperator='");
      bufSQL.append(mOperator);
    }
    if (strScanNo != null && !strScanNo.equals("")) {
      bufSQL.append("' AND ScanNo='");
      bufSQL.append(strScanNo);
    }
    if (strBussNo != null && !strBussNo.equals("")) {
      bufSQL.append("' AND DocCode='");
      bufSQL.append(strBussNo);
    }
    if (strBussType != null && !strBussType.equals("")) {
      bufSQL.append("' AND BussType='");
      bufSQL.append(strBussType);
    }
    if (strSubType != null && !strSubType.equals("")) {
      bufSQL.append("' AND SubType='");
      bufSQL.append(strSubType);
    }
    if (strStartDate != null && !strStartDate.equals("")) {
      if (strEndDate != null && !strEndDate.equals("")) {
        bufSQL.append("' AND MakeDate BETWEEN'");
        bufSQL.append(strStartDate);
        bufSQL.append("' AND '");
        bufSQL.append(strEndDate);
      }
      else {
        bufSQL.append("' AND MakeDate='");
        bufSQL.append(strStartDate);
      }
    }
    else {
      if (strEndDate != null && !strEndDate.equals("")) {
        bufSQL.append("' AND MakeDate='");
        bufSQL.append(strEndDate);
      }
    }
    bufSQL.append("' ORDER BY ScanNo DESC");
    ExeSQL nExeSQL = new ExeSQL();
    SSRS nSSRS = nExeSQL.execSQL(bufSQL.toString());
    if (nSSRS.getMaxRow() > 0) {
      for (int i = 1; i <= nSSRS.getMaxRow(); i++) {
        Element scanNo = new Element(CON_XML_SCANNO);
        String strScanNo = nSSRS.GetText(i, 1);
        if (strScanNo != null && !strScanNo.equals("") &&
            !strScanNo.equals("null")) {
          scanNo.setText(strScanNo);
          root.addContent(scanNo);
        }
        else {
          System.out.println("EasyScan查询历史批次号出错。");
          strNumber = "-500";
          strMessage = "EasyScan查询历史批次号出错!";
          break;
        }
      }
    }
    else {
      strNumber = "-500";
//      strMessage = "没有找到管理机构 [" + mManageCom + "] 的操作员 [" + mOperator +
//          "] 相应的历史批次号，请申请新的批次号!";
      strMessage = "未找到符合条件的批次号，请更改查询条件再次进行查询.或者申请新的批次号。\n\n" +
          "您的查询条件是:\n"
          + "管理机构 [" + mManageCom + "]；\n操作员 [" + mOperator + "]；\n"
          + "扫描批次号 [" + strScanNo + "]；\n业务号码 [" + strBussNo + "]；\n"
          + "操作日期介于 [" + strStartDate + "] 和 [" + strEndDate + "]";
    }
    //添加返回的错误
    //错误代码
    Element column = new Element("NUMBER");
    column = column.setText(strNumber);
    docRet.addContent(column);
    //错误信息
    column = new Element("MESSAGE");
    column = column.setText(strMessage);
    docRet.addContent(column);
    root.addContent(docRet);

    try {
      Document doc = new Document(root);
      XMLOutputter out = new XMLOutputter();
      //把doc转换为OutputStream
      String strXML = out.outputString(doc);
      mResult.add(strXML);
    }
    catch (Exception e) {
      System.out.println("EasyScan查询历史批次号出错。Exception:" + e.toString());
    }
    System.out.println("GetScanNoBL: End of getting history ScanNo.");
    return true;
  }

  //生成流水号，包含错误处理
  private String getMaxNo(String cNoType) {
    String strNo = PubFun1.CreateMaxNo(cNoType, mManageCom);
    if (strNo.equals("") || strNo.equals("0")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "UploadPrepareBL";
      tError.functionName = "getNewScanNo";
      tError.errorNo = "-500";
      tError.errorMessage = "生成流水号失败!";
      this.mErrors.addOneError(tError);
      strNo = "";
    }
    return strNo;
  }

  public static void main(String[] args) {
    GetScanNoBL nGetScanNoBL = new GetScanNoBL();
    VData nVData = new VData();
    nGetScanNoBL.submitData(nVData, "0");
    nGetScanNoBL.submitData(nVData, "1");
  }

}
