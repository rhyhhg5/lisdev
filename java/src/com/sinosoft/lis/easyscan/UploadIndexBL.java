package com.sinosoft.lis.easyscan;

import utils.system;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: EasyScan上载图象索引处理</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author Liuqiang
 * changed by tuqiang
 * @version 1.0
 */
public class UploadIndexBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /**复制传入的参数，在本类中处理**/
    private VData mInputData;

    /**复制传入的参数，传递给后面处理**/
    private VData nInputData;

    /**需要返回的数据**/
    private VData mResult = new VData();

    /**需要数据库提交的数据**/
    private VData mResult1 = new VData();

    /**调用服务的类型**/
    private String m_strServiceType = "";

    /**数据提交**/
    MMap map = new MMap();

    /**扫描日志流水号**/
    private final static String CON_ES_LOG_ID = "ES_LogID";

    private GlobalInput tGI = new GlobalInput();

    String mRtnCode = "0";

    String mRtnDesc = "";

    public UploadIndexBL()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("UploadIndexBL-----begin");

        boolean breturn = true;
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        nInputData = (VData) cInputData.clone();
        mResult = mInputData;

        //处理原始数据//zxs
        if(!dealData(cOperate)){
        	return false;
        }

        CallService tCallService = new CallService();

        /**得到工作流数据**/

        /**判断是否需要质检，需要的话返回true，否则返回false begin**/
        CallQCService tCallQCService = new CallQCService();
        if (!tCallQCService.submitData(mInputData, cOperate))
        {
            System.out.println("UploadIndexBL 不走质检");
            if (!tCallService
                    .submitData(mInputData, cOperate, m_strServiceType))
            {
                mErrors.copyAllErrors(tCallService.mErrors);
                return false;
            }

            map.add((MMap) tCallService.getResult().getObjectByObjectName(
                    "MMap", 0));
        }
        else
        {
            System.out.println("UploadIndexBL 走质检");
            map.add((MMap) tCallQCService.getResult().getObjectByObjectName(
                    "MMap", 0));
        }
        /**判断是否需要质检，需要的话返回true，否则返回false end**/
        mResult1.add(map);
        //如果前面都执行成功后进行数据的提交
        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(mResult1, cOperate))
        {
            mErrors.copyAllErrors(pubsubmit.mErrors);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate)
    {
        ES_DOC_MAINSet tES_DOC_MAINSET = (ES_DOC_MAINSet) mInputData.get(0);
        ES_DOC_PAGESSet tES_DOC_PAGESSet = (ES_DOC_PAGESSet) mInputData.get(1);
        String[] strScanType = (String[]) mInputData.get(7);
        System.out.println(tES_DOC_MAINSET.size());

        if (tES_DOC_MAINSET.size() != 1)
        {
            CError tError = new CError();
            tError.moduleName = "CallService";
            tError.functionName = "save";
            tError.errorNo = "-1";
            tError.errorMessage = "传入数据出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        //对es_doc_main的一个数据转换过程
        ES_DOC_MAINSchema tES_DOC_MAINSchema = tES_DOC_MAINSET.get(1);
        //Edited by wellhi 2005.08.27
        //中心单证修改时借用了ManageCom保存IssueDocID，故要先去掉
        String strManageCom = tES_DOC_MAINSchema.getManageCom();
        ParameterDataConvert nConvert = new ParameterDataConvert();
        String strRManageCom = nConvert.getManageCom(strManageCom);
        String strIssueDocID = nConvert.getIssueDocID(strManageCom);
        tES_DOC_MAINSchema.setManageCom(strRManageCom);
        String strModifyOperator = tES_DOC_MAINSchema.getScanOperator();
        ES_DOC_PAGESDB oldES_DOC_PAGESDB = new ES_DOC_PAGESDB();
        ES_DOC_PAGESSet oldES_DOC_PAGESSet = new ES_DOC_PAGESSet();
        oldES_DOC_PAGESDB.setDocID(tES_DOC_MAINSchema.getDocID());
        oldES_DOC_PAGESSet = oldES_DOC_PAGESDB.query();
        
        //新单证上载
        if (tES_DOC_MAINSchema.getDocFlag().equals("0"))
        {
            //事前扫描
            m_strServiceType = "1";
        }
        else if (tES_DOC_MAINSchema.getDocFlag().equals("1"))
        {
            //Edited by wellhi 2005.08.27
            //单证修改
            m_strServiceType = "2";
        }
        else if (strScanType[0].equals("1"))
        {
            //Added by wellhi 2005.10.21
            //事后扫描
            m_strServiceType = "3";
        }
        //    tES_DOC_MAINSchema.setManageCom(strManageCom);
        if (tES_DOC_MAINSchema.getDocFlag().equals("0"))
        {
            tES_DOC_MAINSchema.setDocFlag("1");
            tES_DOC_MAINSchema.setMakeDate(PubFun.getCurrentDate());
            tES_DOC_MAINSchema.setModifyDate(PubFun.getCurrentDate());
            tES_DOC_MAINSchema.setMakeTime(PubFun.getCurrentTime());
            tES_DOC_MAINSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(tES_DOC_MAINSchema, "INSERT");
        }
        else
        {
            // 如修改，保留原投保书归档号。
            double tDocId = tES_DOC_MAINSchema.getDocID();

            if (tDocId > 0)
            {
                String tStrSql = "select ArchiveNo, State from es_doc_main "
                                 + " where DocId = " + tDocId;
                SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
                if(tSSRS != null && tSSRS.getMaxRow() > 0)
                {
                    tES_DOC_MAINSchema.setArchiveNo(tSSRS.GetText(1, 1));
                    tES_DOC_MAINSchema.setState(tSSRS.GetText(1, 2));
                }
            }
            // ---------------------------------------

            tES_DOC_MAINSchema.setModifyDate(PubFun.getCurrentDate());
            tES_DOC_MAINSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(tES_DOC_MAINSchema, "UPDATE");

            if (oldES_DOC_PAGESSet.size() < 1)
            {
                CError tError = new CError();
                tError.moduleName = "CallService";
                tError.functionName = "save";
                tError.errorNo = "-1";
                tError.errorMessage = "查询数据库出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            //校验是否有被删除的Page
            for (int j = 0; j < oldES_DOC_PAGESSet.size(); j++)
            {
                boolean blnDelete = true;
                for (int i = 0; i < tES_DOC_PAGESSet.size(); i++)
                {
                    if (oldES_DOC_PAGESSet.get(j + 1).getPageID() == tES_DOC_PAGESSet
                            .get(i + 1).getPageID())
                    {
                        blnDelete = false;
                        break;
                    }
                }
                if (blnDelete)
                {
                    map.put(oldES_DOC_PAGESSet.get(j + 1), "DELETE");
                    this.writeLog(oldES_DOC_PAGESSet.get(j + 1), "DELETE",
                            strModifyOperator);
                    
                }
            }
        }
        //对es_doc_pages的数据转换过程
        String tWBSQL="SELECT 1 FROM LLHOSPCASE WHERE CASENO='"+tES_DOC_MAINSchema.getDocCode()+"' with ur";
        String tWB = new ExeSQL().getOneValue(tWBSQL);
		String QYSQL = "SELECT 1 FROM ES_DOC_MAIN where doccode='"+tES_DOC_MAINSchema.getDocCode()+"' and subtype='TB28'and doccode like 'PD%'";
        String tQY = new ExeSQL().getOneValue(QYSQL);
        for (int i = 0; i < tES_DOC_PAGESSet.size(); i++)
        {
            //Edited by wellhi 2005.05.26
            ES_DOC_PAGESSchema nES_DOC_PAGESSchema = tES_DOC_PAGESSet
                    .get(i + 1);
            
            //Added by wellhi 2005.08.26
            //因为再UploadPrepareBL里把ServerBasePath添加到了PicPath所以必需再转换回来
            
            String strPicPath = nES_DOC_PAGESSchema.getPicPath();
            strPicPath = strPicPath.substring(strPicPath.indexOf("||") + 2);
            StrTool nStrTool = new StrTool();
            nES_DOC_PAGESSchema.setPicPath(StrTool.replace(strPicPath, "||",
                    ""));
            String PageName = nES_DOC_PAGESSchema.getPageName().trim();
            double PageId = nES_DOC_PAGESSchema.getPageID();
            System.out.println("单证的名字："+PageName);
            System.out.println("Pageid:"+nES_DOC_PAGESSchema.getPageID());

			if((tES_DOC_MAINSchema.getBussType().equals("TB") && nES_DOC_PAGESSchema.getPageSuffix().equals(".gif"))||(tES_DOC_MAINSchema.getBussType().equals("TB")&&nES_DOC_PAGESSchema.getPageSuffix().equals(".tif"))||!tES_DOC_MAINSchema.getBussType().equals("TB")){ //zxs

            if (nES_DOC_PAGESSchema.getPageFlag().equals("0"))
            {
                nES_DOC_PAGESSchema.setPageFlag("1");
                nES_DOC_PAGESSchema.setMakeDate(PubFun.getCurrentDate());
                nES_DOC_PAGESSchema.setMakeTime(PubFun.getCurrentTime());
                nES_DOC_PAGESSchema.setModifyDate(PubFun.getCurrentDate());
                nES_DOC_PAGESSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(nES_DOC_PAGESSchema, "INSERT");
                this.writeLog(nES_DOC_PAGESSchema, "INSERT", strModifyOperator);
            }
            else
            {
                //对外接口的路径需要还原回来
                if("1".equals(tWB)||"1".equals(tQY)){
                	System.out.println("不为空的");
                	System.out.println("oldES_DOC_PAGESSet:"+oldES_DOC_PAGESSet.size());
                	for(int k = 0; k < oldES_DOC_PAGESSet.size(); k++){
                		ES_DOC_PAGESSchema oldES_DOC_PAGESSchema = oldES_DOC_PAGESSet.get(k+1);
                		double oldPageID = oldES_DOC_PAGESSchema.getPageID();
                		System.out.println("oldPageID:"+oldPageID);
                		System.out.println("oldname:"+oldES_DOC_PAGESSchema.getPageName());
                		System.out.println("原路径为"+oldES_DOC_PAGESSchema.getPicPath());
                		if(PageId == oldPageID){
                			System.out.println("变更路径为"+oldES_DOC_PAGESSchema.getPicPath()); //原路径
                			nES_DOC_PAGESSchema.setPicPath(oldES_DOC_PAGESSchema.getPicPath());
                		}
                	}
                }
                nES_DOC_PAGESSchema.setPageFlag("1");
                nES_DOC_PAGESSchema.setModifyDate(PubFun.getCurrentDate());
                nES_DOC_PAGESSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(nES_DOC_PAGESSchema, "UPDATE");
                this.writeLog(nES_DOC_PAGESSchema, "UPDATE", strModifyOperator);
            }
			}else{
				 // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "UploadPrepareBL";
	            tError.functionName = "getReturnData";
	            tError.errorNo = "-90";
	            tError.errorMessage = "上传的图片不符合相关格式，正确的图片格式为GIF或TIF!";
	            this.mErrors.addOneError(tError);
	            return false;
			}
        }
        //    tES_DOC_MAINSchema.setManageCom(strManageCom);
        mInputData.setElementAt(strIssueDocID, 6);
        return true;
    }

    private boolean writeLog(ES_DOC_PAGESSchema eES_DOC_PAGESSchema,
            String strModifyDesc, String strModifyOperator)
    {
        ES_DOC_RELATIONDB nES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
        nES_DOC_RELATIONDB.setDocID(eES_DOC_PAGESSchema.getDocID());
        //可能会建立多个关联
        ES_DOC_RELATIONSet nES_DOC_RELATIONSet = nES_DOC_RELATIONDB.query();
        int intCount = nES_DOC_RELATIONSet.size();
        if (intCount > 0)
        {
            for (int i = 0; i < intCount; i++)
            {
                ES_DOC_RELATIONSchema nES_DOC_RELATIONSchema = nES_DOC_RELATIONSet
                        .get(i + 1);
                //填写日志信息
                Es_Doc_LogSchema nEs_Doc_LogSchema = new Es_Doc_LogSchema();
                nEs_Doc_LogSchema.setLogID(getMaxNo(CON_ES_LOG_ID));
                nEs_Doc_LogSchema.setBussType(nES_DOC_RELATIONSchema
                        .getBussType());
                nEs_Doc_LogSchema.setSubType(nES_DOC_RELATIONSchema
                        .getSubType());
                nEs_Doc_LogSchema.setBussNo(nES_DOC_RELATIONSchema.getBussNo());
                nEs_Doc_LogSchema.setBussNoType(nES_DOC_RELATIONSchema
                        .getBussNoType());
                nEs_Doc_LogSchema.setPageID(eES_DOC_PAGESSchema.getPageID());
                nEs_Doc_LogSchema.setDocID(eES_DOC_PAGESSchema.getDocID());
                nEs_Doc_LogSchema.setManageCom(eES_DOC_PAGESSchema
                        .getManageCom());
                nEs_Doc_LogSchema.setScanOperator(eES_DOC_PAGESSchema
                        .getOperator());
                nEs_Doc_LogSchema
                        .setPageCode(eES_DOC_PAGESSchema.getPageCode());
                nEs_Doc_LogSchema
                        .setHostName(eES_DOC_PAGESSchema.getHostName());
                nEs_Doc_LogSchema
                        .setPageName(eES_DOC_PAGESSchema.getPageName());
                nEs_Doc_LogSchema.setPageSuffix(eES_DOC_PAGESSchema
                        .getPageSuffix());
                nEs_Doc_LogSchema.setPicPathFTP(eES_DOC_PAGESSchema
                        .getPicPathFTP());
                nEs_Doc_LogSchema.setPicPath(eES_DOC_PAGESSchema.getPicPath());
                nEs_Doc_LogSchema.setScanNo(eES_DOC_PAGESSchema.getScanNo());
                nEs_Doc_LogSchema
                        .setMakeDate(eES_DOC_PAGESSchema.getMakeDate());
                nEs_Doc_LogSchema
                        .setMakeTime(eES_DOC_PAGESSchema.getMakeTime());
                nEs_Doc_LogSchema.setModifyDate(eES_DOC_PAGESSchema
                        .getModifyDate());
                nEs_Doc_LogSchema.setModifyTime(eES_DOC_PAGESSchema
                        .getModifyTime());
                nEs_Doc_LogSchema.setModifyDesc(strModifyDesc);
                nEs_Doc_LogSchema.setModifyOperator(strModifyOperator);
                map.put(nEs_Doc_LogSchema, "INSERT");
            }
        }
        return true;
    }

    //生成流水号，包含错误处理
    private String getMaxNo(String cNoType)
    {
        String strNo = PubFun1.CreateMaxNo(cNoType, 1);

        if (strNo.equals("") || strNo.equals("0"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UploadPrepareBL";
            tError.functionName = "getReturnData";
            tError.errorNo = "-90";
            tError.errorMessage = "生成流水号失败!";
            this.mErrors.addOneError(tError);
            strNo = "";
        }

        return strNo;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        return true;
    }

    public static void main(String[] args)
    {

        VData tVData = new VData();
        ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
        tES_DOC_MAINSchema.setDocID("1");
        tES_DOC_MAINSchema.setDocFlag("1");
        ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
        tES_DOC_MAINSet.add(tES_DOC_MAINSchema);
        tVData.add(tES_DOC_MAINSet);
        CallService tCallService1 = new CallService();
        tCallService1.submitData(tVData, "", "1");

        PubSubmit pubsubmit = new PubSubmit();
        if (!pubsubmit.submitData(tCallService1.getResult(), ""))
        {
            CError tError = new CError();
            tError.moduleName = "UploadIndexBL";
            tError.functionName = "save";
            tError.errorNo = "-1";
            tError.errorMessage = "处理出错";
        }
    }
}
