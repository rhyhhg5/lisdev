package com.sinosoft.lis.easyscan;

import java.io.*;
import java.util.zip.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class OmnipotenceProposalDownloadBL {

    static final int BUFFER = 2048;
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private String zippath;
    private String mContNo;
    private String cDate = "";
    private TransferData mTransferData;
    private String mRealPath;
    private int EntryCount=0;

    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        System.out.println("now in OmnipotenceProposalDownloadBL submit");
        if (this.getInputData(mInputData) == false) {
            return false;
        }
        System.out.println("---getInputData end---");

        if (this.dealData(mInputData) == false) {
            return false;
        } else {
            return true;
        }

    }

    private boolean getInputData(VData cInputData) {

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null) {
            buildError("getInputData",
                       "改造保单下载,生成文件路径不应是静态描述路径<br>author:Ｙａｎｇｍｉｎｇ "
                       + "<br>出现此问题让dongjb修改！1");
            return false;
        }
        mRealPath = (String) mTransferData.getValueByName("OutXmlPath");
        mContNo = (String) mTransferData.getValueByName("ContNo");
        if (StrTool.cTrim(this.mRealPath).equals("")) {
            buildError("getInputData",
                       "改造保单下载,生成文件路径不应是静态描述路径<br>author:Ｙａｎｇｍｉｎｇ "
                       + "<br>出现此问题让dongjb修改！2");
            return false;
        }
            return true;

    }

    private boolean dealData(VData cInputData) {
        cDate = PubFun.getCurrentDate();
        //生成放zip文件的文件名***************************************************
        zippath = mRealPath +mContNo+
                  ".zip";
        System.out.println("生成的zip包是:" + zippath);
        //下面定义一个数组,看该数组的大小*******************************************

         LCContDB mLCContDB = new LCContDB();
         mLCContDB.setContNo(mContNo);
         if(!mLCContDB.getInfo()){
             return false;
         }
         String tPrtNo=mLCContDB.getPrtNo();
         ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
         ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
         tES_DOC_MAINDB.setDocCode(tPrtNo);
         tES_DOC_MAINDB.setSubType("TB04");
         tES_DOC_MAINSet = tES_DOC_MAINDB.query();
         System.out.println(tES_DOC_MAINSet.size());
         ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
         ES_DOC_PAGESSet tES_DOC_PAGESSet = new ES_DOC_PAGESSet();
         if (tES_DOC_MAINSet.size() > 0) {

             tES_DOC_PAGESDB.setDocID(tES_DOC_MAINSet.get(1).getDocID());
             tES_DOC_PAGESSet = tES_DOC_PAGESDB.query();
         }
         if(tES_DOC_PAGESSet.size()>0){
             EntryCount=tES_DOC_PAGESSet.size();
         }

    //下面开始向数组中放入需要input的文件的url地址*******************************
       System.out.println(EntryCount);
        String[] InputEntry = new String[EntryCount];
        String[] OutputEntry = new String[EntryCount];
        //生成放zip文件的文件名***************************************************
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("UIRoot");
        LDSysVarSet tLDSysVarSet = tLDSysVarDB.query();
        String PrePicURL = tLDSysVarSet.get(1).getSysVarValue();
            for (int j = 0; j <EntryCount; j++) {
                InputEntry[j] = PrePicURL +
                             tES_DOC_PAGESSet.get(j+1).getPicPath() +
                             tES_DOC_PAGESSet.get(j+1).getPageName() +
                             ".tif";
                System.out.println("序号为: " + j+1 + "的图片地址为" +
                                   InputEntry[j]);
                OutputEntry[j] = tES_DOC_PAGESSet.get(j+1).getPageName() + ".tif";

            }

        if (this.CreatZipFile(InputEntry, OutputEntry, zippath) == false) {
            return false;
        } else {

            return true;
        }
    }

    public boolean CreatZipFile(String[] tInputEntry, String[] tOutputEntry,
                                String tzippath) {
        System.out.println("===============开始创建压缩文件===============");
        System.out.println("tInputEntry.length is" + tInputEntry.length);
        System.out.println("tOutputEntry.length is" + tOutputEntry.length);
        System.out.println("tzippath is " + tzippath);
        if (tInputEntry.length != tOutputEntry.length) {
            return false;
        } else {
            try {
                System.out.println("开始将压缩文件放入压缩包");
                BufferedInputStream origin = null;
                System.out.println("tzippath is " + tzippath);
                FileOutputStream f = new FileOutputStream(tzippath);
                ZipOutputStream out = new ZipOutputStream(new
                        BufferedOutputStream(
                                f));
                byte data[] = new byte[BUFFER];
                for (int i = 0; i < tInputEntry.length; i++) {
                    FileInputStream fi = new FileInputStream(tInputEntry[i]);
                    origin = new BufferedInputStream(fi, BUFFER);
                    ZipEntry entry = new ZipEntry(tOutputEntry[i]);
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0,
                                                BUFFER)) != -1) {
                        out.write(data, 0, count);
                    }
                    origin.close();
                }
                out.flush();
                out.close();
            } catch (Exception ex) {
                mErrors.addOneError("找不到符合格式的扫描件");
                ex.printStackTrace();
                return false;
            }
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OmnipotenceProposalDownloadBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
}
