package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.util.HashMap;
import com.sinosoft.lis.bq.CommonBL;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 负责接收页面传入的数据，并传递给ScanListPrintBL.java进行清单数据的生成
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ScanListPrintBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null; //操作员信息
    private String mSql = null;  //页面传入的查询扫描件清单的sql语句
    private TextTag mTextTag = new TextTag();  //存储打印要素值
    private ListTable mListTable = new ListTable();  //存储清单列表数据
    private XmlExport mXmlExport = new XmlExport();  //生成的打印清单数据

    public ScanListPrintBL()
    {
    }

    /**
     * 外部操作的提交方法，调用dealData()进行业务逻辑处理，
     * 并得到处理后的XmlExport对象，返回页面
     * @param cInputData VData：对象，需要：
     * a)	TransferData对象：存储页面查询清单的Sql语句。
     * b)	GlobalInput对象：操作员信息。
     * @param operate String：操作方式，此可为“”
     * @returnXmlExport：成功待打印的清单数据，否则null
     */
    public XmlExport getXmlExport (VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return mXmlExport;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 进行业务逻辑处理，查询扫描件表Es_Doc_Relation、Es_Doc_Pages生成扫描件清单数据，
     * 详见（详见数据流图/扫描清单查询）
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        HashMap sumBuss = new HashMap();  //受理件数，同一受理号为一份受理件

        SSRS tSSRS = new ExeSQL().execSQL(mSql);
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到需要的清单数据");
            return false;
        }

        mXmlExport.createDocument("PrtScanList.vts", "printer");
        mListTable.setName("SCAN");
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[tSSRS.getMaxCol()];
            //info[0] = "" + i;
            for(int position = 1; position <= tSSRS.getMaxCol(); position++)
            {
                info[position - 1] = StrTool.cTrim(tSSRS.GetText(i, position));
            }

            sumBuss.put(tSSRS.GetText(i, 4), "1");
            mListTable.add(info);
        }
        mXmlExport.addListTable(mListTable, new String[tSSRS.getMaxRow()]);


        mTextTag.add("SumPages", tSSRS.getMaxRow());
        mTextTag.add("SumBuss", sumBuss.size());
        mTextTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        mXmlExport.addTextTag(mTextTag);

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tTransferData = (TransferData) data
                                     .getObjectByObjectName("TransferData", 0);
        if(tTransferData == null || mGI == null || mGI.Operator == null)
        {
            CError tError = new CError();
            tError.moduleName = "ScanListPrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的数据不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        this.mSql = (String) tTransferData.getValueByName("sql");
        if(mSql == null)
        {
            CError tError = new CError();
            tError.moduleName = "ScanListPrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入清单查询语句sql";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        String sql = "select a.DocID, a.PageCode, (select Name from LDCom where ComCode = a.ManageCom),    b.BussNo, a.MakeDate, a.MakeTime,    (select UserName from LDUser where UserCode = a.Operator),    (select Name from LDCom x, LDUser y    where x.ComCode = y.ComCode and y.UserCode=a.Operator) from ES_Doc_Pages a, ES_Doc_Relation b where a.docID = b.docID    and a.ManageCom like '86%'  and b.BussType='BQ' order by a.docID, a.pageCode";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("sql", sql);

        VData data = new VData();
        data.add(gi);
        data.add(tTransferData);

        ScanListPrintBL bl = new ScanListPrintBL();
        if(bl.getXmlExport(data, "") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
