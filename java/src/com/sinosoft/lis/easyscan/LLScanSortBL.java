package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LLScanSortBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LLScanSortSchema mLLScanSort = new LLScanSortSchema();
    private LLScanSortSet mLLScanSortSet = new LLScanSortSet();

    public static void main(String[] args) {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLScanSortBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LLScanSortBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        else {
            System.out.println("Start LLScanSortBL Submit...");
            LLScanSortBLS tLLScanSortBLS = new LLScanSortBLS();
            tLLScanSortBLS.submitData(mInputData, cOperate);
            System.out.println("End LLScanSortBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLLScanSortBLS.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLLScanSortBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "EsPicModifyInputBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT")) {
            if (mLLScanSortSet.size() > 0) {
                for (int i = 1; i <= mLLScanSortSet.size(); i++) {
                    mLLScanSortSet.get(i).setOperator(this.mGlobalInput.
                            Operator);
                    mLLScanSortSet.get(i).setManageCom(this.mGlobalInput.
                            ManageCom);
                    mLLScanSortSet.get(i).setModifyDate(currentDate);
                    mLLScanSortSet.get(i).setModifyTime(currentTime);
                }
            } else {
                tReturn = false;
            }

//                    String tBussnotype =this.mEs_IssueDocSchema.getBussNoType().trim();
//                    String tCaseNo =this.mEs_IssueDocSchema.getBussNo().trim();
//                    String tSubtype =this.mEs_IssueDocSchema.getSubType().trim();
//                    String tCount;
//                    String tCount1;
//                    int i;
//
//
//                    ES_DOC_RELATIONDB tES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
//
//                    String sql = "select * from ES_DOC_RELATION where"
//                                              + " Bussnotype ='" +tBussnotype + "'"
//                                              + " and Bussno = '" + tCaseNo + "'"
//                                              + " and Subtype = '" + tSubtype + "'";
//                    System.out.println("Start query LLCase:" + sql);
//                    ES_DOC_RELATIONSet tLLCaseSet = (ES_DOC_RELATIONSet) tES_DOC_RELATIONDB.executeQuery(sql);
//
//                    //
//
//                    String tSQL = "select MAX(to_number(IssueDocID)) from Es_IssueDoc where BussNo is not null";
//                    ExeSQL tExeSQL = new ExeSQL();
//                    tCount1 = tExeSQL.getOneValue(tSQL);
//
//                    //
//
//
//                if (tCount1 != null && !tCount1.equals(""))
//                    {
//
//                      Integer tInteger = new Integer(tCount1);
//                      i = tInteger.intValue();
//                      i = i + 1;
//                    }
//
//                    else
//                    {       i = 1 ;
//                    }
//                if (tLLCaseSet == null || tLLCaseSet.size() == 0)
//                {
//                        // @@错误处理
//                        CError tError = new CError();
//                        tError.moduleName = "EsPicModifyInputBL";
//                        tError.functionName = "dealData";
//                        tError.errorMessage = "此单证号不存在!";
//                        this.mErrors.addOneError(tError);
//                        tReturn = false;
//                    }
//                 else
//                 {
//                     this.mEs_IssueDocSchema.setIssueDocID(String.valueOf(i));
//                     this.mEs_IssueDocSchema.setMakeDate(currentDate);
//                     this.mEs_IssueDocSchema.setMakeTime(currentTime);
//                     this.mEs_IssueDocSchema.setModifyDate(currentDate);
//                     this.mEs_IssueDocSchema.setModifyTime(currentTime);
//                     this.mEs_IssueDocSchema.setPromptOperator(this.mGlobalInput.Operator);
//                 }

        } else {
            tReturn = false;
        }
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLLScanSortSet.set((LLScanSortSet) cInputData.
                                getObjectByObjectName("LLScanSortSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLLScanSortSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLScanSortBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
