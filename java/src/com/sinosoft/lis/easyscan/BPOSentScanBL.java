package com.sinosoft.lis.easyscan;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.ftp.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BPOSentScanBL
{

    private static final int BUFFER = 2048;

    private static final int DEAL_COUNT = 10;

    public CErrors mErrors = new CErrors();

    private GlobalInput mG = new GlobalInput();

    private String mUIRoot = null; //应用ui目录部署的绝对路径

    private int mBPOScanCount = 10; //一个批次包含的扫描件份数

    private HashSet mBatchDealtScanCountSet = null; //当前批次的扫描页数

    private int mBatchDealtPageCount = 0; //当前批次的扫描页数

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    private String mBPOBatchNo = null;

    private String mSubType = null;

    private HashMap mBPOBatchNoMap = new HashMap(); //存储外包批次，一次运行可能会生成多个批次

    private BPOServerInfoSchema tBPOServerInfoSchema = new BPOServerInfoSchema();

    private XMLDatasets mXMLDatasets = null;

    private Reflections ref = new Reflections();

    private MMap map = new MMap();

    private VData mResult = new VData();

    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();
        if (!getSubmitMap(cInputData))
        {
            return false;
        }

        return true;
    }

    public boolean getSubmitMap(VData cInputData)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        TransferData tf = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tf == null)
        {
            mErrors.addOneError("请传入需要发送的扫描件类型");
            return false;
        }

        if (tf != null)
        {
            mSubType = (String) tf.getValueByName("SubType");
            if (mSubType == null)
            {
                mErrors.addOneError("请指定需要发送的扫描件类型");
                return false;
            }
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        //查询需发送外包的扫描件信息
        SSRS tSSRS2 = getPagesInfo();
        if (tSSRS2 == null)
        {
            return false;
        }
        

        //为mUIRoot赋值
        if (!setmUIRoot())
        {
            return false;
        }

        //得到外包信息，先按一个外包商开发
//        BPOServerInfoSet set = new BPOServerInfoDB().query();
//        tBPOServerInfoSchema = set.get(1);
          BPOServerInfoDB tBPOServerInfoDB=new BPOServerInfoDB();
          

        //为每个扫描页生成xml和状态信息
        String zipfilepath = null;
        FileOutputStream f = null;
        ZipOutputStream out = null;
        XMLDataset tXMLDataset = null;
        XMLDataList tXmlDataList = null;

        mBPOScanCount = getmBPOScanCount();
        //扫描保单个数
        HashSet ab = new HashSet();
        for(int k=1;k<=tSSRS2.getMaxRow(); k++){
        	ab.add(tSSRS2.GetText(k, 1));
        }
        int length = ab.size();
        double tLen=length;
        //总批次
        //int piNum=(length+mBPOScanCount-1)/mBPOScanCount;
        int piNum=(int)Math.ceil(tLen/mBPOScanCount);
        //第几个批次
        int CountNum=1;
        //得到发送的全部机构
        LDCodeDB tLDCode=new LDCodeDB();
        LDCodeSet tLDCodeSet=tLDCode.executeQuery("select * from ldcode where codetype='bjwbbl' and othersign='1' ");
        //得到总份额
        int tFenE=0;
        for(int k=1;k<=tLDCodeSet.size();k++){
        	tFenE+=Integer.parseInt(tLDCodeSet.get(k).getCodeAlias());
        }  
        
        String tBPOId;
    
        
        for (int i = 1; i <= tSSRS2.getMaxRow(); i++)
        {
        	int num=0; 
        	for(int j=1;j<=tLDCodeSet.size();j++){
        		num+=Integer.parseInt(tLDCodeSet.get(j).getCodeAlias());
        		if( CountNum<=Math.floor(piNum*num/tFenE)){
        			tBPOId=tLDCodeSet.get(j).getCode();
        			tBPOServerInfoDB.setBPOID(tBPOId);
        			tBPOServerInfoSchema=tBPOServerInfoDB.query().get(1);
        			break;
        		}else{
        			tBPOId=tLDCodeSet.get(j).getCode();
        			tBPOServerInfoDB.setBPOID(tBPOId);
        			tBPOServerInfoSchema=tBPOServerInfoDB.query().get(1);
        		}
        		
        		
        	}
        	

            //创建xml及压缩包初始信息
            if (mBatchDealtPageCount == 0)
            {
                mBatchDealtScanCountSet = new HashSet();

                createNewXMLDatasets();
                tXMLDataset = mXMLDatasets.createDataset();

                tXmlDataList = new XMLDataList();
                addListHead(tXmlDataList); //生成扫描页信息标题

                mBPOBatchNo = gettBPOBatchNo(tBPOServerInfoSchema.getBPOID());
                zipfilepath = mUIRoot
                        + tBPOServerInfoSchema.getBackupBasePath()
                        + mBPOBatchNo + ".zip";
                System.out.println("压缩路径名：" + zipfilepath);

                try
                {
                    f = new FileOutputStream(zipfilepath);
                }
                catch (FileNotFoundException ex)
                {
                    ex.printStackTrace();
                    mErrors.addOneError("无法创建zip文件");
                    System.out.println(mErrors.getLastError());
                    return false;
                }
                out = new ZipOutputStream(new BufferedOutputStream(f));
            }

            String tZipDocFileName = tSSRS2.GetText(i, 15); //文件相对路径 + 文件名
            String tDocFileName = mUIRoot + tZipDocFileName; //文件绝对路径 + 文件名
            System.out.println("将图片放入压缩包:" + tDocFileName);
            int flag = compressIntoZip(zipfilepath, out, tZipDocFileName,
                    tDocFileName);
            if (flag == -1)
            {
                return false;
            }
            else if (flag == 0)
            {
                continue;
            }

            mBatchDealtPageCount++;

            //如果一个保单的影印件都不存在（或没有指定格式的影印件），则不进行处理
            //生成状态信息
            if (!mBatchDealtScanCountSet.contains(tSSRS2.GetText(i, 1)))
            {
                if (!createState(tSSRS2.getRowData(i)))
                {
                    continue;
                }
            }
            mBatchDealtScanCountSet.add(tSSRS2.GetText(i, 1));

            //生成扫描页xml信息
            createXMLListRow(tXmlDataList, tSSRS2.getRowData(i));

            //若已处理完毕，或已到达一个批次处理扫描件数上限，则发送zip包
            if (i == tSSRS2.getMaxRow()
                    || mBatchDealtScanCountSet.size() == mBPOScanCount
                    && !tSSRS2.GetText(i, 1).equals(tSSRS2.GetText(i + 1, 1)))
            {
            	CountNum+=1;
            	if (!sendOneZip(tSSRS2, zipfilepath, out, tXMLDataset,
                        tXmlDataList, mBatchDealtScanCountSet.size()))
                {
                    return false;
                }
                mBatchDealtPageCount = 0;
                mBatchDealtScanCountSet = new HashSet();
            }

        }

        return true;
    }

    /**
     * 发送zip到前置机
     * @param tSSRS2 SSRS
     * @param zipfilepath String
     * @param out ZipOutputStream
     * @param tXMLDataset XMLDataset
     * @param tXmlDataList XMLDataList
     * @param tDealtCount int
     * @return boolean
     */
    private boolean sendOneZip(SSRS tSSRS2, String zipfilepath,
            ZipOutputStream out, XMLDataset tXMLDataset,
            XMLDataList tXmlDataList, int tDealtCount)
    {
        //若处理的扫描件达到每个批次上限或是最后一个扫描件，则本批次处理结束
        cerateXLMBody(tXMLDataset, tDealtCount);
        tXMLDataset.addDataObject(tXmlDataList);

        //生成XML到指定目录下
        String strFileName = mUIRoot + tBPOServerInfoSchema.getBackupBasePath()
                + mBPOBatchNo + ".xml";
        mXMLDatasets.output(strFileName);
        try
        {
            int index = strFileName.lastIndexOf(File.separator);
            if (index < 0)
            {
                index = strFileName.lastIndexOf("/");
            }

            byte data[] = new byte[BUFFER];
            FileInputStream fi = new FileInputStream(strFileName);
            BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);
            ZipEntry mentry = new ZipEntry(strFileName.substring(index + 1));
            out.putNextEntry(mentry);
            int count;
            while ((count = origin.read(data, 0, BUFFER)) != -1)
            {
                out.write(data, 0, count);
            }
            origin.close();
            out.flush();
            out.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            this.mErrors.addOneError("找不到待压缩文件压缩路径" + strFileName);
            System.out.println("找不到待压缩文件压缩路径" + strFileName);
            deleteFile(zipfilepath);
            deleteFile(strFileName);
            return false;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            this.mErrors.addOneError("无法生成xml文件");
            System.out.println("无法生成xml文件");
            deleteFile(zipfilepath);
            deleteFile(strFileName);
            return false;
        }

        //生成批次信息
        if (!createBPOBatchInfo(tBPOServerInfoSchema))
        {
            return false;
        }

        if (!sendZip(zipfilepath))
        {
            deleteFile(strFileName);
            return false;
        }

        //存储任务信息
        if (!submit())
        {
            return false;
        }

        return true;
    }

    /**
     * 添加到压缩文件
     * @param zipfilepath String
     * @param out ZipOutputStream
     * @param tZipDocFileName String：zip文件名
     * @param tDocFileName String：待压缩的文件路径名
     * @return int：1：成功、0：失败，但不中断整个程序、-1：失败，中断程序
     */
    private int compressIntoZip(String zipfilepath, ZipOutputStream out,
            String tZipDocFileName, String tDocFileName)
    {
        try
        {
            FileInputStream fi = new FileInputStream(tDocFileName);
            BufferedInputStream origin = new BufferedInputStream(fi, BUFFER);

            ZipEntry entry = new ZipEntry(tZipDocFileName);
            out.putNextEntry(entry);

            int count;
            byte data[] = new byte[BUFFER];
            while ((count = origin.read(data, 0, BUFFER)) != -1)
            {
                out.write(data, 0, count);
            }
            origin.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
            this.mErrors.addOneError("找不到待压缩文件压缩路径" + tDocFileName);
            System.out.println("找不到待压缩文件压缩路径" + tDocFileName);

            //某些页找不到可不用单独处理
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "无法生成压缩包";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            deleteFile(zipfilepath);
            return -1;
        }

        return 1;
    }

    /**
     * createNewXMLDatasets
     */
    private void createNewXMLDatasets()
    {
        //生成xml文件头
        mXMLDatasets = new XMLDatasets();
        mXMLDatasets.setEnCodeType("GBK");
        mXMLDatasets.createDocument();

    }

    /**
     * addListHead
     * 为XMLDataList添加标题
     * @param tXmlDataList XMLDataList
     */
    private void addListHead(XMLDataList tXmlDataList)
    {
        //添加Head单元内的信息
        tXmlDataList.setDataObjectID("Pages");
        tXmlDataList.addColHead("PrtNo");
        tXmlDataList.addColHead("BussType");
        tXmlDataList.addColHead("SubType");
        tXmlDataList.addColHead("PageID");
        tXmlDataList.addColHead("DocID");
        tXmlDataList.addColHead("PageCode");
        tXmlDataList.addColHead("RelativePath");
        tXmlDataList.addColHead("PicPathPtp");
        tXmlDataList.addColHead("PageName");
        tXmlDataList.addColHead("PageSuffix");
        tXmlDataList.addColHead("ManageCom");
        tXmlDataList.addColHead("Operator");
        tXmlDataList.addColHead("MakeDate");
        tXmlDataList.addColHead("MakeTime");
        tXmlDataList.buildColHead();
    }

    /**
     * getmBPOScanCount
     * 得到一个批次的扫描件份数
     * @return int
     */
    private int getmBPOScanCount()
    {
        String sql = "select Code from LDCode "
                + "where CodeType = 'BPOScanCount' ";
        String tBPOSanCount = new ExeSQL().getOneValue(sql);
        if (tBPOSanCount == null || tBPOSanCount.equals(""))
        {
            return DEAL_COUNT;
        }

        return Integer.parseInt(tBPOSanCount);
    }

    /**
     * 得到批次号
     * @param cBPOID String：外包商ID
     * @return String：yyyymmdd_hhmmss_序号
     */
    public String gettBPOBatchNo(String cBPOID)
    {
        String mDateTime = mCurDate.split("-")[0] + mCurDate.split("-")[1]
                + mCurDate.split("-")[2] + mCurTime.split(":")[0]
                + mCurTime.split(":")[1] + mCurTime.split(":")[2];

        // 修改批次命名。
        // mBPOBatchNo = "ScanListSendOut" + mDateTime
        mBPOBatchNo = mSubType + "ScanList" + mDateTime
        // ------------------------

                + (mBPOBatchNoMap.size() + 1);
        mBPOBatchNoMap.put(mBPOBatchNo, cBPOID);

        return mBPOBatchNo;
    }

    /**
     * 为mUIRoot赋值
     * @return boolean
     */
    private boolean setmUIRoot()
    {
        //得到应用根目录绝对路径
        String stSql = "select sysvarvalue from ldsysvar where sysvar='UIRoot'";
        mUIRoot = new ExeSQL().getOneValue(stSql);
        if (mUIRoot == null || mUIRoot.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "setmUIRoot";
            tError.errorMessage = "没有查到应用目录，请配置LDSysVar的UIRoot变量";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 删除生成的ZIP文件和XML文件
     * @param cFilePath String：完整文件名
     */
    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "deleteFile";
            tError.errorMessage = "没有找到需要删除的文件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        f.delete();

        return true;
    }

    /**
     * 得到保单信息，
     * @return SSRS
     */
    private SSRS getPagesInfo()
    {
        //按页查询，此处Order by可保证每一个单证的扫描页都是挨着的，生成xml节点也是挨着的，
        //并且拆分成多个xml时也能保证一个扫描件的扫描页只在一个xml中，
        StringBuffer tSBql = new StringBuffer();
        tSBql
                .append("select a.DocCode, a.BussType, a.SubType, b.PageID, ")
                .append("   a.docid, b.PageCode, b.PicPath, b.PicPath, ")
                .append("   b.PageName,'tif',a.ManageCom,a.ScanOperator, ")
                .append(
                        "   a.MakeDate, a.maketime, b.PicPath||PageName||'.tif' f ")
                .append("from es_doc_main a, es_doc_pages b ")
                .append("where a.DocID = b.DocID ")
                .append("   and a.State = '01' ")
                .append("   and a.SubType = '" + mSubType + "' ")
                  .append( " and a.doccode not like '109%' ")
                //四川分公司扫描件不发送外包进行录入 add by zc 2016-1-25 
                .append(" and not exists (select 1 from ldcode where codetype='BPONotSendCom' and othersign='4' and code = substr(a.ManageCom, 1, 4)) ")
                .append(" and not exists (select 1 from ldcode where codetype='BPONotSendCom' and othersign='8' and code = a.ManageCom) ")
           
                // 暂时控制发送外包数据范围。
                // 除 8612（天津分公司及下属三级机构）外，均走新外包。
                // .append("   and a.ManageCom like '8653%'")
                // .append(" and subStr(a.ManageCom, 1, 4) in ('8653', '8621', '8694', '8635', '8637', '8632') ")
                // .append("   and a.ManageCom not like '8612%'")
                .append(
                        " and not exists (select 1 from LCScanDownload lcsd where lcsd.DocId = a.DocId) ")
                // ---------------------------------------

                .append(
                        "   and a.MakeDate >= Current Date - int((select Code from LDCode where CodeType = 'BPOInterval')) Day ")
                .append("order by a.ManageCom, a.DocCode ");
        System.out.println(tSBql.toString());

        ExeSQL strExeSQL = new ExeSQL();
        SSRS tSSRS2 = strExeSQL.execSQL(tSBql.toString());
        if (tSSRS2.getMaxRow() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有需要处理的扫描件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        return tSSRS2;
    }


    /**
     * createBPOBatchInfo
     *
     * @param cBPOServerInfoSchema BPOServerInfoSchema
     * @param cSSRS2 SSRS
     * @return boolean
     */
    private boolean createBPOBatchInfo(BPOServerInfoSchema cBPOServerInfoSchema)
    {
        BPOBatchInfoSchema tBPOBatchInfoSchema = new BPOBatchInfoSchema();
        tBPOBatchInfoSchema.setBatchNo(mBPOBatchNo);
        tBPOBatchInfoSchema.setBPOID(cBPOServerInfoSchema.getBPOID());
        tBPOBatchInfoSchema.setBackupBasePath(cBPOServerInfoSchema
                .getBackupBasePath());
        tBPOBatchInfoSchema.setDocNumber(mBatchDealtScanCountSet.size());
        tBPOBatchInfoSchema.setPageNumber(mBatchDealtPageCount);
        tBPOBatchInfoSchema.setSendOutDate(PubFun.getCurrentDate());
        tBPOBatchInfoSchema.setSendOutTime(PubFun.getCurrentTime());
        tBPOBatchInfoSchema.setState("01");
        tBPOBatchInfoSchema.setOperator(mG.Operator);
        PubFun.fillDefaultField(tBPOBatchInfoSchema);
        map.put(tBPOBatchInfoSchema, "INSERT");

        return true;
    }

    /**
     * cerateXLMBody
     */
    private void cerateXLMBody(XMLDataset tXMLDataset, int cDocNumber)
    {
        //团个单标志，1个单，2团单
        tXMLDataset.setContType("1");
        tXMLDataset.setTemplate("0");
        tXMLDataset.setPrinter("");
        tXMLDataset.addDataObject(new XMLDataTag("BatchNo", mBPOBatchNo));
        tXMLDataset.addDataObject(new XMLDataTag("DocNumber", cDocNumber));
    }

    /**
     * 生成xml中扫描页row信息
     * @param cPageInfo String[]
     */
    private void createXMLListRow(XMLDataList cXMLDataList, String[] cPageInfo)
    {
        cXMLDataList.setColValue("PrtNo", cPageInfo[0]);
        cXMLDataList.setColValue("BussType", cPageInfo[1]);
        cXMLDataList.setColValue("SubType", cPageInfo[2]);
        cXMLDataList.setColValue("PageID", cPageInfo[3]);
        cXMLDataList.setColValue("DocID", cPageInfo[4]);
        cXMLDataList.setColValue("PageCode", cPageInfo[5]);
        cXMLDataList.setColValue("RelativePath", cPageInfo[6]);
        cXMLDataList.setColValue("PicPathPtp", cPageInfo[7]);
        cXMLDataList.setColValue("PageName", cPageInfo[8]);
        cXMLDataList.setColValue("PageSuffix", cPageInfo[9]);
        cXMLDataList.setColValue("ManageCom", cPageInfo[10]);
        cXMLDataList.setColValue("Operator", cPageInfo[11]);
        cXMLDataList.setColValue("MakeDate", cPageInfo[12]);
        cXMLDataList.setColValue("MakeTime", cPageInfo[13]);
        cXMLDataList.insertRow(0);
    }

    /**
     * 生成扫描件的外包录入状态信息
     * @param cPageInfo String[]：扫描页的信息
     * @return boolean：成功true，否则false
     */
    private boolean createState(String[] cPageInfo)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("subtypetoactivityid");
        tLDCodeDB.setCode(mSubType);
        if (!tLDCodeDB.getInfo())
        {
            mErrors.addOneError("没有查询到扫描件工作流信息");
            return false;
        }

        //外包任务状态
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionProp1(cPageInfo[0]);
        tLWMissionDB.setProcessID(tLDCodeDB.getCodeName()); //个单
        tLWMissionDB.setActivityID(tLDCodeDB.getCodeAlias()); //待录入
        LWMissionSet tLWMissionSet = tLWMissionDB.query();
        if (tLWMissionSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "dealData";
            tError.errorMessage = "印刷号为" + cPageInfo[0] + "的保单没有生成工作流";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        BPOMissionStateSchema tBPOMissionStateSchema = new BPOMissionStateSchema();
        tBPOMissionStateSchema.setActivityID(tLWMissionSet.get(1)
                .getActivityID());
        tBPOMissionStateSchema.setSubMissionID(tLWMissionSet.get(1)
                .getSubMissionID());
        tBPOMissionStateSchema
                .setMissionID(tLWMissionSet.get(1).getMissionID());
        tBPOMissionStateSchema
                .setProcessID(tLWMissionSet.get(1).getProcessID());
        tBPOMissionStateSchema.setBPOBatchNo(mBPOBatchNo);
        tBPOMissionStateSchema.setBatchNo(mBPOBatchNo);
        tBPOMissionStateSchema.setImportCount("0");
        tBPOMissionStateSchema.setDealCount("1");
        tBPOMissionStateSchema.setBussNo(cPageInfo[0]);
        tBPOMissionStateSchema.setBussNoType("TB");
        tBPOMissionStateSchema.setManagecom(cPageInfo[10]);
        tBPOMissionStateSchema.setBPOID(tBPOServerInfoSchema.getBPOID());
        tBPOMissionStateSchema.setState(BPO.MISSION_STATE_SEND_SUCC);
        tBPOMissionStateSchema.setOperator(mG.Operator);
        PubFun.fillDefaultField(tBPOMissionStateSchema);
        map.put(tBPOMissionStateSchema, "DELETE&INSERT");

        //外包任务处理详细状态表
        BPOMissionDetailStateSchema tBPOMissionDetailStateSchema = new BPOMissionDetailStateSchema();
        ref.transFields(tBPOMissionDetailStateSchema, tBPOMissionStateSchema);
        tBPOMissionDetailStateSchema.setOperateType(BPO.OPERATE_TYPE_SEND);
        tBPOMissionDetailStateSchema.setSequenceNo(1);
        tBPOMissionDetailStateSchema.setOperateResult("1");
        tBPOMissionDetailStateSchema.setContent("扫描件发送前置机成功");
        tBPOMissionDetailStateSchema.setOperator(mG.Operator);
        PubFun.fillDefaultField(tBPOMissionDetailStateSchema);
        map.put(tBPOMissionDetailStateSchema, "DELETE&INSERT");

        if (cPageInfo[4] != null)
        {
            String sql = "update ES_DOC_MAIN "
                    + "set State = '03', " //发送前置机成功
                    + "   Operator = '" + mG.Operator + "', "
                    + "   ModifyDate = '" + PubFun.getCurrentDate() + "', "
                    + "   ModifyTime = '" + PubFun.getCurrentTime() + "' "
                    + "where DocID = " + cPageInfo[4];
            map.put(sql, SysConst.UPDATE);
        }

        return true;
    }

    //发送扫描件给前置机
    private boolean sendZip(String cZipFilePath)
    {
        //登入前置机
        String ip = tBPOServerInfoSchema.getServerIP();
        String user = tBPOServerInfoSchema.getLogInUser();
        String pwd = tBPOServerInfoSchema.getLogInPwd();
        String port = tBPOServerInfoSchema.getServerPort();
        FTPTool tFTPTool = new FTPTool(ip, user, pwd, Integer.parseInt(port));
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            }
            else
            {
                if (!tFTPTool.upload(tBPOServerInfoSchema.getBasePath(),
                        cZipFilePath))
                {
                    System.out.println("上载文件失败!");
                    CError tError = new CError();
                    tError.moduleName = "BPOSentScanBL";
                    tError.functionName = "sendZip";
                    tError.errorMessage = tFTPTool
                            .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                    mErrors.addOneError(tError);
                    System.out.println(tError.errorMessage);

                    deleteFile(cZipFilePath);

                    return false;
                }
                tFTPTool.logoutFTP();
            }
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是网络异常。");
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "sendZip";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是无法写入文件");
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "sendZip";
            tError.errorMessage = tFTPTool
                    .getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    public boolean submit()
    {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(map);
        if (!tPubSubmit.submitData(mResult, ""))
        {
            CError tError = new CError();
            tError.moduleName = "BPOSentScanBL";
            tError.functionName = "submit";
            tError.errorMessage = "存储外包任务状态出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        this.map = new MMap();
        return true;
    }

    public void setSubType(String subType)
    {
        mSubType = subType;
    }

    public static void main(String[] args)
    {

    }
}
