package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.easyscan.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class QCManageBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**内部schema*/
    private ES_DOC_QC_MAINSchema mES_DOC_QC_MAINSchema = new
            ES_DOC_QC_MAINSchema();
    private ES_DOC_QC_TRACESchema mES_DOC_QC_TRACESchema = new
            ES_DOC_QC_TRACESchema();
    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();

    public QCManageBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        try {
            // 将传入的数据拷贝到本类中
            this.mInputData = (VData) cInputData.clone();
            this.mOperate = cOperate;
            //判断传入的操作字符串
            if (!cOperate.equals("QCPASS") && !cOperate.equals("QCMODIFY") &&
                !cOperate.equals("QCDELETE") && !cOperate.equals("SCANMODIFY") &&
                !cOperate.equals("SCANDELETE") &&
                !cOperate.equals("SCANMODIFYOK") &&
                !cOperate.equals("SCANDELETEOK")) {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 将外部传入的数据分解到本类的属性中，准备处理
            if (this.getInputData(cInputData) == false)
                return false;
            System.out.println("---End getInputData---");
            //校验数据
            if (this.checkData() == false)
                return false;
            System.out.println("---End checkData---");
            //处理数据
            if (this.dealData() == false)
                return false;
            System.out.println("---End dealData---");
            //统一递交数据
            mResult.add(map);
            PubSubmit pubsubmit = new PubSubmit();
            if (!pubsubmit.submitData(mResult, cOperate)) {
                mErrors.copyAllErrors(pubsubmit.mErrors);
                return false;
            }
            System.out.println("---End pubsubmit---");
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return true;
    }

    private boolean getInputData(VData cInputData) {

        this.mES_DOC_QC_MAINSchema.setSchema((ES_DOC_QC_MAINSchema) cInputData.
                                             getObjectByObjectName(
                "ES_DOC_QC_MAINSchema", 0));
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (this.mES_DOC_QC_MAINSchema == null) {
            buildError("getInputData",
                       "没有得到schema");
            return false;
        }
        return true;
    }

    private boolean checkData() {
        String cOperate = this.mOperate;
        ES_DOC_QC_MAINSchema cES_DOC_QC_MAINSchema = new
                ES_DOC_QC_MAINSchema();
        ES_DOC_QC_MAINSet cES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();
        ES_DOC_QC_MAINDB cES_DOC_QC_MAINDB = new ES_DOC_QC_MAINDB();
        cES_DOC_QC_MAINDB.setDocID(this.mES_DOC_QC_MAINSchema.getDocID());
        cES_DOC_QC_MAINSet = cES_DOC_QC_MAINDB.query();
        cES_DOC_QC_MAINSchema = cES_DOC_QC_MAINSet.get(1).getSchema();
        String cQCFlag = cES_DOC_QC_MAINSchema.getQCFlag(); //质检标记
        String cScanOpeState = cES_DOC_QC_MAINSchema.getScanOpeState(); //扫描员执行操作

        if (cOperate.equals("QCPASS") || cOperate.equals("QCMODIFY") ||
            cOperate.equals("QCDELETE")) {
            if (!cQCFlag.equals("0") && !cScanOpeState.equals("3")) {
                buildError("checkData",
                           "只有处于待质检和修改完毕的单证允许此操作");
                return false;
            }
        }
        if (cOperate.equals("SCANMODIFY")) {
            if (!cQCFlag.equals("1") || !cScanOpeState.equals("")) {
                buildError("checkData",
                           "只有处于未通过状态并且状态不是修改完毕的单证允许此操作");
                return false;
            }
        }
        if (cOperate.equals("SCANDELETE")) {
            if (!cQCFlag.equals("1") || !cScanOpeState.equals("")) {
                buildError("checkData",
                           "只有处于未通过状态并且状态不是修改完毕的单证允许此操作");
                return false;
            }
        }

        if (cOperate.equals("SCANMODIFYOK")) {
            if (!cScanOpeState.equals("1")) {
                buildError("checkData",
                           "只有处于修改状态的单证允许此操作");
                return false;
            }
        }
        if (cOperate.equals("SCANDELETEOK")) {
            if (!cScanOpeState.equals("2")) {
                buildError("checkData",
                           "只有处于删除重扫中状态的单证允许此操作");
                return false;
            }
        }

        return true;
    }

    private boolean dealData() {
        String cOperate = this.mOperate;
        ES_DOC_QC_MAINSchema cES_DOC_QC_MAINSchema = new
                ES_DOC_QC_MAINSchema();
        ES_DOC_QC_MAINSet cES_DOC_QC_MAINSet = new ES_DOC_QC_MAINSet();
        ES_DOC_QC_MAINDB cES_DOC_QC_MAINDB = new ES_DOC_QC_MAINDB();
        cES_DOC_QC_MAINDB.setDocID(this.mES_DOC_QC_MAINSchema.getDocID());
        cES_DOC_QC_MAINSet = cES_DOC_QC_MAINDB.query();
        cES_DOC_QC_MAINSchema = cES_DOC_QC_MAINSet.get(1).getSchema();
        cES_DOC_QC_MAINSchema.setModifyDate(PubFun.getCurrentDate());
        cES_DOC_QC_MAINSchema.setModifyTime(PubFun.getCurrentTime());
        //存放单证主表的信息
        ES_DOC_MAINDB cES_DOC_MAINDB = new ES_DOC_MAINDB();
        cES_DOC_MAINDB.setDocID(cES_DOC_QC_MAINSchema.getDocID());
        ES_DOC_MAINSet cES_DOC_MAINSet = cES_DOC_MAINDB.query();

        if (cOperate.equals("QCPASS")) {
            cES_DOC_QC_MAINSchema.setQCFlag("2");
            cES_DOC_QC_MAINSchema.setQCSuggest("");
            cES_DOC_QC_MAINSchema.setScanOpeState("");
            cES_DOC_QC_MAINSchema.setUnpassReason("");
            cES_DOC_QC_MAINSchema.setQCOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setQCManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setQCLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setQCLastApplyTime(PubFun.getCurrentTime());

            VData cInputData = new VData();
            cInputData.add(cES_DOC_QC_MAINSchema);
            if (callService(cInputData) == false) {
                return false;
            }
            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        if (cOperate.equals("QCMODIFY")) {
            cES_DOC_QC_MAINSchema.setQCFlag("1");
            cES_DOC_QC_MAINSchema.setQCSuggest("1");
            cES_DOC_QC_MAINSchema.setScanOpeState("");
            cES_DOC_QC_MAINSchema.setUnpassReason(mES_DOC_QC_MAINSchema.
                                                  getUnpassReason());
            cES_DOC_QC_MAINSchema.setQCOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setQCManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setQCLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setQCLastApplyTime(PubFun.getCurrentTime());

            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        if (cOperate.equals("QCDELETE")) {
            cES_DOC_QC_MAINSchema.setQCFlag("1");
            cES_DOC_QC_MAINSchema.setQCSuggest("2");
            cES_DOC_QC_MAINSchema.setScanOpeState("");
            cES_DOC_QC_MAINSchema.setUnpassReason(mES_DOC_QC_MAINSchema.
                                                  getUnpassReason());
            cES_DOC_QC_MAINSchema.setQCOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setQCManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setQCLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setQCLastApplyTime(PubFun.getCurrentTime());

            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        if (cOperate.equals("SCANMODIFY")) {
            cES_DOC_QC_MAINSchema.setQCFlag("1");
            cES_DOC_QC_MAINSchema.setScanOpeState("1");
            cES_DOC_QC_MAINSchema.setScanOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setScanManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setScanLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setScanLastApplyTime(PubFun.getCurrentTime());

            VData cInputData = new VData();
            cInputData.add(cES_DOC_QC_MAINSchema);
            if (dealModify(cInputData) == false) {
                return false;
            }
            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        if (cOperate.equals("SCANDELETE")) {
            cES_DOC_QC_MAINSchema.setQCFlag("1");
            cES_DOC_QC_MAINSchema.setScanOpeState("2");
            cES_DOC_QC_MAINSchema.setScanOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setScanManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setScanLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setScanLastApplyTime(PubFun.getCurrentTime());
            VData cInputData = new VData();
            cInputData.add(cES_DOC_QC_MAINSchema);
            if (dealDelete(cInputData) == false) {
                return false;
            }
            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        if (cOperate.equals("SCANMODIFYOK")) {
            cES_DOC_QC_MAINSchema.setQCFlag("1");
            cES_DOC_QC_MAINSchema.setScanOpeState("3");
            cES_DOC_QC_MAINSchema.setScanOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setScanManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setScanLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setScanLastApplyTime(PubFun.getCurrentTime());

            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        if (cOperate.equals("SCANDELETEOK")) {
            cES_DOC_QC_MAINSchema.setQCFlag("1");
            cES_DOC_QC_MAINSchema.setScanOpeState("4");
            cES_DOC_QC_MAINSchema.setScanOperator(mGlobalInput.Operator);
            cES_DOC_QC_MAINSchema.setScanManageCom(mGlobalInput.ManageCom);
            cES_DOC_QC_MAINSchema.setScanLastApplyDate(PubFun.getCurrentDate());
            cES_DOC_QC_MAINSchema.setScanLastApplyTime(PubFun.getCurrentTime());

            map.put(cES_DOC_QC_MAINSchema, "UPDATE");
        }
        return true;
    }

    private boolean callService(VData cInputData) {
        ES_DOC_QC_MAINSchema cES_DOC_QC_MAINSchema = new ES_DOC_QC_MAINSchema();
        cES_DOC_QC_MAINSchema.setSchema((ES_DOC_QC_MAINSchema) cInputData.
                                        getObjectByObjectName(
                                                "ES_DOC_QC_MAINSchema", 0));
        ES_DOC_MAINDB cES_DOC_MAINDB = new ES_DOC_MAINDB();
        ES_DOC_MAINSet cES_DOC_MAINSet = new ES_DOC_MAINSet();
        cES_DOC_MAINDB.setDocID(cES_DOC_QC_MAINSchema.getDocID());
        cES_DOC_MAINSet = cES_DOC_MAINDB.query();
        VData tInputData = new VData();
        tInputData.add(cES_DOC_MAINSet);
        CallService tCallService = new CallService();

        if (!tCallService.submitData(tInputData, "INSERT", "1")) {
            mErrors.copyAllErrors(tCallService.mErrors);
            return false;
        }
        map.add((MMap) tCallService.getResult().getObjectByObjectName(
                "MMap", 0));
        return true;
    }

    private boolean dealModify(VData cInputData) {
        ES_DOC_QC_MAINSchema cES_DOC_QC_MAINSchema = new ES_DOC_QC_MAINSchema();
        cES_DOC_QC_MAINSchema.setSchema((ES_DOC_QC_MAINSchema) cInputData.
                                        getObjectByObjectName(
                                                "ES_DOC_QC_MAINSchema", 0));
        Es_IssueDocSchema cEs_IssueDocSchema = new Es_IssueDocSchema();
        ES_DOC_QC_TRACESchema cES_DOC_QC_MAINTRACESchema = new
                ES_DOC_QC_TRACESchema();

        String cSQL = "select max(traceid) from ES_DOC_QC_TRACE where docid=" +
                      cES_DOC_QC_MAINSchema.getDocID();
        ExeSQL cExeSQL = new ExeSQL();
        String cCount1;
        int c;
        cCount1 = cExeSQL.getOneValue(cSQL);
        if (cCount1 != null && !cCount1.equals("")) {

            Integer cInteger = new Integer(cCount1);
            c = cInteger.intValue();
            c = c + 1;
        }

        else {
            c = 1;
        }
        cES_DOC_QC_MAINTRACESchema.setTraceID(c);
        String cmapSQL = "insert into es_doc_qc_trace (select " + c +
                         ", a.* from es_doc_qc_main a where a.docid=" +
                         cES_DOC_QC_MAINSchema.getDocID() + ")";
        map.put(cmapSQL, "INSERT");

        String tSQL =
                "select MAX(to_number(IssueDocID)) from Es_IssueDoc where BussNo is not null";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount1;
        int i;
        tCount1 = tExeSQL.getOneValue(tSQL);
        if (tCount1 != null && !tCount1.equals("")) {

            Integer tInteger = new Integer(tCount1);
            i = tInteger.intValue();
            i = i + 1;
        }

        else {
            i = 1;
        }
        cEs_IssueDocSchema.setIssueDocID(String.valueOf(i));
        cEs_IssueDocSchema.setBussNo(cES_DOC_QC_MAINSchema.getDocCode());
        cEs_IssueDocSchema.setBussNoType("11");
        cEs_IssueDocSchema.setBussType(cES_DOC_QC_MAINSchema.getBussType());
        cEs_IssueDocSchema.setSubType(cES_DOC_QC_MAINSchema.getSubType());
        cEs_IssueDocSchema.setIssueDesc(cES_DOC_QC_MAINSchema.getUnpassReason());
        cEs_IssueDocSchema.setStatus("0");
        cEs_IssueDocSchema.setResult("");
        cEs_IssueDocSchema.setReplyOperator("");
        cEs_IssueDocSchema.setPromptOperator(cES_DOC_QC_MAINSchema.
                                             getScanOperator());
        cEs_IssueDocSchema.setMakeDate(PubFun.getCurrentDate());
        cEs_IssueDocSchema.setMakeTime(PubFun.getCurrentTime());
        cEs_IssueDocSchema.setModifyDate(PubFun.getCurrentDate());
        cEs_IssueDocSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(cEs_IssueDocSchema, "INSERT");

        return true;
    }

    private boolean dealDelete(VData cInputData) {
        ES_DOC_QC_MAINSchema cES_DOC_QC_MAINSchema = new ES_DOC_QC_MAINSchema();
        cES_DOC_QC_MAINSchema.setSchema((ES_DOC_QC_MAINSchema) cInputData.
                                        getObjectByObjectName(
                                                "ES_DOC_QC_MAINSchema", 0));
        double cDocID = cES_DOC_QC_MAINSchema.getDocID();
        map.put(
                "insert into es_doc_mainb (select * from es_doc_main where docid=" +
                cDocID + ")", "INSERT");
        map.put(
                "insert into es_doc_pagesb (select * from es_doc_pages where docid=" +
                cDocID + ")", "INSERT");
        map.put("delete from es_doc_main where docid=" + cDocID + "", "DELETE");
        map.put("delete from es_doc_pages where docid=" + cDocID + "", "DELETE");

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        {
            QCManageUI tQCManageUI = new QCManageUI();

            ES_DOC_QC_MAINSchema tES_DOC_QC_MAINSchema = new
                    ES_DOC_QC_MAINSchema();
            tES_DOC_QC_MAINSchema.setDocID(547);
            tES_DOC_QC_MAINSchema.setUnpassReason("你好");
            GlobalInput tG = new GlobalInput();
            tG.ComCode = "86";
            tG.ManageCom = "86";
            tG.Operator = "app0001";
            VData vData = new VData();
            vData.add(tG);
            vData.add(tES_DOC_QC_MAINSchema);
            if (!tQCManageUI.submitData(vData, "QCMODIFY"))
                ;
            System.out.println("falasfdsfsdfsd");

        }

    }
}

