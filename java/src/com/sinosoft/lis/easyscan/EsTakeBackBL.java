package com.sinosoft.lis.easyscan;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EsTakeBackBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 数据操作字符串 */
    private String mIssueDocID;

    /** 数据操作字符串 */
    private boolean mIssueDocIDFlag = false;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private Es_IssueDocSchema mEs_IssueDocSchema = new Es_IssueDocSchema();

    MMap map = new MMap();

    private GlobalInput mGlobalInput = new GlobalInput();



    public EsTakeBackBL()
    {
    }
    
    //递交数据
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start submitData");
        try
        {
            this.mInputData = (VData) cInputData.clone();
            this.mOperate = cOperate;
            if (!this.mOperate.equals("ApplyDel") )
                  
            {
                buildError("checkData", "不支持的字符串" + mOperate + "");
                return false;
            }
            if (!this.getInputData(cInputData) )
            {
                return false;
            }
            System.out.println("---End getInputData---");
            if (!this.checkData() )
            {
                return false;
            }
            System.out.println("---End checkData---");
            if (!this.dealData())
            {
                return false;
            }
            System.out.println("---End dealData---");
            mResult.add(map);
            PubSubmit pubsubmit = new PubSubmit();
            if (!pubsubmit.submitData(mResult, ""))
            {
                mErrors.copyAllErrors(pubsubmit.mErrors);
                return false;
            }
            System.out.println("---End pubsubmit---");
        }
        catch (Exception ex)
        {

        }
        return true;
    }
    
    //建立错误机制
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "EsAssessBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    //得到前台输入的数据
    private boolean getInputData(VData cInputData)
    {
        this.mEs_IssueDocSchema.setSchema((Es_IssueDocSchema) cInputData
                .getObjectByObjectName("Es_IssueDocSchema", 0));
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
       
        return true;
    }
    
    //检测得到的数据  和 相应的操作方法
    private boolean checkData()
    {
    	 if (this.mEs_IssueDocSchema == null)
         {
             buildError("getInputData", "没有得到schema");
             return false;
         }
         if ("ApplyDel".equals(this.mOperate))
         {
             this.mOperate = "ApplyDel";
             return true;
         }

        return true;
    }
    
    //数据处理 撤销扫描状态
    private boolean dealData()
    {
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
   
        //判断操作方法  如果等于ApplyDel 执行下面的操作
        if (this.mOperate.equals("ApplyDel"))
        { 
           
            Es_IssueDocDB tEs_IssueDocDB = new Es_IssueDocDB();        
            tEs_IssueDocDB.setBussNo(this.mEs_IssueDocSchema.getBussNo());
            tEs_IssueDocDB.setSubType(this.mEs_IssueDocSchema.getSubType());
            Es_IssueDocSet Es_IssueDocSet = tEs_IssueDocDB.query();
            tEs_IssueDocDB.setSchema(mEs_IssueDocSchema);

			//判断查询的结果是不是唯一的
            if (Es_IssueDocSet.size() != 1)
            {
                buildError("dealData", "扫描修改申请了多次！");
                
                        
                return false;
            }
            //取集合中的一个Schema 对其进行UPDATE
            Es_IssueDocSchema tEs_IssueDocSchema = Es_IssueDocSet.get(1);
            tEs_IssueDocSchema.setStatus("1");
            tEs_IssueDocSchema.setStateFlag("4");
            tEs_IssueDocSchema.setResult("扫描申请撤销");
            tEs_IssueDocSchema.setCheckContent(this.mEs_IssueDocSchema
                    .getCheckContent());
            tEs_IssueDocSchema.setCheckOperator(this.mGlobalInput.Operator);
            tEs_IssueDocSchema.setCheckDate(cDate);
            tEs_IssueDocSchema.setCheckTime(cTime);
            this.map.put(tEs_IssueDocSchema, "UPDATE");
        }
        return true;
    }

}
