/*
 * <p>ClassName: LPEdorPrintDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-01-20
 */
package com.sinosoft.lis.vdb;

import java.sql.Connection;

import com.sinosoft.lis.schema.LPEdorPrintSchema;
import com.sinosoft.lis.vschema.LPEdorPrintSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBOper;

public class LPEdorPrintDBSet extends LPEdorPrintSet
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    // @Constructor
    public LPEdorPrintDBSet(Connection tConnection)
    {
        con = tConnection;
        db = new DBOper(con, "LPEdorPrint");
        mflag = true;
    }

    public LPEdorPrintDBSet()
    {
        db = new DBOper("LPEdorPrint");
        // con = db.getConnection();
    }

    // @Method
    public boolean insert()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LPEdorPrintSchema aSchema = new LPEdorPrintSchema();
            aSchema.setSchema((LPEdorPrintSchema)this.get(i));
            if (db.insert(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LPEdorPrintDBSet";
                tError.functionName = "insert";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean update()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LPEdorPrintSchema aSchema = new LPEdorPrintSchema();
            aSchema.setSchema((LPEdorPrintSchema)this.get(i));
            if (db.update(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LPEdorPrintDBSet";
                tError.functionName = "update";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean deleteSQL()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LPEdorPrintSchema aSchema = new LPEdorPrintSchema();
            aSchema.setSchema((LPEdorPrintSchema)this.get(i));
            if (db.deleteSQL(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LPEdorPrintDBSet";
                tError.functionName = "deleteSQL";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean delete()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LPEdorPrintSchema aSchema = new LPEdorPrintSchema();
            aSchema.setSchema((LPEdorPrintSchema)this.get(i));
            if (db.delete(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LPEdorPrintDBSet";
                tError.functionName = "delete";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

}
