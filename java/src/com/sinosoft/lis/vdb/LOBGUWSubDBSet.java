/*
 * <p>ClassName: LOBGUWSubDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: temp
 * @CreateDate：2004-12-07
 */
package com.sinosoft.lis.vdb;

import java.sql.Connection;

import com.sinosoft.lis.schema.LOBGUWSubSchema;
import com.sinosoft.lis.vschema.LOBGUWSubSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBOper;

public class LOBGUWSubDBSet extends LOBGUWSubSet
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    // @Constructor
    public LOBGUWSubDBSet(Connection tConnection)
    {
        con = tConnection;
        db = new DBOper(con, "LOBGUWSub");
        mflag = true;
    }

    public LOBGUWSubDBSet()
    {
        db = new DBOper("LOBGUWSub");
        // con = db.getConnection();
    }

    // @Method
    public boolean insert()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBGUWSubSchema aSchema = new LOBGUWSubSchema();
            aSchema.setSchema((LOBGUWSubSchema)this.get(i));
            if (db.insert(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LOBGUWSubDBSet";
                tError.functionName = "insert";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean update()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBGUWSubSchema aSchema = new LOBGUWSubSchema();
            aSchema.setSchema((LOBGUWSubSchema)this.get(i));
            if (db.update(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LOBGUWSubDBSet";
                tError.functionName = "update";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean deleteSQL()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBGUWSubSchema aSchema = new LOBGUWSubSchema();
            aSchema.setSchema((LOBGUWSubSchema)this.get(i));
            if (db.deleteSQL(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LOBGUWSubDBSet";
                tError.functionName = "deleteSQL";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean delete()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBGUWSubSchema aSchema = new LOBGUWSubSchema();
            aSchema.setSchema((LOBGUWSubSchema)this.get(i));
            if (db.delete(aSchema) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LOBGUWSubDBSet";
                tError.functionName = "delete";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

}
