/*
 * <p>ClassName: tab_riskcode_covertDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2007-07-06
 */
package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.tab_riskcode_covertSchema;
import com.sinosoft.lis.vschema.tab_riskcode_covertSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class tab_riskcode_covertDBSet extends tab_riskcode_covertSet
{
	// @Field
	private Connection con;
	private DBOper db;
	/**
	* flag = true: 传入Connection
	* flag = false: 不传入Connection
	**/
	private boolean mflag = false;

	public CErrors mErrors = new CErrors();			// 错误信息

	// @Constructor
	public tab_riskcode_covertDBSet(Connection tConnection)
	{
		con = tConnection;
		db = new DBOper(con,"tab_riskcode_covert");
		mflag = true;
	}

	public tab_riskcode_covertDBSet()
	{
		db = new DBOper( "tab_riskcode_covert" );
	}
	// @Method
	public boolean insert()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			tab_riskcode_covertSchema aSchema = new tab_riskcode_covertSchema();
			aSchema.setSchema( (tab_riskcode_covertSchema)this.get(i) );
			if (db.insert(aSchema) == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "tab_riskcode_covertDBSet";
				tError.functionName = "insert";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

	public boolean update()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			tab_riskcode_covertSchema aSchema = new tab_riskcode_covertSchema();
			aSchema.setSchema( (tab_riskcode_covertSchema)this.get(i) );
			if (db.update(aSchema) == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "tab_riskcode_covertDBSet";
				tError.functionName = "update";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

	public boolean deleteSQL()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			tab_riskcode_covertSchema aSchema = new tab_riskcode_covertSchema();
			aSchema.setSchema( (tab_riskcode_covertSchema)this.get(i) );
			if (db.deleteSQL(aSchema) == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "tab_riskcode_covertDBSet";
				tError.functionName = "deleteSQL";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

	public boolean delete()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			tab_riskcode_covertSchema aSchema = new tab_riskcode_covertSchema();
			aSchema.setSchema( (tab_riskcode_covertSchema)this.get(i) );
			if (db.delete(aSchema) == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "tab_riskcode_covertDBSet";
				tError.functionName = "delete";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

}
