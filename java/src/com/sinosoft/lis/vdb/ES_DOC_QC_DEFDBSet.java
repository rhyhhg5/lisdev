/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.sinosoft.lis.vschema.ES_DOC_QC_DEFSet;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_QC_DEFDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class ES_DOC_QC_DEFDBSet extends ES_DOC_QC_DEFSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;


    // @Constructor
    public ES_DOC_QC_DEFDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "ES_DOC_QC_DEF");
        mflag = true;
    }

    public ES_DOC_QC_DEFDBSet() {
        db = new DBOper("ES_DOC_QC_DEF");
        // con = db.getConnection();
    }

    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_DEFDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM ES_DOC_QC_DEF WHERE  ManageCom = ? AND BussType = ? AND SubType = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.GBKToUnicode(this.get(i).
                            getManageCom()));
                }
                if (this.get(i).getBussType() == null ||
                    this.get(i).getBussType().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).
                            getBussType()));
                }
                if (this.get(i).getSubType() == null ||
                    this.get(i).getSubType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).getSubType()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_DEFDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE ES_DOC_QC_DEF SET  ManageCom = ? , BussType = ? , SubType = ? , Remark = ? , State = ? WHERE  ManageCom = ? AND BussType = ? AND SubType = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.GBKToUnicode(this.get(i).
                            getManageCom()));
                }
                if (this.get(i).getBussType() == null ||
                    this.get(i).getBussType().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).
                            getBussType()));
                }
                if (this.get(i).getSubType() == null ||
                    this.get(i).getSubType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).getSubType()));
                }
                if (this.get(i).getRemark() == null ||
                    this.get(i).getRemark().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.GBKToUnicode(this.get(i).getRemark()));
                }
                if (this.get(i).getState() == null ||
                    this.get(i).getState().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.GBKToUnicode(this.get(i).getState()));
                }
                // set where condition
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(6, null);
                } else {
                    pstmt.setString(6,
                                    StrTool.GBKToUnicode(this.get(i).
                            getManageCom()));
                }
                if (this.get(i).getBussType() == null ||
                    this.get(i).getBussType().equals("null")) {
                    pstmt.setString(7, null);
                } else {
                    pstmt.setString(7,
                                    StrTool.GBKToUnicode(this.get(i).
                            getBussType()));
                }
                if (this.get(i).getSubType() == null ||
                    this.get(i).getSubType().equals("null")) {
                    pstmt.setString(8, null);
                } else {
                    pstmt.setString(8,
                                    StrTool.GBKToUnicode(this.get(i).getSubType()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_DEFDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement(
                    "INSERT INTO ES_DOC_QC_DEF VALUES( ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.GBKToUnicode(this.get(i).
                            getManageCom()));
                }
                if (this.get(i).getBussType() == null ||
                    this.get(i).getBussType().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).
                            getBussType()));
                }
                if (this.get(i).getSubType() == null ||
                    this.get(i).getSubType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).getSubType()));
                }
                if (this.get(i).getRemark() == null ||
                    this.get(i).getRemark().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.GBKToUnicode(this.get(i).getRemark()));
                }
                if (this.get(i).getState() == null ||
                    this.get(i).getState().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.GBKToUnicode(this.get(i).getState()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_DEFDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

}
