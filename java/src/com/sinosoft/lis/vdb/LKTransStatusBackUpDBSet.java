/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.vschema.LKTransStatusBackUpSet;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LKTransStatusBackUpDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2008-03-24
 */
public class LKTransStatusBackUpDBSet extends LKTransStatusBackUpSet
{
	// @Field
	private Connection con;
	private DBOper db;
	/**
	* flag = true: 传入Connection
	* flag = false: 不传入Connection
	**/
	private boolean mflag = false;


	// @Constructor
	public LKTransStatusBackUpDBSet(Connection tConnection)
	{
		con = tConnection;
		db = new DBOper(con,"LKTransStatusBackUp");
		mflag = true;
	}

	public LKTransStatusBackUpDBSet()
	{
		db = new DBOper( "LKTransStatusBackUp" );
		con = null;
		mflag = false;
	}
	// @Method
	public boolean deleteSQL()
	{
		if (db.deleteSQL(this))
		{
		        return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LKTransStatusBackUpDBSet";
			tError.functionName = "deleteSQL";
			tError.errorMessage = "操作失败!";
			this.mErrors .addOneError(tError);
			return false;
		}
	}

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
	public boolean delete()
	{
		PreparedStatement pstmt = null;

		if( !mflag ) {
			con = DBConnPool.getConnection();
		}

		try
		{
            int tCount = this.size();
			pstmt = con.prepareStatement("DELETE FROM LKTransStatusBackUp WHERE ");
            for (int i = 1; i <= tCount; i++)
            {
                pstmt.addBatch();
            }
            pstmt.executeBatch();
			pstmt.close();
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LKTransStatusBackUpDBSet";
			tError.functionName = "delete()";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

            int tCount = this.size();
            for (int i = 1; i <= tCount; i++)
            {
		// only for debug purpose
		SQLString sqlObj = new SQLString("LKTransStatusBackUp");
		sqlObj.setSQL(4, this.get(i));
		sqlObj.getSQL();
            }

			try {
				pstmt.close();
			} catch (Exception e){e.printStackTrace();}

			if( !mflag ) {
				try {
					con.close();
				} catch (Exception e){e.printStackTrace();}
			}

			return false;
		}

		if( !mflag ) {
			try {
				con.close();
			} catch (Exception e){e.printStackTrace();}
		}

		return true;
	}

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
	public boolean update()
	{
		PreparedStatement pstmt = null;

		if( !mflag ) {
			con = DBConnPool.getConnection();
		}

		try
		{
            int tCount = this.size();
			pstmt = con.prepareStatement("UPDATE LKTransStatusBackUp SET  TransCode = ? , ReportNo = ? , BankCode = ? , BankBranch = ? , BankNode = ? , BankOperator = ? , TransNo = ? , FuncFlag = ? , TransDate = ? , TransTime = ? , ManageCom = ? , RiskCode = ? , ProposalNo = ? , PrtNo = ? , PolNo = ? , EdorNo = ? , TempFeeNo = ? , TransAmnt = ? , BankAcc = ? , RCode = ? , TransStatus = ? , Status = ? , Descr = ? , Temp = ? , State_Code = ? , RequestId = ? , OutServiceCode = ? , ClientIP = ? , ClientPort = ? , IssueWay = ? , ServiceStartTime = ? , ServiceEndTime = ? , RBankVSMP = ? , DesBankVSMP = ? , RMPVSKernel = ? , DesMPVSKernel = ? , ResultBalance = ? , DesBalance = ? , bak1 = ? , bak2 = ? , bak3 = ? , bak4 = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE ");
            for (int i = 1; i <= tCount; i++)
            {
			if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
				pstmt.setString(1,null);
			} else {
				pstmt.setString(1, this.get(i).getTransCode().trim());
			}
			if(this.get(i).getReportNo() == null || this.get(i).getReportNo().equals("null")) {
				pstmt.setString(2,null);
			} else {
				pstmt.setString(2, this.get(i).getReportNo().trim());
			}
			if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
				pstmt.setString(3,null);
			} else {
				pstmt.setString(3, this.get(i).getBankCode().trim());
			}
			if(this.get(i).getBankBranch() == null || this.get(i).getBankBranch().equals("null")) {
				pstmt.setString(4,null);
			} else {
				pstmt.setString(4, this.get(i).getBankBranch().trim());
			}
			if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
				pstmt.setString(5,null);
			} else {
				pstmt.setString(5, this.get(i).getBankNode().trim());
			}
			if(this.get(i).getBankOperator() == null || this.get(i).getBankOperator().equals("null")) {
				pstmt.setString(6,null);
			} else {
				pstmt.setString(6, this.get(i).getBankOperator().trim());
			}
			if(this.get(i).getTransNo() == null || this.get(i).getTransNo().equals("null")) {
				pstmt.setString(7,null);
			} else {
				pstmt.setString(7, this.get(i).getTransNo().trim());
			}
			if(this.get(i).getFuncFlag() == null || this.get(i).getFuncFlag().equals("null")) {
				pstmt.setString(8,null);
			} else {
				pstmt.setString(8, this.get(i).getFuncFlag().trim());
			}
			if(this.get(i).getTransDate() == null || this.get(i).getTransDate().equals("null")) {
				pstmt.setDate(9,null);
			} else {
				pstmt.setDate(9, Date.valueOf(this.get(i).getTransDate()));
			}
			if(this.get(i).getTransTime() == null || this.get(i).getTransTime().equals("null")) {
				pstmt.setString(10,null);
			} else {
				pstmt.setString(10, this.get(i).getTransTime().trim());
			}
			if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
				pstmt.setString(11,null);
			} else {
				pstmt.setString(11, this.get(i).getManageCom().trim());
			}
			if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
				pstmt.setString(12,null);
			} else {
				pstmt.setString(12, this.get(i).getRiskCode().trim());
			}
			if(this.get(i).getProposalNo() == null || this.get(i).getProposalNo().equals("null")) {
				pstmt.setString(13,null);
			} else {
				pstmt.setString(13, this.get(i).getProposalNo().trim());
			}
			if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
				pstmt.setString(14,null);
			} else {
				pstmt.setString(14, this.get(i).getPrtNo().trim());
			}
			if(this.get(i).getPolNo() == null || this.get(i).getPolNo().equals("null")) {
				pstmt.setString(15,null);
			} else {
				pstmt.setString(15, this.get(i).getPolNo().trim());
			}
			if(this.get(i).getEdorNo() == null || this.get(i).getEdorNo().equals("null")) {
				pstmt.setString(16,null);
			} else {
				pstmt.setString(16, this.get(i).getEdorNo().trim());
			}
			if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("null")) {
				pstmt.setString(17,null);
			} else {
				pstmt.setString(17, this.get(i).getTempFeeNo().trim());
			}
			pstmt.setDouble(18, this.get(i).getTransAmnt());
			if(this.get(i).getBankAcc() == null || this.get(i).getBankAcc().equals("null")) {
				pstmt.setString(19,null);
			} else {
				pstmt.setString(19, this.get(i).getBankAcc().trim());
			}
			if(this.get(i).getRCode() == null || this.get(i).getRCode().equals("null")) {
				pstmt.setString(20,null);
			} else {
				pstmt.setString(20, this.get(i).getRCode().trim());
			}
			if(this.get(i).getTransStatus() == null || this.get(i).getTransStatus().equals("null")) {
				pstmt.setString(21,null);
			} else {
				pstmt.setString(21, this.get(i).getTransStatus().trim());
			}
			if(this.get(i).getStatus() == null || this.get(i).getStatus().equals("null")) {
				pstmt.setString(22,null);
			} else {
				pstmt.setString(22, this.get(i).getStatus().trim());
			}
			if(this.get(i).getDescr() == null || this.get(i).getDescr().equals("null")) {
				pstmt.setString(23,null);
			} else {
				pstmt.setString(23, this.get(i).getDescr().trim());
			}
			if(this.get(i).getTemp() == null || this.get(i).getTemp().equals("null")) {
				pstmt.setString(24,null);
			} else {
				pstmt.setString(24, this.get(i).getTemp().trim());
			}
			if(this.get(i).getState_Code() == null || this.get(i).getState_Code().equals("null")) {
				pstmt.setString(25,null);
			} else {
				pstmt.setString(25, this.get(i).getState_Code().trim());
			}
			if(this.get(i).getRequestId() == null || this.get(i).getRequestId().equals("null")) {
				pstmt.setString(26,null);
			} else {
				pstmt.setString(26, this.get(i).getRequestId().trim());
			}
			if(this.get(i).getOutServiceCode() == null || this.get(i).getOutServiceCode().equals("null")) {
				pstmt.setString(27,null);
			} else {
				pstmt.setString(27, this.get(i).getOutServiceCode().trim());
			}
			if(this.get(i).getClientIP() == null || this.get(i).getClientIP().equals("null")) {
				pstmt.setString(28,null);
			} else {
				pstmt.setString(28, this.get(i).getClientIP().trim());
			}
			if(this.get(i).getClientPort() == null || this.get(i).getClientPort().equals("null")) {
				pstmt.setString(29,null);
			} else {
				pstmt.setString(29, this.get(i).getClientPort().trim());
			}
			if(this.get(i).getIssueWay() == null || this.get(i).getIssueWay().equals("null")) {
				pstmt.setString(30,null);
			} else {
				pstmt.setString(30, this.get(i).getIssueWay().trim());
			}
			if(this.get(i).getServiceStartTime() == null || this.get(i).getServiceStartTime().equals("null")) {
				pstmt.setDate(31,null);
			} else {
				pstmt.setDate(31, Date.valueOf(this.get(i).getServiceStartTime()));
			}
			if(this.get(i).getServiceEndTime() == null || this.get(i).getServiceEndTime().equals("null")) {
				pstmt.setDate(32,null);
			} else {
				pstmt.setDate(32, Date.valueOf(this.get(i).getServiceEndTime()));
			}
			if(this.get(i).getRBankVSMP() == null || this.get(i).getRBankVSMP().equals("null")) {
				pstmt.setString(33,null);
			} else {
				pstmt.setString(33, this.get(i).getRBankVSMP().trim());
			}
			if(this.get(i).getDesBankVSMP() == null || this.get(i).getDesBankVSMP().equals("null")) {
				pstmt.setString(34,null);
			} else {
				pstmt.setString(34, this.get(i).getDesBankVSMP().trim());
			}
			if(this.get(i).getRMPVSKernel() == null || this.get(i).getRMPVSKernel().equals("null")) {
				pstmt.setString(35,null);
			} else {
				pstmt.setString(35, this.get(i).getRMPVSKernel().trim());
			}
			if(this.get(i).getDesMPVSKernel() == null || this.get(i).getDesMPVSKernel().equals("null")) {
				pstmt.setString(36,null);
			} else {
				pstmt.setString(36, this.get(i).getDesMPVSKernel().trim());
			}
			if(this.get(i).getResultBalance() == null || this.get(i).getResultBalance().equals("null")) {
				pstmt.setString(37,null);
			} else {
				pstmt.setString(37, this.get(i).getResultBalance().trim());
			}
			if(this.get(i).getDesBalance() == null || this.get(i).getDesBalance().equals("null")) {
				pstmt.setString(38,null);
			} else {
				pstmt.setString(38, this.get(i).getDesBalance().trim());
			}
			if(this.get(i).getbak1() == null || this.get(i).getbak1().equals("null")) {
				pstmt.setString(39,null);
			} else {
				pstmt.setString(39, this.get(i).getbak1().trim());
			}
			if(this.get(i).getbak2() == null || this.get(i).getbak2().equals("null")) {
				pstmt.setString(40,null);
			} else {
				pstmt.setString(40, this.get(i).getbak2().trim());
			}
			if(this.get(i).getbak3() == null || this.get(i).getbak3().equals("null")) {
				pstmt.setString(41,null);
			} else {
				pstmt.setString(41, this.get(i).getbak3().trim());
			}
			if(this.get(i).getbak4() == null || this.get(i).getbak4().equals("null")) {
				pstmt.setString(42,null);
			} else {
				pstmt.setString(42, this.get(i).getbak4().trim());
			}
			if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
				pstmt.setDate(43,null);
			} else {
				pstmt.setDate(43, Date.valueOf(this.get(i).getMakeDate()));
			}
			if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
				pstmt.setString(44,null);
			} else {
				pstmt.setString(44, this.get(i).getMakeTime().trim());
			}
			if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
				pstmt.setDate(45,null);
			} else {
				pstmt.setDate(45, Date.valueOf(this.get(i).getModifyDate()));
			}
			if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
				pstmt.setString(46,null);
			} else {
				pstmt.setString(46, this.get(i).getModifyTime().trim());
			}
			// set where condition
                pstmt.addBatch();
            }
            pstmt.executeBatch();
			pstmt.close();
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LKTransStatusBackUpDBSet";
			tError.functionName = "update()";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

            int tCount = this.size();
            for (int i = 1; i <= tCount; i++)
            {
		// only for debug purpose
		SQLString sqlObj = new SQLString("LKTransStatusBackUp");
		sqlObj.setSQL(2, this.get(i));
		sqlObj.getSQL();
            }

			try {
				pstmt.close();
			} catch (Exception e){e.printStackTrace();}

			if( !mflag ) {
				try {
					con.close();
				} catch (Exception e){e.printStackTrace();}
			}

			return false;
		}

		if( !mflag ) {
			try {
				con.close();
			} catch (Exception e){e.printStackTrace();}
		}

		return true;
	}

    /**
     * 新增操作
     * @return boolean
     */
	public boolean insert()
	{
		PreparedStatement pstmt = null;

		if( !mflag ) {
			con = DBConnPool.getConnection();
		}

		try
		{
            int tCount = this.size();
			pstmt = con.prepareStatement("INSERT INTO LKTransStatusBackUp VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++)
            {
			if(this.get(i).getTransCode() == null || this.get(i).getTransCode().equals("null")) {
				pstmt.setString(1,null);
			} else {
				pstmt.setString(1, this.get(i).getTransCode().trim());
			}
			if(this.get(i).getReportNo() == null || this.get(i).getReportNo().equals("null")) {
				pstmt.setString(2,null);
			} else {
				pstmt.setString(2, this.get(i).getReportNo().trim());
			}
			if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
				pstmt.setString(3,null);
			} else {
				pstmt.setString(3, this.get(i).getBankCode().trim());
			}
			if(this.get(i).getBankBranch() == null || this.get(i).getBankBranch().equals("null")) {
				pstmt.setString(4,null);
			} else {
				pstmt.setString(4, this.get(i).getBankBranch().trim());
			}
			if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
				pstmt.setString(5,null);
			} else {
				pstmt.setString(5, this.get(i).getBankNode().trim());
			}
			if(this.get(i).getBankOperator() == null || this.get(i).getBankOperator().equals("null")) {
				pstmt.setString(6,null);
			} else {
				pstmt.setString(6, this.get(i).getBankOperator().trim());
			}
			if(this.get(i).getTransNo() == null || this.get(i).getTransNo().equals("null")) {
				pstmt.setString(7,null);
			} else {
				pstmt.setString(7, this.get(i).getTransNo().trim());
			}
			if(this.get(i).getFuncFlag() == null || this.get(i).getFuncFlag().equals("null")) {
				pstmt.setString(8,null);
			} else {
				pstmt.setString(8, this.get(i).getFuncFlag().trim());
			}
			if(this.get(i).getTransDate() == null || this.get(i).getTransDate().equals("null")) {
				pstmt.setDate(9,null);
			} else {
				pstmt.setDate(9, Date.valueOf(this.get(i).getTransDate()));
			}
			if(this.get(i).getTransTime() == null || this.get(i).getTransTime().equals("null")) {
				pstmt.setString(10,null);
			} else {
				pstmt.setString(10, this.get(i).getTransTime().trim());
			}
			if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
				pstmt.setString(11,null);
			} else {
				pstmt.setString(11, this.get(i).getManageCom().trim());
			}
			if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
				pstmt.setString(12,null);
			} else {
				pstmt.setString(12, this.get(i).getRiskCode().trim());
			}
			if(this.get(i).getProposalNo() == null || this.get(i).getProposalNo().equals("null")) {
				pstmt.setString(13,null);
			} else {
				pstmt.setString(13, this.get(i).getProposalNo().trim());
			}
			if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
				pstmt.setString(14,null);
			} else {
				pstmt.setString(14, this.get(i).getPrtNo().trim());
			}
			if(this.get(i).getPolNo() == null || this.get(i).getPolNo().equals("null")) {
				pstmt.setString(15,null);
			} else {
				pstmt.setString(15, this.get(i).getPolNo().trim());
			}
			if(this.get(i).getEdorNo() == null || this.get(i).getEdorNo().equals("null")) {
				pstmt.setString(16,null);
			} else {
				pstmt.setString(16, this.get(i).getEdorNo().trim());
			}
			if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("null")) {
				pstmt.setString(17,null);
			} else {
				pstmt.setString(17, this.get(i).getTempFeeNo().trim());
			}
			pstmt.setDouble(18, this.get(i).getTransAmnt());
			if(this.get(i).getBankAcc() == null || this.get(i).getBankAcc().equals("null")) {
				pstmt.setString(19,null);
			} else {
				pstmt.setString(19, this.get(i).getBankAcc().trim());
			}
			if(this.get(i).getRCode() == null || this.get(i).getRCode().equals("null")) {
				pstmt.setString(20,null);
			} else {
				pstmt.setString(20, this.get(i).getRCode().trim());
			}
			if(this.get(i).getTransStatus() == null || this.get(i).getTransStatus().equals("null")) {
				pstmt.setString(21,null);
			} else {
				pstmt.setString(21, this.get(i).getTransStatus().trim());
			}
			if(this.get(i).getStatus() == null || this.get(i).getStatus().equals("null")) {
				pstmt.setString(22,null);
			} else {
				pstmt.setString(22, this.get(i).getStatus().trim());
			}
			if(this.get(i).getDescr() == null || this.get(i).getDescr().equals("null")) {
				pstmt.setString(23,null);
			} else {
				pstmt.setString(23, this.get(i).getDescr().trim());
			}
			if(this.get(i).getTemp() == null || this.get(i).getTemp().equals("null")) {
				pstmt.setString(24,null);
			} else {
				pstmt.setString(24, this.get(i).getTemp().trim());
			}
			if(this.get(i).getState_Code() == null || this.get(i).getState_Code().equals("null")) {
				pstmt.setString(25,null);
			} else {
				pstmt.setString(25, this.get(i).getState_Code().trim());
			}
			if(this.get(i).getRequestId() == null || this.get(i).getRequestId().equals("null")) {
				pstmt.setString(26,null);
			} else {
				pstmt.setString(26, this.get(i).getRequestId().trim());
			}
			if(this.get(i).getOutServiceCode() == null || this.get(i).getOutServiceCode().equals("null")) {
				pstmt.setString(27,null);
			} else {
				pstmt.setString(27, this.get(i).getOutServiceCode().trim());
			}
			if(this.get(i).getClientIP() == null || this.get(i).getClientIP().equals("null")) {
				pstmt.setString(28,null);
			} else {
				pstmt.setString(28, this.get(i).getClientIP().trim());
			}
			if(this.get(i).getClientPort() == null || this.get(i).getClientPort().equals("null")) {
				pstmt.setString(29,null);
			} else {
				pstmt.setString(29, this.get(i).getClientPort().trim());
			}
			if(this.get(i).getIssueWay() == null || this.get(i).getIssueWay().equals("null")) {
				pstmt.setString(30,null);
			} else {
				pstmt.setString(30, this.get(i).getIssueWay().trim());
			}
			if(this.get(i).getServiceStartTime() == null || this.get(i).getServiceStartTime().equals("null")) {
				pstmt.setDate(31,null);
			} else {
				pstmt.setDate(31, Date.valueOf(this.get(i).getServiceStartTime()));
			}
			if(this.get(i).getServiceEndTime() == null || this.get(i).getServiceEndTime().equals("null")) {
				pstmt.setDate(32,null);
			} else {
				pstmt.setDate(32, Date.valueOf(this.get(i).getServiceEndTime()));
			}
			if(this.get(i).getRBankVSMP() == null || this.get(i).getRBankVSMP().equals("null")) {
				pstmt.setString(33,null);
			} else {
				pstmt.setString(33, this.get(i).getRBankVSMP().trim());
			}
			if(this.get(i).getDesBankVSMP() == null || this.get(i).getDesBankVSMP().equals("null")) {
				pstmt.setString(34,null);
			} else {
				pstmt.setString(34, this.get(i).getDesBankVSMP().trim());
			}
			if(this.get(i).getRMPVSKernel() == null || this.get(i).getRMPVSKernel().equals("null")) {
				pstmt.setString(35,null);
			} else {
				pstmt.setString(35, this.get(i).getRMPVSKernel().trim());
			}
			if(this.get(i).getDesMPVSKernel() == null || this.get(i).getDesMPVSKernel().equals("null")) {
				pstmt.setString(36,null);
			} else {
				pstmt.setString(36, this.get(i).getDesMPVSKernel().trim());
			}
			if(this.get(i).getResultBalance() == null || this.get(i).getResultBalance().equals("null")) {
				pstmt.setString(37,null);
			} else {
				pstmt.setString(37, this.get(i).getResultBalance().trim());
			}
			if(this.get(i).getDesBalance() == null || this.get(i).getDesBalance().equals("null")) {
				pstmt.setString(38,null);
			} else {
				pstmt.setString(38, this.get(i).getDesBalance().trim());
			}
			if(this.get(i).getbak1() == null || this.get(i).getbak1().equals("null")) {
				pstmt.setString(39,null);
			} else {
				pstmt.setString(39, this.get(i).getbak1().trim());
			}
			if(this.get(i).getbak2() == null || this.get(i).getbak2().equals("null")) {
				pstmt.setString(40,null);
			} else {
				pstmt.setString(40, this.get(i).getbak2().trim());
			}
			if(this.get(i).getbak3() == null || this.get(i).getbak3().equals("null")) {
				pstmt.setString(41,null);
			} else {
				pstmt.setString(41, this.get(i).getbak3().trim());
			}
			if(this.get(i).getbak4() == null || this.get(i).getbak4().equals("null")) {
				pstmt.setString(42,null);
			} else {
				pstmt.setString(42, this.get(i).getbak4().trim());
			}
			if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
				pstmt.setDate(43,null);
			} else {
				pstmt.setDate(43, Date.valueOf(this.get(i).getMakeDate()));
			}
			if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
				pstmt.setString(44,null);
			} else {
				pstmt.setString(44, this.get(i).getMakeTime().trim());
			}
			if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
				pstmt.setDate(45,null);
			} else {
				pstmt.setDate(45, Date.valueOf(this.get(i).getModifyDate()));
			}
			if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
				pstmt.setString(46,null);
			} else {
				pstmt.setString(46, this.get(i).getModifyTime().trim());
			}
                pstmt.addBatch();
            }
            pstmt.executeBatch();
			pstmt.close();
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			this.mErrors.copyAllErrors(db.mErrors);
			CError tError = new CError();
			tError.moduleName = "LKTransStatusBackUpDBSet";
			tError.functionName = "insert()";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

            int tCount = this.size();
            for (int i = 1; i <= tCount; i++)
            {
		// only for debug purpose
		SQLString sqlObj = new SQLString("LKTransStatusBackUp");
		sqlObj.setSQL(1, this.get(i));
		sqlObj.getSQL();
            }

			try {
				pstmt.close();
			} catch (Exception e){e.printStackTrace();}

			if( !mflag ) {
				try {
					con.close();
				} catch (Exception e){e.printStackTrace();}
			}

			return false;
		}

		if( !mflag ) {
			try {
				con.close();
			} catch (Exception e){e.printStackTrace();}
		}

		return true;
	}

}
