/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LDRiskPopedomSchema;
import com.sinosoft.lis.vschema.LDRiskPopedomSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDRiskPopedomDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-05
 */
public class LDRiskPopedomDBSet extends LDRiskPopedomSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;


    // @Constructor
    public LDRiskPopedomDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LDRiskPopedom");
        mflag = true;
    }

    public LDRiskPopedomDBSet() {
        db = new DBOper("LDRiskPopedom");
        // con = db.getConnection();
    }

    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDRiskPopedomDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement(
                    "DELETE FROM LDRiskPopedom WHERE  RiskCode = ? AND PopedomType = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.GBKToUnicode(this.get(i).getRiskCode()));
                }
                if (this.get(i).getPopedomType() == null ||
                    this.get(i).getPopedomType().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).getPopedomType()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDRiskPopedomDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LDRiskPopedom SET  RiskCode = ? , PopedomType = ? , MaxValue = ? , MinValue = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , ManageCom = ? WHERE  RiskCode = ? AND PopedomType = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.GBKToUnicode(this.get(i).getRiskCode()));
                }
                if (this.get(i).getPopedomType() == null ||
                    this.get(i).getPopedomType().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).getPopedomType()));
                }
                if (this.get(i).getMaxValue() == null ||
                    this.get(i).getMaxValue().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).getMaxValue()));
                }
                if (this.get(i).getMinValue() == null ||
                    this.get(i).getMinValue().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.GBKToUnicode(this.get(i).getMinValue()));
                }
                if (this.get(i).getOperator() == null ||
                    this.get(i).getOperator().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.GBKToUnicode(this.get(i).getOperator()));
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(6, null);
                } else {
                    pstmt.setDate(6, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(7, null);
                } else {
                    pstmt.setString(7,
                                    StrTool.GBKToUnicode(this.get(i).getMakeTime()));
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(8, null);
                } else {
                    pstmt.setDate(8, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(9, null);
                } else {
                    pstmt.setString(9,
                                    StrTool.GBKToUnicode(this.get(i).getModifyTime()));
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(10, null);
                } else {
                    pstmt.setString(10,
                                    StrTool.GBKToUnicode(this.get(i).getManageCom()));
                }
                // set where condition
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(11, null);
                } else {
                    pstmt.setString(11,
                                    StrTool.GBKToUnicode(this.get(i).getRiskCode()));
                }
                if (this.get(i).getPopedomType() == null ||
                    this.get(i).getPopedomType().equals("null")) {
                    pstmt.setString(12, null);
                } else {
                    pstmt.setString(12,
                                    StrTool.GBKToUnicode(this.get(i).getPopedomType()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDRiskPopedomDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement(
                    "INSERT INTO LDRiskPopedom VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.GBKToUnicode(this.get(i).getRiskCode()));
                }
                if (this.get(i).getPopedomType() == null ||
                    this.get(i).getPopedomType().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).getPopedomType()));
                }
                if (this.get(i).getMaxValue() == null ||
                    this.get(i).getMaxValue().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).getMaxValue()));
                }
                if (this.get(i).getMinValue() == null ||
                    this.get(i).getMinValue().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.GBKToUnicode(this.get(i).getMinValue()));
                }
                if (this.get(i).getOperator() == null ||
                    this.get(i).getOperator().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.GBKToUnicode(this.get(i).getOperator()));
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(6, null);
                } else {
                    pstmt.setDate(6, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(7, null);
                } else {
                    pstmt.setString(7,
                                    StrTool.GBKToUnicode(this.get(i).getMakeTime()));
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(8, null);
                } else {
                    pstmt.setDate(8, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(9, null);
                } else {
                    pstmt.setString(9,
                                    StrTool.GBKToUnicode(this.get(i).getModifyTime()));
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(10, null);
                } else {
                    pstmt.setString(10,
                                    StrTool.GBKToUnicode(this.get(i).getManageCom()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDRiskPopedomDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

}
