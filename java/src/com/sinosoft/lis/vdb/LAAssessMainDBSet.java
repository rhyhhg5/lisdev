/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LAAssessMainSchema;
import com.sinosoft.lis.vschema.LAAssessMainSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAssessMainDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class LAAssessMainDBSet extends LAAssessMainSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;


    // @Constructor
    public LAAssessMainDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LAAssessMain");
        mflag = true;
    }

    public LAAssessMainDBSet() {
        db = new DBOper("LAAssessMain");
        // con = db.getConnection();
    }

    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LAAssessMain WHERE  IndexCalNo = ? AND AgentGrade = ? AND BranchType = ? AND ManageCom = ? AND AssessType = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getIndexCalNo() == null ||
                    this.get(i).getIndexCalNo().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1,
                                    StrTool.space(this.get(i).getIndexCalNo(), 8));
                }
                if (this.get(i).getAgentGrade() == null ||
                    this.get(i).getAgentGrade().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.space(this.get(i).getAgentGrade(), 3));
                }
                if (this.get(i).getBranchType() == null ||
                    this.get(i).getBranchType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.space(this.get(i).getBranchType(), 2));
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.space(this.get(i).getManageCom(), 10));
                }
                if (this.get(i).getAssessType() == null ||
                    this.get(i).getAssessType().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.space(this.get(i).getAssessType(), 2));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LAAssessMain SET  IndexCalNo = ? , AgentGrade = ? , BranchType = ? , ManageCom = ? , State = ? , AssessCount = ? , ConfirmCount = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BranchAttr = ? , AssessType = ? , BranchType2 = ? WHERE  IndexCalNo = ? AND AgentGrade = ? AND BranchType = ? AND ManageCom = ? AND AssessType = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getIndexCalNo() == null ||
                    this.get(i).getIndexCalNo().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1, this.get(i).getIndexCalNo());
                }
                if (this.get(i).getAgentGrade() == null ||
                    this.get(i).getAgentGrade().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2, this.get(i).getAgentGrade());
                }
                if (this.get(i).getBranchType() == null ||
                    this.get(i).getBranchType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3, this.get(i).getBranchType());
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4, this.get(i).getManageCom());
                }
                if (this.get(i).getState() == null ||
                    this.get(i).getState().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5, this.get(i).getState());
                }
                pstmt.setInt(6, this.get(i).getAssessCount());
                pstmt.setInt(7, this.get(i).getConfirmCount());
                if (this.get(i).getOperator() == null ||
                    this.get(i).getOperator().equals("null")) {
                    pstmt.setString(8, null);
                } else {
                    pstmt.setString(8, this.get(i).getOperator());
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(9, null);
                } else {
                    pstmt.setDate(9, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(10, null);
                } else {
                    pstmt.setString(10, this.get(i).getMakeTime());
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(11, null);
                } else {
                    pstmt.setDate(11, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(12, null);
                } else {
                    pstmt.setString(12, this.get(i).getModifyTime());
                }
                if (this.get(i).getBranchAttr() == null ||
                    this.get(i).getBranchAttr().equals("null")) {
                    pstmt.setString(13, null);
                } else {
                    pstmt.setString(13, this.get(i).getBranchAttr());
                }
                if (this.get(i).getAssessType() == null ||
                    this.get(i).getAssessType().equals("null")) {
                    pstmt.setString(14, null);
                } else {
                    pstmt.setString(14, this.get(i).getAssessType());
                }
                if (this.get(i).getBranchType2() == null ||
                    this.get(i).getBranchType2().equals("null")) {
                    pstmt.setString(15, null);
                } else {
                    pstmt.setString(15, this.get(i).getBranchType2());
                }
                // set where condition
                if (this.get(i).getIndexCalNo() == null ||
                    this.get(i).getIndexCalNo().equals("null")) {
                    pstmt.setString(16, null);
                } else {
                    pstmt.setString(16,
                                    StrTool.space(this.get(i).getIndexCalNo(), 8));
                }
                if (this.get(i).getAgentGrade() == null ||
                    this.get(i).getAgentGrade().equals("null")) {
                    pstmt.setString(17, null);
                } else {
                    pstmt.setString(17,
                                    StrTool.space(this.get(i).getAgentGrade(), 3));
                }
                if (this.get(i).getBranchType() == null ||
                    this.get(i).getBranchType().equals("null")) {
                    pstmt.setString(18, null);
                } else {
                    pstmt.setString(18,
                                    StrTool.space(this.get(i).getBranchType(), 2));
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(19, null);
                } else {
                    pstmt.setString(19,
                                    StrTool.space(this.get(i).getManageCom(), 10));
                }
                if (this.get(i).getAssessType() == null ||
                    this.get(i).getAssessType().equals("null")) {
                    pstmt.setString(20, null);
                } else {
                    pstmt.setString(20,
                                    StrTool.space(this.get(i).getAssessType(), 2));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LAAssessMain VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getIndexCalNo() == null ||
                    this.get(i).getIndexCalNo().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1, this.get(i).getIndexCalNo());
                }
                if (this.get(i).getAgentGrade() == null ||
                    this.get(i).getAgentGrade().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2, this.get(i).getAgentGrade());
                }
                if (this.get(i).getBranchType() == null ||
                    this.get(i).getBranchType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3, this.get(i).getBranchType());
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4, this.get(i).getManageCom());
                }
                if (this.get(i).getState() == null ||
                    this.get(i).getState().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5, this.get(i).getState());
                }
                pstmt.setInt(6, this.get(i).getAssessCount());
                pstmt.setInt(7, this.get(i).getConfirmCount());
                if (this.get(i).getOperator() == null ||
                    this.get(i).getOperator().equals("null")) {
                    pstmt.setString(8, null);
                } else {
                    pstmt.setString(8, this.get(i).getOperator());
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(9, null);
                } else {
                    pstmt.setDate(9, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(10, null);
                } else {
                    pstmt.setString(10, this.get(i).getMakeTime());
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(11, null);
                } else {
                    pstmt.setDate(11, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(12, null);
                } else {
                    pstmt.setString(12, this.get(i).getModifyTime());
                }
                if (this.get(i).getBranchAttr() == null ||
                    this.get(i).getBranchAttr().equals("null")) {
                    pstmt.setString(13, null);
                } else {
                    pstmt.setString(13, this.get(i).getBranchAttr());
                }
                if (this.get(i).getAssessType() == null ||
                    this.get(i).getAssessType().equals("null")) {
                    pstmt.setString(14, null);
                } else {
                    pstmt.setString(14, this.get(i).getAssessType());
                }
                if (this.get(i).getBranchType2() == null ||
                    this.get(i).getBranchType2().equals("null")) {
                    pstmt.setString(15, null);
                } else {
                    pstmt.setString(15, this.get(i).getBranchType2());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAssessMainDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

}
