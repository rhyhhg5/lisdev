/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.Connection;

import com.sinosoft.lis.schema.LAWageRadix2Schema;
import com.sinosoft.lis.vschema.LAWageRadix2Set;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBOper;

/*
 * <p>ClassName: LAWageRadix2DBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAWageRadix2DBSet extends LAWageRadix2Set
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    // @Constructor
    public LAWageRadix2DBSet(Connection tConnection)
    {
        con = tConnection;
        db = new DBOper(con, "LAWageRadix2");
        mflag = true;
    }

    public LAWageRadix2DBSet()
    {
        db = new DBOper("LAWageRadix2");
        // con = db.getConnection();
    }

    // @Method
    public boolean insert()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAWageRadix2Schema aSchema = new LAWageRadix2Schema();
            aSchema.setSchema((LAWageRadix2Schema)this.get(i));
            if (!db.insert(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageRadix2DBSet";
                tError.functionName = "insert";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean update()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAWageRadix2Schema aSchema = new LAWageRadix2Schema();
            aSchema.setSchema((LAWageRadix2Schema)this.get(i));
            if (!db.update(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageRadix2DBSet";
                tError.functionName = "update";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean deleteSQL()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAWageRadix2Schema aSchema = new LAWageRadix2Schema();
            aSchema.setSchema((LAWageRadix2Schema)this.get(i));
            if (!db.deleteSQL(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageRadix2DBSet";
                tError.functionName = "deleteSQL";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean delete()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAWageRadix2Schema aSchema = new LAWageRadix2Schema();
            aSchema.setSchema((LAWageRadix2Schema)this.get(i));
            if (!db.delete(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageRadix2DBSet";
                tError.functionName = "delete";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

}
