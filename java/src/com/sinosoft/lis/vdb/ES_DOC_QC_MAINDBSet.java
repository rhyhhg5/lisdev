/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;

import com.sinosoft.lis.vschema.ES_DOC_QC_MAINSet;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_QC_MAINDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class ES_DOC_QC_MAINDBSet extends ES_DOC_QC_MAINSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;


    // @Constructor
    public ES_DOC_QC_MAINDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "ES_DOC_QC_MAIN");
        mflag = true;
    }

    public ES_DOC_QC_MAINDBSet() {
        db = new DBOper("ES_DOC_QC_MAIN");
        // con = db.getConnection();
    }

    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement(
                    "DELETE FROM ES_DOC_QC_MAIN WHERE  DocID = ?");
            for (int i = 1; i <= tCount; i++) {
                pstmt.setDouble(1, this.get(i).getDocID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE ES_DOC_QC_MAIN SET  DocID = ? , DocCode = ? , BussType = ? , SubType = ? , ScanOperator = ? , ScanManageCom = ? , ScanLastApplyDate = ? , ScanLastApplyTime = ? , QCOperator = ? , QCManageCom = ? , QCLastApplyDate = ? , QCLastApplyTime = ? , QCFlag = ? , QCSuggest = ? , UnpassReason = ? , ScanOpeState = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  DocID = ?");
            for (int i = 1; i <= tCount; i++) {
                pstmt.setDouble(1, this.get(i).getDocID());
                if (this.get(i).getDocCode() == null ||
                    this.get(i).getDocCode().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).getDocCode()));
                }
                if (this.get(i).getBussType() == null ||
                    this.get(i).getBussType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).
                            getBussType()));
                }
                if (this.get(i).getSubType() == null ||
                    this.get(i).getSubType().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.GBKToUnicode(this.get(i).getSubType()));
                }
                if (this.get(i).getScanOperator() == null ||
                    this.get(i).getScanOperator().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanOperator()));
                }
                if (this.get(i).getScanManageCom() == null ||
                    this.get(i).getScanManageCom().equals("null")) {
                    pstmt.setString(6, null);
                } else {
                    pstmt.setString(6,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanManageCom()));
                }
                if (this.get(i).getScanLastApplyDate() == null ||
                    this.get(i).getScanLastApplyDate().equals("null")) {
                    pstmt.setDate(7, null);
                } else {
                    pstmt.setDate(7,
                                  Date.valueOf(this.get(i).getScanLastApplyDate()));
                }
                if (this.get(i).getScanLastApplyTime() == null ||
                    this.get(i).getScanLastApplyTime().equals("null")) {
                    pstmt.setString(8, null);
                } else {
                    pstmt.setString(8,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanLastApplyTime()));
                }
                if (this.get(i).getQCOperator() == null ||
                    this.get(i).getQCOperator().equals("null")) {
                    pstmt.setString(9, null);
                } else {
                    pstmt.setString(9,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCOperator()));
                }
                if (this.get(i).getQCManageCom() == null ||
                    this.get(i).getQCManageCom().equals("null")) {
                    pstmt.setString(10, null);
                } else {
                    pstmt.setString(10,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCManageCom()));
                }
                if (this.get(i).getQCLastApplyDate() == null ||
                    this.get(i).getQCLastApplyDate().equals("null")) {
                    pstmt.setDate(11, null);
                } else {
                    pstmt.setDate(11,
                                  Date.valueOf(this.get(i).getQCLastApplyDate()));
                }
                if (this.get(i).getQCLastApplyTime() == null ||
                    this.get(i).getQCLastApplyTime().equals("null")) {
                    pstmt.setString(12, null);
                } else {
                    pstmt.setString(12,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCLastApplyTime()));
                }
                if (this.get(i).getQCFlag() == null ||
                    this.get(i).getQCFlag().equals("null")) {
                    pstmt.setString(13, null);
                } else {
                    pstmt.setString(13,
                                    StrTool.GBKToUnicode(this.get(i).getQCFlag()));
                }
                if (this.get(i).getQCSuggest() == null ||
                    this.get(i).getQCSuggest().equals("null")) {
                    pstmt.setString(14, null);
                } else {
                    pstmt.setString(14,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCSuggest()));
                }
                if (this.get(i).getUnpassReason() == null ||
                    this.get(i).getUnpassReason().equals("null")) {
                    pstmt.setString(15, null);
                } else {
                    pstmt.setString(15,
                                    StrTool.GBKToUnicode(this.get(i).
                            getUnpassReason()));
                }
                if (this.get(i).getScanOpeState() == null ||
                    this.get(i).getScanOpeState().equals("null")) {
                    pstmt.setString(16, null);
                } else {
                    pstmt.setString(16,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanOpeState()));
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(17, null);
                } else {
                    pstmt.setDate(17, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(18, null);
                } else {
                    pstmt.setString(18,
                                    StrTool.GBKToUnicode(this.get(i).
                            getMakeTime()));
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(19, null);
                } else {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(20, null);
                } else {
                    pstmt.setString(20,
                                    StrTool.GBKToUnicode(this.get(i).
                            getModifyTime()));
                }
                // set where condition
                pstmt.setDouble(21, this.get(i).getDocID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO ES_DOC_QC_MAIN VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                pstmt.setDouble(1, this.get(i).getDocID());
                if (this.get(i).getDocCode() == null ||
                    this.get(i).getDocCode().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.GBKToUnicode(this.get(i).getDocCode()));
                }
                if (this.get(i).getBussType() == null ||
                    this.get(i).getBussType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.GBKToUnicode(this.get(i).
                            getBussType()));
                }
                if (this.get(i).getSubType() == null ||
                    this.get(i).getSubType().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4,
                                    StrTool.GBKToUnicode(this.get(i).getSubType()));
                }
                if (this.get(i).getScanOperator() == null ||
                    this.get(i).getScanOperator().equals("null")) {
                    pstmt.setString(5, null);
                } else {
                    pstmt.setString(5,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanOperator()));
                }
                if (this.get(i).getScanManageCom() == null ||
                    this.get(i).getScanManageCom().equals("null")) {
                    pstmt.setString(6, null);
                } else {
                    pstmt.setString(6,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanManageCom()));
                }
                if (this.get(i).getScanLastApplyDate() == null ||
                    this.get(i).getScanLastApplyDate().equals("null")) {
                    pstmt.setDate(7, null);
                } else {
                    pstmt.setDate(7,
                                  Date.valueOf(this.get(i).getScanLastApplyDate()));
                }
                if (this.get(i).getScanLastApplyTime() == null ||
                    this.get(i).getScanLastApplyTime().equals("null")) {
                    pstmt.setString(8, null);
                } else {
                    pstmt.setString(8,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanLastApplyTime()));
                }
                if (this.get(i).getQCOperator() == null ||
                    this.get(i).getQCOperator().equals("null")) {
                    pstmt.setString(9, null);
                } else {
                    pstmt.setString(9,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCOperator()));
                }
                if (this.get(i).getQCManageCom() == null ||
                    this.get(i).getQCManageCom().equals("null")) {
                    pstmt.setString(10, null);
                } else {
                    pstmt.setString(10,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCManageCom()));
                }
                if (this.get(i).getQCLastApplyDate() == null ||
                    this.get(i).getQCLastApplyDate().equals("null")) {
                    pstmt.setDate(11, null);
                } else {
                    pstmt.setDate(11,
                                  Date.valueOf(this.get(i).getQCLastApplyDate()));
                }
                if (this.get(i).getQCLastApplyTime() == null ||
                    this.get(i).getQCLastApplyTime().equals("null")) {
                    pstmt.setString(12, null);
                } else {
                    pstmt.setString(12,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCLastApplyTime()));
                }
                if (this.get(i).getQCFlag() == null ||
                    this.get(i).getQCFlag().equals("null")) {
                    pstmt.setString(13, null);
                } else {
                    pstmt.setString(13,
                                    StrTool.GBKToUnicode(this.get(i).getQCFlag()));
                }
                if (this.get(i).getQCSuggest() == null ||
                    this.get(i).getQCSuggest().equals("null")) {
                    pstmt.setString(14, null);
                } else {
                    pstmt.setString(14,
                                    StrTool.GBKToUnicode(this.get(i).
                            getQCSuggest()));
                }
                if (this.get(i).getUnpassReason() == null ||
                    this.get(i).getUnpassReason().equals("null")) {
                    pstmt.setString(15, null);
                } else {
                    pstmt.setString(15,
                                    StrTool.GBKToUnicode(this.get(i).
                            getUnpassReason()));
                }
                if (this.get(i).getScanOpeState() == null ||
                    this.get(i).getScanOpeState().equals("null")) {
                    pstmt.setString(16, null);
                } else {
                    pstmt.setString(16,
                                    StrTool.GBKToUnicode(this.get(i).
                            getScanOpeState()));
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(17, null);
                } else {
                    pstmt.setDate(17, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(18, null);
                } else {
                    pstmt.setString(18,
                                    StrTool.GBKToUnicode(this.get(i).
                            getMakeTime()));
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(19, null);
                } else {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(20, null);
                } else {
                    pstmt.setString(20,
                                    StrTool.GBKToUnicode(this.get(i).
                            getModifyTime()));
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_MAINDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

}
