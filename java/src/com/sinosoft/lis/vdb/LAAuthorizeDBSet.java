/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LAAuthorizeSchema;
import com.sinosoft.lis.vschema.LAAuthorizeSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAuthorizeDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-07
 */
public class LAAuthorizeDBSet extends LAAuthorizeSet
{
	// @Field
	private Connection con;
	private DBOper db;
	/**
	* flag = true: 传入Connection
	* flag = false: 不传入Connection
	**/
	private boolean mflag = false;

	public CErrors mErrors = new CErrors();			// 错误信息

	// @Constructor
	public LAAuthorizeDBSet(Connection tConnection)
	{
		con = tConnection;
		db = new DBOper(con,"LAAuthorize");
		mflag = true;
	}

	public LAAuthorizeDBSet()
	{
		db = new DBOper( "LAAuthorize" );
		// con = db.getConnection();
	}
	// @Method
	public boolean insert()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LAAuthorizeSchema aSchema = new LAAuthorizeSchema();
			aSchema.setSchema( (LAAuthorizeSchema)this.get(i) );
			if (!db.insert(aSchema))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "LAAuthorizeDBSet";
				tError.functionName = "insert";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

	public boolean update()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LAAuthorizeSchema aSchema = new LAAuthorizeSchema();
			aSchema.setSchema( (LAAuthorizeSchema)this.get(i) );
			if (!db.update(aSchema))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "LAAuthorizeDBSet";
				tError.functionName = "update";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

	public boolean deleteSQL()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LAAuthorizeSchema aSchema = new LAAuthorizeSchema();
			aSchema.setSchema( (LAAuthorizeSchema)this.get(i) );
			if (!db.deleteSQL(aSchema))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "LAAuthorizeDBSet";
				tError.functionName = "deleteSQL";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

	public boolean delete()
	{
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LAAuthorizeSchema aSchema = new LAAuthorizeSchema();
			aSchema.setSchema( (LAAuthorizeSchema)this.get(i) );
			if (!db.delete(aSchema))
			{
				// @@错误处理
				this.mErrors.copyAllErrors(db.mErrors);
				CError tError = new CError();
				tError.moduleName = "LAAuthorizeDBSet";
				tError.functionName = "delete";
				tError.errorMessage = "操作失败!";
				this.mErrors .addOneError(tError);

				return false;
			}
		}
		return true;
	}

}
