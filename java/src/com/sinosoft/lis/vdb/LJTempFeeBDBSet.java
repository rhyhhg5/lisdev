/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LJTempFeeBSchema;
import com.sinosoft.lis.vschema.LJTempFeeBSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LJTempFeeBDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class LJTempFeeBDBSet extends LJTempFeeBSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;


    // @Constructor
    public LJTempFeeBDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LJTempFeeB");
        mflag = true;
    }

    public LJTempFeeBDBSet() {
        db = new DBOper("LJTempFeeB");
        // con = db.getConnection();
    }

    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJTempFeeBDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LJTempFeeB WHERE  SeqNo = ? AND TempFeeNo = ? AND TempFeeType = ? AND RiskCode = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getSeqNo() == null ||
                    this.get(i).getSeqNo().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1, StrTool.space(this.get(i).getSeqNo(), 20));
                }
                if (this.get(i).getTempFeeNo() == null ||
                    this.get(i).getTempFeeNo().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2,
                                    StrTool.space(this.get(i).getTempFeeNo(), 20));
                }
                if (this.get(i).getTempFeeType() == null ||
                    this.get(i).getTempFeeType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3,
                                    StrTool.space(this.get(i).getTempFeeType(), 1));
                }
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4, this.get(i).getRiskCode());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJTempFeeBDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LJTempFeeB SET  SeqNo = ? , TempFeeNo = ? , TempFeeType = ? , RiskCode = ? , PayIntv = ? , OtherNo = ? , OtherNoType = ? , PayMoney = ? , PayDate = ? , EnterAccDate = ? , ConfDate = ? , ConfMakeDate = ? , ConfMakeTime = ? , SaleChnl = ? , ManageCom = ? , PolicyCom = ? , AgentCom = ? , AgentType = ? , APPntName = ? , AgentGroup = ? , AgentCode = ? , ConfFlag = ? , SerialNo = ? , Operator = ? , State = ? , MakeTime = ? , MakeDate = ? , ModifyDate = ? , ModifyTime = ? WHERE  SeqNo = ? AND TempFeeNo = ? AND TempFeeType = ? AND RiskCode = ?");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getSeqNo() == null ||
                    this.get(i).getSeqNo().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1, this.get(i).getSeqNo());
                }
                if (this.get(i).getTempFeeNo() == null ||
                    this.get(i).getTempFeeNo().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2, this.get(i).getTempFeeNo());
                }
                if (this.get(i).getTempFeeType() == null ||
                    this.get(i).getTempFeeType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3, this.get(i).getTempFeeType());
                }
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4, this.get(i).getRiskCode());
                }
                pstmt.setInt(5, this.get(i).getPayIntv());
                if (this.get(i).getOtherNo() == null ||
                    this.get(i).getOtherNo().equals("null")) {
                    pstmt.setString(6, null);
                } else {
                    pstmt.setString(6, this.get(i).getOtherNo());
                }
                if (this.get(i).getOtherNoType() == null ||
                    this.get(i).getOtherNoType().equals("null")) {
                    pstmt.setString(7, null);
                } else {
                    pstmt.setString(7, this.get(i).getOtherNoType());
                }
                pstmt.setDouble(8, this.get(i).getPayMoney());
                if (this.get(i).getPayDate() == null ||
                    this.get(i).getPayDate().equals("null")) {
                    pstmt.setDate(9, null);
                } else {
                    pstmt.setDate(9, Date.valueOf(this.get(i).getPayDate()));
                }
                if (this.get(i).getEnterAccDate() == null ||
                    this.get(i).getEnterAccDate().equals("null")) {
                    pstmt.setDate(10, null);
                } else {
                    pstmt.setDate(10, Date.valueOf(this.get(i).getEnterAccDate()));
                }
                if (this.get(i).getConfDate() == null ||
                    this.get(i).getConfDate().equals("null")) {
                    pstmt.setDate(11, null);
                } else {
                    pstmt.setDate(11, Date.valueOf(this.get(i).getConfDate()));
                }
                if (this.get(i).getConfMakeDate() == null ||
                    this.get(i).getConfMakeDate().equals("null")) {
                    pstmt.setDate(12, null);
                } else {
                    pstmt.setDate(12, Date.valueOf(this.get(i).getConfMakeDate()));
                }
                if (this.get(i).getConfMakeTime() == null ||
                    this.get(i).getConfMakeTime().equals("null")) {
                    pstmt.setString(13, null);
                } else {
                    pstmt.setString(13, this.get(i).getConfMakeTime());
                }
                if (this.get(i).getSaleChnl() == null ||
                    this.get(i).getSaleChnl().equals("null")) {
                    pstmt.setString(14, null);
                } else {
                    pstmt.setString(14, this.get(i).getSaleChnl());
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(15, null);
                } else {
                    pstmt.setString(15, this.get(i).getManageCom());
                }
                if (this.get(i).getPolicyCom() == null ||
                    this.get(i).getPolicyCom().equals("null")) {
                    pstmt.setString(16, null);
                } else {
                    pstmt.setString(16, this.get(i).getPolicyCom());
                }
                if (this.get(i).getAgentCom() == null ||
                    this.get(i).getAgentCom().equals("null")) {
                    pstmt.setString(17, null);
                } else {
                    pstmt.setString(17, this.get(i).getAgentCom());
                }
                if (this.get(i).getAgentType() == null ||
                    this.get(i).getAgentType().equals("null")) {
                    pstmt.setString(18, null);
                } else {
                    pstmt.setString(18, this.get(i).getAgentType());
                }
                if (this.get(i).getAPPntName() == null ||
                    this.get(i).getAPPntName().equals("null")) {
                    pstmt.setString(19, null);
                } else {
                    pstmt.setString(19, this.get(i).getAPPntName());
                }
                if (this.get(i).getAgentGroup() == null ||
                    this.get(i).getAgentGroup().equals("null")) {
                    pstmt.setString(20, null);
                } else {
                    pstmt.setString(20, this.get(i).getAgentGroup());
                }
                if (this.get(i).getAgentCode() == null ||
                    this.get(i).getAgentCode().equals("null")) {
                    pstmt.setString(21, null);
                } else {
                    pstmt.setString(21, this.get(i).getAgentCode());
                }
                if (this.get(i).getConfFlag() == null ||
                    this.get(i).getConfFlag().equals("null")) {
                    pstmt.setString(22, null);
                } else {
                    pstmt.setString(22, this.get(i).getConfFlag());
                }
                if (this.get(i).getSerialNo() == null ||
                    this.get(i).getSerialNo().equals("null")) {
                    pstmt.setString(23, null);
                } else {
                    pstmt.setString(23, this.get(i).getSerialNo());
                }
                if (this.get(i).getOperator() == null ||
                    this.get(i).getOperator().equals("null")) {
                    pstmt.setString(24, null);
                } else {
                    pstmt.setString(24, this.get(i).getOperator());
                }
                if (this.get(i).getState() == null ||
                    this.get(i).getState().equals("null")) {
                    pstmt.setString(25, null);
                } else {
                    pstmt.setString(25, this.get(i).getState());
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(26, null);
                } else {
                    pstmt.setString(26, this.get(i).getMakeTime());
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(27, null);
                } else {
                    pstmt.setDate(27, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(28, null);
                } else {
                    pstmt.setDate(28, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(29, null);
                } else {
                    pstmt.setString(29, this.get(i).getModifyTime());
                }
                // set where condition
                if (this.get(i).getSeqNo() == null ||
                    this.get(i).getSeqNo().equals("null")) {
                    pstmt.setString(30, null);
                } else {
                    pstmt.setString(30,
                                    StrTool.space(this.get(i).getSeqNo(), 20));
                }
                if (this.get(i).getTempFeeNo() == null ||
                    this.get(i).getTempFeeNo().equals("null")) {
                    pstmt.setString(31, null);
                } else {
                    pstmt.setString(31,
                                    StrTool.space(this.get(i).getTempFeeNo(), 20));
                }
                if (this.get(i).getTempFeeType() == null ||
                    this.get(i).getTempFeeType().equals("null")) {
                    pstmt.setString(32, null);
                } else {
                    pstmt.setString(32,
                                    StrTool.space(this.get(i).getTempFeeType(),
                                                  1));
                }
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(33, null);
                } else {
                    pstmt.setString(33, this.get(i).getRiskCode());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJTempFeeBDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LJTempFeeB VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if (this.get(i).getSeqNo() == null ||
                    this.get(i).getSeqNo().equals("null")) {
                    pstmt.setString(1, null);
                } else {
                    pstmt.setString(1, this.get(i).getSeqNo());
                }
                if (this.get(i).getTempFeeNo() == null ||
                    this.get(i).getTempFeeNo().equals("null")) {
                    pstmt.setString(2, null);
                } else {
                    pstmt.setString(2, this.get(i).getTempFeeNo());
                }
                if (this.get(i).getTempFeeType() == null ||
                    this.get(i).getTempFeeType().equals("null")) {
                    pstmt.setString(3, null);
                } else {
                    pstmt.setString(3, this.get(i).getTempFeeType());
                }
                if (this.get(i).getRiskCode() == null ||
                    this.get(i).getRiskCode().equals("null")) {
                    pstmt.setString(4, null);
                } else {
                    pstmt.setString(4, this.get(i).getRiskCode());
                }
                pstmt.setInt(5, this.get(i).getPayIntv());
                if (this.get(i).getOtherNo() == null ||
                    this.get(i).getOtherNo().equals("null")) {
                    pstmt.setString(6, null);
                } else {
                    pstmt.setString(6, this.get(i).getOtherNo());
                }
                if (this.get(i).getOtherNoType() == null ||
                    this.get(i).getOtherNoType().equals("null")) {
                    pstmt.setString(7, null);
                } else {
                    pstmt.setString(7, this.get(i).getOtherNoType());
                }
                pstmt.setDouble(8, this.get(i).getPayMoney());
                if (this.get(i).getPayDate() == null ||
                    this.get(i).getPayDate().equals("null")) {
                    pstmt.setDate(9, null);
                } else {
                    pstmt.setDate(9, Date.valueOf(this.get(i).getPayDate()));
                }
                if (this.get(i).getEnterAccDate() == null ||
                    this.get(i).getEnterAccDate().equals("null")) {
                    pstmt.setDate(10, null);
                } else {
                    pstmt.setDate(10, Date.valueOf(this.get(i).getEnterAccDate()));
                }
                if (this.get(i).getConfDate() == null ||
                    this.get(i).getConfDate().equals("null")) {
                    pstmt.setDate(11, null);
                } else {
                    pstmt.setDate(11, Date.valueOf(this.get(i).getConfDate()));
                }
                if (this.get(i).getConfMakeDate() == null ||
                    this.get(i).getConfMakeDate().equals("null")) {
                    pstmt.setDate(12, null);
                } else {
                    pstmt.setDate(12, Date.valueOf(this.get(i).getConfMakeDate()));
                }
                if (this.get(i).getConfMakeTime() == null ||
                    this.get(i).getConfMakeTime().equals("null")) {
                    pstmt.setString(13, null);
                } else {
                    pstmt.setString(13, this.get(i).getConfMakeTime());
                }
                if (this.get(i).getSaleChnl() == null ||
                    this.get(i).getSaleChnl().equals("null")) {
                    pstmt.setString(14, null);
                } else {
                    pstmt.setString(14, this.get(i).getSaleChnl());
                }
                if (this.get(i).getManageCom() == null ||
                    this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(15, null);
                } else {
                    pstmt.setString(15, this.get(i).getManageCom());
                }
                if (this.get(i).getPolicyCom() == null ||
                    this.get(i).getPolicyCom().equals("null")) {
                    pstmt.setString(16, null);
                } else {
                    pstmt.setString(16, this.get(i).getPolicyCom());
                }
                if (this.get(i).getAgentCom() == null ||
                    this.get(i).getAgentCom().equals("null")) {
                    pstmt.setString(17, null);
                } else {
                    pstmt.setString(17, this.get(i).getAgentCom());
                }
                if (this.get(i).getAgentType() == null ||
                    this.get(i).getAgentType().equals("null")) {
                    pstmt.setString(18, null);
                } else {
                    pstmt.setString(18, this.get(i).getAgentType());
                }
                if (this.get(i).getAPPntName() == null ||
                    this.get(i).getAPPntName().equals("null")) {
                    pstmt.setString(19, null);
                } else {
                    pstmt.setString(19, this.get(i).getAPPntName());
                }
                if (this.get(i).getAgentGroup() == null ||
                    this.get(i).getAgentGroup().equals("null")) {
                    pstmt.setString(20, null);
                } else {
                    pstmt.setString(20, this.get(i).getAgentGroup());
                }
                if (this.get(i).getAgentCode() == null ||
                    this.get(i).getAgentCode().equals("null")) {
                    pstmt.setString(21, null);
                } else {
                    pstmt.setString(21, this.get(i).getAgentCode());
                }
                if (this.get(i).getConfFlag() == null ||
                    this.get(i).getConfFlag().equals("null")) {
                    pstmt.setString(22, null);
                } else {
                    pstmt.setString(22, this.get(i).getConfFlag());
                }
                if (this.get(i).getSerialNo() == null ||
                    this.get(i).getSerialNo().equals("null")) {
                    pstmt.setString(23, null);
                } else {
                    pstmt.setString(23, this.get(i).getSerialNo());
                }
                if (this.get(i).getOperator() == null ||
                    this.get(i).getOperator().equals("null")) {
                    pstmt.setString(24, null);
                } else {
                    pstmt.setString(24, this.get(i).getOperator());
                }
                if (this.get(i).getState() == null ||
                    this.get(i).getState().equals("null")) {
                    pstmt.setString(25, null);
                } else {
                    pstmt.setString(25, this.get(i).getState());
                }
                if (this.get(i).getMakeTime() == null ||
                    this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(26, null);
                } else {
                    pstmt.setString(26, this.get(i).getMakeTime());
                }
                if (this.get(i).getMakeDate() == null ||
                    this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(27, null);
                } else {
                    pstmt.setDate(27, Date.valueOf(this.get(i).getMakeDate()));
                }
                if (this.get(i).getModifyDate() == null ||
                    this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(28, null);
                } else {
                    pstmt.setDate(28, Date.valueOf(this.get(i).getModifyDate()));
                }
                if (this.get(i).getModifyTime() == null ||
                    this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(29, null);
                } else {
                    pstmt.setString(29, this.get(i).getModifyTime());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJTempFeeBDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }

}
