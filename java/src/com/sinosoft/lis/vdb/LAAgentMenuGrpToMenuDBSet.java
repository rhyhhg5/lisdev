/*
 * <p>ClassName: LAAgentMenuGrpToMenuDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.vdb;

import java.sql.Connection;

import com.sinosoft.lis.schema.LAAgentMenuGrpToMenuSchema;
import com.sinosoft.lis.vschema.LAAgentMenuGrpToMenuSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBOper;

public class LAAgentMenuGrpToMenuDBSet extends LAAgentMenuGrpToMenuSet
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors(); // 错误信息

    // @Constructor
    public LAAgentMenuGrpToMenuDBSet(Connection tConnection)
    {
        con = tConnection;
        db = new DBOper(con, "LAAgentMenuGrpToMenu");
        mflag = true;
    }

    public LAAgentMenuGrpToMenuDBSet()
    {
        db = new DBOper("LAAgentMenuGrpToMenu");
        // con = db.getConnection();
    }

    // @Method
    public boolean insert()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAAgentMenuGrpToMenuSchema aSchema = new LAAgentMenuGrpToMenuSchema();
            aSchema.setSchema((LAAgentMenuGrpToMenuSchema)this.get(i));
            if (!db.insert(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentMenuGrpToMenuDBSet";
                tError.functionName = "insert";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean update()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAAgentMenuGrpToMenuSchema aSchema = new LAAgentMenuGrpToMenuSchema();
            aSchema.setSchema((LAAgentMenuGrpToMenuSchema)this.get(i));
            if (!db.update(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentMenuGrpToMenuDBSet";
                tError.functionName = "update";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean deleteSQL()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAAgentMenuGrpToMenuSchema aSchema = new LAAgentMenuGrpToMenuSchema();
            aSchema.setSchema((LAAgentMenuGrpToMenuSchema)this.get(i));
            if (!db.deleteSQL(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentMenuGrpToMenuDBSet";
                tError.functionName = "deleteSQL";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

    public boolean delete()
    {
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAAgentMenuGrpToMenuSchema aSchema = new LAAgentMenuGrpToMenuSchema();
            aSchema.setSchema((LAAgentMenuGrpToMenuSchema)this.get(i));
            if (!db.delete(aSchema))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentMenuGrpToMenuDBSet";
                tError.functionName = "delete";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        return true;
    }

}
