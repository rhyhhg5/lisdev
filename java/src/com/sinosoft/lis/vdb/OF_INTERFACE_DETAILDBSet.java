/**
 * Copyright (c) 2006 Sinosoft Co.,LTD.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.io.StringReader;
import java.sql.*;
import com.sinosoft.lis.schema.OF_INTERFACE_DETAILSchema;
import com.sinosoft.lis.vschema.OF_INTERFACE_DETAILSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: OF_INTERFACE_DETAILDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Company: Sinosoft Co.,LTD</p>
 * @Database: PhysicalDataModel_1
 * @author：Makerx
 * @CreateDate：2007-12-19
 */
public class OF_INTERFACE_DETAILDBSet extends OF_INTERFACE_DETAILSet
{
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     */
    private boolean mflag = false;

    // @Constructor
    public OF_INTERFACE_DETAILDBSet(Connection cConnection)
    {
        con = cConnection;
        db = new DBOper(con, "OF_INTERFACE_DETAIL");
        mflag = true;
    }

    public OF_INTERFACE_DETAILDBSet()
    {
        db = new DBOper("OF_INTERFACE_DETAIL");
    }

    // @Method
    public boolean deleteSQL()
    {
        if (db.deleteSQL(this))
        {
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert()
    {
        PreparedStatement pstmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            if (!mflag)
            {
                // 如果是内部创建的连接，需要设置Commit模式
                con.setAutoCommit(false);
            }
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO OF_INTERFACE_DETAIL VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++)
            {
                pstmt.setDouble(1, this.get(i).getROW_ID());
                if (this.get(i).getCURRENCY_CODE() == null || this.get(i).getCURRENCY_CODE().equals("null"))
                {
                    pstmt.setString(2, null);
                }
                else
                {
                    pstmt.setString(2, this.get(i).getCURRENCY_CODE());
                }
                if (this.get(i).getUSER_JE_CATEGORY_NAME() == null || this.get(i).getUSER_JE_CATEGORY_NAME().equals("null"))
                {
                    pstmt.setString(3, null);
                }
                else
                {
                    pstmt.setString(3, this.get(i).getUSER_JE_CATEGORY_NAME());
                }
                if (this.get(i).getSEGMENT1() == null || this.get(i).getSEGMENT1().equals("null"))
                {
                    pstmt.setString(4, null);
                }
                else
                {
                    pstmt.setString(4, this.get(i).getSEGMENT1());
                }
                if (this.get(i).getSEGMENT2() == null || this.get(i).getSEGMENT2().equals("null"))
                {
                    pstmt.setString(5, null);
                }
                else
                {
                    pstmt.setString(5, this.get(i).getSEGMENT2());
                }
                if (this.get(i).getSEGMENT3() == null || this.get(i).getSEGMENT3().equals("null"))
                {
                    pstmt.setString(6, null);
                }
                else
                {
                    pstmt.setString(6, this.get(i).getSEGMENT3());
                }
                if (this.get(i).getSEGMENT4() == null || this.get(i).getSEGMENT4().equals("null"))
                {
                    pstmt.setString(7, null);
                }
                else
                {
                    pstmt.setString(7, this.get(i).getSEGMENT4());
                }
                if (this.get(i).getSEGMENT5() == null || this.get(i).getSEGMENT5().equals("null"))
                {
                    pstmt.setString(8, null);
                }
                else
                {
                    pstmt.setString(8, this.get(i).getSEGMENT5());
                }
                if (this.get(i).getSEGMENT6() == null || this.get(i).getSEGMENT6().equals("null"))
                {
                    pstmt.setString(9, null);
                }
                else
                {
                    pstmt.setString(9, this.get(i).getSEGMENT6());
                }
                if (this.get(i).getSEGMENT7() == null || this.get(i).getSEGMENT7().equals("null"))
                {
                    pstmt.setString(10, null);
                }
                else
                {
                    pstmt.setString(10, this.get(i).getSEGMENT7());
                }
                if (this.get(i).getSEGMENT8() == null || this.get(i).getSEGMENT8().equals("null"))
                {
                    pstmt.setString(11, null);
                }
                else
                {
                    pstmt.setString(11, this.get(i).getSEGMENT8());
                }
                if (this.get(i).getTRANSACTION_DATE() == null || this.get(i).getTRANSACTION_DATE().equals("null"))
                {
                    pstmt.setDate(12, null);
                }
                else
                {
                    pstmt.setDate(12, Date.valueOf(this.get(i).getTRANSACTION_DATE()));
                }
                if (this.get(i).getACCOUNTING_DATE() == null || this.get(i).getACCOUNTING_DATE().equals("null"))
                {
                    pstmt.setDate(13, null);
                }
                else
                {
                    pstmt.setDate(13, Date.valueOf(this.get(i).getACCOUNTING_DATE()));
                }
                pstmt.setDouble(14, this.get(i).getPOSTING_ID());
                pstmt.setDouble(15, this.get(i).getENTERED_DR());
                pstmt.setDouble(16, this.get(i).getENTERED_CR());
                if (this.get(i).getREFERENCE5() == null || this.get(i).getREFERENCE5().equals("null"))
                {
                    pstmt.setString(17, null);
                }
                else
                {
                    pstmt.setString(17, this.get(i).getREFERENCE5());
                }
                if (this.get(i).getREFERENCE10() == null || this.get(i).getREFERENCE10().equals("null"))
                {
                    pstmt.setString(18, null);
                }
                else
                {
                    pstmt.setString(18, this.get(i).getREFERENCE10());
                }
                if (this.get(i).getCURRENCY_CONVERSION_DATE() == null || this.get(i).getCURRENCY_CONVERSION_DATE().equals("null"))
                {
                    pstmt.setDate(19, null);
                }
                else
                {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getCURRENCY_CONVERSION_DATE()));
                }
                pstmt.setDouble(20, this.get(i).getCURRENCY_CONVERSION_RATE());
                pstmt.setDouble(21, this.get(i).getACCOUNTED_DR());
                pstmt.setDouble(22, this.get(i).getACCOUNTED_CR());
                if (this.get(i).getATTRIBUTE1() == null || this.get(i).getATTRIBUTE1().equals("null"))
                {
                    pstmt.setString(23, null);
                }
                else
                {
                    pstmt.setString(23, this.get(i).getATTRIBUTE1());
                }
                if (this.get(i).getATTRIBUTE2() == null || this.get(i).getATTRIBUTE2().equals("null"))
                {
                    pstmt.setString(24, null);
                }
                else
                {
                    pstmt.setString(24, this.get(i).getATTRIBUTE2());
                }
                if (this.get(i).getATTRIBUTE3() == null || this.get(i).getATTRIBUTE3().equals("null"))
                {
                    pstmt.setString(25, null);
                }
                else
                {
                    pstmt.setString(25, this.get(i).getATTRIBUTE3());
                }
                if (this.get(i).getATTRIBUTE4() == null || this.get(i).getATTRIBUTE4().equals("null"))
                {
                    pstmt.setString(26, null);
                }
                else
                {
                    pstmt.setString(26, this.get(i).getATTRIBUTE4());
                }
                if (this.get(i).getATTRIBUTE5() == null || this.get(i).getATTRIBUTE5().equals("null"))
                {
                    pstmt.setString(27, null);
                }
                else
                {
                    pstmt.setString(27, this.get(i).getATTRIBUTE5());
                }
                if (this.get(i).getATTRIBUTE6() == null || this.get(i).getATTRIBUTE6().equals("null"))
                {
                    pstmt.setString(28, null);
                }
                else
                {
                    pstmt.setString(28, this.get(i).getATTRIBUTE6());
                }
                if (this.get(i).getATTRIBUTE7() == null || this.get(i).getATTRIBUTE7().equals("null"))
                {
                    pstmt.setString(29, null);
                }
                else
                {
                    pstmt.setString(29, this.get(i).getATTRIBUTE7());
                }
                if (this.get(i).getATTRIBUTE8() == null || this.get(i).getATTRIBUTE8().equals("null"))
                {
                    pstmt.setString(30, null);
                }
                else
                {
                    pstmt.setString(30, this.get(i).getATTRIBUTE8());
                }
                if (this.get(i).getATTRIBUTE9() == null || this.get(i).getATTRIBUTE9().equals("null"))
                {
                    pstmt.setString(31, null);
                }
                else
                {
                    pstmt.setString(31, this.get(i).getATTRIBUTE9());
                }
                if (this.get(i).getATTRIBUTE10() == null || this.get(i).getATTRIBUTE10().equals("null"))
                {
                    pstmt.setString(32, null);
                }
                else
                {
                    pstmt.setString(32, this.get(i).getATTRIBUTE10());
                }
                if (this.get(i).getATTRIBUTE11() == null || this.get(i).getATTRIBUTE11().equals("null"))
                {
                    pstmt.setString(33, null);
                }
                else
                {
                    pstmt.setString(33, this.get(i).getATTRIBUTE11());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
            if (!mflag)
            {
                // 如果是内部创建的连接，执行成功后需要执行Commit
                con.commit();
                con.close();
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            int tCount = this.size();
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            for (int i = 1; i <= tCount; i++)
            {
                // 输出出错Sql语句
                sqlObj.setSQL(1, this.get(i));
                System.out.println("出错Sql为：" + sqlObj.getSQL());
            }
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                //如果是内部创建的连接，出错后需要执行RollBack
                try
                {
                    con.rollback();
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete()
    {
        PreparedStatement pstmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            if (!mflag)
            {
                // 如果是内部创建的连接，需要设置Commit模式
                con.setAutoCommit(false);
            }
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM OF_INTERFACE_DETAIL WHERE  ROW_ID = ?");
            for (int i = 1; i <= tCount; i++)
            {
                pstmt.setDouble(1, this.get(i).getROW_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
            if (!mflag)
            {
                // 如果是内部创建的连接，执行成功后需要执行Commit
                con.commit();
                con.close();
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            int tCount = this.size();
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            for (int i = 1; i <= tCount; i++)
            {
                // 输出出错Sql语句
                sqlObj.setSQL(4, this.get(i));
                System.out.println("出错Sql为：" + sqlObj.getSQL());
            }
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                //如果是内部创建的连接，出错后需要执行RollBack
                try
                {
                    con.rollback();
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update()
    {
        PreparedStatement pstmt = null;
        if (!mflag)
        {
            con = DBConnPool.getConnection();
        }
        try
        {
            if (!mflag)
            {
                // 如果是内部创建的连接，需要设置Commit模式
                con.setAutoCommit(false);
            }
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE OF_INTERFACE_DETAIL SET  ROW_ID = ? , CURRENCY_CODE = ? , USER_JE_CATEGORY_NAME = ? , SEGMENT1 = ? , SEGMENT2 = ? , SEGMENT3 = ? , SEGMENT4 = ? , SEGMENT5 = ? , SEGMENT6 = ? , SEGMENT7 = ? , SEGMENT8 = ? , TRANSACTION_DATE = ? , ACCOUNTING_DATE = ? , POSTING_ID = ? , ENTERED_DR = ? , ENTERED_CR = ? , REFERENCE5 = ? , REFERENCE10 = ? , CURRENCY_CONVERSION_DATE = ? , CURRENCY_CONVERSION_RATE = ? , ACCOUNTED_DR = ? , ACCOUNTED_CR = ? , ATTRIBUTE1 = ? , ATTRIBUTE2 = ? , ATTRIBUTE3 = ? , ATTRIBUTE4 = ? , ATTRIBUTE5 = ? , ATTRIBUTE6 = ? , ATTRIBUTE7 = ? , ATTRIBUTE8 = ? , ATTRIBUTE9 = ? , ATTRIBUTE10 = ? , ATTRIBUTE11 = ? WHERE  ROW_ID = ?");
            for (int i = 1; i <= tCount; i++)
            {
                pstmt.setDouble(1, this.get(i).getROW_ID());
                if (this.get(i).getCURRENCY_CODE() == null || this.get(i).getCURRENCY_CODE().equals("null"))
                {
                    pstmt.setString(2, null);
                }
                else
                {
                    pstmt.setString(2, this.get(i).getCURRENCY_CODE());
                }
                if (this.get(i).getUSER_JE_CATEGORY_NAME() == null || this.get(i).getUSER_JE_CATEGORY_NAME().equals("null"))
                {
                    pstmt.setString(3, null);
                }
                else
                {
                    pstmt.setString(3, this.get(i).getUSER_JE_CATEGORY_NAME());
                }
                if (this.get(i).getSEGMENT1() == null || this.get(i).getSEGMENT1().equals("null"))
                {
                    pstmt.setString(4, null);
                }
                else
                {
                    pstmt.setString(4, this.get(i).getSEGMENT1());
                }
                if (this.get(i).getSEGMENT2() == null || this.get(i).getSEGMENT2().equals("null"))
                {
                    pstmt.setString(5, null);
                }
                else
                {
                    pstmt.setString(5, this.get(i).getSEGMENT2());
                }
                if (this.get(i).getSEGMENT3() == null || this.get(i).getSEGMENT3().equals("null"))
                {
                    pstmt.setString(6, null);
                }
                else
                {
                    pstmt.setString(6, this.get(i).getSEGMENT3());
                }
                if (this.get(i).getSEGMENT4() == null || this.get(i).getSEGMENT4().equals("null"))
                {
                    pstmt.setString(7, null);
                }
                else
                {
                    pstmt.setString(7, this.get(i).getSEGMENT4());
                }
                if (this.get(i).getSEGMENT5() == null || this.get(i).getSEGMENT5().equals("null"))
                {
                    pstmt.setString(8, null);
                }
                else
                {
                    pstmt.setString(8, this.get(i).getSEGMENT5());
                }
                if (this.get(i).getSEGMENT6() == null || this.get(i).getSEGMENT6().equals("null"))
                {
                    pstmt.setString(9, null);
                }
                else
                {
                    pstmt.setString(9, this.get(i).getSEGMENT6());
                }
                if (this.get(i).getSEGMENT7() == null || this.get(i).getSEGMENT7().equals("null"))
                {
                    pstmt.setString(10, null);
                }
                else
                {
                    pstmt.setString(10, this.get(i).getSEGMENT7());
                }
                if (this.get(i).getSEGMENT8() == null || this.get(i).getSEGMENT8().equals("null"))
                {
                    pstmt.setString(11, null);
                }
                else
                {
                    pstmt.setString(11, this.get(i).getSEGMENT8());
                }
                if (this.get(i).getTRANSACTION_DATE() == null || this.get(i).getTRANSACTION_DATE().equals("null"))
                {
                    pstmt.setDate(12, null);
                }
                else
                {
                    pstmt.setDate(12, Date.valueOf(this.get(i).getTRANSACTION_DATE()));
                }
                if (this.get(i).getACCOUNTING_DATE() == null || this.get(i).getACCOUNTING_DATE().equals("null"))
                {
                    pstmt.setDate(13, null);
                }
                else
                {
                    pstmt.setDate(13, Date.valueOf(this.get(i).getACCOUNTING_DATE()));
                }
                pstmt.setDouble(14, this.get(i).getPOSTING_ID());
                pstmt.setDouble(15, this.get(i).getENTERED_DR());
                pstmt.setDouble(16, this.get(i).getENTERED_CR());
                if (this.get(i).getREFERENCE5() == null || this.get(i).getREFERENCE5().equals("null"))
                {
                    pstmt.setString(17, null);
                }
                else
                {
                    pstmt.setString(17, this.get(i).getREFERENCE5());
                }
                if (this.get(i).getREFERENCE10() == null || this.get(i).getREFERENCE10().equals("null"))
                {
                    pstmt.setString(18, null);
                }
                else
                {
                    pstmt.setString(18, this.get(i).getREFERENCE10());
                }
                if (this.get(i).getCURRENCY_CONVERSION_DATE() == null || this.get(i).getCURRENCY_CONVERSION_DATE().equals("null"))
                {
                    pstmt.setDate(19, null);
                }
                else
                {
                    pstmt.setDate(19, Date.valueOf(this.get(i).getCURRENCY_CONVERSION_DATE()));
                }
                pstmt.setDouble(20, this.get(i).getCURRENCY_CONVERSION_RATE());
                pstmt.setDouble(21, this.get(i).getACCOUNTED_DR());
                pstmt.setDouble(22, this.get(i).getACCOUNTED_CR());
                if (this.get(i).getATTRIBUTE1() == null || this.get(i).getATTRIBUTE1().equals("null"))
                {
                    pstmt.setString(23, null);
                }
                else
                {
                    pstmt.setString(23, this.get(i).getATTRIBUTE1());
                }
                if (this.get(i).getATTRIBUTE2() == null || this.get(i).getATTRIBUTE2().equals("null"))
                {
                    pstmt.setString(24, null);
                }
                else
                {
                    pstmt.setString(24, this.get(i).getATTRIBUTE2());
                }
                if (this.get(i).getATTRIBUTE3() == null || this.get(i).getATTRIBUTE3().equals("null"))
                {
                    pstmt.setString(25, null);
                }
                else
                {
                    pstmt.setString(25, this.get(i).getATTRIBUTE3());
                }
                if (this.get(i).getATTRIBUTE4() == null || this.get(i).getATTRIBUTE4().equals("null"))
                {
                    pstmt.setString(26, null);
                }
                else
                {
                    pstmt.setString(26, this.get(i).getATTRIBUTE4());
                }
                if (this.get(i).getATTRIBUTE5() == null || this.get(i).getATTRIBUTE5().equals("null"))
                {
                    pstmt.setString(27, null);
                }
                else
                {
                    pstmt.setString(27, this.get(i).getATTRIBUTE5());
                }
                if (this.get(i).getATTRIBUTE6() == null || this.get(i).getATTRIBUTE6().equals("null"))
                {
                    pstmt.setString(28, null);
                }
                else
                {
                    pstmt.setString(28, this.get(i).getATTRIBUTE6());
                }
                if (this.get(i).getATTRIBUTE7() == null || this.get(i).getATTRIBUTE7().equals("null"))
                {
                    pstmt.setString(29, null);
                }
                else
                {
                    pstmt.setString(29, this.get(i).getATTRIBUTE7());
                }
                if (this.get(i).getATTRIBUTE8() == null || this.get(i).getATTRIBUTE8().equals("null"))
                {
                    pstmt.setString(30, null);
                }
                else
                {
                    pstmt.setString(30, this.get(i).getATTRIBUTE8());
                }
                if (this.get(i).getATTRIBUTE9() == null || this.get(i).getATTRIBUTE9().equals("null"))
                {
                    pstmt.setString(31, null);
                }
                else
                {
                    pstmt.setString(31, this.get(i).getATTRIBUTE9());
                }
                if (this.get(i).getATTRIBUTE10() == null || this.get(i).getATTRIBUTE10().equals("null"))
                {
                    pstmt.setString(32, null);
                }
                else
                {
                    pstmt.setString(32, this.get(i).getATTRIBUTE10());
                }
                if (this.get(i).getATTRIBUTE11() == null || this.get(i).getATTRIBUTE11().equals("null"))
                {
                    pstmt.setString(33, null);
                }
                else
                {
                    pstmt.setString(33, this.get(i).getATTRIBUTE11());
                }
                pstmt.setDouble(34, this.get(i).getROW_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
            if (!mflag)
            {
                // 如果是内部创建的连接，执行成功后需要执行Commit
                con.commit();
                con.close();
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "OF_INTERFACE_DETAILDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            int tCount = this.size();
            SQLString sqlObj = new SQLString("OF_INTERFACE_DETAIL");
            for (int i = 1; i <= tCount; i++)
            {
                // 输出出错Sql语句
                sqlObj.setSQL(2, this.get(i));
                System.out.println("出错Sql为：" + sqlObj.getSQL());
            }
            try
            {
                pstmt.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (!mflag)
            {
                //如果是内部创建的连接，出错后需要执行RollBack
                try
                {
                    con.rollback();
                    con.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            return false;
        }
        finally
        {
            if (!mflag)
            {
                try
                {
                    if(con.isClosed() == false)
                    {
                        con.close();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
}
