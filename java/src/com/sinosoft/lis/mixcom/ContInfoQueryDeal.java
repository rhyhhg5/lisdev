package com.sinosoft.lis.mixcom;

import java.util.Date;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class ContInfoQueryDeal
{
    private final static Logger cLogger = Logger
            .getLogger(ContInfoQueryDeal.class);

    private final Document cInXmlDoc;

    private String mDealState = null;

    private StringBuffer mErrInfo = new StringBuffer();

    /** 报文处理成功 */
    public final static String DEAL_SUCC = new String("00");

    /** 报文处理失败 */
    public final static String DEAL_FAIL = new String("01");

    // ----- 卡单中所需数据 -----
    private String mGrpAgentCode = null;

    private String mGrpAgentCom = null;

    private String mDate = null;

    private String mOperator = null;

    // -------------------------

    public ContInfoQueryDeal(Document pInXmlDoc)
    {
        cInXmlDoc = pInXmlDoc;
    }

    public Document deal()
    {
        cLogger.info("Into ContInfoQueryDeal.deal()...");

        if (!dealContXml())
        {
            this.mDealState = ContInfoQueryDeal.DEAL_FAIL;
        }
        else
        {
            this.mDealState = ContInfoQueryDeal.DEAL_SUCC;
        }

        // 以报文形式，返回处理结果
        Document tDealResultDoc = createDealResultDoc();
        // --------------------

        cLogger.info("Out BusinessDeal.deal()!");

        return tDealResultDoc;
    }

    /**
     * 处理接收到的报文xml
     * @return
     */
    private boolean dealContXml()
    {
        // 装载报文中业务数据。
        if (!loadData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkLogicData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkBaseData())
        {
            return false;
        }

        return true;
    }

    private boolean loadData()
    {
        Document tTmpDoc = this.cInXmlDoc;

        if (tTmpDoc == null)
        {
            return false;
        }

        Element tRootData = tTmpDoc.getRootElement();

        Element tContTable = tRootData.getChild("BODY");
        if (tContTable == null)
        {
            errLog("无保单数据");
            return false;
        }

        this.mGrpAgentCode = tContTable.getChildTextTrim("SALERCOD");
        this.mGrpAgentCom = tContTable.getChildTextTrim("SALERORGCOD");
        this.mDate = tContTable.getChildTextTrim("SIGNDATE");

        return true;
    }

    /**
     * 校验报文数据是否完整。
     * @return
     */
    private boolean checkBaseData()
    {
        if (this.mGrpAgentCode == null || this.mGrpAgentCode.equals(""))
        {
            errLog("对方业务员代码为空");
            return false;
        }

        if (this.mGrpAgentCom == null || this.mGrpAgentCom.equals(""))
        {
            errLog("对方机构代码为空");
            return false;
        }

        if (this.mDate == null || this.mDate.equals(""))
        {
            errLog("开始时间为空");
            return false;
        }

        if (this.mOperator == null || this.mOperator.equals(""))
        {
            //            errLog("单证操作员为空");
            //            return false;
        }

        return true;
    }

    /**
     * 校验业务逻辑
     * @return
     */
    private boolean checkLogicData()
    {
        Date actDate = new FDate().getDate(mDate);
        if (actDate == null)
        {
            errLog("系统传入日期不符合YYYY-MM-DD格式。");
            return false;
        }
        return true;
    }

    private Document createDealResultDoc()
    {
        String tGrpAgentCode = mGrpAgentCode;
        String tGrpAgentCom = mGrpAgentCom;

        String tStrSql = null;

        //查询符合条件的记录
        SSRS tSSRS = new SSRS();
        tStrSql = " select '',a.contno,a.appntname,a.insuredname,(select riskcode from lcpol where contno = a.contno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' ) fetch first 1 rows only),"
                + " a.prem,a.amnt,a.cvalidate,(select insuyear from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = a.uwflag),"
                + " case a.stateflag when '1' then '有效' else '失效' end from lccont a where a.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and a.Grpagentcom = '"
                + tGrpAgentCom
                + "' and a.signdate >= '"
                + this.mDate
                + "' "
                + " union "
                + " select '',b.grpcontno,b.grpname,'',(select riskcode from lcgrppol where grpcontno = b.grpcontno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' )fetch first 1 rows only),"
                + " b.prem,b.amnt,b.cvalidate,(select insuyear from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = b.uwflag),"
                + " case b.stateflag when '1' then '有效' else '失效' end from lcgrpcont b where b.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and b.grpagentcom = '"
                + tGrpAgentCom
                + "' and b.signdate >= '"
                + this.mDate
                + "' "
                + " union"
                + " select '',a.contno,a.appntname,a.insuredname,(select riskcode from lcpol where contno = a.contno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' ) fetch first 1 rows only),"
                + " a.prem,a.amnt,a.cvalidate,(select insuyear from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = a.uwflag),"
                + " case a.stateflag when '1' then '有效' else '失效' end from lccont a where a.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and a.Grpagentcom = '"
                + tGrpAgentCom
                + "' and a.uwdate >= '"
                + this.mDate
                + "' "
                + " union "
                + " select '',b.grpcontno,b.grpname,'',(select riskcode from lcgrppol where grpcontno = b.grpcontno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' )fetch first 1 rows only),"
                + " b.prem,b.amnt,b.cvalidate,(select insuyear from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = b.uwflag),"
                + " case b.stateflag when '1' then '有效' else '失效' end from lcgrpcont b where b.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and b.grpagentcom = '"
                + tGrpAgentCom
                + "' and b.uwdate >= '"
                + this.mDate
                + "' "
                + " union"
                + " select '',a.contno,a.appntname,a.insuredname,(select riskcode from lcpol where contno = a.contno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' ) fetch first 1 rows only),"
                + " a.prem,a.amnt,a.cvalidate,(select insuyear from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = a.uwflag),"
                + " case a.stateflag when '1' then '有效' else '失效' end from lccont a where a.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and a.grpagentcom = '"
                + tGrpAgentCom
                + "' and a.polapplydate >= '"
                + this.mDate
                + "' "
                + " union "
                + " select '',b.grpcontno,b.grpname,'',(select riskcode from lcgrppol where grpcontno = b.grpcontno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' )fetch first 1 rows only),"
                + " b.prem,b.amnt,b.cvalidate,(select insuyear from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = b.uwflag),"
                + " case b.stateflag when '1' then '有效' else '失效' end from lcgrpcont b where b.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and b.grpagentcom = '"
                + tGrpAgentCom
                + "' and b.polapplydate >= '"
                + this.mDate
                + "' "
                + " union "
                + " select '',a.contno,a.appntname,a.insuredname,(select riskcode from lcpol where contno = a.contno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' ) fetch first 1 rows only),"
                + " a.prem,a.amnt,a.cvalidate,(select insuyear from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where contno = a.contno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = a.uwflag),"
                + " case a.stateflag when '1' then '有效' else '失效' end from lccont a where a.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and a.grpagentcom = '"
                + tGrpAgentCom
                + "' and a.makedate >= '"
                + this.mDate
                + "' "
                + " union "
                + " select '',b.grpcontno,b.grpname,'',(select riskcode from lcgrppol where grpcontno = b.grpcontno"
                + " and riskcode in(select riskcode from lmriskapp where subriskflag = 'M' )fetch first 1 rows only),"
                + " b.prem,b.amnt,b.cvalidate,(select insuyear from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select insuyearflag from lcpol where grpcontno = b.grpcontno fetch first 1 rows only),"
                + " (select codename from ldcode where codetype = 'uwflag' and code = b.uwflag),"
                + " case b.stateflag when '1' then '有效' else '失效' end from lcgrpcont b where b.GrpAgentCode = '"
                + tGrpAgentCode
                + "' "
                + " and b.grpagentcom = '"
                + tGrpAgentCom + "' and b.makedate >= '" + this.mDate + "' ";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tStrSql);

        Element tPacket = new Element("PACKET");
        Element tRootData = new Element("BODY");

        tPacket.addAttribute("type", "RESPONSE");
        tPacket.addAttribute("version", "1.0");

        Element mHeadData = new Element("HEAD");
        Element tOperator = new Element("CLASSIC");
        tOperator.setText(mGrpAgentCode + "|" + mGrpAgentCom);

        Element tQuenu = new Element("QUENU");
        tQuenu.setText("HENLTH");

        Element tStatus = new Element("STATUS");
        if (tSSRS.MaxRow >= 500)
        {
            tStatus.setText("2");
        }
        else
        {
            if (tSSRS.MaxRow < 0)
            {
                tStatus.setText("0");
            }
            else
            {
                tStatus.setText("1");
            }

        }
        mHeadData.addContent(tOperator);
        mHeadData.addContent(tQuenu);
        mHeadData.addContent(tStatus);
        tPacket.addContent(mHeadData);

        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            Element mRootData = new Element("POLICY");
            Element tPolicyNo = new Element("POLICYNO");
            tPolicyNo.setText("");

            Element tProposalNo = new Element("PROPOSALNO");
            tProposalNo.setText(tSSRS.GetText(i, 2));

            Element tApplyName = new Element("APPLINAME");
            tApplyName.setText(tSSRS.GetText(i, 3));

            Element tInsuredName = new Element("INSUERDNAME");
            tInsuredName.setText(tSSRS.GetText(i, 4));

            Element tRiskName = new Element("RISKNAME");
            tRiskName.setText(tSSRS.GetText(i, 5));

            Element tPrem = new Element("PREMPOL");
            tPrem.setText(tSSRS.GetText(i, 6));

            Element tAmnt = new Element("AMOUNT");
            tAmnt.setText(tSSRS.GetText(i, 7));

            Element tCvalidate = new Element("VALIDATE");
            tCvalidate.setText(tSSRS.GetText(i, 8));

            Element tYears = new Element("PERIOD");
            tYears.setText(tSSRS.GetText(i, 9));

            Element tUwflag = new Element("UNDERWRITEFLAG");
            tUwflag.setText(tSSRS.GetText(i, 11));

            Element tStatusFlag = new Element("STATUSCOD");
            tStatusFlag.setText(tSSRS.GetText(i, 12));

            mRootData.addContent(tPolicyNo);
            mRootData.addContent(tProposalNo);
            mRootData.addContent(tApplyName);
            mRootData.addContent(tInsuredName);
            mRootData.addContent(tRiskName);
            mRootData.addContent(tPrem);
            mRootData.addContent(tAmnt);
            mRootData.addContent(tCvalidate);
            mRootData.addContent(tYears);
            mRootData.addContent(tUwflag);
            mRootData.addContent(tStatusFlag);

            tRootData.addContent(mRootData);

        }
        tPacket.addContent(tRootData);
        Document tDocDealResult = new Document(tPacket);

        return tDocDealResult;
    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        cLogger.error(cErrInfo);
        this.mErrInfo.append(cErrInfo);
    }
}
