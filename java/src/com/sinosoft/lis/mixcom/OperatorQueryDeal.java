package com.sinosoft.lis.mixcom;

import java.util.Date;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class OperatorQueryDeal
{
    private final static Logger cLogger = Logger
            .getLogger(ContInfoQueryDeal.class);

    private final Document cInXmlDoc;

    private String mDealState = null;

    private StringBuffer mErrInfo = new StringBuffer();


    /** 报文处理失败 */
    public final static String DEAL_FAIL = new String("0");
    /** 报文处理成功 */
    public final static String DEAL_SUCC = new String("1");
    /** 信息条数过大（大于500条信息时，按照签单日期正序排列并取前500条记录）*/
    public final static String DEAL_LARGE = new String("2");

    // ----- 卡单中所需数据 -----
    private String mOperatorcod  = null;//代理方出单人员工号

    private String mOperatorOrgcod = null; //代理方出单人员机构号

    private String mOperatorCompcod = null;//代理方出单人员子公司代码

    private String mSigndate = null;//查询日期

    // -------------------------

    public OperatorQueryDeal(Document pInXmlDoc)
    {
        cInXmlDoc = pInXmlDoc;
    }

    public Document deal()
    {
        cLogger.info("Into ContInfoQueryDeal.deal()...");

        if (!dealContXml())
        {
            this.mDealState = ContInfoQueryDeal.DEAL_FAIL;
        }
        else
        {
            this.mDealState = ContInfoQueryDeal.DEAL_SUCC;
        }

        // 以报文形式，返回处理结果
        Document tDealResultDoc = createDealResultDoc();
        // --------------------

        cLogger.info("Out BusinessDeal.deal()!");

        return tDealResultDoc;
    }

    /**
     * 处理接收到的报文xml
     * @return
     */
    private boolean dealContXml()
    {
        // 装载报文中业务数据。
        if (!loadData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkLogicData())
        {
            return false;
        }
        // --------------------

        // 校验报文数据是否完整
        if (!checkBaseData())
        {
            return false;
        }

        return true;
    }

    private boolean loadData()
    {
        Document tTmpDoc = this.cInXmlDoc;

        if (tTmpDoc == null)
        {
            return false;
        }

        Element tRootData = tTmpDoc.getRootElement();

        Element tContTable = tRootData.getChild("BODY");
        if (tContTable == null)
        {
            errLog("无保单数据");
            return false;
        }

        this.mOperatorcod = tContTable.getChildTextTrim("OPERATORCOD");//代理方出单人员工号
        this.mOperatorOrgcod = tContTable.getChildTextTrim("OPERATORORGCOD");//代理方出单人员机构号
        this.mOperatorCompcod = tContTable.getChildTextTrim("OPERATORCOMPCOD");//代理方出单人员子公司代码
        this.mSigndate = tContTable.getChildTextTrim("SIGNDATE");//查询日期

        return true;
    }

    /**
     * 校验报文数据是否完整。
     * @return
     */
    private boolean checkBaseData()
    {
        if (this.mOperatorcod == null || this.mOperatorcod.equals(""))
        {
            errLog("代理方出单人员工号为空!");
            return false;
        }

        if (this.mOperatorOrgcod == null || this.mOperatorOrgcod.equals(""))
        {
            errLog("代理方出单人员机构号为空");
            return false;
        }
        
        if (this.mOperatorCompcod == null || this.mOperatorCompcod.equals(""))
        {
            errLog("单证操作员为空");
            return false;
        }

        if (this.mSigndate == null || this.mSigndate.equals(""))
        {
            errLog("开始时间为空");
            return false;
        }
        return true;
    }

    /**
     * 校验业务逻辑
     * @return
     */
    private boolean checkLogicData()
    {
        Date actDate = new FDate().getDate(mSigndate);
        if (actDate == null)
        {
            errLog("系统传入日期不符合YYYY-MM-DD格式。");
            return false;
        }
        return true;
    }

    private Document createDealResultDoc()
    {
//        String tGrpAgentCode = mOperatorcod;
//        String tGrpAgentCom = mOperatorOrgcod;

        String tStrSql = null;

        //查询符合条件的记录
        SSRS tSSRS = new SSRS();

        //date 2010 by gzh
        tStrSql = "select lcc.contno 保单号,lcc.proposalcontno 投保单号," +
        		  " es.makedate 扫描日期,"+
                  " lcu.uwidea 核保结论," +
                  " codename('stateflag',lcc.stateflag) 保单状态,"+
                  " es.doccode 文件编号,'' 文件名称,es.subtype," +
                  "  codename('esapplytype',subtype)  投保书名称,"+
                  " (select riskname from lcpol lcp,lmrisk lmr where lcc.contno = contno and lcp.riskcode = lmr.riskcode fetch first 1 rows only) 主险险种,"+
                  " es.numpages"+
                  " from lccont lcc left join es_doc_main es on lcc.prtno = es.doccode" +
//                  " left join ldcode ldc on es.subtype = ldc.code "+
                  " left join lcuwmaster lcu on lcu.contno = lcc.contno"+
                  " left join lomixcom lom on lom.grpagentcom = lcc.grpagentcom "+
                  " where 1=1"+
                  " and lcc.crs_salechnl <> ''"+
                  " and lcc.grpagentcode = '"+this.mOperatorcod+"'"+
                  " and lcc.grpagentcom = '"+this.mOperatorOrgcod+"'"+
                  " and es.makedate >='"+this.mSigndate+"'"+
                  " and comp_cod ='"+mOperatorCompcod+"' "+
                  " group by lcc.contno,lcc.proposalcontno,es.makedate,lcu.uwidea,lcc.stateflag,es.doccode,es.subtype,es.numpages";
        
        System.out.println("出单员保单实时查询："+tStrSql);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tStrSql);

        Element tPacket = new Element("PACKET");
        tPacket.addAttribute("type", "RESPONSE");
        tPacket.addAttribute("version", "1.0");
        
// 处理"HEAD"节点
        Element mHeadData = new Element("HEAD");
        Element tOperator = new Element("CLASSIC");
        tOperator.setText(mOperatorcod + "|" + mOperatorOrgcod);
        Element tQuenu = new Element("QUENU");
        tQuenu.setText("HENLTH");
        Element tStatus = new Element("STATUS");
        if (tSSRS.MaxRow >= 500)
        {
            tStatus.setText("2");
        }
        else
        {
            if (tSSRS.MaxRow < 0)
            {
                tStatus.setText("0");
            }
            else
            {
                tStatus.setText("1");
            }

        }
        mHeadData.addContent(tOperator);
        mHeadData.addContent(tQuenu);
        mHeadData.addContent(tStatus);
        tPacket.addContent(mHeadData);
//      处理"BODY"节点
        Element tRootData = new Element("BODY");

        for (int i = 1; i <= tSSRS.MaxRow; i++)
        {
            Element mRootData = new Element("POLICY");
            Element tPolicyNo = new Element("POLICYNO");
            tPolicyNo.setText(tSSRS.GetText(i, 1));

            Element tProposalNo = new Element("PROPOSALNO");
            tProposalNo.setText(tSSRS.GetText(i, 2));

            Element tScandate = new Element("SCANDATE");
            tScandate.setText(tSSRS.GetText(i, 3));

            Element tUnderwriteflag = new Element("UNDERWRITEFLAG");
            tUnderwriteflag.setText(tSSRS.GetText(i, 4));

            Element tStatuscod = new Element("STATUSCOD");
            tStatuscod.setText(tSSRS.GetText(i, 5));

            Element tFilecod = new Element("FILECOD");
            tFilecod.setText(tSSRS.GetText(i, 6));

            Element tFilename = new Element("FILENAME");
            tFilename.setText(tSSRS.GetText(i, 8)+"-"+tSSRS.GetText(i, 9));
            
//            add  by fuxin 投保书名称
            Element tPROPOSALNAME = new Element("PROPOSALNAME");
            tPROPOSALNAME.setText(tSSRS.GetText(i, 9));
            
            Element tMainRisk = new Element("MAINRISK");
            tMainRisk.setText(tSSRS.GetText(i, 10));
            
            Element tProofData = new Element("PROOF");
            Element tProof1 = new Element("PROOF1");

            Element tPeriod = new Element("PROOFID");
            tPeriod.setText(tSSRS.GetText(i, 6));

            Element tProofname = new Element("PROOFNAME");
            tProofname.setText(tSSRS.GetText(i, 8)+"-"+tSSRS.GetText(i, 9));
            
            String nums = "";
            for(int proofnums=1;proofnums<=Integer.parseInt(tSSRS.GetText(i, 11));proofnums++){
            	if(!nums.equals("")){
            		nums+=",";
            	} 
            	nums +=proofnums+"";
            }
            Element tProofnum = new Element("PROOFNUM");
            tProofnum.setText(nums);
            
            Element tProofcount = new Element("PROOFCOUNT");
            tProofcount.setText(tSSRS.GetText(i, 11));
            
            tProof1.addContent(tPeriod);
            tProof1.addContent(tProofname);
            tProof1.addContent(tProofnum);
            tProof1.addContent(tProofcount);
            tProofData.addContent(tProof1);
            mRootData.addContent(tPolicyNo);
            mRootData.addContent(tProposalNo);
            mRootData.addContent(tScandate);
            mRootData.addContent(tUnderwriteflag);
            mRootData.addContent(tStatuscod);
            mRootData.addContent(tFilecod);
            mRootData.addContent(tFilename);
            mRootData.addContent(tPROPOSALNAME);
            mRootData.addContent(tMainRisk);
            mRootData.addContent(tProofData);
//            mRootData.addContent(tYears);
//            mRootData.addContent(tUwflag);
//            mRootData.addContent(tStatusFlag);

            tRootData.addContent(mRootData);

        }
        tPacket.addContent(tRootData);
        Document tDocDealResult = new Document(tPacket);

        return tDocDealResult;
    }

    /**
     * 记录错误日志
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        cLogger.error(cErrInfo);
        this.mErrInfo.append(cErrInfo);
    }
    
}
