package com.sinosoft.lis.delbankbatch;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;

import com.sinosoft.utility.VData;

public class DelBankBatchBL {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private VData mInputData;

	private String mOperate;

	private GlobalInput mGlobalInput;

	private String Serialno;

	private TransferData mTransferData = new TransferData();

	private LYBankLogDB mLYBankLogDB = new LYBankLogDB();

	private LYSendToBankDB mLYSendToBankDB = new LYSendToBankDB();

	private LYBankLogSet mLYBankLogSet = new LYBankLogSet();

	private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

	private LYBankLogSchema mLYBankLogSchema = new LYBankLogSchema();

	private LYSendToBankSchema mLYSendToBankSchema = new LYSendToBankSchema();

	private LJSPaySet tLJSPaySet = new LJSPaySet();

	private LJAGetSet tLJAGetSet = new LJAGetSet();

	private String mCurrentData = PubFun.getCurrentDate();

	private String mCurrentTime = PubFun.getCurrentTime();

	private String paycode;

	private String paytype;

	private MMap map = new MMap();

	public DelBankBatchBL() {
	}

	public boolean submitData(VData nInputData, String cOperate) {

		mInputData = nInputData;

		mOperate = cOperate;

		if (!getInputData()) {

			return false;
		}
		if (!dealdata()) {

			return false;
		}

		if (!prepareOutputData()) {

		}
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mResult, "INSERT")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	// 接收数据
	private boolean getInputData() {

		try {
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
			mTransferData = (TransferData) mInputData.getObjectByObjectName(
					"TransferData", 0);
			System.out.println("Into DelBankBatchBL.getInputData()...");
			if (mTransferData == null) {
				CError tError = new CError();
				tError.moduleName = "DelBankBatchBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "传入的数据为空！";
				this.mErrors.addOneError(tError);
				return false;
			}

			Serialno = (String) mTransferData.getValueByName("Serialno");
			paycode = (String) mTransferData.getValueByName("paycode");
			paytype = (String) mTransferData.getValueByName("paytype");

			System.out.println(mOperate + "   " + paycode);

		} catch (Exception e) {

			e.printStackTrace();
			System.out.println("获取数据失败，具体原因是：" + e.getMessage());
			return false;

		}

		return true;
	}

	// 数据处理
	private boolean dealdata() {
		if (mOperate.equals("DELETE")) {

			if (Serialno != null || !Serialno.equals("")) {
				mLYBankLogDB.setSerialNo(Serialno);
				mLYSendToBankDB.setSerialNo(Serialno);
				mLYBankLogSet = mLYBankLogDB.query();
				mLYSendToBankSet = mLYSendToBankDB.query();
				if (mLYBankLogSet.size() == 0) {
					CError tError = new CError();
					tError.moduleName = "DelBankBatchBL";
					tError.functionName = "dealdata";
					tError.errorMessage = "LYBankLog数据为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if (mLYSendToBankSet.size() == 0) {
					CError tError = new CError();
					tError.moduleName = "DelBankBatchBL";
					tError.functionName = "dealdata";
					tError.errorMessage = "LYSendToBank数据为空！";
					this.mErrors.addOneError(tError);

					return false;
				}

				mLYBankLogSchema = mLYBankLogSet.get(1);
				mLYSendToBankSchema = mLYSendToBankSet.get(1);
				String LogType = mLYBankLogSchema.getLogType();
				if (LogType.equals("S")) {

					for (int i = 1; i <= mLYSendToBankSet.size(); i++) {
						LJSPaySchema mLJSPaySchema = new LJSPaySchema();
						mLYSendToBankSchema = mLYSendToBankSet.get(i);
						LJSPayDB mLJSPayDB = new LJSPayDB();
						mLJSPayDB.setGetNoticeNo(mLYSendToBankSchema.getPayCode());
						LJSPaySet mLJSPaySet = mLJSPayDB.query();
						mLJSPaySchema = mLJSPaySet.get(1);
						mLJSPaySchema.setBankOnTheWayFlag("0");
						mLJSPaySchema.setCanSendBank("0");
						mLJSPaySchema.setSendBankCount(mLJSPaySchema.getSendBankCount() - 1);
						mLJSPaySchema.setModifyDate(mCurrentData);
						mLJSPaySchema.setModifyTime(mCurrentTime);
						tLJSPaySet.add(mLJSPaySchema);
					}

				} else {
					for (int i = 1; i <= mLYSendToBankSet.size(); i++) {
						LJAGetSchema mLJAGetSchema = new LJAGetSchema();
						mLYSendToBankSchema = mLYSendToBankSet.get(i);
						LJAGetDB mLJAGetDB = new LJAGetDB();
						mLJAGetDB.setActuGetNo(mLYSendToBankSchema.getPayCode());
						LJAGetSet mLJAGetSet = mLJAGetDB.query();
						mLJAGetSchema = mLJAGetSet.get(1);
						mLJAGetSchema.setBankOnTheWayFlag("0");
						mLJAGetSchema.setCanSendBank("0");
						mLJAGetSchema.setSendBankCount(mLJAGetSchema.getSendBankCount() - 1);
						mLJAGetSchema.setModifyDate(mCurrentData);
						mLJAGetSchema.setModifyTime(mCurrentTime);
						tLJAGetSet.add(mLJAGetSchema);
					}

				}
			}
		}
		if (mOperate.equals("DELETE1")) {
			if (paytype.equals("F")) {
				System.out.println("进入删除应付数据");
				LYSendToBankDB aLYSendToBankDB = new LYSendToBankDB();
				aLYSendToBankDB.setPayCode(paycode);
				LYSendToBankSet aLYSendToBankSet = aLYSendToBankDB.query();
				LYSendToBankSchema aLYSendToBankSchema = aLYSendToBankSet.get(1);
				LYBankLogDB aLYBankLogDB = new LYBankLogDB();
				aLYBankLogDB.setSerialNo(aLYSendToBankSchema.getSerialNo());
				LYBankLogSet aLYBankLogSet = aLYBankLogDB.query();
				LYBankLogSchema aLYBankLogSchema = aLYBankLogSet.get(1);
				aLYBankLogSchema.setTotalMoney(aLYBankLogSchema.getTotalMoney()- aLYSendToBankSchema.getPayMoney());
				aLYBankLogSchema.setTotalNum(aLYBankLogSchema.getTotalNum() - 1);
				aLYBankLogSchema.setModifyDate(mCurrentData);
				aLYBankLogSchema.setModifyTime(mCurrentTime);
				LJAGetDB aLJAGetDB = new LJAGetDB();
				aLJAGetDB.setActuGetNo(paycode);
				LJAGetSet aLJAGetSet = aLJAGetDB.query();
				LJAGetSchema aLJAGetSchema = aLJAGetSet.get(1);
				aLJAGetSchema.setCanSendBank("1");
				aLJAGetSchema.setBankOnTheWayFlag("0");
				aLJAGetSchema.setModifyDate(mCurrentData);
				aLJAGetSchema.setModifyTime(mCurrentTime);
				map.put(aLYSendToBankSchema, "DELETE");
				map.put(aLYBankLogSchema, "UPDATE");
				map.put(aLJAGetSchema, "UPDATE");
			} else {

				LYSendToBankDB gLYSendToBankDB = new LYSendToBankDB();
				gLYSendToBankDB.setPayCode(paycode);
				LYSendToBankSet gLYSendToBankSet = gLYSendToBankDB.query();
				LYSendToBankSchema gLYSendToBankSchema = gLYSendToBankSet.get(1);
				LYBankLogDB gLYBankLogDB = new LYBankLogDB();
				gLYBankLogDB.setSerialNo(gLYSendToBankSchema.getSerialNo());
				LYBankLogSet gLYBankLogSet = gLYBankLogDB.query();
				LYBankLogSchema gLYBankLogSchema = gLYBankLogSet.get(1);
				gLYBankLogSchema.setTotalMoney(gLYBankLogSchema.getTotalMoney()- gLYSendToBankSchema.getPayMoney());
				gLYBankLogSchema.setTotalNum(gLYBankLogSchema.getTotalNum() - 1);
				gLYBankLogSchema.setModifyDate(mCurrentData);
				gLYBankLogSchema.setModifyTime(mCurrentTime);
				LJSPayDB gLJSPayDB = new LJSPayDB();
				gLJSPayDB.setGetNoticeNo(paycode);
				LJSPaySet gLJSPaySet = gLJSPayDB.query();
				LJSPaySchema gLJSPaySchema = gLJSPaySet.get(1);
				gLJSPaySchema.setCanSendBank("1");
				gLJSPaySchema.setBankOnTheWayFlag("0");
				gLJSPaySchema.setModifyDate(mCurrentData);
				gLJSPaySchema.setModifyTime(mCurrentTime);
				map.put(gLYSendToBankSchema, "DELETE");
				map.put(gLYBankLogSchema, "UPDATE");
				map.put(gLJSPaySchema, "UPDATE");
			}
		}
		return true;
	}

	private boolean prepareOutputData() {

		if (tLJSPaySet.size() != 0) {

			map.put(mLYBankLogSchema, "DELETE");
			map.put(mLYSendToBankSet, "DELETE");
			map.put(tLJSPaySet, "UPDATE");
		} else {
			map.put(tLJAGetSet, "UPDATE");
			map.put(mLYBankLogSchema, "DELETE");
			map.put(mLYSendToBankSet, "DELETE");

		}
		mResult.add(map);
		return true;
	}

}
