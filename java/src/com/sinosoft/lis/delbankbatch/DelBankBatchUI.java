package com.sinosoft.lis.delbankbatch;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

public class DelBankBatchUI {
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	public DelBankBatchUI() {
	}

	public boolean submitData(VData nInputData, String cOperate) {
		DelBankBatchBL tDelBankBatchBL = new DelBankBatchBL();
		if (!tDelBankBatchBL.submitData(nInputData, cOperate)) {
			this.mErrors.copyAllErrors(tDelBankBatchBL.mErrors);
			return false;
		} else {
			mResult = tDelBankBatchBL.getResult();
		}
		return true;
	}

	/**
	 * �������
	 * 
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}
}
