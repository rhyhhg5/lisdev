package com.sinosoft.lis.xb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 本类用来处理个单险种的续保申请，生成续保数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewPolAppBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private VData mPolInfoVData = null;
    private MMap map = null;
    /*需要做续保的险种信息*/
    private LCPolSchema mLCPolSchema = null;  //换号前的LCPol信息
    private LCPolSchema mLCPolSchemaUWed = null;  //核保后的险种
    private LPPolSchema mLPPolSchemaUWed = null;  //核保后的险种
    private LCPolSchema newLCPolSchema = null;  //换号后的LCPol信息
    private TransferData mTransferData = null;
    private boolean isGrpCont = false;
    private boolean isAfterUW = false;
    public static HashMap mSqlHashMap = new HashMap();

    private String mNewProposalNo = null;

    private String mOperate = null;

    public PRnewPolAppBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括：LCPolSchema, GlobalInput
     * 不需要在上一层取出这些信息进行变更
     * GlobalInput：操作员信息
     * @param operate String，在此位“”
     * @return boolean
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in PRnewPolAppBL->getSubmitMap");
        mOperate = operate;
        if(!getInputData(inputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 得到保单传入的数据
     * @param inputData VData: 包括：LCPolSchema, GlobalInput
     * @return boolean：获取成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCPolSchema = (LCPolSchema) inputData
                       .getObjectByObjectName("LCPolSchema", 0);
        mTransferData = (TransferData) inputData
                        .getObjectByObjectName("TransferData", 0);
        if(mTransferData != null)
        {
            String strGrp = (String) mTransferData.getValueByName(XBConst.
                IS_GRP);
            isGrpCont = (strGrp !=null && XBConst.IS_GRP.equals(strGrp));

            String afterUW = (String) mTransferData.getValueByName(XBConst.AFTERUW);
            isAfterUW = (afterUW !=null && XBConst.AFTERUW.equals(afterUW));
        }

        if (mGlobalInput == null || mLCPolSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        mLCPolSchemaUWed = (LCPolSchema) mTransferData
                        .getValueByName("LCPolUWed");
        mLPPolSchemaUWed = (LPPolSchema) inputData
        .getObjectByObjectName("LPPolSchema", 0);

        return true;
    }

    /**
     * 对需要续保的险种进行续保操作合法性的校验：
     * 1、校验被保人年龄是否大于险种描述的最大年龄
     * 2、校验当前险种定义是否符合续保定义
     * @return boolean，符合校验规则true，否则false
     */
    private boolean checkData()
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();  //险种定义

        String sql = "select * from LMRiskApp where riskCode = '"
                     + mLCPolSchema.getRiskCode() + "' ";
        Object o = mSqlHashMap.get(sql);
        if(o != null)
        {
            tLMRiskAppDB = (LMRiskAppDB) mSqlHashMap.get(sql);
        }
        else
        {
            tLMRiskAppDB.setRiskCode(mLCPolSchema.getRiskCode());
            tLMRiskAppDB.getInfo();
            mSqlHashMap.put(sql, tLMRiskAppDB);

            //校验当前险种定义是否符合续保定义
            if (!checkPolDefinition(tLMRiskAppDB))
            {
                return false;
            }
        }

        if(!this.isGrpCont && !isAfterUW && this.inReNew())
        {
            return false;
        }

        //校验被保人年龄是否超过险种定义的最大投保年龄
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLCPolSchema.getContNo());
        tLCInsuredDB.setInsuredNo(mLCPolSchema.getInsuredNo());
        if(!tLCInsuredDB.getInfo())
        {
            mErrors.addOneError("没有查询到险种的被保人信息，");
            return false;
        }
        //有的险种被保人最大年龄没有描述，认为没有限制，此时tLMRiskAppDB的MaxInsuredAge值为0(旧)
        //续保时投保年龄先计算,避免没有MaxInsuredAge的险种,不计算投保年龄(新)
        int tAppntAge = PubFun.calInterval(tLCInsuredDB.getBirthday(),
                                           mLCPolSchema.getEndDate(), "Y");

        System.out.println(tLCInsuredDB.getBirthday() + ", " + mLCPolSchema.getEndDate());
        System.out.println("投保年龄:" + tAppntAge);
        System.out.println("投保年龄描述:" + tLMRiskAppDB.getMaxInsuredAge());
        
        /**
         *  增加最大续保年龄的校验,若符合最大续保年龄,则不再校验投保年龄
         */
        String tMaxRnewAge = tLMRiskAppDB.getMaxRnewAge();
        //按退休年龄校验,暂时按男60，女55算
        if("FD".equals(tMaxRnewAge)){
        	//三个税优险种 123601 123602  123501 如果首次投保为45周岁之前，可续保至75周岁,否则续保至退休年龄
        	if("123601".equals(tLMRiskAppDB.getRiskCode())||"123602".equals(tLMRiskAppDB.getRiskCode())||"123501".equals(tLMRiskAppDB.getRiskCode())){
        		String Querysql1="select min(insuredappage) firstage from ( "
    				+" select insuredappage,contno from lcpol where conttype='1' and appflag='1' and prtno='"+mLCPolSchema.getPrtNo()+"' "
    				+" union all "
    				+" select insuredappage,contno from lbpol where conttype='1' and appflag='1' and prtno='"+mLCPolSchema.getPrtNo()+"')";
            	SSRS tSSRS1 = new ExeSQL().execSQL(Querysql1);
            	//首次投保年龄
            	String age=tSSRS1.GetText(1,1);
            	int firstage = Integer.parseInt(age);
            	System.out.println("首次投保年龄为:"+firstage);
            	if(firstage < 45){
            		if(tAppntAge>=75){
            			mErrors.addOneError("被保人年龄不符合险种的最大续保年龄，");
            			return false;
            		}            		
            	}else{
            		if("0".equals(tLCInsuredDB.getSex())){
            			if(tAppntAge>=60){
            				mErrors.addOneError("被保人年龄不符合险种的最大续保年龄，");
            				return false;
            			}
            		}
            		if("1".equals(tLCInsuredDB.getSex())){
            			if(tAppntAge>=55){
            				mErrors.addOneError("被保人年龄不符合险种的最大续保年龄，");
            				return false;
            			}
            		}            		
            	}
        	}else{
        		if("0".equals(tLCInsuredDB.getSex())){
        			if(tAppntAge>=60){
        				mErrors.addOneError("被保人年龄不符合险种的最大续保年龄，");
        				return false;
        			}
        		}
        		if("1".equals(tLCInsuredDB.getSex())){
        			if(tAppntAge>=55){
        				mErrors.addOneError("被保人年龄不符合险种的最大续保年龄，");
        				return false;
        			}
        		}            		
        	}
        }else if(!"".equals(tMaxRnewAge)&&tMaxRnewAge!=null){//按实际年龄校验
        	
        	if(tAppntAge > Integer.parseInt(tMaxRnewAge)){
        		mErrors.addOneError("被保人年龄不符合险种的最大续保年龄，");
                return false;
        	}
        }else        
        //校验被保人年龄是否大于险种描述的最大年龄
        if(tLMRiskAppDB.getMaxInsuredAge() > 0)
        {
            if (tAppntAge < tLMRiskAppDB.getMinAppntAge())
            {
                mErrors.addOneError("被保人年龄不符合投保要求，");
                return false;
            }
            if (tAppntAge > tLMRiskAppDB.getMaxInsuredAge())
            {
                mErrors.addOneError("被保人年龄不符合投保要求，");
                return false;
            }
        }
        mLCPolSchema.setInsuredAppAge(tAppntAge);
           
        
        //2017-03-02 yukun新增
        //医保卡住院校验
        if("123201".equals(mLCPolSchema.getRiskCode()) || "123202".equals(mLCPolSchema.getRiskCode())){
//			//办理了满期不续保手续校验
//			String Sql = "select polstate from lcpol where contno = '"+mLCPolSchema.getContNo()+"' with ur";
//			SSRS tSSRS = new ExeSQL().execSQL(Sql);
//			if("".equals(tSSRS)||null==tSSRS||tSSRS.getMaxRow()<=0){
//				System.out.println("没有办理了满期不续保手续");
//			}else if(BQ.CONTSTATE_MJED.equals(tSSRS.GetText(1,1))){
//				System.out.println("办理了满期不续保手续!");
//				mErrors.addOneError("办理了满期不续保手续!");
//				return false;
//			}
			
        	//实赔或应赔已达20万
        	String Querysql="select sum(realpay) realpay from llclaimdetail a,llcase b where a.caseno=b.caseno and b.rgtstate in ('11','12') and a.contno in "
        					+" (select contno from lcpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('123201','123202') "
        					+" union all "
        					+" select contno from lbpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('123201','123202'))";
        	SSRS nSSRS = new ExeSQL().execSQL(Querysql);
        	String money=nSSRS.GetText(1,1);
        	System.out.println("money======"+money);
        	if("".equals(money)||null==money||"null".equals(money)){
        		System.out.println("保单为:"+mLCPolSchema.getContNo()+"下的被保险人未获得赔付金!");
        	}else{
        		Double getmoney = Double.parseDouble(money);
        		if(getmoney>=200000.00){
        			System.out.println("在本公司投保本保险的所有保单的保险期间内,被保险人累积已从本保险获得或应当获得的赔付金额已达到人民币20万元,不能再续保!");
        			mErrors.addOneError("在本公司投保本保险的所有保单的保险期间内,被保险人累积已从本保险获得或应当获得的赔付金额已达到人民币20万元,不能再续保!");
        			return false;
        		}
        	}
        	
        	//年龄校验
        	String Querysql1="select min(insuredappage) firstage,"
        			+" (select contno from LCRnewStateLog where newcontno = AB.contno) as contno "
        			+" from (select insuredappage,contno from lcpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('123201','123202') "
        			+" union all "
        			+" select insuredappage,contno from lbpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('123201','123202')) as AB"
        			+" group by contno order by firstage "
        			+" fetch first 1 rows only with ur ";
        	SSRS tSSRS1 = new ExeSQL().execSQL(Querysql1);
        	//首次投保年龄
        	String age=tSSRS1.GetText(1,1);
        	String contno=tSSRS1.GetText(1,2);
        	int firstage = Integer.parseInt(age);
        	System.out.println("首次投保年龄为:"+firstage);
        	//续保年龄
        	int mAppntAge = PubFun.calInterval(tLCInsuredDB.getBirthday(),mLCPolSchema.getEndDate(), "Y");
        	System.out.println("续保年龄为:"+mAppntAge);
        	if(mAppntAge>65){
        		if(firstage<55){
        			if(mLCPolSchema.getInsuredAppAge()<55){
        				System.out.println("续保无最高年龄限制!");
        			}else{
        				System.out.println("续保最高年龄65岁!");
        				mErrors.addOneError("续保最高年龄65岁!");
            			return false;
        			}
        		}else{
//        			String Querysql2="select min(insuredappage) firstage,contno from ( "
//									+" select insuredappage,contno from lcpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode='123202' "
//									+" union all "
//									+" select insuredappage,contno from lbpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode='123202') group by contno order by firstage";
//        			SSRS tSSRS2 = new ExeSQL().execSQL(Querysql2);
//                	String contno=tSSRS2.GetText(1,2);
                	//tAppntAge-mLCPolSchema.getInsuredAppAge()==0
        			if(contno.equals(mLCPolSchema.getContNo())){
        				System.out.println("续保无最高年龄限制!");
        			}else{
        				System.out.println("续保最高年龄65岁!");
        				mErrors.addOneError("续保最高年龄65岁!");
            			return false;
        			}
        		}
        	}else{
        		return true;
        	}
        }
        
        //2017-03-02 yukun新增
        //医保卡重疾校验
        if("220601".equals(mLCPolSchema.getRiskCode()) || "220602".equals(mLCPolSchema.getRiskCode())){
//			//办理了满期不续保手续校验
//			String Sql = "select polstate from lcpol where contno = '"+mLCPolSchema.getContNo()+"' with ur";
//			SSRS tSSRS = new ExeSQL().execSQL(Sql);
//			if("".equals(tSSRS)||null==tSSRS||tSSRS.getMaxRow()<=0){
//				System.out.println("没有办理了满期不续保手续");
//			}else if(BQ.CONTSTATE_MJED.equals(tSSRS.GetText(1,1))){
//				System.out.println("办理了满期不续保手续!");
//				mErrors.addOneError("办理了满期不续保手续!");
//				return false;
//			}
			
        	//被保人已患重疾
        	String Querysql="select * from llclaimdetail a,llcase b where a.caseno=b.caseno and b.rgtstate in ('11','12') and polno in (  "
							+" select polno from lcpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('220601','220602') " 
							+" union all "
							+" select polno from lbpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('220601','220602'))";
        	SSRS nSSRS = new ExeSQL().execSQL(Querysql); 
        	if(nSSRS.getMaxRow()>0){
    			System.out.println("被保人已患重疾不能再续保!");
    			mErrors.addOneError("被保人已患重疾不能再续保!");
    			return false;
    		}
        	
        	//年龄校验
        	String Querysql1="select min(insuredappage) firstage,"
        			+" (select contno from LCRnewStateLog where newcontno = AB.contno) as contno "
        			+" from (select insuredappage,contno from lcpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('220601','220602') "
        			+" union all "
        			+" select insuredappage,contno from lbpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode in ('220601','220602')) as AB "
        			+" group by contno order by firstage "
        			+" fetch first 1 rows only with ur";
        	SSRS tSSRS1 = new ExeSQL().execSQL(Querysql1);
        	//首次投保年龄
        	String age=tSSRS1.GetText(1,1);
        	String contno=tSSRS1.GetText(1,2);
        	int firstage = Integer.parseInt(age);	
        	System.out.println("首次投保年龄为:"+firstage);
        	//续保年龄
        	int mAppntAge = PubFun.calInterval(tLCInsuredDB.getBirthday(),mLCPolSchema.getEndDate(), "Y");
        	System.out.println("续保年龄为:"+mAppntAge);
        	if(mAppntAge>75){
				System.out.println("续保最高年龄75岁!");
				mErrors.addOneError("续保最高年龄75岁!");
    			return false;
        	}else if(mAppntAge>65&&mAppntAge<=75){
        		if(firstage<55){
        			if(mLCPolSchema.getInsuredAppAge()<55){
        				System.out.println("续保最高年龄75岁!");
        			}else{
        				System.out.println("续保最高年龄65岁!");
        				mErrors.addOneError("续保最高年龄65岁!");
            			return false;
        			}
        		}else{
//        			String Querysql2="select min(insuredappage) firstage,contno from ( "
//        							+" select insuredappage,contno from lcpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode='220602' "
//        							+" union all "
//        							+" select insuredappage,contno from lbpol where conttype='1' and appflag='1' and insuredno='"+mLCPolSchema.getInsuredNo()+"' and riskcode='220602') group by contno order by firstage";
//        			SSRS tSSRS2 = new ExeSQL().execSQL(Querysql2);
//        			String contno=tSSRS2.GetText(1,2);
        			//tAppntAge-mLCPolSchema.getInsuredAppAge()==0
        			if(contno.equals(mLCPolSchema.getContNo())){
        				System.out.println("续保最高年龄75岁!");
        			}else{
        				System.out.println("续保最高年龄65岁!");
        				mErrors.addOneError("续保最高年龄65岁!");
            			return false;
        			}
        		}
        	}else{
        		return true;
        	}
        }

        return true;
    }

    /**
     * 校验是否正在续保
     * @return boolean：正续保true，否则false
     */
    private boolean inReNew()
    {
        //校验险种是否正在续保
        StringBuffer checkRenewSql = null;
        checkRenewSql = new StringBuffer();
        checkRenewSql.append("select * ")
            .append("from LCRnewStateLog ")
            .append("where state != '").append(XBConst.RNEWSTATE_CONFIRM)
            .append("'  and state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("'  and polNo = '").append(mLCPolSchema.getPolNo())
            .append("' ");
        System.out.println(checkRenewSql.toString());
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet tLCRnewStateLogSet =
            tLCRnewStateLogDB.executeQuery(checkRenewSql.toString());
        if (tLCRnewStateLogSet.size() > 0)
        {
            StringBuffer errMsg = new StringBuffer();
            errMsg.append("被保人")
                .append(mLCPolSchema.getInsuredName())
                .append("的")
                .append(mLCPolSchema.getRiskCode())
                .append("险种正在续保，");
            mErrors.addOneError(errMsg.toString());
            return true;
        }

        return false;
    }

    /**
     * 校验当前险种定义是否符合续保定义：
     * 1、能不能续保
     * 2、该投保单录入的生效日期是否该险种的停办日期
     * @param tLMRiskAppDB LMRiskAppDB：险种定义信息
     * @return boolean：校验通过tue，否则false
     */
    private boolean checkPolDefinition(LMRiskAppDB tLMRiskAppDB)
    {
        //校验险种是否有可续保的标记
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(tLMRiskAppDB.getRiskCode());
        tLMRiskDB.getInfo();
        if(tLMRiskDB.getRnewFlag() == null
           || tLMRiskDB.getRnewFlag().equals("N"))
        {
            mErrors.addOneError("险种：" + mLCPolSchema.getRiskCode()
                                + "不能续保(险种定义表查询失败)，");
            return false;
        }

        //校验险种是否已停办
        if (tLMRiskAppDB.getEndDate() != null
            && tLMRiskAppDB.getEndDate().trim().equals(""))
        {
            if (PubFun.calInterval(tLMRiskAppDB.getEndDate(),
                                   mLCPolSchema.getEndDate(), "D") > 0)
            {
                mErrors.addOneError("该投保单录入的生效日期超过该险种的停办日期");
                return false;
            }
        }


        return true;
    }

    /**
     * 对保单数据进行处理
     * 续保采用备份险种信息相关，并更改备份信息的相关流水号，如保单号险种号等
     * 而原险种信息的流水号将不做变更
     * 集体保单号，个人保单号等在对保单级别上做续保时更改
     * @return boolean：处理成功true，否则false
     */
    private boolean dealData()
    {
        map = new MMap();

        if(!preRnewPolInfo())
        {
            return false;
        }

        return true;
    }

    private boolean preRnewPolInfo()
    {
        mPolInfoVData = new VData();

        //准备重算保费所需的相关数据
        if(!preCalculteData())
        {
            return false;
        }

        //重算保费
        mPolInfoVData.add(mGlobalInput);
        PRnewPolBL tPRnewPolBL = new PRnewPolBL();
        MMap tMMap = tPRnewPolBL.getSubmitMap(mPolInfoVData, mOperate);
        if(tMMap == null || tPRnewPolBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tPRnewPolBL.mErrors);
            return false;
        }
        map.add(tMMap);

        //得到重算后的险种信息
        newLCPolSchema = (LCPolSchema) map
                         .getObjectByObjectName("LCPolSchema", 0);

        //续保状态
        LCRnewStateLogSchema tLCRnewStateLogSchema = preLCRnewStateLog();
        map.put(tLCRnewStateLogSchema, "INSERT");

        mNewProposalNo = newLCPolSchema.getPolNo();

        return true;
    }

    /**
     * 正被计算所需信息
     * @return boolean：准备成功true，否则false
     */
    private boolean preCalculteData()
    {
        //准备需险种信息
        preLCPol();
        mPolInfoVData.add(mLCPolSchema);
        //得到责任信息
        LCDutySet tLCDutySet = preLCDuty();
        if(tLCDutySet == null)
        {
            return false;
        }
        mPolInfoVData.add(tLCDutySet);

        //不能通过新契约计算出来的保费项，如加费纪录等
        LCPremSet tLCPremSet = preLCPrem();
        mPolInfoVData.add(tLCPremSet);

        return true;
    }

    /**
     * 为续保准备需险种信息
     * 准备责任信息
     * @return LCDuty：准备完毕的责任信息
     * @return boolean
     */
    private boolean preLCPol()
    {
        mLCPolSchema.setLeavingMoney(0);
        mLCPolSchema.setCValiDate(mLCPolSchema.getEndDate());
        mLCPolSchema.setSpecifyValiDate("Y"); //生效日期（指定）
        mLCPolSchema.setEndDate("");
        mLCPolSchema.setSignDate("");
        mLCPolSchema.setSignTime("");
        mLCPolSchema.setFirstPayDate("");
//        mLCPolSchema.setInsuredAppAge(PubFun.calInterval(
//        		mLCPolSchema.getInsuredBirthday(),
//    			mLCPolSchema.getPolApplyDate(), "Y"));

        if (mLCPolSchema.getPayLocation() == null)
        { //多责任项
            mLCPolSchema.setPayLocation("");
        }

        return true;
    }

    /**
     * 准备责任信息
     * @return LCDuty：准备完毕的责任信息
     */
    private LCDutySet preLCDuty()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from lcduty ")
            .append("where polno = '")
            .append(mLCPolSchema.getPolNo())
            .append("' ");
        System.out.println(sql.toString());
        LCDutyDB tLCDutyDB = new LCDutyDB();
        LCDutySet tLCDutySet = tLCDutyDB.executeQuery(sql.toString());
        if(tLCDutySet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种"
                                + mLCPolSchema.getRiskCode() +"的责任信息。");
            return null;
        }

        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            //得到续保核保后的保额档次
            if(mLCPolSchemaUWed != null)
            {
                tLCDutySet.get(i).setMult(mLCPolSchemaUWed.getMult());
                tLCDutySet.get(i).setAmnt(mLCPolSchemaUWed.getAmnt());
            }
            else if(mLPPolSchemaUWed != null){
            	System.out.println("我就不信了不信了不信了");
            	tLCDutySet.get(i).setMult(mLPPolSchemaUWed.getMult());
                tLCDutySet.get(i).setAmnt(mLPPolSchemaUWed.getAmnt());
            }

            tLCDutySet.get(i).setEndDate("");
            tLCDutySet.get(i).setFirstPayDate("");
            tLCDutySet.get(i).setPaytoDate("");
        }

        return tLCDutySet;
    }

    /**
     * 得到非计算得到的保费项信息
     * @return LCPremSet：准备完毕的缴费项信息
     */
    private LCPremSet preLCPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from lcprem ")
            .append("where polno = '")
            .append(mLCPolSchema.getPolNo())
            .append("'  and payPlanCode like '000000%' ")
            .append("   and (payEndDate is null or payEndDate >= ")
            .append(" (select enddate from lcpol where polno ='"+mLCPolSchema.getPolNo()+"')").append(") ");
        System.out.println("" + sql.toString());
        LCPremDB tLCPremDB = new LCPremDB();
        LCPremSet tLCPremSet = tLCPremDB.executeQuery(sql.toString());

        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            //待定
        }

        return tLCPremSet;
    }

    /**
     * 得到保费项与帐户的关联信息
     * @return LCPremToAccSet：准备完毕的保费项与账户关联信息
     */
    private LCPremToAccSet preLCPremToAcc()
    {
        LCPremToAccDB tLCPremToAccDB = new LCPremToAccDB();
        tLCPremToAccDB.setPolNo(mLCPolSchema.getPolNo());
        LCPremToAccSet tLCPremToAccSet = tLCPremToAccDB.query();

        return tLCPremToAccSet;
    }

    /**
     * 续保日志信息
     * @return LCRnewStateLogSchema：准备完毕的续保日至信息
     */
    private LCRnewStateLogSchema preLCRnewStateLog()
    {

        LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
        tLCRnewStateLogSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLCRnewStateLogSchema.setNewGrpContNo(newLCPolSchema.getGrpContNo());
        tLCRnewStateLogSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLCRnewStateLogSchema.setNewGrpPolNo(newLCPolSchema.getGrpPolNo());
        tLCRnewStateLogSchema.setContNo(mLCPolSchema.getContNo());
        tLCRnewStateLogSchema.setNewContNo(newLCPolSchema.getContNo());
        tLCRnewStateLogSchema.setPolNo(mLCPolSchema.getPolNo());
        tLCRnewStateLogSchema.setNewPolNo(newLCPolSchema.getProposalNo());
        tLCRnewStateLogSchema.setPrtNo(mLCPolSchema.getPrtNo());
        tLCRnewStateLogSchema.setRenewCount(newLCPolSchema.getRenewCount());
        tLCRnewStateLogSchema.setRiskFlag(mLCPolSchema.getMainPolNo()
                                          .equals(mLCPolSchema.getPolNo())
                                          ? "M" : "S");
        //满期结算（特许险种）不需要核保和交费
        if(mOperate.equals(com.sinosoft.lis.bq.BQ.EDORTYPE_MJ))
        {
            tLCRnewStateLogSchema.setState(XBConst.RNEWSTATE_UNCONFIRM);
        }
        else
        {
            tLCRnewStateLogSchema.setState(XBConst.RNEWSTATE_APP);
        }
        tLCRnewStateLogSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCRnewStateLogSchema);
        tLCRnewStateLogSchema.setPaytoDate(newLCPolSchema.getPaytoDate());
        tLCRnewStateLogSchema.setMainRiskStatus("0");

        return tLCRnewStateLogSchema;
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括：LCPolSchema, GlobalInput
     * 不需要在上一层取出这些信息进行变更
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return boolean：操作成功true：否则false
     */
    public boolean submitData(VData inputData, String operate)
    {
        return true;
    }

    /**
     * 得到投保单号
     * @return String：投保单号
     */
    public String getNewProposalNo()
    {
        return mNewProposalNo;
    }

    /**
     * 调试方法
     * @param args String[]，null
     */
    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "qulq";
        mGlobalInput.ManageCom = "86";
        TransferData mTransferData = new TransferData();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo("13001311378");
        LCPolSet mLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= mLCPolSet.size(); i++) {
            VData tVData = new VData();
            tVData.clear();
            tVData.add(mGlobalInput);
            tVData.add(mLCPolSet.get(i));
            mTransferData.setNameAndValue(XBConst.IS_GRP, XBConst.IS_GRP);
            tVData.add(mTransferData);

            PRnewPolAppBL prnewpolappbl = new PRnewPolAppBL();
            prnewpolappbl.getSubmitMap(tVData, "");
        }

    }
}
