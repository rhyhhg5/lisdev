package com.sinosoft.lis.xb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class OmnipDueVerifyUI {
	public CErrors mErrors = new CErrors();
	
	public OmnipDueVerifyUI(){
		
	}
	public boolean submitData(VData data, String operate)
	    {
	        OmnipDueVerifyBL bl = new OmnipDueVerifyBL();
	        if(!bl.submitData(data, operate))
	        {
	            mErrors = bl.mErrors;
	            return false;
	        }
	        return true;
	    }
}
