package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 团单续保确认，更新保单费用信息、相关起止日期，并换号
 * </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author liuzhenjiang
 * @version 1.1
 */
public class GRnewGrpContSignBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private LCGrpContSchema mLCGrpContSchema = null;  //续保前的团单信息
    private LCGrpPolSet mLCGrpPolSet = null;  //续保前的险种信息
    private LCGrpPolSet mNewLCGrpPolSet = null; //处理后的险种信息
    private GlobalInput mGlobalInput = null;
    private String mNewGrpContNo = null;  //新团单号
    private String mNewGrpPolNo = null;  //新险种号
    private TransferData mTransferData = null;

    private MMap map = null;  //存储处理后的数据

//    private double mPrem = 0;
//    private double mSumPrem = 0;
//    private double mAmnt = 0;
    public GRnewGrpContSignBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        if(getSubmitMap(inputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("保存数据失败。");
            System.out.println(tPubSubmit.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    /**
     * 提交操作的外部方法
     * @param data VData
     * @param operate String
     * @return MMap
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in GRnewContSignBL->getSubmitMap");
        if(!getInputData(inputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCGrpPolSet = (LCGrpPolSet) inputData
                       .getObjectByObjectName("LCGrpPolSet", 0);
        mTransferData = (TransferData) inputData
                        .getObjectByObjectName("TransferData", 0);
        if(mGlobalInput == null || mLCGrpPolSet == null
           || mLCGrpPolSet.size() == 0 || mTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        if(!checkLCGrpPol())
        {
            return false;
        }

        if(!checkLCGrpCont())
        {
            return false;
        }

        return true;
    }

    private boolean checkLCGrpCont()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpPolSet.get(1).getGrpContNo());
        if(!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("查询团单出错。");
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();

        return true;
    }

    /**
     * 进行续保确认
     * @return boolean
     */
    private boolean dealData()
    {
        map = new MMap();
        if(!dealAllLCCont())
        {
            return false;
        }

        if(!dealAllGrpPol())
        {
            return false;
        }

        if(!dealLCGrpCont())
        {
            return false;
        }

        if(!changeGrpContNo())
        {
            return false;
        }

        if(!changeGrpAppntNum())
        {
            return false;
        }

        if(!reNewFee())
        {
            return false;
        }

        return true;
    }

    private boolean checkLCGrpPol()
    {
        for(int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
            tLCGrpPolDB.setGrpPolNo(mLCGrpPolSet.get(i).getGrpPolNo());
            if(!tLCGrpPolDB.getInfo())
            {
                mErrors.addOneError("没有查询到团险信息。");
                return false;
            }

            mLCGrpPolSet.set(i, tLCGrpPolDB.getSchema());
        }

        return true;
    }

    /**
     * 对团单下的个单进行续保确认
     * @return boolean
     */
    private boolean dealAllLCCont()
    {
        //得到该团单下的个单
        LCContSet tLCContSet = getLCCont();
        if(tLCContSet == null)
        {
            return false;
        }

        //得到该团单需要续保确认的险种
        String grpPolNos = "";
        for(int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            grpPolNos += "'" + mLCGrpPolSet.get(i).getGrpPolNo() + "',";
        }
        grpPolNos = grpPolNos.substring(0, grpPolNos.length() - 1);

        //对团单下的个单进行续保确认
        VData data = new VData();

        PRnewContSignBL tPRnewContSignBL = new PRnewContSignBL();
        PubSubmit p = new PubSubmit();
        tPRnewContSignBL.setPubSubmit(p);
        VData v = new VData();
        tPRnewContSignBL.setVData(v);
        for(int i = 1; i <= tLCContSet.size(); i++)
        {
            try{
                System.out.println("\n\n个单: " + i);
                StringBuffer sql = new StringBuffer();
                sql.append("select a.* ")
                    .append("from LCPol a, LCRnewStateLog b ")
                    .append("where a.polNo = b.polNo")
                    .append("   and b.state = '")
                    .append(XBConst.RNEWSTATE_UNCONFIRM)
                    .append("'   and a.contNo = '")
                    .append(tLCContSet.get(i).getContNo())
                    .append("'  and a.grpPolNo in (").append(grpPolNos).append(
                    ") ");
                System.out.println(sql.toString());
                LCPolDB tLCPolDB = new LCPolDB();
                LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql.toString());
                //该个单没有需要续保确认的险种
                if (tLCPolSet.size() == 0)
                {
                    continue;
                }

                data.clear();
                data.add(mGlobalInput);
                data.add(tLCPolSet);

                data.add(tLCContSet.get(i));  //Added by Yalin
                mTransferData.setNameAndValue(XBConst.IS_GRP,XBConst.IS_GRP);

                data.add(mTransferData);

                //调用个单续保确认进行处理
                //小事务提交
//                if(!tPRnewContSignBL.submitData(data, ""))
//                {
//                    mErrors.copyAllErrors(tPRnewContSignBL.mErrors);
//                    mErrors.addOneError("处理个单" + tLCPolSet.get(1).getContNo()
//                                        + "续保确认时出错");
//                    return false;
//                }

                //为了将大事务提交改造为小事务（按个单处理）提交，注掉以下代码
                MMap tMMap = tPRnewContSignBL.getSubmitData(data, "");
                if (tMMap == null || tPRnewContSignBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tPRnewContSignBL.mErrors);
                    return false;
                }
                map.add(tMMap);
            }catch(Exception e)
            {
                e.printStackTrace();
                mErrors.addOneError("续保确认失败");
                return false;
            }
        }

        return true;
    }

    /**
     * 得到需要处理的个单
     * 由于采用小事务按个单提交，可能已有部分个单续保确认
     * @return LCContSet
     */
    private LCContSet getLCCont()
    {
        StringBuffer sql = new StringBuffer();
        sql.append(" select distinct a.* ")
            .append("from LCCont a, LCRnewStateLog b ")
            .append("where a.contNo = b.contNo ")
            .append("   and a.appFlag = '")
            .append(com.sinosoft.lis.bq.BQ.APPFLAG_SIGN)
            .append("'   and a.stateFlag = '")
            .append(com.sinosoft.lis.bq.BQ.STATE_FLAG_SIGN)
            .append("'  and b.state = '").append(XBConst.RNEWSTATE_UNCONFIRM)
            .append("'  and a.grpContNo = '")
            .append(mLCGrpContSchema.getGrpContNo()).append("' ");
        System.out.println(sql.toString());

        //得到多有个单信息
        LCContDB tLCContDB = new LCContDB();
//        tLCContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
//        tLCContDB.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_SIGN);
        LCContSet tLCContSet = tLCContDB.executeQuery(sql.toString());
        if (tLCContSet.size() == 0)
        {
            String getOneRenewedPol = "  select newPolNo "
                                      + "from LCRnewStateLog "
                                      + "where grpContNo = '"
                                      + mLCGrpContSchema.getGrpContNo()
                                      + "'  and state = '"
                                      + XBConst.RNEWSTATE_CONFIRM + "' ";
            ExeSQL e = new ExeSQL();
            String newPolNo = e.getOneValue(getOneRenewedPol);
            if (newPolNo.equals("") || newPolNo.equals("null"))
            {
                mErrors.addOneError("没有查询到相关的个单信息。");
                return null;
            }
            System.out.println("GRnewContSignBL->getLCCont:没有需要续保确认的个单");
        }
        return tLCContSet;
    }

    /**
     * 对团单险种进行续保确认
     * @return boolean
     */
    private boolean dealAllGrpPol()
    {
        //得到所有已续保处理的个险信息
        Object[] tLCPolInfo = map.getAllObjectByObjectName("LCPolSchema", 0);

//        mNewLCGrpPolSet = new LCGrpPolSet();
        for (int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            LCGrpPolSchema needRnewGrpPolSchema
                = getNeedRnewGrpPol(mLCGrpPolSet.get(i).getGrpPolNo());  //需要续保确认的数据

                this.mNewGrpPolNo = PubFun1.CreateMaxNo("GrpPolNo",
                    mLCGrpContSchema.getAppntNo());
                System.out.println("mNewGrpPolNo: " + mNewGrpPolNo);



                //将相关信息的团险号更换成新号
                String updateSql = "";
                String[] polTables = {"LCPol"};
                String condition = "where grpPolNo = '"
                                   + needRnewGrpPolSchema.getGrpPolNo() + "' ";  //此处的团险号是GrpProposalNo
                for (int j = 0; j < polTables.length; j++)
                {
                    updateSql = "  update " + polTables[j]
                          + " set grpPolNo = '" + this.mNewGrpPolNo + "' "
                          + condition;
                    map.put(updateSql, "UPDATE");
                }
                String[] accTables ={"LCInsureAcc",
                                     "LCInsureAccClass", "LCInsureAccTrace"};
                 for (int j = 0; j < accTables.length; j++)
                 {
                     updateSql = "update " + accTables[j]
                           + " set grpPolNo = '" + mNewGrpPolNo + "' "
                           + "where polNo in "
                           + "    (select newPolNo "
                           + "    from LCRnewStateLog "
                           + "    where grpContNo = '"
                           + this.mLCGrpContSchema.getGrpContNo()+ "') "
                           + "   and grpPolNo = '"
                           + needRnewGrpPolSchema.getGrpPolNo() + "' ";
                     map.put(updateSql, "UPDATE");
                }

                //更新保费信息，必须在更新个险和团险的团体险种号后执行，因为此处用的是新号
                StringBuffer sql = new StringBuffer();
                sql.append("update LCGrpPol ")
                    .append("set grpPolNo = '")
                    .append(mNewGrpPolNo).append("', ")
                    .append("appFlag = '")
                    .append(com.sinosoft.lis.bq.BQ.APPFLAG_INIT).append("', ")
                    .append("prem = (select sum(prem) from LCPol where grpPolNo = '")
                    .append(this.mNewGrpPolNo).append("'), ")
                    .append("sumPrem = (select sum(sumPrem) from LCPol where grpPolNo = '")
                    .append(this.mNewGrpPolNo).append("'), ")
                    .append("amnt = (select sum(amnt) from LCPol where grpPolNo = '")
                    .append(this.mNewGrpPolNo).append("'), ")
                    .append("cValiDate = (select min(cValiDate) from LCPol where grpPolNo = '")
                    .append(this.mNewGrpPolNo).append("'), ")
                    .append("payToDate = (select max(payToDate) from LCPol where grpPolNo = '")
                    .append(this.mNewGrpPolNo).append("'), ")
                    .append("payEndDate = (select max(payEndDate) from LCPol where grpPolNo = '")
                    .append(this.mNewGrpPolNo).append("'), ")
                    .append("operator = '")
                    .append(mGlobalInput.Operator).append("', ")
                    .append("modifyDate = '")
                    .append(PubFun.getCurrentDate()).append("', ")
                    .append("modifyTime = '")
                    .append(PubFun.getCurrentTime()).append("' ")
                    .append("where grpPolNo = '")
                    .append(needRnewGrpPolSchema.getGrpPolNo())
                    .append("' ");
                map.put(sql.toString(), "UPDATE");


                updateSql = "  update LCRnewStateLog "
                      + "set newGrpPolNo = '" + mNewGrpPolNo + "' "
                      + "where newGrpPolNo = '"
                      + needRnewGrpPolSchema.getGrpPolNo() + "' ";
                map.put(updateSql, "UPDATE");
//            }
        }
        //map.put(mNewLCGrpPolSet, "INSERT");

        return true;
    }

    /**
     * 得到续保前险种号grpPolNo对应的续保申请数据
     * @param grpPolNo String：续保前险种号
     * @return LCGrpPolSchema
     */
    private LCGrpPolSchema getNeedRnewGrpPol(String grpPolNo)
    {
        //通过关联表得到需要续保处理的团险信息
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from LCGrpPol ")
            .append("where grpPolNo = ")
            .append("    (select distinct newGrpPolNo ")
            .append("    from LCRnewStateLog ")
            .append("    where grpPolNo = '").append(grpPolNo).append("' ")
            .append("        and state = '")
//            .append(XBConst.RNEWSTATE_CONFIRM)
//            为了将大事务拆分为按个单提交，注释以下语句
            .append(XBConst.RNEWSTATE_UNCONFIRM)
            .append("') ");
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet needRnewGrpPolSet
            = tLCGrpPolDB.executeQuery(sql.toString());
        if(needRnewGrpPolSet.size() == 0 || needRnewGrpPolSet.size() > 1)
        {
            mErrors.addOneError("待续保确认的险种状态不正确。");
            return null;
        }

        return needRnewGrpPolSet.get(1);
    }

    private boolean dealLCGrpCont()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select newGrpContNo ")
            .append("from LCRnewStateLog ")
            .append("where grpContNo = '")
            .append(mLCGrpContSchema.getGrpContNo())
            .append("'  and state = '")
//            .append(XBConst.RNEWSTATE_CONFIRM)
//            为了将大事务拆分为按个单提交，注释以下语句
            .append(XBConst.RNEWSTATE_UNCONFIRM)
            .append("' ");
        ExeSQL e = new ExeSQL();
        String newGrpContNoInLog = e.getOneValue(sql.toString());
        if(newGrpContNoInLog.equals("") || newGrpContNoInLog.equals("null"))
        {
            mErrors.addOneError("没有查询到团单号为"
                                + mLCGrpContSchema.getGrpContNo()
                                + "的续保信息。");
            return false;
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(newGrpContNoInLog);
        if(!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("没有查询到需续保确认的团单号为"
                                + mLCGrpContSchema.getGrpContNo()
                                + "的续保数据。");
            System.out.println(tLCGrpContDB.mErrors.getErrContent());
            return false;
        }

        LCGrpContSchema tLCGrpContSchema = tLCGrpContDB.getSchema();
        map.put(tLCGrpContSchema.getSchema(), "DELETE");

        mNewGrpContNo = PubFun1.CreateMaxNo("GRPCONTNO",
                                            mLCGrpContSchema.getAppntNo());
        System.out.println("mNewGrpContNo: " + mNewGrpContNo);
        tLCGrpContSchema.setGrpContNo(this.mNewGrpContNo);
        tLCGrpContSchema.setSignDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setSignTime(PubFun.getCurrentTime());
        tLCGrpContSchema.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_INIT);
        tLCGrpContSchema.setOperator(mGlobalInput.Operator);
        tLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCGrpContSchema, "INSERT");

        return true;
    }

    /**
     * 更换新团单号
     * @return boolean
     */
    private boolean changeGrpContNo()
    {
        String queryNewGrpContNoInLog
            = "  select newGrpContNo "
              + "from LCRnewStateLog "
              + "where grpContNo = '"
              + mLCGrpContSchema.getGrpContNo()
              + "'   and state = '"
//              + XBConst.RNEWSTATE_CONFIRM + "' ";
//              为了将大事务拆分为按个单提交，注释以下语句
              + XBConst.RNEWSTATE_UNCONFIRM + "' ";
        ExeSQL e = new ExeSQL();
        String newGrpContNoInLog = e.getOneValue(queryNewGrpContNoInLog);
        if(newGrpContNoInLog.equals("") || newGrpContNoInLog.equals("null"))
        {
            mErrors.addOneError("续保状态查询出错。");
            return false;
        }
        String[] polTables =
                             {"LCGrpPol", "LCPol", "LCPrem", "LCGet"};
        String condition = "where grpContNo = '"
                           + newGrpContNoInLog + "' ";
        String sql = "";
        for(int i = 0; i < polTables.length; i++)
        {
            sql = "  update " + polTables[i]
                         + " set GrpContNo = '" + this.mNewGrpContNo + "', "
                         + "operator = '" + mGlobalInput.Operator + "', "
                         + "makeDate = '"  + PubFun.getCurrentDate() + "', "
                         + "makeTime = '" + PubFun.getCurrentTime() + "' "
                         + condition;
            map.put(sql.toString(), "UPDATE");
        }

        String[] accTables = {"LCInsureAcc",
                             "LCInsureAccClass", "LCInsureAccTrace"};
        for(int i = 0; i < accTables.length; i++)
        {
            sql = "update " + accTables[i]
                  + " set grpContNo = '" + mNewGrpContNo + "', "
                  + "operator = '" + mGlobalInput.Operator + "', "
                  + "makeDate = '"  + PubFun.getCurrentDate() + "', "
                  + "makeTime = '" + PubFun.getCurrentTime() + "' "
                  + "where polNo in "
                  + "    (select newPolNo "
                  + "    from LCRnewStateLog "
                  + "    where grpContNo = '"
                  + this.mLCGrpContSchema.getGrpContNo() + "')";
            map.put(sql, "UPDATE");
        }


        String[] contTables = {"LCCont"};
        for(int i = 0; i < contTables.length; i++)
        {
            sql = "  update " + contTables[i]
                         + "   set grpContNo = '" + this.mNewGrpContNo + "', "
                         + "operator = '" + mGlobalInput.Operator + "', "
                         + "makeDate = '"  + PubFun.getCurrentDate() + "', "
                         + "makeTime = '" + PubFun.getCurrentTime() + "' "
                         + "where grpContNo = '"
                         + newGrpContNoInLog + "' ";
            map.put(sql.toString(), "UPDATE");
        }

        sql = "  update LCRnewStateLog "
              + "set newGrpContNo = '" + mNewGrpContNo + "', "
              + "state = '" + XBConst.RNEWSTATE_CONFIRM + "', "
              + "operator = '" + mGlobalInput.Operator + "', "
              + "makeDate = '"  + PubFun.getCurrentDate() + "', "
              + "makeTime = '" + PubFun.getCurrentTime() + "' "
              + "where newGrpContNo = '" + newGrpContNoInLog + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 团体客户投保次数+1
     * @return boolean
     */
    private boolean changeGrpAppntNum()
    {
        LDGrpDB tLDGrpDB = new LDGrpDB();
        tLDGrpDB.setCustomerNo(mLCGrpContSchema.getAppntNo());
        if(!tLDGrpDB.getInfo())
        {
            mErrors.addOneError("没有查询到客户号为"
                                + tLDGrpDB.getCustomerNo() + "的团体客户信息。");
            return false;
        }
        tLDGrpDB.setGrpAppntNum(tLDGrpDB.getGrpAppntNum() + 1);
        map.put(tLDGrpDB.getSchema(), "UPDATE");

        return true;
    }

    /**
     * 更新保单费用信息
     * @return boolean
     */
    private boolean reNewFee()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("update LCGrpCont ")
            .append("set prem = (select sum(prem) from LCPol where grpContNo = '")
            .append(this.mNewGrpContNo).append("'), ")
            .append("sumPrem = (select sum(sumPrem) from LCPol where grpContNo = '")
            .append(this.mNewGrpContNo).append("'), ")
            .append("amnt = (select sum(amnt) from LCPol where grpContNo = '")
            .append(this.mNewGrpContNo).append("') ")
            .append("where grpContNo = '")
            .append(this.mNewGrpContNo)
            .append("' ");
        map.put(sql.toString(), "UPDATE");

        return true;
    }

    public static void main(String[] args)
    {
        LCGrpPolDB db = new LCGrpPolDB();
        db.setGrpContNo("00113761000001");
        LCGrpPolSet set = db.query();

        GlobalInput g = new GlobalInput();
        g.Operator = "pa0001";
        g.ComCode = "86";

        TransferData t = new TransferData();
        t.setNameAndValue("rate", "0.1");

        VData data = new VData();
        data.add(set);
        data.add(g);
        data.add(t);

        long a = System.currentTimeMillis();
        GRnewGrpContSignBL bl = new GRnewGrpContSignBL();

        MMap tMMap = bl.getSubmitMap(data, "");
        if(tMMap == null && bl.mErrors.needDealError())
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        System.out.println("生成续保数据花费："
                           + (System.currentTimeMillis() - a)/1000 + "秒");
    }
}
