package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewPolAppUI
{
    /**错误的容器*/
    public CErrors mErrors = null;

    public PRnewPolAppUI()
    {
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        PRnewPolAppBL tPRnewPolAppBL = new PRnewPolAppBL();
        if(!tPRnewPolAppBL.submitData(inputData, operate))
        {
            mErrors = tPRnewPolAppBL.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        PRnewPolAppUI prnewpolappui = new PRnewPolAppUI();
    }
}
