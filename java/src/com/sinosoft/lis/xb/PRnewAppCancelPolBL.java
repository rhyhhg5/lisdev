package com.sinosoft.lis.xb;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LPUWMasterSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.vschema.LCRnewStateLogSet;
import com.sinosoft.lis.db.LCPolDB;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 删除续保终止险种的临时数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class PRnewAppCancelPolBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private LPUWMasterSchema mLPUWMasterSchema = null;

    private MMap map = new MMap();

    public PRnewAppCancelPolBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括：LPUWMasterSchema, GlobalInput
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return boolean
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in PRnewAppCancelPolBL->getSubmitMap");
        if(!getInputData(inputData))
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 对保单数据进行处理
     * 删除险种临时数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealData()
    {
        String[] tables =
            {"LCPol", "LCDuty", "LCPrem", "LCGet"};

        for(int i = 0; i < tables.length; i++)
        {
            String sql = "delete from " + tables[i] + " "
                         + "where polNo = '"
                         + mLPUWMasterSchema.getPolNo() + "' ";
            map.put(sql, "DELETE");
        }
        map.put("update LCRnewStateLog "
                + "set state = '" + XBConst.RNEWSTATE_XBSTOP + "' "
                + " where newPolNo = '" + mLPUWMasterSchema.getPolNo() + "' ",
                "UPDATE");

        //续保状态该为续保终止
//        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
//        tLCRnewStateLogDB.setNewPolNo(mLPUWMasterSchema.getPolNo());
//        LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
//        if(tLCRnewStateLogSet.size() == 0)
//        {
//            mErrors.addOneError("没有查询到续保日志记录");
//            return false;
//        }
//        tLCRnewStateLogSet.get(1).setState(XBConst.RNEWSTATE_XBSTOP);
//        map.put(tLCRnewStateLogSet.get(1), "UPDATE");

//        LCPolDB tLCPolDB = new LCPolDB();
//        tLCPolDB.setPolNo(tLCRnewStateLogSet.get(1).getPolNo());
//        if(!tLCPolDB.getInfo())
//        {
//            mErrors.addOneError("查询原险种信息出错"
//                                + tLCRnewStateLogSet.get(1).getPolNo());
//            return false;
//        }
//        tLCPolDB.setPolState("");
//        tLCPolDB.setOperator(mGlobalInput.Operator);
//        tLCPolDB.setModifyDate(PubFun.getCurrentDate());
//        tLCPolDB.setModifyTime(PubFun.getCurrentTime());
//        map.put(tLCPolDB.getSchema(), "UPDATE");

        return true;
    }


    /**
     * 得到保单传入的数据
     * @param inputData VData: 包括：LCPolSchema, GlobalInput
     * @return boolean：获取成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLPUWMasterSchema = (LPUWMasterSchema) inputData
                            .getObjectByObjectName("LPUWMasterSchema", 0);

        if(mGlobalInput == null || mLPUWMasterSchema == null
            || mLPUWMasterSchema.getPolNo() == null
            || mLPUWMasterSchema.getPolNo().equals(""))
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }
    public static void main(String[] args)
    {
        PRnewAppCancelPolBL prnewappcancelpolbl = new PRnewAppCancelPolBL();
    }
}
