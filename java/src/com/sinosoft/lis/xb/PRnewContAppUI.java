package com.sinosoft.lis.xb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewContAppUI
{
    public CErrors mErrors = null;

    public PRnewContAppUI()
    {
    }

    /**
     * 提交外部操作的方法
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        PRnewContAppBL tPRnewContAppBL = new PRnewContAppBL();
        if(!tPRnewContAppBL.submitData(inputData, operate))
        {
            mErrors = tPRnewContAppBL.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        PRnewContAppUI prenewcontappui = new PRnewContAppUI();
    }
}
