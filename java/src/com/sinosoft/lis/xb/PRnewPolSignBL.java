package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import java.util.*;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 险种续保确认，更换相关号码
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewPolSignBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private LCPolSchema mLCPolSchema = null;  //新生成的险种信息
    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    private boolean isGrpCont = false;

    private FDate mFDate = new FDate();

    private double rate = 0;

    private String mPolNo = null;  //续保前的险种号
    private String mNewPolNo = null;  //新个险号

    private Date mFirstPayDate = null;

    private MMap map = null;  //存储处理后的数据

    public PRnewPolSignBL()
    {
    }

    /**
     * 外部操作的提交方法，处理业务逻辑
     * @param inputData VData：包括：
     * 1、GlobalInput：操作员信息
     * 2、LCPolSchema：需要续保确认的险种
     * 3、TransferData：存储特殊数据，需保存是否团单标志IS_GRP
     * @param operate String：操作方式，此为""
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData inputDate, String operate)
    {
        return true;
    }

    /**
     * 外部操作的提交方法，处理业务逻辑
     * @param inputData VData：包括：
     * 1、GlobalInput：操作员信息
     * 2、LCPolSchema：需要续保确认的险种
     * 3、TransferData：存储特殊数据，需保存是否团单标志IS_GRP
     * @param operate String：操作方式，此为""
     * @return MMap：返回出处理后的数据集合
     */
    public MMap getSubmitData(VData inputDate, String operate)
    {
        System.out.println("Now in PRnewPolSignBL->getSubmitMap");
        if(!getInputData(inputDate))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    private boolean dealData()
    {
        if(!createNewNo())
        {
            return false;
        }

        map = new MMap();
        if(!dealPolInfo())
        {
            return false;
        }

        return true;
    }

    /**
     * 获取外部传入的数据
     * @param inputData VData：包括：
     * 1、GlobalInput：操作员信息
     * 2、LCPolSchema：需要续保确认的险种
     * 3、TransferData：存储特殊数据，需保存是否团单标志IS_GRP
     * @return boolean：获取成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCPolSchema = (LCPolSchema) inputData
                       .getObjectByObjectName("LCPolSchema", 0);
        mTransferData = (TransferData) inputData
                        .getObjectByObjectName("TransferData", 0);
        String strGrpCont = (String) mTransferData.getValueByName(XBConst.IS_GRP);
        isGrpCont = XBConst.IS_GRP.equals(strGrpCont);

        if(mGlobalInput == null || mLCPolSchema == null
            || mTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        System.out.println("...Now sign pol: " + mLCPolSchema.getPolNo());

        return true;
    }

    private boolean checkData()
    {
        if (!isGrpCont)
        {
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCPolSchema.getPolNo()); //实际上是proposalNo
            if (!tLCPolDB.getInfo())
            {
                mErrors.addOneError("没有查询到需要续保的个险险种信息。");
                return false;
            }
            mLCPolSchema = tLCPolDB.getSchema();
        }
        //校验续保状态
        if (!checkRnewPolStatus(mLCPolSchema.getPolNo()))
        {
            return false;
        }
        //校验保全，校验理赔，校验续期，校验财务数据

        //对于帐户型险种，若没有往帐户再存钱，那么可以FirstPayDate = 险种生效日期，否则需要从财务表里得到
        mFirstPayDate = mFDate.getDate(mLCPolSchema.getCValiDate());

        return true;
    }

    /**
     * 检查同印刷号的续保投保单状态
     * @param tPrtNo
     * @return
     */
    private boolean checkRnewPolStatus(String proposalNo)
    {
        //保存全局续保状态日志纪录
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogDB.setNewPolNo(proposalNo);
        tLCRnewStateLogDB.setState(XBConst.RNEWSTATE_UNCONFIRM);  //待续保确认
        LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
        if (tLCRnewStateLogSet.size() == 0)
        {
            CError.buildErr(this, "险种不是待续保确认状态！");
            return false;
        }
        mPolNo = tLCRnewStateLogSet.get(1).getPolNo();

        return true;
    }

    /**
     * 生成新的保单号，险种号
     * @return boolean
     */
    private boolean createNewNo()
    {
        //险种号
        String tLimit = PubFun.getNoLimit(mLCPolSchema.getManageCom());
        this.mNewPolNo = PubFun1.CreateMaxNo("POLNO", tLimit);
        System.out.println("mNewPolNo: " + mNewPolNo);
        return true;
    }

    /**
     * 将待续保确认的险种相关信息换号
     * @return boolean
     */
    private boolean dealPolInfo()
    {
        LCPremSet dealtLCPremSet = dealLCPrem();
        if(dealtLCPremSet == null)
        {
            return false;
        }

        if(!dealLCGet())
        {
            return false;
        }

        LCDutySet dealtLCDutySet = dealLCDuty(dealtLCPremSet);
        if(dealtLCDutySet == null)
        {
            return false;
        }

        if(!dealAcc())
        {
            return false;
        }

        if(!dealLCPol(dealtLCDutySet))
        {
            return false;
        }
/*
        if(!dealFinaceData())
        {
            return false;
        }
*/
        if(!dealRnewLog())
        {
            return false;
        }

        return true;
    }

    private boolean dealFinaceData()
    {
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPolNo(mLCPolSchema.getPolNo());
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if(tLJAPayPersonSet.size() > 0 && mLCPolSchema.getPolNo() != null
           && !mLCPolSchema.getPolNo().equals(""))
        {
            String sql = "  update LJAPayPerson "
                         + "set polNo = '" + mNewPolNo + "', "
                         + "   operator = '" + mGlobalInput.Operator + "', "
                         + "   modifyDate = '" + PubFun.getCurrentDate() + "', "
                         + "   modifyTime = '" + PubFun.getCurrentTime() + "' "
                         + "where polNo = '" + mLCPolSchema.getPolNo() + "' ";
            map.put(sql.toString(), "UPDATE");

            sql = sql.replaceFirst("LJAPayPerson", "LJSPayPersonB");
            map.put(sql, "UPDATE");
        }

        return true;
    }

    private boolean dealRnewLog()
    {
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogDB.setNewPolNo(mLCPolSchema.getPolNo());
        LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
        map.put(tLCRnewStateLogSet.get(1).getSchema(), "DELETE");

        tLCRnewStateLogSet.get(1).setNewPolNo(this.mNewPolNo);
        tLCRnewStateLogSet.get(1).setState(XBConst.RNEWSTATE_CONFIRM);
        tLCRnewStateLogSet.get(1).setOperator(mGlobalInput.Operator);;
        tLCRnewStateLogSet.get(1).setModifyDate(PubFun.getCurrentDate());
        tLCRnewStateLogSet.get(1).setModifyTime(PubFun.getCurrentTime());
        PubFun.fillDefaultField(tLCRnewStateLogSet.get(1));
        map.put(tLCRnewStateLogSet.get(1), "INSERT");

        return true;
    }

    /**
     * 处理帐户信息
     * @return boolean
     */
    private boolean dealAcc()
    {
        //对于特需险种，续保时不需要将保费注入帐户，对帐户进行利息结算即可
        //得到险种的所有帐户

//        String inputRate = (String) mTransferData
//                           .getValueByName(BQ.DETAILTYPE_ACCRATE);
//        if(inputRate != null)
//        {
//            rate = Double.parseDouble(inputRate);
//        }
//
//        int days = 365;  //特需险种的算利息期间为365天
    	String str="select 1 from lmriskapp where riskcode='"+mLCPolSchema.getRiskCode()+"' and TaxOptimal='Y'";
		String SYflag=new ExeSQL().getOneValue(str);
		if(null != SYflag && !"".equals(SYflag)){
			return true;//税优直接返回
		}
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(mPolNo);
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.query();

        LCInsureAccSet dealtLCInsureAccSet = new LCInsureAccSet();
 
        for(int i = 1; i <= tLCInsureAccSet.size(); i++)
        {
            double sumInterest = 0;  //帐户总利息

            //得到某帐户的所有分类
            LCInsureAccSchema tLCInsureAccSchema
                = tLCInsureAccSet.get(i).getSchema();
            LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
            tLCInsureAccClassDB.setPolNo(tLCInsureAccSchema.getPolNo());
            tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
            LCInsureAccClassSet tAccClassSet = tLCInsureAccClassDB.query();

            LCInsureAccClassSet dealtLCInsureAccClassSet
                = new LCInsureAccClassSet();  //处理后的帐户分类信息
            for(int t = 1; t <= tAccClassSet.size(); t++)
            {
                //计算该分类的利息
                LCInsureAccClassSchema tLCInsureAccClassSchema
                    = tAccClassSet.get(t).getSchema();

                LJSGetEndorseSet tLJSGetEndorseSet
                    = getAccFinaInfo(tLCInsureAccClassSchema);
                if(tLJSGetEndorseSet == null)
                {
                    return false;
                }

                double interest = 0;
                double accBala = 0;
                for(int temp = 1; temp <= tLJSGetEndorseSet.size(); temp++)
                {
                    if(tLJSGetEndorseSet.get(temp).getFeeFinaType().equals("LX"))
                    {
                        interest = Math.abs(tLJSGetEndorseSet.get(temp)
                                            .getGetMoney());
                    }
                    else
                    {
                        accBala = Math.abs(tLJSGetEndorseSet.get(temp)
                                            .getGetMoney());
                    }
                }

                //得到帐户分类信息的轨迹
                LCInsureAccTraceDB db = new LCInsureAccTraceDB();
                db.setPolNo(tLCInsureAccClassSchema.getPolNo());
                db.setInsuAccNo(tLCInsureAccClassSchema.getInsuAccNo());
                db.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
                LCInsureAccTraceSet set = db.query();

                //生成一条轨迹表示当前本金
                LCInsureAccTraceSet dealtTraceSet = new LCInsureAccTraceSet(); //新的帐户轨迹
                String tLimit = PubFun.getNoLimit(tLCInsureAccClassSchema
                                                  .getManageCom());  //轨迹序号
                String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                LCInsureAccTraceSchema aTrace = set.get(1).getSchema();
                aTrace.setPolNo(mNewPolNo);
                aTrace.setSerialNo(serNo);
                aTrace.setMoneyType("BF");
                aTrace.setMoney(accBala);
                aTrace.setPayDate(mLCPolSchema.getCValiDate());
                aTrace.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(aTrace);
                dealtTraceSet.add(aTrace);

                //插入一个轨迹表示利息滚本金
                tLimit = PubFun.getNoLimit(tLCInsureAccClassSchema
                                                  .getManageCom());  //轨迹序号
                serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

                sumInterest += interest;

                LCInsureAccTraceSchema tLCInsureAccTraceSchema
                    = aTrace.getSchema();
                tLCInsureAccTraceSchema.setPolNo(mNewPolNo);
                tLCInsureAccTraceSchema.setSerialNo(serNo);
                tLCInsureAccTraceSchema.setMoneyType("LX");
                tLCInsureAccTraceSchema.setMoney(interest);
                tLCInsureAccTraceSchema.setPayDate(mLCPolSchema.getCValiDate());
                tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(tLCInsureAccTraceSchema);
                dealtTraceSet.add(tLCInsureAccTraceSchema);

                map.put(dealtTraceSet, "INSERT");

                //处理帐户分类信息
                tLCInsureAccClassSchema.setPolNo(this.mNewPolNo);
                tLCInsureAccClassSchema.setInsuAccBala(accBala + interest);
                tLCInsureAccClassSchema.setSumPay(
                    tLCInsureAccClassSchema.getInsuAccBala());
                tLCInsureAccClassSchema.setLastAccBala(
                    tLCInsureAccClassSchema.getInsuAccBala());
                tLCInsureAccClassSchema.setInsuAccGetMoney(
                    tLCInsureAccClassSchema.getInsuAccBala());
                tLCInsureAccClassSchema.setSumPaym(0);
                tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(tLCInsureAccClassSchema);
                dealtLCInsureAccClassSet.add(tLCInsureAccClassSchema);
            }
            map.put(dealtLCInsureAccClassSet, "INSERT");

            //处理帐户信息
            tLCInsureAccSchema.setPolNo(mNewPolNo);
            tLCInsureAccSchema.setInsuAccBala(
                tLCInsureAccSchema.getInsuAccBala() + sumInterest);
            tLCInsureAccSchema.setSumPay(tLCInsureAccSchema.getInsuAccBala());
            tLCInsureAccSchema.setSumPaym(0);
            tLCInsureAccSchema.setLastAccBala(
                tLCInsureAccSchema.getInsuAccBala());
            tLCInsureAccSchema.setInsuAccGetMoney(
                tLCInsureAccSchema.getInsuAccBala());
            tLCInsureAccSchema.setBalaDate(PubFun.getCurrentDate());
            tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLCInsureAccSchema);
            dealtLCInsureAccSet.add(tLCInsureAccSchema);
        }
        map.put(dealtLCInsureAccSet, "INSERT");
        return true;
    }

    /**
     * 从财务接口得到账户利息
     * @return double
     */
    private LJSGetEndorseSet getAccFinaInfo(
        LCInsureAccClassSchema tLCInsureAccClassSchema)
    {
        String edorNo = (String) mTransferData.getValueByName(BQ.EDORNO);
        if(edorNo == null || edorNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PRnewPolSignBL";
            tError.functionName = "getAccFinaInfo";
            tError.errorMessage = "没有查询到账户财务信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
        tLJSGetEndorseDB.setEndorsementNo(edorNo);
        tLJSGetEndorseDB.setFeeOperationType(BQ.EDORTYPE_MJ);
        tLJSGetEndorseDB.setPolNo(tLCInsureAccClassSchema.getPolNo());
        tLJSGetEndorseDB.setPayPlanCode(tLCInsureAccClassSchema.getPayPlanCode());
        return tLJSGetEndorseDB.query();
    }

    private String getCInValiDate()
    {
         LCPolDB tLCPolDB = new LCPolDB();
         tLCPolDB.setPolNo(mPolNo);
         tLCPolDB.getInfo();
         if(tLCPolDB.getContType().equals(XBConst.CONTTYPE_G))
         {
             LCGrpContDB tLCGrpContDB = new LCGrpContDB();
             tLCGrpContDB.setGrpContNo(tLCPolDB.getGrpContNo());
             tLCGrpContDB.getInfo();
             return tLCGrpContDB.getCInValiDate();
         }
         else
         {
             LCContDB tLCContDB = new LCContDB();
             tLCContDB.setContNo(tLCPolDB.getContNo());
             tLCContDB.getInfo();
             return tLCContDB.getCInValiDate();
         }
    }

    /**
     * 处理缴费项信息
     * @return boolean
     */
    private LCPremSet dealLCPrem()
    {
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(mLCPolSchema.getPolNo());
        LCPremSet tLCPremSet = tLCPremDB.query();
        map.put(tLCPremSet, "DELETE");

        LCPremSet dealtLCPremSet = new LCPremSet();  //处理后的缴费项信息
        for(int i = 1; i <= tLCPremSet.size(); i++)
        {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i).getSchema();
            tLCPremSchema.setPolNo(this.mNewPolNo);

            tLCPremSchema.setSumPrem(tLCPremSchema.getPrem());
            // 计算交至日期
            System.out.println("mLCPolSchema.getPaytoDate()="
                               + mLCPolSchema.getPaytoDate());
            Date baseDate = mFDate.getDate(mLCPolSchema.getPaytoDate());  //此时mLCPolSchema.PayToDate仍为续保前日起

            int interval = tLCPremSchema.getPayIntv();
            if (interval == -1)
            {
                interval = 0; // 不定期缴费按照交费日期计算
            }
            String unit = "M";
            Date paytoDate = PubFun.calDate(baseDate, interval, unit, null);

            if (tLCPremSchema.getPayIntv() == 0) // 趸交按照交费终止日期计算
            {
                paytoDate = mFDate.getDate(tLCPremSchema.getPayEndDate());
            }

            //tLCPremSchema.setPayTimes(tLCPremSchema.getPayTimes() + 1);
            tLCPremSchema.setPaytoDate(mFDate.getString(paytoDate));
            tLCPremSchema.setOperator(mGlobalInput.Operator);
            tLCPremSchema.setModifyDate(PubFun.getCurrentDate());
            tLCPremSchema.setModifyTime(PubFun.getCurrentTime());

            dealtLCPremSet.add(tLCPremSchema);
        }
        map.put(dealtLCPremSet, "INSERT");

        return dealtLCPremSet;
    }

    /**
     * 处理领取项信息
     * @return boolean
     */
    private boolean dealLCGet()
    {
        try
        {
            LCGetDB tLCGetDB = new LCGetDB();
            tLCGetDB.setPolNo(mLCPolSchema.getPolNo()); //proposalNo
            LCGetSet tLCGetSet = tLCGetDB.query();
            map.put(tLCGetSet, "DELETE");

            LCGetSet dealtLCGetSet = new LCGetSet();
            for (int i = 1; i <= tLCGetSet.size(); i++)
            {
                LCGetSchema tLCGetSchema = tLCGetSet.get(i).getSchema();

                tLCGetSchema.setPolNo(this.mNewPolNo);
                tLCGetSchema.setOperator(mGlobalInput.Operator);
                tLCGetSchema.setModifyDate(PubFun.getCurrentDate());
                tLCGetSchema.setModifyTime(PubFun.getCurrentTime());

                dealtLCGetSet.add(tLCGetSchema);
            }
            map.put(dealtLCGetSet, "INSERT");

            return true;
        }catch(Exception e){e.printStackTrace(); return false;}
    }

    /**
     * 处理责任信息
     * @return boolean
     */
    private LCDutySet dealLCDuty(LCPremSet dealtLCPremSet)
    {
        try
        {

            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(mLCPolSchema.getPolNo());
            LCDutySet tLCDutySet = tLCDutyDB.query();
            map.put(tLCDutySet, "DELETE");
            LCDutySet dealtLCDutySet = new LCDutySet();
            for (int i = 1; i <= tLCDutySet.size(); i++)
            {
                LCDutySchema tLCDutySchema = tLCDutySet.get(i).getSchema();

                tLCDutySchema.setPolNo(this.mNewPolNo);
                tLCDutySchema.setSumPrem(tLCDutySchema.getPrem());
                // 交至日期
                Date maxPaytoDate = mFDate.getDate("1900-01-01");
                for (int j = 1; j <= dealtLCPremSet.size(); j++)
                {
                    LCPremSchema tLCPremSchema = dealtLCPremSet.get(j);
                    if (tLCDutySchema.getDutyCode().trim().equals(
                        tLCPremSchema.
                        getDutyCode().trim())
                        &&
                        mFDate.getDate(tLCPremSchema.getPaytoDate()).after(
                            maxPaytoDate))
                    {
                        maxPaytoDate = mFDate.getDate(tLCPremSchema.
                            getPaytoDate());
                    }
                }
                tLCDutySchema.setPaytoDate(mFDate.getString(maxPaytoDate));
                tLCDutySchema.setFirstPayDate(mLCPolSchema.getCValiDate());
                tLCDutySchema.setOperator(mGlobalInput.Operator);
                tLCDutySchema.setModifyDate(PubFun.getCurrentDate());
                tLCDutySchema.setModifyTime(PubFun.getCurrentTime());
                dealtLCDutySet.add(tLCDutySchema);
            }
            map.put(dealtLCDutySet, "INSERT");

            return dealtLCDutySet;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 处理险种信息
     * @return boolean
     */
    private boolean dealLCPol(LCDutySet dealtLCDutySet)
    {
        LCPolSchema tLCPolSchema = mLCPolSchema.getSchema();
        map.put(mLCPolSchema, "DELETE");

        tLCPolSchema.setPolNo(this.mNewPolNo);
        tLCPolSchema.setSumPrem(tLCPolSchema.getPrem());
        tLCPolSchema.setSignDate(PubFun.getCurrentDate());
        tLCPolSchema.setLastRevDate(tLCPolSchema.getCValiDate()); // 把最近复效日期置为起保日期
        tLCPolSchema.setAppFlag("0");
        tLCPolSchema.setPolState("00019999"); //sxy-2003-09-08(polstatus编码规则:前两位描述保单状态,后两位描述使保单处于该状态的原因.末尾四位描述了前一保单状态和处于该状态的原因)
        tLCPolSchema.setFirstPayDate(mFDate.getString(mFirstPayDate));

        // 交至日期
        Date maxPaytoDate = mFDate.getDate("1900-01-01");
        //2011-07-20  修改自动签单程序只将操作用户修改为UWXB9999 杨天政
        // ------------------------------------------------
        String tcontno = null;
        String tuwcode = null;
        String tapproveCode = null;
        //------------------------------------------------
        for (int j = 1; j <= dealtLCDutySet.size(); j++)
        {
            LCDutySchema tLCDutySchema = dealtLCDutySet.get(j).getSchema();
           
            if (mFDate.getDate(tLCDutySchema.getPaytoDate()).after(maxPaytoDate))
            {
                maxPaytoDate = mFDate.getDate(tLCDutySchema.getPaytoDate());
            }
            tcontno= tLCDutySchema.getContNo();    
        }
        
        tuwcode = getUWCODE(tcontno);
        tapproveCode = getAPPROVECODE(tcontno);
        	
        tLCPolSchema.setPaytoDate(mFDate.getString(maxPaytoDate));
        tLCPolSchema.setSignDate(PubFun.getCurrentDate());
        tLCPolSchema.setSignTime(PubFun.getCurrentTime());
        tLCPolSchema.setUWDate(PubFun.getCurrentDate());
        tLCPolSchema.setUWTime(PubFun.getCurrentTime());
        tLCPolSchema.setUWCode(tuwcode);
        tLCPolSchema.setUWFlag("9");
        tLCPolSchema.setApproveDate(PubFun.getCurrentDate());
        tLCPolSchema.setApproveTime(PubFun.getCurrentTime());
        tLCPolSchema.setApproveCode(tapproveCode);
        tLCPolSchema.setApproveFlag("9");
        tLCPolSchema.setOperator(mGlobalInput.Operator);
        tLCPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCPolSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCPolSchema, "INSERT");

        //如果是特需险种，则险种保费需要从账户余额得到
        if(CommonBL.isEspecialPol(tLCPolSchema.getRiskCode()))
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update LCPol ")
                .append("set prem = (select sum(InsuAccBala) ")
                .append("            from LCInsureAcc ")
                .append("            where polNo = '")
                .append(tLCPolSchema.getPolNo()).append("') ")
                .append("where polNo = '")
                .append(tLCPolSchema.getPolNo())
                .append("' ");
            map.put(sql.toString(), "UPDATE");
            sql = new StringBuffer();
            sql.append("update LCPol ")
                .append("set sumPrem = prem ")
                .append("where polNo = '")
                .append(tLCPolSchema.getPolNo())
                .append("' ");
            map.put(sql.toString(), "UPDATE");
        }

        return true;
    }

    private String getUWCODE(String tcontno)
    {
    	String UWCODE=null;
    	
    	String First_UW_SQL = "select uwcode from lbpol "
            + " where edorno like 'xb%' and prtno in " +
            		"(select prtno from lccont where contno= '" +  tcontno  + " ' ) "
            + " order by makedate asc fetch first 1 rows only " ;
        
    	UWCODE = new ExeSQL().getOneValue(First_UW_SQL);

    	if(UWCODE==null||UWCODE.equals(null)||UWCODE.equals(""))
    	{
    		First_UW_SQL = "select uwcode from lcpol where prtno in " +
            		"(select prtno from lccont where contno= '" +  tcontno  + " ' ) "
            + " order by makedate asc fetch first 1 rows only " ;
    		UWCODE = new ExeSQL().getOneValue(First_UW_SQL);
    	}
    	
    	return UWCODE;
    }
    
    private String getAPPROVECODE(String tcontno)
    {
    	String APPROVECODE = null;
    	
    	 String First_AP_SQL = "select approveCode from lbpol "
             + " where  edorno like 'xb%' and  prtno in " +
             		"(select prtno from lccont where contno= '" +  tcontno  + " ' ) "
             + " order by makedate asc fetch first 1 rows only " ;
          
    	 APPROVECODE = new ExeSQL().getOneValue(First_AP_SQL);

    	 if(APPROVECODE==null||APPROVECODE.equals(null)||APPROVECODE.equals(""))
      	{
     		 First_AP_SQL = "select approveCode from lcpol where prtno in " +
            		"(select prtno from lccont where contno= '" +  tcontno  + " ' ) "
            + " order by makedate asc fetch first 1 rows only " ;
     		 APPROVECODE = new ExeSQL().getOneValue(First_AP_SQL);
      	}
    	
    	return APPROVECODE;
    }
    public static void main(String[] args)
    {
        PRnewPolSignBL tPRnewPolSignBL = new PRnewPolSignBL();
    }
}
