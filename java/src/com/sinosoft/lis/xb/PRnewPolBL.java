package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.*;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 对险种信息的续保处理，在这里只处理信LCPol、LCDuty、LCPrem、LCGet表，
 * 其他的险种信息只需要简单的备份，放在转移时处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewPolBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private VData mInputData = null;
    private MMap map = null;

    private LCPolSchema mLCPolSchema = null;
    private double mInputStandPrem = 0;
    private CalBL mCalBL = null;

    private String mOperate = null;

    /**
     * 无参构造方法
     */
    public PRnewPolBL()
    {
    }

    /**
     * 得到处理后的数据集合
     * @param inputData VData：包括：
     * GlobalInput: 准备好的操作员信息
     * LCPolSchema: 准备好的险种信息
     * LCDutySet: 准备好的责任信息
     * LCPremSet: 准备好缴费项信息
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return MMap：处理后的数据集合
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in PRnewPolBL->getSubmitMap");
        mInputData = inputData;
        mOperate = operate;

        if(!getInputData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括：
     * GlobalInput: 准备好的操作员信息
     * LCPolSchema: 准备好的险种信息
     * LCDutySet: 准备好的责任信息
     * LCPremSet: 准备好缴费项信息
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData inputData, String operate)
    {
        if(getSubmitMap(inputData, operate) == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 得到传入的数据
     * @return boolean，获取成功true，否则false
     */
    private boolean getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData
                       .getObjectByObjectName("GlobalInput", 0);

        if(mGlobalInput == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }

    /**
     * 进行业务处理，生成新的续保数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealData()
    {

        mLCPolSchema = ((LCPolSchema) mInputData
                       .getObjectByObjectName("LCPolSchema", 0)).getSchema();

        mInputStandPrem = mLCPolSchema.getStandPrem(); //纪录原标准保费
        mLCPolSchema.setRenewCount(mLCPolSchema.getRenewCount() + 1);//续保时将续保次数+1，以便续保算费
        if(!reCalculateFee())
        {
            return false;
        }

        if(!dealReCalData())
        {
            return false;
        }

        return true;
    }

    /**
     * 重算险种的相关费用
     * @return boolean：成功true，否则false
     */
    private boolean reCalculateFee()
    {
        map = new MMap();
        //重算险种的相关费用
        LCDutySet tLCDutySet = (LCDutySet) mInputData.
                               getObjectByObjectName("LCDutySet", 0);
        if(null != mLCPolSchema && (mLCPolSchema.getRiskCode().equals("123701")||mLCPolSchema.getRiskCode().equals("123901"))){
        	TransferData tTransferData = new TransferData();
        	String str ="select RiskWrapCode from lcriskdutywrap where riskcode='"+mLCPolSchema.getRiskCode()+"' and contno='"+mLCPolSchema.getContNo()+"' fetch first 1 rows only";
        	 ExeSQL tExeSQL = new ExeSQL();
             String tRiskWrapCode =tExeSQL.getOneValue(str);
            
        	tTransferData.setNameAndValue("RiskWrapCode", tRiskWrapCode);
        	  mCalBL = new CalBL(mLCPolSchema, tLCDutySet, null, tTransferData);
        }else {
           mCalBL = new CalBL(mLCPolSchema, tLCDutySet, null, null);
        }
        if (!mCalBL.calPol())
        {
            mErrors.copyAllErrors(mCalBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 处理重算得到的数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealReCalData()
    {
        mLCPolSchema = mCalBL.getLCPol().getSchema();

        //由下往上换号，这样可以避免在上层处理时再取出处理后的数据
        String newProposalNo = PubFun1.CreateMaxNo(
            "ProposalNo", PubFun.getNoLimit(mLCPolSchema.getManageCom()));
        if(mLCPolSchema.getPaytoDate() == null || mLCPolSchema.equals(""))
        {
            System.out.println(mLCPolSchema.getPolNo());
        }

        mLCPolSchema.setProposalNo(newProposalNo);
        if (mLCPolSchema.getPolNo().equals(mLCPolSchema.getMainPolNo()))
        { //主险-号码全部换
//            mLCPolSchema.setMainPolNo(mLCPolSchema.getProposalNo());
        }
        mLCPolSchema.setPolNo(mLCPolSchema.getProposalNo());

        PubFun.fillDefaultField(mLCPolSchema);

        //
        LCDutySet tLCDutySet = mCalBL.getLCDuty();  //得到的是LCDutyBLSet
        LCPremSet tLCPremSet = mCalBL.getLCPrem();  //得到的是LCPremBLSet
        LCGetSet tLCGetSet = mCalBL.getLCGet();  //得到的是LCGetBLSet

        //处理重算后的责任和保费项信息
        if(!dealDutyAndPrem(tLCDutySet, tLCPremSet))
        {
            return false;
        }

        if(!dealLCPol(tLCDutySet))
        {
            return false;
        }

        // 领取项
        for (int j = 1; j <= tLCGetSet.size(); j++)
        {
            LCGetSchema tLCGetSchema = (LCGetSchema)tLCGetSet.get(j).getSchema();
            tLCGetSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
            tLCGetSchema.setContNo(mLCPolSchema.getContNo());
            tLCGetSchema.setPolNo(mLCPolSchema.getPolNo());
            tLCGetSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLCGetSchema);

            map.put(tLCGetSchema, "INSERT");
        }

        return true;
    }

    /**
     * 处理重算后的责任和保费项信息
     * @param tLCDutySet LCDutySet：重算后的责任信息
     * @param tLCPremSet LCPremSet：重算后的缴费项信息
     * @return boolean：处理成功true，否则
     */
    private boolean dealDutyAndPrem(LCDutySet tLCDutySet, LCPremSet tLCPremSet)
    {
        //计算出的标准保费与原保费的比例
        double rate = mLCPolSchema.getStandPrem() / this.mInputStandPrem;

        //处理计算后的责任和保费项信息
        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            //换号
            tLCDutySet.get(i).setContNo(mLCPolSchema.getContNo());
            tLCDutySet.get(i).setPolNo(mLCPolSchema.getPolNo());

            //PaytoDate在签单时计算得到，此处赋值为原险种源缴至日期原因见下面对LCPrem的PayToDate说明：
            tLCDutySet.get(i).setPaytoDate(mLCPolSchema.getPaytoDate());

            //非计算得到的保费项，这些保费项可能是由加费等操作产生的
            LCPremSet otherLCPremSet = (LCPremSet) mInputData
                                       .getObjectByObjectName("LCPremSet", 0);
            if (otherLCPremSet == null)
            {
                continue;
            }
            for (int otherPrem = 1; otherPrem <= otherLCPremSet.size(); otherPrem++)
            {
                LCPremSchema tLCPremSchema = otherLCPremSet.get(i);
                if (tLCDutySet.get(i).getDutyCode().equals(tLCPremSchema.
                    getDutyCode()))
                {
                    //通过比例计算单前标准保费和保费
                    tLCPremSchema.setPrem(tLCPremSchema.getPrem() * rate);
                    tLCPremSchema.setStandPrem(tLCPremSchema.getStandPrem()
                                               * rate);
//                    tLCPremSchema.setPayStartDate(tLCDutySet.get(i).
//                                                  getFirstPayDate());
                    tLCPremSchema.setSumPrem(tLCPremSchema.getSumPrem() * rate);
                    //tLCPremSchema.setPayEndDate(tLCDutySet.get(i).getPayEndDate());
                    tLCPremSchema.setPaytoDate(tLCDutySet.get(i).getPaytoDate());

                    //将非计算的保费加到责任、险种上
                    tLCDutySet.get(i).setStandPrem(
                        tLCDutySet.get(i).getStandPrem()
                        + tLCPremSchema.getStandPrem());
                    tLCDutySet.get(i).setPrem(
                        tLCDutySet.get(i).getPrem()
                        + tLCPremSchema.getPrem());
/*
                    mLCPolSchema.setStandPrem(
                        mLCPolSchema.getStandPrem()
                        + tLCPremSchema.getStandPrem());
*/
                    mLCPolSchema.setPrem(
                       mLCPolSchema.getPrem()
                       + tLCPremSchema.getPrem());

                   tLCPremSet.add(tLCPremSchema);
                }
            }

            //处理责任信息
            tLCDutySet.get(i).setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLCDutySet.get(i));
            map.put(tLCDutySet.get(i).getSchema(), "INSERT");
        }

        //处理保费项
        for (int temp = 1; temp <= tLCPremSet.size(); temp++)
        {
            //换号
            tLCPremSet.get(temp).setGrpContNo(mLCPolSchema.getGrpContNo());
            tLCPremSet.get(temp).setContNo(mLCPolSchema.getContNo());
            tLCPremSet.get(temp).setPolNo(mLCPolSchema.getPolNo());

            //PaytoDate在签单时计算得到，此处赋值为原险种源缴至日期原因有二：
            //1、与LCPol统一起来
            //2、在催收的时候直接用它计算下一缴至日期
            tLCPremSet.get(temp).setPaytoDate(mLCPolSchema.getPaytoDate());

            PubFun.fillDefaultField(tLCPremSet.get(temp));
            tLCPremSet.get(temp).setOperator(mGlobalInput.Operator);
            map.put(tLCPremSet.get(temp).getSchema(), "INSERT");
        }

        return true;
    }

    /**
     * 处理重算后的险种信息
     * @param tLCDutySet LCDutySet，处理后的责任信息
     * @return boolean：成功true，否则false
     */
    private boolean dealLCPol(LCDutySet tLCDutySet)
    {
        if (mLCPolSchema.getPayIntv() == 0)
        {
            mLCPolSchema.setPayIntv(tLCDutySet.get(1).getPayIntv());
        }
        if (mLCPolSchema.getInsuYear() == 0)
        {
            mLCPolSchema.setInsuYear(tLCDutySet.get(1).getInsuYear());
        }
        if (mLCPolSchema.getPayEndYear() == 0)
        {
            mLCPolSchema.setPayEndYear(tLCDutySet.get(1).getPayEndYear());
        }
        if (mLCPolSchema.getGetYear() == 0)
        {
            mLCPolSchema.setGetYear(tLCDutySet.get(1).getGetYear());
        }
        if (mLCPolSchema.getAcciYear() == 0)
        {
            mLCPolSchema.setAcciYear(tLCDutySet.get(1).getAcciYear());
        }
        if (mLCPolSchema.getInsuYearFlag() == null &&
            tLCDutySet.get(1).getInsuYearFlag() != null)
        {
            mLCPolSchema.setInsuYearFlag(tLCDutySet.get(1).getInsuYearFlag());
        }
        if (mLCPolSchema.getGetStartType() == null &&
            tLCDutySet.get(1).getGetStartType() != null)
        {
            mLCPolSchema.setGetStartType(tLCDutySet.get(1).getGetStartType());
        }
        if (mLCPolSchema.getGetYearFlag() == null && tLCDutySet.get(1).getGetYearFlag() != null)
        {
            mLCPolSchema.setGetYearFlag(tLCDutySet.get(1).getGetYearFlag());
        }
        if (mLCPolSchema.getPayEndYearFlag() == null &&
            tLCDutySet.get(1).getPayEndYearFlag() != null)
        {
            mLCPolSchema.setPayEndYearFlag(tLCDutySet.get(1).getPayEndYearFlag());
        }
        if (mLCPolSchema.getAcciYearFlag() == null &&
            tLCDutySet.get(1).getAcciYearFlag() != null)
        {
            mLCPolSchema.setAcciYearFlag(tLCDutySet.get(1).getAcciYearFlag());
        }

        mLCPolSchema.setAppFlag(BQ.APPFLAG_INIT);
        mLCPolSchema.setStateFlag(BQ.STATE_FLAG_APP);
        //是否制定生效日期
        String specifyValiDateFlag =
            (mLCPolSchema.getCValiDate() == null
             || mLCPolSchema.getCValiDate().equals("")) ? "N" : "Y";

        if(tLCDutySet.get(1).getPaytoDate() != null)
        {
            mLCPolSchema.setPaytoDate(tLCDutySet.get(1).getPaytoDate());
        }
//        mLCPolSchema.setRenewCount(mLCPolSchema.getRenewCount() + 1);
        mLCPolSchema.setRenewCount(mLCPolSchema.getRenewCount());
        mLCPolSchema.setApproveCode("");
        mLCPolSchema.setApproveDate("");
        mLCPolSchema.setApproveTime("");
        mLCPolSchema.setApproveFlag("");
        mLCPolSchema.setUWCode("");
        mLCPolSchema.setUWDate("");
        mLCPolSchema.setUWTime("");
        mLCPolSchema.setUWFlag("");
        mLCPolSchema.setSpecifyValiDate(specifyValiDateFlag); //生效日期（指定）
        mLCPolSchema.setLastEdorDate("");
        mLCPolSchema.setLastGetDate("");
        mLCPolSchema.setLastLoanDate("");
        mLCPolSchema.setLastRegetDate("");
        mLCPolSchema.setLastRevDate("");
        mLCPolSchema.setPolApplyDate(PubFun.getCurrentDate());
//        int tInsuredAge = PubFun.calInterval(mLCPolSchema.getInsuredBirthday(),
//                                             mLCPolSchema.getCValiDate(), "Y");
        int tInsuredAge = PubFun.calInterval(mLCPolSchema.getInsuredBirthday(),
              mLCPolSchema.getPolApplyDate(), "Y");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+tInsuredAge);
        mLCPolSchema.setInsuredAppAge(tInsuredAge);
        mLCPolSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCPolSchema);

        map.put(mLCPolSchema, "INSERT");

        return true;
    }

    /**
     * 生成财务数据
     * @return boolean，生成成功true，否则false
     */
    private boolean createFee()
    {
        return true;
    }

    /**
     * 调试方法
     * @param args String[] null
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LCPolDB db = new LCPolDB();
        db.setPolNo("21000003975");
        db.getInfo();

        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(db.getPolNo());

        VData data = new VData();
        data.add(gi);
        data.add(db.getSchema());
        data.add(tLCDutyDB.query());

        PRnewPolBL bl = new PRnewPolBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
