package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import java.util.HashSet;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 转移团单数据：将处理后的续保数据和原数据相关号码互换，
 * 这样可以使得续保结束后可以使用原来的号码直接查询新保单信息，
 * 并把续保前的保单信息备份到b表
 * 对于在续保过程中处理过的信息，需要转移到B表
 * 而续保过程中未处理过的信息则只需要备份到B表，并修改相关的C表信息
 *  </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class TransferCGDataToBG
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private LCGrpContSchema mLCGrpContSchema = null;  //续保前的团单信息

    private Reflections ref = null;
    private String mEdorNo = null;
    private String mCurrDate = PubFun.getCurrentDate();
    private String mCurrTime = PubFun.getCurrentTime();
    private String mNewGrpContNo = null;
    private String mNewProposalGrpContNo = null;
    private String mNewGrpPolNo = null;
    private String mNewGrpProposalNo = null;
    private HashSet mTablesHashSet = null;   //存储修改的表明


    private MMap map = null;

    public TransferCGDataToBG()
    {
    }

    /**
     * 操作的提交方法
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        if(getSubmitMap(inputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("存储数据出错。");
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in TransferCGDataToBG->getSubmitMap");
        if(!getInputData(inputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }
        map = new MMap();
        ref = new Reflections();
        if(mEdorNo == null || mEdorNo.equals("") || mEdorNo.equals("null"))
        {
            mEdorNo = "xb" + com.sinosoft.task.CommonBL.createWorkNo();
        }
        else if(mEdorNo.indexOf("xb") < 0)
        {
            mEdorNo = "xb" + mEdorNo;
        }

        if(!transferAllPData())
        {
            return null;
        }
        if(!transferGData())
        {
            return null;
        }

        batchChangeInfo();

        return map;
    }

    private boolean getInputData(VData inputData)
    {
        this.mGlobalInput = (GlobalInput) inputData
                            .getObjectByObjectName("GlobalInput", 0);
        this.mLCGrpContSchema = (LCGrpContSchema) inputData
                                .getObjectByObjectName("LCGrpContSchema", 0);
        TransferData tf = (TransferData) inputData
                                .getObjectByObjectName("TransferData", 0);
        if(this.mGlobalInput == null || this.mLCGrpContSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        if(tf != null)
        {
            this.mEdorNo = (String) tf.getValueByName("EdorNo");
        }

        return true;
    }

    private boolean checkData()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if(!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("没有查询到团保单号为"
                                + mLCGrpContSchema.getGrpContNo()
                                + "的保单信息。");
            System.out.println(tLCGrpContDB.mErrors.getErrContent());
            return false;
        }
        this.mLCGrpContSchema = tLCGrpContDB.getSchema();

        String sql = "  select polNo "
                     + "from LCRnewStateLog "
                     + "where grpContNo = '" + mLCGrpContSchema.getGrpContNo()
                     + "'  and state in('" + XBConst.RNEWSTATE_APP
                     + "', '" + XBConst.RNEWSTATE_UNUW
                     + "', '" + XBConst.RNEWSTATE_UNHASTEN
                     + "', '" + XBConst.RNEWSTATE_UNCONFIRM + "') ";
        ExeSQL e = new ExeSQL();
        String polNo = e.getOneValue(sql);
        if(!polNo.equals("") && !polNo.equals("null"))
        {
            mErrors.addOneError("请先签单");
            return false;
        }

        return true;
    }

    /**
     * 转移团单下的所有个单信息
     * @return boolean
     */
    private boolean transferAllPData()
    {
        String sql = "  select distinct a.* "
                     + "from LCCont a, LCRnewStateLog b "
                     + "where a.contNo = b.contNo "
                     + "   and b.grpContNo = '"
                     + mLCGrpContSchema.getGrpContNo()
                     + "'  and b.newGrpContNo = "
                     + "     (select max(newGrpContNo) "
                     + "      from LCRnewStateLog "
                     + "      where grpContNo = '"
                     + mLCGrpContSchema.getGrpContNo()
                     + "'         and state = '"
                     + XBConst.RNEWSTATE_CONFIRM
                     + "') ";
        System.out.println(sql);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sql);
        VData data = new VData();
        TransferCPDataToBP tTransferCPDataToBP = new TransferCPDataToBP(mEdorNo);
        tTransferCPDataToBP.setIsGrpCont(true);
        for (int i = 1; i <= tLCContSet.size(); i++)
        {
            System.out.println("个单: " + i);
            LCContSchema tLCContSchema = tLCContSet.get(i).getSchema();
            data.clear();
            data.add(this.mGlobalInput);
            data.add(tLCContSchema);
            MMap tMMap = tTransferCPDataToBP.getSubmitMap(data, "");
            if(tMMap == null)
            {
                mErrors.copyAllErrors(tTransferCPDataToBP.mErrors);
                return false;
            }
            map.add(tMMap);
        }

        return true;
    }

    /**
     * 转移团单级别的数据
     */
    private boolean transferGData()
    {
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCRnewStateLogDB.setState(XBConst.RNEWSTATE_CONFIRM);
        LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
        if(tLCRnewStateLogSet.size() == 0)
        {
            mErrors.addOneError("没有查询到团单"
                                + tLCRnewStateLogDB.getGrpContNo()
                                + "需要转移的续保数据。");
            System.out.println(tLCRnewStateLogDB.mErrors.getErrContent());
            return false;
        }

        if(!transferGrpFee(tLCRnewStateLogSet.get(1)))
        {
            return false;
        }
        if(!transferGrpPol())
        {
            return false;
        }
        if(!transferGrpCont())
        {
            return false;
        }
        if(!transferPlan())
        {
            return false;
        }
        if(!transferGrpAppnt())
        {
            return false;
        }

        map.put("update LCRnewStateLog "
                + "set state = '" + XBConst.RNEWSTATE_DELIVERED + "' "
                + "where newGrpContNo = '" + mNewGrpContNo + "' ", "UPDATE");

        return true;
    }

    private boolean transferGrpCont()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(this.mNewGrpContNo);
        if(!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("没有取得续保后保单信息.");
            return false;
        }
        LCGrpContSchema newLCGrpContSchema = tLCGrpContDB.getSchema();
        map.put(newLCGrpContSchema.getSchema(), "DELETE");
        this.mNewProposalGrpContNo = newLCGrpContSchema.getProposalGrpContNo();

        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if(!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("没有取得续保前保单信息.");
            return false;
        }
        LCGrpContSchema oldLCGrpContSchema = tLCGrpContDB.getSchema();
        map.put(oldLCGrpContSchema.getSchema(), "DELETE");

        //将新保单的号换成旧号
        newLCGrpContSchema.setGrpContNo(oldLCGrpContSchema.getGrpContNo());
        newLCGrpContSchema.setProposalGrpContNo(oldLCGrpContSchema.getProposalGrpContNo());
        newLCGrpContSchema.setAppFlag(BQ.APPFLAG_SIGN);
        newLCGrpContSchema.setStateFlag(BQ.STATE_FLAG_SIGN);

        newLCGrpContSchema.setOperator(mGlobalInput.Operator);
        newLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        newLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(newLCGrpContSchema.getSchema(), "INSERT");

        //将原保单放到B表并换成新号码
        LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();  //原数据放到B表
        ref.transFields(tLBGrpContSchema, oldLCGrpContSchema);
        tLBGrpContSchema.setEdorNo(this.mEdorNo);
        tLBGrpContSchema.setGrpContNo(mNewGrpContNo);
        tLBGrpContSchema.setProposalGrpContNo(this.mNewProposalGrpContNo);
        tLBGrpContSchema.setOperator(mGlobalInput.Operator);
        tLBGrpContSchema.setStateFlag(BQ.STATE_FLAG_TERMINATE);
        PubFun.fillDefaultField(tLBGrpContSchema);
        map.put(tLBGrpContSchema, "INSERT");

        setGrpContState(BQ.FILLDATA, mLCGrpContSchema.getCInValiDate());

        return true;
    }

    /**
     * 转移集体险种管理费信息
     * @return boolean
     */
    private boolean transferGrpFee(LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
        //转移团单管理费计算参数------------------
        LCGrpFeeParamDB tLCGrpFeeParamDB = new LCGrpFeeParamDB();
        tLCGrpFeeParamDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCGrpFeeParamSet tLCGrpFeeParamSet = tLCGrpFeeParamDB.query();
        for (int i = 1; i <= tLCGrpFeeParamSet.size(); i++)
        {
            LBGrpFeeParamSchema tLBGrpFeeParamSchema = new LBGrpFeeParamSchema();
            ref.transFields(tLBGrpFeeParamSchema, tLCGrpFeeParamSet.get(i));
            tLBGrpFeeParamSchema.setEdorNo(this.mEdorNo);
            tLBGrpFeeParamSchema.setGrpContNo(tLCRnewStateLogSchema.getNewGrpContNo());
            tLBGrpFeeParamSchema.setGrpPolNo(tLCRnewStateLogSchema.getNewGrpPolNo());
            tLBGrpFeeParamSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLBGrpFeeParamSchema);
            map.put(tLBGrpFeeParamSchema, "INSERT");

            tLCGrpFeeParamSet.get(i).setOperator(mGlobalInput.Operator);
            tLCGrpFeeParamSet.get(i).setModifyDate(this.mCurrDate);
            tLCGrpFeeParamSet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCGrpFeeParamSet, "UPDATE");

        //转移集体险种管理费描述---------
        LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
        tLCGrpFeeDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCGrpFeeSet tLCGrpFeeSet = tLCGrpFeeDB.query();
        for (int i = 1; i <= tLCGrpFeeSet.size(); i++)
        {
            LBGrpFeeSchema tLBGrpFeeSchema = new LBGrpFeeSchema();
            ref.transFields(tLBGrpFeeSchema, tLCGrpFeeSet.get(i));
            tLBGrpFeeSchema.setEdorNo(this.mEdorNo);
            tLBGrpFeeSchema.setGrpContNo(tLCRnewStateLogSchema.getNewGrpContNo());
            tLBGrpFeeSchema.setGrpPolNo(tLCRnewStateLogSchema.getNewGrpPolNo());
            tLBGrpFeeSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLBGrpFeeSchema);
            map.put(tLBGrpFeeSchema, "INSERT");

            tLCGrpFeeSet.get(i).setOperator(mGlobalInput.Operator);
            tLCGrpFeeSet.get(i).setModifyDate(this.mCurrDate);
            tLCGrpFeeSet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCGrpFeeSet, "UPDATE");

        return true;
    }

    /**
     * 转移团险信息
     * @return boolean
     */
    private boolean transferGrpPol()
    {
        //转移集体险种---------
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        for(int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            tLCRnewStateLogDB.setGrpPolNo(tLCGrpPolSet.get(i).getGrpPolNo());
            tLCRnewStateLogDB.setState(XBConst.RNEWSTATE_CONFIRM);
            LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
            if (tLCRnewStateLogSet.size() == 0)
            {
                continue;
            }
            LCRnewStateLogSchema tLCRnewStateLogSchema
                = tLCRnewStateLogSet.get(1);
            tLCGrpPolDB.setGrpPolNo(tLCRnewStateLogSchema.getNewGrpPolNo());
            if(!tLCGrpPolDB.getInfo())
            {
                mErrors.addOneError("没有查询到续保确认后的险种信息。");
                return false;
            }
            LCGrpPolSchema newLCGrpPolSchema = tLCGrpPolDB.getSchema();
            LCGrpPolSchema oldLCGrpPolSchema = tLCGrpPolSet.get(i);
            map.put(oldLCGrpPolSchema.getSchema(), "DELETE");
            map.put(newLCGrpPolSchema.getSchema(), "DELETE");

            mNewGrpPolNo = newLCGrpPolSchema.getGrpPolNo();
            mNewGrpContNo = newLCGrpPolSchema.getGrpContNo();
            mNewGrpProposalNo = newLCGrpPolSchema.getGrpProposalNo();

            //将新险种的号换成旧号
            newLCGrpPolSchema.setGrpPolNo(oldLCGrpPolSchema.getGrpPolNo());
            newLCGrpPolSchema.setGrpContNo(oldLCGrpPolSchema.getGrpContNo());
            newLCGrpPolSchema.setGrpProposalNo(oldLCGrpPolSchema.getGrpProposalNo());
            newLCGrpPolSchema.setAppFlag(BQ.APPFLAG_SIGN);
            newLCGrpPolSchema.setStateFlag(BQ.STATE_FLAG_SIGN);
            newLCGrpPolSchema.setOperator(mGlobalInput.Operator);
            newLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
            newLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(newLCGrpPolSchema, "INSERT");

            //将原险种放到B表并换成新号码
            LBGrpPolSchema tLBGrpPolSchema = new LBGrpPolSchema(); //原数据放到B表
            ref.transFields(tLBGrpPolSchema, oldLCGrpPolSchema);
            tLBGrpPolSchema.setEdorNo(this.mEdorNo);
            tLBGrpPolSchema.setGrpPolNo(mNewGrpPolNo);
            tLBGrpPolSchema.setGrpContNo(mNewGrpContNo);
            tLBGrpPolSchema.setGrpProposalNo(this.mNewGrpProposalNo);
            tLBGrpPolSchema.setStateFlag(BQ.STATE_FLAG_TERMINATE);
            tLBGrpPolSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLBGrpPolSchema);
            map.put(tLBGrpPolSchema, "INSERT");

            setGrpContState(mNewGrpPolNo, mLCGrpContSchema.getCInValiDate());
        }

        return true;
    }

    private void setGrpContState(String grpPolNo, String startDate)
    {
        LCGrpContStateSchema tLCGrpContStateSchema = new LCGrpContStateSchema();
        tLCGrpContStateSchema.setGrpContNo(mNewGrpContNo);
        tLCGrpContStateSchema.setGrpPolNo(grpPolNo);
        tLCGrpContStateSchema.setStateType("Terminate");
        tLCGrpContStateSchema.setStateReason(BQ.EDORTYPE_XB);
        tLCGrpContStateSchema.setState("1");
        tLCGrpContStateSchema.setOtherNo(mEdorNo);
        tLCGrpContStateSchema.setOtherNoType(BQ.EDORTYPE_XB);
        tLCGrpContStateSchema.setOperator(mGlobalInput.Operator);
        tLCGrpContStateSchema.setStartDate(startDate);
        tLCGrpContStateSchema.setEndDate("9999-12-31");
        tLCGrpContStateSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContStateSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpContStateSchema.setContPlanCode("00");
        map.put(tLCGrpContStateSchema, "DELETE&INSERT");
    }

    /**
     * 转移保障计划
     * @return boolean
     */
    private boolean transferPlan()
    {
        //保单保险计划--------------------
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCContPlanSet tLCContPlanSet = tLCContPlanDB.query();
        for(int i = 1; i <= tLCContPlanSet.size(); i++)
        {
            LBContPlanSchema tLBContPlanSchema = new LBContPlanSchema();
            ref.transFields(tLBContPlanSchema, tLCContPlanSet.get(i));
            tLBContPlanSchema.setEdorNo(this.mEdorNo);
            tLBContPlanSchema.setGrpContNo(this.mNewGrpContNo);
            tLBContPlanSchema.setProposalGrpContNo(this.mNewProposalGrpContNo);;
            tLBContPlanSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLBContPlanSchema);
            map.put(tLBContPlanSchema, "INSERT");

            tLCContPlanSet.get(i).setOperator(mGlobalInput.Operator);
            tLCContPlanSet.get(i).setModifyDate(this.mCurrDate);
            tLCContPlanSet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCContPlanSet, "UPDATE");

        //保单险种保险计划-----------------
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        tLCContPlanRiskDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.query();
        for(int i = 1; i <= tLCContPlanRiskSet.size(); i++)
        {
            LBContPlanRiskSchema tLBContPlanRiskSchema = new LBContPlanRiskSchema();
            ref.transFields(tLBContPlanRiskSchema, tLCContPlanRiskSet.get(i));
            tLBContPlanRiskSchema.setEdorNo(this.mEdorNo);
            tLBContPlanRiskSchema.setGrpContNo(this.mNewGrpContNo);
            tLBContPlanRiskSchema.setProposalGrpContNo(this.mNewProposalGrpContNo);;
            tLBContPlanRiskSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(tLBContPlanRiskSchema);
            map.put(tLBContPlanRiskSchema, "INSERT");

            tLCContPlanRiskSet.get(i).setOperator(mGlobalInput.Operator);
            tLCContPlanRiskSet.get(i).setModifyDate(this.mCurrDate);
            tLCContPlanRiskSet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCContPlanRiskSet, "UPDATE");

        //保险计划责任要素值-----------------
        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
        tLCContPlanDutyParamDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.query();
        for(int i = 1; i <= tLCContPlanDutyParamSet.size(); i++)
        {
            LBContPlanDutyParamSchema tLBContPlanDutyParamSchema
                = new LBContPlanDutyParamSchema();
            ref.transFields(tLBContPlanDutyParamSchema,
                            tLCContPlanDutyParamSet.get(i));
            tLBContPlanDutyParamSchema.setEdorNo(this.mEdorNo);
            tLBContPlanDutyParamSchema.setGrpContNo(this.mNewGrpContNo);
            tLBContPlanDutyParamSchema.setProposalGrpContNo(
                this.mNewProposalGrpContNo);
            tLBContPlanDutyParamSchema.setGrpPolNo(this.mNewGrpPolNo);
            map.put(tLBContPlanDutyParamSchema, "INSERT");
        }
        //LCContPlanDutyParam表没有需要变更的字段

        //保险计划责任要素值-----------------
        LCContPlanFactoryDB tLCContPlanFactoryDB = new LCContPlanFactoryDB();
        tLCContPlanFactoryDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCContPlanFactorySet tLCContPlanFactorySet = tLCContPlanFactoryDB.query();
        for(int i = 1; i <= tLCContPlanFactorySet.size(); i++)
        {
            LBContPlanFactorySchema tLBContPlanFactorySchema
                = new LBContPlanFactorySchema();
            ref.transFields(tLBContPlanFactorySchema,
                            tLCContPlanFactorySet.get(i));
            tLBContPlanFactorySchema.setEdorNo(this.mEdorNo);
            tLBContPlanFactorySchema.setGrpContNo(this.mNewGrpContNo);
            tLBContPlanFactorySchema.setProposalGrpContNo(
                this.mNewProposalGrpContNo);
            PubFun.fillDefaultField(tLBContPlanFactorySchema);
            map.put(tLBContPlanFactorySchema, "INSERT");

            tLCContPlanFactorySet.get(i).setModifyDate(this.mCurrDate);
            tLCContPlanFactorySet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCContPlanFactorySet, "UPDATE");

        //LCContPlanParam-----------------
        LCContPlanParamDB tLCContPlanParamDB = new LCContPlanParamDB();
        tLCContPlanParamDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCContPlanParamSet tLCContPlanParamSet = tLCContPlanParamDB.query();
        for(int i = 1; i <= tLCContPlanParamSet.size(); i++)
        {
            LBContPlanParamSchema tLBContPlanParamSchema
                = new LBContPlanParamSchema();
            ref.transFields(tLBContPlanParamSchema,
                            tLCContPlanParamSet.get(i));
            tLBContPlanParamSchema.setEdorNo(this.mEdorNo);
            tLBContPlanParamSchema.setGrpContNo(this.mNewGrpContNo);
            tLBContPlanParamSchema.setProposalGrpContNo(
                this.mNewProposalGrpContNo);
            PubFun.fillDefaultField(tLBContPlanParamSchema);
            map.put(tLBContPlanParamSchema, "INSERT");

            tLCContPlanParamSet.get(i).setModifyDate(this.mCurrDate);
            tLCContPlanParamSet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCContPlanParamSet, "UPDATE");

        return true;
    }

    /**
     * 转移团单投保人信息
     * @return boolean
     */
    private boolean transferGrpAppnt()
    {
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCGrpAppntSet tLCGrpAppntSet = tLCGrpAppntDB.query();
        for(int i = 1; i <= tLCGrpAppntSet.size(); i++)
        {
            LBGrpAppntSchema tLBGrpAppntSchema = new LBGrpAppntSchema();
            ref.transFields(tLBGrpAppntSchema, tLCGrpAppntSet.get(i));
            tLBGrpAppntSchema.setEdorNo(this.mEdorNo);
            tLBGrpAppntSchema.setGrpContNo(this.mNewGrpContNo);
            PubFun.fillDefaultField(tLBGrpAppntSchema);
            map.put(tLBGrpAppntSchema, "INSERT");

            tLCGrpAppntSet.get(i).setOperator(mGlobalInput.Operator);
            tLCGrpAppntSet.get(i).setModifyDate(this.mCurrDate);
            tLCGrpAppntSet.get(i).setModifyTime(this.mCurrTime);
        }
        map.put(tLCGrpAppntSet, "UPDATE");

        return true;
    }

    /**
     * 批量换号
     * 在未更换前，C表中的号是新号，B表中是旧号
     * 本方法用来交换C B表的号码
     */
    private void batchChangeInfo()
    {
        mTablesHashSet = new HashSet();
        String[] updateGrpContNoTable =
            {"LCPol", "LCPrem", "LCGet", "LCInsureAcc", "LCInsureAccClass",
            "LCInsureAccTrace", "LCInsureAccFee", "LCInsureAccClassFee",
            "LCInsured", "LCCont", "LCCustomerImpart", "LCCustomerImpartDetail",
            "LCCustomerImpartParams", "LCAppnt"};
        String[] updateGrpPolNoTable =
            {"LCPol", "LCInsureAcc", "LCInsureAccClass", "LCInsureAccTrace",
            "LCInsureAccFee", "LCInsureAccClassFee"};
        String[] updateContNoTable =
            {"LCPol", "LCDuty", "LCPrem", "LCGet", "LCInsureAcc",
            "LCInsureAccClass", "LCInsureAccTrace", "LCBnf", "LCInsureAccFee",
            "LCInsureAccClassFee", "LCInsured", "LCCont", "LCCustomerImpart",
            "LCCustomerImpartDetail", "LCCustomerImpartParams", "LCAppnt"};
        String[] updatePolNoTable =
            {"LCPol", "LCDuty", "LCPrem", "LCGet", "LCInsureAcc",
            "LCInsureAccClass", "LCInsureAccTrace", "LCBnf", "LCInsuredRelated",
            "LCPremToAcc", "LCGetToAcc", "LCInsureAccFee", "LCInsureAccClassFee",
        };

        String conditionC = " operator = '" + mGlobalInput.Operator
                            + "', modifyDate = '" + mCurrDate
                            + "', modifyTime = '" + mCurrTime + "' ";
        String conditionB = conditionC
                            + ",  makeDate = '" + mCurrDate
                            + "',  makeTime = '" + mCurrTime + "' ";
        changeGrpContNo(updateGrpContNoTable);
        changeGrpPolNo(updateGrpPolNoTable, conditionC, conditionB);
        changeContNo(updateContNoTable, conditionC, conditionB);
        changePolNo(updatePolNoTable, conditionC, conditionB);

        updateState();
    }

    /**
     * 更换团单号
     * @param updateGrpContNoTable String[]
     */
    private void changeGrpContNo(String[] updateGrpContNoTable)
    {
        String sql;
        //更换C B表的GrpContNO
        for (int i = 0; i < updateGrpContNoTable.length; i++)
        {
            sql = "update " + updateGrpContNoTable[i]
                  + " set grpContNo = '" + mLCGrpContSchema.getGrpContNo()
                  + "',  operator = '" + mGlobalInput.Operator
                  + "',  modifyDate = '" + mCurrDate
                  + "',  modifyTime = '" + mCurrTime + "' "
                  + "where grpContNo = '" + mNewGrpContNo + "' ";
            map.put(sql, "UPDATE");

            String tableB = updateGrpContNoTable[i].replaceFirst("C", "B");
            sql = "update " + tableB
                  + " set grpContNo = '" + mNewGrpContNo
                  + "',  operator = '" + mGlobalInput.Operator
                  + "',  modifyDate = '" + mCurrDate
                  + "',  modifyTime = '" + mCurrTime + "' "
                  + "where grpContNo = '"
                  + mLCGrpContSchema.getGrpContNo() + "' ";
            map.put(sql, "UPDATE");
            mTablesHashSet.add(updateGrpContNoTable[i]);
        }
    }

    /**
     * 更换团体险种号
     * @param updateGrpPolNoTable String[]
     * @param conditionC String
     * @param conditionB String
     */
    private void changeGrpPolNo(String[] updateGrpPolNoTable,
                                String conditionC,
                                String conditionB)
    {
        String sql;
        //更换C B表的grpPolNo
        for (int i = 0; i < updateGrpPolNoTable.length; i++)
        {
            String cdnC = "";
            String cdnB = "";
            if (!mTablesHashSet.contains(updateGrpPolNoTable[i]))
            {
                cdnC = ", " + conditionC;
                cdnB = ", " + conditionB;
                mTablesHashSet.add(updateGrpPolNoTable[i]);
            }
            sql = "update " + updateGrpPolNoTable[i] + " a "
                  + "set grpPolNo = "
                  + "     (select distinct grpPolNo "
                  + "     from LCRnewStateLog "
                  + "     where newGrpPolNo = a.grpPolNo "
                  + "        and newGrpContNo = '" + mNewGrpContNo + "') "
                  + cdnC
                  + "where grpContNo = '" + mNewGrpContNo + "' ";
            map.put(sql, "UPDATE");

            String tableB = updateGrpPolNoTable[i].toUpperCase()
                            .replaceFirst("C", "B");
            sql = "update " + tableB + " a "
                  + "set grpPolNo = "
                  + "     (select distinct newGrpPolNo "
                  + "     from LCRnewStateLog "
                  + "     where newGrpPolNo = a.grpPolNo "
                  + "        and newGrpContNo = '" + mNewGrpContNo + "') "
                  + cdnB
                  + "where grpContNo = '"
                  + mLCGrpContSchema.getGrpContNo() + "' ";
            map.put(sql, "UPDATE");
        }
    }

    /**
     * 更换个单号
     * 在未更换前，C表中的号是新号，B表中是旧号
     * 本方法用来交换C B表的号码
     * @param updateContNoTable String[]
     * @param conditionC String
     * @param conditionB String
     */
    private void changeContNo(String[] updateContNoTable,
                              String conditionC,
                              String conditionB)
    {
        String sql;
        //更换C B表的contNo
        for (int i = 0; i < updateContNoTable.length; i++)
        {
            String cdnC = "";
            String cdnB = "";
            if (!mTablesHashSet.contains(updateContNoTable[i]))
            {
                cdnC = ", " + conditionC;
                cdnB = ", " + conditionB;
                mTablesHashSet.add(updateContNoTable[i]);
            }
            sql = "update " + updateContNoTable[i] + " a "
                  + "set contNo = "
                  + "     (select distinct contNo "
                  + "     from LCRnewStateLog "
                  + "     where newContNo = a.contNo "
                  + "        and newGrpContNo = '" + mNewGrpContNo + "') "
                  + cdnC
                  + "where contNo in "
                  + "     (select newContNo "
                  + "     from LCRnewStateLog "
                  + "     where newGrpContNo = '" + mNewGrpContNo + "') ";
            map.put(sql, "UPDATE");
            String tableB = updateContNoTable[i].toUpperCase()
                            .replaceFirst("C", "B");
            sql = "update " + tableB + " a "
                  + "set contNo = "
                  + "     (select distinct newContNo "
                  + "     from LCRnewStateLog "
                  + "     where contNo = a.contNo "
                  + "        and newGrpContNo = '" + mNewGrpContNo + "') "
                  + cdnB
                  + "where contNo in "
                  + "     (select contNo "
                  + "     from LCRnewStateLog "
                  + "     where newGrpContNo = '" + mNewGrpContNo + "') ";
            map.put(sql, "UPDATE");
        }
    }

    /**
     * 更换个险号
     * @param updatePolNoTable String[]
     * @param conditionC String
     * @param conditionB String
     */
    private void changePolNo(String[] updatePolNoTable,
                             String conditionC,
                             String conditionB)
    {
        String sql;
        //更换C B表的polNo
        for (int i = 0; i < updatePolNoTable.length; i++)
        {
            String cdnC = "";
            String cdnB = "";
            if (!mTablesHashSet.contains(updatePolNoTable[i]))
            {
                cdnC = ", " + conditionC;
                cdnB = ", " + conditionB;
                mTablesHashSet.add(updatePolNoTable[i]);
            }
            sql = "update " + updatePolNoTable[i] + " a "
                  + "set polNo = "
                  + "     (select distinct polNo "
                  + "     from LCRnewStateLog "
                  + "     where newPolNo = a.polNo "
                  + "        and newGrpContNo = '" + mNewGrpContNo + "') "
                  + cdnC
                  + "where polNo in "
                  + "     (select newPolNo "
                  + "     from LCRnewStateLog "
                  + "     where newGrpContNo = '" + mNewGrpContNo + "') ";
            map.put(sql, "UPDATE");
            String tableB = updatePolNoTable[i].toUpperCase()
                            .replaceFirst("C", "B");
            sql = "update " + tableB + " a "
                  + "set polNo = "
                  + "     (select distinct newPolNo "
                  + "     from LCRnewStateLog "
                  + "     where polNo = a.polNo "
                  + "        and newGrpContNo = '" + mNewGrpContNo + "') "
                  + cdnB
                  + "where polNo in "
                  + "     (select polNo "
                  + "     from LCRnewStateLog "
                  + "     where newGrpContNo = '" + mNewGrpContNo + "') ";
            map.put(sql, "UPDATE");
        }
    }

    /**
     * 更改签单状态：appFlag
     * 保单状态: LCGrpCont,LCGrpPol,LCCont的state, LCPol的polState
     */
    private void updateState()
    {
        String[] tables = {"LCGrpCont", "LCGrpPol", "LCCont", "LCPol"};
        String[] tableAndValue = new String[2];
        for (int i = 0; i < tables.length; i++)
        {
            tableAndValue[0] = "appFlag";
            tableAndValue[1] = com.sinosoft.lis.bq.BQ.APPFLAG_SIGN;
            updateFields(tables[i], tableAndValue);
            tableAndValue[0] = "StateFlag";
            tableAndValue[1] = BQ.STATE_FLAG_SIGN;
            updateFields(tables[i], tableAndValue);

            tableAndValue[0] = (i != tables.length - 1) ? "state" : "polState";
            tableAndValue[1] = com.sinosoft.lis.bq.BQ.CONTSTATE_MJSIGN;
            updateFields(tables[i], tableAndValue);
        }
    }

    private void updateFields(String tableName, String[] fieldAndValue)
    {
            String sql = "update " + tableName
                         + " set " + fieldAndValue[0]
                         + " = '" + fieldAndValue[1] + "' "
                         + "where grpContNo = '"
                         + mLCGrpContSchema.getGrpContNo() + "' ";
            map.put(sql, "UPDATE");
    }

    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";

        LCGrpContSchema schema = new LCGrpContSchema();
        schema.setGrpContNo("0000000802");

        VData data = new VData();
        data.add(gi);
        data.add(schema);

        long a = System.currentTimeMillis();
        TransferCGDataToBG bl = new TransferCGDataToBG();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        System.out.println("交换续保数据花费："
                           + (System.currentTimeMillis() - a)/1000 + "秒");
    }
}
