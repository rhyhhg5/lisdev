package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.xb.GRnewDueFeeBL;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: Sinosoft</p>
 * @author lzj
 * @version 1.0
 */

public class GRnewDueFeeUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public GRnewDueFeeUI() {
  }
  public static void main(String[] args) {


    GRnewDueFeeUI GrpDueFeeUI1 = new GRnewDueFeeUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="pa0001";
    tGI.ManageCom="86";
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("StartDate","2018-07-02");
    tempTransferData.setNameAndValue("EndDate","2018-08-25");
    tempTransferData.setNameAndValue("ManageCom",tGI.ManageCom);

    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setGrpContNo("00114309000001");
    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tLCGrpContSchema);
    tVData.add(tempTransferData);
    GRnewDueFeeUI tGrpDueFeeUI = new GRnewDueFeeUI();
    tGrpDueFeeUI.submitData(tVData,"INSERT");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate){
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    GRnewDueFeeBL tGRnewDueFeeBL=new GRnewDueFeeBL();
    System.out.println("Start GrpDueFee UI Submit...");
    tGRnewDueFeeBL.submitData(mInputData,cOperate);

    System.out.println("End GrpDueFee UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tGRnewDueFeeBL .mErrors .needDealError() ){
       this.mErrors .copyAllErrors(tGRnewDueFeeBL.mErrors ) ;
       return false;
    }
    System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
