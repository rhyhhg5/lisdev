package com.sinosoft.lis.xb;

import java.util.Date;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.operfee.DuePayPersonFeeSimQueryUI;
import com.sinosoft.lis.operfee.LCDutyQueryUI;
import com.sinosoft.lis.operfee.LCPremQueryUI;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.LCContDB;


/**
 * <p>Title: </p>
 * <p>Description:
 * 进行续保催收，生成财务应收数据
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
public class PRnewManualDunBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private Reflections mReflections = new Reflections();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperater;
    private String mManageCom;
    private String mPolNo;
    private String mPrtNo; //印刷号
    private String mPrtSeq; //打印流水号
    private String mInsuredNo;
    private double mSumPay = 0; //保存累计和的变量
    private double mSubSumPay = 0; //保存累计和的变量
    private String tLimit = "";
    private String tNo = ""; //产生的应收总表通知书号
    private String serNo = ""; //应收总表流水号
    private String mMainRiskStatus = "0";

    //保单数据
    private LCPolSet mSubLCPolSet = new LCPolSet();
    private LCPolSchema mInLCPolSchema = new LCPolSchema();
    private LCPolSchema mMainLCPolSchema = new LCPolSchema();
//    private LCPolSchema mOldMainLCPolSchema = new LCPolSchema();
    private String mOldContNo = null;

    private TransferData mTransferData = null;

    //应收个人交费表
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();

    //因收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    /**
     * 存储外部的map，本类只对一个险种进行续保催收，由于会对险种生产打印管理字表信息，
     * 若出现两个riskCode相同的险种续保催收，多次调用本类将会出现打印管理子表将主键冲突
     * 故暂时使用从外部传入mmap进行校验的方式避免这种情况的发生
     * 除了本缘由外的所有变更都严厉禁止
     */
    private MMap mMMapOutSide = null;

    //操作时间戳
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();
    private LCRnewStateLogSet mLCRnewStateLogSet = new LCRnewStateLogSet();

    private double mAccBala = 0;  //客户帐户余额

    private MMap map = null;

    public PRnewManualDunBL()
    {
    }

    /**
     * PRnewManualDunBL
     * @param cInputData VData：包括
     * InLCPolSchema：保单信息
     * GlobalInput：用户信息
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        System.out.println("---1---");

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return null;
        }

        System.out.println("---PRnewManualDunBL getInputData---");

        //进行业务数据校验
        if (!checkData())
        {
            return null;
        }

        //进行业务处理
        if (!dealData())
        {
            return null;
        }

        System.out.println("---PRnewManualDunBL dealData---");

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return null;
        }

        System.out.println("---PRnewManualDunBL prepareOutputData---");

        return map;
    }

    /**
     * PRnewManualDunBL
     * @param cInputData VData：包括
     * InLCPolSchema：保单信息
     * GlobalInput：用户信息
     * @param cOperate String
     * @return MMap
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        getSubmitMap(cInputData, cOperate);
        if(map == null && mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        //数据提交
        PRnewManualDunBLS tPRnewManualDunBLS = new PRnewManualDunBLS();
        System.out.println("Start tPRnewManualDunBLS Submit...");

        if (!tPRnewManualDunBLS.submitData(data, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPRnewManualDunBLS.mErrors);

            CError tError = new CError();
            tError.moduleName = "tPRnewManualDunBLS";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            //this.mErrors .addOneError(tError) ;
            return false;
        }

        System.out.println("---PRnewManualDunBL commitData---");

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mInLCPolSchema = (LCPolSchema) cInputData
                         .getObjectByObjectName("LCPolSchema", 0);
        mTransferData = (TransferData) cInputData
                        .getObjectByObjectName("TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PRnewManualDunBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;

        if ((mOperater == null) || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PRnewManualDunBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operater失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;

        if ((mManageCom == null) || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PRnewManualDunBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得业务数据
        if (mInLCPolSchema == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PRnewManualDunBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        mPrtNo = mInLCPolSchema.getPrtNo();

        if (mPrtNo == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "PRnewManualDunBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据PrtNo失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {
        //校验待续保保单状态信息
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogDB.setPrtNo(mPrtNo);
        tLCRnewStateLogDB.setNewPolNo(mInLCPolSchema.getPolNo());
        mLCRnewStateLogSet = tLCRnewStateLogDB.query();

        if ((mLCRnewStateLogSet == null) || (mLCRnewStateLogSet.size() == 0))
        {
            CError tError = new CError();
            tError.moduleName = "PRnewManualDunBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单" + mPrtNo + "续保状态表信息查询失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //死亡报案的不能催收
        ExeSQL tExeSQL = new ExeSQL();
        String count = tExeSQL.getOneValue(
                "select count(*) from ldsystrace WHERE polstate=4001 AND valiflag='1' and polno='" +
                mInLCPolSchema.getPolNo() + "'");

        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.addOneError("保单号:" + mInLCPolSchema.getPolNo() +
                                     "查询死亡报案时错误:" +
                                     tExeSQL.mErrors.getFirstError());

            return false;
        }

        if ((count != null) && !count.equals("0"))
        {
            this.mErrors.addOneError("保单号:" + mInLCPolSchema.getPolNo() +
                                     "催收错误:已有死亡报案记录!");

            return false;
        }

        //如果正在做保全，不能催收
        tExeSQL = new ExeSQL();
        count = null;
        count = tExeSQL.getOneValue(
                "SELECT COUNT(*) FROM lpedormain WHERE edorstate<>'0' AND contNo='" +
                mInLCPolSchema.getContNo() + "'");

        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.addOneError("保单号:" + mInLCPolSchema.getPolNo() +
                                     "查询保全状态时错误:" +
                                     tExeSQL.mErrors.getFirstError());

            return false;
        }

        if ((count != null) && !count.equals("0"))
        {
            this.mErrors.addOneError("保单号:" + mInLCPolSchema.getPolNo() +
                                     "催收错误:正在做保全!请手工催收!");

            return false;
        }

        for (int i = 1; i <= mLCRnewStateLogSet.size(); i++)
        {
            //校验待续保保单状态是否处于待催收状态信息
            if ((mLCRnewStateLogSet.get(i).getState() == null) ||
                !mLCRnewStateLogSet.get(i).getState().equals("3"))
            {
                CError tError = new CError();
                tError.moduleName = "PRnewManualDunBL";
                tError.functionName = "checkData";
                tError.errorMessage = "保单印刷号〔" + mPrtNo +
                                      "〕的续保保单中有不处于待催发状态的投保单,请根据印刷号查询续保状态!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //校验待续保投保单的主险是否处于待续期催收，若主险续期，附加险续保
            if ((mLCRnewStateLogSet.get(i).getMainRiskStatus() != null) &&
                mLCRnewStateLogSet.get(i).getMainRiskStatus().trim().equals("1"))
            {
                mMainRiskStatus = "1";

                //准备主险数据
                String tString = "  select * "
                                 + "from lcpol "
                                 + "where polNo = ("
                                 + "   select mainPolNo "
                                 + "   from LCPol "
                                 + "   where polNo = '"
                                 + mLCRnewStateLogSet.get(i).getPolNo() + "') ";
                System.out.println(tString);
                LCPolDB tLCPolDB = new LCPolDB();
                LCPolSet tLCPolSet = tLCPolDB.executeQuery(tString);
                if ((tLCPolSet == null) || (tLCPolSet.size() < 1))
                {
                    CError tError = new CError();
                    tError.moduleName = "PRnewManualDunBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "印刷号为〔" + mPrtNo + "〕的主险保单表信息查询失败!";
                    this.mErrors.addOneError(tError);

                    return false;
                }

                mMainLCPolSchema = tLCPolSet.get(1);
            }

            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCRnewStateLogSet.get(i).getNewPolNo()); //获得原主险信息
            LCPolSet tLCPolSet = tLCPolDB.query();

            if((tLCPolSet == null) || (tLCPolSet.size() != 1))
            {
                CError tError = new CError();
                tError.moduleName = "PRnewManualDunBL";
                tError.functionName = "checkData";
                tError.errorMessage = "主险保单" +
                                      mLCRnewStateLogSet.get(i).getPolNo() +
                                      "表信息查询失败!";
                this.mErrors.addOneError(tError);

                return false;
            }

            //校验待续保投保单中是否已有处于待续保催收的主险
            if ((mLCRnewStateLogSet.get(i).getRiskFlag() != null) &&
                mLCRnewStateLogSet.get(i).getRiskFlag().trim().equals("M"))
            {
//                LCPolDB tLCPolDB = new LCPolDB();
//                LCPolSet tLCPolSet = new LCPolSet();
//                tLCPolDB.setPolNo(mLCRnewStateLogSet.get(i).getPolNo()); //获得原主险信息
//                tLCPolSet = tLCPolDB.query();

                mMainLCPolSchema = tLCPolSet.get(1);
                mMainRiskStatus = "2";
            }
            else
            {
                mSubLCPolSet.add(tLCPolSet.get(1));
            }
        }

        //校验当待续保投保单的主险是否处于待续期催收校验其主险是否趸交或不定期交。否则-记录日志
        if (mMainRiskStatus.equals("0"))
        {
            String tString = "select count(*) from lcpol where prtno='" +
                             mPrtNo + "' and polno = mainpolno and payintv >0";

            String tReSult = new String();
            tExeSQL = new ExeSQL();
            tReSult = tExeSQL.getOneValue(tString);

            if (tExeSQL.mErrors.needDealError() || (tReSult == null) ||
                tReSult.equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "PRnewManualDunBL";
                tError.functionName = "checkData";
                tError.errorMessage = "执行SQL语句：" + tString + "失败!";
                this.mErrors.addOneError(tError);

                return false;
            }

            if (tReSult.equals("0"))
            {
                mMainRiskStatus = "0";
            }
            else
            {
                mMainRiskStatus = "3"; //需要在日志中添加该错误信息
            }

            //准备主险数据
            tString = "select * from lcpol where prtno='" + mPrtNo +
                      "' and polno = mainpolno and appflag ='1' and StateFlag ='"
                      +BQ.STATE_FLAG_SIGN+"'";

            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = new LCPolSet();
            tLCPolSet = tLCPolDB.executeQuery(tString);

            if ((tLCPolSet == null) || (tLCPolSet.size() < 1))
            {
                CError tError = new CError();
                tError.moduleName = "PRnewManualDunBL";
                tError.functionName = "checkData";
                tError.errorMessage = "印刷号为〔" + mPrtNo + "〕的主险保单表信息查询失败!";
                this.mErrors.addOneError(tError);

                return false;
            }

            mMainLCPolSchema = tLCPolSet.get(1);
        }

//        //准备主险数据
//        String tString = "  select * "
//                         + "from lcpol "
//                         + "where polNo = '"
//                         + mLCRnewStateLogSet.get(1).getPolNo() +
//                         "' and polno = mainpolno and appflag ='1'";
//        LCPolDB tLCPolDB = new LCPolDB();
//        LCPolSet tLCPolSet = new LCPolSet();
//        tLCPolSet = tLCPolDB.executeQuery(tString);
//
//        if ((tLCPolSet == null) || (tLCPolSet.size() < 1))
//        {
//            CError tError = new CError();
//            tError.moduleName = "PRnewManualDunBL";
//            tError.functionName = "checkData";
//            tError.errorMessage = "印刷号为〔" + mPrtNo + "〕的主险保单表信息查询失败!";
//            this.mErrors.addOneError(tError);
//
//            return false;
//        }
//
//        mOldMainLCPolSchema = tLCPolSet.get(1);

        mOldContNo = mLCRnewStateLogSet.get(1).getContNo();

        // 处于未打印状态的续保催收通知书在打印队列中只能有一个
        // 条件：同一个单据类型，同一个其它号码，同一个其它号码类型
//        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
//
//        tLOPRTManagerDB.setCode(PrintManagerBL.CODE_REPAY); //
//        tLOPRTManagerDB.setOtherNo(mOldContNo);
//        tLOPRTManagerDB.setOtherNoType(PrintManagerBL.ONT_INDPOL); //保单号
//        tLOPRTManagerDB.setStateFlag("0");
//
//        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
//
//        if (tLOPRTManagerSet == null)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "PRnewManualDunBL";
//            tError.functionName = "preparePrint";
//            tError.errorMessage = "查询打印管理表" + mPrtNo + "信息出错!";
//            this.mErrors.addOneError(tError);
//
//            return false;
//        }
//
//        if (tLOPRTManagerSet.size() > 0)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "PRnewManualDunBL";
//            tError.functionName = "preparePrint";
//            tError.errorMessage =
//                    "在打印队列中已有处于待打印的催收通知书,即部分数据已经发过催收，可以做续保催收撤销后重新发催收!";
//            this.mErrors.addOneError(tError);
//
//            return false;
//        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        createNO();

        //校验当待续保投保单的主险是趸交或不定期交,所以只处理附加险续保催收问题
        if (mMainRiskStatus.equals("0"))
        {

            if (preparePrint() == false)
            {
                return false;
            }

            //准备附加险数据
            for (int i = 1; i <= mSubLCPolSet.size(); i++)
            {
                ////准备附加险续期催收分项表数据
                if (PrepareSubPay(mSubLCPolSet.get(i), "2") == false)
                {
                    return false;
                }

                // 准备应打印子表附加险数据信息
                if (prepareSubPrint(mSubLCPolSet.get(i), "2") == false)
                {
                    return false;
                }
            }

            //准备应收总表数据
            if (PreparePay(mMainLCPolSchema, "2") == false)
            {
                return false;
            }

            //修改－Houzm 因为主险是趸交或不定期交,所以交费的日期必须取附加险的交费日期
            String unit = "M"; //日期单位（Y:年 M:月 D:日）
            FDate fDate = new FDate();
            Date payDate = PubFun.calDate(fDate.getDate(
                    mSubLCPolSet.get(1).getPaytoDate()), 2, unit, null);
            String strPayDate = fDate.getString(payDate);
            mLJSPaySchema.setPayDate(strPayDate); //失效日期－截止交费日期
            mLJSPaySchema.setStartPayDate(mSubLCPolSet.get(1).getPaytoDate()); //最早交费日期
        }

        //待续保投保单的主险处于待续期催收
        if (mMainRiskStatus.equals("1"))
        {
            if (preparePrint() == false)
            {
                return false;
            }

            //准备主险续期催收分项表数据
            if (PrepareSubPay(mMainLCPolSchema, "1") == false)
            {
                return false;
            }

            //？打印子表中添加一条主险数据
            if (prepareSubPrint(mMainLCPolSchema, "1") == false)
            {
                return false;
            }

            //准备附加险数据
            for (int i = 1; i <= mSubLCPolSet.size(); i++)
            {
                ////准备附加险续期催收分项表数据
                if (PrepareSubPay(mSubLCPolSet.get(i), "2") == false)
                {
                    return false;
                }

                // 准备应打印子表附加险数据信息
                if (prepareSubPrint(mSubLCPolSet.get(i), "2") == false)
                {
                    return false;
                }
            }

            //准备应收总表数据
            if (PreparePay(mMainLCPolSchema, "2") == false)
            {
                return false;
            }
        }

        //校验待续保投保单中是否已有处于待续保催收的主险
        if (mMainRiskStatus.equals("2"))
        {
            if (preparePrint() == false)
            {
                return false;
            }

            //准备主险续期催收分项表数据
            if (PrepareSubPay(mMainLCPolSchema, "2") == false)
            {
                return false;
            }

            //？打印子表中添加一条主险数据
            if (prepareSubPrint(mMainLCPolSchema, "2") == false)
            {
                return false;
            }

            //准备附加险数据
            for (int i = 1; i <= mSubLCPolSet.size(); i++)
            {
                //准备附加险续期催收分项表数据
                if (PrepareSubPay(mSubLCPolSet.get(i), "2") == false)
                {
                    return false;
                }

                // 准备应打印子表附加险数据信息
                if (prepareSubPrint(mSubLCPolSet.get(i), "2") == false)
                {
                    return false;
                }
            }

            //准备应收总表数据
            if (PreparePay(mMainLCPolSchema, "2") == false)
            {
                return false;
            }
        }

        //报错处理-2种情况
        //1-主险未发催收，去个案做续期催收
        //2-主险已经发催收，但是在附加险续保申请之前，因此需要先撤销主险的续期催收数据，再重新发主险的续期催收
        if (mMainRiskStatus.equals("3"))
        {
            //查询应收总表是否有数据
            LJSPayDB tLJSPayDB = new LJSPayDB();
            tLJSPayDB.setOtherNo(mMainLCPolSchema.getPolNo());
            tLJSPayDB.setOtherNoType("2");

            LJSPaySet tLJSPaySet = tLJSPayDB.query();

            if (tLJSPaySet.size() > 0)
            {
                CError.buildErr(this,
                                "主险催收在续保申请之前已经操作，请如下操作：1-对该主险做续期催收撤销 2-重新对该主险做催期催收 3-发续保催收通知书");

                return false;
            }
            else
            {
                CError.buildErr(this, "主险尚未发续期催收，请在发续保催收通知书之前对待续期的主险进行催收");

                return false;
            }
        }

        if (prepareData() == false)
        {
            return false;
        }

        return true;
    }

    private void createNO()
    {
        //得到相应流水号

        if(this.mMMapOutSide != null && mMMapOutSide.size() != 0)
        {
            LJSPaySchema schema = (LJSPaySchema) mMMapOutSide
                                  .getObjectByObjectName("LJSPaySchema", 0);
            if(schema != null)
            {
                tNo = schema.getGetNoticeNo();
                serNo = schema.getSerialNo();
            }
            else
            {
                // 产生通知书号
                tLimit = PubFun.getNoLimit(mMainLCPolSchema.getManageCom());
                tNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

                // 产生统一流水号
                tLimit = PubFun.getNoLimit(mMainLCPolSchema.getManageCom());
                serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            }

            LOPRTManagerSchema tLOPRTManagerSchema
                = (LOPRTManagerSchema) mMMapOutSide
                  .getObjectByObjectName("LOPRTManagerSchema", 0);
            if(tLOPRTManagerSchema == null)
            {
                LOPRTManagerSubSchema tLOPRTManagerSubSchema
                    = (LOPRTManagerSubSchema) mMMapOutSide
                      .getObjectByObjectName("LOPRTManagerSubSchema", 0);
                if(tLOPRTManagerSubSchema != null)
                {
                    mPrtSeq = tLOPRTManagerSubSchema.getPrtSeq();
                }
                else
                {
                    //产生打印流水号
                    String strNoLimit = PubFun.getNoLimit(mMainLCPolSchema.
                        getManageCom());
                    mPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", strNoLimit);
                }
            }
            else
            {
                mPrtSeq = tLOPRTManagerSchema.getPrtSeq();
            }
        }
        else
        {
            if(mTransferData != null)
            {
                tNo = (String) mTransferData.getValueByName("PayNoticeNo");
                serNo = (String) mTransferData.getValueByName("SerialNo");
                mPrtSeq = (String) mTransferData.getValueByName("PrtSeqNo");
            }
            if(tNo != null && serNo != null && mPrtSeq != null)
            {
                return;
            }

            // 产生通知书号
            tLimit = PubFun.getNoLimit(mMainLCPolSchema.getManageCom());
            tNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

            // 产生统一流水号
            tLimit = PubFun.getNoLimit(mMainLCPolSchema.getManageCom());
            serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

            //产生打印流水号
            String strNoLimit = PubFun.getNoLimit(mMainLCPolSchema.
                                                  getManageCom());
            mPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", strNoLimit);
        }
    }

    //准备提交数据
    private boolean prepareOutputData()
    {
        map = new MMap();

        //添加续保通知数据
        if ((mLOPRTManagerSchema != null) &&
            (mLOPRTManagerSchema.getPrtSeq() != null) &&
            !mLOPRTManagerSchema.getPrtSeq().trim().equals(""))
        {
            map.put(mLOPRTManagerSchema, "DELETE&INSERT");
        }

        //添加续保通知子表数据
        if ((mLOPRTManagerSubSet != null) && (mLOPRTManagerSubSet.size() > 0))
        {
            map.put(mLOPRTManagerSubSet, "INSERT");
        }

        //得到保单数据，存储缴费方式
        String bankCode = "";
        String bankAccNo = "";
        LCContSchema tLCContSchema = getLCContInfo(this.mOldContNo);
        if(tLCContSchema != null)
        {
            if(tLCContSchema.getPayMode().equals("3")
               || tLCContSchema.getPayMode().equals("4")
               || tLCContSchema.getPayMode().equals("5"))
            {
                bankCode = tLCContSchema.getBankCode();
                bankAccNo = tLCContSchema.getBankAccNo();
            }

        }
        //添加续保应收总表数据
        if ((mLJSPaySchema != null) &&
            (mLJSPaySchema.getGetNoticeNo() != null) &&
            !mLJSPaySchema.getGetNoticeNo().trim().equals(""))
        {
            LJSPaySchema dueFeeLJSPaySchema = null;
            if(mMMapOutSide != null)
            {
                dueFeeLJSPaySchema
                    = (LJSPaySchema) mMMapOutSide
                      .getObjectByObjectName("LJSPaySchema", 0);
            }
            //若已存在应收总表数据，则只需要求和
            if(dueFeeLJSPaySchema != null)
            {
                dueFeeLJSPaySchema.setSumDuePayMoney(
                    dueFeeLJSPaySchema.getSumDuePayMoney()
                    + mLJSPaySchema.getSumDuePayMoney());  //这里使用的是引用，不需要再次放到map中

                LJSPayBSchema dueFeeLJSPayBSchema
                    = (LJSPayBSchema)mMMapOutSide
                      .getObjectByObjectName("LJSPayBSchema", 0);
                dueFeeLJSPayBSchema.setSumDuePayMoney(
                    dueFeeLJSPayBSchema.getSumDuePayMoney()
                    + mLJSPaySchema.getSumDuePayMoney());

                dueFeeLJSPaySchema.setBankAccNo(bankAccNo);
                dueFeeLJSPaySchema.setBankCode(bankCode);
                dueFeeLJSPayBSchema.setBankAccNo(bankAccNo);
                dueFeeLJSPayBSchema.setBankCode(bankCode);
            }
            else
            {
                map.put(mLJSPaySchema, "DELETE&INSERT");
                //b表备份
                LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
                mReflections.transFields(tLJSPayBSchema, mLJSPaySchema);
                tLJSPayBSchema.setDealState("0");
                map.put(tLJSPayBSchema, "DELETE&INSERT");

                mLJSPaySchema.setBankAccNo(bankAccNo);
                mLJSPaySchema.setBankCode(bankCode);
                tLJSPayBSchema.setBankAccNo(bankAccNo);
                tLJSPayBSchema.setBankCode(bankCode);
            }
        }


        //添加续保应收分类表数据
        if((mLJSPayPersonSet != null) && (mLJSPayPersonSet.size() > 0))
        {
            map.put(mLJSPayPersonSet, "DELETE&INSERT");

            //b表备份
            for(int i = 1; i <= mLJSPayPersonSet.size(); i++)
            {
                mLJSPayPersonSet.get(i).setBankAccNo(bankAccNo);
                mLJSPayPersonSet.get(i).setBankCode(bankCode);

                LJSPayPersonBSchema schema = new LJSPayPersonBSchema();
                mReflections.transFields(schema, mLJSPayPersonSet.get(i));
                schema.setDealState("0");
                schema.setBankAccNo(bankAccNo);
                schema.setBankCode(bankCode);
                map.put(schema, "DELETE&INSERT");
            }
        }

        //添加体检通知书打印管理表数据
        if ((mLCRnewStateLogSet != null) && (mLCRnewStateLogSet.size() > 0))
        {
            map.put(mLCRnewStateLogSet, "UPDATE");
        }

        return true;
    }

    /**
     * 打印信息表
     * @return
     */
    private boolean prepareData()
    {
        for (int i = 1; i <= mLCRnewStateLogSet.size(); i++)
        {
            mLCRnewStateLogSet.get(i).setState("4");
            mLCRnewStateLogSet.get(i).setModifyDate(CurrentDate);
        }

        return true;
    }

    /**
     * 打印信息表
     * @return
     */
    private boolean preparePrint()
    {
        //准备打印管理表数据
        mLOPRTManagerSchema.setPrtSeq(mPrtSeq);
        mLOPRTManagerSchema.setOtherNo(mOldContNo);
        mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL); // 印刷号
        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY); //续保通知书
        mLOPRTManagerSchema.setManageCom(mMainLCPolSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mMainLCPolSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);

        //mLOPRTManagerSchema.setExeCom();
        //mLOPRTManagerSchema.setExeOperator();
        mLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT); //前台打印
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setPatchFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        //mLOPRTManagerSchema.setDoneDate() ;
        //mLOPRTManagerSchema.setDoneTime();
        mLOPRTManagerSchema.setStandbyFlag1(mMainLCPolSchema.getInsuredNo()); //被保险人编码
        mLOPRTManagerSchema.setStandbyFlag2(tNo); //续保催收应收总表号
        mLOPRTManagerSchema.setOldPrtSeq(mPrtSeq);

        return true;
    }

    /**
     * 打印信息表
     * @return
     */
    private boolean prepareSubPrint(LCPolSchema tLCPolSchema, String tType)
    {
        //准备打印管理表数据
        if(mMMapOutSide != null)
        {
            //本段代码说明见mMMapOutSide申明出
            Object[] tLOPRTManagerSubSets
                = mMMapOutSide.getAllObjectByObjectName("LOPRTManagerSubSet", 0);
            for(int i = 0; i < tLOPRTManagerSubSets.length; i++)
            {
                LOPRTManagerSubSet set
                    = (LOPRTManagerSubSet)tLOPRTManagerSubSets[i];
                for(int j = 1; j <= set.size(); j++)
                {
                    if(set.get(j).getRiskCode().equals(tLCPolSchema.getRiskCode()))
                    {
                        set.get(j).setDuePayMoney(set.get(j).getDuePayMoney()
                                                  + mSubSumPay);
                        return true;
                    }
                }
            }
        }

        LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();
        tLOPRTManagerSubSchema.setPrtSeq(mPrtSeq);
        tLOPRTManagerSubSchema.setGetNoticeNo(tNo);
        tLOPRTManagerSubSchema.setOtherNo(tLCPolSchema.getPolNo());
        tLOPRTManagerSubSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL); // 保单号
        tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
        tLOPRTManagerSubSchema.setAppntName(tLCPolSchema.getAppntName());
        tLOPRTManagerSubSchema.setDuePayMoney(mSubSumPay);
        tLOPRTManagerSubSchema.setTypeFlag(tType); //１续期；２续保
        mLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);

        return true;
    }

    /**
     * 打印信息表
     * @return
     */
    private boolean PreparePay(LCPolSchema tLCPolSchema, String tType)
    {
        mLJSPaySchema.setGetNoticeNo(tNo);
        mLJSPaySchema.setSumDuePayMoney(mSumPay);
//        mLJSPaySchema.setOtherNo(mOldMainLCPolSchema.getPolNo()); //旧主险保单号
        mLJSPaySchema.setOtherNo(mOldContNo);
        mLJSPaySchema.setOtherNoType("2");
        mLJSPaySchema.setAppntNo(tLCPolSchema.getAppntNo());

        //计算交费日期 =个人保单表交至日期+2个月
        String strPayDate = "";
        Date baseDate = new Date();
        Date paytoDate = new Date(); //交至日期
        Date payDate = new Date(); //交费日期
        FDate fDate = new FDate();

        if ((tLCPolSchema.getPaytoDate() != null) &&
            !tLCPolSchema.getPaytoDate().equals(""))
        {
            paytoDate = fDate.getDate(tLCPolSchema.getPaytoDate());
        }
        else
        {
            paytoDate = fDate.getDate(tLCPolSchema.getCValiDate());
        }

        if (paytoDate != null) //下面的函数第一个参数不能为空
        {
            payDate = PubFun.calDate(paytoDate, 2, "M", null);
            strPayDate = fDate.getString(payDate);
        }
        else
        {
            return false;
        }

        mLJSPaySchema.setPayDate(strPayDate);

        if ((tLCPolSchema.getPaytoDate() != null) &&
            !tLCPolSchema.getPaytoDate().equals(""))
        {
            mLJSPaySchema.setStartPayDate(tLCPolSchema.getPaytoDate()); //交费最早应缴日期保存上次交至日期
        }
        else
        {
            mLJSPaySchema.setStartPayDate(tLCPolSchema.getFirstPayDate()); //交费最早应缴日期保存上次交至日期
        }

        mLJSPaySchema.setBankOnTheWayFlag("0");
        mLJSPaySchema.setBankSuccFlag("0");
        mLJSPaySchema.setSendBankCount(0); //送银行次数
        mLJSPaySchema.setApproveCode(tLCPolSchema.getApproveCode());
        mLJSPaySchema.setApproveDate(tLCPolSchema.getApproveDate());
        mLJSPaySchema.setRiskCode(tLCPolSchema.getRiskCode());
        /*Lis5.3 upgrade get
                 mLJSPaySchema.setBankAccNo(tLCPolSchema.getBankAccNo());
                 mLJSPaySchema.setBankCode(tLCPolSchema.getBankCode());
                 mLJSPaySchema.setAccName(tLCPolSchema.getAccName());
         */
        mLJSPaySchema.setSerialNo(serNo); //流水号
        mLJSPaySchema.setOperator(mOperater);

        if (tType.equals("2"))
        {
            mLJSPaySchema.setPayTypeFlag("1");
        }

        mLJSPaySchema.setManageCom(tLCPolSchema.getManageCom());
        mLJSPaySchema.setAgentCom(tLCPolSchema.getAgentCom());
        mLJSPaySchema.setAgentCode(tLCPolSchema.getAgentCode());
        mLJSPaySchema.setAgentType(tLCPolSchema.getAgentType());
        mLJSPaySchema.setAgentGroup(tLCPolSchema.getAgentGroup());
        mLJSPaySchema.setMakeDate(CurrentDate);
        mLJSPaySchema.setMakeTime(CurrentTime);
        mLJSPaySchema.setModifyDate(CurrentDate);
        mLJSPaySchema.setModifyTime(CurrentTime);

        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean PrepareSubPay(LCPolSchema tempLCPolSchema, String tType)
    {
        VData saveData = new VData();

        /** 数据表  保存数据*/

        //个人保单表
        LCPolSet mLCPolSet = new LCPolSet();
        LCPolSchema mLCPolSchema = new LCPolSchema();

        //保费项表
        LCPremSet mLCPremSet = new LCPremSet();
        LCPremSchema mLCPremSchema = new LCPremSchema();

        //保险责任表
        LCDutySet mLCDutySet = new LCDutySet();
        LCDutySchema mLCDutySchema = new LCDutySchema();

        //Step 1 : set variable
        String PolNo = tempLCPolSchema.getPolNo();

        //判断个人保单表中是否有余额的标志
        boolean yelFlag = false; //默认没有
        LJSPayPersonSchema yelLJSPayPersonSchema; //如果有余额，添加一个应收个人交费schema
        double LeavingMoney = 0; //如果有余额，保存余额

        //判断余额是否>(保费项表.实际保费*保险责任表.免交比率)累计和 的标志
        boolean yetFlag = false; //默认不是（yelFlag=true 是前提条件）
        LJSPayPersonSchema yetLJSPayPersonSchema; //如果余额>累计和，添加一个应收个人交费schema

        //保存纪录的条数
        int recordCount = 0;
        int i = 0;
        int iMax = 0;
        mSubSumPay = 0;

        //Step 2 :query from LCPol table
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo(PolNo);
        tLCPolSchema.setManageCom(mManageCom);

        //tLCPolSchema.setRiskCode("Y");//设置催交标志为Y
        //tLCPolSchema.setPayIntv(-1);//设置交费间隔不等于0:注意是!=0
        VData tVData = new VData();

        //tVData.add(tLCPolSchema);
        //LCPolQueryUI tLCPolQueryUI = new LCPolQueryUI();
        //if(!tLCPolQueryUI.submitData(tVData,"QUERY"))
        //{
        //  this.mErrors .copyAllErrors(tLCPolQueryUI.mErrors ) ;
        //  return false;
        //}
        //tVData.clear();
        //mLCPolSet = new LCPolSet();
        //tVData = tLCPolQueryUI.getResult();
        //mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
        //tLCPolSchema = (LCPolSchema)mLCPolSet.get(1); //保存后用
        tLCPolSchema.setSchema(tempLCPolSchema);

        //计算新的交至日期
        FDate tD = new FDate();
        Date newBaseDate = new Date();
        String CurrentPayToDate;

        if ((tLCPolSchema.getPaytoDate() != null) &&
            !tLCPolSchema.getPaytoDate().equals(""))
        {
            //续保险种的payToDate是在签单时计算的，这里的PayToDate还是原来的值
            CurrentPayToDate = tLCPolSchema.getPaytoDate();
        }
        else
        {
            CurrentPayToDate = tLCPolSchema.getCValiDate();
        }
        System.out.println(CurrentPayToDate);

        int PayInterval = tLCPolSchema.getPayIntv();
        newBaseDate = tD.getDate(CurrentPayToDate);

        Date NewPayToDate;

        if (PayInterval == 0)
        {
            NewPayToDate = tD.getDate(tLCPolSchema.getPayEndDate());
        }
        else
        {
            NewPayToDate = PubFun.calDate(newBaseDate, PayInterval, "M", null);
        }

        String strNewPayToDate = tD.getString(NewPayToDate);
        System.out.println("现交至日期" + strNewPayToDate);

        //交费日期=失效日期=原交至日期+2个月
        Date NewPayDate = PubFun.calDate(newBaseDate, 2, "M", null);
        String strNewPayDate = tD.getString(NewPayDate);

        //Step 3: query  from LJSPayPerson table
        tVData.clear();

        LJSPayPersonSchema tempLJSPayPersonSchema = new LJSPayPersonSchema();
        tempLJSPayPersonSchema.setPolNo(PolNo);
        tVData.add(tempLJSPayPersonSchema);

        DuePayPersonFeeSimQueryUI tDuePayPersonFeeSimQueryUI = new
                DuePayPersonFeeSimQueryUI();

        if (!tDuePayPersonFeeSimQueryUI.submitData(tVData, "QUERY"))
        {
            this.mErrors.copyAllErrors(tDuePayPersonFeeSimQueryUI.mErrors);

            return false;
        }

        //Step 4:query from LCPrem table
        tVData.clear();

        LCPremSchema tLCPremSchema = new LCPremSchema();
        tLCPremSchema.setPolNo(PolNo);

        if (tType.equals("1"))
        {
            tLCPremSchema.setUrgePayFlag("Y");
        }

        tVData.add(tLCPremSchema);

        LCPremQueryUI tLCPremQueryUI = new LCPremQueryUI();

        if (!tLCPremQueryUI.submitData(tVData, "QUERY"))
        {
            this.mErrors.copyAllErrors(tLCPremQueryUI.mErrors);

            return false;
        }

        tVData.clear();
        mLCPremSet = new LCPremSet();
        tVData = tLCPremQueryUI.getResult();

        //可能有多条纪录，所以保存记录集
        mLCPremSet.set((LCPremSet) tVData.getObjectByObjectName("LCPremSet", 0));

        //Step 4 : query from LCDuty
        recordCount = mLCPremSet.size();
        System.out.println("recordCount=" + recordCount);
        mLCDutySet = new LCDutySet(); //保存保险责任表纪录集合

        LCDutySchema tLCDutySchema = new LCDutySchema();
        LCDutySet tempLCDutySet = new LCDutySet(); //临时存放保险责任表纪录集合的容器

        LCPremSet saveLCPremSet = new LCPremSet(); //保存过滤后的保费项纪录集

        for (i = 1; i <= recordCount; i++)
        {
            tLCPremSchema = new LCPremSchema();
            tLCPremSchema = (LCPremSchema) mLCPremSet.get(i);
            tLCDutySchema.setPolNo(tLCPremSchema.getPolNo());
            tLCDutySchema.setDutyCode(tLCPremSchema.getDutyCode());
            tVData.clear();
            tVData.add(tLCDutySchema);

            //*.java内置查询条件:免交比率<1.0
             LCDutyQueryUI tLCDutyQueryUI = new LCDutyQueryUI();

            if (!tLCDutyQueryUI.submitData(tVData, "QUERY"))
            {
                System.out.println("没有查到符合条件的责任纪录");
            }
            else
            {
                tVData.clear();
                tVData = tLCDutyQueryUI.getResult();
                tempLCDutySet.set((LCDutySet) tVData.getObjectByObjectName(
                        "LCDutySet", 0));
                tLCDutySchema = (LCDutySchema) tempLCDutySet.get(1);
                mLCDutySet.add(tLCDutySchema);
                saveLCPremSet.add(tLCPremSchema);
                mSubSumPay = mSubSumPay +
                             ((1 - tLCDutySchema.getFreeRate()) *
                              tLCPremSchema.getPrem());
            }
        }

        //end for()
        if (saveLCPremSet.size() == 0) //如果过滤后的保费项表纪录数=0
        {
            this.mErrors.addOneError("查询保险责任表失败，原因是:都为免交");

            return false;
        }

        //Step 5:
        yelLJSPayPersonSchema = new LJSPayPersonSchema();
        yetLJSPayPersonSchema = new LJSPayPersonSchema();

        //计算交费日期和现交至日期 而定义一些处理变量：
        Date baseDate = new Date();
        Date paytoDate = new Date(); //交至日期
        Date payDate = new Date(); //交费日期
        int interval = 0; //交费间隔
        String unit = ""; //日期单位（Y:年 M:月 D:日）
        FDate fDate = new FDate();

        //取一条保费项表纪录，将相关字段值赋给添加的应收个人表字段
        tLCPremSchema = (LCPremSchema) saveLCPremSet.get(1);

        AppAcc tAppAcc = new AppAcc();
        LCAppAccSchema tLCAppAccSchema
            = tAppAcc.getLCAppAcc(tLCPolSchema.getAppntNo());
        double appAccBala = tLCAppAccSchema.getAccGetMoney();

        if (appAccBala > 0) //如果有余额，根据余额设置一条应收个人Schema
        {
            yelFlag = true;
            LeavingMoney = appAccBala;
            yelLJSPayPersonSchema.setPolNo(PolNo);
            yelLJSPayPersonSchema.setSumDuePayMoney( -tLCPolSchema.
                    getLeavingMoney());
            yelLJSPayPersonSchema.setSumActuPayMoney( -tLCPolSchema.
                    getLeavingMoney());
            yelLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() + 1);
            yelLJSPayPersonSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
            yelLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
            yelLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
            yelLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
            yelLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());

            // 产生通知书号
            //tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
            //tNo=PubFun1.CreateMaxNo("PAYNOTICENO",tLimit);
            yelLJSPayPersonSchema.setGetNoticeNo(tNo);
            yelLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
            yelLJSPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());

            //repair:交费日期=个人保单表的交至日期
            yelLJSPayPersonSchema.setPayDate(strNewPayDate);
            yelLJSPayPersonSchema.setPayType("YEL"); //交费类型=yel

            //repair:保险帐户号码=待定
            yelLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo());

            //repair:这条纪录的//原交至日期=个人保单表的交至日期
            //现交至日期CurPayToDate=个人保单表的交至日期+交费间隔 待修改
            if ((tLCPolSchema.getPaytoDate() != null) &&
                !tLCPolSchema.getPaytoDate().equals(""))
            {
                yelLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.
                        getPaytoDate());
            }
            else
            {
                yelLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.
                        getCValiDate());
            }

            yelLJSPayPersonSchema.setCurPayToDate(strNewPayToDate);
            /*Lis5.3 upgrade get
             yelLJSPayPersonSchema.setBankCode(tLCPolSchema.getBankCode());
             yelLJSPayPersonSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
             */
            yelLJSPayPersonSchema.setBankOnTheWayFlag("0");
            yelLJSPayPersonSchema.setBankSuccFlag("0");
            yelLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
            yelLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());
            yelLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
            yelLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
            yelLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
            yelLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            yelLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
            yelLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
            yelLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
            yelLJSPayPersonSchema.setOperator(mOperater);

            if (tType.equals("2"))
            {
                yelLJSPayPersonSchema.setPayTypeFlag("1");
            }

            yelLJSPayPersonSchema.setMakeDate(CurrentDate); //入机日期
            yelLJSPayPersonSchema.setMakeTime(CurrentTime); //入机时间
            yelLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
            yelLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
        }

        System.out.println("余额:" + tLCPolSchema.getLeavingMoney());

        if (yelFlag) //如果个人保单有余额
        {
            if (LeavingMoney > mSubSumPay) //如果个人保单中的余额>应收款，设置一条纪录
            {
                yetFlag = true; //设置标志位
                yetLJSPayPersonSchema.setPolNo(PolNo);
                yetLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() +
                                                  1);
                yetLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                yetLJSPayPersonSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                yetLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
                yetLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.
                        getPayPlanCode());
                yetLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
                yetLJSPayPersonSchema.setSumDuePayMoney(LeavingMoney -
                        mSubSumPay);
                yetLJSPayPersonSchema.setSumActuPayMoney(LeavingMoney -
                        mSubSumPay);

                // 产生通知书号
                //tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
                //tNo=PubFun1.CreateMaxNo("PAYNOTICENO",tLimit);
                yetLJSPayPersonSchema.setGetNoticeNo(tNo);
                yetLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
                yetLJSPayPersonSchema.setPayIntv(tLCPolSchema.getPayIntv());

                //repair: 交费日期=个人保单表的交至日期 待计算
                yetLJSPayPersonSchema.setPayDate(strNewPayDate);
                yetLJSPayPersonSchema.setPayType("YET"); //交费类型=YET

                //repair:保险帐户号码=待定
                yetLJSPayPersonSchema.setAppntNo(tLCPremSchema.getAppntNo());

                //repair:这条纪录的//原交至日期=个人保单表的交至日期
                //repair:现交至日期CurPayToDate=个人保单表的交至日期
                if ((tLCPolSchema.getPaytoDate() != null) &&
                    !tLCPolSchema.getPaytoDate().equals(""))
                {
                    yetLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.
                            getPaytoDate());
                }
                else
                {
                    yetLJSPayPersonSchema.setLastPayToDate(tLCPolSchema.
                            getCValiDate());
                }

                yetLJSPayPersonSchema.setCurPayToDate(strNewPayToDate);
                /*Lis5.3 upgrade get
                 yetLJSPayPersonSchema.setBankCode(tLCPolSchema.getBankCode());
                 yetLJSPayPersonSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
                 */
                yetLJSPayPersonSchema.setBankOnTheWayFlag("0");
                yetLJSPayPersonSchema.setBankSuccFlag("0");
                yetLJSPayPersonSchema.setApproveCode(tLCPolSchema.
                        getApproveCode());
                yetLJSPayPersonSchema.setApproveDate(tLCPolSchema.
                        getApproveDate());
                yetLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
                yetLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
                yetLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
                yetLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
                yetLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
                yetLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());
                yetLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                yetLJSPayPersonSchema.setOperator(mOperater);

                if (tType.equals("2"))
                {
                    yetLJSPayPersonSchema.setPayTypeFlag("1");
                }

                yetLJSPayPersonSchema.setMakeDate(CurrentDate); //入机日期
                yetLJSPayPersonSchema.setMakeTime(CurrentTime); //入机时间
                yetLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
                yetLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
            }
        }

        //因此，mLCPremSet中记录总是=mLCDutySet中的纪录，这两个记录集中相同序号的纪录是一一对应的.
        //这将有助于我们根据mLCDutySet的记录数，循环处理（两个记录集中记录序号是对应的）
        //在整理应收个人表纪录时，添加完保费项集合中的纪录后，不要忘了判断：yelFlag和yetFlag
        //根据判断，添加 yelLJSPayPersonSchema ，yetLJSPayPersonSchema 到应收个人纪录，并纪录到应收总表中
        //Step 6 : transaction:将个人保单纪录，保费项表集合，保险责任表集合整理成应收总表纪录，应收个人表集合
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();

        for (int index = 1; index <= saveLCPremSet.size(); index++)
        {
            LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLCDutySchema = new LCDutySchema();
            tLCPremSchema = saveLCPremSet.get(index); //保费项纪录
            tLCDutySchema = mLCDutySet.get(index); //保险责任纪录

            tLJSPayPersonSchema.setPolNo(PolNo);
            tLJSPayPersonSchema.setPayCount(tLCPremSchema.getPayTimes() + 1);
            tLJSPayPersonSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
            tLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
            tLJSPayPersonSchema.setContNo(tLCPolSchema.getContNo());
            tLJSPayPersonSchema.setAppntNo(tLCPolSchema.getAppntNo());

            // 产生通知书号
            //tLimit=PubFun.getNoLimit(tLCPolSchema.getManageCom());
            //tNo=PubFun1.CreateMaxNo("PAYNOTICENO",tLimit);
            tLJSPayPersonSchema.setGetNoticeNo(tNo);
            tLJSPayPersonSchema.setPayAimClass("1"); //交费目的分类=个人
            tLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
            tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
            tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem() * (1 -
                    tLCDutySchema.getFreeRate()));
            tLJSPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem() * (1 -
                    tLCDutySchema.getFreeRate()));
            tLJSPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());

            //-------------------------------------------------------
            // 交费日期=失效日期 =现交至日期+2个月 （待修订）repair:
            //下面计算 现交至日期
            if ((tLCPremSchema.getPaytoDate() != null) &&
                !tLCPremSchema.getPaytoDate().equals(""))
            {
                baseDate = fDate.getDate(tLCPremSchema.getPaytoDate());
            }
            else
            {
                baseDate = fDate.getDate(tLCPremSchema.getPayStartDate());
            }

            System.out.println("?????:" + baseDate);
            interval = tLCPremSchema.getPayIntv();

            if (interval == -1)
            {
                interval = 0; // 不定期缴费
            }

            unit = "M";

            //repair:日期不能为空，否则下面的函数处理会有问题
            String strPayDate = "";
            String strPayToDate = "";

            if (baseDate != null)
            {
                if (PayInterval == 0)
                {
                    paytoDate = tD.getDate(tLCPremSchema.getPayEndDate());
                }
                else
                {
                    paytoDate = PubFun.calDate(baseDate, interval, unit, null);
                }

                //针对每个保费项，计算新的交至日期
                System.out.println("交至日期:" + paytoDate);

                //计算交费日期=失效日期=旧的交至日期+2个月
                payDate = PubFun.calDate(baseDate, 2, unit, null);
                System.out.println("失效日期:" + payDate);

                //得到日期型，转换成String型
                strPayToDate = fDate.getString(paytoDate);
                strPayDate = fDate.getString(payDate);
            }

            tLJSPayPersonSchema.setPayDate(strPayDate);
            tLJSPayPersonSchema.setPayType("ZC"); //交费类型=ZC即"正常"

            if ((tLCPremSchema.getPaytoDate() != null) &&
                !tLCPremSchema.getPaytoDate().equals(""))
            {
                tLJSPayPersonSchema.setLastPayToDate(tLCPremSchema.getPaytoDate());
            }
            else
            {
                tLJSPayPersonSchema.setLastPayToDate(tLCPremSchema.
                        getPayStartDate());
            }

            //原交至日期=保费项表的交至日期
            //现交至日期CurPayToDate=交至日期+日期（interval=12 12个月-年交，interval=1 即1个月-月交）待修改
            tLJSPayPersonSchema.setCurPayToDate(strPayToDate);

            //保险帐户号码=待定
            /*Lis5.3 upgrade get
             tLJSPayPersonSchema.setBankCode(tLCPolSchema.getBankCode());
             tLJSPayPersonSchema.setBankAccNo(tLCPolSchema.getBankAccNo());
             */
            tLJSPayPersonSchema.setBankOnTheWayFlag("0");
            tLJSPayPersonSchema.setBankSuccFlag("0");
            tLJSPayPersonSchema.setApproveCode(tLCPolSchema.getApproveCode());
            tLJSPayPersonSchema.setApproveDate(tLCPolSchema.getApproveDate());

            tLJSPayPersonSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJSPayPersonSchema.setAgentCom(tLCPolSchema.getAgentCom());
            tLJSPayPersonSchema.setAgentType(tLCPolSchema.getAgentType());
            tLJSPayPersonSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLJSPayPersonSchema.setAgentGroup(tLCPolSchema.getAgentGroup());
            tLJSPayPersonSchema.setRiskCode(tLCPolSchema.getRiskCode());

            tLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
            tLJSPayPersonSchema.setOperator(mOperater);

            if (tType.equals("2"))
            {
                tLJSPayPersonSchema.setPayTypeFlag("1");
            }

            tLJSPayPersonSchema.setMakeDate(CurrentDate); //入机日期
            tLJSPayPersonSchema.setMakeTime(CurrentTime); //入机时间
            tLJSPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
            tLJSPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
            mLJSPayPersonSet.add(tLJSPayPersonSchema);
        }

        //根据不同情况，添加应收总表的应收总额
        if (yelFlag)
        {
            //添加一条纪录
            mLJSPayPersonSet.add(yelLJSPayPersonSchema);

            if (yetFlag)
            {
                //添加一条纪录
                mLJSPayPersonSet.add(yetLJSPayPersonSchema);

                //添加应收总表 =0
                mSubSumPay = 0;

                // tLJSPaySchema.setSumDuePayMoney(0);
            }
            else
            {
                mSubSumPay = mSubSumPay - LeavingMoney;

                //tLJSPaySchema.setSumDuePayMoney(sumPay-LeavingMoney);
            }
        }

        //else{tLJSPaySchema.setSumDuePayMoney(sumPay);}
        mSumPay = mSumPay + mSubSumPay;

        //saveData=tVData;
        return true;
    }

    private LCContSchema getLCContInfo(String contNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if(!tLCContDB.getInfo())
        {
            mErrors.addOneError("没有查询到保单信息" + contNo);
            return null;
        }
        return tLCContDB.getSchema();
    }

    /**
     * 外部MMp结果集穿入本类
     * @param tMMap MMap，注意，此处只能使用引用参数
     */
    public void setOutSideMap(MMap tMMap)
    {
        mMMapOutSide = tMMap;
    }
}
