package com.sinosoft.lis.xb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.TransferCPDataToBP;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class XBRun {

    CErrors mErrors = new CErrors();
    
    GlobalInput mGlobalInput = new GlobalInput();
    
    private String CurrentDate = PubFun.getCurrentDate();
    
    private String CurrentTime = PubFun.getCurrentTime();


    public XBRun() {
    }

    public boolean submitData()
    {
    	dealRnewCont();
    	delLCRnewStateLog();
    	dealMainPolNo();
    	dealLJFIGet();
    	return true;
    }
    
    //续保中间状态批处理开始
    private void dealRnewCont()
    {
    	System.out.println("续保中间状态批处理开始！");
        String sql =" select distinct ( select  contno from lccont a where a.contno = b.contno)"
           +" ,(select  CInValiDate from lccont a where a.contno= b.newcontno) "
           +" ,operator "
           +" ,( select  managecom from lccont a where a.contno = b.contno) "
           +"  from lcrnewstatelog b where  state='5' with ur ";
       SSRS tSSRS = new ExeSQL().execSQL(sql);

       for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
          LCContSchema schema = new LCContSchema();
          String contno = tSSRS.GetText(i, 1);
          String CInValiDate = tSSRS.GetText(i, 2);
          String Operator = tSSRS.GetText(i, 3);
          String ComCode = tSSRS.GetText(i, 4);

          schema.setContNo(contno);
          schema.setCInValiDate(CInValiDate);

          GlobalInput gi = new GlobalInput();

          gi.Operator = Operator;
          gi.ComCode = ComCode;
          VData data = new VData();
          data.add(schema);
          data.add(gi);
          System.out.println(i);
          TransferCPDataToBP tTransferCPDataToBP = new TransferCPDataToBP();
          if (!tTransferCPDataToBP.submitData(data, "")) 
          {
              System.out.println(tTransferCPDataToBP.mErrors.getErrContent());
          }
      }
      System.out.println("续保中间状态批处理完成！");
    }
    //20080812 zhanggm 删除LCRnewStateLog中State='3'的错误数据
    private void delLCRnewStateLog()
    {
    	MMap map = new MMap();
    	System.out.println("删除LCRnewStateLog中State='3'的错误数据批处理开始！");
    	ExeSQL tExeSQL = new ExeSQL();
    	String sql = "select distinct NewContNo from LCRnewStateLog where State = '3'";
    	System.out.println(sql);
    	SSRS tSSRS = tExeSQL.execSQL(sql);
        for(int i=1; i<=tSSRS.getMaxRow();i++)
        {
     	   String [] tTableName = {"LCCont","LCPol","LCDuty","LCPrem","LCGet"};
     	   for(int j=0;j<tTableName.length;j++)
     	   {
     		  String delSql = "delete from " + tTableName[j] + " where ContNo = '" + tSSRS.GetText(i, 1) + "' ";
     		  System.out.println(delSql);
     		 map.put(delSql, "DELETE");
     	   }
        }
        sql = "delete from LCRnewStateLog where State = '3'";
        System.out.println(sql);
        map.put(sql, "DELETE");
        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数据失败");
        }
        System.out.println("删除LCRnewStateLog中State='3'的错误数据批处理完成！");
    }
    
    //20090611 zhanggm 修改续保后lcpol中mainpolno错误的数据：'11%'->'21%'
    private void dealMainPolNo()
    {
    	System.out.println("修改续保后lcpol中mainpolno错误的数据批处理开始");
    	MMap map = new MMap();
    	ExeSQL tExeSQL = new ExeSQL();
    	StringBuffer sql = new StringBuffer(128);
    	sql.append("select polno,riskcode from lcpol a where mainpolno like '11%' and stateflag = '1' ")
    	   .append("and grpcontno = '00000000000000000000'  and conttype = '1' and appflag = '1' ")
    	   .append("and exists (select 1 from ljspaypersonb where polno = a.polno and dealstate = '1') ");
    	
    	System.out.println(sql.toString());
    	SSRS tSSRS = tExeSQL.execSQL(sql.toString());
    	System.out.println("拢共需要处理"+tSSRS.MaxRow+"条");
    	if(tSSRS.MaxRow == 0)
    	{
    		System.out.println("修改续保后lcpol中mainpolno错误的数据批处理结束");
    		return;
    	}
    	for(int i=1;i<=tSSRS.MaxRow;i++)
    	{
    		System.out.println("处理 polno="+tSSRS.GetText(i, 1)+" riskcode="+tSSRS.GetText(i, 2));
    		StringBuffer sql1 = new StringBuffer(128);
    		sql1.append("select 1 from ldcode1 where codetype = 'checkappendrisk' and code = '")
    		    .append(tSSRS.GetText(i, 2)).append("' ");
    		System.out.println(sql1.toString());
    		String tRiskType = tExeSQL.getOneValue(sql1.toString());
    		if(!"".equals(tRiskType) && tRiskType != null && !"null".equals(tRiskType))
    		{
    			String sqlPol = "select polno from lcpol where proposalno = (select mainpolno from lcpol where polno = '" + tSSRS.GetText(i, 1) + "') ";
    			String tPolNo = tExeSQL.getOneValue(sqlPol);
    			//附加险
    			if(!"".equals(tPolNo) && tPolNo != null && !"null".equals(tPolNo))
        		{
    				StringBuffer updateSql = new StringBuffer(128);
        			updateSql.append("update lcpol a set mainpolno = '").append(tPolNo)
        			         .append("',modifydate = '").append(CurrentDate).append("', modifytime = '")
        			         .append(CurrentTime).append("' where polno = '")
        			         .append(tSSRS.GetText(i, 1)).append("' ");
        			map.put(updateSql.toString(), SysConst.UPDATE);
    			}
    		}
    		else
    		{
    			//主险
    			StringBuffer updateSql = new StringBuffer(128);
    			updateSql.append("update lcpol set mainpolno = polno,modifydate = '").append(CurrentDate)
    			         .append("', modifytime = '").append(CurrentTime).append("' where polno = '")
    			         .append(tSSRS.GetText(i, 1)).append("' ");
    			map.put(updateSql.toString(), SysConst.UPDATE);
    		}
    	}
    	VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数据失败");
        }
    	System.out.println("修改续保后lcpol中mainpolno错误的数据批处理结束");
    }
    
//  20090617 zhanggm 补充少儿险续保后缺少的ljfiget 
    private void dealLJFIGet()
    {
    	System.out.println("补充少儿险续保后缺少的ljfiget批处理开始");
    	String sql = "select * from ljaget a where a.confdate >= current date - 7 days and  a.confdate <=current date and not exists (select 1 from ljfiget b where b.actugetno=a.actugetno ) and othernotype = '20' and paymode = '5' ";
    	System.out.println(sql);
    	LJAGetDB tLJAGetDB = new LJAGetDB();
        LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(sql);
        this.dealDate(tLJAGetSet);
        System.out.println("补充少儿险续保后缺少的ljfiget批处理结束");
    }
    
    private void dealDate(LJAGetSet tLJAGetSet)
    {
    	MMap map = new MMap();
        for (int i = 1; i <= tLJAGetSet.size(); i++) 
        {
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i).getSchema();
            LJFIGetSet tLJFIGetSet = new LJFIGetSet();
            LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLJFIGetSchema, tLJAGetSchema);
            tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
            tLJFIGetSchema.setPolicyCom(tLJAGetSchema.getManageCom());
            tLJFIGetSet.add(tLJFIGetSchema);
            map.put(tLJFIGetSet, SysConst.INSERT);
        }
        PubSubmit tPubSubmit = new PubSubmit();
        VData data = new VData();
        data.add(map);
        if (!tPubSubmit.submitData(data, "")) 
        {
        	System.out.println(tPubSubmit.mErrors.getErrContent());
        	mErrors.addOneError("提交数据失败");
        }
    }
    
    
    public static void main(String[] args)
    {
        XBRun x = new XBRun();
//        x.dealLJFIGet();
        x.submitData();
        System.out.println("XBRun完成！");
    }
}
