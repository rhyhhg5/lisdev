package com.sinosoft.lis.xb;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.operfee.OmnipFinUrgeVerifyBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCRnewStateLogSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class OmnipDueVerifyBL {
	
	    public CErrors mErrors = new CErrors();
	    private GlobalInput mGlobalInput = null;  //操作员信息
	    private TransferData mTransferData = null;
	    private LCContSchema mLCContSchema = null;  //需要续保续期财务核销的原保单信息，可以只录入保单号
	    private String mOperate = null;
	    private String mGetNoticeNo ;
	    private String mRnewState = null;
	    
	    public OmnipDueVerifyBL(){
	    	
	    }

	    /**
	     * 万能险续期核销功能的对外接口，实现相关的操作
	     * @param inputData VData：包括
	     * 1、LCContSchema：保单号
	     * 2、GlobalInput：操作员信息
	     * @param operate String
	     * @return boolean
	     */
	    public boolean submitData(VData inputData, String operate)
	    {
	        mOperate = operate;

	        if(!getInputData(inputData))
	        {
	            return false;
	        }

	        if(!checkData())
	        {
	            return false;
	        }

	        if(!dealData())
	        {
	            return false;
	        }

	        return true;
	    }

	    /**
	     * 进行业务处理
	     * 分别调用续期核销
	     * @return boolean
	     */
	    private boolean dealData()
	    {
	        

	        //续期续保核销
	        if(mTransferData == null)
	        {
	            mTransferData = new TransferData();
	        }
	        mTransferData.setNameAndValue("NeedRnewVerify", "y");

	        VData data = new VData();
	        data.add(mLCContSchema);
	        data.add(mGlobalInput);
	        data.add(mTransferData);

	        boolean needFinVerify = needFinVerify();

	        if(needFinVerify)
	        {
	        	OmnipFinUrgeVerifyBL tOmnipFinUrgeVerifyBL = new OmnipFinUrgeVerifyBL();
	            if(!tOmnipFinUrgeVerifyBL.submitData(data, "VERIFY"))
	            {
	                mErrors.copyAllErrors(tOmnipFinUrgeVerifyBL.mErrors);
	                return false;
	            }
	        }
	        if(!needPRnewOperate())
	        {
	            return true;
	        }
	        String edorNo = null;
	        String sql = "select WorkNo from LGWork a, LJSPayB b "
	                     + "where a.InnerSource = b.GetNoticeNo "
	                     + "   and b.OtherNoType = '2' "
	                     + "   and b.OtherNo='" + mLCContSchema.getContNo() + "' ";
	        String workNo = new ExeSQL().getOneValue(sql);
	        if(!workNo.equals("") && !workNo.equals("null"))
	        {
	            edorNo = workNo;
	        }

	        data.clear();
	        data.add(mLCContSchema);
	        data.add(mGlobalInput);

	        System.out.println("Beginning of PRnewDueVerifyBL.dealData: TransferCPDataToBP");
	        TransferCPDataToBP tTransferCPDataToBP = new TransferCPDataToBP(edorNo);
	        MMap tMMap = tTransferCPDataToBP.getSubmitMap(data, mOperate);
	        if(tMMap == null && tTransferCPDataToBP.mErrors.needDealError())
	        {
	            mErrors.copyAllErrors(tTransferCPDataToBP.mErrors);
	            return false;
	        }
	        data.clear();
	        data.add(tMMap);
	        PubSubmit tPubSubmit = new PubSubmit();
	        if(!tPubSubmit.submitData(data, mOperate))
	        {
	            mErrors.addOneError("续保数据转移失败");
	            return false;
	        }
	        
	        return true;
	    }

	  

	    /**
	     * 校验是否需要进行财务核销
	     * 若有财务暂收，则需要进行财务核销
	     * @return boolean：需要，true，否认false
	     */
	    private boolean needFinVerify()
	    {
	        LJSPayDB tLJSPayDB = new LJSPayDB();
//	        tLJSPayDB.setOtherNo(mLCContSchema.getContNo());
	        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
	        tLJSPayDB.setOtherNoType("2");

	        if(tLJSPayDB.query().size() == 0)
	        {
	            return false;
	        }
	        return true;
	    }

	    /**
	     * 校验操作和数据的合法性
	     * @return boolean
	     */
	    private boolean checkData()
	    {
	        LCContDB db = new LCContDB();
	        db.setContNo(mLCContSchema.getContNo());
	        if(!db.getInfo())
	        {
	            mErrors.addOneError("没有查询到保单号为" + mLCContSchema.getContNo()
	                + "的保单");
	            return false;
	        }
	        mLCContSchema.setSchema(db);

	        String sql = "select a.edorAcceptNo "
	                     + "from LPEdorApp a, LPEdorItem b "
	                     + "where a.edorAcceptNo = b.edorNo "
	                     + "   and b.contNo = '" + mLCContSchema.getContNo() + "' "
	                     + "   and a.edorState not in('0')";
	        String temp = new ExeSQL().getOneValue(sql);
	        if(!(temp==null||temp.equals("")))
	        {
	            mErrors.addOneError("保单正在做保全，不能核销" + mLCContSchema.getContNo());
	            return false;
	        }

	        return true;
	    }
	    
	    /**
	     * 校验是否需要续保确认和数据转移
	     * 1、没有续保轨迹，不需要续保转移，false
	     * 2、有续保轨迹但状态不是待确认、确认成功，转移成功，false
	     * @return boolean
	     */
	    private boolean needPRnewOperate()
	    {
	        String sql = "  select * "
	                     + "from LCRnewStateLog a "
	                     + "where contNo = '" + mLCContSchema.getContNo() + "' "
	                     + "   and newContNo = "
	                     + "      (select max(newContNo) "
	                     + "      from LCRnewStateLog "
	                     + "      where contNo = a.contNo "
	                     + "         and state != '6') ";
	        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
	        LCRnewStateLogSet tLCRnewStateLogSet
	            = tLCRnewStateLogDB.executeQuery(sql);
	        if(tLCRnewStateLogSet.size() == 0)
	        {
	            return false;
	        }
	        if(tLCRnewStateLogSet.get(1).getState()
	           .compareTo(XBConst.RNEWSTATE_UNCONFIRM) < 0)
	        {
	            return false;
	        }
	        mRnewState = tLCRnewStateLogSet.get(1).getState();

	        return true;
	    }

	    private boolean getInputData(VData data)
	    {
	        mLCContSchema = (LCContSchema) data
	                        .getObjectByObjectName("LCContSchema", 0);
	        mGlobalInput = (GlobalInput) data
	                       .getObjectByObjectName("GlobalInput", 0);
	        mTransferData = (TransferData) data
	                        .getObjectByObjectName("TransferData", 0);
	        mGetNoticeNo =(String)mTransferData.getValueByName("GetNoticeNo");
	        

	        if(mLCContSchema == null || mLCContSchema.getContNo() == null
	           || mGlobalInput == null)
	        {
	            mErrors.addOneError("传入到续保续期财务核销的数据不完整");
	            return false;
	        }
	        return true;
	    }
}
