package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理个单续保申请功能
 * 包含了续保续保数据的生成，续保的抽档，续期抽档等功能
 * 若所有险种都已续保申请且未结案，则不能在续保申请
 * 若保单险种需要人工核保，则不能进行催缴
 * 本类只处理单个保单的情况
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class PRnewDueFeeBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private String mOperate = null;

    private LCContSchema mLCContSchema = null;   //需要续期续保处理的个单号
    private TransferData mTransferData = null;
    private String mStartDate = null;  //续保续期开始日期
    private String mEndDate = null;  //续保续期截止日期
    private String mSerialNo = null;  //抽档批次号
    private String mEdorNo = null;  //为了在进行校验时排除当前工单
    private String message = null;
    private String queryType = null; //保单类型，2是普通保单，3是银代少儿
    private String subString = null;

    private boolean mNeedRnewFee = false;  //需续保催收标志

    private MMap map = new MMap();
    
    ExeSQL mExeSQL = new ExeSQL();

    /**
     * 构造方法
     */
    public PRnewDueFeeBL()
    {
    }

    /**
     * 得到经过处理后的结果集合
     * @param inputData VData，
     * 包含
     * 1、TrasnferData数据类型，其包含了续保申请的起止日期
     * 2、LCContSchema，完整的个单信息，在本类不会进行校验
     * 3、GlobalInput，操作员信息，可为空
     * @param operate String， 操作类型，在此为“”
     * @return MMap
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        mOperate = operate;

        if(!getInputData(inputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }
        return map;
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData，
     * 包含
     * 1、TrasnferData数据类型，其包含了续保申请的起止日期
     * 2、LCContSchema，完整的个单信息，在本类不会进行校验
     * 3、GlobalInput，操作员信息，可为空
     * @param operate String，操作类型
     * @return boolean，操作成功返回true，否则放回false
     */
    public boolean submitData(VData inputData, String operate)
    {
        getSubmitMap(inputData, operate);
        if(map == null || map.size() == 0 || mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("存储数据失败");
            System.out.println("存储数据失败\n"
                               + tPubSubmit.mErrors.getErrContent());
            return false;
        }


//        IndiDueFeeBL tIndiDueFeeBL = new IndiDueFeeBL();
//        MMap aMMap = tIndiDueFeeBL.changePayDate(
//            (LJSPaySchema) map.getObjectByObjectName("LJSPaySchema", 0));
//        data.clear();
//        data.add(aMMap);
//        PubSubmit aPubSubmit = new PubSubmit();
//        aPubSubmit.submitData(data, "");
//        System.out.println(aPubSubmit.mErrors.getErrContent());

        System.out.println("续保续期抽当成功\n" + mErrors.getErrContent());

        return true;
    }

    /**
     * 得到传入的数据
     * @param data VData
     * 包含
     * 1、TrasnferData数据类型，其包含了续保申请的起止日期
     * 2、LCContSchema，完整的个单信息，在本类不会进行校验
     * 3、GlobalInput，操作员信息，可为空
     * @return boolean：成功true， 失败false
     */
    private boolean getInputData(VData data)
    {
        if(data == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        //保单信息
        mLCContSchema = (LCContSchema) data.getObjectByObjectName("LCContSchema", 0);
        if(mLCContSchema == null || mLCContSchema.getContNo() == null)
        {
            mErrors.addOneError("请传入个单号");
            return false;
        }

        //起止日期
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);
        if(mTransferData == null)
        {
            mErrors.addOneError("请传入起止日期");
            return false;
        }
        mStartDate = (String) mTransferData.getValueByName("StartDate");
        mEndDate = (String) mTransferData.getValueByName("EndDate");
        mSerialNo = (String) mTransferData.getValueByName("serNo");
        mEdorNo = (String) mTransferData.getValueByName("EdorNo");
        queryType = (String) mTransferData.getValueByName("queryType");
        //modify by hyy
        if(queryType == null){
        	queryType="2";
        }
        System.out.println("保单类型："+queryType);
        if(mStartDate == null)
        {
            mErrors.addOneError("请传入起始日期");
            return false;
        }
        if(mEndDate == null)
        {
            mErrors.addOneError("请传入截止日期");
            return false;
        }
//        if(mSerialNo == null)
//        {
//            mErrors.addOneError("未传入抽档批次号");
//            return false;
//        }

        //操作员信息
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        if(mGlobalInput == null)
        {
            mGlobalInput.Operator = mLCContSchema.getOperator();
            mGlobalInput.ComCode = mLCContSchema.getManageCom();
            mGlobalInput.ManageCom = mLCContSchema.getManageCom();
        }

        return true;
    }

    /**
     * 对数据和操作合法性的校验
     * @return boolean：通过true，失败false
     */
    private boolean checkData()
    {
        LCContDB db = new LCContDB();
        db.setContNo(mLCContSchema.getContNo());
        if(!db.getInfo())
        {
            mErrors.addOneError("没有查询到保单信息");
            return false;
        }
        mLCContSchema = db.getSchema();

        if((mEdorNo == null || mEdorNo.equals(""))&&!checkUWState())
        {
            return false;
        }
        
        if(!checkDueFeeBack())
    	{
    		return false;
    	}

        if(!checkBQState())
        {
            return false;
        }
        if(!checkLPState())
        {
            return false;
        }
        if(!checkRepeal())
        {
            return false;
        }
        if(!checkIndiBack())
        {
            return false;
        }
        if(!checkUWStateXB())
        {
            return false;
        }
        if(!checkJianGuan())
        {
            return false;
        }
        if(!checkWNZanHuan())
        {
            return false;
        }
        if(!checkEXTRALRisk()){
        	return false;
        }
        if(!checkFuJiaWN()){
        	return false;
        }

        return true;
    }
    
    /**
     * 校验保单下是否存在失效的附加重疾险种
     * wujun
     * @return
     */
    private boolean checkEXTRALRisk(){
    	//查询保单下是否存在失效的附加重疾险种
    	String sqlStr = "select 1 from lcpol a where a.contno='"+mLCContSchema.getContNo()+"'"
    	+ " and exists (select 1 from ldcode where codetype='extralrisk' and code=a.riskcode) "
    	+ " and a.stateflag='2' and (polstate is null or polstate!='"+BQ.POLSTATE_ZFEND+"') "//因终止缴费失效的可以续期
    	+ " fetch first 1 rows only  ";
    	String riskFlag = mExeSQL.getOneValue(sqlStr);
    	if(riskFlag!=null&&!riskFlag.equals("") && !riskFlag.equals("null")){
    	       mErrors.addOneError("该单附加重疾已经失效,请复效后再进行续期抽档!");
    	       return false;
    	}
    	return true;
    }
/**
* 万能暂缓缴费状态，不能做续期操作
* @return boolean：能做续期续保true
*/
private boolean checkWNZanHuan()
{
    String sql = " select 1 from lcpol where contno = '" + mLCContSchema.getContNo()+"' " 
    	       + " and riskcode in (select riskcode from lmriskapp where risktype4='4' )  " 
    	       + " and (standbyflag1 is not null and standbyflag1='"+BQ.ULI_HUAN+"') ";//区分新旧缓交
    String flag = mExeSQL.getOneValue(sql);
    if (!flag.equals("") && !flag.equals("null"))
    {
       mErrors.addOneError("该单已是万能暂缓缴费状态，不能做续期操作。");
       return false;
    } 
    return true;
}
/**
 * 保单下只有建管险种有效，则不能进行续期续保
 * @return boolean：能做续期续保true
 */
private boolean checkJianGuan()
{
	String sql = " select 1 from lcpol where contno = '" + mLCContSchema.getContNo()+"' " 
	            +" and riskcode not in (select riskcode from lmriskapp where risktype ='M' )  " 
	            +" and stateflag='1' ";
	String flag = mExeSQL.getOneValue(sql);
	if (!flag.equals("") && !flag.equals("null"))
	{
	    return true;
	}
	else
    {
        mErrors.addOneError("保单下只有健管险种有效，不能做续期续保操作。");
        return false;
    } 
}
    /**
     * 若正在做实收保费转出，则不能进行续期续保
     * @return boolean：能做续期续保true
     */
    private boolean checkIndiBack()
    {
        //CommonBL.checkIndiBack返回true，则正在进行失收保费转出
        if(CommonBL.checkIndiBack(mLCContSchema.getContNo()))
        {
            mErrors.addOneError("保单正在做续期续保实收保费转出，不能做续期续保操作。");
            return false;
        }

        return true;
    }

    /**
     * 校验是否正在人工核保
     * @return boolean：正人工核保true，否则false
     */
    private boolean checkUWState()
    {
        String sql = "  select * "
              + "from LCRnewStateLog "
              + "where contNo = '" + mLCContSchema.getContNo() + "' "
              + "   and state ='" + XBConst.RNEWSTATE_UNUW + "' ";
        System.out.println(sql);
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet set = tLCRnewStateLogDB.executeQuery(sql);
        if(set.size() > 0)
        {
            mErrors.addOneError("保单正在人工核保");
            return false;
        }
        return true;
    }

    /**
     * 校验是否已做过撤销操作
     * 做过撤销操作的保单不可再抽档
     * @return boolean：做过撤销true，否则false
     */
    private boolean checkRepeal()
    {
    	// modify by 2008-5-12 少儿险给付在应收的状态是普通保单应收撤销的状态，
    	// 所以少儿险先做了给付抽档的，普通单在抽档的时候就会出现校验问题，现在修改成如果少儿险已经做了给付的 就不做此校验。
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	String sql = " select count(1) From ljspayb a,ljspaypersonb b where  otherno ='"+mLCContSchema.getContNo()+"'"
    			   + " and a.getnoticeno = b.getnoticeno and a.dealstate ='3' and b.riskcode in('320106','120706')"
    			   ;
    	tSSRS = tExeSQL.execSQL(sql);

    	if(tSSRS.getMaxRow() != 0)
    	{
    		return true;
    	}


        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setOtherNo(mLCContSchema.getContNo());
        tLJSPayBDB.setOtherNoType("2");
        tLJSPayBDB.setDealState(FeeConst.DEALSTATE_REPEAL);  //撤销
        LJSPayBSet set = tLJSPayBDB.query();
        if(set.size() > 0)
        {
            mErrors.addOneError("保单" + mLCContSchema.getContNo()
                + "发生过续期续保撤销，不能再进行续期续保");
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑
     * 循环保单的每个险种，依次执行续保申请、续期催缴、续保催缴
     * 若没有进行续保的险种，则只会进行续期催缴，在此生成应收
     * 若有需要续保的险种，则只在续保催收的时候生成应收
     * @return boolean：操作成功true，失败false
     */
    private boolean dealData()
    {
//    	20090306 zhanggm 防止并发，加锁
    	MMap tCekMap = null;
    	tCekMap = lockLjsPayb(mLCContSchema);
        if (tCekMap == null)
        {
            return false;
        }
        map.add(tCekMap);
//--------------------------
    	if(!createAppAcc())
    	{
            return false;
    	}

        if(!renewPContApp())
        {
            return false;
        }
        if(!indiDueFee())
        {
            return false;
        }
//        if(!renewAutoDun())
//        {
//            return false;
//        }

        return true;
    }

    /**
     * 续保抽档
     * 不管什么情况下，续保申请结果都需要存储
     * @return boolean：抽档成功true，否则false
     */
    private boolean renewPContApp()
    {

        if(queryType.equals("2")) //普通单
        {
            subString =" and b.riskcode not in ('320106','120706') " ;
        }
        else
        {
            subString =" and b.riskcode  in ('320106','120706') ";
        }
            String sql = "  select distinct b.* "
                         + "from LCPol b, LMRiskApp c, LMRisk d "
                         + "where b.riskCode = c.riskCode "
                         + "   and c.riskCode = d.riskCode "
                         + "   and b.endDate <= '" + mEndDate
                         + "'  and b.endDate >= '" + mStartDate + "' "
                         + "   and b.payToDate >= b.endDate"//lzy 2015税优，续期保费交满之后进行续保
                         + "   and b.contType = '" + XBConst.CONTTYPE_P + "' " //个单
                         + "   and b.appFlag = '1' " //已生效
                         + "   and (b.StateFlag is null or b.StateFlag = '1')" //qulq 2007-1-11 保单状态控制
                         + "   and b.rnewFlag != 2 " //可续保
                         + "   and (c.riskType5 = '2' or c.riskcode in ('510901','511001')) " //一年期险
                         + "   and contNo = '" + mLCContSchema.getContNo() +"' "
                         + subString
                         + "   and not exists " //为失效
                         + "      (select 1 from LCContState "
                         + "      where polNo = b.polNo "
                         + "         and StateType= '" + XBConst.TERMINATE +
                         "' "
                         + "         and state = '1' "
                         + "         and startDate <= b.endDate) "
                         + " and (polstate is null or (polstate is not null and polstate not like '02%' and polstate not like '03%' "
                         + " or (polstate='"+XBConst.POLSTATE_LPXBSTOP+"' and exists (select code from ldcode where codetype='bzXB' and code=b.riskcode)) )) "//保证续保产品被理赔核保终止
                         + " order by polNo ";

            System.out.println(sql);
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
        //若没有需要续保的险种，可能险种要进行续期，正常退出，不进行错误处理
        if(tLCPolSet.size() == 0)
        {
            mNeedRnewFee = false;
            return true;
        }

        //校验是否已续保申请，若所有险种都已续保申请且未结案，则不能再续保申请
        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            sql = "  select * "
                  + "from LCRnewStateLog "
                  + "where polNo = '" + tLCPolSet.get(i).getPolNo() + "' "
                  + "   and state not in('" + XBConst.RNEWSTATE_DELIVERED
                  + "', '" + XBConst.RNEWSTATE_XBSTOP + "') ";
            System.out.println(sql);
            LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
            LCRnewStateLogSet set = tLCRnewStateLogDB.executeQuery(sql);
            if(set.size() > 0)
            {
                int compared = set.get(1).getState()
                   .compareTo(XBConst.RNEWSTATE_UNHASTEN);
                if(compared < 0)
                {
                    mNeedRnewFee = false;
                    mErrors.addOneError("保单" + mLCContSchema.getContNo()
                        + "需要人工核保,不能催收");
                    return false;
                }
                //待催收状态
                if(compared == 0)
                {
                    mNeedRnewFee = true;
                }
                else
                {
                    mNeedRnewFee = mNeedRnewFee || false; //所有险种都不需要续保时，mNeedRnewFee标志才为false
                }

                tLCPolSet.removeRange(i, i);
                i--;
            }
        }
        //没有需要续保的险种，正常退出
        if(tLCPolSet.size() == 0)
        {
            return true;
        }

        //调用续保申请
        VData data = new VData();
        data.add(mGlobalInput);
        data.add(mLCContSchema);
        data.add(tLCPolSet);

        PRnewContAppBL tPRnewContAppBL = new PRnewContAppBL();
        if(!tPRnewContAppBL.submitData(data, mOperate))
        {
            mErrors.copyAllErrors(tPRnewContAppBL.mErrors);
            return false;
        }

        if(tPRnewContAppBL.needManuUW())
        {
            mErrors.addOneError(com.sinosoft.task.CommonBL
                                .getErrContent(tPRnewContAppBL.mErrors));
            return false;
        }

        mNeedRnewFee = true;

        return true;
    }

    /**
     * 若保单正在做保全true，否则false
     * @return boolean
     */
    private boolean checkBQState()
    {
        String dealEdorNo = "";
        if(mEdorNo != null && !mEdorNo.equals(""))
        {
            dealEdorNo = "   and b.edorNo != '" + mEdorNo + "' ";
        }
        StringBuffer checkBQ = new StringBuffer(128);
        checkBQ.append("select distinct a.edorAcceptNo ")
            .append("from LPEdorApp a, LPEdorMain b ")
            .append("where a.edorAcceptNo = b.edorAcceptNo ")
            .append("   and b.contNo = '")
            .append(mLCContSchema.getContNo())
            .append("'  and a.edorState != '")
            .append(BQ.EDORSTATE_CONFIRM).append("' ")
            .append(dealEdorNo);
        System.out.println(checkBQ.toString());
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(checkBQ.toString());
        if(tSSRS.getMaxRow() > 0)
        {
            String edorNos = "";
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                edorNos += tSSRS.GetText(i, 1) + " ";
            }
            mErrors.addOneError("保单" + mLCContSchema.getContNo() + "正在做保全:"
                                + edorNos);
            return false;
        }
        return true;
    }

    /*
   *判断保单是否有理赔:如果有返回false,没有返回true;
   *@return boolean
   */

  private boolean checkLPState()
  {
	  String tContNo=mLCContSchema.getContNo();
      if(tContNo!=null && !tContNo.equals(""))
      {
          //校验保单下被保人如果正在理赔不能续期抽档
          String sql ="SELECT a.CusTomerName FROM LLCase a,LCPol b "
                      +"WHERE a.CusTomerNo = b.InsuredNo "
                      +"AND a.RGTState NOT IN ('11','12','14') AND b.contno='" + tContNo + "' ";
         System.out.println(sql);
         
         SSRS tSSRS = mExeSQL.execSQL(sql);
         if(tSSRS.getMaxRow()>0)
         {
            mErrors.addOneError("被保人"+tSSRS.GetText(1, 1)+"正在做理赔。");
            return false;
         }
         
         //处于合同处理受理的保单不能续期抽档
         sql = "SELECT 1 FROM LLContDeal a,LGWork b WHERE a.EdorNo = b.WorkNo AND b.StatusNo NOT IN ('5','8') "
         	+ "AND a.ContNo = '" + tContNo + "' ";
         
         SSRS tSSRS1 = mExeSQL.execSQL(sql);
         if(tSSRS1.getMaxRow()>0)
         {
            mErrors.addOneError("该保单正在做合同处理。");
            return false;
         }
      }
      return true;
  }


    /**
     * 续期催缴
     * 针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
     * @return boolean：成功true， 失败false
     */
    private boolean indiDueFee()
    {
        VData tVData = new VData();
        tVData.add(mLCContSchema);
        tVData.add(mGlobalInput);
        tVData.add(mSerialNo);

        mTransferData.setNameAndValue(FeeConst.GETNOTICENO, getGetNoticeNo());
        tVData.add(mTransferData);
        tVData.add(queryType);

        //若没有需要续期抽档的险种，可能会有需要续保抽档的险种，正常退出
        IndiDueFeeBL tIndiDueFeeBL = new IndiDueFeeBL();
        MMap tMMap = tIndiDueFeeBL.getSubmitMMap(tVData, "INSERT");
        if((tMMap == null || tMMap.size() == 0) && tIndiDueFeeBL.mErrors.needDealError())
        {
            if(mEdorNo != null && !tIndiDueFeeBL.getHasePolNeedFee())
            {
                message = "没有需要生成应收的险种，可能续保险种核保结论为续保终止。";
                return true;
            }

            mErrors.copyAllErrors(tIndiDueFeeBL.mErrors);
            return false;
        }
        else
        {
            map.add(tMMap);
        }
        return true;
    }

    public String getMessage()
    {
        return this.message;
    }

    /**
     * 得到应收记录号
     * @return String
     */
    private String getGetNoticeNo()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mEdorNo);
        if(tLGWorkDB.getInfo() && tLGWorkDB.getInnerSource() != null)
        {
            return tLGWorkDB.getInnerSource();
        }
        return null;
    }

    /**
     * 续保催缴
     * 调用续保催缴类进行业务处理
     * @return boolean：催缴成功true，否则false
     */
    private boolean renewAutoDun()
    {
        //若不需要续保催缴，则返回
        if(!mNeedRnewFee)
        {
            return true;
        }


        String sql = "  select * "
                     + "from LCPol a, LCRnewStateLog b "
                     + "where a.polNo = b.newPolNo "
                     + "   and b.state = '" + XBConst.RNEWSTATE_UNHASTEN + "' "
                     + "   and b.newContNo = ("
                     + "      select max(newContNo) "
                     + "      from LCRnewStateLog "
                     + "      where contNo = '"
                     + mLCContSchema.getContNo() + "') ";
        LCPolDB tLCPolDB = new LCPolDB();
        System.out.println(sql);
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

        VData data = new VData();
        TransferData tTransferData = getSerialNOs();

        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            data.clear();
            data.add(tLCPolSet.get(i));
            data.add(mGlobalInput);
            data.add(tTransferData);
            PRnewManualDunBL tPRnewManualDunBL = new PRnewManualDunBL();
            tPRnewManualDunBL.setOutSideMap(map);
            MMap tMMap = tPRnewManualDunBL.getSubmitMap(data, "INSERT");
            if(tMMap == null && tPRnewManualDunBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tPRnewManualDunBL.mErrors);
                return false;
            }
            map.add(tMMap);
        }

        return true;
    }

    /**
     * 生成催缴需要的流水号
     * @return TransferData：生成的流水号
     */
    private TransferData getSerialNOs()
    {
        // 产生通知书号
        TransferData td = new TransferData();

        String tNo = PubFun1.CreateMaxNo(
        "PAYNOTICENO", PubFun.getNoLimit(mLCContSchema.getManageCom()));
        td.setNameAndValue("PayNoticeNo", tNo);

        // 产生统一流水号
        String serNo = PubFun1.CreateMaxNo(
            "SERIALNO", PubFun.getNoLimit(mLCContSchema.getManageCom()));
            td.setNameAndValue("SerialNo", serNo);

        //产生打印流水号
        String prtSeq = PubFun1.CreateMaxNo(
            "PRTSEQNO", PubFun.getNoLimit(mLCContSchema.getManageCom()));
        td.setNameAndValue("PrtSeqNo", prtSeq);

        return td;
    }
    /**
     * 续保中没有没有回销的函件抽档
     * modify by fuxin 2008-1-23
     */
    private boolean checkUWStateXB()
    {
        String mContno = mLCContSchema.getContNo();

        String sql = " select a.edoracceptno From lpedoritem a, lgletter b ,lpedorapp c  where a.edoracceptno = b.edoracceptno "
                   + " and BackFlag = '1' "           //需要回销
                   + " and realbackdate is null "
                   + " and state in('1', '2', '6') "  // 6 是函件超时 1 待回销 2 待处理
                   + " and c.edorstate = '0' "
                   + " and a.edoracceptno = c.edoracceptno "
                   + " and c.othernotype = '1' "
                   + " and lettersubtype = '1' "
                   + " and a.contno ='"+ mContno +"'"
                   + " union "
                   + " select a.edoracceptno From  lgletter a ,lpuwmaster b where edoracceptno = b.edorno "
                   + " and realbackdate is not null " //函件回销 但是客户不同意的 不可以抽档
                   + " and CustomerReply = '0' "
                   + " with ur "
                   ;
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if(tSSRS.getMaxRow()>0)
        {
            mErrors.addOneError("保单"+mContno+"有没有回销的函件不能续保!");
            return false ;
        }
        return true ;
    }

    public static void main(String[] args)
    {
        LCContDB db = new LCContDB();
        db.setContNo("00006399201");
        db.getInfo();

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";
        g.ManageCom = g.ComCode;

        TransferData td = new TransferData();
        td.setNameAndValue("StartDate", "1000-01-01");
        td.setNameAndValue("EndDate", "3000-1-28");
//        td.setNameAndValue("serNo", "111111111");

        VData data = new VData();
        data.add(db.getSchema());
        data.add(g);
        data.add(td);

        PRnewDueFeeBL bl = new PRnewDueFeeBL();
        if(!bl.submitData(data, ""))
        {
            if(bl.mErrors.needDealError())
            {
                System.out.println(bl.mErrors.getErrContent());
            }
        }
    }
    
    //20080626 zhanggm 抽档时如果没有投保人账户则自动创建
    private boolean createAppAcc()
    {
    	LCAppAccDB tLCAppAccDB =new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(mLCContSchema.getAppntNo());
        if(!tLCAppAccDB.getInfo())
        {
            AppAcc mAppAcc = new AppAcc();
            MMap mMMap = new MMap();
            LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
            schema.setCustomerNo(mLCContSchema.getAppntNo());
            schema.setOtherNo(mLCContSchema.getContNo());
            schema.setOtherType("1");
            schema.setMoney(0);
            schema.setOperator(mLCContSchema.getOperator());

            mMMap.add(mAppAcc.accShiftToXQY(schema, "0"));

            VData dd = new VData();
            dd.add(mMMap);
            PubSubmit pub = new PubSubmit();
            if(!pub.submitData(dd, ""))
            {
                System.out.println(pub.mErrors.getErrContent());
                mErrors.addOneError("自动创建帐户,提交失败!");
                return false;
            }
        }
    	return true;
    }
    
    /**
     * 锁定动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockLjsPayb(LCContSchema cLCContSchema)
    {
        MMap tMMap = null;
        /**续期抽档锁定标志为："XB"*/
        String tLockNoType = "XB";
        /** 锁定有效时间*/
        String tAIS = "30";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
    
    //20100119 zhanggm 少儿险做过实收保费转出不能续保催收
    private boolean checkDueFeeBack()
    {
 	   if(queryType.equals("3"))
 	   {
 		   String sql = "select distinct getnoticeno from ljspaypersonb where contno = '" + mLCContSchema.getContNo()
 		              + "' with ur";
 		   ExeSQL mExeSQL = new ExeSQL();
 		   SSRS tSSRS = new SSRS();
 		   tSSRS = mExeSQL.execSQL(sql);
 		   if(tSSRS.getMaxRow()==0)
 		   {
 			   return true;
 		   }
 		   for(int i=1; i<=tSSRS.MaxRow; i++)
 		   {
 			   sql = "select count(1) from ljspayb where getnoticeno = '" + tSSRS.GetText(i, 1) + "' and dealstate = '6' with ur";
 			   String tCount =  mExeSQL.getOneValue(sql);
 			   if(!tCount.equals("0")) 
 			   {
 				   mErrors.addOneError("该少儿险保单" + mLCContSchema.getContNo() + "已经做过实收保费转出，不能进行续保催收！");
 				   return false;
 			   }
 		   }
 	   }
 	   return true;
    }
    private boolean checkFuJiaWN(){
    	
    	LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema.setContNo(mLCContSchema.getContNo());
        
        //判断险种是否是附加万能，若是则判断主险是否失效
        //若主险失效，附加万能不能单独续期
        
        String sql=" select riskcode from lmriskapp WHERE RiskType4='4' " +
        	       " and subriskflag='S'  and riskcode in (select riskcode from lcpol where contno='"+mLCContSchema.getContNo()+"')";

        System.out.println(sql);
        String temp = new ExeSQL().getOneValue(sql);
        if(!temp.equals(""))
        {
        	//主险是否失效
        	String mainSql= " select 1 from lcpol a where contno ='"+mLCContSchema.getContNo()+"'"
        				   +" and polno=mainpolno and stateflag in ('2','3')  and riskcode in ('340101','340301','340401','730201')" ;
            SSRS mSSRS = new SSRS();
            String temp1 = new ExeSQL().getOneValue(mainSql);
            if(!temp1.equals("")){
                mErrors.addOneError("保单主险已经失效，附加万能险种不能进行续期");
        		return false;
        	}
        }
    	return true;
    }
}
