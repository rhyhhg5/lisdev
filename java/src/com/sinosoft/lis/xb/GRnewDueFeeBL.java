package com.sinosoft.lis.xb;

import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.CalPayDateBL;
import com.sinosoft.lis.operfee.FeeConst;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import java.lang.String;


import com.sinosoft.lis.bq.AppAcc;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GRnewDueFeeBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private Reflections ref = new Reflections();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String strNewPayToDate = "";
    private String newProposalGrpContNo = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mStartDate = ""; //应收开始日期
    private String mEndDate = ""; //应收结束日期
    private String StartDate = ""; //操作开始日期
    private String StartTime = ""; //操作结束时间
    private TransferData mTransferData = new TransferData();
    private String mSerGrpNo = ""; //接受外部参数
    private SSRS mSSRS;
    private SSRS mPerSSRS;
    /** 数据表  保存数据*/

    LCGrpContSchema newLCGrpContSchema = new LCGrpContSchema();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    //集体保单表
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();


    //应收个人交费表
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    //应收团体交费表
    private LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPaySet mLJSPaySet = new LJSPaySet();
    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();
    //应收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    //应收总表集
    private LJSPaySet tLJSPaySet = new LJSPaySet();
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPayGrpSet mYelLJSPayGrpSet = new LJSPayGrpSet();
    //应收总表集备份表
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayGrpBSet mYelLJSPayGrpBSet = new LJSPayGrpBSet();
    //应收总表交费截止日期
    private String strNewLJSPayDate;
    //应收总表最早交费日期
    private String strNewLJSStartDate;
    //应收总表费额
    private double totalsumPay = 0;
    //保险责任表
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySchema mLCDutySchema = new LCDutySchema();

    private LCRnewStateLogSet mLCRnewStateLogSet = new LCRnewStateLogSet();
    
    //集体险种保单表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet(); //杨红于2005-07-19添加该私有变量
    //针对一个集体单可能对应多个集体险种单的情况
    private LCGrpPolSet newLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    private LCUrgeVerifyLogSchema mLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();

    //缴费通知书
    private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();
    private MMap map = new MMap();
    private MMap mapPer = new MMap();
    private MMap mapAcc = new MMap();
    private String mOpertor = "INSERT";
    private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收
    private String mserNo = "1"; //批次号

    //保单缴费方式
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mAccName = "";

    public GRnewDueFeeBL() {
    }

    public static void main(String[] args) {

        GlobalInput tGt = new GlobalInput();
        tGt.ComCode = "86";
        tGt.Operator = "pa0001";
        tGt.ManageCom = "86";

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("00114309000001");

        
        
        
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("StartDate", "2018-03-28");
        tempTransferData.setNameAndValue("EndDate", "2018-10-28");
        tempTransferData.setNameAndValue("ManageCom", "86");

        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(tGt);
        tVData.add(tempTransferData);


        GRnewDueFeeBL GRnewDueFeeBL1 = new GRnewDueFeeBL();
        GRnewDueFeeBL1.submitData(tVData, "INSERT");
    }
    
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {

        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCGrpContSchema = ((LCGrpContSchema) mInputData.getObjectByObjectName(
                "LCGrpContSchema", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tGI == null || mLCGrpContSchema == null || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GRnewDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (tLCGrpContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GRnewDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        mLCGrpPolSet = tLCGrpPolDB.query();
        
        return true;
    }
    /**
     * 数据校验，判断页面中查询出的信息是否已被催收
     * @return boolean
     */
    public boolean checkData() {

     //   System.out.println("续保抽档校验");

        //非银行转账不能进行催收
 //       String checkPayMode = "select paymode from lcgrpcont where grpcontno = '"+mLCGrpContSchema.getGrpContNo()+"'";
 //       String payMode = new ExeSQL().getOneValue(checkPayMode);
//        if(!"4".equals(payMode)){
//            this.mErrors.addOneError("团单号:" + mLCGrpContSchema.getGrpContNo() +"交费方式不是银行转账，请更改交费方式后再进行续保催收！");
//            return false;
//        }
        
    	if (mLCGrpContSchema.getPayMode().equals("4")){
            mBankCode = mLCGrpContSchema.getBankCode();
            mBankAccNo = mLCGrpContSchema.getBankAccNo();
            mAccName = mLCGrpContSchema.getAccName();

            String errMsg = "";
            boolean flag = true;
            if(mBankCode == null || mBankCode.equals(""))
            {
                errMsg += "银行代码,";
                flag = false;
            }
            if(mBankAccNo == null || mBankAccNo.equals(""))
            {
                errMsg += "银行账号,";
                flag = false;
            }
            if(mAccName == null || mAccName.equals(""))
            {
                errMsg += "账户名,";
                flag = false;
            }
            if(!flag)
            {
                mErrors.addOneError(
                    "缴费方式为银行转账，但是"
                    + errMsg.substring(0, errMsg.lastIndexOf(","))
                    + "为空，请确认。");
                return false;
            }
        }
    	
    	
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(
                "SELECT a.* FROM LPGrpEdorMain a WHERE a.edorstate<>'0' ")
                .append(" and (select count(EdorAcceptNo) from LPGrpEdorItem where EdorAcceptNo=a.EdorAcceptNo")
                .append(" and EdorNo = a.EdorNo) > 0")
                .append(" AND a.GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("'")
                ;
        String sqlEndor = tSBql.toString();
        System.out.println(sqlEndor);
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
        tLPGrpEdorMainSet = tLPGrpEdorMainDB.executeQuery(sqlEndor);
        if (tLPGrpEdorMainDB.mErrors.needDealError()) {
            this.mErrors.addOneError("团单号:" + mLCGrpContSchema.getGrpContNo() +"查询错误!");
            return false;
        }

        if ((tLPGrpEdorMainSet != null) && tLPGrpEdorMainSet.size() != 0) {
            this.mErrors.addOneError("团单号:" + mLCGrpContSchema.getGrpContNo() +"有保全未结案，不能进行催收!");
            return false;
        }
        
        
        if(!checkLPState())
        {
            return false;
        }
        
        
        StartDate = (String) mTransferData.getValueByName("CurrStartDate");
        StartTime = (String) mTransferData.getValueByName("CurrStartTime");
        mStartDate = (String) mTransferData.getValueByName("StartDate");
        mEndDate = (String) mTransferData.getValueByName("EndDate");
        String ManageCom = (String) mTransferData.getValueByName("ManageCom");
        //添加对团单是否正在进行实收转出的校验
        if(CommonBL.checkGrpBack(mLCGrpContSchema.getGrpContNo()))
        {
            this.mErrors.addOneError("您好，团单正进行实收保费转出，不能催收。");
            return false;
        }
        //
        tSBql = new StringBuffer(128);
        tSBql.append("select * from LCGrpCont where AppFlag='1' and (StateFlag is null or StateFlag = '1') and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and managecom like '")
                .append(ManageCom)
                .append("%'")
                .append(" and GrpContNo not in (select otherno from ljspay where othernotype='1')")
                .append(" and GrpContNo in (select GrpContNo from LCGrpPol")
                .append(" where PaytoDate >= '")
                .append(mStartDate)
                .append("' and PaytoDate <= '")
                .append(mEndDate)
                .append("' and PaytoDate = PayEndDate")
                .append(" and PayIntv=0 and AppFlag='1' and (StateFlag is null or StateFlag = '1') )");

        String GrpContStr = tSBql.toString();
        System.out.println(GrpContStr);
        System.out.println("续保团单抽档开始");
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContSet = tLCGrpContDB.executeQuery(GrpContStr);
        if (tLCGrpContDB.mErrors.needDealError()) {
            this.mErrors.addOneError("团单号:" + mLCGrpContSchema.getGrpContNo() +
                                     "查询错误!");
            return false;
        }
        if (tLCGrpContSet.size() == 0) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "催收错误,可能原因：已被其他业务员催收!");
            return false;
        }
        //下面校验lcgrppol是否有符合条件的记录，将符合条件的lcgrppol保存到mLCGrpPolSet私有变量中
        tSBql = new StringBuffer(128);
        tSBql.append("select * from LCGrpPol where GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and PaytoDate >= '")
                .append(mStartDate)
                .append("' and PaytoDate <= '")
                .append(mEndDate)
                .append("' and PaytoDate = PayEndDate  and AppFlag='1' and (StateFlag is null or StateFlag = '1') and ManageCom like '")
                .append(ManageCom)
                .append("%'")
                .append(" and PayIntv = 0 and GrpPolNo not in (select GrpPolNo from LJSPayGrp)");
        String GrpPolStr = tSBql.toString();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        System.out.println(GrpPolStr);
        System.out.println("续保团险抽档开始");
        mLCGrpPolSet = tLCGrpPolDB.executeQuery(GrpPolStr);
        if (tLCGrpPolDB.mErrors.needDealError()) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "下的集体险种查询失败！");
            return false;
        }
        if (mLCGrpPolSet.size() == 0) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "下没有符合催收条件的集体险种！");
            return false;
        }

        return true;
    }

    // 为更新数据库准备数据
    public boolean prepareData() {


    	map.put(newLCGrpPolSet, "INSERT");
    	map.put(newLCGrpContSchema, "INSERT");
    	map.put(mLCRnewStateLogSet, "INSERT");
        map.put(mLJSPayGrpSet, "INSERT");
        map.put(mLJSPaySchema, "INSERT");
        map.put(mLJSPayGrpBSet, "INSERT");
        map.add(mapAcc);
        map.put(mLJSPayBSchema, "INSERT");
        map.put(mLOPRTManagerSubSet, "INSERT");
        map.put(mLOPRTManagerSchema, "INSERT");


        updateDealState();
        mInputData.add(map);
        return true;
    }

    /*
     *判断保单是否有理赔:如果有返回false,没有返回true;
     *@return boolean
     */

    private boolean checkLPState()
    {
  	  String tGrpContNo=mLCGrpContSchema.getGrpContNo();
  	  ExeSQL tExeSQL = new ExeSQL();
  	  String getContNoSQL = "select contno from lccont where grpcontno = '"+tGrpContNo+"'";
  	  SSRS tSSRS2 = tExeSQL.execSQL(getContNoSQL);
  	  if(tSSRS2.getMaxRow()>0){
  		  for(int i = 1;i<tSSRS2.getMaxRow();i++){
  			  String tContNo = tSSRS2.GetText(i, 1);
  			  if(tContNo!=null && !tContNo.equals(""))
  			  {
  				  //校验保单下被保人如果正在理赔不能续期抽档
  				  String sql ="SELECT a.CusTomerName FROM LLCase a,LCPol b "
  					  +"WHERE a.CusTomerNo = b.InsuredNo "
  					  +"AND a.RGTState NOT IN ('11','12','14') AND b.contno='" + tContNo + "' ";
  				  System.out.println(sql);
  				  
  				  SSRS tSSRS = tExeSQL.execSQL(sql);
  				  if(tSSRS.getMaxRow()>0)
  				  {  
  					  this.mErrors.addOneError("被保人"+tSSRS.GetText(1, 1)+"有未结案的理赔。");//
  					  return false;
  				  }
  				  
  			  }
  		  }
  	  }
        return true;
    }
//    传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("GRnewDueFeeBL Begin......");
        this.mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
//    	避免并发错误，加锁
        if(!lockGrpCont())
        {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {
            //添加数据到催收核销日志表
            if (!dealUrgeLog("2", "PubSubmit:处理总应收表等失败!", "UPDATE")) {
                return false;
            }

            this.mErrors.addOneError("PubSubmit:处理数据库失败!");

            return false;
        }
        //添加数据到催收核销日志表
        //非批次核销
        if (mSerGrpNo == null || mSerGrpNo.equals("")) {
            if (!dealUrgeLog("3", "", "UPDATE")) {
                return false;
            }
        }

        return true;
    }

    /**
     * 生成新团单号
     * @return boolean
     */
    private boolean createNewNo()
    {
        newProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",mLCGrpContSchema.getAppntNo());

        return true;
    }
    

    
    
    
    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean dealData() {

        System.out.println("GRnewDueFeeBL dealData Begin");

        //获取页面参数
        String startDate = (String) mTransferData.getValueByName("StartDate");
        String endDate = (String) mTransferData.getValueByName("EndDate"); //获取页面
        mSerGrpNo = (String) mTransferData.getValueByName("serNo");

        String Operator = tGI.Operator; //保存登陆管理员账号
        String ManageCom = (String) mTransferData.getValueByName("ManageCom");

        double totalPay = 0; //计算合同下lcprem保费项记录的交费总和
        String serNo = "";
        String prtSeqNo = "";
        String tNo = "";
        String tLimit = "";

        int intCont = 0;

        //下面查找LJSPayPerson的记录
        String strPerSQL =
                " SELECT PolNo,GetNoticeNo,SerialNo  FROM LJSPayPerson where GrpContNo = '"
                + mLCGrpContSchema.getGrpContNo() + "' fetch first row only";
        ExeSQL tExeSQL = new ExeSQL();
        mPerSSRS = tExeSQL.execSQL(strPerSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 LJSPayPerson 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mPerSSRS.MaxRow >= 1) {
            intCont = 1;
            tNo = mPerSSRS.GetText(1, 2);
            serNo = mPerSSRS.GetText(1, 3);
        }
        mserNo = serNo;
        if (intCont != 1) {
            tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
            tNo = PubFun1.CreateMaxNo("PayNoticeNo", tLimit);
            if (mSerGrpNo == null || mSerGrpNo.equals("")) {
                serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                mserNo = serNo;
                mOperateFlag = "1";
                //添加数据到催收核销日志表
                if (!dealUrgeLog("1", "", "INSERT")) {
                    return false;
                }

            } else {
                serNo = mSerGrpNo;
                mserNo = serNo;
                mOperateFlag = "2";
            }
        }
        
        double WLPrate = checkWLPYH();//无理赔优惠费率
        double GYHrate = getGYHRate();//家庭团体优惠费率

        // 产生通知书号--所有个人共用一个交费通知书号
        tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);

        boolean yelFlag = false; //上次余额的标志
        boolean yetFlag = false; //置是否有余额的标志位（上期合同余额用于交本期保费还有剩余，则置true）
        LJSPayGrpSchema yelLJSPayGrpSchema = new LJSPayGrpSchema(); //如果有差额，添加一个应收集体交费schema
        LJSPayGrpSchema yetLJSPayGrpSchema = new LJSPayGrpSchema();
        LJSPayGrpBSchema yelLJSPayGrpBSchema = new LJSPayGrpBSchema(); //如果有差额，添加一个应收集体交费schema
        LJSPayGrpBSchema yetLJSPayGrpBSchema = new LJSPayGrpBSchema();

        //计算 缴费日期
        if(!calPayDate(mLCGrpPolSet, mLCGrpContSchema.getPayMode()))
        {
            return false;
        }
        String strNewPayToDate2 = "";

        System.out.println("GRnewDueFeeBL： 产生应收记录");

        for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
            double grpSumPay = 0.0;
            int childNum = getChildNum();

            LCGrpPolSchema tLCGrpPolSchema = mLCGrpPolSet.get(i);
            String GrpContNo = tLCGrpPolSchema.getGrpContNo();

            FDate tD = new FDate();
            Date newBaseDate = tD.getDate(tLCGrpPolSchema.getPayEndDate());
            Date NewPayToDate = PubFun.calDate(newBaseDate, 1, "Y", null);
            strNewPayToDate = tD.getString(NewPayToDate); //存入到ljspaygrp的新的交至日期

            //下面获取第一条记录的 newPayDate和newPayToDate以方便以后在yelLjsPayGrp和yetLjsPayGrp里赋值
            if (i == 1) {
                strNewPayToDate2 = strNewPayToDate;
            }

            //下面查询出符合条件的lccont
            StringBuffer tSBql = new StringBuffer(128);
            tSBql.append("select * from lccont where grpcontno='")
                    .append(GrpContNo)
                    .append("' and appflag='1' and (StateFlag is null or StateFlag = '1')")
                    .append(" and CInValiDate>='")
                    .append(startDate)
                    .append("' and CInValiDate<='")
                    .append(endDate)
                    .append("' and PaytoDate=CInValiDate")
                    .append(" and ManageCom like '")
                    .append(ManageCom)
                    .append("%'")
                    .append(" and payintv=0")
                    .append(" and contno not in (select contno from ljspayperson where grpcontno = lccont.grpcontno)")
                    .append("order by contno ")
                    .append("with ur ");

            String contSqlStr = tSBql.toString();

            System.out.println("GRnewDueFeeBL： 取 lccont数据 ");
            System.out.println(contSqlStr);

            /** 新增方法:
             * 游标查询方法 */
            RSWrapper rswrapper = new RSWrapper();
            LCContSet tLCContSet = new LCContSet();
            rswrapper.prepareData(tLCContSet, contSqlStr);
            do {
                //循环取得2000条数据，进行处理
                rswrapper.getData();

                for (int j = 1; j <= tLCContSet.size(); j++) {

                	if(!checkRnewAge(tLCContSet.get(j))){
                		continue;
                	}
                    mLJSPayPersonBSet.clear();
                    mLJSPayPersonSet.clear();

                    LCContSchema tLCContSchema = new LCContSchema();
                    tLCContSchema = tLCContSet.get(j);
                    String contNo = tLCContSchema.getContNo();
                    
                    LCPolSet tLCPolSet = new LCPolSet();
                    LCPolDB tLCPolDB = new LCPolDB();
                    tLCPolDB.setContNo(tLCContSchema.getContNo());
                    tLCPolSet = tLCPolDB.query();
                    
                    //调用续保申请
                    VData data = new VData();
                    data.add(tGI);
                    data.add(tLCContSchema);
                    data.add(tLCPolSet);

                    GRnewGrpContAppBL tGRnewGrpContAppBL = new GRnewGrpContAppBL();
                    if(!tGRnewGrpContAppBL.submitData(data, mOperate))
                    {
                        mErrors.copyAllErrors(tGRnewGrpContAppBL.mErrors);
                        return false;
                    }

                    System.out.println("GRnewDueFeeBL： 设置LJSPayPerson 数据 ");
                    

                    //循环每个险种进行处理
                    for(int k = 1; k <= tLCPolSet.size(); k++){
                        //获取合同下的每条险种的明细信息
                        LCPolSchema tNewLCPolSchema;
                        LCPolSchema tOldLCPolSchema = tLCPolSet.get(k).getSchema(); //对应的原险种信息，若是续期则为险种本身
                        boolean childFlag = isChild(tOldLCPolSchema);
                        tNewLCPolSchema = getNewLCPolSchema(tOldLCPolSchema);
                        if(tNewLCPolSchema == null)
                        {
                        	return false;
                        }

                        //得到险种下责任信息
                        LCDutyDB tLCDutyDB = new LCDutyDB();
                        tLCDutyDB.setPolNo(tNewLCPolSchema.getPolNo());
                        LCDutySet tLCDutySet = tLCDutyDB.query();
                        if(tLCDutySet.size() == 0){
                            CError tError = new CError();
                            tError.moduleName = "IndiDueFeeBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "没有查询到险种的责任信息："
                                                  + tOldLCPolSchema.getInsuredNo() + " "
                                                  + tOldLCPolSchema.getRiskCode();
                            mErrors.addOneError(tError);
                            System.out.println(tError.errorMessage);
                            return false;
                        }

                        for(int dutyCount = 1; dutyCount <= tLCDutySet.size(); dutyCount++){
                            LCDutySchema tLCDutySchema = tLCDutySet.get(dutyCount);

                            //得到责任下所有缴费
                            LCPremDB tLCPremDB = new LCPremDB();
                            tLCPremDB.setPolNo(tLCDutySchema.getPolNo());
                            tLCPremDB.setDutyCode(tLCDutySchema.getDutyCode());
                            LCPremSet tLCPremSet = tLCPremDB.query();
                            if(tLCPremSet.size() == 0)
                            {
                                CError tError = new CError();
                                tError.moduleName = "IndiDueFeeBL";
                                tError.functionName = "dealData";
                                tError.errorMessage = "没有查询到责任的缴费项信息："
                                                      + tOldLCPolSchema.getInsuredNo() + " "
                                                      + tOldLCPolSchema.getRiskCode() + " "
                                                      + tLCDutySchema.getDutyCode();
                                mErrors.addOneError(tError);
                                System.out.println(tError.errorMessage);
                                return false;
                            }

                            for(int premCount = 1; premCount <= tLCPremSet.size(); premCount++){
                                LCPremSchema tLCPremSchema = tLCPremSet.get(premCount); //保费项纪录
                                double prem = tLCPremSchema.getPrem() * (1 - tLCDutySchema.getFreeRate()) * GYHrate * WLPrate;
                                if(childFlag){
                                	prem = prem/childNum;
                                }
                                setOneLJSPayPerson(tOldLCPolSchema, tNewLCPolSchema, tLCPremSchema, prem, "ZC", tNo);
                            }
                        }

                    }
                    //TODO 将分单应收记录表落地
                    PubSubmit tPubSubmit = new PubSubmit();
                    VData tVData = new VData();
                    MMap tmap = new MMap();
                    tmap.put(mLJSPayPersonSet,"INSERT");
                    tmap.put(mLJSPayPersonBSet,"INSERT");
                    tVData.add(tmap);
                    if (tPubSubmit.submitData(tVData, "") == false) {
                        //添加数据到催收核销日志表
                        if (!dealUrgeLog("2", "PubSubmit:处理分单应收表失败!", "UPDATE")) {
                            return false;
                        }

                        this.mErrors.addOneError("PubSubmit:处理数据库失败!");

                        return false;
                    }

                }
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } while (tLCContSet.size() > 0);
            
            //TODO 生成临时团单号和团单险种号码
            
            createNewNo();
            String newProposalNo = PubFun1.CreateMaxNo("GrpProposalNo", PubFun.getNoLimit(mLCGrpContSchema.getManageCom()));
            
            LCGrpPolSchema newLCGrpPolSchema = new LCGrpPolSchema();
            ref.transFields(newLCGrpPolSchema, tLCGrpPolSchema);
            
            newLCGrpPolSchema.setGrpContNo(newProposalGrpContNo);
            newLCGrpPolSchema.setGrpPolNo(newProposalNo);
            newLCGrpPolSchema.setGrpProposalNo(newProposalNo);
            newLCGrpPolSchema.setFirstPayDate(strNewLJSStartDate);
            newLCGrpPolSchema.setPaytoDate(strNewPayToDate);
            newLCGrpPolSchema.setPayEndDate(strNewPayToDate);
            newLCGrpPolSchema.setOperator(Operator);
            newLCGrpPolSchema.setMakeDate(CurrentDate);
            newLCGrpPolSchema.setMakeTime(CurrentTime);
            newLCGrpPolSchema.setModifyDate(CurrentDate);
            newLCGrpPolSchema.setModifyTime(CurrentTime);
            
            newLCGrpPolSet.add(newLCGrpPolSchema);
            
            String polChangeNo = "update lcpol set grpcontno = '" + newProposalGrpContNo + "',grppolno = '" + newProposalNo + "' where grpcontno = '" + tLCGrpPolSchema.getGrpContNo() + "' and grppolno = '" + tLCGrpPolSchema.getGrpPolNo() + "' and contno like '13%' and polno like '11%'";
            map.put(polChangeNo, "UPDATE");            
            
            //TODO 设置续保状态
            //下面查找LCRnewStateLog的记录
            String RenewCountSQL = " SELECT max(RenewCount) FROM LCRnewStateLog where GrpContNo = '" + mLCGrpContSchema.getGrpContNo() + "'";
            String RenewCount = tExeSQL.getOneValue(RenewCountSQL);

            LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
            tLCRnewStateLogSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            tLCRnewStateLogSchema.setNewGrpContNo(newLCGrpPolSchema.getGrpContNo());
            tLCRnewStateLogSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLCRnewStateLogSchema.setNewGrpPolNo(newLCGrpPolSchema.getGrpPolNo());
            tLCRnewStateLogSchema.setContNo(tLCGrpPolSchema.getGrpContNo());
            tLCRnewStateLogSchema.setNewContNo(newLCGrpPolSchema.getGrpContNo());
            tLCRnewStateLogSchema.setPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLCRnewStateLogSchema.setNewPolNo(newLCGrpPolSchema.getGrpPolNo());
            tLCRnewStateLogSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
            tLCRnewStateLogSchema.setRenewCount(RenewCount);
            tLCRnewStateLogSchema.setRiskFlag("M");

            tLCRnewStateLogSchema.setState(XBConst.RNEWSTATE_UNCONFIRM);
            tLCRnewStateLogSchema.setOperator(Operator);
            PubFun.fillDefaultField(tLCRnewStateLogSchema);
            tLCRnewStateLogSchema.setPaytoDate(tLCGrpPolSchema.getPaytoDate());
            tLCRnewStateLogSchema.setMainRiskStatus("0");
            
            mLCRnewStateLogSet.add(tLCRnewStateLogSchema);

            String logChangeNo = "update lcrnewstatelog set newgrpcontno = '" + newLCGrpPolSchema.getGrpContNo() + "',newgrppolno = '" + newLCGrpPolSchema.getGrpPolNo() + "' where grpcontno = '" + tLCGrpPolSchema.getGrpContNo() + "' and state = '3' ";
            map.put(logChangeNo, "UPDATE");
                        
            
            //下面生成ljspaygrp的记录
            String strSQL = " SELECT SUM(SumDuePayMoney) FROM LJSPayPerson where GrpPolNo='" + tLCGrpPolSchema.getGrpPolNo() + "'";
            tExeSQL = new ExeSQL();
            String strGrpSumPay = tExeSQL.getOneValue(strSQL);
            if (tExeSQL.mErrors.needDealError()) {
                //添加数据到催收核销日志表
                if (!dealUrgeLog("2", "取LJSPayPerson总保费失败", "UPDATE")) {
                    return false;
                }
                CError tError = new CError();
                tError.moduleName = "GRnewDueFeeBL";
                tError.functionName = "DealData";
                tError.errorMessage = "查询数据 LJSPayPerson 出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (strGrpSumPay == null || strGrpSumPay.equals("")) {
                grpSumPay = 0;
            } else {
                grpSumPay = Double.parseDouble(strGrpSumPay);
            }

            LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
            tLJSPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo()); 
            tLJSPayGrpSchema.setPayCount(1); //交费次数
            tLJSPayGrpSchema.setGetNoticeNo(tNo); // 通知书号
            tLJSPayGrpSchema.setSumDuePayMoney(grpSumPay);
            tLJSPayGrpSchema.setSumActuPayMoney(grpSumPay);
            tLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJSPayGrpSchema.setPayDate(strNewLJSPayDate); //交费日期
            tLJSPayGrpSchema.setPayType("ZC"); //交费类型=ZC 正常交费
            tLJSPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJSPayGrpSchema.setCurPayToDate(strNewPayToDate);
            tLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJSPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
            tLJSPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJSPayGrpSchema.setSerialNo(serNo);
            tLJSPayGrpSchema.setInputFlag("1");

            tLJSPayGrpSchema.setOperator(Operator);
            tLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            tLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            tLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            tLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            tLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

            mLJSPayGrpSet.add(tLJSPayGrpSchema);
            //备份表
            LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
            tLJSPayGrpBSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo()); 
            tLJSPayGrpBSchema.setPayCount(1); //交费次数
            tLJSPayGrpBSchema.setGetNoticeNo(tNo); // 通知书号
            tLJSPayGrpBSchema.setSumDuePayMoney(grpSumPay);
            tLJSPayGrpBSchema.setSumActuPayMoney(grpSumPay);
            tLJSPayGrpBSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); //交费日期
            tLJSPayGrpBSchema.setPayType("ZC"); //交费类型=ZC 正常交费
            tLJSPayGrpBSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJSPayGrpBSchema.setCurPayToDate(strNewPayToDate);
            tLJSPayGrpBSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJSPayGrpBSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJSPayGrpBSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
            tLJSPayGrpBSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJSPayGrpBSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJSPayGrpBSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJSPayGrpBSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJSPayGrpBSchema.setSerialNo(serNo);

            tLJSPayGrpBSchema.setInputFlag("1");

            tLJSPayGrpBSchema.setOperator(Operator);
            tLJSPayGrpBSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJSPayGrpBSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            tLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            tLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            tLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            tLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            if(tLJSPayGrpBSchema.getSumDuePayMoney()>0.01)
                tLJSPayGrpBSchema.setDealState("0"); //催收状态：不成功
            else
                tLJSPayGrpBSchema.setDealState("4"); //催收状态：待核销
            mLJSPayGrpBSet.add(tLJSPayGrpBSchema);

            totalPay += grpSumPay; //计算出合同下总的应交费用
            LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                    LOPRTManagerSubSchema();
            tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSubSchema.setGetNoticeNo(tNo);
            tLOPRTManagerSubSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSubSchema.setOtherNoType("01");
            tLOPRTManagerSubSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLOPRTManagerSubSchema.setDuePayMoney(grpSumPay);
            tLOPRTManagerSubSchema.setAppntName(mLCGrpContSchema.getAppntNo());
            tLOPRTManagerSubSchema.setTypeFlag("1");
            mLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);

        }
        
        //TODO 生成团单临时数据
        ref.transFields(newLCGrpContSchema, mLCGrpContSchema);
        
        newLCGrpContSchema.setGrpContNo(newProposalGrpContNo);
        newLCGrpContSchema.setProposalGrpContNo(newProposalGrpContNo);
        newLCGrpContSchema.setPolApplyDate(CurrentDate);
        newLCGrpContSchema.setCValiDate(PubFun.calDate(newLCGrpContSchema.getCValiDate(), 1, "Y", null));
        newLCGrpContSchema.setCInValiDate(PubFun.calDate(newLCGrpContSchema.getCInValiDate(), 1, "Y", null));
        newLCGrpContSchema.setOperator(Operator);
        newLCGrpContSchema.setMakeDate(CurrentDate);
        newLCGrpContSchema.setMakeTime(CurrentTime);
        newLCGrpContSchema.setModifyDate(CurrentDate);
        newLCGrpContSchema.setModifyTime(CurrentTime);
        
        String contChangeNo = "update lccont set grpcontno = '" + newProposalGrpContNo + "' where grpcontno = '" + mLCGrpContSchema.getGrpContNo() + "' and contno like '13%'";
        map.put(contChangeNo, "UPDATE");
        String premChangeNo = "update lcprem set grpcontno = '" + newProposalGrpContNo + "' where grpcontno = '" + mLCGrpContSchema.getGrpContNo() + "' and contno like '13%'";
        map.put(premChangeNo, "UPDATE");
        String getChangeNo = "update lcget set grpcontno = '" + newProposalGrpContNo + "' where grpcontno = '" + mLCGrpContSchema.getGrpContNo() + "' and contno like '13%'";
        map.put(getChangeNo, "UPDATE");
                
        //下面根据情况生成yel和yet的ljspaygrp的记录
        double leavingMoney = getAccBala(mLCGrpContSchema.getAppntNo());
        if (leavingMoney != Double.MIN_VALUE && leavingMoney > 0.0) {
            yelFlag = true;
            yelLJSPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            yelLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yelLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yelLJSPayGrpSchema.setGetNoticeNo(tNo); //通知书号
            yelLJSPayGrpSchema.setSumDuePayMoney( -leavingMoney);
            yelLJSPayGrpSchema.setSumActuPayMoney( -leavingMoney);
            yelLJSPayGrpSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yelLJSPayGrpSchema.setPayDate(strNewLJSPayDate); //交费日期
            yelLJSPayGrpSchema.setPayType("YEL");
            yelLJSPayGrpSchema.setLastPayToDate(mLCGrpPolSet.get(1).getPaytoDate());
            yelLJSPayGrpSchema.setCurPayToDate(strNewPayToDate2);
            yelLJSPayGrpSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
            yelLJSPayGrpSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
            yelLJSPayGrpSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());

            yelLJSPayGrpSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yelLJSPayGrpSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yelLJSPayGrpSchema.setAgentCode(mLCGrpPolSet.get(1).getAgentCode());
            yelLJSPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(1).getAgentGroup());
            yelLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yelLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yelLJSPayGrpSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yelLJSPayGrpSchema.setInputFlag("1");
            yelLJSPayGrpSchema.setSerialNo(serNo);
            yelLJSPayGrpSchema.setOperator(Operator);
            yelLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yelLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yelLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yelLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

            mLJSPayGrpSet.add(yelLJSPayGrpSchema);

            //备份表
            yelLJSPayGrpBSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            yelLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yelLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yelLJSPayGrpBSchema.setGetNoticeNo(tNo); //通知书号
            yelLJSPayGrpBSchema.setSumDuePayMoney( -leavingMoney);
            yelLJSPayGrpBSchema.setSumActuPayMoney( -leavingMoney);
            yelLJSPayGrpBSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yelLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); //交费日期
            yelLJSPayGrpBSchema.setPayType("YEL");//投保人账户转出
            yelLJSPayGrpBSchema.setLastPayToDate(mLCGrpPolSet.get(1).getPaytoDate());
            yelLJSPayGrpBSchema.setCurPayToDate(strNewPayToDate2);
            yelLJSPayGrpBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
            yelLJSPayGrpBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
            yelLJSPayGrpBSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());

            yelLJSPayGrpBSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yelLJSPayGrpBSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yelLJSPayGrpBSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yelLJSPayGrpBSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yelLJSPayGrpBSchema.setInputFlag("1");
            yelLJSPayGrpBSchema.setSerialNo(serNo);
            yelLJSPayGrpBSchema.setOperator(Operator);
            yelLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yelLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yelLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yelLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            yelLJSPayGrpBSchema.setDealState("0"); //催收状态：不成功

            mLJSPayGrpBSet.add(yelLJSPayGrpBSchema);

            dealAccBala(yelLJSPayGrpBSchema);
        }
        if (yelFlag && leavingMoney > totalPay) {
            yetFlag = true;
            yetLJSPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            yetLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yetLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yetLJSPayGrpSchema.setGetNoticeNo(tNo); //通知书号
            yetLJSPayGrpSchema.setSumDuePayMoney(leavingMoney - totalPay);
            yetLJSPayGrpSchema.setSumActuPayMoney(leavingMoney - totalPay);
            yetLJSPayGrpSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yetLJSPayGrpSchema.setPayDate(strNewLJSPayDate); //交费日期
            yetLJSPayGrpSchema.setPayType("YET"); //投保人账户转入
            yetLJSPayGrpSchema.setLastPayToDate(mLCGrpPolSet.get(1).getPaytoDate());
            yetLJSPayGrpSchema.setCurPayToDate(strNewPayToDate2);
            yetLJSPayGrpSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
            yetLJSPayGrpSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
            yetLJSPayGrpSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());

            yetLJSPayGrpSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yetLJSPayGrpSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yetLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yetLJSPayGrpSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yetLJSPayGrpSchema.setAgentCode(mLCGrpPolSet.get(1).getAgentCode());
            yetLJSPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(1).getAgentGroup());
            yetLJSPayGrpSchema.setInputFlag("1");
            yetLJSPayGrpSchema.setSerialNo(serNo);
            yetLJSPayGrpSchema.setOperator(Operator);
            yetLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yetLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yetLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yetLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

            mLJSPayGrpSet.add(yetLJSPayGrpSchema);
            

            //备份表
            yetLJSPayGrpBSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            yetLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yetLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yetLJSPayGrpBSchema.setGetNoticeNo(tNo); //通知书号
            yetLJSPayGrpBSchema.setSumDuePayMoney(leavingMoney - totalPay);
            yetLJSPayGrpBSchema.setSumActuPayMoney(leavingMoney - totalPay);
            yetLJSPayGrpBSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yetLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); //交费日期
            yetLJSPayGrpBSchema.setPayType("YET"); //交费类型=yet
            yetLJSPayGrpBSchema.setLastPayToDate(mLCGrpPolSet.get(1).getPaytoDate());
            yetLJSPayGrpBSchema.setCurPayToDate(strNewPayToDate2);
            yetLJSPayGrpBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
            yetLJSPayGrpBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
            yetLJSPayGrpBSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());
            yetLJSPayGrpBSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yetLJSPayGrpBSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yetLJSPayGrpBSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yetLJSPayGrpBSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yetLJSPayGrpBSchema.setInputFlag("1");
            yetLJSPayGrpBSchema.setSerialNo(serNo);
            yetLJSPayGrpBSchema.setOperator(Operator);
            yetLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yetLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yetLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yetLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            yetLJSPayGrpBSchema.setDealState("0"); //催收状态：待收费
            mLJSPayGrpBSet.add(yetLJSPayGrpBSchema);
            
            dealAccBalaYET(yetLJSPayGrpBSchema);//账户余额大于续期保费要在账户轨迹退费处理
        }
        //下面生成应收总表的记录LJSPAY
        if (yelFlag) {
            //如果团单上有余额,并且余额足够支付团单下应交的所有费用
            if (yetFlag) {
                totalsumPay = 0;
            } else {
                totalsumPay = totalPay - leavingMoney;
            }
        } else {
            totalsumPay = totalPay;
        }
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(tNo); // 通知书号
        tLJSPaySchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLJSPaySchema.setOtherNoType("1");
        tLJSPaySchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLJSPaySchema.setSumDuePayMoney(totalsumPay);
        tLJSPaySchema.setPayDate(strNewLJSPayDate); //有冲突,暂定为取集体险种最早的日期
        tLJSPaySchema.setStartPayDate(strNewLJSStartDate); //交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
        tLJSPaySchema.setBankOnTheWayFlag("0");
        tLJSPaySchema.setBankSuccFlag("0");
        tLJSPaySchema.setSendBankCount(0);
        tLJSPaySchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
        tLJSPaySchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
        tLJSPaySchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJSPaySchema.setAccName(mAccName);
        tLJSPaySchema.setBankAccNo(mBankAccNo);
        tLJSPaySchema.setBankCode(mBankCode);
        tLJSPaySchema.setSerialNo(serNo);
        tLJSPaySchema.setOperator(Operator);
        tLJSPaySchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLJSPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLJSPaySchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLJSPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLJSPaySchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());

        String sql = "select LF_SaleChnl('2', '"+ mLCGrpContSchema.getGrpContNo()+"'),LF_MarketType('"+ mLCGrpContSchema.getGrpContNo()+"') from dual where 1=1";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow()>0)
        {
        	String tSaleChnl = tSSRS.GetText(1, 1);
            String tMarketType = tSSRS.GetText(1, 2);
            tLJSPaySchema.setSaleChnl(tSaleChnl);
            tLJSPaySchema.setMarketType(tMarketType);
        }
        
        mLJSPaySchema.setSchema(tLJSPaySchema);
        //备份表
        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema.setGetNoticeNo(tNo); // 通知书号
        tLJSPayBSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLJSPayBSchema.setOtherNoType("1");
        tLJSPayBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLJSPayBSchema.setSumDuePayMoney(totalsumPay);
        tLJSPayBSchema.setPayDate(strNewLJSPayDate); //有冲突,暂定为取集体险种最早的日期
        tLJSPayBSchema.setStartPayDate(strNewLJSStartDate); //交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
        tLJSPayBSchema.setBankOnTheWayFlag("0");
        tLJSPayBSchema.setBankSuccFlag("0");
        tLJSPayBSchema.setSendBankCount(0);
        tLJSPayBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
        tLJSPayBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
        tLJSPayBSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJSPayBSchema.setAccName(mAccName);
        tLJSPayBSchema.setBankAccNo(mBankAccNo);
        tLJSPayBSchema.setBankCode(mBankCode);
        tLJSPayBSchema.setSerialNo(serNo);
        tLJSPayBSchema.setOperator(Operator);
        tLJSPayBSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLJSPayBSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLJSPayBSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLJSPayBSchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLJSPayBSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLJSPayBSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());
        if(tLJSPayBSchema.getSumDuePayMoney()>0.01)
            tLJSPayBSchema.setDealState("0"); //催收状态：待收费
        else
            tLJSPayBSchema.setDealState("4"); //催收状态：待核销
        
        tLJSPayBSchema.setSaleChnl(mLJSPaySchema.getSaleChnl());
        tLJSPayBSchema.setMarketType(mLJSPaySchema.getMarketType());

        mLJSPayBSchema.setSchema(tLJSPayBSchema);


        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLOPRTManagerSchema.setOtherNoType("01");
        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
        mLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(tNo); //这里存放 催缴通知书号（也为交费收据号）
        
        //续保状态变更为已催收
        sql = "  update LCRnewStateLog "
                     + "set state = '" + XBConst.RNEWSTATE_UNCONFIRM + "', "
                     + "   operator = '" + tGI.Operator + "', "
                     + "   modifyDate = '" + CurrentDate + "' , "
                     + "   modifyTime = '" + CurrentTime + "' "
                     + "where grpcontNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
                     + "   and state = '3' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 生成个人应收记录
     * @param tOldLCPolSchema LCPolSchema：原险种信息，若为续保，则是续保全险种信息
     * @param tLCPolSchema LCPolSchema：需生成应收记录的险种信息，若为续保则是保费重算后的数据
     * @param tLCPremSchema LCPremSchema：缴费项数据
     * @param money double：应缴费
     * @param payType String：缴费类型
     */
    private void setOneLJSPayPerson(
        LCPolSchema tOldLCPolSchema,
        LCPolSchema tNewLCPolSchema,
        LCPremSchema tLCPremSchema,
        double money,
        String payType,String tGetNoticeNo)
    {
        LJSPayPersonSchema schema = new LJSPayPersonSchema();

        schema.setPolNo(tOldLCPolSchema.getPolNo());
        schema.setSumDuePayMoney(money);
        schema.setSumActuPayMoney(money);
        schema.setPayCount(1);
        schema.setGrpContNo(tOldLCPolSchema.getGrpContNo());
        schema.setGrpPolNo(tOldLCPolSchema.getGrpPolNo());
        schema.setContNo(tOldLCPolSchema.getContNo());
        schema.setDutyCode(tLCPremSchema.getDutyCode());
        schema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
        schema.setGetNoticeNo(tGetNoticeNo); // 通知书号
        schema.setPayAimClass("1"); //交费目的分类=个人
        schema.setPayIntv(tOldLCPolSchema.getPayIntv());

        //:交费日期=个人保单表的交至日期
        schema.setPayDate(strNewLJSPayDate);
        schema.setPayType(payType); //交费类型=yel
        schema.setAppntNo(tOldLCPolSchema.getAppntNo());

        //:这条纪录的//原交至日期=个人保单表的交至日期
        //现交至日期CurPayToDate=个人保单表的交至日期+交费间隔 待修改
        schema.setLastPayToDate(tNewLCPolSchema.getPaytoDate());
        schema.setCurPayToDate(strNewPayToDate);
        schema.setBankCode(mBankCode);
        schema.setBankAccNo(mBankAccNo);
        schema.setBankOnTheWayFlag("0");
        schema.setBankSuccFlag("0");
        schema.setPayTypeFlag("1");
        schema.setApproveCode(tNewLCPolSchema.getApproveCode());
        schema.setApproveDate(tNewLCPolSchema.getApproveDate());
        schema.setManageCom(tNewLCPolSchema.getManageCom());
        schema.setAgentCom(tNewLCPolSchema.getAgentCom());
        schema.setAgentType(tNewLCPolSchema.getAgentType());
        schema.setRiskCode(tNewLCPolSchema.getRiskCode());
        schema.setSerialNo(mserNo); //统一一个流水号
        schema.setInputFlag("1"); 
        schema.setOperator(tGI.Operator);
        schema.setMakeDate(CurrentDate); //入机日期
        schema.setMakeTime(CurrentTime); //入机时间
        schema.setModifyDate(CurrentDate); //最后一次修改日期
        schema.setModifyTime(CurrentTime); //最后一次修改时间
        schema.setAgentCode(tNewLCPolSchema.getAgentCode());
        schema.setAgentGroup(tNewLCPolSchema.getAgentGroup());
        mLJSPayPersonSet.add(schema);

        //生成B表信息
        LJSPayPersonBSchema schemaB = new LJSPayPersonBSchema();
        ref.transFields(schemaB, schema);
        schemaB.setDealState(FeeConst.DEALSTATE_WAITCHARGE);
        mLJSPayPersonBSet.add(schemaB);
    }
    
    /**
     * 根据原险种查询临时险种信息
     * @param schema LCPolSchema：原险种信息
     * @return LCPolSchema：临时险种信息
     */
    private LCPolSchema getNewLCPolSchema(LCPolSchema schema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCPol a, LCRnewStateLog b ")
            .append("where a.polNo = b.NewPolNo ")
            .append("   and b.PolNo = '")
            .append(schema.getPolNo())
            .append("' ");
        LCPolSet set = new LCPolDB().executeQuery(sql.toString());
        if(set == null)
        {
            mErrors.addOneError("没有查询到续保原险种" + schema.getRiskCode()+ "的临时险种信息");
            return null;
        }
        return set.get(1);
    }
    
    
    /**
     * 计算缴费日期
     * @return boolean
     */
    private boolean calPayDate(LCGrpPolSet tLCGrpPolSetXB, String payMode)
    {
        CalPayDateBL bl = new CalPayDateBL();
        HashMap dates = bl.calPayDateG(tLCGrpPolSetXB, payMode);
        if(dates == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        strNewLJSStartDate = (String) dates.get(CalPayDateBL.STARTPAYDATE);
        strNewLJSPayDate = (String) dates.get(CalPayDateBL.PAYDATE);
        if(strNewLJSStartDate == null || strNewLJSStartDate.equals("")
           || strNewLJSPayDate == null || strNewLJSPayDate.equals(""))
        {
            mErrors.addOneError("计算失败，没有得到缴费日期。");
            return false;
        }

        return true;
    }


    /**
     * 得到投保人帐户余额
     * @param appntNo String：投保人
     * @return double：帐户余额
     */
    private double getAccBala(String appntNo) {
        StringBuffer sql = new StringBuffer();
        sql.append(" select accGetMoney ")
                .append("from LCAppAcc ")
                .append("where customerNo = '").append(appntNo).append("' ");
        String accGetBala = new ExeSQL().getOneValue(sql.toString());
        if (accGetBala.equals("") || accGetBala.equals("null")) {
            return Double.MIN_VALUE;
        }
        return Double.parseDouble(accGetBala);
    }
    /**
     * 生成使用余额轨迹
     * @param yelLJSPayGrpBSchema LJSPayGrpBSchema：余额财务应收数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealAccBalaYET(LJSPayGrpBSchema yelLJSPayGrpBSchema) {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(yelLJSPayGrpBSchema.getAppntNo());
        tLCAppAccTraceSchema.setOtherType("1"); //团单号
        tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLCAppAccTraceSchema.setMoney(yelLJSPayGrpBSchema.getSumActuPayMoney());
        tLCAppAccTraceSchema.setOperator(tGI.Operator);

        AppAcc tAppAcc = new AppAcc();
        MMap tMMap = tAppAcc.accTakeOutXQYET(tLCAppAccTraceSchema);
        if(tMMap!=null){
        	//这里得到的是引用
        	LCAppAccSchema tLCAppAccSchema= (LCAppAccSchema) tMMap.getObjectByObjectName("LCAppAccSchema",0);
        	tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap.getObjectByObjectName("LCAppAccTraceSchema", 0);
        	//续期业务未结束，不能直接更新余额
        	tLCAppAccSchema.setAccBala(getAccBala(tLCAppAccSchema.getCustomerNo()));
        	tLCAppAccSchema.setState("0"); //续期核销时赋值为有效1
        	
        	tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());
        	tLCAppAccTraceSchema.setState(tLCAppAccSchema.getState());
        	tLCAppAccTraceSchema.setBakNo(yelLJSPayGrpBSchema.getGetNoticeNo());
        	mapAcc.put(tLCAppAccTraceSchema, "INSERT");
//        mapAcc.put(tLCAppAccSchema, "UPDATE");
        }

        return true;
    }

    /**
     * 生成使用余额轨迹
     * @param yelLJSPayGrpBSchema LJSPayGrpBSchema：余额财务应收数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealAccBala(LJSPayGrpBSchema yelLJSPayGrpBSchema) {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(yelLJSPayGrpBSchema.getAppntNo());
        tLCAppAccTraceSchema.setOtherType("1"); //团单号
        tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLCAppAccTraceSchema.setMoney(yelLJSPayGrpBSchema.getSumActuPayMoney());
        tLCAppAccTraceSchema.setOperator(tGI.Operator);

        AppAcc tAppAcc = new AppAcc();
        MMap tMMap = tAppAcc.accTakeOutXQ(tLCAppAccTraceSchema);
        if(tMMap!=null){
        	//这里得到的是引用
        	LCAppAccSchema tLCAppAccSchema
        	= (LCAppAccSchema) tMMap.getObjectByObjectName("LCAppAccSchema",
        			0);
        	tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap
        	.getObjectByObjectName("LCAppAccTraceSchema", 0);
        	//续期业务未结束，不能直接更新余额
        	tLCAppAccSchema.setAccBala(getAccBala(tLCAppAccSchema.getCustomerNo()));
        	tLCAppAccSchema.setState("0"); //续期核销时赋值为有效1
        	
        	tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());
        	tLCAppAccTraceSchema.setState(tLCAppAccSchema.getState());
        	tLCAppAccTraceSchema.setBakNo(yelLJSPayGrpBSchema.getGetNoticeNo());
        	mapAcc.put(tLCAppAccTraceSchema, "INSERT");
        	mapAcc.put(tLCAppAccSchema, "UPDATE");
        }

        return true;
    }


    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String pmDealState, String pmReason,
                                String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
            tLCUrgeVerifyLogSchema.setRiskFlag("1");
            tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
            tLCUrgeVerifyLogSchema.setOperateFlag(mOperateFlag); //1：个案操作,2：批次操作
            tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
            tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

            tLCUrgeVerifyLogSchema.setContNo(mLCGrpContSchema.getGrpContNo());
            tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
            StartDate = tLCUrgeVerifyLogSchema.getMakeDate();
            StartTime = tLCUrgeVerifyLogSchema.getMakeTime(); ;
            System.out.println("StartDate" + StartDate);
            System.out.println("StartTime" + StartTime);
            tLCUrgeVerifyLogSchema.setErrReason(pmReason);
        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("1");
            tLCUrgeVerifyLogDB.setOperateFlag(mOperateFlag);
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                tLCUrgeVerifyLogSchema.setErrReason(pmReason);
            } else {
                return false;
            }

        }

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);

        VData tInputData = new VData();
        tInputData.add(tMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }


    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public LOPRTManagerSchema getPrintData(LCGrpContSchema tLCGrpContSchema,
                                           String GetNoticeNo) {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        try {
            String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
            tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setStandbyFlag2(GetNoticeNo);
        } catch (Exception ex) {
            return null;
        }
        return tLOPRTManagerSchema;
    }

    
    //同步LJSPayGrpB表的DealState状态。当保费从LCAppAcc表抵充、SumActuPayMoney==0时,DealState应该为4而不是0,同LJSPayB是一样的
    private void updateDealState()
    {
    	String sql = "update LJSPayGrpB set DealState = '" + mLJSPayBSchema.getDealState() + "' "
    	           + "where GetNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' ";
    	System.out.println(sql);
    	map.put(sql, "UPDATE");
    }
    
    private boolean lockGrpCont()
    {
    	MMap tCekMap = null;
    	tCekMap = lockBalaCount(mLCGrpContSchema.getGrpContNo());
        if (tCekMap == null)
        {
            return false;
        }
        
        VData tInputData = new VData();
        tInputData.add(tCekMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LockTable表失败，不能在一小时之内对该保单进行重复催收!");
            return false;
        }
        
        return true;

    }
    /**
     * 锁定动作
     * @param aGrpContNo
     * @return MMap
     */
    private MMap lockBalaCount(String aGrpContNo)
    {
        MMap tMMap = null;
        /**续期催收"XQ"*/
        String tLockNoType = "XQ";
        /**锁定有效时间（秒）*/
        String tAIS = "600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aGrpContNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
    
    
    /**
     * 判断该家庭单首期生效日至满期日是否发生过理赔
     * @return
     */
    private double checkWLPYH(){
    	double rate = 1;
    	ExeSQL tExeSQL = new ExeSQL(); 
    	String contNoSQL = "select contno from lccont where grpcontno = '" + mLCGrpContSchema.getGrpContNo() + "'";
    	SSRS tContNoSSRS = tExeSQL.execSQL(contNoSQL);
    	if(tContNoSSRS.getMaxRow()>0){
    		for(int i = 1;i<=tContNoSSRS.getMaxRow();i++){
    			String contNo = tContNoSSRS.GetText(i, 1);
    			String mContInfosSQL = "select cvalidate,paytodate from lcpol where contno='" + contNo + "' and riskcode in ('162601','162801')";
    			SSRS tContInfosSSRS = null;
    			tContInfosSSRS = tExeSQL.execSQL(mContInfosSQL);
    			String date = tContInfosSSRS.GetText(1, 1);	  
    			String date2 = tContInfosSSRS.GetText(1, 2);
    			//首期生效日
    			String csql = "select cvalidate from lbcont where contno in (select newcontno from lcrnewstatelog where contno='" + contNo +"' and state='6' order by paytodate fetch first 1 rows only)";
    			String date3 = tExeSQL.getOneValue(csql);
    			if("".equals(date3) || date3 == null){
    				date3 = date;
    			}
    			String sql2 = "SELECT NVL(sum(a.realpay),0) "
    				+ " FROM llclaimpolicy a ,llcase b "
    				+ " WHERE a.caseno = b.caseno"
    				+ " AND a.contno = '" + contNo + "'"
    				+ " AND b.EndCaseDate between '" + date3 + "' and '" + date2 + "'"
    				+ " AND b.rgtstate IN ('09','11','12')";
    			String LPyn = tExeSQL.getOneValue(sql2);
    			if("0".equals(LPyn) || "0.0".equals(LPyn) || "0.00".equals(LPyn)){
    				rate = 0.9;
    			} else {
    				rate = 1;
    				break;
    			}    	
    		}
    	}
    	
    	return rate;
    }

    /**
     * 获取家庭单团体优惠费率
     * @return
     */
    private double getGYHRate(){
    	double rate = 1;
    	String tlcpolSQL = "select * from lcpol where grpcontno = '" + mLCGrpContSchema.getGrpContNo() + "'";
    	LCPolSet tLCPolSet = new LCPolSet();
    	LCPolDB tLCPolDB = new LCPolDB();
    	tLCPolSet = tLCPolDB.executeQuery(tlcpolSQL);
    	int peoples = tLCPolSet.size();
    	if(peoples<3){
    		return 1;
    	}
    	for(int i = 1;i<=tLCPolSet.size();i++){
    		String birthday = tLCPolSet.get(i).getInsuredBirthday();
    		int year = PubFun.calInterval(birthday,tLCPolSet.get(i).getEndDate(),"Y");
    		if(year >= 75){
    			peoples--;
    		}
    	}
        if(peoples>=3){
        	rate = 0.9;
        }       
        
    	return rate;
    }
    
    /**
     * 校验分单被保人年龄是否超过险种定义的最大续保年龄
     * @param tLCContSchema
     * @return
     */
    private boolean checkRnewAge(LCContSchema tLCContSchema){
    	
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();  //险种定义
        tLMRiskAppDB.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
        if(!tLMRiskAppDB.getInfo()){
            mErrors.addOneError("没有查询到险种信息，");
            return false;
        }
    	
        //校验被保人年龄是否超过险种定义的最大续保年龄
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(tLCContSchema.getContNo());
        tLCInsuredDB.setInsuredNo(tLCContSchema.getInsuredNo());
        if(!tLCInsuredDB.getInfo()){
            return false;
        }

        int tRnewAge = PubFun.calInterval(tLCInsuredDB.getBirthday(),
        		tLCContSchema.getPaytoDate(), "Y");

        System.out.println(tLCInsuredDB.getBirthday() + ", " + mLCPolSchema.getEndDate());
        System.out.println("续保投保年龄:" + tRnewAge);
        System.out.println("最大投保年龄描述:" + tLMRiskAppDB.getMaxRnewAge());
        //校验被保人年龄是否大于险种描述的最大年龄
        if (tRnewAge > Integer.parseInt(tLMRiskAppDB.getMaxRnewAge()))
        {
        	return false;
        }

    	return true;
    }
    
    /**
     * 判断该被保人是否是孩子
     * @param insuredno
     * @return
     */
    private boolean isChild(LCPolSchema tLCPolSchema){
    	ExeSQL tExeSQL = new ExeSQL(); 
    	//如果是作为连带被保人承保的
    	String sql = "select 1 from lcinsured where grpcontno = '"+mLCGrpContSchema.getGrpContNo()+"' and relationtomaininsured = '03' and insuredno = '"+tLCPolSchema.getInsuredNo()+"'";
		String result = tExeSQL.getOneValue(sql);
    	if("1".equals(result)){
    		return true;
    	}else{//否则按照年龄进行判断,续保时年龄大于19是成人（后续优化）
    		String birthday = tLCPolSchema.getInsuredBirthday();
    		int year = PubFun.calInterval(birthday,tLCPolSchema.getPaytoDate(),"Y");
    		if(year <= 19){
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    /**
     * 获取团单下孩子数量
     * @param insuredno
     * @return
     */
    private int getChildNum(){
    	ExeSQL tExeSQL = new ExeSQL(); 
    	String sql = "select count(1) from lcinsured where grpcontno = '"+mLCGrpContSchema.getGrpContNo()+"'";
		String num = tExeSQL.getOneValue(sql); 
		
    	return Integer.parseInt(num)-2;
    }
    
    
    
    
}
