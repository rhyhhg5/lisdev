package com.sinosoft.lis.xb;

import com.sinosoft.task.Task;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.operfee.IndiFinUrgeVerifyBL;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理续期续保核销功能(包括财务核销和续保确认及续保数据转移)，步骤如下：
 * 1、财务核销：调用续期核销统一核销续保续期财务数据，若有续保数据则进行下面操作
 * 2、续保确认：调用个单续保确认进行处理
 * 3、续保数据转移：调用个单续保数据转移进行处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class PRnewDueVerifyBL
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;  //操作员信息
    private TransferData mTransferData = null;

    private LCContSchema mLCContSchema = null;  //需要续保续期财务核销的原保单信息，可以只录入保单号
    private String mOperate = null;
    private String mGetNoticeNo ;

    private String mRnewState = null;

    /**
     * 无参构造方法
     */
    public PRnewDueVerifyBL()
    {
    }

    /**
     * 续保续期核销功能的对外接口，实现相关的操作
     * @param inputData VData：包括
     * 1、LCContSchema：保单号
     * 2、GlobalInput：操作员信息
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        mOperate = operate;

        if(!getInputData(inputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }
       
        return true;
    }

    /**
     * 进行业务处理
     * 分别调用续期续保核销和续保确认及转移程序
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Beginning of PRnewDueVerifyBL.dealData");

        //续期续保核销
        if(mTransferData == null)
        {
            mTransferData = new TransferData();
        }
        mTransferData.setNameAndValue("NeedRnewVerify", "y");

        VData data = new VData();
        data.add(mLCContSchema);
        data.add(mGlobalInput);
        data.add(mTransferData);

        boolean needFinVerify = needFinVerify();

        if(needFinVerify)
        {
            IndiFinUrgeVerifyBL tIndiFinUrgeVerifyBL = new IndiFinUrgeVerifyBL();
            if(!tIndiFinUrgeVerifyBL.submitData(data, "VERIFY"))
            {
                mErrors.copyAllErrors(tIndiFinUrgeVerifyBL.mErrors);
                return false;
            }
        }
        
        /**
         * @author wujun
         * 增加对特权复效后续期核销的数据逻辑处理
         */
         changeContStateDate();
        
        //续保确认和转移数据
        if(!needPRnewOperate())
        {
            return true;
        }
        
        

//        if(mRnewState.equals(XBConst.RNEWSTATE_UNCONFIRM))
//        {
//            data.clear();
//
//            LCPolDB db = new LCPolDB();
//            db.setContNo(mLCContSchema.getContNo());
//            LCPolSet tLCPolSet = db.query();
//
//            data.add(tLCPolSet);
//            data.add(mGlobalInput);
//            data.add(new TransferData());
//
//            System.out.println("Beginning of PRnewDueVerifyBL.dealData: PRnewContSign");
//            PRnewContSignBL tPRnewContSignBL = new PRnewContSignBL();
//            if(!tPRnewContSignBL.submitData(data, mOperate))
//            {
//                mErrors.copyAllErrors(tPRnewContSignBL.mErrors);
//                System.out.println(tPRnewContSignBL.mErrors.getErrContent());
//                return false;
//            }
//        }

        //续保数据转移

        String edorNo = null;
        String sql = "select WorkNo from LGWork a, LJSPayB b "
                     + "where a.InnerSource = b.GetNoticeNo "
                     + "   and b.OtherNoType = '2' "
                     + "   and b.OtherNo='" + mLCContSchema.getContNo() + "' ";
        String workNo = new ExeSQL().getOneValue(sql);
        if(!workNo.equals("") && !workNo.equals("null"))
        {
            edorNo = workNo;
        }

        data.clear();
        data.add(mLCContSchema);
        data.add(mGlobalInput);

        System.out.println("Beginning of PRnewDueVerifyBL.dealData: TransferCPDataToBP");
        TransferCPDataToBP tTransferCPDataToBP = new TransferCPDataToBP(edorNo);
        MMap tMMap = tTransferCPDataToBP.getSubmitMap(data, mOperate);
        if(tMMap == null && tTransferCPDataToBP.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tTransferCPDataToBP.mErrors);
            return false;
        }
        //LIULI 修改于080923  续保成功后对有健管险种的保单插入工作流数据
        String sqlh = "select RenewCount+1 from lcpol c where contno='"+mLCContSchema.getContNo()+"' and exists (select * from lmriskapp m where m.risktype2='5' and c.riskcode=m.riskcode) "
                      +"and appflag='1' and stateflag in ('0','1') with ur";
//        System.out.println("我的天   ："+sqlh);
        SSRS hSSRS = new ExeSQL().execSQL(sqlh);
        if(hSSRS.getMaxRow()>0 && Double.parseDouble(hSSRS.GetText(1,1))>0){
            LWMissionSchema tLWMissionSchema = new LWMissionSchema();
            String tMissionID = PubFun1.CreateMaxNo("MissionID", 20);
            tLWMissionSchema.setMissionID(tMissionID);
            tLWMissionSchema.setSubMissionID("1");
            tLWMissionSchema.setActivityID("0000001190");
            tLWMissionSchema.setProcessID("0000000003");
            tLWMissionSchema.setActivityStatus("1");
            tLWMissionSchema.setMissionProp1(mLCContSchema.getContNo());
            tLWMissionSchema.setMissionProp2(mLCContSchema.getPrtNo());
            tLWMissionSchema.setMissionProp3(mLCContSchema.getAgentCode());
            tLWMissionSchema.setMissionProp4(mLCContSchema.getAppntNo());
            tLWMissionSchema.setMissionProp5(mLCContSchema.getAppntName());
            tLWMissionSchema.setMissionProp6(mLCContSchema.getUWDate());
            tLWMissionSchema.setMissionProp7(mLCContSchema.getManageCom());
            tLWMissionSchema.setMissionProp8(hSSRS.GetText(1,1));//续保次数
            tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
            tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
            tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
            tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
            tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
            tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
            tMMap.put(tLWMissionSchema,"INSERT");
        }
        
        data.clear();
        data.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, mOperate))
        {
            mErrors.addOneError("续保数据转移失败");
            return false;
        }

        return true;
    }
    
    /**
     * 一、做过特权复效
     * 二、lccontstate表的enddate为空
     * 三、特权复效的结束时间在lccontstate表的makedate之后
     * @return
     */
    private boolean changeContStateDate(){
    	String edorAppSQL = "select a.* from lpedorapp a where exists (" +
    			" select 1 from lpedoritem b where a.edoracceptno=b.edoracceptno and contno='"+mLCContSchema.getContNo()+"' and b.edortype='TF') " +
    					" and edorstate='0'  order by confdate desc with ur ";
    	LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
    	LPEdorAppSet tLPEdorAppSet = tLPEdorAppDB.executeQuery(edorAppSQL);
    	LCContStateSet ttLCContStateSet = new LCContStateSet();
    	if(tLPEdorAppSet!=null&&tLPEdorAppSet.size()>0){
    		LPEdorAppSchema tLPEdorAppSchema = tLPEdorAppSet.get(1);
    		
    		//查询ljapayperson
    		String payPersonSQL = "select * from ljapayperson where getnoticeno='"+mGetNoticeNo+"' and PayType='ZC' with ur";
    		LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
    		LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.executeQuery(payPersonSQL);
    		
    		if(tLJAPayPersonSet!=null&&tLJAPayPersonSet.size()>0){
    			for(int i=1;i<=tLJAPayPersonSet.size();i++){
    				//查询lccontstate表信息
    				String stateSQL = "select * from lccontstate where polno='"+tLJAPayPersonSet.get(i).getPolNo()+"' and enddate is null" +
    						" and (makedate<'"+tLPEdorAppSchema.getConfDate()+"' or (makedate<'"+tLPEdorAppSchema.getConfDate()+"' and maketime <'"+tLPEdorAppSchema.getModifyTime()+"')) with ur ";
    				LCContStateDB tLCContStateDB = new LCContStateDB();
    				LCContStateSet tLCContStateSet = tLCContStateDB.executeQuery(stateSQL);
    				
    				if(tLCContStateSet!=null&&tLCContStateSet.size()>0){
    					//修改enddate为当前时间
    					for(int j =1 ;j<=tLCContStateSet.size();j++){
    						LCContStateSchema tLCContStateSchema = tLCContStateSet.get(j).getSchema();
    						tLCContStateSchema.setEndDate(PubFun.getCurrentDate());
    						tLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
    						tLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
    						ttLCContStateSet.add(tLCContStateSchema);
    					}
    				}
    			}
    		}
    		
    	}
    	
    	VData adata = new VData();
    	MMap amap = new MMap();
        if(ttLCContStateSet!=null&&ttLCContStateSet.size()>0){
        	String contSQL = "select stateflag  from lcpol where contno='"+mLCContSchema.getContNo()+"' and  stateflag!='1'";
        	String state = new ExeSQL().getOneValue(contSQL);
        	if(state==null||"".equals(state)){
        		String updateSQL=" update lccontstate set enddate='"+PubFun.getCurrentDate()+"',modifydate=current date,modifytime=current time where polno='000000' and contno='"+mLCContSchema.getContNo()+"' ";
        		amap.put(updateSQL, SysConst.UPDATE);
        	}
        	amap.put(ttLCContStateSet, SysConst.UPDATE);
        	adata.clear();
        	adata.add(amap);
        	PubSubmit tPubSubmit = new PubSubmit();
        	tPubSubmit.submitData(adata, "");
        }
        
    	return true;
    }


    /**
     * 校验是否需要续保确认和数据转移
     * 1、没有续保轨迹，不需要续保转移，false
     * 2、有续保轨迹但状态不是待确认、确认成功，转移成功，false
     * @return boolean
     */
    private boolean needPRnewOperate()
    {
        String sql = "  select * "
                     + "from LCRnewStateLog a "
                     + "where contNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and newContNo = "
                     + "      (select max(newContNo) "
                     + "      from LCRnewStateLog "
                     + "      where contNo = a.contNo "
                     + "         and state != '6') ";
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet tLCRnewStateLogSet
            = tLCRnewStateLogDB.executeQuery(sql);
        if(tLCRnewStateLogSet.size() == 0)
        {
            return false;
        }
        if(tLCRnewStateLogSet.get(1).getState()
           .compareTo(XBConst.RNEWSTATE_UNCONFIRM) < 0)
        {
            return false;
        }
        mRnewState = tLCRnewStateLogSet.get(1).getState();

        return true;
    }

    /**
     * 校验是否需要进行财务核销
     * 若有财务暂收，则需要进行财务核销
     * @return boolean：需要，true，否认false
     */
    private boolean needFinVerify()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
//        tLJSPayDB.setOtherNo(mLCContSchema.getContNo());
        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
        tLJSPayDB.setOtherNoType("2");

        if(tLJSPayDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 校验操作和数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("Beginning of PRnewDueVerifyBL.checkData");

        LCContDB db = new LCContDB();
        db.setContNo(mLCContSchema.getContNo());
        if(!db.getInfo())
        {
            mErrors.addOneError("没有查询到保单号为" + mLCContSchema.getContNo()
                + "的保单");
            return false;
        }
        mLCContSchema.setSchema(db);

        String sql = "select b.edortype "
                     + "from LPEdorApp a, LPEdorItem b "
                     + "where a.edorAcceptNo = b.edorNo "
                     + "   and b.contNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and a.edorState not in('0')";
        String temp = new ExeSQL().getOneValue(sql);
        if(!temp.equals(""))
        {
        	//若保全项目为理赔续保核保，则校验续保抽挡时间与续保核保时间的先后
        	//校验保全项目是否为理赔续保核保
        	if("XB".equals(temp)){
        		String uwSQL = "select * from LWMission where ProcessId = '0000000003'  and ActivityId = '0000001181' " +
        				" and Missionprop5 in (select caseno from llclaimpolicy where contno='"+mLCContSchema.getContNo()+"') with ur ";
        		LWMissionDB tLWMissionDB = new LWMissionDB();
        		LWMissionSet tLWMissionSet = new LWMissionSet();
        		tLWMissionSet = tLWMissionDB.executeQuery(uwSQL);
        		if(tLWMissionSet!=null&&tLWMissionSet.size()>0){
        			//续保核保时间
        			String uwDate = tLWMissionSet.get(1).getMakeDate();
        			//查询续保抽挡时间
        	        LJSPayDB tLJSPayDB = new LJSPayDB();
//        	        tLJSPayDB.setOtherNo(mLCContSchema.getContNo());
        	        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
        	        tLJSPayDB.setOtherNoType("2");

        	        if(tLJSPayDB.query().size() == 0)
        	        {
        	            return false;
        	        }
        	        
        	        LJSPaySet tLJSPaySet = new LJSPaySet();
        	        tLJSPaySet = tLJSPayDB.query();
        	        String xbDate = tLJSPaySet.get(1).getMakeDate();
        	        
        	        //判断续保核保时间与续保抽挡时间的先后
        	        if(CommonBL.stringToDate(xbDate).before(CommonBL.stringToDate(uwDate))){
        	        	System.out.println("续保抽挡在续保核保之前，续保通过");
        	        	return true;
        	        }else if(xbDate.equals(uwDate)){
        	        	if(tLWMissionSet.get(1).getMakeTime().compareTo(tLJSPaySet.get(1).getMakeTime())>0){
        	        	  	System.out.println("续保抽挡在续保核保之前，续保通过");
            	        	return true;
        	        	}
        	        }
        		}
        	} 
            mErrors.addOneError("保单正在做保全，不能核销" + mLCContSchema.getContNo());
            return false;
        }

        return true;
    }

    private boolean getInputData(VData data)
    {
        System.out.println("Beginning of PRnewDueVerifyBL.getInputData");

        mLCContSchema = (LCContSchema) data
                        .getObjectByObjectName("LCContSchema", 0);
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);
        mGetNoticeNo =(String)mTransferData.getValueByName("GetNoticeNo");
        System.out.println("我是超人："+mGetNoticeNo);

        if(mLCContSchema == null || mLCContSchema.getContNo() == null
           || mGlobalInput == null)
        {
            mErrors.addOneError("传入到续保续期财务核销的数据不完整");
            return false;
        }
        if(mGetNoticeNo == null ||mGetNoticeNo.equals("")||mGetNoticeNo.equals("null"))
        {
            mErrors.addOneError("传入到续保续期应收号不完整");
            return false;
        }
        return true;
    }

    public static void main(String[] a)
    {
        LCContSchema schema = new LCContSchema();
        schema.setContNo("00001619901");

        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.ManageCom = g.ComCode;
        g.Operator = "endor0";

        VData data = new VData();
        data.add(g);
        data.add(schema);

        PRnewDueVerifyBL bl = new PRnewDueVerifyBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("Verify successfully");
        }
    }
}
