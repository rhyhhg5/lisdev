package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import java.util.Date;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GRnewContAppBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;  //登陆的用户信息
    private MMap map = null;  //处理后的数据

    private LCGrpContSchema mLCGrpContSchema = null;  // 需处理团单信息
    private LCGrpPolSet mLCGrpPolSet = null;  //未处理的团单险种信息
    private LCGrpPolSet mNewLCGrpPolSet = null;  //处理过的团单险种信息
    private String newProGrpContNo = null;
    private String newGrpProposalNo = null;
    private double mPrem = 0;
    private double mSumPrem = 0;
    private double mAmnt = 0;

    private String mOperate = null;

    public GRnewContAppBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        if(getSubmitMap(inputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.addOneError("保存数据失败。");
            System.out.println(tPubSubmit.mErrors.getErrContent());
            return false;
        }

        return true;
    }

    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCGrpPolSet = (LCGrpPolSet) inputData
                       .getObjectByObjectName("LCGrpPolSet", 0);
        if(mGlobalInput == null
           || mLCGrpPolSet == null
           || mLCGrpPolSet.size() == 0)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        return true;
    }

    /**
     * 得到处理后的数据
     * @param inputData VData
     * @param operate String
     * @return MMap
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in GRnewContAppBL->getSubmitMap");
        mOperate = operate;
        if(!getInputData(inputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作的合法性和数据的完整性
     * @return boolean
     */
    private boolean checkData()
    {
        if(!checkLCGrpPolInfo())
        {
            return false;
        }

        if(!hasReNewPol())
        {
            return false;
        }

        //得到团单信息
        if(!checkLCGrpContInfo())
        {
            return false;
        }

        ///校验保全，校验理赔，校验续期

        return true;
    }

    private boolean hasReNewPol()
    {
        ExeSQL tExeSQL = new ExeSQL();
        for(int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
           String sql = "  select grpPolNo "
                        + "from LCRnewStateLog "
                        + "where grpContNo = '"
                        + mLCGrpPolSet.get(i).getGrpContNo()
                        + "'  and state != '" + XBConst.RNEWSTATE_DELIVERED
                        + "' ";
           String grpPolNo = tExeSQL.getOneValue(sql);
           if(!grpPolNo.equals("") && !grpPolNo.equals("null"))
           {
               mErrors.addOneError("险种" + mLCGrpPolSet.get(i).getRiskCode()
                   + "正在续保。");
               return false;
           }
        }

        return true;
    }

    /**
     * 处理续保信息
     * @return boolean
     */
    private boolean dealData()
    {
        map = new MMap();
        //处理个单续保
        if(!dealPRenewContApp())
        {
            return false;
        }
        System.out.println("\nend of dealPRenewContApp");
        //处理团单续保
        if(!dealGRenewContApp())
        {
            return false;
        }

        return true;
    }

    /**
     * 得到保单信息
     * @return LCGrpContSchema
     */
    private boolean checkLCGrpContInfo()
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpPolSet.get(1).getGrpContNo());
        tLCGrpContDB.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_SIGN);
        tLCGrpContDB.setStateFlag(BQ.STATE_FLAG_SIGN);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();

        if(tLCGrpContSet.size() == 0)
        {
            mErrors.addOneError("没有查询到保单号为"
                                + mLCGrpPolSet.get(1).getGrpContNo()
                                + "的有效团单");
            return false;
        }
        mLCGrpContSchema = tLCGrpContSet.get(1);

        return true;
    }

    /**
     * 得到险种的完整信息
     * @return LCGrpPolSet
     */
    private boolean checkLCGrpPolInfo()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        for(int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            tLCGrpPolDB.setGrpPolNo(mLCGrpPolSet.get(i).getGrpPolNo());
            if(!tLCGrpPolDB.getInfo())
            {
                mErrors.addOneError("没有查询到相关的险种信息。");
                return false;
            }
            mLCGrpPolSet.set(i, tLCGrpPolDB.getSchema());
        }

        return true;
    }

    /**
     * 处理团单下的个单续保
     * @return boolean
     */
    private boolean dealPRenewContApp()
    {
        //得到多有个单信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCContDB.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_SIGN);
        tLCContDB.setStateFlag(BQ.STATE_FLAG_SIGN);
        LCContSet tLCContSet = tLCContDB.query();
        if(tLCContSet.size() == 0)
        {
            mErrors.addOneError("没有查询到相关的个单信息。");
            return false;
        }

        String grpPolNos = "";
        for(int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            grpPolNos += "'" + mLCGrpPolSet.get(i).getGrpPolNo() + "',";
        }
        grpPolNos = grpPolNos.substring(0, grpPolNos.length() - 1);

        //对团单下的个单进行续保
        VData data = new VData();
        PRnewContAppBL tPRnewContAppBL = new PRnewContAppBL();
        for(int i = 1; i <= tLCContSet.size(); i++)
        {
            System.out.println("\n\n个单: " + i);
            StringBuffer sql = new StringBuffer();
            sql.append("select * ")
                .append("from LCPol ")
                .append("where contNo = '")
                .append(tLCContSet.get(i).getContNo())
                .append("'  and grpPolNo in (").append(grpPolNos).append(") ");
            System.out.println(sql.toString());
            LCPolDB tLCPolDB = new LCPolDB();
            LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql.toString());
            if(tLCPolSet.size() == 0)
            {
                continue;
            }

            data.clear();
            data.add(mGlobalInput);
            data.add(tLCPolSet);
            data.add(XBConst.IS_GRP);
            data.add(tLCContSet.get(i));  //Added by Yalin.

            //提交个单续保操作
            MMap tMMap = tPRnewContAppBL.getSubmitMap(data, mOperate);
            if(tMMap == null || tPRnewContAppBL.mErrors.needDealError())
            {
                System.out.println(i + ": " + tPRnewContAppBL.mErrors.getErrContent());
                mErrors.copyAllErrors(tPRnewContAppBL.mErrors);
                return false;
            }
            System.out.println(i + ": app success");
            map.add(tMMap);
        }

        return true;
    }

    /**
     * 处理团单续保
     * @return boolean
     */
    private boolean dealGRenewContApp()
    {
        this.newProGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo",
            mLCGrpContSchema.getAppntNo());

        if (!dealGRenewPolApp())
        {
            return false;
        }

        if(!dealLCGrpCont())
        {
            return false;
        }

        if(!changeNo())
        {
            return false;
        }

        return true;
    }

    private boolean changeNo()
    {
        String[] polTables = {"LCPol", "LCPrem", "LCGet"};
        String condition = "where polNo like '1%' "
                           + "  and grpContNo = '"
                           + mLCGrpContSchema.getGrpContNo() + "' ";
        String sql = "";
        for(int i = 0; i < polTables.length; i++)
        {
            sql = "  update " + polTables[i]
                         + " set GrpContNo = '" + this.newProGrpContNo + "' "
                         + condition;
            map.put(sql.toString(), "UPDATE");
        }

        String[] contTables = {"LCCont"};
        for(int i = 0; i < contTables.length; i++)
        {
            sql = "  update " + contTables[i]
                         + "   set grpContNo = '" + this.newProGrpContNo + "' "
                         + "where contNo like '1%' "
                         + "   and grpContNo = '"
                         + mLCGrpContSchema.getGrpContNo() + "' ";
            map.put(sql.toString(), "UPDATE");
        }

        sql = "  update LCRnewStateLog "
              + "set newGrpContNo = '" + newProGrpContNo + "' "
              + "where grpContNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
              + "   and state not in ('5', '6') ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 处理团单险种续保
     * @return boolean
     */
    private boolean dealGRenewPolApp()
    {
        //得到所有个险信息
        Object[] tLCPolInfo = map.getAllObjectByObjectName("LCPolSchema", 0);
        System.out.println("LCPolSchema.length: " + tLCPolInfo.length);
        mNewLCGrpPolSet = new LCGrpPolSet();
        for (int i = 1; i <= mLCGrpPolSet.size(); i++)
        {
            double prem = 0; //团险保费
            double sumPrem = 0; //团险总保费
            double amnt = 0; //团险总保额

            LCPolSchema tLCPolSchema = null;
            int polCountUnderGrpPol = 0;
            for (int t = 0; t < tLCPolInfo.length; t++)
            {
                LCPolSchema schema = (LCPolSchema) tLCPolInfo[t];
                //只累加相同团单号的个险费用
                if (schema == null || !schema.getGrpPolNo()
                    .equals(mLCGrpPolSet.get(i).getGrpPolNo()))
                {
                    continue;
                }
                tLCPolSchema = schema;
                prem += schema.getPrem();
                sumPrem += schema.getSumPrem();
                amnt += schema.getAmnt();
                polCountUnderGrpPol++;
            }

            if (tLCPolSchema == null)
            {
                mErrors.addOneError("没有的到团险下的个险续保信息。");
                System.out.println("没有的到团险下的个险续保信息");
                return false;
            }
            else
            {
                mPrem += prem;
                mSumPrem += sumPrem;
                mAmnt += amnt;
                //团险的险种相关日期从map中它的第一个个险取得
                LCGrpPolSchema tLCGrpPolSchema = mLCGrpPolSet.get(i).getSchema();

                this.newGrpProposalNo = PubFun1.CreateMaxNo("GrpProposalNo",
                    mLCGrpContSchema.getManageCom());

                tLCGrpPolSchema.setGrpContNo(this.newProGrpContNo);
                tLCGrpPolSchema.setGrpPolNo(this.newGrpProposalNo);
                tLCGrpPolSchema.setGrpProposalNo(tLCGrpPolSchema.getGrpPolNo());
                tLCGrpPolSchema.setPrem(prem);
                tLCGrpPolSchema.setSumPrem(sumPrem);
                tLCGrpPolSchema.setAmnt(amnt);
                tLCGrpPolSchema.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_INIT);
                tLCGrpPolSchema.setStateFlag(BQ.STATE_FLAG_APP);
                tLCGrpPolSchema.setPaytoDate(tLCPolSchema.getPaytoDate());
                tLCGrpPolSchema.setPayEndDate(tLCPolSchema.getPayEndDate());
                tLCGrpPolSchema.setCValiDate(tLCPolSchema.getCValiDate());
                System.out.println(tLCGrpPolSchema.getPayEndDate());
                System.out.println(tLCGrpPolSchema.getCValiDate());
                tLCGrpPolSchema.setApproveCode("");
                tLCGrpPolSchema.setApproveDate("");
                tLCGrpPolSchema.setApproveTime("");
                tLCGrpPolSchema.setApproveFlag("");
                tLCGrpPolSchema.setUWDate("");
                tLCGrpPolSchema.setUWTime("");
                tLCGrpPolSchema.setUWOperator("");
                tLCGrpPolSchema.setUWFlag("");
                PubFun.fillDefaultField(tLCGrpPolSchema);
                mNewLCGrpPolSet.add(tLCGrpPolSchema);

                String sql = "";
                String[] polTables = {"LCPol"};
                String condition = "where polNo like '1%' "
                                   + "  and grpPolNo = '"
                                   + mLCGrpPolSet.get(i).getGrpPolNo() + "' ";
                for (int j = 0; j < polTables.length; j++)
                {
                    sql = "  update " + polTables[j]
                          + " set grpPolNo = '" + this.newGrpProposalNo + "' "
                          + condition;
                    map.put(sql.toString(), "UPDATE");
                }

                sql = "  update LCRnewStateLog "
                      + "set newGrpPolNo = '" + newGrpProposalNo + "' "
                      + "where grpPolNo = '"
                      + mLCGrpPolSet.get(i).getGrpPolNo() + "' "
                      + "   and state not in ('5', '6') ";
                map.put(sql, "UPDATE");

            }
        }
        map.put(mNewLCGrpPolSet, "INSERT");

        return true;
    }

    /**
     * 处理LCGrpCont续保
     * @return boolean
     */
    private boolean dealLCGrpCont()
    {
        LCGrpContSchema tLCGrpContSchema = mLCGrpContSchema.getSchema();
        tLCGrpContSchema.setGrpContNo(this.newProGrpContNo);
        tLCGrpContSchema.setProposalGrpContNo(this.newProGrpContNo);
        tLCGrpContSchema.setPrem(mPrem);
        tLCGrpContSchema.setSumPrem(mSumPrem);
        tLCGrpContSchema.setAmnt(mAmnt);
        tLCGrpContSchema.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_INIT);
        tLCGrpContSchema.setStateFlag(BQ.STATE_FLAG_APP);
        tLCGrpContSchema.setCValiDate(getCValiDate());
        tLCGrpContSchema.setCInValiDate(getCInValiDate());
        tLCGrpContSchema.setHandlerDate(tLCGrpContSchema.getCValiDate());
        tLCGrpContSchema.setReceiveDate(tLCGrpContSchema.getCValiDate());
        tLCGrpContSchema.setSignDate("");
        tLCGrpContSchema.setSignTime("");
        tLCGrpContSchema.setApproveCode("");
        tLCGrpContSchema.setApproveDate("");
        tLCGrpContSchema.setApproveTime("");
        tLCGrpContSchema.setApproveFlag("");
        tLCGrpContSchema.setUWDate("");
        tLCGrpContSchema.setUWTime("");
        tLCGrpContSchema.setUWFlag("");
        tLCGrpContSchema.setUWOperator("");
        tLCGrpContSchema.setFirstTrialDate("");
        tLCGrpContSchema.setFirstTrialTime("");
        tLCGrpContSchema.setOperator(mGlobalInput.Operator);
        tLCGrpContSchema.setFirstTrialOperator("");
        PubFun.fillDefaultField(tLCGrpContSchema);
        tLCGrpContSchema.setPolApplyDate(tLCGrpContSchema.getMakeDate());
        map.put(tLCGrpContSchema, "INSERT");

        return true;
    }

    private String getCInValiDate()
    {
        if (mLCGrpContSchema.getCInValiDate() == null
            || mLCGrpContSchema.getCInValiDate().equals(""))
        {
            return "";
        }
        System.out.println(mLCGrpContSchema.getCValiDate() + ", "
                           + mLCGrpContSchema.getCInValiDate());
        int intval = PubFun.calInterval2(mLCGrpContSchema.getCValiDate(),
                                         mLCGrpContSchema.getCInValiDate(),
                                         "M");
        String sql = "  select date('" + mLCGrpContSchema.getCInValiDate()
                     + "') + " + intval + " month "
                     + "from dual ";
        ExeSQL e = new ExeSQL();
        String newDate = e.getOneValue(sql);
        if(newDate.equals("") || newDate.equals("null"))
        {
            mErrors.addOneError("计算保单截至日期有误。");
            return null;
        }
        System.out.println(newDate);
        return newDate;
    }

    /**
     * 得到生效日期
     * @return Date
     */
    private Date getCValiDate()
    {
        FDate date = new FDate();
       Date cValiDate = date.getDate(mLCGrpContSchema.getCValiDate());

       LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();  //原保单的所有险种
       tLCGrpPolDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
       LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

       //部分险种续保
       if(tLCGrpPolSet.size() > mNewLCGrpPolSet.size())
       {
           return cValiDate;
       }

       //得到最早的险种生效日期作为续保保单的生效日期
       cValiDate = date.getDate(mNewLCGrpPolSet.get(1).getCValiDate());
       for(int i = 2; i <= mNewLCGrpPolSet.size(); i++)
       {
           Date polCValiDate = date.getDate(
               mNewLCGrpPolSet.get(i).getCValiDate());
           if(polCValiDate.before(cValiDate))
           {
               cValiDate = polCValiDate;
           }
       }
       System.out.println(cValiDate);

       return cValiDate;
    }

    /**
     * 得到fieldName所表示的截止日期
     * @param fieldName String
     * @return Date
     */
    private Date getEndDate(String fieldName)
    {
        System.out.println(fieldName);
        FDate date = new FDate();
        Date reDate = date.getDate(mLCGrpContSchema.getV(fieldName));
        //得到最晚的险种失效日期作为续保保单的失效日期
        for(int i = 1; i <= mNewLCGrpPolSet.size(); i++)
        {
            Date polCValiDate = date.getDate(
                mNewLCGrpPolSet.get(i).getV(fieldName));
            if(polCValiDate.after(reDate))
            {
                reDate = polCValiDate;
            }
        }

        return reDate;
    }

    public static void main(String[] args)
    {
        LCGrpPolSet dealGrpPolSet = new LCGrpPolSet();

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo("0000000802");
        LCGrpPolSet set = tLCGrpPolDB.query();
        for(int i = 1; i <= set.size(); i++)
        {
            LCGrpPolSchema schema = new LCGrpPolSchema();
            schema.setGrpPolNo(set.get(i).getGrpPolNo());
            dealGrpPolSet.add(schema);
        }

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        VData data = new VData();
        data.add(dealGrpPolSet);
        data.add(g);

        long a = System.currentTimeMillis();
        GRnewContAppBL tGRnewContAppBL = new GRnewContAppBL();
        if(!tGRnewContAppBL.submitData(data, "MJ"))
        {
            System.out.println(tGRnewContAppBL.mErrors.getErrContent());
        }
        if(tGRnewContAppBL.mErrors.needDealError())
        {
            System.out.println(tGRnewContAppBL.mErrors.getErrContent());
        }

        System.out.println("生成续保数据花费："
                           + (System.currentTimeMillis() - a)/1000 + "秒");
    }
}
