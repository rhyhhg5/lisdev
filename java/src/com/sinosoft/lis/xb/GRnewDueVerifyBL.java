package com.sinosoft.lis.xb;

import com.sinosoft.task.Task;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.operfee.GRnewFinUrgeVerifyBL;
import com.sinosoft.lis.operfee.IndiFinUrgeVerifyBL;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理续期续保核销功能(包括财务核销和续保确认及续保数据转移)，步骤如下：
 * 1、财务核销：调用续期核销统一核销续保续期财务数据，若有续保数据则进行下面操作
 * 2、续保确认：调用个单续保确认进行处理
 * 3、续保数据转移：调用个单续保数据转移进行处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2018</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author lzj
 * @version 1.2
 */
public class GRnewDueVerifyBL
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;  //操作员信息
    private TransferData mTransferData = null;

    private LCGrpContSchema mLCGrpContSchema = null;  //需要续保续期财务核销的原保单信息，可以只录入保单号
    private String mOperate = null;
    private String mGetNoticeNo ;

    private String mRnewState = null;

    /**
     * 无参构造方法
     */
    public GRnewDueVerifyBL(){
    }

    /**
     * 续保续期核销功能的对外接口，实现相关的操作
     * @param inputData VData：包括
     * 1、LCContSchema：保单号
     * 2、GlobalInput：操作员信息
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        mOperate = operate;

        if(!getInputData(inputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 进行业务处理
     * 分别调用续期续保核销和续保确认及转移程序
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Beginning of GRnewDueVerifyBL.dealData");

        //续期续保核销
        if(mTransferData == null)
        {
            mTransferData = new TransferData();
        }
        mTransferData.setNameAndValue("NeedRnewVerify", "y");

        VData data = new VData();
        data.add(mLCGrpContSchema);
        data.add(mGlobalInput);
        data.add(mTransferData);

        boolean needFinVerify = needFinVerify();

        if(needFinVerify)
        {
        	GRnewFinUrgeVerifyBL tGRnewFinUrgeVerifyBL = new GRnewFinUrgeVerifyBL();
            if(!tGRnewFinUrgeVerifyBL.submitData(data, "VERIFY"))
            {
                mErrors.copyAllErrors(tGRnewFinUrgeVerifyBL.mErrors);
                return false;
            }
        }
        
        
        //续保确认和转移数据
        if(!needPRnewOperate())
        {
            return true;
        }

        //续保数据转移

        String edorNo = null;
        String sql = "select WorkNo from LGWork a, LJSPayB b "
                     + "where a.InnerSource = b.GetNoticeNo "
                     + "   and b.OtherNoType = '2' "
                     + "   and b.OtherNo='" + mLCGrpContSchema.getGrpContNo() + "' ";
        String workNo = new ExeSQL().getOneValue(sql);
        if(!workNo.equals("") && !workNo.equals("null"))
        {
            edorNo = workNo;
        }

        data.clear();
        data.add(mLCGrpContSchema);
        data.add(mGlobalInput);

        System.out.println("Beginning of GRnewDueVerifyBL.dealData: TransferCGDataToBG");
        TransferCGDataToBG tTransferCGDataToBG = new TransferCGDataToBG();
        MMap tMMap = tTransferCGDataToBG.getSubmitMap(data, mOperate);
        if(tMMap == null && tTransferCGDataToBG.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tTransferCGDataToBG.mErrors);
            return false;
        }
 
        
        data.clear();
        data.add(tMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, mOperate))
        {
            mErrors.addOneError("续保数据转移失败");
            return false;
        }

        return true;
    }
    



    /**
     * 校验是否需要续保确认和数据转移
     * 1、没有续保轨迹，不需要续保转移，false
     * 2、有续保轨迹但状态不是待确认、确认成功，转移成功，false
     * @return boolean
     */
    private boolean needPRnewOperate()
    {
        String sql = "  select * "
                     + "from LCRnewStateLog a "
                     + "where GrpContNo = '" + mLCGrpContSchema.getGrpContNo() + "' "
                     + "   and newGrpContNo = "
                     + "      (select max(newGrpContNo) "
                     + "      from LCRnewStateLog "
                     + "      where GrpContNo = a.GrpContNo "
                     + "         and state != '6') ";
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet tLCRnewStateLogSet
            = tLCRnewStateLogDB.executeQuery(sql);
        if(tLCRnewStateLogSet.size() == 0)
        {
            return false;
        }
        if(tLCRnewStateLogSet.get(1).getState()
           .compareTo(XBConst.RNEWSTATE_UNCONFIRM) < 0)
        {
            return false;
        }
        mRnewState = tLCRnewStateLogSet.get(1).getState();

        return true;
    }

    /**
     * 校验是否需要进行财务核销
     * 若有财务暂收，则需要进行财务核销
     * @return boolean：需要，true，否认false
     */
    private boolean needFinVerify()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
//        tLJSPayDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLJSPayDB.setGetNoticeNo(mGetNoticeNo);
        tLJSPayDB.setOtherNoType("1");

        if(tLJSPayDB.query().size() == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * 校验操作和数据的合法性
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("Beginning of GRnewDueVerifyBL.checkData");

        LCGrpContDB db = new LCGrpContDB();
        db.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if(!db.getInfo())
        {
            mErrors.addOneError("没有查询到保单号为" + mLCGrpContSchema.getGrpContNo()
                + "的保单");
            return false;
        }
        mLCGrpContSchema.setSchema(db);

        return true;
    }

    private boolean getInputData(VData data)
    {
        System.out.println("Beginning of GRnewDueVerifyBL.getInputData");

        mLCGrpContSchema = (LCGrpContSchema) data
                        .getObjectByObjectName("LCGrpContSchema", 0);
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);
        mGetNoticeNo =(String)mTransferData.getValueByName("GetNoticeNo");
        System.out.println("缴费通知书号码："+mGetNoticeNo);

        if(mLCGrpContSchema == null || mLCGrpContSchema.getGrpContNo() == null
           || mGlobalInput == null)
        {
            mErrors.addOneError("传入到续保续期财务核销的数据不完整");
            return false;
        }
        if(mGetNoticeNo == null ||mGetNoticeNo.equals("")||mGetNoticeNo.equals("null"))
        {
            mErrors.addOneError("传入到续保续期应收号不完整");
            return false;
        }
        return true;
    }

    public static void main(String[] a)
    {
        LCGrpContSchema schema = new LCGrpContSchema();
        schema.setGrpContNo("00113761000001");

        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.ManageCom = g.ComCode;
        g.Operator = "endor0";

        TransferData transferData = new TransferData();
        transferData.setNameAndValue("GetNoticeNo", "31000629469");
        VData data = new VData();
        data.add(g);
        data.add(schema);
        data.add(transferData);

        GRnewDueVerifyBL bl = new GRnewDueVerifyBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("Verify successfully");
        }
    }
}
