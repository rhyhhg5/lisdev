package com.sinosoft.lis.xb;

import java.util.Date;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 处理续保确认操作
 * 财务核销后调用本类进行续保数据的生效处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewContSignBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private MMap map = null;
    private LCPolSet mLCPolSet = null;  //传入的险种号是续保前的险种号
    private LCPolSet mNeedRnewLCPolSet = null;
    private TransferData mTransferData = null;
    private VData mVData = null;
    private PubSubmit mPubSubmit = null;

    private boolean isGrpCont = false;

    private String mProposalContNo = null;  //续保申请处理后的投保单号
    private String mNewContNo = null;  //生成的新号

    public PRnewContSignBL()
    {
    }

    /**
     * * 外部操作的提交方法
     * @param inputData VData：包括
     * 1、GlobalInput：操作员信
     * 2、LCPolSet：需要续保确认的险种信息
     * 3、TransferData：特殊数据机和：存储团险标志
     * @param operate String：操作标识，此为""
     * @return boolean: 操作成功true，否则false
     */
    public boolean submitData(VData inputData, String operate)
    {
        MMap tMMap = getSubmitData(inputData, operate);
        if(tMMap == null)
        {
            return false;
        }

        if(mVData == null)
        {
            mVData = new VData();
        }
        mVData.clear();
        mVData.add(tMMap);

//不能这样用
//        if(mPubSubmit == null)
//        {
            mPubSubmit = new PubSubmit();
//        }

        if (!mPubSubmit.submitData(mVData, ""))
        {
            mErrors.addOneError("处理个单"
                                + mLCPolSet.get(1).getContNo() + "续保确认时出错");
            return false;
        }

        return true;
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括
     * 1、GlobalInput：操作员信
     * 2、LCPolSet：需要续保确认的险种信息
     * 3、TransferData：特殊数据机和：存储团险标志
     * @param operate String：操作标识，此为""
     * @return MMap
     */
    public MMap getSubmitData(VData inputData, String operate)
    {
        System.out.println("Now in PRnewContSignBL->getSubmitMap");
        if(!getInputData(inputData))
        {
            return null;
        }

        mNeedRnewLCPolSet = new LCPolSet();
        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 进行续保确认处理
     * @return boolean
     */
    private boolean dealData()
    {
        map = new MMap();

        if(!dealAllPol())
        {
            return false;
        }

        if(!dealLCCont())
        {
            return false;
        }

        if(!changeNo())
        {
            return false;
        }

        //更新个单费用，必须在changeNo后执行
        if(!reNewFee())
        {
            return false;
        }

        if(!updateAppntNum())
        {
            return false;
        }

        return true;
    }

    /**
     * 若是个单续保，则需要将投保人保单数+1
     * @return boolean：成功true，否则false
     */
    private boolean updateAppntNum()
    {
        if(!isGrpCont)
        {
            StringBuffer sql = new StringBuffer();
            sql.append("update LDPerson ")
                .append("set appntNum = appntNum + 1 ")
                .append("where customerNo = ")  //LCPol中的appntNo可能为空
                .append("        (select appntNo from LCAppnt ")
                .append("         where contNo = '")
                .append(mLCPolSet.get(1).getContNo()).append("') ");
            map.put(sql.toString(), "UPDATE");
        }

        return true;
    }

    /**
     * 保单续保确认
     * @return boolean
     */
    private boolean dealLCCont()
    {
        //得到需要处理的个单信息
        LCContSchema tLCContSchema = getNeedDealContInfo();
        if(tLCContSchema == null)
        {
            return false;
        }
        map.put(tLCContSchema.getSchema(), "DELETE");

        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCPolSet.get(1).getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();

        //处理费用信息
        Object[] pols = map.getAllObjectByObjectName("LCPolSchema", 0);
        FDate date = new FDate();
        Date maxPtyToDate = date.getDate(tLCContSchema.getPaytoDate());
        Date minFirstPayToDate = date.getDate("3000-12-31");  //得到一个比较点
        Date maxEndDate = date.getDate(tLCContSchema.getCInValiDate());

        for(int i = 0; i < pols.length; i++)
        {
            LCPolSchema tLCPolSchema = ((LCPolSchema) pols[i]).getSchema();

            //只处理生成了新险种号的险种
            if(tLCPolSchema.getPolNo().equals(tLCPolSchema.getProposalNo()))
            {
                continue;
            }

            tLCContSchema.setPrem(tLCContSchema.getPrem()
                                  + tLCPolSchema.getPrem());
            tLCContSchema.setSumPrem(tLCContSchema.getSumPrem()
                                     + tLCPolSchema.getSumPrem());
            tLCContSchema.setAmnt(tLCContSchema.getAmnt()
                                  + tLCPolSchema.getAmnt());

            //保单交至日期为险种的最大交至日期
            if (date.getDate(tLCPolSchema.getPaytoDate()).after(maxPtyToDate))
            {
                tLCContSchema.setPaytoDate(tLCPolSchema.getPaytoDate());
                maxPtyToDate = date.getDate(tLCPolSchema.getPaytoDate());
            }
            //保单缴费日期为险种的最小缴费日期
            if (tLCPolSet.size() == mLCPolSet.size()
                && date.getDate(tLCPolSchema.getFirstPayDate())
                .after(minFirstPayToDate))
            {
                tLCContSchema.setFirstPayDate(tLCPolSchema.getFirstPayDate());
                minFirstPayToDate = date.getDate(tLCPolSchema.getFirstPayDate());
            }
            //保单终止日期为险种最大终止日期
            if(date.getDate(tLCPolSchema.getEndDate()).after(maxEndDate))
            {
                tLCContSchema.setCInValiDate(
                    date.getDate(tLCPolSchema.getEndDate()));
                maxEndDate = date.getDate(tLCPolSchema.getEndDate());
            }
        }
        //2011-07-25  修改自动签单程序只将操作用户修改为UWXB9999 杨天政
        // ------------------------------------------------
       
        String tUWOperator = null;
        String tapproveCode = null;
        //------------------------------------------------
        
        tUWOperator = getUWCODE(mLCPolSet.get(1).getContNo());
        tapproveCode = getAPPROVECODE(mLCPolSet.get(1).getContNo());
        
        tLCContSchema.setContNo(mNewContNo);
        tLCContSchema.setCValiDate(tLCContSchema.getCInValiDate());
        tLCContSchema.setSignDate(PubFun.getCurrentDate());
        tLCContSchema.setSignTime(PubFun.getCurrentTime());
        tLCContSchema.setUWDate(PubFun.getCurrentDate());
        tLCContSchema.setUWTime(PubFun.getCurrentTime());
        tLCContSchema.setUWOperator(tUWOperator);
        tLCContSchema.setUWFlag("9");
        tLCContSchema.setApproveDate(PubFun.getCurrentDate());
        tLCContSchema.setApproveTime(PubFun.getCurrentTime());
        tLCContSchema.setApproveCode(tapproveCode);
        tLCContSchema.setApproveFlag("9");
        tLCContSchema.setOperator(mGlobalInput.Operator);
        tLCContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLCContSchema, "INSERT");

        return true;
    }

    private LCContSchema getNeedDealContInfo()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mProposalContNo);
        if(!tLCContDB.getInfo())
        {
            mErrors.addOneError("没有查询到保单号为" + mProposalContNo +"的信息。");
            return null;
        }

        //生成新保单号
        if(tLCContDB.getContType().equals(XBConst.CONTTYPE_P))
        {
            mNewContNo = PubFun1.CreateMaxNo("CONTNO",
                                             tLCContDB.getAppntNo());
        }
        else if (tLCContDB.getContType().equals("2"))
        {
            mNewContNo = PubFun1.CreateMaxNo("GRPPERSONCONTNO", "");
        }
        System.out.println("newContNo: " + mNewContNo);

        return tLCContDB.getSchema();
    }

    private boolean checkData()
    {

        for(int i = 1; i <= mLCPolSet.size(); i++)
        {
            if(!isGrpCont)
            {
                //得到原险种信息
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(mLCPolSet.get(i).getPolNo());
                if (!tLCPolDB.getInfo()) {
                    mErrors.addOneError("查询险种信息出错。");
                    System.out.println("查询险种" + tLCPolDB.getPolNo() + "信息出错:");
                    return false;
                }
                mLCPolSet.set(i, tLCPolDB.getSchema());
            }
        }

        if (!checkRnew())
        {
            return false;
        }

        //校验保全，校验理赔，校验财务收费
        return true;
    }

    /**
     * 得到已续保申请的险种信息
     * @param schema LCRnewStateLogSchema
     * @return LCPolSchema
     */
    private boolean checkRnew()
    {
        String polNos = "";
        for(int i = 1; i <= mLCPolSet.size(); i++)
        {
            polNos += "'" + mLCPolSet.get(i).getPolNo() + "',";
        }
        polNos = polNos.substring(0, polNos.lastIndexOf(","));

        //查询险种续保信息
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCPol a, LCRnewStateLog b ")
            .append("where a.polNo = b.newPolNo ")
            .append("   and b.polNo in (").append(polNos).append(") ")
            .append("   and b.state = '")
            .append(XBConst.RNEWSTATE_UNCONFIRM).append("' ");
        System.out.println(sql.toString());
        LCPolDB db = new LCPolDB();
        LCPolSet set = db.executeQuery(sql.toString());
        if(set.size() == 0)
        {
            String err = "保单" + mLCPolSet.get(1).getContNo()
                                + "没有待续保确认的记录。";
            mErrors.addOneError(err);
            System.out.println(err + db.mErrors.getErrContent());
            return false;
        }
        mNeedRnewLCPolSet.add(set);
        mProposalContNo = set.get(1).getProposalContNo();

        return true;
    }

    /**
     * 得到传入的数据
     * @param inputData VData：包括
     * 1、GlobalInput：操作员信
     * 2、LCPolSet：需要续保确认的险种信息
     * 3、TransferData：特殊数据机和：存储团险标志
     * @return boolean: 获取数据成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCPolSet = (LCPolSet) inputData.getObjectByObjectName("LCPolSet", 0);
        mTransferData = (TransferData) inputData
                        .getObjectByObjectName("TransferData", 0);

        String strGrp = (String) mTransferData.getValueByName(XBConst.IS_GRP);
        isGrpCont = XBConst.IS_GRP.equals(strGrp);

        if(mGlobalInput == null || mLCPolSet == null || mLCPolSet.size() == 0
            || mTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        return true;
    }

    private boolean dealAllPol()
    {
        //对每个个险进行续保确认
        VData data = new VData();
        PRnewPolSignBL tPRnewPolSignBL = new PRnewPolSignBL();
        for(int i = 1; i <= mNeedRnewLCPolSet.size(); i++)
        {
            System.out.println("险种续保确认：" + i);
            data.clear();
            data.add(mGlobalInput);
            data.add(mNeedRnewLCPolSet.get(i));
            data.add(mTransferData);

            MMap tMMap = tPRnewPolSignBL.getSubmitData(data, "");
            if(map == null || tPRnewPolSignBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tPRnewPolSignBL.mErrors);
                return false;
            }
            map.add(tMMap);
        }

        return true;
    }

    private boolean changeNo()
    {
        String[] polTables =
            {"LCPol", "LCDuty", "LCPrem", "LCGet"};
        String condition = "where contNo= '" + mProposalContNo + "' ";

        String sql = "";
        for (int i = 0; i < polTables.length; i++)
        {
            sql = "  update " + polTables[i]
                         + " set contNo = '" + mNewContNo + "' "
                         + condition;
            map.put(sql.toString(), "UPDATE");
        }
        String[] accTables = {"LCInsureAcc",
                             "LCInsureAccClass", "LCInsureAccTrace"};
        for(int i = 0; i < accTables.length; i++)
        {
            sql = "update " + accTables[i]
                  + " set contNo = '" + mNewContNo + "' "
                  + "where polNo in "
                  + "    (select newPolNo "
                  + "    from LCRnewStateLog "
                  + "    where grpContNo = '"
                  + mLCPolSet.get(1).getGrpContNo() + "') "
                  + "   and contNo = '" + mProposalContNo + "' ";
            map.put(sql, "UPDATE");
        }

        sql = "  update LCRnewStateLog "
              + "set NewcontNo = '" + mNewContNo + "', "
              + "   state = '" + XBConst.RNEWSTATE_CONFIRM + "', "
              + "   operator = '" + mGlobalInput.Operator + "', "
              + "   modifyDate = '" + PubFun.getCurrentDate() + "', "
              + "   modifyTime = '" + PubFun.getCurrentTime() + "' "
              + "where newContNo = '" + mProposalContNo + "' ";
        map.put(sql, "UPDATE");

        if(!isGrpCont)
        {
            sql = "  update LJAPayPerson "
                  + " set contNo = '" + mNewContNo + "' "
                  + condition;
            map.put(sql.toString(), "UPDATE");
        }

        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setContNo(mProposalContNo);
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if(tLJAPayPersonSet.size() > 0 && mProposalContNo != null
            && !mProposalContNo.equals(""))
        {
            sql = "  update LJAPayPerson "
                  + " set contNo = '" + mNewContNo + "' "
                  + condition;
            map.put(sql.toString(), "UPDATE");

            sql = sql.replaceFirst("LJAPayPerson", "LJSPayPersonB");
            map.put(sql, "UPDATE");
        }

        return true;
    }

    /**
     * 更新保单费用信息
     * 若同一保单出现长期险续期短期险续保的情况，这里将不会起作用，所以在保单数据转移的时候需要更新保费
     * @return boolean
     */
    private boolean reNewFee()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("update LCCont ")
            .append("set prem = (select sum(prem) from LCPol where contNo = '")
            .append(this.mNewContNo).append("'), ")
            .append("sumPrem = (select sum(sumPrem) from LCPol where contNo = '")
            .append(this.mNewContNo).append("'), ")
            .append("amnt = (select sum(amnt) from LCPol where contNo = '")
            .append(this.mNewContNo).append("') ")
            .append("where contNo = '")
            .append(this.mNewContNo)
            .append("' ");
        map.put(sql.toString(), "UPDATE");

        return true;
    }

    /**
     * 设置公共提交类
     * @param tPubSubmit PubSubmit：公共提交方法
     */
    public void setPubSubmit(PubSubmit tPubSubmit)
    {
        mPubSubmit = tPubSubmit;
    }

    /**
     * 传入数据集合
     * @param data VData：数据集合，为团单调用个单预留接口
     */
    public void setVData(VData data)
    {
        mVData = data;
    }
    
    private String getUWCODE(String tcontno)
    {
    	String UWCODE=null;
    	
    	String First_UW_SQL = "select uwoperator from lbcont "
            + " where edorno like 'xb%' and prtno in " +
            		"(select prtno from lccont where contno= '" +  tcontno  + " ' ) "
            + " order by makedate asc fetch first 1 rows only " ;
        
    	UWCODE = new ExeSQL().getOneValue(First_UW_SQL);

    	if(UWCODE==null||UWCODE.equals(null)||UWCODE.equals(""))
    	{
    		First_UW_SQL = "select uwoperator from lccont where " 
    		 + " prtno in (select prtno from lccont where contno= '" +  tcontno  + " ' ) "
             + " order by makedate asc fetch first 1 rows only " ;
    		UWCODE = new ExeSQL().getOneValue(First_UW_SQL);
    	}
    	
    	return UWCODE;
    }
    
    private String getAPPROVECODE(String tcontno)
    {
    	String APPROVECODE = null;
    	
    	 String First_AP_SQL = "select approveCode from lbcont "
             + " where edorno like 'xb%' and  prtno in " +
             		"(select prtno from lccont where contno= '" +  tcontno  + " ' ) "
             + " order by makedate asc fetch first 1 rows only " ;
          
    	 APPROVECODE = new ExeSQL().getOneValue(First_AP_SQL);

    	 if(APPROVECODE==null||APPROVECODE.equals(null)||APPROVECODE.equals(""))
     	{
    		 First_AP_SQL = "select approveCode from lccont " 
    		 + " where  prtno in (select prtno from lccont where contno= '" +  tcontno  + " ' ) "
             + " order by makedate asc fetch first 1 rows only " ;
    		 APPROVECODE = new ExeSQL().getOneValue(First_AP_SQL);
     	}
    	
    	return APPROVECODE;
    }

    public static void main(String[] args)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo("00001151901");

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData td = new TransferData();
        td.setNameAndValue("state", "4");

        VData data = new VData();
        data.add(g);
        data.add(tLCPolDB.query());
        data.add(td);

        PRnewContSignBL bl = new PRnewContSignBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
