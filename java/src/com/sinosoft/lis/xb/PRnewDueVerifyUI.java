package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 财务实收核销用户接口类，接受从页面传入的数据，并传入到后台处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class PRnewDueVerifyUI
{
    public CErrors mErrors = new CErrors();

    public PRnewDueVerifyUI()
    {
    }

    /**
     * 外部接口方法，调用逻辑处理类PRnewDueVerifyUI进行业务处理
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        PRnewDueVerifyBL bl = new PRnewDueVerifyBL();
        if(!bl.submitData(data, operate))
        {
            mErrors = bl.mErrors;
            return false;
        }
        return true;
    }
}
