package com.sinosoft.lis.xb;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import java.util.Date;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.task.Task;
import com.sinosoft.task.CheckData;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 本类用来处理个单的续保申请，生成续保数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class PRnewContAppBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    /*需要续保的个单险种*/
    private GlobalInput mGlobalInput = null;
    private LCContSchema mLCContSchema = null; //需续保的保单
    private LCPolSet mLCPolSet = null;  //需续保的险种
    private LCPolSet mNewLCpolSet = null;
    private String newProposalContNo = null;
    private double mPrem = 0;
    private double mAmnt = 0;
    private double mSumPrem = 0;
    private String mGetNoticeNo = null;

    private boolean mNeedManuUW = false;
    private String mEdorNo = null;  //若需要核保，则生成保全项目信息
    private boolean mRnewCanSubmit = true;  //本类生成的续保数据是否可以提交

    private boolean mLPXBFlag = false;
    
    private boolean isGrpCont = false;
    private TransferData mTransferData = new TransferData();

    private String mOperate = null;

    private MMap map = null;
    
    private double mMult = 0.00;
    
    private boolean mMultFlag = false;
    
    private boolean mStopFlag=false;	//保证续保届满续保终止标记 
    private boolean mChangeFalg=false;	//保证续保届满需变更承保
    private LPUWMasterSchema ttLPUWMasterSchema = null;
    
    Object[] Pduty = null ;
    Object[] Pprem = null;
    Object[] Pget = null;



    public PRnewContAppBL()
    {
    }

    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in PRnewContAppBL->getSubmitMap");
        mOperate = operate;
        if(!getInputData(inputData))
        {
            return null;
        }

        if(!isGrpCont)
        {
            if (!checkData()) {
                return null;
            }
        }

        map = new MMap();

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 提交外部操作的方法
     *
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        if(getSubmitMap(inputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }

        return true;
    }

    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCPolSet = (LCPolSet)inputData.getObjectByObjectName("LCPolSet", 0);
        mLCContSchema = (LCContSchema) inputData.getObjectByObjectName("LCContSchema",0);
        String strGrpCont = (String) inputData.getObjectByObjectName("String",0);
        isGrpCont = XBConst.IS_GRP.equals(strGrpCont);
        mTransferData = (TransferData) inputData.getObjectByObjectName("TransferData",0);

        if(mGlobalInput == null || mLCPolSet == null || mLCPolSet.size() == 0)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        return true;
    }

    private LCContSchema getLCContInfo()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCPolSet.get(1).getContNo());
        if(!tLCContDB.getInfo())
        {
            return null;
        }
        tLCContDB.setModifyDate(PubFun.getCurrentDate());
        tLCContDB.setModifyTime(PubFun.getCurrentTime());
        tLCContDB.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_INIT);
        tLCContDB.setStateFlag(com.sinosoft.lis.bq.BQ.STATE_FLAG_APP);
        return tLCContDB.getSchema();
    }

    /**
     * 处理险种续保
     * @return boolean
     */
    private boolean renewAllPolApp()
    {
        mNewLCpolSet = new LCPolSet();
        VData tVData = new VData();
        for(int i = 1; i <= mLCPolSet.size(); i++)
        {
            System.out.println("\n\n个单下险种" + i);
            mLCPolSet.get(i).setContNo(mLCContSchema.getContNo());

            tVData.clear();
            tVData.add(mGlobalInput);
            tVData.add(mLCPolSet.get(i));
            if(mTransferData==null)
            {
                mTransferData = new TransferData();
            }
            mTransferData.setNameAndValue(XBConst.IS_GRP,XBConst.IS_GRP);
            

            LPPolSchema tLPPolUWed = rCalLPXBFee(mLCPolSet.get(i).getSchema());
            //2015-5 续保终止的险种不处理
            if(mStopFlag){
            	continue;
            }
            //变更承保的 需在下发核保通知书后才能续保，
			//但下发通知书的功能暂时没有开发，直接阻断。
            if(!mStopFlag && mChangeFalg){
            	mErrors.addOneError("保单"+mLCPolSet.get(i).getContNo()+"需要下发核保结论通知书，暂不能进行续保");
            	return false;
            }
            
	         if(tLPPolUWed != null){
	         	System.out.println("已经对核保数据进行处理，看看能不能出来，快出来，快成功");
//	        	 mTransferData.setNameAndValue("LPPolUWed",tLPPolSchema);
	        	 tVData.add(tLPPolUWed);
	        	 mLPXBFlag = true;
	         }
	        
	         System.out.println("已经对核保数据进行处理，看看能不能出来，快出来，快成功11111111111111111111111111111111111111111");	 
	        tVData.add(mTransferData);

            PRnewPolAppBL tPRnewPolAppBL = new PRnewPolAppBL();
            MMap tMMap = tPRnewPolAppBL.getSubmitMap(tVData, mOperate);

            if(tMMap == null || tPRnewPolAppBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tPRnewPolAppBL.mErrors);
                return false;
            }
            map.add(tMMap);

            LCPolSchema tLCPolSchema = (LCPolSchema) tMMap
                                       .getObjectByObjectName("LCPolSchema", 0);
            this.mPrem += tLCPolSchema.getPrem();
            this.mSumPrem += tLCPolSchema.getSumPrem();
            this.mAmnt += tLCPolSchema.getAmnt();
            mNewLCpolSet.add(tLCPolSchema);
            Pduty = tMMap.getAllObjectByObjectName("LCDutySchema", 0);
            Pprem = tMMap.getAllObjectByObjectName("LCPremSchema", 0);
            Pget = tMMap.getAllObjectByObjectName("LCGetSchema", 0);
            /**理赔续保核保数据处理   处理原则为将理赔续保核保时保存在P表的临时保存的
    		保费保额及免责信息存储到续保换号生成的新的13开头的C表数据中**/
	    	 boolean tLPFlag = dealLPXB(tLCPolSchema);
	         if(!tLPFlag){
	         	
	         	return false;
	         }
            
        }

        return true;
    }

    private boolean dealData()
    {
//        //续保前过滤掉续保终止的险种
//        if(!autoUWBeforeReCal())
//        {
//            return false;
//        }

        //处理险种续保
        if(!renewAllPolApp())
        {
            return false;
        }

        if(!createNewNo())
        {
            return false;
        }

        //处理保单数据
        LCContSchema tLCContSchema = preLCCont();
        map.put(tLCContSchema, "INSERT");

        //更换险种相关信息的号码
        if(!changeNo())
        {
            return false;
        }

        //若自动核保部通过，则提交人工核保，
        if(!autoUW())
        {
            //自核不通过autoUW返回false，程序其他校验不通过(!mRnewCanSubmit)也返回false
            //若不能提交，则放回false
            if(!mRnewCanSubmit)
            {
                return false;
            }

            updateState(XBConst.RNEWSTATE_UNUW);
            createWorkflow();
        }
        else
        {
            //qulq 061213 自核通过标记
            tLCContSchema.setUWFlag("9");
//            map.put(tLCContSchema,SysConst.UPDATE);
            updateState(XBConst.RNEWSTATE_UNHASTEN);
        }

        return true;
    }

    /**
     * 将续保终止的险种放入续保结论表
     * @param schema LCPolSchema
     * @param tLMUWSchema LMUWSchema
     * @param passFlag String
     */
    private boolean insertIntoLPUWMaster(int uwNo,
                                      LCPolSchema schema,
                                      LCPolSchema oldLCPolSchema,
                                      String passFlag)
    {
        //this.mEdorNo = com.sinosoft.task.CommonBL.createWorkNo();

        LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
        tLPUWMasterSchema.setEdorNo(this.getEdorNo());
        tLPUWMasterSchema.setEdorType(BQ.EDORTYPE_XB);
        tLPUWMasterSchema.setGrpContNo(schema.getGrpContNo());
        tLPUWMasterSchema.setContNo(this.newProposalContNo);
        tLPUWMasterSchema.setProposalContNo(newProposalContNo);
        tLPUWMasterSchema.setPolNo(schema.getPolNo());
        tLPUWMasterSchema.setProposalNo(schema.getProposalNo());
        tLPUWMasterSchema.setUWNo(uwNo);
        tLPUWMasterSchema.setInsuredNo(schema.getInsuredNo());
        tLPUWMasterSchema.setInsuredName(schema.getInsuredName());
        tLPUWMasterSchema.setAppntNo(schema.getAppntNo());
        tLPUWMasterSchema.setAutoUWFlag("1");
        tLPUWMasterSchema.setPassFlag(passFlag);
        tLPUWMasterSchema.setUWIdea("续保终止");
        tLPUWMasterSchema.setDisagreeDeal("");
        tLPUWMasterSchema.setAddPremFlag("");
        tLPUWMasterSchema.setSpecFlag("");
        tLPUWMasterSchema.setSubMultFlag("");
        tLPUWMasterSchema.setSubAmntFlag("");
        tLPUWMasterSchema.setMult("");
        tLPUWMasterSchema.setAmnt("");
        tLPUWMasterSchema.setOperator(mGlobalInput.Operator);
        tLPUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
        tLPUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
        tLPUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        tLPUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLPUWMasterSchema, "INSERT");

        //险种续保终止
        VData data = new VData();
        data.add(this.mGlobalInput);
        data.add(tLPUWMasterSchema);

        PRnewAppCancelPolBL tPRnewAppCancelPolBL = new PRnewAppCancelPolBL();  //撤销险种续保
        MMap tMMap = tPRnewAppCancelPolBL.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tPRnewAppCancelPolBL.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * 计算被保人续保时年龄
     * @param schema LCPolSchema: 险种信息
     * @return String: 年龄
     */
    private String getInsuredAge(LCPolSchema schema)
    {
        int age = PubFun.calInterval2(
        schema.getInsuredBirthday(), schema.getEndDate(), "Y");

        return String.valueOf(age);
    }

    /**
     * 将日志状态变更为state
     * @param state String
     */
    private void updateState(String state)
    {
        String sql = "  update LCRnewStateLog "
                     + "set state = '" + state + "' "
                     + "where contNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and state = '" + XBConst.RNEWSTATE_APP + "' ";
        map.put(sql, "UPDATE");
    }

    /**
     * 按保单进行自动核保，此处只处理需要人工核保的险种，续保终止的险种在保费重算前处理
     * 若需人工核保，则自动生成保全项目信息，和核保工作流
     * @return boolean
     */
    private boolean autoUW()
    {
        LCRnewStateLogSet tLCRnewStateLogSet = getLCRnewStateLog();
        LCPolSet needManuUWLCPolSet = new LCPolSet();  //要二核的险种
//        ArrayList needStopPolList = new ArrayList();  //要续保终止的险种
        String autoUWMsg ="";

        for(int count = 1; count <= mNewLCpolSet.size(); count++)
        {
            LCPolSchema schema = mNewLCpolSet.get(count);

            LCPolSchema oldLCPolSchema = getOldLCPol(tLCRnewStateLogSet, schema);
            if(oldLCPolSchema == null)
            {
                return false;
            }

            LMUWDB tLMUWDB = new LMUWDB();
            tLMUWDB.setRelaPolType(BQ.AUTOUWTYPE_IP);
            tLMUWDB.setRiskCode(schema.getRiskCode());
            tLMUWDB.setUWType(BQ.EDORTYPE_XB);
            LMUWSet tLMUWSet = tLMUWDB.query();
            //遍历自核规则

            for(int i = 1; i <= tLMUWSet.size(); i++)
            {
                LMUWSchema tLMUWSchema = tLMUWSet.get(i);

                Calculator cal = new Calculator();
                cal.setCalCode(tLMUWSchema.getCalCode());
                cal.addBasicFactor("EdorType", BQ.EDORTYPE_XB);
                cal.addBasicFactor("OldContNo", oldLCPolSchema.getContNo());
                cal.addBasicFactor("OldPolNo", oldLCPolSchema.getPolNo());
                cal.addBasicFactor(
                    "Age", String.valueOf(schema.getInsuredAppAge()));
                cal.addBasicFactor("RiskCode", schema.getRiskCode());
                cal.addBasicFactor("Amnt", String.valueOf(schema.getAmnt()));
                cal.addBasicFactor("Mult", String.valueOf(schema.getMult()));
                cal.addBasicFactor("EdorValiDate", schema.getCValiDate());
                String result = cal.calculate();

                //需要人工核保
                if(!result.equals("") && result.equals(XBConst.NEED_MANU_UW))
                {
                    System.out.println("人工核保：" + schema.getRiskCode()
                                       + tLMUWSchema.getRemark());
                    mNeedManuUW = true;
                    needManuUWLCPolSet.add(schema);

//                    if(mEdorNo == null)
//                    {
//                        mEdorNo = com.sinosoft.task.CommonBL.createWorkNo();
//                    }

                    setUWError(i, "险种" + tLMUWSchema.getRiskCode()
                               + tLMUWSchema.getRemark(), getEdorNo(), schema);
                    autoUWMsg += tLMUWSchema.getRemark() + "; ";
                }
                //续保终止
                else if(!result.equals("")
                        && result.equals(XBConst.XBSTOP))
                {
                    System.out.println("续保终止：" + schema.getRiskCode()
                                       + tLMUWSchema.getRemark());
                    //qulq 2007-2-9 add
                    String updatePol = "Update lcpol set polState ='"+BQ.POLSTATE_XBEND
      				+"' where polno ='"
      				+oldLCPolSchema.getPolNo()+"'";
                    map.put(updatePol,SysConst.UPDATE);
                    if(!insertIntoLPUWMaster(count, schema,
                                             oldLCPolSchema, XBConst.XBSTOP))
                    {
                        return false;
                    }

//                    if(!needStopPolList.contains(schema.getPolNo()))
//                    {
//                        needStopPolList.add(schema.getPolNo());
//                    }
                }
                else
                {
                    System.out.println("规则自核通过:"
                                       + tLMUWSchema.getRiskCode()
                                       + tLMUWSchema.getRemark());
                    LCPolSchema tempSchema = mNewLCpolSet.get(count);
                    tempSchema.setUWFlag("9");
                    mNewLCpolSet.set(count,tempSchema.getSchema());
                }
            }
            if(tLMUWSet.size()<1)
            {
                LCPolSchema tempSchema = mNewLCpolSet.get(count);
                tempSchema.setUWFlag("9");
                mNewLCpolSet.set(count, tempSchema.getSchema());
            }
        }

        System.out.println("mNeedManuUW=" + mNeedManuUW);
        //若需要二核
        if(mNeedManuUW)
        {
            //需要人工核保，生成工单及保全受理
            if(!createTask(autoUWMsg) || !createEdorItem()
               || !createXBDetail(needManuUWLCPolSet))
            {
                mRnewCanSubmit = false;
                return false;
            }
        }

        return !mNeedManuUW;
    }

    /**
     * 得到险种newSchema对应的原险种信息
     * @param newSchema LCPolSchema
     * @return LCPolSchema
     */
    private LCPolSchema getOldLCPol(LCRnewStateLogSet tLCRnewStateLogSet,
                                    LCPolSchema newSchema)
    {
        for(int i = 1; i <= tLCRnewStateLogSet.size(); i++)
        {
            if(tLCRnewStateLogSet.get(i).getNewPolNo()
               .equals(newSchema.getPolNo()))
            {
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tLCRnewStateLogSet.get(i).getPolNo());
                if(!tLCPolDB.getInfo())
                {
                    mErrors.addOneError(
                        "没有查询到险种" + newSchema.getRiskCode() + "的信息");
                    return null;
                }
                return tLCPolDB.getSchema();
            }
        }
        return null;
    }

    /**
     * 得到续保日志
     * @return LCRnewStateLogSet
     */
    private LCRnewStateLogSet getLCRnewStateLog()
    {
        LCRnewStateLogSet tLCRnewStateLogSet = new LCRnewStateLogSet();
        Object[] arr = map.getAllObjectByObjectName("LCRnewStateLogSchema", 0);
        if(arr != null)
        {
            for(int i = 0; i < arr.length; i++)
            {
                tLCRnewStateLogSet.add((LCRnewStateLogSchema) arr[i]);
            }
        }
        return tLCRnewStateLogSet;
    }

    /**
     * 若需要人工核保，则生成工单信息
     * @return boolean
     */
    private boolean createTask(String autoUWMsg)
    {
        System.out.println("\n\n生成工单");
//        if(mEdorNo == null)
//        {
//            mEdorNo = com.sinosoft.task.CommonBL.createWorkNo();
//        }

        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        mGetNoticeNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setWorkNo(getEdorNo());
        tLGWorkSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLGWorkSchema.setContNo(mLCContSchema.getContNo());
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_BATCH_AUTO);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DOING);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo(Task.WORK_TYPE_XQXBP);
        tLGWorkSchema.setRemark(autoUWMsg);
        tLGWorkSchema.setInnerSource(mGetNoticeNo);

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGlobalInput);
        tVData.add(getWorkBoxNo());

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("需要人工核保，但是生成工单信息失败:\n"
                + tTaskInputBL.mErrors.getFirstError());
            return false;
        }
        map.add(tMMap);

        this.mEdorNo = tTaskInputBL.getWorkNo();

        return true;
    }

    /**
     * 得到信箱号
     * @return String
     */
    private String getWorkBoxNo()
    {
        String manageCom8 = mLCContSchema.getManageCom();
        String manageCom4 = manageCom8.substring(0, 4);
        String manageCom2 = manageCom8.substring(0, 2);
        ExeSQL tExeSQL = new ExeSQL();

//        StringBuffer sql = new StringBuffer();
//        sql.append()
//            .append()
//            .append()
//            .append().append("' ")
//            .append(")
//            .append().append("' ");
        String sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom8 + "' "
                     + "   and goalType = '" + CheckData.XBP + "' "
                     + "   and defaultFlag = '1' ";
        String target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom4 + "' "
                     + "   and goalType = '" + CheckData.XBP + "' "
                     + "   and defaultFlag = '1' ";
        target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        sql = "select target "
                     + "from LGAutoDeliverRule "
                     + "where sourceComCode = '" + manageCom2 + "' "
                     + "   and goalType = '" + CheckData.XBP + "' "
                     + "   and defaultFlag = '1' ";
        target = tExeSQL.getOneValue(sql);
        if(!target.equals("") && !target.equals("null"))
        {
            return target;
        }

        return null;
    }

    /**
     * 为做人工核保生成XB项目
     * @return boolean
     */
    private boolean createEdorItem()
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setEdorAcceptNo(getEdorNo());
        tLPEdorItemSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        //tLPEdorItemSchema.setDisplayType("1");
        tLPEdorItemSchema.setEdorType(BQ.EDORTYPE_XB);
        tLPEdorItemSchema.setContNo(mLCContSchema.getContNo());
        tLPEdorItemSchema.setInsuredNo(BQ.FILLDATA);
        tLPEdorItemSchema.setPolNo(BQ.FILLDATA);
        tLPEdorItemSchema.setEdorValiDate(mNewLCpolSet.get(1).getCValiDate());
        tLPEdorItemSchema.setEdorAppDate(PubFun.getCurrentDate());

        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        tLPEdorItemSet.add(tLPEdorItemSchema);

        // 准备传输数据 VData
         VData tVData = new VData();
         tVData.add(tLPEdorItemSet);
         tVData.add(mGlobalInput);

         PEdorAppItemBL tPEdorAppItemBL = new PEdorAppItemBL();
         MMap tMMap = tPEdorAppItemBL.getSubmitData(tVData,"INSERT||EDORITEM");
        if(tMMap == null)
        {
            mErrors.addOneError("需要人工核保，但是生成续保项目信息失败:\n"
                + tPEdorAppItemBL.mErrors.getFirstError());
            return false;
        }
        map.add(tMMap);

        setUWState();

        return true;
    }

    /**
     * 设置核保状态
     */
    private void setUWState()
    {
        //更新App表
        String sql = "update LPEdorApp " +
                     "set EdorState = '" + BQ.EDORSTATE_SENDUW + "' " +
                     "where EdorAcceptNo = '" + this.getEdorNo() + "'";
        map.put(sql, "UPDATE");
        //更新Main表
        sql = "update LPEdorMain " +
              "set EdorState = '" + BQ.EDORSTATE_SENDUW + "' " +
              "where EdorNo = '" + this.getEdorNo() + "'";
        map.put(sql, "UPDATE");
    }


    private boolean createXBDetail(LCPolSet needManuUWLCPolSet)
    {
        Reflections ref = new Reflections();
        for(int i = 1; i <= needManuUWLCPolSet.size(); i++)
        {
            LPPolSchema tLPPolSchema = new LPPolSchema();
            ref.transFields(tLPPolSchema, needManuUWLCPolSet.get(i));
            tLPPolSchema.setEdorNo(this.getEdorNo());
            tLPPolSchema.setEdorType(BQ.EDORTYPE_XB);
            tLPPolSchema.setContNo(newProposalContNo);
            tLPPolSchema.setProposalContNo(newProposalContNo);
            map.put(tLPPolSchema, "INSERT");
        }

        return true;
    }

    /**
     * 创建工作流，向工作流表中插数据，送人工核保
     */
    private void createWorkflow()
    {
        String missionId = PubFun1.CreateMaxNo("MissionId", 20);
        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        tLWMissionSchema.setMissionID(missionId);
        tLWMissionSchema.setSubMissionID("1");
        tLWMissionSchema.setProcessID("0000000003");
        tLWMissionSchema.setActivityID("0000001180");
        tLWMissionSchema.setActivityStatus("1");
        tLWMissionSchema.setMissionProp1(this.getEdorNo());
        tLWMissionSchema.setMissionProp2(mLCContSchema.getContNo());
        tLWMissionSchema.setMissionProp3(mLCContSchema.getAppntNo());
        tLWMissionSchema.setMissionProp4(
                CommonBL.getAppntName(mLCContSchema.getAppntNo()));
        tLWMissionSchema.setMissionProp5(
                CommonBL.getUserName(mGlobalInput.Operator));
        tLWMissionSchema.setMissionProp6(PubFun.getCurrentDate());
        tLWMissionSchema.setMissionProp7(
                CommonBL.getUserName(mGlobalInput.Operator));
        tLWMissionSchema.setMissionProp8(PubFun.getCurrentDate());
        tLWMissionSchema.setMissionProp9(PubFun.getCurrentTime());
        //QULQ ADD MANAGECOM 2007-2-3
        tLWMissionSchema.setMissionProp10(mLCContSchema.getManageCom());
        tLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
        tLWMissionSchema.setLastOperator(mGlobalInput.Operator);
        tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
        tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
        tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
        tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(tLWMissionSchema, "DELETE&INSERT");
    }

    /**
     * 记录自核错误信息
     * @param error String
     * @param aLPPolSchema LPPolSchema
     */
    private void setUWError(int uwNo, String error, String edorNo, LCPolSchema schema)
    {
        LPUWErrorSchema tLPUWErrorSchema = new LPUWErrorSchema();
        tLPUWErrorSchema.setEdorNo(edorNo);
        tLPUWErrorSchema.setEdorType(BQ.EDORTYPE_XB);
        tLPUWErrorSchema.setGrpContNo(schema.getGrpContNo());
        tLPUWErrorSchema.setContNo(schema.getContNo());
        tLPUWErrorSchema.setProposalContNo(schema.getProposalNo());
        tLPUWErrorSchema.setPolNo(schema.getPolNo());
        tLPUWErrorSchema.setProposalNo(schema.getPolNo());
        tLPUWErrorSchema.setUWNo(uwNo);
        tLPUWErrorSchema.setSerialNo("1"); //对自核来说SerialNo没有意义
        tLPUWErrorSchema.setInsuredNo(schema.getInsuredNo());
        tLPUWErrorSchema.setInsuredName(schema.getInsuredName());
        tLPUWErrorSchema.setAppntNo(schema.getAppntNo());
        tLPUWErrorSchema.setAppntName(schema.getAppntName());
        tLPUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
        tLPUWErrorSchema.setUWError(error);
        tLPUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLPUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLPUWErrorSchema.setUWPassFlag(BQ.UWFLAG_FAIL); //核保未通过
        map.put(tLPUWErrorSchema, "DELETE&INSERT");

        mErrors.addOneError(error);
    }


    private boolean changeNo()
    {
    		// modified by qulq 2008-3-5 13:52
        String[] polTables =
        {"LCDuty", "LCPrem", "LCGet","LCPol"};
        String condition = " where polNo like '1%' "
                           + "  and contNo= '" + mLCContSchema.getContNo()
                           + "' and appflag ='0' and stateflag ='0' "
                           ;
        String sql = "";
        for (int i = 0; i < polTables.length; i++)
        {
            sql = "  update " + polTables[i]
                  + " set contNo = '" + newProposalContNo + "' "
                  + " where polno in (select polno from lcpol "
                  + condition
                  + " ) "
                  ;
            map.put(sql.toString(), "UPDATE");
        }
        
        sql = "  update LCPol "
              + " set ProposalContNo = '" + newProposalContNo + "' "
              + "where contNo = '" + newProposalContNo + "' ";  //上面修改了险种的保单号
        map.put(sql, "UPDATE");
        //特需续保的状态一直就是4，所以这里要更新状态为4的记录。把它换成新号。
        sql = "  update LCRnewStateLog "
              + " set NewcontNo = '" + newProposalContNo + "' "
              + "where contNo = '" + mLCContSchema.getContNo() + "' "
              + "   and state = '"
              ;
        if(mOperate.equals(com.sinosoft.lis.bq.BQ.EDORTYPE_MJ))
        {
            sql += XBConst.RNEWSTATE_UNCONFIRM + "' ";
        }
        else
        {
            sql += XBConst.RNEWSTATE_APP + "' ";

        }
        map.put(sql, "UPDATE");
        if(mLPXBFlag){
        	sumMoney(newProposalContNo);
        }

        return true;
    }
    
	private LPPolSchema rCalLPXBFee(LCPolSchema tLCPolSchema) {
		mStopFlag=false;
		mChangeFalg=false;
		ExeSQL tExeSQL = new ExeSQL();
		/*
		 * add by lzy 2015-5-26
		 *  #2395添加对保证续保产品的处理
		 */
		String guarantee="select codealias,othersign from ldcode where codetype='bzXB' and code='"+tLCPolSchema.getRiskCode()+"' ";
		SSRS tSSRS=tExeSQL.execSQL(guarantee);
		String bzXB = null;//保证年期
		String once="Y";//Y--只保证一期，以后的每年还需看核保结论   N--每X年为一个保证期
		if(null!=tSSRS && tSSRS.getMaxRow()>0){
			bzXB=tSSRS.GetText(1, 1);
			once=tSSRS.GetText(1, 2);
		}
		if(null!=bzXB && !"".equals(bzXB)){
			boolean needUW=false;
			System.out.println("--++++++进入保证续保处理");
			//获取续保保单的承保年度：用保单首期的生效日和当前保单的日期计算
			String strSQ="select a.cvalidate from lbpol a,lcrnewstatelog b "
							+ " where a.contno=b.newcontno and a.polno=b.newpolno "
							+ " and b.polno='"+tLCPolSchema.getPolNo()+"' and b.state='6' "
							+ " order by b.paytodate,b.renewcount fetch first 1 row only"
							+" with ur";
			String Scvalidate=tExeSQL.getOneValue(strSQ);
			if(null==Scvalidate || "".equals(Scvalidate)){
				System.out.println("&&&&***没有找到保单续保数据，取当前生效日期");
				Scvalidate=tLCPolSchema.getCValiDate();
			}
			//当前保单年度
			int years = PubFun.calInterval(Scvalidate, tLCPolSchema.getEndDate(), "Y");
			//保证续保年度
			int bzYears=Integer.parseInt(bzXB);
			int count=years / bzYears ;	//处于第几次保证续保期间 0、1、2
			if("N".equals(once) && (years % bzYears==0)){
				System.out.println("&&&&&&*****保证续保险种"+tLCPolSchema.getPolNo()+"需要处理理赔二核数据，当前年度："+years);
				needUW=true;
			}else if("Y".equals(once) && (years>=bzYears)){//少儿险只有两次保证续保权
				System.out.println("&&&&&&*****保证续保险种"+tLCPolSchema.getPolNo()+"需要处理理赔二核数据，当前年度："+years);
				needUW=true;
			}else{
				//因保单还在保证续保期间,所以把保单置为正常续保状态
				String updatePolState="update LCPol set polState = '00019999' "
	                + " where polNo = '" + tLCPolSchema.getPolNo() + "' and polstate='"+XBConst.POLSTATE_LPXBSTOP+"' ";
				map.put(updatePolState,  "UPDATE");
				System.out.println("&&&&&&*****险种"+tLCPolSchema.getPolNo()+"在保证续保期间，当前年度："+years);
				return null;
			}
				/*
				 * 查询当前保证续保期间内所有的理赔二核记录
				 * 若在本次期间内有续保终止结论，则续保终止，若只有变更承保结论则以最后一次的核保结论为准
				 * 若没有理赔二核记录则正常续保
				 */
				
				String startDate=PubFun.calDate(Scvalidate,count-1, "Y", null);//当前保证续保期间的起始日期
				//少儿险保证续保两次后再续保时都需要处核保记录
				if("Y".equals(once) && years > bzYears){
					startDate=PubFun.calDate(startDate,years-1, "Y", null);
					startDate=PubFun.calDate(startDate,1, "D", null);//加上一天才是下个期间的起始日期
				}
				if(count>1 && "N".equals(once)){
					startDate=PubFun.calDate(startDate,1, "D", null);//加上一天才是下个期间的起始日期
				}
	//			String uwSQL="select * from LPUWMaster a where  contno='"+ tLCPolSchema.getContNo()
	//				+ "' and edortype='LB' and polno = '"+tLCPolSchema.getPolNo()+"'"
	//				+ " and makedate>='"+startDate+"' ";
				//以理赔的出险日期来分
				String uwSQL="select distinct lpu.* from LBMission lbm,LPUWMASTER lpu,llcaserela llc,LLSubReport lls"
							+ " where lbm.activityid='0000001181' "
							+ " and lpu.edorno=lbm.missionprop12 "
							+ " and llc.caseno=lbm.missionprop5 "
							+ " and lls.subrptno=llc.subrptno "
							+ " and lpu.edortype='LB' "
							+ " and lpu.polno='"+tLCPolSchema.getPolNo()+"' "
							+ " and lls.accdate>='"+startDate+"'"
							+ " and lls.accdate<='"+tLCPolSchema.getEndDate()+"'"
							+ " order by lpu.makedate desc with ur";
				
				LPUWMasterDB mLPUWMasterDB = new LPUWMasterDB();
				System.out.println(uwSQL);
				LPUWMasterSet mLPUWMasterSet=mLPUWMasterDB.executeQuery(uwSQL);
				if(mLPUWMasterSet.size()==0){
					System.out.println("######在"+startDate+"之后没有理赔核保记录,正常续保");
					return null;
				}
				ttLPUWMasterSchema=mLPUWMasterSet.get(1);//取最后一次核保结论
				for(int i=1; i<=mLPUWMasterSet.size();i++){
					LPUWMasterSchema mLPUWMasterSchema=mLPUWMasterSet.get(i);
					String passFlag=mLPUWMasterSchema.getPassFlag();
					if(null!=passFlag && BQ.PASSFLAG_STOPXB.equals(passFlag)){
						System.out.println("###########保证续保期间内有终止续保结论");
						mStopFlag=true;
					}
					else if(null!=passFlag && BQ.PASSFLAG_CONDITION.equals(passFlag)){
						System.out.println("#########保证续保期间内有变更结论");
						mChangeFalg=true;
					}
				}
				//终止续保
				if(mStopFlag){
					map.put("update LCPol set polState = '" + XBConst.POLSTATE_LPXBSTOP + "' "
			                + "where polNo = '" + tLCPolSchema.getPolNo() + "' ",  "UPDATE");
					return null;
				}else if(!mStopFlag && mChangeFalg){
					
					/*
					 * 变更承保的处理
					 * 这里需要在理赔核保下发通知书的功能开发之后
					 * 加上下发通知书的处理
					*/
					
					LPUWMasterSchema mLPUWMasterSchema=mLPUWMasterSet.get(1);
						String tLPPol = "select * from lppol a where  contno='"
							+ tLCPolSchema.getContNo()
							+ "' and edortype='LB' and edorno='"
							+ mLPUWMasterSet.get(1).getEdorNo() + "' and polno "
							+ " = '" + tLCPolSchema.getPolNo() + "'";
					System.out.println(tLPPol);
					LPPolDB tLPPolDB = new LPPolDB();
					LPPolSet tLPPolSet = tLPPolDB.executeQuery(tLPPol);
					if(tLPPolSet.size()>0){
						return tLPPolSet.get(1).getSchema();
					}else{
						//modify date 2015-5-11 若理赔二核结论是免责或加费时 没有核保险种数据
						if(	(null!=mLPUWMasterSchema.getSpecFlag() && "1".equals(mLPUWMasterSchema.getSpecFlag())
								|| (null!=mLPUWMasterSchema.getAddPremFlag() && "1".equals(mLPUWMasterSchema.getAddPremFlag())) )
								&& (null==mLPUWMasterSchema.getSubAmntFlag() || "".equals(mLPUWMasterSchema.getAddPremFlag())))
						{
							return null;
						}else{
							mErrors.addOneError("查询保单核保险种信息失败！");
							return null;
						}
					}
				}
			
		}
		//-----保证续保处理到此为止，下面原程序不动
		
		LPPolSchema tLPPolSchema = new LPPolSchema();
		String tRiskSql = "select 1 from ldcode where code = '"
				+ tLCPolSchema.getRiskCode() + "' and codetype='lxb' ";
		System.out.println(tRiskSql);
		String tRisk = tExeSQL.getOneValue(tRiskSql);
		if (tRisk.equals("") || tRisk.equals("null")) {
			return null;
		}
		
		/**
		 * 判断是否做过理赔续保核保，同时未进行过续保操作。LPUWMaster的state为0时说明核保成功，
		 * 为1是历史核保数据，为2是续保核销成功
		 */
		String tLBFlag = "select * from LPUWMaster a where  contno='"
				+ tLCPolSchema.getContNo()
				+ "' and edortype='LB' and state='0' and polno "
				+ " = '"+tLCPolSchema.getPolNo()+"'";
		System.out.println(tLBFlag);
		LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
		System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&11111111111111");
		LPUWMasterSet tLPUWMasterSet = tLPUWMasterDB.executeQuery(tLBFlag);
		if (tLPUWMasterSet.size() > 0) {
			System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&22222222222222222222222222");
			LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet
			.get(1).getSchema();
			System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&33333333333333333333333333333");
			if (tLPUWMasterSchema.getPassFlag().equals("4")) {
				System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&4444444444444444444444444444444");
				
				String tLPPol = "select * from lppol a where  contno='"
						+ tLCPolSchema.getContNo()
						+ "' and edortype='LB' and edorno='"
						+ tLPUWMasterSet.get(1).getEdorNo() + "' and polno "
						+ " = '" + tLCPolSchema.getPolNo() + "'";
				System.out.println(tLPPol);
				LPPolDB tLPPolDB = new LPPolDB();
				LPPolSet tLPPolSet = tLPPolDB.executeQuery(tLPPol);
				if(tLPPolSet.size()>0){
					tLPPolSchema = tLPPolSet.get(1).getSchema();
				}else{
					//modify date 2015-5-11 若理赔二核结论是免责或加费时 没有核保险种数据
					if(	(null!=tLPUWMasterSchema.getSpecFlag() && "1".equals(tLPUWMasterSchema.getSpecFlag())
							|| (null!=tLPUWMasterSchema.getAddPremFlag() && "1".equals(tLPUWMasterSchema.getAddPremFlag())) )
							&& (null==tLPUWMasterSchema.getSubAmntFlag() || "".equals(tLPUWMasterSchema.getAddPremFlag())))
					{
						return null;
					}else{
						mErrors.addOneError("查询保单核保险种信息失败！");
						return null;
					}
				}
			}
			else 
				return null;
			

		}
		

		return tLPPolSchema;
	}
    
	private boolean dealLPXB(LCPolSchema tLCPolSchema) {
				LPUWMasterSet tLPUWMasterSet=null;
				ExeSQL tExeSQL = new ExeSQL();
				/*
				 * modify by lzy 2015-5-26
				 *  #2395添加对保证续保产品的处理
				 */
				String guarantee="select codealias from ldcode where codetype='bzXB' and code='"+tLCPolSchema.getRiskCode()+"' ";
				String bzXB = tExeSQL.getOneValue(guarantee);
				if(null!=bzXB && !"".equals(bzXB)){
					//如果是保证续保险种没有变更承保的结论的话就直接返回
					if(!mChangeFalg){
						return true;
					}
					//保证续保产品这里得到的就肯定是变更承保的结论
					tLPUWMasterSet.add(this.ttLPUWMasterSchema.getSchema());
						
				}else{
					//首先判断是否是理赔续保核保险种
					String tRiskSql = "select 1 from ldcode where code = '"
							+ tLCPolSchema.getRiskCode() + "' and codetype='lxb' ";
					System.out.println(tRiskSql);
					
					String tRisk = tExeSQL.getOneValue(tRiskSql);
					if (tRisk.equals("") || tRisk.equals("null")) {
						return true;
					}
					/**判断是否做过理赔续保核保，同时未进行过续保操作。LPUWMaster的state为0时说明核保成功，
					 * 为1是历史核保数据，为2是续保核销成功
					 */
					String tLBFlag = "select * from LPUWMaster a where  contno='"
							+ tLCPolSchema.getContNo()
							+ "' and edortype='LB' and state='0' and polno " +
									" in (select polno from lcpol where contno='"+tLCPolSchema.getContNo()+"' " +
											" and riskcode = '"+tLCPolSchema.getRiskCode()+"') ";
					System.out.println(tLBFlag);
					LPUWMasterDB tLPUWMasterDB = new LPUWMasterDB();
					tLPUWMasterSet = tLPUWMasterDB
							.executeQuery(tLBFlag);
				}
				
				if (tLPUWMasterSet.size() > 0) {

					for (int t = 1; t <= tLPUWMasterSet.size(); t++) {
						LPUWMasterSchema tLPUWMasterSchema = tLPUWMasterSet
								.get(t).getSchema();
						//处理理赔核保结论为降档或者降额的数据
						if (tLPUWMasterSchema.getSubAmntFlag() != null
								&& tLPUWMasterSchema.getSubAmntFlag().equals(
										"1")
								|| tLPUWMasterSchema.getSubMultFlag() != null
								&& tLPUWMasterSchema.getSubMultFlag().equals(
										"1")) {
							if (!dealSubAmntAndMult(tLCPolSchema,
									tLPUWMasterSchema)) {
								return false;
							}
						}

						// 处理理赔核保结论为加费的数据
						if (tLPUWMasterSchema.getAddPremFlag() != null
								&& tLPUWMasterSchema.getAddPremFlag().equals(
										"1")) {

							if (!dealAddFee(tLCPolSchema, tLPUWMasterSchema)) {
								return false;
							}

						}

						// 处理理赔核保结论为免责的数据 现在是直接在此将核保结论的数据保存到C表，不需要核销的时候再换号了
						if (tLPUWMasterSchema.getSpecFlag() != null
								&& tLPUWMasterSchema.getSpecFlag().equals(
										"1")) {
							if (!dealSpec(tLCPolSchema, tLPUWMasterSchema)) {
								return false;
							}
						}

					}
				}
			

		return true;

	}
    
    /**
     * 处理降额和降档
     * 用核保后的险种责任信息进行保费重算
     * @param tLPPolSchema LPPolSchema
     * @return boolean
     */
    private boolean dealSubAmntAndMult(LCPolSchema tLCPolSchema,LPUWMasterSchema tLPUWMasterSchema)
    {
        LPPolSchema tLPPolSchema = getLPPol(tLPUWMasterSchema);  //核保结论信息
        LPPremSet tLPPremSet = getLPPrem(tLPUWMasterSchema);//核保结论信息
        LPDutySet tLPDutySet = getLPDuty(tLPUWMasterSchema);//核保结论信息
        LPGetSet tLPGetSet = getLPGet(tLPUWMasterSchema);//核保结论信息
        if(tLPPolSchema == null)
        {
            return false;
        }
        if(tLPPremSet == null)
        {
            return false;
        }
        if(tLPDutySet == null)
        {
            return false;
        }
        if(tLPGetSet == null)
        {
            return false;
        }

       

        VData data = new VData();
        data.add(tLCPolSchema);
        data.add(tLPPolSchema);
        data.add(tLPPremSet);
        data.add(tLPDutySet);
        data.add(tLPGetSet);
        data.add(tLPUWMasterSchema);
        

        //将P表里的核保结论信息保存到换号后的C表数据中
        if(!changeData(data, tLPUWMasterSchema))
        {
            return false;
        }

        return true;
    }
    
    private boolean dealAddFee(LCPolSchema tLCPolSchema,LPUWMasterSchema tLPUWMasterSchema)
    {
       
        LPPremSet tLPPremSet = getAddLPPrem(tLPUWMasterSchema);
//        LCPremSet tLCPremSet = getLCPrem(tLCPolSchema);
        
        Reflections ref = new Reflections();
        if(tLPPremSet == null)
        {
            return false;
        }
        
        	for(int t = 1; t <= tLPPremSet.size(); t++){
        		        		
        		
        		
        		LPPremSchema tLPPremSchema = tLPPremSet.get(t).getSchema();
        		for(int i = 0 ; i < Pprem.length ; i++){
        			LCPremSchema tLCPremSchema = new LCPremSchema();
        			LCPremSchema tnLCPremSchema = new LCPremSchema();
        			tLCPremSchema = (LCPremSchema)Pprem[i];
        			if(tLCPremSchema.getPolNo().equals(tLCPolSchema.getPolNo())
        					&&tLCPremSchema.getDutyCode().equals(tLPPremSchema.getDutyCode())){
        				
        				ref.transFields(tnLCPremSchema, tLCPremSchema);    			
            			
            			tnLCPremSchema.setRate(tLPPremSchema.getRate());
            			tnLCPremSchema.setPrem(tLPPremSchema.getPrem());
            			tnLCPremSchema.setStandPrem(tLPPremSchema.getStandPrem());
            			tnLCPremSchema.setPayPlanCode(tLPPremSchema.getPayPlanCode());
            			tnLCPremSchema.setPayPlanType(tLPPremSchema.getPayPlanType());
                	       
            			this.mPrem += tLPPremSchema.getPrem();
                        this.mSumPrem += tLPPremSchema.getPrem();
            			//tLCPolSchema.setPrem(tLCPolSchema.getPrem()+tLPPremSchema.getPrem());
            			//tLCPolSchema.setSumPrem(tLCPolSchema.getSumPrem()+tLPPremSchema.getPrem());
            			
                	    map.put(tnLCPremSchema, "DELETE&INSERT");
                	    
                	    //map.put(tLCPolSchema, "UPDATE");
        				
        			}
        			
        		}
        		
        			
        		
        	}
        	// sumMoney(tLCPolSchema.getContNo());  
        
        

        return true;
    }
    
    
    private boolean dealSpec(LCPolSchema tLCPolSchema,LPUWMasterSchema tLPUWMasterSchema)
    {
        LPSpecDB db = new LPSpecDB();
        db.setPolNo(tLPUWMasterSchema.getPolNo());
        db.setEdorNo(tLPUWMasterSchema.getEdorNo());
        db.setEdorType(tLPUWMasterSchema.getEdorType());
        LPSpecSet tLPSpecSet = db.query();
        Reflections ref = new Reflections();
        LCSpecSet tLCSpecSet = new LCSpecSet();

        for(int i = 1; i <= tLPSpecSet.size(); i++)
        {
            LCSpecSchema tLCSpecSchema = new LCSpecSchema();
            ref.transFields(tLCSpecSchema, tLPSpecSet.get(i));
            tLCSpecSchema.setOperator(mGlobalInput.Operator);
            tLCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCSpecSchema.setModifyTime(PubFun.getCurrentTime());

            tLCSpecSet.add(tLCSpecSchema);
        }
        map.put(tLCSpecSet, "DELETE&INSERT");
       
       

        return true;
    }
   
    
    private void sumMoney(String newContNo)
    {
        //责任
    	String sql = "update LCDuty a "
            + "set (prem, sumPrem) = "
            + "   (select sum(prem), sum(sumPrem) from LCPrem "
            + "    where polNo = a.polNo "
            + "       and dutyCode = a.dutyCode) "
            + "where contno = '" + newContNo + "' ";
		map.put(sql, SysConst.UPDATE);

        //险种
        sql = "update LCPol a "
                     + "set (prem, sumPrem) = "
                     + "   (select sum(prem), sum(sumPrem) from LCDuty "
                     + "    where polNo = a.polNo) "
                     + "where contno = '" + newContNo + "' ";
        map.put(sql, SysConst.UPDATE);

        //保单
//        sql = "update LCCont a "
//                     + "set (prem, sumPrem) = "
//                     + "   (select sum(prem), sum(sumPrem) from LCPol "
//                     + "    where contNo = a.contNo) "
//                     + "where contNo = '" + newContNo + "' ";
//        map.put(sql, SysConst.UPDATE);

    }
    
    
    private LPPolSchema getLPPol(LPUWMasterSchema tLPUWMasterSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LPPol a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLPUWMasterSchema.getPolNo())
            //.append("'  and b.state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("' and edortype='LB' and edorno = '")
            .append(tLPUWMasterSchema.getEdorNo())
            .append("' ");
        System.out.println(sql.toString());

        LPPolSet tLPPolSet = new LPPolDB().executeQuery(sql.toString());
        if(tLPPolSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLPUWMasterSchema.getPolNo()
                                + "对应的理赔核保险种信息。");
            return null;
        }

        LPPolSchema tLPPolSchema = tLPPolSet.get(1);

       


        return tLPPolSchema;
    }
    
    private LPPremSet getLPPrem(LPUWMasterSchema tLPUWMasterSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LPPrem a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLPUWMasterSchema.getPolNo())
            //.append("'  and b.state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("' and edortype='LB' and edorno = '")
            .append(tLPUWMasterSchema.getEdorNo())
            .append("' ");
        System.out.println(sql.toString());

        LPPremSet tLPPremSet = new LPPremDB().executeQuery(sql.toString());
        if(tLPPremSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLPUWMasterSchema.getPolNo()
                                + "对应的理赔核保险种保费信息。");
            return null;
        }

       // LPPremSchema tLPPremSchema = tLPPremSet.get(1);

       


        return tLPPremSet;
    }
    
    private LPPremSet getAddLPPrem(LPUWMasterSchema tLPUWMasterSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LPPrem a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLPUWMasterSchema.getPolNo())
            //.append("'  and b.state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("' and edortype='LB' and payplancode='00000001' and edorno = '")
            .append(tLPUWMasterSchema.getEdorNo())
            .append("' ");
        System.out.println(sql.toString());

        LPPremSet tLPPremSet = new LPPremDB().executeQuery(sql.toString());
        if(tLPPremSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLPUWMasterSchema.getPolNo()
                                + "对应的理赔核保险种保费信息。");
            return null;
        }

       // LPPremSchema tLPPremSchema = tLPPremSet.get(1);

       


        return tLPPremSet;
    }
    
    
    private LPDutySet getLPDuty(LPUWMasterSchema tLPUWMasterSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LPDuty a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLPUWMasterSchema.getPolNo())
            //.append("'  and b.state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("' and edortype='LB' and edorno = '")
            .append(tLPUWMasterSchema.getEdorNo())
            .append("' ");
        System.out.println(sql.toString());

        LPDutySet tLPDutySet = new LPDutyDB().executeQuery(sql.toString());
        if(tLPDutySet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLPUWMasterSchema.getPolNo()
                                + "对应的理赔核保险种责任信息。");
            return null;
        }

       // LPDutySchema tLPDutySchema = tLPDutySet.get(1);

       


        return tLPDutySet;
    }
    
    private LPGetSet getLPGet(LPUWMasterSchema tLPUWMasterSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LPGet a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLPUWMasterSchema.getPolNo())
            //.append("'  and b.state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("' and edortype='LB' and edorno = '")
            .append(tLPUWMasterSchema.getEdorNo())
            .append("' ");
        System.out.println(sql.toString());

        LPGetSet tLPGetSet = new LPGetDB().executeQuery(sql.toString());
        if(tLPGetSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLPUWMasterSchema.getPolNo()
                                + "对应的理赔核保险种给付信息。");
            return null;
        }

        //LPGetSchema tLPGetSchema = tLPGetSet.get(1);

       


        return tLPGetSet;
    }
    
    
    private LCPremSet getLCPrem(LCPolSchema tLCPolSchemaa)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCPrem a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLCPolSchemaa.getPolNo())
            .append("' ");
        System.out.println(sql.toString());

        LCPremSet tLCPremSet = new LCPremDB().executeQuery(sql.toString());
        if(tLCPremSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLCPolSchemaa.getPolNo()
                                + "对应的理赔核保险种保费信息。");
            return null;
        }

       // LPPremSchema tLPPremSchema = tLPPremSet.get(1);

       


        return tLCPremSet;
    }
    
    private LCDutySet getLCDuty(LCPolSchema tLCPolSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCDuty a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLCPolSchema.getPolNo())
            .append("' ");
        System.out.println(sql.toString());

        LCDutySet tLCDutySet = new LCDutyDB().executeQuery(sql.toString());
        if(tLCDutySet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLCPolSchema.getPolNo()
                                + "对应的理赔核保险种责任信息。");
            return null;
        }

       // LPDutySchema tLPDutySchema = tLPDutySet.get(1);

       


        return tLCDutySet;
    }
    
    private LCGetSet getLCGet(LCPolSchema tLCPolSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCGet a ")
            .append(" where a.polNo =  '")
            //.append("   and b.newPolNo = '")
            .append(tLCPolSchema.getPolNo())
            .append("' ");
        System.out.println(sql.toString());

        LCGetSet tLCGetSet = new LCGetDB().executeQuery(sql.toString());
        if(tLCGetSet.size() == 0)
        {
            mErrors.addOneError("没有查询到核保险种"
                                + tLCPolSchema.getPolNo()
                                + "对应的核保险种给付信息。");
            return null;
        }

        //LPGetSchema tLPGetSchema = tLPGetSet.get(1);

       


        return tLCGetSet;
    }
    
    
    
    /**
     * 将重算得到的险种信息相关号码变更为核保险种的号码
     * LCPolSchema：polNo、proposalNo
     * @param tMMap MMap
     * @return boolean
     */
    private boolean changeData(VData inputData, LPUWMasterSchema tLPUWMasterSchema)
    {
    	LPPolSchema tLPPolSchema =  (LPPolSchema) inputData
        .getObjectByObjectName("LPPolSchema", 0);
        LPPremSet tLPPremSet= (LPPremSet) inputData
        .getObjectByObjectName("LPPremSet", 0);
        LPDutySet tLPDutySet= (LPDutySet) inputData
        .getObjectByObjectName("LPDutySet", 0);
        LPGetSet tLPGetSet= (LPGetSet) inputData
        .getObjectByObjectName("LPGetSet", 0);
        if(tLPPolSchema == null
           || tLPPremSet == null || tLPPremSet.size() == 0
           || tLPDutySet == null || tLPDutySet.size() == 0
           || tLPGetSet == null || tLPGetSet.size() == 0)
        {
            mErrors.addOneError("核保结束后重算保费出错");
            return false;
        }

        LCPolSchema tLCPolSchema = (LCPolSchema) inputData
        .getObjectByObjectName("LCPolSchema", 0);
//        LCPremSet tLCPremSet = getLCPrem(tLCPolSchema);
//        LCDutySet tLCDutySet = getLCDuty(tLCPolSchema);
//        LCGetSet tLCGetSet = getLCGet(tLCPolSchema);

        tLCPolSchema.setAmnt(tLPPolSchema.getAmnt());
        //tLCPolSchema.setPrem(tLPPolSchema.getPrem());
        //tLCPolSchema.setStandPrem(tLPPolSchema.getStandPrem());
        if(tLPUWMasterSchema.getSubMultFlag() != null
        		&& tLPUWMasterSchema.getSubMultFlag().equals("1")){
        	
        	tLCPolSchema.setMult(tLPPolSchema.getMult());
        	mMultFlag = true;
        	mMult = tLPPolSchema.getMult();
        	
        }
        map.put(tLCPolSchema, "UPDATE");

        for(int i = 0; i < Pduty.length; i++)
        {
        	LCDutySchema tLCDutySchema =(LCDutySchema)Pduty[i];
        	for(int t = 1; t <= tLPDutySet.size(); t++){
        		
        		LPDutySchema tLPDutySchema = tLPDutySet.get(t).getSchema();
        		
        		if(tLCDutySchema.getPolNo().equals(tLCPolSchema.getPolNo())&&tLCDutySchema.getDutyCode().equals(tLPDutySchema.getDutyCode())){
        			
        		
        			tLCDutySchema.setAmnt(tLPDutySchema.getAmnt());
        			//tLCDutySchema.setPrem(tLPDutySchema.getPrem());
        			//tLCDutySchema.setStandPrem(tLPDutySchema.getStandPrem());
        	        if(tLPUWMasterSchema.getSubMultFlag() != null
        	        		&& tLPUWMasterSchema.getSubMultFlag().equals("1")){
        	        	
        	        	tLCDutySchema.setMult(tLPDutySchema.getMult());
        	        	
        	        }
        	        map.put(tLCDutySchema, "UPDATE");
        			
        		}
        	}
            
        }

        for(int i = 0; i < Pget.length; i++)
        {
        	LCGetSchema tLCGetSchema = (LCGetSchema)Pget[i];
        	for(int t = 1; t <= tLPGetSet.size(); t++){
        		LPGetSchema tLPGetSchema = tLPGetSet.get(t).getSchema();
        		
        		if(tLCGetSchema.getPolNo().equals(tLCPolSchema.getPolNo())&&tLCGetSchema.getDutyCode().equals(tLPGetSchema.getDutyCode())&&
        				tLCGetSchema.getGetDutyCode().equals(tLPGetSchema.getGetDutyCode())){
        			
        			tLCGetSchema.setActuGet(tLPGetSchema.getActuGet());
        			tLCGetSchema.setStandMoney(tLPGetSchema.getStandMoney());
        			 
        	        map.put(tLCGetSchema, "UPDATE");
        			
        		}
        	}
            
        }

        for(int i = 0; i < Pprem.length; i++)
        {
        	LCPremSchema tLCPremSchema = (LCPremSchema)Pprem[i];
        	for(int t = 1; t <= tLPPremSet.size(); t++){
        		
        		LPPremSchema tLPPremSchema = tLPPremSet.get(t).getSchema();
        		if(tLCPremSchema.getPolNo().equals(tLCPolSchema.getPolNo())&&tLCPremSchema.getDutyCode().equals(tLPPremSchema.getDutyCode())&&
        				tLCPremSchema.getPayPlanType().equals(tLPPremSchema.getPayPlanType())&&
        				tLCPremSchema.getPayPlanCode().equals(tLPPremSchema.getPayPlanCode())){
        			
        			
        			
        			//tLCPremSchema.setRate(tLPPremSchema.getRate());
        			//tLCPremSchema.setPrem(tLPPremSchema.getPrem());
        			//tLCPremSchema.setStandPrem(tLPPremSchema.getStandPrem());
        	        
        	        //map.put(tLCPremSchema, "UPDATE");
        			
        		}
        	}
            
        }        

        return true;
    }
    /**
     * 生成新号
     * @return boolean
     */
    private boolean createNewNo()
    {
        newProposalContNo = PubFun1.CreateMaxNo("ProposalContNo", null);

        return true;
    }

    /**
     * 对需要续保的险种进行续保操作合法性的校验
     * @return boolean
     */
    private boolean checkData()
    {
        //校验个单险种的数据完整性
        LCPolDB tLCPolDB = new LCPolDB();
        for(int i = 1; i <= mLCPolSet.size(); i++)
        {
            tLCPolDB.setPolNo(mLCPolSet.get(i).getPolNo());
            if(!tLCPolDB.getInfo())
            {
                mErrors.addOneError("没有查询到相关的个单险种信息");
                return false;
            }
            mLCPolSet.set(i, tLCPolDB.getSchema());
        }

        mLCContSchema = getLCContInfo();

        //校验保全，校验理赔，校验续期

        return true;
    }

    private LCContSchema preLCCont()
    {
        LCContSchema tLCContSchema = mLCContSchema.getSchema();
        tLCContSchema.setContNo(newProposalContNo);
        tLCContSchema.setProposalContNo(newProposalContNo);
        tLCContSchema.setPrem(mPrem);
        tLCContSchema.setSumPrem(mSumPrem);
        tLCContSchema.setAmnt(mAmnt);
        tLCContSchema.setCValiDate(getCValiDate());
        tLCContSchema.setCInValiDate(getCInValiDate());
        tLCContSchema.setPaytoDate(getEndDate("PaytoDate"));
        tLCContSchema.setSignDate("");
        tLCContSchema.setSignTime("");
        tLCContSchema.setApproveCode("");
        tLCContSchema.setApproveDate("");
        tLCContSchema.setApproveTime("");
        tLCContSchema.setApproveFlag("");
        tLCContSchema.setUWOperator("");
        tLCContSchema.setUWDate("");
        tLCContSchema.setUWTime("");
        tLCContSchema.setUWFlag("");
        tLCContSchema.setPolApplyDate(PubFun.getCurrentDate());
        tLCContSchema.setFirstTrialDate("");
        tLCContSchema.setFirstTrialTime("");
        tLCContSchema.setFirstTrialOperator("");
        tLCContSchema.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_INIT);
        tLCContSchema.setStateFlag(com.sinosoft.lis.bq.BQ.STATE_FLAG_APP);
        tLCContSchema.setOperator(mGlobalInput.Operator);
        if(mMultFlag){
        	tLCContSchema.setMult(mMult);
        }
        PubFun.fillDefaultField(tLCContSchema);

        return tLCContSchema;
    }

    //得到最晚的险种失效日期作为续保保单的失效日期
    private Date getCInValiDate()
    {
        FDate date = new FDate();
        Date reDate = date.getDate(mLCContSchema.getCInValiDate());
        if(reDate == null || reDate.equals(""))
        {
            String sql = "  select max(endDate) "
                         + "from LCPol "
                         + "where contNo = '" + mLCContSchema.getContNo() + "' ";
            ExeSQL e = new ExeSQL();
            String d = e.getOneValue(sql);
            reDate = date.getDate(d);
        }

        for(int i = 1; i <= mNewLCpolSet.size(); i++)
        {
            Date polCValiDate = date.getDate(mNewLCpolSet.get(i).getEndDate());
            if(polCValiDate.after(reDate))
            {
                reDate = polCValiDate;
            }
        }

        return reDate;

    }

    /**
     * 得到保单生效日期
     * @return Date
     */
    private Date getCValiDate()
    {
        FDate date = new FDate();
        Date cValiDate = date.getDate(mLCContSchema.getCValiDate());

        LCPolDB tLCPolDB = new LCPolDB();  //原保单的所有险种
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();

        //部分险种续保
        if(tLCPolSet.size() > this.mNewLCpolSet.size())
        {
            return cValiDate;
        }

        //得到最早的险种生效日期作为续保保单的生效日期
        cValiDate = date.getDate(mNewLCpolSet.get(1).getCValiDate());
        for(int i = 2; i <= mNewLCpolSet.size(); i++)
        {
            Date polCValiDate = date.getDate(mNewLCpolSet.get(i).getCValiDate());
            if(polCValiDate.before(cValiDate))
            {
                cValiDate = polCValiDate;
            }
        }

        return cValiDate;
    }

    /**
     * 得到保单的失效日期
     * @return Date
     */
    private Date getEndDate(String fieldName)
    {
        FDate date = new FDate();
        Date reDate = date.getDate(mLCContSchema.getV(fieldName));
        if(reDate == null)
        {
            reDate = date.getDate("1900-12-31");
        }
        //得到最晚的险种失效日期作为续保保单的失效日期
        for(int i = 1; i <= mNewLCpolSet.size(); i++)
        {
            Date polCValiDate = date.getDate(mNewLCpolSet.get(i).getV(fieldName));
            if(polCValiDate.after(reDate))
            {
                reDate = polCValiDate;
            }
        }

        return reDate;
    }

    /**
     * 得到新险种的投保单号
     * @return String
     */
    public String getNewProposalContNo()
    {
        return newProposalContNo;
    }

    /**
     * 得到续保后保单的期交保费
     * @return double
     */
    public double getPrem()
    {
        return mPrem;
    }

    /**
     * 得到续保后保单的总保费
     * @return double
     */
    public double getSumPrem()
    {
        return mSumPrem;
    }

    /**
     * 得到续保后保单的保额
     * @return double
     */
    public double getAmnt()
    {
        return mAmnt;
    }

    /**
     * 得到是否需要人工核保的标志
     * @return boolean
     */
    public boolean needManuUW()
    {
        return mNeedManuUW;
    }
    /**
     * 为核保，取得保全受理号
     * 单一模式既可减少代码，又可避免错误。
     * 如果受理号不存在就生成
     * qulq add 2007-2-3
     */
    private String getEdorNo()
    {
        if(mEdorNo == null)
        {
            mEdorNo = com.sinosoft.task.CommonBL.createWorkNo();
        }
        return mEdorNo;
    }
    public static void main(String[] args)
    {
        LCPolSchema schema = new LCPolSchema();
        //schema.setContNo("00000936401");
        schema.setPolNo("21000013866");
        LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSet.add(schema);

        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.Operator = "endor0";

        VData data = new VData();
        data.add(tLCPolSet);
        data.add(g);

        PRnewContAppBL bl = new PRnewContAppBL();
        if(bl.getSubmitMap(data, "") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
