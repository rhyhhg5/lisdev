package com.sinosoft.lis.xb;

import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;


/**
 * <p>Title: Web业务系统 </p>
 * <p>Description:保全删除数据库操作类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author lh
 * @version 1.0
 */

public class PRnewAppCancelBLS
{
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    // 数据操作字符串
    public String mOperate;

    public PRnewAppCancelBLS()
    {}

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;
        if (mOperate.equals("DELETE||Rnew"))
        {
            System.out.println("edordeldata");
            if (!this.delEdor())
            {
                return false;
            }
        }
        return true;
    }


    public boolean delEdor()
    {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB;
        tLCPolSet = (LCPolSet) mInputData.getObjectByObjectName("LCPolSet", 0);

        LCPremSchema tLCPremSchema = new LCPremSchema();
        LCPremSet tLCPremSet = new LCPremSet();
        LCPremDB tLCPremDB;
        tLCPremSet = (LCPremSet) mInputData.getObjectByObjectName("LCPremSet",
                0);

        LCDutySchema tLCDutySchema = new LCDutySchema();
        LCDutySet tLCDutySet = new LCDutySet();
        LCDutyDB tLCDutyDB;
        tLCDutySet = (LCDutySet) mInputData.getObjectByObjectName("LCDutySet",
                0);

        LCGetSchema tLCGetSchema = new LCGetSchema();
        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB;
        tLCGetSet = (LCGetSet) mInputData.getObjectByObjectName("LCGetSet", 0);

        LCCustomerImpartSchema tLCCustomerImpartSchema = new
                LCCustomerImpartSchema();
        LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
        LCCustomerImpartDB tLCCustomerImpartDB;
        tLCCustomerImpartSet = (LCCustomerImpartSet) mInputData.
                               getObjectByObjectName("LCCustomerImpartSet", 0);

        LCAppntIndSchema tLCAppntIndSchema = new LCAppntIndSchema();
        LCAppntIndSet tLCAppntIndSet = new LCAppntIndSet();
        LCAppntIndDB tLCAppntIndDB;
        tLCAppntIndSet = (LCAppntIndSet) mInputData.getObjectByObjectName(
                "LCAppntIndSet", 0);

        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        LCInsuredDB tLCInsuredDB;
        tLCInsuredSet = (LCInsuredSet) mInputData.getObjectByObjectName(
                "LCInsuredSet", 0);

        LCBnfSchema tLCBnfSchema = new LCBnfSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LCBnfDB tLCBnfDB;
        tLCBnfSet = (LCBnfSet) mInputData.getObjectByObjectName("LCBnfSet", 0);

        LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        tLCInsureAccSet = (LCInsureAccSet) mInputData.getObjectByObjectName(
                "LCInsureAccSet", 0);

        LCInsureAccTraceSchema tLCInsureAccTraceSchema = new
                LCInsureAccTraceSchema();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        tLCInsureAccTraceSet = (LCInsureAccTraceSet) mInputData.
                               getObjectByObjectName("LCInsureAccTraceSet", 0);

        LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
        LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
        LCUWErrorDB tLCUWErrorDB;
        tLCUWErrorSet = (LCUWErrorSet) mInputData.getObjectByObjectName(
                "LCUWErrorSet", 0);

        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        LCUWSubDB tLCUWSubDB;
        tLCUWSubSet = (LCUWSubSet) mInputData.getObjectByObjectName(
                "LCUWSubSet", 0);

        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        LCUWMasterDB tLCUWMasterDB;
        tLCUWMasterSet = (LCUWMasterSet) mInputData.getObjectByObjectName(
                "LCUWMasterSet", 0);

        //保全核保工作流相关数据表add by sxy

        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        LWMissionSet tLWMissionSet = new LWMissionSet();
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionSet = (LWMissionSet) mInputData.getObjectByObjectName(
                "LWMissionSet", 0);

        LBMissionSchema tLBMissionSchema = new LBMissionSchema();
        LBMissionSet tLBMissionSet = new LBMissionSet();
        LBMissionDB tLBMissionDB = new LBMissionDB();
        tLBMissionSet = (LBMissionSet) mInputData.getObjectByObjectName(
                "LBMissionSet", 0);

        //保全工作流特约数据表add by sxy
        LCSpecSchema tLCSpecSchema = new LCSpecSchema();
        LCSpecSet tLCSpecSet = new LCSpecSet();
        LCSpecDB tLCSpecDB = new LCSpecDB();
        tLCSpecSet = (LCSpecSet) mInputData.getObjectByObjectName("LCSpecSet",
                0);

        LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
        LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
        tLCPENoticeSet = (LCPENoticeSet) mInputData.getObjectByObjectName(
                "LCPENoticeSet", 0);

        LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();
        LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
        LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
        tLCPENoticeItemSet = (LCPENoticeItemSet) mInputData.
                             getObjectByObjectName("LCPENoticeItemSet", 0);

        LCRReportSchema tLCRReportSchema = new LCRReportSchema();
        LCRReportSet tLCRReportSet = new LCRReportSet();
        LCRReportDB tLCRReportDB = new LCRReportDB();
        tLCRReportSet = (LCRReportSet) mInputData.getObjectByObjectName(
                "LCRReportSet", 0);

        LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
        LCRnewStateLogSet tLCRnewStateLogSet = new LCRnewStateLogSet();
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogSet = (LCRnewStateLogSet) mInputData.
                             getObjectByObjectName("LCRnewStateLogSet", 0);

        System.out.println("indeldata");
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "EdorAppCancelBLS";
            tError.functionName = "delEdor";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                tLCPolDB = new LCPolDB(conn);
                tLCPolDB.setSchema(tLCPolSet.get(i));
                if (!tLCPolDB.deleteSQL())
                { // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "个人保单表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCPremSet.size(); i++)
            {
                tLCPremDB = new LCPremDB(conn);
                tLCPremDB.setSchema(tLCPremSet.get(i));
                if (!tLCPremDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "保费项表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCDutySet.size(); i++)
            {
                tLCDutyDB = new LCDutyDB(conn);
                tLCDutyDB.setSchema(tLCDutySet.get(i));
                if (!tLCDutyDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "保险责任表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCGetSet.size(); i++)
            {
                tLCGetDB = new LCGetDB(conn);
                tLCGetDB.setSchema(tLCGetSet.get(i));
                if (!tLCGetDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "领取项表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCCustomerImpartSet.size(); i++)
            {
                tLCCustomerImpartDB = new LCCustomerImpartDB(conn);
                tLCCustomerImpartDB.setSchema(tLCCustomerImpartSet.get(i));
                if (!tLCCustomerImpartDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "客户告知表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCAppntIndSet.size(); i++)
            {
                tLCAppntIndDB = new LCAppntIndDB(conn);
                tLCAppntIndDB.setSchema(tLCAppntIndSet.get(i));
                if (!tLCAppntIndDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "多投保人表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCInsuredSet.size(); i++)
            {
                tLCInsuredDB = new LCInsuredDB(conn);
                tLCInsuredDB.setSchema(tLCInsuredSet.get(i));
                if (!tLCInsuredDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "多被保人表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCBnfSet.size(); i++)
            {
                tLCBnfDB = new LCBnfDB(conn);
                tLCBnfDB.setSchema(tLCBnfSet.get(i));
                if (!tLCBnfDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "受益人表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCInsureAccSet.size(); i++)
            {
                LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB(conn);
                tLCInsureAccDB.setSchema(tLCInsureAccSet.get(i));
                if (!tLCInsureAccDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "转移表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++)
            {
                LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB(
                        conn);
                tLCInsureAccTraceDB.setSchema(tLCInsureAccTraceSet.get(i));
                if (!tLCInsureAccTraceDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "转移表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCUWErrorSet.size(); i++)
            {
                tLCUWErrorDB = new LCUWErrorDB(conn);
                tLCUWErrorDB.setSchema(tLCUWErrorSet.get(i));
                if (!tLCUWErrorDB.deleteSQL())
                { // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "个人保全核保错误信息表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCUWSubSet.size(); i++)
            {
                tLCUWSubDB = new LCUWSubDB(conn);
                tLCUWSubDB.setSchema(tLCUWSubSet.get(i));
                if (!tLCUWSubDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "个人保全核保轨迹表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCUWMasterSet.size(); i++)
            {
                tLCUWMasterDB = new LCUWMasterDB(conn);
                tLCUWMasterDB.setSchema(tLCUWMasterSet.get(i));
                if (!tLCUWMasterDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorUWCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "个人保全核保最近结果表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLWMissionSet.size(); i++)
            {
                tLWMissionDB = new LWMissionDB(conn);
                tLWMissionDB.setSchema(tLWMissionSet.get(i));
                if (!tLWMissionDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "保全核保工作流相关数据表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLBMissionSet.size(); i++)
            {
                tLBMissionDB = new LBMissionDB(conn);
                tLBMissionDB.setSchema(tLBMissionSet.get(i));
                if (!tLBMissionDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "保全核保工作流相关数据B表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //保全工作流特约数据表
            for (int i = 1; i <= tLCSpecSet.size(); i++)
            {
                tLCSpecDB = new LCSpecDB(conn);
                tLCSpecDB.setSchema(tLCSpecSet.get(i));
                if (!tLCSpecDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "保全工作流特约数据表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCPENoticeSet.size(); i++)
            {
                tLCPENoticeDB = new LCPENoticeDB(conn);
                tLCPENoticeDB.setSchema(tLCPENoticeSet.get(i));
                if (!tLCPENoticeDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "续保体检主表数据表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCPENoticeItemSet.size(); i++)
            {
                tLCPENoticeItemDB = new LCPENoticeItemDB(conn);
                tLCPENoticeItemDB.setSchema(tLCPENoticeItemSet.get(i));
                if (!tLCPENoticeItemDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "续保体检子表数据表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCRReportSet.size(); i++)
            {
                tLCRReportDB = new LCRReportDB(conn);
                tLCRReportDB.setSchema(tLCRReportSet.get(i));
                if (!tLCRReportDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "续保生调数据表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            for (int i = 1; i <= tLCRnewStateLogSet.size(); i++)
            {
                tLCRnewStateLogDB = new LCRnewStateLogDB(conn);
                tLCRnewStateLogDB.setSchema(tLCRnewStateLogSet.get(i));
                if (!tLCRnewStateLogDB.deleteSQL())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "EdorAppCancelBLS";
                    tError.functionName = "delEdor";
                    tError.errorMessage = "续保状态数据表删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "EdorAppCancelBLS";
            tError.functionName = "delData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }

        return true;
    }


}