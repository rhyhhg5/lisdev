package com.sinosoft.lis.xb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 用户接口，接收页面传入数据，调用逻辑处理类PRnewDueFeeBL进行业务处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class PRnewDueFeeUI
{
    /**错误的容器*/
    public CErrors mErrors = null;

    public PRnewDueFeeUI()
    {
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData，
     * 包含
     * 1、TrasnferData数据类型，其包含了续保申请的起止日期
     * 2、LCContSchema，完整的个单信息，在本类不会进行校验
     * 3、GlobalInput，操作员信息，可为空
     * @param operate String，操作类型
     * @return boolean，操作成功返回true，否则放回false
     */
    public boolean submitData(VData data, String operate)
    {
        PRnewDueFeeBL bl = new PRnewDueFeeBL();
        if(!bl.submitData(data, operate))
        {
            if(bl.mErrors.needDealError())
            {
                mErrors = bl.mErrors;
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args)
    {
        LCContSchema  tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("00001491401");

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";
        g.ManageCom = g.ComCode;

        TransferData td = new TransferData();
        td.setNameAndValue("StartDate", "2005-01-01");
        td.setNameAndValue("EndDate", "2006-10-28");


        VData data = new VData();
        data.add(g);
        data.add(td);
        data.add(tLCContSchema);

        PRnewDueFeeUI tPRnewDueFeeUI = new PRnewDueFeeUI();
        tPRnewDueFeeUI.submitData(data, "INSERT");
        CErrors tError = tPRnewDueFeeUI.mErrors;
        if(!tError.needDealError())
        {
           System.out.println("处理成功！");
        }
        else
        {
           System.out.println(tError.getErrContent());
        }


    }
}
