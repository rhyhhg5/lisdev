package com.sinosoft.lis.xb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description:
 * 本类用来处理个单险种的续保申请，生成续保数据
 * </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GRnewPolAppBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private VData mPolInfoVData = null;
    private MMap map = null;
    /*需要做续保的险种信息*/
    private LCPolSchema mLCPolSchema = null;  //换号前的LCPol信息
    private LCPolSchema mLCPolSchemaUWed = null;  //核保后的险种
    private LPPolSchema mLPPolSchemaUWed = null;  //核保后的险种
    private LCPolSchema newLCPolSchema = null;  //换号后的LCPol信息
    private TransferData mTransferData = null;
    private boolean isGrpCont = false;
    private boolean isAfterUW = false;
    public static HashMap mSqlHashMap = new HashMap();

    private String mNewProposalNo = null;

    private String mOperate = null;

    public GRnewPolAppBL()
    {
    	
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括：LCPolSchema, GlobalInput
     * 不需要在上一层取出这些信息进行变更
     * GlobalInput：操作员信息
     * @param operate String，在此位“”
     * @return boolean
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in GRnewPolAppBL->getSubmitMap");
        mOperate = operate;
        if(!getInputData(inputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 得到保单传入的数据
     * @param inputData VData: 包括：LCPolSchema, GlobalInput
     * @return boolean：获取成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData.getObjectByObjectName("GlobalInput", 0);
        mLCPolSchema = (LCPolSchema) inputData.getObjectByObjectName("LCPolSchema", 0);
        mTransferData = (TransferData) inputData.getObjectByObjectName("TransferData", 0);
        if(mTransferData != null)
        {
            String strGrp = (String) mTransferData.getValueByName(XBConst.IS_GRP);
            isGrpCont = (strGrp !=null && XBConst.IS_GRP.equals(strGrp));

            String afterUW = (String) mTransferData.getValueByName(XBConst.AFTERUW);
            isAfterUW = (afterUW !=null && XBConst.AFTERUW.equals(afterUW));
        }

        if (mGlobalInput == null || mLCPolSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        mLCPolSchemaUWed = (LCPolSchema) mTransferData.getValueByName("LCPolUWed");
        mLPPolSchemaUWed = (LPPolSchema) inputData.getObjectByObjectName("LPPolSchema", 0);

        return true;
    }

    /**
     * 对需要续保的险种进行续保操作合法性的校验：
     * 1、校验被保人年龄是否大于险种描述的最大年龄
     * 2、校验当前险种定义是否符合续保定义
     * @return boolean，符合校验规则true，否则false
     */
    private boolean checkData()
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();  //险种定义

        String sql = "select * from LMRiskApp where riskCode = '"
                     + mLCPolSchema.getRiskCode() + "' ";
        Object o = mSqlHashMap.get(sql);
        if(o != null)
        {
            tLMRiskAppDB = (LMRiskAppDB) mSqlHashMap.get(sql);
        }
        else
        {
            tLMRiskAppDB.setRiskCode(mLCPolSchema.getRiskCode());
            tLMRiskAppDB.getInfo();
            mSqlHashMap.put(sql, tLMRiskAppDB);

        }

        if(!this.isGrpCont && !isAfterUW && this.inReNew())
        {
            return false;
        }

        //校验被保人年龄是否超过险种定义的最大投保年龄
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(mLCPolSchema.getContNo());
        tLCInsuredDB.setInsuredNo(mLCPolSchema.getInsuredNo());
        if(!tLCInsuredDB.getInfo())
        {
            mErrors.addOneError("没有查询到险种的被保人信息，");
            return false;
        }
        //有的险种被保人最大年龄没有描述，认为没有限制，此时tLMRiskAppDB的MaxInsuredAge值为0(旧)
        //续保时投保年龄先计算,避免没有MaxInsuredAge的险种,不计算投保年龄(新)
        int tAppntAge = PubFun.calInterval(tLCInsuredDB.getBirthday(),
                                           mLCPolSchema.getEndDate(), "Y");

        System.out.println(tLCInsuredDB.getBirthday() + ", " + mLCPolSchema.getEndDate());
        System.out.println("投保年龄:" + tAppntAge);
        System.out.println("投保年龄描述:" + tLMRiskAppDB.getMaxInsuredAge());
        //校验被保人年龄是否大于险种描述的最大年龄
        if(tLMRiskAppDB.getMaxInsuredAge() > 0)
        {
            if (tAppntAge < tLMRiskAppDB.getMinAppntAge())
            {
                mErrors.addOneError("被保人年龄不符合投保要求，");
                return false;
            }
        }
        mLCPolSchema.setInsuredAppAge(tAppntAge);

        return true;
    }

    /**
     * 校验是否正在续保
     * @return boolean：正续保true，否则false
     */
    private boolean inReNew()
    {
        //校验险种是否正在续保
        StringBuffer checkRenewSql = null;
        checkRenewSql = new StringBuffer();
        checkRenewSql.append("select * ")
            .append("from LCRnewStateLog ")
            .append("where state != '").append(XBConst.RNEWSTATE_CONFIRM)
            .append("'  and state != '").append(XBConst.RNEWSTATE_DELIVERED)
            .append("'  and polNo = '").append(mLCPolSchema.getPolNo())
            .append("' ");
        System.out.println(checkRenewSql.toString());
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet tLCRnewStateLogSet =
            tLCRnewStateLogDB.executeQuery(checkRenewSql.toString());
        if (tLCRnewStateLogSet.size() > 0)
        {
            StringBuffer errMsg = new StringBuffer();
            errMsg.append("被保人")
                .append(mLCPolSchema.getInsuredName())
                .append("的")
                .append(mLCPolSchema.getRiskCode())
                .append("险种正在续保，");
            mErrors.addOneError(errMsg.toString());
            return true;
        }

        return false;
    }


    /**
     * 对保单数据进行处理
     * 续保采用备份险种信息相关，并更改备份信息的相关流水号，如保单号险种号等
     * 而原险种信息的流水号将不做变更
     * 集体保单号，个人保单号等在对保单级别上做续保时更改
     * @return boolean：处理成功true，否则false
     */
    private boolean dealData()
    {
        map = new MMap();

        if(!preRnewPolInfo())
        {
            return false;
        }

        return true;
    }

    private boolean preRnewPolInfo()
    {
        mPolInfoVData = new VData();

        //准备重算保费所需的相关数据
        if(!preCalculteData())
        {
            return false;
        }

        //重算保费
        mPolInfoVData.add(mGlobalInput);
        PRnewPolBL tPRnewPolBL = new PRnewPolBL();
        MMap tMMap = tPRnewPolBL.getSubmitMap(mPolInfoVData, mOperate);
        if(tMMap == null || tPRnewPolBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(tPRnewPolBL.mErrors);
            return false;
        }
        map.add(tMMap);

        //得到重算后的险种信息
        newLCPolSchema = (LCPolSchema) map.getObjectByObjectName("LCPolSchema", 0);

        //续保状态
        LCRnewStateLogSchema tLCRnewStateLogSchema = preLCRnewStateLog();
        map.put(tLCRnewStateLogSchema, "INSERT");

        mNewProposalNo = newLCPolSchema.getPolNo();

        return true;
    }

    /**
     * 准备计算所需信息
     * @return boolean：准备成功true，否则false
     */
    private boolean preCalculteData()
    {
        //准备需险种信息
        preLCPol();
        mPolInfoVData.add(mLCPolSchema);
        //得到责任信息
        LCDutySet tLCDutySet = preLCDuty();
        if(tLCDutySet == null)
        {
            return false;
        }
        mPolInfoVData.add(tLCDutySet);

        //不能通过新契约计算出来的保费项，如加费纪录等
        LCPremSet tLCPremSet = preLCPrem();
        mPolInfoVData.add(tLCPremSet);
        
        return true;
    }

    /**
     * 为续保准备需险种信息
     * 准备责任信息
     * @return LCDuty：准备完毕的责任信息
     * @return boolean
     */
    private boolean preLCPol()
    {
        mLCPolSchema.setLeavingMoney(0);
        mLCPolSchema.setCValiDate(mLCPolSchema.getEndDate());
        mLCPolSchema.setSpecifyValiDate("Y"); //生效日期（指定）
        mLCPolSchema.setEndDate("");
        mLCPolSchema.setSignDate("");
        mLCPolSchema.setSignTime("");
        mLCPolSchema.setFirstPayDate("");
//        mLCPolSchema.setInsuredAppAge(PubFun.calInterval(mLCPolSchema.getInsuredBirthday(),mLCPolSchema.getPolApplyDate(), "Y"));

        if (mLCPolSchema.getPayLocation() == null)
        { //多责任项
            mLCPolSchema.setPayLocation("");
        }

        return true;
    }

    /**
     * 准备责任信息
     * @return LCDuty：准备完毕的责任信息
     */
    private LCDutySet preLCDuty()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from lcduty ")
            .append("where polno = '")
            .append(mLCPolSchema.getPolNo())
            .append("' ");
        System.out.println(sql.toString());
        LCDutyDB tLCDutyDB = new LCDutyDB();
        LCDutySet tLCDutySet = tLCDutyDB.executeQuery(sql.toString());
        if(tLCDutySet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种"+ mLCPolSchema.getRiskCode() +"的责任信息。");
            return null;
        }

        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            //得到续保核保后的保额档次
            if(mLCPolSchemaUWed != null)
            {
                tLCDutySet.get(i).setMult(mLCPolSchemaUWed.getMult());
                tLCDutySet.get(i).setAmnt(mLCPolSchemaUWed.getAmnt());
            }
            else if(mLPPolSchemaUWed != null){
            	System.out.println("我就不信了不信了不信了");
            	tLCDutySet.get(i).setMult(mLPPolSchemaUWed.getMult());
                tLCDutySet.get(i).setAmnt(mLPPolSchemaUWed.getAmnt());
            }

            if("162601".equals(mLCPolSchema.getRiskCode())||"162801".equals(mLCPolSchema.getRiskCode())){
            	tLCDutySet.get(i).setPrem(0);
            }
            tLCDutySet.get(i).setEndDate("");
            tLCDutySet.get(i).setFirstPayDate("");
            tLCDutySet.get(i).setPaytoDate("");
        }

        return tLCDutySet;
    }

    /**
     * 得到非计算得到的保费项信息
     * @return LCPremSet：准备完毕的缴费项信息
     */
    private LCPremSet preLCPrem()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from lcprem ")
            .append("where polno = '")
            .append(mLCPolSchema.getPolNo())
            .append("'  and payPlanCode like '000000%' ")
            .append("   and (payEndDate is null or payEndDate >= ")
            .append(" (select enddate from lcpol where polno ='"+mLCPolSchema.getPolNo()+"')").append(") ");
        System.out.println("" + sql.toString());
        LCPremDB tLCPremDB = new LCPremDB();
        LCPremSet tLCPremSet = tLCPremDB.executeQuery(sql.toString());

        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            //待定
        }

        return tLCPremSet;
    }

    /**
     * 续保日志信息
     * @return LCRnewStateLogSchema：准备完毕的续保日至信息
     */
    private LCRnewStateLogSchema preLCRnewStateLog(){
        LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
        tLCRnewStateLogSchema.setGrpContNo(mLCPolSchema.getGrpContNo());
        tLCRnewStateLogSchema.setNewGrpContNo(newLCPolSchema.getGrpContNo());
        tLCRnewStateLogSchema.setGrpPolNo(mLCPolSchema.getGrpPolNo());
        tLCRnewStateLogSchema.setNewGrpPolNo(newLCPolSchema.getGrpPolNo());
        tLCRnewStateLogSchema.setContNo(mLCPolSchema.getContNo());
        tLCRnewStateLogSchema.setNewContNo(newLCPolSchema.getContNo());
        tLCRnewStateLogSchema.setPolNo(mLCPolSchema.getPolNo());
        tLCRnewStateLogSchema.setNewPolNo(newLCPolSchema.getProposalNo());
        tLCRnewStateLogSchema.setPrtNo(mLCPolSchema.getPrtNo());
        tLCRnewStateLogSchema.setRenewCount(newLCPolSchema.getRenewCount());
        tLCRnewStateLogSchema.setRiskFlag(mLCPolSchema.getMainPolNo().equals(mLCPolSchema.getPolNo())
                                          ? "M" : "S");
        tLCRnewStateLogSchema.setState(XBConst.RNEWSTATE_APP);
        tLCRnewStateLogSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCRnewStateLogSchema);
        tLCRnewStateLogSchema.setPaytoDate(newLCPolSchema.getPaytoDate());
        tLCRnewStateLogSchema.setMainRiskStatus("0");

        return tLCRnewStateLogSchema;
    }

    /**
     * 外部操作的提交方法
     * @param inputData VData：包括：LCPolSchema, GlobalInput
     * 不需要在上一层取出这些信息进行变更
     * GlobalInput：操作员信息
     * @param operate String，在此为“”
     * @return boolean：操作成功true：否则false
     */
    public boolean submitData(VData inputData, String operate)
    {
        return true;
    }

    /**
     * 得到投保单号
     * @return String：投保单号
     */
    public String getNewProposalNo()
    {
        return mNewProposalNo;
    }

    /**
     * 调试方法
     * @param args String[]，null
     */
    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "qulq";
        mGlobalInput.ManageCom = "86";
        TransferData mTransferData = new TransferData();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo("13001311378");
        LCPolSet mLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= mLCPolSet.size(); i++) {
            VData tVData = new VData();
            tVData.clear();
            tVData.add(mGlobalInput);
            tVData.add(mLCPolSet.get(i));
            mTransferData.setNameAndValue(XBConst.IS_GRP, XBConst.IS_GRP);
            tVData.add(mTransferData);

            GRnewPolAppBL GRnewPolAppBL = new GRnewPolAppBL();
            GRnewPolAppBL.getSubmitMap(tVData, "");
        }

    }
}
