package com.sinosoft.lis.xb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GRnewContAppUI
{
    /**错误的容器*/
    public CErrors mErrors = null;

    public GRnewContAppUI()
    {
    }

    /**
     * 外部操作提交的方法
     * @param inputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData inputData, String operate)
    {
        GRnewContAppBL tGRnewContAppBL = new GRnewContAppBL();
        if(!tGRnewContAppBL.submitData(inputData, operate))
        {
            mErrors = tGRnewContAppBL.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GRnewContAppUI grnewcontappui = new GRnewContAppUI();
    }
}
