package com.sinosoft.lis.xb;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class XBConst
{
    /**保单类型，个单*/
    public static final String CONTTYPE_P = "1";
    /**保单类型，团单*/
    public static final String CONTTYPE_G = "2";

    /**续保状态：续保申请，即待自核*/
    public static final String RNEWSTATE_APP = "1";
    /**续保状态：待人工核保*/
    public static final String RNEWSTATE_UNUW = "2";
    /**续保状态：待催收*/
    public static final String RNEWSTATE_UNHASTEN = "3";
    /**续保状态：待续保确认*/
    public static final String RNEWSTATE_UNCONFIRM = "4";
    /**续保状态：续保确认成功*/
    public static final String RNEWSTATE_CONFIRM = "5";
    /**续保状态：原保单到期并已转移备份*/
    public static final String RNEWSTATE_DELIVERED = "6";
    /**续保状态：原险种续保终止*/
    public static final String RNEWSTATE_XBSTOP = "0";

    /**团单标志*/
    public static final String IS_GRP = "IS_GRP";

    /**续保核保后险种*/
    public static final String AFTERUW = "AFTERUW";

    public static final String NEED_MANU_UW = "1";

    public static final String XBSTOP = "2";

    /**险种状态，核保续保终止*/
    public static final String POLSTATE_XBSTOP = "03050001";
    
    public static final String POLSTATE_LPXBSTOP = "03070001";

    public static final String TERMINATE = "Terminate";

    private XBConst()
    {
    }

    public static void main(String[] args)
    {
        XBConst xbconst = new XBConst();
    }
}
