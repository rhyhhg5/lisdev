package com.sinosoft.lis.xb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: </p>
 * <p>Description:
 * 转移个单数据：将处理后的续保数据和原数据相关号码互换，
 * 这样可以使得续保结束后可以使用原来的号码直接查询新保单信息，
 * 并把续保前的保单信息备份到b表
 * 对于在续保过程中处理过的信息，需要转移到B表
 * 而续保过程中未处理过的信息则只需要备份到B表，并修改相关的C表信息
 * </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
public class TransferCPDataToBP
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;  //用户信息
    private LCContSchema mLCContSchema = null;  //续保前的个单信息
    private boolean isGrpCont = false;
    private Reflections ref = null;

    private String mCurrDate = PubFun.getCurrentDate();
    private String mCurrTime = PubFun.getCurrentTime();
    private String mChgModiFyInfo = null; //变更Operator,modifyDate,modifyTime的sql
    private String mChgDefaultInfo = null; //变更Operator,modifyDate,modifyTime,makeDate,makeTime的sql

    private String mEdorNo = null;

    private String mNewPolNo = null;
    private String mNewProposalNo = null;
    private String mNewContNo = null;
    private String mNewProposalContNo = null;
    private String mNewGrpPolNo = null;  //为了支持团单调用个单，需要记录相关的团单号码
    private String mNewGrpContNo = null;

    private String mOldPolNo = null;
    private String mOldProposalNo = null;
    private String mOldContNo = null;
    private String mOldProposalContNo = null;
    private String mOldGrpPolNo = null;
    private String mOldGrpContNo = null;

    private MMap map = null;  //存储处理后的数据

    public TransferCPDataToBP()
    {
    }

    /**
     * 构造方法
     * @param edorNo String：受理号
     */
    public TransferCPDataToBP(String edorNo)
    {
        mEdorNo = edorNo;
    }

    /**
     * 转移续保确认后的保单
     * @param inputData VData：包含：
     * 1、GlobalInput：操作员信息
     * 2、LCContSchema：保单信息
     * @param cOperate
     * @return
     */
    public boolean submitData(VData inputData, String cOperate)
    {
        if(getSubmitMap(inputData, cOperate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("存储数据失败");
            return false;
        }

        return true;
    }

    /**
     * 外部操作的提交方法，处理业务逻辑
     * @param inputData VData：包含：
     * 1、GlobalInput：操作员信息
     * 2、LCContSchema：保单信息
     * @param operate String：操作方式，此为""
     * @return MMap
     */
    public MMap getSubmitMap(VData inputData, String operate)
    {
        System.out.println("Now in TransferCPDataToBP->getSubmitMap");
        if(!getInputData(inputData))
        {
            return null;
        }

        map = new MMap();
        if(mEdorNo == null || mEdorNo.equals("") || mEdorNo.equals("null"))
        {
            mEdorNo = "xb" + com.sinosoft.task.CommonBL.createWorkNo();
        }
        else if(mEdorNo.indexOf("xb") < 0)
        {
            mEdorNo = "xb" + mEdorNo;
        }

        ref = new Reflections();
        //转移个单
        if (!transferP())
        {
            return null;
        }

        return map;
    }

    /**
     * 转移个单数据
     * 在TransferCGDataToBG中，不能够批次处理有字段proposalNo和proposalCont的表，
     * 需要在本类中按险种（或个单）为单位进行处理
     * @return boolean
     */
    private boolean transferP()
    {
        //得到个单的所有需要转移的险种记录
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        tLCRnewStateLogDB.setContNo(mLCContSchema.getContNo());
        tLCRnewStateLogDB.setState(XBConst.RNEWSTATE_CONFIRM);
        LCRnewStateLogSet tLCRnewStateLogSet = tLCRnewStateLogDB.query();
        if(tLCRnewStateLogSet.size() == 0)
        {
            mErrors.addOneError("没有需要转移的续保数据。");
            if(tLCRnewStateLogDB.mErrors.needDealError())
            {
                System.out.println(tLCRnewStateLogDB.mErrors.getErrContent());
            }
            return false;
        }

        //转移保单下所有险种相关的信息
        for (int polCount = 1; polCount <= tLCRnewStateLogSet.size(); polCount++)
        {
            if(!transferOnePolData(tLCRnewStateLogSet.get(polCount)))
            {
                return false;
            }
        }

        if(!transferContData(tLCRnewStateLogSet.get(1)))
        {
            return true;
        }

        if(!isGrpCont && !changeLogState())
        {
            return false;
        }

        return true;
    }

    /**
     * 获取传入的数据
     * @param inputData VData：包含：
     * 1、GlobalInput：操作员信息
     * 2、LCContSchema：保单信息
     * @return boolean：获取成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        this.mGlobalInput = (GlobalInput) inputData
                            .getObjectByObjectName("GlobalInput", 0);
        this.mLCContSchema = (LCContSchema) inputData
                             .getObjectByObjectName("LCContSchema", 0);

        if (this.mGlobalInput == null || this.mGlobalInput.Operator == null
            || this.mLCContSchema == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        this.mChgModiFyInfo = " Operator = '" + mGlobalInput.Operator
                              + "', ModifyDate = '" + this.mCurrDate
                              + "', ModifyTime = '" + this.mCurrTime + "' ";
        this.mChgDefaultInfo = this.mChgModiFyInfo
                               + ", MakeDate = '" + this.mCurrDate
                               + "', MakeTime = '" + this.mCurrTime + "' ";

        return true;
    }

    /**
     * 转移一个险种的信息
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferOnePolData(
        LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
        if (tLCRnewStateLogSchema.getPolNo() == null
            || tLCRnewStateLogSchema.getPolNo().equals("")
            || tLCRnewStateLogSchema.getNewPolNo() == null
            || tLCRnewStateLogSchema.getNewPolNo().equals(""))
        {
            mErrors.addOneError("续保数据不完整。");
            return false;
        }


        if (!transferPol(tLCRnewStateLogSchema))
        {
            return false;
        }
        if (!transferDuty(tLCRnewStateLogSchema))
        {
            return false;
        }
        if (!transferPrem(tLCRnewStateLogSchema))
        {
            return false;
        }
        if (!transferGet(tLCRnewStateLogSchema))
        {
            return false;
        }
        String str="select 1 from lmriskapp a,lcpol b where a.riskcode=b.riskcode and b.polno='"+mNewPolNo+"' and a.TaxOptimal='Y'";
		String SYflag=new ExeSQL().getOneValue(str);
		if(null == SYflag && "".equals(SYflag)){
			//税优保单续保时不处理账户
	        if (!transferInsureAcc(tLCRnewStateLogSchema))
	        {
	            return false;
	        }
	        if (!transferInsureAccClass(tLCRnewStateLogSchema))
	        {
	            return false;
	        }
	        if (!transferInsureAccTrace(tLCRnewStateLogSchema))
	        {
	            return false;
	        }
		}
        //转移在续保中并不需要直接处理的险种相关数据
        if (!transferOtherPolInfo(tLCRnewStateLogSchema))
        {
            return false;
        }
        if(!dealFinaceData())
        {
            return false;
        }

        return true;
    }

    /**
     * 变更财务数据的相关号码
     * 不需要讲就保单财务数据放到备份表
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean dealFinaceData()
    {
        //得到续保新收费的保单收费号
        String getPayNo = "  select distinct payNo "
                          + "from LJAPayPerson "
                          + "where polNo = '" + this.mNewPolNo + "' ";
        ExeSQL e = new ExeSQL();
        String tPRnewPayNo = e.getOneValue(getPayNo);
        if(tPRnewPayNo.equals("") || tPRnewPayNo.equals("null"))
        {
            return true;
        }

        //将新续保财务实收数据号码变更为旧号
        String sql = "  update LJAPayPerson "
                     + "set grpContNo = '" + this.mOldGrpContNo + "', "
                     + "   grpPolNo = '" + this.mOldGrpPolNo + "', "
                     + "   contNo = '" + this.mOldContNo + "', "
                     + "   polNo = '" + this.mOldPolNo + "', "
                     + "   operator = '" + this.mGlobalInput.Operator + "', "
                     + "   modifyDate = '" + this.mCurrDate + "', "
                     + "   modifyTime = '" + this.mCurrTime + "' "
                     + "where polNo = '" + this.mNewPolNo + "' ";
        map.put(sql, "UPDATE");

        //将新续保财务实收备份数据号码变更为旧号
        sql = sql.replaceAll("LJAPayPerson", "LJSPayPersonB");
        map.put(sql, "UPDATE");

        //将原保单续保财务实收数据号码变更为新号
        //因为在上面已将续保财务财务数据换为旧号，所以此处换号需要要排除本次续保的险种
        sql = "  update LJAPayPerson "
                     + "set grpContNo = '" + this.mNewGrpContNo + "', "
                     + "   grpPolNo = '" + this.mNewGrpPolNo + "', "
                     + "   contNo = '" + this.mNewContNo + "', "
                     + "   polNo = '" + this.mNewPolNo + "', "
                     + "   operator = '" + this.mGlobalInput.Operator + "', "
                     + "   modifyDate = '" + this.mCurrDate + "', "
                     + "   modifyTime = '" + this.mCurrTime + "' "
                     + "where polNo = '" + this.mOldPolNo + "' "
                     + "   and payNo != '" + tPRnewPayNo + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 转移LCPol信息
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferPol(LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
        LCPolDB tLCPolDB = new LCPolDB();
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
            .append("from LCPol ")
            .append("where polNo in ('")
            .append(tLCRnewStateLogSchema.getPolNo())
            .append("', '")
            .append(tLCRnewStateLogSchema.getNewPolNo())
            .append("') ")
            .append("order by contNo "); //生成保单号是顺序的，号大的是新保单
        System.out.println(sql.toString());
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql.toString());
        if (tLCPolSet.size() != 2)
        {
            mErrors.addOneError("续保险种数据不完整。");
            System.out.println(tLCPolDB.mErrors.getErrContent());
            return false;
        }
        //qulq modify 保单升位后上面的语句执行不正常，所以需要判断一下
        LCPolSchema oldLCPolSchema = tLCPolSet.get(1); //续保前
        LCPolSchema newLCPolSchema = tLCPolSet.get(2); //续保后
        if(tLCPolSet.get(2).getPolNo().equals(tLCRnewStateLogSchema.getPolNo()))
        {
            oldLCPolSchema = tLCPolSet.get(2);
            newLCPolSchema = tLCPolSet.get(1);
        }


//        map.put(oldLCPolSchema.getSchema(), "DELETE");
//        map.put(newLCPolSchema.getSchema(), "DELETE");

        //新生成的号码
        mNewPolNo = tLCRnewStateLogSchema.getNewPolNo();
        mNewProposalNo = newLCPolSchema.getProposalNo();
        mNewContNo = tLCRnewStateLogSchema.getNewContNo();
        mNewProposalContNo = newLCPolSchema.getProposalContNo();
        mNewGrpPolNo = tLCRnewStateLogSchema.getNewGrpPolNo();
        mNewGrpContNo = tLCRnewStateLogSchema.getNewGrpContNo();

        //系统原来的号码
        mOldPolNo = tLCRnewStateLogSchema.getPolNo();
        mOldProposalNo = oldLCPolSchema.getProposalNo();
        mOldContNo = tLCRnewStateLogSchema.getContNo();
        mOldProposalContNo = oldLCPolSchema.getProposalContNo();
        mOldGrpPolNo = tLCRnewStateLogSchema.getGrpPolNo();
        mOldGrpContNo = tLCRnewStateLogSchema.getGrpContNo();

        //将新险种的号换成旧号
        transferPolTableData("LCPol", "LBPol");
        String updateSql = "  update LCPol "
                           + "set PolNo = '" + mOldPolNo
                           + "', ProposalNo = '" + mOldProposalNo
                           + "', ContNo = '" + mOldContNo
                           + "', ProposalContNo = '" + mOldProposalContNo
                           + "', GrpPolNo = '" + mOldGrpPolNo
                           + "', GrpContNo = '" + mOldGrpContNo
                           + "', AppFlag = '"
                           + BQ.APPFLAG_SIGN
                           + "', StateFlag = '"
                           + BQ.STATE_FLAG_SIGN
                           + "', "
                           + mChgModiFyInfo
                           + "where polNo = '" + mNewPolNo + "' ";
        map.put(updateSql, "UPDATE");

//        newLCPolSchema.setPolNo(oldLCPolSchema.getPolNo());
//        newLCPolSchema.setProposalNo(oldLCPolSchema.getProposalNo());
//        newLCPolSchema.setContNo(oldLCPolSchema.getContNo());
//        newLCPolSchema.setProposalContNo(oldLCPolSchema.getProposalContNo());
//        newLCPolSchema.setGrpPolNo(oldLCPolSchema.getGrpPolNo());
//        newLCPolSchema.setGrpContNo(oldLCPolSchema.getGrpContNo());
//        newLCPolSchema.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_SIGN);
//        newLCPolSchema.setOperator(mGlobalInput.Operator);
//        newLCPolSchema.setModifyDate(PubFun.getCurrentDate());
//        newLCPolSchema.setModifyTime(PubFun.getCurrentTime());
//        map.put(newLCPolSchema, "INSERT");

        //将原险种放到B表并换成新号码
        updateSql = "  update LBPol "
                    + "set PolNo = '" + mNewPolNo
                    + "', ProposalNo = '" + mNewProposalNo
                    + "', ContNo = '" + mNewContNo
                    + "', ProposalContNo = '" + mNewProposalContNo
                    + "', GrpPolNo = '" + mNewGrpPolNo
                    + "', GrpContNo = '" + mNewGrpContNo
                    + "', StateFlag = '" + BQ.STATE_FLAG_TERMINATE
                    + "', "
                    + mChgDefaultInfo
                    + "where polNo = '" + mOldPolNo + "' ";
        map.put(updateSql, "UPDATE");

        setContState(mNewPolNo, oldLCPolSchema.getInsuredNo(),
                     oldLCPolSchema.getEndDate());

//        LBPolSchema tLBPolSchema = new LBPolSchema(); //原数据放到B表
//        ref.transFields(tLBPolSchema, oldLCPolSchema);
//        tLBPolSchema.setEdorNo(this.mEdorNo);
//        tLBPolSchema.setPolNo(mNewPolNo);
//        tLBPolSchema.setProposalNo(mNewProposalNo);
//        tLBPolSchema.setContNo(mNewContNo);
//        tLBPolSchema.setProposalContNo(mNewProposalContNo);
//        tLBPolSchema.setGrpPolNo(mNewGrpPolNo);
//        tLBPolSchema.setGrpContNo(mNewGrpContNo);
//        tLBPolSchema.setOperator(mGlobalInput.Operator);
//        PubFun.fillDefaultField(tLBPolSchema);
//        map.put(tLBPolSchema, "INSERT");

        return true;
    }

    /**
     *
     * @param newPolNo String:若为个单状态，则为BQ.FILLDATA
     * @param oldLCPolSchema LCPolSchema
     */
    private void setContState(String newPolNo,
                              String insuredNo,
                              String StartDate)
    {
        //个单续保才会生成保单状态表信息，团单只需要生成团单级别的即可
        if(!isGrpCont)
        {
            LCContStateSchema tLCContStateSchema = new LCContStateSchema();
            tLCContStateSchema.setGrpContNo(BQ.FILLDATA);
            tLCContStateSchema.setContNo(mNewContNo);
            tLCContStateSchema.setInsuredNo(insuredNo);
            tLCContStateSchema.setPolNo(newPolNo);
            tLCContStateSchema.setStateType("Terminate");
            tLCContStateSchema.setStateReason(BQ.EDORTYPE_XB);
            tLCContStateSchema.setOtherNo(mEdorNo);
            tLCContStateSchema.setOtherNoType(BQ.EDORTYPE_XB);
            tLCContStateSchema.setState("1");
            tLCContStateSchema.setOperator(mGlobalInput.Operator);
            tLCContStateSchema.setStartDate(StartDate);
            tLCContStateSchema.setEndDate("9999-12-31");
            tLCContStateSchema.setMakeDate(PubFun.getCurrentDate());
            tLCContStateSchema.setMakeTime(PubFun.getCurrentTime());
            tLCContStateSchema.setModifyDate(PubFun.getCurrentDate());
            tLCContStateSchema.setModifyTime(PubFun.getCurrentTime());
            System.out.println("StartDate   +++++"+StartDate);
            map.put(tLCContStateSchema, "DELETE&INSERT");
        }

    }

    private void transferPolTableData(String cTableName, String bTableName)
    {
        //将旧信息转移到B表
        copyPolTableDataToB(cTableName, bTableName);
        String sql = "delete from " + cTableName
              + " where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "DELETE");
    }

    private void copyPolTableDataToB(String cTableName, String bTableName)
    {
        String sql = "insert into " + bTableName
                     + " (select '" + mEdorNo + "', " + cTableName + ".* "
                     + " from " + cTableName
                     + " where polNo = '" + mOldPolNo + "') ";
        map.put(sql, "INSERT");
    }

    private void transferContTableData(String cTableName, String bTableName)
    {
        //将旧信息转移到B表
        copyContTableDataToB(cTableName, bTableName);
        String sql = "delete from " + cTableName
              + " where contNo = '" + mOldContNo + "' ";
        map.put(sql, "DELETE");
    }

    private void copyContTableDataToB(String cTableName, String bTableName)
    {
        String sql = "insert into " + bTableName
                     + " (select '" + mEdorNo + "', " + cTableName + ".* "
                     + " from " + cTableName
                     + " where contNo = '" + mOldContNo + "') ";
        map.put(sql, "INSERT");
    }


    /**
     * 转移责任信息
     * @return boolean
     */
    private boolean transferDuty(LCRnewStateLogSchema tLCRnewStateLogSchema)
    {

//        StringBuffer sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCDuty ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getPolNo())
//            .append("' ");  //续保前的责任信息
//        System.out.println(sql.toString());
//        LCDutyDB tLCDutyDB = new LCDutyDB();
//        LCDutySet oldLCDutySet = tLCDutyDB.executeQuery(sql.toString());
//        map.put(oldLCDutySet, "DELETE");
//        sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCDuty ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getNewPolNo())
//            .append("' ");  //续保确认后的责任信息
//        System.out.println(sql.toString());
//        LCDutySet newLCDutySet = tLCDutyDB.executeQuery(sql.toString());
//        map.put(newLCDutySet, "DELETE");
//
//        if(oldLCDutySet == null || newLCDutySet == null)
//        {
//            mErrors.addOneError("续保责任数据不完整。");
//            return false;
//        }
//
//
//        for(int i = 1; i <= oldLCDutySet.size(); i++)
//        {
//            LBDutySchema oldLBDutySchema = new LBDutySchema();;
//            ref.transFields(oldLBDutySchema, oldLCDutySet.get(i));
//            oldLBDutySchema.setEdorNo(this.mEdorNo);
//            oldLBDutySchema.setContNo(this.mNewContNo);
//            oldLBDutySchema.setPolNo(this.mNewPolNo);
//            oldLBDutySchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(oldLBDutySchema);
//            map.put(oldLBDutySchema, "INSERT");
//        }
//
//        LCDutySet dealtLCDutySet = new LCDutySet();
//        for (int i = 1; i <= newLCDutySet.size(); i++)
//        {
//            LCDutySchema newLCDutySchema = newLCDutySet.get(i).getSchema();
//            newLCDutySchema.setContNo(tLCRnewStateLogSchema.getContNo());
//            newLCDutySchema.setPolNo(tLCRnewStateLogSchema.getPolNo());
//            newLCDutySchema.setOperator(mGlobalInput.Operator);
//            newLCDutySchema.setModifyDate(this.mCurrDate);
//            newLCDutySchema.setModifyTime(this.mCurrTime);
//            dealtLCDutySet.add(newLCDutySchema);
//        }
//        map.put(dealtLCDutySet, "INSERT");

        transferPolTableData("LCDuty", "LBDuty");
        if(isGrpCont)
        {
            return true;
        }

        String sql = "update LCDuty "
                     + "set polNo = '" + mOldPolNo
                     + "', contNo = '" + mOldContNo + "', "
                     + mChgModiFyInfo
                     + "where polNo = '" + mNewPolNo + "' ";
        map.put(sql, "UPDATE");

        sql = "update LBDuty "
              + "set polNo = '" + mNewPolNo
              + "', contNo = '" + mNewContNo + "', "
              + this.mChgDefaultInfo
              + "where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "UPDATE");
        return true;
    }

    /**
     * 转移缴费项信息
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferPrem(LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
//        StringBuffer sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCPrem ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getPolNo())
//            .append("'  ");
//        System.out.println(sql.toString());
//        LCPremDB tLCPremDB = new LCPremDB();
//        LCPremSet oldLCPremSet = tLCPremDB.executeQuery(sql.toString());
//        map.put(oldLCPremSet, "DELETE");
//
//        sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCPrem ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getNewPolNo())
//            .append("' ");
//        System.out.println(sql.toString());
//        LCPremSet newLCPremSet = tLCPremDB.executeQuery(sql.toString());
//        map.put(newLCPremSet, "DELETE");
//        if(oldLCPremSet == null || newLCPremSet == null)
//        {
//            mErrors.addOneError("续保缴费项不完整。");
//            return false;
//        }
//
//        for(int i = 1; i <= oldLCPremSet.size(); i++)
//        {
//            LBPremSchema tLBPremSchema = new LBPremSchema();
//            ref.transFields(tLBPremSchema, oldLCPremSet.get(i));
//            tLBPremSchema.setEdorNo(this.mEdorNo);
//            tLBPremSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBPremSchema.setContNo(this.mNewContNo);
//            tLBPremSchema.setPolNo(this.mNewPolNo);
//            tLBPremSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBPremSchema);
//            map.put(tLBPremSchema, "INSERT");
//        }
//        LCPremSet dealtLCPremSet = new LCPremSet();
//        for(int i = 1; i <= newLCPremSet.size(); i++)
//        {
//            LCPremSchema newLCPremSchema = newLCPremSet.get(i).getSchema();
//            newLCPremSchema.setGrpContNo(tLCRnewStateLogSchema.getGrpContNo());
//            newLCPremSchema.setContNo(tLCRnewStateLogSchema.getContNo());
//            newLCPremSchema.setPolNo(tLCRnewStateLogSchema.getPolNo());
//            newLCPremSchema.setOperator(mGlobalInput.Operator);
//            newLCPremSchema.setModifyDate(this.mCurrDate);
//            newLCPremSchema.setModifyTime(this.mCurrTime);
//            dealtLCPremSet.add(newLCPremSchema);
//        }
//        map.put(dealtLCPremSet, "INSERT");

        this.transferPolTableData("LCPrem", "LBPrem");
        if(isGrpCont)
        {
            return true;
        }

        String sql = "update LCPrem "
                     + "set polNo = '" + mOldPolNo
                     + "', contNo = '" + mOldContNo
                     + "', grpContNo = '" + mOldGrpContNo + "', "
                     + mChgModiFyInfo
                     + "where polNo = '" + mNewPolNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LBPrem "
              + "set polNo = '" + mNewPolNo
              + "', contNo = '" + mNewContNo
              + "', grpContNo = '" + mNewGrpContNo + "', "
              + this.mChgDefaultInfo
              + "where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 转移保费领取项
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferGet(LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
//        StringBuffer sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCGet ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getPolNo())
//            .append("'  ");
//        System.out.println(sql.toString());
//        LCGetDB tLCGetDB = new LCGetDB();
//        LCGetSet oldLCGetSet = tLCGetDB.executeQuery(sql.toString());
//        map.put(oldLCGetSet, "DELETE");
//
//        sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCGet ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getNewPolNo())
//            .append("' ");
//        System.out.println(sql.toString());
//        LCGetSet newLCGetSet = tLCGetDB.executeQuery(sql.toString());
//        map.put(newLCGetSet, "DELETE");
//        if(oldLCGetSet == null || newLCGetSet == null)
//        {
//            mErrors.addOneError("续保缴费项不完整。");
//            return false;
//        }
//
//        //转移到B表
//        for(int i = 1; i <= oldLCGetSet.size(); i++)
//        {
//            LBGetSchema tLBGetSchema = new LBGetSchema();
//            ref.transFields(tLBGetSchema, oldLCGetSet.get(i));
//            tLBGetSchema.setEdorNo(this.mEdorNo);
//            tLBGetSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBGetSchema.setContNo(this.mNewContNo);
//            tLBGetSchema.setPolNo(this.mNewPolNo);
//            tLBGetSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBGetSchema);
//            map.put(tLBGetSchema, "INSERT");
//        }
//        //处理新保费项
//        LCGetSet dealtLCGetSet = new LCGetSet();
//        for(int i = 1; i <= newLCGetSet.size(); i++)
//        {
//            LCGetSchema newLCGetSchema = newLCGetSet.get(i).getSchema();
//            newLCGetSchema.setGrpContNo(tLCRnewStateLogSchema.getGrpContNo());
//            newLCGetSchema.setContNo(tLCRnewStateLogSchema.getContNo());
//            newLCGetSchema.setPolNo(tLCRnewStateLogSchema.getPolNo());
//            newLCGetSchema.setOperator(mGlobalInput.Operator);
//            newLCGetSchema.setModifyDate(this.mCurrDate);
//            newLCGetSchema.setModifyTime(this.mCurrTime);
//            dealtLCGetSet.add(newLCGetSchema);
//        }
//        map.put(dealtLCGetSet, "INSERT");

        this.transferPolTableData("LCGet", "LBGet");
        if(isGrpCont)
        {
            return true;
        }

        String sql = "update LCGet "
                     + "set polNo = '" + mOldPolNo
                     + "', contNo = '" + mOldContNo
                     + "', grpContNo = '" + mOldGrpContNo + "', "
                     + mChgModiFyInfo
                     + "where polNo = '" + mNewPolNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LBGet "
              + "set polNo = '" + mNewPolNo
              + "', contNo = '" + mNewContNo
              + "', grpContNo = '" + mNewGrpContNo + "', "
              + this.mChgDefaultInfo
              + "where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 转移帐户信息
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferInsureAcc(LCRnewStateLogSchema
                                      tLCRnewStateLogSchema)
    {
//        StringBuffer sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCInsureAcc ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getPolNo())
//            .append("'  ");
//        System.out.println(sql.toString());
//        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
//        LCInsureAccSet oldInsureAccSet = tLCInsureAccDB.executeQuery(sql.toString());
//        map.put(oldInsureAccSet, "DELETE");
//
//        sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCInsureAcc ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getNewPolNo())
//            .append("' ");
//        System.out.println(sql.toString());
//        LCInsureAccSet newLCInsureAccSet = tLCInsureAccDB.executeQuery(sql.toString());
//        map.put(newLCInsureAccSet, "DELETE");
//        if(oldInsureAccSet == null || newLCInsureAccSet == null)
//        {
//            mErrors.addOneError("续保缴费项不完整。");
//            return false;
//        }
//
//        //转移到B表
//        for(int i = 1; i <= oldInsureAccSet.size(); i++)
//        {
//            LBInsureAccSchema tLBInsureAccSchema = new LBInsureAccSchema();
//            ref.transFields(tLBInsureAccSchema, oldInsureAccSet.get(i));
//            tLBInsureAccSchema.setEdorNo(this.mEdorNo);
//            tLBInsureAccSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBInsureAccSchema.setGrpPolNo(this.mNewGrpPolNo);
//            tLBInsureAccSchema.setContNo(this.mNewContNo);
//            tLBInsureAccSchema.setPolNo(this.mNewPolNo);
//            tLBInsureAccSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBInsureAccSchema);
//            map.put(tLBInsureAccSchema, "INSERT");
//        }
//
//        //处理新保费项
//        LCInsureAccSet dealtInsureAccSet = new LCInsureAccSet();
//        for(int i = 1; i <= newLCInsureAccSet.size(); i++)
//        {
//            LCInsureAccSchema newLCInsureAccSchema = newLCInsureAccSet.get(i).getSchema();
//            newLCInsureAccSchema.setGrpContNo(tLCRnewStateLogSchema.getGrpContNo());
//            newLCInsureAccSchema.setGrpPolNo(tLCRnewStateLogSchema.getGrpPolNo());
//            newLCInsureAccSchema.setContNo(tLCRnewStateLogSchema.getContNo());
//            newLCInsureAccSchema.setPolNo(tLCRnewStateLogSchema.getPolNo());
//            newLCInsureAccSchema.setOperator(mGlobalInput.Operator);
//            newLCInsureAccSchema.setModifyDate(this.mCurrDate);
//            newLCInsureAccSchema.setModifyTime(this.mCurrTime);
//            dealtInsureAccSet.add(newLCInsureAccSchema);
//        }
//        map.put(dealtInsureAccSet, "INSERT");

        this.transferPolTableData("LCInsureAcc", "LBInsureAcc");
        if(isGrpCont)
        {
            return true;
        }

        String sql = "update LCInsureAcc "
                     + "set polNo = '" + mOldPolNo
                     + "', contNo = '" + mOldContNo
                     + "', grpPolNo = '" + mOldGrpPolNo
                     + "', grpContNo = '" + mOldGrpContNo + "', "
                     + mChgModiFyInfo
                     + "where polNo = '" + mNewPolNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LBInsureAcc "
              + "set insuAccBala = 0 "
              + ", polNo = '" + mNewPolNo
              + "', contNo = '" + mNewContNo
              + "', grpPolNo = '" + mNewGrpPolNo
              + "', grpContNo = '" + mNewGrpContNo + "', "
              + this.mChgDefaultInfo
              + "where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 转移帐户分类信息
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferInsureAccClass(
        LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
//        StringBuffer sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCInsureAccClass ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getPolNo())
//            .append("'  ");
//        System.out.println(sql.toString());
//        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
//        LCInsureAccClassSet tLCInsureAccClassSet
//            = tLCInsureAccClassDB.executeQuery(sql.toString());
//        map.put(tLCInsureAccClassSet, "DELETE");
//
//        sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCInsureAccClass ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getNewPolNo())
//            .append("' ");
//        System.out.println(sql.toString());
//        LCInsureAccClassSet newLCInsureAccClassSet
//            = tLCInsureAccClassDB.executeQuery(sql.toString());
//        map.put(newLCInsureAccClassSet, "DELETE");
//        if(tLCInsureAccClassSet == null || newLCInsureAccClassSet == null)
//        {
//            mErrors.addOneError("续保缴费项不完整。");
//            return false;
//        }
//
//        //转移到B表
//        for(int i = 1; i <= tLCInsureAccClassSet.size(); i++)
//        {
//            LBInsureAccClassSchema tLBInsureAccClassSchema
//                = new LBInsureAccClassSchema();
//            ref.transFields(tLBInsureAccClassSchema, tLCInsureAccClassSet.get(i));
//            tLBInsureAccClassSchema.setEdorNo(this.mEdorNo);
//            tLBInsureAccClassSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBInsureAccClassSchema.setGrpPolNo(this.mNewGrpPolNo);
//            tLBInsureAccClassSchema.setContNo(this.mNewContNo);
//            tLBInsureAccClassSchema.setPolNo(this.mNewPolNo);
//            tLBInsureAccClassSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBInsureAccClassSchema);
//            map.put(tLBInsureAccClassSchema, "INSERT");
//        }
//        //处理新保费项
//        LCInsureAccClassSet dealtLCInsureAccSet = new LCInsureAccClassSet();
//        for(int i = 1; i <= newLCInsureAccClassSet.size(); i++)
//        {
//            LCInsureAccClassSchema newLCInsureAccClassSchema
//                = newLCInsureAccClassSet.get(i).getSchema();
//            newLCInsureAccClassSchema.setGrpContNo(tLCRnewStateLogSchema.getGrpContNo());
//            newLCInsureAccClassSchema.setGrpPolNo(tLCRnewStateLogSchema.getGrpPolNo());
//            newLCInsureAccClassSchema.setContNo(tLCRnewStateLogSchema.getContNo());
//            newLCInsureAccClassSchema.setPolNo(tLCRnewStateLogSchema.getPolNo());
//            newLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
//            newLCInsureAccClassSchema.setModifyDate(this.mCurrDate);
//            newLCInsureAccClassSchema.setModifyTime(this.mCurrTime);
//            dealtLCInsureAccSet.add(newLCInsureAccClassSchema);
//        }
//        map.put(dealtLCInsureAccSet, "INSERT");

        this.transferPolTableData("LCInsureAccClass", "LBInsureAccClass");
        if(isGrpCont)
        {
            return true;
        }

        String sql = "update LCInsureAccClass "
                     + "set polNo = '" + mOldPolNo
                     + "', contNo = '" + mOldContNo
                     + "', grpPolNo = '" + mOldGrpPolNo
                     + "', grpContNo = '" + mOldGrpContNo + "', "
                     + mChgModiFyInfo
                     + "where polNo = '" + mNewPolNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LBInsureAccClass "
              + "set insuAccBala = 0 "
              + ",  polNo = '" + mNewPolNo
              + "', contNo = '" + mNewContNo
              + "', grpPolNo = '" + mNewGrpPolNo
              + "', grpContNo = '" + mNewGrpContNo + "', "
              + this.mChgDefaultInfo
              + "where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    private boolean transferInsureAccTrace(
        LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append("select * ")
            .append("from LCInsureAccTrace ")
            .append("where polNo = '")
            .append(tLCRnewStateLogSchema.getPolNo())
            .append("'  ");
        System.out.println(sqlBuffer.toString());
        LCInsureAccTraceSet oldLCInsureAccTraceSet
            = new LCInsureAccTraceDB().executeQuery(sqlBuffer.toString());
        double money = 0;
        for(int i = 1; i <= oldLCInsureAccTraceSet.size(); i++)
        {
            money += oldLCInsureAccTraceSet.get(i).getMoney();
        }
        if(oldLCInsureAccTraceSet.size() > 0)
        {
            LCInsureAccTraceSchema schema = new LCInsureAccTraceSchema();
            ref.transFields(schema, oldLCInsureAccTraceSet.get(1));
            schema.setOtherNo(this.mEdorNo);
            schema.setOtherType("3");
            String serNo = PubFun1.CreateMaxNo("SERIALNO", PubFun.getNoLimit(schema.getManageCom()));
                schema.setSerialNo(serNo);
            schema.setMoney( -money);
            schema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(schema);
            map.put(schema, SysConst.INSERT);
        }

//        map.put(oldLCInsureAccTraceSet, "DELETE");
//
//        sql = new StringBuffer();
//        sql.append("select * ")
//            .append("from LCInsureAccTrace ")
//            .append("where polNo = '")
//            .append(tLCRnewStateLogSchema.getNewPolNo())
//            .append("' ");
//        System.out.println(sql.toString());
//        LCInsureAccTraceSet newLCInsureAccTraceSet
//            = tLCInsureAccTraceDB.executeQuery(sql.toString());
//        map.put(newLCInsureAccTraceSet, "DELETE");
//        if(oldLCInsureAccTraceSet == null || newLCInsureAccTraceSet == null)
//        {
//            mErrors.addOneError("续保缴费项不完整。");
//            return false;
//        }
//
//        //转移到B表
//        for(int i = 1; i <= oldLCInsureAccTraceSet.size(); i++)
//        {
//            LBInsureAccTraceSchema tLBInsureAccTraceSchema
//                = new LBInsureAccTraceSchema();
//            ref.transFields(tLBInsureAccTraceSchema, oldLCInsureAccTraceSet.get(i));
//            tLBInsureAccTraceSchema.setEdorNo(this.mEdorNo);
//            tLBInsureAccTraceSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBInsureAccTraceSchema.setGrpPolNo(this.mNewGrpPolNo);
//            tLBInsureAccTraceSchema.setContNo(this.mNewContNo);
//            tLBInsureAccTraceSchema.setPolNo(this.mNewPolNo);
//            tLBInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBInsureAccTraceSchema);
//            map.put(tLBInsureAccTraceSchema, "INSERT");
//        }
//
//        LCInsureAccTraceSet dealtLCInsureAccSet = new LCInsureAccTraceSet();
//        for(int i = 1; i <= newLCInsureAccTraceSet.size(); i++)
//        {
//            LCInsureAccTraceSchema newLCInsureAccTraceSchema
//                = newLCInsureAccTraceSet.get(i).getSchema();
//            newLCInsureAccTraceSchema.setGrpContNo(tLCRnewStateLogSchema.getGrpContNo());
//            newLCInsureAccTraceSchema.setGrpPolNo(tLCRnewStateLogSchema.getGrpPolNo());
//            newLCInsureAccTraceSchema.setContNo(tLCRnewStateLogSchema.getContNo());
//            newLCInsureAccTraceSchema.setPolNo(tLCRnewStateLogSchema.getPolNo());
//            newLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
//            newLCInsureAccTraceSchema.setModifyDate(this.mCurrDate);
//            newLCInsureAccTraceSchema.setModifyTime(this.mCurrTime);
//            dealtLCInsureAccSet.add(newLCInsureAccTraceSchema);
//        }
//        map.put(dealtLCInsureAccSet, "INSERT");

        this.transferPolTableData("LCInsureAccTrace", "LBInsureAccTrace");
        if(isGrpCont)
        {
            return true;
        }

        String sql = "update LCInsureAccTrace "
                     + "set polNo = '" + mOldPolNo
                     + "', contNo = '" + mOldContNo
                     + "', grpPolNo = '" + mOldGrpPolNo
                     + "', grpContNo = '" + mOldGrpContNo + "', "
                     + mChgModiFyInfo
                     + "where polNo = '" + mNewPolNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LBInsureAccTrace "
              + "set polNo = '" + mNewPolNo
              + "', contNo = '" + mNewContNo
              + "', grpPolNo = '" + mNewGrpPolNo
              + "', grpContNo = '" + mNewGrpContNo + "', "
              + this.mChgDefaultInfo
              + "where polNo = '" + mOldPolNo + "' ";
        map.put(sql, "UPDATE");


        return true;
    }

    /**
     * 转移在续保中部需要直接处理的险种相关数据
     * 这些数据在续保中不会生成对应的C表数据，因此不存在新信息的换号问题，、
     * 只需要简单得备份并将备份数据换号
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferOtherPolInfo(
        LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
        //处理受益人信息----------------------
//        LCBnfDB tLCBnfDB = new LCBnfDB();
//        tLCBnfDB.setPolNo(tLCRnewStateLogSchema.getPolNo());
//        LCBnfSet tLCBnfSet = tLCBnfDB.query();
//
//        for(int i = 1; i <= tLCBnfSet.size(); i++)
//        {
//            LBBnfSchema tLBBnfSchema = new LBBnfSchema();
//            ref.transFields(tLBBnfSchema, tLCBnfSet.get(i));
//            tLBBnfSchema.setEdorNo(this.mEdorNo);
//            tLBBnfSchema.setContNo(this.mNewContNo);
//            tLBBnfSchema.setPolNo(this.mNewContNo);
//            tLBBnfSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBBnfSchema);
//            map.put(tLBBnfSchema, "INSERT");
//
//            tLCBnfSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCBnfSet.get(i).setModifyDate(this.mCurrDate);
//            tLCBnfSet.get(i).setModifyTime(this.mCurrTime);
//        }
//        map.put(tLCBnfSet, "UPDATE");

        String sql = "";
        this.copyPolTableDataToB("LCBnf", "LBBnf");
        if (!isGrpCont)
        {
            sql = "update LCBnf "
                  + "set "
                  + mChgModiFyInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
            sql = "update LBBnf "
                  + "set polNo = '" + mNewPolNo
                  + "', contNo = '" + mNewContNo + "', "
                  + this.mChgDefaultInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
        }




        //处理连带被保人-----------------
//        LCInsuredRelatedDB tLCInsuredRelatedDB = new LCInsuredRelatedDB();
//        tLCInsuredRelatedDB.setPolNo(tLCRnewStateLogSchema.getPolNo());
//        LCInsuredRelatedSet tLCInsuredRelatedSet = tLCInsuredRelatedDB.query();
//        for(int i = 1; i <= tLCInsuredRelatedSet.size(); i++)
//        {
//            LBInsuredRelatedSchema schema = new LBInsuredRelatedSchema();
//            ref.transFields(schema, tLCInsuredRelatedSet.get(i));
//            schema.setEdorNo(this.mEdorNo);
//            schema.setPolNo(this.mNewContNo);
//            schema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(schema);
//            map.put(schema, "INSERT");
//
//            tLCInsuredRelatedSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCInsuredRelatedSet.get(i).setModifyDate(this.mCurrDate);
//            tLCInsuredRelatedSet.get(i).setModifyTime(this.mCurrTime);
//        }
//        map.put(tLCInsuredRelatedSet, "UPDATE");

        this.copyPolTableDataToB("LCInsuredRelated", "LBInsuredRelated");
        if(!isGrpCont)
        {
            sql = "update LCInsuredRelated "
                  + "set "
                  + mChgModiFyInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
            sql = "update LBInsuredRelated "
                  + "set polNo = '" + mNewPolNo + "', "
                  + this.mChgDefaultInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
        }




        //处理保费项表和客户帐户表的关联信息----------------
//        LCPremToAccDB tLCPremToAccDB = new LCPremToAccDB();
//        tLCPremToAccDB.setPolNo(tLCRnewStateLogSchema.getPolNo());
//        LCPremToAccSet tLCPremToAccSet = tLCPremToAccDB.query();
//        for(int i = 1; i <= tLCPremToAccSet.size(); i++)
//        {
//            LBPremToAccSchema tLBPremToAccSchema = new LBPremToAccSchema();
//            ref.transFields(tLBPremToAccSchema, tLCPremToAccSet.get(i));
//            tLBPremToAccSchema.setEdorNo(this.mEdorNo);
//            tLBPremToAccSchema.setPolNo(this.mNewPolNo);
//            tLBPremToAccSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBPremToAccSchema);
//            map.put(tLBPremToAccSchema, "INSERT");
//
//            tLCPremToAccSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCPremToAccSet.get(i).setModifyDate(this.mCurrDate);
//            tLCPremToAccSet.get(i).setModifyTime(this.mCurrTime);
//        }
//        map.put(tLCPremToAccSet, "UPDATE");

        this.copyPolTableDataToB("LCPremToAcc", "LBPremToAcc");
        if(!isGrpCont)
        {
            sql = "update LCPremToAcc "
                  + "set "
                  + mChgModiFyInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
            sql = "update LBPremToAcc "
                  + "set polNo = '" + mNewPolNo + "', "
                  + this.mChgDefaultInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
        }

        //给付项表和客户帐户表的关联表------------------
//        LCGetToAccDB tLCGetToAccDB = new LCGetToAccDB();
//        tLCGetToAccDB.setPolNo(tLCRnewStateLogSchema.getPolNo());
//        LCGetToAccSet tLCGetToAccSet = tLCGetToAccDB.query();
//        for(int i = 1; i <= tLCGetToAccSet.size(); i++)
//        {
//            LBGetToAccSchema tLBGetToAccSchema = new LBGetToAccSchema();
//            ref.transFields(tLBGetToAccSchema, tLCGetToAccSet.get(i));
//            tLBGetToAccSchema.setEdorNo(this.mEdorNo);
//            tLBGetToAccSchema.setPolNo(this.mNewPolNo);
//            tLBGetToAccSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBGetToAccSchema);
//            map.put(tLBGetToAccSchema, "INSERT");
//
//            tLCGetToAccSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCGetToAccSet.get(i).setModifyDate(this.mCurrDate);
//            tLCGetToAccSet.get(i).setModifyTime(this.mCurrTime);
//        }
//        map.put(tLCGetToAccSet, "UPDATE");

        this.copyPolTableDataToB("LCGetToAcc", "LBGetToAcc");
        if(!isGrpCont)
        {
            sql = "update LCGetToAcc "
                  + "set "
                  + mChgModiFyInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
            sql = "update LBGetToAcc "
                  + "set polNo = '" + mNewPolNo + "', "
                  + this.mChgDefaultInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
        }



        //管理费怎么生成
        //保险帐户管理费表---------------------
//        LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
//        tLCInsureAccFeeDB.setPolNo(tLCRnewStateLogSchema.getPolNo());
//        LCInsureAccFeeSet tLCInsureAccFeeSet = tLCInsureAccFeeDB.query();
//        for(int i = 1; i <= tLCInsureAccFeeSet.size(); i++)
//        {
//            LBInsureAccFeeSchema tLBInsureAccFeeSchema = new LBInsureAccFeeSchema();
//            ref.transFields(tLBInsureAccFeeSchema, tLCInsureAccFeeSet.get(i));
//            tLBInsureAccFeeSchema.setEdorNo(this.mEdorNo);
//            tLBInsureAccFeeSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBInsureAccFeeSchema.setGrpPolNo(this.mNewGrpPolNo);
//            tLBInsureAccFeeSchema.setContNo(this.mNewContNo);
//            tLBInsureAccFeeSchema.setPolNo(this.mNewPolNo);
//            tLBInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBInsureAccFeeSchema);
//            map.put(tLBInsureAccFeeSchema, "INSERT");
//
//            tLCInsureAccFeeSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCInsureAccFeeSet.get(i).setModifyDate(this.mCurrDate);
//            tLCInsureAccFeeSet.get(i).setModifyTime(this.mCurrTime);
//        }
//        map.put(tLCInsureAccFeeSet, "UPDATE");

        this.copyPolTableDataToB("LCInsureAccFee", "LBInsureAccFee");
        if(!isGrpCont)
        {
            sql = "update LCInsureAccFee "
                  + "set "
                  + mChgModiFyInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
            sql = "update LBInsureAccFee "
                  + "set polNo = '" + mNewPolNo
                  + "', contNo = '" + mNewContNo
                  + "', grpPolNo = '" + mNewGrpPolNo
                  + "', grpContNo = '" + mNewGrpContNo + "', "
                  + this.mChgDefaultInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
        }



        //保险账户管理费分类表-------------------------
//        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
//        tLCInsureAccClassFeeDB.setPolNo(tLCRnewStateLogSchema.getPolNo());
//        LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
//        for(int i = 1; i <= tLCInsureAccClassFeeSet.size(); i++)
//        {
//            LBInsureAccClassFeeSchema tLBInsureAccClassFeeSchema
//                = new LBInsureAccClassFeeSchema();
//            ref.transFields(tLBInsureAccClassFeeSchema, tLCInsureAccClassFeeSet.get(i));
//            tLBInsureAccClassFeeSchema.setEdorNo(this.mEdorNo);
//            tLBInsureAccClassFeeSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBInsureAccClassFeeSchema.setGrpPolNo(this.mNewGrpPolNo);
//            tLBInsureAccClassFeeSchema.setContNo(this.mNewContNo);
//            tLBInsureAccClassFeeSchema.setPolNo(this.mNewPolNo);
//            tLBInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(tLBInsureAccClassFeeSchema);
//            map.put(tLBInsureAccClassFeeSchema, "INSERT");
//
//            tLCInsureAccClassFeeSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCInsureAccClassFeeSet.get(i).setModifyDate(this.mCurrDate);
//            tLCInsureAccClassFeeSet.get(i).setModifyTime(this.mCurrTime);
//        }
//            map.put(tLCInsureAccClassFeeSet, "UPDATE");

        this.copyPolTableDataToB("LCInsureAccClassFee", "LBInsureAccClassFee");
        if(!isGrpCont)
        {
            sql = "update LCInsureAccClassFee "
                  + "set "
                  + mChgModiFyInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
            sql = "update LBInsureAccClassFee "
                  + "set polNo = '" + mNewPolNo
                  + "', contNo = '" + mNewContNo
                  + "', grpPolNo = '" + mNewGrpPolNo
                  + "', grpContNo = '" + mNewGrpContNo + "', "
                  + this.mChgDefaultInfo
                  + "where polNo = '" + mOldPolNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
            map.put(sql, "UPDATE");
        }



        return true;
    }

    /**
     * 将续保状态改为已转移
     * @param <any> tLCRnewStateLogSchema
     * @return boolean
     */
    private boolean changeLogState()
    {
//        tLCRnewStateLogSchema.setState(XBConst.RNEWSTATE_DELIVERED);
//        map.put(tLCRnewStateLogSchema, "UPDATE");

        map.put("update LCRnewStateLog "
                + "set state = '" + XBConst.RNEWSTATE_DELIVERED + "', "
                + "   operator = '" + mGlobalInput.Operator + "', "
                + "   modifyDate = '" + this.mCurrDate + "', "
                + "   modifyTime = '" + this.mCurrTime + "' "
                + "where newContNo = '"
                + this.mNewContNo + "' ", "UPDATE");
        return true;
    }

    /**
     * 转移个单数据
     * @param tLCRnewStateLogSchema LCRnewStateLogSchema
     * @return boolean
     */
    private boolean transferContData(LCRnewStateLogSchema tLCRnewStateLogSchema)
    {
        //转移个单被保人信息
//        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
//        tLCInsuredDB.setContNo(tLCRnewStateLogSchema.getContNo());
//        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
//        for(int i = 1; i <= tLCInsuredSet.size(); i++)
//        {
//            LBInsuredSchema tLBInsuredSchema = new LBInsuredSchema();
//            ref.transFields(tLBInsuredSchema, tLCInsuredSet.get(i));
//            tLBInsuredSchema.setEdorNo(this.mEdorNo);
//            tLBInsuredSchema.setGrpContNo(this.mNewGrpContNo);
//            tLBInsuredSchema.setContNo(this.mNewContNo);
//            tLBInsuredSchema.setOperator(mGlobalInput.Operator);
//            PubFun.fillDefaultField(-tLBInsuredSchema);
//            map.put(tLBInsuredSchema, "INSERT");
//
//            tLCInsuredSet.get(i).setOperator(mGlobalInput.Operator);
//            tLCInsuredSet.get(i).setModifyDate(this.mCurrDate);
//            tLCInsuredSet.get(i).setModifyTime(this.mCurrTime);
//        }
//        map.put(tLCInsuredSet, "UPDATE");
        String sql;
        this.copyContTableDataToB("LCInsured", "LBInsured");
        if (!isGrpCont)
        {
            sql = "update LCInsured "
                         + "set "
                         + mChgModiFyInfo
                         + "where contNo = '" + mOldContNo + "' "; //在处理过程中没有生成新数据，所有使用mOldContNp
            map.put(sql, "UPDATE");
            sql = "update LBInsured "
                  + "set contNo = '" + mNewContNo
                  + "', grpContNo = '" + mNewGrpContNo + "', "
                  + this.mChgDefaultInfo
                  + "where contNo = '" + mOldContNo + "' "  //在处理过程中没有生成新数据，所有使用mOldPolNo
                  + "   and EdorNo like 'xb%' ";
            map.put(sql, "UPDATE");
        }

        //转移保单续保--------------------------------
//        LCContDB tLCContDB = new LCContDB();
//        tLCContDB.setContNo(tLCRnewStateLogSchema.getNewContNo());
//        if(!tLCContDB.getInfo())
//        {
//            mErrors.addOneError("没有取得续保后保单信息.");
//            return false;
//        }
//        LCContSchema newLCContSchema = tLCContDB.getSchema();
//        map.put(newLCContSchema, "DELETE");
//
//        tLCContDB.setContNo(tLCRnewStateLogSchema.getContNo());
//        if(!tLCContDB.getInfo())
//        {
//            mErrors.addOneError("没有取得续保前保单信息.");
//            return false;
//        }
//        LCContSchema oldLCContSchema = tLCContDB.getSchema();
//        map.put(oldLCContSchema.getSchema(), "DELETE");
//
//        //将新保单的号换成旧号
//        newLCContSchema.setGrpContNo(oldLCContSchema.getGrpContNo());
//        newLCContSchema.setContNo(oldLCContSchema.getContNo());
//        newLCContSchema.setProposalContNo(oldLCContSchema.getProposalContNo());
//        newLCContSchema.setAppFlag(com.sinosoft.lis.bq.BQ.APPFLAG_SIGN);
//        newLCContSchema.setOperator(mGlobalInput.Operator);
//        newLCContSchema.setModifyDate(PubFun.getCurrentDate());
//        newLCContSchema.setModifyTime(PubFun.getCurrentTime());
//        map.put(newLCContSchema.getSchema(), "INSERT");
//
//        //将原保单放到B表并换成新号码
//        LBContSchema tLBContSchema = new LBContSchema();  //原数据放到B表
//        ref.transFields(tLBContSchema, oldLCContSchema);
//        tLBContSchema.setEdorNo(this.mEdorNo);
//        tLBContSchema.setGrpContNo(mNewGrpContNo);
//        tLBContSchema.setContNo(mNewContNo);
//        tLBContSchema.setProposalContNo(mNewProposalContNo);
//        tLBContSchema.setOperator(mGlobalInput.Operator);
//        PubFun.fillDefaultField(tLBContSchema);
//        map.put(tLBContSchema, "INSERT");

        this.transferContTableData("LCCont", "LBCont");
        sql = "update LCCont "
              + "set contNo = '" + mOldContNo
              + "', appFlag = '" + com.sinosoft.lis.bq.BQ.APPFLAG_SIGN
              + "', StateFlag = '" + com.sinosoft.lis.bq.BQ.STATE_FLAG_SIGN
              + "', proposalContNo = '" + mOldProposalContNo
              + "', grpContNo = '" + mOldGrpContNo
              + "', prem = (select sum(prem) from LCPol where contNo = '" + mOldContNo + "') "
              + ",  sumPrem = (select sum(sumPrem) from LCPol where contNo = '" + mOldContNo + "') "
              + ",  amnt = (select sum(amnt) from LCPol where contNo = '" + mOldContNo + "') "
              + ", signDate = (select min(signDate) from LCPol where contNo = '" + mOldContNo + "') "
              + ", payToDate = (select max(payToDate) from LCPol where contNo = '" + mOldContNo + "') "
              + ", firstPayDate = (select min(firstPayDate) from LCPol where contNo = '" + mOldContNo + "') "
              + ", cValiDate = (select min(cValiDate) from LCPol where contNo = '" + mOldContNo + "') "
              + ", "
              + mChgModiFyInfo
              + "where contNo = '" + mNewContNo + "' ";
        map.put(sql, "UPDATE");
        sql = "update LBCont "
              + "set contNo = '" + mNewContNo
              + "', proposalContNo = '" + mNewProposalContNo
              + "', grpContNo = '" + mNewGrpContNo
              + "', StateFlag = '" + BQ.STATE_FLAG_TERMINATE + "', "
              + this.mChgDefaultInfo
              + "where contNo = '" + mOldContNo + "' "
              + "   and EdorNo like 'xb%' ";
        map.put(sql, "UPDATE");

        setContState(BQ.FILLDATA, BQ.FILLDATA, mLCContSchema.getCInValiDate());

        //客户告知--------------------------------
//       LCCustomerImpartDB tLCCustomerImpartDB = new LCCustomerImpartDB();
//       tLCCustomerImpartDB.setContNo(tLCRnewStateLogSchema.getContNo());
//       LCCustomerImpartSet tLCCustomerImpartSet = tLCCustomerImpartDB.query();
//       for(int i = 1; i <= tLCCustomerImpartSet.size(); i++)
//       {
//           LBCustomerImpartSchema tLBCustomerImpartSchema
//               = new LBCustomerImpartSchema();
//           ref.transFields(tLBCustomerImpartSchema, tLCCustomerImpartSet.get(i));
//           tLBCustomerImpartSchema.setEdorNo(this.mEdorNo);
//           tLBCustomerImpartSchema.setGrpContNo(this.mNewGrpContNo);
//           tLBCustomerImpartSchema.setContNo(this.mNewContNo);
//           tLBCustomerImpartSchema.setProposalContNo(this.mNewProposalContNo);
//           tLBCustomerImpartSchema.setOperator(mGlobalInput.Operator);
//           PubFun.fillDefaultField(tLBCustomerImpartSchema);
//           map.put(tLBCustomerImpartSchema, "INSERT");
//
//           tLCCustomerImpartSet.get(i).setOperator(mGlobalInput.Operator);
//           tLCCustomerImpartSet.get(i).setModifyDate(this.mCurrDate);
//           tLCCustomerImpartSet.get(i).setModifyTime(this.mCurrTime);
//       }
//       map.put(tLCCustomerImpartSet, "UPDATE");

        this.copyContTableDataToB("LCCustomerImpart", "LBCustomerImpart");
        sql = "update LCCustomerImpart "
              + "set "
              + mChgModiFyInfo
              + "where contNo = '" + mOldContNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
        map.put(sql, "UPDATE");
        sql = "update LBCustomerImpart "
              + "set contNo = '" + mNewContNo
              + "', grpContNo = '" + mNewGrpContNo
              + "', proposalContNo = '" + mNewProposalContNo + "', "
              + this.mChgDefaultInfo
              + "where contNo = '" + mOldContNo + "' "  //在处理过程中没有生成新数据，所有使用mOldPolNo
              + "   and EdorNo like 'xb%' ";
        map.put(sql, "UPDATE");


       //转移告知明细--------------------------------
//       LCCustomerImpartDetailDB tLCCustomerImpartDetailDB
//           = new LCCustomerImpartDetailDB();
//       tLCCustomerImpartDetailDB.setContNo(tLCRnewStateLogSchema.getContNo());
//       LCCustomerImpartDetailSet tLCCustomerImpartDetailSet
//           = tLCCustomerImpartDetailDB.query();
//       for(int i = 1; i <= tLCCustomerImpartDetailSet.size(); i++)
//       {
//           LBCustomerImpartDetailSchema tLBCustomerImpartDetailSchema
//               = new LBCustomerImpartDetailSchema();
//           ref.transFields(tLBCustomerImpartDetailSchema,
//                           tLCCustomerImpartDetailSet.get(i));
//           tLBCustomerImpartDetailSchema.setEdorNo(this.mEdorNo);
//           tLBCustomerImpartDetailSchema.setGrpContNo(this.mNewGrpContNo);
//           tLBCustomerImpartDetailSchema.setContNo(this.mNewContNo);
//           tLBCustomerImpartDetailSchema.setProposalContNo(this.mNewProposalContNo);
//           tLBCustomerImpartDetailSchema.setOperator(mGlobalInput.Operator);
//           PubFun.fillDefaultField(tLBCustomerImpartDetailSchema);
//           map.put(tLBCustomerImpartDetailSchema, "INSERT");
//
//           tLCCustomerImpartDetailSet.get(i).setOperator(mGlobalInput.Operator);
//           tLCCustomerImpartDetailSet.get(i).setModifyDate(this.mCurrDate);
//           tLCCustomerImpartDetailSet.get(i).setModifyTime(this.mCurrTime);
//       }
//       map.put(tLCCustomerImpartDetailSet, "UPDATE");

       this.copyContTableDataToB("LCCustomerImpartDetail", "LBCustomerImpartDetail");
       sql = "update LCCustomerImpartDetail "
             + "set "
             + mChgModiFyInfo
             + "where contNo = '" + mOldContNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
       map.put(sql, "UPDATE");
       sql = "update LBCustomerImpartDetail "
             + "set contNo = '" + mNewContNo
             + "', grpContNo = '" + mNewGrpContNo
             + "', proposalContNo = '" + mNewProposalContNo + "', "
             + this.mChgDefaultInfo
             + "where contNo = '" + mOldContNo + "' "  //在处理过程中没有生成新数据，所有使用mOldPolNo
              + "   and EdorNo like 'xb%' ";
       map.put(sql, "UPDATE");

       //转移客户告知参数-------------------------------
//       LCCustomerImpartParamsDB tLCCustomerImpartParamsDB
//           = new LCCustomerImpartParamsDB();
//       tLCCustomerImpartParamsDB.setContNo(tLCRnewStateLogSchema.getContNo());
//       LCCustomerImpartParamsSet tLCCustomerImpartParamsSet
//           = tLCCustomerImpartParamsDB.query();
//       for (int i = 1; i <= tLCCustomerImpartParamsSet.size(); i++)
//       {
//           LBCustomerImpartParamsSchema tLBCustomerImpartParamsSchema
//               = new LBCustomerImpartParamsSchema();
//           ref.transFields(tLBCustomerImpartParamsSchema,
//                           tLCCustomerImpartParamsSet.get(i));
//           tLBCustomerImpartParamsSchema.setEdorNo(this.mEdorNo);
//           tLBCustomerImpartParamsSchema.setGrpContNo(this.mNewGrpContNo);
//           tLBCustomerImpartParamsSchema.setContNo(this.mNewContNo);
//           tLBCustomerImpartParamsSchema.setProposalContNo(mNewProposalContNo);
//           tLBCustomerImpartParamsSchema.setOperator(mGlobalInput.Operator);
//           PubFun.fillDefaultField(tLBCustomerImpartParamsSchema);
//           map.put(tLBCustomerImpartParamsSchema, "INSERT");
//
//           tLCCustomerImpartParamsSet.get(i).setOperator(mGlobalInput.Operator);
//           tLCCustomerImpartParamsSet.get(i).setModifyDate(this.mCurrDate);
//           tLCCustomerImpartParamsSet.get(i).setModifyTime(this.mCurrTime);
//       }
//       map.put(tLCCustomerImpartParamsSet, "UPDATE");

//       this.copyContTableDataToB("LCCustomerImpartParams", "LBCustomerImpartParams");
       sql = "update LCCustomerImpartParams "
             + "set "
             + mChgModiFyInfo
             + "where contNo = '" + mOldContNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
       map.put(sql, "UPDATE");
       sql = "update LBCustomerImpartParams "
             + "set contNo = '" + mNewContNo
             + "', grpContNo = '" + mNewGrpContNo
             + "', proposalContNo = '" + mNewProposalContNo + "', "
             + this.mChgDefaultInfo
             + "where contNo = '" + mOldContNo + "' "  //在处理过程中没有生成新数据，所有使用mOldPolNo
              + "   and EdorNo like 'xb%' ";
       map.put(sql, "UPDATE");


       //个单投保人表----------------------------
//       LCAppntDB tLCAppntDB = new LCAppntDB();
//       tLCAppntDB.setContNo(tLCRnewStateLogSchema.getContNo());
//       LCAppntSet tLCAppntSet = tLCAppntDB.query();
//       for (int i = 1; i <= tLCAppntSet.size(); i++)
//       {
//           LBAppntSchema tLBAppntSchema = new LBAppntSchema();
//           ref.transFields(tLBAppntSchema, tLCAppntSet.get(i));
//           tLBAppntSchema.setEdorNo(this.mEdorNo);
//           tLBAppntSchema.setGrpContNo(this.mNewGrpContNo);
//           tLBAppntSchema.setContNo(this.mNewContNo);
//           tLBAppntSchema.setOperator(mGlobalInput.Operator);
//           PubFun.fillDefaultField(tLBAppntSchema);
//           map.put(tLBAppntSchema, "INSERT");
//
//           tLCAppntSet.get(i).setOperator(mGlobalInput.Operator);
//           tLCAppntSet.get(i).setModifyDate(this.mCurrDate);
//           tLCAppntSet.get(i).setModifyTime(this.mCurrTime);
//       }
//       map.put(tLCAppntSet, "UPDATE");

       this.copyContTableDataToB("LCAppnt", "LBAppnt");
       if (!isGrpCont)
       {
           sql = "update LCAppnt "
                 + "set "
                 + mChgModiFyInfo
                 + "where contNo = '" + mOldContNo + "' "; //在处理过程中没有生成新数据，所有使用mOldPolNo
           map.put(sql, "UPDATE");
           sql = "update LBAppnt "
                 + "set contNo = '" + mNewContNo
                 + "', grpContNo = '" + mNewGrpContNo + "', "
                 + this.mChgDefaultInfo
                 + "where contNo = '" + mOldContNo + "' "  //在处理过程中没有生成新数据，所有使用mOldPolNo
                 + "   and EdorNo like 'xb%' ";
           map.put(sql, "UPDATE");
       }

       return true;
    }

    /**
     * 设置是否团单标志
     * @param aIsGrpCont boolean: 团单true，个单false
     */
    public void setIsGrpCont(boolean aIsGrpCont)
    {
        this.isGrpCont = aIsGrpCont;
    }

    public static void main(String[] args)
    {
        String str = " select distinct ( select  contno from lccont a where a.contno = b.contno)"
                   + " ,(select  CInValiDate from lccont a where a.contno= b.newcontno) "
                   + " ,operator "
                   + ",( select  managecom from lccont a where a.contno = b.contno) "
                   + " from lcrnewstatelog b where  state='5' and contno='002016220000001' with ur "
                   ;


        SSRS tSSRS = (new ExeSQL()).execSQL(str);
       for (int i = 1; i <= tSSRS.getMaxRow(); i++)
       {
           LCContSchema schema = new LCContSchema();
           String contno = tSSRS.GetText(i, 1);
           String CInValiDate = tSSRS.GetText(i, 2);
           String Operator =  tSSRS.GetText(i,3);
           String ComCode  = tSSRS.GetText(i,4) ;

           schema.setContNo(contno);
           schema.setCInValiDate(CInValiDate);

//           LCContSchema schema = new LCContSchema();
//           schema.setContNo("00147473001");
//        schema.setCValiDate("select * From lccont where contno ='00099703901';");
//        schema.setCInValiDate("2008-11-3");

           GlobalInput gi = new GlobalInput();
//           gi.Operator = "server";
//           gi.ComCode = "86000000";
           gi.Operator = Operator;
           gi.ComCode = ComCode ;
           VData data = new VData();
           data.add(schema);
           data.add(gi);
           System.out.println(i);
           TransferCPDataToBP tTransferCPDataToBP = new TransferCPDataToBP();
           if (!tTransferCPDataToBP.submitData(data, "")) {
               System.out.println(tTransferCPDataToBP.mErrors.getErrContent());
           }
       }
    }
}
