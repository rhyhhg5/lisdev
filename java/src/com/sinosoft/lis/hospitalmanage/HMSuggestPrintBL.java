package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.*;

public class HMSuggestPrintBL {
     public CErrors mErrors = new CErrors();
     private String mOperate;
     private GlobalInput mG = new GlobalInput();
     public HMSuggestPrintBL() {}

     private LOPRTManagerSet mLOPRTManagerSet = new
             LOPRTManagerSet();
     private TransferData mPrintFactor = new TransferData();
     private HMSuggestSendTrackSet mHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
     private int mCount = 0;

     /**
      * 传输数据的公共方法
      * @param: cInputData 输入的数据
      *         cOperate 数据操作
      * @return:
      */
     public boolean submitData(VData cInputData, String cOperate) {
         cInputData = (VData) cInputData.clone();
         this.mOperate = cOperate;
         System.out.println(mOperate + "--HMSuggestPrintBL--");
         if (!getInputData(cInputData)) {
             return false;
         }
         if (!dealData()) {
             return false;
         }
         return true;
     }

     private boolean getInputData(VData cInputData) {
         mPrintFactor = (TransferData) cInputData.getObjectByObjectName(
                 					"TransferData", 0);
         mHMSuggestSendTrackSet = (HMSuggestSendTrackSet)cInputData.getObjectByObjectName(
        		 					"HMSuggestSendTrackSet", 0);
         mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                 "GlobalInput", 0));
         return true;
     }

     public int getCount() {
         return mCount;
     }

     /**
      * 数据操作类业务处理
      * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean dealData() {
         if (!prepareOutputData()) {
             return false;
         }
         mCount = mLOPRTManagerSet.size();
         VData tVData = new VData();
         tVData.add(mG);
         tVData.add(mLOPRTManagerSet);
         tVData.add(mHMSuggestSendTrackSet);
         PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
         if(!tPDFPrintBatchManagerBL.submitData(tVData,mOperate)){
              //mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
        	  mErrors.copyAllErrors(tPDFPrintBatchManagerBL.mErrors);
              return false;
         }
         return true;
     }

     private boolean prepareOutputData() {
         int count = mHMSuggestSendTrackSet.size();
         for (int i = 1; i <= count; i++) {
              LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
              String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
              String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
              tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
              tLOPRTManagerSchema.setOtherNo(mHMSuggestSendTrackSet.get(i).getCustomerNo());//存放客户号
              tLOPRTManagerSchema.setOtherNoType("HM");
              tLOPRTManagerSchema.setCode("HM001");
              tLOPRTManagerSchema.setManageCom(this.mG.ManageCom);
              tLOPRTManagerSchema.setAgentCode("");
              tLOPRTManagerSchema.setReqCom(this.mG.ManageCom);
              tLOPRTManagerSchema.setReqOperator(this.mG.Operator);
              tLOPRTManagerSchema.setPrtType("0");
              tLOPRTManagerSchema.setStateFlag("0");
              tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
              tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
              tLOPRTManagerSchema.setStandbyFlag1(mHMSuggestSendTrackSet.get(i).getBusinessType()); //存储业务类型
              tLOPRTManagerSchema.setStandbyFlag2(mHMSuggestSendTrackSet.get(i).getBusinessCode()); //存储业务代码
              
              mLOPRTManagerSet.add(tLOPRTManagerSchema);
         }

         if (mLOPRTManagerSet == null || mLOPRTManagerSet.size() <= 0) {
             buildError("HMSuggestPrintBL", "没有可以打印的数据！");
             return false;
         }
         return true;
     }


     public void buildError(String szFunc, String szErrMsg) {
         CError cError = new CError();
         cError.moduleName = "HMSuggestPrintBL";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         this.mErrors.addOneError(cError);
     }

     public static void main(String[] args) {

         TransferData PrintElement = new TransferData();
         PrintElement.setNameAndValue("RgtNo", "P1100070225000019");
         PrintElement.setNameAndValue("BatchType", "1");
         PrintElement.setNameAndValue("DetailPrt", "on");
         PrintElement.setNameAndValue("GrpDetailPrt", "on");
         PrintElement.setNameAndValue("NoticePrt", "off");

         GlobalInput tGlobalInput = new GlobalInput();
         tGlobalInput.ClientIP = "10.252.130.163";
         tGlobalInput.ManageCom = "86";
         tGlobalInput.Operator = "cm0001";

         VData aVData = new VData();
         aVData.add(tGlobalInput);
         aVData.add(PrintElement);

         HMSuggestPrintBL tHMSuggestPrintBL = new HMSuggestPrintBL();
         tHMSuggestPrintBL.submitData(aVData, "batch");


     }
}
