package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.message.*;
import java.util.*;

/**
 * <p>Title: 客户病例档案建立</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class HMCustomerAddDisDocTaskBL {
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private String mOperator = "";
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mLastDate = "";
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private HMCustDisDocInfoSet mHMCustDisDocInfoSet = new HMCustDisDocInfoSet();

    public HMCustomerAddDisDocTaskBL() {}

    private boolean getInputData(VData cInputData) {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = mG.ManageCom;
        mOperator = mG.Operator;
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) {
            return false;
        }
        if(!addDisDoc()) {
        	return false;
        }
        
        if(!submitMap()) {
        	return false;
        }
        return true;
    }

    private boolean addDisDoc() {
    	String sql = 
    		" select "
    		+" a.CustomerNo, "
    		+" a.CustomerName,"
    		+" b.caseno, "
    		+" b.DiseaseName, "
    		+" b.DiseaseCode, "
    		+" b.HospitalCode, "
    		+" b.HospitalName, "
    		+" (select value(sum(RealHospDate),0) from LLFeeMain where CaseNo = b.caseno) as HospDate, "
    		+" (select value(sum(SumFee),0) from LLFeeMain where CaseNo = b.caseno) as SumFee, "
    		+" (select value(sum(aa.RealPay),0) from LLClaimDetail aa,lmriskapp bb "
    			+" where aa.riskcode=bb.riskcode and bb.risktype1='1' and aa.CaseNo = b.caseno) as RealPay "
    	+" from HMCustDocInfo a, LLCaseCure b "
    	+" where a.CustomerNo = b.CustomerNo "
    	+" and not exists (select 1 "
    		+" from HMCustDisDocInfo "
    		+" where CustomerNo = a.CustomerNo "
    		+" and CaseNo = b.caseno) " 
    	+" and exists (select 1 from llcase q where q.caseno=b.caseno and q.rgtstate='12') with ur ";
    	SSRS tSSRS = new SSRS();
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRS = tExeSQL.execSQL(sql);
    	for(int i=1;i<=tSSRS.MaxRow;i++) {
		   //生成流水号
	       String tLimit = PubFun.getNoLimit(mManageCom);
	       String tSerialNo = "";
	       tSerialNo = PubFun1.CreateMaxNo("DISNO", tLimit);
	       System.out.println("**********tSerialNo=" + tSerialNo);
	       HMCustDisDocInfoSchema tHMCustDisDocInfoSchema = new HMCustDisDocInfoSchema();
	       
	       tHMCustDisDocInfoSchema.setSerialNo(tSerialNo);
	       tHMCustDisDocInfoSchema.setCustomerNo(tSSRS.GetText(i, 1));
	       tHMCustDisDocInfoSchema.setCustomerName(tSSRS.GetText(i, 2));
	       tHMCustDisDocInfoSchema.setCaseNo(tSSRS.GetText(i, 3));
	       tHMCustDisDocInfoSchema.setDiseaseName(tSSRS.GetText(i, 4));
	       tHMCustDisDocInfoSchema.setDiseaseNo(tSSRS.GetText(i, 5));
	       tHMCustDisDocInfoSchema.setHospitalNo(tSSRS.GetText(i, 6));
	       tHMCustDisDocInfoSchema.setHospitalName(tSSRS.GetText(i, 7));
	       tHMCustDisDocInfoSchema.setRealHospDate(tSSRS.GetText(i, 8));
	       tHMCustDisDocInfoSchema.setSumFee(tSSRS.GetText(i, 9));
	       tHMCustDisDocInfoSchema.setRealMedicinePay(tSSRS.GetText(i, 10));
	       tHMCustDisDocInfoSchema.setOperator(mOperator);
	       tHMCustDisDocInfoSchema.setManageCom(mManageCom);
	       tHMCustDisDocInfoSchema.setMakeDate(PubFun.getCurrentDate());
	       tHMCustDisDocInfoSchema.setMakeTime(PubFun.getCurrentTime());
	       tHMCustDisDocInfoSchema.setModifyDate(PubFun.getCurrentDate());
	       tHMCustDisDocInfoSchema.setModifyTime(PubFun.getCurrentTime());
	       
	       mHMCustDisDocInfoSet.add(tHMCustDisDocInfoSchema);
    	}
    	System.out.println("*****mHMCustDisDocInfoSet.size=" + mHMCustDisDocInfoSet.size());
    	if(mHMCustDisDocInfoSet.size() > 0) {
    		mMap.put(mHMCustDisDocInfoSet, "INSERT");
    	}
        
        return true;
    }

    /*数据处理*/
    private boolean submitMap() {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.clear();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, "")) {
            System.out.println("提交数据失败！");
            return false;
        }
        this.mMap = new MMap();
        return true;
    }

    public static void main(String[] args) {
    	 GlobalInput mGlobalInput = new GlobalInput();
         VData mVData = new VData();
         mGlobalInput.Operator = "001";
         mGlobalInput.ManageCom = "86";
         mVData.add(mGlobalInput);
         HMCustomerAddDisDocTaskBL tHMCustomerAddDisDocBL = new HMCustomerAddDisDocTaskBL();
         if (!tHMCustomerAddDisDocBL.submitData(mVData, "")) {
             System.out.println("客户病例档案建立失败！");
             
         }
    }
}
