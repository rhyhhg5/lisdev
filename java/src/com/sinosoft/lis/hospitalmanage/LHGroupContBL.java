/*
 * <p>ClassName: LHGroupContBL </p>
 * <p>Description:LHGroupContBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-17 18:32:42
 */
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LHGroupContBL 
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    String manage = "";

    /** 业务处理相关变量 */
    private LHGroupContSchema mLHGroupContSchema = new LHGroupContSchema();
    private LHContItemSet mLHContItemSet=new LHContItemSet();
    
    public LHGroupContBL() 
    {
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        System.out.println("******after get :bl***");
        //进行业务处理
        if (!dealData()) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHGroupContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHGroupContBL-->dealData!";
            mErrors.addOneError(tError);
            return false;
        }
        System.out.println("******after dealData :bl***");
        //准备往后台的数据
        if (!prepareOutputData()) 
        {
            return false;
        }
        
        System.out.println("******after prepareOutputData :bl***");
          
        if (this.mOperate.equals("QUERY||MAIN")) 
        {
            this.submitquery();
        } 
        else 
        {
            System.out.println("Start OLHGroupContBL Submit...");
            LHGroupContBLS tLHGroupContBLS = new LHGroupContBLS();
            tLHGroupContBLS.submitData(mInputData,mOperate);
            System.out.println("End OLHGroupContBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLHGroupContBLS.mErrors.needDealError()) 
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLHGroupContBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LHGroupContBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, mOperate);
        //this.mResult=mInputData;
        mInputData = null;
        return true;
    }

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() 
	{
        if (this.mOperate.equals("INSERT||MAIN")) 
        {
//          mLHGroupContSchema.setContraNo(PubFun1.CreateMaxNo("Contra", 9));
			mLHGroupContSchema.setOperator(mGlobalInput.Operator);
			mLHGroupContSchema.setMakeDate(PubFun.getCurrentDate());
			mLHGroupContSchema.setMakeTime(PubFun.getCurrentTime());
			mLHGroupContSchema.setModifyDate(PubFun.getCurrentDate());
			mLHGroupContSchema.setModifyTime(PubFun.getCurrentTime());
			mLHGroupContSchema.setManageCom(manage);
			map.put(mLHGroupContSchema, "INSERT");
			if (mLHContItemSet != null && mLHContItemSet.size() > 0) 
			{
				for (int i = 1; i <= mLHContItemSet.size(); i++) 
				{
					  mLHContItemSet.get(i).setContraNo(mLHGroupContSchema.
					          getContraNo());
					
					  mLHContItemSet.get(i).setContraItemNo(PubFun1.CreateMaxNo(
					          "ContraDuty", 9));
					
					  mLHContItemSet.get(i).setOperator(mGlobalInput.
					          Operator);
					  mLHContItemSet.get(i).setMakeDate(PubFun.
					          getCurrentDate());
					  mLHContItemSet.get(i).setMakeTime(PubFun.
					          getCurrentTime());
					  mLHContItemSet.get(i).setModifyDate(PubFun.
					          getCurrentDate());
					  mLHContItemSet.get(i).setModifyTime(PubFun.
					          getCurrentTime());
					  mLHContItemSet.get(i).setManageCom(manage);
					  System.out.println(i);
				}
				map.put(mLHContItemSet, "INSERT");
			}
			else
			{
				System.out.println("mLHContItemSet is null");
			}
        }

        if (this.mOperate.equals("UPDATE||MAIN")) 
        {

            mLHGroupContSchema.setOperator(mGlobalInput.Operator);
            System.out.println("-------UPDATE--------");
            mLHGroupContSchema.setOperator(mGlobalInput.Operator);
            mLHGroupContSchema.setMakeDate(PubFun.getCurrentDate());
            mLHGroupContSchema.setMakeTime(PubFun.getCurrentTime());
            mLHGroupContSchema.setModifyDate(PubFun.getCurrentDate());
            mLHGroupContSchema.setModifyTime(PubFun.getCurrentTime());
            mLHGroupContSchema.setManageCom(manage);

            map.put(mLHGroupContSchema, "DELETE&INSERT");

//          if (mLHContItemSet != null && mLHContItemSet.size() > 0) {

             for (int i = 1; i <= mLHContItemSet.size(); i++)
             {
                 if(mLHContItemSet.get(i).getContraItemNo() == null ||
                    mLHContItemSet.get(i).getContraItemNo().equals("") )
                 {
                     mLHContItemSet.get(i).setContraItemNo(PubFun1.CreateMaxNo(
                          "ContraDuty", 9));
                 }
                 mLHContItemSet.get(i).setOperator(mGlobalInput.Operator);
                 mLHContItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
                 mLHContItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
                 mLHContItemSet.get(i).setManageCom(manage);
                 mLHContItemSet.get(i).setMakeDate(PubFun.
                          getCurrentDate());
                  mLHContItemSet.get(i).setMakeTime(PubFun.
                          getCurrentTime());

             }
                 map.put(" delete from  LHContItem where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
                 map.put(mLHContItemSet, "INSERT");
//            }
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(" delete from  LHContServPrice where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
            map.put(" delete from  LHChargeBalance where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
            map.put(" delete from  LHContraAssoSetting where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
            map.put(" delete from  LDTestPriceMgt where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
            map.put(" delete from  LHUnitContra where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
            map.put(" delete from  lhcontitem where contrano='"+mLHGroupContSchema.getContraNo()+"'","DELETE");
            map.put(mLHGroupContSchema, "DELETE");
            map.put(mLHContItemSet, "DELETE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHGroupContSchema.setSchema((LHGroupContSchema)
                                           cInputData.
                                           getObjectByObjectName(
                "LHGroupContSchema", 0));
        this.mLHContItemSet.set((LHContItemSet) cInputData.
                                        getObjectByObjectName(
                                                "LHContItemSet", 0));

                            if (mLHContItemSet.size() < 1) {
                                CError tError = new CError();
                                tError.moduleName = "LHUnitContraBL";
                                tError.functionName = "submitData";
                                tError.errorMessage = "合同责任项目信息不许为空!";
                                this.mErrors.addOneError(tError);
                                return false;

                            }

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        if(mGlobalInput.ManageCom.length() == 8)
        {
            manage = mGlobalInput.ManageCom.substring(0,4);
        }
        else
        {
            manage = mGlobalInput.ManageCom;
        }
        for (int i = 1; i <= mLHContItemSet.size(); i++) {
          if (mLHContItemSet.get(i).getContraType() == null ||
              mLHContItemSet.get(i).getContraType().equals("") ||
              mLHContItemSet.get(i).getContraType().equals("null")) {
              CError tError = new CError();
              tError.moduleName = "LHUnitContraBL";
              tError.functionName = "submitData";
              tError.errorMessage = "您输入的合同责任项目类型不许为空!";
              this.mErrors.addOneError(tError);
              return false;
          }
      }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHGroupContDB tLHGroupContDB = new LHGroupContDB();
        tLHGroupContDB.setSchema(this.mLHGroupContSchema);
        //如果有需要处理的错误，则返回
        if (tLHGroupContDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHGroupContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHGroupContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHGroupContSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHGroupContSchema);
            mResult.add(this.mLHContItemSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() 
    {
        return this.mResult;
    }
    public static void main(String[] args) 
    {

        GlobalInput tG = new GlobalInput();
        tG.ManageCom="8600";
        tG.Operator="001";
        tG.ComCode="8600";

       LHGroupContSchema tLHGroupContSchema = new LHGroupContSchema();
       LHContItemSet tLHContItemSet = new LHContItemSet();
       LHGroupContUI tLHGroupContUI = new LHGroupContUI();
       System.out.println("***********begin init**************");
       //输出参数


       //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
      String transact = "INSERT||MAIN";
       tLHGroupContSchema.setContraNo("ContraNo");
       tLHGroupContSchema.setContraName("ContraName");

       tLHGroupContSchema.setIdiogrDate("IdiogrDate");
       tLHGroupContSchema.setContraBeginDate("ContraBeginDate");
       tLHGroupContSchema.setContraEndDate("ContraEndDate");
       tLHGroupContSchema.setContraState("ContraState");

       tLHGroupContSchema.setOperator("Operator");
       tLHGroupContSchema.setMakeDate("MakeDate");
       tLHGroupContSchema.setMakeTime("MakeTime");
       tLHGroupContSchema.setModifyDate("ModifyDate");
       tLHGroupContSchema.setModifyTime("ModifyTime");


       for (int i = 0; i < 3; i++) {
           LHContItemSchema tLHContItemSchema = new LHContItemSchema();
           tLHContItemSchema.setContraNo("343432");
           tLHContItemSchema.setContraItemNo("34");
           tLHContItemSchema.setDutyItemCode("01010");
           tLHContItemSchema.setDutyState("1");
    //       tLHContItemSchema.setDutyExolai("2dfesdf");
           tLHContItemSchema.setFeeExplai("dfdfesadsfa");

           tLHContItemSchema.setOperator("hm");
           tLHContItemSchema.setMakeDate("2004-05-05");
           tLHContItemSchema.setMakeTime("2004-05-05");
           tLHContItemSchema.setModifyDate("2004-05-05");
           tLHContItemSchema.setModifyTime("2004-05-05");

           tLHContItemSet.add(tLHContItemSchema);
       }
       System.out.println("***********end get data **************");

           // 准备传输数据 VData
           VData tVData = new VData();
           tVData.add(tLHGroupContSchema);
           tVData.add(tG);
           tVData.add(tLHContItemSet);
           tLHGroupContUI.submitData(tVData, transact);
           System.out.println("***********after submit data**************");


    }    
}
