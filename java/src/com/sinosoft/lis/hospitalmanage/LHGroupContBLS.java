/*
 * <p>ClassName: LHGroupContBLS </p>
 * <p>Description: LHGroupContBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-19 10:10:52
 */
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.utility.*;

public class LHGroupContBLS 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	//传输数据类
	private VData mInputData ;
	 /** 数据操作字符串 */
	private String mOperate;
	public LHGroupContBLS() 
	{
	}

    /**
	 传输数据的公共方法
	*/
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //将操作数据拷贝到本类中
	    this.mOperate =cOperate;
	          mInputData=(VData)cInputData.clone();
	    if(this.mOperate.equals("INSERT||MAIN"))
	    {
	    }
	    if (this.mOperate.equals("DELETE||MAIN"))
	    {
	    }
	    if (this.mOperate.equals("UPDATE||MAIN"))
	    {
	    }
	  return true;
	}


	public static void main(String[] args) 
	{
	}

}
