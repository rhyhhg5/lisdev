/**
 * <p>ClassName: HMDisDocUpDetailBL.java </p>
 * <p>Description: 添加客户档案明细 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author:chenxw
 * @version 1.0
 * @CreateDate：2010-3-8
 */
 
//包名
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class HMDisDocUpDetailBL  {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 提交数据的Map类 */
    private MMap map = new MMap();
    
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private TransferData mTransferData = new TransferData();
    
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    /** 数据操作字符串 */
    private String mOperator; //获取操作员代码
    private String mOperate;
    private String mManageCom;
    
    /** 业务操作类 */
    
    //客户档案信息
    HMCustDocInfoSchema mHMCustDocInfoSchema = new HMCustDocInfoSchema();
    //客户检查指标信息
    HMCustExamInfoSet mHMCustExamInfoSet = new HMCustExamInfoSet();

    public HMDisDocUpDetailBL()
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
            return false;

        //校验
        if (!checkData())
            return false;
        
        //进行业务处理
        if (!dealData())
            return false;

        if(!prepareOutputData())
            return false;

        PubSubmit tPubSubmit = new PubSubmit();

        if(!tPubSubmit.submitData(mInputData,""))
        {
            CError tError = new CError();
            tError.moduleName = "HMDisDocUpDetailBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据库提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        } 

        System.out.println("End Submit....");
        return true;
    }


    /**
     * dealData
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
    	//订单录入
    	if(mOperate.equals("INSERT||MAIN")) {  //保存
        	//判断是否已经存在客户文档，如果存在，则修改，否则进行插入
    		String tCustomerNo = mHMCustDocInfoSchema.getCustomerNo();
    		HMCustDocInfoSet tHMCustDocInfoSet = new HMCustDocInfoSet();
    		HMCustDocInfoSchema tHMCustDocInfoSchema = new HMCustDocInfoSchema();
    		HMCustDocInfoDB tHMCustDocInfoDB = new HMCustDocInfoDB();
    		tHMCustDocInfoDB.setCustomerNo(tCustomerNo);
    		//tHMCustDocInfoSet = tHMCustDocInfoDB.getInfo();
    		//不存在的情况
    		if(!tHMCustDocInfoDB.getInfo()) {
    			mHMCustDocInfoSchema.setOperator(mOperator);
    			mHMCustDocInfoSchema.setManageCom(mManageCom);
    			mHMCustDocInfoSchema.setMakeDate(PubFun.getCurrentDate());
    			mHMCustDocInfoSchema.setMakeTime(PubFun.getCurrentTime());
    			mHMCustDocInfoSchema.setModifyDate(PubFun.getCurrentDate());
    			mHMCustDocInfoSchema.setModifyTime(PubFun.getCurrentTime());
    			
    			map.put(mHMCustDocInfoSchema, "INSERT");
    			
    		//存在的情况
    		} else {
    			tHMCustDocInfoSchema = tHMCustDocInfoDB.getSchema();
    			
    			tHMCustDocInfoSchema.setPastDisHistory(mHMCustDocInfoSchema.getPastDisHistory());
    			tHMCustDocInfoSchema.setFamilyDisHistory(mHMCustDocInfoSchema.getFamilyDisHistory());
    			tHMCustDocInfoSchema.setBadHabit(mHMCustDocInfoSchema.getBadHabit());
    			tHMCustDocInfoSchema.setOperator(mOperator);
    			tHMCustDocInfoSchema.setManageCom(mManageCom);
    			tHMCustDocInfoSchema.setModifyDate(PubFun.getCurrentDate());
    			tHMCustDocInfoSchema.setModifyTime(PubFun.getCurrentTime());
    			
    			map.put(tHMCustDocInfoSchema, "UPDATE");
    		}
    		
    		//客户检查指标信息
    		int examInfoSize = mHMCustExamInfoSet.size();
    		String delExamSQL = " delete from HMCustExamInfo where CustomerNo='" + tCustomerNo + "'";
    		if(examInfoSize != 0) {
    			for(int i=1; i<=examInfoSize; i++ ) {
        			mHMCustExamInfoSet.get(i).setOperator(mOperator);
        			mHMCustExamInfoSet.get(i).setManageCom(mManageCom);
        			mHMCustExamInfoSet.get(i).setMakeDate(PubFun.getCurrentDate());
        			mHMCustExamInfoSet.get(i).setMakeTime(PubFun.getCurrentTime());
        			mHMCustExamInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
        			mHMCustExamInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
    			}
        		map.put(delExamSQL, "DELETE");
    			map.put(mHMCustExamInfoSet, "INSERT");
    		} else {
    			map.put(delExamSQL, "DELETE");
    		}
        } 
        
        return true;
    }

    /**
     * 将所需业务处理完的结果信息返回到前台
     * @param : cInputData  输入的数据
     * @return: mResult     返回的保存结果的容器
     */
    public VData getResult()
    {
        return this.mResult;
    }
    
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
    	 if (mGlobalInput == null)
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDocUpDetailBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         
         if (mOperator == null || mOperator.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDocUpDetailBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据Operate失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         if (mManageCom == null || mManageCom.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDocUpDetailBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据mManageCom失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         
         if (mOperate == null || mOperate.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDocUpDetailBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据mOperate失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
    	return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
    	 try
         {
             mInputData.clear();
             mInputData.add(map);
         }
         catch (Exception ex)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "HMDisDocUpDetailBL";
             tError.functionName = "prepareOutputData";
             tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
             this.mErrors.addOneError(tError);
             return false;
         }
         return true;
    }


    /**
     * getInputData
     * 得到前台传输的数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        
        mHMCustDocInfoSchema.setSchema((HMCustDocInfoSchema) cInputData.
                				getObjectByObjectName("HMCustDocInfoSchema",0));
        
        mHMCustExamInfoSet = (HMCustExamInfoSet) cInputData.getObjectByObjectName("HMCustExamInfoSet",0);
        
        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        //获取操作机构
        mManageCom = mGlobalInput.ManageCom;
        mOperate = cOperate;
        
        return true;
    }

}
