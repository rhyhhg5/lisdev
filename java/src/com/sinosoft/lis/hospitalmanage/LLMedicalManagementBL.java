package com.sinosoft.lis.hospitalmanage;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;


public class LLMedicalManagementBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors=new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData= new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LLMedicalManagementSchema mLLMedicalManagementSchema=new LLMedicalManagementSchema();

    public LLMedicalManagementBL()
    {
    }

   public boolean submitData(VData cInputData,String cOperate)
   {
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  this.mInputData = cInputData;
  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData(cInputData))
       return false;
	  //进行业务处理
	  if (!dealData())
	  {
	        // @@错误处理
	        CError tError = new CError();
	        tError.moduleName = "LLMedicalManagementBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败LLMedicalManagementBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	  }

	  String a= mLLMedicalManagementSchema.getHospitalCode();
	  String b= mLLMedicalManagementSchema.getCharger();
	  String c= mLLMedicalManagementSchema.getManageCom();
	  String e= mLLMedicalManagementSchema.getSerialNo();
	  //String d= "select SerialNo,ManageCom from LLMedicalManagement where HospitalCode = '"+a+"' and ManageCom = '"+c+"' and Charger='"+b+"'";
	  String d= "select SerialNo,ManageCom from LLMedicalManagement where SerialNo = '"+e+"'";
	  ExeSQL tExeSQL = new ExeSQL();

	  SSRS tTask = null;
	  tTask =  tExeSQL.execSQL(d);
	  System.out.println(tTask.getMaxRow());
	  if (this.mOperate.equals("INSERT||MAIN") && tTask.getMaxRow()!=0)
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LLMedicalManagementBL";
        tError.functionName = "submitData";
        tError.errorMessage = "该记录已存在!";

        this.mErrors.addOneError(tError);
        return false;
      }


  //准备往后台的数据
	  if (!prepareOutputData())
	  {
	    return false;
	  }
	  LLMedicalManagementBLS tLLMedicalManagementBLS = new LLMedicalManagementBLS();
	  tLLMedicalManagementBLS.submitData(mInputData, cOperate);
	  System.out.println("End LLMedicalManagementBLS Submit...");
	  
	  //如果有需要处理的错误，则返回
	  if (tLLMedicalManagementBLS.mErrors.needDealError())
	  {
	      // @@错误处理
	      this.mErrors.copyAllErrors(tLLMedicalManagementBLS.mErrors);
	      CError tError = new CError();
	      tError.moduleName = "LLMedicalManagementBLS";
	      tError.functionName = "submitDat";
	      tError.errorMessage = "数据提交失败!";
	      this.mErrors.addOneError(tError);
	      return false;
	  }
	  this.mResult.add(this.mLLMedicalManagementSchema);
	  
	  if (this.mOperate.equals("QUERY||MAIN"))
	  {
	    this.submitquery();
	  }
	  mInputData=null;
	  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if (this.mOperate.equals("INSERT||MAIN"))
 {
	  //mGobalInput.ManageCom
	  System.out.println("ManageCom="+mLLMedicalManagementSchema.getManageCom());
      String tLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
      String strSerialNo = PubFun1.CreateMaxNo("MEDICALNO", tLimit);
      System.out.println("SerialNo="+strSerialNo);
	  mLLMedicalManagementSchema.setSerialNo(strSerialNo);
	  mLLMedicalManagementSchema.setOperator(mGlobalInput.Operator);
      mLLMedicalManagementSchema.setMakeDate(PubFun.getCurrentDate());
      mLLMedicalManagementSchema.setMakeTime(PubFun.getCurrentTime());
      mLLMedicalManagementSchema.setModifyDate(PubFun.getCurrentDate());
      mLLMedicalManagementSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLLMedicalManagementSchema, "INSERT");
 }
 if (this.mOperate.equals("UPDATE||MAIN"))
 {
	System.out.println("---------------In Update BL---------------");
	System.out.println(	mLLMedicalManagementSchema.getSerialNo());
	LLMedicalManagementDB tLLMedicalManagementDB = new LLMedicalManagementDB();
	
	tLLMedicalManagementDB.setSerialNo(mLLMedicalManagementSchema.getSerialNo());
	if (!tLLMedicalManagementDB.getInfo())
	{
        CError tError = new CError();
        tError.moduleName = "LLMedicalManagementBL";
        tError.functionName = "submitData";
        tError.errorMessage = "模块出错!";
        this.mErrors.addOneError(tError);
        return false;
	}
	//LLMedicalManagementSchema tLLMedicalManagementSchema = new LLMedicalManagementSchema();
	//tLLMedicalManagementSchema 
	LLMedicalManagementSchema m_LLMedicalManagementSchema=new LLMedicalManagementSchema();
	m_LLMedicalManagementSchema=tLLMedicalManagementDB.getSchema();
	//mLLMedicalManagementSchema= tLLMedicalManagementDB.getSchema();
	mLLMedicalManagementSchema.setManageCom(mGlobalInput.ManageCom);
	mLLMedicalManagementSchema.setOperator(mGlobalInput.Operator);
	
	mLLMedicalManagementSchema.setModifyDate(PubFun.getCurrentDate());
	mLLMedicalManagementSchema.setModifyTime(PubFun.getCurrentTime());
	mLLMedicalManagementSchema.setMakeDate(m_LLMedicalManagementSchema.getMakeDate());
	mLLMedicalManagementSchema.setMakeTime(m_LLMedicalManagementSchema.getMakeTime());
    map.put(mLLMedicalManagementSchema, "UPDATE");
    System.out.println("--------------End Update BL--------------");
 }
 if (this.mOperate.equals("DELETE||MAIN"))
 {
   System.out.println("---------------In Delete---------------");
   map.put("delete from LLMedicalManagement where SerialNo='"+mLLMedicalManagementSchema.getSerialNo()+"'", "DELETE");
 }

  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
 return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
	return true;
}
/**
* 从输入数据中得到所有对象
*输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
*/
private boolean getInputData(VData cInputData)
{
       this.mLLMedicalManagementSchema.setSchema((LLMedicalManagementSchema)cInputData.getObjectByObjectName("LLMedicalManagementSchema",0));
       this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
       return true;
}

private boolean submitquery()
{
  this.mResult.clear();
  LLMedicalManagementDB tLLMedicalManagementDB=new LLMedicalManagementDB();
  tLLMedicalManagementDB.setSchema(this.mLLMedicalManagementSchema);
              //如果有需要处理的错误，则返回
              if (tLLMedicalManagementDB.mErrors.needDealError())
              {
                // @@错误处理
                      this.mErrors.copyAllErrors(tLLMedicalManagementDB.mErrors);
                      CError tError = new CError();
                      tError.moduleName = "LLMedicalManagementBL";
                      tError.functionName = "submitData";
                      tError.errorMessage = "数据提交失败!";
                      this.mErrors .addOneError(tError) ;
                      return false;
  }
  mInputData=null;
  return true;
}
private boolean prepareOutputData()
{
	  try
      {
              this.mInputData.clear();
              this.mInputData.add(this.mLLMedicalManagementSchema);
              mInputData.add(map);
              mResult.clear();
              mResult.add(this.mLLMedicalManagementSchema);
      }
      catch(Exception ex)
      {
              // @@错误处理
              CError tError =new CError();
              tError.moduleName="LLMedicalManagementBL";
              tError.functionName="prepareData";
              tError.errorMessage="在准备往后层处理所需要的数据时出错。";
              this.mErrors .addOneError(tError) ;
              return false;
      }
      return true;
 }
  public VData getResult()
  {
	  return this.mResult;
  }


}
