/*
 * <p>ClassName: HMCustomerMatch </p>
 * <p>Description: HMCustomerMatch类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 14:25:18
 */
package com.sinosoft.lis.hospitalmanage;

import java.io.File;
import org.apache.xpath.XPathAPI;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.llcase.SIXmlParser;
import com.sinosoft.lis.pubfun.*;

public class HMCustomerMatch 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();
	
	private MMap map = new MMap();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private TransferData outTransferData = new TransferData();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
    /** 保单信息在xml中的结点 */
    private static final String PARSE_PATH = "/DATASET";
    
    private static String PATH_INSURED = "ROW";  
      
    /** 配置文件名 */
    private String Config_File_Name = "HMCustomerImport.xml";
    
    private String[] mTitle = null;
    
	/** 数据操作字符串 */
	private String mFilePath;
	private String mFileName;
	private String mOutFileName;
	
	private String mOperate;
	//识别条件
	private String mMngCom;
	private String mTradeChannel;
	private String mHMRiskType ;
	private String mConditionSQL = "" ;
	private boolean mConFlag = false;

	public HMCustomerMatch() 
	{
	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) 
	{
		// 将操作数据拷贝到本类中
		mOperate = cOperate;
		mInputData = cInputData;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) 
		{
			return false;
		}
		// 进行业务处理
		if (!dealData()) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustomerMatch";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败HMCustomerMatch-->dealData!";
			mErrors.addOneError(tError);
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) 
		{
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start HMCustomerMatch Submit...");

		if (!tPubSubmit.submitData(mInputData, mOperate))
		{
			// @@错误处理
			mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "HMCustomerMatch";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);
			return false;
		}

		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) 
	{
		
        System.out.println("getInputData ......");
        
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        TransferData tTransferData = new TransferData();
        
        tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        mFilePath = (String) tTransferData.getValueByName("FilePath");
        mFileName = (String) tTransferData.getValueByName("FileName");
        mMngCom = (String) tTransferData.getValueByName("MngCom");
        mTradeChannel = (String) tTransferData.getValueByName("TradeChannel");
        mHMRiskType = (String) tTransferData.getValueByName("HMRiskType");
        
        System.out.println("mMngCom = "+mMngCom+" mTradeChannel="+mTradeChannel+"  mHMRiskType="+mHMRiskType);
        
        mConditionSQL = " and exists( select 'X' from lcpol b where b.contno = a.contno " ;
        
        if(mTradeChannel!=null && !"".equals(mTradeChannel))
        {
        	mTradeChannel = "0"+mTradeChannel;     	
        	mConditionSQL = mConditionSQL + " and b.SaleChnl='"+mTradeChannel+"' ";
        	mConFlag = true;
        }
        
        if("1".equals(mHMRiskType))
        {
        	mConditionSQL = mConditionSQL + " and exists ( select 'X' from lmriskapp c where b.riskcode = c.riskcode and risktype1='1' and riskprop='G')";
        	mConFlag = true;
        }
        
        if("2".equals(mHMRiskType))
        {
        	mConditionSQL = mConditionSQL + " and exists ( select 'X' from lmriskapp c where b.riskcode = c.riskcode and risktype1='1' and riskprop='I')";
        	mConFlag = true;
        }

        mConditionSQL = mConditionSQL+" ) ";
        
        
		return true;
	}
	
	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() 
	{

		//（1）获取导入文件
		String[] xmlFiles = importData();
		if(xmlFiles == null)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustomerMatch";
			tError.functionName = "dealData";
			tError.errorMessage = "解析导入文件出错！";
			mErrors.addOneError(tError);			
			return false;
		}
		
		//（2）数据校验并生成结果文件
		mOutFileName = outPutFile(xmlFiles[0]) ;
			
		return true;
	}
	
	/**
	 * 数据导入
	 * @return
	 */
	private String[] importData()
	{
		//解析Excel到xml
		String[] xmlFiles = parseVts(); 
        if (xmlFiles == null) 
        {
            return null;
        }

		return xmlFiles;
	}
	
	/**
	 * 生成比对后的文件信息
	 * @return
	 */    
    private String outPutFile(String xmlFileName) 
    {
		WriteToExcel t = new WriteToExcel("OUT_"+mFileName);
		t.createExcelFile();
		String[] sheetName = {"sheet1"};
		t.addSheet(sheetName);
		// t.setFontName("宋体");
		String[] title = {"序号", "姓名", "客户号", "性别","出生日期","证件类型","证件号码"};
		t.setTitle(0, title);

		try 
		{
            //解析客户信息
            XMLPathTool xmlPT = new XMLPathTool(xmlFileName);
            NodeList nodeList = xmlPT.parseN(PARSE_PATH);
            System.out.println("长度 ： " + nodeList.getLength());
            
            for (int i = 0; i < nodeList.getLength(); i++) 
            {
                Node node = nodeList.item(i);
                NodeList contNodeList = node.getChildNodes();
                System.out.println("长度 ： " + contNodeList.getLength());
                
                if (contNodeList.getLength() <= 0)
                    continue;
                
            	//过滤信息并将正确的信息写入excel
                System.out.println("HMCustomerMatch.outPutFile()--过滤信息并将正确的信息写入excel");
            	checkData(node,t);                 	

            }
            //解析完成删除xml临时文件
            File xmlFile = new File(xmlFileName);
            xmlFile.delete();
            
            t.write(mFilePath);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return "OUT_"+mFileName;
    }	
	
	/**
	 * 导入数据比对处理
	 * @return
	 */
	private boolean checkData(Node node,WriteToExcel t)
	{
        NodeList nodeList = null;
        String[] rowData  = null;
        int trueCount = 1;
        try 
        {
            nodeList = XPathAPI.selectNodeList(node, PATH_INSURED);
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            nodeList = null;
            return false;
        }
        int nLength = nodeList == null ? 0 : nodeList.getLength();
        
        for (int nIndex = 0; nIndex < nLength; nIndex++) 
        {
            Node nodeCase = nodeList.item(nIndex);
            //客户信息校验
            TransferData cTD = getValue(nodeCase);
            
            //确认客户信息
            String tSeq = "" + (String) cTD.getValueByName("Seq");
            String tCustomerName = "" + (String) cTD.getValueByName("CustomerName");
            
            String tSex = "" + (String) cTD.getValueByName("Sex");
            String tBirthDay = FormatDate((String) cTD.getValueByName("BirthDay")); 
            
            String tIDType = "" + (String) cTD.getValueByName("IDType");
            String tIDNo = "" + (String) cTD.getValueByName("IDNo");

            StringBuffer strSQL = new StringBuffer(" select distinct insuredno from lcinsured a where ");
            strSQL.append(" a.IDType = '");
            strSQL.append(tIDType);
            strSQL.append("' and IDNo = '");
            strSQL.append(tIDNo);
            strSQL.append("' and Birthday = '");
            strSQL.append(tBirthDay);
            strSQL.append("' and Sex='");
            strSQL.append(tSex);
            strSQL.append("' and Name = '");
            strSQL.append(tCustomerName);
            strSQL.append("' ");
            if((mMngCom !=null && !"".equals(mMngCom)))
            {
            	strSQL.append(" and managecom= '"+mMngCom+"' ");
            }            
            if(mConFlag)           	
            {
            	strSQL.append(mConditionSQL);
            }
            
            ExeSQL texesql = new ExeSQL();
            SSRS tssrs = new SSRS();
            
            tssrs = texesql.execSQL(strSQL.toString());
            
            System.out.println(strSQL.toString());
            
            if (tssrs != null && tssrs.getMaxRow() > 0) 
            {
            	rowData = new String[mTitle.length];
            	rowData[0] = tSeq;
            	rowData[1] = tCustomerName;
            	rowData[2] = tssrs.GetText(1, 1);
            	rowData[3] = tSex;
            	rowData[4] = tBirthDay;
            	rowData[5] = tIDType;
            	rowData[6] = tIDNo;
            	System.out.println(rowData[0]+"=="+rowData[1]+"=="+rowData[2]+"=="+rowData[3]+"=="+rowData[4]);
            	if(rowData != null)
            	{
            		t.setRowData(0, trueCount++, rowData);
            	}  
            } 
        }        
		return true;
	}

	/**
     * 解析excel并转换成多个xml文件
     * @return String[] 存放xml文件名的数组
     */
    private String[] parseVts() 
    {
    	HMXmlParser rgtvp = new HMXmlParser();
        rgtvp.setFileName(getFilePath());
        rgtvp.setConfigFileName(getConfigFilePath());
        //转换excel到xml
        if (!rgtvp.transform()) 
        {
            mErrors.copyAllErrors(rgtvp.mErrors);
            return null;
        }
        mTitle = rgtvp.getTitle();
        return rgtvp.getDataFiles();
    }
    
    /**
     * 得到Excel文件路径
     * @return String
     */
    private String getFilePath() 
    {
        return mFilePath + File.separator + mFileName;
    }

    /**
     * 得到配置文件路径
     * @return String
     */
    private String getConfigFilePath() 
    {
        return mFilePath + File.separator + Config_File_Name;
    }	
	
	
	private boolean prepareOutputData() 
	{
		try 
		{
			outTransferData.setNameAndValue("OutFileName", mOutFileName);
			mResult.clear();
			mResult.add(outTransferData);
		} 
		catch (Exception ex) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustomerMatch";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
    private TransferData getValue(Node nodeCase) 
    {
        TransferData td = new TransferData();
        for (int i = 0; i < mTitle.length; i++) 
        {
            System.out.println(mTitle[i]);
            String tvalue = "" + parseNode(nodeCase, mTitle[i]);
            td.setNameAndValue(mTitle[i], tvalue);
        }
        return td;
    }	
    /**
     * 利用XPathAPI取得某个节点的节点值
     * @param node Node
     * @param strPath String
     * @return String
     */
    private static String parseNode(Node node, String strPath) 
    {
        String strValue = "";
        try 
        {
            XObject xobj = XPathAPI.eval(node, strPath);
            strValue = xobj == null ? "" : xobj.toString();
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
        }
        return strValue;
    }
    
    private String FormatDate(String aDate) 
    {
        String rDate = "";
        rDate += aDate;
        if (rDate.equals("") || rDate.equals("null")) 
        {
            return "";
        }
        if (aDate.length() < 8) 
        {
            int days = 0;
            try 
            {
                days = Integer.parseInt(aDate) - 2;
            } 
            catch (Exception ex) 
            {
                return "";
            }
            rDate = PubFun.calDate("1900-1-1", days, "D", null);
        }
        return rDate;
    }    
    
    
	public VData getResult() 
	{
		return mResult;
	}   
    
	public static void main(String[] args) 
	{
		HMCustomerMatch tHMCustomerMatch = new HMCustomerMatch();
		//tHMCustomerMatch.outPutFile();
	}
}
