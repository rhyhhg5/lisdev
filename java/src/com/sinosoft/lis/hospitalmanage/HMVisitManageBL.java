/*
 * <p>ClassName: HMVisitManageBL </p>
 * <p>Description: HMVisitManageBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 14:25:18
 */
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.schema.HMVisitConditionSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.HMVisitConditionDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.*;

public class HMVisitManageBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private TransferData mTransferData = new TransferData();
	
	private MMap map = new MMap();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	/** 数据操作字符串 */
	private String mOperate;
	private String mManageCom;
	private String mOperType;
	
	/** 业务处理相关变量 */
	private HMVisitConditionSet mHMVisitConditionSet = new HMVisitConditionSet();

	private HMVisitConditionSet out_HMVisitConditionSet = new HMVisitConditionSet();
	
	private HMVisitInfoSet  mHMVisitInfoSet = new HMVisitInfoSet();
	
	public HMVisitManageBL() 
	{
	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) 
	{
		// 将操作数据拷贝到本类中
		mOperate = cOperate;
		mInputData = cInputData;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) 
		{
			return false;
		}
		// 进行业务处理
		if (!dealData()) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMVisitManageBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LDAreaHospitalBL-->dealData!";
			mErrors.addOneError(tError);
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) 
		{
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start HMVisitManageBL Submit...");

		if (!tPubSubmit.submitData(mInputData, mOperate))
		{
			// @@错误处理
			mErrors.copyAllErrors(tPubSubmit.mErrors);

			CError tError = new CError();
			tError.moduleName = "HMVisitManageBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			mErrors.addOneError(tError);
			return false;
		}

		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) 
	{
		
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		
		mHMVisitConditionSet = (HMVisitConditionSet) cInputData.getObjectByObjectName("HMVisitConditionSet", 0);
		mHMVisitInfoSet = (HMVisitInfoSet) cInputData.getObjectByObjectName("HMVisitInfoSet", 0);
		
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mOperType = (String)mTransferData.getValueByName("OperType");
		mManageCom =(String)mTransferData.getValueByName("MngCom");
			
		System.out.println("mOperType="+mOperType +" ;mManageCom="+mManageCom);
		
		if( !"01".equals(mOperType) && !"02".equals(mOperType))
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMVisitManageBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "业务操作类型缺失。";
			mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() 
	{		
		//保存操作
		if ("DELETE&&INSERT||MAIN".equals(mOperate)) 
		{			
			if(!updateDeal())
			{
				return false;
			}
		}
		
		//删除操作
		if ("DELETE||MAIN".equals(mOperate)) 
		{
			if(!deleDeal())
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	/**
	 * 保存操作
	 * @return
	 */
	private boolean updateDeal()
	{
		if("01".equals(mOperType))
		{				
			//01-探访条件操作；
			if(mHMVisitConditionSet.size()<1)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "HMVisitManageBL";
				tError.functionName = "dealData";
				tError.errorMessage = "探访条件操作数据缺失";
				mErrors.addOneError(tError);
				return false;
			}
			
			for(int i = 1 ;i<=mHMVisitConditionSet.size();i++)
			{
				String flowNo = mHMVisitConditionSet.get(i).getFlowNo();
														
				if((flowNo==null || "".equals(flowNo)))
				{
					//新增记录处理
					mHMVisitConditionSet.get(i).setFlowNo(PubFun1.CreateMaxNo("VisitCondition", 20));
					mHMVisitConditionSet.get(i).setMakeDate(PubFun.getCurrentDate());
					mHMVisitConditionSet.get(i).setMakeTime(PubFun.getCurrentTime());
					//mHMVisitConditionSet.get(i).setMngCom(mManageCom);
					mHMVisitConditionSet.get(i).setState("01"); //新增
					
					if(("86".equals(mManageCom)||"86000000".equals(mManageCom)))
					{
						//总公司新增记录，所有分公司同时添加
						LDComDB tLDComDB = new LDComDB();
						LDComSet tLDComSet  = tLDComDB.executeQuery(" select * from ldcom where 1 = 1 and char(length(trim(comcode)))='4' and comcode<>'8600'");
						if(tLDComSet !=null && tLDComSet.size()>0)
						{
							for (int j = 1;j<=tLDComSet.size();j++ )
							{
								HMVisitConditionSchema tHMVisitConditionSchema = new HMVisitConditionSchema();
								
					    		tHMVisitConditionSchema.setManageCom(tLDComSet.get(j).getComCode()+"0000");
					    		
					    		tHMVisitConditionSchema.setFlowNo(mHMVisitConditionSet.get(i).getFlowNo());
					    		
					    		tHMVisitConditionSchema.setMngCom(mHMVisitConditionSet.get(i).getManageCom()); //录入机构 
					    		tHMVisitConditionSchema.setRate(mHMVisitConditionSet.get(i).getRate());
					    		tHMVisitConditionSchema.setCondition(mHMVisitConditionSet.get(i).getCondition());
					    		
				    			tHMVisitConditionSchema.setMakeDate(PubFun.getCurrentDate());
				    			tHMVisitConditionSchema.setMakeTime(PubFun.getCurrentTime());	    		
					    		tHMVisitConditionSchema.setModifyDate(PubFun.getCurrentDate());
					    		tHMVisitConditionSchema.setModifyTime(PubFun.getCurrentTime());  
					    		
					    		tHMVisitConditionSchema.setOperator(mGlobalInput.Operator);
					    		tHMVisitConditionSchema.setState("01");
							
								out_HMVisitConditionSet.add(tHMVisitConditionSchema);
							}
						}
					}						
				}
				else
				{
					mHMVisitConditionSet.get(i).setState("02"); //修改
					//修改记录
					if(("86".equals(mManageCom)||"86000000".equals(mManageCom)))
					{
						HMVisitConditionDB tHMVisitConditionDB = new HMVisitConditionDB();
						
						StringBuffer srtSQL = new StringBuffer(" select * from HMVisitCondition where MngCom = '");
						srtSQL.append(mHMVisitConditionSet.get(i).getManageCom());
						srtSQL.append("' and State = '01' and MngCom<>ManageCom   and FlowNo = '"); //分公司未修改过
						srtSQL.append(mHMVisitConditionSet.get(i).getFlowNo());
						srtSQL.append("'");
						System.out.println(srtSQL.toString());
						HMVisitConditionSet ssHMVisitConditionSet = tHMVisitConditionDB.executeQuery(srtSQL.toString());
						
						if(ssHMVisitConditionSet!=null && ssHMVisitConditionSet.size()>0)
						{
							for(int j=1;j<=ssHMVisitConditionSet.size();j++)
							{
								ssHMVisitConditionSet.get(j).setCondition(mHMVisitConditionSet.get(i).getCondition());
								ssHMVisitConditionSet.get(j).setRate(mHMVisitConditionSet.get(i).getRate())	;
								ssHMVisitConditionSet.get(j).setModifyDate(PubFun.getCurrentDate());
								ssHMVisitConditionSet.get(j).setModifyTime(PubFun.getCurrentTime());									
							}
							out_HMVisitConditionSet.add(ssHMVisitConditionSet);
						}
					}
				}
				
				out_HMVisitConditionSet.add(mHMVisitConditionSet.get(i));					
			}
			
			map.put(out_HMVisitConditionSet, "DELETE&INSERT");					
		}
		else if("02".equals(mOperType))
		{
			//02探访记录操作
			if(mHMVisitInfoSet.size()<1)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "HMVisitManageBL";
				tError.functionName = "dealData";
				tError.errorMessage = "探访记录操作数据缺失";
				mErrors.addOneError(tError);
				return false;
			}				
			map.put(mHMVisitInfoSet, "DELETE&INSERT");	
		}		
		
		return true;
	}
	
	/**
	 * 删除操作
	 * @return
	 */
	private boolean deleDeal()
	{
		if("01".equals(mOperType))
		{				
			//01-探访条件操作；
			if(mHMVisitConditionSet.size()<1)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "HMVisitManageBL";
				tError.functionName = "dealData";
				tError.errorMessage = "探访条件操作数据缺失";
				mErrors.addOneError(tError);
				return false;
			}
			
			for(int i = 1 ;i<=mHMVisitConditionSet.size();i++)
			{
				
				out_HMVisitConditionSet.add(mHMVisitConditionSet.get(i));
				//总公司删除记录
				if(("86".equals(mManageCom)||"86000000".equals(mManageCom)))
				{
					HMVisitConditionDB tHMVisitConditionDB = new HMVisitConditionDB();
					
					StringBuffer srtSQL = new StringBuffer(" select * from HMVisitCondition where MngCom = '");
					srtSQL.append(mHMVisitConditionSet.get(i).getManageCom());
					srtSQL.append("' and State = '01' and MngCom<>ManageCom   and FlowNo = '"); //分公司未修改过
					srtSQL.append(mHMVisitConditionSet.get(i).getFlowNo());
					srtSQL.append("'");
					
					HMVisitConditionSet ssHMVisitConditionSet = tHMVisitConditionDB.executeQuery(srtSQL.toString());
					
					if(ssHMVisitConditionSet!=null && ssHMVisitConditionSet.size()>0)
					{
						out_HMVisitConditionSet.add(ssHMVisitConditionSet);
					}
				}
				map.put(out_HMVisitConditionSet, "DELETE");					
			}	
		}
		else if("02".equals(mOperType))
		{
			//02探访记录操作
			if(mHMVisitInfoSet.size()<1)
			{
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "HMVisitManageBL";
				tError.functionName = "dealData";
				tError.errorMessage = "探访记录操作数据缺失";
				mErrors.addOneError(tError);
				return false;
			}				
			map.put(mHMVisitInfoSet, "DELETE");	
		}			
		
		return true;
	}
	
	
	private boolean prepareOutputData() 
	{
		try 
		{
			mInputData.clear();
			mInputData.add(map);
			mResult.clear();
		} 
		catch (Exception ex) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMVisitManageBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() 
	{
		return this.mResult;
	}
	

	public static void main(String[] args) 
	{
	}
}
