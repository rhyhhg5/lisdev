package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.diskimport.DefaultDiskImporter;

/**
 * <p>
 * Title: 增加被保人磁盘导入类
 * </p>
 * <p>
 * Description: 把从磁盘导入的被保人清单添加到数据库
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author liuli
 * @version 1.0
 */

public class AddHospitalInfo 
{
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 全局变量 */
	private GlobalInput mGlobalInput;

	private VData mResult = new VData();

	/** 保全号 */
	private String mEdorNo = null;

	/** 批次号 */
	private String mBatchNo = null;

	/** 当前日期 */
	private String mCurrentDate = PubFun.getCurrentDate();

	/** 当前时间 */
	private String mCurrentTime = PubFun.getCurrentTime();

	private static final String schemaClassName = "com.sinosoft.lis.schema.LDHospitalSchema";

	private static final String schemaSetClassName = "com.sinosoft.lis.vschema.LDHospitalSet";

	/** 节点名 */
	private String sheetName = "Sheet1";

	/** 配置文件名 */
	private String configName = "HospitalInfoImport.xml";

	private LDHospitalSet mLDHospitalSet = new LDHospitalSet(); // 医院信息表

	private LDComHospitalSet mLDComHospitalSet = new LDComHospitalSet(); // 医院信息合作级别表

	/**
	 * 构造函数，新契约入口
	 * 
	 * @param GrpContNo
	 *            String
	 * @param gi
	 *            GlobalInputs
	 */
	public AddHospitalInfo(GlobalInput gi) 
	{
		this.mGlobalInput = gi;
	}

	/**
	 * 添加被保人清单
	 * 
	 * @param path
	 *            String
	 * @param fileName
	 *            String
	 */
	public boolean doAdd(String path, String fileName) 
	{
		// 处理批次号：批次号码为文件名（不含扩展名）
		// mBatchNo = fileName.substring(0, fileName.lastIndexOf("."));

		// 从磁盘导入数据
		DefaultDiskImporter importFile = new DefaultDiskImporter(path
				+ fileName, path + configName, sheetName);
		importFile.setSchemaClassName(schemaClassName);
		importFile.setSchemaSetClassName(schemaSetClassName);
		
		if (!importFile.doImport()) 
		{
			mErrors.copyAllErrors(importFile.mErrors);
			return false;
		}

		LDHospitalSet tLDHospitalSet = (LDHospitalSet) importFile.getSchemaSet();

		// 校验导入的数据
		if (!checkData(tLDHospitalSet)) 
		{
			return false;
		}

		// 存放Insert Into语句的容器
		MMap map = new MMap();
		for (int i = 1; i <= tLDHospitalSet.size(); i++) 
		{
			addOneHospital(tLDHospitalSet.get(i));
		}
		map.put(mLDHospitalSet, "INSERT");
		map.put(mLDComHospitalSet, "INSERT");
		this.mResult.add(mLDHospitalSet);
		// 提交数据到数据库
		if (!submitData(map)) 
		{
			return false;
		}

		return true;
	}

	private void deletePayList(MMap map) 
	{

	}

	/**
	 * 校验导入数据
	 * 
	 * @param cLDHospitalSet
	 *            LDHospitalSetSet
	 * @return boolean
	 */
	private boolean checkData(LDHospitalSet cLDHospitalSet) 
	{
		for (int i = 1; i <= cLDHospitalSet.size(); i++) 
		{
			LDHospitalSchema schema = cLDHospitalSet.get(i);
			String strAreaCode = schema.getAreaCode();
			if (strAreaCode == null || strAreaCode.equals("")) 
			{
				mErrors.addOneError("缺少地区编码！");
				return false;
			}
			String strHospitName = schema.getHospitName();
			if ((strHospitName == null) || (strHospitName.equals(""))) 
			{
				mErrors.addOneError("缺少医院名称！");
				return false;
			}

			String strManageCom = schema.getManageCom();
			if ((strManageCom == null) || (strManageCom.equals(""))) 
			{
				mErrors.addOneError("缺少管理机构！");
				return false;
			}
			//校验导入医院是否重复
			String sql = " select count(1) from LDHospital where 1=1 "
					+" and ManageCom = '"+ strManageCom +"' "
					+" and (Phone = '"+ schema.getPhone()+"' or Address='"+schema.getAddress()+"' or HospitName = '"+schema.getHospitName()+"')" ;
			ExeSQL tExeSQL = new ExeSQL();
			String count = tExeSQL.getOneValue(sql);
			if(Integer.parseInt(count)>=1)
			{
				mErrors.addOneError(schema.getHospitName()+"在系统中已经存在!");
				return false;
			}
		}
		return true;
	}

	/**
	 * 处理医院信息数据
	 * 
	 * @param map
	 *            MMap
	 * @param LDHospitalSchema
	 *            aLDHospitalSchema
	 */
	private void addOneHospital(LDHospitalSchema cLDHospitalSchema) 
	{

		LDHospitalSchema tLDHospitalSchema = cLDHospitalSchema;

		String HospitCode = new SysMaxNoPicch().CreateMaxNo("HOSOITALINFO",
				tLDHospitalSchema.getAreaCode(), 3); // 生成区域号码＋3位流水号
		tLDHospitalSchema.setHospitCode(tLDHospitalSchema.getAreaCode()
				+ HospitCode);
		String sqlName = "select codename from ldcode where codetype='hmareacode' and code='"
				+ tLDHospitalSchema.getAreaCode() + "' with ur";
		String name = new ExeSQL().getOneValue(sqlName);
		tLDHospitalSchema.setAreaName(name); // 地区名称
		tLDHospitalSchema.setHospitalType("1"); // 模版导入类型默认为医疗机构
		String HospitalStandardCode = "";
		
		if (tLDHospitalSchema.getHospitCode() != null) 
		{
			HospitalStandardCode = tLDHospitalSchema.getHospitCode();
		}
		if (tLDHospitalSchema.getCommunFixFlag() != null) 
		{
			HospitalStandardCode += tLDHospitalSchema.getCommunFixFlag();
		}
		if (tLDHospitalSchema.getAdminiSortCode() != null) 
		{
			HospitalStandardCode += tLDHospitalSchema.getAdminiSortCode();
		}
		if (tLDHospitalSchema.getEconomElemenCode() != null) 
		{
			HospitalStandardCode += tLDHospitalSchema.getEconomElemenCode();
		}
		if (tLDHospitalSchema.getLevelCode() != null) 
		{
			HospitalStandardCode += tLDHospitalSchema.getLevelCode();
		}
		if (tLDHospitalSchema.getBusiTypeCode() != null) 
		{
			HospitalStandardCode += tLDHospitalSchema.getBusiTypeCode();
		}
		tLDHospitalSchema.setHospitalStandardCode(HospitalStandardCode); // 医院完全码
		tLDHospitalSchema.setOperator(mGlobalInput.Operator);
		tLDHospitalSchema.setMakeDate(PubFun.getCurrentDate());
		tLDHospitalSchema.setMakeTime(PubFun.getCurrentTime());
		tLDHospitalSchema.setModifyDate(PubFun.getCurrentDate());
		tLDHospitalSchema.setModifyTime(PubFun.getCurrentTime());
		mLDHospitalSet.add(tLDHospitalSchema);
		// 推荐医院信息记录表,记录合作级别信息
		LDComHospitalSchema tLDComHospitalSchema = new LDComHospitalSchema();
		tLDComHospitalSchema.setSerialNo(PubFun1.CreateMaxNo("ComHospital", 9));
		tLDComHospitalSchema.setHospitCode(tLDHospitalSchema.getHospitCode());
		if (tLDHospitalSchema.getAssociateClass().equals("1")) 
		{
			tLDComHospitalSchema.setAssociatClass("推荐医院"); // 合同级别
		} 
		else if (tLDHospitalSchema.getAssociateClass().equals("2")) 
		{
			tLDComHospitalSchema.setAssociatClass("指定医院"); // 合同级别
		} 
		else 
		{
			tLDComHospitalSchema.setAssociatClass("非指定医院"); // 合同级别
		}
		tLDComHospitalSchema.setStartDate(PubFun.getCurrentDate()); // 合作级别开始日期
		tLDComHospitalSchema.setOperator(mGlobalInput.Operator);
		tLDComHospitalSchema.setMakeDate(PubFun.getCurrentDate());
		tLDComHospitalSchema.setMakeTime(PubFun.getCurrentTime());
		tLDComHospitalSchema.setModifyDate(PubFun.getCurrentDate());
		tLDComHospitalSchema.setModifyTime(PubFun.getCurrentTime());

		mLDComHospitalSet.add(tLDComHospitalSchema);
	}

	/**
	 * 提交数据到数据库
	 * 
	 * @param map
	 *            MMap
	 * @return boolean
	 */
	private boolean submitData(MMap map) 
	{
		VData data = new VData();
		data.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(data, "")) 
		{
			mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 返回执行结果
	 * 
	 * @return VData
	 */
	public VData getResult() 
	{
		return this.mResult;
	}

	/**
	 * 主函数，测试用
	 * 
	 * @param args
	 *            String[]
	 */
	public static void main(String[] args) 
	{
		GlobalInput tGI = new GlobalInput();
		tGI.ManageCom = "86";
		tGI.Operator = "endor";
		String path = "d:\\";
		String fileName = "JSBJ2006070601.xls";
		path = "D:\\work\\UI\\temp\\";
		String config = "D:\\work\\UI\\temp\\CertifyPayDiskImport.xml";
		AddHospitalInfo tAddHospitalInfo = new AddHospitalInfo(tGI);
		if (!tAddHospitalInfo.doAdd(path, fileName)) {
			System.out.println(tAddHospitalInfo.mErrors.getFirstError());
			System.out.println("磁盘导入失败！");
		}
	}
}
