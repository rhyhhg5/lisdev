package com.sinosoft.lis.hospitalmanage;

/**
 * <p>Title: HMSuggestPrintServiceBL</p>
 * <p>Description: 定点医院管理中健康建议打印</p>
 * <p>Copyright: Copyright sinosoft (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author chenxw
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LLHospGetDB;
import com.sinosoft.lis.f1print.PayColPrtBL;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOBatchPRTManagerSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class HMSuggestPrintServiceBL implements PrintService {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    private String mOperate;
    // 打印次数或打印方式（即时打印or批量打印）
    private String mdirect = "";
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    // 业务处理相关变量
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mCustomerNo = "";
    private String mBusinessType = "";
    private String mBusinessCode = "";
    

    private String mflag = null;

    private int mPrintTimes = 0;

    public HMSuggestPrintServiceBL() {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        mflag = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        // 全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema tLOPRTManagerSchema = 
        			(LOPRTManagerSchema)cInputData.getObjectByObjectName("LOPRTManagerSchema", 0);
        
        if (tLOPRTManagerSchema.getOtherNo() != null) {
            mCustomerNo = tLOPRTManagerSchema.getOtherNo();
            mBusinessType = tLOPRTManagerSchema.getStandbyFlag1();
            mBusinessCode = tLOPRTManagerSchema.getStandbyFlag2();
        } else {
            buildError("getInputData", "没有指定打印数据！");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean getPrintData() {

        TextTag tTextTag = new TextTag(); // 新建一个TextTag的实例
        //收件人
        String tRecipient = "";
        String tCustZipCode = "";
        String tCustAddress = "";
        String tPhone = "";
        
        //公司信息
        String tCompany = "";
        String tCoAddress = "";
        String tCoPhone = "";
        String tCoFax = "";
        
        //被保险人信息
//        String tOperatorName = "";
//        String tOperator = "";
//        String tOperatorDept = "";
//        String tOperatorPhone = "";
        
        //健康建议
        String tSuggest = "";

        if (mCustomerNo == null || "".equals(mCustomerNo) || "null".equals(mCustomerNo)) {
            buildError("CustomerNo", "客户号不能为空！");
            return false;
        }
        
        //客户基本信息
        String custInfoSQL = " select distinct a.name,a.zipcode,a.homeaddress,a.mobile "
        			+" from HMSENDCUSTDETAIL a "
        			+" where a.insuredno='" + mCustomerNo + "' ";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(custInfoSQL);
        if(tSSRS.MaxRow >0) {
        	tRecipient = tSSRS.GetText(1, 1);
        	tCustZipCode = tSSRS.GetText(1, 2);
        	tCustAddress = tSSRS.GetText(1, 3);
        	tPhone = tSSRS.GetText(1, 4);
        }
        
        //公司信息
        String tCoInfoSQL = " select letterservicename,address,phone,fax "
        						+ " from ldcom where comcode='" + mGlobalInput.ManageCom + "'";
        tSSRS = new ExeSQL().execSQL(tCoInfoSQL);
        if(tSSRS.MaxRow >0) {
        	tCompany = tSSRS.GetText(1, 1);
        	tCoAddress = tSSRS.GetText(1, 2);
        	tCoPhone = tSSRS.GetText(1, 3);
        	tCoFax = tSSRS.GetText(1, 4);
        }
        
        //健康建议
        String tSuggestSQL = " select HealthSuggest from HMSuggestInfo "
        				+" where BusinessCode='" + mBusinessCode + "'"
        				+" and BusinessType ='" + mBusinessType + "'";
        tSuggest = new ExeSQL().getOneValue(tSuggestSQL);
        
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                          mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        tTextTag.add("JetFormType", "HM001");
         if (comcode.equals("86") || comcode.equals("8600")||comcode.equals("86000000")) {
            comcode = "86";
             } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
          } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
          }
          String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"+comcode+"' with ur";
          String printcode = new ExeSQL().getOneValue(printcom);

          tTextTag.add("ManageComLength4", printcode);
          tTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.","_"));
        if(mflag.equals("batch")){
        	tTextTag.add("previewflag", "0");
        }else{
        	tTextTag.add("previewflag", "1");
        }
        
        tTextTag.add("Recipient", tRecipient); //收件人
        tTextTag.add("CustZipCode", tCustZipCode); //邮政编码
        tTextTag.add("CustAddress", tCustAddress); //地址
        tTextTag.add("Company", tCompany); //公司名称
        tTextTag.add("CoAddress", tCoAddress); //公司地址
        tTextTag.add("CoPhone", tCoPhone); //公司名称
        tTextTag.add("CoFax", tCoFax); //公司传真
        
        tTextTag.add("InsName", tRecipient); //被保险人
        tTextTag.add("InsPhone", tPhone); //被保险人联系电话
        
        tTextTag.add("TextName",tRecipient); //正文姓名
        tTextTag.add("TextDate",PubFun.getCurrentDate()); //正文日期
        
        tTextTag.add("Suggest", tSuggest); //健康建议
        tTextTag.add("SuggestDate", 
        				PubFun.getCurrentDate().replaceFirst("-", "年").replaceFirst("-", "月") + "日"); //本建议书生成日期

        XmlExport tXmlExport = new XmlExport();
        tXmlExport.createDocument("HMSuggestPrint", "");

        if (tTextTag.size() > 0) {
            tXmlExport.addTextTag(tTextTag);
        }
        tXmlExport.outputDocumentToFile("e:\\", "testHM"); // 输出xml文档到文件

        mResult.clear();
        mResult.addElement(mLOPRTManagerSchema);
        mResult.addElement(tXmlExport);

        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        PayColPrtBL tPayColPrtBL = new PayColPrtBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "cm0001";
        tVData.addElement(tGlobalInput);
        LOBatchPRTManagerSchema tLOBatchPRTManagerSchema = new
                LOBatchPRTManagerSchema();
        tLOBatchPRTManagerSchema.setOtherNo("P1100070201000013");
        tLOBatchPRTManagerSchema.setStandbyFlag2("1");
        tVData.addElement(tLOBatchPRTManagerSchema);
        tPayColPrtBL.submitData(tVData, "PRINT");
    }

}
