/*
 * <p>ClassName: LHContServPriceBL </p>
 * <p>Description: LHContServPriceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-09 15:00:38
 */
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LHContServPriceBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	private String tNo;

	/** 业务处理相关变量 */
	private LHContServPriceSchema mLHContServPriceSchema = new LHContServPriceSchema();

	// private LHContServPriceSet mLHContServPriceSet=new LHContServPriceSet();
	private LDTestPriceMgtSet mLDTestPriceMgtSet = new LDTestPriceMgtSet();

	public LHContServPriceBL() 
	{

	}

	public static void main(String[] args) 
	{
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) 
	{
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
			return false;
		// 进行业务处理
		if (!dealData()) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHContServPriceBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LHContServPriceBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData())
			return false;
		if (this.mOperate.equals("QUERY||MAIN")) 
		{
			this.submitquery();
		} 
		else 
		{
			PubSubmit tPubSubmit = new PubSubmit();
			System.out.println("Start LHContServPriceBL Submit...");
			// LHContServPriceBLS tLHContServPriceBLS=new LHContServPriceBLS();
			// tLHContServPriceBLS.submitData(mInputData,mOperate);
			if (!tPubSubmit.submitData(mInputData, mOperate)) 
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);

				CError tError = new CError();
				tError.moduleName = "LHContServPriceBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}

			System.out.println("End LHContServPriceBL Submit...");
			// 如果有需要处理的错误，则返回
			// if (tLHContServPriceBLS.mErrors.needDealError())
			// {
			// @@错误处理
			// this.mErrors.copyAllErrors(tLHContServPriceBLS.mErrors);
			// CError tError = new CError();
			// tError.moduleName = "LHContServPriceBL";
			// tError.functionName = "submitDat";
			// tError.errorMessage ="数据提交失败!";
			// this.mErrors .addOneError(tError) ;
			// return false;
			// }
		}
		mInputData = null;
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (this.mOperate.equals("INSERT||MAIN")) 
		{

			System.out.println("SSSSSSSSSSSSSSSS ");
			mLHContServPriceSchema.setOperator(mGlobalInput.Operator);
			mLHContServPriceSchema.setMakeDate(PubFun.getCurrentDate());
			mLHContServPriceSchema.setMakeTime(PubFun.getCurrentTime());
			mLHContServPriceSchema.setModifyDate(PubFun.getCurrentDate());
			mLHContServPriceSchema.setModifyTime(PubFun.getCurrentTime());
			mLHContServPriceSchema.setManageCom(mGlobalInput.ManageCom);

			mLHContServPriceSchema.setContraItemNo(mLHContServPriceSchema
					.getContraItemNo());
			System.out.println(mLHContServPriceSchema.getContraItemNo());
			map.put(mLHContServPriceSchema, "INSERT");
			// System.out.println("CCCCCCCCCC
			// "+mLDTestPriceMgtSet.get(1).getHospitCode());
			System.out.println("EEEEEEEEEEEEEEEEE ");
			if (mLDTestPriceMgtSet != null && mLDTestPriceMgtSet.size() > 0) 
			{

				for (int i = 1; i <= mLDTestPriceMgtSet.size(); i++) 
				{

					mLDTestPriceMgtSet.get(i).setHospitCode(mLDTestPriceMgtSet.get(1).getHospitCode());

					mLDTestPriceMgtSet.get(i).setSerialNo(PubFun1.CreateMaxNo("TestPriceMgt", 12));

					mLDTestPriceMgtSet.get(i).setOperator(mGlobalInput.Operator);
					mLDTestPriceMgtSet.get(i).setMakeDate(PubFun.getCurrentDate());
					mLDTestPriceMgtSet.get(i).setMakeTime(PubFun.getCurrentTime());
					mLDTestPriceMgtSet.get(i).setModifyDate(PubFun.getCurrentDate());
					mLDTestPriceMgtSet.get(i).setModifyTime(PubFun.getCurrentTime());
					mLDTestPriceMgtSet.get(i).setManageCom(mGlobalInput.ManageCom);
					System.out.println(i);
				}
				map.put(mLDTestPriceMgtSet, "INSERT");

			} 
			else 
			{
				System.out.println("mLDTestPriceMgtSet is null");
			}
		}

		if (this.mOperate.equals("UPDATE||MAIN")) 
		{

			mLHContServPriceSchema.setServPriceType(mLHContServPriceSchema.getServPriceType());
			System.out.println(" TYPE TYPE  TYPE  TYPE  "+ mLHContServPriceSchema.getServPriceType());
			if (mLHContServPriceSchema.getServPriceType().equals("1")) 
			{
				mLHContServPriceSchema.setOperator(mGlobalInput.Operator);
				mLHContServPriceSchema.setMakeDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setMakeTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setModifyDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setModifyTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setManageCom(mGlobalInput.ManageCom);
				map.put(mLHContServPriceSchema, "UPDATE");

			}
			if (mLHContServPriceSchema.getServPriceType().equals("3")) 
			{
				mLHContServPriceSchema.setOperator(mGlobalInput.Operator);
				mLHContServPriceSchema.setMakeDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setMakeTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setModifyDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setModifyTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setManageCom(mGlobalInput.ManageCom);
				map.put(mLHContServPriceSchema, "UPDATE");

			}

			if (mLHContServPriceSchema.getServPriceType().equals("4")) 
			{
				mLHContServPriceSchema.setOperator(mGlobalInput.Operator);
				mLHContServPriceSchema.setMakeDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setMakeTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setModifyDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setModifyTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setManageCom(mGlobalInput.ManageCom);
				map.put(mLHContServPriceSchema, "UPDATE");

			}

			if (mLHContServPriceSchema.getServPriceType().equals("2")) 
			{
				mLHContServPriceSchema.setOperator(mGlobalInput.Operator);
				mLHContServPriceSchema.setMakeDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setMakeTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setModifyDate(PubFun.getCurrentDate());
				mLHContServPriceSchema.setModifyTime(PubFun.getCurrentTime());
				mLHContServPriceSchema.setManageCom(mGlobalInput.ManageCom);

				map.put(mLHContServPriceSchema, "DELETE&INSERT");
				for (int i = 1; i <= mLDTestPriceMgtSet.size(); i++) 
				{
					if (mLDTestPriceMgtSet.get(i).getSerialNo() == null
							|| mLDTestPriceMgtSet.get(i).getSerialNo().equals("")) 
					{
						mLDTestPriceMgtSet.get(i).setSerialNo(PubFun1.CreateMaxNo("TestPriceMgt", 12));
					}
					mLDTestPriceMgtSet.get(i).setOperator(mGlobalInput.Operator);
					mLDTestPriceMgtSet.get(i).setMakeDate(PubFun.getCurrentDate());
					mLDTestPriceMgtSet.get(i).setMakeTime(PubFun.getCurrentTime());
					mLDTestPriceMgtSet.get(i).setModifyDate(PubFun.getCurrentDate());
					mLDTestPriceMgtSet.get(i).setModifyTime(PubFun.getCurrentTime());
					mLDTestPriceMgtSet.get(i).setManageCom(mGlobalInput.ManageCom);

				}
				System.out.println("TTTTTTTTTTTTTTT "+ mLDTestPriceMgtSet.get(1).getSerialNo());
				map.put(" delete from  LDTestPriceMgt where contrano='"+ mLHContServPriceSchema.getContraNo()+ "' and ContraItemNo='"
						+ mLHContServPriceSchema.getContraItemNo() + "' ","DELETE");
				map.put(mLDTestPriceMgtSet, "INSERT");
			}
		}

		if (this.mOperate.equals("DELETE||MAIN")) 
		{
			map.put(mLHContServPriceSchema, "DELETE");
			map.put(mLDTestPriceMgtSet, "DELETE");
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean updateData() 
	{
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean deleteData() 
	{
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) 
	{
		this.mLHContServPriceSchema.setSchema((LHContServPriceSchema) cInputData.getObjectByObjectName("LHContServPriceSchema", 0));
		this.mLDTestPriceMgtSet.set((LDTestPriceMgtSet) cInputData.getObjectByObjectName("LDTestPriceMgtSet", 0));

		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));

		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean submitquery() 
	{
		this.mResult.clear();
		LHContServPriceDB tLHContServPriceDB = new LHContServPriceDB();
		tLHContServPriceDB.setSchema(this.mLHContServPriceSchema);
		// 如果有需要处理的错误，则返回
		if (tLHContServPriceDB.mErrors.needDealError()) 
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tLHContServPriceDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LHGroupContBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	private boolean prepareOutputData() 
	{
		try {
			this.mInputData.clear();
			this.mInputData.add(this.mLHContServPriceSchema);
			this.mInputData.add(this.mLDTestPriceMgtSet);
			this.mInputData.add(map);
			mResult.clear();
			mResult.add(this.mLHContServPriceSchema);
			mResult.add(this.mLDTestPriceMgtSet);
		} 
		catch (Exception ex) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHContServPriceBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() 
	{
		return this.mResult;
	}
}
