package com.sinosoft.lis.hospitalmanage;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

public class LDindHospitalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors=new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData= new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDDingHospitalSchema mLDDingHospitalSchema=new LDDingHospitalSchema();

    public LDindHospitalBL()
    {
    }

   public boolean submitData(VData cInputData,String cOperate)
   {
	  this.mOperate =cOperate;
	  this.mInputData = cInputData;
	  if (!getInputData(cInputData))
	  {
		  return false;
	  }
	  if (!dealData())
	  {
		  return false;
	  }
	  if(!submitMap())
	  {
		  return false;
	  }
		 
	  return true;
  }

   
   
	private boolean dealData()
	{
		LDDingHospitalDB tLDDingHospitalDB = new LDDingHospitalDB();
		tLDDingHospitalDB.setHospitCode(mLDDingHospitalSchema.getHospitCode());
		tLDDingHospitalDB.setManageCom(mLDDingHospitalSchema.getManageCom());
		tLDDingHospitalDB.setRiskcode(mLDDingHospitalSchema.getRiskcode());
		LDDingHospitalSet tLDDingHospitalSet=tLDDingHospitalDB.query();
		if (this.mOperate.equals("INSERT||MAIN"))
		{
			 tLDDingHospitalDB.setHospitCode(mLDDingHospitalSchema.getHospitCode());
			 tLDDingHospitalDB.setManageCom(mLDDingHospitalSchema.getManageCom());
			 tLDDingHospitalDB.setRiskcode(mLDDingHospitalSchema.getRiskcode());
			 tLDDingHospitalSet=tLDDingHospitalDB.query();
			 if(tLDDingHospitalSet.size()>0)
			 {
				 CError tError = new CError();
			     tError.moduleName = "LDindHospitalBL";
			     tError.functionName = "submitData";
			     tError.errorMessage = "该管理机构下的此险种的医院已经配置，请核实!";
			     this.mErrors.addOneError(tError);
			     return false;
			 }
			  mLDDingHospitalSchema.setOperator(mGlobalInput.Operator);
		      mLDDingHospitalSchema.setMakeDate(PubFun.getCurrentDate());
		      mLDDingHospitalSchema.setMakeTime(PubFun.getCurrentTime());
		      mLDDingHospitalSchema.setModifyDate(PubFun.getCurrentDate());
		      mLDDingHospitalSchema.setModifyTime(PubFun.getCurrentTime());
		      map.put(mLDDingHospitalSchema, "INSERT");
		 }
		 if (this.mOperate.equals("UPDATE||MAIN"))
		 {
			if (tLDDingHospitalSet.size()<=0)
			{
		        CError tError = new CError();
		        tError.moduleName = "LDindHospitalBL";
		        tError.functionName = "submitData";
		        tError.errorMessage = "未查询到该医院的信息!";
		        this.mErrors.addOneError(tError);
		        return false;
			}
			LDDingHospitalSchema m_LDDingHospitalSchema=new LDDingHospitalSchema();
			m_LDDingHospitalSchema=tLDDingHospitalSet.get(1);
			Beifen(tLDDingHospitalSet,"INSERT");
			m_LDDingHospitalSchema.setManageCom(mLDDingHospitalSchema.getManageCom());
			m_LDDingHospitalSchema.setHospitName(mLDDingHospitalSchema.getHospitName());
			m_LDDingHospitalSchema.setRiskcode(mLDDingHospitalSchema.getRiskcode());
			m_LDDingHospitalSchema.setValidate(mLDDingHospitalSchema.getValidate());
			m_LDDingHospitalSchema.setCInvalidate(mLDDingHospitalSchema.getCInvalidate());
			m_LDDingHospitalSchema.setModifyDate(PubFun.getCurrentDate());
			m_LDDingHospitalSchema.setModifyTime(PubFun.getCurrentTime());
			m_LDDingHospitalSchema.setOperator(mGlobalInput.Operator);
		    map.put(m_LDDingHospitalSchema, "UPDATE");
		 }
		 if (this.mOperate.equals("DELETE||MAIN"))
		 {
			Beifen(tLDDingHospitalSet,"INSERT");
			String deleteSql="delete from LDDingHospital where HospitCode='"+mLDDingHospitalSchema.getHospitCode()
			                +"' and ManageCom='"+mLDDingHospitalSchema.getManageCom()
			                +"' and Riskcode='"+mLDDingHospitalSchema.getRiskcode()+"'";
		    map.put(deleteSql, "DELETE");

		 }
	
	  return true;
	}
	
	
	/**
	* 从输入数据中得到所有对象
	*输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	private boolean getInputData(VData cInputData)
	{
	       this.mLDDingHospitalSchema.setSchema((LDDingHospitalSchema)cInputData.getObjectByObjectName("LDDingHospitalSchema",0));
	       this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
	       return true;
	}
	/**
	* 备份数据 将修改和删除记录备份到B表
	*/
	private boolean Beifen(LDDingHospitalSet tLDDingHospitalSet,String operat )
	{
		LDDingHospitalSchema m_LDDingHospitalSchema=new LDDingHospitalSchema();
		m_LDDingHospitalSchema=tLDDingHospitalSet.get(1);
		LDDingHospitalBSchema tLDDingHospitalBSchema=new LDDingHospitalBSchema();
		String serialNo=PubFun1.CreateMaxNo("GETNO",null);
		tLDDingHospitalBSchema.setSerialNo(serialNo);
		tLDDingHospitalBSchema.setCInvalidate(m_LDDingHospitalSchema.getCInvalidate());
		tLDDingHospitalBSchema.setHospitCode(m_LDDingHospitalSchema.getHospitCode());
		tLDDingHospitalBSchema.setHospitName(m_LDDingHospitalSchema.getHospitName());
		tLDDingHospitalBSchema.setMakeDate(m_LDDingHospitalSchema.getMakeDate());
		tLDDingHospitalBSchema.setMakeTime(m_LDDingHospitalSchema.getMakeTime());
		tLDDingHospitalBSchema.setModifyDate(m_LDDingHospitalSchema.getModifyDate());
		tLDDingHospitalBSchema.setModifyTime(m_LDDingHospitalSchema.getModifyTime());
		tLDDingHospitalBSchema.setValidate(m_LDDingHospitalSchema.getValidate());
		tLDDingHospitalBSchema.setRiskcode(m_LDDingHospitalSchema.getRiskcode());
		tLDDingHospitalBSchema.setManageCom(m_LDDingHospitalSchema.getManageCom());
		tLDDingHospitalBSchema.setStandByFlag1(m_LDDingHospitalSchema.getStandByFlag1());
		tLDDingHospitalBSchema.setStandByFlag2(m_LDDingHospitalSchema.getStandByFlag2());
		tLDDingHospitalBSchema.setStandByFlag3(m_LDDingHospitalSchema.getStandByFlag3());
		tLDDingHospitalBSchema.setOperator(mGlobalInput.Operator);
		map.put(tLDDingHospitalBSchema, operat);
	       return true;
	}
	
	
	
	private boolean submitMap() 
	{   
	     mResult.add(map);
	     PubSubmit tPubsubmit = new PubSubmit();
	     if(!tPubsubmit.submitData(mResult, mOperate))
	     {
	    	 CError tError = new CError();
		     tError.moduleName = "LDindHospitalBL";
		     tError.functionName = "submitMap()";
		     tError.errorMessage = "操作数据库出错!";
		     this.mErrors.addOneError(tError);
		     return false;
	     }
	     return true;
	 }  
	public VData getResult()
	{
		return this.mResult;
	}


}
