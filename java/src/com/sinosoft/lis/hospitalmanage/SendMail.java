package com.sinosoft.lis.hospitalmanage;

import java.io.UnsupportedEncodingException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;

/**
 * <p>
 * Title: LIS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Yangming
 * @version 6.0
 */
public class SendMail {
	/** 邮件消息 */
	private MimeMessage mMessage;
	/** 收件人地址 */
	private Address[] toAddress;
	/** 抄送人地址 */
	private Address[] ccAddress;
	/** 暗抄人地址 */
	private Address[] bccAddress;
	/** 传送对象 */
	private Transport mTransport;
	/** 邮件服务器配置信息 */
	private MailServerConfig mMailServerConfig;
	/** 邮件内容 */
	private String mBody;
	/** 邮件标题 */
	private String mSubject;
	/** 邮件体 */
	private BodyPart messageBodyPart;
	/** 发送邮件 */
	private Multipart multipart = new MimeMultipart();
	/** 添加附件 */
	private String mFile;
	/** 发送邮件地址 */
	private String mFromMail;
	/** 发送邮件姓名 */
	private String mFromName;

	public CErrors mErrors = new CErrors();

	/**
	 * 发送邮件
	 */
	public SendMail() {
		try {
			mMailServerConfig = new MailServerConfig();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 发送邮件
	 * 
	 * @return boolean
	 */
	public boolean send(String body, String subject) {
		this.mBody = StrTool.unicodeToGBK(body);
		this.mSubject = subject;
		this.mFromMail = mMailServerConfig.getFromMail();
//		this.mFromName = mMailServerConfig.getFromName() == null ? mMailServerConfig
//				.getFromMail()
//				: mMailServerConfig.getFromName();
		this.mFromName = mMailServerConfig.getFromMail();
		if (!creatMessage()) {
			return false;
		}
		if (!Transport()) {
			return false;
		}
		return true;
	}

	/**
	 * Transport
	 * 
	 * @return boolean
	 */
	private boolean Transport() {
		mTransport = mMailServerConfig.getMTransport();
		try {
			mTransport.sendMessage(mMessage, mMessage.getAllRecipients()); // 发邮件
		} catch (MessagingException ex) {
			buildError("Transport", "发送邮件失败原因是:" + ex.getMessage());
			return false;
		}
		return true;
	}

	private Session mSession = null;

	/**
	 * creatMessage
	 * 
	 * @return boolean
	 */
	private boolean creatMessage() {

		mSession = mMailServerConfig.getSession();
		mMessage = new MimeMessage(mSession);

		if (!addAddress()) {
			return false;
		}
		if (!addMailBody()) {
			return false;
		}
		if (!addFile()) {
			return false;
		}
		try {
			this.mMessage.setContent(this.multipart);
		} catch (MessagingException ex) {
			ex.printStackTrace();
			buildError("creatMessage", "添加邮件体信息失败！" + ex.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * addFile
	 * 
	 * @return boolean
	 */
	private boolean addFile() {
		if (this.mFile != null) {

			String[] multFile = mFile.split(";");
			if (multFile != null && multFile.length > 0) {
				for (int i = 0; i < multFile.length; i++) {
					messageBodyPart = new MimeBodyPart();

					DataSource source = new FileDataSource(multFile[i]);
					try {
						messageBodyPart.setDataHandler(new DataHandler(source));
						
						System.out.println(multFile[i]
													.lastIndexOf("\\"));
						int len=multFile[i].lastIndexOf("\\");
						if(len==-1)
						{
							messageBodyPart.setFileName(multFile[i].substring(multFile[i]
							                           								.lastIndexOf("/") + 1));
						}
						else
						{
							messageBodyPart.setFileName(multFile[i].substring(multFile[i]
							                           								.lastIndexOf("\\") + 1));
						}
						

						multipart.addBodyPart(messageBodyPart);
					} catch (MessagingException ex) {
						ex.printStackTrace();
						buildError("addFile", "添加附件错误！" + ex.getMessage());
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * addMailBody
	 * 
	 * @return boolean
	 */
	private boolean addMailBody() {
		messageBodyPart = new MimeBodyPart();
		try {
			messageBodyPart.setContent(this.mBody, "text/html;charset=gbk");
			multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException ex2) {
			ex2.printStackTrace();
			buildError("creatMessage", "添加邮件内容错误！" + ex2.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * addAddress
	 * 
	 * @return boolean
	 */
	private boolean addAddress() {
		/** 添加发件人地址 */
		try {
			mMessage.setFrom(new InternetAddress(mFromMail, mFromName));
		} catch (UnsupportedEncodingException ex2) {
			buildError("addAddress", "添加发件人姓名错误！" + ex2.getMessage());
			return false;
		} catch (MessagingException ex2) {
			buildError("addAddress", "添加发件人地址错误！" + ex2.getMessage());
			return false;
		}
		/** 添加收件人 */
		if (this.toAddress != null && toAddress.length > 0) {
			addAddress(Message.RecipientType.TO, toAddress);
		}
		/** 添加抄送 */
		if (this.ccAddress != null && ccAddress.length > 0) {
			addAddress(Message.RecipientType.CC, ccAddress);
		}
		/** 添加暗送 */
		if (this.bccAddress != null && bccAddress.length > 0) {
			addAddress(Message.RecipientType.BCC, bccAddress);
		}
		try {
			mMessage.setSubject(this.mSubject);
		} catch (MessagingException ex1) {
			buildError("addAddress", "添加邮件标题失败！" + ex1.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * addAddress
	 * 
	 * @param recipientType
	 *            RecipientType
	 * @param toAddress
	 *            Address[]
	 */
	private boolean addAddress(javax.mail.Message.RecipientType recipientType,
			Address[] toAddress) {
		for (int i = 0; i < toAddress.length; i++) {
			try {

				mMessage.addRecipient(recipientType, toAddress[i]);
			} catch (MessagingException ex) {
				ex.printStackTrace();
				buildError("creatMessage", "添加收件人地址错误！" + ex.getMessage());
				return false;
			}
		}
		return true;
	}

	/**
	 * 获得收件人地址
	 * 
	 * @return Address[]
	 */
	public Address[] getToAddress() {
		return toAddress;
	}

	/**
	 * 获得抄送人地址
	 * 
	 * @return Address[]
	 */
	public Address[] getCcAddress() {
		return ccAddress;
	}

	/**
	 * 添加暗抄人地址
	 * 
	 * @param bccAddress
	 *            Address[]
	 */
	public void setBccAddress(Address[] bccAddress) {
		this.bccAddress = bccAddress;
	}

	/**
	 * 添加收件人地址
	 * 
	 * @param toAddress
	 *            Address[]
	 */
	public void setToAddress(Address[] toAddress) {
		this.toAddress = toAddress;
	}

	/**
	 * 添加抄送人地址
	 * 
	 * @param ccAddress
	 *            Address[]
	 */
	public void setCcAddress(Address[] ccAddress) {
		this.ccAddress = ccAddress;
	}

	public void setFile(String mFile) {
		this.mFile = mFile;
	}

	/**
	 * 添加暗抄人地址
	 * 
	 * @param bccAddress
	 *            Address[]
	 */
	public void setBccAddress(String bccAddress) throws Exception {
		Address[] tAddress = getAddress(bccAddress);
		this.bccAddress = tAddress;
	}

	/**
	 * 添加收件人地址
	 * 
	 * @param toAddress
	 *            Address[]
	 */
	public void setToAddress(String toAddress) throws Exception {
		this.toAddress = getAddress(toAddress);
	}

	/**
	 * 添加抄送人地址
	 * 
	 * @param ccAddress
	 *            Address[]
	 */
	public void setCcAddress(String ccAddress) throws Exception {
		this.ccAddress = getAddress(ccAddress);
	}

	/**
	 * getAddress
	 * 
	 * @param bccAddress
	 *            String
	 * @return Address[]
	 */
	private Address[] getAddress(String bccAddress) throws Exception {
		Address[] tAddress = null;

		if (bccAddress == null) {
			throw new Exception("录入地址为null");
		}
		if (bccAddress.indexOf("@") == -1) {
			throw new Exception("录入邮件地址不合法");
		}
		String[] count = bccAddress.split("@");
		System.out.println("存在邮件地址有" + (count.length - 1) + "个");

		/** 存在多个邮件地址 */
		if (count.length - 1 > 1) {
			if (bccAddress.indexOf(";") == -1) {
				throw new Exception("请使用\";\"分割邮件地址");
			}
			String[] address = bccAddress.split(";");
			tAddress = new Address[address.length];
			for (int i = 0; i < address.length; i++) {
				if (address[i] != null && !address[i].equals("")) {
					tAddress[i] = new InternetAddress(address[i]);
				}
			}
		} else if (count.length - 1 == 1) {
			tAddress = new Address[1];
			tAddress[0] = new InternetAddress(bccAddress);
		}
		return tAddress;
	}

	/**
	 * 获得暗抄送人地址
	 * 
	 * @return Address[]
	 */
	public Address[] getBccAddress() {
		return bccAddress;
	}

	/**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "SendMail";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.println("程序报错：" + cError.errorMessage);
	}

	public static void main(String[] args) {
		SendMail tSendMail = new SendMail();
		String address = "yangming@sinosoft.com.cn";
		try {
			tSendMail.setToAddress(address);
			tSendMail.setCcAddress("jarodmay@gmail.com");
			// tSendMail.setBccAddress(address);
		} catch (Exception ex) {
		}
		try {
			tSendMail.setFile("c:/windows/EasyScan Setup Log.txt");
			tSendMail.send("<b>中文</b>", "442");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
