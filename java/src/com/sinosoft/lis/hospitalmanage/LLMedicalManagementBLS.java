/*
 * <p>ClassName: LLMedicalManagementBLS </p>
 * <p>Description: LLMedicalManagementBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-29 10:45:26
 */
package com.sinosoft.lis.hospitalmanage;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
 public class LLMedicalManagementBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
//传输数据类
  private VData mInputData ;
 /** 数据操作字符串 */
private String mOperate;
public LLMedicalManagementBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
	  mInputData=(VData)cInputData.clone();
	System.out.println(mOperate);
	System.out.println("--------BLS submitData----------");
    if(this.mOperate.equals("INSERT||MAIN"))
    {if (!save())
        return false;
    }
    if (this.mOperate.equals("DELETE||MAIN"))
    {if (!delete())
        return false;
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {if (!update())
        return false;
    }
    System.out.println("--------BLS EndSubmitData----------");
  return true;
}
 /**
* 保存函数
*/
private boolean save()
{
  
  LLMedicalManagementSchema tLLMedicalManagementSchema = new LLMedicalManagementSchema();
  tLLMedicalManagementSchema = (LLMedicalManagementSchema)mInputData.getObjectByObjectName("LLMedicalManagementSchema",0);
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LLMedicalManagementBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
   }
	try{
 	  conn.setAutoCommit(false);
    LLMedicalManagementDB tLLMedicalManagementDB=new LLMedicalManagementDB(conn);
    tLLMedicalManagementDB.setSchema(tLLMedicalManagementSchema);
    System.out.println("------------DB insert------------");
    if (!tLLMedicalManagementDB.insert())
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLLMedicalManagementDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LLMedicalManagementBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      conn.rollback();
      conn.close();
      return false;
    }
    conn.commit() ;
    conn.close();
  }
  catch (Exception ex)
  {
    // @@错误处理
    CError tError =new CError();
    tError.moduleName="LLMedicalManagementBLS";
    tError.functionName="submitData";
    tError.errorMessage=ex.toString();
    this.mErrors .addOneError(tError);
      try{
      conn.rollback() ;
      conn.close();
      }
      catch(Exception e){}
    return false;
	}
  return true;
}
    /**
    * 保存函数
    */
    private boolean delete()
    {
        LLMedicalManagementSchema tLLMedicalManagementSchema = new LLMedicalManagementSchema();
        tLLMedicalManagementSchema = (LLMedicalManagementSchema)mInputData.getObjectByObjectName("LLMedicalManagementSchema",0);
        System.out.println("Start Connect...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "LLMedicalManagementBLS";
           tError.functionName = "save";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start delete...");
           LLMedicalManagementDB tLLMedicalManagementDB=new LLMedicalManagementDB(conn);
           tLLMedicalManagementDB.setSchema(tLLMedicalManagementSchema);
           if (!tLLMedicalManagementDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLLMedicalManagementDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "LLMedicalManagementBLS";
		    tError.functionName = "delete";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               conn.close();
               return false;
           }
               conn.commit() ;
               conn.close();
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="LLMedicalManagementBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          try{conn.rollback() ;
          conn.close();} catch(Exception e){}
         return false;
         }
         return true;
}
/**
  * 保存函数
*/
private boolean update()
{
     LLMedicalManagementSchema tLLMedicalManagementSchema = new LLMedicalManagementSchema();
     tLLMedicalManagementSchema = (LLMedicalManagementSchema)mInputData.getObjectByObjectName("LLMedicalManagementSchema",0);
     System.out.println("Start Update...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "LLMedicalManagementBLS";
        tError.functionName = "update";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 更新...");
           LLMedicalManagementDB tLLMedicalManagementDB=new LLMedicalManagementDB(conn);
	tLLMedicalManagementDB.setSchema(tLLMedicalManagementSchema);
           if (!tLLMedicalManagementDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tLLMedicalManagementDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "LLMedicalManagementBLS";
	         tError.functionName = "update";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            conn.close();
            return false;
            }
            conn.commit() ;
            conn.close();
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="LLMedicalManagementBLS";
               tError.functionName="update";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               try{conn.rollback() ;
               conn.close();} catch(Exception e){}
               return false;
     }
               return true;
     }
}
