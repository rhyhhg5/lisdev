package com.sinosoft.lis.hospitalmanage;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class LDindHospitalUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors=new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData =new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private LDDingHospitalSchema mLDDingHospitalSchema=new LDDingHospitalSchema();

    public LDindHospitalUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
	{
	  //将操作数据拷贝到本类中
	  this.mOperate =cOperate;
	  this.mInputData = cInputData;
	
	  LDindHospitalBL tLDindHospitalBL=new LDindHospitalBL();
	  tLDindHospitalBL.submitData(mInputData,mOperate);
	  //如果有需要处理的错误，则返回
	  if (tLDindHospitalBL.mErrors.needDealError() )
	  {
	     // @@错误处理
	     this.mErrors.copyAllErrors(tLDindHospitalBL.mErrors);
	     CError tError = new CError();
	     tError.moduleName = "LDindHospitalUI";
	     tError.functionName = "submitData";
	     tError.errorMessage = "数据提交失败!";
	     this.mErrors .addOneError(tError) ;
	     return false;
	  }
	  if (mOperate.equals("INSERT||MAIN"))
	  {
	     this.mResult.clear();
	     this.mResult=tLDindHospitalBL.getResult();
	  }
	  mInputData=null;
	  return true;
	  }

	public VData getResult()
	{
	  return this.mResult;
	}
}
