/*
 * <p>ClassName: LDAreaHospitalBL </p>
 * <p>Description: LDAreaHospitalBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 14:25:18
 */
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LDAreaHospitalBL 
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	private TransferData mTransferData = new TransferData();
	
	private MMap map = new MMap();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	/** 数据操作字符串 */
	private String mOperate;
	private String mMngCom;
	private String mCount;

	/** 业务处理相关变量 */
	private LDAreaHospitalSet mLDAreaHospitalSet = new LDAreaHospitalSet();

	public LDAreaHospitalBL() 
	{
	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) 
	{
		// 将操作数据拷贝到本类中
		mOperate = cOperate;
		mInputData = cInputData;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) 
		{
			return false;
		}
		// 进行业务处理
		if (!dealData()) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDAreaHospitalBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LDAreaHospitalBL-->dealData!";
			mErrors.addOneError(tError);
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) 
		{
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start LDAreaHospitalBL Submit...");

		if (!tPubSubmit.submitData(mInputData, mOperate))
		{
			// @@错误处理
			//mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LDAreaHospitalBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败,可能存在重复记录!";
			mErrors.addOneError(tError);
			return false;
		}

		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 
	 * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) 
	{
		
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		mLDAreaHospitalSet = (LDAreaHospitalSet) cInputData.getObjectByObjectName("LDAreaHospitalSet", 0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mMngCom = (String)mTransferData.getValueByName("ComInfo");
		mCount = (String)mTransferData.getValueByName("Count");
		System.out.println("mMngCom="+mMngCom +"count="+mCount);
		if(mMngCom==null || "".equals(mMngCom))
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDAreaHospitalBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			mErrors.addOneError(tError);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() 
	{
		
		if ("DELETE&&INSERT||MAIN".equals(mOperate)) 
		{
			map.put("delete from LDAreaHospital where ManageCom like '"+ mMngCom + "%'","DELETE");
			map.put(mLDAreaHospitalSet, "INSERT");			
		}
		
		if ("DELETE||MAIN".equals(mOperate)&&mLDAreaHospitalSet.size()<1) 
		{
			map.put("delete from LDAreaHospital where ManageCom like '"+ mMngCom + "%'","DELETE");
		}
		return true;
	}

	private boolean prepareOutputData() 
	{
		try 
		{
			mInputData.clear();
			mInputData.add(map);
			mResult.clear();
			mResult.add(mLDAreaHospitalSet);
		} 
		catch (Exception ex) 
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDAreaHospitalBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() 
	{
		return this.mResult;
	}
	

	public static void main(String[] args) 
	{
	}
}
