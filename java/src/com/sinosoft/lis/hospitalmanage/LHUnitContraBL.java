/*
 * <p>ClassName: LHUnitContraBL </p>
 * <p>Description:LHUnitContraBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-17 18:32:42
 */
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LHUnitContraBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    /** 业务处理相关变量 */
    private LHUnitContraSchema mLHUnitContraSchema = new
            LHUnitContraSchema();
    private LHContItemSet mLHContItemSet = new LHContItemSet();

    private LHContraAssoSettingSet mLHContraAssoSettingSet = new
            LHContraAssoSettingSet();

    public LHUnitContraBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("******after get :bl***");
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHUnitContraBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHUnitContraBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("******after dealData :bl***");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("******after prepareOutputData :bl***");
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLHUnitContraBL Submit...");
            LHUnitContraBLS tLHUnitContraBLS = new LHUnitContraBLS();
            tLHUnitContraBLS.submitData(mInputData, mOperate);
            System.out.println("End OLHUnitContraBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLHUnitContraBLS.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLHUnitContraBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LHUnitContraBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, mOperate);
        //this.mResult=mInputData;
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {

            mLHUnitContraSchema.setOperator(mGlobalInput.Operator);
            mLHUnitContraSchema.setMakeDate(PubFun.getCurrentDate());
            mLHUnitContraSchema.setMakeTime(PubFun.getCurrentTime());
            mLHUnitContraSchema.setModifyDate(PubFun.getCurrentDate());
            mLHUnitContraSchema.setModifyTime(PubFun.getCurrentTime());
            mLHUnitContraSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHUnitContraSchema, "INSERT");
            if (mLHContItemSet != null && mLHContItemSet.size() > 0) {
                for (int i = 1; i <= mLHContItemSet.size(); i++) {

                    mLHContItemSet.get(i).setContraNo(mLHUnitContraSchema.
                            getContraNo());

                    mLHContItemSet.get(i).setContraItemNo(PubFun1.CreateMaxNo(
                            "ContItem", 9));

                    mLHContItemSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHContItemSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHContItemSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHContItemSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHContItemSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHContItemSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                System.out.println("AAAAAAAAAAAAAAA" +
                                   mLHContItemSet.get(1).getContraFlag());
                System.out.println("MMMMMMMMMMM" +
                                   mLHContItemSet.get(1).getContraItemNo());

                if (mLHContItemSet.get(1).getContraFlag().equals("Y")) {

                    //  LHContraAssoSettingSchema mLHContraAssoSettingSchema = new LHContraAssoSettingSchema();
                    if (mLHContItemSet != null && mLHContItemSet.size() > 0) {

                        for (int i = 1; i <= mLHContItemSet.size(); i++) {
                            System.out.println("?????????" +
                                               mLHContItemSet.size());
                            mLHContraAssoSettingSet.get(i).setOperator(
                                    mLHContItemSet.get(i).getOperator());
                            mLHContraAssoSettingSet.get(i).setOperator(
                                    mLHContItemSet.get(i).getOperator());
                            mLHContraAssoSettingSet.get(i).setMakeDate(
                                    mLHContItemSet.get(i).getMakeDate());
                            mLHContraAssoSettingSet.get(i).setMakeTime(
                                    mLHContItemSet.get(i).getMakeTime());
                            mLHContraAssoSettingSet.get(i).setModifyDate(
                                    mLHContItemSet.get(i).getModifyDate());
                            mLHContraAssoSettingSet.get(i).setModifyTime(
                                    mLHContItemSet.get(i).getModifyTime());
                            mLHContraAssoSettingSet.get(i).setManageCom(
                                    mLHContItemSet.get(i).getManageCom());
                            mLHContraAssoSettingSet.get(i).setIndivContraNo(
                                    mLHContItemSet.get(1).getContraNo());
                            mLHContraAssoSettingSet.get(i).setIndivContraItemNo(
                                    mLHContItemSet.get(1).getContraItemNo());
                            System.out.println("MNNNNNNNNNNNNNNnzzz" +
                                               mLHContraAssoSettingSet.get(i).
                                               getIndivContraItemNo());
                            System.out.println(i);
                            //  mLHContraAssoSettingSet.add(mLHContraAssoSettingSchema);
                        }
                    }
                    map.put(mLHContraAssoSettingSet, "INSERT");
                    map.put(mLHContItemSet, "INSERT");
                    System.out.println("BBBBBBBBBBBBBBBBSuccess");

                } else {
                    map.put(mLHContItemSet, "INSERT");
                    System.out.println("CCCCCCCCCCCCCcCCCCCCCCCC");
                }

            }

        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            mLHUnitContraSchema.setOperator(mGlobalInput.Operator);
            mLHUnitContraSchema.setModifyDate(PubFun.getCurrentDate());
            mLHUnitContraSchema.setModifyTime(PubFun.getCurrentTime());
            mLHUnitContraSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHUnitContraSchema, "UPDATE");

//            if (mLHContItemSet != null && mLHContItemSet.size() > 0) {

            for (int i = 1; i <= mLHContItemSet.size(); i++) {
                if (mLHContItemSet.get(i).getContraItemNo() == null ||
                    mLHContItemSet.get(i).getContraItemNo().equals("")) {
                    mLHContItemSet.get(i).setContraItemNo(PubFun1.CreateMaxNo(
                            "ContraDuty", 9));
                }

                mLHContItemSet.get(i).setOperator(mGlobalInput.
                                                  Operator);

                mLHContItemSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHContItemSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHContItemSet.get(i).setManageCom(mGlobalInput.ManageCom);

            }
            map.put(" delete from  LHContItem where contrano='" +
                    mLHUnitContraSchema.getContraNo() + "'", "DELETE");
            map.put(mLHContItemSet, "INSERT");
//         }

        }

        if (this.mOperate.equals("DELETE||MAIN")) {

            map.put(" delete from  LHContServPrice where contrano='" +
                    mLHUnitContraSchema.getContraNo() + "'", "DELETE");
            map.put(" delete from  LHChargeBalance where contrano='" +
                    mLHUnitContraSchema.getContraNo() + "'", "DELETE");
            map.put(" delete from  LHContraAssoSetting where contrano='" +
                    mLHUnitContraSchema.getContraNo() + "'", "DELETE");
            map.put(" delete from  LDTestPriceMgt where contrano='" +
                    mLHUnitContraSchema.getContraNo() + "'", "DELETE");

            map.put(mLHUnitContraSchema, "DELETE");
            map.put(mLHContItemSet, "DELETE");

        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHUnitContraSchema.setSchema((LHUnitContraSchema)
                                           cInputData.
                                           getObjectByObjectName(
                "LHUnitContraSchema", 0));
        this.mLHContraAssoSettingSet.set((LHContraAssoSettingSet) cInputData.
                                         getObjectByObjectName(
                                                 "LHContraAssoSettingSet", 0));

        this.mLHContItemSet.set((LHContItemSet) cInputData.
                                getObjectByObjectName(

                                        "LHContItemSet", 0));

        if (mLHContItemSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "LHUnitContraBL";
            tError.functionName = "submitData";
            tError.errorMessage = "合同责任项目信息不许为空!";
            this.mErrors.addOneError(tError);
            return false;

        }

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        for (int i = 1; i <= mLHContItemSet.size(); i++) {
            if (mLHContItemSet.get(i).getContraType() == null ||
                mLHContItemSet.get(i).getContraType().equals("") ||
                mLHContItemSet.get(i).getContraType().equals("null")) {
                CError tError = new CError();
                tError.moduleName = "LHUnitContraBL";
                tError.functionName = "submitData";
                tError.errorMessage = "您输入的合同责任项目类型不许为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHUnitContraDB tLHUnitContraDB = new LHUnitContraDB();
        tLHUnitContraDB.setSchema(this.mLHUnitContraSchema);
        //如果有需要处理的错误，则返回
        if (tLHUnitContraDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHUnitContraDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHUnitContraBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();

            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHUnitContraSchema);
            //   mResult.add(this.mLHUnitContraSchema);
            mResult.add(this.mLHContItemSet);
            mResult.add(this.mLHContraAssoSettingSet);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
