package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.message.*;
import java.util.*;

/**
 * <p>Title: 定点医院健康建议短信发送</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class HMSuggestSMSSendBL {
    private GlobalInput mG = new GlobalInput();
    private String mManageCom = "";
    private VData mResult = new VData();
    private String mOperate = "";
    private MMap mMap = new MMap();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mLastDate = "";
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private LJSPaySchema mLJSPaySchema = null;
    private HMSuggestSendTrackSet mHMSuggestSendTrackSet = new HMSuggestSendTrackSet();

    public HMSuggestSMSSendBL() {}

    private boolean getInputData(VData cInputData) {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mHMSuggestSendTrackSet = (HMSuggestSendTrackSet)
        							cInputData.getObjectByObjectName("HMSuggestSendTrackSet", 0);
        mManageCom = mG.ManageCom;

        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) {
            return false;
        }
        if (cOperate != null && !cOperate.equals("")) {
        	mOperate = cOperate;
        	if(!sendMsg()) {
        		return false;
        	}
        	
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("定点医院康建议短信发送处理开始......");
        SmsServiceSoapBindingStub binding = null;
        try {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().
                      getSmsService(); //创建binding对象
        } catch (javax.xml.rpc.ServiceException jre) {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        com.sinosoft.lis.message.Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("lipei"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        Vector vec = new Vector();
        if(mOperate.equals("HMDISSUGGEST")) {
        	vec = getDisSendMessage();
        }
        

        if (!vec.isEmpty()) {
            msgs.setMessages((SmsMessage[]) vec.toArray(new SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            try {
                value = binding.sendSMS(msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                System.out.println(value.getStatus());
                System.out.println(value.getMessage());
            } catch (RemoteException ex) {
            	 //@@错误处理
            	 ex.printStackTrace();
 	 	         //this.mErrors.copyAllErrors( tLCContDB.mErrors );
 	 	         CError tError = new CError();
 	 	         tError.moduleName = "HMSuggestBL";
 	 	         tError.functionName = "dealData";
 	 	         tError.errorMessage = "发送短信过程中出现异常!";
 	 	         this.mErrors.addOneError(tError);
 	 	         return false;
            }
        } else {
            System.out.print("无符合发送条件的短信！");
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
 	         CError tError = new CError();
 	         tError.moduleName = "HMSuggestBL";
 	         tError.functionName = "dealData";
 	         tError.errorMessage = "无符合发送条件的短信！!";
 	         this.mErrors.addOneError(tError);
 	         return false;
        }
        return true;
    }
    
    //按疾病短信健康建议
    private Vector getDisSendMessage() {
        Vector tVector = new Vector();
        int size = mHMSuggestSendTrackSet.size();
        for(int i=1; i<=size; i++) {
        	String tCustomerNo = mHMSuggestSendTrackSet.get(i).getCustomerNo();
        	String tName = mHMSuggestSendTrackSet.get(i).getName();
        	String tMobile = mHMSuggestSendTrackSet.get(i).getMobile();
        	String tSuggest = mHMSuggestSendTrackSet.get(i).getSuggest();
        	
			SmsMessage msg = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
			msg.setReceiver(tMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
			msg.setContents(tSuggest); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
			msg.setOrgCode("86000000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
			tVector.add(msg);
        }
        
        return tVector;
    }


//    /*数据处理*/
//    private boolean SubmitMap() {
//        PubSubmit tPubSubmit = new PubSubmit();
//        mResult.clear();
//        mResult.add(mMap);
//        if (!tPubSubmit.submitData(mResult, "")) {
//            System.out.println("提交数据失败！");
//             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
//	         CError tError = new CError();
//	         tError.moduleName = "HMSuggestSMSSendBL";
//	         tError.functionName = "dealData";
//	         tError.errorMessage = "提交数据失败！";
//	         this.mErrors.addOneError(tError);
//	         return false;
//        }
//        this.mMap = new MMap();
//        return true;
//    }

    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        HMSuggestSendTrackSet tHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
        HMSuggestSendTrackSchema tHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
        tHMSuggestSendTrackSchema.setCustomerNo("0000001");
        tHMSuggestSendTrackSchema.setMobile("13240703546");
        tHMSuggestSendTrackSchema.setSuggest("你好123！");
        tHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
        
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
       // mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        mVData.add(tHMSuggestSendTrackSet);
        
        
        HMSuggestSMSSendBL tHMSuggestSMSSendBL = new HMSuggestSMSSendBL();

        tHMSuggestSMSSendBL.submitData(mVData, "HMDISSUGGEST");
//       System.out.print(mPolicyAbateDealBL.edorseState("00000619801"));
//         mPolicyAbateDealBL.edorseState("0000126302");
    }


}
