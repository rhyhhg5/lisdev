/**
 * <p>ClassName: HMSuggestBL.java </p>
 * <p>Description: 专项健康建立录入 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author chenxw
 * @version 1.0
 * @CreateDate：2010-3-17
 */

//包名
package com.sinosoft.lis.hospitalmanage;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.mail.SendMail;
import com.sinosoft.lis.schema.HMSuggestInfoSchema;
import com.sinosoft.lis.schema.HMSuggestSendTrackSchema;
import com.sinosoft.lis.vschema.HMSuggestSendTrackSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class HMSuggestBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();
	
	private String mSendResult = "'^'";;

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private TransferData mTransferData = new TransferData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 数据操作字符串 */
	private String mOperator; //获取操作员代码

	private String mManageCom;

	private boolean mIsLargeDate = false; //判断是否是大数据量，如果是，不走正常提交。应该是单独分批次提交
	
	private String mSendType = "";

	/** 健康建议表 */
	private HMSuggestInfoSchema mHMSuggestInfoSchema = new HMSuggestInfoSchema();

	/** 健康建议发送轨迹表 */
	private HMSuggestSendTrackSet mHMSuggestSendTrackSet = new HMSuggestSendTrackSet();

	private MMap map = new MMap();

	public HMSuggestBL() {

	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据
	 *         cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}
		
		//进行业务处理
		if (!dealData()) {
			return false;
		}
		
		if (!mIsLargeDate) {
			if (!prepareOutputData()) {
				return false;
			}

			PubSubmit tPubSubmit = new PubSubmit();

			System.out.println("Start HMSuggestBL Submit...");

			if (!tPubSubmit.submitData(mInputData, "")) {
				CError tError = new CError();
				tError.moduleName = "HMSuggestBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据库提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		System.out.println("End HMSuggestBL Submit...");
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData, String cOperate) {
		//从输入数据中得到所有对象
		//获得全局公共数据
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		mHMSuggestInfoSchema = (HMSuggestInfoSchema) cInputData
				.getObjectByObjectName("HMSuggestInfoSchema", 0);
		mHMSuggestSendTrackSet = (HMSuggestSendTrackSet) cInputData
				.getObjectByObjectName("HMSuggestSendTrackSet", 0);
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		//获得操作员编码
		mOperator = mGlobalInput.Operator;
		//获取操作机构
		mManageCom = mGlobalInput.ManageCom;
		mOperate = cOperate;

		return true;
	}

	private boolean checkData() {
		if (mGlobalInput == null) {
			// @@错误处理
			//this.mErrors.copyAllErrors( tLCContDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "HMSuggestBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mOperator == null || mOperator.trim().equals("")) {
			// @@错误处理
			//this.mErrors.copyAllErrors( tLCContDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "HMSuggestBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据Operate失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mManageCom == null || mManageCom.trim().equals("")) {
			// @@错误处理
			//this.mErrors.copyAllErrors( tLCContDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "HMSuggestBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据mManageCom失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mOperate == null || mOperate.trim().equals("")) {
			// @@错误处理
			//this.mErrors.copyAllErrors( tLCContDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "HMSuggestBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据mOperate失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		//病例健康建议保存
		if (mOperate.equals("DISSUGGEST||MAIN")) {
			//具体操作类别
			String tDisDetialOperate = (String) mTransferData
					.getValueByName("DisDetialOperate");
			if (tDisDetialOperate == null || "".equals(tDisDetialOperate)) {
				//@@错误处理
				//this.mErrors.copyAllErrors( tLCContDB.mErrors );
				CError tError = new CError();
				tError.moduleName = "HMSuggestBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "前台传输详细操作方式失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			//疾病建议插入操作
			if (tDisDetialOperate.equals("INSERT||DIS")) {
				mHMSuggestInfoSchema.setOperator(mOperator);
				mHMSuggestInfoSchema.setManageCom(mManageCom);
				mHMSuggestInfoSchema.setMakeDate(PubFun.getCurrentDate());
				mHMSuggestInfoSchema.setMakeTime(PubFun.getCurrentTime());
				mHMSuggestInfoSchema.setModifyDate(PubFun.getCurrentDate());
				mHMSuggestInfoSchema.setModifyTime(PubFun.getCurrentTime());
				map.put(mHMSuggestInfoSchema, "INSERT");
			}
			//修改
			if (tDisDetialOperate.equals("UPDATE||DIS")) {
				String tDisCode = mHMSuggestInfoSchema.getBusinessCode();
				if (tDisCode == null || "".equals(tDisCode)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "前台传输疾病编码异常!";
					this.mErrors.addOneError(tError);
					return false;
				}
				String upSql = " update HMSuggestInfo set HealthSuggest='"
						+ mHMSuggestInfoSchema.getHealthSuggest() + "'"
						+ " , Operator='" + mOperator + "' , ManageCom='"
						+ mManageCom + "'" + " , ModifyDate='"
						+ PubFun.getCurrentDate() + "'" + " , ModifyTime='"
						+ PubFun.getCurrentTime() + "' "
						+ " where BusinessCode=" + tDisCode
						+ " and BusinessType='1'";
				map.put(upSql, "UPDATE");
			}
			//删除
			if (tDisDetialOperate.equals("DELETE||DIS")) {
				String tDisCode = mHMSuggestInfoSchema.getBusinessCode();
				if (tDisCode == null || "".equals(tDisCode)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "前台传输疾病编码异常!";
					this.mErrors.addOneError(tError);
					return false;
				}
				String delSql = " delete from HMSuggestInfo "
						+ " where BusinessCode=" + tDisCode
						+ " and BusinessType='1'";
				map.put(delSql, "DELETE");
			}

			//建议发送（未选中全部发送）
			if (tDisDetialOperate != null
					&& "SUGGESTSEND||DIS".equals(tDisDetialOperate)) {
				//发送方式
				String tSendType = (String) mTransferData
						.getValueByName("SendType");
				if (tSendType == null || "".equals(tSendType)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "前台传输发送方式异常!";
					this.mErrors.addOneError(tError);
					return false;
				}

				//重新包装InputData
				VData tmpInputData = new VData();
				tmpInputData.add(mGlobalInput);

				//短信发送
				if (tSendType.equals("1")) {
					//健康建议
					HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
							mHMSuggestSendTrackSet,"DIS","SMSSEND");
					tmpInputData.add(willSendSuggest);
					
					HMSuggestSMSSendBL tHMSuggestSMSSendBL = new HMSuggestSMSSendBL();
					if (!tHMSuggestSMSSendBL.submitData(tmpInputData,"HMDISSUGGEST")) {
						//@@错误处理
						mErrors.clearErrors();
						this.mErrors.copyAllErrors(tHMSuggestSMSSendBL.mErrors);
						return false;
					} else {
						//健康建议表插入
						insertHMSuggestSendTrack(mHMSuggestSendTrackSet);
					}
				}
				//邮件发送
				if (tSendType.equals("2")) {
					String mailTitle = "健康建议";
					int sendMailCount = 0; //发送mail数
					//健康建议
					HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
							mHMSuggestSendTrackSet,"DIS","MAILSEND");
					//HMCustSendMailBL tHMCustSendMailBL = new HMCustSendMailBL();
					for(int i=1;i<=willSendSuggest.size();i++) {
						String mailTo = willSendSuggest.get(i).getEMail();
						String mailContent = willSendSuggest.get(i).getSuggest();
						
//						tHMCustSendMailBL.setMailTo(mailTo);
//						tHMCustSendMailBL.setMailTitle(mailTitle);
//						tHMCustSendMailBL.setMailConent(mailContent);
						
						if(SendMailFun.TMail(null, mailTo, null, mailTitle, mailContent)) {
							sendMailCount ++;
						}
					}
					//存在合格邮件地址，并且一封邮件也没有发送成功的话，我们认为是邮件发送失败
					if(willSendSuggest.size() > 0 && sendMailCount == 0) {
						//@@错误处理
						//this.mErrors.copyAllErrors( tLCContDB.mErrors );
						CError tError = new CError();
						tError.moduleName = "HMSuggestBL";
						tError.functionName = "dealData";
						tError.errorMessage = "发送邮件失败!";
						this.mErrors.addOneError(tError);
						return false;
					} else {
						//健康建议表插入
						insertHMSuggestSendTrack(mHMSuggestSendTrackSet);
					}
					
				}
				//打印
				if (tSendType.equals("3")) {
					HMSuggestPrintBL tHMSuggestPrintBL = new HMSuggestPrintBL();
					
					HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
							mHMSuggestSendTrackSet,"DIS","PRINTSEND");
					
					tmpInputData.add(willSendSuggest);
					if(!tHMSuggestPrintBL.submitData(tmpInputData, "batch")) {
						//@@错误处理
						//this.mErrors.copyAllErrors( tLCContDB.mErrors );
						this.mErrors.copyAllErrors(tHMSuggestPrintBL.mErrors);
						return false;
					} else {
						//健康建议表插入
						insertHMSuggestSendTrack(mHMSuggestSendTrackSet);
					}
				}
			}

			//健康建议发送（全部发送）
			if (tDisDetialOperate != null && "SUGGESTSENDALL||DIS".equals(tDisDetialOperate)) {

				//使用自己的提交方式
				mIsLargeDate = true;
				//发送方式
				String tSendType = (String) mTransferData.getValueByName("SendType");
				 mSendType = tSendType ;
				if (tSendType == null || "".equals(tSendType)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "前台传输发送方式异常!";
					this.mErrors.addOneError(tError);
					return false;
				}

				String tDisQueryCollectionSQL = "";
				HashMap tHashMap = new HashMap();
				if (!disQueryCollection(tHashMap)) {
					return false;
				} else {
					tDisQueryCollectionSQL = (String) tHashMap.get("DisQueryCollectionSQL");
				}
				if (tDisQueryCollectionSQL.equals("")) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "根据条件转换SQL时出错!";
					this.mErrors.addOneError(tError);
					return false;
				}

				//重新包装InputData
				VData tmpInputData = new VData();
				tmpInputData.add(mGlobalInput);

				RSWrapper rsWrapper = new RSWrapper();

				if (!rsWrapper.prepareData(null, tDisQueryCollectionSQL)) {
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "数据准备失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				try {
					SSRS tSSRS = new SSRS();
					do {

						//根据查询条件返回结果
						HMSuggestSendTrackSet tHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
						tSSRS = rsWrapper.getSSRS();

						if (tSSRS == null || tSSRS.MaxRow < 1) {
							break;
						}
						for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
							HMSuggestSendTrackSchema tmpHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
							tmpHMSuggestSendTrackSchema.setCustomerNo(tSSRS.GetText(i, 1));
							tmpHMSuggestSendTrackSchema.setName(tSSRS.GetText(i, 2));
							tmpHMSuggestSendTrackSchema.setMobile(tSSRS.GetText(i, 3));
							tmpHMSuggestSendTrackSchema.setEMail(tSSRS.GetText(i, 4));
							tmpHMSuggestSendTrackSchema.setHomeAddress(tSSRS.GetText(i, 5));
							tmpHMSuggestSendTrackSchema.setBusinessCode(tSSRS.GetText(i, 6));
							tmpHMSuggestSendTrackSchema.setLastVisEndDate(tSSRS.GetText(i, 7));
							tmpHMSuggestSendTrackSchema.setLastYearVisCount(tSSRS.GetText(i, 8));
							tmpHMSuggestSendTrackSchema.setLastYearMedRealPay(tSSRS.GetText(i, 9));
							tmpHMSuggestSendTrackSchema.setVisType(tSSRS.GetText(i, 10));
							tmpHMSuggestSendTrackSchema.setSendType(tSendType);
							tmpHMSuggestSendTrackSchema.setBusinessType("1");

							tHMSuggestSendTrackSet.add(tmpHMSuggestSendTrackSchema);
						}

						//短信发送
						if (tSendType.equals("1")) {
							//健康建议
							HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
													tHMSuggestSendTrackSet,"DIS","SMSSEND");
							tmpInputData.add(willSendSuggest);
							
							HMSuggestSMSSendBL tHMSuggestSMSSendBL = new HMSuggestSMSSendBL();
							if (!tHMSuggestSMSSendBL.submitData(tmpInputData,"HMDISSUGGEST")) {
								//@@错误处理
								mErrors.clearErrors();
								this.mErrors.copyAllErrors(tHMSuggestSMSSendBL.mErrors);
								return false;
							} else {
								//健康建议表插入
								insertHMSuggestSendTrack(tHMSuggestSendTrackSet);
								if (!prepareOutputData()) {
									return false;
								}
								PubSubmit tPubSubmit = new PubSubmit();
								System.out.println("Start HMSuggestBL Submit...");
								if (!tPubSubmit.submitData(mInputData, "")) {
									CError tError = new CError();
									tError.moduleName = "HMSuggestBL";
									tError.functionName = "submitData";
									tError.errorMessage = "数据库提交失败!";
									this.mErrors.addOneError(tError);
									return false;
								}
							}
							willSendSuggest = null;
						}
						//邮件发送
						if (tSendType.equals("2")) {
							String mailTitle = "健康建议";
							int sendMailCount = 0; //发送mail数
							//健康建议
							HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
									tHMSuggestSendTrackSet,"DIS","MAILSEND");
							//HMCustSendMailBL tHMCustSendMailBL = new HMCustSendMailBL();
							for(int i=1;i<=willSendSuggest.size();i++) {
								String mailTo = willSendSuggest.get(i).getEMail();
								String mailContent = willSendSuggest.get(i).getSuggest();
								
//								tHMCustSendMailBL.setMailTo(mailTo);
//								tHMCustSendMailBL.setMailTitle(mailTitle);
//								tHMCustSendMailBL.setMailConent(mailContent);
//								if(tHMCustSendMailBL.send()) {
//									sendMailCount ++;
//								}
								if(SendMailFun.TMail(null, mailTo, null, mailTitle, mailContent)) {
									sendMailCount ++;
								}
							}
							//存在合格邮件地址，并且一封邮件也没有发送成功的话，我们认为是邮件发送失败
							if(willSendSuggest.size() > 0 &&sendMailCount == 0) {
								//@@错误处理
								//this.mErrors.copyAllErrors( tLCContDB.mErrors );
								CError tError = new CError();
								tError.moduleName = "HMSuggestBL";
								tError.functionName = "dealData";
								tError.errorMessage = "发送邮件失败!";
								this.mErrors.addOneError(tError);
								return false;
							} else {
								//健康建议表插入
								insertHMSuggestSendTrack(tHMSuggestSendTrackSet);
								if (!prepareOutputData()) {
									return false;
								}
								PubSubmit tPubSubmit = new PubSubmit();
								System.out.println("Start HMSuggestBL Submit...");
								if (!tPubSubmit.submitData(mInputData, "")) {
									CError tError = new CError();
									tError.moduleName = "HMSuggestBL";
									tError.functionName = "submitData";
									tError.errorMessage = "数据库提交失败!";
									this.mErrors.addOneError(tError);
									return false;
								}
							}
							willSendSuggest = null;
						}
						//打印
						if (tSendType.equals("3")) {
							HMSuggestPrintBL tHMSuggestPrintBL = new HMSuggestPrintBL();
							
							HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
									tHMSuggestSendTrackSet,"DIS","PRINTSEND");
							
							tmpInputData.add(willSendSuggest);
							if(!tHMSuggestPrintBL.submitData(tmpInputData, "batch")) {
								//@@错误处理
								//this.mErrors.copyAllErrors( tLCContDB.mErrors );
								this.mErrors.copyAllErrors(tHMSuggestPrintBL.mErrors);
								return false;
							} else {
								//健康建议表插入
								insertHMSuggestSendTrack(tHMSuggestSendTrackSet);
								if (!prepareOutputData()) {
									return false;
								}
								PubSubmit tPubSubmit = new PubSubmit();
								System.out.println("Start HMSuggestBL Submit...");
								if (!tPubSubmit.submitData(mInputData, "")) {
									CError tError = new CError();
									tError.moduleName = "HMSuggestBL";
									tError.functionName = "submitData";
									tError.errorMessage = "数据库提交失败!";
									this.mErrors.addOneError(tError);
									return false;
								}
							}
							willSendSuggest = null;
						}

					} while (tSSRS != null && tSSRS.MaxRow > 0);

				} catch (Exception e) {
					e.printStackTrace();
					return false;
				} finally {
					rsWrapper.close();
				}
			}
		}
//******************************************************************************************
		
		//按客户健康建议
		if (mOperate.equals("CUSTSUGGEST||MAIN")) {
			//具体操作类别
			String tDisDetialOperate = (String) mTransferData
					.getValueByName("DisDetialOperate");
			if (tDisDetialOperate == null || "".equals(tDisDetialOperate)) {
				//@@错误处理
				//this.mErrors.copyAllErrors( tLCContDB.mErrors );
				CError tError = new CError();
				tError.moduleName = "HMSuggestBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "前台传输详细操作方式失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			//客户建议插入操作
			if (tDisDetialOperate.equals("INSERT||CUST")) {
				mHMSuggestInfoSchema.setOperator(mOperator);
				mHMSuggestInfoSchema.setManageCom(mManageCom);
				mHMSuggestInfoSchema.setMakeDate(PubFun.getCurrentDate());
				mHMSuggestInfoSchema.setMakeTime(PubFun.getCurrentTime());
				mHMSuggestInfoSchema.setModifyDate(PubFun.getCurrentDate());
				mHMSuggestInfoSchema.setModifyTime(PubFun.getCurrentTime());
				map.put(mHMSuggestInfoSchema, "INSERT");
			}
			//修改
			if (tDisDetialOperate.equals("UPDATE||CUST")) {
				String tCustemorNo = mHMSuggestInfoSchema.getBusinessCode();
				if (tCustemorNo == null || "".equals(tCustemorNo)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "getInputData";
					tError.errorMessage = "前台传输客户号异常!";
					this.mErrors.addOneError(tError);
					return false;
				}
				String upSql = " update HMSuggestInfo set HealthSuggest='"
						+ mHMSuggestInfoSchema.getHealthSuggest() + "'"
						+ " , Operator='" + mOperator + "' , ManageCom='"
						+ mManageCom + "'" + " , ModifyDate='"
						+ PubFun.getCurrentDate() + "'" + " , ModifyTime='"
						+ PubFun.getCurrentTime() + "' "
						+ " where BusinessCode=" + tCustemorNo
						+ " and BusinessType='2'";
				map.put(upSql, "UPDATE");
			}
			//删除
			if (tDisDetialOperate.equals("DELETE||CUST")) {
				String tCustemorNo = mHMSuggestInfoSchema.getBusinessCode();
				if (tCustemorNo == null || "".equals(tCustemorNo)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "前台传输客户号异常!";
					this.mErrors.addOneError(tError);
					return false;
				}
				String delSql = " delete from HMSuggestInfo "
						+ " where BusinessCode=" + tCustemorNo
						+ " and BusinessType='2'";
				map.put(delSql, "DELETE");
			}
			
			
			//建议发送逐一发送
			if (tDisDetialOperate != null
					&& "SUGGESTSEND||CUST".equals(tDisDetialOperate)) {
				//发送方式
				String tSendType = (String) mTransferData.getValueByName("CSendType");
				
				if (tSendType == null || "".equals(tSendType)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "前台传输发送方式异常!";
					this.mErrors.addOneError(tError);
					return false;
				}

				//重新包装InputData
				VData tmpInputData = new VData();
				tmpInputData.add(mGlobalInput);

				//短信发送
				if (tSendType.equals("1")) {
					//健康建议
					HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
							mHMSuggestSendTrackSet,"CUST","SMSSEND");
					tmpInputData.add(willSendSuggest);
					
					HMSuggestSMSSendBL tHMSuggestSMSSendBL = new HMSuggestSMSSendBL();
					if (!tHMSuggestSMSSendBL.submitData(tmpInputData,"HMDISSUGGEST")) {
						//@@错误处理
						mErrors.clearErrors();
						this.mErrors.copyAllErrors(tHMSuggestSMSSendBL.mErrors);
						return false;
					} else {
						//健康建议表插入
						insertHMSuggestSendTrack(mHMSuggestSendTrackSet);
					}
				}
				//邮件发送
				if (tSendType.equals("2")) {
					String mailTitle = "健康建议";
					int sendMailCount = 0; //发送mail数
					//健康建议
					HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
							mHMSuggestSendTrackSet,"CUST","MAILSEND");
					//HMCustSendMailBL tHMCustSendMailBL = new HMCustSendMailBL();
					for(int i=1;i<=willSendSuggest.size();i++) {
						String mailTo = willSendSuggest.get(i).getEMail();
						String mailContent = willSendSuggest.get(i).getSuggest();
						
//						tHMCustSendMailBL.setMailTo(mailTo);
//						tHMCustSendMailBL.setMailTitle(mailTitle);
//						tHMCustSendMailBL.setMailConent(mailContent);
//						if(tHMCustSendMailBL.send()) {
//							sendMailCount ++;
//						}
						if(SendMailFun.TMail(null, mailTo, null, mailTitle, mailContent)) {
							sendMailCount ++;
						}
					}
					//存在合格邮件地址，并且一封邮件也没有发送成功的话，我们认为是邮件发送失败
					if(willSendSuggest.size() > 0 &&sendMailCount == 0) {
						//@@错误处理
						//this.mErrors.copyAllErrors( tLCContDB.mErrors );
						CError tError = new CError();
						tError.moduleName = "HMSuggestBL";
						tError.functionName = "dealData";
						tError.errorMessage = "发送邮件失败!";
						this.mErrors.addOneError(tError);
						return false;
					} else {
						//健康建议表插入
						insertHMSuggestSendTrack(mHMSuggestSendTrackSet);
					}
					
				}
				//打印
				if (tSendType.equals("3")) {
					HMSuggestPrintBL tHMSuggestPrintBL = new HMSuggestPrintBL();
					HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
							mHMSuggestSendTrackSet,"CUST","PRINTSEND");
					
					tmpInputData.add(willSendSuggest);
					if(!tHMSuggestPrintBL.submitData(tmpInputData, "batch")) {
						//@@错误处理
						//this.mErrors.copyAllErrors( tLCContDB.mErrors );
						this.mErrors.copyAllErrors(tHMSuggestPrintBL.mErrors);
						return false;
					} else {
						//健康建议表插入
						insertHMSuggestSendTrack(mHMSuggestSendTrackSet);
					}
				}
			}
			
			//健康建议发送（全部发送）
			if (tDisDetialOperate != null && "SUGGESTSENDALL||CUST".equals(tDisDetialOperate)) {

				//使用自己的提交方式
				mIsLargeDate = true;
				//发送方式
				String tSendType = (String) mTransferData.getValueByName("CCSendType");
				 mSendType = tSendType ;
				if (tSendType == null || "".equals(tSendType)) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "前台传输发送方式异常!";
					this.mErrors.addOneError(tError);
					return false;
				}

				String tCustQueryCollectionSQL = "";
				HashMap tHashMap = new HashMap();
				if (!custQueryCollection(tHashMap)) {
					return false;
				} else {
					tCustQueryCollectionSQL = (String) tHashMap.get("CustQueryCollectionSQL");
				}
				if (tCustQueryCollectionSQL.equals("")) {
					//@@错误处理
					//this.mErrors.copyAllErrors( tLCContDB.mErrors );
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "根据条件转换SQL时出错!";
					this.mErrors.addOneError(tError);
					return false;
				}

				//重新包装InputData
				VData tmpInputData = new VData();
				tmpInputData.add(mGlobalInput);

				RSWrapper rsWrapper = new RSWrapper();

				if (!rsWrapper.prepareData(null, tCustQueryCollectionSQL)) {
					CError tError = new CError();
					tError.moduleName = "HMSuggestBL";
					tError.functionName = "dealData";
					tError.errorMessage = "数据准备失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				try {
					SSRS tSSRS = new SSRS();
					do {

						//根据查询条件返回结果
						HMSuggestSendTrackSet tHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
						tSSRS = rsWrapper.getSSRS();

						if (tSSRS == null || tSSRS.MaxRow < 1) {
							break;
						}
						for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
							HMSuggestSendTrackSchema tmpHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
							tmpHMSuggestSendTrackSchema.setCustomerNo(tSSRS.GetText(i, 2));
							tmpHMSuggestSendTrackSchema.setName(tSSRS.GetText(i, 1));
							tmpHMSuggestSendTrackSchema.setMobile(tSSRS.GetText(i, 3));
							tmpHMSuggestSendTrackSchema.setEMail(tSSRS.GetText(i, 4));
							tmpHMSuggestSendTrackSchema.setHomeAddress(tSSRS.GetText(i, 5));
							
							tmpHMSuggestSendTrackSchema.setBusinessCode(tSSRS.GetText(i, 2));
							tmpHMSuggestSendTrackSchema.setLastVisEndDate(tSSRS.GetText(i, 7));
							tmpHMSuggestSendTrackSchema.setLastYearVisCount(tSSRS.GetText(i, 8));
							tmpHMSuggestSendTrackSchema.setLastYearMedRealPay(tSSRS.GetText(i, 11));
							tmpHMSuggestSendTrackSchema.setVisType(tSSRS.GetText(i, 9));
							tmpHMSuggestSendTrackSchema.setSendType(tSendType);
							tmpHMSuggestSendTrackSchema.setBusinessType("2");

							tHMSuggestSendTrackSet.add(tmpHMSuggestSendTrackSchema);
						}

						//短信发送
						if (tSendType.equals("1")) {
							//健康建议
							HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
													tHMSuggestSendTrackSet,"CUST","SMSSEND");
							tmpInputData.add(willSendSuggest);
							
							HMSuggestSMSSendBL tHMSuggestSMSSendBL = new HMSuggestSMSSendBL();
							if (!tHMSuggestSMSSendBL.submitData(tmpInputData,"HMDISSUGGEST")) {
								//@@错误处理
								mErrors.clearErrors();
								this.mErrors.copyAllErrors(tHMSuggestSMSSendBL.mErrors);
								return false;
							} else {
								//健康建议表插入
								insertHMSuggestSendTrack(tHMSuggestSendTrackSet);
								if (!prepareOutputData()) {
									return false;
								}
								PubSubmit tPubSubmit = new PubSubmit();
								System.out.println("Start HMSuggestBL Submit...");
								if (!tPubSubmit.submitData(mInputData, "")) {
									CError tError = new CError();
									tError.moduleName = "HMSuggestBL";
									tError.functionName = "submitData";
									tError.errorMessage = "数据库提交失败!";
									this.mErrors.addOneError(tError);
									return false;
								}
							}
							willSendSuggest = null;
						}
						//邮件发送
						if (tSendType.equals("2")) {
							String mailTitle = "健康建议";
							int sendMailCount = 0; //发送mail数
							//健康建议
							HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
									tHMSuggestSendTrackSet,"CUST","MAILSEND");
							//HMCustSendMailBL tHMCustSendMailBL = new HMCustSendMailBL();
							for(int i=1;i<=willSendSuggest.size();i++) {
								String mailTo = willSendSuggest.get(i).getEMail();
								String mailContent = willSendSuggest.get(i).getSuggest();
								
//								tHMCustSendMailBL.setMailTo(mailTo);
//								tHMCustSendMailBL.setMailTitle(mailTitle);
//								tHMCustSendMailBL.setMailConent(mailContent);
//								if(tHMCustSendMailBL.send()) {
//									sendMailCount ++;
//								}
								if(SendMailFun.TMail(null, mailTo, null, mailTitle, mailContent)) {
									sendMailCount ++;
								}
							}
							//存在合格邮件地址，并且一封邮件也没有发送成功的话，我们认为是邮件发送失败
							if(willSendSuggest.size() > 0 &&sendMailCount == 0) {
								//@@错误处理
								//this.mErrors.copyAllErrors( tLCContDB.mErrors );
								CError tError = new CError();
								tError.moduleName = "HMSuggestBL";
								tError.functionName = "dealData";
								tError.errorMessage = "发送邮件失败!";
								this.mErrors.addOneError(tError);
								return false;
							} else {
								//健康建议表插入
								insertHMSuggestSendTrack(tHMSuggestSendTrackSet);
								if (!prepareOutputData()) {
									return false;
								}
								PubSubmit tPubSubmit = new PubSubmit();
								System.out.println("Start HMSuggestBL Submit...");
								if (!tPubSubmit.submitData(mInputData, "")) {
									CError tError = new CError();
									tError.moduleName = "HMSuggestBL";
									tError.functionName = "submitData";
									tError.errorMessage = "数据库提交失败!";
									this.mErrors.addOneError(tError);
									return false;
								}
							}
							willSendSuggest = null;
						}
						//打印
						if (tSendType.equals("3")) {
							HMSuggestPrintBL tHMSuggestPrintBL = new HMSuggestPrintBL();
							HMSuggestSendTrackSet willSendSuggest = getSuggestMessage(
									tHMSuggestSendTrackSet,"CUST","PRINTSEND");
							
							tmpInputData.add(willSendSuggest);
							if(!tHMSuggestPrintBL.submitData(tmpInputData, "batch")) {
								//@@错误处理
								//this.mErrors.copyAllErrors( tLCContDB.mErrors );
								this.mErrors.copyAllErrors(tHMSuggestPrintBL.mErrors);
								return false;
							} else {
								//健康建议表插入
								insertHMSuggestSendTrack(tHMSuggestSendTrackSet);
								if (!prepareOutputData()) {
									return false;
								}
								PubSubmit tPubSubmit = new PubSubmit();
								System.out.println("Start HMSuggestBL Submit...");
								if (!tPubSubmit.submitData(mInputData, "")) {
									CError tError = new CError();
									tError.moduleName = "HMSuggestBL";
									tError.functionName = "submitData";
									tError.errorMessage = "数据库提交失败!";
									this.mErrors.addOneError(tError);
									return false;
								}
							}
							willSendSuggest = null;
						}

					} while (tSSRS != null && tSSRS.MaxRow > 0);

				} catch (Exception e) {
					e.printStackTrace();
					return false;
				} finally {
					rsWrapper.close();
				}
			}
			
			
			
		}
		
		
		return true;
	}

	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMSuggestBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//符合发送短信条件的健康建议
	private HMSuggestSendTrackSet getSuggestMessage(
					HMSuggestSendTrackSet tHMSuggestSendTrackSet,String disOrCust,String tSendType) {
		//存储可以发送的记录
		HMSuggestSendTrackSet tOKHMSuggestSendTrackSet = new HMSuggestSendTrackSet();
		Vector tVector = new Vector();
			
		int size = tHMSuggestSendTrackSet.size();
		for (int i = 1; i <= size; i++) {
			HMSuggestSendTrackSchema tHMSuggestSendTrackSchema = tHMSuggestSendTrackSet.get(i);
			String tBusinessCode = tHMSuggestSendTrackSchema.getBusinessCode();
			String tMobile = tHMSuggestSendTrackSchema.getMobile();
			String tMail = tHMSuggestSendTrackSchema.getEMail();
			String tAddr = tHMSuggestSendTrackSchema.getHomeAddress();
			String suggestSQL= "";
			
			//按疾病
			if(disOrCust != null && disOrCust.equals("DIS")) {
				//是否存在健康建议
				suggestSQL = " select HealthSuggest from HMSuggestInfo where BusinessType='1'"
						+ " and BusinessCode='" + tBusinessCode + "'";
			}
			//按客户
			if(disOrCust != null && disOrCust.equals("CUST")) {
				//按客户是否存在健康建议
				suggestSQL = " select HealthSuggest from HMSuggestInfo where BusinessType='2'"
					+ " and BusinessCode='" + tBusinessCode + "'";
			}
			
			String suggest = new ExeSQL().getOneValue(suggestSQL);
			if (suggest == null || "".equals(suggest)) {
				tHMSuggestSendTrackSet.get(i).setSendStatus("0");
				tHMSuggestSendTrackSet.get(i).setRemark("代码：" + tBusinessCode + "没有录入健康建议");
				continue;
			} else {
				tHMSuggestSendTrackSet.get(i).setSuggest(suggest);
				tHMSuggestSendTrackSet.get(i).setSendStatus("1");
			}
			
			//手机号码校验
			if (tSendType != null && ("SMSSEND".equals(tSendType))) {
				//手机号码是否正常
				if (tMobile == null || tMobile.equals("")) {
					tHMSuggestSendTrackSet.get(i).setSendStatus("0");
					tHMSuggestSendTrackSet.get(i).setRemark("手机号码为空");
					continue;
				} else {
					tHMSuggestSendTrackSet.get(i).setSendStatus("1");
				}

				tOKHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
			}
			
			//EMail 校验
			if (tSendType != null && ("MAILSEND".equals(tSendType))) {
				//Mail地址是否正常
				if (tMail == null || tMail.equals("")) {
					tHMSuggestSendTrackSet.get(i).setSendStatus("0");
					tHMSuggestSendTrackSet.get(i).setRemark("Email地址为空");
					continue;
				} else {
					tHMSuggestSendTrackSet.get(i).setSendStatus("1");
				}

				tOKHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
			}
			
			//打印 校验
			if (tSendType != null && ("PRINTSEND".equals(tSendType))) {
				//Mail地址是否正常
				if (tAddr == null || tAddr.equals("")) {
					tHMSuggestSendTrackSet.get(i).setSendStatus("0");
					tHMSuggestSendTrackSet.get(i).setRemark("通讯地址为空");
					continue;
				} else {
					tHMSuggestSendTrackSet.get(i).setSendStatus("1");
				}

				tOKHMSuggestSendTrackSet.add(tHMSuggestSendTrackSchema);
			}
			
		}
		return tOKHMSuggestSendTrackSet;
	}

	//向轨迹表中插入发送记录
	private void insertHMSuggestSendTrack(
			HMSuggestSendTrackSet mHMSuggestSendTrackSet) {
		int size = mHMSuggestSendTrackSet.size();
		for (int i = 1; i <= size; i++) {
			//生成流水号
			String tLimit = PubFun.getNoLimit(mManageCom);
			String tSerialNo = "";
			tSerialNo = PubFun1.CreateMaxNo("HMSEND", tLimit);
			System.out.println("**********tSerialNo=" + tSerialNo);
			mSendResult = mSendResult + ",'" +tSerialNo + "'";
			
			mHMSuggestSendTrackSet.get(i).setSerialNo(tSerialNo);
			mHMSuggestSendTrackSet.get(i).setMakeDate(PubFun.getCurrentDate());
			mHMSuggestSendTrackSet.get(i).setMakeTime(PubFun.getCurrentTime());
			mHMSuggestSendTrackSet.get(i).setManageCom(mManageCom);
			mHMSuggestSendTrackSet.get(i).setOperator(mOperator);
			mHMSuggestSendTrackSet.get(i).setModifyDate(PubFun.getCurrentDate());
			mHMSuggestSendTrackSet.get(i).setModifyTime(PubFun.getCurrentTime());
		}
		map.put(mHMSuggestSendTrackSet, "INSERT");
	}

	/**
	 * 疾病健康建议全部发送时，根据前台的查询条件返回结果集
	 * 
	 * */
	public boolean disQueryCollection(HashMap tDisQueryCollectionSQL) {
		//	查询条件
		String tDQ_LastVisStartDate = (String) mTransferData.getValueByName("DQ_LastVisStartDate");
		String tDQ_LastVisEndDate = (String) mTransferData.getValueByName("DQ_LastVisEndDate");
		String tDQ_DiseaseCode = (String) mTransferData.getValueByName("DQ_DiseaseCode");
		String tDQ_LastVisCount = (String) mTransferData.getValueByName("DQ_LastVisCount");
		String tDQ_HCostClaimFee = (String) mTransferData.getValueByName("DQ_HCostClaimFee");
		String tDQ_VistType = (String) mTransferData.getValueByName("DQ_VistType");
		String tDQ_SendType = (String) mTransferData.getValueByName("DQ_SendType");
		String tDQ_IsALSend = (String) mTransferData.getValueByName("DQ_IsALSend");

		//最近一次就诊时间
		String lastVistDateCondition = "";
		//疾病代码
		String disCodeCondition = "";
		//最近一年就诊次数
		String LastVisCountCondition = "";
		//最近一年医疗费用理赔额度
		String LastYearMedRealPayCondition = "";
		//就诊方式
		String visttypecondition = "";
		String tSQL = "";
		//查询未发送的客户
		if (tDQ_SendType == null || tDQ_SendType.equals("")) {

			try {
				//最近一次就诊时间
				//String lastVistDateCondition = "";
				if ((tDQ_LastVisStartDate != null && !"".equals(tDQ_LastVisStartDate))
						|| (tDQ_LastVisEndDate != null && !"".equals(tDQ_LastVisEndDate))) {

					if (tDQ_LastVisStartDate != null && !"".equals(tDQ_LastVisStartDate)) {
						lastVistDateCondition = " and xa.lastFeeDate>='"
								+ tDQ_LastVisStartDate + "'";
					}
					if (tDQ_LastVisEndDate != null && !"".equals(tDQ_LastVisEndDate)) {
						lastVistDateCondition = " and xa.lastFeeDate<='"
								+ tDQ_LastVisEndDate + "'";
					}
					if ((tDQ_LastVisStartDate != null && !"".equals(tDQ_LastVisStartDate))
							&& (tDQ_LastVisEndDate != null && !"".equals(tDQ_LastVisEndDate))) {
						lastVistDateCondition = " and xa.lastFeeDate between '"
													+ tDQ_LastVisStartDate + "' " + " and '"
													+ tDQ_LastVisEndDate + "' ";
					}
				}
				//疾病代码
				//String disCodeCondition = "";
				if (tDQ_DiseaseCode != null && !"".equals(tDQ_DiseaseCode)) {
					disCodeCondition = " and b.DiseaseCode='" + tDQ_DiseaseCode + "'";
				}

				//最近一年就诊次数
				//String LastVisCountCondition = "";
				if (tDQ_LastVisCount != null && !tDQ_LastVisCount.equals("")) {
					LastVisCountCondition = " and coalesce(xa.Illcount,0)>="+ Integer.parseInt(tDQ_LastVisCount);
				}

				//最近一年医疗费用理赔额度
				//String LastYearMedRealPayCondition = "";
				if (tDQ_HCostClaimFee != null && !tDQ_HCostClaimFee.equals("")) {
					LastYearMedRealPayCondition = " and coalesce(xa.Realpay,0)>="
							+ Integer.parseInt(tDQ_HCostClaimFee);
				}

				//就诊方式
				//String visttypecondition ="";
				if (tDQ_VistType != null && !tDQ_VistType.equals("")) {
					//门诊
					if (tDQ_VistType.equals("2")) {
						visttypecondition = " and b.FeeType='1'";
					}
					//住院
					if (tDQ_VistType.equals("3")) {
						visttypecondition = " and b.FeeType='2' ";
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				//@@错误处理
				CError tError = new CError();
				tError.moduleName = "HMSuggestBL";
				tError.functionName = "dealData";
				tError.errorMessage = "查询条件出现异常！";
				this.mErrors.addOneError(tError);
				return false;

			}

			tSQL = " select distinct a.insuredno as CustomerNo, "
					+ " a.name as Name, "
					+ " a.mobile as Mobile, "
					+ " a.email as EMail, "
					+ " a.homeaddress as HomeAddress, "
					+ " b.DiseaseCode as BusinessCode, "
					+ " xa.lastFeeDate as LastVisEndDate, "
					+ " coalesce(xa.Illcount, 0) as LastYearVisCount, "
					+ " coalesce(xa.Realpay, 0) as LastYearMedRealPay, "
					//+ " b.FeeType as VisType "
					+" '' "
					
					+ " from HMSENDCUSTDETAIL a, "
					//+ " left outer join LLCUSTOMERCLAIM xa on xa.CustomerNo = a.InsuredNo, "
					+ " LLCUSTOMERCLAIM xa, "
					+ " HMCLAIMDETAIL b "
					+ " where a.insuredno = b.customerno "
					+ " and xa.CustomerNo = a.InsuredNo "
					+ " and not exists(select 1 from HMSuggestSendTrack xe "
					+ " where xe.CustomerNo=a.InsuredNo and BusinessType = '1' "
					+ " and xe.BusinessCode = b.DiseaseCode and xe.SendStatus='1' "
									+" and xe.SendType='"+ mSendType +"') "

					+ lastVistDateCondition 
					+ disCodeCondition
					+ LastVisCountCondition 
					+ LastYearMedRealPayCondition
					+ visttypecondition + " with ur ";

			//已发送的，从轨迹表中查询
		} else {
			try {
				//最近一次就诊时间
				//String lastVistDateCondition = "";
				if ((tDQ_LastVisStartDate != null && !"".equals(tDQ_LastVisStartDate))
						|| (tDQ_LastVisEndDate != null && !"".equals(tDQ_LastVisEndDate))) {

					if (tDQ_LastVisStartDate != null && !"".equals(tDQ_LastVisStartDate)) {
						lastVistDateCondition = " and a.LastVisEndDate>='" + tDQ_LastVisStartDate + "'";
					}
					if (tDQ_LastVisEndDate != null && !"".equals(tDQ_LastVisEndDate)) {
						lastVistDateCondition = " and a.LastVisEndDate<='" + tDQ_LastVisEndDate + "'";
					}
					if ((tDQ_LastVisStartDate != null && !"".equals(tDQ_LastVisStartDate))
							&& (tDQ_LastVisEndDate != null && !"".equals(tDQ_LastVisEndDate))) {
						lastVistDateCondition = " and a.LastVisEndDate between '"
								+ tDQ_LastVisStartDate + "' and '" + tDQ_LastVisEndDate + "' ";
					}
				}
				//疾病代码
				//String disCodeCondition = "";
				if (tDQ_DiseaseCode != null && !"".equals(tDQ_DiseaseCode)) {
					disCodeCondition = " and a.BusinessCode='" + tDQ_DiseaseCode + "'";
				}

				//最近一年就诊次数
				//String LastVisCountCondition = "";
				if (tDQ_LastVisCount != null && !tDQ_LastVisCount.equals("")) {
					LastVisCountCondition = " and a.LastYearVisCount>=" + Integer.parseInt(tDQ_LastVisCount);
				}

				//最近一年医疗费用理赔额度
				//String LastYearMedRealPayCondition = "";
				if (tDQ_HCostClaimFee != null && !tDQ_HCostClaimFee.equals("")) {
						LastYearMedRealPayCondition = " and a.LastYearMedRealPay>="
															+ Integer.parseInt(tDQ_HCostClaimFee);
				}

				//就诊方式
				//String visttypecondition ="";
				if (tDQ_VistType != null && !tDQ_VistType.equals("")) {
					//门诊
					if (tDQ_VistType.equals("2")) {
						visttypecondition = " and a.VisType='1'";
					}
					//住院
					if (tDQ_VistType.equals("3")) {
						visttypecondition = " and a.VisType='2' ";
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				//@@错误处理
				CError tError = new CError();
				tError.moduleName = "HMSuggestBL";
				tError.functionName = "dealData";
				tError.errorMessage = "查询条件出现异常！";
				this.mErrors.addOneError(tError);
				return false;

			}
			tSQL = " select distinct CustomerNo,Name,Mobile,EMail,HomeAddress,BusinessCode, "
					+ " LastVisEndDate,LastYearVisCount,LastYearMedRealPay,VisType "
					+ " from HMSuggestSendTrack a where BusinessType='1' and SendStatus='1' "
												+" and SendType ='"+ tDQ_SendType +"' "
					+ lastVistDateCondition
					+ disCodeCondition
					+ LastVisCountCondition
					+ LastYearMedRealPayCondition
					+ visttypecondition + " with ur ";

		}

		System.out.println(tSQL);
		tDisQueryCollectionSQL.put("DisQueryCollectionSQL", tSQL);
		return true;
	}

	/**
	 * 客户健康建议全部发送时，根据前台的查询条件返回结果集
	 * 
	 * */
	public boolean custQueryCollection(HashMap tCustQueryCollectionSQL) {
		//	查询条件
		 String tCQ_CustomerName = (String)mTransferData.getValueByName("CQ_CustomerName");
		 String tCQ_CCustomerNo = (String)mTransferData.getValueByName("CQ_CCustomerNo");
		 String tCQ_CCustSuggestStartDate = (String)mTransferData.getValueByName("CQ_CCustSuggestStartDate");
		 String tCQ_CCustSuggestEndDate = (String)mTransferData.getValueByName("CQ_CCustSuggestEndDate");
		 String tCQ_CSendType = (String)mTransferData.getValueByName("CQ_CSendType");
		 String tCQ_isCALSend = (String)mTransferData.getValueByName("CQ_isCALSend");

		//客户名
		String tCQ_CustomerCondition = "";
		//客户号
		String tCQ_CCustomerNoCondition = "";
		//健康建议建立起始时间
		String tCQ_CCustSuggestStartDateCondition = "";
		//健康建议建立结束时间
		String tCQ_CCustSuggestEndDateCondition = "";
		//发送方式
		String tCQ_CSendTypeCondition = "";
		
		String tSQL = "";
		//查询未发送的客户
		if (tCQ_CSendType == null || tCQ_CSendType.equals("")) {
					
			//客户姓名
			if(tCQ_CustomerName != null && !"".equals(tCQ_CustomerName)) {
				tCQ_CustomerCondition = " and a.BusinessName='" + tCQ_CustomerName + "'";
			}
			
			//客户号
			if(tCQ_CCustomerNo != null && !"".equals(tCQ_CCustomerNo)) {
				tCQ_CCustomerNoCondition = " and a.BusinessCode='" + tCQ_CCustomerNoCondition + "'";
			}
			
			//客户健康建议录入时间起期
			if(tCQ_CCustSuggestStartDate != null && !"".equals(tCQ_CCustSuggestStartDate)) {
				tCQ_CCustSuggestStartDateCondition = " and a.MakeDate >='" + tCQ_CCustSuggestStartDate + "'";
			}
			
			//客户健康建议录入时间止期
			if(tCQ_CCustSuggestEndDate != null && !"".equals(tCQ_CCustSuggestEndDate)) {
				tCQ_CCustSuggestEndDateCondition = " and a.MakeDate <='" + tCQ_CCustSuggestEndDate + "'";
			}
			
			tSQL = " select distinct a.BusinessName,a.BusinessCode, b.mobile,b.Email,b.homeaddress, a.MakeDate, "
								+" '',coalesce(xa.Illcount, 0),'','',coalesce(xa.Realpay, 0) "
				+ " from HMSuggestInfo a,"
					+" HMSENDCUSTDETAIL b, "
					//+" left outer join LLCUSTOMERCLAIM xa on xa.CustomerNo = b.insuredno " 
					+" LLCUSTOMERCLAIM xa "
					+ " where a.BusinessCode = b.insuredno "
						+" and xa.CustomerNo = b.insuredno "
						+" and a.BusinessType='2' "
						+" and not exists ("
						+" select 1 from HMSuggestSendTrack where BusinessType='2' "
									+" and BusinessCode=a.BusinessCode) "
				+ tCQ_CustomerCondition
				+ tCQ_CCustomerNoCondition
				+ tCQ_CCustSuggestStartDateCondition
				+ tCQ_CCustSuggestEndDateCondition;
		
			
			//已发送的，从轨迹表中查询
		} else {
			
			//客户姓名
			if(tCQ_CustomerName != null && !"".equals(tCQ_CustomerName)) {
				tCQ_CustomerCondition = " and b.Name='" + tCQ_CustomerName + "'";
			}
			
			//客户号
			if(tCQ_CCustomerNo != null && !"".equals(tCQ_CCustomerNo)) {
				tCQ_CCustomerNoCondition = " and b.CustomerNo='" + tCQ_CCustomerNo + "'";
			}
			
			//客户健康建议录入时间起期
			if(tCQ_CCustSuggestStartDate != null && !"".equals(tCQ_CCustSuggestStartDate)) {
				tCQ_CCustSuggestStartDateCondition = " and a.MakeDate >='" 
												+ tCQ_CCustSuggestStartDate + "'";
			}
			
			//客户健康建议录入时间止期
			if(tCQ_CCustSuggestEndDate != null && !"".equals(tCQ_CCustSuggestEndDate)) {
				tCQ_CCustSuggestEndDateCondition = " and a.MakeDate <='" 
							+ tCQ_CCustSuggestEndDate + "'";
			}
			
			//发送方式
			if(tCQ_CSendType != null && !"".equals(tCQ_CSendType)) {
				tCQ_CSendTypeCondition = " and b.SendType ='" + tCQ_CSendType + "'";
			}
			
			tSQL = " select distinct b.Name,b.CustomerNo,b.Mobile,b.EMail,b.HomeAddress,a.MakeDate, "
					+" b.LastVisEndDate,b.LastYearVisCount,b.VisType,'',LastYearMedRealPay "
					+" from HMSuggestInfo a,HMSuggestSendTrack b "
					+" where a.BusinessType = '2' and a.BusinessCode = b.BusinessCode and b.BusinessType='2' "
					+ tCQ_CustomerCondition
					+ tCQ_CCustomerNoCondition
					+ tCQ_CCustSuggestStartDateCondition
					+ tCQ_CCustSuggestEndDateCondition
					+ tCQ_CSendTypeCondition;

		}

		System.out.println(tSQL);
		tCustQueryCollectionSQL.put("CustQueryCollectionSQL", tSQL);
		return true;
	}
	
	/**
	 * 返回结果
	 * */
	public VData getResult() {
		mResult.clear();
		mResult.add(mSendResult);
		return mResult;
	}
	
	public static void main(String[] args) {
    	
	}
}
