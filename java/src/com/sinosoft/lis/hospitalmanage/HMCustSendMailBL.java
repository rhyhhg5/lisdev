package com.sinosoft.lis.hospitalmanage;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public class HMCustSendMailBL {
    /** 邮件发送者 */
    private String mailTo = "";
    /** 邮件抄送者 */
    private String mailCC = "";
    /** 邮件内容 */
    private String mailConent = "";
    /** 邮件主题 */
    private String mailTitle = "";
    
	/** Smtp服务器配置 */
    private String MAIL_PASSWORD;
    private String MAIL_USER;
    private String MAIL_SERVER;
    private String MAIL_SMTP_FROM_NAME;
    private String MAIL_SMTP_FROM_MAIL;
    public CErrors mErrors = new CErrors();
    
	public HMCustSendMailBL() {
		try {
			initServer();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
	}
	
	 /**
     * initServer
     *
     * @return boolean
     */
    private boolean initServer() {
        StringBuffer sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_PASSWORD'");
        MAIL_PASSWORD = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_USER'");
        MAIL_USER = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_SERVER'");
        MAIL_SERVER = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_FROM_MAIL'");
        MAIL_SMTP_FROM_MAIL = (new ExeSQL()).getOneValue(sql.toString());
        sql = new StringBuffer(255);
        sql.append(
                "SELECT SYSVARVALUE FROM LDSYSVAR WHERE SYSVAR='MAIL_SMTP_FROM_NAME'");
        MAIL_SMTP_FROM_NAME = (new ExeSQL()).getOneValue(sql.toString());

        if (MAIL_PASSWORD == null || MAIL_PASSWORD == null || MAIL_PASSWORD == null ||
            MAIL_SMTP_FROM_MAIL == null || MAIL_SMTP_FROM_NAME == null) {
            buildError("initServer", "邮件服务信息配置错误,请检查是否已经配置有mail服务信息！");
            return false;
        }
        return true;
    }
	
	public boolean send() {
		
		Properties props = System.getProperties();//得到系统属性  
	    props.put("mail.smtp.host",MAIL_SERVER);//压入发送邮件服务器(SMTP)  
	    props.put("mail.smtp.auth", "true"); //这样才能通过验证  
	    MyAuthenticator myauth = new MyAuthenticator(MAIL_USER,MAIL_PASSWORD);  
	    Session session = Session.getDefaultInstance(props, myauth);//获取默认会话  
	    try{  
	
	        MimeMessage msg = new MimeMessage(session);//建立邮件  
	        msg.setFrom(new InternetAddress(MAIL_SMTP_FROM_MAIL));
	        
	        //添加收件人
	        InternetAddress[] iatoList = InternetAddress.parse(mailTo);
	        //System.out.println("to = " + to);
	        msg.setRecipients(Message.RecipientType.TO,iatoList);
	        
	        //添加抄送
	        if (mailCC != null && !mailCC.equals("")) {
	     	
	        	InternetAddress[] iaCCList = InternetAddress.parse(mailCC);
	        	msg.setRecipients(Message.RecipientType.CC,iaCCList);
	        }
	        
	        msg.setSubject(mailTitle);  
	        MimeBodyPart mbp1 = new MimeBodyPart();//用于存放文本内容  
	        mbp1.setText(mailConent);  
	        MimeMultipart mimemultipart = new MimeMultipart();  
	        mimemultipart.addBodyPart(mbp1);//加入文字内容     
	        
	        //压入附件内容  
	        msg.setContent(mimemultipart);  
	        //压入当前时间  
	        msg.setSentDate(new Date());  
	        //发送邮件  
	        Transport.send(msg);
	        System.out.println("发送mail成功！");
	        return true;
	    }   catch (Exception mex) {  
	    	System.out.println("发送mail失败！");
	    	mex.printStackTrace(); 
	    	return false;
	    }  
	}
	
	/**
	 * 接收发送者
	 * */
	public void setMailTo(String to) {
		this.mailTo = to;
	}
	
	/**
	 * 接收发送内容
	 * */
	public void setMailConent(String conent) {
		this.mailConent = conent;
	}
	
	/**
	 * 接收抄送人
	 * */
	public void setMailCC(String CC) {
		this.mailCC = CC;
	}
	
	/**
	 * 接收主题
	 * */
	public void setMailTitle(String title) {
		this.mailTitle = title;
	}
	
	/**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "SendMail";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HMCustSendMailBL tHMCustSendMailBL = new HMCustSendMailBL();
		tHMCustSendMailBL.setMailTitle("邮件测试");
		tHMCustSendMailBL.setMailConent("这是一封测试邮件");
		tHMCustSendMailBL.setMailTo("chenxw@sinosoft.com.cn");
		tHMCustSendMailBL.send();
	}

}
