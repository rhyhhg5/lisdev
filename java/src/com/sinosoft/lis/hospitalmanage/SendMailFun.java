package com.sinosoft.lis.hospitalmanage;
/**
 * 发送邮件共用类
 */
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.ExportExcel;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.mail.*;

public class SendMailFun {
   
   /**
    * 发送邮件
    * 参数：附件地址(可用分号隔开)，发送人(可用分号隔开)，抄送人(可用分号隔开)，主题，内容
    * author:chenxw
    */
   public static boolean TMail(String appentPath,String recipient,String carbonCopy,String title,String body)
   {
	   
	   List tlist = new ArrayList();
	   tlist.add(appentPath);
	   tlist.add(recipient);
	   tlist.add(carbonCopy);
	   tlist.add(title);
	   tlist.add(body);
	   
	try {		
			String path = appentPath;
			String tRecipient =recipient;
			String tCarbonCopy = carbonCopy;
			String tTitle = title;
			String tBody = body;
			
			
			SendMail tSendMail = new SendMail();
			if (tRecipient != null && !tRecipient.equals("")) {
				tSendMail.setToAddress(tRecipient);
			}
			
			if (tCarbonCopy != null && !tCarbonCopy.equals("")) {
				tSendMail.setCcAddress(tCarbonCopy);
			}
			
			System.out.println(path);
			if(path != null && path.equals("")) {
				File file = new File(path);
				if (!file.exists()) {
		            return false;
				} else {
					tSendMail.setFile(path);
				}
			}
				tSendMail.send(tBody, tTitle);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	} 
	
   /**
    * path:后缀名：zip
    * file:要压缩的文件数组
    */
   public static boolean zip(String[] file,String path)
   {
     try{
     	  FileOutputStream f=new FileOutputStream(path);
     	  ZipOutputStream out=null;
		  CheckedOutputStream ch=new CheckedOutputStream(f,new CRC32());
		  out=new ZipOutputStream(new BufferedOutputStream(ch));
     	  for(int i=0;i<file.length;i++)
     	  {
	           BufferedReader in=new BufferedReader(
	           new InputStreamReader(new FileInputStream(file[i]),"ISO8859_1"));
			      int c;
			      
			      File tfile=new File(file[i]);//如果不这样，会把路径也压缩进去的
			      String filename=tfile.getName();
			      
			      
			      out.putNextEntry(new ZipEntry(filename));
			      while((c=in.read())!=-1)
			         out.write(c);
			         out.closeEntry();
			         in.close();
			         
     	  }
     	  out.close();
        }
       catch(Exception e){
           e.printStackTrace();
           return false;
       }
       return true;
   }
   
   /**
    *  查询是否邮件出现问题要阻断流程
    */
   public static boolean EmaiExp_IsNeedBreak()
   {
	   try
	   {
		   LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		   tLDSysVarDB.setSysVar("Email_Exp_IsBreak");
		   String IsNeedBreakStr=(tLDSysVarDB.query().get(1)).getSysVarValue();
		   if(IsNeedBreakStr==null || IsNeedBreakStr.equals(""))
		   {
			   return false;
		   }
		   if(IsNeedBreakStr!=null && IsNeedBreakStr.equals("Y"))
		   {
			   return true;
		   }
	   }catch(Exception ex)
	   {
		   return false;
	   }
	   
	   return false;
   }
   
   public static boolean VerifyIsNulls(List list)
   {
	   for(int i=0;i<list.size();i++)
	   {
		   String str=(String)list.get(i);
		   if(!VerifyIsNull(str))
		   {
			   return false;
		   }
	   }
	   return true;
   }
   private static boolean VerifyIsNull(String str)
   {
	   if(str==null || str.equals(""))
	   {
		   return false;
	   }
	   return true;
   }
  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {		
			if(!SendMailFun.TMail(null, "chenxw@sinosoft.com.cn", null, "测试", "这是一封测试邮件")) {
				System.out.println("发送邮件失败！");
			} else {
				System.out.println("发送成功");
			}
			
		} catch (Exception ex) {
			System.out.println(ex);
			ex.printStackTrace();
			//return false;
		}
	}

}