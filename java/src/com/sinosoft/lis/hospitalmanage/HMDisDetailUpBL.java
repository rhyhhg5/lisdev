/**
 * <p>ClassName: HMDisDetailUpBL.java </p>
 * <p>Description: 添加客户档案明细 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author:chenxw
 * @version 1.0
 * @CreateDate：2010-3-8
 */
 
//包名
package com.sinosoft.lis.hospitalmanage;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class HMDisDetailUpBL  {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 提交数据的Map类 */
    private MMap map = new MMap();
    
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private TransferData mTransferData = new TransferData();
    
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    /** 数据操作字符串 */
    private String mOperator; //获取操作员代码
    private String mOperate;
    private String mManageCom;
    
    /** 业务操作类 */
    //客户疾病档案信息
    HMCustDisDocInfoSet mHMCustDisDocInfoSet = new HMCustDisDocInfoSet();

    public HMDisDetailUpBL()
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
            return false;

        //校验
        if (!checkData())
            return false;
        
        //进行业务处理
        if (!dealData())
            return false;

        if(!prepareOutputData())
            return false;

        PubSubmit tPubSubmit = new PubSubmit();

        if(!tPubSubmit.submitData(mInputData,""))
        {
            CError tError = new CError();
            tError.moduleName = "HMDisDetailUpBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据库提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        } 

        System.out.println("End Submit....");
        return true;
    }


    /**
     * dealData
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
    	//病例档案保存
    	if(mOperate.equals("DISSAVE||MAIN")) {  //保存
    		int disSize = mHMCustDisDocInfoSet.size();
    		for (int i=1; i<=disSize; i++) {
    		   //生成流水号
		       String tLimit = PubFun.getNoLimit(mManageCom);
		       String tSerialNo = "";
		       tSerialNo = PubFun1.CreateMaxNo("DISNO", tLimit);
		       System.out.println("**********tSerialNo=" + tSerialNo);
		       
		       mHMCustDisDocInfoSet.get(i).setSerialNo(tSerialNo);
		       mHMCustDisDocInfoSet.get(i).setOperator(mOperator);
		       mHMCustDisDocInfoSet.get(i).setManageCom(mManageCom);
		       mHMCustDisDocInfoSet.get(i).setMakeDate(PubFun.getCurrentDate());
		       mHMCustDisDocInfoSet.get(i).setMakeTime(PubFun.getCurrentTime());
		       mHMCustDisDocInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
		       mHMCustDisDocInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
		       
		       map.put(mHMCustDisDocInfoSet, "INSERT");
    		}
    	}
    	
    	//病例档案修改
    	if(mOperate.equals("DISMODIFY||MAIN")) {  //修改
    		
    		int disSize = mHMCustDisDocInfoSet.size();
    		for (int i=1; i<=disSize; i++) {
    		   HMCustDisDocInfoSchema tHMCustDisDocInfoSchema = new HMCustDisDocInfoSchema();
    		   HMCustDisDocInfoDB tHMCustDisDocInfoDB = new HMCustDisDocInfoDB();
    		   
    		   tHMCustDisDocInfoDB.setSerialNo(mHMCustDisDocInfoSet.get(i).getSerialNo());
    		   if(tHMCustDisDocInfoDB.getInfo()) {
    			   tHMCustDisDocInfoSchema = tHMCustDisDocInfoDB.getSchema();
   				   
    			   mHMCustDisDocInfoSet.get(i).setOperator(mOperator);
    			   mHMCustDisDocInfoSet.get(i).setManageCom(mManageCom);
    			   mHMCustDisDocInfoSet.get(i).setMakeDate(tHMCustDisDocInfoSchema.getMakeDate());
    		       mHMCustDisDocInfoSet.get(i).setMakeTime(tHMCustDisDocInfoSchema.getMakeTime());
    			   mHMCustDisDocInfoSet.get(i).setModifyDate(PubFun.getCurrentDate());
    			   mHMCustDisDocInfoSet.get(i).setModifyTime(PubFun.getCurrentTime());
    		   } else {
    			   //@@错误处理
    	           //this.mErrors.copyAllErrors( tLCContDB.mErrors );
    	           CError tError = new CError();
    	           tError.moduleName = "HMDisDetailUpBL";
    	           tError.functionName = "deaData";
    	           tError.errorMessage = "没有查询到要修改的病例档案信息！";
    	           this.mErrors.addOneError(tError);
    	           return false;
    		   }
    		}
    		map.put(mHMCustDisDocInfoSet, "UPDATE");
    	}
    	
    	//病例档案删除
    	if(mOperate.equals("DISDEL||MAIN")) {  //删除
    		int size = mHMCustDisDocInfoSet.size();
    		for(int i=1; i<=size; i++) {
    			String tSerialNo = mHMCustDisDocInfoSet.get(i).getSerialNo();
    			String tDocIdSQL = "select docid from Es_Doc_main " 
    						+ " where busstype='HM' and Subtype='HM21' and docCode='"+ tSerialNo +"' with ur";
    			String tDocId = new ExeSQL().getOneValue(tDocIdSQL);
    			
    			if(tDocId != null && !"".equals(tDocId)) {
    				
    				String delEs_Doc_Pages = " delete from Es_Doc_Pages where docid= " + tDocId ;
        			String delEs_Doc_Main = " delete from Es_Doc_main "
        						+ " where busstype='HM' and Subtype='HM21' and docCode='"+ tSerialNo +"'";
        			String delES_DOC_RELATION = " delete from ES_DOC_RELATION "
        						+ " where docid=" + tDocId + " and BussNoType='10' and BussNo='" + tSerialNo + "'";
        			String delLHScanInfo = " delete from LHScanInfo where SerialNo='" + tSerialNo + "'";
        			
        			map.put(delEs_Doc_Pages, "DELETE");
        			map.put(delEs_Doc_Main, "DELETE");
        			map.put(delES_DOC_RELATION, "DELETE");
        			map.put(delLHScanInfo, "DELETE");
    			}
    		}
    		map.put(mHMCustDisDocInfoSet, "DELETE");
    	}
        return true;
    }

    /**
     * 将所需业务处理完的结果信息返回到前台
     * @param : cInputData  输入的数据
     * @return: mResult     返回的保存结果的容器
     */
    public VData getResult()
    {
        return this.mResult;
    }
    
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
    	 if (mGlobalInput == null)
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDetailUpBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         
         if (mOperator == null || mOperator.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDetailUpBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据Operate失败!";
             this.mErrors.addOneError(tError);
             return false;
         }

         if (mManageCom == null || mManageCom.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDetailUpBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据mManageCom失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         
         if (mOperate == null || mOperate.trim().equals(""))
         {
             // @@错误处理
             //this.mErrors.copyAllErrors( tLCContDB.mErrors );
             CError tError = new CError();
             tError.moduleName = "HMDisDetailUpBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "前台传输全局公共数据mOperate失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
    	return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
    	 try
         {
             mInputData.clear();
             mInputData.add(map);
         }
         catch (Exception ex)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "HMDisDetailUpBL";
             tError.functionName = "prepareOutputData";
             tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
             this.mErrors.addOneError(tError);
             return false;
         }
         return true;
    }


    /**
     * getInputData
     * 得到前台传输的数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        
        mHMCustDisDocInfoSet = (HMCustDisDocInfoSet) cInputData.getObjectByObjectName("HMCustDisDocInfoSet",0);
        
        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        //获取操作机构
        mManageCom = mGlobalInput.ManageCom;
        mOperate = cOperate;
        
        return true;
    }

}
