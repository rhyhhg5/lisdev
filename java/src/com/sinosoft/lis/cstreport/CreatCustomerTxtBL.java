package com.sinosoft.lis.cstreport;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CreatCustomerTxtBL {
	
	private String mURL = "";
	public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private MMap map = new MMap();
    
    private String mCstDate = "";
    
    private TransferData mTransferData = null;
    private BufferedWriter mBufferedWriter;
    
    private String mOperate = "";
    
    public CreatCustomerTxtBL()
    {
    }
	
	/**
     * 外部提交的方法，本方法不直接处理业务
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	mOperate = cOperate;
    	cInputData = (VData) cInputData.clone();
        if (getSubmitMap(cInputData) == null)
        {
            return false;
        }

        return true;
    }
    
    /**
     * 外部提交的方法，返回处理后的MMap集合
     * @param cInputData VData
     * @return boolean
     */
    public MMap getSubmitMap(VData cInputData)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }
    
    /**
     * 将传入的外包数据分解到本类的全局变量
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        if(mGI == null)
        {
            mErrors.addOneError("页面超时，请重登录");
            return false;
        }
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
        	mErrors.addOneError( "所需参数不完整。");
            return false;
        }
        mCstDate = (String)mTransferData.getValueByName("CstDate"); 
        
        if(mCstDate == null || "".equals(mCstDate)){
        	mErrors.addOneError("获取提数时间失败！");
            return false;
        }
        mURL = (String)mTransferData.getValueByName("ExportPath");

        return true;
    }
    /**
     * 进行数据合法性的校验
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }
    
    /**
     * 进行数据合法性的校验
     * @return boolean
     */
    private boolean dealData()
    {
    	
//    	this.mURL = "D:\\customertxt\\";
    	this.mURL = "F:\\王娜娜\\开发\\契约\\2015\\#2665 第三季度客户数\\正式提取\\";
    	
    	
    	if("psn".equals(mOperate)){
    		getS001();
    	}else if("grp".equals(mOperate)){
    		getS002();
    	}else if("Summary".equals(mOperate)){
    		getS003();
    	}
    	
    	return true;
    }
	/**
     * 个人客户
     */
    private void getS001()
    {
        try
        {
        	System.out.println("开始提取" + mCstDate + "个人客户数据,开始时间是" +PubFun.getCurrentTime());
        	String tDate = mCstDate.replaceAll("-", "").substring(0, 6);
        	FileOutputStream tFileOutputStream = new FileOutputStream(mURL + tDate+"000085psn.txt", true);
            String tSQL ="select CstDate,CompCod,CstNo,CstID,CstName,CstType,CstBirthday,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '0' and cinvalidate >='2014-10-01' and serno='2013_12_252042.00' group by CstDate,CompCod,CstNo,CstID,CstName,CstType,CstBirthday,RiskType,CstZone,CstState  with ur";
            int start = 1;
            int nCount = 5000;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                //默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
//            mFileWriter = new FileWriter("C:\\000085S001" +
//                                         mCurrentDate.replaceAll("-", "") +
//                                         ".txt");
//            System.out.println(mFileWriter.getEncoding());
//            mBufferedWriter = new BufferedWriter(mFileWriter);
//          指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
                tFileOutputStream = new FileOutputStream(mURL + tDate+"000085psn.txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    String t = tempStringArr[i].replaceAll("\\|", ",");
                    String[] tArr = t.split("\\,");
                    for (int j = 0; j < tArr.length; j++)
                    {
                        mBufferedWriter.write(tArr[j] + "||");
                    }
                    mBufferedWriter.newLine();
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            System.out.println("提取" + mCstDate + "个人客户数据完成,完成时间是" +PubFun.getCurrentTime());
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }
    
    /**
     * 机构客户
     */
    private void getS002()
    {
        try
        {
        	System.out.println("开始提取" + mCstDate + "机构客户数据,开始时间是" +PubFun.getCurrentTime());
        	long t1 = System.currentTimeMillis();
        	String tDate = mCstDate.replaceAll("-", "").substring(0, 6);
        	FileOutputStream tFileOutputStream = new FileOutputStream(mURL + tDate+"000085grp.txt", true);
        	// 带主键，便于业务获取删除数据
//            String tSQL ="select SerNo,CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '1' and ((cinvalidate >='2015-07-01' and cvalidate >='2015-07-01' and cvalidate <='2015-09-30') or  SerNo like 'B2015_3%' ) group by SerNo, CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState order by length(CstName)  with ur";
        	
//        	String tSQL ="select SerNo,CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '1' and SerNo like 'B%'  group by SerNo, CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState order by length(CstName)  with ur";
            
//            //不带主键，正规提数。
            String tSQL ="select CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '1' and cinvalidate >='2014-10-01' group by CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState order by length(CstName)  with ur";
            int start = 1;
            int nCount = 5000;
            while (true)
            {
                SSRS tSSRS = new ExeSQL().execSQL(tSQL, start, nCount);
                if (tSSRS.getMaxRow() <= 0)
                {
                    break;
                }
                String tempString = tSSRS.encode();
                String[] tempStringArr = tempString.split("\\^");
                //默认字符集，这里通过mFileWriter.getEncoding()测试是GBK
//            mFileWriter = new FileWriter("C:\\000085S001" +
//                                         mCurrentDate.replaceAll("-", "") +
//                                         ".txt");
//            System.out.println(mFileWriter.getEncoding());
//            mBufferedWriter = new BufferedWriter(mFileWriter);
//          指定字符集，可以使用FileOutputStream指定输出文件，在OutputStreamWriter的构造函数中指定字符集,下面的例子中制定GBK
                tFileOutputStream = new FileOutputStream(mURL + tDate+"000085grp.txt", true);
                OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(
                        tFileOutputStream, "GBK");
                mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
                for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                {
                    String t = tempStringArr[i].replaceAll("\\|", ",");
                    String[] tArr = t.split("\\,");
                    for (int j = 0; j < tArr.length; j++)
                    {
                        mBufferedWriter.write(tArr[j] + "||");
                    }
                    mBufferedWriter.newLine(); 	
                    mBufferedWriter.flush();
                }
                mBufferedWriter.close();
                start += nCount;
            }
            System.out.println("提取" + mCstDate + "机构客户数据完成,用时"+ (System.currentTimeMillis() - t1));
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }
    
    /**
     * 汇总数据
     */
    private void getS003()
    {
        try
        {
        	System.out.println("开始提取" + mCstDate + "汇总数据,开始时间是" +PubFun.getCurrentTime());
        	String tDate = mCstDate.replaceAll("-", "").substring(0, 6);
        	FileOutputStream tFileOutputStream = new FileOutputStream(mURL + tDate+"000085Summary.txt", true);
            //String tSQL ="select CstDate,CompCod,CstID,CstName,CstType,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '1' group by CstDate,CompCod,CstID,CstName,CstType,RiskType,CstZone,CstState  with ur";
//          报送文件包数据条数
            String tSql1 = "select count(1) from (select CstDate,CompCod,CstNo,CstID,CstName,CstType,CstBirthday,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '0' and cinvalidate >='2014-10-01' group by CstDate,CompCod,CstNo,CstID,CstName,CstType,CstBirthday,RiskType,CstZone,CstState ) as temp ";
            String tNum1 = new ExeSQL().getOneValue(tSql1);
            
            String tSql2 = "select count(1) from (select CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState from lctempcustomerdetail where cstflag = '1' and cinvalidate >='2014-10-01' group by CstDate,CompCod,CstNo,CstID,CstName,CstType,RiskType,CstZone,CstState ) as temp";
            String tNum2 = new ExeSQL().getOneValue(tSql2);
            
//          删选后客户数
//            //个人客户 姓名一样，证件号码为空，算一人
//            String tSql3 = "select count(1) from ( select CstID,CstName from lctempcustomerdetail where cstflag = '0' group by CstID,CstName ) as temp";
//            String tNum3 = new ExeSQL().getOneValue(tSql3);
            
//          个人客户 姓名一样，证件号码为空，根据保单号分组姓名
            String tSql31 = "select count(1) from ( select CstNo from lctempcustomerdetail where cstflag = '0' and (CstID = '' or CstID is null) and cinvalidate >='2014-10-01'  group by CstNo) as temp";
            String tNum31 = new ExeSQL().getOneValue(tSql31);
//            String tSql32 = "SELECT COUNT(DISTINCT CstID) + COUNT(DISTINCT CstName) - COUNT(DISTINCT CstID || CstName) FROM lctempcustomerdetail WHERE (CstID IS NOT NULL OR CstID <> '') and cstflag = '0' ";
            String tSql32 = "select count(1) from ( select CstID,CstName from lctempcustomerdetail where cstflag = '0' and CstID != '' and CstID is not null and cinvalidate >='2014-10-01'  group by CstID,CstName ) as temp";
            String tNum32 = new ExeSQL().getOneValue(tSql32);
            String tNum3 = String.valueOf(Integer.parseInt(tNum31)+Integer.parseInt(tNum32));
            
//            String tSql4 = "select count(1) from ( select CstID,CstName from lctempcustomerdetail where cstflag = '1' group by CstID,CstName ) as temp";
//            String tNum4 = new ExeSQL().getOneValue(tSql4);
            
//            机构客户 姓名一样，证件号码为空，根据保单号分组姓名
            String tSql41 = "select count(1) from ( select CstNo from lctempcustomerdetail where cstflag = '1' and (CstID = '' or CstID is null) and cinvalidate >='2014-10-01'  group by CstNo) as temp";
            String tNum41 = new ExeSQL().getOneValue(tSql41);
            String tSql42 = "SELECT COUNT(DISTINCT CstID) + COUNT(DISTINCT CstName) - COUNT(DISTINCT CstID || CstName) FROM lctempcustomerdetail WHERE (CstID IS NOT NULL OR CstID <> '') and cstflag = '1' and cinvalidate >='2014-10-01' ";
            String tNum42 = new ExeSQL().getOneValue(tSql42);
            String tNum4 = String.valueOf(Integer.parseInt(tNum41)+Integer.parseInt(tNum42));
            
//            期末有效投保人数量
            String tSql51 = "select count(1) from ( select CstNo from lctempcustomerdetail where cstflag = '0' and CstType = 'T01' and cststate = 'S01' and (CstID = '' or CstID is null) and cinvalidate >='2014-10-01' group by CstNo ) as temp";
            String tNum51 = new ExeSQL().getOneValue(tSql51);
//            String tSql52 = "SELECT COUNT(DISTINCT CstID) + COUNT(DISTINCT CstName) - COUNT(DISTINCT CstID || CstName) FROM lctempcustomerdetail WHERE (CstID IS NOT NULL OR CstID <> '') and cstflag = '0' and CstType = 'T01' and cststate = 'S01'";
            String tSql52 = "select count(1) from ( select CstID,CstName from lctempcustomerdetail where cstflag = '0' and CstType = 'T01' and cststate = 'S01' and CstID != '' and CstID is not null and cinvalidate >='2014-10-01' group by CstID,CstName ) as temp";
            String tNum52 = new ExeSQL().getOneValue(tSql52);
            String tNum5 = String.valueOf(Integer.parseInt(tNum51)+Integer.parseInt(tNum52));
            
            String tSql61 = "select count(1) from ( select CstNo from lctempcustomerdetail where cstflag = '1' and CstType = 'T01' and cststate = 'S01' and (CstID = '' or CstID is null) and cinvalidate >='2014-10-01' group by CstNo ) as temp";
            String tNum61 = new ExeSQL().getOneValue(tSql61);
            String tSql62 = "SELECT COUNT(DISTINCT CstID) + COUNT(DISTINCT CstName) - COUNT(DISTINCT CstID || CstName) FROM lctempcustomerdetail WHERE (CstID IS NOT NULL OR CstID <> '') and cstflag = '1' and CstType = 'T01' and cststate = 'S01' and cinvalidate >='2014-10-01' ";
            String tNum62 = new ExeSQL().getOneValue(tSql62);
            String tNum6 = String.valueOf(Integer.parseInt(tNum61)+Integer.parseInt(tNum62));
            
//            期末有效保单数量
            String tSql7 = "select count(1)  from ( select contno from lctempcustomerdetail where cstflag = '0' and cststate = 'S01' and cinvalidate >='2014-10-01' group by contno ) as temp ";
            String tNum7 = new ExeSQL().getOneValue(tSql7);
            
            String tSql8 = "select count(1)  from ( select contno from lctempcustomerdetail where cstflag = '1' and cststate = 'S01' and cinvalidate >='2014-10-01' group by contno ) as temp ";
            String tNum8 = new ExeSQL().getOneValue(tSql8);
            
//          期间满期保单数量
            String tSql9 = "select count(1) from ( select contno from lctempcustomerdetail where cstflag = '0' and cststate = 'S02' and cinvalidate >='2014-10-01' group by contno ) as temp";
            String tNum9 = new ExeSQL().getOneValue(tSql9);
            
            String tSql10 = "select count(1) from ( select contno from lctempcustomerdetail where cstflag = '1' and cststate = 'S02' and cinvalidate >='2014-10-01' group by contno ) as temp";
            String tNum10 = new ExeSQL().getOneValue(tSql10);
            
            tFileOutputStream = new FileOutputStream(mURL + tDate+"000085Summary.txt", true);
            OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(tFileOutputStream, "GBK");
	        mBufferedWriter = new BufferedWriter(tOutputStreamWriter);
            mBufferedWriter.write("报送文件包数据条数: 个人客户："+tNum1 +" 机构客户："+tNum2);
            mBufferedWriter.newLine();
            mBufferedWriter.flush();
            mBufferedWriter.write("删选后客户数: 个人客户："+tNum3 +" 机构客户："+tNum4);
            mBufferedWriter.newLine();
            mBufferedWriter.flush();
            mBufferedWriter.write("期末有效投保人数量: 个人客户："+tNum5 +" 机构客户："+tNum6);
            mBufferedWriter.newLine();
            mBufferedWriter.flush();
            mBufferedWriter.write("期末有效保单数量: 个人客户："+tNum7 +" 机构客户："+tNum8);
            mBufferedWriter.newLine();
            mBufferedWriter.flush();
            mBufferedWriter.write("期间满期保单数量: 个人客户："+tNum9 +" 机构客户："+tNum10);
            mBufferedWriter.flush();
            
            mBufferedWriter.close();
            System.out.println("提取" + mCstDate + "汇总数据完成,完成时间是" +PubFun.getCurrentTime());
        } catch (IOException ex2)
        {
            System.out.println(ex2.getStackTrace());
        }
    }
    
    public static void main(String[] args){
    	CreatCustomerTxtBL tCreatCustomerTxtBL = new CreatCustomerTxtBL();
    	VData tVData = new VData();
    	
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "001";
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("CstDate", "2015-09-30");
    	
    	tVData.add(tGlobalInput);
    	tVData.add(tTransferData);
    	tCreatCustomerTxtBL.submitData(tVData, "psn");
    } 
}
