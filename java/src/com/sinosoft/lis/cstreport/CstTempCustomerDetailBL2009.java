package com.sinosoft.lis.cstreport;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LCTempCustomerDetail2009Set;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: 集团客户上报系统</p>
 *
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author guozhonghua
 * @version 1.0
 */
public class CstTempCustomerDetailBL2009
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private MMap map = new MMap();
    
    private String mCstDate = "";
    
    private TransferData mTransferData = null;

    public CstTempCustomerDetailBL2009()
    {
    }

    /**
     * 外部提交的方法，本方法不直接处理业务
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();
        if (getSubmitMap(cInputData) == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 外部提交的方法，返回处理后的MMap集合
     * @param cInputData VData
     * @return boolean
     */
    public MMap getSubmitMap(VData cInputData)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 将传入的外包数据分解到本类的全局变量
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        if(mGI == null)
        {
            mErrors.addOneError("页面超时，请重登录");
            return false;
        }
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        mCstDate = (String)mTransferData.getValueByName("CstDate"); 
        
        if(mCstDate == null || "".equals(mCstDate)){
        	mErrors.addOneError("获取提数时间失败！");
            return false;
        }

        return true;
    }

    /**
     * 进行数据合法性的校验
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
//    	if(!deleteLCTempCustomerDetail2009()){
//    		return false;
//    	}
    	
    	System.out.println("开始生成" + mCstDate + "临时表数据,开始时间是" +PubFun.getCurrentTime());
    	String tCstDate = mCstDate.replaceAll("-", ""); 
    	String tCurrentDate = PubFun.getCurrentDate();
    	String tCurrentTime = PubFun.getCurrentTime();
    	String tYear = mCstDate.substring(0, 4);
    	String tYear1 = String.valueOf(Integer.parseInt(tYear)+1);
    	String tCinvalidate = mCstDate.substring(0, 4)+"-01-01";//提数年期第一天
    	
    	String mCstDate1 = "";
    	String tCvalidate1 = "";
    	for(int i = 0;i<5;i++){
    		if(i==0){
    			mCstDate1 = tYear+"-01-01";
    			tCvalidate1 = "2005-01-01";
    		}else if(i==1){
    			mCstDate1 = tYear+"-04-01";
    			tCvalidate1 = tYear+"-01-01";
    		}else if(i==2){
    			mCstDate1 = tYear+"-07-01";
    			tCvalidate1 = tYear+"-04-01";
    		}else if(i==3){
    			mCstDate1 = tYear+"-10-01";
    			tCvalidate1 = tYear+"-07-01";
    		}else if(i==4){
    			mCstDate1 = tYear1+"-01-01";//若为全年，则该时间为需提数年费的次年
    			tCvalidate1 = tYear+"-10-01";
    		}
    		StringBuffer strSqlBuff = new StringBuffer();
        	strSqlBuff.append(" select '"+i+"' || '_' || db2inst1.varchar(row_number() over()) as Serno, ");
        	strSqlBuff.append(" '"+tCstDate+"' CstDate,'000085' CompCod," );
        	strSqlBuff.append(" temp.CST_NO CstNo,temp.CST_ID CstID,temp.CST_NAME CstName,temp.CST_TYPE CstType,temp.CST_BITHDAY CstBirthday,temp.RISK_TYPE RiskType,temp.CST_ZONE CstZone,temp.CST_STATE CstState,temp.CSTFlag CstFlag,temp.CST_Contno ContNo, temp.CST_CValiDate CValiDate,temp.CST_CInValiDate CInValiDate,'"+tCurrentDate+"' MakeDate,'"+tCurrentTime+"' MakeTime,'"+tCurrentDate+"' ModifyDate,'"+tCurrentTime+"' ModifyTime");
        	strSqlBuff.append(" from ");
        	strSqlBuff.append(" (");
        	strSqlBuff.append(" select ");
        	strSqlBuff.append(" lcc.appntno CST_NO,");
        	strSqlBuff.append(" lcc.appntidno CST_ID,");
        	strSqlBuff.append(" lcc.appntname CST_NAME,");
        	strSqlBuff.append(" 'T01' CST_TYPE, ");
        	strSqlBuff.append(" DECIMAL(lcc.Appntbirthday) CST_BITHDAY,");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15' ");
        	strSqlBuff.append(" when lfr.risktype = 'A'  then 'R16'");
        	strSqlBuff.append(" when lfr.risktype = 'L' then 'R14' end RISK_TYPE,");
        	strSqlBuff.append(" lcc.managecom CST_ZONE,");
        	strSqlBuff.append(" case when lcc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '0' CSTFlag, ");
        	strSqlBuff.append(" lcc.contno CST_Contno,lcc.Cvalidate CST_CValiDate,lcc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lccont lcc ");
        	strSqlBuff.append(" inner join lcpol lcp on lcc.contno = lcp.contno ");
        	strSqlBuff.append(" inner join lfrisk lfr on lfr.riskcode = lcp.riskcode");
        	strSqlBuff.append(" where 1 = 1 ");
        	strSqlBuff.append(" and lcc.ContType = '1'  ");
        	strSqlBuff.append(" and lcc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lcc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lcc.CInValiDate >= '"+tCinvalidate+"' ");
        	strSqlBuff.append(" and lcc.AppFlag = '1'");
        	strSqlBuff.append(" union ");
        	strSqlBuff.append(" select ");
        	strSqlBuff.append(" lcc.appntno CST_NO,");
        	strSqlBuff.append(" lcc.appntidno CST_ID,");
        	strSqlBuff.append(" lcc.appntname CST_NAME,");
        	strSqlBuff.append(" 'T01' CST_TYPE, ");
        	strSqlBuff.append(" DECIMAL(lcc.Appntbirthday) CST_BITHDAY,");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15' ");
        	strSqlBuff.append(" when lfr.risktype = 'A'  then 'R16'");
        	strSqlBuff.append(" when lfr.risktype = 'L' then 'R14' end RISK_TYPE,");
        	strSqlBuff.append(" lcc.managecom CST_ZONE,");
        	strSqlBuff.append(" case when lcc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '0' CSTFlag, ");
        	strSqlBuff.append(" lcc.contno CST_Contno,lcc.Cvalidate CST_CValiDate,lcc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lbcont lcc ");
        	strSqlBuff.append(" inner join lbpol lcp on lcc.contno = lcp.contno ");
        	strSqlBuff.append(" inner join lfrisk lfr on lfr.riskcode = lcp.riskcode");
        	strSqlBuff.append(" where 1 = 1 ");
        	strSqlBuff.append(" and lcc.ContType = '1'  ");
        	strSqlBuff.append(" and lcc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lcc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lcc.CInValiDate >= '"+tCinvalidate+"' ");
        	strSqlBuff.append(" and lcc.AppFlag = '1'");
//        	被保人
        	strSqlBuff.append(" union");
        	strSqlBuff.append(" select ");
        	strSqlBuff.append(" lci.insuredno CST_NO,");
        	strSqlBuff.append(" lci.idno CST_ID,");
        	strSqlBuff.append(" lci.name CST_NAME,");
        	strSqlBuff.append(" 'T02' CST_TYPE, ");
        	strSqlBuff.append(" DECIMAL(lci.birthday) CST_BITHDAY,");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15' ");
        	strSqlBuff.append(" when lfr.risktype  = 'A'  then 'R16'");
        	strSqlBuff.append(" when lfr.risktype  = 'L' then 'R14' end RISK_TYPE,");
        	strSqlBuff.append(" lcc.managecom CST_ZONE,");
        	strSqlBuff.append(" case when lcc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '0' CSTFlag, ");
        	strSqlBuff.append(" lcc.contno CST_Contno,lcc.Cvalidate CST_CValiDate,lcc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lccont lcc ");
        	strSqlBuff.append(" inner join lcinsured lci on lcc.contno = lci.contno");
        	strSqlBuff.append(" inner join lcpol lcp on lcp.insuredno = lci.insuredno and lcp.contno = lci.contno  ");
        	strSqlBuff.append(" inner join lfrisk lfr on lfr.riskcode = lcp.riskcode");
        	strSqlBuff.append(" where 1 = 1  ");
        	strSqlBuff.append(" and lcc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lcc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lcc.CInValiDate >= '"+tCinvalidate+"' ");
        	strSqlBuff.append(" and lcc.AppFlag = '1'");
        	strSqlBuff.append(" and lcc.ContType = '1'");
        	strSqlBuff.append(" union"); 
        	strSqlBuff.append(" select "); //团
        	strSqlBuff.append(" lci.insuredno CST_NO,");
        	strSqlBuff.append(" lci.idno CST_ID,");
        	strSqlBuff.append(" lci.name CST_NAME,");
        	strSqlBuff.append(" 'T02' CST_TYPE, ");
        	strSqlBuff.append(" DECIMAL(lci.birthday) CST_BITHDAY,");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15' ");
        	strSqlBuff.append(" when lfr.risktype  = 'A'  then 'R16'");
        	strSqlBuff.append(" when lfr.risktype  = 'L' then 'R14' end RISK_TYPE,");
        	strSqlBuff.append(" lgc.managecom CST_ZONE,");
        	strSqlBuff.append(" case when lgc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '0' CSTFlag, ");
        	strSqlBuff.append(" lgc.GrpContNo CST_Contno,lgc.Cvalidate CST_CValiDate,lgc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lcgrpcont lgc ");
        	strSqlBuff.append(" inner join lcinsured lci on lgc.GrpContNo = lci.GrpContNo");
        	strSqlBuff.append(" inner join lcpol lcp on lcp.insuredno = lci.insuredno and lcp.contno = lci.contno  ");
        	strSqlBuff.append(" inner join lfrisk lfr on lfr.riskcode = lcp.riskcode");
        	strSqlBuff.append(" where 1 = 1  ");
        	strSqlBuff.append(" and lgc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lgc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lgc.CInValiDate >= '"+tCinvalidate+"' ");
        	strSqlBuff.append(" and lgc.AppFlag = '1'");
        	strSqlBuff.append(" and lgc.SaleChnl in ('02','03')");
        	strSqlBuff.append(" and lci.Name not in ('无名单','公共账户')");
        	strSqlBuff.append(" union ");
        	strSqlBuff.append(" select ");
        	strSqlBuff.append(" lci.insuredno CST_NO,");
        	strSqlBuff.append(" lci.idno CST_ID,");
        	strSqlBuff.append(" lci.name CST_NAME,");
        	strSqlBuff.append(" 'T02' CST_TYPE, ");
        	strSqlBuff.append(" DECIMAL(lci.birthday) CST_BITHDAY,");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15' ");
        	strSqlBuff.append(" when lfr.risktype  = 'A'  then 'R16'");
        	strSqlBuff.append(" when lfr.risktype  = 'L' then 'R14' end RISK_TYPE,");
        	strSqlBuff.append(" lcc.managecom CST_ZONE,");
        	strSqlBuff.append(" case when lcc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '0' CSTFlag, ");
        	strSqlBuff.append(" lcc.contno CST_Contno,lcc.Cvalidate CST_CValiDate,lcc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lbcont lcc ");
        	strSqlBuff.append(" inner join lbinsured lci on lcc.contno = lci.contno");
        	strSqlBuff.append(" inner join lbpol lcp on lcp.insuredno = lci.insuredno and lcp.contno = lci.contno ");
        	strSqlBuff.append(" inner join lfrisk lfr on lfr.riskcode = lcp.riskcode");
        	strSqlBuff.append(" where 1 = 1  ");
        	strSqlBuff.append(" and lcc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lcc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lcc.CInValiDate >= '"+tCinvalidate+"' ");
        	strSqlBuff.append(" and lcc.AppFlag = '1'");
        	strSqlBuff.append(" and lcc.ContType = '1'");
        	strSqlBuff.append(" union ");
        	strSqlBuff.append(" select "); //团
        	strSqlBuff.append(" lci.insuredno CST_NO,");
        	strSqlBuff.append(" lci.idno CST_ID,");
        	strSqlBuff.append(" lci.name CST_NAME,");
        	strSqlBuff.append(" 'T02' CST_TYPE, ");
        	strSqlBuff.append(" DECIMAL(lci.birthday) CST_BITHDAY,");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15' ");
        	strSqlBuff.append(" when lfr.risktype  = 'A'  then 'R16'");
        	strSqlBuff.append(" when lfr.risktype  = 'L' then 'R14' end RISK_TYPE,");
        	strSqlBuff.append(" lgc.managecom CST_ZONE,");
        	strSqlBuff.append(" case when lgc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '0' CSTFlag, ");
        	strSqlBuff.append(" lgc.GrpContNo CST_Contno,lgc.Cvalidate CST_CValiDate,lgc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lbgrpcont lgc ");
        	strSqlBuff.append(" inner join lbinsured lci on lgc.GrpContNo = lci.GrpContNo");
        	strSqlBuff.append(" inner join lbpol lcp on lcp.insuredno = lci.insuredno and lcp.contno = lci.contno ");
        	strSqlBuff.append(" inner join lfrisk lfr on lfr.riskcode = lcp.riskcode");
        	strSqlBuff.append(" where 1 = 1  ");
        	strSqlBuff.append(" and lgc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lgc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lgc.CInValiDate >= '"+tCinvalidate+"' ");
        	strSqlBuff.append(" and lgc.AppFlag = '1'");
        	strSqlBuff.append(" and lgc.SaleChnl in ('02','03')");
        	strSqlBuff.append(" and lci.Name not in ('无名单','公共账户')");
        	//以下为机构客户
        	strSqlBuff.append(" union ");
        	strSqlBuff.append(" select ");
        	strSqlBuff.append(" lgc.appntno CST_NO,");
        	strSqlBuff.append(" lcga.OrganComCode CST_ID, ");                                                                                                                              
        	strSqlBuff.append(" lcga.name CST_NAME, ");                                                                                                                                         
        	strSqlBuff.append(" 'T01' CST_TYPE,  "); 
        	strSqlBuff.append(" 0 CST_BITHDAY, ");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15'  ");                                                                                                                    
        	strSqlBuff.append(" when lfr.risktype = 'A'  then 'R16' ");                                                                                                                         
        	strSqlBuff.append(" when lfr.risktype = 'L' then 'R14' end RISK_TYPE, ");                                                                                                           
        	strSqlBuff.append(" lgc.managecom CST_ZONE, ");                                                                                                                                     
        	strSqlBuff.append(" case when lgc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '1' CSTFlag, ");
        	strSqlBuff.append(" lgc.GrpContNo CST_Contno,lgc.Cvalidate CST_CValiDate,lgc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lcgrpcont lgc  ");                                                                                                                                         
        	strSqlBuff.append(" inner join lcgrpappnt lcga  on lgc.grpcontno = lcga.grpcontno ");                                                                                               
        	strSqlBuff.append(" inner join lcgrppol lcgp on lcga.GrpContNo = lcgp.GrpContNo  ");                                                                                                
        	strSqlBuff.append(" inner join LFRisk lfr on lfr.riskcode = lcgp.riskcode ");                                                                                                       
        	strSqlBuff.append(" where 1 = 1  ");                                                                                                                                                
        	strSqlBuff.append(" and lgc.AppFlag = '1' ");                                                                                                                                       
        	strSqlBuff.append(" and lgc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lgc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lgc.CInValiDate >= '"+tCinvalidate+"' ");                                                                                                                       
        	strSqlBuff.append(" and (lgc.CardFlag in ('0','4') or lgc.CardFlag is null)  ");                                                                                                    
        	strSqlBuff.append(" and not exists (select 1 from lcgrppol where grpcontno = lgc.GrpContno and RiskCode in ('170301','170106','170401','1605')) ");                                 
        	strSqlBuff.append(" union   ");                                                                                                                                                     
        	strSqlBuff.append(" select ");
        	strSqlBuff.append(" lgc.appntno CST_NO,");
        	strSqlBuff.append(" lcga.OrganComCode CST_ID, ");                                                                                                                              
        	strSqlBuff.append(" lcga.name CST_NAME, ");                                                                                                                                         
        	strSqlBuff.append(" 'T01' CST_TYPE,  ");
        	strSqlBuff.append(" 0 CST_BITHDAY, ");
        	strSqlBuff.append(" case when lfr.risktype = 'H' then 'R15'  ");                                                                                                                    
        	strSqlBuff.append(" when lfr.risktype = 'A'  then 'R16' ");                                                                                                                         
        	strSqlBuff.append(" when lfr.risktype = 'L' then 'R14' end RISK_TYPE, ");                                                                                                           
        	strSqlBuff.append(" lgc.managecom CST_ZONE, ");                                                                                                                                     
        	strSqlBuff.append(" case when lgc.cinvalidate >= '"+mCstDate+"' then 'S01' else 'S02' end CST_STATE, ");
        	strSqlBuff.append(" '1' CSTFlag, ");
        	strSqlBuff.append(" lgc.GrpContNo CST_Contno,lgc.Cvalidate CST_CValiDate,lgc.Cinvalidate CST_CInValiDate ");
        	strSqlBuff.append(" from lbgrpcont lgc  ");                                                                                                                                         
        	strSqlBuff.append(" inner join lbgrpappnt lcga  on lgc.grpcontno = lcga.grpcontno ");                                                                                               
        	strSqlBuff.append(" inner join lbgrppol lcgp on lcga.GrpContNo = lcgp.GrpContNo  ");                                                                                                
        	strSqlBuff.append(" inner join LFRisk lfr on lfr.riskcode = lcgp.riskcode ");                                                                                                       
        	strSqlBuff.append(" where 1 = 1  ");                                                                                                                                                
        	strSqlBuff.append(" and lgc.AppFlag = '1'  ");                                                                                                                                      
        	strSqlBuff.append(" and lgc.CValiDate < '"+mCstDate1+"' ");
        	strSqlBuff.append(" and lgc.CValiDate >= '"+tCvalidate1+"' ");
        	strSqlBuff.append(" and lgc.CInValiDate >= '"+tCinvalidate+"' ");                                                                                                                        
        	strSqlBuff.append(" and (lgc.CardFlag in ('0','4') or lgc.CardFlag is null)  ");                                                                                                    
        	strSqlBuff.append(" and not exists (select 1 from lcgrppol where grpcontno = lgc.GrpContno and RiskCode in ('170301','170106','170401','1605')) ");                                 
        	strSqlBuff.append(" ) as temp");
        	strSqlBuff.append(" group by temp.CST_NO,temp.CST_Contno, temp.CST_ID,temp.CST_NAME,temp.CST_TYPE,temp.CST_BITHDAY,temp.RISK_TYPE,temp.CST_ZONE,temp.CST_STATE,temp.CSTFlag, temp.CST_CValiDate,temp.CST_CInValiDate");
        	strSqlBuff.append(" with ur ");
        	/* 游标查询方法 */
             RSWrapper rswrapper = new RSWrapper();
             LCTempCustomerDetail2009Set tLCTempCustomerDetail2009Set = new LCTempCustomerDetail2009Set();
             rswrapper.prepareData(tLCTempCustomerDetail2009Set, strSqlBuff.toString());
         
             try
             {
                 do
                 {
                     rswrapper.getData();
                     if(tLCTempCustomerDetail2009Set !=null && tLCTempCustomerDetail2009Set.size()>0){
                    	 MMap tempMMap = new MMap();
                    	 tempMMap.put(tLCTempCustomerDetail2009Set, SysConst.DELETE_AND_INSERT);
                    	 submit(tempMMap);
                     }
                 }
                 while (tLCTempCustomerDetail2009Set.size() > 0);
                 rswrapper.close();
             }
             
             catch (Exception ex)
             {
                 ex.printStackTrace();
                 rswrapper.close();
             }
    	}
    	
    	
        System.out.println("生成" + mCstDate + "临时表数据,结束时间是" +PubFun.getCurrentTime());
    	return true;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean deleteLCTempCustomerDetail2009()
    {
        String delSql = "delete from LCTempCustomerDetail2009 ";
        MMap delMMap = new MMap();
        
        delMMap.put(delSql, SysConst.DELETE);
        
        VData data = new VData();
        data.add(delMMap);
        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("生成客户提数临时表数据前，清空临时表数据失败");
            buildError("submitData", "生成客户提数临时表数据前，清空临时表数据失败");
            return false;
        }

        return true;
    } 
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    } 
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriGrpTempFeeFromBank";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public static void main(String[] args){
    	CstTempCustomerDetailBL2009 tCstTempCustomerDetailBL = new CstTempCustomerDetailBL2009();
    	VData tVData = new VData();
    	
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "001";
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("CstDate", "2009-12-31");
    	
    	tVData.add(tGlobalInput);
    	tVData.add(tTransferData);
    	
    	tCstTempCustomerDetailBL.submitData(tVData, "");
    }

}
