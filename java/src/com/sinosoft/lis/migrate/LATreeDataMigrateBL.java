package com.sinosoft.lis.migrate;

import java.util.HashMap;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.StrTool;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LATreeDataMigrateBL {
    /** 传入参数 */
    private VData mInputData;
    /** 传入操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();
    /** 最后保存结果 */
    private VData mResult = new VData();
    /** 最后递交Map */
    private MMap map = new MMap();
    /** 传入的LATree */
    private LATreeSchema mLATreeSchema;
    /** 最终生成的LARecomeRelation */
    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    /** 缓存上级key为下级 */
    private HashMap mUpAgent = new HashMap();
    /**
     * 传入的仅为LATree中的链表起点位置
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into BriefCardSignBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("BriefCardSignBL finished...");
        mLARecomRelationSet.clear();
        map = new MMap();
        mResult.clear();
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into BriefCardSignBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLATreeSchema = (LATreeSchema) mInputData.getObjectByObjectName(
                "LATreeSchema",
                0);
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into BriefCardSignBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        if (this.mLATreeSchema == null || mLATreeSchema.getAgentCode() == null) {
            String str = "传入的LATree是空!";
            buildError("checkData", str);
            System.out.println("在程序LATreeDataMigrateBL.checkData() - 111 : " +
                               str);
            return false;
        }
        LATreeDB tLATreeDB = mLATreeSchema.getDB();
        if (!tLATreeDB.getInfo()) {
            String str = "传入的LATree查询失败，请检查传入代理人编码是否正确：" +
                         this.mLATreeSchema.getAgentCode() + "!";
            buildError("checkData", str);
            System.out.println("在程序LATreeDataMigrateBL.checkData() - 119 : " +
                               str);
            return false;
        }
        this.mLATreeSchema.setSchema(tLATreeDB);
        /** 判断代理人是否存在 */
        String tAgentCode = this.mLATreeSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (tLAAgentDB.getCount() <= 0) {
            String str = "查询代理人失败，请确认代理人：" + tAgentCode + "是否存在!";
            buildError("checkData", str);
            System.out.println(
                    "在程序LATreeDataMigrateBL.checkData() - 109 : " + str);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into BriefCardSignBL.dealData()...");
        /** 查找全部的下级，下下级 */
        if (!queryDownAgent(this.mLATreeSchema)) {
            return false;
        }
        if (this.mLARecomRelationSet != null &&
            this.mLARecomRelationSet.size() > 0) {
            map.put(mLARecomRelationSet, "INSERT");
        }
        return true;
    }


    /**
     * 传入的AgentCode,最为推荐人，HashMap作为全部的被推荐人
     *
     * @param tAgentCode String
     * @param tRecomGensMap HashMap
     * @param grade int
     * @param size int
     * @return boolean
     */
    private boolean creatRelation(String tAgentCode, HashMap tRecomGensMap,
                                  int grade, int size) {
//        if (mSize < 0) {
//            mSize = tRecomGensMap.size();
//        }
        LATreeSet tLATreeSet =
                (LATreeSet) tRecomGensMap.get(String.valueOf(size));
        if (tLATreeSet != null && tLATreeSet.size() > 0) {
            fillRecomRelation(tAgentCode, tLATreeSet, grade);
        } else {
            return true;
        }
        size--;
        grade--;
        creatRelation(tAgentCode, tRecomGensMap, grade, size);
        return true;
    }

    /**
     * fillRecomRelation
     *
     * @param tAgentCode String
     * @param tLATreeSet LATreeSet
     * @param grade int
     * @return boolean
     */
    private boolean fillRecomRelation(String tAgentCode, LATreeSet tLATreeSet,
                                      int grade) {
        String tUpAgent = (String) mUpAgent.get(tAgentCode);
        if (tUpAgent != null) {
            int grade2 = grade + 1;
            fillRecomRelation(tUpAgent, tLATreeSet, grade2);
        }
        LATreeDB mLATreeDB = new LATreeDB();
        mLATreeDB.setAgentCode(tAgentCode);
        if (!mLATreeDB.getInfo()) {
            String str = "查询代理人失败，代理费编码：" + tAgentCode + "!";
            buildError("fillRecomRelation", str);
            System.out.println(
                    "在程序LATreeDataMigrateBL.fillRecomRelation() - 214 : " +
                    str);
            return false;
        }

        for (int i = 1; i <= tLATreeSet.size(); i++) {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tLATreeSet.get(i).getAgentCode());
            if (!tLAAgentDB.getInfo()) {
                String str = "查询代理人失败，代理费编码：" + tAgentCode + "!";
                buildError("fillRecomRelation", str);
                System.out.println(
                        "在程序LATreeDataMigrateBL.fillRecomRelation() - 214 : " +
                        str);
                return false;
            }

            LARecomRelationSchema tLARecomRelationSchema = new
                    LARecomRelationSchema();
            tLARecomRelationSchema.setRecomLevel("01"); //推荐级别
            tLARecomRelationSchema.setRecomGens(grade); //推荐代数目
            tLARecomRelationSchema.setAgentCode(tLATreeSet.get(i).getAgentCode()); //被推荐人
            tLARecomRelationSchema.setRecomAgentCode(tAgentCode); //推荐人
            tLARecomRelationSchema.setAgentGroup(tLATreeSet.get(i).
                                                 getAgentGroup()); //被推荐机构
            tLARecomRelationSchema.setStartDate(tLATreeSet.get(1).
                                                getIntroCommStart()); //推荐起期
            tLARecomRelationSchema.setEndDate(tLATreeSet.get(1).
                                              getIntroCommStart()); //推荐止期
            if (!StrTool.cTrim(tLAAgentDB.getAgentState()).equals("") &&
                Integer.parseInt(tLAAgentDB.getAgentState()) > 2) {
                tLARecomRelationSchema.setRecomFlag("0");
            } else {
                tLARecomRelationSchema.setRecomFlag("1");
            }
//            if (!tLAAgentDB.getAgentGroup().equals(mLATreeDB.getAgentGroup()) &&
//                !tLAAgentDB.getAgentGroup().equals(mLATreeDB.getAgentGroup())) {
//
//            }

//        tLARecomRelationSchema.setRecomComFlag(); //推荐津贴抽取标记
//        tLARecomRelationSchema.setRecomStartYear(); //推荐津贴抽取起始年度
//        tLARecomRelationSchema.setRate(); //推荐津贴抽取比例
            tLARecomRelationSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            tLARecomRelationSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            tLARecomRelationSchema.setModifyDate(PubFun.getCurrentDate()); //最近修改日期
            tLARecomRelationSchema.setModifyTime(PubFun.getCurrentTime()); //最近修改时间
            tLARecomRelationSchema.setOperator(mGlobalInput.Operator); //操作员代码
//        tLARecomRelationSchema.setDeductFlag(); //行销比例扣除标志
//        tLARecomRelationSchema.setInheritCalFlag(); //继承推荐津贴计算标志
//        tLARecomRelationSchema.setRecomBonusFlag(); //推荐奖金有效标志
//        tLARecomRelationSchema.setDeductRate(); //行销扣除比例
            tLARecomRelationSchema.setAgentGrade(tLATreeSet.get(i).
                                                 getAgentGrade()); //被推荐人职级
            tLARecomRelationSchema.setRecomAgentGrade(mLATreeDB.getAgentGrade()); //推荐人职级
            this.mLARecomRelationSet.add(tLARecomRelationSchema);
        }
        return true;
    }

    /**
     * queryDownAgent
     *
     * @return boolean
     * @param tLATreeSchema String
     */
    private boolean queryDownAgent(LATreeSchema tLATreeSchema) {
        /** 使用递归查询全部下级 */
        String tAgentCode = tLATreeSchema.getAgentCode();
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setUpAgent(tAgentCode);
        LATreeSet tLATreeSet = tLATreeDB.query();
        if (tLATreeSet.size() > 0) {
            fillRecomRelation(tAgentCode, tLATreeSet, 1);
            for (int i = 1; i <= tLATreeSet.size(); i++) {
                mUpAgent.put(tLATreeSet.get(i).getAgentCode(), tAgentCode);
                queryDownAgent(tLATreeSet.get(i));
            }
        }
        return true;
    }

    /**
     * dealUpAgent
     *
     * @param tAgentCode String
     * @return HashMap
     */
    private HashMap dealUpAgent(String tAgentCode) {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tAgentCode);
        if (!tLATreeDB.getInfo()) {
            String str = "查找代理人失败!";
            buildError("dealUpAgent", str);
            System.out.println("在程序LATreeDataMigrateBL.dealUpAgent() - 328 : " +
                               str);
            return null;
        }
        return null;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into BriefCardSignBL.prepareOutputData()...");
        this.mResult.add(map);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {

        GlobalInput g = new GlobalInput();
        g.ManageCom = "86";
        g.Operator = "ym";
        g.ComCode = "86";

        /** 测试ｑｕｅｒｙＡｇｅｎｔ方法 */
//        LATreeDB tLATreeDB = new LATreeDB();
//        LATreeSet tLATreeSet = tLATreeDB.executeQuery(
//                "select * from latree where agentcode='1101000016' order by agentcode");
//        for (int i = 1; i <= tLATreeSet.size(); i++) {
//            System.out.println("开始检测代理人编码：" + tLATreeSet.get(i).
//                               getAgentCode());
//            tLATreeDataMigrateBL.testQueryDownAgent(tLATreeSet.get(i));
//        }
        /** 测试 */
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = tLATreeDB.executeQuery(
                "select * from LATree where UpAgent is null and int(branchtype)=1 order by agentcode");
        for (int i = 1; i <= tLATreeSet.size(); i++) {
            VData v = new VData();
            v.add(tLATreeSet.get(i));
            v.add(g);
            LATreeDataMigrateBL tLATreeDataMigrateBL = new LATreeDataMigrateBL();
            tLATreeDataMigrateBL.submitData(v, null);
            if (tLATreeDataMigrateBL.mErrors.needDealError()) {
                System.err.print("ERROR:" +
                                 tLATreeDataMigrateBL.mErrors.getContent());
            }
        }
    }

    /**
     * 于测试递归算法是否正确
     *
     * @param tLATreeSchema String
     * @return boolean
     */
    public boolean testQueryDownAgent(LATreeSchema tLATreeSchema) {
        queryDownAgent(tLATreeSchema);
        return true;
    }
}
