package com.sinosoft.lis.message;

import java.net.URL;
import javax.xml.namespace.QName;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sinosoft.lis.db.LCCSpecTransLogDB;
import com.sinosoft.lis.message.SmsMessagesNew;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCCSpecTransLogSchema;
import com.sinosoft.utility.ExeSQL;

/**
 * 短信发送客户端
 * 20170508新短信平台接口
 */
public class SMSClient {
//	private static Log log = LogFactory.getLog(SMSClient.class);

	private String serviceType;// 短信类型标识
	private String orgCode;// 机构代码
	private String sms_userName;// 短信平台分配的用户名
	private String sms_password;// 短信平台分配的密码
	private String taskValue;// 该批次短信对应的业务部门
	private String targetEndpointAddress; //接口地址
	private String operationName ; //命名空间
	
	//短信归属部门
	public final static String mes_taskVlaue1 = "总公司办公室/董事会办公室";
	public final static String mes_taskVlaue2 = "总公司人力资源部";
	public final static String mes_taskVlaue3 = "总公司计划财务部";
	public final static String mes_taskVlaue4 = "总公司战略发展部";
	public final static String mes_taskVlaue5 = "总公司产品开发部";
	public final static String mes_taskVlaue6 = "总公司精算部";
	public final static String mes_taskVlaue7 = "总公司投资管理部";
	public final static String mes_taskVlaue8 = "总公司社会保险部";
	public final static String mes_taskVlaue9 = "总公司团体保险部";
	public final static String mes_taskVlaue10 = "总公司个人保险部";
	public final static String mes_taskVlaue11 = "总公司银行保险部";
	public final static String mes_taskVlaue12 = "总公司互动管理部";
	public final static String mes_taskVlaue13 = "总公司电子商务部";
	public final static String mes_taskVlaue14 = "总公司健康管理部";
	public final static String mes_taskVlaue15 = "总公司运营管理部";
	public final static String mes_taskVlaue16 = "总公司客户服务部";
	public final static String mes_taskVlaue17 = "总公司风险管理部  ";
	public final static String mes_taskVlaue18 = "总公司法律合规部";
	public final static String mes_taskVlaue19 = "总公司监察审计部/监事会办公室";
	public final static String mes_taskVlaue20 = "总公司信息技术部";
	public final static String mes_taskVlaue21 = "人保健康管理公司筹备组";
	
	//短信归属模块
	public final static String mes_serviceType1 = "xuqi";
	
	public SMSClient() {
		try {
			//测试接口--无法实际发送短信
			this.targetEndpointAddress="http://10.252.4.127:8000/WSSMSIF/services/SmsService?wsdl";
			//--测试环境需要实际发送短信时使用
			String str="select codename from ldcode where codetype='SmsService'";
			targetEndpointAddress = new ExeSQL().getOneValue(str);
			//正式接口
//			this.targetEndpointAddress="http://10.252.4.123:8000/WSSMSIF/services/SmsService?wsdl";
			this.operationName="http://service.ws.adapter.mgw.mars.umpay.com/";
			this.sms_userName="02hexin";
			this.sms_password="123456";
		} catch (Exception e) {
			System.out.println("初始化短信接口出错");
			e.printStackTrace();
		}
	}

	public boolean sendSMS(SmsMessagesNew messages) {
		String smstXml = "";
		String tTaskID = "";
		try {
			tTaskID = PubFun1.CreateMaxNo("SMSTASKID","")+"0";
			System.out.println("短信任务号："+tTaskID);
			messages.setTaskId(tTaskID);
			if(null == messages.getNeedUseTemplateFlag() || "".equals(messages.getNeedUseTemplateFlag())){
				messages.setNeedUseTemplateFlag("false");
			}
			if(null == messages.getStartDate() || "".equals(messages.getStartDate())){
				messages.setStartDate(PubFun.getCurrentDate());
			}
			if(null == messages.getStartTime() || "".equals(messages.getStartTime())){
				messages.setStartTime("9:00");
			}
			if(null == messages.getEndDate() || "".equals(messages.getEndDate())){
				messages.setEndDate(PubFun.getCurrentDate());
			}
			if(null == messages.getEndTime() || "".equals(messages.getEndTime())){
				messages.setEndTime("20:00");
			}
			if(null == messages.getExtension() || "".equals(messages.getExtension())){
				messages.setExtension("false");
			}
			if(null == messages.getOrganizationId() || "".equals(messages.getOrganizationId())){
				messages.setOrganizationId("86");
			}
		
			smstXml = "<?xml version=\"1.0\" encoding=\"GBK\"?>" + messages.getXml().toString();
			Service service = new Service();
			Call call = (Call) service.createCall();
			URL url = new URL(targetEndpointAddress);
			call.setTargetEndpointAddress( url );
			call.setOperationName(new QName(operationName, "sendSMS") );
			call.addParameter( "username", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN );// 操作的参数
			call.addParameter( "pwd", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN );// 操作的参数
			call.addParameter( "smsXml", org.apache.axis.encoding.XMLType.XSD_STRING, javax.xml.rpc.ParameterMode.IN );// 操作的参数

			call.setReturnType( org.apache.axis.encoding.XMLType.XSD_STRING );// 设置返回类型
			call.setUseSOAPAction( true );
			System.out.println("请求报文："+smstXml);
			String result = (String) call.invoke(new Object[]{ sms_userName, sms_password, smstXml });
			System.out.println("res:"+result);
			saveXMLinfo(tTaskID,smstXml,result,"00");
		} catch (Exception e) {
			saveXMLinfo(tTaskID,smstXml,"发生异常","01");
			System.out.println(messages.getServiceType()+"短信发送出错-_-!!");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void saveXMLinfo(String mTaskID,String reqXML,String resXML,String mState){
		LCCSpecTransLogDB tLCCSpecTransLogDB = new LCCSpecTransLogDB();
		tLCCSpecTransLogDB.setTaskId(mTaskID);
		tLCCSpecTransLogDB.setState(mState);//00-成功 01-失败 
		tLCCSpecTransLogDB.setRequestXml(reqXML);
		tLCCSpecTransLogDB.setResponseXml(resXML);
		tLCCSpecTransLogDB.setMakeDate(PubFun.getCurrentDate());
		tLCCSpecTransLogDB.setMakeTime(PubFun.getCurrentTime());
		tLCCSpecTransLogDB.insert();
	}

	// 获取Message
//	private SmsMessagesNew getMessages(String mobile, String content) {
//		SmsMessagesNew msgs = new SmsMessagesNew();
////		msgs.setTaskId(createSMSTaskID());
//		msgs.setOrganizationId("86"); // 设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
//		msgs.setServiceType(this.serviceType); // 设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
//		msgs.setTaskValue(this.taskValue);
//		msgs.setNeedUseTemplateFlag("false");
//		List<SmsMessageNew> mess = new ArrayList<SmsMessageNew>();
//		SmsMessageNew msg = new SmsMessageNew(); // 创建SmsMessage对象，定义同上文下行短信格式中的Message元素
//		msg.setReceiver(mobile); // 设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//		msg.setContents(content); // 设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
//		msg.setOrgCode(this.orgCode); // 设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
//		mess.add(msg);
//		msgs.setMessages(mess.toArray(new SmsMessageNew[mess.size()])); // 设置该批短信的每一条短信，一批短信可以包含多条短信
//		return msgs;
//	}

	public static void main(String[] args) throws Exception {
		SMSClient s = new SMSClient();
		String mobile="15600685916";
		String contents="尊敬的xx女士，您好！您的客户xx的保单(保单号222，本期保费1504元)将于2017-05-18进入缴费期，请您尽快联系您的客户收取续期保费。";
//		 s.sendViaStub(mobile,contents);
	}
}