package com.sinosoft.lis.message;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.pubfun.*;

public class TestSMS {
    public static void main(String[] args) {
//        String num = args[0];
//        String conent = args[1];
        String num = "15810313249";
        String  conent = "test message";
        String date = PubFun.getCurrentDate();
        com.sinosoft.lis.message.SmsServiceSoapBindingStub binding = null;
        try {
          binding = (com.sinosoft.lis.message.SmsServiceSoapBindingStub)
          new com.sinosoft.lis.message.SmsServiceServiceLocator().getSmsService();//创建binding对象
        }
        catch (javax.xml.rpc.ServiceException jre) {
                jre. printStackTrace();
        }
        binding.setTimeout(60000);
        com.sinosoft.lis.message.Response value = null;//声明Response对象，该对象将在提交短信后包含提交的结果。
        com.sinosoft.lis.message.SmsMessages msgs = new com.sinosoft.lis.message.SmsMessages();//创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0");//设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("false");//设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("lipei");//设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(date);//设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00");//设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(date);//设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("20:00");//设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        java.util.Vector vec = new java.util.Vector();
        for(int i=0; i < 1; i++) {
          com.sinosoft.lis.message.SmsMessage msg = new com.sinosoft.lis.message.
              SmsMessage();//创建SmsMessage对象，定义同上文下行短信格式中的Message元素
          msg.setReceiver(num);//设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
          msg.setContents(conent); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
          msg.setOrgCode("86110000");//设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

          vec.add(msg);
        }
        msgs.setMessages((com.sinosoft.lis.message.SmsMessage[])vec.toArray(new com.sinosoft.lis.message.SmsMessage[vec.size()]));//设置该批短信的每一条短信，一批短信可以包含多条短信

        try {
            value = binding.sendSMS(msgs);//提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
          System.out.println(value.getStatus());
          System.out.println(value.getMessage());
        }
        catch (RemoteException ex) {
          ex.printStackTrace();
        }
      }
}
