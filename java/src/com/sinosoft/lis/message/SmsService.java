/**
 * SmsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.message;

public interface SmsService extends java.rmi.Remote {
    public com.sinosoft.lis.message.Response sendSMS(java.lang.String userName, java.lang.String password, com.sinosoft.lis.message.SmsMessages msgs) throws java.rmi.RemoteException;
}
