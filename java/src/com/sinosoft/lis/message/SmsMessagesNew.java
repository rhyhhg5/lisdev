/**
 * SmsMessages.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.message;

import java.util.HashMap;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tools.ant.types.CommandlineJava.SysProperties;

//@SuppressWarnings("rawtypes")
public class SmsMessagesNew  extends BaseBean  implements java.io.Serializable {
//	private static Log log = LogFactory.getLog(SmsMessagesNew.class);
	private static final long serialVersionUID = 1L;
	
    private SmsMessageNew[] messages;

    private String needUseTemplateFlag;

    private String organization;

    private String serviceType;

    private String taskId;

    private String taskValue;

    private String templatId;
    
    private String endDate;

    private String endTime;
    
    private String startDate;

    private String startTime;
    
    private String extension;

    public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public SmsMessagesNew() {
    }

    public SmsMessagesNew(
           String access,
           String endDate,
           String endTime,
           String extension,
           String extensionValue,
           HashMap hashMap,
           HashMap hashMapExten,
           SmsMessageNew[] messages,
           String needUseTemplateFlag,
           String organization,
           String serviceType,
           String smTaskID,
           String startDate,
           String startTime,
           String taskId,
           String taskValue,
           String templatId) {
        this.messages = messages;
        this.needUseTemplateFlag = needUseTemplateFlag;
        this.organization = organization;
        this.serviceType = serviceType;
        this.taskId = taskId;
        this.taskValue = taskValue;
        this.templatId = templatId;
        this.endDate = endDate;
        this.endTime = endTime;
        this.startDate = startDate;
        this.startTime = startTime;
        this.extension = extension;
    }
    

    public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
     * Gets the messages value for this SmsMessages.
     * 
     * @return messages
     */
    public SmsMessageNew[] getMessages() {
        return messages;
    }


    /**
     * Sets the messages value for this SmsMessages.
     * 
     * @param messages
     */
    public void setMessages(SmsMessageNew[] messages) {
        this.messages = messages;
    }


    /**
     * Gets the needUseTemplateFlag value for this SmsMessages.
     * 
     * @return needUseTemplateFlag
     */
    public String getNeedUseTemplateFlag() {
        return needUseTemplateFlag;
    }


    /**
     * Sets the needUseTemplateFlag value for this SmsMessages.
     * 
     * @param needUseTemplateFlag
     */
    public void setNeedUseTemplateFlag(String needUseTemplateFlag) {
        this.needUseTemplateFlag = needUseTemplateFlag;
    }


    /**
     * Gets the organizationId value for this SmsMessages.
     * 
     * @return organizationId
     */
    public String getOrganizationId() {
        return organization;
    }


    /**
     * Sets the organizationId value for this SmsMessages.
     * 
     * @param organizationId
     */
    public void setOrganizationId(String organizationId) {
        this.organization = organizationId;
    }


    /**
     * Gets the serviceType value for this SmsMessages.
     * 
     * @return serviceType
     */
    public String getServiceType() {
        return serviceType;
    }


    /**
     * Sets the serviceType value for this SmsMessages.
     * 
     * @param serviceType
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }



    /**
     * Gets the taskId value for this SmsMessages.
     * 
     * @return taskId
     */
    public String getTaskId() {
        return taskId;
    }


    /**
     * Sets the taskId value for this SmsMessages.
     * 
     * @param taskId
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * Gets the taskValue value for this SmsMessages.
     * 
     * @return taskValue
     */
    public String getTaskValue() {
        return taskValue;
    }

    /**
     * Sets the taskValue value for this SmsMessages.
     * 
     * @param taskValue
     */
    public void setTaskValue(String taskValue) {
        this.taskValue = taskValue;
    }


    /**
     * Gets the templatId value for this SmsMessages.
     * 
     * @return templatId
     */
    public String getTemplatId() {
        return templatId;
    }


    /**
     * Sets the templatId value for this SmsMessages.
     * 
     * @param templatId
     */
    public void setTemplatId(String templatId) {
        this.templatId = templatId;
    }

//	@Override
	public OMElement getXml() {
//		log.info("---into SmsMessages.getXml()");
		System.out.println("---into SmsMessages.getXml()");
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement messagesOm =factory.createOMElement("Messages", null);
		try {
			addOm(messagesOm, "NeedUseTemplateFlag", needUseTemplateFlag);
			addOm(messagesOm, "TemplatId", templatId);
			addOm(messagesOm, "TaskId", taskId);
			addOm(messagesOm, "TaskValue", taskValue);
			addOm(messagesOm, "Organization", organization);
			addOm(messagesOm, "ServiceType", serviceType);
			addOm(messagesOm, "Extension", extension);
			addOm(messagesOm, "StartDate", startDate);
			addOm(messagesOm, "EndDate", endDate);
			addOm(messagesOm, "StartTime", startTime);
			addOm(messagesOm, "EndTime", endTime);
			for (SmsMessageNew smsMessage : messages) {
				OMElement messageOm = factory.createOMElement("Message", null);
				addOm(messageOm, "Receiver", smsMessage.getReceiver());
				addOm(messageOm, "Contents", smsMessage.getContents());
				addOm(messageOm, "OrgCode", smsMessage.getOrgCode());
				messagesOm.addChild(messageOm);
//				log.info("---SmsMessages.getChildXml()=" + smsMessage.getXml().toString());
			}
//			log.info("---SmsMessages.getXml=" + messagesOm.toString());
			System.out.println("---SmsMessages.getXml=" + messagesOm.toString());
		} catch (Exception e){
			e.printStackTrace();
		}
//		log.info("---SmsMessages.getXml() return");
		System.out.println("---SmsMessages.getXml() return");
		return messagesOm;
	}

}
