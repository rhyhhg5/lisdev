/**
 * SmsMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.message;

import java.io.Serializable;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

public class SmsMessageNew extends BaseBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String contents;

	private String orgCode;

	private String receiver;

	public SmsMessageNew() {
	}

	public SmsMessageNew(String contents, String orgCode,
			String receiver) {
		this.contents = contents;
		this.orgCode = orgCode;
		this.receiver = receiver;
	}

	/**
	 * Gets the contents value for this SmsMessage.
	 * 
	 * @return contents
	 */
	public String getContents() {
		return contents;
	}

	/**
	 * Sets the contents value for this SmsMessage.
	 * 
	 * @param contents
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}

	/**
	 * Gets the orgCode value for this SmsMessage.
	 * 
	 * @return orgCode
	 */
	public String getOrgCode() {
		return orgCode;
	}

	/**
	 * Sets the orgCode value for this SmsMessage.
	 * 
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	/**
	 * Gets the receiver value for this SmsMessage.
	 * 
	 * @return receiver
	 */
	public String getReceiver() {
		return receiver;
	}

	/**
	 * Sets the receiver value for this SmsMessage.
	 * 
	 * @param receiver
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

//	@Override
	public OMElement getXml() {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement messageOm = factory.createOMElement("Message", null);
		addOm(messageOm, "Receiver", receiver);
		addOm(messageOm, "Contents", contents);
		addOm(messageOm, "OrgCode", orgCode);
		return messageOm;
	}


}
