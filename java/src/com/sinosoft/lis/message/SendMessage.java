package com.sinosoft.lis.message;

import java.rmi.*;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class SendMessage {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
    private static com.sinosoft.lis.message.SmsMessages msgs = new com.sinosoft.
            lis.message.SmsMessages();

    private TransferData mTransferData = new TransferData();
    //短信类型号
    private String ServiceType = null;
    //客户手机号
    private String[] Receiver = null;
    //短信内容
    private String Contents = null;
    //该短信机构号
    private String[] OrgCode = null;

    private String user = null;

    private String pwd = null;

    public void SendMessage() {

    }

    public boolean submitData(VData cInputData) {

        //得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        // 准备发送短信所需要的数据
        if (!getMessageData()) {
            return false;
        }

        //发送短信
        if (!dealDate()) {
            return false;
        }
        return true;

    }

    /**
     * dealDate
     * 短信发送
     * @return boolean
     */
    private boolean dealDate() {
        com.sinosoft.lis.message.Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        com.sinosoft.lis.message.SmsServiceSoapBindingStub binding = null;
        try {
            binding = (com.sinosoft.lis.message.SmsServiceSoapBindingStub)
                      new com.sinosoft.lis.message.SmsServiceServiceLocator().
                      getSmsService(); //创建binding对象
        } catch (javax.xml.rpc.ServiceException jre) {
            jre.printStackTrace();
            return false;
        }
        binding.setTimeout(60000);

        java.util.Vector vec = new java.util.Vector();
        int leng = Receiver.length;
        for (int i = 0; i < leng; i++) {
            com.sinosoft.lis.message.SmsMessage msg = new com.sinosoft.lis.
                    message.
                    SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            msg.setReceiver(Receiver[i]); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            msg.setContents(Contents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            msg.setOrgCode(OrgCode[i]); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            vec.add(msg);
        }
        msgs.setMessages((com.sinosoft.lis.message.SmsMessage[]) vec.toArray(new
                com.sinosoft.lis.message.SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信

        try {
            value = binding.sendSMS("cbs", "cbs", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
            System.out.println(value.getMessage()); //打印短信内容,not
        } catch (Exception ex) {
            ex.printStackTrace();
            mErrors.addOneError(ex.getMessage());
//         System.out.println("fdsafdas"+ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * getMessageData
     * 准备发送短信所需要的数据
     * @return boolean
     */
    private boolean getMessageData() {
//        user = (String) mTransferData.getValueByName("user"); //短信用户名
//        pwd = (String) mTransferData.getValueByName("pwd"); //短信用户密码

        String Organization = (String) mTransferData.getValueByName(
                "Organization"); //取哪个?用户机构号吗
        if (Organization != null && !Organization.equals("")) {
            msgs.setOrganizationId(Organization); //设置该批短信的OrganizationId，机构号码
        } else {
            msgs.setOrganizationId("0");
        }

        String Extension = (String) mTransferData.getValueByName("Extension");
        if (Extension != null && !Extension.equals("")) {
            msgs.setExtension(Extension); //设置该批短信的Extension,短信扩展码
        } else {
//             msgs.setExtension("024");
        }
        msgs.setServiceType(ServiceType); //设置该批短信的ServiceType,短信类型

        String tCurrentDate = PubFun.getCurrentDate(); //获取当前日期
        String StartDate = (String) mTransferData.getValueByName("StartDate");
        if (StartDate != null) {
            msgs.setStartDate(StartDate); //设置该批短信的StartDate
        } else {
            msgs.setStartDate(tCurrentDate); //若没有则置当前日期
        }
        String EndDate = (String) mTransferData.getValueByName("EndDate");
        if (EndDate != null) {
            msgs.setEndDate(EndDate); //设置该批短信的EndDate
        } else {
            msgs.setStartDate(tCurrentDate); //若没有则置当前日期
        }

        String StartTime = (String) mTransferData.getValueByName("StartTime");
        if (StartTime != null) {
            msgs.setStartTime(StartTime); //设置该批短信的StartTime
        } else {
            msgs.setStartTime("9:00"); //待定??????????????????
        }
        String EndTime = (String) mTransferData.getValueByName("EndTime");
        if (EndTime != null) {
            msgs.setEndTime(EndTime); //设置该批短信的EndTime
        } else {
            msgs.setStartTime("20:00"); //待定??????????????????
        }

        //获取每个客户的机构号，如果有客户机构号不存在，则置为批次的机构号。如果所有客户机构号都不存在，则将此机构号置为批次机构号Organization
        OrgCode = (String[]) mTransferData.getValueByName("OrgCode");
        if (OrgCode != null && OrgCode.length != 0) {
            int leng = OrgCode.length;
            for (int i = 0; i < leng; i++) {
                if (OrgCode[i] == null) {
                    OrgCode[i] = Organization;
                }
            }
        } else {
            int leng = Receiver.length;
            for (int i = 0; i < leng; i++) {
                OrgCode[i] = Organization;
            }
        }
        return true;
    }

    /**
     * getInputData
     * 获取外部所传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        ServiceType = (String) mTransferData.getValueByName("ServiceType"); //短信类型,必传变量
        if (ServiceType == null || ServiceType.equals("")) {
            mErrors.addOneError("短信类型值为空");
            return false;
        }
        Receiver = (String[]) mTransferData.getValueByName("Receiver"); //手机号,必传变量
        if (Receiver == null || Receiver.length == 0) {
            mErrors.addOneError("没有客户手机号");
            return false;
        }
        Contents = (String) mTransferData.getValueByName("Contents"); //短信内容,必传变量
        if (Contents == null || Contents.equals("")) {
            mErrors.addOneError("短信内容为空");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        TransferData tTransferData = new TransferData();
        String[] aa = new String[2];
        aa[0] = "13718725653";
        aa[1] = "13621158294";
        tTransferData.setNameAndValue("Receiver", aa);
        String[] bb = new String[2];
        bb[1] = "86110000";
        tTransferData.setNameAndValue("OrgCode", bb);
        String Contents = "测试短信 hello word!";
        tTransferData.setNameAndValue("Contents", Contents);
        tTransferData.setNameAndValue("ServiceType", "cbs");
        tTransferData.setNameAndValue("user", "cbs");
        tTransferData.setNameAndValue("pwd", "cbs");
        VData tVData = new VData();
        tVData.add(tTransferData);
        SendMessage test = new SendMessage();
        test.submitData(tVData);
    }
}
