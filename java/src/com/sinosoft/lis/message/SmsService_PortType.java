/**
 * SmsService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Aug 08, 2005 (11:49:10 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.message;

public interface SmsService_PortType extends java.rmi.Remote {
    public com.sinosoft.lis.message.Response sendSMS(java.lang.String userName, java.lang.String password, com.sinosoft.lis.message.SmsMessages msgs) throws java.rmi.RemoteException;
}
