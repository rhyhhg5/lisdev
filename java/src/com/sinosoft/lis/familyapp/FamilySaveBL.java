package com.sinosoft.lis.familyapp;

import java.util.ArrayList;
import java.util.Arrays;

import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.XFYJAppntDB;
import com.sinosoft.lis.db.XFYJInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.schema.XFYJAppntSchema;
import com.sinosoft.lis.schema.XFYJInsuredSchema;
import com.sinosoft.lis.schema.XFYJLogSchema;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.XFYJAppntSet;
import com.sinosoft.lis.vschema.XFYJInsuredSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;



/**
 * <p>
 * Title: LIS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author Yangming
 * @version 6.0
 */
public class FamilySaveBL {
	public FamilySaveBL() {
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 往工作流引擎中传输数据的容器 */
	private GlobalInput mGlobalInput = new GlobalInput();

	// private VData mIputData = new VData();
	private TransferData mTransferData = new TransferData();
	private XFYJAppntSet mXfyjAppntSet;

	/** 数据操作字符串 */
	private String mOperater;

	private String mManageCom;

	

	

	private String mBatchno;

	private VData tVData = new VData();
	

	/**
	 * 传输数据的公共方法
	 *
	 * @param: cInputData 输入的数据
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		// 数据操作业务处理
		if (!dealData()) {
			return false;
		}

		System.out.println("-----Go HOME-----");

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData, String cOperate) {
		// 从输入数据中得到所有对象
		// 获得全局公共数据
		mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		tVData.add(mGlobalInput);
		mInputData = cInputData;
		if (mGlobalInput == null) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		// 获得操作员编码
		mOperater = mGlobalInput.Operator;
		if ((mOperater == null) || mOperater.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据Operate失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		// 获得登陆机构编码
		mManageCom = mGlobalInput.ManageCom;
		if ((mManageCom == null) || mManageCom.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		mBatchno = (String) mTransferData.getValueByName("BatchNo");
		if ((mBatchno == null) || mBatchno.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据BatchNO失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		XFYJAppntDB mXfyjAppntDB = new XFYJAppntDB();

		mXfyjAppntSet = mXfyjAppntDB
				.executeQuery("select * from xfyjappnt where batchno='"
						+ mBatchno + "'and prtno <> '000000'");
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 * @return boolean
	 */
	private boolean dealData() {
		boolean flag=true; 
		for (int i = 1; i <= mXfyjAppntSet.size(); i++) {
			flag=true;
			XFYJAppntSchema mXfyjAppntSchema = mXfyjAppntSet.get(i);
			//检查投保人数据
			if(!checkdate(mXfyjAppntSchema)){
				continue;
				
			}
			
			

			String prtno = mXfyjAppntSchema.getPrtNo();
			XFYJInsuredDB mXfyjInsuredDB = new XFYJInsuredDB();

			XFYJInsuredSet mxInsuredSet = mXfyjInsuredDB
					.executeQuery("select * from xfyjinsured where prtno='"
							+ mXfyjAppntSchema.getPrtNo() + "' and other='"+mXfyjAppntSchema.getBatchNo()+"'");
			if(mxInsuredSet.size()==0){
				String error="请核实存在被保人";
				writeerror(error, mXfyjAppntSchema.getPrtNo());
				continue;
			}
			if(mxInsuredSet.size()>6){
				String error="被保人已满额最多6人!";
				writeerror(error, mXfyjAppntSchema.getPrtNo());
				continue;
			}
			for (int j = 1; j <= mxInsuredSet.size(); j++) {
				XFYJInsuredSchema mXfyjInsuredSchema = mxInsuredSet.get(j);
				String relation1=mXfyjInsuredSchema.getRelationToAppnt();
				if(!"00".equals(relation1)){
					if(!checkinsured( mXfyjInsuredSchema)){
						flag=false;
						continue;
					}
				}else{
					SSRS s1=new SSRS();
					ExeSQL mExeSQL = new ExeSQL();				
					String no=mXfyjInsuredSchema.getNo();
					String error1="";
					
					String sql="select count(*) from xfyjinsured where prtno='"+prtno+"' and no ='"+no+"' and other='"+mBatchno+"'";
					s1=mExeSQL.execSQL(sql);
					String num=s1.GetText(1, 1);
					if("0".equals(num)){
						error1="被保人序号不能为空!";
						writeerror(error1, prtno);
						flag=false;
						continue;
					}
					if(!"1".equals(num)){
						error1="同一印刷号下被保人序号不能重复!";
						writeerror(error1, prtno);
						flag=false;
						continue;
					}
					s1.Clear();
					LCInsuredSchema lcInsuredSchema=new LCInsuredSchema();
					lcInsuredSchema.setName(mXfyjAppntSchema.getAppntName());
					lcInsuredSchema.setSex(mXfyjAppntSchema.getAppntSex());
					lcInsuredSchema.setBirthday(mXfyjAppntSchema.getAppntBirthday());
					lcInsuredSchema.setIDType(mXfyjAppntSchema.getAppntIDtype());
					lcInsuredSchema.setIDNo(mXfyjAppntSchema.getAppntIDno());
					String InsuredNo=queryInsuerd(lcInsuredSchema);
					if("".equals(InsuredNo)||InsuredNo==null){
						
					}else{
						  String StrSql="  select distinct lcr.calfactorvalue,lcr.prtno from lcriskdutywrap lcr where insuredno='"+InsuredNo+"' and lcr.riskwrapcode='WR0296' and lcr.calfactor='Copys' and lcr.prtno <> '"+mXfyjInsuredSchema.getPrtNo()+"' and exists (select 1 from lccont  where prtno=lcr.prtno and uwflag<>'a' and stateflag in ('0','1') ) ";
					      String StrSqlMax=" select Maxcopys from lDwrap where riskwrapcode = 'WR0296' and wraptype='10'";
					      s1=mExeSQL.execSQL(StrSql);
					      int copys=0;
					      if(s1.getMaxRow()==0){
					    	  copys=0;
					      }else{
					    	  for(int g=1;g<=s1.getMaxRow();g++){
					    		  int copy=(int)Double.parseDouble(s1.GetText(g, 1));
					    		  copys+=copy;
					    	  }
					      }
					      String str="select copys from xfyjappnt where prtno='"+mXfyjInsuredSchema.getPrtNo()+"' and batchno='"+mBatchno+"'";
					      s1.Clear();
					      s1=mExeSQL.execSQL(str);
					      copys=copys+(int)Double.parseDouble(s1.GetText(1, 1));	  
					    		  
					      s1.Clear();
					      s1=mExeSQL.execSQL(StrSqlMax);
					      int maxcopys=(int)Double.parseDouble(s1.GetText(1, 1));
					      if(maxcopys<copys){
					    	  String error="被保人"+mXfyjAppntSchema.getAppntName()+"购买此套餐已达上限,最多"+maxcopys+"份,不能继续购买!";
					    	  writeerror(error, mXfyjAppntSchema.getPrtNo());
					    	  flag=false;
							  continue;
					      
					      }
					      
					      
						
						
					}
				}

				
				
			}
			if(!flag){
				continue;
			}
			//创建工作流
			createmission(mXfyjAppntSchema);
			//保存数据 即调用新单录入的保存功能
			if(!getdata(mXfyjAppntSchema)){
				flag=false;
			}
			for (int j = 1; j <= mxInsuredSet.size(); j++) {
				XFYJInsuredSchema mXfyjInsuredSchema = mxInsuredSet.get(j);
				//新增被保人数据
				if (!addoneinsured(mXfyjInsuredSchema, mXfyjAppntSchema)) {
					flag=false;
					continue;
				}
				
			}
			//检查被保人年龄
			if(!checkinsuredyear(mXfyjAppntSchema.getPrtNo())){
				continue;
			}
			if(flag){
				//保存险种信息
				addonerisk(mXfyjAppntSchema);
			}
			
		}
		
		System.out.println("--------over-----------");
		ExeSQL meExeSQL = new ExeSQL();
		SSRS ssrs =meExeSQL.execSQL("select * from xfyjappnt where batchno='"+mBatchno+"' and state='3' and prtno='000000'");
		if(ssrs.getMaxRow()==0){
			meExeSQL.execUpdateSQL("update xfyjappnt set state='1' where batchno='"
					+ mBatchno + "' and prtno='000000'");
		}
		

		return true;
	}

	private boolean getlccontschema(XFYJAppntSchema mXfyjAppntSchema) {

		return true;
	}

	private boolean getdata(XFYJAppntSchema mXfyjAppntSchema) {
		LCContSchema tLCContSchema = new LCContSchema();

		ExeSQL eSql = new ExeSQL();
		tLCContSchema.setContNo("");
		tLCContSchema.setPrtNo(mXfyjAppntSchema.getPrtNo());
		tLCContSchema.setContType("1");
		tLCContSchema.setCardFlag("");
		tLCContSchema.setManageCom(mXfyjAppntSchema.getManageCom());
		tLCContSchema.setAgentCom(mXfyjAppntSchema.getAgentCom());

		SSRS ssrs = eSql
				.execSQL("select agentgroup from laagent where groupagentcode='"
						+ mXfyjAppntSchema.getAgentCode() + "'");
		String magentgroup = ssrs.GetText(1, 1);

		ssrs = eSql
				.execSQL("select agentcode from laagent where groupagentcode='"
						+ mXfyjAppntSchema.getAgentCode() + "'");
		String magentcode = ssrs.GetText(1, 1);
		ssrs.Clear();

		ssrs = eSql
				.execSQL("select agenttype from laagent where groupagentcode='"
						+ mXfyjAppntSchema.getAgentCode() + "'");
		String magenttype = ssrs.GetText(1, 1);

		tLCContSchema.setAgentCode(magentcode);
		tLCContSchema.setAgentGroup(magentgroup);
		tLCContSchema.setAgentType(magenttype);
		tLCContSchema.setSaleChnl(mXfyjAppntSchema.getSaleChnl());
		tLCContSchema.setAppntNo("");// 投保人客户号
		tLCContSchema.setAppntName(mXfyjAppntSchema.getAgentName());// 投保人姓名
		tLCContSchema.setAppntSex(mXfyjAppntSchema.getAppntSex());// 投保人性别
		tLCContSchema.setAppntBirthday(mXfyjAppntSchema.getAppntBirthday());
		tLCContSchema.setCValiDate(mXfyjAppntSchema.getPolapplyDate());
		tLCContSchema.setPayIntv(mXfyjAppntSchema.getPayIntv());
		tLCContSchema.setPayMode(mXfyjAppntSchema.getPayMode());
		tLCContSchema.setBankCode(mXfyjAppntSchema.getBankCode());
		tLCContSchema.setBankAccNo(mXfyjAppntSchema.getBankAccNo());

		tLCContSchema.setAccName(mXfyjAppntSchema.getAccName());

		tLCContSchema.setRemark(mXfyjAppntSchema.getRemark());
		ssrs.Clear();
		ssrs = eSql.execSQL("select count(*) from xfyjinsured where prtno='"
				+ mXfyjAppntSchema.getPrtNo() + "' and other='"+mBatchno+"'");
		String mpeoples = ssrs.GetText(1, 1);
		tLCContSchema.setPeoples(mpeoples);
		tLCContSchema.setInputDate(PubFun.getCurrentDate());
		tLCContSchema.setInputTime(PubFun.getCurrentTime());
		tLCContSchema.setPolApplyDate(mXfyjAppntSchema.getPolapplyDate());
		tLCContSchema.setReceiveDate(mXfyjAppntSchema.getReceiveDate());
		tLCContSchema.setTempFeeNo("");
		tLCContSchema.setGrpAgentCom(mXfyjAppntSchema.getGrpAgentCom());
		tLCContSchema.setGrpAgentIDNo(mXfyjAppntSchema.getGrpAgentIDno());
		tLCContSchema.setDueFeeMsgFlag(mXfyjAppntSchema.getDueFeeMsgFlag());
		tLCContSchema.setGrpAgentCode(mXfyjAppntSchema.getGrpAgentCode());
		tLCContSchema.setGrpAgentName(mXfyjAppntSchema.getGrpAgentName());

		tLCContSchema.setAgentSaleCode(mXfyjAppntSchema.getAgentSaleCode());

		// 集团交叉业务
		tLCContSchema.setCrs_SaleChnl(mXfyjAppntSchema.getCrs_SaleChnl());
		tLCContSchema.setCrs_BussType(mXfyjAppntSchema.getCrs_SaleType());

		// 缴费模式
		tLCContSchema.setPayMethod(mXfyjAppntSchema.getPayMode());
		LCAppntSchema tLCAppntSchema = new LCAppntSchema();

		tLCAppntSchema.setAppntNo("");
		tLCAppntSchema.setPosition(mXfyjAppntSchema.getPosition());
		tLCAppntSchema.setSalary(mXfyjAppntSchema.getSalary());
		ExeSQL eSql1 = new ExeSQL();
		SSRS ssrs1 = eSql1
				.execSQL("select OccupationType from LDOCCUPATION where OccupationCode='"
						+ mXfyjAppntSchema.getOccupationCode() + "'");
		String mOccupationType = ssrs1.GetText(1, 1);

		tLCAppntSchema.setOccupationType(mOccupationType);		
		tLCAppntSchema.setOccupationCode(mXfyjAppntSchema.getOccupationCode());
		tLCAppntSchema.setAppntName(mXfyjAppntSchema.getAppntName());
		tLCAppntSchema.setAppntSex(mXfyjAppntSchema.getAppntSex());
		tLCAppntSchema.setAppntBirthday(mXfyjAppntSchema.getAppntBirthday());
		tLCAppntSchema.setIDType(mXfyjAppntSchema.getAppntIDtype());
		tLCAppntSchema.setIDStartDate(mXfyjAppntSchema.getIDStartDate());
		tLCAppntSchema.setIDEndDate(mXfyjAppntSchema.getIDEndDate());
		tLCAppntSchema.setIDNo(mXfyjAppntSchema.getAppntIDno());
		tLCAppntSchema.setNativeCity(mXfyjAppntSchema.getNativePlace());
		tLCAppntSchema.setNativePlace(mXfyjAppntSchema.getNativePlace());

		System.out.println("处理完成投保人信息！");
		System.out.println("开始处理地址信息~");

		// #1855 客户类型
		LCContSubSchema tLCContSubSchema = new LCContSubSchema();
		tLCContSubSchema.setPrtNo(mXfyjAppntSchema.getPrtNo());
		tLCContSubSchema.setCountyType(mXfyjAppntSchema.getAppntType());
		tLCContSubSchema.setFamilySalary(mXfyjAppntSchema.getFamilySalary());

		LCAddressSchema mAppntAddressSchema = new LCAddressSchema();

		mAppntAddressSchema.setGrpName(mXfyjAppntSchema.getWorkName());
		mAppntAddressSchema.setPostalAddress(mXfyjAppntSchema.getPostAddress());
		mAppntAddressSchema.setZipCode(mXfyjAppntSchema.getZipCode());
		mAppntAddressSchema.setPhone(mXfyjAppntSchema.getPhone());
		mAppntAddressSchema.setHomePhone(mXfyjAppntSchema.getHomePhone());
		mAppntAddressSchema.setMobile(mXfyjAppntSchema.getMobile());
		mAppntAddressSchema.setEMail(mXfyjAppntSchema.getEmail());
		mAppntAddressSchema.setPostalAddress(mXfyjAppntSchema.getPostAddress());

		LCExtendSchema tLCExtendSchema = new LCExtendSchema();
		tLCExtendSchema.setPrtNo(mXfyjAppntSchema.getPrtNo());

		// 处理LDPerson信息
		String tWorkName = mXfyjAppntSchema.getWorkName();
		System.out.println("工作单位是..." + tWorkName);
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("AppntAddress", mAppntAddressSchema);
		tTransferData.setNameAndValue("WorkName", tWorkName);
		tTransferData.setNameAndValue("MissionProp5",
				"");
		tTransferData.setNameAndValue("ContType", "1");

		VData tvData = new VData();
		tvData.add(tTransferData);
		tvData.add(tLCAppntSchema);
		tvData.add(tLCContSubSchema);
		tvData.add(tLCContSchema);
		tvData.add(tLCExtendSchema);
		tvData.add(mGlobalInput);

		FamilyContUI tBriefFamilyContUI = new FamilyContUI();
		tBriefFamilyContUI.submitData(tvData, "INSERT||MAIN");
		CErrors mError = tBriefFamilyContUI.mErrors;
		if (!mError.needDealError()) {
		} else {
			String error = mError.getFirstError();
			XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
			mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
					null));
			mXfyjLogSchema.setPrtNo(mXfyjAppntSchema.getPrtNo());
			mXfyjLogSchema.setBatchNo(mBatchno);
			mXfyjLogSchema.setLogInfo(error);
			mXfyjLogSchema.setOther("导入失败！");
			mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
			mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
			MMap tMMap = new MMap();
			tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

			VData tVData = new VData();
			tVData.add(tMMap);

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tVData, "DELETE&INSERT")) {
				CError tError = new CError();
				tError.moduleName = "FamilyTaxBL";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "生成日志失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			ExeSQL meExeSQL = new ExeSQL();
			meExeSQL.execUpdateSQL("update xfyjappnt set state='3' where batchno='"
					+ mBatchno
					+ "' and prtno='"
					+ mXfyjAppntSchema.getPrtNo()
					+ "'");
			return false;
		}
		return true;
	}

	private boolean createmission(XFYJAppntSchema mXfyjAppntSchema) {
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("PrtNo", mXfyjAppntSchema.getPrtNo());
		tTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
		tTransferData.setNameAndValue("ManageCom", mGlobalInput.ManageCom);
		tTransferData.setNameAndValue("ScanOperator", mGlobalInput.Operator);

		/** 总变量 */
		VData tmData = new VData();
		tmData.add(mGlobalInput);
		tmData.add(tTransferData);
		ActivityOperator aOperator = new ActivityOperator();
		String tProcessID = "0000000013";
		
		LWMissionSchema tLwMissionSchema = aOperator.CreateStartMission(tProcessID, tmData);
		System.out.println(tLwMissionSchema.getMissionProp1());
		tLwMissionSchema.setMissionProp1(mXfyjAppntSchema.getPrtNo());
		tLwMissionSchema.setMissionProp2(PubFun.getCurrentDate());
		tLwMissionSchema.setMissionProp3(mGlobalInput.ManageCom);
		tLwMissionSchema.setMissionProp4(mGlobalInput.Operator);
		tLwMissionSchema.setMissionProp5("");
		tLwMissionSchema.setMissionProp7("");
		MMap tMMap = new MMap();
		tMMap.put(tLwMissionSchema, "INSERT");

		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "FamilyTaxBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "生成日志失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;

	}

	private boolean addoneinsured(XFYJInsuredSchema mXfyjInsuredSchema,
			XFYJAppntSchema mXfyjAppntSchema) {
		
		String relation1=mXfyjInsuredSchema.getRelationToAppnt();
//		if(!"00".equals(relation1)){
//			if(!checkinsured( mXfyjInsuredSchema)){
//				return false;
//			}
//		}
//
//		SSRS s1=new SSRS();
//		ExeSQL mExeSQL = new ExeSQL();
//		String prtno=mXfyjInsuredSchema.getPrtNo();
//		String no=mXfyjInsuredSchema.getNo();
//		String error1="";
//		
//		String sql="select count(*) from xfyjinsured where prtno='"+prtno+"' and no ='"+no+"'";
//		s1=mExeSQL.execSQL(sql);
//		String num=s1.GetText(1, 1);
//		if("0".equals(num)){
//			error1="被保人序号不能为空!";
//			writeerror(error1, prtno);
//			return false;
//		}
//		if(!"1".equals(num)){
//			error1="同一印刷号下被保人序号不能重复!";
//			writeerror(error1, prtno);
//			return false;
//		}
		String tAction = "INSERT||MAIN";
		
		// 处理被保人
		LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		
		tLCInsuredSchema.setPrtNo(mXfyjInsuredSchema.getPrtNo());
		tLCInsuredSchema.setRelationToMainInsured(mXfyjInsuredSchema
				.getRelationToMainInsured());// 朱被保险人(第一被保险人)
		tLCInsuredSchema.setRelationToAppnt(mXfyjInsuredSchema
				.getRelationToAppnt());
		String relation = mXfyjInsuredSchema.getRelationToAppnt();
		if ("00".equals(relation)) {

			tLCInsuredSchema.setPosition(mXfyjAppntSchema.getPosition());
			tLCInsuredSchema.setSalary(mXfyjAppntSchema.getSalary());// 年收入

			ExeSQL eSql = new ExeSQL();
			SSRS ssrs = eSql
					.execSQL("select OccupationType from LDOCCUPATION where OccupationCode='"
							+ mXfyjAppntSchema.getOccupationCode() + "'");
			String mOccupationType = ssrs.GetText(1, 1);
			tLCInsuredSchema.setOccupationType(mOccupationType);
			tLCInsuredSchema.setOccupationCode(mXfyjAppntSchema
					.getOccupationCode());
			tLCInsuredSchema.setOperator(mOperater);
			tLCInsuredSchema.setMakeDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setMakeTime(PubFun.getCurrentTime());
			tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
			tLCInsuredSchema.setName(mXfyjAppntSchema.getAppntName());
			tLCInsuredSchema.setSex(mXfyjAppntSchema.getAppntSex());
			tLCInsuredSchema.setIDStartDate(mXfyjAppntSchema.getIDStartDate());
			tLCInsuredSchema.setIDEndDate(mXfyjAppntSchema.getIDEndDate());
			tLCInsuredSchema.setBirthday(mXfyjAppntSchema.getAppntBirthday());
			tLCInsuredSchema.setIDType(mXfyjAppntSchema.getAppntIDtype());
			tLCInsuredSchema.setIDNo(mXfyjAppntSchema.getAppntIDno());
			tLCInsuredSchema.setNativePlace(mXfyjAppntSchema.getNativePlace());
			tLCInsuredSchema.setNativeCity(mXfyjAppntSchema.getNativePlace());

		} else {
			tLCInsuredSchema.setPosition(mXfyjInsuredSchema.getPosition());
			tLCInsuredSchema.setSalary(mXfyjInsuredSchema.getSalary());// 年收入

			ExeSQL eSql = new ExeSQL();
			SSRS ssrs = eSql
					.execSQL("select OccupationType from LDOCCUPATION where OccupationCode='"
							+ mXfyjInsuredSchema.getOccupationCode() + "'");
			String mOccupationType = ssrs.GetText(1, 1);
			tLCInsuredSchema.setOccupationType(mOccupationType);
			tLCInsuredSchema.setOccupationCode(mXfyjInsuredSchema
					.getOccupationCode());
			tLCInsuredSchema.setOperator(mOperater);
			tLCInsuredSchema.setMakeDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setMakeTime(PubFun.getCurrentTime());
			tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
			tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
			tLCInsuredSchema.setName(mXfyjInsuredSchema.getInsuredName());
			tLCInsuredSchema.setSex(mXfyjInsuredSchema.getInsuredSex());
			tLCInsuredSchema
					.setIDStartDate(mXfyjInsuredSchema.getIDStartDate());
			tLCInsuredSchema.setIDEndDate(mXfyjInsuredSchema.getIDEndDate());
			tLCInsuredSchema.setBirthday(mXfyjInsuredSchema.getBirthday());
			tLCInsuredSchema.setIDType(mXfyjInsuredSchema.getIDType());
			tLCInsuredSchema.setIDNo(mXfyjInsuredSchema.getIDNo());
			tLCInsuredSchema.setRelationToAppnt(mXfyjInsuredSchema
					.getRelationToAppnt());
			tLCInsuredSchema
					.setNativePlace(mXfyjInsuredSchema.getNativePlace());
			tLCInsuredSchema.setNativeCity(mXfyjInsuredSchema.getNativePlace());

		}

		System.out.println("处理完成被保人信息！");

		LCAddressSchema mInsuredAddressSchema = new LCAddressSchema();
		/**
		 * 1、联系地址 PostalAddress 2、邮政编码 ZipCode 3、家庭电话 HomePhone 4、移动电话 Mobile
		 * 5、办公电话 CompanyPhone 6、电子邮箱 EMail
		 */

		System.out.println("开始处理被保人地址信息");
		if ("00".equals(relation)) {
			mInsuredAddressSchema.setGrpName(mXfyjAppntSchema.getWorkName());
			mInsuredAddressSchema.setPostalAddress(mXfyjAppntSchema
					.getPostAddress());
			mInsuredAddressSchema.setZipCode(mXfyjAppntSchema.getZipCode());
			mInsuredAddressSchema.setMobile(mXfyjAppntSchema.getMobile());
			mInsuredAddressSchema.setEMail(mXfyjAppntSchema.getEmail());

		} else {
			mInsuredAddressSchema.setGrpName(mXfyjInsuredSchema.getWorkName());
			mInsuredAddressSchema.setPostalAddress(mXfyjInsuredSchema
					.getPostAddress());
			mInsuredAddressSchema.setZipCode(mXfyjInsuredSchema.getZipCode());
			mInsuredAddressSchema.setMobile(mXfyjInsuredSchema.getMobile());
			mInsuredAddressSchema.setEMail(mXfyjInsuredSchema.getEmail());
		}

		System.out.println("处理完成");

		// 完成全部的封装，开始递交．
		FamilySingleContInputUI tFamilySingleContInputUI = new FamilySingleContInputUI();
		try {
			VData tVData = new VData();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("InsuredAddress",
					mInsuredAddressSchema);
			tTransferData.setNameAndValue("upInsuredno", "");
			tVData.add(tLCInsuredSchema);

			tVData.add(mGlobalInput);
			tVData.add(tTransferData);
			System.out.println("开始递交数据！" + tAction);
			tFamilySingleContInputUI.submitData(tVData, tAction);
			CErrors mError = tFamilySingleContInputUI.mErrors;
			if (!mError.needDealError()) {
			} else {
				String error = mError.getFirstError();
				XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
				mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO",
						"86", null));
				mXfyjLogSchema.setPrtNo(mXfyjInsuredSchema.getPrtNo());
				mXfyjLogSchema.setLogInfo(error);
				mXfyjLogSchema.setOther("导入失败！");
				mXfyjLogSchema.setBatchNo(mBatchno);
				mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
				mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
				mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
				mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
				MMap tMMap = new MMap();
				tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

				VData tvData = new VData();
				tvData.add(tMMap);

				PubSubmit tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(tvData, "DELETE&INSERT")) {
					CError tError = new CError();
					tError.moduleName = "FamilyTaxBL";
					tError.functionName = "PubSubmit";
					tError.errorMessage = "生成日志失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				ExeSQL meExeSQL = new ExeSQL();
				meExeSQL.execUpdateSQL("update xfyjappnt set state='3' where batchno='"
						+ mBatchno
						+ "' and prtno='"
						+ mXfyjAppntSchema.getPrtNo() + "'");

				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return true;

	}

	private boolean addonerisk(XFYJAppntSchema mXfyjAppntSchema) {
		String tAction = "INSERT||MAIN";

		// 合同信息查询
		LCContSchema tLCContSchema = new LCContSchema();
		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setPrtNo(mXfyjAppntSchema.getPrtNo());
		tLCContSchema = tLCContDB.query().get(1);

		// 投保人信息

		LCAppntSchema tLCAppntSchema = new LCAppntSchema();
		LCAppntDB tLCAppntDB = new LCAppntDB();
		tLCAppntDB.setContNo(tLCContSchema.getContNo());
		tLCAppntSchema = tLCAppntDB.query().get(1);

		// 处理被保人
		LCInsuredSet tLCInsuredSet = new LCInsuredSet();
		LCInsuredDB tLCInsuredDB = new LCInsuredDB();
		tLCInsuredDB.setContNo(tLCContSchema.getContNo());
		tLCInsuredSet = tLCInsuredDB.query();

		// 处理套餐信息
		LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

		// select a.riskwrapcode,a.wrapname from ldwrap a where wraptype='10'
		// and (WrapManageCom =86440000 or WrapManageCom=substr(86440000,1,2) or
		// WrapManageCom=substr(86440000,1,4));

		for (int i = 3; i < 13; i = i + 2) {
			LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
			tLCRiskDutyWrapSchema.setRiskWrapCode("WR0296");
			if (i == 3) {
				tLCRiskDutyWrapSchema.setCalFactor("Amnt");
				tLCRiskDutyWrapSchema.setCalFactorValue("");
			} else if (i == 5) {
				tLCRiskDutyWrapSchema.setCalFactor("Prem");
				tLCRiskDutyWrapSchema.setCalFactorValue("");
			} else if (i == 7) {
				tLCRiskDutyWrapSchema.setCalFactor("InsuYear");
				tLCRiskDutyWrapSchema.setCalFactorValue("");
			} else if (i == 9) {
				tLCRiskDutyWrapSchema.setCalFactor("InsuYearFlag");
				tLCRiskDutyWrapSchema.setCalFactorValue("");
			} else if (i == 11) {
				tLCRiskDutyWrapSchema.setCalFactor("Copys");
				tLCRiskDutyWrapSchema.setCalFactorValue(Double
						.toString(mXfyjAppntSchema.getCopys()));
			}

			System.out.println("套餐编码:"
					+ tLCRiskDutyWrapSchema.getRiskWrapCode());
			mLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
		}

		// 处理LDPerson信息
		String tWorkName = mXfyjAppntSchema.getWorkName();
		System.out.println("工作单位是..." + tWorkName);
		// 完成全部的封装，开始递交．
		FamilySingleContUI tFamilySingleContUI = new FamilySingleContUI();

		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("WorkName", tWorkName);
		tTransferData.setNameAndValue("WrapCode", "WR0296");
		tVData.add(tLCContSchema);
		tVData.add(tLCInsuredSet);
		tVData.add(tLCAppntSchema);

		tVData.add(mGlobalInput);
		tVData.add(tTransferData);

		tVData.add(mLCRiskDutyWrapSet);
		System.out.println("开始递交数据！" + tAction);
		tFamilySingleContUI.submitData(tVData, tAction);
		CErrors mError = tFamilySingleContUI.mErrors;
		if (!mError.needDealError()) {
			XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
			mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
					null));
			ExeSQL eSql = new ExeSQL();
			SSRS ssrs = eSql
					.execSQL("select sum(amnt),sum(prem) from lcpol where prtno='"
							+ mXfyjAppntSchema.getPrtNo() + "'");
			String mamnt = ssrs.GetText(1, 1);
			String mprem = ssrs.GetText(1, 2);
			mXfyjLogSchema.setPrtNo(mXfyjAppntSchema.getPrtNo());
			mXfyjLogSchema.setLogInfo("印刷号【" + mXfyjAppntSchema.getPrtNo()
					+ "】保费为:" + mprem + ",保额为：" + mamnt);
			mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setOther("导入成功！");
			mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setBatchNo(mBatchno);
			mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
			mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
			MMap tMMap = new MMap();
			tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

			VData tvData = new VData();
			tvData.add(tMMap);

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tvData, "DELETE&INSERT")) {
				CError tError = new CError();
				tError.moduleName = "FamilyTaxBL";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "生成日志失败!";
				this.mErrors.addOneError(tError);
				return false;
			}

			return true;
		} else {
			String error = mError.getFirstError();
			XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
			mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
					null));
			mXfyjLogSchema.setPrtNo(mXfyjAppntSchema.getPrtNo());
			mXfyjLogSchema.setLogInfo(error);
			mXfyjLogSchema.setBatchNo(mBatchno);
			mXfyjLogSchema.setOther("导入失败！");
			mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
			mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
			MMap tMMap = new MMap();
			tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

			VData tvData = new VData();
			tvData.add(tMMap);

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tvData, "DELETE&INSERT")) {
				CError tError = new CError();
				tError.moduleName = "FamilyTaxBL";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "生成日志失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			ExeSQL meExeSQL = new ExeSQL();
			meExeSQL.execUpdateSQL("update xfyjappnt set state='3' where batchno='"
					+ mBatchno
					+ "' and prtno='"
					+ mXfyjAppntSchema.getPrtNo()
					+ "'");
			return true;
		}

	}
	private boolean checkdate(XFYJAppntSchema xfyj){
		try {
			
		
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		String error="";
		String prtno=xfyj.getPrtNo();		
		s1.Clear();
		String sql1="select 1 from lccont where prtno='"+prtno+"'";
		s1=meExeSQL.execSQL(sql1);
		if(s1.getMaxRow()!=0){
			error="已存在该印刷号的保单，请核实是否需要导入!";
	        ExeSQL mExeSQL=new ExeSQL();
	        mExeSQL.execUpdateSQL("delete from xfyjappnt where prtno='"+prtno+"' and batchno='"+xfyj.getBatchNo()+"'");
	        mExeSQL.execUpdateSQL("delete from xfyjinsured where prtno='"+prtno+"' and other='"+xfyj.getBatchNo()+"'");
	        s1.Clear();
			String sql2="select batchno from xfyjlog where prtno='"+prtno+"'";
			s1=meExeSQL.execSQL(sql2);
			if(s1.getMaxRow()!=0){
				error="批次号为"+s1.GetText(1, 1)+"的批次已处理过该印刷号保单，如需再次处理请先删除该批次,若批次已经确定，请去新单删除该印刷号保单";
			}
	        XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
			mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
					null));
			mXfyjLogSchema.setPrtNo(prtno);
			mXfyjLogSchema.setLogInfo(error);
			mXfyjLogSchema.setBatchNo(mBatchno);
			mXfyjLogSchema.setOther("导入失败！");
			mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
			mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
			mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
			MMap tMMap = new MMap();
			tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

			VData tvData = new VData();
			tvData.add(tMMap);

			PubSubmit tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(tvData, "DELETE&INSERT")) {
				CError tError = new CError();
				tError.moduleName = "FamilyTaxBL";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "生成日志失败!";
				this.mErrors.addOneError(tError);			
			}
			ExeSQL ExeSQL = new ExeSQL();
			ExeSQL.execUpdateSQL("update xfyjappnt set state='2' where batchno='"
					+ mBatchno
					+ "' and prtno='"
					+ "000000"
					+ "'");
			return false;
		}
		s1.Clear();
		String sql2="select count(*) from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+prtno+"'";
		s1=meExeSQL.execSQL(sql2);
		if(!checkappntisnull(prtno)){
			return false;
		}
		if(!checksalechnl(xfyj.getSaleChnl())){
			error="录入的销售渠道有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		String a=checkSaleChnlMixedSales(xfyj.getSaleChnl(), xfyj.getCrs_SaleType());
		if(!("".equals(a) || a == null)){
			error=a;
			writeerror(a, prtno);
			return false;
		}
		if(!checkpayintv(xfyj.getPayIntv())){
			error="录入的缴费频次有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		if(!checkpaymode(xfyj.getPayMode())){
			error="录入的缴费模式有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		
		if(!checkdueflad(xfyj.getDueFeeMsgFlag())){
			error="录入的是否需要提醒信息有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		if(!checkappnttype(xfyj.getAppntType())){
			error="录入的客户类型有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		String zipcode=xfyj.getZipCode();
		if("".equals("")||zipcode==null){			
		}else{
			int len=zipcode.length();
			if(len!=6){
				error="投保人邮编长度必须等于6!";
				writeerror(error, prtno);
				return false;
			}
		}
		String nativeplace=xfyj.getNativePlace();
		if(!"ML".equals(nativeplace)){
			error="国籍输入错误!";
			writeerror(error, prtno);
			return false;
		}
//		s1.Clear();
//		String sql2="select polapplydate from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+prtno+"'";
//		s1=meExeSQL.execSQL(sql2);
		String poldate=xfyj.getPolapplyDate();
		if("".equals(poldate)||poldate==null){
			error="投保单申请日期必须填写!";
			writeerror(error, prtno);
			return false;
		}
//		s1.Clear();
//		String sql3="select payintv,DueFeeMsgFlag,paymode,bankcode,bankaccno,accname from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+prtno+"'";
//		s1=meExeSQL.execSQL(sql3);
		int payintv=xfyj.getPayIntv();
		String DueFeeMsgFlag=xfyj.getDueFeeMsgFlag();
		String paymode=xfyj.getPayMode();
		String bankcode=xfyj.getBankCode();
		String bankaccno=xfyj.getBankAccNo();
		String accname=xfyj.getAccName();
		
		if("".equals(DueFeeMsgFlag)||DueFeeMsgFlag==null){
			error="是否需要续期缴费提醒不能为空！";
			writeerror(error, prtno);
			return false;
		}
		if("".equals(paymode)||paymode==null){
			error="缴费方式不能为空!";
			writeerror(error, prtno);
			return false;
		}
		if("4".equals(paymode)||"6".equals(paymode)){
			if("".equals(bankcode)||bankcode==null){
				error="开户银行不能为空";
				writeerror(error, prtno);
				return false;
				
			}else if("".equals(bankaccno)||bankaccno==null){
				error="账号不能为空";
				writeerror(error, prtno);
				return false;
			}else if("".equals(accname)||accname==null){
				error="户名不能为空";
				writeerror(error, prtno);
				return false;				
			}	
			s1.Clear();
			String sql4="select 1 from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1')  and BankCode = '" + bankcode + "' ";
			s1=meExeSQL.execSQL(sql4);
			
			
			if(s1.getMaxRow()==0){
				error="没有银行编码对应的银行";
				writeerror(error, prtno);
				return false;
			}
		}		
		
//		s1.Clear();
//		String sql5="select crs_salechnl,crs_saletype,grpagentcom,grpagentcode,grpagentname,grpagentidno from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+prtno+"'";
//		s1=meExeSQL.execSQL(sql5);
		String crschnl=xfyj.getCrs_SaleChnl();
		String crstype=xfyj.getCrs_SaleType();
		String grpagentcom=xfyj.getGrpAgentCom();
		
		String grpagentcode=xfyj.getGrpAgentCode();
		String grpagentname=xfyj.getGrpAgentName();
		String grpagentidno=xfyj.getGrpAgentIDno();
		if(!("".equals(crschnl)||crschnl==null)){
			if("".equals(crstype)||crstype==null){
				error="选择集团交叉渠道时，集团交叉业务类型不能为空。";
				writeerror(error, prtno);
				return false;
			}else if("".equals(grpagentcom)||grpagentcom==null){
				error="选择集团交叉渠道时，对方业务员机构不能为空。";
				writeerror(error, prtno);
				return false;
			}else if("".equals(grpagentcode)||grpagentcode==null){
				error="选择集团交叉渠道时，对方业务员代码不能为空";
				writeerror(error, prtno);
				return false;
			}else if("".equals(grpagentname)||grpagentname==null){
				error="选择集团交叉渠道时，对方业务员姓名不能为空。";
				writeerror(error, prtno);
				return false;
			}else if("".equals(grpagentidno)||grpagentidno==null){
				error="选择集团交叉渠道时，对方业务员身份证不能为空。";
				writeerror(error, prtno);
				return false;
			}
			//zxs 20190815 #4502
	    	if(grpagentcode.length()!=10){
	    		error="对方业务员代码字段必须为10位，请核实！";
				writeerror(error, prtno);
				return false;
	    	}
	    	if(crschnl.equals("02")&&grpagentcode.indexOf("3")!=0){
	    		error="交叉销售渠道是寿代健，对方业务员代码需以数字3开头！";
				writeerror(error, prtno);
	    		return false;
	    	}
	    	if(crschnl.equals("01")&&grpagentcode.indexOf("1")!=0){
	    		error="交叉销售渠道是财代健，对方业务员代码需以数字1开头！";
				writeerror(error, prtno);
	    		return false;
	    	}
			
		}else{
			if(!("".equals(crstype)||crstype==null)){
				error="未选择集团交叉渠道时，集团交叉业务类型不能填写。";
				writeerror(error, prtno);
				return false;
			}else if(!("".equals(grpagentcom)||grpagentcom==null)){
				error="未选择集团交叉渠道时，对方业务员机构不能为填写。。";
				writeerror(error, prtno);
				return false;
			}else if(!("".equals(grpagentcode)||grpagentcode==null)){
				error="未选择集团交叉渠道时，对方业务员代码不能为填写。";
				writeerror(error, prtno);
				return false;
			}else if(!("".equals(grpagentname)||grpagentname==null)){
				error="未选择集团交叉渠道时，对方业务员姓名不能为填写。";
				writeerror(error, prtno);
				return false;
			}else if(!("".equals(grpagentidno)||grpagentidno==null)){
				error="未选择集团交叉渠道时，对方业务员身份证不能为填写。";
				writeerror(error, prtno);
				return false;
			}
		}
//		s1.Clear();
//		String sql6="select appntidtype,appntidno,appntbirthday,appntsex  from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+prtno+"'";
//		s1=meExeSQL.execSQL(sql6);
		String idtype=xfyj.getAppntIDtype();
		String idno=xfyj.getAppntIDno();
		String day=xfyj.getAppntBirthday();
		String sex=xfyj.getAppntSex();
		if(!checkidno(idtype, idno, day, prtno, sex)){
			return false;
		}
		if(!("0".equals(sex)||"1".equals(sex)||"2".equals(sex))){
			error="性别填写有误，请查看。！";
			writeerror(error, prtno);
			return false;
		}
		
		
//		s1.Clear();
//		String sql7="select OccupationCode,agentcode,managecom,salechnl,agentcom  from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+prtno+"'";
//		s1=meExeSQL.execSQL(sql7);
		String code=xfyj.getOccupationCode();
		String agentcode=xfyj.getAgentCode();
		
		String managecom=xfyj.getManageCom();
		String saleChnl=xfyj.getSaleChnl();
		String agentcom=xfyj.getAgentCom();
		s1.Clear();
		String sql8="select 1 from ldoccupation where OccupationCode = '" + code +"'";
		s1=meExeSQL.execSQL(sql8);
		
		if(s1.getMaxRow()==0){
			error="有职业代码与系统描述不一致，请查看！";
			writeerror(error, prtno);
			return false;
		}
		
		s1.Clear();
		String sql9="select 1 from LAAgent where groupAgentCode = '" + agentcode +"' and AgentState < '06'";
		s1=meExeSQL.execSQL(sql9);
		
		if(s1.getMaxRow()==0){
			error="该业务员已离职";
			writeerror(error, prtno);
			return false;
		}
		s1.Clear();
		String sql10="select 1 from LAAgent where groupAgentCode = '" + agentcode +"' and managecom = '"+managecom+"'";
		s1=meExeSQL.execSQL(sql10);
		
		if(s1.getMaxRow()==0){
			error="业务员与管理机构不匹配";
			writeerror(error, prtno);
			return false;
		}
		s1.Clear();
		String sql17="select 1 from LAAgent where groupAgentCode = '" + agentcode +"' and name ='"+StrTool.cTrim(xfyj.getAgentName())+"'";
		s1=meExeSQL.execSQL(sql17);
		
		if(s1.getMaxRow()==0){
			error="业务员编码与名字不匹配";
			writeerror(error, prtno);
			return false;
		}
		if(!checkidtype(idtype)){
			error="录入的证件类型有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		s1.Clear();
		String sql13= "select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'";
		s1=meExeSQL.execSQL(sql13);
		String othersign=s1.GetText(1, 3);		
		int first=Integer.parseInt(s1.GetText(1, 1));
		String codelias=s1.GetText(1, 2);
		if(!("".equals(codelias)||codelias==null)){
			int two=Integer.parseInt(s1.GetText(1, 2));
			if("double".equals(othersign)){
				if(idno.length()!=first&&idno.length()!=two){
					error="录入的证件号码有误，请检查！！";
					writeerror(error, prtno);
					return false;
				}
				
			}
			if("between".equals(othersign)){
				if(idno.length()<first||idno.length()>two){
					error="录入的证件号码有误，请检查！！";
					writeerror(error, prtno);
					return false;
				}
				
			}
		}
		
		
		if("min".equals(othersign)){
			if(idno.length()<first){
				error="录入的证件号码有误，请检查！！";
				writeerror(error, prtno);
				return false;
			}
			
		}
		
		String salechnl=xfyj.getSaleChnl();
		if("03".equals(salechnl)||"15".equals(salechnl)||"10".equals(salechnl)||"04".equals(salechnl)||"20".equals(salechnl)){
			String com=xfyj.getAgentCom();
			if(com==null||"".equals(com)){
				error="中介公司代码不能为空！！";
				writeerror(error, prtno);
				return false;
			}
		}else{
			String com=xfyj.getAgentCom();
			if(com==null||"".equals(com)){
				
			}else{
				error="录入的销售渠道，中介公司代码应该为空！！";
				writeerror(error, prtno);
				return false;
			}
		}
		if("04".equals(saleChnl)||"15".equals(saleChnl)||"10".equals(saleChnl)||"03".equals(saleChnl)||"20".equals(salechnl)){
			s1.Clear();
			s1 = meExeSQL.execSQL("select agentcode from laagent where groupagentcode='"
							+ agentcode + "'");
			if(s1.getMaxRow()==0){
				error="请核实业务员编码！";
				writeerror(error, prtno);
				return false;
			}
			String magentcode = s1.GetText(1, 1);
			s1.Clear();
			String sql12= "select 1 from LAComToAgent where AgentCom='" + agentcom
			        + "' and RelaType='1' and AgentCode = '" + magentcode + "'";
			s1=meExeSQL.execSQL(sql12);
		
			if(s1.getMaxRow()==0){
				error="录入的业务员代码与中介机构不匹配！";
				writeerror(error, prtno);
				return false;
			}
			
		}
		s1.Clear();
		s1 = meExeSQL.execSQL("select agentcode from laagent where groupagentcode='"
						+ agentcode + "'");
		if(s1.getMaxRow()==0){
			error="请核实业务员编码！";
			writeerror(error, prtno);
			return false;
		}
		String magentcode = s1.GetText(1, 1);
		s1.Clear();
		String sql11="select 1 from LAAgent a, LDCode1 b where a.AgentCode = '" + magentcode
		        + "' and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName and b.Code = '" + saleChnl + "'"
		        + "union all "
				+ "select 1 from LAAgent a where a.AgentCode = '" + magentcode + "' "
				+ "and '10' = '" + saleChnl + "' and a.BranchType2 = '04'  "
				+ "union all "
				+ "select 1 from LAAgent a where a.AgentCode = '" + magentcode+ "' "
				+ "and '03' = '" + saleChnl + "' and a.BranchType2 = '04'  "
				+ "union all "
				+ "select 1 from LAAgent a where a.AgentCode = '" + magentcode + "' "
				+ "and '02' = '" + saleChnl + "' and a.BranchType = '2' and a.BranchType2 = '02'  "
				;
		s1=meExeSQL.execSQL(sql11);
		
		if(s1.getMaxRow()==0){
			error="业务员和销售渠道不相符";
			writeerror(error, prtno);
			return false;
		}
		double copys=xfyj.getCopys();
		
		if(copys!=1.0&&copys!=2.0){
			error="份数只能为1份或者2份！！";
			writeerror(error, prtno);
			return false;
		}
		
		} catch (Exception e) {
			e.printStackTrace();			
			String error="信息存在问题，请核实！！"+e.getCause();
			String prtno=xfyj.getPrtNo();
			writeerror(error, prtno);
			return false;
		}
		String receivedate=xfyj.getReceiveDate();
		int day=PubFun.calInterval(xfyj.getPolapplyDate(), receivedate, "D");
		
		
		return true;
	}
	
	
	private void writeerror(String error,String prtno){
		XFYJLogSchema mXfyjLogSchema = new XFYJLogSchema();
		mXfyjLogSchema.setBusiNo(PubFun1.CreateMaxNo("XFYJBUSINO", "86",
				null));
		mXfyjLogSchema.setPrtNo(prtno);
		mXfyjLogSchema.setLogInfo(error);
		mXfyjLogSchema.setBatchNo(mBatchno);
		mXfyjLogSchema.setOther("导入失败！");
		mXfyjLogSchema.setMakeDate(PubFun.getCurrentDate());
		mXfyjLogSchema.setModifyDate(PubFun.getCurrentDate());
		mXfyjLogSchema.setMakeTime(PubFun.getCurrentTime());
		mXfyjLogSchema.setModifyTime(PubFun.getCurrentTime());
		MMap tMMap = new MMap();
		tMMap.put(mXfyjLogSchema, "DELETE&INSERT");

		VData tvData = new VData();
		tvData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tvData, "DELETE&INSERT")) {
			CError tError = new CError();
			tError.moduleName = "FamilyTaxBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "生成日志失败!";
			this.mErrors.addOneError(tError);			
		}
		ExeSQL meExeSQL = new ExeSQL();
		meExeSQL.execUpdateSQL("update xfyjappnt set state='3' where batchno='"
				+ mBatchno
				+ "' and prtno='"
				+ "000000"
				+ "'");
		meExeSQL.execUpdateSQL("update xfyjappnt set flag='1' where batchno='"
				+ mBatchno
				+ "' and prtno='"
				+ prtno
				+ "'");
		
		
	}
	
	private boolean checkidno(String idtype,String idno,String day,String prtno,String sex){
		if("0".equals(idtype)){
			String birthday=PubFun.getBirthdayFromId(idno);
			if("".equals(birthday)||!(birthday.equals(day))){
				String error="投保人 或被保人的生日与身份证号输入有问题";
				writeerror(error, prtno);
				return false;
			}
			String sex1=PubFun.getSexFromId(idno);
			System.out.println("idno:"+idno);
			System.out.println("sex:"+sex1);
			if(sex1.equals("")||(!sex1.equals(sex))){
				String error="投保人 或被保人的性别与身份证号输入有问题";
				writeerror(error, prtno);
				return false;
			}
		}
		return true;
	}
	private boolean checkappntisnull(String prtno){
		String [] ss={"管理机构","销售渠道","业务员代码","业务员名称","投保单申请日期"
				,"收单日期","投保人姓名","证件号码","证件生效日","证件失效日期","性别","出生日期","职业代码","套餐份数","缴费频次","缴费方式","客户类型","是否需要续期缴费提醒","联系地址"};
		ArrayList<String> l1=new ArrayList<String>(Arrays.asList(ss));
		String sql="select MANAGECOM,SALECHNL,AGENTCODE,AGENTNAME,POLAPPLYDATE"
				+",RECEIVEDATE,APPNTNAME,APPNTIDNO,IDSTARTDATE,IDENDDATE,APPNTSEX,APPNTBIRTHDAY"
				+",OCCUPATIONCODE,COPYS,PAYINTV,PAYMODE,APPNTTYPE,DUEFEEMSGFLAG,postaddress"
				+" from xfyjappnt where batchno='"+mBatchno+"'"
				+" and prtno='"+prtno+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		for(int i=0;i<s1.getMaxCol();i++){
			String info=s1.GetText(1, i+1);
			if("".equals(info)||info==null){
				String error=l1.get(i)+"为空，请检查";
				writeerror(error, prtno);
				return false;
			}
			
		}
		
		
		return true;
	}
	private boolean checkinsuredyear(String prtno){		
		String sql=" select Birthday,InsuredNo,Name from lcinsured where prtno='"+prtno+"' ";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		if(s1.getMaxRow()<=2){
			for(int i=1;i<=s1.getMaxRow();i++){
				String currentdate=PubFun.getCurrentDate();
				String stardate=s1.GetText(i, 1);
				int day=PubFun.calInterval(stardate, currentdate, "D");
				int age=PubFun.calInterval(stardate, currentdate, "Y");
				if(day<28||age>55){
					String error=s1.GetText(i, 3)+"的年龄不符合投保要求,俩人家庭投保年龄为出生满28天—55周岁!";
					writeerror(error, prtno);
					return false;
				}
			}
			
			
		}
		if(s1.getMaxRow()>2){
			for(int i=1;i<=s1.getMaxRow();i++){
				String currentdate=PubFun.getCurrentDate();
				String stardate=s1.GetText(i, 1);
				int day=PubFun.calInterval(stardate, currentdate, "D");
				int age=PubFun.calInterval(stardate, currentdate, "Y");
				if(day<28||age>65){
					String error=s1.GetText(i, 3)+"的年龄不符合投保要求,3及3人以上的家庭投保年龄为出生满28天—65周岁!";
					writeerror(error, prtno);
					return false;
				}
			}
			
			
		}
		
		return true;
	}
	private boolean checkinsured(XFYJInsuredSchema mXfyjInsuredSchema){
		try {
		SSRS s1=new SSRS();
		ExeSQL meExeSQL = new ExeSQL();
		
		
		
		
		String prtno=mXfyjInsuredSchema.getPrtNo();
		String no=mXfyjInsuredSchema.getNo();
		String error="";
		s1.Clear();
		String sql="select count(*) from xfyjinsured where prtno='"+prtno+"' and no ='"+no+"' and other='"+mBatchno+"'";
		s1=meExeSQL.execSQL(sql);
		String num=s1.GetText(1, 1);
		if("0".equals(num)){
			
			error="被保人序号不能为空!";
			writeerror(error, prtno);
			return false;
		}
		if(!"1".equals(num)){
			error="同一印刷号下被保人序号不能重复!";
			writeerror(error, prtno);
			return false;
		}
		if(!checkinsuredisnull(prtno,mXfyjInsuredSchema.getNo())){
			return false;
		}	
		String zipcode=mXfyjInsuredSchema.getZipCode();
		
		if("".equals("")||zipcode==null){			
		}else{
			int len=zipcode.length();
			if(len!=6){
				error="投保人邮编长度必须等于6!";
				writeerror(error, prtno);
				return false;
			}
		}		
		String nativeplace=mXfyjInsuredSchema.getNativePlace();
		if(!"ML".equals(nativeplace)){
			error="国籍输入错误!";
			writeerror(error, prtno);
			return false;
		}
		String occupation=mXfyjInsuredSchema.getOccupationCode();
		String sql1="select OCCUPATIONTYPE from LDOCCUPATION where OCCUPATIONCODE='"+occupation+"'";
		s1=meExeSQL.execSQL(sql1);
		if(s1.getMaxRow()==0){
			error="未找到职业代码对应的职业类别,请核实职业代码!";
			writeerror(error, prtno);
			return false;
		}else{
			String occpationtype=s1.GetText(1, 1);
			s1.Clear();
			String tsql="select Maxoccutype from lDwrap where riskwrapcode = 'WR0296' and wraptype='10'";
			s1=meExeSQL.execSQL(tsql);
			if(s1.getMaxRow()==0){
				
			}else{
				String maxtype=s1.GetText(1, 1);
				int type=Integer.parseInt(occpationtype);
				int max=Integer.parseInt(maxtype);
				if(type>max){
					error="职业类型不符合要求!";
					writeerror(error, prtno);
					return false;
				}
				
			}
			
		}
		String idtype=mXfyjInsuredSchema.getIDType();
		String idno=mXfyjInsuredSchema.getIDNo();
		String day=mXfyjInsuredSchema.getBirthday();
		String sex=mXfyjInsuredSchema.getInsuredSex();
		if(!checkidno(idtype, idno, day, prtno, sex)){
			return false;
		}
		if(!("0".equals(sex)||"1".equals(sex)||"2".equals(sex))){
			error="性别填写有误，请查看。！";
			writeerror(error, prtno);
			return false;
		}
				
		String code=mXfyjInsuredSchema.getOccupationCode();		
		s1.Clear();
		String sql8="select 1 from ldoccupation where OccupationCode = '" + code +"'";
		s1=meExeSQL.execSQL(sql8);
		
		if(s1.getMaxRow()==0){
			error="有职业代码与系统描述不一致，请查看！";
			writeerror(error, prtno);
			return false;
		}
		
		if(!checkidtype(idtype)){
			error="录入的证件类型有误，请检查！！";
			writeerror(error, prtno);
			return false;
		}
		s1.Clear();
		String sql13= "select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'";
		s1=meExeSQL.execSQL(sql13);
		String othersign=s1.GetText(1, 3);
		int first=Integer.parseInt(s1.GetText(1, 1));
		String codelias=s1.GetText(1, 2);
		if(!("".equals(codelias)||codelias==null)){
			int two=Integer.parseInt(s1.GetText(1, 2));
			if("double".equals(othersign)){
				if(idno.length()!=first&&idno.length()!=two){
					error="录入的证件号码有误，请检查！！";
					writeerror(error, prtno);
					return false;
				}
				
			}
			if("between".equals(othersign)){
				if(idno.length()<first||idno.length()>two){
					error="录入的证件号码有误，请检查！！";
					writeerror(error, prtno);
					return false;
				}
				
			}
		}
		
		
		if("min".equals(othersign)){
			if(idno.length()<first){
				error="录入的证件号码有误，请检查！！";
				writeerror(error, prtno);
				return false;
			}
			
		}
		String relasion=mXfyjInsuredSchema.getRelationToAppnt();
		String [] ss1={"00","01","02","03","04","05","06","07","08","09","11"};
		ArrayList<String> l2=new ArrayList<String>(Arrays.asList(ss1));
		if(!l2.contains(relasion)){
			error="被保人与投保人关系只能是本人、父母、子女、配偶的关系!";
			writeerror(error, prtno);
			return false;
		}
		String rela=mXfyjInsuredSchema.getRelationToMainInsured();
		if(!checkrela(rela)){
			error="请核实被保人与主被保人关系!";
			writeerror(error, prtno);
			return false;
		}
		LCInsuredSchema lcInsuredSchema=new LCInsuredSchema();
		lcInsuredSchema.setName(mXfyjInsuredSchema.getInsuredName());
		lcInsuredSchema.setSex(mXfyjInsuredSchema.getInsuredSex());
		lcInsuredSchema.setBirthday(mXfyjInsuredSchema.getBirthday());
		lcInsuredSchema.setIDType(mXfyjInsuredSchema.getIDType());
		lcInsuredSchema.setIDNo(mXfyjInsuredSchema.getIDNo());
		String InsuredNo=queryInsuerd(lcInsuredSchema);
		if("".equals(InsuredNo)||InsuredNo==null){
			
		}else{
			  String StrSql="  select distinct lcr.calfactorvalue,lcr.prtno from lcriskdutywrap lcr where insuredno='"+InsuredNo+"' and lcr.riskwrapcode='WR0296' and lcr.calfactor='Copys' and lcr.prtno <> '"+mXfyjInsuredSchema.getPrtNo()+"' and exists (select 1 from lccont  where prtno=lcr.prtno and uwflag<>'a' and stateflag in ('0','1') ) ";
		      String StrSqlMax=" select Maxcopys from lDwrap where riskwrapcode = 'WR0296' and wraptype='10'";
		      s1=meExeSQL.execSQL(StrSql);
		      int copys=0;
		      if(s1.getMaxRow()==0){
		    	  copys=0;
		      }else{
		    	  for(int i=1;i<=s1.getMaxRow();i++){
		    		  int copy=(int)Double.parseDouble(s1.GetText(i, 1));
		    		  copys+=copy;
		    	  }
		      }
		      String str="select copys from xfyjappnt where prtno='"+mXfyjInsuredSchema.getPrtNo()+"' and batchno='"+mXfyjInsuredSchema.getOther()+"'";
		      s1.Clear();
		      s1=meExeSQL.execSQL(str);
		      copys=copys+(int)Double.parseDouble(s1.GetText(1, 1));	  
		    		  
		      s1.Clear();
		      s1=meExeSQL.execSQL(StrSqlMax);
		      int maxcopys=(int)Double.parseDouble(s1.GetText(1, 1));
		      if(maxcopys<copys){
		    	  error="被保人"+mXfyjInsuredSchema.getInsuredName()+"购买此套餐已达上限,最多"+maxcopys+"份,不能继续购买!";
		    	  writeerror(error, mXfyjInsuredSchema.getPrtNo());
				  return false;
		      
		      }
		      
		      
			
			
		}
		
		
		} catch (Exception e) {
			e.printStackTrace();
			String error="信息存在问题，请核实！！";
			String prtno=mXfyjInsuredSchema.getPrtNo();
			writeerror(error, prtno);
			return false;
		}
		
		
		return true;
	}
	private boolean checkinsuredisnull(String prtno,String no){
		String [] ss={"与投保人关系","与主被保险人关系","被保人姓名","证件类型","证件号码","性别"
				,"出生日期"};//,"证件生效日","证件失效日期","职业代码","国籍","邮编","联系地址"
		ArrayList<String> l1=new ArrayList<String>(Arrays.asList(ss));
		String sql="select RELATIONTOAPPNT,RELATIONTOMAININSURED,INSUREDNAME,IDTYPE,IDNO,INSUREDSEX"
				+",BIRTHDAY"		//,IDSTARTDATE,IDENDDATE,OCCUPATIONCODE,NATIVEPLACE,ZIPCODE,POSTADDRESS		
				+" from xfyjinsured where "
				+" prtno='"+prtno+"' and no='"+no+"' and other='"+mBatchno+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		for(int i=0;i<s1.getMaxCol();i++){
			String info=s1.GetText(1, i+1);
			if("".equals(info)||info==null){
				String error=l1.get(i)+"为空，请检查";
				writeerror(error, prtno);
				return false;
			}
			
		}
		
		
		return true;
	}
	private boolean checkrela(String rela){
		String sql="select 1 from ldcode where codetype='relation' and code='"+rela+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		if(s1.getMaxRow()==0){
			return false;
		}
		return true;	
		
	}
	private boolean checkidtype(String idtype){
		String sql="select 1 from ldcode where codetype='idtype1' and code='"+idtype+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		if(s1.getMaxRow()==0){
			return false;
		}
		return true;
	}
	
	private boolean checksex(String sex){
		String sql="select 1 from ldcode where codetype='sex' and code='"+sex+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		if(s1.getMaxRow()==0){
			return false;
		}
		return true;
	}
	private boolean checksalechnl(String salechnl){
		String sql="select 1 from ldcode where codetype='salechnl' and code='"+salechnl+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		if(s1.getMaxRow()==0){
			return false;
		}
		return true;
	}
	private boolean checkappnttype(String appnttype){
		String [] ss={"0","1"};
		ArrayList<String> l1=new ArrayList<String>(Arrays.asList(ss));
		if(!l1.contains(appnttype)){
			return false;
		}	
		return true;
		
	}
	private boolean checkpayintv(int payintv){
		if(payintv!=0&&payintv!=12){
			return false;
		}		
		return true;
	}
	private boolean checkpaymode(String paymode){
		String sql="select 1 from ldcode where codetype='paymode' and code='"+paymode+"'";
		ExeSQL meExeSQL = new ExeSQL();
		SSRS s1=new SSRS();
		s1=meExeSQL.execSQL(sql);
		if(s1.getMaxRow()==0){
			return false;
		}
		return true;
	}
	private boolean checkdueflad(String flag){
		String [] ss={"Y","N"};
		ArrayList<String> l1=new ArrayList<String>(Arrays.asList(ss));
		if(!l1.contains(flag)){
			return false;
		}	
		return true;
		
	}
	/**
     * queryInsuerd
     *
     * @return boolean
     */
    private String queryInsuerd(LCInsuredSchema mLCInsuredSchema)
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        String Sex = "";
        String Birthday = "";
        String no="";
        if (!StrTool.cTrim(mLCInsuredSchema.getName()).equals(""))
        {
            tLDPersonDB.setName(mLCInsuredSchema.getName());
        }
        
        if (!StrTool.cTrim(mLCInsuredSchema.getSex()).equals(""))
        {
            Sex = mLCInsuredSchema.getSex();
        }
        
        if (!StrTool.cTrim(mLCInsuredSchema.getBirthday()).equals(""))
        {
            Birthday = mLCInsuredSchema.getBirthday();
        }
        
        if (!StrTool.cTrim(mLCInsuredSchema.getIDType()).equals(""))
        {
            
            String tIDType = mLCInsuredSchema.getIDType();

            tLDPersonDB.setIDType(mLCInsuredSchema.getIDType());
        }
      
        if (!StrTool.cTrim(mLCInsuredSchema.getIDNo()).equals(""))
        {
            tLDPersonDB.setIDNo(mLCInsuredSchema.getIDNo());
        }
      

        LDPersonSet tLDPersonSet = new LDPersonSet();

        if (tLDPersonDB.getIDType().equals("0"))
        {
            tLDPersonSet = tLDPersonDB.query();
        }
        else
        {
            tLDPersonDB.setSex(Sex);
            tLDPersonDB.setBirthday(Birthday);
            tLDPersonSet = tLDPersonDB.query();
        }

        if (tLDPersonSet.size() >= 1)
        {
            /** 找到为一个客户信息 */
            /** 取出当前客户的客户号码 */
            no=tLDPersonSet.get(1).getCustomerNo();
            
        }
        else if (tLDPersonSet.size() == 0)
        {
            
        }
       
        return no;
    }

	private String checkSaleChnlMixedSales(String salechnl,String Crs_BussType){
		
		String str="";
		System.out.println("Crs_BussType ="+Crs_BussType);
		if("".equals(Crs_BussType) || Crs_BussType == null){
			str="";
		}else{
			if("16".equals(salechnl) || "18".equals(salechnl) || "20".equals(salechnl)){
				System.out.println("社保渠道不得有交叉销售业务！");
				str="社保渠道不得有交叉销售业务！";
			}else if(("14".equals(salechnl)||"01".equals(salechnl) || "02".equals(salechnl) || "13".equals(salechnl)||"17".equals(salechnl)||"21".equals(salechnl))  && !"02".equals(Crs_BussType)  && !"03".equals(Crs_BussType) && !"13".equals(Crs_BussType) && !"14".equals(Crs_BussType)){
				System.out.println("销售渠道为直销渠道时，交叉销售业务类型只能是联合展业、渠道共享、互动部、农网共建！");
				str="销售渠道为直销渠道时，交叉销售业务类型只能是联合展业、渠道共享、互动部、农网共建！";
			}else if ("15".equals(salechnl) && !"01" .equals(Crs_BussType)) {
				System.out.println("销售渠道为互动中介时，交叉销售业务类型只能是相互代理！");
				str="销售渠道为互动中介时，交叉销售业务类型只能是相互代理！";
			}else if("03".equals(salechnl)||"10".equals(salechnl)||"22".equals(salechnl)){
					System.out.println("除互动中介外其他中介渠道不能选择交叉销售！");
					str="除互动中介外其他中介渠道不能选择交叉销售！";
				
			}else if(!"14".equals(salechnl)  && !"15".equals(salechnl) && !"16".equals(salechnl) && !"18".equals(salechnl) && !"20".equals(salechnl) && !"02".equals(Crs_BussType)){
//				System.out.println("非社保渠道和互动渠道，交叉销售业务类型只能是相互代理、联合展业！");
//				str="非社保渠道和互动渠道，交叉销售业务类型只能是相互代理、联合展业！";
				System.out.println("非互动中介渠道，交叉销售业务类型只可选择联合展业！");
				str="非互动中介渠道，交叉销售业务类型只可选择联合展业！";
			}
		}
		return str;
		
	}
    
    
    
    

}
