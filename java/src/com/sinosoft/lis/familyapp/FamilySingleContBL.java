package com.sinosoft.lis.familyapp;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.db.LDOccupationDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.db.LMDutyDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyDB;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.CheckFieldCom;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.FieldCarrier;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LJTempFeeClassBSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCCustomerImpartDetailSet;
import com.sinosoft.lis.vschema.LCCustomerImpartParamsSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.LMRiskDutySet;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FamilySingleContBL
{
    /** 报错对象 */
    public CErrors mErrors = new CErrors();

    /** 操作完成保存结果 */
    private VData mResult = new VData();

    /** 前台传入的封装对象 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 个人合同信息 */
    private LCContSchema mLCContSchema;

    /** 客户登陆信息 */
    private GlobalInput mGlobalInput;

    /** 被保险人信息 */
    private LCInsuredSchema mLCInsuredSchema;

    /** 被保险人信息列表 */
    private LCInsuredSet mLCInsuredSet;

    /** 受益人信息 */
    private LCBnfSet mLCBnfSet = new LCBnfSet();
    
    /** 受益人信息汇总 */
    private LCBnfSet tLCBnfSet = new LCBnfSet();

    /** 套餐险种表 */
    private LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

    /** 套餐险种表 */
    private LCRiskDutyWrapSet LLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

    /** 套餐险种表汇总 */
    private LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

    /** 责任信息汇总 */
    private LCDutySet mLCDutySet = new LCDutySet();

    /** 责任信息 */
    private LCDutySet LLCDutySet;

    /** 险种号码 */
    private String mPolNo;

    /** 投保客户信息 */
    private LCAppntSchema mLCAppntSchema;

    /** 投保人客户号 */
    private String mAppntNo;

    /** 险种信息 */
    private LCPolSet mLCPolSet = new LCPolSet();

    /** 险种信息汇总 */
    private LCPolSet tLCPolSet = new LCPolSet();

    /** 团体合同号 */
    private String mContNo;

    /** 套餐中的险种个数 */
    private int mNo;

    /** 被保人个数 */
    private double mInsuredNum;

    /**汇总*/
    private LCPremSet mLCPremSet = new LCPremSet();

    private LCPremSet LLCPremSet;

    /**汇总*/
    private LCGetSet mLCGetSet = new LCGetSet();

    private LCGetSet LLCGetSet;

    /** 险种信息 */
    // private LMRiskAppSet mLMRiskAppSet;
    /** 当前时间 */
    private String mCurrentData = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    /** 递交数据 */
    private MMap map;

    /** 保单级的保费，保额，档次 */
    private double sumPrem, sumAmnt, sumMult;

    /** 封装地址信息的 */
    private TransferData mTransferData = new TransferData();

    /** 工作流信息 */
    private LWMissionSet mLWMissionSet;

    /** 前台传入的受益人信息 */
    private LCBnfSet mInputLCBnfSet;

    /** 数据库递交的受益人信息 */
    private LCBnfSet mLCBnfet = new LCBnfSet();

    /** 险种套餐信息 */
    private LDRiskWrapSet mLDRiskWrapSet = new LDRiskWrapSet();

    /** 需要进行制定保险期间 */
    private boolean needInsuYear = true;

    /** 缓存 */
    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();

    /** 报错 */
    private VData errorVData = new VData();

    /** 添加日志对象 */
    private static Logger log = Logger.getLogger(FamilySingleContBL.class);

    /** 描述套餐要素信息 */
    private LDRiskDutyWrapSet mLDRiskDutyWrapSet;

    // 录入日期
    private String polApplyDate;

    //电子商务报文类型，当为试算时，不生成polno，避免浪费数据。
    private String mMsgType = "";

    //折扣
    private String FeeRate = "";

    public FamilySingleContBL()
    {
    }

    /**
     * submitData
     *
     * @param nInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate)
    {
        System.out.println("Into FamilySingleContBL.submitData()...");
        if (getSubmitMap(nInputData, cOperate) == null)
        {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, "INSERT"))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
       
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.add(this.mLCContSchema);
        mResult.add(this.tLCPolSet);

        if (this.mOperate.equals("INSERT||MAIN") || this.mOperate.equals("UPDATE||MAIN"))
        {
            if (map == null)
            {
                map = new MMap();
            }

            map.put(this.mLCContSchema, "DELETE&INSERT");
            map.put(this.tLCPolSet, "INSERT");
            map.put(this.mLCDutySet, "INSERT");
            map.put(this.mLCPremSet, "INSERT");
            map.put(this.mLCGetSet, "INSERT");


            if (this.tLCBnfSet != null && this.tLCBnfSet.size() > 0)
            {
                for (int i = 1; i <= tLCBnfSet.size(); i++)
                {
                    System.out.println("测试多受益人 PolNo: " + tLCBnfSet.get(i).getPolNo());
                }
                map.put(tLCBnfSet, "INSERT");
            }

            ///** 处理特殊字符，否则入库时会报错。
            if (tLCRiskDutyWrapSet != null && tLCRiskDutyWrapSet.size() > 0)
            {
                for (int i = 1; i <= tLCRiskDutyWrapSet.size(); i++)
                {
                    String tStrCalSql = tLCRiskDutyWrapSet.get(i).getCalSql();
                    if (tStrCalSql != null)
                        tStrCalSql = tStrCalSql.replaceAll("'", "''");
                    tLCRiskDutyWrapSet.get(i).setCalSql(tStrCalSql); // CalSql无用，不需要赋值
                }
            }
            //------------------------------------------------*/

            map.put(tLCRiskDutyWrapSet, "DELETE&INSERT");

            mResult.add(map);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if (this.map.size() > 0)
            {
                this.mResult.add(map);
            }
            else
            {
                buildError("prepareOutputData", "进行删除操作,但没有删除数据！");
                return false;
            }
        }
        return true;
    }

    /**
     * 生成简易平台保单信息，并返回MMap结果集
     *
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return MMap FamilySingleContBL
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        System.out.println("Into FamilySingleContBL.getSubmitMap()...");
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        System.out.println("@@完成程序submitData第41行");

        if (!getInputData())
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        if (!fillApprovUWField())
        {
            return null;
        }

        if (!prepareOutputData())
        {
            return null;
        }

        return map;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Into FamilySingleContBL.dealData()..." + mOperate);
        /** 执行新单录入过程 */
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if(!deletedate()){
                return false;
            }
            
            if (!insertData())
            {
                return false;
            }
        }

        else
        {
            buildError("dealData", "目前只支持保存修改,删除功能不支持其它操作！");
            return false;
        }
        return true;
    }
    
    /**
     * 
     */
    
    private boolean deletedate(){
        
        if (map == null)
        {
            map = new MMap();
        }

        String wherePart_ContNo = " and ContNo = '" + this.mLCContSchema.getContNo() + "'";
        map.put("delete from lcpol where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from lcduty where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from lcprem where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from lcget where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from LCBnf where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from LCRiskDutyWrap where 1=1 " + wherePart_ContNo, "DELETE");
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData()
    {
        System.out.println("Into FamilySingleContBL.insertData()...");

        if (!dealRisk())
        {
            return false;
        }
        
        if(!dealBnf()){
            return false;
        }

        return true;
    }
    
    /**
     * 处理受益人信息多受益人
     * @return
     */
    private boolean dealBnf(){
        if(mLCBnfSet!=null && mLCBnfSet.size()>0){
            for(int i=1;i<= mLCBnfSet.size(); i++){
                LCBnfSchema tLCBnfSchema= new LCBnfSchema();
                tLCBnfSchema=mLCBnfSet.get(i);
                
                //被保人险种信息
                for(int j=1;j<=tLCPolSet.size();j++){
                    if(tLCBnfSchema.getCustomerNo().equals(tLCPolSet.get(j).getInsuredNo())){
                        LCBnfSchema LLCBnfSchema = new LCBnfSchema();
                  
                        LLCBnfSchema.setContNo(tLCPolSet.get(j).getContNo());
                        LLCBnfSchema.setPolNo(tLCPolSet.get(j).getPolNo());
                        LLCBnfSchema.setInsuredNo(tLCPolSet.get(j).getInsuredNo());
                        LLCBnfSchema.setBnfType(tLCBnfSchema.getBnfType());
                        LLCBnfSchema.setBnfNo(i);
                        LLCBnfSchema.setBnfLot(tLCBnfSchema.getBnfLot());
                        LLCBnfSchema.setBnfGrade(tLCBnfSchema.getBnfGrade());
                        LLCBnfSchema.setRelationToInsured(tLCBnfSchema.getRelationToInsured());
                        
                        LLCBnfSchema.setName(tLCBnfSchema.getName());
                        LLCBnfSchema.setSex(tLCBnfSchema.getSex());
                        LLCBnfSchema.setIDType(tLCBnfSchema.getIDType());
                        LLCBnfSchema.setIDNo(tLCBnfSchema.getIDNo());
                        String s = PubFun.CheckIDNo(tLCBnfSchema.getIDType(), tLCBnfSchema.getIDNo(), tLCBnfSchema.getBirthday(), tLCBnfSchema.getSex());
    					if (!"".equals(s))
    					{
    						buildError("dealBnf", "受益人信息错误：" + s);
    						return false;
    					}
                        LLCBnfSchema.setOperator(mGlobalInput.Operator);
                        LLCBnfSchema.setMakeDate(mCurrentData);
                        LLCBnfSchema.setMakeTime(mCurrentTime);
                        LLCBnfSchema.setModifyDate(mCurrentData);
                        LLCBnfSchema.setModifyTime(mCurrentTime);
                        
                        tLCBnfSet.add(LLCBnfSchema);
                    }
                }
                
            }
        }
        return true;
    }

    /**
     * 为LCCont、LCPol的复核和自动核保字段复制
     *
     * @return boolean
     */
    private boolean fillApprovUWField()
    {
        if (this.mLCContSchema == null || this.mLCPolSet == null)
        {
            CError tError = new CError();
            tError.moduleName = "FamilySingleContBL";
            tError.functionName = "fillApprovUWField";
            tError.errorMessage = "请先处理合同和险种信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        this.mLCContSchema.setApproveCode(mGlobalInput.Operator);
        this.mLCContSchema.setApproveDate(mCurrentData);
        this.mLCContSchema.setApproveFlag("9"); // 符合通过
        this.mLCContSchema.setApproveTime(mCurrentTime);

        for (int i = 1; i <= tLCPolSet.size(); i++)
        {
            this.tLCPolSet.get(i).setApproveCode(mGlobalInput.Operator);
            this.tLCPolSet.get(i).setApproveDate(mCurrentData);
            this.tLCPolSet.get(i).setApproveFlag("9"); // 符合通过
            this.tLCPolSet.get(i).setApproveTime(mCurrentTime);
        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            System.out.println("Into FamilySingleContBL.getInputData()...");
            if (this.mInputData == null)
            {
                CError tError = new CError();
                tError.moduleName = "FamilySingleContBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的封装对象为空！";
                System.out.println("程序第97行出错，" + "请检查FamilySingleContBL中的" + "getInputData方法！" + tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (this.mOperate == null)
            {
                CError tError = new CError();
                tError.moduleName = "FamilySingleContBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有传入操作符！";
                System.out.println("程序第169行出错，" + "请检查FamilySingleContBL中的" + "getInputData方法！" + tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName("LCContSchema", 0);

            mContNo = mLCContSchema.getContNo();

            if (mLCContSchema.getPolApplyDate() != null && !"".equals(mLCContSchema.getPolApplyDate()))
            {
                polApplyDate = mLCContSchema.getPolApplyDate();
            }
            else
            {
                polApplyDate = PubFun.getCurrentDate();
            }
            System.out.println("************  PolApplyDate : " + polApplyDate + "************");
            mLCAppntSchema = (LCAppntSchema) mInputData.getObjectByObjectName("LCAppntSchema", 0);

            //mLCInsuredSchema = (LCInsuredSchema) mInputData.getObjectByObjectName("LCInsuredSchema", 0);
            //获取多名被保人
            mLCInsuredSet = (LCInsuredSet) mInputData.getObjectByObjectName("LCInsuredSet", 0);
            //被保人个数
            mInsuredNum = mLCInsuredSet.size();
            //受益人列表
            mLCBnfSet = (LCBnfSet) mInputData.getObjectByObjectName("LCBnfSet", 0);

            LLCRiskDutyWrapSet = (LCRiskDutyWrapSet) mInputData.getObjectByObjectName("LCRiskDutyWrapSet", 0);
            
            if(LLCRiskDutyWrapSet.size()<=0){
                buildError("getInputData", "录入险种信息不完整，请确认是否选择险种信息！");
                return false;
            }

            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
            if (mTransferData == null)
            {
                buildError("getInputData", "传入的地址信息为空！");
                return false;
            }

            this.mInputLCBnfSet = (LCBnfSet) mInputData.getObjectByObjectName("LCBnfSet", 0);

            if (mLCAppntSchema != null && this.mLCAppntSchema.getOccupationCode() != null
                    && !this.mLCAppntSchema.getOccupationCode().equals(""))
            {
                LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                tLDOccupationDB.setOccupationCode(mLCAppntSchema.getOccupationCode());
                if (!tLDOccupationDB.getInfo())
                {
                    String str = "查询投保人职业代码失败！";
                    buildError("getInputData", str);
                    System.out.println("在程序FamilySingleContBL.getInputData() - 1923 : " + str);
                    return false;
                }
                mLCAppntSchema.setOccupationType(tLDOccupationDB.getOccupationType());
            }

            for (int i = 1; i <= mLCInsuredSet.size(); i++)
            {
                mLCInsuredSchema = mLCInsuredSet.get(i);
                if (mLCInsuredSchema != null && this.mLCInsuredSchema.getOccupationCode() != null
                        && !this.mLCInsuredSchema.getOccupationCode().equals(""))
                {
                    LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                    tLDOccupationDB.setOccupationCode(mLCInsuredSchema.getOccupationCode());
                    if (!tLDOccupationDB.getInfo())
                    {
                        String str = "查询被保人职业代码失败！";
                        buildError("getInputData", str);
                        System.out.println("在程序FamilySingleContBL.getInputData() - 1923 : " + str);
                        return false;
                    }
                    mLCInsuredSchema.setOccupationType(tLDOccupationDB.getOccupationType());
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("获取数据失败，具体原因是：" + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        //        if (mLCContSchema.getCValiDate() == null || mLCContSchema.getCValiDate().equals(""))
        //        {
        //            mErrors.addOneError("请录入生效日期");
        //            return false;
        //        }

        System.out.println("Into FamilySingleContBL.checkData()...");
        /** 开始校验前台传入的数据 */
        /** 被保人与投保人是 */
        if (StrTool.cTrim(this.mOperate).equals("INSERT||MAIN"))
        {

            if (StrTool.cTrim(this.mLCAppntSchema.getAppntName()).equals(""))
            {
                buildError("checkData", "没有录入投保人姓名！");
                return false;
            }

        }
        if (!StrTool.cTrim(this.mOperate).equals("INSERT||MAIN"))
        {
            if (StrTool.cTrim(this.mLCContSchema.getContNo()).equals(""))
            {
                buildError("checkData", "执行删除或修改失败,原因是没有传入合同号码,请确认合同是否保存！");
                return false;
            }
        }

        /** 增加校验 */
        if (mLCContSchema.getAppFlag() != null && mLCContSchema.getAppFlag().equals("1"))
        {
            buildError("checkData", "保单已签单完成,不能进行任何后续操作！");
            return false;
        }
        return true;
    }

    /**
     * dealRisk
     *
     * @return boolean
     */
    private boolean dealRisk()
    {

        if (mLCInsuredSet != null && mLCInsuredSet.size() > 0)
        {

            for (int i = 1; i <= mLCInsuredSet.size(); i++)
            {

                mLCInsuredSchema = mLCInsuredSet.get(i);

                //              处理套餐信息
                if (!dealWrap())
                {
                    buildError("dealRisk", "处理套餐信息失败！");
                    return false;
                }
                //处理险种的投保人信息
                for (int j = 1; j <= mLCPolSet.size(); j++)
                {
                    this.mLCPolSet.get(j).setAppntName(this.mLCAppntSchema.getAppntName());
                    this.mLCPolSet.get(j).setAppntNo(this.mLCAppntSchema.getAppntNo());
                }
                //处理险种的被保人信息
                for (int k = 1; k <= mLCPolSet.size(); k++)
                {
                    this.mLCPolSet.get(k).setInsuredBirthday(mLCInsuredSchema.getBirthday());
                    this.mLCPolSet.get(k).setInsuredName(mLCInsuredSchema.getName());
                    this.mLCPolSet.get(k).setInsuredNo(mLCInsuredSchema.getInsuredNo());
                    System.out.println("InsuredNo===================================="
                            + mLCInsuredSchema.getInsuredNo());
                    this.mLCPolSet.get(k).setInsuredSex(mLCInsuredSchema.getSex());
                }

                for (int l = 1; l <= this.mLCRiskDutyWrapSet.size(); l++)
                {
                    mLCRiskDutyWrapSet.get(l).setInsuredNo(mLCInsuredSchema.getInsuredNo());

                    System.out.println("InsuredNo--------" + mLCRiskDutyWrapSet.get(l).getInsuredNo());
                    if ("FeeRate".equals(mLCRiskDutyWrapSet.get(l).getCalFactor())
                            && "2".equals(mLCRiskDutyWrapSet.get(l).getCalFactorType()))
                    {
                        //              每个保单统一一个折扣
                        FeeRate = mLCRiskDutyWrapSet.get(l).getCalFactorValue();
                    }

                    //添加到汇总列表
                    tLCRiskDutyWrapSet.add(mLCRiskDutyWrapSet.get(l));
                }

                /** 处理险种信息，主要是取出LMRiskApp中的险种信息，放到LCPol表中 */
                System.out.println("开始处理险种信息");
                System.out.println("本保单有" + this.mLCPolSet.size() + "个险种");
                //部分信息
                LLCDutySet = new LCDutySet();
                LLCPremSet = new LCPremSet();
                LLCGetSet = new LCGetSet();
                CalBL tCalBL;
                for (int a = 1; a <= this.mLCPolSet.size(); a++)
                {
                    /**
                     * 针对每一个险种的信息进行的操作 : 1、首先生成PolNo 2、将一般险种信息封装。
                     * 3、处理责任信息，把责任信息封装后传入CalBL中进行保费计算。
                     * 4、获取封装信息，回填LCPol，LCCont的保费保额，缴至日期，生效日期，失效日期，责任终止日期
                     */

                    LCPolSchema tLCPolSchema = this.mLCPolSet.get(a);
                    if (!dealLCPol(tLCPolSchema, a))
                    {
                        return false;
                    }
                    if (!CheckTBField(tLCPolSchema, "INSERT"))
                    {
                        return false;
                    }
                    if (!checkInsuredAge(tLCPolSchema))
                    {
                        return false;
                    }
                    String mRiskCode = mLCPolSet.get(a).getRiskCode();
                    /** 封装责任信息 */
                    LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
                    tLMRiskDutyDB.setRiskCode(mRiskCode);
                    LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
                    // System.out.println("险种数：" + mLCPolSet.size());
                    LCDutySet tLCDutySet = getDuty(tLMRiskDutySet, mLCPolSet.get(a));
                    // System.out.println("责任个数：" + tLCDutySet.size());
                    if (tLCDutySet == null || tLCDutySet.size() <= 0)
                    {
                        return false;
                    }
                    System.out.println("测试档次才决定；送风机司法苏联法扫雷；冬季" + tLCDutySet.get(1).getMult());
                    System.out.println("责任准备完毕");
                    /** 责任信息封装完毕 */
                    if (FeeRate != null && !"".equals(FeeRate))
                    {
                        TransferData FTransferData = new TransferData();
                        FTransferData.setNameAndValue("FeeRate", FeeRate);
                        tCalBL = new CalBL(tLCPolSchema, tLCDutySet, null, FTransferData);
                    }
                    else
                    {
                        tCalBL = new CalBL(tLCPolSchema, tLCDutySet, null, null);
                    }
                    System.out.println("lcpol.insuredno+++++" + tLCPolSchema.getInsuredNo());

                    tCalBL.setRiskWrapCode(getRiskWrapCode(tLCPolSchema)); //注意
                    if (!tCalBL.calPol())
                    {
                        this.mErrors.copyAllErrors(tCalBL.mErrors);
                        return false;
                    }
                    if (tCalBL.mErrors.needDealError())
                    {
                        this.mErrors.copyAllErrors(tCalBL.mErrors);
                        return false;
                    }
                    tLCPolSchema.setSchema(tCalBL.getLCPol());
                    tLCDutySet = tCalBL.getLCDuty();
                    LCPremSet tLCPremSet = tCalBL.getLCPrem();
                    LCGetSet tLCGetSet = tCalBL.getLCGet();
                    for (int m = 1; m <= tLCDutySet.size(); m++)
                    {
                        System.out.println("验证档次 :" + tLCDutySet.get(m).getMult());
                        tLCDutySet.get(m).setPolNo(this.mPolNo);
                        tLCPolSchema.setGetYear(tLCDutySet.get(m).getGetYear());
                        tLCPolSchema.setGetYearFlag(tLCDutySet.get(m).getGetYearFlag());
                        tLCPolSchema.setPayEndYearFlag(tLCDutySet.get(m).getPayEndYearFlag());
                        tLCPolSchema.setPayEndYear(tLCDutySet.get(m).getPayEndYear());
                        tLCPolSchema.setInsuYearFlag(tLCDutySet.get(m).getInsuYearFlag());
                        tLCPolSchema.setInsuYear(tLCDutySet.get(m).getInsuYear());
                        tLCPolSchema.setMult(tLCDutySet.get(m).getMult());
                    }
                    for (int m = 1; m <= tLCPremSet.size(); m++)
                    {
                        tLCPremSet.get(m).setPolNo(this.mPolNo);
                        tLCPremSet.get(m).setGrpContNo(SysConst.ZERONO);
                        tLCPremSet.get(m).setContNo(this.mContNo);
                        tLCPremSet.get(m).setOperator(this.mGlobalInput.Operator);
                    }
                    for (int m = 1; m <= tLCGetSet.size(); m++)
                    {
                        tLCGetSet.get(m).setPolNo(this.mPolNo);
                        tLCGetSet.get(m).setGrpContNo(SysConst.ZERONO);
                        tLCGetSet.get(m).setContNo(this.mContNo);
                        tLCGetSet.get(m).setOperator(this.mGlobalInput.Operator);
                    }
                    PubFun.fillDefaultField(tLCPremSet);
                    PubFun.fillDefaultField(tLCGetSet);
                    PubFun.fillDefaultField(tLCDutySet);
                    this.LLCDutySet.add(tLCDutySet);
                    this.LLCPremSet.add(tLCPremSet);
                    this.LLCGetSet.add(tLCGetSet);
                }

                //把每个投保人的责任,保费,领取信息汇总
                for (int j = 1; j <= LLCDutySet.size(); j++)
                {
                    //分割费用
                    LCDutySchema tLCDutySchema = new LCDutySchema();
                    tLCDutySchema = LLCDutySet.get(j);
                    if(i==mInsuredNum){//最后一个人
                        tLCDutySchema.setPrem(tLCDutySchema.getPrem()-(Arith.div(tLCDutySchema.getPrem(), mInsuredNum, 2)*(i-1)));
                        tLCDutySchema.setSumPrem(tLCDutySchema.getSumPrem()-(Arith.div(tLCDutySchema.getSumPrem(), mInsuredNum, 2)*(i-1)));
                        tLCDutySchema.setAmnt(tLCDutySchema.getAmnt()-(Arith.div(tLCDutySchema.getAmnt(), mInsuredNum, 2)*(i-1)));
                        tLCDutySchema.setRiskAmnt(tLCDutySchema.getRiskAmnt()-(Arith.div(tLCDutySchema.getRiskAmnt(), mInsuredNum, 2)*(i-1)));
                    }else{
                        tLCDutySchema.setPrem(Arith.div(tLCDutySchema.getPrem(), mInsuredNum, 2));
                        tLCDutySchema.setSumPrem(Arith.div(tLCDutySchema.getSumPrem(), mInsuredNum, 2));
                        tLCDutySchema.setAmnt(Arith.div(tLCDutySchema.getAmnt(), mInsuredNum, 2));
                        tLCDutySchema.setRiskAmnt(Arith.div(tLCDutySchema.getRiskAmnt(), mInsuredNum, 2));
                    }
                    
                    
                    mLCDutySet.add(tLCDutySchema);
                }

                for (int j = 1; j <= LLCPremSet.size(); j++)
                {
                    //                  分割费用
                    LCPremSchema tLCPremSchema = new LCPremSchema();
                    tLCPremSchema = LLCPremSet.get(j);
                    if(i==mInsuredNum){//最后一个人
                        tLCPremSchema.setPrem(tLCPremSchema.getPrem()-(Arith.div(tLCPremSchema.getPrem(), mInsuredNum, 2)*(i-1)));
                        tLCPremSchema.setSumPrem(tLCPremSchema.getSumPrem()-(Arith.div(tLCPremSchema.getSumPrem(), mInsuredNum, 2)*(i-1)));
                    }else{
                        tLCPremSchema.setPrem(Arith.div(tLCPremSchema.getPrem(), mInsuredNum, 2));
                        tLCPremSchema.setSumPrem(Arith.div(tLCPremSchema.getSumPrem(), mInsuredNum, 2));
                    }
                    mLCPremSet.add(tLCPremSchema);
                }

                for (int j = 1; j <= LLCGetSet.size(); j++)
                {
                    //                  分割费用
                    LCGetSchema tLCGetSchema = new LCGetSchema();
                    tLCGetSchema = LLCGetSet.get(j);
                    if(i==mInsuredNum){//最后一个人
                        tLCGetSchema.setActuGet(tLCGetSchema.getActuGet()-(Arith.div(tLCGetSchema.getActuGet(), mInsuredNum, 2)*(i-1)));
                    }else{
                        tLCGetSchema.setActuGet(Arith.div(tLCGetSchema.getActuGet(), mInsuredNum, 2));
                    }
                    mLCGetSet.add(tLCGetSchema);
                }

                //把每个人的险种信息汇总
                for (int j = 1; j <= mLCPolSet.size(); j++)
                {
                    //                  分割费用
                    LCPolSchema tLCPolSchema = new LCPolSchema();
                    tLCPolSchema = mLCPolSet.get(j);
                    if(i==mInsuredNum){//最后一个人
                        tLCPolSchema.setPrem(tLCPolSchema.getPrem()-(Arith.div(tLCPolSchema.getPrem(), mInsuredNum, 2)*(mInsuredNum-1)));
                        tLCPolSchema.setSumPrem(tLCPolSchema.getSumPrem()-(Arith.div(tLCPolSchema.getSumPrem(), mInsuredNum, 2)*(mInsuredNum-1)));
                        tLCPolSchema.setAmnt(tLCPolSchema.getAmnt()-(Arith.div(tLCPolSchema.getAmnt(), mInsuredNum, 2)*(mInsuredNum-1)));
                        tLCPolSchema.setRiskAmnt(tLCPolSchema.getRiskAmnt()-(Arith.div(tLCPolSchema.getRiskAmnt(), mInsuredNum, 2)*(mInsuredNum-1)));
                    }else{
                        tLCPolSchema.setPrem(Arith.div(tLCPolSchema.getPrem(), mInsuredNum, 2));
                        tLCPolSchema.setSumPrem(Arith.div(tLCPolSchema.getSumPrem(), mInsuredNum, 2));
                        tLCPolSchema.setAmnt(Arith.div(tLCPolSchema.getAmnt(), mInsuredNum, 2));
                        tLCPolSchema.setRiskAmnt(Arith.div(tLCPolSchema.getRiskAmnt(), mInsuredNum, 2));
                    }
                    
                   
                    tLCPolSet.add(tLCPolSchema);
                }

                //重置lcpolset
                mLCPolSet.clear();
                //              重置lcriskdutywrapset
                mLCRiskDutyWrapSet.clear();
                mLDRiskDutyWrapSet.clear();
            }

        }
        else
        {
            buildError("dealRisk", "获取被保人信息失败！");
            return false;
        }

        //        /** 完成保存险种之后校验受益人是否保存成功 */
        //        if (this.mLCBnfSchema != null && this.mLCBnfSchema.getPolNo() == null)
        //        {
        //            String str = "没有存在身故受益人的险种,不需要录入身故受益人!";
        //            buildError("dealRisk", str);
        //            return false;
        //        }
        /** 将保费维护到LCCont表中 */
        for (int i = 1; i <= this.tLCPolSet.size(); i++)
        {
            sumPrem += this.tLCPolSet.get(i).getPrem();
            sumAmnt += this.tLCPolSet.get(i).getAmnt();
            sumMult += this.tLCPolSet.get(i).getMult();
        }
        this.mLCContSchema.setPrem(sumPrem);
        this.mLCContSchema.setAmnt(sumAmnt);
        this.mLCContSchema.setMult(sumMult);
        if (mLCContSchema.getCInValiDate() == null)
        {
            Date tCinValidate = null;
            for (int i = 1; i <= this.tLCPolSet.size(); i++)
            {
                Date lcpolDate = (new FDate()).getDate(tLCPolSet.get(i).getEndDate());
                if (tCinValidate == null || tCinValidate.before(lcpolDate))
                {
                    tCinValidate = lcpolDate;
                }
            }
            this.mLCContSchema.setCInValiDate(tCinValidate);
        }
        
        

        return true;
    }

    /**
     * getDuty
     *
     * @param tLMRiskDutySet
     *            LMRiskDutySet
     * @param tLCPolSchema
     *            LCPolSchema
     * @return LCDutySet
     */
    private LCDutySet getDuty(LMRiskDutySet tLMRiskDutySet, LCPolSchema tLCPolSchema)
    {
        System.out.println(tLMRiskDutySet.get(1).getDutyCode());
        LCDutySet tLCDutySet = new LCDutySet();
        HashMap tDutyMap = null;
        if (mLDRiskDutyWrapSet != null && mLDRiskDutyWrapSet.size() > 0)
        {
            tDutyMap = new HashMap();
            for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++)
            {
                tDutyMap.put(mLDRiskDutyWrapSet.get(i).getDutyCode(), "1");
            }
        }
        boolean tFlag = false;
        for (int k = 1; k <= mLDRiskDutyWrapSet.size(); k++)
        {
            if (mLDRiskDutyWrapSet.get(k).getRiskCode().trim().equals(tLMRiskDutySet.get(1).getRiskCode()))
            {
                tFlag = true;
                break;
            }
        }

        for (int i = 1; i <= tLMRiskDutySet.size(); i++)
        {
            /** 如果是套餐类险种，并且制定责任 */

            if (tDutyMap != null && tFlag)
            {
                System.out.println((String) tDutyMap.get(tLMRiskDutySet.get(i).getDutyCode()));
                String chk = (String) tDutyMap.get(tLMRiskDutySet.get(i).getDutyCode());
                if (chk == null)
                {
                    continue;
                }
            }
            LCDutySchema tLCDutySchema = new LCDutySchema();
            /** 查询责任描述 */
            LMDutyDB tLMDutyDB = new LMDutyDB();
            tLMDutyDB.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
            if (!tLMDutyDB.getInfo())
            {
                buildError("getDuty", "查询险种责任出错！");
                return null;
            }
            tLCDutySchema.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
            /** 境外救援是旅行次数 */
            tLCDutySchema.setStandbyFlag1(this.mLCContSchema.getDegreeType());
            /** 保额 */
            tLCDutySchema.setAmnt(tLCPolSchema.getAmnt());
            /** 保费 */
            tLCDutySchema.setMult(tLCPolSchema.getMult());
            /** 份数 */
            tLCDutySchema.setCopys(tLCPolSchema.getCopys());
            /** 保费 */
            tLCDutySchema.setPrem(tLCPolSchema.getPrem());
            /** 保险期间 */
            tLCDutySchema.setPayIntv(tLCPolSchema.getPayIntv());
            /** 缴费频次 */
            System.out.println("hsafhkdlashfdioahfkdalsfh " + mLCContSchema.getCInValiDate());
            System.out.println("baoxianqijian shi " + tLCPolSchema.getInsuYear());
            System.out.println(" needInsuYear : " + needInsuYear);
            if (needInsuYear)
            {
                int insuYear = getInsuYear(mLCContSchema.getCValiDate(), mLCContSchema.getCInValiDate(), tLMDutyDB
                        .getInsuYearFlag());

                tLCDutySchema.setInsuYear(insuYear);
                tLCDutySchema.setInsuYearFlag(tLMDutyDB.getInsuYearFlag());
                tLCDutySet.add(tLCDutySchema);
            }
            else
            {
                tLCDutySchema.setInsuYear(tLCPolSchema.getInsuYear());
                tLCDutySchema.setInsuYearFlag(tLMDutyDB.getInsuYearFlag());
                tLCDutySet.add(tLCDutySchema);
            }
            /** 当描述指定责任的要素和要素值得情况下，以描述为主 */
            String tWrapCode = null;
            for (int m = 1; m <= this.mLDRiskDutyWrapSet.size(); m++)
            {
                LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(m);
                String tRiskCode = tLCPolSchema.getRiskCode();
                String tDutyCode = tLCDutySchema.getDutyCode();

                if (tLDRiskDutyWrapSchema.getRiskCode().equals(tRiskCode)
                        && tLDRiskDutyWrapSchema.getDutyCode().equals(tDutyCode))
                {
                    if (tWrapCode == null)
                    {
                        tWrapCode = tLDRiskDutyWrapSchema.getRiskWrapCode();
                    }
                    else if (!tWrapCode.equals(tLDRiskDutyWrapSchema.getRiskWrapCode()))
                    {
                        String str = "系统选择两个套餐中有相同的险种，不允许出现单个被保人拥有相同险种的情况!";
                        buildError("getDuty", str);
                        log.debug(str);
                        return null;
                    }
                    // /** 此处应该判断要素的存放方式，如果是需要计算的应是用计算引擎计算，
                    // * 如果是直接取值的应直接取值，但由于时间紧张，咱不支持套餐责任要素的
                    // * 多种计算形式 */
                    if (tLDRiskDutyWrapSchema.getCalFactorType().equals("1"))
                    {
                        /** 直接取值 */
                        tLCDutySchema.setV(mLCRiskDutyWrapSet.get(m).getCalFactor(), tLDRiskDutyWrapSchema
                                .getCalFactorValue());
                    }
                    else if (tLDRiskDutyWrapSchema.getCalFactorType().equals("2"))
                    {
                        /** 准备要素 calbase */
                        PubCalculator tCal = new PubCalculator();
                        tCal.addBasicFactor("RiskCode", tRiskCode);
                        for (int n = 1; n <= mLCRiskDutyWrapSet.size(); n++)
                        {
                            if (mLCRiskDutyWrapSet.get(n).getDutyCode().equals(tLCDutySchema.getDutyCode()))
                            {
                                tCal.addBasicFactor(mLCRiskDutyWrapSet.get(n).getCalFactor(), mLCRiskDutyWrapSet.get(n)
                                        .getCalFactorValue());
                                System.out.println("********************");
                                System.out.println(mLCRiskDutyWrapSet.get(n).getCalFactor() + ":"
                                        + mLCRiskDutyWrapSet.get(n).getCalFactorValue());
                                System.out.println("********************");
                            }
                        }
                        mLCRiskDutyWrapSet.get(m).setCalSql(tLDRiskDutyWrapSchema.getCalSql());
                        tCal.setCalSql(mLCRiskDutyWrapSet.get(m).getCalSql());
                        System.out.println(mLCRiskDutyWrapSet.get(m).getCalSql());

                        /** 计算 */
                        System.out.println("rrrrrrrr" + mLCRiskDutyWrapSet.get(m));
                        String result = tCal.calculate();
                        if (result == null)
                        {
                            log.error("套餐要素计算失败！");
                        }
                        tLCDutySchema.setV(mLCRiskDutyWrapSet.get(m).getCalFactor(), result);
                        if (mLCRiskDutyWrapSet.get(m).getCalFactor().equals("FeeRate"))
                        {
                            tLCDutySchema.setV("FreeRate", result);
                        }
                    }
                }
                System.out.println("查看保额" + tLCDutySchema.getAmnt());
                System.out.println("查看保费" + tLCDutySchema.getPrem());
                System.out.println("查看折扣" + tLCDutySchema.getFreeRate());
                System.out.println("查看缴费期间标记 " + tLCDutySchema.getPayEndYearFlag());
            }

            //若不录入给付比例，则系统默认为1
            if (Math.abs(0 - tLCDutySchema.getGetRate()) < 0.00001)
            {
                tLCDutySchema.setGetRate(1);
            }
        }
        return tLCDutySet;
    }

    /**
     * getInsuYear
     *
     * @param tCValiDate
     *            String
     * @param tCInValiDate
     *            String
     * @param tInsuYearFlag
     *            String
     * @return int
     */
    private int getInsuYear(String tCValiDate, String tCInValiDate, String tInsuYearFlag)
    {
        return PubFun.calInterval2(tCValiDate, tCInValiDate, tInsuYearFlag);
    }

    /**
     * dealLCPol
     *
     * @return boolean
     * @param tLCPolSchema
     *            LCPolSchema
     * @param seq
     *            int
     */
    private boolean dealLCPol(LCPolSchema tLCPolSchema, int seq)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        LMRiskDB tLMRiskDB = new LMRiskDB();
        mPolNo = PubFun1.CreateMaxNo("ProposalNo", PubFun.getNoLimit(mLCContSchema.getManageCom()));

        if (StrTool.cTrim(this.mPolNo).equals(""))
        {
            buildError("dealRisk", "生成险种号码错误！");
            return false;
        }
        System.out.println("生成的险种号码是 ：" + this.mPolNo);
        System.out.println("险种是" + tLCPolSchema.getRiskCode());
        tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
        if (!tLMRiskAppDB.getInfo() || !tLMRiskDB.getInfo())
        {
            buildError("dealRisk", "查询险种信息失败！");
            return false;
        }

        tLCPolSchema.setPolNo(this.mPolNo);
        tLCPolSchema.setProposalNo(this.mPolNo);
        tLCPolSchema.setContNo(this.mContNo);
        tLCPolSchema.setPrtNo(this.mLCContSchema.getPrtNo());
        tLCPolSchema.setGrpContNo(SysConst.ZERONO);
        tLCPolSchema.setGrpPolNo(SysConst.ZERONO);
        tLCPolSchema.setContType("1"); // 个单
        tLCPolSchema.setPolTypeFlag("0");
        tLCPolSchema.setMainPolNo(this.mPolNo);
        tLCPolSchema.setProposalContNo(this.mContNo);
        tLCPolSchema.setKindCode(tLMRiskAppDB.getKindCode());
        tLCPolSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
        tLCPolSchema.setManageCom(this.mLCContSchema.getManageCom());
        tLCPolSchema.setAgentCom(this.mLCContSchema.getAgentCom());
        tLCPolSchema.setAgentType(this.mLCContSchema.getAgentType());
        tLCPolSchema.setAgentCode(this.mLCContSchema.getAgentCode());
        tLCPolSchema.setAgentGroup(this.mLCContSchema.getAgentGroup());
        tLCPolSchema.setSaleChnl(this.mLCContSchema.getSaleChnl());
        tLCPolSchema.setSaleChnlDetail(this.mLCContSchema.getSaleChnlDetail());
        tLCPolSchema.setCValiDate(this.mLCContSchema.getCValiDate());
        tLCPolSchema.setAppntNo(this.mLCContSchema.getAppntNo());
        tLCPolSchema.setAppntName(this.mLCContSchema.getAppntName());
        // tLCPolSchema.setApproveCode(this.mGlobalInput.Operator);
        System.out.println(this.mLCContSchema.getCValiDate());
        System.out.println(this.mLCInsuredSchema.getBirthday());
        tLCPolSchema.setInsuredAppAge(PubFun.getInsuredAppAge(this.mLCContSchema.getCValiDate(), this.mLCInsuredSchema
                .getBirthday()));
        tLCPolSchema.setInsuredPeoples(1);
        tLCPolSchema.setOccupationType(this.mLCInsuredSchema.getOccupationType());
        tLCPolSchema.setInsuredSex(this.mLCInsuredSchema.getSex());
        tLCPolSchema.setInsuredBirthday(mLCInsuredSchema.getBirthday());
        tLCPolSchema.setInsuredName(mLCInsuredSchema.getName());
        tLCPolSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());

        tLCPolSchema.setAutoPayFlag(tLMRiskAppDB.getAutoPayFlag());
        if (StrTool.cTrim(tLMRiskDB.getRnewFlag()).equals("N"))
        {
            tLCPolSchema.setRnewFlag(-2);
        }
        tLCPolSchema.setComFeeRate(tLMRiskAppDB.getAppInterest());
        tLCPolSchema.setBranchFeeRate(tLMRiskAppDB.getAppPremRate());

        tLCPolSchema.setSpecifyValiDate("1");
        tLCPolSchema.setPayMode(mLCContSchema.getPayMode());
        tLCPolSchema.setPayIntv(mLCContSchema.getPayIntv());

        //        tLCPolSchema.setApproveDate(mCurrentData);
        //        tLCPolSchema.setApproveFlag("9"); // 符合通过
        //        tLCPolSchema.setApproveTime(mCurrentTime);
        tLCPolSchema.setUWFlag("9");

        tLCPolSchema.setUWCode(mGlobalInput.Operator);
        tLCPolSchema.setUWDate(mCurrentData);
        tLCPolSchema.setUWTime(mCurrentTime);

        tLCPolSchema.setAppFlag("0");
        tLCPolSchema.setStateFlag("0");
        tLCPolSchema.setPolApplyDate(polApplyDate);
        tLCPolSchema.setOperator(mGlobalInput.Operator);
        // tLCPolSchema.setProposalContNo(mLCContSchema.getProposalContNo());
        PubFun.fillDefaultField(tLCPolSchema);
        tLCPolSchema.setRiskSeqNo(String.valueOf(seq).length() <= 1 ? "0" + String.valueOf(seq) : String.valueOf(seq));
        System.out.println(mLCBnfSet);
        System.out.println(StrTool.cTrim(tLMRiskAppDB.getBnfFlag()));
        //        if (this.mLCBnfSchema != null && StrTool.cTrim(tLMRiskAppDB.getBnfFlag()).equals("N"))
        //        {
        //            System.out.println("开始处理受益人信息");
        //            if (!dealBnf(tLCPolSchema))
        //            {
        //                return false;
        //            }
        //        }
        //        if (this.mInputLCBnfSet != null && this.mInputLCBnfSet.size() >= 0)
        //        {
        //            System.out.println("开始处理多受益人信息");
        //            if (!dealMulBnf(tLCPolSchema))
        //            {
        //                return false;
        //            }
        //        }

        return true;
    }

    /**
     * 校验字段
     *
     * @return boolean
     * @param tLCPolSchema
     *            LCPolSchema
     * @param operType
     *            String
     */
    private boolean CheckTBField(LCPolSchema tLCPolSchema, String operType)
    {
        // 保单 mLCPolBL mLCGrpPolBL
        // 投保人 mLCAppntBL mLCAppntGrpBL
        // 被保人 mLCInsuredBLSet mLCInsuredBLSetNew
        // 受益人 mLCBnfBLSet mLCBnfBLSetNew
        // 告知信息 mLCCustomerImpartBLSet mLCCustomerImpartBLSetNew
        // 特别约定 mLCSpecBLSet mLCSpecBLSetNew
        // 保费项表 mLCPremBLSet 保存特殊的保费项数据(目前针对磁盘投保，不用计算保费保额类型)
        // 给付项表 mLCGetBLSet
        // 一般的责任信息 mLCDutyBL
        // 责任表 mLCDutyBLSet
        String strMsg = "";
        boolean MsgFlag = false;

        String RiskCode = tLCPolSchema.getRiskCode();

        try
        {
            VData tVData = new VData();
            CheckFieldCom tCheckFieldCom = new CheckFieldCom();

            // 计算要素
            FieldCarrier tFieldCarrier = new FieldCarrier();
            tFieldCarrier.setAppAge(tLCPolSchema.getInsuredAppAge()); // 被保人年龄
            tFieldCarrier.setInsuredName(tLCPolSchema.getInsuredName()); // 被保人姓名
            tFieldCarrier.setSex(tLCPolSchema.getInsuredSex()); // 被保人性别
            tFieldCarrier.setInsuredNo(tLCPolSchema.getInsuredNo()); // 被保人号
            tFieldCarrier.setMult(tLCPolSchema.getMult()); // 投保份数
            tFieldCarrier.setPolNo(tLCPolSchema.getPolNo()); // 投保单号码
            tFieldCarrier.setContNo(tLCPolSchema.getContNo()); // 投保单合同号码
            tFieldCarrier.setMainPolNo(tLCPolSchema.getMainPolNo()); // 主险号码
            tFieldCarrier.setRiskCode(tLCPolSchema.getRiskCode()); // 险种编码
            tFieldCarrier.setCValiDate(tLCPolSchema.getCValiDate()); // 生效日期
            tFieldCarrier.setAmnt(tLCPolSchema.getAmnt()); // 保额
            tFieldCarrier.setInsuredBirthday(tLCPolSchema.getInsuredBirthday()); // 被保人出生日期
            tFieldCarrier.setInsuYear(tLCPolSchema.getInsuYear()); // 保险期间
            tFieldCarrier.setInsuYearFlag(tLCPolSchema.getInsuYearFlag()); // 保险期间单位
            tFieldCarrier.setPayEndYear(tLCPolSchema.getPayEndYear()); // 交费期间
            tFieldCarrier.setPayEndYearFlag(tLCPolSchema.getPayEndYearFlag()); // 交费期间单位
            tFieldCarrier.setPayIntv(tLCPolSchema.getPayIntv()); // 交费方式
            tFieldCarrier.setPayYears(tLCPolSchema.getPayYears()); // 交费年期
            tFieldCarrier.setOccupationType(tLCPolSchema.getOccupationType()); // 被保人职业类别
            tFieldCarrier.setGrpPolNo(tLCPolSchema.getGrpPolNo());
            tFieldCarrier.setEndDate(tLCPolSchema.getEndDate());
            // System.out.println("保单类型为："+mLCPolBL.getPolTypeFlag());
            tFieldCarrier.setPolTypeFlag(tLCPolSchema.getPolTypeFlag());
            tFieldCarrier.setPrem(tLCPolSchema.getPrem());

            tFieldCarrier.setSupplementaryPrem(tLCPolSchema.getSupplementaryPrem());

            if (tLCPolSchema.getStandbyFlag1() != null)
            {
                tFieldCarrier.setStandbyFlag1(tLCPolSchema.getStandbyFlag1());
            }
            if (tLCPolSchema.getStandbyFlag2() != null)
            {
                tFieldCarrier.setStandbyFlag2(tLCPolSchema.getStandbyFlag2());
            }
            if (tLCPolSchema.getStandbyFlag3() != null)
            {
                tFieldCarrier.setStandbyFlag3(tLCPolSchema.getStandbyFlag3());
            }

            //Added By Liuyp  At 2009-12-24 为信保通借款人校验规则而加
            tFieldCarrier.setCardFlag(this.mLCContSchema.getCardFlag());

            tVData.add(tFieldCarrier);

            LMCheckFieldSchema tLMCheckFieldSchema = new LMCheckFieldSchema();
            tLMCheckFieldSchema.setRiskCode(RiskCode);

            tLMCheckFieldSchema.setFieldName("TB" + operType); // 投保
            tVData.add(tLMCheckFieldSchema);
            if (tCheckFieldCom.CheckField(tVData) == false)
            {
                this.mErrors.copyAllErrors(tCheckFieldCom.mErrors);
                return false;
            }
            else
            {
                LMCheckFieldSet mLMCheckFieldSet = tCheckFieldCom.GetCheckFieldSet();
                for (int n = 1; n <= mLMCheckFieldSet.size(); n++)
                {
                    LMCheckFieldSchema tField = mLMCheckFieldSet.get(n);
                    if ((tField.getReturnValiFlag() != null) && tField.getReturnValiFlag().equals("N"))
                    {
                        if ((tField.getMsgFlag() != null) && tField.getMsgFlag().equals("Y"))
                        {
                            MsgFlag = true;
                            strMsg = strMsg + tField.getMsg() + " ; ";

                            break;
                        }
                    }
                }
                if (MsgFlag == true)
                {
                    // @@错误处理
                    String str = "数据有误：" + strMsg;
                    buildError("CheckTBField", str);
                    log.debug(str);
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            String str = "异常错误!" + ex.getMessage();
            buildError("CheckTBField", str);
            log.debug(str, ex);
            return false;
        }

        return true;
    }

    /**
     * 
     * 
     */
    private boolean checkInsuredAge(LCPolSchema tLCPolSchema)
    {
        int insuredAge = tLCPolSchema.getInsuredAppAge();
        String sql = "select count(1) from ldriskwrap where RiskCode = '" + tLCPolSchema.getRiskCode()
                + "' and (MinInsuredAge is null or MinInsuredAge <= " + insuredAge
                + ") and (MaxInsuredAge is null or MaxInsuredAge >= " + insuredAge + ") and riskwrapcode='"+mLCRiskDutyWrapSet.get(1).getRiskWrapCode()+"'";
        String count = new ExeSQL().getOneValue(sql);
        if (count != null && count.equals("1"))
        {
            return true;
        }
        else
        {
            buildError("checkInsuredAge", "被保人年龄不在允许范围内！");
            return false;
        }
    }

    /**
     * 处理套餐信息
     * @return boolean
     */
    private boolean dealWrap()
    {
        mLDRiskDutyWrapSet = new LDRiskDutyWrapSet();
        mLCRiskDutyWrapSet = new LCRiskDutyWrapSet();

        mLCRiskDutyWrapSet.set(LLCRiskDutyWrapSet);

        getWrapPolInfo(); //得到待处理套餐的险种信息

        getLDRiskDutyWrapInfo(); //得到传入险种或责任套餐要素的描述信息

        setDefaultCalFactor(); //从系统获取相关默认要素值

        parseWrapInfoToLCPol(); //将套餐信息解析到险种

        if (mLCRiskDutyWrapSet != null && mLCRiskDutyWrapSet.size() > 0)
        {
            for (int i = 1; i <= mLCRiskDutyWrapSet.size(); i++)
            {
                // 注意，简易险平台中，这个地方可能没有生成ContNo、所以在后面还有对ContNo、PrtNo的赋值
                // 但国际业务中存在
                mLCRiskDutyWrapSet.get(i).setContNo(mLCContSchema.getContNo());
                mLCRiskDutyWrapSet.get(i).setPrtNo(mLCContSchema.getPrtNo());

                String tStrCalSql = mLCRiskDutyWrapSet.get(i).getCalSql();
                if (tStrCalSql != null)
                    tStrCalSql = tStrCalSql.replaceAll("'", "''");
                mLCRiskDutyWrapSet.get(i).setCalSql(tStrCalSql); // CalSql无用，不需要赋值
            }
        }

        return true;
    }

    /**
     * 将套餐信息解析到险种
     */
    private void parseWrapInfoToLCPol()
    {
        Reflections ref = new Reflections();
        ref.transFields(mLCRiskDutyWrapSet, mLDRiskDutyWrapSet);

        //      传递年龄要素，根据年龄算费
        HashMap tDutyMap = new HashMap();
        LCRiskDutyWrapSet tempLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
        tempLCRiskDutyWrapSet.add(mLCRiskDutyWrapSet);
        String tInsuredBirthday = mLCInsuredSchema.getBirthday();
        if ("00".equals(mLCInsuredSchema.getRelationToAppnt()))
        {
            if (mLCAppntSchema != null && mLCAppntSchema.getAppntBirthday() != null)
            {
                tInsuredBirthday = mLCAppntSchema.getAppntBirthday();
            }
            else
            {
                String tSQL = "select AppntBirthday from LCAppnt where prtno = '" + mLCInsuredSchema.getPrtNo() + "' ";
                tInsuredBirthday = new ExeSQL().getOneValue(tSQL);
            }
        }
        int tAppAge = PubFun.getInsuredAppAge(mLCContSchema.getCValiDate(), tInsuredBirthday);
        for (int i = 1; i <= tempLCRiskDutyWrapSet.size(); i++)
        {
            String tempDutyCode = tempLCRiskDutyWrapSet.get(i).getDutyCode();
            if (!tDutyMap.containsKey(tempDutyCode))
            {
                LCRiskDutyWrapSchema tempLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
                ref.transFields(tempLCRiskDutyWrapSchema, tempLCRiskDutyWrapSet.get(i));
                tempLCRiskDutyWrapSchema.setCalFactor("AppAge");
                tempLCRiskDutyWrapSchema.setCalFactorType("1");
                tempLCRiskDutyWrapSchema.setCalFactorValue(tAppAge + "");
                tempLCRiskDutyWrapSchema.setCalSql(null);

                mLCRiskDutyWrapSet.add(tempLCRiskDutyWrapSchema);
                tDutyMap.put(tempDutyCode, "");
            }
        }

        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            for (int n = 1; n <= mLCRiskDutyWrapSet.size(); n++)
            {
                LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = mLCRiskDutyWrapSet.get(n);

                //只处理本险种的计算要素
                if (tLCRiskDutyWrapSchema.getRiskCode() != null && !tLCRiskDutyWrapSchema.getRiskCode().equals("")
                        && !tLCRiskDutyWrapSchema.getRiskCode().equals("null")
                        && !tLCRiskDutyWrapSchema.getRiskCode().equals("000000")
                        && !tLCRiskDutyWrapSchema.getRiskCode().equals(mLCPolSet.get(i).getRiskCode()))
                {
                    continue;
                }

                if (tLCRiskDutyWrapSchema.getCalFactor().equals("InsuYear"))
                {
                    needInsuYear = false;
                }
                System.out.println(tLCRiskDutyWrapSchema.getCalFactorType());
                if (tLCRiskDutyWrapSchema.getCalFactorType().equals("1"))
                {
                    /** 直接取值 */
                    mLCPolSet.get(i).setV(tLCRiskDutyWrapSchema.getCalFactor(),
                            tLCRiskDutyWrapSchema.getCalFactorValue());
                }
                else if (tLCRiskDutyWrapSchema.getCalFactorType().equals("2"))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();
                    tCal.addBasicFactor("RiskCode", mLCPolSet.get(i).getRiskCode());
                    for (int m = 1; m <= mLCRiskDutyWrapSet.size(); m++)
                    {
                        //得到对应险种的计算要素
                        if (mLCRiskDutyWrapSet.get(m).getRiskCode().equals(mLCPolSet.get(i).getRiskCode()))
                        {
                            tCal.addBasicFactor(mLCRiskDutyWrapSet.get(m).getCalFactor(), mLCRiskDutyWrapSet.get(m)
                                    .getCalFactorValue());
                        }
                    }
                    /** 计算 */
                    tCal.setCalSql(tLCRiskDutyWrapSchema.getCalSql());
                    System.out.println(tLCRiskDutyWrapSchema.getCalSql());
                    mLCPolSet.get(i).setV(tLCRiskDutyWrapSchema.getCalFactor(), tCal.calculate());
                }
            }
        }
    }

    /**
     * 得到待处理的套餐信息的描述信息
     */
    private void getLDRiskDutyWrapInfo()
    {
        HashSet keys = new HashSet();
        for (int i = 1; i <= mLCPolSet.size(); i++)
        {
            System.out.println(mLCRiskDutyWrapSet.size());
            for (int m = 1; m <= mLCRiskDutyWrapSet.size(); m++)
            {
                LDRiskDutyWrapDB tLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
                tLDRiskDutyWrapDB.setRiskCode(mLCPolSet.get(i).getRiskCode());
                tLDRiskDutyWrapDB.setRiskWrapCode(mLCRiskDutyWrapSet.get(m).getRiskWrapCode());

                if (mLCRiskDutyWrapSet.get(m).getDutyCode() != null
                        && !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("")
                        && !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("null")
                        && !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("000000"))
                {
                    tLDRiskDutyWrapDB.setDutyCode(mLCRiskDutyWrapSet.get(m).getDutyCode());
                }

                tLDRiskDutyWrapDB.setCalFactor(mLCRiskDutyWrapSet.get(m).getCalFactor());
                LDRiskDutyWrapSet tLDRiskDutyWrapSet = tLDRiskDutyWrapDB.query();
                if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() <= 0)
                {
                    continue;
                }

                //下面是为了为避免套餐责任信息的重复
                for (int temp = 1; temp <= tLDRiskDutyWrapSet.size(); temp++)
                {
                    String key = tLDRiskDutyWrapSet.get(temp).getRiskWrapCode()
                            + tLDRiskDutyWrapSet.get(temp).getRiskCode() + tLDRiskDutyWrapSet.get(temp).getDutyCode()
                            + tLDRiskDutyWrapSet.get(temp).getCalFactor();
                    if (!keys.contains(key))
                    {
                        keys.add(key);
                        mLDRiskDutyWrapSet.add(tLDRiskDutyWrapSet.get(temp));
                    }
                }
            }
        }

        //将传入的要素值存储到mLDRiskDutyWrapSet
        for (int i = 1; i <= this.mLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mLDRiskDutyWrapSet.get(i);
            if (!"2".equals(tLDRiskDutyWrapSchema.getCalFactorType()))
            {
                continue;
            }
            for (int m = 1; m <= mLCRiskDutyWrapSet.size(); m++)
            {
                LCRiskDutyWrapSchema tLCRiskdutyWrapSchema = mLCRiskDutyWrapSet.get(m).getSchema();

                //如果传入了责任编码，必须用责任编码和计算要素进行判断
                if (mLCRiskDutyWrapSet.get(m).getDutyCode() != null
                        && !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("")
                        && !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("null")
                        && !mLCRiskDutyWrapSet.get(m).getDutyCode().equals("000000"))
                {
                    if (tLCRiskdutyWrapSchema.getDutyCode().equals(tLDRiskDutyWrapSchema.getDutyCode())
                            && tLCRiskdutyWrapSchema.getCalFactor().equals(tLDRiskDutyWrapSchema.getCalFactor())
                            && tLCRiskdutyWrapSchema.getRiskWrapCode().equals(tLDRiskDutyWrapSchema.getRiskWrapCode()))
                    {
                        tLDRiskDutyWrapSchema.setCalFactorValue(tLCRiskdutyWrapSchema.getCalFactorValue());
                    }
                }
                //否则只用计算要素就可以
                else if (tLCRiskdutyWrapSchema.getCalFactor().equals(tLDRiskDutyWrapSchema.getCalFactor())
                        && tLCRiskdutyWrapSchema.getRiskWrapCode().equals(tLDRiskDutyWrapSchema.getRiskWrapCode()))
                {
                    System.out.println("LDRiskDutyWrap : " + tLDRiskDutyWrapSchema.getCalFactor() + " value : "
                            + tLDRiskDutyWrapSchema.getCalFactorValue());
                    tLDRiskDutyWrapSchema.setCalFactorValue(tLCRiskdutyWrapSchema.getCalFactorValue());
                }
            }
        }
    }

    /**
     * 得到待处理套餐的险种信息
     */
    private void getWrapPolInfo()
    {
        if (mLCPolSet == null)
        {
            mLCPolSet = new LCPolSet();
        }

        //得到本次需要处理的险种信息
        String tWrapCode = "";
        for (int i = 1; i <= this.mLCRiskDutyWrapSet.size(); i++)
        {

            if (tWrapCode.equals(mLCRiskDutyWrapSet.get(i).getRiskWrapCode()))
            {
                continue;
            }
            tWrapCode = mLCRiskDutyWrapSet.get(i).getRiskWrapCode();
            LDRiskWrapDB tLDRiskWrapDB = new LDRiskWrapDB();
            tLDRiskWrapDB.setRiskWrapCode(tWrapCode);
            LDRiskWrapSet tLDRiskWrapSet = tLDRiskWrapDB.query();
            for (int j = 1; j <= tLDRiskWrapSet.size(); j++)
            {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema.setRiskCode(tLDRiskWrapSet.get(j).getRiskCode());
                mLCPolSet.add(tLCPolSchema);
            }

        }
    }

    /**mLCRiskDutyWrapSet.get(i).setRiskCode()
     * 得到险种所属套餐编码
     * @param tLCPolSchema LCPolSchema
     * @return String
     */
    private String getRiskWrapCode(LCPolSchema tLCPolSchema)
    {
        String tRiskWrapCode = "";

        for (int i = 1; i < mLCRiskDutyWrapSet.size(); i++)
        {
            if (mLCRiskDutyWrapSet.get(i).getRiskCode().equals(tLCPolSchema.getRiskCode())
                    && mLCRiskDutyWrapSet.get(i).getInsuredNo().equals(tLCPolSchema.getInsuredNo()))
            {
                return mLCRiskDutyWrapSet.get(i).getRiskWrapCode();
            }
        }

        return tRiskWrapCode;
    }

    /**
     * setDefaultCalFactor
     * 读取险种或责任对应套餐的默认要素
     * @param tLCPolSchema LCPolSchema
     */
    private void setDefaultCalFactor()
    {
        HashSet keys = new HashSet(); //存储每个要素的“套餐编码+险种编码+责任编码+计算要素”，用来进行重复要素的过滤
        for (int i = 1; i <= mLDRiskDutyWrapSet.size(); i++)
        {
            keys.add(mLDRiskDutyWrapSet.get(i).getRiskWrapCode() + mLDRiskDutyWrapSet.get(i).getRiskCode()
                    + mLDRiskDutyWrapSet.get(i).getDutyCode() + mLDRiskDutyWrapSet.get(i).getCalFactor());
        }

        //循环每个套餐要素，这样做是为了避免查询到业务不需要的责任的要素，如国际业务的责任是可选的
        int tWrapCount = mLDRiskDutyWrapSet.size(); //查询到默认套餐后，mLDRiskDutyWrapSet的大小可能会动态增加，此处用tWrapCount保证最少循环
        for (int m = 1; m <= tWrapCount; m++)
        {
            LDRiskDutyWrapDB tLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
            tLDRiskDutyWrapDB.setRiskWrapCode(mLDRiskDutyWrapSet.get(m).getRiskWrapCode());
            tLDRiskDutyWrapDB.setRiskCode(mLDRiskDutyWrapSet.get(m).getRiskCode());
            tLDRiskDutyWrapDB.setDutyCode(mLDRiskDutyWrapSet.get(m).getDutyCode());
            tLDRiskDutyWrapDB.setCalFactorType("1"); //默认要素
            LDRiskDutyWrapSet tLDRiskDutyWrapSet = tLDRiskDutyWrapDB.query();
            if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() <= 0)
            {
                continue;
            }

            for (int temp = 1; temp <= tLDRiskDutyWrapSet.size(); temp++)
            {
                //下面是为了为避免套餐责任信息的重复
                String key = tLDRiskDutyWrapSet.get(temp).getRiskWrapCode()
                        + tLDRiskDutyWrapSet.get(temp).getRiskCode() + tLDRiskDutyWrapSet.get(temp).getDutyCode()
                        + tLDRiskDutyWrapSet.get(temp).getCalFactor();
                if (!keys.contains(key))
                {
                    keys.add(key);
                    mLDRiskDutyWrapSet.add(tLDRiskDutyWrapSet.get(temp));
                }
            }
        }
    }

    /**
     * riskAutoCheck
     *
     * @return boolean
     * @param tLCPolSchema
     *            LCPolSchema
     */
    private boolean riskAutoCheck(LCPolSchema tLCPolSchema)
    {
        Calculator mCalculator = new Calculator();
        LMUWDB tLMUWDB = new LMUWDB();
        LMUWSet tLMUWSet = tLMUWDB.executeQuery("select * from lmuw where riskcode='" + tLCPolSchema.getRiskCode()
                + "' and uwtype like '1%'");
        System.out.println("yyyyyy444444444444yyyy" + tLCPolSchema.getRiskCode());
        System.out.println("tLMUWSet.size() " + tLMUWSet.size());
        if (tLMUWSet.size() <= 0)
        {
            /** 没有自核信息,不需要教研,直接返回 */
            return true;
        }

        for (int m = 1; m <= tLCPolSchema.getFieldCount(); m++)
        {
            mCalculator.addBasicFactor(tLCPolSchema.getFieldName(m), tLCPolSchema.getV(m));
        }
        mCalculator.addBasicFactor("CardFlag", this.mLCContSchema.getCardFlag());

        //--  向自核规则中传入套餐编码  -------------------------------
        mCalculator.addBasicFactor("RiskWrapCode", getRiskWrapCode(tLCPolSchema));
        //--------------------------------------------------------
        //by gzh 20110602 有些责任校验职业编码occupationtype为job，这里给job赋值。
        mCalculator.addBasicFactor("job", mLCInsuredSchema.getOccupationType());

        for (int i = 1; i <= tLMUWSet.size(); i++)
        {
            mCalculator.setCalCode(tLMUWSet.get(i).getCalCode());
            String tStr = mCalculator.calculate();
            System.out.println("这么多的自核规则为什么没有出来呢？？？ " + tStr);
            if (!StrTool.cTrim(tStr).equals("") && Integer.parseInt(tStr) > 0)
            {
                /** 解析报错要素 */

                errorVData.add(parseUWResultFactor(tLMUWSet.get(i).getRemark()));
            }
        }
        return true;
    }

    /*
     * @TODO//规则引擎待修改
     */
    private String parseUWResultFactor(String tSql)
    {
        PubCalculator tPubCalculator = new PubCalculator();
        tPubCalculator.addBasicFactor("ContNo", this.mContNo);
        tPubCalculator.addBasicFactor("AppntNo", this.mAppntNo);
        //tPubCalculator.addBasicFactor("InsuredNo", this.mInsuredNo);
        tPubCalculator.addBasicFactor("PolNo", this.mPolNo);
        String tStr = "";
        String tRemark = tSql;
        String tStr1 = "";
        try
        {
            while (true)
            {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals(""))
                {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                // 替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            buildError("parseUWResultFactor", "解析要素失败！");
            return null;
        }
        if (tSql.trim().equals(""))
        {
            return tRemark;
        }
        return tSql;

    }

    /**
     * 校验是否定额单证
     *
     * @param cardNo
     *            String
     * @return boolean
     */
    private boolean isCertifyD(String cardNo)
    {
        if (cardNo == null || cardNo.length() < 2)
        {
            return false;
        }

        String subCode = cardNo.substring(0, 2);

        LMCertifyDesDB db = new LMCertifyDesDB();
        db.setSubCode(subCode);
        db.setCertifyClass("D"); // 定额单证
        if (db.query().size() > 0)
        {
            return true;
        }

        return false;
    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 出错处理
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FamilySingleContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.err.println("程序报错：" + cError.errorMessage);
    }
}
