package com.sinosoft.lis.familyapp;

import java.io.UnsupportedEncodingException;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.XFYJAppntSet;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;
import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.cbcheck.RecordFlagBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.XFYJAppntDB;

/**
 * <p>
 * Title: LIS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author Yangming
 * @version 6.0
 */
public class FamilydoubleConfirmBL {
	public FamilydoubleConfirmBL() {
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 往工作流引擎中传输数据的容器 */
	private GlobalInput mGlobalInput = new GlobalInput();

	// private VData mIputData = new VData();
	private TransferData mTransferData = new TransferData();

	/** 数据操作字符串 */
	private String mOperater;

	private String mManageCom;

	private String mOperate;

	private String mContType;

	private String mBatchno;
	private Boolean flag = true;

	/**
	 * 传输数据的公共方法
	 *
	 * @param: cInputData 输入的数据
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		// 数据操作业务处理
		if (!dealData()) {
			return false;
		}
//		if (flag) {
		ExeSQL mExeSQL = new ExeSQL();
		mExeSQL.execUpdateSQL("update XFYJAppnt set state='2' where batchno='"
				+ mBatchno + "' and prtno='000000'");
//		} else {
//			ExeSQL mExeSQL = new ExeSQL();
//			mExeSQL.execUpdateSQL("update XFYJAppnt set state='3' where batchno='"
//					+ mBatchno + "' and prtno='000000'");
//			return false;
//		}
		if(!flag){
			return false;
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 * @param cInputData
	 *            VData
	 * @param cOperate
	 *            String
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData, String cOperate) {
		// 从输入数据中得到所有对象
		// 获得全局公共数据
		mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mInputData = cInputData;
		if (mGlobalInput == null) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		// 获得操作员编码
		mOperater = mGlobalInput.Operator;
		if ((mOperater == null) || mOperater.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据Operate失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		// 获得登陆机构编码
		mManageCom = mGlobalInput.ManageCom;
		if ((mManageCom == null) || mManageCom.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
			this.mErrors.addOneError(tError);

			return false;
		}
		mBatchno = (String) mTransferData.getValueByName("BatchNo");
		if ((mBatchno == null) || mBatchno.trim().equals("")) {
			// @@错误处理
			// this.mErrors.copyAllErrors( tLCPolDB.mErrors );
			CError tError = new CError();
			tError.moduleName = "GrpBriefTbWorkFlowBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据BatchNO失败!";
			this.mErrors.addOneError(tError);

			return false;
		}

		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 * @return boolean
	 */
	private boolean dealData() {
		XFYJAppntDB mXfyjAppntDB = new XFYJAppntDB();
		XFYJAppntSet mxAppntSet = mXfyjAppntDB
				.executeQuery("select * from xfyjAppnt where batchno='"
						+ mBatchno + "' and prtno<>'000000' and flag='0'");  

		for (int i = 1; i <= mxAppntSet.size(); i++) {
			XFYJAppntSchema mxAppntSchema = mxAppntSet.get(i);
			dealmission(mxAppntSchema);
			
		}

		return true;
	}

	private void dealmission(XFYJAppntSchema mxAppntSchema) {

		VData tVData = new VData();
		String wFlag = "0000013001";
		TransferData mTransferData = new TransferData();
		ExeSQL eSql1 = new ExeSQL();
		SSRS ssrs1 = eSql1
				.execSQL("select contno,appntno,AppntName,AgentCode from Lccont where prtno='"
						+ mxAppntSchema.getPrtNo() + "'");
		String mContno = ssrs1.GetText(1, 1);
		if(ssrs1.getMaxRow()==0){
			return;
		}

		ExeSQL eSql = new ExeSQL();
		SSRS ssrs = eSql
				.execSQL("select MissionID,SubMissionID from lwmission where 1=1 and lwmission.missionprop1 = '"
						+ mxAppntSchema.getPrtNo() + "'");
		if(ssrs.getMaxRow()==0){
			return;
		}
		mTransferData.setNameAndValue("ContNo", mContno);
		mTransferData.setNameAndValue("PrtNo", mxAppntSchema.getPrtNo());
		mTransferData.setNameAndValue("AppntNo", ssrs1.GetText(1, 2));
		mTransferData.setNameAndValue("AppntName", ssrs1.GetText(1, 3));
		mTransferData.setNameAndValue("AgentCode", ssrs1.GetText(1, 4));
		mTransferData
				.setNameAndValue("ManageCom", mxAppntSchema.getManageCom());
		mTransferData.setNameAndValue("MakeDate", PubFun.getCurrentDate());
		mTransferData.setNameAndValue("Operator", mOperater);
		mTransferData.setNameAndValue("MissionID", ssrs.GetText(1, 1));
		mTransferData.setNameAndValue("SubMissionID", ssrs.GetText(1, 2));
		System.out.println(ssrs.GetText(1, 1));
		System.out.println(ssrs.GetText(1, 2));
		tVData.add(mTransferData);
		tVData.add(mGlobalInput);

		FamilyConfirmUI tTbWorkFlowUI = new FamilyConfirmUI();
		if (!tTbWorkFlowUI.submitData(tVData, wFlag)) {
			String error = tTbWorkFlowUI.mErrors.getFirstError();
			int i = error.indexOf("<br>");
			int j = error.lastIndexOf("<br>");
			error = error.substring(i + 4, j);
			error = error.replaceAll("<br>", ",");
			try {
				if(error.getBytes("utf-8").length>999){
					error=substr(error, 900);
				}
			} catch (UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}			
			System.out.println(error);
			ExeSQL meExeSQL = new ExeSQL();
			String sqlString = "update xfyjlog set loginfo='" + error
					+ "' ,other='确认失败！'  where prtno='"
					+ mxAppntSchema.getPrtNo() + "' and batchno='"+mxAppntSchema.getBatchNo()+"'";
			meExeSQL.execUpdateSQL(sqlString);
			String sqlString1 = "update xfyjappnt set flag='2'" 
					+  "where prtno='"
					+ mxAppntSchema.getPrtNo() + "' and batchno='"+mxAppntSchema.getBatchNo()+"'";
			meExeSQL.execUpdateSQL(sqlString1);
			flag = false;

		} else {
			ExeSQL meExeSQL = new ExeSQL();
			String sqlString = "update xfyjlog set "
					+ " other='确认成功！'  where prtno='"
					+ mxAppntSchema.getPrtNo() + "' and batchno='"+mxAppntSchema.getBatchNo()+"'";
			meExeSQL.execUpdateSQL(sqlString);
			/*
			//双录标识
			VData ttVData = new VData();
			TransferData ttTransferData = new TransferData();
			ttTransferData.setNameAndValue("ContNo", mContno);
			ttTransferData.setNameAndValue("PrtNo", mxAppntSchema.getPrtNo());
			ttVData.add(ttTransferData);
			RecordFlagBL tRecordFlagBL = new RecordFlagBL();
			tRecordFlagBL.submitData(ttVData, "");
			*/
		}

	}

	public static void main(String[] args) {

	}
	/**  
     * 判断传进来的字符串，是否  
     * 大于指定的字节，如果大于递归调用
     * 直到小于指定字节数  
     * @param s  
     *            原始字符串  
     * @param num  
     *            传进来指定字节数  
     * @return String 截取后的字符串  
     */ 
    public static String substr(String s,int num){
        int changdu;
		try {
			changdu = s.getBytes("utf-8").length;
		
        if(changdu > num){
            s = s.substring(0, s.length() - 1);
            s = substr(s,num);
        }} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        return s;
    }
    
}
