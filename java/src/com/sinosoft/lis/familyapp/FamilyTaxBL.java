package com.sinosoft.lis.familyapp;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.vschema.XFYJInsuredSet;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;
import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.db.XFYJInsuredDB;


/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class FamilyTaxBL
{
    public FamilyTaxBL()
    {
    	
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperater;

    private String mManageCom;

    private String mOperate;

    private String mContType;
    
    private String mBatchno;

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        // 数据操作业务处理
        if (!dealData())
        {
            return false;
        }

       

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mInputData = cInputData;
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        mBatchno=(String) mTransferData.getValueByName("BatchNo");
        if ((mBatchno == null) || mBatchno.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据BatchNO失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        
        return true;
    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
    	XFYJAppntSchema mxfyj=new XFYJAppntSchema();
    	mxfyj.setBatchNo(mBatchno);
    	mxfyj.setPrtNo("000000");
    	mxfyj.setMngCom(mManageCom);
    	mxfyj.setManageCom(mManageCom);
    	mxfyj.setMakeDate(PubFun.getCurrentDate());
    	mxfyj.setModifyDate(PubFun.getCurrentDate());
    	mxfyj.setMakeTime(PubFun.getCurrentTime());
    	mxfyj.setModifyTime(PubFun.getCurrentTime());
    	mxfyj.setOperator(mOperater);
    	mxfyj.setState("0");
    	
        //生成申请和同首节点 
        System.out.println("mOperate2 :" + mOperater);
        MMap tMMap = new MMap();		
		tMMap.put(mxfyj, "INSERT");
		
		VData tVData = new VData();
		tVData.add(tMMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "FamilyTaxBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "申请批次号失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

       
        return true;
    }
    public static void main(String[] args) {
//    	 VData tVData = new VData();
//         GlobalInput mGlobalInput = new GlobalInput();
//         TransferData mTransferData = new TransferData();
//         
//         
//         /** 全局变量 */
//         mGlobalInput.Operator = "Admin";
//         mGlobalInput.ComCode = "asd";
//         mGlobalInput.ManageCom = "sdd";
//         /** 传递变量 */
//         //mTransferData.setNameAndValue("MissionID", tMissionID);
//         mTransferData.setNameAndValue("PrtNo", "16170214001");
//         mTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
//         mTransferData.setNameAndValue("ManageCom",mGlobalInput.ManageCom);
//         mTransferData.setNameAndValue("ScanOperator",mGlobalInput.Operator);
//         
//         
//         /**总变量*/
//         tVData.add(mGlobalInput);
//         tVData.add(mTransferData);
//    	ActivityOperator aOperator=new ActivityOperator();
//    	String tProcessID="0000000013";
//    	LWMissionSchema a1=aOperator.CreateStartMission(tProcessID, tVData);
//    	System.out.println(aOperator.mErrors.getFirstError());
//    	System.out.println(a1.getActivityID());
//    	System.out.println(a1.getMissionID());
//    	System.out.println(a1.getActivityStatus());
//    	System.out.println(a1.getMissionProp2());
        CreateExcelList createexcellist = new CreateExcelList("ffff..xls");
        String sql = "select usercode,username,comcode,password,userstate from lduser fetch first 100 rows only with ur";
        String sql1="select contno from lccont fetch first 100 rows only with ur";      
       
        createexcellist.createExcelFile();
        String[] sheetName ={"list","cont"};        
        createexcellist.addSheet(sheetName);
        createexcellist.setTitle(0, new String[]{"1","2","3"});
        createexcellist.setTitle(1,  new String[]{"1","2","3"});
        createexcellist.setData(0, sql);
        createexcellist.setData(1, sql1);
        
        try{
            createexcellist.write("d:\\ffff..xls");
        }catch(Exception e)
        {
        }
    	
    	
	}
    
  

}
