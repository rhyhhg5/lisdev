package com.sinosoft.lis.familyapp;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FamilySingleContUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public FamilySingleContUI()
    {
    }

    public boolean submitData(VData nInputData, String cOperate)
    {
        FamilySingleContBL tFamilySingleContBL = new FamilySingleContBL();
        if (!tFamilySingleContBL.submitData(nInputData, cOperate))
        {
            this.mErrors.copyAllErrors(tFamilySingleContBL.mErrors);
            return false;
        }
        else
        {
            mResult = tFamilySingleContBL.getResult();
        }
        return true;
    }

    /**
     * �������
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }
}
