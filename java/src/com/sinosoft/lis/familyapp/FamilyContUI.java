package com.sinosoft.lis.familyapp;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

public class FamilyContUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public FamilyContUI() {
    }

    public boolean submitData(VData nInputData, String cOperate) {
        FamilyContBL tBriefFamilyContBL = new FamilyContBL();
        if (!tBriefFamilyContBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefFamilyContBL.mErrors);
            return false;
        } else {
            mResult = tBriefFamilyContBL.getResult();
        }
        return true;
    }

    /**
     * �������
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
