package com.sinosoft.lis.familyapp;


import org.jdom.Document;

import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.EngineRuleSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LMUWSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.*;
import com.sinosoft.workflow.tb.NewEngineRuleService;
import com.sinosoft.workflowengine.ActivityOperator;

public class FamilyConfirmBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往前台传输数据的容器 */
    private VData tResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /**工作流引擎 */
    ActivityOperator mActivityOperator = new ActivityOperator();
    
    private LCContSchema mLCContSchema;

    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;
    private String mOperate;
    private String contno;
    /**是否提交标志**/
    private String flag;
    private boolean mFlag = true;

    public FamilyConfirmBL() {
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate)) {
            return false;
        }
        System.out.println("---TbWorkFlowBL getInputData---");
        // 数据操作业务处理
        if(!gzcheck()){
        	
        	return false;
        }
        
        if (!dealData()) {
            return false;
        }
        System.out.println("---TbWorkFlowBL dealData---");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("---TbWorkFlowBL prepareOutputData---");

        if (mFlag) { //如果置相应的标志位，不提交
            //数据提交
            FamilyConfirmBLS tTbWorkFlowBLS = new FamilyConfirmBLS();
            System.out.println("Start TbWorkFlowBL Submit...");

            if (!tTbWorkFlowBLS.submitData(mResult, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tTbWorkFlowBLS.mErrors);

                CError tError = new CError();
                tError.moduleName = "TbWorkFlowBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);

                return false;
            }
        }
        

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate) {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData",
                0);
        mInputData = cInputData;
        if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        mOperate = cOperate;
        if ((mOperate == null) || mOperate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate任务节点编码失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        flag = (String) mTransferData.getValueByName("flag");
        if (flag != null) {
            if (flag.equals("N")) {
                mFlag = false;
            }
        }
       contno =(String)mTransferData.getValueByName("ContNo");  
       if((contno==null)||contno.trim().equals("")){
    	   CError tError = new CError();
           tError.moduleName = "TbWorkFlowBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "前台传输全局公共数据ContNo失败!";
           this.mErrors.addOneError(tError);

           return false; 
    	   
       }
        return true;
    }

    private boolean gzcheck(){
    	
            ExeSQL tExeSQL = new ExeSQL();
            String tSQL = "select code from ldcode where codetype='QYJYRuleButton' with ur";
            String tRuleButton = tExeSQL.getOneValue(tSQL);
            LCContDB mLCContDB = new LCContDB();
            mLCContDB.setContNo(contno);
            LCContSet mLCContSet =mLCContDB.query();
            mLCContSchema =mLCContSet.get(1);
            if (tRuleButton != null && tRuleButton.equals("00")){
                System.out.println("开始调用规则引擎!");           
                try
                {
                	// modify by zxs
    				NewEngineRuleService newEngineRule = new NewEngineRuleService();
    				MMap map = newEngineRule.dealNUData(mLCContSchema, "NewQYJYRuleButton");
    				VData mData = new VData();
    				// modify by zxs
                    //              调用规则引擎  
                    RuleTransfer ruleTransfer = new RuleTransfer();
                    //获得规则引擎返回的校验信息 调用投保规则
                    String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NU", this.mLCContSchema.getContNo(), false);
                    //将校验信息转换为Xml的document对象
                    Document doc = ruleTransfer.stringToDoc(xmlStr);
                    String approved = ruleTransfer.getApproved(doc);
                    if (approved == null)
                    {
                        System.out.println("approved is null执行规则引擎是出错!");
                        CError tError = new CError();
                        tError.moduleName = "UWAutoChkBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "执行规则引擎时出错";
                        this.mErrors.addOneError(tError);
                        return false;
                        // return true;
                    }
                    if ("1".equals(approved))
                    {
                    }
                    else if ("0".equals(approved) || "2".equals(approved))
                    {
                    }
                    else if ("-1".equals(approved))
                    {
                        CError tError = new CError();
                        tError.moduleName = "UWAutoChkBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "规则引擎执行异常";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    else
                    {
                        CError tError = new CError();
                        tError.moduleName = "UWAutoChkBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "规则引擎返回了未知的错误类型";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的投保规则
                    String errorMessage = "<br>";
                    String tempRiskcode = "";
                    String tempInsured = "";

                    if (tLMUWSetPolAllUnpass.size() > 0)
                    {
                        for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++)
                        {
                        	// modify by zxs
    						LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
    						EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
    						engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
    						engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
    						engineRuleSchema.setApproved(approved);
    						engineRuleSchema.setRuleResource("old");
    						engineRuleSchema.setRuleType("NU");
    						engineRuleSchema.setContType("简易保单");
    						engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
    						engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
    						engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
    						engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
    							if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
								engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
							}else{
								engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
							} 
    						engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
    						map.put(engineRuleSchema, "INSERT");
    						// modify by zxs
                            tempRiskcode = "";
                            tempInsured = "";
                            if (tLMUWSetPolAllUnpass.get(m).getRiskName() != null
                                    && !tLMUWSetPolAllUnpass.get(m).getRiskName().equals(""))
                            {
                                tempInsured += "被保人" + tLMUWSetPolAllUnpass.get(m).getRiskName();
                                if (tLMUWSetPolAllUnpass.get(m).getRiskCode() != null
                                        && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
                                        && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals(""))
                                {
                                    tempRiskcode += "，险种代码" + tLMUWSetPolAllUnpass.get(m).getRiskCode() + "，";
                                }
                                else
                                {
                                    tempInsured += "，";
                                }
                            }
                            else
                            {
                                if (tLMUWSetPolAllUnpass.get(m).getRiskCode() != null
                                        && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
                                        && !tLMUWSetPolAllUnpass.get(m).getRiskCode().equals(""))
                                {
                                    tempRiskcode += "险种代码" + tLMUWSetPolAllUnpass.get(m).getRiskCode() + "，";
                                }
                            }
                            errorMessage += tLMUWSetPolAllUnpass.get(m).getUWCode();
                            errorMessage += "：";
                            errorMessage += tempInsured;
                            errorMessage += tempRiskcode;
                            errorMessage += tLMUWSetPolAllUnpass.get(m).getRemark();
                            errorMessage += "<br>";
                        }
                        // modify by zxs
        				mData.add(map);
        				PubSubmit pubSubmit = new PubSubmit();
        				if (!pubSubmit.submitData(mData, "")) {
        					this.mErrors.addOneError(pubSubmit.mErrors.getContent());
        					return false;
        				}
        				// modify by zxs
                        
                        //                   @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UWAutoChkBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = errorMessage;
                        this.mErrors.addOneError(tError);
                        return false;
                    }else{
                    	EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
     					engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
     					engineRuleSchema.setPrtno(mLCContSchema.getPrtNo());
     					engineRuleSchema.setApproved(approved);
     					engineRuleSchema.setRuleResource("old");
     					engineRuleSchema.setRuleType("NU");
     					engineRuleSchema.setContType("简易保单");
     					engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
     					engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
     					map.put(engineRuleSchema, "INSERT");
                    }
                 // modify by zxs
    				mData.add(map);
    				PubSubmit pubSubmit = new PubSubmit();
    				if (!pubSubmit.submitData(mData, "")) {
    					this.mErrors.addOneError(pubSubmit.mErrors.getContent());
    					return false;
    				}
    				// modify by zxs
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    CError tError = new CError();
                    tError.moduleName = "UWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "执行规则引擎时出错";
                    this.mErrors.addOneError(tError);
                    return false;
                } 
            }
    	
    	return true;
    }
    
    
    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
          
        if(mOperate.trim().equals("0000013001")){
            	Execute();
            }else{    
                return false;
            }
          
        return true;
    }

    /**
     * 执行承保工作流待人工核保活动表任务
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean Execute() {
        mResult.clear();
        VData tVData = new VData();
        ActivityOperator tActivityOperator = new ActivityOperator();

        //获得当前工作任务的任务ID
        String tMissionID = (String) mTransferData.getValueByName("MissionID");
        String tSubMissionID = (String) mTransferData.getValueByName(
                "SubMissionID");
       System.out.println("++++++++++++++++++++"+tMissionID+"++++++++++++++++");
       System.out.println("++++++++++++++++++++"+tSubMissionID+"++++++++++++++++");
        if (tMissionID == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "Execute0000000000";
            tError.errorMessage = "前台传输数据TransferData中的必要参数MissionID失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (tSubMissionID == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "Execute0000000000";
            tError.errorMessage = "前台传输数据TransferData中的必要参数SubMissionID失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        try {
            if (!mActivityOperator.ExecuteMission(tMissionID, tSubMissionID,
                                                  mOperate, mInputData)) {
                // @@错误处理
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);

                return false;
            }
            //获得执行承保工作流待人工核保活动表任务的结果
            tVData = mActivityOperator.getResult();
            if (tVData != null) {
                for (int i = 0; i < tVData.size(); i++) {
                    VData tempVData = new VData();
                    tempVData = (VData) tVData.get(i);
                    mResult.add(tempVData);
                }
            }

            //产生执行完承保工作流待人工核保活动表任务后的任务节点
            if (tActivityOperator.CreateNextMission(tMissionID, tSubMissionID,
                    mOperate, mInputData)) {
                
            	VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                if ((tempVData != null) && (tempVData.size() > 0)) {
                    mResult.add(tempVData);
                    tempVData = null;
                }
            } else {
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);

                return false;
            }

            tActivityOperator = new ActivityOperator();
            if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
                                                mOperate, mInputData)) {
                VData tempVData = new VData();
                tempVData = tActivityOperator.getResult();
                if ((tempVData != null) && (tempVData.size() > 0)) {
                    mResult.add(tempVData);
                    tempVData = null;
                }
            } else {
                this.mErrors.copyAllErrors(mActivityOperator.mErrors);

                return false;
            }
        } catch (Exception ex) {
            // @@错误处理

            this.mErrors.copyAllErrors(mActivityOperator.mErrors);

            CError tError = new CError();
            tError.moduleName = "TbWorkFlowBL";
            tError.functionName = "dealData";
            tError.errorMessage = "工作流引擎执行新契约活动表任务出错!";
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }


    /**
     * 准备需要保存的数据
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            MMap tmap = new MMap();
            for (int i = 0; i < mResult.size(); i++) {
                VData tData = new VData();
                tData = (VData) mResult.get(i);
                MMap map = (MMap) tData.getObjectByObjectName("MMap", 0);
                tmap.add(map);
            }
            tResult.add(tmap);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public VData getResult() {
        return tResult;
    }
}
