package com.sinosoft.lis.familyapp;

import java.util.*;

import org.apache.log4j.*;
import org.jdom.Document;

import com.sinosoft.lis.brms.databus.RuleTransfer;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class FamilyContBL
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData;
    private String mOperate;
    private LCContSchema mLCContSchema;
    private GlobalInput mGlobalInput;
    private LDPersonSet mLDPersonSet = new LDPersonSet();
    private LCAddressSet mLCAddressSet = new LCAddressSet();
    private boolean needCreatAppnt = false;
    private LCAppntSchema mLCAppntSchema;
    private String mAppntNo;
    private String mContNo;
    private String mWorkName;
    private String mAddressNo;
    private String mCurrentData = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private MMap map;
    private TransferData mTransferData = new TransferData();
    private LCExtendSchema mLCExtendSchema = new LCExtendSchema();
    private LCContSubSchema mLCContSubSchema = new LCContSubSchema();
    private LCAddressSchema mAppntAddressSchema = new LCAddressSchema();
    private String polApplyDate;
    public FamilyContBL()
    {
    }

    public boolean submitData(VData nInputData, String cOperate)
    {
        
        if (getSubmitMap(nInputData, cOperate) == null)
        {
            return false;
        }

        PubSubmit ps = new PubSubmit();
       
        System.out.println("Into BriefSingleContInputBL.submitData()...");
        System.out.println(mResult);
        if (!ps.submitData(mResult, "INSERT"))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
       System.out.println("--------------------go home-----------------------------------------");
        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        System.out.println("Into BriefSingleContInputBL.getSubmitMap()...");
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        System.out.println("@@完成程序submitData第41行");

        if (!getInputData())
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }
System.out.println("----------------------开始dealdate------------------------------");
        if (!dealData())
        {
            return null;
        }

        
        // 综合开拓处理
        if (!dealAssist())
        {
        	return null;
        }

        if (!prepareOutputData())
        {
            return null;
        }

        return map;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        System.out.println(mLCContSchema.getBankAccNo());
        System.out.println(mLCContSchema.getBankCode());
    	mResult.add(mLCContSchema);
        mResult.add(mLCAppntSchema);
        mResult.add(mLDPersonSet);
        mResult.add(mLCContSubSchema);
        mResult.add(mAppntAddressSchema);
        if (this.mOperate.equals("INSERT||MAIN") || this.mOperate.equals("UPDATE||MAIN"))
        {
            if (map == null)
            {
                map = new MMap();
            }
            map.put(mLCContSchema, "DELETE&INSERT");
            map.put(mLCAppntSchema, "DELETE&INSERT");
            map.put(mLDPersonSet, "INSERT");
            map.put(mLCContSubSchema, "DELETE&INSERT");
            map.put(mAppntAddressSchema, "DELETE&INSERT");
//            if (mLCAddressSet.size() > 0)
//            {
//                for (int i = 1; i <= mLCAddressSet.size(); i++)
//                {
//                    LCAddressSchema tLCAddressSchema = new LCAddressSchema();
//                    tLCAddressSchema = mLCAddressSet.get(i);
//                    map.put(tLCAddressSchema, "DELETE&INSERT");
//                }
//            }

            mResult.add(map);
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Into BriefSingleContInputBL.dealData()..." + mOperate);
      if(mOperate.equals("INSERT||MAIN")){
            if (!insertData())
            {
                return false;
            }
      }else{
    	  
    	  if(!updateData()){
    		  
    		  return false;
    	  }
    	  
      }
        System.out.println("--------------------开始insertdata------------------------");

        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData()
    {
        /** 执行删除和执行插入 */
        if (!deleteData())
        {
            return false;
        }
        if (!insertData())
        {
            return false;
        }
        return true;
    }

    private boolean deleteData()
    {
        if (!checkData())
        {
            return false;
        }
        String wherePart_ContNo = " and ContNo = '" + this.mLCContSchema.getContNo() + "'";
        String wherePart_PrtNo = " and PrtNo = '" + this.mLCContSchema.getPrtNo() + "'";
        if (map == null)
        {
            map = new MMap();
        map.put("delete from lccont where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from lcappnt where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from LCNation where 1=1 " + wherePart_ContNo, "DELETE");
        map.put("delete from LCContSub where 1=1 " + wherePart_PrtNo, "DELETE");
        
    }
        return true;
    }
    private boolean insertData()
    {
        System.out.println("Into BriefSingleContInputBL.insertData()...");
        if (!dealAppnt())
        {
            return false;
        }
        if (!dealCont())
        {
            return false;
        }
   System.out.print("-------开始dealCont---------");
        if(!dealContSub()){
        	return false;
        }
               
        return true;
    }

    private boolean dealCont()
    {
        System.out.println("Into BriefSingleContInputBL.dealCont()...");
            /** 生成号码 */
            System.out.println("ContNo：" + mLCContSchema.getContNo());
            LCContDB tLCContDB = new LCContDB();
            LCContSet tLCContSet = new LCContSet();
            tLCContDB.setPrtNo(mLCContSchema.getPrtNo());
            tLCContSet = tLCContDB.query();
            System.out.println("zhuzhuzhzu++++" + tLCContSet.size());
            if (tLCContSet.size() == 0)
            {
                String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
                mContNo = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
            }
            else
            {
                mContNo = tLCContSet.get(1).getContNo();
            }

            if (StrTool.cTrim(mContNo).equals(""))
            {
                buildError("dealCont", "生成合同号码错误！");
                return false;
            }
            System.out.println("合同号码 " + mContNo);
            /** 保存合同号码 */
            mLCContSchema.setContNo(mContNo);
            if (mLCContSchema.getProposalContNo() == null || mLCContSchema.getProposalContNo().equals(""))
            {
                mLCContSchema.setProposalContNo(mContNo);
            }
            mLCContSchema.setGrpContNo(SysConst.ZERONO);
            /** 添加管理机构 */
            if (!dealAgentAndMangeCom())
            {
                return false;
            }
            this.mLCContSchema.setContType("1"); // 个单
            this.mLCContSchema.setInputOperator(mGlobalInput.Operator);
            if (StrTool.cTrim(mLCContSchema.getInputDate()).equals(""))
            {
                this.mLCContSchema.setInputDate(mCurrentData);
            }
            this.mLCContSchema.setInputTime(mCurrentTime);
            mLCContSchema.setUWFlag("9");
            mLCContSchema.setUWOperator(mGlobalInput.Operator);
            mLCContSchema.setUWDate(mCurrentData);
            mLCContSchema.setUWTime(mCurrentTime);
            mLCContSchema.setFamilyType("F");
            mLCContSchema.setAppFlag("0");
            mLCContSchema.setStateFlag("0");
            mLCContSchema.setPrintCount(0);
            mLCContSchema.setCardFlag("2");
            mLCContSchema.setPolType("0");
            mLCContSchema.setPolApplyDate(polApplyDate);
            mLCContSchema.setCValiDate(polApplyDate);
            mLCContSchema.setOperator(mGlobalInput.Operator);
            mLCContSchema.setSignCom(mGlobalInput.ManageCom);
            PubFun.fillDefaultField(mLCContSchema);
            mLCContSchema.setProposalType("01");
            mLCContSchema.setIntlFlag("0"); 
            mLCContSchema.setPayerType("2"); 
            if(mLCContSchema.getInsuredNo() == null || "".equals(mLCContSchema.getInsuredNo())) {
            	mLCContSchema.setInsuredNo("0");
            }
            if (!fillOtherTable())
            {
                return false;
            }
           System.out.println("------------------------------1123-----------------------------------");
            System.out.println(" LCCont deal..........");
        
        return true;
    }

    /**
     * fillOtherTable
     *
     * @return boolean
     */
    private boolean fillOtherTable()
    {
        this.mLCAppntSchema.setContNo(mContNo);
        this.mLCAppntSchema.setPrtNo(mLCContSchema.getPrtNo());
        this.mLCAppntSchema.setGrpContNo(SysConst.ZERONO);
        return true;
    }

    private boolean dealAgentAndMangeCom()
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mGlobalInput.ManageCom);
        if (!tLDComDB.getInfo())
        {
            buildError("dealAgentAndMangeCom", "查询管理机构失败请确认管理机构是否录入正确！");
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        if(mLCContSchema.getAgentGroup()==null){   
        mLCContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
        }
        if (mLCContSchema.getPayMode() == null)
        {
            buildError("dealAgentAndMangeCom", "没有录入缴费方式！");
            return false;
        }

        return true;
    }


    private boolean dealAppnt()
    {

        System.out.println("开始处理投保客户");
        LDPersonDB tLDPersonDB = new LDPersonDB();
        System.out.println("33333333333333");
        System.out.println(mLCAppntSchema.getAppntNo()+"=====1111111");
        if (!"".equals(mLCAppntSchema.getAppntNo()))
        {
        	
            mAppntNo = mLCAppntSchema.getAppntNo();
            System.out.println("33333333333333"+mAppntNo);
            tLDPersonDB.setCustomerNo(mLCAppntSchema.getAppntNo());
            if (!tLDPersonDB.getInfo()&&mAppntNo!=null)
            {
            //	needCreatAppnt=true;
            	buildError("dealAppnt", "录入投保客户号码但是系统中没有此刻户信息，请确认客户号码是否录入错误！！");
                return false;
            }
            else
            { // 查询到客户信息
                /** 如果是老客户校验客户信息与投保信息是否相同 */
                this.mAppntNo = mLCAppntSchema.getAppntNo();
                if (!checkAppnt(tLDPersonDB.getSchema()))
                {
                    return false;
                }
            }
        }
        else{
        	if(mLCAppntSchema.getAppntSex()!=null&&mLCAppntSchema.getAppntBirthday()!=null&&mLCAppntSchema.getAppntName()!=null
        	&&mLCAppntSchema.getIDType()!=null&&mLCAppntSchema.getIDNo()!=null		
        	)
        	{
        		LDPersonDB mLDPersonDB= new LDPersonDB();
        		mLDPersonDB.setBirthday(mLCAppntSchema.getAppntBirthday());
        		mLDPersonDB.setIDNo(mLCAppntSchema.getIDNo());
        		mLDPersonDB.setIDType(mLCAppntSchema.getIDType());
        		mLDPersonDB.setSex(mLCAppntSchema.getAppntSex());
        		mLDPersonDB.setName(mLCAppntSchema.getAppntName());
        	LDPersonSet mLDPersonSet = mLDPersonDB.query();
        	System.out.println(mLDPersonSet.size());
        		if(mLDPersonSet.size()==0){
        		
        		needCreatAppnt=true;
        	}else{
        		
        		//mAppntNo=tLDPersonDB.getCustomerNo();
        		mAppntNo=mLDPersonSet.get(1).getCustomerNo();
        		mLCAppntSchema.setAppntNo(mAppntNo);
        		mAppntAddressSchema.setCustomerNo(mAppntNo);
        		System.out.println(mAppntNo);
        	
        	}
        	
        	}
        	
        }
        /** 完成上面的操作,如果判断是新客户则生成客户号 */
        if (needCreatAppnt)
        {
            mAppntNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
           System.out.println(mAppntNo);
            mLDPersonSet = new LDPersonSet();
            if (StrTool.cTrim(mAppntNo).equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "FamilyContInputBL";
                tError.functionName = "insertData";
                tError.errorMessage = "生成团体客户号错误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLCAppntSchema.setAppntNo(mAppntNo);

            if (mLCAppntSchema.getIDType() != null && mLCAppntSchema.getIDNo() != null)
            {
                if (mLCAppntSchema.getIDType().equals("0"))
                {
                    String tLCAppntIdNo = mLCAppntSchema.getIDNo().toUpperCase();
                    mLCAppntSchema.setIDNo(tLCAppntIdNo);
                }
            }

            /** 生成客户 */
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            tLDPersonSchema.setCustomerNo(mAppntNo);
            tLDPersonSchema.setName(mLCAppntSchema.getAppntName());
            tLDPersonSchema.setSex(mLCAppntSchema.getAppntSex());
            tLDPersonSchema.setBirthday(mLCAppntSchema.getAppntBirthday());
            tLDPersonSchema.setIDType(mLCAppntSchema.getIDType());
            tLDPersonSchema.setIDNo(mLCAppntSchema.getIDNo());
            tLDPersonSchema.setEnglishName(mLCAppntSchema.getEnglishName());
            tLDPersonSchema.setOperator(mGlobalInput.Operator);
            tLDPersonSchema.setGrpName(mWorkName);
            tLDPersonSchema.setNativePlace(mLCAppntSchema.getNativePlace());
            tLDPersonSchema.setNativeCity(mLCAppntSchema.getNativeCity());
            mAppntAddressSchema.setCustomerNo(mAppntNo);
            PubFun.fillDefaultField(tLDPersonSchema);

            //保证客户信息数据的实时性
            //this.mLDPersonSet.add(tLDPersonSchema);
            LDPersonDB mLDPersonDB = new LDPersonDB();
            mLDPersonDB.setSchema(tLDPersonSchema);
            if (!mLDPersonDB.insert())
            {
                //错误处理
                System.out.println("FamilyContBL中，插入投保人信息错误！");
                CError tError = new CError();
                tError.moduleName = "BriefSingleContInputBL";
                tError.functionName = "dealInsured";
                tError.errorMessage = "保存客户信息错误,请重新录入客户信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        /** 封装Makedate等信息 */
        PubFun.fillDefaultField(mLCAppntSchema);
        mLCAppntSchema.setOperator(mGlobalInput.Operator);
        mLCAppntSchema.setManageCom(mGlobalInput.ManageCom);
        LCAddressDB tLCAddressDB = new LCAddressDB();
//        tLCAddressDB.setCustomerNo(mLCContSchema.get);
//        tLCAddressDB.setAddressNo(mInsuredAddressSchema.getAddressNo());
        
        ExeSQL tExeSQL = new ExeSQL();
        mAddressNo = tExeSQL
        .getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
                + mAppntNo + "'");
        if (StrTool.cTrim(mAddressNo).equals(""))
        {
            buildError("checkAppntAddress", "生成地址代码错误！");
            return false;
        }
        mLCAppntSchema.setAddressNo(mAddressNo);
        if (!dealAppntToCont())
        {
            return false;
        }

        return true;
    }

    private boolean dealAppntToCont()
    {
        mLCContSchema.setAppntBirthday(mLCAppntSchema.getAppntBirthday());
        mLCContSchema.setAppntIDNo(mLCAppntSchema.getIDNo());
        mLCContSchema.setAppntIDType(mLCAppntSchema.getIDType());
        mLCContSchema.setAppntName(mLCAppntSchema.getAppntName());
        mLCContSchema.setAppntNo(mLCAppntSchema.getAppntNo());
        mLCContSchema.setAppntSex(mLCAppntSchema.getAppntSex());
        mLCContSchema.setFamilyID(mAddressNo);
        
        mAppntAddressSchema.setAddressNo(mAddressNo);
        mAppntAddressSchema.setOperator(mGlobalInput.Operator);
        mAppntAddressSchema.setMakeDate(mCurrentData);
        mAppntAddressSchema.setMakeTime(mCurrentTime);
        mAppntAddressSchema.setModifyDate(mCurrentData);
        mAppntAddressSchema.setModifyTime(mCurrentTime);
        return true;
    }


    /**
     * checkAppnt
     *
     * @return boolean
     * @param tLDPersonSchema
     *            LDPersonSchema
     */
    private boolean checkAppnt(LDPersonSchema tLDPersonSchema)
    {
        if (!StrTool.unicodeToGBK(StrTool.cTrim(this.mLCAppntSchema.getAppntName())).equals(
                StrTool.cTrim(tLDPersonSchema.getName())))
        {
            System.out.println("系统中的客户姓名为 : " + StrTool.cTrim(tLDPersonSchema.getName()));
            buildError("checkAppnt", "投保客户姓名与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntBirthday()).equals(StrTool.cTrim(tLDPersonSchema.getBirthday())))
        {
            buildError("checkAppnt", "投保客户生日与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        /** 性别 */
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntSex()).equals(StrTool.cTrim(tLDPersonSchema.getSex())))
        {
            buildError("checkAppnt", "投保客户性别与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        /** 证件类型 */
        if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).equals(StrTool.cTrim(tLDPersonSchema.getIDType())))
        {
            buildError("checkAppnt", "投保客户证件类型与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }

        if (this.mLCAppntSchema.getIDType() != null && tLDPersonSchema.getIDType() != null)
        {
            if (this.mLCAppntSchema.getIDType().equals("0") && tLDPersonSchema.getIDType().equals("0"))
            {
                if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).toUpperCase().equals(
                        StrTool.cTrim(tLDPersonSchema.getIDNo()).toUpperCase()))
                {
                    buildError("checkAppnt", "投保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
            }
        }
        else
        {
            if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).equals(StrTool.cTrim(tLDPersonSchema.getIDNo())))
            {
                buildError("checkAppnt", "投保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                return false;
            }
        }
        return true;
    }
    private boolean checkData()
    {

        System.out.println("Into BriefSingleContInputBL.checkData()...");
    
        if (StrTool.cTrim(this.mOperate).equals("INSERT||MAIN"))
        {
            if (StrTool.cTrim(this.mLCContSchema.getPrtNo()).equals(""))
            {
                buildError("checkData", "保存合同信息失败,原因是没有录入印刷号码！");
                return false;
            }
            if (StrTool.cTrim(this.mLCAppntSchema.getAppntName()).equals(""))
            {
                buildError("checkData", "没有录入投保人姓名！");
                return false;
            }
        }  
        return true;
    }


    private boolean getInputData()
    {
        try
        {
            System.out.println("Into BriefSingleContInputBL.getInputData()...");
            if (this.mInputData == null)
            {
                CError tError = new CError();
                tError.moduleName = "BriefSingleContInputBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的数据为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (this.mOperate == null)
            {
                CError tError = new CError();
                tError.moduleName = "BriefSingleContInputBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有传入操作符！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName("LCContSchema", 0);
            System.out.println("银行帐号银行帐号是yyyyyyyyyyy:" + mLCContSchema.getBankAccNo());
            System.out.println("名字是"+mLCContSchema.getAccName());
            if (mLCContSchema.getPolApplyDate() != null && !"".equals(mLCContSchema.getPolApplyDate()))
            {
                polApplyDate = mLCContSchema.getPolApplyDate();
            }
            else
            {
                polApplyDate = PubFun.getCurrentDate();
            }
            System.out.println("************  PolApplyDate : " + polApplyDate + "************");
            mLCAppntSchema = (LCAppntSchema) mInputData.getObjectByObjectName("LCAppntSchema", 0);
           System.out.println("555555555555555555555"+mLCAppntSchema.getAppntNo()+"666666666666666666666");
            mLCExtendSchema = (LCExtendSchema) mInputData.getObjectByObjectName("LCExtendSchema", 0);
            
            mLCContSubSchema = (LCContSubSchema) mInputData.getObjectByObjectName("LCContSubSchema", 0); 
            System.out.println(mLCContSubSchema);
            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
            if (mTransferData == null)
            {
                buildError("getInputData", "传入的地址信息为空！");
                return false;
            }

            System.out.println("33333333333333");
            mAppntAddressSchema = (LCAddressSchema) mTransferData.getValueByName("AppntAddress");
           
            mWorkName = (String) mTransferData.getValueByName("WorkName");
            System.out.println("mWorkName的值&&&&&&&&&&&:" + mWorkName);

            if (mLCAppntSchema != null && mLCAppntSchema.getOccupationCode() != null
                    && !this.mLCAppntSchema.getOccupationCode().equals(""))
            {
                LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                tLDOccupationDB.setOccupationCode(mLCAppntSchema.getOccupationCode());
                if (!tLDOccupationDB.getInfo())
                {
                    String str = "查询投保人职业代码失败！";
                    buildError("getInputData", str);
                    return false;
                }
                mLCAppntSchema.setOccupationType(tLDOccupationDB.getOccupationType());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("获取数据失败，具体原因是：" + e.getMessage());
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriefFamilyContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.err.println("程序报错：" + cError.errorMessage);
    }
    
    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap delExtendInfo(LCExtendSchema aLCExtendSchema)
    {
        MMap tMMap = null;
       
        
        
        LCAssistSalechnlBL tAssistSalechnlBL = new LCAssistSalechnlBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tAssistSalechnlBL.submitData(tVData, "");

        if (tMMap == null)
        {
            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "delExtendInfo";
            tError.errorMessage = "综合开拓数据处理失败！";
            this.mErrors.addOneError(tError);
            return null;
        }

        return tMMap;
    }
    
    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap deleteExtendInfo(LCExtendSchema aLCExtendSchema)
    {
        MMap tMMap = null;

        LCAssistSalechnlBL tAssistSalechnlBL = new LCAssistSalechnlBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tAssistSalechnlBL.submitData(tVData, "DELETE");

        return tMMap;
    }
    
    private boolean dealAssist(){
    	if (map == null)
        {
            map = new MMap();
        }
    	if(mOperate.equals("DELETE||MAIN")){
        	
        	MMap tTmpMap = null;
            tTmpMap = deleteExtendInfo(mLCExtendSchema);
            
            if (tTmpMap != null && tTmpMap.size() != 0)
            {
            	map.add(tTmpMap);
            } 
            
        } else if (!"".equals(mLCExtendSchema.getAssistSalechnl())) {
            MMap tTmpMap = null;
            tTmpMap = delExtendInfo(mLCExtendSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        } else {
        	String tSql = "delete from LCExtend where prtno = '"+mLCExtendSchema.getPrtNo()+"'";
        	map.put(tSql, SysConst.DELETE);
        }
    	
    	return true;
    }
    
    private boolean dealContSub()
    {
        mLCContSubSchema.setOperator(mGlobalInput.Operator);
        mLCContSubSchema.setManageCom(mGlobalInput.ManageCom);
        PubFun.fillDefaultField(mLCContSubSchema);
        return true;
    }
}
