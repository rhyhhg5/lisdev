package com.sinosoft.lis.familyapp;

import weblogic.wtc.jatmi.secflds;

import com.f1j.chart.sn;
import com.informix.msg.sblob_en_US;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.CommonBL;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.XFYJAppntSet;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.*;
import com.sinosoft.lis.brieftb.BriefSingleContInputBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.XFYJAppntDB;
import com.sinosoft.lis.db.XFYJInsuredDB;
import com.sinosoft.lis.db.XFYJLogDB;

import examples.newsgroups;



/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class FamilydeleteBL
{
    public FamilydeleteBL()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperater;

    private String mManageCom;

    private String mOperate;

    private String mContType;
    
    private String mBatchno;
    
    private String flag;

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        // 数据操作业务处理
        if (!dealData())
        {
            return false;
        }

        ExeSQL meExeSQL=new ExeSQL();
//        meExeSQL.execUpdateSQL("delete from xfyjappnt where batchno='"+mBatchno+"'");
//        meExeSQL.execUpdateSQL("delete from xfyjlog where batchno='"+mBatchno+"'");
		if("3".equals(flag)){
			meExeSQL.execUpdateSQL("update xfyjappnt set state='0' where batchno='"
					+ mBatchno + "' and prtno='000000'");
			meExeSQL.execUpdateSQL("delete from xfyjlog where batchno='"+mBatchno+"'");
		}
		ExeSQL eSql1=new ExeSQL();
    	SSRS ssrs1=eSql1.execSQL("select 1 from xfyjappnt where batchno='"+mBatchno+"' and prtno<>'000000'");
			
    	if(ssrs1.getMaxRow()==0){
    		meExeSQL.execUpdateSQL("update xfyjappnt set state='0' where batchno='"
					+ mBatchno + "' and prtno='000000'");
    	}else{
    		meExeSQL.execUpdateSQL("update xfyjappnt set state='1' where batchno='"
					+ mBatchno + "' and prtno='000000'");
    		
    	}
		
		

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mInputData = cInputData;
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        mBatchno=(String) mTransferData.getValueByName("BatchNo");
        if ((mBatchno == null) || mBatchno.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "GrpBriefTbWorkFlowBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据BatchNO失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        flag=(String) mTransferData.getValueByName("flag");
        
        return true;
    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        XFYJAppntDB mXfyjAppntDB=new XFYJAppntDB();
        StringBuffer sB=new StringBuffer();
        sB.append("select * from xfyjAppnt where batchno='"+mBatchno+"' and prtno<>'000000'");
        if("1".equals(flag)){
        	sB.append(" and flag='1'");
        }else if("2".equals(flag)){
        	sB.append(" and flag <>'0'");
        	ExeSQL meExeSQL=new ExeSQL();
        	SSRS s1=meExeSQL.execSQL("select prtno from xfyjlog  lg where not exists (select 1 from xfyjappnt where prtno=lg.prtno and batchno=lg.batchno) and batchno='"+mBatchno+"'");
        	if(s1.getMaxRow()==0){
        		
        	}else{
        		for(int i=1;i<=s1.getMaxRow();i++){
        			meExeSQL.execUpdateSQL("delete from xfyjlog where batchno='"+mBatchno+"' and prtno='"+s1.GetText(i, 1)+"'");
        		}
        		
        		
        	}
        	
        	
        }else{
			
		  }
        System.out.println(flag);
        System.out.println(sB.toString());
        XFYJAppntSet mxAppntSet=mXfyjAppntDB.executeQuery(sB.toString()); //sB.toString()
        for(int i=1;i<=mxAppntSet.size();i++){
        	XFYJAppntSchema mxAppntSchema=mxAppntSet.get(i);
        	delete(mxAppntSchema);
	    	 ExeSQL meExeSQL=new ExeSQL();
	         meExeSQL.execUpdateSQL("delete from xfyjinsured where prtno='"+mxAppntSchema.getPrtNo()+"' and other='"+mBatchno+"'");
	         meExeSQL.execUpdateSQL("delete from xfyjappnt where batchno='"+mBatchno+"' and prtno='"+mxAppntSchema.getPrtNo()+"'");
	         meExeSQL.execUpdateSQL("delete from xfyjlog where batchno='"+mBatchno+"' and prtno='"+mxAppntSchema.getPrtNo()+"'");
	         meExeSQL.execUpdateSQL("delete from lwmission where missionprop1='"+mxAppntSchema.getPrtNo()+"'");
        }
       
        return true;
    }
    private void delete(XFYJAppntSchema mxAppntSchema){
    	  
    	CErrors tError = null;  
    	ExeSQL eSql1=new ExeSQL();
    	SSRS ssrs1=eSql1.execSQL("select contno from Lccont where prtno='"
				+mxAppntSchema.getPrtNo()+"'" );
		if(ssrs1.getMaxRow()==0){
			
		}else{
		
    	// 投保单列表
    	String tContNo = ssrs1.GetText(1, 1);
    	String tPrtNo = mxAppntSchema.getPrtNo();
    	String tDeleteReason = "幸福一家错误单删除";
    	System.out.println("tDeleteReason&&&&&&&&&&&&&&&:"+tDeleteReason);
    	boolean flag = false;

    	System.out.println("ready for UWCHECK ContNo: " + tContNo);
    	
    	LCContSet tLCContSet = null;
    	LCContDB tLCContDB = new LCContDB();
    	TransferData tTransferData = new TransferData();
    	if(!StrTool.cTrim(tContNo).equals(""))
    	{     
    		tLCContDB.setContNo(tContNo);   
    		tLCContSet = tLCContDB.query();   
    	}   
    	else   
    	{   
    		LCContSchema mLCContSchema = new LCContSchema();
    		mLCContSchema.setPrtNo(tPrtNo);
    		tLCContSet = new LCContSet();
    		tLCContSet.add(mLCContSchema);
    	}
    	if (tLCContSet == null)
    	{
    		System.out.println("（投）保单号为" + tContNo + "的合同查找失败！");
    		return;
    	}   
    //   System.out.println("（投）保单号为!!!!!!!!1！"); 
    	LCContSchema tLCContSchema = tLCContSet.get(1);
      tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
    	
  
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCContSchema );
		tVData.add( tTransferData );
		tVData.add( mGlobalInput);
		
		// 数据传输
		ContDeleteUI tContDeleteUI = new ContDeleteUI();
		if (tContDeleteUI.submitData(tVData,"DELETE") == false)
		{
			
		}
    		
		
		}
   
      
    	
    }
    public static void main(String[] args) {
    	
    	
	}
    
  

}
