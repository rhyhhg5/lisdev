package com.sinosoft.lis.familyapp;

import org.apache.log4j.Logger;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDOccupationDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LJTempFeeClassBSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LCCustomerImpartDetailSet;
import com.sinosoft.lis.vschema.LCCustomerImpartParamsSet;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LDPersonSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 幸福一家亲
 * @author zjd
 *
 */
public class FamilySingleContInputBL
{

    /** 报错对象 */
    public CErrors mErrors = new CErrors();

    /** 操作完成保存结果 */
    private VData mResult = new VData();

    /** 前台传入的封装对象 */
    private VData mInputData;

    /** 操作符 */
    private String mOperate;

    /** 客户登陆信息 */
    private GlobalInput mGlobalInput;

    /** 被保险人信息 */
    private LCInsuredSchema mLCInsuredSchema;

    /** 被保险人信息 */
    private LCInsuredSchema tLCInsuredSchema;

    /** 保单信息 */
    private LCContSchema mLCContSchema;

    /** 投保人信息 */
    private LCAppntSchema mLCAppntSchema;

    /** 客户信息（受益人不算客户） */
    private LDPersonSet mLDPersonSet = new LDPersonSet();

    /** 地址信息 */
    private LCAddressSet mLCAddressSet = new LCAddressSet();

    /** 创建被保客户标志 */
    private boolean needCreatInsured = false;

    /** 投保人客户号 */
    private String mAppntNo;

    /** 险种信息 */
    private LCPolSet mLCPolSet;

    /** 被保人客户号 */
    private String mInsuredNo;
    
    /** 被保人客户号 */
    private String upInsuredNo;

    /** 被保人客户号 */
    private String tInsuredNo;

    /** 团体合同号 */
    private String mContNo;

    /** 套餐中的险种个数 */
    private int mNo;

    private String mWorkName;

    /** 递交数据 */
    private MMap map = new MMap();

    /** 封装地址信息的 */
    private TransferData mTransferData = new TransferData();

    /** 被保人信息 */
    private LCAddressSchema mInsuredAddressSchema;

    /** 被保人信息 */
    private LCAddressSchema tInsuredAddressSchema;

    /** 生成地址编码标志 */
    private boolean needCreatAddressNo = false;
    
    
    private String taddressNo;//修改前地地号

    /** 添加日志对象 */
    private static Logger log = Logger.getLogger(FamilySingleContInputBL.class);

    //查询公共方法
    ExeSQL tExeSQL = new ExeSQL();

    public boolean submitData(VData nInputData, String cOperate)
    {
        System.out.println("Into FamilySingleContInputBL.submitData()...");
        if (getSubmitMap(nInputData, cOperate) == null)
        {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (!ps.submitData(this.mResult, "INSERT"))
            {
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if (!ps.submitData(this.mResult, "DELETE"))
            {
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!ps.submitData(this.mResult, "UPDATE"))
            {
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }
        }
        return true;
    }

    /**
     * 生成简易平台被保人信息，并返回MMap结果集
     *
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        System.out.println("Into FamilySingleContInputBL.getSubmitMap()...");
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        System.out.println("@@完成程序submitData第41行");

        if (!getInputData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        if (!prepareOutputData())
        {
            return null;
        }

        return map;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.add(this.mLCContSchema);
        mResult.add(this.mLCAppntSchema);
        mResult.add(this.mLDPersonSet);

        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (map == null)
            {
                map = new MMap();
            }
            if ("1".equals(mLCInsuredSchema.getSequenceNo()))
            {
                map.put(this.mLCContSchema, "UPDATE");
            }
            map.put(this.mLCAppntSchema, "UPDATE");
            map.put(this.mLCInsuredSchema, "DELETE&INSERT");
            map.put(this.mLDPersonSet, "INSERT");

            if (this.mLCAddressSet.size() > 0)
            {
                for (int i = 1; i <= mLCAddressSet.size(); i++)
                {
                    LCAddressSchema tLCAddressSchema = new LCAddressSchema();
                    tLCAddressSchema = mLCAddressSet.get(i);
                    map.put(tLCAddressSchema, "DELETE&INSERT");
                }
            }

            mResult.add(map);
        }

        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (map == null)
            {
                map = new MMap();
            }
            if ("1".equals(mLCInsuredSchema.getSequenceNo()))
            {
                map.put(this.mLCContSchema, "UPDATE");
            }
            map.put(this.tLCInsuredSchema, "DELETE&INSERT");
            map.put(this.tInsuredAddressSchema, "DELETE&INSERT");
            map.put(this.mLDPersonSet, "DELETE&INSERT");

            mResult.add(map);
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if (map == null)
            {
                map = new MMap();
            }
            if ("1".equals(mLCInsuredSchema.getSequenceNo()))
            {
                map.put(this.mLCContSchema, "UPDATE");
            }
            map.put(this.tLCInsuredSchema, "DELETE");
            map.put(this.tInsuredAddressSchema, "DELETE");

            mResult.add(map);

            if (this.map.size() > 0)
            {
                this.mResult.add(map);
            }
            else
            {
                buildError("prepareOutputData", "进行删除操作,但没有删除数据！");
                return false;
            }
        }
        return true;
    }

    private boolean insertData()
    {
        System.out.println("Into FamilySingleContInputBL.insertData()...");

        /** 处理完成投保人开始处理被保人信息 */
        System.out.println("开始处理被保险人");
        if (!dealInsured())
        {
            return false;
        }
        System.out.println("完成被保人信息录入！");
        if (!dealAddress())
        {
            return false;
        }

        return true;
    }

    private boolean updateData()
    {
        System.out.println("Into FamilySingleContInputBL.updateData()...");

        /** 处理完成投保人开始处理被保人信息 */
        System.out.println("开始处理被保险人");
        if (!dealupdateInsured())
        {
            return false;
        }
        System.out.println("完成被保人信息录入！");
        if (!dealupdateAddress())
        {
            return false;
        }

        return true;
    }

    private boolean dealupdateInsured()
    {
        tLCInsuredSchema= new LCInsuredSchema();

        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(upInsuredNo);
        tLCInsuredDB.setContNo(mLCContSchema.getContNo());
        tLCInsuredSchema = tLCInsuredDB.query().get(1);
        tInsuredNo = tLCInsuredSchema.getInsuredNo();//修改之前客户号
        taddressNo=tLCInsuredSchema.getAddressNo();
        //判断被保人是否修改了五要素
        if (!checkInsured())
        {//修改了
            return false;
        }else
            {
            
            
            if (this.needCreatInsured)
            {
                
                ExeSQL tExeSQL = new ExeSQL();
                String mAddressNo = tExeSQL
                        .getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
                                + mInsuredNo + "'");
                if (StrTool.cTrim(mAddressNo).equals(""))
                {
                    buildError("checkAppntAddress", "生成地址代码错误！");
                    return false;
                }
                
             
            /** 检查完被保人，根据创建标志，判断是否新建客户 */
            this.mInsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            
            tLCInsuredSchema.setInsuredNo(mInsuredNo);//修改客户号
            tLCInsuredSchema.setRelationToMainInsured(mLCInsuredSchema.getRelationToMainInsured());//朱被保险人(第一被保险人)
            tLCInsuredSchema.setPosition(mLCInsuredSchema.getPosition());
            tLCInsuredSchema.setSalary(mLCInsuredSchema.getSalary());//年收入
            tLCInsuredSchema.setOccupationType(mLCInsuredSchema.getOccupationType());
            tLCInsuredSchema.setOccupationCode(mLCInsuredSchema.getOccupationCode());
            tLCInsuredSchema.setOperator(mGlobalInput.Operator);
            tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
            tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
            tLCInsuredSchema.setMakeDate(PubFun.getCurrentDate());
            tLCInsuredSchema.setMakeTime(PubFun.getCurrentTime());
            tLCInsuredSchema.setName(mLCInsuredSchema.getName());
            tLCInsuredSchema.setSex(mLCInsuredSchema.getSex());
            tLCInsuredSchema.setIDStartDate(mLCInsuredSchema.getIDStartDate());
            tLCInsuredSchema.setIDEndDate(mLCInsuredSchema.getIDEndDate());
            tLCInsuredSchema.setBirthday(mLCInsuredSchema.getBirthday());
            tLCInsuredSchema.setIDType(mLCInsuredSchema.getIDType());
            tLCInsuredSchema.setIDNo(mLCInsuredSchema.getIDNo());
            tLCInsuredSchema.setRelationToAppnt(mLCInsuredSchema.getRelationToAppnt());
            tLCInsuredSchema.setNativePlace(mLCInsuredSchema.getNativePlace());
            tLCInsuredSchema.setNativeCity(mLCInsuredSchema.getNativeCity());
            tLCInsuredSchema.setAddressNo(mAddressNo);
            dealInsuredToCont();//处理保单表  gengxinsql

            if(upInsuredNo!=""){
                String tsql = "delete from LCInsured where ContNo='" + mLCInsuredSchema.getContNo()
                + "' and InsuredNo='" + upInsuredNo + "'";

                map.put(tsql, "DELETE");
            }
            

           
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();

            tLDPersonSchema.setCustomerNo(tLCInsuredSchema.getInsuredNo());
            tLDPersonSchema.setName(this.mLCInsuredSchema.getName());
            tLDPersonSchema.setSex(this.mLCInsuredSchema.getSex());
            tLDPersonSchema.setBirthday(this.mLCInsuredSchema.getBirthday());
            tLDPersonSchema.setIDType(this.mLCInsuredSchema.getIDType());
            tLDPersonSchema.setIDNo(this.mLCInsuredSchema.getIDNo());
            tLDPersonSchema.setOperator(this.mGlobalInput.Operator);
            tLDPersonSchema.setModifyDate(PubFun.getCurrentDate());
            tLDPersonSchema.setModifyTime(PubFun.getCurrentTime());
            tLDPersonSchema.setMakeDate(PubFun.getCurrentDate());
            tLDPersonSchema.setMakeTime(PubFun.getCurrentTime());
            tLDPersonSchema.setNativePlace(this.mLCInsuredSchema.getNativePlace());
            tLDPersonSchema.setNativeCity(this.mLCInsuredSchema.getNativeCity());
            mLDPersonSet.add(tLDPersonSchema);

           

        }else{
            
            if(upInsuredNo!="" && !upInsuredNo.equals(mLCInsuredSchema.getInsuredNo())){
                String tsql = "delete from LCInsured where ContNo='" + mLCInsuredSchema.getContNo()
                + "' and InsuredNo='" + tInsuredNo + "'";

                map.put(tsql, "DELETE");
                
                String tsqladd = "delete from LCAddress where CustomerNo='" + upInsuredNo+ "' and AddressNo='" + taddressNo + "'";

                map.put(tsqladd, "DELETE");
                
                ExeSQL tExeSQL = new ExeSQL();
                String mAddressNo = tExeSQL
                        .getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
                                + mInsuredNo + "'");
                if (StrTool.cTrim(mAddressNo).equals(""))
                {
                    buildError("checkAppntAddress", "生成地址代码错误！");
                    return false;
                }
                
                tLCInsuredSchema.setAddressNo(mAddressNo);
            }
            
            
            
            tLCInsuredSchema.setInsuredNo(mLCInsuredSchema.getInsuredNo());//修改客户号
            tLCInsuredSchema.setRelationToMainInsured(mLCInsuredSchema.getRelationToMainInsured());//朱被保险人(第一被保险人)
            tLCInsuredSchema.setPosition(mLCInsuredSchema.getPosition());
            tLCInsuredSchema.setSalary(mLCInsuredSchema.getSalary());//年收入
            tLCInsuredSchema.setOccupationType(mLCInsuredSchema.getOccupationType());
            tLCInsuredSchema.setOccupationCode(mLCInsuredSchema.getOccupationCode());
            tLCInsuredSchema.setOperator(mGlobalInput.Operator);
            tLCInsuredSchema.setModifyDate(PubFun.getCurrentDate());
            tLCInsuredSchema.setModifyTime(PubFun.getCurrentTime());
            tLCInsuredSchema.setName(mLCInsuredSchema.getName());
            tLCInsuredSchema.setSex(mLCInsuredSchema.getSex());
            tLCInsuredSchema.setIDStartDate(mLCInsuredSchema.getIDStartDate());
            tLCInsuredSchema.setIDEndDate(mLCInsuredSchema.getIDEndDate());
            tLCInsuredSchema.setBirthday(mLCInsuredSchema.getBirthday());
            tLCInsuredSchema.setIDType(mLCInsuredSchema.getIDType());
            tLCInsuredSchema.setIDNo(mLCInsuredSchema.getIDNo());
            tLCInsuredSchema.setRelationToAppnt(mLCInsuredSchema.getRelationToAppnt());
            tLCInsuredSchema.setNativePlace(mLCInsuredSchema.getNativePlace());
            tLCInsuredSchema.setNativeCity(mLCInsuredSchema.getNativeCity());

            
                
            }

        }
       return true;
    }

    private boolean dealupdateAddress()
    {
        tInsuredAddressSchema= new LCAddressSchema();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(upInsuredNo);
        tLCAddressDB.setAddressNo(taddressNo);
        tInsuredAddressSchema = tLCAddressDB.query().get(1);
        
        if (this.needCreatInsured)
        {//修改了
            
            tInsuredAddressSchema.setCustomerNo(tLCInsuredSchema.getInsuredNo());//修改后的客户号
            tInsuredAddressSchema.setAddressNo(tLCInsuredSchema.getAddressNo());
            tInsuredAddressSchema.setGrpName(mInsuredAddressSchema.getGrpName());
            tInsuredAddressSchema.setPostalAddress(mInsuredAddressSchema.getPostalAddress());
            tInsuredAddressSchema.setPostalProvince(mInsuredAddressSchema.getPostalProvince());
            tInsuredAddressSchema.setPostalCity(mInsuredAddressSchema.getPostalCity());
            tInsuredAddressSchema.setPostalCounty(mInsuredAddressSchema.getPostalCounty());
            tInsuredAddressSchema.setPostalStreet(mInsuredAddressSchema.getPostalStreet());
            tInsuredAddressSchema.setPostalCommunity(mInsuredAddressSchema.getPostalCommunity());
            tInsuredAddressSchema.setZipCode(mInsuredAddressSchema.getZipCode());
            tInsuredAddressSchema.setPhone(mInsuredAddressSchema.getPhone());
            tInsuredAddressSchema.setHomePhone(mInsuredAddressSchema.getHomePhone());
            tInsuredAddressSchema.setMobile(mInsuredAddressSchema.getMobile());
            tInsuredAddressSchema.setCompanyPhone(mInsuredAddressSchema.getCompanyPhone());
            tInsuredAddressSchema.setEMail(mInsuredAddressSchema.getEMail());
            tInsuredAddressSchema.setOperator(mGlobalInput.Operator);
            tInsuredAddressSchema.setMakeDate(PubFun.getCurrentDate());
            tInsuredAddressSchema.setModifyDate(PubFun.getCurrentDate());
            tInsuredAddressSchema.setMakeTime(PubFun.getCurrentTime());
            tInsuredAddressSchema.setModifyTime(PubFun.getCurrentTime());
            
            if(upInsuredNo!=""){
                String tsql = "delete from LCAddress where CustomerNo='" +upInsuredNo+ "' and AddressNo='" + taddressNo + "'";

                map.put(tsql, "DELETE");
            }

        }
        else
        {
            
            tInsuredAddressSchema.setCustomerNo(tLCInsuredSchema.getInsuredNo());
            tInsuredAddressSchema.setAddressNo(tLCInsuredSchema.getAddressNo());
            tInsuredAddressSchema.setGrpName(mInsuredAddressSchema.getGrpName());
            tInsuredAddressSchema.setPostalAddress(mInsuredAddressSchema.getPostalAddress());
            tInsuredAddressSchema.setPostalProvince(mInsuredAddressSchema.getPostalProvince());
            tInsuredAddressSchema.setPostalCity(mInsuredAddressSchema.getPostalCity());
            tInsuredAddressSchema.setPostalCounty(mInsuredAddressSchema.getPostalCounty());
            tInsuredAddressSchema.setPostalStreet(mInsuredAddressSchema.getPostalStreet());
            tInsuredAddressSchema.setPostalCommunity(mInsuredAddressSchema.getPostalCommunity());
            tInsuredAddressSchema.setZipCode(mInsuredAddressSchema.getZipCode());
            tInsuredAddressSchema.setPhone(mInsuredAddressSchema.getPhone());
            tInsuredAddressSchema.setHomePhone(mInsuredAddressSchema.getHomePhone());
            tInsuredAddressSchema.setMobile(mInsuredAddressSchema.getMobile());
            tInsuredAddressSchema.setCompanyPhone(mInsuredAddressSchema.getCompanyPhone());
            tInsuredAddressSchema.setEMail(mInsuredAddressSchema.getEMail());
            tInsuredAddressSchema.setAddressNo(tLCInsuredSchema.getAddressNo());
            tInsuredAddressSchema.setOperator(mGlobalInput.Operator);
            tInsuredAddressSchema.setMakeDate(PubFun.getCurrentDate());
            tInsuredAddressSchema.setModifyDate(PubFun.getCurrentDate());
            tInsuredAddressSchema.setMakeTime(PubFun.getCurrentTime());
            tInsuredAddressSchema.setModifyTime(PubFun.getCurrentTime());
        }

        return true;
    }

    //删除被保人信息
    private boolean deleteData()
    {
        System.out.println("Into FamilySingleContInputBL.deleteData()...");

        /** 处理完成投保人开始处理被保人信息 */
        System.out.println("开始处理被保险人");
        if (!deleteInsured())
        {
            return false;
        }
        System.out.println("完成被保人信息删除！");
        if (!deleteAddress())
        {
            return false;
        }

        return true;
    }

    private boolean deleteInsured()
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setInsuredNo(mLCInsuredSchema.getInsuredNo());
        tLCInsuredDB.setContNo(mLCContSchema.getContNo());
        tLCInsuredSchema = tLCInsuredDB.query().get(1);

        if ("1".equals(tLCInsuredSchema.getSequenceNo()))
        {
            mLCContSchema.setInsuredNo("0");
            mLCContSchema.setInsuredBirthday("");
            mLCContSchema.setInsuredIDNo("");
            mLCContSchema.setInsuredIDType("");
            mLCContSchema.setInsuredName("");
            mLCContSchema.setInsuredSex("");
        }

        return true;
    }

    private boolean deleteAddress()
    {
        if(!"00".equals(mLCInsuredSchema.getRelationToAppnt())){
            LCAddressDB tLCAddressDB = new LCAddressDB();
            tLCAddressDB.setCustomerNo(tLCInsuredSchema.getInsuredNo());
            tLCAddressDB.setAddressNo(tLCInsuredSchema.getAddressNo());
            tInsuredAddressSchema = tLCAddressDB.query().get(1);
        }
        
        return true;
    }

    /**
     * dealAddress
     *
     * @return boolean
     */
    private boolean dealAddress()
    {

        /** 处理被保人地址信息 */
        if (!dealInsuredAddress())
        {
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        System.out.println("Into BriefSingleContInputBL.dealData()..." + mOperate);
        /** 执行新单录入过程 */
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (!insertData())
            {
                return false;
            }
        }
        else if (this.mOperate.equals("DELETE||MAIN"))
        {
            if (!deleteData())
            {
                return false;
            }
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!updateData())
            {
                return false;
            }
        }
        else
        {
            buildError("dealData", "目前只支持保存修改,删除功能不支持其它操作！");
            return false;
        }
        return true;
    }

    private boolean getInputData()
    {
        try
        {
            System.out.println("Into FamilySingleContInputBL.getInputData()...");
            if (this.mInputData == null)
            {
                CError tError = new CError();
                tError.moduleName = "FamilySingleContInputBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的封装对象为空！";
                System.out.println("程序第277行出错，" + "请检查FamilySingleContInputBL中的" + "getInputData方法！"
                        + tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            if (this.mOperate == null)
            {
                CError tError = new CError();
                tError.moduleName = "FamilySingleContInputBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有传入操作符！";
                System.out.println("程序第169行出错，" + "请检查FamilySingleContInputBL中的" + "getInputData方法！"
                        + tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            
           

            //前台传过来的被保人信息
            mLCInsuredSchema = (LCInsuredSchema) mInputData.getObjectByObjectName("LCInsuredSchema", 0);

            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);

            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setPrtNo(mLCInsuredSchema.getPrtNo());

            //数据库中的保单信息
            mLCContSchema = tLCContDB.query().get(1);

            if (mLCContSchema == null)
            {
                System.out.println("查询保单信息失败");
                CError tError = new CError();
                tError.moduleName = "FamilySingleContInputBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "查询保单信息失败！";
                this.mErrors.addOneError(tError);
                return false;
            }

            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(mLCContSchema.getContNo());

            mLCAppntSchema = tLCAppntDB.query().get(1);
            mAppntNo = mLCAppntSchema.getAppntNo();
            if (mTransferData == null)
            {
                buildError("getInputData", "传入的地址信息为空！");
                return false;
            }

            mInsuredAddressSchema = (LCAddressSchema) mTransferData.getValueByName("InsuredAddress");

            mWorkName = mInsuredAddressSchema.getGrpName();//工作单位
            
            upInsuredNo=(String)mTransferData.getValueByName("upInsuredno");
            
            System.out.println("工作单位:================"+mWorkName);
            if (mLCAppntSchema != null && this.mLCAppntSchema.getOccupationCode() != null
                    && !this.mLCAppntSchema.getOccupationCode().equals(""))
            {
                LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                tLDOccupationDB.setOccupationCode(mLCAppntSchema.getOccupationCode());
                if (!tLDOccupationDB.getInfo())
                {
                    String str = "查询投保人职业代码失败！";
                    buildError("getInputData", str);
                    System.out.println("在程序FamilySingleContInputBL.getInputData() - 1923 : " + str);
                    return false;
                }
                mLCAppntSchema.setOccupationType(tLDOccupationDB.getOccupationType());
            }

            if (mLCInsuredSchema != null && this.mLCInsuredSchema.getOccupationCode() != null
                    && !this.mLCInsuredSchema.getOccupationCode().equals(""))
            {
                LDOccupationDB tLDOccupationDB = new LDOccupationDB();
                tLDOccupationDB.setOccupationCode(mLCInsuredSchema.getOccupationCode());
                if (!tLDOccupationDB.getInfo())
                {
                    String str = "查询被保人职业代码失败！";
                    buildError("getInputData", str);
                    System.out.println("在程序FamilySingleContInputBL.getInputData() - 1923 : " + str);
                    return false;
                }
                mLCInsuredSchema.setOccupationType(tLDOccupationDB.getOccupationType());
            }

            //通过lccont和lcappnt 补充被保人信息

            mLCInsuredSchema.setGrpContNo(mLCContSchema.getGrpContNo());
            mLCInsuredSchema.setContNo(mLCContSchema.getContNo());
            mLCInsuredSchema.setAppntNo(mLCAppntSchema.getAppntNo());
            mLCInsuredSchema.setManageCom(mLCContSchema.getManageCom());
            mLCInsuredSchema.setExecuteCom(mGlobalInput.ManageCom);
            mLCInsuredSchema.setFamilyID(mLCContSchema.getFamilyID());
            //mLCInsuredSchema.setAddressNo(request.getParameter("insured_AddressNo"));//地址号码

            //mLCInsuredSchema.setInsuredStat(request.getParameter("insured_InsuredStat"));
            //被保人顺序
            String tSql = "select max(SequenceNo) from lcinsured where ContNo='" + mLCContSchema.getContNo() + "'";
            String tseqInsured = tExeSQL.getOneValue(tSql);
            if (tseqInsured != null && !"".equals(tseqInsured))
            {
                String tseqno = ((Integer.parseInt(tseqInsured) + 1)) + "";
                mLCInsuredSchema.setSequenceNo(tseqno);
            }
            else
            {
                mLCInsuredSchema.setSequenceNo("1");
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("获取数据失败，具体原因是：" + e.getMessage());
            return false;
        }
        return true;
    }

    private boolean dealInsured()
    {
        /** 首先判断被保人与投保人的关系,如果是投保人被人,则不需要上面那么多校验 */
        if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).equals("00"))
        {
            if (!dealAppntToInsured())
            {
                return false;
            }
        }
        else
        {
            /** 新建被保险客户 */
            if (!checkInsured())
            {
                return false;
            }
            if (this.needCreatInsured)
            {
                /** 检查完被保人，根据创建标志，判断是否新建客户 */
                this.mInsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
                
                System.out.println("");
                if (StrTool.cTrim(this.mInsuredNo).equals(""))
                {
                    // @@错误处理
                    System.out.println("FamilySingleContInputBL中" + "insertData方法报错，" + "在程序202行，Author:Yangming");
                    CError tError = new CError();
                    tError.moduleName = "BriefSingleContInputBL";
                    tError.functionName = "insertData";
                    tError.errorMessage = "生成客户号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                else
                {
                    this.mLCInsuredSchema.setAppntNo(this.mAppntNo);
                    this.mLCInsuredSchema.setInsuredNo(this.mInsuredNo);

                    //将客户身份证号码中的x转换成大写(被保险人) 2009-02-05 liuyp
                    if (mLCInsuredSchema.getIDType() != null && mLCInsuredSchema.getIDNo() != null)
                    {
                        if (mLCInsuredSchema.getIDType().equals("0"))
                        {
                            String tLCInsuredIdNo = mLCInsuredSchema.getIDNo().toUpperCase();
                            mLCInsuredSchema.setIDNo(tLCInsuredIdNo);
                        }
                    }

                    /** 生成客户 */
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tLDPersonSchema.setCustomerNo(this.mInsuredNo);
                    tLDPersonSchema.setName(this.mLCInsuredSchema.getName());
                    tLDPersonSchema.setSex(this.mLCInsuredSchema.getSex());
                    tLDPersonSchema.setBirthday(this.mLCInsuredSchema.getBirthday());
                    tLDPersonSchema.setIDType(this.mLCInsuredSchema.getIDType());
                    tLDPersonSchema.setIDNo(this.mLCInsuredSchema.getIDNo());
                    tLDPersonSchema.setOperator(this.mGlobalInput.Operator);
                    System.out.println("工作单位是...." + mWorkName);
                    tLDPersonSchema.setGrpName(mWorkName);
                    PubFun.fillDefaultField(tLDPersonSchema);

                    //保证客户信息数据的实时性
                    //this.mLDPersonSet.add(tLDPersonSchema);
                    LDPersonDB mLDPersonDB = new LDPersonDB();
                    mLDPersonDB.setSchema(tLDPersonSchema);
                    if (!mLDPersonDB.insert())
                    {
                        //错误处理
                        System.out.println("FamilySingleContInputBL中，插入被保人信息错误！");
                        CError tError = new CError();
                        tError.moduleName = "BriefSingleContInputBL";
                        tError.functionName = "dealInsured";
                        tError.errorMessage = "保存客户信息错误！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                /** 封装Makedate等信息 */
                PubFun.fillDefaultField(mLCInsuredSchema);
                this.mLCInsuredSchema.setOperator(this.mGlobalInput.Operator);
                this.mLCInsuredSchema.setManageCom(this.mGlobalInput.ManageCom);
                System.out.println("处理完成被保人");
            }
        }
        if (!dealInsuredToCont())
        {
            return false;
        }

        return true;
    }

    /**
     * dealInsuredToCont
     *
     * @return boolean
     */
    private boolean dealInsuredToCont()
    {
        this.mLCContSchema.setInsuredBirthday(this.mLCInsuredSchema.getBirthday());
        this.mLCContSchema.setInsuredIDNo(this.mLCInsuredSchema.getIDNo());
        this.mLCContSchema.setInsuredIDType(this.mLCInsuredSchema.getIDType());
        this.mLCContSchema.setInsuredName(this.mLCInsuredSchema.getName());
        this.mLCContSchema.setInsuredNo(this.mLCInsuredSchema.getInsuredNo());
        this.mLCContSchema.setInsuredSex(this.mLCInsuredSchema.getSex());
        return true;
    }

    private boolean checkInsured()
    {
        System.out.println("系统同录入的投保人客户号码，开始验证客户信息");
        System.out.println("录入_" + "被保人" + "姓名 : " + StrTool.unicodeToGBK(mLCInsuredSchema.getName()));
        System.out.println("录入_" + "被保人" + "性别 : " + StrTool.unicodeToGBK(mLCInsuredSchema.getSex()));
        System.out.println("录入_" + "被保人" + "生日 :　" + StrTool.unicodeToGBK(mLCInsuredSchema.getBirthday()));
        System.out.println("录入_" + "被保人" + "证件类型 :　" + StrTool.unicodeToGBK(mLCInsuredSchema.getIDType()));
        System.out.println("录入_" + "被保人" + "证件号码 :　" + StrTool.unicodeToGBK(mLCInsuredSchema.getIDNo()));

        LDPersonDB tLDPersonDB = new LDPersonDB();
        if (!StrTool.unicodeToGBK(StrTool.cTrim(this.mLCInsuredSchema.getInsuredNo())).equals(""))
        {
            tLDPersonDB.setCustomerNo(mLCInsuredSchema.getInsuredNo());
            if (!tLDPersonDB.getInfo())
            {
                // @@错误处理
                buildError("checkInsured", "录入被保人客户号，但是没有找到此客户号对应的客户信息！！");
                return false;
            }
            else
            {
                this.mInsuredNo = mLCInsuredSchema.getInsuredNo();
                if (!StrTool.unicodeToGBK(StrTool.cTrim(this.mLCInsuredSchema.getName())).equals(
                        StrTool.cTrim(tLDPersonDB.getName())))
                {
                    buildError("checkInsured", "被保客户姓名与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                if (!StrTool.cTrim(this.mLCInsuredSchema.getBirthday())
                        .equals(StrTool.cTrim(tLDPersonDB.getBirthday())))
                {
                    buildError("checkInsured", "被保客户生日与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                /** 性别 */
                if (!StrTool.cTrim(this.mLCInsuredSchema.getSex()).equals(StrTool.cTrim(tLDPersonDB.getSex())))
                {
                    buildError("checkInsured", "被保客户性别与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                /** 证件类型 */
                if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).equals(StrTool.cTrim(tLDPersonDB.getIDType())))
                {
                    buildError("checkInsured", "被保客户证件类型与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }

                /** 证件号码 */
                // 将客户身份证号码中的x转换成大写（被保险人） 2009-02-17 liuyp
                if (this.mLCInsuredSchema.getIDType() != null && tLDPersonDB.getIDType() != null)
                {
                    if (this.mLCInsuredSchema.getIDType().equals("0") && tLDPersonDB.getIDType().equals("0"))
                    {
                        if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).toUpperCase().equals(
                                StrTool.cTrim(tLDPersonDB.getIDNo()).toUpperCase()))
                        {
                            buildError("checkInsured", "被保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                            return false;
                        }
                    }
                }
                else
                {
                    if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).equals(StrTool.cTrim(tLDPersonDB.getIDNo())))
                    {
                        buildError("checkInsured", "被保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                        return false;
                    }
                }
            }
        }
        else
        {
            /** 如果没有录入客户号码，根据基本信息校验是否是老客户 */
            if (!queryInsuerd())
            {
                return false;
            }
        }
        System.out.println("校验完成，无论是否应该生成客户都应该把投保人号码存入" + this.mAppntNo);
        mLCInsuredSchema.setAppntNo(this.mAppntNo);
        mLCInsuredSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCInsuredSchema);
        return true;
    }

    /**
     * queryInsuerd
     *
     * @return boolean
     */
    private boolean queryInsuerd()
    {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        String Sex = "";
        String Birthday = "";
        if (!StrTool.cTrim(this.mLCInsuredSchema.getName()).equals(""))
        {
            tLDPersonDB.setName(this.mLCInsuredSchema.getName());
        }
        else
        {
            buildError("queryInsuerd", "没有录入被保人姓名！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getSex()).equals(""))
        {
            Sex = this.mLCInsuredSchema.getSex();
        }
        else
        {
            buildError("queryInsuerd", "没有录入被保人性别！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getBirthday()).equals(""))
        {
            Birthday = this.mLCInsuredSchema.getBirthday();
        }
        else
        {
            buildError("queryInsuerd", "没有录入被保人生日！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).equals(""))
        {
            //证件类型校验     by zhangyang 2011-01-12
            String tIDType = this.mLCInsuredSchema.getIDType();
//            if (!tIDType.equals("0") && !tIDType.equals("1") && !tIDType.equals("2") && !tIDType.equals("3")
//                    && !tIDType.equals("4"))
//            {
//                buildError("queryInsuerd", "被保险人的证件类型为" + tIDType + ", 不符合系统规范，系统规定证件类型必须为数字的0到4！");
//                return false;
//            }
            //-------------------------------------

            tLDPersonDB.setIDType(this.mLCInsuredSchema.getIDType());
        }
        else
        {
            buildError("queryInsuerd", "没有录入被保人证件类型！！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).equals(""))
        {
            tLDPersonDB.setIDNo(this.mLCInsuredSchema.getIDNo());
        }
        else if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).equals("4"))
        {
            buildError("queryInsuerd", "被保人证件号码录入错误！！");
            return false;
        }

        LDPersonSet tLDPersonSet = new LDPersonSet();

        if (tLDPersonDB.getIDType().equals("0"))
        {
            tLDPersonSet = tLDPersonDB.query();
        }
        else
        {
            tLDPersonDB.setSex(Sex);
            tLDPersonDB.setBirthday(Birthday);
            tLDPersonSet = tLDPersonDB.query();
        }

        if (tLDPersonSet.size() >= 1)
        {
            /** 找到为一个客户信息 */
            /** 取出当前客户的客户号码 */
            this.mLCInsuredSchema.setInsuredNo(tLDPersonSet.get(1).getCustomerNo());
            this.mInsuredNo = mLCInsuredSchema.getInsuredNo();
        }
        else if (tLDPersonSet.size() == 0)
        {
            this.needCreatInsured = true;
        }
        else
        {
            buildError("queryInsuerd", "不会这么巧吧,存在相近客户,请检查客户信息！");
            return false;
        }
        return true;
    }

    private boolean checkInsuredAddress()
    {
        String mAddressNo = "";
        System.out.println("投保人" + "地址代码 : " + this.mInsuredAddressSchema.getAddressNo());
        System.out.println("是否需要创建客户信息 :　" + (needCreatInsured ? "是" : "否"));
        if (!StrTool.cTrim(this.mInsuredAddressSchema.getAddressNo()).equals("") && !this.needCreatInsured)
        {
            LCAddressDB tLCAddressDB = new LCAddressDB();
            tLCAddressDB.setCustomerNo(this.mInsuredNo);
            tLCAddressDB.setAddressNo(mInsuredAddressSchema.getAddressNo());
            if (!tLCAddressDB.getInfo())
            {
                buildError("checkInsuredAddress", "录入客户号码与地址编码，但没有在系统中查询到该地址信息！");
                return false;
            }
            /**
             * 前台之会录入 ： 1、联系地址 PostalAddress 2、邮政编码 ZipCode 3、家庭电话 HomePhone
             * 4、移动电话 Mobile 5、办公电话 CompanyPhone 6、电子邮箱 EMail
             * 只校验上述信息如果不同则生成新的地址编码
             */
            if (!StrTool.cTrim(mInsuredAddressSchema.getPostalAddress()).equals(
                    StrTool.cTrim(tLCAddressDB.getPostalAddress())))
            {
                needCreatAddressNo = true;
            }
            else if (!StrTool.cTrim(mInsuredAddressSchema.getZipCode())
                    .equals(StrTool.cTrim(tLCAddressDB.getZipCode())))
            {
                needCreatAddressNo = true;
            }
            else if (!StrTool.cTrim(mInsuredAddressSchema.getHomePhone()).equals(
                    StrTool.cTrim(tLCAddressDB.getHomePhone())))
            {
                needCreatAddressNo = true;
            }
            else if (!StrTool.cTrim(mInsuredAddressSchema.getMobile()).equals(StrTool.cTrim(tLCAddressDB.getMobile())))
            {
                needCreatAddressNo = true;
            }
            else if (!StrTool.cTrim(mInsuredAddressSchema.getCompanyPhone()).equals(
                    StrTool.cTrim(tLCAddressDB.getCompanyPhone())))
            {
                needCreatAddressNo = true;
            }
            if (needCreatAddressNo)
            {
                ExeSQL tExeSQL = new ExeSQL();
                mAddressNo = tExeSQL
                        .getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
                                + mInsuredNo + "'");
                if (StrTool.cTrim(mAddressNo).equals(""))
                {
                    buildError("checkAppntAddress", "生成地址代码错误！");
                    return false;
                }
                mInsuredAddressSchema.setCustomerNo(this.mInsuredNo);
                mInsuredAddressSchema.setAddressNo(mAddressNo);
            }
        }
        else
        {
            mInsuredAddressSchema.setCustomerNo(this.mInsuredNo);
            mInsuredAddressSchema.setAddressNo("1");
        }
        this.mInsuredAddressSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(this.mInsuredAddressSchema);
        mInsuredAddressSchema.setCustomerNo(this.mInsuredNo);
        this.mLCAddressSet.add(mInsuredAddressSchema);
        System.out.println("完成地址信息 \n地址代码：" + mInsuredAddressSchema.getAddressNo() + "\n客户号码"
                + mInsuredAddressSchema.getCustomerNo());
        this.mLCInsuredSchema.setAddressNo(this.mInsuredAddressSchema.getAddressNo());
        return true;
    }

    private boolean dealInsuredAddress()
    {
        if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).equals("00"))
        {
            /** 投保人和被保人是本人,不需要生成地址编码 */
            mLCInsuredSchema.setAddressNo(this.mLCAppntSchema.getAddressNo());
            return true;
        }
        else
        {
            if (!checkInsuredAddress())
            {
                return false;
            }
            return true;
        }
    }

    /**
     * dealAppntToInsured
     *
     * @return boolean
     */
    private boolean dealAppntToInsured()
    {
        System.out.println("投保人和被保人是本人处理");
        mLCInsuredSchema.setInsuredNo(mLCAppntSchema.getAppntNo());
        mLCInsuredSchema.setAccName(mLCAppntSchema.getAccName());
        mLCInsuredSchema.setAddressNo(mLCAppntSchema.getAddressNo());
        mLCInsuredSchema.setAppntNo(mLCAppntSchema.getAppntNo());
        mLCInsuredSchema.setBirthday(mLCAppntSchema.getAppntBirthday());
        mLCInsuredSchema.setIDStartDate(mLCAppntSchema.getIDStartDate());
        mLCInsuredSchema.setIDEndDate(mLCAppntSchema.getIDEndDate());
        mLCInsuredSchema.setEnglishName(mLCAppntSchema.getEnglishName());
        mLCInsuredSchema.setOccupationCode(mLCAppntSchema.getOccupationCode());
        mLCInsuredSchema.setOccupationType(mLCAppntSchema.getOccupationType());
        mLCInsuredSchema.setName(mLCAppntSchema.getAppntName());
        mLCInsuredSchema.setSex(mLCAppntSchema.getAppntSex());
        mLCInsuredSchema.setIDType(mLCAppntSchema.getIDType());
        mLCInsuredSchema.setIDNo(mLCAppntSchema.getIDNo());
        mLCInsuredSchema.setOperator(mGlobalInput.Operator);
        mLCInsuredSchema.setManageCom(mLCContSchema.getManageCom());
        mLCInsuredSchema.setExecuteCom(mGlobalInput.ManageCom);
        mLCInsuredSchema.setPosition(mLCAppntSchema.getPosition());
        mLCInsuredSchema.setSalary(mLCAppntSchema.getSalary());
        mLCInsuredSchema.setNativePlace(mLCAppntSchema.getNativePlace());
        mLCInsuredSchema.setNativeCity(mLCAppntSchema.getNativeCity());
        PubFun.fillDefaultField(mLCInsuredSchema);
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 出错处理
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriefSingleContInputBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.err.println("程序报错：" + cError.errorMessage);
    }
}
