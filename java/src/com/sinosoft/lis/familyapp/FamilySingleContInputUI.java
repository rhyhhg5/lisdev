package com.sinosoft.lis.familyapp;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2014</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zjd
 * @version 6.0
 */
public class FamilySingleContInputUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    public FamilySingleContInputUI()
    {
    }

    public boolean submitData(VData nInputData, String cOperate)
    {
        FamilySingleContInputBL tFamilySingleContInputBL = new FamilySingleContInputBL();
        if (!tFamilySingleContInputBL.submitData(nInputData, cOperate))
        {
            this.mErrors.copyAllErrors(tFamilySingleContInputBL.mErrors);
            return false;
        }
        else
        {
            mResult = tFamilySingleContInputBL.getResult();
        }
        return true;
    }

    /**
     * �������
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }
}
