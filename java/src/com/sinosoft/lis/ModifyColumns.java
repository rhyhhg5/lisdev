package com.sinosoft.lis;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class ModifyColumns {
    /** 字段名 */
    private String[] mColumns;
    /** 报错信息 */
    private CErrors mErrors = new CErrors();
    /** 查询表名sql语句 */
    private StringBuffer mTableNameSql = new StringBuffer();
    /** 添加附加条件 */
    private String[] dataType;
    /** Table Schema */
    private String mSchema;
    /** Table Name */
    private String[] mTableNameWherePart;
    /** Data Sql */
    private StringBuffer[] dataSql;
    /** 修改内容 */
    private String[] mField;
    public ModifyColumns() {
        JdbcUrl jdbc = new JdbcUrl();
        String tSchema;
        if (!jdbc.getDBType().equals("DB2")) {
            tSchema = "DB2INST1";
        } else {
            tSchema = jdbc.getUserName().toUpperCase();
        }
        mSchema = (tSchema);
    }

    public static void main(String[] args) {
        ModifyColumns modifycolumns = new ModifyColumns();
        String[] col = {"NAME", "APPNTNAME", "INSUREDNAME"};
        String[] table = {"LC", "LP", "LB", "LOB", "LD", "LL", "LA", "LH", "LF",
                         "LI", "LJ", "LY"};
        String[] field = {"Tserenchunl.B"};
        modifycolumns.setColumns(col);
        modifycolumns.setTableNameWherePart(table);
        modifycolumns.setMField(field);
        modifycolumns.perpareSql();
        modifycolumns.query();
    }

    public void setColumns(String[] mColumns) {
        this.mColumns = mColumns;
    }

    public String[] getColumns() {
        return mColumns;
    }

    public boolean perpareSql() {
        if (this.mColumns == null || this.mColumns.length <= 0) {
            buildError("perpareSql", "准备sql出错,检查是否传入字段名！");
            return false;
        }

        mTableNameSql.append("SELECT TABLE_NAME FROM SYSIBM.SQLCOLUMNS ");
        mTableNameSql.append("WHERE TABLE_SCHEM='");
        mTableNameSql.append(mSchema);
        mTableNameSql.append("' ");
        mTableNameSql.append(" AND COLUMN_NAME IN (");
        for (int i = 0; i < this.mColumns.length; i++) {
            mTableNameSql.append("'");
            mTableNameSql.append(mColumns[i]);
            mTableNameSql.append("'");
            if (i != mColumns.length - 1) {
                mTableNameSql.append(",");
            }
        }
        mTableNameSql.append(") ");
        if (dataType != null && dataType.length > 0) {
            mTableNameSql.append("AND TYPE_NAME IN (");
            for (int i = 0; i < this.dataType.length; i++) {
                mTableNameSql.append("'");
                mTableNameSql.append(dataType[i]);
                mTableNameSql.append("'");
                if (i != dataType.length - 1) {
                    mTableNameSql.append(",");
                }
            }
            mTableNameSql.append(") ");
        }
        if (mTableNameWherePart != null && mTableNameWherePart.length > 0) {
            mTableNameSql.append("AND (TABLE_NAME ");
            for (int i = 0; i < this.mTableNameWherePart.length; i++) {
                mTableNameSql.append("LIKE '");
                mTableNameSql.append(mTableNameWherePart[i]);
                mTableNameSql.append("%' ");
                if (i != mTableNameWherePart.length - 1) {
                    mTableNameSql.append(" OR TABLE_NAME ");
                }
            }
            mTableNameSql.append(")");
        }

        System.out.println("完成sql :　" + mTableNameSql.toString());
        return true;
    }

    public boolean query() {
        /** 查旬各个表明然后分析数据 */
        SSRS ssrs = (new ExeSQL()).execSQL(mTableNameSql.toString());
        if (ssrs == null || ssrs.getMaxRow() <= 0) {
            buildError("query", "没有查询到符合条件的表名！");
            return false;
        }
        dataSql = new StringBuffer[ssrs.getMaxRow() * mColumns.length];
        int x = ssrs.getMaxRow() * mColumns.length - 1;
        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            for (int m = 0; m < this.mColumns.length; m++) {
                if (!needQuery(mColumns[m], ssrs.GetText(i, 1))) {
                    continue;
                }
                dataSql[x] = new StringBuffer();
                dataSql[x].append("SELECT '");
                dataSql[x].append(mColumns[m]);
                dataSql[x].append("','  ',A.* ");
                dataSql[x].append("FROM ");
                dataSql[x].append(ssrs.GetText(i, 1));
                dataSql[x].append(" A");
                if (mField != null && mField.length > 0) {
                    dataSql[x].append(" WHERE ");
                    dataSql[x].append(mColumns[m]);
                    dataSql[x].append(" IN (");
                    for (int n = 0; n < this.mField.length; n++) {
                        dataSql[x].append("'");
                        dataSql[x].append(mField[n]);
                        dataSql[x].append("'");
                        if (n != mField.length - 1) {
                            dataSql[x].append(",");
                        }
                    }
                    dataSql[x].append(")");
                }
                x--;
            }
        }
        for (int o = 0; o < dataSql.length; o++) {
//            System.out.println(dataSql[o].toString());
            try {
                if (dataSql[o] != null) {
                    SSRS tSSRS = (new ExeSQL()).execSQL(dataSql[o].toString());
                    if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                        System.out.println(dataSql[o].toString() + ";");
                    }
                }
            } catch (Exception ex) {
            }
        }
        return true;
    }

    /**
     * needQuery
     *
     * @param tColumns String
     * @param tTableName String
     * @return boolean
     */
    private boolean needQuery(String tColumns, String tTableName) {
        StringBuffer sql = new StringBuffer();
        sql.append(
                "SELECT COUNT(1) FROM SYSIBM.SQLCOLUMNS WHERE TABLE_NAME='");
        sql.append(tTableName);
        sql.append("' AND COLUMN_NAME='");
        sql.append(tColumns);
        sql.append("'");
        String chk = (new ExeSQL()).getOneValue(sql.toString());
        if (Integer.parseInt(chk) > 0) {
            return true;
        } else if (Integer.parseInt(chk) <= 0) {
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ModifyColumns";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public void setDataType(String[] dataType) {
        this.dataType = dataType;
    }

    public void setMField(String[] mField) {
        this.mField = mField;
    }

    public void setTableNameWherePart(String[] mTableNameWherePart) {
        this.mTableNameWherePart = mTableNameWherePart;
    }

    public String[] getDataType() {
        return dataType;
    }

    public String[] getMField() {
        return mField;
    }

    public String[] getMableNameWherePart() {
        return mTableNameWherePart;
    }

}
