/*
* <p>ClassName: LMCalModeBLS </p>
* <p>Description: LMCalModeBLS类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 险种定义
* @CreateDate：2003-06-21
 */
package com.sinosoft.lis.sys;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

public class LMCalModeBLS {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors=new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LMCalModeBLS() {
    }
    public static void main(String[] args) {
    }
    /**
 传输数据的公共方法
 */
    public boolean submitData(VData cInputData,String cOperate)
    {
        boolean tReturn =false;
        //将操作数据拷贝到本类中
        this.mOperate =cOperate;

        System.out.println("Start LMCalModeBLS Submit...");
        if(this.mOperate.equals("INSERT||MAIN"))
                {tReturn=saveLMCalMode(cInputData);}
        if (this.mOperate.equals("DELETE||MAIN"))
                {tReturn=deleteLMCalMode(cInputData);}
        if (this.mOperate.equals("UPDATE||MAIN"))
                {tReturn=updateLMCalMode(cInputData);}
        if (tReturn)
            System.out.println(" sucessful");
        else
            System.out.println("Save failed") ;
        System.out.println("End LMCalModeBLS Submit...");
        return tReturn;
    }
    /**
     * 保存函数
     */
    private boolean saveLMCalMode(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCalModeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        try{
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LMCalModeDB tLMCalModeDB=new LMCalModeDB(conn);
            tLMCalModeDB.setSchema((LMCalModeSchema)mInputData.getObjectByObjectName("LMCalModeSchema",0));
            if (!tLMCalModeDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMCalModeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LMCalModeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors .addOneError(tError) ;
                conn.rollback();
                conn.close();
                return false;
            }
            LMCalFactorSet tLMCalFactorSet = new LMCalFactorSet();
            tLMCalFactorSet.set((LMCalFactorSet)mInputData.getObjectByObjectName("LMCalFactorSet",0));
            if (tLMCalFactorSet.size()>0)
            {
                LMCalFactorDBSet tLMCalFactorDBSet = new LMCalFactorDBSet(conn);
                tLMCalFactorDBSet.set(tLMCalFactorSet);
                if (!tLMCalFactorDBSet.insert())
                {
                    this.mErrors.copyAllErrors(tLMCalFactorDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LMCalFactorBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "计算要素记录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit() ;
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError =new CError();
            tError.moduleName="LMCalModeBLS";
            tError.functionName="submitData";
            tError.errorMessage=ex.toString();
            this.mErrors .addOneError(tError);
            tReturn=false;
            try{conn.rollback() ;
            conn.close();
            } catch(Exception e){}
        }
        return tReturn;
    }
    /**
     * 保存函数
     */
    private boolean deleteLMCalMode(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCalModeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        try{
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LMCalModeDB tLMCalModeDB=new LMCalModeDB(conn);
            tLMCalModeDB.setSchema((LMCalModeSchema)mInputData.getObjectByObjectName("LMCalModeSchema",0));
            if (!tLMCalModeDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMCalModeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LMCalModeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors .addOneError(tError) ;
                conn.rollback();
                conn.close();
                return false;
            }
            LMCalFactorDB tLMCalFactorDB = new LMCalFactorDB(conn);
            tLMCalFactorDB.setCalCode(tLMCalModeDB.getCalCode());
            if (!tLMCalFactorDB.deleteSQL())
            {
                this.mErrors.copyAllErrors(tLMCalFactorDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LMCalFactorBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "计算要素记录删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit() ;
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError =new CError();
            tError.moduleName="LMCalModeBLS";
            tError.functionName="submitData";
            tError.errorMessage=ex.toString();
            this.mErrors .addOneError(tError);
            tReturn=false;
            try{conn.rollback() ;
            conn.close();} catch(Exception e){}
        }
        return tReturn;
    }
    /**
     * 保存函数
     */
    private boolean updateLMCalMode(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
            CError tError = new CError();
            tError.moduleName = "LMCalModeBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        try{
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LMCalModeDB tLMCalModeDB=new LMCalModeDB(conn);
            tLMCalModeDB.setSchema((LMCalModeSchema)mInputData.getObjectByObjectName("LMCalModeSchema",0));
            if (!tLMCalModeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMCalModeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LMCalModeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError) ;
                conn.rollback();
                conn.close();
                return false;
            }
            LMCalFactorDB tLMCalFactorDB = new LMCalFactorDB(conn);
            tLMCalFactorDB.setCalCode(tLMCalModeDB.getCalCode());
            if (!tLMCalFactorDB.deleteSQL())
            {
                this.mErrors.copyAllErrors(tLMCalFactorDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LMCalFactorBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "计算要素记录删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LMCalFactorSet tLMCalFactorSet = new LMCalFactorSet();
            tLMCalFactorSet.set((LMCalFactorSet)mInputData.getObjectByObjectName("LMCalFactorSet",0));
            if (tLMCalFactorSet.size()>0)
            {
                LMCalFactorDBSet tLMCalFactorDBSet = new LMCalFactorDBSet(conn);
                tLMCalFactorDBSet.set(tLMCalFactorSet);
                if (!tLMCalFactorDBSet.insert())
                {
                    this.mErrors.copyAllErrors(tLMCalFactorDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LMCalFactorBLS";
                    tError.functionName = "updateData";
                    tError.errorMessage = "计算要素记录保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit() ;
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError =new CError();
            tError.moduleName="LMCalModeBLS";
            tError.functionName="submitData";
            tError.errorMessage=ex.toString();
            this.mErrors .addOneError(tError);
            tReturn=false;
            try{conn.rollback() ;
            conn.close();} catch(Exception e){}
        }
        return tReturn;
    }
}