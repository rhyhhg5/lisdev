package com.sinosoft.lis.sys;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author miaoxz
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GrpCDQListUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public GrpCDQListUI()
    {
        System.out.println("GrpCDQListUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	GrpCDQListBL tGrpCDQListBL = new GrpCDQListBL();
        if(!tGrpCDQListBL.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(tGrpCDQListBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
}
