package com.sinosoft.lis.sys;

import java.util.HashMap;

import com.sinosoft.lis.pubfun.diskimport.copy.MultiSheetImporter;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

public class LPContListImportFile {
	public LPContListImportFile() {
		try{
			jbInit();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	/**从xls文件读入sheet的开始行*/
	private static final int STARTROW = 1;
	/**使用默认的导入方式*/
	private MultiSheetImporter importer;
	/**错误处理*/
	public CErrors mErrors = new CErrors();
	/**Sheet Name*/
	private String[] mSheetName;
	/**Schema的名字*/
	private LDCodeSet mLDCodeSet;
	/**sheet对应table的名字*/
	private static final String[] mTableNames = {"LDCode"};
	public LPContListImportFile(String fileName,String configFileName,String[] sheetName) {
		mSheetName = sheetName;
		importer = new MultiSheetImporter(fileName, configFileName, sheetName);
	}
	/**执行导入*/
	public boolean doImport() {
		System.out.println("Into------doImport-------");
		importer.setTableName(mTableNames);
		importer.setMStartRows(STARTROW);
		if(!importer.doImport()) {
			mErrors.copyAllErrors(importer.mErrors);
			return false;
		}
		return true;
	}
	/**出错处理*/
	private void buildError(String szFunc,String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "MultiSheetImporter";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	/**得到导入结果*/
	public HashMap getResult() {
		return importer.getResult();
	}
	/**用于获取导入文件的版本号*/
	public String getVersion() {
		HashMap tHashMap = importer.getResult();
		String version = "";
		version = (String)tHashMap.get(mSheetName[1]);
		return version;
	}
	/**getSchemaSet 获取从MutiSheetImporter获得的SchemaSet*/
	public LDCodeSet getSchemaSet() {
		LDCodeSet tLDCodeSet = new LDCodeSet();
		HashMap tHashMap = importer.getResult();
		tLDCodeSet.add((LDCodeSet) tHashMap.get(mSheetName[0]));
		return tLDCodeSet;
	}
	private void jbInit() throws Exception {
	}
}
