package com.sinosoft.lis.sys;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CQLDPersonBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /*  */
  private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
  public CQLDPersonBL(){}
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;

    if (!getInputData(cInputData))
      return false;

    if (cOperate.equals("QUERY"))
    {
      if(!queryData())
        return false;
      System.out.println("---queryData---");
    }
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  private boolean getInputData(VData cInputData)
  {
    if (mOperate.equals("QUERY"))
    {
      mLDPersonSchema = (LDPersonSchema)cInputData.getObjectByObjectName("LDPersonSchema",0);
    }
    return true;
  }

  private boolean queryData()
  {
    LDPersonDB tLDPersonDB = new LDPersonDB();
    tLDPersonDB.setCustomerNo(mLDPersonSchema.getCustomerNo());
    tLDPersonDB.getInfo();
    LDPersonSchema tLDPersonSchema=new LDPersonSchema();
    tLDPersonSchema.setSchema(tLDPersonDB.getSchema());
    System.out.println("CustomerNo===="+mLDPersonSchema.getCustomerNo());
    if (tLDPersonDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LDPersonBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单查询失败!";
      this.mErrors.addOneError(tError);
      //mLDPersonSet.clear();
      return false;
    }
    	mResult.clear();
	mResult.add( tLDPersonSchema );

	return true;
  }

}