/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.sys;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LDHBlackListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    //private String mBlackListCode;

    //录入
    private LDHBlackListSchema mLDHBlackListSchema = new LDHBlackListSchema();
    private LDHBlackListSchema mupLDHBlackListSchema = new LDHBlackListSchema();
    
    private MMap mMap = new MMap();


    public LDHBlackListBL() {
    }



    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHBlackListBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LDHBlackListBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start BlackListPersonBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "BlackListPersonBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    ////////////////////////////////////////////////
    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mLDHBlackListSchema.setSchema((LDHBlackListSchema) cInputData.
                                            getObjectByObjectName(
                "LDHBlackListSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        return true;

    }

////////////////////////////////////////////////////////
    private boolean dealData() {

        if (mOperate.equals("INSERT||MAIN")) {
            mLDHBlackListSchema.setOperator(mGlobalInput.Operator);
            mLDHBlackListSchema.setMakeDate(CurrentDate);
            mLDHBlackListSchema.setMakeTime(CurrentTime);
            mLDHBlackListSchema.setModifyDate(CurrentDate);
            mLDHBlackListSchema.setModifyTime(CurrentTime);
            mMap.put(this.mLDHBlackListSchema, "INSERT");
        } else if (mOperate.equals("DELETE||MAIN")) {
        	LDHBlackListSet mLDHBlackListSet = new LDHBlackListSet();
        	LDHBlackListDB tLDHBlackListDB = new LDHBlackListDB();
        	tLDHBlackListDB.setBlacklistNo(mLDHBlackListSchema.getBlacklistNo());
        	mLDHBlackListSet = tLDHBlackListDB.query();
            if (mLDHBlackListSet == null || mLDHBlackListSet.size() == 0) {
                this.mErrors.copyAllErrors(tLDHBlackListDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "BlackListPersonBL";
                tError.functionName = "dealData";
                tError.errorMessage = "黑名单编号为" +
                                       mLDHBlackListSchema.getBlacklistNo() +
                                      "的信息不存在，无法删除！";
                this.mErrors.addOneError(tError);
                return false;
            } else {
                mMap.put(mLDHBlackListSet, "DELETE");
            }
        }

        else if (mOperate.equals("UPDATE||MAIN")) {
        	LDHBlackListDB tLDHBlackListDB = new LDHBlackListDB();
        	tLDHBlackListDB.setBlacklistNo(mLDHBlackListSchema.getBlacklistNo());
            if (!tLDHBlackListDB.getInfo()) {
                this.mErrors.copyAllErrors(tLDHBlackListDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "BlackListPersonBL";
                tError.functionName = "dealData";
                tError.errorMessage = "黑名单编号为" +
                                        mLDHBlackListSchema.getBlacklistNo() +
                                      "出错，需查询信息后修改！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mupLDHBlackListSchema = tLDHBlackListDB.getSchema();
            mupLDHBlackListSchema.setName(mLDHBlackListSchema.getName());
            mupLDHBlackListSchema.setType(mLDHBlackListSchema.getType());
            mupLDHBlackListSchema.setBirthday(mLDHBlackListSchema.getBirthday());
            mupLDHBlackListSchema.setIDType(mLDHBlackListSchema.getIDType());
            mupLDHBlackListSchema.setIDNo(mLDHBlackListSchema.getIDNo());
            mupLDHBlackListSchema.setNationality(mLDHBlackListSchema.getNationality());
            mupLDHBlackListSchema.setPublishCom(mLDHBlackListSchema.getPublishCom());
            mupLDHBlackListSchema.setOrganComCode(mLDHBlackListSchema.getOrganComCode());
            mupLDHBlackListSchema.setUnifiedSocialCreditNo(mLDHBlackListSchema.getUnifiedSocialCreditNo());
            mupLDHBlackListSchema.setCause(mLDHBlackListSchema.getCause());
            mupLDHBlackListSchema.setRemark(mLDHBlackListSchema.getRemark());
            mupLDHBlackListSchema.setSex(mLDHBlackListSchema.getSex());
            mupLDHBlackListSchema.setModifyDate(CurrentDate);
            mupLDHBlackListSchema.setModifyTime(CurrentTime);

            mMap.put(this.mupLDHBlackListSchema, "UPDATE");

             }
            return true;

    }
////////////////////////////////////////////////////////

        private boolean submitquery() {
            return true;
        }
////////////////////////////////////////////////////////

        /**
         * 准备后台的数据
         * @return boolean
         */
        private boolean prepareOutputData() {
            try {
                mInputData = new VData();
                this.mInputData.add(mMap);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "BlackListPersonBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }

//////////////////////////////////////////////

        public VData getResult() {
            this.mResult.clear();
            return mResult;
        }


}
