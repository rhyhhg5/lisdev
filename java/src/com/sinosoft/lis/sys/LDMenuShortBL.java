package com.sinosoft.lis.sys;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LDMenuShortBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  //private LDMenuShortSet mLDMenuShortSet ;

  //业务处理相关变量
  /** 个人信息 */
  private LDMenuShortSchema  mLDMenuShortSchema=new LDMenuShortSchema() ;

  public LDMenuShortBL() {
  }
  public static void main(String[] args) {
    LDMenuShortBL LDMenuShortBL1 = new LDMenuShortBL();
    LDMenuShortSchema mLDMenuShortSchema =new LDMenuShortSchema();
    VData tv=new VData();
    tv.add(mLDMenuShortSchema);
    LDMenuShortBL1.submitData(tv,"insert");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("OperateData:"+cOperate);
    //进行业务处理
    if (!dealData(cOperate))
      return false;
    
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
      
    System.out.println("Start LDPerson BL Submit...");

    LDMenuShortBLS tLDMenuShortBLS=new LDMenuShortBLS();
    tLDMenuShortBLS.submitData(mInputData,cOperate);

    System.out.println("End LDPerson BL Submit...");

    //如果有需要处理的错误，则返回
    if (tLDMenuShortBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tLDMenuShortBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData(String cOperate)
  {
    boolean tReturn =false;
    String tNo="";

    if (!getInputData())
      return false ;
    System.out.println("After getinputdata");
    System.out.println(cOperate);
    //处理个人信息数据
    //添加纪录
    if(cOperate.equals("insert"))
    {
    //添加个人信息表中的不能为空的字段(MakeDate,MakeTime,ModifyDate,ModifyTime)
    //入机日期,入机时间, 最后一次修改日期,最后一次修改时间
    //mLDPersonSchema.setMakeDate("1999/1/1");
    //mLDPersonSchema.setMakeTime("21:12:12");
    //mLDPersonSchema.setModifyDate("1999/1/1");
   // mLDPersonSchema.setModifyTime("21:12:12");
    tReturn=true;
    }
    //更新纪录
    if(cOperate.equals("update"))
    {
    //更新个人信息表中的不能为空的字段(MakeDate,MakeTime,ModifyDate,ModifyTime)
    //入机日期,入机时间, 最后一次修改日期,最后一次修改时间
    //mLDPersonSchema.setMakeDate("1999/1/1");
    //mLDPersonSchema.setMakeTime("21:12:12");
    
    //mLDPersonSchema.setModifyDate("2000/11/11");
    //mLDPersonSchema.setModifyTime("02:02:02");
    tReturn=true;
    }
    //删除纪录
    if(cOperate.equals("delete"))
    {
      //为删除操作处理数据
      System.out.println("delete is done!!!");
      tReturn=true;	
    }	
    
    return tReturn ;
  }

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  private boolean getInputData()
  {
    // 个人信息
    mLDMenuShortSchema   = (LDMenuShortSchema)mInputData.getObjectByObjectName("LDMenuShortSchema",0);
    if(mLDMenuShortSchema==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDMenuShortBL";
      tError.functionName="getInputData";
      tError.errorMessage="在个人信息接受时没有得到足够的数据，请您确认有：个人的完整信息!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  //准备往后层输出所需要的数据
  //输出：如果输入数据有错，则返回false,否则返回true
  private boolean checkInputData()
  {
    try
    {
//      mLDPersonSchema;

    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDPersonBL";
      tError.functionName="checkData";
      tError.errorMessage="在校验输入的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    try
    {    	
      mInputData.clear();
      mInputData.add(mLDMenuShortSchema);
      System.out.println("prepareOutputData:");      
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDMenuShortBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  
   //查询纪录的公共方法
 /*  public LDMenuShortSet queryData(VData cInputData)
   {   
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("LDPersonBL:QueryData");
    //进行业务处理
  
    //准备往后台的数据     
    
    System.out.println("Start LDPerson BL Query...");

    LDPersonBLS tLDPersonBLS=new LDPersonBLS();
    mLDPersonSet=(LDPersonSet)tLDPersonBLS.queryData(mInputData);

    System.out.println("End LDPerson BL Query...");

    //如果有需要处理的错误，则返回
    if (tLDPersonBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tLDPersonBLS.mErrors ) ;
        mLDPersonSet=null;
    }

    mInputData=null;
    return mLDPersonSet;   	
   }*/
  
}
