package com.sinosoft.lis.sys;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LDPersonBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private LDPersonSet mLDPersonSet;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();

    // 个人信息
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    private LCAddressSchema mLCAddressSchema = new LCAddressSchema();
    private LCAccountSchema mLCAccountSchema = new LCAccountSchema();
    public LDPersonBL() {
    }

    public static void main(String[] args) {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData()) {
            return false;
        }
        //检查数据合法性
        if (!checkInputData()) {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate)) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData() {
        // 个人信息
        mLDPersonSchema = (LDPersonSchema) mInputData.getObjectByObjectName(
                "LDPersonSchema", 0);
        mLCAddressSchema = (LCAddressSchema) mInputData.getObjectByObjectName(
                "LCAddressSchema", 0);
        mLCAccountSchema = (LCAccountSchema) mInputData.getObjectByObjectName(
                "LCAccountSchema", 0);
        if (mLDPersonSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在个人信息接受时没有得到足够的数据，请您确认有：个人的完整信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果输入数据有错，则返回false,否则返回true
    private boolean checkInputData() {
        try {
            if (this.checkLCAddress() == false) {
                return false;
            }

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "checkData";
            tError.errorMessage = "在校验输入的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate) {
        boolean tReturn = false;
        String tNo = "";
        //生成地址码,在添加用户或者更新用户时都可能生成
        if (mLCAddressSchema != null) {
            if (!StrTool.compareString(mLCAddressSchema.getPostalAddress(), "")
                && (mLCAddressSchema.getAddressNo() == null
                    || ("").equals(mLCAddressSchema.getAddressNo()))) {
                try {
                    SSRS tSSRS = new SSRS();
                    String sql = "Select Case When varchar(max(int(AddressNo))) Is Null Then '0' Else varchar(max(int(AddressNo))) End from LCAddress where CustomerNo='"
                                 + mLDPersonSchema.getCustomerNo() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                    int ttNo = firstinteger.intValue() + 1;
                    Integer integer = new Integer(ttNo);
                    tNo = integer.toString();
                    mLCAddressSchema.setAddressNo(tNo);
                } catch (Exception e) {
                    CError tError = new CError();
                    tError.moduleName = "LDPersonBL";
                    tError.functionName = "createAddressNo";
                    tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
                    this.mErrors.addOneError(tError);
                    mLCAddressSchema.setAddressNo("");
                }

            }
            mLCAddressSchema.setModifyDate(CurrentDate);
            mLCAddressSchema.setModifyTime(CurrentTime);
            mLCAddressSchema.setMakeDate(CurrentDate);
            mLCAddressSchema.setMakeTime(CurrentTime);
        }
        //添加纪录
        if (cOperate.equals("INSERT")) {
            //添加个人信息表中的不能为空的字段(MakeDate,MakeTime,ModifyDate,ModifyTime)
            //入机日期,入机时间, 最后一次修改日期,最后一次修改时间
            String tLimit = "";
            String CustomerNo = "";
            tLimit = "SN";
            CustomerNo = PubFun1.CreateMaxNo("CustomerNo", tLimit);
            mLDPersonSchema.setCustomerNo(CustomerNo);
            mLDPersonSchema.setMakeDate(CurrentDate);
            mLDPersonSchema.setMakeTime(CurrentTime);
            mLDPersonSchema.setModifyDate(CurrentDate);
            mLDPersonSchema.setModifyTime(CurrentTime);

            mLCAddressSchema.setCustomerNo(CustomerNo);

            mLCAccountSchema.setCustomerNo(CustomerNo);
            mLCAccountSchema.setMakeDate(CurrentDate);
            mLCAccountSchema.setMakeTime(CurrentTime);
            mLCAccountSchema.setModifyDate(CurrentDate);
            mLCAccountSchema.setModifyTime(CurrentTime);

            map.put(mLDPersonSchema, "INSERT");
            if (!tNo.equals("")) {
                map.put(mLCAddressSchema, "INSERT");
            }
            if (mLCAccountSchema.getAccKind().equals("Y")) {
                map.put(mLCAccountSchema, "INSERT");
            }
            tReturn = true;
        }
        //更新纪录
        if (cOperate.equals("UPDATE")) {
            //更新个人信息表中的不能为空的字段(MakeDate,MakeTime,ModifyDate,ModifyTime)
            //入机日期,入机时间, 最后一次修改日期,最后一次修改时间
            mLDPersonSchema.setMakeDate(CurrentDate);
            mLDPersonSchema.setMakeTime(CurrentTime);
            mLDPersonSchema.setModifyDate(CurrentDate);
            mLDPersonSchema.setModifyTime(CurrentTime);

            mLCAddressSchema.setModifyDate(CurrentDate);
            mLCAddressSchema.setModifyTime(CurrentTime);
            mLCAddressSchema.setMakeDate(CurrentDate);
            mLCAddressSchema.setMakeTime(CurrentTime);

            mLCAccountSchema.setMakeDate(CurrentDate);
            mLCAccountSchema.setMakeTime(CurrentTime);
            mLCAccountSchema.setModifyDate(CurrentDate);
            mLCAccountSchema.setModifyTime(CurrentTime);

            map.put(mLDPersonSchema, "UPDATE");
            if (tNo.equals("") && mLCAddressSchema != null &&
                !StrTool.compareString(mLCAddressSchema.getPostalAddress(), "")) {
                map.put(mLCAddressSchema, "UPDATE");
            } else if (!tNo.equals("")) {
                map.put(mLCAddressSchema, "INSERT");
            }
            String tStr = "Select * from LCAccount where CustomerNo = '" +
                          mLDPersonSchema.getCustomerNo() + "'";
            LCAccountDB tLCAccountDB = new LCAccountDB();
            LCAccountSet tLCAccountSet = new LCAccountSet();
            tLCAccountSet = tLCAccountDB.executeQuery(tStr);
            if (mLCAccountSchema.getAccKind().equals("Y")) {
                if (tLCAccountSet.size() == 0) {
                    map.put(mLCAccountSchema, "INSERT");
                } else {
                    map.put(mLCAccountSchema, "UPDATE");
                }
            }
            tReturn = true;
        }
        //删除纪录
        if (cOperate.equals("DELETE")) {
            map.put(mLDPersonSchema, "DELETE");
            map.put("delete from LCAddress where CustomerNo='" +
                    mLDPersonSchema.getCustomerNo() + "'", "DELETE");
            map.put(mLCAccountSchema, "DELETE");
            tReturn = true;
        }

        return tReturn;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
            mResult.add(mLDPersonSchema);
            mResult.add(mLCAddressSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    // 检查地址数据是否正确
    //如果在处理过程中出错或者数据有错误，则返回false,否则返回true
    private boolean checkLCAddress() {

        if (mLCAddressSchema != null) {
            if (mLCAddressSchema.getPostalAddress() != null) {
                mLCAddressSchema.setPostalAddress(mLCAddressSchema.
                                                  getPostalAddress().
                                                  trim());
                if (mLCAddressSchema.getZipCode() != null) {
                    mLCAddressSchema.setZipCode(mLCAddressSchema.getZipCode().
                                                trim());
                }
                if (mLCAddressSchema.getPhone() != null) {
                    mLCAddressSchema.setPhone(mLCAddressSchema.getPhone().trim());
                }

            }
            if (mLCAddressSchema.getAddressNo() != null) {
                if (!mLCAddressSchema.getAddressNo().equals("")) {
                    LCAddressDB tLCAddressDB = new LCAddressDB();

                    tLCAddressDB.setAddressNo(mLCAddressSchema.getAddressNo());
                    tLCAddressDB.setCustomerNo(mLCAddressSchema.getCustomerNo());
                    if (tLCAddressDB.getInfo() == false) {
                        CError tError = new CError();
                        tError.moduleName = "LDPersonBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage = "数据库查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (!StrTool.compareString(mLCAddressSchema.getCustomerNo(),
                                               tLCAddressDB.getCustomerNo())
                        || !StrTool.compareString(mLCAddressSchema.getAddressNo(),
                                                  tLCAddressDB.getAddressNo())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getPostalAddress(),
                                                  tLCAddressDB.getPostalAddress())
                        || !StrTool.compareString(mLCAddressSchema.getZipCode(),
                                                  tLCAddressDB.getZipCode())
                        || !StrTool.compareString(mLCAddressSchema.getPhone(),
                                                  tLCAddressDB.getPhone())
                        || !StrTool.compareString(mLCAddressSchema.getFax(),
                                                  tLCAddressDB.getFax())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getHomeAddress(),
                                                  tLCAddressDB.getHomeAddress())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getHomeZipCode(),
                                                  tLCAddressDB.getHomeZipCode())
                        || !StrTool.compareString(mLCAddressSchema.getHomePhone(),
                                                  tLCAddressDB.getHomePhone())
                        || !StrTool.compareString(mLCAddressSchema.getHomeFax(),
                                                  tLCAddressDB.getHomeFax())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyAddress(),
                                                  tLCAddressDB.
                                                  getCompanyAddress())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyZipCode(),
                                                  tLCAddressDB.
                                                  getCompanyZipCode())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyPhone(),
                                                  tLCAddressDB.getCompanyPhone())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getCompanyFax(),
                                                  tLCAddressDB.getCompanyFax())
                        || !StrTool.compareString(mLCAddressSchema.getMobile(),
                                                  tLCAddressDB.getMobile())
                        || !StrTool.compareString(mLCAddressSchema.getMobileChs(),
                                                  tLCAddressDB.getMobileChs())
                        || !StrTool.compareString(mLCAddressSchema.getEMail(),
                                                  tLCAddressDB.getEMail())
                        || !StrTool.compareString(mLCAddressSchema.getBP(),
                                                  tLCAddressDB.getBP())
                        || !StrTool.compareString(mLCAddressSchema.getMobile2(),
                                                  tLCAddressDB.getMobile2())
                        || !StrTool.compareString(mLCAddressSchema.
                                                  getMobileChs2(),
                                                  tLCAddressDB.getMobileChs2())
                        || !StrTool.compareString(mLCAddressSchema.getEMail2(),
                                                  tLCAddressDB.getEMail2())
                        || !StrTool.compareString(mLCAddressSchema.getBP2(),
                                                  tLCAddressDB.getBP2())
                            ) {
                        CError tError = new CError();
                        tError.moduleName = "LDPersonBL";
                        tError.functionName = "checkAddress";
                        tError.errorMessage =
                                "您输入的地址信息与数据库里对应的信息不符，请去掉地址号，重新生成!";
                        this.mErrors.addOneError(tError);

                        return false;

                    }

                }
            }

        }

        return true;
    }

    //得到结果
    public VData getResult() {
        return this.mResult;
    }


}
