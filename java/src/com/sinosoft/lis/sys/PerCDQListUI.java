package com.sinosoft.lis.sys;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author miaoxz
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PerCDQListUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public PerCDQListUI()
    {
        System.out.println("PerCDQListUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	PerCDQListBL tPerCDQListBL = new PerCDQListBL();
        if(!tPerCDQListBL.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(tPerCDQListBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
}
