package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: 催办控制 </p>
 * <p>Description: 更新催办控制时间</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: SinoSoft</p>
 * @author zhangxing
 * @version 1.0
 */

public class ContrlUrgeNoticeUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public ContrlUrgeNoticeUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ComCode = "86";
  tG.ManageCom = "86";

  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("PrtSeq","81000000392");
  tTransferData.setNameAndValue("UrgeDate","2006-01-10");
  VData tVData = new VData();
  tVData.add( tTransferData );
  tVData.add( tG );
  ContrlUrgeNoticeUI ui = new ContrlUrgeNoticeUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    ContrlUrgeNoticeBL tContrlUrgeNoticeBL = new ContrlUrgeNoticeBL();

    System.out.println("---UWAutoHealthBL UI BEGIN---");
    if (tContrlUrgeNoticeBL.submitData(cInputData,mOperate) == false)
        {
                  // @@错误处理
      this.mErrors.copyAllErrors(tContrlUrgeNoticeBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoHealthUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
        }
    return true;
  }
}
