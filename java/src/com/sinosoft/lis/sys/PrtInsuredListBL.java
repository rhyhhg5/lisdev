package com.sinosoft.lis.sys;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtInsuredListBL
{
    public static final String TemplateName = "TemplateName";
    public static final Object SQL = "SQL";

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;
    private String mGrpContNo = null;

    private ExeSQL mExeSQL = new ExeSQL();

    private String mTemplatName = null;
    private String mSql = null;

    private TextTag mTextTag = null;
    private XmlExport mXmlExport = new XmlExport();

    public PrtInsuredListBL()
    {
    }

    /**
     * 生成清单数据
     * @param cInputData VData：需包含TransferData对象和GlobalInput对象
     * @param cOperator String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String cOperator)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return mXmlExport;
    }

    /**
     * dealData
     * 生成清单数据
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        mXmlExport.createDocument(mTemplatName, "printer");

        if(!dealVariable())
        {
            return false;
        }

        if(!dealList())
        {
            return false;
        }

        mXmlExport.outputDocumentToFile("D:\\", "PrtContList");

        return true;
    }

    /**
     * dealList
     *生成打印列表数据
     * @return boolean
     */
    private boolean dealList()
    {
    	//RSWrapper rsWrapper = new RSWrapper();
        System.out.println("\n\ndealList \n" + mSql);
        SSRS tSSRS = new SSRS();
        tSSRS = mExeSQL.execSQL(mSql);
        System.out.println("mSql是什么："+mSql);
     //   if (!rsWrapper.prepareData(null, mSql))
        	
        if (tSSRS ==null)
        {
            System.out.println("处理数据准备失败! ");
            return false;
        }
       // SSRS tSSRS = rsWrapper.getSSRS();
    		
    		 ListTable tListTable = new ListTable();
    	        tListTable.setName("List");
    		 for(int row = 1; row <= tSSRS.getMaxRow(); row++)
	    	{
    			 String[] info = new String[tSSRS.getMaxCol() + 1];

    	            info[0] = "" + row;

    	            for(int col = 1; col < info.length; col++)
    	            {
    	                info[col] = StrTool.cTrim(tSSRS.GetText(row, col));
    	            }

    	            String sql = "select sum(a) from ( "
    	                         + "   select sum(SumActuPayMoney) a "
    	                         + "   from LJAPayPerson a, LCPol b "
    	                         + "   where a.PolNo = b.PolNo  "
    	                         + "     and a.CurPayToDate = b.PayToDate "
    	                         + "     and b.GrpContNo = '" + mGrpContNo + "' "
    	                         + "     and b.InsuredNo = '" + info[1] + "' "
    	                         + "   union "
    	                         + "   select sum(SumActuPayMoney) a "
    	                         + "   from LJAPayPerson a, LBPol b "
    	                         + "   where a.PolNo = b.PolNo  "
    	                         + "     and a.CurPayToDate = b.PayToDate "
    	                         + "     and b.GrpContNo = '" + mGrpContNo + "' "
    	                         + "     and b.InsuredNo = '" + info[1] + "' "
    	                         + ") t ";
    	            String rs = mExeSQL.getOneValue(sql);
    	            if(!"".equals(rs) && !"null".equals(rs))
    	            {
    	                info[12] = rs;
    	            }
    	            tListTable.add(info);
	    	}
    	//rsWrapper.close();
        

        mXmlExport.addListTable(tListTable, new String[tSSRS.getMaxRow()]);

        return true;
    }

    /**
     * dealVariable
     *
     * @return boolean
     */
    private boolean dealVariable()
    {
        if(mTextTag == null)
        {
            mTextTag = new TextTag();
        }

        mXmlExport.addTextTag(mTextTag);

        return true;
    }

    /**
     * 从VData中将传入的数据获取到类变量
     * @param cData VData
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData cData)
    {
        mGI = (GlobalInput) cData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) cData
                        .getObjectByObjectName("TransferData", 0);
        mTextTag = (TextTag) cData.getObjectByObjectName("TextTag", 0);

        if(mGI == null || mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtCoreBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的数据不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mTemplatName = (String)mTransferData.getValueByName(TemplateName);
        if(mTemplatName == null || mTemplatName.equals(""))
        {
           CError tError = new CError();
           tError.moduleName = "PrtCoreBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "请传入模版名";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
        }

        mSql = (String)mTransferData.getValueByName(SQL);
        if(mSql == null || mSql.equals(""))
        {
           CError tError = new CError();
           tError.moduleName = "PrtCoreBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "请传入查询清单的Sql";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
        }

        mGrpContNo = (String)mTransferData.getValueByName("GrpContNo");
        if(mGrpContNo == null || mGrpContNo.equals(""))
        {
           CError tError = new CError();
           tError.moduleName = "PrtCoreBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "没有读到保单号";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
        }


        return true;
    }

    public static void main(String[] args)
    {
        PrtInsuredListBL prtcorebl = new PrtInsuredListBL();

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "endor0";
        tGI.ComCode = "86";
        tGI.ManageCom = tGI.ComCode;

        TransferData tTransferData = new TransferData();

        String templateName = "PrtContListVali.vts";
    //    String templateName = "PrtContListInVali.vts";
        String sql = "select a.InsuredNo, a.Name,    (select InsuredName from LCInsured where ContNo =a.ContNo and RelationToMainInsured = '00'),    CodeName('relation',a.RelationToMainInsured), CodeName('sex', a.Sex), a.Birthday,    CodeName('idtype',a.IDType), a.IDNo, CodeName('occupationtype',a.OccupationType),    a.ContPlanCode, (select sum(Prem) from LCPol where ContNo = a.ContNo and InsuredNo = a.InsuredNo), '', b.CValiDate, CInvaliDate,    (select nvl(sum(InsuAccBala), 0) from LCInsureAcc where ContNo = a.ContNo and InsuredNo = a.InsuredNo),    a.BankAccNo from LCInsured a, LCCont b where a.ContNo = b.ContNo    and b.StateFlag in('2', '3')    and a.GrpContNo = '0000112601'  union select a.InsuredNo, a.Name,    (select Name from LCInsured where ContNo = a.ContNo and RelationToMainInsured = '00' union select Name from LBInsured where ContNo = a.ContNo and RelationToMainInsured = '00'),    CodeName('relation',a.RelationToMainInsured), CodeName('sex', a.Sex), a.Birthday,    CodeName('idtype',a.IDType), a.IDNo, CodeName('occupationtype',a.OccupationType),    a.ContPlanCode, sum(b.Prem), '', min(b.CValiDate),    (select EdorValiDate from LPGrpEdorItem where EdorNo = a.EdorNo and EdorType in('CT','XT','WT','ZT')),    (select nvl(sum(InsuAccBala), 0) from LBInsureAcc where ContNo = a.ContNo and InsuredNo = a.InsuredNo),    a.BankAccNo from LBInsured a, LBPol b where a.ContNo = b.ContNo    and a.GrpContNo = '0000112601' group by a.InsuredNo, a.Name, a.RelationToMainInsured, a.Sex, a.Birthday, a.IDType,         a.IDNo,a.OccupationType, a.ContNo, a.ContPlanCode , a.EdorNo, a.BankAccNo  order by InsuredNo with ur ";

        tTransferData.setNameAndValue(PrtCoreBL.TemplateName, templateName);
        tTransferData.setNameAndValue(PrtCoreBL.SQL, sql);
  tTransferData.setNameAndValue("GrpContNo", "0000112601");

        TextTag tTextTag = new TextTag();
//        tTextTag.add("ContNo", request.getParameter("ContNo"));
//        tTextTag.add("PrtNo", request.getParameter("PrtNo"));
        tTextTag.add("StateFlag", "1");
        tTextTag.add("StateFlagName", "承保有效");
        tTextTag.add("ContKind", "11");
        tTextTag.add("ContKindName", "个险一年期保单");

        VData d = new VData();
        d.add(tGI);
        d.add(tTransferData);
        d.add(tTextTag);

        PrtInsuredListUI ui = new PrtInsuredListUI();
        XmlExport txmlExport = ui.getXmlExport(d, "");
        if(txmlExport == null)
        {
            System.out.println(ui.mErrors);
        }
    }
}
