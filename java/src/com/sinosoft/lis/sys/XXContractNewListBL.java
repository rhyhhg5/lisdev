package com.sinosoft.lis.sys;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import com.sinosoft.lis.schema.*;
import java.io.*;

public class XXContractNewListBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String tSysPath = null;
    private FileWriter mFileWriter ;
    private BufferedWriter mBufferedWriter ;
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    private VData mResult = new VData();
    public XXContractNewListBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        //ExeSQL tExeSQL = new ExeSQL();
//        System.out.println("BL->dealDate()");
//        System.out.println(mSql);
//        System.out.println(mOutXmlPath);
        //SSRS tSSRS = tExeSQL.execSQL(mSql);

//        if(tExeSQL.mErrors.needDealError())
//        {
//            System.out.println(tExeSQL.mErrors.getErrContent());
//
//            CError tError = new CError();
//            tError.moduleName = "LAStandardRCDownloadBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = "没有查询到需要下载的数据";
//            mErrors.addOneError(tError);
//            System.out.println(tError.errorMessage);
//            return false;
//        }
        tSysPath +="sys/";
        String tPath="";
        try{
             tPath = "XinDanMingXi"+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
             mFileWriter = new FileWriter(tSysPath+tPath+".txt");
             mBufferedWriter = new BufferedWriter(mFileWriter);
       String[] mToExcel = new String[15];

        mToExcel[0] = "管理机构";
        mToExcel[1] = "产品代码";
        mToExcel[2] = "保单号";
        mToExcel[3] = "生效日期";
        mToExcel[4] = "失效日期";
        mToExcel[5] = "增减人日期";
        mToExcel[6] = "签单日期";
        mToExcel[7] = "投保人";
        mToExcel[8] = "投保人客户号";
        mToExcel[9] = "被保险人";
        mToExcel[10] = "保额";
        mToExcel[11] = "标准保费";
        mToExcel[12] = "实收保费";
        mToExcel[13] = "折扣率";
        mToExcel[14] = "年度保费";
        String head = "";
        for(int m=0;m<mToExcel.length;m++){
                    head += mToExcel[m]+"|";
        }
        mBufferedWriter.write(head+"\r\n");
        mBufferedWriter.flush();
    }catch(Exception ex1){
        ex1.printStackTrace();
    }

    int start = 1;
    int nCount = 10000;
      while (true) {
           SSRS tSSRS = new ExeSQL().execSQL(mSql, start, nCount);
           int count=0;
           if (tSSRS.getMaxRow() <= 0) {
                break;
            }else{
                count=tSSRS.getMaxRow();
            }
          if (tSSRS != null && count > 0) {
            for (int i = 1; i <= count; i++) {
              try{
                  String result="";
                  for(int m=1;m<=tSSRS.getMaxCol();m++){
                        result += tSSRS.GetText(i,m)+"|";
                  }
                   mBufferedWriter.write(result+"\r\n");
                   mBufferedWriter.flush();
               } catch (IOException ex2) {
                      ex2.printStackTrace();
                }
               }
           }
           start += nCount;
         }
       try {
          mBufferedWriter.close();
        } catch (IOException ex3) {
        }
        String[] FilePaths = new String[1];
        FilePaths[0] = tSysPath+tPath+".txt";
        String[] FileNames = new String[1];
        FileNames[0] =tPath+".txt";
        String newPath = tSysPath +tPath+".zip";
        String FullPath = tPath+".zip";
        CreateZip(FilePaths,FileNames,newPath);
        try{
            File fd = new File(FilePaths[0]);
            fd.delete();
        }catch(Exception ex4){
            ex4.printStackTrace();
        }
      mResult.add(FullPath);
      return true;
    }
    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
            String tZipPath) {
    	ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
    	if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
    		System.out.println("生成压缩文件失败");
    		CError.buildErr(this, "生成压缩文件失败");
    		return false;
    	}
    	return true;
    }
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);
        mResult.clear();
        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySqlnew");
        tSysPath = (String) tf.getValueByName("SysPath");
        //mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || tSysPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
    public static void main(String[] args)
    {
        
    }
}
