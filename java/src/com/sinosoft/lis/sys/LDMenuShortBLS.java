package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LDMenuShortBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  //private LDMenuShortSet mLDMenuShortSet ;

  public LDMenuShortBLS() {
  }
  public static void main(String[] args) {
    LDMenuShortBLS proposalBLS1 = new LDMenuShortBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start LDPerson BLS Submit...");
    //执行添加纪录活动
    if(cOperate.equals("insert"))
    {
    tReturn=save();
    if (tReturn)
      System.out.println("Save sucessful");
    else
        System.out.println("Save failed") ;
    }
    //执行更新纪录活动
    if(cOperate.equals("update"))
    {
    tReturn=update();
    if (tReturn)
      System.out.println("Update sucessful");
    else
        System.out.println("Update failed") ;
    }
    //执行删除纪录活动
    if(cOperate.equals("delete"))
    {
    tReturn=delete();
    if (tReturn)
      System.out.println("Delete sucessful");
    else
        System.out.println("Delete failed") ;
    }

    System.out.println("End LDPerson BLS Submit...");

    //如果有需要处理的错误，则返回
//    if (tLDPersonBLS.mErrors .needDealError() )
//        this.mErrors .copyAllErrors(tLDPersonBLS.mErrors ) ;

    mInputData=null;
    return tReturn;
  }
  private boolean save()
  {
    boolean tReturn =true;
      System.out.println("Start Save...");
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");
      LDMenuShortSchema tLDMenuShortSchema   = (LDMenuShortSchema)mInputData.getObjectByObjectName("com.sinosoft.lis.schema.LDMenuShortSchema",0);
      LDMenuShortDB tLDMenuShortDB = new LDMenuShortDB();
      tLDMenuShortDB.setSchema(tLDMenuShortSchema);
      tLDMenuShortDB.insert() ;
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDMenuShortBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      tReturn=false;
    }
    return tReturn;
  }

  private boolean update()
  {
    boolean tReturn =true;
      System.out.println("Start Update...");
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");
      LDMenuShortSchema tLDMenuShortSchema   = (LDMenuShortSchema)mInputData.getObjectByObjectName("com.sinosoft.lis.schema.LDMenuShortSchema",0);
      LDMenuShortDB tLDMenuShortDB=new LDMenuShortDB();
      tLDMenuShortDB.setSchema(tLDMenuShortSchema);
      tLDMenuShortDB.update() ;
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDMenuShortBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      tReturn=false;
    }
    return tReturn;
  }

  private boolean delete()
  {
    boolean tReturn =true;
      System.out.println("Start Delete...");
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");
      LDMenuShortSchema tLDMenuShortSchema   = (LDMenuShortSchema)mInputData.getObjectByObjectName("com.sinosoft.lis.schema.LDMenuShortSchema",0);
      LDMenuShortDB tLDMenuShortDB=new LDMenuShortDB();
      tLDMenuShortDB.setSchema(tLDMenuShortSchema);
      tLDMenuShortDB.delete() ;
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDMenuShortBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      tReturn=false;
    }
    return tReturn;
  }

   //查询纪录的公共方法
/*   public LDPersonSet queryData(VData cInputData)
   {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("Start LDPerson BLS Query...");
    //执行查询纪录活动
    System.out.println("Start Query...");
    try{
      JdbcUrl tJdbcUrl=new JdbcUrl();
      /** 个人信息 */
/*      System.out.println("Start 个人信息...");
      LDPersonSchema tLDPersonSchema   = (LDPersonSchema)mInputData.getObjectByObjectName("com.sinosoft.lis.schema.LDPersonSchema",0);
System.out.println("1");
      LDPersonDB tLDPersonDB=new LDPersonDB(tJdbcUrl);
System.out.println("2");
System.out.println("tLDPersonSchema="+tLDPersonSchema);
      tLDPersonDB.setSchema(tLDPersonSchema);
System.out.println("3");
      mLDPersonSet=(LDPersonSet)tLDPersonDB.query() ;
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDPersonBLS";
      tError.functionName="QueryData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      mLDPersonSet=null;
    }
    mInputData=null;
    return mLDPersonSet;
   }*/

}