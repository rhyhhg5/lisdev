package com.sinosoft.lis.sys;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

public class LDBlackListBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private LDBlackListSet mLDBlackListSet;
    private GlobalInput tG = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String tBlacklistType;
    //业务处理相关变量
    /** 黑名单信息 */
    private LDBlackListSchema mLDBlackListSchema = new LDBlackListSchema();
    private LDBlackListSchema delLDBlackListSchema = new LDBlackListSchema();
    private LDBlackListReasonDetailSet mLDBlackListReasonDetailSet = new
            LDBlackListReasonDetailSet();
    private LDBlackListReasonDetailSet saveLDBlackListReasonDetailSet = new
            LDBlackListReasonDetailSet();
    private LDBlackListReasonDetailSet delLDBlackListReasonDetailSet = new
            LDBlackListReasonDetailSet();
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    private LDGrpSchema mLDGrpSchema = new LDGrpSchema();
    private MMap tmpMap = new MMap();

    public LDBlackListBL() {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        System.out.println("OperateData:" + cOperate);
        //进行业务处理
        if (dealData(cOperate) == false) {
            return false;
        }
        System.out.println("dealData true:");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("Start LDBlackList BL Submit...");

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mInputData, null)) {
            CError.buildErr(this, "数据保存失败");
            return false;
        }
//    LDBlackListBLS tLDBlackListBLS=new LDBlackListBLS();
//    tLDBlackListBLS.submitData(mInputData,cOperate);
//
//    System.out.println("End LDBlackList BL Submit...");
//
//    //如果有需要处理的错误，则返回
//    if (tLDBlackListBLS.mErrors .needDealError())
//    {
//        this.mErrors .copyAllErrors(tLDBlackListBLS.mErrors ) ;
//    }

        mInputData = null;
        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate) {
        boolean tReturn = false;

        if (!getInputData()) {
            return false;
        }
        System.out.println("After getinputdata");
        System.out.println(cOperate);
        //处理个人信息数据
        //添加纪录
        if (cOperate.equals("INSERT")) {
            LDBlackListDB tLDBlackListDB = new LDBlackListDB();
            LDBlackListSet tLDBlackListSet = new LDBlackListSet();
            tLDBlackListDB.setBlackListNo(mLDBlackListSchema.getBlackListNo());
            tLDBlackListDB.setBlackName(mLDBlackListSchema.getBlackName());
            tLDBlackListDB.setBlackListSource(mLDBlackListSchema.
                                              getBlackListSource());
            tLDBlackListDB.setRelaOtherNo(mLDBlackListSchema.getRelaOtherNo());
            tLDBlackListDB.setRelaOtherNoType(mLDBlackListSchema.
                                              getRelaOtherNoType());
            //tLDBlackListDB.setRelaContNo(mLDBlackListSchema.getRelaContNo());
            tLDBlackListSet = tLDBlackListDB.query();
            if (tLDBlackListSet.size() > 0) {
                CError tError = new CError();
                tError.moduleName = "LDBlackListBL";
                tError.functionName = "dealData";
                tError.errorMessage = "黑名单中已有该客户的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tLimit = PubFun.getNoLimit(this.tG.ManageCom);
            String aBlackSerial = PubFun1.CreateMaxNo("BlackSerial", tLimit);
            mLDBlackListSchema.setBlackListSerialNo(aBlackSerial);
            mLDBlackListSchema.setBlackListOperator(tG.Operator);
            mLDBlackListSchema.setBlackListMakeDate(CurrentDate);
            mLDBlackListSchema.setMakeDate(CurrentDate);
            mLDBlackListSchema.setMakeTime(CurrentTime);

            mLDBlackListSchema.setModifyDate(CurrentDate);
            mLDBlackListSchema.setModifyTime(CurrentTime);
            tBlacklistType = mLDBlackListSchema.getBlackListType();
            if (tBlacklistType.equals("0")) {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(mLDBlackListSchema.getBlackListNo());
                if (!tLDPersonDB.getInfo()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDBlackListBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有与此黑名单信息对应的个人信息，请您确认：黑名单信息的正确!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tLDPersonDB.setBlacklistFlag("1");
                mLDPersonSchema.setSchema(tLDPersonDB);
            }

            if (tBlacklistType.equals("3")) {
                LDGrpDB tLDGrpDB = new LDGrpDB();
//        tLDGrpDB.setGrpNo(mLDBlackListSchema.getBlacklistNo());
                if (!tLDGrpDB.getInfo()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDBlackListBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有与此黑名单信息对应的集体信息，请您确认：黑名单信息的正确!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tLDGrpDB.setBlacklistFlag("1");
                mLDGrpSchema.setSchema(tLDGrpDB);
            }
            if (mLDBlackListReasonDetailSet != null) {
                for (int i = 1; i <= mLDBlackListReasonDetailSet.size(); i++) {
                    LDBlackListReasonDetailSchema
                            tLDBlackListReasonDetailSchema =
                                    mLDBlackListReasonDetailSet.get(i);
                    tLDBlackListReasonDetailSchema.setBlackListSerialNo(
                            aBlackSerial);
                    tLDBlackListReasonDetailSchema.setBlackListKind(
                            mLDBlackListSchema.getBlackListType());
                    tLDBlackListReasonDetailSchema.setRelaContNo(
                            mLDBlackListSchema.
                            getRelaContNo());
                    tLDBlackListReasonDetailSchema.setRelaOtherNo(
                            mLDBlackListSchema.getRelaOtherNo());
                    tLDBlackListReasonDetailSchema.setRelaPolNo(
                            mLDBlackListSchema.
                            getRelaPolNo());
                    tLDBlackListReasonDetailSchema.setRelaOtherNoType(
                            mLDBlackListSchema.getRelaOtherNoType());
                    saveLDBlackListReasonDetailSet.add(
                            tLDBlackListReasonDetailSchema);
                }
            }
            mLDBlackListSchema.setBlackListReason(
                    saveLDBlackListReasonDetailSet.
                    get(1).getBlackListReasonDetail());
            mInputData.clear();
            tmpMap.put(mLDBlackListSchema, "INSERT");
            tmpMap.put(saveLDBlackListReasonDetailSet, "INSERT");
            mResult.add(mLDBlackListSchema);
            if (tBlacklistType != null) {
                if (tBlacklistType.equals("0")) {
                    tmpMap.put(mLDPersonSchema, "UPDATE");
                    mResult.add(mLDPersonSchema);
                }
                if (tBlacklistType.equals("3")) {
                    tmpMap.put(mLDGrpSchema, "UPDATE");
                    mResult.add(mLDGrpSchema);
                }
            }
            mInputData.add(tmpMap);
            tReturn = true;
        }

        if (cOperate.equals("UPDATE")) {
            LDBlackListDB tsLDBlackListDB = new LDBlackListDB();
            LDBlackListSet tLDBlackListSet = new LDBlackListSet();
            tsLDBlackListDB.setBlackListNo(mLDBlackListSchema.getBlackListNo());
            tsLDBlackListDB.setBlackName(mLDBlackListSchema.getBlackName());
            tsLDBlackListDB.setBlackListSource(mLDBlackListSchema.
                                               getBlackListSource());
            tsLDBlackListDB.setRelaOtherNo(mLDBlackListSchema.getRelaOtherNo());
            tsLDBlackListDB.setRelaOtherNoType(mLDBlackListSchema.
                                               getRelaOtherNoType());

            tLDBlackListSet = tsLDBlackListDB.query();
            if (tLDBlackListSet.size() <= 0) {
                CError tError = new CError();
                tError.moduleName = "LDBlackListBL";
                tError.functionName = "dealData";
                tError.errorMessage = "黑名单中尚无该客户的信息，请先按增加黑名单按钮！";
                this.mErrors.addOneError(tError);
                return false;
            }

            delLDBlackListSchema = tLDBlackListSet.get(1);

            if (!tG.Operator.equals(delLDBlackListSchema.getBlackListOperator())) {
                CError tError = new CError();
                tError.moduleName = "LDBlackListBL";
                tError.functionName = "dealData";
                tError.errorMessage = "您无权限对黑名单进行修改！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LDBlackListReasonDetailDB tsLDBlackListReasonDetailDB = new
                    LDBlackListReasonDetailDB();
            tsLDBlackListReasonDetailDB.setBlackListSerialNo(
                    delLDBlackListSchema.getBlackListSerialNo());
            delLDBlackListReasonDetailSet = tsLDBlackListReasonDetailDB.query();
            String tLimit = PubFun.getNoLimit(this.tG.ManageCom);
            String aBlackSerial = PubFun1.CreateMaxNo("BlackSerial", tLimit);
            mLDBlackListSchema.setBlackListSerialNo(aBlackSerial);
            mLDBlackListSchema.setBlackListOperator(tG.Operator);
            mLDBlackListSchema.setMakeDate(CurrentDate);
            mLDBlackListSchema.setMakeTime(CurrentTime);

            mLDBlackListSchema.setModifyDate(CurrentDate);
            mLDBlackListSchema.setModifyTime(CurrentTime);
            tBlacklistType = mLDBlackListSchema.getBlackListType();
            if (tBlacklistType.equals("0")) {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(mLDBlackListSchema.getBlackListNo());
                if (!tLDPersonDB.getInfo()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDBlackListBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有与此黑名单信息对应的个人信息，请您确认：黑名单信息的正确!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tLDPersonDB.setBlacklistFlag("1");
                mLDPersonSchema.setSchema(tLDPersonDB);
            }

            if (tBlacklistType.equals("3")) {
                LDGrpDB tLDGrpDB = new LDGrpDB();
//        tLDGrpDB.setGrpNo(mLDBlackListSchema.getBlacklistNo());
                if (!tLDGrpDB.getInfo()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDBlackListBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有与此黑名单信息对应的集体信息，请您确认：黑名单信息的正确!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tLDGrpDB.setBlacklistFlag("1");
                mLDGrpSchema.setSchema(tLDGrpDB);
            }
            if (mLDBlackListReasonDetailSet != null) {
                for (int i = 1; i <= mLDBlackListReasonDetailSet.size(); i++) {
                    LDBlackListReasonDetailSchema
                            tLDBlackListReasonDetailSchema =
                                    mLDBlackListReasonDetailSet.get(i);
                    tLDBlackListReasonDetailSchema.setBlackListSerialNo(
                            aBlackSerial);
                    tLDBlackListReasonDetailSchema.setBlackListKind(
                            mLDBlackListSchema.getBlackListType());
                    tLDBlackListReasonDetailSchema.setRelaContNo(
                            mLDBlackListSchema.
                            getRelaContNo());
                    tLDBlackListReasonDetailSchema.setRelaOtherNo(
                            mLDBlackListSchema.getRelaOtherNo());
                    tLDBlackListReasonDetailSchema.setRelaPolNo(
                            mLDBlackListSchema.
                            getRelaPolNo());
                    tLDBlackListReasonDetailSchema.setRelaOtherNoType(
                            mLDBlackListSchema.getRelaOtherNoType());
                    saveLDBlackListReasonDetailSet.add(
                            tLDBlackListReasonDetailSchema);
                }
            }
            mLDBlackListSchema.setBlackListReason(
                    saveLDBlackListReasonDetailSet.
                    get(1).getBlackListReasonDetail());
            mInputData.clear();
            tmpMap.put(mLDBlackListSchema, "INSERT");
            tmpMap.put(saveLDBlackListReasonDetailSet, "INSERT");
            tmpMap.put(delLDBlackListSchema, "DELETE");
            tmpMap.put(delLDBlackListReasonDetailSet, "DELETE");
            mResult.add(mLDBlackListSchema);
            if (tBlacklistType != null) {
                if (tBlacklistType.equals("0")) {
                    tmpMap.put(mLDPersonSchema, "UPDATE");
                    mResult.add(mLDPersonSchema);
                }
                if (tBlacklistType.equals("3")) {
                    tmpMap.put(mLDGrpSchema, "UPDATE");
                    mResult.add(mLDGrpSchema);
                }
            }
            mInputData.add(tmpMap);
            tReturn = true;
        }
        if (cOperate.equals("DELETE")) {
            LDBlackListDB tLDBlackListDB = new LDBlackListDB();
            LDBlackListSet tsLDBlackListSet = new LDBlackListSet();
            tLDBlackListDB.setBlackListNo(mLDBlackListSchema.getBlackListNo());
            tLDBlackListDB.setBlackName(mLDBlackListSchema.getBlackName());
            tsLDBlackListSet = tLDBlackListDB.query();

            LDBlackListDB tsLDBlackListDB = new LDBlackListDB();
            LDBlackListSet tLDBlackListSet = new LDBlackListSet();
            tsLDBlackListDB.setBlackListNo(mLDBlackListSchema.getBlackListNo());
            tsLDBlackListDB.setBlackName(mLDBlackListSchema.getBlackName());
            tsLDBlackListDB.setBlackListSource(mLDBlackListSchema.
                                               getBlackListSource());
            tsLDBlackListDB.setRelaOtherNo(mLDBlackListSchema.getRelaOtherNo());
            tsLDBlackListDB.setRelaOtherNoType(mLDBlackListSchema.
                                               getRelaOtherNoType());

            tLDBlackListSet = tsLDBlackListDB.query();
            if (tLDBlackListSet.size() <= 0) {
                CError tError = new CError();
                tError.moduleName = "LDBlackListBL";
                tError.functionName = "dealData";
                tError.errorMessage = "黑名单中尚无该客户的信息，请先按增加黑名单按钮！";
                this.mErrors.addOneError(tError);
                return false;
            }

            delLDBlackListSchema = tLDBlackListSet.get(1);

            if (!tG.Operator.equals(delLDBlackListSchema.getBlackListOperator())) {
                CError tError = new CError();
                tError.moduleName = "LDBlackListBL";
                tError.functionName = "dealData";
                tError.errorMessage = "您无权限对黑名单进行删除！";
                this.mErrors.addOneError(tError);
                return false;
            }

            LDBlackListReasonDetailDB tsLDBlackListReasonDetailDB = new
                    LDBlackListReasonDetailDB();
            tsLDBlackListReasonDetailDB.setBlackListSerialNo(
                    delLDBlackListSchema.getBlackListSerialNo());
            delLDBlackListReasonDetailSet = tsLDBlackListReasonDetailDB.query();
            tBlacklistType = mLDBlackListSchema.getBlackListType();
            if (delLDBlackListSchema.getBlackListType().equals("0")) {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                tLDPersonDB.setCustomerNo(mLDBlackListSchema.getBlackListNo());
                if (!tLDPersonDB.getInfo()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDBlackListBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有与此黑名单信息对应的个人信息，请您确认：黑名单信息的正确!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tsLDBlackListSet.size() == 1) {
                    tLDPersonDB.setBlacklistFlag("0");
                }
                mLDPersonSchema.setSchema(tLDPersonDB);
            }

            if (delLDBlackListSchema.getBlackListType().equals("3")) {
                LDGrpDB tLDGrpDB = new LDGrpDB();
                if (!tLDGrpDB.getInfo()) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDBlackListBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "没有与此黑名单信息对应的集体信息，请您确认：黑名单信息的正确!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tsLDBlackListSet.size() == 1) {
                    tLDGrpDB.setBlacklistFlag("0");
                }
                mLDGrpSchema.setSchema(tLDGrpDB);
            }
            mInputData.clear();
            tmpMap.put(delLDBlackListSchema, "DELETE");
            tmpMap.put(delLDBlackListReasonDetailSet, "DELETE");
            if (tBlacklistType != null) {
                if (tBlacklistType.equals("0")) {
                    tmpMap.put(mLDPersonSchema, "UPDATE");
                    mResult.add(mLDPersonSchema);
                }
                if (tBlacklistType.equals("3")) {
                    tmpMap.put(mLDGrpSchema, "UPDATE");
                    mResult.add(mLDGrpSchema);
                }
            }
            mInputData.add(tmpMap);
            tReturn = true;
        }
        return tReturn;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData() {
        // 个人信息
        mLDBlackListSchema = (LDBlackListSchema) mInputData.
                             getObjectByObjectName("LDBlackListSchema", 0);
        mLDBlackListReasonDetailSet = (LDBlackListReasonDetailSet) mInputData.
                                      getObjectByObjectName(
                                              "LDBlackListReasonDetailSet", 0);
        tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        if (mLDBlackListSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlackListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在黑名单信息接受时没有得到足够的数据，请您确认有：黑名单的完整信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果输入数据有错，则返回false,否则返回true
    private boolean checkInputData() {
        try {
//      mLDBlackListSchema;

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlackListBL";
            tError.functionName = "checkData";
            tError.errorMessage = "在校验输入的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        try {
            System.out.println("prepareOutputData:");
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlackListBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //查询纪录的公共方法
    public LDBlackListSet queryData(VData cInputData) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        System.out.println("LDBlackListBL:QueryData");
        //进行业务处理

        //准备往后台的数据

        System.out.println("Start LDBlackList BL Query...");

        LDBlackListBLS tLDBlackListBLS = new LDBlackListBLS();
        mLDBlackListSet = (LDBlackListSet) tLDBlackListBLS.queryData(mInputData);

        System.out.println("End LDBlackList BL Query...");

        //如果有需要处理的错误，则返回
        if (tLDBlackListBLS.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLDBlackListBLS.mErrors);
            mLDBlackListSet = null;
        }

        mInputData = null;
        return mLDBlackListSet;
    }

    public static void main(String[] args) {
        LDBlackListBL LDBlackListBL1 = new LDBlackListBL();
    }
}
