package com.sinosoft.lis.sys;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class YHDNewPeopleUI {

	// 错误处理类 ，每个错误处理的类都放置到该类当中
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	// 向后端传输数据需要的容器
	private VData mInputData = new VData();
	// 数据操作的字符串命令
	private String mOperate;

	// 业务相关处理的变量
	public YHDNewPeopleUI() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		this.mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();
		YHDNewPeopleBL tYHDNewPeopleBL = new YHDNewPeopleBL();
		try {
			if (!tYHDNewPeopleBL.submitData(mInputData, mOperate)) {
				this.mErrors.copyAllErrors(tYHDNewPeopleBL.mErrors);
				CError tError = new CError();
				tError.moduleName = "YHDNewPeopleUI";
				tError.functionName = "submitDat";
				tError.errorMessage = "BL类处理失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mResult = tYHDNewPeopleBL.getResult();
		return true;
	}

	public VData getResult() {
		return mResult;
	}
}
