package com.sinosoft.lis.sys;

import com.sinosoft.lis.db.NewPeopleTaskTableDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.NewPeopleTaskTableSchema;
import com.sinosoft.lis.vschema.NewPeopleTaskTableSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class YHDNewPeopleBL {

	// 错误处理类 ，每个错误处理的类都放置到该类当中
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	// 向后端传输数据需要的容器
	private VData mInputData = new VData();
	// 数据操作的字符串命令
	private String mOperate;

	// 全局数据
	private GlobalInput mGlobalInput = new GlobalInput();
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();

	// 录入
	private NewPeopleTaskTableSchema mNewPeopleTaskTableScahma = new NewPeopleTaskTableSchema();
	private NewPeopleTaskTableSchema mupNewPeopleTaskTableScahma = new NewPeopleTaskTableSchema();

	MMap mmap = new MMap();

	public YHDNewPeopleBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部数据，将数据拷贝到类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YHDNewPeopleBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败YHDNewPeopleBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		// 往后层传输数据失败
		if (!prepareOutputData()) {
			return false;
		}

		if (this.mOperate.equals("QUERY||MAIN")) {
			this.submitQuery();
		} else {
			PubSubmit tPubSubmit = new PubSubmit();
			System.out.println("start YHDNewPeopleBL submit");
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "YHDNewPeopleBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mInputData = null;
		return true;
	}

	/**
	 * 输入VData数据，得到所有对象
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		this.mNewPeopleTaskTableScahma
				.setSchema((NewPeopleTaskTableSchema) cInputData
						.getObjectByObjectName("NewPeopleTaskTableSchema", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		return true;
	}

	private boolean dealData() {

		if (mOperate.equals("INSERT||MAIN")) {
			mNewPeopleTaskTableScahma.setOperator(mGlobalInput.Operator);
			mNewPeopleTaskTableScahma.setMakeDate(CurrentDate);
			mNewPeopleTaskTableScahma.setMakeTime(CurrentTime);
			mNewPeopleTaskTableScahma.setModifyDate(CurrentDate);
			mNewPeopleTaskTableScahma.setModifyTime(CurrentTime);
			mmap.put(this.mNewPeopleTaskTableScahma, "INSERT");
		} else if (mOperate.equals("DELETE||MAIN")) {
			NewPeopleTaskTableSet mNewPeopleTaskTableSet = new NewPeopleTaskTableSet();
			NewPeopleTaskTableDB tNewPeopleTaskTableDB = new NewPeopleTaskTableDB();
			tNewPeopleTaskTableDB.setSerialNo(mNewPeopleTaskTableScahma
					.getSerialNo());
			mNewPeopleTaskTableSet = tNewPeopleTaskTableDB.query();
			if (mNewPeopleTaskTableSet == null
					|| mNewPeopleTaskTableSet.size() == 0) {
				this.mErrors.copyAllErrors(tNewPeopleTaskTableDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "YHDNewPeopleBL";
				tError.functionName = "dealData";
				tError.errorMessage = "录入编号为"
						+ mNewPeopleTaskTableScahma.getSerialNo()
						+ "的信息不存在，无法删除！";
				this.mErrors.addOneError(tError);
				return false;
			} else {
				mmap.put(mNewPeopleTaskTableSet, "DELETE");
			}
		} else if (mOperate.equals("UPDATE||MAIN")) {
			NewPeopleTaskTableDB tNewPeopleTaskTableDB = new NewPeopleTaskTableDB();
			tNewPeopleTaskTableDB.setSerialNo(mNewPeopleTaskTableScahma
					.getSerialNo());
			if (!tNewPeopleTaskTableDB.getInfo()) {
				CError tError = new CError();
				tError.moduleName = "YHDNewPeopleBL";
				tError.functionName = "dealData";
				tError.errorMessage = "录入编号为"
						+ mNewPeopleTaskTableScahma.getSerialNo()
						+ "出错，需查询信息后修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			mupNewPeopleTaskTableScahma = tNewPeopleTaskTableDB.getSchema();
			mupNewPeopleTaskTableScahma.setName(mNewPeopleTaskTableScahma
					.getName());
			mupNewPeopleTaskTableScahma.setBirthday(mNewPeopleTaskTableScahma
					.getBirthday());
			mupNewPeopleTaskTableScahma.setIDNo(mNewPeopleTaskTableScahma
					.getIDNo());
			mupNewPeopleTaskTableScahma.setIDType(mNewPeopleTaskTableScahma
					.getIDType());
			mupNewPeopleTaskTableScahma.setSex(mNewPeopleTaskTableScahma
					.getSex());
			mupNewPeopleTaskTableScahma.setRemark(mNewPeopleTaskTableScahma
					.getRemark());
			mupNewPeopleTaskTableScahma.setModifyDate(CurrentDate);
			mupNewPeopleTaskTableScahma.setModifyTime(CurrentTime);
			mmap.put(this.mupNewPeopleTaskTableScahma, "UPDATE");
		}
		return true;
	}

	/**
	 * 准备数据
	 * 
	 * @return
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mmap);
		} catch (Exception e) {
			// 错误处理
			CError tError = new CError();
			tError.errorMessage = "YHDNewPeopleBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "准备往后层传输的数据出错";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public boolean submitQuery() {
		return true;
	}

	/**
	 * 返回一个空VData容器
	 * 
	 * @return
	 */
	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
