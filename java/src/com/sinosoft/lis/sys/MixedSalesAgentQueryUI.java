package com.sinosoft.lis.sys;

import com.cbsws.obj.RspSaleInfo;


public class MixedSalesAgentQueryUI {
	DealQueryAgentInfoBL dealQueryAgentInfoBL = new DealQueryAgentInfoBL();
	RspSaleInfo rspSaleInfo = new RspSaleInfo();
	
	public RspSaleInfo getSaleInfo(String prtNo,String manageCom){
		rspSaleInfo = dealQueryAgentInfoBL.queryAgentInfo(prtNo,manageCom);
		return rspSaleInfo;
	}
	
	
}
