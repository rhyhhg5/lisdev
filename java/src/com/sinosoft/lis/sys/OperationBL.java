/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: </p>
 * @author  孙伟超
 * @version 1.0
 */

package com.sinosoft.lis.sys;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

import examples.newsgroups;


public class OperationBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    //private String mBlackListCode;

    //录入
    private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
    private LJTempFeeClassSchema mLjTempFeeClassSchema = new LJTempFeeClassSchema();
    
    private MMap mMap = new MMap();


    public OperationBL() {
    }



    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperationBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OperationBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start OperationBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OperationBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    ////////////////////////////////////////////////
    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mLJTempFeeSchema.setSchema((LJTempFeeSchema) cInputData.
                                            getObjectByObjectName(
                "LJTempFeeSchema", 0));
        
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        return true;

    }

////////////////////////////////////////////////////////
    private boolean dealData() {
    	//yuan
        	LJTempFeeSet srcLJTempFeeSet = new LJTempFeeSet();
        	LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
        	LJTempFeeSet updateLJTempFeeSet = new LJTempFeeSet();
        	LJTempFeeDB srcLJTempFeeDB = new LJTempFeeDB();
        	LJTempFeeDB updateLJTempFeeDB = new LJTempFeeDB();
        	
        	LJTempFeeClassSet srcljTempFeeClassSet = new LJTempFeeClassSet();
        	LJTempFeeClassSet mLJTempFeeClassSet =  new LJTempFeeClassSet();
        	LJTempFeeClassSet updateLJTempFeeClassSet = new LJTempFeeClassSet();
        	LJTempFeeClassDB srcLjTempFeeClassDB =  new LJTempFeeClassDB();
        	LJTempFeeClassDB updateLjTempFeeClassDB = new LJTempFeeClassDB();
        	
        	
        	srcLJTempFeeDB.setTempFeeNo(mLJTempFeeSchema.getTempFeeNo());
        	srcLjTempFeeClassDB.setTempFeeNo(mLJTempFeeSchema.getTempFeeNo());
        	
        	
        	//前台选中的所有数据
        	srcLJTempFeeSet = srcLJTempFeeDB.query();
        	srcljTempFeeClassSet = srcLjTempFeeClassDB.query();
        	
        	System.out.println("11111111:" + srcLJTempFeeSet.get(1).getTempFeeNo());
        	System.out.println("22222222:" + srcljTempFeeClassSet.get(1).getTempFeeNo());
        	
        	mLJTempFeeSchema  = srcLJTempFeeSet.get(1);
        	mLjTempFeeClassSchema = srcljTempFeeClassSet.get(1);
        	
        	System.out.println("/////"+mLjTempFeeClassSchema.getPayMoney());
        	
        	System.out.println("srcLJTempFeeSet="+srcLJTempFeeSet.size());
        	System.out.println("srcljTempFeeClassSet="+srcljTempFeeClassSet.get(1).getPayMoney());
            mLJTempFeeSet = srcLJTempFeeSet;
            mLJTempFeeClassSet = srcljTempFeeClassSet;
            //更新
            updateLJTempFeeSet  = updateLJTempFeeDB.executeQuery("select * from ljtempfee where otherno ='"+mLJTempFeeSchema.getOtherNo()+"' and tempfeeno <>'"+mLJTempFeeSchema.getTempFeeNo()+ "'");
            System.out.println("原****:"+mLJTempFeeSchema.getTempFeeNo()+"===="+mLJTempFeeSchema.getPayMoney());
            System.out.println("update****"+updateLJTempFeeSet.get(1));
            System.out.println("最終："+mLJTempFeeSchema.getPayMoney() + updateLJTempFeeSet.get(1).getPayMoney());
            updateLJTempFeeSet.get(1).setPayMoney(mLJTempFeeSchema.getPayMoney() + updateLJTempFeeSet.get(1).getPayMoney());
            updateLJTempFeeSet.get(1).setModifyDate(CurrentDate);
            updateLJTempFeeSet.get(1).setModifyTime(CurrentTime);
            
            updateLJTempFeeClassSet = updateLjTempFeeClassDB.executeQuery("select * from LJtempfeeclass where tempfeeno ='"+updateLJTempFeeSet.get(1).getTempFeeNo()+ "'");
            System.out.println("+++++++" + updateLJTempFeeClassSet.get(1).getPayMoney());
            updateLJTempFeeClassSet.get(1).setPayMoney(mLjTempFeeClassSchema.getPayMoney() + updateLJTempFeeClassSet.get(1).getPayMoney());
            updateLJTempFeeClassSet.get(1).setModifyDate(CurrentDate);
            updateLJTempFeeClassSet.get(1).setModifyTime(CurrentTime);
            
            mMap.put(srcLJTempFeeSet, "DELETE");
            mMap.put(updateLJTempFeeSet, "UPDATE");
            
            mMap.put(srcljTempFeeClassSet, "DELETE");
            mMap.put(updateLJTempFeeClassSet, "UPDATE");
            return true;

    }
////////////////////////////////////////////////////////

        private boolean submitquery() {
            return true;
        }
////////////////////////////////////////////////////////

        /**
         * 准备后台的数据
         * @return boolean
         */
        private boolean prepareOutputData() {
            try {
                mInputData = new VData();
                this.mInputData.add(mMap);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "OperationBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }

//////////////////////////////////////////////

        public VData getResult() {
            this.mResult.clear();
            return mResult;
        }


}
