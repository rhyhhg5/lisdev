/*
 * <p>ClassName: CodeBL </p>
 * <p>Description: LDCodeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
package com.sinosoft.lis.sys;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class CodeBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
 /** 往后面传输数据的容器 */
 private VData mInputData= new VData();
 /** 全局数据 */
 //private GlobalInput mGlobalInput =new GlobalInput() ;
 /** 数据操作字符串 */
 private String mOperate;
/** 业务处理相关变量 */
 private LDCodeSchema mLDCodeSchema=new LDCodeSchema();
 //private LDCodeSet mLDCodeSet=new LDCodeSet();
 public CodeBL() {
 }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
       //将操作数据拷贝到本类中
       this.mOperate =cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
            return false;
     //进行业务处理
       //进行插入数据
       if (mOperate.equals("INSERT||CODE"))
         {if (!dealData())
           {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "CodeBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据处理失败CodeBL-->dealData!";
             this.mErrors .addOneError(tError) ;
             return false;
           }
         }
         //对数据进行修改操作
         if(mOperate.equals("UPDATE||CODE"))
         {
           if(!updateData())
           {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "CodeBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据处理失败CodeBL-->updateData!";
             this.mErrors .addOneError(tError) ;
             return false;
           }
           System.out.println("---updateData---");
         }
         //对数据进行删除操作
         if (mOperate.equals("DELETE||CODE"))
         {
           if (!deleteData())
           {
             // @@错误处理
               CError tError = new CError();
               tError.moduleName = "CodeBL";
               tError.functionName = "submitData";
               tError.errorMessage = "数据处理失败CodeBL-->deleteData!";
               this.mErrors .addOneError(tError) ;
               return false;
           }
           System.out.println("----deleteData---");
         }
     //准备往后台的数据
     if (!prepareOutputData())
        return false;
     if (this.mOperate.equals("QUERY||MAIN"))
     {
            this.submitquery();
     }
     else
    {
        System.out.println("Start CodeBL Submit...");
        CodeBLS tCodeBLS=new CodeBLS();
        tCodeBLS.submitData(mInputData,mOperate);
        System.out.println("End CodeBL Submit...");
        //如果有需要处理的错误，则返回
        if (tCodeBLS.mErrors.needDealError())
        {
		// @@错误处理
		this.mErrors.copyAllErrors(tCodeBLS.mErrors);
		CError tError = new CError();
		tError.moduleName = "CodeBL";
		tError.functionName = "submitDat";
		tError.errorMessage ="数据提交失败!";
		this.mErrors .addOneError(tError) ;
		return false;
         }
    }
        mInputData=null;
        return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    return true;
}



/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}

/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  LDCodeSchema tLDCodeSchema = new LDCodeSchema();
  tLDCodeSchema.setCodeType(mLDCodeSchema.getCodeType());
  tLDCodeSchema.setCode(mLDCodeSchema.getCode());
  mLDCodeSchema.setSchema(tLDCodeSchema);
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLDCodeSchema.setSchema((LDCodeSchema)cInputData.getObjectByObjectName("LDCodeSchema",0));
         //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
           this.mResult.clear();
           System.out.println("Start LDCodeBLQuery Submit...");
 	        LDCodeDB tLDCodeDB=new LDCodeDB();
	        tLDCodeDB.setSchema(this.mLDCodeSchema);
	        //this.mLDCodeSet=tLDCodeDB.query();
	        //this.mResult.add(this.mLDCodeSet);
		System.out.println("End LDCodeBLQuery Submit...");
		//如果有需要处理的错误，则返回
		if (tLDCodeDB.mErrors.needDealError())
 		{
		// @@错误处理
  			this.mErrors.copyAllErrors(tLDCodeDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LDCodeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
}
                   mInputData=null;
                   return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		//this.mInputData=new VData();
		//this.mInputData.add(this.mGlobalInput);
		this.mInputData.clear();
		this.mInputData.add(this.mLDCodeSchema);
		mResult.clear();
                mResult.add(this.mLDCodeSchema);
	}
	catch(Exception ex)
	{
 		 // @@错误处理
		CError tError =new CError();
 		tError.moduleName="LDCodeBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  		return this.mResult;
	}
}
