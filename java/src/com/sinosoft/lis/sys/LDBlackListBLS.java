package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

public class LDBlackListBLS {

  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  private LDBlackListSet mLDBlackListSet ;
  private String tBlacklistType ;
  public LDBlackListBLS() {
  }
  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start LDBlackList BLS Submit...");
    //执行添加纪录活动
    if(cOperate.equals("INSERT"))
    {
      tReturn=save();
      if (tReturn)
        System.out.println("Save sucessful");
      else
        System.out.println("Save failed") ;
    }
    //执行更新纪录活动
    if(cOperate.equals("UPDATE"))
    {
      tReturn=update();
      if (tReturn)
        System.out.println("Update sucessful");
      else
        System.out.println("Update failed") ;
    }
    //执行删除纪录活动
    if(cOperate.equals("DELETE"))
    {
      tReturn=delete();
      if (tReturn)
        System.out.println("Delete sucessful");
      else
        System.out.println("Delete failed") ;
    }

    System.out.println("End LDBlackList BLS Submit...");

    //如果有需要处理的错误，则返回
//    if (tLDBlackListBLS.mErrors .needDealError() )
//        this.mErrors .copyAllErrors(tLDBlackListBLS.mErrors ) ;

    mInputData=null;
    return tReturn;
  }
  private boolean save()
  {
    System.out.println("Start Save...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDBlackListBLS";
      tError.functionName = "save";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try{
      /** 黑名单信息 */
      conn.setAutoCommit(false);
      System.out.println("Start 黑名单信息...");

      LDBlackListDB tLDBlackListDB=new LDBlackListDB(conn);
      tLDBlackListDB.setSchema((LDBlackListSchema)mInputData.getObjectByObjectName("LDBlackListSchema",0));
      tBlacklistType = tLDBlackListDB.getBlackListType();
      if(!tLDBlackListDB.insert())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLDBlackListDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "LDBlackListBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "黑名单信息表数据保存失败!";
        this.mErrors .addOneError(tError) ;
        try{
          conn.rollback();
          conn.close();
          } catch(Exception e){}
          return false;
      }
      LDBlackListReasonDetailDBSet tLDBlackListReasonDetailDBSet = new LDBlackListReasonDetailDBSet(conn);
      tLDBlackListReasonDetailDBSet.set((LDBlackListReasonDetailSet)mInputData.getObjectByObjectName("LDBlackListReasonDetailSet",0));
      if(!tLDBlackListReasonDetailDBSet.insert())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLDBlackListReasonDetailDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "LDBlackListBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "黑名单信息表数据保存失败!";
        this.mErrors .addOneError(tError) ;
        try{
          conn.rollback();
          conn.close();
          } catch(Exception e){}
          return false;
      }

      if (tBlacklistType.equals("0"))
      {
        LDPersonDB tLDPersonDB=new LDPersonDB(conn);
        tLDPersonDB.setSchema((LDPersonSchema)mInputData.getObjectByObjectName("LDPersonSchema",0));
        if(!tLDPersonDB.update())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "LDBlackListBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "个人客户信息表数据修改失败!";
          this.mErrors .addOneError(tError) ;
          try{
            conn.rollback();
            conn.close();
            } catch(Exception e){}
            return false;
        }
      }

      if (tBlacklistType.equals("1"))
     {
       LDGrpDB tLDGrpDB=new LDGrpDB(conn);
       tLDGrpDB.setSchema((LDGrpSchema)mInputData.getObjectByObjectName("LDGrpSchema",0));
       if(!tLDGrpDB.update())
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLDGrpDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "LDBlackListBLS";
         tError.functionName = "saveData";
         tError.errorMessage = "集体客户信息表数据修改失败!";
         this.mErrors .addOneError(tError) ;
         try{
           conn.rollback();
           conn.close();
           } catch(Exception e){}
           return false;
       }
      }
      conn.commit();
      conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDBlackListBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      try{
            conn.rollback() ;
            conn.close();
          }
      catch(Exception e){}
      return false;
    }
    return true;
  }

  private boolean update()
  {
    System.out.println("Start Update...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDBlackListBLS";
      tError.functionName = "update";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try{
      /** 黑名单信息 */
      System.out.println("Start 黑名单信息...");
      LDBlackListDB tLDBlackListDB=new LDBlackListDB(conn);
      tLDBlackListDB.setSchema((LDBlackListSchema)mInputData.getObjectByObjectName("LDBlackListSchema",0));

      if(!tLDBlackListDB.update())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLDBlackListDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "LDBlackListBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "黑名单信息表数据更新失败!";
        this.mErrors .addOneError(tError) ;
        try{
          conn.close();
          } catch(Exception e){}
          return false;
      }
      conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDBlackListBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  private boolean delete()
  {
    System.out.println("Start Delete...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDBlackListBLS";
      tError.functionName = "delete";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try{
      /** 黑名单信息 */
      System.out.println("Start 黑名单信息...");
      LDBlackListDB tLDBlackListDB=new LDBlackListDB(conn);
      tLDBlackListDB.setSchema((LDBlackListSchema)mInputData.getObjectByObjectName("LDBlackListSchema",0));

      if(!tLDBlackListDB.delete())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLDBlackListDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "LDBlackListBLS";
        tError.functionName = "delData";
        tError.errorMessage = "黑名单信息表数据删除失败!";
        this.mErrors .addOneError(tError) ;
        try{
          conn.close();
          } catch(Exception e){}
          return false;
      }
      conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDBlackListBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //查询纪录的公共方法
  public LDBlackListSet queryData(VData cInputData)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("Start LDBlackList BLS Query...");
    //执行查询纪录活动
    System.out.println("Start Query...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LDBlackListBLS";
      tError.functionName = "queryData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return null;
    }
    try{
      /** 黑名单信息 */
      System.out.println("Start 黑名单信息...");

      LDBlackListDB tLDBlackListDB=new LDBlackListDB(conn);
      tLDBlackListDB.setSchema((LDBlackListSchema)mInputData.getObjectByObjectName("LDBlackListSchema",0));

      mLDBlackListSet=(LDBlackListSet)tLDBlackListDB.query() ;
      try{
        conn.close();
        } catch(Exception e){}
    }
    catch (Exception ex)
    {
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDBlackListBLS";
      tError.functionName="QueryData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      mLDBlackListSet=null;
    }
    mInputData=null;
    return mLDBlackListSet;
  }

  public static void main(String[] args) {
    LDBlackListBLS LDBlackListBLS1 = new LDBlackListBLS();
  }
}
