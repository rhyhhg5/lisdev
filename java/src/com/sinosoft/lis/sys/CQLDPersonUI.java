package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:承保暂交费功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class CQLDPersonUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public CQLDPersonUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    CQLDPersonBL tCQLDPersonBL = new CQLDPersonBL();

    System.out.println("---UI BEGIN---");
    if (tCQLDPersonBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tCQLDPersonBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "CQLDPersonUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
    }
    else
      mResult = tCQLDPersonBL.getResult();
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

  public static void main(String[] args)
  {
    CQLDPersonUI tCQLDPersonUI=new CQLDPersonUI();
    VData tVData=new VData();
    LDPersonSchema tLDPersonSchema=new LDPersonSchema();
    tVData.addElement(tLDPersonSchema);
    tCQLDPersonUI.submitData(tVData,"QUERY");
  }




}