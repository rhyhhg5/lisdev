package com.sinosoft.lis.sys;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:个人信息类（界面输入）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LDPersonUI
{

    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    private LDPersonSet mLDPersonSet;
    public LDPersonUI()
    {
    }

    public static void main(String[] args)
    {
    }
    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        LDPersonBL tLDPersonBL = new LDPersonBL();
        tLDPersonBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tLDPersonBL.mErrors.needDealError())
        this.mErrors.copyAllErrors(tLDPersonBL.mErrors);
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLDPersonBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
