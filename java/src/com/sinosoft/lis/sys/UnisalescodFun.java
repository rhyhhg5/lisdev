package com.sinosoft.lis.sys;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.cbsws.obj.RspUniSalescod;
import com.cbsws.obj.UniSalesAppayInfo;
import com.sinosoft.lis.sys.DealUniSalescodBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;

public class UnisalescodFun {
	 /**
     * 从集团获取统一工号：Unisalescod
     * 
     */
	private DealUniSalescodBL dealUniSalescodBL=new DealUniSalescodBL();
	private RspUniSalescod mRspUniSalescod = new RspUniSalescod();
	private ExeSQL tExeSQL1=new ExeSQL();
    public RspUniSalescod getUnisalescod(String mOperate,LAAgentSchema mLAAgentSchema,LADimissionSchema tLADimissionSchema,String mGroupAgentCode,String mQualifNo){
    	String date1 = PubFun.getCurrentDate2();
    	String time = PubFun.getCurrentTime();
    	String manOrgCod = mLAAgentSchema.getManageCom();  //管理机构
    	String APPID = "H00001";  //系统id
    	String TIMESTAMP = date1+" "+time;  //请求时间
    	String MESSAGETYPE = "";  //消息类型
    	String uniSalesCod = "";  //统一编码
    	String procode = manOrgCod.substring(2, 4);
    	//modify by zhuxt 20140927 获取省份名称
//    	String SQL3="select showname from ldcom where length(trim(comcode))=4 and substr(comcode,3,2)='"+procode+"'";
    	String SQL3 = "select case when locate('杭州',a.name) >= 1 then '杭州' when locate('广州',a.name) >= 1 then '广州' " 
    			      +"else (select b.showname from ldcom b where b.comcode = substr(a.comcode,1,4)) end " 
    			      +"from ldcom a where a.comcode = '" + manOrgCod.trim() + "'";
    	String proname = tExeSQL1.getOneValue(SQL3);
    	if("91".equals(procode)){
    		procode="98";
    	}else if("92".equals(procode)){
    		procode="95";
    	}else if("93".equals(procode)){
    		procode="97";
    	}else if("94".equals(procode)){
    		procode="96";
    	}else if("95".equals(procode)){
    		procode="99";
    	}else if("86330100".equals(manOrgCod)||"86330101".equals(manOrgCod)||"863301".equals(manOrgCod)){
    		procode="94";
    	}else if("86440100".equals(manOrgCod)||"864401".equals(manOrgCod)){
    		procode="93";
    	}
    	String SID = "2" + procode + "0001" +date1 + PubFun1.CreateMaxNo("SIDCode", 7);  //序列号
    	if(mOperate.equals("INSERT||MAIN")&&tLADimissionSchema == null){
    		MESSAGETYPE = "01"; 
        	uniSalesCod = ""; 
    	}else {
    		MESSAGETYPE = "02"; 
        	uniSalesCod = mGroupAgentCode; 
        	if("".equals(uniSalesCod)||uniSalesCod==null){
        		uniSalesCod = "";
        	}
    	}
    	String compCod = "000085";  //子公司代码
    	String compNam = "中国人民健康保险股份有限公司";  //子公司名称
    	String salesNam = mLAAgentSchema.getName().toString();  //姓名
    	if(!StrTool.isGBKString(salesNam)){
    		salesNam=StrTool.unicodeToGBK(salesNam);
    	}
    	System.out.println("66666666666666666666666"+salesNam);
//    	if (tLADimissionSchema == null) {
//    		
//    		try {
//    			salesNam = mLAAgentSchema.getName().toString();
//    			salesNam=new String(salesNam.getBytes("ISO-8859-1"),"GBK");
//    			System.out.println("66666666666666666666666"+salesNam);
//    		} catch (UnsupportedEncodingException e) {
//    			// TODO Auto-generated catch block
//    			e.printStackTrace();
//    		}
//    	}else{
//    		salesNam = mLAAgentSchema.getName().toString();
//    		System.out.println("66666666666666666666666"+salesNam);
//    	}
    	String dateBirthday = mLAAgentSchema.getBirthday().replaceAll("-", "");  //生日
    	if(dateBirthday==null||"".equals(dateBirthday)){
    		dateBirthday="20000101";
    	}
    	String sexCod ;  //性别
    	if("0".equals( mLAAgentSchema.getSex())){
    		sexCod="1";
    	}else if("1".equals( mLAAgentSchema.getSex())){
    		sexCod="2";
    	}else{
    		sexCod="9";
    	}
//    	ExeSQL tExeSQL1=new ExeSQL();
  		String SQL="select name from ldcom where comcode ='"+manOrgCod+"'";
  		String manOrgNam=tExeSQL1.getOneValue(SQL);
    	String idtypCod ;  //证件类型
    	String idtypNam ;
    	if("0".equals(mLAAgentSchema.getIDNoType())){
    		idtypCod="11";
    		idtypNam="居民身份证";
    	}else if("1".equals(mLAAgentSchema.getIDNoType())){
    		idtypCod="17";
    		idtypNam="中国护照";
    	}else if("2".equals(mLAAgentSchema.getIDNoType())){
    		idtypCod="14";
    		idtypNam="军官证";
    	}else{
    		idtypCod="99";
    		idtypNam="其它证件";
    	}
    	String idNo = mLAAgentSchema.getIDNo();  //证件号码
    	if(idNo==null||"".equals(idNo)){
    		idNo = "000000000000000000";
    	}
//    	modify by zhuxt 20140927 为空的字段不再填充'N'
    	String salesTel = mLAAgentSchema.getPhone();  //电话
    	if("".equals(salesTel)||salesTel==null){
    		salesTel = "";
    	}
    	String salesMob = mLAAgentSchema.getMobile();  //手机
    	if("".equals(salesMob)||salesMob==null){
    		salesMob = "11111111111";
    	}
    	String salesMail = mLAAgentSchema.getEMail();  //邮箱
    	if("".equals(salesMail)||salesMail==null){
    		salesMail = "";
    	}
    	String salesAddr = mLAAgentSchema.getHomeAddress();  //地址
    	if (salesAddr == null || "".equals(salesAddr)) {
//			salesAddr = "N";
			salesAddr = "";
		} else {
			if (!StrTool.isGBKString(salesAddr)) {
				salesAddr = StrTool.unicodeToGBK(salesAddr);
			}

		}
    	System.out.println("66666666666666666666666"+salesAddr);
//    	if(salesAddr==null||"".equals(salesAddr)){
//    			salesAddr="N";
//    	}else{
//    		if (tLADimissionSchema == null) {
//    			try {
//    				salesAddr=new String(salesAddr.getBytes("ISO-8859-1"),"GBK");
//    				System.out.println("66666666666666666666666"+salesAddr);
//    			} catch (UnsupportedEncodingException e) {
//    				// TODO Auto-generated catch block
//    				e.printStackTrace();
//    			}
//    		}
//    	}
    	String educationCod ;  //学历
    	String educationNam ;
    	if("0".equals(mLAAgentSchema.getDegree())){
    		educationCod="20";
    		educationNam="博士";
    	}else if("1".equals(mLAAgentSchema.getDegree())){
    		educationCod="30";
    		educationNam="硕士";
    	}else if("2".equals(mLAAgentSchema.getDegree())){
    		educationCod="40";
    		educationNam="本科";
    	}else if("3".equals(mLAAgentSchema.getDegree())){
    		educationCod="50";
    		educationNam="专科";
    	}else if("4".equals(mLAAgentSchema.getDegree())){
    		educationCod="60";
    		educationNam="中专";
    	}else if("5".equals(mLAAgentSchema.getDegree())){
    		educationCod="70";
    		educationNam="高中";
    	}else if("6".equals(mLAAgentSchema.getDegree())){
    		educationCod="80";
    		educationNam="初中";
    	}else{
    		educationCod="99";
    		educationNam="文盲";
    	}
    	String qualifiNo = "";
    	if(mOperate.equals("INSERT||MAIN")&&!"".equals(mQualifNo)){
    		qualifiNo = mQualifNo;
    	}else{
    		String tsql = "select QualifNo from  LAQualification where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
    		qualifiNo = tExeSQL1.getOneValue(tsql); //资格证书编号
    	}
    	if("".equals(qualifiNo)||qualifiNo==null){
    		qualifiNo = "";
    	}
    	String certifiNo = "";  //执业证书编号
    	if(mOperate.equals("UPDATE||MAIN")){
    		String tsql = "select CertifNo from  LACertification where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
    		certifiNo = tExeSQL1.getOneValue(tsql); //
    	}
    	if("".equals(certifiNo)||certifiNo==null){
    		certifiNo = "";
    	}
    	String contractNo = "";  //委托代理合同编号
    	String statusCod = "1";  //在职状态
    	String status = "在职";
    	String salesTypCod = mLAAgentSchema.getAgentType(); //销售人员类型名称
    	String salesTyp = "";
    	String BranchType2 = mLAAgentSchema.getBranchType2();  
    	String BranchType = mLAAgentSchema.getBranchType(); 
    	if("1".equals(salesTypCod)){
    		salesTyp = "营销人员";
    		if("1".equals(BranchType)&&"01".equals(BranchType2)){
    			contractNo=mLAAgentSchema.getAgentCode();
    		}else{
    			contractNo=mLAAgentSchema.getRetainContNo();
    		}
    		if("".equals(contractNo)||contractNo==null){
    			contractNo="";
    		}
    			
    	}else if("2".equals(salesTypCod)){
    		salesTyp = "直销人员";
    		contractNo="";
    	}else{
    		salesTypCod="2";
    		salesTyp = "直销人员";
    		contractNo="";
    	}
    	String isCrosssale = "1";  //是否允许开展交叉销售业务
    	String sql1= "select codename from  ldcode where codetype = 'branchtype' and code='"+BranchType+"'";
    	String BranchTypeName = tExeSQL1.getOneValue(sql1);
    	String sql2= "select codename from  ldcode where codetype = 'branchtype2' and code='"+BranchType2+"'";
    	String BranchType2Name = tExeSQL1.getOneValue(sql2);
    	String channelCod = BranchType + BranchType2;  //销售渠道编码
    	String salechaNam = BranchTypeName + BranchType2Name;  //销售渠道名称
    	String entrantDate = mLAAgentSchema.getEmployDate().replaceAll("-", "");  //入职时间
    	if("".equals(entrantDate)||entrantDate==null){
    		entrantDate = "20000101";
    	}
    	String dimissionDate = "";  //离司时间
    	String demissionReason = "";  //离职原因
    	if (tLADimissionSchema != null) {
			dimissionDate = tLADimissionSchema.getDepartDate().replaceAll("-", ""); // 离司时间
			String DepartRsn = tLADimissionSchema.getDepartRsn(); // 离职原因
			if ("0".equals(DepartRsn)) {
				demissionReason = "考核";
			} else if ("1".equals(DepartRsn)) {
				demissionReason = "辞聘";
			} else if ("2".equals(DepartRsn)) {
				demissionReason = "违规";
			} else if ("3".equals(DepartRsn)) {
				demissionReason = "其它";
			}
			statusCod = "0"; // 在职状态
			status = "离职";
		}
    	System.out.print("777777777777777"+demissionReason);
    	System.out.print("777777777777777"+demissionReason);
    	String dateSend = date1+" "+time;  // 报送时间
    	
    	UniSalesAppayInfo uniSalesAppayInfo = new UniSalesAppayInfo();
    	uniSalesAppayInfo.setAPPID(APPID);
    	uniSalesAppayInfo.setSID(SID);
    	uniSalesAppayInfo.setTIMESTAMP(TIMESTAMP);
    	uniSalesAppayInfo.setMESSAGETYPE(MESSAGETYPE);
    	uniSalesAppayInfo.setUni_sales_cod(uniSalesCod);
    	uniSalesAppayInfo.setComp_cod(compCod);
    	uniSalesAppayInfo.setComp_nam(compNam);
    	uniSalesAppayInfo.setPROV_ORGCOD(procode);
    	uniSalesAppayInfo.setPROV_ORGNAME(proname);
    	uniSalesAppayInfo.setSales_nam(salesNam);
    	uniSalesAppayInfo.setDate_birthd(dateBirthday);
    	uniSalesAppayInfo.setSex_cod(sexCod);
    	uniSalesAppayInfo.setMan_org_cod(manOrgCod);
    	uniSalesAppayInfo.setMan_org_nam(manOrgNam);
    	uniSalesAppayInfo.setIdtyp_cod(idtypCod);
    	uniSalesAppayInfo.setIdtyp_nam(idtypNam);
    	uniSalesAppayInfo.setId_no(idNo);
    	uniSalesAppayInfo.setSales_tel(salesTel);
    	uniSalesAppayInfo.setSales_mob(salesMob);
    	uniSalesAppayInfo.setSales_mail(salesMail);
    	uniSalesAppayInfo.setSales_addr(salesAddr);
    	uniSalesAppayInfo.setEducation_cod(educationCod);
    	uniSalesAppayInfo.setEducation_nam(educationNam);
    	uniSalesAppayInfo.setQualifi_no(qualifiNo);
    	uniSalesAppayInfo.setCertifi_no(certifiNo);
    	uniSalesAppayInfo.setContract_no(contractNo);
    	uniSalesAppayInfo.setStatus_cod(statusCod);
    	uniSalesAppayInfo.setStatus(status);
    	uniSalesAppayInfo.setSales_typ_cod(salesTypCod);
    	uniSalesAppayInfo.setSales_typ(salesTyp);
    	uniSalesAppayInfo.setIs_crosssale(isCrosssale);
    	uniSalesAppayInfo.setChannel_cod(channelCod);
    	uniSalesAppayInfo.setSalecha_nam(salechaNam);
    	uniSalesAppayInfo.setEntrant_date(entrantDate);
    	uniSalesAppayInfo.setDimission_date(dimissionDate);
    	uniSalesAppayInfo.setDemission_reason(demissionReason);
    	uniSalesAppayInfo.setDate_send(dateSend);
    	//modify by zhuxt 20141028 没有集团统一工号的业务员不再向集团发送报文。
    	if((mOperate.equals("UPDATE||MAIN")&&("".equals(mGroupAgentCode)||mGroupAgentCode==null))||(tLADimissionSchema != null&&("".equals(mGroupAgentCode)||mGroupAgentCode==null))){
//    			mRspUniSalescod.setMESSAGETYPE("01");
//	    		mRspUniSalescod.setERRDESC("集团统一工号不存在，不能进行修改！");
    	}else{
    		mRspUniSalescod=dealUniSalescodBL.uniSalesCodInfo(uniSalesAppayInfo);
    		if(mRspUniSalescod.getAPPID()==null||"".equals(mRspUniSalescod.getAPPID())){
    			mRspUniSalescod.setMESSAGETYPE("01");
    			mRspUniSalescod.setERRDESC("与集团接口连接失败！");
    		}else{
    			if("03".equals(mRspUniSalescod.getMESSAGETYPE())){
    				String sql4="select agentcode from laagent where groupagentcode='"+mRspUniSalescod.getUNI_SALES_COD()+"'";
    				String result = tExeSQL1.getOneValue(sql4);
    				if(result!=null&&!"".equals(result)){
    					mRspUniSalescod.setMESSAGETYPE("01");
    					mRspUniSalescod.setERRDESC("集团统一工号已存在，请重新获取！");
    				}
    			}
    		}
    	}
    	if("01".equals(mRspUniSalescod.getMESSAGETYPE())){
    		mRspUniSalescod.setERRDESC("集团工号获取失败，"+mRspUniSalescod.getERRDESC());
    	}
    	return mRspUniSalescod;
    }
    
    public static void main(String [] args){
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	LAAgentSchema tAgentSchema = new LAAgentSchema();
    	LADimissionSchema tDimissionSchema = new LADimissionSchema();
    	tLAAgentDB.setAgentCode("1101000187");
    	tLAAgentDB.getInfo();
    	tAgentSchema = tLAAgentDB.getSchema();
    	UnisalescodFun tf = new UnisalescodFun();
    	tf.getUnisalescod("", tAgentSchema, tDimissionSchema, "", "");
    	
    }
}
