package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.GlobalInput;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

public class LDBlackListUI {

  private VData mInputData ;
  public  CErrors mErrors=new CErrors();
  private LDBlackListSet mLDBlackListSet ;
  private VData mResult = new VData();
  public LDBlackListUI() {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    LDBlackListBL tLDBlackListBL=new LDBlackListBL();
    System.out.println("Start LDBlackList UI Submit...");
    tLDBlackListBL.submitData(mInputData,cOperate);

    System.out.println("End LDBlackList UI Submit...");

    //如果有需要处理的错误，则返回
    if (tLDBlackListBL .mErrors .needDealError() )
      this.mErrors .copyAllErrors(tLDBlackListBL.mErrors ) ;
    System.out.println(mErrors.getErrorCount());
    if (cOperate.equals("INSERT")||cOperate.equals("QUERY"))
    {
      this.mResult.clear();
      this.mResult=tLDBlackListBL.getResult();
      LDBlackListSchema tLDBlackListSchema = new LDBlackListSchema();
      tLDBlackListSchema=(LDBlackListSchema)mResult.getObjectByObjectName("LDBlackListSchema",0);
    }
    mInputData=null;
    return true;
  }

//查询纪录的公共方法
//  public LDBlackListSet queryData(VData cInputData)
//  {
//    //首先将数据在本类中做一个备份
//    mInputData=(VData)cInputData.clone() ;
//
//    LDBlackListBL tLDBlackListBL=new LDBlackListBL();
//    System.out.println("Start LDBlackList UI Query...");
//    mLDBlackListSet=(LDBlackListSet)tLDBlackListBL.queryData(mInputData);
//
//    System.out.println("End LDBlackList UI Query...");
//
//    //如果有需要处理的错误，则返回
//    if (tLDBlackListBL .mErrors .needDealError() )
//      this.mErrors .copyAllErrors(tLDBlackListBL.mErrors ) ;
//    System.out.println(mErrors.getErrorCount());
//    mInputData=null;
//    return mLDBlackListSet;
//
//  }

  public VData getResult()
  {
    return this.mResult;
  }

  public static void main(String[] args) {
    LDBlackListUI tLDBlackListUI = new LDBlackListUI();
     LDBlackListSchema tLDBlackListSchema   = new LDBlackListSchema();
     LDBlackListReasonDetailSet tLDBlackListReasonDetailSet = new LDBlackListReasonDetailSet();
     GlobalInput tG = new GlobalInput();
     tG.ManageCom = "86";
     tG.Operator = "claim";
     tLDBlackListSchema.setBlackListNo("000000016");
     tLDBlackListSchema.setBlackName("任旭东");
     tLDBlackListSchema.setBlackListType("0");
     tLDBlackListSchema.setBlackListOperator("claim");
     tLDBlackListSchema.setBlackListMakeDate("2005-6-12");
     tLDBlackListSchema.setBlackListSource("1");
     tLDBlackListSchema.setRelaContNo("54643");
     tLDBlackListSchema.setRelaPolNo("656565");
     tLDBlackListSchema.setRelaOtherNo("C56456456");
     tLDBlackListSchema.setRelaOtherNoType("1");
     LDBlackListReasonDetailSchema tLDBlackListReasonDetailSchema = new LDBlackListReasonDetailSchema();
     tLDBlackListReasonDetailSchema.setBlackListReasonKind("5101");
     tLDBlackListReasonDetailSchema.setBlackListReasonDetail("dfsdfs");
     tLDBlackListReasonDetailSet.add(tLDBlackListReasonDetailSchema);
     tLDBlackListReasonDetailSchema = new LDBlackListReasonDetailSchema();
     tLDBlackListReasonDetailSchema.setBlackListReasonKind("5202");
     tLDBlackListReasonDetailSchema.setBlackListReasonDetail("dfsdfs");
     tLDBlackListReasonDetailSet.add(tLDBlackListReasonDetailSchema);
     VData tVData = new VData();
           tVData.addElement(tLDBlackListSchema);
           tVData.addElement(tLDBlackListReasonDetailSet);
           tVData.addElement(tG);
     tLDBlackListUI.submitData(tVData, "INSERT");
   }
}
