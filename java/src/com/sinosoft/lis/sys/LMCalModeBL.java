/*
* <p>ClassName: LMCalModeBL </p>
* <p>Description: LMCalModeBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 险种定义
* @CreateDate：2003-06-21
 */
package com.sinosoft.lis.sys;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LMCalModeBL  {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors=new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LMCalModeSchema mLMCalModeSchema=new LMCalModeSchema();
    private LMCalModeSet mLMCalModeSet=new LMCalModeSet();
    private LMCalFactorSet mLMCalFactorSet=new LMCalFactorSet();
    public LMCalModeBL() {
    }
    public static void main(String[] args) {
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate =cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCalModeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LMCalModeBL-->dealData!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LMCalModeBL Submit...");
            LMCalModeBLS tLMCalModeBLS=new LMCalModeBLS();
            tLMCalModeBLS.submitData(mInputData,cOperate);
            System.out.println("End LMCalModeBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLMCalModeBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMCalModeBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LMCalModeBL";
                tError.functionName = "submitDat";
                tError.errorMessage ="数据提交失败!";
                this.mErrors .addOneError(tError) ;
                return false;
            }
        }
        mInputData=null;
        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        tLMCalModeDB.setCalCode(this.mLMCalModeSchema.getCalCode());
        int iCount = tLMCalModeDB.getCount();
        if (tLMCalModeDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLMCalModeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMCalModeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询计算编码唯一性失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (iCount > 0)
            {
                CError tError = new CError();
                tError.moduleName = "LMCalModeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "所录计算编码已存在!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else if (this.mOperate.equals("UPDATE||MAIN")
              ||this.mOperate.equals("DELETE||MAIN"))
        {
            if (iCount < 1)
            {
                CError tError = new CError();
                tError.moduleName = "LMCalModeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "所修改或删除的记录不存在!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        tReturn=true;
        return tReturn ;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLMCalModeSchema.setSchema((LMCalModeSchema)cInputData.getObjectByObjectName("LMCalModeSchema",0));
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        this.mLMCalFactorSet.set((LMCalFactorSet)cInputData.getObjectByObjectName("LMCalFactorSet",0));
        return true;
    }
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LMCalModeBLQuery Submit...");
        LMCalModeDB tLMCalModeDB=new LMCalModeDB();
        tLMCalModeDB.setSchema(this.mLMCalModeSchema);
        this.mLMCalModeSet=tLMCalModeDB.query();
        this.mResult.add(this.mLMCalModeSet);
        System.out.println("End LMCalModeBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLMCalModeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMCalModeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMCalModeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        mInputData=null;
        return true;
    }
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData=new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLMCalModeSchema);
            this.mInputData.add(this.mLMCalFactorSet);
        }
        catch(Exception ex)
        {
            // @@错误处理
            CError tError =new CError();
            tError.moduleName="LMCalModeBL";
            tError.functionName="prepareData";
            tError.errorMessage="在准备往后层处理所需要的数据时出错。";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        return true;
    }
    public VData getResult()
    {
        return this.mResult;
    }
}