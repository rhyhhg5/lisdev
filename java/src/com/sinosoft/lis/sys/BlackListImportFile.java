package com.sinosoft.lis.sys;

import java.io.File;
import java.util.HashMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.db.LLRegisterDB;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.lis.vschema.LCBlackListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLPathTool;

public class BlackListImportFile {

    public BlackListImportFile() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 从xls文件读入Sheet的开始行*/
    private static final int STARTROW = 2;
    /** 使用默认的导入方式 */
    private MultiSheetImporter importer;
    /** 错误处理 */
    public CErrors mErrors = new CErrors();
    /** Sheet Name */
    private String[] mSheetName;
    /** Schema 的名字 */
    private LCBlackListSet mLCBlackListSet;
    /** Sheet对应table的名字*/
    private static final String[] mTableNames = {"LCBlackList"};

    public BlackListImportFile(String fileName, String configFileName,
                         String[] sheetName) {
        mSheetName = sheetName;
        importer = new MultiSheetImporter(fileName, configFileName, mSheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
      System.out.println(" Into---- doImport...");
        importer.setTableName(mTableNames);
        importer.setMStartRows(STARTROW);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "MultiSheetImporter";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public HashMap getResult() {
        return importer.getResult();
    }

    /**
     * 用于获取导入文件版本号
     * @return String
     */
    public String getVersion() {
        HashMap tHashMap = importer.getResult();
        String version = "";
        version = (String) tHashMap.get(mSheetName[1]);
        return version;
    }

    /**
     * getSchemaSet 获取从MutiSheetImporter获得的SchemaSet
     *
     * @return LMProtocolFeeRateSet
     */
    public LCBlackListSet getLCBlackListSet()
    {
    	LCBlackListSet tLCBlackListSet = new LCBlackListSet();
        HashMap tHashMap = importer.getResult();
        tLCBlackListSet.add((LCBlackListSet) tHashMap.get(mSheetName[0]));
        return tLCBlackListSet;
    }

    private void jbInit() throws Exception {
    }
}
