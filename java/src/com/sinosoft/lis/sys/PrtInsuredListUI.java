package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtInsuredListUI
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    public PrtInsuredListUI()
    {
    }

    /**
     * 生成清单数据
     * @param cInputData VData：需包含TransferData对象和GlobalInput对象
     * @param cOperator String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String cOperator)
    {
        PrtInsuredListBL bl = new PrtInsuredListBL();

        XmlExport xml = bl.getXmlExport(cInputData, cOperator);
        if(bl == null)
        {
            mErrors = bl.mErrors;
            return null;
        }

        return xml;
    }

    public static void main(String[] args)
    {
        PrtInsuredListUI ui = new PrtInsuredListUI();
    }
}
