package com.sinosoft.lis.sys;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtCoreBL
{
    public static final String TemplateName = "TemplateName";
    public static final Object SQL = "SQL";

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;

    private String mTemplatName = null;
    private String mSql = null;

    private TextTag mTextTag = null;
    private XmlExport mXmlExport = new XmlExport();

    public PrtCoreBL()
    {
    }

    /**
     * 生成清单数据
     * @param cInputData VData：需包含TransferData对象和GlobalInput对象
     * @param cOperator String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String cOperator)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return mXmlExport;
    }

    /**
     * dealData
     * 生成清单数据
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        mXmlExport.createDocument(mTemplatName, "printer");

        if(!dealVariable())
        {
            return false;
        }

        if(!dealList())
        {
            return false;
        }

        mXmlExport.outputDocumentToFile("D:\\", "PrtContList");

        return true;
    }

    /**
     * dealList
     *生成打印列表数据
     * @return boolean
     */
    private boolean dealList()
    {
        System.out.println("\n\ndealList" + mSql);
        SSRS tSSRS = new ExeSQL().execSQL(mSql);
        if(tSSRS.getMaxRow() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "PrtCoreBL";
            tError.functionName = "dealList";
            tError.errorMessage = "没有查询到清单数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        ListTable tListTable = new ListTable();
        tListTable.setName("List");
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            String[] info = new String[tSSRS.getMaxCol() + 1];

            info[0] = "" + row;

            for(int col = 1; col < info.length; col++)
            {
                info[col] = StrTool.cTrim(tSSRS.GetText(row, col));
            }
            tListTable.add(info);
        }

        mXmlExport.addListTable(tListTable, new String[tSSRS.getMaxCol()]);

        return true;
    }

    /**
     * dealVariable
     *
     * @return boolean
     */
    private boolean dealVariable()
    {
        if(mTextTag == null)
        {
            mTextTag = new TextTag();
        }

        mXmlExport.addTextTag(mTextTag);

        return true;
    }

    /**
     * 从VData中将传入的数据获取到类变量
     * @param cData VData
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData cData)
    {
        mGI = (GlobalInput) cData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) cData
                        .getObjectByObjectName("TransferData", 0);
        mTextTag = (TextTag) cData.getObjectByObjectName("TextTag", 0);

        if(mGI == null || mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtCoreBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的数据不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mTemplatName = (String)mTransferData.getValueByName(TemplateName);
        if(mTemplatName == null || mTemplatName.equals(""))
        {
           CError tError = new CError();
           tError.moduleName = "PrtCoreBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "请传入模版名";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
        }

        mSql = (String)mTransferData.getValueByName(SQL);
        if(mSql == null || mSql.equals(""))
        {
           CError tError = new CError();
           tError.moduleName = "PrtCoreBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "请传入查询清单的Sql";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        PrtCoreBL prtcorebl = new PrtCoreBL();

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "endor0";
        tGI.ComCode = "86";
        tGI.ManageCom = tGI.ComCode;

        TransferData tTransferData = new TransferData();

        String templateName = "PrtContListVali.vts";
    //    String templateName = "PrtContListInVali.vts";
        String sql = "select a.ContNo,a.PrtNo,'个单',    case when hasLongRisk(a.ContNo) = 'L' then '个险长期' else '个险一年' end,    AppntName,a.InsuredName,a.CValiDate, a.CInvaliDate, a.PayToDate, a.Prem,    codeName('stateflag', StateFlag),    ShowManageName(a.ManageCom),    (select Name from LAAgent where AgentCode = a.AgentCode) from LCCont a where AppFlag='1'    and ContType = '1'    and (StateFlag is null or StateFlag in('1')) and a.StateFlag='1'  and hasLongRisk(a.ContNo) = 'O'    and exists(select 1 from LDCom where Name like '%%')    and exists(select 1 from LABranchGroup where Name like '%%')    and exists(select 1 from LAAgent where Name like '%%') ";

        tTransferData.setNameAndValue(PrtCoreBL.TemplateName, templateName);
        tTransferData.setNameAndValue(PrtCoreBL.SQL, sql);

        TextTag tTextTag = new TextTag();
//        tTextTag.add("ContNo", request.getParameter("ContNo"));
//        tTextTag.add("PrtNo", request.getParameter("PrtNo"));
        tTextTag.add("StateFlag", "1");
        tTextTag.add("StateFlagName", "承保有效");
        tTextTag.add("ContKind", "11");
        tTextTag.add("ContKindName", "个险一年期保单");
//        tTextTag.add("ManageCom", request.getParameter("ManageCom"));
//        tTextTag.add("ManageComName", request.getParameter("ManageComName"));
//        tTextTag.add("Group03", request.getParameter("Group03"));
//        tTextTag.add("Group03Name", request.getParameter("Group03Name"));
//        tTextTag.add("AgentCode", request.getParameter("AgentCode"));
//        tTextTag.add("AgentName", request.getParameter("AgentName"));
//        tTextTag.add("RiskCode1", request.getParameter("RiskCode1"));
//        tTextTag.add("RiskCode1Name", request.getParameter("RiskCode1Name"));
//        tTextTag.add("RiskCode2", request.getParameter("RiskCode2"));
//        tTextTag.add("RiskCode2Name", request.getParameter("RiskCode2Name"));
//        tTextTag.add("ApplyDateStart", request.getParameter("ApplyDateStart"));
//        tTextTag.add("ApplyDateEnd", request.getParameter("ApplyDateEnd"));
//        tTextTag.add("PayToDateStart", request.getParameter("PayToDateStart"));
//        tTextTag.add("PayToDateEnd", request.getParameter("PayToDateEnd"));

        VData d = new VData();
        d.add(tGI);
        d.add(tTransferData);
        d.add(tTextTag);

        PrtCoreUI ui = new PrtCoreUI();
        XmlExport txmlExport = ui.getXmlExport(d, "");
        if(txmlExport == null)
        {
            System.out.println(ui.mErrors);
        }
    }
}
