package com.sinosoft.lis.sys;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.ActivityOperator;
/**
 * <p>Title: 催办控制 </p>
 * <p>Description: 更新催办控制时间</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: SinoSoft</p>
 * @author zhangxing
 * @version 1.0
 */

public class ContrlUrgeNoticeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    /**工作流引擎 */
    ActivityOperator mActivityOperator = new ActivityOperator();
    /** 数据操作字符串 */
    private String mOperater;
    private String mManageCom;

    /** 业务数据操作字符串 */
    private String mUrgeDate;
    private String mPrtSeq;


    /** 打印管理表 */
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    public ContrlUrgeNoticeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
            return false;

        //校验是否有未打印的体检通知书
        if (!checkData())
            return false;

        //进行业务处理
        if (!dealData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        System.out.println("Start SysUWNoticeBL Submit...");
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, "")) {
         // @@错误处理
         this.mErrors.copyAllErrors(tSubmit.mErrors);
         CError tError = new CError();
         tError.moduleName = "UWAutoChkBL";
         tError.functionName = "submitData";
         tError.errorMessage = "数据提交失败!";
         this.mErrors.addOneError(tError);
         return false;
     }


        return true;
    }

    /**
     * 校验业务数据
     * @return
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        //打印队列
        if (preparePrint() == false)
            return false;

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ContrlUrgeNotice";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperater = mGlobalInput.Operator;
        if (mOperater == null || mOperater.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ContrlUrgeNotice";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ContrlUrgeNotice";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得业务数据
        if (mTransferData == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ContrlUrgeNotice";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mUrgeDate = (String) mTransferData.getValueByName("UrgeDate");
        if (mUrgeDate == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ContrlUrgeNotice";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中UrgeDate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mPrtSeq = (String) mTransferData.getValueByName("PrtSeq");
        if (mPrtSeq == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ContrlUrgeNotice";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输业务数据中PrtSeq失败!";
            this.mErrors.addOneError(tError);
            return false;
        }




        return true;
    }

    /**
     * 打印信息表
     * @return
     */
    private boolean preparePrint()
    {

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mPrtSeq);

        if (!tLOPRTManagerDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ContrlUrgNoticeBL";
            tError.functionName = "ContrlUrgNoticeBL";
            tError.errorMessage = "查询打印管理表信息出错!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();
        //准备打印管理表数据

       mLOPRTManagerSchema.setForMakeDate(mUrgeDate);


        return true;
    }



    /**
     * 工作流准备后台提交数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();

        //添加体检通知书打印管理表数据
        map.put(mLOPRTManagerSchema, "UPDATE");

        mResult.add(map);
        return true;
    }

    /**
     * 返回处理后的结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回工作流中的Lwfieldmap所描述的值
     * @return TransferData
     */
    public TransferData getReturnTransferData()
    {
        return mTransferData;
    }

    /**
     * 返回错误对象
     * @return CErrors
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {

    }
}
