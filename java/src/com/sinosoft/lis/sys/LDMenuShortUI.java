package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:个人信息类（界面输入）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LDMenuShortUI {
	
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();
  //private LDMenuShortSet mLDMenuShortSet ;
  public LDMenuShortUI() {
  }
  public static void main(String[] args) {
    LDMenuShortUI LDMenuShortUI1 = new LDMenuShortUI();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    LDMenuShortBL tLDMenuShortBL=new LDMenuShortBL();
    System.out.println("Start LDMenuShort UI Submit...");
    tLDMenuShortBL.submitData(mInputData,cOperate);

    System.out.println("End LDPerson UI Submit...");

    //如果有需要处理的错误，则返回
    if (tLDMenuShortBL .mErrors .needDealError() )
        this.mErrors .copyAllErrors(tLDMenuShortBL.mErrors ) ;
    System.out.println(mErrors.getErrorCount());  	
    mInputData=null;
    return true;
  }
  
  //查询纪录的公共方法
 /*  public LDPersonSet queryData(VData cInputData)
   {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    
    LDPersonBL tLDPersonBL=new LDPersonBL();
    System.out.println("Start LDPerson UI Query...");
    mLDPersonSet=(LDPersonSet)tLDPersonBL.queryData(mInputData);

    System.out.println("End LDPerson UI Query...");

    //如果有需要处理的错误，则返回
    if (tLDPersonBL .mErrors .needDealError() )
        this.mErrors .copyAllErrors(tLDPersonBL.mErrors ) ;
    System.out.println(mErrors.getErrorCount());  	
    mInputData=null;
    return mLDPersonSet;   	
   	
   }	*/

}