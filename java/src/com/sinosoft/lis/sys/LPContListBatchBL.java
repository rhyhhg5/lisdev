package com.sinosoft.lis.sys;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LPContListBatchBL {
	/**错误处理类，每个错误需要处理的类中都放置的有该类*/
	public CErrors mErrors = new CErrors();
	/**往后边传输数据的容器*/
	private VData mResult = new VData();
	/**接收前台数据的容器*/
	private VData mInputData = new VData();
	/**数据操作字符串*/
	private String mOperator;
	/**全局数据*/
	private GlobalInput mGlobalInput = new GlobalInput();
	private String CurrentDate = PubFun.getCurrentDate();
	private String mLDCode;
	/**节点名*/
	private String[] mSheetName = {"Sheet1"};
	/**配置文件名*/
	private String mConfigName = "LPContListBatchDefiList.xml";
	/**文件路径*/
	private String mPath;
	/**文件名*/
	private String mFileName = "";
	/**导入成功的记录数*/
	private int mSuccNum = 0 ;
	/**批次号*/
	private String mBatchNo = "";
	private MMap mMap = new MMap();
	/**无参构造*/
	public LPContListBatchBL() {
		super();
	}
	/**解析excel的构造函数*/
	public LPContListBatchBL(String aPath,String aFileName,GlobalInput aGloBalInput) {
		this.mPath = aPath;
		this.mFileName = aFileName;
		this.mGlobalInput = aGloBalInput;
	}
	/**添加传入的一个sheet数据*/
	public boolean doAdd(String aPath,String aFileName) {
		//从磁盘导入数据
		LPContListImportFile importFile = new LPContListImportFile(aPath+aFileName,
				aPath+mConfigName,mSheetName);
		if(!importFile.doImport()) {
			this.mErrors.copyAllErrors(importFile.mErrors);
			return false;
		}
		if(mGlobalInput.ManageCom.length()<4) {
			CError tError = new CError();
			tError.moduleName="LDCodeListBatchBL";
			tError.functionName="submitData";
			tError.errorMessage="总公司不能进行上报";
			this.mErrors.addOneError(tError);
			return false;
		}
		LDCodeSet tLDCodeSet = (LDCodeSet)importFile.getSchemaSet();
		MMap map = new MMap();
		mSuccNum = tLDCodeSet.size();
		System.out.println("导入数据的容量是：" + mSuccNum);
		mBatchNo = PubFun1.CreateMaxNo("mLDCodeListCode", 10);
		System.out.println("tBatchNo=" + mBatchNo);
		//对sheet中的数据进行再处理
		boolean flag = true;
		String books="";
		for(int i=1;i<=mSuccNum;i++) {
			LDCodeSchema tLDCodeSchema = new LDCodeSchema();
			tLDCodeSchema = tLDCodeSet.get(i).getSchema();
			String StrSQL = "select * from ldcode where 1=1 " +
							" and Code='" + tLDCodeSchema.getCode() + "'" + 
							" with ur";
			ExeSQL tExeSQL = new ExeSQL();
			SSRS ssrs = tExeSQL.execSQL(StrSQL);
			int MaxNumber = ssrs.getMaxRow();
			System.out.println("^^^^^^^^^ssrs=" + MaxNumber);
			if(MaxNumber>0) {
				CError tError = new CError();
				tError.moduleName="LDCodeListBatchBL";
				tError.functionName="submitData";
				tError.errorMessage="插入数据失败，系统内已有" + tLDCodeSchema.getCode() + "保单，不能重复导入。" ;
				this.mErrors.addOneError(tError);
				flag=false;
				break;
			}
			
			String SQLStr = "select managecom from lcGrpCont where 1=1 "
						  + "and GrpContNo = '"  + tLDCodeSchema.getCode() + "'" 
						  //+ "and managecom like '" + mGlobalInput.ManageCom + "%'"
						 /* + "union " 
						  + "select managecom from lbGrpCont where 1=1 " 
						  + "and GrpContNo = '" + tLDCodeSchema.getCode() + "'"*/
						  + "with ur ";
			SSRS ss = tExeSQL.execSQL(SQLStr);
			int count = ss.getMaxRow();
			if(count <= 0) {
				CError tError = new CError();
				tError.moduleName="LDCodeListBatchBL";
				tError.functionName="submitData";
				tError.errorMessage="导入的" + tLDCodeSchema.getCode() + "保单不存在!";
				this.mErrors.addOneError(tError);
				flag=false;
				break;
			}

			System.out.println("保单：" + tLDCodeSchema.getCode());

			if(books.indexOf(tLDCodeSchema.getCode()+",")!=-1){
				CError tError = new CError();
				tError.moduleName="LDCodeListBatchBL";
				tError.functionName="submitData";
				tError.errorMessage="当前文件中导入的" + tLDCodeSchema.getCode() + "保单出现重复!";
				this.mErrors.addOneError(tError);
				flag=false;
				break;
			}
			String sql = "select code from ldcode where 1=1 and codetype='thridcompany' "
					+ "and codename = '" +tLDCodeSchema.getCodeName() + "'" 
					+ "with ur";
			SSRS sr = tExeSQL.execSQL(sql);
			int number = sr.getMaxRow();
			if(number<=0) {
				CError tError = new CError();
				tError.moduleName="LDCodeListBatchBL";
				tError.functionName="submitData";
				tError.errorMessage="导入的" + tLDCodeSchema.getCodeName() + "外包第三方不存在!";
				this.mErrors.addOneError(tError);
				flag=false;
				break;
			}
			String code = sr.GetText(1, 1);
			System.out.println("code=" + code);
			String managecom = ss.GetText(1, 1).substring(0, 4);
			System.out.println("managecom=" + managecom);
			if(!mGlobalInput.ManageCom.substring(0, 4).equals(managecom)) {
				CError tError = new CError();
				tError.moduleName="LDCodeListBatchBL";
				tError.functionName="submitData";
				tError.errorMessage="当前机构不能导入" + tLDCodeSchema.getCode() + "号的保单!";
				this.mErrors.addOneError(tError);
				flag=false;
				break;
			}
			
				
			tLDCodeSchema.setCodeType(code);//类型
			tLDCodeSchema.setCodeName(CurrentDate); //日期
			tLDCodeSchema.setCodeAlias(mGlobalInput.Operator);//操作人
			tLDCodeSchema.setComCode(mGlobalInput.ManageCom); //保单机构
			
			books=books+tLDCodeSchema.getCode()+",";
			
			//将每一条数据放入返回的结果集
			this.mResult.add(tLDCodeSchema);
			//添加一条理赔案件外包对应保单信息
			
			addOneLDCodeUser(map, tLDCodeSchema);
		}
		if(!flag){
			return false;
		}
		if(!submit(map)) {
			return false;
		}
		return true;
	}
	
	public VData getResult() {
		return mResult;
	}
	/**添加一个理赔外包案件对应保单*/
	public void addOneLDCodeUser(MMap aMap,LDCodeSchema aLDCodeSchema) {
		aMap.put(aLDCodeSchema, "DELETE&INSERT");
	}
	/**数据提交的公共方法*/
	public boolean submit(MMap aMap) {
		PubSubmit pb = new PubSubmit();
		VData tVData = new VData();
		tVData.add(aMap);
		if(!pb.submitData(tVData, "")) {
			//错误处理
			this.mErrors.copyAllErrors(pb.mErrors);
			CError tError = new CError();
			tError.moduleName="LDCodeListBatchBL";
			tError.functionName="submitData";
			tError.errorMessage="数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	public int getSuccNum() {
		return mSuccNum;
	}
	public void setSuccNum(int succNum) {
		mSuccNum = succNum;
	}
	public String getBatchNo() {
		return mBatchNo;
	}
}
