package com.sinosoft.lis.sys;
import com.sinosoft.utility.ExeSQL;

public class LoginBL {
	public String checkUserState(String usercode){
		String userstate = new ExeSQL().getOneValue("select userstate from lduser where usercode = '"+usercode+"'");
		return userstate;
	}
	
	public String checkUserCom(String usercode){
		String userCom = new ExeSQL().getOneValue("select comcode from lduser where usercode='"+usercode+"'");
		return userCom;
	}
	
	public String getLeader(String usercode){
		String operator = new ExeSQL().getOneValue("select Operator from lduser where usercode='"+usercode+"'");
		return operator;
	}
	public String getLeader1Name(String usercode){
		String leaderName = new ExeSQL().getOneValue("select username from lduser where usercode='"+usercode+"'");
		return leaderName;
	}
	
	public String getDateSub(String usercode){
		String dateSub = new ExeSQL().getOneValue("select current_date-pmodifydate from lduser where usercode='"+usercode+"'");
		return dateSub;
	}
}
