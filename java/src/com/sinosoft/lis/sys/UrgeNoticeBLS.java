package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */

public class UrgeNoticeBLS {

  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;

  public UrgeNoticeBLS() {
  }
  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start UrgeNotice BLS Submit...");
//    //执行添加纪录活动
//    if(cOperate.equals("INSERT"))
//    {
//      tReturn=save();
//      if (tReturn)
//        System.out.println("Save sucessful");
//      else
//        System.out.println("Save failed") ;
//    }
    //执行更新纪录活动
    if(cOperate.equals("UPDATE"))
    {
      tReturn=save();
      if (tReturn)
        System.out.println("Update sucessful");
      else
        System.out.println("Update failed") ;
    }
	if(cOperate.equals("INSERT"))
	{
	  tReturn=save();
	  if (tReturn)
		System.out.println("INSERT sucessful");
	  else
		System.out.println("INSERT failed") ;
    }
//    //执行删除纪录活动
//    if(cOperate.equals("DELETE"))
//    {
//      tReturn=delete();
//      if (tReturn)
//        System.out.println("Delete sucessful");
//      else
//        System.out.println("Delete failed") ;
//    }

    System.out.println("End UrgeNotice BLS Submit...");

    //如果有需要处理的错误，则返回
//    if (tLDGrpBLS.mErrors .needDealError() )
//        this.mErrors .copyAllErrors(tLDGrpBLS.mErrors ) ;

    mInputData=null;
    return tReturn;
  }
//  private boolean save()
//  {
//    boolean tReturn =true;
//    System.out.println("Start Save...");
//
//    Connection conn=DBConnPool.getConnection();
//    if (conn==null)
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "LDGrpBLS";
//      tError.functionName = "save";
//      tError.errorMessage = "数据库连接失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
//    try{
//      /** 集体信息 */
//      System.out.println("Start 集体信息...");
//
//      LDGrpDB tLDGrpDB=new LDGrpDB(conn);
//      tLDGrpDB.setSchema((LDGrpSchema)mInputData.getObjectByObjectName("LDGrpSchema",0));
//      tLDGrpDB.insert() ;
//      conn.close();
//    }
//    catch (Exception ex)
//    {
//      System.out.println("Exception in BLS");
//      System.out.println("Exception:"+ex.toString());
//      // @@错误处理
//      CError tError =new CError();
//      tError.moduleName="LDGrpBLS";
//      tError.functionName="submitData";
//      tError.errorMessage=ex.toString() ;
//      this.mErrors .addOneError(tError) ;
//      try{
//        conn.close();
//        } catch(Exception e){}
//        tReturn=false;
//    }
//
//    return tReturn;
//  }

  private boolean save()
  {
    boolean tReturn =true;
    LOPRTManagerSet NewLOPRTManagerSet = new LOPRTManagerSet();
    LOPRTManagerSet OldLOPRTManagerSet = new LOPRTManagerSet();
    NewLOPRTManagerSet.set((LOPRTManagerSet)mInputData.getObject(0));
    OldLOPRTManagerSet.set((LOPRTManagerSet)mInputData.getObject(1));
    System.out.println("Start Update...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "UrgeNoticeBLS";
      tError.functionName = "update";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try{
      System.out.println("Start 催收信息...");
      conn.setAutoCommit(false);
      if (OldLOPRTManagerSet.size()!=0&&(NewLOPRTManagerSet.size()!=0))
      {
        LOPRTManagerDBSet NewLOPRTManagerDBSet = new LOPRTManagerDBSet(conn);
        NewLOPRTManagerDBSet.set(NewLOPRTManagerSet);
        LOPRTManagerDBSet OldLOPRTManagerDBSet = new LOPRTManagerDBSet(conn);
        OldLOPRTManagerDBSet.set(OldLOPRTManagerSet);
        if (!NewLOPRTManagerDBSet.insert())
        {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "UrgeNoticeBLS";
          tError.functionName = "save";
          tError.errorMessage = "保存所发的催收通知书数据信息失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }
        if (!OldLOPRTManagerDBSet.update())
        {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "UrgeNoticeBLS";
          tError.functionName = "save";
          tError.errorMessage = "更改原来通知书打印状态信息失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }
      }
      conn.commit();
      conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="UrgeNoticeBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      try{
        conn.rollback();
        conn.close();
        } catch(Exception e){}
        tReturn=false;
    }
    return tReturn;
  }

//  private boolean delete()
//  {
//    boolean tReturn =true;
//    System.out.println("Start Delete...");
//    Connection conn=DBConnPool.getConnection();
//    if (conn==null)
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "UrgeNoticeBLS";
//      tError.functionName = "delete";
//      tError.errorMessage = "数据库连接失败!";
//      this.mErrors .addOneError(tError) ;
//      return false;
//    }
//    try{
//      /** 集体信息 */
//      System.out.println("Start 集体信息...");
//      LDGrpDB tLDGrpDB=new LDGrpDB(conn);
//      tLDGrpDB.setSchema((LDGrpSchema)mInputData.getObjectByObjectName("LDGrpSchema",0));
//      tLDGrpDB.delete() ;
//      conn.close();
//    }
//    catch (Exception ex)
//    {
//      System.out.println("Exception in BLS");
//      System.out.println("Exception:"+ex.toString());
//      // @@错误处理
//      CError tError =new CError();
//      tError.moduleName="UrgeNoticeBLS";
//      tError.functionName="submitData";
//      tError.errorMessage=ex.toString() ;
//      this.mErrors .addOneError(tError) ;
//      try{
//        conn.close();
//        } catch(Exception e){}
//        tReturn=false;
//    }
//    return tReturn;
//  }

//  //查询纪录的公共方法
//  public LDGrpSet queryData(VData cInputData)
//  {
//    //首先将数据在本类中做一个备份
//    mInputData=(VData)cInputData.clone() ;
//    System.out.println("Start LDGrp BLS Query...");
//    //执行查询纪录活动
//    System.out.println("Start Query...");
//    Connection conn=DBConnPool.getConnection();
//    if (conn==null)
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "UrgeNoticeBLS";
//      tError.functionName = "queryData";
//      tError.errorMessage = "数据库连接失败!";
//      this.mErrors .addOneError(tError) ;
//      return   null;
//    }
//
//    try{
//      /** 集体信息 */
//      System.out.println("Start 集体信息...");
//      LDGrpDB tLDGrpDB=new LDGrpDB(conn);
//      tLDGrpDB.setSchema((LDGrpSchema)mInputData.getObjectByObjectName("LDGrpSchema",0));
//      mLDGrpSet=(LDGrpSet)tLDGrpDB.query() ;
//      conn.close();
//    }
//    catch (Exception ex)
//    {
//      System.out.println("Exception in BLS");
//      System.out.println("Exception:"+ex.toString());
//      // @@错误处理
//      CError tError =new CError();
//      tError.moduleName="UrgeNoticeBLS";
//      tError.functionName="QueryData";
//      tError.errorMessage=ex.toString() ;
//      this.mErrors .addOneError(tError) ;
//      mLDGrpSet=null;
//      try{
//        conn.close();
//        } catch(Exception e){}
//        return null;
//    }
//    mInputData=null;
//    return mLDGrpSet;
//  }

  public static void main(String[] args) {
    UrgeNoticeBLS urgeNoticeBLS1 = new UrgeNoticeBLS();
  }
}