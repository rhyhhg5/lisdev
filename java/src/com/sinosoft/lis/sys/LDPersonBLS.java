package com.sinosoft.lis.sys;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LDPersonBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  private LDPersonSet mLDPersonSet ;

  public LDPersonBLS() {
  }
  public static void main(String[] args) {
    LDPersonBLS proposalBLS1 = new LDPersonBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println("Start LDPerson BLS Submit...");
    //执行添加纪录活动
    if(cOperate.equals("INSERT"))
    {
    tReturn=save();
    if (tReturn)
      System.out.println("Save sucessful");
    else
        System.out.println("Save failed") ;
    }
    //执行更新纪录活动
    if(cOperate.equals("UPDATE"))
    {
    tReturn=update();
    if (tReturn)
      System.out.println("Update sucessful");
    else
        System.out.println("Update failed") ;
    }
    //执行删除纪录活动
    if(cOperate.equals("DELETE"))
    {
    tReturn=delete();
    if (tReturn)
      System.out.println("Delete sucessful");
    else
        System.out.println("Delete failed") ;
    }

    System.out.println("End LDPerson BLS Submit...");

    //如果有需要处理的错误，则返回
//    if (tLDPersonBLS.mErrors .needDealError() )
//        this.mErrors .copyAllErrors(tLDPersonBLS.mErrors ) ;

    mInputData=null;
    return tReturn;
  }
  private boolean save()
  {
    System.out.println("Start Save...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "LDPersonBLS";
                tError.functionName = "save";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");

      LDPersonDB tLDPersonDB=new LDPersonDB(conn);
      tLDPersonDB.setSchema((LDPersonSchema)mInputData.getObjectByObjectName("LDPersonSchema",0));
      LCAddressDB tLCAddressDB=new LCAddressDB(conn);
      tLCAddressDB.setSchema((LCAddressSchema)mInputData.getObjectByObjectName("LCAddressSchema",0));

      if(!tLDPersonDB.insert()||!tLCAddressDB.insert())
      {
    		// @@错误处理
         this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
    	 CError tError = new CError();
	 tError.moduleName = "LDPersonBLS";
	 tError.functionName = "saveData";
	 tError.errorMessage = "个人信息表数据保存失败!";
	 this.mErrors .addOneError(tError) ;
         try{
          conn.close();
      } catch(Exception e){}
 	 return false;
      }
          conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDPersonBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  private boolean update()
  {
      System.out.println("Start Update...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "LDPersonBLS";
                tError.functionName = "update";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");
      LDPersonDB tLDPersonDB=new LDPersonDB(conn);
      tLDPersonDB.setSchema((LDPersonSchema)mInputData.getObjectByObjectName("LDPersonSchema",0));

      if(!tLDPersonDB.update())
      {
      	    		// @@错误处理
         this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
    	 CError tError = new CError();
	 tError.moduleName = "LDPersonBLS";
	 tError.functionName = "updateData";
	 tError.errorMessage = "个人信息表数据更新失败!";
	 this.mErrors .addOneError(tError) ;
         try{
          conn.close();
      } catch(Exception e){}
 	 return false;
      }
          conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDPersonBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  private boolean delete()
  {
      System.out.println("Start Delete...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "LDPersonBLS";
                tError.functionName = "delete";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");
      LDPersonDB tLDPersonDB=new LDPersonDB(conn);
      tLDPersonDB.setSchema((LDPersonSchema)mInputData.getObjectByObjectName("LDPersonSchema",0));

      if(!tLDPersonDB.delete())
      {
      	    		// @@错误处理
         this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
    	 CError tError = new CError();
	 tError.moduleName = "LDPersonBLS";
	 tError.functionName = "delData";
	 tError.errorMessage = "个人信息表数据删除失败!";
	 this.mErrors .addOneError(tError) ;
         try{
          conn.close();
      } catch(Exception e){}
 	 return false;
      }
          conn.close();
    }
    catch (Exception ex)
    {
      System.out.println("Exception in BLS");
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDPersonBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

   //查询纪录的公共方法
   public LDPersonSet queryData(VData cInputData)
   {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("Start LDPerson BLS Query...");
    //执行查询纪录活动
    System.out.println("Start Query...");
    Connection conn=DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "LDPersonBLS";
                tError.functionName = "queryData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return null;
    }
    try{
      /** 个人信息 */
      System.out.println("Start 个人信息...");

      LDPersonDB tLDPersonDB=new LDPersonDB(conn);
      tLDPersonDB.setSchema((LDPersonSchema)mInputData.getObjectByObjectName("LDPersonSchema",0));

      mLDPersonSet=(LDPersonSet)tLDPersonDB.query() ;
      try{
       conn.close();
      } catch(Exception e){}
    }
    catch (Exception ex)
    {
      System.out.println("Exception:"+ex.toString());
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDPersonBLS";
      tError.functionName="QueryData";
      tError.errorMessage=ex.toString() ;
      this.mErrors .addOneError(tError) ;
      mLDPersonSet=null;
    }
    mInputData=null;
    return mLDPersonSet;
   }

}
