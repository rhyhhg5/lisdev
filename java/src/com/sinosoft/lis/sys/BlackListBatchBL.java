/**
 * 黑名单批次导入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  Houyd
 * @version 1.0
 */

package com.sinosoft.lis.sys;

import java.io.File;
import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class BlackListBatchBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 接受前台数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mBlackListCode;

    /** 节点名 */
    private String[] mSheetName = {"Sheet1"};
    /** 配置文件名 */
    private String mConfigName = "BlackListBatchDefiList.xml";
    /** 文件路径 */
    private String mPath;
    /**文件名*/
    private String mFileName="";
    /**导入成功的记录数*/
    private int mSuccNum = 0;
    /**批次号*/
    private String mBatchNo = ""; 
    
    private MMap mMap = new MMap();

    /**
     * 默认无参构造函数
     */
    public BlackListBatchBL() {
    }

    /**
     * 解析Excel的构造函数
     * @param path String
     * @param fileName String
     */
    public BlackListBatchBL(String aPath, String aFileName,
    		GlobalInput aGlobalInput) {
        this.mPath = aPath;
        this.mFileName = aFileName;
        this.mGlobalInput = aGlobalInput;
        
    }
    
    /**
     * 添加传入的一个Sheet数据
     * @param path String
     * @param fileName String
     */
	public boolean doAdd(String aPath, String aFileName) 
    {
   
        //从磁盘导入数据
        BlackListImportFile importFile = new BlackListImportFile(aPath + aFileName,
        		aPath + mConfigName, mSheetName);
        if (!importFile.doImport()) {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LCBlackListSet tLCBlackListSet = (LCBlackListSet) importFile.getLCBlackListSet();
        
        MMap map = new MMap();

        mSuccNum = tLCBlackListSet.size();
        System.out.println("导入数据容量："+tLCBlackListSet.size());
        mBatchNo = PubFun1.CreateMaxNo("mBlackListCode", 10);//为批次添加统一的标示
        System.out.println("tBatchNo："+mBatchNo);
        //对sheet中的数据进行再处理
        for (int i = 1; i <= tLCBlackListSet.size(); i++) 
        {
        	LCBlackListSchema tLCBlackListSchema = new LCBlackListSchema();
        	tLCBlackListSchema = tLCBlackListSet.get(i).getSchema();
        	
        	//根据是否有“证件类型”判断是个人还是团体
        	if(tLCBlackListSchema.getIDNoType() != null && !"".equals(tLCBlackListSchema.getIDNoType())){
        		//个人
        		tLCBlackListSchema.setType("0");
        	}else{
        		tLCBlackListSchema.setType("1");
        	}
        	
        	//为每一条记录添加共有的信息
        	mBlackListCode = PubFun1.CreateMaxNo("mBlackListCode", 10);
        	System.out.println("mBlackListCode："+mBlackListCode);
        	
        	tLCBlackListSchema.setBlacklistCode(mBlackListCode);
        	tLCBlackListSchema.setOperator(mGlobalInput.Operator);
        	tLCBlackListSchema.setMakeDate(CurrentDate);
        	tLCBlackListSchema.setMakeTime(CurrentTime);
        	tLCBlackListSchema.setModifyDate(CurrentDate);
        	tLCBlackListSchema.setModifyTime(CurrentTime);
        	tLCBlackListSchema.setRem1(mBatchNo);//为批次添加统一的标示
        	
        	//将每一条数据放入返回的结果集
        	this.mResult.add(tLCBlackListSchema);
            //添加一条黑客户信息
        	addOneBlackUser(map, tLCBlackListSchema);
        	
        }

        //提交数据到数据库
        if (!submit(map)) {
            return false;
        }
        return true;
    }


    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        
        //进行业务处理
        if (!doAdd(mPath, mFileName)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BlackListBatchBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败BlackListBatchBL-->doAdd!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println(mPath);
        System.out.println(mFileName);

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
    	mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        TransferData tTransferData = new TransferData();
        tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        mPath = (String) tTransferData.getValueByName("FilePath");
        mFileName = (String) tTransferData.getValueByName("FileName");
        return true;

    }

        /**
         * 准备后台的数据
         * @return boolean
         */
        private boolean prepareOutputData() {
            try {
                mInputData = new VData();
                this.mInputData.add(mMap);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "BlackListPersonBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }

        public VData getResult() {
            return mResult;
        }
        
        /**
         * 添加一个黑名单用户
         *
         */
        private void addOneBlackUser(MMap aMap,LCBlackListSchema aLCBlackListSchema){
        	aMap.put(aLCBlackListSchema,"DELETE&INSERT");
        }
        
        /**
         * 数据提交公共方法
         * @return
         */
        public boolean submit(MMap aMap ){
			PubSubmit pb = new PubSubmit();
			VData tVData = new VData();
			tVData.add(aMap);
			if(!pb.submitData(tVData,"")){
				// @@错误处理
				this.mErrors.copyAllErrors(pb.mErrors);

				CError tError = new CError();
				tError.moduleName = "BlackListBatchBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";

				this.mErrors.addOneError(tError);
				return false;
			}
			return true;
		}

		public int getSuccNum() {
			return mSuccNum;
		}

		public void setMSuccNum(int succNum) {
			mSuccNum = succNum;
		}

		public String getBatchNo() {
			return mBatchNo;
		}
}
