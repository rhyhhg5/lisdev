package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpRReportItemSchema;

/**
 * <p>Title: 团体契调通知书发送接口 </p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YangMing
 * @version 1.0
 */

public class GrpUWSendRReportUI {
    public GrpUWSendRReportUI() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        GrpUWSendRReportBL rGrpUWSendRReportBL = new GrpUWSendRReportBL();

        System.out.println("---GrpUWSendRReportBL BEGIN---");
        if (rGrpUWSendRReportBL.submitData(cInputData, mOperate) == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(rGrpUWSendRReportBL.mErrors);
            mResult.clear();
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        GrpUWSendRReportUI tGrpUWSendRReportUI = new GrpUWSendRReportUI();
        VData tVData = new VData();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        LCGrpContSet tLCGrpContSet =  new LCGrpContSet();
        GlobalInput mGlobalInput = new GlobalInput();
        LCGrpRReportItemSchema tLCGrpRReportItemSchema = new  LCGrpRReportItemSchema();
        tLCGrpRReportItemSchema.setGrpContNo("1400000447");
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom="86";
        tLCGrpContSchema.setGrpContNo("1400000447");
        tLCGrpContSet.add(tLCGrpContSchema);
        tVData.add(tLCGrpContSet);
        tVData.add(mGlobalInput);
        tVData.add(tLCGrpRReportItemSchema);
        tGrpUWSendRReportUI.submitData(tVData,"");
    }
}
