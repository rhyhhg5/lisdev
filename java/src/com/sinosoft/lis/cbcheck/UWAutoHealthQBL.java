package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.f1print.*;

/**
 * <p>Title: Web业务系统体检资料录入结论部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWAutoHealthQBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData ;
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;
        private String mManageCom;
	private String mpassflag; //通过标记
	private int merrcount; //错误条数
	private String mCalCode; //计算编码
	private String mUser;
	private FDate fDate = new FDate();
	private double mValue;
        private String mInsuredNo = "";

	/** 业务处理相关变量 */
	private LCPolSet mLCPolSet = new LCPolSet();
	private LCPolSet mmLCPolSet = new LCPolSet();
	private LCPolSet m2LCPolSet = new LCPolSet();
	private LCPolSet mAllLCPolSet = new LCPolSet();
	private LCPolSchema mLCPolSchema = new LCPolSchema();
	private String mPolNo = "";
	private String mOldPolNo = "";
	/** 保费项表 */
	private LCPremSet mLCPremSet = new LCPremSet();
	private LCPremSet mAllLCPremSet = new LCPremSet();
	/** 领取项表 */
	private LCGetSet mLCGetSet = new LCGetSet();
	private LCGetSet mAllLCGetSet = new LCGetSet();
	/** 责任表 */
	private LCDutySet mLCDutySet = new LCDutySet();
	private LCDutySet mAllLCDutySet = new LCDutySet();
	/** 特别约定表 */
	private LCSpecSet mLCSpecSet = new LCSpecSet();
	private LCSpecSet mAllLCSpecSet = new LCSpecSet();
	/** 特别约定注释表 */
	private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
	private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();
	/** 核保主表 */
	private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
	private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
        private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();
	/** 核保子表 */
	private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
	private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
        private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();
	/** 核保错误信息表 */
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
	private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();
	/** 告知表 */
	private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
	private LCCustomerImpartSet mAllLCCustomerImpartSet = new LCCustomerImpartSet();
	/** 投保人表 */
	private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
	private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();
	/** 受益人表 */
	private LCBnfSet mLCBnfSet = new LCBnfSet();
	private LCBnfSet mAllLCBnfSet = new LCBnfSet();
	/** 被保险人表 */
	private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
	private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();
        /** 体检资料主表 */
        private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
        private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();
        private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();
        /** 体检资料项目表 */
        private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
        private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
        private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();
        /** 打印管理表 */
        private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
        private LOPRTManagerSet mAllLOPRTManagerSet = new LOPRTManagerSet();
	/**计算公式表**/
	private LMUWSchema mLMUWSchema = new LMUWSchema();
	//private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
	private LMUWSet mLMUWSet = new LMUWSet();

	private CalBase mCalBase = new CalBase();

        private GlobalInput mGlobalInput = new GlobalInput();

	public UWAutoHealthQBL() {}

	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
	public boolean submitData(VData cInputData,String cOperate)
	{
		int flag = 0; //判断是不是所有数据都不成功
		int j = 0; //符合条件数据个数

		//将操作数据拷贝到本类中
		mInputData = (VData)cInputData.clone();

		System.out.println("---1---");
	   	//得到外部传入的数据,将数据备份到本类中
	    	if (!getInputData(cInputData))
	        	return false;
		System.out.println("---UWAutoHealthQBL getInputData---");

                mLCPENoticeSchema = ( LCPENoticeSchema )mLCPENoticeSet.get( 1 );
                mOldPolNo = mLCPENoticeSchema.getContNo();
                mPolNo = mLCPENoticeSchema.getContNo();

                System.out.println("---UWAutoHealthQBL checkData---");
                // 数据操作业务处理
                if (!dealData())
                  //continue;
                  return false;

		System.out.println("---UWAutoHealthQBL dealData---");
	    	//准备给后台的数据
	    	prepareOutputData();

		System.out.println("---UWAutoHealthQBL prepareOutputData---");
		//数据提交
	    	UWAutoHealthQBLS tUWAutoHealthQBLS = new UWAutoHealthQBLS();
		System.out.println("Start UWAutoHealthQBL Submit...");
		if (!tUWAutoHealthQBLS.submitData(mInputData,mOperate))
	    	{
			// @@错误处理
			this.mErrors.copyAllErrors(tUWAutoHealthQBLS.mErrors);
			CError tError = new CError();
			tError.moduleName = "UWAutoHealthQBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		}
		System.out.println("---UWAutoHealthQBL commitData---");
	    return true;
	}

	/**
	* 数据操作类业务处理
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	private boolean dealData()
	{

		if (dealOnePol() == false)
			return false;

		return true;
	}

	/**
	* 操作一张保单的业务处理
	* 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	private boolean dealOnePol()
	{
		// 健康信息
		if (prepareHealth() == false)
			return false;

		LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
		tLCPENoticeSet.set( mLCPENoticeSet );
		mAllLCPENoticeSet.add( tLCPENoticeSet );

		return true;
	}

	/**
	* 从输入数据中得到所有对象
	*输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	*/
	private boolean getInputData(VData cInputData)
	{
	        GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
		mOperate = tGlobalInput.Operator;
                mManageCom = tGlobalInput.ManageCom;


		//mmLCPolSet.set((LCPolSet)cInputData.getObjectByObjectName("LCPolSet",0));
                mLCPENoticeSet.set((LCPENoticeSet)cInputData.getObjectByObjectName("LCPENoticeSet",0));
                //mmLCPENoticeItemSet.set((LCPENoticeItemSet)cInputData.getObjectByObjectName("LCPENoticeItemSet",0));
                mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
		int n = mLCPENoticeSet.size();
		int flag = 0; //怕判断是不是所有保单都失败
		int j = 0; //符合条件保单个数


		if (n > 0)
		{
                        LCPolDB tLCPolDB = new LCPolDB();
			LCPolSchema tLCPolSchema = mmLCPolSet.get(1);

                        //取被保人客户号
                        LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
                        tLCPENoticeSchema = mLCPENoticeSet.get(1);
                        mInsuredNo = tLCPENoticeSchema.getCustomerNo();
                        mPolNo = tLCPENoticeSchema.getContNo();

			tLCPolDB.setPolNo(mPolNo);
			if (tLCPolDB.getInfo() == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors( tLCPolDB.mErrors );
				CError tError = new CError();
				tError.moduleName = "UWAutoHealthQBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "投保单查询失败!";
				this.mErrors .addOneError(tError) ;
				return false;
			}
			else
			{
				flag = 1;
			}

        	}

        	if (flag == 0)
        	{
        		return false;
        	}
        	else
        	{
        		return true;
        	}
	}

	/**
	* 准备体检资料信息
	* 输出：如果发生错误则返回false,否则返回true
	*/
	private boolean prepareHealth()
	{
          LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
          LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

          tLCPENoticeDB.setContNo(mPolNo);
          tLCPENoticeDB.setCustomerNo(mInsuredNo);

          if(tLCPENoticeDB.getInfo() == false)
          {
            CError tError = new CError();
            tError.moduleName = "UWAutoHealthQBL";
            tError.functionName = "prepareHealth";
            tError.errorMessage = "没有体检资料信息!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          tLCPENoticeSchema = tLCPENoticeDB.getSchema();

          if(tLCPENoticeSchema.getPEResult() != null)
          {
            CError tError = new CError();
            tError.moduleName = "UWAutoHealthQBL";
            tError.functionName = "prepareHealth";
            tError.errorMessage = "已经录入结论!";
            this.mErrors .addOneError(tError) ;
            return false;
          }

          tLCPENoticeSchema.setPEResult(mLCPENoticeSchema.getPEResult());

          mLCPENoticeSet.clear();
          mLCPENoticeSet.add(tLCPENoticeSchema);

          return true;
	}

	/**
	* 准备需要保存的数据
	*/
	private void prepareOutputData()
	{
		mInputData.clear();
		mInputData.add( mAllLCPENoticeSet );
	}

}