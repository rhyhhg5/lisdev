package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统承保个人单自动核保部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 2.0
 */
public class LRAutoCheckBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    MMap mMap = new MMap();
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperator;
    private String mManagecom;
    private String mContPassFlag = "0"; //合同通过标记
    private String mUWGrade = "1";
    private String mCalCode; //计算编码
    private double mValue;
    private String mContNo = "";
    //再保自核是否全部通过
    private boolean UwSuccFlg = true;
    private String mUwTimes = "";//保单再保规则次数

    /** 再保自核错误信息表 */
    private LRUWErrorSet mLRUWErrorSet = new LRUWErrorSet();
    private CalBase mCalBase = new CalBase();

    public LRAutoCheckBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中

        System.out.println("---1---");

        //得到外部传入的数据,将数据备份到本类中
        LCContSchema tLCContSchema = getInputData(cInputData);

        if (tLCContSchema == null) {
            return false;
        }

        System.out.println("---LRAutoCheckBL getInputData---");

        if (!checkData(tLCContSchema)) { //验证数据
            return false;
        }

        if (!dealData(tLCContSchema)) { //数据处理
            return false;
        }

        System.out.println("---LRAutoCheckBL dealData---");

        //准备给后台的数据
        mResult.clear();

        prepareOutputData(tLCContSchema);

        mResult.add(mMap);

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, "")) { //数据提交
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LRAutoCheckBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        cInputData.clear();
        cInputData.add(mContPassFlag); //给调用程序（团单自动核保）回传核保结论
        cInputData.add(mUWGrade); //给调用程序（团单自动核保）回传核保级别

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData(LCContSchema tLCContSchema) {
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        LCPolSchema tLCPolSchema = null;

        mContNo = tLCContSchema.getContNo(); //获得合同保单号

        tLCPolDB.setContNo(mContNo);
        tLCPolSet = tLCPolDB.query(); //查询合同下的所有险种

        int nPolCount = tLCPolSet.size();
        int nPolIndex = 0;

        LMUWSet tLMUWSetUnpass = new LMUWSet(); //未通过的核保规则
        LMUWSet tLMUWSetAll = null; //所有核保规则
        LMUWSchema tLMUWSchema = null;

        for (nPolIndex = 1; nPolIndex <= nPolCount; nPolIndex++) {
            tLCPolSchema = tLCPolSet.get(nPolIndex);

            //准备算法，获取某险种的所有核保规则的集合
            tLMUWSetUnpass.clear();

            if (tLMUWSetAll != null) {
                tLMUWSetAll.clear();
            }

            tLMUWSetAll = CheckKinds(tLCPolSchema); //获得该险种全部核保规则

            if (tLMUWSetAll == null) {
                return false;
            }

            CheckPolInit(tLCPolSchema); //准备数据，从险种信息中获取各项计算信息

            int n = tLMUWSetAll.size(); //核保规则数量

            if (n == 0) {
            	
            } else { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
                for (int i = 1; i <= n; i++) { //对每一核保规则进行循环
                    //取计算编码
                    tLMUWSchema = new LMUWSchema();
                    tLMUWSchema = tLMUWSetAll.get(i);
                    mCalCode = tLMUWSchema.getCalCode();
                    if (CheckPol(tLCPolSchema.getInsuredNo(),tLCPolSchema.getAppntNo(),
                                 tLCPolSchema.getRiskCode()) == 0) { //检验核保规则
                    } else { //核保不通过
                    	UwSuccFlg = false;
                    	tLMUWSetUnpass.add(tLMUWSchema);
                    }
                }
            }
           preparePolUW(tLCPolSchema, tLMUWSetUnpass); //此处已经将再保自核错误信息添加进 mLRUWErrorSet
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private LCContSchema getInputData(VData cInputData) { //rewrited by zhangrong 2004.11.16
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperator = tGlobalInput.Operator;
        mManagecom = tGlobalInput.ManageCom;
        
        TransferData tRTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        
        if(tRTransferData == null){
        	 CError tError = new CError();
             tError.moduleName = "LRAutoCheckBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "再保自核获取参数失败!";
             this.mErrors.addOneError(tError);

             return null;
        }
        mUwTimes = (String)tRTransferData.getValueByName("UwTimes");
        if(mUwTimes==null || "".equals(mUwTimes)){
        	CError tError = new CError();
            tError.moduleName = "LRAutoCheckBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "再保自核获取自核次数失败!";
            this.mErrors.addOneError(tError);

            return null;
        }
        
        LCContSchema tLCContSchema = (LCContSchema) cInputData.
                                     getObjectByObjectName("LCContSchema", 0); //从输入数据中获取合同记录的数据
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tLCContSchema.getContNo());
        if (tLCContDB.getInfo()) { //验证LCCont表中是否存在该合同项记录
            return tLCContDB.getSchema();
        } else {
            CError tError = new CError();
            tError.moduleName = "LRAutoCheckBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "合同号为" + tLCContSchema.getContNo() + "未查询到!";
            this.mErrors.addOneError(tError);

            return null;
        }
    }

    private boolean checkData(LCContSchema tLCContSchema) {

        return true;
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds(LCPolSchema tLCPolSchema) {
        String tsql = "select * from lmuw where uwtype = 'RI' and riskcode = '"+tLCPolSchema.getRiskCode()+"'  order by calcode";
        //查询算法编码
        LMUWDB tLMUWDB = new LMUWDB();
        LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds";
            tError.errorMessage = tLCPolSchema.getRiskCode().trim() +
                                  "险种核保信息查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();

            return null;
        }
        return tLMUWSet;
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(LCPolSchema tLCPolSchema) {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCPolSchema.getPrem());
        mCalBase.setGet(tLCPolSchema.getAmnt());
        mCalBase.setMult(tLCPolSchema.getMult());
        mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
        mCalBase.setSex(tLCPolSchema.getInsuredSex());
        mCalBase.setJob(tLCPolSchema.getOccupationType());
        mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
        mCalBase.setPolNo(tLCPolSchema.getPolNo());
        mCalBase.setGrpContNo(tLCPolSchema.getGrpContNo());
        mCalBase.setContNo(mContNo);
    }

    /**
     * 个人单核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private double CheckPol(String tInsuredNo,String tAppntNo, String tRiskCode) { //LCPolSchema tLCPolSchema)
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        String tSql = "select  grppolno from lcgrppol where grpcontno = '"+mCalBase.getGrpContNo()+"' and riskcode = '"+tRiskCode+"' ";
        String tGrpPolNo = new ExeSQL().getOneValue(tSql);
        //增加基本要素
        mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
        mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());        
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        mCalculator.addBasicFactor("GrpPolNo", tGrpPolNo);
        mCalculator.addBasicFactor("InsuredNo", tInsuredNo);
        mCalculator.addBasicFactor("AppntNo", tAppntNo);
        mCalculator.addBasicFactor("RiskCode", tRiskCode);

        String tStr = "";
        tStr = mCalculator.calculate();

        if ((tStr == null) || tStr.trim().equals("")) {
            mValue = 0;
        } else {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);

        return mValue;
    }


    /**
     * 准备险种核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void preparePolUW(LCPolSchema tLCPolSchema,LMUWSet tLMUWSet) {
        
        //取核保错误信息
        //mLRUWErrorSet.clear();
        for(int i=1;i<=tLMUWSet.size();i++){
        	 //取出错信息
        	LMUWSchema tLMUWSchema = new LMUWSchema();
        	tLMUWSchema = tLMUWSet.get(i);
    	    LRUWErrorSchema tLRUWErrorSchema = new LRUWErrorSchema();
    	    //生成流水号
    	    String tserialno = PubFun1.CreateMaxNo("LRUWSerNo",20);
    	    tLRUWErrorSchema.setSerialNo(tserialno);
    	    tLRUWErrorSchema.setPrtNo(tLCPolSchema.getPrtNo());
    	    tLRUWErrorSchema.setProposalNo(tLCPolSchema.getProposalNo());
    	    tLRUWErrorSchema.setRiskCode(tLCPolSchema.getRiskCode());
    	    tLRUWErrorSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
    	    tLRUWErrorSchema.setInsuredName(tLCPolSchema.getInsuredName());
    	    tLRUWErrorSchema.setAppntNo(tLCPolSchema.getAppntNo());
    	    tLRUWErrorSchema.setAppntName(tLCPolSchema.getAppntName());
    	    tLRUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode());
    	    tLRUWErrorSchema.setUWError(tLMUWSchema.getRemark());
    	    tLRUWErrorSchema.setUWType("1");
    	    tLRUWErrorSchema.setUWTimes(mUwTimes+"");
    	    tLRUWErrorSchema.setManageCom(mManagecom);
    	    tLRUWErrorSchema.setOperator(mOperator);
    	    tLRUWErrorSchema.setMakeDate(PubFun.getCurrentDate());
    	    tLRUWErrorSchema.setMakeTime(PubFun.getCurrentTime());
    	    tLRUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    	    tLRUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    	    mLRUWErrorSet.add(tLRUWErrorSchema); //将自核中出错的信息添加进 mLCUWErrorSet
        }
    }

    /**
     * 准备需要保存的数据
     */
    private void prepareOutputData(LCContSchema tLCContSchema) {
        mMap.put(mLRUWErrorSet, "INSERT");
    }

	public boolean isUwSuccFlg() {
		return UwSuccFlg;
	}

	public void setUwSuccFlg(boolean uwSuccFlg) {
		UwSuccFlg = uwSuccFlg;
	}

}
