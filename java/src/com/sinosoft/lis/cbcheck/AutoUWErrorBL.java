package com.sinosoft.lis.cbcheck;

import org.jdom.Document;

import com.sinosoft.lis.brms.databus.RuleTransfer;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflow.tb.NewEngineRuleService;

/**
 * <p>Title: 自动核保 </p>
 * <p>Description: 再人工核保时手动自动核保</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: SinoSoft</p>
 * @author 张星
 * @version 1.0
 */

public class AutoUWErrorBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 往工作流引擎中传输数据的容器 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private TransferData mTransferData = new TransferData();

  /** 业务数据操作字符串 */
  private String mContNo;
  private String mPrtNo;
  private String mCalCode; //计算编码
  private double mValue;
  private String mPolNo;
  private String mUWNo;
  private int nUWNo = 0;

  /**保单表*/
  private LCContSchema mLCContSchema = new LCContSchema();

  /**险种表*/
  private LCPolSchema mLCPolSchema = new LCPolSchema();

  //未通过自核的险种核保规则
  private LMUWSet mLMUWSetUnpass = new LMUWSet();

  //未通过自核的合同核保规则
  LMUWSet mLMUWSetContUnpass = new LMUWSet(); //未通过的合同核保规则
  //险种自动核保信息
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

  //合同自动核保信息
  private LCCUWErrorSet mLCCUWErrorSet = new LCCUWErrorSet();
  private CalBase mCalBase = new CalBase();
  ExeSQL tExeSQL = new ExeSQL();

  public AutoUWErrorBL() {
  }

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData, cOperate)) {
      return false;
    }

    //校验是否有未打印的体检通知书
    if (!checkData()) {
      return false;
    }

    //进行业务处理
    if (!dealData(mLCContSchema)) {
      return false;
    }

    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }

    System.out.println("Start AutoUWErrorBL Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mResult, cOperate)) {
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }

    return true;
  }

  /**
   * 校验业务数据
   * @return
   */
  private boolean checkData() {
    //校验保单信息
    LCContDB tLCContDB = new LCContDB();
    tLCContDB.setContNo(mContNo);
    if (tLCContDB.getInfo()) { //验证LCCont表中是否存在该合同项记录
      mLCContSchema.setSchema(tLCContDB.getSchema());
    }
    else {
      CError tError = new CError();
      tError.moduleName = "AutoUWErrorBL";
      tError.functionName = "checkData";
      tError.errorMessage = "合同号为" + mLCContSchema.getContNo() + "未查询到!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData(LCContSchema tLCContSchema) {
    LCPolDB tLCPolDB = new LCPolDB();
    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolDB.setContNo(mContNo);
    tLCPolSet = tLCPolDB.query();
    this.mPrtNo = tLCPolSet.get(1).getPrtNo();
    int nPolCount = tLCPolSet.size();
    int nPolIndex = 0;
    ExeSQL tExeSQL=new ExeSQL();
    String tSQL="select code from ldcode where codetype='QYBZRuleButton' with ur";
	String tRuleButton=tExeSQL.getOneValue(tSQL);
	tSQL="select comcode from ldcode where codetype='CardFlagRuleButton' and code='" +
	tLCContSchema.getCardFlag()+"' with ur";
	String tCardFlagButton=tExeSQL.getOneValue(tSQL);
	if(tRuleButton!=null && tRuleButton.equals("00") 
    		&& (tCardFlagButton==null||!tCardFlagButton.equals("11"))){
		try{
//    	调用规则引擎	
		RuleTransfer ruleTransfer = new RuleTransfer();
		//获得规则引擎返回的校验信息 调用投保规则
		String xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NP", mContNo, false);
		//将校验信息转换为Xml的document对象
		Document doc = ruleTransfer.stringToDoc(xmlStr);
		String approved = ruleTransfer.getApproved(doc);
		if(approved==null){
			CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行规则引擎投保规则时出错";
            this.mErrors.addOneError(tError);
            return false;
		}
		if("1".equals(approved)){
		}else if("0".equals(approved) || "2".equals(approved)){
		}else if("-1".equals(approved)){
			CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "规则引擎投保规则执行异常";
            this.mErrors.addOneError(tError);
            return false;
		}else {
			CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "规则引擎投保规则返回了未知的错误类型";
            this.mErrors.addOneError(tError);
            return false;
		    }
		LMUWSet tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的投保规则
		LMUWSchema tempLMUWSchema=new LMUWSchema();
		String errorMessage="<br>";
		String tempRiskcode="";
		String tempInsured="";
		
		// modify by zxs
		NewEngineRuleService newEngineRule = new NewEngineRuleService();
		MMap map = newEngineRule.dealNPData(tLCContSchema,"NewQYBZRuleButton");
		VData mData = new VData();
		if(tLMUWSetPolAllUnpass.size()>0){
			for(int m=1;m<=tLMUWSetPolAllUnpass.size();m++){
				
				// modify by zxs
				LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
				EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
				engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
				engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
				engineRuleSchema.setApproved(approved);
				engineRuleSchema.setRuleResource("old");
				engineRuleSchema.setRuleType("NP");
				engineRuleSchema.setContType("标准保单");
				engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
				engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
				engineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
				engineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
				if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
					engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
				}else{
					engineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
				}
				engineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
				map.put(engineRuleSchema, "INSERT");
				// modify by zxs
				tempRiskcode="";
				tempInsured="";
				if(tLMUWSetPolAllUnpass.get(m).getRiskName()!=null 
						&& !tLMUWSetPolAllUnpass.get(m).getRiskName().equals("")){
					tempInsured+="被保人"+tLMUWSetPolAllUnpass.get(m).getRiskName();
					if(tLMUWSetPolAllUnpass.get(m).getRiskCode()!=null
							&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
							&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("")){
						tempRiskcode+="，险种代码"+tLMUWSetPolAllUnpass.get(m).getRiskCode()+"，";
					}else{
						tempInsured+="，";
					}
				}else{
					if(tLMUWSetPolAllUnpass.get(m).getRiskCode()!=null
							&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("000000")
							&&!tLMUWSetPolAllUnpass.get(m).getRiskCode().equals("")){
						tempRiskcode+="险种代码"+tLMUWSetPolAllUnpass.get(m).getRiskCode()+"，";
					}
				}
				errorMessage+=tLMUWSetPolAllUnpass.get(m).getUWCode();
				errorMessage+="：";
				errorMessage+=tempInsured;
				errorMessage+=tempRiskcode;
				errorMessage+=tLMUWSetPolAllUnpass.get(m).getRemark();
				errorMessage+="<br>";
			}
			
			mData.add(map);
			PubSubmit pubSubmit = new PubSubmit();
			if (!pubSubmit.submitData(mData, "")) {
				this.mErrors.addOneError(pubSubmit.mErrors.getContent());
				return false;
			}
			// modify by zxs
			
//			 @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = errorMessage;
            this.mErrors.addOneError(tError);
			return false;
		}else{
			//modify by zxs
			EngineRuleSchema engineRuleSchema = new EngineRuleSchema();
			engineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
			engineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
			engineRuleSchema.setApproved(approved);
			engineRuleSchema.setRuleResource("old");
			engineRuleSchema.setRuleType("NP");
			engineRuleSchema.setContType("标准保单");
			engineRuleSchema.setMakeDate(PubFun.getCurrentDate());
			engineRuleSchema.setMakeTime(PubFun.getCurrentTime());
			map.put(engineRuleSchema, "INSERT");
			//modify by zxs
		}
//		获得规则引擎返回的校验信息 调用核保规则
		xmlStr = ruleTransfer.getXmlStr("lis", "uw", "NU", mContNo, false);
		//将校验信息转换为Xml的document对象
		doc = ruleTransfer.stringToDoc(xmlStr);
		
		NewEngineRuleService newEngineRule1 = new NewEngineRuleService();
		MMap map1 = newEngineRule1.dealNUData(tLCContSchema,"NewQYBZRuleButton");
		
		//获得校验结果 1自核通过，-1执行异常，0自核不通过
		approved = ruleTransfer.getApproved(doc);
		tLMUWSetPolAllUnpass = ruleTransfer.getAllPolUWMSG(doc); //未通过的投保规则
		if (tLMUWSetPolAllUnpass.size() > 0) {
			for (int m = 1; m <= tLMUWSetPolAllUnpass.size(); m++) {
				LMUWSchema tLMUWSChema = tLMUWSetPolAllUnpass.get(m);
				// modify by zxs
				EngineRuleSchema nuEngineRuleSchema = new EngineRuleSchema();
				nuEngineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
				nuEngineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
				nuEngineRuleSchema.setApproved(approved);
				nuEngineRuleSchema.setRuleResource("old");
				nuEngineRuleSchema.setRuleType("NU");
				nuEngineRuleSchema.setContType("标准保单");
				nuEngineRuleSchema.setMakeDate(PubFun.getCurrentDate());
				nuEngineRuleSchema.setMakeTime(PubFun.getCurrentTime());
				nuEngineRuleSchema.setRuleName(tLMUWSChema.getUWCode());
				nuEngineRuleSchema.setRiskCode(tLMUWSChema.getRiskCode());
				if(tLMUWSChema.getOthCalCode().indexOf("'")!=-1){
					nuEngineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode().replace("'", "''"));
				}else{
					nuEngineRuleSchema.setInsuredNo(tLMUWSChema.getOthCalCode());
				}
				nuEngineRuleSchema.setReturnInfo(tLMUWSChema.getRemark());
				map1.put(nuEngineRuleSchema, "INSERT");
			}
		}else{
			// modify by zxs
			EngineRuleSchema nuEngineRuleSchema = new EngineRuleSchema();
			nuEngineRuleSchema.setSerialNo(PubFun1.CreateMaxNo("ENGINERULE", 10));
			nuEngineRuleSchema.setPrtno(tLCContSchema.getPrtNo());
			nuEngineRuleSchema.setApproved(approved);
			nuEngineRuleSchema.setRuleResource("old");
			nuEngineRuleSchema.setRuleType("NU");
			nuEngineRuleSchema.setContType("标准保单");
			nuEngineRuleSchema.setMakeDate(PubFun.getCurrentDate());
			nuEngineRuleSchema.setMakeTime(PubFun.getCurrentTime());
			map1.put(nuEngineRuleSchema, "INSERT");
		}
		mData.add(map);
		PubSubmit pubSubmit = new PubSubmit();
		if (!pubSubmit.submitData(mData, "")) {
			this.mErrors.addOneError(pubSubmit.mErrors.getContent());
			return false;
		}
		PubSubmit pubSubmit1 = new PubSubmit();
		VData vData = new VData();
		vData.add(map1);
		if (!pubSubmit1.submitData(vData, "")) {
			this.mErrors.addOneError(pubSubmit1.mErrors.getContent());
			return false;
		}
		// modify by zxs
		if(approved==null){
			CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行规则引擎核保规则时出错";
            this.mErrors.addOneError(tError);
            return false;
		}
		if("1".equals(approved)||"0".equals(approved) || "2".equals(approved)){
			LMUWSet tLMUWSetPolUnpass=null;
            LMUWSet tLMUWSetCommonPolUnpass=null;
			for (nPolIndex = 1; nPolIndex <= nPolCount; nPolIndex++) {
				mLCPolSchema = tLCPolSet.get(nPolIndex);
			      //险种编码
			      mPolNo = mLCPolSchema.getPolNo();
			      String riskcode = mLCPolSchema.getRiskCode(); //获得保单险种号
			      if (mLMUWSetUnpass != null) {
			    	  mLMUWSetUnpass.clear();
			        }
			      //获取当前险种的未通过信息
			      mLMUWSetUnpass = ruleTransfer.getSinglePolUWMSG(tLMUWSetPolAllUnpass, riskcode,mLCPolSchema.getInsuredNo());
			      String tSql = "select max(a) from ( select  max(uwno) + 1 a from LCCUWError where contno = '" +
                mContNo + "'"
                + " union select max(uwno) + 1 a from LCUWError where contno = '" +
                mContNo + "' ) as x ";
            mUWNo = tExeSQL.getOneValue(tSql);

            if (mUWNo.equals("") || mUWNo.equals(null)) {
              nUWNo = 1;
            }
            else {
              nUWNo = Integer.parseInt(mUWNo);
            }

            //险种自核信息
            if (!prepareLCUWError()) {
              return false;
            }  
				
			}
			mLMUWSetContUnpass=ruleTransfer.getCommonPolUWMSG(tLMUWSetPolAllUnpass);
			//			合同自核信息
		    if (!prepareLCCUWError()) {
		      return false;
		    }
		}else if("-1".equals(approved)){
			CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "规则引擎核保规则执行异常";
            this.mErrors.addOneError(tError);
            return false;
		}else {
			CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "规则引擎核保规则返回了未知的错误类型";
            this.mErrors.addOneError(tError);
            return false;
		    }
	}catch(Exception ex){
		CError tError = new CError();
        tError.moduleName = "UWAutoChkBL";
        tError.functionName = "dealData";
        tError.errorMessage = "执行规则引擎时出错";
        this.mErrors.addOneError(tError);
        return false;
	}
		
    }else{
        /**
         * 险种核保
         */

        LMUWSet tLMUWRiskContSet = null; //所有险种自核规则

        LMUWSchema tLMUWSchema = null;
        for (nPolIndex = 1; nPolIndex <= nPolCount; nPolIndex++) {
          mLCPolSchema = tLCPolSet.get(nPolIndex);
          //险种编码
          mPolNo = mLCPolSchema.getPolNo();
          //准备算法，获取某险种的所有核保规则的集合
          mLMUWSetUnpass.clear();
          if (tLMUWRiskContSet != null) {
            tLMUWRiskContSet.clear();
          }
          //险种自核规则
          tLMUWRiskContSet = CheckKinds(mLCPolSchema);
          if (tLMUWRiskContSet == null) {
            return false;
          }

          //准备数据，从险种信息中获取各项计算信息
          CheckPolInit(mLCPolSchema);
          int n = tLMUWRiskContSet.size(); //核保规则数量
          for (int i = 1; i <= n; i++) {
            //取计算编码
            tLMUWSchema = new LMUWSchema();
            tLMUWSchema = tLMUWRiskContSet.get(i);
            mCalCode = tLMUWSchema.getCalCode();
            if (CheckPol(mLCPolSchema.getInsuredNo(), mLCPolSchema.getRiskCode()) ==
                0) {
            }
            else {
              mLMUWSetUnpass.add(tLMUWSchema);
            }
          }

          String tSql = "select max(a) from ( select  max(uwno) + 1 a from LCCUWError where contno = '" +
              mContNo + "'"
              + " union select max(uwno) + 1 a from LCUWError where contno = '" +
              mContNo + "' ) as x ";
          mUWNo = tExeSQL.getOneValue(tSql);

          if (mUWNo.equals("") || mUWNo.equals(null)) {
            nUWNo = 1;
          }
          else {
            nUWNo = Integer.parseInt(mUWNo);
          }

          //险种自核信息
          if (!prepareLCUWError()) {
            return false;
          }

        }

//        /* 合同核保 */
    //
        LMUWSet tLMUWSetContAll = CheckKinds3(); //所有合同核保规则

        //准备数据，从险种信息中获取各项计算信息
        CheckContInit(tLCContSchema);

        /**
            个人合同核保
         */
        int tCount = tLMUWSetContAll.size(); //核保规则数量
        if (tCount == 0) {

        }
        else { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
          int j = 0;
          for (int index = 1; index <= tCount; index++) {
            //取计算编码
            tLMUWSchema = new LMUWSchema();
            tLMUWSchema = tLMUWSetContAll.get(index);
            mCalCode = tLMUWSchema.getCalCode();
            if (CheckPol(tLCContSchema.getInsuredNo(), "000000") == 0) {
            }
            else {
              j++;
              mLMUWSetContUnpass.add(tLMUWSchema);

            }
          }
        }

        //合同自核信息
        if (!prepareLCCUWError()) {
          return false;
        }
    }
    return true;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private LMUWSet CheckKinds(LCPolSchema tLCPolSchema) {
    String tsql = "";

    int tType =  mTransferData.findIndexByName("Type");
    //查询算法编码
    if (tType == 1)
  {
      tsql = "select * from lmuw where riskcode = '" +
          tLCPolSchema.getRiskCode().trim() +
        "' and relapoltype = 'I' and uwtype = '1' and passflag = 'R'  order by calcode";

     LMUWDB tLMUWDB = new LMUWDB();
     LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
     if(tLMUWDB.mErrors.needDealError())
     {
         CError tError = new CError();
         tError.moduleName = "AutoUWErrorBL";
         tError.functionName = "CheckKinds";
         tError.errorMessage = "查询出错";
         mErrors.addOneError(tError);
         System.out.println(tError.errorMessage);
         return null;
     }
     else if (tLMUWSet.size() == 0)
     {
         System.out.println("不需要发起再保:" + tLCPolSchema.getRiskCode());
         return tLMUWSet;
     }
    }
    else
    {
    tsql = "select * from lmuw where (riskcode = '000000' and relapoltype = 'I' and uwtype = '11') or (riskcode = '" +
        tLCPolSchema.getRiskCode().trim() +
        "' and relapoltype = 'I' and uwtype = '1' and (passflag is null or passflag <> 'R'))  order by calcode";
    }
    System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"+tType);
    System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"+tsql);
    LMUWDB tLMUWDB = new LMUWDB();

    LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
    if (tLMUWDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoChkBL";
      tError.functionName = "CheckKinds";
      tError.errorMessage = tLCPolSchema.getRiskCode().trim() +
          "险种核保信息查询失败!";
      this.mErrors.addOneError(tError);
      tLMUWSet.clear();
      return null;
    }
    return tLMUWSet;
  }

  /**
   * 核保险种信息校验,准备核保算法
   * 输出：如果发生错误则返回false,否则返回true
   */
  private LMUWSet CheckKinds3() {
    String tsql = "";
    int tType =  mTransferData.findIndexByName("Type");
    //查询算法编码
    tsql =
        "select * from lmuw where riskcode = '000000' and relapoltype = 'I' and uwtype = '19'";
        LMUWDB tLMUWDB = new LMUWDB();

    LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);
    if (tLMUWDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLMUWDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoChkBL";
      tError.functionName = "CheckKinds3";
      tError.errorMessage = "合同险种核保信息查询失败!";
      this.mErrors.addOneError(tError);
      tLMUWSet.clear();
      return null;
    }
    return tLMUWSet;
  }

  /**
   * 个人单核保数据准备
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void CheckPolInit(LCPolSchema tLCPolSchema) {
    mCalBase = new CalBase();
    mCalBase.setPrem(tLCPolSchema.getPrem());
    mCalBase.setGet(tLCPolSchema.getAmnt());
    mCalBase.setMult(tLCPolSchema.getMult());
    mCalBase.setAppAge(tLCPolSchema.getInsuredAppAge());
    mCalBase.setSex(tLCPolSchema.getInsuredSex());
    mCalBase.setJob(tLCPolSchema.getOccupationType());
    mCalBase.setCount(tLCPolSchema.getInsuredPeoples());
    mCalBase.setPolNo(tLCPolSchema.getPolNo());
    mCalBase.setContNo(mContNo);
    mCalBase.setCValiDate(tLCPolSchema.getCValiDate());
  }

  /**
   * 个人单核保数据准备
   * 输出：如果发生错误则返回false,否则返回true
   */
  private void CheckContInit(LCContSchema tLCContSchema) {
    mCalBase = new CalBase();
    mCalBase.setPrem(tLCContSchema.getPrem());
    mCalBase.setGet(tLCContSchema.getAmnt());
    mCalBase.setMult(tLCContSchema.getMult());
    mCalBase.setSex(tLCContSchema.getInsuredSex());
    mCalBase.setCValiDate(tLCContSchema.getCValiDate());
    mCalBase.setContNo(mContNo);
  }

  /**
   * 个人单核保
   * 输出：如果发生错误则返回false,否则返回true
   */
  private double CheckPol(String tInsuredNo, String tRiskCode) { //LCPolSchema tLCPolSchema)
    // 计算
    Calculator mCalculator = new Calculator();
    mCalculator.setCalCode(mCalCode);
    //增加基本要素
    mCalculator.addBasicFactor("Get", mCalBase.getGet());
    mCalculator.addBasicFactor("Mult", mCalBase.getMult());
    mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
    mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
    mCalculator.addBasicFactor("Sex", mCalBase.getSex());
    mCalculator.addBasicFactor("Job", mCalBase.getJob());
    mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
    mCalculator.addBasicFactor("GetStartDate", "");
    mCalculator.addBasicFactor("Years", mCalBase.getYears());
    mCalculator.addBasicFactor("Grp", "");
    mCalculator.addBasicFactor("GetFlag", "");
    mCalculator.addBasicFactor("CValiDate", mCalBase.getCValiDate());
    mCalculator.addBasicFactor("Count", mCalBase.getCount());
    mCalculator.addBasicFactor("FirstPayDate", "");
    mCalculator.addBasicFactor("ContNo", mCalBase.getContNo());
    mCalculator.addBasicFactor("PolNo", mCalBase.getPolNo());
    mCalculator.addBasicFactor("InsuredNo", tInsuredNo); //tLCPolSchema.getInsuredNo());;
    mCalculator.addBasicFactor("RiskCode", tRiskCode); //tLCPolSchema.getRiskCode());;
    mCalculator.addBasicFactor("PrtNo", this.mPrtNo);
    String tStr = "";
    tStr = mCalculator.calculate();
    if (tStr == null || tStr.trim().equals("")) {
      mValue = 0;
    }
    else {
      mValue = Double.parseDouble(tStr);

    }
    System.out.println(mValue);
    return mValue;
  }

  /**
   * 准备险种自动核保信息
   * @return boolean
   */
  private boolean prepareLCUWError() {
    // 核保错误信息表
    LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
    LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
    tLCUWErrorDB.setPolNo(mPolNo);
    int tType =  mTransferData.findIndexByName("Type");

    if (tLCUWErrorDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAtuoChkBL";
      tError.functionName = "prepareUW";
      tError.errorMessage = mPolNo + "个人错误信息表查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    tLCUWErrorSchema.setUWNo(nUWNo);
    tLCUWErrorSchema.setContNo(mContNo);
    tLCUWErrorSchema.setGrpContNo(mLCContSchema.getGrpContNo());
    tLCUWErrorSchema.setProposalContNo(mContNo);
    tLCUWErrorSchema.setPolNo(mPolNo);
    tLCUWErrorSchema.setProposalNo(mLCPolSchema.getProposalNo());
    tLCUWErrorSchema.setInsuredNo(mLCPolSchema.getInsuredNo());
    tLCUWErrorSchema.setInsuredName(mLCPolSchema.getInsuredName());
    tLCUWErrorSchema.setAppntNo(mLCPolSchema.getAppntNo());
    tLCUWErrorSchema.setAppntName(mLCPolSchema.getAppntName());
    tLCUWErrorSchema.setManageCom(mLCPolSchema.getManageCom());
    tLCUWErrorSchema.setUWRuleCode(""); //核保规则编码
    tLCUWErrorSchema.setUWError(""); //核保出错信息
    tLCUWErrorSchema.setCurrValue(""); //当前值
    tLCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    tLCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    tLCUWErrorSchema.setManuUWFlag("Y"); //手动自动核保标志

    //取核保错误信息
   // mLCUWErrorSet.clear();
    int merrcount = mLMUWSetUnpass.size();
    if (merrcount > 0) {
      for (int i = 1; i <= merrcount; i++) {
        //取出错信息
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mLMUWSetUnpass.get(i);
        //生成流水号
        String tserialno = "" + i;

        tLCUWErrorSchema.setSerialNo(tserialno);
        tLCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
        tLCUWErrorSchema.setUWError(parseUWResult(tLMUWSchema)); //核保出错信息，即核保规则的文字描述内容
        tLCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
        tLCUWErrorSchema.setCurrValue(""); //当前值
        tLCUWErrorSchema.setSugPassFlag(tLMUWSchema.getPassFlag()); //picch需求对自核规则分类（体检、契调）

        LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
        ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
        mLCUWErrorSet.add(ttLCUWErrorSchema);
      }
    }

    return true;
  }

  /**
   * 合同核保错误信息表LCCUWError
   */

  private boolean prepareLCCUWError() {
    LCCUWErrorSchema tLCCUWErrorSchema = new LCCUWErrorSchema();
    LCCUWErrorDB tLCCUWErrorDB = new LCCUWErrorDB();
    tLCCUWErrorDB.setContNo(mContNo);
    LCCUWErrorSet tLCCUWErrorSet = new LCCUWErrorSet();
    tLCCUWErrorSet = tLCCUWErrorDB.query();
    if (tLCCUWErrorDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCCUWErrorDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAtuoChkAfterInitService";
      tError.functionName = "prepareContUW";
      tError.errorMessage = mContNo + "合同错误信息表查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    tLCCUWErrorSchema.setUWNo(nUWNo);

    tLCCUWErrorSchema.setContNo(mContNo);
    tLCCUWErrorSchema.setGrpContNo(mLCContSchema.getGrpContNo());
    tLCCUWErrorSchema.setProposalContNo(mLCContSchema.getProposalContNo());
    //tLCCUWErrorSchema.setInsuredNo(mLCContSchema.getInsuredNo());
    //tLCCUWErrorSchema.setInsuredName(mLCContSchema.getInsuredName());
    tLCCUWErrorSchema.setAppntNo(mLCContSchema.getAppntNo());
    tLCCUWErrorSchema.setAppntName(mLCContSchema.getAppntName());
    tLCCUWErrorSchema.setManageCom(mGlobalInput.ManageCom);
    tLCCUWErrorSchema.setUWRuleCode(""); //核保规则编码
    tLCCUWErrorSchema.setUWError(""); //核保出错信息
    tLCCUWErrorSchema.setCurrValue(""); //当前值
    tLCCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    tLCCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    tLCCUWErrorSchema.setManuUWFlag("Y"); //手动自动核保标志


    //取核保错误信息
    mLCCUWErrorSet.clear();
    int merrcount = mLMUWSetContUnpass.size();
    System.out.println("................merrcount"+merrcount);
    if (merrcount > 0) {
      for (int i = 1; i <= merrcount; i++) {
        //取出错信息
        LMUWSchema tLMUWSchema = new LMUWSchema();
        tLMUWSchema = mLMUWSetContUnpass.get(i);
        //生成流水号
        String tserialno = "" + i;

        tLCCUWErrorSchema.setSerialNo(tserialno);
        tLCCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
        tLCCUWErrorSchema.setUWError(parseUWResult(tLMUWSchema)); //核保出错信息，即核保规则的文字描述内容
        tLCCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
        tLCCUWErrorSchema.setCurrValue(""); //当前值
        LCCUWErrorSchema ttLCCUWErrorSchema = new LCCUWErrorSchema();
        ttLCCUWErrorSchema.setSchema(tLCCUWErrorSchema);
        mLCCUWErrorSet.add(ttLCCUWErrorSchema);

      }
    }
    System.out.println(".....mLCCUWErrorSet.size()"+mLCCUWErrorSet.size());
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData, String cOperate) {
    //从输入数据中得到所有对象
    //获得全局公共数据
    mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
        "TransferData", 0);

    if (mGlobalInput == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWAutoHealthAfterInitService";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    mContNo = (String) mTransferData.getValueByName("ContNo");
    if (mContNo == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "ProposalApproveAfterEndService";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输业务数据中mContNo失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 工作流准备后台提交数据
   * @return boolean
   */
  private boolean prepareOutputData() {
      if(1 == mTransferData.findIndexByName("Type")
          //&& mLCCUWErrorSet.size() == 0
          && mLCUWErrorSet.size() == 0)
      {
          CError tError = new CError();
          tError.moduleName = "AutoUWErrorBL";
          tError.functionName = "prepareOutputData";
          tError.errorMessage = "不需要发送再保审核";
          mErrors.addOneError(tError);
          System.out.println(tError.errorMessage);
          return false;
      }

    mResult.clear();
    MMap map = new MMap();
    mLCPolSchema.setReinsureFlag("2");
    map.put(mLCPolSchema, "UPDATE");
    //合同自核规则
    map.put(mLCCUWErrorSet, "INSERT");
    //险种自核规则
    map.put(mLCUWErrorSet, "INSERT");

    mResult.add(map);
    return true;
  }

  private String parseUWResult(LMUWSchema tLMUWSchema) {
    System.out.println(
        "GrpUWAutoChkBL.parseUWResult(tLMUWSchema)  \n--Line:1350  --Author:YangMing");
    String result = null;
    PubCalculator tPubCalculator = new PubCalculator();
    tPubCalculator.addBasicFactor("GrpContNo", this.mContNo);
    tPubCalculator.addBasicFactor("polno", this.mPolNo);
    String tSql = tLMUWSchema.getRemark().trim();
    String tStr = "";
    String tStr1 = "";
    try {
      while (true) {
        tStr = PubFun.getStr(tSql, 2, "$");
        if (tStr.equals("")) {
          break;
        }
        tPubCalculator.setCalSql(tStr);
        tStr1 = "$" + tStr.trim() + "$";
        //替换变量

        tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "Calculator";
      tError.functionName = "interpretFactorInSQL";
      tError.errorMessage = "解释" + tSql + "的变量:" + tStr + "时出错。";
      this.mErrors.addOneError(tError);
      return null;
    }
    if (tSql.trim().equals("")) {
      return tLMUWSchema.getRemark();
    }
    return tSql;
  }

  /**
   * 返回错误对象
   * @return CErrors
   */
  public CErrors getErrors() {
    return mErrors;
  }

  public static void main(String[] args) {
    AutoUWErrorBL tAutoUWErrorBL = new AutoUWErrorBL();
    GlobalInput tG = new GlobalInput();

    tG.Operator = "group";
    tG.ManageCom = "86";

    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo", "00089970401");
    tVData.add(tTransferData);
    tVData.add(tG);
    tAutoUWErrorBL.submitData(tVData, "");

  }
}
