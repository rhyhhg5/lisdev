package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统被保人资料变动功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author Tjj
 * @version 1.0
 */

public class UWModifyTypeBDBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  //操作符
  public String mOperate;

  public UWModifyTypeBDBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
	  LCPolSchema  mLCPolSchema= new LCPolSchema();
      LCDutySet mLCDutySet = new LCDutySet();

	   String Flag;

	   mLCPolSchema = (LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0);
	   mLCDutySet = (LCDutySet)cInputData.getObjectByObjectName("LCDutySet",1);


	   Connection conn = null;
	   conn = DBConnPool.getConnection();

	   if (conn==null)
	   {
		 // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "PEdorAppConfirmWTBLS";
		 tError.functionName = "saveData";
		 tError.errorMessage = "数据库连接失败!";
		 this.mErrors .addOneError(tError) ;
		 return false;
	   }

	   try
	   {
		 conn.setAutoCommit(false);

		 if (mLCDutySet != null && mLCDutySet.size() > 0)
		 {
		   for(int i = 1;i<=mLCDutySet.size() ;i++)
		   {
			 LCDutyDB tLCDutyDB = new LCDutyDB(conn);
		   tLCDutyDB.setSchema(mLCDutySet.get(i)) ;
			 if (!tLCDutyDB.update())
			 {
			   // @@错误处理
			   CError tError = new CError();
			   tError.moduleName = "UWModifyTypeBDBLS";
			   tError.functionName = "saveData";
			   tError.errorMessage = "红利领取方式变动信息更新失败!";
			   this.mErrors .addOneError(tError) ;
			   conn.rollback();
			   conn.close();
			   return false;
			 }
		   }
		 }


		 if (mLCPolSchema!=null)
		 {

		   LCPolDB tLCPolDB = new LCPolDB(conn);
		   tLCPolDB.setSchema(mLCPolSchema) ;
		   if (!tLCPolDB.update())
		   {
			 // @@错误处理
			 CError tError = new CError();
			 tError.moduleName = "PEdorBCDetailBLS";
			 tError.functionName = "saveData";
			 tError.errorMessage = "投保单红利领取方式信息保存失败!";
			 this.mErrors .addOneError(tError) ;
			 conn.rollback();
			 conn.close();
			 return false;
		   }
		 }

		 conn.commit();
		 conn.close();
	   } catch (Exception ex)
	   {
		 // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "PEdorBCDetailBLS";
		 tError.functionName = "submitData";
		 tError.errorMessage = ex.toString();
		 this.mErrors .addOneError(tError);
		 try
		 {
		   conn.rollback() ;
		   conn.close();
		   } catch(Exception e){}
		 return false;
	   }
	   return true;
  }

}