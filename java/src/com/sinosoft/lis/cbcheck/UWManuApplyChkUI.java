package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统人工核保投保单申请录入部分</p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWManuApplyChkUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public UWManuApplyChkUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "f03";
  tG.ManageCom = "86";

  LCPolSchema p = new LCPolSchema();
  p.setProposalNo( "86110020030110003887" );
  p.setPolNo("86110020030110003887");

  LCPolSet tLCPolSet = new LCPolSet();
  tLCPolSet.add(p);

  //
  LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

  tLCPENoticeSchema.setContNo("86110020030110001393");
  tLCPENoticeSchema.setCustomerNo("33");
  tLCPENoticeSchema.setPEAddress("001000000");
  tLCPENoticeSchema.setPEDate("2002-10-31");
  tLCPENoticeSchema.setPEBeforeCond("0");
  tLCPENoticeSchema.setRemark("TEST");

  LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
  tLCPENoticeSet.add(tLCPENoticeSchema);

  //
  LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();

  tLCPENoticeItemSchema.setPEItemCode("001");
  tLCPENoticeItemSchema.setPEItemName("普通体检");

  LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  tLCPENoticeItemSchema = new LCPENoticeItemSchema();
  tLCPENoticeItemSchema.setPEItemCode("002");
  tLCPENoticeItemSchema.setPEItemName("X光");
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();

  tLCIssuePolSchema.setBackObjType("1");
  tLCIssuePolSchema.setIssueCont("1234567");
  tLCIssuePolSchema.setOperatePos("1");
//  tLCIssuePolSchema.setProposalNo("86110020030110001397");
  tLCIssuePolSchema.setIssueType("100");

  LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();
  tLCIssuePolSet.add(tLCIssuePolSchema);

  VData tVData = new VData();
  tVData.add( tLCPolSet );
  tVData.add( tG );
  UWManuApplyChkUI ui = new UWManuApplyChkUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    UWManuApplyChkBL tUWManuApplyChkBL = new UWManuApplyChkBL();

    System.out.println("---QuestInputChkBL UI BEGIN---");
    if (tUWManuApplyChkBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tUWManuApplyChkBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWManuApplyChkUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}
    return true;
  }
}
