package com.sinosoft.lis.cbcheck;

import com.sinosoft.httpclient.inf.GetTaxCode;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetContTaxCodeBL {

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();
	
	private String mContNo = "";

	private String mPrtNo = "";

	private ExeSQL mExeSQL = new ExeSQL();

	private LCContSubDB mLCContSubDB = null;

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public boolean submitData(VData cInputData, String cOperate) {

		// 数据过滤器
		if (!getInputData(cInputData, cOperate)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		return true;
	}

	private boolean dealData() {

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mContNo);
		if (!tLCContDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		mLCContSubDB = new LCContSubDB();
		mLCContSubDB.setPrtNo(mPrtNo);
		if (!mLCContSubDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单税优信息失败！";
			this.mErrors.addOneError(tError);
			return false;

		}
		
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", mContNo);
		tVData.add(tTransferData);
		GetTaxCode tGetTaxCode = new GetTaxCode();
		if(!tGetTaxCode.submitData(tVData, "ZBXPT")){
			System.out.println("保单："+mContNo +"获取税优识别码失败！");
			String updLCCont = "update lccontsub set succflag='99', errorinfo = '"+tGetTaxCode.mErrors.getError(0).errorMessage+"',modifydate=current date,modifytime=current time where prtno = '"+mPrtNo+"'";
			mExeSQL.execUpdateSQL(updLCCont);
			CError tError = new CError();
			tError.moduleName = "GetContTaxCodeBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取税优识别码失败，原因为："+tGetTaxCode.mErrors.getError(0).errorMessage;
			this.mErrors.addOneError(tError);
		    return false;
		}else{
			String updLCCont = "update lccontsub set succflag='00', errorinfo =null ,taxcode = '"+tGetTaxCode.getTaxCode()+"',modifydate=current date,modifytime=current time where prtno = '"+mPrtNo+"'";
		    mExeSQL.execUpdateSQL(updLCCont);
		}

		return true;
	}

	private boolean getInputData(VData cInputData, String cOperate) {
		// 从VData容器中取出对象TransferData
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		// 分别对TransferData中数据进行过滤
		mContNo = (String) mTransferData.getValueByName("ContNo");
		if (mContNo == null || "".equals(mContNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保单号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		if (mPrtNo == null || "".equals(mPrtNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TaxCheckContBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "印刷号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
	
}


