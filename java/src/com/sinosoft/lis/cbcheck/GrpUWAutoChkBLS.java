package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统承保团体自动核保业务部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class GrpUWAutoChkBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	//private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB();
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public GrpUWAutoChkBLS() {}

	public static void main(String[] args)
	{
	}

	/**whn
	//传输数据的公共方法
	public int submitData(Vdata cInputData,String cOperate)
	{
	    if(submitDataMain(cInputData,cOperate) == false)
	    {
		return 2; //操作失败
	    }
	    else
	    {
		if (merrno>0)
		{
		    return 1; //操作成功有单要人工核保
		}
		else
		{
		    return 0; //操作成功全部自动通过
		}
	    }
	}
	whn**/

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start GrpUWAutoChkBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End GrpUWAutoChkBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{
        LCGrpPolSet mLCGrpPolSet = (LCGrpPolSet)mInputData.getObjectByObjectName("LCGrpPolSet",0);
        int gtemp = mLCGrpPolSet.size();
        System.out.println("Grpsizeof"+gtemp);
        LCGUWErrorSet mLCGUWErrorSet = (LCGUWErrorSet)mInputData.getObjectByObjectName("LCGUWErrorSet",0);
        LCGUWMasterSet mLCGUWMasterSet = (LCGUWMasterSet)mInputData.getObjectByObjectName("LCGUWMasterSet",0);
        LCGUWSubSet mLCGUWSubSet = (LCGUWSubSet)mInputData.getObjectByObjectName("LCGUWSubSet",0);

        LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
		int temp = mLCPolSet.size();
		System.out.println("sizeof"+temp);
	  	LCUWErrorSet mLCUWErrorSet = (LCUWErrorSet)mInputData.getObjectByObjectName("LCUWErrorSet",0);
	  	LCUWMasterSet mLCUWMasterSet = (LCUWMasterSet)mInputData.getObjectByObjectName("LCUWMasterSet",0);
	  	LCUWSubSet mLCUWSubSet = (LCUWSubSet)mInputData.getObjectByObjectName("LCUWSubSet",0);

        try
		{

                  // 删除团单部分
                  int gpolCount = mLCGrpPolSet.size();
                  for (int j = 1; j <= gpolCount; j++)
                  {
                    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
                    tLCGrpPolSchema = ( LCGrpPolSchema )mLCGrpPolSet.get( j );
                    String tGrpProposalNo = tLCGrpPolSchema.getGrpProposalNo();
                    String tGUWFlag = tLCGrpPolSchema.getUWFlag();
                    String tflag = "0"; //判断团单下个单是否全部通过

                    // 删除个单部分
                    int polCount = mLCPolSet.size();
                    for (int k = 1; k <= polCount; k++)
                    {
                      LCPolSchema tLCPolSchema = new LCPolSchema();
                      tLCPolSchema = ( LCPolSchema )mLCPolSet.get( k );
                      String tProposalNo = tLCPolSchema.getProposalNo();
                      String tUWFlag = tLCPolSchema.getUWFlag();

                      if (tGrpProposalNo.equals(tLCPolSchema.getGrpPolNo()))
                      {
                        Connection conn = DBConnPool.getConnection();

                        if (conn==null)
                        {
                          // @@错误处理
                          CError tError = new CError();
                          tError.moduleName = "GrpUWAutoChkBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tGrpProposalNo+"下"+tProposalNo+"数据库连接失败!";
                          this.mErrors .addOneError(tError) ;
                          tflag = "1";
                          //return false;
                          continue;
                        }
                        conn.setAutoCommit(false);


                        // 核保主表
                        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB( conn );
                        tLCUWMasterDB.setProposalNo( tProposalNo );
                        if (tLCUWMasterDB.deleteSQL() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpUWAtuoChkBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tGrpProposalNo+"下"+tProposalNo+"个人单核保总表删除失败!";
                          this.mErrors .addOneError(tError);
                          conn.rollback();
                          conn.close();
                          tflag = "1";
                          //return false;
                          continue;
                        }
                        System.out.println("-----------BBB--------------");
                        // 投保单
                        LCPolDB tLCPolDB = new LCPolDB( conn );
                        tLCPolDB.setProposalNo( tProposalNo );
                        if (tLCPolDB.deleteSQL() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpUWAutoChkBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = tGrpProposalNo+"下"+tProposalNo+"个人单保单表删除失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          tflag = "1";
                          //return false;
                          continue;
                        }
                        System.out.println("-----------CCC--------------");
                        // 保存部分
                        // 个人保单
                        tLCPolDB.setSchema(tLCPolSchema);

                        if(tLCPolDB.insert() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "GrpUWAutoChkBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "LCPol表保存失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          tflag = "1";
                          //return false;
                          continue;
                        }

                        System.out.println("-----------DDD--------------");
                        // 核保主表
                        LCUWMasterDBSet tLCUWMasterDBSet = new LCUWMasterDBSet( conn );
                        tLCUWMasterDBSet.set( mLCUWMasterSet );
                        int masterno = tLCUWMasterDBSet.size();
                        for (int i = 1;i <= masterno;i++)
                        {
                          LCUWMasterSchema tLCUWMasterSchema = tLCUWMasterDBSet.get(i);
                          String masterpol = tLCUWMasterSchema.getProposalNo();
                          if (masterpol.equals(tProposalNo))
                          {
                            tLCUWMasterDB.setSchema(tLCUWMasterSchema);
                            if (tLCUWMasterDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "GrpUWAutoChkBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tGrpProposalNo+"下"+tProposalNo+"个人核保总表保存失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              tflag = "1";
                              //return false;
                              continue;
                            }
                          }
                        }
                        System.out.println("-----------EEE--------------");

                        // 核保子表
                        LCUWSubDBSet tLCUWSubDBSet = new LCUWSubDBSet( conn );
                        tLCUWSubDBSet.set( mLCUWSubSet );
                        int subno = tLCUWSubDBSet.size();
                        for (int i = 1;i <= subno;i++)
                        {
                          LCUWSubSchema tLCUWSubSchema = tLCUWSubDBSet.get(i);
                          String subpol = tLCUWSubSchema.getProposalNo();
                          if (subpol.equals(tProposalNo))
                          {
                            LCUWSubDB tLCUWSubDB = new LCUWSubDB(conn);
                            tLCUWSubDB.setSchema(tLCUWSubSchema);
                            if (tLCUWSubDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "GrpUWAutoChkBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tGrpProposalNo+"下"+tProposalNo+"个人核保轨迹表保存失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              tflag = "1";
                              //return false;
                              continue;
                            }
                          }
                        }
                        System.out.println("-----------FFF--------------");
                        // 核保错误信息表
                        int errno = mLCUWErrorSet.size();
                        merrno = merrno + errno;
                        for (int i = 1; i<=errno; i++)
                        {
                          LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
                          tLCUWErrorSchema = mLCUWErrorSet.get(i);
                          String errpol = tLCUWErrorSchema.getPolNo();
                          if (errpol.equals(tProposalNo))
                          {
                            LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB(conn);
                            tLCUWErrorDB.setSchema(tLCUWErrorSchema);
                            if (tLCUWErrorDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "GrpUWAutoChkBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = tGrpProposalNo+"下"+tProposalNo+"个人错误信息表保存失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              tflag = "1";
                              //return false;
                              continue;
                            }
                          }
                        }
                        conn.commit() ;
                        conn.close();
                      }
                    } // end of for

                    if (!tflag.equals("1"))
                    {

                      Connection conn = DBConnPool.getConnection();

                      if (conn==null)
                      {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ProposalFeeBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = tGrpProposalNo+"数据库连接失败!";
                        this.mErrors .addOneError(tError) ;
                        //return false;
                        continue;
                      }
                      conn.setAutoCommit(false);


                      // 核保主表
                      LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB( conn );
                      tLCGUWMasterDB.setGrpProposalNo( tGrpProposalNo );
                      if (tLCGUWMasterDB.deleteSQL() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "GrpUWAtuoChkBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = tGrpProposalNo+"集体核保总表删除失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        //return false;
                        continue;
                      }
                      System.out.println("-----------GBBB--------------");
                      // 投保单
                      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB( conn );
                      tLCGrpPolDB.setGrpProposalNo( tGrpProposalNo );
                      if (tLCGrpPolDB.deleteSQL() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "GrpUWAutoChkBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = tGrpProposalNo+"集体保单表删除失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        //return false;
                        continue;
                      }
                      System.out.println("-----------GCCC--------------");

                      // 保存部分
                      // 团体保单
                      tLCGrpPolDB.setSchema(tLCGrpPolSchema);

                      if(tLCGrpPolDB.insert() == false)
                      {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "GrpUWAutoChkBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = tGrpProposalNo+"集体保单表保存失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        //return false;
                        continue;
                      }

                      System.out.println("-----------GDDD--------------");
                      // 核保主表
                      int masterno = mLCGUWMasterSet.size();
                      for (int i = 1;i <= masterno;i++)
                      {
                        LCGUWMasterSchema tLCGUWMasterSchema = mLCGUWMasterSet.get(i);
                        String masterpol = tLCGUWMasterSchema.getGrpProposalNo();
                        if (masterpol.equals(tGrpProposalNo))
                        {
                          tLCGUWMasterDB.setSchema(tLCGUWMasterSchema);
                          if (tLCGUWMasterDB.insert() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "GrpUWAutoChkBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = tGrpProposalNo+"集体核保总表保存失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }
                        }
                      }
                      System.out.println("-----------GEEE--------------");

                      // 核保子表
                      int subno = mLCGUWSubSet.size();
                      for (int i = 1;i <= subno;i++)
                      {
                        LCGUWSubSchema tLCGUWSubSchema = mLCGUWSubSet.get(i);
                        String subpol = tLCGUWSubSchema.getGrpPolNo();
                        if (subpol.equals(tGrpProposalNo))
                        {
                          LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB(conn);
                          tLCGUWSubDB.setSchema(tLCGUWSubSchema);
                          if (tLCGUWSubDB.insert() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "GrpUWAutoChkBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = tGrpProposalNo+"集体核保轨迹表保存失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }
                        }
                      }
                      System.out.println("-----------GFFF--------------");
                      // 核保错误信息表
                      int errno = mLCGUWErrorSet.size();
                      merrno = merrno + errno;
                      for (int i = 1; i<=errno; i++)
                      {
                        LCGUWErrorSchema tLCGUWErrorSchema = new LCGUWErrorSchema();
                        tLCGUWErrorSchema = mLCGUWErrorSet.get(i);
                        String errpol = tLCGUWErrorSchema.getGrpPolNo();
                        if (errpol.equals(tGrpProposalNo))
                        {
                          LCGUWErrorDB tLCGUWErrorDB = new LCGUWErrorDB(conn);
                          tLCGUWErrorDB.setSchema(tLCGUWErrorSchema);
                          if (tLCGUWErrorDB.insert() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLCGUWErrorDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "GrpUWAutoChkBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = tGrpProposalNo+"集体错误信息表保存失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            //return false;
                            continue;
                          }
                        }
                      }
                      conn.commit();
                      conn.close();
                    }
                  }
                } // end of try
                catch (Exception ex)
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "GrpUWAutoChkBLS";
                  tError.functionName = "submitData";
                  tError.errorMessage = ex.toString();
                  this.mErrors .addOneError(tError);
                  return false;
                }
                return true;
        }

}