package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCPENoticeDB;
import com.sinosoft.lis.db.LCPENoticeItemDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPENoticeSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LCPENoticeItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;



/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class UWHealthCancelBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器,最后保存结果 */
    private VData mResult = new VData();

    /** 传输到后台处理的,最后递交Map */
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    private String mOperator;

    private String mManageCom;

    private String mPrtseq;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LOPRTManagerSchema mLOPrtManagerSchema = new LOPRTManagerSchema();

    private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();

    private String theCurrentTime = PubFun.getCurrentTime();

    public UWHealthCancelBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("into UWHealthCancelBL...");
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("UWHealthCancelBL finished...");
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into UWHealthCancelBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        mLOPrtManagerSchema = (LOPRTManagerSchema) mInputData.
                              getObjectByObjectName("LOPRTManagerSchema", 0);

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into UWHealthCancelBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;
        if (mTransferData == null) {
            String str = "前台参数传入不完整!";
            buildError("checkData", str);
            System.out.println("在程序UWHealthCancelBL.checkData() - 168 : " + str);
            return false;
        }
        mPrtseq = (String) mTransferData.getValueByName("Prtseq");

        if (mLOPrtManagerSchema == null || mLOPrtManagerSchema.getPrtSeq() == null) {
            String str = "传入的印刷号信息为空！";
            buildError("getInputData", str);
            System.out.println("在程序UWHealthCancelBL.getInputData() - 144 : " +
                               str);
            return false;
        }

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setPrtSeq(mLOPrtManagerSchema.getPrtSeq());
        if (!tLOPRTManagerDB.getInfo()) {
            String str = "查询体检通知书信息失败!";
            buildError("checkData", str);
            System.out.println("在程序UWHealthCancelBL.checkData() - 168 : " + str);
            return false;
        }
        mLOPrtManagerSchema.setSchema(tLOPRTManagerDB);

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into UWHealthCancelBL.dealData()...");
        /** 查询体检信息 */
        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
        tLCPENoticeDB.setPrtSeq(this.mLOPrtManagerSchema.getPrtSeq());
        tLCPENoticeDB.setProposalContNo(this.mLOPrtManagerSchema.getOtherNo());
        if (!tLCPENoticeDB.getInfo()) {
            String str = "查询体检信息失败 !";
            buildError("dealData", str);
            System.out.println("在程序UWHealthCancelBL.dealData() - 189 : " + str);
            return false;
        }
        LCPENoticeSchema tLCPENoticeSchema = tLCPENoticeDB.getSchema();
        /** 查询体检项目信息 */
        LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
        tLCPENoticeItemDB.setProposalContNo(mLOPrtManagerSchema.getOtherNo());
        tLCPENoticeItemDB.setPrtSeq(mLOPrtManagerSchema.getPrtSeq());
        LCPENoticeItemSet tLCPENoticeItemSet = tLCPENoticeItemDB.query();
        if (tLCPENoticeItemSet == null || tLCPENoticeItemSet.size() <= 0) {
            String str = "没有查询到体检项目!";
            buildError("dealData", str);
            System.out.println("在程序UWHealthCancelBL.dealData() - 202 : " + str);
            return false;
        }
//        mMap.put(
//                "insert into LOBPrtManager (select * from LOPrtManager where Prptseq = " +
//                mLOPrtManagerSchema.getPrtSeq() + ")", "INSERT");
//        mMap.put(
//                "insert into LCBPENotice (select * from LCPENotice where Prptseq = " +
//                mLOPrtManagerSchema.getPrtSeq() + ")", "INSERT");
        mMap.put("delete from LCPENotice where Prtseq = '" +
                 mLOPrtManagerSchema.getPrtSeq() + "' ", "DELETE");
        System.out.println("mLOPrtManagerSchema " + mLOPrtManagerSchema);
        mMap.put(mLOPrtManagerSchema, "DELETE");
        mMap.put(tLCPENoticeSchema, "DELETE");
        mMap.put(tLCPENoticeItemSet, "DELETE");

//        for (int i = 1; i <= count; i++) {
//
//            LCPENoticeItemSchema tLCPENoticeItemSchema = new
//                    LCPENoticeItemSchema();
//            LCPENoticeItemDB tLCPENoticeItemDB = new LCPENoticeItemDB();
//            tLCPENoticeItemDB.setPrtSeq(tPrtseq);
//            tLCPENoticeItemSet = tLCPENoticeItemDB.query();
//            tLCPENoticeItemSchema.setSchema(tLCPENoticeItemSet.get(1));
//
//            mMap.put(
//                    "insert into LCBPENoticeItem (select * from LCPENoticeItem where Prptseq in " +
//                    mLOPrtManagerSchema.getPrtSeq() + ")", "INSERT");
//            mMap.put("delete from LCPENoticeItem where Prtseq = " +
//                     mLOPrtManagerSchema.getPrtSeq(), "DELETE");
//
//        }

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into UWHealthCancelBL.prepareOutputData()...");
        mResult.clear();
        this.mResult.add(mMap);
//        mResult.add(mMap);
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "UWHealthCancelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
