package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGCUWErrorDB;
import com.sinosoft.lis.db.LCGCUWMasterDB;
import com.sinosoft.lis.db.LCGCUWSubDB;
import com.sinosoft.lis.db.LCGUWErrorDB;
import com.sinosoft.lis.db.LCGUWMasterDB;
import com.sinosoft.lis.db.LCGUWSubDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.pubfun.CalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGCUWErrorSchema;
import com.sinosoft.lis.schema.LCGCUWMasterSchema;
import com.sinosoft.lis.schema.LCGCUWSubSchema;
import com.sinosoft.lis.schema.LCGUWErrorSchema;
import com.sinosoft.lis.schema.LCGUWMasterSchema;
import com.sinosoft.lis.schema.LCGUWSubSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.RSWrapper;

/**
 * <p>Title: Web业务系统承保团体自动核保业务部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 2.0
 */
public class GrpUWAutoChkBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();

    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperator;

    private String mPolPassFlag = "0"; //险种通过标记

    private String mContPassFlag = "0"; //合同通过标记

    private String mUWGrade = "";

    private String mCalCode; //计算编码

    private String mUser;

    private FDate fDate = new FDate();

    private double mValue;

    private LCGrpContSet mAllLCGrpContSet = new LCGrpContSet();

    private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();

    private String mGrpContNo = "";

    private String mProposalGrpContNo = "";

    private String mGrpPolNo = "";

    private String mType;

    /** 合同核保主表*/
    private LCGCUWMasterSet mLCGCUWMasterSet = new LCGCUWMasterSet();

    private LCGCUWMasterSet mAllLCGCUWMasterSet = new LCGCUWMasterSet();

    /** 合同核保子表*/
    private LCGCUWSubSet mLCGCUWSubSet = new LCGCUWSubSet();

    private LCGCUWSubSet mAllLCGCUWSubSet = new LCGCUWSubSet();

    /** 合同核保错误信息表*/
    private LCGCUWErrorSet mLCGCUWErrorSet = new LCGCUWErrorSet();

    private LCGCUWErrorSet mAllLCGCUWErrorSet = new LCGCUWErrorSet();

    /** 各险种核保主表 */
    private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();

    private LCGUWMasterSet mAllLCGUWMasterSet = new LCGUWMasterSet();

    /** 各险种核保子表 */
    private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();

    private LCGUWSubSet mAllLCGUWSubSet = new LCGUWSubSet();

    /** 核保错误信息表 */
    private LCGUWErrorSet mLCGUWErrorSet = new LCGUWErrorSet();

    private LCGUWErrorSet mAllLCErrSet = new LCGUWErrorSet();

    private CalBase mCalBase = new CalBase();

    GlobalInput tGlobalInput = new GlobalInput();

    TransferData mTransferData = new TransferData();

    public GrpUWAutoChkBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        TransferData tTransferData = new TransferData();

        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mResult.clear();

        //团险正常自核时，是不会传入TransferData对象的。防止意外空指针异常。
        //mType = (String) mTransferData.getValueByName("Type");
        if (mTransferData != null)
        {
            mType = (String) mTransferData.getValueByName("Type");
        }
        else
        {
            mType = "";
        }
        // ----------------------------------------

        System.out.println("ssss" + mType);
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        System.out.println("---1---");

        //得到外部传入的数据,将数据备份到本类中
        LCGrpContSchema tLCGrpContSchema = getInputData(cInputData);

        if (tLCGrpContSchema == null)
        {
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);
            return false;
        }

        System.out.println("---GrpUWAutoChkBL getInputData---");

        if (!checkData(tLCGrpContSchema))
        {
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);
            return false;
        }

        if (checkFinished(tLCGrpContSchema))
        {
            tTransferData.setNameAndValue("FinishFlag", "1");
            tTransferData.setNameAndValue("UWFlag", mContPassFlag);
            mResult.add(tTransferData);
            if (mType.equals("2"))
            {
            }
            else
            {
                return true;
            }
        }
        System.out.println("这一步ok");
        if (!dealData(tLCGrpContSchema))
        {
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);
            return false;
        }

        System.out.println("---GrpUWAutoChkBL dealData---");

        //准备给后台的数据
        if (prepareOutputData(tLCGrpContSchema))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交的数据准备失败!";
            this.mErrors.addOneError(tError);
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);

            return false;
        }

        mResult.add(mMap);

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);

            return false;
        }

        tTransferData.setNameAndValue("FinishFlag", "1");
        tTransferData.setNameAndValue("UWFlag", mContPassFlag);
        mResult.add(tTransferData);

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData(LCGrpContSchema tLCGrpContSchema)
    {
        mGrpContNo = tLCGrpContSchema.getGrpContNo(); //获得保单号
        mProposalGrpContNo = tLCGrpContSchema.getProposalGrpContNo();
        mUWGrade = "1";

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(mGrpContNo);

        LCContSet tLCContSet = new LCContSet();
        //            tLCContDB.executeQuery(
        //                "select * from lccont where grpcontno='" + mGrpContNo +
        //                "' and uwflag not in ('4','9')");

        if (tLCContSet == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "取集体保单号为：" + mGrpContNo + " 的个人保单失败";
            this.mErrors.addOneError(tError);

            return false;
        }

        LCContSchema tLCContSchema = null;
        GlobalInput tG = new GlobalInput();
        tG.Operator = mOperator;

        VData tInputData = new VData();
        UWAutoChkBL tUWAutoChkBL = null;
        int nIndex = 0;
        int nCount;
        RSWrapper tRSWrapper = new RSWrapper();
        tRSWrapper.prepareData(tLCContSet,
                "select * from lccont where grpcontno='" + mGrpContNo
                        + "' and uwflag not in ('4','9') with ur");
        do
        {
            tRSWrapper.getData();
            nCount = tLCContSet.size();
            for (nIndex = 1; nIndex <= nCount; nIndex++)
            {
                tLCContSchema = tLCContSet.get(nIndex);
                tInputData.clear();
                tInputData.add(tG);
                tInputData.add(tLCContSchema);
                tUWAutoChkBL = new UWAutoChkBL();
                if (!tUWAutoChkBL.submitData(tInputData, ""))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tUWAutoChkBL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "GrpUWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "对集体保单号为：" + mGrpContNo
                            + " 的集体保单下的个人保单进行核保时失败";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                if (tInputData == null)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpUWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "对集体保单号为：" + mGrpContNo
                            + " 的集体保单下的个人保单进行核保时出现返回空信息错误";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                if (!mContPassFlag.equals("5"))
                {
                    mContPassFlag = (String) tInputData.get(0);
                }
                if (Integer.parseInt(mUWGrade) < Integer
                        .parseInt((String) tInputData.get(1)))
                {
                    mUWGrade = (String) tInputData.get(1);
                }
            }
        }
        while (tLCContSet.size() > 0);

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema tLCGrpPolSchema = null;

        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        tLCGrpPolSet = tLCGrpPolDB.query();

        nCount = tLCGrpPolSet.size();
        nIndex = 0;

        LMUWSet tLMUWSetUnpass = new LMUWSet(); //未通过的核保规则
        LMUWSet tLMUWSetAll = null; //所有核保规则
        LMUWSet tLMUWSetSpecial = null; //需要风险检测的特殊核保规则
        LMUWSchema tLMUWSchema = null;

        for (nIndex = 1; nIndex <= nCount; nIndex++)
        {
            tLCGrpPolSchema = tLCGrpPolSet.get(nIndex);
            mGrpPolNo = tLCGrpPolSchema.getGrpPolNo(); //获得保单险种号

            String tkinds = tLCGrpPolSchema.getRiskCode(); //获得险种号

            //准备算法，获取某险种的所有核保规则的集合
            tLMUWSetUnpass.clear();

            if (tLMUWSetAll != null)
            {
                tLMUWSetAll.clear();
            }

            if (tLMUWSetSpecial != null)
            {
                tLMUWSetSpecial.clear();
            }

            tLMUWSetAll = CheckKinds(tLCGrpPolSchema);

            if (tLMUWSetAll == null)
            {
                return false;
            }

            tLMUWSetSpecial = CheckKinds2(tLCGrpPolSchema);

            if (tLMUWSetSpecial == null)
            {
                return false;
            }

            //准备数据，从险种信息中获取各项计算信息
            CheckPolInit(tLCGrpPolSchema);

            //个人单核保
            mPolPassFlag = "0"; //核保通过标志，初始为未核保

            int n = tLMUWSetAll.size(); //核保规则数量

            if (n == 0)
            {
                mPolPassFlag = "9"; //无核保规则则置标志为通过
            }
            else
            { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
                int j = 0;

                for (int i = 1; i <= n; i++)
                {
                    //取计算编码
                    tLMUWSchema = new LMUWSchema();
                    tLMUWSchema = tLMUWSetAll.get(i);
                    mCalCode = tLMUWSchema.getCalCode();

                    if (CheckPol(tLCGrpPolSchema.getRiskCode()) == 0)
                    {
                    }
                    else
                    {
                        j++;
                        tLMUWSetUnpass.add(tLMUWSchema);
                        mPolPassFlag = "5"; //待人工核保
                        mContPassFlag = "5";

                        //取核保级别
                        String tuwgrade = tLMUWSchema.getUWGrade();

                        if (tuwgrade == null)
                        {
                            // @@错误处理
                            CError tError = new CError();
                            tError.moduleName = "GrpUWAutoChkBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "取自核规则编码为："
                                    + tLMUWSchema.getCalCode() + " 的核保级别错误";
                            this.mErrors.addOneError(tError);

                            return false;
                        }

                        if ((j == 1)
                                && ((mUWGrade == null) || mUWGrade.equals("")))
                        {
                            mUWGrade = tuwgrade;
                        }
                        else
                        {
                            if (mUWGrade.compareTo(tuwgrade) < 0)
                            {
                                mUWGrade = tuwgrade;
                            }
                        }
                    }
                }

                //需要人工核保时候，校验核保返回核保员核保级别
                if ((tLMUWSetUnpass.size() > 0) && (tLMUWSetSpecial.size() > 0))
                {
                    for (int k = 1; k <= tLMUWSetSpecial.size(); k++)
                    {
                        LMUWSchema t2LMUWSchema = new LMUWSchema();
                        t2LMUWSchema = tLMUWSetSpecial.get(k);
                        mCalCode = t2LMUWSchema.getCalCode();

                        String tempuwgrade = checkRiskAmnt(tLCGrpPolSchema);

                        if (tempuwgrade != null)
                        {
                            if ((mUWGrade == null)
                                    || (mUWGrade.compareTo(tempuwgrade) < 0))
                            { //当需要人工核保时候当即tLMUWSetUnpass.size()>0时,mUWGrade应该不为null,否则是自动核保规则中核保级别字段缺少了数据
                                mUWGrade = tempuwgrade;
                            }
                        }
                    }
                }
                else
                { //当所有的自动核保不成功规则均不与该投保单匹配时核保级别会为空,但一旦要进行核保订正会出现无核保级别的异常保错.所以给所有无核保级别的投保单一个最低默认级别
                    if ((mUWGrade == null) || mUWGrade.equals(""))
                    {
                        mUWGrade = "1";
                    }
                }

                if (mPolPassFlag.equals("0"))
                {
                    mPolPassFlag = "9";
                }

                System.out.println("匹配数:" + tLMUWSetAll.size() + "级别计算:"
                        + tLMUWSetSpecial.size() + "级别:" + mUWGrade);
            }

            if (dealOnePol(tLCGrpPolSchema, tLMUWSetUnpass) == false)
            {
                return false;
            }
            if (!dealRiskFee(tLCGrpPolSchema))
            {
                return false;
            }
        }

        /* 合同核保 */
        LMUWSet tLMUWSetContUnpass = new LMUWSet(); //未通过的合同核保规则
        LMUWSet tLMUWSetContAll = CheckKinds3(); //所有合同核保规则

        //准备数据，从险种信息中获取各项计算信息
        CheckContInit(tLCGrpContSchema);

        //个人合同核保
        int tCount = tLMUWSetContAll.size(); //核保规则数量

        if (tCount == 0)
        {
            if (!mContPassFlag.equals("5"))
            {
                mContPassFlag = "9"; //无核保规则则置标志为通过
            }
        }
        else
        { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝
            int j = 0;

            for (int index = 1; index <= tCount; index++)
            {
                //取计算编码
                tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetContAll.get(index);
                mCalCode = tLMUWSchema.getCalCode();

                if (CheckPol("000000") == 0)
                {
                }
                else
                {
                    j++;
                    tLMUWSetContUnpass.add(tLMUWSchema);
                    mContPassFlag = "5"; //核保不通过，待人工核保

                    //取核保级别
                    String tuwgrade = tLMUWSchema.getUWGrade();

                    if (tuwgrade == null)
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "GrpUWAutoChkBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "集体合同核保时取自核规则编码为："
                                + tLMUWSchema.getCalCode() + " 的核保级别错误";
                        this.mErrors.addOneError(tError);

                        return false;
                    }

                    if ((j == 1) && ((mUWGrade == null) || mUWGrade.equals("")))
                    {
                        mUWGrade = tuwgrade;
                    }
                    else
                    {
                        if (mUWGrade.compareTo(tuwgrade) < 0)
                        {
                            mUWGrade = tuwgrade;
                        }
                    }
                }
            }

            if ((mUWGrade == null) || mUWGrade.equals(""))
            {
                mUWGrade = "1";
            }

            if (mContPassFlag.equals("0"))
            {
                if (!mContPassFlag.equals("5"))
                {
                    mContPassFlag = "9";
                }
            }

            System.out.println("合同核保匹配数:" + tLMUWSetContAll.size()
                    + "合同核保未通过数:" + tLMUWSetContUnpass.size() + "级别:"
                    + mUWGrade);
        }

        dealOneCont(tLCGrpContSchema, tLMUWSetContUnpass);

        if (mContPassFlag.equals("9"))
        {
            if (!prepareSendFirstPayNotice())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 发送首期交费通知书
     * 2005-05-23 modify by zhangxing
     * @return boolean
     */
    private boolean prepareSendFirstPayNotice()
    {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setOtherNo(mGrpContNo);
        tLOPRTManagerSchema.setOtherNoType("01");
        tLOPRTManagerSchema.setCode("57");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AutoUWFlag", "1");
        VData tempVData = new VData();
        tempVData.add(tLOPRTManagerSchema);
        tempVData.add(tGlobalInput);
        tempVData.add(tTransferData);
        GrpUWSendPrintUI tGrpUWSendPrintUI = new GrpUWSendPrintUI();
        tGrpUWSendPrintUI.submitData(tempVData, "INSERT");
        return true;
    }

    /**
     * dealRiskFee
     *
     * @param tLCGrpPolSchema LCGrpPolSchema
     * @return boolean
     */
    private boolean dealRiskFee(LCGrpPolSchema tLCGrpPolSchema)
    {
        StringBuffer sql = new StringBuffer();
        sql
                .append("SELECT DIV(SUM(PREM),SUM(STANDPREM)) FROM LCPREM WHERE POLNO IN  (SELECT POLNO FROM LCPOL WHERE RISKCODE='");
        sql.append(tLCGrpPolSchema.getRiskCode());
        sql.append("' AND GRPCONTNO='");
        sql.append(tLCGrpPolSchema.getGrpContNo());
        sql.append("')");
        String polSale = (new ExeSQL()).getOneValue(sql.toString());
        if (StrTool.cTrim(polSale).equals("")
                || StrTool.cTrim(polSale).equals("null"))
        {
            /** 没有折扣.标准保费没有计算出来 */
            return true;
        }
        sql = new StringBuffer();
        sql
                .append("SELECT MAXVALUE,MINVALUE FROM LDRISKPOPEDOM WHERE RISKCODE='");
        sql.append(tLCGrpPolSchema.getRiskCode());
        sql.append("' and POPEDOMTYPE='00'");
        SSRS ssrs = (new ExeSQL()).execSQL(sql.toString());
        if (ssrs == null || ssrs.getMaxRow() <= 0 || ssrs.getMaxRow() > 1)
        {
            //            buildError("dealRiskFee", "没有描述管理费！");
            return true;
        }
        String maxValue = ssrs.GetText(1, 1);
        String minValue = ssrs.GetText(1, 2);
        if (StrTool.cTrim(maxValue).equals("")
                || StrTool.cTrim(maxValue).equals("null")
                || StrTool.cTrim(minValue).equals("")
                || StrTool.cTrim(minValue).equals("null"))
        {
            buildError("dealRiskFee", "没有定义折扣底限！");
            return false;
        }
        double polSaleValue = Double.parseDouble(polSale);
        double minRiskRateValue = Double.parseDouble(maxValue);
        double maxRiskRateValue = Double.parseDouble(minValue);
        double manageFee = 0.0;
        if (polSaleValue > 0 && polSaleValue > minRiskRateValue
                && polSaleValue < maxRiskRateValue)
        {
            /**
             * 机构费用率=（实折－折扣底线）/实折
             */
            manageFee = (polSaleValue - minRiskRateValue) / polSaleValue;
        }
        if (manageFee > 0)
        {
            sql = new StringBuffer();
            sql.append("UPDATE LCGRPPOL SET BRANCHFEERATE=");
            sql.append(manageFee);
            sql.append(" WHERE 1=1 AND RISKCODE='");
            sql.append(tLCGrpPolSchema.getRiskCode());
            sql.append("' AND GRPCONTNO='");
            sql.append(tLCGrpPolSchema.getGrpContNo());
            sql.append("'");
            mMap.put(sql.toString(), "UPDATE");
        }
        return true;
    }

    /**
     * 根据保额校验核保级别
     * @return
     */
    private String checkRiskAmnt(LCGrpPolSchema tLCGrpPolSchema)
    {
        String tUWGrade = "";

        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);

        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        String SQl = "select count(distinct riskwrapcode) from lcriskwrap where grpcontno='"
                + mCalBase.getContNo() + "' with ur";
        System.out.println(mCalBase.getGrpContNo());
        ExeSQL tExeSQL = new ExeSQL();
        int chk = Integer.parseInt(tExeSQL.getOneValue(SQl));
        if (chk > 0)
        {
            mCalculator.addBasicFactor("Wrap", "1");
        }
        else
        {
            mCalculator.addBasicFactor("Wrap", "0");
        }
        //    mCalculator.addBasicFactor("InsuredNo",tLCGrpPolSchema.getInsuredNo());;
        mCalculator.addBasicFactor("RiskCode", tLCGrpPolSchema.getRiskCode());
        ;

        String tStr = "";
        tStr = mCalculator.calculate();

        if (tStr.trim().equals(""))
        {
            tUWGrade = "";
        }
        else
        {
            tUWGrade = tStr.trim();
        }

        System.out.println("AmntGrade:" + tUWGrade);

        return tUWGrade;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol(LCGrpPolSchema tLCGrpPolSchema,
            LMUWSet tLMUWSetUnpass)
    {
        // 保单
        if (preparePol(tLCGrpPolSchema) == false)
        {
            return false;
        }

        // 核保信息
        if (preparePolUW(tLCGrpPolSchema, tLMUWSetUnpass) == false)
        {
            return false;
        }

        LCGrpPolSchema tLCGrpPolSchemaDup = new LCGrpPolSchema();
        tLCGrpPolSchemaDup.setSchema(tLCGrpPolSchema);
        mAllLCGrpPolSet.add(tLCGrpPolSchemaDup);

        LCGUWMasterSet tLCGUWMasterSet = new LCGUWMasterSet();
        tLCGUWMasterSet.set(mLCGUWMasterSet);
        mAllLCGUWMasterSet.add(tLCGUWMasterSet);

        LCGUWSubSet tLCGUWSubSet = new LCGUWSubSet();
        tLCGUWSubSet.set(mLCGUWSubSet);
        mAllLCGUWSubSet.add(tLCGUWSubSet);

        LCGUWErrorSet tLCGUWErrorSet = new LCGUWErrorSet();
        tLCGUWErrorSet.set(mLCGUWErrorSet);
        mAllLCErrSet.add(tLCGUWErrorSet);

        return true;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOneCont(LCGrpContSchema tLCGrpContSchema,
            LMUWSet tLMUWSetContUnpass)
    {
        if (!prepareContUW(tLCGrpContSchema, tLMUWSetContUnpass))
        {
            return false;
        }

        LCGrpContSchema tLCGrpContSchemaDup = new LCGrpContSchema();
        tLCGrpContSchemaDup.setSchema(tLCGrpContSchema);
        mAllLCGrpContSet.add(tLCGrpContSchemaDup);

        LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();
        tLCGCUWMasterSet.set(mLCGCUWMasterSet);
        mAllLCGCUWMasterSet.add(tLCGCUWMasterSet);

        LCGCUWSubSet tLCGCUWSubSet = new LCGCUWSubSet();
        tLCGCUWSubSet.set(mLCGCUWSubSet);
        mAllLCGCUWSubSet.add(tLCGCUWSubSet);

        LCGCUWErrorSet tLCGCUWErrorSet = new LCGCUWErrorSet();
        tLCGCUWErrorSet.set(mLCGCUWErrorSet);
        mAllLCGCUWErrorSet.add(tLCGCUWErrorSet);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private LCGrpContSchema getInputData(VData cInputData)
    {

        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperator = tGlobalInput.Operator;
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        LCGrpContSchema tLCGrpContSchema = (LCGrpContSchema) cInputData
                .getObjectByObjectName("LCGrpContSchema", 0); //从输入数据中获取合同记录的数据
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());

        if (tLCGrpContDB.getInfo())
        { //验证LCGrpCont表中是否存在该合同项记录
            return tLCGrpContDB.getSchema();
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "集体合同号为" + tLCGrpContSchema.getGrpContNo()
                    + "的合同信息未查询到!";
            this.mErrors.addOneError(tError);

            return null;
        }
    }

    private boolean checkFinished(LCGrpContSchema tLCGrpContSchema)
    {
        if (tLCGrpContSchema.getUWFlag() != null
                && !tLCGrpContSchema.getUWFlag().equals("")
                && !tLCGrpContSchema.getUWFlag().equals("0"))
        {
            mContPassFlag = tLCGrpContSchema.getUWFlag();
            System.out.println("weishenme?????" + mContPassFlag);
            return true;
        }
        return false;
    }

    private boolean checkData(LCGrpContSchema tLCGrpContSchema)
    {
        //校验核保级别
        if (!checkUWGrade())
        {
            return false;
        }

        //校验是否复核
        //        if (!checkApprove(tLCGrpContSchema))
        //        {
        //            return false;
        //        }

        return true;
    }

    /**
     * 校验投保单是否复核
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkApprove(LCGrpContSchema tLCGrpContSchema)
    {
        if ((tLCGrpContSchema.getApproveFlag() == null)
                || !tLCGrpContSchema.getApproveFlag().equals("9"))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "checkApprove";
            tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号："
                    + tLCGrpContSchema.getProposalGrpContNo().trim() + "）";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 校验核保员级别
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkUWGrade()
    {
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mOperator);

        if (!tLDUserDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperator + "）";
            this.mErrors.addOneError(tError);

            return false;
        }
        /*
         String tUWPopedom = tLDUserDB.getUWPopedom();

         if ((tUWPopedom == null) || tUWPopedom.equals("")) {
         CError tError = new CError();
         tError.moduleName = "GrpUWAutoChkBL";
         tError.functionName = "checkUWGrade";
         tError.errorMessage = "操作员无核保权限，不能核保!（操作员：" + mOperator + "）";
         this.mErrors.addOneError(tError);

         return false;
         }
         */
        return true;
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds(LCGrpPolSchema tLCGrpPolSchema)
    {
        String tsql = "";

        // 团险正常自核时，是不会传入TransferData对象的。防止意外空指针异常。
        //mType = (String) mTransferData.getValueByName("Type");
//        if (mTransferData != null)
//        {
//            mType = (String) mTransferData.getValueByName("Type");
//        }
//        else
//        {
//            mType = "";
//        }
        //---------------------------------------------------

        LMUWSchema tLMUWSchema = new LMUWSchema();
        if (mType.equals("2"))
        {
            LMUWDB tLMUWDB = new LMUWDB();
            LMUWSet tLMUWSet = null;
            LCGrpPolDB  nLCGrpPolDB = new LCGrpPolDB();
            nLCGrpPolDB.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            LCGrpPolSet  nLCGrpPolSet = nLCGrpPolDB.query();
            for(int i=1;i<=nLCGrpPolSet.size();i++) {
                String msql="";
                System.out.println(nLCGrpPolSet.get(i).getRiskCode().trim());
                msql = "select * from lmuw where riskcode = '"
             + nLCGrpPolSet.get(i).getRiskCode().trim()
             + "' and relapoltype = 'G' and uwtype = '1' and passflag = 'R'  order by calcode";

            tLMUWSet = tLMUWDB.executeQuery(msql);
            System.out.println("团险险种" + msql);
                  }
            if (tLMUWSet == null || tLMUWSet.size() <= 0)
            {
                CError tError = new CError();
                tError.moduleName = "UWApplyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "不需要发起再保!";
                this.mErrors.addOneError(tError);
                tLMUWSet.clear();
                return null;
            }
            tLMUWSet.clear();
        tsql = "select * from lmuw where riskcode = '"
               + tLCGrpPolSchema.getRiskCode().trim()
               + "' and relapoltype = 'G' and uwtype = '1' and passflag = 'R'  order by calcode";
            tLMUWSet = tLMUWDB.executeQuery(tsql);

            return tLMUWSet;
        }
        else
        {
            //查询算法编码
            tsql = "select * from lmuw where (riskcode = '000000' and relapoltype = 'G' and uwtype = '11') or (riskcode = '"
                    + tLCGrpPolSchema.getRiskCode().trim()
                    + "' and relapoltype = 'G' and uwtype = '1')  order by calcode";

            LMUWDB tLMUWDB = new LMUWDB();

            LMUWSet tLMUWSet = tLMUWDB.executeQuery(tsql);

            if (tLMUWDB.mErrors.needDealError() == true)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLMUWDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "GrpUWAutoChkBL";
                tError.functionName = "CheckKinds";
                tError.errorMessage = tLCGrpPolSchema.getRiskCode().trim()
                        + "险种核保信息查询失败!";
                this.mErrors.addOneError(tError);
                tLMUWSet.clear();

                return null;
            }

            return tLMUWSet;
        }
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds2(LCGrpPolSchema tLCGrpPolSchema)
    {
        String tsql = "";
        LMUWSchema tLMUWSchema = new LMUWSchema();
        LMUWSet  tLMUWSet = new LMUWSet();
      if(mType.equals("")){
        //查询算法编码
        tsql = "select * from lmuw where riskcode = '000000' and relapoltype = 'G' and uwtype = '12'";

        LMUWDB tLMUWDB = new LMUWDB();

         tLMUWSet = tLMUWDB.executeQuery(tsql);

        if (tLMUWDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "CheckKinds2";
            tError.errorMessage = tLCGrpPolSchema.getRiskCode().trim()
                    + "险种信息核保查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();

            return null;
        }

        return tLMUWSet;
       }

       return tLMUWSet;
    }
    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds3()
    {
        String tsql = "";
        LMUWSchema tLMUWSchema = new LMUWSchema();
       LMUWSet  tLMUWSet = new LMUWSet();
      if(mType.equals("")){
          //查询算法编码
          tsql = "select * from lmuw where riskcode = '000000' and relapoltype = 'G' and uwtype = '19'";

          LMUWDB tLMUWDB = new LMUWDB();

          tLMUWSet = tLMUWDB.executeQuery(tsql);

          if (tLMUWDB.mErrors.needDealError() == true) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLMUWDB.mErrors);

              CError tError = new CError();
              tError.moduleName = "GrpUWAutoChkBL";
              tError.functionName = "CheckKinds3";
              tError.errorMessage = "集体合同险种核保信息查询失败!";
              this.mErrors.addOneError(tError);
              tLMUWSet.clear();

              return null;
          }

          return tLMUWSet;
      }
       return tLMUWSet;
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(LCGrpPolSchema tLCGrpPolSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCGrpPolSchema.getPrem());
        mCalBase.setGet(tLCGrpPolSchema.getAmnt());
        mCalBase.setMult(tLCGrpPolSchema.getMult());

        mCalBase.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        mCalBase.setGrpContNo(mGrpContNo);
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckContInit(LCGrpContSchema tLCGrpContSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCGrpContSchema.getPrem());
        mCalBase.setGet(tLCGrpContSchema.getAmnt());
        mCalBase.setMult(tLCGrpContSchema.getMult());
        mCalBase.setGrpContNo(mGrpContNo);
    }

    /**
     * 个人单核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private double CheckPol(String tRiskCode)
    {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);

        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());

        //    mCalculator.addBasicFactor("InsuredNo", tInsuredNo); //tLCGrpPolSchema.getInsuredNo());;
        mCalculator.addBasicFactor("RiskCode", tRiskCode); //tLCGrpPolSchema.getRiskCode());;

        String tStr = "";
        tStr = mCalculator.calculate();

        if ((tStr == null) || tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);

        return mValue;
    }

    /**
     * 准备保单信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePol(LCGrpPolSchema tLCGrpPolSchema)
    {
        System.out.println("险种核保标志" + mPolPassFlag);
        tLCGrpPolSchema.setUWFlag(mPolPassFlag);
        tLCGrpPolSchema.setUWDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setUWTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setUWOperator(mOperator);
        tLCGrpPolSchema.setUWDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());

        return true;
    }

    /**
     * 准备合同核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareContUW(LCGrpContSchema tLCGrpContSchema,
            LMUWSet tLMUWSetContUnpass)
    {
        tLCGrpContSchema.setUWFlag(mContPassFlag);
        tLCGrpContSchema.setUWDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setUWTime(PubFun.getCurrentTime());
        tLCGrpContSchema.setUWOperator(mOperator);
        tLCGrpContSchema.setUWDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());

        //合同核保主表
        boolean firstUW = true;
        LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
        LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
        tLCGCUWMasterDB.setGrpContNo(mGrpContNo);

        LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();
        tLCGCUWMasterSet = tLCGCUWMasterDB.query();

        if (tLCGCUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWMasterDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mGrpContNo + "合同核保总表取数失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        if (tLCGCUWMasterSet.size() == 0)
        {
            tLCGCUWMasterSchema.setGrpContNo(mGrpContNo);
            tLCGCUWMasterSchema.setProposalGrpContNo(tLCGrpContSchema
                    .getProposalGrpContNo());
            tLCGCUWMasterSchema.setUWNo(1);
            tLCGCUWMasterSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            tLCGCUWMasterSchema.setAgentGroup(tLCGrpContSchema.getAgentGroup());
            tLCGCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCGCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCGCUWMasterSchema.setPostponeDay("");
            tLCGCUWMasterSchema.setPostponeDate("");
            tLCGCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCGCUWMasterSchema.setState(mContPassFlag);
            tLCGCUWMasterSchema.setPassFlag(mContPassFlag);
            tLCGCUWMasterSchema.setHealthFlag("0");
            tLCGCUWMasterSchema.setSpecFlag("0");
            tLCGCUWMasterSchema.setQuesFlag("0");
            tLCGCUWMasterSchema.setReportFlag("0");
            tLCGCUWMasterSchema.setChangePolFlag("0");
            tLCGCUWMasterSchema.setPrintFlag("0");
            tLCGCUWMasterSchema.setPrintFlag2("0");
            tLCGCUWMasterSchema.setManageCom(tLCGrpContSchema.getManageCom());
            tLCGCUWMasterSchema.setUWIdea("");
            tLCGCUWMasterSchema.setUpReportContent("");
            tLCGCUWMasterSchema.setOperator(mOperator); //操作员
            tLCGCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCGCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCGCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            firstUW = false;
            tLCGCUWMasterSchema = tLCGCUWMasterSet.get(1);
            tLCGCUWMasterSchema.setUWNo(tLCGCUWMasterSchema.getUWNo() + 1);
            tLCGCUWMasterSchema.setState(mContPassFlag);
            tLCGCUWMasterSchema.setPassFlag(mContPassFlag);
            tLCGCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCGCUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCGCUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCGCUWMasterSchema.setOperator(mOperator); //操作员
            tLCGCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }

        mLCGCUWMasterSet.clear();
        mLCGCUWMasterSet.add(tLCGCUWMasterSchema);

        // 合同核保轨迹表
        LCGCUWSubSchema tLCGCUWSubSchema = new LCGCUWSubSchema();
        LCGCUWSubDB tLCGCUWSubDB = new LCGCUWSubDB();
        tLCGCUWSubDB.setGrpContNo(mGrpContNo);

        LCGCUWSubSet tLCGCUWSubSet = new LCGCUWSubSet();
        tLCGCUWSubSet = tLCGCUWSubDB.query();

        if (tLCGCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mGrpContNo + "合同核保轨迹表查失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        int nUWNo = tLCGCUWSubSet.size();

        if (nUWNo > 0)
        {
            tLCGCUWSubSchema.setUWNo(++nUWNo); //第几次核保
        }
        else
        {
            tLCGCUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCGCUWSubSchema.setGrpContNo(tLCGCUWMasterSchema.getGrpContNo());
        tLCGCUWSubSchema.setProposalGrpContNo(tLCGCUWMasterSchema
                .getProposalGrpContNo());
        tLCGCUWSubSchema.setAgentCode(tLCGCUWMasterSchema.getAgentCode());
        tLCGCUWSubSchema.setAgentGroup(tLCGCUWMasterSchema.getAgentGroup());
        tLCGCUWSubSchema.setUWGrade(tLCGCUWMasterSchema.getUWGrade()); //核保级别
        tLCGCUWSubSchema.setAppGrade(tLCGCUWMasterSchema.getAppGrade()); //申请级别
        tLCGCUWSubSchema.setAutoUWFlag(tLCGCUWMasterSchema.getAutoUWFlag());
        tLCGCUWSubSchema.setState(tLCGCUWMasterSchema.getState());
        tLCGCUWSubSchema.setPassFlag(tLCGCUWMasterSchema.getState());
        tLCGCUWSubSchema.setPostponeDay(tLCGCUWMasterSchema.getPostponeDay());
        tLCGCUWSubSchema.setPostponeDate(tLCGCUWMasterSchema.getPostponeDate());
        tLCGCUWSubSchema.setUpReportContent(tLCGCUWMasterSchema
                .getUpReportContent());
        tLCGCUWSubSchema.setHealthFlag(tLCGCUWMasterSchema.getHealthFlag());
        tLCGCUWSubSchema.setSpecFlag(tLCGCUWMasterSchema.getSpecFlag());
        tLCGCUWSubSchema.setSpecReason(tLCGCUWMasterSchema.getSpecReason());
        tLCGCUWSubSchema.setQuesFlag(tLCGCUWMasterSchema.getQuesFlag());
        tLCGCUWSubSchema.setReportFlag(tLCGCUWMasterSchema.getReportFlag());
        tLCGCUWSubSchema.setChangePolFlag(tLCGCUWMasterSchema
                .getChangePolFlag());
        tLCGCUWSubSchema.setChangePolReason(tLCGCUWMasterSchema
                .getChangePolReason());
        tLCGCUWSubSchema.setAddPremReason(tLCGCUWMasterSchema
                .getAddPremReason());
        tLCGCUWSubSchema.setPrintFlag(tLCGCUWMasterSchema.getPrintFlag());
        tLCGCUWSubSchema.setPrintFlag2(tLCGCUWMasterSchema.getPrintFlag2());
        tLCGCUWSubSchema.setUWIdea(tLCGCUWMasterSchema.getUWIdea());
        tLCGCUWSubSchema.setOperator(tLCGCUWMasterSchema.getOperator()); //操作员
        tLCGCUWSubSchema.setManageCom(tLCGCUWMasterSchema.getManageCom());
        tLCGCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        mLCGCUWSubSet.clear();
        mLCGCUWSubSet.add(tLCGCUWSubSchema);

        // 核保错误信息表
        LCGCUWErrorSchema tLCGCUWErrorSchema = new LCGCUWErrorSchema();
        LCGCUWErrorDB tLCGCUWErrorDB = new LCGCUWErrorDB();
        tLCGCUWErrorDB.setGrpContNo(mGrpContNo);

        LCGCUWErrorSet tLCGCUWErrorSet = new LCGCUWErrorSet();
        tLCGCUWErrorSet = tLCGCUWErrorDB.query();

        if (tLCGCUWErrorDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWErrorDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = mGrpContNo + "合同错误信息表查询失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        tLCGCUWErrorSchema.setSerialNo("0");

        if (nUWNo > 0)
        {
            tLCGCUWErrorSchema.setUWNo(nUWNo);
        }
        else
        {
            tLCGCUWErrorSchema.setUWNo(1);
        }

        tLCGCUWErrorSchema.setGrpContNo(mGrpContNo);
        tLCGCUWErrorSchema.setProposalGrpContNo(tLCGCUWSubSchema
                .getProposalGrpContNo());
        tLCGCUWErrorSchema.setManageCom(tLCGCUWSubSchema.getManageCom());
        tLCGCUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCGCUWErrorSchema.setUWError(""); //核保出错信息
        tLCGCUWErrorSchema.setCurrValue(""); //当前值
        tLCGCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGCUWErrorSchema.setUWPassFlag(mPolPassFlag);

        //取核保错误信息
        mLCGCUWErrorSet.clear();

        int merrcount = tLMUWSetContUnpass.size();

        if (merrcount > 0)
        {
            for (int i = 1; i <= merrcount; i++)
            {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetContUnpass.get(i);

                /**
                 * @Author：Yangming
                 * 目的为使自核结论中能显示动态结论，因此对自核结论
                 * 添加算法解析使之能有更大的扩展功能
                 * 方式:
                 * 在核保结论中的Ｓｑｌ添加到＄＄之间，例如：
                 * 参保比例为$select float(OnWorkPeoples)/float(OffWorkPeoples) from lcgrpcont where grpcontno='?GrpContNo?'$
                 * 注意：ｓｑｌ语句只支持一个返回值，也就是查询结果只能是一个字段．
                 */
                System.out
                        .println("GrpUWAutoChkBL.preparePolUW(tLCGrpPolSchema, tLMUWSetUnpass)  \n--Line:1296  --Author:YangMing");
                String uwResult = parseUWResult(tLMUWSchema);

                //生成流水号
                String tserialno = "" + i;

                tLCGCUWErrorSchema.setSerialNo(tserialno);
                tLCGCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                tLCGCUWErrorSchema.setUWError(uwResult.trim()); //核保出错信息，即核保规则的文字描述内容
                tLCGCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCGCUWErrorSchema.setCurrValue(""); //当前值

                LCGCUWErrorSchema ttLCGCUWErrorSchema = new LCGCUWErrorSchema();
                ttLCGCUWErrorSchema.setSchema(tLCGCUWErrorSchema);
                mLCGCUWErrorSet.add(ttLCGCUWErrorSchema);
            }
        }

        return true;
    }

    /**
     * 准备险种核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePolUW(LCGrpPolSchema tLCGrpPolSchema,
            LMUWSet tLMUWSetUnpass)
    {
        int tuwno = 0;
        LCGUWMasterSchema tLCGUWMasterSchema = new LCGUWMasterSchema();
        LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
        tLCGUWMasterDB.setGrpPolNo(mGrpPolNo);

        LCGUWMasterSet tLCGUWMasterSet = new LCGUWMasterSet();
        tLCGUWMasterSet = tLCGUWMasterDB.query();

        if (tLCGUWMasterDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mGrpPolNo + "个人核保总表取数失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        int n = tLCGUWMasterSet.size();

        if (n == 0)
        {
            tLCGUWMasterSchema.setGrpContNo(mGrpContNo);
            tLCGUWMasterSchema.setGrpPolNo(mGrpPolNo);
            tLCGUWMasterSchema.setProposalGrpContNo(mProposalGrpContNo);
            tLCGUWMasterSchema.setGrpProposalNo(tLCGrpPolSchema
                    .getGrpProposalNo());
            tLCGUWMasterSchema.setUWNo(1);
            tLCGUWMasterSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLCGUWMasterSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            tLCGUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCGUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCGUWMasterSchema.setPostponeDay("");
            tLCGUWMasterSchema.setPostponeDate("");
            tLCGUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCGUWMasterSchema.setState(mPolPassFlag);
            tLCGUWMasterSchema.setPassFlag(mPolPassFlag);
            tLCGUWMasterSchema.setHealthFlag("0");
            tLCGUWMasterSchema.setSpecFlag("0");
            tLCGUWMasterSchema.setQuesFlag("0");
            tLCGUWMasterSchema.setReportFlag("0");
            tLCGUWMasterSchema.setChangePolFlag("0");
            tLCGUWMasterSchema.setPrintFlag("0");
            tLCGUWMasterSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLCGUWMasterSchema.setUWIdea("");
            tLCGUWMasterSchema.setUpReportContent("");
            tLCGUWMasterSchema.setOperator(mOperator); //操作员
            tLCGUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCGUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCGUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else if (n == 1)
        {
            tLCGUWMasterSchema = tLCGUWMasterSet.get(1);

            tuwno = tLCGUWMasterSchema.getUWNo();
            tuwno = tuwno + 1;

            tLCGUWMasterSchema.setUWNo(tuwno);
            tLCGUWMasterSchema.setProposalGrpContNo(mProposalGrpContNo);
            tLCGUWMasterSchema.setState(mPolPassFlag);
            tLCGUWMasterSchema.setPassFlag(mPolPassFlag);
            tLCGUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCGUWMasterSchema.setUWGrade(mUWGrade); //核保级别
            tLCGUWMasterSchema.setAppGrade(mUWGrade); //申报级别
            tLCGUWMasterSchema.setOperator(mOperator); //操作员
            tLCGUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mGrpPolNo + "个人核保总表取数据不唯一!";
            this.mErrors.addOneError(tError);

            return false;
        }

        mLCGUWMasterSet.clear();
        mLCGUWMasterSet.add(tLCGUWMasterSchema);

        // 核保轨迹表
        LCGUWSubSchema tLCGUWSubSchema = new LCGUWSubSchema();
        LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB();
        tLCGUWSubDB.setGrpPolNo(mGrpPolNo);

        LCGUWSubSet tLCGUWSubSet = new LCGUWSubSet();
        tLCGUWSubSet = tLCGUWSubDB.query();

        if (tLCGUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mGrpPolNo + "个人核保轨迹表查失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        int m = tLCGUWSubSet.size();

        if (m > 0)
        {
            tLCGUWSubSchema.setUWNo(++m); //第几次核保
        }
        else
        {
            tLCGUWSubSchema.setUWNo(1); //第1次核保
        }

        tLCGUWSubSchema.setGrpContNo(mGrpContNo);
        tLCGUWSubSchema.setGrpPolNo(mGrpPolNo);
        tLCGUWSubSchema.setProposalGrpContNo(tLCGUWMasterSchema
                .getProposalGrpContNo());
        tLCGUWSubSchema.setGrpProposalNo(tLCGUWMasterSchema.getGrpProposalNo());
        tLCGUWSubSchema.setAgentCode(tLCGUWMasterSchema.getAgentCode());
        tLCGUWSubSchema.setAgentGroup(tLCGUWMasterSchema.getAgentGroup());
        tLCGUWSubSchema.setUWGrade(tLCGUWMasterSchema.getUWGrade()); //核保级别
        tLCGUWSubSchema.setAppGrade(tLCGUWMasterSchema.getAppGrade()); //申请级别
        tLCGUWSubSchema.setAutoUWFlag(tLCGUWMasterSchema.getAutoUWFlag());
        tLCGUWSubSchema.setState(tLCGUWMasterSchema.getState());
        tLCGUWSubSchema.setPassFlag(tLCGUWMasterSchema.getState());
        tLCGUWSubSchema.setPostponeDay(tLCGUWMasterSchema.getPostponeDay());
        tLCGUWSubSchema.setPostponeDate(tLCGUWMasterSchema.getPostponeDate());
        tLCGUWSubSchema.setUpReportContent(tLCGUWMasterSchema
                .getUpReportContent());
        tLCGUWSubSchema.setHealthFlag(tLCGUWMasterSchema.getHealthFlag());
        tLCGUWSubSchema.setSpecFlag(tLCGUWMasterSchema.getSpecFlag());
        tLCGUWSubSchema.setSpecReason(tLCGUWMasterSchema.getSpecReason());
        tLCGUWSubSchema.setQuesFlag(tLCGUWMasterSchema.getQuesFlag());
        tLCGUWSubSchema.setReportFlag(tLCGUWMasterSchema.getReportFlag());
        tLCGUWSubSchema.setChangePolFlag(tLCGUWMasterSchema.getChangePolFlag());
        tLCGUWSubSchema.setChangePolReason(tLCGUWMasterSchema
                .getChangePolReason());
        tLCGUWSubSchema.setAddPremReason(tLCGUWMasterSchema.getAddPremReason());
        tLCGUWSubSchema.setPrintFlag(tLCGUWMasterSchema.getPrintFlag());
        tLCGUWSubSchema.setPrintFlag2(tLCGUWMasterSchema.getPrintFlag2());
        tLCGUWSubSchema.setUWIdea(tLCGUWMasterSchema.getUWIdea());
        tLCGUWSubSchema.setOperator(tLCGUWMasterSchema.getOperator()); //操作员
        tLCGUWSubSchema.setManageCom(tLCGUWMasterSchema.getManageCom());
        tLCGUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        mLCGUWSubSet.clear();
        mLCGUWSubSet.add(tLCGUWSubSchema);

        // 核保错误信息表
        LCGUWErrorSchema tLCGUWErrorSchema = new LCGUWErrorSchema();
        LCGUWErrorDB tLCGUWErrorDB = new LCGUWErrorDB();
        tLCGUWErrorDB.setGrpPolNo(mGrpPolNo);

        LCGUWErrorSet tLCGUWErrorSet = new LCGUWErrorSet();
        tLCGUWErrorSet = tLCGUWErrorDB.query();

        if (tLCGUWErrorDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGUWErrorDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mGrpPolNo + "个人错误信息表查询失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        tLCGUWErrorSchema.setSerialNo("0");

        if (m > 0)
        {
            tLCGUWErrorSchema.setUWNo(m);
        }
        else
        {
            tLCGUWErrorSchema.setUWNo(1);
        }

        tLCGUWErrorSchema.setGrpContNo(mGrpContNo);
        tLCGUWErrorSchema.setProposalGrpContNo(mProposalGrpContNo);
        tLCGUWErrorSchema.setGrpPolNo(mGrpPolNo);
        tLCGUWErrorSchema.setGrpProposalNo(tLCGrpPolSchema.getGrpProposalNo());
        tLCGUWErrorSchema.setManageCom(tLCGrpPolSchema.getManageCom());
        tLCGUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCGUWErrorSchema.setUWError(""); //核保出错信息
        tLCGUWErrorSchema.setCurrValue(""); //当前值
        tLCGUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGUWErrorSchema.setUWPassFlag(mPolPassFlag);

        //取核保错误信息
        mLCGUWErrorSet.clear();

        int merrcount = tLMUWSetUnpass.size();

        if (merrcount > 0)
        {
            for (int i = 1; i <= merrcount; i++)
            {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = tLMUWSetUnpass.get(i);

                //生成流水号
                String tserialno = "" + i;

                /**
                 * @Author：Yangming
                 * 目的为使自核结论中能显示动态结论，因此对自核结论
                 * 添加算法解析使之能有更大的扩展功能
                 * 方式:
                 * 在核保结论中的Ｓｑｌ添加到＄＄之间，例如：
                 * 参保比例为$select float(OnWorkPeoples)/float(OffWorkPeoples) from lcgrpcont where grpcontno='?GrpContNo?'$
                 * 注意：ｓｑｌ语句只支持一个返回值，也就是查询结果只能是一个字段．
                 */
                System.out
                        .println("GrpUWAutoChkBL.preparePolUW(tLCGrpPolSchema, tLMUWSetUnpass)  \n--Line:1296  --Author:YangMing");
                String uwResult = parseUWResult(tLMUWSchema);

                tLCGUWErrorSchema.setSerialNo(tserialno);
                tLCGUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                tLCGUWErrorSchema.setUWError(uwResult); //核保出错信息，即核保规则的文字描述内容
                tLCGUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCGUWErrorSchema.setCurrValue(""); //当前值

                LCGUWErrorSchema ttLCGUWErrorSchema = new LCGUWErrorSchema();
                ttLCGUWErrorSchema.setSchema(tLCGUWErrorSchema);
                mLCGUWErrorSet.add(ttLCGUWErrorSchema);
            }
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private boolean prepareOutputData(LCGrpContSchema tLCGrpContSchema)
    {
        mMap.put(tLCGrpContSchema, "UPDATE");
        mMap.put(mLCGCUWMasterSet.get(1), "DELETE&INSERT");
        mMap.put(mLCGCUWSubSet, "INSERT");
        mMap.put(mLCGCUWErrorSet, "INSERT");

        mMap.put(mAllLCGrpPolSet, "UPDATE");

        //		int n = mAllLCGUWMasterSet.size();

        //		for (int i = 1; i <= n; i++)
        //		{
        //			LCGUWMasterSchema tLCGUWMasterSchema = mAllLCGUWMasterSet.get(i);
        //			mMap.put(tLCGUWMasterSchema, "DELETE&INSERT");
        //		}
        mMap.put(mAllLCGUWMasterSet, "DELETE&INSERT");

        mMap.put(mAllLCGUWSubSet, "INSERT");
        mMap.put(mAllLCErrSet, "INSERT");

        return false;
    }

    public VData getResult()
    {
        return mResult;
    }

    private String parseUWResult(LMUWSchema tLMUWSchema)
    {
        System.out
                .println("GrpUWAutoChkBL.parseUWResult(tLMUWSchema)  \n--Line:1350  --Author:YangMing");
        String result = null;
        PubCalculator tPubCalculator = new PubCalculator();
        tPubCalculator.addBasicFactor("GrpContNo", this.mGrpContNo);
        String tSql = tLMUWSchema.getRemark().trim();
        String tStr = "";
        String tStr1 = "";
        try
        {
            while (true)
            {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals(""))
                {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator
                        .calculate());
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Calculator";
            tError.functionName = "interpretFactorInSQL";
            tError.errorMessage = "解释" + tSql + "的变量:" + tStr + "时出错。";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tSql.trim().equals(""))
        {
            return tLMUWSchema.getRemark();
        }
        return tSql;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpUWAutoChkBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
