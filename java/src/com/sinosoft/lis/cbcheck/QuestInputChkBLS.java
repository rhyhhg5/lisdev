package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统问题件功能部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class QuestInputChkBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public QuestInputChkBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start QuestInputChkBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End QuestInputChkBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{

	  	LCIssuePolSet mLCIssuePolSet = (LCIssuePolSet)mInputData.getObjectByObjectName("LCIssuePolSet",0);
		  LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
		  LCGrpPolSet mLCGrpPolSet = (LCGrpPolSet)mInputData.getObjectByObjectName("LCGrpPolSet",0);
		  LCUWMasterSchema mLCUWMasterSchema = (LCUWMasterSchema)mInputData.getObjectByObjectName("LCUWMasterSchema",0);
		  Connection conn = DBConnPool.getConnection();
		//conn = .getDefaultConnection();

	    	if (conn==null)
	    	{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QuestInputChkBLS";
			tError.functionName = "saveData";
			tError.errorMessage = "数据库连接失败!";
			this.mErrors .addOneError(tError) ;
			return false;
	    	}

		try
		{
			conn.setAutoCommit(false);

			// 删除部分
			int pnum = mLCPolSet.size();
			if( pnum > 0)
			{
			  for(int i = 1;i<= pnum;i++)
			  {
				LCPolSchema tLCPolSchema = new LCPolSchema();
				tLCPolSchema = mLCPolSet.get(i);
				LCPolDB tLCPolDB = new LCPolDB(conn);
				tLCPolDB.setProposalNo(tLCPolSchema.getProposalNo());
				if (tLCPolDB.deleteSQL() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLCPolDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "QuestInputChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = "LCPol表删除失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}
			  }
			}

			//集体
			int gnum = mLCGrpPolSet.size();
			if( gnum > 0)
			{
			  for(int i = 1;i<= gnum;i++)
			  {
				LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
				tLCGrpPolSchema = mLCGrpPolSet.get(i);
				LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB(conn);
				tLCGrpPolDB.setGrpProposalNo(tLCGrpPolSchema.getGrpProposalNo());
				if (tLCGrpPolDB.deleteSQL() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "QuestInputChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = "LCGrpPol表删除失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}
			  }
			}


			int polCount = mLCIssuePolSet.size();
			if (polCount > 0)
			{
				LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();
				tLCIssuePolSchema = ( LCIssuePolSchema )mLCIssuePolSet.get( 1 );
				String tContNo = tLCIssuePolSchema.getContNo();

				// 问题件表
				LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB( conn );
				tLCIssuePolDB.setContNo( tContNo );

				/*sxy-2003-09-18将其注释(为问题件表添加了流水号字段,以便处理同一问题件问题.同一问题件是指:同一投保单,同一问题,同一操作岗位,即未添流水号前的问题件表的主键相同者)
				tLCIssuePolDB.setIssueType(tLCIssuePolSchema.getIssueType());
				tLCIssuePolDB.setOperatePos(tLCIssuePolSchema.getOperatePos());
				if (tLCIssuePolDB.deleteSQL() == false)
				{
				    // @@错误处理
				    this.mErrors.copyAllErrors(tLCIssuePolDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "QuestInputChkBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "LCIssuePol表删除失败!";
					this.mErrors .addOneError(tError) ;
					conn.rollback() ;
					conn.close();
					return false;
				}
                */


				// 保存部分
				// 保单
				pnum = mLCPolSet.size();
				if( pnum > 0)
				{
				  for(int i = 1;i<= pnum;i++)
				  {
					LCPolSchema tLCPolSchema = new LCPolSchema();
					tLCPolSchema = mLCPolSet.get(i);
					LCPolDB tLCPolDB = new LCPolDB(conn);
					tLCPolDB.setSchema(tLCPolSchema);
					if (tLCPolDB.insert() == false)
					{
					  // @@错误处理
					  this.mErrors.copyAllErrors(tLCPolDB.mErrors);
					  CError tError = new CError();
					  tError.moduleName = "QuestInputChkBLS";
					  tError.functionName = "saveData";
					  tError.errorMessage = "LCPol表插入失败!";
					  this.mErrors .addOneError(tError) ;
					  conn.rollback() ;
					  conn.close();
					  return false;
					}
				  }
				}

				//集体
				gnum = mLCGrpPolSet.size();
				if( gnum > 0)
				{
				  for(int i = 1;i<= gnum;i++)
				  {
					LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
					tLCGrpPolSchema = mLCGrpPolSet.get(i);
					LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB(conn);
					tLCGrpPolDB.setSchema(tLCGrpPolSchema);
					if (tLCGrpPolDB.insert() == false)
					{
					  // @@错误处理
					  this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
					  CError tError = new CError();
					  tError.moduleName = "QuestInputChkBLS";
					  tError.functionName = "saveData";
					  tError.errorMessage = "LCGrpPol表插入失败!";
					  this.mErrors .addOneError(tError) ;
					  conn.rollback() ;
					  conn.close();
					  return false;
					}
				  }
				}

System.out.println("-----------DDD--------------");

				//LCPolDBSet tLCPolDBSet = new LCPolDBSet( conn );
				//tLCPolDBSet.set( mLCPolSet );
				//if (tLCPolDBSet.insert() == false)
				//LCPolDB tLCPolDB = new LCPolDB();
				tLCIssuePolDB.setSchema(tLCIssuePolSchema);

				if(tLCIssuePolDB.insert() == false)
				{
					// @@错误处理
				    	this.mErrors.copyAllErrors(tLCIssuePolDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "QuestInputChkBLS";
					tError.functionName = "saveData";
					tError.errorMessage = "LCIssuePol表保存失败!";
					this.mErrors .addOneError(tError) ;
		        		conn.rollback() ;
                                        conn.close();
					return false;
				}



System.out.println("-----------EEE--------------");
                                if(mLCUWMasterSchema.getPolNo() != null)
                                {
                                  LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
                                  tLCUWMasterDB.setSchema(mLCUWMasterSchema);
                                  if(tLCUWMasterDB.update() == false)
                                  {
                                    // @@错误处理
                                    this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                                    CError tError = new CError();
                                    tError.moduleName = "QuestInputChkBLS";
                                    tError.functionName = "saveData";
                                    tError.errorMessage = "LCUWMaster表更新失败!";
                                    this.mErrors .addOneError(tError) ;
                                    conn.rollback() ;
                                    conn.close();
                                    return false;
                                  }
                                }


				conn.commit() ;

			} // end of for
                        conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QuestInputChkBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}
			return false;
		}

	    return true;
	}

}
