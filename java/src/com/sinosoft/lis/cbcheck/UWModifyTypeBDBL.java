package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author tjj
 * @version 1.0
 */
public class UWModifyTypeBDBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private String mPolNo;
  private String mBonusGetMode;

  /**  */

  private  LCPolSchema  mLCPolSchema= new LCPolSchema();
  private  LCDutySet mLCDutySet = new LCDutySet();
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  private LPEdorMainSet outLPEdorMainSet = new LPEdorMainSet();

  public UWModifyTypeBDBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public void setOperate(String cOperate)
  {
	this.mOperate=cOperate;
  }

  public  String getOperate()
  {
	return this.mOperate;
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
	System.out.println("--------------in SubMit-------");
	//将操作数据拷贝到本类中
	mInputData = (VData)cInputData.clone() ;

	//得到外部传入的数据,将数据备份到本类中
	getInputData(cInputData)  ;


	 //数据校验操作（checkdata)
	if (!checkData())
	  return false;
	//数据准备操作（preparedata())
	if(!prepareData() )
	   return false;

 cOperate=this.getOperate();
 System.out.println("---oper:"+cOperate);

 UWModifyTypeBDBLS tUWModifyTypeBDBLS = new UWModifyTypeBDBLS();
 if(tUWModifyTypeBDBLS.submitData(mInputData, "") == false)	{
   //@@错误处理
   this.mErrors.copyAllErrors(tUWModifyTypeBDBLS.mErrors);
   return false;

 }
 return true;
  }

  public VData getResult()
  {
	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private void getInputData(VData cInputData)
  {
	mPolNo = (String) cInputData.getObjectByObjectName("String",0);
	mBonusGetMode=(String)cInputData.getObjectByObjectName("String",1);
	mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);
  }
  /**
   *只是撤销本次申请的某个保全项目
   */

  /**
   * 校验传入的数据的合法性
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkData()
  {

	boolean flag = true;
	if(mBonusGetMode == null || mBonusGetMode.trim().equals("") )
	{
	  // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "UWModifyTypeBDBL";
		 tError.functionName = "checkdata";
		 tError.errorMessage = "前台传输红利领取方式数据失败!";
		 this.mErrors.addOneError(tError);
		  return false;
	}

	if(mPolNo == null || mPolNo.trim().equals("") )
	{
	  // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "UWModifyTypeBDBL";
		 tError.functionName = "checkdata";
		 tError.errorMessage = "前台传输投保单号数据失败!";
		 this.mErrors.addOneError(tError);
		  return false;
	}

	LCPolDB tLCPolDB = new LCPolDB();
	LCPolSet tLCPolSet = new LCPolSet();
	tLCPolDB.setProposalNo(mPolNo) ;
	tLCPolSet = tLCPolDB.query() ;
	if(tLCPolSet== null || tLCPolSet.size() != 1)
	{
	   // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "UWModifyTypeBDBL";
		 tError.functionName = "checkdata";
		 tError.errorMessage = "投保单:"+mPolNo+"数据查询失败!";
		 this.mErrors.addOneError(tError);
		  return false;
	}
	mLCPolSchema = tLCPolSet.get(1) ;

	LCDutyDB tLCDutyDB = new LCDutyDB();
    tLCDutyDB.setPolNo(mPolNo) ;
	mLCDutySet = tLCDutyDB.query() ;
	if(mLCDutySet== null || mLCDutySet.size() != 1)
	{
	   // @@错误处理
		 CError tError = new CError();
		 tError.moduleName = "UWModifyTypeBDBL";
		 tError.functionName = "checkdata";
		 tError.errorMessage = "投保单:"+mPolNo+"责任项数据查询失败!";
		 this.mErrors.addOneError(tError);
		  return false;
	}

	return flag;
  }

  /**
   * 准备需要保存的数据
   */
  private boolean prepareData()
  {
	System.out.println("start prepare Data....");
	String cOperate;

	//准备个人投保单表红利信息
    mLCPolSchema.setBonusGetMode(mBonusGetMode);
	mLCPolSchema.setModifyDate(PubFun.getCurrentDate());
    mLCPolSchema.setModifyTime(PubFun.getCurrentTime());

	//准备个人投保单责任表信息
	for(int i= 1 ;i<= mLCDutySet.size() ;i++)
    {
	  mLCDutySet.get(i).setBonusGetMode(mBonusGetMode);
	  mLCDutySet.get(i).setModifyDate(PubFun.getCurrentDate());
      mLCDutySet.get(i).setModifyTime(PubFun.getCurrentTime());
    }


	mInputData.clear();

	mInputData.add(mLCPolSchema) ;

	if(mLCDutySet.size()>0)
	mInputData.add(mLCDutySet) ;
		return true;

  }


}