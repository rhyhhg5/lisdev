/*
 * <p>ClassName: OLCPENoticeBL </p>
 * <p>Description: OLCPENoticeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-06-28 19:20:10
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.SSRS;
public class OLCPENoticeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    private LCContSchema mLCContSchema = new LCContSchema();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String mManageCom;
    /** 业务处理相关变量 */
    private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();
//private LCPENoticeSet mLCPENoticeSet=new LCPENoticeSet();
    public OLCPENoticeBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLCPENoticeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLCPENoticeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLCPENoticeBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLCPENoticeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("END OLCPENoticeBL Submit...");

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
                ExeSQL tExeSQL = new ExeSQL();
        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
        tLCPENoticeDB.setProposalContNo(mLCPENoticeSchema.getProposalContNo());
        tLCPENoticeDB.setPrtSeq(mLCPENoticeSchema.getPrtSeq());
        if (!tLCPENoticeDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "OLCPENoticeBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            String Date = mLCPENoticeSchema.getBookingDate();
            System.out.println("update LCPENotice set  BookingDate='" +
                               mLCPENoticeSchema.getBookingDate()
                               + "',BookingTime='" + mLCPENoticeSchema.getBookingTime()
                               + "',PEState='" + mLCPENoticeSchema.getPEState() + "'"
                               + " where ProposalContNo='" + mLCPENoticeSchema.getProposalContNo()
                               + "' and PrtSeq='" + mLCPENoticeSchema.getPrtSeq() + "'");
            String State = mLCPENoticeSchema.getPEState();
            if (!State.equals("04") && !State.equals("05"))
                State = "01";
            map.put("update LCPENotice set  BookingDate='" + mLCPENoticeSchema.getBookingDate()
                    + "',BookingTime='" + mLCPENoticeSchema.getBookingTime()
                    + "',ContNo='" + mLCPENoticeSchema.getContNo()
                    + "',PEAddress='" + mLCPENoticeSchema.getPEAddress()
                    + "',PEDate='" + mLCPENoticeSchema.getPEDate()
                    + "',Remark='" + mLCPENoticeSchema.getRemark()
                    + "',CustomerNo='" + mLCPENoticeSchema.getCustomerNo()
                    + "',PEState='" + State + "'"
                    + " where ProposalContNo='" + mLCPENoticeSchema.getProposalContNo()
                    + "' and PrtSeq='" + mLCPENoticeSchema.getPrtSeq() + "'", "UPDATE");
            String strsql =
                    "SELECT managecom FROM LCPENotice WHERE PrtSeq='" + mLCPENoticeSchema.getPrtSeq() + "'";
           SSRS ManageCom1 = tExeSQL.execSQL(strsql.toString());
            mManageCom=ManageCom1.GetText(1,1);
            String strsql1 =
        "SELECT OPERATOR FROM LCPENotice WHERE PrtSeq='" + mLCPENoticeSchema.getPrtSeq() + "'";
          SSRS OPERATOR = tExeSQL.execSQL(strsql1.toString());
          String mOPERATOR1=OPERATOR.GetText(1,1);

            System.out.println("$$ : "+mManageCom);
            System.out.println(mOPERATOR1);
            System.out.println( mLCPENoticeSchema.getPrtSeq());
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerDB.setOtherNo(mLCPENoticeSchema.getContNo());
            tLOPRTManagerDB.setCode(PrintManagerBL.CODE_REPE);
            LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
            if (tLOPRTManagerSet.size()== 0)
            {
                    // @@错误处理
                    this.mErrors.copyAllErrors( tLOPRTManagerDB.mErrors );
                    CError tError = new CError();
                    tError.moduleName = "OLCPENoticeBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "体检回执打印查询失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
            }
            else
            {
                 tLOPRTManagerSchema = ( LOPRTManagerSchema )tLOPRTManagerSet.get( 1 );
                 tLOPRTManagerSchema.setStandbyFlag4("1");//体检已预约标记
                 map.put(tLOPRTManagerSchema,"UPDATE");
            }


             //下面map功能移至到人工核保体检录入中去
            //LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            //tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPE);
            //String prtSeq = PubFun1.CreateMaxNo("PRTSEQNO","SN");
            //tLOPRTManagerSchema.setPrtSeq(prtSeq);
            //tLOPRTManagerSchema.setOtherNo(mLCPENoticeSchema.getContNo());
            //tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //保单号
            //tLOPRTManagerSchema.setManageCom(mManageCom);
            //tLOPRTManagerSchema.setAgentCode(mLCPENoticeSchema.getAgentCode());
            //tLOPRTManagerSchema.setReqCom(mManageCom);
            //tLOPRTManagerSchema.setReqOperator(mOPERATOR1);
            ////tLOPRTManagerSchema.setExeCom();
            ////tLOPRTManagerSchema.setExeOperator();
            //tLOPRTManagerSchema.setStandbyFlag1(mLCPENoticeSchema.getCustomerNo());
            //tLOPRTManagerSchema.setStandbyFlag2(mLCPENoticeSchema.getPrtSeq());
            //tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT); //前台打印
            //tLOPRTManagerSchema.setStateFlag("0");
            //tLOPRTManagerSchema.setPatchFlag("0");
            //tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            //tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            //下面map功能移至到人工核保体检录入中去
            //map.put(tLOPRTManagerSchema,"INSERT");
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLCPENoticeSchema.setSchema((LCPENoticeSchema) cInputData.getObjectByObjectName(
                "LCPENoticeSchema", 0));
//            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
//               "GlobalInput", 0));
          return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
        tLCPENoticeDB.setSchema(this.mLCPENoticeSchema);
        //如果有需要处理的错误，则返回
        if (tLCPENoticeDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPENoticeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCPENoticeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLCPENoticeSchema);
            this.mInputData.add(this.map);
            mResult.clear();
            mResult.add(this.mLCPENoticeSchema);
            mResult.add(this.map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPENoticeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
