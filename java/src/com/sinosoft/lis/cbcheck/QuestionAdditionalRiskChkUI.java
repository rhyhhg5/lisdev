package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class QuestionAdditionalRiskChkUI {

  private VData mInputData ;
  private VData mResult = new VData();
  public  CErrors mErrors=new CErrors();
  public QuestionAdditionalRiskChkUI()
  {
  }
  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    QuestionAdditionalRiskChkBL tQuestionAdditionalRiskChkBL=new QuestionAdditionalRiskChkBL();
    System.out.println("Start QuestionAdditionalRiskChk BL Submit...");
    tQuestionAdditionalRiskChkBL.submitData(mInputData,cOperate);

    System.out.println("End QuestionAdditionalRiskChk BL Submit...");

    //如果有需要处理的错误，则返回
    if (tQuestionAdditionalRiskChkBL .mErrors .needDealError() )
    {
        mErrors .copyAllErrors(tQuestionAdditionalRiskChkBL.mErrors ) ;
        return false;
    }
    mResult.clear();
    mResult=tQuestionAdditionalRiskChkBL.getResult();
    mInputData=null;
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

}