package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class UWModifyTypeBCUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public UWModifyTypeBCUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate = cOperate;
	UWModifyTypeBCBL tUWModifyTypeBCBL = new UWModifyTypeBCBL();
	System.out.println("---UI BEGIN---"+mOperate);
	if (tUWModifyTypeBCBL.submitData(cInputData,mOperate) == false)
	{
	  // @@错误处理
	  this.mErrors.copyAllErrors(tUWModifyTypeBCBL.mErrors);
	  return false;
	}
	else
	  mResult = tUWModifyTypeBCBL.getResult();
	return true;
  }

  public VData getResult()
  {
	return mResult;
  }

  public static void main(String[] args)
  {
	VData tInputData=new VData();
	GlobalInput tGlobalInput = new GlobalInput();
	UWModifyTypeBCUI aUWModifyTypeBCUI = new UWModifyTypeBCUI();
	LCBnfSet tLPBnfSet = new LCBnfSet();
	LCBnfSchema tLPBnfSchema = new LCBnfSchema();
	tGlobalInput.ManageCom="86";
	tGlobalInput.Operator="001";

	//tLPBnfSchema.setAddress("test");
	//tLPBnfSchema.setBirthday("1969-10-01");
	tLPBnfSchema.setBnfGrade("1");
	tLPBnfSchema.setBnfLot("1");
	tLPBnfSchema.setBnfType("1");
	//tLPBnfSchema.setCustomerNo("");
	tLPBnfSchema.setIDNo("110210690321121");
	tLPBnfSchema.setIDType("0");
	tLPBnfSchema.setName("王7");
	//tLPBnfSchema.setPhone("1111");
	tLPBnfSchema.setPolNo("86110020030110000049");
	tLPBnfSchema.setRelationToInsured("27");
//	tLPBnfSchema.setRgtAddress("test");
	//tLPBnfSchema.setSex("0");
	//tLPBnfSchema.setZipCode("111101");
	tLPBnfSet.add(tLPBnfSchema);

	LCBnfSchema iLPBnfSchema = new LCBnfSchema();
//	iLPBnfSchema.setAddress("test");
	iLPBnfSchema.setBirthday("1969-10-01");
	iLPBnfSchema.setBnfGrade("2");
	iLPBnfSchema.setBnfLot("1");
	iLPBnfSchema.setBnfType("1");
	//iLPBnfSchema.setCustomerNo("");
	iLPBnfSchema.setIDNo("110210690321121");
	iLPBnfSchema.setIDType("0");
	iLPBnfSchema.setName("peter");
	//iLPBnfSchema.setPhone("1111");
	iLPBnfSchema.setPolNo("86110020030110000049");
	iLPBnfSchema.setCustomerNo("");
	iLPBnfSchema.setRelationToInsured("27");
//	iLPBnfSchema.setRgtAddress("test");
	iLPBnfSchema.setSex("0");
//	iLPBnfSchema.setZipCode("111101");
	tLPBnfSet.add(iLPBnfSchema);

	tInputData.addElement(tLPBnfSet);
	tInputData.addElement(tGlobalInput);
	tInputData.addElement("86110020030110000049");
	aUWModifyTypeBCUI.submitData(tInputData,"INSERT||MAIN");

	System.out.println("-------test...");
  }
}
