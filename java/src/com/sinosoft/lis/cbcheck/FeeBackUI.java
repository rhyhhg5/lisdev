package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统个人充帐功能部分</p>
 * <p>Description:接口功能类 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class FeeBackUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public FeeBackUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ManageCom = "86";

  LCPolSchema p = new LCPolSchema();
  p.setProposalNo( "86120020020220000012" );
  p.setPolNo("86120020020220000012");

  LCPolSet tLCPolSet = new LCPolSet();
  tLCPolSet.add(p);

  //
  LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

  tLCPENoticeSchema.setContNo("00010120020110000007");
  tLCPENoticeSchema.setCustomerNo("33");
  tLCPENoticeSchema.setPEAddress("001000000");
  tLCPENoticeSchema.setPEDate("2002-10-31");
  tLCPENoticeSchema.setPEBeforeCond("0");
  tLCPENoticeSchema.setRemark("TEST");

  LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
  tLCPENoticeSet.add(tLCPENoticeSchema);

  //
  LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();

  tLCPENoticeItemSchema.setPEItemCode("001");
  tLCPENoticeItemSchema.setPEItemName("普通体检");

  LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  tLCPENoticeItemSchema = new LCPENoticeItemSchema();
  tLCPENoticeItemSchema.setPEItemCode("002");
  tLCPENoticeItemSchema.setPEItemName("X光");
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();

  tLCIssuePolSchema.setBackObjType("2");
  tLCIssuePolSchema.setIssueCont("1234567");
  tLCIssuePolSchema.setOperatePos("1");
//  tLCIssuePolSchema.setProposalNo("86110020020110000015");
  tLCIssuePolSchema.setIssueType("001");

  LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();
  tLCIssuePolSet.add(tLCIssuePolSchema);

  //暂交费关联表
  LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
  LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
  tLJTempFeeClassSchema.setTempFeeNo( "1234" );
  tLJTempFeeClassSchema.setPayMode( "1" );

  tLJTempFeeClassSet.add(tLJTempFeeClassSchema);

  //个人实交表
  LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
  tLJAPayPersonSchema.setPayNo("86110020030310000064");

  VData tVData = new VData();
  //tVData.add( tLCPolSet );
  //tVData.add( tLCPENoticeSet);
  //tVData.add( tLCPENoticeItemSet);
  //tVData.add( tLCIssuePolSet);
  tVData.add( tLJAPayPersonSchema);
  tVData.add( tG );
  FeeBackUI ui = new FeeBackUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---OK---");
  else
      System.out.println("---NO---");
      int n = ui.mErrors.getErrorCount();
      for (int i = 0; i < n; i++)
      System.out.println("Error: "+ui.mErrors.getError(i).errorMessage);

  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    FeeBackBL tFeeBackBL = new FeeBackBL();

    System.out.println("---FeeBackBL UI BEGIN2---");
    if (tFeeBackBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tFeeBackBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "FeeBackUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}
    return true;
  }
}
