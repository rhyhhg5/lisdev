package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;

/**
 * <p>Title: Web业务系统工核保投保单申请部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWManuApplyChkBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private String mIsueManageCom;
  private String mManageCom;
  private String mpassflag; //通过标记
  private int merrcount; //错误条数
  private String mCalCode; //计算编码
  private String mUser;
  private FDate fDate = new FDate();
  private double mValue;
  private String mInsuredNo = "";
  private String mBackObj = "";
  private String mflag = ""; //分，个单标记

  /** 业务处理相关变量 */
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSet mmLCPolSet = new LCPolSet();
  private LCPolSet m2LCPolSet = new LCPolSet();
  private LCPolSet mAllLCPolSet = new LCPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private String mPolNo = "";
  private String mOldPolNo = "";
  private String mOperatePos = "";

  /** 集体单表 */
  private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

  /** 保费项表 */
  private LCPremSet mLCPremSet = new LCPremSet();
  private LCPremSet mAllLCPremSet = new LCPremSet();

  /** 领取项表 */
  private LCGetSet mLCGetSet = new LCGetSet();
  private LCGetSet mAllLCGetSet = new LCGetSet();

  /** 责任表 */
  private LCDutySet mLCDutySet = new LCDutySet();
  private LCDutySet mAllLCDutySet = new LCDutySet();

  /** 特别约定表 */
  private LCSpecSet mLCSpecSet = new LCSpecSet();
  private LCSpecSet mAllLCSpecSet = new LCSpecSet();

  /** 特别约定注释表 */
  private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
  private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();

  /** 核保主表 */
  private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();

  /** 核保子表 */
  private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
  private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
  private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();

  /** 核保错误信息表 */
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
  private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();

  /** 告知表 */
  private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
  private LCCustomerImpartSet mAllLCCustomerImpartSet = new LCCustomerImpartSet();

  /** 投保人表 */
  private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
  private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();

  /** 受益人表 */
  private LCBnfSet mLCBnfSet = new LCBnfSet();
  private LCBnfSet mAllLCBnfSet = new LCBnfSet();

  /** 被保险人表 */
  private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
  private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();

  /** 体检资料主表 */
  private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
  private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();
  private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();

  /** 体检资料项目表 */
  private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
  private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
  private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();

  /** 问题件表 */
  private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
  private LCIssuePolSet mmLCIssuePolSet = new LCIssuePolSet();
  private LCIssuePolSet mAllLCIssuePolSet = new LCIssuePolSet();

  /**轨迹锁表**/
  private LDSysTraceSet mLDSysTraceSet = new LDSysTraceSet();
  private LDSysTraceSet mAllLDSysTraceSet = new LDSysTraceSet();

  /**计算公式表**/
  private LMUWSchema mLMUWSchema = new LMUWSchema();

  //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
  private LMUWSet mLMUWSet = new LMUWSet();

  private CalBase mCalBase = new CalBase();

  public UWManuApplyChkBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    //GlobalInput tGlobalInput = new GlobalInput();
    //this.mOperate = tGlobalInput.;

    System.out.println("---1---");
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    System.out.println("---UWManuApplyChkBL getInputData---");

    LCPolSchema tLCPolSchema = new LCPolSchema();
    tLCPolSchema = (LCPolSchema) mLCPolSet.get(1);
    mLCPolSchema = (LCPolSchema) mLCPolSet.get(1);
    mOldPolNo = tLCPolSchema.getProposalNo();
    mPolNo = tLCPolSchema.getProposalNo();

    //校验是不是已经有人申请此保单
    if (!checkApply())
      return false;

    System.out.println("---UWManuApplyChkBL checkData---");
    // 数据操作业务处理
    if (!dealData())
      return false;

    System.out.println("---UWManuApplyChkBL dealData---");
    //准备给后台的数据
    prepareOutputData();

    System.out.println("---UWManuApplyChkBL prepareOutputData---");
    //数据提交
    UWManuApplyChkBLS tUWManuApplyChkBLS = new UWManuApplyChkBLS();
    System.out.println("Start UWManuApplyChkBL Submit...");
    if (!tUWManuApplyChkBLS.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tUWManuApplyChkBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWManuApplyChkkBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---UWManuApplyChkBL commitData---");

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData() {

    if (dealOnePol() == false)
      return false;

    return true;
  }

  /**
   * 操作一张保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealOnePol() {
    // 健康信息
    if (prepareApply() == false)
      return false;

    LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
    tLDSysTraceSet.set(mLDSysTraceSet);
    mAllLDSysTraceSet.add(tLDSysTraceSet);

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mOperate = tGlobalInput.Operator;
    mManageCom = tGlobalInput.ManageCom;

    mLCPolSet.set( (LCPolSet) cInputData.getObjectByObjectName("LCPolSet", 0));

    int n = mLCPolSet.size();
    int flag = 0; //怕判断是不是所有保单都失败
    int j = 0; //符合条件保单个数

    if (n > 0) {
      flag = 1;
    }
    else {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "UWManuApplyChkBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有传入保单信息!";
      this.mErrors.addOneError(tError);
      return false;

    }
    if (flag == 0) {
      return false;
    }
    else {
      return true;
    }
  }

  /**
   * 准备信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean checkApply() {
    LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
    LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB();
    LDSysTraceSet tLDSysTraceSet = new LDSysTraceSet();
    String tsql = "";

    tsql = "select * from ldsystrace where polno = '" + mPolNo +
        "' and operator <> '" + mOperate + "' and polstate = 1001";

    tLDSysTraceSet = tLDSysTraceDB.executeQuery(tsql);

    if (tLDSysTraceSet.size() > 0) {
      tLDSysTraceSchema = tLDSysTraceSet.get(1);

      CError tError = new CError();
      tError.moduleName = "UWManuApplyChkBL";
      tError.functionName = "checkApply";
      tError.errorMessage = "此保单以经被" + tLDSysTraceSchema.getOperator().trim() +
          "核保师申请，申请失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareApply() {
    LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();

    tLDSysTraceSchema.setCreatePos("人工核保");
    tLDSysTraceSchema.setMakeDate(PubFun.getCurrentDate());
    tLDSysTraceSchema.setMakeTime(PubFun.getCurrentTime());
    tLDSysTraceSchema.setModifyDate(PubFun.getCurrentDate());
    tLDSysTraceSchema.setModifyTime(PubFun.getCurrentTime());
    tLDSysTraceSchema.setManageCom(mManageCom);
    tLDSysTraceSchema.setOperator(mOperate);
    tLDSysTraceSchema.setOperator2(mOperate);
    tLDSysTraceSchema.setPolNo(mPolNo);
    tLDSysTraceSchema.setPolState(1001);
    tLDSysTraceSchema.setRemark("U");

    mLDSysTraceSet.add(tLDSysTraceSchema);

    return true;
  }

  /**
   *准备需要保存的数据
   **/
  private void prepareOutputData() {
    mInputData.clear();
    mInputData.add(mAllLDSysTraceSet);
  }
}
