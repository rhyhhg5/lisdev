package com.sinosoft.lis.cbcheck;

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title:团体单问题修改确认BL </p>
 * <p>Description:
 * 对团体单问题修改后进行确认.置团体整单及团单下未核保通过的个单的状态为新单复核状态
 * </p>
 * <p>Copyright: Copyright (c) 2003-08-20</p>
 * <p>Company: </p>
 * @author sxy
 * @version 1.0
 */

public class GrpQuestModifyMakeSureBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  /** 业务处理相关变量 */
  /** 保单数据 */
  private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();
  private LCPolSet mLCPolSet = new LCPolSet();

  /** 复核标记 */
  private String mApproveFlag = "";

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  public GrpQuestModifyMakeSureBL() {
  }

  /**
   * 数据提交的公共方法
   * @param: cInputData 传入的数据
   *		  cOperate 数据操作字符串
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    // 将传入的数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 将外部传入的数据分解到本类的属性中，准备处理
    if (this.getInputData() == false)
      return false;

    // 校验传入的数据
    if (this.checkData() == false)
      return false;

    // 根据业务逻辑对数据进行处理
    if (this.dealData() == false)
      return false;
    System.out.println("---dealData---");

    // 装配处理好的数据，准备给后台进行保存
    this.prepareOutputData();
    System.out.println("---prepareOutputData---");

    //　数据提交、保存
    GrpQuestModifyMakeSureBLS tGrpQuestModifyMakeSureBLS = new
        GrpQuestModifyMakeSureBLS();
    if (tGrpQuestModifyMakeSureBLS.submitData(mInputData, cOperate) == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tGrpQuestModifyMakeSureBLS.mErrors);
      return false;
    }
    System.out.println("---commitData---");

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData() {
    String tGrpPolNo = "";

    //全局变量
    mGlobalInput.setSchema( (GlobalInput) mInputData.getObjectByObjectName(
        "GlobalInput", 0));
    //保单
    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    tLCGrpPolSchema.setSchema( (LCGrpPolSchema) mInputData.
                              getObjectByObjectName("LCGrpPolSchema", 0));
    mApproveFlag = tLCGrpPolSchema.getApproveFlag();

    System.out.println("mApproveFlag:" + mApproveFlag);
    System.out.println("GrpPolNo:" + tLCGrpPolSchema.getGrpPolNo());

    if (mApproveFlag == null || StrTool.cTrim(mApproveFlag).equals("")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpQuestModifyMakeSureBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体复核修改后复核标记没有传入!";
      this.mErrors.addOneError(tError);
      return false;
    }
    tGrpPolNo = tLCGrpPolSchema.getGrpPolNo();

    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    tLCGrpPolDB.setGrpPolNo(tGrpPolNo);
    if (tLCGrpPolDB.getInfo() == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpQuestModifyMakeSureBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体投保单查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mLCGrpPolSchema.setSchema(tLCGrpPolDB);

    String tRiskCode = mLCGrpPolSchema.getRiskCode();
    LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
    tLMRiskAppDB.setRiskCode(tRiskCode);
    if (tLMRiskAppDB.getInfo() == false) {
      this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpQuestModifyMakeSureBL";
      tError.functionName = "getInputData";
      tError.errorMessage = tRiskCode.trim() + "险种信息查询失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (tLMRiskAppDB.getSubRiskFlag().equals("S")) {
      this.mErrors.copyAllErrors(tLMRiskAppDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpQuestModifyMakeSureBL";
      tError.functionName = "getInputData";
      tError.errorMessage = tRiskCode.trim() + "附加险不能单独进行问题修改确认操作!";
      this.mErrors.addOneError(tError);
      return false;
    }

    if (!mLCGrpPolSchema.getApproveFlag().equals("2")) {
      //当人工核保处录入了除操作员的问题件以外的问题件,将会置ApproveFlag=2,发放问题件后,待机构问题件回复后到问题修改处进行修改
      this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpQuestModifyMakeSureBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "集体投保单已问题修改确认过!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 校验传入的数据
   * @param: 无
   * @return: boolean
   */
  private boolean checkData() {
    if (!mLCGrpPolSchema.getAppFlag().trim().equals("0")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "checkData";
      tError.errorMessage = "此团体单不是投保单，不能进行问题修改操作!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println(" uwflag: " + mLCGrpPolSchema.getUWFlag());

    //确保团体投保单下有个人投保单
    ExeSQL tExeSQL = new ExeSQL();
    String sql = "select count(*) from LCPol "
        + "where GrpPolNo = '" + mLCGrpPolSchema.getGrpPolNo() + "'";

    String tStr = "";
    double tCount = -1;
    tStr = tExeSQL.getOneValue(sql);
    if (tStr.trim().equals(""))
      tCount = 0;
    else
      tCount = Double.parseDouble(tStr);

    if (tCount <= 0.0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "团体投保单下没有个人投保单，不能进行复核修改确认操作!";
      this.mErrors.addOneError(tError);
      return false;
    }

    //确保集体投保单进行问题修改确认操作时,已将所有的问题件已回复(目前返回给业务员,保户的问题件不做任何特殊处理,只做为一个记事本的功能)
    sql = "select count(*) from LCIssuePol "
        + "where ProposalNo = '" + mLCGrpPolSchema.getGrpPolNo() +
        "' and backobjtype in ('1','2','3','4') and replyman is null";
    tCount = -1;
    tStr = tExeSQL.getOneValue(sql);
    if (tStr.trim().equals(""))
      tCount = 0;
    else
      tCount = Double.parseDouble(tStr);

    if (tCount >= 1.0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBL";
      tError.functionName = "dealData";
      tError.errorMessage = "集体投保单下有未回复的问题件，不能进行问题修改确认操作!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: boolean
   */
  private boolean dealData() {

    // 修改团体投保单问题修改人编码和复核日期
    mLCGrpPolSchema.setApproveCode(mGlobalInput.Operator);
    mLCGrpPolSchema.setApproveDate(PubFun.getCurrentDate());
    mLCGrpPolSchema.setApproveFlag(mApproveFlag);
    mLCGrpPolSchema.setUWFlag("0");
    mLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
    mLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());

    return true;
  }

  /**
   * 根据业务逻辑对数据进行处理
   * @param: 无
   * @return: void
   */
  private void prepareOutputData() {
    mInputData.clear();
    mInputData.add(mLCGrpPolSchema);

  }

}
