package com.sinosoft.lis.cbcheck;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class TransClaimQueryUI {

	public CErrors mErrors = new CErrors();
	
	/** 往前面传输数据的容器 */
    private String mResult = "";
	
	public boolean submitData(VData cInputData, String cOperate) {
		
		TransClaimQueryBL tTransClaimQueryBL = new TransClaimQueryBL();//BL处理类
		
		if (tTransClaimQueryBL.submitData(cInputData, cOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tTransClaimQueryBL.mErrors);
			return false;
		}else {
            mResult = tTransClaimQueryBL.getResult();
        }
		
		return true;
	}
	
	/**
     * 得到处理后的结果集
     * @return 结果集
     */

    public String getResult()
    {
        return mResult;
    }
	
}
