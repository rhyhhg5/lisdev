package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.vschema.LCGrpPolSet;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class FeeRateUI {
  public FeeRateUI() {
  }

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();

  /** 数据操作字符串 */
  private String mOperate;

//业务处理相关变量
  /** 全局数据 */
  /**
   传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    FeeRateBL tLFeeRateBL = new FeeRateBL();

    //System.out.println("Start OLDSocialInsOrgan UI Submit...");
    tLFeeRateBL.submitData(cInputData, mOperate);
    //System.out.println("End OLDSocialInsOrgan UI Submit...");
    //如果有需要处理的错误，则返回
    if (tLFeeRateBL.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLFeeRateBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "LDSocialInsOrganUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (mOperate.equals("INSERT||MAIN")) {
      this.mResult.clear();
      this.mResult = tLFeeRateBL.getResult();
    }
    mInputData = null;
    return true;
  }

  public static void main(String[] arg) {
    GlobalInput tGI = new GlobalInput();
    tGI.ManageCom = "86";
    tGI.Operator = "001";

    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    tLCGrpPolSchema.setGrpContNo("1400002475");
    tLCGrpPolSchema.setRiskCode("1601");
    tLCGrpPolSchema.setStandbyFlag3("0.8");

    LCGrpPolSchema tLCGrpPolSchema1 = new LCGrpPolSchema();
    tLCGrpPolSchema1.setGrpContNo("1400002475");
    tLCGrpPolSchema1.setRiskCode("1602");
    tLCGrpPolSchema1.setStandbyFlag3("0.5");
    LCGrpPolSchema tLCGrpPolSchema2 = new LCGrpPolSchema();
    tLCGrpPolSchema2.setGrpContNo("1400002475");
    tLCGrpPolSchema2.setRiskCode("5601");
    tLCGrpPolSchema2.setStandbyFlag3("1.00");
    tLCGrpPolSet.add(tLCGrpPolSchema);
    tLCGrpPolSet.add(tLCGrpPolSchema1);
    tLCGrpPolSet.add(tLCGrpPolSchema2);



    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tLCGrpPolSet);

    FeeRateUI tFeeRateUI = new FeeRateUI();
    tFeeRateUI.submitData(tVData, "UPDATE||FEERATE");

  }

}
