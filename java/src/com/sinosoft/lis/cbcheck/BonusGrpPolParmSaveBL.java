package com.sinosoft.lis.cbcheck;


import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import java.sql.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class BonusGrpPolParmSaveBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private LOBonusGrpPolParmSet mLOBonusGrpPolParmSet=new LOBonusGrpPolParmSet();
    private GlobalInput mGlobalInput =new GlobalInput();
    public BonusGrpPolParmSaveBL()
    {
    }
    public static void main(String[] args)
    {
        BonusGrpPolParmSaveBL bonusGrpPolParmSaveBL1 = new BonusGrpPolParmSaveBL();
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData,String cOperate)
    {
        if(getInputData(cInputData)==false)
            return false;

        for(int i=1;i<=mLOBonusGrpPolParmSet.size();i++)
        {
            LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema=mLOBonusGrpPolParmSet.get(i);
            tLOBonusGrpPolParmSchema.setMakeDate(PubFun.getCurrentDate());
            tLOBonusGrpPolParmSchema.setMakeTime(PubFun.getCurrentTime());
            tLOBonusGrpPolParmSchema.setModifyDate(PubFun.getCurrentDate());
            tLOBonusGrpPolParmSchema.setModifyTime(PubFun.getCurrentTime());
            tLOBonusGrpPolParmSchema.setOperator(mGlobalInput.Operator);
            LOBonusGrpPolParmDB tLOBonusGrpPolParmDB =new LOBonusGrpPolParmDB();
            tLOBonusGrpPolParmDB.setSchema(tLOBonusGrpPolParmSchema);
            if(tLOBonusGrpPolParmDB.insert()==false)
            {
                CError.buildErr(this,tLOBonusGrpPolParmDB.mErrors.getFirstError());
            }
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = ((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        mLOBonusGrpPolParmSet=((LOBonusGrpPolParmSet)cInputData.getObjectByObjectName("LOBonusGrpPolParmSet",0));

        if (mLOBonusGrpPolParmSet.size() == 0||mGlobalInput==null)
        {
            CError.buildErr(this, "没有传入数据!");
            return false;
        }
        return true;
    }

}