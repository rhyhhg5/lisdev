/**
 * created 2008-8-4
 * by LY
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LBMissionDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LDUWUserDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCUWSendTraceSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class UWQuestBackBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private LCContSchema mLCContSchema = new LCContSchema();

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mMissionID = null;

    public UWQuestBackBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mMissionID = (String) mTransferData.getValueByName("MissionID");
        if (mMissionID == null || mMissionID.equals(""))
        {
            buildError("getInputData", "工作流信息缺失。");
            return false;
        }

        String tContNo = (String) mTransferData.getValueByName("ContNo");
        mLCContSchema = getContInfo(tContNo);
        if (mLCContSchema == null)
        {
            buildError("getInputData", "未查询到该保单[" + tContNo + "]信息。");
            return false;
        }

        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        // 准备核保业务数据
        if (!prepareUW())
        {
            return false;
        }
        // ---------------------

        //准备核保轨迹表数据
        tTmpMap = prepareSendTrace(mLCContSchema.getContNo(), "3");
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // ---------------------

        //准备工作流表
        tTmpMap = prepareMission();
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        // ---------------------

        return true;
    }

    private boolean checkData()
    {
        // 检验核保师权限。
        if (!checkUWUserGrade())
            return false;

        return true;
    }

    /**
     * 获取保单信息。
     * @param cContNo 保单合同号
     * @return 返回保单信息，如未查到保单信息，则返回null。
     */
    private LCContSchema getContInfo(String cContNo)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(cContNo);
        if (!tLCContDB.getInfo())
        {
            return null;
        }
        return tLCContDB.getSchema();
    }

    /**
     * 校验核保师权限。
     * @return
     */
    private boolean checkUWUserGrade()
    {
        // 保单类型：1－个险；2－团险
        String tContTypeFlag = "1";
        String tUWUserCode = mGlobalInput.Operator;

        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setUserCode(tUWUserCode);

        String tUWType = "";
        if ("1".equals(tContTypeFlag))
        {
            tUWType = "01";
        }
        else if ("2".equals(tContTypeFlag))
        {
            tUWType = "02";
        }
        tLDUWUserDB.setUWType(tUWType);
        // ---------------------------------------

        if (!tLDUWUserDB.getInfo())
        {
            buildError("checkUWUserGrade", "该核保师[" + tUWUserCode + "]权限不足。");
            return false;
        }

        return true;
    }

    /**
     * 生成核保结论轨迹。
     * @return
     */
    private boolean prepareUW()
    {
        return true;
    }

    /**
     * 生成回退轨迹。
     * @return
     */
    private MMap prepareSendTrace(String cContNo, String cSendType)
    {
        MMap tMMap = new MMap();

        // 获取回退轨迹中最大流水号
        String tStrSql = " select Integer(Nvl(Max(lcuwst.UWNo), 0) + 1) "
                + " from LCUWSendTrace lcuwst " + " where lcuwst.OtherNo = '"
                + cContNo + "' " + " and lcuwst.OtherNoType = '" + cSendType
                + "' ";

        int tUWNo = Integer.parseInt((new ExeSQL().getOneValue(tStrSql)));
        // -------------------------------------

        String tOperator = mGlobalInput.Operator;
        String tManageCom = mGlobalInput.ManageCom;

        LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();

        tLCUWSendTraceSchema.setOtherNo(cContNo);
        tLCUWSendTraceSchema.setOtherNoType(cSendType);
        tLCUWSendTraceSchema.setUWNo(tUWNo);

        tLCUWSendTraceSchema.setSendFlag("4");
        tLCUWSendTraceSchema.setSendType("4");

        tLCUWSendTraceSchema.setUWCode(tOperator);
        tLCUWSendTraceSchema.setUWIdea("问题件回退");

        tLCUWSendTraceSchema.setUpUserCode(tOperator);
        tLCUWSendTraceSchema.setDownUWCode(tOperator);

        tLCUWSendTraceSchema.setOperator(tOperator);
        tLCUWSendTraceSchema.setManageCom(tManageCom);
        tLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());

        tMMap.put(tLCUWSendTraceSchema, SysConst.INSERT);

        return tMMap;
    }

    private MMap prepareMission()
    {
        MMap tMMap = new MMap();

        LWMissionSet tLWMissionSet = getPivotalMission();
        if (tLWMissionSet == null)
        {
            return null;
        }

        MMap tTmpMap = new MMap();
        // 回退新单复核工作流。
        tTmpMap = backApproveMission();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // ---------------------

        // 删除人工核保工作流。
        tTmpMap = delUWMission(tLWMissionSet);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        // ---------------------

        return tMMap;
    }

    private LWMissionSet getPivotalMission()
    {
        LWMissionDB tLWMissionDB = new LWMissionDB();
        //tLWMissionDB.setActivityID("0000001100");
        //tLWMissionDB.setMissionProp2(mLCContSchema.getContNo());
        //LWMissionSet tLWMissionSet = tLWMissionDB.query();
        String sql = "select * from LWMission where MissionProp2 = '" 
            + mLCContSchema.getContNo() 
            + "' and ActivityID in ('0000001100', '0000001160')";
        System.out.println(sql);

        LWMissionSet tLWMissionSet = tLWMissionDB.executeQuery(sql);
        if (tLWMissionSet == null || tLWMissionSet.size() != 1)
        {
            buildError("getPivotalMission", "核保工作流节点不存在或出现冗余。");
            return null;
        }

        return tLWMissionSet;
    }

    private MMap backApproveMission()
    {
        MMap tTmpMap = new MMap();

        String tStrSql = "select * from LBMission where ActivityID = '0000001001' "
                + " and MissionID='"
                + mMissionID
                + "' and MissionProp1 = '"
                + mLCContSchema.getContNo()
                + "' "
                + " order by makedate desc, maketime desc ";

        LBMissionDB tLBMissionDB = new LBMissionDB();
        LBMissionSet tLBMissionSet = new LBMissionSet();
        tLBMissionSet = tLBMissionDB.executeQuery(tStrSql);

        if (tLBMissionSet == null || tLBMissionSet.size() <= 0)
        {
            buildError("backApproveMission", "复核工作流节点历史轨迹不存在。");
            return null;
        }

        String tOperator = mGlobalInput.Operator;

        LBMissionSchema tLBMissionSchema = tLBMissionSet.get(1).getSchema();

        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        new Reflections().transFields(tLWMissionSchema, tLBMissionSchema);

        tLWMissionSchema.setActivityStatus("4");
        tLWMissionSchema.setMissionProp20("Y");

        tLWMissionSchema.setDefaultOperator(null);
        tLWMissionSchema.setLastOperator(tOperator);
        tLWMissionSchema.setCreateOperator(tOperator);
        tLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
        tLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
        tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
        tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());

        tTmpMap.put(tLWMissionSchema, SysConst.INSERT);

        return tTmpMap;
    }

    private MMap delUWMission(LWMissionSet cLWMissionSet)
    {
        try
        {
            MMap tTmpMap = new MMap();

            // 将核保工作流备份到B表。
            LBMissionSet tLBMissionSet = new LBMissionSet();
            for (int i = 1; i <= cLWMissionSet.size(); i++)
            {
                String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

                LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                Reflections tReflections = new Reflections();
                tReflections
                        .transFields(tLBMissionSchema, cLWMissionSet.get(i));
                tLBMissionSchema.setSerialNo(tSerielNo);

                tLBMissionSet.add(tLBMissionSchema);
            }

            tTmpMap.put(tLBMissionSet, SysConst.INSERT);
            // --------------------------------------------

            // 清除核保工作流节点。
            tTmpMap.put(cLWMissionSet, SysConst.DELETE);
            // --------------------------------------------

            return tTmpMap;
        }
        catch (Exception e)
        {
            buildError("delUWMission", e.getMessage());
            return null;
        }
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWQuestBackBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
