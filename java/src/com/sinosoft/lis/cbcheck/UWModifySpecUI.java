package com.sinosoft.lis.cbcheck;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 修改或新增核保特约</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class UWModifySpecUI {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public UWModifySpecUI() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate)	{
    // 数据操作字符串拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    System.out.println("---UWModifySpec BL BEGIN---");
    UWModifySpecBL tUWModifySpecBL = new UWModifySpecBL();

    if(tUWModifySpecBL.submitData(cInputData, cOperate) == false)	{
      // @@错误处理
      this.mErrors.copyAllErrors(tUWModifySpecBL.mErrors);
      mResult.clear();
      mResult.add(mErrors.getFirstError());
      return false;
    }	else {
      mResult = tUWModifySpecBL.getResult();
    }
    System.out.println("---UWModifySpec BL END---");

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args) {
    UWModifySpecUI UWModifySpecUI1 = new UWModifySpecUI();

    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.Operator = "001";
    tGlobalInput.ComCode = "86110000";

    TransferData transferData1 = new TransferData();
    transferData1.setNameAndValue("name", "value");

    LCSpecSchema tLCSpecSchema = new LCSpecSchema();
    tLCSpecSchema.setPolNo("12345678901");
    tLCSpecSchema.setSpecContent("abcdefg");
    LCSpecSet inLCSpecSet = new LCSpecSet();
    inLCSpecSet.add(tLCSpecSchema);

    VData tVData = new VData();
    tVData.add(tGlobalInput);
    tVData.add(transferData1);
    tVData.add(inLCSpecSet);

    if (!UWModifySpecUI1.submitData(tVData, "INSERT")) {
      VData rVData = UWModifySpecUI1.getResult();
      System.out.println("Submit Failed! " + (String)rVData.get(0));
    }
    else {
      System.out.println("Submit Succed!");
    }

//    if (!UWModifySpecUI1.submitData(tVData, "QUERY")) {
//      VData rVData = UWModifySpecUI1.getResult();
//      System.out.println("Submit Failed! " + (String)rVData.get(0));
//    }
//    else {
//      VData rVData = UWModifySpecUI1.getResult();
//      String result = ((LCSpecSet)rVData.get(0)).encode();
//      System.out.println("Submit Succed!");
//      System.out.println(result);
//    }
  }
}
