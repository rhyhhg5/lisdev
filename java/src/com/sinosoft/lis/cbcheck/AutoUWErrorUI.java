package com.sinosoft.lis.cbcheck;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author zhangxing
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;


public class AutoUWErrorUI {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  private VData mResult = new VData();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private TransferData mTransferData = new TransferData();

  public AutoUWErrorUI() {
  }

  /**
       传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    // 得到外部传入的数据，将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }

    // 进行业务处理
    if (!dealData()) {
      return false;
    }

    // 准备传往后台的数据
    VData vData = new VData();

    if (!prepareOutputData(vData)) {
      return false;
    }

    AutoUWErrorBL tAutoUWErrorBL = new AutoUWErrorBL();
    System.out.println("Start AutoUWErrorUI  Submit ...");

    if (!tAutoUWErrorBL.submitData(vData, cOperate)) {
      if (tAutoUWErrorBL.mErrors.needDealError()) {
        mErrors.copyAllErrors(tAutoUWErrorBL.mErrors);
        return false;
      }
      else {
        buildError("submitData", "AutoUWErrorUI发生错误，但是没有提供详细的出错信息");
        return false;
      }
    }
    else {
      return true;
    }
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData(VData vData) {
    try {
      vData.clear();
      vData.add(mGlobalInput);
      vData.add(mTransferData);

    }
    catch (Exception ex) {
      ex.printStackTrace();
      buildError("prepareOutputData", "发生异常");
      return false;
    }
    return true;
  }

  /**
   * 根据前面的输入数据，进行UI逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    //从输入数据中得到所有对象
    //获得全局公共数据
    mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mTransferData = (TransferData) cInputData.getObjectByObjectName(
        "TransferData", 0);

    if (mGlobalInput == null) {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCContDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "AutoUWErrorUI";
      tError.functionName = "getInputData";
      tError.errorMessage = "前台传输全局公共数据失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  public VData getResult() {
    return this.mResult;
  }

  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();

    cError.moduleName = "AutoUWErrorUI";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  public static void main(String[] args) {
    VData tVData = new VData();

    GlobalInput tG = new GlobalInput();

    tG.Operator = "group";
    tG.ManageCom = "86";

    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo", "00089970401");
    tVData.add(tTransferData);
    tVData.add(tG);

    AutoUWErrorUI tAutoUWErrorUI = new AutoUWErrorUI();
    tAutoUWErrorUI.submitData(tVData, "");

  }
}
