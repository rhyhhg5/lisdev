package com.sinosoft.lis.cbcheck;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContSubDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSubSchema;
import com.sinosoft.lis.vschema.LCContSubSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * <p>
 * Title: Web业务系统承保个人人工核保部分
 * </p>
 * <p>
 * Description: 逻辑处理类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author zqt
 * @version 1.0
 */
public class RecordFlagBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData;

	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	private TransferData mTransferData = new TransferData();

	private GlobalInput mGlobalInput = new GlobalInput();// 公共信息

	private String mPrtNo = "";

	private String mContNo = "";

	public RecordFlagBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		System.out
				.println("==============start RecordFlagBL===================");
		// 将操作数据拷贝到本类中
		mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		System.out.println("==============end RecordFlagBL===================");
		return true;
	}

	private boolean dealData() {

		LCContDB tLCCont = new LCContDB();
		tLCCont.setContNo(mContNo);
		if (!tLCCont.getInfo()) {
			return false;
		}
		// 复核通过
		if (!"9".equals(tLCCont.getApproveFlag())) {
			System.out.println("未复核通过，不需双录标识！！！");
			//return false;
		}

		LCContSubDB tLCContSub = new LCContSubDB();
		tLCContSub.setPrtNo(mPrtNo);
		LCContSubSet mLCContSubSet = new LCContSubSet();
		LCContSubSchema mLCContSubSchema = new LCContSubSchema();
		if (!tLCContSub.getInfo()) {
			mLCContSubSchema.setMakeDate(theCurrentDate);
			mLCContSubSchema.setMakeTime(theCurrentTime);
			mLCContSubSchema.setModifyDate(theCurrentDate);
			mLCContSubSchema.setModifyTime(theCurrentTime);
			mLCContSubSchema.setOperator(mGlobalInput.Operator);
		} else {
			mLCContSubSchema = tLCContSub.getSchema();
			mLCContSubSchema.setModifyDate(theCurrentDate);
			mLCContSubSchema.setModifyTime(theCurrentTime);
		}

		// 销售渠道
		String tsalechnl = tLCCont.getSaleChnl();
		// 中介机构
		String tagentcom = tLCCont.getAgentCom();

		String tAppntBirthday = tLCCont.getAppntBirthday();
		String tPolApplyDate = tLCCont.getPolApplyDate();
		int age = PubFun.getInsuredAppAge(tPolApplyDate, tAppntBirthday);
		System.out.println("投保人年龄：" + age);
		String riskflag;
		if(tLCCont.getManageCom().indexOf("8632")==0){
			riskflag = "select 1 from lcpol where riskcode in (select riskcode from lmriskapp where (riskperiod='L' or riskperiod='M')) "
					+ "and prtno='" + mPrtNo + "' ";
		}else{
			riskflag = "select 1 from lcpol where riskcode in (select riskcode from lmriskapp where riskperiod='L') "
					+ "and prtno='" + mPrtNo + "' ";
		}
		
		SSRS tSSRS = new ExeSQL().execSQL(riskflag);

		if (tSSRS.getMaxRow() > 0) {
			//zxs 
			if(tLCCont.getManageCom().indexOf("8632")==0){
				mLCContSubSchema.setRecordFlag("1");
			}else{
			// 非中介渠道，投保人年龄超过60
			if ("01".equals(tsalechnl) || "02".equals(tsalechnl)
					|| "13".equals(tsalechnl) || "14".equals(tsalechnl)
					|| "17".equals(tsalechnl) || "18".equals(tsalechnl)
					|| "06".equals(tsalechnl) || "07".equals(tsalechnl)
					|| "16".equals(tsalechnl)) {
				if (age >= 60) {
					mLCContSubSchema.setRecordFlag("1");
				} else {
					mLCContSubSchema.setRecordFlag("0");
				}
			}// 长期险下，个险中介、团险中介、互动中介下，中介机构为兼业代理的保单为双录件,财险、寿险
			else if ("03".equals(tsalechnl) || "10".equals(tsalechnl)
					|| "15".equals(tsalechnl)) {
				String agentcom = "select 1 from lacom where actype in ('02','06','07') "
						+ "and agentcom='" + tagentcom + "' ";
				SSRS tSSRS1 = new ExeSQL().execSQL(agentcom);
				if (tSSRS1.getMaxRow() > 0) {
					mLCContSubSchema.setRecordFlag("1");
				} else {
					// 中介渠道下，中介机构非兼业代理且年龄超过60岁的为双录件
					if (age >= 60) {
						mLCContSubSchema.setRecordFlag("1");
					} else {
						mLCContSubSchema.setRecordFlag("0");
					}
				}
			}
			// 银行代理渠道
			else if ("04".equals(tsalechnl)) {
				String magentcom = "select 1 from lacom where actype='01' "
								+ "and agentcom='" + tagentcom + "' ";
				SSRS tSSRS1 = new ExeSQL().execSQL(magentcom);
				if (tSSRS1.getMaxRow() > 0) {
					mLCContSubSchema.setRecordFlag("1");
				} else {
					// 银行代理渠道下，中介机构非兼业代理且年龄超过60岁的为双录件
					if (age >= 60) {
						mLCContSubSchema.setRecordFlag("1");
					} else {
						mLCContSubSchema.setRecordFlag("0");
					}
				}
			}
			// 除以上条件外的保单都是非双录件
			else {
				mLCContSubSchema.setRecordFlag("0");
			}
			}
		}
		// 非长期险都不是双录件
		else {
			mLCContSubSchema.setRecordFlag("0");

		}

		mLCContSubSet.add(mLCContSubSchema);
		System.out.println("双录标识==================="
				+ mLCContSubSchema.getRecordFlag());
		MMap aMMap = new MMap();
		aMMap.put(mLCContSubSet, "DELETE&INSERT");

		PubSubmit ps = new PubSubmit();
		VData sd = new VData();
		sd.add(aMMap);
		if (!ps.submitData(sd, null)) {
			CError.buildErr(this, "保存错误");
			return false;
		}
		String agentCom = new ExeSQL().getOneValue("select agentcom from lccont where prtno = '"+mLCContSubSchema.getPrtNo()+"'");
		if(tLCCont.getPrtNo().indexOf("PD")!=0){
			if(agentCom.indexOf("PY028")!=0){
				if(mLCContSubSchema.getRecordFlag().equals("1")){
					try {
						String check = checkResult(mLCContSubSchema.getPrtNo());
						System.out.println("check="+check);
						if(check.equals("0")){
							CError.buildErr(this, "此单应进行双录，请补充并上传双录视频文件！");
							return false;
						}else if(!check.equals("1")){
							CError.buildErr(this, "此单应双录质检未通过，请完成双录质检或处理质检问题！");
							return false;
						}
						//。
					} catch (Exception e) {
						e.printStackTrace();
						try {
							String check = checkResult(mLCContSubSchema.getPrtNo());
							System.out.println("check="+check);
							if(check.equals("0")){
								CError.buildErr(this, "此单应进行双录，请补充并上传双录视频文件！");
								return false;
							}else if(!check.equals("1")){
								CError.buildErr(this, "此单应双录质检未通过，请完成双录质检或处理质检问题！");
								return false;
							}} catch (Exception e1) {
								e1.printStackTrace();
								CError.buildErr(this, "与保信通讯异常！请重新尝试");
								return false;
							}
					}  
				}
			}
		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {

		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mContNo = (String) mTransferData.getValueByName("ContNo");
		if (mPrtNo == "" || mPrtNo == null) {
			return false;
		}
		if (mContNo == "" || mContNo == null) {
			return false;
		}
		return true;

	}

	public static void main(String[] args) {
		VData tVData = new VData();
		TransferData ttTransferData = new TransferData();
		ttTransferData.setNameAndValue("ContNo", "13100871543");
		ttTransferData.setNameAndValue("PrtNo", "1130002477555");
		tVData.add(ttTransferData);
		RecordFlagBL tRecordFlagBL = new RecordFlagBL();
		tRecordFlagBL.submitData(tVData, "");
	}
	
	public String checkResult(String prtno) throws Exception{
		HttpClient client = new HttpClient();
		String address = new ExeSQL().getOneValue("select sysvarvalue from LDSYSVAR  where Sysvar='DoubleResult'");
		String url = address+"?type=2&value="+prtno+"";
		PostMethod method = new PostMethod(url);
		//"http://10.136.4.130:8180/RecordDev/DemoAction?type=2&value=PD21000009426"
		method.addRequestHeader(new Header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8") );    
		int result = client.executeMethod(method);
		System.out.println("result="+result);
		if (HttpStatus.SC_OK == result) {
			String responseXml = method.getResponseBodyAsString();
			System.out.println("响应报文：" + responseXml);
			if("[]".equals(responseXml)){
				System.out.println("该保单双录质检未通过！");
				return "0";
			}else{
				JSONArray resultArray=JSONArray.fromObject(responseXml);//转json对象
				JSONObject jsonObject = resultArray.getJSONObject(0);
				System.out.println(jsonObject.get("auditingresult"));
				String jsonResult = (String) jsonObject.get("auditingresult").toString();
				return jsonResult;
			}
		} else {	
			throw new Exception("通信异常，返回码为：" + result);
		}

	}
		
}
