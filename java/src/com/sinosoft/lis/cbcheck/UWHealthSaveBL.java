package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPENoticeSchema;
import com.sinosoft.lis.vschema.LCPENoticeItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.SysConst;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.f1print.PrintManagerBL;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
public class UWHealthSaveBL
{
    /** 传输到后台处理的,最后递交Map */
    private MMap map = new MMap();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGI = null;

    private LCPENoticeSchema mLCPENoticeSchema = null;;
    private LCPENoticeItemSet mLCPENoticeItemSet = null;
    private String mMissionID = null;

    //统一更新日期，时间
    private LCContSchema mLCContSchema = null;

    public UWHealthSaveBL()
    {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("into UWHealthSaveBL...");

        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(data, "INSERT"))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("UWHealthSaveBL finished...");
        return true;
    }

    /**
     * getInputData
     * 得到传入的变量，为进行数据处理作准备
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        mLCPENoticeSchema = (LCPENoticeSchema) data
                            .getObjectByObjectName("LCPENoticeSchema", 0);
        mLCPENoticeItemSet = (LCPENoticeItemSet) data
                             .getObjectByObjectName("LCPENoticeItemSet", 0);
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tTransferData = (TransferData) data
                                     .getObjectByObjectName("TransferData", 0);
        mMissionID = (String) tTransferData.getValueByName("MissionID");

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        System.out.println("into UWHealthCancelBL.checkData()...");
        if(this.mGI == null)
        {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }

        if(mLCPENoticeSchema == null || mLCPENoticeItemSet == null)
        {
            CError tError = new CError();
            tError.moduleName = "UWHealthSaveBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有读取到体检信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        //校验保单信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCPENoticeSchema.getContNo());
        if (!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "UWHealthCancelBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单" + mLCPENoticeSchema.getContNo()
                                  + "信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        String prtSeq = getPrtSeq();
        if(prtSeq == null)
        {
            return false;
        }

        if(!createPENotice(prtSeq))
        {
            return false;
        }

        if(!createPrtManager(prtSeq))
        {
            return false;
        }

        return true;
    }

    /**
     * 生成打印信息
     * @return boolean
     */
    private boolean createPENotice(String prtSeq)
    {
        String customerName = getName();
        if(customerName == null)
        {
            return false;
        }


        String agentName = getAgentName();
        if(agentName == null)
        {
            return false;
        }

        mLCPENoticeSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        mLCPENoticeSchema.setContNo(mLCContSchema.getContNo());
        mLCPENoticeSchema.setProposalContNo(mLCContSchema.getProposalContNo());
        mLCPENoticeSchema.setName(customerName);
        mLCPENoticeSchema.setPrtSeq(prtSeq);
        mLCPENoticeSchema.setPrintFlag("0");
        mLCPENoticeSchema.setAppName(mLCContSchema.getAppntName());
        mLCPENoticeSchema.setAgentCode(mLCContSchema.getAgentCode());
        mLCPENoticeSchema.setAgentName(agentName);
        mLCPENoticeSchema.setManageCom(mLCContSchema.getManageCom());
        mLCPENoticeSchema.setOperator(mGI.Operator); //操作员
        mLCPENoticeSchema.setMakeDate(PubFun.getCurrentDate());
        mLCPENoticeSchema.setMakeTime(PubFun.getCurrentTime());
        mLCPENoticeSchema.setModifyDate(PubFun.getCurrentDate());
        mLCPENoticeSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLCPENoticeSchema, SysConst.INSERT);

        //准备检资料项目信息
        for(int i = 1; i <= mLCPENoticeItemSet.size(); i++)
        {

            mLCPENoticeItemSet.get(i).setGrpContNo(mLCContSchema.getGrpContNo());
            mLCPENoticeItemSet.get(i).setContNo(mLCContSchema.getContNo());
            mLCPENoticeItemSet.get(i).setProposalContNo(mLCContSchema.
                getProposalContNo());
            mLCPENoticeItemSet.get(i).setPrtSeq(prtSeq);
            mLCPENoticeItemSet.get(i).setModifyDate(PubFun.getCurrentDate()); //当前值
            mLCPENoticeItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
        }
        map.put(mLCPENoticeItemSet, SysConst.INSERT);

        return true;
    }

    /**
     * 生成打印管理信息
     * @return boolean
     */
    private boolean createPrtManager(String prtSeq)
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("URGEInterval");

        if(tLDSysVarDB.getInfo() == false)
        {
            CError tError = new CError();
            tError.moduleName = "UWSendPrintBL";
            tError.functionName = "prepareURGE";
            tError.errorMessage = "没有描述催发间隔!";
            this.mErrors.addOneError(tError);
            return false;
        }
        FDate tFDate = new FDate();
        int tInterval = Integer.parseInt(tLDSysVarDB.getSysVarValue());
        System.out.println(tInterval);

        Date tDate = PubFun.calDate(tFDate.getDate(PubFun.getCurrentDate()),
                                    tInterval, "D", null);
        System.out.println(tDate); //取预计催办日期

        //准备打印管理表数据
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(prtSeq);
        tLOPRTManagerSchema.setOtherNo(mLCPENoticeSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //保单号
        tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PE); //体检
        tLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(mGI.ComCode);
        tLOPRTManagerSchema.setReqOperator(mGI.Operator);
        tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT); //前台打印
        tLOPRTManagerSchema.setStateFlag("0");
        tLOPRTManagerSchema.setPatchFlag("0");
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTManagerSchema.setStandbyFlag1(mLCPENoticeSchema.getCustomerNo()); //被保险人编码
        tLOPRTManagerSchema.setStandbyFlag3(mMissionID);
        tLOPRTManagerSchema.setOldPrtSeq(prtSeq);
        tLOPRTManagerSchema.setForMakeDate(tDate);
        map.put(tLOPRTManagerSchema, SysConst.INSERT);

        return true;
    }

    /**
     * getAgentName
     *得到代理人名字
     * @return String
     */
    private String getAgentName()
    {
        //取代理人姓名
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mLCContSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWHealthCancelBL";
            tError.functionName = "getAgentName";
            tError.errorMessage = "取代理人姓名失败!";
            this.mErrors.addOneError(tError);
            return null;
        }

        return tLAAgentDB.getName();
    }

    /**
     * getPrtSeq
     *生成打印通知书号码
     * @return String
     */
    private String getPrtSeq()
    {
        String strNoLimit = mLCContSchema.getPrtNo();
        String prtSeq = PubFun1.CreateMaxNo("PPAYNOTICENO", strNoLimit);
        if(prtSeq == null || prtSeq.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UWHealthCancelBL";
            tError.functionName = "getPrtSeq";
            tError.errorMessage = "生成通知书号码失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return prtSeq;
    }

    /**
     * 得到客户姓名
     * @return String
     */
    private String getName()
    {
        //取体检人姓名
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tLDPersonDB.setCustomerNo(mLCPENoticeSchema.getCustomerNo());
        if (!tLDPersonDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAutoHealthAfterInitService";
            tError.functionName = "prepareHealth";
            tError.errorMessage = "取被体检客户姓名失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLDPersonDB.getName();
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWHealthCancelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
