/*
 * <p>ClassName: LCGrpRReportItemBL </p>
 * <p>Description: LCGrpRReportItemBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-08-4
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LCGrpRReportItemBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tOperate;
    private String mPrtSeq;
    private String mPolNo = "";
    /** 业务处理相关变量 */
    private LCGrpRReportItemSchema mLCGrpRReportItemSchema = new
            LCGrpRReportItemSchema();
    private LCGrpRReportItemSet mLCGrpRReportItemSet = new LCGrpRReportItemSet();
    private LCGrpRReportItemSchema tLCGrpRReportItemSchema = new
            LCGrpRReportItemSchema();
    private LCGrpRReportSchema mLCGrpRReportSchema = new LCGrpRReportSchema();
    private LCGrpRReportSet mLCGrpRReportSet = new LCGrpRReportSet();
    private LCGrpRReportSchema tLCGrpRReportSchema = new LCGrpRReportSchema();
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    public LCGrpRReportItemBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(mInputData))
            return false;
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpRReportItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LCGrpRReportItemBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LCGrpRReportItemBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("---commitData---");
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema = (LCGrpContSchema) mLCGrpContSet.get(1);
        mPolNo = tLCGrpContSchema.getGrpContNo();
        System.out.print("mPolNo=" + mPolNo);

        tLCGrpContDB.setGrpContNo(mPolNo);
        if (!tLCGrpContDB.getInfo()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAutoHealthBL";
            tError.functionName = "prepareHealth";
            tError.errorMessage = "取集体合同信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCGrpContSchema = tLCGrpContDB.getSchema();

        String SeqNo = PubFun1.CreateMaxNo("GRPAYNOTICENO",
                                           mLCGrpContSchema.getPrtNo());
        mPrtSeq = SeqNo;

        System.out.println("mPrtSeq=" + mPrtSeq);
        if (this.mOperate.equals("INSERT||MAIN")) {
            System.out.println("In dealDate mLCGrpRReportItemSet ：" +
                               mLCGrpRReportItemSet.encode());
            if (mLCGrpRReportItemSet != null &&
                mLCGrpRReportItemSet.size() > 0) {
                for (int i = 1; i <= mLCGrpRReportItemSet.size(); i++) {
                    mLCGrpRReportItemSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLCGrpRReportItemSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLCGrpRReportItemSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLCGrpRReportItemSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLCGrpRReportItemSet.get(i).setOperator(tOperate); //操作员
                    mLCGrpRReportItemSet.get(i).setGrpContNo(mLCGrpContSchema.
                            getGrpContNo());
                    mLCGrpRReportItemSet.get(i).setPrtSeq(mPrtSeq);
                }
            }
           // if (mLCGrpRReportSet != null &&
              //  mLCGrpRReportSet.size() > 0) {
              //  for (int i = 1; i <= mLCGrpRReportSet.size(); i++) {
                    mLCGrpRReportSchema.setMakeDate(PubFun.getCurrentDate());
                    mLCGrpRReportSchema.setMakeTime(PubFun.getCurrentTime());
                    mLCGrpRReportSchema.setModifyDate(PubFun.getCurrentDate());
                    mLCGrpRReportSchema.setModifyTime(PubFun.getCurrentTime());
                   mLCGrpRReportSchema.setOperator(tOperate); //操作员
                   mLCGrpRReportSchema.setPrtSeq(mPrtSeq);
               // }
          //  }

//            mPrtSeq = mLCGrpRReportItemSchema.getPrtSeq();
//            LCGrpRReportItemDB tLCGrpRReportItemDB = new LCGrpRReportItemDB();
//            tLCGrpRReportItemDB.setPrtSeq(mPrtSeq);
//            tLCGrpRReportItemDB.getInfo();
//            tLCGrpRReportItemSchema = tLCGrpRReportItemDB.getSchema();

//            mPrtSeq = mLCGrpRReportSchema.getPrtSeq();
//            LCGrpRReportDB tLCGrpRReportDB = new LCGrpRReportDB();
//            tLCGrpRReportDB.setPrtSeq(mPrtSeq);
//            tLCGrpRReportDB.getInfo();
//            tLCGrpRReportSchema = tLCGrpRReportDB.getSchema();

            map.put("delete from LCGrpRReportItem where PrtSeq='" +
                    mPrtSeq + "' ", "DELETE");
            map.put(mLCGrpRReportItemSet, "INSERT");
            map.put("delete from LCGrpRReport where PrtSeq='" +
                    mPrtSeq + "'", "DELETE");
            map.put(mLCGrpRReportSchema, "INSERT");
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLCGrpRReportItemSet.set((LCGrpRReportItemSet) cInputData.
                                      getObjectByObjectName(
                                              "LCGrpRReportItemSet", 0));
        this.mLCGrpRReportSet.set((LCGrpRReportSet) cInputData.
                                  getObjectByObjectName(
                                          "LCGrpRReportSet", 0));
            this.mLCGrpRReportSchema.setSchema((LCGrpRReportSchema) cInputData.
                                 getObjectByObjectName(
                                         "LCGrpRReportSchema", 0));

        System.out.println(mLCGrpRReportItemSet.encode());
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mLCGrpContSet.set((LCGrpContSet) cInputData.getObjectByObjectName(
                "LCGrpContSet", 0));
        GlobalInput tGlobalInput = new GlobalInput();

        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        tOperate = tGlobalInput.Operator;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LCGrpRReportItemDB tLCGrpRReportItemDB = new LCGrpRReportItemDB();
        tLCGrpRReportItemDB.setSchema(this.mLCGrpRReportItemSchema);
        LCGrpRReportDB tLCGrpRReportDB = new LCGrpRReportDB();
        tLCGrpRReportDB.setSchema(this.mLCGrpRReportSchema);
        //如果有需要处理的错误，则返回
        if (tLCGrpRReportItemDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpRReportItemDB.mErrors);
            this.mErrors.copyAllErrors(tLCGrpRReportDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpRReportItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLCGrpRReportItemSet);
            this.mInputData.add(this.mLCGrpRReportSchema);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLCGrpRReportItemSet);
            mResult.add(this.mLCGrpRReportSchema);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpRReportItemBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
