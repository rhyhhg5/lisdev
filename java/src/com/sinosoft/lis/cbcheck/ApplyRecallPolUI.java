package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author
 * @version 1.0
 */

public class ApplyRecallPolUI {

    private VData mInputData;
    private GlobalInput tG = new GlobalInput();
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    public ApplyRecallPolUI() {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        ApplyRecallPolBL tApplyRecallPolBL = new ApplyRecallPolBL();
        System.out.println("Start tApplyRecallPolBL Submit...");
        tApplyRecallPolBL.submitData(mInputData, cOperate);

        System.out.println("End tApplyRecallPolBL Submit...");

        //如果有需要处理的错误，则返回
        if (tApplyRecallPolBL.mErrors.needDealError()) {
            mErrors.copyAllErrors(tApplyRecallPolBL.mErrors);
            return false;
        }
        mResult.clear();
        mResult = tApplyRecallPolBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "Yang";

        LCApplyRecallPolSchema tLCApplyRecallPolSchema = new
                LCApplyRecallPolSchema();
        tLCApplyRecallPolSchema.setRemark("数据维护撤销");
        tLCApplyRecallPolSchema.setPrtNo("16000002135");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLCApplyRecallPolSchema);
        tVData.add(tG);

        // 数据传输
        ApplyRecallPolUI tApplyRecallPolUI = new ApplyRecallPolUI();
        if (tApplyRecallPolUI.submitData(tVData, "") == false) {
            System.out.println(tApplyRecallPolUI.mErrors.getError(0).
                               errorMessage);
        }

    }

}
