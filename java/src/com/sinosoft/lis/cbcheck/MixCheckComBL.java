package com.sinosoft.lis.cbcheck;

import com.cbsws.obj.RspSaleInfo;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.sys.MixedSalesAgentQueryUI;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class MixCheckComBL {
	private VData vmDate = new VData();
	private TransferData mTransferData = new TransferData();
	private String mContNo = "";
	private String mtPrtNo = "";
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	public RspSaleInfo rspSaleInfo = new RspSaleInfo();// 该类中的属性都是调用第三方接口返回来的

	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("进入MixCheckComBL处理类===========");
		vmDate = (VData) cInputData.clone();

		// 数据过滤器
		if (!getInputData(vmDate, cOperate)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		// 本类备份容器清空
		vmDate = null;
		return true;
	}

	private boolean dealData() {

		LCContDB tLCContDB = new LCContDB();
		tLCContDB.setContNo(mContNo);
		if(!tLCContDB.getInfo()){
			CError tError = new CError();
			tError.moduleName = "MixCheckComBL";
			tError.functionName = "dealData";
			tError.errorMessage = "保单信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
 
        String tGrpAgentIDNo = tLCContDB.getGrpAgentIDNo();
        String tGrpAgentCom = tLCContDB.getGrpAgentCom();
        String tGrpAgentCode = tLCContDB.getGrpAgentCode();
        String tGrpAgentName = tLCContDB.getGrpAgentName();
        String tManageCom = tLCContDB.getManageCom();
		
        if(!"".equals(tGrpAgentCode)&& null!=tGrpAgentCode){
        	MixedSalesAgentQueryUI mixedSalesAgentQueryUI = new MixedSalesAgentQueryUI();
    		try {
    			rspSaleInfo = mixedSalesAgentQueryUI.getSaleInfo(tGrpAgentCode,
    					tManageCom);// 调用处理类中的方法(调用第三方接口返回来的结果)
    		} catch (Exception ex) {
    			ex.getStackTrace();
    		}
    		
    		String type = rspSaleInfo.getMESSAGETYPE();
    		String typeInfo = rspSaleInfo.getERRDESC();
    		if("01".equals(type)){
    			CError tError = new CError();
    			tError.moduleName = "MixCheckComBL";
    			tError.functionName = "getInputData";
    			tError.errorMessage = typeInfo;
    			this.mErrors.addOneError(tError);
    			return false;
    		}
    		String code = rspSaleInfo.getUNI_SALES_COD();// 对方业务员代码
    		String name = rspSaleInfo.getSALES_NAM();// 对方业务员姓名
    		String com = rspSaleInfo.getMAN_ORG_COD();// 对方机构代码
    		String idno = rspSaleInfo.getID_NO();// 对方业务员证件号码
    		if(!idno.equals(tGrpAgentIDNo)||!com.equals(tGrpAgentCom)||!name.equals(tGrpAgentName)||!code.equals(tGrpAgentCode)){
    			CError tError = new CError();
    			tError.moduleName = "MixCheckComBL";
    			tError.functionName = "getInputData";
    			tError.errorMessage = "交叉业务员信息与集团不相符！";
    			this.mErrors.addOneError(tError);
    			return false;
    		}
        }
		
		return true;
	}

	private boolean getInputData(VData cInputData, String cOperate) {
		// 从VData容器中取出对象TransferData
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);

		// 分别对TransferData中数据进行过滤
		mContNo = (String) mTransferData.getValueByName("ContNo");
		if (mContNo == null || "".equals(mContNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "MixCheckComBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "保单号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		mtPrtNo = (String) mTransferData.getValueByName("tPrtNo");
		if (mtPrtNo == null || "".equals(mtPrtNo)) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "MixCheckComBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "印刷号获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}
}
