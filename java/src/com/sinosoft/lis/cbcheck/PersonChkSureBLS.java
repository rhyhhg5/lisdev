package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统客户查重功能部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class PersonChkSureBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();

	//
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public PersonChkSureBLS() {}

	public static void main(String[] args)
	{
	}

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	System.out.println("Start PersonChkSureBLS Submit...");
		if (!this.saveData())
			return false;
	System.out.println("End PersonChkSureBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{
          LCContSet mLCContSet = (LCContSet)mInputData.getObjectByObjectName("LCContSet",0);
          LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
          LCPremSet mLCPremSet = (LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0);
          LCCustomerImpartSet mLCCustomerImpartSet = (LCCustomerImpartSet)mInputData.getObjectByObjectName("LCCustomerImpartSet",0);
          LCAppntSet mLCAppntSet = (LCAppntSet)mInputData.getObjectByObjectName("LCAppntSet",0);
          LCUWErrorSet mLCUWErrorSet = (LCUWErrorSet)mInputData.getObjectByObjectName("LCUWErrorSet",0);
          LCUWMasterSet mLCUWMasterSet = (LCUWMasterSet)mInputData.getObjectByObjectName("LCUWMasterSet",0);
          LCGetSet mLCGetSet = (LCGetSet)mInputData.getObjectByObjectName("LCGetSet",0);
          LCInsuredSet mLCInsuredSet = (LCInsuredSet)mInputData.getObjectByObjectName("LCInsuredSet",0);
          LCInsureAccSet mLCInsureAccSet = (LCInsureAccSet)mInputData.getObjectByObjectName("LCInsureAccSet",0);
          LCPENoticeSet mLCPENoticeSet = (LCPENoticeSet)mInputData.getObjectByObjectName("LCPENoticeSet",0);

          Connection conn = DBConnPool.getConnection();
          try
          {

                    if (conn==null)
                    {
                      // @@错误处理
                      CError tError = new CError();
                      tError.moduleName = "PersonChkSureBLS";
                      tError.functionName = "saveData";
                      tError.errorMessage = "数据库连接失败!";
                      this.mErrors .addOneError(tError) ;
                      return false;
                    }

                    conn.setAutoCommit(false);

                    // 修改部分
                    //保单表
                    if(mLCContSet.size()>0)
                    {
                      for (int i = 1;i <= mLCContSet.size();i++)
                      {
                        LCContSchema tLCContSchema = new LCContSchema();
                        tLCContSchema = mLCContSet.get(i);

                        LCContDB tLCContDB = new LCContDB(conn);
                        tLCContDB.setSchema(tLCContSchema);
                        if(tLCContDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCContDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "保单表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }
                    System.out.println("-----------保单表更新结束--------------");
                    if(mLCPolSet.size()>0)
                    {
                      for (int i = 1;i <= mLCPolSet.size();i++)
                      {
                          System.out.println("失败1");
                        LCPolSchema tLCPolSchema = new LCPolSchema();
                        tLCPolSchema = mLCPolSet.get(i);
                        LCPolDB tLCPolDB = new LCPolDB(conn);
                        tLCPolDB.setSchema(tLCPolSchema);
                        if(tLCPolDB.update() == false)
                        {
                          System.out.println("失败*************");
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "保单表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                          System.out.println("失败2");
                    }

                    System.out.println("-----------保单险种表更新结束--------------");

                    //保费项表
                    if(mLCPremSet.size()>0)
                    {
                      for (int i = 1;i <= mLCPremSet.size();i++)
                      {
                        LCPremSchema tLCPremSchema = new LCPremSchema();
                        tLCPremSchema = mLCPremSet.get(i);

                        LCPremDB tLCPremDB = new LCPremDB(conn);
                        tLCPremDB.setSchema(tLCPremSchema);
                        if(tLCPremDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "保费项表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------保费项表更新结束--------------");

                    //客户告知表
                    if(mLCCustomerImpartSet.size()>0)
                    {
                      for (int i = 1;i <= mLCCustomerImpartSet.size();i++)
                      {
                        LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
                        tLCCustomerImpartSchema = mLCCustomerImpartSet.get(i);

                        LCCustomerImpartDB tLCCustomerImpartDB = new LCCustomerImpartDB(conn);
                        tLCCustomerImpartDB.setSchema(tLCCustomerImpartSchema);
                        if(tLCCustomerImpartDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCCustomerImpartDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "客户告知表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------客户告知表更新结束--------------");

                    //核保错误信息表
                    if(mLCUWErrorSet.size()>0)
                    {
                      for (int i = 1;i <= mLCUWErrorSet.size();i++)
                      {
                        LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
                        tLCUWErrorSchema = mLCUWErrorSet.get(i);

                        LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB(conn);
                        tLCUWErrorDB.setSchema(tLCUWErrorSchema);
                        if(tLCUWErrorDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "核保错误信息表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------核保错误信息表更新结束--------------");

                    //核保主表
                    if(mLCUWMasterSet.size()>0)
                    {
                      for (int i = 1;i <= mLCUWMasterSet.size();i++)
                      {
                        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
                        tLCUWMasterSchema = mLCUWMasterSet.get(i);

                        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB(conn);
                        tLCUWMasterDB.setSchema(tLCUWMasterSchema);
                        if(tLCUWMasterDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "核保主表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------核保主表更新结束--------------");

                    //领取项表
                    if(mLCGetSet.size()>0)
                    {
                      for (int i = 1;i <= mLCGetSet.size();i++)
                      {
                        LCGetSchema tLCGetSchema = new LCGetSchema();
                        tLCGetSchema = mLCGetSet.get(i);

                        LCGetDB tLCGetDB = new LCGetDB(conn);
                        tLCGetDB.setSchema(tLCGetSchema);
                        if(tLCGetDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCGetDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "领取项表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------领取项表更新结束--------------");

                    //账户表
                    if(mLCInsureAccSet.size()>0)
                    {
                      for (int i = 1;i <= mLCInsureAccSet.size();i++)
                      {
                        LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
                        tLCInsureAccSchema = mLCInsureAccSet.get(i);

                        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB(conn);
                        tLCInsureAccDB.setSchema(tLCInsureAccSchema);
                        if(tLCInsureAccDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCInsureAccDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "账户表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------账户表更新结束--------------");

                    //体检通知表
                    if(mLCPENoticeSet.size()>0)
                    {
                      for (int i = 1;i <= mLCPENoticeSet.size();i++)
                      {
                        LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
                        tLCPENoticeSchema = mLCPENoticeSet.get(i);

                        LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB(conn);
                        tLCPENoticeDB.setSchema(tLCPENoticeSchema);
                        if(tLCPENoticeDB.update() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCPENoticeDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "体检通知表更新失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }
                      }
                    }

                    System.out.println("-----------体检通知表更新结束--------------");

                    //删除部分
                    //投保人表
                    if(mLCAppntSet.size()>0)
                    {
//                      for (int i = 1;i <= mLCAppntSet.size();i++)
//                      {
                        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
                        tLCAppntSchema = mLCAppntSet.get(1);//投保人唯一
                        String tCustomerNo = tLCAppntSchema.getAppntNo();
                        LCAppntDB tLCAppntDB = new LCAppntDB(conn);
                        tLCAppntDB.setSchema(tLCAppntSchema);
                        if(tLCAppntDB.delete() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCAppntDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "投保人表删除失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }

//                        for(int j=1;j <= mLCPolSet.size();j++)
//                        {
                          LCPolSchema tLCPolSchema = new LCPolSchema();
                          tLCPolSchema = mLCPolSet.get(1);//投保人是唯一，取一个即可
                          if(tLCAppntSchema.getContNo().equals(tLCPolSchema.getContNo()))
                          {
                            tLCAppntSchema.setAppntNo(tLCPolSchema.getAppntNo());
                            tLCAppntDB = new LCAppntDB(conn);
                            tLCAppntDB.setSchema(tLCAppntSchema);

                            if(tLCAppntDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCAppntDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "PersonChkSureBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = "投保人表更新失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              return false;
                            }

                          }
//                        }

                        System.out.println("-----------投保人表更新结束--------------");

                        LCPolDB tLCPolDB = new LCPolDB(conn);
                        LCPolSet tLCPolSet = new LCPolSet();
                        tLCPolDB.setAppntNo(tCustomerNo);
                        tLCPolSet = tLCPolDB.query();
                        if(tLCPolSet.size()==0)
                        {
                          LDPersonDB tLDPersonDB = new LDPersonDB(conn);
                          tLDPersonDB.setCustomerNo(tCustomerNo);
                          if(tLDPersonDB.delete() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "PersonChkSureBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "客户信息表删除失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            return false;
                          }
                        }

                        System.out.println("-----------客户信息表删除结束--------------");

                      }
 //                   }

                    //被保人表
                    if(mLCInsuredSet.size()>0)
                    {
                      for (int i = 1;i <= mLCInsuredSet.size();i++)
                      {
                        System.out.println("要修改的客户数"+mLCInsuredSet.size());
                        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
                        tLCInsuredSchema = mLCInsuredSet.get(i);
                        String tCustomerNo = tLCInsuredSchema.getInsuredNo();
                        LCInsuredDB tLCInsuredDB = new LCInsuredDB(conn);
                        tLCInsuredDB.setSchema(tLCInsuredSchema);
                        if(tLCInsuredDB.delete() == false)
                        {
                          // @@错误处理
                          this.mErrors.copyAllErrors(tLCInsuredDB.mErrors);
                          CError tError = new CError();
                          tError.moduleName = "PersonChkSureBLS";
                          tError.functionName = "saveData";
                          tError.errorMessage = "被保人表删除失败!";
                          this.mErrors .addOneError(tError) ;
                          conn.rollback() ;
                          conn.close();
                          return false;
                        }

                        for(int j=1;j <= mLCPolSet.size();j++)
                        {
                          LCPolSchema tLCPolSchema = new LCPolSchema();
                          tLCPolSchema = mLCPolSet.get(j);
                          {
                            tLCInsuredSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
                            tLCInsuredDB = new LCInsuredDB(conn);
                            tLCInsuredDB.setSchema(tLCInsuredSchema);

                            if(tLCInsuredDB.insert() == false)
                            {
                              // @@错误处理
                              this.mErrors.copyAllErrors(tLCInsuredDB.mErrors);
                              CError tError = new CError();
                              tError.moduleName = "PersonChkSureBLS";
                              tError.functionName = "saveData";
                              tError.errorMessage = "被保人表更新失败!";
                              this.mErrors .addOneError(tError) ;
                              conn.rollback() ;
                              conn.close();
                              return false;
                            }

                          }

                        }

                        System.out.println("-----------被保人表更新结束--------------");

                        LCPolDB tLCPolDB = new LCPolDB(conn);
                        LCPolSet tLCPolSet = new LCPolSet();
                        tLCPolDB.setInsuredNo(tCustomerNo);
                        tLCPolSet = tLCPolDB.query();
                        if(tLCPolSet.size()==0)
                        {
                          LDPersonDB tLDPersonDB = new LDPersonDB(conn);
                          tLDPersonDB.setCustomerNo(tCustomerNo);
                          if(tLDPersonDB.delete() == false)
                          {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLDPersonDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "PersonChkSureBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "客户信息表删除失败!";
                            this.mErrors .addOneError(tError) ;
                            conn.rollback() ;
                            conn.close();
                            return false;
                          }
                        }
                        System.out.println("-----------客户信息表删除结束--------------");

                      }
                    }

                    conn.commit();
                    conn.close();

                } // end of try
                catch (Exception ex)
                {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "PersonChkSureBLS";
                  tError.functionName = "submitData";
                  tError.errorMessage = ex.toString();
                  this.mErrors .addOneError(tError);
                  try {
                    conn.rollback() ;
                    conn.close();
                  }
                  catch (Exception e) {
                  }
                  return false;
                }
                return true;
        }
}
