package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.pubfun.CalBase;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LMUWSchema;
import com.sinosoft.lis.schema.LRUWErrorSchema;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;

/**
 * <p>Title: Web业务系统承保团体自动核保业务部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 2.0
 */
public class LRGrpUWAutoChkBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private MMap mMap = new MMap();

    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperator;
    private String mManagecom;

    private String mContPassFlag = "0"; //合同通过标记

    private String mCalCode; //计算编码

    private double mValue;

    private String mGrpContNo = "";

    private String mType;
    
    private String retire;//退休比
    
    private CalBase mCalBase = new CalBase();

    GlobalInput tGlobalInput = new GlobalInput();

    TransferData mTransferData = new TransferData();
    
    private boolean mGrpUwSuccFlag = true;
    
    int mUwTimes = 0;//保单再保规则次数
    
    /** 再保自核错误信息表 */
    private LRUWErrorSet mLRUWErrorSet = new LRUWErrorSet();

    public LRGrpUWAutoChkBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        TransferData tTransferData = new TransferData();

        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mResult.clear();

        mType = "2";//团单
        // ----------------------------------------

        System.out.println("ssss" + mType);
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();

        System.out.println("---1---");

        //得到外部传入的数据,将数据备份到本类中
        LCGrpContSchema tLCGrpContSchema = getInputData(cInputData);

        if (tLCGrpContSchema == null)
        {
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);
            return false;
        }

        System.out.println("---LRGrpUWAutoChkBL getInputData---");

        if (!dealData(tLCGrpContSchema))
        {
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);
            return false;
        }

        System.out.println("---LRGrpUWAutoChkBL dealData---");

        //准备给后台的数据
        prepareOutputData(tLCGrpContSchema);

        mResult.add(mMap);

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            tTransferData.setNameAndValue("FinishFlag", "0");
            tTransferData.setNameAndValue("UWFlag", "0");
            mResult.add(tTransferData);

            return false;
        }

        tTransferData.setNameAndValue("FinishFlag", "1");
        tTransferData.setNameAndValue("UWFlag", mContPassFlag);
        mResult.add(tTransferData);

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData(LCGrpContSchema tLCGrpContSchema)
    {
        mGrpContNo = tLCGrpContSchema.getGrpContNo(); //获得保单号

        LCContSet tLCContSet = new LCContSet();


        LCContSchema tLCContSchema = null;
        GlobalInput tG = new GlobalInput();
        tG.Operator = mOperator;
        tG.ManageCom = mManagecom;

        VData tInputData = new VData();
        LRAutoCheckBL tLRAutoCheckBL = null;
        int nIndex = 0;
        int nCount;
        RSWrapper tRSWrapper = new RSWrapper();
        tRSWrapper.prepareData(tLCContSet,"select * from lccont where grpcontno='" + mGrpContNo+ "'");
        String tSql = "select max(uwtimes) from lruwerror where prtno = '"+tLCGrpContSchema.getPrtNo()+"' ";
        String uwtimes = new ExeSQL().getOneValue(tSql);
        if(uwtimes == null || "".equals(uwtimes)){
        	uwtimes = "0";
        }
        mUwTimes = Integer.parseInt(uwtimes)+1;
        TransferData tRTransferData = new TransferData();
        tRTransferData.setNameAndValue("UwTimes", mUwTimes+"");
        System.out.println("保单["+mGrpContNo+"]第["+mUwTimes+"]次再保审核！");
        do
        {
        	if (tLCContSet == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpUWAutoChkBL";
                tError.functionName = "dealData";
                tError.errorMessage = "取集体保单号为：" + mGrpContNo + " 的个人保单失败";
                this.mErrors.addOneError(tError);

                return false;
            }
        	
        	tRSWrapper.getData();
            nCount = tLCContSet.size();
            for (nIndex = 1; nIndex <= nCount; nIndex++)
            {
                tLCContSchema = tLCContSet.get(nIndex);
                tInputData.clear();
                tInputData.add(tG);
                tInputData.add(tRTransferData);
                tInputData.add(tLCContSchema);
                tLRAutoCheckBL = new LRAutoCheckBL();
                if (!tLRAutoCheckBL.submitData(tInputData, ""))
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLRAutoCheckBL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "GrpUWAutoChkBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "对集体保单号为：" + mGrpContNo
                            + " 的集体保单下的个人保单进行核保时失败";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if(!tLRAutoCheckBL.isUwSuccFlg()){//获取分单再保成功标志)
                	mGrpUwSuccFlag = false;
                }
            }
        }
        while (tLCContSet.size() > 0);
        System.out.println("保单["+mGrpContNo+"]对分单再保自核是否通过："+mGrpUwSuccFlag);
//        以前开始保单层再保自核
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolSchema tLCGrpPolSchema = null;

        tLCGrpPolDB.setGrpContNo(mGrpContNo);
        tLCGrpPolSet = tLCGrpPolDB.query();

        nCount = tLCGrpPolSet.size();
        nIndex = 0;

        LMUWSet tLMUWSetUnpass = new LMUWSet(); //未通过的核保规则
        LMUWSet tLMUWSetAll = null; //所有核保规则
        LMUWSchema tLMUWSchema = null;

        for (nIndex = 1; nIndex <= nCount; nIndex++)
        {
            tLCGrpPolSchema = tLCGrpPolSet.get(nIndex);
            //准备算法，获取某险种的所有核保规则的集合
            tLMUWSetUnpass.clear();

            if (tLMUWSetAll != null)
            {
                tLMUWSetAll.clear();
            }

            tLMUWSetAll = CheckKinds(tLCGrpPolSchema);

            if (tLMUWSetAll == null)
            {
                return false;
            }

            //准备数据，从险种信息中获取各项计算信息
            CheckPolInit(tLCGrpPolSchema);

            int n = tLMUWSetAll.size(); //核保规则数量

            if (n == 0)
            {
            	
            }
            else
            { //目前目前所有的险种均有一些公共的核保规则,所以必定走该分枝

                for (int i = 1; i <= n; i++)
                {
                    //取计算编码
                    tLMUWSchema = new LMUWSchema();
                    tLMUWSchema = tLMUWSetAll.get(i);
                    mCalCode = tLMUWSchema.getCalCode();

                    if (CheckPol(tLCGrpPolSchema.getRiskCode()) == 0)
                    {
                    }
                    else//团单层再保自核不成功
                    {
                    	mGrpUwSuccFlag = false;
                    	tLMUWSetUnpass.add(tLMUWSchema);
                    }
                }
            }

            dealOnePol(tLCGrpPolSchema, tLMUWSetUnpass);
        }
        return true;
    }


    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private void dealOnePol(LCGrpPolSchema tLCGrpPolSchema,
            LMUWSet tLMUWSetUnpass)
    {
        if(mUwTimes ==0){
        	mUwTimes = 1;
        }
        //取核保错误信息
        //mLRUWErrorSet.clear();
        for(int i=1;i<=tLMUWSetUnpass.size();i++){
        	 //取出错信息
        	LMUWSchema tLMUWSchema = new LMUWSchema();
        	tLMUWSchema = tLMUWSetUnpass.get(i);
    	    LRUWErrorSchema tLRUWErrorSchema = new LRUWErrorSchema();
    	    //生成流水号
    	    String tserialno = PubFun1.CreateMaxNo("LRUWSerNo",20);
    	    tLRUWErrorSchema.setSerialNo(tserialno);
    	    tLRUWErrorSchema.setPrtNo(tLCGrpPolSchema.getPrtNo());
    	    tLRUWErrorSchema.setProposalNo(tLCGrpPolSchema.getGrpProposalNo());
    	    tLRUWErrorSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
    	    tLRUWErrorSchema.setInsuredNo("");
    	    tLRUWErrorSchema.setInsuredName("");
    	    tLRUWErrorSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
    	    tLRUWErrorSchema.setAppntName(tLCGrpPolSchema.getGrpName());
    	    tLRUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode());
    	    tLRUWErrorSchema.setUWError(tLMUWSchema.getRemark());
    	    tLRUWErrorSchema.setUWType("1");
    	    tLRUWErrorSchema.setUWTimes(mUwTimes+"");
    	    tLRUWErrorSchema.setManageCom(mManagecom);
    	    tLRUWErrorSchema.setOperator(mOperator);
    	    tLRUWErrorSchema.setMakeDate(PubFun.getCurrentDate());
    	    tLRUWErrorSchema.setMakeTime(PubFun.getCurrentTime());
    	    tLRUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
    	    tLRUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
    	    mLRUWErrorSet.add(tLRUWErrorSchema); //将自核中出错的信息添加进 mLCUWErrorSet
        }
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private LCGrpContSchema getInputData(VData cInputData)
    {

        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperator = tGlobalInput.Operator;
        mManagecom = tGlobalInput.ManageCom;
        this.mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        LCGrpContSchema tLCGrpContSchema = (LCGrpContSchema) cInputData
                .getObjectByObjectName("LCGrpContSchema", 0); //从输入数据中获取合同记录的数据
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tLCGrpContSchema.getGrpContNo());

        if (tLCGrpContDB.getInfo())
        { //验证LCGrpCont表中是否存在该合同项记录
            return tLCGrpContDB.getSchema();
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "集体合同号为" + tLCGrpContSchema.getGrpContNo()
                    + "的合同信息未查询到!";
            this.mErrors.addOneError(tError);

            return null;
        }
    }

    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private LMUWSet CheckKinds(LCGrpPolSchema tLCGrpPolSchema)
    {
        String tsql = "select * from lmuw where uwtype = 'R' and riskcode = '"+tLCGrpPolSchema.getRiskCode()+"'  order by calcode";

        LMUWDB tLMUWDB = new LMUWDB();
        LMUWSet tLMUWSet = null;
        tLMUWSet = tLMUWDB.executeQuery(tsql);
        if (tLMUWDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMUWDB.mErrors);

            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds";
            tError.errorMessage = tLCGrpPolSchema.getRiskCode().trim() +
                                  "险种再保规则查询失败!";
            this.mErrors.addOneError(tError);
            tLMUWSet.clear();

            return null;
        }
        return tLMUWSet;
    }

    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(LCGrpPolSchema tLCGrpPolSchema)
    {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCGrpPolSchema.getPrem());
        mCalBase.setGet(tLCGrpPolSchema.getAmnt());
        mCalBase.setMult(tLCGrpPolSchema.getMult());
        mCalBase.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        mCalBase.setGrpContNo(mGrpContNo);
    }

    /**
     * 个人单核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private double CheckPol(String tRiskCode)
    {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        retire =  getRetire(tRiskCode);

        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        mCalculator.addBasicFactor("GrpPolNo", mCalBase.getGrpPolNo());
        mCalculator.addBasicFactor("GrpContNo", mCalBase.getGrpContNo());
        mCalculator.addBasicFactor("RetireRate", retire);
        mCalculator.addBasicFactor("RiskCode", tRiskCode); //tLCGrpPolSchema.getRiskCode());;

        String tStr = "";
        tStr = mCalculator.calculate();

        if ((tStr == null) || tStr.trim().equals(""))
        {
            mValue = 0;
        }
        else
        {
            mValue = Double.parseDouble(tStr);
        }

        System.out.println(mValue);

        return mValue;
    }


    /**
     * 准备需要保存的数据
     */
    private void prepareOutputData(LCGrpContSchema tLCGrpContSchema)
    {
    	mMap.put(mLRUWErrorSet, "INSERT");
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpUWAutoChkBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    /**
     * getRetire
     *
     * @return Double
     * @param tLCContPlanRiskSchema LCContPlanRiskSchema
     */
    private String getRetire(String  tRiskCode)
    {
        
        String insuredSql = "select count(1) from lcinsuredlist where grpcontno='"
                		+ this.mGrpContNo + "' and contplancode in " +
                		"(select contplancode from LCContPlanRisk where grpcontno = '"+this.mGrpContNo+"' and riskcode = '"+tRiskCode+"' )";
        String no = (new ExeSQL()).getOneValue(insuredSql);
        System.out.println("ExeSQL : " + insuredSql);
        if (StrTool.cTrim(no).equals("") || StrTool.cTrim(no).equals("null")
                || StrTool.cTrim(no).equals("0"))
        {
            buildError("getRetire", "参保人数为空！");
            return "0";
        }
        String sql = "select DECIMAL((select count(1) from lcinsuredlist where retire='2' and grpcontno='"
                + this.mGrpContNo
                + "' and contplancode in "
                +"(select contplancode from LCContPlanRisk where grpcontno = '"+this.mGrpContNo+"' and riskcode = '"+tRiskCode+"' )) "
                + ",12,2)/"
                + no
                + " from dual";
        String retire = (new ExeSQL()).getOneValue(sql);
        return retire;
    }

	public boolean isMGrpUwSuccFlag() {
		return mGrpUwSuccFlag;
	}

	public void setMGrpUwSuccFlag(boolean grpUwSuccFlag) {
		mGrpUwSuccFlag = grpUwSuccFlag;
	}
	
	public static void  main(String[] args){
		LRGrpUWAutoChkBL tLRGrpUWAutoChkBL = new LRGrpUWAutoChkBL();
		
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "ly";
		
		TransferData tTransferData = new TransferData();
		
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		String tSql = "select * from lcgrpcont where prtno = '18111111001' ";
		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContSchema = tLCGrpContDB.executeQuery(tSql).get(1);
		
		VData tVData = new VData();
		tVData.add(tGlobalInput);
		tVData.add(tTransferData);
		tVData.add(tLCGrpContSchema);
		tLRGrpUWAutoChkBL.submitData(tVData, "");
	}

}
