package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统个人单人工核保生调录入部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class AskTrackChkBL
{
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();

        /** 往后面传输数据的容器 */
        private VData mInputData;


        /** 数据操作字符串 */
        private String mOperate;
        private String mIsueManageCom;
        private String mManageCom;
        private String mpassflag;    //通过标记
        private int merrcount;    //错误条数
        private String mCalCode;    //计算编码
        private String mUser;
        private FDate fDate = new FDate();
        private double mValue;
        private String mInsuredNo = "";
        private String mBackObj = "";
        private String mflag = "";    //分，个单标记
        private String mPrtSeq ="";

        /** 业务处理相关变量 */
        private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
        private LCGrpContSet mmLCGrpContSet = new LCGrpContSet();
        private LCGrpContSet m2LCGrpContSet = new LCGrpContSet();
        private LCGrpContSet mAllLCGrpContSet = new LCGrpContSet();
        private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
        private String mPolNo = "";
        private String mOldPolNo = "";

        /** 集体单表 */
        private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
        private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
        private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();


        /** 往界面传输数据的容器 */
        MMap mMap = new MMap();
        private VData mResult = new VData();

        /** 核保主表 */
        private LCCUWMasterSet mLCCUWMasterSet = new LCCUWMasterSet();
        private LCCUWMasterSet mAllLCCUWMasterSet = new LCCUWMasterSet();
        private LCCUWMasterSchema mLCCUWMasterSchema = new LCCUWMasterSchema();

        /** 核保子表 */
        private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
        private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
        private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();

        /** 核保错误信息表 */
        private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
        private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();

        /** 被保险人表 */
        private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
        private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();

        /** 生存调查表 */
        private LCAskTrackSchema mLCAskTrackSchema = new LCAskTrackSchema();
        private LCAskTrackSet mLCAskTrackSet = new LCAskTrackSet();



        /** 打印管理表 */
        private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

        private LOPRTManagerSet mAllLOPRTManagerSet = new LOPRTManagerSet();

        /**计算公式表**/
        private LMUWSchema mLMUWSchema = new LMUWSchema();


        private LMUWSet mLMUWSet = new LMUWSet();
        private CalBase mCalBase = new CalBase();
        private GlobalInput mGlobalInput = new GlobalInput();

        public AskTrackChkBL()
        {
        }

        /**
         * 传输数据的公共方法
         * @param: cInputData 输入的数据
         *         cOperate 数据操作
         * @return:
         */
        public boolean submitData(VData cInputData, String cOperate)
        {
                int flag = 0;    //判断是不是所有数据都不成功
                int j = 0;    //符合条件数据个数


                //将操作数据拷贝到本类中
                mInputData = (VData)cInputData.clone();


                System.out.println("---1---");



                //得到外部传入的数据,将数据备份到本类中
                if (!getInputData(cInputData))
                {
                        return false;
                }

                System.out.println("---UWRReportBL getInputData---");





                System.out.println("---UWRReportBL checkData---");

                // 数据操作业务处理
                if (!dealData())
                {
                        return false;
                }

                System.out.println("---UWRReportBL dealData---");

                //准备给后台的数据
                prepareOutputData();

                System.out.println("---UWRReportBL prepareOutputData---");

                //数据提交
//                PubSubmit tSubmit = new PubSubmit();
//
//                if (!tSubmit.submitData(mResult, ""))
//                {
//                        // @@错误处理
//                        this.mErrors.copyAllErrors(tSubmit.mErrors);
//
//                        CError tError = new CError();
//                        tError.moduleName = "GrpUWAutoChkBL";
//                        tError.functionName = "submitData";
//                        tError.errorMessage = "数据提交失败!";
//                        this.mErrors.addOneError(tError);
//                        //tTransferData.setNameAndValue("FinishFlag", "0");
//                       // mResult.add(tTransferData);
//
//                        return false;
//                }

                System.out.println("---GrpUWManuNormChkBL commitData---");
                //tTransferData.setNameAndValue("FinishFlag", "1");
                //mResult.add(tTransferData);

                return true;
        }

        /**
         * 数据操作类业务处理
         * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean dealData()
        {
                if (dealOnePol() == false)
                {
                        return false;
                }

                //打印队列
                if (print() == false)
                {
                        return false;
                }

                return true;
        }

        /**
         * 操作一张保单的业务处理
         * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean dealOnePol()
        {
                // 健康信息
                if (prepareReport() == false)
                {
                        return false;
                }

                return true;
        }

        /**
         * 从输入数据中得到所有对象
         *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData)
        {
                GlobalInput tGlobalInput = new GlobalInput();
                tGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
                mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
                mOperate = tGlobalInput.Operator;
                mManageCom = tGlobalInput.ManageCom;

                mLCGrpContSet.set((LCGrpContSet)cInputData.getObjectByObjectName("LCGrpContSet", 0));
                mLCGrpContSchema = mLCGrpContSet.get(1);
               String  tPolNo = mLCGrpContSchema.getGrpContNo();
               mPolNo = tPolNo;
                if (mLCGrpContSchema == null)
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UWRReportBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "没有传入保单信息!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(mPolNo);

                if (tLCGrpContDB.getInfo() == false)
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UWRReportBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "投保单读取失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCGrpContSchema = tLCGrpContDB.getSchema();

                return true;
        }

        /**
         * 校验是不是已经全部回复
         * @return
         */

        private boolean CheckReply()
        {
                String tsql = "select * from LCAskTrack where GrpContNo = '" + "' and serialno = (select max(serialno) from LCAskTrack where GrpContNo = '";

                System.out.println("sql:" + tsql);

                LCAskTrackDB tLCAskTrackDB = new LCAskTrackDB();
                LCAskTrackSet tLCAskTrackSet = new LCAskTrackSet();

                tLCAskTrackSet = tLCAskTrackDB.executeQuery(tsql);

                if (tLCAskTrackSet.size() == 0)
                {
                }
                else
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UWRReportBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "上次询价跟踪报告尚未回复,不能录入!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                return true;
        }

        /**
         * 打印信息表
         * @return
         */
        private boolean print()
        {
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

                tLOPRTManagerSchema.setPrtSeq(mPrtSeq);
                tLOPRTManagerSchema.setOtherNo(mPolNo);
                tLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
                tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT);
                tLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
                tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GRPPOL);
                tLOPRTManagerSchema.setStateFlag("0");
                tLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
                tLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());


                tLOPRTManagerSchema.setCode(PrintManagerBL.ASK_GRP_TRACK);

                mLOPRTManagerSet.add(tLOPRTManagerSchema);

                return true;

        }

        /**
         * 准备体检资料信息
         * 输出：如果发生错误则返回false,否则返回true
         */
        private boolean prepareReport()
        {
                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(mPolNo);

                if (tLCGrpContDB.getInfo() == false)
                {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);

                        CError tError = new CError();
                        tError.moduleName = "UWRReportBL";
                        tError.functionName = "prepareReport";
                        tError.errorMessage = "无符合条件数据!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
                tLCGrpContSchema = tLCGrpContDB.getSchema();

                //生成流水号
                String strNoLimit = PubFun.getNoLimit( mGlobalInput.ManageCom );
                String tPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", strNoLimit);
                mPrtSeq = tPrtSeq;
                mLCAskTrackSchema.setPrtSeq(tPrtSeq);
                mLCAskTrackSchema.setGrpContNo(tLCGrpContSchema.getGrpContNo());
                mLCAskTrackSchema.setProposalContNo(tLCGrpContSchema.getProposalGrpContNo());

                mLCAskTrackSchema.setAppntNo(tLCGrpContSchema.getAppntNo());

                mLCAskTrackSchema.setManageCom(mManageCom);

                mLCAskTrackSchema.setReplyFlag("0");
                mLCAskTrackSchema.setOperator(mOperate);
                mLCAskTrackSchema.setMakeDate(PubFun.getCurrentDate());
                mLCAskTrackSchema.setMakeTime(PubFun.getCurrentTime());
                mLCAskTrackSchema.setReplyOperator("");
                mLCAskTrackSchema.setReplyDate("");
                mLCAskTrackSchema.setReplyTime("");
                mLCAskTrackSchema.setModifyDate(PubFun.getCurrentDate());
                mLCAskTrackSchema.setModifyTime(PubFun.getCurrentTime());

                //核保主表信息
/*
                LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB();
                tLCCUWMasterDB.setGrpContNo(mPolNo);

                if (tLCCUWMasterDB.getInfo() == false)
                {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "UWRReportBL";
                        tError.functionName = "prepareReport";
                        tError.errorMessage = "无核保主表信息!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCCUWMasterSchema = tLCCUWMasterDB.getSchema();

                if (mLCCUWMasterSchema.getPrintFlag().equals("1"))
                {
                        CError tError = new CError();
                        tError.moduleName = "UWRReportBL";
                        tError.functionName = "prepareReport";
                        tError.errorMessage = "已经发核保通知不可录入!";
                        this.mErrors.addOneError(tError);

                        return false;
                }

                mLCCUWMasterSchema.setReportFlag("1");
*/

                return true;

        }

        /**
         *准备需要保存的数据
     **/
    private void prepareOutputData()
    {
        mMap.put(mLCAskTrackSchema, "INSERT");
        mMap.put(mLOPRTManagerSet, "INSERT");
        mResult.add(mMap);
        mResult.add(mLOPRTManagerSet.get(1));
    }

    public VData getResult()
    {
        return mResult;
    }
}
