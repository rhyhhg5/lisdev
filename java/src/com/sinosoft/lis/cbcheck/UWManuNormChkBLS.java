package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统承保个人人工核保部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class UWManuNormChkBLS
{
  //是否存在需要人工核保保单
  int merrno = 0;
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();

  //
  //private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB(conn);
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

  public UWManuNormChkBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    System.out.println(" UWManuNormChkBLS Submit  Start");
    if (!this.saveData())
      return false;
    System.out.println(" UWManuNormChkBLS Submit   End");

    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
    int temp = mLCPolSet.size();
    System.out.println("投保单数量:  "+temp);
    LCUWErrorSet mLCUWErrorSet = (LCUWErrorSet)mInputData.getObjectByObjectName("LCUWErrorSet",0);
    LCUWMasterSet mLCUWMasterSet = (LCUWMasterSet)mInputData.getObjectByObjectName("LCUWMasterSet",0);
    LCUWSubSet mLCUWSubSet = (LCUWSubSet)mInputData.getObjectByObjectName("LCUWSubSet",0);

    try
    {
      Connection conn = DBConnPool.getConnection();

      if (conn==null)
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "UWManuNormChkBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }

      conn.setAutoCommit(false);

      // 删除部分
      int polCount = mLCPolSet.size();
      for (int i = 1; i <= polCount; i++)
      {

        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema = ( LCPolSchema )mLCPolSet.get( i );
        String tProposalNo = tLCPolSchema.getProposalNo();
        String tUWFlag = tLCPolSchema.getUWFlag();

        // 核保主表
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB( conn );
        tLCUWMasterDB.setProposalNo( tProposalNo );
        if (tLCUWMasterDB.deleteSQL() == false)
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "UWAtuoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = tProposalNo+"个人核保主表删除失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback() ;
          conn.close();
          return false;
        }

        // 投保单
        LCPolDB tLCPolDB = new LCPolDB( conn );
        tLCPolDB.setProposalNo( tProposalNo );
        if (tLCPolDB.deleteSQL() == false)
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "UWManuNormChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = tProposalNo+"个人保单表删除失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback() ;
          conn.close();
          return false;
        }
        // 保存部分
        // 保单

        //LCPolDBSet tLCPolDBSet = new LCPolDBSet( conn );
        //tLCPolDBSet.set( mLCPolSet );
        //if (tLCPolDBSet.insert() == false)
        //LCPolDB tLCPolDB = new LCPolDB(conn);
        tLCPolDB.setSchema(tLCPolSchema);

        if(tLCPolDB.insert() == false)
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "UWManuNormChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = tProposalNo+"个人保单表保存失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback() ;
          conn.close();
          return false;
        }

        System.out.println("----------更新核保主表-------------");
        // 核保主表
        LCUWMasterDBSet tLCUWMasterDBSet = new LCUWMasterDBSet( conn );
        tLCUWMasterDBSet.set( mLCUWMasterSet );
        int masterno = tLCUWMasterDBSet.size();
        for (int j = 1;j <= masterno;j++)
        {
          LCUWMasterSchema tLCUWMasterSchema = tLCUWMasterDBSet.get(j);
          String masterpol = tLCUWMasterSchema.getProposalNo();
          if (masterpol.equals(tProposalNo))
          {
            //LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB(conn);
            tLCUWMasterDB.setSchema(tLCUWMasterSchema);
            if (tLCUWMasterDB.insert() == false)
            {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "UWManuNormChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = tProposalNo+"个人核保总表保存失败!";
              this.mErrors .addOneError(tError) ;
              conn.rollback() ;
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------更新核保子表--------------");

        // 核保子表
        LCUWSubDBSet tLCUWSubDBSet = new LCUWSubDBSet( conn );
        tLCUWSubDBSet.set( mLCUWSubSet );
        int subno = tLCUWSubDBSet.size();
        for (int j = 1;j <= subno;j++)
        {
          LCUWSubSchema tLCUWSubSchema = tLCUWSubDBSet.get(j);
          String subpol = tLCUWSubSchema.getProposalNo();
          if (subpol.equals(tProposalNo))
          {
            LCUWSubDB tLCUWSubDB = new LCUWSubDB(conn);
            tLCUWSubDB.setSchema(tLCUWSubSchema);
            if (tLCUWSubDB.insert() == false)
            {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "UWManuNormChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = tProposalNo+"个人核保轨迹表保存失败!";
              this.mErrors .addOneError(tError) ;
              conn.rollback() ;
              conn.close();
              return false;
            }
          }
        }
        //System.out.println("-----------FFF--------------");

        // 核保错误信息表
                                /*whn
    LCUWErrorDBSet tLCUWErrorDBSet = new LCUWErrorDBSet( conn );
    tLCUWErrorDBSet.set( mLCUWErrorSet );
    int errno = tLCUWErrorDBSet.size();
    merrno = merrno + errno;
    for (i = 1; i<=errno; i++)
    {
     LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
     tLCUWErrorSchema = tLCUWErrorDBSet.get(i);
     String errpol = tLCUWErrorSchema.getPolNo();
     if (errpol.equals(tProposalNo))
     {
      LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB(conn);
      tLCUWErrorDB.setSchema(tLCUWErrorSchema);
      if (tLCUWErrorDB.insert() == false)
      {
        // @@错误处理
           this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "UWAutoChkBLS";
       tError.functionName = "saveData";
       tError.errorMessage = "LCErr表保存失败!";
       this.mErrors .addOneError(tError) ;
              conn.rollback() ;
       return false;
      }
     }
    }
                                whn*/


        if (tUWFlag.equals("3"))
        {
		  System.out.println("-----------更新特别约定表--------------");
          LCSpecSet mLCSpecSet = (LCSpecSet)mInputData.getObjectByObjectName("LCSpecSet",0);
          LCPremSet mLCPremSet = (LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0);
          LCDutySet mLCDutySet = (LCDutySet)mInputData.getObjectByObjectName("LCDutySet",0);
          LBSpecSet mLBSpecSet = (LBSpecSet)mInputData.getObjectByObjectName("LBSpecSet",0);

		  // 特别约定表
         //保存特别约定B表
		  if (mLBSpecSet.size() > 0)
		  {
			 for (int j = 1; j <= mLBSpecSet.size(); j++)
			{
			  System.out.println("J="+j);
			  LBSpecSchema tLBSpecSchema = new LBSpecSchema();
			  tLBSpecSchema = mLBSpecSet.get(j);
			  String errpol = tLBSpecSchema.getPolNo();
			  System.out.println("errpol="+errpol);
			  if (errpol.equals(tProposalNo))
			  {
				LBSpecDB tLBSpecDB = new LBSpecDB(conn);
				tLBSpecDB.setSchema(tLBSpecSchema);
				if (tLBSpecDB.insert() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLBSpecDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "UWManuNormChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = tProposalNo+"特别约定B表保存失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}
			  }
			}
          }

		  //删除特别约定C表
		  LCSpecDB tLCSpecDB = new LCSpecDB(conn);
		  LCSpecSet ttLCSpecSet = new LCSpecSet();
		  tLCSpecDB.setPolNo( tProposalNo ) ;
		  if(!tLCSpecDB.deleteSQL())
		  {
			this.mErrors.copyAllErrors(tLCSpecDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "UWManuNormChkBLS";
			tError.functionName = "saveData";
			tError.errorMessage = tProposalNo+"特别约定C表删除失败!";
			this.mErrors .addOneError(tError) ;
			conn.rollback() ;
			conn.close();
			return false;
		  }

		  //保存特别约定C表
          LCSpecDBSet tLCSpecDBSet = new LCSpecDBSet( conn );
          tLCSpecDBSet.set( mLCSpecSet );
          int errno = tLCSpecDBSet.size();
          merrno = merrno + errno;
          System.out.println("speccount="+errno);
          if (errno > 0)
          {
		     for (int j = 1; j<=errno; j++)
            {
              System.out.println("J="+j);
              LCSpecSchema tLCSpecSchema = new LCSpecSchema();
			  tLCSpecSchema = tLCSpecDBSet.get(j);
              String errpol = tLCSpecSchema.getPolNo();
              System.out.println("errpol="+errpol);
              if (errpol.equals(tProposalNo))
              {
                LCSpecDB mLCSpecDB = new LCSpecDB(conn);
                mLCSpecDB.setSchema(tLCSpecSchema);
                if (mLCSpecDB.insert() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(mLCSpecDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWManuNormChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = tProposalNo+"特别约定表保存失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }
              }
            }
          }

          // 交费项目表
          //删除
          String tsql = "select * from LCPrem where payplancode  like '000000%' and polno = '"+tProposalNo.trim()+"'";
          LCPremDB t2LCPremDB = new LCPremDB(conn);
          LCPremSet tLCPremSet = new LCPremSet();

          tLCPremSet = t2LCPremDB.executeQuery(tsql);
          if(tLCPremSet.size() > 0)
          {
			System.out.println("-----------删除以前的特约责任交费项目--------------");
            for(int j = 1;j <= tLCPremSet.size();j++)
            {
              LCPremSchema tLCPremSchema = new LCPremSchema();
              tLCPremSchema = tLCPremSet.get(j);

              t2LCPremDB = new LCPremDB(conn);

              t2LCPremDB.setPolNo(tProposalNo);
              t2LCPremDB.setDutyCode(tLCPremSchema.getDutyCode());
              t2LCPremDB.setPayPlanCode(tLCPremSchema.getPayPlanCode());

              if(t2LCPremDB.deleteSQL()==false)
              {
                // @@错误处理
                this.mErrors.copyAllErrors(t2LCPremDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBLS";
                tError.functionName = "saveData";
                tError.errorMessage = tProposalNo+"保费项目表删除失败!";
                this.mErrors .addOneError(tError) ;
                conn.rollback() ;
                conn.close();
                return false;
              }
            }
          }

          //保存
          errno = mLCPremSet.size();
          merrno = merrno + errno;
          System.out.println("特约责任交费条数:    "+errno);
          if (errno > 0)
          {
			System.out.println("-----------添加特约责任交费项目--------------");
            for (int j = 1; j<=errno; j++)
            {
              LCPremSchema tLCPremSchema = new LCPremSchema();
              tLCPremSchema = mLCPremSet.get(j);
              String errpol = tLCPremSchema.getPolNo();
              if (errpol.equals(tProposalNo))
              {
                LCPremDB tLCPremDB = new LCPremDB(conn);
                tLCPremDB.setSchema(tLCPremSchema);
                if (tLCPremDB.insert() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCPremDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWManuNormChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = tProposalNo+"保费项目表保存失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }
              }
            }
          }


          // 责任表
          errno = mLCDutySet.size();
          merrno = merrno + errno;
          System.out.println("mLCDutySet"+errno);
          if (errno > 0)
          {
			System.out.println("----------更新责任表--------------");
            for (int j = 1; j<=errno; j++)
            {
              LCDutySchema tLCDutySchema = new LCDutySchema();
              tLCDutySchema = mLCDutySet.get(j);
              String errpol = tLCDutySchema.getPolNo();
              if (errpol.equals(tProposalNo))
              {
                LCDutyDB tLCDutyDB = new LCDutyDB(conn);
                tLCDutyDB.setSchema(tLCDutySchema);
                if (tLCDutyDB.update() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWManuNormChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = tProposalNo+"责任表更新失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }
              }
            }
          }
          //add by yt 2003-6-30
          //更新责任表的保费
          ExeSQL tExeSQL=new ExeSQL(conn);
          String tSql="";
          tSql="update lcduty a set a.prem=(select sum(prem) from lcprem where polno=a.polno and dutycode=a.dutycode) where a.polno='"+tLCPolSchema.getPolNo()+"'";
          if (!tExeSQL.execUpdateSQL(tSql)){
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSpecChkBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "更新责任的总保费失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback() ;
            conn.close();
            return false;
          }
          //更新保单表的保费
          tSql="update lcpol a set a.prem=(select sum(prem) from lcprem where polno=a.polno ) where a.polno='"+tLCPolSchema.getPolNo()+"'";
          if (!tExeSQL.execUpdateSQL(tSql)){
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSpecChkBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "更新责任的总保费失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback() ;
            conn.close();
            return false;
          }
          //add end by yt 2003-6-30
        }


        //打印管理表
       /* if(tUWFlag.equals("999")) //0227　"1-->999"
        {
		  System.out.println("-----------更新打印管理表--------------");

          LOPRTManagerSet mLOPRTManagerSet = (LOPRTManagerSet)mInputData.getObjectByObjectName("LOPRTManagerSet",0);

          // 打印管理表
          int errno = mLOPRTManagerSet.size();
          merrno = merrno + errno;
          System.out.println("speccount="+errno);
          if (errno > 0)
          {
            for (int j = 1; j<=errno; j++)
            {
              System.out.println("J="+j);
              LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
              tLOPRTManagerSchema = mLOPRTManagerSet.get(j);
              String errpol = tLOPRTManagerSchema.getOtherNo();
              System.out.println("printpol="+errpol);
              if (errpol.equals(tProposalNo))
              {
                LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB(conn);
                tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
                if (tLOPRTManagerDB.insert() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWManuNormChkBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = tProposalNo+"打印管理表保存失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }
              }
            }
          }

        }

        System.out.println("-----------KKK--------------");
    *///将其注释--sxy

        //核保申请锁表
        if(tUWFlag.equals("6")) //0227　"1-->999"
        {
          LDSysTraceSet mLDSysTraceSet = (LDSysTraceSet)mInputData.getObjectByObjectName("LDSysTraceSet",0);

          //锁表
          int errno = mLDSysTraceSet.size();
          if (errno > 0)
          {
            for (int j = 1; j<=errno; j++)
            {
              LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
              tLDSysTraceSchema = mLDSysTraceSet.get(j);
              String errpol = tLDSysTraceSchema.getPolNo();

              LDSysTraceDB tLDSysTraceDB = new LDSysTraceDB(conn);
              tLDSysTraceDB.setSchema(tLDSysTraceSchema);
              if (tLDSysTraceDB.deleteSQL() == false)
              {
                // @@错误处理
                this.mErrors.copyAllErrors(tLDSysTraceDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWManuNormChkBLS";
                tError.functionName = "saveData";
                tError.errorMessage = tProposalNo+"撤销核保申请失败!";
                this.mErrors .addOneError(tError) ;
                conn.rollback() ;
                conn.close();
                return false;
              }
            }
          }
        }

      } // end of for


        //应收总表
        System.out.println("Start 应收总表...");
        LJSPayDBSet tLJSPayDBSet = new LJSPayDBSet(conn);
        tLJSPayDBSet.set((LJSPaySet)mInputData.getObjectByObjectName("LJSPaySet",0));
        if (!tLJSPayDBSet.delete()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJSPayDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "LJSPayBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "应收总表数据删除失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }

        //暂交费表
        System.out.println("Start 暂交费表...");
        LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
        tLJTempFeeDBSet.set((LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0));
        if (!tLJTempFeeDBSet.delete()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "LJTempFeeBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "暂交费表数据删除失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }

        //暂交费备份表
        System.out.println("Start 暂交费备份表...");
        LBTempFeeDBSet tLBTempFeeDBSet = new LBTempFeeDBSet(conn);
        tLBTempFeeDBSet.set((LBTempFeeSet)mInputData.getObjectByObjectName("LBTempFeeSet",0));
        if (!tLBTempFeeDBSet.insert()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLBTempFeeDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "LBTempFeeBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "暂交费备份表数据插入失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }

        //暂交费分类表
        System.out.println("Start 暂交费分类表...");
        LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(conn);
        tLJTempFeeClassDBSet.set((LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0));
        if (!tLJTempFeeClassDBSet.delete()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "LJTempFeeClassBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "暂交费分类表数据删除失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }

        //暂交费分类备份表
        System.out.println("Start 暂交费分类备份表...");
        LBTempFeeClassDBSet tLBTempFeeClassDBSet = new LBTempFeeClassDBSet(conn);
        tLBTempFeeClassDBSet.set((LBTempFeeClassSet)mInputData.getObjectByObjectName("LBTempFeeClassSet",0));
        if (!tLBTempFeeClassDBSet.insert()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLBTempFeeClassDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "LBTempFeeClassBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "暂交费分类备份表数据插入失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback();
          conn.close();
          return false;
        }

      conn.commit();
      conn.close();
    } // end of try
    catch (Exception ex)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "UWManuNormChkBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      //try{conn.rollback() ;} catch(Exception e){}
      return false;
    }

    return true;
  }

}