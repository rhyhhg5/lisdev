package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.vbl.LCDutyBLSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * 处理减档减保额
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author YangMing
 * @version 1.0
 */
public class DealSubMultAndAmnt {
    public DealSubMultAndAmnt() {
    }

    public static void main(String[] args) {
        DealSubMultAndAmnt DealSubMultAndAmnt = new DealSubMultAndAmnt();
    }

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private MMap map = new MMap();
    private TransferData mTransferData = new TransferData();
    private GlobalInput tGI = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;
    private String PolNo = "";
    private String EdorType = null;
    /**
     * 降档处理相关字段
     * 传入的责任属性,需要此对象的最小信息量为,必须有责任
     * 必须保额或者档次,必须有险种代码,否则无法计算保费
     */
    private LCDutySet mLCDutySet = null;
    private LCDutySet queryLCDutySet = null;
    private LCPolSchema queryLCPolSchema = null;
    private LCContSchema queryLCContSchema = null;

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLCDutySet = (LCDutySet) cInputData.getObjectByObjectName("LCDutySet", 0);
        //主要用于传送保全标记
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData != null) {
            this.EdorType = (String) mTransferData.getValueByName("EdorType");
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (this.mLCDutySet == null) {
            System.out.println("程序第107行出错，请检查DealSubMultAndAmnt.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的责任为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mLCDutySet.size() <= 0) {
            System.out.println("程序第116行出错，请检查DealSubMultAndAmnt.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的责任为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        /**
         * 个人险种的责任一般为一个责任
         * 即使不是一个责任降档或者降保
         * 额的操作也一定是针对一个险种
         */
        this.PolNo = this.mLCDutySet.get(1).getPolNo();
        if (PolNo == null) {
            System.out.println("程序第146行出错，请检查DealSubMultAndAmnt.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "checkData";
            tError.errorMessage = "险种代码为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(this.PolNo);
        LCDutySet tLCDutySet = tLCDutyDB.query();
        //将查询的责任原始信息保存,作后续更新操作时需要用到
        if (tLCDutySet.size() <= 0) {
            System.out.println("程序第148行出错，请检查DealSubMultAndAmnt.java中的dealData方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "dealData";
            tError.errorMessage = "查询险种责任信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        queryLCDutySet = new LCDutySet();
        this.queryLCDutySet.set(tLCDutySet);
        /**
         * 传入侧责任与查旬的责任数目必须相同
         * 目前降档或者降保额都只是针对险种降
         * 但程序需要为以后扩展而支持针对某一
         * 责任降档或者降保额
         */
        if (tLCDutySet.size() != this.mLCDutySet.size()) {
            System.out.println("程序第163行出错，请检查DealSubMultAndAmnt.java中的dealData方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "dealData";
            tError.errorMessage = "系统中目前的责任数目与降档的责任数目不符！";
            this.mErrors.addOneError(tError);
            return false;
        }
        /**
         * 将降低后的档次和保额付给计算责任
         */
        for (int i = 1; i <= this.mLCDutySet.size(); i++) {
            tLCDutySet.get(i).setPrem(0.0);
            tLCDutySet.get(i).setMult(this.mLCDutySet.get(i).getMult());
            tLCDutySet.get(i).setAmnt(this.mLCDutySet.get(i).getAmnt());
            tLCDutySet.get(i).setModifyDate(PubFun.getCurrentDate());
            tLCDutySet.get(i).setModifyTime(PubFun.getCurrentTime());
        }
        LCDutyBLSet mLCDutyBLSet = new LCDutyBLSet();
        mLCDutyBLSet.set(tLCDutySet);
        /**
         * 准备险种信息,主要作用是为保费计算完更新期保费保额字段
         */
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(PolNo);
        if (!tLCPolDB.getInfo()) {
            System.out.println("程序第144行出错，请检查DealSubMultAndAmnt.java中的dealData方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "dealData";
            tError.errorMessage = "查询到险种信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCPolBL tLCPolBL = new LCPolBL();
        tLCPolBL.setSchema(tLCPolDB.getSchema());
        //为后续操作保存
        queryLCPolSchema = new LCPolSchema();
        this.queryLCPolSchema = tLCPolDB.getSchema();
        //EdorType 主要为保全核保中下降档降保额处理时标记
        //mTransferData 传入保全计算需要的要素信息
        CalBL tCalBL = new CalBL(tLCPolBL, mLCDutyBLSet, EdorType, mTransferData);
        if (!tCalBL.calPol()) {
            this.mErrors.copyAllErrors(tCalBL.mErrors);
            return false;
        }
        /**
         * 保费保额计算完毕后只将保费保额信息更新
         * 其他的保险期间信息,缴费期间信息,风险保
         * 费信息不变.
         * 较复杂的情况是存在加费做降档操作,保费计
         * 算是不含加费的,因此如果有加费的话需要先
         * 将加费信息提出,跟新完成新的保费保额后再
         * 从新处理加费.
         */
        if (!afterCalPol(tCalBL)) {
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        this.mResult.add(map);
        return true;
    }

    /**
     * 处理保费计算完成后的险种信息处理
     * @param tCalBL CalBL
     * @return boolean
     */
    private boolean afterCalPol(CalBL tCalBL) {
        //perm表信息
        LCPremDB queryLCPremDB = new LCPremDB();
        queryLCPremDB.setPolNo(this.PolNo);
        LCPremSet queryLCPremSet = queryLCPremDB.query();
        if (queryLCPremSet.size() <= 0) {
            System.out.println("程序第258行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "查询保费向失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //合同信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.queryLCPolSchema.getContNo());
        if (!tLCContDB.getInfo()) {
            System.out.println("程序第281行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "查询合同信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        queryLCContSchema = new LCContSchema();
        this.queryLCContSchema = tLCContDB.getSchema();
        //查询给付信息
        LCGetDB queryLCGetDB = new LCGetDB();
        queryLCGetDB.setPolNo(this.PolNo);
        LCGetSet queryLCGetSet = queryLCGetDB.query();
        if (queryLCGetSet.size() <= 0) {
            System.out.println("程序第297行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "查询给付失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //查询保单信息完成,开始处理计算完成的保单信息
        LCPolSchema calLCPolSchema = tCalBL.getLCPol();
        LCPremSet calLCPremSet = tCalBL.getLCPrem(); //得到的保费项集合不包括加费的保费项，所以在后面处理
        LCGetSet calLCGetSet = tCalBL.getLCGet();
        LCDutySet calLCDutySet = tCalBL.getLCDuty();
        //处理保费项
        double AddFee = 0.0;
        for (int i = 1; i <= queryLCPremSet.size(); i++) {
            for (int m = 1; m <= calLCPremSet.size(); m++) {
                if (queryLCPremSet.get(i).getPayPlanCode().equals(calLCPremSet.get(m).
                        getPayPlanCode())) {
                    queryLCPremSet.get(i).setPrem(calLCPremSet.get(m).getPrem());
                    queryLCPremSet.get(i).setStandPrem(calLCPremSet.get(m).getStandPrem());
                    break;
                }
            }
            if (queryLCPremSet.get(i).getPayPlanCode().startsWith("000000")) {
                AddFee = queryLCPremSet.get(i).getPrem();
            }
        }
        //处理保额档次
        if (queryLCGetSet.size() != calLCGetSet.size()) {
            System.out.println("程序第328行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "目前险种下的给付项与降档后的给付项不同！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= queryLCGetSet.size(); i++) {
            for (int m = 1; m <= calLCGetSet.size(); m++) {
                if (queryLCGetSet.get(i).getGetDutyCode().equals(queryLCGetSet.get(i).
                        getGetDutyCode())) {
                    queryLCGetSet.get(i).setStandMoney(calLCGetSet.get(m).getStandMoney());
                    queryLCGetSet.get(i).setActuGet(calLCGetSet.get(m).getActuGet());
                    break;
                }
            }
        }
        //处理责任信息
        double Mult = 0;
        if (this.queryLCDutySet.size() != calLCDutySet.size()) {
            System.out.println("程序第345行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "保单中的责任项与降档降保额后的责任项不同！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= queryLCDutySet.size(); i++) {
            for (int m = 1; m <= calLCDutySet.size(); m++) {
                if (queryLCDutySet.get(i).getDutyCode().equals(calLCDutySet.get(m).getDutyCode())) {
                    if (i == 1 && AddFee > 0) {
                        queryLCDutySet.get(i).setPrem(calLCDutySet.get(m).getPrem() + AddFee);
                    } else {
                        queryLCDutySet.get(i).setPrem(calLCDutySet.get(m).getPrem());
                    }
                    queryLCDutySet.get(i).setStandPrem(calLCDutySet.get(m).getStandPrem());
                    queryLCDutySet.get(i).setAmnt(calLCDutySet.get(m).getAmnt());
                    queryLCDutySet.get(i).setMult(calLCDutySet.get(m).getMult());
                    break;
                }
            }
            Mult += queryLCDutySet.get(i).getMult();
        }
        //处理险种信息
        this.queryLCPolSchema.setPrem(calLCPolSchema.getPrem() + AddFee);
        this.queryLCPolSchema.setStandPrem(calLCPolSchema.getStandPrem());
        this.queryLCPolSchema.setAmnt(calLCPolSchema.getAmnt());
        this.queryLCPolSchema.setMult(Mult);
        this.queryLCPolSchema.setRiskAmnt(calLCPolSchema.getRiskAmnt());
        this.queryLCPolSchema.setUWFlag("4");
        //处理合同信息
        String strSql =
                "select sum(prem),sum(standprem),sum(mult),sum(amnt) from lcpol where contno='"
                + this.queryLCContSchema.getContNo() + "' and polno<>'"
                + this.queryLCPolSchema.getPolNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs = tExeSQL.execSQL(strSql);
        String[][] result = ssrs.getAllData();
        if (result.length != 1) {
            System.out.println("程序第384行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "查询总保费保额失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        double SumPrem = 0.0;
        double SumMult = 0.0;
        double SumAmnt = 0.0;
        try {
            if (!result[0][0].equals("null")) {
                SumPrem = Double.parseDouble(result[0][0]);
            }
            if (!result[0][2].equals("null")) {
                SumMult = Double.parseDouble(result[0][2]);
            }
            if (!result[0][3].equals("null")) {
                SumAmnt = Double.parseDouble(result[0][3]);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("程序第399行出错，请检查DealSubMultAndAmnt.java中的afterCalPol方法！");
            CError tError = new CError();
            tError.moduleName = "DealSubMultAndAmnt.java";
            tError.functionName = "afterCalPol";
            tError.errorMessage = "取总保费保额失败！" + ex.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }
        this.queryLCContSchema.setPrem(SumPrem + queryLCPolSchema.getPrem());
        this.queryLCContSchema.setMult(SumMult + queryLCPolSchema.getMult());
        this.queryLCContSchema.setAmnt(SumAmnt + queryLCPolSchema.getAmnt());

        map.put(queryLCPremSet, "UPDATE");
        map.put(queryLCGetSet, "UPDATE");
        map.put(queryLCDutySet, "UPDATE");
        map.put(queryLCPolSchema, "UPDATE");
        map.put(queryLCContSchema, "UPDATE");

        return true;
    }

    /**
     * 获取结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
