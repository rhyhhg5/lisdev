package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class UWIndCustomerReplayUI {
    /** �������� */
    public CErrors mErrors = new CErrors();
    public UWIndCustomerReplayUI() {
    }

    /**
     *
     * @param tInputData VData
     * @param tOperate String
     * @return boolean
     */
    public boolean submitData(VData tInputData, String tOperate) {
        UWIndCustomerReplayBL tUWIndCustomerReplayBL = new
                UWIndCustomerReplayBL();
        if (!tUWIndCustomerReplayBL.submitData(tInputData, tOperate)) {
            this.mErrors.copyAllErrors(tUWIndCustomerReplayBL.mErrors);
            return false;
        }
        return true;
    }
}
