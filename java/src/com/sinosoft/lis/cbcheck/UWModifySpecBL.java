package com.sinosoft.lis.cbcheck;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 修改或新增核保特约</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class UWModifySpecBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 全局基础数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  /** 额外传递的参数 */
  //private TransferData mTransferData = null;

  /** 传入的业务数据 */
  private LCSpecSet inLCSpecSet = new LCSpecSet();

  /** 传出的业务数据 */
  private LCSpecSet outLCSpecSet = new LCSpecSet();
  private LCSpecSet delLCSpecSet = new LCSpecSet();

  public UWModifySpecBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //需要传到后台处理
    if (mOperate.equals("INSERT")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start UWModifySpec BLS Submit...");
      UWModifySpecBLS tUWModifySpecBLS = new UWModifySpecBLS();
      if(tUWModifySpecBLS.submitData(mInputData, cOperate) == false)	{
        //@@错误处理
        this.mErrors.copyAllErrors(tUWModifySpecBLS.mErrors);
        mResult.clear();
        mResult.add(mErrors.getFirstError());
        return false;
      }	else {
        mResult = tUWModifySpecBLS.getResult();
      }
      System.out.println("End UWModifySpec BLS Submit...");
    }
    //不需要传到后台处理
    else if (mOperate.equals("")) {
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);

      //mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      inLCSpecSet = (LCSpecSet)mInputData.getObjectByObjectName("LCSpecSet", 0);
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "UWModifySpecBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      if (mOperate.equals("INSERT")) {
        for (int i=0; i<inLCSpecSet.size(); i++) {
          LCSpecSchema tLCSpecSchema = inLCSpecSet.get(i+1);

          LCPolDB tLCPolDB = new LCPolDB();
          tLCPolDB.setPolNo(tLCSpecSchema.getPolNo());
          if (!tLCPolDB.getInfo()) throw new Exception("该保单号不存在，不能为其增加特约！");

//          tLCSpecSchema.setSpecNo(PubFun1.CreateMaxNo("SpecNo", PubFun.getNoLimit(mGlobalInput.ComCode)));
//          tLCSpecSchema.setPolType("1");
          tLCSpecSchema.setOperator(mGlobalInput.Operator);
          tLCSpecSchema.setMakeDate(PubFun.getCurrentDate());
          tLCSpecSchema.setMakeTime(PubFun.getCurrentTime());
          tLCSpecSchema.setModifyDate(PubFun.getCurrentDate());
          tLCSpecSchema.setModifyTime(PubFun.getCurrentTime());

          LCSpecSchema delLCSpecSchema = new LCSpecSchema();
          delLCSpecSchema.setPolNo(tLCSpecSchema.getPolNo());
          delLCSpecSet.add(delLCSpecSchema.getDB().query());
        }

        outLCSpecSet = inLCSpecSet;
      }
      else if (mOperate.equals("QUERY")) {
        for (int i=0; i<inLCSpecSet.size(); i++) {
          LCSpecSchema tLCSpecSchema = new LCSpecSchema();
          String strSql = "select * from lcspec where polno='" + inLCSpecSet.get(i+1).getPolNo()
                        + "' and specno in (select max(specno) from lcspec where polno='" + inLCSpecSet.get(i+1).getPolNo() + "')";

          outLCSpecSet.add(tLCSpecSchema.getDB().executeQuery(strSql));
        }

        mResult.add(outLCSpecSet);
      }
    }
    catch(Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "UWModifySpecBL";
      tError.functionName = "dealData";
      tError.errorMessage = "数据处理错误! " + e.getMessage();
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();

      mInputData.add(outLCSpecSet);
      mInputData.add(delLCSpecSet);
    }
    catch(Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError =new CError();
      tError.moduleName="UWModifySpecBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错! ";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args) {
    UWModifySpecBL UWModifySpecBL1 = new UWModifySpecBL();
  }
}
