package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:核保抽检上报通用程序 </p>
 * <p>Description: 新契约个险和团险投保单的核保抽检、上报、下发等处理程序 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 * 修 改 人：闫少杰
 * 修改日期：2006-02-24
 * 修改内容:
 *          1 增加系统自动上报时对核保上报原因实体表处理上报原因记录的保存
 *                说明： 主动上报不在核保上报原因实体表保存上报原因记录；
 */

public class UWSendBackTraceBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    //private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperator;

    private String mManageCom;

    //private String mOperate;

    /** 业务逻辑类*/
    private LCUWSendTraceSchema mLCUWSendTraceSchema = new LCUWSendTraceSchema();

    private LWMissionSchema mLWMissionSchema = new LWMissionSchema();

    private LCCUWMasterSchema mLCCUWMasterSchema = new LCCUWMasterSchema();

    private LDUWUserSchema mLDUWUserSchema = new LDUWUserSchema();

    private LDSpotTrackSet mLDSpotTrackSet = new LDSpotTrackSet();

    private LCGCUWMasterSchema mLCGCUWMasterSchema = new LCGCUWMasterSchema();

    private LCGCUWSubSchema mLCGCUWSubSchema = new LCGCUWSubSchema();

    private LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();

    private LCGCUWMasterSchema mNewLCGCUWMasterSchema = new LCGCUWMasterSchema();

    private LCGUWMasterSet mLCGUWMasterSet = new LCGUWMasterSet();

    private LCGUWSubSet mLCGUWSubSet = new LCGUWSubSet();

    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();

    private LWMissionSchema cLWMissionSchema = new LWMissionSchema();

    private String Inputflag;

    private MMap map = new MMap();

    private LCGrpContDB tLCGrpContDB = new LCGrpContDB();

    private LCGrpIssuePolDB tLCGrpIssuePolDB = new LCGrpIssuePolDB();

    private LCGrpIssuePolSet tLCGrpIssuePolSet = new LCGrpIssuePolSet();

    /** 业务数据字符串 */
    private String mOtherNoType = "0"; //其他编码类型，用于表示是何种类型上报或抽检。  1-个险新契约，2-团险新契约

    private String mUserCode; //用户代码,用于记录需要跳转到的用户

    public UWSendBackTraceBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        //判断保单所属状态
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LWMissionSet tLWMissionSet = new LWMissionSet();
        tLWMissionDB.setActivityID("0000002004");
        System.out.println("bababab" + mLCGrpContSet.get(1).getGrpContNo());
        tLWMissionDB.setMissionProp1(mLCGrpContSet.get(1).getGrpContNo());
        tLWMissionSet = tLWMissionDB.query();
        if (tLWMissionSet.size() == 0)
        {
            System.out.println("zzzzzzz!");
            //            backstate("2");
            if (!backstate("2"))
            {
                return false;
            }
            System.out.println("zhuzhuzhuzhzuz");
        }
        else
        {
            Inputflag = "1";
            //校验前台传入数据是否有效业务数据
            if (!checkData())
            {
                return false;
            }

            System.out.println("Start  dealData...");
            //进行业务处理
            if (!dealData())
            {
                return false;
            }
            System.out.println("dealData successful!");
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start  Submit...");
        //提交数据
        if (cOperate.equals("submit"))
        {
            PubSubmit tSubmit = new PubSubmit();
            if (!tSubmit.submitData(mResult, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 准备返回前台统一存储数据 输出：如果发生错误则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        System.out.println("Inputflag" + Inputflag);
        if (Inputflag.equals("1"))
        {
            map.put(mLCUWSendTraceSchema, "INSERT");
            map.put(mLWMissionSchema, "UPDATE");
            map.put(mNewLCGCUWMasterSchema, "UPDATE");
            map.put(mLCGCUWSubSchema, "INSERT");
            map.put(mLCGUWMasterSet, "UPDATE");
            map.put(mLCGUWSubSet, "INSERT");

            map.put(mLDSpotTrackSet, "INSERT");
            mTransferData.setNameAndValue("UserCode", mUserCode);
            mResult.add(mTransferData);
            mResult.add(map);
        }
        else if (Inputflag.equals("2"))
        {
            System.out.println("我第n次进来了!!!");
            mResult.add(map);
        }
        return true;
    }

    /**
     * 校验业务数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        LDSpotUWRateDB tLDSpotUWRateDB = new LDSpotUWRateDB();

        //校验核保师权限表
        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setUserCode(mOperator);

        // 修正对问题件回退权限判断的bug。
        String tUWType = "";
        if ("1".equals(mOtherNoType))
        {
            tUWType = "01";
        }
        else if ("2".equals(mOtherNoType))
        {
            tUWType = "02";
        }
        tLDUWUserDB.setUWType(tUWType);
        // ---------------------------------------

        if (!tLDUWUserDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "checkData";
            tError.errorMessage = "未定义核保师权限!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLDUWUserSchema = tLDUWUserDB.getSchema();
        this.mUserCode = mLDUWUserSchema.getUpUserCode();
        if (mOtherNoType.equals("2"))
        {
            LCGCUWMasterDB tLCGCUWMasterDB = new LCGCUWMasterDB();
            tLCGCUWMasterDB.setProposalGrpContNo(mLCGCUWMasterSchema
                    .getGrpContNo());
            if (!tLCGCUWMasterDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "checkData";
                tError.errorMessage = "查询集体核保主表失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            String newPassFlag = mLCGCUWMasterSchema.getPassFlag();
            String newUWIdea = mLCGCUWMasterSchema.getUWIdea();
            mNewLCGCUWMasterSchema = tLCGCUWMasterDB.getSchema();
            mNewLCGCUWMasterSchema.setPassFlag(newPassFlag);
            mNewLCGCUWMasterSchema.setUWIdea(newUWIdea);

            //校验核保师抽检比例
            tLDSpotUWRateDB.setUserCode(mOperator);
            tLDSpotUWRateDB.setUWType("02"); //团险抽检类型为02
            if (!tLDSpotUWRateDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "checkData";
                tError.errorMessage = "核保师团险抽检比例未定义!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLDSpotUWRateSchema = tLDSpotUWRateDB.getSchema();
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mLCGrpContSet = (LCGrpContSet) cInputData.getObjectByObjectName(
                "LCGrpContSet", 0);
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        if (mOperator == null || mOperator.trim().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mOperator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得管理机构编码
        mManageCom = mGlobalInput.ManageCom;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mOperator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得上报轨迹表数据
        mLCUWSendTraceSchema = (LCUWSendTraceSchema) mTransferData
                .getValueByName("LCUWSendTraceSchema");
        if (mLCUWSendTraceSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mLCUWSendTraceSchema失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得抽检类型标志  1-个险，2-团险
        mOtherNoType = mLCUWSendTraceSchema.getOtherNoType();
        if (mOtherNoType == null
                || mOtherNoType.trim().equals("")
                || (!mOtherNoType.trim().equals("1") && !mOtherNoType.trim()
                        .equals("2")))
        {
            CError tError = new CError();
            tError.moduleName = "UWSendTraceAllBL";
            tError.functionName = "getInputData";
            // @@错误处理
            tError.errorMessage = "前台传输保单类型标志失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLCGrpIssuePolSet = tLCGrpIssuePolDB
                .executeQuery("select * from LCGrpIssuepol where grpcontno='"
                        + mLCGrpContSet.get(1).getGrpContNo()
                        + "' and state<>'5' ");
        if (tLCGrpIssuePolSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tLCGrpIssuePolSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "dealbackstate";
            tError.errorMessage = "没有需要回销的问题件,不能进行回退!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获取外部数据时，考虑到程序的通用型，每种类型写一个getInputData方法。
        if (mOtherNoType.equals("2"))
        { //新契约团险
            if (!getInputGrpData())
            { //获得团险核保主表数据
                return false;
            }
        }
        System.out.println("zhuzhu!!!");
        return true;
    }

    /**
     * getInputGrpData
     * 获得团险数据
     * @return boolean
     */
    private boolean getInputGrpData()
    {
        //获得集体核保主表数据
        mLCGCUWMasterSchema = (LCGCUWMasterSchema) mTransferData
                .getValueByName("LCGCUWMasterSchema");
        if (mLCGCUWMasterSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输集体核保主表数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {

        //准备核保业务数据
        if (!prepareUW())
        {
            return false;
        }

        //准备核保轨迹表数据
        if (!prepareSendTrace())
        {
            return false;
        }
        //准备工作流表
        if (!prepareMission())
        {
            return false;
        }

        return true;
    }

    /**
     * prepareMission
     * 准备工作流轨迹表
     * @return boolean
     */
    private boolean prepareMission()
    {
        LWMissionDB tLWMissionDB = new LWMissionDB();
        if (mOtherNoType.equals("2"))
        {
            tLWMissionDB.setActivityID("0000002004"); //人工核保工作流节点
            tLWMissionDB.setMissionProp1(mLCUWSendTraceSchema.getOtherNo());
        }

        LWMissionSet tLWMissionSet = tLWMissionDB.query();
        if (tLWMissionSet == null || tLWMissionSet.size() <= 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询核保主表失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLWMissionSchema = tLWMissionSet.get(1);
        if (mOtherNoType.equals("2"))
        {
            mLWMissionSchema.setActivityID("0000002004");
            if (findDownuwUser(mOperator).equals(""))
            {
                if (!dealbackstate())
                {
                    return false;
                }
            }
            else
            {
                if (!prepareQuestionSendTrace())
                {
                    return false;
                }
                mLWMissionSchema.setDefaultOperator(findDownuwUser(mOperator));

            }
            mLWMissionSchema.setMissionProp11(this.mOperator);
        }
        return true;
    }

    /**
     * prepareSendTrace
     * 准备核保处理轨迹表数据
     * @return boolean
     */
    private boolean prepareSendTrace()
    {
        int uwno = 0; //核保顺序号
        LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
        //        tLCUWSendTraceDB.setOtherNo(mLCUWSendTraceSchema.getOtherNo());
        //        tLCUWSendTraceDB.setOtherNoType(mOtherNoType);
        StringBuffer sql = new StringBuffer(255);
        sql.append("select * from lcuwsendtrace where otherno='");
        sql.append(mLCUWSendTraceSchema.getOtherNo());
        sql.append("' and othernotype='");
        sql.append(mOtherNoType);
        sql.append("' order by uwno desc");
        LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
        tLCUWSendTraceSet = tLCUWSendTraceDB.executeQuery(sql.toString());
        if (tLCUWSendTraceSet == null || tLCUWSendTraceSet.size() == 0)
        {
            uwno = 1;
            mLCUWSendTraceSchema.setUWCode(mOperator);
            mLCUWSendTraceSchema.setUWPopedom(getPopedom(mOperator));
        }
        else
        {
            uwno = tLCUWSendTraceSet.size() + 1;
            mUserCode = tLCUWSendTraceSet.get(1).getUWCode();
            mLCUWSendTraceSchema.setUWCode(mUserCode);
            mLCUWSendTraceSchema.setUWPopedom(getPopedom(mUserCode));
        }
        mLCUWSendTraceSchema.setUWNo(uwno);
        mLCUWSendTraceSchema.setOperator(mOperator);
        mLCUWSendTraceSchema.setManageCom(mManageCom);
        mLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
        mLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
        mLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
        mLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
        mLCUWSendTraceSchema.setSendFlag("0");
        mLCUWSendTraceSchema.setSendType("5");
        mLCUWSendTraceSchema.setUpUserCode(mOperator);
        mLCUWSendTraceSchema.setUpUWPopedom(getPopedom(mOperator));
        mLCUWSendTraceSchema.setDownUWCode(mOperator);
        mLCUWSendTraceSchema.setDownUWPopedom(getPopedom(mOperator));
        return true;
    }

    /**
     * prepareUW
     * 准备核保业务数据
     * @return boolean
     */
    private boolean prepareUW()
    {
        if (mOtherNoType.equals("2"))
        {
            if (!prepareGrpUW())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * prepareGrpUW
     * 准备团险数据
     * @return boolean
     */
    private boolean prepareGrpUW()
    {
        mNewLCGCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
        mNewLCGCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        mNewLCGCUWMasterSchema.setOperator(mOperator);
        //        mNewLCGCUWMasterSchema.setPassFlag("6"); //上报
        mNewLCGCUWMasterSchema.setUWIdea(mLCUWSendTraceSchema.getUWIdea());
        mNewLCGCUWMasterSchema.setUWNo(mNewLCGCUWMasterSchema.getUWNo() + 1);

        // 合同核保轨迹表
        LCGCUWSubDB tLCGCUWSubDB = new LCGCUWSubDB();
        tLCGCUWSubDB.setGrpContNo(mNewLCGCUWMasterSchema.getGrpContNo());
        LCGCUWSubSet tLCGCUWSubSet = new LCGCUWSubSet();
        tLCGCUWSubSet = tLCGCUWSubDB.query();
        if (tLCGCUWSubDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mLCCUWMasterSchema.getContNo()
                    + "合同核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int nUWNo = tLCGCUWSubSet.size();
        if (nUWNo > 0)
        {
            nUWNo++;
            mLCGCUWSubSchema.setUWNo(nUWNo); //第几次核保
        }
        else
        {
            mLCGCUWSubSchema.setUWNo(1); //第1次核保
        }

        mLCGCUWSubSchema = tLCGCUWSubSet.get(1).getSchema();
        mLCGCUWSubSchema.setUWNo(nUWNo);
        //mLCGCUWSubSchema.setPassFlag("6");
        mLCGCUWSubSchema.setUWIdea(mLCUWSendTraceSchema.getUWIdea());
        mLCGCUWSubSchema.setOperator(mOperator);
        mLCGCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
        mLCGCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
        mLCGCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
        mLCGCUWSubSchema.setModifyTime(PubFun.getCurrentTime());

        LCGUWSubDB tLCGUWSubDB;
        LCGUWSubSchema tLCGUWSubSchema = new LCGUWSubSchema();
        int uwno = 0;
        LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB();
        tLCGUWMasterDB.setGrpContNo(mNewLCGCUWMasterSchema.getGrpContNo());
        mLCGUWMasterSet = tLCGUWMasterDB.query();
        if (mLCGUWMasterSet == null)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = "集体险种核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mLCGUWMasterSet.size(); i++)
        {
            uwno = mLCGUWMasterSet.get(i).getUWNo(); //获得核保序号
            //mLCGUWMasterSet.get(i).setPassFlag("6");
            mLCGUWMasterSet.get(i).setUWIdea(mLCUWSendTraceSchema.getUWIdea());
            mLCGUWMasterSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLCGUWMasterSet.get(i).setUWNo(uwno + 1);
            mLCGUWMasterSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLCGUWMasterSet.get(i).setOperator(mOperator);

            tLCGUWSubDB = new LCGUWSubDB();
            tLCGUWSubDB.setGrpProposalNo(mLCGUWMasterSet.get(i)
                    .getGrpProposalNo());
            tLCGUWSubDB.setGrpPolNo(mLCGUWMasterSet.get(i).getGrpPolNo());
            tLCGUWSubDB.setUWNo(uwno);
            if (!tLCGUWSubDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGCUWSubDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "prepareUW";
                tError.errorMessage = "集体险种核保轨迹表查失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLCGUWSubSchema = tLCGUWSubDB.getSchema();
            //tLCGUWSubSchema.setPassFlag("6");
            tLCGUWSubSchema.setUWIdea(mLCUWSendTraceSchema.getUWIdea());
            tLCGUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGUWSubSchema.setUWNo(uwno + 1);
            tLCGUWSubSchema.setModifyTime(PubFun.getCurrentTime());
            tLCGUWSubSchema.setOperator(mOperator);

            mLCGUWSubSet.add(tLCGUWSubSchema);
        }

        return true;
    }

    /**
     * getPopedom 得到上级核保师级别
     *
     * @param tUserCode String
     * @return String
     */
    private String getPopedom(String tUserCode)
    {
        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setUserCode(tUserCode);
        tLDUWUserDB.getInfo();

        return tLDUWUserDB.getUWPopedom();
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 返回错误
     * @return VData
     */
    public CErrors getErrors()
    {
        return mErrors;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "UWSendBackTraceBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    //取操作员的下级相关信息
    private String findDownuwUser(String UserCode)
    {
        String LastUserCode = "";
        boolean Userflag = false;
        StringBuffer tSBql = new StringBuffer(128);
        tSBql
                .append("select distinct upusercode,downuwcode,uwno from lcuwsendtrace where otherno='"
                        + mLCGCUWMasterSchema.getGrpContNo()
                        + "' and downuwcode<>upusercode and sendtype in ('1','2') order by uwno desc");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
        System.out.println("UserCode" + UserCode);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            if (UserCode.equals(tSSRS.GetText(i, 1)))
            {
                //System.out.println("User"+i+"Code"+tSSRS.GetText(i, 1));
                LastUserCode = tSSRS.GetText(i, 2);
                Userflag = true;
                System.out.println("Userflag11" + Userflag);
                //               System.out.println("LastUserCode"+LastUserCode);
                break;
            }
        }
        System.out.println("User11" + LastUserCode);
        return LastUserCode;
    }

    private boolean dealbackstate()
    {
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LWMissionSet tLWMissionSet = new LWMissionSet();
        tLWMissionDB.setActivityID("0000002004");
        System.out.println("babababba11" + mLCGrpContSet.get(1).getGrpContNo());
        tLWMissionDB.setMissionProp1(mLCGrpContSet.get(1).getGrpContNo());
        tLWMissionSet = tLWMissionDB.query();
        if (tLWMissionSet.size() == 0)
        {
            if (!backstate("2"))
            {//返回录入状态
                System.out.println("返回录入状态");
                return false;
            }
        }
        else
        {
            System.out.println("返回复核状态111");
            if (!backstate("1"))
            { //返回复核状态
                System.out.println("返回复核状态");
                return false;
            }
        }
        return true;
    }

    private boolean backstate(String flag)
    {
        Inputflag = "2";
        System.out.println("start query lcgrpcont" + flag);
        tLCGrpContDB.setGrpContNo(mLCGrpContSet.get(1).getGrpContNo());
        //        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        if (tLCGrpContDB.getInfo() == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWSendBackTraceBL";
            tError.functionName = "dealbackstate";
            tError.errorMessage = "查询团体投保信息出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("end query lcgrpcont");
        LCGrpContSchema tLCGrpContSchema = tLCGrpContDB.getSchema();
        if (StrTool.cTrim(tLCGrpContSchema.getUWFlag()).equals("z"))
        {
            tLCGrpContSchema.setUWFlag("5");
        }
        tLCGrpContSchema.setApproveFlag("5");
        tLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        //更新ApproveFlag
        map.put(tLCGrpContSchema, "UPDATE");
        String tMissionID = (String) mTransferData.getValueByName("MissionID");
        String tSubMissionID = (String) mTransferData
                .getValueByName("SubMissionID");
        String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        //备份lwmission
        System.out.println("start put map lbmission");
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LWMissionSchema tLWMissionSchema = new LWMissionSchema();
        if (flag.equals("1"))
        {
            map.put("insert into lbmission (select '" + tSerielNo
                    + "', lwmission.* from lwmission where MissionID='"
                    + tMissionID + "' and SubMissionID='" + tSubMissionID
                    + "' and ACTIVITYID='0000002004')", "INSERT");
            //恢复到原来的状态
            System.out.println("end put map lbmission");

            System.out.println("start query lwmission");
            tLWMissionDB.setActivityID("0000002004");
            tLWMissionDB.setMissionID(tMissionID);
            tLWMissionDB.setSubMissionID(tSubMissionID);
            if (tLWMissionDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLWMissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "dealbackstate";
                tError.errorMessage = "查询工作流信息出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("end query lwmission");
            tLWMissionSchema = tLWMissionDB.getSchema();
            map.put(tLWMissionSchema, "DELETE");
            System.out.println("end query lwmission");
        }
        else
        {
            System.out.println("我进来了2!!");
            tLWMissionDB.setActivityID("0000002001");
            tLWMissionDB.setMissionID(tMissionID);
            tLWMissionDB.setSubMissionID(tSubMissionID);
            if (tLWMissionDB.getInfo() == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLWMissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "dealbackstate";
                tError.errorMessage = "查询工作流信息出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("end query lwmission");
            tLWMissionSchema = tLWMissionDB.getSchema();
            map.put(tLWMissionSchema, "DELETE");
            System.out.println("end query lwmission");

        }
        System.out.println("出来了！！！");
        if (dealmission(tMissionID, tLWMissionSchema.getMissionProp2(), flag) == false)
        {
            return false;
        }
        return true;
    }

    private boolean dealmission(String cMissionID, String cPrtNo, String cflag)
    {
        if (cflag.equals("1"))
        {
            LBMissionDB tLBMissionDB = new LBMissionDB();
            LBMissionSet tLBMissionSet = new LBMissionSet();
            tLBMissionSet = tLBMissionDB
                    .executeQuery("select * from lbmission where ActivityID='0000002001' and MissionID='"
                            + cMissionID
                            + "' and MissionProp2='"
                            + cPrtNo
                            + "' order by makedate desc,maketime desc");
            if (tLBMissionSet.size() > 0)
            {
                //返回无扫描录入
                LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                tLBMissionSchema = tLBMissionSet.get(1).getSchema();
                cLWMissionSchema.setMissionID(tLBMissionSchema.getMissionID());
                cLWMissionSchema.setSubMissionID(tLBMissionSchema
                        .getSubMissionID());
                cLWMissionSchema.setProcessID(tLBMissionSchema.getProcessID());
                cLWMissionSchema
                        .setActivityID(tLBMissionSchema.getActivityID());
                cLWMissionSchema.setActivityStatus("1");
                cLWMissionSchema.setMissionProp1(tLBMissionSchema
                        .getMissionProp1());
                cLWMissionSchema.setMissionProp2(tLBMissionSchema
                        .getMissionProp2());
                cLWMissionSchema.setMissionProp3(tLBMissionSchema
                        .getMissionProp3());
                cLWMissionSchema.setMissionProp4(tLBMissionSchema
                        .getMissionProp4());
                cLWMissionSchema.setMissionProp5(tLBMissionSchema
                        .getMissionProp5());
                cLWMissionSchema.setMissionProp6(tLBMissionSchema
                        .getMissionProp6());
                cLWMissionSchema.setMissionProp7(tLBMissionSchema
                        .getMissionProp7());
                cLWMissionSchema.setMissionProp8(tLBMissionSchema
                        .getMissionProp8());
                cLWMissionSchema.setMissionProp11(tLBMissionSchema
                        .getMissionProp11());
                cLWMissionSchema.setMissionProp12(tLBMissionSchema
                        .getMissionProp12());
                cLWMissionSchema.setMissionProp20("Y");
                cLWMissionSchema.setLastOperator(mGlobalInput.Operator);
                cLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
                cLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
                cLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
                cLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
                cLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(cLWMissionSchema, "INSERT");
                //                System.out.println("好了！！！！");
                LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
                tLCUWSendTraceSchema.setOtherNo(mLCGrpContSet.get(1)
                        .getGrpContNo());
                tLCUWSendTraceSchema.setOtherNoType("3");
                int uwno = 0; //核保顺序号
                LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
                StringBuffer sql = new StringBuffer(255);
                sql.append("select * from lcuwsendtrace where otherno='");
                sql.append(mLCGrpContSet.get(1).getGrpContNo());
                sql.append("' and othernotype='3' order by uwno desc");
                LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
                tLCUWSendTraceSet = tLCUWSendTraceDB.executeQuery(sql
                        .toString());
                if (tLCUWSendTraceSet == null || tLCUWSendTraceSet.size() == 0)
                {
                    uwno = 1;
                }
                else
                {
                    uwno = tLCUWSendTraceSet.size() + 1;
                }
                tLCUWSendTraceSchema.setUWNo(uwno);
                tLCUWSendTraceSchema.setSendType("4");
                tLCUWSendTraceSchema.setSendFlag("4");
                tLCUWSendTraceSchema.setUpUserCode(mGlobalInput.Operator);
                if (!StrTool.cTrim(tLCGrpContDB.getApproveCode()).equals(""))
                {
                    tLCUWSendTraceSchema.setDownUWCode(tLCGrpContDB
                            .getApproveCode());
                }
                else
                {
                    this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "UWSendBackTraceBL";
                    tError.functionName = "dealbackstate";
                    tError.errorMessage = "复核员为空，不能进行回退操作！";
                    this.mErrors.addOneError(tError);

                }
                tLCUWSendTraceSchema.setUWCode(mGlobalInput.Operator);
                tLCUWSendTraceSchema.setUWIdea("问题件回退");
                tLCUWSendTraceSchema.setManageCom(mManageCom);
                tLCUWSendTraceSchema.setOperator(mGlobalInput.Operator);
                tLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
                tLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
                tLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
                map.put(tLCUWSendTraceSchema, "INSERT");
            }
        }
        else
        {
            System.out.println("here!!!");
            LBMissionDB tLBMissionDB = new LBMissionDB();
            LBMissionSet tLBMissionSet = new LBMissionSet();
            tLBMissionSet = tLBMissionDB
                    .executeQuery("select * from lbmission where ActivityID='0000002098' and MissionID='"
                            + cMissionID
                            + "' and MissionProp1='"
                            + cPrtNo
                            + "' order by makedate desc,maketime desc");
            System.out.println("pig1!!!");
            if (tLBMissionSet.size() > 0)
            {
                //返回无扫描录入
                System.out.println("pig2!");
                LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                //  System.out.println("噢噢噢噢ooo!!");
                tLBMissionSchema = tLBMissionSet.get(1).getSchema();
                //System.out.println("噢噢噢噢ooo!");
                cLWMissionSchema.setMissionID(tLBMissionSchema.getMissionID());
                cLWMissionSchema.setSubMissionID(tLBMissionSchema
                        .getSubMissionID());
                cLWMissionSchema.setProcessID(tLBMissionSchema.getProcessID());
                cLWMissionSchema
                        .setActivityID(tLBMissionSchema.getActivityID());
                cLWMissionSchema.setActivityStatus("1");
                cLWMissionSchema.setMissionProp1(tLBMissionSchema
                        .getMissionProp1());
                cLWMissionSchema.setMissionProp2(PubFun.getCurrentDate());
                cLWMissionSchema.setMissionProp3(tLBMissionSchema
                        .getMissionProp3());
                cLWMissionSchema.setMissionProp4(tLBMissionSchema
                        .getMissionProp4());
                //团体万能保单工作流置为1 ，一般置为0
                ExeSQL tExeSQL = new ExeSQL();
                String verifySql ="select 1  from lcgrppol a where a.prtno='"+tLBMissionSchema
                .getMissionProp1()+"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
                String verifySqlResult = tExeSQL.getOneValue(verifySql);
                if (verifySqlResult.trim().equals("1")) {
                	
                cLWMissionSchema.setMissionProp5("1");//增加团体万能交费通知书
                } else {
                cLWMissionSchema.setMissionProp5("0");
                }
                
                cLWMissionSchema.setMissionProp20("Y");
                System.out.println(mLCGrpContSet.size()
                        + "dddddddddddddddddddd");
                cLWMissionSchema.setLastOperator(mGlobalInput.Operator);
                cLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
                cLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
                cLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
                cLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
                cLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
                System.out.println("pig5");
                map.put(cLWMissionSchema, "INSERT");
                System.out.println("pig!");
            }
            else
            {
                System.out.println("here2");
                //返回扫描录入  ActivityID 0000002099
                tLBMissionSet.clear();
                tLBMissionSet = tLBMissionDB
                        .executeQuery("select * from lbmission where ActivityID='0000002099' and MissionID='"
                                + cMissionID
                                + "' and MissionProp1='"
                                + cPrtNo
                                + "' order by makedate desc,maketime desc");
                if (tLBMissionSet.size() > 0)
                {
                    LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                    tLBMissionSchema = tLBMissionSet.get(1).getSchema();
                    cLWMissionSchema.setMissionID(tLBMissionSchema
                            .getMissionID());
                    cLWMissionSchema.setSubMissionID(tLBMissionSchema
                            .getSubMissionID());
                    cLWMissionSchema.setProcessID(tLBMissionSchema
                            .getProcessID());
                    cLWMissionSchema.setActivityID(tLBMissionSchema
                            .getActivityID());
                    cLWMissionSchema.setActivityStatus("1");
                    cLWMissionSchema.setMissionProp1(tLBMissionSchema
                            .getMissionProp1());
                    cLWMissionSchema.setMissionProp2(tLBMissionSchema
                            .getMissionProp2());
                    cLWMissionSchema.setMissionProp3(tLBMissionSchema
                            .getMissionProp3());
                    cLWMissionSchema.setMissionProp4(tLBMissionSchema
                            .getMissionProp4()); 
                     //团体万能保单工作流置为1 ，一般置为0
                    ExeSQL tExeSQL = new ExeSQL();
                    String verifySql ="select 1  from lcgrppol a where a.prtno='"+tLBMissionSchema
                    .getMissionProp1()+"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
                    String verifySqlResult = tExeSQL.getOneValue(verifySql);
                    if (verifySqlResult.trim().equals("1")) {
                    	
                    cLWMissionSchema.setMissionProp5("1");//增加团体万能交费通知书
                    } else {
                    cLWMissionSchema.setMissionProp5("0");
                    }
                    cLWMissionSchema.setMissionProp20("Y");
                    cLWMissionSchema.setLastOperator(mGlobalInput.Operator);
                    cLWMissionSchema.setCreateOperator(mGlobalInput.Operator);
                    cLWMissionSchema.setMakeDate(PubFun.getCurrentDate());
                    cLWMissionSchema.setMakeTime(PubFun.getCurrentTime());
                    cLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
                    cLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
                    map.put(cLWMissionSchema, "INSERT");

                }
            }
            LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
            tLCUWSendTraceSchema
                    .setOtherNo(mLCGrpContSet.get(1).getGrpContNo());
            tLCUWSendTraceSchema.setOtherNoType("3");
            int uwno = 0; //核保顺序号
            LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
            StringBuffer sql = new StringBuffer(255);
            sql.append("select * from lcuwsendtrace where otherno='");
            sql.append(mLCGrpContSet.get(1).getGrpContNo());
            sql.append("' and othernotype='3' order by uwno desc");
            LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
            tLCUWSendTraceSet = tLCUWSendTraceDB.executeQuery(sql.toString());
            if (tLCUWSendTraceSet == null || tLCUWSendTraceSet.size() == 0)
            {
                uwno = 1;
            }
            else
            {
                uwno = tLCUWSendTraceSet.size() + 1;
            }
            tLCUWSendTraceSchema.setUWNo(uwno);
            tLCUWSendTraceSchema.setSendType("4");
            tLCUWSendTraceSchema.setSendFlag("4");
            tLCUWSendTraceSchema.setUpUserCode(mGlobalInput.Operator);
            if (!StrTool.cTrim(tLCGrpContDB.getInputOperator()).equals(""))
            {
                tLCUWSendTraceSchema.setDownUWCode(tLCGrpContDB
                        .getInputOperator());
            }
            else
            {
                this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "UWSendBackTraceBL";
                tError.functionName = "dealbackstate";
                tError.errorMessage = "录入员为空，不能进行回退操作！";
                this.mErrors.addOneError(tError);

            }
            tLCUWSendTraceSchema.setUWCode(mGlobalInput.Operator);
            tLCUWSendTraceSchema.setUWIdea("问题件回退");
            tLCUWSendTraceSchema.setOperator(mGlobalInput.Operator);
            tLCUWSendTraceSchema.setManageCom(mManageCom);
            tLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(tLCUWSendTraceSchema, "INSERT");
        }
        System.out.println("bigpig");
        return true;
    }

    private boolean prepareQuestionSendTrace()
    {
        int uwno = 0; //核保顺序号
        LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
        StringBuffer sql = new StringBuffer(255);
        sql.append("select * from lcuwsendtrace where otherno='");
        sql.append(mLCUWSendTraceSchema.getOtherNo());
        sql.append("' and othernotype='3' order by uwno desc");
        LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
        tLCUWSendTraceSet = tLCUWSendTraceDB.executeQuery(sql.toString());
        if (tLCUWSendTraceSet == null || tLCUWSendTraceSet.size() == 0)
        {
            uwno = 1;
        }
        else
        {
            uwno = tLCUWSendTraceSet.size() + 1;
        }
        LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
        tLCUWSendTraceSchema.setOtherNo(mLCGrpContSet.get(1).getGrpContNo());
        tLCUWSendTraceSchema.setOtherNoType("3");
        tLCUWSendTraceSchema.setUWNo(uwno);
        tLCUWSendTraceSchema.setOperator(mOperator);
        tLCUWSendTraceSchema.setManageCom(mManageCom);
        tLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
        tLCUWSendTraceSchema.setSendFlag("4");
        tLCUWSendTraceSchema.setSendType("4");
        tLCUWSendTraceSchema.setUpUserCode(mOperator);
        //tLCUWSendTraceSchema.setUpUWPopedom(getPopedom(mOperator));
        tLCUWSendTraceSchema.setDownUWCode(findDownuwUser(mOperator));
        //tLCUWSendTraceSchema.setDownUWPopedom(getPopedom(mOperator));
        tLCUWSendTraceSchema.setUWCode(mOperator);
        tLCUWSendTraceSchema.setUWIdea("问题件回退");
        map.put(tLCUWSendTraceSchema, "INSERT");
        return true;
    }

}
