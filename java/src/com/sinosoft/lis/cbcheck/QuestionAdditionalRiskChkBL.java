package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class QuestionAdditionalRiskChkBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private GlobalInput tGI = new GlobalInput();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private LCIssuePolSet mLCIssuePolSet ;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();

  //业务处理相关变量
  /** 个人信息 */
  private LCIssuePolSchema  mLCIssuePolSchema=new LCIssuePolSchema() ;

  public QuestionAdditionalRiskChkBL()
  {
  }
  public static void main(String[] args)
  {

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    //进行业务处理
    if (!dealData(cOperate))
      return false;

    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    System.out.println("Start QuestionAdditionalRiskChkBLS Submit...");

    QuestionAdditionalRiskChkBLS tQuestionAdditionalRiskChkBLS=new QuestionAdditionalRiskChkBLS();
    tQuestionAdditionalRiskChkBLS.submitData(mInputData,cOperate);

    System.out.println("End QuestionAdditionalRiskChkBLS  Submit...");

    //如果有需要处理的错误，则返回
    if (tQuestionAdditionalRiskChkBLS.mErrors .needDealError())
    {
        mErrors .copyAllErrors(tQuestionAdditionalRiskChkBLS.mErrors ) ;
        return false;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData(String cOperate)
  {

    if (!getInputData())
      return false ;

    if(!queryData())
      return false;

    mLCIssuePolSchema.setBackObj(mLCPolSchema.getManageCom());
    mLCIssuePolSchema.setBackObjType("2");
    mLCIssuePolSchema.setIssueType("99");
    mLCIssuePolSchema.setOperatePos("9");
    mLCIssuePolSchema.setBackObjType("2");
    mLCIssuePolSchema.setNeedPrint("N");
    mLCIssuePolSchema.setReplyResult("添加附加险");
    mLCIssuePolSchema.setReplyMan(tGI.Operator);
    mLCIssuePolSchema.setIsueManageCom(tGI.ComCode);
    mLCIssuePolSchema.setOperator(tGI.Operator);
    mLCIssuePolSchema.setManageCom(tGI.ManageCom);
    mLCIssuePolSchema.setMakeDate(CurrentDate);
    mLCIssuePolSchema.setMakeTime(CurrentTime);
    mLCIssuePolSchema.setModifyDate(CurrentDate);
    mLCIssuePolSchema.setModifyTime(CurrentTime);

    //查询问题键表
    LCIssuePolDB tLCIssuePolDB = new LCIssuePolDB();
//    tLCIssuePolDB.setProposalNo(mLCIssuePolSchema.getProposalNo());
    tLCIssuePolDB.setIssueType(mLCIssuePolSchema.getIssueType());
    tLCIssuePolDB.setOperatePos(mLCIssuePolSchema.getOperatePos());
    if(tLCIssuePolDB.getInfo())
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="QuestionAdditionalRiskChkBL";
      tError.functionName="dealData";
      tError.errorMessage="问题键表中已经存在相同数据!";
      mErrors .addOneError(tError) ;
      return false;
    }

    //更新投保单状态
    mLCPolSchema.setUWFlag("8");
    mLCPolSchema.setModifyDate(CurrentDate);
    mLCPolSchema.setModifyTime(CurrentTime);
    return true ;
  }

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  private boolean getInputData()
  {
    boolean flag=false;
    mLCIssuePolSchema   = (LCIssuePolSchema)mInputData.getObjectByObjectName("LCIssuePolSchema",0);
    tGI = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
    if(mLCIssuePolSchema==null)
    {
      flag=true;
    }
//    if(mLCIssuePolSchema.getProposalNo()==null)
//    {
//      flag=true;
//    }
    if(flag)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="QuestionAdditionalRiskChkBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据!";
      mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  //准备往后层输出所需要的数据
  //输出：如果输入数据有错，则返回false,否则返回true
  private boolean queryData()
  {

      LCPolSchema tLCPolSchema=new LCPolSchema();
//      tLCPolSchema.setPolNo(mLCIssuePolSchema.getProposalNo());//查询投保单
      LCPolDB tLCPolDB=new LCPolDB();
      tLCPolDB.setSchema(tLCPolSchema);
      if(!tLCPolDB.getInfo())
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="QuestionAdditionalRiskChkBL";
        tError.functionName="checkData";
        tError.errorMessage="没有查询到投保单";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      mLCPolSchema=tLCPolDB.getSchema();

      return true;
  }

  public VData getResult()
  {
    return mResult;
   }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    try
    {
      mInputData.clear();
      mInputData.add(mLCIssuePolSchema);
      mInputData.add(mLCPolSchema);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="QuestionAdditionalRiskChkBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

}
