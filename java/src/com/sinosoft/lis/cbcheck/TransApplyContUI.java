package com.sinosoft.lis.cbcheck;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class TransApplyContUI {

	public CErrors mErrors = new CErrors();
	
	/** 往前面传输数据的容器 */
    private String mResult = "";
	
	public boolean submitData(VData cInputData, String cOperate) {
		
		TransApplyContBL tTransApplyContBL = new TransApplyContBL();//BL处理类
		
		if (tTransApplyContBL.submitData(cInputData, cOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tTransApplyContBL.mErrors);
			return false;
		}else {
            mResult = tTransApplyContBL.getResult();
        }
		
		return true;
	}
	
	/**
     * 得到处理后的结果集
     * @return 结果集
     */

    public String getResult()
    {
        return mResult;
    }
	
}
