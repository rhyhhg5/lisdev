package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统承保团体自动核保</p>
 * <p>Description:接口功能类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by ZhangRong  2004.11
 * @version 1.0
 */
public class GrpUWAutoChkUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public GrpUWAutoChkUI()
	{
	}

	/**
	传输数据的公共方法
	*/
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		GrpUWAutoChkBL tGrpUWAutoChkBL = new GrpUWAutoChkBL();

		System.out.println("---GrpUWAutoChkBL UI BEGIN---");

		if (tGrpUWAutoChkBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tGrpUWAutoChkBL.mErrors);

			CError tError = new CError();
			tError.moduleName = "GrpUWAutoChkUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据查询失败!";

			//this.mErrors .addOneError(tError) ;
			showerror(mErrors);
			mResult.clear();

			return false;
		}

		return true;
	}

	public void showerror(CErrors tErrors)
	{
		String terror = "";
		CError tError = new CError();
		int n = tErrors.getErrorCount();

		if (n > 0)
		{
			for (int i = 0; i < n; i++)
			{
				tError = tErrors.getError(i);
				terror = terror + i + ". " + tError.errorMessage.trim() + ".";
			}

			System.out.println("errormessage: " + terror);
		}
	}

	// @Main
	public static void main(String[] args)
	{
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";

		LCGrpContSchema p = new LCGrpContSchema();

		p.setGrpContNo("120110000000275");

		VData tVData = new VData();

		tVData.add(p);
		tVData.add(tG);

		GrpUWAutoChkUI ui = new GrpUWAutoChkUI();

		if (ui.submitData(tVData, "INSERT") == true)
		{
			System.out.println("---ok---");
		}
		else
		{
			System.out.println("---NO---");
		}
	}

}
