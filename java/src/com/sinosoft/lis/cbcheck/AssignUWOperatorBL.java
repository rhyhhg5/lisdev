package com.sinosoft.lis.cbcheck;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AssignUWOperatorBL {
	public CErrors mErrors = new CErrors();

	private String mAction = "";

	private String mModifyUWOperator = "";
	private String mPiType="";
	private String mOtherno="";
	private String mlc="";
	private String mlp="";
	private String mll="";
	private String mlls="";
	private LCContSet mLCContSet = new LCContSet();
	private LPEdorItemSet mLPEdorItemSet=new LPEdorItemSet();
	private LLCaseSet mLLCaseSet=new LLCaseSet();
	private LLCaseSet mLLCaseSets=new LLCaseSet();

	MMap map = new MMap();

	private GlobalInput mGlobalInput = new GlobalInput();

	public AssignUWOperatorBL() {
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "QyModifyUWOperatorBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("UPDATE")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		System.out.println("Begin getInputData");
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("End getInputData");

		// 修改保单核保师
		if ("ModifyUWOperator".equals(mAction)) {
			if(mLCContSet.size()>0)
			{
				String res="n";
				for (int i = 1; i <= mLCContSet.size(); i++) {
					String tOtherno = mLCContSet.get(i).getPrtNo();
					System.out.println("BL层的PrtNo="+tOtherno);
					String tContType = mLCContSet.get(i).getContType();
					if (!ModifyUWOperator(tOtherno, mModifyUWOperator, tContType,res)) {
						buildError("ModifyUWOperator", "修改保单核保师失败！");
						return false;
					}
				}
			}
			 if(mLPEdorItemSet.size()>0)
			{
				 String res="n";
				for (int i = 1; i <= mLPEdorItemSet.size(); i++) {
					String tOtherno = mLPEdorItemSet.get(i).getEdorNo();
					System.out.println("BL层的EdorNo="+tOtherno);
					String tContType = "1";
					if (!ModifyUWOperator(tOtherno, mModifyUWOperator, tContType,res)) {
						buildError("ModifyUWOperator", "修改保单核保师失败！");
						return false;
					}
				}
			}
			 if(mLLCaseSet.size()>0)
			{
				 String res="n";
				for (int i = 1; i <= mLLCaseSet.size(); i++) {
					String tOtherno = mLLCaseSet.get(i).getCaseNo();
					System.out.println("BL层的CaseNo="+tOtherno);
					String tContType =  "1";
					if (!ModifyUWOperator(tOtherno , mModifyUWOperator, tContType,res)) {
						buildError("ModifyUWOperator", "修改保单核保师失败！");
						return false;
					}
				}
			}
			 if(mLLCaseSets.size()>0)
				{
				 String res="y";
					for (int i = 1; i <= mLLCaseSets.size(); i++) {
						String tOtherno = mLLCaseSets.get(i).getCaseNo();
						System.out.println("BL层的CaseNo="+tOtherno);
						String tContType =  "1";
						if (!ModifyUWOperator(tOtherno , mModifyUWOperator, tContType,res)) {
							buildError("ModifyUWOperator", "修改保单核保师失败！");
							return false;
						}
					}
				}
		}
		
        if(map.size()>0){
        	VData vData = new VData();
    		vData.add(map);

    		PubSubmit pubsubmit = new PubSubmit();
    		if (!pubsubmit.submitData(vData, "")) {
    			mErrors.copyAllErrors(pubsubmit.mErrors);
    			return false;
    		}
        }		
		System.out.println("---End pubsubmit---");
		System.out.println("操作成功！");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mlc = (String) cInputData.getObjectByObjectName("String", 9);
		mlp = (String) cInputData.getObjectByObjectName("String", 10);
		mll = (String) cInputData.getObjectByObjectName("String", 11);
		mlls = (String) cInputData.getObjectByObjectName("String", 12);
		String tResult[]={mlc,mlp,mll,mlls};
		System.out.println(mlc);
		System.out.println(mlp);
		System.out.println(mll);
		System.out.println(mlls);
		for(int i = 0; i <tResult.length; i++)
		{
			if("0000001100".equals(tResult[i]))
			{
				mLCContSet = (LCContSet) cInputData.getObjectByObjectName("LCContSet",
						3);
			}
			if("0000001180".equals(tResult[i]))
			{
				mLPEdorItemSet = (LPEdorItemSet) cInputData.getObjectByObjectName("LPEdorItemSet",
						4);
			}
			if("0000001182".equals(tResult[i]))
			{
				mLLCaseSet = (LLCaseSet) cInputData.getObjectByObjectName("LLCaseSet",
						5);
			}
			if("0000001181".equals(tResult[i]))
			{
				mLLCaseSets = (LLCaseSet) cInputData.getObjectByObjectName("LLCaseSet",
						6);
				System.out.println("+++++++++++++++++++++++++-"+mLLCaseSets);
			}
		}
		
		mAction = (String) cInputData.getObjectByObjectName("String", 1);
		
		mOtherno=(String) cInputData.getObjectByObjectName("String", 5);
		mModifyUWOperator = (String) cInputData.getObjectByObjectName("String",
				0);

		if (mLCContSet.size() < 1&&mLPEdorItemSet.size() < 1&&mLLCaseSet.size() < 1&&mLLCaseSets.size() < 1) {
			buildError("getInputData", "获取保单信息失败！");
			return false;
		}

		if (mAction == null || "".equals(mAction)) {
			buildError("getInputData", "获取操作信息失败！");
			return false;
		}

		if ("ModifyUWOperator".equals(mAction)
				&& (mModifyUWOperator == null || "".equals(mModifyUWOperator))) {
			buildError("getInputData", "获取核保师信息失败！");
			return false;
		}

		return true;
	}

	// 修改保单的核保师
	private boolean ModifyUWOperator(String tOtherno, String tModifyUWOperator,
			String tContType,String res) {
		String strSQL = "";
		String updateContSQL = "";

		if ("1".equals(tContType)) 
		{ 
			if("y".equals(res))
			{
				String checkS = "select  1 from lwmission where MissionProp5='"
					+ tOtherno
					+ "'  and (defaultoperator is null or trim(defaultoperator)='')  ";
			SSRS tSSRS = new ExeSQL().execSQL(checkS);
			if (tSSRS.getMaxRow() > 0) 
			     {
				strSQL = "update lwmission set DefaultOperator = '"
						+ tModifyUWOperator + "' " + ", ModifyDate = '"
						+ PubFun.getCurrentDate() + "' " + ", ModifyTime = '"
						+ PubFun.getCurrentTime() + "' "
						+ " where  MissionProp5 = '" + tOtherno +"' ";
			     }
			}
			else
			{
				String checkS = "select  1 from lwmission where MissionProp1='"
					+ tOtherno
					+ "'  and (defaultoperator is null or trim(defaultoperator)='')  ";
			SSRS tSSRS = new ExeSQL().execSQL(checkS);
			if (tSSRS.getMaxRow() > 0) 
			     {
				strSQL = "update lwmission set DefaultOperator = '"
						+ tModifyUWOperator + "' " + ", ModifyDate = '"
						+ PubFun.getCurrentDate() + "' " + ", ModifyTime = '"
						+ PubFun.getCurrentTime() + "' "
						+ " where  MissionProp1 = '" + tOtherno +"' ";
				
/*				updateContSQL = "update lccont set UwOperator = '"
					+ tModifyUWOperator + "' " + ", ModifyDate = '"
					+ PubFun.getCurrentDate() + "' " + ", ModifyTime = '"
					+ PubFun.getCurrentTime() + "' " + " where PrtNo = '"
					+ tOtherno + "' ";*/

			     }
			}
			
		
		
			
		}
			else if ("2".equals(tContType)) 
		{
			strSQL = "update lwmission set DefaultOperator = '"
					+ tModifyUWOperator + "' " + ", ModifyDate = '"
					+ PubFun.getCurrentDate() + "' " + ", ModifyTime = '"
					+ PubFun.getCurrentTime() + "' "
					+ " where MissionProp2 = '" + tOtherno
					+ "' and Activityid = '0000002004' ";

			updateContSQL = "update lcgrpcont set UwOperator = '"
					+ tModifyUWOperator + "' " + ", ModifyDate = '"
					+ PubFun.getCurrentDate() + "' " + ", ModifyTime = '"
					+ PubFun.getCurrentTime() + "' " + " where PrtNo = '"
					+ tOtherno + "' ";
		}

		if ("".equals(strSQL)) {
			System.out.println(tOtherno+"已被申请");
			return true;
		}

		System.out.println("UpdateSQL : " + strSQL);
		System.out.println("ContUpdateSQL : " + updateContSQL);
		map.put(strSQL, "UPDATE");
	//	map.put(updateContSQL, "UPDATE");
		return true;
	}

		
	// 将保单放回核保池
	private boolean ReturnUWPol(String tPrtNo, String tContType) {
		String strSQL = "";
		String updateContSQL = "";
		if ("1".equals(tContType)) {
			strSQL = "update lwmission set DefaultOperator = null "
					+ ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
					+ ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
					+ " where MissionProp1 = '" + tPrtNo
					+ "' and Activityid = '0000001100' ";

			updateContSQL = "update lccont set UwOperator = null "
					+ ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
					+ ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
					+ " where PrtNo = '" + tPrtNo + "' ";
		} else if ("2".equals(tContType)) {
			strSQL = "update lwmission set DefaultOperator = null "
					+ ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
					+ ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
					+ " where MissionProp2 = '" + tPrtNo
					+ "' and Activityid = '0000002004' ";

			updateContSQL = "update lcgrpcont set UwOperator = null "
					+ ", ModifyDate = '" + PubFun.getCurrentDate() + "' "
					+ ", ModifyTime = '" + PubFun.getCurrentTime() + "' "
					+ " where PrtNo = '" + tPrtNo + "' ";
		}

		if ("".equals(strSQL)) {
			buildError("ModifyUWOperator", "生成将保单放回核保池的维护语句失败！");
			return false;
		}

		System.out.println("UpdateSQL : " + strSQL);
		map.put(strSQL, "UPDATE");
		map.put(updateContSQL, "UPDATE");
		return true;
	}

}
