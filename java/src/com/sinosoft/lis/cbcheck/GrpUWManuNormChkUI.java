package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统承保集体人工核保部分</p>
 * <p>Description:接口功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @modified by zhangrong
 * @version 1.0
 */
public class GrpUWManuNormChkUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 往界面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	public GrpUWManuNormChkUI()
	{
	}

	/**
	   传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;

		GrpUWManuNormChkBL tGrpUWManuNormChkBL = new GrpUWManuNormChkBL();

		System.out.println("---GrpUWMaunNormChkUI BEGIN---");

		if (tGrpUWManuNormChkBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tGrpUWManuNormChkBL.mErrors);

			CError tError = new CError();
			tError.moduleName = "GrpUWMaunNormChkUI";
			tError.functionName = "submitData";
			mResult.clear();

			return false;
		}

		return true;
	}

	// @Main
	public static void main(String[] args)
	{
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ComCode = "0101";

		LCPolSchema p = new LCPolSchema();
		LCGrpContSchema g = new LCGrpContSchema();
		LCSpecSchema tLCSpecSchema = new LCSpecSchema();
		LCPremSchema tLCPremSchema = new LCPremSchema();
		LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
		LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();

		p.setProposalNo("86110020030120000213");
		p.setPolNo("86110020030120000213");
		p.setRemark("测试一");
		g.setGrpContNo("86110020040120000104");
		g.setProposalGrpContNo("86110020040120000104");
		g.setRemark("团体测试!");
		tLCSpecSchema.setSpecContent("测试特别约定");
		tLCPremSchema.setDutyCode("001001");
		tLCPremSchema.setPayStartDate("2002-10-18");
		tLCPremSchema.setPayEndDate("2002-10-18");
		tLCPremSchema.setPrem(900);
		tLCUWMasterSchema.setUWIdea("测试三");

		// tLCUWMasterSchema.setPostponeDay(90);
		tLCGCUWMasterSchema.setUWIdea("团体测试一");
		p.setUWFlag("3");
		g.setUWFlag("1");

		LCPolSet tLCPolSet = new LCPolSet();
		LCGrpContSet tLCGrpContSet = new LCGrpContSet();
		LCSpecSet tLCSpecSet = new LCSpecSet();
		LCPremSet tLCPremSet = new LCPremSet();
		LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
		LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();

		tLCPolSet.add(p);
		tLCGrpContSet.add(g);

		//tLCSpecSet.add(tLCSpecSchema);
		//tLCPremSet.add(tLCPremSchema);
		tLCUWMasterSet.add(tLCUWMasterSchema);
		tLCGCUWMasterSet.add(tLCGCUWMasterSchema);

		VData tVData = new VData();
		tVData.add(tLCPolSet);
		tVData.add(tLCGrpContSet);
		tVData.add(tLCSpecSet);
		tVData.add(tLCPremSet);
		tVData.add(tLCUWMasterSet);
		tVData.add(tLCGCUWMasterSet);
		tVData.add(tG);

		GrpUWManuNormChkUI ui = new GrpUWManuNormChkUI();

		if (ui.submitData(tVData, "") == true)
		{
			System.out.println("---ok---");
		}
		else
		{
			System.out.println("---NO---");
		}
	}
}
