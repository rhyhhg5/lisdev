package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统核保功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class ContUWAutoChkBLS {
  //是否存在需要人工核保保单
  int merrno = 0;

  //传输数据类
  private VData mInputData;

  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();

  //
  //private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB();
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

  public ContUWAutoChkBLS() {}

  public static void main(String[] args) {
  }

  /**whn
    //传输数据的公共方法
    public int submitData(Vdata cInputData,String cOperate)
    {
      if(submitDataMain(cInputData,cOperate) == false)
      {
   return 2; //操作失败
      }
      else
      {
   if (merrno>0)
   {
       return 1; //操作成功有单要人工核保
   }
   else
   {
       return 0; //操作成功全部自动通过
   }
      }
    }
    whn**/

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //首先将数据在本类中做一个备份
    mInputData = (VData) cInputData.clone();

    System.out.println("Start GrpUWAutoChkBLS Submit...");
    if (!this.saveData())
      return false;
    System.out.println("End GrpUWAutoChkBLS Submit...");

    mInputData = null;
    return true;
  }

  private boolean saveData() {
    LCContSet mLCContSet = (LCContSet) mInputData.getObjectByObjectName(
        "LCContSet", 0);
    int ctemp = mLCContSet.size();
    System.out.println("Contsizeof" + ctemp);
    LCCUWErrorSet mLCCUWErrorSet = (LCCUWErrorSet) mInputData.
        getObjectByObjectName("LCCUWErrorSet", 0);
    LCCUWMasterSet mLCCUWMasterSet = (LCCUWMasterSet) mInputData.
        getObjectByObjectName("LCCUWMasterSet", 0);
    LCCUWSubSet mLCCUWSubSet = (LCCUWSubSet) mInputData.getObjectByObjectName(
        "LCCUWSubSet", 0);

    LCGrpPolSet mLCGrpPolSet = (LCGrpPolSet) mInputData.getObjectByObjectName(
        "LCGrpPolSet", 0);
    int gtemp = mLCGrpPolSet.size();
    System.out.println("Grpsizeof" + gtemp);
    LCGUWErrorSet mLCGUWErrorSet = (LCGUWErrorSet) mInputData.
        getObjectByObjectName("LCGUWErrorSet", 0);
    LCGUWMasterSet mLCGUWMasterSet = (LCGUWMasterSet) mInputData.
        getObjectByObjectName("LCGUWMasterSet", 0);
    LCGUWSubSet mLCGUWSubSet = (LCGUWSubSet) mInputData.getObjectByObjectName(
        "LCGUWSubSet", 0);

    LCPolSet mLCPolSet = (LCPolSet) mInputData.getObjectByObjectName("LCPolSet",
        0);
    int temp = mLCPolSet.size();
    System.out.println("sizeof" + temp);
    LCUWErrorSet mLCUWErrorSet = (LCUWErrorSet) mInputData.
        getObjectByObjectName("LCUWErrorSet", 0);
    LCUWMasterSet mLCUWMasterSet = (LCUWMasterSet) mInputData.
        getObjectByObjectName("LCUWMasterSet", 0);
    LCUWSubSet mLCUWSubSet = (LCUWSubSet) mInputData.getObjectByObjectName(
        "LCUWSubSet", 0);

    Connection conn = DBConnPool.getConnection();
    //conn = .getDefaultConnection();

    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalFeeBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    try {
      conn.setAutoCommit(false);

      // 删除合同单部分
      int CpolCount = mLCContSet.size();
      for (int j = 1; j <= CpolCount; j++) {
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = (LCContSchema) mLCContSet.get(j);
        /*Lis5.3 upgrade set
         String tContProposalNo = tLCContSchema.getProposalNo();
         */
        String tContProposalNo = tLCContSchema.getProposalContNo();
        String tGUWFlag = tLCContSchema.getUWFlag();

        // 核保主表
        LCCUWMasterDB tLCCUWMasterDB = new LCCUWMasterDB(conn);
        tLCCUWMasterDB.setContNo(tContProposalNo);
        if (tLCCUWMasterDB.deleteSQL() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAtuoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCCUWMaster表删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }
        System.out.println("-----------CBBB--------------");
        // 投保单
        LCContDB tLCContDB = new LCContDB(conn);
        /*Lis5.3 upgrade set
         tLCContDB.setProposalNo( tContProposalNo );
         */
        tLCContDB.setProposalContNo(tContProposalNo);

        if (tLCContDB.deleteSQL() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCContDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAutoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCCont表删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }
        System.out.println("-----------CCCC--------------");
        // 保存部分
        // 合同保单

        tLCContDB.setSchema(tLCContSchema);

        if (tLCContDB.insert() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCContDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAutoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCCont表保存失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }

        System.out.println("-----------CDDD--------------");
        // 核保主表
        LCCUWMasterDBSet tLCCUWMasterDBSet = new LCCUWMasterDBSet(conn);
        tLCCUWMasterDBSet.set(mLCCUWMasterSet);
        int masterno = tLCCUWMasterDBSet.size();
        for (int i = 1; i <= masterno; i++) {
          LCCUWMasterSchema tLCCUWMasterSchema = tLCCUWMasterDBSet.get(i);
//          String masterpol = tLCCUWMasterSchema.getProposalNo();
                    String masterpol="";
          if (masterpol.equals(tContProposalNo)) {
            //LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCCUWMasterDB.setSchema(tLCCUWMasterSchema);
            if (tLCCUWMasterDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCCUWMasterDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCCUWMaster表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------CEEE--------------");

        // 核保子表
        int subno = mLCCUWSubSet.size();
        for (int i = 1; i <= subno; i++) {
          LCCUWSubSchema tLCCUWSubSchema = mLCCUWSubSet.get(i);
          String subpol = tLCCUWSubSchema.getContNo();
          if (subpol.equals(tContProposalNo)) {
            LCCUWSubDB tLCCUWSubDB = new LCCUWSubDB(conn);
            tLCCUWSubDB.setSchema(tLCCUWSubSchema);
            if (tLCCUWSubDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCCUWSubDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCCUWSub表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------CFFF--------------");
        // 核保错误信息表
        LCCUWErrorDBSet tLCCUWErrorDBSet = new LCCUWErrorDBSet(conn);
        tLCCUWErrorDBSet.set(mLCCUWErrorSet);
        int errno = tLCCUWErrorDBSet.size();
        merrno = merrno + errno;
        for (int i = 1; i <= errno; i++) {
          LCCUWErrorSchema tLCCUWErrorSchema = new LCCUWErrorSchema();
          tLCCUWErrorSchema = tLCCUWErrorDBSet.get(i);
          String errpol = tLCCUWErrorSchema.getContNo();
          if (errpol.equals(tContProposalNo)) {
            LCCUWErrorDB tLCCUWErrorDB = new LCCUWErrorDB(conn);
            tLCCUWErrorDB.setSchema(tLCCUWErrorSchema);
            if (tLCCUWErrorDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCCUWErrorDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCCError表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }

      }

      // 删除团单部分
      int gpolCount = mLCGrpPolSet.size();
      for (int j = 1; j <= gpolCount; j++) {
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema = (LCGrpPolSchema) mLCGrpPolSet.get(j);
        String tGrpProposalNo = tLCGrpPolSchema.getGrpProposalNo();
        String tGUWFlag = tLCGrpPolSchema.getUWFlag();

        // 核保主表
        LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB(conn);
        tLCGUWMasterDB.setGrpProposalNo(tGrpProposalNo);
        if (tLCGUWMasterDB.deleteSQL() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAtuoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCGUWMaster表删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }
        System.out.println("-----------GBBB--------------");
        // 投保单
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB(conn);
        tLCGrpPolDB.setGrpProposalNo(tGrpProposalNo);
        if (tLCGrpPolDB.deleteSQL() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAutoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCGrpPol表删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }
        System.out.println("-----------GCCC--------------");
        // 保存部分
        // 团体保单

        //LCPolDBSet tLCPolDBSet = new LCPolDBSet( conn );
        //tLCPolDBSet.set( mLCPolSet );
        //if (tLCPolDBSet.insert() == false)
        //LCPolDB tLCPolDB = new LCPolDB();
        tLCGrpPolDB.setSchema(tLCGrpPolSchema);

        if (tLCGrpPolDB.insert() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAutoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCGrpPol表保存失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }

        System.out.println("-----------GDDD--------------");
        // 核保主表
        LCGUWMasterDBSet tLCGUWMasterDBSet = new LCGUWMasterDBSet(conn);
        tLCGUWMasterDBSet.set(mLCGUWMasterSet);
        int masterno = tLCGUWMasterDBSet.size();
        for (int i = 1; i <= masterno; i++) {
          LCGUWMasterSchema tLCGUWMasterSchema = tLCGUWMasterDBSet.get(i);
          String masterpol = tLCGUWMasterSchema.getGrpProposalNo();
          if (masterpol.equals(tGrpProposalNo)) {
            //LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCGUWMasterDB.setSchema(tLCGUWMasterSchema);
            if (tLCGUWMasterDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCGUWMaster表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------GEEE--------------");

        // 核保子表
        //LCGUWSubDBSet tLCGUWSubDBSet = new LCGUWSubDBSet( conn );
        //tLCGUWSubDBSet.set( mLCGUWSubSet );
        int subno = mLCGUWSubSet.size();
        for (int i = 1; i <= subno; i++) {
          LCGUWSubSchema tLCGUWSubSchema = mLCGUWSubSet.get(i);
          String subpol = tLCGUWSubSchema.getGrpPolNo();
          if (subpol.equals(tGrpProposalNo)) {
            LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB(conn);
            tLCGUWSubDB.setSchema(tLCGUWSubSchema);
            if (tLCGUWSubDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCGUWSub表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------GFFF--------------");
        // 核保错误信息表
        LCGUWErrorDBSet tLCGUWErrorDBSet = new LCGUWErrorDBSet(conn);
        tLCGUWErrorDBSet.set(mLCGUWErrorSet);
        int errno = tLCGUWErrorDBSet.size();
        merrno = merrno + errno;
        for (int i = 1; i <= errno; i++) {
          LCGUWErrorSchema tLCGUWErrorSchema = new LCGUWErrorSchema();
          tLCGUWErrorSchema = tLCGUWErrorDBSet.get(i);
          String errpol = tLCGUWErrorSchema.getGrpPolNo();
          if (errpol.equals(tGrpProposalNo)) {
            LCGUWErrorDB tLCGUWErrorDB = new LCGUWErrorDB(conn);
            tLCGUWErrorDB.setSchema(tLCGUWErrorSchema);
            if (tLCGUWErrorDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCGUWErrorDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCGError表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }

      }

      // 删除个单部分
      int polCount = mLCPolSet.size();
      for (int j = 1; j <= polCount; j++) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema = (LCPolSchema) mLCPolSet.get(j);
        String tProposalNo = tLCPolSchema.getProposalNo();
        String tUWFlag = tLCPolSchema.getUWFlag();

        // 核保主表
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB(conn);
        tLCUWMasterDB.setProposalNo(tProposalNo);
        if (tLCUWMasterDB.deleteSQL() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAtuoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCUWMaster表删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }
        System.out.println("-----------BBB--------------");
        // 投保单
        LCPolDB tLCPolDB = new LCPolDB(conn);
        tLCPolDB.setProposalNo(tProposalNo);
        if (tLCPolDB.deleteSQL() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAutoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCPol表删除失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }
        System.out.println("-----------CCC--------------");
        // 保存部分
        // 个人保单

        //LCPolDBSet tLCPolDBSet = new LCPolDBSet( conn );
        //tLCPolDBSet.set( mLCPolSet );
        //if (tLCPolDBSet.insert() == false)
        //LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setSchema(tLCPolSchema);

        if (tLCPolDB.insert() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "ContUWAutoChkBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "LCPol表保存失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
        }

        System.out.println("-----------DDD--------------");
        // 核保主表
        LCUWMasterDBSet tLCUWMasterDBSet = new LCUWMasterDBSet(conn);
        tLCUWMasterDBSet.set(mLCUWMasterSet);
        int masterno = tLCUWMasterDBSet.size();
        for (int i = 1; i <= masterno; i++) {
          LCUWMasterSchema tLCUWMasterSchema = tLCUWMasterDBSet.get(i);
          String masterpol = tLCUWMasterSchema.getProposalNo();
          if (masterpol.equals(tProposalNo)) {
            //LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
            tLCUWMasterDB.setSchema(tLCUWMasterSchema);
            if (tLCUWMasterDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCUWMaster表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------EEE--------------");

        // 核保子表
        LCUWSubDBSet tLCUWSubDBSet = new LCUWSubDBSet(conn);
        tLCUWSubDBSet.set(mLCUWSubSet);
        int subno = tLCUWSubDBSet.size();
        for (int i = 1; i <= subno; i++) {
          LCUWSubSchema tLCUWSubSchema = tLCUWSubDBSet.get(i);
          String subpol = tLCUWSubSchema.getProposalNo();
          if (subpol.equals(tProposalNo)) {
            LCUWSubDB tLCUWSubDB = new LCUWSubDB(conn);
            tLCUWSubDB.setSchema(tLCUWSubSchema);
            if (tLCUWSubDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCUWSub表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }
        System.out.println("-----------FFF--------------");
        // 核保错误信息表
        LCUWErrorDBSet tLCUWErrorDBSet = new LCUWErrorDBSet(conn);
        tLCUWErrorDBSet.set(mLCUWErrorSet);
        int errno = tLCUWErrorDBSet.size();
        merrno = merrno + errno;
        for (int i = 1; i <= errno; i++) {
          LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
          tLCUWErrorSchema = tLCUWErrorDBSet.get(i);
          String errpol = tLCUWErrorSchema.getPolNo();
          if (errpol.equals(tProposalNo)) {
            LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB(conn);
            tLCUWErrorDB.setSchema(tLCUWErrorSchema);
            if (tLCUWErrorDB.insert() == false) {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ContUWAutoChkBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "LCErr表保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
            }
          }
        }

        conn.commit();

      } // end of for
      conn.close();
    } // end of try
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ContUWAutoChkBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
      }
      catch (Exception e) {}
      return false;
    }

    return true;
  }

}
