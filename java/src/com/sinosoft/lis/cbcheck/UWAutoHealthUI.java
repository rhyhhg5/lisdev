package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统体检资料录入部分</p>
 * <p>Description:承保个人单自动核保功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class UWAutoHealthUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public UWAutoHealthUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ComCode = "86";

  LCContSchema p = new LCContSchema();
  p.setProposalContNo( "130110000000914" );
  p.setContNo("130110000000914");

  LCContSet tLCContSet = new LCContSet();
  tLCContSet.add(p);

  //
  LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();

  tLCPENoticeSchema.setContNo("130110000000914");
  tLCPENoticeSchema.setProposalContNo("130110000000914");
  tLCPENoticeSchema.setCustomerNo("0000008080");
  tLCPENoticeSchema.setPEAddress("11003001");
  tLCPENoticeSchema.setPEDate("2005-01-27");
  tLCPENoticeSchema.setPEBeforeCond("N");
  tLCPENoticeSchema.setRemark("TEST");

  LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
  tLCPENoticeSet.add(tLCPENoticeSchema);

  //
  LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema();

  tLCPENoticeItemSchema.setPEItemCode("001");
  tLCPENoticeItemSchema.setPEItemName("普通体检");
  tLCPENoticeItemSchema.setFreePE("N");

  LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  tLCPENoticeItemSchema = new LCPENoticeItemSchema();
  tLCPENoticeItemSchema.setPEItemCode("002");
  tLCPENoticeItemSchema.setPEItemName("X光");
  tLCPENoticeItemSchema.setFreePE("N");
  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);

  VData tVData = new VData();
  tVData.add( tLCContSet );
  tVData.add( tLCPENoticeSet);
  tVData.add( tLCPENoticeItemSet);
  tVData.add( tG );
  UWAutoHealthUI ui = new UWAutoHealthUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    UWAutoHealthBL tUWAutoHealthBL = new UWAutoHealthBL();

    System.out.println("---UWAutoHealthBL UI BEGIN---");
    if (tUWAutoHealthBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tUWAutoHealthBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "UWAutoHealthUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
	}
    return true;
  }
}