package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: 人工核保险种结论 </p>
 * <p>Description: 人工核保险种结论保存 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class ManuUWRiskSaveBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private VData mInputData;

    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    private String mOperator;

    private String mManageCom;

    private int retPayEndYear = 0;

    private String retPayEndYearFlag = "";

    private TransferData mTransferData = new TransferData();

    /** 业务操作类 */
    private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();

    private LCPolSchema mLCPolSchema = new LCPolSchema();

    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();

    private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();

    /** 业务操作字符串 */
    private String mUWFlag = "";

    private String mUWIdea = "";

    private String mSugPassFlag;

    private String mSugUWIdea;

    private String mAddPrem;

    private String mSpecReason;

    private String mCustomerReply;

    private String mAddFeeType;

	private String mRejectReason;
    /**
     * 加费相关字段
     */
    /**
     * mAddFeeFlag 如果前台传入的标志是L
     * 则代表加费操作
     */
    private String mAddFeeFlag = "";

    /**
     * 前台传入的加费数据
     */
    private LCPremSet mLCPremSet = null;

    /**
     * 前台传入的评点加费数据
     */
    private LCUWDiseaseCheckInfoSet mLCUWDiseaseCheckInfoSet = null;

    /**
     * 免责标记 如果为E表示免责
     */
    private String mSpecFlag = "";

    /**
     * 接受前台传入的免责信息
     */
    private LCSpecSet mLCSpecSet = null;

    /**
     * 降档处理相关字段
     */
    private String LoadFlag = "";

    private LCDutySet mLCDutySet = null;

    private VData subVData = null;

    /** 延期时间 */
    private String mPostponeDay;

    /** 延期天数 */
    private String mPostponeDate;

    /** 延期描述 */
    private String mPostponeDescribe;

    public ManuUWRiskSaveBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("After getinputdata");
        if (!checkData())
        {
            return false;
        }
        System.out.println("After checkData");
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("After prepareOutputData");
        System.out.println("Start ManuUWRiskSaveBL Submit...");
        PubSubmit tSubmit = new PubSubmit();
        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("ManuUWRiskSaveBL end");

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        MMap map = new MMap();
        if (!mUWFlag.equals("L") && !mUWFlag.equals("E") && !StrTool.cTrim(this.mAddFeeFlag).equals("L")
                && !StrTool.cTrim(this.mSpecFlag).equals("E"))
        {
            if (this.subVData != null)
            {
                MMap tempMap = (MMap) subVData.getObjectByObjectName("MMap", 0);
                if (tempMap.size() > 0)
                {
                    map.add(tempMap);
                }
            }
            else
            {
                map.put(mLCPolSchema, "UPDATE");
            }
            map.put(mLCUWMasterSchema, "UPDATE");
            map.put(mLCUWSubSchema, "INSERT");
        }

        mResult.add(map);

        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("in dealData!");
        if (SysConst.MAXNOTYPE.equals("Picch"))
        {
            if (mUWFlag.equals("4") && StrTool.cTrim(this.mAddFeeFlag).equals("L"))
            { //加费
                if (!prepareAdd())
                {
                    return false;
                }
            }
            if (mUWFlag.equals("4") && StrTool.cTrim(this.mSpecFlag).equals("E"))
            { //免责
                if (!prepareSpec())
                {
                    return false;
                }
            }
            if (mUWFlag.equals("4")
                    && (StrTool.cTrim(this.LoadFlag).equals("M") || StrTool.cTrim(this.LoadFlag).equals("A")))
            { //降档
                if (!prepareSubMult())
                {
                    return false;
                }
            }
            if (mUWFlag.equals("4") && (StrTool.cTrim(this.LoadFlag).equals("C")))
            { //变更缴费期间
                if (!prepareChPayEndYear())
                {
                    return false;
                }
            }
        }
        if (!mUWFlag.equals("L") && !mUWFlag.equals("E") && !StrTool.cTrim(mAddFeeFlag).equals("L")
                && !StrTool.cTrim(mSpecFlag).equals("E"))
        {
            mLCPolSchema.setUWFlag(mUWFlag);
            mLCPolSchema.setUWCode(mOperator);
            mLCPolSchema.setUWDate(PubFun.getCurrentDate());
            mLCPolSchema.setUWTime(PubFun.getCurrentTime());
            //每次核保后uwno加1
            int uwno = mLCUWMasterSchema.getUWNo() + 1;
            //如果是险种拒保 应将Cont表中的保费减去。
            //            if (this.mUWFlag.equals("1")) {
            //                dealContInfo();
            //            }
            if (StrTool.cTrim(this.LoadFlag).equals("M"))
            {
                mLCUWMasterSchema.setSubMultFlag("1");
                if (mLCUWMasterSchema.getMult() > 0)
                {
                    if (((mLCUWMasterSchema.getMult()) - (mLCDutySet.get(1).getMult())) == 0)
                    {
                        mLCUWMasterSchema.setSubMultFlag("0");
                        mLCUWMasterSchema.setMult(0);
                    }
                }
                else
                {
                    if (((mLCPolSchema.getMult()) - (mLCDutySet.get(1).getMult()) == 0))
                    {
                        mLCUWMasterSchema.setSubMultFlag("0");
                        mLCUWMasterSchema.setMult(0);
                    }
                    else
                    {
                        mLCUWMasterSchema.setMult(this.mLCPolSchema.getMult());
                    }
                }
            }
            if (StrTool.cTrim(this.LoadFlag).equals("A"))
            {
                mLCUWMasterSchema.setSubAmntFlag("1");
                if (mLCUWMasterSchema.getAmnt() > 0)
                {
                    if (((mLCUWMasterSchema.getAmnt()) - (mLCDutySet.get(1).getAmnt())) == 0)
                    {
                        mLCUWMasterSchema.setSubAmntFlag("0");
                        mLCUWMasterSchema.setAmnt(0);
                    }
                }
                else
                {
                    if (((mLCPolSchema.getAmnt()) - (mLCDutySet.get(1).getAmnt())) == 0)
                    {
                        mLCUWMasterSchema.setSubAmntFlag("0");
                        mLCUWMasterSchema.setAmnt(0);
                    }
                    else
                    {
                        mLCUWMasterSchema.setAmnt(this.mLCPolSchema.getAmnt());
                    }
                }
            }
            if (StrTool.cTrim(this.LoadFlag).equals("C"))
            {//如果是变更保险期间的话，
                mLCUWMasterSchema.setChPayEndYearFlag("1");
                if (mLCUWMasterSchema.getPayEndYear() > 0)
                {
                    if ((((mLCUWMasterSchema.getPayEndYear()) - (mLCDutySet.get(1).getPayEndYear())) == 0)
                            && (mLCUWMasterSchema.getPayEndYearFlag().equals(mLCDutySet.get(1).getPayEndYearFlag())))
                    {
                        mLCUWMasterSchema.setChPayEndYearFlag("0");
                        mLCUWMasterSchema.setPayEndYear(0);
                        mLCUWMasterSchema.setPayEndYearFlag("");
                    }
                }
                else
                {
                    if ((((mLCPolSchema.getPayEndYear()) - (mLCDutySet.get(1).getPayEndYear())) == 0)
                            && (mLCPolSchema.getPayEndYearFlag().equals(mLCDutySet.get(1).getPayEndYearFlag())))
                    {
                        mLCUWMasterSchema.setChPayEndYearFlag("0");
                        mLCUWMasterSchema.setPayEndYear(0);
                        mLCUWMasterSchema.setPayEndYearFlag("");
                    }
                    else
                    {
                        mLCUWMasterSchema.setPayEndYear(this.mLCPolSchema.getPayEndYear());
                        mLCUWMasterSchema.setPayEndYearFlag(this.mLCPolSchema.getPayEndYearFlag());
                    }
                }
                //开始最后判断,如果核保轨迹表中的值和lcpol中的值相同的话也去掉核保表中的payendyearflag
                if ((mLCUWMasterSchema.getPayEndYearFlag().equals(this.retPayEndYearFlag))
                        && (mLCUWMasterSchema.getPayEndYear() - this.retPayEndYear == 0))
                {
                    mLCUWMasterSchema.setChPayEndYearFlag("0");
                    mLCUWMasterSchema.setPayEndYear(0);
                    mLCUWMasterSchema.setPayEndYearFlag("");
                }

            }
            //判断险种结论是否是延期
            if (mUWFlag.equals("8"))
            {
                mLCUWMasterSchema.setPostponeDate(mPostponeDate);
                mLCUWMasterSchema.setPostponeDay(mPostponeDay);
                mLCUWMasterSchema.setPostponeDescribe(mPostponeDescribe);
                mLCUWMasterSchema.setCustomerReply("");//去掉撤销申请的原因
                mLCUWMasterSchema.setRejectReason("");//去掉谢绝承保的原因
            }
            else
            {
                mLCUWMasterSchema.setPostponeDate("");
                mLCUWMasterSchema.setPostponeDay("");
                mLCUWMasterSchema.setPostponeDescribe("");
            }
            //判断险种结论是否是撤销申请
            if (mUWFlag.equals("a"))
            {
                mLCUWMasterSchema.setCustomerReply(this.mCustomerReply);
                mLCUWMasterSchema.setPostponeDate("");//去掉原来的延期信息
                mLCUWMasterSchema.setPostponeDay("");//去掉原来的延期信息
                mLCUWMasterSchema.setPostponeDescribe("");
                mLCUWMasterSchema.setRejectReason("");//去掉谢绝承保的原因
            }
            else
            {
                mLCUWMasterSchema.setCustomerReply("");
            }
            //判断险种结论是否是谢绝承保
            if (mUWFlag.equals("1"))
            {
            	mLCUWMasterSchema.setRejectReason(this.mRejectReason);
                mLCUWMasterSchema.setCustomerReply("");//去掉撤销申请的原因
                mLCUWMasterSchema.setPostponeDate("");//去掉原来的延期信息
                mLCUWMasterSchema.setPostponeDay("");//去掉原来的延期信息
                mLCUWMasterSchema.setPostponeDescribe("");
            }
            else
            {
            	mLCUWMasterSchema.setRejectReason("");
            }
            mLCUWMasterSchema.setPassFlag(mUWFlag);
            mLCUWMasterSchema.setUWIdea(mUWIdea);
            mLCUWMasterSchema.setSugPassFlag(mSugPassFlag);
            mLCUWMasterSchema.setSugUWIdea(mSugUWIdea);
            System.out.println("在程序中测试　：" + mCustomerReply);
            mLCUWMasterSchema.setUWNo(uwno); //表示核保次数的序列号
            mLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            mLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
            mLCUWMasterSchema.setOperator(mOperator); //操作员

            if (!LCUWSub())
            {
                return false;
            }

        }

        return true;
    }

    /**
     * prepareSpec
     * picc特约录入
     * @return boolean
     */
    private boolean prepareSpec()
    {
        //        if (mSpecReason == null || mSpecReason.length() <= 0) {
        //            // @@错误处理
        //            CError tError = new CError();
        //            tError.moduleName = "ManuUWRiskSaveBL";
        //            tError.functionName = "prepareSpec";
        //            tError.errorMessage = "特别约定为录入!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }

        //轨迹表
        if (!LCUWSub())
        {
            return false;
        }
        //        if (this.mLCSpecSet.size() <= 0) {
        //            System.out.println(
        //                    "ManuUWRiskSaveBL.prepareSpec()  \n--Line:208  --Author:YangMing");
        //            CError tError = new CError();
        //            tError.moduleName = "ManuUWRiskSaveBL";
        //            tError.functionName = "prepareSpec";
        //            tError.errorMessage = "未录入特别约定!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(this.mLCUWMasterSchema.getPolNo());
        if (!tLCPolDB.getInfo())
        {
            System.out.println("ManuUWRiskSaveBL.prepareSpec()  \n--Line:282  --Author:YangMing");
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);
            return false;
        }
        String StartDate = tLCPolDB.getCValiDate();
        String EndDate = tLCPolDB.getEndDate();
        for (int i = 1; i <= this.mLCSpecSet.size(); i++)
        {
            mLCSpecSet.get(i).setContNo(mLCPolSchema.getContNo());
            mLCSpecSet.get(i).setPolNo(mLCPolSchema.getPolNo());
            mLCSpecSet.get(i).setProposalNo(mLCPolSchema.getProposalNo());
            mLCSpecSet.get(i).setGrpContNo(mLCPolSchema.getGrpContNo());
            if (StrTool.cTrim(this.mLCSpecSet.get(i).getSpecCode()).equals("")
                    && StrTool.cTrim(this.mLCSpecSet.get(i).getSpecContent()).equals(""))
            {

                CError tError = new CError();
                tError.moduleName = "ManuUWRiskSaveBL";
                tError.functionName = "prepareSpec";
                tError.errorMessage = "特约代码和特约内容不能同时为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else if (StrTool.cTrim(this.mLCSpecSet.get(i).getSpecContent()).equals(""))
            {
                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("speccode");
                tLDCodeDB.setCode(this.mLCSpecSet.get(i).getSpecCode());
                if (!tLDCodeDB.getInfo())
                {
                    System.out.println("ManuUWRiskSaveBL.prepareSpec()  \n--Line:239  --Author:YangMing");
                    CError tError = new CError();
                    tError.moduleName = "ManuUWRiskSaveBL";
                    tError.functionName = "prepareSpec";
                    tError.errorMessage = "未查询到特约代码!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LDCodeSchema tLDCodeSchema = tLDCodeDB.getSchema();
                this.mLCSpecSet.get(i).setSpecContent(tLDCodeSchema.getCodeName());
            }
            else if (StrTool.cTrim(this.mLCSpecSet.get(i).getSpecCode()).equals(""))
            {
                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("speccode");
                tLDCodeDB.setCodeName(this.mLCSpecSet.get(i).getSpecContent());
                LDCodeSet tLDCodeSet = tLDCodeDB.query();
                if (tLDCodeSet != null)
                {
                    if (tLDCodeSet.size() > 0)
                    {
                        this.mLCSpecSet.get(i).setSpecCode(tLDCodeSet.get(1).getCode());
                    }
                    if (tLDCodeSet.size() == 0)
                    {
                        //为查询到特约代码生成一个临时特约代码
                        String SpecCode = PubFun1.CreateMaxNo("SpecCode", 5);
                        this.mLCSpecSet.get(i).setSpecCode(SpecCode);
                    }
                }
                else
                {
                    String SpecCode = PubFun1.CreateMaxNo("SpecCode", 5);
                    this.mLCSpecSet.get(i).setSpecCode(SpecCode);
                }
                LDCodeSchema tLDCodeSchema = tLDCodeDB.getSchema();
                this.mLCSpecSet.get(i).setSpecContent(tLDCodeSchema.getCodeName());
            }
            if (StrTool.cTrim(this.mLCSpecSet.get(i).getSpecStartDate()).equals(""))
            {
                this.mLCSpecSet.get(i).setSpecStartDate(StartDate);
            }
            //            if (StrTool.cTrim(this.mLCSpecSet.get(i).getSpecEndDate()).equals("")) {
            //                this.mLCSpecSet.get(i).setSpecEndDate(EndDate);
            //            }
        }
        // LCSpecSchema tLCSpecSchema = new LCSpecSchema();
        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();

        tLCUWMasterSchema.setSpecReason(mLCUWMasterSchema.getUWIdea());

        VData tVData = new VData();
        //tVData.add(tLCSpecSchema);
        tVData.add(mLCSpecSet);
        tVData.add(tLCUWMasterSchema);
        tVData.add(mLCUWSubSchema); //轨迹表
        tVData.add(mLCPolSchema.getPolNo());
        tVData.add(tGI);
        UWSpecUI tUWSpecUI = new UWSpecUI();
        tUWSpecUI.submitData(tVData, "");
        if (tUWSpecUI.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tUWSpecUI.mErrors);
            return false;
        }
        return true;
    }

    /**
     * prepareAdd
     * picch下险种结论时的加费处理
     * @return boolean
     */
    private boolean prepareAdd()
    {
        if (this.mLCPremSet == null)
        {
            System.out.println("ManuUWRiskSaveBL.prepareAdd()  \n--Line:289  --Author:YangMing");
            System.out.println("检查Save页面封装LCPrem时是否有错！");
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "prepareAdd";
            tError.errorMessage = "加费操作,但传入的加费数据为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tAddReason = mUWIdea;
        LCPremSchema tLCPremSchema = new LCPremSchema();
        LCPremSet tLCPremSet = new LCPremSet();
        String tSql = "select * from lcduty where 1=1 " + " and polno = '" + mLCPolSchema.getPolNo() + "'";
        LCDutyDB tLCDutyDB = new LCDutyDB();
        LCDutySet tLCDutySet = new LCDutySet();
        tLCDutySet = tLCDutyDB.executeQuery(tSql);
        if (tLCDutySet == null || tLCDutySet.size() <= 0)
        {
            System.out.println("ManuUWRiskSaveBL.prepareAdd()  \n--Line:310  --Author:YangMing");
            System.out.println("检查Save的PolNo是否传入！");
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "prepareAdd";
            tError.errorMessage = "查询责任表失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //轨迹表
        if (!LCUWSub())
        {
            return false;
        }

        //需求待定
        if (tLCDutySet.size() > 1)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "prepareAdd";
            tError.errorMessage = "目前只支持险种下单个责任的产品加费!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //        tLCPremSchema.setPrem(mAddPrem);
        //        tLCPremSchema.setPolNo(mLCPolSchema.getPolNo());
        //        tLCPremSchema.setDutyCode(tLCDutySet.get(1).getDutyCode());
        //        tLCPremSchema.setPayStartDate(tLCDutySet.get(1).getFirstPayDate());
        //        tLCPremSchema.setPayPlanType("1"); //健康加费
        //        tLCPremSchema.setPayEndDate(tLCDutySet.get(1).getPayEndDate());
        for (int i = 1; i <= this.mLCPremSet.size(); i++)
        {
            this.mLCPremSet.get(i).setPolNo(mLCPolSchema.getPolNo());
            this.mLCPremSet.get(i).setDutyCode(tLCDutySet.get(1).getDutyCode());
            if (StrTool.cTrim(this.mLCPremSet.get(i).getPayStartDate()).equals(""))
            {
                this.mLCPremSet.get(i).setPayStartDate(tLCDutySet.get(1).getFirstPayDate());
            }
            //            if (StrTool.cTrim(this.mLCPremSet.get(i).getPayEndDate()).equals("")) {
            //                this.mLCPremSet.get(i).setPayEndDate(tLCDutySet.get(1).
            //                        getPayEndDate());
            //            }
        }
        /**
         * 如果是传入的保费项是0 表示取消加费
         */
        String CancelAddFeeFlag = "";
        if (mAddFeeType.equals("1"))
        {//评点加费
            if (mLCPremSet.size() == 0)
            {
                CancelAddFeeFlag = "DELETE||ADDFEE";
            }

        }
        else if (mAddFeeType.equals("2"))
        {
            if (mLCUWDiseaseCheckInfoSet.size() == 0)
            {
                CancelAddFeeFlag = "DELETE||ADDFEE";
            }

        }
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AddFeeType", mAddFeeType);
        VData tInputData = new VData();
        tInputData.add(tGI);
        tInputData.add(mLCPolSchema);
        tInputData.add(mLCUWSubSchema); //轨迹表
        tInputData.add(tAddReason);
        tInputData.add(tTransferData);
        tInputData.add(this.mLCPremSet);
        tInputData.add(this.mLCUWDiseaseCheckInfoSet);

        UWManuAddChkUI tUWManuAddChkUI = new UWManuAddChkUI();

        tUWManuAddChkUI.submitData(tInputData, CancelAddFeeFlag);
        if (tUWManuAddChkUI.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tUWManuAddChkUI.mErrors);
            return false;
        }

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(mLCUWMasterSchema.getPolNo());
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setProposalNo(mLCUWMasterSchema.getProposalNo());

        if (!tLCPolDB.getInfo())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询险种保单表失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCPolSchema = tLCPolDB.getSchema();

        if (!tLCUWMasterDB.getInfo())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询险种核保险种表失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCUWMasterSchema = tLCUWMasterDB.getSchema();
        if (mUWFlag != null && !"".equals(mUWFlag))
        {
            mLCUWMasterSchema.setPassFlag(mUWFlag);
            mLCUWMasterSchema.setUWIdea(mSpecReason);
            //chenhq
        }
        /**
         * 校验该险种由变更承保变为其他（正常承保或者撤销或者延期）
         *
         */
        System.out.println("mUWFlag is ：" + mUWFlag);
        if (!mUWFlag.equals("4"))
        {
            if ((StrTool.cTrim(mLCUWMasterSchema.getAddPremFlag()).equals("1"))
                    || (StrTool.cTrim(mLCUWMasterSchema.getSpecFlag()).equals("1"))
                    || (StrTool.cTrim(mLCUWMasterSchema.getSubAmntFlag()).equals("1"))
                    || (StrTool.cTrim(mLCUWMasterSchema.getSubMultFlag()).equals("1"))
                    || (StrTool.cTrim(mLCUWMasterSchema.getChPayEndYearFlag()).equals("1")))
            {
                CError tError = new CError();
                tError.moduleName = "ManuUWRiskSaveBL";
                tError.functionName = "checkData";
                tError.errorMessage = "该单还有[加费]或[免责]或[变更档次]或[变更保额]或[变更缴费期间]的结论，请在“变更承保”中将这些信息去掉后再下险种结论！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (mUWFlag.equals("4"))
        {
            if (StrTool.cTrim(this.LoadFlag).equals("M"))
            {
                if (StrTool.cTrim(String.valueOf(this.mLCDutySet.get(1).getMult())).equals("0.0")
                        || StrTool.cTrim(String.valueOf(this.mLCDutySet.get(1).getMult())).equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "ManuUWRiskSaveBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "执行“降档”操作时请录入降至档次。如果需要恢复原档次，请在降至档次中录入原档次！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            if (StrTool.cTrim(this.LoadFlag).equals("A"))
            {
                if (StrTool.cTrim(String.valueOf(this.mLCDutySet.get(1).getAmnt())).equals("0.0")
                        || StrTool.cTrim(String.valueOf(this.mLCDutySet.get(1).getAmnt())).equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "ManuUWRiskSaveBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "执行“减额”操作时请录入降至保额。如果需要恢复原保额，请在降至保额中录入原保额！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            //变更缴费期的校验
            if (StrTool.cTrim(this.LoadFlag).equals("C"))
            {
                if ((StrTool.cTrim(String.valueOf(this.mLCDutySet.get(1).getPayEndYear())).equals("0.0") || StrTool
                        .cTrim(String.valueOf(this.mLCDutySet.get(1).getPayEndYear())).equals(""))
                        || (StrTool.cTrim(String.valueOf(this.mLCDutySet.get(1).getPayEndYearFlag())).equals("0.0") || StrTool
                                .cTrim(String.valueOf(this.mLCDutySet.get(1).getPayEndYearFlag())).equals("")))
                {
                    CError tError = new CError();
                    tError.moduleName = "ManuUWRiskSaveBL";
                    tError.functionName = "checkData";
                    tError.errorMessage = "执行“减额”操作时请录入降至保额。如果需要恢复原保额，请在降至保额中录入原保额！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

        }

        return true;
    }

    /**
     * checkUW 校验时否经过核保
     * 
     * @return boolean
     */
    private boolean checkUW()
    {
        if (mLCUWMasterSchema.getPassFlag().equals("1") || mLCUWMasterSchema.getPassFlag().equals("4")
                || mLCUWMasterSchema.getPassFlag().equals("9"))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "checkUW";
            tError.errorMessage = "此险种核保结论已下!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLCUWMasterSchema = (LCUWMasterSchema) cInputData.getObjectByObjectName("LCUWMasterSchema", 0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

        //mAddFeeFlag 如果是 L 则表示加费
        mAddFeeFlag = (String) mTransferData.getValueByName("AddFeeFlag");
        mAddFeeType = (String) mTransferData.getValueByName("AddFeeType");
        //如果是加费操作获取加费数据
        if (mAddFeeFlag != null)
        {
            if (mAddFeeFlag.equals("L"))
            {
                mLCPremSet = (LCPremSet) cInputData.getObjectByObjectName("LCPremSet", 0);
                mLCUWDiseaseCheckInfoSet = (LCUWDiseaseCheckInfoSet) cInputData.getObjectByObjectName(
                        "LCUWDiseaseCheckInfoSet", 0);
                if (mLCUWDiseaseCheckInfoSet == null)
                    mLCUWDiseaseCheckInfoSet = new LCUWDiseaseCheckInfoSet();
            }
        }
        else
        {
            mAddFeeFlag = "";
        }
        // 免责信息 如果是免责操作则SpecFlag为 E
        mSpecFlag = (String) mTransferData.getValueByName("SpecFlag");
        if (StrTool.cTrim(this.mSpecFlag).equals("E"))
        {
            mLCSpecSet = (LCSpecSet) cInputData.getObjectByObjectName("LCSpecSet", 0);
        }
        LoadFlag = (String) mTransferData.getValueByName("LoadFlag");
        if (StrTool.cTrim(this.LoadFlag).equals("M") || StrTool.cTrim(this.LoadFlag).equals("A"))
        {
            this.mLCDutySet = (LCDutySet) cInputData.getObjectByObjectName("LCDutySet", 0);
            if (this.mLCDutySet == null)
            {
                System.out.println("程序第522行出错，请检查ManuUWRiskSaveBL.java中的getInputData方法！");
                CError tError = new CError();
                tError.moduleName = "ManuUWRiskSaveBL.java";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的责任信息为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (StrTool.cTrim(this.LoadFlag).equals("C"))
        {
            this.mLCDutySet = (LCDutySet) cInputData.getObjectByObjectName("LCDutySet", 0);
            if (this.mLCDutySet == null)
            {
                System.out.println("程序第698行出错，请检查ManuUWRiskSaveBL.java中的getInputData方法！");
                CError tError = new CError();
                tError.moduleName = "ManuUWRiskSaveBL.java";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的责任信息为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mSpecReason = (String) mTransferData.getValueByName("SpecReason");
        mOperator = tGI.Operator;
        if (mOperator == null || mOperator.length() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mLCUWMasterSchema == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据LCUWMasterSchema失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mUWFlag = mLCUWMasterSchema.getPassFlag();
        if (mUWFlag == null || mUWFlag.length() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据UWFlag失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mUWIdea = mLCUWMasterSchema.getUWIdea();
        mSugPassFlag = mLCUWMasterSchema.getSugPassFlag();
        mSugUWIdea = mLCUWMasterSchema.getSugUWIdea();
        mCustomerReply = mLCUWMasterSchema.getCustomerReply();
        mRejectReason = mLCUWMasterSchema.getRejectReason();
        this.mPostponeDate = mLCUWMasterSchema.getPostponeDate();
        this.mPostponeDay = mLCUWMasterSchema.getPostponeDay();
        this.mPostponeDescribe = mLCUWMasterSchema.getPostponeDescribe();

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        //        LCPremSchema tLCPremSchema = new LCPremSchema();
        //        LCPremSet tLCPremSet = new LCPremSet();
        //        tLCPremSchema.setPrem("");
        //        tLCPremSchema.setRate(0.2);
        //        tLCPremSchema.setPayStartDate("");
        //        tLCPremSchema.setPayEndDate("");
        //        tLCPremSet.add(tLCPremSchema);

        LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();
        TransferData mTransferData = new TransferData();
        LCDutySchema tLCDutySchema = new LCDutySchema();
        LCDutySet tLCDutySet = new LCDutySet();
        tLCDutySchema.setMult(2);
        tLCDutySchema.setAmnt("");
        tLCDutySchema.setPolNo("11000081003");
        tLCDutySet.add(tLCDutySchema);
        mTransferData.setNameAndValue("LoadFlag", "M");

        //
        //        mTransferData.setNameAndValue("AddFeeFlag","L");
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "001";
        mLCUWMasterSchema.setPolNo("11000081003");
        mLCUWMasterSchema.setProposalNo("11000081003");
        mLCUWMasterSchema.setPassFlag("4");
        ManuUWRiskSaveBL mManuUWRiskSaveBL = new ManuUWRiskSaveBL();
        VData mInputData = new VData();
        mInputData.add(mLCUWMasterSchema);
        //        mInputData.add(tLCPremSet);
        mInputData.add(tLCDutySet);
        mInputData.add(mGlobalInput);
        mInputData.add(mTransferData);
        mManuUWRiskSaveBL.submitData(mInputData, "");

    }

    private boolean LCUWSub()
    {

        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
        tLCUWSubDB.setProposalNo(mLCUWMasterSchema.getProposalNo());
        mLCUWSubSet = tLCUWSubDB.query();
        if (mLCUWSubSet == null || mLCUWSubSet.size() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ManuUWRiskSaveBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询核保轨迹表失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int m = mLCUWSubSet.size();
        if (m >= 0)
        {
            m = m + 1; //核保次数
            mLCUWSubSchema.setUWNo(m); //第几次核保
            mLCUWSubSchema.setContNo(mLCUWMasterSchema.getContNo());
            mLCUWSubSchema.setPolNo(mLCUWMasterSchema.getPolNo());
            mLCUWSubSchema.setGrpContNo(mLCUWMasterSchema.getGrpContNo());
            mLCUWSubSchema.setProposalContNo(mLCUWMasterSchema.getProposalContNo());
            mLCUWSubSchema.setProposalNo(mLCUWMasterSchema.getProposalNo());
            mLCUWSubSchema.setInsuredNo(mLCUWMasterSchema.getInsuredNo());
            mLCUWSubSchema.setInsuredName(mLCUWMasterSchema.getInsuredName());
            mLCUWSubSchema.setAppntNo(mLCUWMasterSchema.getAppntNo());
            mLCUWSubSchema.setAppntName(mLCUWMasterSchema.getAppntName());
            mLCUWSubSchema.setAgentCode(mLCUWMasterSchema.getAgentCode());
            mLCUWSubSchema.setAgentGroup(mLCUWMasterSchema.getAgentGroup());
            mLCUWSubSchema.setUWGrade(mLCUWMasterSchema.getUWGrade()); //核保级别
            mLCUWSubSchema.setAppGrade(mLCUWMasterSchema.getAppGrade()); //申请级别
            mLCUWSubSchema.setAutoUWFlag(mLCUWMasterSchema.getAutoUWFlag());
            mLCUWSubSchema.setState(mLCUWMasterSchema.getState());
            mLCUWSubSchema.setPassFlag(mLCUWMasterSchema.getPassFlag());
            mLCUWSubSchema.setPostponeDay(mLCUWMasterSchema.getPostponeDay());
            mLCUWSubSchema.setPostponeDate(mLCUWMasterSchema.getPostponeDate());
            mLCUWSubSchema.setPostponeDescribe(mLCUWMasterSchema.getPostponeDescribe());
            mLCUWSubSchema.setUpReportContent(mLCUWMasterSchema.getUpReportContent());
            mLCUWSubSchema.setHealthFlag(mLCUWMasterSchema.getHealthFlag());
            mLCUWSubSchema.setSpecFlag(mLCUWMasterSchema.getSpecFlag());
            mLCUWSubSchema.setSpecReason(mLCUWMasterSchema.getSpecReason());
            mLCUWSubSchema.setQuesFlag(mLCUWMasterSchema.getQuesFlag());
            mLCUWSubSchema.setReportFlag(mLCUWMasterSchema.getReportFlag());
            mLCUWSubSchema.setChangePolFlag(mLCUWMasterSchema.getChangePolFlag());
            mLCUWSubSchema.setChangePolReason(mLCUWMasterSchema.getChangePolReason());
            mLCUWSubSchema.setAddPremReason(mLCUWMasterSchema.getAddPremReason());
            mLCUWSubSchema.setPrintFlag(mLCUWMasterSchema.getPrintFlag());
            mLCUWSubSchema.setPrintFlag2(mLCUWMasterSchema.getPrintFlag2());
            mLCUWSubSchema.setUWIdea(mLCUWMasterSchema.getUWIdea());
            mLCUWSubSchema.setSubMultFlag(mLCUWMasterSchema.getSubMultFlag());
            mLCUWSubSchema.setMult(mLCUWMasterSchema.getMult());
            mLCUWSubSchema.setOperator(mLCUWMasterSchema.getOperator()); //操作员
            mLCUWSubSchema.setManageCom(mLCUWMasterSchema.getManageCom());
            mLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
            mLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
            mLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            mLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
            mLCUWSubSchema.setSubAmntFlag(mLCUWMasterSchema.getSubAmntFlag());
            mLCUWSubSchema.setAmnt(mLCUWMasterSchema.getAmnt());
            mLCUWSubSchema.setCustomerReply(mLCUWMasterSchema.getCustomerReply());
            mLCUWSubSchema.setChPayEndYearFlag(mLCUWMasterSchema.getChPayEndYearFlag());
            mLCUWSubSchema.setPayEndYear(mLCUWMasterSchema.getPayEndYear());
            mLCUWSubSchema.setPayEndYearFlag(mLCUWMasterSchema.getPayEndYearFlag());
            mLCUWSubSchema.setRejectReason(mLCUWMasterSchema.getRejectReason());

        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpUWManuNormChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = "核保轨迹表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 处理险种拒保保单的总保费信息
     * @return boolean
     */
    private boolean dealContInfo()
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(this.mLCUWMasterSchema.getContNo());
        if (!tLCContDB.getInfo())
        {

        }
        LCContSchema tLCContSchema = tLCContDB.getSchema();
        return true;
    }

    /**
     * 处理降档
     * @return boolean
     */
    private boolean prepareSubMult()
    {
        VData inputData = new VData();
        DealSubMultAndAmnt tdealSubMultandAmnt = new DealSubMultAndAmnt();
        inputData.add(this.mLCDutySet);
        System.out.println("开始处理减档");
        if (!tdealSubMultandAmnt.submitData(inputData, ""))
        {
            this.mErrors.copyAllErrors(tdealSubMultandAmnt.mErrors);
            return false;
        }
        this.subVData = new VData();
        subVData = tdealSubMultandAmnt.getResult();
        return true;
    }

    private boolean prepareChPayEndYear()
    {
        VData inputData = new VData();
        DealChPayEndYear tDealChPayEndYear = new DealChPayEndYear();
        inputData.add(this.mLCDutySet);
        System.out.println("开始处理变更缴费期间");
        if (!tDealChPayEndYear.submitData(inputData, ""))
        {
            this.mErrors.copyAllErrors(tDealChPayEndYear.mErrors);
            return false;
        }
        this.subVData = new VData();
        subVData = tDealChPayEndYear.getResult();
        this.retPayEndYear = tDealChPayEndYear.retPayEndYear();
        this.retPayEndYearFlag = tDealChPayEndYear.retPayEndYearFlag();
        return true;
    }

}
