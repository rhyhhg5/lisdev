package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统体检资料录入部分</p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class UWAskInfoBLS
{
  //是否存在需要人工核保保单
  int merrno = 0;
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();

  //
  //private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB();
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

  public UWAskInfoBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
        //首先将数据在本类中做一个备份
        mInputData=(VData)cInputData.clone() ;

        System.out.println("Start UWAutoHealthBLS Submit...");
        if (!this.saveData())
   return false;
 System.out.println("End UWAutoHealthBLS Submit...");

 mInputData=null;
 return true;
  }

  private boolean saveData()
  {

        LCInformationSet mLCInformationSet = (LCInformationSet)mInputData.getObjectByObjectName("LCInformationSet",0);
        LCInformationItemSet mLCInformationItemSet = (LCInformationItemSet)mInputData.getObjectByObjectName("LCInformationItemSet",0);
        LCUWMasterSchema mLCUWMasterSchema = (LCUWMasterSchema)mInputData.getObjectByObjectName("LCUWMasterSchema",0);
        LOPRTManagerSet mLOPRTManagerSet = (LOPRTManagerSet)mInputData.getObjectByObjectName("LOPRTManagerSet",0);

        Connection conn = DBConnPool.getConnection();
        //conn = .getDefaultConnection();

        if (conn==null)
        {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "UWAutoHealthBLS";
          tError.functionName = "saveData";
          tError.errorMessage = "数据库连接失败!";
          this.mErrors .addOneError(tError) ;
          return false;
        }

        try
        {
          conn.setAutoCommit(false);

          // 删除部分
          int polCount = mLCInformationSet.size();
          if (polCount > 0)
          {
                LCInformationSchema tLCInformationSchema = new LCInformationSchema();
                tLCInformationSchema = ( LCInformationSchema )mLCInformationSet.get( 1 );
                String tProposalNo = tLCInformationSchema.getGrpContNo();

                // 核保主表
                LCInformationDB tLCInformationDB = new LCInformationDB( conn );
                tLCInformationDB.setCustomerNo( tProposalNo );
                if (tLCInformationDB.deleteSQL() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCInformationDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWAtuoHealthBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "LCInformation表删除失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }

                // 投保单
                LCInformationItemDB tLCInformationItemDB = new LCInformationItemDB( conn );
//		tLCInformationItemDB.setCustomerNo( tProposalNo );
                if (tLCInformationItemDB.deleteSQL() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCInformationItemDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWAutoHealthBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "LCInformationItem表删除失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }
                // 保存部分
                // 保单

                //LCPolDBSet tLCPolDBSet = new LCPolDBSet( conn );
                //tLCPolDBSet.set( mLCPolSet );
                //if (tLCPolDBSet.insert() == false)
                //LCPolDB tLCPolDB = new LCPolDB();
                tLCInformationDB.setSchema(tLCInformationSchema);

                if(tLCInformationDB.insert() == false)
                {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCInformationDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "UWAutoHealthBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "LCInformation表保存失败!";
                  this.mErrors .addOneError(tError) ;
                  conn.rollback() ;
                  conn.close();
                  return false;
                }

                System.out.println("-----------DDD--------------");
                // 体检资料主表

                int masterno = mLCInformationItemSet.size();
                for (int i = 1;i <= masterno;i++)
                {
                  LCInformationItemSchema tLCInformationItemSchema = mLCInformationItemSet.get(i);
//		  String masterpol = tLCInformationItemSchema.getCustomerNo();

                  tLCInformationItemDB.setSchema(tLCInformationItemSchema);
                  if (tLCInformationItemDB.insert() == false)
                  {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCInformationItemDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "UWAutoHealthBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "LCInformationItem表保存失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        return false;
                  }

                }
                System.out.println("-----------EEE--------------");
                if(mLCUWMasterSchema != null)
                {
                  LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
                  tLCUWMasterDB.setSchema(mLCUWMasterSchema);
                  if(tLCUWMasterDB.update() == false)
                  {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "UWAutoHealthBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "LCUWMaster表更新失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback() ;
                        conn.close();
                        return false;
                  }
                }

                if (mLOPRTManagerSet!=null&&mLOPRTManagerSet.size()>0)
                {
                  LOPRTManagerDBSet tLOPRTManagerDBSet = new LOPRTManagerDBSet(conn);
                  tLOPRTManagerDBSet.set(mLOPRTManagerSet);
                  if (!tLOPRTManagerDBSet.insert())
                  {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLOPRTManagerDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "UWAutoHealthBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "打印管理表数据提交失败!";
                        this.mErrors .addOneError(tError) ;
                        conn.rollback();
                        conn.close();
                        return false;
                  }
                }

                conn.commit() ;
                conn.close();
          } // end of for

        } // end of try
        catch (Exception ex)
        {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "ProposalFeeBLS";
          tError.functionName = "submitData";
          tError.errorMessage = ex.toString();
          this.mErrors .addOneError(tError);
          try{conn.rollback() ;} catch(Exception e){}
          return false;
        }

        return true;
  }

}
