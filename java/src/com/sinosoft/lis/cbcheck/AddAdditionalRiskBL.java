package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class AddAdditionalRiskBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private GlobalInput tGI = new GlobalInput();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private LCAddPolSet mLCAddPolSet ;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();

  //业务处理相关变量
  /** 个人信息 */
  private LCAddPolSchema  mLCAddPolSchema=new LCAddPolSchema() ;

  public AddAdditionalRiskBL()
  {
  }
  public static void main(String[] args)
  {
    GlobalInput tG = new GlobalInput();
    tG.ManageCom="86";
    tG.Operator="001";
    //问题件表
    LCAddPolSchema tLCAddPolSchema = new LCAddPolSchema();
    tLCAddPolSchema.setRemark("test");
    tLCAddPolSchema.setMainPolNo("86110020030110000084");

    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add( tLCAddPolSchema);
    tVData.add( tG );

    // 数据传输
    AddAdditionalRiskUI tAddAdditionalRiskUI   = new AddAdditionalRiskUI();
    tAddAdditionalRiskUI.submitData(tVData,"");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    //进行业务处理
    if (!dealData(cOperate))
      return false;

    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    System.out.println("Start AddAdditionalRiskBLS Submit...");

    AddAdditionalRiskBLS tAddAdditionalRiskBLS=new AddAdditionalRiskBLS();
    tAddAdditionalRiskBLS.submitData(mInputData,cOperate);

    System.out.println("End AddAdditionalRiskBLS  Submit...");

    //如果有需要处理的错误，则返回
    if (tAddAdditionalRiskBLS.mErrors .needDealError())
    {
        mErrors .copyAllErrors(tAddAdditionalRiskBLS.mErrors ) ;
        return false;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData(String cOperate)
  {

    if (!getInputData())
      return false ;

    mLCPolSchema=queryData();
    if(mLCPolSchema==null)
      return false;
    if(!(mLCPolSchema.getUWFlag().equals("0")||mLCPolSchema.getUWFlag().equals("8")))
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="AddAdditionalRiskBL";
      tError.functionName="dealData";
      tError.errorMessage="该投保单的核保状态不符合!";
      mErrors .addOneError(tError) ;
      return false;
    }

    String tLimit = "";
    String serNo="";//流水号
    tLimit=PubFun.getNoLimit(mLCPolSchema.getManageCom());
    serNo=PubFun1.CreateMaxNo("ADDRISKSN",10);
    VData tVData = new VData();

    mLCAddPolSchema.setSerialNo(serNo);
    mLCAddPolSchema.setPrtNo(mLCPolSchema.getPrtNo());
    mLCAddPolSchema.setManageCom(mLCPolSchema.getManageCom());
    mLCAddPolSchema.setInputState("0");
    mLCAddPolSchema.setOperator(tGI.Operator);
    mLCAddPolSchema.setManageCom(tGI.ManageCom);
    mLCAddPolSchema.setMakeDate(CurrentDate);
    mLCAddPolSchema.setMakeTime(CurrentTime);
    mLCAddPolSchema.setModifyDate(CurrentDate);
    mLCAddPolSchema.setModifyTime(CurrentTime);

    return true ;
  }

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  private boolean getInputData()
  {
    boolean flag=false;
    mLCAddPolSchema   = (LCAddPolSchema)mInputData.getObjectByObjectName("LCAddPolSchema",0);
    tGI = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
    if(mLCAddPolSchema==null)
    {
      flag=true;
    }
    if(mLCAddPolSchema.getMainPolNo()==null)
    {
      flag=true;
    }
    if(flag)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="AddAdditionalRiskBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据!";
      mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }


  private LCPolSchema queryData()
  {

      LCPolSchema tLCPolSchema=new LCPolSchema();
      tLCPolSchema.setPolNo(mLCAddPolSchema.getMainPolNo());//查询投保单
      LCPolDB tLCPolDB=new LCPolDB();
      tLCPolDB.setSchema(tLCPolSchema);
      if(!tLCPolDB.getInfo())
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="AddAdditionalRiskBL";
        tError.functionName="checkData";
        tError.errorMessage="没有查询到投保单";
        this.mErrors .addOneError(tError) ;
        return null;
      }
      return tLCPolDB.getSchema();
  }

  public VData getResult()
  {
    return mResult;
   }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    try
    {
      mInputData.clear();
      mInputData.add(mLCAddPolSchema);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="AddAdditionalRiskBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

}
