/*
 * <p>ClassName: LPWBContBL </p>
 * <p>Description: LDCodeSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-07-10
 */
package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LPWBContBL extends LDCodeSchema
{
	public CErrors mErrors = new CErrors();
	private String mOperate = "";//添加或删除标志
	private TransferData mTransferData = new TransferData();//数据包装类（数据打包）
	MMap map = new MMap();
	// @Constructor
	public LPWBContBL() {}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "LPWBContBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	
	//提交过来的数据
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("页面数据封装传到后台==["+cInputData+"]"+"["+cOperate+"]");
		if (!getInputData(cInputData,cOperate)) {
			return false;
		}
		
		if(!dealData()){
        	return false;
        }
		
		if(map.size()>0){//map不为空
        	VData vData = new VData();
    		vData.add(map);

    		PubSubmit pubsubmit = new PubSubmit();
    		if (!pubsubmit.submitData(vData, "")) {
    			mErrors.copyAllErrors(pubsubmit.mErrors);
    			return false;
    		}
        }		
		System.out.println("操作成功！");
		return true;
	}
	
	//对数据做增删改操作（与模型层交互）
	private boolean dealData() {
		String policyNo1 = (String) mTransferData.getValueByName("PolicyNo1");//保单号
		String manageCom1 = (String) mTransferData.getValueByName("ManageCom1");//管理机构
		System.out.println("dealData----"+policyNo1+"==="+manageCom1);
		//拿到的值做为空校验
		if(manageCom1==null||"".equals(manageCom1)){
    		buildError("dealData", "管理机构不能为空。");
    		return false;
    	}
		if(policyNo1==null||"".equals(policyNo1)){
    		buildError("dealData", "保单号不能为空。");
    		return false;
    	}
		
		//执行操作
		if(mOperate.equals("save")){//添加
			//创建实体类ldcode
			LDCodeSchema tLDCodeSchema = new LDCodeSchema();//ldcode表的实体类
			tLDCodeSchema.setCodeType("LPWBCont");
			tLDCodeSchema.setCode(policyNo1);//添加表单号
			tLDCodeSchema.setCodeName(PubFun.getCurrentDate());//PubFun.getCurrentDate();取系统的当前日期
			map.put(tLDCodeSchema, SysConst.INSERT);
			
		}else if(mOperate.equals("delete")){//删除
			String strSQL = "delete from ldcode where  code = '"+policyNo1+"' and codetype='LPWBCont' "; 
			map.put(strSQL, SysConst.DELETE);
		}
		
		return true;
	}

	//判断传到控制层的参数值不为空
	private boolean getInputData(VData cInputData,String cOperate) {
		mOperate = cOperate;//添加 删除标志
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);//VData容器中存的TransferData对象（赋值给上面新new的TransferData对象），“0”位置，相当于数组的下标
        
        if (mTransferData == null)
        {
            buildError("checkData", "所需参数不完整。");
            return false;
        }
        
        if (mOperate == null)
        {
            buildError("checkData", "操作类型错误。");
            return false;
        }

		return true;
	}
}
