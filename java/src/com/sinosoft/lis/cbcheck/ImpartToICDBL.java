package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class ImpartToICDBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private MMap map = new MMap();

    private VData mResult = new VData();

    private VData mInputData;

    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    private String mOperator;

    private String mManageCom;

    /** 业务操作类 */
    private LCDiseaseResultSet mLCDiseaseResultSet = new LCDiseaseResultSet();

    private LCDiseaseResultSet mdelLCDiseaseResultSet = new LCDiseaseResultSet();

    private TransferData mTransferData = new TransferData();

    private String mContNo;

    private LCInsuredSchema mLCInsuredSchemaIn = null; //传入的被保人信息

    /** 业务数据 */
    private String mName = "";

    private String mDelSql = "";

    public ImpartToICDBL()
    {
    }

    /**
     * 传入客户信息，这样可不需要进行被保人查询
     * @param mLDPersonSchema LCInsuredSchema
     * @return void
     */
    public void setLCInsured(LCInsuredSchema cLCInsuredSchema)
    {
        mLCInsuredSchemaIn = cLCInsuredSchema;
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (this.getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        System.out.println("Start ImpartToICDBL Submit...");

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("ImpartToICDBL end");

        return true;
    }

    /**
     * 处理告知信息
     * @param cInputData VData
     * @param cOperate String
     * @return MMap：处理后的结果集合
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return null;
        }
        System.out.println("After getinputdata");

        if (!checkData())
        {
            return null;
        }
        //进行业务处理
        if (!dealData())
        {
            return null;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return null;
        }
        System.out.println("After prepareOutputData");

        return map;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        System.out.println("mdelLCDiseaseResultSet" + mLCDiseaseResultSet);

        long currentTime = System.currentTimeMillis();

        map.put("delete from LCDiseaseResult " + "where ProposalContNo = '"
                + mContNo + "' " + "   and " + currentTime + " = "
                + currentTime, "DELETE");
        //       if (!(this.mdelLCDiseaseResultSet == null)
        //               ||mdelLCDiseaseResultSet.size()>0) {
        //            map.put(mdelLCDiseaseResultSet,"DELETE");
        //        }

        if (!SysConst.DELETE.equals(this.mOperate)
                && mLCDiseaseResultSet.size() > 0)
        {

            map.put(mLCDiseaseResultSet, "INSERT");
        }
        System.out.println("mLCDiseaseResultSet ： "
                + mLCDiseaseResultSet.size());

        mResult.add(map);

        return true;
    }

    /**
     * dealData
     * 业务逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {
        if (mLCDiseaseResultSet.size() > 0)
        {

            int tDisSerialNo;
            String mflag = "";
            LCDiseaseResultDB tLCDiseaseResultDB = new LCDiseaseResultDB();
            tLCDiseaseResultDB
                    .setContNo(mLCDiseaseResultSet.get(1).getContNo());
            String diseaseSource = mLCDiseaseResultSet.get(1)
                    .getDiseaseSource();
            //            if (!diseaseSource.equals("")) {
            //                tLCDiseaseResultDB.setDiseaseSource(diseaseSource);
            //            }
            //        tLCDiseaseResultDB.setCustomerNo(mLCDiseaseResultSet.get(1).getCustomerNo());
            LCDiseaseResultSet tLCDiseaseResultSet = tLCDiseaseResultDB.query();
            if (tLCDiseaseResultSet.size() > 0)
            {
                this.mdelLCDiseaseResultSet = tLCDiseaseResultSet;
            }
            //        if(tLCDiseaseResultSet.size()==0)
            //        {
            //            tDisSerialNo = 0;
            //        }
            //        else
            //        {
            //            tDisSerialNo = tLCDiseaseResultSet.size() + 1;
            //        }

            for (int i = 1; i <= mLCDiseaseResultSet.size(); i++)
            {
                //检验getDisDesb,getDisResult,getICDCode不能为空
                if (mLCDiseaseResultSet.get(i).getDisDesb().trim().equals("")
                        || mLCDiseaseResultSet.get(i).getDisResult().trim()
                                .equals("")
                        || mLCDiseaseResultSet.get(i).getICDCode().trim()
                                .equals(""))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ImpartToICDBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "疾病症状，疾病结果，ICD编码录入不能为空，请检查！";
                    this.mErrors.addOneError(tError);
                    mflag = "ICDERROR";
                    break;
                }

                if (mLCInsuredSchemaIn != null)
                {
                    mLCDiseaseResultSet.get(i).setCustomerNo(
                            mLCInsuredSchemaIn.getInsuredNo());
                    mLCDiseaseResultSet.get(i).setName(
                            mLCInsuredSchemaIn.getName());
                }
                else
                {
                    LDPersonDB tLDPersonDB = new LDPersonDB();
                    tLDPersonDB.setCustomerNo(mLCDiseaseResultSet.get(i)
                            .getCustomerNo());
                    if (!tLDPersonDB.getInfo())
                    {
                        buildError("dealData", "查找客户信息失败！");
                        return false;
                    }
                    mLCDiseaseResultSet.get(i).setName(tLDPersonDB.getName());
                }

                /*
                 if (StrTool.cTrim(mLCDiseaseResultSet.get(i).getSerialNo()).
                 equals("")) {
                 int j = i + 1;
                 mLCDiseaseResultSet.get(i).setSerialNo("" + j);
                 }
                 */
                mLCDiseaseResultSet.get(i).setSerialNo(String.valueOf(i));
                mLCDiseaseResultSet.get(i).setOperator(mOperator);
                mLCDiseaseResultSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLCDiseaseResultSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLCDiseaseResultSet.get(i).setModifyDate(
                        PubFun.getCurrentDate());
                mLCDiseaseResultSet.get(i).setModifyTime(
                        PubFun.getCurrentTime());
            }
            if (mflag == "ICDERROR")
            {
                return false;
            }
        }
        /*
         else {
         LCDiseaseResultDB tLCDiseaseResultDB = new LCDiseaseResultDB();
         tLCDiseaseResultDB.setContNo(mContNo);
         tLCDiseaseResultDB.setDiseaseSource("1");
         LCDiseaseResultSet tLCDiseaseResultSet = tLCDiseaseResultDB.query();
         if (tLCDiseaseResultSet.size() > 0) {
         this.mdelLCDiseaseResultSet = tLCDiseaseResultSet;
         }

         }
         */
        return true;
    }

    /**
     * checkData
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        if (mLCDiseaseResultSet.size() > 0)
        {
            //            if (mLCInsuredSchemaIn == null)
            //            {
            //                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            //                tLCInsuredDB.setContNo(mLCDiseaseResultSet.get(1).getContNo());
            //                tLCInsuredDB.setInsuredNo(mLCDiseaseResultSet.get(1)
            //                        .getCustomerNo());
            //                if (!tLCInsuredDB.getInfo())
            //                {
            //                    // @@错误处理
            //                    //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            //                    CError tError = new CError();
            //                    tError.moduleName = "ImpartToICDBL";
            //                    tError.functionName = "checkData";
            //                    tError.errorMessage = "客户信息查询失败!";
            //                    this.mErrors.addOneError(tError);
            //                    return false;
            //                }
            //                mLCInsuredSchemaIn = tLCInsuredDB.getSchema();
            //            }
            //校验是否有重复的疾病结果
            for (int i = 1; i <= mLCDiseaseResultSet.size(); i++)
            {
                for (int j = i + 1; j <= mLCDiseaseResultSet.size(); j++)
                {
                    if (StrTool.cTrim(
                            mLCDiseaseResultSet.get(i).getProposalContNo())
                            .equals(
                                    mLCDiseaseResultSet.get(j)
                                            .getProposalContNo())
                            & StrTool.cTrim(
                                    mLCDiseaseResultSet.get(i).getContNo())
                                    .equals(
                                            mLCDiseaseResultSet.get(j)
                                                    .getContNo())
                            & StrTool.cTrim(
                                    mLCDiseaseResultSet.get(i).getCustomerNo())
                                    .equals(
                                            mLCDiseaseResultSet.get(j)
                                                    .getCustomerNo())
                            & StrTool.cTrim(
                                    mLCDiseaseResultSet.get(i).getDisDesb())
                                    .equals(
                                            mLCDiseaseResultSet.get(j)
                                                    .getDisDesb())
                            & StrTool.cTrim(
                                    mLCDiseaseResultSet.get(i).getDisResult())
                                    .equals(
                                            mLCDiseaseResultSet.get(j)
                                                    .getDisResult())
                            & StrTool.cTrim(
                                    mLCDiseaseResultSet.get(i).getICDCode())
                                    .equals(
                                            mLCDiseaseResultSet.get(j)
                                                    .getICDCode()))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ImpartToICDBL";
                        tError.functionName = "checkData";
                        tError.errorMessage = "录入了重复的疾病结果,请检查!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mLCDiseaseResultSet = (LCDiseaseResultSet) cInputData
                .getObjectByObjectName("LCDiseaseResultSet", 0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mOperator = tGI.Operator;
        if (mOperator == null || mOperator.length() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ImpartToICDBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData != null)
        {
            mContNo = (String) this.mTransferData.getValueByName("ContNo");
        }

        //取得疾病信息
        //        if(mLCDiseaseResultSet==null||mLCDiseaseResultSet.size()<= 0)
        //        {
        //            // @@错误处理
        //            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
        //            CError tError = new CError();
        //            tError.moduleName = "ImpartToICDBL";
        //            tError.functionName = "getInputData";
        //            tError.errorMessage = "前台传输数据LCDiseaseResultSet失败!";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ImpartToICDBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

}
