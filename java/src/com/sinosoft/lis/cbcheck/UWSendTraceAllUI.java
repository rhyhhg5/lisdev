package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class UWSendTraceAllUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private VData mInputData;


    /** 数据操作字符串 */
    private String mOperate;

    public UWSendTraceAllUI() {
    }


    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;

        UWSendTraceAllBL tUWSendTraceAllBL = new UWSendTraceAllBL();

        System.out.println("--------UWSendTraceAllUI Start!---------");
        tUWSendTraceAllBL.submitData(cInputData, cOperate);
        System.out.println("--------UWSendTraceAllUI End!---------");

        //如果有需要处理的错误，则返回
        if (tUWSendTraceAllBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tUWSendTraceAllBL.mErrors);
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        this.mResult = tUWSendTraceAllBL.getResult();

        return true;
    }


    /**
     * 返回结果方法
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


// @Main
    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "test1";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        TransferData tTransferData = new TransferData();
//        个单上报
                LCCUWMasterSet tLCCUWMasterSet = new LCCUWMasterSet();
                LCContSchema tLCContSchema = new LCContSchema();
                LCContSet tLCContSet = new LCContSet();
                tLCContSchema.setContNo("13000000464");
                tLCContSchema.setUWFlag("4");
                tLCContSchema.setRemark("");
                tLCContSet.add( tLCContSchema );

                LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
                LCCUWMasterSchema tLCCUWMasterSchema = new LCCUWMasterSchema();
                tLCCUWMasterSchema.setContNo("13000000464");
                tLCCUWMasterSchema.setProposalContNo("13000000464");
                tLCCUWMasterSchema.setPassFlag("4");
                tLCCUWMasterSet.add(tLCCUWMasterSchema);
                tLCUWSendTraceSchema.setOtherNo("13000000464");
                tLCUWSendTraceSchema.setOtherNoType("1");

                tTransferData.setNameAndValue("PSendUpFlag", "3");
                tTransferData.setNameAndValue("SendType", "2");
                tTransferData.setNameAndValue("LCCUWMasterSchema",tLCCUWMasterSchema);
                tTransferData.setNameAndValue("LCUWSendTraceSchema",tLCUWSendTraceSchema);
                tTransferData.setNameAndValue("ActivityID","0000001110");
                tLCUWSendTraceSchema.setYesOrNo("");
                VData tVData = new VData();

                tVData.add(tG);
                tVData.add(tTransferData);
                tVData.add(tLCCUWMasterSet);
                tVData.add(tLCUWSendTraceSchema);
        tVData.add(tLCContSet);

//团单上报
        /*
        LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();
        LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
        LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
        tLCGCUWMasterSchema.setGrpContNo("1400000092");
        tLCGCUWMasterSchema.setPassFlag("9");

        tLCUWSendTraceSchema.setOtherNo("1400000092");
        tLCUWSendTraceSchema.setOtherNoType("2");

        tTransferData.setNameAndValue("LCGCUWMasterSchema", tLCGCUWMasterSchema);
        tTransferData.setNameAndValue("LCUWSendTraceSchema",
                                      tLCUWSendTraceSchema);
        tLCUWSendTraceSchema.setYesOrNo("N");
        VData tVData = new VData();

        tVData.add(tG);
        tVData.add(tTransferData);
        tVData.add(tLCUWSendTraceSchema);
*/
        UWSendTraceAllUI ui = new UWSendTraceAllUI();
        if (ui.submitData(tVData, "submit") == true) {
            System.out.println("---ok---");
        } else {
            System.out.println("---NO---");
        }

    }

}
