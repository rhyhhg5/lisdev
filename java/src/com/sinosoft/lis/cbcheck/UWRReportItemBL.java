package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCRReportDB;
import com.sinosoft.lis.vdb.LCRReportDBSet;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.vschema.LCRReportResultSet;
import com.sinosoft.lis.vschema.LCRReportSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;



/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class UWRReportItemBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData;
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mOperator;
    private String mManageCom;
    private MMap map = new MMap();
    /** 业务操作类 */
    private LCRReportSchema mLCRReportSchema = new LCRReportSchema();
    private LCRReportSchema tLCRReportSchema = new LCRReportSchema();

    private LCRReportItemSchema mLCRReportItemSchema = new LCRReportItemSchema();
   /** 业务数据 */

    public UWRReportItemBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("After getinputdata");

        if (!checkData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("After prepareOutputData");

        System.out.println("Start RReportResultBL Submit...");

        PubSubmit tSubmit = new PubSubmit();

        if (!tSubmit.submitData(mResult, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "GrpUWAutoChkBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        System.out.println("RReportResultBL end");

        return true;
    }


    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        if (mOperate =="UPDATE")
        {
          map.put(mLCRReportItemSchema,"UPDATE");
        }
          if (mOperate =="MODIFY")
        {
          map.put(tLCRReportSchema,"UPDATE");
        }
        if (mOperate =="DELETE")
        {
          map.put(mLCRReportItemSchema,"DELETE");
        }
        mResult.add(map);
        return true;
    }


    /**
     * dealData
     * 业务逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {
      if (mOperate =="UPDATE")
      {
         mLCRReportItemSchema.setModifyDate(PubFun.getCurrentDate());
         mLCRReportItemSchema.setModifyTime(PubFun.getCurrentTime());
      }

      if (mOperate =="MODIFY")
      {
       ExeSQL tExeSQL=new  ExeSQL();
       SSRS tSSRS= new SSRS();
       tSSRS=tExeSQL.execSQL("select * from lwmission where missionprop2='"+mLCRReportSchema.getProposalContNo()+"' and activityid in('0000001100','0000001160') ");
       if(tSSRS.getMaxRow()>0){
          map.put("UPDATE lwmission set activitystatus='3' where missionprop2='"+mLCRReportSchema.getProposalContNo()+"' and activityid in('0000001100','0000001160') "
          , "UPDATE");
       }
        LCRReportDB tLCRReportDB = new LCRReportDB();
        tLCRReportDB.setProposalContNo(mLCRReportSchema.getProposalContNo());
        tLCRReportDB.setPrtSeq(mLCRReportSchema.getPrtSeq());
        if (tLCRReportDB.getInfo())
         {
            tLCRReportSchema = tLCRReportDB.getSchema();
            tLCRReportSchema.setContente(mLCRReportSchema.getContente());
            tLCRReportSchema.setModifyDate(PubFun.getCurrentDate());
            tLCRReportSchema.setModifyTime(PubFun.getCurrentTime());
            tLCRReportSchema.setReplyDate(PubFun.getCurrentDate());
            tLCRReportSchema.setReplyTime(PubFun.getCurrentTime());
            tLCRReportSchema.setReplyOperator(tGI.Operator);
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "UWRReportItemBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "LCRReport未查询到信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
      }
       return true;
    }


    /**
     * checkData
     * 数据校验
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

   /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 公用变量
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mOperate =="UPDATE" || mOperate =="DELETE")
        {
          mLCRReportItemSchema.setSchema((LCRReportItemSchema)cInputData.getObjectByObjectName("LCRReportItemSchema",0));
        }
        if (mOperate =="MODIFY")
        {
          mLCRReportSchema.setSchema((LCRReportSchema)cInputData.getObjectByObjectName("LCRReportSchema",0));
        }
        mOperator = tGI.Operator;
        if (mOperator == null || mOperator.length() <= 0)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "RReportResultBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operator失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //
        if (mLCRReportItemSchema == null && mOperate=="UPDATE")
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LcRReportItemBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据LCRReportItem失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mLCRReportSchema == null && mOperate=="MODIFY")
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCGrpContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "LcRReportItemBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据LCRReport失败!";
            this.mErrors.addOneError(tError);
            return false;
        }



        return true;
    }

    /**
     * 返回结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

}
