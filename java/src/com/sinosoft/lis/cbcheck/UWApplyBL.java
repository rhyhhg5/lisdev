package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: 人工核保申请 </p>
 * <p>Description: 每个核保员可以申请n个核保单 </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SinoSoft</p>
 * @author HYQ
 * @version 1.0
 */

public class UWApplyBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap map = new MMap();

    /** 往工作流引擎中传输数据的容器 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //private VData mIputData = new VData();
    private TransferData mTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperator;

    private String mManageCom;

    private String mOperate;

    private String mPrtNo;

    private String mContNo;

    private String mApplyType; //申请类型  1-个险 2-团险

    private int mApplyNo; //前台传输的需要申请的保单数

    private int mNo; //实际申请的个数
    public static ExeSQL tExeSQL = new ExeSQL();

    /**  保单渠道 */
    private String mContSaleChnlType = null;

    /** 业务操作类 */
    private LWMissionSet mLWMissionSet = new LWMissionSet();

    private LWMissionSet mNewLWMissionSet = new LWMissionSet();

    private LCUWSendTraceSet tLCUWSendTraceSet = new LCUWSendTraceSet();
    

    public UWApplyBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        //校验是否有未打印的体检通知书
        if (!checkData())
        {
            return false;
        }

        System.out.println("Start  dealData...");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("dealData successful!");

        System.out.println("Start  Submit...");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mResult, ""))
        {
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据库提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("End Submit....");
        return true;
    }

    /**
     * dealData
     * 处理数据
     * @return boolean
     */
    private boolean dealData()
    {
        String tSql = "";
        String mSql="";
        SSRS sqlStrjs = null;
        StringBuffer sql = new StringBuffer(255);
        LWMissionDB tLWMissionDB = new LWMissionDB();
        String uwUsql1 = "";
        String uwUsql2 = "";
        String uwSql = "select trim(code1) from ldcode1 where codetype='uwuserconf' and code='"+mOperator+"' ";
        SSRS uwSSR= new ExeSQL().execSQL(uwSql);
        if(uwSSR.getMaxRow()>0){
        	uwUsql1 = " and substr(lcc.managecom,1,4) in (select code1 from ldcode1 where codetype='uwuserconf' and code='"+mOperator+"' ) ";
        	String sqlstr = "";
            for(int i=1;i<=uwSSR.getMaxRow();i++){
            	sqlstr+=" ll.mngcom like '"+uwSSR.GetText(i, 1)+"%' ";
            	if(i!=uwSSR.getMaxRow()){
            		sqlstr+= " or ";
            	}
            }
            uwUsql2 = " and exists (select 1 from llcase ll where ll.caseno=aa.missionprop1 and ("+sqlstr+") )";
        }
        //查出所有待核保,且未被申请的工作
        if (mApplyType.trim().equals("1"))
        {
            String tCondSaleChnlType = "";
            //个单新契约人工核保
            if ("00".equals(mContSaleChnlType))
            {
                tCondSaleChnlType = " and 1 = 1 ";
            }
            else if ("01".equals(mContSaleChnlType))
            {
                tCondSaleChnlType = " and lcc.SaleChnl in ('01', '07','03','10')";
            }
            else if ("02".equals(mContSaleChnlType))
            {
                tCondSaleChnlType = " and lcc.SaleChnl in ('04', '13')";
            }
            else if ("99".equals(mContSaleChnlType))
            {
                tCondSaleChnlType = " and lcc.SaleChnl not in ('01', '07', '04', '13')";
            }
            else 
            {
                tCondSaleChnlType = " and 1 = 2 ";
            }
            tSql = " select * "
                    + " from lwmission lwm "
                    + " inner join LCCont lcc on lcc.ContNo = lwm.MissionProp2 "
                    + " where 1 = 1 "
                    + " and lwm.ActivityId in ('0000001100', '0000001180','0000001181') "
                    + " and (lwm.DefaultOperator is null or trim(lwm.DefaultOperator) = '') "
                    + " and lwm.MissionProp10 like '"
                    + mGlobalInput.ManageCom
                    + "%' "
                    + tCondSaleChnlType
                    + uwUsql1
                    + " order by lwm.ActivityId desc, lwm.ModifyDate asc, lwm.ModifyTime asc " ;

        }
        else if (mApplyType.trim().equals("2"))
        { //团单新契约人工核保
        	String tCondSaleChnlType = "";
            //个单新契约人工核保
            if ("01".equals(mContSaleChnlType))
            {
                tCondSaleChnlType = " and lcg.SaleChnl='16' "; //社保直销渠道
                System.out.println(mGlobalInput.ManageCom);
            }
            else if ("02".equals(mContSaleChnlType))
            {
                tCondSaleChnlType = " and lcg.SaleChnl <>'16'"; //非社保直销渠道
            }
            tSql = "select * from lwmission lw inner join lcgrpcont lcg on lw.MissionProp1=lcg.grpcontno where 1=1 "
                    + " and lw.activityid in ('0000002004') " //将来会加入续保、保全核保等的工作流节点
                    + " and (lw.defaultoperator is null or trim(lw.defaultoperator)='') "
                    + " and lw.MissionProp4 like '" + mGlobalInput.ManageCom
                    + "%'" 
                    + tCondSaleChnlType
                    +" and lcg.approvecode<> '"+mOperator+"'"
                    +" order by lw.modifydate asc,lw.modifytime asc";
             mSql= "select * from lwmission lw inner join lcgrpcont lcg on lw.MissionProp1=lcg.grpcontno where 1=1 "
                    + " and lw.activityid in ('0000002004') " //将来会加入续保、保全核保等的工作流节点
                    + " and (lw.defaultoperator is null or trim(lw.defaultoperator)='') "
                    + " and lw.MissionProp4 like '" + mGlobalInput.ManageCom
                    + "%'" 
                    + tCondSaleChnlType
                    +" order by lw.modifydate asc,lw.modifytime asc";
             
        }
        else if (mApplyType.trim().equals("3"))
        { //询价人工核保
            tSql = "select * from lwmission where 1=1 "
                    + " and activityid in ('0000006004') " //将来会加入续保、保全核保等的工作流节点
                    + " and (defaultoperator is null or trim(defaultoperator)='') "
                    + " and MissionProp4 like '" + mGlobalInput.ManageCom
                    + "%'" + " order by modifydate asc,modifytime asc";
        }

        System.out.println("Sql=" + tSql);
        mLWMissionSet = tLWMissionDB.executeQuery(tSql);
        if(mSql!=""&&mSql!=null){
        sqlStrjs = tExeSQL.execSQL(mSql);
        if (sqlStrjs == null || sqlStrjs.getMaxRow() <= 0) {
        	CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "当前管理机构下没有待核保的保单或查询失败!";
            this.mErrors.addOneError(tError);
            return false;
		}
        
        if((mLWMissionSet == null || mLWMissionSet.size() <= 0)&&sqlStrjs.MaxRow>0){
        	CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "核保池中投保单均是由您（工号："+mOperator+"）复核通过的，您不能进行核保申请！!";
            this.mErrors.addOneError(tError);
            return false;
        }
        }
        
       String LPSQL =  " select * from lwmission aa where  " +
        " aa.ActivityId = '0000001182' " +
        " and (aa.DefaultOperator is null or trim(aa.DefaultOperator) = '') "
        + uwUsql2;
       System.out.println("LPSQL=" + LPSQL);
       LWMissionDB ttLWMissionDB = new LWMissionDB();
       LWMissionSet ttLWMissionSet = ttLWMissionDB.executeQuery(LPSQL);
       if(ttLWMissionSet!=null&&ttLWMissionSet.size()>0){
//    	   add lyc #2057 2014-07-25  理赔二核只能有86机构的用户申请
    	   String LPRHSQL = "select comcode from lduser where  "+
    	   " usercode = '" +mOperator+ "'" ;
    	   System.out.println("---------"+LPRHSQL+"");
    	   ExeSQL tExeSQL = new ExeSQL();
   		   String tResult = tExeSQL.getOneValue(LPRHSQL);
    	   if("86".equals(tResult)){
    		   mLWMissionSet.add(ttLWMissionSet) ;
    	   }
//    	  add lyc
       }
       
        if (mLWMissionSet == null || mLWMissionSet.size() <= 0)
        {
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "当前管理机构下没有待核保的保单!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            if (mNo > mLWMissionSet.size())
            {
                mNo = mLWMissionSet.size(); //如果核保池中的待核保工作数小于申请数，则申请数=待核保数
            }
        }

        LWMissionSchema tLWMissionSchema;
        for (int i = 1; i <= mNo; i++)
        {
            tLWMissionSchema = new LWMissionSchema();
            tLWMissionSchema = mLWMissionSet.get(i);
            if("0000001182".equals(tLWMissionSchema.getActivityID())){
            	tLWMissionSchema.setMissionProp4(mOperator);
            	tLWMissionSchema.setMissionProp9(mOperator);
            	tLWMissionSchema.setMissionProp5(PubFun.getCurrentDate());
            }
            tLWMissionSchema.setDefaultOperator(mOperator);
            tLWMissionSchema.setMissionProp19(PubFun.getCurrentDate());
            tLWMissionSchema.setMissionProp18(PubFun.getCurrentTime());
            mNewLWMissionSet.add(tLWMissionSchema);
            //更新各表中的核保师
            map.put("update LCUWMaster set operator='" + mOperator
                    + "' where contno='" + tLWMissionSchema.getMissionProp2()
                    + "'", "UPDATE");
            map.put("update LCUWSub set operator='" + mOperator
                    + "' where contno='" + tLWMissionSchema.getMissionProp2()
                    + "'", "UPDATE");
            map.put("update LCCUWMaster set operator='" + mOperator
                    + "' where contno='" + tLWMissionSchema.getMissionProp2()
                    + "'", "UPDATE");
            map.put("update LCCUWSub set operator='" + mOperator
                    + "' where contno='" + tLWMissionSchema.getMissionProp2()
                    + "'", "UPDATE");

            //为配合规则引擎取数，保全核保不设置UWDate为空
            if (!"0000001180".equals(tLWMissionSchema.getActivityID()))
            {
                map.put("update LCCont set UWOperator='" + mOperator
                        + "',uwdate=null,uwtime=null where contno='"
                        + tLWMissionSchema.getMissionProp2() + "'", "UPDATE");
                map.put("update LCpol set UWCode='" + mOperator
                        + "',uwdate=null,uwtime=null where contno='"
                        + tLWMissionSchema.getMissionProp2() + "'", "UPDATE");
            }

            int uwno = 0; //核保顺序号
            LCUWSendTraceDB tLCUWSendTraceDB = new LCUWSendTraceDB();
            tLCUWSendTraceDB.setUWNo(0);
            tLCUWSendTraceDB.setOtherNo(tLWMissionSchema.getMissionProp2());
            tLCUWSendTraceDB.setOtherNoType("1");
            if (!tLCUWSendTraceDB.getInfo())
            {
                LCUWSendTraceSchema tLCUWSendTraceSchema = new LCUWSendTraceSchema();
                tLCUWSendTraceSchema.setUpUserCode(mOperator);
                tLCUWSendTraceSchema.setDownUWCode(mOperator);
                tLCUWSendTraceSchema.setOperator(mOperator);
                tLCUWSendTraceSchema.setUWCode(mOperator);
                if("0000001181".equals(tLWMissionSchema.getActivityID())){
                	tLCUWSendTraceSchema.setUWNo(i-1);
                }else{
                	tLCUWSendTraceSchema.setUWNo(uwno);
                }
                tLCUWSendTraceSchema.setOtherNo(tLWMissionSchema
                        .getMissionProp2());
                tLCUWSendTraceSchema.setOtherNoType("1");
                tLCUWSendTraceSchema.setSendType("6");
                tLCUWSendTraceSchema.setSendFlag("1");
                tLCUWSendTraceSchema.setManageCom(mManageCom);
                tLCUWSendTraceSchema.setMakeDate(PubFun.getCurrentDate());
                tLCUWSendTraceSchema.setMakeTime(PubFun.getCurrentTime());
                tLCUWSendTraceSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUWSendTraceSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUWSendTraceSet.add(tLCUWSendTraceSchema);
            }
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        //        LWMissionSet tLWMissionSet = new LWMissionSet();
        //        LWMissionDB tLWMissionDB = new LWMissionDB();
        //        //查出此核保师所拥有的所有待核保工作
        //        String tSql = "select * from lwmission where 1=1 "
        //                      + " and activityid in ('0000001100') "      //将来会加入续保、保全核保等的工作流节点
        //                      + " and defaultoperator = '" + mOperator + "'"
        //                      ;
        //        System.out.println("Sql="+tSql);
        //        tLWMissionSet = tLWMissionDB.executeQuery(tSql);
        //        if (tLWMissionSet.size() > mApplyNo)
        //        {
        //            // @@错误处理
        //            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
        //            CError tError = new CError();
        //            tError.moduleName = "UWApplyBL";
        //            tError.functionName = "checkData";
        //            tError.errorMessage = "您的工作池中已有"+mApplyNo+"个待核保保单，不能申请！";
        //            this.mErrors.addOneError(tError);
        //            return false;
        //        }
        //        mNo = mApplyNo - tLWMissionSet.size(); //还需要申请的工作数
        mNo = mApplyNo; //mod by heyq 20040206  不控制工作任务数
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        map.put(mNewLWMissionSet, "UPDATE");
        map.put(tLCUWSendTraceSet, "INSERT");

        mResult.add(map);

        return true;
    }

    /**
     * getInputData
     * 得到前台传输的数据
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData, String cOperate)
    {
        //从输入数据中得到所有对象
        //获得全局公共数据
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mGlobalInput == null)
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //获得操作员编码
        mOperator = mGlobalInput.Operator;
        if (mOperator == null || mOperator.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mManageCom = mGlobalInput.Operator;
        if (mManageCom == null || mManageCom.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据mManageCom失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tNo = (String) mTransferData.getValueByName("ApplyNo");
        if (tNo == null || tNo.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据ApplyNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mApplyNo = Integer.parseInt(tNo);

        mApplyType = (String) mTransferData.getValueByName("ApplyType");
        if (mApplyType == null || mApplyType.trim().equals(""))
        {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCContDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "UWApplyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输数据ApplyType失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mContSaleChnlType = (String) mTransferData
                .getValueByName("ContSaleChnlType");

        return true;
    }

}
