package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;

/**
 * <p>Title: Web业务系统承保个人单状态查询部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class PolStatusChkBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mpassflag; //通过标记
    private int merrcount; //错误条数
    private String mCalCode; //计算编码
    private String mUser;
    private FDate fDate = new FDate();
    private float mValue;
    private String muwgrade = "";

    /** 业务处理相关变量 */
    private LCContSet mLCContSet = new LCContSet();
    private LCContSet mmLCContSet = new LCContSet();
    private LCContSet m2LCContSet = new LCContSet();
    private LCContSet mAllLCContSet = new LCContSet();
    private LCContSchema mLCContSchema = new LCContSchema();
    private String mPolNo = "";
    private String mOldPolNo = "";
    /** 保费项表 */
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSet mAllLCPremSet = new LCPremSet();
    /** 领取项表 */
    private LCGetSet mLCGetSet = new LCGetSet();
    private LCGetSet mAllLCGetSet = new LCGetSet();
    /** 责任表 */
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySet mAllLCDutySet = new LCDutySet();
    /** 特别约定表 */
    private LCSpecSet mLCSpecSet = new LCSpecSet();
    private LCSpecSet mAllLCSpecSet = new LCSpecSet();
    /** 特别约定注释表 */
    private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
    private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();
    /** 核保主表 */
    private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
    private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();
    /** 核保子表 */
    private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
    private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
    private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();
    /** 核保错误信息表 */
    private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
    private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();
    /** 告知表 */
    private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
    private LCCustomerImpartSet mAllLCCustomerImpartSet = new
            LCCustomerImpartSet();
    /** 投保人表 */
    private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
    private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();
    /** 受益人表 */
    private LCBnfSet mLCBnfSet = new LCBnfSet();
    private LCBnfSet mAllLCBnfSet = new LCBnfSet();
    /** 被保险人表 */
    private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
    private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();
    /**计算公式表**/
    private LMUWSchema mLMUWSchema = new LMUWSchema();
    private LMUWSet mLMUWSet = new LMUWSet();
    private LMUWSet m2LMUWSet = new LMUWSet();
    private LMUWSet mmLMUWSet = new LMUWSet();

    private LMCalModeSet mmLMCalModeSet = new LMCalModeSet();
    private LMCalModeSet mLMCalModeSet = new LMCalModeSet();

    private CalBase mCalBase = new CalBase();

    public PolStatusChkBL() {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        int flag = 0; //判断是不是所有数据都不成功
        int j = 0; //符合条件数据个数

        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        //GlobalInput tGlobalInput = new GlobalInput();
        //this.mOperate = tGlobalInput.;

        System.out.println("---1---");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("---PolStatusChkBL getInputData---");
        LCContDB tLCContDB = new LCContDB();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = (LCContSchema) mLCContSet.get(1);
        mLCContSchema = (LCContSchema) mLCContSet.get(1);
        String tPrtNo = mLCContSchema.getPrtNo();
        tLCContDB.setContNo(mLCContSchema.getContNo());
        if (!tLCContDB.getInfo()) {
//            this.mErrors.copyAllErrors(tLCContDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "PolStatusChkBL";
//            tError.functionName = "getInputData";
//            tError.errorMessage = "投保单查询失败!";
//            this.mErrors.addOneError(tError);
//            return false;
        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        System.out.println(" tPrtNo: " + tPrtNo);
        mLCContSchema.setPrtNo(tPrtNo);
        String tProposalNo = tLCContSchema.getProposalContNo();
        mOldPolNo = tLCContSchema.getProposalContNo();
        mPolNo = tLCContSchema.getProposalContNo();

        // 校验数据

        //校验主附险

        System.out.println("---PolStatusChkBL checkData---");
        // 数据操作业务处理
        if (!dealData(tLCContSchema)) {
            return false;
        }

        System.out.println("---PolStatusChkBL dealData---");
        //准备返回的数据
        prepareOutputData();

        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData(LCContSchema tLCContSchema) {
        mOldPolNo = tLCContSchema.getContNo(); //冗余的代码

        //准备算法
        if (CheckKinds(tLCContSchema) == false) {
            return false;
        }

        //准备数据
        CheckPolInit(tLCContSchema); //冗余代码

        //取保单信息
        int n = mmLMCalModeSet.size();
        if (n == 0) {
        } else {
            int j = 0;
            mLMCalModeSet.clear();
            for (int i = 1; i <= n; i++) {
                //取计算编码
                LMCalModeSchema tLMCalModeSchema = new LMCalModeSchema();
                tLMCalModeSchema = mmLMCalModeSet.get(i);
                mCalCode = tLMCalModeSchema.getCalCode();
                if (CheckPol() == 0) {
                } else {
                    j++;
                    mLMCalModeSet.add(parseResultFactor(tLMCalModeSchema));
                }
            }
        }
        if (mLMCalModeSet.size() == 0) {
            LMCalModeSchema tLMCalModeSchema = new LMCalModeSchema();
            tLMCalModeSchema.setRemark("未查询到此保单状态，"
                                       + "造成这样的原因可能是由于"
                                       + "在进行数据库操作时造成了数据的丢失！");
            mLMCalModeSet.add(tLMCalModeSchema);
        }
        if (dealOnePol() == false) { //冗余代码
            return false;
        }

        return true;
    }

    /**
     * 根据保额校验核保级别
     * @return
     */
    private String checkRiskAmnt() {
        String tUWGrade = "";
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);

        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
        //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
        //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
        mCalculator.addBasicFactor("ContNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", mLCContSchema.getInsuredNo()); ;

        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            tUWGrade = "";
        } else {
            tUWGrade = tStr.trim();
        }

        System.out.println("AmntGrade:" + tUWGrade);

        return tUWGrade;
    }

    /**
     * 操作一张保单的业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealOnePol() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mOperate = tGlobalInput.Operator;

        mLCContSet.set((LCContSet) cInputData.getObjectByObjectName("LCContSet",
                0));
        int n = mLCContSet.size();
        System.out.println("dddddddddd=" + n);
        int flag = 0; //怕判断是不是所有保单都失败
        int j = 0; //符合条件保单个数

        if (n > 0) {
            LCContDB tLCContDB = new LCContDB();
            LCContSchema tLCContSchema = mLCContSet.get(1);
            tLCContDB.setContNo(tLCContSchema.getContNo());
            String temp = tLCContSchema.getContNo();
            System.out.println("temp" + temp);
            LCContSet tLCContSet=tLCContDB.executeQuery("select * from lccont where contno='"+temp+"' with ur");
            if (tLCContSet.size()!=1) {
                System.out.println("根据contno"+temp+"查询失败");
            }
//            if (tLCContDB.getInfo() == false) {
                // @@错误处理
//                this.mErrors.copyAllErrors(tLCContDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "PolStatusChkBL";
//                tError.functionName = "getInputData";
//                tError.errorMessage = temp + "投保单查询失败!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
        } else {
            CError tError = new CError();
            tError.moduleName = "PolStatusChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据传输失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 校验投保单是否复核
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkApprove(LCContSchema tLCContSchema) {
        if (tLCContSchema.getApproveCode() == null ||
            tLCContSchema.getApproveDate() == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "checkApprove";
            tError.errorMessage = "投保单尚未进行复核操作，不能核保!（投保单号：" +
                                  tLCContSchema.getContNo().trim() + "）";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 校验核保员级别
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkUWGrade() {
        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mOperate);

        if (!tLDUserDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "无此操作员信息，不能核保!（操作员：" + mOperate + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tUWPopedom = tLDUserDB.getUWPopedom();
        if (tUWPopedom.equals("")) {
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "checkUWGrade";
            tError.errorMessage = "操作员无核保权限，不能核保!（操作员：" + mOperate + "）";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 核保险种信息校验,准备核保算法
     * 输出：如果发生错误则返回false,否则返回true
     */

    private boolean CheckKinds(LCContSchema tLCContSchema) {
        String tsql = "";
        mmLMCalModeSet.clear();
        LMCalModeSchema tLMCalModeSchema = new LMCalModeSchema();
        //查询算法编码
        tsql =
                "select * from LMCalMode where riskcode = 'Status' order by calcode with ur";

        LMCalModeDB tLMCalModeDB = new LMCalModeDB();
        //tLMUWDB.setSchema(tLMUWSchema);

        //LMUWDBSet tLMUWDBSet = new LMUWDBSet();
        mmLMCalModeSet = tLMCalModeDB.executeQuery(tsql);
        if (tLMCalModeDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLMCalModeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAutoChkBL";
            tError.functionName = "CheckKinds";

            this.mErrors.addOneError(tError);
            mLMUWSet.clear();
            return false;
        }
        return true;
    }


    /**
     * 个人单核保数据准备
     * 输出：如果发生错误则返回false,否则返回true
     */
    private void CheckPolInit(LCContSchema tLCContSchema) {
        mCalBase = new CalBase();
        mCalBase.setPrem(tLCContSchema.getPrem());
        mCalBase.setGet(tLCContSchema.getAmnt());
        mCalBase.setMult(tLCContSchema.getMult());
        //mCalBase.setYears( tLCContSchema.getYears() );

        mCalBase.setPolNo(tLCContSchema.getProposalContNo());
    }

    /**
     * 个人单核保
     * 输出：如果发生错误则返回false,否则返回true
     */
    private float CheckPol() {
        // 计算
        Calculator mCalculator = new Calculator();
        mCalculator.setCalCode(mCalCode);
        //增加基本要素
        mCalculator.addBasicFactor("Get", mCalBase.getGet());
        mCalculator.addBasicFactor("Mult", mCalBase.getMult());
        mCalculator.addBasicFactor("Prem", mCalBase.getPrem());
        //mCalculator.addBasicFactor("PayIntv", mCalBase.getPayIntv() );
        //mCalculator.addBasicFactor("GetIntv", mCalBase.getGetIntv() );
        mCalculator.addBasicFactor("AppAge", mCalBase.getAppAge());
        mCalculator.addBasicFactor("Sex", mCalBase.getSex());
        mCalculator.addBasicFactor("Job", mCalBase.getJob());
        mCalculator.addBasicFactor("PayEndYear", mCalBase.getPayEndYear());
        mCalculator.addBasicFactor("GetStartDate", "");
        //mCalculator.addBasicFactor("GetYear", mCalBase.getGetYear() );
        mCalculator.addBasicFactor("Years", mCalBase.getYears());
        mCalculator.addBasicFactor("Grp", "");
        mCalculator.addBasicFactor("GetFlag", "");
        mCalculator.addBasicFactor("ValiDate", "");
        mCalculator.addBasicFactor("Count", mCalBase.getCount());
        mCalculator.addBasicFactor("FirstPayDate", "");
        //mCalculator.addBasicFactor("AddRate", mCalBase.getAddRate() );
        //mCalculator.addBasicFactor("GDuty", mCalBase.getGDuty() );
        mCalculator.addBasicFactor("ContNo", mCalBase.getPolNo());
        mCalculator.addBasicFactor("InsuredNo", mLCContSchema.getInsuredNo());
        mCalculator.addBasicFactor("PrtNo", mLCContSchema.getPrtNo());
        String tStr = "";
        tStr = mCalculator.calculate();
        if (tStr.trim().equals("")) {
            mValue = 0;
        } else {
            mValue = Float.parseFloat(tStr);
        }

        System.out.println(mValue);
        return mValue;
    }


    /**
     * 准备保单信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean preparePol() {
        System.out.println("核保标志" + mpassflag);
        mLCContSchema.setUWFlag(mpassflag); //待人工核保
        //mLCContSchema.setUWCode(mOperate);
        mLCContSchema.setUWDate(PubFun.getCurrentDate());

        return true;
    }

    /**
     * 准备核保信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean prepareUW() {
        int tuwno = 0;
        LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
        LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB();
        tLCUWMasterDB.setPolNo(mOldPolNo);
        LCUWMasterSet tLCUWMasterSet = new LCUWMasterSet();
        tLCUWMasterSet = tLCUWMasterDB.query();
        if (tLCUWMasterDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保总表取数失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int n = tLCUWMasterSet.size();
        if (n == 0) {
            tLCUWMasterSchema.setPolNo(mPolNo);
            tLCUWMasterSchema.setProposalNo(mPolNo);
            tLCUWMasterSchema.setUWNo(1);
            tLCUWMasterSchema.setContNo("");
//			tLCUWMasterSchema.setGrpPolNo("00000000000000000000");
//			tLCUWMasterSchema.setRiskVersion(mLCContSchema.getRiskVersion());
//			tLCUWMasterSchema.setRiskCode(mLCContSchema.getRiskCode());
            tLCUWMasterSchema.setInsuredNo(mLCContSchema.getInsuredNo());
            tLCUWMasterSchema.setInsuredName(mLCContSchema.getInsuredName());
            tLCUWMasterSchema.setAppntNo(mLCContSchema.getAppntNo());
            tLCUWMasterSchema.setAppntName(mLCContSchema.getAppntName());
            tLCUWMasterSchema.setAgentCode(mLCContSchema.getAgentGroup());
            tLCUWMasterSchema.setAgentGroup(mLCContSchema.getAgentGroup());
            tLCUWMasterSchema.setPassFlag(mpassflag); //通过标志
            tLCUWMasterSchema.setUWGrade(muwgrade); //核保级别
            tLCUWMasterSchema.setAppGrade(muwgrade); //申报级别
            tLCUWMasterSchema.setPostponeDay("");
            tLCUWMasterSchema.setPostponeDate("");
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setState(mpassflag);
            tLCUWMasterSchema.setHealthFlag("0");
            tLCUWMasterSchema.setSpecFlag("0");
            tLCUWMasterSchema.setQuesFlag("0");
            tLCUWMasterSchema.setReportFlag("0");
            tLCUWMasterSchema.setChangePolFlag("0");
            tLCUWMasterSchema.setPrintFlag("0");
            tLCUWMasterSchema.setManageCom(mLCContSchema.getManageCom());
            tLCUWMasterSchema.setUWIdea("");
            tLCUWMasterSchema.setUpReportContent("");
            tLCUWMasterSchema.setOperator(mOperate); //操作员
            tLCUWMasterSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        } else if (n == 1) {

            tLCUWMasterSchema = tLCUWMasterSet.get(1);

            tuwno = tLCUWMasterSchema.getUWNo();
            tuwno = tuwno + 1;

            tLCUWMasterSchema.setUWNo(tuwno);
            tLCUWMasterSchema.setPassFlag(mpassflag); //通过标志
            tLCUWMasterSchema.setAutoUWFlag("1"); // 1 自动核保 2 人工核保
            tLCUWMasterSchema.setUWGrade(muwgrade); //核保级别
            tLCUWMasterSchema.setAppGrade(muwgrade); //申报级别
            tLCUWMasterSchema.setOperator(mOperate); //操作员
            tLCUWMasterSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWMasterSchema.setModifyTime(PubFun.getCurrentTime());
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保总表取数据不唯一!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCUWMasterSet.clear();
        mLCUWMasterSet.add(tLCUWMasterSchema);

        // 核保轨迹表
        LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
        LCUWSubDB tLCUWSubDB = new LCUWSubDB();
        tLCUWSubDB.setPolNo(mOldPolNo);
        LCUWSubSet tLCUWSubSet = new LCUWSubSet();
        tLCUWSubSet = tLCUWSubDB.query();
        if (tLCUWSubDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人核保轨迹表查失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        int m = tLCUWSubSet.size();
        if (m > 0) {
            m++; //核保次数
            tLCUWSubSchema = tLCUWSubSet.get(1);

            tLCUWSubSchema.setUWNo(m); //第几次核保
//                        tLCUWSubSchema.setUWFlag(mpassflag); //核保意见
            tLCUWSubSchema.setUWGrade(muwgrade); //核保级别
            tLCUWSubSchema.setAppGrade(muwgrade); //申请级别
            tLCUWSubSchema.setAutoUWFlag("1");
            tLCUWSubSchema.setState(mpassflag);
            tLCUWSubSchema.setOperator(mOperate); //操作员
            tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
        } else {
            tLCUWSubSchema.setProposalNo(mPolNo);
            tLCUWSubSchema.setUWNo(1);
            tLCUWSubSchema.setPolNo(mPolNo);
            tLCUWSubSchema.setManageCom(mLCContSchema.getManageCom());
//			tLCUWSubSchema.setUWFlag(mpassflag); //核保意见
            tLCUWSubSchema.setUWGrade(muwgrade); //核保级别
            tLCUWSubSchema.setAppGrade(muwgrade); //申请级别
            tLCUWSubSchema.setPostponeDay("");
            tLCUWSubSchema.setPostponeDate("");
            tLCUWSubSchema.setAutoUWFlag("1");
            tLCUWSubSchema.setUWIdea("");
            tLCUWSubSchema.setUpReportContent("");
            tLCUWSubSchema.setState(mpassflag);
            tLCUWSubSchema.setOperator(mOperate); //操作员
            tLCUWSubSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUWSubSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUWSubSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUWSubSchema.setModifyTime(PubFun.getCurrentTime());
        }

        mLCUWSubSet.clear();
        mLCUWSubSet.add(tLCUWSubSchema);

        // 核保错误信息表
        LCUWErrorSchema tLCUWErrorSchema = new LCUWErrorSchema();
        LCUWErrorDB tLCUWErrorDB = new LCUWErrorDB();
        tLCUWErrorDB.setPolNo(mOldPolNo);
        LCUWErrorSet tLCUWErrorSet = new LCUWErrorSet();
        tLCUWErrorSet = tLCUWErrorDB.query();
        if (tLCUWErrorDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCUWErrorDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "prepareUW";
            tError.errorMessage = mOldPolNo + "个人错误信息表查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        /**
           int k = tLCUWErrorSet.size();

           if ( k > 0)
           {
                              k++; //核保次数
         tLCUWErrorSchema = mLCUWErrorSet.get(k);

                              tLCUWErrorSchema.setUWNo(k); //核保次数
           }
           else
           {
         **/
        tLCUWErrorSchema.setSerialNo("0");
        if (m > 0) {
            tLCUWErrorSchema.setUWNo(m);
        } else {
            tLCUWErrorSchema.setUWNo(1);
        }
        tLCUWErrorSchema.setPolNo(mPolNo);
        tLCUWErrorSchema.setContNo("");
//			tLCUWErrorSchema.setGrpPolNo("");
        tLCUWErrorSchema.setInsuredNo(mLCContSchema.getInsuredNo());
        tLCUWErrorSchema.setInsuredName(mLCContSchema.getInsuredName());
        tLCUWErrorSchema.setAppntNo(mLCContSchema.getAppntNo());
        tLCUWErrorSchema.setAppntName(mLCContSchema.getAppntName());
        tLCUWErrorSchema.setManageCom(mLCContSchema.getManageCom());
        tLCUWErrorSchema.setUWRuleCode(""); //核保规则编码
        tLCUWErrorSchema.setUWError(""); //核保出错信息
        tLCUWErrorSchema.setCurrValue(""); //当前值
        tLCUWErrorSchema.setModifyDate(PubFun.getCurrentDate());
        tLCUWErrorSchema.setModifyTime(PubFun.getCurrentTime());
        tLCUWErrorSchema.setUWPassFlag(mpassflag);

        //}

        //取核保错误信息
        mLCUWErrorSet.clear();
        merrcount = mLMUWSet.size();
        if (merrcount > 0) {
            for (int i = 1; i <= merrcount; i++) {
                //取出错信息
                LMUWSchema tLMUWSchema = new LMUWSchema();
                tLMUWSchema = mLMUWSet.get(i);
                //生成流水号
                String tserialno = "" + i;

                tLCUWErrorSchema.setSerialNo(tserialno);
                tLCUWErrorSchema.setUWRuleCode(tLMUWSchema.getUWCode()); //核保规则编码
                tLCUWErrorSchema.setUWError(tLMUWSchema.getRemark().trim()); //核保出错信息
                tLCUWErrorSchema.setUWGrade(tLMUWSchema.getUWGrade());
                tLCUWErrorSchema.setCurrValue(""); //当前值

                LCUWErrorSchema ttLCUWErrorSchema = new LCUWErrorSchema();
                ttLCUWErrorSchema.setSchema(tLCUWErrorSchema);
                mLCUWErrorSet.add(ttLCUWErrorSchema);
            }
        }

        return true;
    }

    /**
     * 准备需要保存的数据
     */
    private void prepareOutputData() {
        mResult.clear();
        mResult.add(mLMCalModeSet);
    }

    public VData getResult() {
        return mResult;
    }
    /**
     * parseResultFactor
     *
     * @param tLMCalModeSchema LMCalModeSchema
     * @return Schema
     */
    private LMCalModeSchema parseResultFactor(LMCalModeSchema tLMCalModeSchema) {
        PubCalculator tPubCalculator = new PubCalculator();
        
        tPubCalculator.addBasicFactor("ContNo",mCalBase.getPolNo());
        tPubCalculator.addBasicFactor("PrtNo",mLCContSchema.getPrtNo());
        
        String tStr = "";
        String tRemark = tLMCalModeSchema.getRemark();
        String tSql = tLMCalModeSchema.getRemark();
        String tStr1 = "";
        try {
            while (true) {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals("")) {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量
                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UWAtuoChkBL";
            tError.functionName = "parseUWResultFactor";
            tError.errorMessage = "解析要素失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tSql.trim().equals("")) {
            tLMCalModeSchema.setRemark(tRemark);
        } else {
            tLMCalModeSchema.setRemark(tSql);
        }
        return tLMCalModeSchema;
    }
    public void testparseResultFactor(){
    	LMCalModeDB tLMCalModeDB = new LMCalModeDB();
    	tLMCalModeDB.setCalCode("S00065");
    	tLMCalModeDB.getInfo();
    	mCalBase.setPolNo("13000540775");
    	mLCContSchema.setPrtNo("160000000388");
    	parseResultFactor(tLMCalModeDB);
    }
    public static void main(String[] args) {
    	PolStatusChkBL tPolStatusChkBL = new PolStatusChkBL();
    	tPolStatusChkBL.testparseResultFactor();
		
	}
}
