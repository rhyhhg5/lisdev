package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;

/**
 * <p>Title: Web业务系统集体充帐部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class GrpFeeBackBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private String mIsueManageCom;
  private String mManageCom;
  private String mpassflag; //通过标记
  private int merrcount; //错误条数
  private String mCalCode; //计算编码
  private String mUser;
  private FDate fDate = new FDate();
  private double mValue;
  private String mInsuredNo = "";
  private String mBackObj = "";
  private String mflag = ""; //分，个单标记
  private String mTempFeeNo = ""; //交费收据号

  /** 业务处理相关变量 */
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSet mmLCPolSet = new LCPolSet();
  private LCPolSet m2LCPolSet = new LCPolSet();
  private LCPolSet mAllLCPolSet = new LCPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private String mPolNo = "";
  private String mOldPolNo = "";

  /** 集体单表 */
  private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

  /** 保费项表 */
  private LCPremSet mLCPremSet = new LCPremSet();
  private LCPremSet mAllLCPremSet = new LCPremSet();

  /** 领取项表 */
  private LCGetSet mLCGetSet = new LCGetSet();
  private LCGetSet mAllLCGetSet = new LCGetSet();

  /** 责任表 */
  private LCDutySet mLCDutySet = new LCDutySet();
  private LCDutySet mAllLCDutySet = new LCDutySet();

  /** 特别约定表 */
  private LCSpecSet mLCSpecSet = new LCSpecSet();
  private LCSpecSet mAllLCSpecSet = new LCSpecSet();

  /** 特别约定注释表 */
  private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
  private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();

  /** 核保主表 */
  private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();

  /** 核保子表 */
  private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
  private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
  private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();

  /** 核保错误信息表 */
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
  private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();

  /** 告知表 */
  private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
  private LCCustomerImpartSet mAllLCCustomerImpartSet = new LCCustomerImpartSet();

  /** 投保人表 */
  private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
  private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();

  /** 受益人表 */
  private LCBnfSet mLCBnfSet = new LCBnfSet();
  private LCBnfSet mAllLCBnfSet = new LCBnfSet();

  /** 被保险人表 */
  private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
  private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();

  /** 体检资料主表 */
  private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
  private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();
  private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();

  /** 体检资料项目表 */
  private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
  private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
  private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();

  /** 问题件表 */
  private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
  private LCIssuePolSet mmLCIssuePolSet = new LCIssuePolSet();
  private LCIssuePolSet mAllLCIssuePolSet = new LCIssuePolSet();

  /** 暂交费表 */
  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeSet mAllLJTempFeeSet = new LJTempFeeSet();

  /** 暂交费关联表 */
  private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mmLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mAllLJTempFeeClassSet = new LJTempFeeClassSet();

  /** 分红计算表 */
  private LOBonusMainSchema mLOBonusMainSchema = new LOBonusMainSchema();
  private LOBonusMainSet mLOBonusMainSet = new LOBonusMainSet();
  private LOBonusMainSet mmLOBonusMainSet = new LOBonusMainSet();
  private LOBonusMainSet m2LOBonusMainSet = new LOBonusMainSet();
  private LOBonusMainSet mAllLOBonusMainSet = new LOBonusMainSet();

  /** 实收总表 */
  private LJAPaySet mLJAPaySet = new LJAPaySet();
  private LJAPaySet mAllLJAPaySet = new LJAPaySet();

  /** 个人实收表 */
  private LJAPayPersonSchema mLJAPayPersonSchema = new LJAPayPersonSchema();
  private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
  private LJAPayPersonSet m2LJAPayPersonSet = new LJAPayPersonSet();
  private LJAPayPersonSet mAllLJAPayPersonSet = new LJAPayPersonSet();

  /** 团体实收表 */
  private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
  private LJAPayGrpSet mAllLJAPayGrpSet = new LJAPayGrpSet();

  /** 给付总表*/
  private LJAGetSet mLJAGetSet = new LJAGetSet();
  private LJAGetSet mAllLJAGetSet = new LJAGetSet();

  /** 红利给付表 */
  private LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
  private LJABonusGetSet mAllLJABonusGetSet = new LJABonusGetSet();

  /** 帐户表 */
  private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
  private LCInsureAccSet mAllLCInsureAccSet = new LCInsureAccSet();

  /** 帐户轨迹表 */
  private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
  private LCInsureAccTraceSet mAllLCInsureAccTraceSet = new LCInsureAccTraceSet();

  /** 暂交费退费应付表 */
  private LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
  private LJAGetTempFeeSet mAllLJAGetTempFeeSet = new LJAGetTempFeeSet();

  /**计算公式表**/
  private LMUWSchema mLMUWSchema = new LMUWSchema();

  //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
  private LMUWSet mLMUWSet = new LMUWSet();

  private CalBase mCalBase = new CalBase();

  public GrpFeeBackBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    //GlobalInput tGlobalInput = new GlobalInput();
    //this.mOperate = tGlobalInput.;

    System.out.println("---1---");
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    System.out.println("---GrpFeeBackBL getInputData---");
    if (!checklast())
      return false;

    System.out.println("---GrpFeeBackBL checkData---");
    // 数据操作业务处理
    if (!dealData())
      return false;
    else {
      flag = 1;
    }

    if (flag == 0) {
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "submitData";
      tError.errorMessage = "冲帐失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---GrpFeeBackBL dealData---");
    //准备给后台的数据
    prepareOutputData();

    System.out.println("---GrpFeeBackBL prepareOutputData---");
    //数据提交
    GrpFeeBackBLS tGrpFeeBackBLS = new GrpFeeBackBLS();
    System.out.println("Start GrpFeeBackBL Submit...");
    if (!tGrpFeeBackBLS.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tGrpFeeBackBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("---GrpFeeBackBL commitData---");
    return true;
  }

  /**
   * 校验是不是最后一笔数据冲帐
   */
  private boolean checklast() {
    String tsql = "select * from LJAPayGrp where PayCount > 0 and PayNo = '" +
        mTempFeeNo + "'"; ;
    LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
    LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();

    tLJAPayGrpDB.setPayNo(mTempFeeNo);
    tLJAPayGrpSet = tLJAPayGrpDB.executeQuery(tsql);
    if (tLJAPayGrpSet.size() > 0) {
      for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {
        LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
        tLJAPayGrpSchema = tLJAPayGrpSet.get(i);
        String tGrpPolNo = tLJAPayGrpSchema.getGrpPolNo();

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tGrpPolNo);
        if (tLCGrpPolDB.getInfo() == false) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "GrpFeeBackBL";
          tError.functionName = "checklast";
          tError.errorMessage = "没有保单信息!";
          this.mErrors.addOneError(tError);
          return false;
        }
        else {
          if (tLCGrpPolDB.getPaytoDate() == null) {
            CError tError = new CError();
            tError.moduleName = "GrpFeeBackBL";
            tError.functionName = "checklast";
            tError.errorMessage = "保单信息有误,交至日期为空！";
            this.mErrors.addOneError(tError);
            return false;
          }
          if (!tLCGrpPolDB.getPaytoDate().equals(tLJAPayGrpSchema.
                                                 getCurPayToDate())) {
            CError tError = new CError();
            tError.moduleName = "GrpFeeBackBL";
            tError.functionName = "checklast";
            tError.errorMessage = "不是最后一笔财务收费，不能冲帐!";
            this.mErrors.addOneError(tError);
            return false;
          }
        }
      }
    }
    else {
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "checklast";
      tError.errorMessage = "没有保费实收记录!";
      this.mErrors.addOneError(tError);
      return false;
    }

    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    tLJTempFeeDB.setTempFeeNo(mTempFeeNo);
    tLJTempFeeSet = tLJTempFeeDB.query();
    if (tLJTempFeeSet.size() > 0) {
      for (int i = 1; i <= tLJTempFeeSet.size(); i++) {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema = tLJTempFeeSet.get(i);
        if (!tLJTempFeeSchema.getTempFeeType().equals("2")) {
          CError tError = new CError();
          tError.moduleName = "GrpFeeBackBL";
          tError.functionName = "checklast";
          tError.errorMessage = "此收据不能不是续期交费收据不能进行冲帐操作!";
          this.mErrors.addOneError(tError);
          return false;
        }
      }
    }

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData() {

    if (dealOnePol() == false)
      return false;

    return true;
  }

  /**
   * 操作一张保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealOnePol() {
    // 健康信息
    if (prepareFee() == false)
      return false;

    if (preparePay() == false)
      return false;

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    GlobalInput tGlobalInput = new GlobalInput();
    tGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mOperate = tGlobalInput.Operator;
    mManageCom = tGlobalInput.ManageCom;

    mLJAPayPersonSchema.setSchema( (LJAPayPersonSchema) cInputData.
                                  getObjectByObjectName("LJAPayPersonSchema", 0));

    int n = mmLOBonusMainSet.size();
    int flag = 0; //怕判断是不是所有保单都失败
    int j = 0; //符合条件保单个数

    if (mLJAPayPersonSchema != null) {
      mTempFeeNo = mLJAPayPersonSchema.getPayNo();
    }
    else {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有传入数据!";
      this.mErrors.addOneError(tError);
      return false;

    }
    return true;
  }

  /**
   * 准备分红信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareFee() {

    /**
     * 集体实交表
     */
    LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
    LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();

    //取个人实收数据
    tLJAPayGrpDB.setPayNo(mTempFeeNo);
    tLJAPayGrpSet = tLJAPayGrpDB.query();
    if (tLJAPayGrpSet.size() > 0) {
    }
    else {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "prepareFee";
      tError.errorMessage = "没有" + mTempFeeNo + "收据记录!";
      this.mErrors.addOneError(tError);
      return false;
    }

//把每条数据对保单进行冲帐处理，包括日期和金额的回退
    for (int i = 1; i <= tLJAPayGrpSet.size(); i++) {
      LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
      LJAPayGrpSchema bLJAPayGrpSchema = new LJAPayGrpSchema();
      tLJAPayGrpSchema = tLJAPayGrpSet.get(i);

      if (tLJAPayGrpSchema.getPayType().equals("ZC")) {
        tLJAPayGrpSchema.setPayType("CZZC");
      }
      if (tLJAPayGrpSchema.getPayType().equals("YET")) {
        tLJAPayGrpSchema.setPayType("CZYET");
      }
      if (tLJAPayGrpSchema.getPayType().equals("YEL")) {
        tLJAPayGrpSchema.setPayType("CZYEL");
      }

      String tGrpPolNo = tLJAPayGrpSchema.getGrpPolNo();
      //String tDutyCode = tLJAPayGrpSchema.getDutyCode();
      //String tPayPlanCode = tLJAPayGrpSchema.getPayPlanCode();
      char tPayNo1[] = tLJAPayGrpSchema.getPayNo().toCharArray();
      tPayNo1[12] = '9';
      String tPayNo = String.valueOf(tPayNo1);
      String tlastdate = tLJAPayGrpSchema.getCurPayToDate();
      String tcurdate = tLJAPayGrpSchema.getLastPayToDate();

//            double tmoney = 0;
//            tmoney = - tLJAPayPersonSchema.getSumActuPayMoney();
//            System.out.print(tmoney);

      //生成冲帐记录--个人实收表
      bLJAPayGrpSchema.setSchema(tLJAPayGrpSchema);
      //bLJAPayPersonSchema.setPayType("");
      bLJAPayGrpSchema.setPayDate(PubFun.getCurrentDate());
      bLJAPayGrpSchema.setCurPayToDate(tcurdate);
      bLJAPayGrpSchema.setLastPayToDate(tlastdate);
      bLJAPayGrpSchema.setSumActuPayMoney( -tLJAPayGrpSchema.getSumActuPayMoney());
      bLJAPayGrpSchema.setPayNo(tPayNo);
      bLJAPayGrpSchema.setConfDate(PubFun.getCurrentDate());
      bLJAPayGrpSchema.setEnterAccDate(PubFun.getCurrentDate());
      bLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
      bLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
      bLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
      bLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());

      mLJAPayGrpSet.add(tLJAPayGrpSchema);
      mLJAPayGrpSet.add(bLJAPayGrpSchema);

      //生成冲帐记录--保单表

      if (!Grpbackpol(bLJAPayGrpSchema))
        return false;
      //生成冲帐记录--个人实收表
    }
    /**
     *个人实交表
     */
    LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
    LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();

    //取个人实收数据
    tLJAPayPersonDB.setPayNo(mTempFeeNo);
    tLJAPayPersonSet = tLJAPayPersonDB.query();
    if (tLJAPayPersonSet.size() > 0) {
    }
    else {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "prepareFee";
      tError.errorMessage = "没有" + mTempFeeNo + "收据记录!";
      this.mErrors.addOneError(tError);
      return false;
    }

//把每条数据对保单进行冲帐处理，包括日期和金额的回退
    for (int i = 1; i <= tLJAPayPersonSet.size(); i++) {
      LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
      LJAPayPersonSchema bLJAPayPersonSchema = new LJAPayPersonSchema();
      tLJAPayPersonSchema = tLJAPayPersonSet.get(i);

      if (tLJAPayPersonSchema.getPayType().equals("ZC")) {
        tLJAPayPersonSchema.setPayType("CZZC");
      }
      if (tLJAPayPersonSchema.getPayType().equals("YET")) {
        tLJAPayPersonSchema.setPayType("CZYET");
      }
      if (tLJAPayPersonSchema.getPayType().equals("YEL")) {
        tLJAPayPersonSchema.setPayType("CZYEL");
      }

      String tPolNo = tLJAPayPersonSchema.getPolNo();
      String tDutyCode = tLJAPayPersonSchema.getDutyCode();
      String tPayPlanCode = tLJAPayPersonSchema.getPayPlanCode();
      char tPayNo1[] = tLJAPayPersonSchema.getPayNo().toCharArray();
      tPayNo1[12] = '9';
      String tPayNo = String.valueOf(tPayNo1);
      String tlastdate = tLJAPayPersonSchema.getCurPayToDate();
      String tcurdate = tLJAPayPersonSchema.getLastPayToDate();

//            double tmoney = 0;
//            tmoney = - tLJAPayPersonSchema.getSumActuPayMoney();
//            System.out.print(tmoney);

      //生成冲帐记录--个人实收表
      bLJAPayPersonSchema.setSchema(tLJAPayPersonSchema);
      //bLJAPayPersonSchema.setPayType("");
      bLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());
      bLJAPayPersonSchema.setCurPayToDate(tcurdate);
      bLJAPayPersonSchema.setLastPayToDate(tlastdate);
      bLJAPayPersonSchema.setSumActuPayMoney( -tLJAPayPersonSchema.
                                             getSumActuPayMoney());
      bLJAPayPersonSchema.setPayNo(tPayNo);
      bLJAPayPersonSchema.setConfDate(PubFun.getCurrentDate());
      bLJAPayPersonSchema.setEnterAccDate(PubFun.getCurrentDate());
      bLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
      bLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
      bLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
      bLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());

      mLJAPayPersonSet.add(tLJAPayPersonSchema);
      mLJAPayPersonSet.add(bLJAPayPersonSchema);

      //生成冲帐记录--保费项目表
      LCPremSchema tLCPremSchema = new LCPremSchema();
      LCPremDB tLCPremDB = new LCPremDB();
      tLCPremDB.setPolNo(tPolNo);
      tLCPremDB.setDutyCode(tDutyCode);
      tLCPremDB.setPayPlanCode(tPayPlanCode);
      if (!tLCPremDB.getInfo()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCPremDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpFeeBackBL";
        tError.functionName = "prepareFee";
        tError.errorMessage = "保费项生成失败!";
        this.mErrors.addOneError(tError);
        return false;
      }

      tLCPremSchema = tLCPremDB.getSchema();

      tLCPremSchema.setSumPrem(tLCPremSchema.getSumPrem() +
                               bLJAPayPersonSchema.getSumActuPayMoney());
      tLCPremSchema.setPaytoDate(bLJAPayPersonSchema.getCurPayToDate());
      tLCPremSchema.setModifyDate(PubFun.getCurrentDate());
      tLCPremSchema.setModifyTime(PubFun.getCurrentTime());

      mLCPremSet.add(tLCPremSchema);
      //生成冲帐记录--帐户表
      if (tLCPremSchema.getNeedAcc().equals("1")) {
        //帐户轨迹表
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
        tLCInsureAccTraceDB.setOtherNo(mTempFeeNo);

        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();

        if (tLCInsureAccTraceSet.size() > 0) {
          for (int j = 1; j <= tLCInsureAccTraceSet.size(); j++) {
            VData tVData = new VData();
            DealAccount tDealAccount = new DealAccount();

            LCInsureAccTraceSchema tLCInsureAccTraceSchema = new
                LCInsureAccTraceSchema();
            tLCInsureAccTraceSchema = tLCInsureAccTraceSet.get(j);

            tVData = tDealAccount.addPrem(tLCInsureAccTraceSchema.getPolNo(),
                                          tLCInsureAccTraceSchema.getInsuAccNo(),
                                          mTempFeeNo,
                                          tLCInsureAccTraceSchema.getOtherType(),
                                          tLCInsureAccTraceSchema.getMoneyType(),
                                          mManageCom,
                                          0 - tLCInsureAccTraceSchema.getMoney());

            //帐户表
            LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
            tLCInsureAccSet = (LCInsureAccSet) tVData.getObjectByObjectName(
                "LCInsureAccSet", 0);
            if (tLCInsureAccSet.size() > 0) {
              for (int k = 1; k <= tLCInsureAccSet.size(); k++) {
                LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
                tLCInsureAccSchema = tLCInsureAccSet.get(k);
                mLCInsureAccSet.add(tLCInsureAccSchema);
              }
            }

            //帐户轨迹表
            tLCInsureAccTraceSet = new LCInsureAccTraceSet();
            tLCInsureAccTraceSet = (LCInsureAccTraceSet) tVData.
                getObjectByObjectName("LCInsureAccTraceSet", 0);
            if (tLCInsureAccTraceSet.size() > 0) {
              for (int k = 1; k <= tLCInsureAccTraceSet.size(); k++) {
                LCInsureAccTraceSchema t2LCInsureAccTraceSchema = new
                    LCInsureAccTraceSchema();
                t2LCInsureAccTraceSchema = tLCInsureAccTraceSet.get(i);
                mLCInsureAccTraceSet.add(t2LCInsureAccTraceSchema);
              }
            }

          }
        }
        else {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "GrpFeeBackBL";
          tError.functionName = "prepareFee";
          tError.errorMessage = "没有帐户轨迹信息!";
          this.mErrors.addOneError(tError);
          return false;
        }
      }

      //生成冲帐记录--保险责任表
      LCDutySchema tLCDutySchema = new LCDutySchema();
      LCDutyDB tLCDutyDB = new LCDutyDB();
      tLCDutyDB.setPolNo(tPolNo);
      tLCDutyDB.setDutyCode(tDutyCode);
      if (!tLCDutyDB.getInfo()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpFeeBackBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "保险责任生成失败!";
        this.mErrors.addOneError(tError);
        return false;
      }

      tLCDutySchema = tLCDutyDB.getSchema();

      tLCDutySchema.setSumPrem(tLCPremSchema.getSumPrem() +
                               bLJAPayPersonSchema.getSumActuPayMoney());
      tLCDutySchema.setPaytoDate(bLJAPayPersonSchema.getCurPayToDate());
      tLCDutySchema.setModifyDate(PubFun.getCurrentDate());
      tLCDutySchema.setModifyTime(PubFun.getCurrentTime());

      mLCDutySet.add(tLCDutySchema);

      //生成冲帐记录--保单表

      if (!backpol(bLJAPayPersonSchema))
        return false;
      //生成冲帐记录--个人实收表
    }
    return true;
  }

  /*
   *生成回退保单数据
   */
  private boolean backpol(LJAPayPersonSchema tLJAPayPersonSchema) {
    if (mLCPolSet.size() > 0) {
      String tflag = "0";
      for (int j = 1; j <= mLCPolSet.size(); j++) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema = mLCPolSet.get(j);

        if (tLJAPayPersonSchema.getPolNo().equals(tLCPolSchema.getPolNo())) {
          tflag = "1";
          mLCPolSet.remove(tLCPolSchema);
          backcrtpol(tLCPolSchema, tLJAPayPersonSchema);
        }
      }
      if (tflag.equals("0")) {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(tLJAPayPersonSchema.getPolNo());
        if (tLCPolDB.getInfo() == false) {
          this.mErrors.copyAllErrors(tLCPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "GrpFeeBackBL";
          tError.functionName = "backpol";
          tError.errorMessage = "没有" + tLJAPayPersonSchema.getPolNo() + "保单记录!";
          this.mErrors.addOneError(tError);
          return false;
        }
        else {
          tLCPolSchema = tLCPolDB.getSchema();
          backcrtpol(tLCPolSchema, tLJAPayPersonSchema);
        }
      }
    }
    else {
      LCPolSchema tLCPolSchema = new LCPolSchema();
      LCPolDB tLCPolDB = new LCPolDB();
      tLCPolDB.setPolNo(tLJAPayPersonSchema.getPolNo());
      if (tLCPolDB.getInfo() == false) {
        this.mErrors.copyAllErrors(tLCPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpFeeBackBL";
        tError.functionName = "backpol";
        tError.errorMessage = "没有" + tLJAPayPersonSchema.getPolNo() + "保单记录!";
        this.mErrors.addOneError(tError);
        return false;

      }
      else {
        tLCPolSchema = tLCPolDB.getSchema();
        backcrtpol(tLCPolSchema, tLJAPayPersonSchema);
      }
    }
    return true;
  }

  /*
   *生成回退保单数据
   */
  private boolean Grpbackpol(LJAPayGrpSchema tLJAPayGrpSchema) {
    if (mLCGrpPolSet.size() > 0) {
      String tflag = "0";
      for (int j = 1; j <= mLCGrpPolSet.size(); j++) {
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema = mLCGrpPolSet.get(j);

        if (tLJAPayGrpSchema.getGrpPolNo().equals(tLCGrpPolSchema.getGrpPolNo())) {
          tflag = "1";
          mLCPolSet.remove(tLCGrpPolSchema);
          Grpbackcrtpol(tLCGrpPolSchema, tLJAPayGrpSchema);
        }
      }
      if (tflag.equals("0")) {
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tLJAPayGrpSchema.getGrpPolNo());
        if (tLCGrpPolDB.getInfo() == false) {
          this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "GrpFeeBackBL";
          tError.functionName = "Grpbackpol";
          tError.errorMessage = "没有" + tLJAPayGrpSchema.getGrpPolNo() + "保单记录!";
          this.mErrors.addOneError(tError);
          return false;
        }
        else {
          tLCGrpPolSchema = tLCGrpPolDB.getSchema();
          Grpbackcrtpol(tLCGrpPolSchema, tLJAPayGrpSchema);
        }
      }
    }
    else {
      LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
      tLCGrpPolDB.setGrpPolNo(tLJAPayGrpSchema.getGrpPolNo());
      if (tLCGrpPolDB.getInfo() == false) {
        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpFeeBackBL";
        tError.functionName = "Grpbackpol";
        tError.errorMessage = "没有" + tLJAPayGrpSchema.getGrpPolNo() + "保单记录!";
        this.mErrors.addOneError(tError);
        return false;

      }
      else {
        tLCGrpPolSchema = tLCGrpPolDB.getSchema();
        Grpbackcrtpol(tLCGrpPolSchema, tLJAPayGrpSchema);
      }
    }
    return true;
  }

  /*
   *生成回退保单数据
   */
  private void backcrtpol(LCPolSchema tLCPolSchema,
                          LJAPayPersonSchema tLJAPayPersonSchema) {
    tLCPolSchema.setPaytoDate(tLJAPayPersonSchema.getCurPayToDate());
    if (tLJAPayPersonSchema.getPayType().equals("CZZC")) {
      tLCPolSchema.setSumPrem(tLCPolSchema.getSumPrem() +
                              tLJAPayPersonSchema.getSumActuPayMoney());
    }
    if (tLJAPayPersonSchema.getPayType().equals("CZYET")) {
      tLCPolSchema.setLeavingMoney(tLCPolSchema.getLeavingMoney() +
                                   tLJAPayPersonSchema.getSumActuPayMoney());
    }
    if (tLJAPayPersonSchema.getPayType().equals("CZYEL")) {
      tLCPolSchema.setLeavingMoney(tLCPolSchema.getLeavingMoney() +
                                   tLJAPayPersonSchema.getSumActuPayMoney());
    }

    mLCPolSet.add(tLCPolSchema);
  }

  /*
   *生成回退保单数据
   */
  private void Grpbackcrtpol(LCGrpPolSchema tLCGrpPolSchema,
                             LJAPayGrpSchema tLJAPayGrpSchema) {
    tLCGrpPolSchema.setPaytoDate(tLJAPayGrpSchema.getCurPayToDate());
    if (tLJAPayGrpSchema.getPayType().equals("CZZC")) {
      tLCGrpPolSchema.setSumPrem(tLCGrpPolSchema.getSumPrem() +
                                 tLJAPayGrpSchema.getSumActuPayMoney());
    }
    if (tLJAPayGrpSchema.getPayType().equals("CZYET")) {
      tLCGrpPolSchema.setDif(tLCGrpPolSchema.getDif() +
                             tLJAPayGrpSchema.getSumActuPayMoney());
    }
    if (tLJAPayGrpSchema.getPayType().equals("CZYEL")) {
      tLCGrpPolSchema.setDif(tLCGrpPolSchema.getDif() +
                             tLJAPayGrpSchema.getSumActuPayMoney());
    }

    mLCGrpPolSet.add(tLCGrpPolSchema);
  }

  private boolean preparePay() {
    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();

    tLJTempFeeDB.setTempFeeNo(mTempFeeNo);
    tLJTempFeeSet = tLJTempFeeDB.query();
    if (tLJTempFeeSet.size() > 0) {
    }
    else {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpFeeBackBL";
      tError.functionName = "preparePay";
      tError.errorMessage = "没有暂交费数据!";
      this.mErrors.addOneError(tError);
      return false;
    }

    for (int i = 1; i <= tLJTempFeeSet.size(); i++) {
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema = tLJTempFeeSet.get(i);

      String tGrpPolNo = tLJTempFeeSchema.getOtherNo();

      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
      LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
      tLCGrpPolDB.setGrpPolNo(tGrpPolNo);
      if (!tLCGrpPolDB.getInfo()) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpFeeBackBL";
        tError.functionName = "preparePay";
        tError.errorMessage = "没有保单数据!!";
        this.mErrors.addOneError(tError);
        return false;
      }
      else {
        tLCGrpPolSchema = tLCGrpPolDB.getSchema();
      }

      tLCGrpPolSchema = tLCGrpPolDB.getSchema();

      //给付总表
      LJAGetSchema tLJAGetSchema = new LJAGetSchema();
      String tLimit = PubFun.getNoLimit(tLCGrpPolSchema.getManageCom());
      String tGetNo = PubFun1.CreateMaxNo("GETNO", tLimit);
      String tNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit);
      String tSerialNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

      //取投保人信息
      LCAppntGrpDB tLCAppntGrpDB = new LCAppntGrpDB();
      tLCAppntGrpDB.setPolNo(tLCGrpPolSchema.getGrpPolNo());
      /*Lis5.3 upgrade get
                   tLCAppntGrpDB.setGrpNo(tLCGrpPolSchema.getGrpNo());
       */
      if (!tLCAppntGrpDB.getInfo()) {
      }
      else {
        //tLJAGetSchema.setDrawerID(tLCAppntGrpDB.getIDNo());
      }
      tLJAGetSchema.setActuGetNo(tGetNo);
      tLJAGetSchema.setOtherNo(tNoticeNo);
      tLJAGetSchema.setOtherNoType("4");
      tLJAGetSchema.setPayMode("1");
      /*Lis5.3 upgrade get
                   tLJAGetSchema.setAppntNo(tLCGrpPolSchema.getGrpNo());
       */
      tLJAGetSchema.setSumGetMoney(tLJTempFeeSchema.getPayMoney());
      tLJAGetSchema.setSaleChnl(tLCGrpPolSchema.getSaleChnl());
      tLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
      tLJAGetSchema.setEnterAccDate(" ");
      tLJAGetSchema.setConfDate(" ");
      tLJAGetSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
      tLJAGetSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
      tLJAGetSchema.setGetNoticeNo(tNoticeNo);
//      tLJAGetSchema.setBankCode(tLCGrpPolSchema.getBankCode());
//      tLJAGetSchema.setBankAccNo(tLCGrpPolSchema.getBankAccNo());
      tLJAGetSchema.setDrawer(tLCGrpPolSchema.getGrpName());
      //tLJAGetSchema.setDrawerID();
      tLJAGetSchema.setSerialNo("0");
      tLJAGetSchema.setOperator(mOperate);
      tLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
      tLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
      tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
      tLJAGetSchema.setBankOnTheWayFlag("");
      tLJAGetSchema.setBankSuccFlag("");
      tLJAGetSchema.setSendBankCount("");
      tLJAGetSchema.setManageCom(tLCGrpPolSchema.getManageCom());
      tLJAGetSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
      tLJAGetSchema.setAgentType(tLCGrpPolSchema.getAgentType());
      tLJAGetSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
      tLJAGetSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());

      mLJAGetSet.add(tLJAGetSchema);

      //暂交费退费应付表
      LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();

      tLJAGetTempFeeSchema.setActuGetNo(tGetNo);
      tLJAGetTempFeeSchema.setGetNoticeNo(tNoticeNo);
      tLJAGetTempFeeSchema.setTempFeeNo(mTempFeeNo);
      tLJAGetTempFeeSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
      tLJAGetTempFeeSchema.setTempFeeType(tLJTempFeeSchema.getTempFeeType());
      tLJAGetTempFeeSchema.setPayMode("1");
      tLJAGetTempFeeSchema.setGetMoney(tLJTempFeeSchema.getPayMoney());
      tLJAGetTempFeeSchema.setGetDate(PubFun.getCurrentDate());
      tLJAGetTempFeeSchema.setManageCom(mManageCom);
      tLJAGetTempFeeSchema.setAgentCom(tLJTempFeeSchema.getAgentCom());
      tLJAGetTempFeeSchema.setAgentType(tLJTempFeeSchema.getAgentType());
      tLJAGetTempFeeSchema.setAPPntName(tLJTempFeeSchema.getAPPntName());
      tLJAGetTempFeeSchema.setAgentGroup(tLJTempFeeSchema.getAgentGroup());
      tLJAGetTempFeeSchema.setAgentCode(tLJTempFeeSchema.getAgentCode());
      tLJAGetTempFeeSchema.setFeeOperationType("CZ");
      tLJAGetTempFeeSchema.setFeeFinaType("CZ");
      tLJAGetTempFeeSchema.setSerialNo(tSerialNo);
      tLJAGetTempFeeSchema.setOperator(mOperate);
      tLJAGetTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
      tLJAGetTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
      tLJAGetTempFeeSchema.setGetReasonCode("CZ");
      tLJAGetTempFeeSchema.setState("0");
      tLJAGetTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

      mLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
    }
    return true;
  }

  /**
   *准备需要保存的数据
   **/
  private void prepareOutputData() {
    LCPolSet tLCPolSet = new LCPolSet();
    tLCPolSet.set(mLCPolSet);
    mAllLCPolSet.add(tLCPolSet);

    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    tLCGrpPolSet.set(mLCGrpPolSet);
    mAllLCGrpPolSet.add(tLCGrpPolSet);

    LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
    tLJAGetTempFeeSet.set(mLJAGetTempFeeSet);
    mAllLJAGetTempFeeSet.add(tLJAGetTempFeeSet);

    LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
    tLJAPayPersonSet.set(mLJAPayPersonSet);
    mAllLJAPayPersonSet.add(tLJAPayPersonSet);

    LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
    tLJAPayGrpSet.set(mLJAPayGrpSet);
    mAllLJAPayGrpSet.add(tLJAPayGrpSet);

    LJAGetSet tLJAGetSet = new LJAGetSet();
    tLJAGetSet.set(mLJAGetSet);
    mAllLJAGetSet.add(tLJAGetSet);

    LCDutySet tLCDutySet = new LCDutySet();
    tLCDutySet.set(mLCDutySet);
    mAllLCDutySet.add(tLCDutySet);

    LCPremSet tLCPremSet = new LCPremSet();
    tLCPremSet.set(mLCPremSet);
    mAllLCPremSet.add(tLCPremSet);

    LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
    tLCInsureAccSet.set(mLCInsureAccSet);
    mAllLCInsureAccSet.add(tLCInsureAccSet);

    LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
    tLCInsureAccTraceSet.set(mLCInsureAccTraceSet);
    mAllLCInsureAccTraceSet.add(tLCInsureAccTraceSet);

    mInputData.clear();
    mInputData.add(mAllLCPolSet);
    mInputData.add(mAllLCGrpPolSet);
    mInputData.add(mAllLJAPayPersonSet);
    mInputData.add(mAllLJAPayGrpSet);
    mInputData.add(mAllLJAGetTempFeeSet);
    mInputData.add(mAllLJAGetSet);
    mInputData.add(mAllLCDutySet);
    mInputData.add(mAllLCPremSet);
    mInputData.add(mAllLCInsureAccSet);
    mInputData.add(mAllLCInsureAccTraceSet);

  }
}
