package com.sinosoft.lis.cbcheck;

import java.lang.*;
import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.finfee.AfterTempFeeBL;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.ChangeFinFeeStateBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;

/**
 * <p>Title: Web业务系统到帐确认部分</p>
 * <p>Description: 逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class FinFeeSureBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 往界面传输数据的容器 */
  private VData mResult = new VData();

  /** 数据操作字符串 */
  private String mOperate;
  private String mIsueManageCom;
  private String mManageCom;
  private String mpassflag; //通过标记
  private int merrcount; //错误条数
  private String mCalCode; //计算编码
  private String mUser;
  private FDate fDate = new FDate();
  private double mValue;
  private String mInsuredNo = "";
  private String mBackObj = "";
  private String mflag = ""; //分，个单标记

  /** 业务处理相关变量 */
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCPolSet mmLCPolSet = new LCPolSet();
  private LCPolSet m2LCPolSet = new LCPolSet();
  private LCPolSet mAllLCPolSet = new LCPolSet();
  private LCPolSchema mLCPolSchema = new LCPolSchema();
  private String mPolNo = "";
  private String mOldPolNo = "";

  /** 集体单表 */
  private LCGrpPolSet mAllLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
  private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

  /** 保费项表 */
  private LCPremSet mLCPremSet = new LCPremSet();
  private LCPremSet mAllLCPremSet = new LCPremSet();

  /** 领取项表 */
  private LCGetSet mLCGetSet = new LCGetSet();
  private LCGetSet mAllLCGetSet = new LCGetSet();

  /** 责任表 */
  private LCDutySet mLCDutySet = new LCDutySet();
  private LCDutySet mAllLCDutySet = new LCDutySet();

  /** 特别约定表 */
  private LCSpecSet mLCSpecSet = new LCSpecSet();
  private LCSpecSet mAllLCSpecSet = new LCSpecSet();

  /** 特别约定注释表 */
  private LCSpecNoteSet mLCSpecNoteSet = new LCSpecNoteSet();
  private LCSpecNoteSet mAllLCSpecNoteSet = new LCSpecNoteSet();

  /** 核保主表 */
  private LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSet mAllLCUWMasterSet = new LCUWMasterSet();
  private LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();

  /** 核保子表 */
  private LCUWSubSet mLCUWSubSet = new LCUWSubSet();
  private LCUWSubSet mAllLCUWSubSet = new LCUWSubSet();
  private LCUWSubSchema mLCUWSubSchema = new LCUWSubSchema();

  /** 核保错误信息表 */
  private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();
  private LCUWErrorSet mAllLCErrSet = new LCUWErrorSet();

  /** 告知表 */
  private LCCustomerImpartSet mLCCustomerImpartSet = new LCCustomerImpartSet();
  private LCCustomerImpartSet mAllLCCustomerImpartSet = new LCCustomerImpartSet();

  /** 投保人表 */
  private LCAppntIndSet mLCAppntIndSet = new LCAppntIndSet();
  private LCAppntIndSet mAllLCAppntIndSet = new LCAppntIndSet();

  /** 受益人表 */
  private LCBnfSet mLCBnfSet = new LCBnfSet();
  private LCBnfSet mAllLCBnfSet = new LCBnfSet();

  /** 被保险人表 */
  private LCInsuredSet mLCInsuredSet = new LCInsuredSet();
  private LCInsuredSet mAllLCInsuredSet = new LCInsuredSet();

  /** 体检资料主表 */
  private LCPENoticeSet mLCPENoticeSet = new LCPENoticeSet();
  private LCPENoticeSet mAllLCPENoticeSet = new LCPENoticeSet();
  private LCPENoticeSchema mLCPENoticeSchema = new LCPENoticeSchema();

  /** 体检资料项目表 */
  private LCPENoticeItemSet mLCPENoticeItemSet = new LCPENoticeItemSet();
  private LCPENoticeItemSet mmLCPENoticeItemSet = new LCPENoticeItemSet();
  private LCPENoticeItemSet mAllLCPENoticeItemSet = new LCPENoticeItemSet();

  /** 问题件表 */
  private LCIssuePolSet mLCIssuePolSet = new LCIssuePolSet();
  private LCIssuePolSet mmLCIssuePolSet = new LCIssuePolSet();
  private LCIssuePolSet mAllLCIssuePolSet = new LCIssuePolSet();

  /** 暂交费表 */
  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeSet mAllLJTempFeeSet = new LJTempFeeSet();

  /** 暂交费关联表 */
  private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mmLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mAllLJTempFeeClassSet = new LJTempFeeClassSet();

  /**计算公式表**/
  private LMUWSchema mLMUWSchema = new LMUWSchema();

  //private LMUWDBSet mLMUWDBSet = new LMUWDBSet();
  private LMUWSet mLMUWSet = new LMUWSet();

  private CalBase mCalBase = new CalBase();
  
  private GlobalInput tGlobalInput = new GlobalInput();

  public FinFeeSureBL() {}

  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    int flag = 0; //判断是不是所有数据都不成功
    int j = 0; //符合条件数据个数

    //将操作数据拷贝到本类中
    mInputData = (VData) cInputData.clone();
    //GlobalInput tGlobalInput = new GlobalInput();
    //this.mOperate = tGlobalInput.;

    System.out.println("---1---");
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    System.out.println("---FinFeeSureBL getInputData---");

    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    int SetNo = mmLJTempFeeClassSet.size();
    for (int i = 1; i <= SetNo; i++) {
      tLJTempFeeClassSchema = (LJTempFeeClassSchema) mmLJTempFeeClassSet.get(i);
      mLJTempFeeClassSchema = (LJTempFeeClassSchema) mmLJTempFeeClassSet.get(i);

      System.out.println("---FinFeeSureBL checkData---");
      // 数据操作业务处理
      if (!dealData(tLJTempFeeClassSchema))
        continue;
      //return false;
      else {
        flag = 1;
      }
    }

    if (flag == 0) {
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "submitData";
      tError.errorMessage = "到帐确认失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    try {
      if (!setTempfeeAccDate()) {
        throw new Exception();
      }
    }
    catch (Exception e) {
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "submitData";
      tError.errorMessage = "更新暂交费表失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    System.out.println("---FinFeeSureBL dealData---");
    //准备给后台的数据
    prepareOutputData();

    System.out.println("---FinFeeSureBL prepareOutputData---");
    //数据提交
    FinFeeSureBLS tFinFeeSureBLS = new FinFeeSureBLS();
    System.out.println("Start FinFeeSureBL Submit...");
    if (!tFinFeeSureBLS.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tFinFeeSureBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    
    if (!claimFinish())
    {
        //return false;
    }
    
    //续期应收数据状态修改 2011-11-4
    if(!finUrgeVerify()){
        return false;
    }
    
    if(!afterSubmit())
    {
        return false;
    }
    
    System.out.println("---FinFeeSureBL commitData---");
    return true;
   
    
  }

  /**
   * 设置暂交费表到帐日期
   * @return
   */
  private boolean setTempfeeAccDate() {
    HashMap tHashMap = new HashMap(); //记录每个暂交费号出现的次数，一次表示一种交费
    int num = 0;
    HashMap accDateHashMap = new HashMap(); //记录每个暂交费号对应的最大到帐日期
    String tDate = "";

    //统计该批次中的暂交费子表数据（暂交费号－－出现次数）
    for (int i = 0; i < mAllLJTempFeeClassSet.size(); i++) {
      LJTempFeeClassSchema tLJTempFeeClassSchema = mAllLJTempFeeClassSet.get(i +
          1);

      //出现过，增加一次
      if (tHashMap.containsKey(tLJTempFeeClassSchema.getTempFeeNo())) {
        num = ( (Integer) tHashMap.get(tLJTempFeeClassSchema.getTempFeeNo())).
            intValue() + 1;
        tHashMap.put(tLJTempFeeClassSchema.getTempFeeNo(), new Integer(num));

        //取最大的到帐日期
        if (PubFun.calInterval(tLJTempFeeClassSchema.getEnterAccDate(),
                               (String)
                               accDateHashMap.get(tLJTempFeeClassSchema.
                                                  getTempFeeNo()), "D") < 0) {
          accDateHashMap.put(tLJTempFeeClassSchema.getTempFeeNo(),
                             tLJTempFeeClassSchema.getEnterAccDate());
        }
      }
      else {
        tHashMap.put(tLJTempFeeClassSchema.getTempFeeNo(), new Integer(1));
        accDateHashMap.put(tLJTempFeeClassSchema.getTempFeeNo(),
                           tLJTempFeeClassSchema.getEnterAccDate());
      }
    }

    //遍历所有的暂交费号
    for (Iterator tIterator = tHashMap.keySet().iterator(); tIterator.hasNext(); ) {
      String tempfeeNo = (String) tIterator.next();

      //选择出该暂交费号未到帐的数据
      LJTempFeeClassDB tLJTempFeeClassDB2 = new LJTempFeeClassDB();
      String strSql = "select * from LJTempFeeClass where TempFeeNo='" +
          tempfeeNo + "' and EnterAccDate is null";
      LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB2.executeQuery(
          strSql);
      //如果数量相同，表示这次全部到帐，否则表示还有未到帐的数据
      if (tLJTempFeeClassSet.size() ==
          ( (Integer) tHashMap.get(tempfeeNo)).intValue()) {
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();

        tLJTempFeeDB.setTempFeeNo(tempfeeNo);
        tLJTempFeeSet = tLJTempFeeDB.query();
        //取子表中已经到帐的数据的最大到帐日期
        LJTempFeeClassDB tLJTempFeeClassDB3 = new LJTempFeeClassDB();
        strSql = "select * from LJTempFeeClass where TempFeeNo='" + tempfeeNo + "' and EnterAccDate=(select max(EnterAccDate) from LJTempFeeClass where TempFeeNo='" +
            tempfeeNo + "')";
        LJTempFeeClassSet tLJTempFeeClassSet3 = tLJTempFeeClassDB3.executeQuery(
            strSql);

        if (tLJTempFeeClassSet3.size() > 0 &&
            (PubFun.calInterval(tLJTempFeeClassSet3.get(1).getEnterAccDate(),
                                (String) accDateHashMap.get(tempfeeNo), "D") <
             0)) {
          tDate = tLJTempFeeClassSet3.get(1).getEnterAccDate();
        }
        else {
          tDate = (String) accDateHashMap.get(tempfeeNo);
        }

        for (int i = 1; i <= tLJTempFeeSet.size(); i++) {
          LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
          tLJTempFeeSchema = tLJTempFeeSet.get(i);

          tLJTempFeeSchema.setEnterAccDate(tDate);
          tLJTempFeeSchema.setOperator(mOperate);
          tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
          tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
          tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

          mLJTempFeeSet.add(tLJTempFeeSchema);
        }
      }

      mAllLJTempFeeSet = mLJTempFeeSet;
    }

    return true;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData(LJTempFeeClassSchema tLJTempFeeClassSchema) {

    if (dealOnePol() == false)
      return false;

    return true;
  }

  /**
   * 操作一张保单的业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealOnePol() {
    // 健康信息
    if (prepareFee() == false)
      return false;

//    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
//    tLJTempFeeClassSet.set( mLJTempFeeClassSet );
//    mAllLJTempFeeClassSet.add( tLJTempFeeClassSet );

    mAllLJTempFeeClassSet = mLJTempFeeClassSet;

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    
    tGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName(
        "GlobalInput", 0));
    mOperate = tGlobalInput.Operator;
    mManageCom = tGlobalInput.ManageCom;

    mmLJTempFeeClassSet.set( (LJTempFeeClassSet) cInputData.
                            getObjectByObjectName("LJTempFeeClassSet", 0));
    //mLJTempFeeClassSet.set((LJTempFeeClassSet)cInputData.getObjectByObjectName("LJTempFeeClassSet",0));

    int n = mmLJTempFeeClassSet.size();
    int flag = 0; //怕判断是不是所有保单都失败
    int j = 0; //符合条件保单个数

    if (n > 0) {
      flag = 1;
    }
    else {
      // @@错误处理
      //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有传入数据!";
      this.mErrors.addOneError(tError);
      return false;

    }
    return true;
  }

  private boolean Getbackobj() {
    return true;
  }

  /**
   * 准备体检资料信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean prepareFee() {
    /**
     *暂交费关联表
     */
    String tPrint = "";
    //到帐日期
    String tDate = "";
    tDate = mLJTempFeeClassSchema.getEnterAccDate();

    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
    tLJTempFeeClassDB.setSchema(mLJTempFeeClassSchema);
    String tTempFeeNo = mLJTempFeeClassSchema.getTempFeeNo();
    if (tLJTempFeeClassDB.getInfo() == false) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "prepareFee";
      tError.errorMessage = "没有" + tTempFeeNo + "收据!";
      this.mErrors.addOneError(tError);
      return false;
    }

    tLJTempFeeClassSchema = tLJTempFeeClassDB.getSchema();

    if (tDate.compareTo(tLJTempFeeClassSchema.getMakeDate()) < 0) {
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "prepareFee";
      tError.errorMessage = tTempFeeNo + "收据,到帐日期小于入机日期";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (tDate.compareTo(PubFun.getCurrentDate()) > 0) {
      CError tError = new CError();
      tError.moduleName = "FinFeeSureBL";
      tError.functionName = "prepareFee";
      tError.errorMessage = tTempFeeNo + "收据,到帐日期大于系统日期";
      this.mErrors.addOneError(tError);
      return false;
    }
    tLJTempFeeClassSchema.setEnterAccDate(tDate);
    tLJTempFeeClassSchema.setOperator(mOperate);
    tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
    tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
    tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
    tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());

    mLJTempFeeClassSet.add(tLJTempFeeClassSchema);

    return true;
  }

  /**
   *准备需要保存的数据
   **/
  private void prepareOutputData() {
    mInputData.clear();
    mInputData.add(mAllLJTempFeeSet);
    mInputData.add(mAllLJTempFeeClassSet);
  }
  
  /**
   * 处理财务处理结束后的操作。
   * 本操作在支票确认和银行转帐回盘时都会调用，
   * 请注意实现AfterTempFeeBL接口时要保障实现后的类在这两个地方都能使用
   * @return boolean
   */
  private boolean afterSubmit()
  {
      System.out.println("暂收处理后的相关业务处理afterSubmit");
      String tempFeeType = null;
      MMap map = new MMap();

      //注意，一条LJSPay可能对应多个LJTempFee纪录，所以进行处理时要注意相关性
      for(int i = 1; i <= mAllLJTempFeeSet.size(); i++)
      {
          try
          {
              LJTempFeeSchema tLJTempFeeSchema
                  = mAllLJTempFeeSet.get(i).getSchema();
              //按TempFeeType编码调用不同的类
              tempFeeType = tLJTempFeeSchema.getTempFeeType();

              String className = "com.sinosoft.lis.finfee.AfterTempFee" +
                                 tempFeeType + "BL";
              Class confirmClass = Class.forName(className);
              AfterTempFeeBL tAfterTempFeeBL = (AfterTempFeeBL)
                                          confirmClass.newInstance();
              VData data = new VData();
              data.add(tGlobalInput);
              data.add(tLJTempFeeSchema);

              MMap tMMap = tAfterTempFeeBL.getSubmitMMap(data, "");
              if(tMMap == null)
              {
                  mErrors.copyAllErrors(tAfterTempFeeBL.mErrors);
                  return false;
              }
              map.add(tMMap);
          }
          catch(ClassNotFoundException ex)
          {
              System.out.println(tempFeeType + "没有暂收后处理类!");
          }
          catch(NullPointerException ex)
          {
              ex.printStackTrace();
          }
          catch(Exception ex)
          {
              ex.printStackTrace();
              return false;
          }
      }

      if(map == null || map.size() == 0)
      {
          return true;
      }

      VData data = new VData();
      data.add(map);
      PubSubmit p = new PubSubmit();
      if(!p.submitData(data, ""))
      {
          CError tError = new CError();
          tError.moduleName = "TempFeeBL";
          tError.functionName = "afterSubmit";
          tError.errorMessage = "到账确认成功，但后续处理失败";
          mErrors.addOneError(tError);
          System.out.println(tError.errorMessage);
          return false;
      }

      return true;
  }
  
  private boolean finUrgeVerify() {
      for (int i = 1; i <= mLJTempFeeSet.size(); i++)
      {
          if (!mLJTempFeeSet.get(i).getTempFeeType().equals("2")) {
              continue;
            }
          LJSPaySchema tLJSPaySchema = new LJSPaySchema();
          tLJSPaySchema.setGetNoticeNo(mLJTempFeeSet.get(1).getTempFeeNo());
          VData ttVdata = new VData();
          ttVdata.add(tLJSPaySchema);
          ttVdata.add(tGlobalInput);
          ChangeFinFeeStateBL bl = new ChangeFinFeeStateBL("4");
          if (!bl.submitData(ttVdata, "")) {
              CError tError = new CError();
              tError.moduleName = "FinFeeSureBL";
              tError.functionName = "finUrgeVerify";
              tError.errorMessage = "收费成功，但续期应收数据处理失败";
              mErrors.addOneError(tError);
              System.out.println(tError.errorMessage);
              continue;
          }
      }
      return true;
  }
  
  private boolean claimFinish()
  {
      for (int i = 1; i <= mLJTempFeeSet.size(); i++)
      {
          if (!mLJTempFeeSet.get(i).getTempFeeType().equals("Y") && !mLJTempFeeSet.get(i).getTempFeeType().equals("9"))
          {
              continue;
          }
          try
          {
              LLClaimFinishTask tLLClaimFinishTask = new LLClaimFinishTask(
                      tGlobalInput.ManageCom);
              tLLClaimFinishTask.run();
          }
          catch (Exception ex)
          {
              CError tError = new CError();
              tError.moduleName = "FinFeeSureBL";
              tError.functionName = "claimFinish";
              tError.errorMessage = "到账确认成功，但理赔预付回收核销失败";
              mErrors.addOneError(tError);
              System.out.println(tError.errorMessage);
              ex.printStackTrace();
              return false;
          }
      }
      return true;
  }
}
