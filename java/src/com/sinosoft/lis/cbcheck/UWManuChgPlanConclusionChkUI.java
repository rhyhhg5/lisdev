package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;

/**
 * <p>Title: 承保计划变更结论录入</p>
 * <p>Description: 承保计划变更结论录入</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Sinosoft</p>
 * @author ZhangRong
 * @version 1.0
 */
import com.sinosoft.utility.*;


public class UWManuChgPlanConclusionChkUI
{
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	public UWManuChgPlanConclusionChkUI()
	{
	}

	public boolean submitData(VData cInputData, String cOperate)
	{
		UWManuChgPlanConclusionChkBL tUWManuChgPlanConclusionChkBL = new UWManuChgPlanConclusionChkBL();

		if (!tUWManuChgPlanConclusionChkBL.submitData(cInputData, cOperate))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tUWManuChgPlanConclusionChkBL.mErrors);

			CError tError = new CError();
			tError.moduleName = "UWManuChgPlanConclusionChkUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理提交失败!";
			this.mErrors.addOneError(tError);
			mResult.clear();

			return false;
		}
		else
		{
			this.mErrors.copyAllErrors(tUWManuChgPlanConclusionChkBL.mErrors);
		}

		return true;
	}

	public static void main(String[] args)
	{
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";

		LCContSchema tLCContSchema = new LCContSchema();
		LCContSet tLCContSet = new LCContSet();

		tLCContSchema.setContNo("00000000000000000005");
		tLCContSchema.setUWFlag("0");
		tLCContSchema.setRemark("I want to change without any reasons");
		tLCContSet.add(tLCContSchema);

		VData tVData = new VData();
		tVData.add(tLCContSet);
		tVData.add(tG);

		UWManuChgPlanConclusionChkUI tUWManuChgPlanConclusionChkUI = new UWManuChgPlanConclusionChkUI();

		if (!tUWManuChgPlanConclusionChkUI.submitData(tVData, ""))
		{
			int n = tUWManuChgPlanConclusionChkUI.mErrors.getErrorCount();
			String tContent = " 承保计划变更原因录入失败，原因是:\n";

			for (int i = 0; i < n; i++)
				tContent += (tUWManuChgPlanConclusionChkUI.mErrors.getError(i).errorMessage + "\n");

			System.out.println(tContent);
		}
	}
}
