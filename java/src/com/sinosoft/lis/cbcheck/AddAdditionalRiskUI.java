package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class AddAdditionalRiskUI {

  private VData mInputData ;
  private VData mResult = new VData();
  public  CErrors mErrors=new CErrors();
  public AddAdditionalRiskUI()
  {
  }
  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    AddAdditionalRiskBL tAddAdditionalRiskBL=new AddAdditionalRiskBL();
    System.out.println("Start AddAdditionalRisk BL Submit...");
    tAddAdditionalRiskBL.submitData(mInputData,cOperate);

    System.out.println("End AddAdditionalRisk BL Submit...");

    //如果有需要处理的错误，则返回
    if (tAddAdditionalRiskBL .mErrors .needDealError() )
    {
        mErrors .copyAllErrors(tAddAdditionalRiskBL.mErrors ) ;
        return false;
    }
    mResult.clear();
    mResult=tAddAdditionalRiskBL.getResult();
    mInputData=null;
    return true;
  }

  public VData getResult()
  {
    return mResult;
  }

}