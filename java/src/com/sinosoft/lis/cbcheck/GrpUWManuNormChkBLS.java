package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统承保集体人工核保部分 </p>
 * <p>Description: 数据库功能类</p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class GrpUWManuNormChkBLS
{
        //是否存在需要人工核保保单
	int merrno = 0;
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();
	//private LCUWErrorDB mLCUWErrorDB = new LCUWErrorDB(conn);
	private LCUWErrorSet mLCUWErrorSet = new LCUWErrorSet();

	public GrpUWManuNormChkBLS() {}

	public static void main(String[] args)
	{
	}

	/**whn
	//传输数据的公共方法
	public int submitData(Vdata cInputData,String cOperate)
	{
	    if(submitDataMain(cInputData,cOperate) == false)
	    {
		return 2; //操作失败
	    }
	    else
	    {
		if (merrno>0)
		{
		    return 1; //操作成功有单要人工核保
		}
		else
		{
		    return 0; //操作成功全部自动通过
		}
	    }
	}
	whn**/

	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
	    mInputData=(VData)cInputData.clone() ;

	    System.out.println("Start GrpUWManuNormChkBLS Submit...");
		if (!this.saveData())
			return false;
	    System.out.println("End GrpUWManuNormChkBLS Submit...");

	    mInputData=null;
	    return true;
	}

	private boolean saveData()
	{
	  LCGrpPolSet mLCGrpPolSet = (LCGrpPolSet)mInputData.getObjectByObjectName("LCGrpPolSet",0);
	  LCGUWErrorSet mLCGUWErrorSet = (LCGUWErrorSet)mInputData.getObjectByObjectName("LCGUWErrorSet",0);
	  LCGUWMasterSet mLCGUWMasterSet = (LCGUWMasterSet)mInputData.getObjectByObjectName("LCGUWMasterSet",0);
	  LCGUWSubSet mLCGUWSubSet = (LCGUWSubSet)mInputData.getObjectByObjectName("LCGUWSubSet",0);
	  LCPolSet mLCPolSet = (LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0);
	  LCUWMasterSet mLCUWMasterSet = (LCUWMasterSet)mInputData.getObjectByObjectName("LCUWMasterSet",0);
	  LCUWSubSet mLCUWSubSet = (LCUWSubSet)mInputData.getObjectByObjectName("LCUWSubSet",0);

	  try
	  {
	    // 删除部分
		int polCount = mLCGrpPolSet.size();
		for (int i = 1; i <= polCount; i++)
		{
		  LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		  tLCGrpPolSchema = ( LCGrpPolSchema )mLCGrpPolSet.get( i );
		  String tGrpProposalNo = tLCGrpPolSchema.getGrpProposalNo();
		  String tUWFlag = tLCGrpPolSchema.getUWFlag();

		  Connection conn = DBConnPool.getConnection();

		  if (conn==null)
		  {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpUWManuNormChkBLS";
			tError.functionName = "saveData";
			tError.errorMessage = tGrpProposalNo+"数据库连接失败!";
			this.mErrors .addOneError(tError) ;
			return false;
		  }
		  conn.setAutoCommit(false);


		  // 核保主表
		  LCGUWMasterDB tLCGUWMasterDB = new LCGUWMasterDB( conn );
		  tLCGUWMasterDB.setGrpProposalNo( tGrpProposalNo );
		  if (tLCGUWMasterDB.deleteSQL() == false)
		  {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "GrpUWManuNormChkBLS";
			tError.functionName = "saveData";
			tError.errorMessage = tGrpProposalNo+"集体核保总表删除失败!";
			this.mErrors .addOneError(tError) ;
			conn.rollback() ;
			conn.close();
			return false;
		  }

		  // 投保单
		  LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB( conn );
		  tLCGrpPolDB.setGrpProposalNo( tGrpProposalNo );
		  if (tLCGrpPolDB.deleteSQL() == false)
		  {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "GrpUWManuNormChkBLS";
			tError.functionName = "saveData";
			tError.errorMessage = tGrpProposalNo+"集体保单表删除失败!";
			this.mErrors .addOneError(tError) ;
			conn.rollback() ;
			conn.close();
			return false;
		  }
		  // 保存部分
		  // 保单
		  tLCGrpPolDB.setSchema(tLCGrpPolSchema);

		  if(tLCGrpPolDB.insert() == false)
		  {
			// @@错误处理
			this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "GrpUWManuNormChkBLS";
			tError.functionName = "saveData";
			tError.errorMessage = tGrpProposalNo+"集体保单表保存失败!";
			this.mErrors .addOneError(tError) ;
			conn.rollback() ;
			conn.close();
			return false;
		  }

          System.out.println("-----------DDD--------------");
		  // 核保主表
		  LCGUWMasterDBSet tLCGUWMasterDBSet = new LCGUWMasterDBSet( conn );
		  tLCGUWMasterDBSet.set( mLCGUWMasterSet );
		  int masterno = tLCGUWMasterDBSet.size();
		  for (int j = 1;j <= masterno;j++)
		  {
			LCGUWMasterSchema tLCGUWMasterSchema = tLCGUWMasterDBSet.get(j);
			String masterpol = tLCGUWMasterSchema.getGrpProposalNo();
			if (masterpol.equals(tGrpProposalNo))
			{
			  tLCGUWMasterDB.setSchema(tLCGUWMasterSchema);
			  if (tLCGUWMasterDB.insert() == false)
			  {
				// @@错误处理
				this.mErrors.copyAllErrors(tLCGUWMasterDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "GrpUWManuNormChkBLS";
				tError.functionName = "saveData";
				tError.errorMessage = tGrpProposalNo+"集体核保总表保存失败!";
				this.mErrors .addOneError(tError) ;
				conn.rollback() ;
				conn.close();
				return false;
			  }
			}
		  }
          System.out.println("-----------EEE--------------");

		  // 核保子表
		  //LCGUWSubDBSet tLCGUWSubDBSet = new LCGUWSubDBSet( conn );
		  //tLCGUWSubDBSet.set( mLCGUWSubSet );
		  int subno = mLCGUWSubSet.size();
		  for (int j = 1;j <= subno;j++)
		  {
			LCGUWSubSchema tLCGUWSubSchema = mLCGUWSubSet.get(j);
			String subpol = tLCGUWSubSchema.getGrpPolNo();
			if (subpol.equals(tGrpProposalNo))
			{
			  LCGUWSubDB tLCGUWSubDB = new LCGUWSubDB(conn);
			  tLCGUWSubDB.setSchema(tLCGUWSubSchema);
			  if (tLCGUWSubDB.insert() == false)
			  {
				// @@错误处理
				this.mErrors.copyAllErrors(tLCGUWSubDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "GrpUWManuNormChkBLS";
				tError.functionName = "saveData";
				tError.errorMessage = tGrpProposalNo+"集体核保轨迹表保存失败!";
				this.mErrors .addOneError(tError) ;
				conn.rollback() ;
				conn.close();
				return false;
			  }
			}
		  }
          System.out.println("-----------FFF--------------");

		  //个人保单
		  if(mLCPolSet.size() > 0)
		  {
			for(int j = 1;j <= mLCPolSet.size();j++)
			{
			  LCPolSchema tLCPolSchema = new LCPolSchema();
			  tLCPolSchema = mLCPolSet.get(j);
			  if(tLCPolSchema.getGrpPolNo().equals(tGrpProposalNo))
			  {
				LCPolDB tLCPolDB = new LCPolDB(conn);
				tLCPolDB.setSchema(tLCPolSchema);
				if(tLCPolDB.update() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLCPolDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "GrpUWManuNormChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = "个人保单表更新失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}
			  }
			}
		  }

		  //个人单核保主表
		  if(mLCUWMasterSet.size() > 0)
		  {
			for(int j = 1;j<=mLCUWMasterSet.size();j++)
			{
			  LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
			  tLCUWMasterSchema = mLCUWMasterSet.get(j);
/*lis 6.0 update by yuanaq
			  if(tLCUWMasterSchema.getGrpPolNo().equals(tGrpProposalNo))
			  {
				LCUWMasterDB tLCUWMasterDB = new LCUWMasterDB(conn);
				tLCUWMasterDB.setSchema(tLCUWMasterSchema);
				if(tLCUWMasterDB.update() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLCUWMasterDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "GrpUWManuNormChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = "个人核保主表更新失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}

		      //个人单核保轨迹表
		      if(mLCUWSubSet.size() > 0)
			  {
				for(int k = 1;k<=mLCUWSubSet.size();k++)
				{
				  LCUWSubSchema tLCUWSubSchema = new LCUWSubSchema();
				  tLCUWSubSchema = mLCUWSubSet.get(k);

				  if(tLCUWSubSchema.getPolNo().equals(tLCUWMasterSchema.getPolNo()))
				  {
					LCUWSubDB tLCUWSubDB = new LCUWSubDB(conn);
					tLCUWSubDB.setSchema(tLCUWSubSchema);
					if(tLCUWSubDB.insert() == false)
					{
					  // @@错误处理
					  this.mErrors.copyAllErrors(tLCUWSubDB.mErrors);
					  CError tError = new CError();
					  tError.moduleName = "GrpUWManuNormChkBLS";
					  tError.functionName = "saveData";
					  tError.errorMessage = "个人核保轨迹表更新失败!";
					  this.mErrors .addOneError(tError) ;
					  conn.rollback() ;
					  conn.close();
					  return false;
					}
				  }
				}
			  }


			  }*/
			}
		  }


        System.out.println("-----------GGG--------------");
		if (tUWFlag.equals("3"))
		{
		  LCSpecSet mLCSpecSet = (LCSpecSet)mInputData.getObjectByObjectName("LCSpecSet",0);
		  LCPremSet mLCPremSet = (LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0);

		  // 特别约定表
		  LCSpecDBSet tLCSpecDBSet = new LCSpecDBSet( conn );
		  tLCSpecDBSet.set( mLCSpecSet );
		  int errno = tLCSpecDBSet.size();
		  merrno = merrno + errno;
		  System.out.println("speccount="+errno);
		  if (errno > 0)
		  {
			for (int j = 1; j<=errno; j++)
			{
			  System.out.println("j="+j);
			  LCSpecSchema tLCSpecSchema = new LCSpecSchema();
			  tLCSpecSchema = tLCSpecDBSet.get(j);
			  String errpol = tLCSpecSchema.getPolNo();
			  System.out.println("errpol="+errpol);
			  if (errpol.equals(tGrpProposalNo))
			  {
				LCSpecDB tLCSpecDB = new LCSpecDB(conn);
				tLCSpecDB.setSchema(tLCSpecSchema);
				if (tLCSpecDB.insert() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLCSpecDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "GrpUWManuNormChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = "特别约定表保存失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}
			  }
			}
		  }
          System.out.println("-----------HHH--------------");
		  // 交费项目表
		  LCPremDBSet tLCPremDBSet = new LCPremDBSet( conn );
		  tLCPremDBSet.set( mLCPremSet );
		  errno = tLCPremDBSet.size();
		  merrno = merrno + errno;
		  System.out.println("premcount"+errno);
		  if (errno > 0)
		  {
			for (int j = 1; j<=errno; j++)
			{
			  LCPremSchema tLCPremSchema = new LCPremSchema();
			  tLCPremSchema = tLCPremDBSet.get(j);
			  String errpol = tLCPremSchema.getPolNo();
			  if (errpol.equals(tGrpProposalNo))
			  {
				LCPremDB tLCPremDB = new LCPremDB(conn);
				tLCPremDB.setSchema(tLCPremSchema);
				if (tLCPremDB.insert() == false)
				{
				  // @@错误处理
				  this.mErrors.copyAllErrors(tLCPremDB.mErrors);
				  CError tError = new CError();
				  tError.moduleName = "GrpUWManuNormChkBLS";
				  tError.functionName = "saveData";
				  tError.errorMessage = "保费项目表保存失败!";
				  this.mErrors .addOneError(tError) ;
				  conn.rollback() ;
				  conn.close();
				  return false;
				}
			  }
			}
		  }
		}
        System.out.println("-----------JJJ--------------");

		conn.commit() ;
		conn.close();
		} // end of for
	  } // end of try
	  catch (Exception ex)
	  {
		// @@错误处理
		CError tError = new CError();
		tError.moduleName = "GrpUWManuNormChkBLS";
		tError.functionName = "submitData";
		tError.errorMessage = ex.toString();
		this.mErrors .addOneError(tError);
		return false;
	  }

	    return true;
	}

}
