package com.sinosoft.lis.cbcheck;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title:团体单问题修改确认BLS </p>
 * <p>Description:
 *  对团体单问题修改后进行确认.置团体整单及团单下未核保通过的个单的状态为新单复核状态
 *  </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author sxy
 * @version 1.0
 */

public class GrpQuestModifyMakeSureBLS {
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  /** 数据操作字符串 */
  private String mOperate;

  /** 错误处理类 */
  public CErrors mErrors = new CErrors();

  public GrpQuestModifyMakeSureBLS() {
  }

  /**
   * 数据提交的公共方法
   * @param: cInputData 传入的数据
   *		  cOperate 数据操作字符串
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate) {
    // 首先将数据在本类中做一个备份
    mInputData = (VData) cInputData.clone();
    this.mOperate = cOperate;

    // 保存数据
    if (this.saveData() == false)
      return false;

    // 释放数据容器
    mInputData = null;

    return true;
  }

  /**
   * 保存数据
   * @param: 无
   * @return: boolean
   */
  private boolean saveData() {
    // 分解传入数据容器中的数据
    LCGrpPolSchema mLCGrpPolSchema = (LCGrpPolSchema) mInputData.
        getObjectByObjectName("LCGrpPolSchema", 0);

    // 建立数据连接
    Connection conn = null;
    conn = DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError);
      return false;
    }

    try {
      conn.setAutoCommit(false);

      // 修改未核保通过的个人投保单
      ExeSQL tExeSQL = new ExeSQL(conn);
      String sql = "update LCPol "
          + "set ApproveCode = '" + mLCGrpPolSchema.getApproveCode() + "', "
          + "ApproveDate = '" + mLCGrpPolSchema.getApproveDate() + "', "
          + "ApproveFlag = '" + mLCGrpPolSchema.getApproveFlag() + "', "
          + "UWFlag = '" + mLCGrpPolSchema.getUWFlag() + "',"
          + "ModifyDate = '" + mLCGrpPolSchema.getModifyDate() + "', "
          + "ModifyTime = '" + mLCGrpPolSchema.getModifyTime() + "' "
          + "where GrpPolNo in ( select GrpPolNo from lcGrpPol where PrtNo='" +
          mLCGrpPolSchema.getPrtNo() +
          "' and appflag = '0' and approveflag = '2')"
          + "and appflag = '0'"
          + "and approveflag = '2'";
      if (tExeSQL.execUpdateSQL(sql) == false) {
        // @@错误处理
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpPolApproveBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "LCPolDBSet修改失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      // 修改团体单中未核保通过的附加险投保单
      tExeSQL = new ExeSQL(conn);
      sql = "update LCGrpPol "
          + "set ApproveCode = '" + mLCGrpPolSchema.getApproveCode() + "', "
          + "ApproveDate = '" + mLCGrpPolSchema.getApproveDate() + "', "
          + "ApproveFlag = '" + mLCGrpPolSchema.getApproveFlag() + "', "
          + "UWFlag = '" + mLCGrpPolSchema.getUWFlag() + "',"
          + "ModifyDate = '" + mLCGrpPolSchema.getModifyDate() + "', "
          + "ModifyTime = '" + mLCGrpPolSchema.getModifyTime() + "' "
          + "where PrtNo = '" + mLCGrpPolSchema.getPrtNo() + "'"
          +
          "and riskcode in (select riskcode from lmriskapp where subriskflag='S')"
          + "and appflag = '0'"
          + "and approveflag = '2'";
      if (tExeSQL.execUpdateSQL(sql) == false) {
        // @@错误处理
        this.mErrors.copyAllErrors(tExeSQL.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpPolApproveBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "LCGrpPolDBSet修改失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      // 修改集体投保单总表
      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB(conn);
      tLCGrpPolDB.setSchema(mLCGrpPolSchema);
      if (tLCGrpPolDB.update() == false) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "GrpPolApproveBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "LCGrpPol修改失败!";
        this.mErrors.addOneError(tError);
        conn.rollback();
        conn.close();
        return false;
      }

      conn.commit();
      conn.close();
    } // end of try
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpPolApproveBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try {
        conn.rollback();
        conn.close();
      }
      catch (Exception e) {}
      return false;
    }
    return true;
  }
}
