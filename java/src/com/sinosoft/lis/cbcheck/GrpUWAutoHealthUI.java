package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统体检资料录入部分</p>
 * <p>Description:承保个人单自动核保功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author WHN
 * @version 1.0
 */
public class GrpUWAutoHealthUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public GrpUWAutoHealthUI() {}

// @Main
  public static void main(String[] args)
  {
  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ComCode = "86";

  LCGrpContSchema p = new LCGrpContSchema();
  p.setGrpContNo( "140110000000500" );


  LCGrpContSet tLCGrpContSet = new LCGrpContSet();
  tLCGrpContSet.add(p);

  VData tVData = new VData();
  tVData.add( tLCGrpContSet );

  tVData.add( tG );
  GrpUWAutoHealthUI ui = new GrpUWAutoHealthUI();
  if( ui.submitData( tVData, "" ) == true )
      System.out.println("---ok---");
  else
      System.out.println("---NO---");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    GrpUWAutoHealthBL tGrpUWAutoHealthBL = new GrpUWAutoHealthBL();

    System.out.println("---GrpUWAutoHealthBL UI BEGIN---");
    if (tGrpUWAutoHealthBL.submitData(cInputData,mOperate) == false)
        {
                  // @@错误处理
      this.mErrors.copyAllErrors(tGrpUWAutoHealthBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpUWAutoHealthUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mResult.clear();
      return false;
        }
    return true;
  }
}