package com.sinosoft.lis.cbcheck;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author zhangxing
 * @version 1.0
 */

public class GrpApplyRecallPolBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private String mOperate;
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private MMap map = new MMap();
    private LCGrpApplyRecallPolSchema mLCGrpApplyRecallPolSchema = new
            LCGrpApplyRecallPolSchema();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mPrtNo = "";
    private String mGrpContNo = "";
    private String mMissionID = "";
    private String mCardFlag = "";

    //业务处理相关变量

    private LCPolSet mLCPolSet = new LCPolSet();
    private LCContSet mLCContSet = new LCContSet();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

    public GrpApplyRecallPolBL() {
    }

    public static void main(String[] args) {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        //进行业务处理
        if (!dealData(mOperate)) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    /**
     * checkData
     * 校验是否已经财务收付费
     * @return boolean
     */
    private boolean checkData() {
        /** 首先校验财务数据 */
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setOtherNo(this.mPrtNo);
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
        if (tLJTempFeeSet.size() > 0) {

            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = "该保单已经交费,无法撤销处理!";
            this.mErrors.addOneError(tError);
            return false;

        }

        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    private boolean dealData(String Operate) {
        //查询合同信息
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setPrtNo(mPrtNo);
        tLCGrpContSet.set(tLCGrpContDB.query());
        if (tLCGrpContDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = "合同信息查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLCGrpContSet == null || tLCGrpContSet.size() != 1) {
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareContUW";
            tError.errorMessage = "没有找到该印刷号对应的合同信息，请确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLCGrpContSchema = tLCGrpContSet.get(1).getSchema();
        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        mLCGrpContSchema.setUWFlag("a");

        /**
         * 处理该集体险重的核保结论
         */
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setPrtNo(mPrtNo);
        tLCGrpPolSet.set(tLCGrpPolDB.query());
        if (tLCGrpPolSet == null || tLCGrpPolSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "GrpApplyRecallPolBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有找到集体险种该印刷号对应的保单，请确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int w = 1; w <= tLCGrpPolSet.size(); w++) {
            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
            tLCGrpPolSchema.setSchema(tLCGrpPolSet.get(w));
            tLCGrpPolSchema.setUWFlag("a");
            mLCGrpPolSet.add(tLCGrpPolSchema);
        }

        /**
         * 处理该合同的每个人的核保结论
         */
        LCContSet tLCContSet = new LCContSet();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setPrtNo(mPrtNo);
        tLCContSet.set(tLCContDB.query());
        if (tLCContSet == null || tLCContSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "GrpApplyRecallPolBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有找到该印刷号对应的个人保单，请确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int k = 1; k <= tLCContSet.size(); k++) {
            LCContSchema tLCContSchema = new LCContSchema();
            tLCContSchema.setSchema(tLCContSet.get(k));
            tLCContSchema.setUWFlag("a");
            mLCContSet.add(tLCContSchema);
        }

        /**
         *  查询该保单下所有险种保单
         */

        LCPolSet tLCPolSet = new LCPolSet();
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPrtNo(mPrtNo);
        tLCPolSet.set(tLCPolDB.query());
        if (tLCPolSet == null || tLCPolSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有找到该印刷号对应的险种保单，请确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int j = 1; j <= tLCPolSet.size(); j++) {
            LCPolSchema aLCPolSchema = new LCPolSchema();
            aLCPolSchema.setSchema(tLCPolSet.get(j));
            aLCPolSchema.setUWFlag("a");
            mLCPolSet.add(aLCPolSchema);

        }

        /**
         * 生成撤保申请纪录
         */
        LCGrpApplyRecallPolSchema tLCGrpApplyRecallPolSchema = new
                LCGrpApplyRecallPolSchema();
        tLCGrpApplyRecallPolSchema.setPrtNo(mPrtNo);
        tLCGrpApplyRecallPolSchema.setGrpContNo(mGrpContNo);
        tLCGrpApplyRecallPolSchema.setRemark(mLCGrpApplyRecallPolSchema.
                                             getRemark());
        tLCGrpApplyRecallPolSchema.setApplyType(mLCGrpApplyRecallPolSchema.
                                                getApplyType()); //Add by Minim
        tLCGrpApplyRecallPolSchema.setOperator(mGlobalInput.Operator);
        tLCGrpApplyRecallPolSchema.setManageCom(mGlobalInput.ManageCom);
        tLCGrpApplyRecallPolSchema.setMakeDate(CurrentDate);
        tLCGrpApplyRecallPolSchema.setMakeTime(CurrentTime);
        tLCGrpApplyRecallPolSchema.setModifyDate(CurrentDate);
        tLCGrpApplyRecallPolSchema.setModifyTime(CurrentTime);

        map.put(mLCPolSet, "UPDATE");
        map.put(mLCContSet, "UPDATE");
        map.put(mLCGrpPolSet, "UPDATE");
        map.put(mLCGrpContSchema, "UPDATE");
        map.put(tLCGrpApplyRecallPolSchema, "DELETE");
        map.put(tLCGrpApplyRecallPolSchema, "INSERT");

        //获取工作流任务ID
        if (!getMissionID()) {
            return false;
        }

        //删除保单工作流节点 [备份]
        LWMissionSet tLWMissionSet = new LWMissionSet();
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionID);
        tLWMissionSet.set(tLWMissionDB.query());
        if (tLWMissionDB.mErrors.needDealError() == true) {
            // @@错误处理
            mErrors.copyAllErrors(tLWMissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpGiveEnsureBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "保单工作流任务查询失败!" +
                                  "印刷号：" + mPrtNo +
                                  "任务号：" + mMissionID;
            mErrors.addOneError(tError);
            return false;
        }
        Reflections tReflections = new Reflections();
        LBMissionSet tLBMissionSet = new LBMissionSet();
        LBMissionSchema tLBMissionSchema;
        String tSerielNo = "";
        for (int i = 1; i <= tLWMissionSet.size(); i++) {
            tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

            tLBMissionSchema = new LBMissionSchema();
            tReflections.transFields(tLBMissionSchema, tLWMissionSet.get(i));

            tLBMissionSchema.setSerialNo(tSerielNo);
            tLBMissionSchema.setActivityStatus("3"); //节点任务执行完毕
            tLBMissionSchema.setLastOperator(mGlobalInput.Operator);
            tLBMissionSchema.setMakeDate(CurrentDate);
            tLBMissionSchema.setMakeTime(CurrentTime);
            tLBMissionSchema.setModifyDate(CurrentDate);
            tLBMissionSchema.setModifyTime(CurrentTime);
            tLBMissionSet.add(tLBMissionSchema);
        }
        map.put(tLBMissionSet, "INSERT");
        map.put(tLWMissionSet, "DELETE");

        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData() {
        mLCGrpApplyRecallPolSchema =
                (LCGrpApplyRecallPolSchema)
                mInputData.getObjectByObjectName("LCGrpApplyRecallPolSchema", 0);

        mGlobalInput =
                (GlobalInput)
                mInputData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        if (mLCGrpApplyRecallPolSchema == null ||
            mLCGrpApplyRecallPolSchema.getPrtNo() == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传输数据失败!";
            mErrors.addOneError(tError);
            return false;
        }

        mPrtNo = mLCGrpApplyRecallPolSchema.getPrtNo();
        if (mTransferData == null) {
            mCardFlag = "";
        } else {
            mCardFlag = (String) mTransferData.getValueByName("CardFlag");
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    //准备往后层输出所需要的数据
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean getMissionID() {
        LWMissionSet tLWMissionSet = new LWMissionSet();
        LBMissionSet tLBMissionSet = new LBMissionSet();
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LBMissionDB tLBMissionDB = new LBMissionDB();

        LWProcessInstanceSet tLWProcessInstanceSet = new LWProcessInstanceSet();
        LWProcessInstanceDB tLWProcessInstanceDB = new LWProcessInstanceDB();
        if (mCardFlag.equals("1")) {
            tLWProcessInstanceDB.setProcessID("0000000005");
        }
        tLWProcessInstanceDB.setStartType("0"); //根节点
        tLWProcessInstanceSet.set(tLWProcessInstanceDB.query());
        if (tLWProcessInstanceDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLWProcessInstanceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDContTimeBL";
            tError.functionName = "getContNo";
            tError.errorMessage = "工作流过程实例查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLWProcessInstanceSet == null || tLWProcessInstanceSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "LDContTimeBL";
            tError.functionName = "getContNo";
            tError.errorMessage = "工作流过程实例查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //循环查找该过程所有根节点寻找MissionID
        String tActivityID;
        for (int i = 1; i <= tLWProcessInstanceSet.size(); i++) {
            tActivityID = tLWProcessInstanceSet.get(i).getTransitionStart();

            LWFieldMapSet tLWFieldMapSet = new LWFieldMapSet();
            LWFieldMapDB tLWFieldMapDB = new LWFieldMapDB();
            tLWFieldMapDB.setActivityID(tActivityID);
            tLWFieldMapSet.set(tLWFieldMapDB.query());
            if (tLWFieldMapDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLWFieldMapDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LDContTimeBL";
                tError.functionName = "getContNo";
                tError.errorMessage = "工作流任务字段映射查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            LWFieldMapSchema tLWFieldMapSchema = new LWFieldMapSchema();
            for (int j = 1; j <= tLWFieldMapSet.size(); j++) {
                tLWFieldMapSchema.setSchema(tLWFieldMapSet.get(j));
                //找到印刷号影射字段
                if (tLWFieldMapSchema.getSourFieldName().equals("PrtNo")) {
                    //匹配该印刷号在此节点是否有任务
                    tLWMissionSet.clear();
                    tLWMissionDB = new LWMissionDB();
                    tLWMissionDB.setActivityID(tActivityID);
                    tLWMissionDB.setV(tLWFieldMapSchema.getDestFieldName(),
                                      mPrtNo);
                    tLWMissionSet.set(tLWMissionDB.query());
                    if (tLWMissionDB.mErrors.needDealError() == true) {
                        // @@错误处理
                        mErrors.copyAllErrors(tLWMissionDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ApplyRecallPolBL";
                        tError.functionName = "getBaseData";
                        tError.errorMessage = "保单工作流任务ID查询失败!" +
                                              "印刷号：" + mPrtNo;
                        mErrors.addOneError(tError);
                        return false;
                    }
                    if (tLWMissionSet != null && tLWMissionSet.size() == 1) {
                        mMissionID = tLWMissionSet.get(1).getMissionID();
                        System.out.println("==MissionID==" + mMissionID);
                        return true;
                    }
                    if (StrTool.cTrim(this.mCardFlag).equals("1")) {
                        if (tLWMissionSet != null && tLWMissionSet.size() > 0) {
                            mMissionID = tLWMissionSet.get(1).getMissionID();
                            System.out.println("==MissionID==" + mMissionID);
                            return true;
                        }
                    }
                    //查找备份表
                    if (tLWMissionSet == null || tLWMissionSet.size() == 0) {
                        tLBMissionSet.clear();
                        tLBMissionDB = new LBMissionDB();
                        tLBMissionDB.setActivityID(tActivityID);
                        tLBMissionDB.setV(tLWFieldMapSchema.getDestFieldName(),
                                          mPrtNo);
                        tLBMissionSet.set(tLBMissionDB.query());
                        if (tLBMissionDB.mErrors.needDealError() == true) {
                            // @@错误处理
                            mErrors.copyAllErrors(tLBMissionDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "ApplyRecallPolBL";
                            tError.functionName = "getBaseData";
                            tError.errorMessage = "保单工作流任务ID查询失败!" +
                                                  "印刷号：" + mPrtNo;
                            mErrors.addOneError(tError);
                            return false;
                        }
                        if (tLBMissionSet != null && tLBMissionSet.size() == 1) {
                            mMissionID = tLBMissionSet.get(1).getMissionID();
                            System.out.println("==MissionID==" + mMissionID);
                            return true;
                        }
                        if (StrTool.cTrim(this.mCardFlag).equals("1")) {
                            if (tLBMissionSet != null &&
                                tLBMissionSet.size() > 0) {
                                mMissionID = tLBMissionSet.get(1).getMissionID();
                                System.out.println("==MissionID==" + mMissionID);
                                return true;
                            }
                        }
                    }
                }
            }
        }
        if (mMissionID == null || "".equals(mMissionID)) {
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "保单工作流任务ID查询失败!" +
                                  "印刷号：" + mPrtNo;
            mErrors.addOneError(tError);
            return false;
        }
        //如果有任务正处于申请处理中，不能撤单
        tLWMissionSet.clear();
        tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionID);
        tLWMissionSet.set(tLWMissionDB.query());
        if (tLWMissionDB.mErrors.needDealError() == true) {
            // @@错误处理
            mErrors.copyAllErrors(tLWMissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ApplyRecallPolBL";
            tError.functionName = "getBaseData";
            tError.errorMessage = "保单工作流任务查询失败!" +
                                  "印刷号：" + mPrtNo;
            mErrors.addOneError(tError);
            return false;
        }
        //当前处理申请人
        String tDefaultOperator;
        for (int i = 1; i <= tLWMissionSet.size(); i++) {
            tDefaultOperator = tLWMissionSet.get(i).getDefaultOperator();
            if (tDefaultOperator != null && !"".equals(tDefaultOperator)) {
                CError tError = new CError();
                tError.moduleName = "ApplyRecallPolBL";
                tError.functionName = "getBaseData";
                tError.errorMessage = "保单工作流任务正处于申请处理中，不能撤单!" +
                                      "印刷号：" + mPrtNo +
                                      "申请人：" + tDefaultOperator;
                mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

}
