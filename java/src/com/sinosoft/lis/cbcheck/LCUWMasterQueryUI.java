package com.sinosoft.lis.cbcheck;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class LCUWMasterQueryUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;


  public LCUWMasterQueryUI() {}

  public static void main(String[] args)
 {
   LCPolSchema p = new LCPolSchema();
   p.setPolNo( "00010220020110000003" );
   VData v = new VData();
   v.add( p );
   LCUWMasterQueryUI PUI = new LCUWMasterQueryUI();
   PUI.submitData( v, "QUERY||MAIN" );
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate = cOperate;

	LCUWMasterQueryBL tLCUWMasterQueryBL=new LCUWMasterQueryBL();
    System.out.println("begin LCUWMasterQueryUI") ;
	if (tLCUWMasterQueryBL.submitData(cInputData,mOperate) == false)
	{
		  // @@错误处理
	  this.mErrors.copyAllErrors(tLCUWMasterQueryBL.mErrors);
	  CError tError = new CError();
	  tError.moduleName = "ProposalQueryUI";
	  tError.functionName = "submitData";
	  tError.errorMessage = "数据查询失败!";
	  this.mErrors .addOneError(tError) ;
	  mInputData.clear();
	  return false;
	}
	else
		mInputData = tLCUWMasterQueryBL.getResult();
    System.out.println("end  LCUWMasterQueryUI") ;
	return true;
  }

  public VData getResult()
  {
	  return mInputData;
  }

}